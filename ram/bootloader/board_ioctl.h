#ifndef _BOARD_IOCTL_H_
#define _BOARD_IOCTL_H_

#define BIOC_SET_INIT_PRE                1 /*pre initialize*/
#define BIOC_SET_INIT_POST               2 /*post initialize*/
#define BIOC_GET_BURNKEY                 3 /*board burnkey*/
#define BIOC_SET_BOOT                    4 /*board boota*/

#include "board_ioctl_func.h"

#endif
