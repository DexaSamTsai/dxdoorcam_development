#include "includes.h"
#include "libCommon.h"
#include "board_ioctl.h"
#include "mcu_ctrl.h"

static void board_init_pre(void)
{
	WATCHDOG_DISABLE;
	lowlevel_putc('\n');
	lowlevel_putc('B');
}

static void board_init_post(void)
{
}

static void board_boot(void){
	lowlevel_putc('B');
	lowlevel_putc('O');
	lowlevel_putc('O');
	lowlevel_putc('T');
	g_sflash.sta_reg_qe_bit = 0; //do not set qe register to reduce boot time
	g_sflash.check_id_en = 0;
	sf_init(SF_SOURCE_SFC, &g_sflash);
	mcu_init();
	u32 evt = mcu_get_event_nortos();
	u32 next_fw;
	mcu_set_led_nortos(LED_COLOR_RED, LED_STATUS_ON);
	mcu_set_led_nortos(LED_COLOR_GREEN, LED_STATUS_OFF);
	debug_printf("wakeup event %d\n", evt);
	switch (evt) {
	default:
		next_fw = FIRMWARE_DOORCAM;
		break;
	}
	partition_boot_sf(next_fw);
	return;
}

s32 board_ioctl(u32 cmd, u32 arg, void * ret_arg)
{
	switch(cmd){
		case BIOC_SET_INIT_PRE:
			board_init_pre();
			return 0;
		case BIOC_SET_INIT_POST:
			board_init_post();
			return 0;
		case BIOC_SET_BOOT:
			board_boot();
			return 0;
		case BIOC_HW_EARLY_INIT:
			return DxDoorCam_hw_early_init();
		default:
			return -EINVAL;
	}
}
