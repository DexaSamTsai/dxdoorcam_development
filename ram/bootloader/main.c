#include "includes.h"
#include "board_ioctl.h"

int except_tick(void)
{
	return 0;
}

int main(void)
{
	WATCHDOG_DISABLE;

	BSS_CLEAR();

#if defined(CONFIG_SYSLOG_LEVEL) && (CONFIG_SYSLOG_LEVEL > 0)
	syslog_noos_level = CONFIG_SYSLOG_LEVEL;
#endif
#ifdef BOOT_TIME_TEST
	syslog_noos_level = 4;
#endif
	if(board_ioctl(BIOC_HW_EARLY_INIT, 0, NULL) < 0){
		//FIXME: if not implemented, set default to 360M with DDR3 400M
		//in most cases, the ram/ should implement the clock config in board_xxx.c BIOC_HW_EARLY_INIT !!! Don't use the default one
#ifndef FOR_FPGA
		clockconfig_360M();
		ddrset_ddr3_400M();
#endif
	}

#ifdef CONFIG_HAS_SF
	if(g_sflash_fun_init.init == NULL){
		//if sflash not inited in HW_EARLY_INIT
		sf_init(SF_SOURCE_SFC, &g_sflash);
	}
#endif

	lowlevel_config(115200, 0);

#if defined(CONFIG_ICACHE) || defined(CONFIG_DCACHE)
	tlb_set_bufaddr((u32 *)0x100c8000);
#endif

#ifdef CONFIG_ICACHE
	ic_enable();
#endif

#ifdef CONFIG_DCACHE
	dc_enable();
#endif

	board_ioctl(BIOC_SET_INIT_PRE, 0, NULL);

	board_ioctl(BIOC_SET_INIT_POST, 0, NULL);

	board_ioctl(BIOC_SET_BOOT, 0, NULL);

	while(1){
		uart_forceputc('E');

		{volatile int d; for(d=0; d<100; d++);}
		BOOT_SRAM();
		asm("dsb");
		asm("wfe");
		asm("wfe");
	}
}
