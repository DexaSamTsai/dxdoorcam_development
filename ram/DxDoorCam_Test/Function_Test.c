/*
 * function_test.c
 *  Created on: 20180601
 *      Author: Leon Liao
 */
#include "Function_Test.h"

t_function_test_list g_function_test[]= {
//      {function point, command name, comment, 0}
        {dxOSTest_main, "os", "Test OS", 0},
        {dxBSPTest_main, "bsp", "Test BSP", 0},
        {dxDNSTest_main, "dns", "Test DNS", 0},
        {MCUUart_test, "mcuuart", "Test MCU Uart", 0},
        { NULL, "EOF", "EOF", 0, NULL, NULL },
};

static int 
_FunctionTest_Shell(int argc, char ** argv)
{
    int count=0;
	if( argc != 2 ){
		goto test_usage;
	}
    if( !strncmp( argv[1], "EOF", 3 ) ){
		goto test_usage;
    }
    
    if( !strncmp( argv[1], "all", 3) ) //Run all functions.
    {
        syslog(LOG_INFO,  "<---TEST  ALL--->\n");  
        count=0;
        while(g_function_test[count].FunctionPoint)
        {
            syslog(LOG_INFO,  "<TEST Function Start---Function[%s]+++++++++++++++>\n",   g_function_test[count].FunctionName);  
            g_function_test[count].FunctionResult=g_function_test[count].FunctionPoint();
            syslog(LOG_INFO,  "<TEST Function Result[%s(%d)]------------------------>\n",   g_function_test[count].FunctionName,g_function_test[count].FunctionResult ); 
            count++;
        }
        goto test_result;
    }
    else
    {
        count=0;
        do
        {
            if( !strncmp(argv[1], g_function_test[count].FunctionName, strlen(g_function_test[count].FunctionName)))
            {
                syslog(LOG_INFO,  "<TEST Function Start---Function[%s]+++++++++++++++>\n",   g_function_test[count].FunctionName);  
                g_function_test[count].FunctionResult=g_function_test[count].FunctionPoint();
                syslog(LOG_INFO,  "<TEST Function Result[%s(%d)]------------------------>\n",   g_function_test[count].FunctionName,g_function_test[count].FunctionResult ); 
                break;
            }

            count++;
        }
        while(g_function_test[count].FunctionPoint);
        
        if(!strncmp(g_function_test[count].FunctionName, "EOF",3))
        {
            goto test_usage;
        }
     }
	return 0;

test_result:
	syslog(LOG_INFO,  "\n=======Function test result========\n"  );  
	count=0;
	while(g_function_test[count].FunctionPoint)
    {
        syslog(LOG_INFO,  "Function %s ->result(%d)\n",  g_function_test[count].FunctionName,   g_function_test[count].FunctionResult);  
        count++;
    }
    syslog(LOG_INFO,  "=================================\n"  );  
	return 0;

test_usage:

	syslog(LOG_INFO,  "\n=======Function test usage========\ntest all : all functions test"  );  
	count=0;
	while(g_function_test[count].FunctionPoint)
    {
        syslog(LOG_INFO,  "test %s : %s\n",  g_function_test[count].FunctionName,   g_function_test[count].FunctionComment);  
        count++;
    }
    syslog(LOG_INFO,  "=================================\n"  );  
	return 0;


}

int FunctionTest_CmdStart(void)
{
	int ret = -1;
	ret = cmdshell_add("test", _FunctionTest_Shell, "Test DxFramwork functions by shell");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init UART(shell) setup failed\n");
	}

	return ret;
}

int FunctionTest_CmdStop(void)
{
	int ret = -1;
	ret = cmdshell_del("test");
	return ret;
}

#if 1
/*OV798   Test function */
int MCUUart_test(void)
{
    uarts_init(clockget_bus(), 9600, 0);

    if(API_msp430_bsl_set_baudrate(115200))
    {
        return -1;

    }
    return 0;
}
#endif

