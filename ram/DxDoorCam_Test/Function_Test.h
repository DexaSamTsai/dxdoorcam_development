/*
 * function_test.h
 *  Created on: 20180601
 *      Author: Leon Liao
 */
 #ifndef _FUNCTION_TEST_H_
#define _FUNCTION_TEST_H_

 #include "includes.h"
 
//Struct
typedef struct s_function_test_list
{
    int (* FunctionPoint)(void);
	u8*    FunctionName;
	u8*    FunctionComment;
	int    FunctionResult;
}t_function_test_list;

//API
int FunctionTest_CmdStart(void);
int FunctionTest_CmdStop(void);
int MCUUart_test(void);

//Extern function
extern int dxBSPTest_main(void) ;
extern int dxDNSTest_main(void);
extern int dxOSTest_main(void);
extern int API_msp430_bsl_set_baudrate(int baundrate);

#endif //_FUNCTION_TEST_H_