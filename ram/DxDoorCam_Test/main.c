/*
 * function_test.c
 *  Created on: 20180601
 *      Author: Leon Liao
 */
//==========================================================================
// Include File
//==========================================================================
#include "includes.h"
#include "board_ioctl.h"
#include "mcu_ctrl.h"
#include "Function_Test.h"
#include "ISP_Test.h"

//==========================================================================
// APIs
//==========================================================================

void * 
thread_main( void *arg )
{
    board_ioctl( BIOC_SET_SYS_INIT, 0, NULL );

    if ( DxDoorCam_Init() < 0 ) {
        //DxDoorCam_stop();
        //TODO Wifi Setup
    }

    board_ioctl( BIOC_SET_HW_INIT, 0, NULL );

    if(mcu_get_mode() == MODE_INIT || getenv("wifi_dnsserver") == NULL){
        DxDoorCam_SetFirstBoot( 1 );
	} else {
        DxDoorCam_SetFirstBoot( 0 );
    }

    DxDoorCam_InitNetwork();

    syslog(LOG_INFO, "Input test command or [test] or [ispt] to show command list:\n");
    FunctionTest_CmdStart();
    ISPTest_CmdStart();
    
    //DxDoorCam_ExitNetwork();
    //DxDoorCam_Exit();
    //sleep(5);
    //DxDoorCam_PowerOff(  );
    
    return NULL;
}
