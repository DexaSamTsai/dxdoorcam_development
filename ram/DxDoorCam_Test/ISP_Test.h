/*
 * ISP_test.h
 *  Created on: 20180701
 *      Author: Leon Liao
 */
 #ifndef _ISP_TEST_H_
#define _ISP_TEST_H_

 #include "includes.h"
 
//Struct
typedef struct s_ISP_test_list
{
	int    FunctionNum;
    int (* FunctionPoint_0)(void);
    int (* FunctionPoint_1)(int);
    int (* FunctionPoint_2)(int, int);
	char*    FunctionName;
	char*    FunctionComment;
	int    FunctionResult;
}t_ISP_test_list;

//API
int ISPTest_CmdStart(void);
int ISPTest_CmdStop(void);

int set_autofps(int max, int min);

#endif //_ISP_TEST_H_