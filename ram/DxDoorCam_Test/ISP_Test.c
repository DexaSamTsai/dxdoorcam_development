/*
 * function_test.c
 *  Created on: 20180601
 *      Author: Leon Liao
 */
#include "ISP_Test.h"

t_ISP_test_list g_ISP_test[]= {
//  {function point(), function point(a), function point(a,b), command name, comment, 0, function name}
    { 2, NULL, NULL, libvs_set_lightfreq,           "vlig",    "libvs_set_lightfreq(int vs_id, u32 lightfreq)", 0,},
    { 2, NULL, NULL, libvs_set_brightness,          "vbri",    "libvs_set_brightness(int vs_id, u32 brightness)", 0,},
    { 2, NULL, NULL, libvs_set_sde,                 "vsde",    "libvs_set_sde(int vs_id, int sde_effect_No)", 0, },
    { 2, NULL, NULL, libvs_set_daynight,            "vsdn",    "libvs_set_daynight(int vs_id, int daynight)", 0,},
    { 2, NULL, NULL, libvs_set_exp,                 "vexp",    "(TODO)libvs_set_exp(int vs_id, u32 exp)", 0, },
    { 2, NULL, NULL, libvs_set_gain,                "vgai",    "(TODO)libvs_set_gain(int vs_id, u32 gain)", 0, },
    { 2, NULL, NULL, libvs_set_mirror,              "vmir",    "libvs_set_mirror(int vs_id, u32 mirror)", 0,},
    { 2, NULL, NULL, libvs_set_flip,                "vfli",    "libvs_set_flip(int vs_id, u32 flip)", 0,},
    { 2, NULL, NULL, libvs_set_mirror_flip,         "vmaf",    "libvs_set_mirror_flip(int vs_id, u32 mirror_flip)", 0,},
    { 2, NULL, NULL, libvs_set_contrast,            "vcon",    "libvs_set_contrast(int vs_id, u32 contrast)", 0,},
    { 2, NULL, NULL, libvs_set_saturation,          "vsat",    "libvs_set_saturation(int vs_id, u32 saturation)", 0,},
    { 2, NULL, NULL, libvs_set_wdr,                 "vwdr",    "libvs_set_wdr(int vs_id, u32 wdr)", 0,},
    
    { 2, NULL, NULL, libisp_set_exp,                "isexp",   "(TODO)libisp_set_exp(int isp_id, u32 exp)", 0, },
    { 2, NULL, NULL, libisp_set_gain,               "isgai",   "(TODO)libisp_set_gain(int isp_id, u32 gain)", 0, },
    { 2, NULL, NULL, libisp_set_awb_manual,         "iseaw",   "libisp_set_awb_manual(int isp_id, u32 mode)", 0,},
    { 2, NULL, NULL, libisp_set_awb_b_gain,         "isawb",   "libisp_set_awb_b_gain(int isp_id, u32 awb)", 0,      },
    { 2, NULL, NULL, libisp_set_awb_g_gain,         "isawg",   "libisp_set_awb_g_gain(int isp_id, u32 awb)", 0,      },
    { 2, NULL, NULL, libisp_set_awb_r_gain,         "isawr",   "libisp_set_awb_r_gain(int isp_id, u32 awb)", 0,      },
    { 2, NULL, NULL, libisp_set_sharpness,          "issga",   "libisp_set_sharpness(int isp_id, int sharpness_level)", 0,  },
    { 2, NULL, NULL, libisp_set_sde,                "issde",   "libisp_set_sde(int isp_id, int sde_effect_No)", 0,},
    { 2, NULL, NULL, libisp_set_brightness,         "isbri",   "libisp_set_brightness(int isp_id, int brightness_level)", 0,},
    { 2, NULL, NULL, libisp_set_daynight,           "issdn",   "libisp_set_daynight(int isp_id, int daynight)", 0,},
    { 2, NULL, NULL, libisp_set_hdr,                "ishdr",   "libisp_set_hdr(int isp_id, u32 val)", 0,},
    { 2, NULL, NULL, libisp_set_aec_manual,         "isaec",   "libisp_set_aec_manual(int isp_id, u32 enable)", 0,},
    { 2, NULL, NULL, libisp_set_aec_manual_gain,    "isaga",   "libisp_set_aec_manual_gain(int isp_id, u32 gain)", 0,},
    { 2, NULL, NULL, libisp_set_aec_manual_exp,     "isaex",   "libisp_set_aec_manual_exp(int isp_id, u32 exp)", 0,},
    { 2, NULL, NULL, libisp_set_roi,                "isroi",   "libisp_set_roi(int isp_id, u32 roi)", 0,},
        
    { 1, NULL, libisp_get_lum, NULL,                "iglum",   "libisp_get_lum(int isp_id)", 0,},
        { 1, NULL, libisp_get_exp, NULL,                "igexp",   "libisp_get_exp(int isp_id)", 0, },
        { 1, NULL, libisp_get_gain, NULL,               "iggai",   "libisp_get_gain(int isp_id)", 0, },
        { 1, NULL, libisp_get_awb_b_gain, NULL,         "igawb",   "libisp_get_awb_b_gain(int isp_id)", 0,      },
        { 1, NULL, libisp_get_awb_g_gain, NULL,         "igawg",   "libisp_get_awb_g_gain(int isp_id)", 0,      },
        { 1, NULL, libisp_get_awb_r_gain, NULL,         "igawr",   "libisp_get_awb_r_gain(int isp_id)", 0,      },
        { 1, NULL, libisp_get_daynight, NULL,           "igsdn",   "libisp_get_daynight(int isp_id)", 0,  },
        { 1, NULL, libisp_get_hdr, NULL,                "ighdr",   "libisp_get_hdr(int isp_id)", 0,},
    
        { 2, NULL, NULL, set_autofps,                   "safps",   "set_autofps(int max, int min)", 0,      },
    
        { 0, NULL, NULL, NULL,  "EOF", NULL, 0,      },
    };


static int 
_ISPTest_Shell(int argc, char ** argv)
{
    int count=0;
	if( argc < 2 ){
		goto test_usage;
	}
    if( !strncmp( argv[1], "EOF", 3 ) ){
		goto test_usage;
    }
    
    count=0;
    do
    {
        if( !strncmp(argv[1], g_ISP_test[count].FunctionName, strlen(g_ISP_test[count].FunctionName)))
        {
            syslog(LOG_INFO,  "<TEST Function Start---Function[%s]+++++++++++++++>\n",   g_ISP_test[count].FunctionName);  
            if(g_ISP_test[count].FunctionNum==1)
            {
            	if( argc != 3 ){
		            goto test_usage;
	            }
                int input_01=0;
                input_01 = atoi(argv[2]);
                g_ISP_test[count].FunctionResult=g_ISP_test[count].FunctionPoint_1(input_01);
             }
             else if(g_ISP_test[count].FunctionNum==2)
             {
                if( argc != 4 ){
		            goto test_usage;
	            }
                int input_01=0;
                u32 input_02=0;
                input_01 = atoi(argv[2]);
                input_02 = atoi(argv[3]);
                g_ISP_test[count].FunctionResult=g_ISP_test[count].FunctionPoint_2(input_01, input_02);
             }
             else
             {
                g_ISP_test[count].FunctionResult=g_ISP_test[count].FunctionPoint_0();
             }
            syslog(LOG_INFO,  "<TEST Function Result[%s(%d)]------------------------>\n",   g_ISP_test[count].FunctionName,g_ISP_test[count].FunctionResult ); 
            break;
        }
        count++;
    }
    while(g_ISP_test[count].FunctionComment !=NULL);
        
    if(!strncmp(g_ISP_test[count].FunctionName, "EOF",3))
    {
        goto test_usage;
    }
	return 0;

test_usage:
	syslog(LOG_INFO,  "\n=======Function test usage========\n"  );  
	count=0;
    while(g_ISP_test[count].FunctionComment !=NULL)
    {
        syslog(LOG_INFO,  "test %s : %s\n",  g_ISP_test[count].FunctionName,   g_ISP_test[count].FunctionComment);  
        count++;
    }
    syslog(LOG_INFO,  "=================================\n"  );  
	return 0;
}

int set_autofps(int max, int min)
{
    syslog(LOG_INFO,  "set_autofps(%d,%d)\n",max,min  );  
    return libisp_set_autofps(0,1,max,min);
}


int ISPTest_CmdStart(void)
{
    int ret = -1;
	ret = cmdshell_add("ispt", _ISPTest_Shell, "Test Libisp and Libvs functions by shell");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init ISP test(shell) setup failed\n");
	}

	return ret;
}

int ISPTest_CmdStop(void)
{
	int ret = -1;
	ret = cmdshell_del("ispt");
	return ret;
}
