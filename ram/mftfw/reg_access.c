#include "calfw_includes.h"
#include "prj_cfg.h"
#include "basic/production.h"

int g_ddr_inited = 0;
static u8  iq_update_state = 0;
void app_reg_wr(u8 sccb_id, u16 sccb_addr, u8 sccb_v)
{
	debug_printf("reg_wr: %x:%x:%x\n", sccb_id, sccb_addr, sccb_v);

	if(sccb_id < 0x10){
		if((sccb_id & ~1) == 0x08){
			// ISP
		}else if((sccb_id & ~1) == 0x0a){
			//SC
			switch(sccb_addr) {
				case 0x0001:
					/*
					       1: LPDDR2_533
					       2: LPDDR2_400
					       4: LPDDR2_200
					      11: DDR3_533
					      12: DDR3_400
					      14: DDR3_200
					   */
					ddr_disable();
					switch(sccb_v){
					case 1:
						ddrset_lpddr2_533M();
						break;
					case 2:
						ddrset_lpddr2_400M();
						break;
					case 4:
						ddrset_lpddr2_200M();
						break;
					case 11:
						ddrset_ddr3_533M();
						break;
					case 12:
						ddrset_ddr3_400M();
						break;
					default:
						syslog(LOG_ERR, "ddrset value not support:%d\n", sccb_v);
						break;
					}
					g_ddr_inited = 1;
					break;
				/*
				case 0x002:
					prj_cfg.pincfg.pin_bsl_rst = sccb_v;
					break;
				case 0x003:
					prj_cfg.pincfg.bsl_rst_level = sccb_v;
					break;
                case 0x004:
					prj_cfg.pincfg.pin_bsl_jtag = sccb_v;
					break;
				*/
				case 0x005:
					prj_cfg.pincfg.sn_reset = sccb_v;
				    break;
				case 0xff06:
					input_width &= 0x00ff;
					input_width |= (((u16)sccb_v) << 8);
					debug_printf("fb -- cmd:0x%x, width=%d, v=%d\n", sccb_addr, input_width, sccb_v);
					break;
				case 0xff07:
					input_width &= 0xff00;
					input_width |= sccb_v;
					debug_printf("fb -- cmd:0x%x, width=%d, v=%d\n", sccb_addr, input_width, sccb_v);
					break;
				case 0xff08:
					input_height &= 0x00ff;
					input_height |= (((u16)sccb_v) << 8);
					debug_printf("fb -- cmd:0x%x, height=%d, v=%d\n", sccb_addr, input_height, sccb_v);
					break;
				case 0xff09:
					input_height &= 0xff00;
					input_height |= sccb_v;
					debug_printf("fb -- cmd:0x%x, height=%d, v=%d\n", sccb_addr, input_height, sccb_v);
					break;
				default:
					break;
			}
		}
		else if((sccb_id & ~1) == 0x0c){	// cali mode
			// cali mode
			switch(sccb_addr){
				case 0x0000:
					// af vosd enable/disable
					if(sccb_v == 0x01){
					}else if(sccb_v == 0x02){
					}else if(sccb_v == 0x03){
						cali_aec_manual_enable();
					}else if(sccb_v == 0x04){
						cali_aec_manual_disable();
					}else if(sccb_v == 0x05){
						cali_set_daymode();
					}else if(sccb_v == 0x06){
						cali_set_nightmode();
					}
					break;
				case 0x0001:
					// erase cali item
					erase_cali_data(sccb_v);
					break;
				case 0x0002:
					// check cali item validity
					check_cali_data(sccb_v);
					break;
				case 0x0003:
					break;
				case 0x0004:
					break;
				case 0x0005:
					// get cali item
					if(sccb_v < 64){
						get_cali_data(sccb_v);
					}
					break;
				case 0x0006:
					if(sccb_v == 1){	// lens shading for day
						cali_lens_dtbl_update();
					}else if(sccb_v == 2){	// color temp
						cali_color_update();
					}else if(sccb_v == 3){	// lens shading for A
						cali_lens_atbl_update();
					}else if(sccb_v == 4){	// lens shading for C
						cali_lens_ctbl_update();
						sensor_setting_backup();
			
					}else if(sccb_v == 0xff){	// erase
						cali_erase_all();
					}else if(sccb_v == 0xf1){	// erase lens shading data
						sensor_lens_default();
					}else if(sccb_v == 0xf2){	// erase color temp data
					}else if(sccb_v == 0xf3){
						sensor_lens_atbl_default();
					}else if(sccb_v == 0xf4){
						sensor_lens_ctbl_default();
					}else if(sccb_v == 0xf5){
					}
					break;
				case 0x0007:
					break;
				case 0x0008:
					cali_awb_mode(sccb_v);
					break;
				case 0x0010:
					if(sccb_v == 0){
						u32 laddr = libisp_get_laddr();
						WriteReg32(laddr + 0x0040, 0x01010107);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
					}else if(sccb_v == 1){
						u32 laddr = libisp_get_laddr();
						WriteReg32(laddr + 0x0040, 0x01010207);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
					}
					break;
				case 0x0011:
					break;
				case 0x0012:
					break;
				case 0x0013:
					break;
				case 0x0014:
					prj_cfg.calicfg.manual_gain &= 0xff00;
					prj_cfg.calicfg.manual_gain |= sccb_v;
					break;
				case 0x0015:
					prj_cfg.calicfg.manual_gain &= 0x00ff;
					prj_cfg.calicfg.manual_gain |= (((u16)sccb_v) << 8);
					cali_aec_manual_gain(prj_cfg.calicfg.manual_gain);
					break;
				case 0x0016:
					cali_devs_load();
					break;
				case 0x0017:
					prj_cfg.calicfg.manual_exp &= 0xff00;
					prj_cfg.calicfg.manual_exp |= sccb_v;
					break;
				case 0x0018:
					prj_cfg.calicfg.manual_exp &= 0x00ff;
					prj_cfg.calicfg.manual_exp |= (((u16)sccb_v) << 8);
					cali_aec_manual_exp(prj_cfg.calicfg.manual_exp);
					break;
				case 0x0021:
//					board_ioctl(BIOC_SET_AWB_DEFAULT, sccb_v, NULL);
					break;
				case 0x0022:
					// fb_init : 0 yuy2, 1 raw, 3 yuv
					if(sccb_v == 1){
						WriteReg32(ISP_BASE_ADDR+0x8000, 0x8a7f0100);
						fb_init(0);
					}else if(sccb_v == 2){
						  if (mpu_init() != 0) {
							  debug_printf("mpu_init failed\n");
							  return;
						  }
					     fb_init(1);
					}else if(sccb_v == 3){
						WriteReg32(ISP_BASE_ADDR+0x8000, 0x0050000|BIT27|BIT25|BIT17); // awb enable, lens shading enable
						fb_init(0);
					}else if(sccb_v == 4){
						WriteReg32(ISP_BASE_ADDR+0x8000, 0x0050000);	// CIP Only
						fb_init(0);
					}else if(sccb_v == 5){
						  if (mpu_init() != 0) {
							  debug_printf("mpu_init failed\n");
							  return;
						  }
						 if(iq_update_state == 0){
							 sensor_setting_backup();
							 iq_update_state = 1;
						 }
					     WriteReg32(ISP_BASE_ADDR+0x8000, 0x8a7f0100);
					     fb_init(3);
					}else if(sccb_v == 6){
						debug_printf("AWB enable, Shading enable!!!!!\n");
						WriteReg32(ISP_BASE_ADDR+0x8000, 0x0050000|BIT27|BIT25|BIT17); // awb enable, lens shading enable
						fb_init(3);
					}else if(sccb_v == 7){
						debug_printf("CIP Only !!!!!!!!!\n");
						WriteReg32(ISP_BASE_ADDR+0x8000, 0x0050000);	// CIP Only
						fb_init(3);
					}
					break;
				case 0x0050:
					if(sccb_v == 0){
						get_user_conf_addr_size();
					}else if(sccb_v == 1){
						save_user_conf();
					}else if(sccb_v == 2){
						get_cali_addr_size();
					}
					break;
#ifdef CONFIG_AUDIO_EN
				case 0x0052:
					mic_adj_vol(sccb_v);
					break;
				case 0x0053:
					spk_adj_vol(sccb_v);
					break;
#endif
				case 0x0054: 
					board_ioctl(BIOC_SET_BLUE_LED, sccb_v, NULL);
					break;
				case 0x0055:
					board_ioctl(BIOC_SET_AMBER_LED, sccb_v, NULL);
					break;
				case 0x0057:
					board_ioctl(BIOC_SAVE_LUM, sccb_v, NULL);
					break;
				default:
					break;
			}
		}
	}else{
		//real sccb
		libisp_sccbwr(0, sccb_id, sccb_addr, sccb_v);
	}
}


int app_reg_rd(u8 sccb_id, u16 sccb_addr)
{
	int sccb_v=0;

	if(sccb_id < 0x10){
		if((sccb_id & ~1) == 0x08){
			u32 laddr = libisp_get_laddr();
			u32 raddr = libisp_get_raddr();
			if(sccb_addr == 0x0000){
				sccb_v = (laddr & 0xff000000) >> 24;
			}else if(sccb_addr == 0x0001){
				sccb_v = (laddr & 0x00ff0000) >> 16;
			}else if(sccb_addr == 0x0002){
				sccb_v = (laddr & 0x0000ff00) >> 8;
			}else if(sccb_addr == 0x0003){
				sccb_v = (laddr & 0x000000ff) >> 0;
			}else if(sccb_addr == 0x0004){
				isp_get_lum();
 			}else if(sccb_addr == 0x0005){
				sccb_v = (raddr & 0xff000000) >> 24;
			}else if(sccb_addr == 0x0006){
				sccb_v = (raddr & 0x00ff0000) >> 16;
			}else if(sccb_addr == 0x0007){
				sccb_v = (raddr & 0x0000ff00) >> 8;
			}else if(sccb_addr == 0x0008){
				sccb_v = (raddr & 0x000000ff) >> 0;
			}
		}else if((sccb_id & ~1) == 0x0a){
		}else if((sccb_id & ~1) == 0xc){		// Cali mode
			if(sccb_addr == 0x0020){
			}else if(sccb_addr == 0x0030){
			}else if(sccb_addr == 0x0031){
			}else if(sccb_addr == 0x0032){
			}else if(sccb_addr == 0x0033){
			}else if(sccb_addr == 0x0034){
			}else if(sccb_addr == 0x0035){
			}else if(sccb_addr == 0x0036){
			}else if(sccb_addr == 0x0037){
			}else if(sccb_addr == 0x0038){
			}else if(sccb_addr == 0x0039){
			}else if(sccb_addr == 0x003a){
			}else if(sccb_addr == 0x003b){
			}else if(sccb_addr == 0x003c){
				sccb_v = (u8) cali_get_awb_gain();
			}else if(sccb_addr == 0x0040){
				sccb_v = board_ioctl(BIOC_SET_UPDATE_MCU, 1, NULL);
			}else if(sccb_addr == 0x0050){
				mpu_exit();
				sccb_v = ddr_test();
#ifdef CONFIG_AUDIO_EN
			}else if(sccb_addr == 0x0051){
				sccb_v = mic_stop();
			}else if(sccb_addr == 0x0052){
				mpu_exit();
				sccb_v = mic_start();
			}else if(sccb_addr == 0x0053){ 
				mpu_exit();
				sccb_v = spk_test();
#endif
			}else if(sccb_addr == 0xf001){
				sccb_v = erase_cali_area();
			} else if (sccb_addr == 0x0054) {
				if (flash_test() != 0)
					sccb_v = -1;
				else
					sccb_v = 0;
			}
		}
	}else{
		// real sccb
		sccb_id &= ~(1);
		{
			sccb_v = libisp_sccbrd(0, sccb_id, sccb_addr);
		}
	}

	debug_printf("reg_rd: %x:%x:%x\n", sccb_id, sccb_addr, sccb_v);

	return sccb_v;
}
