#ifndef _PRJ_CFG_H_
#define _PRJ_CFG_H_

#define DEVTYPE_NAND 1
#define DEVTYPE_EEPROM 2
#define DEVTYPE_SFLASH 3

#define MAX_SFLASH_FW (16)  
#define MAX_CALI_DATA (64)

// sf_cali.h
#define CALI_MAGIC_NUM  (0x43414c49)
#define CALI_MAX_FW     (64)

typedef enum{
	CALI_ERR_PARTITION = -1,
	CALI_ERR_CALIADDR = -2,
	CALI_ERR_MAGIC = -3,
	CALI_ERR_INVALID = -4,
}CALI_ERR;

struct cali_id2table
{
	u32 addr;
	u32 len;
	u32 maxlen;
	u32 valid;  // 1 : valid, 0: invalid
};

typedef struct cali_header
{
	// from 0x00
	u32 magicnumber;
	u32 version;
	u32 total_len;
	u32 reserved[5];

	// from 0x20
	struct cali_id2table id2table[CALI_MAX_FW];
}t_cali_header;

typedef struct s_sf_partition
{
	unsigned int magic_number;
	unsigned int sflash_setting;
	unsigned int bootloader_offset;
	unsigned int bootloader_ram;
	unsigned int bootloader_length;
	unsigned int system_config;
	unsigned int bootloader2_offset;
	unsigned int bootloader2_length;
	unsigned int fw_offset;
	unsigned int fw_length;
	unsigned int fw2_offset;
	unsigned int fw2_length;
	unsigned int para_S_offset; //For OV788 system config
	unsigned int para_S_length;
	unsigned int para_F_offset; //For DK framework config
	unsigned int para_F_length;
	unsigned int calib_offset;
	unsigned int calib_length;
	unsigned int other_offset;
	unsigned int other_length;
}t_sf_partition;


struct sflash_id2table
{
	u32 addr;
	u32 len;
};

typedef struct sflash_header
{
	u32 magicnumber;
	u32 sflash_setting;
	u32 bl_offset;
	u32 bl_ram;
	u32 bl_len;
	u32 sys_config;
	u32 reserved[2];
	
	//from 0x20
	u32 totalsize;
	u32 version;
	u32 fw_num;
	u32 max_support;

	//from 0x30
	struct sflash_id2table id2table[MAX_SFLASH_FW];

}t_sflash_header;

typedef struct s_sflashcfg
{
	t_libsif_cfg libsif_cfg;
	t_sf_partition sh;
	int notuseid2;
	u32 totalsize;
	u32 needbl; //status for need burn bootloader
	u8 sifdiv;
	u32 inited;
}t_sflashcfg;


typedef struct s_eepromcfg
{
	u32 totalsize;
//	struct eeprom_header eh;
	u32 needbl; //status for need burn bootloader
	u32 delay;
	t_libsccb_cfg libsccb_cfg;
}t_eepromcfg;

typedef struct s_nandcfg
{
#if 0
	t_nand_config tnc;
	u32 fssize;
	u32 page_size_n;
	u32 block_size_n;
	u32 nand_start;
//	struct nand_header *nh;
//	unsigned int nh_buf[(sizeof(struct nand_header) + 16)/4];
	t_nand_partition nh;
	t_badblock bbb;
	u32 bb[512/4];
	t_sparecache sc;
	u32 sc_buf[256];
	//for _res
#define RES_MAX_GROUP 10
#define MAX_ITEM_INGROUP 1000
	unsigned int groupoffset[RES_MAX_GROUP+2];
	unsigned int itemoffset[MAX_ITEM_INGROUP*2+2];
#endif

}t_nandcfg;

typedef struct s_cali_item
{
	u32 cnt;
	int lens_dtbl_id; 
	int lens_atbl_id;
	int lens_ctbl_id;
	int white_balance_id;
	// R-Channel
	int lens_dtbl_id_r;
	int lens_atbl_id_r;
	int lens_ctbl_id_r;
	int white_balance_id_1;
	u32 len[MAX_CALI_DATA];
}t_cali_item;

typedef struct s_calicfg
{
	u16 manual_gain;
	u16 manual_exp;
	u32 cal_area_addr;
	u32 cal_area_size;
	u32 cal_hdr_len;

	t_cali_item	cali_item;
	t_cali_header cali_hdr;
}t_calicfg;

typedef struct s_pincfg
{
	int pin_bsl_rst;
	int bsl_rst_level;
	int pin_bsl_jtag;
	int sn_reset;
}t_pincfg;

typedef struct s_agcfg
{
	int en;
	int max;
	int target;
	int nf;
}t_agcfg;

typedef struct s_prj_cfg
{
	unsigned char hasnand;
	unsigned char haseeprom;
	unsigned char hassflash;
	unsigned char bootfast;

	unsigned char isitem_mcufile;
	int fileid_mcu;

	t_sflashcfg sflashcfg;
	t_eepromcfg eepromcfg;
	t_nandcfg nandcfg;

	t_calicfg calicfg;

	t_pincfg pincfg;

	t_agcfg adecagc;
	t_agcfg aencagc;
}t_prj_cfg;

extern t_prj_cfg prj_cfg;
#define NANDCFG prj_cfg.nandcfg
#define EEPROMCFG prj_cfg.eepromcfg
#define SFLASHCFG prj_cfg.sflashcfg
#define CALIITEM prj_cfg.calicfg.cali_item

int parse_prj_cfg(u32 len, u32 base);
void re_detect_single_bin(void);
#endif
