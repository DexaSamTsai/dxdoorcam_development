#ifndef __CALI_H__
#define __CALI_H__


//#define CHECK_CALI_DATA
#define CALI_CFG		prj_cfg.calicfg.cali_hdr
#define CALI_ADDR		prj_cfg.calicfg.cal_area_addr
#define CALI_SIZE       prj_cfg.calicfg.cal_area_size
#define CALI_SCCB		prj_cfg.eepromcfg.libsccb_cfg

typedef enum{
	CALI_DEV_SIF = 1,
	CALI_DEV_EEPROM,
	CALI_DEV_NAND,
} CALI_DEV_TYPE;


//#define USE_SRAM	//(256k)	test

#define STATE_IDLE      0x1234
#define STATE_DONE      0x5678
#define STATE_EXIT      0xaa55
#define STATE_ERR       0xffff
#ifdef USE_SRAM
#define BURN_STATE      0x90060000
#define BURN_LEN        0x90060004
#define BURN_ID2        0x90060008
#define BURN_ADDR       0x9006000c
#define BURN_BUFF       0x90060010
#else
// USE DDR
#define BURN_STATE      0x901c2000
#define BURN_LEN        0x901c2004
#define BURN_ID2        0x901c2008
#define BURN_ADDR       0x901c200c
#define BURN_BUFF       0x901c2010
#endif

extern unsigned short af_val[7];
extern u32 cali_awb[3];// DAY, A, CWF

extern void burn_cali_setting(void);
extern void get_af_data(void);
extern void cali_color_update(void);
extern void cali_lens_dtbl_update(void);
extern void cali_lens_atbl_update(void);
extern void cali_lens_ctbl_update(void);
extern void sensor_setting_backup(void);
extern void sensor_color_default(void);
extern void sensor_lens_default(void);
extern void sensor_lens_atbl_default(void);
extern void sensor_lens_ctbl_default(void);
extern void cali_erase_all(void);
extern void cali_aec_manual_enable(void);
extern void cali_aec_manual_disable(void);
extern int cali_check_awb(void);
extern void cali_aec_manual_gain(u16 gain);
extern void cali_aec_manual_exp(u16 exp);
extern void cali_set_daymode(void);
extern void cali_set_nightmode(void);
extern void cali_revert_lens(int mode);
extern s32 cali_devs_load(void);
extern void cali_awb_mode(int mode);
extern void get_sn_from_module(void);
extern void get_cali_data(int id);
extern void erase_cali_data(int id);
extern void check_cali_data(int id);
extern void get_user_conf_addr_size(void);
extern void get_cali_addr_size(void);
extern void save_user_conf(void); 

extern int cali_get_awb_gain(void);
extern u8 erase_cali_area(void);

extern int isp_get_lum(void);
#endif
