#include "calfw_includes.h"
#include "libCommon.h"

static void board_init_pre(void)
{
	debug_printf("\n");
	debug_printf("board_init_pre!\n");
}

static void board_init_post(void)
{
	debug_printf("\n");
	debug_printf("board_init_post!\n");
}
static u8 c2i(u8 p)
{
	if((p <='F')&&(p >='A'))
	{
		return (p-'A'+10);
	}
	if((p <='f')&&(p >='a'))
	{
		return (p-'a'+10);
	}
	if((p <='9')&&(p >='0'))
	{
		return (p-'0');
	}
	return 0;
}

#define i2c(x) (((x)>9)?((x)-10+'a'):((x)+'0'))
static void i2s(char *pw_string,u8 *pw )
{
	int i=0;
	if((pw_string !=NULL )&&(pw!=NULL))
	{
		for(i=0;i<32;i++)
		{
			u8 tmp = *(pw+i);
			*(pw_string+i*2) = i2c(tmp/16);
			*(pw_string+i*2+1) = i2c(tmp%16);
		}
	}
}
void setpw(u8 *pw)
{
	char *bsl_pw;
	int i=0;
	bsl_pw = getenv("bsl_password");
	if(bsl_pw == NULL){
		debug_printf("bsl_password not set in para.txt,using default value,all 0xff\n");
		for(i=0;i<32;i++)
		{
			*(pw+i) = 0xff;
		}
	}
	else{
		for(i=0;i<32;i++)
		{
			*(pw+i) = (c2i(*(bsl_pw+2*i))<<4) | (c2i(*(bsl_pw+2*i+1)));
	//		debug_printf("0x%x ",*(pw+i));
		}
	}
}

void savepw(void)
{
	char pw[78];
	int i=0;
	char *p = "bsl_password=";
	i2s(pw+13,g_msp430_bsl_cfg.password);
	for(i=0;i<13;i++)
	{
		pw[i] = *(p+i);
	}
	pw[77] = '\0';
#if 0
	debug_printf("savepw:\n");
	for(i=0;i<78;i++)
		debug_printf("%c ",pw[i]);
#endif
	loadenv();
	int ret = putenv(pw);
	if(ret !=0)
	{
		debug_printf("putenv() error\n");
                return;
	}
	saveenv();

}



#define MSP430_FWBUF_SIZE		(50*1024)
static u8 _msp430_fwbuf[MSP430_FWBUF_SIZE];

static int board_update_mcu(u32 from)
{
	int re_flag =0;
	syslog(LOG_INFO, "Update msp430 FW ...\n");
	memset(&g_msp430_bsl_cfg, 0, sizeof(g_msp430_bsl_cfg));

	//FIXME: only for test
	g_msp430_bsl_cfg.pin_bsl_rst = PIN_BSL_RST;
	g_msp430_bsl_cfg.bsl_rst_level = 1;
	g_msp430_bsl_cfg.pin_bsl_jtag = PIN_BSL_TEST;
	g_msp430_bsl_cfg.force_masserase = 0;

	int ret = Common_GetMCUFW(_msp430_fwbuf, from);
	if(ret <= 0){
		syslog(LOG_ERR, "[ERROR]: Get msp430 FW Fail.\n");
		return -1;
	}

	setpw(g_msp430_bsl_cfg.password);
retry:
	ret = msp430_bsl_burn_full(_msp430_fwbuf, ret);
	if( ret == 0){
		savepw();
		g_msp430_bsl_cfg.force_masserase = 0;
		syslog(LOG_INFO, "Burn msp430 FW Done!\n");
	}else{
		if(re_flag ==0)
		{
			syslog(LOG_ERR, "Burn msp430 FW Failed,will try again with mass erase and default password!\n");
			g_msp430_bsl_cfg.force_masserase = 1;
			re_flag = 1;
			goto retry;
		}
		else
		{
			syslog(LOG_ERR, "Burn msp430 FW Fail!\n");
			return -1;
		}
	}
	return 0;
}

#ifdef CALITOOL
static int board_set_loop(void)
{
	burn_cali_setting();
	return 0;
}
#endif
int board_evb_hw_init(void)
{
	libgpio_config(PIN_GPIO_05, PIN_DIR_OUTPUT);
	libgpio_write(PIN_GPIO_05, 1);
	return 0;
}
void board_boot(void){

	debug_printf("board_9756_boot.\n");
	debug_printf("flash status reg1 = %d, reg2 = %d\n", libsfc_flash_read_status_reg1(), libsfc_flash_read_status_reg2());
	return;
}

extern int ref_sfinit_fast_disable;
s32 board_ioctl(u32 cmd, u32 arg, void * ret_arg)
{
	switch(cmd){
		case BIOC_SET_INIT_PRE:
			board_init_pre();
			return 0;
		case BIOC_SET_INIT_POST:
			board_init_post();
			return 0;
		case BIOC_SET_UPDATE_MCU:
			//'arg': 0, from uart; 1, from sflash
			return board_update_mcu(arg);
		case BIOC_SET_BOOT:
			board_boot();
			return 0;
		case BIOC_HW_EARLY_INIT:
			ref_sfinit_fast_disable = 1;
			return DxDoorCam_hw_early_init();
		case BIOC_HW_INIT:
			return DxDoorCam_hw_init();
		case BIOC_AU_PA_ON:
		if (arg) {
			debug_printf("arg = %d, enable the speaker!\n", arg); //Enable speaker PA
			libgpio_config(PA_ON_GPIO, PIN_DIR_OUTPUT);
			libgpio_write(PA_ON_GPIO, 1);
		} else {
			debug_printf("arg = %d, disable the speaker!\n", arg);
			libgpio_config(PA_ON_GPIO, PIN_DIR_OUTPUT);
			libgpio_write(PA_ON_GPIO, 0);
		}
				return 0;
#ifdef CALITOOL
		case BIOC_UDEV_SET_LOOP:
			return board_set_loop();
#endif
		default:
			return -EINVAL;
	}
}
