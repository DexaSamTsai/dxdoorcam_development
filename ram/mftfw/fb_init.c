#include "calfw_includes.h"

u32 input_width  = 1280;//default 1280
u32 input_height = 720;//default 720

void set_format(int fmt);
void fb_init(int fmt)
{
	static int _last_fmt=-1;
	if(fmt != _last_fmt){
		_last_fmt = fmt;
		set_format(fmt);
	}

	ENABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
}

