/***************************
 * audio.h
 * Created on: Jul 29, 2017
 * Author: xuefeng.zhang
 ***************************/
#include "audio.h"
#include "../cali.h"
static int sptr = 0;
// decode
static t_adec_inpara adec_para;
static volatile t_adec_inpara *p_adec;
static t_codec_cpara codec_para;


int read_audio_stream(char *dest, int len){
	int i;
	char * p = (char *) BURN_BUFF;
	for (i = 0; i < len; i++) {
		dest[i] = p[sptr++];
		if (sptr >= ReadReg32(BURN_LEN)) {
			return 0;
		}
	}
	return len;

}
void audio_play(int rate, int format, int vol){
	FRAME *afrm;
	int ret;
	p_adec = (t_adec_inpara*) (&adec_para);
	memset(p_adec, 0, sizeof(t_adec_inpara));
	p_adec->fmt = format;
	p_adec->sr = rate;
	p_adec->ch = 1;
	p_adec->dvol = 0;
#ifdef CONFIG_AUDIO_CODEC_EN_interncodec
	p_adec->aimode = AI_PLY_MASTER;
#else
	p_adec->aimode = AI_CDC_MASTER;

#endif
	syslog(LOG_ALERT, "play audio format is %d\n", p_adec->fmt);
	syslog(LOG_ALERT, "play audio rate is %d\n", p_adec->sr);

	memset(&codec_para, 0, sizeof(t_codec_cpara));

	codec_para.sr = rate;
	codec_para.ch = p_adec->ch;
	codec_para.master = p_adec->aimode ? 0 : 1;
	codec_para.dac_dvol = 0;
	codec_para.adc_dvol = 0;
	codec_para.dir = AI_DIR_PLAY;

	libacodec_init(&codec_para);
	mpu_start();
	debug_printf("BA22 started\n");
	//CONFIG_AUDIO_DEC_EN
	if (libadec_init(p_adec) < 0) {
		syslog(LOG_ERR, "libadec_init failed\n");
		return;
	} else {
		syslog(LOG_DEBUG, "libadec_init ok\n");
	}

	libadec_start();
	while (1) {
		afrm = libadec_get_frame(0);
		if (afrm) {
			debug_printf("p");
			ret = read_audio_stream((u8*) afrm->addr, afrm->size);
			if (ret < afrm->size) {
				debug_printf("read file end, play end\n");
				break;
			}
			libadec_add_frame(afrm);
		}
	}
	ertos_timedelay(100);
	libadec_stop();
	libadec_exit();
	libacodec_pwrdown();
	mpu_stop();
	sptr = 0;
}
