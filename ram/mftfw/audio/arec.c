/***************************
 * audio.h
 * Created on: Jul 29, 2017
 * Author: xuefeng.zhang
 ***************************/


#include "audio.h"

/**************
 * encode
 */
static t_arec_inpara arec_para;
static volatile t_arec_inpara *p_arec;
//static char p_audio[10240];
char *p_audio = NULL;
void audio_rec_start(int rate, int format, int vol){
  
	p_audio = malloc(REC_DATA_LEN);
	if(p_audio == NULL){
		debug_printf("audio malloc failed!\n");
		return;
	}else{
		debug_printf("malloc successfully!\n");
	}
	p_arec = (t_arec_inpara*) (&arec_para);
	memset((void*) p_arec, 0, sizeof(t_arec_inpara));
	FRAME *frm = NULL;

	mpu_start();
	debug_printf("BA22 started\n");
	ertos_timedelay(10);

	p_arec->fmt = format; //format
	p_arec->sr = rate; // rate
	p_arec->ch = 1; // single channel
	p_arec->dvol = 0;
#ifdef CONFIG_AUDIO_CODEC_EN_interncodec
	p_arec->aimode = AI_REC_MASTER;
#else
	p_arec->aimode = AI_REC_MASTER;
#endif
	p_arec->drop_samples = 0;
	p_arec->denoise_en = 1;
	p_arec->bDMIC = 0;
	t_codec_cpara codec_para;
	memset(&codec_para, 0, sizeof(t_codec_cpara));
	codec_para.sr = rate;
	codec_para.ch = 1;
	codec_para.master = p_arec->aimode ? 0 : 1;
	codec_para.dac_dvol = 0;
	codec_para.adc_dvol = vol;
	codec_para.dir = AI_DIR_RECORD; //from codec -> 798

	if (!p_arec->bDMIC) {
		libacodec_init(&codec_para);
	}
	ertos_timedelay(10);
	if (libaenc_init(p_arec) < 0) {
		debug_printf("libaenc_init failed!\n");
		return;
	}
	libaenc_start();
	return;
}

/*******
 * get the audio frame & save the data
 */
void arec_get_frame(u32 * addr, u32 * len, u32 * addr1, u32 * len1)
{
	volatile FRAME *afrm;
	int offset = 0;
	while(1){
		afrm = libaenc_get_frame(0);
		if(afrm){
			if ((offset + afrm->size + afrm->size1) > REC_DATA_LEN) {
				debug_printf("space = %d, no more space to store!\n",offset);
				break;
			}
			memcpy(&p_audio[offset],(u8 *)afrm->addr,afrm->size);
			offset += afrm->size;
			if(afrm->size1){
				memcpy(&p_audio[offset],(u8 *)afrm->addr1,afrm->size1);
				offset += afrm->size1;
			}
			uart_putc('w');
			libaenc_remove_frame((FRAME *)afrm);
			break;
		}
	}

	*addr = (u32)p_audio;
	*len = offset;
}

void audio_rec_stop(void){
	libaenc_stop();
	libaenc_exit();
	libacodec_pwrdown();
	free(p_audio);
	p_audio = NULL;
	mpu_stop();
}





