/***************************
 * audio.h
 * Created on: Jul 29, 2017
 * Author: xuefeng.zhang
 ***************************/

#ifndef RAM_MFTFW_AUDIO_AUDIO_H_
#define RAM_MFTFW_AUDIO_AUDIO_H_

#include "includes.h"

#define REC_DATA_LEN (1024*100)
/******
 * brief: recoder the audio
 * parameter:
 *           @rate:audio rate
 *           @format:PCM,ADPCM,AAC
 *           @vol:volume
 */
void audio_rec_start(int rate, int format, int vol);
/***********
 * brief: save the audio data
 */
void arec_get_frame(u32 * addr, u32 * len, u32 * addr1, u32 * len1);
/*******
 * stop record
 */
void audio_rec_stop(void);

/********
 *brief: play the audio
 *parameter:
 *	      @rate:audio rate
 *	      @format:PCM
 *	      @vol:volume
 */
void audio_play(int rate, int format, int vol);
int read_audio_stream(char *dest, int len);
#endif /* RAM_MFTFW_AUDIO_AUDIO_H_ */
