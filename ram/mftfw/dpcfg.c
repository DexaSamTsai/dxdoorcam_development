/****************************************************
 * Workfile:      dpcfg.c
 * Modify on:     Jan 5, 2017
 * Author:        xuefeng.zhang
 ****************************************************/
#include "calfw_includes.h"
int g_sensor_has = -1;
static int mpu_inited = 0;
#define MAX_VS  (4)
int mpu_init(void)
{
	if(mpu_inited == 1){
		syslog(LOG_ALERT,"mpu has been inited !\n");
		return 0;
	}
	mpu_start();
	syslog(LOG_ALERT, "BA22 started\n");
	int ret = libdatapath_init(0);
	syslog(LOG_ALERT, "datapath init: %d\n", ret);
	if(ret!=0){
		syslog(LOG_ALERT, "datapath init failed\n");
		return ret;
	}
	ret |= libdatapath_start();
	syslog(LOG_ALERT, "datapath start: %d\n", ret);
	if (ret!=0) {
		syslog(LOG_ALERT, "datapath start failed\n");
		return ret;
	}
	ret |= libisp_init();
	syslog(LOG_INFO, "isp_init: %d\n", ret);
	if (ret) {
		syslog(LOG_ALERT, "isp_init failed\n");
		return ret;
	}
	u32 i = 0;
	int ret_val;
	for(i = 0; i < MAX_VS;i++){
		ret_val = libvs_get_type(i);
		debug_printf("stream type is %d\n", ret_val);
		if(ret_val >= 0){
			ret |= libvs_init(i);
			syslog(LOG_ALERT, "vs init %d: %d\n", i, ret);
			if (ret) {
				syslog(LOG_ALERT, "libvs_init [%d] failed\n",i);
				return ret;
			}
			ret |= libvs_start(i);
			syslog(LOG_ALERT, "vs start %d: %d\n", i, ret);
			if (ret) {
				syslog(LOG_ALERT, "libvs_start [%d] failed\n",i);
				return ret;
			}
		}
	}
	g_sensor_has = 1;
	if (!ret)
		mpu_inited = 1;
	return 0;
}

int mpu_exit(void)
{

	if(!mpu_inited){
		syslog(LOG_ERR, "mpu was not inited or exited!\n");
		return 0;
	}
	int ret;
	ret = libdatapath_stop();

    debug_printf("datapaht stop = %d\n",ret);
    if(ret){
    	syslog(LOG_ERR, "datapath stop failed\n");
    	return ret;
    }

    u32 i;
    int ret_val;
    for(i = 0; i < MAX_VS; i++){
    	ret_val = libvs_get_type(i);
    	debug_printf("i = %d, ret_val = %d\n", i, ret_val);
    	if(ret_val >= 0){
    		debug_printf("...............\n");
    		while (libvs_stop(i) == 1);
    		ret = libvs_exit(i);
    		debug_printf("libvs_exit(%d) = %d\n",i,ret);
    		if(ret){
    			syslog(LOG_ERR, "libvs stop vs[%d] failed\n",i);
    			return ret;
    		}
    	}
    }
	ret |= libisp_exit();
	debug_printf("libisp_exit = %d\n",ret);
	if(ret){
		syslog(LOG_ERR, "libisp_exit failed\n");
		return ret;
	}
	ret |= libdatapath_exit();
	debug_printf("libdatapath_exit = %d\n",ret);
	if(ret){
		syslog(LOG_ERR, "libdatapath_exit failed\n");
		return ret;
	}
	mpu_stop();
	if(!ret)
		mpu_inited = 0;
	return ret;
}
