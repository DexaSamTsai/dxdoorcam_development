#include "calfw_includes.h"


#ifdef CONFIG_MFT_TOOL_VERSION_EN
#define _g_verion  __attribute__ ((__section__ (".version")))
_g_verion char version_str[20] = CONFIG_MFT_SERIAL_NUMBER;
#endif

#ifdef CALITOOL
extern int interrupt_handler_init(void);
static t_ertos_taskhandler taskhandler_func;
static void task_func(void * arg)
{
	while(1){
		board_ioctl(BIOC_FUNC_LOOP, 0, NULL);
	}
	ertos_task_delete(NULL);
}
#endif

#ifdef CALFW_UART_CMD_TEST
static void clear_other_area(void)
{
	
	u32 _other_offset;
	int other_len = partition_get_otherarealen_sf(&_other_offset);
	if(other_len < 0){
		return -1;
	}
	debug_printf("other area offset:%x len:%x\n", _other_offset, other_len);
    u32 cur;
	u32 f_addr = _other_offset;
	int i;
	for(i=0; i*p_sflash->sector_size < other_len; i++){
		debug_printf("ERASE:[%x / %x]\n", i*p_sflash->sector_size, other_len);
		if(sf_erase(f_addr, ERASE_SECTOR) != 0){
			syslog(LOG_ERR, "[ERROR]: Erase %x fail\n", f_addr); 
		}
        f_addr += p_sflash->sector_size;
	}
}
static void sf_burn(u32 addr, int len)
{
	int i;
	u32 lasttick = ticks;

#define SHOW_PROGRESS(str, cent, all) \
	if(ticks - lasttick > 100){ \
		debug_printf("%s [%d / %d]\n", str, cent, all); \
		lasttick = ticks; \
	}


	for(i=0; i*p_sflash->sector_size < len; i++){
		SHOW_PROGRESS("ERASE", i, len/p_sflash->sector_size);
		if(sf_erase(i*p_sflash->sector_size, ERASE_SECTOR) != 0){
			syslog(LOG_ERR, "[ERROR]: Erase %x fail\n", i*p_sflash->sector_size);
		}
	}
	for(i=0; i*p_sflash->page_size < len; i++){
		SHOW_PROGRESS("PROG", i, len/p_sflash->page_size);
		if(sf_program(addr + i*p_sflash->page_size, p_sflash->page_size, i*p_sflash->page_size) < 0){
			//syslog(LOG_ERR, "[ERROR]: Program %x fail\n", i*p_sflash->page_size);
		}
	}
}

static void sf_boot(int id2)
{
#define TESTLEN 32
	u32 buf[TESTLEN];
	int i;

	memset(buf, 0, sizeof(buf));
	sf_read(buf, TESTLEN*4, 0);
	debug_printf("0\n");
	for(i=0; i<TESTLEN; i++){
		syslog(LOG_INFO, "%d:%x\n", i, buf[i]);
	}

	partition_boot_sf(id2);
}

static void burn_sf_fromsd(void)
{
#define EFS_BUFFER  (MEMBASE_DDR+0xa00000)
#define EFS_BUFFER_SIZE   (1024*1024)
	debug_printf("  efs init\n"); 
	efs_init(EFS_HW_SCIF, 0, EFS_BUFFER, EFS_BUFFER_SIZE);
	debug_printf("  SD card capacity: %d sectors\n", sh_efs.sectors);
	debug_printf("  Filesystem sectors per Cluster: %d\n", sh_efs.bpb.sectors_per_cluster);
	debug_printf("  Filesystem sectors count: %d, cluster count:%d\n", sh_efs.sectors, sh_efs.sectors/sh_efs.bpb.sectors_per_cluster);
	debug_printf("  Filesystem Free Clusters: %d\n", sh_efs.clusters_free);
	debug_printf("  Filesystem first Free Cluster: %d\n", sh_efs.next_free_cluster);
	FILE *fp;
	fp = fopen("/SINGLE.bin", "r");
	fseek(fp, 0, SEEK_END);
	int len = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	if(fp == NULL){
		debug_printf("/SINGLE.bin not exist\n");
	}else{
		debug_printf("open SINGLE.bin, len:%d\n", len);	
	}
	u32 ret = fread(MEMBASE_DDR, 1, len, fp);
	debug_printf("read :%d\n", ret);	
	int i;
	char *buf = MEMBASE_DDR;
	for(i = 0;i<16;i++)
	{
	debug_printf("buf :%x\n", buf[i]);	
	}
	sf_burn(MEMBASE_DDR, ret);
}
#endif

void * thread_main(void * arg)
{
#ifdef FOR_FPGA
	lowlevel_config(115200, 0);
#endif

	board_ioctl(BIOC_SET_INIT_PRE, 0, NULL);
	board_ioctl(BIOC_SET_INIT_POST, 0, NULL);
	board_ioctl(BIOC_SET_BOOT, 0, NULL);
	//board_ioctl(BIOC_SET_SHADING_BACKUP, 0, NULL);

#ifdef CALITOOL
	debug_printf("Start CalFW Firmware ...\n");

	//taskhandler_func = ertos_task_create((void *)task_func, "task.func", 512, NULL, 3);

	//mpu_init();
	//board_ioctl(BIOC_SET_SHADING_BACKUP, 0, NULL);
	libusb_init();
	libusb_start();
	while(1);

#endif

	return NULL;
}
