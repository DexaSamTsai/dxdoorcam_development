#include "calfw_includes.h"
#include "prj_cfg.h"
#include "cali.h"
t_sflash g_sf;

static u8 cali_buf[4096];

/**
 * @brief initial calibration device data index
 */
static void cali_dev_init_index(void)
{
	u32 i;
	CALI_CFG.magicnumber = CALI_MAGIC_NUM;
	CALI_CFG.id2table[0].addr = CALI_ADDR + prj_cfg.calicfg.cal_hdr_len;
	CALI_CFG.id2table[0].maxlen = CALIITEM.len[0];
	for(i=1; i<CALI_MAX_FW; i++){
		CALI_CFG.id2table[i].addr = CALI_CFG.id2table[i-1].addr + CALI_CFG.id2table[i-1].maxlen;
		CALI_CFG.id2table[i].maxlen = CALIITEM.len[i];
	}
	CALI_CFG.total_len = 0;
	for(i=0; i<CALI_MAX_FW; i++){
		CALI_CFG.total_len += CALI_CFG.id2table[i].maxlen;
	}
}

/**
 * @brief check the device calibration index
 */
static s8 cali_dev_check_index(void)
{
	if(CALI_CFG.magicnumber != CALI_MAGIC_NUM){
		return -1;
	}
	return 0;
}
/**
 * @brief update calibration index to device
 */
static void cali_dev_update_index(void)
{
	int i;
	if(prj_cfg.hassflash){
		sf_erase(CALI_ADDR, ERASE_SECTOR);
		for(i = 0; i < sizeof(CALI_CFG); i += g_sf.page_size){
			sf_program((u8 *)(((u32)(&CALI_CFG)) + i), g_sf.page_size, CALI_ADDR + i);
		}
	}
#if 0
	else if(prj_cfg.haseeprom){
		for(i = 0; i< sizeof(CALI_CFG); i++){
			int ret = sccb_wr(&CALI_SCCB, i + CALI_ADDR, ((u8 *)(&CALI_CFG))[i]);
			if(ret != 0){
				debug_printf("ret1:%d\n", ret);
			}
			{volatile int t;for(t=0;t<prj_cfg.eepromcfg.delay;t++);} //from spec, need 5ms
		}
	}else if(prj_cfg.hasnand){
		nand_erase_block(CALI_ADDR / g_nc->page_size);
		nand_program_page(CALI_ADDR%g_nc->page_size, CALI_ADDR/g_nc->page_size, ((u32)(&CALI_CFG)), sizeof(CALI_CFG));
	}else{
		debug_printf("ERR:can not update index\n");
	}
#endif
}

/**
 * @brief load calibration index from device
 */
static void cali_dev_load_index(void)
{
	if(prj_cfg.hassflash){
		sf_read((u8*)&CALI_CFG, sizeof(CALI_CFG), CALI_ADDR);
	}
#if 0
	else if(prj_cfg.haseeprom){
		sccb_seqread(&CALI_SCCB, CALI_ADDR, sizeof(CALI_CFG), (u8*)&CALI_CFG);
	}else if(prj_cfg.hasnand){
		nand_read_page(0,  CALI_ADDR / g_nc->page_size , (u32)&CALI_CFG, sizeof(CALI_CFG), 0);
	}else{
		debug_printf("ERR:can not load index\n");
	}
#endif
}

/**
 * @brief erase calibration data
 */
static void cali_dev_erase_data(int id)
{
	cali_dev_load_index();
	cali_dev_init_index();
	CALI_CFG.id2table[id].valid = 0x0;
	CALI_CFG.id2table[id].len = 0x0;
	cali_dev_update_index();
}

static int cali_dev_check_data(int id)
{
	debug_printf("id %d, valid %d\n", id, CALI_CFG.id2table[id].valid);
	return (CALI_CFG.id2table[id].valid);
}

/**
 * @brief read calibration data
 */
static void cali_dev_read_data(u8 *buf, u32 id)
{
	if(CALI_CFG.magicnumber != CALI_MAGIC_NUM){
		return;
	}
	if(prj_cfg.hassflash){
		sf_read((u8*)buf, CALI_CFG.id2table[id].len, CALI_CFG.id2table[id].addr);
	}
#if 0
	else if(prj_cfg.haseeprom){
		sccb_seqread(&CALI_SCCB, CALI_CFG.id2table[id].addr, CALI_CFG.id2table[id].len, buf);
	}else if(prj_cfg.hasnand){
		u8 page_buffer[2048];
		rom_nand_read_page(CALI_CFG.id2table[0].addr%g_nc->page_size,  CALI_CFG.id2table[0].addr / g_nc->page_size, page_buffer, sizeof(page_buffer), 0);
		memcpy(buf, page_buffer + (CALI_CFG.id2table[id].addr - CALI_CFG.id2table[0].addr), CALI_CFG.id2table[id].len);
	}
#endif
}

/**
 * @brief write calibration data
 */
static s32 cali_dev_write_data(u8 *buf, int len, u8 id2)
{
	int i;
	CALI_CFG.total_len = 0;
	for(i=0; i<CALI_MAX_FW; i++){
		CALI_CFG.total_len += CALI_CFG.id2table[i].maxlen;
	}
	u32 cali_offset = CALI_CFG.id2table[id2].addr - CALI_CFG.id2table[0].addr;
	u32 cali_start = CALI_CFG.id2table[0].addr;
	while(cali_offset > sizeof(cali_buf)){
		cali_offset -= sizeof(cali_buf);
		cali_start += sizeof(cali_buf);
	}
	if(prj_cfg.hassflash){
		sf_read((u8*)cali_buf, sizeof(cali_buf), cali_start);
	}
#if 0
	else if(prj_cfg.hasnand){
		nand_read_page(0, CALI_CFG.id2table[0].addr/g_nc->page_size, cali_buf, CALI_CFG.total_len, 0);
	}else{
	}
#endif

	if(len > CALI_CFG.id2table[id2].maxlen){
		debug_printf("Cali Invalid Length!!! %x, %x\n", len, CALI_CFG.id2table[id2].maxlen);
		return -50;
	}
	if(CALI_CFG.id2table[id2].addr < CALI_CFG.id2table[0].addr){
		debug_printf("Cali Invalid Address!!! %x, %x\n", CALI_CFG.id2table[id2].addr,  CALI_CFG.id2table[0].addr);
		return -51;
	}
	memcpy(cali_buf + cali_offset, buf, len);
	if(prj_cfg.hassflash){
		for(i=0; i<sizeof(cali_buf); i+=g_sf.page_size){
			if(((cali_start + i) & (g_sf.sector_size - 1)) == 0){
				sf_erase(cali_start + i, ERASE_SECTOR);
			}

			sf_program((u8 *)(cali_buf + i), g_sf.page_size, cali_start + i);

			// check sflash data
			static u8 checkbuf1[1024];
			u8 * checkbuf2 = (u8*)cali_buf + i;
			sf_read(checkbuf1, g_sf.page_size, cali_start + i);
			int j;
			for(j=0; j<g_sf.page_size; j++){
				if(checkbuf1[j] != checkbuf2[j]){
					debug_printf("check data error [0x%x] : 0x%x, 0x%x\n", cali_start + i + j, checkbuf1[j], checkbuf2[j]);
					return -52;
				}
			}
		}
	}
#if 0
	else if(prj_cfg.haseeprom){
		for(i = 0; i < len; i++){
			int ret = sccb_wr(&CALI_SCCB, CALI_CFG.id2table[id2].addr+i, buf[i]);
			if(ret != 0){
				debug_printf("ret1:%d\n", ret);
			}
			{volatile int t;for(t=0;t<prj_cfg.eepromcfg.delay;t++);} //from spec, need 5ms
		}
	}else if(prj_cfg.hasnand){
		for(i=0; i<CALI_CFG.total_len; i+=g_nc->page_size){
			if(((cali_start + i) & (g_nc->block_size - 1)) == 0){
				nand_erase_block((cali_start + i)/g_nc->page_size);
			}

			nand_program_page(0, (cali_start +i)/g_nc->page_size, (u8 *)cali_buf, CALI_CFG.total_len);
			// check nand data
			static u8 checkbuf1[1024];
			u8 * checkbuf2 = (u8*)cali_buf + i;
			nand_read_page(0,  cali_start / g_nc->page_size, checkbuf1, CALI_CFG.total_len, 0);
			int j;
			for(j=0; j<CALI_CFG.total_len; j++){
				if(checkbuf1[j] != checkbuf2[j]){
					debug_printf("check data error [0x%x] : 0x%x, 0x%x\n", cali_start + i + j, checkbuf1[j], checkbuf2[j]);
					return -52;
				}
			}
		}
	}
#endif
	CALI_CFG.id2table[id2].len = len;
	CALI_CFG.id2table[id2].valid = 1;
	cali_dev_update_index();
	return 0;
}

s32 cali_devs_load(void)
{
	int len = ReadReg32(BURN_LEN);
	return parse_prj_cfg(len, BURN_BUFF);
}

static int burn_cali_fw(u8 * buf, int len, int id2)
{
	if(CALI_ADDR == 0){
		debug_printf("Error : No calibration area\n");
		return -1;
	}
	cali_dev_load_index();
	if(cali_dev_check_index() != 0){
		memset(&CALI_CFG, 0, sizeof(CALI_CFG));
	}
	cali_dev_init_index();
	cali_dev_update_index();

	debug_printf("buf %x, len %x, id2 %x\n", buf, len, id2);
	return cali_dev_write_data(buf, len, id2);
}

extern int burn_singlebin(u8 * buf, int len, u32 addr);
extern int dump_singlebin(u8 * buf, int len, u32 addr);
extern int erase(int len, u32 addr);

void get_cali_addr_size(void)
{

	debug_printf("addr %x, size %x\n", CALI_ADDR, CALI_SIZE);
	WriteReg32(BURN_ADDR, CALI_ADDR);
	WriteReg32(BURN_LEN, CALI_SIZE);

	cali_dev_load_index();
	if(CALI_CFG.magicnumber == CALI_MAGIC_NUM){
		WriteReg32(BURN_ID2, 0);
	}else{
		WriteReg32(BURN_ID2, 1);
	}
	return;
}

void get_user_conf_addr_size(void)
{
	// [0] maigc
	// [1] length
	// [2] crc16 >> 16
	// [3] ENV length
	u32 header[4];
	t_sf_hdr sf_partition;

	//check header
	p_sflash->cmd_cur_rd.fast_read_en = 1;
	sf_read((u8*)&sf_partition, sizeof(t_sf_hdr), 0);
	if(sf_partition.magic_num != 0x4f565449 || sf_partition.user_config_addr == 0 || sf_partition.user_config_addr == 0xffffffff){
		debug_printf("no para area!!! %x, %x\n", sf_partition.magic_num, sf_partition.user_config_addr);
		goto fail;
	}

	sf_read((u8*)header, 16, sf_partition.user_config_addr);
	if(header[0] != 0x4f565450 || header[1] == 0 || header[1] == 0xffffffff){
		debug_printf("para header error!!! %x, %x\n", header[0], header[1]);
		goto fail;
	}

	if(crc16_ccitt(&header[0], 8) != (header[2] >> 16) ){
		debug_printf("para crc error\n");
		goto fail;
	}

	WriteReg32(BURN_ADDR, sf_partition.user_config_addr + p_sflash->sector_size);
	WriteReg32(BURN_LEN, header[1]);

	return;

fail:
	WriteReg32(BURN_ADDR, 0);
	WriteReg32(BURN_LEN, 0);
	return;
}

void save_user_conf(void)
{
	int len = ReadReg32(BURN_LEN);
	partition_conf_save_sf((u8 *)BURN_BUFF, len);
}

void burn_cali_setting(void)
{
	int ret=0;

	if(ReadReg32(BURN_STATE) == STATE_DONE){
		int len = ReadReg32(BURN_LEN);
		int id2 = ReadReg32(BURN_ID2);
		u8 * buf = (u8 *)BURN_BUFF;

		if(id2 == 0xffff0000){
			re_detect_single_bin();
			u32 addr = ReadReg32(BURN_ADDR);
			ret = burn_singlebin(buf, len, addr);
		}else if(id2 == 0xffff0001){
            re_detect_single_bin();
			u32 addr = ReadReg32(BURN_ADDR);
			ret = dump_singlebin(buf, len, addr);
		}else if(id2 == 0xffff0002){
             re_detect_single_bin();
			u32 addr = ReadReg32(BURN_ADDR);
			ret = erase(len, addr);
		}else{
			re_detect_single_bin();
			ret = burn_cali_fw(buf, len, id2);
		}
		if(ret){
			debug_printf("ret %d\n", ret);
			WriteReg32(BURN_STATE, STATE_ERR);
		}else{
			WriteReg32(BURN_STATE, STATE_EXIT);
		}
	}
}

void erase_cali_data(int id)
{
	memset(&CALI_CFG, 0x00, sizeof(CALI_CFG));
	cali_dev_load_index();
	if(CALI_CFG.magicnumber != CALI_MAGIC_NUM){
		return;
	}
	cali_dev_erase_data(id);
}

void check_cali_data(int id)
{
	u8 *buf= (u8 *)BURN_BUFF;
	memset(buf, 0, 64);

	memset(&CALI_CFG, 0x00, sizeof(CALI_CFG));
	cali_dev_load_index();
	if(CALI_CFG.magicnumber != CALI_MAGIC_NUM){
		return;
	}
	int valid = cali_dev_check_data(id);
	WriteReg32(BURN_BUFF, valid);
}

void get_cali_data(int id)
{
	u8 *buf= (u8 *)BURN_BUFF;
	memset(buf, 0, 64);

	memset(&CALI_CFG, 0x00, sizeof(CALI_CFG));
	cali_dev_load_index();
	if(CALI_CFG.magicnumber != CALI_MAGIC_NUM){
		return;
	}
	cali_dev_read_data(buf, id);
}

void cali_color_update(void)
{
	board_ioctl(BIOC_IQ_CALI, 0, NULL);
}

void cali_lens_dtbl_update(void)
{
	board_ioctl(BIOC_DOORCAM_CALI_SHADING, SHADING_D_TABLE, NULL);
}

void cali_lens_atbl_update(void)
{
	board_ioctl(BIOC_DOORCAM_CALI_SHADING, SHADING_A_TABLE, NULL);
}

void cali_lens_ctbl_update(void)
{
	board_ioctl(BIOC_DOORCAM_CALI_SHADING, SHADING_C_TABLE, NULL);
}

void sensor_setting_backup(void)
{
	board_ioctl(BIOC_SET_SHADING_BACKUP, 0, NULL);
}

void sensor_color_erase(void)
{
	cali_dev_erase_data(CALIITEM.white_balance_id);
}

void sensor_lens_erase(void)
{
	cali_dev_erase_data(CALIITEM.lens_dtbl_id);
}

void sensor_lens_atbl_erase(void)
{
	cali_dev_erase_data(CALIITEM.lens_atbl_id);
}

void sensor_lens_ctbl_erase(void)
{
	cali_dev_erase_data(CALIITEM.lens_ctbl_id);
}

void sensor_lens_default(void)
{
	board_ioctl(BIOC_SET_SHADING_DEFAULT, 2, NULL);
}

void sensor_lens_atbl_default(void)
{
	board_ioctl(BIOC_SET_SHADING_DEFAULT, 0, NULL);
}

void sensor_lens_ctbl_default(void)
{
	board_ioctl(BIOC_SET_SHADING_DEFAULT, 1, NULL);
}

void cali_erase_all(void)
{
	cali_dev_load_index();

	if(CALI_CFG.magicnumber == CALI_MAGIC_NUM){
		sensor_color_erase();
		sensor_lens_erase();
		sensor_lens_atbl_erase();
		sensor_lens_ctbl_erase();

		sensor_lens_default();
		sensor_lens_ctbl_default();
		sensor_lens_atbl_default();
		debug_printf("erase all iq data\n");
	}else{
		debug_printf("no calibration data in flash\n");
	}
}

void cali_aec_manual_enable(void)
{
	libisp_ioctl(0, MPUCMD_ISP_SET_AEC_MANUAL_EN, (void *)1);
	debug_printf("AEC manual enable\n");
}

void cali_aec_manual_disable(void)
{
	libisp_ioctl(0, MPUCMD_ISP_SET_AEC_MANUAL_EN, (void *)0);
	debug_printf("AEC manual disable\n");
}

void cali_set_daymode(void)
{
#ifdef PIN_IRLED_CTL
	if (Common_GetBoardID() != BOARD_ID_RD19)
		libgpio_write(PIN_IRLED_CTL, 0);
	else
		libgpio_write(PIN_IRLED_CTL, 1);
#endif
	libisp_set_daynight(0, 0);
}

void cali_set_nightmode(void)
{
#ifdef PIN_IRLED_CTL
	if (Common_GetBoardID() != BOARD_ID_RD19)
		libgpio_write(PIN_IRLED_CTL, 1);
	else
		libgpio_write(PIN_IRLED_CTL, 0);
#endif
	libisp_set_daynight(0, 1);
}

int cali_get_awb_gain(void)
{
	u32 *buf = (u32 *)BURN_BUFF;
	buf[0] = libisp_ioctl(0, MPUCMD_ISP_GET_AWB_B_GAIN, (void *)0);
	buf[1] = libisp_ioctl(0, MPUCMD_ISP_GET_AWB_G_GAIN, (void *)0);
	buf[2] = libisp_ioctl(0, MPUCMD_ISP_GET_AWB_R_GAIN, (void *)0);
	return 0;
}

void cali_aec_manual_gain(u16 gain)
{
	libisp_ioctl(0, MPUCMD_ISP_SET_AEC_MANUAL_GAIN, (void *)(u32)gain);
	debug_printf("AEC Manual Gain 0x%x\n", gain);
}

void cali_aec_manual_exp(u16 exp)
{
	libisp_ioctl(0, MPUCMD_ISP_SET_AEC_MANUAL_EXP, (void *)(u32)exp);
	debug_printf("AEC Manual Exp 0x%x\n", exp);
}

void cali_awb_mode(int mode)
{
	while(libisp_ioctl(0, MPUCMD_ISP_SET_AWB_MANUAL_EN, (void *)mode));
}

u8 erase_cali_area(void)
{
	debug_printf("addr %x, size %x\n", CALI_ADDR, CALI_SIZE);

	if(CALI_ADDR == 0 || CALI_SIZE == 0 || CALI_SIZE == 0xffffffff){
		return -1;
	}

	int i;
	for(i=0;i<CALI_SIZE;i+=g_sf.sector_size){
		sf_erase(CALI_ADDR + i, ERASE_SECTOR);
	}
	debug_printf("erase cali area done\n");
	return 0;
}

int save_lum = 0;
int isp_get_lum(void)
{
	u32 *buf = (u32 *)BURN_BUFF;
	memset(buf, 0, 64);
	save_lum = libisp_get_lum(0);
	syslog(LOG_INFO,"lum:0x%x\n",save_lum);
	WriteReg32(BURN_BUFF, save_lum);
	return save_lum;
}
