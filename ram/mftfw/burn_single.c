#include "calfw_includes.h"
#include "prj_cfg.h"


extern t_sflash g_sf;

static int burn_singlebin_sf(u8 * buf, int len, u32 sf_addr)
{
//	debug_printf("update buf %x, len %x, sf_addr %x\n", buf, len, sf_addr);
	u8 * check_data;
	u8 sf_data[g_sf.page_size];

	int erase_len = len;
	int offset = 0;
	while(erase_len){
		if(erase_len >= g_sf.block_size){
			sf_erase(sf_addr + offset, ERASE_BLOCK);
			offset += g_sf.block_size;
			erase_len -= g_sf.block_size;
		}else if(erase_len >= g_sf.sector_size){
			sf_erase(sf_addr + offset, ERASE_SECTOR);
			offset += g_sf.sector_size;
			erase_len -= g_sf.sector_size;
		}else{
			sf_erase(sf_addr + offset, ERASE_SECTOR);
			break;
		}
	}

	int i, j;
	for(i=0; i<len; i+=g_sf.page_size){
		sf_program((u8*)(buf + i), g_sf.page_size, sf_addr + i);
		check_data = (u8*)buf + i;
		sf_read(sf_data, g_sf.page_size, sf_addr + i);
		for(j=0; j<g_sf.page_size; j++){
			if(sf_data[j] != check_data[j]){
				debug_printf("check data error [0x%x] : 0x%x, 0x%x\n", sf_addr + i + j, sf_data[j], check_data[j]);
				return -1;
			}
		}
	}
	return 0;
}

#ifdef APP_HAS_NAND
static u32 _nand_start_newpart = 0;
static u32 _nand_bin_cnt = 0;
static u32 _nand_burn_len = 0;
static u32 _hw_nand_start = 0;
#define ALIGN_BLOCK(x) (((x) + g_nc->block_size - 1) & (~(g_nc->block_size-1)))

static int check_bad_block(unsigned int *start,unsigned int size)
{
	int nand_start = *start;
	int bn = (size + NANDCFG.tnc.block_size - 1) >> NANDCFG.block_size_n;
	int i = 0;
	int j;
	u8 buf[32];
	
	if((*start & (NANDCFG.tnc.block_size - 1)) != 0)
	{
		debug_printf("err, not at block start\n");
		return -1;
	}

	///< find the good block. read the first 16 bytes data in the spare area of the first page in the block and check whether it is 0xff or not. If it isn't 0xff, the block is bad block and skip it.
	while( i < bn )
	{
	 	nand_read_page(0, nand_start >> NANDCFG.page_size_n,(int)buf, 16, 1);
	 	
		if(buf[5] != 0xff)
	 	{
	 		debug_printf("BAD BLOCK:%x\n", nand_start);
			nand_start += NANDCFG.tnc.block_size;
			i = 0;
			continue;
	 	}
		if(i== 0) *start = nand_start;
		
		i++;
		nand_start += NANDCFG.tnc.block_size;
	}

	if(i != bn)
	{
		debug_printf("Note: can not find %d continuous block\n",bn);
		return -1;
	}
	
	nand_start = *start;
	return 0;
}

static char nf_check_data[2048];
/**
 * @brief burn SINGLE.BIN to NAND flash starting from the address of 0.
 * @param buf: the part of data of SINGLE.BIN
 * @param len: size is 4K Bytes.
 * @param nf_addr: the address of NAND flash
 */
static int burn_singlebin_nf(u8 * buf, int len, u32 nf_addr)
{
	//debug_printf("NF:update buf %x, len %x, nf_addr %x\n", buf, len, nf_addr);
	int i, n;

	u32 nand_start;
	u32 todo;

	if(nf_addr == 0){
		///< get the content of hardware header area, the format see "chapter 1.1 Hardware Header" in document "OV788 NAND Flash New Partition". 
		memcpy(&NANDCFG.nh, buf, sizeof(t_nand_partition));
		debug_printf("%x,%x,%x,%x,%x,%x,%x\n", NANDCFG.nh.magic, NANDCFG.nh.nand_config, NANDCFG.nh.bl_nand_start, NANDCFG.nh.bl_mem_start, NANDCFG.nh.bl_len, NANDCFG.nh.syscfg, NANDCFG.nh.bl2_len);
	}
	nand_start = nf_addr;
	if(nand_start < _nand_start_newpart){
		nand_start = _nand_start_newpart + _nand_burn_len;	
	}
	if(!_nand_burn_len){
		///< get the length of every area of SINGLE.BIN. The description of every area of SINGLE.BIN see "chapter 2 The format of SINGLE.BIN" in the document "OV788 NAND Flash New Partition".
		if(_nand_bin_cnt == 0){
			todo = g_nc->block_size;
		}else if(_nand_bin_cnt == 1){
			todo = ALIGN_BLOCK(NANDCFG.nh.bl_len);	
		}else if(_nand_bin_cnt == 2){
			todo = ALIGN_BLOCK(NANDCFG.nh.bl2_len);
		}else if(_nand_bin_cnt == 3){
			todo = ALIGN_BLOCK(NANDCFG.nh.fw_header_len);
		}else if(_nand_bin_cnt == 4){
			///< add 5 remaining blocks for firmware area. There is a new block for upgrading firmware if generate bad block when upgrading firmware. 
			todo = ALIGN_BLOCK(NANDCFG.nh.para_header_nand_start - NANDCFG.nh.fw_nand_start);
		}else if(_nand_bin_cnt == 5){
			todo = ALIGN_BLOCK(NANDCFG.nh.para_header_len);
		}else if(_nand_bin_cnt == 6){
			todo = ALIGN_BLOCK(NANDCFG.nh.para_len);	
		}else if(_nand_bin_cnt == 7){
			todo = ALIGN_BLOCK(NANDCFG.nh.fw_backup_header_len);
		}else if(_nand_bin_cnt == 8){
			todo = ALIGN_BLOCK(NANDCFG.nh.fw_backup_len);
		}else if(_nand_bin_cnt == 9){
			todo = ALIGN_BLOCK(NANDCFG.nh.para_backup_header_len);
		}else if(_nand_bin_cnt == 10){
			todo = ALIGN_BLOCK(NANDCFG.nh.para_backup_len);
		}
		///< find a good block for the length of "todo" from "nand_start". If there is bad block and skip it, then update the value of nand_start.
		if(check_bad_block(&nand_start, todo)){
			return -1;
		}
		_nand_burn_len = todo;

		///< update the content of hardware header area.
		if(_nand_bin_cnt == 0){
			_hw_nand_start = nand_start;
		}else if(_nand_bin_cnt == 1){
			NANDCFG.nh.bl_nand_start = nand_start;	
		}else if(_nand_bin_cnt == 2){
			NANDCFG.nh.bl2_nand_start = nand_start;
		}else if(_nand_bin_cnt == 3){
			NANDCFG.nh.fw_header_nand_start = nand_start;
		}else if(_nand_bin_cnt == 4){
			NANDCFG.nh.fw_nand_start = nand_start;
		}else if(_nand_bin_cnt == 5){
			NANDCFG.nh.para_header_nand_start = nand_start;
		}else if(_nand_bin_cnt == 6){
			NANDCFG.nh.para_nand_start = nand_start;
		}else if(_nand_bin_cnt == 7){
			NANDCFG.nh.fw_backup_header_nand_start = nand_start;
		}else if(_nand_bin_cnt == 8){
			NANDCFG.nh.fw_backup_nand_start = nand_start;
		}else if(_nand_bin_cnt == 9){
			NANDCFG.nh.para_backup_header_nand_start = nand_start;
		}else if(_nand_bin_cnt == 10){
			NANDCFG.nh.para_backup_nand_start = nand_start;
		}
		_nand_bin_cnt ++;

		///< erase the block of hardware header.
		if((_hw_nand_start & (g_nc->block_size - 1)) == 0){
			nand_erase_block(_hw_nand_start / g_nc->page_size);
		}
		///< update the content of hardware header to NAND flash.
		for(i=0; i<len; i+=g_nc->page_size){
			nand_program_page(0, (_hw_nand_start + i)/ g_nc->page_size, ((u32)&NANDCFG.nh) + i, g_nc->page_size);
		}
		_nand_start_newpart = nand_start;
	}

	//debug_printf("nand_start = %x,%x,%d,%d\n", nand_start,_nand_start_newpart, _nand_burn_len, _nand_bin_cnt);

	///< erase the found blocks
	if((nand_start & (g_nc->block_size - 1)) == 0){
		nand_erase_block(nand_start / g_nc->page_size);
	}
	///< burn the data(4K bytes) to the erased blocks.
	for(i=0; i<len; i+=g_nc->page_size){
		char * nf_data  = (char *)(buf + i);
		nand_program_page(0, nand_start / g_nc->page_size, buf + i, g_nc->page_size);
		nand_read_page(0, nand_start/g_nc->page_size, nf_check_data, g_nc->page_size, 0);
		nand_start += g_nc->page_size;
		_nand_burn_len -= g_nc->page_size;
		///< read written data and check whether the data are wrong or not.
		for(n=0; n<g_nc->page_size; n++){
			if(nf_check_data[n] != nf_data[n]){
				debug_printf("check data error [0x%x] : 0x%x, 0x%x\n", nf_addr + i + n, nf_data[n], nf_check_data[n]);
				return -1;
			}
		}
	}
	return 0;
}
#endif

int burn_singlebin(u8 * buf, int len, u32 addr)
{
	int ret=0;
	if(prj_cfg.hassflash){
		ret = burn_singlebin_sf(buf, len, addr);
	}else if(prj_cfg.hasnand){
#ifdef APP_HAS_NAND
		ret = burn_singlebin_nf(buf, len, addr);
#endif
	}else{
		ret = -1;
	}
	return ret;
}

int erase_sf(int len, u32 sf_addr)
{
	int erase_len = len;
	int offset = 0;
	while(erase_len){
		if(erase_len >= g_sf.block_size){
			sf_erase(sf_addr + offset, ERASE_BLOCK);
			offset += g_sf.block_size;
			erase_len -= g_sf.block_size;
		}else if(erase_len >= g_sf.sector_size){
			sf_erase(sf_addr + offset, ERASE_SECTOR);
			offset += g_sf.sector_size;
			erase_len -= g_sf.sector_size;
		}else{
			sf_erase(sf_addr + offset, ERASE_SECTOR);
			break;
		}
	}

	return 0;
}

#ifdef APP_HAS_NAND
int erase_nf(int len, u32 addr)
{
	return 0;
}
#endif

int erase(int len, u32 addr)
{
	int ret=0;
	if(prj_cfg.hassflash){
		ret = erase_sf(len, addr);
	}else if(prj_cfg.hasnand){
#ifdef APP_HAS_NAND
		ret = erase_nf(len, addr);
#endif
	}else{
		ret = -1;
	}
	return ret;

}

