#ifndef __CALFW_INCLUDES_H__
#define __CALFW_INCLUDES_H__

#include "includes.h"
#include "board_ioctl.h"
#include "libCommon.h"

#ifdef CALITOOL
#include "cali.h"
#endif

extern u32 input_width;
extern u32 input_height;

// -1 stop, 0 yuv, 1 raw8
void fb_init(int fmt);

int mpu_init(void);
int mpu_exit(void);
#endif
