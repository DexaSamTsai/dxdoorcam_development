#ifndef _BOARD_IOCTL_H
#define _BOARD_IOCTL_H

/* defines of board_ioctl's command*/
#define BIOC_SET_INIT_PRE				0
#define BIOC_SET_VERSION                1
#define BIOC_SET_INIT_POST 				2
#define BIOC_SET_UPDATE_MCU				3
#define BIOC_SET_BOOT                   4
#define BIOC_SET_AWB_RGAIN				5
#define BIOC_SET_AWB_BGAIN				6
#define BIOC_FUNC_LOOP				    7
#define BIOC_SET_SNR_BACKUP          	8 /*board set backup sensor b/r value*/
#define BIOC_SET_SNR_DEFAULT         	9 /*board set backuped sensor b/r value*/
#define BIOC_SET_BLUE_LED				10
#define BIOC_SET_AMBER_LED				11
#define BIOC_SAVE_LUM					13
#define BIOC_SET_SHADING_BACKUP			14
#define BIOC_SET_SHADING_DEFAULT        15
#define BIOC_IQ_CALI                    0x0083
#define BIOC_VS_START					0x0A00 // Check which VS should be initialized and started in mpu_init

#define PA_ON_GPIO          PIN_GPIO_07
#include "board_ioctl_func.h"

#endif
