#include "calfw_includes.h"
#include "prj_cfg.h"
#include "basic/production.h"
extern t_sflash g_sf;
t_prj_cfg prj_cfg;

#define ALIGN_SECTOR(x, sector_size) (((x) + sector_size - 1) & (~(sector_size -1)))

int parse_prj_cfg(u32 len, u32 base)
{
//	int sf_source=-1;
	memset(&prj_cfg, 0, sizeof(prj_cfg));

	// init item id 
	CALIITEM.lens_dtbl_id = - 1;
	CALIITEM.lens_atbl_id = - 1;
	CALIITEM.lens_ctbl_id = - 1;
	CALIITEM.white_balance_id = - 1;
	prj_cfg.isitem_mcufile = 0;

	char * cur = (char *)base;
	char * buf;
	char * c;
	char * value;

	while( (u32)cur < base + len){
		buf = cur;
		/* xmodem fill 0x1a, so skip 0x1a */
		for(; cur[0] != '\0' && cur[0] != '\n' && cur[0] != '\r' && (u32)cur < (base+len) && cur[0] != 0x1a ; cur++);
		cur[0] = '\0';
		cur ++;
		if(buf[0] == ';' || buf[0] == '\n' || buf[0] == '\r' || buf[0] == '\0' || buf[0] == '#' || buf[0] == ' ') continue;
		if(buf[0] == '/' && buf[1] == '/') continue;
		if(buf[0] == '['){
			if(!strncmp(buf+1, "CALI", 4) || !strncmp(buf+1, "AREA", 4)){
				if(CALIITEM.cnt>= MAX_CALI_DATA) break;
				CALIITEM.cnt++;
			}
			continue;
		}
		c = strchr(buf, '=');
		if(c == NULL){
			continue;
		}
		for(value=c+1; value[0]==' ' || value[0] == '\t'; value ++);
		for(c--; c[0] == ' ' || c[0]=='\t'; c--);
		c[1] = '\0';

		if(!strcmp(buf, "HasNand")){
			prj_cfg.hasnand = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "HasEEPROM")){
			prj_cfg.haseeprom = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "HasSFlash")){
			prj_cfg.hassflash = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "BootFast")){
			prj_cfg.bootfast = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SIFClock")){
			int div = 24000000/strtoul(value, NULL, 0);
			div = div/2;
			if(div > 1){
				div = div-1;
			}else{
				div = 1;
			}
			if(div > 0xf) div = 0xf;
			prj_cfg.sflashcfg.sifdiv = div;
		}else if(!strcmp(buf, "PIN_BSL_RST")){
			prj_cfg.pincfg.pin_bsl_rst = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "BSL_RST_LEVEL")){
			prj_cfg.pincfg.bsl_rst_level = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "PIN_BSL_JTAG")){
			prj_cfg.pincfg.pin_bsl_jtag = strtoul(value, NULL, 0);
			/*
		}else if(!strcmp(buf, "SN_RESET")){
			prj_cfg.pincfg.sn_reset = strtoul(value, NULL, 0);
			*/
		}else if(!strcmp(buf, "DECAGCEnable")){
			prj_cfg.adecagc.en = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "DECAGCMaxdbfs")){
			prj_cfg.adecagc.max = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "DECAGCTargetdbfs")){
			prj_cfg.adecagc.target = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "DECAGCNfdbfs")){
			prj_cfg.adecagc.nf = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "ENCAGCEnable")){
			prj_cfg.aencagc.en = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "ENCAGCMaxdbfs")){
			prj_cfg.aencagc.max = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "ENCAGCTargetdbfs")){
			prj_cfg.aencagc.target = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "ENCAGCNfdbfs")){
			prj_cfg.aencagc.nf = strtoul(value, NULL, 0);
		}else if(!strncmp(buf, "CALI_", 5)){
			if(CALIITEM.cnt == 0){
				continue;
			}
			if(!strcmp(buf, "CALI_LEN")){
				CALIITEM.len[CALIITEM.cnt-1] = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "CALI_TYPE")){
				if(!strcmp(value, "LENS_DTBL")){
					CALIITEM.lens_dtbl_id = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "LENS_ATBL")){
					CALIITEM.lens_atbl_id = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "LENS_CTBL")){
					CALIITEM.lens_ctbl_id = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "AWB")){
					CALIITEM.white_balance_id = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "LENS_DTBL_R")){
					CALIITEM.lens_dtbl_id_r = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "LENS_ATBL_R")){
					CALIITEM.lens_atbl_id_r = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "LENS_CTBL_R")){
					CALIITEM.lens_ctbl_id_r = CALIITEM.cnt - 1;
				}else if(!strcmp(value, "AWB_1")){
					CALIITEM.white_balance_id_1 = CALIITEM.cnt - 1;
				}
			}
			//mcu id
		}else if(!strncmp(buf, "File_", 5)){
			if(!strcmp(buf, "File_filename")){
				if(!strcmp(value, "mcu.bin")){
						prj_cfg.isitem_mcufile = 1;
				}
			}else if(!strcmp(buf, "File_id2")){
				if(prj_cfg.isitem_mcufile){
					prj_cfg.fileid_mcu = strtoul(value, NULL, 0);	
					prj_cfg.isitem_mcufile = 0;
				}
			}		
			//
		}else if(!strcmp(buf, "EEPROM_Delay")){
			EEPROMCFG.delay = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "EEPROM_Size")){
			EEPROMCFG.totalsize = strtoul(value, NULL, 0);
#if 0
		}else if(!strcmp(buf, "Nand_FSSize")){
			NANDCFG.fssize = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_OOBCMD")){
			NANDCFG.tnc.oob_use_cmd = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_ColSize")){
			NANDCFG.tnc.col_adr_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_RowSize")){
			NANDCFG.tnc.row_adr_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_Read30")){
			NANDCFG.tnc.use_read30 = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_PageSize")){
			NANDCFG.tnc.page_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "Nand_BlockSize")){
			NANDCFG.tnc.block_size = strtoul(value, NULL, 0);
#endif
		}else if(!strcmp(buf, "SFlash_id2table")){
			if(strtoul(value, NULL, 0)){
				SFLASHCFG.notuseid2 = 0;
			}else{
				SFLASHCFG.notuseid2 = 1;
			}
#if 1
// SF Param
		}else if(!strcmp(buf, "SF_Source")){
//			sf_source = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_TransMode")){
			g_sf.trans_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_Clk")){
			g_sf.clk = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_XipMode")){
			g_sf.xip_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_RxDelayCycle")){
			g_sf.rx_delay_cycle = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CeActiveSetupTime")){
			g_sf.ce_active_setuptime = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CeActiveHoldTime")){
			g_sf.ce_active_holdtime = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CeInactiveTime")){
			g_sf.ce_inactivetime = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_Dq2Mode")){
			g_sf.DQ2_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_Dq2Value")){
			g_sf.DQ2_value = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_Dq3Mode")){
			g_sf.DQ3_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_Dq3Value")){
			g_sf.DQ3_value = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SFlash_PageSize")){
			g_sf.page_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SFlash_SectorSize")){
			g_sf.sector_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SFlash_BlockSize")){
			g_sf.block_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SFlash_Size")){
			g_sf.chip_size = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_DmaEn")){
			g_sf.dma_en = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_StaRegQeBit")){
			g_sf.sta_reg_qe_bit = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_DeviceID")){
			g_sf.deviceID = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_StaRegNum")){
			g_sf.sta_reg_num = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdDevicid_ID")){
			g_sf.cmd_deviceid.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdDevicid_AddrLen")){
			g_sf.cmd_deviceid.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdDevicid_DummyCys")){
			g_sf.cmd_deviceid.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdDevicid_DataDummyCys")){
			g_sf.cmd_deviceid.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdDevicid_Mode")){
			g_sf.cmd_deviceid.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurRd_ID")){
			g_sf.cmd_cur_rd.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurRd_AddrLen")){
			g_sf.cmd_cur_rd.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurRd_DummyCys")){
			g_sf.cmd_cur_rd.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurRd_DataDummyCys")){
			g_sf.cmd_cur_rd.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurRd_Mode")){
			g_sf.cmd_cur_rd.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg1_ID")){
			g_sf.cmd_rd_sta_reg1.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg1_AddrLen")){
			g_sf.cmd_rd_sta_reg1.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg1_DummyCys")){
			g_sf.cmd_rd_sta_reg1.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg1_DataDummyCys")){
			g_sf.cmd_rd_sta_reg1.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg1_Mode")){
			g_sf.cmd_rd_sta_reg1.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg2_ID")){
			g_sf.cmd_rd_sta_reg2.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg2_AddrLen")){
			g_sf.cmd_rd_sta_reg2.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg2_DummyCys")){
			g_sf.cmd_rd_sta_reg2.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg2_DataDummyCys")){
			g_sf.cmd_rd_sta_reg2.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdRdStaReg2_Mode")){
			g_sf.cmd_rd_sta_reg2.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrStaReg_ID")){
			g_sf.cmd_write_sta_reg.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrStaReg_AddrLen")){
			g_sf.cmd_write_sta_reg.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrStaReg_DummyCys")){
			g_sf.cmd_write_sta_reg.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrStaReg_DataDummyCys")){
			g_sf.cmd_write_sta_reg.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrStaReg_Mode")){
			g_sf.cmd_write_sta_reg.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurWr_ID")){
			g_sf.cmd_cur_write.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurWr_AddrLen")){
			g_sf.cmd_cur_write.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurWr_DummyCys")){
			g_sf.cmd_cur_write.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurWr_DataDummyCys")){
			g_sf.cmd_cur_write.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdCurWr_Mode")){
			g_sf.cmd_cur_write.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdSecErase_ID")){
			g_sf.cmd_sec_erase.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdSecErase_AddrLen")){
			g_sf.cmd_sec_erase.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdSecErase_DummyCys")){
			g_sf.cmd_sec_erase.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdSecErase_DataDummyCys")){
			g_sf.cmd_sec_erase.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdSecErase_Mode")){
			g_sf.cmd_sec_erase.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdBlockErase_ID")){
			g_sf.cmd_block_erase.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdBlockErase_AddrLen")){
			g_sf.cmd_block_erase.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdBlockErase_DummyCys")){
			g_sf.cmd_block_erase.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdBlockErase_DataDummyCys")){
			g_sf.cmd_block_erase.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdBlockErase_Mode")){
			g_sf.cmd_block_erase.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdChipErase_ID")){
			g_sf.cmd_chip_erase.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdChipErase_AddrLen")){
			g_sf.cmd_chip_erase.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdChipErase_DummyCys")){
			g_sf.cmd_chip_erase.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdChipErase_DataDummyCys")){
			g_sf.cmd_chip_erase.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdChipErase_Mode")){
			g_sf.cmd_chip_erase.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrEn_ID")){
			g_sf.cmd_write_en.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrEn_AddrLen")){
			g_sf.cmd_write_en.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrEn_DummyCys")){
			g_sf.cmd_write_en.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrEn_DataDummyCys")){
			g_sf.cmd_write_en.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrEn_Mode")){
			g_sf.cmd_write_en.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrDis_ID")){
			g_sf.cmd_write_dis.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrDis_AddrLen")){
			g_sf.cmd_write_dis.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrDis_DummyCys")){
			g_sf.cmd_write_dis.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrDis_DataDummyCys")){
			g_sf.cmd_write_dis.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdWrDis_Mode")){
			g_sf.cmd_write_dis.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownEn_ID")){
			g_sf.cmd_powerdown_en.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownEn_AddrLen")){
			g_sf.cmd_powerdown_en.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownEn_DummyCys")){
			g_sf.cmd_powerdown_en.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownEn_DataDummyCys")){
			g_sf.cmd_powerdown_en.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownEn_Mode")){
			g_sf.cmd_powerdown_en.sfc_sif_mode = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownDis_ID")){
			g_sf.cmd_powerdown_dis.cmd_id = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownDis_AddrLen")){
			g_sf.cmd_powerdown_dis.addr_len = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownDis_DummyCys")){
			g_sf.cmd_powerdown_dis.dummy_cys = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownDis_DataDummyCys")){
			g_sf.cmd_powerdown_dis.data_dummy_cycles = strtoul(value, NULL, 0);
		}else if(!strcmp(buf, "SF_CmdPowerDownDis_Mode")){
			g_sf.cmd_powerdown_dis.sfc_sif_mode = strtoul(value, NULL, 0);
		}
#endif
		if(prj_cfg.hasnand){
#if 0
			if(!strcmp(buf, "Part_BlSize")){
				NANDCFG.nh.bl_len = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_Bl2Size")){
				NANDCFG.nh.bl2_len = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_FwSize")){
				NANDCFG.nh.fw_len = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_ParaSize")){
				NANDCFG.nh.para_len = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_CalibSize")){
				NANDCFG.nh.calib_len = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_BackupEnable")){
				if(strtoul(value, NULL, 0)){
					NANDCFG.nh.fw_backup_len = NANDCFG.nh.fw_len;
					NANDCFG.nh.para_backup_len = NANDCFG.nh.para_len;
				}else{
					NANDCFG.nh.fw_backup_len = 0;
					NANDCFG.nh.para_backup_len = 0;
				}
			}else{
			}
#endif
		}else{
			if(!strcmp(buf, "Part_BlSize")){
				SFLASHCFG.sh.bootloader_length = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_Bl2Size")){
				SFLASHCFG.sh.bootloader2_length = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_FwSize")){
				SFLASHCFG.sh.fw_length = strtoul(value, NULL, 0);
				SFLASHCFG.sh.fw2_length = SFLASHCFG.sh.fw_length;
			}else if(!strcmp(buf, "Part_ParaSize_S")){
				SFLASHCFG.sh.para_S_length = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_ParaSize_F")){
				SFLASHCFG.sh.para_F_length = strtoul(value, NULL, 0);
			}else if(!strcmp(buf, "Part_CalibSize")){
				SFLASHCFG.sh.calib_length = strtoul(value, NULL, 0);
			}else{
			}
		}
	}

	debug_printf("hassflash %d\n", prj_cfg.hassflash);
	if(prj_cfg.haseeprom){
	}

	if(prj_cfg.hasnand){
#ifdef APP_HAS_NAND
		nandflash_init();
		
		int i;
		for(i=0;i<16;i++){
			if( (1 << i) == g_nc->page_size) break;
		}
		NANDCFG.page_size_n = i;
		for(i=0;i<32;i++){
			if( (1 << i) == g_nc->block_size) break;
		}
		NANDCFG.block_size_n = i;
		
		prj_cfg.calicfg.cal_hdr_len = g_nc->block_size;
		prj_cfg.calicfg.cal_area_size = NANDCFG.nh.calib_len;
		prj_cfg.calicfg.cal_area_addr = g_nc->block_size + NANDCFG.nh.bl_len + NANDCFG.nh.bl2_len + g_nc->block_size + NANDCFG.nh.fw_len + g_nc->block_size + NANDCFG.nh.para_len + g_nc->block_size + NANDCFG.nh.fw_backup_len + g_nc->block_size + NANDCFG.nh.para_backup_len;
		debug_printf("calib hdlen : %x, area size : %x area addr : %x\n", prj_cfg.calicfg.cal_hdr_len, prj_cfg.calicfg.cal_area_size, prj_cfg.calicfg.cal_area_addr);
#endif
	}

	if(prj_cfg.hassflash){
		t_sflash * p_temp_flash = NULL;
		p_temp_flash = flash_detect(SF_SOURCE_SFC);
		if (p_temp_flash != NULL) {
			debug_printf("flash name %s\n", p_temp_flash->name);
			strcpy((u8*) base, p_temp_flash->name);
		} else {
			debug_printf("spi_flash id do not match!\n");
			return -1;
		}
		t_sf_hdr sf_hdr;
		sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);
		if(sf_hdr.magic_num != 0x4f565449){
			syslog(LOG_ERR, "Not found partition, should burn single.bin before calibration!\n");
		}else{
			prj_cfg.calicfg.cal_area_addr = sf_hdr.cali_addr;
			prj_cfg.calicfg.cal_area_size = sf_hdr.cali_len;
			prj_cfg.calicfg.cal_hdr_len = g_sf.sector_size;
			debug_printf("calib hdlen : %x, area size : %x area addr : %x\n", prj_cfg.calicfg.cal_hdr_len, prj_cfg.calicfg.cal_area_size, prj_cfg.calicfg.cal_area_addr);
		}
	}

	return 0;
}
void re_detect_single_bin(void){
	t_sflash * p_temp_flash = NULL;
	p_temp_flash = flash_detect(SF_SOURCE_SFC);
    if(p_temp_flash == NULL){
    	debug_printf("Test the flash error!\n");
    	prj_cfg.calicfg.cal_area_addr = 0;
    }else{
		t_sf_hdr sf_hdr;
		sf_read((u8*) &sf_hdr, sizeof(t_sf_hdr), 0);
		if (sf_hdr.magic_num != 0x4f565449) {
			syslog(LOG_ERR,	"Not found partition, should burn single.bin before calibration!\n");
			prj_cfg.calicfg.cal_area_addr = 0;
		} else {
			prj_cfg.calicfg.cal_area_addr = sf_hdr.cali_addr;
			prj_cfg.calicfg.cal_area_size = sf_hdr.cali_len;
			prj_cfg.calicfg.cal_hdr_len = g_sf.sector_size;
			debug_printf("calib hdlen : %x, area size : %x area addr : %x\n",prj_cfg.calicfg.cal_hdr_len, prj_cfg.calicfg.cal_area_size,
					prj_cfg.calicfg.cal_area_addr);
		} 
    } 
}

