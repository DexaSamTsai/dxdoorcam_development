/***************************
 * basic_test.c
 * Created on: Jul 27, 2017
 * Author: xuefeng.zhang
 ***************************/
#include "production.h"
#include "audio/audio.h"

/***
 * flash test
 */
static t_sflash *s_flash = NULL;
extern u32 sif_flash_readid(t_sflash *sflash);
extern u32 sfc_flash_readid(t_sflash *sflash);
t_sflash *flash_detect(SF_SOURCE source)
{
	t_sflash * tmp;
	u32 id = 0;

extern t_sflash sflash_cfg_start;
extern t_sflash sflash_cfg_end;
	for(tmp = (t_sflash *)(&sflash_cfg_start); tmp < (t_sflash *)(&sflash_cfg_end) ; tmp ++ ){
		if(source == SF_SOURCE_SIF0_P0 ||
				source == SF_SOURCE_SIF0_P1 ||
				source == SF_SOURCE_SIF0_P2 ||
				source == SF_SOURCE_SIF2_P0 ||
				source == SF_SOURCE_SIF2_P1
		  ){
			id = sif_flash_readid(tmp);
		}else{
			id = sfc_flash_readid(tmp);
		}

		syslog(LOG_DEBUG, "sf_detect %s: %x(%x)\n", tmp->name ? tmp->name : "NOName", id, tmp->deviceID);

		if(id == 0 || id == 0xff || id == 0xffffff){
			continue;
		}

		if(id == tmp->deviceID){
			syslog(LOG_INFO, "sflash found: %s\n", tmp->name ? tmp->name : "NOName");
			s_flash = tmp;
			return s_flash;
		}else{
			continue;
		}
	}
	syslog(LOG_ERR, "sf detect failed, readid: %x\n", id);
	return NULL;
}
/*
 * if return 0 : ok
 * else fail
 */
int flash_test(void){
    int ret = 0;
    if(flash_detect(SF_SOURCE_SFC) == NULL)
    	return ret =-1;
    int i;
    for(i =0 ;i < BUFSIZE; i++){
    	buff[i] = i % 256;
    }

	if((sf_write(buff, BUFSIZE, 0)) <0){
		debug_printf("write error \n");
	    return ret = -2;
	}

	memset(buff, 0, BUFSIZE);
	if((sf_read(buff,BUFSIZE,0)) <0){
		debug_printf("read error\n");
		return ret =-3;
	}

	for(i=0; i<BUFSIZE; i++){
	    if(buff[i] != (i%256)){
	    	debug_printf("spi_sflash write data is %08x, read data is %08x, error address is %08x\n", i%256, buff[i], i+0);
	    	ret = -4;
	    }
	}
    return ret;
}

/***
 * audio test
 */
#ifdef CONFIG_AUDIO_EN
static int _spk_vol = -10;
static int _mic_vol = 8;

extern void set_format(int fmt);

typedef enum{
	FUNC_CLEAN = 0,
	FUNC_SPEAK,
} FUNC_TEST;
volatile int g_mode = FUNC_CLEAN;

void production_test_loop(void)
{
	switch(g_mode){
		case FUNC_SPEAK:
			//adec_play((char *)BURN_BUFF, ReadReg32(BURN_LEN), 1, 11025, _spk_vol);
			audio_play(11025,1,_spk_vol );
			g_mode = FUNC_CLEAN;
			break;
		default:
			break;
	}
}

s8 spk_test(void)
{
	g_mode = FUNC_SPEAK;
	production_test_loop();
	return 0;
}

void spk_adj_vol(int vol)
{
	_spk_vol = vol - 62;
	if(_spk_vol > 4) _spk_vol = 4;
	if(_spk_vol < -62) _spk_vol = -62;

	libacodec_set_dacvol(_spk_vol);
}

void mic_adj_vol(int vol)
{
	_mic_vol = vol - 17;
	if(_mic_vol > 29) _mic_vol = 29;
	if(_mic_vol < -17) _mic_vol = -17;

	libacodec_set_adcvol(_mic_vol);
}

s8 mic_start(void)
{
	set_format(2);
	g_libuprinter_cfg->get_frame = arec_get_frame;
	ENABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
	//arec_start(1, 11025, _mic_vol);
	audio_rec_start(11025,1, _mic_vol);
	return 0;
}

s8 mic_stop(void)
{
	//arec_stop();
	audio_rec_stop();
	set_format(-1);
	g_libuprinter_cfg->get_frame = NULL;
	return 0;
}
#endif




