/*********************************
 * production.h
 *
 *  Created on: Jul 27, 2017
 *      Author: zxf
 ********************************/

#ifndef RAM_MFTFW_BASIC_PRODUCTION_H_
#define RAM_MFTFW_BASIC_PRODUCTION_H_

#include "calfw_includes.h"
#include "../prj_cfg.h"


#define SDRAM_START_ADDR 0x90100000
#define BUFSIZE 128
static int buff[BUFSIZE];

s8 ddr_test(void);
t_sflash *flash_detect(SF_SOURCE source);
void production_test_loop(void);
int flash_test(void);


#ifdef CONFIG_AUDIO_EN
extern s8 spk_test(void);
extern void spk_adj_vol(int vol);
extern void  mic_adj_vol(int vol);
extern s8 mic_start(void);
extern s8 mic_stop(void);
#endif

#endif /* RAM_MFTFW_BASIC_PRODUCTION_H_ */
