/***************************
 * ddr_test.c
 * Created on: Jul 27, 2017
 * Author: xuefeng.zhang
 ***************************/

#include "production.h"
static int _ddr_test(u32 ddr_size) {
	int error = 0;
	u32 i = 0;
	volatile u32 *pStart = (u32*) (SDRAM_START_ADDR);
	u32 size = ddr_size / sizeof(u32);
	debug_printf("the size = %x\n", size);
	volatile u32 *pEnd = pStart + size;
	debug_printf("the pStart = 0x%x\n", pStart);
	debug_printf("the pEnd = 0x%x\n", pEnd);
	volatile u32 *p;
	for (p = pStart; p != pEnd; p++) {
		*p = (u32) p;
	}
	//debug_printf("write the address vaule to addr.\n");
	// 将所有地址的变为其
	for (p = pStart; p < pEnd; p++) {
		i++;
		if ((*p) != (u32) p) {
			debug_printf("addr error, the addr =[0x%x], the value = [0x%x], num = [0x%x]\n",p, *p, i);
			return error -2;
		}
	}
	return error;
}


s8 ddr_test(void)
{
	debug_printf("*************************\n");
	u32 ddr_size;
#ifdef CONFIG_BOARD_RD_18_EN
	ddr_size = 64*1024*1024;
#elif CONFIG_BOARD_RD_19_EN
	ddr_size = 128*1024*1024;
#elif CONFIG_BOARD_RING_EN
	ddr_size = 128*1024*1024;
#else
	debug_printf("please set the ddr size\n");
	ddr_size = 0;
	return -1;
#endif
	lowlevel_forceputc('>');
	if(_ddr_test(ddr_size) != 0){
		return -1;
	}
	return 0;
}

