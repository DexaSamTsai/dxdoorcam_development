#include "calfw_includes.h"
#include "prj_cfg.h"

extern t_sflash g_sf;

static int dump_singlebin_sf(u8 * buf, int len, u32 sf_addr)
{
	int i;
	if(prj_cfg.hassflash){
		for(i=0; i<len; i+=g_sf.page_size){
			sf_read(buf+i, g_sf.page_size, sf_addr+i);
		}
	}
	WriteReg32(BURN_ADDR, sf_addr);
	return 0;
}

static int dump_singlebin_nf(u8 * buf, int len, u32 nf_addr)
{
#ifdef APP_HAS_NAND
	if(prj_cfg.hasnand){
		/* check bad block */
		u32 nand_start = nf_addr;
		u8 badblock_check[32];
		while(1){
			nand_read_page(0, nand_start >> NANDCFG.page_size_n,(int)badblock_check, 16, 1);

			if(badblock_check[5] != 0xff){
				nand_start += NANDCFG.tnc.block_size;
				debug_printf("BAD BLOCK:%x\n", nand_start);
			}else{
				WriteReg32(BURN_ADDR, nand_start);
				break;
			}
		}

		/* read */
		for(i=0; i<len; i+=g_nc->page_size){
			nand_read_page(0, nand_start/g_nc->page_size, buf + i, g_nc->page_size, 0);
			nand_start += g_nc->page_size;
		}
	}
#endif
	return 0;
}

int dump_singlebin(u8 * buf, int len, u32 addr)
{
	if(prj_cfg.hassflash){
		dump_singlebin_sf(buf, len, addr);
	}else if(prj_cfg.hasnand){
		dump_singlebin_nf(buf, len, addr);
	}else{}
	return 0;
}
