# include "includes.h"
# include "board_ioctl.h"

static void
board_sys_init( void )
{
	syslog( LOG_INFO, "\nStart DxDoorCam\n" );
}

#ifndef SET_CLKDIV_BUGFIX
static void
set_clkdiv( u32 *ve_div, u32 *pram_div )
{
	*ve_div		= 2;		//ve:120M
	*pram_div	= 2;		//120M
}
#endif

static void
board_hw_init( void )
{
extern int DxDoorCam_hw_init( void );
	DxDoorCam_hw_init();
	while( !libsccb_grp_check( 0 ) );	//wait sensor setting download finished
	clockconfig_up_fix();
	mpu_start();
}

int
board_ioctl( u32 cmd, u32 arg, void * ret_arg )
{
	switch( cmd ) {
		case BIOC_SET_SYS_INIT:
			board_sys_init();
			return 0;
		case BIOC_SET_HW_INIT:
			board_hw_init();
			return 0;
		case BIOC_HW_EARLY_INIT:
			return DxDoorCam_hw_early_init();
		default:
			return -EINVAL;
	}
}

