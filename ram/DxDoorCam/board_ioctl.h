#ifndef _BOARD_IOCTL_H
#define _BOARD_IOCTL_H

/* defines of board_ioctl's command*/
#define BIOC_SET_SYS_INIT  		1 
#define BIOC_SET_HW_INIT         2
//#define BIOC_SET_HW_EARLY_INIT 3

#include "board_ioctl_func.h"

#endif
