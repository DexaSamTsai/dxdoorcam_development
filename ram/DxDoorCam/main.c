//==========================================================================
// Include File
//==========================================================================
# include "includes.h"
# include "board_ioctl.h"
# include "mcu_ctrl.h"

//==========================================================================
// Type Define
//==========================================================================
# ifndef dbg_line
#	define dbg_line( fmt, ... )		printf( "[%s line: %d]"fmt"\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_error
#	define dbg_error( fmt, ... )	printf( "\033[1;31m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

extern void MainProcessRun( void );

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
void *
thread_main( void *arg )
{
	board_ioctl( BIOC_SET_SYS_INIT, 0, NULL );

	if ( DxDoorCam_Init() < 0 ) {
		//DxDoorCam_Stop();
		//TODO Wifi Setup
		dbg_error( "DxDoorCam_Init() Failed!!!" );
		return NULL;
	}

	board_ioctl( BIOC_SET_HW_INIT, 0, NULL );

	if(mcu_get_mode() == MODE_INIT || getenv("wifi_dnsserver") == NULL){
		DxDoorCam_SetFirstBoot( 1 );
	} else {
		DxDoorCam_SetFirstBoot( 0 );
	}

	dbg_line( "ertos_heap_cur_free = %u Bs", ertos_heap_cur_free() );
	dbg_line( "ertos_heap_min_free = %u Bs", ertos_heap_min_free() );
	dbg_line( "rom_ertos_heap_size = %u Bs", rom_ertos_heap_size );

	DxDoorCam_InitNetwork();

	MainProcessRun();

    DxDoorCam_ExitNetwork();
    DxDoorCam_Exit();
    sleep(5);
    DxDoorCam_PowerOff(  );

	return NULL;
}
