#ifndef __LIBOSD_H__
#define __LIBOSD_H__

/**
 * @defgroup libvosd
 * @ingroup libvenc
 * @brief library for adding osd on video stream or image by OV788 front osd module, video overlay module or drawing by CPU.There are some limitation in different ways. If use video overlay to draw osd, it doesn't support transparent color. If use cpu to draw osd, it only be drawn on the top-right corner of video stream and osd maximal height is 16 pixel, meanwhile, can't support to draw osd on the second channel of video stream.
 * @{
 */
/**
 * @name color
 * @details format:0xYYUUVV
 * @{
 */
#define YUV_BLACK	0x007F7F
#define YUV_BLUE		0x1DFE6A
#define YUV_GREEN	0x952A14
#define YUV_RED		0x4C54FF
#define YUV_CYAN		0xB2AA00
#define YUV_MAGENTA	0x69D4Ea
#define YUV_YELLOW	0xE10094
#define YUV_WHITE	0xFF7F7F
/** @} */

//INTERNAL_START
enum{
	VOSD_MEM_LE,
	VOSD_MEM_BE,
};
//INTERNAL_END

enum{
	VOSD_FORMAT_YUV420,
	VOSD_FORMAT_YUV422,
	VOSD_FORMAT_YUV444,
};

/**
 * @name osd drawing ways
 * @brief use different way to implement the function of drawing osd on video stream.
 * @{
 */
enum{
	VOSD_USE_FRONT_OSD,
	VOSD_USE_VOL,
	VOSD_USE_CPU,
};
/** @}*/

enum{
	VOSD_DATA_FORMAT_YUV_UNCOMPRESS,
	VOSD_DATA_FORMAT_YUV_COMPRESS,
};

#define VOSD_WIN_TOTAL_COUNT   (4)

typedef struct s_vosd_win_cfg
{
	u8 win_no;
	u8 win_en;
	u8 mem_id;
	u16 win_hsize;
	u16 win_vsize;
	u16 win_hstart;   ///< for R3 new, BIT15: 1-->window start x is out of background picture, 0-->window start x is in picture.BIT0---BIT14 for start address
	u16 win_vstart;   ///< for R3 new, BIT15: 1-->window start x is out of background picture, 0-->window start x is in picture.BIT0---BIT14 for start address
	u32 win_memaddr;
	u32 color[7];     ///< 1.Old(4LUT):osd color value. 0---23: is YUV420 color.  24--32 is transparence, option is 0--7. 0: indicates transparent and display backgroud layer, 7: indicates opaque and display menu osd layer.2.new(7LUT):[23--16]Y,[15--8]U,[7--0]V
}t_vosd_win_cfg;


typedef struct s_vosd_str_cfg
{
	u8 (*cb_vosd_time_get)(char * output);      ///<callback function, get current date for drawing it on osd 
	char vosd_ui_str[24];                       ///<buffer for current date string.
	u8 font_color;
	u8 bk_color;
}t_vosd_str_cfg;

//INTERNAL_START
typedef struct s_vosd_res_item
{
	unsigned short width;
	unsigned short height;
	unsigned int size;
	unsigned int offset;
}t_vosd_res_item;

struct s_vosd_res;
typedef struct s_vosd_res_load_hw
{
	int (*cb_vosd_resitem_load)(struct s_vosd_res * res, u32 memaddr, u32 item_no);
	int (*cb_vosd_res_load)(struct s_vosd_res * res);
}t_vosd_res_load_hw;

typedef struct s_vosd_res
{
	unsigned int item_count;
	unsigned int offset_base;
	struct s_vosd_res_load_hw * hw;
	struct s_vosd_res_item * item;
}t_vosd_res;
//INTERNAL_END

/**
 * @brief video osd structure for the first channel video.
 */
typedef struct s_libvosd_cfg
{
	u8 format;                                  ///<osd data format
//INTERNAL_START
	u16 img_hsize;
	u16 img_vsize;
	u8 in_format;
	u8 out_format;	
//INTERNAL_END
	struct s_vosd_win_cfg vosd_win[VOSD_WIN_TOTAL_COUNT];
	struct s_vosd_str_cfg * str_cfg;
	struct s_vosd_res * vosd_res;
}t_libvosd_cfg;

extern t_vosd_res_load_hw g_res_load_sflash;
#define VOSD_RES_LOAD_SLFASH   &g_res_load_sflash

#define LIBVOSD_RES_DECLARE(resnum, hwname) \
static t_vosd_res_item _vosd_resitems[resnum]; \
t_vosd_res g_vosd_res =  \
{ \
	.item_count = resnum, \
	.offset_base = 0, \
	.hw = VOSD_RES_LOAD_##hwname;\
	.item = &_vosd_resitems,\
};

extern t_vosd_res g_vosd_res;

/**
 * @brief init libosd
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_init(t_libvosd_cfg * cfg);


/**
 * @brief enable osd
 *
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_enable(t_libvosd_cfg * cfg, u8 win_no);

/**
 * @brief disable osd
 *
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_disable(t_libvosd_cfg * cfg, u8 win_no);

/**
 * @brief update osd color
 *
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_win_color_update(t_libvosd_cfg * cfg, u8 win_no);


/**
 * @brief configure osd window 
 * 
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_win_config(t_libvosd_cfg * cfg, u8 win_no);

/**
 * @brief get buffer address of osd window.
 * 
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
u32 libvosd_draw_buf_get(t_libvosd_cfg * cfg, u8 win_no);

/**
 * @breif draw time
 * @details only use old osd module, support 2bit color and 4 window.
 *
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param str       time string
 * @param win_no    window number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_draw_time(t_libvosd_cfg * cfg, unsigned char * str, u8 win_no);

/**
 * @brief draw compressed pictures on osd
 * @details use new osd module, support to draw compressed pictures. need load pictures from DDR or external device.
 *
 * @param cfg       pointer to the structure \ref t_libvosd_cfg
 * @param win_no    window number
 * @param item_no   picture number
 * 
 * @return 0: OK, < 0 failed
 */
int libvosd_draw_compress(t_libvosd_cfg * cfg, u8 win_no, u8 item_no);
/** @} */

#endif  /// __LIBOSD_H__
