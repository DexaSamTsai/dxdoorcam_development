#ifndef _LIBSYSDMA_H_
#define _LIBSYSDMA_H_

/*
 *Macro of I/O address for dma hardware mode. Define macro as 0xAB-A:channel B:if A is same but B not, it mean B share same dma channel
 *In hardware mode, 4 channels connect fixed peripheral, cannot choose which channel connect which peripheral.
 * channe0-->spi/scif
 * channel-->sfc/uart_s
 * channe2-->sif2 /scio
 * channe3-->uart_m/bch/usb
 *Software mode is only used to case that memory<-->memory and use channel 1 by default.
 */
#define SYSDMA_DEV_SIF      0x00
#define SYSDMA_DEV_SCIF     0x01
#define SYSDMA_DEV_SFC      0x10 //SFC<->SIF1
#define SYSDMA_DEV_UART_S   0x11 //uart_s<-> uart1
#define SYSDMA_DEV_SIF2     0x20
#define SYSDMA_DEV_SCIO     0x21
#define SYSDMA_DEV_UART_M   0x30 //uart0
#define SYSDMA_DEV_BCH      0x31
#define SYSDMA_DEV_USB      0x32

/*Macro of I/o address for dma software mode*/
#define SYSDMA_DEV_SOFT_SPI  SIF_BASE

#define SYSDMA_NO_WAIT 0

//typedef int (*SYSDMA_IRQ_HANDLER)(void);

/** @name dma transmit address mode */
/* @{ */
#define SYSDMA_ADDR_INC  0x00
#define SYSDMA_ADDR_DEC  0x01
#define SYSDMA_ADDR_FIX  0x02
/* @} */

/** @name dma transmit direct */
/* @{ */
#define SYSDMA_TRANS_AHB2RAM  0x00
#define SYSDMA_TRANS_AHB2AHB  0x01
#define SYSDMA_TRANS_RAM2AHB  0x02
#define SYSDMA_TRANS_RAM2RAM  0x03
/* @} */

/** @name dma request mode */
/* @{ */
#define SYSDMA_REQ_HW  0x0
#define SYSDMA_REQ_SW  0x1
/* @} */

typedef enum{
	TRANSBITWIDE32 = 0,
	TRANSBITWIDE8
}TRANSBITWIDE;

/** @define dma interrupt hanlder */
/* @{ */
typedef s32 (*SYSDMA_IRQ_HANDLER)(void);
/* @} */

/*config for every channle separately*/
typedef struct s_dma_channel_config
{
	//	SYSDMA_IRQ_HANDLER * handler;
	u8 prio;
	u8 burst_len; /*TODO　０　support burst 4/8/16*/
	TRANSBITWIDE trans_bit_wide;//0: 32bit AHB/AXI 1:8bit AHB/AXI. default is 0
}t_dma_channel_config;

/*dma top level struct*/
typedef struct s_dma
{
	u8 endian; /*4 channel share one same endian setting. 
				*BIT0 : AHB write port endian. 1: reverse 0: no change
				*BIT1 : AHB read port endian. 1: reverse 0: no change
				*BIT2 : AXI write port endian. 1: reverse 0: no change
				*BIT3 : AXI read port endian. 1: reverse 0: no change
				*BIT4 : GRFC write port endian. 1: reverse 0: no change
				*BIT5 : GRFC read port endian. 1: reverse 0: no change
				*/
	
	t_dma_channel_config dma_ch_config[4]; /*config for every channle separately*/

	/*call back*/
	s32 (*callback_dma_config_pre)(u32 src, u32 dst, u32 len, u16 wait);
	s32 (*callback_dma_config_post)(u32 src, u32 dst, u32 len, u16 wait);
}t_dma;

extern t_dma g_dma;

s32 libdma_init(void);
/**
 *@ breif 
 *@ return 0:ok -1: timeout
 */
s32 dma_poll(u32 dev, u32 timeout);

/**
 *@ breif dma config function
 *
 *@ param len unit is byte
 *@ return 0:ok -1: cannot wait because signal full, increase MAX_EVENTS in newos_init.c
 */
s32 dma_config(u32 src, u32 dst, u32 len, u16 wait);
s32 dma_config_new(u32 src, u32 dst, u32 len, u16 wait);
/**
 *@ breif dma interrupt function for except.c call
 *@ return 0:ok
 */
s32 dma_irq_handler(void);
u8 dma_complete_wait(s32 chn, s32 time);
#endif
