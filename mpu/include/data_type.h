/****************************************************************************
 * 
 * Copyright (c) 2008 OmniVision Technologies Inc. 
 * The material in this file is subject to copyright. It may not
 * be used, copied or transferred by any means without the prior written
 * approval of OmniVision Technologies, Inc.
 *
 * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 ****************************************************************************/

#ifndef _DATA_TYPE_H_
#define _DATA_TYPE_H_

/**
 * @defgroup data type
 * @brief data type definition
 * @{
 */

#ifndef NULL
#define NULL 0L
#endif

typedef unsigned long size_t;
typedef unsigned char  		bool;
typedef unsigned char  		u8;  
typedef signed   char  		s8;  
typedef unsigned short 		u16; 
typedef signed   short		s16; 
typedef unsigned int  		u32; 
typedef signed   int  		s32; 
typedef unsigned long long 	u64; 
typedef signed   long long 	s64; 
typedef unsigned char  		BYTE;  
typedef unsigned char  		INT8U;  
typedef unsigned int  		INT32U; 

//u*le type indicate that we hope data variable is little endian
#ifndef __u16le__
#define __u16le__
typedef unsigned short		u16le;
#endif

#ifndef __u32le__
#define __u32le__
typedef unsigned int  		u32le; 
#endif

//u*be type indicate that we hope data variable is big endian
#ifndef __u16be__
#define __u16be__
typedef unsigned short		u16be;
#endif

#ifndef __u32be__
#define __u32be__
typedef unsigned int  		u32be; 
#endif


#define	EPERM		 1	/* Operation not permitted */
#define	ENOENT		 2	/* No such file or directory */
#define	ESRCH		 3	/* No such process */
#define	EINTR		 4	/* Interrupted system call */
#define	EIO		 5	/* I/O error */
#define	ENXIO		 6	/* No such device or address */
#define	E2BIG		 7	/* Argument list too long */
#define	ENOEXEC		 8	/* Exec format error */
#define	EBADF		 9	/* Bad file number */
#define	ECHILD		10	/* No child processes */
#define	EAGAIN		11	/* Try again */
#define	ENOMEM		12	/* Out of memory */
#define	EACCES		13	/* Permission denied */
#define	EFAULT		14	/* Bad address */
#define	ENOTBLK		15	/* Block device required */
#define	EBUSY		16	/* Device or resource busy */
#define	EEXIST		17	/* File exists */
#define	EXDEV		18	/* Cross-device link */
#define	ENODEV		19	/* No such device */
#define	ENOTDIR		20	/* Not a directory */
#define	EISDIR		21	/* Is a directory */
#define	EINVAL		22	/* Invalid argument */
#define	ENFILE		23	/* File table overflow */
#define	EMFILE		24	/* Too many open files */
#define	ENOTTY		25	/* Not a typewriter */
#define	ETXTBSY		26	/* Text file busy */
#define	EFBIG		27	/* File too large */
#define	ENOSPC		28	/* No space left on device */
#define	ESPIPE		29	/* Illegal seek */
#define	EROFS		30	/* Read-only file system */
#define	EMLINK		31	/* Too many links */
#define	EPIPE		32	/* Broken pipe */
#define	EDOM		33	/* Math argument out of domain of func */
#define	ERANGE		34	/* Math result not representable */
#define	EDEADLK		35	/* Resource deadlock would occur */
#define	ENAMETOOLONG	36	/* File name too long */
#define	ENOLCK		37	/* No record locks available */
#define	ENOSYS		38	/* Function not implemented */
#define	ENOTEMPTY	39	/* Directory not empty */
#define	ELOOP		40	/* Too many symbolic links encountered */
#define	EWOULDBLOCK	EAGAIN	/* Operation would block */
#define	ENOMSG		42	/* No message of desired type */
#define	EIDRM		43	/* Identifier removed */
#define	ECHRNG		44	/* Channel number out of range */
#define	EL2NSYNC	45	/* Level 2 not synchronized */
#define	EL3HLT		46	/* Level 3 halted */
#define	EL3RST		47	/* Level 3 reset */
#define	ELNRNG		48	/* Link number out of range */
#define	EUNATCH		49	/* Protocol driver not attached */
#define	ENOCSI		50	/* No CSI structure available */
#define	EL2HLT		51	/* Level 2 halted */
#define	EBADE		52	/* Invalid exchange */
#define	EBADR		53	/* Invalid request descriptor */
#define	EXFULL		54	/* Exchange full */
#define	ENOANO		55	/* No anode */
#define	EBADRQC		56	/* Invalid request code */
#define	EBADSLT		57	/* Invalid slot */
#define	EDEADLOCK	EDEADLK
#define	EBFONT		59	/* Bad font file format */
#define	ENOSTR		60	/* Device not a stream */
#define	ENODATA		61	/* No data available */
#define	ETIME		62	/* Timer expired */
#define	ENOSR		63	/* Out of streams resources */
#define	ENONET		64	/* Machine is not on the network */
#define	ENOPKG		65	/* Package not installed */
#define	EREMOTE		66	/* Object is remote */
#define	ENOLINK		67	/* Link has been severed */
#define	EADV		68	/* Advertise error */
#define	ESRMNT		69	/* Srmount error */
#define	ECOMM		70	/* Communication error on send */
#define	EPROTO		71	/* Protocol error */
#define	EMULTIHOP	72	/* Multihop attempted */
#define	EDOTDOT		73	/* RFS specific error */
#define	EBADMSG		74	/* Not a data message */
#define	EOVERFLOW	75	/* Value too large for defined data type */
#define	ENOTUNIQ	76	/* Name not unique on network */
#define	EBADFD		77	/* File descriptor in bad state */
#define	EREMCHG		78	/* Remote address changed */
#define	ELIBACC		79	/* Can not access a needed shared library */
#define	ELIBBAD		80	/* Accessing a corrupted shared library */
#define	ELIBSCN		81	/* .lib section in a.out corrupted */
#define	ELIBMAX		82	/* Attempting to link in too many shared libraries */
#define	ELIBEXEC	83	/* Cannot exec a shared library directly */
#define	EILSEQ		84	/* Illegal byte sequence */
#define	ERESTART	85	/* Interrupted system call should be restarted */
#define	ESTRPIPE	86	/* Streams pipe error */
#define	EUSERS		87	/* Too many users */
#define	ENOTSOCK	88	/* Socket operation on non-socket */
#define	EDESTADDRREQ	89	/* Destination address required */
#define	EMSGSIZE	90	/* Message too long */
#define	EPROTOTYPE	91	/* Protocol wrong type for socket */
#define	ENOPROTOOPT	92	/* Protocol not available */
#define	EPROTONOSUPPORT	93	/* Protocol not supported */
#define	ESOCKTNOSUPPORT	94	/* Socket type not supported */
#define	EOPNOTSUPP	95	/* Operation not supported on transport endpoint */
#define	EPFNOSUPPORT	96	/* Protocol family not supported */
#define	EAFNOSUPPORT	97	/* Address family not supported by protocol */
#define	EADDRINUSE	98	/* Address already in use */
#define	EADDRNOTAVAIL	99	/* Cannot assign requested address */
#define	ENETDOWN	100	/* Network is down */
#define	ENETUNREACH	101	/* Network is unreachable */
#define	ENETRESET	102	/* Network dropped connection because of reset */
#define	ECONNABORTED	103	/* Software caused connection abort */
#define	ECONNRESET	104	/* Connection reset by peer */
#define	ENOBUFS		105	/* No buffer space available */
#define	EISCONN		106	/* Transport endpoint is already connected */
#define	ENOTCONN	107	/* Transport endpoint is not connected */
#define	ESHUTDOWN	108	/* Cannot send after transport endpoint shutdown */
#define	ETOOMANYREFS	109	/* Too many references: cannot splice */
#define	ETIMEDOUT	110	/* Connection timed out */
#define	ECONNREFUSED	111	/* Connection refused */
#define	EHOSTDOWN	112	/* Host is down */
#define	EHOSTUNREACH	113	/* No route to host */
#define	EALREADY	114	/* Operation already in progress */
#define	EINPROGRESS	115	/* Operation now in progress */
#define	ESTALE		116	/* Stale NFS file handle */
#define	EUCLEAN		117	/* Structure needs cleaning */
#define	ENOTNAM		118	/* Not a XENIX named type file */
#define	ENAVAIL		119	/* No XENIX semaphores available */
#define	EISNAM		120	/* Is a named type file */
#define	EREMOTEIO	121	/* Remote I/O error */
#define	EDQUOT		122	/* Quota exceeded */
#define	ENOMEDIUM	123	/* No medium found */
#define	EMEDIUMTYPE	124	/* Wrong medium type */

//little endian
#define getu16l(x) ( ((*((unsigned char *)(x))) ) | (*((unsigned char *)(x) + 1) << 8) ) 
#define setu16l(x,y) do { *(unsigned char *)(x) = (y) & 0xff ;  *((unsigned char *)(x) + 1) = (y) >> 8 ; }while(0)
#define getu32l(x) ( ((*((unsigned char *)(x))) ) | (*((unsigned char *)(x) + 1) << 8) | (*((unsigned char *)(x) + 2) << 16) | (*((unsigned char *)(x) + 3) << 24) ) 
#define setu32l(x,y) do { *(unsigned char *)(x) = (y) & 0xff ;  *((unsigned char *)(x) + 1) = ((y) >> 8) & 0xff ;  *((unsigned char *)(x) + 2) = ((y) >> 16) & 0xff ; *((unsigned char *)(x) + 3) = ((y) >> 24) & 0xff ;}while(0)

//big endian
#define getu16b(x) ( ((*((unsigned char *)(x) + 1)) ) | (*((unsigned char *)(x)) << 8) ) 
#define setu16b(x,y) do { *((unsigned char *)(x) + 1 ) = (y) & 0xff ;  *((unsigned char *)(x)) = (y) >> 8 ; }while(0)
#define getu32b(x) ( ((*((unsigned char *)(x) + 3 )) ) | (*((unsigned char *)(x) + 2) << 8) | (*((unsigned char *)(x) + 1) << 16) | (*((unsigned char *)(x)) << 24) ) 
#define setu32b(x,y) do { *((unsigned char *)(x) + 3) = (y) & 0xff ;  *((unsigned char *)(x) + 2) = ((y) >> 8) & 0xff ;  *((unsigned char *)(x) + 1) = ((y) >> 16) & 0xff ; *((unsigned char *)(x) ) = ((y) >> 24) & 0xff ;}while(0)

/* @} */

typedef enum {
    VIDEO_SIZE_720P = (1280 << 16) | 720,
    VIDEO_SIZE_960P = (1280 << 16) | 960,
    VIDEO_SIZE_1088P = (1920 << 16) | 1088,
} VIDEO_SIZE;

#define __sensor_cfg      __attribute__ ((__section__ (".sensor_cfg")))

#define __adecoder_cfg      __attribute__ ((__section__ (".adecoder_cfg")))
#define __aencoder_cfg      __attribute__ ((__section__ (".aencoder_cfg")))

#define __ba2app_cfg      __attribute__ ((__section__ (".ba2app_cfg")))
#define __aencpre_cfg      __attribute__ ((__section__ (".aencpre_cfg")))
#define __adecpost_cfg      __attribute__ ((__section__ (".adecpost_cfg")))

#define __unused      __attribute__ ((unused))

extern u32 sensor_cfg_start;
extern u32 sensor_cfg_end;

#define RTOS_BSS_CLEAR() \
do{ \
	extern u32 rtos_bss_start, rtos_bss_end; \
	unsigned int *addr; \
	for(addr = &rtos_bss_start; addr < &rtos_bss_end; addr ++) { \
		*((volatile unsigned int *)addr) = 0; \
	} \
}while(0)

static inline int ff1(unsigned int x)
{
	int n;
	asm("b.ff1	%0,%1": "=r" (n) : "r" (x));
	return n;
}


#endif /* _DATA_TYPE_H_ */
