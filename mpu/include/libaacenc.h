#ifndef _LIBAACENC_H_
#define _LIBAACENC_H_

#include "includes.h"

#define aacenclib_printf debug_printf

/*-------------------------- defines --------------------------------------*/


/*-------------------- structure definitions ------------------------------*/
struct ST_AAC_ENCODER;

typedef  struct {
  int   SR;            /* audio file sample rate */
  int   bitRate;               /* encoder bit rate in bits/sec */
  short   nChIn;           /* number of channels on input (1,2) */
  short   nChOut;          /* number of channels on output (1,2) */
  short   bandWidth;             /* targeted audio bandwidth in Hz */
} AACEncoder_cfg;


/*-----------------------------------------------------------------------------

functionname: AacInitDefaultConfig
description:  gives reasonable default configuration
returns:      ---

------------------------------------------------------------------------------*/
void AacInitDefaultConfig(AACEncoder_cfg *config);

/*---------------------------------------------------------------------------

functionname:aacencoder_open
description: allocate and initialize a new encoder instance
returns:     AACENC_OK if success

---------------------------------------------------------------------------*/

short  aacencoder_open (struct ST_AAC_ENCODER**     phAacEnc,       /* pointer to an encoder handle, initialized on return */
                    const  AACEncoder_cfg    * config);        /* pre-initialized config struct */

short aacencoder_process(struct ST_AAC_ENCODER *pAACEncoder,
                    short             *timeSignal,
                    const unsigned char       *ancBytes,      /*!< pointer to ancillary data bytes */
                    short             *numAncBytes,   /*!< number of ancillary Data Bytes, send as fill element  */
                    unsigned char             *outBytes,      /*!< pointer to output buffer            */
                    int             *numOutBytes    /*!< number of bytes in output buffer */
                    );

/*---------------------------------------------------------------------------

functionname:aacencoder_close
description: deallocate an encoder instance

---------------------------------------------------------------------------*/

void aacencoder_close (struct ST_AAC_ENCODER* pAACEncoder); /* an encoder handle */

/* both MONO/STEREO use MAX_CHANNELS as 2, because for ROM code */
#define MAX_CHANNELS        2

#define QMF_CHANNELS                            64
#define QMF_TIME_SLOTS                          32

#define BLOCK_SWITCHING_OFFSET		   (1*1024+3*128+64+128)

#define AACENC_BLOCKSIZE    1024   /*! encoder only takes BLOCKSIZE samples at a time */
#define FRAME_LEN_LONG    AACENC_BLOCKSIZE

#define MAX_NO_OF_GROUPS 4
#define MAX_SFB_SHORT   15  /* 15 for a memory optimized implementation, maybe 16 for convenient debugging */
#define MAX_SFB_LONG    51  /* 51 for a memory optimized implementation, maybe 64 for convenient debugging */
#define MAX_SFB         (MAX_SFB_SHORT > MAX_SFB_LONG ? MAX_SFB_SHORT : MAX_SFB_LONG)   /* = MAX_SFB_LONG */
#define MAX_GROUPED_SFB (MAX_NO_OF_GROUPS*MAX_SFB_SHORT > MAX_SFB_LONG ? \
                         MAX_NO_OF_GROUPS*MAX_SFB_SHORT : MAX_SFB_LONG)

extern s32 * mdct_spectra;
extern s32 * scrtch_TNS;
extern s16 * mdct_dbuffer;
extern s16 * quant_spectra;
extern s16 * scf;
extern u16 * sfb_maxval;
extern s16 * long_sideinfo_table;
extern s16 * short_sideinfo_table;

#endif
