#ifndef _ISP_SDE_H_
#define _ISP_SDE_H_

typedef enum ISPSDE
{
	NOSDE = 0,
	MONOCHROME,
	NEGATIVE,
	SEPIA,
	SKETCH,
	WATERCOLOR,
	INK,
	AQUA,
	BLACKBOARD,
	CARTOON,
	COLORINK,
	POSTER,
	SOLARIZE,
	WHITEBOARD,
}ISPSDE;

u32 isp_set_saturation(int level);
u32 isp_set_contrast(int level);
u32 isp_set_sde(int level);
u32 isp_set_ae_target(t_dpm_ispidc *dpm_ispidc,int level);
u32 isp_set_lightfreq(int level);
u32 isp_set_brightness(int level);
u32 isp_set_awb_manual(int enable);
u32 isp_get_awb_b_gain(void);
u32 isp_get_awb_r_gain(void);
u32 isp_get_awb_g_gain(void);
u32 isp_get_hist_buf(t_dpm_ispidc * dpm);
int isp_set_roi(t_dpm_ispidc * dpm, u32 addr);
u32 isp_set_wdr(int level);
u32 isp_set_mctf(int level);
u32 isp_set_sharpness(int level);
#endif
