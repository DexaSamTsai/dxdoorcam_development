#ifndef __LIBA_ENC_INTERNAL__
#define __LIBA_ENC_INTERNAL__

#include "liba.h"
#include "liba_common.h"

typedef enum{
	AENC_STATUS_IDLE,
	AENC_STATUS_READY
}E_AENC_STATUS;

extern E_AENC_STATUS g_aenc_sta;

/**
 * @defgroup liba_enc_algorithm
 * @ingroup liba_encode
 * @brief library for audio encoder algorithm, called by module \ref liba_enc "liba_enc"
 * @details this module includes several audio compress format like liba_epost_f1,liba_epost_f2,...,liba_epost_f6. For each audio format, a \ref t_aencoder_cfg "t_aencoder_cfg" type global structure variable are implemented to be called by liba_enc. For example,\ref t_aencoder_cfg "t_aencoder_cfg"  g_aencoder_f1_cfg is implemented in liba_epost_f1 module, liba_enc will call g_aencoder_f1_cfg.init() to initialize f1 encorder and call g_aencoder_f1_cfg.process() to encord f1 bitstream.User can use menuconfig to choose what audio format algorithm be linked into the application, you can select one or more of the algorithms. 
 * @{
 */

typedef struct s_aencoder_cfg{
	E_AFMT afmt;///< audio decoder and encoder format
	u32 infrmsz;
	u32 outfrmsz; //the possible max out frame size
/**
 * @brief audio encoder init
 * @param frmsz: param to configure algo's input frmsz, if 0, use default frmsz
 * @return 0:ok, <0:err
 */
	int (*init)(u32 frmsz);
	
/**
 * @brief audio frame encoding entry. 
 * @details called by liba_enc_2 module
 * @param inbuf: input pcm buffer
 * @param *len: input pcm data length and left pcm data lenght after encoding
 * @param outbuf: output encoded bitstream data buffer
 * @return encoded frame length
 */
	int (*process)(void *inbuf, int* len, void *outbuf);
}t_aencoder_cfg; 

typedef struct s_aencpre_cfg{
	E_AEPRE_ALGO alg;///< audio encoder preprocess algorithm
	void *p_usrdef0;   ///< user define pointer, for some specific usage
	void *p_usrdef1;   ///< user define pointer, for some specific usage
/**
 * @brief audio encoder preprocess init
 * @return 0:ok, <0:err
 */
	int (*pre_init)(void);
	
/**
 * @brief audio encoder preprocess entry. 
 * @details
 * @param inbuf: input pcm buffer
 * @param len:  data length in inbuf, bytes
 * @return void
 */
	void (*pre_process)(void *buf, int len);

	int (*pre_exit)(void);
}t_aencpre_cfg; 



/** @} */ //end liba_enc_algorithm

typedef struct s_liba_ecfg{
	t_arec_inpara inp;

	u32 ai_sr;          ///<AI's lrck clock
	u32 skipcount;		///< audio skipping count
	//AI buffer, input buf configuration
	u32 AFI_BUF0;///< input audio PCM buffer0 address
	u32 AFI_BUF1;///< input audio PCM buffer1 address
	u32 AFI_BUFLEN; ///< length of one input PCM buffer, in short


	//pre-process configuration
	int (*pre_init)(void);		///< preprocess init callback, NULL to disable preprocess
	void (*pre_process)(void * buf, int len); ///< preprocess callback, NULL to disable preprocess. parameter "int len" is in byte; parameter "void *buf" is the input and output buffer for the process, if stereo, this callback should process each channel respectively,first half in buf is left channel data, and the second half is right channel data
	int (*aec_init)(void);		///< preprocess init callback, NULL to disable preprocess
	void (*aec_process)(void *out, void *in, void *ref, int len); ///< preprocess callback, NULL to disable preprocess. parameter "int len" is in byte; parameter "void *buf" is the input and output buffer for the process, if stereo, this callback should process each channel respectively,first half in buf is left channel data, and the second half is right channel data
	int (*aec_exit)(void);
	void (*control_volume)(void *buf,u32 len,s32 vol);///<function pointer for recorder volume software control	
	u32 AGCSThreshold; ///<AGC speaking threshold to identify the start of voice
	u32 AGCLevel;   ///<AGC level, to control the sound level, it should be less than 32767,recomend 12000 

	void (*dc_flush_all_proc)(void); 	///< flush d-cache callback
	void (*dc_invalidate_all_proc)(void);	///< invalidate d-cache callback

	u32 reftm;
	u32 in_clk;
	//shared buffers between BA1 and BA2
	u32 outbuf_addr;	///< output audio encoded data linklist buffer address
	u32 outbuf_size;		///< length of each linklist buffer, in bytes

	t_cbuf_ctrl ibuf;

	FRAMELIST *list;

//	int (*cb_fifo_list_add)(AFRAME_LIST * pframe, t_fifo_cfg * cfg);  ///< callback function that copy audio frame to sdram fifo and add it to fifo list.
//	t_fifo_cfg * flist_cfg;     ///< pointer to structure t_fifo_cfg;
}t_liba_ecfg;

extern t_liba_ecfg g_liba_ecfg;

#define AD_BUF_SIZE  AI_PINGPONGBUF_SIZE  
#ifdef I2SIRQ_MEMCOPY
#define PCM_VS_AD 16
#else
#define PCM_VS_AD 3
#endif
#define PCMIN_BUF_SIZE (AD_BUF_SIZE*PCM_VS_AD)
#define OUTBUF_ONELEN 1024
#define MAX_INBUFLEN  (1152*2)  //currently AF3's inbuf length is 1152*2, which is the biggest

#if 0
#define AREC_ALLOCATE_STEREO(outlist_num,outbuflen)	\
static  u8 __attribute__  ((aligned(4))) g_ad_ppbuf[AD_BUF_SIZE*2];	\
static  u8 __attribute__  ((aligned(4))) g_pcm_in_cbuf[PCMIN_BUF_SIZE];	\
u8 __attribute__ ((aligned(4))) g_arec_framelist_buf[outbuflen];	\
static int arec_outbuflen = outbuflen; \
FRAMELIST_DECLARE(areclist, outlist_num)

#define AREC_CONFIG_STEREO()	\
{\
g_liba_ecfg.AFI_BUF0 = ((int)&g_ad_ppbuf[0]|BIT31);	\
g_liba_ecfg.AFI_BUF1 = ((int)&g_ad_ppbuf[AD_BUF_SIZE]|BIT31);	\
g_liba_ecfg.outbuf_addr = (u32)g_arec_framelist_buf|BIT31;	\
g_liba_ecfg.outbuf_size = arec_outbuflen;	\
g_liba_ecfg.AFI_BUFLEN = AD_BUF_SIZE;	\
g_liba_ecfg.list = &areclist;	\
g_liba_ecfg.ibuf_start = (u32)&g_pcm_in_cbuf;\
g_liba_ecfg.ibuf_len = PCMIN_BUF_SIZE;\
g_liba_ecfg.ibuf_wp = AD_BUF_SIZE;\
g_liba_ecfg.ibuf_rp = 0;\
memset(g_pcm_in_cbuf,0,sizeof(g_pcm_in_cbuf));\
_AREC_CONFIG_DCACHE \
}
#else

#define AREC_ALLOCATE_STEREO	\
static  u8 __attribute__  ((aligned(4))) g_pcm_in_cbuf[PCMIN_BUF_SIZE];

#define AREC_CONFIG_STEREO	\
	.AFI_BUFLEN = AD_BUF_SIZE,	\
	.ibuf.start = (u32)g_pcm_in_cbuf,\
	.ibuf.len = PCMIN_BUF_SIZE,\
	.ibuf.wp = 0,\
	.ibuf.rp = 0,


#endif







/**
 * @REAL_FORMAT void AREC_CONFIG_EPRE_PROCESS(epre_init,epre_process);
 * @brief  config preprocess callback functions when record
 * @details set two functions to the pre_init and pre_process function pointer
 * @param  epre_init: init callback function
 * @param  epre_process: process callback function
 * @return void
 */
//INTERNAL_START
#define AREC_CONFIG_EPRE_PROCESS(epre_init,epre_process)\
{\
g_liba_ecfg.pre_init = epre_init;	\
g_liba_ecfg.pre_process = epre_process; \
}
//INTERNAL_END

/**
 * @REAL_FORMAT void AREC_CONFIG_AGC(agc_threshold,agc_level);
 * @brief  config agc parameters 
 * @details set speaking threshold and sound level need by the AGC algorithm.  
 * @param  agc_threshold: speaking threshold to identify the start of voice 
 * @param  agc_level: AGC output sound level,should be less than 32767, 12000 recommended 
 * @return void
 */
//INTERNAL_START
#define AREC_CONFIG_AGC(agc_threshold, agc_level)\
{\
g_liba_ecfg.AGCSThreshold = agc_threshold;\
g_liba_ecfg.AGCLevel = agc_level;\
}
//INTERNAL_END


/**
 * @REAL_FORMAT void AREC_CONFIG_MONO(int sr,in amp,in denoise);
 * @brief  config g_liba_ecfg for MONO record
 * @details initialize g_liba_ecfg using input parameter and buffers/variable defined in AREC_ALLOCATE_MONO
 * @param  sr: sample rate
 * @param  amp: analag gain in ADC
 * @param  denoise: no use, kept for API compatible
 * @return void
 */
//INTERNAL_START
#ifdef CONFIG_DCACHE
#define _AREC_CONFIG_DCACHE \
	g_liba_ecfg.dc_flush_all_proc = dc_flush_all; \
	g_liba_ecfg.dc_invalidate_all_proc = dc_invalidate_all;
#else
#define _AREC_CONFIG_DCACHE 
#endif

//INTERNAL_END


/**
 * @defgroup liba_ehw
 * @ingroup liba_encode
 * @brief library for audio record hardware driver. Support external audio CODEC and digital microphone. So, there are 2 sub-module driver for application selecting. This module called by \ref liba_enc "liba_enc".
* @{
*/

/**
 * @brief init audio record hardware
 * @details init audio hardware configure. MasterBA call this API to config BA2 in idle status
 * @return 0:ok, <0:err
 */
int liba_ehw_init(void);

/**
 * @brief start audio record hardware
 * @details MasterBA call this API to config BA2 in start status
 * @return 0:ok, <0:err
 */
int liba_ehw_start(void);

/**
 * @brief stop audio record hardware
 * @details MasterBA call this API to config BA2 in stop status
 * @return 0:ok, <0:err
 */
int liba_ehw_stop(void);


/**
 * @brief get available PCM buffer address
 * @details called by BA2, since total audio encoding flow is runing on BA2
 * @return 0:no data, >0:return data address
 */
u32 liba_ehw_data_update(void);
/** @} */ //end liba_ehw

//INTERNAL_END

/**
 * @brief init audio encoding
 * @details init func, please memset g_liba_ecfg to zero, and set necessary paramaters in g_liba_ecfg
 * @return 0:ok, <0:err
 */
int libaenc_init(void);

/**
 * @brief start audio encoding
 * @return 0:ok, <0:err
 */
int libaenc_start(void);

/**
 * @brief stop audio encoding
 * @return 0:ok, <0:err
 */
int libaenc_stop(void);

/**
 * @brief clear all buffer used by audio encoding
 * @return 0:ok, <0:err
 */
int libaenc_exit(void);

 /**
 * @brief remove one frame from output linklist(update linklist)
 * @details when the frame in the linklist head is taken away,it should be remove from the linklist and move the head pointer to the next frame.
 * @return 0 :ok
 */
int libaenc_update(void);

/**
 * @brief encode audio frame and update output buffer list
 * @details Main task can pend on g_liba_cfg.evt_input, after receive the signal, call liba_update_input. It is called by audio encoding flow control module running on BA2.
 * @return 0:ok, <0:err
 */
extern int libaenc_update_input_internal(void);

/**
 * @brief a fake API to encode audio frame and update output buffer list. In order to compatible with former API. This is called by application.
 * @return 0:ok, <0:err
 */
int libaenc_update_input(void);

/**
 * @brief audio record interrupt handler
 * @details Two buffers are used as pingpong buffer to store PCM AI transfered from ADC when recording. when one buffer is used by AI transfering PCM, the other buffer is used as the input buffer for libaenc_update_input(). Each time the buffer to which AI is transfering PCM is full, an interrupt will be generated, this function (libaenc_irq()) is called by except.c to switch the two pingpong buffer. 
 */ 
void libaenc_irq(void);

/**
 * @brief get liba used frame list count
 * @details call the function to calculate the total audio frame count. 
 */
int libaenc_afrmlist_used_get(void);

/**
 * @brief get one available encoded audio frame from linklist
 * @param frm_addr: available encoded audio frame buffer address
 * @param frm_len: available encoded audio frame size, in byte.
 * @return 0:ok, <0: err
 */
FRAME* libaenc_get_frm(void);

int libaenc_remove_frm(FRAME *frm);

u32 libaenc_get_reftm(void);

/**
 * @brief set audio volume
 * @return void
 */
int libaenc_volume_set(int volume);

/**
 * @brief update oldfrm_num
 * @return void
 */
void libaenc_update_max_frm_cnt(void);

/**
 * @brief get audio framelist head
 * @return return audio framelist head
 */
//S_AREC_FRMLIST_NODE* libaenc_get_afrmlist_head(void);
/** @} */ //end liba_enc

/**
 * @defgroup liba_epre
 * @ingroup liba_encode
 * @brief library for audio pre-process algorithm.
 * @details  
 * -# <b>Source Code:</b>
 	* -# share/liba_epre/liba_epre.c for encoding pre-process APIs
	* -# share/liba_epre/audioprolib/ pre-process algorithm library
 * @{
 */
/**
 * @brief audio encode pre-process init
 * @return 0:ok, <0: error
 */
int liba_epre_init(void);

/**
 * @brief audio encode pre-process handler
 * @param buf : input data buffer address 
 * @param len : input data size in byte
 */
void * liba_epre_process(void *buf, int len);

/**
 * @REAL_FORMAT void liba_fifo_enable(t_fifo_cfg * cfg);
 * @brief  enable sdram fifo.
 * @details copy audio frame to sdram fifo, then add it to fifo list. 
 */
//INTERNAL_START
#define liba_fifo_enable(cfg) do{ \
	g_liba_ecfg.cb_fifo_list_add = audio_fifo_list_add; \
	g_liba_ecfg.flist_cfg = cfg; \
}while(0);
//INTERNAL_END

/** @} */ //end liba_epre



#endif /// __LIBA_EHW_H__
