#ifndef _KCMD_H_
#define _KCMD_H_

//INTERNAL_START
typedef enum {
    RECORDER_CMD_QUEUE_STA_EMPTY = 1,
    RECORDER_CMD_QUEUE_STA_HAS_DATA,
    RECORDER_CMD_QUEUE_STA_FULL,
} RECORDER_CMD_QUEUE_STA;

typedef u32 LIBCMD_LIST[];
//INTERNAL_END

/**
 * @defgroup lib_cmd
 * @brief this is command library.
 * @ingroup lib_newos
 * @details
    command list is a list of commands, application can post(add) command to the list, and get(del) command from the list. if list full, command post will be failed, so commands will not be overwritten in list.<br>
    each command has 4 bytes id and 4 bytes param.
 <p>
 * -# <b>Sample Codes :</b>
   * -# one task define the cmdlist and wait for all commands.
\code
DECLARE_CMDLIST(test_cmdlist, 5)

u32 id;
u32 param;
while(1){
	if(libcmd_get(test_cmdlist, &id, &param) == 0){
		//TODO: handle id/param
	}
}
\endcode
   * -# other tasks send commands
\code
extern LIBCMD_LIST test_cmdlist;
if(libcmd_post(test_cmdlist, CMDID_TEST, CMDPARAM_TEST) != 0){
	debug_printf("commands overflow\n");
}
\endcode
 * -# <b>Notes:</b>
   * -# in most cases, libcmd work with t_signal(see lib_newos), one task wait for signal, and handle all commands
        and other tasks or interrupt send commands, and send signal to that tasks.
 * @{
 */

/**
 * @REAL_FORMAT DECLARE_CMDLIST(cmdlist, MAX_CMD_NUM);
 * @details declare command list
 * @param cmdlist cmdlist name for libcmd_get() and libcmd_post(), it's LIBCMD_LIST type.
 * @param MAX_CMD_NUM max command number in list
 */
//INTERNAL_START
#define DECLARE_CMDLIST(cmdlist, MAX_CMD_NUM) \
	u32 cmdlist[2*MAX_CMD_NUM+1] = { ((RECORDER_CMD_QUEUE_STA_EMPTY) << 8) | (MAX_CMD_NUM), 0 };
//INTERNAL_END

/**
 * REAL_FORMAT void libcmd_reset(u32 *cmdlist);
 * @details reset cmdlist to no command
 */
#define libcmd_reset(cmdlist) cmdlist[0] = ((RECORDER_CMD_QUEUE_STA_EMPTY) << 8) | (((u8 *)(cmdlist))[3]);

/** @} */

//INTERNAL_START
//to compatible with old codes
#define recorder_get_cmd libcmd_get
#define recorder_post_cmd libcmd_post
//INTERNAL_END

#endif
