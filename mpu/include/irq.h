#ifndef _IRQ_H_
#define _IRQ_H_

typedef int (*t_irq_handler)(void *);

/**
 * @brief 注册中断处理函数
 * @param irq 中断号
 * @param handler 中断处理函数
 * @param arg 传递给中断处理函数的参数
 * @return 0:success <0:failed
 */
int irq_request(unsigned int irq, t_irq_handler handler, void * arg);

/**
 * @brief 删除中断处理函数
 * @param irq 中断号
 * @return 0:success <0:failed
 */
int irq_free(unsigned int irq);

#endif
