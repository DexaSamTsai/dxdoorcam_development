/** @file pool_api.h
 * pool_api header file
 *
 * @author Jerry Yuan
 * @date Oct. 29, 2008
 */

#ifndef __MEM_POOL_H__
#define __MEM_POOL_H__

/**
 * @defgroup pool_api
 * @brief pool module APIs, it's the interface of decode module, just used for demuxer test. 
 * @{
 */

/**
 * The definition of pool element, it jsut has a pointer points to the next pool element. This pointer reuses the data buffer for the pool element
 */
typedef struct _t_pool_element
{
	struct _t_pool_element *next; 
} t_pool_element;

/**
 * The Pool structure definition. The main structure for the pool. If the application wants to create a pool, it will call pool_init() function, then it will get a pointer to this structure. The application will use this to indentify a pool. The property of the pool is in this structure: the *head pointer of the pool; the buffer list; the element_size etc.
 */
typedef struct _t_pool
{
	t_pool_element *head;       ///< the head pointer of the pool
	u32 element_size;           ///< the element size of the pool
	u32 element_total_cnt;      ///< the total element count of the pool
	u32 element_ref_cnt;	       ///< the referenced element count of the pool, pool_element_alloc() is called, this value will be increased by 1; while pool_element_free() is called, this value will be decreased by 1.
} t_pool;

/** Get the overhead of the pool, it is used for static pool when external module calculate the memory requirment for the pool  */
#define POOL_OVERHEAD_SIZE  (sizeof(t_pool))

/**
 * Pool initial function.
 *
 * @param element_size : the size of the element of the pool.
 * @param element_init_count : the initial count of the pool, the fucntion will try to get this amount of the elements for the pool.
 * @param pbuf : the start address of buffer which is used to contain the pool, The size of buffer should be at least (element_size*element_init_count + pool_header_size), the pool_header_size can be obtained from malloc GET_POOL_OVERHEAD_SIZE
 *
 * @return the pointer to t_pool structure, it's used to identify a pool.
 */
//t_pool *pool_init(u32 element_size, u32 element_init_count, u8 *pbuf);


/**
 * Get a element from the specified pool
 *
 * @param pool : the pointer to the pool which you wanna to get the element.
 *
 * @return the pointer to the element
 */
//void *pool_element_alloc(t_pool *pool);

/**
 * Release a element to the specified pool
 *
 * @param pool : the pointer to the pool which you wanna to release the element.
 * @param element : the pointer to the element
 *
 * @return none
 */
//void pool_element_free(t_pool *pool, void *element);


/** @} */	//end of pool_api group

#endif	// __MEM_POOL_H__
