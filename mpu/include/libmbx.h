#ifndef _LIBSEC_H_
#define _LIBSEC_H_

void libmbx_s2m(u32 data);
u32 libmbx_s_read(void);
u32 libmbx_s_getintsta(void);

#endif
