#ifndef _LIBAV_FORMAT_H_
#define _LIBAV_FORMAT_H_

#define FLV_HEADER_MAX_SIZE  32
typedef struct s_VIDEO_INFO{
    u8 *sps;
    u8 *pps;
    s32 sps_size;
    s32 pps_size;
    s32 width;
    s32 height;
    s32 framerate;
}VIDEO_INFO;

typedef struct s_AUDIO_INFO{
	u8 channels;
	enum CodecID codec_id; 
	u32 bitrate;
	u32 samplerate;
	u32 samplesize;
	int audio_flags;
	u32 codec_tag;
}AUDIO_INFO;

enum {
    AV_FRAME_VIDEO,
    AV_FRAME_AUDIO
};

enum {
    AV_FORMAT_FLV,
    AV_FORMAT_AVI,
    AV_FORMAT_STREAM
};

extern s32 wc_file_write(File *file, int size, u8 *buf);

/* flv file */
void rec_wr_flv_param_add(void *ap, void *vp);
s32 rec_wr_flv_hdr(File *pfile, u8 *buf);
s32 rec_wr_flv_payload(File *pfile, s32 av_flag, void *avframe);
void rec_wr_flv_close(File *pfile, u8 *file_name);

/* stream file */
s32 rec_wr_stream_payload(File *pfile, s32 av_flag, void *avframe);
void rec_wr_stream_close(File *pfile, u8 *file_name);


/* API: */
typedef struct
{
    void (*param_add)(int av_flag, void *avparam);
    s32 (*wr_hdr)(File *pfile, u8 *buf);
    s32 (*wr_payload)(File *pfile, s32 av_flag, void *avframe);
    void (*close)(File *pfile, u8 *file_name);
}t_recfile_hdl;

s32 rec_file_open(File *pfile, u8 *file_name, u8 format, void *aparam, void *vparam);

s32 rec_file_write(File *pfile, s32 av_flag, void *avframe);

void rec_file_close(File *pfile, u8 *file_name);

#endif
