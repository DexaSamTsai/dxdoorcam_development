#ifndef _MPU_TICK_H_
#define _MPU_TICK_H_

/**
 * @defgroup libtick
 * @brief Tick library.
 * @details this library handle the Tick, include Tick handler, set Tick and so on.
 <p>
 * -# <b>Depends On</b>:
 * -# <b>Used By</b>:
   * -# Tick1 is used by \ref lib_newos
 * -# <b>Design Doc</b>:
   * -# there are two Tick Timers in this chip, The Tick Timer is used to schedule operating system and user tasks on regular time basis or as a high precision time reference
   * -# when call tickX_set_YY(), the auto-incremented with each clock cycle counter is started from zero.
   * -# when the counter match 'cycle' in the argument, match event happens
   * -# each timer has three working mode:
     * -# single-run mode: counter is stopped when match event happens
     * -# restart mode: counter restarts counting from zero when a match event happens
     * -# continuous mode: counter keeps counting even when match event happens
   * -# when match event happens, interrupt(exception) will be triggered,
     * -# for tick1, except_tick() function will be called.
     * -# for tick2, tick2_irq_handler() function will be called.
 * -# <b>Sample codes</b>
    * -# tick2 250us sample
\code
    void main()
	{
		tick2_set_rt(sysclk_get() / 4000);
	}

	unsigned log tick2_irq_handler(void)
	{
		tick2_clean();
		return 0;
	}
\endcode
 * -# <b>NOTES</b>
 * -# <b>FAQ</b>
 * @ingroup lib_cpu
 * @{
 */

/**
 * @REAL_FORMAT tick1_set_sr(cycle)
 * @brief set tick1 single-run
 * @details in single-run mode, timer is stopped when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick1_set_sr(cycle) \
do{ \
	MTSPR(SPR_TTCR, 0); \
	MTSPR(SPR_TTMR, SPR_TTMR_SR | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END


/**
 * @REAL_FORMAT tick1_set_ct(cycle)
 * @brief set tick1 continuous mode
 * @details in continuous mode, timer keeps counting even when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick1_set_ct(cycle) \
do{ \
	MTSPR(SPR_TTCR, 0); \
	MTSPR(SPR_TTMR, SPR_TTMR_CR | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick1_set_rt(cycle)
 * @brief set tick1 restart mode
 * @details in restart mode, timer restarts counting from zero when a match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick1_set_rt(cycle) \
do{ \
	MTSPR(SPR_TTCR, 0); \
	MTSPR(SPR_TTMR, SPR_TTMR_RT | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick1_clean_disable()
 * @brief clean tick1 interrupt and disable
 * @details
*/
//INTERNAL_START
#define tick1_clean_disable() \
do{ \
	unsigned int sr; \
	MFSPR(SPR_TTMR, sr); \
	MTSPR(SPR_TTMR, sr & (~SPR_TTMR_IE) & (~SPR_TTMR_IP)); \
	MTSPR(SPR_TTCR, 0); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick1_clean()
 * @brief clean tick1 interrupt
 * @details
 *
*/
//INTERNAL_START
#define tick1_clean() \
do{ \
	unsigned int sr; \
	MFSPR(SPR_TTMR, sr); \
	MTSPR(SPR_TTMR, sr & (~SPR_TTMR_IP)); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT except_tick()
 * @brief tick1 exception callback
 * @details when tick1 interrupt triggered, this func will be called.<br>
            so this functions should be implemented in ram/xxx/
 *
*/
//INTERNAL_START
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_set_sr(cycle)
 * @brief set tick2 single-run
 * @details in single-run mode, timer is stopped when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick2_set_sr(cycle) \
do{ \
	MTSPR(SPR_TTCR1, 0); \
	MTSPR(SPR_TTMR1, SPR_TTMR_SR | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END


/**
 * @REAL_FORMAT tick2_set_ct(cycle)
 * @brief set tick2 continuous mode
 * @details in continuous mode, timer keeps counting even when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick2_set_ct(cycle) \
do{ \
	MTSPR(SPR_TTCR1, 0); \
	MTSPR(SPR_TTMR1, SPR_TTMR_CR | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_set_rt(cycle)
 * @brief set tick2 restart mode
 * @details in restart mode, timer restarts counting from zero when a match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick2_set_rt(cycle) \
do{ \
	MTSPR(SPR_TTCR1, 0); \
	MTSPR(SPR_TTMR1, SPR_TTMR_RT | SPR_TTMR_IE | (cycle) ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_clean_disable()
 * @brief clean tick2 interrupt and disable
 * @details
*/
//INTERNAL_START
#define tick2_clean_disable() \
do{ \
	unsigned int sr; \
	MFSPR(SPR_TTMR1, sr); \
	MTSPR(SPR_TTMR1, sr & (~SPR_TTMR_IE) & (~SPR_TTMR_IP)); \
	MTSPR(SPR_TTCR1, 0); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_clean()
 * @brief clean tick2 interrupt
 * @details
 *
*/
//INTERNAL_START
#define tick2_clean() \
do{ \
	unsigned int sr; \
	MFSPR(SPR_TTMR1, sr); \
	MTSPR(SPR_TTMR1, sr & (~SPR_TTMR_IP)); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_irq_handler()
 * @brief tick2 exception callback
 * @details when tick2 interrupt triggered, this func will be called.<br>
            so this functions should be implemented in ram/xxx/
 *
*/
//INTERNAL_START
//INTERNAL_END

/** @}*/

void sleepinit(void);
void msleep(int msec);
void usleep(int usec);

#endif
