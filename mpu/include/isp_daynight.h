#ifndef _ISP_DAYNIGHT_H_
#define _ISP_DAYNIGHT_H_

u32 isp_set_colormode(t_dpm_ispidc * dpm, int level);
u32 isp_set_auto_fps(t_dpm_ispidc * dpm, int fps_range);

int isp_daynight_auto_init(t_dpm_ispidc * dpm);
int isp_daynight_auto_check(t_dpm_ispidc * dpm);

#endif
