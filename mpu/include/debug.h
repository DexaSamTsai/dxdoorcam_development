#ifndef _DEBUG_H_
#define _DEBUG_H_

/**
 * @defgroup lib_debug 
 * @details this library is new debug APIs, and used to output debug information by different hardware. such as uart, usb and so on.<br><br>
 * new debug library supports to print out directly or save data in print buffer. Data could be outputted by hardware interrupt mode if save data in print buffer. 
 <p>
 * -# <b> Sample codes </b>
\code
	///< declare a debug print buffer in rom or application.
#define DECLARE_DEBUGPRINTF_BUF(512)

	///< call initialization function if print debug message directly, enable the flag DEBUG_PRINTF_FLAG_CRLF if want to convert '\n' to '\r\n'.But don't convert '\r\n' to '\r\r\n'.
	debug_printf_init(DEBUG_PRINTF_MODE_DIRECT|DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart, 0);
	///< call initialization function if flag is DEBUG_PRINTF_BUFWAIT
	debug_printf_init(DEBUG_PRINTF_BUFWAIT|DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart, 0);
	///< call initialization function if flag is DEBUG_PRINTF_TRUNCATE
	debug_printf_init(DEBUG_PRINTF_TRUNCATE|DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart, 0);
	///< call initialization function if flag is DEBUG_PRINTF_OVERWRITTEN
	debug_printf_init(DEBUG_PRINTF_OVERWRITTEN|DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart, 0);
	
	///< enable UART IRQ and call uart interrupt handler functionif flag is DEBUG_PRINTF_BUFWAIT or DEBUG_PRINTF_OVERWRITTEN
	enable_irq(UART);

\endcode
 * -# <b> FAQ: </b>
 	* -# Q: which flag should be set if print debug message to file?<br>
	* -# A: only set flag DEBUG_PRINTF_BUFWAIT and DEBUG_PRINTF_OVERWRITTEN.<br>
 * @{
 */

/**
 * @details debug flag
 */
enum
{
	DEBUG_PRINTF_MODE_DISABLE,
	DEBUG_PRINTF_MODE_STRING,
	DEBUG_PRINTF_MODE_DIRECT,       ///< print debug message directly, don't save in debug print buffer.
	DEBUG_PRINTF_MODE_TRUNCATE,     ///< the part of the newest debug message will be lost when print buffer is full, and show '....'
	DEBUG_PRINTF_FLAG_CRLF = 0x10000,
	DEBUG_PRINTF_FLAG_CFLUSH = 0x20000,
	DEBUG_PRINTF_FLAG_NOLFLUSH = 0x40000,
};



/**
 * @details debug print buffer structure
 */
typedef struct s_debugprintf_buf
{
	unsigned char * base;   ///< buffer base address
	int size;      ///< buffer size
	int w;         ///< write position
	int r;         ///< read position
	int remcnt;    ///< unprinted data count
}t_debugprintf_buf;

struct s_debugprintf;

/**
 * @brief debug hardware interface structure
 */
typedef struct s_debugpintf_hw
{
	int (*hw_init)(void *arg, struct s_debugprintf * dpcfg, void **hw_arg); ///< callback function for hardware initialization  
	int (*hw_output_buf)(void *hw_arg, struct s_debugprintf * dpcfg, unsigned char *buf, int len); ///< callback function for outputting data to file. return outputted length.
	int (*hw_exit)(void *hw_arg, struct s_debugprintf * dpcfg); ///< callback function for hardware exit
}t_debugprintf_hw;


/**
 * @brief debug structure
 */
typedef struct s_debugprintf
{
	u32 flag;                  ///< debug flag, see "DEBUG_PRINTF_XXXX"
	t_debugprintf_buf * dpbuf; ///< point to debug print buffer
	t_debugprintf_hw * hw;     ///< point to debug hardware
	u32 timeout;               ///< used in BUFWAIT
	u8 prev_crlg;
	void * hw_arg;             ///< for hw_xxx() callback function
	int (*cb_putchar)(struct s_debugprintf * dp, unsigned char c, int width); ///< callback function for print character. Nrmally, set it null. if there is bug in default putchar function, implement a new function.
}t_debugprintf;


//INTERNAL_START
extern const t_debugprintf_hw g_debugprintf_hw_uart;
extern const t_debugprintf_hw g_debugprintf_hw_uarts;
#define DEBUGPRINTF_MODE_VALID_VAL    (0xFFFF)
#define DEBUGPRINTF_FLAG_VALID_VAL    (0xFFFF0000)
#define DEBUGPRINTF_REDUNDANCY_WIDTH  (4)
#define DEBUGPRINTF_BUFWAIT_TIMEOUT   (0x10000)
//INTERNAL_END

/**
 * @REAL_FORMAT #define DECLARE_DEBUGPRINTF_BUF(bufsize) \
 * @brief declare the debug print buffer.
 * 
 * @param bufsize  buffer size. if buffer size is 0, print debug message directly
 */
/**
 * @REAL_FORMAT t_debugprintf g_debugprintf_cfg;
 */
//INTERNAL_START
#define DECLARE_DEBUGPRINTF_BUF(bufsize) \
static unsigned char dprintf_buf[bufsize+DEBUGPRINTF_REDUNDANCY_WIDTH]; \
t_debugprintf_buf g_dpbuf = {\
	.base = dprintf_buf,\
	.size = bufsize + DEBUGPRINTF_REDUNDANCY_WIDTH,\
	.w = 0,\
	.r = 0,\
	.remcnt = 0,\
};\
t_debugprintf g_debugprintf_cfg = {\
	.dpbuf = &g_dpbuf,\
	.timeout = DEBUGPRINTF_BUFWAIT_TIMEOUT,\
	.cb_putchar = NULL,\
};
extern t_debugprintf g_debugprintf_cfg;
//INTERNAL_END

/**
 @REAL_FORMAT int debug_printf_init(u32 flag, t_debugprintf_hw * hw, void * arg);
 *@brief  debug initialization function
 * 
 *@param dp point to debug data structure
 *@param flag debug flag, see "DEBUG_PRINTF_XXXX"
 *@param hw point to the debug hardware structure
 *@param arg hardware argument, if hardware is uart, arg is 0|1 for uar0/uart1, if hardware is file, arg is the filename
 *
 *@return 0 ok. < 0 failed
 */
//INTERNAL_START
int debug_printf_init_internal(t_debugprintf *dp_cfg, u32 flag, t_debugprintf_hw * hw, void * arg);
#define debug_printf_init(flag, hw, arg) debug_printf_init_internal(&g_debugprintf_cfg, flag, hw, arg)
//INTERNAL_END


/**
 *@brief change debug mode or switch to other debug hardware or 
 *@details the function is called in other tasks.
 *
 *@param flag debug flag, see "DEBUG_PRINTF_XXXX"
 *@param hw point to the debug hardware structure
 *@param arg hardware argument, if hardware is uart, arg is 0|1 for uar0/uart1, if hardware is file, arg is the filename
 *
 *@return 0 ok. < 0 failed
 */
int debug_printf_set(u32 flag, t_debugprintf_hw * hw, void * arg);

int debug_printf_getbuf(t_debugprintf * dp, unsigned char * buf, int maxlen);

/**
 * @REAL_FORMAT void debug_rom_on(void);
 * @breif print debug message of rom functions
 * @param none
 *
 * @return void
 */
/**
 * @REAL_FORMAT void debug_rom_off(void);
 * @breif disable the output of debug message of rom functions
 * @param none
 *
 * @return void
 */
/**
 * @REAL_FORMAT void debug_share_on(void);
 * @breif print debug message of share functions
 * @param none
 *
 * @return void
 */
/**
 * @REAL_FORMAT void debug_share_off(void);
 * @breif disable the output of debug message of share functions
 * @param none
 *
 * @return void
 */
/**
 * @REAL_FORMAT void debug_buf_on(void);
 * @breif use debug print buffer 
 * @param none
 *
 * @return void
 */
/**
 * @REAL_FORMAT void debug_buf_off(void);
 * @breif don't use debug print buffer
 * @param none
 *
 * @return void
 */
//INTERNAL_START
extern u32 debug_rom_level;
extern u32 debug_share_level;
#define debug_rom_on() do{debug_rom_level = 0;}while(0);
#define debug_rom_off() do{debug_rom_level = 1;}while(0);
#define debug_share_on() do{debug_share_level = 0;}while(0);
#define debug_share_off() do{debug_share_level = 1;}while(0);
//INTERNAL_END

/**
 *@brief print one character
 *@param c character
 * 
 *@return 0 ok ENOMEM buffer full
 */
int dputc(unsigned char c);

/**
 * @REAL_FORMAT int debug_printf(const char *fmt0, ...);
 * @brief print debug message 
 * @param fmt0  debug message
 * 
 * @return 0 ok <0 failed
 */
//INTERNAL_START
int dprintf(const char *fmt0, ...);
int vfprintf(t_debugprintf *dp, const char *fmt0, va_list ap);
int vfprintf_putchar(t_debugprintf * dp, unsigned char c, int width);
//INTERNAL_END

/**
 * @REAL_FORMAT int debug_printf_rom(const char *fmt0, ...);
 * @breif print the debug message of rom functions 
 * @param fmt0  debug message
 *
 * @return 0 ok <0 failed
 */
/**
 * @REAL_FORMAT int debug_printf_share(const char *fmt0, ...);
 * @breif print the debug message of share functions 
 * @param fmt0  debug message
 *
 * @return 0 ok <0 failed
 */
//INTERNAL_START
//#define debug_printf_null(...)

#define debug_printf_rom(fmt0...) ((debug_rom_level==0)?dprintf(fmt0):(0))
#define debug_putc_rom(c) ((debug_rom_level==0)?dputc(c):(0))

#define debug_printf_share(fmt0...) ((debug_share_level==0)?dprintf(fmt0):debug_printf_null(fmt0))

#ifndef _FILE_IN_ROM_
#define debug_printf dprintf
#define debug_putc dputc
#else
#define debug_printf debug_printf_rom
#define debug_putc debug_putc_rom
#endif
//INTERNAL_END

/** @} */ //end lib_debug
#endif
