#ifndef __LIBA_H__
#define __LIBA_H__
#include "liba_common.h"

#define AEC_BUF_NUM 8
#define AIPSC_BASE 24

#ifdef AI1REC_AI2PLAY
// record I2S register config
#define AI_REC_MODE_SET		AI1_MODE_SET		
#define AI_REC_CTL		AI1_CTL_REG
#define AI_REC_LADDR		AI1_REC_LADDR
#define AI_REC_RADDR		AI1_REC_RADDR
#define AI_REC_BUF_SET		AI1_BUF_SET
#define AI_REC_INT_STATUS	AI1_INT_STATUS
#define AI_REC_INT_MSK		AI1_INT_MSK
#define AI_REC_TAKE_EFFECT	AI1_TAKE_EFFECT

// play I2S register config
#define AI_PLAY_MODE_SET	AI2_MODE_SET			
#define AI_PLAY_CTL             AI2_CTL_REG     
#define AI_PLAY_LADDR           AI2_PLAY_LADDR   
#define AI_PLAY_RADDR           AI2_PLAY_RADDR   
#define AI_PLAY_BUF_SET         AI2_BUF_SET     
#define AI_PLAY_INT_STATUS      AI2_INT_STATUS  
#define AI_PLAY_INT_MSK         AI2_INT_MSK   
#define AI_PLAY_TAKE_EFFECT		AI2_TAKE_EFFECT

#else  //AI2REC_AI1PLAY

// record I2S register config
#define AI_REC_MODE_SET		AI2_MODE_SET	
#define AI_REC_CTL              AI2_CTL_REG     
#define AI_REC_LADDR            AI2_REC_LADDR   
#define AI_REC_RADDR            AI2_REC_RADDR   
#define AI_REC_BUF_SET          AI2_BUF_SET     
#define AI_REC_INT_STATUS       AI2_INT_STATUS  
#define AI_REC_INT_MSK          AI2_INT_MSK  
#define AI_REC_TAKE_EFFECT	AI2_TAKE_EFFECT

// play I2S register config
#define AI_PLAY_MODE_SET	AI1_MODE_SET		
#define AI_PLAY_CTL             AI1_CTL_REG     
#define AI_PLAY_LADDR           AI1_PLAY_LADDR  
#define AI_PLAY_RADDR           AI1_PLAY_RADDR  
#define AI_PLAY_BUF_SET         AI1_BUF_SET     
#define AI_PLAY_INT_STATUS      AI1_INT_STATUS  
#define AI_PLAY_INT_MSK         AI1_INT_MSK 
#define AI_PLAY_TAKE_EFFECT		AI1_TAKE_EFFECT
#endif

typedef struct{
	u32 start;
	u32 len;
	u32 rp;
	u32 wp;
	u32 seq;
}t_cbuf_ctrl;

typedef struct{
	u32 pos0;
	u32 len0;
	u32 pos1;
	u32 len1;
}t_cbuf_pos;


extern volatile t_cbuf_ctrl g_AECrefbuf;
extern volatile t_cbuf_ctrl g_AECinbuf;
extern volatile u32 g_aec_enabled;
void audio_hw_disable(u32 en_i2s,u32 en_mclk);
void audio_hw_enable(u32 en_i2s,u32 en_mclk);
s32 ai_set_sr(u32 sample_rate, u32 reg);
void liba_volume_control(void *pcm, u32 len,s32 dbvol);

void liba_cbuf_init(t_cbuf_ctrl *cbuf, u32 bufstart, u32 len);
void liba_cbuf_reset(t_cbuf_ctrl *cbuf);
s32 liba_cbuf_get_wpos(t_cbuf_ctrl *cbuf, u32 needspace, t_cbuf_pos *w);
s32 liba_cbuf_get_rpos(t_cbuf_ctrl *cbuf, u32 needsize,t_cbuf_pos *r);
void liba_cbuf_update_rpos(t_cbuf_ctrl *cbuf, u32 size);
void liba_cbuf_update_wpos(t_cbuf_ctrl *cbuf, u32 size);
int liba_cbuf_frame_aligned(t_cbuf_ctrl *cbuf);
u32 liba_cbuf_getdatalen(t_cbuf_ctrl *cbuf);
#endif /// __LIBA_H__

