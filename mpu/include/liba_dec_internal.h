#ifndef __LIBA_DEC_INTERNAL__
#define __LIBA_DEC_INTERNAL__

#include "liba.h"
#include "liba_common.h"

typedef enum{
	ADEC_STATUS_IDLE,
	ADEC_STATUS_READY
}E_ADEC_STATUS;

extern E_ADEC_STATUS g_adec_sta;

typedef struct s_adecoder_cfg{
	E_AFMT afmt;  ///< audio decoder and encoder format
	u32 outfrmsz;
	u32 infrmsz; // the possibe max in frame size

/**
 * @brief audio decoder algorithm init.
 * @details called by libadec_init().
 * @param frmsz: param to configure algo's output frmsz, if 0, use default frmsz
 * @return 0:ok; <0: init error
 */
	int (*init)(u32 frmsz);///<audio decoder algorithm init.

/**
 * @brief audio frame decoding entry. 
 * @details called by liba_dec module
 * @param bufout: PCM buffer address
 * @param out_len: as an input param, denotes the output pcm length(bytes numbers) to be decoded;as an output param, denotes the real output pcm length 
 * @param bufin: audio bitstream buffer address                
 * @param in_len: as an input param, denotes the input bitsteam length in the buffer;as an output param, denotes the left bitstream length not decoded
 * @return 0:ok, -1:overflow
 */
	int (*process)(void *bufout,u32 *out_len, void *bufin, u32 *in_len);
}t_adecoder_cfg;

typedef struct s_adecpost_cfg{
	E_ADPOST_ALGO alg;///< audio encoder preprocess algorithm

/**
 * @brief audio decoder postprocess init
 * @return 0:ok, <0:err
 */
	int (*post_init)(void);
	
/**
 * @brief audio decoder postprocess entry. 
 * @details
 * @param buf: input pcm buffer
 * @param len:  data length in inbuf, bytes
 * @return void
 */
	void (*post_process)(void *buf, int len);

//	int (*post_exit)(void);
}t_adecpost_cfg; 


typedef struct s_liba_dcfg{
	t_adec_inpara inp;

	u32 ai_sr;          ///<AI lrck clock
	u8 bps;				///< audio bps : support 8bit or 16bit, if use internal DAC, set 8 bits, else usually set 16 bits
	//audio interface, input buf configuration
	u32 AFI_BUF0;		///< audio PCM buffer0 address
	u32 AFI_BUF1;		///< audio PCM buffer1 address
	u32 AFI_BUFLEN; 	///< PCM buffer length,in byte
	u32 AFI_RAWBUF; 	///<another input buf, for prevent input overflow, 0 to disable RAWBUF
	u32 PACKET_BUF; 	///<another BUFLEN buf for packet, only needed if use STEREO.

	//post-process configuration
	int (*post_init)(void);		///< post-process init callback, NULL to disable preprocess
	void (*post_process)(void * buf, int len); ///< post-process callback, NULL to disable post-process, parameter "int len" is in byte; parameter "void *buf" is the input and output buffer for the process, if stereo, this callback should process each channel respectively,first half in buf is left channel data, and the second half is right channel data
	int (*eq_init)(void);		///< equalizer init callback, NULL to disable preprocess
	void (*eq_process)(void * buf, int len); ///< equalizer process callback, NULL to disable post-process, parameter "int len" is in byte; parameter "void *buf" is the input and output buffer for the process, if stereo, this callback should process each channel respectively,first half in buf is left channel data, and the second half is right channel data
	void (*fill_AFIBUF)(u8 *buf,int len);	///<callback function used for fill fade out data in AFIBUF 
	void (*fill_FADEIN)(u8 *buf,int len);   ///<callback function used for fill fade in data in AFIBUF 	
	void (*control_volume)(void *buf,u32 len,s32 vol);///<function pointer for playback volume software control	

	u32 inbuf_addr;		///< input audio linklist buffer address
	u32 inbuf_size;	///< length of each linklist buffer, in byte

	FRAMELIST *list;
	t_cbuf_ctrl obuf;
	t_eq_params *eq_para;
}t_liba_dcfg;

extern t_liba_dcfg g_liba_dcfg;

#define DA_BUF_SIZE AI_PINGPONGBUF_SIZE
#ifdef I2SIRQ_MEMCOPY
#define PCM_VS_DA 16
#else
#define PCM_VS_DA 3
#endif
#define PCMOUT_BUF_SIZE (DA_BUF_SIZE*PCM_VS_DA)

#define INBUF_ONELEN 1024
#define MAX_OUTBUFLEN (1152*2)  //currently, af3 decoder output 1152*2 bytes each process(MONO)

#if 0
//INTERNAL_START
#define ADEC_ALLOCATE(inbuflen, inlist_num)	\
static u8 __attribute__  ((aligned(4))) g_da_ppbuf[DA_BUF_SIZE*2];	\
static u8 __attribute__  ((aligned(4))) g_pcm_cbuf[PCMOUT_BUF_SIZE];	\
 u8 __attribute__ ((aligned(4))) g_adec_framelist_buf[inbuflen];	\
static u8 __attribute__ ((aligned(4))) s_adec_inbuf[INBUF_ONELEN];\
FRAMELIST_DECLARE(adeclist, inlist_num)\
u8* g_adec_inbuf = s_adec_inbuf;\
static s32 adec_inbuflen = inbuflen;
//INTERNAL_END

#define ADEC_CONFIG()	\
{\
g_liba_dcfg.AFI_BUF0 = ((int)&g_da_ppbuf[0]|0x80000000);	\
g_liba_dcfg.AFI_BUF1 = ((int)&g_da_ppbuf[DA_BUF_SIZE]|0x80000000);	\
g_liba_dcfg.inbuf_addr = (u32)g_adec_framelist_buf;	\
g_liba_dcfg.inbuf_size = adec_inbuflen;	\
g_liba_dcfg.AFI_RAWBUF = 0; \
g_liba_dcfg.AFI_BUFLEN = DA_BUF_SIZE;	\
g_liba_dcfg.list = &adeclist;\
g_liba_dcfg.obuf_start = (u32)&g_pcm_cbuf;\
g_liba_dcfg.obuf_len = PCMOUT_BUF_SIZE;\
g_liba_dcfg.obuf_wp = 0;\
g_liba_dcfg.obuf_rp = 0;\
}
#else

#define ADEC_ALLOCATE \
static u8 __attribute__  ((aligned(4))) g_da_ppbuf[DA_BUF_SIZE*2*2];	\
static u8 __attribute__  ((aligned(4))) g_pcm_cbuf[PCMOUT_BUF_SIZE];
//static u8 __attribute__ ((aligned(4))) s_adec_inbuf[INBUF_ONELEN]; 
//u8* g_adec_inbuf = s_adec_inbuf;


#define ADEC_CONFIG	\
	.AFI_BUF0 = (u32)g_da_ppbuf,	\
	.AFI_BUF1 = (u32)g_da_ppbuf + DA_BUF_SIZE*2,	\
	.AFI_RAWBUF = 0, \
	.AFI_BUFLEN = DA_BUF_SIZE,	\
	.obuf.start = (u32)g_pcm_cbuf,\
	.obuf.len = PCMOUT_BUF_SIZE,\
	.obuf.wp = 0,\
	.obuf.rp = 0,

#endif


/**
 * @REAL_FORMAT void ADEC_CONFIG_DPOST_PROCESS(dpost_init,dpost_process);
 * @brief  config post-process callback functions when decoding
 * @details set two functions to the post_init and post_process function pointer
 * @param  dpost_init: init callback function
 * @param  dpost_process: process callback function
 * @return void
 */
//INTERNAL_START
#define ADEC_CONFIG_DPOST_PROCESS(dpost_init,dpost_process)\
{\
g_liba_dcfg.post_init = dpost_init;	\
g_liba_dcfg.post_process = dpost_process; \
}
//INTERNAL_END


/**
 * @defgroup liba_dhw_ai
 * @ingroup liba_decode
 * @brief library for audio playback hardware driver. Including internal audio interface and external DAC. 
 * @details 
 * -# <b>Source Codes:</b>
	* -# share/r2/liba_dhw/liba_dhw.c config internal audio playback interface.
	* -# share/r2/liba_dhw2/liba_dhw2.c command APIs to control audio decoding status on BA2.
 * -# <b>Depends On:</b>
 	* -# \ref lib_sccb "lib_sccb"
	* -# \ref libirq "libirq"
* @{
 */

/**
 * @brief init audio playback hardware
 * @details init audio hardware configure. MasterBA call this API to config BA2 in idle status
 *
 * @return 0:ok, <0:err
 *
 */
int liba_dhw_init(void);

/**
 * @brief start audio playback hardware
 * @details MasterBA call this API to config BA2 in start status
 * @return 0:ok, <0:err
 */
int liba_dhw_start(void);

/**
 * @brief stop audio playback hardware
 * @details MasterBA call this API to config BA2 in stop status
 * @return 0:ok, <0:err
 */
int liba_dhw_stop(void);

/**
 * @brief exit audio playback hardware
 * @details MasterBA call this API to config BA2 in exit status
 * @return 0:ok, <0:err
 */
int liba_dhw_exit(void);

/**
 * @brief get available output PCM buffer address
 * @details called by BA2, since total audio decoding flow is running on BA2
 * @return 0:no data, >0:return data address
 */
u32 liba_dhw_data_update(void);

/** @} */ //end liba_dhw_ai


/**
 * @defgroup liba_dec_algorithm
 * @ingroup liba_decode
 * @brief library for audio decoder algorithm.
 * @details this module includes several audio compress format like liba_dec_f1,liba_dec_f2,...,liba_dec_f6. For each audio format, a \ref t_adecoder_cfg "t_adecoder_cfg" type global structure variable are implemented to be called by liba_dec. For example,\ref t_adecoder_cfg "t_adecoder_cfg" g_adecoder_f1_cfg is implemented in liba_dec_f1 module, liba_dec will call g_adecoder_f1_cfg.init() to initialize f1 deocder and call g_adecoder_f1_cfg.process() to decode f1 bitstream.User can use menuconfig to choose what audio format algorithm be linked into the application, you can select one or more of the algorithms. 
 * @{
 */

//INTERNAL_END

/**
 * @brief init libadec
 * @details init decoder , init audio interface settings and init frame linklist status.
 * @return 0:ok, <0:err
 */
int libadec_init(void);

/**
 * @brief start audio decoding
 * @return 0:ok, <0:err
 */
int libadec_start(void);
/**
 * @brief stop audio decoding
 * @return 0:ok, <0:err
 */
int libadec_stop(void);

/**
 * @brief exit audio decoding
 * @return 0:ok, <0:err
 */
int libadec_exit(void);

/**
 * @brief clear output pcm buffer for pause and seek operation
 * @return void
 */
void libadec_pcmbuf_clear(void);

/**
 * @brief update audio hardware output buffer
 * @details Main task can pend on g_liba_dcfg.evt_int, after receive the signal, call liba_update_output to decode audio frame and update audio output PCM buffer. It is called by audio decoding flow control module running on BA2.
 * @return 0:ok, <0:err
 */
extern int libadec_update_output_internal(void);
/**
 * @brief a fake API to update audio hardware output buffer. In order to compatible with former API. This is called by application.
 * @return 0:ok, <0:err
 */
int libadec_update_output(void);

/**
 * @brief audio playback interrupt handler
 * @details Two buffers are used as pingpong buffer for AI transfering PCM to DAC when playback. when one buffer is used by AI transfering PCM, the other buffer is used as the output buffer for libadec_update_output(). Each time AI finished transfering all PCM in one buffer, an interrupt will be generated, this function (libadec_irq()) is called by except.c to switch the two pingpong buffer. 
 */
void libadec_irq(void);

/**
 * @brief get buffer address to store one block audio
 * @details called by task which is sending audio data to 
 libadec to decode and play
 * @return >0 buffer address :ok, 0 : err
 */
FRAME* libadec_inbuf_get(void);

/**
 * @brief add one block of audio to audio framelist
 * @details called by task which is sending audio data to
 libadec.
 * @param addr: address storing one block of audio data, it
 				is got by calling libadec_inbuf_get.
 * @param size: data size in byte
 * @return 0:ok, <0:err
 */
int libadec_frm_add(FRAME* afrm);

void libadec_frmlist_reset(void);
void libadec_frmlist_stop(void);
/** @} */ //end liba_dec


#endif
