#ifndef __SENSOR_H__
#define __SENSOR_H__

#include "libsccb.h"

/**
 * @defgroup libsensor
 * @ingroup lib_interface
 */

/**
 * @defgroup libsensor_setting
 * @ingroup libsensor
 * @{
 */

/** 0~31 are exact the same as PU_Descriptor bmControls bits, to match UVC standard. Not full defined. */
typedef enum
{
	SNR_EFFECT_BRIGHT = 0, ///< D0: Brightness
	SNR_EFFECT_CONTRAST, ///< D1: Contrast
	SNR_EFFECT_HUE, ///< D2: Hue
	SNR_EFFECT_SATURATION, ///< D3: Saturation
	SNR_EFFECT_SHARPNESS, ///< D4: Sharpness
	SNR_EFFECT_GAMMA, ///< D5: Gamma
	SNR_EFFECT_WHITE_BALANCE_TEMP, ///< D6: White Balance Temprature
	SNR_EFFECT_WHITE_BALANCE_COMP, ///< D7: White Balance Component
	SNR_EFFECT_GAIN = 9, ///< D9: Gain
	SNR_EFFECT_LIGHT_FREQ, ///< D10: Power Line Frequency  level: 0: disabled, 1: 50hz, 2: 60hz
	SNR_EFFECT_WHITE_BALANCE_TEMP_AUTO = 12,
	SNR_EFFECT_FLIP_MIR = 32, ///< level: 0:noflip/nomirror, 1:noflip/mirror, 2:flip/nomirror, 3:flip/mirror
	                          ///< bit[0] 0:disable mirror, 1: enable mirror ; bit[1] 0:disable flip 1:enable flip
	                          ///< recommend to use SNR_EFFECT_FLIP and SNR_EFFECT_MIRROR instead.
	SNR_EFFECT_FLIP,///< level: 0:disable flip, 1:enable flip
	SNR_EFFECT_MIRROR,///< level: 0 disable mirror 1:enable mirror
	SNR_EFFECT_LIGHT_COND, ///< level: 0-1
	SNR_EFFECT_SIZE, ///< level: it supports two types of arg: 1)VIDEO_SIZE_XXX, 2)(video_width<<16)|video_height. it change both sensor size and cif output size. in current design, we only support the same sensor size and cif output size
	SNR_EFFECT_FRMRATE, ///< level: 1-120.
	SNR_EFFECT_AF, ///< level: 1, do auto focus one shot, 0, do auto focus continuously.
	SNR_EFFECT_BW_SWITCH,  ///level:1,enable black/white  0,disable BW
	SNR_EFFECT_FASTBOOT,
	SNR_EFFECT_WBCALI,		///< level: buffer address
	SNR_EFFECT_HDR,
}e_sensor_effect;

#define SNR_EFFECT_PROC 0xffffffff ///< for sensor_effect_change() the last one

/**
 * @brief this is a sensor effect configuration data 
 */
typedef struct s_sensoreffect_one
{
	u16 effect;///< sensor effect type
	u8 need_proc;
	u8 padding;
	u32 min;///< minimum value
	u32 max;///< maxmum value
	u32 def;///< defined value
	u32 cur;///< current value
}t_sensoreffect_one;

/* sensor interface type */
#define SNR_IF_DVP		0x00
#define SNR_IF_MIPI_1LN	0x01
#define SNR_IF_MIPI_2LN	0x02
#define SNR_IF_MIPI_4LN	0x04

typedef struct s_sensor_isptables
{


}t_sensor_isptables;

/**
 * @brief this is sensor configuration struct 
 */
typedef struct s_sensor_cfg
{
	u8 sccb_mode; ///< SCCB MODE(SCCB_MODE8 or SCCB_MODE16)
	u8 sccb_id; ///< SCCB slave id
	char * name;///< sensor id name
	u8 datafmt;	///< DPM_DATAFMT_XXX
	u8 interface_type; ///< dvp or mipi

	u16 cur_frame_rate;
	u32 cur_video_size; ///< sensor output size

	void * init_config ; ///< pointer to array
	s32 init_config_len; ///< array len

	s32 (*detect)(struct s_sensor_cfg *); ///< callback to detect sensor, ret 0:found, <0:failed
	int (*sensor_effect_change)(struct s_sensor_cfg *, u32 effect, u32 level);///< callback to process sensor effect change request function

	t_sensoreffect_one * effects;///< pointer to sensor effect
	u32 effect_cnt;///< sensor effect count

	int (*isp_init)(struct s_sensor_cfg *); ///< callback to init isp
	t_sensor_isptables * isptables;

	//private below
	t_libsccb_cfg * sccb_cfg; //set by dpm_sensor_init()
	u8 effect_need_proc;
	
	int (*rgbir_init)(void *); ///< callback to config RGBIR module because each sensor has different default setting
}t_sensor_cfg;

/** @} */ //end lib_sensor_setting

//INTERNAL_START
int sensor_effect_req(struct s_sensor_cfg *, u32 effect, u32 level);
int sensor_effect_proc(t_sensor_cfg * cfg);
//INTERNAL_END

#endif
