#ifndef __TM_H__
#define __TM_H__

u32 get_ms(void);

#define abs(v)   ((v > 0) ? v : -(v))
#define labs(v)   (((long)(v) > 0) ? (long)(v) : (long)-(v))

typedef unsigned int time_t ;

/**
 * @brief the same name for POSIX compatible
 */
struct tm
{
  int tm_sec;			///< Seconds.	[0-60] (1 leap second)
  int tm_min;			///< Minutes.	[0-59]
  int tm_hour;			///< Hours.	[0-23]
  int tm_mday;			///< Day.		[1-31]
  int tm_mon;			///< Month.	[0-11]
  int tm_year;			///< Year	- 1900. 
  int tm_wday;			///< Day of week.	[0-6]
  int tm_yday;			///< Days in year.[0-365]
  int tm_isdst;			///< DST.		[-1/0/1]
};

void rtos_time_format(struct tm *t, u8 *str);
struct tm *rtos_localtime_r(const time_t *timer, struct tm *t);

#endif //__TM_H__
