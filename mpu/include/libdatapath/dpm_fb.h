#ifndef _DPM_FB_H_
#define _DPM_FB_H_

struct s_dpm_fb_buf;

typedef struct s_dpm_fb_buflink
{
	struct s_dpm_fb * fbnext;
}t_dpm_fb_buflink;//stagger HDR buffer management link

typedef struct s_dpm_fb_buf
{
	u32 id;
	u32 ymem_base; //membase for Y data
	u32 uvmem_base; ///< membase for UV data. y address and uv address should be in different DDR blank.
	u32 status;
#define DPM_FB_BUFSTATUS_IDLE	0x0 //buffer is available for FB
#define DPM_FB_BUFSTATUS_WRITING	0x1 //buffer is writing by FB
#define DPM_FB_BUFSTATUS_READY	0x2 //buffer is ready for ECIF
#define DPM_FB_BUFSTATUS_READING	0x3 //buffer is reading by ECIF
	struct s_dpm_fb_buf * next;
	t_dpm_fb_buflink * link;///<config this for stargger HDR buffer management
}t_dpm_fb_buf;

typedef enum
{
	FB_FORMAT_RAW8IN_6STORE,
	FB_FORMAT_RAW10IN_6STORE,
	FB_FORMAT_RAW8IN_NOSTORE,
	FB_FORMAT_RAW10IN_8STORE,
	FB_FORMAT_RAW12IN_8STORE,
	FB_FORMAT_RAW10IN_NOSTORE,
	FB_FORMAT_RAW12IN_NOSTORE,
	FB_FORMAT_YUV422IN_6STORE,
	FB_FORMAT_YUV422IN_NOSTORE,
	FB_FORMAT_YUV422IN_YUV420OUT_6STORE,
	FB_FORMAT_YUV422IN_YUV420STORE,
}FB_DATA_MODE;

typedef struct s_dpm_fb_seqlock
{
	struct s_dpm_fb * fb_running; //the running FB obtained the lock
}t_dpm_fb_seqlock;

typedef struct s_dpm_fb_reuselock
{
	struct s_dpm_fb * fb_running; //the running FB obtained the lock
	u8 irq_requested;
}t_dpm_fb_reuselock;

typedef struct s_dpm_fb
{
	DECLARE_DPM

	t_dpm_fb_buf * bufhead;

	u8 enabled;
	u8 int_mask;
	u8 data_format;
	u8 pingpong_en;
	u8 ecif_switch;	///<if FB is output as a bitstream, and the input data is from ECIF, please config this.
	u8 mipi_kick;	///<FB will be kick in MIPICIF interrupt instead of in FB interrupt
	u8 init_enabled;///<in mipi_kick mode, 1: kick FB at the first frame, 0: don't kick FB at the first frame.
	u8 skip_early_frm;///<if need to skip earlier frames in buflist, enable this
	u8 no_dbg;///<no need of extra debug information
	u8 yuv_swap; ///< 1: yvyu, 0:vyuy
	u8 need_loop; ///< in manual start mode, only add fb_loop at the beginning, no need to add/delet fb_loop at fb_start/stop repeatedly

	///<dpm_check is for loopback hdr timing control. if this fb is for l-channel before loopback, the change of MIPICIF mux and the start of dp_chain should wait for dpm_check->switch_tag == 0, which means MIPICIF0 is ready for mux switch from RX0VC1 to RX1VC0.
	struct s_dpm_mipirx* dpm_check;
	struct s_dpm_fb* fb_check;///<for loopback HDR l/s channel buf sync, need to check fb frame done signal of another channel 
	volatile u8	fb_buf_rdy;///<for loopback stagghdr, showing l/s channel fb buffer is ready 
#define MODE_SEQOUT_PARAIN (1)
#define MODE_PARAOUT_PARAIN (2)
	u32 fb_inout_mode;
	u32 yaddr;
	u32 uvaddr;
	u32 offset;

	struct s_dpm_antishake * antishake;
	u8	skip_current_frm;//internal use

	//if need do some FBs in sequence, set it to the same seqlock
	t_dpm_fb_seqlock * seqlock;
	//if need to set the same id for different dpm_fb, set them to the same reuselock. 
	//Please make sure the reused FB should run different datapath paths in sequential order
	t_dpm_fb_reuselock * reuselock;

	u32 (*handle_cmd)(struct s_dpm_fb * dpm, u32 cmd, u32 arg, u32 * handled);

	//private
	struct s_dpm_fl * fl_cur; //point to dpm_ecif

	t_dpm_fb_buf * bufread; //the 1st read one for ECIF
	t_dpm_fb_buf * bufwrite; //the next write buf for FB

	t_dp_loop loop_func;

	u32 reg_addr;
	u32 fb_irq_cnt;
	u32 fb_wait_irq_cnt;
	u32 dcpc_pic_cnt; 
	u32 frm_skip_cnt;
	u32 frm_cnt;
	u8	buf_full_flag;	//FB buffer full, need to re-fill after ECIF EOF intr
	u8 fb_hdr_en;

	u8 flag_fb_merge;   //< the following 4 parameters are used in the 2sensor DCPC cases.
	u8 fb_merge_id;
	u32 fb_merge_yaddr;
	u32 fb_merge_uvaddr;

	t_vs_fps_cfg* fps_cfg;///<manual fps control param
    u8 (*cb_frmrate_drop)(t_vs_fps_cfg*);  ///< callback for manually control frame rate in ecif mode, if set, FB will call this function before encoding each frame, if return 1, FB will encode the frame, if 0, the frame will be skipped
	FRAME* frm;///<need to allocated fb frame externally if need more than one raw stream

	short    num_of_buf; // <the number of frame buffers.>
	struct s_dpm_fb* p_fb_pair; // <The related frame buffer of the other channel. This is for synchronizing the buffers of the input channels.>
	
}t_dpm_fb;

int fb_sanity_check(t_dpm_fb * dpm);
int fb_init(t_dpm_fb * dpm);
int fb_start(t_dpm_fb * dpm);
int fb_tmp_start(t_dpm_fb * dpm);
int fb_stop(t_dpm_fb * dpm);
int fb_exit(t_dpm_fb * dpm);
int fb_enable(t_dpm_fb * dpm);
int fb_disable(t_dpm_fb * dpm);
FRAME * fb_get_frame(t_dpm_fb * dpm);
int fb_get_resolution(t_dpm_fb * dpm);
int fb_stream_free_frame(t_dpm_fb * dpm, FRAME* frm);
int	fb_sync_hdrbuf(t_dpm_fb* dpm);
int fb_reset(t_dpm_fb * dpm);

#define DPM_INIT_FB \
	.type = DPM_TYPE_FB, \
	.dpmname = "FB", \
	.sanity_check = (int (*)(t_dpm * dpm))fb_sanity_check, \
	.init = (int (*)(t_dpm * dpm))fb_init, \
	.start = (int (*)(t_dpm * dpm))fb_start, \
	.stop = (int (*)(t_dpm * dpm))fb_stop, \
	.enable = (int (*)(t_dpm * dpm))fb_enable, \
	.disable = (int (*)(t_dpm * dpm))fb_disable, \
	.exit = (int (*)(t_dpm * dpm))fb_exit,

#endif
