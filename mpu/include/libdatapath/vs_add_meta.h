#ifndef _VS_ADD_META_
#define _VS_ADD_META_

int libvs_add_meta_int(u8 vsid, char *key, int value);
int libvs_add_meta_string(u8 vsid, char *key, char *string);

#endif
