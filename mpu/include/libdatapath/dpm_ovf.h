#ifndef _DPM_OVF_H_
#define _DPM_OVF_H_

typedef struct s_ovf_header{
	u32 magic; //0x4f565959
	u32 format; //0x00: default.  0x1: same YUV format from our FB. 0x2: same YUV format from our MD.
	u32 width;
	u32 height;
	u32 frame_size; //bytes of each video frame, 0 for dynamic frame size
	u32 file_size; //total file size, include the 32 bytes header.
	
	u32 reserved[2]; //make header 32bytes.
}t_ovf_header;

typedef struct s_dpm_ovf //ovf: OV video file
{
	DECLARE_DPM

	u32 buf_addr; //buffer addr
	u32 max_len; //buffer max length
	
	struct s_ovf_header * ovf_header;//OV video file header
	u32 (*handle_cmd)(struct s_dpm_ovf * dpm, u32 cmd, u32 arg, u32 * handled);
	FRAMELIST * framelist;

	//for ECIF mode
	int buf_status;
	u32 y_base;
	u32 uv_base;
	struct s_dpm_fl * fl_cur; //point to dpm_ecif
	int req_next;
	t_dp_loop loop_func;
}t_dpm_ovf;

int dpm_ovf_init(t_dpm_ovf * dpm); 
int dpm_ovf_start(t_dpm_ovf * dpm);
int dpm_ovf_stop(t_dpm_ovf * dpm);

void ovf_stream_free_frame(t_dpm_ovf* dpm, FRAME* frm);
FRAME* ovf_get_frame(t_dpm_ovf* dpm);

#define DPM_INIT_OVF \
	.type = DPM_TYPE_OVF, \
	.dpmname = "OVF", \
	.init = (int (*)(t_dpm * dpm))dpm_ovf_init, \
	.start = (int (*)(t_dpm * dpm))dpm_ovf_start, \
	.stop = (int (*)(t_dpm * dpm))dpm_ovf_stop,

#endif
