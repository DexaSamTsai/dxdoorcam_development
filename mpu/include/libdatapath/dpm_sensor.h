#ifndef _DPM_SENSOR_H__
#define _DPM_SENSOR_H__

typedef struct s_dpm_sensor
{
	DECLARE_DPM

	char * name; //sensor name, if NULL, use the 1st sensor found.
	t_libsccb_cfg * sccb_cfg; //must be set, for the sccb of the dpm_sensor
	int fastboot;
	//private
	t_sensor_cfg * sensor_cfg;
}t_dpm_sensor;

int dpm_sensor_init(t_dpm_sensor * dpm); //find the sensor_cfg
int dpm_sensor_start(t_dpm_sensor * dpm);
int dpm_sensor_stop(t_dpm_sensor * dpm);

u32 dpm_sensor_effect(t_dpm *dpm, u32 effect, u32 arg);

#ifdef CONFIG_FAST_BOOT_EN
#define DPM_SENSOR_INIT_FAST .fastboot = 1,
#else
#define DPM_SENSOR_INIT_FAST .fastboot = 0,
#endif

#define DPM_INIT_SENSOR \
	.type = DPM_TYPE_SENSOR, \
	DPM_SENSOR_INIT_FAST \
	.dpmname = "SNR", \
	.init = (int (*)(t_dpm * dpm))dpm_sensor_init, \
	.start = (int (*)(t_dpm * dpm))dpm_sensor_start, \
	.stop = (int (*)(t_dpm * dpm))dpm_sensor_stop,

#endif
