#ifndef _DPM_IMG_H_
#define _DPM_IMG_H_

typedef enum{
	IMGFRAME_STATUS_IDLE,
	IMGFRAME_STATUS_PREPARE,
	IMGFRAME_STATUS_READY,
	IMGFRAME_STATUS_DOING,
	IMGFRAME_STATUS_DONE,
	IMGFRAME_STATUS_OVERWRITE,
}IMG_STATUS;

typedef enum
{
	IN420_OUT420_NOSCALE=0,
	IN420_OUT422_2XSCALE,
	IN422_OUT422_NOSCALE,
	IN422_OUT422_2XSCALE,
}E_ENC_L2BMODE;

typedef FRAME IMGFRAME_LIST;

typedef struct s_dpm_img
{
	/*dpm param*/
	DECLARE_DPM

	t_dp_loop loop_func;
	
	/*img param*/
	u32 buflist_start; //general buffer start 
	u32 buflist_size; //buffer list size
	u32 imgenc_size; //maximum one frame size, frame will be dropped if exceed this size 
	u32 imgenc_tgt_size; //targeted one frame size, config this will enable img rate control
	u32 width;
	u32 height; //encoder input size
	
	u8 frame_rate;
	u8 qs;
	u8 min_qs;//minimum QS for rate control, 4 ~ 60
	u8 max_qs;//maximum QS for rate control, 4 ~ 60
	u8 qs_auto;//should disable qs_auto if need rate control
	u8 qs_manual;//set qs. should disable qs_auto and rate control.
	u8  *q_table;  //pointer to quantization table. 	
	u8 ecif_switch;    //internal use. no need to config 
	u8	vif_frm_skip;
	u8 imgbuf_fixsize_en; //< img ptr_w skip imgenc_size when the flag is enabled. 

	t_vs_fps_cfg* fps_cfg;///<manual fps control param
    u8 (*cb_frmrate_drop)(t_vs_fps_cfg*);  ///< callback for manually control frame rate in ecif mode, if set, img will call this function before encoding each frame, if return 1, img will encode the frame, if 0, the frame will be skipped

	u32 (*handle_cmd)(struct s_dpm_img * dpm, u32 cmd, u32 arg, u32 * handled);
	
	u32 eof_interval;
//INTERNAL_START
	/*private param*/
	u32 imgenc_buf_size; //buffer for one frame size
	u32 img_size; //current img_size
	
	VFDIST*	vfdist;
	FRAMELIST * framelist;
	IMGFRAME_LIST * imgfrm;
	u32 ptr_r;
	u32 ptr_w;
	u32 reftm;	
//INTERNAL_END
}t_dpm_img;

typedef t_dpm_img t_img_cfg;

int img_init(t_dpm_img * dpm);
int img_start(t_dpm_img * dpm);
int img_enable(t_dpm_img * dpm);
int img_stop(t_dpm_img * dpm);
int img_exit(t_dpm_img * dpm);


int libimg_init(t_img_cfg *img_cfg);
int libimg_enable(t_img_cfg *img_cfg);
void libimg_start(t_img_cfg *img_cfg);
void libimg_stop(t_img_cfg *img_cfg);

void libimg_irq_handler(t_img_cfg *img_cfg);

FRAME *libimg_get_frame(t_img_cfg *img_cfg);
void libimg_release_frame(t_img_cfg *img_cfg, FRAME* frm);
u32 libimg_get_reftm(t_img_cfg *img_cfg);
int libimg_set_bitrate(t_img_cfg *img_cfg, u32 bit_rate);
int libimg_set_qs(t_img_cfg * img_cfg, u8 qs);


#define DPM_INIT_IMG \
	.type = DPM_TYPE_IMG, \
	.dpmname = "IMG", \
	.init = (int (*)(t_dpm * dpm))img_init, \
	.start = (int (*)(t_dpm * dpm))img_start, \
	.enable = (int (*)(t_dpm * dpm))img_enable, \
	.stop = (int (*)(t_dpm * dpm))img_stop, \
	.exit = (int (*)(t_dpm * dpm))img_exit,

#endif

