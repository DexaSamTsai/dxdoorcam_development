#ifndef _DPM_ISPIDC_H_
#define _DPM_ISPIDC_H_

typedef struct s_nv_ctrl
{
	u8 bNVFlag;///< day or night flag
	u32 nNightFrmCount;
	u32 nDayFrmCount;
	u32 nDayNightThr;
	int nNightDayThr;
	u32 nNightDayLum;
	u16 nNVTriggerCnt;
	u32 nFrameCycle;
	u32 nFrameCyclePerNV;
	u32 nThresLumVal;
	u32 nNightLum;
}t_nv_ctrl;

//because there's no path options between ISP and IDC, so they're in one configuration named ispidc
typedef struct s_dpm_ispidc
{
	DECLARE_DPM

	DECLARE_FL

	//because there are 4 idc input, so 'in' means 'in0'.
	//please not 'ispidc' must specify the 'in' or 'inN' variable to father in struct init before libdatapath_init().
	//because libdatapath_init() don't fill the 'in' or 'inN' variables.
	struct s_dpm * in1 ;
	struct s_dpm * in2 ;
	struct s_dpm * in3 ;
	// short exposure, for hdr switch
	struct s_dpm * in_s_exp;

	u8 isp_bypass;
	u8 add_meta_en;///<add isp AWB/EXP information at the end of each encoded frame

	u8 idc_rd_mem; ///< set to 1 if IDC get data from DDR

	///<idc dummy_mode
#define IDC_DUMMY_AUTO_MODE   (0) ///<idc will automatically add dummy pixels for each pixel line
#define IDC_DUMMY_MANUAL_MODE (1) ///<manually configure idc dummy pixels
	u8 dummy_mode; ///< dummy mode for idc dummy pixels for each pixel line
	u8 dummy_line; ///< dummy lines added after a frame. Because ISP need to buffer some lines(10 ~ 16) for processing, IDC need to add some dummy lines after each frame for ISP to output the complete image. 
	u32 dummy_hblk;//dummy pixels for each pixel line, by default 0xfff. If dummy_mode is AUTO, no need to configure this

	///<idc mode
#define IDC_MODE_1S_NORMAL    (0)  ///< one sensor normal
#define IDC_MODE_1S_IHDR      (1)  ///< one sensor with iHDR
#define IDC_MODE_1S_PIXELHDR  (2)  ///< one sensor with pixel-based HDR
#define IDC_MODE_1S_STAHDR_OR_2S  (3) ///< one sensor with staggerred HDR without using ddr and l/s exposure line must be at the same time or two sensor
#define IDC_MODE_3S           (4)  ///< 3sensor
#define IDC_MODE_4S           (5)  ///< 4 sensor
	u8 idc_mode; ///< mode0:snr normal mode2:snr pixel, based HDR mode4:snr

	u8 idc_done_check; ///< it is used only in the HDR case or read data from DDR by IDC, when idc_done_check == 1, check the full modules' status in chain, otherwise, only check IDC done status.
	u8 bit_sel;///<0: raw8, 1: raw10

	///<V2 D1 timing configurations
	u16 v2d1_hblk; //hblank for v2d1, by default 0x100, need to configured proportionally to idc hblk & V2D1(ISPD1) clock
	u32 cpp; //clock per pixel, 0: v2d1 process one pixel every cycle; 0x11: v2d1 process one pixel every two cycles
	
	///< 3ddns
	u8 TDdns_en;
	u32 TDdns_mem1;
	u32 TDdns_mem2;
	
	///< configuration for IDC get data from DDR, need to config if idc_rd_mem set to 1
	u8 rd_non_hdr;
	u8 hdr_from_ddr;
	u32 hdr_l_addr;
	u32 hdr_s_addr;
	u32 hdr_wait_num;///<the period between eacy request for DDR, should not be too small when DDR load is high
	///< timing configurations for IDC get data from DDR
	u32 rd_auto_gap_num;///<dummy pixels after each even lines(e.g. The 2nd, 4th, 6th, ect).
	u32 r_rd_v2_auto_gap_num_o;///<dummy pixels after each odd lines(1st, 3rd, 5th, ect)
	u32 r_rd_auto_v_gap_num;///<dummy pixels between ECOF and SOF of the next frame. 

	u8 isp_tuning;
	u8 isp_mode; //current isp mode, day or night.
	u8 isp_setmode; //isp set mode, day or night or auto.

	///< isp yuv crop;
	u32 crop_x;
	u32 crop_y;

	///<idc private
	u8 idc_start;
	t_dp_loop loop_func;
	volatile u8 ispeof_intr;

	u8 isp_brightness;	

	///< lum config, TODO: move to sensor
	u32 lum_sensor_pixelsize;
	u32 lum_sensor_hts;
	u32 lum_sensor_clk;
	u16 vts_current;	//sensor current vts when autofps

	///< lum output
	u32 lum;
	///< private
	u32 lum_ratio;
	///< daynight manual mode config
	int night2day_delay_frm;
	t_nv_ctrl daynight_arg;

	///< isp private
	int inited;
	struct s_dpm_rgbir * dpm_rgbir;
	u8 drop_frst_ispeof;

	///<hdr
#define ISPIDC_HDR_SWITCH_MODE	(1)
	int hdr_switch_signal;
	int hdr_mode;
	int hdr_switch_wait_frm;
	int hdr_switch_frm_cnt;
	int hdr_switch_l_only; 

	///<histogram
	int hist_status;	// 0: idle, 1: doing, 2:ready
}t_dpm_ispidc;


int ispidc_init(t_dpm_ispidc * dpm);
int ispidc_start(t_dpm_ispidc * dpm);
int ispidc_stop(t_dpm_ispidc * dpm);
int ispidc_exit(t_dpm_ispidc * dpm);
int ispidc_update_channel(u8 idc_id, u8 ispout_id);

#define DPM_INIT_ISPIDC \
	.type = DPM_TYPE_ISPIDC, \
	.dpmname = "IDC", \
	.init = (int (*)(t_dpm * dpm))ispidc_init, \
	.start = (int (*)(t_dpm * dpm))ispidc_start, \
	.stop = (int (*)(t_dpm * dpm))ispidc_stop, \
	.exit = (int (*)(t_dpm * dpm))ispidc_exit,

#endif
