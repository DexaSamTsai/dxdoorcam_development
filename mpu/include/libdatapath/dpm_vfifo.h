#ifndef _DPM_VFIFO_H_
#define _DPM_VFIFO_H_

typedef struct s_dpm_vfifo{
	DECLARE_DPM
	
	u32 start_size;
#define VFIFO_OUTPUT_RAW8   (0)
#define VFIFO_OUTPUT_RAW10  (1)
#define VFIFO_OUTPUT_RAW12  (2)
#define VFIFO_OUTPUT_YUV422 (3)
	u8 data_format;
#define VFIFO_AUTO_MODE     (0)
#define VFIFO_MANUAL_MODE   (1)
	u8 start_mode;
	u8 clk_src;//0: isp, 1: mipi tx
	u8 clk_div;
#define VFIFO_DVP_OUT    (0)
#define VFIFO_MIPITX_CSI_OUT (1)
#define VFIFO_MIPITX_DSI_OUT (2)
	u8 vfifo_out;
	u8 yuv_sel; ///< 0 YUYV 1:UYVY
	u8 hsync_header;
	u8 hsync_tail;
}t_dpm_vfifo;

int vfifo_init(t_dpm_vfifo * dpm);
int vfifo_start(t_dpm_vfifo * dpm);
int vfifo_stop(t_dpm_vfifo * dpm);
int vfifo_exit(t_dpm_vfifo * dpm);

#define DPM_INIT_VFIFO \
	.type = DPM_TYPE_VFIFO, \
	.dpmname = "VFIFO", \
	.init = (int (*)(t_dpm * dpm))vfifo_init, \
	.start = (int (*)(t_dpm * dpm))vfifo_start, \
	.stop = (int (*)(t_dpm * dpm))vfifo_stop, \
	.exit = (int (*)(t_dpm * dpm))vfifo_exit,

#endif
