#ifndef __DPM_OSDRLC_H__
#define __DPM_OSDRLC_H__

#define OSDRLC_WIN_TOTAL_COUNT  (8)

typedef struct s_osdrlc_win_cfg
{
	u8 win_no;
	u8 mem_id;
	u8 win_update;
	u8 win_disable;
	u16 win_hsize;
	u16 win_vsize;
	u16 win_hstart;   ///< for R3 new, BIT15: 1-->window start x is out of background picture, 0-->window start x is in picture.BIT0---BIT14 for start address
	u16 win_vstart;   ///< for R3 new, BIT15: 1-->window start x is out of background picture, 0-->window start x is in picture.BIT0---BIT14 for start address
	u32 win_memaddr;
	u32 compressed_size;
	u32 color[8];     ///< (8LUT):[23--16]Y,[15--8]U,[7--0]V
}t_osdrlc_win_cfg;

typedef struct s_dpm_osdrlc
{
	DECLARE_DPM

	u8 bypass;
#define OSDRLC_FORMAT_YUV420 (0)
#define OSDRLC_FORMAT_YUV422 (1)
	u8 in_format;
	u8 out_format;
	u8 default_sel;///< default selection when osd4l not running, 0: ecif0, 1: yuvscale0, 2: ecif1, 3: dcpcstitch
//INTERNAL_START
//INTERNAL_START
	u8 osdrlc_en;
//INTERNAL_END
	u8 ecif_switch;
	void (*cb_osdrlc_tick2_draw)(void);
	u32 wait_cycle; ///< the above callback function and parameter are used in the case of single stream.
	t_osdrlc_win_cfg osd_win[OSDRLC_WIN_TOTAL_COUNT];
}t_dpm_osdrlc;


int osdrlc_init(t_dpm_osdrlc * dpm);
int osdrlc_start(t_dpm_osdrlc * dpm);
int osdrlc_stop(t_dpm_osdrlc * dpm);
int osdrlc_exit(t_dpm_osdrlc * dpm);

int osdrlc_draw(void);
int osdrlc_update(t_osdrlc_win_cfg * cfg, u8 osd_id);
u32 osdrlc_bufget(u8 win_no, u8 osd_id);
void osdrlc_tick2_draw(void);
int osdrlc_enable(int win_no);
int osdrlc_disable(int win_no);

#define DPM_INIT_OSDRLC \
	.type = DPM_TYPE_OSDRLC, \
	.dpmname = "OSDRLC", \
	.init = (int (*)(t_dpm * dpm))osdrlc_init, \
	.start = (int (*)(t_dpm * dpm))osdrlc_start, \
	.stop = (int (*)(t_dpm * dpm))osdrlc_stop, \
	.exit = (int (*)(t_dpm * dpm))osdrlc_exit,

#endif
