#ifndef _DPM__H_
#define _DPM__H_

typedef struct s_dpm_mipirx
{
	DECLARE_DPM

	u8 lane_num; ///< 1,2,4
	u8 four_sel;
	u8 yuv_sel;
	u8 vc0_emb_en;
	u8 vc1_emb_en;
	u8 vc0_emb_id;
	u8 vc1_emb_id;
	u8 vc0_emb_use_dt;
	u8 vc1_emb_use_dt;
	u8 lnum_int;
	u8 pnum_int;
	u8 c2d4_en;
	u32 int_en;
	u8 invert_pclk;
/*
 	Abstract datapath for stagHDR mipi loopback datapath:
  <--------------------------MIPITX
  |                             |
  |      -->VC0--->MIPICIF1--->FB1(l-channel before RGBIR processing)
  |  RX0-|         _______________________
  |      -->VC1-->|                       |-->FB2(s-channel, RGBIR processed)
  |               |MIPICIF0->RGBIR->RAWSCL|
  |-->RX1VC0----->|_______________________|-->FB0(l-channel, RGBIR processed)
Config dpm as follows 
static t_dpm_mipirx dp_mipirx0 = 
{
	.dpm_switch1 = &dp_fb1,//l-channel before loopback
	.dpm_switch2 = &dp_fb2,//s-channel no need of loopback
}

static t_dpm_mipirx dp_mipirx1 = 
{
	.dpm_switch1 = &dp_fb1,//l-channel before loopback
	.fb_out = &dp_fb0,//l-channel after loopback
}
static t_dpm_fb dp_fb1 = 
{
	.dpm_check = &dp_mipirx0,
	.fb_check = &dp_fb2,
}
static t_dpm_fb dp_fb2 = 
{
	.fb_check = &dp_fb1,
}
 */
	t_dpm_fb* dpm_switch1;//In stagHDR mipi loopback mode, for long channel fb before loopback
	t_dpm_fb* dpm_switch2;//In stagHDR mipi loopback mode, for short channel fb, needed only by mipirx0
	t_dpm_fb* fb_out;//In stagHDR mipi loopback mode, config this to long channel fb after loopback, needed only by mipirx1

	///<if drop_en, switch_tag is for stagHDR no-loopback VC0 fb kick control, 
	///<if drop_en == 0, switch_tag is for loopback fb kick control. it is set to 1 at rx0vc1 sof and set to 0 at rx0vc1 eof. when switch_tag is 0, it means it is the period between last rx0vc1 eof and next rx0vc1 sof.
	volatile u8 switch_tag;

	//private
	u32 reg_addr;
	u8 drop_en;//drop fb frame at RXEOF interrupt, for stagHDR no-loopback fb kick control
}t_dpm_mipirx;

int mipirx_init(t_dpm_mipirx * dpm);
int mipirx_start(t_dpm_mipirx * dpm);
int mipirx_stop(t_dpm_mipirx * dpm);
int mipirx_exit(t_dpm_mipirx * dpm);

#define DPM_INIT_MIPIRX \
	.type = DPM_TYPE_MIPIRX, \
	.dpmname = "MIPIRX", \
	.init = (int (*)(t_dpm * dpm))mipirx_init, \
	.start = (int (*)(t_dpm * dpm))mipirx_start, \
	.stop = (int (*)(t_dpm * dpm))mipirx_stop, \
	.exit = (int (*)(t_dpm * dpm))mipirx_exit,

#endif
