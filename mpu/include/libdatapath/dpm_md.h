#ifndef _DPM_MD_H_
#define _DPM_MD_H_
#include "libmd_common.h"
#define MD_WIN_TOTAL_CNT   (8)

typedef struct s_dpm_md
{
	DECLARE_DPM

	u8 win_cnt;
	u8 algo_en;
	u8 post_pro_iter_num;	//post processing iteration number, ranged 0~15.  It is a trade-off between motion and noise, smaller value will cause md to be more sensitive but easily influenced by noise.
	u8 pix_diff_th;	// pixel differ threshold, ranged 0~32. It is a trade-off between motion and noise, smaller value will cause md to be more sensitive but easily influenced by noise.
	///< internal parameters
	u8 reset_en;
	u8 md_first_flag;
	
	t_md_win_cfg md_win[MD_WIN_TOTAL_CNT];
	u32 intr_mask;
	u32 md_status;
	u32 mask_addr;
	u32 copy_addr;
    u32 prev_addr0;
    u32 prev_addr1;
	u32 (*handle_cmd)(struct s_dpm_md * dpm, u8 md_id, u32 cmd, u32 arg, u32 * handled);
	///< internal parameters
	u32 frame_done;
	t_dp_loop loop_func;
}t_dpm_md;

int md_init(t_dpm_md * dpm);
int md_start(t_dpm_md * dpm);
int md_stop(t_dpm_md * dpm);
int md_exit(t_dpm_md * dpm);

u32 md_get_status(int md_id);
u32 md_wnd_cnt_set(int md_id, int win_cnt);
u32 md_config(int md_id, t_md_win_cfg *win_cfg);
t_md_algo_buf * md_get_buf(int md_id);

#define DPM_INIT_MD \
	.type = DPM_TYPE_MD, \
	.dpmname = "MD", \
	.init = (int (*)(t_dpm * dpm))md_init, \
	.start = (int (*)(t_dpm * dpm))md_start, \
	.stop = (int (*)(t_dpm * dpm))md_stop, \
	.exit = (int (*)(t_dpm * dpm))md_exit,

#endif
