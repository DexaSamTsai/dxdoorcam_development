#ifndef __DPM_DCPC_H__
#define __DPM_DCPC_H__

typedef struct s_dpm_dcpcstitch
{
	DECLARE_DPM

	DECLARE_FL
	
	struct s_dpm * in1 ;

	u32 dcpc_cfg;
	u32 dcpc_lut_base_0;
	u32 dcpc_lut_base_1;
	u32 dcpc_lut_base_2;
	u32 dcpc_lut_base_3;

	u32 dcpc_y0_base_0;
	u32 dcpc_y0_base_1;
	u32 dcpc_y0_base_2;
	u32 dcpc_y0_base_3;
	u32 dcpc_uv0_base_0;
	u32 dcpc_uv0_base_1;
	u32 dcpc_uv0_base_2;
	u32 dcpc_uv0_base_3;

	u32 dcpc_y1_base_0;
	u32 dcpc_y1_base_1;
	u32 dcpc_y1_base_2;
	u32 dcpc_y1_base_3;
	u32 dcpc_uv1_base_0;
	u32 dcpc_uv1_base_1;
	u32 dcpc_uv1_base_2;
	u32 dcpc_uv1_base_3;

	u32 dcpc_para0_h1;
	u32 dcpc_para0_h2;
	u32 dcpc_para0_l1;
	u32 dcpc_para0_l2;

	u32 dcpc_para1_h1;
	u32 dcpc_para1_h2;
	u32 dcpc_para1_l1;
	u32 dcpc_para1_l2;
	
	u32 dcpc_para2_h1;
	u32 dcpc_para2_h2;
	u32 dcpc_para2_l1;
	u32 dcpc_para2_l2;

	u32 dcpc_para3_h1;
	u32 dcpc_para3_h2;
	u32 dcpc_para3_l1;
	u32 dcpc_para3_l2;	
	
	u32 dcpc_hts;
	u32 dcpc_vts;
	u32 dcpc_rotate_en;
	u32 dcpc_output_w;
	u32 dcpc_output_h;

	///< stitch parameters
	u32 stitch_bypass; //< stitch bypass
	u32 stitch_cmd;
	u32 stitch_pic0_left_color_info;
	u32 stitch_pic0_right_color_info;
	u32 stitch_pic0_width;
	u32 stitch_pic0_crop_width;
	u32 stitch_pic0_xpos;
	u32 stitch_pic0_target_xpos;
	u32 stitch_pic1_left_color_info;
	u32 stitch_pic1_right_color_info;
	u32 stitch_pic1_width;
	u32 stitch_pic1_crop_width;
	u32 stitch_pic1_xpos;
	u32 stitch_pic1_target_xpos;
	u32 stitch_pic2_left_color_info;
	u32 stitch_pic2_right_color_info;
	u32 stitch_pic2_width;
	u32 stitch_pic2_crop_width;
	u32 stitch_pic2_xpos;
	u32 stitch_pic2_target_xpos;
	u32 stitch_pic3_left_color_info;
	u32 stitch_pic3_right_color_info;
	u32 stitch_pic3_width;
	u32 stitch_pic3_crop_width;
	u32 stitch_pic3_xpos;
	u32 stitch_pic3_target_xpos;
	u32 stitch_seam0_width;
	u32 stitch_seam0_left_picid;
	u32 stitch_seam0_right_picid;
	u32 stitch_seam0_left_pic_xpos;
	u32 stitch_seam0_right_pic_xpos;
	u32 stitch_seam0_target_xpos;
	u32 stitch_seam0_para;
	u32 stitch_seam1_width;
	u32 stitch_seam1_left_picid;
	u32 stitch_seam1_right_picid;
	u32 stitch_seam1_left_pic_xpos;
	u32 stitch_seam1_right_pic_xpos;
	u32 stitch_seam1_target_xpos;
	u32 stitch_seam1_para;
	u32 stitch_seam2_width;
	u32 stitch_seam2_left_picid;
	u32 stitch_seam2_right_picid;
	u32 stitch_seam2_left_pic_xpos;
	u32 stitch_seam2_right_pic_xpos;
	u32 stitch_seam2_target_xpos;
	u32 stitch_seam2_para;
	u32 stitch_seam3_width;
	u32 stitch_seam3_left_picid;
	u32 stitch_seam3_right_picid;
	u32 stitch_seam3_left_pic_xpos;
	u32 stitch_seam3_right_pic_xpos;
	u32 stitch_seam3_target_xpos;
	u32 stitch_seam3_para;
	u32 stitch_line_buf0_base;
	u32 stitch_line_buf1_base;
	u32 stitch_vblank_adly;
	u32 stitch_vblank_bdly;
	u32 stitch_hblank_dly;
	u32 stitch_data_vld_dly;
	u32 stitch_io_mode;

	u32 flag_dcpcstitch_start;
	t_dp_loop loop_func;

	u8 reset_en;
}t_dpm_dcpcstitch;

int dcpcstitch_init(t_dpm_dcpcstitch * dpm);
int dcpcstitch_start(t_dpm_dcpcstitch * dpm);
int dcpcstitch_stop(t_dpm_dcpcstitch * dpm);
int dcpcstitch_exit(t_dpm_dcpcstitch * dpm);

#define DPM_INIT_DCPCSTITCH \
	.type = DPM_TYPE_DCPCSTITCH, \
	.dpmname = "DCPCSTITCH", \
	.init = (int (*)(t_dpm * dpm))dcpcstitch_init, \
	.start = (int (*)(t_dpm * dpm))dcpcstitch_start, \
	.stop = (int (*)(t_dpm * dpm))dcpcstitch_stop, \
	.exit = (int (*)(t_dpm * dpm))dcpcstitch_exit,

#endif
