#ifndef _VS_FPS_H_
#define _VS_FPS_H_

/*
 VS fps by dropping frame, sensor frame rate must be smaller than 32 
 */

typedef struct s_vs_fps_cfg
{
	u32 drop_mask;
	u8 current;
	u8 sensor_fps;
}t_vs_fps_cfg;

u8 vs_fps_init(t_vs_fps_cfg* fps_cfg);
u8 vs_frm_drop_cb(t_vs_fps_cfg* fps_cfg);
int vs_set_fps_drop(t_dpm* dpm, u32 frame_rate);

#endif
