#ifndef _DPM_ISPOUT_H_
#define _DPM_ISPOUT_H_

typedef struct s_dpm_ispout
{
	DECLARE_DPM
}t_dpm_ispout;

#define DPM_INIT_ISPOUT \
	.type = DPM_TYPE_ISPOUT, \
	.dpmname = "ISP",

#endif
