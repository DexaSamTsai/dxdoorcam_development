#ifndef _DPM_DPFBCAP_H_
#define _DPM_DPFBCAP_H_


int libfb_capture_start(t_dpm * dpm);
int libfb_capture_stop(t_dpm * dpm);

#endif
