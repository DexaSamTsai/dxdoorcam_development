#ifndef _DPM_RAWSCALE_H_
#define _DPM_RAWSCALE_H_

typedef enum{
	RAWSCALE_FACTOR_2X,   ///< factor is 2. scaledown
	RAWSCALE_FACTOR_167X, ///< factor is 1.67
	RAWSCALE_FACTOR_3X,   ///< factor is 3
	RAWSCALE_FACTOR_15X,  ///< factor is 1.5
	RAWSCALE_FACTOR_25X,  ///< factor is 2.5
}RAWSCALE_FACTOR;

typedef enum{
	RAWSCALE_RAW8,
	RAWSCALE_RAW10,
	RAWSCALE_RAW12,
}RAWSCALE_FORMAT;

typedef struct s_dpm_rawscale
{
	DECLARE_DPM

	u8 scale_factor;
	u8 raw_format;
	u8 hscale_en;
	u8 vscale_en;
	u8 in_hcrop_en;
	u8 in_vcrop_en;
	u8 out_vcrop_en;
	u32 hstart;
	u32 vstart;

	//private
	u32 reg_addr;
}t_dpm_rawscale;

int rawscale_init(t_dpm_rawscale * dpm);
int rawscale_start(t_dpm_rawscale * dpm);
int rawscale_stop(t_dpm_rawscale * dpm);
int rawscale_exit(t_dpm_rawscale * dpm);

#define DPM_INIT_RAWSCALE \
	.type = DPM_TYPE_RAWSCALE, \
	.dpmname = "RAWSCL", \
	.init = (int (*)(t_dpm * dpm))rawscale_init, \
	.start = (int (*)(t_dpm * dpm))rawscale_start,\
	.stop = (int (*)(t_dpm * dpm))rawscale_stop,\
	.exit = (int (*)(t_dpm * dpm))rawscale_exit,


#endif
