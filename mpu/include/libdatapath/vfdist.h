/** 
 * @file   mpu/include/libdatapath/vfdist.h
 * @brief  Video Frame Distributor
 *
 * @details mpu distributing video frames as different streams in A5
 *
 * Video Frame Distributor Instruction:
 *
 * 1. Each real stream need to be distributed will be allocated a distributor in mpu. 
 *
 * 2. A5 will access the distributed stream frame as from different streams. From mpu's perspective, they are virtual streams 
 *    all linked to one single stream. 
 *
 * 3. Distributor will recorded the status of each virtual stream, start the real stream when start_vs is first called in A5 and 
 *    stop the stream when all virtual vs have been called to stop. 
 *
 * 4. Distributor will manage the frame list of the real stream. 
 *
 * 5. Because a newly started virtual vs need to wait for an IDR frame, if different virtual streams are not started together, it is recommended that VIDEO GOP should not be too long.
 *
 * 6. The allocated frame list number for distributor should be the same as that of the real stream.
 *
 * 7. Only support distributing H.264/MIMG stream, not RAW stream.
 *
 * 8. Supported up to 32 virtual streams.
 *
 * @by MannaGao
 * @2017/01/23
 */
#ifndef _LIBVFDIST_H_
#define _LIBVFDIST_H_

#define VIRTUAL_VS_NOT_DONE		0x7f	

typedef struct s_vfdist_frm
{
	FRAME* vfrm;
	u32	vs_flag;///<待读取标记，添加进列表时，只有enabled的虚拟码流才会在对应位置上(1<<vs_id)标记为1
	u32	reading_flag;///<正在读取标记 
	struct s_vfdist_frm* next;
}t_vfdist_frm;

typedef struct s_VFDIST
{
	u32 num;	///< total frame list buffer number
	t_pool * pool;
	u8 * poolbuf;	///< frame list pool address
	u32 cnt;///< number of frames in current frame list
	
	t_vfdist_frm * head;	
	t_vfdist_frm * tail;

	t_dpm*	dpm;	///< linking to the real VS need to be distributed
	u32	running_mask;///< if vs enable, its corresponding position(1<<vs_id) in the running_mask will be set to 1
	u32	init_mask;///< if vs initialized, its corresponding position(1<<vs_id) in the running_mask will be set to 1
	u32 wait_ifrm_mask;///< a newly started vs should wais an I frame as its first frame
}VFDIST;	//VIDEO FRAME DISTRIBUTOR

typedef struct s_virtual_vs
{
	u8	id;	///should be less than 32
	u32	mask;
	VFDIST	* vfdist;
}t_virtual_vs;


#define VFDIST_DECLARE(name, vs_name, listnum) \
static unsigned char  __attribute__ ((aligned(4))) g_vfdistlist_##name##_poolbuf[POOL_OVERHEAD_SIZE + sizeof(t_vfdist_frm) * listnum + 4]; \
static VFDIST name = \
{ \
	.poolbuf = g_vfdistlist_##name##_poolbuf, \
	.num = listnum, \
	.dpm = &vs_name, \
};

int virtual_vs_init(t_virtual_vs* vt_vs);
int virtual_vs_start(t_virtual_vs* vt_vs);
int virtual_vs_stop(t_virtual_vs* vt_vs);
FRAME* virtual_vs_get_frm(t_virtual_vs* vt_vs);
int virtual_vs_free_frm(t_virtual_vs* vt_vs, FRAME* vs_frm);
int virtual_vs_exit(t_virtual_vs* vt_vs);
int virtual_vs_set_event(VFDIST* vfdist);
int virtual_vs_clear_event(VFDIST* vfdist);
#endif
