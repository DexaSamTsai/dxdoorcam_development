#ifndef _DPM_VIF_H_
#define _DPM_VIF_H_

typedef struct s_dpm_vif
{
	DECLARE_DPM
	
	u8 no_kick;
	u8 yuv420_in;
	u8 dcpc_yuv420_in;
	u8 frm_skip_cnt;//if use ECIF for encoder, can set this to skip frame, it can save energy.
	u16 intr_mask;
	u16 max_in_width;//in multi-streams case, please set this to be the width of the maximum encoded width
	u32 buf_base;
	///internal
	u8 working_mode;
	u8 kick_mode;
	u8 mbl_fifo_depth;
	u8 mbl_fifo_th;
	u8 reset_en;
	u32 reg_addr;
//INTERNAL_START
    void (*cb_intr_ve)(t_dpm* dpm_ve, u32 intr_flag);  ///< callback for extra interrupt process 
    void (*cb_intr_img)(t_dpm* dpm_img, u32 intr_flag);  ///< callback for extra interrupt process 
	t_dpm *out_dpm_img;
	t_dpm *out_dpm_ve;
//INTERNAL_END
}t_dpm_vif;

int vif_init(t_dpm_vif * dpm);
int vif_start(t_dpm_vif * dpm);
int vif_stop(t_dpm_vif * dpm);
int vif_exit(t_dpm_vif * dpm);

#define DPM_INIT_VIF \
	.type = DPM_TYPE_VIF, \
	.dpmname = "VIF", \
	.init = (int (*)(t_dpm * dpm))vif_init, \
	.start = (int (*)(t_dpm * dpm))vif_start, \
	.stop = (int (*)(t_dpm * dpm))vif_stop, \
	.exit = (int (*)(t_dpm * dpm))vif_exit, 

#endif
