#ifndef _DPM_ECIF_H_
#define _DPM_ECIF_H_

typedef enum
{
	ECIF_FORMAT_RAW6_DEC8,
	ECIF_FORMAT_RAW6_DEC10,
	ECIF_FORMAT_RAW8_NODEC,
	ECIF_FORMAT_RAW8_DEC10,
	ECIF_FORMAT_RAW8_DEC12,
	ECIF_FORMAT_RAW10_NODEC,
	ECIF_FORMAT_RAW12_NODEC,
	ECIF_FORMAT_YUV422_DEC8,
	ECIF_FORMAT_YUV422_NODEC,
	ECIF_FORMAT_YUV420_DEC8,
	ECIF_FORMAT_YUV420_NODEC,
	ECIF_FORMAT_MIMGYUV422,
	ECIF_FORMAT_MIMGYUV420,
}ECIF_DATA_FORMAT;

typedef struct s_dpm_ecif
{
	DECLARE_DPM

	DECLARE_FL

	u8 frame_num; ///<the number of frame needs to fetch,  only used in software start mode, 0~255 express 1~256.
	u8 p_vsync;///<blank line number of vsync period, 0~255 express 1~256. SOF is at the end(falling edge) of VSYNC
	u8 p_v2h;///<blank line number between vsync and the first href, 0~255 express 1~256. 
	u8 p_h2v;///<blank line number between the last href and the next frame vsync, 0~255 express 1~256.
	u8 p_h2e;///<blank pixel number between the last href and eof,  0~255 express (1~256)*16 

	u8 cpp0;///<valid pixels between dummy(cpp1) pixels
	u8 cpp1;///<dummy pixels between valid(cpp0) pixels
	///<cpp0 and cpp1 is for output timing, if both set to 0, then one cycle output 1 pixel.
	///<if cpp0 = 2, cpp1 = 1, then between every two valid pixels, there will be one dummy pixel

	u8 int_mask;
	u8 data_format;///<ECIF_DATA_FORMAT
#define SOFTWARE_HANDSHAKE  (0)
#define HARDWARE_HANDSHAKE  (1)
	u8 handshk;///<0: start ECIF by software; 1: ecif hand shake with FB automatically
	u8 yuv420in_yuv422out_en;
	u8 pingpong_en;
	u8 wait_antishake;
	u16 p_hblk;///<blank pixel number at the end of every line href,  0~255 express (1~256)*8.
	u16 crop_x;
	u16 crop_y;
	u32 yaddr;
	u32 uvaddr;
	u32 offset;

	//private
	t_dp_loop loop_func;

	u32 reg_addr;
}t_dpm_ecif;

int ecif_sanity_check(t_dpm_ecif * dpm);
int ecif_init(t_dpm_ecif * dpm);
int ecif_start(t_dpm_ecif * dpm);
int ecif_stop(t_dpm_ecif * dpm);
int ecif_exit(t_dpm_ecif * dpm);

#define DPM_INIT_ECIF \
	.type = DPM_TYPE_ECIF, \
	.dpmname = "ECIF", \
	.sanity_check = (int (*)(t_dpm * dpm))ecif_sanity_check, \
	.init = (int (*)(t_dpm * dpm))ecif_init, \
	.start = (int (*)(t_dpm * dpm))ecif_start, \
	.stop = (int (*)(t_dpm * dpm))ecif_stop, \
	.exit = (int (*)(t_dpm * dpm))ecif_exit, 

#endif
