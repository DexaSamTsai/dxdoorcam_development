#ifndef __DPM_ANTISHAKE_H__
#define __DPM_ANTISHAKE_H__

typedef struct s_dpm_antishake
{
	DECLARE_DPM
	
//INTERNAL_START
	volatile int shift_x;
	volatile int shift_y;
	volatile int anti_done;
	t_dp_loop loop_func;
//INTERNAL_END
}t_dpm_antishake;

typedef struct POINT
{
    int x;
    int y;
} Point;


//int as_det(int ncorners,Point *prev,Point *curr);
int as_det(int ncorners,Point *prev,Point *curr,Point *tr);

int antishake_init(t_dpm_antishake * dpm);
int antishake_start(t_dpm_antishake * dpm);
int antishake_stop(t_dpm_antishake * dpm);
int antishake_exit(t_dpm_antishake * dpm);

#define DPM_INIT_ANTISHAKE \
	.type = DPM_TYPE_ANTISHAKE, \
	.dpmname = "ANTISHAKE", \
	.init = (int (*)(t_dpm * dpm))antishake_init, \
	.start = (int (*)(t_dpm * dpm))antishake_start, \
	.stop = (int (*)(t_dpm * dpm))antishake_stop, \
	.exit = (int (*)(t_dpm * dpm))antishake_exit,

#endif
