#ifndef _DPM_RGBIR_H_
#define _DPM_RGBIR_H_

typedef enum IR_COEFF_STATUS
{
	ISP_FULL_COEFF,
	ISP_HALF_COEFF,
} IR_COEFF_STATUS;
typedef enum LOWLUX_SAT_STATUS
{
	ISP_FULL_SAT,
	ISP_TUNE_SAT,
	ISP_NO_SAT,
} LOWLUX_SAT_STATUS;

typedef struct s_dpm_rgbir
{
	DECLARE_DPM

	u8 bypass; //1: bypass rgbir.

	///<rgbir timing config: RGBIR will buffer 6 lines before output, the timing of the last 6 lines of a frame need to be tuned some times
	u8 dummy_manual_en;///<1: manually config hblk for the last 6 lines. 0: use RGBIR internal dummy mode
	u16 dummy_hblk;///<if dummy_manual_en = 1, need to config this for the dummy_hblk of the last 6 pixel lines

	u8 ir_check_en;
	u8 desat_algo_no;
	u32 rgbir_reg; 
	u32 isp_sat_thr1;
	u32 isp_sat_thr2;
	u32 isp_coeff_thr1;
	u32 isp_coeff_thr2;
	u32 isp_coeff_value;
	u32 lum_in_de_thre;
	u32 lum_out_de_thre;

	//private
	u8 target_sat;
	u32 coeff_status;
	u32 sat_status;
	u8 last_sat;
	u8 de_stat_no; 
#define CGAIN_LIST_CNT 5
	u32 cgain_list[CGAIN_LIST_CNT];

	u32 reg_back[23];//for rgbir reset back up
}t_dpm_rgbir;


int rgbir_init(t_dpm_rgbir * dpm);
int rgbir_start(t_dpm_rgbir * dpm);
int rgbir_stop(t_dpm_rgbir * dpm);
int rgbir_exit(t_dpm_rgbir * dpm);
int rgbir_config(t_dpm_rgbir * dpm);
int rgbir_reset_back(t_dpm_rgbir * dpm);
int rgbir_reset_config(t_dpm_rgbir * dpm);

void isp_ir_check(struct s_dpm_ispidc * dpm_ispidc, t_dpm_rgbir * dpm_rgbir);

#define DPM_INIT_RGBIR \
	.type = DPM_TYPE_RGBIR, \
	.dpmname = "RGBIR", \
	.init = (int (*)(t_dpm * dpm))rgbir_init, \
	.start = (int (*)(t_dpm * dpm))rgbir_start, \
	.stop = (int (*)(t_dpm * dpm))rgbir_stop, \
	.exit = (int (*)(t_dpm * dpm))rgbir_exit,

#endif
