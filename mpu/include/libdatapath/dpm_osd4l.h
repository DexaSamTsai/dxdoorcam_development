#ifndef _DPM_OSD4L_H_
#define _DPM_OSD4L_H_

#define OSD4L_WIN_TOTAL_COUNT   (4)

/**
 * @name color
 * @details format:0xYYUUVV
 * @{
 */
#define YUV_BLACK	0x007F7F
#define YUV_BLUE		0x1DFE6A
#define YUV_GREEN	0x952A14
#define YUV_RED		0x4C54FF
#define YUV_CYAN		0xB2AA00
#define YUV_MAGENTA	0x69D4Ea
#define YUV_YELLOW	0xE10094
#define YUV_WHITE	0xFF7F7F
/** @} */

typedef struct s_osd4l_win_cfg
{
	u8 win_no;
	u8 mem_id;
//INTERNAL_START
	u8 win_update;
//INTERNAL_END
	u8 win_disable;
	u16 win_hsize;
	u16 win_vsize;
	u16 win_hstart;
	u16 win_vstart;
	u32 win_memaddr;
	u32 win_max_offset;
	u32 color[4]; ///< (4LUT):osd color value. 0---23: is YUV420 color.  24--32 is transparency, option is 0--7. 0: indicates transparent and display background layer, 7: indicates opaque and display menu osd layer.
}t_osd4l_win_cfg;

typedef struct s_dpm_osd4l
{
	DECLARE_DPM

	u8 bypass;
	void (*cb_osd4l_tick2_draw)(void);
	u32 wait_cycle; ///< the above callback function and parameter are used in the case of single stream.
	t_osd4l_win_cfg osd_win[OSD4L_WIN_TOTAL_COUNT];
	u8 default_sel;///< default selection when osd4l not running, 0: ecif0, 1: yuvscale1, 2: ecif1, 3: dcpcstitch
//INTERNAL_START
	u8 osd4l_en;
	u8 ecif_switch; ///<this parameter is used in the case of multi-streams. 
//INTERNAL_END
}t_dpm_osd4l;

int osd4l_init(t_dpm_osd4l * dpm);
int osd4l_start(t_dpm_osd4l * dpm);
int osd4l_stop(t_dpm_osd4l * dpm);
int osd4l_exit(t_dpm_osd4l * dpm);

int osd4l_update(t_osd4l_win_cfg * cfg, u8 osd_id);
u32 osd4l_get(u8 win_no, u8 osd_id);
int osd4l_draw(void);
void osd4l_tick2_draw(void);
int osd4l_enable(int win_no);
int osd4l_disable(int win_no);

#define DPM_INIT_OSD4L \
	.type = DPM_TYPE_OSD4L, \
	.dpmname = "OSD4L", \
	.init = (int (*)(t_dpm * dpm))osd4l_init, \
	.start = (int (*)(t_dpm * dpm))osd4l_start, \
	.stop = (int (*)(t_dpm * dpm))osd4l_stop,\
	.exit = (int (*)(t_dpm * dpm))osd4l_exit,

#endif
