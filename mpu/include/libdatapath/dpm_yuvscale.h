#ifndef _DPM_YUVSCALE_H_
#define _DPM_YUVSCALE_H_

typedef struct s_dpm_yuvscale
{
	DECLARE_DPM
	
	u8 yuv422_out;
	u8 default_sel;///< default selection when yuvscale not running, 0: dcpcstitch, 1: ecif0, 2: cif, 3: ispout0, 4: ispout1, 5: ecif1

	//private
	u8	delay_done;
	u8  stop_in_intr; ///< check whether yuvscale_stop is called in interrupt or thread. 0: stop in thread, 1: in other interrupt handler.
	u32 reg_addr;	

}t_dpm_yuvscale;

int yuvscale_init(t_dpm_yuvscale * dpm);
int yuvscale_start(t_dpm_yuvscale * dpm);
int yuvscale_stop(t_dpm_yuvscale * dpm);
int yuvscale_exit(t_dpm_yuvscale * dpm);

#define DPM_INIT_YUVSCALE \
	.type = DPM_TYPE_YUVSCALE, \
	.dpmname = "YUVSCL", \
	.init = (int (*)(t_dpm * dpm))yuvscale_init, \
	.start = (int (*)(t_dpm * dpm))yuvscale_start,\
	.stop = (int (*)(t_dpm * dpm))yuvscale_stop, \
	.exit = (int (*)(t_dpm * dpm))yuvscale_exit,

#endif
