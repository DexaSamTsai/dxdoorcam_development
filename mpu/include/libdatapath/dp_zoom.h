#ifndef _DPM_DPZOOM_H_
#define _DPM_DPZOOM_H_


u32 dpzoom_noscale_isp_crop_size(t_dpm *dpm, u16 crop_width, u16 crop_height);
u32 dpzoom_isp_crop_coordinate(t_dpm *dpm, u16 x, u16 y);
u32 dpzoom_noscale_ecif_crop_size(t_dpm *dpm, u16 crop_width, u16 crop_height);
u32 dpzoom_ecif_crop_coordinate(t_dpm *dpm, u16 x, u16 y);
u32 dpzoom_scale_size(t_dpm *dpm, u16 scale_width, u16 scale_height);
u32 dpzoom_ecif_crop_config(t_dpm *dpm, u16 x, u16 y, u16 crop_width, u16 crop_height, u32 ecif_timing);

#endif
