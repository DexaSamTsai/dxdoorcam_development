#ifndef _DPM_DVPCIF_H_
#define _DPM_DVPCIF_H_

typedef struct s_dpm_dvpcif
{
	DECLARE_DPM

	u8 format_raw;
	u8 reverse_clk;
	u8 inmode_656;		///< set to 1 for 656 mode
	u8 fdrop;			///< BIT5: dropEn; BIT4: EDrop.
	u16 crop_hoft;		///< crop x offset
	u16 crop_voft;		///< crop y offset
	u32 stop_timeout;	///< timeout when call stop
	u8 downscale;		///< 01: 2X, 10: 4X, 11: 8X

	u8 padding0;
	u8 padding1;
	u8 padding2;
	u8	colorbar_en;
	u32 padding3;
}t_dpm_dvpcif;


int dvpcif_init(t_dpm_dvpcif * dpm);
int dvpcif_start(t_dpm_dvpcif * dpm);
int dvpcif_stop(t_dpm_dvpcif * dpm);
int dvpcif_exit(t_dpm_dvpcif * dpm);

#define DVP0_PIN_EN do{ \
	WriteReg32(REG_SC_ADDR + 0x0, ReadReg32(REG_SC_ADDR + 0x0) & ~(BIT8)); \
	SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SNR_CCLK | SC_PINSEL0_SNR_PCLK | SC_PINSEL0_SNR_VSYNC | SC_PINSEL0_SNR_HREF | SC_PINSEL0_SNR_D9 | SC_PINSEL0_SNR_D8 | SC_PINSEL0_SNR_DATA7TO0 ); \
	SC_PIN0_EN_MUL(SC_PINEN0_DVP_PCLK | SC_PINEN0_DVP_CCLK | SC_PINEN0_DVP_HREF | SC_PINEN0_DVP_VSYNC | SC_PINEN0_DVP_D9 | SC_PINEN0_DVP_D8 | SC_PINEN0_DVP_D7 | SC_PINEN0_DVP_D6 | SC_PINEN0_DVP_D5 | SC_PINEN0_DVP_D4 | SC_PINEN0_DVP_D3 | SC_PINEN0_DVP_D2 | SC_PINEN0_DVP_D1 | SC_PINEN0_DVP_D0); \
}while(0)

#define DVP1_PIN_EN do{ \
	/* NOTE: DVP1 as MIPI, always en */ \
}while(0)

#define DPM_INIT_DVPCIF \
	.type = DPM_TYPE_DVPCIF, \
	.dpmname = "DVP", \
	.init = (int (*)(t_dpm * dpm))dvpcif_init, \
	.start = (int (*)(t_dpm * dpm))dvpcif_start, \
	.stop = (int (*)(t_dpm * dpm))dvpcif_stop, \
	.exit = (int (*)(t_dpm * dpm))dvpcif_exit,


#endif
