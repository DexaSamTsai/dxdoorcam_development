#ifndef _DPM_MIPICIF_H_
#define _DPM_MIPICIF_H_

typedef struct s_dpm_mipicif
{
	DECLARE_DPM

	/*if MIPI cif is followed by RGBIR, output_width/output_height/crop_x/crop_y all have to be 4 pixels aligned*/
	//private
	u32 reg_addr;
	u32 crop_x;
	u32 crop_y;
#define MIPICIF_SEL_MIPIRXVC0   (0)
#define MIPICIF_SEL_MIPIRXVC1   (1)
	u8 vc_select; ///< only for HDR case, select virtual channel0 or virtual channel1
	u8 loopback_en;///< config for HDR loopback mode
	u32 frm_cnt;
}t_dpm_mipicif;

int mipicif_init(t_dpm_mipicif * dpm);
int mipicif_start(t_dpm_mipicif * dpm);
int mipicif_stop(t_dpm_mipicif * dpm);
int mipicif_exit(t_dpm_mipicif * dpm);

#define DPM_INIT_MIPICIF \
	.type = DPM_TYPE_MIPICIF, \
	.dpmname = "MIPCIF", \
	.init = (int (*)(t_dpm * dpm))mipicif_init, \
	.start = (int (*)(t_dpm * dpm))mipicif_start,\
	.stop = (int (*)(t_dpm * dpm))mipicif_stop, \
	.exit = (int (*)(t_dpm * dpm))mipicif_exit,


#endif
