#ifndef _LIBDATAPATH_H_
#define _LIBDATAPATH_H_

/* DPM: datapath module */

/* Use Tree to present the datapath:
   in: node's father
   out: node's first son
   next: node's brothers
 */
/* uart log of LOG_ERR
@: MD overflow
   */
/* uart log of LOG_WARNING
h: VE drop one frame   
k: JPEG frame drop

//following is for stagHDR+RGBIR case only:
Y: s-channel fb(FB2) frame done but no l-channel fb(FB1) frame done intr
*: no FB2 at next rx sof, reset FB2
%: no FB1 at next rx sof, reset FB1
+: at RX0VC0 eof, no fb1 frame done interrupt
!: l-channel fb(FB1) frame done but no s-channel fb(FB2) frame done intr at rx0vc1 eof
k: frame drop at mipi rx eof
#: FB0/FB1/FB2 buffer full(This should not happen if timing is correct)
*/

/* uart log of LOG_NOTICE 
s: MIPIRX0 sof
S: MIPIRX1 sof
e: MIPIRX0 eof
t: MIPIRX1 eof
m: MIPICIF0 frame done
M: MIPICIF1 frame done
j: FB0 frame done or JPEG frame done
f: FB1 frame done
g: FB2 frame done
q: FB3/FB4 frame done 
I: idc frame done
c: ECIF frame done
E: VE frame done
V: VIF eof
D: ddr writing
R: ddr reading
*/

struct s_dpm;

#define DECLARE_DPM \
	u8 type; \
	u8 id; \
	u8 flag; \
	u8 status; \
	u16 output_width; \
	u16 output_height; \
	u8 output_fmt; \
	u8 dpm_padding; \
	u16 dpm_padding1; \
	struct s_dpm * in ; /* input in the datapath chain */ \
	struct s_dpm * out ; /* output in the datapath chain */ \
	struct s_dpm * next ; /* next in the linklist */ \
	/* below items only set by DPM_INIT_XXX, don't need to set in config.c */ \
	char * dpmname; /* print only, set by DPM_INIT_XXX */ \
	int (*sanity_check)(struct s_dpm * dpm); /* 检查dpm的配置(比如in/out)是否符合规范. ret:0:OK. <0:error, set by DPM_INIT_XXX */ \
	int (*init)(struct s_dpm * dpm); /* 初始化模块, 只在libdatapath_init()里调用一次，这里不要有操作硬件的地方，尽量只是初始化数据结构（比如链表）set by DPM_INIT_XXX */ \
	int (*start)(struct s_dpm * dpm); /* 启动模块 set by DPM_INIT_XXX */ \
	int (*enable)(struct s_dpm * dpm); /* enable one stream */ \
	int (*disable)(struct s_dpm * dpm); /* disable one stream */ \
	int (*stop)(struct s_dpm * dpm); /* 停止模块 set by DPM_INIT_XXX */ \
	int (*exit)(struct s_dpm * dpm); /* 关闭模块，只在libdatapath_exit()时调用一次。set by DPM_INIT_XXX */

#define DPM_FLAG_END	0x01 //默认情况下，一个节点的所有孩子表示父亲同时送数据给这些孩子。但是在某些情况下，可能需要发送给一些孩子，结束后再发送给另外一些孩子。比如:同一个FB，先同时给ECIF0_mode0和ECIF1，做完后再给ECIF0_mode1。这个时候，DPM_FLAG_END标记表示当前的模块配置完马上就要启动。启动完成才能进行链表中的下一个模块的处理。在这里就是需要设置ecif1->flag = DPM_FLAG_END。这样datapath在检索到ecif1的时候就不会往下走，会等ecif0_mode0和ecif1处理完再处理ecif0_mode1。只有ECIF模块支持这个操作。其他模块都是必须同时发送数据给所有孩子的.
#define DPM_FLAG_DISABLED	0x02 //表明之后的dpchain都是没有使能的。用来libvs_enable()和libvs_disable()
#define DPM_FLAG_TODISABLE	0x04 //需要disable，和DISABLED FLAG不会并存。
#define DPM_FLAG_NOFASTBOOT	0x08 //don't start this stream in fastboot mode

/* for dpm->status */
#define DPM_STATUS_IDLE		0x00
#define DPM_STATUS_INITED	0x01 //dpm->init() called, and not start(or stop() called).
#define DPM_STATUS_RUNNING	0x02 //dpm->start() called
#define DPM_STATUS_DONE		0x03 //dpm->start() called and done(can call dpm->stop() )

/* for dpm->type */
#define DPM_TYPE_IMG		0x00
#define DPM_TYPE_FB			0x01
#define DPM_TYPE_ISPOUT		0x02
#define DPM_TYPE_ISPIDC		0x03
#define DPM_TYPE_RAWSCALE	0x04
#define DPM_TYPE_DVPCIF		0x05
#define DPM_TYPE_MIPICIF	0x06
#define DPM_TYPE_MIPIRX		0x07
#define DPM_TYPE_MD			0x08
#define DPM_TYPE_YUVSCALE	0x09
#define DPM_TYPE_ECIF		0x0a
#define DPM_TYPE_OSD4L		0x0b
#define DPM_TYPE_VIF		0x0c
#define DPM_TYPE_SENSOR		0x0d
#define DPM_TYPE_OSDRLC     0x0e
#define DPM_TYPE_DCPCSTITCH 0x0f
#define DPM_TYPE_VFIFO      0x10
#define DPM_TYPE_RGBIR      0x11
#define DPM_TYPE_PDAF       0x12
#define DPM_TYPE_ANTISHAKE  0x13
#define DPM_TYPE_VE			0x14
#define DPM_TYPE_OVF        0x15
#define DPM_TYPE_MAX		(DPM_TYPE_OVF+1)
/* for dpm->datatype */
#define DPM_DATAFMT_NA 			0x00 //default datatype not configured
#define DPM_DATAFMT_YUV420		0x01
#define DPM_DATAFMT_YUV422		0x02
#define DPM_DATAFMT_YUV444		0x03
#define DPM_DATAFMT_RAW8		0x04
#define DPM_DATAFMT_RAW10		0x05
#define DPM_DATAFMT_RAW12		0x06

typedef struct s_dpm
{
	DECLARE_DPM
}t_dpm;

/* main()里调用的loop函数接口定义 */
struct s_dp_loop;
typedef struct s_dp_loop
{
	int (*func)(void * arg);
	void * arg;
	struct s_dp_loop * next;
}t_dp_loop;

/* fl(frame loader) is the base class of ecif/idc/dcpc */
#define DPM_FL_CHAINSTATUS_IDLE		0x0
#define DPM_FL_CHAINSTATUS_RUNNING	0x1
#define DPM_FL_CHAINSTATUS_DONE		0x2

#define DECLARE_FL \
	u32 chain_status; /* 判断一个ecif chain done的方法是在ecif_loop()里判断chain里所有类型为VE/IMG/MD的节点的状态都为DONE,并且ECIF本身也是DONE */\
	u32 timeout_wait; /* Set timeout for VS no frame done error process, unit in 0.1 ms, e.g. 200 for 20 ms. Please enable TICK2 in menuconfig.*/\
	u8	tickset_tag; /*	set to 1 if tick has been enabled after ecif frame doen. clear in tick intr or dpchain_check_done. for VS no frame done error process. */ \
	u8	timeout_err; /*	timout_tag: set to 1 if dpchain_check_done timeout, for VS no frame done error process. */ 

typedef struct s_dpm_fl
{
	DECLARE_DPM

	DECLARE_FL
}t_dpm_fl;

#include "vs_fps.h"
#include "dpm_fb.h"
#include "dpm_ispout.h"
#include "dpm_ispidc.h"
#include "dpm_rawscale.h"
#include "dpm_dvpcif.h"
#include "dpm_mipirx.h"
#include "dpm_mipicif.h"
#include "dpm_md.h"
#include "dpm_yuvscale.h"
#include "dpm_ecif.h"
#include "dpm_osd4l.h"
#include "dpm_osdrlc.h"
#include "dpm_vif.h"
#include "vfdist.h"
#include "dpm_ve.h"
#include "dpm_img.h"
#include "dpm_rgbir.h"
#include "dpm_sensor.h"
#include "dpm_vfifo.h"
#include "dpm_antishake.h"
#include "dpm_dcpcstitch.h"
#include "vs.h"
#include "dp_zoom.h"
#include "dp_fbcap.h"
#include "vs_add_meta.h"
#include "dpm_ovf.h"

#define VS_MAX_STREAMS 8
#define VIRTUAL_VS_MAX_STREAMS 8//Maximum: 32. Please make sure VENC_MAX_STREAMS(in share/media/vs/vs_ctl.c) is defined the same

typedef struct s_libdatapath_cfg
{
	t_dpm * head; //一般情况下，head为DPM_TYPE_SENSOR link list
	//private
	t_dpm * vs_map[VS_MAX_STREAMS];
	t_dpm * osd_map[VS_MAX_STREAMS];
	t_dpm * md_map[VS_MAX_STREAMS];
	t_dpm * ispidc_map[VS_MAX_STREAMS];
//	u32 reserved;
#define DATAPATH_STA_IDLE 0
#define DATAPATH_STA_STARTED 1
#define DATAPATH_STA_STOPPED 2
	u32 datapath_sta;
	u8	total_ve_strm_num;//total number of ve stream running. 

	t_virtual_vs * vtvs_map[VIRTUAL_VS_MAX_STREAMS];//if need video frame distributed as different streams in A5, config this in datapath

#define DATAPATH_FASTBOOT_EN	0x01
#define DATAPATH_FASTBOOT_DEBUG	0x02
#define DATAPATH_FASTBOOT_2STAGE	0x04
#define DATAPATH_FASTBOOT_VDEBUG 0x08 //enable:record from fps1. disable:record from fps2
	u32 fastboot_flag;	//fast boot flag
	u8 fastboot_stage; ///< FAST_STAGE_XXX
	u8 fastboot_cur_fps1cnt; ///< private for current fps1 cnt
	u8 fastboot_cur_fps2cnt; ///< private for current fps1 cnt
	u8 fastboot_fps2_fps; ///< private, fps2 fps, 24/15...
	u8 fastboot_mipirx; ///< private, the mipirx index to reset between fps1->fps2
	t_dpm * fastboot_isp_out;
	u32 (*fastboot_vs_handle_cmd)(int vs_id, u32 cmd, u32 arg);

	//private
	unsigned char default_TargetYLow; //default TargetY before brightness. load in libisp_init after sensor isp init, used when user set the brightness.
	unsigned char default_TargetYHigh;
	unsigned char isp_EOF_flag;
	int fastboot_exp; //store the manual exp from libisp_set_exp()
	int fastboot_gain; //the manual gain from libisp_set_gain()
	int fps1_exp; //before FPS1DONE, used as fps1 exp/gain. after FPS2RDY, used as fps2 exp
	int fps1_gain;
	int isp_mode;	//store the manual isp mode before isp start 
	int fps1_anagain;
	int fps1_awb0;
	int fps1_awb1;
	int fps1_awb2;

	void (*cb_multi_sensor_sync)(void); ///< call back for multi sensors sync
	u32 (*handle_cmd)(u32 cmd, u32 arg, u32 * handled);
}t_libdatapath_cfg;

//INTERNAL_START
/* used by rom/ only */
extern t_libdatapath_cfg * g_libdatapath_cfg;
//INTERNAL_END

extern t_libdatapath_cfg libdatapath_cfg[];

/* APIs */

int libdatapath_init(t_libdatapath_cfg * cfg);
int libdatapath_start(void);
int libdatapath_stop(void);
int libdatapath_loop(void);
int libdatapath_exit(void);

/* Internal API for dpm_xxx */
int libdatapath_loop_add(t_dp_loop * dp_loop);
int libdatapath_loop_del(t_dp_loop * dp_loop);
void libdatapath_loop_signal(void);

/* 只操作当前dpm */
/* private API.
int dpm_init(t_dpm * dpm);
int dpm_start(t_dpm * dpm);
int dpm_stop(t_dpm * dpm);
*/

/* 操作当前dpm及其孩子dpm */
int dpchain_init(t_dpm * dpm, int force);
int dpchain_start(t_dpm * dpm);
int dpchain_stop(t_dpm * dpm);
int dpchain_exit(t_dpm * dpm);
int dpchain_enable(t_dpm * dpm);
int dpchain_disable(t_dpm * dpm);
int dpchain_check_done(t_dpm * dpm);
int dpchain_check_timeout(t_dpm * dpm);
int dpchain_timeout_find(t_dpm * dpm);
int dpchain_check_disable(t_dpm * dpm);
int dpchain_check_manual_mode(t_dpm * dpm);
int dpchain_check_yscl_stop_mode(t_dpm * dpm);

/* debug */
void libdatapath_print(void);

int libdatapath_fastinit(int dpcfg_id);

int libdatapath_faststart(void);

/**
 * @brief check libdatapath fastboot status
 * @return FAST_STAGE_XXX
 */
int libdatapath_fast_status(void);

/**
 * @brief setup the fps2 fps
 */
int libdatapath_fast_fps2fps(int fps);

int libdatapath_fast_fps2start(void);

int libdatapath_fast_simu(int dpcfg_id);

//dpm debug
extern unsigned int dpm_syslog_disable;
void dpm_syslog(unsigned int dpm_type, int priority, const char * fmt0, ...);

#endif
