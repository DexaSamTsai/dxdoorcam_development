#ifndef _LIBVS_H_
#define _LIBVS_H_

//启动vs
//返回0: 已经启动
//返回<0: 出错
int libvs_enable(t_dpm * dpm);

//disable，如果是ECIF的模式，会等到当前frame做完再停止
//返回0: 已经被disable
//返回1: 已经下了disable指令，但是还没有完成。需要应用不断的调用disable来查看进度
//返回<0: 出错
int libvs_disable(t_dpm * dpm);

void vstream_free_frame(t_dpm* dpm, FRAME* frm);
FRAME* vstream_get_frame(t_dpm* dpm);
//internal, for dpm_vs or dpm_img only
int libvs_disable_force(t_dpm * dpm);

/* following API for VIF->VE+MIMG non-ECIF dual stream case only.
 * In such datapath, VIF will keep on running after all VS have been stopped, if need to crop in ISP, need to stop the whole
 * chain from MIPI and start the chain again when crop configuration is done
 */
int libvs_chain_disable_force(t_dpm* dpm);

int libvs_enable_manual(t_dpm * dpm, u8 dpmtype_from, u8 dpmtype_to);

int libvs_disable_manual(t_dpm * dpm, u8 dpmtype_from, u8 dpmtype_to, u32 timeout);

int libvs_disable_force_manual(t_dpm* dpm, u8 dpmtype_from, u8 dpmtype_to);

u32 vstream_handle_cmd(u32 cmd, u32 arg);
#endif
