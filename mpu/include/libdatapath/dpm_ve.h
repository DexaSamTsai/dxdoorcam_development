#ifndef _DPM_VE_H_
#define _DPM_VE_H_

#define	SPS_PPS_BUF_SIZE 64 

typedef enum {
    RC_FEATURE,		//skip the current frame, the encoded frame data will be abandoned
    RC_MBLINE,		//encoder auto skipped frame, mainly used when A/V sync is required
}RATE_CTL_MODE;

typedef enum {
    ADD_SPS_NULL = 0,		//use HW encoded sps/pps
    ADD_SPS_MODE0 = 1,		//add zeros after sps/pps to reach 64 bytes aligned.
    ADD_SPS_MODE1 = 2,		//add zeros before sps/pps to reach 64 bytes aligned.
}ADD_SPS_MODE;

typedef struct
{
	RATE_CTL_MODE rc_mode;
	u32 bit_rate;

	u8	init_qp;	///<初始化QP
	u8 	i_p_qp_delta;///<P帧和I帧的QP差值，范围为1 ~ 15，默认为1
	u8	min_qp[2];///<min_qp[0] 为I帧最小QP, min_qp[1] 为P帧最小QP
	u8	max_qp[2];///<同上，设置最大QP

	u8	adp_gop_en;		///<开启动态GOP
	u8  max_gop_size;	///< 最大GOP size 
	u8  min_gop_size;	///< 最小GOP size 
	u8	init_gop_size;	///< the size of initial GOP
	u8 	init_gop_num;	///< the number of the the GOP of init_gop_size before gop size switch to max_gop_size
	
	u8	force_i_frame;		///< 设置1，强制下一帧编码帧为I帧 
	u8	auto_skip_offset;	///< use HW algorithm to decide MB skip
	u16 init_dstn;		///< 初始化DSTN
	u16 init_grad;		///< 初始化Gradient
}t_ve_rc_cfg;

typedef struct
{
	u16 window_x;		///< should be integral multiple of 16
	u16 window_y;		///< should be integral multiple of 16
	u16 window_width;	///< should be integral multiple of 16
	u16 window_height;	///< should be integral multiple of 16
	u8  mdetect_mv_th; ///< motion vector threshold, the unit is 1/4 pixel. For example, by default it is 128, that is to say, only the MBs with motion vector larger than 2mb(128/4=32 pixels) will be counted as moving MBs for IntraWidthMaxMVPro. range from 1 ~ 255
    void (*motion_detected_status)(u16 DetectedMBNum); ///<return motion statics of each P frame, refer is number of Intra MB and MBS with MV larger than mdetect_mv_th. channel_idx is returned to denote the encoding channel. if single channel encoding, it is always return 0.
}t_ve_md_cfg;

typedef FRAME VFRAME_LIST;

typedef struct s_dpm_ve
{
	DECLARE_DPM

	t_dp_loop loop_func;

	void* rc_cfg;
	t_ve_md_cfg* md_cfg;
	
	u32 (*handle_cmd)(struct s_dpm_ve * dpm, u32 cmd, u32 arg, u32 * handled);

	u8	fix_i_qp;		//need to config at non-rc case
	u8	fix_p_qp;		//need to config at non-rc case
	u8	frame_rate;

	u8	ecif_switch;			///< ecif need switch to encode different channels of bitstreams
	u8	add_sps_head_en;

	u8	entropy;
	u8	ip_i4_disable;///< disable intra predict I4 mode

	t_vs_fps_cfg* fps_cfg;///<manual fps control param
    u8 (*cb_frmrate_drop)(t_vs_fps_cfg*);  ///< callback for manually control frame rate, if set, libvenc will call this function before encoding each frame, if return 1, libvenc will encode the frame, if 0, the frame will be skipped
	u8	add_skip_frm;	///< if need to add empty frame when frame is skipped, please set to 1
	u8	skip_frm_added_num;	///< how many empty frame to be added after each encoded frame, default 1, can be configured at any time, either before encoder start or after.
	u8	no_skip_frm;
	u8  flag_drop_all_old_frms; ///< it is used when switch drop mode from dropping old frames to dropping new frames.

	u16	gop_size;
	u16	drop_old_frms_th;	///<threshold to drop old frames(one entire GOP)
	u8	drop_old_frms_when_full;	///<drop old frames(one entire GOP) when buffer is almost full


	u16 width;
	u16 height;
	u16	frm_crop_left;
	u16	frm_crop_right;
	u16	frm_crop_top;
	u16	frm_crop_bottom;

	u32 bs_buf_skip_th;		///< when left size smaller than this threshold, skip the frame
	u32 idr_period;
	u32 drop_all_old_frms_cnt; ///< when flag_drop_all_old_frms_flag is set 1, calculate how many old frames(how many gop) should be dropped.

	u16	max_node_in_list;	///< user can set this to reduce delay for certain application; frame will be dropped once the number of encoded frames(not included added empty frame) in BS buffer is more than this configuration

	u32 ve_ref_buf_base;	///< base address for ve reference buffer0,it has to be configured. if ve_ref_buf_base1 is not configured, it is non-pingpong reference buffer mode.in this case, reference buffer size is (vs_width*16*1.5) * (vs_height/16 + 5)
	u32 ve_ref_buf_base1;	///< base address for ve reference buffer1, config this will enable PINGPONG ref buffer
	//in pingpong reference buffer mode, each pingpong buffer size is vs_width * vs_height * 1.5. 
	
	u32 bs_buf_base;		///< by default following reference buffer, if need switch resolution, please config this
	u32 bs_buf_size;		///< bitstream buffer size, need to set before libvenc_init. the minimum bs_buf_size is recommended to be more than (bitrate * 2 / 8), that is to store at least 2 seconds of bitstream to avoid frame drop.

	u8 *meta_buf; ///< buffer addr for meta data, the size is meta_maxlen + tag_maxlen and should be 32Byte aligned
	u16 tag_maxlen; ///< buffer size for meta tag, "OVMS", "OVMZ", "OVME", "FF FF ... FF", 32Bytes aligned
	u16 meta_maxlen;	///< buffer size for meta data, 32Byte aligned
	
	FRAMELIST * framelist;
	VFDIST* vfdist;

	void* rc_ctl;	//array for storing rate control parameters
	void* extend_ctl;
    void (*cb_ve_intr_extra_process)(struct s_dpm_ve *p);  ///< callback for interrupt printf debug
    void (*cb_extra_channel_init)(struct s_dpm_ve *p);  ///< callback for extra channel configuration 
    void (*cb_extra_hw_cfg)(struct s_dpm_ve *p);  ///< callback for extra hw configuration 
	void (*cb_rc_set_bitrate)(struct s_dpm_ve *p); ///< call back for bit rate config
	void (*cb_rc_pre_hw_cfg)(struct s_dpm_ve *p); ///< call back for rate control hardware pre configuration 
	s32 (*cb_rc_init)(struct s_dpm_ve *p);	///< call back for rate control init
	u32 (*cb_rc_pre_process)(struct s_dpm_ve *p); ///< call back for rate control frame pre-process
	u32	(*cb_rc_post_process)(struct s_dpm_ve *p); ///< call back for rate control frame post-process

//INTERNAL_START
	u8	clock_gating_disable;	///<for debug only. clock gating means disable certain module when it is not used. by default gating for all VE modules are enabled. set clock_gating_disable to disable some of the module gatings to test whether it helps improve stability.
	u8	prefetch_manual;	///< set to 1 if encoder use ECIF
	u8	frm_skip_th;///< default 10, if the last 10 input frames are all dropped, next frame will be an I frame, range from 3 ~ 255
	u8	profile_idc;	///<66: baseline; 77: main profile; 100: high profile
	u8	level_idc;
	u8	no_cabac;		///< disable CABAC independently

	u8	picType;

	u8 idr_pic_id;
	u8 empty_frm_size;

	u8 	vfrm_ovf;
	u8	need_pps;
	u8	sps_len;
	u8	strm_encoding;
	u8	vs_running;
	
	u8	I8X8;
	u8	P8X8;

	u8	X_SEARCH;
	u8	Y_SEARCH;

	u8	enc_next_frm;
#define CLEAN_PART_OLD_FRAMES      (1)
#define CLEAN_ALL_OLD_FRAMES       (2)
	u8	clean_old_frms_flag;
#define    VFRM_NO_SKIP 	0  //no skip, normally encoded
#define    VFRM_SKIP		1	//skip the current frame, the encoded frame data will be abandoned
#define    VFRM_AUTO_SKIP	2	//encoder auto skipped frame, mainly used when A/V sync is required
	u8	frame_skip;	//frame skip mode
	u8	prefetch_clean;

	u16 prefch_req_period;	//prefetch request period, by default 0x180. The shorter it is ,the higher bandwidth it will cost. For VE 360Mhz, (1080P + 720P)@30fps case, 0x180 is recommended. For VE 240Mhz, (1080P + 720P)@15fps, 0x200 is recommended.

	u16 frm_in_buf;//total number of real frames in buffer, excluded added skipped frames
	u16 frm_to_add_count;//skipcount; ///< total frames that has been forced to be skipped because of buffer limitation and not yet been added back as empty frame, increase this will increase the empty frames to be added to the bitstream
	u16 autoskip_count; ///< total frame need to be automatically skipped required by firmware, for example, if set to 3, the following 3 frames will be skipped automatically, and it will decrease one each time a frame has been dropped
	u16 poc_tmp ;
	u16 frame_num ;

	u16 skip_th;//SKIP_TH;
	u16 skip_offset;//EARLY_TH;

	u16 buf_frm_cnt;//total number of frames in buffer, including skipped frames
	u16	skipping_cnt;	//the count of frames that are keeping on skipping

	u16 nGOP;
	u32 nIDR;
	u32 skipped_frm_cnt;//skip_frm_cnt
	u32	prefetch_time_out;	///< 
	u32 bs_buf_ovf;

	u32 reftm; ///< the absolute timestamp from system powerup when video start, in 10ms

	u32 ref_y_start[2];
	u32 ref_y_base;
	u32 ref_y_end;
	u32 ref_y_size;
	u32 ref_uv_start[2];
	u32 ref_uv_base;
	u32 ref_uv_end;
	u32 ref_uv_size;
	u8	ref_idx;
	
	u32 frm_cnt;//total encoded frames after current VS started
	u32 sensor_frm_cnt;
	u32 fb_frm_cnt;
	u32 bs_buf_end;

	u32 g_skip_frame;
	u8	__attribute__ ((aligned(4)))sps_pps_buf[SPS_PPS_BUF_SIZE];///<buffer for SW sps info

	u32 bs_start;	//start address of the bitstream buffer
	u32 bs_end;		//end address of the bitstream buffer
	u32 bs_last_rd;	//firmware last read address of the bitstream buffer, encoder will stop output bitstream if it reach this address, bitstream overflow interrupt will occur as well
	u32 bs_rewind;	//rewind address of the bitstream buffer
	u32 bs_base;	//starting address for the output frame
	u32	tmp_fb_size;//size of the temperoray frame buffer for skipped frame control

	u32	p_wr;		//general buffer write address
	u32	p_rd;		//general buffer read address
	u32 g_vfrm_cnt;//total number of nodes in frame lists

	struct s_dpm_ve * next_enc;	

	u8  FME_EN;
	u8	SKIP_EN;
	u8	DIS_DBLK_IDC;
	s8	DBLK_ALPHA;//range from -6 ~ 6, for deblocking threshold
	s8	DBLK_BETA;//range from -6 ~ 6, for deblocking threshold
	u8	ROWS_IN_UNIT; 
	u8	NUM_OF_SUBSLICE;

	u8	SLICE_NUM;
	u8	SLICE_MODE;
	u8	SLICE_MB_LINE; 
	u8	MB_LINE_FIRST_SLICE;
	u8	luma_only;
	u16 PIC_HEIGHT_IN_MB;
	u16 PIC_WIDTH_IN_MB;

	u16 meta_offset;	///<offset for storing meta data after meta head TAG. 
	u16 meta_len;		///<actual length of current meta data in meta buffer
	
//INTERNAL_END
}t_dpm_ve;

typedef t_dpm_ve ve_enc_t;

void libvenc_frm_start(ve_enc_t *ve_p);
s8 libvenc_bitrate_config(ve_enc_t* p);
u8 libvenc_intr_process(ve_enc_t* p);
s32 vrec_get_genbuf_left_size(ve_enc_t *ve_p);
VFRAME_LIST* libvenc_stream_get_frame(ve_enc_t* ve_p);
void libvenc_stream_free_frame(ve_enc_t* ve_p, VFRAME_LIST* frm);
u8 libvenc_config_i_p_qp_delta(ve_enc_t* ve_p);
u8 libvenc_config_qp_range(ve_enc_t* ve_p);
u32 libvenc_get_reftm(ve_enc_t *ve_p);
u32 venc_generate_sps(ve_enc_t *ve_p);


int ve_init(t_dpm_ve * dpm);
int ve_start(t_dpm_ve * dpm);
int ve_enable(t_dpm_ve * dpm);
int ve_disable(t_dpm_ve * dpm);
int ve_stop(t_dpm_ve * dpm);
int ve_exit(t_dpm_ve * dpm);

#define DPM_INIT_VE \
	.type = DPM_TYPE_VE, \
	.dpmname = "VE", \
	.sanity_check = (int (*)(t_dpm * dpm))ve_sanity_check, \
	.init = (int (*)(t_dpm * dpm))ve_init, \
	.start = (int (*)(t_dpm * dpm))ve_start, \
	.enable = (int (*)(t_dpm * dpm))ve_enable, \
	.disable = (int (*)(t_dpm * dpm))ve_disable, \
	.stop = (int (*)(t_dpm * dpm))ve_stop, \
	.exit = (int (*)(t_dpm * dpm))ve_exit,


#endif
