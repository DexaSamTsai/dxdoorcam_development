#ifndef _CRYPTION_H_
#define _CRYPTION_H_

enum crp_alg{
	DES,
	AES,
	DES3_KEY2,
	DES3_KEY3,
};

enum crp_mode{
	ECB=0,
	CBC,
	CFB,
	OFB,
	CTR,
};
typedef struct{
	 unsigned char		crp_dir;	//0:encryption  1:descryption
	 enum crp_alg 		alg;		//cryption algorithm
	 enum crp_mode	  	mode;		//cryption mode
	 unsigned int		key_bits;	//key bits,may be 64(DES),128(AES_128,DES3_KEY2),192(AES_192,DES3_KEY3),256(AES_256) bits,should set in app if use AES
	 unsigned char		fb_seglen;	//segment lenth for CFB/OFB,may be 1,8,64,128
	 unsigned char *	data_addr;	//cryption/descryption data address
	 unsigned int		data_length;	//cryption/descryption data length
	 unsigned char *	key_addr;	//key address
}t_crp_para;
//t_crp_para * crp_para;

int crp_config(t_crp_para * p_crp_para);

#endif

