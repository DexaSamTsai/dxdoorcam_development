#ifndef _ISP_H_
#define _ISP_H_

// ========== address range allocation starts ==========//
#define PRE_PIPE_ADDR_L							0xe0088000
#define MID_PIPE_ADDR_L							0xe0089400
#define POS_PIPE_ADDR_L							0xe0089800
#define RISC_RW_ADDR_L							0xe008a000
#define RISC_RO_ADDR_L							0xe008b000
#define ISP_SRAM_ADDR_L							0xe0080000
#define PDAF_RO_ADDR_L							0xe0090800

#define PRE_PIPE_ADDR_R							0xe008c000
#define MID_PIPE_ADDR_R							0xe008d400
#define POS_PIPE_ADDR_R							0xe008d800
#define RISC_RW_ADDR_R							0xe008e000
#define RISC_RO_ADDR_R							0xe008f000
#define ISP_SRAM_ADDR_R							0xe0084000

#define ISP_TOP_ADDR								0xe0089200 //?
#define ISP_ADDR_OFFSET							0x4000
#define RGBIR_TOP_ADDR                          0xc0038c00

// ========== address range allocation ends ==========//
// ========== RGBIr registers start =============//
#define RGBIR_DIGI_GAIN                      (RGBIR_TOP_ADDR+0x30)
// ========== RGBIr registers end =============//

// ========== ISP top registers start =============//
#define REG_ISP_PATH_MODE					ISP_TOP_ADDR
//#define REG_NORMAL_ENABLE					(ISP_TOP_ADDR+0x8)
//#define REG_NORMAL_ENABLE_R				(ISP_TOP_ADDR+0xc)
//#define REG_COMBINE_ENABLE					ISP_TOP_ADDR  // 0x89203[2]
// ========== ISP top registers end =============//


// ========== LENC registers start =============//

// ========== LENC registers end =============//

//===========lens online registers start ==========//
#define REG_LENS_ONLINE_BASE				(PRE_PIPE_ADDR_L+0x180)
#define REG_LENS_ONLINE_STAT				RISC_RO_ADDR_L
#define REG_LENS_ONLINE_RISC_RW		RISC_RW_ADDR_L
#define REG_LENS_SEN_GAIN					(REG_LENS_ONLINE_BASE+0x24)
#define REG_LENS_MIN_GAIN					(REG_LENS_ONLINE_BASE+0x28)
#define REG_LENS_CONTROL_1					REG_LENS_ONLINE_BASE
#define REG_LENS_CONTROL_2					(REG_LENS_ONLINE_BASE+0x20)
//===========lens online registers end ==========//

// ========== RGBH stretch registers start =============//

// ========== RGBH stretch registers end =============//

// ================ CIP register start ================//
#define REG_CIP_BASE (PRE_PIPE_ADDR_L+0x800)
//#define REG_CIP_SLOPE (REG_CIP_BASE+0x03)
//#define REG_CIP_KNEE_TH0 (REG_CIP_BASE+0x99)
//#define REG_CIP_KNEE_TH1 (REG_CIP_BASE+0x9A)
//#define REG_CIP_KNEE_TH2 (REG_CIP_BASE+0x9B)

// ================ CIP register end ================//

// ========== AECAGC registers start =============//
#define REG_AEC_MEAN_Y_0						(RISC_RO_ADDR_L+0x60)
#define REG_AEC_MEAN_Y_1						(RISC_RO_ADDR_R+0x60)
#define RISC_RW_REAL_GAIN_L					(RISC_RW_ADDR_L+0xa8)
#define RISC_RW_REAL_GAIN_R					(RISC_RW_ADDR_R+0xa8)
// ========== AECAGC registers end =============//

// ========== AWB starts =============//
#define REG_AWB_GAIN_BASE							0xe0088200
// ========== AWB ends =============//

// ========== RGBH curve starts =============//
#define REG_RGBH_CURVE_ENABLE					POS_PIPE_ADDR_L
// ========== RGBH curve ends =============//


// ========== DMA read/write address starts ==========//
#define DMA_READ_ADDR_1_L				RISC_RO_ADDR_L						// Read RISC RO
#define DMA_READ_ADDR_2_L				(ISP_SRAM_ADDR_L+0x800)		// Read hist Y(sram)
#define DMA_READ_ADDR_3_L				(ISP_SRAM_ADDR_L+0xc00)		// Read hist R(sram)
#define DMA_READ_ADDR_4_L				(ISP_SRAM_ADDR_L+0xe00)		// Read hist G(sram)
//#define DMA_READ_ADDR_5_L				(ISP_SRAM_ADDR_L+0x1000)	// Read hist B(sram)
#define DMA_READ_ADDR_5_L				(ISP_SRAM_ADDR_L+0x1200)	// Read mean(sram)
#define DMA_WRITE_ADDR_1_L				RISC_RW_ADDR_L						// Write RISC RW
#define DMA_WRITE_ADDR_2_L				ISP_SRAM_ADDR_L					// Write LENC profile(sram)
#define DMA_READ_ADDR_1_R				RISC_RO_ADDR_R						// Read RISC RO
#define DMA_READ_ADDR_2_R				(ISP_SRAM_ADDR_R+0x800)		// Read hist Y(sram)
#define DMA_READ_ADDR_3_R				(ISP_SRAM_ADDR_R+0xc00)		// Read hist R(sram)
#define DMA_READ_ADDR_4_R				(ISP_SRAM_ADDR_R+0xe00)		// Read hist G(sram)
//#define DMA_READ_ADDR_5_R				(ISP_SRAM_ADDR_R+0x1000)	// Read hist B(sram)
#define DMA_READ_ADDR_5_R				(ISP_SRAM_ADDR_R+0x1400)	// Read mean(sram)
#define DMA_WRITE_ADDR_1_R				RISC_RW_ADDR_R					// Write RISC RW
#define DMA_WRITE_ADDR_2_R				ISP_SRAM_ADDR_R					// Write LENC profile(sram)
// ========== DMA read/write address ends ==========//

// ========== I2C address starts ==========//
//#define I2C_BASE_ADDR					0x80063500
//#define I2C_ADDR_DATA					(I2C_BASE_ADDR+0x04)
//#define I2C_READ_BUFFER_1				(I2C_BASE_ADDR+0x40)
//#define I2C_READ_BUFFER_2				(I2C_BASE_ADDR+0x44)
//#define I2C_READ_BUFFER_3				(I2C_BASE_ADDR+0x48)
//#define I2C_READ_BUFFER_4				(I2C_BASE_ADDR+0x4c)
//#define I2C_READ_BUFFER_FLAG		(I2C_BASE_ADDR+0x50)
// ========== I2C address ends ==========//

// ========== debug register starts ==========//
//#define DEBUG_REG_1                          0x80063028
//#define DEBUG_REG_2                          0x8006302c
//#define DEBUG_REG_3                          0x80063038
//#define DEBUG_REG_4                          0x8006303c
// ========== debug register ends ==========//


// ========== firmware data arrangement starts ==========//

//#define L_BASE_ADDRESS								0x900c6000
//#define R_BASE_ADDRESS								0x900c4000
extern unsigned int L_BASE_ADDRESS;
extern unsigned int R_BASE_ADDRESS;

// L address starts
// Control Address
#define GLOBAL_CONTROL_L							L_BASE_ADDRESS					// 1536/1536 bytes
#define AF_CONTROL_L									(L_BASE_ADDRESS+0x600)	// 256/256 bytes
#define LENC_PROFILE_L								(L_BASE_ADDRESS+0x700)	// 	2304/2304 bytes
// DMA Table Address (256 bytes for 32 instructions)
#define ISP_DMA_DRAM_TABLE_1_L				(L_BASE_ADDRESS+0x1000)	// 32 bytes
#define ISP_DMA_DRAM_TABLE_2_L				(L_BASE_ADDRESS+0x1020) // 32 bytes
#define DRAM_DMA_ISP_TABLE_1_L				(L_BASE_ADDRESS+0x1040) // 32 bytes
#define DRAM_DMA_ISP_TABLE_2_L				(L_BASE_ADDRESS+0x1060) // 32 bytes
// data I/O address
#define DMA_BLANKING_READ_STATISTIC_L	(L_BASE_ADDRESS+0x1100)	// 340/512 bytes
#define DMA_READ_HIST_MEAN_L					(L_BASE_ADDRESS+0x1300) // 1024/1024 bytes
//#define DMA_READ_AEC_HIST_L					(L_BASE_ADDRESS+0x1500) // 2560/2560 bytes
//#define DMA_READ_AEC_MEAN_L					(L_BASE_ADDRESS+0x2500)	// 1024/1024 bytes
#define DMA_WRITE_PARAMETERS_L				(L_BASE_ADDRESS+0x1700) // 512/768 bytes
#define DMA_WRITE_LENC_PROF_L				(L_BASE_ADDRESS+0x1a00)	// 768/768 bytes
#define READ_PDAF_STAT_L								(L_BASE_ADDRESS+0x1d00)  // 388/512 bytes
//#define READ_GMV_STAT_L							(L_BASE_ADDRESS+0x1f00)  // 12/16 bytes
//#define READ_ANTISHAKE_STAT_L					(L_BASE_ADDRESS+0x1f10)  // 16/16 bytes
//#define READ_PDAF_STAT_L                        (L_BASE_ADDRESS+0x1f20)  // 80/80 bytes
// L address ends

// R address starts
// Control Address
#define GLOBAL_CONTROL_R							R_BASE_ADDRESS					// 1536/1536 bytes
#define AF_CONTROL_R									(R_BASE_ADDRESS+0x600)	// 256/256 bytes
#define LENC_PROFILE_R								(R_BASE_ADDRESS+0x700)	// 	2304/2304 bytes
// DMA Table Address(256 bytes for 32 instructions)
#define ISP_DMA_DRAM_TABLE_1_R				(R_BASE_ADDRESS+0x1000)	// 32 bytes
#define ISP_DMA_DRAM_TABLE_2_R				(R_BASE_ADDRESS+0x1020) // 32 bytes
#define DRAM_DMA_ISP_TABLE_1_R				(R_BASE_ADDRESS+0x1040) // 32 bytes
#define DRAM_DMA_ISP_TABLE_2_R				(R_BASE_ADDRESS+0x1060) // 32 bytes
// data I/O address
#define DMA_BLANKING_READ_STATISTIC_R	(R_BASE_ADDRESS+0x1100)	// 340/512 bytes
#define DMA_READ_HIST_MEAN_R					(R_BASE_ADDRESS+0x1300) // 1024/1024 bytes
//#define DMA_READ_AEC_HIST_R					(R_BASE_ADDRESS+0x1b00) // 2560/2560 bytes
//#define DMA_READ_AEC_MEAN_R					(R_BASE_ADDRESS+0x2500)	// 1024/1024 bytes
#define DMA_WRITE_PARAMETERS_R				(R_BASE_ADDRESS+0x1700) // 512/768 bytes
#define DMA_WRITE_LENC_PROF_R				(R_BASE_ADDRESS+0x1a00) // 768/768 bytes
//#define READ_AF_STAT_R								(R_BASE_ADDRESS+0x1d00)  // 388/512 bytes
//#define READ_GMV_STAT_R							(R_BASE_ADDRESS+0x1f00)  // 12/16 bytes
//#define READ_ANTISHAKE_STAT_R					(R_BASE_ADDRESS+0x1f10)  // 16/16 bytes
//#define READ_PDAF_STAT_R                        (R_BASE_ADDRESS+0x1f20)  // 80/80 bytes
// R address ends
// ========== firmware data arrangement ends ==========//


// ================ DMA starts ================//
#define DMA_BASE_ADDR						0xe0090000
#define DMA_GRP_REPEAT_NUM				DMA_BASE_ADDR			// 0x0:grp78([0:3]grp7 [4:7]grp8), 0x1:grp56, 0x2:grp34, 0x3:grp12

#define DMA_GRP1_BASE_ADDR				(DMA_BASE_ADDR+0x4)		//grp1_start_addr<23:0>
#define DMA_GRP2_BASE_ADDR				(DMA_BASE_ADDR+0x8)		//grp2_start_addr<23:0>
#define DMA_GRP3_BASE_ADDR				(DMA_BASE_ADDR+0xc)		//grp3_start_addr<23:0>
#define DMA_GRP4_BASE_ADDR				(DMA_BASE_ADDR+0x10)		//grp4_start_addr<23:0>
#define DMA_GRP5_BASE_ADDR				(DMA_BASE_ADDR+0x14)		//grp5_start_addr<23:0>
#define DMA_GRP6_BASE_ADDR				(DMA_BASE_ADDR+0x18)		//grp6_start_addr<23:0>
#define DMA_GRP7_BASE_ADDR				(DMA_BASE_ADDR+0x1c)		//grp7_start_addr<23:0>
#define DMA_GRP8_BASE_ADDR				(DMA_BASE_ADDR+0x20)		//grp8_start_addr<23:0>

#define DMA_GRP1234_INSTR_NUM			(DMA_BASE_ADDR+0x24)		//0x27:grp1,0x26:grp2, 0x25:grp3, 0x24:grp4
#define DMA_GRP5678_INSTR_NUM			(DMA_BASE_ADDR+0x28)		//0x2b:grp5,0x2a:grp6, 0x29:grp7, 0x28:grp8
#define DMA_GRP1_INSTR_NUM					(DMA_BASE_ADDR+0x27)
#define DMA_GRP2_INSTR_NUM					(DMA_BASE_ADDR+0x26)
#define DMA_GRP3_INSTR_NUM					(DMA_BASE_ADDR+0x25)
#define DMA_GRP4_INSTR_NUM					(DMA_BASE_ADDR+0x24)
#define DMA_GRP5_INSTR_NUM					(DMA_BASE_ADDR+0x2b)
#define DMA_GRP6_INSTR_NUM					(DMA_BASE_ADDR+0x2a)
#define DMA_GRP7_INSTR_NUM					(DMA_BASE_ADDR+0x29)
#define DMA_GRP8_INSTR_NUM					(DMA_BASE_ADDR+0x28)

#define DMA_GRP1_TRIG							(DMA_BASE_ADDR+0x2c)		//grp1_trig_en<7:0>
#define DMA_GRP2_TRIG							(DMA_BASE_ADDR+0x30)		//grp2_trig_en<7:0>
#define DMA_GRP3_TRIG							(DMA_BASE_ADDR+0x34)		//grp3_trig_en<7:0>
#define DMA_GRP4_TRIG							(DMA_BASE_ADDR+0x38)		//grp4_trig_en<7:0>
#define DMA_GRP5_TRIG							(DMA_BASE_ADDR+0x64)		//grp5_trig_en<7:0>
#define DMA_GRP6_TRIG							(DMA_BASE_ADDR+0x68)		//grp6_trig_en<7:0>
#define DMA_GRP7_TRIG							(DMA_BASE_ADDR+0x6c)		//grp7_trig_en<7:0>
#define DMA_GRP8_TRIG							(DMA_BASE_ADDR+0x70)		//grp8_trig_en<7:0>

#define DMA_GRP_SEL 							(DMA_BASE_ADDR+0x4c)		// 0x4d:exception_vsync_sel, 0x4e:grp_busy, 0x4f:grp_done
#define DMA_GRP_BUSY							(DMA_BASE_ADDR+0x4e)		// [0] grp1 [1] grp2 ...
#define DMA_GRP_DONE							(DMA_BASE_ADDR+0x4f)		// [0] grp1 [1] grp2 ...
//#define DMA_GRP_CLEAR							0x80063386		// [0]

#define DMA_EXCEPTION_FRM12 					(DMA_BASE_ADDR+0x50)		// 0x50-0x51:frm2,0x52-0x53:frm1
#define DMA_EXCEPTION_FRM3  					(DMA_BASE_ADDR+0x54)		// 0x55:grp,0x56-0x57:frm3
#define DMA_DEBUG_ST       					    (DMA_BASE_ADDR+0x58)		// 0x5a-0x5b
#define DMA_CLEAR_EXCEPTION   				    (DMA_BASE_ADDR+0x5c)		//
#define DMA_CLEAR_ERROR       				    (DMA_BASE_ADDR+0x60)		//

#define DMA_GRP_ENABLE							(DMA_BASE_ADDR+0x3c)		//[0] grp1_en, [1] grp2_en...
//#define DMA_TRIG_MODE							(DMA_BASE_ADDR+0x3e)		//[0] grp1 [1] grp2 ...
#define DMA_MCU_TRIG								(DMA_BASE_ADDR+0x48)		// manual trigger
#define DMA_GRP_RUN								(DMA_BASE_ADDR+0x40)		//[0] grp1_run, [1] grp2_run...
#define DMA_EN										(DMA_BASE_ADDR+0x44)		//

// ================ DMA ends ================//
//#define LIBISP_DBG
int libisp_irq_init(void);
int libisp_irq_free(void);

int isp_lum_init(t_dpm_ispidc * dpm);
void isp_lum_update(t_dpm_ispidc * dpm);
void isp_frm_update(t_dpm_ispidc *dpm);
void isp_hist_update(t_dpm_ispidc * dpm);

u32 libisp_handle_cmd(u32 cmd, u32 arg);
int Do_DayNightSwitch(t_dpm_ispidc * dpm);
#endif
