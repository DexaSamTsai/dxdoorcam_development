#ifndef _NEWOS_H_
#define _NEWOS_H_

/* Task priority list

sys : 1
isp : 5 
dp/vs : 10
aenc: 15
adec: 16
acodec: 17
sample: 30
IDLE: 31

   */

#define local_irq_save(flags) do{ flags=local_irq_disable(); }while(0)

#define spin_lock_irqsave(flags)  local_irq_save(flags)
#define spin_unlock_irqrestore(flags) local_irq_restore(flags)

#define MAX_TASK 31

#define TASK_STATUS_OK		0
#define TASK_STATUS_WAIT	1

#define TASK_WAIT_OK		0
#define TASK_WAIT_TIMEOUT	1 //timeout

typedef struct s_task {

	//Stk Ptr should be at first because assemble codes call it
    u32	* cur_stk;

    u16	tick_delay; //ticks delayed
    u8	status; //task status: TASK_STATUS_XXX
    u8	wait_status; //return value of wait status:  TASK_WAIT_XXX
    u8	prio; //priority
	u8	msg;
	u8	mpucmd_ready;
	u8	reserved0;

	u32 * stk;
	u32 stack_size;

	char * name;

#define	LOG_CRIT	2
#define	LOG_ERR		3
#define	LOG_WARNING	4 /* in most cases, use this as default */
#define	LOG_NOTICE	5 /* interrupt trace */
#define	LOG_INFO	6 /* informations */
#define	LOG_DEBUG	7 /* more debug informations */
	u8 logmask;

	u32 mpucmd_type; //MPUCMD_TYPE_XXX
	u32 (*app_cmdhandler)(u32 cmd, u32 arg);///< command handler prototype 
	s32 (*app_process)(void); ///< application process function prototype
} t_task;

/**
 * @brief t_newos, newos configurations and status
 * @details never change it!!!
 */
typedef struct s_newos
{

	t_task * cur_task; ///< @internal current task pointer
	t_task * next_task; ///< @internal next task to run

	u8 inirq; ///< @internal is in irq
	u8 unused; ///<padding
	u8 next_prio; ///< @internal next prio to run
	u8 cur_prio; ///< @internal current prio

	u8 running; ///< @internal 1: started, 0: not started, internal use
#define LOG_DISABLED	1
	u8 loglevel_default;
	u8 reserved1;
	u8 reserved2;

	u32 ready; ///< @internal how many tasks ready to run, one task one bit, internal use

	/* set before newos_init */
	t_task * tasks[MAX_TASK + 1];

	/* callback set before newos_init, load R1/EPCR/EPSR and start run */
	void (*cb_start_schedule)(void); ///<set to S_start_schedule before newos_init

	/* idle task, set before newos_init */
	u32            *task_idle_stk;
	int task_idle_stksize; ///<if zero, disable idle task, it's recommend
	int task_idle_prio;
}t_newos;

#define	LOG_MASK(pri)	(1 << (pri))
#define	LOG_UPTO(pri)	((1 << ((pri)+1)) - 1)

extern t_newos NewOSCtx;
extern t_newos * g_newos;

void S_context_switch(void);
void S_start_schedule(void);

int newos_init(t_newos * _new_os);
void newos_start (void);
void newos_timedelay (u16 tick);

int task_add(t_task * task, void (*task_main)(void * arg));
int task_wait(u16 timeout);
void task_signal(t_task * task);

void task_generic_main(void * arg);
int task_idle_init(void);

void syslog(int priority, const char * fmt0, ...);

#endif
