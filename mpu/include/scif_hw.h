/***************************************************************************
  *
  * Copyright (c) 2007 OmniVision Technologies Inc.
  * The material in this file is subject to copyright. It may not
  * be used, copied or transferred by any means without the prior written
  * approval of OmniVision Technologies, Inc.
  *
  * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
  * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
  * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
  * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
  * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
  * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
  * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
  *
  ******************************************************************************/
 /**
 * @file SCIF_HW.h
 * Header file for smca driver hardware
 */

#ifndef _SCIF_HW_H_
#define _SCIF_HW_H_

#include "scif.h"

/* scif host controller translate mode bit field */
#define HCD_DAT_PRESENT			(1<<21)
#define HCD_DAT_PRESENT_NONE	(0<<21)

#define HCD_CMD_IDX_CRC_NONE	(0<<20)
#define HCD_CMD_IDX_CRC 		(1<<20)

#define HCD_CMD_CRC_NONE		(0<<19)
#define HCD_CMD_CRC		 		(1<<19)

#define HCD_RES_NONE			(0<<16)
#define HCD_RES_136 			(1<<16)
#define HCD_RES_48				(2<<16)
#define HCD_RES_48_CHECK	    (3<<16)

#define HCD_FIFO_SWAP           (1 << 12)
#define HCD_ENDIAN_BIG          (1 << 13)

#define HCD_TRANS_BLOCK         (0<<6)
#define HCD_TRANS_SEQ           (1<<6)

#define HCD_TRANS_BLOCK_SGL     (0<<5)
#define HCD_TRANS_BLOCK_MULTI   (1<<5)

#define HCD_TRANS_DIR_WRITE     (0<<4)
#define HCD_TRANS_DIR_READ      (1<<4)

#define HCD_AUTOCMD12_NONE      (0<<2)
#define HCD_AUTOCMD12		    (1<<2)

#define HCD_BLOCK_COUNT_NONE    (0<<1)
#define HCD_BLOCK_COUNT		    (1<<1)

#define HCD_TRANS_DMA_NONE      (0<<0)
#define HCD_TRANS_DMA		    (1<<0)


/* sd host controller status bit field */
#define HCD_ADMA_ERR                (1 << 25)
#define HCD_DATA_CRC_ERR		    (1 << 21)
#define HCD_DATA_TIMEOUT_ERR		(1 << 20)
#define HCD_CMD_INDEX_ERR			(1 << 19)
#define HCD_CMD_ENDBIT_ERR			(1 << 18)
#define HCD_CMD_CRC_ERR				(1 << 17)
#define HCD_CMD_TIMEOUT_ERR			(1 << 16)
#define HCD_DATA_PROG_COMP			(1 << 9)
#define HCD_SCIO_STAT_RESP			(1 << 8)
#define HCD_CARD_REMOVED     		(1 << 7 )
#define HCD_CARD_INSERT				(1 << 6 )
#define HCD_DMA                     (1 << 3 )
#define HCD_DATA_TRANS_COMP			(1 << 1 )
#define HCD_CMD_COMP				(1 << 0 )

void scif_hcd_reset(t_scif_drv *p_scif_drv, int mode);

int scif_hcd_send_cmd(t_scif_drv *p_scif_drv, int cmd, unsigned int arg, 
                        unsigned int flags, 
						unsigned int st_bits, unsigned int *resp);

void scif_hcd_ctl_config(t_scif_drv *p_scif_drv, host_op_mode mode, 
					unsigned int dma_data_addr, unsigned int blk_siz, 
					unsigned int blk_num, unsigned char dam_flag, 
					unsigned int speed, unsigned char bus_width);

int  scif_hcd_read_start(t_scif_drv *p_scif_drv, 
                        unsigned int src_adr,
                        unsigned int dst_adr,
						unsigned int blk_size,unsigned int blk_num,
                        unsigned char dam_flag);

int scif_hcd_write_start(t_scif_drv *p_scif_drv, unsigned int src_adr, 
                        unsigned int dst_adr,
						unsigned int blk_size,unsigned int blk_num,
                        unsigned char dam_flag);

int scif_hcd_data_completed_wait(struct t_scif_drv *p_scif_drv, host_op_mode op_mode, unsigned int blk_num, unsigned char dma_flag);
#endif // _SCIF_DRV_H_

