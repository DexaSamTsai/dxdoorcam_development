#ifndef __ECC_BCH_H_
#define __ECC_BCH_H_

#define ECCBCH_BUFSIZE  1600
#define PARITY_NUM_MAX  (32*4)
#define BUFLEN      24

#define ECC_PARITY_GEN_RESET        0x01
#define ECC_SYND_GEN_RESET          0x02
#define ECC_ROOT_GEN_RESET          0x04
#define ECC_PARITY_SYND_ROOT_RESET  0x07

typedef enum{
    ECC_BCH_WRITE,
    ECC_BCH_READ,
    ECC_BCH_SYND_GEN,
    ECC_BCH_ROOT_GEN,
}ECC_OP_MODE;

#define ECCBCH_MODULE_DBG

#ifdef ECCBCH_MODULE_DBG
#define ECCBCH_DEBUG(x...) do{ if(g_ecc_bch->dbg) g_ecc_bch->dbg(x); }while(0)
#else
#define ECCBCH_DEBUG(x...) do{}while(0)
#endif

typedef struct 
{
    char op_mode;  /*mode: read or write*/
	volatile char ecc_bch_done;
    int parity_num; /* parity number per page */
    int parity_num_store; /* stored parity number per page */
    int parity_oft; /* the offset of the parity every page */
    unsigned int buf[ECCBCH_BUFSIZE/4]; /* parity and sydrome pointer */
    int number;
    int page_offset; 
#ifdef ECCBCH_MODULE_DBG
    int (*dbg)(const char *fmt, ...);
#endif
}t_ecc_bch;
extern t_ecc_bch * g_ecc_bch;


void ecc_bch_init(t_ecc_bch * eccbch);
void ecc_bch_reset(int module);
void ecc_bch_disable(void);
void ecc_bch_write(void);
void ecc_bch_read(void *parity);

int ecc_bch_irq_handler(void);

/* 
   .recd: the data read from nand 
   .s: syndrome 
*/
void ecc_bch_gen_syndromes(unsigned char *dataset_addr, unsigned char *parity_addr, short *syndromes);

void ecc_bch_error_search (short *syndromes, short *alpha_to, short *index_of, short *l_u);

void ecc_bch_error_correct(unsigned int *recd, short *l_u);

void ecc_bch_test(unsigned char *dat_adr, unsigned char *parity);

#endif

