#ifndef _RATE_CTL__H_
#define _RATE_CTL__H_


//INTERNAL_START

//#define CMODEL_T	//if for C model, need to define this, undefine RTL_T and HW_SYS
//#define RTL_T		//if for RTL simulation, need to define this, undefine CMODEL_T and HW_SYS
#define HW_SYS

#define MIN_WEIGHT      128	//256/1024 = 0.125, minimum weight of subslice 0 & 2 set to 0.125
#define MAX_WEIGHT      1024	//maximum weight of subslice 0 & 2 should be no more than 1
#define MIN_DELTA_W     64	//mininum step for weight adjustments, should be no more than MIN_WEIGHT
//#define MAX_DELTA_W     256	//maximum delta weight, should be no more than MIN_WEIGHT
#define MAX_I_P_QP_DELTA	15

#ifndef HW_SYS
typedef unsigned char       u8;
typedef signed   char       s8;
typedef unsigned short      u16;
typedef signed   short      s16;
typedef unsigned int        u32;
typedef signed   int        s32;
#endif

#define MAX_SUBSLICE_NUM	3
typedef struct T_RC_CTL
{
/*following is reg input*/
	u32	avg_unit_tgt_bits;//the average target bits for each unit
	u8 	num_of_subslice;//how many subslice in each frame, either 1 or 3
	u8 	units_in_subslice[MAX_SUBSLICE_NUM];//number of units in each sub-slice
	u8	max_qp_in_unit[MAX_SUBSLICE_NUM];//maximum qp in each sub-slice
	u8	min_qp_in_unit[MAX_SUBSLICE_NUM];//minimum qp in each sub-slice
	u8	delta_qp[MAX_SUBSLICE_NUM-1];//delta_qp for each sub-slice
	u8	cur_slice_qp;//init qp of the current frame
	u16	units_per_pic;//how many units per pic
	u8	mb_rows_in_unit;//how many mb rows in one unit
	u16	weight_of_subslice[MAX_SUBSLICE_NUM];//weight_of_subslice/1024 will be the actual weight

/*following is reg output after each frame*/	
	/*NOTE************total encoded bits (pFrmCtrlSt->accuEncodedBits)is required also*/
	u16	sum_subslice_qp[MAX_SUBSLICE_NUM];//sum of the qp in sub-slice
	u32	coded_subslice_bits[MAX_SUBSLICE_NUM-1];//sum of the encoded bits in each subslice

/*register for SATD rate control input*/
	u16	satdClassTh[3];
	u16	satdInterClassTh[3];
	u16	satdMbTh[3];
	u8	qp_sets[4];
	u32	avg_mb_dstn_intra;		//for P frame early skip mode dstn estimation

/*register for SATD rate control output*/
	u32	sum_totalMB_qp;	//sum of the qp of all MBs, for RC_FEATURE
	u32 accu_mb_dstn;
	u32 accu_mb_dstn_inter;
	u32 accu_mb_grad;
	u16	mb_intra_cnt;
	u16	mb_inter_cnt;
	u16 satd_class_num[4];
	u16 satd_inter_class_num[4];
	u16 grad_class_num[4];

/*following is for firmware application*/	
//Public
	s32 bitRate;
	u32 frameRate;
	s32 bitsPerFrame;//average frame bits, equals to rcPt->bitRate/rcPt->frameRate
	u32 max_exp_pbits; 
	u8	picType;//frame type, SLICE_I or SLICE_P
	u8	short_gop_en;

	u16 init_weight;		///< 初始化权重 
//Private
	u32 tgt_bits;			//target bits for the next frame
	u8	mb_rows_per_pic;	//how many mb rows in a picture
	u32 maxMBNum; //total MB number in a frame, e.g. for VGA, there are (640/16)*(480/16) = 1200 MBs in a frame
	u32 bitsPerMB;//equals to rcPt->bitsPerFrame/rcPt->maxMBNum
	s32 remBits;//remain bits of the frame group 
	s32 t_remBits;//remain bits of the current GOP
	s32 t_bpf_last_gop;
	u16 nGOP;//the number of frames need to be encoded in the current GOP
	u16	nPcnt;//the number of P frames that have been encoded in the current GOP
	u16	total_units;//number of units in a frame, a frame is divided in to certain units, each units encode with the same QP
//	u8	bf_frame_num;	//to avoid no lost frame, the estimated frame can be buffered

	u16 I_P_RATIO;  //the ratio of expected size of I_frame*10/P_frame, will be automatically adjusted according to the scene
	u16 numFramesInGRP; //number of frame in a frame group
	u16 numFramesInGOP; //number of frame in a GOP
	u16 min_GOP_size; //minimum number of frame in a GOP
	u16 max_GOP_size; //maximum number of frame in a GOP
	u16 last_GOP_size; //number of frame in last encoded GOP

	u32 qp_last_gop;  //average qp of last gop
//	u8 	max_delta_qp;			  
	u8 	qp_last_frame; //qp of last frame
	u8	qp_before_last_frm;	//qp of frame before last frame
	u8 	qp_last_I;//qp of last I frame
	u8 	qp_last_P;//qp of last I frame
//	u8 	qp_ave_slice;						  //average qp in gop
	u32 qp_sum_p;							  //sum of p frame qp

	u8  init_qp;  //the expected average encoded QP of next frame

	u8  max_qp[2]; //maximum encoded QP limit
	u8	min_qp[2];//minimum encoded QP limit
	u16	tgt_ratio_avg[2];//rcPt->tgt_bits*100/actual_encoded_bits, 0 for I frame, 1 for P frame
	u16 	tgt_gop_ratio;
	u16 	exp_gop_ratio;
	
	u8	bNewSceneDetect;//new scene detected
	
	u16	MV_MB_last_frm;//detected motion MB number
	u32 force_I_th;//the threshold of detected motion MB number, exceed the threshold will forcing encoding an I frame

	u32 nGRP;//the number of frames need to be encoded in the current frame group
	u32 nBitsPerGRP;//equals to rcPt->bitsPerFrame * rcPt->numFramesInGRP
	s32 nBitsPerGOP;//equals to rcPt->bitsPerFrame * rcPt->numFramesInGOP

	u32 I_avg_bits;//the expected I frame size for the current GOP, will be calculated according to I_P_RATIO before each GOP
	u32 P_avg_bits;//the expected average P frame size for the current GOP, will be updated after I frame encoding done
	u32 exp_p_bits;//the expected average P frame size for the current GOP, will be updated after I frame encoding done
	u32 enc_bits[2];
	u32 bit_cnt;
	u32	min_p_size;
	u32 last_P_bits;
	u32 min_I_size;//the minimum target bits for I frame

	u16 I_Ratio;//param for I frame tgt_bits adjustment, I_avg_bits * I_Ratio/100 will be the expected I tgt_bits
	u16 P_Ratio;//param for P frame tgt_bits adjustment, P_avg_bits * P_Ratio/100 will be the expected P tgt_bits
	u16 max_I_P_ratio;//maximum limit of rcPt->I_P_ratio

	u8 rc_feature_en;
	s8 i_p_qp_delta;
	u8 qp_just_tuned;
	u8 qp_increased;
	u8 half_tune;
	u8 half_inc;
	u16 	diff_last_frm;
	u16 	grad_last_frm;
	u16 increased_diff[2];
	/*result from ISP, extracted feature*/
	u32	avg_grad;
	u32	grad_region_num[4];
	u32	avg_diff;
	u32	grad_info_read;

}t_ve_rc_ctl;
#ifndef HW_SYS
typedef struct s_libvenc_cfg
{
	u32 GENBUF_SIZE;		///< bitstream buffer size, need to set before libvenc_init
	u8  min_qp[2];			///< QP of encoding frame will be >=min_qp (0~51), [0] for channel 0, [1]for channel 1
	u8  max_qp[2];			///< QP of encoding frame will be <=max_qp (0~51)
	u8  no_skipfrm;		///< keep frame rate
	u8  max_gop_size;		///< if set, max GOP size restricted as required	
	u8  init_qp;		///< if set, config initial QP	
	u16 init_weight;
} t_libvenc_cfg;
#endif
/**
 * @brief <b>rate control initial function</b>
 * @details call this function before encoder start
 *
 * @param ve_p: pointer of dpm ve  
 *
 * @return 0:ok, <0:err
 *
 */
int InitRateControl(ve_enc_t* ve_p);

/**
 * @brief <b>rate control bit rate configuration</b>
 * @details call this function after InitRateControl(), while encoder is running, this function can be called as well to update the bit rate
 *
 * @param ve_p: pointer of dpm ve  
 *
 * @return NULL 
 *
 */
void RCSetBitrate(ve_enc_t* ve_p);

/**
 * @brief <b>rate control pre process function</b>
 * @details call this function before encoding each frame. Rate control will return necessary params for next frame configuration
 *
 * @param *rcPt: pointer of rate control structure 
 *
 * @return target bits for each unit of the next frame 
 *
 */
u32 RCFramePre(ve_enc_t *ve_p);

/**
 * @brief <b>rate control post process function</b>
 * @details call this function after encoding each frame. Rate control need the encoded data to update the algorithm moudule
 *
 * @param ve_p: pointer of dpm ve  
 *
 * @return rcPt->nGOP, how many frames still need to be encode for the current GOP, for encoding frame type control 
 *
 */
u32 RCFramePost(ve_enc_t *ve_p);
void RCPreRegCfg(ve_enc_t* ve_p);
void RCPostRegGet(ve_enc_t* ve_p);

int ve_sanity_check(t_dpm_ve * dpm);
//INTERNAL_END

#ifdef CMODEL_T
void
RcInit( 
       int bitRate,       
       int frmRate,
       int width,
       int height,
       int intra_period,
       int minQp,
       int maxQp,
       int unitSize,
       int num_of_subslice,
       int units_in_subslice_0,
       int units_in_subslice_1,
       int weight,
       int SceneChangeEnable,
				int RcFeatureEnable,
				int init_qp,
				int init_dstn,
				int init_grad,
				int i_p_qp_delta
       );
t_ve_rc_ctl* GetRcSt(void);
void RcClose();
void rc_unit_level(t_ve_rc_ctl *rc);
unsigned int rc_unit_update(t_ve_rc_ctl *rc, int row_no,int coded_row_len);
void rc_mb_qp_accu(t_ve_rc_ctl *rc, int mb_idx, char mb_qp);
u8 RcSatdGetClass(t_ve_rc_ctl *rcPt, u16 mb_dstn, u16 mb_dstn_inter, u16 mb_idx, u16 mb_is_inter);
u8 RcGradGetClass(t_ve_rc_ctl *rcPt, u16 mb_grad, u16 mb_idx);
void rtctrlfrAddCntIntraMB(void); 
void rtctrlfrAddCntIntraMBMaxMV(void);
#endif

#ifdef RTL_T
t_ve_rc_ctl *rc_p;
t_ve_rc_ctl rc_t;
t_libvenc_cfg g_libvenc_cfg;
#endif

#endif	//_RATE_CTL__H_
