#ifndef _SHARE_H_
#define _SHARE_H_


extern struct t_scif_drv sh_scif_drv;
extern t_scif_info sh_scif_info;
extern struct t_scif_drv sh_scio_drv;
extern t_scif_info sh_scio_info;
extern t_efs sh_efs;
extern t_efs_iobuf sh_iombuf[4];

//ret size, 0:failed
__init u32 app_scif_init(void);
__init int rom_fs_init(void);

#endif
