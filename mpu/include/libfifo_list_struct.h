#ifndef __LIBFIFO_LIST_STRUCT__
#define __LIBFIFO_LIST_STRUCT__

typedef struct s_fifo
{
	u32 fifo_base;
	u32 fifo_size;
	u32 fifo_wr;
	u32 fifo_rd;
}t_fifo;

enum{
	FIFO_LIST_ENCV,
	FIFO_LIST_MIMG,
	FIFO_LIST_AUDIO,
};

typedef struct s_fifo_cfg
{
	u8 list_en;           ///< enable fifo list
	u8 list_type;         ///< list type
	u32 list_pool_buf;    ///< the address of list pool buffer
	u32 list_num;         ///< list total count
	u32 list_cnt;         ///< current list count
	t_pool * plist_pool;  ///< pointer to the list pool
	void * head;          ///< pointer to the list head
	void * tail;          ///< pointer to the list tail
	struct s_fifo fifo;   ///< fifo structure
}t_fifo_cfg;

#endif  ///< __LIBFIFO_LIST_STRUCT__
