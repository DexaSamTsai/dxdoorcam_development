#ifndef _ROM_FUNCS_H_
#define _ROM_FUNCS_H_

size_t strlen(const char *s);
char *strcpy(char *dest, const char *src);
char *strncpy(char *dest, const char *src, size_t n);
char *strcat(char *dest, const char *src);
char *strncat(char *dest, const char *src, size_t n);
int strcmp(const char *s1, const char *s2);
int strcasecmp(const char *s1, const char *s2);
int strncmp(const char *s1, const char *s2, size_t n);
char *strchr(const char *s, int c);
char *strrchr(const char *s, int c);

void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, void *src, size_t n);
void *memmove_new(void *dest, void *src, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);
void *memchr(const void *s, int c, size_t n);
void *memset(void *d, int c, size_t n);

int _sprintf(char *buf, const char *fmt0, va_list ap);
int sprintf(char *buf, const char *fmt0, ...);

int util_atoi(char *c);
int util_toupper(int c);

void uart_init(unsigned int uart_clk_freq, unsigned int baud_rate, unsigned char irq_en);
int uart_putc(unsigned char c);
void lowlevel_putc(unsigned char c);
void lowlevel_forceputc(unsigned char c);
int uart_printf(const char *fmt0, ...);
unsigned char uart_getc_echo(void);
unsigned char *uart_gets(unsigned char *buf, unsigned int size);
unsigned char uart_getc(void);
int uart_getc_nob(unsigned char *pch);
int uart_dma_puts(unsigned int addr, unsigned int length, unsigned int timeout);
int uart_dma_gets(unsigned int addr, unsigned int length, unsigned int timeout);
int uart_testgetc(void);
#define uart_forceputc(c) do{WriteReg32(UART_BASE_M + UART_TX,c);}while(0)
#define uarts_forceputc(c) do{WriteReg32(UART_BASE_S + UART_TX,c);}while(0)

void uarts_init(unsigned int uart_clk_freq, unsigned int baud_rate, unsigned char irq_en);
void uarts_putc(unsigned char c);
unsigned char uarts_getc(void);

void sdram_init(void);
void boot_syscfg(u32 cfg);

unsigned short crc16_ccitt(const void *buf, int len);
int xmodemReceive(unsigned char *xbuff, unsigned char *dest, int destsz);
int load_from_xmodem(u32 baseaddr);

int uprintf(const char *fmt0, ...);
int uprintf2(const char *fmt0, ...);
int dprintf(const char *fmt0, ...);

unsigned short mcu_getc(void);
void mcu_putc(unsigned short c);

#ifdef ROM_DEBUG
extern int (*rom_printf)(const char *fmt0, ...);
#else
#define rom_printf(x...)
#endif


/**
 * @defgroup lib_cpu
 * @{
 */

/**
 * @details disable interrupt and tick
 *
 * @param TODO
 *
 * @return previous interrupt status saved, for local_iqr_restore.
 *
 */
u32 local_irq_disable (void);


/**
 * @details store the old interrupt status.
 *
 * @param flags: return from local_irq_disable().
 *
 * @return TODO
 *
 */
void local_irq_restore(u32 flags);


/**
 * @details enable I cache
 *
 * @param TODO
 *
 * @return TODO
 *
 */
int ic_enable(void);


/**
 * @brief disable I cache
 *
 * @param TODO
 *
 * @return TODO
 *
 */
int ic_disable(void);

/**
 * @details flush all D cache to RAM
 *
 * @param TODO
 *
 * @return TODO
 *
 */
void dc_flush_all(void);


/**
 * @details invalidate all D cache.
 *
 * @param TODO
 *
 * @return TODO
 *
 */
void dc_invalidate_all(void);


/**
 * @details enable D cache
 *
 * @param TODO
 *
 * @return TODO
 *
 */
int dc_enable(void);


/**
 * @details disable D cache
 *
 * @param TODO
 *
 * @return TODO
 *
 */
int dc_disable(void);


void disable_cache(void);
/** @} */ //end lib_cpu

/**
 * @defgroup lib_newos
 * @{
 */

//INTERNAL_START
/**
 * @details called by assemble, don't care it
 *
 * @param TODO
 *
 * @return TODO
 *
 */
void newos_except_tick (void);


/**
 * @defgroup lib_pool
 * @{
 */

/**
 * @details TODO
 *
 * @param TODO
 *
 * @return TODO
 *
 */
t_pool *pool_init(u32 element_size, u32 element_init_count, u8 *pbuf);

/**
 * @details TODO
 *
 * @param TODO
 *
 * @return TODO
 *
 */
void *pool_element_alloc(t_pool *pool);

/**
 * @details TODO
 *
 * @param TODO
 *
 * @return TODO
 *
 */
void pool_element_free(t_pool *pool, void *element);

/** @} */ //end lib_pool

/**
 * @defgroup lib_flvenc
 * @{
 */

/**
 * @details write flv header to frm_hdr
 *
 * @param TODO
 *
 * @return TODO
 *
 */
//int flvenc_write_header(u8 *frm_hdr);

/**
 * @details fix header after done
 *
 * @param TODO
 *
 * @return TODO
 *
 */
//int flvenc_fix_header(u8 * frm_hdr, int filesize);

//u32 flvenc_get_tag_hdr(u8 type, unsigned int size, int ts, int duration, u8 *frm_hdr);

/** @} */ //end lib_flvenc
void sccb_set_old(u8 id, u8 mode, u8 sccb_num);
s32 sccb_rd_old(s32 address);
s32 sccb_seqread_old(s32 address, s32 count, u8 * buf);
s32 sccb_wr_old(s32 addr, s32 data);
s32 sccb_seqwrite_old(s32 address, s32 count, u8 * buf);

int sccb_set (t_libsccb_cfg *cfg);
int sccb_wr (t_libsccb_cfg *cfg, int addr, int data);
int sccb_rd(t_libsccb_cfg *cfg, int address);
int sccb_seqread(t_libsccb_cfg *cfg, int address, int count, unsigned char * buf);

extern unsigned int volatile ticks;

void gpio_config(int pin, int input);
void gpio_write(int pin, int data);
int gpio_read(int pin);

void gpio_config1(int pin, int input);
void gpio_write1(int pin, int data);
#endif
