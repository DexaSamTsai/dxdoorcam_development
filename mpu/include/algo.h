/****************************************************************************
 * 
 * Copyright (c) 2013 OmniVision Technologies Inc. 
 * The material in this file is subject to copyright. It may not
 * be used, copied or transferred by any means without the prior written
 * approval of OmniVision Technologies, Inc.
 *
 * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * $Author  : enigma_shen@ovt.com
 * $Data    : 
 * $Version : V5122  
 ****************************************************************************/

#ifndef _ALGO_DATA_H_
#define _ALGO_DATA_H_

#define __DEBUG_PRINT_INFO_ENABLE__
//#define __TWO_SET_AWB_ENABLE__
#define __OPTIMIZATION_FOR_ROM_SIZE__

#define __DEBUG_ADVDNS_COMMAND__

#define ReadHWRegister(x)		(REG8(x))
#define WriteHWRegister(x, y)   WriteReg8(x,y)
//#define WriteHWRegister(x, y)	(REG8(x) = (y))

#define ReadHW32BITRegister(x)		(REG32(x))
#define WriteHW32BITRegister(x,y)   WriteReg32(x,y)
//#define WriteHW32BITRegister(x,y)	(REG32(x)) = (y)

#define true 1
#define false 0
#define __int64 long long int
#define DIFF(x, y) (((x)>(y))?((x)-(y)):((y)-(x)))
#ifndef ABS
#define ABS(x) ((x<0)?-(x):(x))
#endif
#define AMIN(x,y) ((ABS(x)<ABS(y))?ABS(x):ABS(y))
//#define abs(x) ((x)>=0? (x) : -(x))
#ifndef max
#define max(a,b)            (((a) > (b)) ? (a) : (b))
#endif
#ifndef min
#define min(a,b)            (((a) < (b)) ? (a) : (b))
#endif
//#define clip(x,a,b)  max((a),min((x),(b)))

// default AWB gain
//#define DEFAULT_WB_GAIN1 255
//#define DEFAULT_WB_GAIN2 128
//#define DEFAULT_WB_GAIN3 128
//#define DEFAULT_WB_GAIN4 188

#define DEFAULT_WB_GAIN1 128
#define DEFAULT_WB_GAIN2 128
#define DEFAULT_WB_GAIN3 128
#define DEFAULT_WB_GAIN4 128


// Constant value for ISP FW
#define MINLIGHTEXP 0x20
#define MAXLIGHTEXP 0xffff0000

// LENC map size
#define LENC_16x16_MAP
//#define LENC_12x10_MAP
#ifdef LENC_16x16_MAP
#define REG_PROFILE_LENGTH					768
#endif
#ifdef LENC_12x10_MAP
#define REG_PROFILE_LENGTH					360
#endif


typedef enum tagISPWorkMode
{
	omNormalProcess = 0,
	omEDRProcess,
	omHDRProcess,
//	om3DProcess,
}TISPWorkMode;

typedef enum tagFWWorkMode
{
	fmINITIAL = 0,
	fmYUVPROCESS,
	fmRAWPROCESS,
	fmBYPASSPROCESS,
}TFWWorkMode;

typedef enum tagHDRTwoExpCombineMode
{
	tmHDRCombine = 0,
	tmHDRLongExp,
	tmHDRShortExp,
}THDRTwoExpCombineMode;

typedef enum tagHDRAWBWorkMode
{
	wmSeperateAWB = 0,
	wmLongExpAWB,
	wmShortExpAWB,
	wmCombineAWB,
}THDRAWBWorkMode;

typedef enum tagBandStatus
{
	bmUnknown = 0,
	bm60HZ,
	bm50HZ,
}TBandStatus;

typedef enum tagISPPipeMode
{	
	pmSyncUp=0,
	pmHighSpeed,
	pmSeparate,
	pmSingle,
	pmHDR,
	pmIdle,
}TISPPipeMode;

typedef struct tagGlobalParameterControl	// item=2	baseaddress1=0x80032000	baseaddress2=0x80034000
{	
	// system	16 bytes	0x00~0x0f 
	unsigned short nInputWidth;	// 0x00		v=0x500	d="ISP input width"
	unsigned short nInputHeight;// 0x02		v=0x320	d="ISP input height"
	unsigned short nOutputWidth; // 0x04	v=0x500	d="ISP output width"
	unsigned short nOutputHeight; // 0x06	v=0x320	d="ISP output height"
	unsigned char nSensorProcessFrameNum; // 0x08	v=0x2 d="Frame numbers ISP updating parameters once"
	unsigned char nFWWorkMode; // 0x09		v=0x0	d="Firmware work mode"
	unsigned char pReserved1; // 0x0a		v=0x0	d="0:OVT, 1:Sony, 2:Samsung"
	unsigned char nIdleWorkMode; // 0x0b	v=0x0	d="Idle work mode selection"
	unsigned short nBaseAddress; // 0xc		v=0x0	d="Pipeline base address, 0 for pipe 1 while HW_ADDRESS_OFFSET for pipe 2"
	unsigned char bHighSpeedEnable; // 0xe v=0x0	d="High speed mode enable, can not combine with HDR or 3D"
	unsigned char nPipeMode;// 0xf v=0x0 d="pipeline work mode"

	// AECAGC		120 bytes		0x10~0x87
	unsigned char bAdvancedAECEnable; // 0x10	v=0x01	d="Advanced AEC enable"
	unsigned char bAECAGCWriteSensorEnable; // 0x11	v=0x1	d="MCU writes exposure and gain to sensor enable"
	unsigned char nSensorGainMode; // 0x12	v=0x02	d="sensor gain mode"
	unsigned char nAECExposureShift; // 0x13	v=0x08	d="AEC exposure bit shift"
	unsigned char nTargetYLow; // 0x14	v=0x28	d="AEC low target"
	unsigned char nTargetYHigh; // 0x15	v=0x50	d="AEC high target"
	unsigned short nSlowRange; // 0x16	v=0x20	d="AEC slow range"
	unsigned char pStableRange[2]; // 0x18	v=0x04	d="AEC stable range"
	unsigned char nFastStep; // 0x1a	v=0x0a	d="AEC fast step"
	unsigned char nSlowStep; // 0x1b	v=0x08	d="AEC slow step"
	unsigned char nStrechGainLowStep; // 0x1c	v=0x20 d="AEC stretch gain low step"
	unsigned char nStrechGainHighStep; // 0x1d	v=0x20 d="AEC stretch gain high step"
	unsigned char nStretchSlowStep; // 0x1e		v=0x02 d="AEC stretch slow step"
	unsigned char nStretchSlowRange; // 0x1f	v=0x08 d="AEC stretch slow range"
	unsigned char pTargetStableRange; // 0x20	v=0x08 d="AEC target stable range"
	unsigned char nStretchTargetSlowRange; // 0x21	v=0x06 d="AEC stretch target slow range"
	unsigned char bAECManualEnable; // 0x22		v=0x00 d="AEC manual enable"
	unsigned char nAECManualDone; // 0x23		v=0x02 d="AEC manual done selection"
	unsigned short nMaxCameraGain; // 0x24	v=0x7f	d="Max camera gain"
	unsigned short nMinCameraGain; // 0x26	v=0x10	d="Min camera gain"
	unsigned int nMaxExposure; // 0x28		v=0x340	d="Max camera exposure"
	unsigned int nMinExposure; // 0x2c		v=0x01	d="Min camera exposure"
	unsigned int nAECManualExp; // 0x30		v=0x200	d="AEC manual exposure"
	unsigned short nAECManualGain; // 0x34	v=0x10	d="AEC manual gain"
	unsigned char nLinearExp; // 0x36		v=0x80	d="Linear exposure start"
	unsigned char nPreChargeWidth; // 0x37	v=0x08 d="sensor pre charge width"
	unsigned char nReadHistMeanSelect;//0x38 v=0x0 d="select hist or mean for read"
	unsigned char nApplyAnaGainMode; //0x39  v= 0x01 d="analog gain apply mode"
	unsigned char nSaturationPer1; //0x3a v=0x01 d="saturate percentage 1 of histgram"
	unsigned char nSaturationPer2; //0x3b v=0x04 d="saturate percentage 2 of histgram"
	unsigned char nSaturateRefBin; // 0x3c v=0xfd d="saturate reference bin"
	unsigned char bAllowFractionalExp; // 0x3d	v=0x00	d="Allow fractal exposure enable"
	unsigned char nMaxFractalExp; // 0x3e	v=0x0b	d="Max fractal exposure"
	unsigned char nMinFractalExp; // 0x3f	v=0x04	d="Min fractal exposure"
	unsigned char nGainFractalBit; // 0x40 v=0x04 d="fractal bit of sensor gain"
	unsigned char nBandingFilterMode; // 0x41	v=0x01	d="0:unknown, 1:60Hz, 2:50Hz"
	unsigned char bBandingFilterEnable; // 0x42	v=0x01	d="Banding filter enable"
	unsigned char bLessThan1BandEnable; // 0x43	v=0x01	d="Less than 1 band mode enable"
	unsigned short nBandValue60Hz; // 0x44	v=0x01	d="Band value for 60Hz"
	unsigned short nBandValue50Hz; // 0x46	v=0x01	d="Band value for 50Hz"
	unsigned char bNightMode; // 0x48		v=0x00	d="Nigh mode enable"
	unsigned char bOnlyInsertFrameEnable; // 0x49	v=0x00	d="Only insert frame in night mode enable"
	unsigned char nAECInsertFrameNum; // 0x4a	v=0x1	d="Max insert frames in night mode"
	unsigned char nNightModeStep; // 0x4b	v=0x0c	d="Night mode step"
	unsigned int nMaxLightExp; // 0x4c v=0xffff0000 d="max brightness metric"
	unsigned int nMinLightExp; // 0x50	v=0x20 d="min brightness metric"
	unsigned short nVTS; // 0x54	 v=0x9d0 d="sensor VTS value"
	// Sensor registers for AECAGC
	unsigned char nSensorDeviceID; // 0x56	v=0x6c	d="sensor device ID"
	unsigned char nSensorI2COption; // 0x57	v=0x09	d="sensor I2C option"
	unsigned short pSensorAECAddr[12]; // 0x58		d="sensor AEC address"
	unsigned char pSensorAECMask[12]; // 0x70		d="sensor AEC bit enable"
	unsigned short nMaxAnaGain; //0x7c~d v=0xff  d="max analog gain"
	unsigned short nMaxDigiGain; //0x7e~f v=0x100 d="max digi gain"
	unsigned char bSensorDigiGainEnable; // 0x80 v=0x0 d="apply digi gain enable"
	unsigned char nApplyDigiGainMode; // 0x81 v=1 d="digital gain apply mode"
	unsigned char nDigiGainFormat; // 0x82 v=0x0 d="sensor digi gain format"
	unsigned char bHDRECOEnable; // 0x83 v=0 d="enable hdr eco option"
	unsigned char bAECAddressSwitch; //0x84
	unsigned char pAECReserved[3]; // 0x85~87			// no use

	// AWB		376 bytes		0x88~0x1ff
	unsigned char nAWBMethodType;  // 0x88	v=0x01	d="AWB Method Type"
	unsigned char nAWBWorkMode;    // 0x89	v=0x01	d="AWB Work Mode"
	unsigned char nAWBStep1;	   // 0x8a	v=0x01	d="AWB Step1"
	unsigned char nAWBStep2;	   // 0x8b	v=0x80	d="AWB Step2"
	unsigned short pMaxAWBGain[4]; // 0x8c d="max AWB gain"
	char pManualAWBOffset[4];      // 0x94 d="manual AWB offset"
	unsigned char pAWBReserved1[8]; //0x98~0x9f					// no use
	unsigned short pManualAWBGain[8][3]; //	0xa0 d="manual AWB gain"
	unsigned char nCurveAWBMap[128]; // 0xd0 d="AWB map"
	unsigned char nMidLightMask[32]; // 0x150 d="mid light mask"
	unsigned char nHighLightMask[32];// 0x170 d="high light mask"
	unsigned char bCurveAWBOptions;  // 0x190	v=0x03	d="Curve AWB Options"
	unsigned char nAWBShiftEnable;   // 0x191	v=0x01	d="Enable/Disable Curve AWB Shift function"
//	unsigned char nMapSwitchYth;     // 0x192	v=0x05	d="Map Switch Y threshold"
//	unsigned char nCurveAWBDarkCountTh;// 0x193	v=0x10	d="Dark Count Threashold"
//	unsigned char nCurveAWBDarkCount; // 0x194	v=0x00	d="Dark Count"
	unsigned char bAWBDigiGainEnable; // 0x192 v=0x0 d="apply digi-gain on WB gain enable"
	unsigned char pAWBReserved3[2]; // 0x193~194
	unsigned char nCurveAWBDebug;  // 0x195	v=0x00	d="Curve AWB debug mode"
	unsigned short nCompensatedGain; // 0x196~7	v=0x80	d="compensated gain"
//	unsigned char nCurveAWBExtTh; // 0x198	v=0x11	d="Extension Range"
//	unsigned char nCurveAWBCutLLLAxis; // 0x199	v=0xa0	d="Curve AWB LL tolerance-X"
//	unsigned char nCurveAWBCutLLSAxis; // 0x19a	v=0x80	d="Curve AWB LL tolerance-Y"
	unsigned char pAWBReserved4[3];//0x198~19a
	unsigned char nCurveAWBBrRatio; // 0x19b v=0x20	d="AWB Brigntess Ratio"
	unsigned short nCurveAWBBrThres0; // 0x19c	v=0x200	d="AWB Brigntess Threshold0"
	unsigned short nCurveAWBBrThres1; // 0x19e	v=0x50	d="AWB Brigntess Threshold1"
	unsigned short nCurveAWBMinNum; // 0x1a0	v=0x500	d="AWB Min Count Num"
	unsigned char pAWBReserved5[2]; // 0x1a2~3
//	unsigned short nBrDGainStep; // 0x1a2	v=0x2	d="BrigntessShift Delta Gain step"
	unsigned short nCurveAWBXScale; // 0x1a4	v=0xa0	d="AWB X Scale"
	unsigned short nCurveAWBYScale; // 0x1a6 v=0x90	d="AWB Y Scale"
	short nCurveAWBXOffset;  //0x1a8 v=0x0 d="AWB x offset"
	short nCurveAWBYOffset;	 // 0x1aa v=0x0 d="AWB y offset"
	unsigned char nCurveAWBScaleBits; //0x1ac v=0x08 d="AWB scale bits"
	unsigned char bManualAWBEnable; // 0x1ad v=0x0 d="manual WB enable"
	unsigned char bWriteBackWBGainEnable; // 0x1ae v=0x1 d="sync WB gain in manual WB mode"
    unsigned char bAWBMapSwap;//0x1af
    short pAWBShiftCTCP[5];//0x1b0~9 v = 0x10 0x20 0x30 0x40 0x50 d="AWB shift CT control points"
	unsigned short nAWBBrTh[3]; // 0x1ba
	unsigned char pAWBReserved2[8]; //0x1c0~0x1c7
	unsigned short nBrD65Th;  // 0x1c8	v=0x200	d="BR D65 count throushold"
	unsigned char nAWBBrCTd65[2];  // 0x1ca	v=0x00	d="BR D65 Target B/R gains"
	unsigned short pROIBrThre[6]; //  0x1cc~1d7 d="ROI brightness control points"
	unsigned char pROIWeightList[4][6];// 0x1d8~1ef d=" ROI 0/1/2 weight list for 4 control points, ROI3 is win16x16"
	short pRGBIrOffset;  //0x1f0
	unsigned char bROIBrOption;  //0x1f2
	unsigned char bROIEnable;  //0x1f3
	unsigned char bFastAWBEnable; //0x1f4
	unsigned char pAWBReserved6[11]; //0x1f5~0x1ff

	// RGBH curve		384 bytes		0x200~0x37f
	unsigned short pRGBCurveH[33]; // 0x200~ d="RGB Curve"                 ++
	unsigned short pRGBCurveM[33];  // 0x242~ d="RGB Curve in dark"        ++
	unsigned short pRGBCurveL[33]; // 0x284~ d="curve X list"              ++
	unsigned short pHFreqGain[9]; // 0x2c6~ d="high frequency gain"        ++
	unsigned int pCurveBrThre[4];//0x2d8~0x2e7 v-0x2000 d="curve switch brightness control points"   ++
	unsigned char bDynamicCurveEnable;// 0x2e8 v-0x0 d="dynamic curve enable"
	unsigned char nCurveReserved[151];// 0x2e9~0x37f

	// LENC		48 bytes	0x380~0x3af
	unsigned char bLensAutoGainEnable; // 0x380	v=0x01	d="lens auto gain mode enable"
	unsigned char bLensAutoCTEnable; // 0x381	v=0x01	d="lens auto CT mode enable"
	unsigned char nLensMaxQ; // 0x382			v=0x40	d="lens max apply amplitude"
	unsigned char nLensMinQ; // 0x383			v=0x20	d="lens min apply amplitude"
	unsigned short pLensGain[2]; // 0x384				d="lens apply gain threshold"
	unsigned short pLensCTThre[4]; // 0x388				d="lens auto CT threshold"
	unsigned short nLensManualCT; // 0x390		v=0x80	d="lens manual CT threshold"
	unsigned char nLensProfile; // 0x392		v=0x00	d="lens profile"
	unsigned char nLensSwitchSign; // 0x393		v=0x01	d="lens profile switch flag"
	unsigned char bMirrorFlipEnable; // 0x394 v=0x0 d="mirror/flip enable"
	unsigned char pLensReserved1[4]; // 0x395~8
	unsigned char bAutoLensOnlineEnable; // 0x399	v=0x01	d="auto lens online enable"
	unsigned short nLensBrThres; // 0x39a	v=0x60	d="lens Br threshold"
	short nLensRaThres[2]; //0x39c	d="lens Ra threshold"
	unsigned char nCustomLensProfile;// 0x3a0 v=0x0 d="index of lens profile"
	unsigned char nLensBrRatio;//0x3a1 v=0x08 d="brightness ratio"
	char nLensScope[4][2];//0x3a2 d="lens scope"
	unsigned char nMaxLensGain; // 0x3aa
	unsigned char bLensOption; //0x3ab
//	unsigned char pLensReserved2[4]; // 0x3ac~0x3af				// no use
   
	// CCM		80 bytes		0x3ac~0x3ff
	unsigned char bCCMAutoCTEnable; // 0x3ac	v=0x01	d="CCM auto CT mode enable"
	unsigned char pCMXPreGain[3]; // 0x3ad		v=0x80	d="Color matrix pre gain"
	unsigned short pCCMCTThre[6]; // 0x3b0~b d="CT threshold for CCM"
	short pCCMCMatrix[3][3]; // 0x3bc			d="CCM center matrix"
	short pCCMAMatrix[3][3]; // 0x3ce		d="CCM left matrix"
	short pCCMDMatrix[3][3]; // 0x3e0		d="CCM right matrix"
	unsigned short nCCMManualCT; // 0x3f2	v=0x100	d="CCM manual CT"
	// UVSaturateAdjust
	unsigned char bUVSaturateAdjustEnable; // 0x3f4	v=0x00	d="UV saturation adjust enable"
	unsigned char nUVMaxSaturation; // 0x3f5	v=0x80	d="UV max saturation"
	unsigned char nUVMinSaturation; // 0x3f6	v=0x70	d="UV min saturation"
	unsigned char nUVSaturateOption; //0x3f7 	v=0x0
	unsigned int nUVSaturateGainThre1; //0x3f8	v=0x40	d="UV saturation gain threshold1"
	unsigned int nUVSaturateGainThre2; //0x3fc	v=0x80	d="UV saturation gain threshold2"

	// HDR & EDR	160 bytes		0x400~0x49f						
	unsigned short nLogTargetY; // 0x400	v=0x7e00 d="Log target Y"
	unsigned short nLSSensitivityRatio; // 0x402 v=0x100 d="LS sensitivity ratio"
	unsigned char bFixedRatioEnable; // 0x404	v=0x00	d="Fixed HDR ratio enable"
	unsigned char nFixedRatio; // 0x405		v=0x07	d="Fixed HDR ratio"
	unsigned char nMaxFastRatio; // 0x406	v=0x40	d="max fast ratio"
	unsigned char nMaxSlowRatio; // 0x407	v=0x04	d="max slow ratio"
	unsigned char nHDRGainStep; // 0x408	v=0x04	d="HDR gain step"
	unsigned char nExpRatioStep; // 0x409	v=0x20	d="exposure ratio step"
	unsigned char nGammaStep; // 0x40a		v=0x04	d="gamma step"
	unsigned char nMinSaturateLevel; // 0x40b v=0x20	d="min saturation level"
	unsigned short pMinGammaList[3]; // 0x40c	d="min gamma list"
	unsigned short pMaxGammaList[3]; // 0x412	d="max gamma list"
	unsigned short pDynamicRangeList[3]; // 0x418	d="dynamic range list"
	unsigned short nMaxRatio; // 0x41e v=0x100	d="max LS ratio"
	unsigned short nMinRatio; // 0x420 v=0x02	d="min LS ratio"
	unsigned char bReduceNoiseInDarkEnable; // 0x422 v=0x00 d="reduce noise in dark mode enable"
	unsigned char bReduceNoiseMinGain; // 0x423 v=0x60	d="reduce noise start gain"
	unsigned char bReduceNoiseMaxGain; // 0x424 v=0x80	d="reduce noise end gain"
	unsigned char bReduceNoiseStep; // 0x425 v=0x10	d="reduce noise step"
	unsigned char pHDRReserved1[2]; // 0x426~
	// Combine, Gamma, Normalize & ToneMapping
	unsigned char bCutBlackLevelEnable; // 0x428 v=0x01 d="cut black level enable"
	unsigned char bDarkBoostAutoSwitchEnable; // 0x429 v=0x01 d="dark boost auto switch enable"
	unsigned char bHDRGammaSwap; // 0x42a
	unsigned char bManualGammaEnable; // 0x42b v=0x01 d="manual gamma enable"
	unsigned short nManualGamma; // 0x42c v=0x101	d="manual gamma"
	unsigned short nRGBGamma; // 0x42e v=0x101	d="RGB gamma"
	unsigned short nMaxCurveGain; // 0x430 v=0x20	d="max curve gain"
	unsigned short nMaxRGBGammaGain; // 0x432 v=0x20	d="max RGB gamma gain"
	unsigned int nLocalCombineNum; // 0x434 v=0x5355	d="local statistic number"
	unsigned short nDarkBoostGainThre1; // 0x438 v=0x20 d="dark boost gain threshold 1"
	unsigned short nDarkBoostGainThre2; // 0x43a v=0x80 d="dark boost gain threshold 2"
	unsigned char nDarkBoostAmount; // 0x43c v=0x10 d="dark boost amount"
	unsigned char nDarkBoostAmountMin; // 0x43d v=0x00 d="dark boost amount min"
	unsigned char nDarkBoostAmountMax; // 0x43e v=0x10 d="dark boost amount max"
	unsigned char nHighLightDepressAmount; // 0x43f v=0x10 d="high light depress amount"
	unsigned short nDarkBoostMaxGamma; // 0x440 v=0x3a0 d="dark boost max gamma"
	unsigned short nDarkToneWidth; // 0x442 v=0x1000	d="dark tone width"
	unsigned short pLBDynamicRangeList[2]; // 0x444	d="LB dynamic range list"
	unsigned short pSaveRange[2]; // 0x448			d="save range"
	unsigned short nHighlightDepressMaxGamma; // 0x44c v=0x500 d="high light depress max gamma"
	unsigned char nStepE; // 0x44e v=0x04	d="error step"
	unsigned char nErrorMinNumPer; // 0x44f v=0x01 d="error min number percentage"
	unsigned char pContrastCurve[15]; // 0x450	d="contrast curve"
	unsigned char nCurveStep; // 0x45f	v=0x80	d="curve step"
	unsigned short nMinDynamicRange; // 0x460 v=0x20 d="min dynamic range"
	unsigned short nMaxDynamicRange; // 0x462 v=0x200 d="max dynamic range"
	unsigned char nMinAlpha; // 0x464 v=0x40 d="min alpha"
	unsigned char nMaxAlpha; // 0x465 v=0xa0 d="max alpha"
	unsigned char nGammaCutRatio; // 0x466 v=0x20 d="gamma cut ratio"
	unsigned char nUVCutLowBrightThres; // 0x467 v=0x10 d="UV cut low bright threshold"
	unsigned char bEDRGainControlEnable; // 0x468 v=0x00 d="EDR gain control enable"
	unsigned char bCurveGainControlEnable; // 0x469 v=0x00 d="curve gain control enable"
	unsigned char nCurveGainThre1; // 0x46a v=0x20 d="curve gain threshold1"
	unsigned char nCurveGainThre2; // 0x46b v=0x80 d="curve gain threshold2"
	unsigned char bUVAdaptiveCutEnable; // 0x46c v=0x00 d="UV adaptive cut enable"
    unsigned char nUVCutHighBrightThres; // 0x46d v=0x18 d="UV cut high bright threshold"
	unsigned char nLowGainThres; // 0x46e v=0x68 d="low gain threshold"
	unsigned char nUpperGainThres; // 0x46f v=0x78 d="upper gain threshold"
	unsigned char pContrastCurveHighGain[15]; // 0x470 d="contrast curve in high gain"
	unsigned char bAdaptiveGammaCurveEnable; // 0x47f v=0x00 d="adaptive gamma curve enable"
	unsigned short nLowGainGamma; // 0x480 v=0x101 d="low gain gamma"
	unsigned short nHighGainGamma; // 0x482 v=0x101 d="high gain gamma"
	unsigned short nDarkImageGamma; // 0x484 v=0x101 d="dark image gamma"
	unsigned char nLowGainSlope; // 0x486 v=0x88 d="low gain slope"
	unsigned char nHighGainSlope; // 0x487 v=0x88 d="high gain slope"
	unsigned char nDarkImageSlope; // 0x488 v=0x40 d="dark image slope"
	unsigned char nLowBrightThres; // 0x489 v=0x10 d="low bright threshold"
	unsigned char nHighBrightThres; // 0x48a v=0x20 d="high bright threshold"
	unsigned char nRGBCurveGain1; // 0x48b v=0x40 d="gain threshold 1 for RGBCurve"
	unsigned char nRGBCurveGain2; // 0x48c v=0x80 d="gain threshold 2 for RGBCurve"
	unsigned char bDarkBoostEnable; // 0x48d
	unsigned char bHighLightDepressEnable; // 0x48e
	unsigned char bCompensateErrorEnable; // 0x48f
	unsigned char bRGBGammaEnable; // 0x490
	unsigned char pHDRReserved2; // 0x491					// no use
	short pCCMHMatrix[3][3]; // 0x492~4a3			d="CCM matrix H"

	unsigned int nDebugTime0;  //0x4a4
	unsigned int nDebugTime1;  //0x4a8
	unsigned int nDebugTime2;  //0x4ac
	unsigned int nDebugTime3;  //0x4b0
	unsigned int nDebugTime4;  //0x4b4
	unsigned int nDebugTime5;  //0x4b8
	unsigned int nDebugTime6;  //0x4bc
	unsigned int nDebugTime7;  //0x4c0
	unsigned char pReserved2[156]; //0x4a4~0x55f  [188]

	// Debug
	// 1376~1407	32 bytes			0x560~0x57f
	unsigned char bDebugEnable; // 0x560	v=0x00 d="firmware debug enable"	
	unsigned char nDebugAECMode; // 0x561	v=0x00 d="AEC debug mode"
	unsigned char nDebugCombineMode; // 0x562	v=0x00 d="combine debug mode"
	unsigned char nDebugNormalizeMode; // 0x563	v=0x00 d="normalize debug mode"
	unsigned char nDebugToneMappingMode; // 0x564 v=0x00 d="tone mapping debug mode"
	unsigned char nDebugAWBMode; // 0x565 v=0x00 d="AWB debug mode"
	unsigned char nDebugOthersMode; // 0x566 v=0x00 d="Other modules debug mode"
	unsigned char nDebugSystemMode; // 0x567 v=0x00 d="system debug mode"
	unsigned char pDebugReserved[8]; // 0x568~0x56f			// no use

	unsigned char bPreviewEnable;//0x570
	unsigned char pReserved3[48]; //0x571~0x5a0

	//new AWB shift 61bytes
	unsigned char nAWBShiftCT00;//0x5a1 v=0x33 d="CT threshold 00 of AWB shift"
	unsigned char nAWBShiftCT000;//0x5a2 v=0x23 d="CT threshold 000 of AWB shift"
	unsigned char nAWBHighlightShiftBrRatio;//0x5a3 v=0x20 d="brightness ratio"
	unsigned short nAWBHighlightShiftBrThres0;//0x5a4~5 v=0xf0 d="brightness threshold 0"
	unsigned short nAWBHighlightShiftBrThres1;//0x5a6~7 v=0x800 d="brightness threshold 1"
	unsigned char nAWBShiftLowBrightThres;//0x5a8 v=0x04 d="low brightness theshold"
	unsigned char nAWBShiftHighBrightThres;//0x5a9 v=0x22 d="low brightness theshold"
	unsigned short nAWBBrTh_3;//0x5aa~b v=0x3000 d="brightness threshold"
	unsigned char nAWBShiftDX0[4];//0x5ac~f d="AWB shift DX0"
	unsigned char nAWBShiftDY0[4];//0x5b0~3 d="AWB shift DY0"
    unsigned char nAWBShiftDX1[4];//0x5b4~7 d="AWB shift DX1"
    unsigned char nAWBShiftDY1[4];//0x5b8~b d="AWB shift DY1"
    unsigned char nAWBShiftDX2[4];//0x5bc~f d="AWB shift DX2"
    unsigned char nAWBShiftDY2[4];//0x5c0~3 d="AWB shift DY2"
    unsigned char nAWBShiftDX00[4];//0x5c4~7 d="AWB shift DX00"
    unsigned char nAWBShiftDY00[4];//0x5c8~b d="AWB shift DY00"
    unsigned char nAWBShiftDX000[4];//0x5cc~f d="AWB shift DX000"
    unsigned char nAWBShiftDY000[4];//0x5d0~3 d="AWB shift DY000"
    unsigned char pNewAWBReserved1[3]; //0x5d4~6
	unsigned char nAWBShiftCwf[3];//0x5d7~9 d="AWB shift in cwf"
	unsigned char nAWBShiftA[3];//0x5da~c d="AWB shift in A"
	unsigned char pNewAWBReserved2[6];//0x5dd~5e2

	// auto frame rate
	unsigned char bAutoFrameRateEnable;// 0x5e3 v=0x0 d="auto frame rate enable"
	unsigned short nAFRGainThre[3];// 0x5e4~0x5e9 d="gain threshold of auto frame rate"
	unsigned short nAFRRatio[3];// 0x5ea~ef d = "ratio of auto frame rate "
	unsigned short nSVNRevision; // 0x5f0
	
	unsigned char pGlobalReserved[270];	// 0x5f2~6ff			// no use

}TGlobalControl;

typedef struct tagLENCProfile	// item=2	baseaddress1=0x80032700	baseaddress2=0x80034700		
{
	unsigned char pLensCPArray[3][768]; // L: 0x32700~, R: 0x34700~ 
}TLENCProfile;

typedef volatile struct tagMiddleVariableGroup		// L: 0x300b4	R: 0x303a0
{
	// Global transition variables
	unsigned char bVblankingFlag; // 0x00
	unsigned char bI2CBusyFlag;
	unsigned char nTargetOffset;
	unsigned char nHighLevel;
	unsigned char nAECAGCILatch; // 0x04
	unsigned char bAECStableFlagBuf;
	unsigned char nMeanY;
	unsigned char nSensorProcessFrameFlag; 
	unsigned char bFrameProcessFinishedFlag; // 0x08
	unsigned char bWExpEnable;
	unsigned char bWRegEnable;
	unsigned char bWGainEnable;
	unsigned int pExpBuf[2]; // 0x0c
	unsigned short pGainBuf[2]; // 0x14
	unsigned int nLastExp; // 0x18
	unsigned short nLastGain;// 0x1c
	unsigned short nWriteBackGain;
	unsigned int nWriteBackExpo; // 0x20
	unsigned char nNewAECTarget; // 0x24
	unsigned char bTargetStableFlag;
	unsigned short nStretchGainBuf;
	unsigned short nBand; // 0x28
	short nBrprvDRGain;
//	unsigned char bManualAWBEnable; // 0x2c
	unsigned char bWExpEnable_HDRECO; // 0x2c
	unsigned char nCurveAWBShiftValue;
	unsigned short nAnaGain; // 0x2e
	unsigned short nDigiGain; // 0x30
	unsigned short pAWBGainBuf[3]; // 0x32
	short pLensOnlineBR[2]; // 0x38
	unsigned char nAECReserved[2]; // 0x3c
	unsigned short nApplyDigiGain; //0x3e
	unsigned int nApplyAnaGain; // 0x40
	short pCropPos[2]; // 0x44
	short pTotalMotion[2]; // 0x48
//	unsigned char nCombineWorkMode; // 0x4c
	//unsigned char bDarkBoostEnable;
	//unsigned char bHighLightDepressEnable;
	//unsigned char bCompensateErrorEnable; 	
	unsigned char bIDISOFFlag;// 0x4c
	unsigned char bIDIEOFFlag;  
	//unsigned short nApplyAnaGain;
	unsigned short nCurrentVTS; // 0x4e
	unsigned short nHighLightDepressGamma; // 0x50
	unsigned short nMinGamma;
	unsigned short nMaxGamma; // 0x54
	unsigned short nGamma;
	unsigned int nHDRGain; // 0x58
	unsigned int nCurrentRatio; // 0x5c
	unsigned int nExpectRatio; // 0x60
	unsigned short nDynamicRange; // 0x64
	unsigned short nNormalizedLogMeanY;
	int pLocalMeanYBuf[63]; // 0x68
	unsigned int nOutputBlackLevel;// 0x164
	int nDelE; // 0x168
//	unsigned char bRGBGammaEnable; // 0x16c
	unsigned char bLensOnlineStatDone; // 0x16c
	unsigned char nCurveAWBMode; // 0x16d
	unsigned char bRGBHCurveEnable; // 0x16e
	unsigned char bApplyNewGain;// 0x16f
	unsigned int nBrightnessMetric;// 0x170~173
	unsigned int nInitTime;//0x174~177

	unsigned int nEOFTime;//0x178~b
	unsigned int nDebugTime1;//0x17c~f
	unsigned int nDebugTime2;//0x180~3
	unsigned int nDebugTime3;//0x184~7
	unsigned int nDebugTime4;//0x188~b

	unsigned char bMACReadDoneFlagPort0; //0x18c
	unsigned char bMACReadDoneFlagPort1; //0x18d
	unsigned char bMACReadDoneFlagPort2; //0x18e
	unsigned char bMACReadDoneFlagPort3; //0x18f

	unsigned char bMACWriteDoneFlagPort0; //0x190
	unsigned char bMACWriteDoneFlagPort1; //0x191
	unsigned char bMACWriteDoneFlagPort2; //0x192
	unsigned char bMACWriteDoneFlagPort3; //0x193
	unsigned char bMACWriteDoneFlagPort4; //0x194
	unsigned char bMACWriteDoneFlagPort5; //0x195
	unsigned char bVTSChangeFlag;//0x196 0-> VTS do not change; 1-> VTS decrease; 2-> VTS increase
	unsigned char bRealAECStableFlagBuf;//0x197  judge from Gain or Exposure

	//unsigned char nReserved[2]; //0x197
	
	unsigned char nCurveAWBReadIndex; // 0x198	v=0x88	d="Current Map Read index"
	unsigned char nCurveAWBReadValue; // 0x199	v=0x00	d="Current Map Read Value"
//	unsigned char pReserved[2]; // 0x198~0x199				// no use
	
	unsigned char pOTPBufFlag[2]; // 0x19a
	unsigned short pOTPPos[2]; // 0x19c
	// end of 416 bytes
	unsigned int nMaxG; // 0x1a0
	unsigned int nMinG; // 0x1a4

	unsigned short nCT; // 0x1a8
	unsigned char nCurveAWBIndex_x; // 0x1aa
	unsigned char nCurveAWBIndex_y;

	unsigned char nAECDoneStep;  // 0x1ac
	unsigned char nFrameProcessStep;

	unsigned char bVsyncFlag; //0x1ae
	unsigned char bSOFFlag;
	unsigned char bEOFFlag;  //0x1b0 L:0x3025c
	unsigned char bGMVDoneFlag;
	unsigned char bAECDoneFlag;
	unsigned char bAFCDoneFlag;
	unsigned char bStatDoneFlag;  //0x1b4
	unsigned char bAntiShakeDoneFlag; 
	
	unsigned char bNormalModeEnable; // 0x1b6
	unsigned char bCombineEnable;//0x1b7

	unsigned char bPDAFDoneFlag; //0x1b8
//	unsigned char nMeanY13;//0x1b9

	unsigned char nReserved[3]; //0x1b9
	unsigned int nAECStatParaBuf[14]; //0x1bc
	unsigned int nRGBIrGain; //0x1f4
	unsigned int nLum; //0x1f8; nMeanY/(ExpBuf[0]*GainBuf[0])
}TMiddleGroup;


extern TBlankingReadControl *g_pBlankingRead_L;
extern TBlankingReadControl *g_pBlankingRead_R;
extern TReadHistControl *g_pReadHist_L;
extern TReadHistControl *g_pReadHist_R;
extern TReadMeanControl *g_pReadMean_L;
extern TReadMeanControl *g_pReadMean_R;
extern TFrameWriteControl *g_pWrite_L;
extern TFrameWriteControl *g_pWrite_R;
extern TWriteLENCControl *g_pWriteLENC_L;
extern TWriteLENCControl *g_pWriteLENC_R;
extern TGlobalControl *g_pControl_L;
extern TGlobalControl *g_pControl_R;
extern TLENCProfile *g_pLENCProfile_L;
extern TLENCProfile *g_pLENCProfile_R;
extern TMiddleGroup g_tMiddle_L;
extern TMiddleGroup g_tMiddle_R;
extern TReadPDAFControl *g_pReadPDAF_L;



extern volatile unsigned char g_dPC;
extern volatile unsigned int  g_dFC;

// function declaration
// communicate
void ISPFirmwareInitialize(void);
#ifdef __OPTIMIZATION_FOR_ROM_SIZE__
void WriteHWRegister16BIT(unsigned int reg, unsigned short data);
void MulWriteHWRegister16BIT(unsigned int reg, unsigned short *addr);
void WriteHWRegister32BIT(unsigned int reg, unsigned int data);
void ClearVsyncFlag(unsigned char nPipeline);
void ClearSOFFlag(unsigned char nPipeline);
void ClearEOFFlag(unsigned char nPipeline);
void ClearMACDoneFlag(unsigned char nOutportSelection);
unsigned char QueryVsyncFlag(unsigned char nPipeline);
unsigned char QuerySOFFlag(unsigned char nPipeline);
unsigned char QueryEOFFlag(unsigned char nPipeline);
unsigned char QueryMACDoneFlag(unsigned char nOutportSelection);
void WaitForEOF(unsigned char times);
void WaitForSOF(void);
void WaitForEOFFlag(unsigned char times, unsigned char nPipeline);
void WaitForCurrentEOFFlag(unsigned char times, unsigned char nPipeline);
void WaitForSOFFlag(unsigned char nPipeline);
void WaitForCurrentSOFFlag(unsigned char nPipeline);
void MACInterruptDisable(void);
void MACInterruptEnable(void);
#endif
void WriteHWRegisterMask(unsigned int reg, unsigned char data, unsigned char mask);
unsigned char WriteSensorRegister(TGlobalControl *pGlobalControl, unsigned char id, unsigned char index, unsigned short data, unsigned char bData16b);
unsigned short ReadSensorRegister(TGlobalControl *pGlobalControl, unsigned char id, unsigned char index, unsigned char bData16b);
unsigned char WriteI2CRegister(unsigned char nI2COption, unsigned char nDeviceID, unsigned char *pData, unsigned char length);
unsigned char ReadI2CRegister(unsigned char nI2COption, unsigned char nDeviceID, unsigned char *pData, unsigned char length);
int LinearToLog(int nX);
int LogToLinear(int nY);
void GetInverseValue(unsigned int nX, unsigned char *A, unsigned char *B);
int ComputeLogRatio(int nRatio, int nShift);
void GetCameraExposureReg(unsigned int exp[2]);
void GetCameraGainReg(unsigned int gain[2]);
void SetCameraGainReg(TGlobalControl *pGlobalControl, unsigned int nSensorGain);
void SetCameraDigiGainReg(TGlobalControl *pGlobalControl, unsigned short nDigiGain);
void ApplyExposure(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup);
void UpdateVTS(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup);
void IdleProcess(unsigned char nFlag);
void REG32Mask(INT32U reg, INT32U data, INT32U mask);

// Tasks
int RunTasks(void);
void AutoFocus(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TReadPDAFControl *pReadPDAF, unsigned char bLFlag);
int AECDoneProcess(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TReadHistControl *pReadHist, TReadMeanControl *pReadMean,unsigned char bLFlag);
void AECDoneProcess_1(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TReadHistControl *pReadHist, unsigned char bLFlag);
void AECDoneProcess_2(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TReadMeanControl *pReadMean,unsigned char bLFlag);
void AECDoneProcess_3(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, unsigned char bLFlag);
void UpdateAECParaInHDR(TGlobalControl *pGlobalControl_L, TGlobalControl *pGlobalControl_R, TMiddleGroup *pMiddleGroup_L, TMiddleGroup *pMiddleGroup_R);
void SyncUpDualPipe(TMiddleGroup *pMiddleGroup_L, TMiddleGroup *pMiddleGroup_R);
void BlankingProcess(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, TFrameWriteControl *pDMAWrite, unsigned char bLFlag);
int FrameProcess(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, TFrameWriteControl *pDMAWrite, TWriteLENCControl *pDMAWrite_2, TLENCProfile *pLENCProfile, unsigned char bLFlag);
void FrameProcess_1(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, TFrameWriteControl *pDMAWrite);
void FrameProcess_2(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void FrameProcess_3(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void FrameProcess_4(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void FrameProcess_5(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TLENCProfile *pLENCProfile, TWriteLENCControl *pDMAWrite_2);
void FrameProcess_6(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, TFrameWriteControl *pDMAWrite);
void FrameProcess_7(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, unsigned char bLFlag);
void UpdateParaInShort(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite_L, TWriteLENCControl *pDMAWrite_2_L, TFrameWriteControl *pDMAWrite_S, TWriteLENCControl *pDMAWrite_2_S, TBlankingReadControl *pDMARead);

void OutputStatistic(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, unsigned char bLFlag);
void OutputWriteParameters(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, unsigned char bLFlag);
void CombineTwoPipelineStatistic(TBlankingReadControl *pDMARead_L, TBlankingReadControl *pDMARead_R);
void EstimateAECAGC(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup);
void AECAGCHDRControl(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
unsigned int ComputeSensorGain(unsigned int G, int nMode, int nFracBit);
unsigned short SensorGainFormat(unsigned int G, unsigned int *DG, int nMode, int nFracBit);
unsigned short DigiGainFormat(unsigned int G, int nMode);
void NewGainConversion(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup);
//int clip(int x, int a, int b);
int LinearInterpolation(int x, int x0, int x1, int y0, int y1);
void EstimateAWBGain(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void ManualAWBProcess(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
void GetWBGainReg(unsigned short pCFAGain[4], short pCFAOffset[4], int offset);
void SetWBGainReg(unsigned short pCFAGain[4], short pCFAOffset[4], int offset);
void Combine(void);
void EstimateCombineError(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void UpdateLocalControl(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
void FinishLogYStatistics(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void Gamma(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void EstimateCurve(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void UpdateRGBCurve(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
//void GainControlCip(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup,unsigned char bLFlag);
void Normalize(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);
void LensCorrection(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TLENCProfile *pLENCProfile, TWriteLENCControl *pDMAWrite_2);
void LensOnline(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TBlankingReadControl *pDMARead, TFrameWriteControl *pDMAWrite, unsigned char bPreview, unsigned char bLFlag);
void ColorMatrix(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
void Stretch(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite, TBlankingReadControl *pDMARead);

void CommandSetProcess(void);
void CurveAdjust(TGlobalControl *pGlobalControl);
void FirmwareGroupWriteProcess(void);
//unsigned char GetGlobalMeanY(TGlobalControl *pGlobalControl,TReadMeanControl *pReadMean);
void HistStretchGain(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TReadHistControl *pReadHist);
void CurveAWBMapUpdate(TGlobalControl *pGlobalControl, TMiddleGroup *pMiddleGroup, TFrameWriteControl *pDMAWrite);
int log2_256(short in);
s32 Division_64bit_32bit(u32 tmp_hi, u32 tmp_lo, u32 w);
u32 tick1_get_cycle(void);

// INT
void Irq_DMADone(void);
void Irq_PD_DETECTION_DONE(void);
void Irq_VSYNC_L(void);
void Irq_VSYNC_R(void);
void Irq_ISP_SOF_L(void);
void Irq_ISP_EOF_L(void);
void Irq_ISP_SOF_R(void);
void Irq_ISP_EOF_R(void);
void Irq_LINE_INT(void);
void Irq_AECDone_L(void);
void Irq_AECDone_R(void);
void Irq_LENSON_DONE_L(void);
void Irq_LENSON_DONE_R(void);
void Irq_ISP_Stat_L(void);
void Irq_ISP_Stat_R(void);

//
void init_algo(void);
void init_dma(void);

#endif

