#ifndef __BYTE_ORDER_H__
#define __BYTE_ORDER_H__

#ifndef HOST_APP
#define CONFIG_LITTLE_ENDIAN
#endif

enum data_endian_type {
	LITTLE_ENDIAN_TYPE,
	BIG_ENDIAN_TYPE,
};

typedef u16 __le16;
typedef u16 __be16;
typedef u32 __le32;
typedef u32 __be32;
typedef u64 __le64;
typedef u64 __be64;

#ifdef CONFIG_BIG_ENDIAN
#define BIG_ENDIAN 4321 //to compatible with old codes
#include "big_endian.h"
#else
#define LITTLE_ENDIAN 1234
#include "little_endian.h"
#endif

#include "generic.h"

#define __getu16_le(x) ( ((*((unsigned char *)(x))) ) | (*((unsigned char *)(x) + 1) << 8) ) 
#define __setu16_le(x,y) do { *(unsigned char *)(x) = (y) & 0xff ;  *((unsigned char *)(x) + 1) = (y) >> 8 ; }while(0)
#define __getu32_le(x) ( ((*((unsigned char *)(x))) ) | (*((unsigned char *)(x) + 1) << 8) | (*((unsigned char *)(x) + 2) << 16) | (*((unsigned char *)(x) + 3) << 24) ) 
#define __setu32_le(x,y) do { *(unsigned char *)(x) = (y) & 0xff ;  *((unsigned char *)(x) + 1) = ((y) >> 8) & 0xff ;  *((unsigned char *)(x) + 2) = ((y) >> 16) & 0xff ; *((unsigned char *)(x) + 3) = ((y) >> 24) & 0xff ;}while(0)

#define __getu16_be(x) ( ((*((unsigned char *)(x) + 1)) ) | (*((unsigned char *)(x)) << 8) ) 
#define __setu16_be(x,y) do { *((unsigned char *)(x) + 1 ) = (y) & 0xff ;  *((unsigned char *)(x)) = (y) >> 8 ; }while(0)
#define __getu32_be(x) ( ((*((unsigned char *)(x) + 3 )) ) | (*((unsigned char *)(x) + 2) << 8) | (*((unsigned char *)(x) + 1) << 16) | (*((unsigned char *)(x)) << 24) ) 
#define __setu32_be(x,y) do { *((unsigned char *)(x) + 3) = (y) & 0xff ;  *((unsigned char *)(x) + 2) = ((y) >> 8) & 0xff ;  *((unsigned char *)(x) + 1) = ((y) >> 16) & 0xff ; *((unsigned char *)(x) ) = ((y) >> 24) & 0xff ;}while(0)

static inline u16 getu16_le(void * addr){
	return __getu16_le(addr);
}
static inline void setu16_le(void * addr, u16 d){
	__setu16_le(addr, d);
}
static inline u32 getu32_le(void * addr){
	return __getu32_le(addr);
}
static inline void setu32_le(void * addr, u32 d){
	__setu32_le(addr, d);
}

static inline u16 getu16_be(void * addr){
	return __getu16_be(addr);
}
static inline void setu16_be(void * addr, u16 d){
	__setu16_be(addr, d);
}
static inline u32 getu32_be(void * addr){
	return __getu32_be(addr);
}
static inline void setu32_be(void * addr, u32 d){
	__setu32_be(addr, d);
}

#endif
