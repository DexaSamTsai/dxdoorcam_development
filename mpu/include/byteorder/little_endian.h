#ifndef _BYTEORDER_LITTLE_ENDIAN_H
#define _BYTEORDER_LITTLE_ENDIAN_H

#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1234
#endif
#ifndef __LITTLE_ENDIAN_BITFIELD
#define __LITTLE_ENDIAN_BITFIELD
#endif

#include "swab.h"

#define __constant_htonl(x) (( __be32)___constant_swab32((x)))
#define __constant_ntohl(x) ___constant_swab32(( __be32)(x))
#define __constant_htons(x) (( __be16)___constant_swab16((x)))
#define __constant_ntohs(x) ___constant_swab16(( __be16)(x))
#define __constant_cpu_to_le32(x) (( __le32)(u32)(x))
#define __constant_le32_to_cpu(x) (( u32)(__le32)(x))
#define __constant_cpu_to_le16(x) (( __le16)(u16)(x))
#define __constant_le16_to_cpu(x) (( u16)(__le16)(x))
#define __constant_cpu_to_be32(x) (( __be32)___constant_swab32((x)))
#define __constant_be32_to_cpu(x) ___constant_swab32(( u32)(__be32)(x))
#define __constant_cpu_to_be16(x) (( __be16)___constant_swab16((x)))
#define __constant_be16_to_cpu(x) ___constant_swab16(( u16)(__be16)(x))
#define __cpu_to_le32(x) (( __le32)(u32)(x))
#define __le32_to_cpu(x) (( u32)(__le32)(x))
#define __cpu_to_le16(x) (( __le16)(u16)(x))
#define __le16_to_cpu(x) (( u16)(__le16)(x))
#define __cpu_to_be32(x) (( __be32)__swab32((x)))
#define __be32_to_cpu(x) __swab32(( u32)(__be32)(x))
#define __cpu_to_be16(x) (( __be16)__swab16((x)))
#define __be16_to_cpu(x) __swab16(( u16)(__be16)(x))

#endif
