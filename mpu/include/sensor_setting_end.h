

#define SENSOR_EFFECT_SET_INTERNAL(sname, smode, sid, stype, sif) \
__sensor_cfg t_sensor_cfg g_sensor##sname##_cfg = { \
	.sccb_mode = smode, \
	.sccb_id = sid, \
	.datafmt = stype, \
	.interface_type = sif, \
	.name = #sname, \
	.detect = sensor__detect, \
	.init_config = sensor__init, \
	.init_config_len =  sizeof(sensor__init)/sizeof(sensor__init[0]), \
	.effects = tss, \
	.effect_cnt = sizeof(tss)/sizeof(tss[0]), \
	.sensor_effect_change = sensor__effect_change, \
	.isp_init = sensor__ispinit, \
	.isptables = &sensor__isptables, \
	.rgbir_init = sensor__rgbirinit, \
};

#define SENSOR_EFFECT_SET(name, smode, sid, stype, sif) SENSOR_EFFECT_SET_INTERNAL(name, smode, sid, stype, sif)

SENSOR_EFFECT_SET(SENSOR_NAME_INTERNAL, SENSOR_SCCB_MODE_INTERNAL, SENSOR_SCCB_ID_INTERNAL, SENSOR_DATAFMT_INTERNAL, SENSOR_INTERFACETYPE_INTERNAL)


#ifdef SENSOR_NAME_INTERNAL2
SENSOR_EFFECT_SET(SENSOR_NAME_INTERNAL2, SENSOR_SCCB_MODE_INTERNAL2, SENSOR_SCCB_ID_INTERNAL2, SENSOR_DATAFMT_INTERNAL2, SENSOR_INTERFACETYPE_INTERNAL2)
#endif //SENSOR_NAME_INTERNAL2
