#ifndef _INCLUDES_H_
#define _INCLUDES_H_

#include "stdarg.h"
#include "data_type.h"
#include "byteorder/byteorder.h"

#define CONFIG_UART_NEW

#include "board.h"
#include "spr_defs.h"

#include "tm.h"
#include "mem_pool.h"

#include "newos.h"
#include "irq.h"
#include "tick.h"

#include "uart_physical.h"
#include "sccb.h"
#include "libsccb.h"
#include "libsccb_old.h"
#include "libdma.h"

#include "rom_funcs.h"

#include "libdcpu.h"

#include "frame.h"
#include "framelist.h"
#include "sensor.h"
#include "libdatapath/libdatapath.h"
#include "libvenc_rc.h"
#include "libvs_common.h"
#include "hw_fw_io.h"
#include "isp.h"
#include "isp_reg.h"
#include "algo.h"
#include "isp_daynight.h"
#include "isp_sde.h"

#include "liba_dec_internal.h"
#include "liba_enc_internal.h"

#include "debug.h"
//#include "libvosd.h"
#include "libmd_common.h"
/*
#ifdef FORBA2
#define debug_printf uprintf2
#else
#define debug_printf uprintf
#endif
*/

//#define debug_printf(x...) do{ if(is_cpu2()){ uprintf2(x); } else {uprintf(x);} }while(0)

//#define debug_printf dprintf

//from link script
extern unsigned int ram_bss_start;
extern unsigned int ram_bss_end;
extern unsigned int buffer_start;

// Static assert
#define ENABLE_STATIC_ASSERT        1

#if ENABLE_STATIC_ASSERT
#define _STATIC_ASSERT(EXP,MSG)      _Static_assert(EXP,MSG)
#else
#define _STATIC_ASSERT(EXP,MSG)
#endif //ENABLE_STATIC_ASSERT

#endif
