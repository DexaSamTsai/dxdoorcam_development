#ifndef _BOARD_H_
#define _BOARD_H_

#define WriteReg32(addr,data) (*(volatile unsigned long *)(addr)=(data))
#define WriteReg16(addr,data) (*( volatile unsigned short *)(addr)=(data))
#define WriteReg8(addr,data) (*( volatile unsigned char *)(addr)=(data))

#ifndef COMPILE_FOR_AS
unsigned long ReadReg32_func(volatile unsigned long * addr);
unsigned short ReadReg16_func(volatile unsigned short * addr);
unsigned char ReadReg8_func(volatile unsigned char * addr);
#endif

#define ReadReg32(addr) ReadReg32_func((volatile unsigned long *)(addr))
#define ReadReg16(addr) ReadReg16_func((volatile unsigned short *)(addr))
#define ReadReg8(addr) ReadReg8_func((volatile unsigned char *)(addr))

#define ReadFIFO32(addr) (*( volatile unsigned long *)(addr))
#define ReadFIFO16(addr) (*( volatile unsigned short *)(addr))
#define ReadFIFO8(addr) (*(volatile unsigned char *)(addr))

#include "board_reg.h"

#define IN_CLK      24000000

#define CONFIG_ICACHE
#define CONFIG_DCACHE

////////////////////////////////////////////////////
////////////////////////////////////////////////////

#define MEMBASE_DDR        (0x90100000)


/*********************************************
   Interrupt Number
  *********************************************/
/**
 * @defgroup IRQ_BIT
 * @brief interrupt bits
 * @ingroup libirq
 * @{
 */

/*****************************************************/
// BA22 register  
/*****************************************************/
#define REG_INT_BASE   0xc0039800

////////////////////////////////////////////////////
////////////////////////////////////////////////////

////////////////////////////////////////////////////
////////////////////////////////////////////////////

#define MTSPR(reg,data) asm volatile ("l.mtspr r0,%0,%1": : "r" (data), "i" (reg))
#define MFSPR(reg,data) asm volatile ("l.mfspr %0,r0,%1": "=r" (data) : "i" (reg))
//#define NOP asm volatile ("l.nop")

#define ENABLE_TICK do{u32 sr; MFSPR(SPR_SR, sr); sr |= SPR_SR_TEE; MTSPR(SPR_SR, sr); }while(0)
#define ENABLE_INTERRUPT do{u32 sr; MFSPR(SPR_SR, sr); sr |= SPR_SR_IEE; MTSPR(SPR_SR, sr); }while(0)
#define ENABLE_INTBIT(x) do{u32 sr; MFSPR(SPR_PICMR, sr); sr |= x; MTSPR(SPR_PICMR, sr); }while(0)

#endif
