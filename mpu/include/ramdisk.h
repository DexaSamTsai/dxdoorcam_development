#ifndef _RAMDISK_H_
#define _RAMDISK_H_

/* ram disk Image Definitions */
#define MSC_ImageSize   0x00001000
#define MSC_ImageStart  0x00820000

extern const unsigned char DiskImage[MSC_ImageSize];   /* Disk Image */

#endif
