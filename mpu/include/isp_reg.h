#ifndef _ISP_REG_H_
#define _ISP_REG_H_

/******* LENC ********/
#define ISP_LENC_ADDR_L							0xe0088100
#define ISP_LENC_ADDR_R							0xe008c100
#define ISP_LENC_OFFSET_HVSCALE		(0x8)

#endif
