#if (SENSOR_SCCB_MODE_INTERNAL == SCCB_MODE8)
#define SENSOR_SETTING_UTYPE u8
#else
#define SENSOR_SETTING_UTYPE u16
#endif

#define SENSOR_SETTING_USTATIC static __unused

#define SENSOR_SETTING_TABLE SENSOR_SETTING_USTATIC SENSOR_SETTING_UTYPE
#define SENSOR_SETTING_FUNC SENSOR_SETTING_USTATIC

#define SENSOR_SETTING_SUPPORT_INTERNAL1(w, h, f) \
				if((cfg->cur_video_size == ((w<<16) | h)) && ((cfg->cur_frame_rate == f))){\
						cfg1 = (SENSOR_SETTING_UTYPE *)sensor__size_##w##_##h; \
						len1 = sizeof(sensor__size_##w##_##h)/sizeof(sensor__size_##w##_##h[0]); \
						cfg2 = (SENSOR_SETTING_UTYPE *)sensor__size_##w##_##h##_framerate_##f; \
						len2 = sizeof(sensor__size_##w##_##h##_framerate_##f)/sizeof(sensor__size_##w##_##h##_framerate_##f[0]); \
				}else

#define SENSOR_SETTING_SUPPORT_INTERNAL(w, h, f) SENSOR_SETTING_SUPPORT_INTERNAL1(w, h, f)

#define SENSOR_SETTING_SUPPORT_SIZE_FRM(w, h, fps) SENSOR_SETTING_SUPPORT_INTERNAL(w, h, fps)

#define SENSOR_SETTING_SUPPORT_START \
			{ \
					SENSOR_SETTING_UTYPE *cfg1 = NULL; \
					SENSOR_SETTING_UTYPE *cfg2 = NULL; \
					u32 len1=0, len2=0, i;

#define SENSOR_SETTING_SUPPORT_END \
					{ \
							return -1; \
					} \
					for(i=0; i<len1; i++){ \
						libsccb_wr(cfg->sccb_cfg, cfg1[2*i], cfg1[2*i+1]);\
					} \
					for(i=0; i<len2; i++){ \
						libsccb_wr(cfg->sccb_cfg, cfg2[2*i], cfg2[2*i+1]);\
					} \
					debug_printf("sensor %d set to %d*%d,%dfps\n", SENSOR_NAME_INTERNAL, (cfg->cur_video_size >> 16), cfg->cur_video_size & 0xffff, cfg->cur_frame_rate ); \
			}
