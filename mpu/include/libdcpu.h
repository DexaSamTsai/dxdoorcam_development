#ifndef __LIBDCPU_H__
#define __LIBDCPU_H__

#include "dcpu_common.h"
#include "mpu_cmd.h"
#include "libmbx.h"

/**
 * @defgroup lib_dualBA
 * @ingroup lib_engine
 * @brief Library for communication between the two CPUs.
 * @details <p></p> 
 	<h2> 1. Introduction </h2>
	<ul>
 	<li> There are two CPUs in ov788, the master(ARM) and the slave(BA22).Audio,video and ISP will run in BA22, OS(linux or NEWOS) will run on ARM.
 	<li> There are three layers defined for dual CPU communication, this library is the layer2 in the the dual CPUs communication framework.
		<ul>
 		<li>	L1: layer 1, the hardware link layer, the bottom level driver for mailbox
 		<li>	L2: layer 2, the middle common layer,  provide API for Layer 3.
 		<li>	L3: layer 3, app layer, the application specific layer.(eg: audio,video,ISP).
		</ul>
	</ul>
	<h2> 2. The layer1 </h2>
	<ul>
	<li> 1) Mailbox
		<ul>
		<li>Mailbox is a hardware module in OV788 for communication between BA22 and ARM.
		<li>ARM can send data to BA22 , and generate an interrupt to BA22, BA22 can read the data  to clear the interrupt.
		<li>BA22 also can send data to ARM and generate an interrupt to ARM, ARM read the data to clear the interrupt.     
		</ul>
	<li> 2) Shared Memory
		<ul>
		<li>The shared common memory can be accessed by both BA22 and ARM. So it can be used for data exchange between the two CPUs. The shared memory should be uncacheable(set the MSB bit of the data address as 1).		
		</ul>
	<li> 3) Interface
		<ul>
		<li> int libsec_start(void): Enable BA22 clock, set BA22 base address and start BA22
		<li> int libsec_stop(void)a: Stop BA22
		<li> void libmbx_m2s(u32 data): ARM send data to BA22,  trigger an interrupt to BA22
		<li> u32 libmbx_m_read(void):   
	        ARM read the data sent by BA22 using libmbx_s2m(), and clear the 	interrupt triggered by BA22
		<li> void libmbx_s2m(u32 data): 
		    BA22 send data to ARM,  trigger an interrupt to ARM
		<li> u32 libmbx_s_read(void) :   
		    BA22 read the data sent by ARM using  libmbx_m2s(), and clear the 	interrupt triggered by ARM
		</ul>
	</ul>
	<h2> 3. The layer2 </h2>
		<ul>
		<li> This layer implement the command and signal protocol between ARM and BA22,  provide APIs for layer3 audio and video application . Video or audio main functions runs in ARM scheduled by RTOS or Linux, it send command to BA22 to initialize, start, stop audio/video engine run in BA22; audio/video engine in BA22 also can send signal to ARM to share some information. 
		</ul>
	<h2> 4. The layer3 </h2>
		<ul>
		<li>	The layer3 is the implementation of  application, like audio and video encoder. There should be two parts of code for an application, the BA22 part and ARM part.
  		<li>	The application entry and flow control are in ARM part , it calls libmpu_m_sndcmdb() to control BA22, BA22 handle the command, then put result in mpu_cmd.cmd_ret.
 		<li>	BA22 part code should be located in OV788_fw/engine/ directory.   For each application, a t_ba2app_cfg type structure variable must be defined; its app_cmdhandler and app_process must be implemented, however its app_inthandler is not necessary, if no interrupt need be handled. Below is an example for NEWAPPNAME:  
			\code
			typedef enum{
				BA2_APP_SAMPLE,
				BA2_APP_APLAY, 
				BA2_APP_AREC, 
				....
				BA2_APP_NEWAPPNAME
			}E_BA2_APP;

			__ba2app_cfg t_ba2app_cfg g_ba2app_newappname_cfg = {
				.apptype = BA2_APP_NEWAPPNAME,
				.app_cmdhandler = newappname_handle_cmd,
				.app_inthandler = newappname_int_handler,
				.app_process = newappname_process,
			};
			\endcode
	 	<li>	For new application that need run in BA22,its new commands(ARM->BA22) should be added to enumerator E_DCPU_CMD, and its new signals(BA22->ARM) should be added to  enumerator E_DCPU_SIG.
			\code
			typedef enum{
				DCPUCMD_APLAY_GCFGADDR,
				DCPUCMD_APLAY_INIT,  
				....
				DCPUCMD_NEWAPPNAME_A,
				DCPUCMD_NEWAPPNAME_B,
			}E_DCPU_CMD;

			typedef enum{
				DCPUSIG_SAMPLE,  ///<signal for example
				....
				DCPUSIG_NEWAPPNAME_1,
				DCPUSIG_NEWAPPNAME_2,
			}E_DCPU_SIG;
			\endcode
 		<li>	For xxx_handle_cmd(), it should defined as below(need a default branch to return 0):
			\code
			static u32 newappname_handle_cmd(u32 cmdID,u32 param_addr)
			{
				u32 ret;
				switch(cmdID)
				{
				case DCPUCMD_NEWAPPNAME_A:
					ret = ...
					return ret;

				case DCPUCMD_NEWAPPNAME_B:
					ret = ...
					return ret;

				default:
					return 0;
				}
			}

			\endcode
		</ul>
 * @{
 */






/**
 * @brief   BA22 handle command from ARM
 * @details When ARM send command to BA22 with a command id, BA22 will receive a mailbox interrupt. In the mailbox interrupt handler,this API should be called
 * @param void
 * @return void
 */
void libmbx_irq_handler(void * arg);

/**
 * @REAL_FORMAT void libmpu_s_sndsig(u32 idx, u32 arg, u32 timeout)
 * @brief   BA22 send signal to ARM
 * @details BA22 use it to send a signal to ARM in blocking mode until timeout
 * @param idx: signal ID 
 * @param arg: parameter address used for the command if needed 
 * @param timeout: in tick,0 for wait all the time
 * @return void
 */
void libmpu_s_sndsig(u32 idx, u32 arg, u32 timeout);



void libmpu_set_event(E_MPU_EVT evt);

void libmpu_clear_event(E_MPU_EVT evt);

/** @} */ //end lib_dualba
#endif ///__LIBDCPU_H__

