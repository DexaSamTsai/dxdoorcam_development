/****************************************************************************
 * 
 * Copyright (c) 2013 OmniVision Technologies Inc. 
 * The material in this file is subject to copyright. It may not
 * be used, copied or transferred by any means without the prior written
 * approval of OmniVision Technologies, Inc.
 *
 * OMNIVISION TECHNOLOGIES, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO
 * THIS SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS, IN NO EVENT SHALL OMNIVISION TECHNOLOGIES, INC. BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
 * RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
 * CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 * CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * $Author  : enigma_shen@ovt.com
 * $Data    : 
 * $Version : V5122 
 ****************************************************************************/
/*
v0.1	initial version
v0.2	AF interface defined as V5212
*/

#ifndef _HWFWIO_H_
#define _HWFWIO_H_


// DMA read from RISC-RO at stat done, 1 instruction 
typedef struct tagBlankingReadLensOnline	// 4 bytes (0x0008_B000--0x0008_B003)->[0x33100~0x33103]
{
	short nRa;
	short nBa;
}TBlankingReadLensOnline;

typedef struct tagBlankingReadCurveAWB			//	92 bytes(0x0008_B004--0x0008_B05F)->[0x33104~0x3315f]
{
	u32 pSumRGB[3][2];   // 0x8b004, 0x33104
	unsigned int nCount;  // 0x8b01c, 0x3311c
	unsigned int pWindowsSum[4][4];  // 0x8b020, 0x33120
}TBlankingReadCurveAWB;

typedef struct tagBlankingReadAEC				//	4 bytes(0x0008_B060--0x0008_B063)->[0x33160~0x33163]
{
	unsigned int pMeanY;			
}TBlankingReadAEC;

typedef struct tagBlankingReadRGBHStretch		//	4 bytes(0x0008_B064--0x0008_B067)->[0x33164~0x33167]
{
	unsigned short nLowLevel;		// 0x8b064, 0x33164
	unsigned short nHighLevel;	    // 0x8b006, 0X33166
}TBlankingReadRGBHStretch;

typedef struct tagBlankingReadHistStat				// 4 bytes(0x0008_B068--0x0008_B06B)->[0x33168~0x3316b]
{
	unsigned int nILatch;			
}TBlankingReadHistStat;

typedef struct tagBlankingReadLocalBoost			//	196 bytes(0x0008_B06C--0x0008_B12F)->[0x3316c~0x3322f]
{
	unsigned int pLocalSumYBuf[48];   // 0x8b06c, 0x3316c
	unsigned int nLocalNum;	          // 0x8b12c, 0x3322c
}TBlankingReadLocalBoost;

typedef struct tagBlankingReadToneMapping		//	20 bytes(0x0008_B130--0x0008_B143)->[0x33230~0x33243]
{
	unsigned char pIList[16];		// 0x8b130, 0x33230
	unsigned int nJ;				// 0x8b140, 0x33240
}TBlankingReadToneMapping;

typedef struct tagBlankingReadCombine			//	16 bytes(0x0008_B144--0x0008_B153)->[0x33244~0x33253]
{
	u32 nSumE[2];					// 0x8b144, 0x33244
	unsigned int nSumW;			    // 0x8b14c, 0x3324c
	unsigned int nMinNum;		    // 0x8b150, 0x33250
}TBlankingReadCombine;

typedef struct tagBlankingReadControl			//	340 bytes
{
	TBlankingReadLensOnline tLensOnline;		// 4 bytes
	TBlankingReadCurveAWB tCurveAWB;  		// 92 bytes
	TBlankingReadAEC tAEC;							// 4 bytes
	TBlankingReadRGBHStretch tStretch;			//	4 bytes
	TBlankingReadHistStat tHistStat;				// 4 bytes 
	TBlankingReadLocalBoost tLocalBoost;			//	196 bytes
	TBlankingReadToneMapping tToneMapping;	//	20 bytes
	TBlankingReadCombine tCombine;				//	16 bytes
}TBlankingReadControl;
//

typedef struct tagReadPDAF
{
	unsigned char pConfidence[25]; // 0x33d00
	char pShiftValue[25];
	unsigned short pSum[15];
}TReadPDAFControl;


// DMA read from SRAM at AECDone, 2 instructions
//*** needs to clarify whether Y/R/G/B or Y/B/G/R ***//
typedef struct tagReadHistControl   // 2560 bytes,  1024 bytes in(0x0008_0800--0x0008_11FF)-> [0x33300 ~ 0x336ff]
{
	unsigned int pHistY[256]; // 1024 bytes [0x80800~0x80bff]
//	unsigned int pHistB[128]; // 512 bytes [0x80c00~0x80dff]
//	unsigned int pHistG[128]; // 512 bytes [0x80e00~0x80fff]
//	unsigned int pHistR[128]; // 512 bytes [0x81000~0x811ff]
}TReadHistControl;
/// ********************************************** ///

typedef struct tagReadMeanControl  // 1024 bytes(0x0008_1200--0x0008_15FF) -> [0x33300 ~ 0x336ff]
{
	unsigned int pMean[256];// [23:16] b, [15:8] g, [7:0] r
}TReadMeanControl;


// DMA write in FrameProcess, 2 instructions
// 1. DMA write RISC-RW manually
typedef struct tagFrameWriteLensOnline			// 4 bytes [0x33700~0x33703]->(0x0008_A000--0x0008A003)
{
	short nT0;
	short nT1;
}TFrameWriteLensOnline;

typedef struct tagFrameWriteLENC				// 4 Bytes [0x33704~0x33707]->(0x0008_A004--0x0008_A007)
{
	unsigned int nQ;
}TFrameWriteLENC;

typedef struct tagFrameWriteAWB					// 16 Bytes [0x33708~0x33717]->(0x0008_A008--0x0008_A017)
{	
	unsigned short pCFAGain[4];
	short pCFAOffset[4];
}TFrameWriteAWB;

typedef struct tagFrameWriteCurveAWB			// 144 Bytes [0x33718~0x337a7]->(0x0008_A018--0x0008_A0A7)
{
    unsigned short nXScale;
	unsigned short nYScale;
	short nXOffset;
	short nYOffset;
	unsigned int nScaleBits;
	short pInverseGain[2];
	unsigned char pMap[128];		
}TFrameWriteCurveAWB;

typedef struct tagFrameWriteAECAGC				//	4 Bytes	[0x337a8~0x337ab]->(0x0008_A0A8--0x0008_A0AB)
{
	unsigned int nRealGain;
}TFrameWriteAECAGC;

typedef struct tagFrameWriteColorMatrix			// 20 Bytes	[0x337ac~0x337bf]->(0x0008_A0AC--0x0008_A0BF)
{
	unsigned short pColorMatrix[10]; // [8] is reserved, [9] is the 9th value
}TFrameWriteColorMatrix;

typedef struct tagFrameWriteStretch				// 4 Bytes [0x337c0~0x337c3]->(0x0008_A0C0--0x0008_A0C3)
{
    unsigned short nStretchGain;
	unsigned short nApplyLowLevel;	
}TFrameWriteStretch;

typedef struct tagFrameWriteLogControl			// 128 bytes [0x337c4~0x33843]->(0x0008_A0C4--0x0008_A143)
{
	unsigned char bDarkBoostEnable; 
	unsigned char bHighLightDepressEnable;
	unsigned short pLocalGainBuf[63];     
}TFrameWriteLogControl;

typedef struct tagFrameWriteLogProcess			// 20 Bytes [0x33844~0x33857]->(0x0008_A144--0x0008_A157)
{
    unsigned short nLogYMax;
	unsigned short nLogXMax;
	unsigned short bRGBGammaEnable;
	unsigned short nGlobalLogHDRGain;
	unsigned short nGlobalLogHDRGamma;
	unsigned short nInverseRGBGamma;
    unsigned short nLogMaxYUVGammaGain;
	unsigned short nLogMaxRGBGammaGain;
    unsigned int nOutputRange;
}TFrameWriteLogProcess;

typedef struct tagFrameWriteNormalize			// 8 Bytes [0x33858~0x3385f]->(0x0008_A158--0x0008_A15F)
{
	unsigned int nHDROffset;
	unsigned int nNormalizeGain;
}TFrameWriteNormalize;

typedef struct tagFrameWriteToneMapping		// 64 bytes [0x33860~0x3389f]->(0x0008_A160--0x0008_A19F)
{
	unsigned short pCurveList[16];
	unsigned short pCCList[16];
}TFrameWriteToneMapping;

typedef struct tagFrameWriteRGBCurve			// 88 bytes [0x338a0~0x338f7]->(0x0008_A1A0---0x0008_A1F7)
{
	unsigned short pCurveYList[33]; 
	unsigned short pHFreqGain[9];
	unsigned int nSlope;     
}TFrameWriteRGBCurve;

typedef struct tagFrameWriteCombine				// 8 Bytes [0x338f8~0x338ff]->(0x0008_A1F8--0x0008_A1FF)
{
	short nGlobalLogRatio;
	short nLogE;
	unsigned int bCompensateErrorEnable;	
}TFrameWriteCombine;




typedef struct tagFrameWriteControl				//	512 bytes [0x33700~0x338ff]->(0x0008_A000--0x0008A1FF)
{
	TFrameWriteLensOnline tLensOnline;	// 4 bytes
	TFrameWriteLENC tLENC;				// 4 bytes
	TFrameWriteAWB tAWB; 				// 16 bytes
	TFrameWriteCurveAWB tCurveAWB;		// 144 bytes
	TFrameWriteAECAGC tAECAGC;			// 4 bytes
	TFrameWriteColorMatrix tColorMatrix;// 20 bytes
	TFrameWriteStretch tStretch;		// 4 bytes
	TFrameWriteLogControl tLogControl;		// 128 bytes
	TFrameWriteLogProcess tLogProcess;		// 20 bytes
	TFrameWriteNormalize tNormalize;	// 8 bytes
	TFrameWriteToneMapping tToneMapping;// 64 bytes
	TFrameWriteRGBCurve tRGBCurve;		// 88 bytes
	TFrameWriteCombine tCombine;		// 8 bytes
}TFrameWriteControl;
//

// 2. DMA write SRAM manually
typedef struct tagWriteLENCProf			//  768 bytes [0x33a00~0x33cff]->(0x0008_0000--0x0008_02ff)
{
	unsigned int pCP[192];
}TWriteLENCControl;

#endif
