#ifndef _FRAMELIST_H_
#define _FRAMELIST_H_

/* FRAMELIST是一个FRAME的列表。
   在FRAMELIST里有两个FRAME结构的链表，一个为空的未被使用的FRAME结构，一个为按照添加时间排序的有效FRAME链表(有效FRAME指的是还在使用的FRAME).
   常见的操作有：
   1. 从FRAMELIST获取一个空的FRAME结构(FRAME状态为idle）
   2. 添加一个FRAME到有效FRAME列表（这个FRAME必须从这个FRAMELIST获取), FRAME状态自动改为READY
   3. 从列表获取第一个READY的FRAME(按照添加时间依次)
   4. 释放一个READY的FRAME. 释放FRAME不一定要按照需要按照获取的次序。所以释放一个FRAME的时候，列表可能只是把他标记为DONE，不一定真的从资源中释放。列表只会释放有效FRAME链表头上的所有状态为DONE的FRAME。
   */

typedef struct s_FRAMELIST
{
	u32 num;	///< total list buffer number
	t_pool * pool;
	u8 * poolbuf;	///< linklist pool address
	u32 cnt;///< number of frames in current frame list

	volatile FRAME * volatile frame_head;
	volatile FRAME * volatile frame_tail;
}FRAMELIST;

#define FRAMELIST_DECLARE(name, listnum) \
static unsigned char  __attribute__ ((aligned(4))) g_framelist_##name##_poolbuf[POOL_OVERHEAD_SIZE + sizeof(FRAME) * listnum + 4]; \
static FRAMELIST name = \
{ \
	.poolbuf = g_framelist_##name##_poolbuf, \
	.num = listnum, \
};

/* 初始化,reset */
int framelist_init(FRAMELIST * list);

/* 获取未被使用的FRAME数目 */
u32 framelist_remain(FRAMELIST * list);

/* 获取总的FRAME结构数目 */
u32 framelist_num(FRAMELIST * list);

/* 获取当前有效FRAME数目 */
u32 framelist_cnt(FRAMELIST * list);

/* 获取一个空的FRAME结构 */
FRAME * framelist_alloc(FRAMELIST * list);

/* 添加一个FRAME到有效FRAME列表，不关中断, 因为这个函数很可能是在中断里被调用的 */
int framelist_add(FRAMELIST * list, FRAME * frm);

/* 获取第一个READY的FRAME */
FRAME * framelist_get(FRAMELIST * list);

/* 释放一个READY的FRAME，如果可以真实释放的话，调用free_resource()释放硬件资源。
   注意一个framelist_free()可能调用多次free_resource()。
   注意free_resource()被调用的时候是关中断的。
 */
int framelist_free(FRAMELIST * list, FRAME * frm, void (*free_resource)(void * resource_arg, FRAME * frm), void * resource_arg);

#endif
