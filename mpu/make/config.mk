#########################################################################

ifndef MPU_CROSS_COMPILE
MPU_CROSS_COMPILE = ba-elf-
endif

ifdef V
Q:=
else
Q:=@
endif

#########################################################################

SHELL	:= $(shell if [ -x "$$BASH" ]; then echo $$BASH; \
		    else if [ -x /bin/bash ]; then echo /bin/bash; \
		    else echo sh; fi ; fi)

HOSTCC		= gcc
HOSTCFLAGS	= -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer

MAKE = make

TOPDIR := $(shell if [ "$$PWD" != "" ]; then echo $$PWD; else pwd; fi)

CODEDIR = $(TOPDIR)/..

BLDDIR = $(CODEDIR)/build

MPUPREFIX = mpu/
#########################################################################

AS	= $(MPU_CROSS_COMPILE)as
LD	= $(MPU_CROSS_COMPILE)ld
CC	= $(MPU_CROSS_COMPILE)gcc
AR	= $(MPU_CROSS_COMPILE)ar
NM	= $(MPU_CROSS_COMPILE)nm
STRIP	= $(MPU_CROSS_COMPILE)strip
OC      = $(MPU_CROSS_COMPILE)objcopy
OD      = $(MPU_CROSS_COMPILE)objdump
RANLIB	= $(MPU_CROSS_COMPILE)ranlib

CFLAGS += -I$(CODEDIR)/include
CFLAGS += -I$(CODEDIR)/export_include

ifeq ($(COMPILE_ADD_UNDERSCORE),1)
CFLAGS += -DCOMPILE_ADD_UNDERSCORE
endif

include $(TOPDIR)/$(CHIP)/mk.chip

ifdef BUILD_NO_ROM
CFLAGS += -DBUILD_NO_ROM
endif

ifdef FOR_FPGA
CFLAGS += -DFOR_FPGA
endif

#########################################################################
