#!/bin/bash
if [ "$FOR_SDKRELEASE" != "1" ] ; then
	MPU_CROSS_COMPILE=/opt/ba-elf-new/bin/ba-elf-
fi
export MPU_CROSS_COMPILE

unset PRJ
unset APP
unset OBJDIR

export MPUAPP=full

LIBSDIR=../${LIBSDIR}/mpu
export LIBSDIR

usage ()
{
	echo "=======firmware build script======="
	echo "APP: APP is one firmware binary of ram/xxx/ "
	echo "PRJ: PRJ is group of APPs with specific Defines in prj/$chip/xxx/yyy.config"
	echo "1. ./autobuild.sh all #build all APPs in all PRJ"
	echo "2. ./autobuild.sh APP #build APP in default PRJ"
	echo "3. ./autobuild.sh PRJ #build all APPs in PRJ"
	echo "4. ./autobuild.sh PRJ APP #build APP in PRJ"
	exit 1
}

if [ $# -eq 0 ] ; then
	usage
fi

if [ -z $chip ] ; then
	echo "please run ./autobuild.sh"
	exit 1
fi

#check gcc version
gccv=`${MPU_CROSS_COMPILE}gcc --version | grep GCC | sed -e "s/ 2014.*$//g" | sed -e "s/^.* //g" | sed -e "s/\.//g"`
if [ -z $gccv ] ; then
	echo "${MPU_CROSS_COMPILE}gcc not found"
	exit 1
fi
gccfile=`which ${MPU_CROSS_COMPILE}gcc`
gccdir=`dirname $gccfile`
COMPILE_ADD_UNDERSCORE=0
if [ $gccv -gt 440 ] ; then
	COMPILE_ADD_UNDERSCORE=1
	link_libgccdir="-L$gccdir/../lib/gcc/ba-elf/4.7.3/ -L$gccdir/../ba-elf/lib/ -lgcc_us -lm_us"
else
	echo "[ERROR] This SDK cannot compiled by old toolchain, please upgrade toolchain"
	exit 1
#	link_libgccdir="-L$gccdir/../lib/gcc/ba-elf/4.4.0/ba2/ -L$gccdir/../ba-elf/lib/ba2"
fi
export COMPILE_ADD_UNDERSCORE
export link_libgccdir

if [ ! -e build ] ; then
	mkdir build
fi

build_prj_app ()
{
	if [ ! -z $LDFILE ] ; then
		ldf=$LDFILE
	fi

	mkf="Makefile.all"

	CHIP=$1 PRJ=$2 APP=$3 LDFILE=$ldf make all -f $mkf -C make/ --no-print-directory || exit 1
}

# clean
build_clean=0
if [ `echo "$1" | grep clean` ] ; then
	build_clean=1
fi
if [ ! -z "$2" ] ; then
	if [ `echo "$2" | grep clean` ] ; then
		build_clean=1
	fi
fi
if [ $build_clean -ne 0 ] ; then
	CHIP=$chip make clean -f Makefile.clean -C make/ --no-print-directory || exit 1
	exit 0
fi

# check build chip revision
cur_rev="revA"
LIB_POSTFIX=""
if [ ! -z $CHIPV ] ; then
	if [ "$CHIPV" == "ECO1" ] ; then
		cur_rev="revB"
		LIB_POSTFIX="_ECO1"
	fi
else
	CHIPV=""
fi
export LIB_POSTFIX
if [ -e build/chiprev.txt ] ; then
	pre_rev=`cat build/chiprev.txt`
	if [ "$pre_rev" != "$cur_rev" ] ; then
		echo "build with $cur_rev conflict with last $pre_rev build"
		echo "'rm -rf build/ mpu/build/' and try again"
		exit
	fi
else
	echo "$cur_rev" > build/chiprev.txt
fi

# generate version
if [ -z $svn_rev] ; then
	fw_version=`svn info . | grep "Last Changed Rev" | sed -e "s/^.*: //g"`
fi
if [[ "" == $fw_version ]] ; then
	if [ ! -e include/version.h ] ; then
		echo "#define FW_VERSION 0" > include/version.h
	fi
	if [ ! -e build/v.txt ] ; then
		echo "0" > build/v.txt
	fi
else
	echo "#define FW_VERSION $fw_version" > include/version.new.h
	changed=`diff include/version.h include/version.new.h 2>&1`
	if [ ! -z "$changed" ] ; then
		mv include/version.new.h include/version.h
	else
		rm -f include/version.new.h
	fi
	echo "$fw_version" > build/v.txt
fi
echo "  [FW_VERSION] $fw_version"

# build all share libraries
echo "  [RUNLIB] mpu/share/ start"
RUNLIB=1 LIBNEEDLINK=0 CHIP=$chip OBJDIR=share/ make all -f Makefile.dir -C make/ --no-print-directory || exit 1
echo "  [RUNLIB] mpu/share/ done"

#build application
build_prj_app $chip $1 $2

exit 0

