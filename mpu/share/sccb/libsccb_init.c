#include "includes.h"

#ifdef FOR_FPGA
#define SCCB_DEFAULT_TIMEOUT    (0x40000)
#else
#define SCCB_DEFAULT_TIMEOUT    (0x400000)
#endif
s32 libsccb_init(t_libsccb_cfg * cfg)
{
	//TODO: if gpio simulated, libgpio_config()
	//TODO: calc the dividor
	if(cfg->clk_in == 0){
#ifdef FOR_FPGA
		cfg->clk_in = 48000000;
#endif
extern unsigned int clockget_bus(void);
		cfg->clk_in = clockget_bus();
	}
	if(cfg->timeout == 0){
		cfg->timeout = SCCB_DEFAULT_TIMEOUT;
	}
#ifndef BUILD_FOR_BA
	pthread_mutex_init(&cfg->sccb_mutex, NULL);
	pthread_cond_init(&cfg->sccb_cond, NULL);
#endif
	u8 num = cfg->sccb_num;
	u32 irq_num;
	switch(num)
	{
		case 0:
			SC_RESET0_SET(SCCBM0);
			SC_RRESET0_SET(SCCBM0);
			SC_CLK0_EN(SCCBM0);
			SC_RESET0_RELEASE(SCCBM0);
			SC_RRESET0_RELEASE(SCCBM0);
			WriteReg32(REG_SC_EN, ReadReg32(REG_SC_EN) | BIT20 | BIT21);
			irq_num = IRQ_BIT_SCCBM0;
			break;
		case 1:
			SC_CLK0_EN(SCCBM1);
			SC_RESET0_RELEASE(SCCBM1);
			SC_RRESET0_RELEASE(SCCBM1);
			SC_PIN1_EN(UARTM_RX);
			SC_PIN1_EN(UARTM_TX);
			WriteReg32(REG_SC_CTRL2, ReadReg32(REG_SC_CTRL2) | BIT0); //pin shared with uart
			irq_num = IRQ_BIT_SCCBM1;
			break;
		case 2:
			SC_CLK0_EN(SCCBM2);
			SC_RESET2_RELEASE(SCCBM2);
			SC_RRESET0_RELEASE(SCCBM2);
			SC_PIN1_EN(UARTM_CTS);
			SC_PIN1_EN(UARTM_RTS);
			WriteReg32(REG_SC_CTRL2, ReadReg32(REG_SC_CTRL2) | BIT1); //pin shared with uart
			irq_num = IRQ_BIT_SCCBM2;
			break;
		case 3:
			SC_CLK0_EN(SCCBM3);
			SC_RESET2_RELEASE(SCCBM3);
			SC_RRESET0_RELEASE(SCCBM3);
			WriteReg32(REG_SC_EN, ReadReg32(REG_SC_EN) | BIT22 | BIT23);
			irq_num = IRQ_BIT_SCCBM3;
			break;
		default:
			return -1;
	}
#ifdef BUILD_FOR_BA
	irq_request(irq_num, libsccbgrp_irq_handler, cfg);
#else
	irq_request(irq_num, libsccbgrp_irq_handler, "SCCB", cfg);
#endif
	SC_RESET0_RELEASE(MBUS);
	
	sccb_set(cfg);

	return 0;
}
