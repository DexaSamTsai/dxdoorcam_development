/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/

#include "includes.h"

void vstream_free_frame(t_dpm* dpm, FRAME* frm){
	if(dpm->type == DPM_TYPE_IMG){ 
		libimg_release_frame((t_img_cfg *)dpm, (FRAME *)frm);
	}else if(dpm->type == DPM_TYPE_VE){ 
		libvenc_stream_free_frame((ve_enc_t *)dpm, (FRAME *)frm);
	}else if(dpm->type == DPM_TYPE_FB){
		fb_stream_free_frame((t_dpm_fb *)dpm, (FRAME *)frm);	
	}else if(dpm->type == DPM_TYPE_OVF){
		ovf_stream_free_frame((t_dpm_ovf *)dpm, (FRAME *)frm);
	}
	return;
}

FRAME* vstream_get_frame(t_dpm* dpm){
	if(dpm->type == DPM_TYPE_IMG){
		return (FRAME*)libimg_get_frame((t_img_cfg *)dpm);
	}else if(dpm->type == DPM_TYPE_VE){ 
		return (FRAME*)libvenc_stream_get_frame((ve_enc_t *)dpm);
	}else if(dpm->type == DPM_TYPE_FB){
		return (FRAME*)fb_get_frame((t_dpm_fb *)dpm);
	}else if(dpm->type == DPM_TYPE_OVF){
		return (FRAME*)ovf_get_frame((t_dpm_ovf *)dpm);
	}
	return NULL;
}

u32 vstream_handle_cmd(u32 cmd, u32 arg)
{
	int vs_id = (cmd >> 12) & 0xf;
	cmd &= 0xffff0fff;
	if(vs_id >= VS_MAX_STREAMS){
		return -1;
	}
	t_virtual_vs* virtual_vs = g_libdatapath_cfg->vtvs_map[vs_id];
	if(virtual_vs != NULL)
	{
		u32 ret = VIRTUAL_VS_NOT_DONE;	
		switch(cmd)
		{
		case MPUCMD_VS_INIT:
			ret = virtual_vs_init(virtual_vs);
			break;
		case MPUCMD_VS_START:
			ret = virtual_vs_start(virtual_vs);
			break;
		case MPUCMD_VS_STOP:
			ret = virtual_vs_stop(virtual_vs);
			break;
		case MPUCMD_VS_EXIT:
			ret = virtual_vs_exit(virtual_vs);	
			break;
		case MPUCMD_VS_GETFRM:
			return (u32)virtual_vs_get_frm(virtual_vs);
		case MPUCMD_VS_RELFRM:
			virtual_vs_free_frm(virtual_vs, (FRAME*)arg);
			return 0;
		default:	
			break;
		}
		if(ret != VIRTUAL_VS_NOT_DONE){
			return ret;	
		}
		if(virtual_vs->vfdist != NULL){
			vs_id = virtual_vs->vfdist->dpm->id;	
		}else{
			return -2;	
		}
	}

	t_dpm* dpm = g_libdatapath_cfg->vs_map[vs_id];

	if(dpm == NULL){
		if((g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE) && ((g_libdatapath_cfg->fastboot_stage == FAST_STAGE_IDLE)||(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_FPS1DONE))){
			if(g_libdatapath_cfg->fastboot_vs_handle_cmd){
				return g_libdatapath_cfg->fastboot_vs_handle_cmd(vs_id, cmd, arg);
			}
		}
		return -1;
	}

	if(dpm->type == DPM_TYPE_IMG){
		t_dpm_img * dpm_img = (t_dpm_img *)dpm;
		if(dpm_img->handle_cmd){
			u32 handled = 0;
			u32 ret = dpm_img->handle_cmd(dpm_img, cmd, arg, &handled);
			if(handled){
				return ret;
			}
		}
	}else if(dpm->type == DPM_TYPE_VE){
		t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
		if(dpm_ve->handle_cmd){
			u32 handled = 0;
			u32 ret = dpm_ve->handle_cmd(dpm_ve, cmd, arg, &handled);
			if(handled){
				return ret;
			}
		}
	}else if(dpm->type == DPM_TYPE_FB){
		t_dpm_fb * dpm_fb = (t_dpm_fb *)dpm;
		if(dpm_fb->handle_cmd){
			u32 handled = 0;
			u32 ret = dpm_fb->handle_cmd(dpm_fb, cmd, arg, &handled);
			if(handled){
				return ret;
			}
		}
	}else if(dpm->type == DPM_TYPE_OVF){
		t_dpm_ovf * dpm_ovf = (t_dpm_ovf *)dpm;
		if(dpm_ovf->handle_cmd){
			u32 handled = 0;
			u32 ret = dpm_ovf->handle_cmd(dpm_ovf, cmd, arg, &handled);
			if(handled){
				return ret;
			}
		}
	}


	if(cmd >= (MPUCMD_TYPE_VS | VSIOC_OSD_START) && cmd <= (MPUCMD_TYPE_VS | VSIOC_OSD_END)){
		t_dpm * dpm_osd = dpm;
		u8 osd_id = 0;
		while(dpm_osd){
			if(dpm_osd->type == DPM_TYPE_OSD4L){
				osd_id = dpm_osd->id;
				break;
			}
			if(dpm_osd->type == DPM_TYPE_OSDRLC){
				osd_id = dpm_osd->id;
				break;
			}
			dpm_osd = dpm_osd->in;
		}
		switch(cmd){
			case (MPUCMD_TYPE_VS | VSIOC_OSD4L_PARAGET):
				return osd4l_get(arg, osd_id);
			case (MPUCMD_TYPE_VS | VSIOC_OSD4L_UPDATE):
				return osd4l_update((t_osd4l_win_cfg *)(arg|BIT31), osd_id);
			case (MPUCMD_TYPE_VS | VSIOC_OSD_ENABLE_SET):
				if(dpm_osd->type == DPM_TYPE_OSD4L){
					return osd4l_enable(arg);
				}						  
				else if(dpm_osd->type == DPM_TYPE_OSDRLC){
					return osdrlc_enable(arg);
				}				
				return 0;
			case (MPUCMD_TYPE_VS | VSIOC_OSD_DISABLE_SET):
				if(dpm_osd->type == DPM_TYPE_OSD4L){
					return osd4l_disable(arg);
				}
				else if(dpm_osd->type == DPM_TYPE_OSDRLC){
					return osdrlc_disable(arg);
				}
				return 0;
			case (MPUCMD_TYPE_VS | VSIOC_OSDRLC_BUFGET):
				return osdrlc_bufget(arg, osd_id);
			case (MPUCMD_TYPE_VS | VSIOC_OSDRLC_UPDATE):
				return osdrlc_update((t_osdrlc_win_cfg *)(arg|BIT31), osd_id);
			default:
				return 1;
		}
	}
	if((cmd >= (MPUCMD_TYPE_VS | VSIOC_ISP_START) && cmd <= (MPUCMD_TYPE_VS | VSIOC_ISP_END))
			||(cmd == (MPUCMD_TYPE_VS | VSIOC_EXPOSURE_SET))){
		t_dpm * dpm_ispidc = dpm;
		while(dpm_ispidc){
			if(dpm_ispidc->type == DPM_TYPE_ISPIDC){
				break;	
			}
			dpm_ispidc = dpm_ispidc->in;
		}
		if(dpm_ispidc == NULL)
		{
			return -1;
		}
		u8 isp_id = dpm_ispidc->id;
		switch(cmd){
			case (MPUCMD_TYPE_VS | VSIOC_EXPOSURE_SET):
				return  isp_set_ae_target((t_dpm_ispidc*)dpm_ispidc,arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_SHARPNESS):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_SHARPNESS | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_AEC_MANUAL_EXP):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_EXP | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_AEC_MANUAL_GAIN):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_GAIN | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_SDE):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_SDE | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_DAYNIGHT):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_DAYNIGHT | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_WDR):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_WDR | (isp_id << 12), arg);
			case (MPUCMD_TYPE_VS | VSIOC_SET_MCTF):
				return  libisp_handle_cmd(MPUCMD_ISP_SET_MCTF | (isp_id << 12), arg);
			default:
				return 1;
		}
	}
	switch(cmd)
	{
	case MPUCMD_VS_INIT:
		return 0;

	case MPUCMD_VS_START:
		return libvs_enable(dpm);

	case MPUCMD_VS_STOP:
		return libvs_disable(dpm);

	case MPUCMD_VS_EXIT:
		return 0;

	case MPUCMD_VS_GETFRM:
		return (u32)vstream_get_frame(dpm);
	case MPUCMD_VS_RELFRM:
		vstream_free_frame(dpm, (FRAME *)arg);
		return 0;
	case MPUCMD_VS_GET_RESOLUTION:
		return ((dpm->output_width<<16)|(dpm->output_height));
	case (MPUCMD_TYPE_VS | VSIOC_VIDEOMIRROR_SET):
		return dpm_sensor_effect(dpm, SNR_EFFECT_MIRROR, arg);
	case (MPUCMD_TYPE_VS | VSIOC_VIDEOFLIP_SET):
		return dpm_sensor_effect(dpm, SNR_EFFECT_FLIP, arg);;
	case (MPUCMD_TYPE_VS | VSIOC_VIDEOMIRRORFLIP_SET):
		return dpm_sensor_effect(dpm, SNR_EFFECT_FLIP_MIR, arg);			
	case (MPUCMD_TYPE_VS | VSIOC_CONTRAST_SET):
		return  isp_set_contrast(arg);
	case (MPUCMD_TYPE_VS | VSIOC_SATURATION_SET):
		return  isp_set_saturation(arg);
	case (MPUCMD_TYPE_VS | VSIOC_ANTIFLICK_SET):
		//return dpm_sensor_effect(dpm, SNR_EFFECT_LIGHT_FREQ, arg);
		return isp_set_lightfreq(arg);
	case (MPUCMD_TYPE_VS | VSIOC_SENSORSIZE_SET):
		return dpm_sensor_effect(dpm, SNR_EFFECT_SIZE, arg);
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_SET):
		if(dpm_sensor_effect(dpm, SNR_EFFECT_FRMRATE, arg) != 0){
			return 1;	
		}	
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			dpm_ve->frame_rate = arg;
			libvenc_bitrate_config(dpm_ve);
		}
		else if(dpm->type == DPM_TYPE_IMG){
			t_dpm_img * dpm_img = (t_dpm_img *)dpm;
			u8 old_frm_rate = dpm_img->frame_rate;
			dpm_img->frame_rate = arg;
			if(dpm_img->imgenc_tgt_size != 0 && arg > 0 && old_frm_rate != 0 && old_frm_rate != arg){
				libimg_set_bitrate(dpm_img, dpm_img->imgenc_tgt_size * 8 * old_frm_rate);
			}
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_TIMESTAMP_GET):
		if(dpm->type == DPM_TYPE_VE){
			return libvenc_get_reftm((t_dpm_ve*)dpm);
		}
		if(dpm->type == DPM_TYPE_IMG){
			return libimg_get_reftm((t_dpm_img *)dpm);
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			return dpm_ve->frame_rate;
		}else{
			t_dpm_sensor * dpm_sensor = (t_dpm_sensor *)dpm;
			while(dpm_sensor){
				if(dpm_sensor->type == DPM_TYPE_SENSOR){
					return dpm_sensor->sensor_cfg->cur_frame_rate;
				}
				dpm_sensor = (t_dpm_sensor *)dpm_sensor->in;
			}
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_BITRATE_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->bit_rate = arg;
				libvenc_bitrate_config(dpm_ve);
				return 0;
			}else{
				return 1;	
			}
		}
		else if(dpm->type == DPM_TYPE_IMG){
			t_dpm_img * dpm_img = (t_dpm_img *)dpm;
			libimg_set_bitrate(dpm_img, arg);
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_IMG_QS_SET):
		if(dpm->type == DPM_TYPE_IMG){
			t_dpm_img * dpm_img = (t_dpm_img *)dpm;
			libimg_set_qs(dpm_img, arg);
			return 0;
		}else{
			return 1;
		}
	case (MPUCMD_TYPE_VS | VSIOC_GOP_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->max_gop_size = arg;
				libvenc_bitrate_config(dpm_ve);
			}else{
				dpm_ve->gop_size = arg;	
			}
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_GOP_DURATION_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			if(dpm_ve->rc_cfg){
				t_ve_rc_cfg *rc_cfg = dpm_ve->rc_cfg;
				rc_cfg->max_gop_size = dpm_ve->frame_rate * arg / 30;
				libvenc_bitrate_config(dpm_ve);
			}else{
				dpm_ve->gop_size = dpm_ve->frame_rate * arg / 30;
			}
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_INIT_GOP_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->init_gop_size = (arg&0xffff);
				rc_cfg->init_gop_num = ((arg>>16)&0xffff);
			}else{
				return 2;	
			}
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_ADPGOP_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->adp_gop_en = arg;
				libvenc_bitrate_config(dpm_ve);
				return 0;
			}else{
				return 1;	
			}
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_MINGOP_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->min_gop_size = arg;
				libvenc_bitrate_config(dpm_ve);
				return 0;
			}else{
				return 1;	
			}
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_FORCEIFRAME_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL && rc_cfg->force_i_frame == 0){
				rc_cfg->force_i_frame = arg;
				return 0;
			}else{
				return 1;	
			}
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_IDRPERIOD_SET):
		if(dpm->type == DPM_TYPE_VE && arg > 0){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			dpm_ve->idr_period = arg;
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_MAXNODE_SET):
		if(dpm->type == DPM_TYPE_VE && arg > 0){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			dpm_ve->max_node_in_list = arg;
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_DROPFRM_TH_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			u8 drop_all_old = (arg >> 24);
			u32 drop_th = (arg & 0xffffff);
			dpm_ve->flag_drop_all_old_frms = drop_all_old;
			if(drop_th > 0){
				t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
				if(rc_cfg != NULL){
					dpm_ve->gop_size = rc_cfg->max_gop_size;
				}
				dpm_ve->drop_old_frms_th = drop_th + dpm_ve->gop_size - 1;
			}else{
				dpm_ve->drop_old_frms_th = 0;
			}
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_ADDSKIPFRM_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			dpm_ve->add_skip_frm = arg;
			return 0;
		}
		else{
			return 1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_VS_MAX_NODE_SET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			if(arg > 0){
				dpm_ve->max_node_in_list = arg;
			}else{
				dpm_ve->max_node_in_list = framelist_num(dpm_ve->framelist) - 1;
			}
			return 0;
		}
		else{
			return -1;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_MINQP_SET):
		{
			u8 i_qp = ((arg>>16)&0xff);
			u8 p_qp = arg&0xff;
			if(dpm->type != DPM_TYPE_VE || i_qp > 51 || i_qp < 1 || p_qp > 51 || p_qp < 1){
				return 1;	
			}else{
				t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
				t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
				if(rc_cfg != NULL){
					rc_cfg->min_qp[0] = i_qp;
					rc_cfg->min_qp[1] = p_qp;
					libvenc_config_qp_range(dpm_ve);
					return 0;
				}else{
					return 1;	
				}
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_MAXQP_SET):
		{
			u8 i_qp = (arg>>16)&0xff; 
			u8 p_qp = arg&0xff;
			if(dpm->type != DPM_TYPE_VE || i_qp > 51 || i_qp < 1 || p_qp > 51 || p_qp < 1){
				return 1;	
			}else{
				t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
				t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
				if(rc_cfg != NULL){
					rc_cfg->max_qp[0] = i_qp;
					rc_cfg->max_qp[1] = p_qp;
					libvenc_config_qp_range(dpm_ve);
					return 0;
				}else{
					return 1;	
				}
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_MINQP_GET):
		if(dpm->type != DPM_TYPE_VE ){
			return 0;	
		}else{
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_ctl *rcPt = dpm_ve->rc_ctl;
			if(rcPt != NULL){
				return ((rcPt->min_qp[0]<<16)|(rcPt->min_qp[1]));
			}else{
				return 0;	
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_MAXQP_GET):
		if(dpm->type != DPM_TYPE_VE ){
			return 0;	
		}else{
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_ctl *rcPt = dpm_ve->rc_ctl;
			if(rcPt != NULL){
				return ((rcPt->max_qp[0]<<16)|(rcPt->max_qp[1]));
			}else{
				return 0;	
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_INITQP_SET):
		if(dpm->type != DPM_TYPE_VE || arg > 51 || arg < 1){
			return 1;	
		}else{
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->init_qp = arg;
				libvenc_bitrate_config(dpm_ve);
				return 0;
			}else{
				return 1;	
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_QPDELTA_SET):
		if(dpm->type != DPM_TYPE_VE || arg > 20 || arg < 1){
			return 1;	
		}else{
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			t_ve_rc_cfg* rc_cfg = dpm_ve->rc_cfg;
			if(rc_cfg != NULL){
				rc_cfg->i_p_qp_delta = arg;
				libvenc_config_i_p_qp_delta(dpm_ve);
				return 0;
			}else{
				return 1;	
			}
		}
	case (MPUCMD_TYPE_VS | VSIOC_VIDEOPROFILE_SET):
		{
		t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
		if(dpm->type == DPM_TYPE_VE && dpm_ve->vs_running == 0 && (arg == 100 || arg == 77 || arg == 66)){
			dpm_ve->profile_idc = arg;	
			return 0;
		}else{
			return 1;
		}
		}
	case (MPUCMD_TYPE_VS | VSIOC_GET_TYPE):
		if(dpm->type == DPM_TYPE_VE){
			return 1;
		}
		if(dpm->type == DPM_TYPE_IMG){
			return 2;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_SPS_PPS_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			if(dpm_ve->vs_running == 0){
				venc_generate_sps(dpm_ve);
			}
			return (u32)(dpm_ve->sps_pps_buf)|BIT31;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_DROP_FRM_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			return dpm_ve->skipped_frm_cnt;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRM_IN_BUF_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			return dpm_ve->frm_in_buf;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRM_TOTAL_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			return dpm_ve->frm_cnt;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRM_READ_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			return (dpm_ve->frm_cnt - dpm_ve->buf_frm_cnt);
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_STRM_BUF_SIZE_GET):
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve* dpm_ve = (t_dpm_ve *)dpm;
			return dpm_ve->bs_buf_size;
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_STRM_BUF_LEFT_GET):
		if(dpm->type == DPM_TYPE_VE){
			return vrec_get_genbuf_left_size((t_dpm_ve*)dpm);
		}
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_ENABLE_META):
		if(dpm->type == DPM_TYPE_VE)
		{
			t_dpm_ispidc *dpm = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[vs_id];
			if( dpm != NULL)
			{
				dpm->add_meta_en = arg;
			}
		}
		return 0;
	default:
		return 1;
	}
	return 1;
}
