#include "includes.h"

int virtual_vs_set_event(VFDIST* vfdist){
	if(vfdist == NULL){
		return -1;	
	}	
	u8 vt_id = 0;
	u32 running_mask = vfdist->running_mask;
	while(running_mask){
		if(running_mask & (1<<vt_id)){
			libmpu_set_event(vt_id);
			running_mask &= (~(1<<vt_id));
		}
		vt_id++;
	}
	return 0;
}

int virtual_vs_clear_event(VFDIST* vfdist){
	if(vfdist == NULL){
		return -1;	
	}	
	u8 vt_id = 0;
	u32 running_mask = vfdist->running_mask;
	while(running_mask){
		if(running_mask & (1<<vt_id)){
			libmpu_clear_event(vt_id);
			running_mask &= (~(1<<vt_id));
		}
		vt_id++;
	}
	return 0;
}

static int vfdist_init(VFDIST* vfdist){
	int ret = 0;	
	vfdist->pool = pool_init(sizeof(t_vfdist_frm), vfdist->num, (u8*)vfdist->poolbuf);
	vfdist->cnt = 0;
	vfdist->head = NULL;
	vfdist->tail = NULL;
	vfdist->running_mask = 0;
	vfdist->init_mask = 0;
	vfdist->wait_ifrm_mask = 0;
	if(vfdist->dpm->type == DPM_TYPE_VE){
		t_dpm_ve* dpm_ve = (t_dpm_ve*)vfdist->dpm;
		dpm_ve->idr_period = 1;
		dpm_ve->vfdist = vfdist;
	}else if(vfdist->dpm->type == DPM_TYPE_IMG){
		t_dpm_img* dpm_img = (t_dpm_img*)vfdist->dpm;	
		dpm_img->vfdist = vfdist;
	}else{
		syslog(LOG_ERR, "ERR: unsupported dpm type %x for VFDIST\n", vfdist->dpm->type);	
		ret = -1;
	}
	return ret;
}

int virtual_vs_init(t_virtual_vs* vt_vs){
	u32 ret = 0;	
	if(vt_vs == NULL || vt_vs->vfdist == NULL){
		syslog(LOG_ERR, "ERR: vt_vs or vfdist NULL\n");	
		return -1;	
	}
	if(vt_vs->id > 31){
		syslog(LOG_ERR, "ERR: VS id more than 31\n");	
		return -1;	
	}
	vt_vs->mask = 1<<vt_vs->id;//每一路码流都有自己独立的标识位，作为该码流使能状态以及每一帧的读取状态的标记
	if(vt_vs->vfdist->init_mask == 0){
		//当前虚拟码流初始化是实际码流的首次初始化，需要设置特别标志去初始化实际码流
		ret = VIRTUAL_VS_NOT_DONE;
		vfdist_init(vt_vs->vfdist);	
	}
	vt_vs->vfdist->init_mask |= vt_vs->mask;//标记该虚拟码流已经初始化
	return ret;	
}

int virtual_vs_start(t_virtual_vs* vt_vs){
	u32 ret = 0;	
	if(vt_vs == NULL || vt_vs->vfdist == NULL){
		syslog(LOG_ERR, "ERR: vt_vs or vfdist NULL\n");	
		return -1;	
	}
	if(vt_vs->mask == 0){
		syslog(LOG_ERR, "VS %d not initalized\n", vt_vs->id);	
		return -1;	
	}
	if((vt_vs->vfdist->running_mask & vt_vs->mask) == 0){
		if(vt_vs->vfdist->running_mask  == 0){
			//running_mask为零，代表当前虚拟码流enable是实际码流的首次enable，需要返回特定标志去enable实际码流
			ret = VIRTUAL_VS_NOT_DONE;
		}
		//else: running_mask非零，实际码流已经在running，无需再启动
		if(vt_vs->vfdist->dpm->type == DPM_TYPE_VE){
			//A5端每一路虚拟码流都可以独立启动，启动后需要等一个IDR帧作为该码流的第一帧传出去
			vt_vs->vfdist->wait_ifrm_mask |= vt_vs->mask;
		}
		vt_vs->vfdist->running_mask |= vt_vs->mask;//标记该虚拟码流已经启动
	}
	return ret;	
}

int virtual_vs_stop(t_virtual_vs* vt_vs){
	if(vt_vs == NULL || vt_vs->vfdist == NULL){
		syslog(LOG_ERR, "ERR: vt_vs or vfdist NULL\n");	
		return -1;	
	}
	if(vt_vs->mask == 0){
		syslog(LOG_WARNING, "VS%d not initalized\n", vt_vs->id);	
		return -1;	
	}
	if((vt_vs->vfdist->running_mask & vt_vs->mask) != 0){
		vt_vs->vfdist->running_mask &= ~(vt_vs->mask);//标记该虚拟码流已经停止
		if(vt_vs->vfdist->running_mask == 0){
			//running_mask为零，代表所有虚拟码流都已经停止，返回特定标志请求停止实际码流	
			return VIRTUAL_VS_NOT_DONE;	
		}
	}
	return 0;	
}

static t_vfdist_frm * vfdist_alloc(VFDIST * vfdist){
	t_vfdist_frm * ret = (t_vfdist_frm *)pool_element_alloc(vfdist->pool);
	if(ret){
		memset(ret, 0, sizeof(t_vfdist_frm));
		vfdist->cnt ++;
	}
	return ret;
}

static int vfdist_frm_add(VFDIST* vfdist, FRAME* vs_frm){
	t_vfdist_frm* frm = vfdist_alloc(vfdist);	
	if(frm == NULL){
		syslog(LOG_ERR, "vfdist alloc error! %d, %d\n", vfdist->cnt, vfdist->num);
		return -2;
	}
	if(vs_frm->type == FRAME_TYPE_VIDEO_IDR){
		vfdist->wait_ifrm_mask = 0;
	}
	frm->vs_flag = vfdist->running_mask & (~vfdist->wait_ifrm_mask);
	//vs_flag的每一位代表对应码流是否需要读取该帧,1为需要读取，0为不需要读取.每一帧的free都必须等到vs_flag为零
	//只有当一个虚拟码流在running状态，并且不需要等待I帧的情况下，这一帧在该码流对应标志位设为待读取
	frm->vfrm = vs_frm;
	if(vfdist->head == NULL){
		vfdist->head = frm;
		vfdist->tail = frm;
	}else{
		vfdist->tail->next = frm;	
		vfdist->tail = frm;
	}
	return 0;
}

FRAME* virtual_vs_get_frm(t_virtual_vs* vt_vs){
	if(vt_vs == NULL || vt_vs->vfdist == NULL){
		syslog(LOG_ERR, "ERR: vt_vs or vfdist NULL\n");	
		return NULL;	
	}
	if(vt_vs->mask == 0){
		syslog(LOG_WARNING, "VS%d not initalized\n", vt_vs->id);	
		return NULL;	
	}
	FRAME* vs_frm = NULL;
	//先往VFDIST链表里面添加有效的stream frame
	if((vt_vs->vfdist->running_mask & vt_vs->mask) != 0 //只有该码流在enable状态才可能往vfdist链表里添加有效节点	
		&& (vt_vs->vfdist->num > vt_vs->vfdist->cnt) //VFDIST链表尚有空余节点
		&& ((vt_vs->vfdist->tail == NULL) //tail为空意味着当前VFDIST链表为空，需要添加节点
			|| (vt_vs->vfdist->tail->vs_flag & vt_vs->mask) == 0)){	
		//当前链表里最末一帧没有待读取标记，意味该码流start后VFDIST尚未加入新的frame，因此需要get stream frame
		vs_frm = vstream_get_frame(vt_vs->vfdist->dpm);
		if(vs_frm != NULL){
			if(vfdist_frm_add(vt_vs->vfdist, vs_frm) != 0){
				return NULL;	
			}	
		}else{
			return NULL;	
		}
	}
	vs_frm = NULL;
	t_vfdist_frm* frm = (t_vfdist_frm*)vt_vs->vfdist->head;	
	while(frm != NULL){
		if((frm->vs_flag & vt_vs->mask) != 0 && (frm->reading_flag & vt_vs->mask) == 0){
			//这一帧对应的当前虚拟码流待读取标记为1，并且这一帧不在reading状态，返回该有效帧	
			frm->reading_flag |= vt_vs->mask;	//标记为reading状态,下次读取时若为reading会继续往后搜索链表
			vs_frm = frm->vfrm;
			break;	
		}
		frm = frm->next;
	}
	return vs_frm;	
}

static int vfdist_frm_clean(VFDIST* vfdist){
	//清空链表里所有待读取标记为零的frame	
	while(vfdist->head && vfdist->head->vs_flag == 0){
		t_vfdist_frm* relframe = vfdist->head;
		vstream_free_frame(vfdist->dpm, (FRAME*)relframe->vfrm);
		if(relframe->next == NULL){
			vfdist->tail = NULL;	
		}
		vfdist->head = relframe->next;
		pool_element_free(vfdist->pool, relframe);
		vfdist->cnt--;
	}
	return 0;
}

int virtual_vs_free_frm(t_virtual_vs* vt_vs, FRAME* vs_frm){
	if(vt_vs == NULL || vt_vs->vfdist == NULL || vt_vs->mask == 0 || vs_frm == NULL){
		syslog(LOG_ERR, "ERR: vt_vs/vfdist/vs_frm NULL or not initalized\n");	
		return -1;	
	}
	t_vfdist_frm* frm = (t_vfdist_frm*)vt_vs->vfdist->head;
	while(frm != NULL){
		if(frm->vfrm == vs_frm){
			frm->vs_flag &= ~(vt_vs->mask);//清空待读取标记
			frm->reading_flag &= ~(vt_vs->mask);//清除reading标记
			break;
		}
		frm = frm->next;
	}
	if(frm == NULL){
		syslog(LOG_ERR, "free frm not found!\n");
		return -2;
	}
	return vfdist_frm_clean((VFDIST*)vt_vs->vfdist);
}

int virtual_vs_exit(t_virtual_vs* vt_vs){
	u32 ret = 0;
	if(vt_vs == NULL || vt_vs->vfdist == NULL){
		syslog(LOG_ERR, "ERR: vt_vs or vfdist NULL\n");	
		return -1;	
	}
	if(vt_vs->mask == 0){
		syslog(LOG_WARNING, "VS%d not initalized\n", vt_vs->id);	
		return -1;	
	}
	t_vfdist_frm* frm = (t_vfdist_frm*)vt_vs->vfdist->head;
	while(frm != NULL){
		//退出码流时把链表里的所有待读取标记和reading标记全部清掉
		frm->vs_flag &= ~(vt_vs->mask);
		frm->reading_flag &= ~(vt_vs->mask);
		frm = frm->next;
	}
	vfdist_frm_clean(vt_vs->vfdist);	//清掉VFDIST里所有读取完成的frame
	vt_vs->vfdist->init_mask &= ~(vt_vs->mask);//清空init状态，标记该码流已经退出
	if(vt_vs->vfdist->init_mask == 0){
		ret = VIRTUAL_VS_NOT_DONE;//所有虚拟码流都已经exit，返回特定标记请求退出实际码流
	}
	vt_vs->mask = 0;
	return ret;	
}
