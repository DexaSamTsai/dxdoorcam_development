/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/

#include "includes.h"

#define PSEUDO_STREAM_SIZE 0xd400
#define DDR_ADDR_OFFSET    MEMBASE_DDR 
#define DUMMY_DATA_ADDR (DDR_ADDR_OFFSET + 10*1024*1024)
extern u8 sample_h264[];
int total_frm;
FRAME stream_frm[10];
int framerate = 30;
int bitrate = 0;
u8 *dummy_data;
static int vs_id;
static int video_ts = 0;
void sample_framerate(int frmrate)
{
//	debug_printf("frmrate:%d\n",frmrate);
	framerate = frmrate;
}
static u32 reftm;
u32 _get_reftm(ref)
{
	return get_ms() - ref * 10 ;
}

void sample_bitrate(int bitrate)
{
	bitrate = bitrate;
	int def_bitrate = 1272*1024;
	int dummy_len; 
	int i;
	//if(def_bitrate<(bitrate/framerate*30))
	if(PSEUDO_STREAM_SIZE/10<(bitrate/framerate))
	{
		dummy_data = DUMMY_DATA_ADDR;

		dummy_len = (bitrate/framerate - PSEUDO_STREAM_SIZE/10)/8;
		debug_printf("dummy:%d",dummy_len);
		for(i = 0;i<total_frm; i++)
		{
			stream_frm[i].addr1 = dummy_data;
			memset(dummy_data, 0, dummy_len);
			stream_frm[i].size1 = dummy_len; 
		}
	}
	else
	{
		for(i = 0;i<total_frm; i++)
		{
			stream_frm[i].addr1 = 0;
			stream_frm[i].size1 = 0; 
		}
	}
}

void sample_frame_init(void)
{
	
	u8 *buf = sample_h264;
	int i = 0;
	int cur_ts = 0;
	framerate = 30;
	while(1)
	{
		if(*buf== 0 && *(buf + 1) == 0 && *(buf+2)== 0 && *(buf+3) == 0x01)
		{
			if(*(buf+4) == 0x67 && *(buf+5) == 0x64)
			{
			//	debug_printf("idrframe\n");
				stream_frm[i].addr = buf;
//				stream_frm[i].ts = cur_ts;
				stream_frm[i].type = FRAME_TYPE_VIDEO_IDR;
				if(i != 0)
				{
					stream_frm[i-1].size = stream_frm[i].addr - stream_frm[i-1].addr;
				}
				i ++;
			}
			if(*(buf+4) == 0x41 && *(buf+5) == 0x9a)
			{
//				debug_printf("pframe\n");
				stream_frm[i].addr = buf;
//				stream_frm[i].ts = cur_ts;
				stream_frm[i].type = FRAME_TYPE_VIDEO_P;
				if(i != 0)
				{
					stream_frm[i-1].size = stream_frm[i].addr - stream_frm[i-1].addr;
				}
				i ++;
			}
		}
		buf ++;
		if((buf - sample_h264 )>= PSEUDO_STREAM_SIZE)//sizeof(sample_h264))
		{
			debug_printf("total frame:%x", i);
			stream_frm[i-1].size = buf - stream_frm[i-1].addr;
			total_frm = i;
			break;
		}
//		cur_ts += (100/framerate);
	}
	reftm = ticks;
}

static int cur_frm_num = 0;
static u32 cur_ticks = 0;
FRAME * get_sample_frame(void)
{
	if(cur_ticks == 0)
	{
		cur_ticks = ticks;	
	}
	//debug_printf("t:%d\n", ticks);
	if((ticks - cur_ticks)>(100/framerate))
	{
//		debug_printf("r:%d\n", ticks);
		cur_ticks = ticks;
		FRAME *frm = &stream_frm[cur_frm_num];
		//frm->ts = _get_reftm(reftm);
		video_ts += (100*10/framerate);
		frm->ts = video_ts; 
		//debug_printf("getsamplets:%d \n", frm->ts);
		return frm;
	}
	else
	{
		libmpu_clear_event(vs_id);
		return NULL;
	}
}

int release_sample_frame(FRAME *frm)
{
	cur_frm_num ++;
	if(cur_frm_num  == total_frm)
	{
		cur_frm_num = 0;
	}
	return 0;
}
static u32 video_running = 0;
static u32 vstream_handle_cmd(u32 cmd, u32 arg)
{
	vs_id = (cmd >> 12) & 0xf;
	cmd &= 0xffff0fff;
	t_dpm *dpm;
	dpm = g_libdatapath_cfg->vs_map[vs_id];
	switch(cmd)
	{
	case MPUCMD_VS_INIT:
		return 0;

	case MPUCMD_VS_START:
		sample_frame_init();
		video_running = 1;
		return 0;

	case MPUCMD_VS_STOP:
		video_running = 0;
		return 0;

	case MPUCMD_VS_EXIT:
		return 0;

	case MPUCMD_VS_GETFRM:
		if(video_running == 1)
		{
			return (u32)get_sample_frame();
		}
		else
		{
			return NULL;
		}

	case MPUCMD_VS_RELFRM:
		release_sample_frame(arg);
		return 0;
	
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_SET):
		sample_framerate(arg);
		return 0;
	
	case (MPUCMD_TYPE_VS | VSIOC_BITRATE_SET):
		sample_bitrate(arg);
		return 0;
	default:
		return 1;
	}
	return 1;
}

static int vstream_process(void)
{
	if((ticks - cur_ticks)>(100/framerate))
	{
		libmpu_set_event(vs_id);
	}
	return 0;
}

__ba2app_cfg t_ba2app_cfg g_ba2app_vstream_cfg = {
	.mpucmd_type = MPUCMD_TYPE_VS,
	.app_cmdhandler = vstream_handle_cmd,
	.app_process = vstream_process,
};
