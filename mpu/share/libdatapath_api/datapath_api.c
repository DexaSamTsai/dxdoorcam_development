/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/

#include "includes.h"

extern u32 vstream_handle_cmd(u32 cmd, u32 arg);
static u32 datapath_handle_cmd(u32 cmd, u32 arg)
{
	if(g_libdatapath_cfg && g_libdatapath_cfg->handle_cmd){
		u32 handled = 0;
		u32 ret = g_libdatapath_cfg->handle_cmd(cmd, arg, &handled);
		if(handled){
			return ret;
		}
	}

	if((cmd & 0xffff0000) == MPUCMD_TYPE_VS){
		return vstream_handle_cmd(cmd, arg);
	}

	int ret = 0;
	if(cmd >= MPUCMD_TYPE_DATAPATH && cmd <= (MPUCMD_TYPE_DATAPATH+0xff)){
		switch(cmd)
		{
			case MPUCMD_DATAPATH_INIT:
				return libdatapath_init(&libdatapath_cfg[arg]);
			case MPUCMD_DATAPATH_START:
				ret = libdatapath_start();
				if(g_libdatapath_cfg->cb_multi_sensor_sync){
					g_libdatapath_cfg->cb_multi_sensor_sync();
				}
				return ret;
			case MPUCMD_DATAPATH_STOP:
				return libdatapath_stop();
			case MPUCMD_DATAPATH_EXIT:
				return libdatapath_exit();
			case MPUCMD_DATAPATH_FASTINIT:
				g_libdatapath_cfg = &libdatapath_cfg[arg];
				g_libdatapath_cfg->fastboot_cur_fps1cnt = 0;
				return libdatapath_init(g_libdatapath_cfg);
			case MPUCMD_DATAPATH_FASTSTART:
				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_INITED;
				libdatapath_loop_signal();
				return 0;
			case MPUCMD_DATAPATH_FASTFPS2FPS:
				g_libdatapath_cfg->fastboot_fps2_fps = arg;
				libdatapath_loop_signal();
				return 0;
			case MPUCMD_DATAPATH_FASTFPS2START:
				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS2START;
				libdatapath_loop_signal();
				return 0;
			case MPUCMD_DATAPATH_FAST_SIMU:
				{
					WriteReg32(SC_BASE_ADDR+0x0c, (ReadReg32(SC_BASE_ADDR+0x0c)&0xff9fffff)|(1<<21));		//2 lanes 
					WriteReg32(SC_BASE_ADDR+0x10c, (ReadReg32(SC_BASE_ADDR+0x10c)&0xfffffff0)|0x02);  
					WriteReg32(SC_BASE_ADDR+0x104, (ReadReg32(SC_BASE_ADDR+0x104))|BIT24);  				//invert pclk
					t_dpm_sensor * p = (t_dpm_sensor *)libdatapath_cfg[arg].head;
					p->fastboot = 0;
					dpm_sensor_init((t_dpm_sensor *)libdatapath_cfg[arg].head);
					dpm_sensor_start((t_dpm_sensor *)libdatapath_cfg[arg].head);
					p->fastboot = 1;
				}
				return 0;
			default:
				return 1;
		}
	}else if(cmd >= MPUCMD_TYPE_MD && cmd <= (MPUCMD_TYPE_MD+0xff)){
		u8 md_id = ((cmd >> 12)&0xf);
		cmd &= 0xffff0fff;
		t_dpm_md *dpm_md = (t_dpm_md *)(g_libdatapath_cfg->md_map[md_id]);
		if(dpm_md && dpm_md->handle_cmd){
			u32 handled = 0;
			u32 ret = dpm_md->handle_cmd(dpm_md, md_id, cmd, arg, &handled);
			if(handled){
				return ret;
			}
		}
		switch(cmd){
			case MPUCMD_DATAPATH_MD_GET_STA:
				return md_get_status(md_id);
			case MPUCMD_DATAPATH_MD_CFG:
				return md_config(md_id, (t_md_win_cfg *)arg);
			case MPUCMD_DATAPATH_MDWNDCNT_SET:
				return md_wnd_cnt_set(md_id, arg);
			case MPUCMD_DATAPATH_MD_GET_BUF:
				return (u32)md_get_buf(md_id);
			default:
				return 1;
		}
	}
	return 1;
}

extern int libisp_fast_fps1init(void);
extern int libisp_fast_fps1stop(void);
extern int libisp_fast_fps2init(void);
static int datapath_process(void)
{
	if(g_libdatapath_cfg && (g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_EN))
	{
		if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE){
			//2 stage mode
			if(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_INITED)
			{
				if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_DEBUG){
					lowlevel_putc('\n');
					lowlevel_putc('1');
				}
				///< FIXME: when isp does crop, and libisp_init is called after isp_start in datapath thread, there is no ISP EOF interrupt.
				libisp_fast_fps1init();
				libdatapath_start();

				if(g_libdatapath_cfg->vs_map[0] && g_libdatapath_cfg->vs_map[0]->type == DPM_TYPE_VE ){
					t_dpm_ve * dpm_ve = (t_dpm_ve *)g_libdatapath_cfg->vs_map[0];
					if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_VDEBUG){
						dpm_ve->autoskip_count = 0;
					}
					else{
						dpm_ve->autoskip_count = 0xFFFF;
					}
				}

				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS1RDY;
				libmpu_set_event(MPUEVT_FASTBOOT_STATUS + FAST_STAGE_FPS1RDY);
			}
		
			if(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_FPS1STOP)
			{
				if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_DEBUG){
					lowlevel_putc('\n');
					lowlevel_putc('2');
				}

				libisp_fast_fps1stop();

				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS1DONE;
				libmpu_set_event(MPUEVT_FASTBOOT_STATUS + FAST_STAGE_FPS1DONE);
			}

			if(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_FPS1DONE)
			{
				if(g_libdatapath_cfg->fastboot_fps2_fps !=0)
				{
					libisp_fast_fps2init();

					g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS2RDY;
					libmpu_set_event(MPUEVT_FASTBOOT_STATUS + FAST_STAGE_FPS2RDY);
				}
			}

			if(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_FPS2START){
				if(g_libdatapath_cfg->fastboot_fps2_fps !=0){
					if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_DEBUG){
						lowlevel_putc('\n');
						lowlevel_putc('3');
					}

					if(g_libdatapath_cfg->vs_map[0] && g_libdatapath_cfg->vs_map[0]->type == DPM_TYPE_VE ){
						t_dpm_ve * dpm_ve = (t_dpm_ve *)g_libdatapath_cfg->vs_map[0];
						dpm_ve->autoskip_count = 0;
					}

					if(g_libdatapath_cfg->fastboot_mipirx == 0){
						SC_RESET0_RELEASE(MIPIRX0);
					}else if(g_libdatapath_cfg->fastboot_mipirx == 1){
						SC_RESET0_RELEASE(MIPIRX1);
					}else if(g_libdatapath_cfg->fastboot_mipirx == 2){
						SC_RESET0_RELEASE(MIPIRX2);
					}else if(g_libdatapath_cfg->fastboot_mipirx == 3){
						SC_RESET0_RELEASE(MIPIRX3);
					}

					t_dpm_sensor *dpm_s = (t_dpm_sensor *)g_libdatapath_cfg->head;
					libsccb_wr(dpm_s->sccb_cfg,0x100,1);
					//done
					g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS2STARTED;
					if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_DEBUG){
						lowlevel_putc('\n');
						lowlevel_putc('4');
					}
					libmpu_set_event(MPUEVT_FASTBOOT_STATUS + FAST_STAGE_FPS2STARTED);
				}
			}
		}else{	//only one stage
			if((g_libdatapath_cfg->fastboot_stage == FAST_STAGE_INITED)&&(g_libdatapath_cfg->fastboot_fps2_fps !=0))
			{

				//1 stage, inited -> fps2rdy
				///< FIXME: when isp does crop, and libisp_init is called after isp_start in datapath thread, there is no ISP EOF interrupt.
#ifdef BOOT_TIME_TEST
				lowlevel_forceputc('\n');
				lowlevel_forceputc('i');
				lowlevel_forceputc('i');
				lowlevel_forceputc('s');
				lowlevel_forceputc('p');
#endif
				libisp_fast_fps2init();
#ifdef BOOT_TIME_TEST
				lowlevel_forceputc('\n');
				lowlevel_forceputc('i');
				lowlevel_forceputc('o');
#endif
				libdatapath_start();
#ifdef BOOT_TIME_TEST
				lowlevel_forceputc('\n');
				lowlevel_forceputc('p');
				lowlevel_forceputc('a');
				lowlevel_forceputc('t');
				lowlevel_forceputc('h');
				lowlevel_forceputc('o');
#endif
				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS1RDY;
				libmpu_set_event(MPUEVT_FASTBOOT_STATUS + FAST_STAGE_FPS1RDY);
				
				
			}
			else if(g_libdatapath_cfg->fastboot_stage == FAST_STAGE_FPS1RDY)
			{
				g_libdatapath_cfg->fastboot_stage = FAST_STAGE_FPS1DONE;
			}
		}
	}
	return libdatapath_loop();
}

#define TASK_STACKSIZE 4096
static u32 _task_stack_dp[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_dp_cfg = {
	.name = "dp",
	.prio = 10,
	.stk = _task_stack_dp,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_DATAPATH,
	.app_cmdhandler = datapath_handle_cmd,
	.app_process = datapath_process,
};

void libdatapath_loop_signal(void)
{
	task_signal(&g_ba2app_dp_cfg);
}
