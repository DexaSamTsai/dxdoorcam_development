/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/

#include "includes.h"

static u32 datapath_handle_cmd(u32 cmd, u32 arg)
{
	switch(cmd)
	{
	case MPUCMD_DATAPATH_INIT:
		return 0;
	case MPUCMD_DATAPATH_START:
		return 0;
	case MPUCMD_DATAPATH_STOP:
		return 0;
	default:
		return 1;
	}
	return 1;
}

static int datapath_process(void)
{
	return 0;
}

#define TASK_STACKSIZE 4096
static u32 _task_stack_dp[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_dp_cfg = {
	.name = "dp",
	.prio = 10,
	.stk = _task_stack_dp,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_DATAPATH,
	.app_cmdhandler = datapath_handle_cmd,
	.app_process = datapath_process,
};
