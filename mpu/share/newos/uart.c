#include "includes.h"

void lowlevel_forceputc(unsigned char c){
#ifdef CONFIG_CONSOLE_UART1
	uarts_forceputc(c);
#else
	uart_forceputc(c);
#endif
}
