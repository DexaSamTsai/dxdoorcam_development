#include "includes.h"

t_newos NewOSCtx;

#ifdef CONFIG_MPUUART_LOWLEVEL_EN
unsigned char cbuf[64];
#endif
/*********** idle task ***********/
static void task_idle_main(void * arg)
{
	MTSPR(SPR_PM_EVENT_CTRL,0x2); //enable PM event
	while(1){
		//set CPU in idle
		//WriteReg32(SC_BASE_ADDR+0x60,ReadReg32(SC_BASE_ADDR+0x60)|BIT16);
		//WriteReg32(SC_BASE_ADDR+0x60,ReadReg32(SC_BASE_ADDR+0x60)&~BIT16); //clear it
//		lowlevel_putc('.');
//		volatile int i; for(i=0; i<10000000; i++);
#ifdef CONFIG_MPUUART_LOWLEVEL_EN
		int len = debug_printf_getbuf(&g_debugprintf_hw_uart, &cbuf, 16);
		int n;
		for(n=0; n<len; n++){
			lowlevel_putc(cbuf[n]);
		}
#endif
	}
}

#define IDLETASK_STACKSIZE 1024
static u32 _task_stack_idle[IDLETASK_STACKSIZE/4];
static t_task task_idle_cfg = {
	.name = "idle",
	.prio = 31,
	.stk = _task_stack_idle,
	.stack_size = IDLETASK_STACKSIZE/4,
};

int task_idle_init(void)
{
	return task_add(&task_idle_cfg, task_idle_main);
}

/********** sys task ***********/
static u32 sys_handle_cmd(u32 cmdID,u32 param_addr)
{
	switch(cmdID)
	{
	case MPUCMD_SYS_TEST_BA:
		return 0;
	case MPUCMD_SYS_GETPRIO:
	{
		char * name = (char *)param_addr;
		int i;
		for(i=0; i<=MAX_TASK; i++){
			if(g_newos->tasks[i] == NULL || g_newos->tasks[i]->name == NULL){
				continue;
			}
			if(!strcmp(g_newos->tasks[i]->name, name)){
				break;
			}
		}
		if(i > MAX_TASK){
			return -1;
		}else{
			return i;
		}
		break;
	}
	case MPUCMD_SYS_SETLOGMASK:
	{
		u32 prio = ((param_addr >> 16) & 0xffff);
		int logmask = param_addr & 0xff;

		if(prio == 0xffff){
			//set all the tasks's logmask
			int i;
			for(i=0; i<=MAX_TASK; i++){
				if(g_newos->tasks[i]){
					g_newos->tasks[i]->logmask = logmask;
				}
			}
			return 0;
		}else if(prio < 0 || prio > MAX_TASK){
			return -1;
		}
		if(g_newos->tasks[prio] == NULL){
			return -3;
		}
		g_newos->tasks[prio]->logmask = logmask;
		return 0;
	}
	default:
		return 0;
	}
	return 0;
}

#define SYSTASK_STACKSIZE 1024
static u32 _task_stack_sys[SYSTASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_sys_cfg = {
	.name = "sys",
	.prio = 1,
	.stk = _task_stack_sys,
	.stack_size = SYSTASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_SYS,
	.app_cmdhandler = sys_handle_cmd,
};
