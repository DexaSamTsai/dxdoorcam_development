#include "includes.h"

static unsigned int clockget_pll_vco(u32 v1, u32 v2)
{
	u32 prediv_sel = (v1 >> 16) & 7;
	u32 prediv = 0;

	switch(prediv_sel){
		case 0: prediv = 24000000/1; break;
		case 1: prediv = 16000000; break;
		case 2: prediv = 24000000/2; break;
		case 3: prediv = 9600000; break;
		case 4: prediv = 24000000/3; break;
		case 5: prediv = 24000000/4; break;
		case 6: prediv = 24000000/6; break;
		case 7: prediv = 24000000/8; break;
		default:prediv = 24000000; break;
	}

	u32 divp = v1 & 0xff;
	u32 divs = (v1 >> 19) & 1;
	u32 dsm  = v2 & 0x000fffff;
	
	if(v2&BIT26){
			return (prediv) * (2 * divp + divs) + (((prediv/5861)* dsm) >> 20)*5861;
	}
	return (prediv) * (2 * divp + divs);
}

static unsigned int clockget_pll_ext(unsigned int base)
{
	u32 v1 = ReadReg32(base);
	u32 v2 = ReadReg32(base + 4);

	u32 vco = clockget_pll_vco(v1, v2);

	u32 div = (v1 >> 12) & 0xf;
	switch(div){
		case 2: 
		case 3: 
		case 4: 
		case 5: 
		case 6: 
		case 7: 
		case 8: 
		case 9: 
		case 10: 
			 return (vco/div);
		case 11:
		case 12:
		case 13:
			 return ((vco*2)/3);
		case 14:
		case 15:
			 return ((vco*2)/5);
		case 0:
		case 1:
		default:
		     return (vco);
	}
}

static unsigned int clockget_mipipll_ext(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x110);
}

static unsigned int clockget_ddrpll_ext(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x120);
}
static unsigned int clockget_usbpll(void)
{
	return 240000000;
}

static unsigned int clockget_pll_sys(unsigned int base)
{
	u32 v1 = ReadReg32(base);
	u32 v2 = ReadReg32(base + 4);

	u32 vco = clockget_pll_vco(v1, v2);

	u32 div = (v1 >> 8) & 0xf;
	switch(div){
		case 2: 
		case 3: 
		case 4: 
		case 5: 
		case 6: 
		case 7: 
		case 8: 
		case 9: 
		case 10: 
			 return (vco/div);
		case 11:
		case 12:
		case 13:
			 return ((vco*2)/3);
		case 14:
		case 15:
			 return ((vco*2)/5);
		case 0:
		case 1:
		default:
		     return (vco);
	}
}
static unsigned int clockget_syspll_sys(void)
{
	return clockget_pll_sys(REG_SC_ADDR + 0x68);
}

static unsigned int clockget_syspll_isp(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x68);
}

static unsigned int clockget_mpupresys(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV5);

	u32 presys = 0;
	if((div0 & BIT22) == 0){
		presys = 24000000;
	}else{
		switch((div0 >> 23) & 3){
		case 0:
			presys = clockget_syspll_sys();
			break;
		case 1:
			presys = clockget_usbpll();
			break;
		case 2:
			presys = clockget_ddrpll_ext();
			break;
		case 3:
		default:
			presys = clockget_mipipll_ext();
			break;
		}
	}

	return presys;
}

unsigned int clockget_mpupm(void)
{
	u32 pmdiv = ReadReg32(REG_SC_ADDR + 0x3c) & 0xff;
	if(pmdiv == 0){
		pmdiv = 1;
	}

	return clockget_mpupresys() / pmdiv;
}

static unsigned int clockget_cpupresys(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV0);

	u32 presys = 0;
	if((div0 & 1) == 0){
		presys = 24000000;
	}else{
		switch((div0 >> 1) & 3){
		case 0:
			presys = clockget_syspll_sys();
			break;
		case 1:
			presys = clockget_usbpll();
			break;
		case 2:
			presys = clockget_ddrpll_ext();
			break;
		case 3:
		default:
			presys = clockget_mipipll_ext();
			break;
		}
	}

	return presys;
}

static unsigned int clockget_cpusys(void)
{
//	u32 sysdiv = (ReadReg32(REG_SC_DIV0) >> 7) & 0x1f;
	u32 sysdiv = (ReadReg32(REG_SC_DIV1) >> 24) & 0xff;
	if(sysdiv == 0){
		sysdiv = 1;
	}

	u32 presys = clockget_cpupresys();

	return presys / sysdiv;
}


static unsigned int clockget_cpubus(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV5);

	if(div0 & BIT28){
		return clockget_cpusys() / 2;
	}else{
		return clockget_cpusys();
	}
}

unsigned int clockget_bus(void)
{
#ifdef FOR_FPGA
	return 48000000;
#else
	u32 regdiv6 = ReadReg32(REG_SC_DIV6);
	u32 inclk = 0;

	switch((regdiv6  >> 5) & 3){
	case 0:
		inclk = clockget_cpubus();
		break;
	case 1:
		inclk = clockget_usbpll();
		break;
	case 2:
		inclk = clockget_syspll_isp();
		break;
	case 3:
	default:
		inclk = clockget_mipipll_ext();
		break;
	}

	int div = (regdiv6 & 0x1f) ;
	if(div == 0){
		div = 1;
	}

	return inclk / div;
#endif
}

extern void (*cb_tick_intr_enable)(void);

typedef struct s_mpu_cmd{
	volatile u32 used;     ///<command status,1:command in used 0:command not in used
	volatile u32 arg;               ///<command argument
	volatile u32 id;         ///<command id
	volatile s32 ret;        ///<command return
	volatile u32 ret_ready;
}t_mpu_cmd;

typedef struct s_mpu_sig{
	volatile u32 status;
}t_mpu_sig;

#define g_mpucmd (*(t_mpu_cmd *)(0x900cffe0))
#define g_mpusig (*(t_mpu_sig *)(0x900cfff8))

//#define DCPU_DEBUG_PRINT
void libmbx_irq_handler_fix(void * arg)
{
	u32 ret = 0;

#ifdef DCPU_DEBUG_PRINT
	debug_printf("libmbx_irq_handler\n");
#endif

	ret = libmbx_s_getintsta();
	if(!ret){
		return;
	}

	libmbx_s_read(); //clear interrupt

	g_mpucmd.ret_ready = 0;

	int i;
	for(i=0; i <= MAX_TASK; i++){
		if(g_newos->tasks[i] != NULL && g_newos->tasks[i]->app_cmdhandler && ( g_newos->tasks[i]->mpucmd_type == (g_mpucmd.id & 0xffff0000) || ((g_mpucmd.id & 0xffff0000) == MPUCMD_TYPE_VS && g_newos->tasks[i]->mpucmd_type == MPUCMD_TYPE_DATAPATH) ) ){
			g_newos->tasks[i]->mpucmd_ready = 1;
#ifdef DCPU_DEBUG_PRINT
			debug_printf("mpucmd tasksignal:%d\n", i);
#endif
			task_signal(g_newos->tasks[i]);
			break;
		}
	}
	if(i > MAX_TASK){
		debug_printf("MPUERR: cmd:%x no handler\n", g_mpucmd.id);
	}
}

static void tick_intr_enable_new(void)
{
	ENABLE_INTERRUPT;
	//TODO: get from register, probably not 72M
	//tick1_set_rt(72000000/ TICKS_PER_SEC);
	tick1_set_rt(clockget_mpupm()/ TICKS_PER_SEC);
	ENABLE_TICK;

	irq_request(IRQ_BIT_MBX, (t_irq_handler)libmbx_irq_handler_fix, NULL);
}

/*********** callback ************/
void newos_fix(void)
{
	cb_tick_intr_enable = tick_intr_enable_new;
}

