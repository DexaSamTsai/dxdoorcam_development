#include "includes.h"

void isp_frm_update(t_dpm_ispidc *dpm)
{
    if (dpm->vts_current != g_tMiddle_L.nCurrentVTS){
		unsigned int frm = 0x338*24/g_tMiddle_L.nCurrentVTS;	//TODO:clean
		if(frm > 0 && frm < 25){//avoid incorrect initial value
			vstream_handle_cmd((MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET) | (0 << 12), frm);
		}
		dpm->vts_current = g_tMiddle_L.nCurrentVTS;
	   }
}

u32 isp_set_auto_fps(t_dpm_ispidc * dpm, int fps_range){
	int autofps_enable = (fps_range&0xff0000)>>16;
	if (autofps_enable){
		g_pControl_L->bAutoFrameRateEnable = 1;
		dpm->vts_current = 0x338;	//TODO:this need clean to be ok with differrnt sensors
		
		int max_fps = (fps_range & 0xff00) >> 8;
		int min_fps = fps_range & 0x00ff;
		unsigned int max_ratio = (max_fps<<5)/min_fps;
		g_pControl_L->nAFRRatio[0]= max_ratio*1/3;
		g_pControl_L->nAFRRatio[1]= max_ratio*2/3;
		g_pControl_L->nAFRRatio[2]= max_ratio;
	}else
	{
		g_pControl_L->bAutoFrameRateEnable = 0;
	}
    	
	return 0;	
}
