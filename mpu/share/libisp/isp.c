/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/

#include "includes.h"

int libisp_init(t_dpm_ispidc *dpm_ispidc)
{
//	g_libdatapath_cfg->fastboot_exp = 0;
//	g_libdatapath_cfg->fastboot_gain = 0;

	dpm_ispidc->night2day_delay_frm = 0;

	if(dpm_ispidc->isp_brightness == 0)
	{
		dpm_ispidc->isp_brightness = 128;
	}

	//find the RGBIR dpm
	t_dpm * dpm_tmp = dpm_ispidc->in;
	while(dpm_tmp){
		if(dpm_tmp->type == DPM_TYPE_RGBIR){
			break;
		}
		dpm_tmp = dpm_tmp->in;
	}
	if(dpm_tmp != NULL)
	{
		dpm_ispidc->dpm_rgbir = (t_dpm_rgbir *)dpm_tmp;
		dpm_ispidc->isp_mode = g_libdatapath_cfg->isp_mode;
		dpm_ispidc->isp_setmode = g_libdatapath_cfg->isp_mode;
	}

	dpm_ispidc->inited = 1;

	init_algo();
	init_dma();
	if(g_libdatapath_cfg == NULL || g_libdatapath_cfg->head == NULL){
		return -1;
	}
	t_dpm_sensor * dpm_sensor = (t_dpm_sensor *)g_libdatapath_cfg->head;
	t_dpm_sensor * dpm_sensor2 = (t_dpm_sensor *)g_libdatapath_cfg->head->next;
	if(dpm_sensor->sensor_cfg == NULL){
		return -2;
	}
	if(dpm_sensor->sensor_cfg->isp_init){
		dpm_sensor->sensor_cfg->isp_init(dpm_sensor->sensor_cfg);
	}
	if(dpm_ispidc->dpm_rgbir){
		dpm_ispidc->dpm_rgbir->rgbir_reg = ReadReg32(0xc0038c20);
	}

	g_libdatapath_cfg->default_TargetYLow = g_pControl_L->nTargetYLow;
	g_libdatapath_cfg->default_TargetYHigh = g_pControl_L->nTargetYHigh;

	//adjust ae target according to brightness setting.
	if(dpm_ispidc->isp_brightness != 128){
		isp_set_ae_target(dpm_ispidc,dpm_ispidc->isp_brightness);
	}

	g_pControl_L->nSensorI2COption = (g_pControl_L->nSensorI2COption&0xfc)|dpm_sensor->sccb_cfg->sccb_num;
	if(dpm_sensor2){
		g_pControl_R->nSensorI2COption = (g_pControl_R->nSensorI2COption&0xfc)|dpm_sensor2->sccb_cfg->sccb_num;
	}else{
		g_pControl_R->nSensorI2COption = g_pControl_L->nSensorI2COption;	
	}
	libisp_irq_init();
	isp_lum_init(dpm_ispidc);

	return 0;
}

//TODO : need clean
u8 isp_lenc_c[768];
u8 isp_lenc_a[768];
u8 isp_lenc_d[768];

u32 libisp_handle_cmd(u32 cmd, u32 arg)
{
	int isp_id = (cmd >> 12) & 0xf;
	cmd &= 0xffff0fff;

	if(isp_id >= VS_MAX_STREAMS){
		syslog(LOG_ERR, "ispid not exist\n");
		return -1;
	}

	//these four cmds maybe used before datapath start, so don't check the dpm_ispidc
	if(cmd == MPUCMD_ISP_SET_LBUF)
	{
		L_BASE_ADDRESS = arg | BIT31;
		return 0;
	}
	else if(cmd == MPUCMD_ISP_SET_RBUF)
	{
		R_BASE_ADDRESS = arg | BIT31;
		return 0;
	}

	if(((g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE) && (g_libdatapath_cfg->fastboot_stage < FAST_STAGE_FPS2RDY))||
		((g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_EN) && (g_libdatapath_cfg->fastboot_stage < FAST_STAGE_FPS1DONE))){
		// set exp/gain in one stage mode or night mode two stage mode 
		if(cmd == MPUCMD_ISP_SET_EXP) {
			g_libdatapath_cfg->fastboot_exp = arg;
			return 0;
		}else if(cmd == MPUCMD_ISP_SET_GAIN) {
			g_libdatapath_cfg->fastboot_gain = arg;
			return 0;
		}

		if(!(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE))
		{
			if( cmd == MPUCMD_ISP_SET_DAYNIGHT)
			{
				g_libdatapath_cfg->isp_mode = arg;
				return 0;
			}else if( cmd == MPUCMD_ISP_SET_AWB_B_GAIN) {
				g_libdatapath_cfg->fps1_awb0 = arg;
				return 0;
			}else if( cmd == MPUCMD_ISP_SET_AWB_G_GAIN) {
				g_libdatapath_cfg->fps1_awb1 = arg;
				return 0;
			}else if( cmd == MPUCMD_ISP_SET_AWB_R_GAIN) {
				g_libdatapath_cfg->fps1_awb2 = arg;
				return 0;
			}
		}
	}

	t_dpm_ispidc *dpm_ispidc;
	dpm_ispidc = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[isp_id];
	if(dpm_ispidc == NULL){
		syslog(LOG_ERR, "ispidc not exist\n");
		return -1;
	}

	//t_dpm_sensor * dpm_sensor = (t_dpm_sensor *)g_libdatapath_cfg->head;
	t_dpm_sensor * dpm_sensor2 = (t_dpm_sensor *)g_libdatapath_cfg->head->next;
	switch(cmd)
	{
	case MPUCMD_ISP_INIT:
		if(dpm_ispidc->inited){
			return 0;
		}
		return libisp_init(dpm_ispidc);
	case MPUCMD_ISP_EXIT:
		if(! dpm_ispidc->inited){
			return 0;
		}
		libisp_irq_free();
		dpm_ispidc->inited = 0;
		return 0;
	case MPUCMD_ISP_SET_SDE:
        isp_set_sde(arg); 
        return 0;
	case MPUCMD_ISP_SET_WDR:
		isp_set_wdr(arg);
		return 0;
	case MPUCMD_ISP_SET_MCTF:
		isp_set_mctf(arg);
		return 0;
	case MPUCMD_ISP_SET_SHARPNESS:
		isp_set_sharpness(arg);
        return 0;
	case MPUCMD_ISP_SET_BRIGHTNESS:
		//TODO: set brightness, value: arg
		isp_set_brightness(arg);
		return 0;
	case MPUCMD_ISP_SET_AUTOFPS:
		return isp_set_auto_fps(dpm_ispidc, arg);
	case MPUCMD_ISP_SET_DAYNIGHT:
	{
		if((g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE)&&(g_libdatapath_cfg->fastboot_stage < FAST_STAGE_FPS2START)){
			if(dpm_ispidc)
			{
				dpm_ispidc->isp_mode = arg;
				dpm_ispidc->isp_setmode = arg;
			}
			return 0;
		}
		dpm_ispidc->isp_setmode = arg;

		if((ISPCM_RGB_COLOR == arg) || (ISPCM_IR_BW == arg)){
			isp_set_colormode(dpm_ispidc, arg);
		}
		else if(ISPCM_AUTO == arg){
			isp_daynight_auto_init(dpm_ispidc);
		}
		else{
			return -1;
		}
		return 0;
	}
	case MPUCMD_ISP_GET_LUMADDR:
		return ((u32)(&dpm_ispidc->lum) | BIT31);
	case MPUCMD_ISP_SET_AEC_MANUAL_EN:
		g_pControl_L->bAECManualEnable = arg;
		return 0;
	case MPUCMD_ISP_SET_AEC_MANUAL_GAIN:
		g_pControl_L->nAECManualGain= arg;
		return 0;
	case MPUCMD_ISP_SET_AEC_MANUAL_EXP:
		g_pControl_L->nAECManualExp = arg;
		return 0;
	case MPUCMD_ISP_SET_AWB_MANUAL_EN:
		return isp_set_awb_manual(arg);
	case MPUCMD_ISP_GET_AWB_B_GAIN:
		return isp_get_awb_b_gain();
	case MPUCMD_ISP_GET_AWB_G_GAIN:
		return isp_get_awb_g_gain();
	case MPUCMD_ISP_GET_AWB_R_GAIN:
		return isp_get_awb_r_gain();
	case MPUCMD_ISP_ISPOUT_SEL:
		return ispidc_update_channel(isp_id, arg);
	case MPUCMD_ISP_SET_EXP:
		//TODO
		break;
	case MPUCMD_ISP_SET_GAIN:
		//TODO
		break;
	case MPUCMD_ISP_SET_AWB_B_GAIN:
		//TODO
		break;
	case MPUCMD_ISP_SET_AWB_G_GAIN:
		//TODO
		break;
	case MPUCMD_ISP_SET_AWB_R_GAIN:
		//TODO
		break;
	case MPUCMD_ISP_GET_EXP:
		return g_tMiddle_L.pExpBuf[1]; 
	case MPUCMD_ISP_GET_GAIN:
		return g_tMiddle_L.pGainBuf[1];
	case MPUCMD_ISP_GET_MEANY:
		return g_tMiddle_L.nMeanY;
	case MPUCMD_ISP_SCCBRD:
	{
		unsigned char temp[4];
		temp[0] = (arg >> 8) & 0xff;
		temp[1] = (arg) & 0xff;
		temp[2] = temp[3] = 0;
		if(isp_id == 0)
		{
			ReadI2CRegister(g_pControl_L->nSensorI2COption, g_pControl_L->nSensorDeviceID, temp, 1);
		}
		else if(isp_id == 1 && dpm_sensor2)
		{
			ReadI2CRegister(g_pControl_R->nSensorI2COption, g_pControl_R->nSensorDeviceID, temp, 1);
		}
		return temp[2];
	}
	case MPUCMD_ISP_SCCBWR:
	{
		unsigned char temp[4];
		temp[0] = (arg >> 16) & 0xff;
		temp[1] = (arg >> 8) & 0xff;
		temp[2] = arg&0xff;
		if(isp_id == 0)
		{
			WriteI2CRegister(g_pControl_L->nSensorI2COption, g_pControl_L->nSensorDeviceID, temp, 1);
		}
		else if(isp_id == 1 && dpm_sensor2)
		{
			WriteI2CRegister(g_pControl_R->nSensorI2COption, g_pControl_R->nSensorDeviceID, temp, 1);
		}
		return temp[2];
	}
	case MPUCMD_ISP_SET_CALI_AWB:
	{
		return dpm_sensor_effect((t_dpm *)dpm_ispidc, SNR_EFFECT_WBCALI, arg);
	}
	case MPUCMD_ISP_SET_CALI_LENC_A:
	{
		if(isp_id == 0)
		{
			memcpy(isp_lenc_a, (u8*)arg, 768);
			memcpy((u8*)(L_BASE_ADDRESS+0x700), (u8*)arg, 768);
		}
		else if(isp_id == 1 && dpm_sensor2)
		{
			memcpy((u8*)(R_BASE_ADDRESS+0x700), (u8*)arg, 768);
		}
		return 0;
	}
	case MPUCMD_ISP_SET_CALI_LENC_C:
	{
		if(isp_id == 0)
		{
			memcpy(isp_lenc_c, (u8*)arg, 768);
			memcpy((u8*)(L_BASE_ADDRESS+0xa00), (u8*)arg, 768);
		}
		else if(isp_id == 1 && dpm_sensor2)
		{
			memcpy((u8*)(R_BASE_ADDRESS+0xa00), (u8*)arg, 768);
		}
		return 0;
	}
	case MPUCMD_ISP_SET_CALI_LENC_D:
	{
		if(isp_id == 0)
		{
			memcpy(isp_lenc_d, (u8*)arg, 768);
			memcpy((u8*)(L_BASE_ADDRESS+0xd00), (u8*)arg, 768);
		}
		else if(isp_id == 1 && dpm_sensor2)
		{
			memcpy((u8*)(R_BASE_ADDRESS+0xd00), (u8*)arg, 768);
		}
		return 0;
	}
	case MPUCMD_ISP_GET_RGBIR_CGAIN:
	{
		return ((ReadReg32(0xc0038c30) & 0x3ff)>>1);
	}
	case MPUCMD_ISP_SET_SAT_THR:
	{
		if(dpm_ispidc->dpm_rgbir == NULL)
		{
			return -1;
		}
		if(arg > 0x110 || arg < dpm_ispidc->dpm_rgbir->isp_coeff_thr2)
		{
			dpm_ispidc->dpm_rgbir->isp_sat_thr1 = arg; 
		}
		else if(arg >= dpm_ispidc->dpm_rgbir->isp_coeff_thr2)
		{
			dpm_ispidc->dpm_rgbir->isp_sat_thr1 = dpm_ispidc->dpm_rgbir->isp_coeff_thr2; 
		}
		else if(arg <= 0x110)
		{
			dpm_ispidc->dpm_rgbir->isp_sat_thr1 = 0x110; 
		}
		syslog(LOG_INFO,"saturation_threshold=%x\n", dpm_ispidc->dpm_rgbir->isp_sat_thr1);
		
		return 0;
	}
	case MPUCMD_ISP_SET_COEFF_THR:
	{
		if(dpm_ispidc->dpm_rgbir == NULL)
		{
			return -1;
		}
		dpm_ispidc->dpm_rgbir->isp_coeff_thr1 = (arg&0xffff0000) >> 16; 
		dpm_ispidc->dpm_rgbir->isp_coeff_thr2 = arg&0xffff; 
		syslog(LOG_INFO,"coeff_thr_low:%x, thr_high:%x\n", dpm_ispidc->dpm_rgbir->isp_coeff_thr1, dpm_ispidc->dpm_rgbir->isp_coeff_thr2);
		return 0;
	}
	case MPUCMD_ISP_SET_COEFF:
	{
		if(dpm_ispidc->dpm_rgbir == NULL)
		{
			return -1;
		}
		dpm_ispidc->dpm_rgbir->isp_coeff_value = arg;	
		syslog(LOG_INFO,"set coeff:%x\n", arg);
		return 0;
	}
	case MPUCMD_ISP_SET_HDR:
	{
		dpm_ispidc->hdr_mode = arg;
		dpm_ispidc->hdr_switch_signal = ISPIDC_HDR_SWITCH_MODE;
		return 0;
	}
	case MPUCMD_ISP_GET_AEC_STABLE:
	{
//		syslog(LOG_ERR, "aec stable %d\n", g_tMiddle_L.bRealAECStableFlagBuf);
		return g_tMiddle_L.bRealAECStableFlagBuf;
	}
	case MPUCMD_ISP_GET_HIST:
		return isp_get_hist_buf((t_dpm *)dpm_ispidc);
	case MPUCMD_ISP_SET_ROI:
		return isp_set_roi((t_dpm *)dpm_ispidc, arg);
	case MPUCMD_ISP_GET_DAYNIGHT:
		return dpm_ispidc->isp_mode;
	case MPUCMD_ISP_GET_HDR:
		return dpm_ispidc->hdr_mode; 
	case MPUCMD_ISP_GET_HDR_STABLE:
		if(dpm_ispidc->hdr_switch_wait_frm == 0){
			return 1;
		}else{
			return 0;
		}
	default:
		return 1;
	}
	return 1;
}

void Irq_ISP_EOF_L_(void)
{
	t_dpm_ispidc *dpm;
	dpm = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[0];
	if(dpm){
		if(dpm->drop_frst_ispeof){
			dpm->drop_frst_ispeof = 0;
			return;
		}
	}
	
	if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE){
		if(g_libdatapath_cfg->fastboot_stage > FAST_STAGE_FPS2RDY){
			if(g_libdatapath_cfg->fastboot_cur_fps2cnt < 100){
				g_libdatapath_cfg->fastboot_cur_fps2cnt++;
			}
		}
	}
	if((g_libdatapath_cfg->fastboot_flag&DATAPATH_FASTBOOT_EN) &&!(g_libdatapath_cfg->fastboot_flag &DATAPATH_FASTBOOT_2STAGE)){
		if(g_libdatapath_cfg->fastboot_stage > FAST_STAGE_INITED){
			if(g_libdatapath_cfg->fastboot_cur_fps2cnt < 100){
				g_libdatapath_cfg->fastboot_cur_fps2cnt++;
			}
		}
	}

	Irq_ISP_EOF_L();

	g_libdatapath_cfg->isp_EOF_flag = 1;
}

void isp_switch_hdr(t_dpm_ispidc * dpm)
{
	if(dpm->hdr_switch_signal == ISPIDC_HDR_SWITCH_MODE){
		dpm->hdr_switch_frm_cnt = 0;
		dpm->hdr_switch_wait_frm = 0;
		dpm_sensor_effect(dpm, SNR_EFFECT_HDR, dpm->hdr_mode);
		dpm->hdr_switch_signal = 0;
	}

	if(dpm->hdr_switch_wait_frm > 0){
		if(dpm->hdr_switch_frm_cnt == dpm->hdr_switch_wait_frm){
			dpm_sensor_effect(dpm, SNR_EFFECT_HDR, dpm->hdr_mode);
		}
		dpm->hdr_switch_frm_cnt++;
	}
}

extern void libisp_fast_eof_handler(void);
static int libisp_process(void)
{
	t_dpm_ispidc *dpm;
	dpm = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[0];
	if(dpm == NULL){
		return 0;
	}

	if(! dpm->inited){
		return 0;
	}

	RunTasks();

	if(g_libdatapath_cfg->isp_EOF_flag == 1)
	{
		g_libdatapath_cfg->isp_EOF_flag = 0;

		isp_lum_update(dpm);

		if(dpm->hist_status == 1){
			isp_hist_update(dpm);
			dpm->hist_status = 2;
		}

		if(dpm->add_meta_en){
			t_dpm_ve * dpm_vs = NULL;
			if(g_libdatapath_cfg->vs_map[0]){
				dpm_vs = (t_dpm_ve *)(g_libdatapath_cfg->vs_map[0]);
			}
			if(dpm_vs && dpm_vs->type == DPM_TYPE_VE){
				libvs_add_meta_int(0, "FNUM", dpm_vs->frm_cnt);
			}
			u8 string_buf[32];
			sprintf((char *)string_buf, "0x%x", g_pControl_L->nAECManualExp);
			libvs_add_meta_string(0, "LEXP", string_buf);
			sprintf((char *)string_buf, "0x%x", g_pControl_R->nAECManualExp);
			libvs_add_meta_string(0, "SEXP", string_buf);
			sprintf((char *)string_buf, "0x%x", g_pControl_L->nAECManualGain);
			libvs_add_meta_string(0, "LGAIN", string_buf);
			sprintf((char *)string_buf, "0x%x", g_pControl_R->nAECManualGain);
			libvs_add_meta_string(0, "SGAIN", string_buf);

			libvs_add_meta_int(0, "Yavg", g_tMiddle_L.nMeanY);	
			libvs_add_meta_int(0, "rg", g_pControl_L->pManualAWBGain[0][0]);	
			libvs_add_meta_int(0, "rb", g_pControl_L->pManualAWBGain[0][2]);	
		}

		if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_EN)
		{
			//check if fps1 done
			libisp_fast_eof_handler();

			//if enable auto AWB earlier,the third and fourth frames will be green.fixed for tmp
 			if(g_libdatapath_cfg->fastboot_flag & DATAPATH_FASTBOOT_2STAGE){
				if(g_libdatapath_cfg->fastboot_cur_fps2cnt == 2)
				{
					g_pControl_L->bManualAWBEnable =0;
					g_pControl_L->bAECManualEnable =0;
					if(ReadReg32(0xe0089200) & BIT20){	// combine enable
						g_pControl_R->bManualAWBEnable =0;
						g_pControl_R->bAECManualEnable =0;
					}
				}
			}
			else
			{
				if(g_libdatapath_cfg->fastboot_cur_fps2cnt == 4)//yavg goes to stable in 4th frame
				{
					g_pControl_L->bAECManualEnable =0;
					if(ReadReg32(0xe0089200) & BIT20){	// combine enable
						g_pControl_R->bAECManualEnable =0;
					}
				}
				if(g_libdatapath_cfg->fastboot_cur_fps2cnt == 2)
				{
					g_pControl_L->bManualAWBEnable =0;
					if(ReadReg32(0xe0089200) & BIT20){	// combine enable
						g_pControl_R->bManualAWBEnable =0;
					}
				}
			}
		}
		//do RGBIR desat/halfcoeff mode check.
		if(dpm->dpm_rgbir && dpm->dpm_rgbir->ir_check_en && dpm->isp_mode != ISPCM_IR_BW)
		{
			isp_ir_check(dpm, dpm->dpm_rgbir);
		}

		//day/night switch check
		Do_DayNightSwitch(dpm);

		// switch hdr
		isp_switch_hdr(dpm);
	}

	return 0;

	if(g_pControl_L->bAutoFrameRateEnable == 1)
	{
		isp_frm_update(dpm);
	}

	return 0;
}

#define TASK_STACKSIZE 2048
static u32 _task_stack_isp[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_isp_cfg = {
	.name = "isp",
	.prio = 5,
	.stk = _task_stack_isp,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_ISP,
	.app_cmdhandler = libisp_handle_cmd,
	.app_process = libisp_process,
};

int isp_irq_handler(void (* real_handler)(void) )
{
	if(real_handler){
		real_handler();
	}
	task_signal(&g_ba2app_isp_cfg);

	return 0;
}

int libisp_irq_init(void)
{
	irq_request(IRQ_BIT_ISPDMADONE,		(t_irq_handler)isp_irq_handler,	Irq_DMADone);
	irq_request(IRQ_BIT_ISPPDDONE,		(t_irq_handler)isp_irq_handler,	Irq_PD_DETECTION_DONE);
	irq_request(IRQ_BIT_ISPVSYNCL,		(t_irq_handler)isp_irq_handler,	Irq_VSYNC_L);
	irq_request(IRQ_BIT_ISPVSYNCR,		(t_irq_handler)isp_irq_handler,	Irq_VSYNC_R);
	irq_request(IRQ_BIT_ISPSOFL,		(t_irq_handler)isp_irq_handler,	Irq_ISP_SOF_L);
	irq_request(IRQ_BIT_ISPSOFR,		(t_irq_handler)isp_irq_handler,	Irq_ISP_SOF_R);
	irq_request(IRQ_BIT_ISPEOFL,		(t_irq_handler)isp_irq_handler,	Irq_ISP_EOF_L_);
	irq_request(IRQ_BIT_ISPEOFR,		(t_irq_handler)isp_irq_handler,	Irq_ISP_EOF_R);
	irq_request(IRQ_BIT_ISPLINE,		(t_irq_handler)isp_irq_handler,	Irq_LINE_INT);
	irq_request(IRQ_BIT_ISPAECDONEL,	(t_irq_handler)isp_irq_handler,	Irq_AECDone_L);
	irq_request(IRQ_BIT_ISPAECDONER,	(t_irq_handler)isp_irq_handler,	Irq_AECDone_R);
	irq_request(IRQ_BIT_ISPLENSONDONEL,	(t_irq_handler)isp_irq_handler,	Irq_LENSON_DONE_L);
	irq_request(IRQ_BIT_ISPLENSONDONER,	(t_irq_handler)isp_irq_handler,	Irq_LENSON_DONE_R);
	irq_request(IRQ_BIT_ISPSTATL,		(t_irq_handler)isp_irq_handler,	Irq_ISP_Stat_L);
	irq_request(IRQ_BIT_ISPSTATR,		(t_irq_handler)isp_irq_handler,	Irq_ISP_Stat_R);

	return 0;
}

