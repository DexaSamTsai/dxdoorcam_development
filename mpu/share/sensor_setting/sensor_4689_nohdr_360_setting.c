#include "includes.h"

#define LIGHT_FREQ_50HZ 1
#define SENSOR_2A   0xf8//0x98
#define SENSOR_2B   0x06//0x07
#define BAND50HZ    0x2500//0x0128
#define BAND60HZ    0x1ee0//0x00f7

/// SCCB BUS setting for L and R channels
#define L_CH_SCCB_NUM		SCCB_NUM_0

#ifdef CONFIG_SET_RIGHT_CH_SCCB_ID
#define R_CH_SCCB_NUM		CONFIG_RIGHT_CH_SCCB_ID // SCCB_NUM_3
#else
#define R_CH_SCCB_NUM		SCCB_NUM_2
#endif

#define SENSOR_NAME_INTERNAL           4689_1
#define SENSOR_SCCB_MODE_INTERNAL      SCCB_MODE16
#if (R_CH_SCCB_NUM == L_CH_SCCB_NUM)
#define SENSOR_SCCB_ID_INTERNAL        0x20
#else
#define SENSOR_SCCB_ID_INTERNAL        0x42
#endif
#define SENSOR_DATAFMT_INTERNAL        DPM_DATAFMT_RAW10
#define SENSOR_INTERFACETYPE_INTERNAL  SNR_IF_MIPI_2LN

#define SENSOR_NAME_INTERNAL2          4689_2
#define SENSOR_SCCB_MODE_INTERNAL2     SCCB_MODE16
#if (R_CH_SCCB_NUM == L_CH_SCCB_NUM)
#define SENSOR_SCCB_ID_INTERNAL2       0x6C
#else
#define SENSOR_SCCB_ID_INTERNAL2        0x42
#endif
#define SENSOR_DATAFMT_INTERNAL2       DPM_DATAFMT_RAW10
#define SENSOR_INTERFACETYPE_INTERNAL2 SNR_IF_MIPI_2LN

//#define SENSOR_30FPS
#define TWO_ISP_CH

#define SENSOR_CROP_1536_1440	// This sensor output resolution is 1536x1440

#ifdef CONFIG_SENSOR_FLIP_ON
#define SENSOR_UPSIDE_DOWN
#endif

/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

/*************************
  Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {
//RES 2688_1520_2lane_MIPI648M_PCLK120M_30fps
{0x0103 ,0x01},
{0x3638 ,0x00},
{0x0300 ,0x00},
{0x0302 ,0x1b},//19
{0x0303 ,0x00},
{0x0304 ,0x03},
{0x030b ,0x00},
{0x030d ,0x1e},
{0x030e ,0x04},
{0x030f ,0x01},
{0x0312 ,0x01},
{0x031e ,0x00},
{0x3000 ,0x20},
{0x3002 ,0x00},
{0x3018 ,0x32},//12
{0x3019 ,0x0c},//12
{0x3020 ,0x93},
{0x3021 ,0x03},
{0x3022 ,0x01},
{0x3031 ,0x0a},
{0x303f ,0x0c},
{0x3103 ,0x04},//4
{0x3305 ,0xf1},
{0x3307 ,0x04},
{0x3309 ,0x29},
{0x3500 ,0x00},
{0x3501 ,0x5d},
{0x3502 ,0x50},
{0x3503 ,0x04},
{0x3504 ,0x00},
{0x3505 ,0x00},
{0x3506 ,0x00},
{0x3507 ,0x00},
{0x3508 ,0x01},
{0x3509 ,0xa0},
{0x350a ,0x00},
{0x350b ,0x00},
{0x350c ,0x00},
{0x350d ,0x00},
{0x350e ,0x00},
{0x350f ,0x80},
{0x3510 ,0x00},
{0x3511 ,0x00},
{0x3512 ,0x00},
{0x3513 ,0x00},
{0x3514 ,0x00},
{0x3515 ,0x80},
{0x3516 ,0x00},
{0x3517 ,0x00},
{0x3518 ,0x00},
{0x3519 ,0x00},
{0x351a ,0x00},
{0x351b ,0x80},
{0x351c ,0x00},
{0x351d ,0x00},
{0x351e ,0x00},
{0x351f ,0x00},
{0x3520 ,0x00},
{0x3521 ,0x80},
{0x3522 ,0x08},
{0x3524 ,0x08},
{0x3526 ,0x08},
{0x3528 ,0x08},
{0x352a ,0x08},
{0x3602 ,0x00},
{0x3603 ,0x40},
{0x3604 ,0x02},
{0x3605 ,0x00},
{0x3606 ,0x00},
{0x3607 ,0x00},
{0x3609 ,0x12},
{0x360a ,0x40},
{0x360c ,0x08},
{0x360f ,0xe5},
{0x3608 ,0x8f},
{0x3611 ,0x00},
{0x3613 ,0xf7},
{0x3616 ,0x58},
{0x3619 ,0x99},
{0x361b ,0x60},
{0x361c ,0x7a},
{0x361e ,0x79},
{0x361f ,0x02},
{0x3632 ,0x00},
{0x3633 ,0x10},
{0x3634 ,0x10},
{0x3635 ,0x10},
{0x3636 ,0x15},
{0x3646 ,0x86},
{0x364a ,0x0b},
{0x3700 ,0x17},
{0x3701 ,0x22},
{0x3703 ,0x10},
{0x370a ,0x37},
{0x3705 ,0x00},
{0x3706 ,0x63},
{0x3709 ,0x3c},
{0x370b ,0x01},
{0x370c ,0x30},
{0x3710 ,0x24},
{0x3711 ,0x0c},
{0x3716 ,0x00},
{0x3720 ,0x28},
{0x3729 ,0x7b},
{0x372a ,0x84},
{0x372b ,0xbd},
{0x372c ,0xbc},
{0x372e ,0x52},
{0x373c ,0x0e},
{0x373e ,0x33},
{0x3743 ,0x10},
{0x3744 ,0x88},
{0x3745 ,0xc0},
{0x374a ,0x43},
{0x374c ,0x00},
{0x374e ,0x23},
{0x3751 ,0x7b},
{0x3752 ,0x84},
{0x3753 ,0xbd},
{0x3754 ,0xbc},
{0x3756 ,0x52},
{0x375c ,0x00},
{0x3760 ,0x00},
{0x3761 ,0x00},
{0x3762 ,0x00},
{0x3763 ,0x00},
{0x3764 ,0x00},
{0x3767 ,0x04},
{0x3768 ,0x04},
{0x3769 ,0x08},
{0x376a ,0x08},
{0x376b ,0x20},
{0x376c ,0x00},
{0x376d ,0x00},
{0x376e ,0x00},
{0x3773 ,0x00},
{0x3774 ,0x51},
{0x3776 ,0xbd},
{0x3777 ,0xbd},
{0x3781 ,0x18},
{0x3783 ,0x25},
{0x3798 ,0x1b},
#ifdef SENSOR_CROP_1536_1440
{0x3800	,0x02},
{0x3801	,0x48},
{0x3802	,0x00},
{0x3803	,0x2c},
{0x3804	,0x08},
{0x3805	,0x57},
{0x3806	,0x05},
{0x3807	,0xd3},
{0x3808	,0x06},
{0x3809	,0x00},
{0x380A	,0x05},
{0x380B	,0xa0},
{0x380c ,0x0b},
{0x380d ,0x40},
{0x380e ,0x06},
{0x380f ,0x30},
{0x3810 ,0x00},
{0x3811 ,0x08},
{0x3812 ,0x00},
{0x3813 ,0x04},
#else
{0x3800	,0x00},
{0x3801	,0x08},
{0x3802	,0x00},
{0x3803	,0x04},
{0x3804	,0x0A},
{0x3805	,0x97},
{0x3806	,0x05},
{0x3807	,0xFB},
{0x3808	,0x0A},
{0x3809	,0x80},
{0x380A	,0x05},
{0x380B	,0xF0},
{0x380c ,0xa1},
{0x380d ,0xe0},
{0x380e ,0x06},
{0x380f ,0x30},
{0x3810 ,0x00},
{0x3811 ,0x08},
{0x3812 ,0x00},
{0x3813 ,0x04},
#endif
{0x3814 ,0x01},
{0x3815 ,0x01},
{0x3819 ,0x01},
#ifdef SENSOR_UPSIDE_DOWN
{0x3820 ,0x06}, // bit[1]: array horizontal flip, bit[2]: digital hor. flip
{0x3821 ,0x00}, // Normal
#else
{0x3820 ,0x00}, // Normal
{0x3821 ,0x06}, // bit[1]: array horizontal mirror, bit[2]: digital hor. mirror
#endif
{0x3829 ,0x00},
{0x382a ,0x01},
{0x382b ,0x01},
{0x382d ,0x7f},
{0x3830 ,0x04},
{0x3836 ,0x01},
{0x3837 ,0x00},
{0x3841 ,0x02},
{0x3846 ,0x08},
{0x3847 ,0x07},
{0x3d85 ,0x36},
{0x3d8c ,0x71},
{0x3d8d ,0xcb},
{0x3f0a ,0x00},
{0x4000 ,0xF1},
{0x4001 ,0x40},
{0x4002 ,0x04},
{0x4003 ,0x14},
{0x400e ,0x00},
{0x4011 ,0x00},
{0x401a ,0x00},
{0x401b ,0x00},
{0x401c ,0x00},
{0x401d ,0x00},
{0x401f ,0x00},
#ifdef SENSOR_CROP_1536_1440
{0x4020	,0x00},
{0x4021	,0x10},
{0x4022	,0x04},
{0x4023	,0x93},
{0x4024	,0x05},
{0x4025	,0xc0},
{0x4026	,0x05},
{0x4027	,0xd0},
#else
{0x4020	,0x00},
{0x4021	,0x10},
{0x4022	,0x09},
{0x4023	,0x13},
{0x4024	,0x0A},
{0x4025	,0x40},
{0x4026	,0x0A},
{0x4027	,0x50},
#endif
{0x4028 ,0x00},
{0x4029 ,0x02},
{0x402a ,0x06},
{0x402b ,0x04},
{0x402c ,0x02},
{0x402d ,0x02},
{0x402e ,0x0e},
{0x402f ,0x04},
{0x4302 ,0xff},
{0x4303 ,0xff},
{0x4304 ,0x00},
{0x4305 ,0x00},
{0x4306 ,0x00},
{0x4308 ,0x02},
{0x4500 ,0x6c},
{0x4501 ,0xc4},
{0x4502 ,0x40},
{0x4503 ,0x01},
#ifdef SENSOR_CROP_1536_1440
{0x4601 ,0x5f},
#else
{0x4601 ,0x42},
#endif
{0x4800 ,0x04},
{0x4813 ,0x08},
{0x481f ,0x40},
{0x4829 ,0x78},
{0x4837 ,0x18},
{0x4b00 ,0x2a},
{0x4b0d ,0x00},
{0x4d00 ,0x04},
{0x4d01 ,0x42},
{0x4d02 ,0xd1},
{0x4d03 ,0x93},
{0x4d04 ,0xf5},
{0x4d05 ,0xc1},
{0x5000 ,0xf3},
{0x5001 ,0x11},
{0x5004 ,0x00},
{0x500a ,0x00},
{0x500b ,0x00},
{0x5032 ,0x00},
{0x5040 ,0x00},
{0x5050 ,0x0c},
{0x5500 ,0x00},
{0x5501 ,0x10},
{0x5502 ,0x01},
{0x5503 ,0x0f},
{0x8000 ,0x00},
{0x8001 ,0x00},
{0x8002 ,0x00},
{0x8003 ,0x00},
{0x8004 ,0x00},
{0x8005 ,0x00},
{0x8006 ,0x00},
{0x8007 ,0x00},
{0x8008 ,0x00},
{0x3638 ,0x00},
#ifdef SENSOR_30FPS
{0x380c, 0x0a},
{0x380d, 0x00},
#else
//{0x380c, 0x0b},
//{0x380d, 0x40},	//26fps:0xb40
{0x380c, 0x0c},
{0x380d, 0x80}, //24fps:0xc80
//{0x380c, 0x1e},
//{0x380d, 0x0c}, //10fps(test only, not accurate):0x1e0C
#endif

//{0x380c, 0x0d},
//{0x380d, 0xa2},
//{0x380c, 0x0f},
//{0x380d, 0x00},
//{0x5040, 0x80},///< color bar

//{0x0100 ,0x01},
};

/*************************
  setting for different size and framerate
 *************************/

SENSOR_SETTING_TABLE sensor__size_1280_720[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_30[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_25[][2] = {
};
SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_20[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_15[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_12[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_10[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_5[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_3[][2] = {
};

/*************************
  effect change IOCTL
 *************************/
static u8 last_changed = 0;

SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
    switch(effect){
	case SNR_EFFECT_SHARPNESS:
		return 0;
    case SNR_EFFECT_SIZE:
        switch(level){
        case VIDEO_SIZE_720P:
            cfg->cur_video_size = VIDEO_SIZE_720P;
            last_changed = 1;
            break;
        default:
            return -1;
        }
        return 0;
    case SNR_EFFECT_FRMRATE:
        last_changed = 1;
        cfg->cur_frame_rate = level;
        return 0;
    case SNR_EFFECT_PROC:
        if(last_changed == 0){
            return 0;
        }
        /*SENSOR_SETTING_SUPPORT_START
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 30)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 25)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 20)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 15)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 10)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 5)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 3)
        SENSOR_SETTING_SUPPORT_END*/
        last_changed = 0;
        return 0;
    default:
        return -2;
    }
}

/*************************
  effect description table
  { EFFECT_NAME, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] =
{
    {SNR_EFFECT_HUE, 0x00, 0x0c, 0x06, 0x06},
    {SNR_EFFECT_SHARPNESS, 0x00, 0x09, 0x03, 0x03},
    {SNR_EFFECT_BRIGHT, 0x01, 0xff, 0x80, 0x80},
    {SNR_EFFECT_SATURATION, 0x00, 0x08, 0x02, 0x02},
    {SNR_EFFECT_CONTRAST, 0x00, 0x08, 0x03, 0x03},
};



/*************************
  sensor detect func
 *************************/
#define OV4689_SENSOR_ID_ADR_HI 0x300a
#define OV4689_SENSOR_ID_ADR_LO 0x300b
#define OV4689_SENSOR_ID_VAL_HI 0x46
#define OV4689_SENSOR_ID_VAL_LO 0x88

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
	debug_printf("%s detect! - 0x%x, 0x%x\n", cfg->name, cfg->sccb_id, cfg->sccb_cfg->id);
	int hi = libsccb_rd(cfg->sccb_cfg, OV4689_SENSOR_ID_ADR_HI);
	int lo = libsccb_rd(cfg->sccb_cfg, OV4689_SENSOR_ID_ADR_LO);
#ifdef SENSOR_CROP_1536_1440
	cfg->cur_video_size = (1536<<16) | 1440;
#else
	cfg->cur_video_size = (2688<<16) | 1520;
#endif
	debug_printf("hi = %x,%x,%x ---- 4M\n", hi, lo);

	return 0;
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	return 0;	
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	//debug_printf("\nL_BASE_ADDRESS: 0x%x\n",L_BASE_ADDRESS);
	//debug_printf("\nR_BASE_ADDRESS: 0x%x\n",R_BASE_ADDRESS);

// ================ DMA ================
#ifdef TWO_ISP_CH
#define SEPARATE_LENC
//0xe009001c: dma group7 instruction address = R_BASE_ADDRESS+1048
//0xe0090024: dma instruction number of group1/2/3/4
//0xe0090028: dma instruction number of group5/6/7/8
//0xe009003c: dma group enable
#ifdef SEPARATE_LENC
#define LENC_SETTING_R
	//WriteReg32(0xe009001c, 0x00036e88);
	WriteReg32(0xe009001c, (R_BASE_ADDRESS&0x000fffff)+0x1048);
	WriteReg32(0xe0090024, 0x00030201);
	WriteReg32(0xe0090028, 0x00010001);
#else
	WriteReg32(0xe0090024, 0x00040201);
	WriteReg32(0xe0090028, 0x00000001);
#endif
#else
	WriteReg32(0xe0090024, 0x00020201);
	WriteReg32(0xe0090028, 0x00020201);
#endif
	WriteReg32(0xe009003c, 0xffff3377);	//[31:24]dest endean,[23:16]orig endean,[15:8]repeat mode,[7:0]enable
	WriteReg32(0xe0090040, 0x00000033);	// group enable
	//WriteReg32(0xe0090064, 0x00000400);	// group5 trigger by isp1_all_stat_done

	WriteReg32(ISP_BASE_ADDR + 0x800c,ReadReg32(ISP_BASE_ADDR + 0x800c)|BIT17);
	WriteReg32(ISP_BASE_ADDR + 0xe00c,ReadReg32(ISP_BASE_ADDR + 0xe00c)|BIT17);
	WriteReg32(ISP_BASE_ADDR + 0x9204, 0x00000000);

	// pre pipe top
	WriteReg32(ISP_BASE_ADDR + 0x8000, 0x8a7f0100);
	WriteReg32(ISP_BASE_ADDR + 0xc000, 0x8a7f0100);
	// 00-[7]  RAW_DNS_En
	// 00-[6]  BinC_En
	// 00-[5]  DPC_En
	// 00-[4]  DPCOTP_En
	// 00-[3]  AWBG_En
	// 00-[2]  Lens_Online_En
	// 00-[1]  LENC_En
	// 00-[0]  Pre_Pipe_Manu_En
	// 01-[7] Binc_New_En
	// 01-[6] DNS_3D_En
	// 01-[5] Stretch_En
	// 01-[4] Hist_Stats_En
	// 01-[3] CCM_En
	// 01-[2] AEC_AGC_En
	// 01-[1] Curve_AWB_En
	// 01-[0] CIP_En
	// 02-[7:4] reserved
	// 02-[3] dpc_black_en
	// 02-[2] dpc_white_en
	// 02-[1] pdf_en
	// 02-[0] ca_en
	// 03-[7] Manual_Ctrl_En
	// 03-[6] Manual_Size_En
	// 03-[5] Lenc_mirror
	// 03-[4] Lenc_flip
	// 03-[3:2] reserved
	// 03-[1] Binc_Mirror
	// 03-[0] Binc_Flip

	// post pipe top
	WriteReg32(ISP_BASE_ADDR + 0x9800, 0x3c000010);
	WriteReg32(ISP_BASE_ADDR + 0xd800, 0x3c000010);
	// 00[0]  Manual_Ctrl_En
	// 00[1]  Post_Pipe_Manu_En
	// 00[2]  RGB_Gamma_En
	// 00[3]  JPEG_Ycbcr_En
	// 00[4]  SDE_En
	// 00[5]  UV_DNS_En
	// 00[6]  Anti_Shake_En
	// 00[7]  Reserved

	WriteReg32(0xe0089400, 0x3f000000);	// mid pipe top of L
	WriteReg32(0xe008d400, 0x3f000000);	// mid pipe top of R

	WriteReg32(ISP_BASE_ADDR + 0x8004, 0x00100000);
	WriteReg32(ISP_BASE_ADDR + 0xc004, 0x00100000);

	//WriteReg32(ISP_BASE_ADDR + 0xa1f8, 0x00000000);

// ================ interrupt ================
//48 00063054 00000000; [27:24]select stat done:0-stat all done,1-AWB done,2-hist done

// ================ DMA ================
	//WriteReg32(L_BASE_ADDRESS + 0x0000, 0x058005dc);	// w_in|h_in
	WriteReg32(L_BASE_ADDRESS + 0x0000, (1440<<16)|(1536));	// w_in|h_in
#ifdef TWO_ISP_CH
	WriteReg32(L_BASE_ADDRESS + 0x000c, ((ReadReg32(L_BASE_ADDRESS + 0x000c)&0x00ffffff)));	// Base Address
#else
	WriteReg32(L_BASE_ADDRESS + 0x000c, 0x02000000);	// Base Address
#endif
	WriteReg32(L_BASE_ADDRESS + 0x0570, ((ReadReg32(R_BASE_ADDRESS + 0x0570)&0x00ffffff)|0x1000000));	// Preview Enable

	//WriteReg32(R_BASE_ADDRESS + 0x0000, 0x058005dc);	// w_in|h_in
	WriteReg32(R_BASE_ADDRESS + 0x0000, (1440<<16)|(1536));	// w_in|h_in
#ifdef TWO_ISP_CH
	WriteReg32(R_BASE_ADDRESS + 0x000c, ((ReadReg32(R_BASE_ADDRESS + 0x000c)&0x00ffffff)));	// Base Address
#else
	WriteReg32(R_BASE_ADDRESS + 0x000c, 0x02000000);	// Base Address
#endif
	WriteReg32(R_BASE_ADDRESS + 0x0570, ((ReadReg32(R_BASE_ADDRESS + 0x0570)&0x00ffffff)|0x1000000));	// Preview Enable

// ================ ISP function ================
#define AEC_AGC_SETTING
#define AEC_AGC_SETTING_R
#define RGBH_STRETCH_SETTING
//#define MAN_AWB_SETTING
#define CURVE_AWB_SETTING
#define CCM_SETTING
#ifdef TWO_ISP_CH
#define LENC_SETTING
#endif
//#define LENS_ONLINE_SETTING
#define RAW_DNS_SETTING
#define UV_DNS_SETTING
#define Sharpen_SETTING
#define RGBH_CURVE_SETTING

#ifndef TWO_ISP_CH
#define CURVE_AWB_SETTING_R
#define CCM_SETTING_R
#endif
//#define DEBUG_AEMANUAL_SETTING

#ifdef AEC_AGC_SETTING
// meanY stat(HW)
	WriteReg32(0xe0088b18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe0088b1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe0088b08, 0x20204040);	// left|top|width|height, ratio, bw=7
	WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe0088b30, 0x04020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe0088b34, 0x00000205);	// [12:8]weight12,[4:1]stat sampling,[0]stat brightest channel enable
// hist stat(HW)
	WriteReg32(0xe0088c04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe0088c08, 0x00000011);	// top|bottom, bw=13
// AEC para(FW)
	WriteReg32(L_BASE_ADDRESS + 0x0010, 0x08060100);	// [15:8]sensor gain mode
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00205030);	// [31:24]target low,[23:16]target high
	WriteReg32(L_BASE_ADDRESS + 0x0024, 0x001000ff);	// 01f80010; max gain|min gain
	WriteReg32(L_BASE_ADDRESS + 0x0028, 0x00000700);	// max exposure
	WriteReg32(L_BASE_ADDRESS + 0x002c, 0x00000001);	// min exposure
	//WriteReg32(L_BASE_ADDRESS + 0x0054, 0x09420630);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x09000630|(SENSOR_SCCB_ID_INTERNAL<<16));	// [31:16]VTS,[15:8]device ID,[7:0]I2C option

	WriteReg32(L_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(L_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(L_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(L_BASE_ADDRESS + 0x0064, 0x35093508);	// gain
	WriteReg32(L_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(L_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(L_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(L_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x007c, 0x020000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(L_BASE_ADDRESS + 0x0080, 0x00000101);	//

// banding
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010104);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#ifdef SENSOR_30FPS
	//48 10032044 10a00de0; 60band|50band,fr=30
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x1db318c0);	// 60band|50band,fr=15
#else
//48 10032044 10a00de0; 60band|50band,fr=30
	//WriteReg32(L_BASE_ADDRESS + 0x0044, 0x12ab0f8f);	// 60band|50band,fr=15
	//WriteReg32(L_BASE_ADDRESS + 0x0044, 0x19bd1573);	// 60band|50band,fr=26
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x17c213cc);	// 60band|50band,fr=24
#endif
#endif

#ifdef AEC_AGC_SETTING_R
// meanY stat(HW)
	WriteReg32(0xe008cb18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe008cb1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe008cb08, 0x20204040);	// left|top|width|height, ratio, bw=7
	WriteReg32(0xe008cb28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe008cb2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe008cb30, 0x04020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe008cb34, 0x00000205);	// [12:8]weight12,[4:1]stat sampling,[0]stat brightest channel enable
// hist stat(HW)
	WriteReg32(0xe008cc04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe008cc08, 0x00000011);	// top|bottom, bw=13

// AEC para(FW)
	WriteReg32(R_BASE_ADDRESS + 0x0010, 0x08060100);	// [15:8]sensor gain mode
	WriteReg32(R_BASE_ADDRESS + 0x0014, 0x00205030);	// [31:24]target low,[23:16]target high
	WriteReg32(R_BASE_ADDRESS + 0x0024, 0x001000ff);	// 01f80010; max gain|min gain
	WriteReg32(R_BASE_ADDRESS + 0x0028, 0x00000700);	// max exposure
	WriteReg32(R_BASE_ADDRESS + 0x002c, 0x00000001);	// min exposure
	//WriteReg32(R_BASE_ADDRESS + 0x0054, 0x09420630);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
	WriteReg32(R_BASE_ADDRESS + 0x0054, 0x09000630|(SENSOR_SCCB_ID_INTERNAL2<<16));	// [31:16]VTS,[15:8]device ID,[7:0]I2C option

	WriteReg32(R_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(R_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(R_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(R_BASE_ADDRESS + 0x0064, 0x35093508);	// gain
	WriteReg32(R_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(R_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]
	WriteReg32(R_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(R_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(R_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(R_BASE_ADDRESS + 0x007c, 0x020000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(R_BASE_ADDRESS + 0x0080, 0x00000101);	//

// banding
	WriteReg32(R_BASE_ADDRESS + 0x0040, 0x01010104);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#ifdef SENSOR_30FPS
	//48 100320R4 10a00de0; 60band|50band,fr=30
	WriteReg32(R_BASE_ADDRESS + 0x0044, 0x1db318c0);	// 60band|50band,fr=15
#else
//48 10032044 1Ra00de0; 60band|50band,fr=30
	//WriteReg32(R_BASE_ADDRESS + 0x0044, 0x12ab0f8f);	// 60band|50band,fr=15
	//WriteReg32(R_BASE_ADDRESS + 0x0044, 0x19bd1573);	// 60band|50band,fr=26
	WriteReg32(R_BASE_ADDRESS + 0x0044, 0x17c213cc);	// 60band|50band,fr=24
#endif
#endif

// down framerate
#ifdef MIPI_1LN
	u32 vts = 0x62b;//libsccb_rd(cfg->sccb_cfg,0x380e)<<8 | libsccb_rd(cfg->sccb_cfg,0x380f);
#else
	u32 vts = 0x630;//libsccb_rd(cfg->sccb_cfg,0x380e)<<8 | libsccb_rd(cfg->sccb_cfg,0x380f);
#endif
	WriteReg32(L_BASE_ADDRESS + 0x0028, 0x00000000|(vts-0xc));	// max exposure
 	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x09000000 | vts | (SENSOR_SCCB_ID_INTERNAL<<16));	// [31:16]VTS,[15:8]device ID,[7:0]I2C option

//#ifdef TWO_ISP_CH
	WriteReg32(R_BASE_ADDRESS + 0x0028, 0x00000000|(vts-0xc));	// max exposure
    WriteReg32(R_BASE_ADDRESS + 0x0054, 0x09000000 | vts | (SENSOR_SCCB_ID_INTERNAL2<<16));	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
//#endif

#ifdef RGBH_STRETCH_SETTING
// rgbh stretch : L
	WriteReg32(0xe0088d80, 0x01800100);	// [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d84, 0x01003fff);	// [29:16]manual low level, [13:0] min high level
	WriteReg32(0xe0088d88, 0x04010804);	//
	WriteReg32(0xe0088d8c, 0x00010400);	//
// rgbh stretch : R
	WriteReg32(0xe008cd80, ReadReg32(ISP_BASE_ADDR + 0x8d80));
	WriteReg32(0xe008cd84, ReadReg32(ISP_BASE_ADDR + 0x8d80));
	WriteReg32(0xe008cd88, ReadReg32(ISP_BASE_ADDR + 0x8d80));
	WriteReg32(0xe008cd8c, ReadReg32(ISP_BASE_ADDR + 0x8d80));
#endif

#ifdef MAN_AWB_SETTING
// manual AWB gain
	WriteReg32(L_BASE_ADDRESS + 0x01ac, 0x08000000);	// [31:24]scale bits,[23:16]manualAWB_en
	WriteReg32(L_BASE_ADDRESS + 0x00a0, 0x00aa0080);	// b_gain|g_gain
	WriteReg32(L_BASE_ADDRESS + 0x00a4, 0x00f80080);	// [31:16]r_gain
	WriteReg32(L_BASE_ADDRESS + 0x0094, 0x00000000);	// b_offset|gb_offset|gr_offset|r_offset,x128 when applied
#endif

#ifdef CURVE_AWB_SETTING
	g_pControl_L->bROIEnable = 1;
// AWB stat : L
	WriteReg32(0xe0088a00, 0x00000000);	// [0]GW
	WriteReg32(0xe0088a0c, 0x000e03e0);	// 000e03e0; min_stat_val|max_stat_val,bw=10 (?)
	WriteReg32(0xe0088a10, 0x00000001);	// step=1(downsample=2)
// AWB stat : R
	WriteReg32(0xe008ca00 , ReadReg32(ISP_BASE_ADDR + 0x8a00));
	WriteReg32(0xe008ca0c , ReadReg32(ISP_BASE_ADDR + 0x8a0c));
	WriteReg32(0xe008ca10 , ReadReg32(ISP_BASE_ADDR + 0x8a10));

// AWB calc
	WriteReg32(L_BASE_ADDRESS + 0x008c, 0x020003ff);	// maxBgain|maxGBgain
	WriteReg32(L_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10050000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
	//WriteReg32(L_BASE_ADDRESS + 0x019c, 0x02d000e0);	// brightness threshold of AWB map switch,bw=16, t0|t1
#if 0
	//X_OFF & Y_OFF
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfe18fe53);
	//KX & KY
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x0013ff69);
	//LowMap
	WriteReg32(L_BASE_ADDRESS + 0x00d0, 0x00033355);
	WriteReg32(L_BASE_ADDRESS + 0x00d4, 0x53333000);
	WriteReg32(L_BASE_ADDRESS + 0x00d8, 0x003333cc);
	WriteReg32(L_BASE_ADDRESS + 0x00dc, 0xc3333330);
	WriteReg32(L_BASE_ADDRESS + 0x00e0, 0x00333ccc);
	WriteReg32(L_BASE_ADDRESS + 0x00e4, 0xc333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00e8, 0x003ccccc);
	WriteReg32(L_BASE_ADDRESS + 0x00ec, 0x3333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00f0, 0x03ccccc3);
	WriteReg32(L_BASE_ADDRESS + 0x00f4, 0x3333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00f8, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x00fc, 0x444dddd0);
	WriteReg32(L_BASE_ADDRESS + 0x0100, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x0104, 0x444dddd0);
	WriteReg32(L_BASE_ADDRESS + 0x0108, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x010c, 0x44dddd40);
	WriteReg32(L_BASE_ADDRESS + 0x0110, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x0114, 0x44dddd40);
	WriteReg32(L_BASE_ADDRESS + 0x0118, 0x0355dd55);
	WriteReg32(L_BASE_ADDRESS + 0x011c, 0x5eeee550);
	WriteReg32(L_BASE_ADDRESS + 0x0120, 0x0345cc55);
	WriteReg32(L_BASE_ADDRESS + 0x0124, 0x5eeee550);
	WriteReg32(L_BASE_ADDRESS + 0x0128, 0x0345ccee);
	WriteReg32(L_BASE_ADDRESS + 0x012c, 0xeeee5550);
	WriteReg32(L_BASE_ADDRESS + 0x0130, 0x0345ccee);
	WriteReg32(L_BASE_ADDRESS + 0x0134, 0xeeee5500);
	WriteReg32(L_BASE_ADDRESS + 0x0138, 0x003355ee);
	WriteReg32(L_BASE_ADDRESS + 0x013c, 0xeeee5500);
	WriteReg32(L_BASE_ADDRESS + 0x0140, 0x00033555);
	WriteReg32(L_BASE_ADDRESS + 0x0144, 0xee555500);
	WriteReg32(L_BASE_ADDRESS + 0x0148, 0x00000333);
	WriteReg32(L_BASE_ADDRESS + 0x014c, 0x33333000);

#else
	//X_OFF & Y_OFF
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfe18fe53);
	//KX & KY
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x0013ff69);
	//LowMap
	WriteReg32(L_BASE_ADDRESS + 0x00d0, 0x00033355);
	WriteReg32(L_BASE_ADDRESS + 0x00d4, 0x53333000);
	WriteReg32(L_BASE_ADDRESS + 0x00d8, 0x003333cc);
	WriteReg32(L_BASE_ADDRESS + 0x00dc, 0xc3333330);
	WriteReg32(L_BASE_ADDRESS + 0x00e0, 0x00333ccc);
	WriteReg32(L_BASE_ADDRESS + 0x00e4, 0xc333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00e8, 0x003ccccc);
	WriteReg32(L_BASE_ADDRESS + 0x00ec, 0x3333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00f0, 0x06fffff6);//0x03ccccc3);
	WriteReg32(L_BASE_ADDRESS + 0x00f4, 0x22226660);//0x3333ccc0);
	WriteReg32(L_BASE_ADDRESS + 0x00f8, 0x06666622);//0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x00fc, 0x22288880);//0x444dddd0);
	WriteReg32(L_BASE_ADDRESS + 0x0100, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x0104, 0x888ffff0);//0x444dddd0);
	WriteReg32(L_BASE_ADDRESS + 0x0108, 0x08888822);//0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x010c, 0x44dddd40);
	WriteReg32(L_BASE_ADDRESS + 0x0110, 0x0ddddd44);
	WriteReg32(L_BASE_ADDRESS + 0x0114, 0x44dddd40);
	WriteReg32(L_BASE_ADDRESS + 0x0118, 0x0355dd55);
	WriteReg32(L_BASE_ADDRESS + 0x011c, 0x5eeee550);
	WriteReg32(L_BASE_ADDRESS + 0x0120, 0x068affaa);//0x0345cc55);
	WriteReg32(L_BASE_ADDRESS + 0x0124, 0xaffffaa0);//0x5eeee550);
	WriteReg32(L_BASE_ADDRESS + 0x0128, 0x0345ccee);
	WriteReg32(L_BASE_ADDRESS + 0x012c, 0xffffaaa0);//0xeeee5550);
	WriteReg32(L_BASE_ADDRESS + 0x0130, 0x03457788);//0x0345ccee);
	WriteReg32(L_BASE_ADDRESS + 0x0134, 0x88885500);//0xeeee5500);
	WriteReg32(L_BASE_ADDRESS + 0x0138, 0x00335588);//0x003355ee);
	WriteReg32(L_BASE_ADDRESS + 0x013c, 0xeeee5500);
	WriteReg32(L_BASE_ADDRESS + 0x0140, 0x00033555);
	WriteReg32(L_BASE_ADDRESS + 0x0144, 0xee555500);
	WriteReg32(L_BASE_ADDRESS + 0x0148, 0x00000333);
	WriteReg32(L_BASE_ADDRESS + 0x014c, 0x33333000);

#endif
	//MiddleMask
	WriteReg32(L_BASE_ADDRESS + 0x0150, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0154, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0158, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x015c, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0160, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0164, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0168, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x016c, 0x00000000);
	//HighMask
	WriteReg32(L_BASE_ADDRESS + 0x0170, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0174, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0178, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x017c, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0180, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0184, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x0188, 0x00000000);
	WriteReg32(L_BASE_ADDRESS + 0x018c, 0x00000000);

// ROIs : L
	WriteReg32(0xe0088a14 , 0x00250004);
	WriteReg32(0xe0088a18 , 0x00110006);
	WriteReg32(0xe0088a1c , 0x0007ffff);
	WriteReg32(0xe0088a20 , 0x001d0006);
	WriteReg32(0xe0088a24 , 0x00410043);
	WriteReg32(0xe0088a28 , 0x000b0009);
	WriteReg32(0xe0088a2c , 0x0044fff5);
	WriteReg32(0xe0088a30 , 0x00080008);
// ROIs : R
	WriteReg32(0xe008ca14 , ReadReg32(ISP_BASE_ADDR + 0x8a14));
	WriteReg32(0xe008ca18 , ReadReg32(ISP_BASE_ADDR + 0x8a18));
	WriteReg32(0xe008ca1c , ReadReg32(ISP_BASE_ADDR + 0x8a1c));
	WriteReg32(0xe008ca20 , ReadReg32(ISP_BASE_ADDR + 0x8a20));
	WriteReg32(0xe008ca24 , ReadReg32(ISP_BASE_ADDR + 0x8a24));
	WriteReg32(0xe008ca28 , ReadReg32(ISP_BASE_ADDR + 0x8a28));
	WriteReg32(0xe008ca2c , ReadReg32(ISP_BASE_ADDR + 0x8a2c));
	WriteReg32(0xe008ca30 , ReadReg32(ISP_BASE_ADDR + 0x8a30));

/*
	WriteReg32(L_BASE_ADDRESS + 0x01d8, 0x30303030);
	WriteReg32(L_BASE_ADDRESS + 0x01d8, 0x30303030);
	WriteReg32(L_BASE_ADDRESS + 0x01dc, 0x10083030);
	WriteReg32(L_BASE_ADDRESS + 0x01e0, 0x60402018); // Weight 3,4,5
	WriteReg32(L_BASE_ADDRESS + 0x01e4, 0x20181008);
	WriteReg32(L_BASE_ADDRESS + 0x01e8, 0x08088050);
	WriteReg32(L_BASE_ADDRESS + 0x01ec, 0x08080808);

	WriteReg32(L_BASE_ADDRESS + 0x01cc, 0x00640032); // Thresholds
	WriteReg32(L_BASE_ADDRESS + 0x01d0, 0x012c00c8);
	WriteReg32(L_BASE_ADDRESS + 0x01d4, 0x138802bc);

	WriteReg32(L_BASE_ADDRESS + 0x01f0, 0x01000080); // Offset and ROIOption
// Awb shift
	WriteReg32(L_BASE_ADDRESS + 0x01b0, 0x0062003f); //AWBShiftCTCP[0] | AWBShiftCTCP[1]
	WriteReg32(L_BASE_ADDRESS + 0x01b4, 0x0144006c); //AWBShiftCTCP[2] | AWBShiftCTCP[3]
	WriteReg32(L_BASE_ADDRESS + 0x01b8, 0x010001a0); //AWBShiftCTCP[4] | nAWBBrTh[0]
	WriteReg32(L_BASE_ADDRESS + 0x01bc, 0x04000200); //nAWBBrTh[1] | nAWBBrTh[2]
	WriteReg32(L_BASE_ADDRESS + 0x05a8, 0x0e002204); // ... | nAWBBrTh[3]
	// For A/H light: [3]: 5lux [2]: 50lux [1]: 100lux [0]: 500~300lux
	WriteReg32(L_BASE_ADDRESS + 0x05cc, 0x474b4c50); // AWBShift DX000[0] | DX000[1] | DX000[2] | DX000[3]
	WriteReg32(L_BASE_ADDRESS + 0x05d0, 0xcaa9a6a3); // AWBShift DY000[0] | DY000[1] | DY000[2] | DY000[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c4, 0x494b4c6a); // AWBShift DX00[0] | DX00[1] | DX00[2] | DX00[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c8, 0xb0a4a191); // AWBShift DY00[0] | DY00[1] | DY00[2] | DY00[3]
	WriteReg32(L_BASE_ADDRESS + 0x05ac, 0x80808080); // AWBShift DX0[0] | DX0[1] | DX0[2] | DX0[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b0, 0x80808080); // AWBShift DY0[0] | DY0[1] | DY0[2] | DY0[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b4, 0x80808080); // AWBShift DX1[0] | DX1[1] | DX1[2] | DX1[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b8, 0x80808080); // AWBShift DY1[0] | DY1[1] | DY1[2] | DY1[3]
	WriteReg32(L_BASE_ADDRESS + 0x05bc, 0x80808080); // AWBShift DX2[0] | DX2[1] | DX2[2] | DX2[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c0, 0x80808080); // AWBShift DY2[0] | DY2[1] | DY2[2] | DY2[3]
*/

#endif

#ifdef CURVE_AWB_SETTING_R
	g_pControl_R->bROIEnable = 1;

// AWB calc
	WriteReg32(R_BASE_ADDRESS + 0x008c, 0x020003ff);	// maxBgain|maxGBgain
	WriteReg32(R_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
	WriteReg32(R_BASE_ADDRESS + 0x0190, 0x10050000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
	//WriteReg32(L_BASE_ADDRESS + 0x019c, 0x02d000e0);	// brightness threshold of AWB map switch,bw=16, t0|t1

	//X_OFF & Y_OFF
	WriteReg32(R_BASE_ADDRESS + 0x01a8, 0xfe18fe53);
	//KX & KY
	WriteReg32(R_BASE_ADDRESS + 0x01a4, 0x0013ff69);
	//LowMap
	WriteReg32(R_BASE_ADDRESS + 0x00d0, 0x00033355);
	WriteReg32(R_BASE_ADDRESS + 0x00d4, 0x53333000);
	WriteReg32(R_BASE_ADDRESS + 0x00d8, 0x003333cc);
	WriteReg32(R_BASE_ADDRESS + 0x00dc, 0xc3333330);
	WriteReg32(R_BASE_ADDRESS + 0x00e0, 0x00333ccc);
	WriteReg32(R_BASE_ADDRESS + 0x00e4, 0xc333ccc0);
	WriteReg32(R_BASE_ADDRESS + 0x00e8, 0x003ccccc);
	WriteReg32(R_BASE_ADDRESS + 0x00ec, 0x3333ccc0);
	WriteReg32(R_BASE_ADDRESS + 0x00f0, 0x06fffff6);//0x03ccccc3);
	WriteReg32(R_BASE_ADDRESS + 0x00f4, 0x22226660);//0x3333ccc0);
	WriteReg32(R_BASE_ADDRESS + 0x00f8, 0x06666622);//0x0ddddd44);
	WriteReg32(R_BASE_ADDRESS + 0x00fc, 0x22288880);//0x444dddd0);
	WriteReg32(R_BASE_ADDRESS + 0x0100, 0x0ddddd44);
	WriteReg32(R_BASE_ADDRESS + 0x0104, 0x888ffff0);//0x444dddd0);
	WriteReg32(R_BASE_ADDRESS + 0x0108, 0x08888822);//0x0ddddd44);
	WriteReg32(R_BASE_ADDRESS + 0x010c, 0x44dddd40);
	WriteReg32(R_BASE_ADDRESS + 0x0110, 0x0ddddd44);
	WriteReg32(R_BASE_ADDRESS + 0x0114, 0x44dddd40);
	WriteReg32(R_BASE_ADDRESS + 0x0118, 0x0355dd55);
	WriteReg32(R_BASE_ADDRESS + 0x011c, 0x5eeee550);
	WriteReg32(R_BASE_ADDRESS + 0x0120, 0x068affaa);//0x0345cc55);
	WriteReg32(R_BASE_ADDRESS + 0x0124, 0xaffffaa0);//0x5eeee550);
	WriteReg32(R_BASE_ADDRESS + 0x0128, 0x0345ccee);
	WriteReg32(R_BASE_ADDRESS + 0x012c, 0xffffaaa0);//0xeeee5550);
	WriteReg32(R_BASE_ADDRESS + 0x0130, 0x03457788);//0x0345ccee);
	WriteReg32(R_BASE_ADDRESS + 0x0134, 0x88885500);//0xeeee5500);
	WriteReg32(R_BASE_ADDRESS + 0x0138, 0x00335588);//0x003355ee);
	WriteReg32(R_BASE_ADDRESS + 0x013c, 0xeeee5500);
	WriteReg32(R_BASE_ADDRESS + 0x0140, 0x00033555);
	WriteReg32(R_BASE_ADDRESS + 0x0144, 0xee555500);
	WriteReg32(R_BASE_ADDRESS + 0x0148, 0x00000333);
	WriteReg32(R_BASE_ADDRESS + 0x014c, 0x33333000);

	//MiddleMask
	WriteReg32(R_BASE_ADDRESS + 0x0150, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0154, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0158, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x015c, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0160, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0164, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0168, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x016c, 0x00000000);
	//HighMask
	WriteReg32(R_BASE_ADDRESS + 0x0170, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0174, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0178, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x017c, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0180, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0184, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x0188, 0x00000000);
	WriteReg32(R_BASE_ADDRESS + 0x018c, 0x00000000);

#endif

#ifdef CCM_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x02708001); // UVSaturate enable | mode
	WriteReg32(L_BASE_ADDRESS + 0x03f8, 0x00000130); //
	WriteReg32(L_BASE_ADDRESS + 0x03fc, 0x000001f8); //

// CCM
#if 0
	WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xff3501c6);
	WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xff950005);
	WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xffaa01c1);
	WriteReg32(L_BASE_ADDRESS + 0x03c8, 0xff7bffb6);
	WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x021501cf);	// center(c)
	WriteReg32(L_BASE_ADDRESS + 0x03d0, 0x000ffedc);
	WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x01ddffb8);
	WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xffb2ff6b);
	WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x01b3ff9b);	// left(a)
	WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xff3701c1);
	WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xff790008);
	WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xffb301d4);
	WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xff65ffc1);
	WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x010001da);	// right(d), [15:0] maunual CT
#else
	WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xff0501f4);
	WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xff800007);
	WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xffa101df);
	WriteReg32(L_BASE_ADDRESS + 0x03c8, 0xff53ffa5);
	WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x02320208);	// center(c)
	WriteReg32(L_BASE_ADDRESS + 0x03d0, 0x0014feba);
	WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x01f1ffab);
	WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xffa5ff64);
	WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x01d5ff86);	// left(a)
	WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xfecc0228);
	WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xff49000c);
	WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xffa00217);
	WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xff06ffa3);
	WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x01000257);	// right(d), [15:0] maunual CT
#endif
	WriteReg32(L_BASE_ADDRESS + 0x03b0, 0x00500036); // thre[0] | thre[1]
	WriteReg32(L_BASE_ADDRESS + 0x03b4, 0x00700060); // thre[2] | thre[3]
	WriteReg32(L_BASE_ADDRESS + 0x03b8, 0x012000e0); // thre[4] | thre[5]
	WriteReg32(L_BASE_ADDRESS + 0x03ac, 0x80808001); //[31:24] auto CT enable
	WriteReg32(L_BASE_ADDRESS + 0x0490, 0x02830001);
	WriteReg32(L_BASE_ADDRESS + 0x0494, 0x000ffe6e);
	WriteReg32(L_BASE_ADDRESS + 0x0498, 0x01fcffa8);
	WriteReg32(L_BASE_ADDRESS + 0x049c, 0xff81ff5c);
	WriteReg32(L_BASE_ADDRESS + 0x04a0, 0x01b9ffc6); // last(h)
#endif

#ifdef CCM_SETTING_R
	WriteReg32(R_BASE_ADDRESS + 0x03f4, 0x02708001); // UVSaturate enable | mode
	WriteReg32(R_BASE_ADDRESS + 0x03f8, 0x00000130); //
	WriteReg32(R_BASE_ADDRESS + 0x03fc, 0x000001f8); //

	WriteReg32(R_BASE_ADDRESS + 0x03bc, 0xff0501f4);
	WriteReg32(R_BASE_ADDRESS + 0x03c0, 0xff800007);
	WriteReg32(R_BASE_ADDRESS + 0x03c4, 0xffa101df);
	WriteReg32(R_BASE_ADDRESS + 0x03c8, 0xff53ffa5);
	WriteReg32(R_BASE_ADDRESS + 0x03cc, 0x02320208);	// center(c)
	WriteReg32(R_BASE_ADDRESS + 0x03d0, 0x0014feba);
	WriteReg32(R_BASE_ADDRESS + 0x03d4, 0x01f1ffab);
	WriteReg32(R_BASE_ADDRESS + 0x03d8, 0xffa5ff64);
	WriteReg32(R_BASE_ADDRESS + 0x03dc, 0x01d5ff86);	// left(a)
	WriteReg32(R_BASE_ADDRESS + 0x03e0, 0xfecc0228);
	WriteReg32(R_BASE_ADDRESS + 0x03e4, 0xff49000c);
	WriteReg32(R_BASE_ADDRESS + 0x03e8, 0xffa00217);
	WriteReg32(R_BASE_ADDRESS + 0x03ec, 0xff06ffa3);
	WriteReg32(R_BASE_ADDRESS + 0x03f0, 0x01000257);	// right(d), [15:0] maunual CT

	WriteReg32(R_BASE_ADDRESS + 0x03b0, 0x00500036); // thre[0] | thre[1]
	WriteReg32(R_BASE_ADDRESS + 0x03b4, 0x00700060); // thre[2] | thre[3]
	WriteReg32(R_BASE_ADDRESS + 0x03b8, 0x012000e0); // thre[4] | thre[5]
	WriteReg32(R_BASE_ADDRESS + 0x03ac, 0x80808001); //[31:24] auto CT enable
	WriteReg32(R_BASE_ADDRESS + 0x0490, 0x02830001);
	WriteReg32(R_BASE_ADDRESS + 0x0494, 0x000ffe6e);
	WriteReg32(R_BASE_ADDRESS + 0x0498, 0x01fcffa8);
	WriteReg32(R_BASE_ADDRESS + 0x049c, 0xff81ff5c);
	WriteReg32(R_BASE_ADDRESS + 0x04a0, 0x01b9ffc6); // last(h)
#endif

#ifdef LENC_SETTING
// LENC
//;48 10032388 00800070 ; thre[0],thre[1],16bit
//;48 1003238c 00c000a0 ; thre[2],thre[3],16bit
	WriteReg32(L_BASE_ADDRESS + 0x0388, 0x010000e0);	// thre[0],thre[1],16bit
	WriteReg32(L_BASE_ADDRESS + 0x038c, 0x01800140);	// thre[2],thre[3],16bit
	//WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x0558096d);	// h_scale|v_scale
#ifdef SENSOR_CROP_1536_1440	// This sensor output resolution is 1536x1440
	WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x095509f4);	// h_scale|v_scale
#else
	WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x08df096e);	// h_scale|v_scale
#endif
	//A light shading
	WriteReg32(L_BASE_ADDRESS + 0x0700,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0704,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0708,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x070c,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0710,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x0714,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0718,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x071c,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x0720,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x0724,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x0728,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x072c,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x0730,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x0734,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x0738,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x073c,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x0740,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x0744,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x0748,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x074c,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x0750,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x0754,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x0758,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x075c,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x0760,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0764,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0768,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x076c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0770,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0774,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0778,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x077c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0780,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0784,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0788,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x078c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0790,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0794,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0798,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x079c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x07a0,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x07a4,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x07a8,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x07ac,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x07b0,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x07b4,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x07b8,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x07bc,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x07c0,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x07c4,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x07c8,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x07cc,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x07d0,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x07d4,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x07d8,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x07dc,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x07e0,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x07e4,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x07e8,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x07ec,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x07f0,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x07f4,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x07f8,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x07fc,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0800,0x7A797779);
	WriteReg32(L_BASE_ADDRESS + 0x0804,0x7B7B797B);
	WriteReg32(L_BASE_ADDRESS + 0x0808,0x7B7A7B7C);
	WriteReg32(L_BASE_ADDRESS + 0x080c,0x78787A79);
	WriteReg32(L_BASE_ADDRESS + 0x0810,0x7A797978);
	WriteReg32(L_BASE_ADDRESS + 0x0814,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0818,0x7B7B7B7B);
	WriteReg32(L_BASE_ADDRESS + 0x081c,0x78797A7A);
	WriteReg32(L_BASE_ADDRESS + 0x0820,0x7C7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0824,0x7D7D7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0828,0x7C7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x082c,0x7B7A7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0830,0x7D7D7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0834,0x7E7E7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0838,0x7E7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x083c,0x7A7D7D7E);
	WriteReg32(L_BASE_ADDRESS + 0x0840,0x7E7D7D7B);
	WriteReg32(L_BASE_ADDRESS + 0x0844,0x7F7F7F7E);
	WriteReg32(L_BASE_ADDRESS + 0x0848,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x084c,0x7C7D7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0850,0x7F7E7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0854,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0858,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x085c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0860,0x7F7F7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0864,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0868,0x807F8080);
	WriteReg32(L_BASE_ADDRESS + 0x086c,0x7E7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0870,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0874,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0878,0x7F808080);
	WriteReg32(L_BASE_ADDRESS + 0x087c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0880,0x7F7F7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0884,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0888,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x088c,0x7D7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0890,0x7F7E7E7C);
	WriteReg32(L_BASE_ADDRESS + 0x0894,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0898,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x089c,0x7C7E7F80);
	WriteReg32(L_BASE_ADDRESS + 0x08a0,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x08a4,0x80807F80);
	WriteReg32(L_BASE_ADDRESS + 0x08a8,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x08ac,0x7C7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x08b0,0x7F7E7E7B);
	WriteReg32(L_BASE_ADDRESS + 0x08b4,0x807F807E);
	WriteReg32(L_BASE_ADDRESS + 0x08b8,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x08bc,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x08c0,0x7D7E7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x08c4,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x08c8,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x08cc,0x7E7C7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x08d0,0x7D7C7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x08d4,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x08d8,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x08dc,0x7A7B7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x08e0,0x79797878);
	WriteReg32(L_BASE_ADDRESS + 0x08e4,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x08e8,0x7A7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x08ec,0x7778797A);
	WriteReg32(L_BASE_ADDRESS + 0x08f0,0x76787678);
	WriteReg32(L_BASE_ADDRESS + 0x08f4,0x79787878);
	WriteReg32(L_BASE_ADDRESS + 0x08f8,0x78777979);
	WriteReg32(L_BASE_ADDRESS + 0x08fc,0x7B777A76);
	WriteReg32(L_BASE_ADDRESS + 0x0900,0x87868592);
	WriteReg32(L_BASE_ADDRESS + 0x0904,0x86858585);
	WriteReg32(L_BASE_ADDRESS + 0x0908,0x84868585);
	WriteReg32(L_BASE_ADDRESS + 0x090c,0x82898786);
	WriteReg32(L_BASE_ADDRESS + 0x0910,0x85858683);
	WriteReg32(L_BASE_ADDRESS + 0x0914,0x84858584);
	WriteReg32(L_BASE_ADDRESS + 0x0918,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x091c,0x86868586);
	WriteReg32(L_BASE_ADDRESS + 0x0920,0x83848385);
	WriteReg32(L_BASE_ADDRESS + 0x0924,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0928,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x092c,0x84848484);
	WriteReg32(L_BASE_ADDRESS + 0x0930,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x0934,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0938,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x093c,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0940,0x82838284);
	WriteReg32(L_BASE_ADDRESS + 0x0944,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0948,0x82828181);
	WriteReg32(L_BASE_ADDRESS + 0x094c,0x84828382);
	WriteReg32(L_BASE_ADDRESS + 0x0950,0x81828382);
	WriteReg32(L_BASE_ADDRESS + 0x0954,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0958,0x82818181);
	WriteReg32(L_BASE_ADDRESS + 0x095c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0960,0x81828283);
	WriteReg32(L_BASE_ADDRESS + 0x0964,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0968,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x096c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0970,0x81818282);
	WriteReg32(L_BASE_ADDRESS + 0x0974,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0978,0x81818080);
	WriteReg32(L_BASE_ADDRESS + 0x097c,0x84828281);
	WriteReg32(L_BASE_ADDRESS + 0x0980,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0984,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0988,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x098c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0990,0x82828382);
	WriteReg32(L_BASE_ADDRESS + 0x0994,0x81808081);
	WriteReg32(L_BASE_ADDRESS + 0x0998,0x82818180);
	WriteReg32(L_BASE_ADDRESS + 0x099c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x09a0,0x81828383);
	WriteReg32(L_BASE_ADDRESS + 0x09a4,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x09a8,0x82828281);
	WriteReg32(L_BASE_ADDRESS + 0x09ac,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x09b0,0x83838384);
	WriteReg32(L_BASE_ADDRESS + 0x09b4,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x09b8,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x09bc,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x09c0,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x09c4,0x83828383);
	WriteReg32(L_BASE_ADDRESS + 0x09c8,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x09cc,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x09d0,0x84848585);
	WriteReg32(L_BASE_ADDRESS + 0x09d4,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x09d8,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x09dc,0x85848584);
	WriteReg32(L_BASE_ADDRESS + 0x09e0,0x85868886);
	WriteReg32(L_BASE_ADDRESS + 0x09e4,0x85848585);
	WriteReg32(L_BASE_ADDRESS + 0x09e8,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x09ec,0x88868686);
	WriteReg32(L_BASE_ADDRESS + 0x09f0,0x888A8793);
	WriteReg32(L_BASE_ADDRESS + 0x09f4,0x86878786);
	WriteReg32(L_BASE_ADDRESS + 0x09f8,0x88868789);
	WriteReg32(L_BASE_ADDRESS + 0x09fc,0x898A8C88);
	//CWF light shading
	WriteReg32(L_BASE_ADDRESS + 0x0a00,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0a04,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0a08,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0a0c,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0a10,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x0a14,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0a18,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0a1c,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x0a20,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x0a24,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x0a28,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x0a2c,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x0a30,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x0a34,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x0a38,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x0a3c,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x0a40,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x0a44,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x0a48,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x0a4c,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x0a50,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x0a54,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x0a58,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x0a5c,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x0a60,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0a64,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0a68,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x0a6c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0a70,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0a74,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0a78,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x0a7c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0a80,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0a84,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0a88,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x0a8c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0a90,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0a94,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0a98,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x0a9c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0aa0,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x0aa4,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x0aa8,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x0aac,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x0ab0,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x0ab4,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x0ab8,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x0abc,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x0ac0,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x0ac4,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x0ac8,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x0acc,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x0ad0,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x0ad4,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x0ad8,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x0adc,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x0ae0,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x0ae4,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0ae8,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0aec,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x0af0,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0af4,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0af8,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0afc,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0b00,0x7A797779);
	WriteReg32(L_BASE_ADDRESS + 0x0b04,0x7B7B797B);
	WriteReg32(L_BASE_ADDRESS + 0x0b08,0x7B7A7B7C);
	WriteReg32(L_BASE_ADDRESS + 0x0b0c,0x78787A79);
	WriteReg32(L_BASE_ADDRESS + 0x0b10,0x7A797978);
	WriteReg32(L_BASE_ADDRESS + 0x0b14,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0b18,0x7B7B7B7B);
	WriteReg32(L_BASE_ADDRESS + 0x0b1c,0x78797A7A);
	WriteReg32(L_BASE_ADDRESS + 0x0b20,0x7C7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0b24,0x7D7D7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0b28,0x7C7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0b2c,0x7B7A7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0b30,0x7D7D7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0b34,0x7E7E7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0b38,0x7E7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0b3c,0x7A7D7D7E);
	WriteReg32(L_BASE_ADDRESS + 0x0b40,0x7E7D7D7B);
	WriteReg32(L_BASE_ADDRESS + 0x0b44,0x7F7F7F7E);
	WriteReg32(L_BASE_ADDRESS + 0x0b48,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b4c,0x7C7D7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0b50,0x7F7E7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0b54,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b58,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b5c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b60,0x7F7F7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0b64,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0b68,0x807F8080);
	WriteReg32(L_BASE_ADDRESS + 0x0b6c,0x7E7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b70,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0b74,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0b78,0x7F808080);
	WriteReg32(L_BASE_ADDRESS + 0x0b7c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b80,0x7F7F7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0b84,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0b88,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0b8c,0x7D7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0b90,0x7F7E7E7C);
	WriteReg32(L_BASE_ADDRESS + 0x0b94,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0b98,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0b9c,0x7C7E7F80);
	WriteReg32(L_BASE_ADDRESS + 0x0ba0,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0ba4,0x80807F80);
	WriteReg32(L_BASE_ADDRESS + 0x0ba8,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0bac,0x7C7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0bb0,0x7F7E7E7B);
	WriteReg32(L_BASE_ADDRESS + 0x0bb4,0x807F807E);
	WriteReg32(L_BASE_ADDRESS + 0x0bb8,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0bbc,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0bc0,0x7D7E7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0bc4,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0bc8,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0bcc,0x7E7C7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0bd0,0x7D7C7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0bd4,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0bd8,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0bdc,0x7A7B7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0be0,0x79797878);
	WriteReg32(L_BASE_ADDRESS + 0x0be4,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0be8,0x7A7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0bec,0x7778797A);
	WriteReg32(L_BASE_ADDRESS + 0x0bf0,0x76787678);
	WriteReg32(L_BASE_ADDRESS + 0x0bf4,0x79787878);
	WriteReg32(L_BASE_ADDRESS + 0x0bf8,0x78777979);
	WriteReg32(L_BASE_ADDRESS + 0x0bfc,0x7B777A76);
	WriteReg32(L_BASE_ADDRESS + 0x0c00,0x87868592);
	WriteReg32(L_BASE_ADDRESS + 0x0c04,0x86858585);
	WriteReg32(L_BASE_ADDRESS + 0x0c08,0x84868585);
	WriteReg32(L_BASE_ADDRESS + 0x0c0c,0x82898786);
	WriteReg32(L_BASE_ADDRESS + 0x0c10,0x85858683);
	WriteReg32(L_BASE_ADDRESS + 0x0c14,0x84858584);
	WriteReg32(L_BASE_ADDRESS + 0x0c18,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x0c1c,0x86868586);
	WriteReg32(L_BASE_ADDRESS + 0x0c20,0x83848385);
	WriteReg32(L_BASE_ADDRESS + 0x0c24,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0c28,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0c2c,0x84848484);
	WriteReg32(L_BASE_ADDRESS + 0x0c30,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x0c34,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c38,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c3c,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0c40,0x82838284);
	WriteReg32(L_BASE_ADDRESS + 0x0c44,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0c48,0x82828181);
	WriteReg32(L_BASE_ADDRESS + 0x0c4c,0x84828382);
	WriteReg32(L_BASE_ADDRESS + 0x0c50,0x81828382);
	WriteReg32(L_BASE_ADDRESS + 0x0c54,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0c58,0x82818181);
	WriteReg32(L_BASE_ADDRESS + 0x0c5c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c60,0x81828283);
	WriteReg32(L_BASE_ADDRESS + 0x0c64,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0c68,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0c6c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c70,0x81818282);
	WriteReg32(L_BASE_ADDRESS + 0x0c74,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0c78,0x81818080);
	WriteReg32(L_BASE_ADDRESS + 0x0c7c,0x84828281);
	WriteReg32(L_BASE_ADDRESS + 0x0c80,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c84,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0c88,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0c8c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c90,0x82828382);
	WriteReg32(L_BASE_ADDRESS + 0x0c94,0x81808081);
	WriteReg32(L_BASE_ADDRESS + 0x0c98,0x82818180);
	WriteReg32(L_BASE_ADDRESS + 0x0c9c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0ca0,0x81828383);
	WriteReg32(L_BASE_ADDRESS + 0x0ca4,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0ca8,0x82828281);
	WriteReg32(L_BASE_ADDRESS + 0x0cac,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x0cb0,0x83838384);
	WriteReg32(L_BASE_ADDRESS + 0x0cb4,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0cb8,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x0cbc,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x0cc0,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x0cc4,0x83828383);
	WriteReg32(L_BASE_ADDRESS + 0x0cc8,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x0ccc,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x0cd0,0x84848585);
	WriteReg32(L_BASE_ADDRESS + 0x0cd4,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0cd8,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x0cdc,0x85848584);
	WriteReg32(L_BASE_ADDRESS + 0x0ce0,0x85868886);
	WriteReg32(L_BASE_ADDRESS + 0x0ce4,0x85848585);
	WriteReg32(L_BASE_ADDRESS + 0x0ce8,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x0cec,0x88868686);
	WriteReg32(L_BASE_ADDRESS + 0x0cf0,0x888A8793);
	WriteReg32(L_BASE_ADDRESS + 0x0cf4,0x86878786);
	WriteReg32(L_BASE_ADDRESS + 0x0cf8,0x88868789);
	WriteReg32(L_BASE_ADDRESS + 0x0cfc,0x898A8C88);
	//Day light shading
	WriteReg32(L_BASE_ADDRESS + 0x0d00,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0d04,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0d08,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0d0c,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0d10,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x0d14,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0d18,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0d1c,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x0d20,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x0d24,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x0d28,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x0d2c,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x0d30,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x0d34,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x0d38,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x0d3c,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x0d40,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x0d44,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x0d48,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x0d4c,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x0d50,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x0d54,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x0d58,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x0d5c,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x0d60,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0d64,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0d68,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x0d6c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0d70,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0d74,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0d78,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x0d7c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0d80,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0d84,0x0002060A);
	WriteReg32(L_BASE_ADDRESS + 0x0d88,0x0A060200);
	WriteReg32(L_BASE_ADDRESS + 0x0d8c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0d90,0x14203850);
	WriteReg32(L_BASE_ADDRESS + 0x0d94,0x0202060A);
	WriteReg32(L_BASE_ADDRESS + 0x0d98,0x0A060202);
	WriteReg32(L_BASE_ADDRESS + 0x0d9c,0x50382014);
	WriteReg32(L_BASE_ADDRESS + 0x0da0,0x14243850);
	WriteReg32(L_BASE_ADDRESS + 0x0da4,0x0606060C);
	WriteReg32(L_BASE_ADDRESS + 0x0da8,0x0C060606);
	WriteReg32(L_BASE_ADDRESS + 0x0dac,0x50382414);
	WriteReg32(L_BASE_ADDRESS + 0x0db0,0x1E2C3850);
	WriteReg32(L_BASE_ADDRESS + 0x0db4,0x0A0A0C10);
	WriteReg32(L_BASE_ADDRESS + 0x0db8,0x100C0A0A);
	WriteReg32(L_BASE_ADDRESS + 0x0dbc,0x50382C1E);
	WriteReg32(L_BASE_ADDRESS + 0x0dc0,0x24303850);
	WriteReg32(L_BASE_ADDRESS + 0x0dc4,0x1414141E);
	WriteReg32(L_BASE_ADDRESS + 0x0dc8,0x1E141414);
	WriteReg32(L_BASE_ADDRESS + 0x0dcc,0x50383024);
	WriteReg32(L_BASE_ADDRESS + 0x0dd0,0x30383850);
	WriteReg32(L_BASE_ADDRESS + 0x0dd4,0x2020242C);
	WriteReg32(L_BASE_ADDRESS + 0x0dd8,0x2C242020);
	WriteReg32(L_BASE_ADDRESS + 0x0ddc,0x50383830);
	WriteReg32(L_BASE_ADDRESS + 0x0de0,0x38384050);
	WriteReg32(L_BASE_ADDRESS + 0x0de4,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0de8,0x38383838);
	WriteReg32(L_BASE_ADDRESS + 0x0dec,0x50403838);
	WriteReg32(L_BASE_ADDRESS + 0x0df0,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0df4,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0df8,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0dfc,0x50505050);
	WriteReg32(L_BASE_ADDRESS + 0x0e00,0x7A797779);
	WriteReg32(L_BASE_ADDRESS + 0x0e04,0x7B7B797B);
	WriteReg32(L_BASE_ADDRESS + 0x0e08,0x7B7A7B7C);
	WriteReg32(L_BASE_ADDRESS + 0x0e0c,0x78787A79);
	WriteReg32(L_BASE_ADDRESS + 0x0e10,0x7A797978);
	WriteReg32(L_BASE_ADDRESS + 0x0e14,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0e18,0x7B7B7B7B);
	WriteReg32(L_BASE_ADDRESS + 0x0e1c,0x78797A7A);
	WriteReg32(L_BASE_ADDRESS + 0x0e20,0x7C7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0e24,0x7D7D7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0e28,0x7C7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0e2c,0x7B7A7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0e30,0x7D7D7C7C);
	WriteReg32(L_BASE_ADDRESS + 0x0e34,0x7E7E7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0e38,0x7E7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0e3c,0x7A7D7D7E);
	WriteReg32(L_BASE_ADDRESS + 0x0e40,0x7E7D7D7B);
	WriteReg32(L_BASE_ADDRESS + 0x0e44,0x7F7F7F7E);
	WriteReg32(L_BASE_ADDRESS + 0x0e48,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e4c,0x7C7D7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0e50,0x7F7E7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0e54,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e58,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e5c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e60,0x7F7F7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0e64,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0e68,0x807F8080);
	WriteReg32(L_BASE_ADDRESS + 0x0e6c,0x7E7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e70,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0e74,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0e78,0x7F808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e7c,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e80,0x7F7F7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0e84,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e88,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e8c,0x7D7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0e90,0x7F7E7E7C);
	WriteReg32(L_BASE_ADDRESS + 0x0e94,0x8080807F);
	WriteReg32(L_BASE_ADDRESS + 0x0e98,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e9c,0x7C7E7F80);
	WriteReg32(L_BASE_ADDRESS + 0x0ea0,0x7F7E7E7D);
	WriteReg32(L_BASE_ADDRESS + 0x0ea4,0x80807F80);
	WriteReg32(L_BASE_ADDRESS + 0x0ea8,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0eac,0x7C7E7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0eb0,0x7F7E7E7B);
	WriteReg32(L_BASE_ADDRESS + 0x0eb4,0x807F807E);
	WriteReg32(L_BASE_ADDRESS + 0x0eb8,0x7F807F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0ebc,0x7C7E7E7F);
	WriteReg32(L_BASE_ADDRESS + 0x0ec0,0x7D7E7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0ec4,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0ec8,0x7F7F7F7F);
	WriteReg32(L_BASE_ADDRESS + 0x0ecc,0x7E7C7E7E);
	WriteReg32(L_BASE_ADDRESS + 0x0ed0,0x7D7C7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0ed4,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0ed8,0x7D7D7D7D);
	WriteReg32(L_BASE_ADDRESS + 0x0edc,0x7A7B7D7C);
	WriteReg32(L_BASE_ADDRESS + 0x0ee0,0x79797878);
	WriteReg32(L_BASE_ADDRESS + 0x0ee4,0x7B7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0ee8,0x7A7B7B7A);
	WriteReg32(L_BASE_ADDRESS + 0x0eec,0x7778797A);
	WriteReg32(L_BASE_ADDRESS + 0x0ef0,0x76787678);
	WriteReg32(L_BASE_ADDRESS + 0x0ef4,0x79787878);
	WriteReg32(L_BASE_ADDRESS + 0x0ef8,0x78777979);
	WriteReg32(L_BASE_ADDRESS + 0x0efc,0x7B777A76);
	WriteReg32(L_BASE_ADDRESS + 0x0f00,0x87868592);
	WriteReg32(L_BASE_ADDRESS + 0x0f04,0x86858585);
	WriteReg32(L_BASE_ADDRESS + 0x0f08,0x84868585);
	WriteReg32(L_BASE_ADDRESS + 0x0f0c,0x82898786);
	WriteReg32(L_BASE_ADDRESS + 0x0f10,0x85858683);
	WriteReg32(L_BASE_ADDRESS + 0x0f14,0x84858584);
	WriteReg32(L_BASE_ADDRESS + 0x0f18,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x0f1c,0x86868586);
	WriteReg32(L_BASE_ADDRESS + 0x0f20,0x83848385);
	WriteReg32(L_BASE_ADDRESS + 0x0f24,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0f28,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0f2c,0x84848484);
	WriteReg32(L_BASE_ADDRESS + 0x0f30,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x0f34,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f38,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f3c,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0f40,0x82838284);
	WriteReg32(L_BASE_ADDRESS + 0x0f44,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f48,0x82828181);
	WriteReg32(L_BASE_ADDRESS + 0x0f4c,0x84828382);
	WriteReg32(L_BASE_ADDRESS + 0x0f50,0x81828382);
	WriteReg32(L_BASE_ADDRESS + 0x0f54,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f58,0x82818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f5c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f60,0x81828283);
	WriteReg32(L_BASE_ADDRESS + 0x0f64,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0f68,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0f6c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f70,0x81818282);
	WriteReg32(L_BASE_ADDRESS + 0x0f74,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0f78,0x81818080);
	WriteReg32(L_BASE_ADDRESS + 0x0f7c,0x84828281);
	WriteReg32(L_BASE_ADDRESS + 0x0f80,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f84,0x80808081);
	WriteReg32(L_BASE_ADDRESS + 0x0f88,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0f8c,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f90,0x82828382);
	WriteReg32(L_BASE_ADDRESS + 0x0f94,0x81808081);
	WriteReg32(L_BASE_ADDRESS + 0x0f98,0x82818180);
	WriteReg32(L_BASE_ADDRESS + 0x0f9c,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0fa0,0x81828383);
	WriteReg32(L_BASE_ADDRESS + 0x0fa4,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0fa8,0x82828281);
	WriteReg32(L_BASE_ADDRESS + 0x0fac,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x0fb0,0x83838384);
	WriteReg32(L_BASE_ADDRESS + 0x0fb4,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0fb8,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x0fbc,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x0fc0,0x83838483);
	WriteReg32(L_BASE_ADDRESS + 0x0fc4,0x83828383);
	WriteReg32(L_BASE_ADDRESS + 0x0fc8,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x0fcc,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x0fd0,0x84848585);
	WriteReg32(L_BASE_ADDRESS + 0x0fd4,0x83838383);
	WriteReg32(L_BASE_ADDRESS + 0x0fd8,0x84848483);
	WriteReg32(L_BASE_ADDRESS + 0x0fdc,0x85848584);
	WriteReg32(L_BASE_ADDRESS + 0x0fe0,0x85868886);
	WriteReg32(L_BASE_ADDRESS + 0x0fe4,0x85848585);
	WriteReg32(L_BASE_ADDRESS + 0x0fe8,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x0fec,0x88868686);
	WriteReg32(L_BASE_ADDRESS + 0x0ff0,0x888A8793);
	WriteReg32(L_BASE_ADDRESS + 0x0ff4,0x86878786);
	WriteReg32(L_BASE_ADDRESS + 0x0ff8,0x88868789);
	WriteReg32(L_BASE_ADDRESS + 0x0ffc,0x898A8C88);

#endif

#ifdef LENC_SETTING_R
// LENC
//;48 10032388 00800070 ; thre[0],thre[1],16bit
//;48 1003238c 00c000a0 ; thre[2],thre[3],16bit
	WriteReg32(R_BASE_ADDRESS + 0x0388, 0x010000e0);	// thre[0],thre[1],16bit
	WriteReg32(R_BASE_ADDRESS + 0x038c, 0x01800140);	// thre[2],thre[3],16bit
	//WriteReg32(ISP_LENC_ADDR_R + ISP_LENC_OFFSET_HVSCALE, 0x0558096d);	// h_scale|v_scale
#ifdef SENSOR_CROP_1536_1440	// This sensor output resolution is 1536x1440
	WriteReg32(ISP_LENC_ADDR_R + ISP_LENC_OFFSET_HVSCALE, 0x095509f4);	// h_scale|v_scale
#else
	WriteReg32(ISP_LENC_ADDR_R + ISP_LENC_OFFSET_HVSCALE, 0x08df096e);	// h_scale|v_scale
#endif
	//A light shading
	WriteReg32(R_BASE_ADDRESS + 0x0700,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0704,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0708,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x070c,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0710,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x0714,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0718,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x071c,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x0720,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x0724,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x0728,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x072c,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x0730,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x0734,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x0738,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x073c,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x0740,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x0744,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x0748,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x074c,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x0750,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x0754,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x0758,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x075c,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x0760,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0764,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0768,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x076c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0770,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0774,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0778,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x077c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0780,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0784,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0788,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x078c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0790,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0794,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0798,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x079c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x07a0,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x07a4,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x07a8,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x07ac,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x07b0,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x07b4,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x07b8,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x07bc,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x07c0,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x07c4,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x07c8,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x07cc,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x07d0,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x07d4,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x07d8,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x07dc,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x07e0,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x07e4,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x07e8,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x07ec,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x07f0,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x07f4,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x07f8,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x07fc,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0800,0x7A797779);
	WriteReg32(R_BASE_ADDRESS + 0x0804,0x7B7B797B);
	WriteReg32(R_BASE_ADDRESS + 0x0808,0x7B7A7B7C);
	WriteReg32(R_BASE_ADDRESS + 0x080c,0x78787A79);
	WriteReg32(R_BASE_ADDRESS + 0x0810,0x7A797978);
	WriteReg32(R_BASE_ADDRESS + 0x0814,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0818,0x7B7B7B7B);
	WriteReg32(R_BASE_ADDRESS + 0x081c,0x78797A7A);
	WriteReg32(R_BASE_ADDRESS + 0x0820,0x7C7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0824,0x7D7D7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0828,0x7C7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x082c,0x7B7A7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0830,0x7D7D7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0834,0x7E7E7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0838,0x7E7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x083c,0x7A7D7D7E);
	WriteReg32(R_BASE_ADDRESS + 0x0840,0x7E7D7D7B);
	WriteReg32(R_BASE_ADDRESS + 0x0844,0x7F7F7F7E);
	WriteReg32(R_BASE_ADDRESS + 0x0848,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x084c,0x7C7D7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0850,0x7F7E7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0854,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0858,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x085c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0860,0x7F7F7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0864,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0868,0x807F8080);
	WriteReg32(R_BASE_ADDRESS + 0x086c,0x7E7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0870,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0874,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0878,0x7F808080);
	WriteReg32(R_BASE_ADDRESS + 0x087c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0880,0x7F7F7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0884,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0888,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x088c,0x7D7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0890,0x7F7E7E7C);
	WriteReg32(R_BASE_ADDRESS + 0x0894,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0898,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x089c,0x7C7E7F80);
	WriteReg32(R_BASE_ADDRESS + 0x08a0,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x08a4,0x80807F80);
	WriteReg32(R_BASE_ADDRESS + 0x08a8,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x08ac,0x7C7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x08b0,0x7F7E7E7B);
	WriteReg32(R_BASE_ADDRESS + 0x08b4,0x807F807E);
	WriteReg32(R_BASE_ADDRESS + 0x08b8,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x08bc,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x08c0,0x7D7E7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x08c4,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x08c8,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x08cc,0x7E7C7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x08d0,0x7D7C7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x08d4,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x08d8,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x08dc,0x7A7B7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x08e0,0x79797878);
	WriteReg32(R_BASE_ADDRESS + 0x08e4,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x08e8,0x7A7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x08ec,0x7778797A);
	WriteReg32(R_BASE_ADDRESS + 0x08f0,0x76787678);
	WriteReg32(R_BASE_ADDRESS + 0x08f4,0x79787878);
	WriteReg32(R_BASE_ADDRESS + 0x08f8,0x78777979);
	WriteReg32(R_BASE_ADDRESS + 0x08fc,0x7B777A76);
	WriteReg32(R_BASE_ADDRESS + 0x0900,0x87868592);
	WriteReg32(R_BASE_ADDRESS + 0x0904,0x86858585);
	WriteReg32(R_BASE_ADDRESS + 0x0908,0x84868585);
	WriteReg32(R_BASE_ADDRESS + 0x090c,0x82898786);
	WriteReg32(R_BASE_ADDRESS + 0x0910,0x85858683);
	WriteReg32(R_BASE_ADDRESS + 0x0914,0x84858584);
	WriteReg32(R_BASE_ADDRESS + 0x0918,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x091c,0x86868586);
	WriteReg32(R_BASE_ADDRESS + 0x0920,0x83848385);
	WriteReg32(R_BASE_ADDRESS + 0x0924,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0928,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x092c,0x84848484);
	WriteReg32(R_BASE_ADDRESS + 0x0930,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x0934,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x0938,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x093c,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0940,0x82838284);
	WriteReg32(R_BASE_ADDRESS + 0x0944,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0948,0x82828181);
	WriteReg32(R_BASE_ADDRESS + 0x094c,0x84828382);
	WriteReg32(R_BASE_ADDRESS + 0x0950,0x81828382);
	WriteReg32(R_BASE_ADDRESS + 0x0954,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0958,0x82818181);
	WriteReg32(R_BASE_ADDRESS + 0x095c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0960,0x81828283);
	WriteReg32(R_BASE_ADDRESS + 0x0964,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0968,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x096c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0970,0x81818282);
	WriteReg32(R_BASE_ADDRESS + 0x0974,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0978,0x81818080);
	WriteReg32(R_BASE_ADDRESS + 0x097c,0x84828281);
	WriteReg32(R_BASE_ADDRESS + 0x0980,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0984,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0988,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x098c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0990,0x82828382);
	WriteReg32(R_BASE_ADDRESS + 0x0994,0x81808081);
	WriteReg32(R_BASE_ADDRESS + 0x0998,0x82818180);
	WriteReg32(R_BASE_ADDRESS + 0x099c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x09a0,0x81828383);
	WriteReg32(R_BASE_ADDRESS + 0x09a4,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x09a8,0x82828281);
	WriteReg32(R_BASE_ADDRESS + 0x09ac,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x09b0,0x83838384);
	WriteReg32(R_BASE_ADDRESS + 0x09b4,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x09b8,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x09bc,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x09c0,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x09c4,0x83828383);
	WriteReg32(R_BASE_ADDRESS + 0x09c8,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x09cc,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x09d0,0x84848585);
	WriteReg32(R_BASE_ADDRESS + 0x09d4,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x09d8,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x09dc,0x85848584);
	WriteReg32(R_BASE_ADDRESS + 0x09e0,0x85868886);
	WriteReg32(R_BASE_ADDRESS + 0x09e4,0x85848585);
	WriteReg32(R_BASE_ADDRESS + 0x09e8,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x09ec,0x88868686);
	WriteReg32(R_BASE_ADDRESS + 0x09f0,0x888A8793);
	WriteReg32(R_BASE_ADDRESS + 0x09f4,0x86878786);
	WriteReg32(R_BASE_ADDRESS + 0x09f8,0x88868789);
	WriteReg32(R_BASE_ADDRESS + 0x09fc,0x898A8C88);
	//CWF light shading
	WriteReg32(R_BASE_ADDRESS + 0x0a00,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0a04,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0a08,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0a0c,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0a10,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x0a14,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0a18,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0a1c,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x0a20,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x0a24,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x0a28,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x0a2c,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x0a30,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x0a34,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x0a38,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x0a3c,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x0a40,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x0a44,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x0a48,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x0a4c,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x0a50,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x0a54,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x0a58,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x0a5c,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x0a60,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0a64,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0a68,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x0a6c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0a70,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0a74,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0a78,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x0a7c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0a80,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0a84,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0a88,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x0a8c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0a90,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0a94,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0a98,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x0a9c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0aa0,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x0aa4,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x0aa8,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x0aac,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x0ab0,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x0ab4,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x0ab8,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x0abc,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x0ac0,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x0ac4,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x0ac8,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x0acc,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x0ad0,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x0ad4,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x0ad8,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x0adc,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x0ae0,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x0ae4,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0ae8,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0aec,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x0af0,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0af4,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0af8,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0afc,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0b00,0x7A797779);
	WriteReg32(R_BASE_ADDRESS + 0x0b04,0x7B7B797B);
	WriteReg32(R_BASE_ADDRESS + 0x0b08,0x7B7A7B7C);
	WriteReg32(R_BASE_ADDRESS + 0x0b0c,0x78787A79);
	WriteReg32(R_BASE_ADDRESS + 0x0b10,0x7A797978);
	WriteReg32(R_BASE_ADDRESS + 0x0b14,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0b18,0x7B7B7B7B);
	WriteReg32(R_BASE_ADDRESS + 0x0b1c,0x78797A7A);
	WriteReg32(R_BASE_ADDRESS + 0x0b20,0x7C7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0b24,0x7D7D7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0b28,0x7C7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0b2c,0x7B7A7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0b30,0x7D7D7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0b34,0x7E7E7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0b38,0x7E7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0b3c,0x7A7D7D7E);
	WriteReg32(R_BASE_ADDRESS + 0x0b40,0x7E7D7D7B);
	WriteReg32(R_BASE_ADDRESS + 0x0b44,0x7F7F7F7E);
	WriteReg32(R_BASE_ADDRESS + 0x0b48,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b4c,0x7C7D7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0b50,0x7F7E7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0b54,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b58,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b5c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b60,0x7F7F7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0b64,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0b68,0x807F8080);
	WriteReg32(R_BASE_ADDRESS + 0x0b6c,0x7E7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b70,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0b74,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0b78,0x7F808080);
	WriteReg32(R_BASE_ADDRESS + 0x0b7c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b80,0x7F7F7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0b84,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0b88,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0b8c,0x7D7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0b90,0x7F7E7E7C);
	WriteReg32(R_BASE_ADDRESS + 0x0b94,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0b98,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0b9c,0x7C7E7F80);
	WriteReg32(R_BASE_ADDRESS + 0x0ba0,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0ba4,0x80807F80);
	WriteReg32(R_BASE_ADDRESS + 0x0ba8,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0bac,0x7C7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0bb0,0x7F7E7E7B);
	WriteReg32(R_BASE_ADDRESS + 0x0bb4,0x807F807E);
	WriteReg32(R_BASE_ADDRESS + 0x0bb8,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0bbc,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0bc0,0x7D7E7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0bc4,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0bc8,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0bcc,0x7E7C7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0bd0,0x7D7C7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0bd4,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0bd8,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0bdc,0x7A7B7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0be0,0x79797878);
	WriteReg32(R_BASE_ADDRESS + 0x0be4,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0be8,0x7A7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0bec,0x7778797A);
	WriteReg32(R_BASE_ADDRESS + 0x0bf0,0x76787678);
	WriteReg32(R_BASE_ADDRESS + 0x0bf4,0x79787878);
	WriteReg32(R_BASE_ADDRESS + 0x0bf8,0x78777979);
	WriteReg32(R_BASE_ADDRESS + 0x0bfc,0x7B777A76);
	WriteReg32(R_BASE_ADDRESS + 0x0c00,0x87868592);
	WriteReg32(R_BASE_ADDRESS + 0x0c04,0x86858585);
	WriteReg32(R_BASE_ADDRESS + 0x0c08,0x84868585);
	WriteReg32(R_BASE_ADDRESS + 0x0c0c,0x82898786);
	WriteReg32(R_BASE_ADDRESS + 0x0c10,0x85858683);
	WriteReg32(R_BASE_ADDRESS + 0x0c14,0x84858584);
	WriteReg32(R_BASE_ADDRESS + 0x0c18,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x0c1c,0x86868586);
	WriteReg32(R_BASE_ADDRESS + 0x0c20,0x83848385);
	WriteReg32(R_BASE_ADDRESS + 0x0c24,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0c28,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0c2c,0x84848484);
	WriteReg32(R_BASE_ADDRESS + 0x0c30,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x0c34,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c38,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c3c,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0c40,0x82838284);
	WriteReg32(R_BASE_ADDRESS + 0x0c44,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0c48,0x82828181);
	WriteReg32(R_BASE_ADDRESS + 0x0c4c,0x84828382);
	WriteReg32(R_BASE_ADDRESS + 0x0c50,0x81828382);
	WriteReg32(R_BASE_ADDRESS + 0x0c54,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0c58,0x82818181);
	WriteReg32(R_BASE_ADDRESS + 0x0c5c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c60,0x81828283);
	WriteReg32(R_BASE_ADDRESS + 0x0c64,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0c68,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x0c6c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c70,0x81818282);
	WriteReg32(R_BASE_ADDRESS + 0x0c74,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0c78,0x81818080);
	WriteReg32(R_BASE_ADDRESS + 0x0c7c,0x84828281);
	WriteReg32(R_BASE_ADDRESS + 0x0c80,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c84,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0c88,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x0c8c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0c90,0x82828382);
	WriteReg32(R_BASE_ADDRESS + 0x0c94,0x81808081);
	WriteReg32(R_BASE_ADDRESS + 0x0c98,0x82818180);
	WriteReg32(R_BASE_ADDRESS + 0x0c9c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0ca0,0x81828383);
	WriteReg32(R_BASE_ADDRESS + 0x0ca4,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0ca8,0x82828281);
	WriteReg32(R_BASE_ADDRESS + 0x0cac,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x0cb0,0x83838384);
	WriteReg32(R_BASE_ADDRESS + 0x0cb4,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x0cb8,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x0cbc,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x0cc0,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x0cc4,0x83828383);
	WriteReg32(R_BASE_ADDRESS + 0x0cc8,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x0ccc,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x0cd0,0x84848585);
	WriteReg32(R_BASE_ADDRESS + 0x0cd4,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0cd8,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x0cdc,0x85848584);
	WriteReg32(R_BASE_ADDRESS + 0x0ce0,0x85868886);
	WriteReg32(R_BASE_ADDRESS + 0x0ce4,0x85848585);
	WriteReg32(R_BASE_ADDRESS + 0x0ce8,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x0cec,0x88868686);
	WriteReg32(R_BASE_ADDRESS + 0x0cf0,0x888A8793);
	WriteReg32(R_BASE_ADDRESS + 0x0cf4,0x86878786);
	WriteReg32(R_BASE_ADDRESS + 0x0cf8,0x88868789);
	WriteReg32(R_BASE_ADDRESS + 0x0cfc,0x898A8C88);
	//Day light shading
	WriteReg32(R_BASE_ADDRESS + 0x0d00,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0d04,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0d08,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0d0c,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0d10,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x0d14,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0d18,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0d1c,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x0d20,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x0d24,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x0d28,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x0d2c,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x0d30,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x0d34,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x0d38,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x0d3c,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x0d40,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x0d44,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x0d48,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x0d4c,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x0d50,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x0d54,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x0d58,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x0d5c,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x0d60,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0d64,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0d68,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x0d6c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0d70,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0d74,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0d78,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x0d7c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0d80,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0d84,0x0002060A);
	WriteReg32(R_BASE_ADDRESS + 0x0d88,0x0A060200);
	WriteReg32(R_BASE_ADDRESS + 0x0d8c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0d90,0x14203850);
	WriteReg32(R_BASE_ADDRESS + 0x0d94,0x0202060A);
	WriteReg32(R_BASE_ADDRESS + 0x0d98,0x0A060202);
	WriteReg32(R_BASE_ADDRESS + 0x0d9c,0x50382014);
	WriteReg32(R_BASE_ADDRESS + 0x0da0,0x14243850);
	WriteReg32(R_BASE_ADDRESS + 0x0da4,0x0606060C);
	WriteReg32(R_BASE_ADDRESS + 0x0da8,0x0C060606);
	WriteReg32(R_BASE_ADDRESS + 0x0dac,0x50382414);
	WriteReg32(R_BASE_ADDRESS + 0x0db0,0x1E2C3850);
	WriteReg32(R_BASE_ADDRESS + 0x0db4,0x0A0A0C10);
	WriteReg32(R_BASE_ADDRESS + 0x0db8,0x100C0A0A);
	WriteReg32(R_BASE_ADDRESS + 0x0dbc,0x50382C1E);
	WriteReg32(R_BASE_ADDRESS + 0x0dc0,0x24303850);
	WriteReg32(R_BASE_ADDRESS + 0x0dc4,0x1414141E);
	WriteReg32(R_BASE_ADDRESS + 0x0dc8,0x1E141414);
	WriteReg32(R_BASE_ADDRESS + 0x0dcc,0x50383024);
	WriteReg32(R_BASE_ADDRESS + 0x0dd0,0x30383850);
	WriteReg32(R_BASE_ADDRESS + 0x0dd4,0x2020242C);
	WriteReg32(R_BASE_ADDRESS + 0x0dd8,0x2C242020);
	WriteReg32(R_BASE_ADDRESS + 0x0ddc,0x50383830);
	WriteReg32(R_BASE_ADDRESS + 0x0de0,0x38384050);
	WriteReg32(R_BASE_ADDRESS + 0x0de4,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0de8,0x38383838);
	WriteReg32(R_BASE_ADDRESS + 0x0dec,0x50403838);
	WriteReg32(R_BASE_ADDRESS + 0x0df0,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0df4,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0df8,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0dfc,0x50505050);
	WriteReg32(R_BASE_ADDRESS + 0x0e00,0x7A797779);
	WriteReg32(R_BASE_ADDRESS + 0x0e04,0x7B7B797B);
	WriteReg32(R_BASE_ADDRESS + 0x0e08,0x7B7A7B7C);
	WriteReg32(R_BASE_ADDRESS + 0x0e0c,0x78787A79);
	WriteReg32(R_BASE_ADDRESS + 0x0e10,0x7A797978);
	WriteReg32(R_BASE_ADDRESS + 0x0e14,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0e18,0x7B7B7B7B);
	WriteReg32(R_BASE_ADDRESS + 0x0e1c,0x78797A7A);
	WriteReg32(R_BASE_ADDRESS + 0x0e20,0x7C7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0e24,0x7D7D7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0e28,0x7C7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0e2c,0x7B7A7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0e30,0x7D7D7C7C);
	WriteReg32(R_BASE_ADDRESS + 0x0e34,0x7E7E7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0e38,0x7E7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0e3c,0x7A7D7D7E);
	WriteReg32(R_BASE_ADDRESS + 0x0e40,0x7E7D7D7B);
	WriteReg32(R_BASE_ADDRESS + 0x0e44,0x7F7F7F7E);
	WriteReg32(R_BASE_ADDRESS + 0x0e48,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e4c,0x7C7D7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0e50,0x7F7E7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0e54,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e58,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e5c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e60,0x7F7F7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0e64,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0e68,0x807F8080);
	WriteReg32(R_BASE_ADDRESS + 0x0e6c,0x7E7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e70,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0e74,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0e78,0x7F808080);
	WriteReg32(R_BASE_ADDRESS + 0x0e7c,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e80,0x7F7F7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0e84,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0e88,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0e8c,0x7D7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0e90,0x7F7E7E7C);
	WriteReg32(R_BASE_ADDRESS + 0x0e94,0x8080807F);
	WriteReg32(R_BASE_ADDRESS + 0x0e98,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0e9c,0x7C7E7F80);
	WriteReg32(R_BASE_ADDRESS + 0x0ea0,0x7F7E7E7D);
	WriteReg32(R_BASE_ADDRESS + 0x0ea4,0x80807F80);
	WriteReg32(R_BASE_ADDRESS + 0x0ea8,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0eac,0x7C7E7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0eb0,0x7F7E7E7B);
	WriteReg32(R_BASE_ADDRESS + 0x0eb4,0x807F807E);
	WriteReg32(R_BASE_ADDRESS + 0x0eb8,0x7F807F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0ebc,0x7C7E7E7F);
	WriteReg32(R_BASE_ADDRESS + 0x0ec0,0x7D7E7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0ec4,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0ec8,0x7F7F7F7F);
	WriteReg32(R_BASE_ADDRESS + 0x0ecc,0x7E7C7E7E);
	WriteReg32(R_BASE_ADDRESS + 0x0ed0,0x7D7C7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0ed4,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0ed8,0x7D7D7D7D);
	WriteReg32(R_BASE_ADDRESS + 0x0edc,0x7A7B7D7C);
	WriteReg32(R_BASE_ADDRESS + 0x0ee0,0x79797878);
	WriteReg32(R_BASE_ADDRESS + 0x0ee4,0x7B7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0ee8,0x7A7B7B7A);
	WriteReg32(R_BASE_ADDRESS + 0x0eec,0x7778797A);
	WriteReg32(R_BASE_ADDRESS + 0x0ef0,0x76787678);
	WriteReg32(R_BASE_ADDRESS + 0x0ef4,0x79787878);
	WriteReg32(R_BASE_ADDRESS + 0x0ef8,0x78777979);
	WriteReg32(R_BASE_ADDRESS + 0x0efc,0x7B777A76);
	WriteReg32(R_BASE_ADDRESS + 0x0f00,0x87868592);
	WriteReg32(R_BASE_ADDRESS + 0x0f04,0x86858585);
	WriteReg32(R_BASE_ADDRESS + 0x0f08,0x84868585);
	WriteReg32(R_BASE_ADDRESS + 0x0f0c,0x82898786);
	WriteReg32(R_BASE_ADDRESS + 0x0f10,0x85858683);
	WriteReg32(R_BASE_ADDRESS + 0x0f14,0x84858584);
	WriteReg32(R_BASE_ADDRESS + 0x0f18,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x0f1c,0x86868586);
	WriteReg32(R_BASE_ADDRESS + 0x0f20,0x83848385);
	WriteReg32(R_BASE_ADDRESS + 0x0f24,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0f28,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0f2c,0x84848484);
	WriteReg32(R_BASE_ADDRESS + 0x0f30,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x0f34,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f38,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f3c,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0f40,0x82838284);
	WriteReg32(R_BASE_ADDRESS + 0x0f44,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0f48,0x82828181);
	WriteReg32(R_BASE_ADDRESS + 0x0f4c,0x84828382);
	WriteReg32(R_BASE_ADDRESS + 0x0f50,0x81828382);
	WriteReg32(R_BASE_ADDRESS + 0x0f54,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0f58,0x82818181);
	WriteReg32(R_BASE_ADDRESS + 0x0f5c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f60,0x81828283);
	WriteReg32(R_BASE_ADDRESS + 0x0f64,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0f68,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x0f6c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f70,0x81818282);
	WriteReg32(R_BASE_ADDRESS + 0x0f74,0x80808080);
	WriteReg32(R_BASE_ADDRESS + 0x0f78,0x81818080);
	WriteReg32(R_BASE_ADDRESS + 0x0f7c,0x84828281);
	WriteReg32(R_BASE_ADDRESS + 0x0f80,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f84,0x80808081);
	WriteReg32(R_BASE_ADDRESS + 0x0f88,0x81818180);
	WriteReg32(R_BASE_ADDRESS + 0x0f8c,0x81828282);
	WriteReg32(R_BASE_ADDRESS + 0x0f90,0x82828382);
	WriteReg32(R_BASE_ADDRESS + 0x0f94,0x81808081);
	WriteReg32(R_BASE_ADDRESS + 0x0f98,0x82818180);
	WriteReg32(R_BASE_ADDRESS + 0x0f9c,0x83828282);
	WriteReg32(R_BASE_ADDRESS + 0x0fa0,0x81828383);
	WriteReg32(R_BASE_ADDRESS + 0x0fa4,0x81818181);
	WriteReg32(R_BASE_ADDRESS + 0x0fa8,0x82828281);
	WriteReg32(R_BASE_ADDRESS + 0x0fac,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x0fb0,0x83838384);
	WriteReg32(R_BASE_ADDRESS + 0x0fb4,0x82828282);
	WriteReg32(R_BASE_ADDRESS + 0x0fb8,0x83838282);
	WriteReg32(R_BASE_ADDRESS + 0x0fbc,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x0fc0,0x83838483);
	WriteReg32(R_BASE_ADDRESS + 0x0fc4,0x83828383);
	WriteReg32(R_BASE_ADDRESS + 0x0fc8,0x84838383);
	WriteReg32(R_BASE_ADDRESS + 0x0fcc,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x0fd0,0x84848585);
	WriteReg32(R_BASE_ADDRESS + 0x0fd4,0x83838383);
	WriteReg32(R_BASE_ADDRESS + 0x0fd8,0x84848483);
	WriteReg32(R_BASE_ADDRESS + 0x0fdc,0x85848584);
	WriteReg32(R_BASE_ADDRESS + 0x0fe0,0x85868886);
	WriteReg32(R_BASE_ADDRESS + 0x0fe4,0x85848585);
	WriteReg32(R_BASE_ADDRESS + 0x0fe8,0x85858585);
	WriteReg32(R_BASE_ADDRESS + 0x0fec,0x88868686);
	WriteReg32(R_BASE_ADDRESS + 0x0ff0,0x888A8793);
	WriteReg32(R_BASE_ADDRESS + 0x0ff4,0x86878786);
	WriteReg32(R_BASE_ADDRESS + 0x0ff8,0x88868789);
	WriteReg32(R_BASE_ADDRESS + 0x0ffc,0x898A8C88);

#endif

#ifdef LENS_ONLINE_SETTING
//lens online
	WriteReg32(0xe0088180, 0x00000237);	// [10:8]frame_cn,[5]auto_stat_en_1,[4]auto_tmp_1,[3:2]split stat mode
	WriteReg32(0xe0088184, 0x00020404);	//
	WriteReg32(0xe0088194, 0x03031010);	// compare with 03034040, 01010404
	WriteReg32(0xe0088198, 0x1810f0f0);	// compare with 1810c0c0, 1810fcfc
	WriteReg32(0xe00881a4, 0x00400100);	// nSensorGain[0]|nSensorGain[1], bw=10
	WriteReg32(0xe00881a8, 0x00000000);	// [3:0]m_nMinLensGain
#endif

#ifdef LENS_ONLINE_SETTING_R
//lens online
	WriteReg32(0xe008c180, 0x00000237);	// [10:8]frame_cn,[5]auto_stat_en_1,[4]auto_tmp_1,[3:2]split stat mode
	WriteReg32(0xe008c184, 0x00020404);	//
	WriteReg32(0xe008c194, 0x03031010);	// compare with 03034040, 01010404
	WriteReg32(0xe008c198, 0x1810f0f0);	// compare with 1810c0c0, 1810fcfc
	WriteReg32(0xe008c1a4, 0x00400100);	// nSensorGain[0]|nSensorGain[1], bw=10
	WriteReg32(0xe008c1a8, 0x00000000);	// [3:0]m_nMinLensGain
#endif

#ifdef RAW_DNS_SETTING
//RAW DNS : L
	WriteReg32(0xe0088500, 0x00060008);	// sigma[0]|sigma[1] ;000a0010
	WriteReg32(0xe0088504, 0x000a000e);	// sigma[2]|sigma[3] ;001a0026
	WriteReg32(0xe0088508, 0x00100011);	// sigma[4]|sigma[5] ;00300040
	WriteReg32(0xe008850c, 0x12121212);	// Gns[0]|Gns[1]|Gns[2]|Gns[3]
	WriteReg32(0xe0088510, 0x14141010);	// Gns[4]|Gns[5]|Rbns[0]|Rbns[1]
	WriteReg32(0xe0088514, 0x12121414);	// Rbns[2]|Rbns[3]|Rbns[4]|Rbns[5]

	WriteReg32(0xe0088530, 0x3f3f3f3f);	//
	WriteReg32(0xe0088534, 0x3f303028);	//
	WriteReg32(0xe0088538, 0x20201010);	//
	WriteReg32(0xe008853c, 0x20202020);	//
	WriteReg32(0xe0088540, 0x1010100a);	//
	WriteReg32(0xe0088544, 0x08040101);	//

//RAW DNS : R
	WriteReg32(0xe008c500, ReadReg32(ISP_BASE_ADDR + 0x8500));
	WriteReg32(0xe008c504, ReadReg32(ISP_BASE_ADDR + 0x8504));
	WriteReg32(0xe008c508, ReadReg32(ISP_BASE_ADDR + 0x8508));
	WriteReg32(0xe008c50c, ReadReg32(ISP_BASE_ADDR + 0x850c));
	WriteReg32(0xe008c510, ReadReg32(ISP_BASE_ADDR + 0x8510));
	WriteReg32(0xe008c514, ReadReg32(ISP_BASE_ADDR + 0x8514));

	WriteReg32(0xe008c530, ReadReg32(ISP_BASE_ADDR + 0x8530));
	WriteReg32(0xe008c534, ReadReg32(ISP_BASE_ADDR + 0x8534));
	WriteReg32(0xe008c538, ReadReg32(ISP_BASE_ADDR + 0x8538));
	WriteReg32(0xe008c53c, ReadReg32(ISP_BASE_ADDR + 0x853c));
	WriteReg32(0xe008c540, ReadReg32(ISP_BASE_ADDR + 0x8540));
	WriteReg32(0xe008c544, ReadReg32(ISP_BASE_ADDR + 0x8544));
#endif

#ifdef UV_DNS_SETTING
//UV DNS : L
	WriteReg32(0xe0089b00, 0x01020480);
	WriteReg32(0xe0089b04, 0x80010101);	// 1x|2x|4x
	WriteReg32(0xe0089b08, 0x01010101);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b0c, 0x01020408);	// 128x|1x|2x|4x
	WriteReg32(0xe0089b10, 0x1014181c);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b14, 0x20000000);	// 128x

//UV DNS : R
	WriteReg32(0xe008db00, ReadReg32(ISP_BASE_ADDR + 0x9b00));
	WriteReg32(0xe008db04, ReadReg32(ISP_BASE_ADDR + 0x9b04));
	WriteReg32(0xe008db08, ReadReg32(ISP_BASE_ADDR + 0x9b08));
	WriteReg32(0xe008db0c, ReadReg32(ISP_BASE_ADDR + 0x9b0c));
	WriteReg32(0xe008db10, ReadReg32(ISP_BASE_ADDR + 0x9b10));
	WriteReg32(0xe008db14, ReadReg32(ISP_BASE_ADDR + 0x9b14));
#endif

#ifdef Sharpen_SETTING
// Sharpen SETTING : L
	WriteReg32(0xe0088800, 0x3f0462ff);	// cip
										// 00 - [7]
										// 00 - [6] manual_mode
										// 00 - [5] bIfGbGr
										// 00 - [4] bIfCbCrDns
										// 00 - [3] bIfAntiClrAls
										// 00 - [2] bIfShapren
										// 00 - [1] bIfSharpenNS
										// 00 - [0] bIfDDns
										// 01 - [6:4] GDnsTSlope
 										// 01 - [3:0] WSlopeClr
 										// 02 - [4:0] CombW
										// 03 - [7:0] WSlope
	WriteReg32(0xe0088804, 0x04060810);	// DirGDNST0|1|2|3
	WriteReg32(0xe0088808, 0x12140404);	// [31:16] DirGDNST4|5
										// 0a - [5:0] DirGDNSLevel0
										// 0b - [5:0] DirGDNSLevel1
	WriteReg32(0xe008880c, 0x06080c10);	// 0c - [5:0] DirGDNSLevel2
										// 0d - [5:0] DirGDNSLevel3
										// 0e - [5:0] DirGDNSLevel4
										// 0f - [5:0] DirGDNSLevel5
	WriteReg32(0xe0088810, 0x40404040);	// CleanSharpT0|1|2|3
	WriteReg32(0xe0088814, 0x4040283c);	// [31:16] CleanSharpT4|5
										// [15:0] CleanSharpT0|1
	WriteReg32(0xe0088818, 0x3c3ca0a0);	// CleanSharpT2|3|4|5
	WriteReg32(0xe008881c, 0x10182028);	// Tintp0|1|2|3
	WriteReg32(0xe0088820, 0x38481018);	// Tintp4|5
										// TintpHV0|1
	WriteReg32(0xe0088824, 0x20283848);	// TintpHV0|1|2|3
	WriteReg32(0xe0088828, 0x20304050);	// NV0|1|2|3
	WriteReg32(0xe008882c, 0x78a00404);	// [31:16] NV4|5
										// 0e - [0:5] VNGLevel0
										// 0f - [0:5] VNGLevel1
	WriteReg32(0xe0088830, 0x04080808);	// 30 - [0:5] VNGLevel2
										// 31 - [0:5] VNGLevel3
										// 32 - [0:5] VNGLevel4
										// 33 - [0:5] VNGLevel5
	WriteReg32(0xe0088834, 0x04060810);	// 34 - [0:5] LPG0
										// 35 - [0:5] LPG1
										// 36 - [0:5] LPG2
										// 37 - [0:5] LPG3
	WriteReg32(0xe0088838, 0x10142020);	// 38 - [0:5] LPG4
										// 39 - [0:5] LPG5
										// 3a - [0:5] CbrDbsLevel0
										// 3b - [0:5] CbrDbsLevel1
	WriteReg32(0xe008883c, 0x20202020);	// 3c - [0:5] CbrDbsLevel2
										// 3d - [0:5] CbrDbsLevel3
										// 3e - [0:5] CbrDbsLevel4
										// 3f - [0:5] CbrDbsLevel5
	WriteReg32(0xe0088840, 0x00500078);	// [27:16] - TCbCrH0
										// [11:0] - TCbCrH1
	WriteReg32(0xe0088844, 0x00a00140);	// [27:16] - TCbCrH2
										// [11:0] - TCbCrH3
	WriteReg32(0xe0088848, 0x01680190);	// [27:16] - TCbCrH4
										// [11:0] - TCbCrH5
	WriteReg32(0xe008884c, 0x00c80078);	// [27:16] - TCbCrV0
										// [11:0] - TCbCrV1
	WriteReg32(0xe0088850, 0x00a00140);	// [27:16] - TCbCrV2
										// [11:0] - TCbCrV3
	WriteReg32(0xe0088854, 0x01680190);	// [27:16] - TCbCrV4
										// [11:0] - TCbCrV5
	WriteReg32(0xe0088858, 0x00123333);	// [23:20] - TCbrOffset0
										// [19:16] - TCbrOffset1
										// [15:12] - TCbrOffset2
										// [11:8] - TCbrOffset3
										// [7:4] - TCbrOffset4
										// [3:0] - TCbrOffset5
	WriteReg32(0xe008885c, 0x6455463c);	// CbCrAlsHf0|1|2|3
	WriteReg32(0xe0088860, 0x3732c83c);	// [31:16] CbCrAlsHf4|5|
										// [15:0] CbCrAlsMean0|1
	WriteReg32(0xe0088864, 0x50505050);	// CbCrAlsMean2|3|4|5
	WriteReg32(0xe0088868, 0x0c0b0a08);	// CbrImpulse0|1|2|3
	WriteReg32(0xe008886c, 0x00101204);	// [23:16] CbrImpulse4
										// [15:8] CbrImpulse5
										// [2:0] CbrImpulseThrShift
	WriteReg32(0xe0088870, 0x201c1a18);	// 70 - [5:0] SharpenLevel0
										// 71 - [5:0] SharpenLevel1
										// 72 - [5:0] SharpenLevel2
										// 73 - [5:0] SharpenLevel3
	WriteReg32(0xe0088874, 0x16140808);	// 74 - [5:0] SharpenLevel4
										// 75 - [5:0] SharpenLevel5
										// 76 - [7:0] SharpenThr0
										// 77 - [7:0] SharpenThr1
	WriteReg32(0xe0088878, 0x0a101216);	// SharpenThr2|3|4|5
	WriteReg32(0xe008887c, 0x0002060a);	// 7d - [5:0] LocalRatio0
										// 7e - [5:0] LocalRatio1
										// 7f - [5:0] LocalRatio2
	WriteReg32(0xe0088880, 0x101e1e1e);	// 80 - [5:0] SharpenHalo0
										// 81 - [5:0] SharpenHalo1
										// 82 - [5:0] SharpenHalo2
										// 83 - [5:0] SharpenHalo3
	WriteReg32(0xe0088884, 0x1e1e4839);	// 84 - [5:0] SharpenHalo4
										// 85 - [5:0] SharpenHalo5
										// 86 - [7:0] SharpenCoef0 (48)
										// 87 - [7:0] SharpenCoef1 (39)
	WriteReg32(0xe0088888, 0x1b070104);	// [31:15] SharpenCoef2|3|4 (1b, 07, 01)
										// [7:0] DarkOffset
	WriteReg32(0xe008888c, 0x33433443);	// [31:28] SharpenHDelta0 (4)
										// [27:24] SharpenHDelta1 (4)
										// [23:20] SharpenHDelta2 (4)
										// [19:16] SharpenHDelta3 (4)
										// [15:12] SharpenHDelta4 (4)
										// [11:8] SharpenHDelta5 (4)
										// [7:4] SharpenHDelta6 (4)
										// [3:0] SharpenHDelta7 (4)
	WriteReg32(0xe0088890, 0x16202020);	// 90 - [5:0] SharpenHGain0 (08)
										// 91 - [5:0] SharpenHGain1 (0c)
										// 92 - [5:0] SharpenHGain2 (0a)
										// 93 - [5:0] SharpenHGain3 (09)
	WriteReg32(0xe0088894, 0x1c202018);	// 94 - [5:0] SharpenHGain4 (08)
										// 95 - [5:0] SharpenHGain5 (0a)
										// 96 - [5:0] SharpenHGain6 (0a)
										// 97 - [5:0] SharpenHGain7 (0a)
	WriteReg32(0xe0088898, 0x103f3f3f);	// 9b - [5:0] SharpenHGain8 (06)
										// 99 - [5:0] localSymGThr0
										// 9a - [5:0] localSymGThr1
										// 9b - [5:0] localSymGThr2

	WriteReg32(0xe008889c, 0x00101820);	// 9c - reserved
										// 9d - [5:0] localDNSThr0
										// 9e - [5:0] localDNSThr1
										// 9f - [5:0] localDNSThr2
	WriteReg32(0xe0089900, 0x04120000);	// 00 - [5:0] HThre1
										// 01 - [5:0] HThre2

	WriteReg32(0xe00322c4, 0x3fff0280);	// c4 - [1:0] CurList32[9:8]
										// c5 - [7:0] CurList32[7:0]
										// c6 - [2:0] HFreqGain0[10:8]
										// c7 - [7:0] HFreqGain0[7:0]
	WriteReg32(0xe00322c8, 0x02000180);	// c8 - [2:0] HFreqGain1[10:8]
										// c9 - [7:0] HFreqGain1[7:0]
										// ca - [2:0] HFreqGain2[10:8]
										// cb - [7:0] HFreqGain2[7:0]
	WriteReg32(0xe00322cc, 0x01500120);	// cc - [2:0] HFreqGain3[10:8]
										// cd - [7:0] HFreqGain3[7:0]
										// ce - [2:0] HFreqGain4[10:8]
										// cf - [7:0] HFreqGain4[7:0]
	WriteReg32(0xe00322d0, 0x01100100);	// d0 - [2:0] HFreqGain5[10:8]
										// d1 - [7:0] HFreqGain5[7:0]
										// d2 - [2:0] HFreqGain6[10:8]
										// d3 - [7:0] HFreqGain6[7:0]
	WriteReg32(0xe00322d4, 0x01000100);	// d4 - [2:0] HFreqGain7[10:8]
										// d5 - [7:0] HFreqGain7[7:0]

// Sharpen SETTING : R
	WriteReg32(0xe008c800, ReadReg32(ISP_BASE_ADDR + 0x8800));
	WriteReg32(0xe008c804, ReadReg32(ISP_BASE_ADDR + 0x8804));
	WriteReg32(0xe008c808, ReadReg32(ISP_BASE_ADDR + 0x8808));
	WriteReg32(0xe008c80c, ReadReg32(ISP_BASE_ADDR + 0x880c));
	WriteReg32(0xe008c810, ReadReg32(ISP_BASE_ADDR + 0x8810));
	WriteReg32(0xe008c814, ReadReg32(ISP_BASE_ADDR + 0x8814));
	WriteReg32(0xe008c818, ReadReg32(ISP_BASE_ADDR + 0x8818));
	WriteReg32(0xe008c81c, ReadReg32(ISP_BASE_ADDR + 0x881c));
	WriteReg32(0xe008c820, ReadReg32(ISP_BASE_ADDR + 0x8820));
	WriteReg32(0xe008c824, ReadReg32(ISP_BASE_ADDR + 0x8824));
	WriteReg32(0xe008c828, ReadReg32(ISP_BASE_ADDR + 0x8828));
	WriteReg32(0xe008c82c, ReadReg32(ISP_BASE_ADDR + 0x882c));
	WriteReg32(0xe008c830, ReadReg32(ISP_BASE_ADDR + 0x8830));
	WriteReg32(0xe008c834, ReadReg32(ISP_BASE_ADDR + 0x8834));
	WriteReg32(0xe008c838, ReadReg32(ISP_BASE_ADDR + 0x8838));
	WriteReg32(0xe008c83c, ReadReg32(ISP_BASE_ADDR + 0x883c));
	WriteReg32(0xe008c840, ReadReg32(ISP_BASE_ADDR + 0x8840));
	WriteReg32(0xe008c844, ReadReg32(ISP_BASE_ADDR + 0x8844));
	WriteReg32(0xe008c848, ReadReg32(ISP_BASE_ADDR + 0x8848));
	WriteReg32(0xe008c84c, ReadReg32(ISP_BASE_ADDR + 0x884c));
	WriteReg32(0xe008c850, ReadReg32(ISP_BASE_ADDR + 0x8850));
	WriteReg32(0xe008c854, ReadReg32(ISP_BASE_ADDR + 0x8854));
	WriteReg32(0xe008c858, ReadReg32(ISP_BASE_ADDR + 0x8858));
	WriteReg32(0xe008c85c, ReadReg32(ISP_BASE_ADDR + 0x885c));
	WriteReg32(0xe008c860, ReadReg32(ISP_BASE_ADDR + 0x8860));
	WriteReg32(0xe008c864, ReadReg32(ISP_BASE_ADDR + 0x8864));
	WriteReg32(0xe008c868, ReadReg32(ISP_BASE_ADDR + 0x8868));
	WriteReg32(0xe008c86c, ReadReg32(ISP_BASE_ADDR + 0x886c));
	WriteReg32(0xe008c870, ReadReg32(ISP_BASE_ADDR + 0x8870));
	WriteReg32(0xe008c874, ReadReg32(ISP_BASE_ADDR + 0x8874));
	WriteReg32(0xe008c878, ReadReg32(ISP_BASE_ADDR + 0x8878));
	WriteReg32(0xe008c87c, ReadReg32(ISP_BASE_ADDR + 0x887c));
	WriteReg32(0xe008c880, ReadReg32(ISP_BASE_ADDR + 0x8880));
	WriteReg32(0xe008c884, ReadReg32(ISP_BASE_ADDR + 0x8884));
	WriteReg32(0xe008c888, ReadReg32(ISP_BASE_ADDR + 0x8888));
	WriteReg32(0xe008c88c, ReadReg32(ISP_BASE_ADDR + 0x888c));
	WriteReg32(0xe008c890, ReadReg32(ISP_BASE_ADDR + 0x8890));
	WriteReg32(0xe008c894, ReadReg32(ISP_BASE_ADDR + 0x8894));
	WriteReg32(0xe008c898, ReadReg32(ISP_BASE_ADDR + 0x8898));
	WriteReg32(0xe008c89c, ReadReg32(ISP_BASE_ADDR + 0x889c));
	WriteReg32(0xe008d900, ReadReg32(ISP_BASE_ADDR + 0x9900));
/*
	WriteReg32(0xe00322c4, 0x3fff0280);
	WriteReg32(0xe00322c8, 0x02000180);
	WriteReg32(0xe00322cc, 0x01500120);
	WriteReg32(0xe00322d0, 0x01100100);
	WriteReg32(0xe00322d4, 0x01000100);
*/
#endif

#ifdef RGBH_CURVE_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0200, 0x00800024);
	WriteReg32(L_BASE_ADDRESS + 0x0204, 0x01600100);
	WriteReg32(L_BASE_ADDRESS + 0x0208, 0x01e001ac);
	WriteReg32(L_BASE_ADDRESS + 0x020c, 0x02400210);
	WriteReg32(L_BASE_ADDRESS + 0x0210, 0x028c0268);
	WriteReg32(L_BASE_ADDRESS + 0x0214, 0x02d402b0);
	WriteReg32(L_BASE_ADDRESS + 0x0218, 0x030802f0);
	WriteReg32(L_BASE_ADDRESS + 0x021c, 0x0330031c);
	WriteReg32(L_BASE_ADDRESS + 0x0220, 0x03540344);
	WriteReg32(L_BASE_ADDRESS + 0x0224, 0x03780368);
	WriteReg32(L_BASE_ADDRESS + 0x0228, 0x03900384);
	WriteReg32(L_BASE_ADDRESS + 0x022c, 0x03a80398);
	WriteReg32(L_BASE_ADDRESS + 0x0230, 0x03b803b4);
	WriteReg32(L_BASE_ADDRESS + 0x0234, 0x03d003c4);
	WriteReg32(L_BASE_ADDRESS + 0x0238, 0x03e003d8);
	WriteReg32(L_BASE_ADDRESS + 0x023c, 0x03f403ec);
	WriteReg32(L_BASE_ADDRESS + 0x0240, 0x002403ff);
	WriteReg32(L_BASE_ADDRESS + 0x0244, 0x01000080);
	WriteReg32(L_BASE_ADDRESS + 0x0248, 0x01ac0160);
	WriteReg32(L_BASE_ADDRESS + 0x024c, 0x021001e0);
	WriteReg32(L_BASE_ADDRESS + 0x0250, 0x02680240);
	WriteReg32(L_BASE_ADDRESS + 0x0254, 0x02b0028c);
	WriteReg32(L_BASE_ADDRESS + 0x0258, 0x02f002d4);
	WriteReg32(L_BASE_ADDRESS + 0x025c, 0x031c0308);
	WriteReg32(L_BASE_ADDRESS + 0x0260, 0x03440330);
	WriteReg32(L_BASE_ADDRESS + 0x0264, 0x03680354);
	WriteReg32(L_BASE_ADDRESS + 0x0268, 0x03840378);
	WriteReg32(L_BASE_ADDRESS + 0x026c, 0x03980390);
	WriteReg32(L_BASE_ADDRESS + 0x0270, 0x03b403a8);
	WriteReg32(L_BASE_ADDRESS + 0x0274, 0x03c403b8);
	WriteReg32(L_BASE_ADDRESS + 0x0278, 0x03d803d0);
	WriteReg32(L_BASE_ADDRESS + 0x027c, 0x03ec03e0);
	WriteReg32(L_BASE_ADDRESS + 0x0280, 0x03ff03f4);
	WriteReg32(L_BASE_ADDRESS + 0x0284, 0x00800024);
	WriteReg32(L_BASE_ADDRESS + 0x0288, 0x01600100);
	WriteReg32(L_BASE_ADDRESS + 0x028c, 0x01e001ac);
	WriteReg32(L_BASE_ADDRESS + 0x0290, 0x02400210);
	WriteReg32(L_BASE_ADDRESS + 0x0294, 0x028c0268);
	WriteReg32(L_BASE_ADDRESS + 0x0298, 0x02d402b0);
	WriteReg32(L_BASE_ADDRESS + 0x029c, 0x030802f0);
	WriteReg32(L_BASE_ADDRESS + 0x02a0, 0x0330031c);
	WriteReg32(L_BASE_ADDRESS + 0x02a4, 0x03540344);
	WriteReg32(L_BASE_ADDRESS + 0x02a8, 0x03780368);
	WriteReg32(L_BASE_ADDRESS + 0x02ac, 0x03900384);
	WriteReg32(L_BASE_ADDRESS + 0x02b0, 0x03a80398);
	WriteReg32(L_BASE_ADDRESS + 0x02b4, 0x03b803b4);
	WriteReg32(L_BASE_ADDRESS + 0x02b8, 0x03d003c4);
	WriteReg32(L_BASE_ADDRESS + 0x02bc, 0x03e003d8);
	WriteReg32(L_BASE_ADDRESS + 0x02c0, 0x03f403ec);
	WriteReg32(L_BASE_ADDRESS + 0x02c4, 0x025803ff);

	WriteReg32(R_BASE_ADDRESS + 0x0200, ReadReg32(L_BASE_ADDRESS + 0x0200));
	WriteReg32(R_BASE_ADDRESS + 0x0204, ReadReg32(L_BASE_ADDRESS + 0x0204));
	WriteReg32(R_BASE_ADDRESS + 0x0208, ReadReg32(L_BASE_ADDRESS + 0x0208));
	WriteReg32(R_BASE_ADDRESS + 0x020c, ReadReg32(L_BASE_ADDRESS + 0x020c));
	WriteReg32(R_BASE_ADDRESS + 0x0210, ReadReg32(L_BASE_ADDRESS + 0x0210));
	WriteReg32(R_BASE_ADDRESS + 0x0214, ReadReg32(L_BASE_ADDRESS + 0x0214));
	WriteReg32(R_BASE_ADDRESS + 0x0218, ReadReg32(L_BASE_ADDRESS + 0x0218));
	WriteReg32(R_BASE_ADDRESS + 0x021c, ReadReg32(L_BASE_ADDRESS + 0x021c));
	WriteReg32(R_BASE_ADDRESS + 0x0220, ReadReg32(L_BASE_ADDRESS + 0x0220));
	WriteReg32(R_BASE_ADDRESS + 0x0224, ReadReg32(L_BASE_ADDRESS + 0x0224));
	WriteReg32(R_BASE_ADDRESS + 0x0228, ReadReg32(L_BASE_ADDRESS + 0x0228));
	WriteReg32(R_BASE_ADDRESS + 0x022c, ReadReg32(L_BASE_ADDRESS + 0x022c));
	WriteReg32(R_BASE_ADDRESS + 0x0230, ReadReg32(L_BASE_ADDRESS + 0x0230));
	WriteReg32(R_BASE_ADDRESS + 0x0234, ReadReg32(L_BASE_ADDRESS + 0x0234));
	WriteReg32(R_BASE_ADDRESS + 0x0238, ReadReg32(L_BASE_ADDRESS + 0x0238));
	WriteReg32(R_BASE_ADDRESS + 0x023c, ReadReg32(L_BASE_ADDRESS + 0x023c));
	WriteReg32(R_BASE_ADDRESS + 0x0240, ReadReg32(L_BASE_ADDRESS + 0x0240));
	WriteReg32(R_BASE_ADDRESS + 0x0244, ReadReg32(L_BASE_ADDRESS + 0x0244));
	WriteReg32(R_BASE_ADDRESS + 0x0248, ReadReg32(L_BASE_ADDRESS + 0x0248));
	WriteReg32(R_BASE_ADDRESS + 0x024c, ReadReg32(L_BASE_ADDRESS + 0x024c));
	WriteReg32(R_BASE_ADDRESS + 0x0250, ReadReg32(L_BASE_ADDRESS + 0x0250));
	WriteReg32(R_BASE_ADDRESS + 0x0254, ReadReg32(L_BASE_ADDRESS + 0x0254));
	WriteReg32(R_BASE_ADDRESS + 0x0258, ReadReg32(L_BASE_ADDRESS + 0x0258));
	WriteReg32(R_BASE_ADDRESS + 0x025c, ReadReg32(L_BASE_ADDRESS + 0x025c));
	WriteReg32(R_BASE_ADDRESS + 0x0260, ReadReg32(L_BASE_ADDRESS + 0x0260));
	WriteReg32(R_BASE_ADDRESS + 0x0264, ReadReg32(L_BASE_ADDRESS + 0x0264));
	WriteReg32(R_BASE_ADDRESS + 0x0268, ReadReg32(L_BASE_ADDRESS + 0x0268));
	WriteReg32(R_BASE_ADDRESS + 0x026c, ReadReg32(L_BASE_ADDRESS + 0x026c));
	WriteReg32(R_BASE_ADDRESS + 0x0270, ReadReg32(L_BASE_ADDRESS + 0x0270));
	WriteReg32(R_BASE_ADDRESS + 0x0274, ReadReg32(L_BASE_ADDRESS + 0x0274));
	WriteReg32(R_BASE_ADDRESS + 0x0278, ReadReg32(L_BASE_ADDRESS + 0x0278));
	WriteReg32(R_BASE_ADDRESS + 0x027c, ReadReg32(L_BASE_ADDRESS + 0x027c));
	WriteReg32(R_BASE_ADDRESS + 0x0280, ReadReg32(L_BASE_ADDRESS + 0x0280));
	WriteReg32(R_BASE_ADDRESS + 0x0284, ReadReg32(L_BASE_ADDRESS + 0x0284));
	WriteReg32(R_BASE_ADDRESS + 0x0288, ReadReg32(L_BASE_ADDRESS + 0x0288));
	WriteReg32(R_BASE_ADDRESS + 0x028c, ReadReg32(L_BASE_ADDRESS + 0x028c));
	WriteReg32(R_BASE_ADDRESS + 0x0290, ReadReg32(L_BASE_ADDRESS + 0x0290));
	WriteReg32(R_BASE_ADDRESS + 0x0294, ReadReg32(L_BASE_ADDRESS + 0x0294));
	WriteReg32(R_BASE_ADDRESS + 0x0298, ReadReg32(L_BASE_ADDRESS + 0x0298));
	WriteReg32(R_BASE_ADDRESS + 0x029c, ReadReg32(L_BASE_ADDRESS + 0x029c));
	WriteReg32(R_BASE_ADDRESS + 0x02a0, ReadReg32(L_BASE_ADDRESS + 0x02a0));
	WriteReg32(R_BASE_ADDRESS + 0x02a4, ReadReg32(L_BASE_ADDRESS + 0x02a4));
	WriteReg32(R_BASE_ADDRESS + 0x02a8, ReadReg32(L_BASE_ADDRESS + 0x02a8));
	WriteReg32(R_BASE_ADDRESS + 0x02ac, ReadReg32(L_BASE_ADDRESS + 0x02ac));
	WriteReg32(R_BASE_ADDRESS + 0x02b0, ReadReg32(L_BASE_ADDRESS + 0x02b0));
	WriteReg32(R_BASE_ADDRESS + 0x02b4, ReadReg32(L_BASE_ADDRESS + 0x02b4));
	WriteReg32(R_BASE_ADDRESS + 0x02b8, ReadReg32(L_BASE_ADDRESS + 0x02b8));
	WriteReg32(R_BASE_ADDRESS + 0x02bc, ReadReg32(L_BASE_ADDRESS + 0x02bc));
	WriteReg32(R_BASE_ADDRESS + 0x02c0, ReadReg32(L_BASE_ADDRESS + 0x02c0));
	WriteReg32(R_BASE_ADDRESS + 0x02c4, ReadReg32(L_BASE_ADDRESS + 0x02c4));

#endif

//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
//;;;;;;;;;;;;;;;;;;;;;; SDE color management ;;;;;;;;;;;;;;;;;;;;
//;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
#define SDE_SETTING
#ifdef SDE_SETTING
	WriteReg32(0xe0089a04, 0x00000400);
	WriteReg32(0xe0089a20, 0x80806262);	//
	WriteReg32(0xe0089a24, 0x62c65f63);	//
	WriteReg32(0xe0089a28, 0x4e4e4eb2);	//
	WriteReg32(0xe0089a2c, 0x959d7676);	//
	WriteReg32(0xe0089a30, 0x76da121f);	//
	WriteReg32(0xe0089a34, 0x120c120c);	//
	WriteReg32(0xe0089a38, 0x88888800);	//

	WriteReg32(0xe008da04, 0x00000400);
	WriteReg32(0xe008da20, 0x80806262);	//
	WriteReg32(0xe008da24, 0x62c65f63);	//
	WriteReg32(0xe008da28, 0x4e4e4eb2);	//
	WriteReg32(0xe008da2c, 0x959d7676);	//
	WriteReg32(0xe008da30, 0x76da121f);	//
	WriteReg32(0xe008da34, 0x120c120c);	//
	WriteReg32(0xe008da38, 0x88888800);	//
#endif

#ifdef DEBUG_AEMANUAL_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0020, 0x02010608);	//
	WriteReg32(L_BASE_ADDRESS + 0x0030, 0x00000500);	//
	WriteReg32(L_BASE_ADDRESS + 0x0034, 0x08080010);	//
#else
	WriteReg32(L_BASE_ADDRESS + 0x0020, 0x02000608);	//
#endif

	g_pControl_L->nSensorProcessFrameNum = 3;
/*
	int i;
	for(i = 0; i< 0x2ff; i+=4)
	{
		WriteReg32(ISP_SRAM_ADDR_L+i*4, ReadReg32(L_BASE_ADDRESS+0x0700+i*4));
	}
	for(i =0; i<192; i++)
	{
		g_pWriteLENC_L->pCP[i] = g_pLENCProfile_L->pLensCPArray[0][i];
	}
	for(i=0; i<2; i++)
	{
		g_tMiddle_L.pExpBuf[i] = 0x5d50;      //exp[i];
		g_tMiddle_L.pGainBuf[i] = 0x2d;       //gain[i];
	}

	g_tMiddle_L.pAWBGainBuf[0] = 0xe0;
	g_tMiddle_L.pAWBGainBuf[1] = 0x80;
	g_tMiddle_L.pAWBGainBuf[2] = 0x129;
*/
	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};

/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
