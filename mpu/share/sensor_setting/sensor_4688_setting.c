#include "includes.h"

///< Update IQ setting to 20170223
#define LIGHT_FREQ_50HZ 1
#define SENSOR_2A   0xf8//0x98 
#define SENSOR_2B   0x06//0x07 
#define BAND50HZ    0x2500//0x0128 
#define BAND60HZ    0x1ee0//0x00f7 

#define SENSOR_NAME_INTERNAL 4688
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#define SENSOR_SCCB_ID_INTERNAL 0x42
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_RAW10
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_MIPI_2LN

//#define SENSOR_30FPS
//#define SENSOR_29FPS
//#define SENSOR_28FPS
#define SENSOR_15FPS

#define OV4688_CROP_2592_1452_EN
/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

/*************************
  Initial setting
 *************************/
///< NOTE: sensor setting is from 4682.
SENSOR_SETTING_TABLE sensor__init[][2] = {
//RES 2688_1520_2lane_MIPI648M_PCLK120M_30fps
#if 1
#define VTS  (0x630)
{0x0103 ,0x01},
{0x3638 ,0x00},
{0x0100, 0x00},		//to delay some time for sccb 400K clock ok when fastboot
{0x0100, 0x00},
{0x0100, 0x00},
{0x0100, 0x00},
{0x0100, 0x00},
{0x0300 ,0x00},
{0x0302 ,0x1b},//19
{0x0303 ,0x00},
{0x0304 ,0x03},
{0x030b ,0x00},
{0x030d ,0x1e},
{0x030e ,0x04},
{0x030f ,0x01},
{0x0312 ,0x01},
{0x031e ,0x00},
{0x3000 ,0x20},
{0x3002 ,0x00},
{0x3018 ,0x32},//12
{0x3019 ,0x0c},//12
{0x3020 ,0x93},
{0x3021 ,0x03},
{0x3022 ,0x01},
{0x3031 ,0x0a},
{0x303f ,0x0c},
{0x3103 ,0x04},//4
{0x3305 ,0xf1},
{0x3307 ,0x04},
{0x3309 ,0x29},
{0x3500 ,0x00},
{0x3501 ,0x5d},
{0x3502 ,0x50},
{0x3503 ,0x04},
{0x3504 ,0x00},
{0x3505 ,0x00},
{0x3506 ,0x00},
{0x3507 ,0x00},
{0x3508 ,0x01},
{0x3509 ,0xa0},
{0x350a ,0x00},
{0x350b ,0x00},
{0x350c ,0x00},
{0x350d ,0x00},
{0x350e ,0x00},
{0x350f ,0x80},
{0x3510 ,0x00},
{0x3511 ,0x00},
{0x3512 ,0x00},
{0x3513 ,0x00},
{0x3514 ,0x00},
{0x3515 ,0x80},
{0x3516 ,0x00},
{0x3517 ,0x00},
{0x3518 ,0x00},
{0x3519 ,0x00},
{0x351a ,0x00},
{0x351b ,0x80},
{0x351c ,0x00},
{0x351d ,0x00},
{0x351e ,0x00},
{0x351f ,0x00},
{0x3520 ,0x00},
{0x3521 ,0x80},
{0x3522 ,0x08},
{0x3524 ,0x08},
{0x3526 ,0x08},
{0x3528 ,0x08},
{0x352a ,0x08},
{0x3602 ,0x00},
{0x3603 ,0x40},
{0x3604 ,0x02},
{0x3605 ,0x00},
{0x3606 ,0x00},
{0x3607 ,0x00},
{0x3609 ,0x12},
{0x360a ,0x40},
{0x360c ,0x08},
{0x360f ,0xe5},
{0x3608 ,0x8f},
{0x3611 ,0x00},
{0x3613 ,0xf7},
{0x3616 ,0x58},
{0x3619 ,0x99},
{0x361b ,0x60},
{0x361c ,0x7a},
{0x361e ,0x79},
{0x361f ,0x02},
{0x3632 ,0x00},
{0x3633 ,0x10},
{0x3634 ,0x10},
{0x3635 ,0x10},
{0x3636 ,0x15},
{0x3646 ,0x86},
{0x364a ,0x0b},
{0x3700 ,0x17},
{0x3701 ,0x22},
{0x3703 ,0x10},
{0x370a ,0x37},
{0x3705 ,0x00},
{0x3706 ,0x63},
{0x3709 ,0x3c},
{0x370b ,0x01},
{0x370c ,0x30},
{0x3710 ,0x24},
{0x3711 ,0x0c},
{0x3716 ,0x00},
{0x3720 ,0x28},
{0x3729 ,0x7b},
{0x372a ,0x84},
{0x372b ,0xbd},
{0x372c ,0xbc},
{0x372e ,0x52},
{0x373c ,0x0e},
{0x373e ,0x33},
{0x3743 ,0x10},
{0x3744 ,0x88},
{0x3745 ,0xc0},
{0x374a ,0x43},
{0x374c ,0x00},
{0x374e ,0x23},
{0x3751 ,0x7b},
{0x3752 ,0x84},
{0x3753 ,0xbd},
{0x3754 ,0xbc},
{0x3756 ,0x52},
{0x375c ,0x00},
{0x3760 ,0x00},
{0x3761 ,0x00},
{0x3762 ,0x00},
{0x3763 ,0x00},
{0x3764 ,0x00},
{0x3767 ,0x04},
{0x3768 ,0x04},
{0x3769 ,0x08},
{0x376a ,0x08},
{0x376b ,0x20},
{0x376c ,0x00},
{0x376d ,0x00},
{0x376e ,0x00},
{0x3773 ,0x00},
{0x3774 ,0x51},
{0x3776 ,0xbd},
{0x3777 ,0xbd},
{0x3781 ,0x18},
{0x3783 ,0x25},
{0x3798 ,0x1b},
{0x3800	,0x00},
{0x3801	,0x08},
{0x3802	,0x00},
{0x3803	,0x04},
{0x3804	,0x0A},
{0x3805	,0x97},
{0x3806	,0x05},
{0x3807	,0xFB},
{0x3808	,0x0A},
{0x3809	,0x80},
{0x380A	,0x05},
{0x380B	,0xF0},
{0x380c ,0xa1},
{0x380d ,0xe0},
{0x380e ,0x06},
{0x380f ,0x30},
{0x3810 ,0x00},
{0x3811 ,0x08},
{0x3812 ,0x00},
{0x3813 ,0x04},
{0x3814 ,0x01},
{0x3815 ,0x01},
{0x3819 ,0x01},
{0x3820 ,0x06},
{0x3821 ,0x00},
{0x3829 ,0x00},
{0x382a ,0x01},
{0x382b ,0x01},
{0x382d ,0x7f},
{0x3830 ,0x04},
{0x3836 ,0x01},
{0x3837 ,0x00},
{0x3841 ,0x02},
{0x3846 ,0x08},
{0x3847 ,0x07},
{0x3d85 ,0x36},
{0x3d8c ,0x71},
{0x3d8d ,0xcb},
{0x3f0a ,0x00},
{0x4000 ,0xF1},
{0x4001 ,0x40},
{0x4002 ,0x04},
{0x4003 ,0x14},
{0x400e ,0x00},
{0x4011 ,0x00},
{0x401a ,0x00},
{0x401b ,0x00},
{0x401c ,0x00},
{0x401d ,0x00},
{0x401f ,0x00},
{0x4020	,0x00},
{0x4021	,0x10},
{0x4022	,0x09},
{0x4023	,0x13},
{0x4024	,0x0A},
{0x4025	,0x40},
{0x4026	,0x0A},
{0x4027	,0x50},
{0x4028 ,0x00},
{0x4029 ,0x02},
{0x402a ,0x06},
{0x402b ,0x04},
{0x402c ,0x02},
{0x402d ,0x02},
{0x402e ,0x0e},
{0x402f ,0x04},
{0x4302 ,0xff},
{0x4303 ,0xff},
{0x4304 ,0x00},
{0x4305 ,0x00},
{0x4306 ,0x00},
{0x4308 ,0x02},
{0x4500 ,0x6c},
{0x4501 ,0xc4},
{0x4502 ,0x40},
{0x4503 ,0x01},
{0x4601 ,0x42},
{0x4800 ,0x04},
{0x4813 ,0x08},
{0x481f ,0x40},
{0x4829 ,0x78},
{0x4837 ,0x18},
{0x4b00 ,0x2a},
{0x4b0d ,0x00},
{0x4d00 ,0x04},
{0x4d01 ,0x42},
{0x4d02 ,0xd1},
{0x4d03 ,0x93},
{0x4d04 ,0xf5},
{0x4d05 ,0xc1},
{0x5000 ,0xf3},
{0x5001 ,0x11},
{0x5004 ,0x00},
{0x500a ,0x00},
{0x500b ,0x00},
{0x5032 ,0x00},
{0x5040 ,0x00},
{0x5050 ,0x0c},
{0x5500 ,0x00},
{0x5501 ,0x10},
{0x5502 ,0x01},
{0x5503 ,0x0f},
{0x8000 ,0x00},
{0x8001 ,0x00},
{0x8002 ,0x00},
{0x8003 ,0x00},
{0x8004 ,0x00},
{0x8005 ,0x00},
{0x8006 ,0x00},
{0x8007 ,0x00},
{0x8008 ,0x00},
{0x3638 ,0x00},
#ifdef SENSOR_30FPS
{0x380c, 0x0a},	
{0x380d, 0x00},
#endif
#ifdef SENSOR_29FPS
{0x380c, 0x0a},	
{0x380d, 0x34},	
#endif
#ifdef SENSOR_28FPS
{0x380c, 0x0a},	
{0x380d, 0x96},	//26fps:0xb40
#endif
#ifdef SENSOR_15FPS
//drop to 328Mbps@15fps
{0x380c, 0x14},
{0x380d, 0x00},
{0x380e, 0x06},
{0x380f, 0x1a},
{0x0302, 0x1b},
{0x0303, 0x01},
{0x4837, 0x30},
#endif
{0x0100 ,0x01},
#endif
#ifdef OV4688_CROP_2592_1452_EN
{0x3800, 0x00},	//h start
{0x3801, 0x34},
{0x3802, 0x00},	//v start
{0x3803, 0x28},
{0x3804, 0x0a}, //h end
{0x3805, 0x63},
{0x3806, 0x05}, //v end
{0x3807, 0xdb},
{0x3808, 0x0a},
{0x3809, 0x20},
{0x380a, 0x05},
{0x380b, 0xac},
{0x3810, 0x00}, 
{0x3811, 0x0c}, 
{0x3812, 0x00}, 
{0x3813, 0x04}, 
{0x4020, 0x00},
{0x4021, 0x10},
{0x4022, 0x06},
{0x4023, 0x3f},
{0x4024, 0x07},
{0x4025, 0x6c},
{0x4026, 0x07},
{0x4027, 0x7b},
{0x4028, 0x00},
{0x4029, 0x02},
{0x402a, 0x06},
{0x402b, 0x04},
{0x402c, 0x02},
{0x402d, 0x02},
{0x402e, 0x0e},
{0x402f, 0x04},
#endif

};
/*************************
  setting for different size and framerate
 *************************/

SENSOR_SETTING_TABLE sensor__size_1280_720[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_30[][2] = {
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_25[][2] = {
};    
SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_20[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_15[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_12[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_10[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_5[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_3[][2] = {
};

/*************************
  effect change IOCTL
 *************************/
static u8 last_changed = 0;

SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
	u32 reg = 0;
    switch(effect){
	case SNR_EFFECT_SHARPNESS:
		return 0;
    case SNR_EFFECT_SIZE:
        switch(level){
        case VIDEO_SIZE_720P:
            cfg->cur_video_size = VIDEO_SIZE_720P;
            last_changed = 1;
            break;
        default:
            return -1;
        }
        return 0;
    case SNR_EFFECT_FRMRATE:
        last_changed = 1;
        cfg->cur_frame_rate = level;
        return 0;
    case SNR_EFFECT_PROC:
        if(last_changed == 0){
            return 0;
        }
        SENSOR_SETTING_SUPPORT_START
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 30)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 25)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 20)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 15)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 10)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 5)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 3)
        SENSOR_SETTING_SUPPORT_END
        last_changed = 0;
        return 0;
	case SNR_EFFECT_MIRROR:
	{
		if(level){ //mirror on
			reg = libsccb_rd(cfg->sccb_cfg, 0x3821);
			reg |= 0x6;
			libsccb_wr(cfg->sccb_cfg, 0x3821, reg);
		}
		else{ //mirror off
			reg = libsccb_rd(cfg->sccb_cfg, 0x3821);
			reg &= (~0x6);
			libsccb_wr(cfg->sccb_cfg, 0x3821, reg);
		}
		return 0;
	}
	case SNR_EFFECT_FLIP:
	{
		if(level){ //flip on
			reg = libsccb_rd(cfg->sccb_cfg, 0x3820);
			reg |= 0x6;
			libsccb_wr(cfg->sccb_cfg, 0x3820, reg);
		}
		else{ //flip off
			reg = libsccb_rd(cfg->sccb_cfg, 0x3820);
			reg &= (~0x6);
			libsccb_wr(cfg->sccb_cfg, 0x3820, reg);
		}
		return 0;
	}	
	case SNR_EFFECT_FLIP_MIR:
	{
		if(level){ //mirror and flip on
			reg = libsccb_rd(cfg->sccb_cfg, 0x3821);
			reg |= 0x6;
			libsccb_wr(cfg->sccb_cfg, 0x3821, reg);

			reg = libsccb_rd(cfg->sccb_cfg, 0x3820);
			reg |= 0x6;
			libsccb_wr(cfg->sccb_cfg, 0x3820, reg);			
		}
		else{ //mirror and flip off
			reg = libsccb_rd(cfg->sccb_cfg, 0x3821);
			reg &= (~0x6);
			libsccb_wr(cfg->sccb_cfg, 0x3821, reg);

			reg = libsccb_rd(cfg->sccb_cfg, 0x3820);
			reg &= (~0x6);
			libsccb_wr(cfg->sccb_cfg, 0x3820, reg);			
		}
	return 0;
	}
    default:
        return -2;
    }
}

/*************************
  effect description table
  { EFFECT_NAME, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] = 
{
    {SNR_EFFECT_HUE, 0x00, 0x0c, 0x06, 0x06},
    {SNR_EFFECT_SHARPNESS, 0x00, 0x09, 0x03, 0x03},
    {SNR_EFFECT_BRIGHT, 0x01, 0xff, 0x80, 0x80},
    {SNR_EFFECT_SATURATION, 0x00, 0x08, 0x02, 0x02},
    {SNR_EFFECT_CONTRAST, 0x00, 0x08, 0x03, 0x03},
};



/*************************
  sensor detect func
 *************************/
#define OV4688_SENSOR_ID_ADR_HI 0x300a
#define OV4688_SENSOR_ID_ADR_LO 0x300b
#define OV4688_SENSOR_ID_VAL_HI 0x46
#define OV4688_SENSOR_ID_VAL_LO 0x88

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
	debug_printf("4688 detect!\n");
	int hi = libsccb_rd(cfg->sccb_cfg, OV4688_SENSOR_ID_ADR_HI);	
	int lo = libsccb_rd(cfg->sccb_cfg, OV4688_SENSOR_ID_ADR_LO);	
#ifdef OV4688_CROP_2592_1452_EN
	cfg->cur_video_size = (2592<<16) | 1452;
#else
	cfg->cur_video_size = (2688<<16) | 1520;
#endif
	if(cfg->cur_frame_rate == 0)
	{
#ifdef SENSOR_30FPS
		cfg->cur_frame_rate = 30;
#elif defined(SENSOR_29FPS)
		cfg->cur_frame_rate = 29;
#else
		cfg->cur_frame_rate = 28;
#endif
	}
	debug_printf("hi = %x,%x,%x ---- 4M\n", hi, lo);
	return 0;
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir)
{
	WriteReg32(0xc0038c00, 0x0000001f);	// [4]: rgbir_isp_en
            	// [3]: ir_extract_en
            	// [2]: bIRRemovalEnable
            	// [1]: bDNSEnable
            	// [0]: bDPCEnable
	WriteReg32(0xc0038c04, 0x00000000);	//
	WriteReg32(0xc0038c10, 0x00000800);	// [15:0] real_gain
	WriteReg32(0xc0038c14, 0x3bddb3dd);	// CFAArray
	WriteReg32(0xc0038c18, 0x00040010);	// [18] compensate_en | [17:16] bayer_pattern | [15:0] blc
	WriteReg32(0xc0038c1c, 0x00800100);	// [8:0] manual_gain | [31:16] remove_eof_cnt
	WriteReg32(0xc0038c20, 0x803e4744);	// IR cut
	WriteReg32(0xc0038c28, 0x000a03e8);
	
	WriteReg32(0xc0038c2c, 0x0006040c);	//saturate
	WriteReg32(0xc0038c24, 0x01ff0080);	// [24:16] max_gain | [8:0] min_gain
//; CIP
	WriteReg32(0xc0038e00, 0x00000002);	// Configured by Hardware
	WriteReg32(0xc0038e04, 0x00000000);	// Noise list 0|1|2|3
	WriteReg32(0xc0038e08, 0x00000000);	// Noise list 4|5

//; RGBIR UVDns
	WriteReg32(0xc0038f00, 0x00000000);	// Configured by Hardware

	WriteReg32(0xc0038f04, 0x05040302);	// Noise Y list 0|1|2|3 8Bits
	WriteReg32(0xc0038f08, 0x00000706);	// Noise Y list 4|5 8Bits
	WriteReg32(0xc0038f0c, 0x07050301);	// Noise UV list 0|1|2|3 8Bits
	WriteReg32(0xc0038f10, 0x00000908);	// Noise UV list 4|5 8Bits

	WriteReg32(0xc0038f14, 0x00ba9876);	// Add back Y list 0|1|2|3|4|5 4Bits
	WriteReg32(0xc0038f18, 0x00000082);	// Gain threshold 0|1 4Bits
	WriteReg32(0xc0038f1c, 0x00002010);	// V Scale 0|1 6Bits
	return 0;
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	WriteReg32(0xe0088000, 0x8a7f0100);// pre pipe top
                   //; 00-[7]  RAW_DNS_En      
                   //; 00-[6]  BinC_En         
                   //; 00-[5]  DPC_En          
                   //; 00-[4]  DPCOTP_En       
                   //; 00-[3]  AWBG_En
                   //; 00-[2]  Lens_Online_En  
                   //; 00-[1]  LENC_En         
                   //; 00-[0]  Pre_Pipe_Manu_En
                   //; 01-[7] Binc_New_En     
                   //; 01-[6] DNS_3D_En       
                   //; 01-[5] Stretch_En      
                   //; 01-[4] Hist_Stats_En   
                   //; 01-[3] CCM_En          
                   //; 01-[2] AEC_AGC_En      
                   //; 01-[1] Curve_AWB_En    
                   //; 01-[0] CIP_En
                   //; 02-[7:4] reserved       
                   //; 02-[3] dpc_black_en     
                   //; 02-[2] dpc_white_en     
                   //; 02-[1] pdf_en           
                   //; 02-[0] ca_en            
				   //; 03-[7] Manual_Ctrl_En   -           
                   //; 03-[6] Manual_Size_En   
                   //; 03-[5] Lenc_mirror      
                   //; 03-[4] Lenc_flip        
                   //; 03-[3:2] reserved       
                   //; 03-[1] Binc_Mirror      
                   //; 03-[0] Binc_Flip 
	WriteReg32(0xe0088004, 0x00400001); //BLC target 12bit040; input pattern BGGR00
	WriteReg32(0xe0089800, 0x3c000000); //Post pipe top 
				//;[7] Reserved
				//;[6] Anti-shaking enable
				//;[5] UV DNS enable
				//;[4] YUVH SDE enable
				//;[3] JPEG Ycbcr enable
				//;[2] Gamma curve enable
				//;[1] Post Pipe Process Manual Control enable
				//;[0] Sub-Blocks Manual Control enable

	WriteReg32(0xe0089200, 0x11010000);	// [31:24]latch enable, [23:16]01-sensor1,10-sensor2,11-two sensor EDR mode enable

// ================ DMA ================ 
// input size(fw)
#ifdef OV4688_CROP_2592_1452_EN
	WriteReg32(L_BASE_ADDRESS + 0x0000, (968<<16)|(1728));	// w_in|h_in
#else
	WriteReg32(L_BASE_ADDRESS + 0x0000, (1008<<16)|(1792));	// w_in|h_in
#endif

// ================ ISP function ================
#define AEC_AGC_SETTING
#define RGBH_STRETCH_SETTING
#define CURVE_AWB_SETTING
#define CCM_SETTING
#define LENC_SETTING
#define RGBH_CURVE_SETTING
#define RAW_DNS_SETTING
#define UV_DNS_SETTING
#define SDE_SETTING
#define Sharpen_SETTING
#define CA_SETTING
#define TDDNS_SETTING
#define EDR_SETTING

#ifdef AEC_AGC_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0020, 0x02000608);	// AECAGC enable 
// meanY stat(HW)
	WriteReg32(0xe0088b18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe0088b1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe0088b08, 0x08087070);	// left|top|width|height, ratio, bw=7 
	WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe0088b30, 0x03020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe0088b34, 0x00000205);	// [12:8]weight12 | [4:1]stat sampling | [0]stat brightest channel enable
//; Histogram
	WriteReg32(0xe0088c04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe0088c08, 0x00000011);	// top|bottom, bw=13
	WriteReg32(L_BASE_ADDRESS + 0x0038, 0x04010100);	// nSaturationPer2 | nSaturationPer1 | nApplyAnaGainMode | nReadHistMeanSelect
	WriteReg32(L_BASE_ADDRESS + 0x003c, 0x040b00fd);	// nMaxFractalExp | nMaxFractalExp | bAllowFractionalExp | nSaturateRefBin
//; Black and saturate setting
	WriteReg32(0xe0088b0c, 0x10100000);	// low threshold and high threshold
	WriteReg32(0xe0088b04, 0x30303030);	// black weight and saturate weight
	WriteReg32(0xe0088b10, 0x00040006);	// black percentage threshold
	WriteReg32(0xe0088b14, 0x00040004);	// saturate percentage threshold

// AEC para(FW)
	WriteReg32(L_BASE_ADDRESS + 0x0010, 0x08060101);	// [15:8]sensor gain mode
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00105028);	// [31:24]target low,[23:16]target high
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x060f0804);    //[31:16]pStableRange[2] | FastStep | SlowStep
	WriteReg32(L_BASE_ADDRESS + 0x0024, 0x00100139);	// 01f80010; max gain|min gain
	WriteReg32(L_BASE_ADDRESS + 0x0028, (VTS-0x10));	// max exposure
	WriteReg32(L_BASE_ADDRESS + 0x002c, 0x00000010);	// min exposure
//	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x086c0000|VTS);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x08420000|VTS);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
	WriteReg32(L_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(L_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(L_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(L_BASE_ADDRESS + 0x0064, 0x35093508);	// gain
	WriteReg32(L_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(L_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]	
	WriteReg32(L_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(L_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(L_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x007c, 0x030000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(L_BASE_ADDRESS + 0x0080, 0x00000101);	//

// banding
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010207);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#ifdef SENSOR_30FPS
	//48 10032044 10a00de0; 60band|50band,fr=30
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0ed90c60);	// 60band|50band,fr=15
#else
//48 10032044 10a00de0; 60band|50band,fr=30
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0ed90c60);	// 60band|50band,fr=15
#endif
#endif

#ifdef RGBH_STRETCH_SETTING
// rgbh stretch
	WriteReg32(0xe0088d80, 0x01800020);	// [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d84, 0x01003fff);	// [29:16]manual low level, [13:0] min high level
	WriteReg32(0xe0088d88, 0x04010400);	// [31:24]high step thres, [23:16] low step thres, 
		                                // [15:8]high percentage thres, [7:0] low percentage thres, 
#endif

#ifdef CURVE_AWB_SETTING
// Step1 and Step2
	WriteReg32(L_BASE_ADDRESS + 0x0088, 0x80100101);
// AWB stat
	WriteReg32(0xe0088a00, 0x00000000);	// [0]GW
	WriteReg32(0xe0088a0c, 0x000e03e0);	// 000e03e0; min_stat_val|max_stat_val,bw=10 (?)
	WriteReg32(0xe0088a10, 0x00000003);	// step=1(downsample=2)
// AWB calc
	WriteReg32(L_BASE_ADDRESS + 0x008c, 0x02000340);	// maxBgain|maxGBgain
	WriteReg32(L_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10050000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
// XY settings
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x00100020);	//
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfd5afd3e);	//
// low map
	WriteReg32(L_BASE_ADDRESS + 0x00d0, 0x01111222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d8, 0x11122221);	//
	WriteReg32(L_BASE_ADDRESS + 0x00dc, 0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e0, 0x22222210);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e4, 0x00000011);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e8, 0x22222100);	//
	WriteReg32(L_BASE_ADDRESS + 0x00ec, 0x00000022);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f0, 0x22221000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f4, 0x00000222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f8, 0x44440000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00fc, 0x00000444);	//
	WriteReg32(L_BASE_ADDRESS + 0x0100, 0x66660000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0104, 0x00006666);	//
	WriteReg32(L_BASE_ADDRESS + 0x0108, 0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x010c, 0x00068888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0110, 0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0114, 0x00688888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0118, 0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x011c, 0x00688888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0120, 0xfffdc000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0124, 0x00deffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0128, 0xffdc0000);	//
	WriteReg32(L_BASE_ADDRESS + 0x012c, 0x00deffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0130, 0xfdd00000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0134, 0x00deffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0138, 0xed000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x013c, 0x000eeeee);	//
	WriteReg32(L_BASE_ADDRESS + 0x0140, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0144, 0x0000dddd);	//
	WriteReg32(L_BASE_ADDRESS + 0x0148, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x014c, 0x00000000);	//
// middle map
	WriteReg32(L_BASE_ADDRESS + 0x0150,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0154,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0158,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x015c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0160,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0164,0x1fc01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0168,0x1f801f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x016c,0x00000f00);	//
// high map
	WriteReg32(L_BASE_ADDRESS + 0x0170,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0174,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0178,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x017c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0180,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0184,0x0f800f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0188,0x00000f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x018c,0x00000000);	//

	//;; AWB ROIs
	WriteReg32(0xe0088a14,0x00000000);//
	WriteReg32(0xe0088a18,0x00000000);// W | H
	WriteReg32(0xe0088a1c,0x00000000);// 
	WriteReg32(0xe0088a20,0x00000000);// W | H 
	WriteReg32(0xe0088a24,0x00000000);// 
	WriteReg32(0xe0088a28,0x00000000);// W | H
	WriteReg32(0xe0088a2c,0x00000000);//
	WriteReg32(0xe0088a30,0x00000000);// W | H
	//;; ROI weight
	WriteReg32(L_BASE_ADDRESS + 0x01d8,0x00000000);// 04000000;
	WriteReg32(L_BASE_ADDRESS + 0x01dc,0x00000000);// 00006018; Weight 3,4,5
	WriteReg32(L_BASE_ADDRESS + 0x01e0,0x00000000);//
	WriteReg32(L_BASE_ADDRESS + 0x01e4,0x00000000);//
	WriteReg32(L_BASE_ADDRESS + 0x01e8,0x08080000);//
	WriteReg32(L_BASE_ADDRESS + 0x01ec,0x08080808);//
	//;; ROI threshold
	WriteReg32(L_BASE_ADDRESS + 0x01cc,0x00640032);// Thresholds
	WriteReg32(L_BASE_ADDRESS + 0x01d0,0x012c00c8);//
	WriteReg32(L_BASE_ADDRESS + 0x01d4,0x138802bc);//
	WriteReg32(L_BASE_ADDRESS + 0x01f0,0x01010080);// Offset and ROIOption
#endif

#ifdef CCM_SETTING
// CCM
	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x00808000);	// UVSaturate enable | mode
//;48 100323f8 000002bc; UVSaturate threshold
//;48 100323fc 00001388; UVSaturate threshold

	WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xff5201aa);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xfffd0004);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xff87017c);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03c8, 0xff20ffe7);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x020701f9);	// center(c)
	WriteReg32(L_BASE_ADDRESS + 0x03d0, 0x0014fee5);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x0173fff2);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xffdcff9b);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x0173ffb1);	// left(a)
	WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xff75019a);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xffaafff1);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xffaf01a7);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xff50ffaa);	// 
	WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x01000206);	// right(d), [15:0] maunual CT

	WriteReg32(L_BASE_ADDRESS + 0x03b0, 0x00600058);	// thre[0] | thre[1]
	WriteReg32(L_BASE_ADDRESS + 0x03b4, 0x00900084);	// thre[2] | thre[3]
	WriteReg32(L_BASE_ADDRESS + 0x03b8, 0x01300100);	// thre[4] | thre[5]
	WriteReg32(L_BASE_ADDRESS + 0x03ac, 0x80808001);	// [31:24] auto CT enable

	WriteReg32(L_BASE_ADDRESS + 0x0490, 0x024f0001);	// 
	WriteReg32(L_BASE_ADDRESS + 0x0494, 0x000afea7);	//
	WriteReg32(L_BASE_ADDRESS + 0x0498, 0x0166000d);	//
	WriteReg32(L_BASE_ADDRESS + 0x049c, 0xffcdff8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x04a0, 0x0164ffcf);	// last(h)
#endif

#ifdef LENC_SETTING
// LENC
	WriteReg32(L_BASE_ADDRESS + 0x0388, 0x00600058);	// thre[0],thre[1],16bit
	WriteReg32(L_BASE_ADDRESS + 0x038c, 0x00900084);	// thre[2],thre[3],16bit
	//WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x08000e38);	// h_scale|v_scale (1792*1008)
	WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x084b0ecf);	// h_scale|v_scale (1728*968) (0x380000/Value)

	//H light shading
	WriteReg32(L_BASE_ADDRESS + 0x0700,0x525e6e86);
	WriteReg32(L_BASE_ADDRESS + 0x0704,0x3d3f434b);
	WriteReg32(L_BASE_ADDRESS + 0x0708,0x413e3d3c);
	WriteReg32(L_BASE_ADDRESS + 0x070c,0x85425242);
	WriteReg32(L_BASE_ADDRESS + 0x0710,0x3339434b);
	WriteReg32(L_BASE_ADDRESS + 0x0714,0x282a2c2f);
	WriteReg32(L_BASE_ADDRESS + 0x0718,0x29282828);
	WriteReg32(L_BASE_ADDRESS + 0x071c,0x1d462a2e);
	WriteReg32(L_BASE_ADDRESS + 0x0720,0x24282d32);
	WriteReg32(L_BASE_ADDRESS + 0x0724,0x1d1e2022);
	WriteReg32(L_BASE_ADDRESS + 0x0728,0x1e1d1d1d);
	WriteReg32(L_BASE_ADDRESS + 0x072c,0x301e221e);
	WriteReg32(L_BASE_ADDRESS + 0x0730,0x1c1e2324);
	WriteReg32(L_BASE_ADDRESS + 0x0734,0x1617181a);
	WriteReg32(L_BASE_ADDRESS + 0x0738,0x16151515);
	WriteReg32(L_BASE_ADDRESS + 0x073c,0x191d1818);
	WriteReg32(L_BASE_ADDRESS + 0x0740,0x16191c1e);
	WriteReg32(L_BASE_ADDRESS + 0x0744,0x0e0f1113);
	WriteReg32(L_BASE_ADDRESS + 0x0748,0x0f0e0e0e);
	WriteReg32(L_BASE_ADDRESS + 0x074c,0x18131210);
	WriteReg32(L_BASE_ADDRESS + 0x0750,0x10131718);
	WriteReg32(L_BASE_ADDRESS + 0x0754,0x080a0b0d);
	WriteReg32(L_BASE_ADDRESS + 0x0758,0x09080808);
	WriteReg32(L_BASE_ADDRESS + 0x075c,0x0f0f0c0a);
	WriteReg32(L_BASE_ADDRESS + 0x0760,0x0c0f1316);
	WriteReg32(L_BASE_ADDRESS + 0x0764,0x0405070a);
	WriteReg32(L_BASE_ADDRESS + 0x0768,0x05040404);
	WriteReg32(L_BASE_ADDRESS + 0x076c,0x0d0b0806);
	WriteReg32(L_BASE_ADDRESS + 0x0770,0x0a0c1013);
	WriteReg32(L_BASE_ADDRESS + 0x0774,0x02030507);
	WriteReg32(L_BASE_ADDRESS + 0x0778,0x02010101);
	WriteReg32(L_BASE_ADDRESS + 0x077c,0x09090504);
	WriteReg32(L_BASE_ADDRESS + 0x0780,0x080b0f12);
	WriteReg32(L_BASE_ADDRESS + 0x0784,0x00020406);
	WriteReg32(L_BASE_ADDRESS + 0x0788,0x02010000);
	WriteReg32(L_BASE_ADDRESS + 0x078c,0x0a060503);
	WriteReg32(L_BASE_ADDRESS + 0x0790,0x090c1013);
	WriteReg32(L_BASE_ADDRESS + 0x0794,0x02030406);
	WriteReg32(L_BASE_ADDRESS + 0x0798,0x02010101);
	WriteReg32(L_BASE_ADDRESS + 0x079c,0x07090504);
	WriteReg32(L_BASE_ADDRESS + 0x07a0,0x0c0e1214);
	WriteReg32(L_BASE_ADDRESS + 0x07a4,0x04050709);
	WriteReg32(L_BASE_ADDRESS + 0x07a8,0x04040304);
	WriteReg32(L_BASE_ADDRESS + 0x07ac,0x11060806);
	WriteReg32(L_BASE_ADDRESS + 0x07b0,0x0d121518);
	WriteReg32(L_BASE_ADDRESS + 0x07b4,0x07080a0c);
	WriteReg32(L_BASE_ADDRESS + 0x07b8,0x08090806);
	WriteReg32(L_BASE_ADDRESS + 0x07bc,0x03130909);
	WriteReg32(L_BASE_ADDRESS + 0x07c0,0x17151b1b);
	WriteReg32(L_BASE_ADDRESS + 0x07c4,0x0c0d0f10);
	WriteReg32(L_BASE_ADDRESS + 0x07c8,0x0c0c0b0c);
	WriteReg32(L_BASE_ADDRESS + 0x07cc,0x0e140e0e);
	WriteReg32(L_BASE_ADDRESS + 0x07d0,0x191d2025);
	WriteReg32(L_BASE_ADDRESS + 0x07d4,0x13131517);
	WriteReg32(L_BASE_ADDRESS + 0x07d8,0x12111212);
	WriteReg32(L_BASE_ADDRESS + 0x07dc,0x1c151513);
	WriteReg32(L_BASE_ADDRESS + 0x07e0,0x23282e32);
	WriteReg32(L_BASE_ADDRESS + 0x07e4,0x1a1b1d1f);
	WriteReg32(L_BASE_ADDRESS + 0x07e8,0x1a191919);
	WriteReg32(L_BASE_ADDRESS + 0x07ec,0x24221d1b);
	WriteReg32(L_BASE_ADDRESS + 0x07f0,0x333b4653);
	WriteReg32(L_BASE_ADDRESS + 0x07f4,0x272a2a2f);
	WriteReg32(L_BASE_ADDRESS + 0x07f8,0x28272726);
	WriteReg32(L_BASE_ADDRESS + 0x07fc,0x3a332f2b);
	WriteReg32(L_BASE_ADDRESS + 0x0800,0x66636a6a);
	WriteReg32(L_BASE_ADDRESS + 0x0804,0x71676669);
	WriteReg32(L_BASE_ADDRESS + 0x0808,0x676e706a);
	WriteReg32(L_BASE_ADDRESS + 0x080c,0x66686469);
	WriteReg32(L_BASE_ADDRESS + 0x0810,0x69656765);
	WriteReg32(L_BASE_ADDRESS + 0x0814,0x63666967);
	WriteReg32(L_BASE_ADDRESS + 0x0818,0x66636266);
	WriteReg32(L_BASE_ADDRESS + 0x081c,0x6d626865);
	WriteReg32(L_BASE_ADDRESS + 0x0820,0x6668686a);
	WriteReg32(L_BASE_ADDRESS + 0x0824,0x65656364);
	WriteReg32(L_BASE_ADDRESS + 0x0828,0x64646564);
	WriteReg32(L_BASE_ADDRESS + 0x082c,0x64676363);
	WriteReg32(L_BASE_ADDRESS + 0x0830,0x68636567);
	WriteReg32(L_BASE_ADDRESS + 0x0834,0x68686a68);
	WriteReg32(L_BASE_ADDRESS + 0x0838,0x666a6769);
	WriteReg32(L_BASE_ADDRESS + 0x083c,0x66656668);
	WriteReg32(L_BASE_ADDRESS + 0x0840,0x6b696964);
	WriteReg32(L_BASE_ADDRESS + 0x0844,0x706f6e6b);
	WriteReg32(L_BASE_ADDRESS + 0x0848,0x6e6e726f);
	WriteReg32(L_BASE_ADDRESS + 0x084c,0x646b686c);
	WriteReg32(L_BASE_ADDRESS + 0x0850,0x716b6a70);
	WriteReg32(L_BASE_ADDRESS + 0x0854,0x817b7875);
	WriteReg32(L_BASE_ADDRESS + 0x0858,0x7a828282);
	WriteReg32(L_BASE_ADDRESS + 0x085c,0x6f707177);
	WriteReg32(L_BASE_ADDRESS + 0x0860,0x79726e68);
	WriteReg32(L_BASE_ADDRESS + 0x0864,0x98928b7e);
	WriteReg32(L_BASE_ADDRESS + 0x0868,0x91969d9c);
	WriteReg32(L_BASE_ADDRESS + 0x086c,0x74757e87);
	WriteReg32(L_BASE_ADDRESS + 0x0870,0x8275716c);
	WriteReg32(L_BASE_ADDRESS + 0x0874,0xada59b90);
	WriteReg32(L_BASE_ADDRESS + 0x0878,0xa3adb0b2);
	WriteReg32(L_BASE_ADDRESS + 0x087c,0x787e8a95);
	WriteReg32(L_BASE_ADDRESS + 0x0880,0x84777270);
	WriteReg32(L_BASE_ADDRESS + 0x0884,0xb5a99d90);
	WriteReg32(L_BASE_ADDRESS + 0x0888,0xa8b5b8b5);
	WriteReg32(L_BASE_ADDRESS + 0x088c,0x73838b9b);
	WriteReg32(L_BASE_ADDRESS + 0x0890,0x82747169);
	WriteReg32(L_BASE_ADDRESS + 0x0894,0xaba5988d);
	WriteReg32(L_BASE_ADDRESS + 0x0898,0xa1aab2b0);
	WriteReg32(L_BASE_ADDRESS + 0x089c,0x757d8996);
	WriteReg32(L_BASE_ADDRESS + 0x08a0,0x75706d66);
	WriteReg32(L_BASE_ADDRESS + 0x08a4,0x968c887c);
	WriteReg32(L_BASE_ADDRESS + 0x08a8,0x8d949797);
	WriteReg32(L_BASE_ADDRESS + 0x08ac,0x71757b85);
	WriteReg32(L_BASE_ADDRESS + 0x08b0,0x70696965);
	WriteReg32(L_BASE_ADDRESS + 0x08b4,0x7c7a7372);
	WriteReg32(L_BASE_ADDRESS + 0x08b8,0x7a797f7f);
	WriteReg32(L_BASE_ADDRESS + 0x08bc,0x756b7073);
	WriteReg32(L_BASE_ADDRESS + 0x08c0,0x68676765);
	WriteReg32(L_BASE_ADDRESS + 0x08c4,0x6f6d6e6a);
	WriteReg32(L_BASE_ADDRESS + 0x08c8,0x6d70706f);
	WriteReg32(L_BASE_ADDRESS + 0x08cc,0x6b676a6c);
	WriteReg32(L_BASE_ADDRESS + 0x08d0,0x68656466);
	WriteReg32(L_BASE_ADDRESS + 0x08d4,0x69686668);
	WriteReg32(L_BASE_ADDRESS + 0x08d8,0x67676868);
	WriteReg32(L_BASE_ADDRESS + 0x08dc,0x69666767);
	WriteReg32(L_BASE_ADDRESS + 0x08e0,0x63666967);
	WriteReg32(L_BASE_ADDRESS + 0x08e4,0x66666665);
	WriteReg32(L_BASE_ADDRESS + 0x08e8,0x65646765);
	WriteReg32(L_BASE_ADDRESS + 0x08ec,0x68626465);
	WriteReg32(L_BASE_ADDRESS + 0x08f0,0x6f68675f);
	WriteReg32(L_BASE_ADDRESS + 0x08f4,0x64646669);
	WriteReg32(L_BASE_ADDRESS + 0x08f8,0x65666466);
	WriteReg32(L_BASE_ADDRESS + 0x08fc,0x6a716667);
	WriteReg32(L_BASE_ADDRESS + 0x0900,0x8a8e8e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0904,0x888c8b88);
	WriteReg32(L_BASE_ADDRESS + 0x0908,0x888d868c);
	WriteReg32(L_BASE_ADDRESS + 0x090c,0x90888d8c);
	WriteReg32(L_BASE_ADDRESS + 0x0910,0x8a898a8e);
	WriteReg32(L_BASE_ADDRESS + 0x0914,0x88888989);
	WriteReg32(L_BASE_ADDRESS + 0x0918,0x89878a87);
	WriteReg32(L_BASE_ADDRESS + 0x091c,0x8a8a8989);
	WriteReg32(L_BASE_ADDRESS + 0x0920,0x87888988);
	WriteReg32(L_BASE_ADDRESS + 0x0924,0x86878788);
	WriteReg32(L_BASE_ADDRESS + 0x0928,0x87878786);
	WriteReg32(L_BASE_ADDRESS + 0x092c,0x8c888887);
	WriteReg32(L_BASE_ADDRESS + 0x0930,0x87868786);
	WriteReg32(L_BASE_ADDRESS + 0x0934,0x86868786);
	WriteReg32(L_BASE_ADDRESS + 0x0938,0x87868687);
	WriteReg32(L_BASE_ADDRESS + 0x093c,0x89878886);
	WriteReg32(L_BASE_ADDRESS + 0x0940,0x85868688);
	WriteReg32(L_BASE_ADDRESS + 0x0944,0x84858487);
	WriteReg32(L_BASE_ADDRESS + 0x0948,0x85848585);
	WriteReg32(L_BASE_ADDRESS + 0x094c,0x86878686);
	WriteReg32(L_BASE_ADDRESS + 0x0950,0x84858686);
	WriteReg32(L_BASE_ADDRESS + 0x0954,0x84838483);
	WriteReg32(L_BASE_ADDRESS + 0x0958,0x83848383);
	WriteReg32(L_BASE_ADDRESS + 0x095c,0x85868584);
	WriteReg32(L_BASE_ADDRESS + 0x0960,0x83848584);
	WriteReg32(L_BASE_ADDRESS + 0x0964,0x82828284);
	WriteReg32(L_BASE_ADDRESS + 0x0968,0x83818182);
	WriteReg32(L_BASE_ADDRESS + 0x096c,0x87848482);
	WriteReg32(L_BASE_ADDRESS + 0x0970,0x84848485);
	WriteReg32(L_BASE_ADDRESS + 0x0974,0x81818282);
	WriteReg32(L_BASE_ADDRESS + 0x0978,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x097c,0x86848382);
	WriteReg32(L_BASE_ADDRESS + 0x0980,0x83848487);
	WriteReg32(L_BASE_ADDRESS + 0x0984,0x81828282);
	WriteReg32(L_BASE_ADDRESS + 0x0988,0x82818080);
	WriteReg32(L_BASE_ADDRESS + 0x098c,0x84848382);
	WriteReg32(L_BASE_ADDRESS + 0x0990,0x84848486);
	WriteReg32(L_BASE_ADDRESS + 0x0994,0x82818282);
	WriteReg32(L_BASE_ADDRESS + 0x0998,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x099c,0x87848382);
	WriteReg32(L_BASE_ADDRESS + 0x09a0,0x84848587);
	WriteReg32(L_BASE_ADDRESS + 0x09a4,0x82828483);
	WriteReg32(L_BASE_ADDRESS + 0x09a8,0x82828182);
	WriteReg32(L_BASE_ADDRESS + 0x09ac,0x84858482);
	WriteReg32(L_BASE_ADDRESS + 0x09b0,0x85868687);
	WriteReg32(L_BASE_ADDRESS + 0x09b4,0x84848484);
	WriteReg32(L_BASE_ADDRESS + 0x09b8,0x85848483);
	WriteReg32(L_BASE_ADDRESS + 0x09bc,0x8a858585);
	WriteReg32(L_BASE_ADDRESS + 0x09c0,0x87868885);
	WriteReg32(L_BASE_ADDRESS + 0x09c4,0x85858686);
	WriteReg32(L_BASE_ADDRESS + 0x09c8,0x85858585);
	WriteReg32(L_BASE_ADDRESS + 0x09cc,0x88868787);
	WriteReg32(L_BASE_ADDRESS + 0x09d0,0x8888878c);
	WriteReg32(L_BASE_ADDRESS + 0x09d4,0x87888788);
	WriteReg32(L_BASE_ADDRESS + 0x09d8,0x87868786);
	WriteReg32(L_BASE_ADDRESS + 0x09dc,0x88888786);
	WriteReg32(L_BASE_ADDRESS + 0x09e0,0x89888987);
	WriteReg32(L_BASE_ADDRESS + 0x09e4,0x87878989);
	WriteReg32(L_BASE_ADDRESS + 0x09e8,0x87878788);
	WriteReg32(L_BASE_ADDRESS + 0x09ec,0x86888789);
	WriteReg32(L_BASE_ADDRESS + 0x09f0,0x888c878f);
	WriteReg32(L_BASE_ADDRESS + 0x09f4,0x88888989);
	WriteReg32(L_BASE_ADDRESS + 0x09f8,0x88898886);
	WriteReg32(L_BASE_ADDRESS + 0x09fc,0x99898b87);
	
	//A light shading
	WriteReg32(L_BASE_ADDRESS + 0x0a00,0x64748a99);
	WriteReg32(L_BASE_ADDRESS + 0x0a04,0x4d52535e);
	WriteReg32(L_BASE_ADDRESS + 0x0a08,0x534e4c4e);
	WriteReg32(L_BASE_ADDRESS + 0x0a0c,0xab4f6655);
	WriteReg32(L_BASE_ADDRESS + 0x0a10,0x4048545b);
	WriteReg32(L_BASE_ADDRESS + 0x0a14,0x3437383d);
	WriteReg32(L_BASE_ADDRESS + 0x0a18,0x36353435);
	WriteReg32(L_BASE_ADDRESS + 0x0a1c,0x2858393b);
	WriteReg32(L_BASE_ADDRESS + 0x0a20,0x2f343b3d);
	WriteReg32(L_BASE_ADDRESS + 0x0a24,0x272a2a2d);
	WriteReg32(L_BASE_ADDRESS + 0x0a28,0x2a282829);
	WriteReg32(L_BASE_ADDRESS + 0x0a2c,0x40292f29);
	WriteReg32(L_BASE_ADDRESS + 0x0a30,0x25292e2e);
	WriteReg32(L_BASE_ADDRESS + 0x0a34,0x1d202024);
	WriteReg32(L_BASE_ADDRESS + 0x0a38,0x1f1d1d1d);
	WriteReg32(L_BASE_ADDRESS + 0x0a3c,0x24282320);
	WriteReg32(L_BASE_ADDRESS + 0x0a40,0x1c212626);
	WriteReg32(L_BASE_ADDRESS + 0x0a44,0x1316161a);
	WriteReg32(L_BASE_ADDRESS + 0x0a48,0x15131213);
	WriteReg32(L_BASE_ADDRESS + 0x0a4c,0x221b1a16);
	WriteReg32(L_BASE_ADDRESS + 0x0a50,0x15191f1f);
	WriteReg32(L_BASE_ADDRESS + 0x0a54,0x0b0d0f12);
	WriteReg32(L_BASE_ADDRESS + 0x0a58,0x0d0b0a0b);
	WriteReg32(L_BASE_ADDRESS + 0x0a5c,0x1815120e);
	WriteReg32(L_BASE_ADDRESS + 0x0a60,0x0f141a1a);
	WriteReg32(L_BASE_ADDRESS + 0x0a64,0x0408080d);
	WriteReg32(L_BASE_ADDRESS + 0x0a68,0x07050405);
	WriteReg32(L_BASE_ADDRESS + 0x0a6c,0x140f0c09);
	WriteReg32(L_BASE_ADDRESS + 0x0a70,0x0c111618);
	WriteReg32(L_BASE_ADDRESS + 0x0a74,0x0104050a);
	WriteReg32(L_BASE_ADDRESS + 0x0a78,0x03020101);
	WriteReg32(L_BASE_ADDRESS + 0x0a7c,0x0f0c0905);
	WriteReg32(L_BASE_ADDRESS + 0x0a80,0x0a0f1517);
	WriteReg32(L_BASE_ADDRESS + 0x0a84,0x00030308);
	WriteReg32(L_BASE_ADDRESS + 0x0a88,0x02010000);
	WriteReg32(L_BASE_ADDRESS + 0x0a8c,0x10090804);
	WriteReg32(L_BASE_ADDRESS + 0x0a90,0x0b111618);
	WriteReg32(L_BASE_ADDRESS + 0x0a94,0x01040509);
	WriteReg32(L_BASE_ADDRESS + 0x0a98,0x03020101);
	WriteReg32(L_BASE_ADDRESS + 0x0a9c,0x0d0c0805);
	WriteReg32(L_BASE_ADDRESS + 0x0aa0,0x10141a1b);
	WriteReg32(L_BASE_ADDRESS + 0x0aa4,0x0508080c);
	WriteReg32(L_BASE_ADDRESS + 0x0aa8,0x06050405);
	WriteReg32(L_BASE_ADDRESS + 0x0aac,0x180b0d08);
	WriteReg32(L_BASE_ADDRESS + 0x0ab0,0x13191e20);
	WriteReg32(L_BASE_ADDRESS + 0x0ab4,0x0a0d0e12);
	WriteReg32(L_BASE_ADDRESS + 0x0ab8,0x0d0d0b0a);
	WriteReg32(L_BASE_ADDRESS + 0x0abc,0x0a1a0f0e);
	WriteReg32(L_BASE_ADDRESS + 0x0ac0,0x1f1e2624);
	WriteReg32(L_BASE_ADDRESS + 0x0ac4,0x12141617);
	WriteReg32(L_BASE_ADDRESS + 0x0ac8,0x13121112);
	WriteReg32(L_BASE_ADDRESS + 0x0acc,0x181c1715);
	WriteReg32(L_BASE_ADDRESS + 0x0ad0,0x23282c30);
	WriteReg32(L_BASE_ADDRESS + 0x0ad4,0x1b1d1e22);
	WriteReg32(L_BASE_ADDRESS + 0x0ad8,0x1c1a1b1b);
	WriteReg32(L_BASE_ADDRESS + 0x0adc,0x2a1f201c);
	WriteReg32(L_BASE_ADDRESS + 0x0ae0,0x30353d3f);
	WriteReg32(L_BASE_ADDRESS + 0x0ae4,0x2528282c);
	WriteReg32(L_BASE_ADDRESS + 0x0ae8,0x27252426);
	WriteReg32(L_BASE_ADDRESS + 0x0aec,0x322e2b27);
	WriteReg32(L_BASE_ADDRESS + 0x0af0,0x424c5b66);
	WriteReg32(L_BASE_ADDRESS + 0x0af4,0x3437383f);
	WriteReg32(L_BASE_ADDRESS + 0x0af8,0x36343435);
	WriteReg32(L_BASE_ADDRESS + 0x0afc,0x54423f39);
	WriteReg32(L_BASE_ADDRESS + 0x0b00,0x6e6a6d66);
	WriteReg32(L_BASE_ADDRESS + 0x0b04,0x6d696c6b);
	WriteReg32(L_BASE_ADDRESS + 0x0b08,0x6b6c6c6b);
	WriteReg32(L_BASE_ADDRESS + 0x0b0c,0x5f756c6c);
	WriteReg32(L_BASE_ADDRESS + 0x0b10,0x6c6b6a6c);
	WriteReg32(L_BASE_ADDRESS + 0x0b14,0x6a6c6c6c);
	WriteReg32(L_BASE_ADDRESS + 0x0b18,0x6b6b6a6c);
	WriteReg32(L_BASE_ADDRESS + 0x0b1c,0x71646a6a);
	WriteReg32(L_BASE_ADDRESS + 0x0b20,0x6d6c6d6b);
	WriteReg32(L_BASE_ADDRESS + 0x0b24,0x6a6a6b6c);
	WriteReg32(L_BASE_ADDRESS + 0x0b28,0x6a686969);
	WriteReg32(L_BASE_ADDRESS + 0x0b2c,0x696d6b69);
	WriteReg32(L_BASE_ADDRESS + 0x0b30,0x6b6c6c6e);
	WriteReg32(L_BASE_ADDRESS + 0x0b34,0x6b6b6b6b);
	WriteReg32(L_BASE_ADDRESS + 0x0b38,0x6a6b6c6b);
	WriteReg32(L_BASE_ADDRESS + 0x0b3c,0x6a6a696a);
	WriteReg32(L_BASE_ADDRESS + 0x0b40,0x6e6b6c69);
	WriteReg32(L_BASE_ADDRESS + 0x0b44,0x6f706f6e);
	WriteReg32(L_BASE_ADDRESS + 0x0b48,0x6f6f706f);
	WriteReg32(L_BASE_ADDRESS + 0x0b4c,0x6f6c6d6d);
	WriteReg32(L_BASE_ADDRESS + 0x0b50,0x706e6e6e);
	WriteReg32(L_BASE_ADDRESS + 0x0b54,0x74737172);
	WriteReg32(L_BASE_ADDRESS + 0x0b58,0x73747475);
	WriteReg32(L_BASE_ADDRESS + 0x0b5c,0x6d706f71);
	WriteReg32(L_BASE_ADDRESS + 0x0b60,0x73706f6d);
	WriteReg32(L_BASE_ADDRESS + 0x0b64,0x7b7a7873);
	WriteReg32(L_BASE_ADDRESS + 0x0b68,0x797c7d7c);
	WriteReg32(L_BASE_ADDRESS + 0x0b6c,0x70727377);
	WriteReg32(L_BASE_ADDRESS + 0x0b70,0x74727071);
	WriteReg32(L_BASE_ADDRESS + 0x0b74,0x817f7c7b);
	WriteReg32(L_BASE_ADDRESS + 0x0b78,0x7f818282);
	WriteReg32(L_BASE_ADDRESS + 0x0b7c,0x7374777b);
	WriteReg32(L_BASE_ADDRESS + 0x0b80,0x7771716d);
	WriteReg32(L_BASE_ADDRESS + 0x0b84,0x82827e79);
	WriteReg32(L_BASE_ADDRESS + 0x0b88,0x80848282);
	WriteReg32(L_BASE_ADDRESS + 0x0b8c,0x7176797d);
	WriteReg32(L_BASE_ADDRESS + 0x0b90,0x7571706e);
	WriteReg32(L_BASE_ADDRESS + 0x0b94,0x817f7c79);
	WriteReg32(L_BASE_ADDRESS + 0x0b98,0x7e808383);
	WriteReg32(L_BASE_ADDRESS + 0x0b9c,0x7075777b);
	WriteReg32(L_BASE_ADDRESS + 0x0ba0,0x70706f6a);
	WriteReg32(L_BASE_ADDRESS + 0x0ba4,0x7b7a7674);
	WriteReg32(L_BASE_ADDRESS + 0x0ba8,0x797c7c7c);
	WriteReg32(L_BASE_ADDRESS + 0x0bac,0x70717376);
	WriteReg32(L_BASE_ADDRESS + 0x0bb0,0x706c6d6a);
	WriteReg32(L_BASE_ADDRESS + 0x0bb4,0x74737171);
	WriteReg32(L_BASE_ADDRESS + 0x0bb8,0x73737575);
	WriteReg32(L_BASE_ADDRESS + 0x0bbc,0x756e7071);
	WriteReg32(L_BASE_ADDRESS + 0x0bc0,0x6d6b6b6b);
	WriteReg32(L_BASE_ADDRESS + 0x0bc4,0x706e6e6d);
	WriteReg32(L_BASE_ADDRESS + 0x0bc8,0x6f70706f);
	WriteReg32(L_BASE_ADDRESS + 0x0bcc,0x696d6d6f);
	WriteReg32(L_BASE_ADDRESS + 0x0bd0,0x6c6a6c6d);
	WriteReg32(L_BASE_ADDRESS + 0x0bd4,0x6d6d6c6c);
	WriteReg32(L_BASE_ADDRESS + 0x0bd8,0x6d6d6c6d);
	WriteReg32(L_BASE_ADDRESS + 0x0bdc,0x6e6a6c6b);
	WriteReg32(L_BASE_ADDRESS + 0x0be0,0x6c6d6c6a);
	WriteReg32(L_BASE_ADDRESS + 0x0be4,0x6a6b6b6c);
	WriteReg32(L_BASE_ADDRESS + 0x0be8,0x6b696b6a);
	WriteReg32(L_BASE_ADDRESS + 0x0bec,0x6a6d6a6a);
	WriteReg32(L_BASE_ADDRESS + 0x0bf0,0x6d696d65);
	WriteReg32(L_BASE_ADDRESS + 0x0bf4,0x706f7271);
	WriteReg32(L_BASE_ADDRESS + 0x0bf8,0x6e726c6f);
	WriteReg32(L_BASE_ADDRESS + 0x0bfc,0x716a726f);
	WriteReg32(L_BASE_ADDRESS + 0x0c00,0x8b898a95);
	WriteReg32(L_BASE_ADDRESS + 0x0c04,0x8b8b8d87);
	WriteReg32(L_BASE_ADDRESS + 0x0c08,0x8a8e8d8a);
	WriteReg32(L_BASE_ADDRESS + 0x0c0c,0x808f8a8d);
	WriteReg32(L_BASE_ADDRESS + 0x0c10,0x8a8a8a87);
	WriteReg32(L_BASE_ADDRESS + 0x0c14,0x8788888a);
	WriteReg32(L_BASE_ADDRESS + 0x0c18,0x87868787);
	WriteReg32(L_BASE_ADDRESS + 0x0c1c,0x92878a87);
	WriteReg32(L_BASE_ADDRESS + 0x0c20,0x8787898a);
	WriteReg32(L_BASE_ADDRESS + 0x0c24,0x86858686);
	WriteReg32(L_BASE_ADDRESS + 0x0c28,0x86868686);
	WriteReg32(L_BASE_ADDRESS + 0x0c2c,0x84888686);
	WriteReg32(L_BASE_ADDRESS + 0x0c30,0x86868686);
	WriteReg32(L_BASE_ADDRESS + 0x0c34,0x84848686);
	WriteReg32(L_BASE_ADDRESS + 0x0c38,0x84848484);
	WriteReg32(L_BASE_ADDRESS + 0x0c3c,0x89868686);
	WriteReg32(L_BASE_ADDRESS + 0x0c40,0x8585858a);
	WriteReg32(L_BASE_ADDRESS + 0x0c44,0x83838484);
	WriteReg32(L_BASE_ADDRESS + 0x0c48,0x83838382);
	WriteReg32(L_BASE_ADDRESS + 0x0c4c,0x84868484);
	WriteReg32(L_BASE_ADDRESS + 0x0c50,0x83848486);
	WriteReg32(L_BASE_ADDRESS + 0x0c54,0x82828283);
	WriteReg32(L_BASE_ADDRESS + 0x0c58,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c5c,0x84848382);
	WriteReg32(L_BASE_ADDRESS + 0x0c60,0x82838386);
	WriteReg32(L_BASE_ADDRESS + 0x0c64,0x81818182);
	WriteReg32(L_BASE_ADDRESS + 0x0c68,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0c6c,0x81838282);
	WriteReg32(L_BASE_ADDRESS + 0x0c70,0x82828284);
	WriteReg32(L_BASE_ADDRESS + 0x0c74,0x81818281);
	WriteReg32(L_BASE_ADDRESS + 0x0c78,0x81818081);
	WriteReg32(L_BASE_ADDRESS + 0x0c7c,0x84838181);
	WriteReg32(L_BASE_ADDRESS + 0x0c80,0x82828383);
	WriteReg32(L_BASE_ADDRESS + 0x0c84,0x81808282);
	WriteReg32(L_BASE_ADDRESS + 0x0c88,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0c8c,0x84828282);
	WriteReg32(L_BASE_ADDRESS + 0x0c90,0x82838285);
	WriteReg32(L_BASE_ADDRESS + 0x0c94,0x81828181);
	WriteReg32(L_BASE_ADDRESS + 0x0c98,0x80818082);
	WriteReg32(L_BASE_ADDRESS + 0x0c9c,0x82838282);
	WriteReg32(L_BASE_ADDRESS + 0x0ca0,0x83838485);
	WriteReg32(L_BASE_ADDRESS + 0x0ca4,0x82818282);
	WriteReg32(L_BASE_ADDRESS + 0x0ca8,0x82818281);
	WriteReg32(L_BASE_ADDRESS + 0x0cac,0x86828382);
	WriteReg32(L_BASE_ADDRESS + 0x0cb0,0x83858487);
	WriteReg32(L_BASE_ADDRESS + 0x0cb4,0x82828383);
	WriteReg32(L_BASE_ADDRESS + 0x0cb8,0x82838282);
	WriteReg32(L_BASE_ADDRESS + 0x0cbc,0x85848483);
	WriteReg32(L_BASE_ADDRESS + 0x0cc0,0x86858687);
	WriteReg32(L_BASE_ADDRESS + 0x0cc4,0x84838585);
	WriteReg32(L_BASE_ADDRESS + 0x0cc8,0x84838383);
	WriteReg32(L_BASE_ADDRESS + 0x0ccc,0x87868585);
	WriteReg32(L_BASE_ADDRESS + 0x0cd0,0x87878787);
	WriteReg32(L_BASE_ADDRESS + 0x0cd4,0x85868586);
	WriteReg32(L_BASE_ADDRESS + 0x0cd8,0x86868686);
	WriteReg32(L_BASE_ADDRESS + 0x0cdc,0x85888786);
	WriteReg32(L_BASE_ADDRESS + 0x0ce0,0x8888888c);
	WriteReg32(L_BASE_ADDRESS + 0x0ce4,0x87868888);
	WriteReg32(L_BASE_ADDRESS + 0x0ce8,0x87868686);
	WriteReg32(L_BASE_ADDRESS + 0x0cec,0x8e868688);
	WriteReg32(L_BASE_ADDRESS + 0x0cf0,0x8a8c8d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0cf4,0x85878688);
	WriteReg32(L_BASE_ADDRESS + 0x0cf8,0x87888789);
	WriteReg32(L_BASE_ADDRESS + 0x0cfc,0x7b8f8b85);
	
	//Day and CWF light shading
	WriteReg32(L_BASE_ADDRESS + 0x0d00,0x748098ad);
	WriteReg32(L_BASE_ADDRESS + 0x0d04,0x575a5e67);
	WriteReg32(L_BASE_ADDRESS + 0x0d08,0x5e555755);
	WriteReg32(L_BASE_ADDRESS + 0x0d0c,0xb05c725d);
	WriteReg32(L_BASE_ADDRESS + 0x0d10,0x49535f6a);
	WriteReg32(L_BASE_ADDRESS + 0x0d14,0x3d3e4145);
	WriteReg32(L_BASE_ADDRESS + 0x0d18,0x3d3e3b3c);
	WriteReg32(L_BASE_ADDRESS + 0x0d1c,0x2e643f44);
	WriteReg32(L_BASE_ADDRESS + 0x0d20,0x383d434a);
	WriteReg32(L_BASE_ADDRESS + 0x0d24,0x2f303234);
	WriteReg32(L_BASE_ADDRESS + 0x0d28,0x302e2e2e);
	WriteReg32(L_BASE_ADDRESS + 0x0d2c,0x48303531);
	WriteReg32(L_BASE_ADDRESS + 0x0d30,0x2d313639);
	WriteReg32(L_BASE_ADDRESS + 0x0d34,0x23242629);
	WriteReg32(L_BASE_ADDRESS + 0x0d38,0x24232222);
	WriteReg32(L_BASE_ADDRESS + 0x0d3c,0x292f2726);
	WriteReg32(L_BASE_ADDRESS + 0x0d40,0x23272d30);
	WriteReg32(L_BASE_ADDRESS + 0x0d44,0x16181b1e);
	WriteReg32(L_BASE_ADDRESS + 0x0d48,0x18161515);
	WriteReg32(L_BASE_ADDRESS + 0x0d4c,0x26201e1a);
	WriteReg32(L_BASE_ADDRESS + 0x0d50,0x191f242a);
	WriteReg32(L_BASE_ADDRESS + 0x0d54,0x0c0e1115);
	WriteReg32(L_BASE_ADDRESS + 0x0d58,0x0e0c0c0b);
	WriteReg32(L_BASE_ADDRESS + 0x0d5c,0x1c191411);
	WriteReg32(L_BASE_ADDRESS + 0x0d60,0x12171f22);
	WriteReg32(L_BASE_ADDRESS + 0x0d64,0x05080b0e);
	WriteReg32(L_BASE_ADDRESS + 0x0d68,0x07050404);
	WriteReg32(L_BASE_ADDRESS + 0x0d6c,0x15110d0a);
	WriteReg32(L_BASE_ADDRESS + 0x0d70,0x0f141a21);
	WriteReg32(L_BASE_ADDRESS + 0x0d74,0x0203070a);
	WriteReg32(L_BASE_ADDRESS + 0x0d78,0x03010101);
	WriteReg32(L_BASE_ADDRESS + 0x0d7c,0x120e0906);
	WriteReg32(L_BASE_ADDRESS + 0x0d80,0x0d13191f);
	WriteReg32(L_BASE_ADDRESS + 0x0d84,0x01020509);
	WriteReg32(L_BASE_ADDRESS + 0x0d88,0x02010000);
	WriteReg32(L_BASE_ADDRESS + 0x0d8c,0x120c0804);
	WriteReg32(L_BASE_ADDRESS + 0x0d90,0x0f141b20);
	WriteReg32(L_BASE_ADDRESS + 0x0d94,0x0204070a);
	WriteReg32(L_BASE_ADDRESS + 0x0d98,0x04020201);
	WriteReg32(L_BASE_ADDRESS + 0x0d9c,0x100f0906);
	WriteReg32(L_BASE_ADDRESS + 0x0da0,0x14181f25);
	WriteReg32(L_BASE_ADDRESS + 0x0da4,0x07090b0f);
	WriteReg32(L_BASE_ADDRESS + 0x0da8,0x08060506);
	WriteReg32(L_BASE_ADDRESS + 0x0dac,0x1c0f0f0b);
	WriteReg32(L_BASE_ADDRESS + 0x0db0,0x19202629);
	WriteReg32(L_BASE_ADDRESS + 0x0db4,0x0e0f1216);
	WriteReg32(L_BASE_ADDRESS + 0x0db8,0x10100f0c);
	WriteReg32(L_BASE_ADDRESS + 0x0dbc,0x0f211312);
	WriteReg32(L_BASE_ADDRESS + 0x0dc0,0x28272f31);
	WriteReg32(L_BASE_ADDRESS + 0x0dc4,0x18191d1e);
	WriteReg32(L_BASE_ADDRESS + 0x0dc8,0x19171617);
	WriteReg32(L_BASE_ADDRESS + 0x0dcc,0x1f261d1c);
	WriteReg32(L_BASE_ADDRESS + 0x0dd0,0x2e33383b);
	WriteReg32(L_BASE_ADDRESS + 0x0dd4,0x2525282a);
	WriteReg32(L_BASE_ADDRESS + 0x0dd8,0x25232324);
	WriteReg32(L_BASE_ADDRESS + 0x0ddc,0x342a2926);
	WriteReg32(L_BASE_ADDRESS + 0x0de0,0x3c414952);
	WriteReg32(L_BASE_ADDRESS + 0x0de4,0x30323436);
	WriteReg32(L_BASE_ADDRESS + 0x0de8,0x31302f2f);
	WriteReg32(L_BASE_ADDRESS + 0x0dec,0x3d3c3533);
	WriteReg32(L_BASE_ADDRESS + 0x0df0,0x505d6c7d);
	WriteReg32(L_BASE_ADDRESS + 0x0df4,0x4344484d);
	WriteReg32(L_BASE_ADDRESS + 0x0df8,0x45424142);
	WriteReg32(L_BASE_ADDRESS + 0x0dfc,0x61524e47);
	WriteReg32(L_BASE_ADDRESS + 0x0e00,0x83837e98);
	WriteReg32(L_BASE_ADDRESS + 0x0e04,0x87818480);
	WriteReg32(L_BASE_ADDRESS + 0x0e08,0x7f81857f);
	WriteReg32(L_BASE_ADDRESS + 0x0e0c,0x77888085);
	WriteReg32(L_BASE_ADDRESS + 0x0e10,0x81808180);
	WriteReg32(L_BASE_ADDRESS + 0x0e14,0x7d7f7f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e18,0x7e7e7c7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e1c,0x847c7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e20,0x7f7f8180);
	WriteReg32(L_BASE_ADDRESS + 0x0e24,0x7f7e7e7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e28,0x7d7e7e7d);
	WriteReg32(L_BASE_ADDRESS + 0x0e2c,0x7f7f7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e30,0x807f7f84);
	WriteReg32(L_BASE_ADDRESS + 0x0e34,0x7d7e7f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e38,0x7e7d7d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e3c,0x807e7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e40,0x7e7e807e);
	WriteReg32(L_BASE_ADDRESS + 0x0e44,0x7d7e7d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e48,0x7d7d7d7d);
	WriteReg32(L_BASE_ADDRESS + 0x0e4c,0x807f7d7d);
	WriteReg32(L_BASE_ADDRESS + 0x0e50,0x7e7e807e);
	WriteReg32(L_BASE_ADDRESS + 0x0e54,0x7e7d7e7d);
	WriteReg32(L_BASE_ADDRESS + 0x0e58,0x7d7e7d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e5c,0x7e7e7d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e60,0x7e7e7f7d);
	WriteReg32(L_BASE_ADDRESS + 0x0e64,0x7f7e7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e68,0x7e7f7f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e6c,0x817f7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e70,0x7e7e7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e74,0x7f7f7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e78,0x7f808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e7c,0x7d807e7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e80,0x7e7d7e81);
	WriteReg32(L_BASE_ADDRESS + 0x0e84,0x81807f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0e88,0x80818180);
	WriteReg32(L_BASE_ADDRESS + 0x0e8c,0x7f7f7e80);
	WriteReg32(L_BASE_ADDRESS + 0x0e90,0x7e7e7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e94,0x807f7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0e98,0x80808080);
	WriteReg32(L_BASE_ADDRESS + 0x0e9c,0x817f7f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0ea0,0x7d7d7e81);
	WriteReg32(L_BASE_ADDRESS + 0x0ea4,0x7f7f7d7e);
	WriteReg32(L_BASE_ADDRESS + 0x0ea8,0x7f7f7f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0eac,0x7e807f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0eb0,0x7e7e807f);
	WriteReg32(L_BASE_ADDRESS + 0x0eb4,0x7e7e7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0eb8,0x7f7e7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0ebc,0x847e7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0ec0,0x7e7f7e81);
	WriteReg32(L_BASE_ADDRESS + 0x0ec4,0x7f7e7e7e);
	WriteReg32(L_BASE_ADDRESS + 0x0ec8,0x7e807e7f);
	WriteReg32(L_BASE_ADDRESS + 0x0ecc,0x817f7e7f);
	WriteReg32(L_BASE_ADDRESS + 0x0ed0,0x807f807e);
	WriteReg32(L_BASE_ADDRESS + 0x0ed4,0x807f8180);
	WriteReg32(L_BASE_ADDRESS + 0x0ed8,0x7f7f807f);
	WriteReg32(L_BASE_ADDRESS + 0x0edc,0x7f817f7f);
	WriteReg32(L_BASE_ADDRESS + 0x0ee0,0x7f7f8080);
	WriteReg32(L_BASE_ADDRESS + 0x0ee4,0x807f7f80);
	WriteReg32(L_BASE_ADDRESS + 0x0ee8,0x7f7f7f7e);
	WriteReg32(L_BASE_ADDRESS + 0x0eec,0x847e807e);
	WriteReg32(L_BASE_ADDRESS + 0x0ef0,0x827f8281);
	WriteReg32(L_BASE_ADDRESS + 0x0ef4,0x7f818280);
	WriteReg32(L_BASE_ADDRESS + 0x0ef8,0x80807f80);
	WriteReg32(L_BASE_ADDRESS + 0x0efc,0x7a857e82);
	WriteReg32(L_BASE_ADDRESS + 0x0f00,0x888e8198);
	WriteReg32(L_BASE_ADDRESS + 0x0f04,0x8587848a);
	WriteReg32(L_BASE_ADDRESS + 0x0f08,0x878a8288);
	WriteReg32(L_BASE_ADDRESS + 0x0f0c,0x818b8689);
	WriteReg32(L_BASE_ADDRESS + 0x0f10,0x86868983);
	WriteReg32(L_BASE_ADDRESS + 0x0f14,0x84838584);
	WriteReg32(L_BASE_ADDRESS + 0x0f18,0x83828583);
	WriteReg32(L_BASE_ADDRESS + 0x0f1c,0x89828584);
	WriteReg32(L_BASE_ADDRESS + 0x0f20,0x84868586);
	WriteReg32(L_BASE_ADDRESS + 0x0f24,0x84848384);
	WriteReg32(L_BASE_ADDRESS + 0x0f28,0x83838282);
	WriteReg32(L_BASE_ADDRESS + 0x0f2c,0x81848383);
	WriteReg32(L_BASE_ADDRESS + 0x0f30,0x84848486);
	WriteReg32(L_BASE_ADDRESS + 0x0f34,0x82818383);
	WriteReg32(L_BASE_ADDRESS + 0x0f38,0x81828183);
	WriteReg32(L_BASE_ADDRESS + 0x0f3c,0x86828281);
	WriteReg32(L_BASE_ADDRESS + 0x0f40,0x81848386);
	WriteReg32(L_BASE_ADDRESS + 0x0f44,0x82838283);
	WriteReg32(L_BASE_ADDRESS + 0x0f48,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f4c,0x84828281);
	WriteReg32(L_BASE_ADDRESS + 0x0f50,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0f54,0x82828382);
	WriteReg32(L_BASE_ADDRESS + 0x0f58,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f5c,0x80838281);
	WriteReg32(L_BASE_ADDRESS + 0x0f60,0x81838180);
	WriteReg32(L_BASE_ADDRESS + 0x0f64,0x81818182);
	WriteReg32(L_BASE_ADDRESS + 0x0f68,0x80818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f6c,0x7f828181);
	WriteReg32(L_BASE_ADDRESS + 0x0f70,0x81828281);
	WriteReg32(L_BASE_ADDRESS + 0x0f74,0x82808181);
	WriteReg32(L_BASE_ADDRESS + 0x0f78,0x80808180);
	WriteReg32(L_BASE_ADDRESS + 0x0f7c,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0f80,0x8182817e);
	WriteReg32(L_BASE_ADDRESS + 0x0f84,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f88,0x81808180);
	WriteReg32(L_BASE_ADDRESS + 0x0f8c,0x80817f81);
	WriteReg32(L_BASE_ADDRESS + 0x0f90,0x8181837f);
	WriteReg32(L_BASE_ADDRESS + 0x0f94,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f98,0x80818181);
	WriteReg32(L_BASE_ADDRESS + 0x0f9c,0x81818180);
	WriteReg32(L_BASE_ADDRESS + 0x0fa0,0x82828281);
	WriteReg32(L_BASE_ADDRESS + 0x0fa4,0x81818181);
	WriteReg32(L_BASE_ADDRESS + 0x0fa8,0x81808181);
	WriteReg32(L_BASE_ADDRESS + 0x0fac,0x84828181);
	WriteReg32(L_BASE_ADDRESS + 0x0fb0,0x83838183);
	WriteReg32(L_BASE_ADDRESS + 0x0fb4,0x82828282);
	WriteReg32(L_BASE_ADDRESS + 0x0fb8,0x82838281);
	WriteReg32(L_BASE_ADDRESS + 0x0fbc,0x81848382);
	WriteReg32(L_BASE_ADDRESS + 0x0fc0,0x82828380);
	WriteReg32(L_BASE_ADDRESS + 0x0fc4,0x83828282);
	WriteReg32(L_BASE_ADDRESS + 0x0fc8,0x82828382);
	WriteReg32(L_BASE_ADDRESS + 0x0fcc,0x85828382);
	WriteReg32(L_BASE_ADDRESS + 0x0fd0,0x84848487);
	WriteReg32(L_BASE_ADDRESS + 0x0fd4,0x82818284);
	WriteReg32(L_BASE_ADDRESS + 0x0fd8,0x82818281);
	WriteReg32(L_BASE_ADDRESS + 0x0fdc,0x84848382);
	WriteReg32(L_BASE_ADDRESS + 0x0fe0,0x8584867c);
	WriteReg32(L_BASE_ADDRESS + 0x0fe4,0x82848484);
	WriteReg32(L_BASE_ADDRESS + 0x0fe8,0x84838285);
	WriteReg32(L_BASE_ADDRESS + 0x0fec,0x83858484);
	WriteReg32(L_BASE_ADDRESS + 0x0ff0,0x8289868b);
	WriteReg32(L_BASE_ADDRESS + 0x0ff4,0x89838489);
	WriteReg32(L_BASE_ADDRESS + 0x0ff8,0x84828681);
	WriteReg32(L_BASE_ADDRESS + 0x0ffc,0x86838782);
#endif

#ifdef RGBH_CURVE_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0200, 0x00740040);	//
	WriteReg32(L_BASE_ADDRESS + 0x0204, 0x00e400b4);	//
	WriteReg32(L_BASE_ADDRESS + 0x0208, 0x01380114);	//
	WriteReg32(L_BASE_ADDRESS + 0x020c, 0x01780158);	//
	WriteReg32(L_BASE_ADDRESS + 0x0210, 0x01b80198);	//
	WriteReg32(L_BASE_ADDRESS + 0x0214, 0x01f001d4);	//
	WriteReg32(L_BASE_ADDRESS + 0x0218, 0x0228020c);	//
	WriteReg32(L_BASE_ADDRESS + 0x021c, 0x025c0244);	//
	WriteReg32(L_BASE_ADDRESS + 0x0220, 0x02940278);	//
	WriteReg32(L_BASE_ADDRESS + 0x0224, 0x02c802ac);	//
	WriteReg32(L_BASE_ADDRESS + 0x0228, 0x02f802dc);	//
	WriteReg32(L_BASE_ADDRESS + 0x022c, 0x03280310);	//
	WriteReg32(L_BASE_ADDRESS + 0x0230, 0x035c0344);	//
	WriteReg32(L_BASE_ADDRESS + 0x0234, 0x03880370);	//
	WriteReg32(L_BASE_ADDRESS + 0x0238, 0x03b803a0);	//
	WriteReg32(L_BASE_ADDRESS + 0x023c, 0x03e803cc);	//
	WriteReg32(L_BASE_ADDRESS + 0x0240, 0x004003ff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0244, 0x00b40074);	//
	WriteReg32(L_BASE_ADDRESS + 0x0248, 0x011400e4);	//
	WriteReg32(L_BASE_ADDRESS + 0x024c, 0x01580138);	//
	WriteReg32(L_BASE_ADDRESS + 0x0250, 0x01980178);	//
	WriteReg32(L_BASE_ADDRESS + 0x0254, 0x01d401b8);	//
	WriteReg32(L_BASE_ADDRESS + 0x0258, 0x020c01f0);	//
	WriteReg32(L_BASE_ADDRESS + 0x025c, 0x02440228);	//
	WriteReg32(L_BASE_ADDRESS + 0x0260, 0x0278025c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0264, 0x02ac0294);	//
	WriteReg32(L_BASE_ADDRESS + 0x0268, 0x02dc02c8);	//
	WriteReg32(L_BASE_ADDRESS + 0x026c, 0x031002f8);	//
	WriteReg32(L_BASE_ADDRESS + 0x0270, 0x03440328);	//
	WriteReg32(L_BASE_ADDRESS + 0x0274, 0x0370035c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0278, 0x03a00388);	//
	WriteReg32(L_BASE_ADDRESS + 0x027c, 0x03cc03b8);	//
	WriteReg32(L_BASE_ADDRESS + 0x0280, 0x03ff03e8);	//
	WriteReg32(L_BASE_ADDRESS + 0x0284, 0x00740040);	//
	WriteReg32(L_BASE_ADDRESS + 0x0288, 0x00e400b4);	//
	WriteReg32(L_BASE_ADDRESS + 0x028c, 0x01380114);	//
	WriteReg32(L_BASE_ADDRESS + 0x0290, 0x01780158);	//
	WriteReg32(L_BASE_ADDRESS + 0x0294, 0x01b80198);	//
	WriteReg32(L_BASE_ADDRESS + 0x0298, 0x01f001d4);	//
	WriteReg32(L_BASE_ADDRESS + 0x029c, 0x0228020c);	//
	WriteReg32(L_BASE_ADDRESS + 0x02a0, 0x025c0244);	//
	WriteReg32(L_BASE_ADDRESS + 0x02a4, 0x02940278);	//
	WriteReg32(L_BASE_ADDRESS + 0x02a8, 0x02c802ac);	//
	WriteReg32(L_BASE_ADDRESS + 0x02ac, 0x02f802dc);	//
	WriteReg32(L_BASE_ADDRESS + 0x02b0, 0x03280310);	//
	WriteReg32(L_BASE_ADDRESS + 0x02b4, 0x035c0344);	//
	WriteReg32(L_BASE_ADDRESS + 0x02b8, 0x03880370);	//
	WriteReg32(L_BASE_ADDRESS + 0x02bc, 0x03b803a0);	//
	WriteReg32(L_BASE_ADDRESS + 0x02c0, 0x03e803cc);	//
#endif

#ifdef RAW_DNS_SETTING
 //RAWDNS
 	WriteReg32(0xe0088500, 0x00060007);	// sigma[0]|sigma[1] ;000a0010
 	WriteReg32(0xe0088504, 0x0008000c);	// sigma[2]|sigma[3] ;001a0026
 	WriteReg32(0xe0088508, 0x00160028);	// sigma[4]|sigma[5] ;00300040
 	WriteReg32(0xe008850c, 0x1a1a1616);	// Gns[0]|Gns[1]|Gns[2]|Gns[3]
	WriteReg32(0xe0088510, 0x14141a1a);	// Gns[4]|Gns[5]|Rbns[0]|Rbns[1]
	WriteReg32(0xe0088514, 0x16161414);	// Rbns[2]|Rbns[3]|Rbns[4]|Rbns[5]
#endif

#ifdef UV_DNS_SETTING
	WriteReg32(0xe0089b00, 0x00020480);
	WriteReg32(0xe0089b04, 0x80010101);	// 1x|2x|4x
	WriteReg32(0xe0089b08, 0x01010101);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b0c, 0x01020408);	// 128x|1x|2x|4x
	WriteReg32(0xe0089b10, 0x1014181c);	// 8x|16x|32x|64x	
	WriteReg32(0xe0089b14, 0x20000000);	// 128x
#endif

#ifdef SDE_SETTING
	WriteReg32(0xe0089a04,0x00000400);
	WriteReg32(0xe0089a20,0x80806262);
	WriteReg32(0xe0089a24,0x62c65f63);
	WriteReg32(0xe0089a28,0x4e4e4eb2);
	WriteReg32(0xe0089a2c,0x959d7676);
	WriteReg32(0xe0089a30,0x76da121f);
	WriteReg32(0xe0089a34,0x120c120c);
	WriteReg32(0xe0089a38,0x88888800);
#endif

#ifdef Sharpen_SETTING
	WriteReg32(0xe0088800, 0x3f0462ff);	// cip
					// 00 - [7] 
					// 00 - [6] manual_mode
					// 00 - [5] bIfGbGr
					// 00 - [4] bIfCbCrDns
					// 00 - [3] bIfAntiClrAls
					// 00 - [2] bIfShapren
					// 00 - [1] bIfSharpenNS
					// 00 - [0] bIfDDns
					// 01 - [6:4] GDnsTSlope 
 					// 01 - [3:0] WSlopeClr
 					// 02 - [4:0] CombW
					// 03 - [7:0] WSlope
	WriteReg32(0xe0088804, 0x01040810);	// DirGDNST0|1|2|3
	WriteReg32(0xe0088808, 0x12140808);	// [31:16] DirGDNST4|5
					// 0a - [5:0] DirGDNSLevel0
					// 0b - [5:0] DirGDNSLevel1
	WriteReg32(0xe008880c, 0x08080b10);	// 0c - [5:0] DirGDNSLevel2
					// 0d - [5:0] DirGDNSLevel3
					// 0e - [5:0] DirGDNSLevel4
					// 0f - [5:0] DirGDNSLevel5
	WriteReg32(0xe0088810, 0x40404040);	// CleanSharpT0|1|2|3
	WriteReg32(0xe0088814, 0x4040ffff);	// [31:16] CleanSharpT4|5
				// [15:0] CleanSharpT0|1
	WriteReg32(0xe0088818, 0xffffffff);	// CleanSharpT2|3|4|5
	WriteReg32(0xe008881c, 0x10182028);	// Tintp0|1|2|3
	WriteReg32(0xe0088820, 0x38481018);	// Tintp4|5
				// TintpHV0|1
	WriteReg32(0xe0088824, 0x20283848);	// TintpHV0|1|2|3
	WriteReg32(0xe0088828, 0x20304050);	// NV0|1|2|3
	WriteReg32(0xe008882c, 0x78a00404);	// [31:16] NV4|5
					// 0e - [0:5] VNGLevel0
					// 0f - [0:5] VNGLevel1
	WriteReg32(0xe0088830, 0x04080808);	// 30 - [0:5] VNGLevel2
					// 31 - [0:5] VNGLevel3
					// 32 - [0:5] VNGLevel4
					// 33 - [0:5] VNGLevel5
	WriteReg32(0xe0088834, 0x04060810);	// 34 - [0:5] LPG0
					// 35 - [0:5] LPG1
					// 36 - [0:5] LPG2
					// 37 - [0:5] LPG3
	WriteReg32(0xe0088838, 0x10142020);	// 38 - [0:5] LPG4
					// 39 - [0:5] LPG5
					// 3a - [0:5] CbrDbsLevel0
					// 3b - [0:5] CbrDbsLevel1
	WriteReg32(0xe008883c, 0x20202020);	// 3c - [0:5] CbrDbsLevel2
					// 3d - [0:5] CbrDbsLevel3
					// 3e - [0:5] CbrDbsLevel4
					// 3f - [0:5] CbrDbsLevel5
	WriteReg32(0xe0088840, 0x00500078);	// [27:16] - TCbCrH0
					// [11:0] - TCbCrH1
	WriteReg32(0xe0088844, 0x00a00140);	// [27:16] - TCbCrH2
					// [11:0] - TCbCrH3
	WriteReg32(0xe0088848, 0x01680190);	// [27:16] - TCbCrH4
					// [11:0] - TCbCrH5
	WriteReg32(0xe008884c, 0x00c80078);	// [27:16] - TCbCrV0
					// [11:0] - TCbCrV1
	WriteReg32(0xe0088850, 0x00a00140);	// [27:16] - TCbCrV2
					// [11:0] - TCbCrV3
	WriteReg32(0xe0088854, 0x01680190);	// [27:16] - TCbCrV4
					// [11:0] - TCbCrV5
	WriteReg32(0xe0088858, 0x00123333);	// [23:20] - TCbrOffset0
					// [19:16] - TCbrOffset1
					// [15:12] - TCbrOffset2
					// [11:8] - TCbrOffset3
					// [7:4] - TCbrOffset4
					// [3:0] - TCbrOffset5
	WriteReg32(0xe008885c, 0x6455463c);	// CbCrAlsHf0|1|2|3
	WriteReg32(0xe0088860, 0x3732c83c);	// [31:16] CbCrAlsHf4|5|
					// [15:0] CbCrAlsMean0|1
	WriteReg32(0xe0088864, 0x50505050);	// CbCrAlsMean2|3|4|5
	WriteReg32(0xe0088868, 0x0c0b0a08);	// CbrImpulse0|1|2|3
	WriteReg32(0xe008886c, 0x00101204);	// [23:16] CbrImpulse4
					// [15:8] CbrImpulse5
					// [2:0] CbrImpulseThrShift
	WriteReg32(0xe0088870, 0x24242222);	// 70 - [5:0] SharpenLevel0
					// 71 - [5:0] SharpenLevel1
					// 72 - [5:0] SharpenLevel2
					// 73 - [5:0] SharpenLevel3
	WriteReg32(0xe0088874, 0x20200808);	// 74 - [5:0] SharpenLevel4
					// 75 - [5:0] SharpenLevel5
					// 76 - [7:0] SharpenThr0
					// 77 - [7:0] SharpenThr1
	WriteReg32(0xe0088878, 0x0a0c0f12);	// SharpenThr2|3|4|5
	WriteReg32(0xe008887c, 0x0002060a);	// 7d - [5:0] LocalRatio0
				// 7e - [5:0] LocalRatio1
				// 7f - [5:0] LocalRatio2
	WriteReg32(0xe0088880, 0x14141414);	// 80 - [5:0] SharpenHalo0
					// 81 - [5:0] SharpenHalo1
					// 82 - [5:0] SharpenHalo2
					// 83 - [5:0] SharpenHalo3
	WriteReg32(0xe0088884, 0x1414503a);	// 84 - [5:0] SharpenHalo4
					// 85 - [5:0] SharpenHalo5
					// 86 - [7:0] SharpenCoef0 (48)
					// 87 - [7:0] SharpenCoef1 (39)
	WriteReg32(0xe0088888, 0x17060104);	// [31:15] SharpenCoef2|3|4 (1b, 07, 01)
					// [7:0] DarkOffset
	WriteReg32(0xe008888c, 0x44444444);	// [31:28] SharpenHDelta0 (4)
					// [27:24] SharpenHDelta1 (4)
					// [23:20] SharpenHDelta2 (4)
					// [19:16] SharpenHDelta3 (4)
					// [15:12] SharpenHDelta4 (4)
					// [11:8] SharpenHDelta5 (4)
					// [7:4] SharpenHDelta6 (4)
					// [3:0] ShapenHDelta7 (4)
	WriteReg32(0xe0088890, 0x141b1f1e);	// 90 - [5:0] SharpenHGain0 (08)
				// 91 - [5:0] SharpenHGain1 (0c)
				// 92 - [5:0] SharpenHGain2 (0a)
				// 93 - [5:0] SharpenHGain3 (09)
	WriteReg32(0xe0088894, 0x19181a14);	// 94 - [5:0] SharpenHGain4 (08)
				// 95 - [5:0] SharpenHGain5 (0a)
				// 96 - [5:0] SharpenHGain6 (0a)
				// 97 - [5:0] SharpenHGain7 (0a)
	WriteReg32(0xe0088898, 0x0e000010);	// 9b - [5:0] SharpenHGain8 (06)
					// 99 - [5:0] localSymGThr0
					// 9a - [5:0] localSymGThr1
					// 9b - [5:0] localSymGThr2
	WriteReg32(0xe008889c, 0x00101820);	// 9c - reserved
					// 9d - [5:0] localDNSThr0
					// 9e - [5:0] localDNSThr1
					// 9f - [5:0] localDNSThr2
	WriteReg32(L_BASE_ADDRESS + 0x02c4, 0x025803ff);	// c4 - [1:0] CurList32[9:8]
				// c5 - [7:0] CurList32[7:0]
				// c6 - [2:0] HFreqGain0[10:8]
				// c7 - [7:0] HFreqGain0[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02c8, 0x01a40200);	// c8 - [2:0] HFreqGain1[10:8]
				// c9 - [7:0] HFreqGain1[7:0]
				// ca - [2:0] HFreqGain2[10:8]
				// cb - [7:0] HFreqGain2[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02cc, 0x01200158);	// cc - [2:0] HFreqGain3[10:8]
				// cd - [7:0] HFreqGain3[7:0]
				// ce - [2:0] HFreqGain4[10:8]
				// cf - [7:0] HFreqGain4[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d0, 0x00d000f8);	// d0 - [2:0] HFreqGain5[10:8]
				// d1 - [7:0] HFreqGain5[7:0]
				// d2 - [2:0] HFreqGain6[10:8]
				// d3 - [7:0] HFreqGain6[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d4, 0x008000aa);	// d4 - [2:0] HFreqGain7[10:8]
				// d5 - [7:0] HFreqGain7[7:0]
#endif

#ifdef CA_SETTING
	WriteReg32(0xe0088700,0x00010104);// Alpha[0]|Alpha[1]|Beta[0]|Beta[1]
	WriteReg32(0xe0088714,0x03010104);// Coef[0]|Coef[1]|Coef[2]|Coef[3]
#endif

#ifdef TDDNS_SETTING
	WriteReg32(0xe0088550,0x00040010); // nIfComb on, [15:0] TSigma[0]
	WriteReg32(0xe0088554,0x00120014); // TSigma[1]~[2]
	WriteReg32(0xe0088558,0x00180020); // TSigma[3]~[4]
	WriteReg32(0xe008855c,0x00608006); // [31:16] Tsigma[5],[15:8] TmpW,[7:0] Ratio
	WriteReg32(0xe0088560,0x28080000); // [31:24] PattThr,[23:16] ChgThr,[15:0] Overlap x
#endif

#ifdef EDR_SETTING
//EDR
	WriteReg32(0xe0089400, 0x3f000002); //
	WriteReg32(L_BASE_ADDRESS + 0x0400, 0x01008800);	//	
	WriteReg32(L_BASE_ADDRESS + 0x0428, 0x00000001);
	WriteReg32(L_BASE_ADDRESS + 0x042c, 0x01000100);	//

	WriteReg32(L_BASE_ADDRESS + 0x0438, 0x00800020);	//
	WriteReg32(L_BASE_ADDRESS + 0x043c, 0x10200020);	//
	WriteReg32(L_BASE_ADDRESS + 0x0440, 0x100003a0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0444, 0x01200020);	//
	WriteReg32(L_BASE_ADDRESS + 0x0448, 0x00800100);	//
	WriteReg32(L_BASE_ADDRESS + 0x044c, 0x01040500);	//

	WriteReg32(L_BASE_ADDRESS + 0x0450, 0x40302010);	//
	WriteReg32(L_BASE_ADDRESS + 0x0454, 0x80706050);	//
	WriteReg32(L_BASE_ADDRESS + 0x0458, 0xc0b0a090);	//
	WriteReg32(L_BASE_ADDRESS + 0x045c, 0x80f0e0d0);	//

	WriteReg32(L_BASE_ADDRESS + 0x0460, 0x02000020);	//
	WriteReg32(L_BASE_ADDRESS + 0x0464, 0x10204040);	//
	WriteReg32(L_BASE_ADDRESS + 0x0468, 0x80200101);	//
	WriteReg32(L_BASE_ADDRESS + 0x046c, 0x78681801);	//

	WriteReg32(L_BASE_ADDRESS + 0x0470, 0x40302010);	//
	WriteReg32(L_BASE_ADDRESS + 0x0474, 0x80706050);	//
	WriteReg32(L_BASE_ADDRESS + 0x0478, 0xc0b0a090);	//
	WriteReg32(L_BASE_ADDRESS + 0x047c, 0x00f0e0d0);	//

	WriteReg32(L_BASE_ADDRESS + 0x0480, 0x02010201);	//
	WriteReg32(L_BASE_ADDRESS + 0x0484, 0x88880180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0488, 0x40201040);	//
	WriteReg32(L_BASE_ADDRESS + 0x048c, 0x01010180);	//
#endif	
	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};

/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
