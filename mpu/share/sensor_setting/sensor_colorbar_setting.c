#include "includes.h"

#define SENSOR_NAME_INTERNAL colorbar 
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#define SENSOR_SCCB_ID_INTERNAL 0x00
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_YUV422
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_DVP

/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

/*************************
  Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {
};

SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level){
	return 0;	
}

SENSOR_SETTING_FUNC t_sensoreffect_one tss[] =
{
};

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{

	cfg->cur_video_size = (1280<<16)|720;
//	cfg->cur_video_size = (640<<16)|480;
    debug_printf("sensor use color bar, output size: %x\n", cfg->cur_video_size);

	return 0;
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	return 0;	
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};
/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
