/***IQ version: 2.1.4***/
#include "includes.h"
#define SENSOR_NAME_INTERNAL 9732
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#ifdef CONFIG_SENSOR_SID_HIGH
#define SENSOR_SCCB_ID_INTERNAL 0x20
#else
#define SENSOR_SCCB_ID_INTERNAL 0x6c
#endif
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_RAW10
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_MIPI_1LN
//#define BAND50HZ
#define SENSOR_30FPS

//#define SENSOR_CROP_1040_600

#ifdef SENSOR_CROP_1040_600
#define SENSOR_DEFAULT_SIZE 	((1040 << 16) | 600)
#else
#define SENSOR_DEFAULT_SIZE 	VIDEO_SIZE_720P
#endif
/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"
extern u32 vstream_handle_cmd(u32 cmd, u32 arg);

/*************************
  Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {
#if 1
// OV9732_1280x720_MIPI_Full_30fps_raw
	{0x0103, 0x01},
	{0x0100, 0x00},
	{0x3001, 0x00},
	{0x3002, 0x00},
	{0x3007, 0x1f},
	{0x3008, 0xff},
	{0x3009, 0x02},
	{0x3010, 0x00},
	{0x3011, 0x08},
	{0x3014, 0x22},
	{0x301e, 0x15},
	{0x3030, 0x19},
	{0x3080, 0x02},
	{0x3081, 0x3c},
	{0x3082, 0x04},
	{0x3083, 0x00},
	{0x3084, 0x02},
	{0x3085, 0x01},
	{0x3086, 0x01},
	{0x3089, 0x01},
	{0x308a, 0x00},
	{0x3103, 0x01},
	{0x3600, 0xf6},
	{0x3601, 0x72},
	{0x3605, 0x66},
	{0x3610, 0x0c},
	{0x3611, 0x60},
	{0x3612, 0x35},
	{0x3654, 0x10},
	{0x3655, 0x77},
	{0x3656, 0x77},
	{0x3657, 0x07},
	{0x3658, 0x22},
	{0x3659, 0x22},
	{0x365a, 0x02},
	{0x3700, 0x1f},
	{0x3701, 0x10},
	{0x3702, 0x0c},
	{0x3703, 0x0b},
	{0x3704, 0x3c},
	{0x3705, 0x51},
	{0x370d, 0x20},
	{0x3710, 0x0d},
	{0x3782, 0x58},
	{0x3783, 0x60},
	{0x3784, 0x05},
	{0x3785, 0x55},
	{0x37c0, 0x07},
#ifdef SENSOR_CROP_1040_600
	{0x3800, 0x00}, // x start
	{0x3801, 0x7c},
	{0x3802, 0x00}, // y start
	{0x3803, 0x40},
	{0x3804, 0x04}, // x end(1171)
	{0x3805, 0x93},
	{0x3806, 0x02}, // y end(671)
	{0x3807, 0x9f},
	{0x3808, 0x04}, // x output size(1040)
	{0x3809, 0x10},
	{0x380a, 0x02}, //  y output size(600)
	{0x380b, 0x58},
#else
	{0x3800, 0x00},
	{0x3801, 0x04},
	{0x3802, 0x00},
	{0x3803, 0x04},
	{0x3804, 0x05},
	{0x3805, 0x0b},
	{0x3806, 0x02},
	{0x3807, 0xdb},
	{0x3808, 0x05},
	{0x3809, 0x00},
	{0x380a, 0x02},
	{0x380b, 0xd0},
#endif
	{0x380c, 0x05},
	{0x380d, 0xc6},
	{0x380e, 0x03},
	{0x380f, 0x22},
	{0x3810, 0x00},
	{0x3811, 0x04},
	{0x3812, 0x00},
	{0x3813, 0x04},
	{0x3816, 0x00},
	{0x3817, 0x00},
	{0x3818, 0x00},
	{0x3819, 0x04},
#ifdef CONFIG_SENSOR_FLIP_ON
#ifdef CONFIG_SENSOR_MIRROR_ON
	{0x3820, 0x1C},
#else
	{0x3820, 0x14},
#endif
#else
#ifdef CONFIG_SENSOR_MIRROR_ON
	{0x3820, 0x18},
#else
	{0x3820, 0x10},
#endif
#endif
	{0x3821, 0x00},
	{0x382c, 0x06},
	{0x3500, 0x00},
	{0x3501, 0x31},
	{0x3502, 0x00},
	{0x3503, 0x03},
	{0x3504, 0x00},
	{0x3505, 0x00},
	{0x3509, 0x10},
	{0x350a, 0x00},
	{0x350b, 0x40},
	{0x3d00, 0x00},
	{0x3d01, 0x00},
	{0x3d02, 0x00},
	{0x3d03, 0x00},
	{0x3d04, 0x00},
	{0x3d05, 0x00},
	{0x3d06, 0x00},
	{0x3d07, 0x00},
	{0x3d08, 0x00},
	{0x3d09, 0x00},
	{0x3d0a, 0x00},
	{0x3d0b, 0x00},
	{0x3d0c, 0x00},
	{0x3d0d, 0x00},
	{0x3d0e, 0x00},
	{0x3d0f, 0x00},
	{0x3d80, 0x00},
	{0x3d81, 0x00},
	{0x3d82, 0x38},
	{0x3d83, 0xa4},
	{0x3d84, 0x00},
	{0x3d85, 0x00},
	{0x3d86, 0x1f},
	{0x3d87, 0x03},
	{0x3d8b, 0x00},
	{0x3d8f, 0x00},
	{0x4001, 0xe0},
	{0x4004, 0x00},
	{0x4005, 0x02},
	{0x4006, 0x01},
	{0x4007, 0x40},
	{0x4009, 0x0b},
	{0x4300, 0x03},
	{0x4301, 0xff},
	{0x4304, 0x00},
	{0x4305, 0x00},
	{0x4309, 0x00},
	{0x4600, 0x00},
	{0x4601, 0x04},
	{0x4800, 0x00},
	{0x4805, 0x00},
	{0x4821, 0x50},
	{0x4823, 0x50},
	{0x4837, 0x2d},
	{0x4a00, 0x00},
	{0x4f00, 0x80},
	{0x4f01, 0x10},
	{0x4f02, 0x00},
	{0x4f03, 0x00},
	{0x4f04, 0x00},
	{0x4f05, 0x00},
	{0x4f06, 0x00},
	{0x4f07, 0x00},
	{0x4f08, 0x00},
	{0x4f09, 0x00},
	{0x5000, 0x07},
	{0x500c, 0x00},
	{0x500d, 0x00},
	{0x500e, 0x00},
	{0x500f, 0x00},
	{0x5010, 0x00},
	{0x5011, 0x00},
	{0x5012, 0x00},
	{0x5013, 0x00},
	{0x5014, 0x00},
	{0x5015, 0x00},
	{0x5016, 0x00},
	{0x5017, 0x00},
	{0x5080, 0x00},
	{0x5180, 0x01},
	{0x5181, 0x00},
	{0x5182, 0x01},
	{0x5183, 0x00},
	{0x5184, 0x01},
	{0x5185, 0x00},
	{0x5708, 0x06},
	{0x5781, 0x0e},
	{0x5783, 0x0f},
	{0x0100, 0x01},
#endif
};

/*************************
  setting for different size and framerate
 *************************/
// VTS x HTS x 30fps=802X1478X30= 35560680Hz(fixed)
// VTS: 0x380e, 380f = 0x322(802)
// HTS: 0x380c, 380d = 0x5c6(1478)

SENSOR_SETTING_TABLE sensor__size_1280_720[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_30[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x03}, // VTS - 30fps
	{0x380f, 0x22}, // VTS - 30fps
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_25[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x03}, // VTS - 25fps
	{0x380f, 0xc2}, // VTS - 25fps
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_24[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x03}, // VTS - 24fps
	{0x380f, 0xea}, // VTS - 24fps
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_20[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x04}, // VTS - 20fps
	{0x380f, 0xb3}, // VTS - 20fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_15[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x06}, // VTS - 15fps
	{0x380f, 0x44}, // VTS - 15fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_12[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x07}, // VTS - 12fps
	{0x380f, 0xd5}, // VTS - 12fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_10[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x09}, // VTS - 10fps
	{0x380f, 0x66}, // VTS - 10fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_7[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x0d}, // VTS - 7fps
	{0x380f, 0x6d}, // VTS - 7fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_5[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x12}, // VTS - 5fps
	{0x380f, 0xcc}, // VTS - 5fps
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_3[][2] = {
	{0x380c, 0x05}, // HTS
	{0x380d, 0xc6}, // HTS
	{0x380e, 0x1f}, // VTS - 3fps
	{0x380f, 0x54}, // VTS - 3fps
};

static void set_frm_byvts(int frm_target,int vts_default,int frm_default)
{
   g_pControl_L->nVTS = vts_default*frm_default/frm_target;
   g_pControl_L->nMaxExposure = g_pControl_L->nVTS - g_pControl_L->nPreChargeWidth;
   vstream_handle_cmd((MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET) | (0 << 12), frm_target);
}

/*************************
  effect change IOCTL
 *************************/
SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
    switch(effect){
	case SNR_EFFECT_BRIGHT:
		//TODO: level: 1~255. default 0x80
		return 0;
	case SNR_EFFECT_SHARPNESS:
		return 0;
	case SNR_EFFECT_FLIP_MIR:
	{
    	s32 data = libsccb_rd(cfg->sccb_cfg,0x3820);
		if(level){ //flip and mirror on
			data |= (BIT2 | BIT3);
        	libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("flip and mirror on:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
		}else{ //flip and mirror off
			data ^= (BIT2 | BIT3);
        	libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("flip and mirror off:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
        }
		return 0;
	}
    case SNR_EFFECT_FLIP:
    {
    	s32 data = libsccb_rd(cfg->sccb_cfg,0x3820);
		if(level){ //flip on
			data |= BIT2;
        	libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("flip on:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
		}else{ //flip off
			data ^= BIT2;
        	libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("flip off:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
        }
		return 0;
	}
	case SNR_EFFECT_MIRROR:
	{
		s32 data = libsccb_rd(cfg->sccb_cfg,0x3820);
		if(level){ //mirror on
			data |= BIT3;
        	libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("mirror on:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
		}else{ //mirror off
			data ^= BIT3;
			libsccb_wr(cfg->sccb_cfg,0x3820, data);
			debug_printf("mirror off:%x\n",libsccb_rd(cfg->sccb_cfg,0x3820));
        }
        return 0;
    }
    case SNR_EFFECT_FRMRATE:
        cfg->cur_frame_rate = level;
        SENSOR_SETTING_SUPPORT_START
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 30)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 25)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 24)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 20)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 15)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 12)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 10)
		SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 7)
		SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 5)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 3)
        SENSOR_SETTING_SUPPORT_END

		// band value = vts*fps*16/(freq*2)
        u32 vts = libsccb_rd(cfg->sccb_cfg,0x380e)<<8 | libsccb_rd(cfg->sccb_cfg,0x380f);
        u32 band_50hz = vts*level*16/(50*2);
        u32 band_60hz = vts*level*16/(60*2);

		if((ReadReg32(REG_SC_RESET0)&BIT4) == BIT4){
			SC_CLK0_EN(ISP);	
			SC_RRESET0_RELEASE(ISP);
			SC_RESET0_RELEASE(ISP);
		}
		WriteReg32(L_BASE_ADDRESS + 0x0044, (band_50hz<<16)|(band_60hz));

		set_frm_byvts(level,vts,level);
        return 0;
	case SNR_EFFECT_BW_SWITCH:
	{
		t_dpm_ispidc *dpm_ispidc = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[0];
		if(level){ // 1
			set_frm_byvts(15,0x322,30);
			WriteReg32(0xe0088000, 0x8a770100);	// pre pipe top,3D DNS on ccm off
			WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
			WriteReg32(0xe0088b2c, 0x04040404);	// weight4|weight5|weight6|weight7, bw=5
			WriteReg32(0xe0088b30, 0x04040408);	// weight8|weight9|weight10|weight11, bw=5
			g_libdatapath_cfg->default_TargetYHigh = 0x30;
			g_libdatapath_cfg->default_TargetYLow = 0x30;
			isp_set_ae_target(dpm_ispidc, dpm_ispidc->isp_brightness);	
		
			g_pControl_L->bAWBDigiGainEnable = 1;
			g_pControl_L->bSensorDigiGainEnable = 0;
			g_pControl_L->bUVSaturateAdjustEnable = 0;
		}
		else { // 0
			set_frm_byvts(30,0x322,30);
			WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on ccm on
			WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
			WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
			WriteReg32(0xe0088b30, 0x03020202);	// weight8|weight9|weight10|weight11, bw=5
			g_libdatapath_cfg->default_TargetYHigh = 0x3c;
			g_libdatapath_cfg->default_TargetYLow = 0x3c;
			isp_set_ae_target(dpm_ispidc, dpm_ispidc->isp_brightness);	

			g_pControl_L->bAWBDigiGainEnable = 0;
			g_pControl_L->bSensorDigiGainEnable = 0;
			g_pControl_L->bUVSaturateAdjustEnable = 1;
		}
        return 0; 
	}
#ifdef CONFIG_FAST_BOOT_EN
	case SNR_EFFECT_FASTBOOT:
		// FAST_STAGE_FPS2RDY(6), FAST_STAGE_FPS2START(7)
		if(level == FAST_STAGE_INITED)
		{
#if 0
			libsccb_wr(cfg->sccb_cfg,0x3502,(g_libdatapath_cfg->fastboot_exp&0xf0));
			libsccb_wr(cfg->sccb_cfg,0x3501,((g_libdatapath_cfg->fastboot_exp>>8)&0xff));
			libsccb_wr(cfg->sccb_cfg,0x3500,(g_libdatapath_cfg->fastboot_exp>>16)&0xf);
			libsccb_wr(cfg->sccb_cfg,0x3507,((g_libdatapath_cfg->fastboot_gain<<3)&0xf00)>>16);
			libsccb_wr(cfg->sccb_cfg,0x3508,((g_libdatapath_cfg->fastboot_gain<<3)&0xf00)>>8);
			libsccb_wr(cfg->sccb_cfg,0x3509,(g_libdatapath_cfg->fastboot_gain<<3)&0xff);
			g_libdatapath_cfg->fastboot_exp = 0;	//if set to 0 here,isp exp/gain initial value will read from sensor,refer to function libisp_fast_fps2init()
			g_libdatapath_cfg->fastboot_gain = 0;

			//TODO:need check whether short exp need init value
			libsccb_wr(cfg->sccb_cfg,0x350a,0x00);
			libsccb_wr(cfg->sccb_cfg,0x350b,0x03);
			libsccb_wr(cfg->sccb_cfg,0x350c,0x80);
			libsccb_wr(cfg->sccb_cfg,0x350d,0x00);
			libsccb_wr(cfg->sccb_cfg,0x350e,0x03);
			libsccb_wr(cfg->sccb_cfg,0x350f,0xf2);
			libsccb_wr(cfg->sccb_cfg,0x100,1);
#endif
		}
		return 0;
#endif
	case SNR_EFFECT_PROC:
		return 0;
    default:
        return -2;
    }
}

/*************************
  effect description table
  { EFFECT_NAME,need_proc, padding, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] = 
{
};



/*************************
  sensor detect func
 *************************/
#define OV9732_SENSOR_ID_ADR_HI 0x300a
#define OV9732_SENSOR_ID_ADR_LO 0x300b
#define OV9732_SENSOR_ID_VAL_HI 0x97
#define OV9732_SENSOR_ID_VAL_LO 0x30

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
    int hi,lo;
    int retry = 2000;

    do{
        hi = libsccb_rd(cfg->sccb_cfg,OV9732_SENSOR_ID_ADR_HI);
        lo = (libsccb_rd(cfg->sccb_cfg,OV9732_SENSOR_ID_ADR_LO) & 0xf0);
    }while((retry--) >= 0 && ((hi!=OV9732_SENSOR_ID_VAL_HI) || (lo!=OV9732_SENSOR_ID_VAL_LO)));
    debug_printf("sensor: %x %x\n",hi,lo);
  //  debug_printf("ov9732 ID read %x = %x,%x = %x\n",OV9732_SENSOR_ID_ADR_HI,hi,OV9732_SENSOR_ID_ADR_LO,lo);
	cfg->cur_video_size = SENSOR_DEFAULT_SIZE;
	if(cfg->cur_frame_rate == 0)
	{
		cfg->cur_frame_rate = 30;
	}
	if( (hi == OV9732_SENSOR_ID_VAL_HI) && (lo == (OV9732_SENSOR_ID_VAL_LO&0xf0)) )
    {
        debug_printf("OV9732 detected!\n");
        return 0;
    }
    else
    {
        return -1;
    }
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	return 0;	
}

extern u8 isp_lenc_c[768];
extern u8 isp_lenc_a[768];
extern u8 isp_lenc_d[768];
SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	//WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on
    syslog(LOG_WARNING,"isp setting version:v2.1.4--\n ");
    syslog(LOG_INFO,"g_pControl_L:%x\n ",g_pControl_L);

	/*************************************************/
	//WriteReg32(0xe0088000, 0x8a3f0100);	// pre pipe top,3D DNS off
	WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on
                    	// 00-[7] RAW_DNS_En      
                    	// 00-[6] BinC_En         
                    	// 00-[5] DPC_En          
                    	// 00-[4] DPCOTP_En       
                    	// 00-[3] AWBG_En         
                    	// 00-[2] Lens_Online_En  
                    	// 00-[1] LENC_En         
                    	// 00-[0] Pre_Pipe_Manu_En
                    	// 01-[7] Binc_New_En
                    	// 01-[6] DNS_3D_En       
                    	// 01-[5] Stretch_En      
                    	// 01-[4] Hist_Stats_En   
                    	// 01-[3] CCM_En          
                    	// 01-[2] AEC_AGC_En      
                    	// 01-[1] Curve_AWB_En    
                    	// 01-[0] CIP_En
                    	// 02-[7:4] reserved       
                    	// 02-[3] dpc_black_en     
                    	// 02-[2] dpc_white_en     
                    	// 02-[1] pdf_en           
                    	// 02-[0] ca_en            
                    	// 03-[7] Manual_Ctrl_En   -           
                    	// 03-[6] Manual_Size_En   
                    	// 03-[5] Lenc_mirror      
                    	// 03-[4] Lenc_flip        
                    	// 03-[3:2] reserved       
                    	// 03-[1] Binc_Mirror      
                    	// 03-[0] Binc_Flip     

	WriteReg32(0xe0088004, 0x00400001);	//[27:16]blc,[9:8]input pattern BG/GR
                    
	WriteReg32(0xe0089200, 0x11050000);	// [31:24]latch enable, [23:16]01-sensor1,10-sensor2,11-two sensor

	WriteReg32(0xe0089800, 0x3c000000);	// post pipe top
                    	// 00[0]  Manual_Ctrl_En    
                    	// 00[1]  Post_Pipe_Manu_En 
                    	// 00[2]  RGB_Gamma_En   
                    	// 00[3]  JPEG_Ycbcr_En  
                    	// 00[4]  SDE_En         
                    	// 00[5]  UV_DNS_En      
                    	// 00[6]  Anti_Shake_En  
                    	// 00[7]  Reserved    
					
// ================ interrupt ================
//48 00063054 00000000; [27:24]select stat done:0-stat all done,1-AWB done,2-hist done

// ================ DMA ================ 

// input size(fw)
	WriteReg32(L_BASE_ADDRESS + 0x0000, 0x02d00500);	// w_in|h_in
// ================ ISP function ================
#define AEC_AGC_SETTING
#define RGBH_STRETCH_SETTING
//#define MAN_AWB_SETTING
#define CURVE_AWB_SETTING
#define STANDARD_AWB
#define CCM_SETTING
//#define LENC_SETTING
//#define LENS_ONLINE_SETTING
#define RAW_DNS_SETTING
#define UV_DNS_SETTING
#define Sharpen_SETTING
#define GAMMA_SETTING
#define DNS3D_SETTING
#define SDE_SETTING
#define CA_SETTING

#ifdef AEC_AGC_SETTING
// meanY stat(HW)
	WriteReg32(0xe0088b18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe0088b1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe0088b08, 0x20204040);	// left|top|width|height, ratio, bw=7 
	WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe0088b30, 0x04020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe0088b34, 0x00000205);	// [12:8]weight12,[4:1]stat sampling,[0]stat brightest channel enable
// hist stat(HW)
	WriteReg32(0xe0088c04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe0088c08, 0x00000011);	// top|bottom, bw=13
// AEC para(FW)
	WriteReg32(L_BASE_ADDRESS + 0x0010, 0x08020100);	// AECExposureShift | SensorGainMode | WriteSensorEnable | AdvancedAECEnable
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00103c3c);	// [31:24]target low,[23:16]target high
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x060f0804);	// [31:16]pStableRange[2], FastStep | SlowStep

	WriteReg32(L_BASE_ADDRESS + 0x0038, 0x04010100); //nSaturationPer2 | nSaturationPer1 | nApplyAnaGainMode | nReadHistMeanSelect
    WriteReg32(L_BASE_ADDRESS + 0x003c, 0x040b00fd); //nMaxFractalExp | nMaxFractalExp | bAllowFractionalExp | nSaturateRefBin

	g_pControl_L->bAECManualEnable = 0; //0x0022
	WriteReg32(L_BASE_ADDRESS + 0x0024, 0x001002fd);	// 01f80010; max gain|min gain
//;48 10032028 00000280; max exposure
	WriteReg32(L_BASE_ADDRESS + 0x002c, 0x00000001);	// min exposure
	WriteReg32(L_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(L_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(L_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(L_BASE_ADDRESS + 0x0064, 0x350b350a);	// gain
	WriteReg32(L_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(L_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]	
	WriteReg32(L_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(L_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(L_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x007c, 0x030000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(L_BASE_ADDRESS + 0x0080, 0x00000101);	// SensorDigiGainEnable | ApplyDigiGainMode | MeteringOption | LumOption

    g_pControl_L->nLinearExp = 0x01;
#endif

// down framerate
//48 10032028 0000031a; max exposure
//48 10032054 096c0322; [31:16]VTS,[15:8]device ID,[7:0]I2C option

#ifdef RGBH_STRETCH_SETTING
// rgbh stretch
//48 e0088d80 02400180; [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d80, 0x01800100);	// [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d84, 0x01003fff);	// [29:16]manual low level, [13:0] min high level
//48 e0088d88 04011008;
//48 e0088d8c 00010400;
//48 e0088d8c 02010500;
#endif

#ifdef MAN_AWB_SETTING
// manual AWB gain
	WriteReg32(L_BASE_ADDRESS + 0x01ac, 0x08000000);	// [31:24]scale bits,[23:16]manualAWB_en
	WriteReg32(L_BASE_ADDRESS + 0x00a0, 0x00aa0080);	// b_gain|g_gain
	WriteReg32(L_BASE_ADDRESS + 0x00a4, 0x00f80080);	// [31:16]r_gain
	WriteReg32(L_BASE_ADDRESS + 0x0094, 0x00000000);	// b_offset|gb_offset|gr_offset|r_offset,x128 when applied
#endif

#ifdef CURVE_AWB_SETTING
	g_pControl_L->bROIEnable = 0;
// AWB stat
	WriteReg32(0xe0088a00, 0x00000000);	// [0]GW
	WriteReg32(0xe0088a0c, 0x000e03e0);	// 000e03e0; min_stat_val|max_stat_val,bw=10 (?)
	WriteReg32(0xe0088a10, 0x00000003);	// step=1(downsample=2)
// AWB calc
	WriteReg32(L_BASE_ADDRESS + 0x008c, 0x02000300);	// maxBgain|maxGBgain
	WriteReg32(L_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
	//disable digital gain in default, as the awb gain limitation 0414 by edie to fix the yellow issue in low lux low ct
	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10000000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
// XY settings
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x0014003e);	// AWB XY setting
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfe22fde2);	// AWB XY setting
// AWB map
// low map
	WriteReg32(L_BASE_ADDRESS + 0x00d0,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d4,0x00011111);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d8,0x10000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00dc,0x00122222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e0,0x21000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e4,0x00122222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e8,0x22100000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00ec,0x00122222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f0,0x22200000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f4,0x00122222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f8,0x44440000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00fc,0x00004444);	//
	WriteReg32(L_BASE_ADDRESS + 0x0100,0x88844000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0104,0x00068888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0108,0x88888600);	//
	WriteReg32(L_BASE_ADDRESS + 0x010c,0x00688888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0110,0x88888600);	//
	WriteReg32(L_BASE_ADDRESS + 0x0114,0x00688888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0118,0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x011c,0x00688888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0120,0xffd80000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0124,0x00dfffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0128,0xffd00000);	//
	WriteReg32(L_BASE_ADDRESS + 0x012c,0x00dfffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0130,0xfdd00000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0134,0x00dfffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0138,0xdd000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x013c,0x000dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0140,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0144,0x0000dddd);	//
	WriteReg32(L_BASE_ADDRESS + 0x0148,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x014c,0x00000000);	//
// middle map
	WriteReg32(L_BASE_ADDRESS + 0x0150,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0154,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0158,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x015c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0160,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0164,0x1fc01fc0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0168,0x1f801f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x016c,0x00000f00);	//
// high map
	WriteReg32(L_BASE_ADDRESS + 0x0170,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0174,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0178,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x017c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0180,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0184,0x0f800f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0188,0x0e000f00);	//
	WriteReg32(L_BASE_ADDRESS + 0x018c,0x00000000);	//

	//;; AWB ROIs
	WriteReg32(0xe0088a14,0x00000000);//
	WriteReg32(0xe0088a18,0x00000000);// W | H
	WriteReg32(0xe0088a1c,0x00000000);// 
	WriteReg32(0xe0088a20,0x00000000);// W | H 
	WriteReg32(0xe0088a24,0x00000000);// 
	WriteReg32(0xe0088a28,0x00000000);// W | H
	WriteReg32(0xe0088a2c,0x00000000);//
	WriteReg32(0xe0088a30,0x00000000);// W | H
	//;; ROI weight
	WriteReg32(L_BASE_ADDRESS + 0x01d8,0x00000000);// 04000000;
	WriteReg32(L_BASE_ADDRESS + 0x01dc,0x00000000);// 00006018; Weight 3,4,5
	WriteReg32(L_BASE_ADDRESS + 0x01e0,0x00000000);//
	WriteReg32(L_BASE_ADDRESS + 0x01e4,0x00000000);//
	WriteReg32(L_BASE_ADDRESS + 0x01e8,0x08080000);//
	WriteReg32(L_BASE_ADDRESS + 0x01ec,0x08080808);//
	//;; ROI threshold
	WriteReg32(L_BASE_ADDRESS + 0x01cc,0x00640032);// Thresholds
	WriteReg32(L_BASE_ADDRESS + 0x01d0,0x012c00c8);//
	WriteReg32(L_BASE_ADDRESS + 0x01d4,0x138802bc);//
	WriteReg32(L_BASE_ADDRESS + 0x01f0,0x01010080);// Offset and ROIOption
#endif

#ifdef CCM_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x02808000);	// UVSaturate enable | mode
//	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x02408000);	// UVSaturate enable | mode
	WriteReg32(L_BASE_ADDRESS + 0x03f8, 0x00000130);	//
	WriteReg32(L_BASE_ADDRESS + 0x03fc, 0x000001f8);	//

	// CCM
	WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xfe730301); 
	WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xffdbff8c); 
	WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xff6701be); 
	WriteReg32(L_BASE_ADDRESS + 0x03c8, 0x0055fea6); 
	WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x02810205); // center(c)
	WriteReg32(L_BASE_ADDRESS + 0x03d0, 0xffb9fec6);
	WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x01e9ffeb);
	WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xffccff2c);
	WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x022fff05); // left(a)
	WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xff160214);
	WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xff94ffd6);
	WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xff690203);
	WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xff42ffb3);
	WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x0100020b); // right(d), [15:0] maunual CT

	WriteReg32(L_BASE_ADDRESS + 0x03b0, 0x007b0071); // thre[0] | thre[1]
	WriteReg32(L_BASE_ADDRESS + 0x03b4, 0x00ac009a); // thre[2] | thre[3]
	WriteReg32(L_BASE_ADDRESS + 0x03b8, 0x01430120); // thre[4] | thre[5]
	WriteReg32(L_BASE_ADDRESS + 0x03ac, 0x80808001); //[31:24] auto CT enable

	WriteReg32(L_BASE_ADDRESS + 0x0490, 0x03540001);
	WriteReg32(L_BASE_ADDRESS + 0x0494, 0xff5bfe51);
	WriteReg32(L_BASE_ADDRESS + 0x0498, 0x0193000c);
	WriteReg32(L_BASE_ADDRESS + 0x049c, 0xfe89ff61);
	WriteReg32(L_BASE_ADDRESS + 0x04a0, 0x01b100c6); // last(h)
#endif

#ifdef LENC_SETTING
// LENC
	WriteReg32(L_BASE_ADDRESS + 0x0388, 0x0110007e);	// thre[0],thre[1],16bit
	WriteReg32(L_BASE_ADDRESS + 0x038c, 0x01600130);	// thre[2],thre[3],16bit
	WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x0b3313e9);	// h_scale|v_scale
// A light shading
	if(isp_lenc_a[0] != 0){
		syslog(LOG_ERR, "Apply A light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0x700), isp_lenc_a, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0700, 0x181b1e25);	//
	WriteReg32(L_BASE_ADDRESS + 0x0704, 0x14141516);	//
	WriteReg32(L_BASE_ADDRESS + 0x0708, 0x18161514);	//
	WriteReg32(L_BASE_ADDRESS + 0x070c, 0x28231e1a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0710, 0x0f111416);	//
	WriteReg32(L_BASE_ADDRESS + 0x0714, 0x0d0d0e0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0718, 0x100e0d0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x071c, 0x1a171311);	//
	WriteReg32(L_BASE_ADDRESS + 0x0720, 0x0a0b0d0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0724, 0x08080809);	//
	WriteReg32(L_BASE_ADDRESS + 0x0728, 0x0a090808);	//
	WriteReg32(L_BASE_ADDRESS + 0x072c, 0x100f0c0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0730, 0x0608090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0734, 0x05050505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0738, 0x06050504);	//
	WriteReg32(L_BASE_ADDRESS + 0x073c, 0x0c0a0907);	//
	WriteReg32(L_BASE_ADDRESS + 0x0740, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0744, 0x03030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0748, 0x04030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x074c, 0x09070604);	//
	WriteReg32(L_BASE_ADDRESS + 0x0750, 0x03030406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0754, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0758, 0x02020201);	//
	WriteReg32(L_BASE_ADDRESS + 0x075c, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0760, 0x02030305);	//
	WriteReg32(L_BASE_ADDRESS + 0x0764, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0768, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x076c, 0x04040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0770, 0x02020304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0774, 0x00000101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0778, 0x01010000);	//
	WriteReg32(L_BASE_ADDRESS + 0x077c, 0x05040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0780, 0x02020304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0784, 0x00000101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0788, 0x01010100);	//
	WriteReg32(L_BASE_ADDRESS + 0x078c, 0x05040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0790, 0x02030405);	//
	WriteReg32(L_BASE_ADDRESS + 0x0794, 0x01010102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0798, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x079c, 0x05040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a0, 0x03040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a4, 0x02020303);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a8, 0x03030202);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ac, 0x07060404);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b0, 0x05060708);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b4, 0x04040404);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b8, 0x05040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x07bc, 0x09080705);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c0, 0x08090a0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c4, 0x05060607);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c8, 0x07070606);	//
	WriteReg32(L_BASE_ADDRESS + 0x07cc, 0x0d0c0a09);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d0, 0x0c0d0e11);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d4, 0x0a090a0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d8, 0x0c0b0a0a);	//
	WriteReg32(L_BASE_ADDRESS + 0x07dc, 0x14110f0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e0, 0x1214171b);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e4, 0x0f101011);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e8, 0x13121010);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ec, 0x1c1c1815);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f0, 0x1f23292d);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f4, 0x191a1a1c);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f8, 0x1f1c1b19);	//
	WriteReg32(L_BASE_ADDRESS + 0x07fc, 0x572f2822);	//
	WriteReg32(L_BASE_ADDRESS + 0x0800, 0x7f7b7e75);	//
	WriteReg32(L_BASE_ADDRESS + 0x0804, 0x7c7e7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0808, 0x7a7d7c7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x080c, 0x807a7c79);	//
	WriteReg32(L_BASE_ADDRESS + 0x0810, 0x7d7d7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0814, 0x7d7c7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0818, 0x7d7b7d7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x081c, 0x787b7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0820, 0x8180817e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0824, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0828, 0x807f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x082c, 0x7e7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0830, 0x81828184);	//
	WriteReg32(L_BASE_ADDRESS + 0x0834, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0838, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x083c, 0x7e817f82);	//
	WriteReg32(L_BASE_ADDRESS + 0x0840, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0844, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0848, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x084c, 0x7f808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0850, 0x817f8082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0854, 0x81808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0858, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x085c, 0x807f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0860, 0x81817e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0864, 0x7f808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0868, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x086c, 0x807f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0870, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0874, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0878, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x087c, 0x7e7f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0880, 0x8181807e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0884, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0888, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x088c, 0x7f7f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0890, 0x81807e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0894, 0x82808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0898, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x089c, 0x7e808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a0, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a4, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a8, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ac, 0x7d808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b0, 0x81818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b4, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b8, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08bc, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c0, 0x82828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c4, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08cc, 0x7f808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d0, 0x8080807c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d4, 0x817f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d8, 0x817f817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x08dc, 0x7d7f7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e0, 0x7e7e7d7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e4, 0x7e7e7e7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e8, 0x7d7d7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ec, 0x7c7c7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f0, 0x807f7e73);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f4, 0x7b7b7b7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f8, 0x7c7d7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08fc, 0x73777980);	//
	WriteReg32(L_BASE_ADDRESS + 0x0900, 0x8a898e8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0904, 0x85888989);	//
	WriteReg32(L_BASE_ADDRESS + 0x0908, 0x8a898787);	//
	WriteReg32(L_BASE_ADDRESS + 0x090c, 0x908c8c8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0910, 0x86878889);	//
	WriteReg32(L_BASE_ADDRESS + 0x0914, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0918, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x091c, 0x8a898887);	//
	WriteReg32(L_BASE_ADDRESS + 0x0920, 0x85858586);	//
	WriteReg32(L_BASE_ADDRESS + 0x0924, 0x85848585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0928, 0x85858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x092c, 0x87868685);	//
	WriteReg32(L_BASE_ADDRESS + 0x0930, 0x83848586);	//
	WriteReg32(L_BASE_ADDRESS + 0x0934, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0938, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x093c, 0x84858584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0940, 0x83838385);	//
	WriteReg32(L_BASE_ADDRESS + 0x0944, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0948, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x094c, 0x83848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0950, 0x82828382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0954, 0x81808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0958, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x095c, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0960, 0x81828284);	//
	WriteReg32(L_BASE_ADDRESS + 0x0964, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0968, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x096c, 0x83838282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0970, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0974, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0978, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x097c, 0x83838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0980, 0x82818382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0984, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0988, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x098c, 0x83838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0990, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0994, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0998, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x099c, 0x85838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a0, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a4, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a8, 0x82828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ac, 0x84838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b0, 0x83838484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b4, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b8, 0x84838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09bc, 0x86858483);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c0, 0x85858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c4, 0x84848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c8, 0x84848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09cc, 0x85868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d0, 0x86868687);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d4, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d8, 0x87868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09dc, 0x89888787);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e0, 0x888a8b8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e4, 0x88888889);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e8, 0x89898888);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ec, 0x8b8d8b8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f0, 0x8f8b8d8e);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f4, 0x8d8b8d8c);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f8, 0x8d8e8f8c);	//
	WriteReg32(L_BASE_ADDRESS + 0x09fc, 0x9590908c);
	}
// C light shading
	if(isp_lenc_c[0] != 0){
		syslog(LOG_ERR, "Apply C light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0xa00), isp_lenc_c, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0a00, 0x2b30363e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a04, 0x24242728);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a08, 0x27252424);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a0c, 0x37322c29);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a10, 0x1c1f2328);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a14, 0x19191a1b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a18, 0x1b1a1919);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a1c, 0x26231f1d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a20, 0x13141619);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a24, 0x0f0f0f11);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a28, 0x11100f0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a2c, 0x1a171413);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a30, 0x0c0e1013);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a34, 0x0909090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a38, 0x0b0a0909);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a3c, 0x110f0d0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a40, 0x08090b0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a44, 0x04040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a48, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a4c, 0x0c0a0807);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a50, 0x0406080a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a54, 0x01010103);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a58, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a5c, 0x09070403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a60, 0x03040607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a64, 0x01000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a68, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a6c, 0x06040201);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a70, 0x02030506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a74, 0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a78, 0x02010000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a7c, 0x04030102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a80, 0x01020306);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a84, 0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a88, 0x02010001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a8c, 0x04020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a90, 0x01020305);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a94, 0x01010001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a98, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a9c, 0x04020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa0, 0x01020407);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa4, 0x01000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa8, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aac, 0x06040201);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab0, 0x0305070a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab4, 0x01010103);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab8, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0abc, 0x08070403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac0, 0x07090b0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac4, 0x04040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac8, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0acc, 0x0c0a0907);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad0, 0x0c0e1013);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad4, 0x0808090a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad8, 0x0a090808);	//
	WriteReg32(L_BASE_ADDRESS + 0x0adc, 0x12100d0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae0, 0x1214171b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae4, 0x0e0e0f11);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae8, 0x110f0e0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aec, 0x19171412);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af0, 0x1b1c2026);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af4, 0x16161719);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af8, 0x18161515);	//
	WriteReg32(L_BASE_ADDRESS + 0x0afc, 0x231e1b1a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b00, 0x797a7a78);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b04, 0x7b79797a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b08, 0x7a7a7a7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b0c, 0x7d797a7b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b10, 0x7c7b7a79);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b14, 0x7c7d7d7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b18, 0x7d7d7d7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b1c, 0x787a7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b20, 0x7f7e7e7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b24, 0x7e7e7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b28, 0x7e7e7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b2c, 0x7d7d7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b30, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b34, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b38, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b3c, 0x7d7d7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b40, 0x81817f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b44, 0x80818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b48, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b4c, 0x7c7f7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b50, 0x8080807e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b54, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b58, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b5c, 0x7f7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b60, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b64, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b68, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b6c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b70, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b74, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b78, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b7c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b80, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b84, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b88, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b8c, 0x7f7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b90, 0x80807e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b94, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b98, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b9c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba0, 0x80807f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba4, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba8, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bac, 0x7e7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb0, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bbc, 0x7e7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc0, 0x80807f7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc4, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bcc, 0x7e7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd0, 0x7f7f7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd4, 0x7f7f7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd8, 0x7f807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bdc, 0x7b7d7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be0, 0x7c7d7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be4, 0x7c7c7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be8, 0x7c7c7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bec, 0x7b7b7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf0, 0x7c7b7c76);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf4, 0x7b7a7c7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf8, 0x7b7c7b7a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bfc, 0x747b7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c00, 0x8d8d8e8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c04, 0x8b8d8d8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c08, 0x8c8c8c8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c0c, 0x958a8a8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c10, 0x888a8a8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c14, 0x85858687);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c18, 0x86858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c1c, 0x878a8986);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c20, 0x83848587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c24, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c28, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c2c, 0x86848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c30, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c34, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c38, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c3c, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c40, 0x81828284);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c44, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c48, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c4c, 0x83828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c50, 0x82818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c54, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c58, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c5c, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c60, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c64, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c68, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c6c, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c70, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c74, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c78, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c7c, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c80, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c84, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c88, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c8c, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c90, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c98, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c9c, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca0, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca4, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca8, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cac, 0x82818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb0, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb4, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cbc, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc0, 0x82828384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc8, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ccc, 0x84838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd0, 0x84848482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd4, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd8, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cdc, 0x84858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce0, 0x84858587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce4, 0x85858685);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce8, 0x85858485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cec, 0x86858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf0, 0x86888989);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf4, 0x84848385);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf8, 0x85848584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cfc, 0x87878685);	//
	}
// D light shading
	if(isp_lenc_d[0] != 0){
		syslog(LOG_ERR, "Apply D light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0xd00), isp_lenc_d, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0d00, 0x282a323b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d04, 0x1e1f2124);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d08, 0x241f201e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d0c, 0x57312b26);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d10, 0x181b1f22);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d14, 0x13131416);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d18, 0x15151313);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d1c, 0x1d1f1a18);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d20, 0x0f101316);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d24, 0x0c0c0d0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d28, 0x0d0d0c0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d2c, 0x1812100e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d30, 0x0a0c0d0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d34, 0x07070808);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d38, 0x08080707);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d3c, 0x0d0d0b0a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d40, 0x0607090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d44, 0x04040505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d48, 0x05050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d4c, 0x0b080706);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d50, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d54, 0x03030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d58, 0x04030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d5c, 0x08060604);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d60, 0x03040406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d64, 0x02020302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d68, 0x03030202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d6c, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d70, 0x02030405);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d74, 0x02010102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d78, 0x03020202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d7c, 0x05040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d80, 0x02030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d84, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d88, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d8c, 0x04040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d90, 0x02030403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d94, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d98, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d9c, 0x04040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da0, 0x03040406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da4, 0x02020203);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da8, 0x03020202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dac, 0x06040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db0, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db4, 0x03030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db8, 0x04040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dbc, 0x07060505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc0, 0x0607080a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc4, 0x04050505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc8, 0x06050504);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dcc, 0x0a080706);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd0, 0x090b0c0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd4, 0x07070708);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd8, 0x08080707);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ddc, 0x0f0d0b09);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de0, 0x0e101214);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de4, 0x0b0b0c0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de8, 0x0d0c0b0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dec, 0x1413100f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df0, 0x16171e22);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df4, 0x13121414);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df8, 0x15141212);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dfc, 0x221c1914);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e00, 0x7e7b8075);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e04, 0x7d7b807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e08, 0x7c807c80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e0c, 0x7278817c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e10, 0x807e7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e14, 0x7f7f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e18, 0x7e7f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e1c, 0x7f7d7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e20, 0x8281837d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e24, 0x80818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e28, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e2c, 0x7e7f817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e30, 0x83838182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e34, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e38, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e3c, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e40, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e44, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e48, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e4c, 0x7f818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e50, 0x83818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e54, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e58, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e5c, 0x7f818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e60, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e64, 0x81808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e68, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e6c, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e70, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e74, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e78, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e7c, 0x7e818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e80, 0x81828080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e84, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e88, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e8c, 0x80808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e90, 0x81828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e98, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e9c, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea0, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea4, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea8, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eac, 0x7e808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb0, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb4, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ebc, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec0, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec4, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ecc, 0x7f818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed0, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed4, 0x81828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed8, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0edc, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee0, 0x80807f84);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee4, 0x81807f81);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eec, 0x81807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef0, 0x7d828275);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef4, 0x7d80807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef8, 0x7e7f7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0efc, 0x727f807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f00, 0x9694958d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f04, 0x928f9493);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f08, 0x8b9e8994);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f0c, 0x8f969792);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f10, 0x8d8e9090);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f14, 0x8d8b8c8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f18, 0x8e8c8e8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f1c, 0x90938e8f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f20, 0x898b8a8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f24, 0x87898888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f28, 0x89898789);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f2c, 0x8c8c8b89);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f30, 0x8688868c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f34, 0x85878587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f38, 0x87868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f3c, 0x8a878887);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f40, 0x85848784);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f44, 0x85838485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f48, 0x84858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f4c, 0x86878585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f50, 0x84848486);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f54, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f58, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f5c, 0x84858584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f60, 0x83838386);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f64, 0x81828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f68, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f6c, 0x86858383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f70, 0x81838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f74, 0x80818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f78, 0x83808082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f7c, 0x83848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f80, 0x82838381);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f84, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f88, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f8c, 0x86838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f90, 0x82828384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f98, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f9c, 0x84838482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa0, 0x8282847f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa4, 0x80818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa8, 0x82828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fac, 0x85848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb0, 0x82838287);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb8, 0x84828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fbc, 0x86848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc0, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc4, 0x82838283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc8, 0x84838482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fcc, 0x83868684);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd0, 0x86868787);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd4, 0x85848584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd8, 0x85858684);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fdc, 0x8a888686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe0, 0x8788878d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe4, 0x86868589);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe8, 0x88878786);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fec, 0x8c898988);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff0, 0x8589907f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff4, 0x888a838a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff8, 0x88888589);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ffc, 0x88918b85);	
	}
#endif

#ifdef LENS_ONLINE_SETTING
//lens online
	WriteReg32(0xe0088180, 0x00000237);	// [10:8]frame_cn,[5]auto_stat_en_1,[4]auto_tmp_1,[3:2]split stat mode
	WriteReg32(0xe0088184, 0x00020404);	//
	WriteReg32(0xe0088194, 0x03031010);	// compare with 03034040, 01010404
	WriteReg32(0xe0088198, 0x1810f0f0);	// compare with 1810c0c0, 1810fcfc
	WriteReg32(0xe00881a4, 0x00400100);	// nSensorGain[0]|nSensorGain[1], bw=10
	WriteReg32(0xe00881a8, 0x00000000);	// [3:0]m_nMinLensGain
#endif

#ifdef RAW_DNS_SETTING
//RAWDNS
	WriteReg32(0xe0088500, 0x00060007);	// sigma[0]|sigma[1] ;000a0010
	WriteReg32(0xe0088504, 0x0008000a);	// sigma[2]|sigma[3] ;001a0026
	WriteReg32(0xe0088508, 0x00100012);	// sigma[4]|sigma[5] ;00300040
	WriteReg32(0xe008850c, 0x1a1a1616);	// Gns[0]|Gns[1]|Gns[2]|Gns[3]
	WriteReg32(0xe0088510, 0x14141a1a);	// Gns[4]|Gns[5]|Rbns[0]|Rbns[1]
	WriteReg32(0xe0088514, 0x16161414);	// Rbns[2]|Rbns[3]|Rbns[4]|Rbns[5]

	WriteReg32(0xe0088530, 0x3f3f3f3f);	// 
	WriteReg32(0xe0088534, 0x3f303028);	//
	WriteReg32(0xe0088538, 0x20201010);	//
	WriteReg32(0xe008853c, 0x20202020);	//
	WriteReg32(0xe0088540, 0x1010100a);	//
	WriteReg32(0xe0088544, 0x08040101);	//
#endif

#ifdef CA_SETTING
	// Alpha_0=0, Beta_0=1, Coef_0=3
	WriteReg32(0xe0088700, 0x00010104);
	WriteReg32(0xe0088714, 0x03010104);
#endif

#ifdef UV_DNS_SETTING
	WriteReg32(0xe0089b00, 0x00020480);
	WriteReg32(0xe0089b04, 0x80010101);	// 1x|2x|4x
	WriteReg32(0xe0089b08, 0x01010101);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b0c, 0x01020408);	// 128x|1x|2x|4x
	WriteReg32(0xe0089b10, 0x1014181c);	// 8x|16x|32x|64x	
	WriteReg32(0xe0089b14, 0x20000000);	// 128x
#endif

#ifdef SDE_SETTING
	//changed by Grady in 02/08/17 to solve unsmooth color issue
	WriteReg32(0xe0089a04, 0x00000400);   
	WriteReg32(0xe0089a20, 0x80806262);	//
	WriteReg32(0xe0089a24, 0x62c65f63);	//
	WriteReg32(0xe0089a28, 0x4e4e4eb2);	//
	WriteReg32(0xe0089a2c, 0x959d7676);	//
	WriteReg32(0xe0089a30, 0x76da121f);	//
	WriteReg32(0xe0089a34, 0x120c120c);	//
	WriteReg32(0xe0089a38, 0x88888800);	//
#endif


#ifdef Sharpen_SETTING
	WriteReg32(0xe0088800, 0x3f0462ff);	// cip
					// 00 - [7] 
					// 00 - [6] manual_mode
					// 00 - [5] bIfGbGr
					// 00 - [4] bIfCbCrDns
					// 00 - [3] bIfAntiClrAls
					// 00 - [2] bIfShapren
					// 00 - [1] bIfSharpenNS
					// 00 - [0] bIfDDns
					// 01 - [6:4] GDnsTSlope -- 0
 					// 01 - [3:0] WSlopeClr -- 4
					// 02 - [6:5] SharpenOptions -- 3  
 					// 02 - [4:0] CombW -- 2
					// 03 - [7:0] WSlope -- 0xff
	WriteReg32(0xe0088804, 0x01040810);	// DirGDNST0|1|2|3
	WriteReg32(0xe0088808, 0x12140808);	// [31:16] DirGDNST4|5
					// 0a - [5:0] DirGDNSLevel0
					// 0b - [5:0] DirGDNSLevel1
	WriteReg32(0xe008880c, 0x08080b10);	// 0c - [5:0] DirGDNSLevel2
					// 0d - [5:0] DirGDNSLevel3
					// 0e - [5:0] DirGDNSLevel4
					// 0f - [5:0] DirGDNSLevel5
	WriteReg32(0xe0088810, 0x40404040);	// CleanSharpT0|1|2|3
	WriteReg32(0xe0088814, 0x4040ffff);	// [31:16] CleanSharpT4|5
				// [15:0] CleanSharpT0|1
	WriteReg32(0xe0088818, 0xffffffff);	// CleanSharpT2|3|4|5
	WriteReg32(0xe008881c, 0x10182028);	// Tintp0|1|2|3
	WriteReg32(0xe0088820, 0x38481018);	// Tintp4|5
				// TintpHV0|1
	WriteReg32(0xe0088824, 0x20283848);	// TintpHV0|1|2|3
	WriteReg32(0xe0088828, 0x20304050);	// NV0|1|2|3
	WriteReg32(0xe008882c, 0x78a00404);	// [31:16] NV4|5
					// 0e - [0:5] VNGLevel0
					// 0f - [0:5] VNGLevel1
	WriteReg32(0xe0088830, 0x04080808);	// 30 - [0:5] VNGLevel2
					// 31 - [0:5] VNGLevel3
					// 32 - [0:5] VNGLevel4
					// 33 - [0:5] VNGLevel5
	WriteReg32(0xe0088834, 0x04060810);	// 34 - [0:5] LPG0
					// 35 - [0:5] LPG1
					// 36 - [0:5] LPG2
					// 37 - [0:5] LPG3
	WriteReg32(0xe0088838, 0x10142020);	// 38 - [0:5] LPG4
					// 39 - [0:5] LPG5
					// 3a - [0:5] CbrDbsLevel0
					// 3b - [0:5] CbrDbsLevel1
	WriteReg32(0xe008883c, 0x20202020);	// 3c - [0:5] CbrDbsLevel2
					// 3d - [0:5] CbrDbsLevel3
					// 3e - [0:5] CbrDbsLevel4
					// 3f - [0:5] CbrDbsLevel5
	WriteReg32(0xe0088840, 0x00500078);	// [27:16] - TCbCrH0
					// [11:0] - TCbCrH1
	WriteReg32(0xe0088844, 0x00a00140);	// [27:16] - TCbCrH2
					// [11:0] - TCbCrH3
	WriteReg32(0xe0088848, 0x01680190);	// [27:16] - TCbCrH4
					// [11:0] - TCbCrH5
	WriteReg32(0xe008884c, 0x00c80078);	// [27:16] - TCbCrV0
					// [11:0] - TCbCrV1
	WriteReg32(0xe0088850, 0x00a00140);	// [27:16] - TCbCrV2
					// [11:0] - TCbCrV3
	WriteReg32(0xe0088854, 0x01680190);	// [27:16] - TCbCrV4
					// [11:0] - TCbCrV5
	WriteReg32(0xe0088858, 0x00123333);	// [23:20] - TCbrOffset0
					// [19:16] - TCbrOffset1
					// [15:12] - TCbrOffset2
					// [11:8] - TCbrOffset3
					// [7:4] - TCbrOffset4
					// [3:0] - TCbrOffset5
	WriteReg32(0xe008885c, 0x6455463c);	// CbCrAlsHf0|1|2|3
	WriteReg32(0xe0088860, 0x3732c83c);	// [31:16] CbCrAlsHf4|5|
					// [15:0] CbCrAlsMean0|1
	WriteReg32(0xe0088864, 0x50505050);	// CbCrAlsMean2|3|4|5
	WriteReg32(0xe0088868, 0x0c0b0a08);	// CbrImpulse0|1|2|3
	WriteReg32(0xe008886c, 0x00101204);	// [23:16] CbrImpulse4
					// [15:8] CbrImpulse5
					// [2:0] CbrImpulseThrShift
	WriteReg32(0xe0088870, 0x10100e0e);	// 70 - [5:0] SharpenLevel0
					// 71 - [5:0] SharpenLevel1
					// 72 - [5:0] SharpenLevel2
					// 73 - [5:0] SharpenLevel3
	WriteReg32(0xe0088874, 0x0c0c0808);	// 74 - [5:0] SharpenLevel4
					// 75 - [5:0] SharpenLevel5
					// 76 - [7:0] SharpenThr0
					// 77 - [7:0] SharpenThr1
	WriteReg32(0xe0088878, 0x0a0c0f12);	// SharpenThr2|3|4|5
	WriteReg32(0xe008887c, 0x0002060a);	// 7d - [5:0] LocalRatio0
				// 7e - [5:0] LocalRatio1
				// 7f - [5:0] LocalRatio2
	WriteReg32(0xe0088880, 0x14141414);	// 80 - [5:0] SharpenHalo0
					// 81 - [5:0] SharpenHalo1
					// 82 - [5:0] SharpenHalo2
					// 83 - [5:0] SharpenHalo3
	WriteReg32(0xe0088884, 0x1414503a);	// 84 - [5:0] SharpenHalo4
					// 85 - [5:0] SharpenHalo5
					// 86 - [7:0] SharpenCoef0 (48)
					// 87 - [7:0] SharpenCoef1 (39)
	WriteReg32(0xe0088888, 0x17060104);	// [31:15] SharpenCoef2|3|4 (1b, 07, 01)
					// [7:0] DarkOffset
	WriteReg32(0xe008888c, 0x44444444);	
					// [31:28] SharpenHDelta0 (4)
					// [27:24] SharpenHDelta1 (4)
					// [23:20] SharpenHDelta2 (4)
					// [19:16] SharpenHDelta3 (4)
					// [15:12] SharpenHDelta4 (4)
					// [11:8] SharpenHDelta5 (4)
					// [7:4] SharpenHDelta6 (4)
					// [3:0] SharpenHDelta7 (4)
	WriteReg32(0xe0088890, 0x30383830);	// 90 - [5:0] SharpenHGain0 (08)
				// 91 - [5:0] SharpenHGain1 (0c)
				// 92 - [5:0] SharpenHGain2 (0a)
				// 93 - [5:0] SharpenHGain3 (09)
	WriteReg32(0xe0088894, 0x30201010);	// 94 - [5:0] SharpenHGain4 (08)
				// 95 - [5:0] SharpenHGain5 (0a)
				// 96 - [5:0] SharpenHGain6 (0a)
				// 97 - [5:0] SharpenHGain7 (0a)
	//WriteReg32(0xe0088898, 0x0e000010);	// 9b - [5:0] SharpenHGain8 (06)
	WriteReg32(0xe0088898, 0x0e000010);	// 9b - [5:0] SharpenHGain8 (06)
					// 99 - [5:0] localSymGThr0
					// 9a - [5:0] localSymGThr1
					// 9b - [5:0] localSymGThr2
	WriteReg32(0xe008889c, 0x00101820);	// 9c - reserved
					// 9d - [5:0] localDNSThr0
					// 9e - [5:0] localDNSThr1
					// 9f - [5:0] localDNSThr2
	WriteReg32(L_BASE_ADDRESS + 0x02c4, 0x025803ff);	// c4 - [1:0] CurList32[9:8]
				// c5 - [7:0] CurList32[7:0]
				// c6 - [2:0] HFreqGain0[10:8]
				// c7 - [7:0] HFreqGain0[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02c8, 0x01a40200);	// c8 - [2:0] HFreqGain1[10:8]
				// c9 - [7:0] HFreqGain1[7:0]
				// ca - [2:0] HFreqGain2[10:8]
				// cb - [7:0] HFreqGain2[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02cc, 0x01200158);	// cc - [2:0] HFreqGain3[10:8]
				// cd - [7:0] HFreqGain3[7:0]
				// ce - [2:0] HFreqGain4[10:8]
				// cf - [7:0] HFreqGain4[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d0, 0x00d000f8);	// d0 - [2:0] HFreqGain5[10:8]
				// d1 - [7:0] HFreqGain5[7:0]
				// d2 - [2:0] HFreqGain6[10:8]
				// d3 - [7:0] HFreqGain6[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d4, 0x008000aa);	// d4 - [2:0] HFreqGain7[10:8]
				// d5 - [7:0] HFreqGain7[7:0]
#endif

#ifdef GAMMA_SETTING
//48 100322d8 00000700
 
	WriteReg32(L_BASE_ADDRESS + 0x0200, 0x00c80078);
	WriteReg32(L_BASE_ADDRESS + 0x0204, 0x0198013e);
	WriteReg32(L_BASE_ADDRESS + 0x0208, 0x021a01e0);
	WriteReg32(L_BASE_ADDRESS + 0x020c, 0x02700246);
	WriteReg32(L_BASE_ADDRESS + 0x0210, 0x02ba0296);
	WriteReg32(L_BASE_ADDRESS + 0x0214, 0x02f602da);
	WriteReg32(L_BASE_ADDRESS + 0x0218, 0x03280312);
	WriteReg32(L_BASE_ADDRESS + 0x021c, 0x0350033e);
	WriteReg32(L_BASE_ADDRESS + 0x0220, 0x036e0360);
	WriteReg32(L_BASE_ADDRESS + 0x0224, 0x0388037c);
	WriteReg32(L_BASE_ADDRESS + 0x0228, 0x039c0392);
	WriteReg32(L_BASE_ADDRESS + 0x022c, 0x03b203a6);
	WriteReg32(L_BASE_ADDRESS + 0x0230, 0x03c803bc);
	WriteReg32(L_BASE_ADDRESS + 0x0234, 0x03d803d0);
	WriteReg32(L_BASE_ADDRESS + 0x0238, 0x03e803e0);
	WriteReg32(L_BASE_ADDRESS + 0x023c, 0x03f603ee);
	WriteReg32(L_BASE_ADDRESS + 0x0240, 0x007803ff);
	WriteReg32(L_BASE_ADDRESS + 0x0244, 0x013e00c8);
	WriteReg32(L_BASE_ADDRESS + 0x0248, 0x01e00198);
	WriteReg32(L_BASE_ADDRESS + 0x024c, 0x0246021a);
	WriteReg32(L_BASE_ADDRESS + 0x0250, 0x02960270);
	WriteReg32(L_BASE_ADDRESS + 0x0254, 0x02da02ba);
	WriteReg32(L_BASE_ADDRESS + 0x0258, 0x031202f6);
	WriteReg32(L_BASE_ADDRESS + 0x025c, 0x033e0328);
	WriteReg32(L_BASE_ADDRESS + 0x0260, 0x03600350);
	WriteReg32(L_BASE_ADDRESS + 0x0264, 0x037c036e);
	WriteReg32(L_BASE_ADDRESS + 0x0268, 0x03920388);
	WriteReg32(L_BASE_ADDRESS + 0x026c, 0x03a6039c);
	WriteReg32(L_BASE_ADDRESS + 0x0270, 0x03bc03b2);
	WriteReg32(L_BASE_ADDRESS + 0x0274, 0x03d003c8);
	WriteReg32(L_BASE_ADDRESS + 0x0278, 0x03e003d8);
	WriteReg32(L_BASE_ADDRESS + 0x027c, 0x03ee03e8);
	WriteReg32(L_BASE_ADDRESS + 0x0280, 0x03ff03f6);

	WriteReg32(0xe0089900, 0x08100000);

#endif

#ifdef DNS3D_SETTING
	WriteReg32(0xe0088550, 0x00040010);	// nIfComb on, [15:0] TSigma[0]
	WriteReg32(0xe0088554, 0x00180020);	// TSigma[1]~[2]
	WriteReg32(0xe0088558, 0x00400064);	// TSigma[3]~[4]
	WriteReg32(0xe008855c, 0x00c88006);	// [31:16] Tsigma[5],[15:8] TmpW,[7:0] Ratio
	WriteReg32(0xe0088560, 0x28080000);	// [31:24] PattThr,[23:16] ChgThr,[15:0] Overlap x
	WriteReg32(0xe0088564, 0x00000400);	// [31:16] Overlap y,[15:0] Overlap width
	WriteReg32(0xe0088568, 0x03070000);	// [31:16] Overlap height
#endif


#ifdef BAND50HZ  
	// banding
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010200);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#else
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010100);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#endif

	// band value = vts*fps*16/(freq*2)
	u32 vts = libsccb_rd(cfg->sccb_cfg,0x380e)<<8 | libsccb_rd(cfg->sccb_cfg,0x380f);
#ifdef SENSOR_30FPS
	u32 band_50hz = vts*30*16/(50*2);
	u32 band_60hz = vts*30*16/(60*2);
	WriteReg32(L_BASE_ADDRESS + 0x0044, (band_50hz<<16)|(band_60hz));
#else  
	u32 band_50hz = vts*24*16/(50*2);
	u32 band_60hz = vts*24*16/(60*2);
	WriteReg32(L_BASE_ADDRESS + 0x0044, (band_50hz<<16)|(band_60hz));
#endif
	WriteReg32(L_BASE_ADDRESS + 0x0028, 0x00000000|(vts-g_pControl_L->nPreChargeWidth));	// max exposure
#ifdef CONFIG_SENSOR_SID_HIGH
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x09200000|vts);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
#else
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x096c0000|vts);
#endif
//	g_pControl_L->nAWBStep1 = 15;
//	g_pControl_L->nAWBStep2 = 255;
#ifdef CONFIG_AEC_AWB_MANUAL_EN
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x080f0402);	// [31:16]pStableRange[2], FastStep | SlowStep
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00083520);	// [31:24]target low,[23:16]target high
	WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on ccm on
	g_pControl_L->nAWBStep1 = 15;
	g_pControl_L->nAWBStep2 = 255;
	//debug_printf("[L_BASE_ADDRESS + 0x008a]: 0x%x\n", ReadReg16(L_BASE_ADDRESS + 0x008a));
	//debug_printf("[L_BASE_ADDRESS + 0x0018]: 0x%x\n", ReadReg32(L_BASE_ADDRESS + 0x0018));
	//debug_printf("[L_BASE_ADDRESS + 0x0014]: 0x%x\n", ReadReg32(L_BASE_ADDRESS + 0x0014));
#endif

	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};
/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
