#include "includes.h"

#define LIGHT_FREQ_50HZ 1
#define SENSOR_2A   0xf8//0x98 
#define SENSOR_2B   0x06//0x07 
#define BAND50HZ    0x2500//0x0128 
#define BAND60HZ    0x1ee0//0x00f7 

#define SENSOR_NAME_INTERNAL 2710
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#define SENSOR_SCCB_ID_INTERNAL 0x6c
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_RAW8
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_DVP

/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

/*************************
  Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {

{0x3103,0x93},
{0x3008,0x82},
{0x3017,0x7f}, 
{0x3018,0xfc},
{0x3706,0x61},
{0x3712,0x0c},
{0x3630,0x6d},
{0x3801,0xb4},
{0x3621,0x04},
{0x3604,0x60},
{0x3603,0xa7},
{0x3631,0x26},
{0x3600,0x04},
{0x3620,0x37},
{0x3623,0x00},
{0x3702,0x9e},
{0x3703,0x5c},
{0x3704,0x40},
{0x370d,0x0f},
{0x3713,0x9f},
{0x3714,0x4c},
{0x3710,0x9e},
{0x3801,0xc4},
{0x3605,0x05},
{0x3606,0x3f}, 
{0x302d,0x90},
{0x370b,0x40},
{0x3716,0x31},
{0x3707,0x52},
{0x380d,0x74},
{0x5181,0x20}, 
{0x518f,0x00},
{0x4301,0xff},
{0x4303,0x00},
{0x3a00,0x78},
{0x300f,0x88},
{0x3011,0x28},
{0x3a1a,0x06},
{0x3a18,0x00},
{0x3a19,0x7a},
{0x3a13,0x54},
{0x382e,0x0f},
{0x381a,0x1a},
{0x401d,0x02},
{0x5688,0x03},
{0x5684,0x07},
{0x5685,0xa0}, 
{0x5686,0x04}, 
{0x5687,0x43}, 
{0x3a0f,0x40},
{0x3a10,0x38},
{0x3a1b,0x48}, 
{0x3a1e,0x30},
{0x3a11,0x90},
{0x3a1f,0x10},
{0x3012,0x01},
{0x3011,0x1e},
{0x3010,0x30},
{0x300f,0xc8},
{0x380e,0x04}, //default 0x06
{0x380f,0x80}, //default 0x18
{0x380b,0x30}
};

/*************************
  setting for different size and framerate
 *************************/

SENSOR_SETTING_TABLE sensor__size_1280_720[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_30[][2] = {
	{0x380c, 0x06},
	{0x380d, 0x54},
	{0x380e, 0x03},
	{0x380f, 0xdc},
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_25[][2] = {
	{0x380c, 0x07},
	{0x380d, 0xe0},
	{0x380e, 0x03},
	{0x380f, 0xdc},
};    
SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_20[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_15[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_12[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_10[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_5[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_3[][2] = {
};

/*************************
  effect change IOCTL
 *************************/
static u8 last_changed = 0;

SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
    switch(effect){
	case SNR_EFFECT_SHARPNESS:
		return 0;
    case SNR_EFFECT_SIZE:
        return 0;
    case SNR_EFFECT_FRMRATE:
        last_changed = 1;
        cfg->cur_frame_rate = level;
        return 0;
    case SNR_EFFECT_PROC:
        if(last_changed == 0){
            return 0;
        }
        SENSOR_SETTING_SUPPORT_START
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 30)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 25)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 20)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 15)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 10)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 5)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 3)
        SENSOR_SETTING_SUPPORT_END
        last_changed = 0;
        return 0;
    default:
        return -2;
    }
}

/*************************
  effect description table
  { EFFECT_NAME, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] = 
{
    {SNR_EFFECT_HUE, 0x00, 0x0c, 0x06, 0x06},
    {SNR_EFFECT_SHARPNESS, 0x00, 0x09, 0x03, 0x03},
    {SNR_EFFECT_BRIGHT, 0x01, 0xff, 0x80, 0x80},
    {SNR_EFFECT_SATURATION, 0x00, 0x08, 0x02, 0x02},
    {SNR_EFFECT_CONTRAST, 0x00, 0x08, 0x03, 0x03},
};



/*************************
  sensor detect func
 *************************/
#define OV2710_SENSOR_ID_ADR_HI 0x300a
#define OV2710_SENSOR_ID_ADR_LO 0x300b
#define OV2710_SENSOR_ID_VAL_HI 0x27
#define OV2710_SENSOR_ID_VAL_LO 0x10

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
    int hi,lo;
    int retry = 200;

    do{
        hi = libsccb_rd(cfg->sccb_cfg,OV2710_SENSOR_ID_ADR_HI);
        lo = (libsccb_rd(cfg->sccb_cfg,OV2710_SENSOR_ID_ADR_LO) & 0xf0);
    }while((retry--) >= 0 && (hi==-1 || lo==-1));
    debug_printf("sensor: %x %x\n",hi,lo);
  //  debug_printf("ov2710 ID read %x = %x,%x = %x\n",OV2710_SENSOR_ID_ADR_HI,hi,OV2710_SENSOR_ID_ADR_LO,lo);
	cfg->cur_video_size = (1920<<16)|1072;

	return 0;
	if( (hi == OV2710_SENSOR_ID_VAL_HI) && (lo == (OV2710_SENSOR_ID_VAL_LO&0xf0)) )
    {
        //debug_printf("  sensor_2710_isp_init()\n");
        return 0;
    }
    else
    {
        return -1;
    }
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	return 0;	
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};
/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
