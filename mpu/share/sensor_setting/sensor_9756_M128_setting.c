/***IQ version: 2.1.4***/
#include "includes.h"
#define SENSOR_NAME_INTERNAL 9750
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#ifdef CONFIG_SENSOR_SID_HIGH
#define SENSOR_SCCB_ID_INTERNAL 0x20
#else
#define SENSOR_SCCB_ID_INTERNAL 0x6c
#endif
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_RAW8
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_MIPI_1LN
//#define NIGHTMODE	
#define BAND50HZ  
//#define SENSOR_30FPS
//#define SENSOR_960P
//#define OTP_WB_EN
/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"
extern u32 vstream_handle_cmd(u32 cmd, u32 arg);

/*************************
  Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {
#if 1
	{0x0103, 0x01},
	{0x0100, 0x00},
	{0x0300, 0x04},
	{0x0302, 0x64},
	{0x0303, 0x01},
	{0x0304, 0x03},
	{0x0305, 0x01},
	{0x0306, 0x01},
	{0x030a, 0x00},
	{0x030b, 0x00},
	{0x030d, 0x1e},
	{0x030e, 0x01},
	{0x030f, 0x04},
	{0x0312, 0x01},
	{0x031e, 0x04},
	{0x3000, 0x00},
	{0x3001, 0x00},
	{0x3002, 0x21},
	{0x3005, 0xf0},
	{0x3011, 0x00},
	{0x3016, 0x53},
#ifdef CONFIG_FAST_BOOT_EN
	{0x3018, 0x32},		//2 lane
#else
	{0x3018, 0x12},		//1 lane
#endif
	{0x301a, 0xf0},
	{0x301b, 0xf0},
	{0x301c, 0xf0},
	{0x301d, 0xf0},
	{0x301e, 0xf0},
	{0x3022, 0x01},
	{0x3031, 0x0a},
	{0x3032, 0x80},
	{0x303c, 0xff},
	{0x303e, 0xff},
	{0x3040, 0xf0},
	{0x3041, 0x00},
	{0x3042, 0xf0},
	{0x3104, 0x01},
	{0x3106, 0x15},
	{0x3107, 0x01},
#ifdef CONFIG_FAST_BOOT_EN
	{0x3500, 0x00},//SENSOR_EXP_0},
	{0x3501, 0x30},//SENSOR_EXP_1},
	{0x3502, 0x00},//SENSOR_EXP_2},
#else
	{0x3500, 0x00},
	{0x3501, 0x38},
	{0x3502, 0x00},
#endif
	{0x3503, 0x08},
	{0x3504, 0x03},
	{0x3505, 0x83},
	{0x3508, 0x00},
	{0x3509, 0x80},
	{0x3600, 0x65},
	{0x3601, 0x60},
	{0x3602, 0x22},
	{0x3610, 0xe8},
	{0x3611, 0x56},
	{0x3612, 0x48},
	{0x3613, 0x5a},
	{0x3614, 0x91},
	{0x3615, 0x79},
	{0x3617, 0x57},
	{0x3621, 0x90},
	{0x3622, 0x00},
	{0x3623, 0x00},
	{0x3625, 0x07},
	{0x3633, 0x10},
	{0x3634, 0x10},
	{0x3635, 0x14},
	{0x3636, 0x13},
	{0x3650, 0x00},
	{0x3652, 0xff},
	{0x3654, 0x00},
	{0x3653, 0x34},
	{0x3655, 0x20},
	{0x3656, 0xff},
	{0x3657, 0xc4},
	{0x365a, 0xff},
	{0x365b, 0xff},
	{0x365e, 0xff},
	{0x365f, 0x00},
	{0x3668, 0x00},
	{0x366a, 0x07},
	{0x366d, 0x00},
	{0x366e, 0x10},
	{0x3702, 0x1d},
	{0x3703, 0x10},
	{0x3704, 0x14},
	{0x3705, 0x00},
	{0x3706, 0x27},
	{0x3709, 0x24},
	{0x370a, 0x00},
	{0x370b, 0x7d},
	{0x3714, 0x24},
	{0x371a, 0x5e},
	{0x3730, 0x82},
	{0x3733, 0x10},
	{0x373e, 0x18},
	{0x3755, 0x00},
	{0x3758, 0x00},
	{0x375b, 0x13},
	{0x3772, 0x23},
	{0x3773, 0x05},
	{0x3774, 0x16},
	{0x3775, 0x12},
	{0x3776, 0x08},
	{0x37a8, 0x38},
	{0x37b5, 0x36},
	{0x37c2, 0x04},
	{0x37c5, 0x00},
	{0x37c7, 0x30},
	{0x37c8, 0x00},
	{0x37d1, 0x13},
	{0x3800, 0x00},
	{0x3801, 0x00},
	{0x3802, 0x00},
	{0x3803, 0x7c},
	{0x3804, 0x05},
	{0x3805, 0x0f},
	{0x3806, 0x03},
	{0x3807, 0x53},
	{0x3808, 0x05},
	{0x3809, 0x00},
	{0x380a, 0x02},
	{0x380b, 0xd0},
	{0x3810, 0x00},
	{0x3811, 0x08},
	{0x3812, 0x00},
	{0x3813, 0x04},
	{0x3814, 0x01},
	{0x3815, 0x01},
	{0x3816, 0x00},
	{0x3817, 0x00},
	{0x3818, 0x00},
	{0x3819, 0x00},
#ifdef CONFIG_SENSOR_FLIP_ON
	{0x3820, 0x86},
#else
	{0x3820, 0x80},
#endif
#ifdef CONFIG_SENSOR_MIRROR_ON
	{0x3821, 0x46},
#else
	{0x3821, 0x40},
#endif
	{0x3826, 0x00},
	{0x3827, 0x08},
	{0x382a, 0x01},
	{0x382b, 0x01},
	{0x3836, 0x02},
	{0x3838, 0x10},
	{0x3861, 0x00},
	{0x3862, 0x00},
	{0x3863, 0x02},
	{0x3b00, 0x00},
	{0x3c00, 0x89},
	{0x3c01, 0xab},
	{0x3c02, 0x01},
	{0x3c03, 0x00},
	{0x3c04, 0x00},
	{0x3c05, 0x03},
	{0x3c06, 0x00},
	{0x3c07, 0x05},
	{0x3c0c, 0x00},
	{0x3c0d, 0x00},
	{0x3c0e, 0x00},
	{0x3c0f, 0x00},
	{0x3c40, 0x00},
	{0x3c41, 0xa3},
	{0x3c43, 0x7d},
	{0x3c56, 0x80},
	{0x3c80, 0x08},
	{0x3c82, 0x01},
	{0x3c83, 0x61},
	{0x3d85, 0x17},
	{0x3f08, 0x08},
	{0x3f0a, 0x00},
	{0x3f0b, 0x30},
	{0x4000, 0xcd},
	{0x4003, 0x40},
	{0x4009, 0x0d},
	{0x4010, 0xf0},
	{0x4011, 0x70},
	{0x4017, 0x10},
	{0x4040, 0x00},
	{0x4041, 0x00},
	{0x4303, 0x00},
	{0x4307, 0x30},
	{0x4500, 0x30},
	{0x4502, 0x40},
	{0x4503, 0x06},
	{0x4508, 0xaa},
#ifdef CONFIG_SENSOR_FLIP_ON
	{0x450b, 0x20},
#else
	{0x450b, 0x00},
#endif
	{0x450c, 0x00},
	{0x4600, 0x00},
	{0x4601, 0x80},
	{0x4700, 0x04},
	{0x4704, 0x00},
	{0x4705, 0x04},
	{0x4837, 0x14},
	{0x484a, 0x3f},
	{0x5000, 0x00},
	{0x5001, 0x01},
	{0x5002, 0x28},
	{0x5004, 0x0c},
	{0x5006, 0x0c},
	{0x5007, 0xe0},
	{0x5008, 0x01},
	{0x5009, 0xb0},
	{0x502a, 0x18},
	{0x5901, 0x00},
	{0x5a01, 0x00},
	{0x5a03, 0x00},
	{0x5a04, 0x0c},
	{0x5a05, 0xe0},
	{0x5a06, 0x09},
	{0x5a07, 0xb0},
	{0x5a08, 0x06},
	{0x5e00, 0x00},
	{0x5e10, 0xfc},
	
	{0x300f, 0x00},
	{0x3733, 0x10},
	{0x3610, 0xe8},
	{0x3611, 0x56},
	{0x3635, 0x14},
	{0x3636, 0x13},
	{0x3620, 0x84},
	{0x3614, 0x96},
	{0x481f, 0x30},
	{0x3788, 0x00},
	{0x3789, 0x04},
	{0x378a, 0x01},
	{0x378b, 0x60},
	{0x3799, 0x27},
	
#ifdef CONFIG_FAST_BOOT_EN
	{0x380c, 0x03},		//default 60fps
	{0x380d, 0xcb},
#else
#ifdef SENSOR_30FPS
	{0x380c, 0x07},
	{0x380d, 0x96},
#else
	{0x380c, 0x09},
	{0x380d, 0x7b},
#endif
#ifdef SENSOR_15FPS
	{0x380c, 0x0f},
	{0x380d, 0x2c},
#endif
#endif
//	{0x380c, 0x0a},//22fps;0xb18: 20.5fps
//	{0x380d, 0x50},
	{0x380e, 0x03},
	{0x380f, 0x38},
#ifdef SENSOR_960P

	{0x3802, 0x00},
	{0x3803, 0x04},
	{0x3806, 0x03},//v_end
	{0x3807, 0xcb},
	{0x380a, 0x03},
	{0x380b, 0xc0},
	{0x380e, 0x05},//vts
	{0x380f, 0x00},
#endif
	
	{0x0100, 0x01}
	
	#else 
		{0x103, 0x01},
		{0x100, 0x00},
		{0x300, 0x04},
		{0x302, 0x64},
		{0x303, 0x00},
		{0x304, 0x03},
		{0x305, 0x01},
		{0x306, 0x01},
		{0x30a, 0x00},
		{0x30b, 0x00},
		{0x30d, 0x1e},
		{0x30e, 0x01},
		{0x30f, 0x04},
		{0x312, 0x01},
		{0x31e, 0x04},
		{0x3000, 0x00},
		{0x3001, 0x00},
		{0x3002, 0x21},
		{0x3005, 0xf0},
		{0x3011, 0x00},
		{0x3016, 0x53},
		{0x3018, 0x12},
		{0x301a, 0xf0},
		{0x301b, 0xf0},
		{0x301c, 0xf0},
		{0x301d, 0xf0},
		{0x301e, 0xf0},
		{0x3022, 0x01},
		{0x3031, 0x0a},
		{0x3032, 0x80},
		{0x303c, 0xff},
		{0x303e, 0xff},
		{0x3040, 0xf0},
		{0x3041, 0x00},
		{0x3042, 0xf0},
		{0x3104, 0x01},
		{0x3106, 0x15},
		{0x3107, 0x01},
		{0x3500, 0x00},
		{0x3501, 0x0d},
		{0x3502, 0x00},
		{0x3503, 0x08},
		{0x3504, 0x03},
		{0x3505, 0x83},
		{0x3508, 0x02},
		{0x3509, 0x80},
		{0x3600, 0x65},
		{0x3601, 0x60},
		{0x3602, 0x22},
		{0x3610, 0xe8},
		{0x3611, 0x56},
		{0x3612, 0x18},
		{0x3613, 0x5a},
		{0x3614, 0x91},
		{0x3615, 0x79},
		{0x3617, 0x57},
		{0x3621, 0x90},
		{0x3622, 0x00},
		{0x3623, 0x00},
		{0x3633, 0x10},
		{0x3634, 0x10},
		{0x3635, 0x14},
		{0x3636, 0x13},
		{0x3650, 0x00},
		{0x3652, 0xff},
		{0x3654, 0x00},
		{0x3653, 0x34},
		{0x3655, 0x20},
		{0x3656, 0xff},
		{0x3657, 0xc4},
		{0x365a, 0xff},
		{0x365b, 0xff},
		{0x365e, 0xff},
		{0x365f, 0x00},
		{0x3668, 0x00},
		{0x366a, 0x07},
		{0x366d, 0x00},
		{0x366e, 0x10},
		{0x3702, 0x1d},
		{0x3703, 0x10},
		{0x3704, 0x14},
		{0x3705, 0x00},
		{0x3706, 0x27},
		{0x3709, 0x24},
		{0x370a, 0x00},
		{0x370b, 0x7d},
		{0x3714, 0x24},
		{0x371a, 0x5e},
		{0x3730, 0x82},
		{0x3733, 0x10},
		{0x373e, 0x18},
		{0x3755, 0x00},
		{0x3758, 0x00},
		{0x375b, 0x13},
		{0x3772, 0x23},
		{0x3773, 0x05},
		{0x3774, 0x16},
		{0x3775, 0x12},
		{0x3776, 0x08},
		{0x37a8, 0x38},
		{0x37b5, 0x36},
		{0x37c2, 0x04},
		{0x37c5, 0x00},
		{0x37c7, 0x30},
		{0x37c8, 0x00},
		{0x37d1, 0x13},
		{0x3800, 0x00},
		{0x3801, 0x00},
		{0x3802, 0x00},
		{0x3803, 0x04},
		{0x3804, 0x05},
		{0x3805, 0x0f},
		{0x3806, 0x03},
		{0x3807, 0xcb},
		{0x3808, 0x05},
		{0x3809, 0x00},
		{0x380a, 0x02},
		{0x380b, 0xd0},
		{0x380c, 0x0f},
		{0x380d, 0xc4},
	//	{0x380c, 0x07},
	//	{0x380d, 0xe2},
		{0x380e, 0x03},
		{0x380f, 0xdc},
		{0x3810, 0x00},
		{0x3811, 0x08},
		{0x3812, 0x00},
		{0x3813, 0x7c},
		{0x3814, 0x01},
		{0x3815, 0x01},
		{0x3816, 0x00},
		{0x3817, 0x00},
		{0x3818, 0x00},
		{0x3819, 0x00},
		{0x3820, 0x80},
		{0x3821, 0x40},
		{0x3826, 0x00},
		{0x3827, 0x08},
		{0x382a, 0x01},
		{0x382b, 0x01},
		{0x3836, 0x02},
		{0x3838, 0x10},
		{0x3861, 0x00},
		{0x3862, 0x00},
		{0x3863, 0x02},
		{0x3b00, 0x00},
		{0x3c00, 0x89},
		{0x3c01, 0xab},
		{0x3c02, 0x01},
		{0x3c03, 0x00},
		{0x3c04, 0x00},
		{0x3c05, 0x03},
		{0x3c06, 0x00},
		{0x3c07, 0x05},
		{0x3c0c, 0x00},
		{0x3c0d, 0x00},
		{0x3c0e, 0x00},
		{0x3c0f, 0x00},
		{0x3c40, 0x00},
		{0x3c41, 0xa3},
		{0x3c43, 0x7d},
		{0x3c56, 0x80},
		{0x3c80, 0x08},
		{0x3c82, 0x01},
		{0x3c83, 0x61},
		{0x3d85, 0x17},
		{0x3f08, 0x08},
		{0x3f0a, 0x00},
		{0x3f0b, 0x30},
		{0x4000, 0xcd},
		{0x4003, 0x40},
		{0x4009, 0x0d},
		{0x4010, 0xf0},
		{0x4011, 0x70},
		{0x4017, 0x10},
		{0x4040, 0x00},
		{0x4041, 0x00},
		{0x4303, 0x00},
		{0x4307, 0x30},
		{0x4500, 0x30},
		{0x4502, 0x40},
		{0x4503, 0x06},
		{0x4508, 0xaa},
		{0x450b, 0x00},
		{0x450c, 0x00},
		{0x4600, 0x00},
		{0x4601, 0x80},
		{0x4700, 0x04},
		{0x4704, 0x00},
		{0x4705, 0x04},
		{0x4837, 0x14},
		{0x484a, 0x3f},
		{0x5000, 0x00},
		{0x5001, 0x01},
		{0x5002, 0x28},
		{0x5004, 0x0c},
		{0x5006, 0x0c},
		{0x5007, 0xe0},
		{0x5008, 0x01},
		{0x5009, 0xb0},
		{0x502a, 0x18},
		{0x5901, 0x00},
		{0x5a01, 0x00},
		{0x5a03, 0x00},
		{0x5a04, 0x0c},
		{0x5a05, 0xe0},
		{0x5a06, 0x09},
		{0x5a07, 0xb0},
		{0x5a08, 0x06},
		{0x5e00, 0x00},
		{0x5e10, 0xfc},
		{0x300f, 0x00},
		{0x3733, 0x10},
		{0x3610, 0xe8},
		{0x3611, 0x56},
		{0x3635, 0x14},
		{0x3636, 0x13},
		{0x3620, 0x84},
		{0x3614, 0x96},
		{0x481f, 0x30},
		{0x3788, 0x00},
		{0x3789, 0x04},
		{0x378a, 0x01},
		{0x378b, 0x60},
		{0x3799, 0x27},

#ifdef FOR_FPGA
	{0x380c, 0x3f}, //3fps
	{0x380d, 0x00},
#else
	//{0x380c, 0x0f},//12fps
	//{0x380d, 0xc0},
//	{0x380c, 0x1f}, //6fps
//	{0x380d, 0x80},
//	{0x380c, 0x2f}, //4.5fps
//	{0x380d, 0x40},
//	{0x380c, 0x3f}, //3fps
//	{0x380d, 0x00},
	{0x380c, 0x07},	//25fps
	{0x380d, 0xe0},
//	{0x380c, 0x06},	//30fps
//	{0x380d, 0x54},	
//	{0x380c, 0x09},	//20fps
//	{0x380d, 0x7e},
//	{0x380c, 0x0c},	//15fps
//	{0x380d, 0xa8},
#endif
	{0x380e, 0x03},
	{0x380f, 0xdc},
#ifdef BRIGHT_SCENE
		{0x3500, 0x00},
		{0x3501, 0x00},
		{0x3502, 0x10},
#endif
#ifdef BLACK_SCENE
		{0x3500, 0x00},
		{0x3501, 0x40},
		{0x3502, 0x18},
#endif
		{0x100 , 0x01}, 
#endif
};

/*************************
  setting for different size and framerate
 *************************/

SENSOR_SETTING_TABLE sensor__size_1280_720[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_60[][2] = {
	{0x3018, 0x32},		//2 lane
	{0x380c, 0x03},
	{0x380d, 0xcb},
	{0x380e, 0x03},
	{0x380f, 0xdc},
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_30[][2] = {
	{0x3018, 0x12},		//1 lane
	{0x380c, 0x07},
	{0x380d, 0x96},
	{0x380e, 0x03},
	{0x380f, 0x38},
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_25[][2] = {
	{0x3018, 0x12},		//1 lane
	{0x380c, 0x09},
	{0x380d, 0x18},
	{0x380e, 0x03},
	{0x380f, 0x38},
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_24[][2] = {
	{0x3018, 0x12},		//1 lane
	{0x380c, 0x09},
	{0x380d, 0x7b},
	{0x380e, 0x03},
	{0x380f, 0x38},
};    

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_20[][2] = {
	{0x3018, 0x12},		//1 lane
	{0x380c, 0x0B},
	{0x380d, 0x61},
	{0x380e, 0x03},
	{0x380f, 0x38},
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_15[][2] = {
	{0x3018, 0x12},		//1 lane
//	{0x380c, 0x0f},	//15fps
//	{0x380d, 0x2c},
//	{0x380e, 0x03},
//	{0x380f, 0x38},
	{0x380c, 0x09}, //15fps change vts compared with 24fps
	{0x380d, 0x7b},
	{0x380e, 0x05},
	{0x380f, 0x26},
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_12[][2] = {
	{0x3018, 0x12},		//1 lane
//	{0x380c, 0x12},//12fps
//	{0x380d, 0xf6},
//	{0x380e, 0x03},
//	{0x380f, 0x38},
	{0x380c, 0x09}, 
	{0x380d, 0x7b},
	{0x380e, 0x06},
	{0x380f, 0x70},
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_10[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_5[][2] = {
};

SENSOR_SETTING_TABLE sensor__size_1280_720_framerate_3[][2] = {
	{0x3018, 0x12},		//1 lane
//	{0x380c, 0x4b}, //3fps
//	{0x380d, 0xdc},
//	{0x380e, 0x03},
//	{0x380f, 0x38},
	{0x380c, 0x09}, 
	{0x380d, 0x7b},
	{0x380e, 0x19},
	{0x380f, 0xc0},
};

static void set_frm_byvts(int frm)
{
   g_pControl_L->nVTS = 0x338*24/frm;
   g_pControl_L->nMaxExposure = g_pControl_L->nVTS - g_pControl_L->nPreChargeWidth;
   vstream_handle_cmd((MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET) | (0 << 12), frm);
}

/*************************
  effect change IOCTL
 *************************/
static u8 last_changed = 0;
SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
    switch(effect){
	case SNR_EFFECT_BRIGHT:
		//TODO: level: 1~255. default 0x80
		return 0;
	case SNR_EFFECT_SHARPNESS:
		return 0;
	case SNR_EFFECT_LIGHT_FREQ:
		if(level == 0){//disable banding
		g_pControl_L->bBandingFilterEnable = 0;
		}else if(level == 1){//50hz
		g_pControl_L->bBandingFilterEnable = 1;
		g_pControl_L->nBandingFilterMode = 2;
		}else if(level == 2){//60hz
		g_pControl_L->bBandingFilterEnable = 1;
		g_pControl_L->nBandingFilterMode = 1;
		}
		debug_printf("band en:%x mode:%x \n",g_pControl_L->bBandingFilterEnable,g_pControl_L->nBandingFilterMode);
		return 0;
    case SNR_EFFECT_FLIP:
    {
		if(level){ //flip on
			//WriteReg32(0xe0088004, 0x00401101);	//[27:16]blc,[9:8]input pattern BG/GR
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x86);
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x86);
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x86);
        	libsccb_wr(cfg->sccb_cfg,0x450b, 0x20);
			debug_printf("flip on:%x %x\n",libsccb_rd(cfg->sccb_cfg,0x3820),libsccb_rd(cfg->sccb_cfg,0x450b));
		}else{ //flip off
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x80);
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x80);
        	libsccb_wr(cfg->sccb_cfg,0x3820, 0x80);
        	libsccb_wr(cfg->sccb_cfg,0x450b, 0x00);
			//WriteReg32(0xe0088004, 0x00400001);	//[27:16]blc,[9:8]input pattern BG/GR
			debug_printf("flip off:%x %x\n",libsccb_rd(cfg->sccb_cfg,0x3820),libsccb_rd(cfg->sccb_cfg,0x450b));
        }
		return 0;
	}
	case SNR_EFFECT_MIRROR:
	{
		if(level){ //mirror on
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x46);
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x46);
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x46);
			debug_printf("mirror on:%x\n",libsccb_rd(cfg->sccb_cfg,0x3821));
		}else{ //mirror off
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x40);
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x40);
        	libsccb_wr(cfg->sccb_cfg,0x3821, 0x40);
			debug_printf("mirror off:%x\n",libsccb_rd(cfg->sccb_cfg,0x3821));
        }
        return 0;
    }
    case SNR_EFFECT_SIZE:
        switch(level){
        case VIDEO_SIZE_720P:
            cfg->cur_video_size = VIDEO_SIZE_720P;
            debug_printf("9750 set to 720P\n");
            last_changed = 1;
            break;
        default:
            return -1;
        }
        return 0;
    case SNR_EFFECT_FRMRATE:
        last_changed = 1;
        cfg->cur_frame_rate = level;
//		u32 vts = 0x338;  ///< current vts
//		u32 band_50hz = level*vts*16/(50*2);
//		u32 band_60hz = level*vts*16/(60*2);
		u32 band_50hz = 0x338*24*16/(50*2);
		u32 band_60hz = 0x338*24*16/(60*2);

		if((ReadReg32(REG_SC_RESET0)&BIT4) == BIT4){
			SC_CLK0_EN(ISP);	
			SC_RRESET0_RELEASE(ISP);
			SC_RESET0_RELEASE(ISP);
		}
		WriteReg32(L_BASE_ADDRESS+0x44, (band_50hz<<16)|(band_60hz));
#if 0
		switch(level)
		{
			case 30:
				WriteReg32(L_BASE_ADDRESS + 0x0044, 0x12860f70);	// 60band|50band,fr=30
				break;
			case 25:
				WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0f700cd0);	// 60band|50band,fr=25
				break;
			default:
				break;
		}
#endif
        return 0;
	case SNR_EFFECT_WBCALI:
		{
			u8 * p = (u8 *)level;
			libsccb_wr(cfg->sccb_cfg, 0x5036, p[0]);
			libsccb_wr(cfg->sccb_cfg, 0x5037, p[1]);
			libsccb_wr(cfg->sccb_cfg, 0x5032, p[2]);
			libsccb_wr(cfg->sccb_cfg, 0x5033, p[3]);
			libsccb_wr(cfg->sccb_cfg, 0x5000, 0);
			return 0;
		}
    case SNR_EFFECT_PROC:
        if(last_changed == 0){
            return 0;
        }
		SENSOR_SETTING_SUPPORT_START
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 30)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 25)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 24)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 20)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 15)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 12)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 10)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 5)
        SENSOR_SETTING_SUPPORT_SIZE_FRM(1280, 720, 3)
        SENSOR_SETTING_SUPPORT_END
		last_changed = 0;
        return 0;
	case SNR_EFFECT_BW_SWITCH:
	{
		t_dpm_ispidc *dpm_ispidc = (t_dpm_ispidc *)g_libdatapath_cfg->ispidc_map[0];
		if(level){
			set_frm_byvts(15);
			WriteReg32(0xe0088000, 0x8a770100);	// pre pipe top,3D DNS on ccm off
			WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
			WriteReg32(0xe0088b2c, 0x04040404);	// weight4|weight5|weight6|weight7, bw=5
			WriteReg32(0xe0088b30, 0x04040408);	// weight8|weight9|weight10|weight11, bw=5
			g_libdatapath_cfg->default_TargetYHigh = 0x30;
			g_libdatapath_cfg->default_TargetYLow = 0x30;
			isp_set_ae_target(dpm_ispidc, dpm_ispidc->isp_brightness);	
		
			g_pControl_L->bAWBDigiGainEnable = 1;
			g_pControl_L->bSensorDigiGainEnable = 0;
			g_pControl_L->bUVSaturateAdjustEnable = 0;
		}
		else {
			set_frm_byvts(24);
			WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on ccm on
			WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
			WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
			WriteReg32(0xe0088b30, 0x03020202);	// weight8|weight9|weight10|weight11, bw=5
			g_libdatapath_cfg->default_TargetYHigh = 0x3c;
			g_libdatapath_cfg->default_TargetYLow = 0x3c;
			isp_set_ae_target(dpm_ispidc, dpm_ispidc->isp_brightness);	

			g_pControl_L->bAWBDigiGainEnable = 0;
			g_pControl_L->bSensorDigiGainEnable = 0;
			g_pControl_L->bUVSaturateAdjustEnable = 1;
		}
        return 0; 
	}
#ifdef CONFIG_FAST_BOOT_EN
	case SNR_EFFECT_FASTBOOT:
		if(level == FAST_STAGE_FPS1DONE){
			//called after FPS1DONE, isp handler done.
			if(g_pBlankingRead_L->tCurveAWB.nCount < 0x100)
			{
				//in Low CT Low Lux mode, if there's no enough nCount, the Sum is not accurate.
				//in this case, use the default gain, didnot do any AWB in this case. Show it's original color.
				g_libdatapath_cfg->fps1_awb0 = 0xf7;
				g_libdatapath_cfg->fps1_awb1 = 0x80;
				g_libdatapath_cfg->fps1_awb2 = 0x11b;
			}
		}
		if(level == FAST_STAGE_FPS2RDY){
			//called before FPS2RDY, before isp setting.
			//set nMaxExposure here.
			g_pControl_L->nVTS = 0x338*24/g_libdatapath_cfg->fastboot_fps2_fps;
			g_pControl_L->nMaxExposure = g_pControl_L->nVTS - g_pControl_L->nPreChargeWidth;
		}

		if(level == FAST_STAGE_FPS2START){
			//called, after isp fps2 setting, before sensor setting download.
			//to 1 lane and not revert pclk,should be done before sensor switch to 1 lane
			WriteReg32(0xc0089000,(ReadReg32(0xc0089000)&0xffffff00)|0x0c);
			WriteReg32(SC_BASE_ADDR+0x0c, (ReadReg32(SC_BASE_ADDR+0x0c)&0xff9fffff)|(2<<21));
			WriteReg32(SC_BASE_ADDR+0x10c, (ReadReg32(SC_BASE_ADDR+0x10c)&0xfffffff0)|0x04);
			WriteReg32(SC_BASE_ADDR+0x104, (ReadReg32(SC_BASE_ADDR+0x104))&(~BIT24));
		}
		return 0;
#endif

    default:
        return -2;
    }
}

/*************************
  effect description table
  { EFFECT_NAME,need_proc, padding, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] = 
{
    {SNR_EFFECT_HUE, 0x0, 0x0, 0x00, 0x0c, 0x06, 0x06},
    {SNR_EFFECT_SHARPNESS, 0x0, 0x0, 0x00, 0x09, 0x03, 0x03},
    {SNR_EFFECT_BRIGHT, 0x0, 0x0,0x01, 0xff, 0x80, 0x80},
    {SNR_EFFECT_SATURATION, 0x0,0x0,0x00, 0x08, 0x02, 0x02},
    {SNR_EFFECT_CONTRAST,0x0,0x0, 0x00, 0x08, 0x03, 0x03},
	{SNR_EFFECT_FRMRATE, 0x0, 0x0, 1, 30, 24, 24},
	{SNR_EFFECT_SIZE,  0x0, 0x0,  VIDEO_SIZE_720P, VIDEO_SIZE_720P, VIDEO_SIZE_720P, VIDEO_SIZE_720P},
	{SNR_EFFECT_LIGHT_FREQ,0x0,0x0,0x0, 0x2, 0x1, 0x1},
	{SNR_EFFECT_MIRROR,0x0,0x0,0x0, 0x1, 0x0, 0x0},
	{SNR_EFFECT_FLIP,0x0,0x0,0x0, 0x1, 0x0, 0x0},
	{SNR_EFFECT_FASTBOOT,0x0,0x0,0x0, FAST_STAGE_LAST, 0x0, 0x0},
	{SNR_EFFECT_BW_SWITCH,0x0,0x0,0x0,0x1,0x0,0x0},
	{SNR_EFFECT_WBCALI,0x0,0x0,0x0, 0xffffffff, 0x0, 0x0},
};



/*************************
  sensor detect func
 *************************/
#define OV9750_SENSOR_ID_ADR_HI 0x300b
#define OV9750_SENSOR_ID_ADR_LO 0x300c
#define OV9750_SENSOR_ID_VAL_HI 0x97
#define OV9750_SENSOR_ID_VAL_LO 0x50

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
    int hi,lo;
    int retry = 2000;

    do{
        hi = libsccb_rd(cfg->sccb_cfg,OV9750_SENSOR_ID_ADR_HI);
        lo = (libsccb_rd(cfg->sccb_cfg,OV9750_SENSOR_ID_ADR_LO) & 0xf0);
    }while((retry--) >= 0 && ((hi!=0x97) || (lo!=0x50)));
    debug_printf("sensor: %x %x\n",hi,lo);
  //  debug_printf("ov9750 ID read %x = %x,%x = %x\n",OV9750_SENSOR_ID_ADR_HI,hi,OV9750_SENSOR_ID_ADR_LO,lo);
	cfg->cur_video_size = VIDEO_SIZE_720P;
#ifdef SENSOR_960P
	cfg->cur_video_size = VIDEO_SIZE_960P;
#endif
	if(cfg->cur_frame_rate == 0)
	{
#ifdef CONFIG_FAST_BOOT_EN
		cfg->cur_frame_rate = 60;
#else	
		cfg->cur_frame_rate = 24;
#endif
	}
	if( (hi == OV9750_SENSOR_ID_VAL_HI) && (lo == (OV9750_SENSOR_ID_VAL_LO&0xf0)) )
    {
        //debug_printf("  sensor_9750_isp_init()\n");
        return 0;
    }
    else
    {
        return -1;
    }
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	 WriteReg32(0xc0038c00, 0x0000001f);	// [4]: rgbir_isp_en
                    	// [3]: ir_extract_en
                    	// [2]: bIRRemovalEnable
                    	// [1]: bDNSEnable
                    	// [0]: bDPCEnable

//; IR removal
	WriteReg32(0xc0038c04, 0x00000000);	//
#ifdef SENSOR_960P
	WriteReg32(0xc0038c08, 0x03c00500);	// [31:16] img_vsize | [15:0] img_hsize
#else
	WriteReg32(0xc0038c08, 0x02d00500);	// [31:16] img_vsize | [15:0] img_hsize
#endif
	WriteReg32(0xc0038c0c, 0x081a0680);	// [15:0] hblank | [16] dummy_man_en | [19:17] dummy_ratio
	//WriteReg32(0xc0038c0c, 0x081b0680);	// [15:0] hblank | [16] dummy_man_en | [19:17] dummy_ratio
            	// [20] dummy_ratio_man_en | [27:24] dummy_line_num
	WriteReg32(0xc0038c10, 0x00000800);	// [15:0] real_gain
	WriteReg32(0xc0038c14, 0x3bddb3dd);	// CFAArray
	WriteReg32(0xc0038c18, 0x0004000d);	// [18] compensate_en | [17:16] bayer_pattern | [15:0] blc
	WriteReg32(0xc0038c1c, 0x00800100);	// [8:0] manual_gain | [31:16] remove_eof_cnt
	WriteReg32(0xc0038c20, 0x80808080);	// [31:24] remove_ir | [23:16] coef_b | [15:8] coef_g | [7:0] coef_r
	WriteReg32(0xc0038c24, 0x01ff0080);	// [24:16] max_gain | [8:0] min_gain
	WriteReg32(0xc0038c28, 0x000a03e8);	// [20:16] remove_step | [15:0] remove_thre
	WriteReg32(0xc0038c2c, 0x0008040c);	// [19:16] remove_sat_range | [15:8]:remove_range1 | [7:0]:remove_range2

//; CIP
	WriteReg32(0xc0038e00, 0x00000002);	// Configured by Hardware
//	WriteReg32(0xc0038e00, 0x00000003);	// Configured by Hardware
//	WriteReg32(0xc0038e1c, 0x00000001);// Noise
	//WriteReg32(0xc0038e04, 0x140c0802);	// Noise list 0|1|2|3
	//WriteReg32(0xc0038e08, 0x0000261c);	// Noise list 4|5
	WriteReg32(0xc0038e04, 0x00000000);	// Noise list 0|1|2|3
	WriteReg32(0xc0038e08, 0x00000000);	// Noise list 4|5

//; RGBIR UVDns
	WriteReg32(0xc0038f00, 0x00000000);	// Configured by Hardware

	WriteReg32(0xc0038f04, 0x05040302);	// Noise Y list 0|1|2|3 8Bits
	WriteReg32(0xc0038f08, 0x00000706);	// Noise Y list 4|5 8Bits
	WriteReg32(0xc0038f0c, 0x07050301);	// Noise UV list 0|1|2|3 8Bits
	WriteReg32(0xc0038f10, 0x00000908);	// Noise UV list 4|5 8Bits

	WriteReg32(0xc0038f14, 0x00ba8765);	// Add back Y list 0|1|2|3|4|5 4Bits
	WriteReg32(0xc0038f18, 0x00000082);	// Gain threshold 0|1 4Bits
	WriteReg32(0xc0038f1c, 0x00002010);	// V Scale 0|1 6Bits
#ifdef NIGHTMODE
	WriteReg32(0xc0038c20, 0x80000000);	// [31:24] remove_ir | [23:16] coef_b | [15:8] coef_g | [7:0] coef_r
   	WriteReg32(0xc0038c20, 0x80000000);	// [31:24] remove_ir | [23:16] coef_b | [15:8] coef_g | [7:0] coef_r
#endif
	return 0;	

}
extern u8 isp_lenc_c[768];
extern u8 isp_lenc_a[768];
extern u8 isp_lenc_d[768];
SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
	//WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on
    syslog(LOG_WARNING,"isp setting version:v2.1.4--\n ");
    syslog(LOG_INFO,"g_pControl_L:%x\n ",g_pControl_L);

	/*************************************************/
	//WriteReg32(0xe0088000, 0x8a3f0100);	// pre pipe top,3D DNS off
	WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on
                    	// 00-[7] RAW_DNS_En      
                    	// 00-[6] BinC_En         
                    	// 00-[5] DPC_En          
                    	// 00-[4] DPCOTP_En       
                    	// 00-[3] AWBG_En         
                    	// 00-[2] Lens_Online_En  
                    	// 00-[1] LENC_En         
                    	// 00-[0] Pre_Pipe_Manu_En
                    	// 01-[7] Binc_New_En
                    	// 01-[6] DNS_3D_En       
                    	// 01-[5] Stretch_En      
                    	// 01-[4] Hist_Stats_En   
                    	// 01-[3] CCM_En          
                    	// 01-[2] AEC_AGC_En      
                    	// 01-[1] Curve_AWB_En    
                    	// 01-[0] CIP_En
                    	// 02-[7:4] reserved       
                    	// 02-[3] dpc_black_en     
                    	// 02-[2] dpc_white_en     
                    	// 02-[1] pdf_en           
                    	// 02-[0] ca_en            
                    	// 03-[7] Manual_Ctrl_En   -           
                    	// 03-[6] Manual_Size_En   
                    	// 03-[5] Lenc_mirror      
                    	// 03-[4] Lenc_flip        
                    	// 03-[3:2] reserved       
                    	// 03-[1] Binc_Mirror      
                    	// 03-[0] Binc_Flip     

	WriteReg32(0xe0088004, 0x00400001);	//[27:16]blc,[9:8]input pattern BG/GR
                    
	WriteReg32(0xe0089200, 0x11050000);	// [31:24]latch enable, [23:16]01-sensor1,10-sensor2,11-two sensor

	WriteReg32(0xe0089800, 0x2c000000);	// post pipe top
                    	// 00[0]  Manual_Ctrl_En    
                    	// 00[1]  Post_Pipe_Manu_En 
                    	// 00[2]  RGB_Gamma_En   
                    	// 00[3]  JPEG_Ycbcr_En  
                    	// 00[4]  SDE_En         
                    	// 00[5]  UV_DNS_En      
                    	// 00[6]  Anti_Shake_En  
                    	// 00[7]  Reserved    
					
// ================ interrupt ================
//48 00063054 00000000; [27:24]select stat done:0-stat all done,1-AWB done,2-hist done

// ================ DMA ================ 

// input size(fw)
	WriteReg32(L_BASE_ADDRESS + 0x0000, 0x02d00500);	// w_in|h_in
// ================ ISP function ================
#define AEC_AGC_SETTING
#define RGBH_STRETCH_SETTING
//#define MAN_AWB_SETTING
#define CURVE_AWB_SETTING
#define STANDARD_AWB
#define CCM_SETTING
#define LENC_SETTING
//#define LENS_ONLINE_SETTING
#define RAW_DNS_SETTING
#define UV_DNS_SETTING
#define Sharpen_SETTING
#define GAMMA_SETTING
#define DNS3D_SETTING
#define CA_SETTING

#ifdef AEC_AGC_SETTING
// meanY stat(HW)
	WriteReg32(0xe0088b18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe0088b1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe0088b08, 0x20204040);	// left|top|width|height, ratio, bw=7 
	WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe0088b30, 0x04020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe0088b34, 0x00000205);	// [12:8]weight12,[4:1]stat sampling,[0]stat brightest channel enable
// hist stat(HW)
	WriteReg32(0xe0088c04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe0088c08, 0x00000011);	// top|bottom, bw=13
// AEC para(FW)
	WriteReg32(L_BASE_ADDRESS + 0x0010, 0x08020100);	// AECExposureShift | SensorGainMode | WriteSensorEnable | AdvancedAECEnable
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00203c3c);	// [31:24]target low,[23:16]target high
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x01040402);	// [31:16]pStableRange[2], FastStep | SlowStep

	WriteReg32(L_BASE_ADDRESS + 0x0038, 0x04010100); //nSaturationPer2 | nSaturationPer1 | nApplyAnaGainMode | nReadHistMeanSelect
    WriteReg32(L_BASE_ADDRESS + 0x003c, 0x040b00fd); //nMaxFractalExp | nMaxFractalExp | bAllowFractionalExp | nSaturateRefBin

	g_pControl_L->bAECManualEnable = 0; //0x0022
	WriteReg32(L_BASE_ADDRESS + 0x0024, 0x001003ff);	// 01f80010; max gain|min gain
//;48 10032028 00000280; max exposure
	WriteReg32(L_BASE_ADDRESS + 0x002c, 0x00000001);	// min exposure
	WriteReg32(L_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(L_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(L_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(L_BASE_ADDRESS + 0x0064, 0x35093508);	// gain
	WriteReg32(L_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(L_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]	
	WriteReg32(L_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(L_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(L_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x007c, 0x01c000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(L_BASE_ADDRESS + 0x0080, 0x00000000);	// SensorDigiGainEnable | ApplyDigiGainMode | MeteringOption | LumOption

    g_pControl_L->nLinearExp = 0x01;
#endif

// down framerate
//48 10032028 0000031a; max exposure
//48 10032054 096c0322; [31:16]VTS,[15:8]device ID,[7:0]I2C option

#ifdef RGBH_STRETCH_SETTING
// rgbh stretch
//48 e0088d80 02400180; [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d80, 0x01000100);	// [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d84, 0x01003fff);	// [29:16]manual low level, [13:0] min high level
//48 e0088d88 04011008;
//48 e0088d8c 00010400;
//48 e0088d8c 02010500;
#endif

#ifdef MAN_AWB_SETTING
// manual AWB gain
	WriteReg32(L_BASE_ADDRESS + 0x01ac, 0x08000000);	// [31:24]scale bits,[23:16]manualAWB_en
	WriteReg32(L_BASE_ADDRESS + 0x00a0, 0x00aa0080);	// b_gain|g_gain
	WriteReg32(L_BASE_ADDRESS + 0x00a4, 0x00f80080);	// [31:16]r_gain
	WriteReg32(L_BASE_ADDRESS + 0x0094, 0x00000000);	// b_offset|gb_offset|gr_offset|r_offset,x128 when applied
#endif

#ifdef CURVE_AWB_SETTING
	g_pControl_L->bROIEnable = 1;
// AWB stat
	WriteReg32(0xe0088a00, 0x00000000);	// [0]GW
	WriteReg32(0xe0088a0c, 0x000e03c0);	// 000e03e0; min_stat_val|max_stat_val,bw=10 (?)
	WriteReg32(0xe0088a10, 0x00000001);	// step=1(downsample=2)
// AWB calc
	WriteReg32(L_BASE_ADDRESS + 0x008c, 0x020003ff);	// maxBgain|maxGBgain
	WriteReg32(L_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
//	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10050100);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10050000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh

// XY settings
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x00120026);	//
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfdf7fcea);	//
// AWB map
// low map
	WriteReg32(L_BASE_ADDRESS + 0x00d0, 0x33333333);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d4, 0x00002333);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d8, 0x33333333);	//
	WriteReg32(L_BASE_ADDRESS + 0x00dc, 0x00002333);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e0, 0x55555500);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e4, 0x00022335);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e8, 0x55555330);	//
	WriteReg32(L_BASE_ADDRESS + 0x00ec, 0x00023335);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f0, 0x88866630);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f4, 0x00023358);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f8, 0x88888660);	//
	WriteReg32(L_BASE_ADDRESS + 0x00fc, 0x00024588);	//
	WriteReg32(L_BASE_ADDRESS + 0x0100, 0x88888866);	//
	WriteReg32(L_BASE_ADDRESS + 0x0104, 0x00024888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0108, 0x88888866);	//
	WriteReg32(L_BASE_ADDRESS + 0x010c, 0x00024888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0110, 0x88888866);	//
	WriteReg32(L_BASE_ADDRESS + 0x0114, 0x00444888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0118, 0xdddddaa0);	//
	WriteReg32(L_BASE_ADDRESS + 0x011c, 0x04466ddd);	//
	WriteReg32(L_BASE_ADDRESS + 0x0120, 0xfffdaa00);	//
	WriteReg32(L_BASE_ADDRESS + 0x0124, 0x0466dfff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0128, 0xfffaa000);	//
	WriteReg32(L_BASE_ADDRESS + 0x012c, 0x046dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0130, 0xfffaa000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0134, 0x046dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0138, 0xffaa0000);	//
	WriteReg32(L_BASE_ADDRESS + 0x013c, 0x0466dfff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0140, 0xdaa00000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0144, 0x00066ddd);	//
	WriteReg32(L_BASE_ADDRESS + 0x0148, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x014c, 0x00000000);	//
// middle map
	WriteReg32(L_BASE_ADDRESS + 0x0150, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0154, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0158, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x015c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0160, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0164, 0x1fe01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0168, 0x0fc01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x016c, 0x00000fc0);	//
// high map
	WriteReg32(L_BASE_ADDRESS + 0x0170, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0174, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0178, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x017c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0180, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0184, 0x1fe01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0188, 0x0fc01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x018c, 0x00000000);	//

// ROIs
	WriteReg32(0xe0088a14, 0xfff20018);
	WriteReg32(0xe0088a18, 0x000f000c); // W | H
	WriteReg32(0xe0088a1c, 0xfff8fff2); 
	WriteReg32(0xe0088a20, 0x003b000f); // W | H 
	WriteReg32(0xe0088a24, 0xfff7ffe3); 
	WriteReg32(0xe0088a28, 0x00580003); // W | H
	WriteReg32(0xe0088a2c, 0x00000000);
	WriteReg32(0xe0088a30, 0x00000000); // W | H

	WriteReg32(L_BASE_ADDRESS + 0x01d8, 0x30303030);
	WriteReg32(L_BASE_ADDRESS + 0x01dc, 0x10083030);
	WriteReg32(L_BASE_ADDRESS + 0x01e0, 0x60402018); // Weight 3,4,5
	WriteReg32(L_BASE_ADDRESS + 0x01e4, 0x20181008);
	WriteReg32(L_BASE_ADDRESS + 0x01e8, 0x08088050);
	WriteReg32(L_BASE_ADDRESS + 0x01ec, 0x08080808);
	
	WriteReg32(L_BASE_ADDRESS + 0x01cc, 0x00640032); // Thresholds
	WriteReg32(L_BASE_ADDRESS + 0x01d0, 0x012c00c8);
	WriteReg32(L_BASE_ADDRESS + 0x01d4, 0x138802bc);
	
	WriteReg32(L_BASE_ADDRESS + 0x01f0, 0x01000080); // Offset and ROIOption

// Awb shift
	WriteReg32(L_BASE_ADDRESS + 0x01b0, 0x0062003f); //AWBShiftCTCP[0] | AWBShiftCTCP[1]
	WriteReg32(L_BASE_ADDRESS + 0x01b4, 0x0144006c); //AWBShiftCTCP[2] | AWBShiftCTCP[3]
	WriteReg32(L_BASE_ADDRESS + 0x01b8, 0x010001a0); //AWBShiftCTCP[4] | nAWBBrTh[0]
	WriteReg32(L_BASE_ADDRESS + 0x01bc, 0x04000200); //nAWBBrTh[1] | nAWBBrTh[2]
	WriteReg32(L_BASE_ADDRESS + 0x05a8, 0x0e002204); //... | nAWBBrTh[3]
	 // For A/H light: [3]: 5lux [2]: 50lux [1]: 100lux [0]: 500~300lux
	WriteReg32(L_BASE_ADDRESS + 0x05cc, 0x474b4c50); //AWBShift DX000[0] | DX000[1] | DX000[2] | DX000[3]
	WriteReg32(L_BASE_ADDRESS + 0x05d0, 0xcaa9a6a3); //AWBShift DY000[0] | DY000[1] | DY000[2] | DY000[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c4, 0x494b4c6a); //AWBShift DX00[0] | DX00[1] | DX00[2] | DX00[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c8, 0xb0a4a191); //AWBShift DY00[0] | DY00[1] | DY00[2] | DY00[3]
	WriteReg32(L_BASE_ADDRESS + 0x05ac, 0x80808080); //AWBShift DX0[0] | DX0[1] | DX0[2] | DX0[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b0, 0x80808080); //AWBShift DY0[0] | DY0[1] | DY0[2] | DY0[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b4, 0x80808080); //AWBShift DX1[0] | DX1[1] | DX1[2] | DX1[3]
	WriteReg32(L_BASE_ADDRESS + 0x05b8, 0x80808080); //AWBShift DY1[0] | DY1[1] | DY1[2] | DY1[3]
	WriteReg32(L_BASE_ADDRESS + 0x05bc, 0x80808080); //AWBShift DX2[0] | DX2[1] | DX2[2] | DX2[3]
	WriteReg32(L_BASE_ADDRESS + 0x05c0, 0x80808080); //AWBShift DY2[0] | DY2[1] | DY2[2] | DY2[3]
#endif

#ifdef CCM_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x02018001);	// UVSaturate enable | mode
//	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x02408000);	// UVSaturate enable | mode
	WriteReg32(L_BASE_ADDRESS + 0x03f8, 0x00000130);	//
	WriteReg32(L_BASE_ADDRESS + 0x03fc, 0x000001f8);	//
	// CCM
	 WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xFEE101EB); 
	 WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xFFF90034); 
	 WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xFF69019E); 
	 WriteReg32(L_BASE_ADDRESS + 0x03c8, 0xFF2AFFFD); 
	 WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x013801D9); // center(c)
	 WriteReg32(L_BASE_ADDRESS + 0x03d0, 0x0086FF42);
	 WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x0173FFF2);
	 WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xFFDFFF9B);
	 WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x0136FFEB); // left(a)
	 WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xFF0801E5);
	 WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xFFA40013);
	 WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xFF9D01BF);
	 WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xFF75FFED);
	 WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x0100019E); // right(d), [15:0] maunual CT
	WriteReg32(L_BASE_ADDRESS + 0x03b0, 0x00500036); // thre[0] | thre[1]
	WriteReg32(L_BASE_ADDRESS + 0x03b4, 0x00700060); // thre[2] | thre[3]
	WriteReg32(L_BASE_ADDRESS + 0x03b8, 0x012000e0); // thre[4] | thre[5]
	WriteReg32(L_BASE_ADDRESS + 0x03ac, 0x80808001); //[31:24] auto CT enable
	WriteReg32(L_BASE_ADDRESS + 0x0490, 0x01380001);
	WriteReg32(L_BASE_ADDRESS + 0x0494, 0x0086FF42);
	WriteReg32(L_BASE_ADDRESS + 0x0498, 0x0173FFF2);
	WriteReg32(L_BASE_ADDRESS + 0x049c, 0xFFDFFF9B);
	WriteReg32(L_BASE_ADDRESS + 0x04a0, 0x0136FFEB); // last(h)
#endif

#ifdef LENC_SETTING
// LENC
	WriteReg32(L_BASE_ADDRESS + 0x0388, 0x0110007e);	// thre[0],thre[1],16bit
	WriteReg32(L_BASE_ADDRESS + 0x038c, 0x01600130);	// thre[2],thre[3],16bit
	WriteReg32(ISP_LENC_ADDR_L + ISP_LENC_OFFSET_HVSCALE, 0x0b3313e9);	// h_scale|v_scale
// A light shading
	if(isp_lenc_a[0] != 0){
		syslog(LOG_ERR, "Apply A light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0x700), isp_lenc_a, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0700, 0x181b1e25);	//
	WriteReg32(L_BASE_ADDRESS + 0x0704, 0x14141516);	//
	WriteReg32(L_BASE_ADDRESS + 0x0708, 0x18161514);	//
	WriteReg32(L_BASE_ADDRESS + 0x070c, 0x28231e1a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0710, 0x0f111416);	//
	WriteReg32(L_BASE_ADDRESS + 0x0714, 0x0d0d0e0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0718, 0x100e0d0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x071c, 0x1a171311);	//
	WriteReg32(L_BASE_ADDRESS + 0x0720, 0x0a0b0d0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0724, 0x08080809);	//
	WriteReg32(L_BASE_ADDRESS + 0x0728, 0x0a090808);	//
	WriteReg32(L_BASE_ADDRESS + 0x072c, 0x100f0c0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0730, 0x0608090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0734, 0x05050505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0738, 0x06050504);	//
	WriteReg32(L_BASE_ADDRESS + 0x073c, 0x0c0a0907);	//
	WriteReg32(L_BASE_ADDRESS + 0x0740, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0744, 0x03030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0748, 0x04030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x074c, 0x09070604);	//
	WriteReg32(L_BASE_ADDRESS + 0x0750, 0x03030406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0754, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0758, 0x02020201);	//
	WriteReg32(L_BASE_ADDRESS + 0x075c, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0760, 0x02030305);	//
	WriteReg32(L_BASE_ADDRESS + 0x0764, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0768, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x076c, 0x04040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0770, 0x02020304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0774, 0x00000101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0778, 0x01010000);	//
	WriteReg32(L_BASE_ADDRESS + 0x077c, 0x05040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0780, 0x02020304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0784, 0x00000101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0788, 0x01010100);	//
	WriteReg32(L_BASE_ADDRESS + 0x078c, 0x05040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0790, 0x02030405);	//
	WriteReg32(L_BASE_ADDRESS + 0x0794, 0x01010102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0798, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x079c, 0x05040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a0, 0x03040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a4, 0x02020303);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a8, 0x03030202);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ac, 0x07060404);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b0, 0x05060708);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b4, 0x04040404);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b8, 0x05040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x07bc, 0x09080705);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c0, 0x08090a0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c4, 0x05060607);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c8, 0x07070606);	//
	WriteReg32(L_BASE_ADDRESS + 0x07cc, 0x0d0c0a09);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d0, 0x0c0d0e11);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d4, 0x0a090a0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d8, 0x0c0b0a0a);	//
	WriteReg32(L_BASE_ADDRESS + 0x07dc, 0x14110f0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e0, 0x1214171b);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e4, 0x0f101011);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e8, 0x13121010);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ec, 0x1c1c1815);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f0, 0x1f23292d);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f4, 0x191a1a1c);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f8, 0x1f1c1b19);	//
	WriteReg32(L_BASE_ADDRESS + 0x07fc, 0x572f2822);	//
	WriteReg32(L_BASE_ADDRESS + 0x0800, 0x7f7b7e75);	//
	WriteReg32(L_BASE_ADDRESS + 0x0804, 0x7c7e7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0808, 0x7a7d7c7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x080c, 0x807a7c79);	//
	WriteReg32(L_BASE_ADDRESS + 0x0810, 0x7d7d7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0814, 0x7d7c7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0818, 0x7d7b7d7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x081c, 0x787b7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0820, 0x8180817e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0824, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0828, 0x807f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x082c, 0x7e7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0830, 0x81828184);	//
	WriteReg32(L_BASE_ADDRESS + 0x0834, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0838, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x083c, 0x7e817f82);	//
	WriteReg32(L_BASE_ADDRESS + 0x0840, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0844, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0848, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x084c, 0x7f808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0850, 0x817f8082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0854, 0x81808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0858, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x085c, 0x807f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0860, 0x81817e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0864, 0x7f808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0868, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x086c, 0x807f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0870, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0874, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0878, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x087c, 0x7e7f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0880, 0x8181807e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0884, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0888, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x088c, 0x7f7f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0890, 0x81807e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0894, 0x82808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0898, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x089c, 0x7e808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a0, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a4, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a8, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ac, 0x7d808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b0, 0x81818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b4, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b8, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08bc, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c0, 0x82828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c4, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08cc, 0x7f808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d0, 0x8080807c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d4, 0x817f8180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d8, 0x817f817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x08dc, 0x7d7f7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e0, 0x7e7e7d7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e4, 0x7e7e7e7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e8, 0x7d7d7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ec, 0x7c7c7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f0, 0x807f7e73);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f4, 0x7b7b7b7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f8, 0x7c7d7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x08fc, 0x73777980);	//
	WriteReg32(L_BASE_ADDRESS + 0x0900, 0x8a898e8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0904, 0x85888989);	//
	WriteReg32(L_BASE_ADDRESS + 0x0908, 0x8a898787);	//
	WriteReg32(L_BASE_ADDRESS + 0x090c, 0x908c8c8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0910, 0x86878889);	//
	WriteReg32(L_BASE_ADDRESS + 0x0914, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0918, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x091c, 0x8a898887);	//
	WriteReg32(L_BASE_ADDRESS + 0x0920, 0x85858586);	//
	WriteReg32(L_BASE_ADDRESS + 0x0924, 0x85848585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0928, 0x85858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x092c, 0x87868685);	//
	WriteReg32(L_BASE_ADDRESS + 0x0930, 0x83848586);	//
	WriteReg32(L_BASE_ADDRESS + 0x0934, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0938, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x093c, 0x84858584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0940, 0x83838385);	//
	WriteReg32(L_BASE_ADDRESS + 0x0944, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0948, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x094c, 0x83848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0950, 0x82828382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0954, 0x81808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0958, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x095c, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0960, 0x81828284);	//
	WriteReg32(L_BASE_ADDRESS + 0x0964, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0968, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x096c, 0x83838282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0970, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0974, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0978, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x097c, 0x83838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0980, 0x82818382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0984, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0988, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x098c, 0x83838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0990, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0994, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0998, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x099c, 0x85838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a0, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a4, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a8, 0x82828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ac, 0x84838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b0, 0x83838484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b4, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b8, 0x84838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x09bc, 0x86858483);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c0, 0x85858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c4, 0x84848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c8, 0x84848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09cc, 0x85868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d0, 0x86868687);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d4, 0x86868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d8, 0x87868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09dc, 0x89888787);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e0, 0x888a8b8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e4, 0x88888889);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e8, 0x89898888);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ec, 0x8b8d8b8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f0, 0x8f8b8d8e);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f4, 0x8d8b8d8c);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f8, 0x8d8e8f8c);	//
	WriteReg32(L_BASE_ADDRESS + 0x09fc, 0x9590908c);
	}
// C light shading
	if(isp_lenc_c[0] != 0){
		syslog(LOG_ERR, "Apply C light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0xa00), isp_lenc_c, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0a00, 0x2b30363e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a04, 0x24242728);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a08, 0x27252424);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a0c, 0x37322c29);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a10, 0x1c1f2328);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a14, 0x19191a1b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a18, 0x1b1a1919);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a1c, 0x26231f1d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a20, 0x13141619);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a24, 0x0f0f0f11);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a28, 0x11100f0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a2c, 0x1a171413);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a30, 0x0c0e1013);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a34, 0x0909090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a38, 0x0b0a0909);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a3c, 0x110f0d0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a40, 0x08090b0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a44, 0x04040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a48, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a4c, 0x0c0a0807);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a50, 0x0406080a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a54, 0x01010103);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a58, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a5c, 0x09070403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a60, 0x03040607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a64, 0x01000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a68, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a6c, 0x06040201);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a70, 0x02030506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a74, 0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a78, 0x02010000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a7c, 0x04030102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a80, 0x01020306);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a84, 0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a88, 0x02010001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a8c, 0x04020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a90, 0x01020305);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a94, 0x01010001);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a98, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a9c, 0x04020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa0, 0x01020407);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa4, 0x01000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa8, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aac, 0x06040201);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab0, 0x0305070a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab4, 0x01010103);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab8, 0x02010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0abc, 0x08070403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac0, 0x07090b0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac4, 0x04040506);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac8, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0acc, 0x0c0a0907);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad0, 0x0c0e1013);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad4, 0x0808090a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad8, 0x0a090808);	//
	WriteReg32(L_BASE_ADDRESS + 0x0adc, 0x12100d0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae0, 0x1214171b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae4, 0x0e0e0f11);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae8, 0x110f0e0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aec, 0x19171412);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af0, 0x1b1c2026);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af4, 0x16161719);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af8, 0x18161515);	//
	WriteReg32(L_BASE_ADDRESS + 0x0afc, 0x231e1b1a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b00, 0x797a7a78);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b04, 0x7b79797a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b08, 0x7a7a7a7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b0c, 0x7d797a7b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b10, 0x7c7b7a79);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b14, 0x7c7d7d7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b18, 0x7d7d7d7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b1c, 0x787a7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b20, 0x7f7e7e7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b24, 0x7e7e7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b28, 0x7e7e7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b2c, 0x7d7d7e7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b30, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b34, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b38, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b3c, 0x7d7d7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b40, 0x81817f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b44, 0x80818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b48, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b4c, 0x7c7f7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b50, 0x8080807e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b54, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b58, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b5c, 0x7f7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b60, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b64, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b68, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b6c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b70, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b74, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b78, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b7c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b80, 0x807f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b84, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b88, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b8c, 0x7f7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b90, 0x80807e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b94, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b98, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b9c, 0x7c7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba0, 0x80807f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba4, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba8, 0x807f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bac, 0x7e7e7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb0, 0x80807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bbc, 0x7e7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc0, 0x80807f7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc4, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bcc, 0x7e7e7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd0, 0x7f7f7d7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd4, 0x7f7f7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd8, 0x7f807f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bdc, 0x7b7d7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be0, 0x7c7d7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be4, 0x7c7c7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be8, 0x7c7c7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bec, 0x7b7b7b7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf0, 0x7c7b7c76);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf4, 0x7b7a7c7c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf8, 0x7b7c7b7a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bfc, 0x747b7c7d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c00, 0x8d8d8e8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c04, 0x8b8d8d8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c08, 0x8c8c8c8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c0c, 0x958a8a8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c10, 0x888a8a8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c14, 0x85858687);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c18, 0x86858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c1c, 0x878a8986);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c20, 0x83848587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c24, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c28, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c2c, 0x86848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c30, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c34, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c38, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c3c, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c40, 0x81828284);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c44, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c48, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c4c, 0x83828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c50, 0x82818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c54, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c58, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c5c, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c60, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c64, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c68, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c6c, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c70, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c74, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c78, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c7c, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c80, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c84, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c88, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c8c, 0x82808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c90, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c98, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c9c, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca0, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca4, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca8, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cac, 0x82818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb0, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb4, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cbc, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc0, 0x82828384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc8, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ccc, 0x84838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd0, 0x84848482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd4, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd8, 0x83838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cdc, 0x84858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce0, 0x84858587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce4, 0x85858685);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce8, 0x85858485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cec, 0x86858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf0, 0x86888989);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf4, 0x84848385);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf8, 0x85848584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cfc, 0x87878685);	//
	}
// D light shading
	if(isp_lenc_d[0] != 0){
		syslog(LOG_ERR, "Apply D light shading from flash\n");
		memcpy((u8*)(L_BASE_ADDRESS+0xd00), isp_lenc_d, 768);
	}else{
	WriteReg32(L_BASE_ADDRESS + 0x0d00, 0x282a323b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d04, 0x1e1f2124);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d08, 0x241f201e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d0c, 0x57312b26);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d10, 0x181b1f22);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d14, 0x13131416);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d18, 0x15151313);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d1c, 0x1d1f1a18);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d20, 0x0f101316);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d24, 0x0c0c0d0e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d28, 0x0d0d0c0c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d2c, 0x1812100e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d30, 0x0a0c0d0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d34, 0x07070808);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d38, 0x08080707);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d3c, 0x0d0d0b0a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d40, 0x0607090b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d44, 0x04040505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d48, 0x05050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d4c, 0x0b080706);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d50, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d54, 0x03030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d58, 0x04030303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d5c, 0x08060604);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d60, 0x03040406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d64, 0x02020302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d68, 0x03030202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d6c, 0x06050404);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d70, 0x02030405);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d74, 0x02010102);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d78, 0x03020202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d7c, 0x05040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d80, 0x02030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d84, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d88, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d8c, 0x04040302);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d90, 0x02030403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d94, 0x01010202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d98, 0x02020101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d9c, 0x04040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da0, 0x03040406);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da4, 0x02020203);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da8, 0x03020202);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dac, 0x06040403);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db0, 0x04050607);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db4, 0x03030304);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db8, 0x04040303);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dbc, 0x07060505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc0, 0x0607080a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc4, 0x04050505);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc8, 0x06050504);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dcc, 0x0a080706);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd0, 0x090b0c0f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd4, 0x07070708);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd8, 0x08080707);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ddc, 0x0f0d0b09);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de0, 0x0e101214);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de4, 0x0b0b0c0d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de8, 0x0d0c0b0b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dec, 0x1413100f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df0, 0x16171e22);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df4, 0x13121414);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df8, 0x15141212);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dfc, 0x221c1914);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e00, 0x7e7b8075);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e04, 0x7d7b807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e08, 0x7c807c80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e0c, 0x7278817c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e10, 0x807e7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e14, 0x7f7f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e18, 0x7e7f7f7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e1c, 0x7f7d7c7e);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e20, 0x8281837d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e24, 0x80818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e28, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e2c, 0x7e7f817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e30, 0x83838182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e34, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e38, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e3c, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e40, 0x82838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e44, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e48, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e4c, 0x7f818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e50, 0x83818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e54, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e58, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e5c, 0x7f818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e60, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e64, 0x81808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e68, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e6c, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e70, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e74, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e78, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e7c, 0x7e818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e80, 0x81828080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e84, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e88, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e8c, 0x80808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e90, 0x81828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e98, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e9c, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea0, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea4, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea8, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eac, 0x7e808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb0, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb4, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ebc, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec0, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec4, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ecc, 0x7f818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed0, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed4, 0x81828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed8, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0edc, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee0, 0x80807f84);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee4, 0x81807f81);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eec, 0x81807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef0, 0x7d828275);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef4, 0x7d80807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef8, 0x7e7f7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0efc, 0x727f807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f00, 0x9694958d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f04, 0x928f9493);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f08, 0x8b9e8994);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f0c, 0x8f969792);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f10, 0x8d8e9090);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f14, 0x8d8b8c8d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f18, 0x8e8c8e8a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f1c, 0x90938e8f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f20, 0x898b8a8b);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f24, 0x87898888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f28, 0x89898789);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f2c, 0x8c8c8b89);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f30, 0x8688868c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f34, 0x85878587);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f38, 0x87868686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f3c, 0x8a878887);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f40, 0x85848784);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f44, 0x85838485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f48, 0x84858484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f4c, 0x86878585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f50, 0x84848486);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f54, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f58, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f5c, 0x84858584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f60, 0x83838386);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f64, 0x81828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f68, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f6c, 0x86858383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f70, 0x81838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f74, 0x80818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f78, 0x83808082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f7c, 0x83848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f80, 0x82838381);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f84, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f88, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f8c, 0x86838382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f90, 0x82828384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f94, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f98, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f9c, 0x84838482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa0, 0x8282847f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa4, 0x80818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa8, 0x82828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fac, 0x85848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb0, 0x82838287);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb8, 0x84828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fbc, 0x86848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc0, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc4, 0x82838283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc8, 0x84838482);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fcc, 0x83868684);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd0, 0x86868787);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd4, 0x85848584);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd8, 0x85858684);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fdc, 0x8a888686);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe0, 0x8788878d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe4, 0x86868589);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe8, 0x88878786);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fec, 0x8c898988);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff0, 0x8589907f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff4, 0x888a838a);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff8, 0x88888589);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ffc, 0x88918b85);	
	}
#endif

#ifdef LENS_ONLINE_SETTING
//lens online
	WriteReg32(0xe0088180, 0x00000237);	// [10:8]frame_cn,[5]auto_stat_en_1,[4]auto_tmp_1,[3:2]split stat mode
	WriteReg32(0xe0088184, 0x00020404);	//
	WriteReg32(0xe0088194, 0x03031010);	// compare with 03034040, 01010404
	WriteReg32(0xe0088198, 0x1810f0f0);	// compare with 1810c0c0, 1810fcfc
	WriteReg32(0xe00881a4, 0x00400100);	// nSensorGain[0]|nSensorGain[1], bw=10
	WriteReg32(0xe00881a8, 0x00000000);	// [3:0]m_nMinLensGain
#endif

#ifdef RAW_DNS_SETTING
//RAWDNS
	WriteReg32(0xe0088500, 0x00060007);	// sigma[0]|sigma[1] ;000a0010
	WriteReg32(0xe0088504, 0x0008000a);	// sigma[2]|sigma[3] ;001a0026
	WriteReg32(0xe0088508, 0x00100012);	// sigma[4]|sigma[5] ;00300040
	WriteReg32(0xe008850c, 0x1a1a1616);	// Gns[0]|Gns[1]|Gns[2]|Gns[3]
	WriteReg32(0xe0088510, 0x14141a1a);	// Gns[4]|Gns[5]|Rbns[0]|Rbns[1]
	WriteReg32(0xe0088514, 0x16161414);	// Rbns[2]|Rbns[3]|Rbns[4]|Rbns[5]

	WriteReg32(0xe0088530, 0x3f3f3f3f);	// 
	WriteReg32(0xe0088534, 0x3f303028);	//
	WriteReg32(0xe0088538, 0x20201010);	//
	WriteReg32(0xe008853c, 0x20202020);	//
	WriteReg32(0xe0088540, 0x1010100a);	//
	WriteReg32(0xe0088544, 0x08040101);	//
#endif

#ifdef UV_DNS_SETTING
	WriteReg32(0xe0089b00, 0x01020480);
	WriteReg32(0xe0089b04, 0x80010101);	// 1x|2x|4x
	WriteReg32(0xe0089b08, 0x01010101);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b0c, 0x01020408);	// 128x|1x|2x|4x
	WriteReg32(0xe0089b10, 0x1014181c);	// 8x|16x|32x|64x	
	WriteReg32(0xe0089b14, 0x20000000);	// 128x
#endif

#ifdef Sharpen_SETTING
	WriteReg32(0xe0088800, 0x3f0462ff);	// cip
					// 00 - [7] 
					// 00 - [6] manual_mode
					// 00 - [5] bIfGbGr
					// 00 - [4] bIfCbCrDns
					// 00 - [3] bIfAntiClrAls
					// 00 - [2] bIfShapren
					// 00 - [1] bIfSharpenNS
					// 00 - [0] bIfDDns
					// 01 - [6:4] GDnsTSlope -- 0
 					// 01 - [3:0] WSlopeClr -- 4
					// 02 - [6:5] SharpenOptions -- 3  
 					// 02 - [4:0] CombW -- 2
					// 03 - [7:0] WSlope -- 0xff
	WriteReg32(0xe0088804, 0x01040810);	// DirGDNST0|1|2|3
	WriteReg32(0xe0088808, 0x12140808);	// [31:16] DirGDNST4|5
					// 0a - [5:0] DirGDNSLevel0
					// 0b - [5:0] DirGDNSLevel1
	WriteReg32(0xe008880c, 0x08080b10);	// 0c - [5:0] DirGDNSLevel2
					// 0d - [5:0] DirGDNSLevel3
					// 0e - [5:0] DirGDNSLevel4
					// 0f - [5:0] DirGDNSLevel5
	WriteReg32(0xe0088810, 0x40404040);	// CleanSharpT0|1|2|3
	WriteReg32(0xe0088814, 0x4040ffff);	// [31:16] CleanSharpT4|5
				// [15:0] CleanSharpT0|1
	WriteReg32(0xe0088818, 0xffffffff);	// CleanSharpT2|3|4|5
	WriteReg32(0xe008881c, 0x10182028);	// Tintp0|1|2|3
	WriteReg32(0xe0088820, 0x38481018);	// Tintp4|5
				// TintpHV0|1
	WriteReg32(0xe0088824, 0x20283848);	// TintpHV0|1|2|3
	WriteReg32(0xe0088828, 0x20304050);	// NV0|1|2|3
	WriteReg32(0xe008882c, 0x78a00404);	// [31:16] NV4|5
					// 0e - [0:5] VNGLevel0
					// 0f - [0:5] VNGLevel1
	WriteReg32(0xe0088830, 0x04080808);	// 30 - [0:5] VNGLevel2
					// 31 - [0:5] VNGLevel3
					// 32 - [0:5] VNGLevel4
					// 33 - [0:5] VNGLevel5
	WriteReg32(0xe0088834, 0x04060810);	// 34 - [0:5] LPG0
					// 35 - [0:5] LPG1
					// 36 - [0:5] LPG2
					// 37 - [0:5] LPG3
	WriteReg32(0xe0088838, 0x10142020);	// 38 - [0:5] LPG4
					// 39 - [0:5] LPG5
					// 3a - [0:5] CbrDbsLevel0
					// 3b - [0:5] CbrDbsLevel1
	WriteReg32(0xe008883c, 0x20202020);	// 3c - [0:5] CbrDbsLevel2
					// 3d - [0:5] CbrDbsLevel3
					// 3e - [0:5] CbrDbsLevel4
					// 3f - [0:5] CbrDbsLevel5
	WriteReg32(0xe0088840, 0x00500078);	// [27:16] - TCbCrH0
					// [11:0] - TCbCrH1
	WriteReg32(0xe0088844, 0x00a00140);	// [27:16] - TCbCrH2
					// [11:0] - TCbCrH3
	WriteReg32(0xe0088848, 0x01680190);	// [27:16] - TCbCrH4
					// [11:0] - TCbCrH5
	WriteReg32(0xe008884c, 0x00c80078);	// [27:16] - TCbCrV0
					// [11:0] - TCbCrV1
	WriteReg32(0xe0088850, 0x00a00140);	// [27:16] - TCbCrV2
					// [11:0] - TCbCrV3
	WriteReg32(0xe0088854, 0x01680190);	// [27:16] - TCbCrV4
					// [11:0] - TCbCrV5
	WriteReg32(0xe0088858, 0x00123333);	// [23:20] - TCbrOffset0
					// [19:16] - TCbrOffset1
					// [15:12] - TCbrOffset2
					// [11:8] - TCbrOffset3
					// [7:4] - TCbrOffset4
					// [3:0] - TCbrOffset5
	WriteReg32(0xe008885c, 0x6455463c);	// CbCrAlsHf0|1|2|3
	WriteReg32(0xe0088860, 0x3732c83c);	// [31:16] CbCrAlsHf4|5|
					// [15:0] CbCrAlsMean0|1
	WriteReg32(0xe0088864, 0x50505050);	// CbCrAlsMean2|3|4|5
	WriteReg32(0xe0088868, 0x0c0b0a08);	// CbrImpulse0|1|2|3
	WriteReg32(0xe008886c, 0x00101204);	// [23:16] CbrImpulse4
				// [15:8] CbrImpulse5
				// [2:0] CbrImpulseThrShift
	WriteReg32(0xe0088870, 0x241c1612);	// 70 - [5:0] SharpenLevel0
				// 71 - [5:0] SharpenLevel1
				// 72 - [5:0] SharpenLevel2
				// 73 - [5:0] SharpenLevel3
	WriteReg32(0xe0088874, 0x0e0a0808);	// 74 - [5:0] SharpenLevel4
				// 75 - [5:0] SharpenLevel5
				// 76 - [7:0] SharpenThr0
				// 77 - [7:0] SharpenThr1
	WriteReg32(0xe0088878, 0x0a101216);	// SharpenThr2|3|4|5
	WriteReg32(0xe008887c, 0x0002060a);	// 7d - [5:0] LocalRatio0
				// 7e - [5:0] LocalRatio1
				// 7f - [5:0] LocalRatio2
	WriteReg32(0xe0088880, 0x14141414);	// 80 - [5:0] SharpenHalo0
					// 81 - [5:0] SharpenHalo1
					// 82 - [5:0] SharpenHalo2
					// 83 - [5:0] SharpenHalo3
	WriteReg32(0xe0088884, 0x1414503a);	// 84 - [5:0] SharpenHalo4
					// 85 - [5:0] SharpenHalo5
					// 86 - [7:0] SharpenCoef0 (48)
					// 87 - [7:0] SharpenCoef1 (39)
	WriteReg32(0xe0088888, 0x17060104);	// [31:15] SharpenCoef2|3|4 (1b, 07, 01)
					// [7:0] DarkOffset
	WriteReg32(0xe008888c, 0x44444444);	
					// [31:28] SharpenHDelta0 (4)
					// [27:24] SharpenHDelta1 (4)
					// [23:20] SharpenHDelta2 (4)
					// [19:16] SharpenHDelta3 (4)
					// [15:12] SharpenHDelta4 (4)
					// [11:8] SharpenHDelta5 (4)
					// [7:4] SharpenHDelta6 (4)
					// [3:0] SharpenHDelta7 (4)
//48 e0088890 0204080b;  90 - [5:0] SharpenHGain0 (08)
				// 91 - [5:0] SharpenHGain1 (0c)
				// 92 - [5:0] SharpenHGain2 (0a)
				// 93 - [5:0] SharpenHGain3 (09)
//48 e0088894 0804080c 	; 94 - [5:0] SharpenHGain4 (08)
				// 95 - [5:0] SharpenHGain5 (0a)
				// 96 - [5:0] SharpenHGain6 (0a)
				// 97 - [5:0] SharpenHGain7 (0a)

	WriteReg32(0xe0088890, 0x121a1c1a);	
				//  90 - [5:0] SharpenHGain0 (08)
				// 91 - [5:0] SharpenHGain1 (0c)
				// 92 - [5:0] SharpenHGain2 (0a)
				// 93 - [5:0] SharpenHGain3 (09)
	WriteReg32(0xe0088894, 0x16181a14);	
				// 94 - [5:0] SharpenHGain4 (08)
				// 95 - [5:0] SharpenHGain5 (0a)
				// 96 - [5:0] SharpenHGain6 (0a)
				// 97 - [5:0] SharpenHGain7 (0a)
	WriteReg32(0xe0088898, 0x0e000010);	
					// 9b - [5:0] SharpenHGain8 (06)
					// 99 - [5:0] localSymGThr0
					// 9a - [5:0] localSymGThr1
					// 9b - [5:0] localSymGThr2
	WriteReg32(0xe008889c, 0x00101820);	
					// 9c - reserved
					// 9d - [5:0] localDNSThr0
					// 9e - [5:0] localDNSThr1
					// 9f - [5:0] localDNSThr2

//	WriteReg32(L_BASE_ADDRESS + 0x2c4, 0x01000258),
	WriteReg32(L_BASE_ADDRESS + 0x2c8, 0x01a40200),
	WriteReg32(L_BASE_ADDRESS + 0x2cc, 0x01200158),
	WriteReg32(L_BASE_ADDRESS + 0x2d0, 0x00d000f8),
	WriteReg32(L_BASE_ADDRESS + 0x2d4, 0x008000aa),
#endif

#ifdef GAMMA_SETTING
//48 100322d8 00000700
 
	WriteReg32(L_BASE_ADDRESS + 0x0200, 0x00B00060);
	WriteReg32(L_BASE_ADDRESS + 0x0204, 0x01480104);
	WriteReg32(L_BASE_ADDRESS + 0x0208, 0x01B80188);
	WriteReg32(L_BASE_ADDRESS + 0x020c, 0x022001EC);
	WriteReg32(L_BASE_ADDRESS + 0x0210, 0x027A024E);
	WriteReg32(L_BASE_ADDRESS + 0x0214, 0x02CA02A8);
	WriteReg32(L_BASE_ADDRESS + 0x0218, 0x030402E8);
	WriteReg32(L_BASE_ADDRESS + 0x021c, 0x03300318);
	WriteReg32(L_BASE_ADDRESS + 0x0220, 0x03540340);
	WriteReg32(L_BASE_ADDRESS + 0x0224, 0x03700364);
	WriteReg32(L_BASE_ADDRESS + 0x0228, 0x038C0380);
	WriteReg32(L_BASE_ADDRESS + 0x022c, 0x03A40398);
	WriteReg32(L_BASE_ADDRESS + 0x0230, 0x03B803B0);
	WriteReg32(L_BASE_ADDRESS + 0x0234, 0x03CC03C4);
	WriteReg32(L_BASE_ADDRESS + 0x0238, 0x03E003D8);
	WriteReg32(L_BASE_ADDRESS + 0x023c, 0x03F403E8);
	WriteReg32(L_BASE_ADDRESS + 0x0240, 0x006003FF);

	WriteReg32(L_BASE_ADDRESS + 0x0244, 0x010400B0);
	WriteReg32(L_BASE_ADDRESS + 0x0248, 0x01880148);
	WriteReg32(L_BASE_ADDRESS + 0x024c, 0x01EC01B8);
	WriteReg32(L_BASE_ADDRESS + 0x0250, 0x024E0220);
	WriteReg32(L_BASE_ADDRESS + 0x0254, 0x02A8027A);
	WriteReg32(L_BASE_ADDRESS + 0x0258, 0x02E802CA);
	WriteReg32(L_BASE_ADDRESS + 0x025c, 0x03180304);
	WriteReg32(L_BASE_ADDRESS + 0x0260, 0x03400330);
	WriteReg32(L_BASE_ADDRESS + 0x0264, 0x03640354);
	WriteReg32(L_BASE_ADDRESS + 0x0268, 0x03800370);
	WriteReg32(L_BASE_ADDRESS + 0x026c, 0x0398038C);
	WriteReg32(L_BASE_ADDRESS + 0x0270, 0x03B003A4);
	WriteReg32(L_BASE_ADDRESS + 0x0274, 0x03C403B8);
	WriteReg32(L_BASE_ADDRESS + 0x0278, 0x03D803CC);
	WriteReg32(L_BASE_ADDRESS + 0x027c, 0x03E803E0);
	WriteReg32(L_BASE_ADDRESS + 0x0280, 0x03FF03F4);

	WriteReg32(L_BASE_ADDRESS + 0x0284, 0x00B00060);
	WriteReg32(L_BASE_ADDRESS + 0x0288, 0x01480104);
	WriteReg32(L_BASE_ADDRESS + 0x028c, 0x01B80188);
	WriteReg32(L_BASE_ADDRESS + 0x0290, 0x022001EC);
	WriteReg32(L_BASE_ADDRESS + 0x0294, 0x027A024E);
	WriteReg32(L_BASE_ADDRESS + 0x0298, 0x02CA02A8);
	WriteReg32(L_BASE_ADDRESS + 0x029c, 0x030402E8);
	WriteReg32(L_BASE_ADDRESS + 0x02a0, 0x03300318);
	WriteReg32(L_BASE_ADDRESS + 0x02a4, 0x03540340);
	WriteReg32(L_BASE_ADDRESS + 0x02a8, 0x03700364);
	WriteReg32(L_BASE_ADDRESS + 0x02ac, 0x038C0380);
	WriteReg32(L_BASE_ADDRESS + 0x02b0, 0x03A40398);
	WriteReg32(L_BASE_ADDRESS + 0x02b4, 0x03B803B0);
	WriteReg32(L_BASE_ADDRESS + 0x02b8, 0x03CC03C4);
	WriteReg32(L_BASE_ADDRESS + 0x02bc, 0x03E003D8);
	WriteReg32(L_BASE_ADDRESS + 0x02c0, 0x03F403E8);
	WriteReg32(L_BASE_ADDRESS + 0x02c4, 0x025803ff);

	WriteReg32(0xe0089900, 0x08100000);

#endif

#ifdef DNS3D_SETTING
	WriteReg32(0xe0088550, 0x00040010);	// nIfComb on, [15:0] TSigma[0]
	WriteReg32(0xe0088554, 0x00180020);	// TSigma[1]~[2]
	WriteReg32(0xe0088558, 0x00400064);	// TSigma[3]~[4]
	WriteReg32(0xe008855c, 0x00c88006);	// [31:16] Tsigma[5],[15:8] TmpW,[7:0] Ratio
	WriteReg32(0xe0088560, 0x28080000);	// [31:24] PattThr,[23:16] ChgThr,[15:0] Overlap x
	WriteReg32(0xe0088564, 0x00000400);	// [31:16] Overlap y,[15:0] Overlap width
	WriteReg32(0xe0088568, 0x03070000);	// [31:16] Overlap height
#endif
#ifdef CA_SETTING
	// Alpha_0=0, Beta_0=1, Coef_0=3
	WriteReg32(0xe0088700, 0x00010104);
	//WriteReg32(0xe0088704, 0x0406080e);
	//WriteReg32(0xe0088708, 0x14284256);
	//WriteReg32(0xe008870c, 0x08102040);
	//WriteReg32(0xe0088710, 0x6080c0ff);
	WriteReg32(0xe0088714, 0x03010104);
#endif

#ifdef NIGHTMODE
		    WriteReg32(ISP_BASE_ADDR+0x9800,ReadReg32(ISP_BASE_ADDR+0x9800)|BIT28);
			WriteReg32(ISP_BASE_ADDR+0x9a00, 0x00000000) ; //UVoffset0|UVoffset1
			WriteReg32(ISP_BASE_ADDR+0x9a04, 0x0000f800) ; //yoffset| | [1]pe_en|[0]ne_en
			WriteReg32(ISP_BASE_ADDR+0x9a08, 0x00000000) ; //matrix00|matrix01
			WriteReg32(ISP_BASE_ADDR+0x9a0c, 0x00000000) ; //matrix10|matrix11
			WriteReg32(ISP_BASE_ADDR+0x9a1c, 0x00800004) ; //ygain | hthre |hgain
			WriteReg32(ISP_BASE_ADDR+0x9a38, 0x88888800) ; //[0:2]vector shift
#endif

#ifdef BAND50HZ  
// banding
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010207);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#else
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010107);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#endif
#ifdef SENSOR_30FPS
	//WriteReg32(L_BASE_ADDRESS + 0x0044, 0x12860f70);	// 60band|50band,fr=30 vts = 0x3dc
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0f730ce0);	// 60band|50band,fr=30 vts = 0x338
#else  
	//WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0f700cd0);	// 60band|50band,fr=25 vts = 0x3dc
	//WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0ce00aba);	// 60band|50band,fr=25 vts = 0x338
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x0c5c0a4c);	// 60band|50band,fr=24 vts = 0x338
#endif
	u32 vts = libsccb_rd(cfg->sccb_cfg,0x380e)<<8 | libsccb_rd(cfg->sccb_cfg,0x380f);
	WriteReg32(L_BASE_ADDRESS + 0x0028, 0x00000000|(vts-g_pControl_L->nPreChargeWidth));	// max exposure
#ifdef CONFIG_SENSOR_SID_HIGH
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x09200000|vts);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
#else
	WriteReg32(L_BASE_ADDRESS + 0x0054, 0x096c0000|vts);
#endif
//	g_pControl_L->nAWBStep1 = 15;
//	g_pControl_L->nAWBStep2 = 255;
#ifdef CONFIG_AEC_AWB_MANUAL_EN
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x080f0402);	// [31:16]pStableRange[2], FastStep | SlowStep
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00083520);	// [31:24]target low,[23:16]target high
	WriteReg32(0xe0088000, 0x8a7f0100);	// pre pipe top,3D DNS on ccm on
	g_pControl_L->nAWBStep1 = 15;
	g_pControl_L->nAWBStep2 = 255;
	//debug_printf("[L_BASE_ADDRESS + 0x008a]: 0x%x\n", ReadReg16(L_BASE_ADDRESS + 0x008a));
	//debug_printf("[L_BASE_ADDRESS + 0x0018]: 0x%x\n", ReadReg32(L_BASE_ADDRESS + 0x0018));
	//debug_printf("[L_BASE_ADDRESS + 0x0014]: 0x%x\n", ReadReg32(L_BASE_ADDRESS + 0x0014));
#endif

	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};
/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
