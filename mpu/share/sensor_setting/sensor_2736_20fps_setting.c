
/***IQ version: 1.0.1***/
//the setting which grady released in 20170401
#include "includes.h"

#define SENSOR_NAME_INTERNAL 2736 
#define SENSOR_SCCB_MODE_INTERNAL SCCB_MODE16
#define SENSOR_SCCB_ID_INTERNAL 0x6c
#define SENSOR_DATAFMT_INTERNAL DPM_DATAFMT_RAW8
#define SENSOR_INTERFACETYPE_INTERNAL SNR_IF_MIPI_2LN

/*************************
  NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

#define ENLARGE_HTS_60BASED
#ifdef ENLARGE_HTS_60BASED
	#define VTS (0x4a0)
#endif

#define OV2736_2M_20FPS

/*************************
  Initial setting 2736 is HDR sensor
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] = {
//@@ MIPI_1920x1080_60fps
//TWO_LANE; MIRROR
#if 1
{0x0103, 0x01},
{0x0305, 0x3c},
{0x0307, 0x00}, 
{0x0308, 0x03},
{0x0309, 0x03},
{0x0327, 0x07},
{0x3016, 0x32},
{0x3000, 0x00},
{0x3001, 0x00},
{0x3002, 0x00},
{0x3013, 0x00},
{0x301f, 0xf0},
{0x3023, 0xf0},
{0x3020, 0x9b},
{0x3022, 0x51},
{0x3106, 0x11},
{0x3107, 0x01},
{0x3500, 0x00},
{0x3501, 0x40},
{0x3502, 0x00},
{0x3503, 0x88},
{0x3505, 0x83},
{0x3508, 0x01},
{0x3509, 0x80},
{0x350a, 0x04},
{0x350b, 0x00},
{0x350c, 0x00},
{0x350d, 0x80},
{0x350e, 0x04},
{0x350f, 0x00},
{0x3510, 0x00},
{0x3511, 0x00},
{0x3512, 0x20},
{0x3600, 0x55},
{0x3601, 0x54},
{0x3612, 0xb5},
{0x3613, 0xb3},
{0x3616, 0x83},
{0x3621, 0x00},
{0x3624, 0x06},
{0x3642, 0x88},
{0x3660, 0x00},
{0x3661, 0x00},
{0x366a, 0x64},
{0x366c, 0x00},
{0x366e, 0x44},
{0x366f, 0x4f},
{0x3677, 0x11},
{0x3678, 0x11},
{0x3680, 0xff},
{0x3681, 0xd2},
{0x3682, 0xa9},
{0x3683, 0x91},
{0x3684, 0x8a},
{0x3620, 0x80},
{0x3662, 0x10},
{0x3663, 0x24},
{0x3665, 0xa0},
{0x3667, 0xa6},
{0x3674, 0x01},
{0x373d, 0x24},
{0x3741, 0x28},
{0x3743, 0x28},
{0x3745, 0x28},
{0x3747, 0x28},
{0x3748, 0x00},
{0x3749, 0x78},
{0x374a, 0x00},
{0x374b, 0x78},
{0x374c, 0x00},
{0x374d, 0x78},
{0x374e, 0x00},
{0x374f, 0x78},
{0x3766, 0x12},
{0x37e0, 0x00},
{0x37e6, 0x04},
{0x37e5, 0x04},
{0x37e1, 0x04},
{0x3737, 0x04},
{0x37d0, 0x0a},
{0x37d8, 0x04},
{0x37e2, 0x08},
{0x3739, 0x10},
{0x37e4, 0x18},
{0x37e3, 0x04},
{0x37d9, 0x10},
{0x4040, 0x04},
{0x4041, 0x0f},
{0x4008, 0x00},
{0x4009, 0x0d},
{0x37a1, 0x14},
{0x37a8, 0x16},
{0x37ab, 0x10},
{0x37c2, 0x04},
{0x3705, 0x00},
{0x3706, 0x28},
{0x370a, 0x00},
{0x370b, 0x78},
{0x3714, 0x24},
{0x371a, 0x1e},
{0x372a, 0x03},
{0x3756, 0x00},
{0x3757, 0x0e},
{0x377b, 0x00},
{0x377c, 0x0c},
{0x377d, 0x20},
{0x3790, 0x28},
{0x3791, 0x78},
{0x3800, 0x00},
{0x3801, 0x00},
{0x3802, 0x00},
{0x3803, 0x04},
{0x3804, 0x07},
{0x3805, 0x8f},
{0x3806, 0x04},
{0x3807, 0x43},
{0x3808, 0x07},
{0x3809, 0x80},
{0x380a, 0x04},
{0x380b, 0x38},
{0x380c, 0x02},
{0x380d, 0x78},
{0x380e, 0x04},
{0x380f, 0xa0},
{0x3811, 0x06},
{0x3813, 0x04},
{0x3814, 0x01},
{0x3815, 0x01},
{0x3816, 0x01},
{0x3817, 0x01},
{0x381d, 0x40},
{0x381e, 0x02},
{0x3820, 0x88},
{0x3821, 0x00},
{0x3822, 0x04},
{0x3835, 0x00},
{0x4303, 0x19},
{0x4304, 0x19},
{0x4305, 0x03},
{0x4306, 0x81},
{0x4503, 0x00},
{0x4508, 0x14},
{0x450a, 0x00},
{0x450b, 0x40},
{0x4833, 0x08},
{0x5000, 0xa9},
{0x5001, 0x09},
{0x3b00, 0x00},
{0x3b02, 0x00},
{0x3b03, 0x00},
{0x3c80, 0x08},
{0x3c82, 0x00},
{0x3c83, 0xb1},
{0x3c87, 0x08},
{0x3c8c, 0x10},
{0x3c8d, 0x00},
{0x3c90, 0x00},
{0x3c91, 0x00},
{0x3c92, 0x00},
{0x3c93, 0x00},
{0x3c94, 0x00},
{0x3c95, 0x00},
{0x3c96, 0x00},
{0x3c97, 0x00},
{0x3c98, 0x00},
{0x4000, 0xf3},
{0x4001, 0x60},
{0x4002, 0x00},
{0x4003, 0x40},
{0x4090, 0x14},
{0x4601, 0x10},
{0x4701, 0x00},
{0x4708, 0x09},
{0x470a, 0x00},
{0x470b, 0x40},
{0x470c, 0x81},
{0x480c, 0x12},
{0x4710, 0x06},
{0x4711, 0x00},
{0x4837, 0x12},
{0x4800, 0x00},
{0x4c01, 0x00},
{0x5036, 0x00},
{0x5037, 0x00},
{0x580b, 0x0f},
{0x4903, 0x80},

//ping
{0x4003, 0x40},
{0x5000, 0x81},
{0x5200, 0x18},
#ifdef CONFIG_FAST_BOOT_EN
////60fps
//{0x0307, 0x00},
//{0x4837, 0x16},
//{0x380c, 0x02},
//{0x380d, 0x78}, //< HTS
//40fps
{0x0307, 0x00},
{0x4837, 0x16},
{0x380c, 0x03},
{0x380d, 0xb4}, //< HTS
////30fps
//{0x0307, 0x01}, //0x01 half speed, 0x00 normal
//{0x4837, 0x2c}, //0x2c half speed, 0x16 normal
//{0x380c, 0x04}, //0x04 half speed, 0x02 normal
//{0x380d, 0xf0}, //0xf0 half speed, 0x78 normal  //< HTS is 2 multiple
#else
//24fps
{0x0305, 0x32},
{0x0307, 0x01},
{0x4837, 0x35},
#ifdef OV2736_2M_20FPS
//{0x380c, 0x09},
//{0x380d, 0xe0}, //< HTS
{0x380c, 0x07},
{0x380d, 0x68}, //< HTS
	// 768
#else
{0x380c, 0x06},
{0x380d, 0x2c}, //< HTS
#endif
#endif
{0x380e, 0x04},
	// 4
{0x380f, 0xa0},
	// a0
{0x3500, 0x00},
{0x3501, 0x49},
{0x3502, 0x80},
{0x3508, 0x02},
{0x3509, 0x80},
{0x3d8c, 0x11},
{0x3d8d, 0xf0},
{0x5180, 0x00},
{0x5181, 0x10},
{0x36a0, 0x16},
{0x36a1, 0x50},
{0x36a2, 0x60},
{0x36a3, 0x80},
{0x36a4, 0x00},
{0x36a5, 0xa0},
{0x36a6, 0x00},
{0x36a7, 0x50},
{0x36a8, 0x00},
{0x36a9, 0x50},
{0x36aa, 0x00},
{0x36ab, 0x50},
{0x36ac, 0x00},
{0x36ad, 0x50},
{0x36ae, 0x00},
{0x36af, 0x50},
{0x36b0, 0x00},
{0x36b1, 0x50},
{0x36b2, 0x00},
{0x36b3, 0x50},
{0x36b4, 0x00},
{0x36b5, 0x50},
{0x36b9, 0xee},
{0x36ba, 0xee},
{0x36bb, 0xee},
{0x36bc, 0xee},
{0x36bd, 0x0e},
{0x36b6, 0x08},
{0x36b7, 0x08},
{0x36b8, 0x10},

{0x0100, 0x01}, 
#endif
};

/*************************
  setting for different size and framerate
 *************************/
 SENSOR_SETTING_TABLE sensor__size_1920_1080[][2] = {
};

 SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_60[][2] = {
	{0x0307, 0x00},
	{0x4837, 0x16},
	{0x380c, 0x02},
	{0x380d, 0x78},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_40[][2] = {
	{0x0307, 0x00},
	{0x4837, 0x16},
	{0x380c, 0x03},
	{0x380d, 0xb4},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_30[][2] = {
	{0x0307, 0x01},
	{0x4837, 0x2c},
	{0x380c, 0x04},
	{0x380d, 0xf0},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_25[][2] = {
	{0x0307, 0x01},
	{0x4837, 0x2c},
	{0x380c, 0x05},
	{0x380d, 0xec},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_24[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x06},
	{0x380d, 0x2c},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_20[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x07},
	{0x380d, 0x68},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_15[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x09},
	{0x380d, 0xe0},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_12[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x0c},
	{0x380d, 0x58},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_10[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x0e},
	{0x380d, 0xd0},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_5[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x1d},
	{0x380d, 0xa0},
};

SENSOR_SETTING_TABLE sensor__size_1920_1080_framerate_3[][2] = {
	{0x0305, 0x32},
	{0x0307, 0x01},
	{0x4837, 0x35},
	{0x380c, 0x31},
	{0x380d, 0x60},
};

/*************************
  effect change IOCTL
 *************************/
static u8 last_changed = 0;
SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{	
	int val;

	switch(effect){
		case SNR_EFFECT_FRMRATE:	
			cfg->cur_frame_rate = level;
			SENSOR_SETTING_SUPPORT_START
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 60)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 40)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 30)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 25)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 24)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 20)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 15)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 12)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 10)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 5)
			SENSOR_SETTING_SUPPORT_SIZE_FRM(1920, 1080, 3)
			SENSOR_SETTING_SUPPORT_END			
			return 0;
#ifdef CONFIG_FAST_BOOT_EN
		case SNR_EFFECT_FASTBOOT:
			if(level == FAST_STAGE_FPS1DONE){
				syslog(LOG_INFO, "TODO: FPS1DONE\n");
			}
			if(level == FAST_STAGE_FPS2RDY){
				syslog(LOG_INFO, "TODO: FPS2RDY\n");
			}
			if(level == FAST_STAGE_FPS2START){
				syslog(LOG_INFO, "TODO: FPS2START\n");
			}
			return 0;
#endif
		case SNR_EFFECT_FLIP:
			{
				if(level){ //flip on, 
					val = libsccb_rd(cfg->sccb_cfg,0x3820);
					val |= 0x10;
					libsccb_wr(cfg->sccb_cfg,0x3820, val);
				}else{ //flip off
					val = libsccb_rd(cfg->sccb_cfg,0x3820);
					val &= 0xffffffef;
					libsccb_wr(cfg->sccb_cfg,0x3820, val);
				}
			}
			return 0;
		case SNR_EFFECT_MIRROR:
			{
				if(level){ //mirror on
					val = libsccb_rd(cfg->sccb_cfg,0x3820);
					val |= 0x8;
					libsccb_wr(cfg->sccb_cfg,0x3820, val);
				}else{ //mirror off
					val = libsccb_rd(cfg->sccb_cfg,0x3820);
					val &= 0xfffffff7;
					libsccb_wr(cfg->sccb_cfg,0x3820, val);
				}
			}
			return 0;
	   	default:
    	    		return -2;
    }
}


/*************************
  effect description table
  { EFFECT_NAME,need_proc, padding, min, max, default, current }
 *************************/
SENSOR_SETTING_FUNC t_sensoreffect_one tss[] = 
{
};

/*************************
  sensor detect func
 *************************/
#define OV2736_SENSOR_ID_ADR_HI 0x300b
#define OV2736_SENSOR_ID_ADR_LO 0x300c
#define OV2736_SENSOR_ID_VAL_HI 0x27
#define OV2736_SENSOR_ID_VAL_LO 0x30

SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
    int hi,lo;
    int retry = 2000;

    do{
        hi = libsccb_rd(cfg->sccb_cfg,OV2736_SENSOR_ID_ADR_HI);
        lo = (libsccb_rd(cfg->sccb_cfg,OV2736_SENSOR_ID_ADR_LO) & 0xf0);
    }while((retry--) >= 0 && ((hi!=OV2736_SENSOR_ID_VAL_HI) || (lo!=OV2736_SENSOR_ID_VAL_LO)));
    
	syslog(LOG_INFO,  "sensor: %x %x\n",hi,lo);
	cfg->cur_video_size =(1920 << 16) | 1080;

	if(cfg->cur_frame_rate == 0)
	{
#ifdef OV2736_2M_20FPS
		cfg->cur_frame_rate = 20;	// 20
#endif
	}

	if( (hi == OV2736_SENSOR_ID_VAL_HI) && (lo == (OV2736_SENSOR_ID_VAL_LO&0xf0)) )
    {
        //syslog(LOG_INFO,  "sensor_2736_isp_init()\n");
        return 0;
    }
    else
    {
        return -1;
    }
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	WriteReg32(0xc0038c00, 0x0000001f); //[4]: rgbir_isp_en
			                //; [3]: ir_extract_en
				            //; [2]: bIRRemovalEnable
                            //; [1]: bDNSEnable
                            //; [0]: bDPCEnable
	WriteReg32(0xc0038c04, 0x00000000);
						               // [20] dummyatio_man_en | [27:24] dummy_line_num
	WriteReg32(0xc0038c10, 0x00000800);//[15:0] real_gain
	WriteReg32(0xc0038c14, 0x3bddb3dd);//CFAArray
	WriteReg32(0xc0038c18, 0x00040010);	// [18] compensate_en | [17:16] bayer_pattern | [15:0] blc
	WriteReg32(0xc0038c1c, 0x00800100);//[8:0] manual_gain | [31:16] remove_eof_cnt
	WriteReg32(0xc0038c20, 0x8079717a);	// [31:24] remove_ir | [23:16] coef_b | [15:8] coef_g | [7:0] coef_r
	WriteReg32(0xc0038c24, 0x01ff0080);//[24:16] max_gain | [8:0] min_gain
	WriteReg32(0xc0038c28, 0x000a03e8);//[20:16] remove_step | [15:0] remove_thre
	WriteReg32(0xc0038c2c, 0x0006040c);	// [19:16] remove_sat_range | [15:8]:remove_range1 | [7:0]:remove_range2
	///CIP                 
	WriteReg32(0xc0038e00, 0x00000002);//Configured by Hardware
	WriteReg32(0xc0038e04, 0x00000000);//Noise list 0|1|2|3
	WriteReg32(0xc0038e08, 0x00000000);//Noise list 4|5
	///RGBIR UVDns         
	WriteReg32(0xc0038f00, 0x00000000);//Configured by Hardware
	WriteReg32(0xc0038f04, 0x05040302);	// Noise Y list 0|1|2|3 8Bits
	WriteReg32(0xc0038f08, 0x00000706);	// Noise Y list 4|5 8Bits
	WriteReg32(0xc0038f0c, 0x07050301);	// Noise UV list 0|1|2|3 8Bits
	WriteReg32(0xc0038f10, 0x00000908);	// Noise UV list 4|5 8Bits

	WriteReg32(0xc0038f14, 0x00ba9876);	// Add back Y list 0|1|2|3|4|5 4Bits
	WriteReg32(0xc0038f18, 0x00000082);	// Gain threshold 0|1 4Bits
	WriteReg32(0xc0038f1c, 0x00002010);	// V Scale 0|1 6Bits

	return 0;	
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{

// ================ isp top ================
	//WriteReg32(0xe0088000, 0xaf3f8d00);	// pre pipe top
    syslog(LOG_ERR,"isp setting version:v1.0.1--for 2736\n ");
	WriteReg32(0xe0088000, 0x887f0100);	// pre pipe top
                    	// 00-[7]  RAW_DNS_En      
                    	// 00-[6]  BinC_En         
                    	// 00-[5]  DPC_En          
                    	// 00-[4]  DPCOTP_En       
                    	// 00-[3]  AWBG_En         
                    	// 00-[2]  Lens_Online_En  
                    	// 00-[1]  LENC_En         
                    	// 00-[0]  Pre_Pipe_Manu_En
                    	// 01-[7] Binc_New_En     
                    	// 01-[6] DNS_3D_En       
                    	// 01-[5] Stretch_En      
                    	// 01-[4] Hist_Stats_En   
                    	// 01-[3] CCM_En          
                    	// 01-[2] AEC_AGC_En      
                    	// 01-[1] Curve_AWB_En    
                    	// 01-[0] CIP_En
                    	// 02-[7:4] reserved       
                    	// 02-[3] dpc_black_en     
                    	// 02-[2] dpc_white_en     
                    	// 02-[1] pdf_en           
                    	// 02-[0] ca_en            
                    	// 03-[7] Manual_Ctrl_En   -           
                    	// 03-[6] Manual_Size_En   
                    	// 03-[5] Lenc_mirror      
                    	// 03-[4] Lenc_flip        
                    	// 03-[3:2] reserved       
                    	// 03-[1] Binc_Mirror      
                    	// 03-[0] Binc_Flip     

	WriteReg32(0xe0088004, 0x00400001);	//[27:16]blc,[9:8]input pattern BG/GR

	WriteReg32(0xe0089800, 0x3c000000);	// post pipe top
                    	// 00[0]  Manual_Ctrl_En    
                    	// 00[1]  Post_Pipe_Manu_En 
                    	// 00[2]  RGB_Gamma_En   
                    	// 00[3]  JPEG_Ycbcr_En  
                    	// 00[4]  SDE_En         
                    	// 00[5]  UV_DNS_En      
                    	// 00[6]  Anti_Shake_En  
                    	// 00[7]  Reserved    

	WriteReg32(0xe0089200, 0x11010000);	// [31:24]latch enable, [23:16]01-sensor1,10-sensor2,11-two sensor EDR mode enable

// ================ DMA ================ 

// input size(fw)
	WriteReg32(L_BASE_ADDRESS + 0x0000, 0x04380780);	// w_in|h_in
	
// ================ ISP function ================
//the following settings from Rust@0928
#define RGBIR_SETTING
#define AEC_AGC_SETTING
#define CURVE_AWB_SETTING
#define CCM_SETTING
#define STRETCH_SETTING
#define LENC_SETTING
#define GAMMA_SETTING
#define RAW_DNS_SETTING
#define SDE_SETTING
#define CA_SETTING
#define UVDNS_SETTING
#define CIP_SETTING
#define TDDNS_SETTING
#define EDR_SETTING

#ifdef RGBIR_SETTING
	WriteReg32(0xc0038c00, 0x0000001f); //[4]: rgbir_isp_en
			                //; [3]: ir_extract_en
				            //; [2]: bIRRemovalEnable
                            //; [1]: bDNSEnable
                            //; [0]: bDPCEnable
	WriteReg32(0xc0038c04, 0x00000000);
						               // [20] dummyatio_man_en | [27:24] dummy_line_num
	WriteReg32(0xc0038c10, 0x00000800);//[15:0] real_gain
	WriteReg32(0xc0038c14, 0x3bddb3dd);//CFAArray
	WriteReg32(0xc0038c18, 0x00040010);	// [18] compensate_en | [17:16] bayer_pattern | [15:0] blc
	WriteReg32(0xc0038c1c, 0x00800100);//[8:0] manual_gain | [31:16] remove_eof_cnt
	WriteReg32(0xc0038c20, 0x8079717a);	// [31:24] remove_ir | [23:16] coef_b | [15:8] coef_g | [7:0] coef_r
	WriteReg32(0xc0038c24, 0x01ff0080);//[24:16] max_gain | [8:0] min_gain
	WriteReg32(0xc0038c28, 0x000a03e8);//[20:16] remove_step | [15:0] remove_thre
	WriteReg32(0xc0038c2c, 0x0006040c);	// [19:16] remove_sat_range | [15:8]:remove_range1 | [7:0]:remove_range2
	///CIP                 
	WriteReg32(0xc0038e00, 0x00000002);//Configured by Hardware
	WriteReg32(0xc0038e04, 0x00000000);//Noise list 0|1|2|3
	WriteReg32(0xc0038e08, 0x00000000);//Noise list 4|5
	///RGBIR UVDns         
	WriteReg32(0xc0038f00, 0x00000000);//Configured by Hardware
	WriteReg32(0xc0038f04, 0x05040302);	// Noise Y list 0|1|2|3 8Bits
	WriteReg32(0xc0038f08, 0x00000706);	// Noise Y list 4|5 8Bits
	WriteReg32(0xc0038f0c, 0x07050301);	// Noise UV list 0|1|2|3 8Bits
	WriteReg32(0xc0038f10, 0x00000908);	// Noise UV list 4|5 8Bits

	WriteReg32(0xc0038f14, 0x00ba9876);	// Add back Y list 0|1|2|3|4|5 4Bits
	WriteReg32(0xc0038f18, 0x00000082);	// Gain threshold 0|1 4Bits
	WriteReg32(0xc0038f1c, 0x00002010);	// V Scale 0|1 6Bits
#endif

#ifdef AEC_AGC_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0020, 0x02000608);	// AECAGC enable 

// meanY stat(HW)
	WriteReg32(0xe0088b18, 0x00000000);	// stat win left|right, bw=12
	WriteReg32(0xe0088b1c, 0x00000010);	// stat win top|bottom, bw=12
	WriteReg32(0xe0088b08, 0x08087070);	// left|top|width|height, ratio, bw=7 
	WriteReg32(0xe0088b28, 0x01010101);	// weight0|weight1|weight2|weight3, bw=5
	WriteReg32(0xe0088b2c, 0x02020202);	// weight4|weight5|weight6|weight7, bw=5
	WriteReg32(0xe0088b30, 0x03020202);	// weight8|weight9|weight10|weight11, bw=5
	WriteReg32(0xe0088b34, 0x00000205);	// [12:8]weight12,[4:1]stat sampling,[0]stat brightest channel enable
// hist stat(HW)
	WriteReg32(0xe0088c04, 0x00000000);	// left|right, bw=13
	WriteReg32(0xe0088c08, 0x00000011);	// top|bottom, bw=13
	WriteReg32(L_BASE_ADDRESS + 0x0038, 0x04010100);	// nSaturationPer2 | nSaturationPer1 | nApplyAnaGainMode | nReadHistMeanSelect
	WriteReg32(L_BASE_ADDRESS + 0x003c, 0x040b00fd);	// nMaxFractalExp | nMaxFractalExp | bAllowFractionalExp | nSaturateRefBin
//; Black and saturate setting
	WriteReg32(0xe0088b0c, 0x10100000);	// low threshold and high threshold
	WriteReg32(0xe0088b04, 0x30303030);	// black weight and saturate weight
	WriteReg32(0xe0088b10, 0x00040006);	// black percentage threshold
	WriteReg32(0xe0088b14, 0x00040004);	// saturate percentage threshold
// AEC para(FW)
	WriteReg32(L_BASE_ADDRESS + 0x0010, 0x08020101);	// [15:8]sensor gain mode
	WriteReg32(L_BASE_ADDRESS + 0x0014, 0x00105028);	// [31:24]target low,[23:16]target high
	WriteReg32(L_BASE_ADDRESS + 0x0018, 0x060f0804);    // [31:16]pStableRange[2] | FastStep | SlowStep
	WriteReg32(L_BASE_ADDRESS + 0x0024, 0x001002fd);	// 01f80010; max gain|min gain
	WriteReg32(L_BASE_ADDRESS + 0x0028, 0x00000000|(VTS-0x10));	// max exposure
	WriteReg32(L_BASE_ADDRESS + 0x002c, 0x00000010);	// min exposure
    WriteReg32(L_BASE_ADDRESS + 0x0054, 0x086c0000|VTS);	// [31:16]VTS,[15:8]device ID,[7:0]I2C option
	WriteReg32(L_BASE_ADDRESS + 0x0058, 0x35013500);	// exp_h|exp_m
	WriteReg32(L_BASE_ADDRESS + 0x005c, 0x00003502);	// exp_l|SensorAECAddr[3]
	WriteReg32(L_BASE_ADDRESS + 0x0060, 0x380f380e);	// vts
	WriteReg32(L_BASE_ADDRESS + 0x0064, 0x35093508);	// gain
	WriteReg32(L_BASE_ADDRESS + 0x0068, 0x00000000);	// 55025503; SensorAECAddr[8]~[9]
	WriteReg32(L_BASE_ADDRESS + 0x006c, 0x00000000);	// SensorAECAddr[10]~[11]	
	WriteReg32(L_BASE_ADDRESS + 0x0070, 0x00ffffff);	// SensorAECMask[0]~[3]
	WriteReg32(L_BASE_ADDRESS + 0x0074, 0xffffffff);	// SensorAECMask[4]~[7]
	WriteReg32(L_BASE_ADDRESS + 0x0078, 0x00000000);	// ffff0000; SensorAECMask[8]~[11]
	WriteReg32(L_BASE_ADDRESS + 0x007c, 0x030000ff);	// MaxAnaGain | MaxDigiGain
	WriteReg32(L_BASE_ADDRESS + 0x0080, 0x00000101);	//

// banding
	WriteReg32(L_BASE_ADDRESS + 0x0040, 0x01010207);	// nGainFractalBit|BandingMode|BandingEnable|LessThan1BandEnable
#ifdef ENLARGE_HTS_60BASED
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x11c20ecc);	// 60band|50band,fr=15
#endif
#ifdef ENLARGE_VTS_60BASED
	WriteReg32(L_BASE_ADDRESS + 0x0044, 0x11c20ecc);	// 60band|50band,fr=15
#endif
#endif

#ifdef CURVE_AWB_SETTING
// AWB stat
	WriteReg32(0xe0088a00, 0x00000000);	// [0]GW
	WriteReg32(0xe0088a0c, 0x000e03e0);	// 000e03e0; min_stat_val|max_stat_val,bw=10 (?)
	WriteReg32(0xe0088a10, 0x00000001);	// step=1(downsample=2)
// AWB calc
	WriteReg32(L_BASE_ADDRESS + 0x008c, 0x02000300);	// maxBgain|maxGBgain
	WriteReg32(L_BASE_ADDRESS + 0x0090, 0x02000200);	// maxGRgain|maxRgain
	WriteReg32(L_BASE_ADDRESS + 0x0190, 0x10050000);	// AWBOptions|AWBShiftEnable|MapSwitchYth|DarkCountTh
// XY settings
	WriteReg32(L_BASE_ADDRESS + 0x01a4, 0x000c001e);	//
	WriteReg32(L_BASE_ADDRESS + 0x01a8, 0xfce7fc9d);	//
// AWB map
// low map
	WriteReg32(L_BASE_ADDRESS + 0x00d0,0x01111222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d4,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00d8,0x11122221);	//
	WriteReg32(L_BASE_ADDRESS + 0x00dc,0x00000001);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e0,0x22222210);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e4,0x00000011);	//
	WriteReg32(L_BASE_ADDRESS + 0x00e8,0x22222100);	//
	WriteReg32(L_BASE_ADDRESS + 0x00ec,0x00000022);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f0,0x22221000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f4,0x00000222);	//
	WriteReg32(L_BASE_ADDRESS + 0x00f8,0x44440000);	//
	WriteReg32(L_BASE_ADDRESS + 0x00fc,0x00000444);	//
	WriteReg32(L_BASE_ADDRESS + 0x0100,0x66660000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0104,0x00006666);	//
	WriteReg32(L_BASE_ADDRESS + 0x0108,0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x010c,0x00068888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0110,0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0114,0x00068888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0118,0x88886000);	//
	WriteReg32(L_BASE_ADDRESS + 0x011c,0x00068888);	//
	WriteReg32(L_BASE_ADDRESS + 0x0120,0xfffdc000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0124,0x000dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0128,0xffdc0000);	//
	WriteReg32(L_BASE_ADDRESS + 0x012c,0x000dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0130,0xfdd00000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0134,0x000dffff);	//
	WriteReg32(L_BASE_ADDRESS + 0x0138,0xed000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x013c,0x000eeeee);	//
	WriteReg32(L_BASE_ADDRESS + 0x0140,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0144,0x0000dddd);	//
	WriteReg32(L_BASE_ADDRESS + 0x0148,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x014c,0x00000000);	//
// middle map
	WriteReg32(L_BASE_ADDRESS + 0x0150,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0154,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0158,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x015c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0160,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0164,0x1fc01fe0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0168,0x1f801f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x016c,0x00000f00);	//
// high map
	WriteReg32(L_BASE_ADDRESS + 0x0170,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0174,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0178,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x017c,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0180,0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0184,0x0f800f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0188,0x00000f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x018c,0x00000000);	//

	//;; AWB ROIs
	WriteReg32(0xe0088a14,0x00000000);//
	WriteReg32(0xe0088a18,0x00000000);// W | H
	WriteReg32(0xe0088a1c,0x00000000);// 
	WriteReg32(0xe0088a20,0x00000000);// W | H 
	WriteReg32(0xe0088a24,0x00000000);// 
	WriteReg32(0xe0088a28,0x00000000);// W | H
	WriteReg32(0xe0088a2c,0x00000000);//
	WriteReg32(0xe0088a30,0x00000000);// W | H
	//;; ROI weight
	WriteReg32(0x100321d8,0x00000000);// 04000000;
	WriteReg32(0x100321dc,0x00000000);// 00006018; Weight 3,4,5
	WriteReg32(0x100321e0,0x00000000);//
	WriteReg32(0x100321e4,0x00000000);//
	WriteReg32(0x100321e8,0x08080000);//
	WriteReg32(0x100321ec,0x08080808);//
	//;; ROI threshold
	WriteReg32(0x100321cc,0x00640032);// Thresholds
	WriteReg32(0x100321d0,0x012c00c8);//
	WriteReg32(0x100321d4,0x138802bc);//
	WriteReg32(0x100321f0,0x01010080);// Offset and ROIOption
#endif

#ifdef CCM_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x03f4, 0x00808000);
	WriteReg32(L_BASE_ADDRESS + 0x03bc, 0xff1b01dc);	// ff2001d5;
	WriteReg32(L_BASE_ADDRESS + 0x03c0, 0xfff10009);	// ffd8000b;
	WriteReg32(L_BASE_ADDRESS + 0x03c4, 0xff6c01a3);	// ff8101a7;
	WriteReg32(L_BASE_ADDRESS + 0x03c8, 0xff4b0003);	// ff74fff4;
	WriteReg32(L_BASE_ADDRESS + 0x03cc, 0x01ec01b2);	// 020a0198; center(c)
	WriteReg32(L_BASE_ADDRESS + 0x03d0, 0x005cfeb8);	// 0048feae;
	WriteReg32(L_BASE_ADDRESS + 0x03d4, 0x0190ffe4);	// 018fffd9;
	WriteReg32(L_BASE_ADDRESS + 0x03d8, 0xfff4ff8c);	// ffdcff98;
	WriteReg32(L_BASE_ADDRESS + 0x03dc, 0x0129ffe3);	// 012cfff8; left(a)
	WriteReg32(L_BASE_ADDRESS + 0x03e0, 0xff4301c5);	// ff4e01b3;
	WriteReg32(L_BASE_ADDRESS + 0x03e4, 0xff8ffff8);	// ff9cffff;
	WriteReg32(L_BASE_ADDRESS + 0x03e8, 0xff9901d8);	// ffba01aa;
	WriteReg32(L_BASE_ADDRESS + 0x03ec, 0xff8bffe3);	// ffb2fff3;
	WriteReg32(L_BASE_ADDRESS + 0x03f0, 0x01000192);	// 0100015b; right(d), [15:0] maunual CT

	WriteReg32(L_BASE_ADDRESS + 0x03b0, 0x00800078);	// thre[0] | thre[1]
	WriteReg32(L_BASE_ADDRESS + 0x03b4, 0x00c800b0);	// thre[2] | thre[3]
	WriteReg32(L_BASE_ADDRESS + 0x03b8, 0x01a00108);	// thre[4] | thre[5]
	WriteReg32(L_BASE_ADDRESS + 0x03ac, 0x80808001);	// [31:24] auto CT enable

	WriteReg32(L_BASE_ADDRESS + 0x0490, 0x01ab0001);	// 
	WriteReg32(L_BASE_ADDRESS + 0x0494, 0x008efec7);	//
	WriteReg32(L_BASE_ADDRESS + 0x0498, 0x0158fffe);	//
	WriteReg32(L_BASE_ADDRESS + 0x049c, 0xfff7ffaa);	//
	WriteReg32(L_BASE_ADDRESS + 0x04a0, 0x00f00019);	// last(h)
#endif


#ifdef STRETCH_SETTING
	WriteReg32(0xe0088d80, 0x018000f3);	// [29:16]max low level, [13:0] min low level
	WriteReg32(0xe0088d84, 0x01003fff);	// [29:16]manual low level, [13:0] min high level
#endif

#ifdef LENC_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0388, 0x00c800b0);	// thre[0],thre[1],16bit
	WriteReg32(L_BASE_ADDRESS + 0x038c, 0x01100100);	// thre[2],thre[3],16bit
	WriteReg32(0xe0088108, 0x07770d45); // h_scale 0x380000/width |v_scale 0x380000/height for 1920*1080
// A light shading
	WriteReg32(L_BASE_ADDRESS + 0x0700, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0704, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0708, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x070c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0710, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0714, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0718, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x071c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0720, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0724, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0728, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x072c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0730, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0734, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0738, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x073c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0740, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0744, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0748, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x074c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0750, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0754, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0758, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x075c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0760, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0764, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0768, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x076c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0770, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0774, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0778, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x077c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0780, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0784, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0788, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x078c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0790, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0794, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0798, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x079c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07a8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ac, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07b8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07bc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07c8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07cc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07d8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07dc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07e8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07ec, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07f8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x07fc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0800, 0x86838682);	//
	WriteReg32(L_BASE_ADDRESS + 0x0804, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0808, 0x85848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x080c, 0x80858384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0810, 0x8283827c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0814, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0818, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x081c, 0x89818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0820, 0x83817f83);	//
	WriteReg32(L_BASE_ADDRESS + 0x0824, 0x82828382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0828, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x082c, 0x87818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0830, 0x81808085);	//
	WriteReg32(L_BASE_ADDRESS + 0x0834, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0838, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x083c, 0x88808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0840, 0x81808184);	//
	WriteReg32(L_BASE_ADDRESS + 0x0844, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0848, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x084c, 0x89808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0850, 0x80818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0854, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0858, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x085c, 0x88808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0860, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0864, 0x82828280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0868, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x086c, 0x88808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0870, 0x81828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0874, 0x82828080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0878, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x087c, 0x87808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0880, 0x82818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0884, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0888, 0x82818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x088c, 0x86818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0890, 0x81818283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0894, 0x81808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0898, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x089c, 0x87808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a0, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a4, 0x81808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x08a8, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ac, 0x87808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b0, 0x81818183);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b4, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08b8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08bc, 0x86808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c0, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c4, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08c8, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08cc, 0x877f8181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d0, 0x80808184);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d4, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08d8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08dc, 0x887f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e0, 0x7f818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e4, 0x7f7f7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x08e8, 0x807f7f7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x08ec, 0x867f8181);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f0, 0x817f7f87);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f4, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x08f8, 0x81807e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x08fc, 0x887e7e7f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0900, 0x86868685);	//
	WriteReg32(L_BASE_ADDRESS + 0x0904, 0x85848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0908, 0x85848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x090c, 0x85858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0910, 0x84858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x0914, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0918, 0x84848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x091c, 0x83848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0920, 0x83848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x0924, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0928, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x092c, 0x84838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0930, 0x82828384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0934, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0938, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x093c, 0x82838282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0940, 0x82828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0944, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0948, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x094c, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0950, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0954, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0958, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x095c, 0x80828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0960, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0964, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0968, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x096c, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0970, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0974, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0978, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x097c, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0980, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0984, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0988, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x098c, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0990, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0994, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0998, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x099c, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a0, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a4, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x09a8, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ac, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b0, 0x82828383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b4, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x09b8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x09bc, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c0, 0x83838484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x09c8, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x09cc, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d0, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d4, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09d8, 0x83838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x09dc, 0x84848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e0, 0x85858686);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e4, 0x84848485);	//
	WriteReg32(L_BASE_ADDRESS + 0x09e8, 0x85848484);	//
	WriteReg32(L_BASE_ADDRESS + 0x09ec, 0x84858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f0, 0x85868687);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f4, 0x85858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x09f8, 0x85858585);	//
	WriteReg32(L_BASE_ADDRESS + 0x09fc, 0x87848685);	//
// C light shading
	WriteReg32(L_BASE_ADDRESS + 0x0a00, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a04, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a08, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a0c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a10, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a14, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a18, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a1c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a20, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a24, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a28, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a2c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a30, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a34, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a38, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a3c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a40, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a44, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a48, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a4c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a50, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a54, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a58, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a5c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a60, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a64, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a68, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a6c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a70, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a74, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a78, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a7c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a80, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a84, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a88, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a8c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a90, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a94, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a98, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0a9c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aa8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aac, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ab8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0abc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ac8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0acc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ad8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0adc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ae8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0aec, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0af8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0afc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b00, 0x8782877d);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b04, 0x84848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b08, 0x85838485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b0c, 0x88838483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b10, 0x8383817c);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b14, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b18, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b1c, 0x86808283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b20, 0x83817f83);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b24, 0x83818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b28, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b2c, 0x86818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b30, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b34, 0x82828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b38, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b3c, 0x857f8182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b40, 0x81818184);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b44, 0x83818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b48, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b4c, 0x86808282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b50, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b54, 0x82828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b58, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b5c, 0x877f8182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b60, 0x81828183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b64, 0x82828081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b68, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b6c, 0x87808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b70, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b74, 0x82828080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b78, 0x82808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b7c, 0x86808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b80, 0x82818183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b84, 0x82818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b88, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b8c, 0x85808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b90, 0x81818280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b94, 0x82808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b98, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0b9c, 0x867f8281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba0, 0x82818083);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba4, 0x81818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ba8, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bac, 0x857f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb0, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb4, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bb8, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bbc, 0x857f8181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc0, 0x81818082);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc4, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bc8, 0x81808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bcc, 0x847f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd0, 0x80818083);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd4, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bd8, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bdc, 0x867e8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be0, 0x8080807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be4, 0x81807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0be8, 0x807f7f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bec, 0x837f8081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf0, 0x7f817f85);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf4, 0x807f7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bf8, 0x807e7e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0bfc, 0x897c827f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c00, 0x83808583);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c04, 0x82828280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c08, 0x84818283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c0c, 0x82828381);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c10, 0x81838183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c14, 0x81828383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c18, 0x81838281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c1c, 0x80828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c20, 0x82818382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c24, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c28, 0x82828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c2c, 0x83818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c30, 0x80818183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c34, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c38, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c3c, 0x82818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c40, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c44, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c48, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c4c, 0x7f818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c50, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c54, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c58, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c5c, 0x80818081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c60, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c64, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c68, 0x81818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c6c, 0x8180817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c70, 0x80808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c74, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c78, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c7c, 0x7d818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c80, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c84, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c88, 0x8081817f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c8c, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c90, 0x80808280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c94, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c98, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0c9c, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca0, 0x81808281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca4, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ca8, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cac, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb0, 0x80818183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb4, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cb8, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cbc, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc0, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc4, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cc8, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ccc, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd0, 0x81828285);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd4, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cd8, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cdc, 0x83818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce0, 0x82828381);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ce8, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cec, 0x80838283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf0, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf4, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cf8, 0x84828280);	//
	WriteReg32(L_BASE_ADDRESS + 0x0cfc, 0x847f847e);	//
// D light shading
	WriteReg32(L_BASE_ADDRESS + 0x0d00, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d04, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d08, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d0c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d10, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d14, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d18, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d1c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d20, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d24, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d28, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d2c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d30, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d34, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d38, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d3c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d40, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d44, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d48, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d4c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d50, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d54, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d58, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d5c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d60, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d64, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d68, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d6c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d70, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d74, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d78, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d7c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d80, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d84, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d88, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d8c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d90, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d94, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d98, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0d9c, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0da8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dac, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0db8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dbc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dc8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dcc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dd8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ddc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0de8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dec, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df0, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df4, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0df8, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0dfc, 0x00000000);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e00, 0x84838580);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e04, 0x84848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e08, 0x83848483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e0c, 0x86838483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e10, 0x8282827f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e14, 0x81828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e18, 0x82818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e1c, 0x85818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e20, 0x81818083);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e24, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e28, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e2c, 0x86808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e30, 0x81808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e34, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e38, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e3c, 0x85808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e40, 0x80808283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e44, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e48, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e4c, 0x86818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e50, 0x80818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e54, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e58, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e5c, 0x85808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e60, 0x80818283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e64, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e68, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e6c, 0x85808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e70, 0x81818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e74, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e78, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e7c, 0x85808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e80, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e84, 0x81808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e88, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e8c, 0x85808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e90, 0x81818183);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e94, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e98, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0e9c, 0x85808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea0, 0x81818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea4, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ea8, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eac, 0x84808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb0, 0x80818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb4, 0x80808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eb8, 0x80818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ebc, 0x85808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec0, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ec8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ecc, 0x84808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed0, 0x80818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ed8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0edc, 0x857f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee0, 0x80808182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ee8, 0x8080807f);	//
	WriteReg32(L_BASE_ADDRESS + 0x0eec, 0x847f8080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef0, 0x7f808084);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef4, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ef8, 0x7f807e80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0efc, 0x84808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f00, 0x83848582);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f04, 0x84828485);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f08, 0x84838483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f0c, 0x83838483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f10, 0x84838384);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f14, 0x83848383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f18, 0x83838283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f1c, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f20, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f24, 0x82828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f28, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f2c, 0x82828382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f30, 0x82828283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f34, 0x82818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f38, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f3c, 0x81828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f40, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f44, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f48, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f4c, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f50, 0x81818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f54, 0x80818080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f58, 0x81808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f5c, 0x7f808081);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f60, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f64, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f68, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f6c, 0x81808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f70, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f74, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f78, 0x80807f80);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f7c, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f80, 0x80808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f84, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f88, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f8c, 0x7f808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f90, 0x80818181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f94, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f98, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0f9c, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa0, 0x81818281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa4, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fa8, 0x80808080);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fac, 0x80808180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb0, 0x81828182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb4, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fb8, 0x81818180);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fbc, 0x81808181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc0, 0x82828284);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc4, 0x81828181);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fc8, 0x82818182);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fcc, 0x80828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd0, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd4, 0x82828282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fd8, 0x82828281);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fdc, 0x83828382);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe0, 0x84848385);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe4, 0x82838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fe8, 0x84838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0fec, 0x81838383);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff0, 0x81838483);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff4, 0x85818282);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ff8, 0x81818283);	//
	WriteReg32(L_BASE_ADDRESS + 0x0ffc, 0x83828482);	//
#endif

#ifdef GAMMA_SETTING
	WriteReg32(L_BASE_ADDRESS + 0x0200, 0x009E004B);
	WriteReg32(L_BASE_ADDRESS + 0x0204, 0x01740122);
	WriteReg32(L_BASE_ADDRESS + 0x0208, 0x01EE01B6);
	WriteReg32(L_BASE_ADDRESS + 0x020c, 0x024C0220);
	WriteReg32(L_BASE_ADDRESS + 0x0210, 0x02980274);
	WriteReg32(L_BASE_ADDRESS + 0x0214, 0x02D702B9);
	WriteReg32(L_BASE_ADDRESS + 0x0218, 0x030B02F2);
	WriteReg32(L_BASE_ADDRESS + 0x021c, 0x03370322);
	WriteReg32(L_BASE_ADDRESS + 0x0220, 0x035B034A);
	WriteReg32(L_BASE_ADDRESS + 0x0224, 0x0378036A);
	WriteReg32(L_BASE_ADDRESS + 0x0228, 0x03910385);
	WriteReg32(L_BASE_ADDRESS + 0x022c, 0x03A6039C);
	WriteReg32(L_BASE_ADDRESS + 0x0230, 0x03BA03B0);
	WriteReg32(L_BASE_ADDRESS + 0x0234, 0x03CE03C4);
	WriteReg32(L_BASE_ADDRESS + 0x0238, 0x03E103D7);
	WriteReg32(L_BASE_ADDRESS + 0x023c, 0x03F403EA);
	WriteReg32(L_BASE_ADDRESS + 0x0240, 0x004B03FF);
	WriteReg32(L_BASE_ADDRESS + 0x0244, 0x0122009E);
	WriteReg32(L_BASE_ADDRESS + 0x0248, 0x01B60174);
	WriteReg32(L_BASE_ADDRESS + 0x024c, 0x022001EE);
	WriteReg32(L_BASE_ADDRESS + 0x0250, 0x0274024C);
	WriteReg32(L_BASE_ADDRESS + 0x0254, 0x02B90298);
	WriteReg32(L_BASE_ADDRESS + 0x0258, 0x02F202D7);
	WriteReg32(L_BASE_ADDRESS + 0x025c, 0x0322030B);
	WriteReg32(L_BASE_ADDRESS + 0x0260, 0x034A0337);
	WriteReg32(L_BASE_ADDRESS + 0x0264, 0x036A035B);
	WriteReg32(L_BASE_ADDRESS + 0x0268, 0x03850378);
	WriteReg32(L_BASE_ADDRESS + 0x026c, 0x039C0391);
	WriteReg32(L_BASE_ADDRESS + 0x0270, 0x03B003A6);
	WriteReg32(L_BASE_ADDRESS + 0x0274, 0x03C403BA);
	WriteReg32(L_BASE_ADDRESS + 0x0278, 0x03D703CE);
	WriteReg32(L_BASE_ADDRESS + 0x027c, 0x03EA03E1);
	WriteReg32(L_BASE_ADDRESS + 0x0280, 0x03FF03F4);
#endif

#ifdef RAW_DNS_SETTING
	WriteReg32(0xe0088548,0x0f040010);//Y_offset

	WriteReg32(0xe0088500, 0x00060008);	// sigma[0]|sigma[1]
	WriteReg32(0xe0088504, 0x000a000c);	// sigma[2]|sigma[3]
	WriteReg32(0xe0088508, 0x0014002c);	// sigma[4]|sigma[5]
	WriteReg32(0xe008850c, 0x1212100c);	// Gns[0]|Gns[1]|Gns[2]|Gns[3]
	WriteReg32(0xe0088510, 0x0a061212);	// Gns[4]|Gns[5]|Rbns[0]|Rbns[1]
	WriteReg32(0xe0088514, 0x100c0a06);	// Rbns[2]|Rbns[3]|Rbns[4]|Rbns[5]
	WriteReg32(0xe0088518, 0x00000000);	// PS0[0]|PS0[1]|PS0[2]|PS0[3]
	WriteReg32(0xe008851c, 0x040d0508);	// PS0[4]|PS0[5]|PS1[0]|PS1[1]
	WriteReg32(0xe0088520, 0x0a0c1014);	// PS1[2]|PS1[3]|PS1[4]|PS1[5]
	WriteReg32(0xe0088524, 0x16161616);	// PS2[0]|PS2[1]|PS2[2]|PS2[3]
	WriteReg32(0xe0088528, 0x16163838);	// PS2[4]|PS2[5]|PS3[0]|PS3[1]
	WriteReg32(0xe008852c, 0x38383838);	// PS3[2]|PS3[3]|PS3[4]|PS3[5]
	WriteReg32(0xe0088530, 0x20202020);	// PL0[0]|PL0[1]|PL0[2]|PL0[3]
	WriteReg32(0xe0088534, 0x20201212);	// PL0[4]|PL0[5]|PL1[0]|PL1[1]
	WriteReg32(0xe0088538, 0x20202020);	// PL1[2]|PL1[3]|PL1[4]|PL1[5]
	WriteReg32(0xe008853c, 0x0a0c2020);	// PL2[0]|PL2[1]|PL2[2]|PL2[3]
	WriteReg32(0xe0088540, 0x2020080a);	// PL2[4]|PL2[5]|PL3[0]|PL3[1]
	WriteReg32(0xe0088544, 0x20202020);	// PL3[2]|PL3[3]|PL3[4]|PL3[5]
#endif

#ifdef CA_SETTING
	WriteReg32(0xe0088700,0x00010104);// Alpha[0]|Alpha[1]|Beta[0]|Beta[1]
	WriteReg32(0xe0088714,0x03010104);// Coef[0]|Coef[1]|Coef[2]|Coef[3]
#endif

#ifdef UVDNS_SETTING
	WriteReg32(0xe0089b00, 0x00020480);
	WriteReg32(0xe0089b04, 0x80010101);	// 1x|2x|4x
	WriteReg32(0xe0089b08, 0x01010101);	// 8x|16x|32x|64x
	WriteReg32(0xe0089b0c, 0x01020408);	// 128x|1x|2x|4x
	WriteReg32(0xe0089b10, 0x1014181c);	// 8x|16x|32x|64x	
	WriteReg32(0xe0089b14, 0x20000000);	// 128x
#endif

#ifdef SDE_SETTING
	//changed by Grady in 02/08/17 to solve unsmooth color issue
	WriteReg32(0xe0089a04, 0x00000400);   
	WriteReg32(0xe0089a20, 0x80806262);	//
	WriteReg32(0xe0089a24, 0x62c65f63);	//
	WriteReg32(0xe0089a28, 0x4e4e4eb2);	//
	WriteReg32(0xe0089a2c, 0x959d7676);	//
	WriteReg32(0xe0089a30, 0x76da121f);	//
	WriteReg32(0xe0089a34, 0x120c120c);	//
	WriteReg32(0xe0089a38, 0x88888800);	//
#endif

#ifdef CIP_SETTING
	WriteReg32(0xe0088800, 0x3f0462ff);	// cip
					// 00 - [7] 
					// 00 - [6] manual_mode
					// 00 - [5] bIfGbGr
					// 00 - [4] bIfCbCrDns
					// 00 - [3] bIfAntiClrAls
					// 00 - [2] bIfShapren
					// 00 - [1] bIfSharpenNS
					// 00 - [0] bIfDDns
					// 01 - [6:4] GDnsTSlope 
 					// 01 - [3:0] WSlopeClr
 					// 02 - [4:0] CombW
					// 03 - [7:0] WSlope
	WriteReg32(0xe0088804, 0x01040810);	// DirGDNST0|1|2|3
	WriteReg32(0xe0088808, 0x12140808);	// [31:16] DirGDNST4|5
					// 0a - [5:0] DirGDNSLevel0
					// 0b - [5:0] DirGDNSLevel1
	WriteReg32(0xe008880c, 0x08080b10);	// 0c - [5:0] DirGDNSLevel2
					// 0d - [5:0] DirGDNSLevel3
					// 0e - [5:0] DirGDNSLevel4
					// 0f - [5:0] DirGDNSLevel5
	WriteReg32(0xe0088810, 0x40404040);	// CleanSharpT0|1|2|3
	WriteReg32(0xe0088814, 0x4040ffff);	// [31:16] CleanSharpT4|5
				// [15:0] CleanSharpT0|1
	WriteReg32(0xe0088818, 0xffffffff);	// CleanSharpT2|3|4|5
	WriteReg32(0xe008881c, 0x10182028);	// Tintp0|1|2|3
	WriteReg32(0xe0088820, 0x38481018);	// Tintp4|5
				// TintpHV0|1
	WriteReg32(0xe0088824, 0x20283848);	// TintpHV0|1|2|3
	WriteReg32(0xe0088828, 0x20304050);	// NV0|1|2|3
	WriteReg32(0xe008882c, 0x78a00404);	// [31:16] NV4|5
					// 0e - [0:5] VNGLevel0
					// 0f - [0:5] VNGLevel1
	WriteReg32(0xe0088830, 0x04080808);	// 30 - [0:5] VNGLevel2
					// 31 - [0:5] VNGLevel3
					// 32 - [0:5] VNGLevel4
					// 33 - [0:5] VNGLevel5
	WriteReg32(0xe0088834, 0x04060810);	// 34 - [0:5] LPG0
					// 35 - [0:5] LPG1
					// 36 - [0:5] LPG2
					// 37 - [0:5] LPG3
	WriteReg32(0xe0088838, 0x10142020);	// 38 - [0:5] LPG4
					// 39 - [0:5] LPG5
					// 3a - [0:5] CbrDbsLevel0
					// 3b - [0:5] CbrDbsLevel1
	WriteReg32(0xe008883c, 0x20202020);	// 3c - [0:5] CbrDbsLevel2
					// 3d - [0:5] CbrDbsLevel3
					// 3e - [0:5] CbrDbsLevel4
					// 3f - [0:5] CbrDbsLevel5
	WriteReg32(0xe0088840, 0x00500078);	// [27:16] - TCbCrH0
					// [11:0] - TCbCrH1
	WriteReg32(0xe0088844, 0x00a00140);	// [27:16] - TCbCrH2
					// [11:0] - TCbCrH3
	WriteReg32(0xe0088848, 0x01680190);	// [27:16] - TCbCrH4
					// [11:0] - TCbCrH5
	WriteReg32(0xe008884c, 0x00c80078);	// [27:16] - TCbCrV0
					// [11:0] - TCbCrV1
	WriteReg32(0xe0088850, 0x00a00140);	// [27:16] - TCbCrV2
					// [11:0] - TCbCrV3
	WriteReg32(0xe0088854, 0x01680190);	// [27:16] - TCbCrV4
					// [11:0] - TCbCrV5
	WriteReg32(0xe0088858, 0x00123333);	// [23:20] - TCbrOffset0
					// [19:16] - TCbrOffset1
					// [15:12] - TCbrOffset2
					// [11:8] - TCbrOffset3
					// [7:4] - TCbrOffset4
					// [3:0] - TCbrOffset5
	WriteReg32(0xe008885c, 0x6455463c);	// CbCrAlsHf0|1|2|3
	WriteReg32(0xe0088860, 0x3732c83c);	// [31:16] CbCrAlsHf4|5|
					// [15:0] CbCrAlsMean0|1
	WriteReg32(0xe0088864, 0x50505050);	// CbCrAlsMean2|3|4|5
	WriteReg32(0xe0088868, 0x0c0b0a08);	// CbrImpulse0|1|2|3
	WriteReg32(0xe008886c, 0x00101204);	// [23:16] CbrImpulse4
					// [15:8] CbrImpulse5
					// [2:0] CbrImpulseThrShift
	WriteReg32(0xe0088870, 0x30302828);	// 70 - [5:0] SharpenLevel0
					// 71 - [5:0] SharpenLevel1
					// 72 - [5:0] SharpenLevel2
					// 73 - [5:0] SharpenLevel3
	WriteReg32(0xe0088874, 0x24240808);	// 74 - [5:0] SharpenLevel4
					// 75 - [5:0] SharpenLevel5
					// 76 - [7:0] SharpenThr0
					// 77 - [7:0] SharpenThr1
	WriteReg32(0xe0088878, 0x0a0c0f12);	// SharpenThr2|3|4|5
	WriteReg32(0xe008887c, 0x0002060a);	// 7d - [5:0] LocalRatio0
				// 7e - [5:0] LocalRatio1
				// 7f - [5:0] LocalRatio2
	WriteReg32(0xe0088880, 0x14141414);	// 80 - [5:0] SharpenHalo0
					// 81 - [5:0] SharpenHalo1
					// 82 - [5:0] SharpenHalo2
					// 83 - [5:0] SharpenHalo3
	WriteReg32(0xe0088884, 0x1414503a);	// 84 - [5:0] SharpenHalo4
					// 85 - [5:0] SharpenHalo5
					// 86 - [7:0] SharpenCoef0 (48)
					// 87 - [7:0] SharpenCoef1 (39)
	WriteReg32(0xe0088888, 0x17060104);	// [31:15] SharpenCoef2|3|4 (1b, 07, 01)
					// [7:0] DarkOffset
	WriteReg32(0xe008888c, 0x44444444);	// [31:28] SharpenHDelta0 (4)
					// [27:24] SharpenHDelta1 (4)
					// [23:20] SharpenHDelta2 (4)
					// [19:16] SharpenHDelta3 (4)
					// [15:12] SharpenHDelta4 (4)
					// [11:8] SharpenHDelta5 (4)
					// [7:4] SharpenHDelta6 (4)
					// [3:0] ShapenHDelta7 (4)
	WriteReg32(0xe0088890, 0x141b1f1e);	// 90 - [5:0] SharpenHGain0 (08)
				// 91 - [5:0] SharpenHGain1 (0c)
				// 92 - [5:0] SharpenHGain2 (0a)
				// 93 - [5:0] SharpenHGain3 (09)
	WriteReg32(0xe0088894, 0x19181a14);	// 94 - [5:0] SharpenHGain4 (08)
				// 95 - [5:0] SharpenHGain5 (0a)
				// 96 - [5:0] SharpenHGain6 (0a)
				// 97 - [5:0] SharpenHGain7 (0a)
	WriteReg32(0xe0088898, 0x0e000010);	// 9b - [5:0] SharpenHGain8 (06)
					// 99 - [5:0] localSymGThr0
					// 9a - [5:0] localSymGThr1
					// 9b - [5:0] localSymGThr2
	WriteReg32(0xe008889c, 0x00101820);	// 9c - reserved
					// 9d - [5:0] localDNSThr0
					// 9e - [5:0] localDNSThr1
					// 9f - [5:0] localDNSThr2
	WriteReg32(L_BASE_ADDRESS + 0x02c4, 0x025803ff);	// c4 - [1:0] CurList32[9:8]
				// c5 - [7:0] CurList32[7:0]
				// c6 - [2:0] HFreqGain0[10:8]
				// c7 - [7:0] HFreqGain0[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02c8, 0x01a40200);	// c8 - [2:0] HFreqGain1[10:8]
				// c9 - [7:0] HFreqGain1[7:0]
				// ca - [2:0] HFreqGain2[10:8]
				// cb - [7:0] HFreqGain2[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02cc, 0x01200158);	// cc - [2:0] HFreqGain3[10:8]
				// cd - [7:0] HFreqGain3[7:0]
				// ce - [2:0] HFreqGain4[10:8]
				// cf - [7:0] HFreqGain4[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d0, 0x00d000f8);	// d0 - [2:0] HFreqGain5[10:8]
				// d1 - [7:0] HFreqGain5[7:0]
				// d2 - [2:0] HFreqGain6[10:8]
				// d3 - [7:0] HFreqGain6[7:0]
	WriteReg32(L_BASE_ADDRESS + 0x02d4, 0x008000aa);	// d4 - [2:0] HFreqGain7[10:8]
				// d5 - [7:0] HFreqGain7[7:0]
#endif

#ifdef CA_SETTING
	WriteReg32(0xe0088700,0x00010104);// Alpha[0]|Alpha[1]|Beta[0]|Beta[1]
	WriteReg32(0xe0088714,0x03010104);// Coef[0]|Coef[1]|Coef[2]|Coef[3]
#endif

#ifdef TDDNS_SETTING
	WriteReg32(0xe0088550,0x00040010); // nIfComb on, [15:0] TSigma[0]
	WriteReg32(0xe0088554,0x00120014); // TSigma[1]~[2]
	WriteReg32(0xe0088558,0x00180020); // TSigma[3]~[4]
	WriteReg32(0xe008855c,0x00608006); // [31:16] Tsigma[5],[15:8] TmpW,[7:0] Ratio
	WriteReg32(0xe0088560,0x28080000); // [31:24] PattThr,[23:16] ChgThr,[15:0] Overlap x
#endif

#ifdef EDR_SETTING
//EDR
	WriteReg32(0xe0089400, 0x3f000002); //

	WriteReg32(L_BASE_ADDRESS + 0x0400, 0x01008800);	//	
	WriteReg32(L_BASE_ADDRESS + 0x0428, 0x00000001);
	
	//Gamma list
	WriteReg32(L_BASE_ADDRESS + 0x040c, 0x01000100);
	WriteReg32(L_BASE_ADDRESS + 0x0410, 0x01000100);
	WriteReg32(L_BASE_ADDRESS + 0x0414, 0x01000100);
	WriteReg32(L_BASE_ADDRESS + 0x042c, 0x01000100);

	WriteReg32(L_BASE_ADDRESS + 0x0438, 0x00800020);	//
	WriteReg32(L_BASE_ADDRESS + 0x043c, 0x10080008);	//
	WriteReg32(L_BASE_ADDRESS + 0x0440, 0x100003a0);	//
	WriteReg32(L_BASE_ADDRESS + 0x0444, 0x01200020);	//
	WriteReg32(L_BASE_ADDRESS + 0x0448, 0x00800100);	//
	WriteReg32(L_BASE_ADDRESS + 0x044c, 0x01040500);	//

	WriteReg32(L_BASE_ADDRESS + 0x0450, 0x40302010);	//
	WriteReg32(L_BASE_ADDRESS + 0x0454, 0x80706050);	//
	WriteReg32(L_BASE_ADDRESS + 0x0458, 0xc0b0a090);	//
	WriteReg32(L_BASE_ADDRESS + 0x045c, 0x80f0e0d0);	//

	WriteReg32(L_BASE_ADDRESS + 0x0460, 0x02000020);	//
	WriteReg32(L_BASE_ADDRESS + 0x0464, 0x10204040);	//
	WriteReg32(L_BASE_ADDRESS + 0x0468, 0x80200101);	//
	WriteReg32(L_BASE_ADDRESS + 0x046c, 0x78681801);	//

	WriteReg32(L_BASE_ADDRESS + 0x0470, 0x40302010);	//
	WriteReg32(L_BASE_ADDRESS + 0x0474, 0x80706050);	//
	WriteReg32(L_BASE_ADDRESS + 0x0478, 0xc0b0a090);	//
	WriteReg32(L_BASE_ADDRESS + 0x047c, 0x00f0e0d0);	//

	WriteReg32(L_BASE_ADDRESS + 0x0480, 0x01010101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0484, 0x88880101);	//
	WriteReg32(L_BASE_ADDRESS + 0x0488, 0x40201040);	//
	WriteReg32(L_BASE_ADDRESS + 0x048c, 0x01010180);	//
#endif	
	return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{

};
/*************************
  NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
