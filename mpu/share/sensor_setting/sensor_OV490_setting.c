#include "includes.h"
#define MIRROR
// If defined PRINT_HOST_CMD, uart prints host command name while it is called.
#define PRINT_HOST_CMD

#ifndef HIGH_BYTE
#define HIGH_BYTE(X)                    ( ((X)>>8) & 0xFF )
#endif  //HIGH_BYTE

#ifndef LOW_BYTE
#define LOW_BYTE(X)                     ( (X) & 0xFF )
#endif

#ifndef HIGH_WORD
#define HIGH_WORD(X)                    ( ((X)>>16) & 0xFFFF )
#endif

#ifndef LOW_WORD
#define LOW_WORD(X)                     ( (X) & 0xFFFF )
#endif

#ifndef CEIL_DIV
#define CEIL_DIV(X,Y)                   ( ((X)+(Y)-1) / (Y) )
#endif

#define SENSOR_RESOLUTION_2560x1080     1
#define SENSOR_RESOLUTION_2000x1000     2
#define SENSOR_RESOLUTION_2080x1040     3
#define SENSOR_RESOLUTION_2144x1072     4

#define SENSOR_RESOLUTION               SENSOR_RESOLUTION_2080x1040

#if( SENSOR_RESOLUTION == SENSOR_RESOLUTION_2560x1080 ) 
#define SENSOR_WIDTH                    2560
#define SENSOR_HEIGHT                   1080
#elif( SENSOR_RESOLUTION == SENSOR_RESOLUTION_2080x1040 )
#define SENSOR_WIDTH                    2080
#define SENSOR_HEIGHT                   1040
#elif( SENSOR_RESOLUTION == SENSOR_RESOLUTION_2144x1072 )
#define SENSOR_WIDTH                    2144
#define SENSOR_HEIGHT                   1072
#elif( SENSOR_RESOLUTION == SENSOR_RESOLUTION_2000x1000 ) 
#define SENSOR_WIDTH                    2000  
#define SENSOR_HEIGHT                   1000 
#endif //SENSOR_RESOLUTION

#define NUM_OV490                       2
#define SENSOR_SCCB_ID_INTERNAL         0x48
#define SENSOR_NAME_INTERNAL            OV490
#define SENSOR_SCCB_MODE_INTERNAL       SCCB_MODE16
#define SENSOR_DATAFMT_INTERNAL         DPM_DATAFMT_YUV422
#define SENSOR_INTERFACETYPE_INTERNAL   SNR_IF_MIPI_2LN

#define SENSOR_SCCB_ID_INTERNAL2        0x46
#define SENSOR_NAME_INTERNAL2           OV490_2
#define SENSOR_SCCB_MODE_INTERNAL2      SCCB_MODE16
#define SENSOR_DATAFMT_INTERNAL2        DPM_DATAFMT_YUV422
#define SENSOR_INTERFACETYPE_INTERNAL2  SNR_IF_MIPI_2LN

#define SCCB_MEM_BANK_HIGH              (0xFFFD)
#define SCCB_MEM_BANK_LOW               (0xFFFE)
#define SCCB_BOOT_RDY                   (0xa1)
#define SCCB_BOOT_JUMPBOOT_START        (0xc2)
#define SCCB_BOOT_JUMPBOOT_START_CACHE  (0xc5)
#define SCCB_BOOT_JUMPBOOT_OK           (0xc3)
#define SCCB_BOOT_JUMPBOOT_ERR          (0xc4)
#define FW_RUNNING_STAGE_INIT           (0x01)
#define FW_RUNNING_STAGE_SNR_INIT_DONE  (0x11)
#define FW_RUNNING_STAGE_FINAL_DONE     (0x02)
#define FW_RUNNING_STAGE_SNR_ID_ERR     (0xEE)
#define FW_RUNNING_STAGE_SLAVEID_NACK   (0xFF)

#define SRAM_FLAG_ADDR                  (0x8019ffc8)
#define SRAM_FW_ADDR_BOOT               (0x8019ffc4)
#define SRAM_FW_START                   (0x801900f8)
#define ADDR_FW_BOOT                    (0x00190000)
#define SCCB_BURST_ADDR                 (0x802a6000)
#define FW_RUNNING_STAGE_ADDR           (0x80800120)

#define OV490_STATUS_ADDR               (0x80195ffc)
#define HOST_CMD_PARA_ADDR              (0x80195000)
#define HOST_CMD_RESULT_ADDR            (0x80195000)
#define OV490_HOST_INT_ADDR             (0x808000c0)
#define STATUS_FINISH                   (0x99)
#define STATUS_ERROR2                   (0x88)
#define STATUS_ERROR                    (0x55)
#define CMD_BRIGHTNESS_SET              (0xf1)
#define CMD_SATURATION_SET              (0xf3)
#define CMD_HUE_SET                     (0xf5)
#define CMD_FRAMERATE_SET               (0xf7)
#define CMD_GAMMA_SET                   (0xf9)
#define CMD_SHARPNESS_SET               (0xfb)
#define CMD_CONTRAST_SET                (0xfd)
#define CMD_GROUPWRITE_SET              (0xe1)
#define CMD_STREAMING_CTRL              (0xe2)
#define CMD_CONTEXT_SWITCH_CONFIG       (0xe3)
#define CMD_CONTEXT_SWITCH_CTRL         (0xe4)
#define CMD_MULT_CMD                    (0xe5)
#define CMD_GPIO_SET                    (0xe6)
#define CMD_GPIO_GET                    (0xe7)
#define CMD_FORMAT_SET                  (0xe8)
#define CMD_TEMP_GET                    (0xe9)
#define CMD_EXPOSURE_GAIN_SET           (0xea)
#define CMD_AWBGAIN_SET                 (0xeb)
#define CMD_DENOISE_SET                 (0xec)
#define CMD_TONECURVE_SET               (0xed)
#define CMD_COMB_WEIGHT_SET             (0xee)
#define CMD_AEC_WEIGHT_SET              (0xd2)
#define CMD_AWB_ROI_SET                 (0xd3)
#define CMD_TONEMAPPING_ROI_SET         (0xd4)
#define CMD_STAT_ROI_SET                (0xd5)
#define CMD_TESTPATTERN_SET             (0xd6)
#define CMD_MTF_SET                     (0xd7)
#define CMD_LENC_SET                    (0xd8)
#define CMD_BLC_SET                     (0xd9)
#define CMD_GROUPWRITE_LAUNCH           (0xda)
#define CMD_EMBLINE_CTRL                (0xdb)
#define CMD_MIRRFLIP_CTRL               (0xdc)
#define CMD_EXTRA_VTS_SET               (0xde)
//#define CMD_SNR_REG_ACCESS              (0xc1) //c1 is for single sensor mode
#define CMD_SNR_REG_ACCESS              (0x20)
#define CMD_POSTAWBGAIN_SET             (0xc2)
#define CMD_CROP_SET                    (0xc3)
#define CMD_FRAMESYNC                   (0xc4)
#define CMD_BANDING_SET                 (0xc5)
#define CMD_TOPEMB_SET                  (0xc7)
#define CMD_FWREG_ACCESS                (0x35)
#define CMD_FADE_CTRL                   (0x37)

#define OV490_BOOT_TIMEOUT_TIME         5000
#define OV490_DELAY_TICK_TIME_IN_MS     10   // 10ms, this is the min delay time unit. It is predefined by OS.

typedef struct _OV490
{
    int bank_addr;
    t_libsccb_cfg* sccb_cfg;
} SOV490, *PSOV490;

typedef struct _DEBUG_STR
{
    int id;
    char* str;
} SDEBUG_STR, PDEBUG_STR;

#ifdef PRINT_HOST_CMD

SDEBUG_STR cmd_set[] =
{
{ CMD_BRIGHTNESS_SET, "CMD_BRIGHTNESS_SET" },
{ CMD_SATURATION_SET, "CMD_SATURATION_SET" },
{ CMD_HUE_SET, "CMD_HUE_SET" },
{ CMD_FRAMERATE_SET, "CMD_FRAMERATE_SET" },
{ CMD_GAMMA_SET, "CMD_GAMMA_SET" },
{ CMD_SHARPNESS_SET, "CMD_SHARPNESS_SET" },
{ CMD_CONTRAST_SET, "CMD_CONTRAST_SET" },
{ CMD_GROUPWRITE_SET, "CMD_GROUPWRITE_SET" },
{ CMD_STREAMING_CTRL, "CMD_STREAMING_CTRL" },
{ CMD_CONTEXT_SWITCH_CONFIG, "CMD_CONTEXT_SWITCH_CONFIG" },
{ CMD_CONTEXT_SWITCH_CTRL, "CMD_CONTEXT_SWITCH_CTRL" },
{ CMD_MULT_CMD, "CMD_MULT_CMD" },
{ CMD_GPIO_SET, "CMD_GPIO_SET" },
{ CMD_GPIO_GET, "CMD_GPIO_GET" },
{ CMD_FORMAT_SET, "CMD_FORMAT_SET" },
{ CMD_TEMP_GET, "CMD_TEMP_GET" },
{ CMD_EXPOSURE_GAIN_SET, "CMD_EXPOSURE_GAIN_SET" },
{ CMD_AWBGAIN_SET, "CMD_AWBGAIN_SET" },
{ CMD_DENOISE_SET, "CMD_DENOISE_SET" },
{ CMD_TONECURVE_SET, "CMD_TONECURVE_SET" },
{ CMD_COMB_WEIGHT_SET, "CMD_COMB_WEIGHT_SET" },
{ CMD_AEC_WEIGHT_SET, "CMD_AEC_WEIGHT_SET" },
{ CMD_AWB_ROI_SET, "CMD_AWB_ROI_SET" },
{ CMD_TONEMAPPING_ROI_SET, "CMD_TONEMAPPING_ROI_SET" },
{ CMD_STAT_ROI_SET, "CMD_STAT_ROI_SET" },
{ CMD_TESTPATTERN_SET, "CMD_TESTPATTERN_SET" },
{ CMD_MTF_SET, "CMD_MTF_SET" },
{ CMD_LENC_SET, "CMD_LENC_SET" },
{ CMD_BLC_SET, "CMD_BLC_SET" },
{ CMD_GROUPWRITE_LAUNCH, "CMD_GROUPWRITE_LAUNCH" },
{ CMD_EMBLINE_CTRL, "CMD_EMBLINE_CTRL" },
{ CMD_MIRRFLIP_CTRL, "CMD_MIRRFLIP_CTRL" },
{ CMD_EXTRA_VTS_SET, "CMD_EXTRA_VTS_SET" },
{ CMD_SNR_REG_ACCESS, "CMD_SNR_REG_ACCESS" },
{ CMD_POSTAWBGAIN_SET, "CMD_POSTAWBGAIN_SET" },
{ CMD_CROP_SET, "CMD_CROP_SET" },
{ CMD_FRAMESYNC, "CMD_FRAMESYNC" },
{ CMD_BANDING_SET, "CMD_BANDING_SET" },
{ CMD_TOPEMB_SET, "CMD_TOPEMB_SET" },
{ CMD_FWREG_ACCESS, "CMD_FWREG_ACCESS" },
{ CMD_FADE_CTRL, "CMD_FADE_CTRL" } };

char* lib490_get_cmd_name(int cmd_id)
{
    int i;
    for (i = 0; i < sizeof(cmd_set) / sizeof(SDEBUG_STR); i++)
    {
        if (cmd_set[i].id == cmd_id)
            return cmd_set[i].str;
    }
    return "";
}
#endif //#define PRINT_HOST_CMD

SOV490 sOV490[NUM_OV490] =
{
    { 0 },
    #if (NUM_OV490 >=2 )
    { 0 }
    #endif
};

/*************************
 Function declaration
 *************************/
inline int _get_ov490code_len(void);
inline unsigned char* _get_ov490code(void);

/*************************
 NEVER CHANGE this line BELOW !!!
 *************************/
#include "sensor_setting_start.h"

/*************************
 Initial setting
 *************************/
SENSOR_SETTING_TABLE sensor__init[][2] =
{
};

SENSOR_SETTING_FUNC t_sensoreffect_one tss[] =
{
};

/*************************
 setting for different size and framerate
 *************************/

/*************************
 effect change IOCTL
 *************************/
SENSOR_SETTING_FUNC int sensor__effect_change(t_sensor_cfg * cfg, u32 effect, u32 level)
{
    switch(effect)
    {
        case SNR_EFFECT_SHARPNESS:
        return 0;
        case SNR_EFFECT_SIZE:
        switch(level)
        {
            case VIDEO_SIZE_720P:
            break;
            default:
            return -1;
        }
        return 0;
        case SNR_EFFECT_FRMRATE:
        return 0;
        case SNR_EFFECT_PROC:
        return 0;
        default:
        return -2;
    }
    return 0;
}

inline static int lib490_get_delay_unit(void)
{
    return OV490_DELAY_TICK_TIME_IN_MS;
}

inline static void lib490_delay(int ms)
{
    int ten_ms;
    ten_ms = CEIL_DIV( ms, lib490_get_delay_unit()  );
    newos_timedelay(ten_ms);
}

s32 lib490_setbank(PSOV490 ov490, u32 bank_addr)
{

    if (ov490->bank_addr == bank_addr)
        return 0;

    ov490->bank_addr = bank_addr;
    libsccb_wr(ov490->sccb_cfg, SCCB_MEM_BANK_HIGH, HIGH_BYTE(bank_addr));
    libsccb_wr(ov490->sccb_cfg, SCCB_MEM_BANK_LOW, LOW_BYTE(bank_addr));
    return 0;
}

s32 lib490_read16(PSOV490 ov490, u32 addr)
{
    return libsccb_rd(ov490->sccb_cfg, LOW_WORD(addr)) & 0xFF;
}

s32 lib490_read(PSOV490 ov490, u32 addr)
{
    lib490_setbank(ov490, HIGH_WORD(addr));
    return libsccb_rd(ov490->sccb_cfg, LOW_WORD(addr)) & 0xFF;
}

s32 lib490_write(PSOV490 ov490, u32 addr, s32 data)
{
    lib490_setbank(ov490, HIGH_WORD(addr));
    return libsccb_wr(ov490->sccb_cfg, LOW_WORD(addr), data);
}

s32 lib490_getFlag(PSOV490 ov490)
{
    s32 val;
    val = lib490_read(ov490, SRAM_FLAG_ADDR);
    debug_printf("ready=%X\n\r", val);

    return val;
}

s32 lib490_set_clock(PSOV490 ov490)
{
    debug_printf("ov490 Set clock to  160Mhz...\n\r");
    lib490_write(ov490, 0x80800020, 0x01);
    lib490_write(ov490, 0x80800021, 0x16);
    lib490_write(ov490, 0x808000a0, 0x1d);
    lib490_write(ov490, 0x808000a1, 0xa1);
    lib490_write(ov490, 0x808000a2, 0x74);
    lib490_write(ov490, 0x808000a3, 0x29);
    return 0;
}

s32 lib490_upload_fw(PSOV490 ov490)
{
    int i;
    u32 fw_start = SRAM_FW_START;
    unsigned char* pCode = _get_ov490code();

    for (i = 0; i < _get_ov490code_len(); i++)
    {
        lib490_write(ov490, fw_start++, pCode[i]);

        if (i % 16 == 0)
            debug_printf("\033[15D\033[31mi = %d     ", i);
    }
    debug_printf("\033[15D\033[31mi = %d     \n\r", i);
    debug_printf("\033[30m\n\r", i);

#ifdef VERIFY_FW
    fw_start = SRAM_FW_START;
    for (i = 0; i < _get_ov490code_len(); i++)
    {
        s32 val;
        val = lib490_read(ov490, fw_start++);
        if (val != pCode[i])
        {
            debug_printf("Download Code error code[%d] %X= %X\n\r", i, pCode[i],
                    val);

        }
        if (i % 1024 == 0)
        debug_printf("i = %d\n\r", i);
    }
    debug_printf("i = %d\n\r", i);
#endif
    return 0;
}

s32 lib490_write32(PSOV490 ov490, u32 addr, s32 data)
{
    lib490_setbank(ov490, HIGH_WORD(addr));

    debug_printf("write32 %X =%x\n\r", addr, data);

    libsccb_wr(ov490->sccb_cfg, LOW_WORD(addr) + 0, (data >> (8 * 3)) & 0xFF);
    libsccb_wr(ov490->sccb_cfg, LOW_WORD(addr) + 1, (data >> (8 * 2)) & 0xFF);
    libsccb_wr(ov490->sccb_cfg, LOW_WORD(addr) + 2, (data >> (8 * 1)) & 0xFF);
    libsccb_wr(ov490->sccb_cfg, LOW_WORD(addr) + 3, (data >> (8 * 0)) & 0xFF);

    return 0;
}

s32 lib490_boot_fw(PSOV490 ov490)
{
    int tmp = ((SRAM_FW_START & 0x00ffffff) - 0xf8);

    debug_printf("FW Boot...\n\r");

    lib490_write32(ov490, SRAM_FW_ADDR_BOOT, tmp);

    debug_printf("FW JumpBoot start...\n\r");
    lib490_write(ov490, SRAM_FLAG_ADDR, SCCB_BOOT_JUMPBOOT_START);
    return 0;
}

s32 lib490_boot_wait(PSOV490 ov490)
{
    s32 val;
    int time_out = OV490_BOOT_TIMEOUT_TIME; // 5000 ms

    debug_printf("Get Flag...\n\r");

    val = lib490_read(ov490, SRAM_FLAG_ADDR); // C3 = OK, C4 = ERR

    while (SCCB_BOOT_JUMPBOOT_OK != (val = lib490_read(ov490, SRAM_FLAG_ADDR)))
    {
        debug_printf("val = 0x%x\n\r", val);
        if (val == SCCB_BOOT_JUMPBOOT_ERR)
        {
            debug_printf("Jump boot ERR...\n\r");
            break;
        }
        lib490_delay(lib490_get_delay_unit());
        time_out -= lib490_get_delay_unit();
        if( time_out <=0 )
        {
            debug_printf("OV490 boot timeout...\n\r");
            return -1;
        }
    }

    if (val == SCCB_BOOT_JUMPBOOT_OK)
    {
        debug_printf("Boot OK...%X\n\r", val);
    }
    else
    {
        return -1;
    }
    return 0;
}

s32 lib490_switch_burst_mode(PSOV490 ov490)
{
    // Enter burst mode
    debug_printf("set burst mode...\n\r");
    lib490_write(ov490, SCCB_BURST_ADDR, 0x01);
    return 0;
}

void lib490_hostctrl_set(PSOV490 ov490, u8 host_cmd, u8* para, u16 number)
{
    u32 val = 0;
    u32 i = 0;

#ifdef PRINT_HOST_CMD
    debug_printf("hostcmd( %s ", lib490_get_cmd_name(host_cmd));

    for (i = 0; i < number; i++)
    {
        debug_printf(",0x%02x ", para[i]);
    }

    debug_printf(" ) = ");
#endif

    // Host reset OV490_status_register;
    // val = lib490_read(ov490, OV490_STATUS_ADDR);

    // debug_printf("status  = %x \n", val);

    lib490_write(ov490, OV490_STATUS_ADDR, 0);

    // Host write parameters to OV490 SRAM;
    for (i = 0; i < number; i++)
    {
        lib490_write(ov490, HOST_CMD_PARA_ADDR + i, *(para + i));
    }

    // val = lib490_read(ov490, OV490_HOST_INT_ADDR);
    // debug_printf("cmd  = %x \n", val);

    // Host send I2C CMD to OV490 to start task:
    lib490_write(ov490, OV490_HOST_INT_ADDR, host_cmd);

    // val = lib490_read(ov490, OV490_HOST_INT_ADDR);
    // debug_printf("cmd  = %x \n", val);

    // Host check OV490_status_register val to determine whether the task is finished or not
    while (1)
    {

        val = lib490_read(ov490, OV490_STATUS_ADDR);

        //debug_printf("status  = %x \n", val);

        if (val == STATUS_FINISH)
        {
            debug_printf("STATUS FINISH\r\n");
            break;
        }
        else if (val == STATUS_ERROR || val == STATUS_ERROR2)
        {
            debug_printf("STATUS_ERROR\r\n");
            break;
        }

        // if task is not finished, wait n ms;
        lib490_delay(1);
    }

}

void lib490_hostctrl_get(PSOV490 ov490, unsigned char host_cmd,
        unsigned char* para, unsigned short number)
{
    u32 i = 0;
    //Host reset OV490_status_register;
    lib490_write(ov490, OV490_STATUS_ADDR, 0);
    //Host send I2C CMD to OV490 to start task:
    lib490_write(ov490, OV490_HOST_INT_ADDR, host_cmd);
    // Host check OV490_status_register val to determine whether the task is finished or not
    while (lib490_read(ov490, OV490_STATUS_ADDR) != STATUS_FINISH)
    {
        // if task is not finished, wait n ms;
        lib490_delay(1);
    }
    // Host read results from OV490 SRAM;
    for (i = 0; i < number; i++)
    {
        *(para + i) = lib490_read(ov490, HOST_CMD_RESULT_ADDR + i);
    }
}

void lib490_hostctrl_get_ex(PSOV490 ov490, unsigned char host_cmd,
        unsigned char* paraIn, unsigned short numberIn, unsigned char* paraOut,
        unsigned short numberOut)
{
    unsigned short i = 0;
    //Host reset OV490_status_register;
    lib490_write(ov490, OV490_STATUS_ADDR, 0);
    // Host write parameters to OV490 SRAM;
    for (i = 0; i < numberIn; i++)
    {
        lib490_write(ov490, HOST_CMD_PARA_ADDR + i, *(paraIn + i));
    }
    //Host send I2C CMD to OV490 to start task:
    lib490_write(ov490, OV490_HOST_INT_ADDR, host_cmd);
    // Host check OV490_status_register val to determine whether the task is finished or not
    while (lib490_read(ov490, OV490_STATUS_ADDR) != STATUS_FINISH)
    {
        // if task is not finished, wait n ms;
        lib490_delay(1);
    }
    // Host read results from OV490 SRAM;
    for (i = 0; i < numberOut; i++)
    {
        *(paraOut + i) = lib490_read(ov490, HOST_CMD_RESULT_ADDR + i);
    }
}

PSOV490 lib490_get490_by_index(u8 index)
{
    if (index >= NUM_OV490)
    {
        return NULL;
    }
    return &sOV490[index];
}

//Color Hue Level Adjust
void lib490_set_hue(u8 index, u8 level)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_HUE_SET, &level, 1);
}

//Color Saturation Level Adjust
void lib490_set_saturation(u8 index, u8 level)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_SATURATION_SET, &level, 1);
}

// Brightness Adjust
void lib490_set_brightness(u8 index, u8 type, u8 level)
{
    u8 param[4] =
    { 0, 0, 0, 0 };
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    param[0] = type;
    param[1] = level;
    lib490_hostctrl_set(ov490, CMD_BRIGHTNESS_SET, param, 4);
}

/*
 Sharpness Level Adjust
 Parameter number : 1
 Parameter 0 : level, Range : [0, 10]
 */
void lib490_set_sharpness(u8 index, u8 level)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_SHARPNESS_SET, &level, 1);
}

/*
 Contrast Level Adjust
 Parameter number : 1
 Parameter 0 : level, Range : [0, 10]
 Related firmware registers : LowContrastLevel, HighContrastLevel
 */
void lib490_set_contrast(u8 index, u8 level)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_CONTRAST_SET, &level, 1);
}

/*
 Manual Exposure/Gain Set
 Parameter number: 13
 Related firmware registers: ManualAECEnable, AECManualExp, AECManualGain

 -----------------------------------------------------------------------------
 | Index |            Description              |       Range / Bits          |
 -----------------------------------------------------------------------------
 |   0   |  manual exposure enable             | [0], 0: Auto mode,          |
 |       |                                     |      1: Manual mode         |
 -----------------------------------------------------------------------------
 |   1   |  manual exposure for L [15:8]       | [7:0]                       |
 -----------------------------------------------------------------------------
 |   2   |  manual exposure for L [7:0]        | [7:0]                       |
 -----------------------------------------------------------------------------
 |   3   |  manual exposure for S [15:8]       | [7:0]                       |
 -----------------------------------------------------------------------------
 |   4   |  manual exposure for S [7:0]        | [7:0]                       |
 -----------------------------------------------------------------------------
 |   5   |  manual exposure for VS [15:8]      | [7:0]                       |
 -----------------------------------------------------------------------------
 |   6   |  manual exposure for VS [7:0]       | [7:0]                       |
 -----------------------------------------------------------------------------
 |   7   |  manual gain for L [15:8]           | [7:0]                       |
 -----------------------------------------------------------------------------
 |   8   |  manual gain for L [7:0]            | [7:0]                       |
 -----------------------------------------------------------------------------
 |   9   |  manual gain for S [15:8]           | [7:0]                       |
 -----------------------------------------------------------------------------
 |  10   |  manual gain for S [7:0]            | [7:0]                       |
 -----------------------------------------------------------------------------
 |  11   |  manual gain for VS [15:8]          | [7:0]                       |
 -----------------------------------------------------------------------------
 |  12   |  manual gain for VS [7:0]           | [7:0]                       |
 -----------------------------------------------------------------------------

 */

void lib490_set_exposure_gain(u8 index, u8* param)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_EXPOSURE_GAIN_SET, param, 13);
}

/*
 Manual AWB R/G/B Gain Set
 */
void lib490_set_awb_rgb_gain(u8 index, u8* param)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_AWBGAIN_SET, param, 25);
}

/*
 Raw Denoise
 */
void lib490_set_raw_denoise(u8 index, u8* dns)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_DENOISE_SET, dns, 25);
}

/*
 RGB Denoise
 */
void lib490_set_rgb_denoise(u8 index, u8* dns)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_DENOISE_SET, dns, 13);
}

/*
 CIP Denoise
 */
void lib490_set_cip_denoise(u8 index, u8* dns)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_DENOISE_SET, dns, 9);
}

/*
 UV Denoise
 */
void lib490_set_uv_denoise(u8 index, u8* dns)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_DENOISE_SET, dns, 9);
}

void lib490_set_hdr_weight(u8 index, u8* weight)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_COMB_WEIGHT_SET, weight, 16);

}

void lib490_set_aec_weight(u8 index, u8* weight)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_AEC_WEIGHT_SET, weight, 13);

}

void lib490_set_awb_roi(u8 index, u8* roi)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_AWB_ROI_SET, roi, 9);

}

//TBD:: Tone mapping roi
//TBD:: Manual tone mapping curve set
//TBD:: Gamma set
//TBD:: GPIO control

void lib490_set_frame_rate(u8 index, u8 fr)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_FRAMERATE_SET, &fr, 1);
}

void lib490_stream_on(u8 index, u8 on_off)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_STREAMING_CTRL, &on_off, 1);
}

/*
 mode:
 0x00 - Normal video mode
 0x01 - OV10640 Colorbar, which is mainly used for OV10640 and OV490 DVP connection verification;
 0x02 - OV490 Front Raw Colorbar, which passes through OV490 ISP and is mainly used for OV490 output DVP connection verification;
 0x03 - OV490 Back YUV Colorbar, which is measurable and consistent regardless of ISP status;
 */
void lib490_set_test_pattern(u8 index, u8 mode)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_TESTPATTERN_SET, &mode, 1);
}

/* 
 0x00 YUV 12bit format
 0x01 YUV 12bit format �V sensor L/S channel only
 0x12 Sensor RAW 12bit format for VS Channel
 0x13 Sensor RAW 12bit format for S Channel
 0x14 Sensor RAW 12bit format for L Channel
 0x15 RAW 12bit format for VS Channel(After OV490 AWB)
 0x16 RAW 12bit format for S Channel(After OV490 AWB)
 0x17 RAW 12bit format for L Channel(After OV490 AWB)
 0x18 Linear Raw 12bit format
 0x19 Tone Mapping Raw
 */
void lib490_set_output_format(u8 index, u8 format)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;
    lib490_hostctrl_set(ov490, CMD_FORMAT_SET, &format, 1);
}

// TBD:: BLC SET
// TBD:: Temperature read
// TBD:: YUV Statistics Windows Set
// TBD:: AB Context Switch
// TBD:: Single Sub-pixel Raw12 mode
// TBD:: Mirror/Flip Control
// TBD:: LENC Parameter Set
// TBD:: Embedded Line Control
// TBD:: Special Configure
// TBD:: Group Write
// TBD:: Group Launch
// TBD:: Extra VTS Control
// TBD:: Post AWB Gain Set
// TBD:: Access Sensor/I2C_Dev Register
u8 lib490_sensor_reg_read(u8 index, u8 sensor_id, u16 addr)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return -1;

    {
        unsigned char paraIn[5] =
        { 0 };
        paraIn[0] = 1; //Read
        paraIn[1] = HIGH_BYTE(addr);
        paraIn[2] = LOW_BYTE(addr);
        paraIn[3] = 0;
        paraIn[4] = (sensor_id << 1) | 1;

        u8 value = 0;
        lib490_hostctrl_get_ex(ov490, CMD_SNR_REG_ACCESS, paraIn,
                sizeof(paraIn), &value, 1);
        debug_printf("%d-[%04x]=0x%02x\r\n", paraIn[4], addr, value);
        return value;
    }

}

void lib490_sensor_reg_write(u8 index, u8 sensor_id, u16 addr, u8 val)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    {
        unsigned char paraIn[5] =
        { 0 };
        paraIn[0] = 0; //WRITE 
        paraIn[1] = HIGH_BYTE(addr);
        paraIn[2] = LOW_BYTE(addr);
        paraIn[3] = val;
        paraIn[4] = (sensor_id << 1) | 1;

        lib490_hostctrl_set(ov490, CMD_SNR_REG_ACCESS, paraIn, sizeof(paraIn));
    }

}

// TBD:: Access Firmware Register
// TBD:: Frame Sync (+/- VTS)
// TBD:: Cropping
void lib490_set_cropping(u8 index, u16 x, u16 y, u16 w, u16 h)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    {
        u8 para[8];
        para[0] = HIGH_BYTE(w);
        para[1] = LOW_BYTE(w);
        para[2] = HIGH_BYTE(h);
        para[3] = LOW_BYTE(h);
        para[4] = HIGH_BYTE(x);
        para[5] = LOW_BYTE(x);
        para[6] = HIGH_BYTE(y);
        para[7] = LOW_BYTE(y);

        lib490_hostctrl_set(ov490, CMD_CROP_SET, para, sizeof(para));
    }
}

// TBD:: Fade In/Out Switch
// TBD:: 50Hz/60Hz Banding Control
// TBD:: Host Multi-commands
void lib490_mult_cmd(u8 index, u8* cmd_para, int size)
{
    PSOV490 ov490 = lib490_get490_by_index(index);
    if (ov490 == NULL)
        return;

    lib490_hostctrl_set(ov490, CMD_MULT_CMD, cmd_para, size);

}

bool lib490_is_ov490_exist(PSOV490 ov490)
{
    return (lib490_read(ov490, 0x300a) == 4
            && lib490_read(ov490, 0x300b) == 0x90);
}


s32 lib490_init(PSOV490 ov490)
{
    debug_printf("lib490_init ___%X\n\r", ov490->sccb_cfg->id);

    //libsccb_init(ov490->sccb_cfg);
    /*
     lib490_write(ov490, 0x80800010, 0x04); // RISC reset
     lib490_write(ov490, 0x80800044, 0x06); //[0]: 0: boot from ROM; 1: boot from SRAM
     lib490_write(ov490, 0x80800010, 0x00);
     */
    lib490_delay(200); //200 ms

    if( FW_RUNNING_STAGE_FINAL_DONE == lib490_read(ov490, FW_RUNNING_STAGE_ADDR) )
    {
        debug_printf("Sensor already initialized\n\r");
        return 0;
    }

    while (lib490_getFlag(ov490) != SCCB_BOOT_RDY)
    {
        debug_printf("wait for ready...\n\r");
    }
    debug_printf("Ready...\n\r");

    lib490_set_clock(ov490);

    debug_printf("Upload FW to ov490...\n\r");
    lib490_upload_fw(ov490);

    lib490_boot_fw(ov490);

    lib490_delay(300); //300 ms

    if (lib490_boot_wait(ov490) != 0)
    {
        return -1;
    }

    debug_printf("Wait stage final done ...\n\r");

    s32 last_val = -1;
    while (1)
    {
        s32 val = 0;

        val = lib490_read(ov490, FW_RUNNING_STAGE_ADDR);

        if (val == FW_RUNNING_STAGE_INIT)
        {
        }
        else if (val == FW_RUNNING_STAGE_SNR_INIT_DONE)
        {

        }
        else if (val == FW_RUNNING_STAGE_FINAL_DONE)
        {
            break;
        }
        else if (val == FW_RUNNING_STAGE_SNR_ID_ERR)
        {
            debug_printf("Sensor id error\n\r");
            return -1;
        }
        else if (val == FW_RUNNING_STAGE_SLAVEID_NACK)
        {
            debug_printf("slave id nack...\n\r");
            return -1;
        }

        if (last_val != val)
            debug_printf("running = %X\n\r", val);

        last_val = val;
    }



    debug_printf("done ...\n\r");


    lib490_sensor_reg_read(0, 0, 0x3082);
    lib490_sensor_reg_read(0, 0, 0x3083);

    if( ov490->sccb_cfg->id == 0x48 )
    {
        // Read 10640 sensor id
        lib490_sensor_reg_read(0, 0, 0x300a);
        lib490_sensor_reg_read(0, 0, 0x300b);

        lib490_sensor_reg_read(0, 1, 0x300a);
        lib490_sensor_reg_read(0, 1, 0x300b);

        // Added 10640 driving strength
        lib490_sensor_reg_write(0, 0, 0x301d, 0x30);
        lib490_sensor_reg_write(0, 1, 0x301d, 0x30);
        lib490_stream_on(0, 0);
#ifdef MIRROR

    lib490_sensor_reg_write(0, 0, 0x3090, 8);
    lib490_sensor_reg_write(0, 0, 0x3291, 5);
    lib490_sensor_reg_write(0, 0, 0x3128,0xC2);
    lib490_sensor_reg_write(0, 1, 0x3090, 8);
    lib490_sensor_reg_write(0, 1, 0x3291, 5);
    lib490_sensor_reg_write(0, 1, 0x3128,0xC2);


    // shift startx and endx
    lib490_sensor_reg_write( 0, 0, 0x3075, 0 );
    lib490_sensor_reg_write( 0, 1, 0x3075, 0 );

    lib490_sensor_reg_write( 0, 0, 0x3079, 7 );
    lib490_sensor_reg_write( 0, 1, 0x3079, 7 );


    lib490_sensor_reg_read( 0, 0, 0x3076 );
    lib490_sensor_reg_read( 0, 0, 0x3077 );
    lib490_sensor_reg_read( 0, 0, 0x307a );
    lib490_sensor_reg_read( 0, 0, 0x307b );


    lib490_sensor_reg_write( 0, 0, 0x3077, 1 );
    lib490_sensor_reg_write( 0, 1, 0x3077, 1 );

    lib490_sensor_reg_write( 0, 0, 0x307b, 0x40 );
    lib490_sensor_reg_write( 0, 1, 0x307b, 0x40 );

    /*
    lib490_sensor_reg_write(0, 0, 0x3090, 4);
    lib490_sensor_reg_write(0, 0, 0x3291, 3);
    lib490_sensor_reg_write(0, 0, 0x3128,0xC1);
    lib490_sensor_reg_write(0, 1, 0x3090, 4);
    lib490_sensor_reg_write(0, 1, 0x3291, 3);
    lib490_sensor_reg_write(0, 1, 0x3128,0xC1);


    // shift startx and endx
    lib490_sensor_reg_write( 0, 0, 0x3075, 1 );
    lib490_sensor_reg_write( 0, 1, 0x3075, 1 );

    lib490_sensor_reg_write( 0, 0, 0x3079, 8 );
    lib490_sensor_reg_write( 0, 1, 0x3079, 8 );
*/
#endif
    }
    else
    {
        // Read 10640 sensor id
        lib490_sensor_reg_read(1, 0, 0x300a);
        lib490_sensor_reg_read(1, 0, 0x300b);

        lib490_sensor_reg_read(1, 1, 0x300a);
        lib490_sensor_reg_read(1, 1, 0x300b);

        // Added 10640 driving strength
        lib490_sensor_reg_write(1, 0, 0x301d, 0x30);
        lib490_sensor_reg_write(1, 1, 0x301d, 0x30);
        //lib490_set_frame_rate(1, 12 );
#ifdef MIRROR 
	lib490_stream_on(1,0);

    lib490_sensor_reg_write(1, 0, 0x3090, 8);
    lib490_sensor_reg_write(1, 0, 0x3291, 5);
    lib490_sensor_reg_write(1, 0, 0x3128,0xC2);
    lib490_sensor_reg_write(1, 1, 0x3090, 8);
    lib490_sensor_reg_write(1, 1, 0x3291, 5);
    lib490_sensor_reg_write(1, 1, 0x3128,0xC2);


    // shift startx and endx
    lib490_sensor_reg_write( 1, 0, 0x3075, 0 );
    lib490_sensor_reg_write( 1, 1, 0x3075, 0 );

    lib490_sensor_reg_write( 1, 0, 0x3079, 7 );
    lib490_sensor_reg_write( 1, 1, 0x3079, 7 );

    lib490_sensor_reg_write( 1, 0, 0x3077, 1 );
    lib490_sensor_reg_write( 1, 1, 0x3077, 1 );

    lib490_sensor_reg_write( 1, 0, 0x307b, 0x40 );
    lib490_sensor_reg_write( 1, 1, 0x307b, 0x40 );


/*
    lib490_sensor_reg_write(1, 0, 0x3090, 4);
    lib490_sensor_reg_write(1, 0, 0x3291, 3);
    lib490_sensor_reg_write(1, 0, 0x3128,0xC1);
    lib490_sensor_reg_write(1, 1, 0x3090, 4);
    lib490_sensor_reg_write(1, 1, 0x3291, 3);
    lib490_sensor_reg_write(1, 1, 0x3128,0xC1);


    // shift startx and endx
    lib490_sensor_reg_write( 1, 0, 0x3075, 1 );
    lib490_sensor_reg_write( 1, 1, 0x3075, 1 );

    lib490_sensor_reg_write( 1, 0, 0x3079, 8 );
    lib490_sensor_reg_write( 1, 1, 0x3079, 8 );
*/

	lib490_stream_on(1,1);
#endif
        lib490_stream_on(0, 1);
        lib490_stream_on(1, 1);
    }
    return 0;
}

PSOV490 lib490_find_inst(t_sensor_cfg* cfg)
{
    int i;

    for (i = 0; i < NUM_OV490; i++)
    {
        if (sOV490[i].sccb_cfg == NULL || sOV490[i].sccb_cfg == cfg->sccb_cfg)
        {
            sOV490[i].sccb_cfg = cfg->sccb_cfg;
            sOV490[i].bank_addr = 0;
            return &sOV490[i];
        }
    }

    return NULL;

}

void lib490_show(PSOV490 ov490, s32 base)
{
    int i;
    char val[4];

    debug_printf("%X=[", base);

    for (i = 0; i < 4; i++)
    {
        val[i] = lib490_read(ov490, base + i);
        debug_printf(" %02X ", val[i] & 0xFF);
    }
    debug_printf("]\r\n");
}

void dbg_info0(void)
{
    debug_printf(" ------------------  SENSOR 0 ------------------ \r\n");
    lib490_sensor_reg_read(0, 0, 0x300a);
    lib490_sensor_reg_read(0, 0, 0x300b);

    debug_printf(" R3000: 0x02                        \r\n");
    debug_printf(" R3001: 0x22                        \r\n");
    debug_printf(" R3002: 0x07                        \r\n");
    debug_printf(" R3004: 0x03                        \r\n");
    debug_printf(" R3005: 0x69                        \r\n");
    debug_printf(" R3006: 0x07                        \r\n");
    debug_printf(" R3007: 0x01                        \r\n");

    lib490_sensor_reg_read(0, 0, 0x3000);
    lib490_sensor_reg_read(0, 0, 0x3001);
    lib490_sensor_reg_read(0, 0, 0x3002);
    lib490_sensor_reg_read(0, 0, 0x3004);
    lib490_sensor_reg_read(0, 0, 0x3005);
    lib490_sensor_reg_read(0, 0, 0x3006);
    lib490_sensor_reg_read(0, 0, 0x3007);

    debug_printf(" HTS(3080/3081): 0x5b6(1462)        \r\n");

    lib490_sensor_reg_read(0, 0, 0x3080);
    lib490_sensor_reg_read(0, 0, 0x3081);

    debug_printf(" VTS(3082/3083): 0x48b(1163)        \r\n");
    lib490_sensor_reg_read(0, 0, 0x3082);
    lib490_sensor_reg_read(0, 0, 0x3083);

    debug_printf(" Crop H Start(3074/3075): 0x0(0)    \r\n");

    lib490_sensor_reg_read(0, 0, 0x3074);
    lib490_sensor_reg_read(0, 0, 0x3075);
    debug_printf(" Crop H End(3078/3079): 0x507(1287) \r\n");
    lib490_sensor_reg_read(0, 0, 0x3078);
    lib490_sensor_reg_read(0, 0, 0x3079);

    debug_printf(" Crop V Start(3076/3077): 0x2(2)    \r\n");

    lib490_sensor_reg_read(0, 0, 0x3076);
    lib490_sensor_reg_read(0, 0, 0x3077);

    debug_printf(" Crop V End(307A/307B): 0x441(1089) \r\n");
    lib490_sensor_reg_read(0, 0, 0x307a);
    lib490_sensor_reg_read(0, 0, 0x307b);

    debug_printf(" WinOff_H/V(3084/5, 3086/7): 4 4    \r\n");
    lib490_sensor_reg_read(0, 0, 0x3084);
    lib490_sensor_reg_read(0, 0, 0x3085);

    lib490_sensor_reg_read(0, 0, 0x3086);
    lib490_sensor_reg_read(0, 0, 0x3087);

    debug_printf(" DVP H(307C/307D): 0x508(1288)      \r\n");
    lib490_sensor_reg_read(0, 0, 0x307c);
    lib490_sensor_reg_read(0, 0, 0x307d);

    debug_printf(" DVP V(307E/307F): 0x440(1088)      \r\n");
    lib490_sensor_reg_read(0, 0, 0x307e);
    lib490_sensor_reg_read(0, 0, 0x307f);

}

void dbg_info1(void)
{
    debug_printf(" ------------------  SENSOR 1 ------------------ \r\n");

    lib490_sensor_reg_read(0, 1, 0x300a);
    lib490_sensor_reg_read(0, 1, 0x300b);

    debug_printf(" R3000: 0x02                        \r\n");
    debug_printf(" R3001: 0x22                        \r\n");
    debug_printf(" R3002: 0x07                        \r\n");
    debug_printf(" R3004: 0x03                        \r\n");
    debug_printf(" R3005: 0x69                        \r\n");
    debug_printf(" R3006: 0x07                        \r\n");
    debug_printf(" R3007: 0x01                        \r\n");

    lib490_sensor_reg_read(0, 1, 0x3000);
    lib490_sensor_reg_read(0, 1, 0x3001);
    lib490_sensor_reg_read(0, 1, 0x3002);
    lib490_sensor_reg_read(0, 1, 0x3004);
    lib490_sensor_reg_read(0, 1, 0x3005);
    lib490_sensor_reg_read(0, 1, 0x3006);
    lib490_sensor_reg_read(0, 1, 0x3007);

    debug_printf(" HTS(3080/3081): 0x5b6(1462)        \r\n");

    lib490_sensor_reg_read(0, 1, 0x3080);
    lib490_sensor_reg_read(0, 1, 0x3081);

    debug_printf(" VTS(3082/3083): 0x48b(1163)        \r\n");
    lib490_sensor_reg_read(0, 1, 0x3082);
    lib490_sensor_reg_read(0, 1, 0x3083);

    debug_printf(" Crop H Start(3074/3075): 0x0(0)    \r\n");

    lib490_sensor_reg_read(0, 1, 0x3074);
    lib490_sensor_reg_read(0, 1, 0x3075);
    debug_printf(" Crop H End(3078/3079): 0x507(1287) \r\n");
    lib490_sensor_reg_read(0, 1, 0x3078);
    lib490_sensor_reg_read(0, 1, 0x3079);

    debug_printf(" Crop V Start(3076/3077): 0x2(2)    \r\n");

    lib490_sensor_reg_read(0, 1, 0x3076);
    lib490_sensor_reg_read(0, 1, 0x3077);

    debug_printf(" Crop V End(307A/307B): 0x441(1089) \r\n");
    lib490_sensor_reg_read(0, 1, 0x307a);
    lib490_sensor_reg_read(0, 1, 0x307b);

    debug_printf(" WinOff_H/V(3084/5, 3086/7): 4 4    \r\n");
    lib490_sensor_reg_read(0, 1, 0x3084);
    lib490_sensor_reg_read(0, 1, 0x3085);

    lib490_sensor_reg_read(0, 1, 0x3086);
    lib490_sensor_reg_read(0, 1, 0x3087);

    debug_printf(" DVP H(307C/307D): 0x508(1288)      \r\n");
    lib490_sensor_reg_read(0, 1, 0x307c);
    lib490_sensor_reg_read(0, 1, 0x307d);

    debug_printf(" DVP V(307E/307F): 0x440(1088)      \r\n");
    lib490_sensor_reg_read(0, 1, 0x307e);
    lib490_sensor_reg_read(0, 1, 0x307f);

}

/*************************
 sensor detect func
 *************************/
SENSOR_SETTING_FUNC s32 sensor__detect(t_sensor_cfg * cfg)
{
    PSOV490 ov490;
    s32 status;

    debug_printf("sensor__detect ov490\n\r");

    ov490 = lib490_find_inst(cfg);

    if( ov490 == NULL ) return -1;

    debug_printf( "490 id =  %X%02X\r\n", lib490_read16( ov490, 0x300a ), lib490_read16( ov490, 0x300b ) );

    status = lib490_init(ov490);

//    lib490_set_test_pattern(ov490,3);

    lib490_write( ov490, 0x80296010, lib490_read(ov490, 0x80296010)& (~0x10) );// YUYV format 0x80296010[4]

    //while(1)
    if(0)
    {
        lib490_show( ov490, 0x80290038 );
        lib490_show( ov490, 0x802900B8 );
        lib490_show( ov490, 0x80206085 );
        lib490_show( ov490, 0x80206094 );

        dbg_info0();
        dbg_info1();
        lib490_delay(1000);
    }

    cfg->cur_video_size = (SENSOR_WIDTH<<16) | SENSOR_HEIGHT;

    debug_printf( "sensor_detect() ...... done %d\n\r", status);

    return status;
}

SENSOR_SETTING_FUNC int sensor__rgbirinit(void* dpm_rgbir){
	return 0;	
}

SENSOR_SETTING_FUNC int sensor__ispinit(t_sensor_cfg * cfg)
{
    return 0;
}

SENSOR_SETTING_FUNC t_sensor_isptables sensor__isptables =
{};

/*************************
 OV490 FW Binary code 
 *************************/

#if( SENSOR_RESOLUTION ==  SENSOR_RESOLUTION_2560x1080 )
#include "ov490/sensor_OV490_bin_2560x1080.txt"
#elif (SENSOR_RESOLUTION == SENSOR_RESOLUTION_2080x1040 )
//#include "ov490/sensor_OV490_bin_2080x1040fr4.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr5.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr7_5.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr10.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr13.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr14.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr15.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr18.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr18_5.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr20.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr21.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr22.txt"
//#include "ov490/sensor_OV490_bin_2080x1040fr23.txt"
#include "ov490/sensor_OV490_bin_2080x1040fr24.txt"
//#include "ov490/sensor_OV490_bin_2080x1040.txt"
#elif (SENSOR_RESOLUTION == SENSOR_RESOLUTION_2144x1072 )
#include "ov490/sensor_OV490_bin_2144x1072.txt"
#elif (SENSOR_RESOLUTION == SENSOR_RESOLUTION_2000x1000 )
#include "ov490/sensor_OV490_bin_2000x1000.txt"
#endif //SENSOR_SOLUTION

unsigned char* _get_ov490code(void)
{
    return ov490code;
}

int _get_ov490code_len(void)
{
    return sizeof(ov490code);
}

/*************************
 OV490 Test code
 *************************/

void test_hostcmd(PSOV490 ov490)
{
    int val = 0;

    // Clear register
    libsccb_wr(ov490->sccb_cfg, 0xfffd, 0x80);
    libsccb_wr(ov490->sccb_cfg, 0xfffe, 0x19);
    libsccb_wr(ov490->sccb_cfg, 0x5ffc, 0xCC);  //---> 0x0

    // HUE command
    libsccb_wr(ov490->sccb_cfg, 0xfffd, 0x80);
    libsccb_wr(ov490->sccb_cfg, 0xfffe, 0x19);
    libsccb_wr(ov490->sccb_cfg, 0x5000, 0xc);
    libsccb_wr(ov490->sccb_cfg, 0xfffe, 0x80);
    libsccb_wr(ov490->sccb_cfg, 0xc0, 0xf5);

    // Switch to 0x8019
    libsccb_wr(ov490->sccb_cfg, 0xfffd, 0x80);
    libsccb_wr(ov490->sccb_cfg, 0xfffe, 0x19);

    while (1)
    {
        val = libsccb_rd(ov490->sccb_cfg, 0x5ffc);

        debug_printf("Status  = %x \n", val);

        if (val == STATUS_FINISH)
        {
            break;
        }
        else if (val == STATUS_ERROR)
        {
            break;
        }
    }

}

/*************************
 NEVER CHANGE BELOW !!!
 *************************/
#include "sensor_setting_end.h"
