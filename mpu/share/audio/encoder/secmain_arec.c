

/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/
#include "includes.h"


#if 0
#define OUTLIST_NUM		4
AREC_ALLOCATE_STEREO(OUTLIST_NUM, OUTBUF_ONELEN*OUTLIST_NUM)
#endif

extern u32 total_frames_in, total_frames_out;
static volatile u32 arec_started = 0;

static s32 arec_irq_handler(void * arg);

static u32 arec_handle_cmd(u32 cmdID,u32 arg)
{
	s32 ret;
	t_arec_inpara *inp;

	switch(cmdID)
	{
		case MPUCMD_ARECORD_INIT:
			if(arec_started == 1){
				return 0;
			}
			syslog(LOG_DEBUG, "MPUCMD_AREC_INIT\n");

			inp = (t_arec_inpara*)arg;
#if 0
			memset(&g_liba_ecfg,0,sizeof(g_liba_ecfg));
			AREC_CONFIG_STEREO();
#else
			g_liba_ecfg.AFI_BUF0 |= BIT31;
			g_liba_ecfg.AFI_BUF1 |= BIT31;
			g_liba_ecfg.outbuf_addr |= BIT31;
#endif
			memcpy(&g_liba_ecfg.inp,inp,sizeof(t_arec_inpara));		

			syslog(LOG_DEBUG, "fmt = %d\n",g_liba_ecfg.inp.fmt);
			syslog(LOG_DEBUG, "sr = %d\n",g_liba_ecfg.inp.sr);
			syslog(LOG_DEBUG, "ch = %d\n",g_liba_ecfg.inp.ch);
			syslog(LOG_DEBUG, "aimode = %d\n",g_liba_ecfg.inp.aimode);
			syslog(LOG_DEBUG, "drop_samples = %d\n",g_liba_ecfg.inp.drop_samples);

			if(inp->denoise_en || inp->agc.en || inp->compressor_en){
				extern u32 aencpre_cfg_start;
				extern u32 aencpre_cfg_end;
				t_aencpre_cfg *p_aencpre_cfg = (t_aencpre_cfg *)(&aencpre_cfg_start);
				g_liba_ecfg.pre_init = NULL;
				g_liba_ecfg.pre_process = NULL;
				for(;p_aencpre_cfg < (t_aencpre_cfg*)(&aencpre_cfg_end);p_aencpre_cfg ++){
					if(p_aencpre_cfg->alg == AUDIO_EPRE_DENOISE){
						g_liba_ecfg.pre_init = p_aencpre_cfg->pre_init;
						g_liba_ecfg.pre_process = p_aencpre_cfg->pre_process;
						syslog(LOG_INFO, "set audio encoder preprocess algo AUDIO_EPRE_DENOISE sucessfully\n");
						break;
					}
				}
				if(g_liba_ecfg.pre_process == NULL){
					syslog(LOG_WARNING, "set audio encoder preprocess algo AUDIO_EPRE_DENOISE fail, audio denoise is not enabled\n");
				}
			}

			g_aec_enabled  = 0;
			if(inp->aec_en && inp->ch==1){
				extern u32 aencpre_cfg_start;
				extern u32 aencpre_cfg_end;
				t_aencpre_cfg *p_aencpre_cfg = (t_aencpre_cfg *)(&aencpre_cfg_start);
				g_liba_ecfg.aec_init = NULL;
				g_liba_ecfg.aec_process = NULL;
				g_liba_ecfg.aec_exit = NULL;
				for(;p_aencpre_cfg < (t_aencpre_cfg*)(&aencpre_cfg_end);p_aencpre_cfg ++){
					if(p_aencpre_cfg->alg == AUDIO_EPRE_AEC){
						g_liba_ecfg.aec_init = p_aencpre_cfg->pre_init;
						g_liba_ecfg.aec_process = (void (*)(void *out, void *in, void *ref, int len))p_aencpre_cfg->pre_process;
						g_liba_ecfg.aec_exit = p_aencpre_cfg->pre_exit;
//						g_aec_refbuf = (u8 *)p_aencpre_cfg->p_usrdef;
						liba_cbuf_init((t_cbuf_ctrl *)&g_AECrefbuf, (u32)p_aencpre_cfg->p_usrdef0, DA_BUF_SIZE*AEC_BUF_NUM); 
						liba_cbuf_init((t_cbuf_ctrl *)&g_AECinbuf, (u32)p_aencpre_cfg->p_usrdef1, DA_BUF_SIZE*AEC_BUF_NUM); 
						g_aec_enabled = 1;
						syslog(LOG_INFO, "set audio encoder preprocess algo AUDIO_EPRE_AEC sucessfully\n");
						break;
					}
				}
			}

			if(inp->aec_en && g_liba_ecfg.aec_process == NULL){
				syslog(LOG_WARNING, "set audio encoder preprocess algo AUDIO_EPRE_AEC fail, audio aec is not enabled\n");
			}

			if(inp->bDMIC){ //digital MIC use AI0
				ret = irq_request(IRQ_BIT_AI0, (t_irq_handler)arec_irq_handler, NULL);
			}else{
#ifdef AI1REC_AI2PLAY
				ret = irq_request(IRQ_BIT_AI0, (t_irq_handler)arec_irq_handler, NULL);
#else
				ret = irq_request(IRQ_BIT_AI1, (t_irq_handler)arec_irq_handler, NULL);
#endif
			}
			if(ret < 0){
				syslog(LOG_ERR, "ERROR:MPUCMD_AREC_INIT irq_request failed\n");
				return -1;
			}

			arec_started = 0;
			ret = libaenc_init();
			if(ret < 0){
#ifdef AI1REC_AI2PLAY
				irq_free(IRQ_BIT_AI0);
#else
				irq_free(IRQ_BIT_AI1);
#endif
			}
			return ret;

		case MPUCMD_ARECORD_START:
			if(arec_started == 1){
				return 0;
			}
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_START\n");

			libaenc_start();
			arec_started = 1;
			return 0;
		
		case MPUCMD_ARECORD_STOP:
			if(arec_started == 0){
				return 0;
			}
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_STOP\n");

			libaenc_stop();

			arec_started = 0;
			g_aec_enabled  = 0;
			return 0;

		case MPUCMD_ARECORD_EXIT:
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_EXIT\n");

#ifdef AI1REC_AI2PLAY
			irq_free(IRQ_BIT_AI0);
#else
			irq_free(IRQ_BIT_AI1);
#endif

			libaenc_exit();
			return 0;

		case MPUCMD_ARECORD_GETFRM:
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETFRM\n");

			if(arec_started == 0){
				return NULL;
			}
			return (u32)libaenc_get_frm(); 

		case MPUCMD_ARECORD_RELFRM:
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_RELFRM\n");

			libaenc_remove_frm((FRAME*)arg);	
			return 0; 

		case MPUCMD_ARECORD_SETSR: //set sample rate when AI in master
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_SETSR\n");
			g_liba_ecfg.inp.sr = arg;
			if(g_liba_ecfg.inp.bDMIC){
extern void set_dm_sample_rate(u32 sr);
				set_dm_sample_rate(g_liba_ecfg.inp.sr);
			}else if(g_liba_ecfg.inp.aimode==AI_REC_MASTER){
				ai_set_sr(g_liba_ecfg.inp.sr, AI_REC_MODE_SET);
			}


			return 0; 

		case MPUCMD_ARECORD_GETDROPPED:
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETDROPPED\n");
			return 1000*g_liba_ecfg.skipcount*AD_BUF_SIZE/2/g_liba_ecfg.inp.sr;//ms	


		case MPUCMD_ARECORD_GETTOTFRMSINLIST:   ///< arg: NULL, ret:total frames saved to framelist
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETTOTFRMSINLIST\n");
			return total_frames_in;
			
		case MPUCMD_ARECORD_GETTOTFRMSOUTLIST:   ///< arg: NULL, ret:total frames read from framelist 
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETTOTFRMSOUTLIST\n");
			return total_frames_out;

		case MPUCMD_ARECORD_GETTOTLISTBUFSIZE:    ///< arg: NULL, ret:total buffer size of framelist
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETTOTLISTBUFSIZE\n");
			return g_liba_ecfg.outbuf_size; 

		case MPUCMD_ARECORD_GETUSEDLISTBUFSIZE:///< arg: NULL, ret:used buffer size in framelist 
			{
				u32 ret;
				syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETUSEDLISTBUFSIZE\n");
				FRAMELIST *list = g_liba_ecfg.list;
				FRAME *h = (FRAME *)list->frame_head;
				FRAME *t = (FRAME *)list->frame_tail;
				if(t->addr >= h->addr){
					ret = t->addr - h->addr + t->size + t->size1; 
				}else{
					ret = g_liba_ecfg.outbuf_size - (h->addr - t->addr - t->size); 
				}
				return ret; 
			}
		case MPUCMD_ARECORD_GETREFTM:
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_GETREFTM\n");

			if(arec_started == 0){
				return 0;
			}
			return (u32)libaenc_get_reftm();

		case MPUCMD_ARECORD_GETDFMODE: //get dropping frame mode
			if(arec_started == 0){
				return 0;
			}
			return g_liba_ecfg.inp.dfmode;

		case MPUCMD_ARECORD_SETDFMODE: //set dropping frame mode
			if(arec_started == 0){
				return 0;
			}
			g_liba_ecfg.inp.dfmode = arg;
			if((g_liba_ecfg.inp.dfmode == DROPFRM_LATEST_CLEAN) ||
				(g_liba_ecfg.inp.dfmode == DROPFRM_OLDEST_CLEAN)){
				framelist_init(g_liba_ecfg.list); //reset frame list,clean old data
			}

			libaenc_update_max_frm_cnt();
			
			break;

		case MPUCMD_ARECORD_SETOLDFRMDUR: //set linklist saved frames duration
			if(arec_started == 0){
				return 0;
			}
			g_liba_ecfg.inp.listfrm_dur = arg;
			libaenc_update_max_frm_cnt();
			break;

		case MPUCMD_ARECORD_GETOLDFRMDUR: //get current linklist saved frames duration
			if(arec_started == 0){
				return 0;
			}
			return g_liba_ecfg.inp.listfrm_dur;
			
		case MPUCMD_ARECORD_SETADCPPBUFADDR: //set DMIC ADC pingpong buffer address
			g_liba_ecfg.AFI_BUF0 = (u32)arg;
			g_liba_ecfg.AFI_BUF1 = (u32)arg + AD_BUF_SIZE*2;
			syslog(LOG_DEBUG, "MPUCMD_ARECORD_SETADCPPBUFADDR:");
			syslog(LOG_DEBUG, "g_liba_ecfg.AFI_BUF0 = 0x%x\n",g_liba_ecfg.AFI_BUF0);
			return 0;


		default:
			return 0;
	}
	
	return 0;
}

s32 audio_record(void)
{
	s32 err;

	if(arec_started){
		err = libaenc_update_input_internal();
		if(err){
			return 0;
			//	debug_printf("err=%d\n",err);
		}else{
			//encode done, need try encode again.
			return 1;
		}
	}

	return 0;
}

#define TASK_STACKSIZE (1024*4)
static u32 _task_stack_aenc[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_aenc_cfg = {
	.name = "aenc",
	.prio = 15,
	.stk = _task_stack_aenc,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_ARECORD,
	.app_cmdhandler = arec_handle_cmd,
	.app_process = audio_record,
};

static s32 arec_irq_handler(void * arg)
{
	libaenc_irq();
	task_signal(&g_ba2app_aenc_cfg); //let app_process() do
	return 0;
}

