#include "includes.h"

#define LIBA_EHW_DBG


#define AI_TAKE_EFFECT() do{WriteReg32(AI_REC_TAKE_EFFECT, 1);}while(0)

#define ADC_BASE_ADDR	AI1_BASE_ADDR
#define ADC_CHANNEL_ADDR		ADC_BASE_ADDR+0xc
#define ADC_CONTROL			ADC_BASE_ADDR+0x14
#define ADC_BUFSET			ADC_BASE_ADDR+0x20
#define ADC_INT_STATUS		ADC_BASE_ADDR+0x24
#define ADC_INT_MSK			ADC_BASE_ADDR+0x28
#define ADC2_BASE_ADDR	AI2_BASE_ADDR
#define ADC2_CHANNEL_ADDR		ADC2_BASE_ADDR+0xc
#define ADC2_CONTROL			ADC2_BASE_ADDR+0x14
#define ADC2_BUFSET			ADC2_BASE_ADDR+0x20
#define ADC2_INT_STATUS		ADC2_BASE_ADDR+0x24
#define ADC2_INT_MSK			ADC2_BASE_ADDR+0x28

#define AI_REC_RIGHT0_DONE	BIT2
#define AI_REC_RIGHT1_DONE	BIT3
#define AI_REC_LEFT0_DONE	BIT0
#define AI_REC_LEFT1_DONE	BIT1
#define ADC_DONE0	BIT8
#define ADC_DONE1	BIT9

extern int asys_reset;
extern volatile int liba_ehw_started;
extern volatile int liba_dhw_started;

volatile s32 adbuf_sel;
static volatile s32 pcmcnt;


#ifdef I2SIRQ_MEMCOPY
static void packet_pcm( u32 d_addr,u32 s_addr, u32 len)
{
	s16 * s = (s16 *)s_addr;
	s16 * d = (s16 *)d_addr;

	s32 i;

	for(i=0; i<len/2; i++){
		d[2*i] = s[i];
		d[2*i+1] = s[i+len/2];
	}
	return;
}

static void getdata_from_adbufer(u8* src)
{
	s32 ret;
	t_cbuf_ctrl *ecbuf;
	t_cbuf_pos wpos;
	s32 needsz = AD_BUF_SIZE;

	if(g_aec_enabled){
		ecbuf = (t_cbuf_ctrl*)&g_AECinbuf;
	}else{
		ecbuf = &g_liba_ecfg.ibuf;
	}

    ret = liba_cbuf_get_wpos(ecbuf,needsz,&wpos);
    if(g_aec_enabled && (!liba_cbuf_frame_aligned((t_cbuf_ctrl*)&g_AECinbuf)))
    {
        lowlevel_putc('S');
		return;
    }
	if(ret < 0){ 
	//	lowlevel_putc('u');
		syslog(LOG_WARNING,"u");
		g_liba_ecfg.skipcount ++;
	}else{
		if(g_liba_ecfg.inp.ch == 1){ //mono
			if(g_liba_ecfg.inp.aichannel==0){//choose left
				//copy left channel
				memcpy((u8*)wpos.pos0, src, needsz);
			}else{
				//copy right channel data
				memcpy((u8*)wpos.pos0, src+needsz, needsz);
			}
			/*g_liba_ecfg.ibuf length is multiples of AD_BUF_SIZE, 
			so here wpos.len1=0,don't need check */
		}else{ //stereo
			packet_pcm(wpos.pos0, src, needsz/2);
		}
		liba_cbuf_update_wpos(ecbuf,needsz);
	}

}

#endif

void libaenc_irq(void) 
{
	volatile u32 sta;
	s32 ret;

//	lowlevel_putc('I');
#ifndef I2SIRQ_MEMCOPY
	if(adbuf_sel != -1){
		lowlevel_putc('U');
	}
#endif
	sta = ReadReg32(AI_REC_INT_STATUS); //ADC_INT_STATUS is the same register
	if((sta & AI_REC_LEFT0_DONE) || (sta & ADC_DONE0))	{
#ifdef I2SIRQ_MEMCOPY
		getdata_from_adbufer((u8*)g_liba_ecfg.AFI_BUF1);
#else
		adbuf_sel = 0;
#endif
	}else if((sta & AI_REC_LEFT1_DONE)||(sta & ADC_DONE1)){
#ifdef I2SIRQ_MEMCOPY
		getdata_from_adbufer((u8*)g_liba_ecfg.AFI_BUF0);
#else
		adbuf_sel = 1;
#endif
	}else{
		return;
	}

AI_IRQ_EXIT:
	WriteReg32(AI_REC_INT_STATUS, sta);
	if(g_liba_ecfg.inp.bDMIC)
		WriteReg32(ADC_CONTROL, 0x10);
	else if(g_liba_ecfg.inp.aimode==AI_REC_MASTER)
		WriteReg32(AI_REC_CTL, 0x661);
	else
		WriteReg32(AI_REC_CTL, 0x61);

	AI_TAKE_EFFECT();
}


//set MCLK, digital mic use MCLK to output sample rate
//i2sclk is 12MHz, refer to clockconfig_360M.c
void set_dm_sample_rate(u32 sr)
{
	unsigned int regval;

	u32 div4_reg = 0;

	syslog(LOG_INFO, "set_dm_sample_rate\n");

	//set MCLK = ap_i2s_clk_mdiv/mclkdiv = 128*sr , 128 times over sampling 
	//ap_i2s_clk_mdiv = i2sclk*mdiv/sdiv(mdiv/sdiv is div4[30:16]/div4[15:0]
	regval = ReadReg32(REG_SC_DIV7)&0xffffff00; //clear bit7..0
	regval |= BIT6|BIT5|BIT0;
	WriteReg32(REG_SC_DIV7,regval);
	
	/*  128 times over sample
		apclk = 12000000
		need_sr = 8000, real_sr = 8000, step = 32; mode = 375; 		reg = 0x200177
		need_sr = 11025, real_sr = 11025, step = 147; mode = 1250;  reg = 0x9304e2
		need_sr = 12000, real_sr = 12000, step = 16; mode = 125;    reg = 0x10007d
		need_sr = 16000, real_sr = 16000, step = 64; mode = 375;    reg = 0x400177
		need_sr = 22050, real_sr = 22050, step = 147; mode = 625;   reg = 0x930271
		need_sr = 24000, real_sr = 24000, step = 32; mode = 125;    reg = 0x20007d
		need_sr = 32000, real_sr = 32000, step = 128; mode = 375;   reg = 0x800177
		need_sr = 44100, real_sr = 44100, step = 151; mode = 321;   reg = 0x970141
		need_sr = 48000, real_sr = 46875, step = 1; mode = 2;       reg = 0x10002
	*/
	switch(sr){
		case 8000:  div4_reg =0x200177;break;
		case 11025: div4_reg =0x9304e2;break;
		case 12000: div4_reg =0x10007d;break;
		case 16000: div4_reg =0x400177;break;
		case 22050: div4_reg =0x930271;break;
		case 24000: div4_reg =0x20007d;break;
		case 32000: div4_reg =0x800177;break;
		case 44100: div4_reg =0x970141;break;
		case 48000: div4_reg =0x10002 ;break;
		default:
			syslog(LOG_WARNING, "unsupported Digital Mic samplerate(%d),default to 16K\n",sr);
			div4_reg = 0x400177;
			break;
	}

	WriteReg32(REG_SC_DIV4,div4_reg);

	// Global control 2 0xc0001008
	//	[3] Digital MIC & I2S 1:DM1 0:I2S1
	//	[2] Digital Mic & I2S 1:DM0 0:I2S0
	regval = ReadReg32(REG_SC_CTRL2)|BIT2|BIT3|BIT24|BIT25;//master DM&I2S enalbe
	WriteReg32(REG_SC_CTRL2, regval);
	syslog(LOG_DEBUG, "REG_SC_CTRL2=0x%x\n", ReadReg32(REG_SC_CTRL2));
	syslog(LOG_DEBUG, "REG_SC_DIV3=0x%x\n", ReadReg32(REG_SC_DIV3));
	syslog(LOG_DEBUG, "REG_SC_DIV4=0x%x\n", ReadReg32(REG_SC_DIV4));
	syslog(LOG_DEBUG, "REG_SC_DIV7=0x%x\n", ReadReg32(REG_SC_DIV7));
}



int liba_ehw_init(void)
{
	s32 ret = 0;
	if(g_liba_ecfg.inp.bDMIC){
		syslog(LOG_INFO, "liba_ehw_init bDMIC\n");
#if 1
		audio_hw_enable(1,g_liba_ecfg.inp.en_mclk);
#else
		SC_PINSEL2_FUNC(I2S);
		SC_PINSEL2_FUNC(MCLK);
		SC_PIN1_EN(I2S_MCLK);
		SC_PIN1_EN(I2S_DI);
#endif

		SC_CLK2_EN(AC0);
	//	SC_CLK2_EN(AC1);
		SC_RESET2_SET(AC0);
	//	SC_RESET2_SET(AC1);
		SC_RESET2_RELEASE(AC0);
	//	SC_RESET2_RELEASE(AC1);
		set_dm_sample_rate(g_liba_ecfg.inp.sr);
	}else{
		audio_hw_enable(g_liba_ecfg.inp.aimode==AI_CDC_MASTER,g_liba_ecfg.inp.en_mclk);
	}


#ifdef AI1REC_AI2PLAY
	SC_CLK2_EN(AI0);
	SC_RESET1_SET(AI0);
	SC_RESET1_RELEASE(AI0);
	SC_RRESET1_SET(AI0);
	SC_RRESET1_RELEASE(AI0);
#else  //AI2REC_AI1PLAY
	SC_CLK2_EN(AI1);
	SC_RESET1_SET(AI1);
	SC_RESET1_RELEASE(AI1);
	SC_RRESET1_SET(AI1);
	SC_RRESET1_RELEASE(AI1);
#endif

#ifdef AI1REC_AI2PLAY
	if(g_liba_ecfg.inp.aimode==AI_CDC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x2<<AIPSC_BASE));
	}else if(g_liba_ecfg.inp.aimode==AI_REC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0xa<<AIPSC_BASE));
	}else if(g_liba_ecfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x13<<AIPSC_BASE));
	}else{
		syslog(LOG_INFO,"invalid g_liba_ecfg.inp.aimode=0x%x\n ",g_liba_ecfg.inp.aimode);
	}
#else
	if(g_liba_ecfg.inp.aimode==AI_CDC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x0<<AIPSC_BASE));
	}else if(g_liba_ecfg.inp.aimode==AI_REC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x11<<AIPSC_BASE));
	}else if(g_liba_ecfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x8<<AIPSC_BASE));
	}else{
		syslog(LOG_INFO,"invalid g_liba_ecfg.inp.aimode=0x%x\n ",g_liba_ecfg.inp.aimode);
	}
#endif

	syslog(LOG_DEBUG, "ReadReg32(REG_SC_I2SPSC)>>24 =0x%x\n",ReadReg32(REG_SC_I2SPSC)>>24);


#define BADDR 0x90000000
   	if(g_liba_ecfg.inp.bDMIC){

		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<<AIPSC_BASE))|(0x0<<AIPSC_BASE));

		syslog(LOG_DEBUG, "Digital MIC: ReadReg32(REG_SC_I2SPSC)>>24 =0x%x\n",ReadReg32(REG_SC_I2SPSC)>>24);

	   // config AC0/AC1 parameter
		WriteReg32(0xc0010000, 0x442000); //AC0 
	//	WriteReg32(0xc0010400, 0x441af2); //AC1

		WriteReg32(ADC_CHANNEL_ADDR, g_liba_ecfg.AFI_BUF0 - BADDR);
		WriteReg32(ADC_BUFSET, (((g_liba_ecfg.AFI_BUFLEN>>2) << 16)|(g_liba_ecfg.AFI_BUF1 - g_liba_ecfg.AFI_BUF0)));
		 syslog(LOG_ERR, "[0x%x]=0x%x\n", ADC_CHANNEL_ADDR, ReadReg32(ADC_CHANNEL_ADDR));
         syslog(LOG_ERR, "[0x%x]=0x%x\n", ADC_BUFSET, ReadReg32(ADC_BUFSET));
	     syslog(LOG_ERR, "[0x%x]=0x%x\n", ADC_INT_MSK, ReadReg32(ADC_INT_MSK));
	} else {
		/* AI setting */
		if(g_liba_ecfg.inp.aimode==AI_REC_MASTER)
			ret = ai_set_sr(g_liba_ecfg.ai_sr, AI_REC_MODE_SET);

		WriteReg32(AI_REC_LADDR, g_liba_ecfg.AFI_BUF0- BADDR);
		if(g_liba_ecfg.inp.ch == MONO){
#if 1       //right channel not use the same buffer as the left, assuming the data in left and 
			//right channel are different. In order not to affect AEC algorithm, 
			//in AREC_ALLOCATE_STEREO, g_ad_ppbuf size is enlarged to (AD_BUF_SIZE*2*2) 
			//to reserve space for AI right channel data
			WriteReg32(AI_REC_RADDR, (g_liba_ecfg.AFI_BUF0 + g_liba_ecfg.AFI_BUFLEN)- BADDR);
			WriteReg32(AI_REC_BUF_SET, ((g_liba_ecfg.AFI_BUFLEN>>2) << 16) 
									+ (g_liba_ecfg.AFI_BUF1 - g_liba_ecfg.AFI_BUF0));
#else
			//left channel and right channel use the same buffer, assuming the data in left and
			//right channel are the same.
			WriteReg32(AI_REC_RADDR, g_liba_ecfg.AFI_BUF0- BADDR);
			WriteReg32(AI_REC_BUF_SET, (((g_liba_ecfg.AFI_BUFLEN>>2) << 16) 
									+ (g_liba_ecfg.AFI_BUF1 - g_liba_ecfg.AFI_BUF0)));
#endif
		}else if(g_liba_ecfg.inp.ch == STEREO){
			//in AREC_CONFIG_STEREO, .AFI_BUF1 = (u32)g_ad_ppbuf + AD_BUF_SIZE*2 is for MONO case
			//For Stereo,only use half of g_ad_ppbuf, so reset g_liba_ecfg.AFI_BUF1.
			g_liba_ecfg.AFI_BUF1 = g_liba_ecfg.AFI_BUF0 + AD_BUF_SIZE; 
			WriteReg32(AI_REC_RADDR, (g_liba_ecfg.AFI_BUF0 + g_liba_ecfg.AFI_BUFLEN/2)- BADDR);
			WriteReg32(AI_REC_BUF_SET, (((g_liba_ecfg.AFI_BUFLEN>>3) << 16) + (g_liba_ecfg.AFI_BUF1 - g_liba_ecfg.AFI_BUF0)));
		}

#ifdef LIBA_EHW_DBG
		unsigned int regval,regaddr;
		regaddr = AI_REC_MODE_SET;
		regval = ReadReg32(regaddr);
		syslog(LOG_INFO,"0x%x=0x%x\n", regaddr, regval);

		regaddr = AI_REC_LADDR;
		regval = ReadReg32(regaddr);
		syslog(LOG_INFO,"0x%x=0x%x\n", regaddr, regval);

		regaddr = AI_REC_RADDR;
		regval = ReadReg32(regaddr);
		syslog(LOG_INFO,"0x%x=0x%x\n", regaddr, regval);

		regaddr = AI_REC_BUF_SET;
		regval = ReadReg32(regaddr);
		syslog(LOG_INFO,"0x%x=0x%x\n", regaddr, regval);

		regaddr = AI_REC_INT_MSK;
		regval = ReadReg32(regaddr);
		syslog(LOG_INFO,"0x%x=0x%x\n", regaddr, regval);

		syslog(LOG_INFO, "buf1=0x%x, buf0=0x%x\n", g_liba_ecfg.AFI_BUF1, g_liba_ecfg.AFI_BUF0);
#endif
	}

	memset((void*)g_liba_ecfg.AFI_BUF0, 0, g_liba_ecfg.AFI_BUFLEN);
	memset((void*)g_liba_ecfg.AFI_BUF1, 0, g_liba_ecfg.AFI_BUFLEN);

	adbuf_sel = -1;
	pcmcnt =0;
#ifdef LIBA_EHW_DBG
	syslog(LOG_INFO, "audio record hardware init OK!\n");
#endif
	liba_ehw_started = 1;
	if(!asys_reset)
		asys_reset = 1;

#if 1
//	memset((void*)g_liba_ecfg.outbuf_addr,0,g_liba_ecfg.outbuf_size);

	//memset((void*)g_liba_ecfg.ibuf.start,0,g_liba_ecfg.ibuf.len);
	//g_liba_ecfg.ibuf_wp = AD_BUF_SIZE;
	//g_liba_ecfg.ibuf_rp = 0;
	liba_cbuf_reset(&g_liba_ecfg.ibuf);
#endif
	return ret;
}

int liba_ehw_start(void)
{
//	_irq_arec_enable();

#if 0
	//move these to liba_ehw_init
	memset((void*)g_liba_ecfg.outbuf_addr,0,g_liba_ecfg.outbuf_size);

	//memset((void*)g_liba_ecfg.ibuf.start,0,g_liba_ecfg.ibuf.len);
	//g_liba_ecfg.ibuf_wp = AD_BUF_SIZE;
	//g_liba_ecfg.ibuf_rp = 0;
	liba_cbuf_reset(&g_liba_ecfg.ibuf);
#endif

	if(g_liba_ecfg.inp.bDMIC){
		WriteReg32(ADC_CONTROL, 0x10);
		WriteReg32(ADC_INT_MSK, 0xff);
	}
	else{
		if(g_liba_ecfg.inp.aimode==AI_REC_MASTER)
			WriteReg32(AI_REC_CTL, 0x661);
		else
			WriteReg32(AI_REC_CTL, 0x61);	//negedge and no delay
		WriteReg32(AI_REC_INT_MSK, 0xfc);
	}

	AI_TAKE_EFFECT();

	//keep ADC VMID current in low status:4uA 
	WriteReg32(AUDIO_BASE_ADDR+0x14, ReadReg32(AUDIO_BASE_ADDR+0x14)&~(0xff0000)); //WriteReg8(AUDIO_BASE_ADDR + 0x16,0x00);	

	return 0;
}

int liba_ehw_stop(void)
{
//	WriteReg32(AI_REC_INT_MSK, 0xff);
	WriteReg32(AI_REC_CTL, 0x0);	
	AI_TAKE_EFFECT();

//	_irq_arec_disable();
	liba_ehw_started = 0;
	if(liba_dhw_started == 0){
		asys_reset = 0;
	}


	return 0;
}

int liba_ehw_exit(void)
{
#ifdef AI1REC_AI2PLAY
	SC_RESET1_SET(AI0);
	SC_RRESET1_SET(AI0);
	SC_CLK2_DISABLE(AI0);
#else //AI2REC_AI1PLAY
	SC_RESET1_SET(AI1);
	SC_RRESET1_SET(AI1);
	SC_CLK2_DISABLE(AI1);
#endif

	audio_hw_disable(g_liba_ecfg.inp.aimode==AI_CDC_MASTER,g_liba_ecfg.inp.en_mclk);
	return 0;
}


