#include "includes.h"
//#include "liba_enc.h"
//#include "liba_enc_internal.h"


//t_liba_ecfg g_liba_ecfg;
static volatile u32 need_notempty_evt;
static volatile u32 arecevt_set;
volatile u32 g_askipcount;
t_aencoder_cfg *g_aencoder_cfg = NULL;
static u8 s_arec_outbuf[OUTBUF_ONELEN];
static u8 s_arec_inbuf[MAX_INBUFLEN];
static s32 drop_samples;
static u32 frmlock = 0;
static u32 max_frm_cnt;  //the max old frame number in linklist

u32 total_frames_in, total_frames_out;
extern volatile s32 adbuf_sel;

typedef struct {
	s32 y2;
	s32 y1;
	s32 x0;
	s32 x1;
}t_hpf_coef;

static t_hpf_coef hpf;

#define WC 5
#define PI 3.1415926535897932384626433832795
#define SQRT_2 1.4142135623730950488016887242097

static s32 F_MUL64_RSHIFT(s32 x,s32 y,s32 n)
{
#if 1
	return ((s64)x * y) >> n;
#else
		s32 temp = 0,result;
		asm("b.mulh %0,%1,%2;\
			b.mul %3,%1,%2;\
			b.srl %3,%3,%4;\
			b.sll %0,%0,%5;\
			b.add %0,%0,%3":\
			"=&r"(result):\
			"r"(x),"r"(y),"r"(temp),"r"(n),"r"(32-n));
		return result;
#endif
}	
static s32 rm_dc_offset(s16 *data,s32 len)
{
	t_hpf_coef *st = &hpf;
    s32 i, x2;
    s32 L_tmp;
#define QCOEF 28
#define Q_PREP_COEF (1<<QCOEF)
#define Q16 (1<<16) 
	
#if 1
#define C 0.0019634979317947679//(tan((double)PI*WC/8000))
#define B0 (1.0/(1.0+SQRT_2*C+C*C)) //0.99722704990447017088698334162665
#define  B1 (-2*B0) //-1.9944540998089403417739666832533
#define  B2 B0
#define  A1 (-(2*B0*(C*C-1))) //1.9944464105419268360843161790007
#define  A2 (-(B0*(1-SQRT_2*C+C*C))) //-0.99446178907595384746361718750587
#else
double  C = tan(PI*WC/8000);
double  B0 = (1.0/(1.0+SQRT_2*C+C*C));
double  B1 = (-2*B0);
double  B2 = B0;
double  A1 = -(2*B0*(C*C-1));
double  A2 = -(B0*(1-SQRT_2*C+C*C));
#endif
	//int a11 = (int)(1.906005859*Q_PREP_COEF+0.5);
	//int a22 = (int)(-0.911376953*Q_PREP_COEF+0.5);
	//int b00 = (int)(0.4636230465*2*Q_PREP_COEF+0.5);
	//int b11 = (int)(-0.92724705*2*Q_PREP_COEF+0.5);
	//int b22 = (int)(0.4636234515*2*Q_PREP_COEF+0.5);
	s32 a1 = (s32)(A1*Q_PREP_COEF+0.5);
	s32 a2 = (s32)(A2*Q_PREP_COEF+0.5);
	s32 b0 = (s32)(B0*Q_PREP_COEF+0.5);
	s32 b1 = (s32)(B1*Q_PREP_COEF+0.5);
    s32 b2 = (s32)(B2*Q_PREP_COEF+0.5);
	
    for (i = 0; i < len; i++) {
		
        x2 = st->x1;
        st->x1 = st->x0;
        st->x0 = data[i];
		{
		//	L_tmp = ((long long)st->y1 * a1) >>(16 + QCOEF - 16) ;
			L_tmp = F_MUL64_RSHIFT(st->y1,a1,QCOEF);
		//	L_tmp += ((long long)st->y2 * a2) >>(16 + QCOEF - 16) ;
			L_tmp += F_MUL64_RSHIFT(st->y2,a2,QCOEF);
		//	L_tmp += ((long long)st->x0 * b0) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(st->x0,b0,QCOEF - 16);
		//	L_tmp += ((long long)st->x1 * b1) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(st->x1,b1,QCOEF - 16);
		//	L_tmp += ((long long)x2 * b2) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(x2,b2,QCOEF - 16);
			data[i] = L_tmp >> 16;
			
			st->y2 = st->y1;
			st->y1 = L_tmp;
		}
    }
    return 0;
}


/*
 * get current reference timer value
 */
u32 libaenc_get_reftm(void)
{
	u32 cbufdl = liba_cbuf_getdatalen(&g_liba_ecfg.ibuf); 
	return get_ms() - g_liba_ecfg.reftm * 10 - (cbufdl*1000/2/g_liba_ecfg.inp.sr);
}

void libaenc_list_reset(void)
{
	framelist_init(g_liba_ecfg.list);
	return;
}


static u32 calc_frm_cnt(u32 *dur)
{
	u32 frm_num;
	u32 listnum = framelist_num(g_liba_ecfg.list);
	if(dur){
		frm_num = *dur*2*g_liba_ecfg.inp.sr/g_aencoder_cfg->infrmsz/1000;
		if(frm_num > listnum){
			syslog(LOG_ERR, "Error:expected dur is %d ms(%d frames)\n",*dur,frm_num);
			syslog(LOG_ERR, "Error:total arec framelist num(%d) is not enough\n",listnum);
			frm_num = listnum;
			*dur = frm_num*g_aencoder_cfg->infrmsz*1000/2/g_liba_ecfg.inp.sr;

			syslog(LOG_ERR, "Arec set listfrm_dur to %d ms\n",*dur);
		}
	}else{
		frm_num = listnum;
	}
	return frm_num;
}


void libaenc_update_max_frm_cnt(void)
{
	u32 new_num,cur_cnt;


	//if DROP LATEST, use all audio frm num in linklist
	if((g_liba_ecfg.inp.dfmode == DROPFRM_LATEST_CLEAN) ||
		(g_liba_ecfg.inp.dfmode == DROPFRM_LATEST)){
			//new_num = framelist_num(g_liba_ecfg.list);
			new_num = calc_frm_cnt(0);
	}

	//if DROP OLDEST, use pre-set frm num in linklist determined by 
	//g_liba_ecfg.inp.listfrm_dur
	if((g_liba_ecfg.inp.dfmode == DROPFRM_OLDEST_CLEAN) ||
		(g_liba_ecfg.inp.dfmode == DROPFRM_OLDEST)){
			new_num = calc_frm_cnt(&g_liba_ecfg.inp.listfrm_dur);
	}


	cur_cnt = framelist_cnt(g_liba_ecfg.list);
	if(new_num < cur_cnt){//need drop frames here
		FRAME *head = NULL;
		u32 tmp = cur_cnt - new_num;
		while(tmp--){
			head = libaenc_get_frm();
			if(head){
				libaenc_remove_frm(head);
				syslog(LOG_INFO,"R");
			}else{
				syslog(LOG_ERR,"Failed to get frame to drop\n");
				return 0;
			}
		}
	}

	max_frm_cnt = new_num;
}

/**
 * @brief add one block of encoded audio to audio framelist
 * @details called by epost process function after encodingone audio frame
 * @param addr: address storing one block of audio data, it
 is got by calling libadec_outbuf_get.
 * @param size: data size in byte
 * @return 0:ok, <0:err
 */
static int libaenc_frm_add(FRAME *frm)
{
	FRAME *new_frm;
//	u32 flags;

//	spin_lock_irqsave(flags);
	new_frm = framelist_alloc(g_liba_ecfg.list);
	if(new_frm == NULL){
		syslog(LOG_ERR, "libaenc_frm_add framelist_alloc failed\n");
//		spin_unlock_irqrestore(flags);
		return -1;
	}

	memcpy(new_frm,frm,sizeof(FRAME));
	new_frm->status = FRAME_STATUS_READY;
	new_frm->ts = libaenc_get_reftm();
	new_frm->next = NULL;

	framelist_add(g_liba_ecfg.list, new_frm);
	if(need_notempty_evt){
		need_notempty_evt= 0;

		syslog(LOG_DEBUG, "libaenc set evt\n");

		libmpu_set_event(MPUEVT_AENC_NOTEMPTY);
		arecevt_set = 1;
	}

		total_frames_in++; 
//	spin_unlock_irqrestore(flags);
	return 0;
}

int libaenc_remove_frm(FRAME *frm)
{
	s32 ret;	
	if(!frmlock){
		return 0;
	}
		
	ret = framelist_free(g_liba_ecfg.list, frm, 0,0);
	if(ret < 0){
		syslog(LOG_ERR, "libaenc_remove_frm failed\n");	
	}
	else{
		if(arecevt_set){

			syslog(LOG_DEBUG, "libaenc clear evt\n");

			libmpu_clear_event(MPUEVT_AENC_NOTEMPTY);
			arecevt_set = 0;
		}
		total_frames_out++; 
	}
	frmlock = 0;

	return ret;
}

FRAME* libaenc_get_frm(void)
{
	FRAME *ret;
	if(frmlock){
		return 0;
	}
	ret = framelist_get(g_liba_ecfg.list);
	if(ret == NULL){
		syslog(LOG_DEBUG, "libaenc_get_frm failed\n");	

		need_notempty_evt = 1;
		return 0;
	}
	frmlock = 1;
	return ret;
}



static u32 libaenc_outbuf_get(void)
{
	u32 ret;
	u32 cnt;
	u32 leftbufsz;
	FRAMELIST *list = g_liba_ecfg.list;
	volatile FRAME *tail = NULL;
	volatile FRAME *head = NULL;

	cnt = framelist_cnt(list);
	if(cnt >= max_frm_cnt){
		if((g_liba_ecfg.inp.dfmode == DROPFRM_OLDEST)||
			(g_liba_ecfg.inp.dfmode == DROPFRM_OLDEST_CLEAN)){
			//drop the oldest frame
			head = libaenc_get_frm();
			if(head){
				libaenc_remove_frm(head);
				syslog(LOG_INFO,"D");
			}else{
				return 0;
			}
		}else if((g_liba_ecfg.inp.dfmode == DROPFRM_LATEST) ||
			(g_liba_ecfg.inp.dfmode == DROPFRM_LATEST_CLEAN)){
			//drop the latest data
			return 0;
		}
	}
	if(cnt == 0){
		ret = g_liba_ecfg.outbuf_addr;
	}else{
		tail = list->frame_tail;
		head = list->frame_head;
		if(head->addr > tail->addr){
			leftbufsz = head->addr - tail->addr - tail->size;	
		}else{
			leftbufsz = g_liba_ecfg.outbuf_size - (tail->addr - head->addr + tail->size + tail->size1); 
		}
		if(leftbufsz < g_aencoder_cfg->outfrmsz)  //left buffer size is not enough for a maximum frame
			return 0;

		if(tail->addr1){
			ret = tail->addr1 + tail->size1;
		}else{
			ret = tail->addr + tail->size;
		}

	}

	return ret;
}

#if 0
static void packet_pcm(unsigned int s_addr, unsigned int d_addr, unsigned int len)
{
	short * s = (short *)s_addr;
	short * d = (short *)d_addr;

	int i;

	for(i=0; i<len/2; i++){
		d[2*i] = s[i];
		d[2*i+1] = s[i+len/2];
	}
	return;
}
#endif

static void copy_bitstream_to_linkbuf(u32 inbuf,u32 len, u32 outbuf, FRAME *frm)
{
	if(outbuf + len < g_liba_ecfg.outbuf_addr + g_liba_ecfg.outbuf_size){
		memcpy((u8*)outbuf, (u8*)inbuf, len);
		frm->addr = (u32)outbuf;
		frm->size = len;
	}else{
		frm->addr = (u32)outbuf;
		frm->size = g_liba_ecfg.outbuf_addr + g_liba_ecfg.outbuf_size - (u32)outbuf;
		frm->addr1= g_liba_ecfg.outbuf_addr;
		frm->size1= len - frm->size;
		memcpy((u8*)frm->addr,(u8*)inbuf,frm->size);
		memcpy((u8*)frm->addr1,(u8*)inbuf+frm->size,frm->size1);
	}
}

void do_aec_process(void)
{
	t_cbuf_pos rin;
	t_cbuf_pos ref;
	t_cbuf_pos w;

#if 0
	while(liba_cbuf_get_rpos((t_cbuf_ctrl*)&g_AECinbuf, AD_BUF_SIZE, &rin)==0 &&
		liba_cbuf_get_rpos((t_cbuf_ctrl*)&g_AECrefbuf, DA_BUF_SIZE, &ref)==0 &&
		liba_cbuf_get_wpos(&g_liba_ecfg.ibuf, AD_BUF_SIZE, &w)==0){

		if(g_liba_ecfg.aec_process){
			g_liba_ecfg.aec_process((void*)w.pos0, (void*)rin.pos0, (void*)ref.pos0, AD_BUF_SIZE);
			liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECinbuf, AD_BUF_SIZE);
			liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECrefbuf, DA_BUF_SIZE);
			liba_cbuf_update_wpos(&g_liba_ecfg.ibuf, AD_BUF_SIZE);
//			lowlevel_putc('>');
		}
	}
#else

	u32 aec_in, aec_ref;
	u32 flags;

    while(1)
    {
    	//get in & ref buffer
        aec_in = aec_ref = 0;

        spin_lock_irqsave(flags);
        if(liba_cbuf_get_rpos((t_cbuf_ctrl*)&g_AECinbuf, AD_BUF_SIZE, &rin)==0)
        {
            if(rin.len1)
            {
                liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECinbuf, AD_BUF_SIZE);
                syslog(LOG_DEBUG, "aec in buffer != AD_BUF_SIZE*N\n");
            }
            else
            {
                aec_in = rin.pos0;
            }

            if(liba_cbuf_get_rpos((t_cbuf_ctrl*)&g_AECrefbuf, DA_BUF_SIZE, &ref)==0)
            {
                if(ref.len1)
                {
                    liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECrefbuf, DA_BUF_SIZE);
                    syslog(LOG_DEBUG, "aec ref buffer != DA_BUF_SIZE*N\n");
                }
                else
                {
                    aec_ref = ref.pos0;
                }
            }
        }
        spin_unlock_irqrestore(flags);

        //aec process
        if((g_liba_ecfg.aec_process) && aec_ref && aec_in)
        {
            if(liba_cbuf_get_wpos(&g_liba_ecfg.ibuf, AD_BUF_SIZE, &w)==0)
            {
                if(w.len1)
                {
                    g_liba_ecfg.aec_process(NULL, (void*)aec_in, (void*)aec_ref, AD_BUF_SIZE);
                    syslog(LOG_DEBUG, "enc in buffer != AD_BUF_SIZE*N\n");
                }
                else
                {
                    g_liba_ecfg.aec_process((void*)w.pos0, (void*)aec_in, (void*)aec_ref, AD_BUF_SIZE);
                }
                liba_cbuf_update_wpos(&g_liba_ecfg.ibuf, AD_BUF_SIZE);
            }
            else
            {
                g_liba_ecfg.aec_process(NULL, (void*)aec_in, (void*)aec_ref, AD_BUF_SIZE);
            }

            spin_lock_irqsave(flags);

            liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECinbuf, AD_BUF_SIZE);
            liba_cbuf_update_rpos((t_cbuf_ctrl*)&g_AECrefbuf, DA_BUF_SIZE);

            spin_unlock_irqrestore(flags);

//          lowlevel_putc('>');
        }
        else
        {
            break;
        }
    }
#endif
}

static void decimation(s16* dest,t_cbuf_pos* rpos)
{
	s32 i;
	s16 *d = dest;
	s16 *s = (s16*)rpos->pos0;
	if(g_liba_ecfg.inp.ch == 1){//mono
		u32 buf0_len = rpos->len0/2; //buf0 samples
		for(i=0;i < buf0_len/g_liba_ecfg.inp.deci_times; i++){
			*d++ = *s;
			s += g_liba_ecfg.inp.deci_times;
		}

		if(rpos->len1){
			u32 buf1_len = rpos->len1/2; //buf1 samples
			u32 res = buf0_len%g_liba_ecfg.inp.deci_times; 
			if(res){
				*d++ = *s;
				s = (s16*)rpos->pos1;
				s += g_liba_ecfg.inp.deci_times - res;
			}else{
				s = (s16*)rpos->pos1;
			}
			for(i=0;i < buf1_len/g_liba_ecfg.inp.deci_times; i++){
				*d++ = *s;
				s += g_liba_ecfg.inp.deci_times;
			}
		}
	}else{//Stereo
		u32 buf0_len = rpos->len0/2/2; //buf0 samples for each channel
		for(i=0;i < buf0_len/g_liba_ecfg.inp.deci_times; i++){
			*d++ = *s;
			*d++ = *(s+1);
			s += g_liba_ecfg.inp.deci_times*2;
		}

		if(rpos->len1){
			u32 buf1_len = rpos->len1/2/2; //buf1 samples for each channel
			u32 res = buf0_len%g_liba_ecfg.inp.deci_times; 
			if(res){
				*d++ = *s;
				*d++ = *(s+1);
				s = (s16*)rpos->pos1;
				s += (g_liba_ecfg.inp.deci_times - res)*2;
			}else{
				s = (s16*)rpos->pos1;
			}
			for(i=0;i < buf1_len/g_liba_ecfg.inp.deci_times; i++){
				*d++ = *s;
				*d++ = *(s+1);
				s += g_liba_ecfg.inp.deci_times*2;
			}
		}
	}
}

#ifndef I2SIRQ_MEMCOPY
static void packet_pcm( u32 d_addr,u32 s_addr, u32 len)
{
	s16 * s = (s16 *)s_addr;
	s16 * d = (s16 *)d_addr;

	s32 i;

	for(i=0; i<len/2; i++){
		d[2*i] = s[i];
		d[2*i+1] = s[i+len/2];
	}
	return;
}

static void getdata_from_adbufer(void)
{
	s32 ret;
	t_cbuf_ctrl *ecbuf;
	t_cbuf_pos wpos;
	s32 needsz = AD_BUF_SIZE;
	u8* src;

	if(adbuf_sel == 0){
		src = (u8*)g_liba_ecfg.AFI_BUF1;
	}else if (adbuf_sel == 1){
		src = (u8*)g_liba_ecfg.AFI_BUF0;
	}else{
		return;
	}


	if(g_aec_enabled){
		ecbuf = (t_cbuf_ctrl*)&g_AECinbuf;
	}else{
		ecbuf = &g_liba_ecfg.ibuf;
	}

    ret = liba_cbuf_get_wpos(ecbuf,needsz,&wpos);
    if(g_aec_enabled && (!liba_cbuf_frame_aligned((t_cbuf_ctrl*)&g_AECinbuf)))
    {
        lowlevel_putc('S');
		return;
    }
	if(ret < 0){ 
		syslog(LOG_DEBUG,"u");
	}else{
		if(g_liba_ecfg.inp.ch == 1){ //mono
			if(g_liba_ecfg.inp.aichannel==0){//choose left
				//copy left channel
				memcpy((u8*)wpos.pos0, src, needsz);
			}else{
				//copy right channel data
				memcpy((u8*)wpos.pos0, src+needsz, needsz);
			}
			/*g_liba_ecfg.ibuf length is multiples of AD_BUF_SIZE, 
			so here wpos.len1=0,don't need check */
		}else{ //stereo
			packet_pcm(wpos.pos0, src, needsz/2);
		}
		liba_cbuf_update_wpos(ecbuf,needsz);
		adbuf_sel = -1;
	}

}
#endif

int libaenc_update_input_internal(void)
{
	u32 out_addr;
	s32 len = 0;
	s32 enclen;
	u32 flags;
	FRAME nfrm;
	t_cbuf_pos rpos;
	t_cbuf_ctrl *ecbuf = &g_liba_ecfg.ibuf; 
	s32 ret;
	u32 in_addr;
	s32 inlen = 0;
	
#ifndef I2SIRQ_MEMCOPY
//    spin_lock_irqsave(flags);
	getdata_from_adbufer();
//	spin_unlock_irqrestore(flags);
#endif

	if(g_aec_enabled){
		do_aec_process();
	}

 	out_addr = libaenc_outbuf_get();
	if(out_addr==0){
		return -1;
	}

	len = g_aencoder_cfg->infrmsz * g_liba_ecfg.inp.ch;
	if(g_liba_ecfg.inp.deci_en){
		//need decimation, source data need double
		inlen = len*g_liba_ecfg.inp.deci_times;
	}else{
		inlen = len;
	}

	ret = liba_cbuf_get_rpos(ecbuf, inlen, &rpos);
	if(ret < 0){
		syslog(LOG_DEBUG, "arec.c: liba_cbuf_get_rpos() return -1\n");
		return -2;
	}

	in_addr = (u32)&s_arec_inbuf;
	memset((u8*)in_addr,0,sizeof(s_arec_inbuf));

	spin_lock_irqsave(flags);

	if(g_liba_ecfg.inp.deci_en){
		//decimation and copy decimated data to inbuf	
		decimation((s16*)in_addr, &rpos);
	}else{
		memcpy((u8*)in_addr,(u8*)rpos.pos0, rpos.len0);	
		if(rpos.len1){
			memcpy((u8*)in_addr + rpos.len0,(u8*)rpos.pos1, rpos.len1);	
		}
	}
	liba_cbuf_update_rpos(ecbuf,inlen);

	if(drop_samples){
		if(drop_samples*2 > len){
			memset((u8*)in_addr, 0 ,len);	
			drop_samples -= len/2;
		}else{
			memset((u8*)in_addr, 0 ,drop_samples*2);	
			drop_samples = 0;
		}
	}

	spin_unlock_irqrestore(flags);

	if(g_liba_ecfg.inp.rmdc_en){
		rm_dc_offset((s16*)in_addr,len/2);
	}

	if(g_liba_ecfg.pre_process){
		g_liba_ecfg.pre_process((void*)in_addr,len);
	}
	
	if(g_liba_ecfg.inp.dvol){
		liba_volume_control((void*)in_addr,len/2,g_liba_ecfg.inp.dvol);
	}
		
	memset(&nfrm,0,sizeof(FRAME));
	enclen = g_aencoder_cfg->process((void *)in_addr, &len,s_arec_outbuf);
	if(enclen){	
		copy_bitstream_to_linkbuf((u32)s_arec_outbuf, enclen, out_addr, &nfrm);
		libaenc_frm_add(&nfrm);
	}else{
		return -3;
	}
	return 0;
}



extern u32 aencoder_cfg_start;
extern u32 aencoder_cfg_end;

static s32 libaenc_setfmt(E_AFMT afmt)
{
	g_aencoder_cfg = (t_aencoder_cfg *)(&aencoder_cfg_start) ;

	for(; g_aencoder_cfg < (t_aencoder_cfg *)(&aencoder_cfg_end) ; g_aencoder_cfg ++){
		if(g_aencoder_cfg->afmt == afmt){
			syslog(LOG_INFO, "libaenc_setfmt set format %d sucessfully\n",afmt);
			return 0;
		}
	}

#if 0
	//set defaulti
	g_aencoder_cfg = (t_aencoder_cfg *)(&aencoder_cfg_start) ;
#endif

	return -1;
}


int libaenc_init(void)
{
	u32 listnum;
	u32 process_len;

	frmlock = 0;
	need_notempty_evt = 0;
	arecevt_set = 0;

	if(g_liba_ecfg.inp.drop_samples == 0){
		//mute the first 200 ms
		g_liba_ecfg.inp.drop_samples = g_liba_ecfg.inp.sr/5; 
	}
	drop_samples = g_liba_ecfg.inp.drop_samples;

	if(libaenc_setfmt(g_liba_ecfg.inp.fmt) < 0){
		syslog(LOG_ERR, "libaenc_setfmt set format %d failed\n",g_liba_ecfg.inp.fmt);
		return -1;
	}

	g_aencoder_cfg->init(g_liba_ecfg.inp.algfrmsz);
	libaenc_list_reset();

	process_len = g_aencoder_cfg->infrmsz * g_liba_ecfg.inp.ch;
	if(process_len > MAX_INBUFLEN){
		syslog(LOG_ERR, "Err: MPU arec.c process_len > MAX_INBUFLEN\n");
		syslog(LOG_ERR, "Please set g_liba_ecfg.inp.algfrmsz smaller\n");
		return -1;
	}

	g_liba_ecfg.ai_sr = g_liba_ecfg.inp.sr;
	if(g_liba_ecfg.inp.deci_en){
		//decimation, hardware sample rate should be doulbed.
		g_liba_ecfg.ai_sr = g_liba_ecfg.inp.sr*g_liba_ecfg.inp.deci_times;
		process_len *= g_liba_ecfg.inp.deci_times;
	}

	if(process_len + AD_BUF_SIZE >= g_liba_ecfg.ibuf.len){
		syslog(LOG_ERR, "Err:MPU arec.c process_len + AD_BUF_SIZE >= g_liba_ecfg.ibuf.len\n");
		syslog(LOG_ERR, "Please set g_liba_ecfg.inp.algfrmsz smaller\n");
		return -1;
	}

	//max_frm_cnt = calc_frm_cnt();
	libaenc_update_max_frm_cnt();

	memset(&hpf, 0, sizeof(hpf));
	if(g_liba_ecfg.pre_init){
		if(g_liba_ecfg.pre_init() < 0){
			syslog(LOG_ERR, "g_liba_ecfg.pre_init failed\n");
			return -1;
		}

	}

	if(g_liba_ecfg.aec_init){
		g_liba_ecfg.aec_init();
	}

	if(liba_ehw_init() < 0){
		syslog(LOG_ERR, "liba_ehw_init failed\n");
		return -1;
	}

	g_liba_ecfg.skipcount = 0;
	g_askipcount = 0;

	total_frames_in = 0;
	total_frames_out = 0;

#ifdef TEST_AACENCODER
	pcmcnt = 0;
#endif

	return 0;
}

int libaenc_start(void)
{
	liba_ehw_start();
	g_liba_ecfg.reftm = ticks;

	return 0;
}

int libaenc_stop(void)
{
	liba_ehw_stop();
	memset((u8 *)g_liba_ecfg.AFI_BUF0, 0, g_liba_ecfg.AFI_BUFLEN);
	memset((u8 *)g_liba_ecfg.AFI_BUF1, 0, g_liba_ecfg.AFI_BUFLEN);

	if(g_liba_ecfg.aec_exit){
		g_liba_ecfg.aec_exit();
	}

	syslog(LOG_INFO, "audiolib recorder stop!!\n");

	return 0;
}

int libaenc_exit(void)
{
	framelist_init(g_liba_ecfg.list);
	syslog(LOG_INFO, "libaenc_exit\n");
	return 0;
}


