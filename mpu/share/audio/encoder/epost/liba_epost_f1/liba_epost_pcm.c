#include "includes.h"
//#include "liba_enc.h"
#include "liba_enc_internal.h"

#define PCM_ONCE_OUTPUT_LEN 512

static int pcm_epost_process(void *inbuf, int* len, void *outbuf);
static int pcm_epost_init(u32 frmsz);

__aencoder_cfg t_aencoder_cfg g_aencoder_f1_cfg = {
	.afmt = AUDIO_FMT_F1,
	.infrmsz = PCM_ONCE_OUTPUT_LEN,
	.outfrmsz = PCM_ONCE_OUTPUT_LEN,
	.init = pcm_epost_init,
	.process = pcm_epost_process,
};

static int pcm_epost_process(void *inbuf, int* len, void *outbuf)
{
	
	if(*len != g_aencoder_f1_cfg.infrmsz*g_liba_ecfg.inp.ch){
		syslog(LOG_ERR, "pcm_epost_process error,*len = %d,not equal to frmsz(%d)\n",*len,g_aencoder_f1_cfg.infrmsz*g_liba_ecfg.inp.ch);
		return -1;
	}
	memcpy((u8*)outbuf, (u8*)inbuf, g_aencoder_f1_cfg.infrmsz*g_liba_ecfg.inp.ch);
	*len = 0;
	return g_aencoder_f1_cfg.infrmsz*g_liba_ecfg.inp.ch;

}


static int pcm_epost_init(u32 frmsz){

	if(frmsz){
		g_aencoder_f1_cfg.infrmsz = frmsz; 
		g_aencoder_f1_cfg.outfrmsz = frmsz; 
	}

	return 0;
}


