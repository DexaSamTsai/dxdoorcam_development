#include "includes.h"

//#define R3_SEND_MCLK
int asys_reset;
static int ahw_en = 0;
volatile int liba_ehw_started = 0;
volatile int liba_dhw_started = 0;
u8 *g_aec_refbuf = NULL;
volatile t_cbuf_ctrl g_AECrefbuf;
volatile t_cbuf_ctrl g_AECinbuf;
volatile u32 g_aec_enabled = 0;

u32 i2sclk = 30000000;

void liba_cbuf_init(t_cbuf_ctrl *cbuf, u32 bufstart, u32 len) 
{
	cbuf->start = bufstart;
	cbuf->rp = 0;
	cbuf->wp = 0;
	cbuf->len = len;
	cbuf->seq = 0;
}

void liba_cbuf_reset(t_cbuf_ctrl *cbuf) 
{
	
	cbuf->rp = 0;
	cbuf->wp = 0;
	cbuf->seq = 0;
	if(cbuf->start){
		memset((void*)cbuf->start,0,cbuf->len);
	}
}


u32 liba_cbuf_getdatalen(t_cbuf_ctrl *cbuf) 
{
	u32 datasize;
	u32 rp = cbuf->rp;
	u32 wp = cbuf->wp;

	if(rp <= wp){ //if rp==wp, buffer empty, datasize = 0
		datasize = wp - rp;
	
	}else{ 		
		datasize = cbuf->len - rp + wp;
	}
	return datasize;
}

s32 liba_cbuf_get_wpos(t_cbuf_ctrl *cbuf, u32 needspace, t_cbuf_pos *w)
{
	u32 freesize;
	u32 rp = cbuf->rp;
	u32 wp = cbuf->wp;

	if(rp > wp){
		freesize = rp - wp;
		if(freesize > needspace){
			w->pos0 = cbuf->start + wp;
			w->len0 = needspace;
			w->pos1 = 0;
			w->len1 = 0;
			return 0;
		}
	}else{//if rp==wp, buf is empty
		freesize = cbuf->len - wp + rp;
		if(freesize > needspace){
			if((cbuf->len - wp) >= needspace){
				w->pos0 = cbuf->start + wp;
				w->len0 = needspace;
				w->pos1 = 0;
				w->len1 = 0;
				return 0;
			}else{
				w->pos0 = cbuf->start + wp;
				w->len0 = cbuf->len - wp;
				w->pos1 = cbuf->start;
				w->len1 = needspace - w->len0;
				return 0;
			}
		}
	}

	return -1;
}

s32 liba_cbuf_get_rpos(t_cbuf_ctrl *cbuf, u32 needsize,t_cbuf_pos *r)
{
	u32 datasize;
	u32 rp = cbuf->rp;
	u32 wp = cbuf->wp;

	if(rp <= wp){ //if rp==wp, buffer empty, datasize = 0
		datasize = wp - rp;
		if(datasize >= needsize){
			r->pos0 = cbuf->start + rp;
			r->len0 = needsize;
			r->pos1 = 0;
			r->len1 = 0;
			return 0;
		}
	}else{ 		
		datasize = cbuf->len - rp + wp;
		if(datasize >= needsize){
			if((cbuf->len - rp) >= needsize){
				r->pos0 = cbuf->start + rp;
				r->len0 = needsize;
				r->pos1 = 0;
				r->len1 = 0;
				return 0;
			}else{
				r->pos0 = cbuf->start + rp;
				r->len0 = cbuf->len - rp;
				r->pos1 = cbuf->start;
				r->len1 = needsize - r->len0;
				return 0;
			}
		}
	}
	return -1;
}

void liba_cbuf_update_rpos(t_cbuf_ctrl *cbuf, u32 size)
{
	u32 rp = cbuf->rp;
	rp += size;
	if(rp >= cbuf->len){
		rp -= cbuf->len;
	}
	cbuf->rp = rp;
}

void liba_cbuf_update_wpos(t_cbuf_ctrl *cbuf, u32 size)
{
	u32 wp = cbuf->wp;
	wp += size;
	if(wp >= cbuf->len){
		wp -= cbuf->len;
	}
	cbuf->wp = wp;
	cbuf->seq++;
}

int liba_cbuf_frame_aligned(t_cbuf_ctrl *cbuf)
{
	//dac runs first
	//the adc runs
    //they run at same step
	if(cbuf == (&g_AECrefbuf))
	{
	    //dac
	    if(cbuf->seq > g_AECinbuf.seq)          return 0;
	}
	else if(cbuf == (&g_AECinbuf))
	{
	    //adc
        if(cbuf->seq >= g_AECrefbuf.seq)        return 0;
	}

	return 1;
}

void audio_hw_enable(u32 en_i2s,u32 en_mclk)
{
	if(!asys_reset){
		if(en_mclk){
			u32 regval;
			SC_PINSEL2_FUNC(MCLK);
			SC_PIN1_EN(I2S_MCLK);

			//set MCLK = pad clock(24Mhz)
			regval = ReadReg32(REG_SC_DIV7);
			regval |= BIT7;
			WriteReg32(REG_SC_DIV7,regval);
		}
		if(en_i2s){
			SC_PINSEL2_FUNC(I2S);
			SC_PIN1_EN(I2S_BCLK);
			SC_PIN1_EN(I2S_LCLK);
			SC_PIN1_EN(I2S_DS);
			SC_PIN1_EN(I2S_DI);
		}
		ahw_en = 1;
	}
}


void audio_hw_disable(u32 en_i2s,u32 en_mclk)
{
	if(ahw_en && !asys_reset){
		if(en_mclk){
			SC_PIN1_DISABLE(I2S_MCLK);
			SC_PINSEL2_GPIO(MCLK);
		}
		if(en_i2s){
			SC_PIN1_DISABLE(I2S_BCLK);
			SC_PIN1_DISABLE(I2S_LCLK);
			SC_PIN1_DISABLE(I2S_DS);
			SC_PIN1_DISABLE(I2S_DI);
			SC_PINSEL2_GPIO(I2S);
		}
		ahw_en = 0;
	}
}



s32 ai_set_sr(u32 sample_rate, u32 hwreg)
{
	u32 reg;
	syslog(LOG_INFO, "ai_set_sr sr=%d\n",sample_rate);
	//i2sclock = 24M
	switch(sample_rate){
	/*
		step/mode = 64*sr/i2sclk (i2sclk = 24MHz)
		to make the sample rate match the ov798 internal codec sample rate(if unmatch,will produce noise)
		we configure the step and mode according to the sample rate configuration as below:
		---------------------------------------
		(here MCLK = 12MHz)
		8 kHz (MCLK/1500)  			00110      
		11.0259kHz (MCLK/1088) 		11001   
		12 kHz (MCLK/1000) 			01000       
		16kHz (MCLK/750) 			01010         
		22.0588kHz (MCLK/544) 		11011    
		24kHz (MCLK/500) 			11100         
		32 kHz (MCLK/375) 			01100       
		44.118 kHz (MCLK/272) 		10001 
		48 kHz (MCLK/250) 			00000    
		88.235kHz (MCLK/136) 		11111   
		96 kHz (MCLK/125) 			01110     
		-------------------------------------

		1) i2sclk=24M:
		sr = i2sclk/(mode*64/step) = MCLK/(mode*32/step):
		so, for 8kHz  1500 = mode*32/step --> mode/step = 1500/32 --> mode=1500, step=32, reg=(step<<16)|mode
		8000:  mode = 1500, step=32 reg = 0x2005dc or 0x80177
		11025: mode = 1088, step=32 reg = 0x200440
		12000: mode = 1000, step=32 reg=  0x2003e8
		16000: mode = 750,  step=32 reg=  0x2002ee;
		22050: mode = 544,  step=32 reg=  0x200220;
		24000: mode = 500,  step=32 reg=  0x2001f4;
		32000: mode = 375,  step=32 reg=  0x200177;
		44100: mode = 272,  step=32 reg=  0x200110;
		48000: mode = 250,  step=32 reg=  0x2000fa;
		88200: mode = 136,  step=32 reg=  0x200088;
		96000: mode = 125,  step=32 reg=  0x20007d;

		2) i2sclk=12M
		sr = i2sclk/(mode*64/step) = MCLK/(mode*64/step):
		so, for 8kHz  1500 = mode*64/step --> mode/step = 1500/64 --> mode=1500, step=64, reg=(step<<16)|mode
		8000:  mode = 1500, step=64 reg = 0x4005dc 
		11025: mode = 1088, step=64 reg = 0x400440
		12000: mode = 1000, step=64 reg=  0x4003e8
		16000: mode = 750,  step=64 reg=  0x4002ee;
		22050: mode = 544,  step=64 reg=  0x400220;
		24000: mode = 500,  step=64 reg=  0x4001f4;
		32000: mode = 375,  step=64 reg=  0x400177;
		44100: mode = 272,  step=64 reg=  0x400110;
		48000: mode = 250,  step=64 reg=  0x4000fa;
		88200: mode = 136,  step=64 reg=  0x400088;
		96000: mode = 125,  step=64 reg=  0x40007d;

	*/
#if 0 //i2sclk=24MHz
		case 8000:  reg = 0x80177 ; break;
		case 11025: reg = 0x200440; break;
		case 12000: reg = 0x2003e8; break;
		case 16000: reg = 0x2002ee; break;
		case 22050:	reg = 0x200220; break;
		case 24000: reg = 0x2001f4; break;
		case 32000:	reg = 0x200177; break;
		case 44100: reg = 0x200110; break;
		case 48000: reg = 0x2000fa; break;
		case 88200: reg = 0x200088; break;
		case 96000: reg = 0x20007d; break;
		default:    
			reg = 0x2002ee; 
			syslog(LOG_WARNING, "Warning: unsupported sample rate(%d),default to 16000 \n",sample_rate);
			break;
#else //i2sclk=12MHz
		case 8000:  reg = 0x4005dc ; break;
		case 11025: reg = 0x400440; break;
		case 12000: reg = 0x4003e8; break;
		case 16000: reg = 0x4002ee; break;
		case 22050:	reg = 0x400220; break;
		case 24000: reg = 0x4001f4; break;
		case 32000:	reg = 0x400177; break;
		case 44100: reg = 0x400110; break;
		case 48000: reg = 0x4000fa; break;
		case 88200: reg = 0x400088; break;
		case 96000: reg = 0x40007d; break;
		default:    
			reg = 0x4002ee; 
			syslog(LOG_WARNING, "Warning: unsupported sample rate(%d),default to 16000 \n",sample_rate);
			break;

#endif
	}

	WriteReg32(hwreg, reg);
	
	return 0;
}


extern const int dbvol_param[];

//#define SWAP2BYTES(x) ((((x)&0xFF) << 8) +(((x)&0xFF00) >> 8)) 
#define SWAP2BYTES(x) (x)

void liba_volume_control(void *pcm, u32 len,s32 dbvol)
{
		s32 ifac,idx;
		s16 *psample;
		s32 tmp;
		if(dbvol > 63)
			dbvol = 63;
		else if(dbvol < -64)
			dbvol = -64;
		ifac = dbvol_param[63 - dbvol];
		psample = (s16 *)pcm;

		for(idx = 0;idx < len;idx++){
			tmp = ((s32)psample[idx]*ifac)>>14;
			if(tmp > 32767)
				tmp = 32767;
			else if(tmp < -32768)
				tmp = -32768;
			psample[idx] = (short)tmp;
		}
}	

