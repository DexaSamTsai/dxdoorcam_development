#include "includes.h"


static FRAME *s_cur_frm = NULL;//current decoding frame from linklist
static u32 s_lb_datalen; //data lenght in l_buffer
static u32 s_lb_rpos;  //l_buffer read position
static u32 s_ibuf_datalen;
static u8 __attribute__ ((aligned(4))) s_adec_inbuf[INBUF_ONELEN]; 
static u8 __attribute__ ((aligned(4))) s_adec_outbuf[MAX_OUTBUFLEN]; 

volatile u32 need_notfull_evt;
volatile u32 adecevt_set;
//t_liba_dcfg g_liba_dcfg;

t_adecoder_cfg *g_adecoder_cfg = 0;

static u32 inbuf_cur;
static FRAME aframe;
extern volatile s32 dabuf_sel;


// for uac
volatile u32 pcmbuf_used[2]; 
volatile u32 g_adec_sel_buf;


static u8  fadein_flag;
void libadec_pcmbuf_clear(void)
{
	memset((u8 *)g_liba_dcfg.AFI_BUF0, 0, g_liba_dcfg.AFI_BUFLEN);
	memset((u8 *)g_liba_dcfg.AFI_BUF1, 0, g_liba_dcfg.AFI_BUFLEN);
}


int libadec_frm_add(FRAME *afrm)
{
	FRAME *new_frm;
	u32 flags;

	spin_lock_irqsave(flags);

	new_frm = framelist_alloc(g_liba_dcfg.list);
	if(new_frm == NULL){
		syslog(LOG_ERR, "libadec_frm_add framelist_alloc failed\n");
		spin_unlock_irqrestore(flags);
		return -1;
	}

	new_frm->addr = afrm->addr;
	new_frm->size = afrm->size;
	new_frm->status = FRAME_STATUS_READY;
	new_frm->next = NULL;

	framelist_add(g_liba_dcfg.list, new_frm);
	if(adecevt_set){
		libmpu_clear_event(MPUEVT_ADEC_NOTFULL);
		adecevt_set = 0;
	}

	spin_unlock_irqrestore(flags);
	return 0;
}


/**
 * @brief remove one frame from input linklist
 * @details when the frame linklist head is used up by decoder,it should be remove from the linklist and move the head pointer to the next frame.
 * @return 0 :ok
 */
static int libadec_remove_frm(FRAME* frm)
{
	s32 ret;	
	ret = framelist_free(g_liba_dcfg.list, frm, 0,0);
	if(ret < 0){
		syslog(LOG_WARNING, "libadec_remove_frm failed\n");	
	}
	else{
		if(need_notfull_evt){
			need_notfull_evt = 0;
			syslog(LOG_DEBUG, "libadec set evt\n");
			libmpu_set_event(MPUEVT_ADEC_NOTFULL);
			adecevt_set = 1;
		}
	}

	return ret;

}



/**
 * @brief get a frame address and length 
 * @details when one frame is used up ,a new frame must be obtained to fill data to the audio decoder. this API is to get the address and length of the new frame.
 * @return 0 :ok
 */
FRAME* libadec_get_frm(void)
{
	FRAME *ret;
	ret = framelist_get(g_liba_dcfg.list);

	if(ret)
		return (FRAME*)((u32)ret|BIT31);
	else
		return NULL;
}

static void interpolation(s16* src, t_cbuf_pos* wpos)
{
	u32 i,j;
    s16* dst = (s16*)wpos->pos0;

	if(g_liba_dcfg.inp.ch == 1){//MONO
		u32 buf0_len = wpos->len0/2; //buf0 samples
		for(i=0;i < buf0_len/g_liba_dcfg.inp.interpo_times; i++){
			j = g_liba_dcfg.inp.interpo_times;	
			while(j--){
				*dst++ = *src;
			}
			src++;
		}

		if(wpos->len1){
			u32 buf1_len = wpos->len1/2; //buf1 samples
			u32 res = buf0_len%g_liba_dcfg.inp.interpo_times; 
			if(res){
				j = res;
				while(j--){
					*dst++ = *src;
				}
				dst = (s16*)wpos->pos1;
				j = g_liba_dcfg.inp.interpo_times - res;
				while(j--){
					*dst++ = *src;
				}
				src++;
			}else{
				dst = (s16*)wpos->pos1;
			}
			for(i=0;i < buf1_len/g_liba_dcfg.inp.interpo_times; i++){
				j = g_liba_dcfg.inp.interpo_times;	
				while(j--){
					*dst++ = *src;
				}
				src++;
			}
		}
	}else{//STEREO
		u32 buf0_len = wpos->len0/2/2; //buf0 samples for each channel
		for(i=0;i < buf0_len/g_liba_dcfg.inp.interpo_times; i++){
			j = g_liba_dcfg.inp.interpo_times;	
			while(j--){
				*dst++ = *src;
				*dst++ = *(src+1);
			}
			src += 2;
		}

		if(wpos->len1){
			u32 buf1_len = wpos->len1/2/2; //buf1 samples for each channel
			u32 res = buf0_len%g_liba_dcfg.inp.interpo_times; 
			if(res){
				j = res;
				while(j--){
					*dst++ = *src;
					*dst++ = *(src+1);
				}
				dst = (s16*)wpos->pos1;
				j = g_liba_dcfg.inp.interpo_times - res;
				while(j--){
					*dst++ = *src;
					*dst++ = *(src+1);
				}
				src += 2;;
			}else{
				dst = (s16*)wpos->pos1;
			}
			for(i=0;i < buf1_len/g_liba_dcfg.inp.interpo_times; i++){
				j = g_liba_dcfg.inp.interpo_times;	
				while(j--){
					*dst++ = *src;
					*dst++ = *(src+1);
				}
				src += 2;
			}
		}

	}
}
#ifndef I2SIRQ_MEMCOPY
static void deinterleave_pcm( u32 d_addr,u32 s_addr, u32 len)
{
	s16 * s = (s16 *)s_addr;
	s16 * d = (s16 *)d_addr;

	s32 i;

	for(i=0; i<len/2; i++){
		d[i]=s[2*i];//d[2*i] = s[i];
		d[i+len/2]=s[2*i+1];//d[2*i+1] = s[i+len/2];
	}
	return;
}

static void adec_store_aec_refdata(u8 *src)
{
	t_cbuf_pos wpos;
	s32 ret;
	ret = liba_cbuf_get_wpos((t_cbuf_ctrl*)&g_AECrefbuf,DA_BUF_SIZE,&wpos);		
	if(ret == 0){
//		lowlevel_putc('<');
		memcpy((u8*)wpos.pos0, src, wpos.len0);
		liba_cbuf_update_wpos((t_cbuf_ctrl*)&g_AECrefbuf,DA_BUF_SIZE);
	}else{
		lowlevel_putc('F');
	}
}

static void putdata_to_dabuffer(void)
{
	t_cbuf_ctrl *dcbuf = &g_liba_dcfg.obuf;
	t_cbuf_pos rpos;
	s32 ret;
	s32 needsz = DA_BUF_SIZE;
	u8* dst;
	
	if(dabuf_sel == 0){
		dst = (u8*)g_liba_dcfg.AFI_BUF0;
	}else if(dabuf_sel == 1){
		dst = (u8*)g_liba_dcfg.AFI_BUF1;
	}else{
		return;
	}

	if(g_aec_enabled){
		if(liba_cbuf_frame_aligned((t_cbuf_ctrl*)&g_AECrefbuf))
			adec_store_aec_refdata(dst);
		else
			lowlevel_putc('s');
//            lowlevel_putc('d');
	}

	ret = liba_cbuf_get_rpos(dcbuf, needsz, &rpos);
	if(ret < 0 ){
		syslog(LOG_DEBUG,"n");
	}else{
		if(g_liba_dcfg.inp.ch == 1){ //mono
			if(g_liba_dcfg.inp.aichannel==1){
				//only send data to left channel
				memcpy(dst, (u8*)rpos.pos0, needsz);
				//memset((void*)g_liba_dcfg.AFI_BUF0+needsz, 0, needsz);
			}else if(g_liba_dcfg.inp.aichannel==2){
				//only send data to right channel
				//memset((void*)g_liba_dcfg.AFI_BUF0, 0, needsz);
				memcpy(dst + needsz, (u8*)rpos.pos0, needsz);
			}else if(g_liba_dcfg.inp.aichannel==0){
				// send data to both left and right channel
				memcpy(dst, (u8*)rpos.pos0, needsz);
				memcpy(dst + needsz, (u8*)rpos.pos0, needsz);
			}
			/*g_liba_dcfg.obuf length is multiples of DA_BUF_SIZE, 
			  so here rpos.len1=0, don't need check*/
		}else{//stereo
			deinterleave_pcm(dst,rpos.pos0,needsz/2);
		}
		liba_cbuf_update_rpos(dcbuf,needsz);
		dabuf_sel = -1;
	}
}
#endif


/*
s_adec_inbuf is the decoder tmp input buffer, its size is INBUF_ONELEN.
s_adec_outbuf is the decoder tmp output buffer.

Data path druing decoding:
linklist -> s_adec_inbuf -> decoder(g_adecoder_cfg->process) -> s_adec_outbuf -> g_liba_dcfg.obuf
1, linklist  
  call libadec_get_frm() to get a l_buffer(buffer from linklist), s_lb_rpos point to data start in l_buffer, s_lb_datalen denote the data length in this l_buffer
  we copy bitstream in l_buffer to s_adec_inbuf. if only part of bitstream copied, s_lb_rpos point to the start addr of the left data and s_lb_datalen denote the left data length

2, s_adec_inbuf
	s_ibuf_datalen denote the data lenght in s_adec_inbuf. we copy bitstream from l_buffer to s_adec_inbuf. if s_ibuf_datalen is enough for a frame according to the decoder(g_adecoder_cfg->infrmsz), stop copying
	it's the third parameter in the g_adecoder_cfg->process() of decoder

3, decoder
	g_adecoder_cfg->process(void *bufout,u32 *out_len, void *bufin, u32 *in_len);
	
4, s_adec_outbuf
	it's the first parameter (void *bufout) for decoder
*/

int libadec_update_output_internal(void)
{
	s32 n;
	s16 *outptr ;//= (short *)out_addr;
	u8 *inptr;
	s32 free_size = INBUF_ONELEN - s_ibuf_datalen;	//free space in s_adec_inbuf
	u32 len = g_liba_dcfg.AFI_BUFLEN;
	u32 s_ibuf_datalen_save = 0;
	s32 listcnt;
	u32 out_addr;
	t_cbuf_ctrl *dcbuf = &g_liba_dcfg.obuf;
	t_cbuf_pos wpos;
	s32 ret;
	s32 outlen;
	u32 flags;
	
	//	get available I2S output buffer address.	
#ifndef I2SIRQ_MEMCOPY
//	spin_lock_irqsave(flags);
	putdata_to_dabuffer();
//	spin_unlock_irqrestore(flags);
#endif

	outlen = g_adecoder_cfg->outfrmsz*g_liba_dcfg.inp.ch;
	if(g_liba_dcfg.inp.interpo_en){
		outlen = outlen*g_liba_dcfg.inp.interpo_times;
	}

	spin_lock_irqsave(flags);
	ret = liba_cbuf_get_wpos(dcbuf, outlen, &wpos);
	spin_unlock_irqrestore(flags);

	if(ret < 0){
		return -1;
	}

//	memset(&s_adec_outbuf,0,sizeof(s_adec_outbuf));
	out_addr = (u32)&s_adec_outbuf[0];
	len = g_adecoder_cfg->outfrmsz * g_liba_dcfg.inp.ch;

	listcnt = framelist_cnt(g_liba_dcfg.list);
	if(listcnt == 0){
		if(g_liba_dcfg.fill_AFIBUF){//for fade out
			g_liba_dcfg.fill_AFIBUF((u8*)out_addr,g_liba_dcfg.AFI_BUFLEN);
			fadein_flag = 1;
		}

		syslog(LOG_DEBUG, "adec linklist empty\n");
		return -2; //under flow
	}
	
	outptr = (s16 *)out_addr;

	//fill bitstream to s_adec_inbuf,untill it's enough for one frame or no data in linklist
	while(s_ibuf_datalen < g_adecoder_cfg->infrmsz*g_liba_dcfg.inp.ch){
		if(s_lb_datalen == 0){
			s_cur_frm = libadec_get_frm();
			if(s_cur_frm == NULL){	
				break;	
			}
			s_lb_rpos = s_cur_frm->addr|BIT31;
			s_lb_datalen = s_cur_frm->size;
		}
		n = (s_lb_datalen <= free_size)? s_lb_datalen : free_size;
		memcpy((s_adec_inbuf+s_ibuf_datalen), (u8 *)s_lb_rpos, n);
		
		free_size -= n;
		s_lb_datalen -= n;
		s_ibuf_datalen += n;
		s_lb_rpos += n;
		if(s_lb_datalen == 0){
			libadec_remove_frm(s_cur_frm);
			s_cur_frm = NULL;
			s_lb_rpos = NULL;
		}
	}

	//check if data in s_adec_inbuf is enough for one frame,if not return
	if(s_ibuf_datalen < g_adecoder_cfg->infrmsz*g_liba_dcfg.inp.ch){
		return -3;
	}

	inptr = s_adec_inbuf;
	s_ibuf_datalen_save = s_ibuf_datalen;

	g_adecoder_cfg->process((s16*)outptr, &len, inptr, (u32* )&s_ibuf_datalen);

	//copy left bitstream to the start of inbuf
	if(s_ibuf_datalen){
		memcpy(s_adec_inbuf, (s_adec_inbuf + s_ibuf_datalen_save - s_ibuf_datalen), s_ibuf_datalen);
	}

	if(g_liba_dcfg.eq_process){ //do equalizer
		g_liba_dcfg.eq_process((void *)out_addr,g_adecoder_cfg->outfrmsz*g_liba_dcfg.inp.ch);
	}

	if(g_liba_dcfg.post_process){ //do post-process,like AGC
		g_liba_dcfg.post_process((void *)out_addr,g_adecoder_cfg->outfrmsz*g_liba_dcfg.inp.ch);
	}	

	if(g_liba_dcfg.inp.dvol){ //if dvol is not 0dB, need handle volume
		liba_volume_control((void*)out_addr,g_adecoder_cfg->outfrmsz*g_liba_dcfg.inp.ch/2,g_liba_dcfg.inp.dvol);
	}

#if 0
	if(len == 0){
		pcmbuf_used[g_adec_sel_buf] = 1;
		g_adec_sel_buf = 1 - g_adec_sel_buf;
	}
#endif

	if(fadein_flag && g_liba_dcfg.fill_FADEIN){//if need fade in, call fill_FADEIN callback
		g_liba_dcfg.fill_FADEIN((u8*)out_addr,g_liba_dcfg.AFI_BUFLEN);
		fadein_flag = 0;
	}

	if(g_liba_dcfg.inp.interpo_en){
		interpolation((s16*)out_addr,&wpos);
	}else{
		memcpy((u8*)wpos.pos0,(u8*)out_addr,wpos.len0);
		if(wpos.len1){
			memcpy((u8*)wpos.pos1,(u8*)(out_addr + wpos.len0),wpos.len1);
		}
	}

	spin_lock_irqsave(flags);
	liba_cbuf_update_wpos(dcbuf,outlen);
	spin_unlock_irqrestore(flags);

	return 0;
}


FRAME* libadec_inbuf_get(void)
{
	u32 ret;
	s32 cnt,num;
	FRAME* pfrm =(FRAME*)((u32)&aframe|0x80000000);

	cnt =framelist_cnt(g_liba_dcfg.list);
	num =framelist_num(g_liba_dcfg.list);
	if(cnt >= num){
		need_notfull_evt = 1;
		syslog(LOG_DEBUG, "libadec_inbuf_get failed %d:%d\n",cnt,num);
		return 0;
	}

	memset(pfrm,0,sizeof(FRAME));
	ret = inbuf_cur;
	inbuf_cur += INBUF_ONELEN;
	if(inbuf_cur >= (u32)(g_liba_dcfg.inbuf_addr + g_liba_dcfg.inbuf_size)){
		inbuf_cur = (u32)g_liba_dcfg.inbuf_addr;
	}

	pfrm->addr = ret|BIT31;
	pfrm->size = INBUF_ONELEN;

	return pfrm;
}

extern u32 adecoder_cfg_start;
extern u32 adecoder_cfg_end;
static s32 libadec_setfmt(E_AFMT afmt)
{
	g_adecoder_cfg = (t_adecoder_cfg *)(&adecoder_cfg_start) ;

	for(; g_adecoder_cfg < (t_adecoder_cfg *)(&adecoder_cfg_end) ; g_adecoder_cfg ++){
		if(g_adecoder_cfg->afmt == afmt){
			syslog(LOG_INFO, "libadec_setfmt set format %d sucessfully\n",afmt);
			return 0;
		}
	}

#if 0
	//set default
	g_adecoder_cfg = (t_adecoder_cfg *)(&adecoder_cfg_start) ;
#endif
	return -1;
}

int libadec_init(void)
{
	int err = 0;
	u32 process_len;

	if(libadec_setfmt(g_liba_dcfg.inp.fmt) < 0){
		syslog(LOG_ERR, "libadec_setfmt set format %d failed\n",g_liba_dcfg.inp.fmt);
		return -1;
	}

	err = g_adecoder_cfg->init(g_liba_dcfg.inp.algfrmsz);


	process_len = g_adecoder_cfg->outfrmsz*g_liba_dcfg.inp.ch;
	if(process_len > MAX_OUTBUFLEN){
		syslog(LOG_ERR, "Err:MPU adec.c process_len > MAX_OUTBUFLEN\n");
		syslog(LOG_ERR, "Please set g_liba_dcfg.inp.algfrmsz smaller\n");
		return -1;
	}


	g_liba_dcfg.ai_sr = g_liba_dcfg.inp.sr;
	if(g_liba_dcfg.inp.interpo_en){
		g_liba_dcfg.ai_sr = g_liba_dcfg.inp.sr*g_liba_dcfg.inp.interpo_times;
		process_len *=g_liba_dcfg.inp.interpo_times;
	}

	if(process_len + DA_BUF_SIZE >= g_liba_dcfg.obuf.len){
		syslog(LOG_ERR, "Err:MPU adec.c:process_len + DA_BUF_SIZE >= g_liba_dcfg.obuf.len\n");
		syslog(LOG_ERR, "Please set g_liba_dcfg.inp.algfrmsz smaller\n");
		return -1;
	}

	inbuf_cur = g_liba_dcfg.inbuf_addr;
	if(liba_dhw_init() < 0){
		syslog(LOG_ERR, "liba_dhw_init failed\n");
		return -1;
	}

	if(g_liba_dcfg.post_init){
		if(g_liba_dcfg.post_init() < 0){
			syslog(LOG_ERR, "g_liba_dcfg.post_init failed\n");
			return -1;
		}
	}

	if(g_liba_dcfg.eq_init){
		if(g_liba_dcfg.eq_init() < 0){
			syslog(LOG_ERR, "g_liba_dcfg.eq_init failed\n");
			return -1;
		}
	}


	syslog(LOG_INFO, "libadec_init_internal ok!\n");

#if 1
	need_notfull_evt = 0;
	adecevt_set = 0;

	fadein_flag = 1;

	s_lb_datalen = 0;
	s_lb_rpos = NULL;
	s_ibuf_datalen = 0;

	framelist_init(g_liba_dcfg.list);

	if(framelist_num(g_liba_dcfg.list)*INBUF_ONELEN > g_liba_dcfg.inbuf_size)
	    syslog(LOG_ERR, "g_liba_dcfg inbuf size not match:%d,%d\n", g_liba_dcfg.inbuf_size, framelist_cnt(g_liba_dcfg.list)*INBUF_ONELEN);
#endif

	return err;
}


int libadec_start(void)
{
#if 0
	//move these to init
	need_notfull_evt = 0;
	adecevt_set = 0;

	fadein_flag = 1;

	s_lb_datalen = 0;
	s_lb_rpos = NULL;
	s_ibuf_datalen = 0;

	framelist_init(g_liba_dcfg.list);
#endif

	liba_dhw_start();	

//	syslog(LOG_INFO, "libadec_start_internal ok!\n");

	return 0;
}

int libadec_stop(void)
{
	liba_dhw_stop();	
	memset((u8 *)g_liba_dcfg.AFI_BUF0, 0, g_liba_dcfg.AFI_BUFLEN);
	memset((u8 *)g_liba_dcfg.AFI_BUF1, 0, g_liba_dcfg.AFI_BUFLEN);

	syslog(LOG_INFO, "libadec_stop\n");

	return 0;
}

int libadec_exit(void)
{
	liba_dhw_exit();
	return 0;
}


int libadec_reset(void)
{
	int err = 0;

	memset((u8 *)g_liba_dcfg.AFI_BUF0, 0, g_liba_dcfg.AFI_BUFLEN);
	memset((u8 *)g_liba_dcfg.AFI_BUF1, 0, g_liba_dcfg.AFI_BUFLEN);
	inbuf_cur = g_liba_dcfg.inbuf_addr;
	framelist_init(g_liba_dcfg.list);
	err = g_adecoder_cfg->init(g_liba_dcfg.inp.algfrmsz);
	return err;
}
