#include "includes.h"
#include "compressor.h"
#include "fir.h"

/********************              DEFINES                   ******************/
/********************               MACROS                   ******************/

/********************         TYPE DEFINITIONS               ******************/

/********************        FUNCTION PROTOTYPES             ******************/

/********************           LOCAL VARIABLES              ******************/

/********************          GLOBAL VARIABLES              ******************/

/********************              FUNCTIONS                 ******************/

#define FIR_ORDER 64
static fir_t *g_p_fir = NULL;


extern int tdagc_init(void);
extern void tdagc_process(void *buf, int len);

static int compressor_init(void)
{
    adecagc_effect_compressor_init(15);
    g_p_fir = fir_init(FIR_ORDER, NULL);

	return 0;
}

static int adec_agc_init(void)
{
	if(g_liba_dcfg.inp.compressor_en){
		compressor_init();
	}

	if(g_liba_dcfg.inp.agc.en){
		tdagc_init();
	}

	return 0;
}

/****************************************************************
 * Function: adec_agc_process
 * Args    : 1. (in) point of audio PCM buffer
 *           2. (in) PCM buffer length(byte)
 * Return  : void *
 * Purpose : preprocess callback
 * Notes   : parameter "int len" is in byte;
 *           parameter "void *buf" is the input and output buffer
 *           for the process, if stereo, this callback should process
 *           each channel respectively,first half in buf is left
 *           channel data, and the second half is right channel data
 ****************************************************************/
static void compressor_process(void *buf, int len)
{
    int i;
    short *ptr;

    ptr = (short *) buf;

    len = len >> 1; //byte to short

    if(g_p_fir)
    {
    	fir_process(ptr, ptr, len, g_p_fir);
    }

    for (i = 0; i < len; i+=COMP_LEN)
    {
        adecagc_effect_compressor_run((INT16*)ptr+i);
    }
}


static void adec_agc_process(void *buf, int len)
{
	if(g_liba_dcfg.inp.compressor_en){
		compressor_process(buf, len);
	}
	
	if(g_liba_dcfg.inp.agc.en){
		tdagc_process(buf, len);
	}
}

__adecpost_cfg t_adecpost_cfg g_adecpost_agc_cfg ={
	.alg = AUDIO_DPOST_AGC,  
	.post_init = adec_agc_init,
	.post_process = adec_agc_process,
};
