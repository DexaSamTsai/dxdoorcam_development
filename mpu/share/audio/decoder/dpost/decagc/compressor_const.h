/* the following code is generated by compressor_generate */

#ifndef COMPRESS_CONST_H

typedef short INT16;

#define mCurRate 8000
#define mMaxSample 32767
#define mMaxOutput 31000
#define COMP_LEN 32
#define mMaxGain 24
#define mMinGain 6

static const INT16 settingtbl[] = {
    13817, 349, 353, 88,
    10361, 465, 472, 117,
    7770, 580, 591, 146,
    5826, 695, 710, 176,
    4369, 809, 830, 205,
    3276, 924, 950, 234,
    2457, 1037, 1071, 263,
    1842, 1150, 1192, 292,
    1381, 1263, 1314, 322,
    1036, 1376, 1436, 351,
};

#define mNoiseFloor 184

static const INT16 comptbl[] = {
    11747, 6746, 4877, 3875, 3241, 2801, 2476, 2225, 
    2025, 1861, 1725, 1609, 1509, 1422, 1346, 1278, 
    1217, 1163, 1114, 1069, 1028, 990, 956, 924, 
    894, 866, 841, 816, 794, 773, 753, 734, 
    716, 699, 683, 668, 653, 639, 626, 614, 
    602, 590, 579, 569, 558, 549, 539, 530, 
    522, 513, 505, 497, 490, 483, 476, 469, 
    462, 456, 450, 444, 438, 432, 427, 421, 
    416, 411, 406, 401, 397, 392, 388, 383, 
    379, 375, 371, 367, 363, 359, 356, 352, 
    349, 345, 342, 339, 336, 332, 329, 326, 
    323, 321, 318, 315, 312, 310, 307, 304, 
    302, 299, 297, 295, 292, 290, 288, 285, 
    283, 281, 279, 277, 275, 273, 271, 269, 
    267, 265, 263, 262, 260, 258, 256, 255, 
    253, 251, 250, 248, 246, 245, 243, 242, 
};


#endif /*COMPRESS_CONST_H*/
