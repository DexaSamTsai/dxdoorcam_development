/*******************************************************************************
Workfile:		compressor.h
Revision: 		1.00
Last Modtime: 	2012-6-18
Author: 		hunter.fang
Description:	

*******************************************************************************/

#ifndef _COMPRESSOR_H_
#define _COMPRESSOR_H_

#include "compressor_const.h"

void adecagc_effect_compressor_init(INT16 gain);
void adecagc_effect_compressor_run(INT16 * fin);

#endif /* _COMPRESSOR_H_ */
