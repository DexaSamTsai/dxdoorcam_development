//#include <math.h>
#include "includes.h"


typedef struct {
	int y2;
	int y1;
	int x0;
	int x1;
} ov_rmdc_savedata;
typedef struct ov_agc_st{
    unsigned	sample_max;
    int   counter;
    int   igain;         //Q8
    unsigned   ipeak;
    int   silence_counter;
	int   samplerate;
	unsigned int threshold;
	unsigned maxoutsample;
	ov_rmdc_savedata rmdcdata;
} ov_agc_t;

static ov_agc_t g_AGC_Struct;


#define  SHRT_MAX 32767
#define  SHRT_MIN (-32768)
#define  Q_IGAIN  8

#define SAMPLE_DFBS_TBL_MAXIDX 61
static const short pcm_dbfs_tbl[]={
32767, //0dbfs
29203, //-1dbfs
26027, //-2dbfs
23197, //-3dbfs
20674, //-4dbfs
18426, //-5dbfs
16422, //-6dbfs
14636, //-7dbfs
13044, //-8dbfs
11626, //-9dbfs
10361, //-10dbfs
9234, //-11dbfs
8230, //-12dbfs
7335, //-13dbfs
6537, //-14dbfs
5826, //-15dbfs
5193, //-16dbfs
4628, //-17dbfs
4125, //-18dbfs
3676, //-19dbfs
3276, //-20dbfs
2920, //-21dbfs
2602, //-22dbfs
2319, //-23dbfs
2067, //-24dbfs
1842, //-25dbfs
1642, //-26dbfs
1463, //-27dbfs
1304, //-28dbfs
1162, //-29dbfs
1036, //-30dbfs
923, //-31dbfs
823, //-32dbfs
733, //-33dbfs
653, //-34dbfs
582, //-35dbfs
519, //-36dbfs
462, //-37dbfs
412, //-38dbfs
367, //-39dbfs
327, //-40dbfs
292, //-41dbfs
260, //-42dbfs
231, //-43dbfs
206, //-44dbfs
184, //-45dbfs
164, //-46dbfs
146, //-47dbfs
130, //-48dbfs
116, //-49dbfs
103, //-50dbfs
92, //-51dbfs
82, //-52dbfs
73, //-53dbfs
65, //-54dbfs
58, //-55dbfs
51, //-56dbfs
46, //-57dbfs
41, //-58dbfs
36, //-59dbfs
32, //-60dbfs
0,// < -60dbfs
};

#define CORRECT_DBFS 1
int td_agc_init(int sr,int targetdbfs,int maxdbfs, int nfdbfs)
{
	unsigned int AGCLevel;
	unsigned int threshold;//noise threshold

	
	if(targetdbfs == 0 && maxdbfs == 0 && nfdbfs == 0){
		syslog(LOG_ERR,"DECAGC parameter not configured(all zeros), set to default -18,-15,-18dBFS\n");
		targetdbfs = -18;
		maxdbfs = -15;
		nfdbfs = -18;
	}

	targetdbfs += CORRECT_DBFS ; //+CORRECT_DBFS, tested to be expected result
	maxdbfs += CORRECT_DBFS;
	nfdbfs += CORRECT_DBFS;

	if(targetdbfs > 0 || maxdbfs > 0 ||nfdbfs > 0){
		syslog(LOG_ERR,"DECAGC paremeters not configured properly, please set smaller than %ddBFS\n",
						-CORRECT_DBFS);
		return -1;
	}

	targetdbfs = - targetdbfs; 
	maxdbfs = - maxdbfs; 
	nfdbfs = - nfdbfs; 
	if(targetdbfs >= sizeof(pcm_dbfs_tbl)/2){
		syslog(LOG_ERR,"td_agc.c target dbfs set too low(-%ddBFS)\n",targetdbfs+1);
		return -1;

	}

	if(maxdbfs >= sizeof(pcm_dbfs_tbl)/2){
		syslog(LOG_ERR,"td_agc.c max dbfs set too low(-%ddBFS)\n",maxdbfs+1);
		return -1;
	}

	if(nfdbfs >= sizeof(pcm_dbfs_tbl)/2){
		nfdbfs = sizeof(pcm_dbfs_tbl)/2 - 1; 
	}
	
    AGCLevel = pcm_dbfs_tbl[targetdbfs];
	if(nfdbfs){
		threshold = pcm_dbfs_tbl[nfdbfs];
	}else{
		threshold = 0;
	}

    ov_agc_t *p_AGCstrc = &g_AGC_Struct;
	
    memset(p_AGCstrc, 0, sizeof(ov_agc_t));

	p_AGCstrc->maxoutsample = pcm_dbfs_tbl[maxdbfs]; 
    p_AGCstrc->sample_max = 1;
    p_AGCstrc->counter = 0;
		//igain Q8
    p_AGCstrc->igain = 1<<Q_IGAIN; //256;//32767;//65536;
	p_AGCstrc->samplerate = sr;
	p_AGCstrc->threshold = threshold;
	p_AGCstrc->ipeak = AGCLevel;

    p_AGCstrc->silence_counter = 0;    
	
	p_AGCstrc->rmdcdata.x0 = 0;
	p_AGCstrc->rmdcdata.x1 = 0;
	p_AGCstrc->rmdcdata.y1 = 0;
	p_AGCstrc->rmdcdata.y2 = 0;	

	syslog(LOG_INFO,"adec td_agc.c AGCLevel=%d,threshold=%d, maxoutsample=%d \n",
				 AGCLevel,threshold,p_AGCstrc->maxoutsample);

	return 0;
}


int tdagc_init(void)
{
	return td_agc_init(g_liba_dcfg.inp.sr, g_liba_dcfg.inp.agc.trgtdbfs,
				g_liba_dcfg.inp.agc.maxdbfs, g_liba_dcfg.inp.agc.nfdbfs);
}

//#include <math.h>
#define WC 5
#define PI 3.1415926535897932384626433832795
#define SQRT_2 1.4142135623730950488016887242097

#if 1
int F_MUL64_RSHIFT(int x,int y,int n)
{
#if 1
	return ((long long)x * y) >> n;
#else
		int temp = 0,result;
		asm("b.mulh %0,%1,%2;\
			b.mul %3,%1,%2;\
			b.srl %3,%3,%4;\
			b.sll %0,%0,%5;\
			b.add %0,%0,%3":\
			"=&r"(result):\
			"r"(x),"r"(y),"r"(temp),"r"(n),"r"(32-n));
		return result;
#endif
}	

								 
int RM_DCOffset(ov_rmdc_savedata *st,short *output, short *input,int lg)
{
    short i, x2;
    int L_tmp;
	//long long L_tmp64;
#define QCOEF 28
#define Q_PREP_COEF (1<<QCOEF)
#define Q16 (1<<16) 
	//int tmp_f;
	
#if 1
#define C 0.0019634979317947679//(tan((double)PI*WC/8000))
#define B0 (1.0/(1.0+SQRT_2*C+C*C)) //0.99722704990447017088698334162665
#define  B1 (-2*B0) //-1.9944540998089403417739666832533
#define  B2 B0
#define  A1 (-(2*B0*(C*C-1))) //1.9944464105419268360843161790007
#define  A2 (-(B0*(1-SQRT_2*C+C*C))) //-0.99446178907595384746361718750587
#else
double  C = tan(PI*WC/8000);
double  B0 = (1.0/(1.0+SQRT_2*C+C*C));
double  B1 = (-2*B0);
double  B2 = B0;
double  A1 = -(2*B0*(C*C-1));
double  A2 = -(B0*(1-SQRT_2*C+C*C));
#endif
	//int a11 = (int)(1.906005859*Q_PREP_COEF+0.5);
	//int a22 = (int)(-0.911376953*Q_PREP_COEF+0.5);
	//int b00 = (int)(0.4636230465*2*Q_PREP_COEF+0.5);
	//int b11 = (int)(-0.92724705*2*Q_PREP_COEF+0.5);
	//int b22 = (int)(0.4636234515*2*Q_PREP_COEF+0.5);
	int a1 = (int)(A1*Q_PREP_COEF+0.5);
	int a2 = (int)(A2*Q_PREP_COEF+0.5);
	int b0 = (int)(B0*Q_PREP_COEF+0.5);
	int b1 = (int)(B1*Q_PREP_COEF+0.5);
    int b2 = (int)(B2*Q_PREP_COEF+0.5);
	
	
    for (i = 0; i < lg; i++) {
		
        x2 = st->x1;
        st->x1 = st->x0;
        st->x0 = input[i];
		{
		//	L_tmp = ((long long)st->y1 * a1) >>(16 + QCOEF - 16) ;
			L_tmp = F_MUL64_RSHIFT(st->y1,a1,QCOEF);
		//	L_tmp += ((long long)st->y2 * a2) >>(16 + QCOEF - 16) ;
			L_tmp += F_MUL64_RSHIFT(st->y2,a2,QCOEF);
		//	L_tmp += ((long long)st->x0 * b0) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(st->x0,b0,QCOEF - 16);
		//	L_tmp += ((long long)st->x1 * b1) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(st->x1,b1,QCOEF - 16);
		//	L_tmp += ((long long)x2 * b2) >>(0 + QCOEF - 16);
			L_tmp += F_MUL64_RSHIFT(x2,b2,QCOEF - 16);
			output[i] = L_tmp >> 16;
			
			st->y2 = st->y1;
			st->y1 = L_tmp;
		}
    }
    return 0;
}


#endif

static void  td_agc_process(short *buffer, int len)
{
    ov_agc_t *agc = &g_AGC_Struct;
    short *bufout = buffer;
    int i;
//	RM_DCOffset(&agc->rmdcdata,buffer,buffer, len);
    for(i=0; i<len; i++)
    {
        int gain_new;
        int sample;
		int pcm, pcmabs;

        /* get the abs of buffer[i] */
        sample = buffer[i];
        sample = (sample < 0 ? -(sample):sample);
		
        if(sample > (int)agc->sample_max)
        {
            /* update the max */
            agc->sample_max = (unsigned)sample;
        }
        agc->counter ++;
		
        /* Will we get an overflow with the current gain factor? */
        if ((((int)sample * agc->igain) >> Q_IGAIN ) > agc->ipeak) 
        {
			int gain, tmpgain;
			/* Yes: Calculate new gain. */
			// agc->igain = (240.0/256)*agc->ipeak/agc->sample_max *(1<<Q_IGAIN);
			
			tmpgain = agc->ipeak * 240 / agc->sample_max ; 
			do{
				gain = tmpgain;
				pcm = (((int)buffer[i] * gain) >> Q_IGAIN);
				pcmabs = (pcm < 0 ? -(pcm):pcm);
				tmpgain = (tmpgain * 240)>> 8;
			}while(pcmabs > agc->maxoutsample);

			agc->igain = gain; 
			bufout[i] = (short)pcm;
			agc->silence_counter = 0;
            continue;
        }
		
        /* Calculate new gain factor 20 times per second, 50ms interval */
        if (agc->counter >= agc->samplerate/20) 
        {
            if (agc->sample_max > agc->threshold)        /* speaking? */
            {
            	  //gain_new = (240.0/256)*agc->ipeak/agc->sample_max *(1<<Q_IGAIN);
                gain_new = agc->ipeak * 240  / agc->sample_max;
                
                if (agc->silence_counter > 40)  /* pause -> speaking */
                    agc->igain += ( gain_new - agc->igain) >> 1;
                else
                    agc->igain += ( gain_new - agc->igain) >> 4 ;/// 20;
				
                agc->silence_counter = 0;
            }
            else   /* silence */
            {
                agc->silence_counter++;
                /* silence > 20*50ms = 1s: reduce gain */
    //            if ((agc->igain > 256) && (agc->silence_counter >= 5))
   //                 agc->igain = ((int)agc->igain * 194) >> Q_IGAIN;
            }
            agc->counter = 0;
            agc->sample_max = 1;
        }
        pcm = (((int)buffer[i] * agc->igain) >> Q_IGAIN);
	    bufout[i] = (short)pcm;
    }
}

void tdagc_process(void *buf, int len)
{
	td_agc_process((short*)buf, len/2);
}



