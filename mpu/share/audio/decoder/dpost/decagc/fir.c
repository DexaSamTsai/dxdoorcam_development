/*
 * fir.c
 *
 *
 *
 */

#include "includes.h"
#include "fir.h"

fir_t *fir_init(int order, int *tabs)
{
#if (DEFAULT_ORDER==64)
	static int default_tabs[DEFAULT_ORDER+1] =
	{
		-12,	-8,		0,		11,		20,		25,		19,		0,
		-27,	-54,	-64,	-47,	0,		66,		124,	145,
		104,	0,		-138,	-257,	-296,	-211,	0,		284,
		537,	635,	472,	0,		-740,	-1622,	-2460,	-3061,
		13118,	-3061,	-2460,	-1622,	-740,	0,		472,	635,
		537,	284,	0,		-211,	-296,	-257,	-138,	0,
		104,	145,	124,	66,		0,		-47,	-64,	-54,
		-27,	0,		19,		25,		20,		11,		0,		-8,
		-12
	};

	static short state[DEFAULT_ORDER+1] =
	{
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0
	};
#else
	static int default_tabs[DEFAULT_ORDER+1] =
	{
		49,		81,		109,	0,		-413,	-1181,	-2140,	-2950,
		13073,	-2950,	-2140,	-1181,	-413,	0,		109,	81,
		49
	};

	static short state[DEFAULT_ORDER+1] =
	{
		0,	0,	0,	0,	0,	0,	0,	0,
		0,	0,	0,	0,	0,	0,	0,	0,
		0
	};
#endif


	int i;

	static fir_t fir;

	if((order==0)||(tabs==NULL))
	{
		fir.order = DEFAULT_ORDER;
		fir.tabs = default_tabs;
		fir.state = state;
	}
	else
	{
		fir.order = order;
		fir.tabs = tabs;
		fir.state = state;
	}

	for(i=0; i<fir.order; i++)
	{
		state[i]=0;
	}

	return &fir;
}

void fir_process(short *in, short *out, int frame_size, fir_t *fir)
{
	int i,j;
	for(j=0; j<frame_size; j++)
	{
		int yn=0;
		fir->state[fir->order] = in[j];
		for(i=0; i<fir->order;i++)
		{
			yn += fir->state[i]*fir->tabs[i]>>FIXPOINT_SHIFT;
			fir->state[i]=fir->state[i+1];
		}
		yn += fir->state[i]*fir->tabs[i]>>FIXPOINT_SHIFT;
		out[j] = yn;
	}
}

void fir_destroy(fir_t *fir)
{
	//nothing to do
}


