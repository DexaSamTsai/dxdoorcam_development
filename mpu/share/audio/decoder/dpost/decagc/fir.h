/*
 * fir.h
 *
 *
 *
 */

#ifndef FIR_H_
#define FIR_H_


#define FIXPOINT_SHIFT 14
#define DEFAULT_ORDER 16

typedef struct fir_s
{
	int order;
	int *tabs;
	short *state;
}fir_t;

fir_t *fir_init(int order, int *tabs);
void fir_process(short *in, short *out, int frame_size, fir_t *fir);
void fir_destroy(fir_t *fir);

#endif /* FIR_H_ */
