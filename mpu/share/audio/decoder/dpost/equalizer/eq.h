#ifndef __EQ_H__
#define __EQ_H__

#ifdef __cplusplus
extern "C" {
#endif

typedef struct
{
	s64 wq[AF_NCH][KM][2];      // Circular buffer for W data
	t_eq_params para;
	int volume;     //0~1024
	t_eq_settings settings;
} t_eq_cfg;


int libeq_setvol(t_eq_cfg* peq, int volume);
int libeq_process(t_eq_cfg* peq, short *data, int numsamples, int nchannels);

#ifdef __cplusplus
}
#endif



#endif
