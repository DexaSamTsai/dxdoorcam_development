#include "includes.h"
#include "eq.h"

static t_eq_cfg s_eqstrc;
static volatile t_eq_params cur_para={0};

static s32 liba_eq_init(void)
{
	memset(&s_eqstrc, 0, sizeof(t_eq_cfg));
	memset((void *)(&cur_para), 0, sizeof(cur_para));
	libeq_setvol(&s_eqstrc, T_EQ_MAX_VOLUME);
	g_liba_dcfg.eq_para = (t_eq_params *)(&cur_para);

    return 0;
}



static void liba_eq_process(void * data, int len)
{
	if(cur_para.pending_update){
		cur_para.pending_update = 0;
		memcpy(&s_eqstrc.para, (void *)(&cur_para), sizeof(t_eq_params));	
	}

	if(cur_para.valid_bands == 0){
		return ;
	}

	libeq_process(&s_eqstrc, data, len/g_liba_dcfg.inp.ch/2, g_liba_dcfg.inp.ch);
}


__adecpost_cfg t_adecpost_cfg g_adecpost_eq_cfg ={
	.alg = AUDIO_DPOST_EQ,  
	.post_init = liba_eq_init,
	.post_process = liba_eq_process,
};
