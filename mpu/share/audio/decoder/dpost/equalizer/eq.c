#include "includes.h"
#include "eq.h"

#define FIX_TO_FLOAT(f, p) (((long long)(f)) >> (p))

int libeq_setvol(t_eq_cfg* peq, int volume)
{
	peq->volume = volume;
	return 0;
}

static s64 equalize(t_eq_cfg* peq, const s32 *gain, short in, int ci, int volume)
{
	int k;

	s64 yt=in * volume / T_EQ_MAX_VOLUME;

	for (k=0;k<peq->para.valid_bands;k++)
	{
		//IIR filter 
		s64 *wq = peq->wq[ci][k];
		s64 w=FIX_TO_FLOAT(yt*peq->para.b_weights[k][0], FIXPOINT_B) + FIX_TO_FLOAT((wq[0]*peq->para.a_weights[k][0] + wq[1]*peq->para.a_weights[k][1]), FIXPOINT_A);
		yt = yt + FIX_TO_FLOAT((w + FIX_TO_FLOAT(wq[1]*peq->para.b_weights[k][1], FIXPOINT_B))
			*gain[k], FIXPOINT_G);
		wq[1] = wq[0];
		wq[0] = w;
	}
	
	if (yt > 32767)
	{
		yt = 32767;
	}
	else if (yt < -32768)
	{
		yt = -32768;
	}
	
	return yt; 
}


int libeq_process(t_eq_cfg* peq, short *data, int numsamples, int nchannels)
{
	int ci=nchannels;

	int volume = peq->volume;

	if (volume > peq->para.max_volume)
	{
		volume = peq->para.max_volume;
	}

	while (ci--)
	{
		const s32 *g=peq->para.gain[ci];
		short *tmp=data+ci;
		short *endin=tmp+nchannels*numsamples;

		while (tmp<endin)
		{
			*tmp=(short)equalize(peq, g, *tmp, ci, volume);
			tmp+=nchannels;
		}
	}

	return numsamples;
}

