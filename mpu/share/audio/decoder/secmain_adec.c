#include "includes.h"


#if 0
#define INLIST_NUM 4
ADEC_ALLOCATE(INLIST_NUM*INBUF_ONELEN, INLIST_NUM)
#endif

static volatile u32 adec_started = 0;


static s32 adec_irq_handler(void * arg);

static u32 aplay_handle_cmd(u32 cmdID,u32 arg)
{
	s32 ret;
	t_adec_inpara *inp;

	switch(cmdID)
	{
		case MPUCMD_APLAY_START:
			syslog(LOG_DEBUG, "MPUCMD_APLAY_START\n");
			libadec_start();
			adec_started  = 1;
			break;

		case MPUCMD_APLAY_STOP:
			liba_dhw_stop();
			adec_started  = 0;
			g_aec_enabled  = 0;
			syslog(LOG_DEBUG, "MPUCMD_APLAY_STOP\n");
			break;

		case MPUCMD_APLAY_INIT:
			if(adec_started)
				return 0;
			inp = (t_adec_inpara *)arg;
#if 0
			memset((void*)&g_liba_dcfg,0,sizeof(g_liba_dcfg));
			ADEC_CONFIG();
#else
			g_liba_dcfg.AFI_BUF0 |= BIT31;
			g_liba_dcfg.AFI_BUF1 |= BIT31;
			g_liba_dcfg.inbuf_addr |= BIT31;
#endif
			memcpy((void*)&g_liba_dcfg.inp,(void*)inp,sizeof(t_adec_inpara));
			syslog(LOG_DEBUG, "MPUCMD_APLAY_INIT\n");

			if(inp->agc.en || inp->compressor_en){
				//F6 cannot do AGC because its output frame size is not the multiples of 64 (required by the algo)
				if(inp->ch != 1 || inp->fmt == AUDIO_FMT_F6){
					syslog(LOG_WARNING,"set audio decoder postprocess algo AUDIO_DPOST_AGC fail,\
									AGC only support MONO and cannot be F6 format\n");
				}else{
					extern u32 adecpost_cfg_start;
					extern u32 adecpost_cfg_end;
					t_adecpost_cfg *p_adecpost_cfg = (t_adecpost_cfg *)(&adecpost_cfg_start);
					g_liba_dcfg.post_init = NULL;
					g_liba_dcfg.post_process = NULL;
					for(;p_adecpost_cfg < (t_adecpost_cfg*)(&adecpost_cfg_end);p_adecpost_cfg ++){
						if(p_adecpost_cfg->alg == AUDIO_DPOST_AGC){
							g_liba_dcfg.post_init = p_adecpost_cfg->post_init;
							g_liba_dcfg.post_process = p_adecpost_cfg->post_process;
							syslog(LOG_INFO, "set audio decoder postprocess algo AUDIO_DPOST_AGC sucessfully\n");
							break;
						}
					}
					if(g_liba_dcfg.post_process == NULL){
						syslog(LOG_WARNING, "set audio decoder postprocess algo AUDIO_DPOST_AGC fail, \
										adec agc is not enabled\n");
					}
				}
			}

			if(inp->eq_en){
				extern u32 adecpost_cfg_start;
				extern u32 adecpost_cfg_end;
				t_adecpost_cfg *p_adecpost_cfg = (t_adecpost_cfg *)(&adecpost_cfg_start);
				g_liba_dcfg.eq_init = NULL;
				g_liba_dcfg.eq_process = NULL;
				for(;p_adecpost_cfg < (t_adecpost_cfg*)(&adecpost_cfg_end);p_adecpost_cfg ++){
					if(p_adecpost_cfg->alg == AUDIO_DPOST_EQ){
						g_liba_dcfg.eq_init = p_adecpost_cfg->post_init;
						g_liba_dcfg.eq_process = p_adecpost_cfg->post_process;
						syslog(LOG_INFO, "set audio decoder postprocess algo AUDIO_DPOST_EQ sucessfully\n");
						break;
					}
				}
				if(g_liba_dcfg.eq_process == NULL){
					syslog(LOG_WARNING, "set audio decoder postprocess algo AUDIO_DPOST_EQ fail, \
									adec equalizer is not enabled\n");
				}
			}else{
				g_liba_dcfg.eq_init = NULL;
				g_liba_dcfg.eq_process = NULL;
			}

#ifdef AI1REC_AI2PLAY
			ret = irq_request(IRQ_BIT_AI1, adec_irq_handler, NULL);
#else
			ret = irq_request(IRQ_BIT_AI0, adec_irq_handler, NULL);
#endif
			if(ret < 0){
				syslog(LOG_ERR, "ERROR:MPUCMD_APLAY_INIT irq_request failed %d\n",ret);
				return -1;
			}

			ret = libadec_init();	
			if(ret < 0){
#ifdef AI1REC_AI2PLAY
				irq_free(IRQ_BIT_AI1);
#else
				irq_free(IRQ_BIT_AI0);
#endif
			}
			return ret;

		case MPUCMD_APLAY_EXIT:
#ifdef AI1REC_AI2PLAY
			irq_free(IRQ_BIT_AI1);
#else
			irq_free(IRQ_BIT_AI0);
#endif
			libadec_exit();
			syslog(LOG_DEBUG, "MPUCMD_APLAY_EXIT\n");

			break;

		case MPUCMD_APLAY_GETFRM: 
			syslog(LOG_DEBUG, "MPUCMD_APLAY_GETFRM\n");
			if(adec_started == 0)
				return 0;
			return (u32)libadec_inbuf_get();

		case MPUCMD_APLAY_ADDFRM: 
			syslog(LOG_DEBUG, "MPUCMD_APLAY_ADDFRM\n");
			if(adec_started == 0)
				return 0;
			libadec_frm_add((FRAME *)arg);
			return 0; 

		case MPUCMD_APLAY_SETEQ: 
			syslog(LOG_DEBUG, "MPUCMD_APLAY_SETEQ\n");
			if(!g_liba_dcfg.inp.eq_en){
				syslog(LOG_WARNING, "WARNING: EQ is not enabled, please set padec_inpara->eq_en = 1 \
								 and call libadec_setEQ after libadec_init\n");
				return -1;
			}

			if(!g_liba_dcfg.eq_para){
				syslog(LOG_ERR, "ERROR: libadec EQ not initialized correctly,g_liba_dcfg.eq_para = NULL\n");
				return -1;
			}

			memcpy((void*)g_liba_dcfg.eq_para,(void *)arg,sizeof(t_eq_params));
			t_eq_params *para = g_liba_dcfg.eq_para;
			syslog(LOG_INFO,"g_liba_dcfg.eq_para is 0x%x\n",g_liba_dcfg.eq_para);
			syslog(LOG_INFO,"para.valid_bands = %d\n",para->valid_bands);
			syslog(LOG_INFO,"para.a_weights[0][0] = %d\n",para->a_weights[0][0]);
			syslog(LOG_INFO,"para.a_weights[0][1] = %d\n",para->a_weights[0][1]);
			syslog(LOG_INFO,"para.a_weights[1][0] = %d\n",para->a_weights[1][0]);
			syslog(LOG_INFO,"para.a_weights[1][1] = %d\n",para->a_weights[1][1]);
			syslog(LOG_INFO,"para.a_weights[2][0] = %d\n",para->a_weights[2][0]);
			syslog(LOG_INFO,"para.a_weights[2][1] = %d\n",para->a_weights[2][1]);
			syslog(LOG_INFO,"para.b_weights[0][0] = %d\n",para->b_weights[0][0]);
			syslog(LOG_INFO,"para.b_weights[0][1] = %d\n",para->b_weights[0][1]);
			syslog(LOG_INFO,"para.b_weights[1][0] = %d\n",para->b_weights[1][0]);
			syslog(LOG_INFO,"para.b_weights[1][1] = %d\n",para->b_weights[1][1]);
			syslog(LOG_INFO,"para.b_weights[2][0] = %d\n",para->b_weights[2][0]);
			syslog(LOG_INFO,"para.b_weights[2][1] = %d\n",para->b_weights[2][1]);
			syslog(LOG_INFO,"para.gain[0][0] = %d\n",para->gain[0][0]);
			syslog(LOG_INFO,"para.gain[0][1] = %d\n",para->gain[0][1]);
			syslog(LOG_INFO,"para.gain[0][2] = %d\n",para->gain[0][2]);
			return 0; 

		default:
			return 0;
		}

	return 0;
}


static s32 audio_play(void)
{
	s32 err;
	if(adec_started){
		err = libadec_update_output_internal();
		if(err){
			//error, no output, don't need redo
			return 0;
		}else{
			//no error, need retry
			return 1;
		}
	}
	return 0;
}

#define TASK_STACKSIZE (1024*3)
static u32 _task_stack_adec[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_adec_cfg = {
	.name = "adec",
	.prio = 16,
	.stk = _task_stack_adec,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_APLAY,
	.app_cmdhandler = aplay_handle_cmd,
	.app_process = audio_play,
};

static s32 adec_irq_handler(void * arg)
{
	libadec_irq();
	task_signal(&g_ba2app_adec_cfg); //let app_process() do
	return 0;
}

