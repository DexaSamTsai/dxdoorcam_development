#include "includes.h"


#define AI_TAKE_EFFECT() \
do{\
	WriteReg32(AI_PLAY_TAKE_EFFECT,1);\
}while(0)

#define AI_PLAY_RIGHT0_DONE	BIT6
#define AI_PLAY_RIGHT1_DONE	BIT7
#define AI_PLAY_LEFT0_DONE	BIT4
#define AI_PLAY_LEFT1_DONE	BIT5


extern int asys_reset;
extern volatile int liba_ehw_started;
extern volatile int liba_dhw_started;
volatile int dabuf_sel;

static void deinterleave_pcm( u32 d_addr,u32 s_addr, u32 len)
{
	s16 * s = (s16 *)s_addr;
	s16 * d = (s16 *)d_addr;

	s32 i;

	for(i=0; i<len/2; i++){
		d[i]=s[2*i];//d[2*i] = s[i];
		d[i+len/2]=s[2*i+1];//d[2*i+1] = s[i+len/2];
	}
	return;
}

static void adec_store_aec_refdata(u8 *src)
{
	t_cbuf_pos wpos;
	s32 ret;
	ret = liba_cbuf_get_wpos((t_cbuf_ctrl*)&g_AECrefbuf,DA_BUF_SIZE,&wpos);		
	if(ret == 0){
//		lowlevel_putc('<');
		memcpy((u8*)wpos.pos0, src, wpos.len0);
		liba_cbuf_update_wpos((t_cbuf_ctrl*)&g_AECrefbuf,DA_BUF_SIZE);
	}else{
		lowlevel_putc('F');
	}
}

static void putdata_to_dabuffer(u8* dst)
{
	t_cbuf_ctrl *dcbuf = &g_liba_dcfg.obuf;
	t_cbuf_pos rpos;
	s32 ret;
	s32 needsz = DA_BUF_SIZE;

	if(g_aec_enabled){
		if(liba_cbuf_frame_aligned((t_cbuf_ctrl*)&g_AECrefbuf)){
			adec_store_aec_refdata(dst);
		}else{
			lowlevel_putc('s');
		}
	}

	ret = liba_cbuf_get_rpos(dcbuf, needsz, &rpos);
	if(ret < 0 ){
	//	lowlevel_putc('n');
		syslog(LOG_NOTICE,"n");
		memset(dst,0,needsz);	
	}else{
		if(g_liba_dcfg.inp.ch == 1){ //mono
			if(g_liba_dcfg.inp.aichannel==1){
				//only send data to left channel
				memcpy(dst, (u8*)rpos.pos0, needsz);
				//memset((void*)g_liba_dcfg.AFI_BUF0+needsz, 0, needsz);
			}else if(g_liba_dcfg.inp.aichannel==2){
				//only send data to right channel
				//memset((void*)g_liba_dcfg.AFI_BUF0, 0, needsz);
				memcpy(dst + needsz, (u8*)rpos.pos0, needsz);
			}else if(g_liba_dcfg.inp.aichannel==0){
				// send data to both left and right channel
				memcpy(dst, (u8*)rpos.pos0, needsz);
				memcpy(dst + needsz, (u8*)rpos.pos0, needsz);
			}
			/*g_liba_dcfg.obuf length is multiples of DA_BUF_SIZE, 
			  so here rpos.len1=0, don't need check*/
		}else{//stereo
			deinterleave_pcm(dst,rpos.pos0,needsz/2);
		}
		liba_cbuf_update_rpos(dcbuf,needsz);
	}
}


void libadec_irq(void) 
{
	volatile u32 sta;

//	lowlevel_putc('i');
#ifndef I2SIRQ_MEMCOPY
	if(dabuf_sel != -1){
		lowlevel_putc('N');
	}
#endif
	sta = ReadReg32(AI_PLAY_INT_STATUS);
	if(sta & AI_PLAY_LEFT0_DONE){		
#ifndef I2SIRQ_MEMCOPY
		dabuf_sel = 0;
#else
		putdata_to_dabuffer((u8*)g_liba_dcfg.AFI_BUF0);
#endif
	}else if(sta & AI_PLAY_LEFT1_DONE){
#ifndef I2SIRQ_MEMCOPY
		dabuf_sel = 1;
#else
		putdata_to_dabuffer((u8*)g_liba_dcfg.AFI_BUF1);
#endif
	}else{
		return;
	}

	WriteReg32(AI_PLAY_INT_STATUS, 0x0000030);
	if(g_liba_dcfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(AI_PLAY_CTL, 0x468);
	}else{
		WriteReg32(AI_PLAY_CTL, 0x168);
	}
}

int liba_dhw_stop(void)
{
//	WriteReg32(AI_PLAY_INT_MSK, 0xff); 
//	WriteReg32(AI_PLAY_CTL, 0x168);
	WriteReg32(AI_PLAY_CTL, 0x0);
	AI_TAKE_EFFECT(); 
	// Disable AI interrupt
//	_irq_aplay_disable();
	liba_dhw_started = 0;
	if(liba_ehw_started == 0){
		asys_reset = 0;
	}

		
	return 0;
}

int liba_dhw_exit(void)
{
#ifdef AI1REC_AI2PLAY
	SC_RESET1_SET(AI1);
	SC_RRESET1_SET(AI1);
	SC_CLK2_DISABLE(AI1);
#else //AI2REC_AI1PLAY
	SC_RESET1_SET(AI0);
	SC_RRESET1_SET(AI0);
	SC_CLK2_DISABLE(AI0);
#endif

	audio_hw_disable(g_liba_dcfg.inp.aimode==AI_CDC_MASTER,g_liba_dcfg.inp.en_mclk);
	return 0;
}


int liba_dhw_init(void)
{
	s32 ret = 0;

	audio_hw_enable(g_liba_dcfg.inp.aimode==AI_CDC_MASTER,g_liba_dcfg.inp.en_mclk);

#ifdef AI1REC_AI2PLAY
	SC_CLK2_EN(AI1);
	SC_RESET1_SET(AI1);
	SC_RESET1_RELEASE(AI1);
	SC_RRESET1_SET(AI1);
	SC_RRESET1_RELEASE(AI1);
#else //AI2REC_AI1PLAY
	SC_CLK2_EN(AI0);
	SC_RESET1_SET(AI0);
	SC_RESET1_RELEASE(AI0);
	SC_RRESET1_SET(AI0);
	SC_RRESET1_RELEASE(AI0);
#endif

#ifdef AI1REC_AI2PLAY
	if(g_liba_dcfg.inp.aimode==AI_CDC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x2<<AIPSC_BASE));
	}else if(g_liba_dcfg.inp.aimode==AI_REC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0xa<<AIPSC_BASE));
	}else if(g_liba_dcfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x13<<AIPSC_BASE));
	}else{
		syslog(LOG_WARNING, "invalid g_liba_dcfg.inp.aimode=0x%x\n ",g_liba_dcfg.inp.aimode);
	}
#else
	if(g_liba_dcfg.inp.aimode==AI_CDC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x0<<AIPSC_BASE));
	}else if(g_liba_dcfg.inp.aimode==AI_REC_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x11<<AIPSC_BASE));
	}else if(g_liba_dcfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(REG_SC_I2SPSC,(ReadReg32(REG_SC_I2SPSC)&~(0x1f<AIPSC_BASE))|(0x8<<AIPSC_BASE));
	}else{
		syslog(LOG_WARNING, "invalid g_liba_dcfg.inp.aimode=0x%x\n ",g_liba_dcfg.inp.aimode);
	}
#endif

	syslog(LOG_INFO, "liba_dhw_init REG_SC_I2SPSC=0x%x\n",ReadReg32(REG_SC_I2SPSC));

	if(g_liba_dcfg.inp.aimode==AI_PLY_MASTER)
		ret = ai_set_sr(g_liba_dcfg.ai_sr, AI_PLAY_MODE_SET);


	// IIS setting
	WriteReg32(AI_PLAY_LADDR, g_liba_dcfg.AFI_BUF0-0x90000000);
	if(g_liba_dcfg.inp.ch == MONO){
#if 1
			//right channel not use the same buffer as the left, assuming the data in left and 
			//right channel are different. In order not to affect AEC algorithm, 
			//in AREC_ALLOCATE_STEREO, g_ad_ppbuf size is enlarged to (DA_BUF_SIZE*2*2) 
			//to reserve space for AI right channel data
			WriteReg32(AI_PLAY_RADDR, (g_liba_dcfg.AFI_BUF0 + g_liba_dcfg.AFI_BUFLEN)- 0x90000000);
			WriteReg32(AI_PLAY_BUF_SET, ((g_liba_dcfg.AFI_BUFLEN>>2) << 16) 
									+ (g_liba_dcfg.AFI_BUF1 - g_liba_dcfg.AFI_BUF0));
#else
		WriteReg32(AI_PLAY_RADDR, g_liba_dcfg.AFI_BUF0-0x90000000);
		WriteReg32(AI_PLAY_BUF_SET, (((g_liba_dcfg.AFI_BUFLEN>>2) << 16) + (g_liba_dcfg.AFI_BUF1 - g_liba_dcfg.AFI_BUF0)));
#endif
	}else if(g_liba_dcfg.inp.ch == STEREO){
		WriteReg32(AI_PLAY_RADDR, (g_liba_dcfg.AFI_BUF0+g_liba_dcfg.AFI_BUFLEN/2) - 0x90000000);
		WriteReg32(AI_PLAY_BUF_SET, (((g_liba_dcfg.AFI_BUFLEN>>3) << 16) + (g_liba_dcfg.AFI_BUF1 - g_liba_dcfg.AFI_BUF0)));
	}

	//only enable interrupt of left channel
	WriteReg32(AI_PLAY_INT_MSK, 0xcf); 
	
//	AI_TAKE_EFFECT(); 

	memset((void*)g_liba_dcfg.AFI_BUF0, 0, g_liba_dcfg.AFI_BUFLEN);
	memset((void*)g_liba_dcfg.AFI_BUF1, 0, g_liba_dcfg.AFI_BUFLEN);
#if 0
	if(g_aec_enabled){
		debug_printf("libadec aec enabled, store refdata\n");
		adec_store_aec_refdata((u8*)g_liba_dcfg.AFI_BUF0);
		adec_store_aec_refdata((u8*)g_liba_dcfg.AFI_BUF1);
	}
#endif

//////////////////////////////////////////////////////////////
	u32 regaddr, regval;
	regaddr = AI_PLAY_MODE_SET;
	regval = ReadReg32(regaddr);
	syslog(LOG_INFO, "0x%x=0x%x\n", regaddr, regval);
	
	regaddr = AI_PLAY_LADDR;
	regval = ReadReg32(regaddr);
	syslog(LOG_INFO, "0x%x=0x%x\n", regaddr, regval);

	regaddr = AI_PLAY_RADDR;
	regval = ReadReg32(regaddr);
	syslog(LOG_INFO, "0x%x=0x%x\n", regaddr, regval);

	regaddr = AI_PLAY_BUF_SET;
	regval = ReadReg32(regaddr);
	syslog(LOG_INFO, "0x%x=0x%x\n", regaddr, regval);

	regaddr = AI_PLAY_INT_MSK;
	regval = ReadReg32(regaddr);
	syslog(LOG_INFO, "0x%x=0x%x\n", regaddr, regval);
//////////////////////////////////////////////////////////////

	memset((void*)g_liba_dcfg.AFI_BUF0, 0, g_liba_dcfg.AFI_BUFLEN);
	memset((void*)g_liba_dcfg.AFI_BUF1, 0, g_liba_dcfg.AFI_BUFLEN);
	syslog(LOG_INFO, "audio decoder hardware init OK!\n");
	dabuf_sel = -1;
	liba_dhw_started = 1;
	if(!asys_reset){
		asys_reset = 1;
	}

#if 1
//	memset((void*)g_liba_dcfg.inbuf_addr,0,g_liba_dcfg.inbuf_size);
//	memset((void*)g_liba_dcfg.obuf_start,0,g_liba_dcfg.obuf_len);
//	g_liba_dcfg.obuf_wp = 0;
//	g_liba_dcfg.obuf_rp = 0;
	liba_cbuf_reset(&g_liba_dcfg.obuf);
#endif

	return ret;
}

int liba_dhw_start(void)
{
#if 0
    //move these to liba_dhw_init
	memset((void*)g_liba_dcfg.inbuf_addr,0,g_liba_dcfg.inbuf_size);
//	memset((void*)g_liba_dcfg.obuf_start,0,g_liba_dcfg.obuf_len);
//	g_liba_dcfg.obuf_wp = 0;
//	g_liba_dcfg.obuf_rp = 0;
	liba_cbuf_reset(&g_liba_dcfg.obuf);
#endif

	//	 Enable AI interrupt
	if(g_liba_dcfg.inp.aimode==AI_PLY_MASTER){
		WriteReg32(AI_PLAY_CTL, 0x468);
	}else{
	// enable playback, left channel, right channel, playback slave or master
		WriteReg32(AI_PLAY_CTL, 0x168);	//negedge and no delay
	}

	AI_TAKE_EFFECT(); 

	return 0;
}


