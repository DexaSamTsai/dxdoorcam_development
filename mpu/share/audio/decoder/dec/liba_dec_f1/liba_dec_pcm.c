#ifdef CONFIG_SIMMPU
#include "includes.h"
#include "mpu_includes.h"
#else
#include "includes.h"
#endif

static int pcm_decode_init(u32 frmsz);
static int pcm_decode_process(void *bufout,unsigned int *out_len, void *bufin, unsigned int *in_len);

__adecoder_cfg t_adecoder_cfg g_adecoder_f1_cfg = {
	.afmt = AUDIO_FMT_F1,
	.infrmsz = 512,
	.outfrmsz = 512,
	.init = pcm_decode_init,
	.process = pcm_decode_process,
};

static int pcm_decode_init(u32 frmsz){

	if(frmsz){
		g_adecoder_f1_cfg.infrmsz = frmsz;
		g_adecoder_f1_cfg.outfrmsz = frmsz;
	}

	return 0;
}


static int pcm_decode_process(void *bufout,unsigned int *out_len, void *bufin, unsigned int *in_len)
{
	int declen;
	
	declen = *out_len;
	memcpy((short *)bufout, (short *)bufin, declen);
	*in_len -= declen;
	
	return 0;
}



