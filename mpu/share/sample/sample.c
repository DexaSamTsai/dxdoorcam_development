

/**************************************************************************
 *                                                                        
 *         Copyright (c) 2007, 2008 by Ommnivision Technology Co., Ltd.            
 *                                                                         
 *  This software is copyrighted by and is the property of Ommnivision       
 *  Technology Co., Ltd. All rights are reserved by Ommnivision Technology   
 *  Co., Ltd. This software may only be used in accordance with the        
 *  corresponding license agreement. Any unauthorized use, duplication,    
 *  distribution, or disclosure of this software is expressly forbidden.   
 *                                                                         
 *  This Copyright notice MUST not be removed or modified without prior    
 *  written consent of Ommnivision Technology Co., Ltd.                      
 *                                                                         
 *  Ommnivision Technology Co., Ltd. reserves the right to modify this       
 *  software without notice. 
 *
 **************************************************************************/
#include "includes.h"

static volatile u8 sample_run = 0;
static int run_cnt = 0;

static u32 sample_handle_cmd(u32 cmdID,u32 param_addr)
{
	if(run_cnt == 1){
		libmpu_clear_event(MPUEVT_VS0_NOTEMPTY);
	}

	switch(cmdID)
	{
	case MPUCMD_SAMPLE_RUN:
	//	debug_printf("SAMPLE_DCPU_CMD_RUN\n");

		sample_run = 1;
		run_cnt ++;

		return 0x11111111;
		break;

	case MPUCMD_SAMPLE_STOP:
	//	debug_printf("SAMPLE_DCPU_CMD_STOP\n");
		sample_run = 0;
		return 0x22222222;
		break;

	default:
		return 0;
	}
	return 0;
}

static int sample_process(void)
{
	if(sample_run){
//		debug_printf("-");
//		{volatile int d;for(d=0;d<50000;d++);}
		if(run_cnt >= 5){
			libmpu_set_event(MPUEVT_VS0_NOTEMPTY);
			run_cnt = 0;
		}
	}

	return 0;
}

#define TASK_STACKSIZE 1024
static u32 _task_stack_sample[TASK_STACKSIZE/4];
__ba2app_cfg t_task g_ba2app_sample_cfg = {
	.name = "sample",
	.prio = 30,
	.stk = _task_stack_sample,
	.stack_size = TASK_STACKSIZE/4,

	.mpucmd_type = MPUCMD_TYPE_SAMPLE,
	.app_cmdhandler = sample_handle_cmd,
	.app_process = sample_process,
};
