#include "includes.h"
/* TODO: sample setting, should moved to board_xxx.c

   下面这个数据结构描述了这样一个datapath(print by libdatapath_print()):

   ->: single path
   +>: concurrent path
   *>: sequential path

SNR2736(1920x1080) ->MIPIRX0 ->MIPICIF0 ->RGBIR -> IDC0 ->ISP0 +>FB2 *>ECIF0 ->YUVSCL1(1920x1088) ->OSD4L0->VIF1 +>VE0 
                                                             |                                                 | +>IMG2 
                                                             | +>YUVSCL0(1280*720) ->FB4 *>ECIF0 +>OSD4L0 ->VIF1 ->VE1 
															                                     +>YUVSCL1(320*180)->FB3
   */

/*memory config
                            addr_start   end(not include) start_offset size
	SRAM_START				0x10000000	     
    CODE  	  				0x10000000 --- (0x10060000)
	VBUF_BASE 				0x10060000 --- (0x10081c00) 
	OSD0_BASE 				0x10081c00 --- (0x10089900) 
	MPU_CODE 				0x100a0000 --- (0x10100000) 

	DDR_START 				0x10100000                    DDR offset
	DDR_CODE_SIZE  			0x10100000 --- (0x10900000)   0x000000     	8M 

	VS0_HWBUF				0x10900000 --- (0x10bfd000)   0x0800000    	0x2fd000	
	VS1_HWBUF			 	0x10bfd000 --- (0x10efa000)   		       	0x2fd000
	VS2_HWBUF			 	0x10efa000 --- (0x10f50400)   		       	0x56400
	SENSOR_SCCB_GROUP      	0x10f50400 --- (0x10f51400)   		        4K
	FREE		           	0x10f51400 --- (0x11100000)					

	VS0_HWBUF1  			0x11100000 --- (0x113fd000)   0x1000000		0x2fd000(1920*1088*1.5)
	VS1_HWBUF1  			0x113fd000 --- (0x116fa000)   				0x2fd000(1920*1088*1.5)
	VS2_HWBUF1  			0x116fa000 --- (0x11750400)   				0x56400(640*368*1.5)
	FREE                 	0x11750400 --- (0x12100000)	  0x1650400

	FB3_BUF_BASE			0x12100000 --- (0x12259000)	  0x2000000		0x159000(640*368*1.5*4)
	VS3_BS_BUFFER			0x12259000 --- (0x12659000)					4M
	VS0_BS_BUFFER			0x12659000 --- (0x12a59000)					4M	
	VS1_BS_BUFFER			0x12a59000 --- (0x12e59000)					4M
	VS2_BS_BUFFER			0x12e59000 --- (0x13059000)					2M
	FREE					0x13059000 --- (0x13100000)

	TDDNS_MEM1_BASE_ADDR  	0x13100000 --- (0x134fc000)   0x3000000 	0x3fc000(1920*1088*2)	//3D dnoize
	FREE					0x134fc000 --- (0x14100000)

	FB2_BUF0	        	0x14100000 --- (0x146fa000)	  0x4000000     0x5fa000(1920*1088*1.5*2)
	FREE                 	0x146fa000 --- (0x15100000)

	FB2_BUF1	        	0x15100000 --- (0x156fa000)	  0x5000000   	0x5fa000(1920*1088*1.5*2)
	FREE                 	0x156fa000 --- (0x16100000)

	FB4_BUF0 	     	 	0x16100000 --- (0x161ac800)	  0x6000000 	0xac800(640*368*1.5*2)
	FREE 	      			0x161ac800 --- (0x17100000)  

	FB4_BUF1 	     	 	0x17100000 --- (0x171ac800)	  0x7000000 	0xac800(640*368*1.5*2)
	FREE 	      			0x171ac800 --- (0x18100000)  

*/

#define P_HBLK				12		//8
#define PREFCH_REQ_PERIOD	0x200	//0x180
#define DUAL_STREAM			//H.264 1080P(vs0) + 720P(vs1)
#define DUAL_VS_WITHIMG		//H.264 1080P(vs0) + 720P(vs1) + 720P IMG(vs2)

/********** Don't change **********/
#define	DDR_CODE_SIZE		0x800000		//8M

#define	VS0_WIDTH	1920	//VS0 (H.264)		
#define	VS0_HEIGHT	1088
#define VS1_WIDTH	1280	//VS1(H.264),VS2(IMG)
#define VS1_HEIGHT	720	
#define VS3_WIDTH	320		//FB3
#define VS3_HEIGHT	180	

#define MAX_WIDTH	VS0_WIDTH	//Don't change
#define VS0_OUT_CROP_BOTTOM		8 	//VS0 crop bottom 8 pixels to output 1080P

#define	FRAME_RATE		24

///< SRAM memory map
#define	MIPI_LANE_NUM	2
#define PINGPONG_VE_REF
#define	VIF_FIFO_NUM	3	
#define  VIF_BUF_ADDR     			(CONFIG_VBUF_BASE - MEMBASE_SRAM) ///< offset

///< DDR memory map
#define	VS0_BUF_SIZE		(4*1024*1024)		//Don't change
#define	VS1_BUF_SIZE		(4*1024*1024)		//Don't change
#ifdef PINGPONG_VE_REF
#define VS0_HWBUF_SIZE		(0x2fd000)		//1920*1088*1.5
#define VS1_HWBUF_SIZE		(0x151800)		//1280*720*1.5
#else
#define VS0_HWBUF_SIZE		(0x335400)		
#define VS1_HWBUF_SIZE		(0x177000)
#endif
#define VE_STREAM0_HWBUF_BASE  	(MEMBASE_DDR + DDR_CODE_SIZE)
#define VE_STREAM1_HWBUF_BASE  	(VE_STREAM0_HWBUF_BASE + VS0_HWBUF_SIZE)
#define VE_STREAM0_HWBUF1_BASE  (MEMBASE_DDR + 0x1000000)
#define VE_STREAM1_HWBUF1_BASE  (VE_STREAM0_HWBUF1_BASE + VS0_HWBUF_SIZE)
/*SCCB*/
#define SCCB_GRP_BUF			(VE_STREAM1_HWBUF_BASE + VS1_HWBUF_SIZE)
#define SCCB_GRP_BUF_SIZE       (4*1024)

#define	RAW_WIDTH	1920	//
#define	RAW_HEIGHT	1088	//

#define	FB2_FRM_LIST_NUM		4 ///< Don't Change
#define	FB3_FRM_LIST_NUM		4 ///< Don't Change
#define	FB4_FRM_LIST_NUM		4 ///< Don't Change

/*MIMG*/
#define IMG_BUF_START			(MEMBASE_DDR + 0x2000000)	// 32M
#define IMG_BUF_SIZE			(0x400000)
#define IMG_ENC_SIZE			(350*1024)           
#define VE_STREAM0_BSBUF_BASE  	(IMG_BUF_START + IMG_BUF_SIZE)
#define VE_STREAM1_BSBUF_BASE  	(VE_STREAM0_BSBUF_BASE + VS0_BUF_SIZE)

/* 3D Denoise */
//TDDNS need an independent ddr bank for better performance
#define	TDDNS_MEM1_BASE_ADDR  	(MEMBASE_DDR + 0x3000000)	// 48M	
#define	TDDNS_BUF_SIZE			(RAW_WIDTH * RAW_HEIGHT * 2)	
#define AUDIO_REC_BUF			(TDDNS_MEM1_BASE_ADDR + TDDNS_BUF_SIZE)
#define AUDIO_REC_BUF_SIZE		(512*1024)
#define AUDIO_DEC_BUF			(AUDIO_REC_BUF + AUDIO_REC_BUF_SIZE)
#define AUDIO_DEC_BUF_SIZE		(512*1024)
#define AREC_FRMLIST_NUM 20
#define ADEC_FRMLIST_NUM 20


#define FB2_STORE_ENC
#define	FB2_FRM_SIZE			(RAW_WIDTH * RAW_HEIGHT / 8)//actual_size/8
#define FB2_HW_FRM_SIZE			(FB2_FRM_SIZE * 3 / 2)

#define FB2_YBASE_ADDR0			(0x4000000/8) 	// 64M
#define FB2_UVBASE_ADDR0		(FB2_YBASE_ADDR0 + FB2_FRM_SIZE)

#define FB2_YBASE_ADDR1			(0x5000000/8) 	// 80M
#define FB2_UVBASE_ADDR1		(FB2_YBASE_ADDR1 + FB2_FRM_SIZE)

#define FB4_STORE_ENC
#define	FB4_FRM_SIZE			(VS1_WIDTH * VS1_HEIGHT / 8)//actual_size/8
#define FB4_HW_FRM_SIZE			(FB4_FRM_SIZE * 3 / 2)

#define FB4_YBASE_ADDR0			(0x6000000/8) 	// 96M
#define FB4_UVBASE_ADDR0		(FB4_YBASE_ADDR0 + FB4_FRM_SIZE)

#define FB4_YBASE_ADDR1			(0x6800000/8) 	// 104M
#define FB4_UVBASE_ADDR1		(FB4_YBASE_ADDR1 + FB4_FRM_SIZE)

#define FB3_FRM_SIZE			(VS3_WIDTH * VS3_HEIGHT / 8)
#define FB3_HW_FRM_SIZE			(FB3_FRM_SIZE * 3 / 2)

#define FB3_YBASE_ADDR0			(0x7000000/8) 	// 96M
#define FB3_UVBASE_ADDR0		(FB3_YBASE_ADDR0 + FB3_FRM_SIZE)
#define FB3_YBASE_ADDR1			(0x7800000/8) 	// 104M
#define FB3_UVBASE_ADDR1		(FB3_YBASE_ADDR1 + FB3_FRM_SIZE)


static t_dpm_ve dp_ve_vs0;
static t_dpm_ve dp_ve_vs1;
static t_dpm_img dp_img_vs2;

//#define BASE_LINE
//#define META_DBG
#ifdef META_DBG
#define TAG_MAXLEN 32  //32Byte aligned, "OVMS", "OVMZ", "OVME", "FF FF ... FF"
#define META_MAXLEN 128 //32Byte aligned
static unsigned char meta_buf_data[TAG_MAXLEN + META_MAXLEN];
#endif

#define MANUAL_DROP_FRM
static u32 vs_handle_cmd(struct s_dpm_ve * dpm, u32 cmd, u32 arg, u32 * handled)
{
	switch(cmd){
#ifdef MANUAL_DROP_FRM
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRMRATE_SET):
		*handled = 1;
		return vs_set_fps_drop(dpm, arg);
#endif
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
		*handled = 1;
		return dpzoom_scale_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));	
	}
	return 0;
}


#ifdef MANUAL_DROP_FRM
static t_vs_fps_cfg vs0_fps;
static t_vs_fps_cfg vs1_fps;
static t_vs_fps_cfg vs2_fps;
static t_vs_fps_cfg vs3_fps;
#endif

static t_ve_rc_cfg dp_ve_vs1_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 500 * 1024,
	.init_qp = 32,
	.i_p_qp_delta = 2, 
	.max_gop_size = FRAME_RATE * 2,
};


FRAMELIST_DECLARE(vs1_framelist, 60);
static t_dpm_ve dp_ve_vs1 =
{
	DPM_INIT_VE
	.id = 1,
	.ve_ref_buf_base = VE_STREAM1_HWBUF_BASE,
#ifdef PINGPONG_VE_REF
	.ve_ref_buf_base1 = VE_STREAM1_HWBUF1_BASE,
#endif
	.bs_buf_base = VE_STREAM1_BSBUF_BASE,
	.bs_buf_size = VS1_BUF_SIZE,
	.rc_cfg = &dp_ve_vs1_rc,
	.frame_rate = FRAME_RATE,
	.framelist = &vs1_framelist,
	.flag = DPM_FLAG_END,
	.add_sps_head_en = 1,
	.idr_period = 1,
	.bs_buf_skip_th = 100 * 1024,
	.ip_i4_disable = 1,
	.output_width = VS1_WIDTH,
	.output_height = VS1_HEIGHT, 
	.handle_cmd = vs_handle_cmd,
	.prefch_req_period = PREFCH_REQ_PERIOD,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs1_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
	.no_cabac = 1,
#ifdef BASE_LINE
	.profile_idc = 66,
#endif
};

static t_dpm_vif dp_vif1_mode1 =
{
	DPM_INIT_VIF
	.id = 1,
	.out = &dp_ve_vs1,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
	.mbl_fifo_depth = VIF_FIFO_NUM,
	.max_in_width = MAX_WIDTH,
	.yuv420_in = 1,
};

static t_dpm_fb_seqlock fb_seqlock =
{
	.fb_running = NULL,	
};


static t_dpm_fb_buf dp_fb3_buf0;
static t_dpm_fb_buf dp_fb3_buf3 =
{
	.ymem_base = (FB3_YBASE_ADDR1+FB3_HW_FRM_SIZE),
	.uvmem_base = (FB3_UVBASE_ADDR1+FB3_HW_FRM_SIZE),
	.next = &dp_fb3_buf0,
};

static t_dpm_fb_buf dp_fb3_buf2 =
{
	.ymem_base = (FB3_YBASE_ADDR0+FB3_HW_FRM_SIZE),
	.uvmem_base = (FB3_UVBASE_ADDR0+FB3_HW_FRM_SIZE),
	.next = &dp_fb3_buf3,
};

static t_dpm_fb_buf dp_fb3_buf1 =
{
	.ymem_base = FB3_YBASE_ADDR1,
	.uvmem_base = FB3_UVBASE_ADDR1,
	.next = &dp_fb3_buf2,
};

static t_dpm_fb_buf dp_fb3_buf0 =
{
	.ymem_base = FB3_YBASE_ADDR0,
	.uvmem_base = FB3_UVBASE_ADDR0, 
	.next = &dp_fb3_buf1,
};


static t_dpm_fb dp_fb3 = 
{
	DPM_INIT_FB
	.id = 3,
	.enabled = 1,
	.int_mask = 0,
	.data_format = FB_FORMAT_YUV422IN_YUV420STORE,
	.output_fmt = DPM_DATAFMT_YUV420,
	.pingpong_en = 0,
	.yaddr = FB3_YBASE_ADDR0,
	.uvaddr = FB3_UVBASE_ADDR0,
	.offset = 0,
	.bufhead = &dp_fb3_buf0,
#ifdef DUAL_STREAM
	.seqlock = &fb_seqlock,
#endif
	.flag = DPM_FLAG_END,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs3_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif	

};

static t_dpm_yuvscale dp_yuvscale1_mode1 = 
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_fb3,
	.output_width = VS3_WIDTH,
	.output_height = VS3_HEIGHT,
};

static t_dpm_osd4l dp_osd4l_mode1 =
{
	DPM_INIT_OSD4L
	.id = 1,
	.out = &dp_vif1_mode1,
	.bypass = 1,
	.next = &dp_yuvscale1_mode1,
};

static t_dpm_ecif dp_ecif0_mode1 =
{
	DPM_INIT_ECIF
	.id = 0,
	.out = &dp_osd4l_mode1,
	.flag = DPM_FLAG_END,
	.frame_num = 0,
	.p_hblk = P_HBLK,
	.p_vsync = 4,
	.p_v2h = 12,
	.p_h2v = 0,
	.p_h2e = 8,//20,
	.int_mask = 0,	//0: enable irq, 1: disable irq
#ifdef FB4_STORE_ENC
	.data_format = ECIF_FORMAT_YUV420_DEC8,
#else
	.data_format = ECIF_FORMAT_YUV420_NODEC,
#endif
	.handshk = SOFTWARE_HANDSHAKE,//HARDWARE_HANDSHAKE,
	.yuv420in_yuv422out_en = 1,
	.crop_x = 0,
	.crop_y = 0,
	.pingpong_en = 0,
	.yaddr = FB4_YBASE_ADDR0,
	.uvaddr = FB4_UVBASE_ADDR0,
	.offset = 0,
	.cpp0=0,
	.cpp1=0,
};

FRAMELIST_DECLARE(vs2_framelist, 10);

static t_dpm_img dp_img_vs2 =
{
	DPM_INIT_IMG
	.id = 2,
	.buflist_start = IMG_BUF_START,
	.buflist_size = IMG_BUF_SIZE, // img gen buffer. 
	.imgenc_size = IMG_ENC_SIZE, // img encode size, one image frame should be less than imgenc_size 
	.qs = 10,
	.qs_auto = 0,
	.framelist = &vs2_framelist,
	.flag = DPM_FLAG_END,
	.handle_cmd = vs_handle_cmd,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs2_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
};

static t_ve_rc_cfg dp_ve_vs0_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 1000 * 1024,
	.init_qp = 29,
	.i_p_qp_delta = 2, 
	.max_gop_size = FRAME_RATE * 2,
	.min_qp[0] = 1,
	.min_qp[1] = 1,
};
FRAMELIST_DECLARE(vs0_framelist, 60);
static t_dpm_ve dp_ve_vs0 =
{
	DPM_INIT_VE
	.id = 0,
	.ve_ref_buf_base = VE_STREAM0_HWBUF_BASE,
#ifdef PINGPONG_VE_REF
	.ve_ref_buf_base1 = VE_STREAM0_HWBUF1_BASE,
#endif
	.bs_buf_base = VE_STREAM0_BSBUF_BASE,
	.bs_buf_size = VS0_BUF_SIZE,
	.rc_cfg = &dp_ve_vs0_rc,
	.frame_rate = FRAME_RATE,
	.framelist = &vs0_framelist,
#ifdef DUAL_VS_WITHIMG
	.next = &dp_img_vs2,
#else
	.flag = DPM_FLAG_END,
#endif
	.add_sps_head_en = 1,
	.idr_period = 1,
	.bs_buf_skip_th = 100 * 1024,
	.ip_i4_disable = 1,
	.output_width = VS0_WIDTH,
	.output_height = VS0_HEIGHT - VS0_OUT_CROP_BOTTOM,
	.handle_cmd = vs_handle_cmd,
	.prefch_req_period = PREFCH_REQ_PERIOD,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs0_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
#ifdef META_DBG
 	.meta_buf = &meta_buf_data,
	.meta_maxlen = META_MAXLEN,
	.tag_maxlen = TAG_MAXLEN,
#endif
	.no_cabac = 1,
#ifdef BASE_LINE
	.profile_idc = 66,
#endif
	.level_idc = 40,
};

static t_dpm_vif dp_vif1_mode0 =
{
	DPM_INIT_VIF
	.id = 1,
	.out = &dp_ve_vs0,
	.no_kick = 0,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
	.mbl_fifo_depth = VIF_FIFO_NUM,
	.max_in_width = MAX_WIDTH,
	.yuv420_in = 1,
};

static t_dpm_osd4l dp_osd4l_mode0 =
{
	DPM_INIT_OSD4L
	.id = 0,
	.out = &dp_vif1_mode0,
	.bypass = 1,
};

static t_dpm_fb dp_fb2; 
static t_dpm_yuvscale dp_yuvscale0_mode0; 
static t_dpm_yuvscale dp_yuvscale1_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_osd4l_mode0,
	.output_width = VS0_WIDTH,
	.output_height = VS0_HEIGHT,
};

static t_dpm_fb dp_fb4; 
static t_dpm_yuvscale dp_yuvscale0_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 0,
	.out = &dp_fb4,
	.output_width = VS1_WIDTH,
	.output_height = VS1_HEIGHT,
};

static t_dpm_ecif dp_ecif0_mode0 =
{
	DPM_INIT_ECIF
	.id = 0,
	.out = &dp_yuvscale1_mode0,
	.flag = DPM_FLAG_END,
	.frame_num = 0,
	.p_hblk = P_HBLK,
	.p_vsync = 4,
	.p_v2h = 12,
	.p_h2v = 0,
	.p_h2e = 8,
	.int_mask = 0,	//0: enable irq, 1: disable irq
#ifdef FB2_STORE_ENC
	.data_format = ECIF_FORMAT_YUV420_DEC8,
#else
	.data_format = ECIF_FORMAT_YUV420_NODEC,
#endif
	.handshk = SOFTWARE_HANDSHAKE,//HARDWARE_HANDSHAKE,
	.yuv420in_yuv422out_en = 1,
	.crop_x = 0,
	.crop_y = 0,
	.pingpong_en = 0,
	.yaddr = FB2_YBASE_ADDR0,
	.uvaddr = FB2_UVBASE_ADDR0,
	.offset = 0,
	.cpp0=2,
	.cpp1=1,
};

static t_dpm_fb_buf dp_fb2_buf0;
static t_dpm_fb_buf dp_fb2_buf3 =
{
	.ymem_base = (FB2_YBASE_ADDR1 + FB2_HW_FRM_SIZE),
	.uvmem_base = (FB2_UVBASE_ADDR1 + FB2_HW_FRM_SIZE),
	.next = &dp_fb2_buf0,
};

static t_dpm_fb_buf dp_fb2_buf2 =
{
	.ymem_base = (FB2_YBASE_ADDR0 + FB2_HW_FRM_SIZE),
	.uvmem_base = (FB2_UVBASE_ADDR0 + FB2_HW_FRM_SIZE),
	.next = &dp_fb2_buf3,
};

static t_dpm_fb_buf dp_fb2_buf1 =
{
	.ymem_base = FB2_YBASE_ADDR1,
	.uvmem_base = FB2_UVBASE_ADDR1, 
	.next = &dp_fb2_buf2,
};

static t_dpm_fb_buf dp_fb2_buf0 =
{
	.ymem_base = FB2_YBASE_ADDR0,
	.uvmem_base = FB2_UVBASE_ADDR0, 
	.next = &dp_fb2_buf1,
};

static t_dpm_fb dp_fb2 = 
{
	DPM_INIT_FB
	.id = 2,
	.out = &dp_ecif0_mode0,
	.enabled = 1,
	.int_mask = 0,
#ifdef FB2_STORE_ENC
	.data_format = FB_FORMAT_YUV422IN_YUV420OUT_6STORE,
#else
	.data_format = FB_FORMAT_YUV422IN_YUV420STORE,
#endif
	.pingpong_en = 0,
	.yaddr = FB2_YBASE_ADDR0,
	.uvaddr = FB2_UVBASE_ADDR0,
	.offset = 0,
	.bufhead = &dp_fb2_buf0,

	.seqlock = &fb_seqlock,
#ifdef DUAL_STREAM
	.next = &dp_yuvscale0_mode0,
#endif
	.no_dbg = 1,
};

static t_dpm_fb_buf dp_fb4_buf0;
static t_dpm_fb_buf dp_fb4_buf3 =
{
	.ymem_base = (FB4_YBASE_ADDR1 + FB4_HW_FRM_SIZE),
	.uvmem_base = (FB4_UVBASE_ADDR1 + FB4_HW_FRM_SIZE),
	.next = &dp_fb4_buf0,
};

static t_dpm_fb_buf dp_fb4_buf2 =
{
	.ymem_base = (FB4_YBASE_ADDR0 + FB4_HW_FRM_SIZE),
	.uvmem_base = (FB4_UVBASE_ADDR0 + FB4_HW_FRM_SIZE),
	.next = &dp_fb4_buf3,
};

static t_dpm_fb_buf dp_fb4_buf1 =
{
	.ymem_base = FB4_YBASE_ADDR1,
	.uvmem_base = FB4_UVBASE_ADDR1, 
	.next = &dp_fb4_buf2,
};

static t_dpm_fb_buf dp_fb4_buf0 =
{
	.ymem_base = FB4_YBASE_ADDR0,
	.uvmem_base = FB4_UVBASE_ADDR0, 
	.next = &dp_fb4_buf1,
};

static t_dpm_fb dp_fb4 = 
{
	DPM_INIT_FB
	.id = 4,
	.out = &dp_ecif0_mode1,
	.enabled = 1,
	.int_mask = 0,
#ifdef FB4_STORE_ENC
	.data_format = FB_FORMAT_YUV422IN_YUV420OUT_6STORE,
#else
	.data_format = FB_FORMAT_YUV422IN_YUV420STORE,
#endif
	.pingpong_en = 0,
	.yaddr = FB4_YBASE_ADDR0,
	.uvaddr = FB4_UVBASE_ADDR0,
	.offset = 0,
	.bufhead = &dp_fb4_buf0,
#ifdef DUAL_STREAM
	.seqlock = &fb_seqlock,
#endif
	.no_dbg = 1,
};

static t_dpm_ispout dp_ispout0 =
{
	DPM_INIT_ISPOUT
	.id = 0,
	.out = &dp_fb2,
};

static t_dpm_rgbir dp_rgbir;
static t_dpm_ispidc dp_ispidc =
{
	DPM_INIT_ISPIDC
	.id = 0,
	.out = &dp_ispout0,
	.in = &dp_rgbir,
	.dummy_line = 0x10,	// 0x0c
	.dummy_mode = IDC_DUMMY_AUTO_MODE,
#ifdef TDDNS_EN
	.TDdns_en = 1,
	.TDdns_mem1 = (TDDNS_MEM1_BASE_ADDR - MEMBASE_DDR)/8, 
#endif
	.v2d1_hblk = 0x100,//0x100,
	.cpp = 0x0,
#ifdef META_DBG
	.add_meta_en = 1,
#endif
};

static t_dpm_rgbir dp_rgbir = 
{
	DPM_INIT_RGBIR
//	.dummy_manual_en = 1,
//	.dummy_hblk = 0x140,
	.out = &dp_ispidc,
};

static t_dpm_mipicif dp_mipicif0 = 
{
	DPM_INIT_MIPICIF
	.id = 0,
	.out = &dp_rgbir,
};

static t_dpm_mipirx dp_mipirx0 = 
{
	DPM_INIT_MIPIRX
	.id = 0,
	.out = &dp_mipicif0,
	.lane_num = MIPI_LANE_NUM,
	.four_sel = 1,
	.vc0_emb_id = 0x12,
	.vc1_emb_id = 0x12,
	.vc0_emb_use_dt = 0x1e,
	.vc1_emb_use_dt = 0x1e,
	.pnum_int = 0x4,
	.int_en = 0xfffffff,
	.c2d4_en = 0x1,
	.invert_pclk=0x1,
};

static t_libsccb_cfg dp_sccbcfg =
{
	.mode = SCCB_MODE16,
	.buf_addr = SCCB_GRP_BUF,
	.max_len = SCCB_GRP_BUF_SIZE,
	.clk = 200000,
	.rd_mode = RD_MODE_NORMAL,
	.sccb_num = SCCB_NUM_0,
};

static t_dpm_sensor dp_sensor =
{
	DPM_INIT_SENSOR
	.sccb_cfg = &dp_sccbcfg,
	.out = &dp_mipirx0,
	.name = "2736",
};


t_libdatapath_cfg libdatapath_cfg[] =
{
	{
#ifdef CONFIG_FAST_BOOT_EN
//		.fastboot_flag = DATAPATH_FASTBOOT_EN  |DATAPATH_FASTBOOT_DEBUG,
		.fastboot_flag = DATAPATH_FASTBOOT_EN ,
#endif
		.head = &dp_sensor,
	},
};

#ifdef CONFIG_MPU_AENC_EN
AREC_ALLOCATE_STEREO
FRAMELIST_DECLARE(areclist, AREC_FRMLIST_NUM )
t_liba_ecfg g_liba_ecfg =
{
	AREC_CONFIG_STEREO
	.outbuf_addr = AUDIO_REC_BUF,	
	.outbuf_size = AUDIO_REC_BUF_SIZE, 
	.list = &areclist,
};
#endif

#ifdef CONFIG_MPU_ADEC_EN
ADEC_ALLOCATE
FRAMELIST_DECLARE(adeclist, ADEC_FRMLIST_NUM)
t_liba_dcfg g_liba_dcfg =
{
    ADEC_CONFIG
    .inbuf_addr = AUDIO_DEC_BUF,
    .inbuf_size = AUDIO_DEC_BUF_SIZE,
    .list = &adeclist,
};
#endif

