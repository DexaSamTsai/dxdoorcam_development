#include "includes.h"
/* TODO: sample setting, should moved to board_xxx.c

   下面这个数据结构描述了这样一个datapath(print by libdatapath_print()):

   ->: single path
   +>: concurrent path
   *>: sequential path
->SNR9756 ->MIPIRX0->MIPICIF0 ->RGBIR ->IDC0 ->ISP0 +>OSD4L0 ->VIF1 ->VE0 
                                    			    +>YUVSCL2 ->MD0 
   */

/*memory config
                            addr_start  addr_end(not included) start_offset    size
	SRAM_START				0x10000000	     
    CODE  	  				0x10000000 --- (0x10060000)
	VIF_BUF 				0x10060000 --- (0x10076800) 
	MPU_CODE 				0x108a0000 --- (0x10100000) 

	DDR_START 				0x10100000                    		DDR offset
	CODE_SIZE  				0x10100000 --- (0x10900000)   		0x000000       	8M 
	VS0_HWBUF				0x10900000 --- (0x10a77000)			0x800000      	1500k	
	hw ref buf:1280*(720+16*5)*3/2 = 0x177000
	VS0_BS_BUF	  			0x10a77000 --- (0x10c77000)			0x977000      	2M
	MOTION DETECT        	0x10c77000 --- (0x10cb2000)   		0xb77000      	0x3b000(236k,QVGA)
	SENSOR_SCCB_GROUP      	0x10cb2000 --- (0x10cb3000)   		0xbb2000       	4K	//used for sensor initial
	AUDIO_REC_BUF			0x10cb3000 --- (0x10d33000)   		0xbb3000  		512k	
	AUDIO_DEC_BUF			0x10d33000 --- (0x10db3000)			0xc33000 		512k

	TDDNS_MEM1_BASE_ADDR  	0x11100000 --- (0x112c2000)   		0x1000000		0x1c2000(1280 * 720 * 2)//3D dnoize
	IMG_BUF_START			0x11900000 --- 0x12100000			0x1800000		8M
*/

/* default setting */
#define	VS0_WIDTH		1280
#define	VS0_HEIGHT		720
#define	FRAME_RATE		24	

/* private, don't change */
/* max resolution */
#define MAX_WIDTH		VS0_WIDTH	
#define MAX_HEIGHT		VS0_HEIGHT
#define	VIF_FIFO_NUM	3	

///< SRAM memory map
#define	VIF_BUF_ADDR     		(CONFIG_VBUF_BASE - MEMBASE_SRAM) ///< offset
#define OSD4L_MODE0_BASE_ADDR  	(VIF_BUF_ADDR + ((MAX_WIDTH * 16 * 3) / 2) * VIF_FIFO_NUM) ///< Don't change

///< DDR memory map
#define	DDR_BUF_BASE_OFFSET		(0x800000)
/* VS0 */
#define VE_STREAM0_HWBUF_BASE  	(MEMBASE_DDR + DDR_BUF_BASE_OFFSET)
#define VS0_HWBUF_SIZE			(((MAX_WIDTH * 16 * 3) / 2) * ((MAX_HEIGHT/16) + 5))  ///< Don't Change
#define	VS0_BUF_SIZE			(2*1024*1024)
/* MD */
#define MD_MASK_BASE_ADDR    	(VE_STREAM0_HWBUF_BASE + VS0_HWBUF_SIZE + VS0_BUF_SIZE)
#define MD_BUF_SIZE    			(0x3b000)//(0x3a980)///< Don't Change, by default QVGA
/* SCCB */
#define SCCB_GRP_BUF       		(MD_MASK_BASE_ADDR + MD_BUF_SIZE)
#define SCCB_GRP_BUF_SIZE       (4*1024)///< Don't Change
/* Audio */
#define AUDIO_REC_BUF			(SCCB_GRP_BUF + SCCB_GRP_BUF_SIZE) 
#define AUDIO_REC_BUF_SIZE		(512*1024)
#define AREC_FRMLIST_NUM 		20
#define AUDIO_DEC_BUF			(AUDIO_REC_BUF + AUDIO_REC_BUF_SIZE)
#define AUDIO_DEC_BUF_SIZE		(512*1024)
#define ADEC_FRMLIST_NUM 		20
/* 3D Denoise */
#define	TDDNS_MEM1_BASE_ADDR  	(MEMBASE_DDR + 0x1000000)	//TDDNS need an independent ddr bank for better performance
#define	TDDNS_BUF_SIZE			(VS0_WIDTH * VS0_HEIGHT * 2)	

/* for storing UV data for VA */
#define SCALE_WIDTH             (320)
#define SCALE_HEIGHT            (240)
#define	FB3_HW_FRM_SIZE			(SCALE_WIDTH * SCALE_HEIGHT / 8)//actual_size/8
#define	FB3_HW_FRM_UV_SIZE		(SCALE_WIDTH * SCALE_HEIGHT / 8)//actual_size/8
#define FB3_YBASE_ADDR			(0x2000000/8)
#define FB3_UVBASE_ADDR			(0x2800000/8) 

#define  IMG_BUF_START     		(MEMBASE_DDR + 0x1800000)         //0x11900000, IMG set to a indepedentent bank
#define  IMG_BUF_SIZE      		(1024*1024*8)           						//8M
#define  IMG_ENC_SIZE      		(300*1024)           							//300K

//#if defined(CONFIG_SNAPSHOT_IN_LIVEVIEW) || defined(CONFIG_DUAL_VS_WITHIMG_EN)
#define IMG_EN
//#endif

//#define MD_EN
#ifdef MD_EN
static t_dpm_fb_buf dp_fb3_buf0;

static t_dpm_fb_buf dp_fb3_buf1 =
{
	.ymem_base = (FB3_YBASE_ADDR+FB3_HW_FRM_SIZE),
	.uvmem_base = (FB3_UVBASE_ADDR+FB3_HW_FRM_UV_SIZE),
	.next = &dp_fb3_buf0,
};

static t_dpm_fb_buf dp_fb3_buf0 =
{
	.ymem_base = FB3_YBASE_ADDR,
	.uvmem_base = FB3_UVBASE_ADDR, 
	.next = &dp_fb3_buf1,
};

static t_dpm_fb dp_fb3 = 
{
	DPM_INIT_FB
	.id = 3,
	.enabled = 1,
	.int_mask = 0,
	.data_format = FB_FORMAT_YUV422IN_NOSTORE,
	.output_fmt = DPM_DATAFMT_YUV422,
	.pingpong_en = 0,
	.yaddr = FB3_YBASE_ADDR,
	.uvaddr = FB3_UVBASE_ADDR,
	.offset = 0,
	.bufhead = &dp_fb3_buf0,
	.flag = DPM_FLAG_END,
};

static t_dpm_yuvscale dp_yuvscale0_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 0,
	.out = &dp_fb3,
	.output_width = SCALE_WIDTH,
	.output_height = SCALE_HEIGHT,
	.yuv422_out = 1,
};

static t_dpm_md dp_md =
{
	DPM_INIT_MD
	.win_cnt = 0xff,
	.mask_addr = (MD_MASK_BASE_ADDR - MEMBASE_DDR)/8,
	.intr_mask = 0xf,

	///< QVGA
	.md_win[0] = {
		.xstart = 0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[1] = {
		.xstart = 0x50,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[2] = {
		.xstart = 0xa0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[3] = {
		.xstart = 0xf0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[4] = {
		.xstart = 0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[5] = {
		.xstart = 0x50,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[6] = {
		.xstart = 0xa0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[7] = {
		.xstart = 0xf0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
};

static t_dpm_yuvscale dp_yuvscale2 =
{
	DPM_INIT_YUVSCALE
	.id = 2,
	.out = &dp_md,
	.output_width = SCALE_WIDTH,
	.output_height = SCALE_HEIGHT,
	.yuv422_out = 1,
	.next = &dp_yuvscale0_mode0,
};
#endif


#define MANUAL_DROP_FRM		//sensor input frame rate fixed at 24fps, use firmware to control encode frame rate
static int vs0_framerate = 0;
static int vs1_framerate = 0;
static t_vs_fps_cfg vs0_fps;
static t_vs_fps_cfg vs1_fps;
static u32 vs_handle_cmd(struct s_dpm_ve * dpm, u32 cmd, u32 arg, u32 * handled)
{
	switch(cmd){
#ifdef MANUAL_DROP_FRM
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRMRATE_SET):
		*handled = 1;
		if(dpm->id == 0){
			vs0_framerate = arg;
		}else{
			vs1_framerate = arg;
		}
		return vs_set_fps_drop(dpm, arg);
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_SET):
		*handled = 1;
		if(dpm_sensor_effect(dpm, SNR_EFFECT_FRMRATE, arg) != 0){
			return 1;	
		}	
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			return vs_set_fps_drop(dpm, dpm_ve->frame_rate);
		}else if(dpm->type == DPM_TYPE_IMG){
			t_dpm_img * dpm_img = (t_dpm_img *)dpm;
			return vs_set_fps_drop(dpm, dpm_img->frame_rate);
		}else{
			return 0;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET):
		{
		t_dpm_sensor * dpm_sensor = dpm;
		while(dpm_sensor){
			if(dpm_sensor->type == DPM_TYPE_SENSOR){
				dpm_sensor->sensor_cfg->cur_frame_rate = arg;
				syslog(LOG_INFO, "change vts fps to %d\n", arg);	
				break;
			}
			dpm_sensor = dpm_sensor->in;
		}
		*handled = 1;
		if(dpm->id == 0 && vs0_framerate){
			//customer set the vs_framerate already, use the small one
			if(vs0_framerate < arg){
				arg = vs0_framerate;
			}
		}else if(dpm->id == 1 && vs1_framerate){
			//customer set the vs_framerate already, use the small one
			if(vs1_framerate < arg){
				arg = vs1_framerate;
			}
		}
		return vs_set_fps_drop(dpm, arg);
		}
#endif
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		*handled = 1;
		return dpzoom_ecif_crop_coordinate(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
		*handled = 1;
		return dpzoom_noscale_ecif_crop_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
		*handled = 1;
		return dpzoom_scale_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	}

	return 0;
}

#ifdef IMG_EN
FRAMELIST_DECLARE(vs1_framelist, 24);
static t_dpm_img dp_img_vs1 =
{
	DPM_INIT_IMG
	.id = 1,
	.buflist_start = IMG_BUF_START,
	.buflist_size = IMG_BUF_SIZE, // img gen buffer.
	.imgenc_size = IMG_ENC_SIZE, // img encode size, one image frame should be less than imgenc_size
	.imgenc_tgt_size = 0,
	.frame_rate = 24,
	.qs = 0xa,
	.framelist = &vs1_framelist,
	.flag = DPM_FLAG_END,
	.handle_cmd = vs_handle_cmd,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs1_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
};
#endif

static t_ve_rc_cfg dp_ve_vs0_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 1000 * 1024,
	.init_qp = 29,
	.i_p_qp_delta = 2, 
	.max_gop_size = FRAME_RATE,
	.min_qp[0] = 1,
	.min_qp[1] = 1,
};

FRAMELIST_DECLARE(vs0_framelist, 15);
static t_dpm_ve dp_ve_vs0 =
{
	DPM_INIT_VE
	.id = 0,
	.ve_ref_buf_base = VE_STREAM0_HWBUF_BASE,
	.bs_buf_size = VS0_BUF_SIZE,
	.rc_cfg = &dp_ve_vs0_rc,
	.frame_rate = FRAME_RATE,
	.framelist = &vs0_framelist,
#ifdef IMG_EN
	.next = &dp_img_vs1,
#else
	.flag = DPM_FLAG_END,
#endif
	.add_sps_head_en = ADD_SPS_MODE1 ,
	.idr_period = 1,
	.bs_buf_skip_th = 100 * 1024,
	.autoskip_count = 0,//number of frames to be skipped at the beginning of the stream
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs0_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
	.handle_cmd = vs_handle_cmd,
};

static t_dpm_vif dp_vif1_mode0 =
{
	DPM_INIT_VIF
	.id = 1,
	.out = &dp_ve_vs0,
	.no_kick = 0,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
	.max_in_width = MAX_WIDTH,
	.mbl_fifo_depth = VIF_FIFO_NUM,
#ifndef OSD4L_EN
	.yuv420_in = 1,
#endif
};

static t_dpm_osd4l dp_osd4l_mode0 =
{
	DPM_INIT_OSD4L
	.id = 0,
	.out = &dp_vif1_mode0,
#ifdef OSD4L_EN
	.osd_win[0] = {
		.win_no = 1,
		.win_hsize = 800,
		.win_vsize = 64,
		.win_hstart = 0,
		.win_vstart = 0,
		.win_memaddr = OSD4L_MODE0_BASE_ADDR,
		.color[0] = (0xff7f7f|(2<<24)),
		.color[1] = (0x1dfe6a|(3<<24)),
		.color[2] = (0x4c54ff|(3<<24)),
		.color[3] = (0xe10094|(3<<24)),
	},
#else
	.bypass = 1,
#endif
};

static t_dpm_yuvscale dp_yuvscale1_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_osd4l_mode0,
#ifdef MD_EN
	.next = &dp_yuvscale2,  ///< MD
#endif
#ifdef OSD4L_EN
	.yuv422_out = 1,
#endif
};

static t_dpm_ispout dp_ispout0 =
{
	DPM_INIT_ISPOUT
	.id = 0,
	.out = &dp_yuvscale1_mode0,
};

static t_dpm_rgbir dp_rgbir; 
static t_dpm_ispidc dp_ispidc =
{
	DPM_INIT_ISPIDC
	.id = 0,
	.out = &dp_ispout0,
	.in = &dp_rgbir,
	.dummy_mode = IDC_DUMMY_AUTO_MODE,
	.dummy_line = 0xc,
	.TDdns_en = 1,
	.TDdns_mem1 = (TDDNS_MEM1_BASE_ADDR - MEMBASE_DDR)/8, 
	.v2d1_hblk = 0x100,
	.cpp = 0,
	.lum_sensor_pixelsize = 375,
	.lum_sensor_hts = 0x918,
	.lum_sensor_clk = 48,
	.isp_brightness = 128,
};

static t_dpm_rgbir dp_rgbir = 
{
	DPM_INIT_RGBIR
	.out = &dp_ispidc,
	.isp_sat_thr1 = 0x150,
	.isp_sat_thr2 = 0x1e0,
	.isp_coeff_thr1 = 0x150,
	.isp_coeff_thr2 = 0x1e0,
	.isp_coeff_value = 0x6880,
};

static t_dpm_mipicif dp_mipicif0 = 
{
	DPM_INIT_MIPICIF
	.id = 0,
	.out = &dp_rgbir,
};

static t_dpm_mipirx dp_mipirx0 = 
{
	DPM_INIT_MIPIRX
	.id = 0,
	.out = &dp_mipicif0,
#ifdef CONFIG_FAST_BOOT_EN
	.lane_num = 2,
#else
	.lane_num = 1,
#endif
	.four_sel = 1,
	.vc0_emb_id = 0x12,
	.vc1_emb_id = 0x12,
	.vc0_emb_use_dt = 0x1e,
	.vc1_emb_use_dt = 0x1e,
	.pnum_int = 0x4,
	.int_en = 0xfffffff,
	.c2d4_en = 0x1,
#ifdef CONFIG_FAST_BOOT_EN
	.invert_pclk =1,
#endif
};

static t_libsccb_cfg dp_sccbcfg =
{
	.mode = SCCB_MODE16,
	.buf_addr = SCCB_GRP_BUF,
	.max_len = SCCB_GRP_BUF_SIZE,
	.clk = 400000,
	.rd_mode = RD_MODE_NORMAL,
	.sccb_num = SCCB_NUM_0,
};

static t_dpm_sensor dp_sensor =
{
	DPM_INIT_SENSOR
	.sccb_cfg = &dp_sccbcfg,
	.out = &dp_mipirx0,
	.name = "9750",
};

static _fastboot_vs_handle_cmd(int vs_id, u32 cmd, u32 arg)
{
	if(vs_id != 0){
		return -1;
	}

	switch(cmd){
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		dp_ispidc.crop_x = ((arg>>16)&0xffff)&(~0x1);
		dp_ispidc.crop_y = (arg&0xffff)&(~0x1);
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);	
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			return 1;
		}
		dp_ispidc.output_width = width;
		dp_ispidc.output_height = height;
		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
		dp_ispout0.output_width = width;
		dp_ispout0.output_height = height;
		t_dpm *out = g_libdatapath_cfg->fastboot_isp_out;
		while(out!= NULL)
		{
			out->output_width = width;
			out->output_height = height;
			out = out->out;
		}
		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			syslog(LOG_ERR, "ERROR:size should be more than 64*64\n");
			return 1;
		}
		{
			u32 inwidth = dp_ispout0.output_width;
			u32 inheight = dp_ispout0.output_height;
			if(inwidth/width > 16 || inheight/height > 16){
				syslog(LOG_ERR, "ERROR:scaler ratio is more than 16!\n");
				return 3;
			}
		}

		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
		dp_vif1_mode0.output_width = width;
		dp_vif1_mode0.output_height = height;
		dp_osd4l_mode0.output_width = width;
		dp_osd4l_mode0.output_height = height;
		dp_yuvscale1_mode0.output_width = width;
		dp_yuvscale1_mode0.output_height = height;
		return 0;
	}	
	case(MPUCMD_TYPE_VS | VSIOC_INIT_GOP_SET):
	{
		dp_ve_vs0_rc.init_gop_size = (arg&0xffff);
		dp_ve_vs0_rc.init_gop_num = ((arg>>16)&0xffff);
		return 0;
	}
	case(MPUCMD_TYPE_VS | VSIOC_GOP_SET):
	{
		dp_ve_vs0_rc.max_gop_size = arg;
		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_EXPOSURE_SET):
	{
		dp_ispidc.isp_brightness = arg;
		return 0;
	}
	}
	return -1;
}

t_libdatapath_cfg libdatapath_cfg[] =
{
	{
#ifdef CONFIG_FAST_BOOT_EN
//		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE | DATAPATH_FASTBOOT_DEBUG,
//		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE | DATAPATH_FASTBOOT_VDEBUG,
		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE ,
		.fastboot_vs_handle_cmd = _fastboot_vs_handle_cmd,
#endif
		.head = &dp_sensor,
	},
};

#ifdef CONFIG_MPU_AENC_EN

AREC_ALLOCATE_STEREO
FRAMELIST_DECLARE(areclist, AREC_FRMLIST_NUM )
u8 __attribute__ ((aligned(4))) arec_framelist_buf[OUTBUF_ONELEN*AREC_FRMLIST_NUM];	

t_liba_ecfg g_liba_ecfg =
{
	AREC_CONFIG_STEREO
	.outbuf_addr = AUDIO_REC_BUF,	
	.outbuf_size = AUDIO_REC_BUF_SIZE, 
	.list = &areclist,
};
#endif

#ifdef CONFIG_MPU_ADEC_EN

ADEC_ALLOCATE
FRAMELIST_DECLARE(adeclist, ADEC_FRMLIST_NUM)
u8 __attribute__ ((aligned(4))) adec_framelist_buf[INBUF_ONELEN*ADEC_FRMLIST_NUM];

t_liba_dcfg g_liba_dcfg =
{
	ADEC_CONFIG
	.inbuf_addr = AUDIO_DEC_BUF,
	.inbuf_size = AUDIO_DEC_BUF_SIZE,
	.list = &adeclist,
};
#endif
