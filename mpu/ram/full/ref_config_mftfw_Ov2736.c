#include "includes.h"

/* 
   下面这个数据结构描述了这样一个datapath(print by libdatapath_print()):

   ->: single path
   +>: concurrent path
   *>: sequential path

   ->SNR2736 ->MIPIRX0->MIPICIF0->RGBIR->IDC0->ISP0->YUVSCL1->OSD4L0->VIF1 +>VE0
             +>IMG
                                                        +>YUVSCL2->MD

    memory config:          addr_start  addr_end(not included)   size
	SRAM_START				0x10000000	     
    CODE  	  				0x10000000 --- 0x10050000
	VIFBUF_BASE 			0x10050000 --- 0x10072000            135k
	MPU_CODE 				0x10072000 --- 

	DDR_BANK0_START_ADDR	0x10100000
	DDR_CODES               0x10100000 --- 0x10200000            1MB
	HEAP                    0x10200000 --- 0x10300000            1MB
	TDDNS_MEM1_BASE_ADDR    0x10300000 --- 0x10300000            3.955M
	AUDIO_REC_BUF		  	0x106F4800 --- 0x10774800 	         512k	
	AUDIO_DEC_BUF		    0x10774800 --- 0x107F4800            512k	
	FREE 	                0x107F4800 --- 0x107FFFFF            45.9K
	WIFI_BIN                0x10800000 --- 0x10900000            1MB

	DDR_BANK1_START_ADDR	0x10900000	
	VIDEO_GENERAL_BUF	    0x10900000 --- 0x10C00000            3M
	MD_MASK_BASE_ADDR		0x10C00000 --- 0x10C94600   		 593.5K	
	SENSOR_SCCB_GROUP      	0x10C94600 --- 0x10C95600   		 4K (used for sensor initial)
    IMG buffer              0x10C95600 --- 0x11095600            4MB 

	DDR_BANK2_START_ADDR	0x11100000
	VS0_HWBUF            	0x11100000 --- 0x11435400             3285K
    //VS0_HWBUF(PING)   	0x11100000 --- 0x103FD000             3060K
	
	DDR_BANK3_START_ADDR	0x11900000
	//VS0_HWBUF(PONG)		0x11900000 --- 0x11BFD000             3060K	
 */

/* default setting */
#define VS0_WIDTH               (1920)
#define VS0_HEIGHT              (1088)
#define MAX_WIDTH               (VS0_WIDTH)
#define MAX_HEIGHT              (VS0_HEIGHT)
#define VS0_OUT_CROP_BOTTOM     (8)

#define SENSOR_NAME             "2736"
#define MIPI_LANE_NUM           (2)
#define FRAME_RATE              (24)
#define VIF_FIFO_NUM            (3)
#define PINGPONG_VE_REF

#define OSD4L_WIN_W             VS0_WIDTH
#define OSD4L_WIN_H             16

#define FRAME_RATE              24

///< SRAM memory map
#define VIF_BUF_ADDR            (CONFIG_VBUF_BASE - MEMBASE_SRAM) //offset
#define VIF_BUF_LENGTH          (((MAX_WIDTH * 16 * 3) / 2) * VIF_FIFO_NUM) //fifo_num * MB_line, YUV4:2:0
#define OSD4L_MODE0_BASE_ADDR   (VIF_BUF_ADDR + VIF_BUF_LENGTH)
#define OSD4L_MODE0_SIZE        (OSD4L_WIN_W*OSD4L_WIN_H*2/4)   //(win_w * win_h * 2 / 4)

///< DDR memory map
#define DDR_BANK_SIZE           (16*1024*1024)
#define DDR_BANK0_START_ADDR    (MEMBASE_DDR)
#define DDR_BANK1_START_ADDR    (MEMBASE_DDR + DDR_BANK_SIZE)
#define DDR_BANK2_START_ADDR    (MEMBASE_DDR + DDR_BANK_SIZE*2)
#define DDR_BANK3_START_ADDR    (MEMBASE_DDR + DDR_BANK_SIZE*3)

//bank1
/*3D Denoise*/
#define TDDNS_MEM1_BASE_ADDR    DDR_BANK1_START_ADDR
#define TDDNS_BUF_SIZE          (VS0_WIDTH*VS0_HEIGHT*2) //YUV 4:2:2

/*Audio*/
#define AUDIO_REC_BUF           (TDDNS_MEM1_BASE_ADDR + TDDNS_BUF_SIZE)
#define AUDIO_REC_BUF_SIZE      (512*1024)
#define AREC_FRMLIST_NUM        (128)
#define AUDIO_DEC_BUF           (AUDIO_REC_BUF + AUDIO_REC_BUF_SIZE)
#define AUDIO_DEC_BUF_SIZE      (512*1024)
#define ADEC_FRMLIST_NUM        (20)

/*SCCB*/
#define SCCB_GRP_BUF            (AUDIO_DEC_BUF + AUDIO_DEC_BUF_SIZE)
#define SCCB_GRP_BUF_SIZE       (4*1024)

/*FB2*/
#define FB2_BUF_SIZE			(4*1024*1024)
#define FB2_YBASE_ADDR          (SCCB_GRP_BUF+SCCB_GRP_BUF_SIZE)
#define FB2_UVBASE_ADDR         (FB2_YBASE_ADDR+FB2_BUF_SIZE)
#define FB2_BUF_END_ADDR		(FB2_UVBASE_ADDR+FB2_BUF_SIZE)	// Boundary

/* BANK2*/
//vs
#define VE_STREAM0_BSBUF_BASE   (DDR_BANK2_START_ADDR)
#define VS0_BUF_SIZE            (4*1024*1024)
/*IMG*/
#define IMG_BUF_START           (VE_STREAM0_BSBUF_BASE + VS0_BUF_SIZE)
#define IMG_BUF_SIZE            (1024*1024*4) //4M
#define IMG_ENC_SIZE            (500*1024)

#define VE_STREAM0_HWBUF_BASE  (IMG_BUF_START+IMG_BUF_SIZE)
#ifdef PINGPONG_VE_REF
    #define VS0_HWBUF_SIZE     (MAX_WIDTH * MAX_HEIGHT * 3 / 2) //one_frame, YUV4:2:0
#else
    #define VS0_HWBUF_SIZE     (((MAX_WIDTH * 16 * 3) / 2) * ((MAX_HEIGHT/16) + 5)) //(one_frame + (5*MB_line)), YUV4:2:0
#endif

/*bank3*/
#define VE_STREAM0_HWBUF1_BASE (DDR_BANK3_START_ADDR) //size = VS0_HWBUF_SIZE, one_frame YUV4:2:0
#ifdef PINGPONG_VE_REF
    #define VS1_HWBUF_SIZE      (MAX_WIDTH * MAX_HEIGHT * 3 / 2) //one_frame, YUV4:2:0
#else
    #define VS1_HWBUF_SIZE      (0)
#endif

/* MD */
#define MD_WIDTH                (320)
#define MD_HEIGHT               (180)
#define MD_MASK_BASE_ADDR       (VE_STREAM0_HWBUF1_BASE + VS1_HWBUF_SIZE)
#define MD_MASK_BUF_SIZE        (0x40000) //default QVGA
#define MD_COPY_BASE_ADDR       (MD_MASK_BASE_ADDR + MD_MASK_BUF_SIZE)
#define MD_COPY_BASE_ADDR1      (MD_COPY_BASE_ADDR + MD_WIDTH*MD_HEIGHT*2)
#define MD_COPY_BASE_ADDR2      (MD_COPY_BASE_ADDR1+ MD_WIDTH*MD_HEIGHT*2)
#define MD_BUF_SIZE             (MD_MASK_BUF_SIZE + MD_WIDTH*MD_HEIGHT*6)


#define IP_I4_DISABLE           (1)
#define VE_SKIP_FRM_CNT         (0)
#define VIF_SKIP_FRM_CNT        (0)

#define TAG_MAXLEN              (32)  //32Byte aligned, "OVMS", "OVMZ", "OVME", "FF FF ... FF"
#define META_MAXLEN             (256) //32Byte aligned
static unsigned char meta_buf_data[TAG_MAXLEN + META_MAXLEN];

#define MANUAL_DROP_FRM		//sensor input frame rate fixed at 24fps, use firmware to control encode frame rate

#define RAW_EN	// Enable FB2 for RAW data output

#ifdef RAW_EN
static t_dpm_fb_buf dp_fb2_buf0 =
{
	.ymem_base = FB2_YBASE_ADDR,
	.uvmem_base = FB2_UVBASE_ADDR,
	.next = &dp_fb2_buf0,
};

static t_dpm_fb dp_fb2 =
{
	DPM_INIT_FB
	.id = 2,
	.enabled = 1,
	.int_mask = 0,
	.data_format = FB_FORMAT_RAW8IN_NOSTORE,
	.output_fmt = DPM_DATAFMT_RAW8,
	.pingpong_en = 0,
	.yaddr = FB2_YBASE_ADDR,
	.uvaddr = FB2_UVBASE_ADDR,
	.offset = 0,
	.bufhead = &dp_fb2_buf0,
	.flag = DPM_FLAG_END,
};
#endif

//#if defined(CONFIG_SNAPSHOT_IN_LIVEVIEW) || defined(CONFIG_DUAL_VS_WITHIMG_EN)
#define IMG_EN
//#endif

#ifdef IMG_EN
static u32 ngdual_isp_crop_coordinate(t_dpm *dpm, u16 x, u16 y);
static u32 ngdual_noscale_isp_crop_size(t_dpm *dpm, u16 crop_width, u16 crop_height);
static u32 ngdual_scale_size(t_dpm *dpm, u16 scale_width, u16 scale_height);
#endif

#if defined(CONFIG_MOTION_ANALYTICS)|| defined(CONFIG_VIDEO_ANALYSIS_VA)
#define MD_EN
#endif

#ifdef MD_EN
static t_dpm_md dp_md =
{
	DPM_INIT_MD
	.win_cnt = 0,
	.mask_addr = (MD_MASK_BASE_ADDR - MEMBASE_DDR)/8,
	.algo_en = 0,
	.copy_addr = MD_COPY_BASE_ADDR,
	.prev_addr0 = MD_COPY_BASE_ADDR1,
	.prev_addr1 = MD_COPY_BASE_ADDR2,
	.intr_mask = 0xf,
	.post_pro_iter_num = 3,
	
	///< QVGA
	.md_win[0] = {
		.xstart = 0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
		.threshold = 5,
	},
	.md_win[1] = {
		.xstart = 0x50,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
		.threshold = 10,
	},
	.md_win[2] = {
		.xstart = 0xa0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
		.threshold = 15,
	},
	.md_win[3] = {
		.xstart = 0xf0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
		.threshold = 20,
	},
	.md_win[4] = {
		.xstart = 0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
		.threshold = 25,
	},
	.md_win[5] = {
		.xstart = 0x50,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
		.threshold = 30,
	},
	.md_win[6] = {
		.xstart = 0xa0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
		.threshold = 35,
	},
	.md_win[7] = {
		.xstart = 0xf0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
		.threshold = 40,
	},
};

static t_dpm_yuvscale dp_yuvscale2 =
{
	DPM_INIT_YUVSCALE
	.id = 2,
	.out = &dp_md,
	.output_width = MD_WIDTH,
	.output_height = MD_HEIGHT,
	.yuv422_out = 1,
};
#endif

#ifdef MANUAL_DROP_FRM
static int vs0_framerate = 0;
static int vs1_framerate = 0;
static t_vs_fps_cfg vs0_fps;
static t_vs_fps_cfg vs1_fps;
static u32 vs_handle_cmd(struct s_dpm * dpm, u32 cmd, u32 arg, u32 * handled)
{
	switch(cmd){
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRMRATE_SET):
		*handled = 1;
		if(dpm->id == 0){
			vs0_framerate = arg;
		}else{
			vs1_framerate = arg;
		}
		return vs_set_fps_drop(dpm, arg);
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_SET):
		*handled = 1;
		if(dpm_sensor_effect(dpm, SNR_EFFECT_FRMRATE, arg) != 0){
			return 1;	
		}	
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			return vs_set_fps_drop(dpm, dpm_ve->frame_rate);
		}else if(dpm->type == DPM_TYPE_IMG){
			t_dpm_img * dpm_img = (t_dpm_img *)dpm;
			return vs_set_fps_drop(dpm, dpm_img->frame_rate);
		}else{
			return 0;	
		}
	case (MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET):
		{
		t_dpm_sensor * dpm_sensor = dpm;
		while(dpm_sensor){
			if(dpm_sensor->type == DPM_TYPE_SENSOR){
				dpm_sensor->sensor_cfg->cur_frame_rate = arg;
				syslog(LOG_INFO, "change vts fps to %d\n", arg);	
				break;
			}
			dpm_sensor = dpm_sensor->in;
		}
		*handled = 1;
		if(dpm->id == 0 && vs0_framerate){
			//customer set the vs_framerate already, use the small one
			if(vs0_framerate < arg){
				arg = vs0_framerate;
			}
		}else if(dpm->id == 1 && vs1_framerate){
			//customer set the vs_framerate already, use the small one
			if(vs1_framerate < arg){
				arg = vs1_framerate;
			}
		}
		return vs_set_fps_drop(dpm, arg);
		}
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		*handled = 1;
#ifdef IMG_EN 
		return ngdual_isp_crop_coordinate(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#else
		return dpzoom_isp_crop_coordinate(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#endif
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
		*handled = 1;
#ifdef IMG_EN 
		return ngdual_noscale_isp_crop_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#else
		return dpzoom_noscale_isp_crop_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#endif
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
		*handled = 1;
#ifdef IMG_EN 
		return ngdual_scale_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#else
		return dpzoom_scale_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
#endif
	}
	return 0;
}
#endif

#ifdef IMG_EN
FRAMELIST_DECLARE(vs1_framelist, FRAME_RATE);
static t_dpm_img dp_img_vs1 =
{
	DPM_INIT_IMG
	.id = 1,
	.buflist_start = IMG_BUF_START,
	.buflist_size = IMG_BUF_SIZE, // img gen buffer. 
	.imgenc_size = IMG_ENC_SIZE, // img encode size, one image frame should be less than imgenc_size 
	.imgenc_tgt_size = 0,
	.frame_rate = FRAME_RATE,
	.qs = 0xa,
	.width = VS0_WIDTH,
	.height = VS0_HEIGHT-VS0_OUT_CROP_BOTTOM,
	.framelist = &vs1_framelist,
	.flag = DPM_FLAG_END,
	.handle_cmd = vs_handle_cmd,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs1_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
};
#endif

static t_ve_rc_cfg dp_ve_vs0_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 1000 * 1024,
	.init_qp = 29,
	.i_p_qp_delta = 2, 
	.max_gop_size = FRAME_RATE,
	.min_qp[0] = 1,
	.min_qp[1] = 1,
};

FRAMELIST_DECLARE(vs0_framelist, (FRAME_RATE*8));
static t_dpm_ve dp_ve_vs0 =
{
	DPM_INIT_VE
	.id = 0,
	.ve_ref_buf_base = VE_STREAM0_HWBUF_BASE,
#ifdef PINGPONG_VE_REF
	.ve_ref_buf_base1 = VE_STREAM0_HWBUF1_BASE,
#else
	.ve_ref_buf_base1 = 0,
#endif
	.bs_buf_base = VE_STREAM0_BSBUF_BASE,
	.bs_buf_size = VS0_BUF_SIZE,
	.rc_cfg = &dp_ve_vs0_rc,
	.frame_rate = FRAME_RATE,
	.framelist = &vs0_framelist,
#ifdef IMG_EN 
	.next = &dp_img_vs1,
#else
	.flag = DPM_FLAG_END,
#endif
	.add_sps_head_en = ADD_SPS_MODE0,
	.idr_period = 1,
	.level_idc = 40,
	.entropy = 1,
	.bs_buf_skip_th = 200 * 1024,
	.autoskip_count = VE_SKIP_FRM_CNT,
	.ip_i4_disable = IP_I4_DISABLE,
	.output_width = VS0_WIDTH,
	.output_height = VS0_HEIGHT - VS0_OUT_CROP_BOTTOM,
	.handle_cmd = vs_handle_cmd,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs0_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
	.handle_cmd = vs_handle_cmd,
#endif
 	.meta_buf = &meta_buf_data,
	.meta_maxlen = META_MAXLEN,
	.tag_maxlen = TAG_MAXLEN,
};

static t_dpm_vif dp_vif1_mode0 =
{
	DPM_INIT_VIF
	.id = 1,
	.out = &dp_ve_vs0,
	.no_kick = 0,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
	.frm_skip_cnt = VIF_SKIP_FRM_CNT,
	.mbl_fifo_depth = VIF_FIFO_NUM,
	.max_in_width = MAX_WIDTH,
};

static t_dpm_osd4l dp_osd4l_mode0 =
{
	DPM_INIT_OSD4L
	.id = 0,
	.out = &dp_vif1_mode0,
#ifdef OSD4L_EN
    .osd_win[0] = {
        .win_no = 1,
        .win_hsize = OSD4L_WIN_W, ///< must be the mutiply of 4
        .win_vsize = OSD4L_WIN_H,
        .win_hstart = 0,
        .win_vstart = 0,
        .win_memaddr = (OSD4L_MODE0_BASE_ADDR|BIT31),
        .color[0] = (0xff7f7f|(2<<24)),
        .color[1] = (0x1dfe6a|(3<<24)),
        .color[2] = (0x4c54ff|(3<<24)),
        .color[3] = (0xe10094|(3<<24)),
    },
#else
	.bypass = 1,
#endif
};

static t_dpm_yuvscale dp_yuvscale1_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_osd4l_mode0,
	.output_width = VS0_WIDTH,
	.output_height = VS0_HEIGHT,
#ifdef MD_EN
	.next = &dp_yuvscale2,  ///< MD
#endif
#ifdef OSD4L_EN
	.yuv422_out = 1,
#endif
};

static t_dpm_ispout dp_ispout0 =
{
	DPM_INIT_ISPOUT
	.id = 0,
	.out = &dp_yuvscale1_mode0,
};

static t_dpm_rgbir dp_rgbir; 
static t_dpm_ispidc dp_ispidc =
{
	DPM_INIT_ISPIDC
	.id = 0,
	.out = &dp_ispout0,
	.in = &dp_rgbir,
	.dummy_line = 0xc,
	.dummy_mode = IDC_DUMMY_AUTO_MODE,
	.TDdns_en = 1,
	.TDdns_mem1 = ((TDDNS_MEM1_BASE_ADDR - MEMBASE_DDR) / 8), //offset, 8byte align
	.cpp = 0x0,
	.v2d1_hblk = 0x100,
#ifdef RAW_EN
	.next = (t_dpm *)&dp_fb2,
#endif
};


static t_dpm_rgbir dp_rgbir = 
{
	DPM_INIT_RGBIR
	//.dummy_manual_en = 1,
	//.dummy_hblk = 0x100,//0x100,
	.out = (t_dpm *)&dp_ispidc,
};

static t_dpm_mipicif dp_mipicif0 = 
{
	DPM_INIT_MIPICIF
	.id = 0,
	.out = &dp_rgbir,
};

static t_dpm_mipirx dp_mipirx0 = 
{
	DPM_INIT_MIPIRX
	.id = 0,
	.out = &dp_mipicif0,
	.lane_num = MIPI_LANE_NUM,
	.four_sel = 1,
	.vc0_emb_id = 0x12,
	.vc1_emb_id = 0x12,
	.vc0_emb_use_dt = 0x1e,
	.vc1_emb_use_dt = 0x1e,
	.pnum_int = 0x4,
	.int_en = 0xfffffff,
	.c2d4_en = 0x1,
	.invert_pclk=0x1,
};

static t_libsccb_cfg dp_sccbcfg =
{
	.mode = SCCB_MODE16,
	.buf_addr = SCCB_GRP_BUF,
	.max_len = SCCB_GRP_BUF_SIZE,
	.clk = 200000,
	.rd_mode = RD_MODE_NORMAL,
	.sccb_num = SCCB_NUM_0,
};

static t_dpm_sensor dp_sensor =
{
	DPM_INIT_SENSOR
	.sccb_cfg = &dp_sccbcfg,
	.out = &dp_mipirx0,
	.name = SENSOR_NAME,
};

static _fastboot_vs_handle_cmd(int vs_id, u32 cmd, u32 arg)
{
	switch(cmd){
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		dp_ispidc.crop_x = ((arg>>16)&0xffff)&(~0x1);
		dp_ispidc.crop_y = (arg&0xffff)&(~0x1);
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);	
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			return 1;
		}
		dp_ispidc.output_width = width;
		dp_ispidc.output_height = height;
		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
		dp_ispout0.output_width = width;
		dp_ispout0.output_height = height;
		t_dpm *out = g_libdatapath_cfg->fastboot_isp_out;
		while(out!= NULL)
		{
			out->output_width = width;
			out->output_height = height;
			out = out->out;
			if(out->next){
				t_dpm_img * dpm_img = out->next;
				dpm_img->width = width;
				dpm_img->height = height;
				dpm_img->output_width = width;	
				dpm_img->output_height = height;	
			}
		}

		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			syslog(LOG_ERR, "ERROR:size should be more than 64*64\n");
			return 1;
		}
		{
			u32 inwidth = dp_ispout0.output_width;
			u32 inheight = dp_ispout0.output_height;
			if(inwidth/width > 16 || inheight/height > 16){
				syslog(LOG_ERR, "ERROR:scaler ratio is more than 16!\n");
				return 3;
			}
		}

		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
#ifdef IMG_EN
		dp_img_vs1.width = width;
		dp_img_vs1.height = height;
#endif
		dp_vif1_mode0.output_width = width;
		dp_vif1_mode0.output_height = height;
		dp_osd4l_mode0.output_width = width;
		dp_osd4l_mode0.output_height = height;
		dp_yuvscale1_mode0.output_width = width;
		dp_yuvscale1_mode0.output_height = height;
		return 0;
	}
	case(MPUCMD_TYPE_VS | VSIOC_INIT_GOP_SET):
	{
		dp_ve_vs0_rc.init_gop_size = (arg&0xffff);
		dp_ve_vs0_rc.init_gop_num = ((arg>>16)&0xffff);
		return 0;
	}
	case(MPUCMD_TYPE_VS | VSIOC_GOP_SET):
	{
		dp_ve_vs0_rc.max_gop_size = arg;
		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_GOP_DURATION_SET):
	{
		dp_ve_vs0_rc.max_gop_size = dp_ve_vs0.frame_rate * arg / 30;
		return 0;
 	}
	}
	return -1;
}

#ifdef IMG_EN 
static u32 ngdual_isp_crop_coordinate(t_dpm *dpm, u16 x, u16 y){
	t_dpm * in = dpm;
	while(in){
		if(in->type == DPM_TYPE_ISPIDC){
			t_dpm_ispidc * dpm_ispidc = (t_dpm_ispidc *)in;	
			if(dpm_ispidc->crop_x == (x&(~0x1))&& dpm_ispidc->crop_y == (y&(~0x1))){
							
				return 0;
			}
		}
		in = in->in;
	}

	if(libvs_disable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF, 0x10000000) != 0){
		return -1;
	}

	dpzoom_isp_crop_coordinate(dpm, x, y);
	
	return libvs_enable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF);
}

static u32 ngdual_noscale_isp_crop_size(t_dpm *dpm, u16 crop_width, u16 crop_height){
	crop_width &= (~0xf);	
	crop_height &= (~0xf);
	t_dpm * in = dpm;
	while(in){
		if(in->type == DPM_TYPE_ISPIDC){
			t_dpm_ispidc * dpm_ispidc = (t_dpm_ispidc *)in;	
			if(dpm_ispidc->output_width == crop_width && dpm_ispidc->output_height == crop_height){
				return 0;
			}
		}
		in = in->in;
	}

	if(libvs_disable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF, 0x10000000) != 0){
		return -1;
	}

	if(dpzoom_noscale_isp_crop_size(dpm, crop_width, crop_height) == 0){
		t_dpm_img * dpm_img = NULL;
		if(dpm->next){
			dpm_img = dpm->next;
			dpm_img->width = crop_width;
			dpm_img->height = crop_height;
			dpm_img->output_width = crop_width;	
			dpm_img->output_height = crop_height;	
		}
	}

	return libvs_enable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF);
}

static u32 ngdual_scale_size(t_dpm *dpm, u16 scale_width, u16 scale_height){
	scale_width &= (~0xf);
	scale_height &= (~0xf);
	t_dpm * in = dpm;
	while(in){
		if(in->type == DPM_TYPE_YUVSCALE){
			t_dpm_yuvscale * dpm_yuvscale = (t_dpm_yuvscale *)in;	
			if(dpm_yuvscale->output_width == scale_width && dpm_yuvscale->output_height == scale_height){
				return 0;
			}
		}
		in = in->in;
	}

	if(libvs_disable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF, 0x10000000) != 0){
		return -1;
	}

	if(dpzoom_scale_size(dpm, scale_width, scale_height) == 0){
		t_dpm_img * dpm_img = NULL;
		if(dpm->next){
			dpm_img = dpm->next;
			dpm_img->width = scale_width;
			dpm_img->height = scale_height;
			dpm_img->output_width = scale_width;	
			dpm_img->output_height = scale_height;	
		}
	}

	return libvs_enable_manual(dpm, DPM_TYPE_MIPICIF, DPM_TYPE_VIF);
}
#endif

t_libdatapath_cfg libdatapath_cfg[] =
{
	{
#ifdef CONFIG_FAST_BOOT_EN
//		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE | DATAPATH_FASTBOOT_DEBUG,
//		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE | DATAPATH_FASTBOOT_VDEBUG,
		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_2STAGE ,
		.fastboot_vs_handle_cmd = _fastboot_vs_handle_cmd,
#endif		
		.head = &dp_sensor,
	},
};

#ifdef CONFIG_MPU_AENC_EN
AREC_ALLOCATE_STEREO
FRAMELIST_DECLARE(areclist, AREC_FRMLIST_NUM )
t_liba_ecfg g_liba_ecfg =
{
	AREC_CONFIG_STEREO
	.outbuf_addr = AUDIO_REC_BUF,	
	.outbuf_size = AUDIO_REC_BUF_SIZE, 
	.list = &areclist,
};
#endif

#ifdef CONFIG_MPU_ADEC_EN
ADEC_ALLOCATE
FRAMELIST_DECLARE(adeclist, ADEC_FRMLIST_NUM)
t_liba_dcfg g_liba_dcfg =
{
    ADEC_CONFIG
    .inbuf_addr = AUDIO_DEC_BUF,
    .inbuf_size = AUDIO_DEC_BUF_SIZE,
    .list = &adeclist,
};
#endif

