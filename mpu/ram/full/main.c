#include "includes.h"

#define ENABLE_UART

#ifdef ENABLE_UART
DECLARE_DEBUGPRINTF_BUF(512)
#endif
extern unsigned int ram_bss_start;
extern unsigned int ram_bss_end;

extern u32 ba2app_cfg_start,ba2app_cfg_end;

static void sys_init(void)
{
}

void except_tick (void)
{
}

#ifdef CONFIG_MPU_TICK2_EN
static t_dpm_osd4l * dpm_osd4l_cfg = NULL;
static t_dpm_osdrlc * dpm_osdrlc_cfg = NULL;
unsigned long tick2_irq_handler(void)
{
	tick2_clean_disable();
	t_libdatapath_cfg * datapath_cfg = &libdatapath_cfg[0];
	if(datapath_cfg){
		t_dpm * dpm_out = datapath_cfg->head;
		if(dpchain_timeout_find(dpm_out) == 0){
			//timeout module found, return
			return 0;	
		}
		while(dpm_out != NULL){
			if(dpm_out->type == DPM_TYPE_OSD4L){
				dpm_osd4l_cfg = (t_dpm_osd4l *)dpm_out;
				if(dpm_osd4l_cfg->cb_osd4l_tick2_draw){
					dpm_osd4l_cfg->cb_osd4l_tick2_draw();
					break;
				}
			}else if(dpm_out->type == DPM_TYPE_OSDRLC){
				dpm_osdrlc_cfg = (t_dpm_osdrlc *)dpm_out;
				if(dpm_osdrlc_cfg->cb_osdrlc_tick2_draw){
					dpm_osdrlc_cfg->cb_osdrlc_tick2_draw();
					break;
				}
			}
			dpm_out = dpm_out->out;
		}
	}
	syslog(LOG_NOTICE, "T");
	return 0;
}
#endif

int main(void)
{
	WATCHDOG_DISABLE;

#ifdef CONFIG_ICACHE
	ic_enable();
#endif

#ifdef CONFIG_DCACHE
#ifndef CONFIG_MPU_DCACHE_OFF
	dc_enable();
#endif
#endif

	/* clear rtos bss */
	unsigned int *addr;
	for(addr = &ram_bss_start; addr < &ram_bss_end; addr ++)
   	{
		*((volatile unsigned int *)addr) = 0;
  	}
	//TODO: clean
	//SC_CLK0_EN(ALL);  ///<0x30
	//SC_CLK1_EN(ALL);  ///<0x34
	//SC_CLK2_EN(ALL);  ///<0x38
	
	//SC_RESET0_RELEASE(ALL); ///< 0x20
	//SC_RESET1_RELEASE(ALL); ///< 0x24
	//SC_RESET2_RELEASE(ALL); ///< 0x84
	//SC_RRESET0_RELEASE(ALL); ///< 0x28
	//SC_RRESET1_RELEASE(ALL); ///< 0x2c

#ifdef ENABLE_UART
extern unsigned int rawuart_base;
#ifdef CONFIG_CONSOLE_UART1
	rawuart_base = 0xc0083400;
#else
	rawuart_base = 0;
#endif

	debug_printf_init(
#ifdef CONFIG_MPUUART_LOWLEVEL_EN
					DEBUG_PRINTF_MODE_TRUNCATE
#else
					DEBUG_PRINTF_MODE_DIRECT
#endif
					| DEBUG_PRINTF_FLAG_CRLF, (t_debugprintf_hw *)&g_debugprintf_hw_uart, 
#ifdef CONFIG_CONSOLE_UART1
					1
#else
					0
#endif
					);

#ifdef CONFIG_FAST_BOOT_EN
	lowlevel_forceputc('\n');
	lowlevel_forceputc('&');
#else
    debug_printf("\n\n************ This is MPU Program **************\n");
#endif // CONFIG_FAST_BOOT_EN

#endif

	sys_init();

	//init newos
	memset(&NewOSCtx, 0, sizeof(t_newos));
	NewOSCtx.cb_start_schedule = S_start_schedule;
#ifndef CONFIG_FAST_BOOT_EN
	NewOSCtx.loglevel_default = LOG_ERR;	//LOG_WARNING;
#else
	NewOSCtx.loglevel_default = LOG_ERR;
#endif
//	NewOSCtx.loglevel_default = LOG_NOTICE;
	
#ifdef CONFIG_MPU_VENC_EN
	dpm_syslog_disable = 0;
//	dpm_syslog_disable = ~((1<<DPM_TYPE_MIPICIF) | (1<<DPM_TYPE_MIPIRX)); //only output LOG_NOTICE/LOG_INFO/LOG_DEBUG from MIPICIF/MIPIRX.
//	dpm_syslog_disable = ~((1<<DPM_TYPE_FB) | (1<<DPM_TYPE_MIPICIF)| (1<<DPM_TYPE_VE)); //for hdr debug
#endif

	newos_init(&NewOSCtx);

	//fix
extern void newos_fix(void);
	newos_fix();

	//add tasks
	t_task * g_task_cfg = (t_task *)(&ba2app_cfg_start);
	for(; g_task_cfg < (t_task *)(&ba2app_cfg_end) ; g_task_cfg ++){
		if(task_add(g_task_cfg, task_generic_main) < 0){
			debug_printf("AddTask %x failed !!!\n", g_task_cfg);
			while(1);
		}
	}

	//add idle task
	task_idle_init();

	//start schedule
	newos_start();

	debug_printf("NEVER HERE!\n");
	while(1);
}

#if 0 //code to test BA22 gate clock, BA22 gate itself and recall by timer.
#define ENABLE_TICK_TMP do{u32 sr; MFSPR(SPR_SR, sr); sr |= SPR_SR_TEE; MTSPR(SPR_SR, sr); }while(0)
	tick1_set_rt(4800000000*4/ TICKS_PER_SEC);
	ENABLE_TICK_TMP; 
	MTSPR(SPR_PM_EVENT_CTRL,0x2); //enable PM event

	debug_printf("GATE BA22 clock\n");
	WriteReg32(SC_BASE_ADDR+0x60,ReadReg32(SC_BASE_ADDR+0x60)|BIT16);
	WriteReg32(SC_BASE_ADDR+0x60,ReadReg32(SC_BASE_ADDR+0x60)&~BIT16); //clear it
#endif
