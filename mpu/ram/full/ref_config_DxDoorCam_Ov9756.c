#include "includes.h"
/* TODO: sample setting, should moved to board_xxx.c

   下面这个数据结构描述了这样一个datapath(print by libdatapath_print()):

   ->: single path
   +>: concurrent path
   *>: sequential path
->SNR9756 ->MIPIRX0->MIPICIF0 ->RGBIR ->RAWSCL0 ->IDC0 +>ISP0 ->FB1 *>ECIF0 ->OSD4L0 ->VIF1 +>VE0 
                                                  |     		|      				      | +>IMG0 
                                               	  |				| *>ECIF0 ->YUVSCL1 ->OSD4L0 ->VIF1 ->VE1 
                                         		  |	+>YUVSCL2 ->MD0 
   */

/*memory config
                            addr_start  addr_end(not included) start_offset    size
	SRAM_START				0x10000000	     
    CODE  	  				0x10000000 --- (0x10060000)
	VBUF_BASE 				0x10060000 --- (0x10090000) 
	MPU_CODE 				0x1008a000 --- (0x10100000) 

	DDR_START 				0x10100000                      	DDR offset
	CODE_SIZE  				0x10100000 --- (0x10900000)  		0x000000       	8M 

	VS0_HWBUF				0x10900000 --- (0x10a77000)			0x800000      	1500k	
	hw ref buf:1280*(720+16*5)*3/2 = 0x177000
	VS0_BS_BUF	  			0x10a77000 --- (0x10d77000)					     	3M
	VS1_HWBUF			 	0x10d77000 --- (0x10eee000)        	
	hw ref buf:1280*(720+16*5)*3/2 = 0x177000
	VS1_BS_BUF 				0x10eee000 --- (0x110ee000)   			       		2M
	MOTION DETECT        	0x110ee000 --- (0x11129000)   				      	0x3b000(236k,QVGA)
	SENSOR_SCCB_GROUP      	0x11129000 --- (0x1112a000)   				        4K
	AUDIO_REC_BUF			0x1112a000 --- (0x111aa000)   				  		512k	
	AUDIO_DEC_BUF			0x111aa000 --- (0x1122a000)					 		512k
	IMG_BUF_START          	0x1122a000 --- (0x1172a000)   						5M

	OSDRLC_BASE_ADDR      	0x12000000							0x1f00000       1M 
	FB1_YBASE_ADDR        	0x12100000							0x2000000       
	FB1_UVBASE_ADDR       	0x13100000							0x3000000 
*/

#define MAX_WIDTH		1280	
#define MAX_HEIGHT		720
#define	FRAME_RATE		24	
///< SRAM memory map
#define  VIF_BUF_ADDR     (CONFIG_VBUF_BASE - MEMBASE_SRAM) ///< offset
#define  OSD4L_MODE0_BASE_ADDR   (0x80000) ///< offset
#define  OSD4L_MODE1_BASE_ADDR   (0x88000)

///< DDR memory map
#define	DDR_BUF_BASE_OFFSET		(0x800000)

#define	VS0_BUF_SIZE			(3*1024*1024)
#define	VS1_BUF_SIZE			(2*1024*1024)
#define VS0_HWBUF_SIZE			(((MAX_WIDTH * 16 * 3) / 2) * ((MAX_HEIGHT/16) + 5))  ///< Don't Change
#define VS1_HWBUF_SIZE			(((MAX_WIDTH * 16 * 3) / 2) * ((MAX_HEIGHT/16) + 5))  ///< Don't Change
#define VE_STREAM0_HWBUF_BASE  	(MEMBASE_DDR+DDR_BUF_BASE_OFFSET)
#define VE_STREAM1_HWBUF_BASE  	(VE_STREAM0_HWBUF_BASE + VS0_BUF_SIZE + VS0_HWBUF_SIZE)

/* MD */
#define MD_MASK_BASE_ADDR    	(VE_STREAM1_HWBUF_BASE + VS1_HWBUF_SIZE + VS1_BUF_SIZE)
#define MD_WIDTH				320
#define MD_HEIGHT				240
#define MD_BUF_SIZE    			((((MD_WIDTH*MD_HEIGHT+15)/16)*4 + ((MD_WIDTH*MD_HEIGHT+7)/8)*8 + ((MD_WIDTH*MD_HEIGHT+255)/256)*4) * 8) //777600(0xbdd80) 
/* SCCB */
#define SCCB_GRP_BUF       		(MD_MASK_BASE_ADDR + MD_BUF_SIZE)
#define SCCB_GRP_BUF_SIZE       (4*1024)///< Don't Change
/* Audio */
#define AUDIO_REC_BUF			(SCCB_GRP_BUF + SCCB_GRP_BUF_SIZE) 
#define AUDIO_REC_BUF_SIZE		(512*1024)
#define AREC_FRMLIST_NUM 		20
#define AUDIO_DEC_BUF			(AUDIO_REC_BUF + AUDIO_REC_BUF_SIZE)
#define AUDIO_DEC_BUF_SIZE		(512*1024)
#define ADEC_FRMLIST_NUM 		20
/* IMG */
#define  IMG_BUF_START    		(AUDIO_DEC_BUF + AUDIO_DEC_BUF_SIZE)
#define  IMG_BUF_SIZE      		(1024*1024*5)           //5M
#define  IMG_ENC_SIZE      		(1024*1024)           //1M

#define	FB1_HW_FRM_SIZE			(MAX_WIDTH * MAX_HEIGHT / 8)//actual_size/8
#define	FB3_HW_FRM_SIZE			(MAX_WIDTH * MAX_HEIGHT / 8)//actual_size/8
#define FB1_YBASE_ADDR        	(0x2000000/8) ///< DDR bank1   address/8 start from DDR 32M
#define FB1_UVBASE_ADDR       	(0x3000000/8) ///< DDR bank2 start from DDR 48M

#ifdef CONFIG_VIDEO_ANALYSIS_EN
#define MD_COPY_BASE_ADDR	   (IMG_BUF_START + IMG_BUF_SIZE)
#define MD_COPY_BASE_ADDR1	   (MD_COPY_BASE_ADDR+0x100000)	
#define MD_COPY_BASE_ADDR2	   (MD_COPY_BASE_ADDR1+0x100000)	
#endif

#define SCALE_WIDTH		640
#define SCALE_HEIGHT	352
//#define USE_ECIF	
//#define DUAL_STREAM
//#define SINGLE_IMG
//#define DUAL_VS_WITHIMG	//if need H.264 + IMG, define this, if H.264*2 + IMG, also define DUAL_STREAM 

//#define USE_RAWSCALE
//#define MD_EN
//#define OSD4L_EN
#define ECIF_HBLK	35//set to 8 if disable VEI4, and support dual stream 25fps
#ifdef USE_RAWSCALE
#define	VS0_WIDTH	640
#define	VS0_HEIGHT	352
#define ECIF_CPP1	2	
#else
#define	VS0_WIDTH	1280
#define	VS0_HEIGHT	720
#define ECIF_CPP1	2	
#endif
#define VS0_RC_EN
#define VS1_RC_EN
#define USE_RGBIR
#define MD_EN

static t_dpm_md dp_md =
{
	DPM_INIT_MD

#ifdef CONFIG_VIDEO_ANALYSIS_EN
	.algo_en = 1,
	.mask_addr = (MD_MASK_BASE_ADDR - MEMBASE_DDR)/8,
	.copy_addr = MD_COPY_BASE_ADDR,
	.prev_addr0 = MD_COPY_BASE_ADDR1,
	.prev_addr1 = MD_COPY_BASE_ADDR2,
	.intr_mask = 0xf,
#else
	.win_cnt = 0xff,
	.mask_addr = (MD_MASK_BASE_ADDR - MEMBASE_DDR)/8,
	.intr_mask = 0xf,

	///< QVGA
	.md_win[0] = {
		.xstart = 0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[1] = {
		.xstart = 0x50,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[2] = {
		.xstart = 0xa0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[3] = {
		.xstart = 0xf0,
		.ystart = 0,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[4] = {
		.xstart = 0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[5] = {
		.xstart = 0x50,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[6] = {
		.xstart = 0xa0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
	.md_win[7] = {
		.xstart = 0xf0,
		.ystart = 0x78,
		.width = 0x50,
		.height = 0x78,
	},
#endif
};

static t_dpm_yuvscale dp_yuvscale2 =
{
	DPM_INIT_YUVSCALE
	.id = 2,
	.out = &dp_md,
	.output_width = MD_WIDTH,
	.output_height = MD_HEIGHT,
	.yuv422_out = 1,
};

#ifdef VS1_RC_EN
static t_ve_rc_cfg dp_ve_vs1_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 500 * 1024,
	.init_qp = 32,
	.max_gop_size = 60,
};
#endif
FRAMELIST_DECLARE(vs1_framelist, 60);
static t_dpm_ve dp_ve_vs1 =
{
	DPM_INIT_VE
	.id = 1,
	.ve_ref_buf_base = VE_STREAM1_HWBUF_BASE,
	.bs_buf_size = VS1_BUF_SIZE,
#ifdef VS1_RC_EN
	.rc_cfg = &dp_ve_vs1_rc,
#else
	.fix_i_qp = 29,
	.fix_p_qp = 31,
	.gop_size = 20,
#endif
	.frame_rate = FRAME_RATE,
	.framelist = &vs1_framelist,
	.add_sps_head_en = ADD_SPS_MODE1,
	.entropy = 1,
	.idr_period = 1,
	.flag = DPM_FLAG_END,
	.bs_buf_skip_th = 100 * 1024,
};

static t_dpm_vif dp_vif1_mode1 =
{
	DPM_INIT_VIF
	.id = 1,
	.out = &dp_ve_vs1,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
#ifndef OSD4L_EN
	.yuv420_in = 1,
#endif
};

static t_dpm_osd4l dp_osd4l_mode1 =
{
	DPM_INIT_OSD4L
	.id = 1,
	.out = &dp_vif1_mode1,
#ifdef OSD4L_EN
	.osd_win[0] = {
		.win_no = 1,
		.win_hsize = 240, ///< must be the mutiply of 4
		.win_vsize = 16,
		.win_hstart = 0,
		.win_vstart = 0,
		.win_memaddr = OSD4L_MODE1_BASE_ADDR,
		.color[0] = (0xff7f7f|(2<<24)),
		.color[1] = (0x1dfe6a|(3<<24)),
		.color[2] = (0x4c54ff|(3<<24)),
		.color[3] = (0xe10094|(3<<24)),
	},
#else
	.bypass = 1,
#endif
};

static t_dpm_yuvscale dp_yuvscale1_mode1 =
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_osd4l_mode1,
	.output_width = SCALE_WIDTH,
	.output_height = SCALE_HEIGHT,
};

static t_dpm_ecif dp_ecif0_mode1 =
{
	DPM_INIT_ECIF
	.id = 0,
	.out = &dp_yuvscale1_mode1,
#ifdef DUAL_STREAM
	.flag = DPM_FLAG_END,
#endif
	.frame_num = 0,
	.p_hblk = ECIF_HBLK,//VS0_WIDTH/40,
	.p_vsync = 2,
	.p_v2h = 2,
	.p_h2v = 2,
	.p_h2e = 20,
	.cpp0 = 0,//1,
	.cpp1 = 0,//ECIF_CPP1,
	.int_mask = 0,	//0: enable irq, 1: disable irq
	.data_format = ECIF_FORMAT_YUV422_NODEC,
	.handshk = SOFTWARE_HANDSHAKE,//HARDWARE_HANDSHAKE,
	.yuv420in_yuv422out_en = 0,
	.crop_x = 0,
	.crop_y = 0,
	.pingpong_en = 0,
	.yaddr = FB1_YBASE_ADDR,
	.uvaddr = FB1_UVBASE_ADDR,
	.offset = 0,
};

#define MANUAL_DROP_FRM		//sensor input frame rate fixed at 24fps, use firmware to control encode frame rate
#ifdef MANUAL_DROP_FRM
static t_vs_fps_cfg vs0_fps;
static t_vs_fps_cfg vs2_fps;
#endif
FRAMELIST_DECLARE(vs2_framelist, 10);

static u32 vs0_handle_cmd(struct s_dpm_ve * dpm, u32 cmd, u32 arg, u32 * handled);
static t_dpm_img dp_img_vs2 =
{
	DPM_INIT_IMG
#ifdef SINGLE_IMG
	.id = 0,
#elif defined (DUAL_STREAM) 
	.id = 2,
#else
	.id = 1,
#endif
	.buflist_start = IMG_BUF_START,
	.buflist_size = IMG_BUF_SIZE, // img gen buffer. 
	.imgenc_size = IMG_ENC_SIZE, // img encode size, one image frame should be less than imgenc_size 
	.qs = 4,
	.framelist = &vs2_framelist,
	.flag = DPM_FLAG_END,
	.handle_cmd = vs0_handle_cmd,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs2_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
};

#ifdef VS0_RC_EN
static t_ve_rc_cfg dp_ve_vs0_rc =
{
	.rc_mode = RC_FEATURE,	
	.bit_rate = 1500 * 1024,
	.init_qp = 30,
	.max_gop_size = 10,
	.min_qp[0] = 1,
	.min_qp[1] = 1,
};
#endif

static u32 vs0_handle_cmd(struct s_dpm_ve * dpm, u32 cmd, u32 arg, u32 * handled)
{
	switch(cmd){
#ifdef MANUAL_DROP_FRM
	case (MPUCMD_TYPE_VS | VSIOC_VS_FRMRATE_SET):
		*handled = 1;
		return vs_set_fps_drop(dpm, arg);
	case (MPUCMD_TYPE_VS | VSIOC_FRMRATE_SET):
		*handled = 1;
		if(dpm_sensor_effect(dpm, SNR_EFFECT_FRMRATE, arg) != 0){
			return 1;	
		}	
		if(dpm->type == DPM_TYPE_VE){
			t_dpm_ve * dpm_ve = (t_dpm_ve *)dpm;
			if(arg > dpm_ve->frame_rate){
				arg = dpm_ve->frame_rate;	
			}
			return vs_set_fps_drop(dpm, arg);
		}
	case (MPUCMD_TYPE_VS | VSIOC_VTS_FRMRATE_SET):
		{
		t_dpm_sensor * dpm_sensor = dpm;
		while(dpm_sensor){
			if(dpm_sensor->type == DPM_TYPE_SENSOR){
				dpm_sensor->sensor_cfg->cur_frame_rate = arg;
				syslog(LOG_INFO, "change vts fps to %d\n", arg);	
				break;
			}
			dpm_sensor = dpm_sensor->in;
		}
		*handled = 1;
		return vs_set_fps_drop(dpm, arg);
		}
#endif
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		*handled = 1;
		return dpzoom_ecif_crop_coordinate(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
		*handled = 1;
		return dpzoom_noscale_ecif_crop_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
		*handled = 1;
		return dpzoom_scale_size(dpm, ((arg>>16)&0xffff), (arg&0xffff));
	}

	return 0;
}

FRAMELIST_DECLARE(vs0_framelist, 60);
static t_dpm_ve dp_ve_vs0 =
{
	DPM_INIT_VE
	.id = 0,
	.ve_ref_buf_base = VE_STREAM0_HWBUF_BASE,
	.bs_buf_size = VS0_BUF_SIZE,
#ifdef VS0_RC_EN
	.rc_cfg = &dp_ve_vs0_rc,
#else
	.fix_i_qp = 30,
	.fix_p_qp = 32,
	.gop_size = 10,
#endif
	.frame_rate = FRAME_RATE,
	.framelist = &vs0_framelist,
#ifdef DUAL_VS_WITHIMG 
	.next = &dp_img_vs2,
#else
	.flag = DPM_FLAG_END,
#endif
	.add_sps_head_en = ADD_SPS_MODE1,
	.idr_period = 1,
	.entropy = 1,
//	.max_node_in_list = 5,
	.bs_buf_skip_th = 100 * 1024,
#ifdef MANUAL_DROP_FRM
	.fps_cfg = &vs0_fps,
	.cb_frmrate_drop = vs_frm_drop_cb,
#endif
	.handle_cmd = vs0_handle_cmd,
};

static t_dpm_vif dp_vif1_mode0 =
{
	DPM_INIT_VIF
	.id = 1,
#ifdef SINGLE_IMG
	.out = &dp_img_vs2,
#else
	.out = &dp_ve_vs0,
#endif
	.no_kick = 0,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
#ifndef OSD4L_EN
	.yuv420_in = 1,
#endif
};

static t_dpm_vif dp_vif0_mode0 = 
{
	DPM_INIT_VIF
	.id = 0,
	.out = &dp_ve_vs0,
	.no_kick = 0,
	.buf_base = VIF_BUF_ADDR, ///< SRAM offset
	.intr_mask = BIT1|BIT2|BIT3,
	.yuv420_in = 1,
};

static t_dpm_osd4l dp_osd4l_mode0 =
{
	DPM_INIT_OSD4L
	.id = 0,
	.out = &dp_vif1_mode0,
#ifdef OSD4L_EN
	.osd_win[0] = {
		.win_no = 1,
#ifdef USE_RAWSCALE
		.win_hsize = 400,
		.win_vsize = 32,
#else
		.win_hsize = 800,
		.win_vsize = 64,
#endif
		.win_hstart = 0,
		.win_vstart = 0,
		.win_memaddr = OSD4L_MODE0_BASE_ADDR,
		.color[0] = (0xff7f7f|(2<<24)),
		.color[1] = (0x1dfe6a|(3<<24)),
		.color[2] = (0x4c54ff|(3<<24)),
		.color[3] = (0xe10094|(3<<24)),
	},
#else
	.bypass = 1,
#endif
};

static t_dpm_yuvscale dp_yuvscale1_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 1,
	.out = &dp_osd4l_mode0,
#ifndef USE_ECIF
#ifdef MD_EN
	.next = &dp_yuvscale2,  ///< MD
#endif
#endif
#ifdef OSD4L_EN
	.yuv422_out = 1,
#endif
};

static t_dpm_osdrlc dp_osdrlc_mode0 = 
{
	DPM_INIT_OSDRLC
	.out = &dp_vif0_mode0,
	.bypass = 1,
};

static t_dpm_yuvscale dp_yuvscale0_mode0 = 
{
	DPM_INIT_YUVSCALE
	.id = 0,
	.out = &dp_osdrlc_mode0,
};

static t_dpm_ecif dp_ecif0_mode0 =
{
	DPM_INIT_ECIF
	.id = 0,
	//.out = &dp_osd4l_mode0,
	.out = &dp_yuvscale1_mode0,
#ifdef DUAL_STREAM
	.next = &dp_ecif0_mode1,
#endif
	.flag = DPM_FLAG_END,
	.frame_num = 0,
	.p_hblk = ECIF_HBLK,//VS0_WIDTH/40,
	.p_vsync = 2,
	.p_v2h = 2,
	.p_h2v = 2,
	.p_h2e = 20,
	.cpp0 = 0,//1,
	.cpp1 = 0,//ECIF_CPP1,
	.int_mask = 0,	//0: enable irq, 1: disable irq
	.data_format = ECIF_FORMAT_YUV422_NODEC,
	.handshk = SOFTWARE_HANDSHAKE,//HARDWARE_HANDSHAKE,
	.yuv420in_yuv422out_en = 0,
	.crop_x = 0,
	.crop_y = 0,
	.pingpong_en = 0,
	.yaddr = FB1_YBASE_ADDR,
	.uvaddr = FB1_UVBASE_ADDR,
	.offset = 0,
};

static t_dpm_fb_buf dp_fb1_buf0;
static t_dpm_fb_buf dp_fb1_buf3 =
{
	.ymem_base = (FB1_YBASE_ADDR+FB1_HW_FRM_SIZE * 3),
	.uvmem_base = (FB1_UVBASE_ADDR+FB1_HW_FRM_SIZE * 3),
	.next = &dp_fb1_buf0,
};

static t_dpm_fb_buf dp_fb1_buf2 =
{
	.ymem_base = (FB1_YBASE_ADDR+FB1_HW_FRM_SIZE * 2),
	.uvmem_base = (FB1_UVBASE_ADDR+FB1_HW_FRM_SIZE * 2),
	.next = &dp_fb1_buf3,
};

static t_dpm_fb_buf dp_fb1_buf1 =
{
	.ymem_base = (FB1_YBASE_ADDR+FB1_HW_FRM_SIZE),
	.uvmem_base = (FB1_UVBASE_ADDR+FB1_HW_FRM_SIZE),
	.next = &dp_fb1_buf2,
};

static t_dpm_fb_buf dp_fb1_buf0 =
{
	.ymem_base = FB1_YBASE_ADDR,
	.uvmem_base = FB1_UVBASE_ADDR, 
	.next = &dp_fb1_buf1,
};

static t_dpm_fb dp_fb1 = 
{
	DPM_INIT_FB
	.id = 1,
#ifdef MD_EN
	.next = &dp_yuvscale2,
#endif
	.out = &dp_ecif0_mode0,
//	.out = &dp_ecif0_mode1,		//if encode single stream with scale-down enable, use this

	.enabled = 1,
	.int_mask = 0,
	.data_format = FB_FORMAT_YUV422IN_NOSTORE,
	.pingpong_en = 0,
	.yaddr = FB1_YBASE_ADDR,
	.uvaddr = FB1_UVBASE_ADDR,
	.offset = 0,
	.bufhead = &dp_fb1_buf0,
};

static t_dpm_ispout dp_ispout0 =
{
	DPM_INIT_ISPOUT
	.id = 0,
#ifdef USE_ECIF
	.out = &dp_fb1,
#else
	.out = &dp_yuvscale1_mode0,
	//.out = &dp_yuvscale0_mode0,
#endif
};

static t_dpm_rawscale dp_rawscale0;
static t_dpm_ispidc dp_ispidc =
{
	DPM_INIT_ISPIDC
	.id = 0,
	.out = &dp_ispout0,
	.in = &dp_rawscale0,
#if defined (USE_RAWSCALE)|| defined(USE_RGBIR)
	.dummy_mode = IDC_DUMMY_MANUAL_MODE,
#else
	.dummy_mode = IDC_DUMMY_AUTO_MODE,
#endif
	.dummy_line = 0xc,
	.cpp = 0x00,
	.v2d1_hblk = 0x100,
};

static t_dpm_rawscale dp_rawscale0 =
{
	DPM_INIT_RAWSCALE
	.id = 0,
	.out = &dp_ispidc,
	.scale_factor = RAWSCALE_FACTOR_2X,
	.raw_format = RAWSCALE_RAW10,
#ifdef USE_RAWSCALE
	.hscale_en = 1,
	.vscale_en = 1,
	.in_vcrop_en = 1,
	.vstart = 16,
#else
	.hscale_en = 0,
	.vscale_en = 0,
#endif
};

static t_dpm_rgbir dp_rgbir = 
{
	DPM_INIT_RGBIR
	.out = &dp_rawscale0,
};
static t_dpm_mipicif dp_mipicif0 = 
{
	DPM_INIT_MIPICIF
	.id = 0,
#ifdef USE_RGBIR
	.out = &dp_rgbir,
#else
	.out = &dp_rawscale0,
#endif
};


static t_dpm_mipirx dp_mipirx0 = 
{
	DPM_INIT_MIPIRX
	.id = 0,
	.out = &dp_mipicif0,
#ifdef CONFIG_FAST_BOOT_EN
	.lane_num = 2,
#else
	.lane_num = 1,
#endif
	.four_sel = 1,
	.vc0_emb_id = 0x12,
	.vc1_emb_id = 0x12,
	.vc0_emb_use_dt = 0x1e,
	.vc1_emb_use_dt = 0x1e,
	.pnum_int = 0x4,
	.int_en = 0xfffffff,
	.c2d4_en = 0x1,
#ifdef CONFIG_FAST_BOOT_EN
	.invert_pclk =1,
#endif
};

static t_dpm_dvpcif dp_dvpcif0 =
{
	DPM_INIT_DVPCIF
	.id = 0,
	.out = &dp_rawscale0,
	.format_raw = 1,
};

static t_libsccb_cfg dp_sccbcfg =
{
	.mode = SCCB_MODE16,
	.buf_addr = SCCB_GRP_BUF,
	.max_len = SCCB_GRP_BUF_SIZE,
	.clk = 400000,
	.rd_mode = RD_MODE_NORMAL,
	.sccb_num = SCCB_NUM_0,
};

static t_dpm_sensor dp_sensor =
{
	DPM_INIT_SENSOR
	.sccb_cfg = &dp_sccbcfg,
//	.out = &dp_dvpcif0,
//	.name = "2710",
	.out = &dp_mipirx0,
	.name = "9750",
};

static _fastboot_vs_handle_cmd(int vs_id, u32 cmd, u32 arg)
{
	if(vs_id != 0){
		return -1;
	}

	switch(cmd){
	case (MPUCMD_TYPE_VS | VSIOC_CROP_CO_SET):
		dp_ispidc.crop_x = ((arg>>16)&0xffff)&(~0x1);
		dp_ispidc.crop_y = (arg&0xffff)&(~0x1);
		return 0;
	case (MPUCMD_TYPE_VS | VSIOC_CROP_SIZE_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);	
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			return 1;
		}
		dp_ispidc.output_width = width;
		dp_ispidc.output_height = height;
		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
		dp_ispout0.output_width = width;
		dp_ispout0.output_height = height;
		t_dpm *out = g_libdatapath_cfg->fastboot_isp_out;
		while(out!= NULL)
		{
			out->output_width = width;
			out->output_height = height;
			out = out->out;
		}
		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_RESOLUTION_SET):
	{
		u32 width = ((arg>>16)&0xffff) & (~0xf);
		u32 height = (arg&0xffff) & (~0xf);
		if(width < 64 || height < 64){
			syslog(LOG_ERR, "ERROR:size should be more than 64*64\n");
			return 1;
		}
		{
			u32 inwidth = dp_ispout0.output_width;
			u32 inheight = dp_ispout0.output_height;
			if(inwidth/width > 16 || inheight/height > 16){
				syslog(LOG_ERR, "ERROR:scaler ratio is more than 16!\n");
				return 3;
			}
		}

		dp_ve_vs0.width = width;
		dp_ve_vs0.height = height;
		dp_vif1_mode0.output_width = width;
		dp_vif1_mode0.output_height = height;
		dp_osd4l_mode0.output_width = width;
		dp_osd4l_mode0.output_height = height;
		dp_yuvscale1_mode0.output_width = width;
		dp_yuvscale1_mode0.output_height = height;
		return 0;
	}	
	case(MPUCMD_TYPE_VS | VSIOC_INIT_GOP_SET):
	{
		dp_ve_vs0_rc.init_gop_size = (arg&0xffff);
		dp_ve_vs0_rc.init_gop_num = ((arg>>16)&0xffff);
		return 0;
	}
	case(MPUCMD_TYPE_VS | VSIOC_GOP_SET):
	{
		dp_ve_vs0_rc.max_gop_size = arg;
		return 0;
	}
	case (MPUCMD_TYPE_VS | VSIOC_EXPOSURE_SET):
	{
		dp_ispidc.isp_brightness = arg;
		return 0;
	}
	}
	return -1;
}

t_libdatapath_cfg libdatapath_cfg[] =
{
	{
#ifdef CONFIG_FAST_BOOT_EN
//		.fastboot_flag = DATAPATH_FASTBOOT_EN | DATAPATH_FASTBOOT_DEBUG,
		.fastboot_flag = DATAPATH_FASTBOOT_EN,
		.fastboot_vs_handle_cmd = _fastboot_vs_handle_cmd,
#endif
		.head = &dp_sensor,
	},
};

#ifdef CONFIG_MPU_AENC_EN

AREC_ALLOCATE_STEREO
FRAMELIST_DECLARE(areclist, AREC_FRMLIST_NUM )
u8 __attribute__ ((aligned(4))) arec_framelist_buf[OUTBUF_ONELEN*AREC_FRMLIST_NUM];	

t_liba_ecfg g_liba_ecfg =
{
	AREC_CONFIG_STEREO
	.outbuf_addr = AUDIO_REC_BUF,	
	.outbuf_size = AUDIO_REC_BUF_SIZE, 
	.list = &areclist,
};

#endif

