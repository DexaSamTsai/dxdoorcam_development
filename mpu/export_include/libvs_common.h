/** 
 * @file    R3_fw/libmpu_linux/include/libvs_common.h
 * @brief   headers for libvs data structure
 *
 * This header contains data structure used by libvs to interface with MPU.
 *
 *
 * @date    2016/12/15
 */
#ifndef _LIBVS_COMMON_H_
#define _LIBVS_COMMON_H_

//NOTE: if arg/ret is pointer, please pay attention to the cache coherent issue.
//in ARM, need flush before mpu_cmd, and invalidate after mpu_cmd: share/media/vs/vs_ioctl.c
//in MPU, need invalidate before handle, and flush before return.

/**
 * @enum				enum_VS_IOC
 * @details				enum_VS_IOC
 * @ingroup				libvs
 */
//NOTE: From VSIOC_BITRATE_SET to VSIOC_GET_TYPE are used by rom. Never change them !!!
typedef enum
{
	VSIOC_BITRATE_SET = 0x10,		///< arg: bps ; ret: 0:OK, <0:failed
	VSIOC_FRMRATE_SET ,				///< arg: sensor frame rate; ret: 0:OK, <0:failed
	VSIOC_GOP_SET ,					///< arg: GOP, if adaptive GOP enabled, it is the maximum GOP size ; ret: 0:OK, !=0:failed
	VSIOC_ADPGOP_SET ,				///< arg: enable adaptive GOP; ret: 0:OK, !=0:failed
	VSIOC_MINGOP_SET ,				///< arg: if enable adaptive GOP, can set minimum GOP size; ret: 0:OK, !=0:failed
	VSIOC_FORCEIFRAME_SET ,			///< arg: the number of I frames need to be forced encoded ; ret: 0:OK, !=0:failed
	VSIOC_MINQP_SET ,				///< arg: minimum frame QP (IQP<<16|PQP) ; ret: 0:OK, !=0:failed
	VSIOC_MAXQP_SET ,				///< arg: maximum frame QP (IQP<<16|PQP); ret: 0:OK, !=0:failed
	VSIOC_MINQP_GET ,				///< ret: minimum frame QP (IQP<<16|PQP); 0:failed
	VSIOC_MAXQP_GET ,				///< ret: maximum frame QP (IQP<<16|PQP); 0:failed
	VSIOC_INITQP_SET ,				///< arg: initial QP for the bitstream; ret: 0:OK, !=0:failed
	VSIOC_QPDELTA_SET ,				///< arg: delta step between I frame and Q frame ; ret: 0:OK, !=0:failed
	VSIOC_IDRPERIOD_SET ,			///< arg: the period of IDR frame ; ret: 0:OK, !=0:failed
	VSIOC_MAXNODE_SET ,				///< arg: number of max nodes in frame list ; ret: 0:OK, <0:failed
	VSIOC_ADDSKIPFRM_SET ,			///< arg: add empty frame when video frame is skipped; ret: 0:OK, !=0:failed
	VSIOC_VS_FRMRATE_SET ,			///< arg: enable VE encode frame rate lower than sensor input by dropping frames; 0: disable manual frame rate, > 0, the frame rate to be encoded. ret: 0:OK, !=0:failed
	VSIOC_CROP_CO_SET,				///< arg: crop coordinate. (x<<16|y), ret: 0:OK, <0:failed
	VSIOC_CROP_SIZE_SET,			///< arg: crop size. (width<<16|height), ret: 0:OK, <0:failed
	VSIOC_EXPOSURE_SET ,			///< arg: sensor exposure compensation value; ret: 0:OK, <0:failed
	VSIOC_VIDEOMIRROR_SET ,			///< arg: if true, video images are reversed; ret: 0:OK, <0:failed
	VSIOC_VIDEOFLIP_SET ,			///< arg: if true, video images are flipped; ret: 0:OK, <0:failed
	VSIOC_ANTIFLICK_SET ,			///< arg: 0:disabled. 1:50HZ. 2:60hz; ret: 0:OK, <0:failed
	VSIOC_SENSORSIZE_SET ,			///< arg: sensor resolution, (width<<16)|height, need to re-init datapath; ret: 0:OK, <0:failed
	VSIOC_RESOLUTION_SET ,			///< arg: (width<<16)|height. ret: 0:OK, <0:failed
	VSIOC_GET_TYPE,					///< arg: NULL. ret: <0:vs_not_exist, 1:ve, 2:img, 0:others

	//NOTE: Never change above variables !!!
	
	VSIOC_FRMRATE_GET ,				///< arg: NULL ; ret: frame rate
	VSIOC_CONTRAST_SET, 
	VSIOC_SATURATION_SET,	
	VSIOC_VIDEOMIRRORFLIP_SET,		///< arg: if true, video images are flipped and mirrored; ret: 0:OK, <0:failed
	VSIOC_VIDEOPROFILE_SET ,		///< arg: 100:high profile, 77:main profile, 66:baseline; ret: 0:OK, !=0: failed
	VSIOC_VTS_FRMRATE_SET ,			///< called by ISP, change frame rate by VTS 
	VSIOC_TIMESTAMP_GET ,			///< return the current video stream time stamp
	VSIOC_INIT_GOP_SET ,			///< arg: (init_gop_num<<16|init_gop_size), set the size and number of init gop; ret: 0:OK, !=0:failed
	VSIOC_SPS_PPS_GET,				///< get sps/pps buffer addr. arg: NULL. ret: 0: fail, !0: sps_addr
	VSIOC_GOP_DURATION_SET ,		///< arg: gop duration seconde * 30 ; ret: 0:OK, !=0:failed
	VSIOC_OSD_START,				///< put OSD ioctl between VSIOC_OSD_START and VSIOC_OSD_END 
	VSIOC_OSD4L_PARAGET,			///< called by OSD4L.
	VSIOC_OSD4L_UPDATE,				///< called by OSD4L.
	VSIOC_OSD_ENABLE_SET,			///< called by OSD. 
	VSIOC_OSD_DISABLE_SET,			///< called by OSD.
	VSIOC_OSDRLC_BUFGET,			///< called by OSDRLC.
	VSIOC_OSDRLC_UPDATE,			///< called by OSDRLC.
	VSIOC_OSD_END,
	VSIOC_DROP_FRM_GET,				///< get dropped frames number 
	VSIOC_VS_FRM_TOTAL_GET,			///< get number of total encoded frames 
	VSIOC_VS_FRM_READ_GET,			///< get number of read frames
	VSIOC_VS_FRM_IN_BUF_GET,			///< get number of frames in VS buffer
	VSIOC_VS_MAX_NODE_SET,///<arg: > 0, set max node in list, 0: set max node in list to default total frame list number
	VSIOC_STRM_BUF_SIZE_GET,		///< get total stream buffer size
	VSIOC_STRM_BUF_LEFT_GET, ///< get left stream buffer size
	VSIOC_ENABLE_META,	///<arg:1,enable meta info in video 0:disable met in video
	VSIOC_DROPFRM_TH_SET,///<arg:threshold to drop old frames. 0: do not drop old frames
	VSIOC_IMG_QS_SET ,					///< arg: img qs; ret: 0:OK, <0:failed
	VSIOC_ISP_START,				///< put OSD ioctl between VSIOC_OSD_START and VSIOC_OSD_END 
	VSIOC_SET_SHARPNESS,	///arg:sharpness strength (0~4), ret: 0:OK, <0:failed	
	VSIOC_SET_SDE,			///< arg: sde type(0~13),ret:0:ok,>0:error 
	VSIOC_SET_DAYNIGHT,		///< arg: ISP_COLORMODE
	VSIOC_SET_AEC_MANUAL_EXP,	///< arg:exposure time for manual aecagc, unit: line
	VSIOC_SET_AEC_MANUAL_GAIN,///< arg:gain for manual aecagc, 0x80 equals to 1X
	VSIOC_SET_WDR,	///<arg:wdr strength (0~4), ret: 0:OK, <0:failed
	VSIOC_SET_MCTF,	///<arg:mctf strength (0~4), ret: 0:OK, <0:failed
	VSIOC_ISP_END,				///< put OSD ioctl between VSIOC_OSD_START and VSIOC_OSD_END 
	VSIOC_GET_BUFADDR,    ///< return buf addr of ddr
	VSIOC_GET_BUFMAXLEN,  ///< return buf addr max length 
}enum_VS_IOC;
typedef unsigned int VS_IOC;

#endif
