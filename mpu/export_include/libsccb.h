#ifndef _LIBSCCB_CTL_H_
#define _LIBSCCB_CTL_H_

/* 基于sccb_xxx和sccbgrp_xxx的统一sccb API，TODO: 支持GPIO模拟 */

#define SCCB_MODE8 0x0
#define SCCB_MODE16 0x1

/**
 * @details sccb configuration
 * @ingroup lib_sccb
 */
typedef enum e_libsccbgrp_sta
{
	SCCBG_STA_IDLE,
	SCCBG_STA_START,
	SCCBG_STA_DOING,
	SCCBG_STA_DONE,
	SCCBG_STA_NOACK,
}enum_libsccb_sta;
typedef unsigned char e_libsccb_sta;

typedef struct s_libsccb_cfg
{
	u8 mode; ///<SCCB_MODE8 or SCCB_MODE16
	u8 retry; ///<retry count when no ACK or other error, 0:no retry.
	u8 id; ///<sccb device ID
	u8 rd_mode; //RD_MODE_NORMAL: send stop bit; RD_MODE_RESTART: 1-step read
	u32 clk; ///<sccb speed, eg: 400000 for 400k clk.
	u32 timeout; ///<timeout value for retry, 0:use sys default
	u32 nonblock; ///<default 0 useblock mode, 1 use nonblock mode. Can use libsccb_check_done to check w/r status. 
	u32 clk_in; ///<sccb system clock in, for calc clk. in most cases, set it to IN_CLK

	u32 buf_addr;   // data combine address for group write
	u32 buf_len;   // data len of combine data for group write
	u32 max_len;   // max_len of combine buffer 

	u8 wr_check; ///<1:after write, read back the data to see if write correct.
	u8 sccb_num; /*must. There are 4 sccb: 0, 1, 2, 3, 0xff*/
    int (*dbg)(const char *fmt0, ...); //add debug info callback
	volatile e_libsccb_sta status;
	u8 reserved0;

#define SCCB_NUM_0		0x00
#define SCCB_NUM_1		0x01
#define SCCB_NUM_2		0x02
#define SCCB_NUM_3		0x03
#define SCCB_NUM_GPIO	0xff

	u16 gpio_sim_pin_scl;
	u16 gpio_sim_pin_sda;

#ifndef BUILD_FOR_BA
	pthread_mutex_t sccb_mutex;
	pthread_cond_t sccb_cond; 
#endif	
}t_libsccb_cfg;

s32 libsccb_init(t_libsccb_cfg * cfg);
s32 libsccb_rd(t_libsccb_cfg * cfg, s32 address);
s32 libsccb_wr(t_libsccb_cfg * cfg, s32 addr, s32 data);
s32 libsccb_wr_array(t_libsccb_cfg * cfg, void * config, int len);
s32 libsccb_seqread(t_libsccb_cfg *cfg, int address, int count, unsigned char * buf);
s32 libsccb_seqwrite(t_libsccb_cfg *cfg, int address, int count, unsigned char * buf);
s32 libsccb_combine_setting(t_libsccb_cfg* sccb_cfg, u8 *dst, u8 *src, u32 len);
s32 libsccb_group_write(t_libsccb_cfg* sccb_cfg, u8 *addr, u32 len);
/*to check w/r status for nonblock mode.
  return 1 .w/r done
  return 0 .w/r is running
*/
int libsccb_check_done(t_libsccb_cfg *sccb_cfg);
int libsccbgrp_irq_handler(void * arg);
int libsccb_grp_check(int sccb_num);	//check sccb group write done status ,if done ,switch to single mode

#endif
