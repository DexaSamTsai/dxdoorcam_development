#ifndef __LIBSCCB_OLD_H
#define __LIBSCCB_OLD_H

#define SCCB_WRITE		    0x37000000
#define SCCB_BUSY	 	    0x01
#define SCCB_SPEED_NORMAL	(200000)
#define SCCB_SNR_ID		    0x78

#define RD_MODE_NORMAL	    0
#define RD_MODE_RESTART	    1

/**
 * @brief init sccb
 * @details
 * @ingroup lib_sccb
 *
 * @param id
 * @param mode 
 * @return 0:ok
 *
 */
void sccb_set_old(u8 id, u8 mode, u8 sccb_num);
/**
 * @brief read data from sccb device
 * @ingroup lib_sccb
 *
 * @param address 
 *
 * @return <0: failed(eg:no ACK), >=0: ok
 *
 */
s32 sccb_rd_old(s32 address);
/**
 * @brief sequence read from sccb device 
 * @ingroup lib_sccb
 *
 * @param address
 * @param count number to be readed
 * @param buf buffer address for read 
 *
 * @return <0: failed, 0: ok
 *
 */
s32 sccb_seqread_old(s32 address, s32 count, u8 * buf);
/**
 * @brief write data to sccb device
 * @ingroup lib_sccb
 *
 * @param addr
 * @param data
 *
 * @return <0: failed(eg:no ACK), >=0: ok
 *
 */
s32 sccb_wr_old(s32 addr, s32 data);
/**
 * @brief sequence write to sccb device 
 * @ingroup lib_sccb
 *
 * @param address
 * @param count number to be writen
 * @param buffer address for write 
 *
 * @return <0: failed, 0: ok
 *
 */
s32 sccb_seqwrite_old(s32 address, s32 count, u8 * buf);
#endif
