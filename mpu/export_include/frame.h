/** 
 * @file    R3_fw/libmpu_linux/include/frame.h
 * @brief   headers for FRAME data structure
 *
 * This header contains all frame related data structure to interface with MPU.
 *
 *
 * @date    2016/12/15
 */
#ifndef _FRAME_H_
#define _FRAME_H_

typedef u8 FRAME_TYPE;
#define FRAME_TYPE_VIDEO_IDR       0
#define FRAME_TYPE_VIDEO_I         1
#define FRAME_TYPE_VIDEO_P         2
#define FRAME_TYPE_VIDEO_MIMG      3
#define FRAME_TYPE_AUDIO_ENCAUDIO  4

/* FRAME_STATUS for MPU use only */
typedef u8 FRAME_STATUS;
//INTERNAL_START
#define FRAME_STATUS_READY     1
#define FRAME_STATUS_DOING     2
#define FRAME_STATUS_DONE      3
//INTERNAL_END

/**
 * @struct		s_FRAME
 * @details		Structure of linked FRAME
 * @ingroup		generic
 */
typedef struct s_FRAME{
	FRAME_TYPE type;				///< frame type, could be I/P/IDR
	FRAME_STATUS status;			///< FRAME_STATUS, used by MPU only
#define VFRAME_FLAG_LAST_P_IN_GOP (0x1 << 0) ///< if it is the last p frame in current GOP, this bit will be set
#define VFRAME_FLAG_NON_SKIP_FRM  (0x1 << 1) ///< this is not a skipped frame added for synchronization, but a normal encoded frame
#define VFRAME_FLAG_AE_UNSTABLE   (0x1 << 7) ///< AE doesn't converged!
	u8 	flags;
	u8  padding;
	u32 addr;						///< the start address of the frame 
	u32 size;						///< the size of the frame 
	u32 addr1;						///< second address for the second part of the frame; 0: no second addr
	u32 size1;						///< the size of the second part of the frame
	u32 ts;							///< time stamp in the video(video start with ts = 0), If need the absolute timestamp from system powerup, do in system level.
	struct s_FRAME *next;			///< pointer to the next frame, used by MPU only
}FRAME;

#endif	/// __FRAME_H__
