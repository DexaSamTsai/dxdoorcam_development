/** 
 * @file    R3_fw/libmpu_linux/include/mpu_cmd.h
 * @brief   headers for MPU command/event types
 *
 * This header contains all definitions of MPU commands and events
 *
 *
 * @date    2016/12/16
 */
#ifndef __MPU_CMD_H__
#define __MPU_CMD_H__

/**
 * @enum			ISP_COLORMODE
 * @details			ISP color mode
 * @ingroup			libmpu
 */
typedef enum ISP_COLORMODE
{
	ISPCM_RGB_COLOR=0,		///< color mode from RGB
	ISPCM_IR_BW,			///< B/W mode from IR,
	ISPCM_AUTO,				///< Auto mode
	/*
	ISPCM_RGB_DESAT,	//desaturated mode from RGB
	ISPCM_RGB_BW,		//B/W mode from RGB, probably same as IR_BW
	*/
}ISP_COLORMODE;

/**
 * @enum			enum_MPU_CMD
 * @details			MPU command types
 * @ingroup			libmpu
 */
typedef enum{
#define MPUCMD_TYPE_SAMPLE	0x00000000
	MPUCMD_SAMPLE_RUN =		0x00000000,
	MPUCMD_SAMPLE_STOP,

#define MPUCMD_TYPE_APLAY	0x00020000
	MPUCMD_APLAY_INIT =		0x00020000,			///< arg:t_adec_inpara, ret:0:OK, <0: errror
	MPUCMD_APLAY_START,							///< arg: NULL, ret:0:OK
	MPUCMD_APLAY_STOP,							///< arg: NULL, ret:0:OK
	MPUCMD_APLAY_EXIT,							///< arg: NULL, ret:0:OK
	MPUCMD_APLAY_GETFRM,						///< arg: NULL, ret:0:fail >0: FRAME
	MPUCMD_APLAY_ADDFRM,						///< arg: FRAME,ret:0
	MPUCMD_APLAY_SETEQ,							///< arg: t_eq_param,ret:0

#define MPUCMD_TYPE_ARECORD	0x00030000
	MPUCMD_ARECORD_INIT =	0x00030000,			///< arg:t_adec_inpara, ret:0:OK, <0: errror
	MPUCMD_ARECORD_START,						///< arg: NULL, ret:0:OK
	MPUCMD_ARECORD_STOP,						///< arg: NULL, ret:0:OK
	MPUCMD_ARECORD_EXIT,						///< arg: NULL, ret:0:OK
	MPUCMD_ARECORD_GETFRM,						///< arg: NULL, ret:0:fail >0: FRAME
	MPUCMD_ARECORD_RELFRM,						///< arg: FRAME,ret:0
	MPUCMD_ARECORD_SETSR,						///< arg: sr,ret:0
	MPUCMD_ARECORD_GETDROPPED,					///< arg: NULL, ret:dropped audio ms
	MPUCMD_ARECORD_GETTOTFRMSINLIST,			///< arg: NULL, ret:total frames saved to framelist
	MPUCMD_ARECORD_GETTOTFRMSOUTLIST,			///< arg: NULL, ret:total frames read from framelist 
	MPUCMD_ARECORD_GETTOTLISTBUFSIZE,			///< arg: NULL, ret:total buffer size of framelist
	MPUCMD_ARECORD_GETUSEDLISTBUFSIZE,			///< arg: NULL, ret:used buffer size in framelist 
	MPUCMD_ARECORD_GETREFTM,					///< return the current audio stream time stamp 
	MPUCMD_ARECORD_SETDFMODE,                   ///< arg: dropping mode,ret:0
	MPUCMD_ARECORD_GETDFMODE,                   ///< arg: NULL, ret: current dropping mode
	MPUCMD_ARECORD_SETOLDFRMDUR,                ///< arg: time duration of frames kept in linklist,ret 0
	MPUCMD_ARECORD_GETOLDFRMDUR,                ///< arg: NULL, ret: time current duration for frames kept in linklist
	MPUCMD_ARECORD_SETADCPPBUFADDR,              ///< arg: buffer address, ret:0


#define MPUCMD_TYPE_DATAPATH	0x00040000
	MPUCMD_DATAPATH_INIT =		0x00040000,		///< arg: DATAPATH_ID, ret:0:OK, >0:error
	MPUCMD_DATAPATH_START =		0x00040001,		///< arg: NULL, ret:0:OK, >0:error
	MPUCMD_DATAPATH_STOP =		0x00040002,		///< arg: NULL, ret:0:OK, >0:error
	MPUCMD_DATAPATH_EXIT =		0x00040003,		///< arg: NULL, ret:0:OK, >0:error
	MPUCMD_DATAPATH_FASTINIT =	0x00040010,		///< arg: DATAPATH_ID, ret:0:OK, >0:error
	MPUCMD_DATAPATH_FASTSTART =	0x00040011,		///< arg: NULL, ret:0:OK, >0:error
	MPUCMD_DATAPATH_FASTFPS2FPS =	0x00040012,	///< arg: fps for fps2, ret:0:OK, >0:error
	MPUCMD_DATAPATH_FASTFPS2START = 0x00040013,
	MPUCMD_DATAPATH_FAST_SIMU = 0x00040014,		///< arg: DATAPATH_ID, ret:0:OK, >0:error
#define MPUCMD_TYPE_MD          0x00040200
	MPUCMD_DATAPATH_MD_GET_STA =	0x00040200,
	MPUCMD_DATAPATH_MD_CFG     =	0x00040201,
	MPUCMD_DATAPATH_MDWNDCNT_SET =  0x00040202,
	MPUCMD_DATAPATH_MD_GET_BUF			=	0x00040203,
	MPUCMD_DATAPATH_MDALGO_UNUSED1		=	0x00040204,
	MPUCMD_DATAPATH_MDALGO_UNUSED2		=	0x00040205,

#define MPUCMD_TYPE_VS		0x00060000
	/* Video Stream Commands */
	/* from 0x0006N000 to 0x0006Nfff, N(BIT 12~15) is the videostream id(0~0xf). */
	MPUCMD_VS_INIT =	0x00060000,				///< arg:NULL, ret:0:OK, >0:error
	MPUCMD_VS_START =	0x00060001,				///< arg:NULL, ret:0:OK, >0:error
	MPUCMD_VS_STOP =	0x00060002,				///< arg:NULL, ret:0:stopped. <0: error. >0:command send but not stopped. need send next STOP command to see if real stopped.
	MPUCMD_VS_EXIT =	0x00060003,				///< arg:NULL, ret:0:OK, >0:error
	MPUCMD_VS_GETFRM =	0x00060004,				///< arg:NULL, ret:NULL:no frame, !NULL: VFRAME_LIST. the return VFRAME_LIST is BIT31 set(no cache).
	MPUCMD_VS_RELFRM =	0x00060005,				///< arg:VFRAME_LIST, ret:0:OK, >0:error
	MPUCMD_VS_GET_RESOLUTION = 0x00060006,		///< arg: pointer to resolution. ret:0:OK, >0:error
	/* from 0x0006N010 ~ 0x0006Nfff: VS_IOC */

#define MPUCMD_TYPE_ISP		0x00070000
	MPUCMD_ISP_INIT =	0x00070000,
	MPUCMD_ISP_SET_LBUF =	0x00070001,			///< arg: L_BASE_ADDRESS, ret:0
	MPUCMD_ISP_SET_RBUF =	0x00070002,			///< arg: R_BASE_ADDRESS, ret:0
	MPUCMD_ISP_EXIT =       0x00070003,
	/* from 0x0007N010 to 0x0007Nfff, N(BIT 12~15) is the isp id(0~0xf). */
	MPUCMD_ISP_SET_BRIGHTNESS =	0x00070010,		///< arg: brightness, ret:0:OK, >0:error
	MPUCMD_ISP_SET_SHARPNESS  =	0x00070011,		///< arg: sharpness, ret:0:OK, >0:error
	MPUCMD_ISP_SET_SDE  =	0x00070012,			///< arg: sde type(0~13),ret:0:ok,>0:error 
	MPUCMD_ISP_SET_DAYNIGHT = 0x00070013,		///< arg: ISP_COLORMODE
	MPUCMD_ISP_SET_AEC_MANUAL_EN = 0x00070019,	///< arg:1 isp aecagc manual,0:isp aecagc auto
	MPUCMD_ISP_SET_AEC_MANUAL_EXP = 0x0007001a,	///< arg:exposure time for manual aecagc, unit: line
	MPUCMD_ISP_SET_AEC_MANUAL_GAIN = 0x0007001b,///< arg:gain for manual aecagc, 0x80 equals to 1X
	MPUCMD_ISP_SET_AWB_MANUAL_EN = 0x0007001c,	///< arg:1 isp awb manual,0:isp awb auto
	MPUCMD_ISP_GET_AWB_B_GAIN = 0x0007001d,		///< ret: get b gain 0x80 equals to 1X
	MPUCMD_ISP_GET_AWB_G_GAIN = 0x0007001e,		///< ret: get g gain 0x80 equals to 1X
	MPUCMD_ISP_GET_AWB_R_GAIN = 0x0007001f,		///< ret: get r gain 0x80 equals to 1X
	MPUCMD_ISP_ISPOUT_SEL = 0x00070020,			///< arg: DATAPATH_IDC_ID, ret:0:OK, >0:error
	MPUCMD_ISP_SCCBRD = 0x00070021,				///< arg: (sensorid << 24) | sccbaddr, ret:sccbret
	MPUCMD_ISP_SCCBWR = 0x00070022,				///< arg: (sensorid << 24) | sccbaddr, ret:sccbret
	MPUCMD_ISP_GET_LUMADDR = 0x00070023,		///< ret: lum address
	MPUCMD_ISP_SET_EXP = 0x00070024,			///< arg: exp
	MPUCMD_ISP_SET_GAIN = 0x00070025,			///< arg: gain
	MPUCMD_ISP_GET_EXP = 0x00070026,			///< ret: exp
	MPUCMD_ISP_GET_GAIN = 0x00070027,			///< ret: gain
	MPUCMD_ISP_SET_CALI_AWB = 0x00070028,		///< arg: buffer address, ret 0:OK, >0:error
	MPUCMD_ISP_SET_CALI_LENC_A = 0x00070029,	///< arg: buffer address, ret 0:OK, >0:error
	MPUCMD_ISP_SET_CALI_LENC_C = 0x0007002a,	///< arg: buffer address, ret 0:OK, >0:error
	MPUCMD_ISP_SET_CALI_LENC_D = 0x0007002b,	///< arg: buffer address, ret 0:OK, >0:error
	MPUCMD_ISP_SET_AUTOFPS = 0x0007002c,		///< arg: minimum auto fps, ret 0: OK, > 0: error
	MPUCMD_ISP_GET_RGBIR_CGAIN = 0x0007002d,
	MPUCMD_ISP_SET_SAT_THR = 0x0007002e,		///< arg: minimum auto fps, ret 0: OK, > 0: error
	MPUCMD_ISP_SET_COEFF_THR = 0x0007002f,		///< arg: minimum auto fps, ret 0: OK, > 0: error
	MPUCMD_ISP_SET_COEFF = 0x00070031,			///< arg: minimum auto fps, ret 0: OK, > 0: error
	MPUCMD_ISP_SET_HDR = 0x00070032,			///< arg: 1 hdrenable, 0 non hdr
	MPUCMD_ISP_GET_AEC_STABLE = 0x00070033,     ///arg: get aec stable flag ret 1: ok
	MPUCMD_ISP_GET_HIST = 0x00070034,
	MPUCMD_ISP_SET_ROI = 0x00070035,			///arg: coordinate address
	MPUCMD_ISP_GET_MEANY = 0x00070036,			///ret: get meanY
	MPUCMD_ISP_GET_DAYNIGHT = 0x00070037,		///< ret: get day/night mode
	MPUCMD_ISP_GET_HDR = 0x00070038,		///< ret: get HDR/nonHDR mode
	MPUCMD_ISP_GET_HDR_STABLE = 0x00070039,		///< ret: get HDR stable flag, ret 1: ok
	MPUCMD_ISP_SET_MCTF = 0x0007003a,	///<arg : MCTF, ret 0: OK
	MPUCMD_ISP_SET_WDR = 0x0007003b,	///<arg : WDR, ret 0: OK
	MPUCMD_ISP_SET_AWB_B_GAIN = 0x0007003c,			///< arg: awb 
	MPUCMD_ISP_SET_AWB_G_GAIN = 0x0007003d,			///< arg: awb 
	MPUCMD_ISP_SET_AWB_R_GAIN = 0x0007003e,			///< arg: awb 

#define MPUCMD_TYPE_SYS		0x00080000
	MPUCMD_SYS_NOP	=	0x00080000,
	MPUCMD_SYS_GETPRIO =		0x00080001,		///< arg:taskname_str, ret:prio, <0:error
	MPUCMD_SYS_SETLOGMASK =		0x00080002,		///< arg:((task_prio)<<16)|logmask,task_prio=0xffff to set all the tasks logleve. ret:0
	MPUCMD_SYS_PS =				0x00080003,		///< arg:NULL, ret:0
	MPUCMD_SYS_TEST_BA =  0x00080004,
}enum_MPU_CMD;

typedef unsigned int E_MPU_CMD;

#define FAST_STAGE_IDLE			0
#define FAST_STAGE_INITED		1
#define FAST_STAGE_FPS1RDY		2				///< fps1 sensor setting done, start output fps1
#define FAST_STAGE_FPS1WAITSTOP	3				///< fps1 is enough, need wait mipirx eof to stop it
#define FAST_STAGE_FPS1STOP		4				///< fps1 is stopped in mipirx eof
#define FAST_STAGE_FPS1DONE		5				///< fps1 is done, app can send fps2 settings now
#define FAST_STAGE_FPS2RDY		6				///< in task, change sensor setting to fps2
#define FAST_STAGE_FPS2START    7				///< start fps2 datapath
#define FAST_STAGE_FPS2STARTED  8 
#define FAST_STAGE_LAST			FAST_STAGE_FPS2STARTED

/**
 * @enum            enum_MPU_EVT
 * @details         MPU event types
 * @ingroup         libmpu
 */
typedef enum{
	MPUEVT_VS0_NOTEMPTY = 0,					///< videostream0 not empty.
	MPUEVT_VS1_NOTEMPTY = 1,
	MPUEVT_VS2_NOTEMPTY = 2,
	MPUEVT_VS3_NOTEMPTY = 3,
	MPUEVT_FASTBOOT_STATUS = 10,				///< MPUEVT_FASTBOOT_STATUS + FAST_STAGE_XXX
	MPUEVT_MDBUF_NOTEMPTY = 29,
	MPUEVT_AENC_NOTEMPTY = 30,
	MPUEVT_ADEC_NOTFULL = 31,
}enum_MPU_EVT;
typedef unsigned char E_MPU_EVT;
#define MPUEVT_MAX 32

#endif ///__LIBMPU_H__

