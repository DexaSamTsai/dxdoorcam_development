#ifndef _MPU_SYS_H_
#define _MPU_SYS_H_

#define MPULOG_DISABLED	1
#define	MPULOG_CRIT		2
#define	MPULOG_ERR		3
#define	MPULOG_WARNING	4
#define	MPULOG_NOTICE	5
#define	MPULOG_INFO		6
#define	MPULOG_DEBUG	7

#endif
