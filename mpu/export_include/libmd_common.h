/** 
 * @file    R3_fw/libmpu_linux/include/libmd_common.h
 * @brief   headers for motion detection data structure
 *
 * This header contains all MD (Motion Detection) related data structure
 *
 * @date    2016/12/16
 */
#ifndef _LIBMD_COMMON_H_
#define _LIBMD_COMMON_H_

/**
 * @struct			s_md_win_cfg
 * @details			Motion detection window configuration
 * @ingroup			libmd
 */
typedef struct s_md_win_cfg
{
	u8 num;
	u8 enable;
	u16 xstart;
	u16 ystart;
	u16 width;
	u16 height;
	u16 threshold;
}t_md_win_cfg;

typedef struct s_md_algo_buf {
	u32 addr_ori;
	u32 addr_cur;
    u32 addr_prev0;
    u32 addr_prev1;
	u32 width;
	u32 height;
}t_md_algo_buf;


#endif	///___LIBMD_COMMON_H__
