/** 
 * @file    R3_fw/libmpu_linux/include/liba_common.h
 * @brief   headers for Audio data structure
 *
 * This header contains all Audio related data structure to interface with MPU.
 *
 *
 * @date    2016/12/15
 */
#ifndef _LIBA_COMMON_H_
#define _LIBA_COMMON_H_

#define I2SIRQ_MEMCOPY

#ifdef I2SIRQ_MEMCOPY
#define AI_PINGPONGBUF_SIZE 320
#else
#define AI_PINGPONGBUF_SIZE (320*4)
#endif

#define AREC_OUTBUF_ONELEN 1024
#define AREC_FRMLIST_CNT (10)

#define AI1REC_AI2PLAY

/**
 * @enum			enum_AFMT
 * @details			Audio decoder and encoder algorithms supported by OV798
 * @ingroup			libaudio
 */
typedef enum{
	AUDIO_FMT_UNKNOWN,
	AUDIO_FMT_F1,			///< Linear PCM
	AUDIO_FMT_F2,			///< ADPCM_SWF
	AUDIO_FMT_F3,			///< AF3
	AUDIO_FMT_F4,			///< AF4
	AUDIO_FMT_F5,			///< G711 ULAW/MLAW
	AUDIO_FMT_F6,			///< AMI_ADPCM
	AUDIO_FMT_F7,			///< QT_ADPCM
	AUDIO_FMT_F8,			///< G711 ALAW
	AUDIO_FMT_F9,			///< AMR NB
	AUDIO_FMT_F10,			///< Opus
}enum_AFMT;
typedef unsigned char E_AFMT;

/**
 * @enum			enum_AEPRE_ALGO
 * @details			Audio encoder algorithms supported by OV798
 * @ingroup			libaudio
 */
typedef enum{
	AUDIO_EPRE_UNKNOWN,		///< Unknown
	AUDIO_EPRE_DENOISE,		///< Audio denoise
	AUDIO_EPRE_AEC,			///< Audio Echo Cancellation 
}enum_AEPRE_ALGO;
typedef unsigned char E_AEPRE_ALGO;

/**
 * @enum            enum_ADPOST_ALGO
 * @details         Audio decoder algorithms supported by OV798
 * @ingroup         libaudio
 */
typedef enum{
	AUDIO_DPOST_UNKNOWN,	///< Unknown
	AUDIO_DPOST_AGC,		///< Audio Gain Control
	AUDIO_DPOST_EQ,			///< Equalization
}enum_ADPOST_ALGO;
typedef unsigned char E_ADPOST_ALGO;

/**
 * @enum			enum_AI_MODE
 * @details			enum_AI_MODE
 * @ingroup			libaudio
 */
typedef enum{
	AI_CDC_MASTER,			///< CODEC AI master,PLAY AI and REC AI slave 
	AI_REC_MASTER,			///< REC AI master,PLAY AI slave,CODEC AI slave
	AI_PLY_MASTER,			///< PLAY AI master,REC AI slave,CODEC AI slave
}enum_AI_MODE;
typedef unsigned char E_AI_MODE;

/**
 * @enum			enum_ACHANNEL
 * @details			Audio channel configuration
 * @ingroup			libaudio
 */
typedef enum{
	MONO=1,					///< single channel
	STEREO					///< two channels,stereo
}enum_ACHANNEL;
typedef unsigned char E_ACHANNEL;

/**
 * @enum			enum_FRMDROP_MODE
 * @details			Audio frame drop mode
 * @ingroup			libaudio
 */
typedef enum{
	DROPFRM_LATEST = 0,	 ///< drop latest ,reserve old data when just switched to this mode
	DROPFRM_LATEST_CLEAN,///< drop latest ,clear old data when just switched to this mode
	DROPFRM_OLDEST,		 ///< drop oldest ,reserve old data when just switched to this mode
	DROPFRM_OLDEST_CLEAN ///< drop oldest ,clear old data when just switched to this mode
}enum_DROPFRM_MODE;
typedef u32 E_DROPFRM_MODE;


#define T_EQ_MAX_VOLUME  1024
#define LIBEQ_MAX_BANDS  5
#define AF_NCH 2
#define KM LIBEQ_MAX_BANDS

#define FIXPOINT_A 28
#define FIXPOINT_B 28
#define FIXPOINT_G 28

/**
 * @struct			t_eq_settings
 * @details			equalization settings
 * @ingroup			libaudio	
 */
typedef struct 
{
	int bands;						///< expected EQ bands, 1 ~ LIBEQ_MAX_BANDS
	int bandfreqs[LIBEQ_MAX_BANDS];	///< the central frequency for each band
	int bandgains[LIBEQ_MAX_BANDS];	///< the expected gain for each band
} t_eq_settings;

/**
 * @struct			t_eq_params
 * @details			equalization parameters
 * @ingroup			libaudio
 */
typedef struct
{
	int pending_update;
	int max_volume;
	int valid_bands;
	int gain[AF_NCH][KM];
	int a_weights[KM][2];			///< A weights
	int b_weights[KM][2];			///< B weights
}t_eq_params;

/**
 * @struct          s_agc_ctrl
 * @details         AGC Controls
 * @ingroup         libaudio
 */
typedef struct s_agc_ctrl{
	u32 en;							///< enable agc
	s32 trgtdbfs;					///< target sample in dbfs
	s32 maxdbfs;					///< max output sample in dbfs
	s32 nfdbfs;						///< noise floor threshold indbfs
}t_agc_ctrl;

/**
 * @struct			s_adec_inpara
 * @details			Audio decoder parameters
 * @ingroup			libaudio
 */
typedef struct s_adec_inpara{
	E_AFMT fmt;						///< audio format to be decoded
	E_AI_MODE aimode;				///<
	u16 reserved0;
	u32 ch;							///< audio encoder channel config, MONO/STEREO
	u32 sr;							///< audio sampling rate. support 8K,11K,12K,16K,22K,24K,32K,44K,48K
	s32 dvol;						///< digital volume
	t_agc_ctrl agc;					///< agc control
	u32 algfrmsz;
	u32 compressor_en;				///< enable compressor
	u32 eq_en;						///< enable equalizer
	u32 aichannel;                ///< when playing MONO, which AI channel is selected to send data, 0:both(default);1:left;2:right
	u32 interpo_en;         		///< enable interpolation after decoding
	u32 interpo_times;              ///< interpolation times after decoding
	u32 en_mclk;
}t_adec_inpara;

/**
 * @struct			s_arec_inpara
 * @details			Audio recording parameters
 * @ingroup			libaudio
 */
typedef struct s_arec_inpara{
	E_AFMT fmt;						///< audio format to be encoded
	E_AI_MODE aimode;				///<
	u16 reserved0;
	u32 ch;							///< audio channels, MONO/STEREO
	u32 sr;							///< audio sampling rate. support 8K,11K,12K,16K,22K,24K,32K,44K,48K
	s32 dvol;						///< digital volume,in dB
	u32 denoise_en;					///< enable denoise
	u32 compressor_en;				///< enable compressor
	u32 aec_en;						///< enable aec
	t_agc_ctrl agc;					///< agc control
	u32 bDMIC;						///< recorder source select. AREC_PATH1: CODEC adc; AREC_PATH2: digital microphone
	s32 drop_samples;				///< samples to be dropped when starting recording, to skip some noise at begining caused by CODEC, for different CODEC and encoder algorithms, it should be configured different value
	u32 algfrmsz;
	u32 aichannel;                 ///< when recording MONO, which AI channel data selected, 0:left;non-zero:right
	u32 deci_en;             		///< enable decimation before encoding
	u32 deci_times;             	///< decimation times before encoding
	u32 en_mclk;
	u32 rmdc_en;                    ///<enable remove DC offset function
	E_DROPFRM_MODE dfmode;          ///< audio frame drop mode, drop latest or oldest
	u32 listfrm_dur;                 ///< time duration of frames kept in linklist
}t_arec_inpara;

#endif /// __LIBA_COMMON_H__
