#include "stdio.h"
#include <unistd.h>
#include <stdlib.h>

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef unsigned int LONG;

#pragma pack(push)
#pragma pack(1)
typedef struct tagBITMAPFILEHEADER
{
	WORD    bfType;
	DWORD   bfSize;
	WORD    bfReserved1;
	WORD    bfReserved2;
	DWORD   bfOffBits;
}BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER
{
	DWORD    biSize;
	LONG     biWidth;
	LONG     biHeight;
	WORD     biPlanes;
	WORD     biBitCount;
	DWORD    biCompression;
	DWORD    biSizeImage;
	LONG     biXPelsPerMeter;
	LONG     biYPelsPerMeter;
	DWORD    biClrUsed;
	DWORD    biClrImportant;
}BITMAPINFOHEADER;

typedef struct tagOLYHEADER
{
	BYTE     tag[4];
	DWORD    pal[8];
	WORD     width;
	WORD     height;
	DWORD    size;
}OLYHEADER;
#pragma pack(pop)

unsigned int rgb2yuv(BYTE r, BYTE g, BYTE b)
{
    int y,u,v;

    y = ((299*r) + (587*g) + (114*b))/1280;
    u = ((436*b) - (147*r) - (289*g))/1280 + 128;
    v = ((615*r) - (515*g) - (100*b))/1280 + 128;

    y = (y>255) ? 255 : ((y<0) ? 0 : y);
    u = (u>255) ? 255 : ((u<0) ? 0 : u);
	v = (v>255) ? 255 : ((v<0) ? 0 : v);
	return (((y&0xff)<<16) | ((u&0xff)<<8) | (v&0xff));
}

static int util_atoi(const char *c)
{
    int ret = 0;
    int base;
    int negative = 0;
    const char * s;

    if (c[0] == '0' && (c[1] == 'x' || c[1] == 'X'))
    {
        base = 16;
        s = c + 2;
    }
    else
    {
        base = 10;
        s = c;
        if (c[0] == '-')
        {
            negative = 1;
            s++;
        }
    }
    for (;  *s; s++)
    {
        if (*s >= '0' &&  *s <= '9')
        {
            ret *= base;
            ret += (*s - '0');
        }
        else if (base == 16 &&  *s >= 'a' &&  *s <= 'f')
        {
            ret *= base;
            ret += (*s - 'a' + 10);
        }
        else if (base == 16 &&  *s >= 'A' &&  *s <= 'F')
        {
            ret *= base;
            ret += (*s - 'A' + 10);
        }
        else
        {
            break;
        }
    }
    if (negative)
    {
        ret = 0-ret;
    }

    return ret;
}

main(int argc, char **argv)
{
	FILE * fpi;
	FILE * fpo;
	FILE * ftrans;
	FILE * ftxt;

	BITMAPFILEHEADER   m_bmfh; 
	BITMAPINFOHEADER   m_bmih;
	OLYHEADER          olyh;

	unsigned int cur_lut, prev_lut, pixel_cnt, out_pixel_cnt;
	int n, s, lut_cnt;
	unsigned char pico_buf[3];
	unsigned char trans[8];
	unsigned char *val, *c;


	if(argc < 4){
		printf("usage:%s input_file output_bin_file output_txt_file\n", argv[0]);
		return -1;
	}

	fpi = fopen(argv[1], "r");
	if(fpi == NULL){
		printf("cannot open input file:%s\n", argv[1]);
		return -1;
	}

	fpo = fopen(argv[2], "w");
	if(fpo == NULL){
		printf("cannot open output file:%s\n", argv[2]);
		return -1;
	}

	ftrans = fopen("trans.txt", "r");
	if(ftrans == NULL){
		printf("cannot open transparency file:trans.txt!\n");
		return -1;
	}
	ftxt = fopen(argv[3], "w");
	if(ftxt == NULL){
		printf("cannot open output txt file %s\n",argv[3]);
		return -1;
	}
#define SBUF_LEN    (1024)	
	unsigned char * sbuf = NULL;
	sbuf = (unsigned char *)malloc(SBUF_LEN);
	if(sbuf == NULL){
		printf("it is failed to malloc sbuf!\n");	
	}

	for(s=0; s<8; s++){
		trans[s] = 7;	
	}

	while(fgets(sbuf, SBUF_LEN, ftrans)){
		if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
		if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
		c=sbuf;
REDO:
		for(; *c==' ' || *c=='\t'|| *c=='{' || *c=='}'|| *c==','|| *c=='\n'|| *c=='\r'|| *c==';'; c++){
		}
		if(*c == 0){
			continue;
		}
		val = c;
		unsigned int tmp1 = util_atoi(val);
		c = strchr(val, ' ');
		for(val=c+1; val[0]==' '; val++);
		unsigned int tmp2 = util_atoi(val);
		for(s=0; s<8; s++){
			if(s == tmp1){
				if(tmp2 <= 7){
					trans[s] = tmp2;
				}else{
					printf("transparency's value should be 0--7!\n");
					return -1;
				}
			}
		}
	}

//	trans[0]=0;
	printf("Trans:\n");
	for(s=0; s<8; s++){
		printf("%d:%d\n", s, trans[s]);	
	}

	memset(&olyh, 0, sizeof(olyh));

	fread(&m_bmfh, 1, sizeof(m_bmfh), fpi);
	fread(&m_bmih, 1, sizeof(m_bmih), fpi);

	if(m_bmfh.bfType != 0x4d42){
		printf("it is not bmp file\n");
		exit(0);
	}
	
	if(m_bmih.biBitCount != 24){
		printf("ERR:%s only support 24bpp bmp file\n", argv[0]);
		return -1;
	}

	unsigned int image_size = m_bmfh.bfSize - m_bmfh.bfOffBits;
	unsigned char * pici_buf = malloc(image_size);
	if(pici_buf == NULL){
		printf("It is failed to allocate picture buffer!\n");
		return -1;
	}
	unsigned int line_size = m_bmih.biWidth * (m_bmih.biBitCount >> 3);
	unsigned char * linebuf = malloc(line_size);
	if(linebuf == NULL){
		printf("It is failed to allocate line buffer!\n");
		return -1;
	}
	int skip = line_size;
	if(skip%4){
		skip = ((skip+3)&(~3));
	}

	///< read bmp data from the latest line to the first line.
	s = 0;
	while(s<m_bmih.biHeight){
		fseek(fpi, m_bmfh.bfOffBits+(m_bmih.biHeight-1-s)*skip, SEEK_SET);
		fread(linebuf, 1, line_size, fpi);
		memcpy(&pici_buf[line_size*s], linebuf, line_size);
		s++;
	}
	free(linebuf);

	fseek(fpo, sizeof(OLYHEADER), SEEK_SET);  ///< header and lut

	lut_cnt = 0;
	prev_lut = 0;
	pixel_cnt = 0;
	out_pixel_cnt = 0;
	for(n=0; n<image_size; n+=3){
		cur_lut = rgb2yuv(pici_buf[n], pici_buf[n+1], pici_buf[n+2]);	
		if(prev_lut != cur_lut){
			for(s=0; s<8; s++){
				if(olyh.pal[s] == prev_lut || olyh.pal[s] == 0){
					break;
				}
			}	
			if(s<8 && pixel_cnt > 0){
				lut_cnt = s;
			    while(pixel_cnt>0) {
					//printf("pixel_cnt %d\n",pixel_cnt);
					if(pixel_cnt >= 32768){
					   //save 32767 frist
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = ((1<<7)|(32767>>8));
						pico_buf[2] = (32767&0xff); 
						pixel_cnt -= 32767;
						fwrite(pico_buf, 1, 3, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,0x%02x,",pico_buf[0],pico_buf[1],pico_buf[2]);
						out_pixel_cnt += 32767;
					}else if(pixel_cnt >= 127 && pixel_cnt < 32768){
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = ((1<<7)|(pixel_cnt>>8));
						pico_buf[2] = (pixel_cnt&0xff);
						out_pixel_cnt += pixel_cnt;
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 3, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,0x%02x,",pico_buf[0],pico_buf[1],pico_buf[2]);						
				    }else if(pixel_cnt > 1 && pixel_cnt < 128){
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = (pixel_cnt);
						out_pixel_cnt += pixel_cnt;
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 2, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,",pico_buf[0],pico_buf[1]);						
				    }else{
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|0);
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 1, fpo);
						fprintf(ftxt,"0x%02x,",pico_buf[0]);
						out_pixel_cnt += 1;
					}
				}

				if(!olyh.pal[lut_cnt]){
					olyh.pal[lut_cnt] = prev_lut;
				}
				
			}
			if(s==8){
				printf("ERR: color kind is more than 8!\n");
				return -1;
			}
		
			prev_lut = cur_lut;
			pixel_cnt ++;
		}else{
			pixel_cnt ++;
		}	
	}
	//printf("pixel_cnt %d\n",pixel_cnt);
	//output last pixel(s)
	for(s=0; s<8; s++){
		if(olyh.pal[s] == prev_lut || olyh.pal[s] == 0){
			break;
		}
	}	

	if(s<8 && pixel_cnt > 0){
				lut_cnt = s;
			    while(pixel_cnt>0) {
					//printf("pixel_cnt %d\n",pixel_cnt);
					if(pixel_cnt >= 32768){
					   //save 32767 frist
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = ((1<<7)|(32767>>8));
						pico_buf[2] = (32767&0xff); 
						pixel_cnt -= 32767;
						fwrite(pico_buf, 1, 3, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,0x%02x,",pico_buf[0],pico_buf[1],pico_buf[2]);
						out_pixel_cnt += 32767;
					}else if(pixel_cnt >= 127 && pixel_cnt < 32768){
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = ((1<<7)|(pixel_cnt>>8));
						pico_buf[2] = (pixel_cnt&0xff);
						out_pixel_cnt += pixel_cnt;
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 3, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,0x%02x,",pico_buf[0],pico_buf[1],pico_buf[2]);						
				    }else if(pixel_cnt > 1 && pixel_cnt < 128){
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|1);
						pico_buf[1] = (pixel_cnt);
						out_pixel_cnt += pixel_cnt;
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 2, fpo);
						fprintf(ftxt,"0x%02x,0x%02x,",pico_buf[0],pico_buf[1]);						
				    }else{
						pico_buf[0] = ((lut_cnt<<4)|(trans[lut_cnt]<<1)|0);
						pixel_cnt = 0;
						fwrite(pico_buf, 1, 1, fpo);
						fprintf(ftxt,"0x%02x,",pico_buf[0]);
						out_pixel_cnt += 1;
					}
				}
	}

	strcpy(&olyh.tag, "OLY8");
	olyh.width = m_bmih.biWidth;
	olyh.height = m_bmih.biHeight;
	olyh.size = ftell(fpo)-sizeof(OLYHEADER);
	fseek(fpo, 0, SEEK_SET);  ///< header and lut
	fwrite(&olyh, 1, sizeof(olyh), fpo);
	fprintf(ftxt,"width %d height %d size %d\n",olyh.width, olyh.height, olyh.size);
	if(out_pixel_cnt != olyh.width*olyh.height){
	    printf("Error: expected out_pixel_cnt %d rather than %d\n",olyh.width*olyh.height, out_pixel_cnt);
	}
	fclose(fpi);
	fclose(fpo);
	fclose(ftxt);
	free(pici_buf);

	return 0;
}
