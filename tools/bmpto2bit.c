#include "stdio.h"

//#define DEBUG_EN
main(int argc, char **argv)
{
	if(argc < 3){
		printf("%s 1.bmp 1.res", argv[0]);
		exit(0);
	}

	FILE *fp;
	fp = fopen(argv[1], "r");
	if(fp == NULL){
		printf("cannot find %s\n", argv[1]);
		exit(0);
	}
	
	FILE *fpo;
	fpo = fopen(argv[2], "w");
	if(fpo == NULL){
		printf("cannot find %s\n", argv[2]);
		exit(0);
	}

	unsigned char buf[100+1280*720*3];
	fread(buf, 1, 100, fp);

	if(buf[0] != 'B' || buf[1] != 'M'){
		printf("not bmp\n");
		exit(0);
	}
	
	///< support <=2BPP or 24BPP, but only <= 4color
	if(!(buf[0x1c]<=0x4||buf[0x1c]==0x18)){
		printf("not 2BPP or 24BPP\n");
		exit(0);
	}
	
	int w = buf[0x12] | (buf[0x13] << 8);
	int h = buf[0x16] | (buf[0x17] << 8);

	if((w&3)){
		printf("width isn't 4pixel aligned\n");
		exit(0);
	}
	
#ifdef DEBUG_EN
	printf("0x%x, 0x%x, \n", buf[0x13], buf[0x12]);
	printf("0x%x, 0x%x, \n", buf[0x17], buf[0x16]);
#endif
	fwrite(&buf[0x12], 1, 2, fpo);
	fwrite(&buf[0x16], 1, 2, fpo);

	int offset = buf[0xa];

	unsigned char *c = buf + offset;
	int i,j,k,s;
	unsigned char val=0;
	unsigned char shift[4] = {0, 2, 4, 6};
	unsigned char bpp=0;
	unsigned int co=0;
	unsigned char linebuf[1280*3];
	unsigned int linelen;

	if(buf[0x1c] == 0x18){
		///< 24bit-->2bit.
		unsigned int color[4]={0, 0, 0, 0};
		unsigned char ccnt=0;
		s=0;
		bpp=(0x18>>3);
		linelen=bpp*w;

		while(i<h){
			fseek(fp, offset+(h-1-i)*linelen, SEEK_SET);
			fread(linebuf, 1, linelen, fp);
			memcpy(&c[linelen*i], linebuf, linelen);
			i++;
		}	
		
		for(i=0; i<h; i++){
			for(j=0; j<w; j++){
				co=(c[i*linelen+j*bpp]+(c[i*linelen+j*bpp+1]<<8)+(c[i*linelen+j*bpp+2]<<16));
				if(co==0) co++;
				for(k=0; k<4; k++){
					if(co == color[k]){
						val|=(k<<shift[s++]); 
						if(s==4){
#ifdef DEBUG_EN
							printf("0x%x", val);
							printf(",");
#endif
							fwrite(&val, 1, 1, fpo);
							val=0;
							s=0;
						}
						break;	
					}
				}	
				if(k==4){
					if(ccnt<4){ 
						color[ccnt]=co; ccnt++;
						val|=(k<<shift[s++]);
						if(s==4){
#ifdef DEBUG_EN
							printf("0x%x", val);
							printf(",");
#endif
							fwrite(&val, 1, 1, fpo);
							val=0;
							s=0;
						}
					}else{
						printf("color of this bmp exceeds 4 colors!\n");
						exit(0);
					}
				}
			}
		}
	}

	if(buf[0x1c] == 0x4){
		///< 2BPP-->2bit
		bpp=1; ///< 1/2
		s=0;
		val=0;
		int tmp = (w+8)&(~7);
		linelen = (tmp>>bpp);
		while(i<h){
			fseek(fp, offset+(h-1-i)*linelen, SEEK_SET);
			fread(linebuf, 1, linelen, fp);
			memcpy(&c[linelen*i], linebuf, linelen);
			i++;
		}
		
		for(i=0; i<h; i++){
			for(j=0; j<(w>>bpp); j++){
				val|=(((c[i*(tmp>>bpp)+j]>>4)&0x3)<<shift[s++]);
				val|=((c[i*(tmp>>bpp)+j]&0x3)<<shift[s++]);
				if(s==4){
#ifdef DEBUG_EN
					printf("0x%x", val);
					printf(",");
#endif
					fwrite(&val, 1, 1, fpo);
					val=0;
					s=0;
				}
			}
		}
	}
	
	fclose(fp);
	fclose(fpo);

}
