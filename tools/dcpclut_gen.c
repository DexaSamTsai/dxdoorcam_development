#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>

#define DATA_LEN  0x100000

static int util_atoi(const char *c)
{
    int ret = 0;
    int base;
    int negative = 0;
    const char * s;

    if (c[0] == '0' && (c[1] == 'x' || c[1] == 'X'))
    {
        base = 16;
        s = c + 2;
    }
    else
    {
        base = 10;
        s = c;
        if (c[0] == '-')
        {
            negative = 1;
            s++;
        }
    }
    for (;  *s; s++)
    {
        if (*s >= '0' &&  *s <= '9')
        {
            ret *= base;
            ret += (*s - '0');
        }
        else if (base == 16 &&  *s >= 'a' &&  *s <= 'f')
        {
            ret *= base;
            ret += (*s - 'a' + 10);
        }
        else if (base == 16 &&  *s >= 'A' &&  *s <= 'F')
        {
            ret *= base;
            ret += (*s - 'A' + 10);
        }
        else
        {
            break;
        }
    }
    if (negative)
    {
        ret = 0-ret;
    }

    return ret;
}
int main(int argc, char **argv)
{

	FILE * in = NULL;
	FILE * out = NULL;

	if(argc < 3){
		printf("Usage: yuv_gen.exe input_file output_file\n");
		return -1;
	}
	
	if((in = fopen(argv[1], "r")) == NULL){
		printf("can't open %s\n", argv[1]);
	}
	if((out = fopen(argv[2], "wb")) == NULL){
		printf("can't open %s\n", argv[2]);
	}
	unsigned char *data = NULL;
	unsigned char *sbuf = NULL;
	data = (unsigned char *)malloc(DATA_LEN);
	memset(data, 0, DATA_LEN);
	sbuf = (unsigned char *)malloc(10240);
	memset(sbuf, 0, 10240);

	int n = 0;
	
	unsigned char * c;
	unsigned char * value;

	while(fgets(sbuf, 10240, in)){
		if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
		if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
		c=sbuf;
REDO:
		for(; *c==' ' || *c=='\t'|| *c=='{' || *c=='}'|| *c==','|| *c=='\n'|| *c=='\r'|| *c==';'; c++){
		}
		if(*c == 0){
			continue;
		}
		value = c;
		unsigned int lut_data = util_atoi(value);
		printf("lut1 = %d\n", lut_data);
		data[n++] = (lut_data >> 8)&0xff;
		data[n++] = (lut_data&0xff);
		c = strchr(value, ',');
		for(value=c+1; value[0]==' '; value ++);
		lut_data = util_atoi(value);
		printf("lut2 = %d\n", lut_data);
		data[n++] = (lut_data >> 8)&0xff;
		data[n++] = (lut_data&0xff);
		c = strchr(value, '}');
		goto REDO;
	}

	fwrite(data, 1, n, out);

	free(data);
	free(sbuf);
	fclose(in);
	fclose(out);
}
