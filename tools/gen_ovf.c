#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define u32 unsigned int
#define u8 unsigned char

typedef struct s_ovf_file_header{
	u32 magic; //0x4f565959
	u32 format; //0x1: same YUV format from our FB. 0x2: same YUV format from our MD HW.
	u32 width;
	u32 height;
	u32 frame_size; //bytes of each video frame, 0 for dynamic frame size
	u32 file_size; //total file size, include the 32 bytes header.
	u32 reserved[2]; //make header 32bytes.
}t_ovf_file_header;
 
typedef struct s_ovf_frame_header
{
	u32 frame_size; //if ovf_file->frame_size is not zero, this is ignored.
	u32 reserved[7]; //make frame header 32 bytes
}t_ovf_frame_header;

void test_ovf(char * file_name){
	FILE *fp = fopen(file_name, "r");
	u32 buf_data;
	int i;
	printf("**** ovf file header ********\n");
	for(i=0; i<8;i++){
		fread(&buf_data, 4, 1, fp);
		printf("0x%x\n",buf_data);
	}
	printf("**** first frame header ********\n");
	for(i=0; i<8; i++){
		fread(&buf_data, 4, 1, fp);
		printf("0x%x\n",buf_data);
	}
	fclose(fp);
}

void help(char * argv0)
{
	printf("usage: %s inputfile format width height\n", argv0);
	printf("  format: 1:YUV420\n");
	exit(-1);
}

int main(int argc, char* argv[]){
	int i;

	if(argc != 5){
		help(argv[0]);
		return -1;
	}
	int format = atoi(argv[2]);
	if(format != 1){
		help(argv[0]);
		return -1;
	}
	int width = atoi(argv[3]);
	int height = atoi(argv[4]);
	if(width <=0 || height <= 0){
		help(argv[0]);
		return -1;
	}

	char input_fname[FILENAME_MAX];
	char output_fname[FILENAME_MAX];
	//it's already yuv file ?
	strcpy(output_fname, argv[1]);
	char * postfix = strrchr(output_fname, '.');
	if(postfix && !strcasecmp(postfix, ".yuv")){
	}else{
		if(postfix){
			strcpy(postfix, ".yuv");
		}else{
			strcat(output_fname, ".yuv");
		}
		printf("input file %s not .yuv format, converting to %s\n", argv[1], output_fname);

		char cmd[FILENAME_MAX*3];
		sprintf(cmd, "ffmpeg -y -i %s -s %dx%d -an %s", argv[1], width, height, output_fname);
		system(cmd);
	}
	strcpy(input_fname, output_fname);

	//open yuv file
	FILE * fp_sr = fopen(input_fname, "r");
	if(fp_sr == NULL){
		printf("file %s open failed !\n", input_fname);
		return -1;
	}
	//open ovf file
	postfix = strrchr(output_fname, '.');
	if(postfix){
		strcpy(postfix, ".ovf");
	}else{
		strcat(output_fname, ".ovf");
	}
	FILE * fp_ds = fopen(output_fname, "w");	
	if(fp_ds == NULL){
		printf("file %s open failed !\n", output_fname);
		return -1;
	}

	u32 frm_size = 0;
	if(format == 1){ //YUV420
		frm_size = width * height * 3 / 2;
	}
	u8 * buf_data = malloc(frm_size);
	if(buf_data == NULL){
		printf("malloc %d failed\n", frm_size);
		return -1;
	}

	fseek(fp_sr, 0, SEEK_END);
	u32 file_size = ftell(fp_sr); 
	fseek(fp_sr, 0, SEEK_SET);
	u32 frm_cnt = file_size/frm_size;

	printf("file %s size:%d, resolution: %d*%d, format:%d, framesize:%d, frm_cnt:%d\n", input_fname, file_size, width, height, format, frm_size, frm_cnt);

	/* write file header */
	t_ovf_file_header ovf_file_header =
	{
		.magic = 0x4f565959,
		.format = format,
		.width = width,
		.height = height,
		.frame_size = frm_size,
		.file_size = sizeof(struct s_ovf_file_header) + frm_cnt*(sizeof(t_ovf_frame_header) + frm_size),
	};
	fwrite(&ovf_file_header, 1, sizeof(struct s_ovf_file_header), fp_ds);

	/* write frame data */
	t_ovf_frame_header ovf_frame_header =
	{
		.frame_size = frm_size,
	};
	for(i=0; i <frm_cnt; i++){
		fwrite(&ovf_frame_header, 1, sizeof(struct s_ovf_file_header), fp_ds);
		//write data to file
		fread(buf_data, 1, frm_size, fp_sr);
		fwrite(buf_data,1, frm_size, fp_ds);
	}
	
	fclose(fp_sr);
	fclose(fp_ds);

	printf("test %s file !\n", output_fname);

	test_ovf(output_fname);

	return 0;
}
