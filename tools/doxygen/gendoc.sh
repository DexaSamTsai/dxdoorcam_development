#!/bin/bash

if [ ! -z $1 ] ; then
	CHIP=$1
else
	CHIP=OV798
fi
CHIP_NOUNDERSCORE=`echo $CHIP | sed -e "s/_/ /g"`

if [ ! -e ${CHIP}_files.txt ] ; then
	echo "${CHIP}_files.txt not exist"
	exit -1;
fi

outdir=$CHIP
if [ "$2" == "zh" ] ; then
	outdir=${CHIP}_zh
fi
mkdir -p $outdir/tmp
mkdir -p $outdir/doc

gcc -o conv_h conv_h.c || exit 1

INPUT=`cat ${CHIP}_files.txt`

cp doxy.cfg $outdir/doxy.cfg
echo -n "INPUT = " >> $outdir/doxy.cfg
for file in $INPUT
do
	newfile=`echo $file | sed -e "s/^\.\.\/\.\.\///g" | sed -e "s/\//_/g"`
	rm -f $outdir/tmp/$newfile
	if [ "$2" == "zh" ] ; then
		./conv_h 1 < $file > $outdir/tmp/$newfile || exit 1
	else
		./conv_h < $file > $outdir/tmp/$newfile || exit 1
	fi
	echo -n "$outdir/tmp/$newfile " >> $outdir/doxy.cfg
done
echo >> $outdir/doxy.cfg

sed -e "s/CHIP/${CHIP_NOUNDERSCORE}/g" -i $outdir/doxy.cfg
sed -e "s/OUTDIR/$outdir/g" -i $outdir/doxy.cfg
if [ "$2" == "zh" ] ; then
	sed -e "s/^OUTPUT_LANGUAGE.*$/OUTPUT_LANGUAGE=Chinese/g" -i $outdir/doxy.cfg
fi
doxygen $outdir/doxy.cfg

#### update structXXX file
for file in $outdir/latex/struct* ; do
	sed -e "s/section/subsection/g" -i $file
	awk '{print}/Definition at line/{exit}' $file > $file.tmp
	mv $file.tmp $file
done

#####generate pdf#####
sed -e "s/^\tpdflatex/\t-pdflatex/g" $outdir/latex/Makefile > $outdir/latex/Makefile.tmp
sed -e "s/^\tmakeindex/\t-makeindex/g" $outdir/latex/Makefile.tmp > $outdir/latex/Makefile
sed -e "s/^\tlatex_count=5/\t-latex_count=5/g" $outdir/latex/Makefile > $outdir/latex/Makefile.tmp
sed -e "s/pdflatex/pdflatex --interaction batchmode/g" $outdir/latex/Makefile.tmp > $outdir/latex/Makefile
rm -f $outdir/latex/Makefile.tmp

mv $outdir/latex/refman.tex $outdir/latex/old.tex
cp module_all_tex.cfg $outdir/latex/refman.tex
awk '{if(A) print}/Begin generated contents/{A=1}' $outdir/latex/old.tex >> $outdir/latex/refman.tex
sed -e "s/CHIP/${CHIP_NOUNDERSCORE}/g" -i $outdir/latex/refman.tex
sed -r 's/^\\chapter\{.*\}$//g' -i $outdir/latex/refman.tex
sed -r 's/^\\end\{document\}$/\\end\{CJK\} \\end\{document\}/g' -i $outdir/latex/refman.tex

make -C $outdir/latex/

mv $outdir/latex/refman.pdf $outdir/doc/${CHIP}_SDKAPI.pdf

#### generate modules ####

mkdir -p $outdir/doc/modules

#Please run chk_ov_sty.sh if ov.sty is updated. To check the conformance of ov.sty and update module_tex_ovsty.cfg.
#./chk_ov_sty.sh

### modules/XXX.pdf
if [ -e ${CHIP}_modules.txt ] ; then
while read moduledirall libs ; do 
	moduledir=`echo $moduledirall | sed -e "s/(.*$//g"`
	label=`expr "$moduledirall" : '.*(\(.*\)).*'`
	module=`echo $moduledir | sed -e "s/^.*\///g"`
	if [ -z $label ] ; then
		label=$module
	fi
	if [ "$label" == "" ] ; then
		label=$module
	fi
	cp module_tex.cfg $outdir/latex/module_$module.tex
	for lib in `echo $libs` ; do
		fname=group_${lib}
		realfname=`echo $fname | sed -e "s/_/__/g"`
		realfname=`echo $realfname | sed -e "s/[A-Z]/_&/g" | tr 'A-Z' 'a-z'`
		if [ -e $outdir/latex/$realfname.tex ] ; then
			echo \\include{$realfname} >> $outdir/latex/module_$module.tex
		fi

#		fname=structs_${lib}
#		realfname=`echo $fname | sed -e "s/_/__/g"`
#		realfname=`echo $realfname | sed -e "s/[A-Z]/_&/g" | tr 'A-Z' 'a-z'`
#		if [ -e $outdir/latex/$realfname.tex ] ; then
#			echo \\include{$realfname} >> $outdir/latex/module_$module.tex
#		fi
	done
	cat module_tex_end.cfg >> $outdir/latex/module_$module.tex
	realmodule=`echo $module | sed -e "s/_/\\\\\\\\\\\\\\_/g"`
	sed -e "s/MODULENAME/$realmodule/g" -i $outdir/latex/module_$module.tex

	cd $outdir/latex
#	pdflatex module_$module.tex
	pdflatex --interaction batchmode module_$module.tex
	makeindex module_$module.idx
	pdflatex --interaction batchmode module_$module.tex
	latex_count=5
	while egrep -s 'Rerun (LaTeX|to get cross-references right)' module_$module.log && [ $latex_count -gt 0 ] ; do
		echo "Rerunning latex...."
		pdflatex --interaction batchmode module_$module.tex
		latex_count=`expr $latex_count - 1`
	done
	cd ../../

	mv $outdir/latex/module_$module.pdf $outdir/doc/modules/$label.pdf
done < ${CHIP}_modules.txt ;
fi

#### Document/overall/Abbrev.pdf
./gen_abbrev.sh ${CHIP} $2
