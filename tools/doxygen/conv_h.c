#include "stdio.h"
#include <stdlib.h>
#include "string.h"

static char * find_next_lang(char * buf)
{
	char * c = strstr(buf, "%en");
	if(c){
		return c;
	}
	c = strstr(buf, "%zh");
	return c;
}

int main(int argc, char **argv)
{
#define BUFSIZE 2048
	char buf[BUFSIZE];
	char *c;
	char def[BUFSIZE];
	def[0] = 0;
	int langindex=0;

	if(argc > 1){
		langindex = atoi(argv[1]);
	}

	int langskip=0;
	int inter=0;

	while(fgets(buf, BUFSIZE, stdin)){
		if(strstr(buf, "INTERNAL_START")){
			inter = 1;
			if(def[0]){
				printf("%s;\n", def);
				def[0] = 0;
			}
		}
		if(strstr(buf, "INTERNAL_END")){
			inter = 0;
			continue;
		}
		if(inter) continue;
		if(!strncmp(buf, "%en_start", 9)){
			if(langindex != 0){
				langskip=1;
			}else{
				langskip=0;
			}
			continue;
		}
		if(!strncmp(buf, "%zh_start", 9)){
			if(langindex == 0){
				langskip=1;
			}else{
				langskip=0;
			}
			continue;
		}
		if(!strncmp(buf, "%zh_end", 7) || !strncmp(buf, "%en_end", 7)){
			langskip=0;
			continue;
		}
		if(langskip){
			continue;
		}
		char * c_lang = buf;
		while(c_lang){
			c_lang = find_next_lang(c_lang);
			if(c_lang == NULL){
				break;
			}
			if(!strncmp(c_lang, "%en", 3)){
				if(langindex != 0){
					char * c_lang_end;
					c_lang_end = find_next_lang(c_lang + 3);
					//skip
					if(c_lang_end){
						memmove(c_lang, c_lang_end, strlen(c_lang_end)+1);
					}else{
						strcpy(c_lang, "\n");
					}
				}else{
					memmove(c_lang, c_lang + 3, strlen(c_lang)+1);
				}
			}
			if(!strncmp(c_lang, "%zh", 3)){
				if(langindex == 0){
					char * c_lang_end;
					c_lang_end = find_next_lang(c_lang + 3);
					//skip
					if(c_lang_end){
						memmove(c_lang, c_lang_end, strlen(c_lang_end)+1);
					}else{
						strcpy(c_lang, "\n");
					}
				}else{
					memmove(c_lang, c_lang + 3, strlen(c_lang)+1);
				}
			}
		}


		char * c_real_f = strstr(buf, "REAL_FORMAT");
		if(c_real_f){
			strcpy(def, c_real_f+12);
			continue;
		}
		if(strncmp(buf, "#define", 7) == 0 && def[0] ){
			printf("%s;\n", def);
			def[0] = 0;
			continue;
		}
		c = strstr(buf, "* -#");
		if(c){
			*c = ' ';
		}
		printf("%s", buf);
	}

	return 0;
}
