#!/bin/bash

help()
{
	echo "$0 [ OV780 | OV788 ]"
	exit
}

outdir=$1
if [ "$2" == "zh" ] ; then
	outdir=${1}_zh
fi

cat > Abbrev.tex << EOF
\documentclass{article}
\usepackage{ov}
\usepackage{tikz}
\usetikzlibrary{trees}

\title{$1 Abbreviations}
\begin{document}
\maketitle
\tableofcontents
  \section{Overview}
This document describes the abbreviations used through all the documentation in this design kit. The abbreviations are listed in alphabetical order.
  \section{Abbreviations}
EOF

while read abbr detail ; do
	if [ "$abbr" == "" ] ; then
		continue;
	fi
	if [ ${abbr:0:1} == '#' ] ; then
		continue;
	fi
	echo "\subsection{$abbr}" >> Abbrev.tex
	echo "$detail" >> Abbrev.tex
done < abbrev.txt

cat >> Abbrev.tex << EOF
\end{document}
EOF

pdflatex --interaction batchmode Abbrev.tex
pdflatex --interaction batchmode Abbrev.tex

mv Abbrev.pdf $outdir/doc/
