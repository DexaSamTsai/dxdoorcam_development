#include "mcu_ctrl.h"

/* MCU Command */
#define CMD_POWER_OFF	0x01
#undef CMD_RESET
#define CMD_RESET 		0x02
#define CMD_GET_EVENT 	0x03
#define CMD_GET_MODE 	0x04
#define CMD_GET_PIR 	0x05
#define CMD_GET_LUM 	0x06
#define CMD_GET_BAT 	0x07
#define CMD_SET_MODE 	0x80 /* BIT0~3: MODE_XXX */
#define CMD_SET_PIRST 	0xa0 /* BIT0~3: pir sensitivity. 0: most sensitive. 0xe: most insensitive. 0xf: disable PIR */
#define CMD_SET_NIGHT 	0xb0 /* BIT0~3: 0:force day, 1:force night, 2~f:threshold, 2:more day, f:more night */
#define CMD_SET_LED 	0x40 /* BIT0~3: LED_STATUS_XXX, BIT4~5: LED_COLOR_XXX */
#define CMD_SHORT_SLEEP	0x30
#define CMD_SET_BOUND 	0x60
#define CMD_SET_BOARD_ID 0x70 /* BIT0~1: board id. 00 rd19, 01 sc20, 10 rd15, 11 rd18 */

#define DATA_READY_TIMEOUT 0x10000

static t_libsif_cfg _mcu_cfg ;
static int _mcu_inited = 0;
pthread_mutex_t _mcu_mutex = PTHREAD_MUTEX_INITIALIZER; 

void mcu_init(void)
{
	if(_mcu_inited == 1){
		return;
	}

	//Init SPI
	memset(&_mcu_cfg, 0, sizeof(_mcu_cfg));
	SIF0_USE_P1;
	_mcu_cfg.cpha = 1;
	libsif0_init(&_mcu_cfg, 0, 1, 1000000);

	// Init GPIO
//	libgpio_config(PIN_UPDATE_INT,PIN_DIR_OUTPUT);   
//	libgpio_write(PIN_UPDATE_INT, PIN_LVL_LOW);

	libgpio_config(PIN_BSL_RST,PIN_DIR_OUTPUT);  	 
	libgpio_config(PIN_BSL_TEST,PIN_DIR_OUTPUT);
	libgpio_write(PIN_BSL_RST,0);
	libgpio_write(PIN_BSL_TEST,0);

	libgpio_config(PIN_MCU_EVENT, PIN_DIR_INPUT);    
	libgpio_config(PIN_MCU_DATA_READY, PIN_DIR_INPUT);

	_mcu_inited = 1;
	syslog(LOG_INFO,"Init MCU interface.\n");
}

static int _mcu_cmd(u8 cmd)
{
	if(_mcu_inited == 0){
		mcu_init();
		if(_mcu_inited == 0)
		{
			syslog(LOG_ERR, "[ERROR]: MCU interface is not inited.\n");
			return -1;
		}
	}

	int ret = 0;
	int timeout = 0;

	//wait MCU for it to be ready for reading cmd
	while((ret = libgpio_read(PIN_MCU_DATA_READY)) != PIN_LVL_LOW && timeout++ < DATA_READY_TIMEOUT);

	if( ret != PIN_LVL_LOW){
		syslog(LOG_ERR, "[ERROR]: Wait PIN_MCU_DATA_READY for PIN_LVL_LOW timeout\n");
		return -1;
	}

	libsif0_cs_low();
	if(libsif_write(&_mcu_cfg,&cmd,1) < 0)
	{	
		libsif0_cs_high();
		syslog(LOG_ERR, "[ERROR]: mcu_cmd(0x%x) libsif_write() failed\n", cmd);
		return -1;
	}
	libsif0_cs_high();

	// if no need mcu's ack
	if(cmd != CMD_GET_EVENT 
			&& cmd != CMD_GET_MODE
			&& cmd != CMD_GET_PIR
			&& cmd != CMD_GET_LUM
			&& cmd != CMD_GET_BAT){
		//if don't need read, then don't check data_ready
		//but add delay to avoid command overwritten.
		volatile int delay; 
		for(delay=0; delay<10000;delay++);

		return 0;
	}

	timeout = 0;
	//wait MCU for it to prepare the ack
	while((ret = libgpio_read(PIN_MCU_DATA_READY)) != PIN_LVL_HIGH && timeout++ < DATA_READY_TIMEOUT);

	if( ret != PIN_LVL_HIGH){
		syslog(LOG_ERR, "[ERROR]: Wait PIN_MCU_DATA_READY for PIN_LVL_HIGH timeout\n");
		return -1;
	}

	// read mcu's ack
	u8 ack = 0;
	libsif0_cs_low();
	if(libsif_read(&_mcu_cfg,&ack,1) < 0)
	{
		libsif0_cs_high();
		syslog(LOG_ERR, "[ERROR]: mcu_cmd(0x%x) libsif_read() failed\n", cmd);
		return -1;//while(1);
	}
	libsif0_cs_high();

	return ack;
}

int mcu_cmd(u8 cmd)
{
	pthread_mutex_lock(&_mcu_mutex);
	int ret = _mcu_cmd(cmd);
	pthread_mutex_unlock(&_mcu_mutex);

	return ret;
}

void mcu_set_mode(t_sys_mode mode)
{
	syslog(LOG_INFO, "mcu_set_mode(%d)\n", mode);
	mcu_cmd(CMD_SET_MODE |(mode & 0x0f));
}

t_sys_mode mcu_get_mode(void)
{
	return mcu_cmd(CMD_GET_MODE);
}

void mcu_set_led(t_led_color color, t_led_status status)
{
	syslog(LOG_INFO, "mcu_set_led(%d, %d)\n", color, status);
	mcu_cmd(CMD_SET_LED | ((color & 0x03) << 2) | (status & 0x3));
}

void mcu_set_led_nortos(t_led_color color, t_led_status status)
{
	_mcu_cmd(CMD_SET_LED | ((color & 0x03) << 2) | (status & 0x3));
}

void mcu_set_pirsensitive(u8 st)
{
	syslog(LOG_INFO, "mcu_set_pirsensitive(%d)\n", st);
	mcu_cmd(CMD_SET_PIRST |(st & 0x0f));
}

int mcu_get_pir(void)
{
	int ret;
	ret = mcu_cmd(CMD_GET_PIR);
	syslog(LOG_INFO, "mcu_get_pirsensitive(%d)\n",ret);
	return ret;
}

void mcu_set_nightmode(u8 mode)
{
	syslog(LOG_INFO, "mcu_set_nightmode(%d)\n", mode);
	mcu_cmd(CMD_SET_NIGHT |(mode & 0x0f));
}

static const u32 sleep_time[16] = 
{
	1, 2, 5, 10,
	20, 30, 45, 60,
	90, 120, 180, 300,
	600, 900, 1200, 1800
};
void mcu_short_sleep(int time)
{
	int i; 
	for(i=0; i<16; i++){
		if(time <= sleep_time[i]){
			break;
		}
	}
	if(i >= 16){
		i = 15;
	}

	syslog(LOG_INFO, "mcu_short_sleep(%d)\n", sleep_time[i]);
	u8 cmd =0;
	cmd = CMD_SHORT_SLEEP | (i & 0x0f);
	mcu_cmd(cmd);
}

int mcu_get_vbat(void)
{
	int mcu_evt = mcu_cmd(CMD_GET_BAT);
	if(mcu_evt < 0){
		return 0;
	}else if(mcu_evt == 0){
		//wait 20ms and check again.
		{volatile int d; for(d=0; d<1000000; d++);}
		mcu_evt = mcu_cmd(CMD_GET_BAT);
	}

#if 0 //TODO
	//FIXME: how about V3???
/* 3.0V: 59
   5.0V: 99
   linear
 */
	int volt = (5 + (mcu_evt * 5));


	int v;
	if(volt <= 350){
		v = 0;
	}else if(volt >= 450){
		v =  100;
	}else{
		v = (volt - 350);
	}

	if(libgpio_read(PIN_GPIO_0)){
		v += 256;
	}

	return v;
#endif
	return 0;
}

int mcu_event_ready(void)
{
	return libgpio_read(PIN_MCU_EVENT);
}

t_mcu_evt mcu_get_event(void)
{
	return mcu_cmd(CMD_GET_EVENT);
}

t_mcu_evt mcu_get_event_nortos(void)
{
	return _mcu_cmd(CMD_GET_EVENT);
}

void mcu_power_off(void)
{
	debug_printf("mcu_power_off\n");
	mcu_cmd(CMD_POWER_OFF);
}

void mcu_set_boundary(u8 mode)
{
	syslog(LOG_INFO, "mcu_set_bound(%d)\n", mode);
	mcu_cmd(CMD_SET_BOUND |(mode & 0x0f));
}

int mcu_get_lum(void){

	return mcu_cmd(CMD_GET_LUM);
}

void mcu_set_board_id(u8 board_id)
{
	syslog(LOG_INFO, "mcu_set_board_id(%d)\n", board_id);
	mcu_cmd(CMD_SET_BOARD_ID |(board_id & 0x0f));
}
