#include "includes.h"

int DxDoorCam_hw_init(void)
{
    //ir led
    libgpio_config(PIN_IRLED_CTL, PIN_DIR_OUTPUT);
    libgpio_write(PIN_IRLED_CTL, PIN_LVL_HIGH);

#ifndef CONFIG_FAST_BOOT_EN
    //sensor power
    libgpio_config(PIN_SENSOR_POWER, PIN_DIR_OUTPUT);
    libgpio_write(PIN_SENSOR_POWER, PIN_LVL_HIGH);

    //sensor reset
    libgpio_config(PIN_SENSOR_RESET, PIN_DIR_OUTPUT);
    libgpio_write(PIN_SENSOR_RESET, PIN_LVL_HIGH);
#endif

	return 0;
}

int ref_sfinit_fast_disable = 0;
int DxDoorCam_hw_early_init(void)
{
#ifdef CONFIG_AUDIO_EN
	libacodec_earlyinit();
#endif

    lowlevel_config(115200, UART_IER_RDI);

    ddr_disable();

    {
    	/* Need additional delay on Brickcom board
    	 * otherwise DDR init get fail */
    	int i;
    	for(i=0;i<1000;i++)
    		lowlevel_putc(' ');
    }

#ifdef CONFIG_MFT_TOOL_VERSION_EN
    ddrset_ddr3_533M();
    clockconfig_360M_4M();
#else
    ddrset_ddr3_400M();
    clockconfig_360M_2M_2736();
#endif

	if(ref_sfinit_fast_disable == 0){
        g_sflash.sta_reg_qe_bit =0; //do not set qe register to reduce boot time
        g_sflash.check_id_en =0;
	}
	else{
        //g_sflash.sta_reg_qe_bit =1;
        g_sflash.check_id_en =0;
	}

	return 0;
}

int DxDoorCam_hw_init_post(void)
{
    return 0;
}

#ifdef CONFIG_AUDIO_EN
int DxDoorCam_board_speaker_on(int enable)
{
	debug_printf("DxDoorCam_board_speaker_on enable=%d\n",enable);
	if(enable)
	{//Speaker enable
		libgpio_config(PIN_AU_PA, PIN_DIR_OUTPUT);
		libgpio_write(PIN_AU_PA, PIN_LVL_HIGH);
	}
	else
	{
		libgpio_config(PIN_AU_PA, PIN_DIR_OUTPUT);
		libgpio_write(PIN_AU_PA, PIN_LVL_LOW);
	}
    return 0;

}
#endif

int clockconfig_fps2rdy(u32 ve_div,u32 pram_div)
{
    return 0;
}

int clockconfig_up_fix(void)
{
    return 0;
}
