#include "mcu_ctrl.h"

/* MCU Command */
#define CMD_POWER_OFF	0x01
#undef CMD_RESET
#define CMD_RESET 		0x02
#define CMD_GET_EVENT 	0x03
#define CMD_GET_MODE 	0x04
#define CMD_GET_PIR 	0x05
#define CMD_GET_LUM 	0x06
#define CMD_GET_BAT 	0x07
#define CMD_SET_MODE 	0x80 /* BIT0~3: MODE_XXX */
#define CMD_SET_PIRST 	0xa0 /* BIT0~3: pir sensitivity. 0: most sensitive. 0xe: most insensitive. 0xf: disable PIR */
#define CMD_SET_NIGHT 	0xb0 /* BIT0~3: 0:force day, 1:force night, 2~f:threshold, 2:more day, f:more night */
#define CMD_SET_LED 	0x40 /* BIT0~3: LED_STATUS_XXX, BIT4~5: LED_COLOR_XXX */
#define CMD_SHORT_SLEEP	0x30
#define CMD_SET_BOUND 	0x60

#define DATA_READY_TIMEOUT 0x10000

static t_libsif_cfg _mcu_cfg ;
static int _mcu_inited = 0;
pthread_mutex_t _mcu_mutex = PTHREAD_MUTEX_INITIALIZER; 

void mcu_init(void)
{
	if(_mcu_inited == 1){
		return;
	}
#if 0
	//Init SPI
	memset(&_mcu_cfg, 0, sizeof(_mcu_cfg));
	SIF0_USE_P1;
	_mcu_cfg.cpha = 1;
	libsif0_init(&_mcu_cfg, 0, 1, 1000000);
#else
	uarts_init(clockget_bus(), 115200, 0);
#endif

	// Init GPIO
//	libgpio_config(PIN_UPDATE_INT,PIN_DIR_OUTPUT);   
//	libgpio_write(PIN_UPDATE_INT, PIN_LVL_LOW);

	libgpio_config(PIN_BSL_RST,PIN_DIR_OUTPUT);  	 
	libgpio_config(PIN_BSL_TEST,PIN_DIR_OUTPUT);
	libgpio_write(PIN_BSL_RST,0);
	libgpio_write(PIN_BSL_TEST,0);

	libgpio_config(PIN_MCU_EVENT, PIN_DIR_INPUT);    
	libgpio_config(PIN_MCU_DATA_READY, PIN_DIR_INPUT);

	_mcu_inited = 1;
	syslog(LOG_INFO,"Init MCU interface.\n");
}

int mcu_cmd(int cmd)
{
	return 0;
}

int mcu_cmd_write(void *value, int len)
{	
	int i = 0;
	u8 *val = (u8 *)value;
	for(i=0;i< len;i++)
	{
		uarts_putc(*((unsigned char *)value+i));
	}
	return i;
}

int mcu_cmd_read(void *value, int len)
{
	int ret = 0;
	int timeout = 0;
	*(unsigned char *)value = '\0';
	int read_ret = 0;
	do{
		// Read the text
		read_ret = uarts_getc_nob((unsigned char *)value);
		if(-1 != read_ret)
		{
			ret++;
			value++;
		}	
		else
		{	
			//usleep(10000);    //10ms
			{volatile int delay; for(delay=0; delay<25000;delay++);}
			//syslog(LOG_WARNING, "%s(%d):timeout\n", __FUNCTION__, __LINE__);
			timeout++;
		}
		if(ret == len)
		{
			//syslog(LOG_INFO, "%s(%d), Read data successful\r\n", __FUNCTION__, __LINE__);
#if PM_LOG_LEVEL >= PM_LV_DBG
			int i;
			char buffer[128]="";
			for(i=0; i<ret; i++)
			{
				sprintf(buffer+3*i, " %02x", *((unsigned char *)value+i));
			}
			//PMdbg_rw("[UART R]%s\n", buffer);
#endif
			break;
		}
		else if(200 == timeout)
		{
			syslog(LOG_ERR, "%s(%d): read cmd timeout\r\n", __FUNCTION__, __LINE__);
			return -1;
		}

	}while(1);

	return 0;
}

void mcu_set_mode(t_sys_mode mode)
{
	u8 id = 0x74;
	char ack;

	syslog(LOG_INFO, "mcu_set_mode(%d)\n", mode);
	pthread_mutex_lock(&_mcu_mutex);
	mcu_cmd_write(&id, 1);	
	if(mcu_cmd_read(&ack, 1) < 0)
	{
		syslog(LOG_ERR, "read ack failed\n");
		pthread_mutex_unlock(&_mcu_mutex);
		return;
	}
	mcu_cmd_write(&mode, 1);
	if(mcu_cmd_read(&ack, 1) < 0)
	{
		syslog(LOG_ERR, "read ack failed\n");
		pthread_mutex_unlock(&_mcu_mutex);
		return;
	}
	pthread_mutex_unlock(&_mcu_mutex);
}

t_sys_mode mcu_get_mode(void)
{
	u8 id = 0x22;
	u8 val;

	pthread_mutex_lock(&_mcu_mutex);
	mcu_cmd_write(&id, 1);	
	mcu_cmd_read(&val, 1);
	pthread_mutex_unlock(&_mcu_mutex);
	syslog(LOG_INFO, "mcu_get_mode(%d)\n", val);
	return val;
}

void mcu_set_led(t_led_color color, t_led_status status)
{
	syslog(LOG_INFO, "mcu_set_led(%d, %d)\n", color, status);
	//mcu_cmd(CMD_SET_LED | ((color & 0x03) << 2) | (status & 0x3));
}

void mcu_set_led_nortos(t_led_color color, t_led_status status)
{
	//_mcu_cmd(CMD_SET_LED | ((color & 0x03) << 2) | (status & 0x3));
}

void mcu_set_pirsensitive(u8 st)
{
	syslog(LOG_INFO, "mcu_set_pirsensitive(%d)\n", st);
	//mcu_cmd(CMD_SET_PIRST |(st & 0x0f));
}

int mcu_get_pir(void)
{
	int ret;
	//ret = mcu_cmd(CMD_GET_PIR);
	syslog(LOG_INFO, "mcu_get_pirsensitive(%d)\n",ret);
	return ret;
}

void mcu_set_nightmode(u8 mode)
{
	syslog(LOG_INFO, "mcu_set_nightmode(%d)\n", mode);
	//mcu_cmd(CMD_SET_NIGHT |(mode & 0x0f));
}

static const u32 sleep_time[16] = 
{
	1, 2, 5, 10,
	20, 30, 45, 60,
	90, 120, 180, 300,
	600, 900, 1200, 1800
};
void mcu_short_sleep(int time)
{
	int i; 
	for(i=0; i<16; i++){
		if(time <= sleep_time[i]){
			break;
		}
	}
	if(i >= 16){
		i = 15;
	}

	syslog(LOG_INFO, "mcu_short_sleep(%d)\n", sleep_time[i]);
	u8 cmd =0;
	cmd = CMD_SHORT_SLEEP | (i & 0x0f);
	//mcu_cmd(cmd);
}

int mcu_get_vbat(void)
{
	int mcu_evt = 0;
	//mcu_cmd(CMD_GET_BAT);
	if(mcu_evt < 0){
		return 0;
	}else if(mcu_evt == 0){
		//wait 20ms and check again.
		{volatile int d; for(d=0; d<1000000; d++);}
		//mcu_evt = mcu_cmd(CMD_GET_BAT);
	}

#if 0 //TODO
	//FIXME: how about V3???

/* 3.0V: 59
   5.0V: 99
   linear
 */
	int volt = (5 + (mcu_evt * 5));

	int v;
	if(volt <= 350){
		v = 0;
	}else if(volt >= 450){
		v =  100;
	}else{
		v = (volt - 350);
	}

	if(libgpio_read(PIN_GPIO_0)){
		v += 256;
	}

	return v;
#endif
	return 0;
}

int mcu_event_ready(void)
{
	return libgpio_read(PIN_MCU_EVENT);
}

t_mcu_evt mcu_get_event(void)
{
	u8 id = 0x01;
	u8 evt = 0;

	pthread_mutex_lock(&_mcu_mutex);
	//get event type
	if (mcu_cmd_write(&id, 1) < 0)
	{
		pthread_mutex_unlock(&_mcu_mutex);
		return -1;
	}
	if (mcu_cmd_read(&evt, 1) < 0)
	{
		pthread_mutex_unlock(&_mcu_mutex);
		return -1;
	}
	pthread_mutex_unlock(&_mcu_mutex);
	return evt;
}

t_mcu_evt mcu_get_event_nortos(void)
{
	//return _mcu_cmd(CMD_GET_EVENT);
}

void mcu_power_off(void)
{
	debug_printf("mcu_power_off\n");
	u8 id = 0x87;

	pthread_mutex_lock(&_mcu_mutex);
	mcu_cmd_write(&id, 1);	
	pthread_mutex_unlock(&_mcu_mutex);
}

void mcu_set_boundary(u8 mode)
{
	syslog(LOG_INFO, "mcu_set_bound(%d)\n", mode);
	//mcu_cmd(CMD_SET_BOUND |(mode & 0x0f));
}
int mcu_get_lum(void){

	//return mcu_cmd(CMD_GET_LUM);
}

void mcu_reset_soc(void)
{
	debug_printf("mcu_reset_soc\n");
	//mcu_cmd(CMD_RESET);
}

void mcu_set_selfwake(u32 time)
{
	u8 id = 0x81;
	char ack;
	u8 val;

	debug_printf("mcu_set_time(%d)\n", time);
	pthread_mutex_lock(&_mcu_mutex);
	mcu_cmd_write(&id, 1);	
	if(mcu_cmd_read(&ack, 1) < 0)
	{
		syslog(LOG_ERR, "read ack failed\n");
		pthread_mutex_unlock(&_mcu_mutex);
		return;
	}
	val = time & 0xff;
	mcu_cmd_write(&val, 1);
	if(mcu_cmd_read(&ack, 1) < 0)
	{
		syslog(LOG_ERR, "read ack failed\n");
		pthread_mutex_unlock(&_mcu_mutex);
		return;
	}
	val = time >> 8;
	mcu_cmd_write(&val, 1);
	if(mcu_cmd_read(&ack, 1) < 0)
	{
		syslog(LOG_ERR, "read ack failed\n");
		pthread_mutex_unlock(&_mcu_mutex);
		return;
	}
	pthread_mutex_unlock(&_mcu_mutex);
}

#define PM_CMD_GetFlag (0x26)
#define PM_CMD_SetFlag (0x98)
#define PM_CMD_GetLightSensor (0x21)

unsigned int mcu_get_flag(void)
{
    u8 id = PM_CMD_GetFlag;
    unsigned int ctrl_flag = 0;

    pthread_mutex_lock(&_mcu_mutex);
    //get flag
    if (mcu_cmd_write(&id, 1) < 0)
    {
        pthread_mutex_unlock(&_mcu_mutex);
        return 0;
    }
    if(mcu_cmd_read((unsigned char *)&ctrl_flag, 4) < 0)
    {
        pthread_mutex_unlock(&_mcu_mutex);
        return 0;
    }
    pthread_mutex_unlock(&_mcu_mutex);
    return ctrl_flag;
}

int mcu_set_flag(unsigned int flag)
{
    u8 id = PM_CMD_SetFlag;
    int ret = -1;
    char ack;
    int i = 0;
    unsigned char *p = (unsigned char *)(&flag);

    pthread_mutex_lock(&_mcu_mutex);
    if (mcu_cmd_write(&id, 1) < 0)
    {
        syslog(LOG_ERR, "write id failed\n");
        goto exit;
    }
    if(mcu_cmd_read(&ack, 1) < 0)
    {
        syslog(LOG_ERR, "read ack failed\n");
        goto exit;
    }

    for (i=0;i<4;i++)
    {
        if (mcu_cmd_write(p+i, 1) < 0)
        {
            syslog(LOG_ERR, "write flag failed\n");
            goto exit;
        }
        if(mcu_cmd_read(&ack, 1) < 0)
        {
            syslog(LOG_ERR, "read ack failed\n");
            goto exit;
        }
    }
    ret = 0;
exit:
    pthread_mutex_unlock(&_mcu_mutex);
    return ret;
}

static int mcu_pf(int n)
{
    if(0 == n)
    {
        return 1;
    }
    else if(1 == n)
    {
        return 2;
    }
    else
    {
        return (2 * mcu_pf(n-1));
    }

}
unsigned short mcu_get_lightsensor(void)
{
    u8 id = PM_CMD_GetLightSensor;
    unsigned short vshort = 0;
    unsigned int ch0_value;
    unsigned int ch1_value;
    float dn_value;
    unsigned short result;

    pthread_mutex_lock(&_mcu_mutex);
    if (mcu_cmd_write(&id, 1) < 0)
    {
        pthread_mutex_unlock(&_mcu_mutex);
        return 0;
    }
    if(mcu_cmd_read((unsigned short *)&vshort, 2) < 0)
    {
        pthread_mutex_unlock(&_mcu_mutex);
        return 0;
    }
    pthread_mutex_unlock(&_mcu_mutex);
    ch0_value = (vshort >> 12) & 0xf;
    ch1_value = (((vshort >> 8) & 0xf) << 8) + (vshort & 0xff);
    dn_value = (0.01 * mcu_pf(ch0_value) * ch1_value);
    result = (int)dn_value;
    syslog(LOG_INFO, "Lux:%d, vshort:%04x\n", result, vshort);

    return result;
}
