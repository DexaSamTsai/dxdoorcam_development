#include "libDxDoorCam.h"

//daynight threshold value, 0x0: force day mode, 0x1: force night mode 0x2~0xf: Day/Night threshold.1,2,3,4,6,8,10,13,16,20,24,28,32,40
#define NV_DAYNIGHT_DAYMODE		(0x0)//force daymode
#define NV_DAYNIGHT_NIGHTMODE	(0x1)//force nightmode
#define NV_DAYNIGHT_THRESHOLD	(0x4)//day to night thresvalue,lum < 5lux(ADC=2)

static u8 cur_daynight_mode = 0;//daynight mode 0:day 1: night
static u8 cur_daynight_flag = 0;//daynight gpio status 0:day 1: night
static u32 edge_toggle = 0; //1: postedge; 0:negedge
static void (*cb_cmd_setdaynight)(int) = NULL;
static u32 board_id = 0;

void daynight_mode_switch(u8 mode)
{
	if(mode == 1)//night mode
	{
		cur_daynight_mode = 1;
		libisp_set_daynight(0, 1);//night mode
#ifdef PIN_IRLED_CTL
		if (board_id != BOARD_ID_RD19)
			libgpio_write(PIN_IRLED_CTL, 1);
		else
			libgpio_write(PIN_IRLED_CTL, 0);
#endif
		//debug_printf("night mode\n");
	}else if(mode == 0)//day mode
	{
		cur_daynight_mode = 0;
		libisp_set_daynight(0, 0);//day mode
#ifdef PIN_IRLED_CTL
		if (board_id != BOARD_ID_RD19)
			libgpio_write(PIN_IRLED_CTL, 0);
		else
			libgpio_write(PIN_IRLED_CTL, 1);
#endif
		//debug_printf("day mode\n");
	}
}

u8 get_daynight_status(void)
{
	return cur_daynight_mode;
}

void daynight_sensitive_set(u8 mode)
{
	if(mode == 0x0)//force daymode
	{
		mcu_set_nightmode(NV_DAYNIGHT_DAYMODE);
	}else if(mode == 0x1){//force nightmode
		mcu_set_nightmode(NV_DAYNIGHT_NIGHTMODE);
	}else{//daynight auto mode
		mcu_set_nightmode(NV_DAYNIGHT_THRESHOLD);
	}
}

#ifdef PIN_DAYNIGHT_GPIO
static int daynight_mode_update(void* arg)
{
	//syslog(LOG_INFO, "daynight_mode_update\n");
	if(libgpio_read(PIN_DAYNIGHT_GPIO) != cur_daynight_flag){
		cur_daynight_flag = libgpio_read(PIN_DAYNIGHT_GPIO);

		if(cb_cmd_setdaynight){
			cb_cmd_setdaynight(cur_daynight_flag);
		}

		//syslog(LOG_INFO, "cur_daynight_flag = %d\n",cur_daynight_flag);
		edge_toggle = 1 - edge_toggle;
		libgpio_config_interrupt(PIN_DAYNIGHT_GPIO, 1, 0, edge_toggle);
	}
	return 0;
}
#endif

void daynight_runtime_init(void *cb)
{
#ifdef CONFIG_MCU_EN
	board_id = DxDoorCam_GetBoardID();
	//FIXME: for CPSE demo temp, need more consideration.
	mcu_set_nightmode(3); //set lum sensitive
	mcu_set_boundary(4);
#endif
#ifdef PIN_DAYNIGHT_GPIO
	cb_cmd_setdaynight = cb;
	libgpio_config(PIN_DAYNIGHT_GPIO, PIN_DIR_INPUT);
	cur_daynight_flag = libgpio_read(PIN_DAYNIGHT_GPIO);
	daynight_mode_switch(cur_daynight_flag);
	if(cur_daynight_flag)
		edge_toggle = 0; //night, negedge
	else
		edge_toggle = 1; //day, posedge
	syslog(LOG_INFO, "init daynight:%d, edge_toggle:%d\n", cur_daynight_flag,edge_toggle);
	libgpio_config_interrupt(PIN_DAYNIGHT_GPIO, 1, 0, edge_toggle);//post edge
	libgpio_irq_request(PIN_DAYNIGHT_GPIO,daynight_mode_update,NULL,NULL);
	syslog(LOG_WARNING, "Daynight from MCU, PIN_DAYNIGHT_GPIO defined\n");
#else
#ifndef CONFIG_FAST_BOOT_EN
	libisp_set_daynight(0, 2); //FIXME: set auto mode only for test
#endif
	syslog(LOG_WARNING, "Daynight from MPU, PIN_DAYNIGHT_GPIO not defined\n");
#endif
}
