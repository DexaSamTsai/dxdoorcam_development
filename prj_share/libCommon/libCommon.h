#ifndef _LIBCOMMON_H_
#define _LIBCOMMON_H_

#include "includes.h"
#include "mcu_ctrl.h"

#define DOORCAM_BASE_URL            "alldev.bestwificam.com"
#define DOORCAM_RUN_IDLE			while(1){sleep(1);}

// Firmware ID !!DON'T EDIT FW ID
enum{
	FIRMWARE_BL_FAST = 0,
	FIRMWARE_BL_2    = 1,
	FIRMWARE_DOORCAM = 2,
	FIRMWARE_DOORCAM_DDR = 3,
	FIRMWARE_DOORCAM_LIVEVIEW = 4,
	FIRMWARE_DOORCAM_LIVEVIEW_DDR = 5,
	FIRMWARE_WIFI1  = 6,
	FIRMWARE_WIFI2  = 7,
	FIRMWARE_MCU     = 8,
	FIRMWARE_DOORCAM_UPGRADE = 9,
	FIRMWARE_DOORCAM_UPGRADE_DDR = 10,
	FIRMWARE_RES	= 11,
	FIRMWARE_DOORCAM_TEST = 12,
	FIRMWARE_DOORCAM_TEST_DDR = 13,
	FIRMWARE_CALFW = 14,
};

enum{
	BOARD_ID_RD19 = 0,
	BOARD_ID_SC20 = 1,
	BOARD_ID_RD15 = 2,
	BOARD_ID_RD18 = 3,
};

#define SHADING_D_TABLE     (0)
#define SHADING_C_TABLE     (1)
#define SHADING_A_TABLE     (2)

#define BIOC_DOORCAM_NETWORK_OK		0x0081
#define BIOC_DOORCAM_INIT_POST		0x0082
#define BIOC_DOORCAM_IQ_CALI		0x0083
#define BIOC_DOORCAM_CALI_SHADING   0x0084
#define BIOC_DOORCAM_DETECT_OLD_AWB	0x0085

// Get MCU FW
extern int
Common_GetMCUFW( u8 *buf, u32 from );

extern int
Common_GetBoardID( void );


#endif
