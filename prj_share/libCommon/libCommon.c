/*!
 *  @file       libCommon.c
 */

//==========================================================================
// Header File
//==========================================================================
#include "libCommon.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Param
//==========================================================================

//==========================================================================
// Static Function
//==========================================================================

//==========================================================================
// Global Function
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
// Get WIFI FW, used in  "share/network/libwlan_chip/brcm4334/"
u32 board_firmware_get(u8 *buf)
{
	int ret = partition_load_sf(FIRMWARE_WIFI2, buf);
	if(ret <= 0){
		syslog(LOG_NOTICE, "xmodem send share/network/libwlan_chip/fw/wifi_fw_BRCM4334_2.bin....\n");
		ret = load_from_xmodem((u32)buf);
	}
	return ret;
}

u32 board_bcm4334fw_get(u8 *buf,int part)
{
	int ret = partition_load_sf(FIRMWARE_WIFI1, buf);
	if(ret <= 0){
		syslog(LOG_NOTICE, "xmodem send share/network/libwlan_chip/fw/wifi_fw_BRCM4334_1.bin....\n");
		ret = load_from_xmodem((u32)buf);
	}
	return ret;
}

// Get MCU FW
int
Common_GetMCUFW( u8 *buf, u32 from )
{
	int ret = -1;

	if(from == 1){	// sflash
		ret = partition_load_sf(FIRMWARE_MCU,buf);
	}

	if( ret > 0){
		syslog(LOG_NOTICE, "Load mcu.bin from SFlash.\n");
		return ret;
	}

	// uart
	syslog(LOG_NOTICE, "xmodem send build/doorcam/mcu.bin....\n");
	ret = load_from_xmodem((u32)buf);
	return ret;
}

int
Common_GetBoardID( void )
{
#ifdef RD19
	return BOARD_ID_RD19;
#elif defined RD18
	return BOARD_ID_RD18;
#else
	return BOARD_ID_RD15;
#endif
}
