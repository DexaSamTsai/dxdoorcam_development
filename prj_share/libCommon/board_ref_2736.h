#ifndef _BOARD_SC20_
#define _BOARD_SC20_

#define PIN_BSL_TEST		PIN_GPIO_02		
#define PIN_BSL_RST			PIN_GPIO_03
#define PIN_MCU_EVENT		PIN_GPIO_10		
#define PIN_MCU_DATA_READY	PIN_GPIO_06		

#define PIN_SENSOR_POWER    PIN_GPIO_05
#define PIN_SENSOR_RESET    PIN_GPIO_04 

#define PIN_DAYNIGHT_GPIO   PIN_GPIO_01

#define PIN_AU_PA           PIN_GPIO_07

#define PIN_IRLED_CTL		PIN_GPIO_00

int DxDoorCam_hw_init(void);
int DxDoorCam_hw_early_init(void);
int DxDoorCam_hw_init_post(void);

int clockconfig_fps2rdy(u32 ve_div,u32 pram_div);
int clockconfig_up_fix(void);

#endif
