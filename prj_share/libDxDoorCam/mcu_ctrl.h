#ifndef _MCU_CTRL_H_
#define _MCU_CTRL_H_
#include "includes.h"
#include "libDxDoorCam.h"

/* Event type for mcu_get_event() */
typedef enum e_mcu_evt{
	EVT_NULL,
	EVT_WIFI,
	EVT_WPS_CLICK,	 		/* short click */
	EVT_PIR,
	EVT_DAY_MODE,
	EVT_NIGHT_MODE,
	EVT_WPS_LONGPRESS,
	EVT_WPS_LONGRELEASE,
	EVT_KEY2_PRESSED, 	 	/* short click */
	EVT_WPS_DOUBLECLICK,  	/* double click */
	EVT_TIMER_EXPIRED,
}t_mcu_evt;

/* Mode type for mcu_get_mode()/mcu_set_mode() */
typedef enum e_sys_mode{
	MODE_INIT = 1, 	/* MCU private mode, OV788 off */
	MODE_INIT2, 	/* MCU w, OV788 r, OV788 need init wifi in this mode */
	MODE_INIT3, 	/* MCU r, OV788 w */
	MODE_SETUP, 	/* if OV788 is off, MCU w, OV788 r. Then power on OV788, OV788 run 'doorcam_setup' after get sysmode.
				   	   if OV788 is on, MCU r, OV788 w. OV788 set the sysmode and then run 'doorcam_setup' fw */
	MODE_STANDBY, 	/* MCU r, OV788 w, OV788 going to off */
	MODE_WAKEUP, 	/* MCU private mode */
	MODE_UPLOAD, 	/* MCU r, OV788 w */
	MODE_LIVEVIEW, 	/* MCU r, OV788 w */
	MODE_UPGRADE, 	/* if OV788 is off, MCU w, OV788 r. Then power on OV788, OV788 run 'doorcam_upgrade' after get sysmode.
				       if OV788 is on, MCU r, OV788 w. OV788 set the sysmode and then run 'doorcam_upgrade' fw */
	MODE_LOWPOWER, 	/* MCU private mode, OV788 is off */
	MODE_MP, 		/* MCU private mode, OV788 USB debug mode */
}t_sys_mode;

/* Color type for mcu_set_led() */
typedef enum e_led_color{
	LED_COLOR_UNUSED0,
	LED_COLOR_RED,
	LED_COLOR_GREEN,
	LED_COLOR_UNUSED1,
}t_led_color;

/* Status type for mcu_set_led() */
typedef enum e_led_status{
	LED_STATUS_OFF,
	LED_STATUS_ON,
	LED_STATUS_FAST,
	LED_STATUS_NORMAL,
    LED_STATUS_UNUSED0,
    LED_STATUS_UNUSED1,
    LED_STATUS_UNUSED2,
    LED_STATUS_UNUSED3,
}t_led_status;

/* MCU API */
void mcu_init(void);

// event
int  mcu_event_ready(void);
t_mcu_evt mcu_get_event(void);
t_mcu_evt mcu_get_event_nortos(void);

// mode
void mcu_set_mode(t_sys_mode mode);
t_sys_mode mcu_get_mode(void);

// poweroff && sleep
void mcu_short_sleep(int time);
void mcu_power_off(void);

// other
void mcu_set_led(t_led_color color, t_led_status status);
void mcu_set_led_nortos(t_led_color color, t_led_status status);
void mcu_set_pirsensitive(u8 st);
int mcu_get_pir(void);
void mcu_set_nightmode(u8 mode);
int  mcu_get_vbat(void); //ret: 0~100, percentage
void mcu_set_boundary(u8 mode);
int mcu_get_lum(void);
void mcu_set_selfwake(u32 time);
void mcu_set_board_id(u8 board_id);
#endif
