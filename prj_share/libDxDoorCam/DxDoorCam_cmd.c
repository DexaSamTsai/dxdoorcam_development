#include "libDxDoorCam.h"

static int _cmd_fw_help(void)
{
	printf("usage: fw [ upgrade | test | main]\n");
	return 0;
}

static int _cmd_fw(int argc, char ** argv)
{
	if(argc < 2){
		return _cmd_fw_help();
	}

	if(! strcmp(argv[1], "upgrade") ){
		partition_boot_sf(FIRMWARE_DOORCAM_UPGRADE);
	}else if(! strcmp(argv[1], "test") ){
		partition_boot_sf(FIRMWARE_DOORCAM_TEST);
	}else if(! strcmp(argv[1], "main") ){
		partition_boot_sf(FIRMWARE_DOORCAM);
#if defined(CONFIG_USB_EN) && defined(CONFIG_USB_DEVICE_EN)
	}else if(! strcmp(argv[1], "usbboot") ){
		boot_from_usb();
#endif
	}else{
		return _cmd_fw_help();
	}

	return 0;
}

CMDSHELL_DECLARE(fw)
	.cmd = {'f', 'w', '\0'},
	.handler = _cmd_fw,
	.comment = "switch fw",
};

static int _cmd_frm(int argc, char ** argv)
{
	if(argc < 2){
		return -1;
	}
	u32 frm = atoi(argv[1]);

	debug_printf("frm = %d\n", frm);

	libvs_set_framerate(0, frm);
	libvs_set_vs_framerate(0, frm);
	return 0;
}

CMDSHELL_DECLARE(fm)
	.cmd = {'f', 'm', '\0'},
	.handler = _cmd_frm,
	.comment = "change frm",
};

#ifdef CONFIG_MCU_EN
extern int mcu_cmd(u8 cmd);
static int _cmd_mcu_help(void)
{
	printf("usage: mcu <cmd>\n");
	printf("       cmd: 0xNN\n");
	return 0;
}

static int _cmd_mcu(int argc, char ** argv)
{
	if(argc != 2){
		return _cmd_mcu_help();
	}

	u8 cmd = strtoul(argv[1], NULL, 0);

	int ret = mcu_cmd(cmd);

	printf("MCU cmd 0x%x return 0x%x\n", cmd, ret);

	return 0;
};

CMDSHELL_DECLARE(mcu)
	.cmd = {'m', 'c', 'u', '\0'},
	.handler = _cmd_mcu,
	.comment = "mcu cmd",
};

#endif
