#ifndef _LIBEASYCAM_H_
#define _LIBEASYCAM_H_

#include "includes.h"
#include "mcu_ctrl.h"

#define DOORCAM_BASE_URL            "alldev.bestwificam.com"
#define DOORCAM_RUN_IDLE			while(1){sleep(1);}

// Firmware ID !!DON'T EDIT FW ID
enum{
	FIRMWARE_BL_FAST = 0,
	FIRMWARE_BL_2    = 1,
	FIRMWARE_DOORCAM = 2,
	FIRMWARE_DOORCAM_DDR = 3,
	FIRMWARE_DOORCAM_LIVEVIEW = 4,
	FIRMWARE_DOORCAM_LIVEVIEW_DDR = 5,
	FIRMWARE_WIFI1  = 6,
	FIRMWARE_WIFI2  = 7,
	FIRMWARE_MCU     = 8,
	FIRMWARE_DOORCAM_UPGRADE = 9,
	FIRMWARE_DOORCAM_UPGRADE_DDR = 10,
	FIRMWARE_RES	= 11,
	FIRMWARE_DOORCAM_TEST = 12,
	FIRMWARE_DOORCAM_TEST_DDR = 13,
	FIRMWARE_CALFW = 14,
};

enum{
	BOARD_ID_RD19 = 0,
	BOARD_ID_SC20 = 1,
	BOARD_ID_RD15 = 2,
	BOARD_ID_RD18 = 3,
};

extern char DxDoorCam_wifi_ssid[32];
extern char DxDoorCam_wifi_key[64];
extern u8 DxDoorCam_mac[6];

extern char DxDoorCam_devsn[];
extern char DxDoorCam_devpw[];

// Public Funcs
extern int
DxDoorCam_Init( void );

extern int
DxDoorCam_Exit( void );

extern int
DxDoorCam_PowerOff( void );

extern int
DxDoorCam_GetMCUFW( u8 *buf, u32 from );  // from: 0, uart; 1, sflash.

int DxDoorCam_init_sflash(void);

extern int
DxDoorCam_InitNetwork( void );

extern int
DxDoorCam_ExitNetwork(void);

extern int
DxDoorCam_GetBoardID( void );

extern void
DxDoorCam_SetFirstBoot( int be_first ); // be_first: 0, boot by wakeup; 1, boot by power on.

extern int
DxDoorCam_IsFirstBoot( void );

extern int
DxDoorCam_WlanRestorePara( void );

extern int
DxDoorCam_WlanSavePara( void );

#ifdef CONFIG_AUDIO_EN
int DxDoorCam_board_speaker_on(int enable);
#endif

void iq_calibration_update(void);
#define SHADING_D_TABLE     (0)
#define SHADING_C_TABLE     (1)
#define SHADING_A_TABLE     (2)
void iq_shading_update(int table_num);
u32 detect_old_awb_calibration(void);

void daynight_mode_switch(u8 mode);
void daynight_runtime_init(void* cb);
void daynight_sensitive_set(u8 mode);

#define BIOC_DOORCAM_NETWORK_OK		0x0081
#define BIOC_DOORCAM_INIT_POST		0x0082
#define BIOC_DOORCAM_IQ_CALI		0x0083
#define BIOC_DOORCAM_CALI_SHADING   0x0084
#define BIOC_DOORCAM_DETECT_OLD_AWB	0x0085
#endif
