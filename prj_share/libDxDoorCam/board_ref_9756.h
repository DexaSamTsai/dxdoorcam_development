#ifndef _BOARD_EVB_196PIN_
#define _BOARD_EVB_196PIN_

#define PIN_BSL_TEST		PIN_GPIO_02		// JTAGBSL, MCU's TEST
#define PIN_BSL_RST			PIN_GPIO_03     // RSTBSL_OV
#define PIN_MCU_EVENT		PIN_GPIO_10		// RSVD2_M_OV, MCU'S P2.1
#define PIN_MCU_DATA_READY	PIN_GPIO_06		// RSVD4_M_OV, MCU's P3.2

#define PIN_DAYNIGHT_GPIO 	PIN_GPIO_01     //RSVD1_M_OV, MCU's P2.0
#define PIN_IRLED_CTL		PIN_GPIO_00

int DxDoorCam_hw_init(void);
int DxDoorCam_hw_early_init(void);
int DxDoorCam_hw_init_post(void);

int clockconfig_fps2rdy(u32 ve_div,u32 pram_div);
int clockconfig_up_fix(void);

#endif
