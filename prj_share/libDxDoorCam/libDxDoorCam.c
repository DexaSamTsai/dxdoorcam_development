/*!
 *  @file       libDxDoorCam.c
 */

//==========================================================================
// Header File
//==========================================================================
#include "libDxDoorCam.h"
#include "lwip/opt.h"
#include "lwip/icmp.h"
#include "lwip/inet_chksum.h"
#include "lwip/sockets.h"
#include "lwip/mem.h"
#include "lwip/inet.h"
#include "netif/etharp.h"
#include "lwip/ip.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#if LWIP_VERSION >= 0x02000000
#include "lwip/prot/dhcp.h"
#endif
#include "lwip/ip_addr.h"
#include "lwip/init.h"
#include "lwip/dns.h"

//==========================================================================
// Type Define
//==========================================================================
# ifndef dbg_line
#	define dbg_line( fmt, ... )		printf( "[%s line: %d]"fmt"\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_error
#	define dbg_error( fmt, ... )	printf( "\033[1;31m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

//==========================================================================
// Global Params
//==========================================================================
t_network_cfg	g_network_cfg;

//==========================================================================
// Static Param
//==========================================================================
static t_wifi_conf conf;

static int sys_is_first_boot = 0;

//==========================================================================
// Static Function
//==========================================================================
static void
wifi_lwip_init_done( void *arg )
{
	dbg_warm();
	ertos_event_signal( arg );
}

//==========================================================================
// Global Function
//==========================================================================
extern int wlan_init_load_addr( unsigned int addr );

//==========================================================================
// APIs
//==========================================================================
// Get WIFI FW, used in  "share/network/libwlan_chip/brcm4334/"
u32 board_firmware_get(u8 *buf)
{
	int ret = partition_load_sf(FIRMWARE_WIFI2, buf);
	if(ret <= 0){
		syslog(LOG_NOTICE, "xmodem send share/network/libwlan_chip/fw/wifi_fw_BRCM4334_2.bin....\n");
		ret = load_from_xmodem((u32)buf);
	}
	return ret;
}

u32 board_bcm4334fw_get(u8 *buf,int part)
{
	int ret = partition_load_sf(FIRMWARE_WIFI1, buf);
	if(ret <= 0){
		syslog(LOG_NOTICE, "xmodem send share/network/libwlan_chip/fw/wifi_fw_BRCM4334_1.bin....\n");
		ret = load_from_xmodem((u32)buf);
	}
	return ret;
}

int
DxDoorCam_Init( void )
{
	mcu_init();
	wlan_init_load_addr( CONFIG_WIFI_FW_LOAD_ADDRESS );
	return 0;
}

int
DxDoorCam_Exit( void )
{
    mpu_stop();
	return 0;
}

int
DxDoorCam_PowerOff( void )
{
    saveenv();
    mcu_power_off();
	return 0;
}

// Get MCU FW
int
DxDoorCam_GetMCUFW( u8 *buf, u32 from )
{
	int ret = -1;

	if(from == 1){	// sflash
		ret = partition_load_sf(FIRMWARE_MCU,buf);
	}

	if( ret > 0){
		syslog(LOG_NOTICE, "Load mcu.bin from SFlash.\n");
		return ret;
	}

	// uart
	syslog(LOG_NOTICE, "xmodem send build/doorcam/mcu.bin....\n");
	ret = load_from_xmodem((u32)buf);
	return ret;
}

int
DxDoorCam_InitNetwork( void )
{
	// Init tcpip thread.
	t_ertos_eventhandler handler = ertos_event_create();

	if ( handler == NULL ) {
		dbg_warm( "[ERROR]: network init create event failed\n" );
		return -1;
	}

	tcpip_init( wifi_lwip_init_done, handler );

	// don't continue until tcpip thread is ready.
	if ( ertos_event_wait( handler, 100 ) != 1 ) {
		dbg_warm( "[ERROR]: Lwip Init Fail, Fatal Error!!\n" );
		return -1;
	}

	ertos_event_delete( handler );

	extern int wifi_thread_init( void );
	wifi_thread_init();

	return 0;
}

int
DxDoorCam_ExitNetwork(void)
{
	extern int wifi_thread_exit(void);
	wifi_thread_exit();
	return 0;
}


void
DxDoorCam_SetFirstBoot( int be_first )
{
	sys_is_first_boot = !!be_first;
}

int
DxDoorCam_IsFirstBoot( void )
{
	return sys_is_first_boot;
}

int
DxDoorCam_GetBoardID( void )
{
	return BOARD_ID_RD19;
}
