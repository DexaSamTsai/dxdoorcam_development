#include "includes.h"

int DxDoorCam_hw_init(void)
{
#ifndef CONFIG_FAST_BOOT_EN
	libgpio_config(PIN_GPIO_05, PIN_DIR_OUTPUT);	//sn_pwdn
	libgpio_write(PIN_GPIO_05, 1);
	
	libgpio_config(PIN_GPIO_04, PIN_DIR_OUTPUT);	//sn_rst
	libgpio_write(PIN_GPIO_04, 1);
#endif
	libgpio_config(PIN_IRLED_CTL, PIN_DIR_OUTPUT);
	libgpio_write(PIN_IRLED_CTL, 1);

	return 0;
}

int clockconfig_up_fix(void)
{
	//isp_div, PLL1 control
	WriteReg32(SC_BASE_ADDR+0x68, (REG32(SC_BASE_ADDR+0x68)&0xFFFF0FFF) |  (3 << 12) );

	//set VE clock to 360M
	WriteReg32(REG_SC_ADDR+0x5c, (ReadReg32(REG_SC_ADDR+0x5c)&0xffff20ff)|(0x3<<14)|(1<<8));

	u8 fb_div = 0x2;
	WriteReg32((SC_BASE_ADDR+0x130), (ReadReg32(SC_BASE_ADDR+0x130)&0xfe000000)|fb_div|(fb_div<<5)|(fb_div<<10)|(fb_div<<15)|(fb_div<<20));//fb0~fb4 div r_fb_div[4:0];r_fb_div[9:5];r_fb_div[14:10];r_fb_div[19:15];r_fb_div[24:20]

	return 0;
}

int clockconfig_fps2rdy(u32 ve_div,u32 pram_div)
{
#ifdef CONFIG_LOWPOWER_EN
//#define CONFIG_ISP40M
#define CONFIG_ISP30M
#ifdef CONFIG_ISP30M
//SYSPLL VCO: 480 MHz
//SYSPLL SYS: 240 MHz
//SYSPLL ISP: 120 MHz
//DDRPLL VCO: 408 MHz
//DDR       : 204 MHz
//DDRPLL EXT: 204 MHz
//MPIPLL VCO: 24 MHz
//MIPITX    : 12 MHz
//MPIPLL EXT: 12 MHz
//USBPLL    : 240 MHz
//CPU       : 240 MHz
//CPU BUS   : 120 MHz
//MPU       : 120 MHz
//MPU BUS   : 120 MHz
//MPU PMCLK : 24 MHz
//PRAM      : 120 MHz
//BUS       : 60 MHz
//MBUS      : 60 MHz
//FB0       : 3 MHz
//FB1       : 3 MHz
//FB2       : 3 MHz
//FB3       : 3 MHz
//FB4       : 3 MHz
//ECIF0     : 3 MHz
//ECIF1     : 3 MHz
//ISP       : 30 MHz
//D1        : 60 MHz
//ISPD1     : 60 MHz
//D4        : 15 MHz
//YUVSCALE  : 30 MHz
//VE        : 120 MHz
//CCLK      : 24 MHz
//SCIF SYS  : 60 MHz
//SCIO SYS  : 60 MHz
#endif
#ifdef CONFIG_ISP40M
//SYSPLL VCO: 480 MHz
//SYSPLL SYS: 240 MHz
//SYSPLL ISP: 160 MHz
//DDRPLL VCO: 792 MHz
//DDR       : 396 MHz
//DDRPLL EXT: 198 MHz
//MPIPLL VCO: 24 MHz
//MIPITX    : 12 MHz
//MPIPLL EXT: 12 MHz
//USBPLL    : 240 MHz
//CPU       : 240 MHz
//CPU BUS   : 120 MHz
//MPU       : 120 MHz
//MPU BUS   : 120 MHz
//MPU PMCLK : 48 MHz
//PRAM      : 120 MHz
//BUS       : 60 MHz
//MBUS      : 60 MHz
//FB0       : 5 MHz
//FB1       : 5 MHz
//FB2       : 5 MHz
//FB3       : 5 MHz
//FB4       : 5 MHz
//ECIF0     : 5 MHz
//ECIF1     : 5 MHz
//ISP       : 40 MHz
//D1        : 80 MHz
//ISPD1     : 80 MHz
//D4        : 20 MHz
//YUVSCALE  : 40 MHz
//VE        : 120 MHz
//CCLK      : 24 MHz
//SCIF SYS  : 60 MHz
//SCIO SYS  : 60 MHz
#endif
	if(pram_div == 0){
		pram_div = 2;	//120M
	}
	if(ve_div ==0){//120M
		ve_div = 2;
	}

#ifdef CONFIG_ISP30M
	WriteReg32(SC_BASE_ADDR+0x68, 0xc00420A);
#endif
#ifdef CONFIG_ISP40M
	WriteReg32(SC_BASE_ADDR+0x68, 0xc00320A);
#endif
	WriteReg32(SC_BASE_ADDR+0x6c, 0x700289);  ///< 0x68, 0x6c modify SYSPLL
	WriteReg32(SC_BASE_ADDR+0x110, 0xc082200); 
	WriteReg32(SC_BASE_ADDR+0x114, 0x4700000); ///< 0x110, 0x114 modify MIPIPLL
	
    WriteReg32(SC_BASE_ADDR+0x3c,(REG32(SC_BASE_ADDR+0x3c)&0xffff80ff)|(pram_div<<8));///<set PRAM clock to 120MHz, r_pram_div_o = r_div[15:8]
	WriteReg32(REG_SC_ADDR+0x44, (ReadReg32(REG_SC_ADDR+0x44)&0xffffff)|(1<<24));  ///< set A5 CPU clock is 240M
	WriteReg32(SC_BASE_ADDR+0x40,REG32(SC_BASE_ADDR+0x40)|BIT31|BIT0);  ///< validate pll clock
	asm("wfe");
    asm("wfe");

	WriteReg32(REG_SC_ADDR+0x58, (ReadReg32(REG_SC_ADDR+0x58)&0xffffE0E0)|(2<<8)|(2)); ///< set bus/mbus clock to 60M
	uart_config(115200, UART_IER_RDI);

	///< the following codes are used to disable clock of useless modules and reset the useless modules.
	WriteReg32(REG_SC_ADDR+0x20, 0x69477FA6);
#ifdef CONFIG_MPU_IMG_EN
	WriteReg32(REG_SC_ADDR+0x24, 0xBEFC7F70);
	WriteReg32(REG_SC_ADDR+0x28, 0x1777FD2);
#else
	WriteReg32(REG_SC_ADDR+0x24, 0xBFFC7F70);
	WriteReg32(REG_SC_ADDR+0x28, 0x1F77FD2);
#endif
	WriteReg32(REG_SC_ADDR+0x2c, 0x9F7FD6FC);
	WriteReg32(REG_SC_ADDR+0x84, 0xF90F1BBC);
	WriteReg32(REG_SC_ADDR+0x30, 0xBC301090);
	WriteReg32(REG_SC_ADDR+0x34, 0x108C3019);
	WriteReg32(REG_SC_ADDR+0x38, 0x10B98);

	WriteReg32(REG_SC_ADDR+0x48, ReadReg32(REG_SC_ADDR+0x48)&(~(BIT29|BIT21|BIT5)));
	
//	WriteReg32(REG_SC_ADDR+0x40, ReadReg32(REG_SC_ADDR+0x40)&(~BIT24));
//	WriteReg32(REG_SC_ADDR+0x4c, ReadReg32(REG_SC_ADDR+0x4c)&(~BIT5)&~(0xff<<10)|(20<<10));		//i2s divider 20,for tmp,for low speed form 360->240M,i2s module clk in was fixed to 12M.
	WriteReg32(REG_SC_ADDR+0x4c, (ReadReg32(REG_SC_ADDR+0x4c)&~(0xff<<10))|(20<<10));		//i2s divider 20,for tmp,for low speed form 360->240M,i2s module clk in was fixed to 12M.

#ifndef CONFIG_MPU_IMG_EN
	WriteReg32(REG_SC_ADDR+0x54, ReadReg32(REG_SC_ADDR+0x54)&(~BIT19));  // [19] JPEG clock enable
#endif

 	WriteReg32(REG_SC_ADDR+0x58, ReadReg32(REG_SC_ADDR+0x58)&(~BIT21));
	
	///< set VE clock to 120M
	WriteReg32(REG_SC_ADDR+0x5c, (ReadReg32(REG_SC_ADDR+0x5c)&0xffff20ff)|(0x3<<14)|(ve_div<<8));//set ve clk div = 2

	///< the following codes are used to disable clock of useless modules and reset the useless modules.
	WriteReg32(REG_SC_ADDR+0x5c, ReadReg32(REG_SC_ADDR+0x5c)&(~(BIT21|BIT31)));
	
	WriteReg32(REG_SC_ADDR+0x130, (ReadReg32(REG_SC_ADDR+0x130)&0x3E000000)|(2<<30)|(31<<20)|(31<<15)|(31<<10)|(31<<5)|31);
	WriteReg32(REG_SC_ADDR+0x134, (ReadReg32(REG_SC_ADDR+0x134)&0xfffffc00)|(31<<5)|31);
	WriteReg32(REG_SC_ADDR+0x134, (ReadReg32(REG_SC_ADDR+0x134)&(~BIT15)));

	WriteReg32(REG_SC_ADDR+0x78, (ReadReg32(REG_SC_ADDR+0x78)|BIT1)); //reset pll2 mipitxpll
	
#ifndef CONFIG_USB_SERIAL
//	WriteReg32(REG_SC_ADDR+0x78, (ReadReg32(REG_SC_ADDR+0x78)|BIT20)&(~BIT15)); //close pll of usb_dev_phy 
	WriteReg32(REG_SC_ADDR+0xc0, (ReadReg32(REG_SC_ADDR+0xc0)|BIT12)&(~BIT13)); //close pll of usb_host_phy 
#endif

	WriteReg32(REG_SC_ADDR+0x18, (ReadReg32(REG_SC_ADDR+0x18)&0xff800000)|((4<<20)|(4<<16)|(4<<12)|(4<<8)|(4<<4)|4));

	///< set some useless pin as input
	WriteReg32(REG_SC_ADDR+0x22c, ReadReg32(REG_SC_ADDR+0x22c)|(0xff900000));
	WriteReg32(REG_SC_ADDR+0x23c, ReadReg32(REG_SC_ADDR+0x23c)|(0x00003f03));
	WriteReg32(REG_SC_ADDR+0x228, ReadReg32(REG_SC_ADDR+0x228)&(0x00007c00));
	WriteReg32(REG_SC_ADDR+0x238, ReadReg32(REG_SC_ADDR+0x238)&(0xfffffff7));
	WriteReg32(REG_SC_ADDR+0x24c, ReadReg32(REG_SC_ADDR+0x24c)&(0xf8000070));
	WriteReg32(REG_SC_ADDR+0x250, ReadReg32(REG_SC_ADDR+0x250)&(0x0003ffff));
	WriteReg32(REG_SC_ADDR+0x254, ReadReg32(REG_SC_ADDR+0x254)&(0xffffffc0));
#else
	WriteReg32(SC_BASE_ADDR+0x68,ReadReg32(SC_BASE_ADDR+0x68)&0xffff0fff|0x6000);
    WriteReg32(SC_BASE_ADDR+0x40,REG32(SC_BASE_ADDR+0x40)|BIT31|BIT0);  //sel pllsysclk

    asm("wfe");
    asm("wfe");
#endif
#ifdef CONFIG_LOWPOWER_EN
	u8 d1_div =3;
#else
	u8 d1_div =2;
#endif
	WriteReg32(SC_BASE_ADDR+0x44,(ReadReg32(SC_BASE_ADDR+0x44)&0xffffffe0)|BIT5|BIT6|(d1_div * 2)); //4:isp_div, isp clk = d1/2
//    WriteReg32(SC_BASE_ADDR+0x44, (ReadReg32(SC_BASE_ADDR+0x44)&0xffffe0ff) | (2 << 8)); //12M cclk
    WriteReg32((SC_BASE_ADDR+0x130), (ReadReg32(SC_BASE_ADDR+0x130)&0xc1ffffff)|(d1_div<<25));//v2tod1  isp1_clk div=2 [29:25]
	WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x0083ffff)|BIT23|BIT31|(d1_div<<18)|((d1_div * 4)<<24));//d1_clk div=1
	WriteReg32(SC_BASE_ADDR+0x54,(REG32(SC_BASE_ADDR+0x54)&0xffffffe0) | BIT5|d1_div * 2);

	u8 fb_div = 0x8;
    WriteReg32((SC_BASE_ADDR+0x130), (ReadReg32(SC_BASE_ADDR+0x130)&0xfe000000)|fb_div|(fb_div<<5)|(fb_div<<10)|(fb_div<<15)|(fb_div<<20));//fb0~fb4 div r_fb_div[4:0];r_fb_div[9:5];r_fb_div[14:10];r_fb_div[19:15];r_fb_div[24:20]

#ifdef  CONFIG_LOWPOWER_EN
	WriteReg32(REG_SC_ADDR+0x130, (ReadReg32(REG_SC_ADDR+0x130)&0x3E000000)|(2<<30)|(31<<20)|(31<<15)|(31<<10)|(31<<5)|31);
	tick1_disable();
	extern t_ertos g_ertos_cfg;
	g_ertos_cfg.cycles_per_tick = clockget_cpu() / TICKS_PER_SEC;
	tick1_set_rt(g_ertos_cfg.cycles_per_tick);

	ddr_disable();
	ddrset_lpddr2_200M();
#endif

	return 0;
}


int ref_sfinit_fast_disable = 0;
int DxDoorCam_hw_early_init(void)
{
	clockconfig_360M_lp();
	uart_config(115200, UART_IER_RDI);
	ddr_disable();
	ddrset_ddr3_533M();

	if(ref_sfinit_fast_disable == 0)
	{
		g_sflash.sta_reg_qe_bit =0;	//do not set qe register to reduce boot time
		g_sflash.check_id_en =0;
	}
	return 0;
}

int DxDoorCam_hw_init_post(void)
{
	return 0;
}

#ifdef CONFIG_AUDIO_EN
int DxDoorCam_board_speaker_on(int enable)
{
	debug_printf("easycam_board_speaker_on enable=%d\n",enable);
	if(enable){//Speaker enable
		libgpio_config(PIN_GPIO_07, PIN_DIR_OUTPUT);
		libgpio_write(PIN_GPIO_07, PIN_LVL_HIGH);
	}else{
		libgpio_config(PIN_GPIO_07, PIN_DIR_OUTPUT);
		libgpio_write(PIN_GPIO_07, PIN_LVL_LOW);
	}

	return 0;
}
#endif

