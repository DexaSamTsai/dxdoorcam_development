/*a top level sf API based on driver sfc(sfc_flash.h) and sif flash(libsif_flash.h)*/
#ifndef _LIBSFLASH__H_
#define _LIBSFLASH__H_

/**
 * @defgroup lib_sflash
 * @ingroup lib_sflash
 * @brief this is top sflash API based on sfc flash and sif flash
 * @details SIF flash can be connected to two I/O: sif or sfc(sif flash controller). Corresoponding the two hardware, there are tow driver: sif_base.c and sif_flash_base.c. Based on hardware drivers, encapsulate flash operations in file: sif_flash_internal.c and sfc_flash_internal.c. Based on xxx_internal.c, sif_flash.c&sfc_flash.c include some initialization codes in share/driver.  To write unified read/write flash codes, encapsulate a top level API based on sif_flash.c and sfc_flash.c in this file. API user donot need concern which I/O to be used when call these APIs, just config the I/O source parameter when call sf_init(). <br><br>
<p>
The two kind of I/O flash use one same struct t_sflash. Corresponding to specified flash model, when use sif flash, please config flash parameter in file sf_sif_xxx.c. If use sfc flash, config flash parameter in struct t_libsfc_flash in file sfc_xxx.c.  More details please see below:  <br><br>
		*****************************************************************
		****                                                         ****
		****                       sflash.c                          ****
		****                                                         ****
		*****************************************************************
		****                        ******                           ****
		****      sif_flash.c       ******      sfc_flash.c          ****
		****                        ******                           ****
		*****************************************************************
		****                        ******                           ****
		**** sif_flash_internal.c   ******   sfc_flash_internal.c    ****
		****                        ******                           ****
		*****************************************************************
		****                        ******                           ****
		****      sif_base.c        ******     sfc_flash_base.c      ****
		****                        ******                           ****
		*****************************************************************

	<p>
	How to use: <br><br>
	Firstly config struct t_sflash, refer to existing specified flash model file sf_xxx_xxx.c, call sf_init(). Then API user can operate flash by these APIs.
 */

/*flash is connect to which I/O*/
typedef enum{
	SF_SOURCE_SIF0_P0, //shared with SCIF
	SF_SOURCE_SIF0_P1, //shared with SCIO
	SF_SOURCE_SIF0_P2, //shared with JTAG
	SF_SOURCE_SIF2_P0, //shared with BT1120
	SF_SOURCE_SIF2_P1, //shared with SNR0
	SF_SOURCE_SFC,
}SF_SOURCE;

/*erase type*/
typedef enum{
	ERASE_SECTOR,
	ERASE_BLOCK,
	ERASE_CHIP
}ERASE_TYPE;

typedef enum{
	DQ2_DATA_PIN = 0x0,
	DQ2_WP_PIN = 0x1,
	DQ2_Vpp_PIN = 0x2,
}DQ2_MODE;

typedef enum{
	DQ3_DATA_PIN = 0x0,
	DQ3_RESET_PIN = 0x1,
	DQ3_HOLD_PIN = 0x2,
}DQ3_MODE;

/**
 *SFC_SIF_MODE_cmd mode_addr mode_data mode
 *SFC_SIF_MODE_xxx_yyy_zzz, xxx: set command mode. yyy: address mode. zzz: data mode. For example, SFC_SIF_MODE_SINGLE_SINGLE_QUAD means when send command, use standard sif data pin, one DI and one DO, in address satage use standard sif data pin, in data stage use quad mode, 4 data pin.
    SINGLE --> standard sif, one DI on DO
	DUAL   --> dual sif, two data pin, half-duplex
	QUAD   --> quad sif, four data pin, half-duplex
 */
typedef enum{
	SFC_SIF_MODE_SINGLE_SINGLE_SINGLE	= 0x0,
	SFC_SIF_MODE_SINGLE_SINGLE_DUAL	= 0x1,
	SFC_SIF_MODE_SINGLE_DUAL_DUAL	= 0x2,
	SFC_SIF_MODE_DUAL_DUAL_DUAL	= 0x3,
	SFC_SIF_MODE_SINGLE_SINGLE_QUAD	= 0x5,
	SFC_SIF_MODE_SINGLE_QUAD_QUAD	= 0x6,// extend sif  with quad I/O in both addr and data stage
	SFC_SIF_MODE_QUAD_QUAD_QUAD	= 0x7,
}SFC_SIF_MODE;

//INTERNAL_START
/*sfc control flash mode, 5 mode in all.*/
typedef enum{
	SFC_MODE_STE = 0x0,// standard mdoe
	SFC_MODE_DAM = 0x1,// direct access mode 
	SFC_MODE_DMA = 0x2, // dma mode
	SFC_MODE_CSR = 0x3, //recommended when use non-dma mode
	SFC_MODE_LEGACY = 0x4,
}SFC_MODE;
//INTERNAL_END

/*0xAB means qe bit locate in bit B of status register A*/
typedef enum{
	STA_REG_NOQE = 0x0,
	STA_REG1_BIT0 = 0x10,
	STA_REG1_BIT1 = 0x11,
	STA_REG1_BIT2 = 0x12,
	STA_REG1_BIT3 = 0x13,
	STA_REG1_BIT4 = 0x14,
	STA_REG1_BIT5 = 0x15,
	STA_REG1_BIT6 = 0x16,
	STA_REG1_BIT7 = 0x17,

	STA_REG2_BIT0 = 0x20,
	STA_REG2_BIT1 = 0x21,
	STA_REG2_BIT2 = 0x22,
	STA_REG2_BIT3 = 0x23,
	STA_REG2_BIT4 = 0x24,
	STA_REG2_BIT5 = 0x25,
	STA_REG2_BIT6 = 0x26,
	STA_REG2_BIT7 = 0x27,
}STA_REG_CONFIG;

/*config flash command and sfc pin*/
typedef struct{
	/*config flash command*/
	u8 cmd_id; /* MUST */

	/*sfc flash params*/
	u32 addr_len; /* can be 0 */
	u8 dummy_cys; /* must <= 15, numbers of inner dummy after address stage, continuous_read_mode_bit is sended in dummy cycles */ //TODO add head dummy support
	u8 continuous_read_mode_bit; /*decide next read command if need command id. M5-4 = (1,0): continuous read mode.
								   Sended in dummy_cys*/
	/*config sfc*/
	SFC_MODE sfc_mode; ///< INTERNAL sfc control flash mode, 5 mode in all. STE, Direct access, DMA, CSR, Legacy. 
					   //For now only CSR and DMA mode
	SFC_SIF_MODE sfc_sif_mode; // sfc sif pin mode, 7 modes in all
	
	/*sif flash params*/
	u8 fast_read_en; //1: enable cmd_cur_rd use cmd_fast_read command
	u8 data_dummy_cycles; /*must < 4, unit byte, dummy in data stage*/
}t_sflash_cmd;

/*flash configure*/
typedef struct s_sflash{
	u32 clk; // must. set the clk of pin clk
	u32 module_clk; //internal. input clock of sif/sfc module
	s32 page_size;
	s32 sector_size;
	s32 block_size;
	s32 chip_size; //chip total size. unit: byte
	u32 dma_timeout;
	u32 timeout;
	u8 dma_en; /*if use sif flash, only read support dma mode. if use sfc flash, read&write both support dma mode*/
	u8 check_id_en;//0: no check flash id when initialize flash 1: check id 
	u32 deviceID;

	/*sfc flash params*/
	u8 sif_bauddiv; //TODO 0 clean
	u8 trans_mode; // BIT0: CPHA. BIT1:CPOL
	u8 xip_mode;
	u8 rx_delay_cycle; //delay x cycles after SCK sampling edge to get the RX bit from MISO
	u8 ce_active_setuptime; //ce: chip enable if Tsck/2 > tSLCH, the value is set to 0, else the value = (tSLCH - Tsck/2)/Tmclk;
	u8 ce_active_holdtime; //if Tsck/2 > tCHSH, the value is set to 0, else the vlaue = (tSLCH - Tsck/2)/Tmclk;
	u8 ce_inactivetime; // tSHSL/Tmclk + ce_active_holdtime + 1
	u8 DQ2_mode; // set DA2 to WP#/Vpp pin, when command donot use 4 data pin mode. If 4 data pin mode, this setting is invalid, driver will set it into data pin automatically
	u8 DQ2_value;// DQ2 value when pin is used as WP#/Vpp pin, the value is invalid when DQ2 is used as data pin
	u8 DQ3_mode; // set DA3 to reset#/hold# pin, when command donot use 4 data pin mode. If 4 data pin mode, this setting is invalid, driver will set it into data pin automatically
	u8 DQ3_value; // DQ3 value when pin is used as hold/reset, the vlaue is invalid when DQ3 is used as data pin
	
	/*status registers congfigure*/
	u8 sta_reg_num; /*the number of status register, must configure, initialize into 1 by default. support up to 3 status register*/
	/*status register bit config, config postion of these parms in status register according to flash chip datasheet*/
	STA_REG_CONFIG sta_reg_qe_bit; //quad enable 0: no qe bit
	/*if there is only one read/write status register command, only use libsfc_flash_read_status_reg1()/libsfc_flash_write_status_reg1()*/
	u8 sta_reg_readcmdnum; //the number of read sta reg command	
	u8 sta_reg_writecmdnum; //the number of read sta reg command

	s32 (*post_init)(void); /*can be NULL unless there are special init request for specail flash chip*/

	t_sflash_cmd cmd_deviceid; // must set corresponding cmd according to sfc mode
	t_sflash_cmd cmd_cur_rd; // must. which read cmd to use now
	t_sflash_cmd cmd_fast_read;
	t_sflash_cmd cmd_rd_sta_reg1;
	t_sflash_cmd cmd_rd_sta_reg2;
	t_sflash_cmd cmd_rd_sta_reg3;
	t_sflash_cmd cmd_write_sta_reg; //only for sif flash
	t_sflash_cmd cmd_write_sta_reg1;
	t_sflash_cmd cmd_write_sta_reg2;
	t_sflash_cmd cmd_write_sta_reg3;
	t_sflash_cmd cmd_clear_sta_reg;
	t_sflash_cmd cmd_rd_flag_sta_reg;
	t_sflash_cmd cmd_clear_flag_sta_reg;
	t_sflash_cmd cmd_rd_config_reg;
	t_sflash_cmd cmd_cur_write;
	t_sflash_cmd cmd_ext_difp;//extended dual input fast program
	t_sflash_cmd cmd_ext_qip;// extended quad input fast program
	t_sflash_cmd cmd_sec_erase;
	t_sflash_cmd cmd_block_erase;
	t_sflash_cmd cmd_chip_erase;
	t_sflash_cmd cmd_write_en;
	t_sflash_cmd cmd_write_dis;
	t_sflash_cmd cmd_powerdown_en; /*reduce standby current*/
	t_sflash_cmd cmd_powerdown_dis;

	t_libsif_cfg libsif_cfg_sif_flash; /*internal*/
	u32 sif_pinsel; /*internal*/

	/*callback*/
	void (*sif_delay_cb)(void); /*adjust dealy time after program/readid operation when use sif flash*/
	s8 (*libsfc_flash_write_status_reg1_cb)(u32 value);
	s8 (*libsfc_flash_write_status_reg2_cb)(u8 value);
	s8 (*libsfc_flash_write_status_reg3_cb)(u8 value);
	s32 (*libsfc_flash_read_status_reg1_cb)(void);
	s32 (*libsfc_flash_read_status_reg2_cb)(void);
	s32 (*libsfc_flash_read_status_reg3_cb)(void);

	char * name;
}t_sflash;
	
extern t_sflash g_sflash; /* deprecated, use 'p_sflash' after sf_detect() or sf_init() */
extern t_sflash * p_sflash; /* this can be used after sf_detect() or sf_init() */

//INTERNAL_START
typedef struct{
	s32 (*init)(t_sflash *sflash);
	s32 (*read)(u8 *data_out, u32 data_len, u32 flash_addr);
	s32 (*program)(u8* data_in, u32 data_len, u32 flash_addr);
	s32 (*write)(u8* data_in, u32 data_len, u32 flash_addr);
	s32 (*update)(u8* data_in, u32 data_len, u32 flash_addr);
	s32 (*erase)(u32 flash_addr, ERASE_TYPE erase_type);
	t_sflash * (*info)(void);
}SFLASH_FUNC;
extern SFLASH_FUNC g_sflash_fun_init;
//INTERNAL_END

/*
 * @ brief detect sflash is exist
 * @ param source SF_SOURCE_SIF: flash is connected to sif. SF_SOURCE_SFC: flash is connect to sfc. Before call this init function, if use SF_SOURCE_SIF, configure struct g_sf, if use SF_SOURCE_SFC, configure struct g_libsfc_flash.
 * @ return t_sflash, NULL: not found
 */
t_sflash * sf_detect(SF_SOURCE source);

/*
 * @ brief sf module initialize flash
 * @ param source SF_SOURCE_SIF: flash is connected to sif. SF_SOURCE_SFC: flash is connect to sfc. Before call this init function, if use SF_SOURCE_SIF, configure struct g_sf, if use SF_SOURCE_SFC, configure struct g_libsfc_flash.
 * @ param sflash_inner point to external global t_sflash variable, can be NULL.
 * @ return 0: ok non-0: error
 */
s32 sf_init(SF_SOURCE source, t_sflash *sflash_inner);
/*
 * @ brief sf module read flash 
 * @ param  data_out buffer for readed data. address no necessary to be 16-byte aligned, but in dma mode, must be 16-bytes aligned, or it will change into cpu mode internally.
 * @ param data_len length want to read. If use dma, have to be multiple of 16, or it will change into cpu mode internally. length can be arbitrary, but < chip size(if SF_SOURCE is SF_SOURCE_SFC, length < 2^28(512M))
 * @ param flash_addr flash start address to be written
 * @ return actually read length in byte unit. <0: error
 */
s32 sf_read(u8 *data_out, u32 data_len, u32 flash_addr);
/*
 * @ brief sf modeule write data to flash
 * @ param data_in buffer for writen data. If use dma, have to 16-byges algned, or it will change into cpu mode internally.
 if cpu mode, no necessary to be 16-byte aligned.
 * @ param data_len length want to write. If use dma, have to be multiple of 16, or it will cange into cpu mode intenally .In cpu mode, length < page size.
 * @ param flash_addr flash start address to be written
 * @ return actually read length in byte unit. <0: error
 */
s32 sf_program(u8* data_in, u32 data_len, u32 flash_addr);
/*
 * @ brief sf modeule write data to flash
 * @ details data length can be arbitrary. Sector writen will be erased internally. No need erase flash before this function. Maybe lost data due to secoter erase.
 * @ param data_in buffer for writen data. If use dma, have to 16-byges algned, or it will change into cpu mode internally. If cpu mode, no necessary to be 16-byte aligned.
 * @ param data_len length want to write. If use dma, have to be multiple of 16, or it will cange into cpu mode intenally .In cpu mode, length can be arbitrary. length < chip size.
 * @ param flash_addr flash start address to be written
 * @ return actually write length in byte unit. <0: error
 */
s32 sf_write(u8* data_in, u32 data_len, u32 flash_addr);
/*
 * @ brief deal with the case that cross-sector page programe and erase flash internally. No sector erase issue that exist in sf_write().
 * @ details This fucntion wonnot lost any data caused by sector erase issue. It recorded the data before erasing and then re-write.  data_len can be arbitrary, not be limited to page size
 * @ param data_in buffer for writen data. If use dma, have to 16-byges algned, or it will change into cpu mode internally. If cpu mode, no necessary to be 16-byte aligned.
 * @ param data_len length want to write. If use dma, have to be multiple of 16, or it will cange into cpu mode intenally .In cpu mode, length can be arbitrary. length < chip size.
 * @ param flash_addr flash start address to be written
 * @ return actually write length in byte unit. <0: error
 */
s32 sf_update(u8* data_in, u32 data_len, u32 flash_addr);

t_sflash * sf_info(void);

/*
 * @ brief sf module erase flash
 * @ param flash_addr: erase address 
 * @ param erase_type: \ref ERASE_TYPE 
 * @ return 0: ok <0: error
 */
s32 sf_erase(u32 flash_addr, u8 erase_type);

//INTERNAL_START
#define __sflash_cfg 		__attribute__ ((__section__ (".sflash_cfg")))

#define SFLASH_DECLARE_INTERNAL(sname) \
__sflash_cfg t_sflash g_##sname##_sflash = \
{ \
	.name = #sname,


#define SFLASH_DECLARE(sname) SFLASH_DECLARE_INTERNAL(sname)
//INTERNAL_END

#endif
