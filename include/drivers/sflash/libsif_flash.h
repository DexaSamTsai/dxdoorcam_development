#ifndef __LIBSIF_FLASH_H__
#define __LIBSIF_FLASH_H__

extern t_sflash *libsif_flash_inner;

t_sflash * sif_flash_info(void);
//INTERNAL_START
#define SIF_RW(x) libsif_rw((&(libsif_flash_inner->libsif_cfg_sif_flash)), x)
#define SIF_FLASH_CS_LOW do { WriteReg32((&(libsif_flash_inner->libsif_cfg_sif_flash))->base_addr, ReadReg32((&(libsif_flash_inner->libsif_cfg_sif_flash))->base_addr ) | (0x3 << 4)); } while(0)
#define SIF_FLASH_CS_HIGH do {volatile u32 to; to = 5; WriteReg32((&(libsif_flash_inner->libsif_cfg_sif_flash))->base_addr, ReadReg32((&(libsif_flash_inner->libsif_cfg_sif_flash))->base_addr) & ~(0x3 << 4)); while(to --); } while(0)

//void sf_delay(s32 timeout);
//s32 sf_wait_ready(s32 timeout);
//void sf_power_down(void);
//void sf_release_power_down(void);
//u16 sf_read_manufactID(u8 id_addr);
//s32 sf_init_internal(u8 detect);

/**
 * @brief init sif Flash
 *
 * @return 0:ok, !=0: error
 *
 */
s32 sif_flash_init(t_sflash *sflash);
/**
 * @brief read data from sif flash
 *
 * @param buf buffer address in RAM for read
 * @param len size to read
 * @param flash_addr flash offset to read
 *
 */
s32 sif_flash_read(u8 * buf, u32 len, u32 flash_addr);
/**
 * @brief program sif flash
 *
 * @param buf buffer address in RAM for program
 * @param len size to program. < page size
 * @param flash_addr flash offset to program
 *
 * @return 0:ok, !=0: error
 *
 */
s32 sif_flash_page_program(u8 * buf, u32 len, u32 flash_addr);
/*
 * @ brief sif modeule write data to flash
 * @ details deal with the case that cross-sector/cross page page programe. len can be arbitrary, not be limited to page size
 * @ param buf for writen data.
 * @ param data_len length want to write. Length can be arbitrary, but < chip size.
 * @ param flash_addr write start address
 * @ return actually write length in byte unit. <0: error
 */
s32 sif_flash_write(u8 * buf, u32 len, u32 flash_addr);
/* @ brief deal with the case that cross-sector page programe and erase flash internally. 
 * @ details This fucntion wonnot lost any data caused by sector erase issue. It recorded the data before erasing and then re-write.  data_len can be arbitrary, not be limited to page size
 * @ param  data_in no necessary to be 16-byte aligned, but in dma mode, have to be 16-bytes aligned.
 * @ param  data_len no necessary to be multiple of 16, but in dma mode, have to be multiple of 16. length < chip size
 * @ param  flash_addr arbitrary
 */
s32 sif_flash_update(u8 * buf, u32 len, u32 flash_addr);
/**
 * @brief erase sif flash
 *
 * @param flash_addr offset in flash
 * @param erase_type see ::ERASE_TYPE
 *
 * @return 0:ok, !=0: error
 *
 */
s32 sif_flash_erase(u32 flash_addr, ERASE_TYPE erase_type);

u8 sif_flash_read_deviceid(void);
u8 sif_flash_read_status_reg1(void);
u8 sif_flash_read_status_reg2(void);
s8 sif_flash_write_status_reg(u8 status_reg1, u8 status_reg2);

/** @} */ //end lib_sf

//INTERNAL_END
#endif  ///__SIF_H__
