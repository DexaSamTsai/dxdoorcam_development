/*libsfc_flash.h - sfc(sif flash controller) flash API*/
#ifndef __LIBSFC_FLASH_H__
#define __LIBSFC_FLASH_H__

t_sflash * sfc_flash_info(void);
/*
 * @ brief initialize sfc(sif flash controller) module
 */
s32 sfc_flash_init(t_sflash *sflash);
/*
 * @ brief sfc modeule read arbitrary length data from flash
 * @ details data length can be arbitrary, but < 2^28(512M). Unit: byte
 * @ param  data_out address no necessary to be 16-byte aligned, but in dma mode, must be 16-bytes aligned.
 * @ param  data_len no necessary to be multiple of 16. length can be arbitrary, but < chip size and < 2^28(512M).if use dma mode, must be multiple of 16, or it wiil change into cpu mode internally.
 * @ param  flash_addr arbitrary
 */
s32 sfc_flash_read(u8 *data_out, u32 data_len, u32 flash_addr);
/*
 * @ brief sfc modeule write data to flash
 * @ details programe at previously erased memory locations
 * @ param data_in if use dma mode, must be 16-bytes aligned, or it wiil change into cpu mode internally. if cpu mode, no necessary to be 16-byte aligned
 * @ param  data_len < page size. If use dma mode, length must be multiple of 16, or it will change into cpu mode internally.
 * @ return <0: failed >=0: The length have been read
 */
s32 sfc_flash_page_program(u8* data_in, u32 data_len, u32 flash_addr);
/*
 * @ brief sfc modeule write arbitrary length data to flash
 * @ details data length can be arbitrary. Writen sector will be erased internally. No need erase flash before this function. Maybe lost data due to secoter erase.
 * @ param  data_in no necessary to be 16-byte aligned. If use dma, have to 16-byges algned, or it will change into cpu mode internally.
 * @ param data_len length want to write. If use dma, have to be multiple of 16, or it will cange into cpu mode intenally .In cpu mode, length can be arbitrary. length < chip size.
 * @ param  flash_addr arbitrary
 * @ return <0: failed >=0: The length have been written
 */
s32 sfc_flash_write(u8* data_in, u32 data_len, u32 flash_addr);
/* 
 * @ brief deal with the case that cross-sector page programe and erase flash internally. 
 * @ details This fucntion wonnot lost any data caused by sector erase issue. It recorded the data before erasing and then re-write.  data_len can be arbitrary, not be limited to page size
 * @ param  data_in no necessary to be 16-byte aligned, but in dma mode, have to be 16-bytes aligned.
 * @ param  data_len no necessary to be multiple of 16, but in dma mode, have to be multiple of 16. length < chip size
 * @ param  flash_addr arbitrary
 * @ return <0: failed >=0: The length have been written
 */
s32 sfc_flash_update(u8* data_in, u32 data_len, u32 flash_addr);
/*
 * @ brief sfc moduel erase flash
 */
s32 sfc_flash_erase(u32 flash_addr, ERASE_TYPE erase_type);
/*
 * @ brief sfc module read flash id
 */
u8 sfc_flash_read_deviceid(void);
/*
 * @ brief  powerdown to reduce standby current
 */
s8 sfc_flash_powerdown_en(void);
/*
 * @ brief  release powerdown
 */
s8 sfc_flash_powerdown_dis(void);
#endif  ///__LIBSFC_FLASH_H__
