#ifndef _PARTITION_SF_INTERNAL_H_
#define _PARTITION_SF_INTERNAL_H_

typedef enum{
    //System main partition
    CONFIG_HW_HEADER=0,
    CONFIG_BOOTLOADER1,
    CONFIG_BOOTLOADER2,
    CONFIG_FW_AREA_1,
    CONFIG_FW_AREA_2,
    MAX_SYS_MAINPARTITION = CONFIG_FW_AREA_2,
    //DK main partition
    CONFIG_CALIBRATION_AREA,
    CONFIG_USER_CONFIG,
    MAX_DK_MAINPARTITION = CONFIG_USER_CONFIG,
    //Sub partition
    CONFIG_HCONF,
    CONFIG_LDC,
    CONFIG_SYSTEM_ENV,
    CONFIG_WIFI_PARAMETER,
    CONFIG_DKFRAMEWORK_CONF,
    CONFIG_DKFRAMEWORK_CONF_BK,
    CONFIG_DKFRAMEWORK_TEMP,
    CONFIG_DXDOORCAM_CONF,
    CONFIG_DXDOORCAM_CONF_BK,
    CONFIG_DXDOORCAM_TEMP,
    MAX_SUBPARTITION = CONFIG_DXDOORCAM_TEMP,
    MAX_MAINPARTITION = MAX_DK_MAINPARTITION,
    MAX_PARTITION = MAX_SUBPARTITION,
    //MAX_PARTITION = MAX_MAINPARTITION,
    DKMAINPARTITION_NUM = MAX_DK_MAINPARTITION - MAX_SYS_MAINPARTITION,
}t_partition_id;

typedef struct s_DK_PART_header{
	u32 magic_num;	
	u32 max_len;
	u32 crc;
}t_DK_PART_header; 

typedef struct s_sf_hdr{
    u32 magic_num;    //0
    u32 qs_id2;//4
    u32 rsvd0;//8
    u32 st_id2;//c
    u32 st_dst_addr;//10
    u32 rsvd1;//14
    u32 bl_src_addr;//18
    u32 bl_dst_addr;//1c
    u32 bl_len;//20
    u32 bl_crc;//24
    u32 sf_cfg[4];//28-37
    u32 bl2_src_addr;//38
    u32 bl2_dst_addr;
    u32 bl2_len;
    u32 bl2_crc;
    u32 sys_cfg;
    u32 fw_addr;
    u32 fw_len;
    u32 user_config_addr;
    u32 user_config_len;
    u32 fw2_addr;
    u32 fw2_len;
    u32 rsvpar01_addr;
    u32 rsvpar01_len;
    u32 cali_addr;
    u32 cali_len;
    u32 rsvpar02_addr;
    u32 rsvpar02_len;
    u32 hdr_crc;
}t_sf_hdr; 

typedef struct s_DKHW_partinfo{
    u32 magic_num;
    u32 offset;
    u32 len;
}t_DKHW_partinfo; 

typedef struct s_DKHW_header{
    u32 dk_magic_num;
    u32 dk_part_num;
    u32 crc;
    u32 dk_to_start_fwarea;
    t_DKHW_partinfo dk_partinfo[];
}t_DKHW_header; 

#endif
