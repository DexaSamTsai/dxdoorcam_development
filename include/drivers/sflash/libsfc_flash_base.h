/*libsfc_flash_base.h - sfc(sif flash controller) flash driver. Only support CSR and DMA mdoe for now*/
#ifndef _LIBSFC_FLASH_BASE__H_
#define _LIBSFC_FLASH_BASE__H_

extern t_sflash *libsfc_flash_inner;
t_sflash * libsfc_flash_info(void);
/*
 * @ brief initialize sfc(sif flash controller) module
 * @ param in_clk input clk of sfc module
 */
s32 libsfc_flash_init(u32 in_clk);
/*
 * @ brief sfc modeule write data to flash
 * @ details programe at previously erased(ffh) memory locations
 * @ param data_in if use dma mode, must be 16-bytes aligned, or it wiil change into cpu mode internally.
 * @ param  data_len < (2^12 -1) and < page size. If use dma mode, length must be multiple of 16, or it will change into cpu mode internally.
 */
s32 libsfc_flash_page_program(t_sflash_cmd *cmd, u8* data_in, u32 data_len, u32 flash_addr);
/*
 * @ brief sfc modeule read arbitrary length data from flash
 * @ details data length can be arbitrary, but < 2^28(512M). Unit: byte
 * @ param  data_out address no necessary to be 16-byte aligned, but in dma mode, must be 16-bytes aligned.
 * @ param  data_len no necessary to be multiple of 16. length can be arbitrary, but < chip size and < 2^28(512M).if use dma mode, must be multiple of 16, or it wiil change into cpu mode internally.
 * @ param  flash_addr arbitrary
 */
s32 libsfc_flash_read(t_sflash_cmd *cmd, u32 addr, u8 *data_out, u32 data_len);
/*
 * @ brief sfc moduel erase flash
 */
s32 libsfc_flash_erase(u32 addr, u8 erase_type);
/*
 * @ brief sfc module read flash status reg1, if only one read status register command, use this function to read all status register
 * @ return if multiple read status command, return value is reg1. if one command, but multiple regs, return vaule is reg1 | (reg2<<8) | (reg3<<16) | ...
 */
s32 libsfc_flash_read_status_reg1(void);
/*
 * @ brief sfc module read flash status reg2
 */
s32 libsfc_flash_read_status_reg2(void);
/*
 * @ brief sfc module read flash status reg3
 */
s32 libsfc_flash_read_status_reg3(void);
/*
 *@#brief write sfc flash status register1, if only one read status register command, use this function to read all status register
 *@param if multiple command, value is reg1. if one command, but multiple regs, vaule is reg1 | (reg2<<8) | (reg3<<16) | ...
 */
s8 libsfc_flash_write_status_reg1(u32 value);
/*
 *@#brief write sfc flash status register2 
 */
s8 libsfc_flash_write_status_reg2(u8 value);
/*
 *@#brief write sfc flash status register3
 */
s8 libsfc_flash_write_status_reg3(u8 value);
#endif
