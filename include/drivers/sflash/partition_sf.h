#ifndef _SF_PARTITION_H_
#define _SF_PARTITION_H_

#include "partition_sf_internal.h"

/**
 * @brief <b>get firmware offset and length</b>
 *
 * @param id2: firmware id
 * @param offset [out]: return firmware offset in serial flash
 *
 * @return firmware length, -1 for error.
 *
 */
int partition_get_fwlen_sf(int id2, u32 * offset);

/**
 * @brief <b>check partition in sflash</b>
 * @details check whether the partition in sflash is correct or not.
 *
 * @param void
 *
 * @return 0:ok, 1: old partition, <0: bad partition
 *
 */
int partition_check_sf(void);

/**
 * @brief <b>partition recovery</b>
 * @details recovery partition, copy firmware and para data from backup area.
 *
 * @param void
 *
 * @return 0:ok, <0: error
 *
 */
int partition_recovery_sf(void);

/**
 * @brief <b>Change boot partition</b>
 * @details Change boot partition.
 *
 * @param void
 *
 * @return 0:ok, <0: error
 *
 */
int partition_get_hwhead(t_sf_hdr* sf_hdr);

int partition_boot_area_change(void);

/**
 * @brief <b>partition second boot spif</b>
 * @details boot second firmware from serial flash.
 *
 * @param id2: second firmware id
 *
 * @return <0: failed. if success, function jump to new firmware, not return.
 *
 */
int partition_boot_sf(int id2);

/**
 * @brief <b>partition load spif</b>
 * @details load firmware from serial flash to ram
 *
 * @param id2: second firmware id
 * @param buf: destination buffer
 *
 * @return >0 load length, -1: error
 *
 */
int partition_load_sf(int id2, unsigned char *buf);

/**
 * @brief <b>partition load part spif</b>
 * @details load part of firmware from serial flash to ram.
 *
 * @param id2: second firmware id
 * @param buf: destination buffer
 * @param offset: offset in firmware
 * @param maxlen: max length to load
 *
 * @return >0 load length, -1: error
 *
 */
int partition_load_part_sf(int id2, unsigned char *buf, int offset, int maxlen);

/**
 * @brief <b>partition spif conf load</b>
 * @details load conf data from serial flash
 *
 * @param buf conf buf address
 * @param maxlen conf buf max length
 *
 * @return >=0:conf length read, <0: error
 *
 */
int partition_conf_load_sf(u8 * buf, u32 maxlen);

/**
 * @brief <b>partition spif conf save</b>
 * @details save conf data to serial flash
 *
 * @param buf conf buf address
 * @param len conf length
 *
 * @return 0:ok, <0: error
 *
 */
int partition_conf_save_sf(u8 * buf, u32 len);


/**
 * @brief <b>partition spif cali load</b>
 * @details load calibration data from serial flash
 *
 * @param buf cali buf address
 * @param maxlen cali buf max length
 *
 *
 * @return >=0:cali length read, <0: error
 *
 */
int partition_cali_load_sf(int id, u8 * buf);

/**
 * @brief <b>partition spif cali has</b>
 * @details has calibration area or not
 *
 * @param void
 *
 * @return <0: hasn't . 0: has
 *
 */
int partition_cali_has_sf(void);

/**
 * @brief <b>partition spif upgrade</b>
 *
 * @param buf upgrade firmware buffer address 
 * @param len upgrade firmware length
 *
 * @return 0:ok, -1: error
 *
 */
int partition_upgrade_sf(void *p_buf);

/**
 * @brief <b>partition bootloader upgrade</b>
 * FIXME: Please don't use this API, unless you are clear about what you are doing!!
 *
 * @param buf upgrade bootloader buffer address 
 * @param len upgrade bootloader length
 *
 * @return 0:ok, -1: error
 *
 */
int partition_upgrade_bl(void * buf, int size);

/**
 * @brief <b>partition res load sflash</b>
 * @details load resouce from serial flash
 *
 * @param group: group number
 * @param item: resouce item 
 * @param key: resouce key word 
 * @param buf: res buffer address 
 * @param skip: skip length 
 * @param maxlen: res buffer max length 
 * @param totallen: res totallen length
 *
 * @return >0:load size, <0: error
 *
 */
int partition_res_load_sflash(int group, int item, char * key, unsigned char * buf, int skip, int maxlen, int * totallen);
int partition_Get_Backup_FW(void * buf);//For Test Leon 20180417
int partition_write_sf(u32 offset, u8 * buf, u32 len);
int partition_read_sf(u32 offset, u8 * buf, u32 maxlen);

#endif	// _SF_PARTITION_H_
