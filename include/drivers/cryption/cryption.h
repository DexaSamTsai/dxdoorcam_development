#ifndef _CRYPTION_H_
#define _CRYPTION_H_

typedef enum{
	DES,
	AES,
	DES3_KEY2,
	DES3_KEY3,
}enum_crp_alg;
typedef unsigned char crp_alg; 
typedef enum{
	ECB=0,
	CBC,
	CFB,
	OFB,
	CTR,
}enum_crp_mode;
typedef unsigned char crp_mode;
typedef struct{
	 unsigned char		crp_dir;	//0:encryption  1:descryption
	 crp_alg 		alg;		//cryption algorithm
	 crp_mode	  	mode;		//cryption mode
	 unsigned int		key_bits;	//key bits,may be 64(DES),128(AES_128,DES3_KEY2),192(AES_192,DES3_KEY3),256(AES_256) bits,should set in app if use AES
	 unsigned char		fb_seglen;	//segment lenth for CFB/OFB,may be 1,8,64,128
	 unsigned char *	data_addr;	//cryption/descryption data address,as both source and dst address.offset value in DDR.(confirm with ASIC)
	 unsigned int		data_length;	//cryption/descryption data length,should multiple of 8 and less than 512KB
	 unsigned char *	key_addr;	//key address
	 int init_vector[16];	//inititial vector,use 0 as default

	 t_ertos_queuehandler * crp_evt;
	 int (* cb_irq_handler)(void *);
	 void *irq_arg;
}t_crp_config;

extern t_crp_config * crp_cfg;
/**********************
  supported mode:
  AES_128_ECB,AES_128_CBC,AES_128_CFB_1,AES_128_CFB_8,AES_128_CFB_128,AES_128_CTR,AES_128_OFB_128;
  AES_192_ECB,AES_192_CBC,AES_192_CFB_1,AES_192_CFB_8,AES_192_CFB_128,AES_192_CTR,AES_192_OFB_128;
  AES_256_ECB,AES_256_CBC,AES_256_CFB_1,AES_256_CFB_8,AES_256_CFB_128,AES_256_CTR,AES_256_OFB_128;
 
  AES_128_CFB_8:  AES-algorithm,128-key_length in bit,CFB-mode,8-fregment_length in bits

  DES_ECB,DES_CBC,DES_CFB_1,DES_CFB_8,DES_CFB_64,DES_OFB_1,DES_OFB_8,DES_OFB_64:key length:64 bits
  DES3_KEY2_ECB,DES3_KEY2_CBC,DES2_KEY2_CFB_1,DES3_KEY2_CFB_8,DES3_KEY2__CFB_64,DES3_LEY2_OFB_1,DES3_KEY2_OFB_8,DES3_KEY2_OFB_64:key length:128bits
  DES3_KEY3_ECB,DES3_KEY3_CBC,DES2_KEY3_CFB_1,DES3_KEY3_CFB_8,DES3_KEY3__CFB_64,DES3_LEY3_OFB_1,DES3_KEY3_OFB_8,DES3_KEY3_OFB_64:key length:192bits
 
  maybe can work in some other modes,but not tested.
**************************/

void crp_init(t_crp_config crp_config);
int enable_crp_irq(void);
void disable_crp_irq(void);

/**
 * @brief <b>config cryption</b>
 *
 * @param p_crp_config: t_crp_config
 *
 * @return 0: OK, -1: error
 *
 */
int crp_config(t_crp_config * p_crp_config);
void crp_start(void);

#endif

