#ifndef _LIBGPIO_H_
#define _LIBGPIO_H_

#define PIN_DIR_OUTPUT				0
#define PIN_DIR_INPUT				1
		
#define PIN_LVL_LOW					0
#define PIN_LVL_HIGH				1

/**
 * @details config gpio
 *
 * @param pin GPIO pin
 * @param dir ::PIN_DIR_OUTPUT, ::PIN_DIR_INPUT
 *
 * @return 0:ok, -1:err
 */
int libgpio_config(int pin, int dir);

/**
 * @details write gpio data
 *
 * @param pin GPIO pin
 * @param level ::PIN_LVL_LOW, ::PIN_LVL_HIGH
 *
 */
void libgpio_write(int pin, int level);

/**
 * @details read gpio data
 *
 * @param pin GPIO pin
 *
 * @return 0:PIN_LVL_LOW, 1:PIN_LVL_HIGH
 *
 */
int libgpio_read(int pin);
/**
 * @brief config interrupt. didnot start interrupt. Only GPIO from 00 to 63 support interrupt.
 *
 * @param useedge: 1:edge trigger. 0:level trigger
 * @param bothedge: only used in edge trigger. 1:trigger by both edge. 0:trigger by one edge
 * @param posedge: if edge trigger and not bothedge: 1: trigger by pos edge. 0: trigger by neg edge. if level trigger: 1: trigger high level. 0: trigger by low level.
 *
 * @return 0: OK, -1: error
 */
int libgpio_config_interrupt(int pin, int useedge, int bothedge, int posedge);

/**
 * @brief request interrupt.
 *
 * @param pin: gpio pin
 * @param handler: interrupt handler
 * @param name: interrupt name 
 * @param arg: param for interrupt handler 
 *
 * @return 0: OK, other: error
 */
int libgpio_irq_request(int pin, t_irq_handler handler, char * name, void * arg);

/**
 * @brief free requested interrupt.
 *
 * @param pin: gpio pin
 *
 * @return 0: OK, other: error
 */
int libgpio_irq_free(int pin);

/**
 * @brief disable gpio internal pulldown resistor
 *
 * @param pin: gpio pin, PIN_GPIO_00 <= pin <= PIN_GPIO_11
 *
 * @return 0: OK, other: error
 */
int libgpio_pulldown_disable(int pin);

/**
 * @brief enable gpio internal pulldown resistor
 *
 * @param pin: gpio pin, PIN_GPIO_00 <= pin <= PIN_GPIO_11
 *
 * @return 0: OK, other: error
 */
int libgpio_pulldown_enable(int pin);

/**
 * @brief disable all gpio internal pulldown resistor(GPIO00 ~ GPIO11)
 */
void libgpio_pulldown_disableall(void);

#define PIN_GPIO_00 0
#define PIN_GPIO_01 1
#define PIN_GPIO_02 2 
#define PIN_GPIO_03 3 
#define PIN_GPIO_04 4 
#define PIN_GPIO_05 5 
#define PIN_GPIO_06 6 
#define PIN_GPIO_07 7 
#define PIN_GPIO_08 8 
#define PIN_GPIO_09 9
#define PIN_GPIO_10 10
#define PIN_GPIO_11 11

#define PIN_GPIO_32 32
#define PIN_GPIO_33 33
#define PIN_GPIO_34 34
#define PIN_GPIO_35 35
#define PIN_GPIO_36 36
#define PIN_GPIO_37 37
#define PIN_GPIO_38 38
#define PIN_GPIO_39 39
#define PIN_GPIO_40 40
#define PIN_GPIO_41 41
#define PIN_GPIO_42 42
#define PIN_GPIO_43 43
#define PIN_GPIO_44 44
#define PIN_GPIO_45 45
#define PIN_GPIO_46 46
#define PIN_GPIO_47 47
#define PIN_GPIO_48 48
#define PIN_GPIO_49 49
#define PIN_GPIO_50 50
#define PIN_GPIO_51 51
#define PIN_GPIO_52 52
#define PIN_GPIO_53 53
#define PIN_GPIO_54 54
#define PIN_GPIO_55 55
#define PIN_GPIO_56 56
#define PIN_GPIO_57 57
#define PIN_GPIO_58 58
#define PIN_GPIO_59 59
#define PIN_GPIO_60 60
#define PIN_GPIO_61 61
#define PIN_GPIO_62 62
#define PIN_GPIO_63 63

#define PIN_GPIO_64 64
#define PIN_GPIO_65 65
#define PIN_GPIO_66 66
#define PIN_GPIO_67 67
#define PIN_GPIO_68 68
#define PIN_GPIO_69 69
#define PIN_GPIO_70 70
#define PIN_GPIO_71 71
#define PIN_GPIO_72 72
#define PIN_GPIO_73 73
#define PIN_GPIO_74 74
#define PIN_GPIO_75 75
#define PIN_GPIO_76 76
#define PIN_GPIO_77 77
#define PIN_GPIO_78 78
#define PIN_GPIO_79 79
#define PIN_GPIO_80 80
#define PIN_GPIO_81 81
#define PIN_GPIO_82	82
#define PIN_GPIO_83	83
#define PIN_GPIO_84	84
#define PIN_GPIO_85	85
#define PIN_GPIO_86	86
#define PIN_GPIO_87	87
#define PIN_GPIO_88 88
#define PIN_GPIO_89 89
#define PIN_GPIO_90 90
#define PIN_GPIO_91 91
#define PIN_GPIO_92 92
#define PIN_GPIO_93 93
#define PIN_GPIO_94 94
#define PIN_GPIO_95 95

#define PIN_GPIO_96 96
#define PIN_GPIO_97 97
#define PIN_GPIO_98 98
#define PIN_GPIO_2_00 96
#define PIN_GPIO_2_01 97
#define PIN_GPIO_2_02 98

#define PIN_GPIO_3_00 128
#define PIN_GPIO_3_01 129
#define PIN_GPIO_3_02 130

#endif
