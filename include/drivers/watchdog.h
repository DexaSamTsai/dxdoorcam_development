#ifndef _WATCHDOG_H_
#define _WATCHDOG_H_

/**
 * @details init watchdog
 * @return 0:OK.
 */
int watchdog_init(void);

/**
 * @details set watchdog timeout
 * @param timeout timeout value, CPU cycles. 0 means disable watchdog.
 * @return 0:OK.
 */
int watchdog_set(u32 timeout);

/**
 * @details feed watchdog, or said reset the watchdog timeout counter.
 * @return 0:OK.
 */
int watchdog_feed(void);

#endif
