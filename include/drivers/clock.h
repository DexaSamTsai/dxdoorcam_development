#ifndef _CLOCK_CONFIG_H_
#define _CLOCK_CONFIG_H_

unsigned int clockget_pll_sys(unsigned int base);
unsigned int clockget_pll_ext(unsigned int base);

unsigned int clockget_ddr(void);
unsigned int clockget_ddrpll_ext(void);

unsigned int clockget_syspll_isp(void);
unsigned int clockget_syspll_sys(void);

unsigned int clockget_mipitx(void);
unsigned int clockget_mipipll_ext(void);

unsigned int clockget_usbpll(void);

unsigned int clockget_cpu(void);
unsigned int clockget_cpubus(void);
unsigned int clockget_pram(void);
unsigned int clockget_mpu(void);
unsigned int clockget_mpubus(void);
unsigned int clockget_mpupm(void);
unsigned int clockget_bus(void); //these modules use bus clock: uart, 
unsigned int clockget_mbus(void); //these modules use mbus clock: scif ctrl, scio ctrl, nand ctrl, sccb, usbhost

unsigned int clockget_fb0(void);
unsigned int clockget_fb1(void);
unsigned int clockget_fb2(void);
unsigned int clockget_fb3(void);
unsigned int clockget_fb4(void);
unsigned int clockget_ecif0(void);
unsigned int clockget_ecif1(void);
unsigned int clockget_isp(void);
unsigned int clockget_d1(void);
unsigned int clockget_ispd1(void);
unsigned int clockget_d4(void);
unsigned int clockget_v2(void);
unsigned int clockget_ve(void);
unsigned int clockget_img(void);

unsigned int clockget_cclk(void);
unsigned int clockget_scifsys(void);
unsigned int clockget_sciosys(void);
unsigned int clockget_sfcsys(void);
unsigned int clockget_ls(void);

void clocksif0_cfg(int pll_en, int div);
void clocksif1_cfg(int pll_en, int div);
void clocksif2_cfg(int pll_en, int div);

int clock_print(int argc, char ** argv);

void r3_syspll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm);
void r3_ddrpll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm);
void r3_mipitxpll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm);

void clockconfig_360M(void);
void clockconfig_360M_lp(void);
void clockconfig_360M_scm(void);
void clockconfig_360M_3vs(void);
void clockconfig_360M_4M(void);
void clockconfig_360M_4M_multi(void);
void clockconfig_360M_4Mlp(void);
void clockconfig_360M_4M_nohdr(void);
void clockconfig_360M_4M_4689(void);
void clockconfig_360M_4M_4686(void);
void clockconfig_360M_4M_40fps(void);
void clockconfig_360M_dcpc(void);
void clockconfig_360M_2M(void);
void clockconfig_360M_2M_2736(void);
void clockconfig_360M_2Mlp(void);
void clockconfig_360M_9732(void);

#endif
