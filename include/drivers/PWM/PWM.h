#ifndef __PWM_H__
#define __PWM_H__

typedef struct pwm_config{
	int clock;	//clock source for PWM,0:32.768KHz,1:1 micro sencond as clk period,2:32 micro second,3:1 second
	int low_period;		//PWM lasts (low_period+1) periods for low level
	int high_period;	//PWM lasts (high_period+1) periods for high level
	int default_level;	//PWM start level,1:PWM starts with high level,0:PWM starts with low level
}t_pwm_config;

int pwm_config(int pwm_id, t_pwm_config * cfg); //config gpio_id work as PWM  
int pwm_start(int pwm_id);		//start PWM output
int pwm_stop(int pwm_id);		//stop PWM output 

#endif
