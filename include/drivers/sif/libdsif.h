#ifndef __LIBDSIF_H__
#define __LIBDSIF_H__

#include "libsif_internal.h"

typedef enum{
	DSIF_SIF0_P0, //shared with SCIF
	DSIF_SIF0_P1, //shared with SCIO
	DSIF_SIF0_P2, //shared with JTAG
	DSIF_SIF2_P0, //shared with BT1120
	DSIF_SIF2_P1, //shared with SNR0
}e_dsif_sif_if;

typedef u32 (*dsif_sif_cb)(u8* data, u32 size, u32* data_size, u32* ack_size);

typedef struct s_dsif_sif_cb{
	u8* 			cache_addr;
	u32 			cache_len;
	dsif_sif_cb 	cmd_cb;
	dsif_sif_cb 	data_cb;
	dsif_sif_cb 	ack_cb;
} t_dsif_sif_cb;

typedef enum
{
	DSIF_STATUS_IDLE = 0,	/* IDLE, waiting for GPIO_A high */
	DSIF_STATUS_CMD,		/* in CMD, GPIO_A is high, waiting for GPIO_A low and DMA done */
	DSIF_STATUS_WAITDATA,	/* before DATA, waiting for GPIO_A */
	DSIF_STATUS_DATA,		/* in DATA, GPIO_A is high, waiting for GPIO_A low and DMA done */
	DSIF_STATUS_WAITACK,	/* before ACK, waiting for GPIO_A */
	DSIF_STATUS_ACK			/* in ACK, GPIO_A is high, waiting for GPIO_A low and DMA done */
}e_dsif_status;

typedef struct s_dsif_cfg{
	//public config
	u8 			sif_if;
	u32 		sif_clk;
	u8 			sif_master; //0:slave other:master
	t_dsif_sif_cb	sif_cb;
	u8 			gpio_in;
	u8 			gpio_out;
#define DSIF_DBG_ERR		BIT0
#define DSIF_DBG_INFO		BIT1
#define DSIF_DBG_IRQ		BIT2
#define DSIF_DBG_LOOP		BIT3
#define DSIF_DBG_PROTOCOL	BIT4
	u32 		dbg_flag; //DSIF_DBG_XXX
	//timeout: data sif send/recv duration ticks
	u32			timeout_ticks;

	//private
	t_libsif_cfg 	sif_cfg;
	t_ertos_eventhandler thread_evt;
	pthread_mutex_t thread_mutex;
	e_dsif_status	status;
	int			dma_done;
	int 		gpio_v;
	u32			cmd_size;
	u32			data_size;
	u32			ack_size;
	u32			start_ticks;
	u8 			triger_in;
} t_dsif_cfg;

extern t_dsif_cfg * dsif_cfg;	//Only support one DSIF link now.

int libdsif_init(t_dsif_cfg* cfg);
int libdsif_resume(void);
int libdsif_suspend(void);
int libdsif_exit(void);

#define dsif_dbg(flag, msg...) do { \
	if(dsif_cfg) \
		if(flag & dsif_cfg->dbg_flag) \
			debug_printf(msg); \
} while (0)

#define dsif_dbg_valid(flag) (dsif_cfg && (flag & dsif_cfg->dbg_flag))

u32 dsif_align16(u32 x);
u32 dsif_align32(u32 x);
void dsif_dumpbuf(u8* buf, u32 len);
u8* dsif_malloc(u32 size);
void dsif_free(u8* ptr);

void dsif_dbg_enable(u32 flag);

#endif
