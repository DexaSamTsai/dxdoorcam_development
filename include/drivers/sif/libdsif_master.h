#ifndef __LIBDSIF_H__
#define __LIBDSIF_H__

typedef enum{
	DSIF_SIF0_P0, //shared with SCIF
	DSIF_SIF0_P1, //shared with SCIO
	DSIF_SIF0_P2, //shared with JTAG
	DSIF_SIF2_P0, //shared with BT1120
	DSIF_SIF2_P1, //shared with SNR0
}e_dsif_sif_if;

typedef u32 (*dsif_sif_cb)(u8* data, u32 size, u32* data_size, u32* ack_size);

typedef struct s_dsif_sif_cb{
	u8* 			cache_addr;
	u32 			cache_len;
	dsif_sif_cb 	cmd_cb;
	dsif_sif_cb 	data_cb;
	dsif_sif_cb 	ack_cb;
}__attribute__((packed)) t_dsif_sif_cb;

typedef struct s_dsif_cfg{
	u8 			sif_if;
	u32 			sif_clk;
	//0:slave other:master
	u8 			sif_master;
	t_dsif_sif_cb	sif_cb;
	u8 			gpio_in;
	u8 			gpio_out;
	u32 			dbg_flag;
	//timeout: data sif send/recv duration ticks
	u32			timeout_ticks;
}__attribute__((packed)) t_dsif_cfg;

u32 libdsif_init(t_dsif_cfg* cfg);
u32 libdsif_resume(void);
u32 libdsif_suspend(void);
u32 libdsif_exit(void);

//send notify to master and start thread loop to send/recv data
//data stages in sif_cb callback functions
u32 libdsif_master_notify(void);
//////////////////////////////////////////////////
typedef enum {
	DSIF_DBG_NONE = 		0x0,
	DSIF_DBG_ERR = 			0x1<<0,
	DSIF_DBG_INFO = 		0x1<<1,
	DSIF_DBG_IRQ = 			0x1<<2,
	DSIF_DBG_LOOP = 		0x1<<3,
	DSIF_DBG_PROTOCOL = 	0x1<<4,
} e_dsif_dbg_lib;

extern u32 dsif_dbg_flag;

#define dsif_dbg(flag,msg...) do { \
		if(flag &dsif_dbg_flag) \
			debug_printf(msg); \
} while (0)

#define dsif_dbg_valid(flag) (flag &dsif_dbg_flag)

void dsif_dbg_enable(u32 flag);

u32 dsif_align16(u32 x);
u32 dsif_align32(u32 x);

void dsif_dumpbuf(u8* buf, u32 len);
u16 _crc16_ccitt(const void *buf, u32 len);

u8* dsif_malloc(u32 size);
void dsif_free(u8* ptr);


#endif
