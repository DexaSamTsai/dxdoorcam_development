#ifndef _LIBSIF__H_
#define _LIBSIF__H_

#ifdef CONFIG_DSIF_MASTER_SIF
#include "libdsif_master.h"
#else
#include "libdsif.h"
#endif
#include "libsif_internal.h"

/**
 * @REAL_FORMAT #define SIF_MODE_SLAVE 
 *
 * @ingroup lib_sif
 *
 * @brief use sif as sif slave
 *
 */
//INTERNAL_START
#define SIF_MODE_SLAVE   0x0
//INTERNAL_END

/**
 * @REAL_FORMAT #define SIF_MODE_MASTER 
 *
 * @ingroup lib_sif
 *
 * @brief use sif as sif master 
 *
 */
//INTERNAL_START
#define SIF_MODE_MASTER  0x1
//INTERNAL_END

/**
 * @REAL_FORMAT #define SIF_OP_DMA 
 *
 * @ingroup lib_sif
 *
 * @brief use sif transfer data in cpu mode 
 *
 */
//INTERNAL_START
#define SIF_OP_DMA 0x1
//INTERNAL_END

/**
 * @REAL_FORMAT #define SIF_OP_CPU 
 *
 * @ingroup lib_sif
 *
 * @brief use sif transfer data in cpu mode 
 *
 */
//INTERNAL_START
#define SIF_OP_CPU 0x0
//INTERNAL_END

/*There are two sif controllers, sif0 and sif2. Sif0 has three group pins and sif2 has two groups*/
/* NOTE: sif pins don't need pinsel, it's the 1st priority if pinshare was set */
//TODO when switch pin of one same sif controller, for example, SIF0_USE_P0 to SIF0_USE_P1, must call exit function then switch

/*shared with SCIF*/
void libsif0_use_p0(void);
#define SIF0_USE_P0 do{ libsif0_use_p0(); }while(0)

/*shared with SCIO*/
void libsif0_use_p1(void);
#define SIF0_USE_P1 do{ libsif0_use_p1(); }while(0)
void libsif0_use_p1_nocs(void);
#define SIF0_USE_P1_NOCS do{ libsif0_use_p1_nocs(); }while(0)

/*shared with JTAG*/
void libsif0_use_p2(void);
#define SIF0_USE_P2 do{ libsif0_use_p2(); }while(0)

/*shared with BT1120*/
void libsif2_use_p0(void);
#define SIF2_USE_P0 do{ libsif2_use_p0(); }while(0)

/*shared with SNR0*/
void libsif2_use_p1(void);
#define SIF2_USE_P1 do{ libsif2_use_p1(); }while(0)

/**
 * @brief init serial interface0
 *
 * @param cfg \ref t_libsif_cfg, this cfg maps sif0 control address 
 * @param dma 0:not use dma \ref SIF_OP_CPU, 1:use dma \ref SIF_OP_DMA
 * @param master 0:work as serial interface Slave \ref SIF_MODE_SLAVE, 1: work as serial interface master \ref SIF_MODE_MASTER
 * @param clk serial interface clock want to use.
 *
 * @return 0:ok
 */
s32 libsif0_init(t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk);

/**
 * @brief init serial interface1
 *
 * @param cfg \ref t_libsif_cfg, this cfg maps sif2 control address
 * @param dma 0:not use dma \ref SIF_OP_CPU, 1:use dma \ref SIF_OP_DMA
 * @param master 0:work as serial interface Slave \ref SIF_MODE_SLAVE, 1: work as serial interface master \ref SIF_MODE_MASTER
 * @param clk serial interface clock want to use.
 *
 * @return 0:ok
 */
s32 libsif2_init(t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk);

/**
 * @brief write data by serial interface0 or serial interface1
 * @param cfg get from libsif0_init or libsif2_init
 * @param src_adr buffer address
 * @param len length to write
 * @return return <0 if write fail or return write data length if write successfully
 * @ingroup lib_sif
 */
s32 libsif_write(t_libsif_cfg *cfg, unsigned char *src_adr, u32 len);

/**
 * @brief read by serial interface0 or serial interface1.
 * @detail this func is blocking func. it only return when data received. 
 * @param cfg get from libsif0_init or libsif2_init
 * @param dst_adr buffer address
 * @param len length to read
 * @return return <0 if read fail or return read data length if read successfully
 * @ingroup lib_sif
 */
s32 libsif_read(t_libsif_cfg *cfg, unsigned char *dst_adr, u32 len);

/**
 * @details read and write one byte
 * @param cfg get from libsif0_init or libsif2_init
 * @param data data to write
 * @return data read
 */
u8 libsif_rw(t_libsif_cfg *cfg, u8 data);

/**
 * @brief set serial interface0 CS pin low.
 * @para void
 * @return void
 */
void libsif0_cs_low(void);
/**
 * @brief set serial interface0 CS pin high.
 * @para void
 * @return void
 */
void libsif0_cs_high(void);

/**
 * REAL_FORMAT libsif2_cs_low()
 * @brief set serial interface2 CS pin low.
 * @para void
 * @return void
 */
void libsif2_cs_low(void);

/**
 * REAL_FORMAT libsif2_cs_high()
 * @brief set serial interface2 CS pin high.
 * @para void
 * @return void
 */
void libsif2_cs_high(void);

/**
 * REAL_FORMAT libsif_cs_low(t_libsif_cfg* cfg)
 * @brief set serial interface CS pin low.
 * @para cfg t_libsif_cfg
 * @return void
 */
void libsif_cs_low(t_libsif_cfg *cfg);

/**
 * REAL_FORMAT libsif_cs_low(t_libsif_cfg* cfg)
 * @brief set serial interface CS pin low.
 * @para cfg t_libsif_cfg
 * @return void
 */
void libsif_cs_high(t_libsif_cfg *cfg);

//INTERNAL_START
extern int libsif0_use_gpio;
extern int libsif2_use_gpio;
//INTERNAL_END

#endif
