#ifndef __LIBGPIO_SIF_H__
#define __LIBGPIO_SIF_H__

void libsif_gpio_simu_init(t_libsif_cfg * cfg);
u8 libsif_gpio_rw(t_libsif_cfg *cfg, u8 val);
int libsif_gpio_write(t_libsif_cfg *cfg, unsigned char *src_addr, int len);
int libsif_gpio_read(t_libsif_cfg *cfg, unsigned char *dst_addr, int len);
void libsif_gpio_cs_low(t_libsif_cfg *cfg);
void libsif_gpio_cs_high(t_libsif_cfg *cfg);

void libsif0_gpio_simu_init(t_libsif_cfg *cfg);
void libsif0_gpio_cs_low(void);
void libsif0_gpio_cs_high(void);


#endif ///__LIBGPIO_SIF_H__
