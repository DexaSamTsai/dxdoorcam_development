#ifndef _LIBSIF_INTERNAL_H_
#define _LIBSIF_INTERNAL_H_

#define SIF_SS_MODE_1B   0x00  
#define SIF_SS_MODE_2B   0x01  
#define SIF_SS_MODE_4B   0x02  
#define SIF_SS_MODE_LOW  0x03  

#define SIF_INTR_FIFO_1B  0x00
#define SIF_INTR_FIFO_1W  0x01
#define SIF_INTR_FIFO_2W  0x02
#define SIF_INTR_FIFO_4W  0x03

#define SIF_TRANS_RT     0x00
#define SIF_TRANS_OT     0x01  /* transmit only */
#define SIF_TRANS_OR     0x02  /* receive only */
//#define SIF_TRANS_RT     0x03  /* receive and transmit*/

/**
 @ define sif module struct, internal use
 */
typedef struct{
	u32 base_addr;              /**< sif control base address */
    u8 op_mode;                 /**< sif mode: slave or master */ 
    u8 trans_mode;              /**< sif transmit mode */ 
    u8 cpol;                    /**< clock polarity */ 
    u8 cpha;                    /**< clock phase */ 
    u8 slsb;                    /**< shift lsb firstly */
    u8 div;                /**< divider  */
    u8 op_dma;                  /**< 0:cpu mode. 1:dma mode, if cannot do dma, use cpu. 2:dma mode, if cannot do dma, fail */
    u8 ss_mode;                 /**< the mode of ss signal */
    u8 endian;                  /**< data endian  0: little endia 1: big endian*/
    u8 intr_en;                 /**< enable sif interrupt */
    u8 intr_fifo_cnt;           /**< fifo count when interrupt occurs */
	u8 reserved0;
    s32 timeout;                /**< software timeout */  
	u32 dma_wait;				/**< 0:don't use dma wait(non-blocking) , >0:dma wait timeout(blocking) */
	void (*dma_irq_done_callback)(void); 	/**< dma transfer done callback, called in both blocking and non-blocking mode, but mostly used only in non-blocking mode */

	//private
	u32 dmaread_dstaddr;		/**< used for libsif_dma_readdone() */
	u32 dmaread_len;			/**< used for libsif_dma_readdone() */

	//gpio sif simulate config start
	u32 pin_cs;
	u32 pin_clk;
	u32 pin_mosi;
	u32 pin_miso;
	u32 delay;
#define WIRE_3    3
#define WIRE_4    4
	u8 bus_width;
#define LSB_FIRST 0
#define MSB_FIRST 1
	u8 data_seq;

	//private for gpio sif simulate
#define SIF_MODE0 0  ///<inactive: CS  is high. active: CS is low, write data to device at rising edge of clk and read data from device at falling edge of clk
#define SIF_MODE1 1   ///<inactive: CS is high. active: CS is low, write data to device at falling edge of clk and read data from device at rising edge of clk
#define SIF_MODE2 2  ///<inactive: CS is low. active: CS is high. write data to device at rising edge of clk and read data from device at falling edge of clk
#define SIF_MODE3 3  ///<inactive: CS is low. active: CS is high. write data to device at falling edge of clk and read data from device at rising edge of clk
	u8 sif_mode;
	u8 nocs;
}t_libsif_cfg;

void libsif_hw_init(t_libsif_cfg *cfg); //INTERNAL

#endif
