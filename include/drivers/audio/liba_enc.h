/** 
 * @file    R3_fw/libmpu_linux/include/liba_enc.h
 * @brief   headers for Audio routines
 *
 * This header contains all Audio encoding related routines to interface with MPU.
 *
 *
 * @date    2016/12/15
 */
/** 
 * @defgroup        libaudio	AUDIO API Group
 */
#ifndef __LIBAENC_H__
#define __LIBAENC_H__

#include "liba_common.h"

/**
 * @brief					init audio encoding
 *
 * @param[in]				inp: parameters for initializing audio encoder
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libaenc_init(t_arec_inpara *inp);

/**
 * @brief					start audio encoding
 *
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libaenc_start(void);

/**
 * @brief					stop audio encoding
 *
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libaenc_stop(void);

/**
 * @brief					clear all buffer used by audio encoding
 *
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libaenc_exit(void);

/**
 * @brief					get one available encoded audio frame from linklist
 *
 * @param[in]	timeout		wait time if buffer empty
 * @return					frame pointer, NULL if no new frame.
 * @ingroup					libaudio
 */
FRAME * libaenc_get_frame(u32 timeout);

/**
 * @brief					remove one frame from audio encoded linklist
 *
 * @param[in]	frm			the return pointer from libaenc_get_frame
 * @return					void
 * @ingroup					libaudio
 */
void libaenc_remove_frame(FRAME *frm);

/**
 * @brief					set sample rate when aenc is running
 *
 * @param[in]	sr			sample rate
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libaenc_set_sr(s32 sr);

/**
 * @brief					return current audio stream timestamp
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_timestamp(void);

/**
 * @brief					get skipped audio packet in ms
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_droppedms(void);

/**
 * @brief					get total audio frame number recorded into the frame list
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_totfrmsintostreambuf(void);

/**
 * @brief					get total audio frame number get from the frame linklist
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_totfrmsfromstreambuf(void);

/**
 * @brief					get total buffer size of frame linklist
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_totstreambufsz(void);

/**
 * @brief					get used buffer size in frame linklist
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_usedstreambufsz(void);

/**
 * @brief					set dropping frame mode, drop latest or oldest
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_set_dropfrmmode(E_DROPFRM_MODE mode);

/**
 * @brief					get current dropping frame mode, drop latest or oldest
 *
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_get_dropfrmmode(void);

/**
 * @brief					set the duration of old frame that saved in linklist	
 *
 * @param[in]	dur_ms	    the duration in ms
 * @return					u32
 * @ingroup					libaudio  
 */
u32 libaenc_set_listfrmdur(u32 dur_ms);

/**
 * @brief					get current duration of old frame that saved in linklist	
 *
 * @return					u32 time duration in ms, if 0, set default frame list number
 * @ingroup					libaudio  
 */
u32 libaenc_get_listfrmdur(void);

#define ADATA_FLAG_FIRSTPKT         BIT0
#define ADATA_FLAG_LASTPKT          BIT1

typedef struct _AENC_DATA
{
    u8 *pAddr;
    u32 thisLen;
    u32 frameLen;
    u32 frameType;
    u32 timeStamp;

    u8 *pRel;
}AENC_DATA;

int libaenc_get_data(AENC_DATA *p_data, int maxlen, u32 timeout);

int libaenc_remove_data(u8 *p_rel);

#endif /// __LIBAENC_H__
