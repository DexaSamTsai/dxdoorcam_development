/** 
 * @file    R3_fw/libmpu_linux/include/liba_dec.h
 * @brief   headers for Audio routines
 *
 * This header contains all Audio decoding related routines to interface with MPU.
 *
 *
 * @date    2017/3/29
 */
#ifndef __LIBADEC_H__
#define __LIBADEC_H__

#include "liba_common.h"


/**
 * @brief init audio decoding
 *
 * @para[in]	inp			parameters for initializing audio decoder
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup                 libaudio
 */
int libadec_init(t_adec_inpara *inp);

/**
 * @brief					start audio decoding
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libadec_start(void);

/**
 * @brief					stop audio decoding
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libadec_stop(void);

/**
 * @brief					clear all buffer used by audio decoding
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libaudio
 */
int libadec_exit(void);

/**
 * @brief					get a new frame
 * @param[in]	timeout		wait time if decoder linklist is not available
 * @return					rame pointer, NULL if failed.
 * @ingroup                 libaudio
 */
FRAME * libadec_get_frame(u32 timeout);

 /**
 * @brief					add one frame to audio decoding linklist
 * @param[in]	frm			the return pointer from libadec_get_frame
 * @retval		void
 * @ingroup					libaudio
 */
void libadec_add_frame(FRAME* frm);

 /**
 * @brief					set EQ parameter
 * @param[in]	settings	the EQ parameters, refer to t_eq_settings structure
 * @param[in]	sr			current sample rate
 * @retval		0			OK
 * @retval		<0			Failed
 * @ingroup					libaudio
 */
s32 libadec_setEQ(t_eq_settings *settings, int sr);
#endif /// __LIBDEC_H__
