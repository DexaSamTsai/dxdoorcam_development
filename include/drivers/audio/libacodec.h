#ifndef __AUDIO_CODEC_H__
#define __AUDIO_CODEC_H__

/**
 * @defgroup libacodec
 * @ingroup lib_audio
 * @brief Externer audio CODEC driver setting and API
 * @details  This library defines the API for OV788 to control and capture or playback audio data through external audio CODEC. The audio CODEC's I2C and I2S should be connected to OV788's SCCB and Audio Interface(AI) respectively.OV788 AI can work in master or slave mode,correspondingly CODEC's I2S work in slave or master mode. The master will output lrck and bclk clock, and the slave accept them. OV788 can output MCLK to audio CODEC. 
 * -# <b>Source Codes</b>
   * -# /share/drivers/audio/codec/xxxx.c 
 * -# <b>Guide to support new CODEC</b>
   * -# Add new CODEC driver
     * -# All CODEC settings are in share/drivers/audio/codec/,named xxxx.c(xxxx is the codec name)
     * -# To add new CODEC xxxx,make a copy of existing codec setting and rename it to xxxx.c
	 * -# In xxxx.c file,implement codec_xxx_init() and codec_xxx_ioctl()and use them to initialize g_libacodec_setting.
	 * -# AI_CTL_CHN,AI_CTL_SR,AI_CTL_ADC_DVOL or AI_CTL_DAC_DVOL and AI_CTL_PWRDWN control command must be implemented in the new CODEC driver.
	 * -# In xxxx.c, need implement xxxx_playback_init() for audio playback,implement xxxx_recoording_init() for audio recording, and implement xxxx_duplex_init() for audio duplex case scenario(both playback and recording are required at the same time). 
   * -# Add new CODEC in Kconfig for configuraion
	 * -# In share/drivers/audio/codec/Kconfig,add AUDIO_CODEC_EN_xxxx as below:
\code
choice
	prompt "Audio external CODEC to link"
	help
	  select the CODEC

	config AUDIO_CODEC_EN_wm8955
		bool "wm8955 CODEC"
	....
	config AUDIO_CODEC_EN_xxxx
		bool "XXXX CODEC"			
endchoice
\endcode

 * -# <b>Sample code to use CODEC</b><p></p> 
 	<h2> 1). Link CODEC into application  </h2>
     	In 'menuconfig'->"Hardware configuration"->"Audio configuration"->"Audio Codec support", Please select "Audio Codec enalbe" and choose correct CODEC type(eg:wm8960)
	 	\code
     	[*] Audio Codec enable
            Audio external CODEC to link (wm8960 CODEC)  --->
	 	\endcode

 	<h2> 2). Playback  </h2>
\code
static void aplay_codec_init(void)
{
	s32 tmp;
	u32 codec_master = p_liba_dcfg->ai_master?0:1;
	if(p_liba_dcfg->duplex)
		g_libacodec_setting.init(AI_DIR_DUPLEX,codec_master);
	else
		g_libacodec_setting.init(AI_DIR_PLAY,codec_master);
	tmp = p_liba_dcfg->achannels;
	g_libacodec_setting.ioctl(AI_CTL_CHN,&tmp);
	if(codec_master){
		tmp = p_liba_dcfg->asample_rate;
		g_libacodec_setting.ioctl(AI_CTL_SR,&tmp);
	}
	tmp = p_liba_dcfg->cur_vol;
	p_liba_dcfg->cur_vol = g_libacodec_setting.ioctl(AI_CTL_DAC_DVOL,&tmp);
	debug_printf("p_liba_dcfg->cur_vol = %d\n",p_liba_dcfg->cur_vol);	
}

static void aplay_codec_powerdown(void)
{
	g_libacodec_setting.ioctl(AI_CTL_PWRDWN,0);
}

\endcode
 	<h2> 3). Recording </h2>
\code
void arec_codec_init(void)
{
	s32 tmp;
	u32 codec_master = p_liba_ecfg->ai_master?0:1;
	g_libacodec_setting.init(AI_DIR_RECORD,codec_master);
	tmp = p_liba_ecfg->achannels;
	g_libacodec_setting.ioctl(AI_CTL_CHN,&tmp);
	if(codec_master){
			tmp = p_liba_ecfg->asample_rate;
			g_libacodec_setting.ioctl(AI_CTL_SR,&tmp);
	}
	p_liba_ecfg->adc_plus =
			g_libacodec_setting.ioctl(AI_CTL_ADC_DVOL,&p_liba_ecfg->adc_plus);	
}

void arec_codec_powerdown(void)
{
	g_libacodec_setting.ioctl(AI_CTL_PWRDWN,0);
}

\endcode
 	<h2> 4). Duplex    </h2>
\code
static void aduplex_codec_init(void)
{
	s32 tmp;
	u32 codec_master = p_liba_dcfg->ai_master?0:1;
	g_libacodec_setting.init(AI_DIR_DUPLEX,codec_master);
	tmp = p_liba_dcfg->achannels;
	g_libacodec_setting.ioctl(AI_CTL_CHN,&tmp);
	if(codec_master){
		tmp = p_liba_dcfg->asample_rate;
		g_libacodec_setting.ioctl(AI_CTL_SR,&tmp);
	}
	tmp = p_liba_dcfg->cur_vol;
	p_liba_dcfg->cur_vol = g_libacodec_setting.ioctl(AI_CTL_DAC_DVOL,&tmp);
	p_liba_ecfg->adc_plus =
		g_libacodec_setting.ioctl(AI_CTL_ADC_DVOL,&p_liba_ecfg->adc_plus);	
}

static void aduplex_codec_powerdown(void)
{
	g_libacodec_setting.ioctl(AI_CTL_PWRDWN,0);
}

\endcode
 * @{
 */
 
typedef enum{
	AI_DIR_PLAY,  ///<AI data direction, playback(OV788->CODEC)
	AI_DIR_RECORD,///<AI data direction, recording(CODEC->OV788)
	AI_DIR_DUPLEX,///<AI data direction, duplex(CODEC<->OV788)
}num_E_AI_DIR;
typedef unsigned int E_AI_DIR;

/**
Some CODEC maybe has in-built equlizer,so define these sound effect type
*/
typedef enum {
	DEFAULT,
	STEREO_LIVE,
	CLASSICAL,
	DANCE,
	DISCO,
	HALL,
	HEAVY,
	POP,
	ROCK,
	SOFT,
	SOUL_JAZZ,
}E_AI_EQTYPE; 

/**
For configuring audio codec data path, choose mic and speaker
*/
#define CODEC_PATH_SPKL    0
#define CODEC_PATH_SPKR    1
#define CODEC_PATH_SPKS    2
#define CODEC_PATH_MICL    3
#define CODEC_PATH_MICR    4

 
/**
 *CODEC can adjust digital and analog(PGA) volume,but has different volume range, this struct represent its digital volume range in dB
*/
typedef struct s_codec_volume_info
{
	s32 min;   ///<the minimum volume in dB
	s32 max;   ///<the maximum volume in dB
}t_codec_volume_info;
 
/**
 *This enum defines the IO control type used by int (*ioctl)(E_AI_IOCTL ioctl,void *arg); void *arg is the parameter for the ioctl command. 
 *       for each enum type, the arg point to different struct and have different meaning.
 *	     new enum member can be added if required.
*/
typedef enum
{
	AI_CTL_CHN = 0,    ///< set channels,     1-mono,2-stereo
	AI_CTL_SR,         ///< set sample rate,  supported sample rate range is 8000Hz ~ 48000Hz
	AI_CTL_ADC_DVOL, 	   ///< set digital volume in dB. the volume range is g_libacodec_setting.dvol.min ~ g_libacodec_setting.dvol.max, if the set volume exceed the range of the CODEC, default to the supported minimum or maximum volume,eg: WM8978 has a digital volume range of 0 ~ -127 dB, if set 6dB or -130dB, it actually sets 0 or -127dB  
	AI_CTL_DAC_DVOL, 	   ///< set digital volume in dB. the volume range is g_libacodec_setting.dvol.min ~ g_libacodec_setting.dvol.max, if the set volume exceed the range of the CODEC, default to the supported minimum or maximum volume,eg: WM8978 has a digital volume range of 0 ~ -127 dB, if set 6dB or -130dB, it actually sets 0 or -127dB  
	AI_CTL_ADC_AVOL,       ///< set analog volume in dB. the volume range is g_libacodec_setting.avol.min ~ g_libacodec_setting.avol.max, if the set volume exceed the range of the CODEC, default to the supported minimum or maximum volume
	AI_CTL_DAC_AVOL,       ///< set analog volume in dB. the volume range is g_libacodec_setting.avol.min ~ g_libacodec_setting.avol.max, if the set volume exceed the range of the CODEC, default to the supported minimum or maximum volume
	AI_CTL_MUTE_DAC,      ///< set mute or not, 1:mute, 0:de-mute 
	AI_CTL_MUTE_ADC,      ///< set mute or not, 1:mute, 0:de-mute 
	AI_CTL_PWRDWN,    ///< power down, null arg
	AI_CTL_EQ,         ///< equalizer, equalizer type is defined as E_AI_EQTYPE
	AI_CTL_TRIS_ADCDAT, ///< tristats ADCDATA pin so that it will not affect the use of DM
	AI_CTL_PATH ///< control input/output
}E_AI_IOCTL; 

typedef struct s_codec_ctrl_para{
	u32 ch;          ///< audio channels
	u32 sr;          ///< audio sampling rate. support 8K,11K,12K,16K,22K,24K,32K,44K,48K
	s32 master;     ///< codec ai working in master mode
	s32 dac_dvol;   ///<digital volume,in dB
	s32 adc_dvol;   ///<digital volume,in dB
	E_AI_DIR dir;
}t_codec_cpara;

 
typedef struct s_libacodec_setting
{
  t_codec_volume_info da_dvol; ///< digital volume information
  t_codec_volume_info ad_dvol; ///< digital volume information
  t_codec_volume_info da_avol; ///< analog volume information
  t_codec_volume_info ad_avol; ///< analog volume information
  int (*early_init)(void); ///< use dir to choose playback,record or duplex 
  int (*init)(t_codec_cpara *cp); ///< use dir to choose playback,record or duplex 
  int (*ioctl)(E_AI_IOCTL ioctl, void *arg);  ///< the *arg parameter have different meaning for different ioctl command.
}t_libacodec_setting;


/**
* @brief <b>early init libacodec</b>
* @details early init func, for some codec, they need early init.
*
* @param void
*
* @return void
*
*/
void libacodec_earlyinit(void);

/**
* @brief <b>init libacodec</b>
* @details init func, 
*
* @param cp: audio codec parameters
*
* @return void
*
*/
void libacodec_init(t_codec_cpara *cp);

/**
* @brief <b>set acodec dac volume</b>
*
* @param vol: the audio dac volume in dB
*
* @return void
*
*/
void libacodec_set_dacvol(s32 vol);

/**
* @brief <b>set acodec adc volume</b>
*
* @param vol: the audio adc volume in dB
*
* @return void
*
*/
void libacodec_set_adcvol(s32 vol);

/**
* @brief <b>mute acodec adc</b>
*
* @param mute: 0-unmute  NZ-mute
*
* @return void
*
*/
void libacodec_mute_adc(s32 mute);

/**
* @brief <b>mute acodec dac</b>
*
* @param mute: 0-unmute  NZ-mute
*
* @return void
*
*/
void libacodec_mute_dac(s32 mute);

/**
* @brief <b>set codec sample rate</b>
*
* @param sr: sample rate to set
*
* @return void
*
*/
void libacodec_set_sr(s32 mute);



/**
* @brief <b>power down audio module</b>
*
* @param void 
*
* @return void
*
*/
void libacodec_pwrdown(void);
/** @} */ //end libacodec

#define __acodec_cfg 		__attribute__ ((__section__ (".acodec_cfg")))

#endif
