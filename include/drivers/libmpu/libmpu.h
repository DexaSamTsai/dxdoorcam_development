#ifndef __LIBMPU_H__
#define __LIBMPU_H__

#include "mpu_sys.h"

/**
 * @defgroup lib_dualCPU
 * @ingroup lib_engine
 * @brief Library for communication between the two CPUs.
 * @details <p></p> 
 	<h2> 1. Introduction </h2>
	<ul>
 	<li> There are two CPUs in ov788, the master(ARM A5) and the slave(BA22).Audio,video and ISP will run in BA22, OS(linux or RTOS) will run on ARM A5.
 	<li> There are three layers defined for dual CPU communication, this library is the layer2 in the the dual CPUs communication framework.
		<ul>
 		<li>	L1: layer 1, the hardware link layer, the bottom level driver for mailbox
 		<li>	L2: layer 2, the middle common layer,  provide API for Layer 3.
 		<li>	L3: layer 3, app layer, the application specific layer.(eg: audio,video,ISP).
		</ul>
	</ul>
	<h2> 2. The layer1 </h2>
	<ul>
	<li> 1) Mailbox
		<ul>
		<li>Mailbox is a hardware module in OV788 for communication between BA22 and ARM A5.
		<li>ARM A5 can send data to BA22 , and generate an interrupt to BA22, BA22 can read the data  to clear the interrupt.
		<li>BA22 also can send data to ARM A5 and generate an interrupt to ARM A5, ARM A5 read the data to clear the interrupt.     
		</ul>
	<li> 2) Shared Memory
		<ul>
		<li>The shared common memory can be accessed by both BA22 and ARM A5. So it can be used for data exchange between the two CPUs. The shared memory should be uncacheable(set the MSB bit of the data address as 1).		
		</ul>
	<li> 3) Interface
		<ul>
		<li> int libsec_start(void): Enable BA22 clock, set BA22 base address and start BA22
		<li> int libsec_stop(void)a: Stop BA22
		<li> void libmbx_m2s(u32 data): ARM A5 send data to BA22,  trigger an interrupt to BA22
		<li> u32 libmbx_m_read(void):   
	        ARM A5 read the data sent by BA22 using libmbx_s2m(), and clear the 	interrupt triggered by BA22
		<li> void libmbx_s2m(u32 data): 
		    BA22 send data to ARM A5,  trigger an interrupt to ARM A5
		<li> u32 libmbx_s_read(void) :   
		    BA22 read the data sent by ARM A5 using  libmbx_m2s(), and clear the 	interrupt triggered by ARM A5
		</ul>
	</ul>
	<h2> 3. The layer2 </h2>
		<ul>
		<li> This layer implement the command and signal protocol between ARM A5 and BA22,  provide APIs for layer3 audio and video application . Video or audio main functions runs in ARM A5 scheduled by RTOS or Linux, it send command to BA22 to initialize, start, stop audio/video engine run in BA22; audio/video engine in BA22 also can send signal to ARM A5 to share some information. 
		</ul>
	<h2> 4. The layer3 </h2>
		<ul>
		<li>	The layer3 is the implementation of  application, like audio and video encoder. There should be two parts of code for an application, the BA22 part and ARM A5 part.
  		<li>	The application entry and flow control are in ARM A5 part , it calls mpu_cmd() to control BA22, BA22 handle the command, then put result in dcpu_cmd.cmd_ret.
 		<li>	BA22 part code should be located in OV788_fw/engine/ directory.   For each application, a t_ba2app_cfg type structure variable must be defined; its app_cmdhandler and app_process must be implemented, however its app_inthandler is not necessary, if no interrupt need be handled. Below is an example for NEWAPPNAME:  
			\code
			typedef enum{
				BA2_APP_SAMPLE,
				BA2_APP_APLAY, 
				BA2_APP_AREC, 
				....
				BA2_APP_NEWAPPNAME
			}E_BA2_APP;

			__ba2app_cfg t_ba2app_cfg g_ba2app_newappname_cfg = {
				.apptype = BA2_APP_NEWAPPNAME,
				.app_cmdhandler = newappname_handle_cmd,
				.app_inthandler = newappname_int_handler,
				.app_process = newappname_process,
			};
			\endcode
	 	<li>	For new application that need run in BA22,its new commands(ARM A5->BA22) should be added to enumerator E_DCPU_CMD, and its new signals(BA22->ARM A5) should be added to  enumerator E_DCPU_SIG.
			\code
			typedef enum{
				DCPUCMD_APLAY_GCFGADDR,
				DCPUCMD_APLAY_INIT,  
				....
				DCPUCMD_NEWAPPNAME_A,
				DCPUCMD_NEWAPPNAME_B,
			}E_DCPU_CMD;

			typedef enum{
				DCPUSIG_SAMPLE,  ///<signal for example
				....
				DCPUSIG_NEWAPPNAME_1,
				DCPUSIG_NEWAPPNAME_2,
			}E_DCPU_SIG;
			\endcode
 		<li>	For xxx_handle_cmd(), it should defined as below(need a default branch to return 0):
			\code
			static u32 newappname_handle_cmd(u32 cmdID,u32 param_addr)
			{
				u32 ret;
				switch(cmdID)
				{
				case DCPUCMD_NEWAPPNAME_A:
					ret = ...
					return ret;

				case DCPUCMD_NEWAPPNAME_B:
					ret = ...
					return ret;

				default:
					return 0;
				}
			}

			\endcode
		</ul>
 * @{
 */

#include "mpu_cmd.h"

/**
 * @brief 启动mpu
 * @param  void
 * @return void
 */
int mpu_start(void);

/**
 * @brief 停止mpu
 * @return void
 */
void mpu_stop(void);

/**
 * @brief 释放mpu
 * @return 0
 */
int mpu_release(void);

/**
 * @brief 向mpu发送命令，timeout为超时。
 * @details In blocking mode, ARM A5 will waiting for the response from BA22 until timeout. if timeout is 0, never timeout and it will continuously waiting for the response.
 * @param cmdID: command ID 
 * @param arg: parameter address used for the command if needed 
 * @return an address that contains data returned by the command  
 */
int mpu_cmd(u32 cmd, u32 arg);

/**
 * @brief 初始化mpu
 */
int mpu_init_fromarray(void);

/**
 * @brief init mpu from sf
 * @param id2 id2 in sf
 */
int mpu_init_from_sf(int id2);

/**
 * @brief init mpu from uart xmodem
 */
int mpu_init_from_uart(void);

/**
 * @breif 注册mpu的事件处理接口，当mpu有新事件时，会发送到evt
 */
int mpu_event_reg(int i, t_ertos_eventhandler evt);

/**
 * @breif 注销mpu的事件处理接口
 */
int mpu_event_dereg(int i);

/**
 * @breif 设置mpu loglevel
 * @param name task's name to set loglevel. NULL means set all the tasks' loglevel
 * @param logmask LOG_UPTO(MPULOG_XXX) in mpu/export_include/mpu_sys.h
 * @return 0:OK, <0:failed
 */
int mpu_setlogmask(char * taskname, int logmask);

/**
 * @brief send test cmd to BA.
 * @param trying count
 * @return 0:OK, < 0: failed
 */
int mpu_test_cmd(int try_cnt);
/** @} */ //end lib_dualba
#endif ///__LIBDCPU_H__

