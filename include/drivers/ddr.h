#ifndef _DDR_H_
#define _DDR_H_
int ddrset_ddr2_400M(void);
int ddrset_ddr2_533M(void);

int ddrset_ddr3_400M(void);
int ddrset_ddr3_533M(void);
int ddrset_ddr3_533M_remap(void);
int ddrset_ddr3_564M(void);
int ddrset_ddr3_666M(void);

int ddrset_lpddr2_200M(void);
int ddrset_lpddr2_250M(void);
int ddrset_lpddr2_300M(void);
int ddrset_lpddr2_400M(void);
int ddrset_lpddr2_400M_remap(void);
int ddrset_lpddr2_533M(void);

int ddrset_lpddr2_config(void);

/**
 * @brief ddrset auto
 * @para in: clock, expected ddr clock, if 0,set the usable highest clock
 * @return >0:ok, <0:err
 */
int ddrset_auto(int clock);

u32 ddr_regread(u32 reg);
u32 ddr_regread_cmdshell(int argc, char ** argv);

//since power up,DDR can only be configed once.If you want to redo  DDR config,before ddrset_XX_XX(),call ddr_disable() to reset DDR first 
int ddr_disable(void);

int lpddr2_retrain(void);
int ddr3_retrain(void);
int ddr2_retrain(void);
#endif
