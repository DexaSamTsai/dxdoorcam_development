#ifndef __LIBCACHE__
#define __LIBCACHE__

void tlb_set_bufaddr(unsigned int * addr);

/**
 * @brief enable I cache
 * @param void
 * @return 0:ok
 */
int ic_enable(void);

/**
 * @brief check if I cache enabled
 * @param void
 * @return 0:disabled, 1: enabled
 */
int ic_enabled(void);

/**
 * @brief disable I cache
 * @param void
 * @return 0:ok
 */
int ic_disable(void);

/**
 * @brief clean a part of D cache
 * @param startaddr: start address
 * @param endaddr: end address
 */
void dc_clean_range(unsigned int startaddr, unsigned int endaddr);

/**
 * @brief flush a part of D cache to memory
 * @param startaddr: start address
 * @param endaddr: end address
 */
void dc_flush_range(unsigned int startaddr, unsigned int endaddr);

/**
 * @brief invalidate a part of D cache
 * @param startaddr: start address
 * @param endaddr: end address
 */
void dc_invalidate_range(unsigned int startaddr, unsigned int endaddr);

/**
 * @brief flush all D cache to memory
 */
void dc_flush_all(void);

/**
 * @brief invalidate all D cache.
 */
void dc_invalidate_all(void);

/**
 * @brief enable D cache
 * @param void
 * @return 0:ok
 */
int dc_enable(void);

/**
 * @brief check if D cache enabled
 * @param void
 * @return 0:disabled, 1: enabled
 */
int dc_enabled(void);

/**
 * @brief disable D cache
 * @param void
 * @return 0:ok
 */
int dc_disable(void);

/**
 * @brief disable I cache and D cache
 * @param void
 * @return 0:ok
 */
void disable_cache(void);

extern int cache_writeback;

#endif // __LIBCACHE__
