#ifndef _TICK_H_
#define _TICK_H_

/**
 * @defgroup libtick
 * @brief Tick library.
 * @details this library handle the Tick, include Tick handler, set Tick and so on.
 <p>
 * -# <b>Depends On</b>:
 * -# <b>Used By</b>:
   * -# Tick1 is used by \ref lib_newos
 * -# <b>Design Doc</b>:
   * -# there are two Tick Timers in this chip, The Tick Timer is used to schedule operating system and user tasks on regular time basis or as a high precision time reference
   * -# when call tickX_set_YY(), the auto-incremented with each clock cycle counter is started from zero.
   * -# when the counter match 'cycle' in the argument, match event happens
   * -# each timer has three working mode:
     * -# single-run mode: counter is stopped when match event happens
     * -# restart mode: counter restarts counting from zero when a match event happens
     * -# continuous mode: counter keeps counting even when match event happens
   * -# when match event happens, interrupt(exception) will be triggered,
     * -# for tick1, except_tick() function will be called.
     * -# for tick2, tick2_irq_handler() function will be called.
 * -# <b>Sample codes</b>
 * -# <b>NOTES</b>
 * -# <b>FAQ</b>
 * @ingroup lib_cpu
 * @{
 */

/**
 * @REAL_FORMAT tick1_set_sr(cycle)
 * @brief set tick1 single-run
 * @details in single-run mode, timer is stopped when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick1_set_sr(cycle) \
do{ \
	WriteReg32(REG_INT_BASE + 0x88, cycle); \
	WriteReg32(REG_INT_BASE + 0x80, (ReadReg32(REG_INT_BASE + 0x80) | BIT1 | BIT9) & ~BIT16 ); \
}while(0)
//INTERNAL_END


/**
 * @REAL_FORMAT tick1_set_rt(cycle)
 * @brief set tick1 restart mode
 * @details in restart mode, timer restarts counting from zero when a match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick1_set_rt(cycle) \
do{ \
	WriteReg32(REG_INT_BASE + 0x88, cycle); \
	WriteReg32(REG_INT_BASE + 0x80, ReadReg32(REG_INT_BASE + 0x80) | BIT1 | BIT9 | BIT16 ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick1_disable()
 * @brief clean tick1 interrupt and disable
 * @details
*/
//INTERNAL_START
#define tick1_disable() \
do{ \
	WriteReg32(REG_INT_BASE + 0x80, ReadReg32(REG_INT_BASE + 0x80) & ~BIT1); \
}while(0)
//INTERNAL_END

//TODO
#define tick1_get_cycle() ReadReg32(REG_INT_BASE + 0x94)

/**
 * @REAL_FORMAT tick2_set_sr(cycle)
 * @brief set tick2 single-run
 * @details in single-run mode, timer is stopped when match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick2_set_sr(cycle) \
do{ \
	WriteReg32(REG_INT_BASE + 0x8c, cycle); \
	WriteReg32(REG_INT_BASE + 0x80, (ReadReg32(REG_INT_BASE + 0x80) | BIT2 | BIT10) & ~BIT17 ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_set_rt(cycle)
 * @brief set tick2 restart mode
 * @details in restart mode, timer restarts counting from zero when a match event happens.
 *
 * @param cycle cycles for timer
*/
//INTERNAL_START
#define tick2_set_rt(cycle) \
do{ \
	WriteReg32(REG_INT_BASE + 0x8c, cycle); \
	WriteReg32(REG_INT_BASE + 0x80, (ReadReg32(REG_INT_BASE + 0x80) | BIT2 | BIT10) | BIT17 ); \
}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT tick2_disable()
 * @brief clean tick2 interrupt and disable
 * @details
*/
//INTERNAL_START
#define tick2_disable() \
do{ \
	WriteReg32(REG_INT_BASE + 0x80, ReadReg32(REG_INT_BASE + 0x80) & ~BIT1); \
}while(0)
//INTERNAL_END

/** @}*/

#endif
