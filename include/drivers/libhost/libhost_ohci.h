#ifndef _LIBHOST_OHCI__H_
#define _LIBHOST_OHCI__H_

#include "libhost_user.h"

/* OHCI CONTROL AND STATUS REGISTER MASKS */

/*
 * HcControl (control) register masks
 */
#define OHCI_CTRL_CBSR  (3 << 0)    /* control/bulk service ratio */
#define OHCI_CTRL_PLE   (1 << 2)    /* periodic list enable */
#define OHCI_CTRL_IE    (1 << 3)    /* isochronous enable */
#define OHCI_CTRL_CLE   (1 << 4)    /* control list enable */
#define OHCI_CTRL_BLE   (1 << 5)    /* bulk list enable */
#define OHCI_CTRL_HCFS  (3 << 6)    /* host controller functional state */
#define OHCI_CTRL_IR    (1 << 8)    /* interrupt routing */
#define OHCI_CTRL_RWC   (1 << 9)    /* remote wakeup connected */
#define OHCI_CTRL_RWE   (1 << 10)   /* remote wakeup enable */

/* pre-shifted values for HCFS */
#define OHCI_USB_RESET   (0 << 6)
#define OHCI_USB_RESUME  (1 << 6)
#define OHCI_USB_OPER    (2 << 6)
#define OHCI_USB_SUSPEND (3 << 6)

/*
 * HcCommandStatus (cmdstatus) register masks
 */
#define OHCI_HCR    (1 << 0)    /* host controller reset */
#define OHCI_CLF    (1 << 1)    /* control list filled */
#define OHCI_BLF    (1 << 2)    /* bulk list filled */
#define OHCI_OCR    (1 << 3)    /* ownership change request */
#define OHCI_SOC    (3 << 16)   /* scheduling overrun count */

/*
 * masks used with interrupt registers:
 * HcInterruptStatus (intrstatus)
 * HcInterruptEnable (intrenable)
 * HcInterruptDisable (intrdisable)
 */
#define OHCI_INTR_SO    (1 << 0)    /* scheduling overrun */
#define OHCI_INTR_WDH   (1 << 1)    /* writeback of done_head */
#define OHCI_INTR_SF    (1 << 2)    /* start frame */
#define OHCI_INTR_RD    (1 << 3)    /* resume detect */
#define OHCI_INTR_UE    (1 << 4)    /* unrecoverable error */
#define OHCI_INTR_FNO   (1 << 5)    /* frame number overflow */
#define OHCI_INTR_RHSC  (1 << 6)    /* root hub status change */
#define OHCI_INTR_OC    (1 << 30)   /* ownership change */
#define OHCI_INTR_MIE   (1 << 31)   /* master interrupt enable */


/* OHCI ROOT HUB REGISTER MASKS */

/* roothub.portstatus [i] bits */
#define RH_PS_CCS            0x00000001     /* current connect status */
#define RH_PS_PES            0x00000002     /* port enable status*/
#define RH_PS_PSS            0x00000004     /* port suspend status */
#define RH_PS_POCI           0x00000008     /* port over current indicator */
#define RH_PS_PRS            0x00000010     /* port reset status */
#define RH_PS_PPS            0x00000100     /* port power status */
#define RH_PS_LSDA           0x00000200     /* low speed device attached */
#define RH_PS_CSC            0x00010000     /* connect status change */
#define RH_PS_PESC           0x00020000     /* port enable status change */
#define RH_PS_PSSC           0x00040000     /* port suspend status change */
#define RH_PS_OCIC           0x00080000     /* over current indicator change */
#define RH_PS_PRSC           0x00100000     /* port reset status change */

/* roothub.status bits */
#define RH_HS_LPS        0x00000001     /* local power status */
#define RH_HS_OCI        0x00000002     /* over current indicator */
#define RH_HS_DRWE       0x00008000     /* device remote wakeup enable */
#define RH_HS_LPSC       0x00010000     /* local power status change */
#define RH_HS_OCIC       0x00020000     /* over current indicator change */
#define RH_HS_CRWE       0x80000000     /* clear remote wakeup enable */

/* roothub.b masks */
#define RH_B_DR     0x0000ffff      /* device removable flags */
#define RH_B_PPCM   0xffff0000      /* port power control mask */

/* roothub.a masks */
#define RH_A_NDP    (0xff << 0)     /* number of downstream ports */
#define RH_A_PSM    (1 << 8)        /* power switching mode */
#define RH_A_NPS    (1 << 9)        /* no power switching */
#define RH_A_DT     (1 << 10)       /* device type (mbz) */
#define RH_A_OCPM   (1 << 11)       /* over current protection mode */
#define RH_A_NOCP   (1 << 12)       /* no over current protection */
#define RH_A_POTPGT (0xff << 24)        /* power on to power good time */

/*
 * Hardware transfer status codes -- CC from td->hwINFO or td->hwPSW
 */
#define TD_CC_NOERROR      0x00
#define TD_CC_CRC          0x01
#define TD_CC_BITSTUFFING  0x02
#define TD_CC_DATATOGGLEM  0x03
#define TD_CC_STALL        0x04
#define TD_DEVNOTRESP      0x05
#define TD_PIDCHECKFAIL    0x06
#define TD_UNEXPECTEDPID   0x07
#define TD_DATAOVERRUN     0x08
#define TD_DATAUNDERRUN    0x09
/* 0x0A, 0x0B reserved for hardware */
#define TD_BUFFEROVERRUN   0x0C
#define TD_BUFFERUNDERRUN  0x0D
/* 0x0E, 0x0F reserved for HCD */
#define TD_NOTACCESSED     0x0F

/**************************************************************
 *                   FRAME INTERVAL
 **************************************************************/
#define  FI                     0x2EDF      /* 12000 bits per frame (-1) */
#define  DEFAULT_FMINTERVAL     ((((6 * (FI - 210)) / 7) << 16) | FI)

/***************************************************************
 *                                       TRANSFER DESCRIPTOR CONTROL FIELDS
 ***************************************************************/
#define  TD_ROUNDING        (u32) (0x00040000)        /* Buffer Rounding                             */
#define  TD_DELAY_INT(x)    (u32)((x) << 21)          /* Delay Interrupt                             */

/* typedef hardware accessable buffer to be volatile */
typedef volatile struct ohci_ed * t_ohci_ed_hdl;
typedef volatile struct ohci_td * t_ohci_td_hdl;
typedef volatile struct ohci_hcca * t_ohci_hcca_hdl;

typedef volatile struct ohci_itd * t_ohci_itd_hdl;

/*
 * OHCI Endpoint Descriptor (ED) ... holds TD queue
 * See OHCI spec, section 4.2
 *
 * This is a "Queue Head" for those transfers, which is why
 * both EHCI and UHCI call similar structures a "QH".
 */
struct ohci_ed {
   /* first fields are hardware-specified */
   u32          hwINFO;         /* endpoint config bitmap */
   /* info bits defined by hcd */
#define ED_DEQUEUE  (1 << 27)
   /* info bits defined by the hardware */
#define ED_ISO      (1 << 15)
#define ED_SKIP     (1 << 14)
#define ED_LOWSPEED (1 << 13)
#define ED_OUT      (0x01 << 11)
#define ED_IN       (0x02 << 11)
	u32          hwTailP;    /* tail of TD list */
	u32          hwHeadP;    /* head of TD list (hc r/w) */
#define ED_C        (0x02)          /* toggle carry */
#define ED_H        (0x01)          /* halted */
	u32          hwNextED;   /* next ED in list */

	/* control and bulk EDS are double linked (next, prev), but
	 * periodic ones are single linked (next). that's because the
	 * periodic schedule encodes a tree link figure 3-5 in the ohci
	 * sped: each qh can have several "previous" nodes, and the tree
	 * doesn't have unused/idle descriptors.
	*/
	t_ohci_ed_hdl next;
	t_ohci_ed_hdl prev;//only for non-periodic eds

	u32 physical_addr;//physical memory address of this ed

	// param for interrupt/iso
	u32 interval;
	u16 load;

	u8 type;	/* PIPE_{BULK,...} */

	t_ohci_td_hdl  dummy_td;//next TD to activate

	u8 td_cnt;//numbers of this ed
	t_ohci_td_hdl  td[0];

	u8 ed_state;
#define ED_STATE_UNKNOWN			0
#define ED_STATE_LINKED				1
#define ED_STATE_UNLINK				2
#define ED_STATE_IDLE				3
} __attribute__ ((aligned(16)));

/*
 * OHCI Transfer Descriptor (TD) ... one per transfer segment
 * See OHCI spec, sections 4.3.1 (general = control/bulk/interrupt)
 */
struct ohci_td {
   /* first fields are hardware-specified */
    u32      hwINFO;     /* transfer info bitmask */

   /* hwINFO bits for both general and iso tds: */
#define TD_CC       0xf0000000          /* condition code */
#define TD_CC_GET(td_p) (((ReadReg32(&(td_p))) >>28) & 0x0f)
   //#define TD_CC_SET(td_p, cc) (td_p) = ((td_p) & 0x0fffffff) | (((cc) & 0x0f) << 28)
#define TD_DI       0x00E00000          /* frames before interrupt */
#define TD_DI_SET(X) (((X) & 0x07)<< 21)
   /* these two bits are available for definition/use by HCDs in both
    * general and iso tds ... others are available for only one type
						     */
#define TD_DONE     0x00020000          /* retired to donelist */
#define TD_ISO      0x00010000          /* copy of ED_ISO */

    /* hwINFO bits for general tds: */
#define TD_EC       0x0C000000          /* error count */
#define TD_T        0x03000000          /* data toggle state */
#define TD_T_DATA0  0x02000000              /* DATA0 */
#define TD_T_DATA1  0x03000000              /* DATA1 */
#define TD_T_TOGGLE 0x00000000              /* uses ED_C */
#define TD_DP       0x00180000          /* direction/pid */
#define TD_DP_SETUP 0x00000000          /* SETUP pid */
#define TD_DP_IN    0x00100000              /* IN pid */
#define TD_DP_OUT   0x00080000              /* OUT pid */
			                            /* 0x00180000 rsvd */
#define TD_R        0x00040000          /* round: short packets OK? */

    /* (no hwINFO #defines yet for iso tds) */

	u32      hwCBP;      /* Current Buffer Pointer (or 0) */
	u32      hwNextTD;   /* Next TD Pointer */
    u32      hwBE;       /* Memory Buffer End Pointer */

	struct ohci_ed *ed;

	u32 data_dma;//use for software driver
	
	struct ohci_td *next_dl_td;//use to process ohci done head in ohci irq routine

	struct ohci_td *td_next;//only use for software link

	struct urb *urb;

	u8 td_state;
#define TD_STATE_UNKNOWN			0
#define TD_STATE_LINKED				1
#define TD_STATE_UNLINKED			2
#define TD_STATE_IDLE				3

} __attribute__ ((aligned(16)));    /* c/b/i need 16; only iso needs 32 */

#define TD_MASK ((u32)~0x1f)        /* strip hw status in low addr bits */

/*
 * OHCI Transfer Descriptor (TD) ... one per transfer segment
 * See OHCI spec, sections 4.3.1 and 4.3.2 (iso)
 */
struct ohci_itd {
   /* first fields are hardware-specified */
     u32      hwINFO;     /* transfer info bitmask */
#define ITD_FC_SET(X) (((X) & 0x07)<< 24)
   /* these two bits are available for definition/use by HCDs in 
    * iso tds ... others are available for only one type
						     */
     u32      hwBP0;      /* Current Buffer Pointer (or 0) */
 	 u32      hwNextTD;   /* Next TD Pointer */
     u32      hwBE;       /* Memory Buffer End Pointer */

#define  MAXPSW  4
     u32       hwPSW[MAXPSW];

	 struct urb *urb;
	 struct ohci_ed *ed;

	 struct ohci_itd *itd_next;//only use for software link
} __attribute__ ((aligned(32)));

/*
 * The HCCA (Host Controller Communications Area) is a 256 byte
 * structure defined section 4.4.1 of the OHCI spec. The HC is
 * told the base address of it.  It must be 256-byte aligned.
 */
struct ohci_hcca {
#define NUM_INTS 32
    u32  int_table [NUM_INTS];   /* periodic schedule */

	/*
	 * OHCI defines u16 frame_no, followed by u16 zero pad.
	 * Since some processors can't do 16 bit bus accesses,
	 * portable access must be a 32 bits wide.
	 */
	u32  frame_no;       /* current frame number */
	u32  done_head;      /* info returned for an interrupt */
	u8  reserved_for_hc [116];
	u8  what [4];       /* spec only identifies 252 bytes :) */
} __attribute__ ((aligned(256)));

struct ohci_hcd {
	u32 ohci_hcca_memory_base;
	struct ohci_hcca *hcca;

	struct ohci_ed *ed_ctrltail;//control ed list taili
	struct ohci_ed *ed_bulktail;//bulk ed list tail
	struct ohci_ed *ed_periodic[NUM_INTS];//intr and iso ed list (32)
};

#define OHCI_INTR_MASK		(OHCI_INTR_MIE | OHCI_INTR_WDH)

void td_init(struct ohci_td *td);
struct ohci_td *td_alloc(void);
void td_free(struct ohci_td *td);
struct ohci_ed *ed_alloc(void);
void ed_free(struct ohci_ed *ed);

void ohci_mem_init(void);
u32 ohci_dma_alloc(u32 len);
void ohci_dma_free(u32 dma_addr);
void ohci_hw_init(void);
void ohci_driver_init(void);

void dl_done_list(void);
void ohci_irq(void);
struct ohci_td *td_list_build(struct urb *urb);
int submit_ctrl(struct urb *urb);
int submit_bulk(struct urb *urb);
int submit_periodic(struct urb *urb);
int ohci_urb_enqueue(struct urb *urb);


void ohci_check_irq(void);
int ohci_check_port_status(void);
void ohci_port_suspend(u32 port_index);
void ohci_port_resume(u32 port_index);
int ohci_host_enumdev(unsigned char dev_addr);
int ohci_bulk_transfer(void);
int ohci_intr_transfer(void);
int ohci_iso_transfer(void);
//int ohci_issue_transfer(void);
int ohci_issue_transfer(u32 type);

void ohci_ed_debug(t_ohci_ed_hdl ed);
void ohci_td_debug(t_ohci_td_hdl td);
void ohci_itd_debug(t_ohci_itd_hdl itd);		

#endif
