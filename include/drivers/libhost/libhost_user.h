#ifndef _LIBHOST_USER__H_
#define _LIBHOST_USER__H_

#define USB_HOST_MEMBASE	(CONFIG_HOST_MEMBASE+BIT31)
#define OHCI_HEAP_BASE  (USB_HOST_MEMBASE)
#define EHCI_HEAP_BASE  (USB_HOST_MEMBASE+128*1024)

//64k bytes buffer
#define EHCI_PERIODIC_MEMORY_BASE		(EHCI_HEAP_BASE)
#define EHCI_PERIODIC_FRAME_LIST_SIZE	0x400
#define EHCI_PERIODIC_MEMORY_SIZE		(EHCI_PERIODIC_FRAME_LIST_SIZE << 2)	
#define EHCI_QH_POOL_BASE				(EHCI_PERIODIC_MEMORY_BASE + EHCI_PERIODIC_MEMORY_SIZE)
#define EHCI_QH_POOL_NUM				16
#define	EHCI_QH_POOL_SIZE				(sizeof(struct ehci_qh) * EHCI_QH_POOL_NUM)

#define EHCI_QTD_POOL_BASE				(EHCI_QH_POOL_BASE + EHCI_QH_POOL_SIZE)
#define EHCI_QTD_POOL_NUM				32
#define EHCI_QTD_POOL_SIZE				(sizeof(struct ehci_qtd) * EHCI_QTD_POOL_NUM)

#define EHCI_ITD_POOL_BASE				(EHCI_QTD_POOL_BASE + EHCI_QTD_POOL_SIZE)
#define EHCI_ITD_POOL_NUM				32
#define EHCI_ITD_POOL_SIZE				(sizeof(struct ehci_itd) * EHCI_ITD_POOL_NUM)

//echi dma pool context 
#define EHCI_DATA64_DMA_BASE			(EHCI_ITD_POOL_BASE + EHCI_ITD_POOL_SIZE)
#define EHCI_DATA64_DMA_STEP			64
#define EHCI_DATA64_DMA_NUM				32
#define EHCI_DATA64_DMA_SIZE			(EHCI_DATA64_DMA_STEP * EHCI_DATA64_DMA_NUM)

#define EHCI_DATA512_DMA_BASE			(EHCI_DATA64_DMA_BASE + EHCI_DATA64_DMA_SIZE)
#define EHCI_DATA512_DMA_STEP			512
#define EHCI_DATA512_DMA_NUM			32
#define EHCI_DATA512_DMA_SIZE			(EHCI_DATA512_DMA_STEP * EHCI_DATA512_DMA_NUM)

#define EHCI_DATA1024_DMA_BASE			(EHCI_DATA512_DMA_BASE + EHCI_DATA512_DMA_SIZE)
#define EHCI_DATA1024_DMA_STEP			1024
#define EHCI_DATA1024_DMA_NUM			8
#define EHCI_DATA1024_DMA_SIZE			(EHCI_DATA1024_DMA_STEP * EHCI_DATA1024_DMA_NUM)

#define EHCI_DATA3072_DMA_BASE			(EHCI_DATA1024_DMA_BASE + EHCI_DATA1024_DMA_SIZE)
#define EHCI_DATA3072_DMA_STEP			3072
#define EHCI_DATA3072_DMA_NUM			8
#define EHCI_DATA3072_DMA_SIZE			(EHCI_DATA3072_DMA_STEP * EHCI_DATA3072_DMA_NUM)


#define OHCI_HCCA_BASE					(OHCI_HEAP_BASE)
#define OHCI_HCCA_SIZE					1024

#define OHCI_ED_POOL_BASE				(OHCI_HCCA_BASE + OHCI_HCCA_SIZE)
#define OHCI_ED_POOL_NUM				16	
#define OHCI_ED_POOL_SIZE				(sizeof(struct ohci_ed) * OHCI_ED_POOL_NUM)

#define OHCI_TD_POOL_BASE				(OHCI_ED_POOL_BASE + OHCI_ED_POOL_SIZE)
#define OHCI_TD_POOL_NUM				16
#define OHCI_TD_POOL_SIZE				(sizeof(struct ohci_td) * OHCI_TD_POOL_NUM)

#define OHCI_ITD_POOL_BASE				(OHCI_TD_POOL_BASE + OHCI_TD_POOL_SIZE)
#define OHCI_ITD_POOL_NUM				16
#define OHCI_ITD_POOL_SIZE				(sizeof(struct ohci_itd) * OHCI_ITD_POOL_NUM)

//ochi dma pool
#define OHCI_DATA64_DMA_BASE			(OHCI_ITD_POOL_BASE + OHCI_ITD_POOL_SIZE)
#define OHCI_DATA64_DMA_STEP			64
#define OHCI_DATA64_DMA_NUM				32
#define OHCI_DATA64_DMA_SIZE			(OHCI_DATA64_DMA_STEP * OHCI_DATA64_DMA_NUM)

#define OHCI_DATA512_DMA_BASE			(OHCI_DATA64_DMA_BASE + OHCI_DATA64_DMA_SIZE)
#define OHCI_DATA512_DMA_STEP			512
#define	OHCI_DATA512_DMA_NUM			32	
#define OHCI_DATA512_DMA_SIZE			(OHCI_DATA512_DMA_STEP * OHCI_DATA512_DMA_NUM)

#define OHCI_DATA1024_DMA_BASE			(OHCI_DATA512_DMA_BASE + OHCI_DATA512_DMA_SIZE)
#define OHCI_DATA1024_DMA_STEP			1024
#define OHCI_DATA1024_DMA_NUM			8
#define OHCI_DATA1024_DMA_SIZE			(OHCI_DATA1024_DMA_STEP * OHCI_DATA1024_DMA_NUM)

struct usb_driver;
struct urb;

//==========================================================
struct usb_host_endpoint {
	u8 ep_num; //endpoint number = (ep_addr & 0x0f)  [0-15]
	u8 ep_type;
	u16 ep_mps;//maxpacketsize
	u8 toggle;//data toggle flag

	void *hcpriv;

	struct usb_endpoint_descriptor ep_desc;
};

struct usb_host_interface {
	struct usb_interface_descriptor	intf_desc;

#define MAXENDPOINTCOUNT	8
	u8 ep_cnt;
	struct usb_host_endpoint eps[MAXENDPOINTCOUNT];
};

struct usb_host_config {
	u16 cfg_size; //total length of the whole configuration descriptor

	struct usb_config_descriptor cfg_desc;

	struct usb_host_interface intf;
};

enum {
	/* Maximum packet size; encoded as 0,1,2,3 = 8,16,32,64 */
	PACKET_SIZE_8   = 0,
	PACKET_SIZE_16  = 1,
	PACKET_SIZE_32  = 2,
	PACKET_SIZE_64  = 3,
};

struct usb_device {
	u8 port;//device port index, robin only has a port and must be '0'

	u8 addr;//address on a USB bus

	enum usb_device_state state; ///<indicate usb device's state
	enum usb_device_speed speed; ///<indicate usb device's speed.

	struct usb_host_endpoint *ep0_in;
	struct usb_host_endpoint *ep0_out;

	struct usb_device_descriptor device_desc;
	struct usb_host_config config;

	//OTG property
//	struct usb_otg_descriptor otg_desc; 
	u8 hnp_en;
};

struct __hw_pool {
	u32 base;//physical memory base address
	u32 step;//element address step
	u8 cnt;//total element numbers
	u8 index;//element pointer
};

struct __ehci_hw_pool {
	struct __hw_pool *qh_pool;//qh_pool[0] is dummy qh for software async list address

	struct __hw_pool *qtd_pool;//the effective data qtds of all qh
	struct __hw_pool *itd_pool;//isoc transfer itds struct
};

struct __ohci_hw_pool {
	struct __hw_pool *ed_pool;

	struct __hw_pool *td_pool;
	struct __hw_pool *itd_pool;
};

struct __dma {
	u32 dma_addr;
	u8 dma_state;
#define DMA_STATE_UNKNOWN			0
#define DMA_STATE_USED				1
#define DMA_STATE_IDLE				2
};

struct __hcd_dma {
	struct __hw_pool dma_pool;

#define MAX_ELEMENT_NUM_OF_POOL			32
	struct __dma dma_element[MAX_ELEMENT_NUM_OF_POOL];
};

struct hc_driver {
	const char *hcd_name;
	const char *product_name;
	
	u8 speed;
#define	HCD_USB11		0x0001		/* USB 1.1 */
#define	HCD_USB20		0x0002		/* USB 2.0 */
#define	HCD_SPEED_MASK	0x0003

	void (*irq)(void);
	int (*port_detect)(u8 port_index);
	int (*urb_enqueue)(struct urb *urb);
	int (*hub_control)(struct urb *urb);

	//for EHCI
	u32 periodic_list_size;
	u32 periodic_list_addr;
	u32 interval;//only for periodic transfer(ms), we use const
	u32 period_link_cnt;//(qh and itd)'s count of link to periodic frame list

	struct ehci_qh *async_head;//only use for software link, not real qh to hc async(control and bulk) transfer
	struct ehci_qh *intr_head;//only use for software link, not real qh to hc interrupt transfer
	struct ehci_itd *itd_head;//only use for software link, not real itd to hc isoc transfer

	struct __ehci_hw_pool ehci_pool;
	struct __ohci_hw_pool ohci_pool;

	struct ohci_hcd *ohci_hcd;

	struct __hcd_dma *hcd_dmas;
	u8 hcd_dma_cnt;

	u32 (*dma_alloc)(u32 len);
	void (*dma_free)(u32 dma_addr);
};

struct usb_device_id {
	/* which fields to match against? */
	u16 match_flags;

	/* Used for product specific matches; range is inclusive */
	u16 idVendor;
	u16 idProduct;
	u16 bcdDevice_lo;
	u16 bcdDevice_hi;

	/* Used for device class matches */
	u8 bDeviceClass;
	u8 bDeviceSubClass;
	u8 bDeviceProtocol;

	/* Used for interface class matches */
	u8 bInterfaceClass;
	u8 bInterfaceSubClass;
	u8 bInterfaceProtocol;
};

/* Some useful macros to use to create struct usb_device_id */
#define USB_DEVICE_ID_MATCH_MAXNUM		10

#define USB_DEVICE_ID_MATCH_VENDOR		0x0001
#define USB_DEVICE_ID_MATCH_PRODUCT		0x0002
#define USB_DEVICE_ID_MATCH_DEV_LO		0x0004
#define USB_DEVICE_ID_MATCH_DEV_HI		0x0008
#define USB_DEVICE_ID_MATCH_DEV_CLASS		0x0010
#define USB_DEVICE_ID_MATCH_DEV_SUBCLASS	0x0020
#define USB_DEVICE_ID_MATCH_DEV_PROTOCOL	0x0040
#define USB_DEVICE_ID_MATCH_INT_CLASS		0x0080
#define USB_DEVICE_ID_MATCH_INT_SUBCLASS	0x0100
#define USB_DEVICE_ID_MATCH_INT_PROTOCOL	0x0200

#define USB_DEVICE_ID_MATCH_DEVICE \
		(USB_DEVICE_ID_MATCH_VENDOR | USB_DEVICE_ID_MATCH_PRODUCT)
#define USB_DEVICE_ID_MATCH_DEV_RANGE \
		(USB_DEVICE_ID_MATCH_DEV_LO | USB_DEVICE_ID_MATCH_DEV_HI)
#define USB_DEVICE_ID_MATCH_DEVICE_AND_VERSION \
		(USB_DEVICE_ID_MATCH_DEVICE | USB_DEVICE_ID_MATCH_DEV_RANGE)
#define USB_DEVICE_ID_MATCH_DEV_INFO \
		(USB_DEVICE_ID_MATCH_DEV_CLASS | \
		USB_DEVICE_ID_MATCH_DEV_SUBCLASS | \
		USB_DEVICE_ID_MATCH_DEV_PROTOCOL)
#define USB_DEVICE_ID_MATCH_INT_INFO \
		(USB_DEVICE_ID_MATCH_INT_CLASS | \
		USB_DEVICE_ID_MATCH_INT_SUBCLASS | \
		USB_DEVICE_ID_MATCH_INT_PROTOCOL)

const struct usb_device_id *usb_match_id(struct usb_device *udev, struct usb_host_interface *interface, const struct usb_device_id *id); 

int hostclass_init(void);

struct usb_driver {
	const char *name;

	int (*probe) (struct usb_device *udev, struct usb_host_interface *intf, const struct usb_device_id *id);

	const struct usb_device_id *id_table;
};

extern int usb_driver_register(struct usb_driver *udrv);
extern int hcd_driver_register(struct hc_driver *hcdrv);

void usb_urb_pool_init(void);
struct urb *usb_alloc_urb(void);
void usb_free_urb(struct urb *urb);
typedef void (*urb_complete_callback) (struct urb *urb);

struct urb {
	struct usb_device *udev;

	struct usb_host_endpoint *ep;	//pointer to endpoint of pipe

	s32 pipe;		/* (in) pipe information */
/*
   bits 31-30: pipe type (00=iso, 01=int, 10=ctrl, 11=bulk)
   bits 18-15: endpoint (0-15)
   bits 14-08: device address
   bits 07: direction (0=host to device, 1=device to host)
*/
	
	void *transfer_buffer;		/* (in) associated data buffer */
	u32 transfer_buffer_length;	/* (in) data buffer length */
	u32 actual_length;		/* (return) actual transfer length */
	u32 transfer_dma;
	
	u8 *setup_packet;	/* (in) setup packet (control only) */
	u32 setup_dma;
	
	u32 interval;			/* (modify) transfer interval (INT/ISO) */

	urb_complete_callback urb_callback;
	volatile int urb_status;
#define URB_STATUS_IDLE			0x00
#define URB_STATUS_WORKING		0x01
#define URB_STATUS_COMPLETE		0x02

	struct ehci_qtd *qtd_head;//the first qtd of this urb packet
	struct ehci_qtd *qtd_tail;//the last qtd of this urb packet
	u32 qtd_cnt;//nums of this urb packet

	struct ohci_td *td_head;//the first td of this urb packet
	struct ohci_td *td_tail;//the last td of this urb packet
	u32 td_cnt; //nums tds of this urb packet

	struct ohci_itd *itd_head;//the first td of this urb packet
	struct ohci_itd *itd_tail;//the last td of this urb packet
	u32 itd_cnt; //nums itds of this urb packet
	u32 number_of_packets;
};

//don't modify pipe type value
#define PIPE_ISOCHRONOUS		0
#define PIPE_INTERRUPT			1
#define PIPE_CONTROL			2
#define PIPE_BULK				3
#define PIPE_ASYNC_MASK			0x02

#define usb_pipein(pipe)	((pipe) & USB_DIR_IN)
#define usb_pipeout(pipe)	(!usb_pipein(pipe))

#define usb_pipedevice(pipe)	(((pipe) >> 8) & 0x7f)
#define usb_pipeendpoint(pipe)	(((pipe) >> 15) & 0xf)

#define usb_pipetype(pipe)	(((pipe) >> 30) & 3)
#define usb_pipeisoc(pipe)	(usb_pipetype((pipe)) == PIPE_ISOCHRONOUS)
#define usb_pipeint(pipe)	(usb_pipetype((pipe)) == PIPE_INTERRUPT)
#define usb_pipecontrol(pipe)	(usb_pipetype((pipe)) == PIPE_CONTROL)
#define usb_pipebulk(pipe)	(usb_pipetype((pipe)) == PIPE_BULK)
#define usb_pipeasync(pipe)	((usb_pipetype(pipe)) & PIPE_ASYNC_MASK)

static inline unsigned int __create_pipe(struct usb_device *udev, unsigned int endpoint)
{
	return (udev->addr << 8) | (endpoint << 15);
}

/* Create various pipes... */
#define usb_sndctrlpipe(udev, endpoint)	\
	((PIPE_CONTROL << 30) | __create_pipe(udev, endpoint))
#define usb_rcvctrlpipe(udev, endpoint)	\
	((PIPE_CONTROL << 30) | __create_pipe(udev, endpoint) | USB_DIR_IN)
#define usb_sndisocpipe(udev, endpoint)	\
	((PIPE_ISOCHRONOUS << 30) | __create_pipe(udev, endpoint))
#define usb_rcvisocpipe(udev, endpoint)	\
	((PIPE_ISOCHRONOUS << 30) | __create_pipe(udev, endpoint) | USB_DIR_IN)
#define usb_sndbulkpipe(udev, endpoint)	\
	((PIPE_BULK << 30) | __create_pipe(udev, endpoint))
#define usb_rcvbulkpipe(udev, endpoint)	\
	((PIPE_BULK << 30) | __create_pipe(udev, endpoint) | USB_DIR_IN)
#define usb_sndintpipe(udev, endpoint)	\
	((PIPE_INTERRUPT << 30) | __create_pipe(udev, endpoint))
#define usb_rcvintpipe(udev, endpoint)	\
	((PIPE_INTERRUPT << 30) | __create_pipe(udev, endpoint) | USB_DIR_IN)


static inline void usb_fill_urb(struct urb *urb,
					struct usb_device *udev,
					int pipe,
					void *transfer_buffer,
					unsigned int buffer_length)
{
	urb->udev = udev;
	urb->pipe = pipe;
	urb->transfer_buffer = transfer_buffer;
	urb->transfer_buffer_length = buffer_length;
	urb->urb_status = URB_STATUS_IDLE;
	urb->qtd_cnt = 0;
}

static inline void usb_fill_control_urb(struct urb *urb,
					struct usb_device *udev,
					int pipe,
					unsigned char *setup_packet,
					void *transfer_buffer,
					unsigned int buffer_length,
					urb_complete_callback complete_fn)
{
	urb->setup_packet = setup_packet;
	urb->urb_callback = complete_fn;
	usb_fill_urb(urb, udev, pipe, transfer_buffer, buffer_length);
}

static inline void usb_fill_bulk_urb(struct urb *urb,
					struct usb_device *udev,
					int pipe,
					void *transfer_buffer,
					u32 buffer_length,
					urb_complete_callback complete_fn)
{
	urb->urb_callback = complete_fn;
	usb_fill_urb(urb, udev, pipe, transfer_buffer, buffer_length);
}

static inline void usb_fill_int_urb(struct urb *urb,
					struct usb_device *udev,
					int pipe,
				    void *transfer_buffer,
				    unsigned int buffer_length,
				    unsigned int interval,
					urb_complete_callback complete_fn)
{
	if (udev->speed == USB_SPEED_HIGH)
		urb->interval = 1 << (interval - 1);
	else
		urb->interval = interval;

	urb->urb_callback = complete_fn;
	usb_fill_urb(urb, udev, pipe, transfer_buffer, buffer_length);
}

static inline void usb_fill_iso_urb(struct urb *urb,
					struct usb_device *udev,
					int pipe,
				    void *transfer_buffer,
				    unsigned int buffer_length,
				    unsigned int interval,
					urb_complete_callback complete_fn)
{
	urb->interval = 1 << (interval - 1);
	
	urb->urb_callback = complete_fn;
	usb_fill_urb(urb, udev, pipe, transfer_buffer, buffer_length);
}

#define USB_CTRL_TIMEOUT	50000	//1s
#define USB_BULK_TIMEOUT	50000	//1s
#define USB_INTR_TIMEOUT	50000	//1s
#define USB_ISOC_TIMEOUT	50000	//1s

int usb_enumerate_device(unsigned short dev_addr);
int usb_get_device_descriptor(struct usb_device *udev, void *buf, unsigned short size);
int usb_get_config_descriptor(struct usb_device *udev, void *buf, unsigned short size);
int usb_get_otg_descriptor(struct usb_device *udev, void *buf, unsigned short size);
int usb_get_descriptor(struct usb_device *udev, unsigned char type, unsigned char index, void *buf, unsigned short size);
int usb_get_config(struct usb_device *udev);
int usb_set_address(struct usb_device *udev, unsigned short dev_addr);
int usb_set_config(struct usb_device *udev, unsigned short config_no);

int usb_send_vendor_cmd(struct usb_device *udev, 
						unsigned char dir, 
						unsigned char cmd,
						unsigned short value,
						unsigned short index,
						void *buf,
						unsigned short size);
int usb_send_class_cmd(struct usb_device *udev, 
						unsigned char dir, 
						unsigned char cmd,
						unsigned short value,
						unsigned short index,
						void *buf,
						unsigned short size);

void urb_common_complete_blocking(struct urb *urb);
struct urb *usb_alloc_urb(void);
void usb_free_urb(struct urb *urb);
int usb_submit_urb(struct urb *urb);
int wait_urb_complete_timeout(struct urb *urb, u32 timeout);

int usb_control_msg(struct usb_device *udev, 
					int pipe, 
					unsigned char reqtype, 
					unsigned char req, 
					unsigned short value, 
					unsigned short index, 
					void *data, 
					unsigned short size);

int usb_bulk_msg(struct usb_device *udev, 
				 int pipe,
				 void *data, 
				 unsigned int size,
				 unsigned int *actual_len);

int usb_interrupt_msg(struct usb_device *udev, 
					  int pipe,
					  void *data, 
					  unsigned int size,
					  unsigned int *actual_len);

unsigned char usb_get_ep(struct usb_device *udev, unsigned char type, unsigned char dir);
struct usb_host_endpoint *usb_pipe_endpoint(struct usb_device *udev, int pipe);
int usb_rw(struct usb_device *udev,
		   int pipe,
		   void *data,
		   unsigned int size);
int usb_bulk_read(struct usb_device *udev, void *data, unsigned int size);
int usb_bulk_write(struct usb_device *udev, void *data, unsigned int size);
int usb_set_feature(struct usb_device *udev, int target, int feature, int selector);
int usb_clear_feature(struct usb_device *udev, int target, int feature, int selector);
int usb_get_status(struct usb_device *udev, int target, int featureselector, u16 *status);

#define ClearHubFeature		(0x2000 | USB_REQ_CLEAR_FEATURE)
#define ClearPortFeature	(0x2300 | USB_REQ_CLEAR_FEATURE)
#define GetHubDescriptor	(0xa000 | USB_REQ_GET_DESCRIPTOR)
#define GetHubStatus		(0xa000 | USB_REQ_GET_STATUS)
#define GetPortStatus		(0xa300 | USB_REQ_GET_STATUS)
#define SetHubFeature		(0x2000 | USB_REQ_SET_FEATURE)
#define SetPortFeature		(0x2300 | USB_REQ_SET_FEATURE)

int is_hub_control(struct urb *urb);
int usb_port_suspend(struct usb_device *udev);
int usb_port_resume(struct usb_device *udev);
int usb_port_reset(struct usb_device *udev);
int usb_port_testmode(struct usb_device *udev, int selector);
int usb_port_testmode_exit(struct usb_device *udev, int selector);

#endif
