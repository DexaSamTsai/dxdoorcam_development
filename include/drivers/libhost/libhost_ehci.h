#ifndef __LIB_USB_EHCI__H__
#define __LIB_USB_EHCI__H__

#define USB_DEVSPD_UNKNOWN          0
#define USB_ATTACH_DEVSPD_HIGH      1
#define USB_DETACH_DEVSPD_HIGH      2
#define USB_ATTACH_DEVSPD_FULL      3
#define USB_DETACH_DEVSPD_NOHIGH    4
#define USB_ATTACH_DEVSPD_LOW       5

/*****************************************************/
// EHCI capability macro define
/*****************************************************/
#define HC_LENGTH(p)        (((p)>>00)&0x00ff)  /* bits 7:0 */
#define HC_VERSION(p)       (((p)>>16)&0xffff)  /* bits 31:16 */

/* HCSPARAMS - offset 0x4 */
#define HCS_DEBUG_PORT(p)   (((p)>>20)&0xf) /* bits 23:20, debug port? */
#define HCS_INDICATOR(p)    ((p)&(1 << 16)) /* true: has port indicators */
#define HCS_N_CC(p)     (((p)>>12)&0xf) /* bits 15:12, #companion HCs */
#define HCS_N_PCC(p)        (((p)>>8)&0xf)  /* bits 11:8, ports per CC */
#define HCS_PORTROUTED(p)   ((p)&(1 << 7))  /* true: port routing */
#define HCS_PPC(p)      ((p)&(1 << 4))  /* true: port power control */
#define HCS_N_PORTS(p)      (((p)>>0)&0xf)  /* bits 3:0, ports on HC */

/* HCCPARAMS - offset 0x8 */
#define HCC_EXT_CAPS(p)     (((p)>>8)&0xff) /* for pci extended caps */
#define HCC_ISOC_CACHE(p)       ((p)&(1 << 7))  /* true: can cache isoc frame */
#define HCC_ISOC_THRES(p)       (((p)>>4)&0x7)  /* bits 6:4, uframes cached */
#define HCC_CANPARK(p)      ((p)&(1 << 2))  /* true: can park on async qh */
#define HCC_PGM_FRAMELISTLEN(p) ((p)&(1 << 1))  /* true: periodic_size changes*/
#define HCC_64BIT_ADDR(p)       ((p)&(1))       /* true: can use 64-bit addr */

/*****************************************************/
// EHCI operation macro define
/*****************************************************/
/* USBCMD: offset 0x00 */
/* 23:16 is r/w intr rate, in microframes; default "8" == 1/msec */
#define CMD_PARK    (1<<11)     /* enable "park" on async qh */
#define CMD_PARK_CNT(c) (((c)>>8)&3)    /* how many transfers to park for */
#define CMD_LRESET  (1<<7)      /* partial reset (no ports, etc) */
#define CMD_IAAD    (1<<6)      /* "doorbell" interrupt async advance */
#define CMD_ASE     (1<<5)      /* async schedule enable */
#define CMD_PSE     (1<<4)      /* periodic schedule enable */
/* 3:2 is periodic frame list size */
#define CMD_RESET   (1<<1)      /* reset HC not bus */
#define CMD_RUN     (1<<0)      /* start/stop HC */

/* USBSTS: offset 0x04 */
#define STS_ASS     (1<<15)     /* Async Schedule Status */
#define STS_PSS     (1<<14)     /* Periodic Schedule Status */
#define STS_RECL    (1<<13)     /* Reclamation */
#define STS_HALT    (1<<12)     /* Not running (any reason) */

/* some bits reserved */
/* these STS_* flags are also intr_enable bits (USBINTR) */
#define STS_VBUS	(1<<7)		/* Vbus error */
#define STS_IAA     (1<<5)      /* Interrupted on async advance */
#define STS_FATAL   (1<<4)      /* such as some PCI access errors */
#define STS_FLR     (1<<3)      /* frame list rolled over */
#define STS_PCD     (1<<2)      /* port change detect */
#define STS_ERR     (1<<1)      /* "error" completion (overflow, ...) */
#define STS_INT     (1<<0)      /* "normal" completion (short, ...) */

#define FLAG_CF     (1<<0)      /* true: we'll support "high speed" */

/* PORTSC: offset 0x44 */
/* 31:23 reserved */
#define PORT_WKOC_E (1<<22)     /* wake on overcurrent (enable) */
#define PORT_WKDISC_E   (1<<21)     /* wake on disconnect (enable) */
#define PORT_WKCONN_E   (1<<20)     /* wake on connect (enable) */
									/* 19:16 for port testing */
#define TESTMODE_J          0x1
#define TESTMODE_K          0x2
#define TESTMODE_SE0_NAK    0x3
#define TESTMODE_PACKET     0x4
#define TESTMODE_FORCE_EN   0x5

#define PORT_LED_OFF    (0<<14)
#define PORT_LED_AMBER  (1<<14)
#define PORT_LED_GREEN  (2<<14)
#define PORT_LED_MASK   (3<<14)
#define PORT_OWNER  (1<<13)     /* true: companion hc owns this port */
#define PORT_POWER  (1<<12)     /* true: has power (see PPC) */
#define PORT_USB11(x) (((x)&(3<<10))==(1<<10))  /* USB 1.1 device */
								/* 11:10 for detecting lowspeed devices (reset vs release ownership) */
								/* 9 reserved */
#define PORT_RESET  (1<<8)      /* reset port */
#define PORT_SUSPEND    (1<<7)      /* suspend port */
#define PORT_RESUME (1<<6)      /* resume it */
#define PORT_OCC    (1<<5)      /* over current change */
#define PORT_OC     (1<<4)      /* over current active */
#define PORT_PEC    (1<<3)      /* port enable change */
#define PORT_PE     (1<<2)      /* port enable */
#define PORT_CSC    (1<<1)      /* connect status change */
#define PORT_CONNECT    (1<<0)      /* device connected */
#define PORT_RWC_BITS   (PORT_CSC | PORT_PEC | PORT_OCC)

//PID CODE
#define TD_PID_OUT                        	(0x0)
#define TD_PID_IN                          	(0x1)
#define TD_PID_SETUP                    	(0x2)

//others
#define  TERMINATE     0xdfffffff

/*
 * EHCI Specification 0.95 Section 3.5
 * QTD: describe data transfer components (buffer, direction, ...)
 * See Fig 3-6 "Queue Element Transfer Descriptor Block Diagram".
 *
 * These are associated only with "QH" (Queue Head) structures,
 * used with control, bulk, and interrupt transfers.
 */
struct ehci_qtd {
    /* first part defined by EHCI spec */
	u32          hw_next;      /* see EHCI 3.5.1 */
	u32          hw_alt_next;      /* see EHCI 3.5.2 */
	u32          hw_token;         /* see EHCI 3.5.3 */
#define QTD_TOGGLE  (1 << 31)   /* data toggle */
#define QTD_LENGTH(tok) (((tok)>>16) & 0x7fff)
#define QTD_IOC     (1 << 15)   /* interrupt on complete */
#define QTD_CERR(tok)   (((tok)>>10) & 0x3)
#define QTD_PID(tok)    (((tok)>>8) & 0x3)
#define QTD_STS_ACTIVE  (1 << 7)    /* HC may execute this */
#define QTD_STS_HALT    (1 << 6)    /* halted on error */
#define QTD_STS_DBE (1 << 5)    /* data buffer error (in HC) */
#define QTD_STS_BABBLE  (1 << 4)    /* device was babbling (qtd halted) */
#define QTD_STS_XACT    (1 << 3)    /* device gave illegal response */
#define QTD_STS_MMF (1 << 2)    /* incomplete split transaction */
#define QTD_STS_STS (1 << 1)    /* split transaction state */
#define QTD_STS_PING    (1 << 0)    /* issue PING? */
    u32          hw_buf [5];        /* see EHCI 3.5.4 */

	struct urb *urb;

	u32 length;//counts of this qtd need transfer
	u8 qtd_state;
#define QTD_STATE_UNKNOWN		0
#define QTD_STATE_LINKED		1
#define QTD_STATE_UNLINKED		2
#define QTD_STATE_IDLE			3

	struct ehci_qtd *qtd_next;
} __attribute__ ((aligned (32)));

/*-------------------------------------------------------------------------*/
/*
 * EHCI Specification 0.95 Section 3.6
 * QH: describes control/bulk/interrupt endpoints
 * See Fig 3-7 "Queue Head Structure Layout".
 *
 * These appear in both the async and (for interrupt) periodic schedules.
 */
struct ehci_qh {
   /* first part defined by EHCI spec */
   u32          hw_next;     /* see EHCI 3.6.1 */
#define QH_TERMINATE	(1 << 0)
#define QH_TYPE_ITD		(0 << 1)
#define QH_TYPE_QH		(1 << 1)
#define QH_TYPE_SITD	(2 << 1)
#define QH_TYPE_FSTN	(3 << 1)
   u32          hw_info1;        /* see EHCI 3.6.2 */
#define QH_HEAD     0x00008000
   u32          hw_info2;        /* see EHCI 3.6.2 */
#define QH_SMASK    0x000000ff
#define QH_CMASK    0x0000ff00
#define QH_HUBADDR  0x007f0000
#define QH_HUBPORT  0x3f800000
#define QH_MULT     0xc0000000
	u32          hw_current;  /* current qtd list - see EHCI 3.6.4 */

	/* qtd overlay (hardware parts of a struct ehci_qtd) */
	u32          hw_qtd_next;
	u32          hw_alt_next;
	u32          hw_token;
	u32          hw_buf [5];

	struct ehci_qtd *qtd_head;//the first qtd of this qh
	struct ehci_qtd *qtd_tail;//the last qtd of this qh, it shall be terminate qtd

	u8 qh_state;
#define QH_STATE_UNKNOWN	0
#define QH_STATE_LINKED		1
#define QH_STATE_UNLINK		2
#define QH_STATE_IDLE		3

	struct ehci_qh *qh_next;
} __attribute__ ((aligned (32)));

/*-------------------------------------------------------------------------*/
/*
 * EHCI Specification 0.95 Section 3.3
 * Fig 3-4 "Isochronous Transaction Descriptor (iTD)"
 *
 * Schedule records for high speed iso xfers
 */
struct ehci_itd {
   /* first part defined by EHCI spec */
   u32          hw_next;           /* see EHCI 3.3.1 */
   u32          hw_transaction [8]; /* see EHCI 3.3.2 */
#define EHCI_ISOC_ACTIVE        (1<<31)        /* activate transfer this slot */
#define EHCI_ISOC_BUF_ERR       (1<<30)        /* Data buffer error */
#define EHCI_ISOC_BABBLE        (1<<29)        /* babble detected */
#define EHCI_ISOC_XACTERR       (1<<28)        /* XactErr - transaction error */
#define EHCI_ITD_LENGTH(tok)    (((tok)>>16) & 0x0fff)
#define EHCI_ITD_IOC        (1 << 15)   /* interrupt on complete */

#define ITD_ACTIVE  __constant_cpu_to_le32(EHCI_ISOC_ACTIVE)

	u32          hw_bufp [7];    /* see EHCI 3.3.3 */

	u8 itd_state;
#define ITD_STATE_UNKNOWN		0
#define ITD_STATE_LINKED		1
#define ITD_STATE_UNLINK		2
#define ITD_STATE_IDLE			3

	u32 period_frame;//indicate this itd's position of periodic frame list

	struct urb *urb;

	struct ehci_itd *itd_next;
} __attribute__ ((aligned (32)));

/*-------------------------------------------------------------------------*/
/*
 * EHCI Specification 0.95 Section 3.4
 * siTD, aka split-transaction isochronous Transfer Descriptor
 *       ... describe full speed iso xfers through TT in hubs
 * see Figure 3-5 "Split-transaction Isochronous Transaction Descriptor (siTD)
 */
struct ehci_sitd {
     /* first part defined by EHCI spec */
     u32          hw_next;
     /* uses bit field macros above - see EHCI 0.95 Table 3-8 */
     u32          hw_fullspeed_ep;    /* EHCI table 3-9 */
     u32          hw_uframe;      /* EHCI table 3-10 */
     u32          hw_results;     /* EHCI table 3-11 */
#define SITD_IOC    (1 << 31)   /* interrupt on completion */
#define SITD_PAGE   (1 << 30)   /* buffer 0/1 */
#define SITD_LENGTH(x)  (0x3ff & ((x)>>16))
#define SITD_STS_ACTIVE (1 << 7)    /* HC may execute this */
#define SITD_STS_ERR    (1 << 6)    /* error from TT */
#define SITD_STS_DBE    (1 << 5)    /* data buffer error (in HC) */
#define SITD_STS_BABBLE (1 << 4)    /* device was babbling */
#define SITD_STS_XACT   (1 << 3)    /* illegal IN response */
#define SITD_STS_MMF    (1 << 2)    /* incomplete split transaction */
#define SITD_STS_STS    (1 << 1)    /* split transaction state */

#define SITD_ACTIVE __constant_cpu_to_le32(SITD_STS_ACTIVE)

	 u32          hw_buf [2];     /* EHCI table 3-12 */
	 u32          hw_backpointer;     /* EHCI table 3-13 */
	 u32          hw_buf_hi [2];      /* Appendix B */
} __attribute__ ((aligned (32)));

/*
 * EHCI Specification 0.96 Section 3.7
 * Periodic Frame Span Traversal Node (FSTN)
 *
 * Manages split interrupt transactions (using TT) that span frame boundaries
 * into uframes 0/1; see 4.12.2.2.  In those uframes, a "save place" FSTN
 * makes the HC jump (back) to a QH to scan for fs/ls QH completions until
 * it hits a "restore" FSTN; then it returns to finish other uframe 0/1 work.
 */
struct ehci_fstn {
	  u32          hw_next;    /* any periodic q entry */
	  u32          hw_prev;    /* qh or EHCI_LIST_END */
} __attribute__ ((aligned (32)));
/*-------------------------------------------------------------------------*/

#define EHCI_INTR_MASK (STS_VBUS | STS_IAA | STS_FATAL | STS_PCD | STS_ERR | STS_INT)


void qtd_init(struct ehci_qtd *qtd);
struct ehci_qtd *qtd_alloc(void);
void qtd_free(struct ehci_qtd *qtd);
struct ehci_qh *qh_alloc(void);
void qh_free(struct ehci_qh *qh);
struct ehci_itd *itd_alloc(void);
void itd_free(struct ehci_itd *itd);

u32 ehci_dma_alloc(u32 len);
void ehci_dma_free(u32 dma_addr);

void ehci_mem_init(void);
void ehci_hw_init(void);
int hosthcd_init(void);
void ehci_driver_init(void);
int ehci_check_port_status(u8 port_index);

void ehci_work(void);
void ehci_irq(void);
struct ehci_qtd *qtd_list_build(struct urb *urb);
int submit_async(struct urb *urb);
int submit_intr(struct urb *urb);
int submit_isoc(struct urb *urb);
int ehci_urb_enqueue(struct urb *urb);

int ehci_hub_control(struct urb *urb);


void ehci_qh_debug(struct ehci_qh *qh);
void ehci_qtd_debug(struct ehci_qtd *qtd);
void ehci_itd_debug(struct ehci_itd *itd);
#endif
