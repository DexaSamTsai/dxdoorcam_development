#ifndef  _USBHOST_MS__H
#define  _USBHOST_MS__H
/**
  * @defgroup libhost_ms
  * @ingroup libhost
  * @brief This group APIs are designed for usb host.<br><br>
  * @details
  * - <b>Source Codes</b>:
    <br> share/libhost/libhost_driver/umass/usbhost_ms.c
  * - <b>Depends On</b>:
     <br> \ref libhost "libhost"
  * - <b>Used By</b>:
  * - <b>Design Doc</b>:
  * - <b>Sample Code</b>:
    * -# usb host init for umass
\code
#include "libhost.h"
#include  "libhost_ms.h"

#define CLUSTER_INDEX_BUF     (0x10000000+1024*60)
#define MAX_SECTOR_CNT        ((0x10000-10240*2)/512)

void usbhost_fs_init(void)
{
	debug_printf("umass_app_init begin\n");
	host_init();
	umass_app_init();
	debug_printf("umass_app_init end\n");
}

int main(void){
	....
	....

	usbhost_fs_init();
	sh_efs.myFs.FreeClusterCount = fs_calc_free(CLUSTER_INDEX_BUF, MAX_SECTOR_CNT);
	g_free_cardcap = sh_efs.myFs.FreeClusterCount * sh_efs.myFs.volumeId.SectorsPerCluster;
	debug_printf("g_free_cardcap %x", g_free_cardcap);
}
\endcode
  * - <b>NOTES</b>:
  * - <b>FAQ</b>:
  * @{
  */


//INTERNAL_START
/*
**************************************************************************************************************
*                               MASS STORAGE SPECIFIC DEFINITIONS
**************************************************************************************************************
*/

#define    MS_GET_MAX_LUN_REQ            0xFE
#define    MASS_STORAGE_CLASS            0x08
#define    MASS_STORAGE_SUBCLASS_SCSI    0x06
#define    MASS_STORAGE_PROTOCOL_BO      0x50

/*
**************************************************************************************************************
*                                  SCSI SPECIFIC DEFINITIONS
**************************************************************************************************************
*/

#define  CBW_SIGNATURE               0x43425355
#define  CSW_SIGNATURE               0x53425355
#define  CBW_SIZE                      31
#define  CSW_SIZE                      13
#define  CSW_CMD_PASSED              0x00
#define  SCSI_CMD_REQUEST_SENSE      0x03
#define  SCSI_CMD_TEST_UNIT_READY    0x00
#define  SCSI_CMD_READ_10            0x28
#define  SCSI_CMD_READ_CAPACITY      0x25
#define  SCSI_CMD_WRITE_10           0x2A

/*
**************************************************************************************************************
*                                       TYPE DEFINITIONS
**************************************************************************************************************
*/

typedef enum  ms_data_dir {
    MS_DATA_DIR_IN     = 0x80,
    MS_DATA_DIR_OUT    = 0x00,
    MS_DATA_DIR_NONE   = 0x01
} MS_DATA_DIR;

//INTERNAL_END

/**
  * @brief <b>umass app init</b>
  * @details call this function to init umass app
  *
  */
u32 umass_app_init(void);
//INTERNAL_START
u32 MS_bulk_read(u32 block_number, u16 num_blocks, u8 *user_buffer);
u32 MS_bulk_write(u32 block_number, u16 num_blocks, u8 *user_buffer);
u32 MS_TestUnitReady(void);
u32 MS_ReadCapacity(void);
u32 MS_GetMaxLUN(void);
u32 MS_GetSenseInfo(void);

//void fill_scsi_command(u8 *buffer, u32 block_number, u32 block_size, u16 num_blocks, MS_DATA_DIR direction, u8 scsi_cmd, u8 scsi_cmd_len);

//INTERNAL_END
/** @} */ // end libhost_ms
#endif
