#ifndef __LIB_USB_HOST__H__
#define __LIB_USB_HOST__H__

#ifndef TRUE
#define TRUE (1)
#endif

#ifndef FALSE
#define FALSE (0)
#endif


//#define debug_putc		lowlevel_putc

enum data_endian_type {
	LITTLE_ENDIAN_TYPE,
	BIG_ENDIAN_TYPE,
};

#include "libhost_ch9.h"
#include "libusb_ch11.h"
#include "libhost_user.h"

typedef struct s_libhost_cfg {
	enum data_endian_type endian; ///<indicate cpu data array endian.

	struct usb_device udev; ///<indicate usb device struct

	struct usb_driver *udrv;//only support one interface device

	struct hc_driver *ehci_driver;
	struct hc_driver *ohci_driver;

	volatile u8 pcd_done;//EHCI port change detect done in ehci_irq
}t_libhost_cfg;

extern t_libhost_cfg g_libhost_cfg;

int libhost_init(void);

#include "libhost_ohci.h"
#include "libhost_ehci.h"

#include "libhost_ms.h"

#endif//__LIB_USB_HOST__H__
