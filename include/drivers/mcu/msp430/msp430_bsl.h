#ifndef _MSP430_BSL_H_
#define _MSP430_BSL_H_

typedef struct s_msp430_bsl_cfg
{
	int pin_bsl_rst;
	int bsl_rst_level;
	int pin_bsl_jtag;
	int startup_after_burn;
	int force_masserase;	//if set to 1,will do mass erase before update msp430
	u8 password[32];	//save 32 Bytes password 
}t_msp430_bsl_cfg;

extern t_msp430_bsl_cfg g_msp430_bsl_cfg;

/**
 * @brief brun msp430. Only burn fw area if there are bootloader, fw_area and backup_area in flash 
 * @param buf the address of msp430 firmware buffer
 * @param len the length of the msp430 firmware
 * @return <0: fail, 0: OK
 */
int msp430_bsl_burn(u8 * buf, int len);

/**
 * @brief brun msp430 
 * @param buf the address of msp430 firmware buffer
 * @param len the length of the msp430 firmware
 * @return <0: fail, 0: OK
 */
int msp430_bsl_burn_full(u8 * buf, int len);

#endif
