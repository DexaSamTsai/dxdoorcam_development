#ifndef _NAND_PARTITION_H_
#define _NAND_PARTITION_H_

#include "partition_nand_internal.h"

/**
 * @brief <b>partition second boot nand</b>
 * @details boot second firmware from nand flash.
 *
 * @param id2: second firmware id
 *
 * @return <0: failed. if success, function jump to new firmware, not return.
 *
 */
int partition_boot_nand(int id2);

/**
 * @brief <b>partition second boot nand</b>
 * @details boot second firmware from nand flash.
 *
 * @param id2: second firmware id
 * @param buf: destination buffer
 *
 * @return <0: failed. if success, function jump to new firmware, not return.
 *
 */
int partition_load_nand(int id2, unsigned char *buf);
#endif	// _NAND_PARTITION_H_
