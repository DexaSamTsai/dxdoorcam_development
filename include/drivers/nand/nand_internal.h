#ifndef _NAND_INTERNAL_H_
#define _NAND_INTERNAL_H_

typedef struct s_nand_config
{
	int page_size;
	int block_size;
	unsigned char col_adr_size;
	unsigned char row_adr_size;
	unsigned char bDMA;
	unsigned char oob_use_cmd; //0: oob don't use cmd, >0: oob cmd
	unsigned char use_read30; //send cmd 0x30 after read cmd
    unsigned char ecc_bch;		//0:disable,1:ecc,2:bch
	int timeout;
	int timing;

	//internal
	t_ertos_queuehandler *evt; 
	t_irq_handler *irq_handler;
	int (*erase_block)(int row_adr);
	int (*copy_page)(int src_row_adr, int dst_row_adr, unsigned int buf, int size, int dst_col_adr, unsigned int buf1, int size1, int dst_col_adr1);
	int (*update_secs)(unsigned char *buf, int secnum, int count, int tmpblk);
}t_nand_config;

/*******************************
  nand_r/b can shared with spi1_data3 or bt_de,PIN_NAND_CONFIG_1 to use bt_de as nand_r/b ,PIN_NAND_CONFIG_2 use spi1_data3 as nand_r/b.
 *********************************/
#define NAND_PINCONFIG_1		\
do{ \
		WriteReg32(REG_SC_ADDR+0x1c, (ReadReg32(REG_SC_ADDR+0x1c) & (~BIT10)) |BIT11);	\
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SNR_DATA7TO0|SC_PINSEL0_SNR_D8|SC_PINSEL0_SNR_D9|SC_PINSEL0_SNR_HREF|SC_PINSEL0_SNR_VSYNC|SC_PINSEL0_SNR_PCLK);		\
		SC_PINSEL2_FUNC(BT1120_DE);		\
		SC_PIN1_EN(BT1120_DE);	\
		SC_PIN0_EN_MUL(SC_PINEN0_DVP_PCLK|SC_PINEN0_DVP_HREF|SC_PINEN0_DVP_VSYNC|SC_PINEN0_DVP_D9|SC_PINEN0_DVP_D8|SC_PINEN0_DVP_D7|SC_PINEN0_DVP_D6|SC_PINEN0_DVP_D5|SC_PINEN0_DVP_D4|SC_PINEN0_DVP_D3|SC_PINEN0_DVP_D2|SC_PINEN0_DVP_D1|SC_PINEN0_DVP_D0);	\
}while(0)
#define NAND_PINCONFIG_2		\
do{ \
		WriteReg32(REG_SC_ADDR+0x1c, (ReadReg32(REG_SC_ADDR+0x1c) & (~BIT11)) |BIT10);	\
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SNR_DATA7TO0|SC_PINSEL0_SNR_D8|SC_PINSEL0_SNR_D9|SC_PINSEL0_SNR_HREF|SC_PINSEL0_SNR_VSYNC|SC_PINSEL0_SNR_PCLK|SC_PINSEL0_SFC_DQ3);		\
		SC_PIN1_EN(SFC_DQ3);	\
		SC_PIN0_EN_MUL(SC_PINEN0_DVP_PCLK|SC_PINEN0_DVP_HREF|SC_PINEN0_DVP_VSYNC|SC_PINEN0_DVP_D9|SC_PINEN0_DVP_D8|SC_PINEN0_DVP_D7|SC_PINEN0_DVP_D6|SC_PINEN0_DVP_D5|SC_PINEN0_DVP_D4|SC_PINEN0_DVP_D3|SC_PINEN0_DVP_D2|SC_PINEN0_DVP_D1|SC_PINEN0_DVP_D0);	\
}while(0)

extern t_nand_config * g_nc;

int nand_irq_handler(void * arg);

void nand_chip_reset(void);

int nand_wait_ready(int timeout);

#endif
