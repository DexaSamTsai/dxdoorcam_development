#ifndef _PARTITION_NAND_INTERNAL_H_
#define _PARTITION_NAND_INTERNAL_H_

typedef struct s_romnand_cfg
{
	u8 oob_use_cmd;
	u8 col_adr_size;
	u8 row_adr_size;
	u8 config0;
#define ROMNAND_CFG0_USEREAD30	BIT0
#define ROMNAND_CFG0_USEDMA		BIT1
#define ROMNAND_CFG0_ECC		BIT2
#define ROMNAND_CFG0_BCH		BIT3
	u32 timing;
	u16 page_size;
	u16 timing48;
	u32 block_size;
}t_romnand_cfg;

	/* read header */
typedef struct s_nand_hdr{
	u32 magic_num;	
	u32 qs_id2;
	u32 rsvd0;
	u32 st_id2;
	u32 st_dst_addr;
	u32 rsvd2;
	u32 bl_src_addr;
	u32 bl_dst_addr;
	u32 bl_len;
	u32 bl_crc;
	t_romnand_cfg nand_cfg;
	u32 bl2_src_addr;
	u32 bl2_dst_addr;
	u32 bl2_len;
	u32 bl2_crc;
	u32 sys_cfg;
	u32 fw_addr;
	u32 fw_len;
	u32 para_addr;
	u32 para_len;
	u32 fw_back_addr;
	u32 fw_back_len;
	u32 para_back_addr;
	u32 para_back_len;
	u32 cali_addr;
	u32 cali_len;
	u32 other_addr;
	u32 other_len;
	u32 hdr_crc;
}t_nand_hdr; 

#endif
