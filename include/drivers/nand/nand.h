#ifndef _NAND_H_
#define _NAND_H_

#include "nand_internal.h"

extern t_nand_config g_nandflash;
//return -1:timeout,else return nand id
#define NAND_INTERFACE_RB1	0x0
#define NAND_INTERFACE_RB2	0x1

/**
 * @brief init nand flash 
 * @param interface 0:nand module interface \ref NAND_INTERFACE_RB1 or NAND_INTERFACE_RB2
 * @return 0:ok
 */
int nand_init(int interface);

/**
 * @details read data from nand normal area or spare area, with interrupt disabled.
 * @ingroup lib_nand
 *
 * @param col_adr: page offset
 * @param row_adr: page addr
 * @param dst_adr: data to addr, should be aligned with 4 bytes
 * @param size:    read data bytes, should be aligned with 4 bytes
 * col_adr+size should be less than page_size+size of spare area
 * @param oob:     0:normal area 1:spare area 
 *
 * @return -1:failed, 0:ok
 *
 */
int nand_read(int col_adr, int row_adr, int dst_adr, int size, int oob);

/*
 * @details nand write
 *
 * @param col_adr: page offset
 * @param row_adr: page addr
 * @param src_adr: write data addr
 * @param size: data size
 * @buf1: the address of second part data, 0: no second part
 * @size1: the size of second part data
 * @dst_col_adr1: page offset of the second part data
 *
 * @return 0:success,1/2:timeout,3:error happened
 *
 */
int nand_program(int col_adr, int row_adr, int src_adr, int size, unsigned int buf1, int size1, int dst_col_adr1);

/**
 * @details read data from nand normal page area or spare area, with interrupt disabled.
 * @ingroup lib_nand
 *
 * @param col_adr: page offset
 * @param row_adr: page addr
 * @param dst_adr: data to addr
 * @param size:    read data bytes
 * @param oob:     0:normal page area 1:spare area 
 *
 * @return -1:failed, 0:ok
 *
 */
int nand_read_page(int col_adr, int row_adr, int dst_adr, int size, int oob);
//int nand_read_page_ecc(int col_adr, int row_adr, int dst_adr, int size, int oob);

/*
 * @details nand write
 * note :shoule make sure that the page programed not bad block 
 *
 * @param col_adr: page offset
 * @param row_adr: page addr
 * @param src_adr: write data addr
 * @param size: data size
 *
 * @return 0:success,1/2:timeout,3:error happened
 *
 */
int nand_program_page(int col_adr, int row_adr, int src_adr, int size);

/*
 * @details write 2 part of data to nand
 *
 * @param col_adr: page offset
 * @param row_adr: page addr
 * @param src_adr: write data addr
 * @param size: data size
 * @buf1: the address of second part data, 0: no second part
 * @size1: the size of second part data
 * @dst_col_adr1: page offset of the second part data
 *
 * @return 0:success,1/2:timeout,3:error happened
 *
 */
int nand_program_page_2(int col_adr, int row_adr, int src_adr, int size, unsigned int buf1, int size1, int dst_col_adr1);

/*
 * @details nand erase one block
 *
 * @param row_adr: page addr
 *
 * @return -1: failed, 0: ok
 *
 */
int nand_erase_block(int row_adr);

/*
 * @details copy data from one page to another
 *
 * @param src_row_adr: source page offset
 * @param dst_row_adr: destination page addr
 * @param buf: data buffer
 * @param size: data size
 * @dst_col_adr: page offset of data
 * @buf1: the buffer of second part data, 0: no second part
 * @size1: the size of second part data
 * @dst_col_adr1: page offset of the second part data
 *
 * @return 0:success,1/2:timeout,3:error happened
 *
 */
int nand_copy_page(int src_row_adr, int dst_row_adr, unsigned int buf, int size, int dst_col_adr, unsigned int buf1, int size1, int dst_col_adr1);

/*
 * @details update data in nand flash in sector
 *
 * @param buf: update data buffer address
 * @param secnum: sector addr
 * @param count: sector count
 * @param tmpblk: the block can be used as data temporary storage
 *
 * @return 0:success,1/2:timeout,3:error happened
 *
 */
int nand_update_secs(unsigned char *buf, int secnum, int count, int tmpblk);

#endif
