#ifndef _UART_H_
#define _UART_H_

#include "uart_physical.h"

/**
 * @brief config uart
 *
 * @param baud_rate: baud rate
 * @param irq_en: enable interrupt
 */
void uart_config(u32 baud_rate, u32 irq_en);

//INTERNAL_START
/**
 * @brief uart tx interrupt handler
 * @param none
 *
 * @return 0 ok. 
 */
int uart_tx_interrupt_handler(int id);

/**
 * @brief uart init
 *
 * @param uart_clk_freq: uart clock
 * @param baud_rate: baud rate
 * @param irq_en: enable interrupt
 */
void uart_init(unsigned int uart_clk_freq, unsigned int baud_rate, unsigned char irq_en);

/**
 * @brief uart put one character
 *
 * @param c output character
 *
 * @return 0:OK
 */
int uart_putc(unsigned char c);
unsigned char uart_getc_echo(void);
unsigned char *uart_gets(unsigned char *buf, unsigned int size);

/**
 * @brief blockade untill uart get a character
 *
 * @param none 
 *
 * @return get character
 */
unsigned char uart_getc(void);

/**
 * @brief uart get one character without blockade
 *
 * @param pch get character 
 *
 * @return 0: OK, -1: fail
 */
int uart_getc_nob(unsigned char *pch);

/**
 * @brief uart get one character without blockade
 *
 * @return -1: fail, other: get character
 */
int uart_testgetc(void);

/**
 * REAL_FORMAT uart_forceputc(char c)
 * @brief uart put one character immediately
 *
 * @param c output character
 *
 * @return 0:OK
 */
#define uart_forceputc(c) do{WriteReg32(UART_BASE_M + UART_TX,c);}while(0)

int uart_printf(const char *fmt0, ...);
#define lowlevel_printf uart_printf
//INTERNAL_END

/**
 * @brief config uart1
 *
 * @param baud_rate: baud rate
 * @param irq_en: enable interrupt
 */
void uart1_config(u32 baud_rate, u32 irq_en);

//INTERNAL_START
/**
 * @brief init second uart interface
 *
 * @param uart_clk_freq: uart clock
 * @param baud_rate: baud rate
 * @param irq_en: enable interrupt
 */
void uarts_init(unsigned int uart_clk_freq, unsigned int baud_rate, unsigned char irq_en);

/**
 * @brief Second uart put one character
 *
 * @param c output character
 *
 * @return 0:OK
 */
void uarts_putc(unsigned char c);
#define uarts_forceputc(c) do{WriteReg32(UART_BASE_S + UART_TX,c);}while(0)

/**
 * @brief blockade untill second uart get a character
 *
 * @param none 
 *
 * @return get character
 */
unsigned char uarts_getc(void);

/**
 * @brief second uart get one character without blockade
 *
 * @return -1: fail, other: get character
 */
int uarts_testgetc(void);

int uarts_getc_nob(unsigned char *pch);
//INTERNAL_END

typedef struct s_uartdebug
{
	u32 uart_base;
	pthread_t threadid;
	int askexit; //0:running. 1:askexit. 2:exited
	t_ertos_eventhandler exit_event;
	t_debugprintf * dp;
	int txirqmode;
}t_uartdebug;

extern t_uartdebug * ud_uart0; //for irq handler
extern t_uartdebug * ud_uart1; //for irq handler

int uart_debug_exit(void * hw_arg, t_debugprintf * dpcfg);
int uart_debug_putbuf(void *hw_arg, t_debugprintf * dpcfg, unsigned char *buf, int len);
int uart_debug_init(void * arg, t_debugprintf * dpcfg, void ** hw_arg);

#endif
