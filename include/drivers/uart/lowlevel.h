#ifndef _LOWLEVEL_H_
#define _LOWLEVEL_H_

void lowlevel_config(u32 baud_rate, u32 irq_en);
void lowlevel_putc(unsigned char c);
unsigned char lowlevel_getc(void);
void lowlevel_forceputc(unsigned char c);
int lowlevel_getc_nob(unsigned char *pch);
int lowlevel_tx_interrupt_handler(void);
int lowlevel_testgetc(void);

#endif
