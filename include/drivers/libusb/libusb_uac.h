#ifndef _LIBUSB_UAC_NEW_H_
#define _LIBUSB_UAC_NEW_H_

/**
  * @defgroup libuac
  * @ingroup lib_midware
  * @brief This group APIs are designed for usb audio class.<br><br>
  * @details OV780/OV788 chip acts as a usb audio device, while PCs acts usb host. the host and device communicati on by the way uac protocl, which according to usb audio class specification 1.0.<br><br>
  * OV780/OV788 supports one kinds data format: PCM <br><br>
  <p>
   * - <b>Source Codes</b>:
       share/libuav_cfg_XXX.c is uac topology's configuration, while share/rXXXX/libuac_data_XXX is uac data stream configuration. in the end, share/libuac_request is class-specific request handler for uvc.
   * - <b>Depends On</b>:
   * - <b>Used By</b>:
       utilize this library when uac funtion is enable.
   * - <b>Design Doc</b>:
   * - <b>Sample Code</b>:
      * -# usb initialization and loop
\code
// The part of audio can refer lib_audio documents
int main(void){
	....
	....

	int i, k;
	e_usbisrret uret;

	memset(&g_libusb_cfg, 0, sizeof(g_libusb_cfg));
reinit:
	libusb_init();

	enable_irq(USBSUSPEND);
	
	ENABLE_INTERRUPT;

	while (1) {
		uret = USB_loop();
		if(uret == USBISRRET_REBOOT){
			break;
		}
		if(g_libuav_cfg) {
			for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
				if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
					for(i=0; i<g_libuav_cfg->iad[k].astrmif_cnt; i++){
						if(g_libuav_cfg->iad[k].astrmif[i].filldata){
							g_libuav_cfg->iad[k].astrmif[i].filldata(g_libuav_cfg->iad[k].astrmif + i);
						}
					}
				}
			}
		}
	}
	goto reinit;
	return 0;
}
\endcode
	  * -# interrupt handler
\code
void static libusb_suspend_irq_handler(void){
	lowlevel_putc('U');
	lowlevel_putc('_');
	lowlevel_putc('s');
	if(g_libusb_cfg.status == UDEVSTS_CONFIG) {
		g_libusb_cfg.status = UDEVSTS_UNKNOWN;
		libusb_suspend();
	}
}

IRQ_HANDLER_DECLARE_START
...	\\ other IRQ MAP
IRQ_MAP(USBSUSPEND, libusb_suspend_irq_handler);
IRQ_HANDLER_DECLARE_END
\endcode
   * - <b>NOTES</b>:
   * - <b>FAQ</b>:
*/

/**
  * @brief uac control interface input/output terminal id structure.
  */
typedef struct s_libuac_tid
{
	u8 IT_ID;
	u8 OT_ID;
}t_libuac_tid;

/**
  * @brief uac control interface data structure.
  */
typedef struct s_libuac_ctrlif
{
	 u8 CTRLIF_ID;   ///<uac control interface id
	 u8 FU_ID_MIN;  ///<uac feature min id
	 u8 FU_ID_MAX; ///<uac feature max id
	 u8 SU_ID; ///<uac selector id
	 t_libuac_tid *tid; ///<uac input/output terminal id structure
	 u8 tid_cnt; ///<uac count of input/output terminal id

	 //uac private data
	 u8 mute; ///<auido mute vale: 1 for mute, 0 for normal
	 u8 agc;///<audio agc value
	 u16 volume; ///<audio volume value

     void (*ac_fu_handler)(u32 id, u32 type, u32 value);  ///<callback for uac feature unit handler
}t_libuac_ctrlif;

/**
  * @brief uac stream interface data structure.
  */
typedef struct s_libuac_strmif
{
#define AUDIO_STREAM_APP_UNKNOW	0x00
#define AUDIO_STREAM_APP_MIC	0x01
#define AUDIO_STREAM_APP_SPK	0x02
	 u8 STRMIF_ID;  ///<uac stream id
	 u8 STRM_EP; ///<uac stream endpoint
	 u16 STRM_PYLSIZE; ///<uac stream payload size
	 u8 STRMLINK_ID; ///<uac stream link terminal id
	 u8 STRM_APP; ///<uac stream application type(mic or spk)

	 u16 format; ///<uac stream data format, for example: PCM

	 u32 *sr_all;///< all sample rates supported 
	 u8 sr_cnt; ///< sample rate count of sr all
	 u8 chn_cnt;  ///<total channels in this stream

	 u8 a_status; ///<uac device's status

	 void (*filldata)(struct s_libuac_strmif *); ///<callback to fill this uac stream data
	 void (*setif)(struct s_libuac_strmif *strmif, int alt); ///<callback to set interface
	 int (*setsamplerate)(struct s_libuac_strmif *strmif, u32 asample_rate); ///<callback to set audio sample rate
	 u32 (*getsamplerate)(void); ///<callback to get audio sample rate
	 void (*filldone)(void); ///<callback to deal with the uac stream data when complete to fill.
}t_libuac_strmif;

//INTERNAL_START

/* uac audio volume control */
#define UAC_VOLUME_MIN  0xf7ff
#define UAC_VOLUME_MAX  0x1681
#define UAC_VOLUME_RES  0x10

// audio control feature selector
#define AUDIO_CTRL_UNDEFINED          0x00
#define AUDIO_CTRL_MUTE               0x01
#define AUDIO_CTRL_VOLUME             0x02
#define AUDIO_CTRL_BASS               0x03
#define AUDIO_CTRL_MID                0x04
#define AUDIO_CTRL_TREBLE             0x05
#define AUDIO_CTRL_GRAPHIC            0x06
#define AUDIO_CTRL_AGC                0x07
#define AUDIO_CTRL_DELAY              0x08
#define AUDIO_CTRL_BASS_BOOST         0x09
#define AUDIO_CTRL_LOUDNESS           0x0A

//INTERNAL_END

#endif
