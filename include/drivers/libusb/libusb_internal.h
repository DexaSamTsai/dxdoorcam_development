#ifndef _LIBUSB_INTERNAL__H_
#define _LIBUSB_INTERNAL__H_

#define MAX_EP0_PKTSIZE   64

extern t_ertos_eventhandler evt_udc;
extern volatile int  libusb_chip_isr;
extern volatile int  libudc_irq;
extern volatile int  libep_irq;

#define EPBUF_IS_FULL(EP)  (ReadReg32(UDCIF_EP_CFG3(EP)) & 0x8000)
#define EPBUF_IS_EMPTY(EP)  (ReadReg32(UDCIF_EP_CFG3(EP)) & 0x4000)
#define EPBUF_IS_NOTEMPTY(EP)  (ReadReg32(UDCIF_EP_CFG3(EP)) & 0x7c000000)
#define DATACNT_OF_EPBUF(EP)  (ReadReg32(UDCIF_EP_CFG3(EP)) & 0x3fff)

#define MASK_OF_EPINTR(EP)  (1<<((EP)-1))

#define ENABLE_EPINTR(EP)  WriteReg32(UDCIF_EP_CFG1(EP), (ReadReg32(UDCIF_EP_CFG1(EP)) & ~0x7) | 0x2);
#define DISABLE_EPINTR(EP)  WriteReg32(UDCIF_EP_CFG1(EP), (ReadReg32(UDCIF_EP_CFG1(EP)) & ~0x2))
#define EPINTR_IS_ENABLED(EP) ((ReadReg32(UDCIF_EP_CFG1(EP))&BIT1)>>1)

#define BIT_VIDEO_EN   BIT31
#define DISABLE_VIDEO_EP  WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) & ~BIT_VIDEO_EN);
#define ENABLE_VIDEO_EP  WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) | BIT_VIDEO_EN);

#define BIT_VIDEO_RST  BIT30

#define HS_MAX_PKTSIZE_ISO   0x400
#define HS_MAX_PKTSIZE_BULK  0x200
#define HS_MAX_PKTSIZE_INTR  0x400
#define HS_MAX_PKTSIZE_CTRL  0x40

#define FS_MAX_PKTSIZE_ISO   0x380
#define FS_MAX_PKTSIZE_BULK  0x40
#define FS_MAX_PKTSIZE_INTR  0x40
#define FS_MAX_PKTSIZE_CTRL  0x40

#define HS_HW_MAX_PKTSIZE_ISO   0x400

#define ENABLE_UVC_DP_YUV_CIF WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x00<<25))
#define ENABLE_UVC_DP_MIMG WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x01<<25))
#define ENABLE_UVC_DP_ENCV WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x02<<25))
#define ENABLE_UVC_DP_YUV_SCALE  WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x03<<25))
#define ENABLE_UVC_DP_YUV_ECIF  WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x04<<25))
#define ENABLE_UVC_DP_YUV_MIPI  WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x05<<25))
#define ENABLE_UVC_DP_YUV_COLOR  WriteReg32(REG_SC_CTRL3, (ReadReg32(REG_SC_CTRL3) & 0xf1ffffff) | (0x06<<25))

#endif
