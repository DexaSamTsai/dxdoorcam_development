#ifndef _LIBUSB_USER__H_
#define _LIBUSB_USER__H_

#include "includes.h"

/**
  * @defgroup libusb_hal
  * @ingroup lib_interface
  * @brief This group APIs are designed as HAL(Hardware abstract level) for usb device.<br><br>
  * @details OV780 chip acts as a low-level usb device, it sereves usb upper-level driver such as usb video class, usb audio class, and usb mass storage etc. those APIs is hardware closely related. eg: config csr & endpoint, and so on. <br><br>
  <p>
   * -# <b>Source Codes</b>:
     share/rXXX/libusb_chip/usb.c for usb module initilization and configuration, including low-level usb endpoint read/write .
   * -# <b>Depends On</b>:
   * -# <b>Used By</b>:
      utilize this library when using client driver on usb function device.
   * -# <b>Design Doc</b>:
   * -# <b>NOTES</b>:
   * -# <b>FAQ</b>:
   * @{
*/

/**
  * @brief this is CSR data structure, which use to config USB IP.
  */
typedef struct s_csrdata
{
	u32 addr;   ///<csr data address
	u32 data;   ///<csr data value
	u32 ctrl;  ///<csr control flag
}t_csrdata;

extern t_csrdata g_csrdata[];

/**
  * @brief usb hi-speed config descriptor declaration.
  */
extern u8 cfgConf_HI[];

/**
  * @brief usb full-speed config descriptor declaration.
  */
extern u8 cfgConf_FL[];

/**
  * @brief usb hi-speed device descriptor declaration.
  */
extern u32 cfgDevice_HI[];

/**
  * @brief usb full-speed device descriptor declaration.
  */
extern u32 cfgDevice_FL[];

/**
  * @brief usb string(index 0) descriptor declaration.
  */
extern u32 cfgStr0[];

/**
  * @brief usb string(index 1) descriptor declaration.
  */
extern u32 cfgStr1[];

/**
  * @brief usb string(index 2) descriptor declaration.
  */
extern u32 cfgStr2[];

/**
  * @brief usb string(index 3) descriptor declaration.
  */
extern u32 cfgStr3[];

/**
  * @brief usb qualifier config descriptor declaration.
  */
extern u32 cfgQualifier[];

/**
  * @brief usb other config descriptor declaration.
  */
extern u32 cfgConf_Other[];

/**
  * @brief usb control request stages, it's used to handle request.
  */
typedef enum e_udev_ctrl_stage
{
	UDEV_SETUP_STAGE = 0,
	UDEV_DATA_STAGE,
	UDEV_STATUS_STAGE,
}t_udev_ctrl_stage;

/**
  * @brief usb device request error code.
  */
typedef enum e_udev_errcode
{
	UDEV_NO_ERROR = 0x00,
	UDEV_NOT_READY,
	UDEV_WRG_STATE,
	UDEV_POWER,
	UDEV_OUT_OF_RANGE,
	UDEV_INVALID_UNIT,
	UDEV_INVALID_CONTROL,
	UDEV_INVALID_REQUEST,
	UDEV_UNKNOWN = 0xff,
}t_udev_errcode;

/**
  * @brief usb device request response type.
  */
#define SEND_ACK_NEXTDATA		 0x01
#define SEND_STALL_NEXTDATA		 0x02
#define SEND_ACK_NEXTSTATUS		 0x04
#define SEND_STALL_NEXTSTATUS	 0x08
#define SEND_ACK_ALLDATA		 0x10

/**
  * @brief usb device status.
  */
typedef enum e_udevsts
{
	UDEVSTS_UNKNOWN = 0,
	UDEVSTS_INIT,
	UDEVSTS_CONFIG,
	UDEVSTS_SUSPEND,
}t_udevsts;


/**
  * @brief usb device isr return type.
  */
typedef enum e_usbisrret
{
	USBISRRET_OK,
	USBISRRET_REBOOT,
}t_usbisrret;

/**
  * @brief this function is initilization of usb device module.
  */
void libusb_init(void);

/**
  * @brief this function is working thread of usb device module.
  */
void libusb_start(void);

/**
  * @brief exit umass.
  */
void libusb_exit(void);

/**
  * @brief call this function when usb module is in suspend mode.
  */
void libusb_suspend(void);

/**
  * @brief enter usb boot mode
  */
void boot_from_usb(void);

/**
  * @brief usb isr handler for usb module.
  */
t_usbisrret libusb_loop(void);

/**
  * @brief usb ep0 response function.
  */
void udcif_request_sendack(u8 type, u8 loop);

/**
  * @brief usb ep0 data-write function.
  */
u32 udc_writeEP0(u32 *buffer, u8 length, u8 swap);

/**
  * @brief usb ep0 data-read function.
  */
u32 udc_readEP0(u8 *buffer, u8 length);

/**
  * @brief general endpoint data-write function.
  * @param EPNum is endpoint number, pData is data buffer that must be 4-aligned, cnt is buffer size.
  * @ret the size of data that be written.
  */
u32 udc_writeEP(u32 EPNum, u8 *buffer, u32 length);
u32 udc_writeISO(u32 EPNum, u8 *buffer, u32 length);

/**
  * @brief general endpoint data-read function.
  * @param EPNum is endpoint number, pData is data buffer that must be 4-aligned.
  * @ret the size of data that be read.
  */
u32 udc_readEP(u32 EPNum, u8 *buffer);

/**
  * @brief general endpoint stall function.
  */
void udc_stallEP (u32 EPNum);

/**
  * @brief usb general endpoint configuration function for UDCIF.
  */
void udcif_setcfg(void);

/**
  * @brief general endpoint type define.
  */
#define EPTYPE_ISO 1
#define EPTYPE_BULK 2
#define EPTYPE_INTR 3

#define EPTYPE_HW	0x40

#define EPNUM_MASK 0xf

/**
  * @brief general endpoint data structure. you can refer to spec UDC_IF.doc for more detail.
  */
typedef struct s_libusbep_cfg
{
	u8 ep; ///<endpoint address, D7:direction, 0=OUT. D6..4:zero. D3..0:ep number
	u8 type; ///<endpoint type
	u8 intr_th; ///<interrupt thresh-hold of endpoint: using buffer cnt as measurement  
	u8 baseaddr; ///<base addr of endpoint, using 512bytes as measuerment.
	u8 buffernum; ///<buffer number of endpoint, using 512 or 1024 as measuerment.
	u16 maxpktsize;///<max packet size of endpoint
	u8 interval; ///<interval for polling endpoint for data transfer
}t_libusbep_cfg;

/**
  * @brief usb client class config data structure.
  */
typedef struct s_libusbclass_cfg
{
	t_libusbep_cfg *epcfg; ///<the endpoints data
	u8 epnum; //total epcfg in the arrary 'epcfg' .
	void *arg; //private data structure pointer to client class, like t_libuav_cfg
	void (*handleEP)(u32 *args);  //private endpoint class process callback
	void (*handleGRFC)(void);  //private grfc process callback
}t_libusbclass_cfg;

/**
  * @brief usb class data structure for high speed.
  */
extern t_libusbclass_cfg g_libusbclass_cfg_hs; 

/**
  * @brief usb class data structure for full speed.
  */
extern t_libusbclass_cfg g_libusbclass_cfg_fs;

/**
  * @brief current class data structure, which define in libusb, and point to _hs or _fs in udcif_setcfg().
  */
extern t_libusbclass_cfg *g_libusbclass_cfg; 

extern int usbclass_init(int first);
extern int usbclass_HandleRequest(void);
extern void usbclass_HandleSetif(int if_id, int alt);
extern void usbclass_clean(void);

/**
  * @brief usb auxiliary funtion.
  */
extern void libusb_swap_char(unsigned char *pbuf, int len);
extern void libusb_dump_data(unsigned char *pbuf, int cnt);
extern void usb_debug_enable(void);

#endif
