#ifndef __LIBUSB_MSCUSER_H__
#define __LIBUSB_MSCUSER_H__

#include "includes.h"

/**
 * @defgroup libumass
 * @ingroup lib_midware
 * @brief This group APIs are designed for usb mass storage class(bulk-only protocl).<br><br>
 * @details OV780/OV788 chip acts as a usb device, while PCs acts usb host. the host and device communication by exchange command blocks, data, and status information accordinig to the Bulk-Only specification.<br><br>
 * OV780/OV788 supports most of, but not all of the UFI command. the umass supports two mode: R/W and ReadOnly, and supports three kinds of medium type: scif, nandflash and ramdisk. <br><br> 
 <p>
 * - <b>Source Codes</b>:
      share/libumass_data_XXX.c for a kind of medium's initialization and configuration.
 * - <b>Depends On</b>:
 * - <b>Used By</b>:
      utilize this library when umass funtion is needed.
 * - <b>Design Doc</b>:
 	* -# How to add a new kind of medium as umass's LUN
\code
Take adding ramdisk meduim as umass LUN for example:
step 1: create a directory named libumass_data_ramdisk under share/, and create a file named umass.c under share/libumass_data_ramdisk.

step 2: define a global data structure in the file umass.c
     __lundriver_cfg t_lundriver_cfg g_ramdisk_lundriver = {
        //configure ramdisk's operation(init/read/write) function.
        .int = xxxx_init,
        .msc_read = xxxx_read,
		.msc_write = xxxx_write,
     };
     The data structure above must be put on section "__lundriver_cfg".

step 3: init ramdisk medium on function msc_ramdisk_init.
    void msc_ramdisk_init(void) 
	{
	    // configure parameter of the medium
		// OV780/OV788 support two mode: R/W, ReadOnly. 0 means ReadOnly, 1 means Read/Write.
        g_ramdisk_lundriver.medium_removable = XXX; 
		// block_cnt is the total sectors of the medium.
        g_ramdisk_lundriver.block_cnt = XXX; 
		// block_size is the bytes per sector.
        g_ramdisk_lundriver.block_size = XXX; 
		// size is the total bytes of the medium.
        g_ramdisk_lundriver.size = XXX; 
		// lun_arg is the private argument that msc_read/msc_write will use.
        g_ramdisk_lundriver.lun_arg = XXX; 
		...
    }

step 4: create a file name umass_ramdisk.c on directory share/libumass_data_ramdisk, complete the read/write medium function.
    // the 1st argument is the private argument perserved on data structure g_ramdisk_lundriver.lun_arg, which is inited on msc_ramdisk_init; the 2nd argument is the read/write offset bytes, the 3rd is buffer pointer; adn the 4th is read/write length.
    void ramdisk_rd(u32 arg, u32 off, int buf, u32 len) 
	{
	    ...
	}
    void ramdisk_wr(u32 arg, u32 off, int buf, u32 len) 
	{
	    ...
	}

step 5: add compile condition in make/config.env and include/Kconfig.
    1.in file config.env, add 
	  ifeq ($(FS_RAMDISK), 1)
      endif
	2. in file Kconfig, add
	  if USB_UMASS
	  config FS_RAMDISK
	      bool "ramdisk filesystem enable"
		  help
		       enable ramdisk filesystem
	  endif
	  then you can enable/disable the ramdisk in Kconfig.

\endcode
 * - <b>Sample Code</b>:
      * -# init and loop
\code
static void usb_sys_init(void){
	\\ usb clock config if necessary

	IRQ_DISABLE_ALL;

	memset(&g_libusb_cfg, 0, sizeof(g_libusb_cfg));
	libusb_init();

	\\ enable usb suspend intr
	enable_irq(USBSUSPEND);
}

int main(void){
	....
	....
	e_usbisrret uret;

	usb_sys_init();
reinit:
	while (1) {
		uret = USB_loop();
		if(uret == USBISRRET_REBOOT){
			break;
		}
	}
	usb_sys_init();
	goto reinit;
	return 0;
}
\endcode
	  * -# usb interrupt handler
\code
void static libusb_suspend_irq_handler(void){
	lowlevel_putc('U');
	lowlevel_putc('_');
	lowlevel_putc('s');
	if(g_libusb_cfg.status == UDEVSTS_CONFIG) {
		g_libusb_cfg.status = UDEVSTS_UNKNOWN;
		libusb_suspend();
	}
}

IRQ_HANDLER_DECLARE_START
...	\\ other IRQ MAP
IRQ_MAP(USBSUSPEND, libusb_suspend_irq_handler);
IRQ_HANDLER_DECLARE_END
\endcode
  * - <b>NOTES</b>:
  * - <b>FAQ</b>:
  * @{
  */

/**
  * @brief define the buffer size of umass, this buffer is used to cache the data wr/rd to medium.
  */
#define MSC_MAX_TOTAL_BUF   0x8000

/**
  * @brief the max LUN(medium) count that umass can support.  
  * note: a LUN means a logical unit number on a device.  
  */
#define MSC_MAX_LUNCNT  3  

/**
  * @brief define Bulk OUT endpoint of umass module.  
  */
#define MSC_ENDPOINT_BULKIN      0x84

/**
  * @brief define Bulk OUT endpoint of umass module.  
  */
#define MSC_ENDPOINT_BULKOUT     0x03

/**
  * @brief define the debug macro.  
  *  UMASS_CMD_DEBUG is used for scsi cmd debug, eg: print out SCSI cmd detail msg etc; 
  *  UMASS_FATAL_ERROR is used for fatal error, eg: data buffer overun etc;  
  *  UMASS_WARNING is used for module warning;   
  */
#define UMASS_CMD_DEBUG      1
#define UMASS_FATAL_ERROR    1
#define UMASS_WARNING        1

/**
  * @brief data structure for a LUN. 
  */
typedef struct s_lundriver_cfg{
	u32   size;           ///< total medium size
	u32   block_cnt;      ///< total medium sectors
	u32   block_size;     ///< bytes per sector
	u32   lun_arg;    ///< private argument for medium read/write

#define  UMASS_LUN_REMOVABLE	0x1
#define  UMASS_LUN_WRPROTECT	0x2
#define  UMASS_LUN_WORKING	0x80000000
	u32   lun_param; ///< medium porperiety: read only or R/W, write protect or no

#define UMASS_ID_NULL	 0x1
#define UMASS_ID_RAM	 0x2
#define UMASS_ID_SCIF	 0x4
#define UMASS_ID_NAND	 0x8
#define UMASS_ID_CAMERA	 0x10
	u16   lun_id;    ///< private argument for medium read/write

	u16   lun_private; ///< medium porperiety: read only or R/W, write protect or no
	char *product_info; ///<medium product info: the maxium size is 20byte

	/**
	  * @brief call back to initialize one medium for a umass LUN 
	  *
	  * @param void
	  * @return 0 for ok, <0 for error
	  */
	int   (*init)(void);  ///<return 0 for ok, <0: error

	/**
	  * @brief call back to read data from a umass LUN 
	  *
	  * @param [in] arg: private data structure
	  * @return size to read, in bytes 
	  */
	int   (*msc_read)(u32 arg, u32 off, int buf, u32 len);

	/**
	  * @brief call back to write data to a umass LUN 
	  *
	  * @param [in] arg: private data structure
	  * @return size to write, in bytes 
	  */
	int   (*msc_write)(u32 arg, u32 off, int buf, u32 len);
}t_lundriver_cfg;

/**
  * @brief define string which vendor descripte the product. 
  */
extern char strMscVendor[8];

/**
  * @brief define transaction buffer that umass used to cache data.
  */
extern u8 msc_bulk_buf[MSC_MAX_TOTAL_BUF];

//INTERNAL_START
/**
  * define function for umass's bug on OV78x, which is existed
  * when the buffer count occupied by usb data is smaller than the max buffer count.
  */
void  msc_epout_fix(void);

/**
  * @brief define usb's endpoint read function for bulk-only protocol 
  */
void  handle_msc_bulkout(void);

/**
  * @brief define usb's endpoint write function for bulk-only protocol
  */
void  handle_msc_bulkin(void);

/**
  * @brief define add sd media function for umass
  */
int umass_add_lun(void);

/**
  * @brief define remove sd media function for umass
  */
int umass_remove_lun(void);
//INTERNAL_END

/** @}*/  //end libumass
#endif  /* __LIBUSB_MSCUSER_H__ */
