#ifndef _LIBUSB__H_
#define _LIBUSB__H_

#define USB_UART_PRINT
#define USB_DEBUGREG
#define  USB_BIG_ENDIAN           // endian mode for data pipe

#include "libusb_internal.h"

#define MAX_UDCIF_BUFSIZE   10
#define MIN_UDCIF_BUFSIZE   4

#define MAX_REQBUF_SIZE 64 //max buffer for USB request data

typedef struct s_udev_req
{
	u8 bmRequestType;
	u8 bRequest;   
	u8 wValue_L;  
	u8 wValue_H;   
	u8 wIndex_L;
	u8 wIndex_H;
	u8 wLength_L;
	u8 wLength_H;
}t_udev_req;

#include "libusb_user.h"

typedef struct s_libusb_request
{
	t_udev_req request;

	u32 *buffer;
	u8 databuf[MAX_REQBUF_SIZE]; //buffer for both input and output, when output, set buffer to this address
	int (*HandleRequest_cb)(void);
	u16 wLength;
	t_udev_errcode error_code;
	t_udev_errcode last_errcode; //save last time's ErrorCode
}t_libusb_request;

typedef struct s_libusb_cfg
{
	t_libusb_request req; ///< setup packet structuer, see data structure descriptor for detail info.

	u8 config; ///< usb device config parameter
///<setting this indicate usb device is't self-powered, otherwise it's bus-powered.
#define UDEVCFG_SELPOWER    0x1  
///<setting this indicate usb device will be forced to run on full-speed(12M).
#define UDEVCFG_FLSPEED     0x2 
///<this indicate usb device run on big endian.
#define UDEVCFG_BIGENDIAN   0x4
	u8 bHighSpeed; ///<indicate usb device's speed.
	u8 bInitOnce; ///<inited some parameter once according its value.
	u8 bHandled; ///<indicate wehther the setup packet is handled.

	t_udevsts status; ///<indicate usb device's status
	u32 intr_enable; ///<the usb interrupt enable mask
}t_libusb_cfg;

extern t_libusb_cfg g_libusb_cfg;

#include "libusb_ch9.h"
#include "libusb_uac.h"
#include "libusb_uac_std.h"
#include "libusb_uvc.h"
#include "libusb_msc.h"
#include "libusb_mscuser.h"
#include "libusb_printer.h"
#include "libusb_serial.h"
#include "libusb_vendormsc.h"

#endif
