#ifndef __LIB_USB_MSC_VENDOR__H
#define __LIB_USB_MSC_VENDOR__H


//vendor msc command block wrapper
#define VMSC_CBW_LEN	(sizeof(t_vmsc_cbw))
typedef struct s_vmsc_cbw {
#define VMSC_CBW_SIGN		0x55534243
	u32le signature;//magic number is 0x55534243
	u32le data_len;//in/out data length
	u8 flag;//bit7 use for data packet direction, '1' is in, '0' is out, other bits reserved.

	u8 cmd;//command id 
	u8 cmd_len;//the valid length of command parameter
	u8 cmd_arg[64];//command parameter
}t_vmsc_cbw;

//vendor msc command status wrapper
#define VMSC_CSW_LEN	0x09
typedef struct s_vmsc_csw {
#define VMSC_CSW_SIGN		0x55534253
	u32le signature;//magic number is 0x55534253
	u32le data_residue;
#define VMSC_CMD_STATUS_PASS			0x00
#define VMSC_CMD_STATUS_FAIL			0x01
#define VMSC_CMD_STATUS_PARSE_ERROR		0x02
	u8 status;
}t_vmsc_csw;

typedef struct s_libvmsc_cfg
{
	u8 ep_in;//bulk-in endpoint number
	u8 ep_out;//bulk-out endpoint number

	u8 MSCIF_ID;

	u8 *rx_buffer;
	u32 rx_offset;
	u32 rx_len;

	u8 *tx_buffer;
	u32 tx_offset;
	u32 tx_len;

	u8 vmsc_cmd;//it is a copy of struct s_vmsc_cbw.cmd, be updated only when accept CBW packet successfully
	
#define VMSC_STAGE_CBW			0x00
#define VMSC_STAGE_CSW			0x01
#define VMSC_STAGE_DATAOUT		0x02
#define VMSC_STAGE_DATAIN		0x03
#define VMSC_STAGE_CMD_HDL		0x04
	u8 vmsc_stage;

	void (*init)(struct s_libvmsc_cfg *vmsc_cfg); 

	/* vendor msc class callback function */
	void (*Handle_VMSC)(void);

	u32 efs_buf_start;
	u32 efs_buf_cnt;
	u32 fr_buf_base;//filerecord buffer start address

	u32 fw_update_base;//firmware update buffer
	u32 fw_crc;//firmware crc
	u8 fw_ready;//firmware download ready
	u8 file_opend;
	u8 card_status;//0: no sd card, 1: sd card found

}t_libvmsc_cfg;

#define VMSC_FR_MEMORY_FIX(speed, addr, fw_update_addr, efs_buf, efs_buf_size)	\
do {	\
	t_libuav_cfg *cfg = (t_libuav_cfg *)(g_libusbclass_cfg_##speed.arg);	\
	t_libvmsc_cfg *vmsc_cfg = (t_libvmsc_cfg *)(cfg->extra_cfg);	\
	vmsc_cfg->fr_buf_base = addr;	\
	vmsc_cfg->fw_update_base = fw_update_addr;	\
	vmsc_cfg->efs_buf_start = efs_buf;	\
	vmsc_cfg->efs_buf_cnt = efs_buf_size;	\
}while(0)

void vmsc_cmd_fs_hdl(void);

#endif	//__LIB_USB_MSC_VENDOR__H
