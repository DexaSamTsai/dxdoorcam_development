#ifndef _LIBUSB_SERIAL__H_
#define _LIBUSB_SERIAL__H_

#define  ACM_SET_COMMFEATURE     0x02
#define  ACM_GET_COMMFEATURE     0x03
#define  ACM_CLR_COMMFEATURE     0x04
#define  ACM_SET_LINECODING      0x20
#define  ACM_GET_LINECODING      0x21
#define  ACM_SET_CTRLLINESTATE   0x22
#define  ACM_SEND_BREAK          0x23

#define CONTROL_DTR          0x01
#define CONTROL_RTS          0x02

#define CDC_DCD				0x01 
#define CDC_DSR				0x02 
#define CDC_BREAK		     0x04	// break reception 
#define CDC_RI			     0x08 
#define CDC_FRAME		     0x10	// frame error 
#define CDC_PARITY		     0x20	// parity error 
#define CDC_OVRRUN		     0x40	// overrun error 
#define CDC_CTS			     0x80	// - CTS is not supported in generic CDC

#define SERIAL_STATE_NUM     0x0a

#define	VSER_EP_INTRIN	0x81
#define	VSER_EP_BULKIN	0x84
#define	VSER_EP_BULKOUT	0x03

typedef struct s_libcdc_cfg
{
//-----------------------------------------------------------------------------   
//   
// Interrupt IN EP - Notification element   
//  Return SerialState notification response (10 bytes) to host    
//  whenever the status are changed   
//   
//  response   
//    0:  0xa1   bmRequestType   
//    1:  0x20   bNotification (SERIAL_STATE)   
//    2:  0x00   wValue   
//    3:  0x00   
//    4:  0x00   wIndex (Interface #, LSB first)   
//    5:  0x00   
//    6:  0x02   wLength (Data length = 2 bytes, LSB first)   
//    7:  0x00   
//    8:  xx     UART State Bitmap (16bits, LSB first)   
//    9:  xx   
//   
//  UART State Bitmap   
//    15-8: reserved   
//    7:  (no spec extra) CTS   
//    6:  bOverRun    overrun error   
//    5:  bParity     parity error   
//    4:  bFraming    framing error   
//    3:  bRingSignal RI   
//    2:  bBreak      break reception   
//    1:  bTxCarrier  DSR   
//    0:  bRxCarrier  DCD   
//   
//  called by UDC interrupt   
//----------------------------------------------------------------------------- 
	u8 CDC_EP_IN;  // bulk in endpoint
	u8 CDC_EP_OUT; // bulk out endpoint

	u8 line_state;
	u32 baud_rate;
#define CDC_STATUS_IDLE  (0)
#define CDC_STATUS_READY (1)
	u32 status;
	/* CDC Bulk Callback Functions */
	void (*cdc_bulk_in)(void);
	void (*cdc_bulk_out)(void);
}t_libcdc_cfg;

extern t_libcdc_cfg g_libcdc_cfg;

void handle_cdc_bulkout(void);
void handle_cdc_bulkin(void);

#endif
