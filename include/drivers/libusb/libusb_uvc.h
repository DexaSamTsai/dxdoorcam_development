#ifndef _LIBUSB_UVC__H_
#define _LIBUSB_UVC__H_

/**
  * @defgroup libuvc
  * @ingroup lib_midware
  * @brief This group APIs are designed for usb video class.<br><br>
  * @details OV780/OV788 chip acts as a usb cameral device, while PCs acts usb host. the host and device communication by the way uvc protocl, which according to usb video class specification 1.0.<br><br>
  * OV780/OV788 supports four kinds of data format: yuv422, mimg, encode ts/raw at moment. in addition, we support 4 kinds of topology: yuv422+uac, mimg+uac, ts+uac, mimg+ts+uac, in additional, OV788 also supports encoded ts+mimg or encoded raw data+mimg<br><br>
  <p>
  * - <b>Source Codes</b>:
     share/libuav_cfg_XXX.c is uvc topology's configuration, while share/rXXXX/libuvc_data_XXX is uvc data stream configuration. in the end, share/libuvc_request is class-specific request handler for uvc. 
  * - <b>Depends On</b>:
  * - <b>Used By</b>:
     utilize this library when uvc funtion is enable.
  * - <b>Design Doc</b>:
\code
step 1:create a directory named libuav_cfg_XXX_s under directory ./share/, and create a file named uvc_XXX.c. in the file, you must fill the all kinds of data structure, and all kinds of usb descriptor.
    
step 2: create a directory name libuvc_data_XXX under directory ./share/rXXXX/. and create a file named uvc_XXX.c. in the file, you must fill those function: XXX_HandleSetif, XXX_frame_set. if it's not hardware-encode, you must realise XXX_filldata function. 

step 3: create a project and add necessary files under directory /ram/. at the same time, add compile condition in in make/config.env and include/Kconfig.
\endcode
  * - <b>Sample Code: transfer encoded ts by usb</b>:
  	  * -# initialize video datapath, detailed codes see ram/uvc_av.board_xxxx.c
\code
	  // select a datapath 
	  t_libuvcstrm_frame _strm_encv_frame_hs[] = 
	  {
	  	{
			.size = VIDEO_SIZE_720P,
			.framerates = NULL,
			.framerate_cnt = 0,
		},
		{
			.size = VIDEO_SIZE_VGA,
			.framerates = NULL,
			.framerate_cnt = 0,
		},
		{
			.size = VIDEO_SIZE_QVGA,
			.framerates = NULL, 
			.framerate_cnt = 0,
		}
		{ 
			.size = VIDEO_SIZE_QCIF,
			.framerates = NULL,
			.framerate_cnt = 0,
		},
		{ 
			.size = VIDEO_SIZE_QQVGA,
			.framerates = NULL,
			.framerate_cnt = 0,
		}
	  };
	
	  t_libuvcstrm_frame _strm_encv_frame_fs[] =
      {
		{ 
			.size = VIDEO_SIZE_VGA,
			.framerates = NULL,
			.framerate_cnt = 0,
		},
		{ 
			.size = VIDEO_SIZE_QVGA,
			.framerates = NULL,
			.framerate_cnt = 0,
		},
		{ 
			.size = VIDEO_SIZE_QQVGA,
			.framerates = NULL,
			.framerate_cnt = 0,
		}
	  };
	  void uvc_framecnt_fix(void)
	  {
		UVC_STREAM_FRAMECNT_FIX(hs, ENC, FRAME_BASED, _strm_encv_frame_hs, NULL);
		UVC_STREAM_FRAMECNT_FIX(fs, ENC, FRAME_BASED, _strm_encv_frame_fs, NULL);
	  }
\endcode
      * -# usb initialization and loop, detailed codes see ram/sample_udev
\code
	
	void usb_task(void){
		....
		....

		libusb_init();
		libusb_start();
		....
	}

	void uvc_tasktwo(void *p_arg)
	{
		...
		usb_task();
	}

	void task_one(void)
	{
		...
		//TODO:initailize and start video encoder, detailed codes see ram/uvc_av/main_encvideo.c	
	}
\endcode
	  * -# interrupt handler
  * - <b>NOTES</b>:
  * - <b>FAQ</b>:
  */

//INTERNAL_START
/******************************************
        uvc configuration
 *******************************************/

// uav control request code, referring to UVC spec1.0
#define UAV_SET_CUR     0x01
#define UAV_GET_CUR     0x81
#define UAV_GET_MIN     0x82
#define UAV_GET_MAX     0x83
#define UAV_GET_RES     0x84
#define UAV_GET_LEN     0x85
#define UAV_GET_INFO    0x86
#define UAV_GET_DEF     0x87

//interval = 10000000 / framerate
#define FRAMEINTERVAL_60 	0x28b0a
#define FRAMEINTERVAL_30 	0x51615
#define FRAMEINTERVAL_24 	0x65b9a
#define FRAMEINTERVAL_20 	0x7a120
#define FRAMEINTERVAL_15 	0xa2c2b
#define FRAMEINTERVAL_12 	0xcb736
#define FRAMEINTERVAL_10 	0xf4240
#define FRAMEINTERVAL_7 	0x145856
#define FRAMEINTERVAL_5 	0x1e8480
#define FRAMEINTERVAL_3 	0x28b0ab
#define FRAMEINTERVAL_1 	0x989680

#define PU_CONTROL_START1 0

// selector unit control selectors
#define SU_INPUT_SELECT_CONTROL		0x1

// video class-spedific type define  
#define VS_FORMAT_UNCOMPRESSED   0x4  //yuy
#define VS_FRAME_UNCOMPRESSED    0x5  
#define VS_FORMAT_MIMG          0x6  //mimg
#define VS_FRAME_MIMG           0x7  
#define VS_FORMAT_MPEG2TS        0xa  //ts
#define VS_FORMAT_FRAME_BASED	 0x10 //encv
#define VS_FRAME_FRAME_BASED	 0x11
#define VS_FORMAT_STREAM_BASED	 0x12 //encv
#define VS_FORMAT_VENDOR_ENCV	 0x81 //vendor

// video control selector
#define VS_PROBE_CONTROL   0x1
#define VS_COMMIT_CONTROL  0x2
#define VS_STILL_PROBE_CONTROL			0x3
#define VS_STILL_COMMIT_CONTROL			0x4
#define VS_STILL_IMAGE_TRIGGER_CONTROL  0x5

// uvc descriptor 
#define UVC_DESC_DEVICE		 0x21
#define UVC_DESC_CFG		 0x22
#define UVC_DESC_STRING		 0x23
#define UVC_DESC_INTERFACE	 0x24
#define UVC_DESC_ENDPOINT	 0x25

#define UVC_FLAG_FRM_START 	0x80000000
#define UVC_FLAG_FRM_END 	0x40000000
#define UVC_FLAG_NODATA 	0x20000000
//INTERNAL_END

/**
  * @brief uvc control interface data structure.
  */

/**
  * @brief uvc control interface input/output terminal id structure.
  */
typedef struct s_libuvc_tid
{
	u8 IT_ID;
	u8 OT_ID;
}t_libuvc_tid;

typedef struct s_libuvc_ctrlif
{
	u8 DO_SNR_EFFECT_PROC; ///<flag to callback sensor_effect_proc();
	u8 CTRLIF_ID;  ///<uvc control interface number
	u8 SU_ID;  ///<uvc selector unit id
	u8 PU_ID;  ///<uvc process unit id
	u8 XU_ID;  ///<uvc extension unit id
	u8 H_XU_ID;  //<uvc encv extension unit id
	t_libuvc_tid *tid; ///<uvc input/output terminal id structure
	u8 tid_cnt; ///<uvc count of input/output terminal id

	u8 XU_bmControl; ///<uvc extension unit bmControl value
	char *XU_GUID; ///<uvc extension unit GUID value
	u8 encv_init;
	u8 powermode;  ///<uvc power mode, called in HandleRequest_Power_Control
	int (*xu_handle_cb)(void);	///<return '0' if successful handle request, or return '1'
	int (*encv_req_handle)(struct s_libuvc_ctrlif * vctrlif);
}t_libuvc_ctrlif;

/**
  * @brief uvc stream framerate data structure.
  */
typedef struct s_libuvcstrm_framerate
{
	u8 framerate;
}t_libuvcstrm_framerate;

/**
  * @brief uvc stream frame data structure.
  */
typedef struct s_libuvcstrm_frame
{
	u32 size;  ///<uvc stream frame

	t_libuvcstrm_framerate * framerates;
	u8 framerate_cnt;
}t_libuvcstrm_frame;

struct s_libuvc_strmif; ///< for fix warnings

/**
  * @brief uvc stream(still image) framerate data structure.
  */
typedef struct s_libuvcstrm_still
{
	u32 size;
	u8 frameindex;
	u8 compression_index;
	u8 still_ctrl; 
	u8 frame_cnt; ///<uvc stream still frame total cnt
	t_libuvcstrm_frame *frame; ///<uvc still image frame array
	void (*frame_set)(u32, u8); ///<callback to set the stream framesize/frameratec, which called in CommitRequests
}t_libuvcstrm_still;

/**
  * @brief uvc data buffer param.
  */
typedef struct s_libuvc_param
{
	u32 size;  ///<video frame's size
	u32 offset; ///<video frame's current position
	u32 pl_frmticks; ///< video frame's PTS
	u8 pl_toggle; ///<video frame's toggle flag
	u8 pl_sti;  ///<identifies still image

#define UVC_BUFF_STATUS_END 0x1	
	u8 status; ///<video frame's status
	u8 header_size; ///<uvc header's length
} t_libuvc_param;

/**
  * @brief uvc stream interface data structure.
  */
typedef struct s_libuvc_strmif
{
	//TODO: only support one format in one stream interface
#define VIDEO_STREAM_APP_UNKNOW	0x00
#define VIDEO_STREAM_APP_ENC	0x01
#define VIDEO_STREAM_APP_DEC	0x02
	
	u8 STRMIF_ID;  ///<uvc stream id
	u8 STRM_EP; ///<uvc stream endpoint
	u16 STRM_PYLSIZE; ///<uvc stream payload size 
	u8 STRMLINK_ID;	///<uvc stream link terminal id
	u8 STRM_APP; ///<uvc stream application type(encode or decode)

	u8 format; ///<uvc stream format: VS_FORMAT_UNCOMPRESSED or MIMG OR MPEG2TS

#define UVC_STATUS_IDLE  0
#define UVC_STATUS_START 1
#define UVC_STATUS_RUN   2
	u8 v_status; ///<uvc status: UVC_STATUS_XXX

	t_libusbep_cfg *ep_cfg; ///<the endpoints data

	u8 frame_cnt; ///<uvc stream frame total cnt
	u8 still_trigger;
	u16 pad;  ///<padding

	t_libuvcstrm_frame *frame; ///<uvc stream frame array
	t_libuvcstrm_still *still_image;

	int (*encode)(void *, u8 *, int); ///<callback to get the stream's data
	void (*frame_set)(u32, u8); ///<callback to set the stream framesize/frameratec, which called in CommitRequests
	void (*filldone)(void); //callback to deal with the stream data when completing to fill uvc stream.
	void (*filldata)(struct s_libuvc_strmif *); ///<callback to fill uvc stream, to compile with old uvc lib

	//internal use, for current cfg save
	u8 frameindex; ///<uvc stream current frame index. it's zero-based, not the same as Table 4-47
	u8 framerate; ///<uvc stream current frame rate.
	u8 framerate_max; ///<uvc stream max frame rate.
	u8 framerate_min; ///<uvc stream min frame rate.
	u32 max_size;
	u32 min_size;

	t_libuvc_param uvc;  ///<uvc stream's internal parameter
}t_libuvc_strmif;


/**
  * @brief iad data structure.
  */
typedef struct s_libiad_cfg
{
	u8 intr_ep;

#define USB_IAD_APP_UNKNOWN		0x00
#define USB_IAD_APP_VIDEO		0x01
#define USB_IAD_APP_AUDIO		0x02
	u8	iad_app_type;
	
	union
	{
		struct
		{
			void * ctrlif;
			void * strmif;
			u8 strmif_cnt;
		};

		struct
		{
			t_libuvc_ctrlif *vctrlif;
			t_libuvc_strmif *vstrmif;
			u8 vstrmif_cnt;
		};

		struct
		{
			t_libuac_ctrlif *actrlif;
			t_libuac_strmif *astrmif;
			u8 astrmif_cnt;
		};
	};
}t_libiad_cfg;

/**
  * @brief uvc&uac class data structure.
  */
typedef struct s_libuav_cfg
{
	t_libiad_cfg *iad;
	
	u8 iad_cnt;
	void *extra_cfg;//used for vendor msc class
	void *cdc_cfg;//used for cdc_acm class
}t_libuav_cfg;

//INTERNAL_START
#define g_libuav_cfg ((t_libuav_cfg *)(g_libusbclass_cfg->arg))
#define g_libvmsc_cfg ((t_libvmsc_cfg*)(g_libuav_cfg->extra_cfg))
//INTERNAL_END

/**
  * @brief uvc stream payload header, please refer to uvc spec 1.0 for detail
  */
typedef struct _uvc_plheader{ 
    u8  bHeaderLen; ///<uvc payload header length 
#define UVC_MIMG_PLHEADER_SIZE     0xc
#define UVC_YUV_PLHEADER_SIZE       0xc
#define UVC_FRAME_BASED_PLHEADER_SIZE       0xc   //used for frame_based format
#define UVC_ENCV_PLHEADER_SIZE       0x2
#define UVC_BULK_PLHEADER_SIZE       0x2

    u8  bBitmap; ///<bit field for uvc header, bitmap: EOH|ERR|STI|RES|SCR|PTS|EOF|FID
#define UVC_HDR_FID	0x1
#define UVC_HDR_EOF	0x2
#define UVC_HDR_STI	0x20
#define UVC_HDR_ERR	0x40
#define UVC_HDR_EOH	0x80

	u8  dwPTS[4]; ///<presention time stamp
	u8  dwSCR[4]; ///<source clock reference
	u8  wSOF[2]; ///<sof counter
}__attribute__((packed)) UvcPayloadHeader, *pUvcPayloadHeader;

/**
  * @brief uvc stream data operate function, it controls usb to transfer a video frame 
  */
//INTERNAL_START
void uvc_encode_init(t_libuvc_strmif *strmif);
void uvc_setif(t_libuvc_strmif *strmif, int alt);
void uvc_frame_set(t_libuvc_strmif * strmif);
//INTERNAL_END

void uvc_filldata(t_libuvc_strmif * strmif);

//  UVC board ioctl 
#define BIOC_UVC_FILL_DATA               0x030001 /*uvc fill data */
#define BIOC_UVC_SET_VSTREAM             0x030002 /*uvc set frame */
#define BIOC_UVC_START_VSTREAM            0x030003 /*uvc start stream */
#define BIOC_UVC_STOP_VSTREAM           0x030004 /*uvc stop stream */
#define BIOC_UDEV_POST_INIT	         	0x030005 /*udev initialiation after init */
#define BIOC_UDEV_SET_LOOP              0x030006
#define BIOC_LOAD_DCPC_LUT				0x030007 /* Switch/Load DCPC LUT to memory */

/**
  * @brief uvc control property offset data structure.
  * @brief one offset struct shall be match one iad struct
  */
typedef struct s_uvc_cp_offset
{
	u16 pu_start; ///<uvc pu property start position
	u16 *it_start; ///<uvc it property start position array
	u8 it_cnt; ///<number of it array
}t_uvc_cp_offset;

/**
  * @brief uvc control property parameter data structure
  */
typedef struct s_uvc_cp_param
{
	t_uvc_cp_offset *offset; ///<the offset data
	u8 offset_cnt; ///<number of offset array
}t_uvc_cp_param;

/**
  * @brief uvc control property parameter data structure for high speed.
  * @brief data structure will be auto-generate by desc_gen app
  */
extern t_uvc_cp_param g_uvc_cp_param_hs;
/**
  * @brief uvc control property parameter data structure for full speed.
  * @brief data structure will be auto-generate by desc_gen app
  */
extern t_uvc_cp_param g_uvc_cp_param_fs;

#define DATA_FROM_HOST (!(g_libusb_cfg.req.request.bmRequestType & BIT7))
#define DATA_TO_HOST (g_libusb_cfg.req.request.bmRequestType & BIT7)

#define UVC_STREAM_FRAME_DECLARE UVC_STREAM_FRAMECNT_FIX

#define UVC_STREAM_FRAME_DECLARE_START void uvc_framecnt_fix(void){

#define UVC_STREAM_FRAMECNT_FIX(speed, app_name, format_name, frame_name, stillimage_pointer)	\
do {	\
	u8 i, k;	\
	t_libuav_cfg *cfg = (t_libuav_cfg *)(g_libusbclass_cfg_##speed.arg);	\
	if(cfg)	{	\
		for(k=0; k<cfg->iad_cnt; k++) {	\
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {	\
				for(i=0; i<cfg->iad[k].vstrmif_cnt; i++) {	\
					if((cfg->iad[k].vstrmif[i].format == VS_FORMAT_##format_name) && (cfg->iad[k].vstrmif[i].STRM_APP == VIDEO_STREAM_APP_##app_name)) {	\
						cfg->iad[k].vstrmif[i].frame = &frame_name;	\
						cfg->iad[k].vstrmif[i].frame_cnt = sizeof(frame_name)/sizeof(frame_name[0]);	\
						cfg->iad[k].vstrmif[i].still_image = stillimage_pointer;	\
					}	\
				}	\
			}	\
		}	\
	}	\
}while(0)

#define UVC_STREAM_FRAME_DECLARE_END }

#define UVC_CONTROL_XU_FIX(speed, xu_bmControl, xu_guid_str)	\
do {	\
	u8 i, k;	\
	t_libuav_cfg *cfg = (t_libuav_cfg *)(g_libusbclass_cfg_##speed.arg);	\
	if(cfg)	{	\
		for(k=0; k<cfg->iad_cnt; k++) {	\
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {	\
				cfg->iad[k].vctrlif->XU_bmControl = xu_bmControl;	\
				if(xu_guid_str) {	\
					cfg->iad[k].vctrlif->XU_GUID = xu_guid_str;	\
				}	\
			}	\
		}	\
	}	\
}while(0)

extern void uvc_framecnt_fix(void);

#endif
