#ifndef _LIBUSB_MSC__H_
#define _LIBUSB_MSC__H_

#include "libusb_mscuser.h"

#define UMASS_TRUE   1
#define UMASS_FALSE  0

#define UMASS_OK   0
#define UMASS_FAILED  1

/* MSC's class definitions */
#define MSC_CLASS   0x08

#define MSC_SUBCLASS_SCSI               0x06
#define MSC_PROTO_BO			        0x50

/* MSC's class request */
#define MSC_REQ_RESET	        	    0xFF
#define MSC_REQ_GET_MAX_LUN		        0xFE

/* MSC work stage, please refer to CBW specs on USB-IF */
/* CBW -> DATA(IN or OUT) -> CSW */
#define MSC_CBW                      0       /* cmd stage*/
#define MSC_DATA_OUT                 1       /* data stage: data out */
#define MSC_DATA_IN                  2       /* data stage: data in */
#define MSC_DATA_IN_LAST             3       /* data stage: the last in-packet */
#define MSC_DATA_IN_LAST_STALL       4       /* data stage: last in-packet, but err encounter */
#define MSC_CSW                      5       /* status stage */
#define MSC_ERROR                    6       /* cmd error */

/* bulk-only command block wrapper data structure*/
typedef struct s_msc_cbw {
	  u32 signature;
	  u32 tag;
	  u32 data_length;
	  u8  flags;
	  u8  lun;
	  u8  cb_length;
	  u8  cbdata[16];
}__attribute__((packed)) t_msc_cbw;

/* bulk-only command status wrapper */
typedef struct s_msc_csw {
	 u32 signature;
	 u32 tag;
	 u32 data_residue;
	 u8 status;
}__attribute__((packed)) t_msc_csw;

/* CBW and CSW definitions */
#define MSC_CBW_SIZE                     31        
#define MSC_CBW_SIG						0x43425355 //0x55534243  
#define MSC_CSW_SIZE                     13        
#define MSC_CSW_SIG						0x53425355 //0x55534253 

/* CSW status */
#define CSW_CMD_PASSED                  0x00
#define CSW_CMD_FAILED                  0x01
#define CSW_STAGE_ERR	                0x02

/* SCSI commands */
#define CMD_TEST_UNIT_READY            0x00
#define CMD_REQUEST_SENSE              0x03
#define CMD_FORMAT_UNIT                0x04
#define CMD_INQUIRY                    0x12
#define CMD_MODE_SELECT6               0x15
#define CMD_MODE_SENSE6                0x1A
#define CMD_START_STOP_UNIT            0x1B
#define CMD_MEDIA_REMOVAL              0x1E
#define CMD_READ_FORMAT_CAPACITIES     0x23
#define CMD_READ_CAPACITY              0x25
#define CMD_READ10                     0x28
#define CMD_WRITE10                    0x2A
#define CMD_VERIFY10                   0x2F
#define CMD_READ_TOC                   0x43
#define CMD_MODE_SELECT10              0x55
#define CMD_MODE_SENSE10               0x5A

typedef struct s_libmsc_cfg
{
	u8 *rx_buffer;
	u32 rx_offset;
	u32 rx_length;

	u8 *tx_buffer;
	u32 tx_offset;
	u32 tx_length;

	u32 block_rw_offset;
	u32 block_rw_length;
	u32 block_rw_start;

	u32 request_sense_code;
	u8 msc_lun_cnt;

	u8 scsi_cmd;

	u8 msc_stage;   //MSC_XXX

	u8 fixbug_flag;
	u32 fixbug_count; // bytes

	u8 MSC_EP_IN;  // bulk in endpoint
	u8 MSC_EP_OUT; // bulk out endpoint

	int (*msc_init)(void);

	/* MSC Bulk Callback Functions */
	void (*msc_bulk_in)(void);
	void (*msc_bulk_out)(void);

	t_lundriver_cfg *msc_lundriver[MSC_MAX_LUNCNT];
}t_libmsc_cfg;

extern t_libmsc_cfg g_libmsc_cfg;

#endif  /* __LIBUSB_MSC_H__ */
