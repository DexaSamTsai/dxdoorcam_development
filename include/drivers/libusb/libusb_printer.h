#ifndef _LIBUSB_PRINTER__H_
#define _LIBUSB_PRINTER__H_


#define PRINTER_CLASS					0x07

#define PRINTER_SUBCLASS				0x01

#define PRINTER_PROTOCOL_UNIDIR			0x01
#define PRINTER_PROTOCOL_BIDIR			0x02
#define PRINTER_PROTOCOL_1284			0x03
#define PRINTER_PROTOCOL_VENDOR			0xFF

//printer class request code
#define PRINTER_REQ_GET_DEVICE_ID		0x00
#define PRINTER_REQ_GET_PORT_STATUS		0x01
#define PRINTER_REQ_SOFT_RESET			0x02


#define SIZE_2K				0x800

typedef struct s_libuprinter_cfg
{
	u8 ep_in;//bulk-in endpoint number
	u8 ep_out;//bulk-out endpoint number

	u8 *rx_buffer;
	u32 rx_offset;
	u32 rx_len;

	u8 *tx_buffer;
	u32 tx_len;

	int (*read)(u8 * * buf, u32 maxlen);
	void (*get_frame)(u32 * addr, u32 * len, u32 * addr1, u32 *len1);

	void (*printer_init)(struct s_libuprinter_cfg *cfg);

	/* printer class callback function */
	void (*printer_handle)(void);
}t_libuprinter_cfg;


#define g_libuprinter_cfg ((t_libuprinter_cfg *)(g_libusbclass_cfg->arg))


#endif  /* __LIBUSB_PRINT_H__ */
