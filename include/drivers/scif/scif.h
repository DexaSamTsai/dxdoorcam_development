#ifndef __SCIF_H__
#define __SCIF_H__

/***************************************************************************
*  Marco define 
****************************************************************************/

/* SCIF protocal commands */
#define SCIF_CMD0			0
#define SCIF_CMD1			1
#define SCIF_CMD2			2
#define SCIF_CMD3			3
#define SCIF_CMD4			4
#define SCIF_CMD5			5
#define SCIF_CMD6			6
#define SCIF_CMD7			7
#define SCIF_CMD8			8
#define SCIF_CMD9			9
#define SCIF_CMD10		10
#define SCIF_CMD11		11
#define SCIF_CMD12		12
#define SCIF_CMD13		13
#define SCIF_CMD15		15
#define SCIF_CMD16		16
#define SCIF_CMD17		17
#define SCIF_CMD18		18
#define SCIF_CMD24		24
#define SCIF_CMD25		25
#define SCIF_CMD27		27
#define SCIF_CMD28		28
#define SCIF_CMD29		29
#define SCIF_CMD30		30
#define SCIF_CMD32		32
#define SCIF_CMD33		33
#define SCIF_CMD38		38
#define SCIF_CMD42		42
#define SCIF_CMD52		52
#define SCIF_CMD53		53
#define SCIF_CMD55		55
#define SCIF_CMD56		56
#define SCIF_CMD58		58
#define SCIF_CMD59		59
#define SCIF_ACMD6		6
#define SCIF_ACMD13		13
#define SCIF_ACMD22		22
#define SCIF_ACMD23		23
#define SCIF_ACMD41		41
#define SCIF_ACMD42		42
#define SCIF_ACMD51		51

/* scif status return */
#define SCIF_ERROR			-1
#define SCIF_ERR_NONE			0
#define SCIF_ERR_TIMEOUT		1
#define SCIF_ERR_BADCRC		2
#define SCIF_ERR_FAILED		3
#define SCIF_ERR_INVALID		4
#define SCIF_TIMEOUT_DATA_COMP		5
#define SCIF_TIMEOUT_AHB_DMA_COMP		6
#define SCIF_TIMEOUT_CMD12			7
#define SCIF_TIMEOUT_DAT0	 		8
#define SCIF_DATA_CRC	                9
#define SCIF_RX_UNDERFLOW		10

#define SCIF_SPEC_1_0		 0
#define SCIF_SPEC_1_1		 1
#define SCIF_SPEC_2			2


/* Card Power up status bit */
#define SCIF_CARD_BUSY		0x80000000  
#define SCIF_HIGH_SPEED_FUNC		 1

/* ocr define */
#define OV_OCR_MASK			(SCIF_VDD_165_195 |SCIF_VDD_30_31|SCIF_VDD_31_32|SCIF_VDD_32_33)
#define SCIF_VDD_165_195	 0x00000080  /* VDD voltage 1.65 - 1.95 */
#define SCIF_VDD_30_31		0x00040000  /* VDD voltage 3.0 ~ 3.1 */
#define SCIF_VDD_31_32		0x00080000  /* VDD voltage 3.1 ~ 3.2 */
#define SCIF_VDD_32_33		0x00100000  /* VDD voltage 3.2 ~ 3.3 */

/* card response format */
#define RESP_LENGTH_MAX	 8
#define SCIF_RES_NONE			(HCD_RES_NONE)
#define SCIF_RES_R1			(HCD_RES_48 | HCD_CMD_CRC | HCD_CMD_IDX_CRC)				/* crc+cmd_index */
#define SCIF_RES_R1b			(HCD_RES_48_CHECK | HCD_CMD_CRC | HCD_CMD_IDX_CRC)		/* crc+cmd_index+busy check */
#define SCIF_RES_R2		  (HCD_RES_136 | HCD_CMD_CRC)// | HCD_CMD_IDX_CRC)			/* cid,csd */
#define SCIF_RES_R3			(HCD_RES_48)			 /* ocr */
#define SCIF_RES_R6		  (HCD_RES_48  | HCD_CMD_CRC | HCD_CMD_IDX_CRC)			/* rca: crc+cmd_index */
#define SCIF_RES_R7		  (HCD_RES_48)		  /* cmd8 */

/* only for scio */
#define SCIO_RES_R4		  (HCD_RES_48 | HCD_CMD_CRC | HCD_CMD_IDX_CRC)		  /* cmd52 */
#define SCIO_RES_R5		  (HCD_RES_48 | HCD_CMD_CRC | HCD_CMD_IDX_CRC)		  /* cmd52 */
 
/* data translate mode */
#define SCIF_OP_BKR			(HCD_TRANS_BLOCK | HCD_BLOCK_COUNT | HCD_TRANS_DIR_READ | HCD_DAT_PRESENT)
#define SCIF_OP_BKW			(HCD_TRANS_BLOCK | HCD_BLOCK_COUNT | HCD_TRANS_DIR_WRITE | HCD_DAT_PRESENT)

#define SCIF_OP_BKR_SGL		(SCIF_OP_BKR | HCD_TRANS_BLOCK_SGL)
#define SCIF_OP_BKR_MULTI		(SCIF_OP_BKR | HCD_TRANS_BLOCK_MULTI | HCD_AUTOCMD12)
#define SCIF_OP_BKW_SGL		(SCIF_OP_BKW | HCD_TRANS_BLOCK_SGL)
#define SCIF_OP_BKW_MULTI		(SCIF_OP_BKW | HCD_TRANS_BLOCK_MULTI | HCD_AUTOCMD12)

#define SCIO_OP_BKR_MULTI		(SCIF_OP_BKR | HCD_TRANS_BLOCK_MULTI | HCD_AUTOCMD12_NONE)
#define SCIO_OP_BKW_MULTI		(SCIF_OP_BKW | HCD_TRANS_BLOCK_MULTI | HCD_AUTOCMD12_NONE)

#define SCIF_OP_SER			(HCD_TRANS_SEQ | HCD_TRANS_DIR_READ)	 /* TBD */
#define SCIF_OP_SEW			(HCD_TRANS_SEQ | HCD_TRANS_DIR_WRITE)	/* TBD */ 

#define SCIF_OP_DMA_NONE		(HCD_TRANS_DMA_NONE)
#define SCIF_OP_DMA			(HCD_TRANS_DMA)

#define	SCIF_NONE_DMA		0
#define SCIF_AHB_DMA	  1
#define SCIF_EXT_DMA	  2
#define SCIF_ADMA         3

#define AHB_DMA_AUTO	0
#define AHB_DMA_MANUAL  1

/* only for scio */
#define SCIO_MEM_PRESENT		(0x1<<27)
#define SCIO_IO_FUNC_NUM		(0x7<<28)
/* SCIO COMMON Registers */
/* revision register */
#define CCCR_SCIO_REVISION_REG  0x00
#define CCCR_REV_MASK			0x0F
#define CCCR_REV_1_0			0x00
#define CCCR_REV_1_1			0x01
#define SCIO_REV_MASK			0xF0
#define SCIO_REV_1_00			0x00
#define SCIO_REV_1_10			0x10
#define SCIO_REV_1_20			0x20
/* SCIF physical spec revision */
#define SCIF_SPEC_REVISION_REG	0x01
#define SCIF_REV_MASK			 0x0F
#define SCIF_REV_1_01			 0x00
#define SCIF_REV_1_10			 0x01
/* I/O Enable  */
#define SCIO_ENABLE_REG		 0x02
/* I/O Ready */
#define SCIO_READY_REG		  0x03
/* Interrupt Enable */
#define SCIO_INT_ENABLE_REG	 0x04
#define SCIO_INT_MASTER_ENABLE  0x01
#define SCIO_INT_ALL_ENABLE	 0xFE
/* Interrupt Pending */
#define SCIO_INT_PENDING_REG	0x05
#define SCIO_INT_PEND_MASK	  0xFE
/* I/O Abort */
#define SCIO_IO_ABORT_REG		0x06
#define SCIO_IO_RESET			(1 << 3)
/* Bus Interface */
#define SCIO_BUS_IF_REG		 0x07
#define CARD_DETECT_DISABLE	 0x80
#define SCIO_BUS_WIDTH_1_BIT	0x00
#define SCIO_BUS_WIDTH_4_BIT	0x02
/* Card Capabilities */
#define SCIO_CARD_CAPS_REG		  0x08
#define SCIO_CAPS_CMD52_WHILE_DATA  0x01	/* card can issue CMD52 while data transfer */
#define SCIO_CAPS_MULTI_BLOCK		0x02	/* card supports multi-block data transfers */
#define SCIO_CAPS_READ_WAIT		 0x04	/* card supports read-wait protocol */
#define SCIO_CAPS_SUSPEND_RESUME	0x08	/* card supports I/O function suspend/resume */
#define SCIO_CAPS_INT_MULTI_BLK	 0x10	/* interrupts between multi-block data capable */
#define SCIO_CAPS_ENB_INT_MULTI_BLK 0x20	/* enable ints between muli-block data */
#define SCIO_CAPS_LOW_SPEED		 0x40	/* low speed card */
#define SCIO_CAPS_4BIT_LS			0x80	/* 4 bit low speed card */
/* Common CIS pointer */
#define SCIO_CMN_CIS_PTR_LOW_REG	0x09
#define SCIO_CMN_CIS_PTR_MID_REG	0x0a
#define SCIO_CMN_CIS_PTR_HI_REG	 0x0b

#define SCIO_CIS_AREA_BEGIN	0x00001000
#define SCIO_CIS_AREA_END	 0x00017fff
/* Tuple definitions */
#define CISTPL_NULL		 0x00
#define CISTPL_CHECKSUM	 0x10
#define CISTPL_VERS_1		0x15
#define CISTPL_ALTSTR		0x16
#define CISTPL_MANFID		0x20
#define CISTPL_FUNCID		0x21
#define CISTPL_FUNCE		0x22
#define CISTPL_VENDOR		0x91
#define CISTPL_END		  0xff
#define CISTPL_LINK_END	 0xff

#define SCIO_POWER_CONTROL_REG			0x12
#define xFUNCTION_FBR_OFFSET(funcNo)	 (0x100*(funcNo))

/* scio function */
#define SCIO_FUNC_NONE          0x00
#define SCIO_FUNC_UART          0x01
#define SCIO_FUNC_BLUETOOTHA    0x02
#define SCIO_FUNC_BLUETOOTHB    0x03
#define SCIO_FUNC_GPS           0x04
#define SCIO_FUNC_CAMERA        0x05
#define SCIO_FUNC_PHS           0x06
#define SCIO_FUNC_WLAN          0x07
#define SCIO_FUNC_EXTEND        0x0f
#define SCIO_FUNC_NUM_MAX       0x07

//#define SCIO_CIS_AREA_BEGIN		0x1000
//#define SCIO_CIS_AREA_END		0x1ffff

#define TUPLE_NUM_MAX			0xff

/***************************************************************************
*				 type defination 
*************************************************************************/
/* scio function information */
typedef struct s_scio_func_info
{
    u8 func[8];
    int func_id_sel;
    int io_port_sel;
	t_ertos_queuehandler *evt;//    t_signal *evt;
    unsigned short evt_wait_time;
}t_scio_func_info;

typedef enum
{
	SCIF_OP_READ,
	SCIF_OP_WRITE,
	SCIF_CLK,	
	SCIF_BUS_WIDTH
}enum_host_op_mode;
typedef unsigned char host_op_mode;

enum{
    MSG_SCIF_OK = 0x02,
    MSG_SCIF_CRC_ERR,
    MSG_SCIF_TIMEOUT,
    MSG_SCIF_INSERT,
    MSG_SCIF_REMOVED,
    MSG_SCIO_READY,
    MSG_ADMA_ERR,
};

typedef enum{
	SCIF_SPEED_MODE_SDR12=0,
	SCIF_SPEED_MODE_SDR25,
	SCIF_SPEED_MODE_SDR50,
	SCIF_SPEED_MODE_SDR104,
	SCIF_SPEED_MODE_DDR50, 
}enum_scif_speed_mode;
typedef unsigned char scif_speed_mode;

typedef enum{
	ADMA_NO_OP,
	ADMA_RESERVED,
	ADMA_TRAN,
	ADMA_LINK,
}enum_ADMA_ATTR;
typedef unsigned char ADMA_ATTR;

typedef struct{
	 unsigned char		mmca_vsn;
	 unsigned short	  cmdclass;
	 unsigned short	  tacc_clks;
	 unsigned int		tacc_ns;
	 unsigned int		r2w_factor;
	 unsigned int		max_dtr;
	 unsigned int		read_blkbits;
	 unsigned int		write_blkbits;
	 unsigned int		capacity;
}t_scif_csd_str;

typedef struct{
	unsigned char		sda_vsn;
	unsigned char		bus_widths;
#define SCIF_BUS_WIDTH_1	 (1<<0)
#define SCIF_BUS_WIDTH_4	 (1<<2)

}t_scif_scr_str;

typedef struct{
	unsigned char attr;
	unsigned char reserved;
	unsigned char lenth_l;
	unsigned char lenth_h;
#define ADMA_VALID BIT0
#define ADMA_END   BIT1
#define ADMA_INT   BIT2
	unsigned int addr;
}t_scif_ADMA_desc;
typedef struct{
	unsigned int cnt;
	t_scif_ADMA_desc *adma_desc;
}t_scif_ADMA_tbl;
typedef struct t_scif_info{
	unsigned int 		version;
	unsigned int 	  	rca;
	unsigned int 		state;
#define  SCIF_STATE_PRESENT	(1<<0)	  /* present in sysfs */
#define  SCIF_STATE_READONLY  (1<<1)	  /* card is read-only */
#define  SCIF_STATE_HIGHSPEED (1<<2)	  /* card is in high speed mode */
#define  SCIF_STATE_BLOCKADDR (1<<3)	  /* card uses block-addressing */
	unsigned int	  	ocr;
	unsigned int	  	size;	  
	unsigned int 		blksz;
	t_scif_csd_str			csd;
	t_scif_scr_str			scr;
	unsigned int 		hs_max_dtr;
	unsigned char		mode;
#define CARD_MODE_SCIF	 (1<<0)
#define CARD_MODE_MMC	(1<<1)
#define CARD_MODE_SCIO	(1<<2)
	unsigned char		type;
	unsigned int 		scio_mem_flag;
	unsigned int 		scio_io_func_num;
	unsigned int 		scio_blk_size;
//    unsigned char       scio_io_func[7];         
}t_scif_info;

#define SD_MODULE_DBG

#ifdef SD_MODULE_DBG
#define SCIF_DEBUG(x...) if(p_scif_drv->dbg)p_scif_drv->dbg(x)
#else
#define SCIF_DEBUG(x...)
#endif

typedef struct t_scif_drv
{
	unsigned int (*init)(struct t_scif_drv *p_scif_drv);
	void (*reset)(struct t_scif_drv *p_scif_drv, int mode);
	int (*send_cmd)(struct t_scif_drv *p_scif_drv, int cmd, 
                        unsigned int arg, 
						unsigned int flags, 
						unsigned int st_bits, unsigned int *resp);
	void (*config)(struct t_scif_drv *p_scif_drv, host_op_mode mode, 
					unsigned int dma_data_addr, unsigned int blk_siz, 
					unsigned int blk_num, unsigned char dam_flag, 
					unsigned int speed, unsigned char bus_width);
	int  (*read_start)(struct t_scif_drv *p_scif_drv, 
						unsigned int src_adr,
						unsigned int dst_adr,
						unsigned int blk_siz,unsigned int blk_num,
						unsigned char dam_flag);
	int  (*write_start)(struct t_scif_drv *p_scif_drv, 
                        unsigned int src_adr, 
						unsigned int dst_adr,
						unsigned int blk_siz,unsigned int blk_num,
						unsigned char dam_flag);
	int (*data_completed_wait)(struct t_scif_drv *p_scif_drv, host_op_mode op_mode, unsigned int blk_num, unsigned char dma_flag);
#ifdef SD_MODULE_DBG
    int (*dbg)(const char *fmt0, ...);
#endif

	struct t_scif_info	*scif_info;	
	int timeout; 
	int cmd_resp_dly; 
	int cmd_chk_num; 
	int endian;
	int div;
	int div_hs;
	unsigned flag_18V;				//1.8V power supply if flag_18V ==1 
	unsigned int clk;				//present sd clk
	unsigned int ctrl_base_addr;
	unsigned char dma_flag;
	unsigned char ahb_dma_mode;
	unsigned char phase;
	unsigned char phase_hs;
	unsigned char bus4_used;
	unsigned char hs_enable;
	unsigned char mode;
	unsigned char		dll_delay;	//delay used for dll mode,if 0,not dll mode,otherwise,dll mode with set delay
	int wr_speed;
    char intr_en;
    t_ertos_queuehandler *event;//    t_signal *event;
    unsigned short evt_wait_time;

    /* scio function info */
    t_scio_func_info io_func_info;
	t_scif_ADMA_tbl * ADMA_tbl;
}t_scif_drv;


typedef struct t_scio_tpl
{
	unsigned char tpl_code;
	unsigned char tpl_link;
}t_scio_tpl;

int card_type_get(struct t_scif_drv *p_scif_drv);
int card_all_cid_get(t_scif_drv *p_scif_drv, int rca);
int card_idle_set(t_scif_drv *p_scif_drv);
int card_scr_get(t_scif_drv *p_scif_drv, t_scif_info *p_scif_info, int rca);
int card_csd_get(t_scif_drv *p_scif_drv, t_scif_info *p_scif_info, int rca);
int card_speed_switch(t_scif_drv *p_scif_drv, t_scif_info *p_scif_info,scif_speed_mode speed_mode);
int card_op_cond(t_scif_drv *p_scif_drv, unsigned char mode, int ocr, unsigned int *fin_ocr);
int card_rca_get(t_scif_drv *p_scif_drv, unsigned char mode, int rca, unsigned int *fin_rca);
int card_selected(t_scif_drv *p_scif_drv, int rca);
int card_bus_width_switch(t_scif_drv *p_scif_drv, t_scif_info *p_scif_info, unsigned char mode, int rca);
unsigned int scif_trans_mode_set(t_scif_drv *p_scif_drv, unsigned int mode, unsigned char dma_flag);

//switch sd module clock,used after scif_init()
int card_clk_switch(int clk_in,t_scif_drv *p_scif_drv, t_scif_info *p_scif_info,unsigned int scif_clock);
void scif_module_config(t_scif_drv *p_scif_drv, int mode);
unsigned int scif_init(unsigned char interface,int clk_in,t_scif_drv *p_scif_drv,int clk);
int scif_read(t_scif_drv *p_scif_drv, unsigned int src_adr,unsigned int dst_adr,unsigned int blk_siz,unsigned int blk_num);
int scif_write(t_scif_drv *p_scif_drv, unsigned int src_adr, unsigned int dst_adr, unsigned int length);
int scio_init(unsigned char interface,int clk_in,struct t_scif_drv *p_scif_drv, unsigned char intr_en, int *flag,int clk);
int scio_reg_read(t_scif_drv *p_scif_drv, unsigned int func_num,unsigned int reg, unsigned int *data);
int scio_reg_write(t_scif_drv *p_scif_drv, unsigned int func_num,unsigned int reg, unsigned int data);
int scio_read(t_scif_drv *p_scif_drv, int opcode, unsigned int reg, int func, int data_addr, int size,int blksize);
int scio_write(t_scif_drv *p_scif_drv, int opcode, unsigned int reg, int func, int data_addr,int size,int blksize);
int scio_blk_size_change(t_scif_drv *p_scif_drv, int func_no, unsigned int blk_size);

#define SCIF1_WP_DISABLE do{ WriteReg32(SCIF1_BASE_ADDR+REG_SCIF_FIFO_WM, ReadReg32(SCIF1_BASE_ADDR+REG_SCIF_FIFO_WM)|BIT7); }while(0)
#define SCIF2_WP_DISABLE do{ WriteReg32(SCIF2_BASE_ADDR+REG_SCIF_FIFO_WM, ReadReg32(SCIF2_BASE_ADDR+REG_SCIF_FIFO_WM)|BIT7); }while(0)

/****************************************************
  There are two sd/sdio host interfaces:scif and scio.There is no difference between them.Sd cards or sdio devices can connect to three sets of pins.
1.SCIF_MODULE_01: SCIF_XX, GPIO68-GPIO73
2.SCIF_MODULE_02: SCIO_XX, GPIO58-GPIO63
3.SCIF_MODULE_03: shared with BT1120, GPIO80-GPIO83, GPIO2_01-GPIO2_02
SCIF_MODULE_02 and SCIF_MODULE_03 connect to the scio interface internal, which means you can not use SCIF_MODULE_02 and SCIF_MODULE_03 at the same time.
*****************************************************/
#define SCIF_MODULE_01  1
#define SCIF_MODULE_02  2
#define SCIF_MODULE_03  3

#define PIN_SCIF1_CONFIG		\
do{								\
	PIN_SCIF1_NORMAL;			\
	PIN_SCIF1_ENABLE;			\
	SC_RESET0_RELEASE(SCIF1);	\
	SC_RRESET1_RELEASE(SCIF1);	\
	WriteReg32(REG_SC_DIV2,ReadReg32(REG_SC_DIV2) | BIT5 );	\
	SCIF1_WP_DISABLE;			\
}while(0)
#define PIN_SCIF2_CONFIG		\
do{								\
	PIN_SCIF2_NORMAL;			\
	PIN_SCIF2_ENABLE;			\
	SC_RESET0_RELEASE(SCIF2);	\
	SC_RRESET1_RELEASE(SCIF2);	\
	WriteReg32(REG_SC_DIV2,ReadReg32(REG_SC_DIV2) | BIT13 );	\
	SCIF2_WP_DISABLE;			\
}while(0)

#define PIN_SCIF3_CONFIG		\
do{								\
	PIN_SCIF3_NORMAL;			\
	PIN_SCIF3_ENABLE;			\
	SC_RESET0_RELEASE(SCIF2);	\
	SC_RRESET1_RELEASE(SCIF2);	\
	WriteReg32(REG_SC_DIV2,ReadReg32(REG_SC_DIV2) | BIT13 );	\
	SCIF2_WP_DISABLE;			\
}while(0)

/**
 * @brief initialize scif module
 * REAL_FORMAT int app_scif_init(void);
 * @details this func init the scif from app level. select module(SCIF01 or SCIF02) by config menuconfig, enable CONFIG_APP_USE_SCIF02 indicates select the second SCIF interface, otherwise, select the first SCIF interface.
 *
 *
 * @return scif card capability, < 10 err.
 */
//INTERNAL_START
u32 libscif_init(u8 module);
#ifdef CONFIG_APP_USE_SCIF02
#define app_scif_init() libscif_init(SCIF_MODULE_02)
#else
#define app_scif_init() libscif_init(SCIF_MODULE_01)
#endif
//INTERNAL_END
extern t_scif_drv sh_scif_drv;
#endif


