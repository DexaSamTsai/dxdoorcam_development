#ifndef _ROM_H_
#define _ROM_H_

#define BOOT_SYS_CONFIG_IC		BIT0
#define BOOT_SYS_CONFIG_DC		BIT1
#define BOOT_SYS_CONFIG_QSCRC	BIT2
#define BOOT_SYS_CONFIG_STCRC	BIT3
#define BOOT_SYS_CONFIG_BLCRC	BIT4
#define BOOT_SYS_CONFIG_DDR		BIT8
#define BOOT_SYS_CONFIG_400M	BIT9
#define BOOT_SYS_CONFIG_200M	BIT10
#define BOOT_SYS_CONFIG_NOTDISABLEC	BIT13
#define BOOT_SYS_CONFIG_ROMIC	BIT14
#define BOOT_SYS_CONFIG_ROMDC	BIT15
#define BOOT_SYS_CONFIG_LPDDR2_400M    BIT16
#define BOOT_SYS_CONFIG_LPDDR2_300M    BIT17
#define BOOT_SYS_CONFIG_MIPI1LANE  BIT18
#define BOOT_SYS_CONFIG_MIPI2LANE  BIT19
#define BOOT_SYS_CONFIG_MIPI4LANE  BIT20

extern volatile unsigned int ticks;
#define TICKS_OVERFLOW_SECONDS (42949673)
extern volatile unsigned int ticks_overflow_cnt; //1 overflow is 4G ticks, so it's (4G / 100) second.

int load_from_xmodem(u32 baseaddr);

#endif
