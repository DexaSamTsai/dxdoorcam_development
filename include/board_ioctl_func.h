#ifndef _BOARD_IOCTL_FUNC_H_
#define _BOARD_IOCTL_FUNC_H_

/**
  board_ioctl() is the API for libraries call app/board specific codes,
  or app call board specific code.

  App can define the cmd from 0x0000 to 0xffff.
 */

#define BIOC_HW_EARLY_INIT		0x010001	///< called at the earlist in main(), I/D cache not inited, rtos not running.
#define BIOC_HW_INIT			0x010002	///< called after rtos started, before running thread_main().

#define BIOC_AU_PA_ON			0x010003   ///< called in share/driver/audio/libacodec.c, to enable/disable PA

s32 board_ioctl(u32 cmd, u32 arg, void * ret_arg);

#endif
