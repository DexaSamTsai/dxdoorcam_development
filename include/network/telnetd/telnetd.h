#ifndef _TELNETD_H_
#define _TELNETD_H_

typedef struct s_telnetd_cfg
{
	//config
	int listen_port; //0 means 23

	//internal
	int socket;
	pthread_t pt;
}t_telnetd_cfg;

int telnetd_start(t_telnetd_cfg * cfg);
int cmd_telnetd(int argc, char ** argv);
#endif

