#ifndef _NETWORK_H_
#define _NETWORK_H_

#define NETWORK_IPMODE_AUTO		0 //if CONFIG_NETWORK_DHCPC_EN, use dhcp client. else if CONFIG_NETWORK_DHCPS_EN, use dhcp server. else error.
#define NETWORK_IPMODE_DHCPC	1
#define NETWORK_IPMODE_DHCPS	2
#define NETWORK_IPMODE_MANUAL	3

typedef struct s_network_wifi_wps_cfg
{
	// input:
	u32 scan_timeout;  // wps scan timeout in seconds, 0 for scanning forever.
	u8 pin[16];			// pin mode if this is set, pbc mode otherwise.

	// output:
	u8 ssid[32];
	u8 key[64];

	u8 bssid[6];
	int security;  //0:none, 1:wep, 2: wpa/wpa2-psk


	int is_done;	// result for checking,  0: ongoing, 1: done,  -1: fail;
}t_network_wifi_wps_cfg;

typedef struct s_network_ipmode_manual_cfg
{
 	// IP address in ascii represenation (e.g. "127.0.0.1")
	char * ipaddr;
	char * netmask;
	char * gateway;
	char * dnsserver;
}t_network_ipmode_manual_cfg;



typedef struct s_network_wifi_cfg
{
	u8 mac[6];
	int fix_bssid;  // used by libwifi_scanconnect, if = 1, wifi will always connect this bssid.
#define WIFI_MODE_STATION	0
#define WIFI_MODE_AP		1
#define WIFI_MODE_WPS		2
	u8 wifi_mode;
	u8 ipmode; //NETWORK_IPMODE_XXX

	t_network_wifi_wps_cfg wps;
	t_network_ipmode_manual_cfg dhcpc_ip_cfg;  // ip config for NETWORK_IPMODE_DHCPC
	t_network_ipmode_manual_cfg manual_ip_cfg;  // ip config for NETWORK_IPMODE_MANUAL
	t_network_ipmode_manual_cfg dhcps_ip_cfg;   // ip config for NETWORK_IPMODE_DHCPS
	char * ssid;
	int ssidlen;
	u8  bssid[6];
	char * key;
	u32 keylen;
	int keyindex; // used by wep mode.

	//FIXME: need more clean
	u32 *channel;		// NULL: auto, *0:auto, *1-*13:  num
	u32 *security;		// NULL: auto, *0:none, *1:wep, *2: wpa/wpa2-psk
	u32 *wep_authtype;	// NULL: auto, *0:open system, *1: shared key
	char * country_code; //NULL: "CN";  other: "US","EU","AU","XZ","CN".
	u16 *chanspec;	//if chanspec ==NULL, it will call normal connect api,otherwise will call quick connect api.
	wifi_scan_security_t scan_security;
	wifi_scan_encryption_t scan_encryption;

	u8 pmk[32]; //32byte, used by quick connect.

#define NETIF_LINK_STATUS_OFF		0		// Called when disconnected as STATION or stopped as AP
#define NETIF_LINK_STATUS_ON 		1		// Called when connected as STATION or started as AP
#define NETIF_LINK_STATUS_DOWN		2		// Called when interface is bringed down (Any traffic processing is disabled)
#define NETIF_LINK_STATUS_UP		3		// Called when interface is bringed up (i.e., due to DHCP IP acquistion. Available for processing traffic)
	int (* netif_link_callback)(int evt);	///< Called whenever the netif link changes, evt is NETIF_LINK_STATUS_XXX

	//internal
	struct netif * netif;

/*
 * arguments for ap.
 */

//#define WIFI_WEP64	1
//#define WIFI_WEP128  	2
//#define WIFI_WPA		3
//#define WIFI_WPA2		4
//#define WIFI_OPEN		0
	int ap_encrypt;
//#define WIFI_SEC_NONE	0
//#define WIFI_SEC_AES	1
//#define WIFI_SEC_TKIP     2
	int ap_security;
	char *ap_ssid;
	char *ap_key;
	int ap_keylen;
	int ap_channel;


}t_network_wifi_cfg;

typedef struct s_network_eth_cfg
{
	u8 mac[6];
	u8 ipmode; //NETWORK_IPMODE_XXX

	t_network_ipmode_manual_cfg manual_ip_cfg;	// ip config for NETWORK_IPMODE_MANUAL

	//internal
	struct netif * netif;
}t_network_eth_cfg;

//support one WiFi interface and one Ethernet interface at most.
typedef struct s_network_cfg
{
	t_network_wifi_cfg wifi;
	t_network_eth_cfg eth;

	char * ntp_server;	 ///< NTP server address, such as "128.138.140.44". Default NULL with NTP disabled.
}t_network_cfg;

extern t_network_cfg g_network_cfg;

#endif
