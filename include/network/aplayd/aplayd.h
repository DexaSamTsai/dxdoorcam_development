#ifndef _APLAYD_H_
#define _APLAYD_H_

typedef struct s_aplayd_cfg
{
	//config
	int listen_port;

	int (*adec_start)(int fmt);
	int (*adec_stop)(void);
	int (*adec_exit)(void);
	int adec_stop_ms;
	int half_duplex;

	//internal
	int socket;
	int adec_running;
	pthread_t pt;
	unsigned char recvbuf[1536];
}t_aplayd_cfg;

int aplayd_start(t_aplayd_cfg * cfg);

#endif

