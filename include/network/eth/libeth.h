#ifndef _LIBETH_H_
#define _LIBETH_H_

typedef enum{
	ETH_SIF0_P0, //shared with SCIF
	ETH_SIF0_P1, //shared with SCIO
	ETH_SIF0_P2, //shared with JTAG
	ETH_SIF2_P0, //shared with BT1120
	ETH_SIF2_P1, //shared with SNR0
}ETH_SOURCE;

int libeth_init(int eth_if, u8 * mac, void * arg);
int libeth_packet_write(u8 * buf, int len);
int libeth_get_mac(u8 * macaddr);
void libeth_thread_input_notify(void);
void libeth_stop(void);


//called by internal network.c only
void eth_thread_exit(void);

#endif
