#ifndef _ETH_H_
#define _ETH_H_

/* these functions call driver directly, it's used by internal only.
   Applications, please call APIs in libeth.h
   */

#include "libeth.h"
#include "libnsif.h"

typedef struct s_eth_init_arg
{
	int eth_if;
	u8 * mac;
	void * arg;
}t_eth_init_arg;
int eth_init(t_eth_init_arg * arg);

int eth_get_mac(u8 * mac_addr);

int eth_set_mac(u8 * mac_addr);

typedef struct s_eth_write_arg
{
	u8 * buf;
	int len;
}t_eth_write_arg;
int eth_packet_write(t_eth_write_arg * arg);

typedef struct s_eth_read_arg
{
	u8 * buf;
	int maxlen;
}t_eth_read_arg;
int eth_packet_read(t_eth_read_arg * arg);


int eth_stop(void* arg);


#endif
