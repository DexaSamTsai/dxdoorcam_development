#ifndef _LIBWVTP_H_
#define _LIBWVTP_H_

#define BROADCAST_PORT 2000

#define NR_BUFS_MIN	 8
#ifdef CHIP_R1
#define NR_BUFS_MAX		8 //((MAX_SEQ +1)/2)
#endif
#ifdef CHIP_R2
#define NR_BUFS_MAX		16 //((MAX_SEQ +1)/2)
#endif

#define WVTP_NUM	2


/*
 * the wvtp_context_t is got from wvtp.h, they are the same
 */
#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif

#define WVTP_MAGIC_NUM  'v'





#define ALDG_DATA   0
#define ALDG_ACK    1
#define ALDG_NAK    2
#define ALDG_DATA_BYTIMEOUT   3
#define ALDG_DATA_BYNAK   4

#define WVTP_HEAD_LEN   4

#define MAX_SEQ     255
#define ACK_TIME_INDEX (0)

extern int NR_BUFS;

#define NR_BUFR     64 //((MAX_SEQ +1)/2)

#define MAX_RESEND_TIME     10;

#define CONFIG_WVTP_BUFFER_DATA
#ifndef CONFIG_WVTP_BUFFER_DATA
#define MAX_SEND_DATA_LEN   (1024)
#define DATA_HEAD_LEN   100
#else
#define MAX_SEND_DATA_LEN   (1024)
#define DATA_HEAD_LEN       1024
#endif



typedef struct _WVTP_HEAD{
    u8 type;
    u8 sequence;
    u8 acknum;
    u8 flowid;
}__attribute__((packed)) WVTP_HEAD , *PWVTP_HEAD;

typedef struct _sendflag{
    u32 time;   // record current send time
    u32 num;    //record resend times
    u32 nak;	//record nak resend times
    u32 seq;
    u32 flag;
    u32 timeout;
}SENDFLAG;

typedef struct _WVTP_SBUF{
    u8   *data_ptr ;
    u32  data_len ;
    u32  datahead_len;
    u8   datahead[DATA_HEAD_LEN];
    u8   type;
    u8   *rel;
} WVTP_SBUF, *PWVTP_SBUF;

typedef struct _WVTP_RBUF{
    u8   data[MAX_SEND_DATA_LEN+4];
    u32  len;
} WVTP_RBUF, *PWVTP_RBUF;

typedef struct _wvtp_context{

    //app callback function
    s8 (*app_sbuf_fill)(void* pcontext, u8 buf_index);
    void (*app_sbuf_release)(void* pcontext,u8 buf_index);
    int (*app_rbuf_handle)(void* pcontext,u8 buf_index);
    u32 (*make_frame_from_app_sbuf)(void* pcontext,u8 * pdata, u8 buf_index);
    u32 (*save_frame_to_app_rbuf)(void* pcontext,u8 * pdata, u32 len, u8 buf_index);

    //wvtp protocol parameters
    bool no_nak;
    u8 ack_rcved;
    u8 wvtp_ack_expected;
    u8 wvtp_next_seq_to_send;
    u8 wvtp_seq_expected;
    u8 wvtp_too_far;
    u8 NAK_FLAG;
    u8 nbuffered;
    u8 max_buffer;
    bool arrived[NR_BUFR];

    //protocol status parameters
    u32 getfailnum;
    u32 buffullnum;
    u32 recvnaknum;
    u32 sendpkgnum;
    u32 pollnum;


    ip_addr_t remote_ip;
    u16 remote_port;

    int NR_BUFS;


    u32 wvtp_timer_current;

    int SRTT, RTTVAR, RTO ;
    SENDFLAG * sendflag;
    u32 data_timeout;
    u32 maxretranstime;
    u32 resendnum;
    u32 resendnak;
    u32 senddatalen;
    u32 recvdatalen;
    u32 sbufresendnum ; //
    u32 RTT;
    u32 RTT1;

	int sendbuf_cnt;

    WVTP_SBUF * out_buf;
    WVTP_RBUF in_buf[NR_BUFR];

    u32 time;

    void * parg; //argument for callbacks, first parameter of cb_inbuf_handle() and cb_outbuf_xxx().

}wvtp_context_t;


void wvtp_init_with_buf(ip_addr_t *server_ip, int (*handle)(int), unsigned int timeout, int wvtp_num, WVTP_SBUF * out_buf, SENDFLAG * sendflag, int sendbuf_cnt);

#define wvtp_init2(server_ip, handle, timeout, wvtpnum, bufnum) \
	static WVTP_SBUF _wvtp_outbufs[wvtpnum * bufnum]; \
	static SENDFLAG _wvtp_sendflags[wvtpnum * bufnum]; \
	wvtp_init_with_buf(server_ip, handle, timeout, wvtpnum, &_wvtp_outbufs[0], &_wvtp_sendflags[0], bufnum);

#define wvtp_init(dev_index, handle, timeout, wvtpnum) wvtp_init2(dev_index, handle, timeout, wvtpnum, NR_BUFS_MAX)

int wvtp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port);
int wvtp_poll();
void * wvtp_new_context(int nr_bufs);
int wvtp_context_isfull(void);
void wvtp_set_linktimeout(u32 timeout);

#define WVTP_PORT	1000
#define event_newdata	0
#define event_poll	1

typedef struct tagWVTP_HANDLER
{
	int (*cb_inbuf_handle)(void * parg, char *inbuf, int inbuf_len);
	int (*cb_outbuf_get)(void * parg,void *p_dataHead, unsigned int *p_headLen, unsigned char **pp_dataAddr, unsigned int *p_dataLen, unsigned int capability, unsigned char *p_flag, unsigned char **pp_rel);
	void (*cb_outbuf_release)(void * parg, unsigned char *p_dataAddr, unsigned char flag, unsigned char *p_rel);
	void * (*cb_parg_new)(void);
	void (*cb_parg_release)(void * parg);
}WVTP_HANDLER;

extern WVTP_HANDLER wvtp_handler;

#ifdef CONFIG_WVTP_DUMMY
#undef MAX_SEND_DATA_LEN
#define MAX_SEND_DATA_LEN   ((1024 < bnep_l2cap_outmtu(bnep_pcb_cur))? 1024: bnep_l2cap_outmtu(bnep_pcb_cur)) 
#endif

#endif
