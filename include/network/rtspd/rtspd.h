#ifndef _RTSPD_H_
#define _RTSPD_H_

#define RTSP_DATA_LEN 1400
#define MAX_IP_ADDR_LEN 20
#define MAX_CON_CNT 20
typedef struct RTSP_CONFIG_T rtsp_config_t;

typedef struct RTSP_SESSION_S
{
	rtsp_config_t *rtsp_config;
	rtp_config_t rtp_config; //rtp_config 
	
	//stream info
	//audio
	int as_id;   //audio id
	int achannels;
	int asample_rate;
	//video
	unsigned int resolution;
	int framerate;
	int vs_id;   //video id
	int vs_type; // video type, 1: encv, 2: mimg, 0: others
	//stream
//	int av_fdist_start; //audio_fdist_start<<4|video_fdist_start  //TODO:only support one user now.
	int (*stream_open)(void * arg);
	int (*stream_close)(void * arg);
	void * stream_arg; //arg for stream_open/stream_close

	//network parameters	
	int client_socket;  //client socket
	int client_port;
	char client_ip[MAX_IP_ADDR_LEN];	
	pthread_mutex_t socket_mutex; //mutex for socket
	
	/*private*/
	int data_ready; 
	char sbuf[RTSP_DATA_LEN];
	char rbuf[RTSP_DATA_LEN];
	char type[16];
	char url[64];
	char vers[16];

	int seq;
	int status;
#define RTSP_SESSION_IDLE 0
#define RTSP_SESSION_PLAY 1
#define RTSP_SESSION_TEARDOWN 2
	int send_sta;
	int ssrc;
	int basetime;

	char server_ip[MAX_IP_ADDR_LEN];

	int sport_lo; //serverport
	int sport_hi;
	
	int cport_lo; //clientport
	int cport_hi;
	int rtp_ntptime;
	
	int pt[16];

	struct RTSP_SESSION_S *next;
	struct RTSP_SESSION_S *prev;
}rtsp_session_t;
	
typedef struct RTSP_CONFIG_T
{
	rtsp_session_t *sess_list; //session connection list
	
	//stream info
	//audio
	int as_id;   //audio id
	int achannels;
	int asample_rate;
	//video
	unsigned int resolution;
	int framerate;
	int vs_id;   //video id
	int vs_type; // video type, 1: encv, 2: mimg, 0: others
//	int av_fdist_start; //audio_fdist_start<<4|video_fdist_start  //TODO:only support one user now.
	int (*stream_open)(void * arg);
	int (*stream_close)(void * arg);
	void * stream_arg; //arg for stream_open/stream_close
	
	//internal network socket info
	int socket;
	int port;
	char ip[MAX_IP_ADDR_LEN]; //server ip
	//internal thread info
	int thread_sta;
	pthread_mutex_t mutex; //mutex for cons
	pthread_t thread;
	pthread_attr_t attr;
	int rtsp_status;  //for led status indecation. 0:idle 1:ready 2:transfering 3:stop.
	int rtsp_teardown_exit; //control rtsp routine when session teardown,1: exit 0: not exit
	int rtsp_sess_waitime;  //control wait time for new session to establish. if wait timeout, exit
	int sess_wt_cnt; //wait new session time counter
#define RTSP_STATUS_IDLE 0
#define RTSP_STATUS_READY 1
#define RTSP_STATUS_TRANS 2
#define RTSP_STATUS_STOP 3
}rtsp_config_t;


void rtspd_init(rtsp_config_t *rtsp);
int rtspd_start(rtsp_config_t *rtsp);
int rtspd_stop(rtsp_config_t *rtsp);

//FIXME: add only for temp usage
int rtspd_is_exited(rtsp_config_t *rtsp);

#endif
