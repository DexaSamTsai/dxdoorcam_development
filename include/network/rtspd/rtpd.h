#ifndef _RTPD_H_
#define _RTPD_H_

#define MAX_IP_ADDR_LEN 20
typedef struct LIBRTP_NET_S
{
	void *sess_arg;
	int type; //UDP or TCP
	char ip[MAX_IP_ADDR_LEN];
	int socket;
	int port;
	
	char client_ip[MAX_IP_ADDR_LEN];	
	int client_port;
	int client_socket;
	
	int (*data_parse)(void *sess_arg, char *buf, int len);
	int (*data_fill)(void *sess_arg, char **buf, int *len);

	struct LIBRTP_NET_S *next;
}librtp_net_t;

typedef struct LIBRTP_SESSION
{
	librtp_net_t rtp_net;
	librtp_net_t rtcp_net;
	libmux_rtp_cfg_t mux_rtp;
	int status; //stream status start=1  stop=0
#define RTP_SESSION_STOP 0
#define RTP_SESSION_START 1
	volatile int thread_sta;
	pthread_mutex_t mutex; //mutex for cons
	pthread_t thread;
	pthread_attr_t attr;
}librtp_session_t; //to associate mux_rtp with netsocket

typedef struct RTP_CONFIG_T
{
#ifdef CONFIG_AUDIO_EN
	librtp_session_t sess_a;
#endif
	librtp_session_t sess_v;
	librtp_net_t *net_list;

	//internal thread info
	volatile int thread_sta;
	pthread_mutex_t mutex; //mutex for cons
	pthread_t thread;
	pthread_attr_t attr;
}rtp_config_t;

/**
 * @brief <b>Start a rtp server.</b>
 * @details This fuction will create a rtp server, It will create rtp session server like session_a and session_v. And rtcp session will be created correspondently if RTCP_EN is configured.<br>
 * @param config The point of rtp config. 
 *
 * @return -1: rtp server created fail<br>
 * 	   0: rtp server created success<br>
 */
int rtpd_start(rtp_config_t *rtp_config);

/**
 * @brief <b>stop a rtp server.</b>
 * @details This fuction will stop a rtp server.<br>
 * @param rtp_config The point of rtp config.
 *
 * @return NULL <br> 
 */
int rtpd_stop(rtp_config_t *rtp_config);

/**
 * @brief <b>to exit rtspd.</b>
 * @details This fuction will check if this a rtp is exited.<br>
 * @param rtp_config The point of rtp config.
 *
 * @return NULL <br> 
 */
int rtpd_exit(rtp_config_t *rtp);

/**
 * @brief <b> check exit status of a rtp server</b>
 * @details This fuction check if this rtp server is exited.<br>
 * @param rtp_config The point of rtp config.
 *
 * @return 1: exited <br> 
 * 		   0: not exited <br>
 */
int rtpd_is_exited(rtp_config_t *rtp);

/**
 * @brief <b>get payload type of a rtp session.</b>
 * @details To get current payload type of current rtp session.<br>
 * @param session The point of rtp session.
 *
 * @return payload_type 
 */
int librtp_get_pt(librtp_session_t *session);

/**
 * @brief <b>get seq of a rtp session.</b>
 * @details To get current sequence number of current rtp session.<br>
 * @param session The point of rtp session.
 *
 * @return The sequence number of current rtp session 
 */
int librtp_get_seq(librtp_session_t *session);

/**
 * @brief <b>get ssrc of a rtp session.</b>
 * @details To get current ssrc of current rtp session.<br>
 * @param session The point of rtp session.
 *
 * @return The ssrc of current rtp session 
 */
int librtp_get_ssrc(librtp_session_t *session);

/**
 * @brief <b>get ts of a rtp session.</b>
 * @details To get current timestamp of current rtp session.<br>
 * @param session The point of rtp session.
 *
 * @return Timestamp of current rtp session 
 */
int librtp_get_ts(librtp_session_t *session);
#endif
