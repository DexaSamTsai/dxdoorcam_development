#ifndef __WIFI_BCM4334_H__
#define __WIFI_BCM4334_H__

#if defined(CONFIG_APP_AP_MFG_MODE) || defined(CONFIG_APP_STATION_MFG_MODE)
//this if defined for MFG Calibration
#define  MFG_CMD_SUPPORT

#endif

//#define WIFI_PS_MODE
//
#define WIFI_WEP64		1
#define WIFI_WEP128  	2
#define WIFI_WPA		3
#define WIFI_WPA2		4
#define WIFI_OPEN		0

#define WIFI_OPEN_NAME	"OPEN"
#define WIFI_WEP64_NAME "WEP64"
#define WIFI_WEP128_NAME "WEP128"
#define WIFI_WPA_NAME		"WPA"
#define WIFI_WPA2_NAME		"WPA2"


#define	WIFI_SEC_NONE	0
#define	WIFI_SEC_AES	1
#define WIFI_SEC_TKIP   2

#define	WIFI_SEC_NONE_NAME	"NONE"
#define	WIFI_SEC_AES_NAME	"AES"
#define WIFI_SEC_TKIP_NAME	"TKIP"



#if defined (CONFIG_APP_AP) && defined (CONFIG_APP_STATION) && defined (CONFIG_WIFIMODULE_W8782)
#define WIFI_DEV_STA		0
#define WIFI_DEV_UAP		1
#else
#define WIFI_DEV_STA		0
#define WIFI_DEV_UAP		0
#endif
/** MLAN BSS role */
#ifndef __MLAN_BSS_ROLE__
#define __MLAN_BSS_ROLE__
typedef enum _mlan_bss_role
{
    MLAN_BSS_ROLE_STA = 0,
    MLAN_BSS_ROLE_UAP = 1,
    MLAN_BSS_ROLE_MFG = 2,
    MLAN_BSS_ROLE_ANY = 0xff,
} mlan_bss_role;
#endif
/**
 * @defgroup lib_wifi
 * @ingroup lib_driver
 *@{
 */

/**
 * @ingroup lib_wifi
 *
 * @brief the wifi modes that can be set
 *
 */
typedef enum _WLAN_802_11_NETWORK_INFRASTRUCTURE
{
    Wlan802_11IBSS,
    Wlan802_11Infrastructure,
    Wlan802_11AutoUnknown,
    /* defined as upper bound */
    Wlan802_11InfrastructureMax
} WLAN_802_11_NETWORK_INFRASTRUCTURE, *PWLAN_802_11_NETWORK_INFRASTRUCTURE;

/**
 * @ingroup lib_wifi
 *
 * @brief the wifi message number
 *
 */

typedef struct __attribute__ ((packed)) wlan_802_11_msg_s {
	u8* buf;
	u32 buf_len;
}wlan_802_11_msg_t;

//FIXME: move to network/wifi/wifi.h, because all WIFI drivers should implement these API
/*
typedef enum _WLAN_802_11_MESSAGE
{
	WLAN_MSG_CONNECTED = 0x1,
	WLAN_MSG_DISCONNECTED,
	WLAN_MSG_AP_INIT_SUCCESS,
	WLAN_MSG_AP_INIT_FAILURE,
	WLAN_MSG_CONNECTED_AP,
	WLAN_MSG_DISCONNECTED_AP,
	WLAN_MSG_CALLBACK_PMK,
	WLAN_MSG_HIBERNATE,
	WLAN_MSG_CONNECTED_ALREADY,
	WLAN_MSG_END
} WLAN_802_11_MESSAGE;
*/

#define WLAN_SCAN_RES_FG_BEACON					0x01
#define WLAN_SCAN_RES_FG_RSSI_ONCHANNEL			0x04
#define WLAN_SCAN_RES_FG_BAND_2G				0x10
#define WLAN_SCAN_RES_FG_BAND_5G				0x20

typedef enum {
	WIFI_SECURITY_OPEN		= 0,
	WIFI_SECURITY_WEP		= 1,
	WIFI_SECURITY_WPA		= 2,
	WIFI_SECURITY_INVALID	= 3,
	WIFI_SECURITY_WPA2		= 4,
	WIFI_SECURITY_WPA_AES 	= 5,
	WIFI_SECURITY_WPA2_TKIP = 6,
	WIFI_SECURITY_WPS_OPEN  = 7,
	WIFI_SECURITY_WPS		= 8,
	WIFI_SECURITY_UNKNOWN 	= -1,
} wifi_security_t;

typedef enum {
	WIFI_SCAN_SECURITY_UNKNOWN 	= 0,
	WIFI_SCAN_SECURITY_OPEN		= 1,
	WIFI_SCAN_SECURITY_WEP		= 2,
	WIFI_SCAN_SECURITY_WPA		= 3,
	WIFI_SCAN_SECURITY_WPA2		= 4,
	WIFI_SCAN_SECURITY_WPA_WPA2	= 5,
	WIFI_SCAN_SECURITY_WPA_PSK		= 6,
	WIFI_SCAN_SECURITY_WPA2_PSK		= 7,
	WIFI_SCAN_SECURITY_WPA_WPA2_PSK	= 8,
	WIFI_SCAN_SECURITY_WPS  = 9,
} wifi_scan_security_t;

typedef enum {
	WIFI_SCAN_ENCRYPTION_UNKNOWN 	= 0,
	WIFI_SCAN_ENCRYPTION_NONE		= 1,
	WIFI_SCAN_ENCRYPTION_AES		= 2,
	WIFI_SCAN_ENCRYPTION_TKIP		= 3,
	WIFI_SCAN_ENCRYPTION_AES_TKIP	= 4,
	WIFI_SCAN_ENCRYPTION_WEP_OPEN	= 5,
	WIFI_SCAN_ENCRYPTION_WEP_SHARE	= 6,
	WIFI_SCAN_ENCRYPTION_WEP_OPEN_SHARE = 7,
} wifi_scan_encryption_t;

typedef enum {
	WIFI_BSS_TYPE_INFRA 	= 0,
	WIFI_BSS_TYPE_ADOC 		= 1,
	WIFI_BSS_TYPE_ANY 		= 2,
	WIFI_BSS_TYPE_UNKNOWN 	= -1,
} wifi_bss_type_t;
typedef enum {
	WIFI_WPS_NOSUPPORT 	= 0,
	WIFI_WPS_SUPPORT	= 1,
	WIFI_WPS_BUTTON		= 2,
	WIFI_WPS_PIN 		= 3,
} wifi_wps_status_t;

extern char* SCAN_SECURITY_STR[];
extern char* SCAN_ENCRYPTION_STR[];
extern char *BSS_TYPE_STR[];
extern char *WPS_STATUS_STR[];

typedef struct wifi_wep_key_s {
	short	index;
	short	length;
	u8	data[32];
} wifi_wep_key_t;

typedef struct __attribute__ ((packed)) wlan_scan_resp_s {
	int				ssid_len;
	u8				ssid[32];
	u8				bssid[6];
	u16				beacon_period;
	u16				capability;
	short			rssi;
	u32				ie_length;
	u8				*pie;
	u8				flags;
	u8				reserved;
	u16				channel;
	wifi_security_t         security;
	wifi_scan_security_t         scan_security;
	wifi_scan_encryption_t         scan_encryption;
	wifi_bss_type_t         bss_type;
} wlan_scan_resp_t;

typedef void (*wlan_scan_filter_fun)(void *ctx, wlan_scan_resp_t *pbss);

//#ifdef CONFIG_WIFIMODULE_BRCM4334X
typedef struct  __attribute__ ((packed)) my_ap_s {
    u8 ssid[32+1];
    u8 ssid_len;
    u8 bssid[6];
    u16 beacon_period;
    u16 capability;
    short rssi;
    u8 flags;
    u8 uuid[16];
    u16 channel; /*0x01 - 0x0E for 2G*/
    wifi_security_t         security;
    wifi_bss_type_t         bss_type;
	wifi_scan_security_t	scan_security;
	wifi_scan_encryption_t	scan_encryption;
	wifi_wps_status_t wps;
} my_ap_t,ap_info_t;

typedef struct {
	int ap_cnt;
	int max_cnt;
}ap_info_num_t;


#define MY_APLIST_MAX_APCNT     40
typedef struct  __attribute__ ((packed)) my_aplist_s {
    int apcnt;
    my_ap_t ap_array[MY_APLIST_MAX_APCNT];
} my_aplist_t;
//#endif


/**
 * @brief set wifi fw load memery buf addr before wlan_init
 * @detail addr store wifi-fw bin and download to wifi-devices
               make sure add has enough memory to load wifi fw
		  feature: CONFIG_WIFIMODULE_W8782
		               CONFIG_WIFIMODULE_NMC1000
		               CONFIG_WIFIMODULE_BRCM4334X
		               COMFIG_WIFIMODULE_NRX700
		               CONFIG_WIFIMODULE_W8686
 *
 * @param addr for buffer memory addr, 0: default, MEMBASE_256K MEMBASE_2M MEMBASE_EXT

 * @return 0: ok, !=0: error
 *
 */
int wlan_init_load_addr(unsigned int addr);
/**
 * @brief set wifi fw load memery buf addr before wlan_init
 * @detail addr store wifi-fw bin and download to wifi-devices
               make sure add has enough memory to load wifi fw
		  feature: CONFIG_WIFIMODULE_W8782
		               CONFIG_WIFIMODULE_NMC1000
		               CONFIG_WIFIMODULE_BRCM4334X
		               COMFIG_WIFIMODULE_NRX700
		               CONFIG_WIFIMODULE_W8686
 *
 * @param addr for buffer memory addr, 0: default, MEMBASE_256K MEMBASE_2M MEMBASE_EXT
 * @param len define buf len.
 * @return 0: ok, !=0: error
 *
 */
int wlan_init_load_addr_withlen(unsigned int addr,unsigned int len);
//#endif

/**
 * @brief libwifi initial
 *
 * @param arg argument for different wlan module.
              for w8686 SCIO module, it's the scio interface number of OV chip.
 * @return 0: ok, !=0: error
 *
 */
int wlan_init(void * arg);

/**
 * @brief exit wlan, free interrupt
 *
 * @param void
 *
 * @return =0 success ; !=0 failed.
 *
 */
int wlan_exit(void);

/**
 * @brief set wifi mode, Wlan802_11Infrastructure for normal mode and for Wlan802_11IBSS adhoc mode
 *
 * @param mode Wlan802_11Infrastructure for normal mode and for Wlan802_11IBSS adhoc mode
 * @param args in adhoc mode, 1: enable WEP, 0: disable WEP
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_mode(u32 mode, int args, int priv_index);

/**
 * @brief get current wifi mode
 *
 * @param mode current wifi mode
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_get_mode(u32 *mode);

/**
 * @brief set wifi ssid
 *
 * @param ssid ssid to set
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_essid(char* ssid, int encrypt_type,int priv_index);

/**
 * @brief set wifi ssid with prescan
 *
 * @param ssid ssid to set
 *
 * @return 0: ok, !=0: error
 *
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_set_essid_prescan(char *ssid, int encrypt_type, int priv_index);
#endif
/**
 * @brief get ssid
 *
 * @param ssid ssid to get
 * @param ssid_index ssid index in scan table
 *
 * @return 0: ok, !=0: error
 *
 */

/**
 * @brief set wifi ssid with extra control
 *
 * @param ssid ssid to set
 *
 * @param encrypt_type
 *
 * @param priv_index
 *
 * @param ctrl_flag control flag, Should be either 0 or SET_ESSID_FLAG_PRESCAN
 *
 * @param tmo timeout (in second), 0: never timeout, -1: default timeout (20 sec)
 *
 * @return 0: ok, !=0: error
 *
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_set_essid_ex(char* ssid, int encrypt_type, int priv_index, unsigned int ctrl_flag, int tmo);
#define SET_ESSID_FLAG_PRESCAN				0x0001
#define SET_ESSID_FLAG_MASK					0x0001
#endif

int wlan_get_essid(char* ssid, int ssid_index);

/**
 * @brief set adhoc channel
 *
 * @param channel channel to set
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_current_chan(int channel);

int wlan_get_current_chan(void);
/**
 * @brief set ap max assoc client num, it must be called before ap start.
 *
 * @param client_num the max assoc client num.
 *
 * @return 0: ok
 *
 */
int wlan_set_ap_max_assoc_client(u32 client_num);
/**
 * @brief set ap tx power
 *
 * @param tx power to set (0~20)
 *
 * @return 0: ok, !=0: error
 *
 */

int wlan_set_current_txpower(int txpower);
//int wlan_get_current_txpower(void);

#ifdef CONFIG_WIFIMODULE_BRCM4334X
/**
 * Macro for creating country codes
 */
#define MK_CNTRY( a, b)  (((unsigned char)(a)) + (((unsigned char)(b))<<8))
/**
 * Enumerated list of country codes
 */
typedef enum
{
    WLAN_COUNTRY_AFGHANISTAN                                     = MK_CNTRY( 'A', 'F' ),             /* AF Afghanistan */
    WLAN_COUNTRY_ALBANIA                                         = MK_CNTRY( 'A', 'L' ),             /* AL Albania */
    WLAN_COUNTRY_ALGERIA                                         = MK_CNTRY( 'D', 'Z' ),             /* DZ Algeria */
    WLAN_COUNTRY_AMERICAN_SAMOA                                  = MK_CNTRY( 'A', 'S' ),             /* AS American_Samoa */
    WLAN_COUNTRY_ANDORRA                                         = MK_CNTRY( 'A', 'D' ),             /* AD Andorra */
    WLAN_COUNTRY_ANGOLA                                          = MK_CNTRY( 'A', 'O' ),             /* AO Angola */
    WLAN_COUNTRY_ANGUILLA                                        = MK_CNTRY( 'A', 'I' ),             /* AI Anguilla */
    WLAN_COUNTRY_ANTARCTICA                                      = MK_CNTRY( 'A', 'Q' ),             /* AQ Antarctica */
    WLAN_COUNTRY_ANTIGUA_AND_BARBUDA                             = MK_CNTRY( 'A', 'G' ),             /* AG Antigua_and_Barbuda */
    WLAN_COUNTRY_ARGENTINA                                       = MK_CNTRY( 'A', 'R' ),             /* AR Argentina */
    WLAN_COUNTRY_ARMENIA                                         = MK_CNTRY( 'A', 'M' ),             /* AM Armenia */
    WLAN_COUNTRY_ARUBA                                           = MK_CNTRY( 'A', 'W' ),             /* AW Aruba */
    WLAN_COUNTRY_ASCENSION_ISLAND                                = MK_CNTRY( 'A', 'C' ),             /* AC Ascension Island */
    WLAN_COUNTRY_AUSTRALIA                                       = MK_CNTRY( 'A', 'U' ),             /* AU Australia */
    WLAN_COUNTRY_AUSTRIA                                         = MK_CNTRY( 'A', 'T' ),             /* AT Austria */
    WLAN_COUNTRY_AZERBAIJAN                                      = MK_CNTRY( 'A', 'Z' ),             /* AZ Azerbaijan */
    WLAN_COUNTRY_BAHAMAS                                         = MK_CNTRY( 'B', 'S' ),             /* BS Bahamas */
    WLAN_COUNTRY_BAHRAIN                                         = MK_CNTRY( 'B', 'H' ),             /* BH Bahrain */
    WLAN_COUNTRY_BANGLADESH                                      = MK_CNTRY( 'B', 'D' ),             /* BD Bangladesh */
    WLAN_COUNTRY_BARBADOS                                        = MK_CNTRY( 'B', 'B' ),             /* BB Barbados */
    WLAN_COUNTRY_BELARUS                                         = MK_CNTRY( 'B', 'Y' ),             /* BY Belarus */
    WLAN_COUNTRY_BELGIUM                                         = MK_CNTRY( 'B', 'E' ),             /* BE Belgium */
    WLAN_COUNTRY_BELIZE                                          = MK_CNTRY( 'B', 'Z' ),             /* BZ Belize */
    WLAN_COUNTRY_BENIN                                           = MK_CNTRY( 'B', 'J' ),             /* BJ Benin */
    WLAN_COUNTRY_BERMUDA                                         = MK_CNTRY( 'B', 'M' ),             /* BM Bermuda */
    WLAN_COUNTRY_BHUTAN                                          = MK_CNTRY( 'B', 'T' ),             /* BT Bhutan */
    WLAN_COUNTRY_BOLIVIA                                         = MK_CNTRY( 'B', 'O' ),             /* BO Bolivia */
    WLAN_COUNTRY_BOSNIA_AND_HERZEGOVINA                          = MK_CNTRY( 'B', 'A' ),             /* BA Bosnia_and_Herzegovina */
    WLAN_COUNTRY_BOTSWANA                                        = MK_CNTRY( 'B', 'W' ),             /* BW Botswana */
    WLAN_COUNTRY_BOUVET_ISLAND                                   = MK_CNTRY( 'B', 'V' ),             /* Bouvet Island */
    WLAN_COUNTRY_BRAZIL                                          = MK_CNTRY( 'B', 'R' ),             /* BR Brazil */
    WLAN_COUNTRY_BRITISH_INDIAN_OCEAN_TERRITORY                  = MK_CNTRY( 'I', 'O' ),             /* IO British_Indian_Ocean_Territory */
    WLAN_COUNTRY_BRUNEI_DARUSSALAM                               = MK_CNTRY( 'B', 'N' ),             /* BN Brunei_Darussalam */
    WLAN_COUNTRY_BULGARIA                                        = MK_CNTRY( 'B', 'G' ),             /* BG Bulgaria */
    WLAN_COUNTRY_BURKINA_FASO                                    = MK_CNTRY( 'B', 'F' ),             /* BF Burkina_Faso */
    WLAN_COUNTRY_BURUNDI                                         = MK_CNTRY( 'B', 'I' ),             /* BI Burundi */
    WLAN_COUNTRY_CAMBODIA                                        = MK_CNTRY( 'K', 'H' ),             /* KH Cambodia */
    WLAN_COUNTRY_CAMEROON                                        = MK_CNTRY( 'C', 'M' ),             /* CM Cameroon */
    WLAN_COUNTRY_CANADA                                          = MK_CNTRY( 'C', 'A' ),             /* CA Canada */
    WLAN_COUNTRY_CAPE_VERDE                                      = MK_CNTRY( 'C', 'V' ),             /* CV Cape_Verde */
    WLAN_COUNTRY_CAYMAN_ISLANDS                                  = MK_CNTRY( 'K', 'Y' ),             /* KY Cayman_Islands */
    WLAN_COUNTRY_CENTRAL_AFRICAN_REPUBLIC                        = MK_CNTRY( 'C', 'F' ),             /* CF Central_African_Republic */
    WLAN_COUNTRY_CHAD                                            = MK_CNTRY( 'T', 'D' ),             /* TD Chad */
    WLAN_COUNTRY_CHILE                                           = MK_CNTRY( 'C', 'L' ),             /* CL Chile */
    WLAN_COUNTRY_CHINA                                           = MK_CNTRY( 'C', 'N' ),             /* CN China */
    WLAN_COUNTRY_CHRISTMAS_ISLAND                                = MK_CNTRY( 'C', 'X' ),             /* CX Christmas_Island */
    WLAN_COUNTRY_CLIPPERTON_ISLAND                               = MK_CNTRY( 'C', 'P' ),             /* CP Clipperton Island */
    WLAN_COUNTRY_COCOS_ISLANDS                                   = MK_CNTRY( 'C', 'C' ),             /* CC Cocos Island */
    WLAN_COUNTRY_COLOMBIA                                        = MK_CNTRY( 'C', 'O' ),             /* CO Colombia */
    WLAN_COUNTRY_COMOROS                                         = MK_CNTRY( 'K', 'M' ),             /* KM Comoros */
    WLAN_COUNTRY_CONGO                                           = MK_CNTRY( 'C', 'G' ),             /* CG Congo */
    WLAN_COUNTRY_CONGO_THE_DEMOCRATIC_REPUBLIC_OF_THE            = MK_CNTRY( 'C', 'D' ),             /* CD Congo,_The_Democratic_Republic_Of_The */
    WLAN_COUNTRY_COOK_ISLAND                                     = MK_CNTRY( 'C', 'K' ),             /* CK Cook Island */
    WLAN_COUNTRY_COSTA_RICA                                      = MK_CNTRY( 'C', 'R' ),             /* CR Costa_Rica */
    WLAN_COUNTRY_COTE_DIVOIRE                                    = MK_CNTRY( 'C', 'I' ),             /* CI Cote_D'ivoire */
    WLAN_COUNTRY_CROATIA                                         = MK_CNTRY( 'H', 'R' ),             /* HR Croatia */
    WLAN_COUNTRY_CUBA                                            = MK_CNTRY( 'C', 'U' ),             /* CU Cuba */
    WLAN_COUNTRY_CYPRUS                                          = MK_CNTRY( 'C', 'Y' ),             /* CY Cyprus */
    WLAN_COUNTRY_CZECH_REPUBLIC                                  = MK_CNTRY( 'C', 'Z' ),             /* CZ Czech_Republic */
    WLAN_COUNTRY_DENMARK                                         = MK_CNTRY( 'D', 'K' ),             /* DK Denmark */
    WLAN_COUNTRY_DJIBOUTI                                        = MK_CNTRY( 'D', 'J' ),             /* DJ Djibouti */
    WLAN_COUNTRY_DOMINICA                                        = MK_CNTRY( 'D', 'M' ),             /* DM Dominica */
    WLAN_COUNTRY_DOMINICAN_REPUBLIC                              = MK_CNTRY( 'D', 'O' ),             /* DO Dominican_Republic */
    WLAN_COUNTRY_ECUADOR                                         = MK_CNTRY( 'E', 'C' ),             /* EC Ecuador */
    WLAN_COUNTRY_EGYPT                                           = MK_CNTRY( 'E', 'G' ),             /* EG Egypt */
    WLAN_COUNTRY_EL_SALVADOR                                     = MK_CNTRY( 'S', 'V' ),             /* SV El_Salvador */
    WLAN_COUNTRY_EQUATORIAL_GUINEA                               = MK_CNTRY( 'G', 'Q' ),             /* GQ Equatorial_Guinea */
    WLAN_COUNTRY_ERITREA                                         = MK_CNTRY( 'E', 'R' ),             /* ER Eritrea */
    WLAN_COUNTRY_ESTONIA                                         = MK_CNTRY( 'E', 'E' ),             /* EE Estonia */
    WLAN_COUNTRY_ETHIOPIA                                        = MK_CNTRY( 'E', 'T' ),             /* ET Ethiopia */
    WLAN_COUNTRY_EUROPEAN_UNION                                  = MK_CNTRY( 'E', '0' ),             /* E0 European Union */
    WLAN_COUNTRY_FALKLAND_ISLANDS_MALVINAS                       = MK_CNTRY( 'F', 'K' ),             /* FK Falkland_Islands_(Malvinas) */
    WLAN_COUNTRY_FAROE_ISLANDS                                   = MK_CNTRY( 'F', 'O' ),             /* FO Faroe_Islands */
    WLAN_COUNTRY_FIJI                                            = MK_CNTRY( 'F', 'J' ),             /* FJ Fiji */
    WLAN_COUNTRY_FINLAND                                         = MK_CNTRY( 'F', 'I' ),             /* FI Finland */
    WLAN_COUNTRY_FRANCE                                          = MK_CNTRY( 'F', 'R' ),             /* FR France */
    WLAN_COUNTRY_FRENCH_GUINA                                    = MK_CNTRY( 'G', 'F' ),             /* GF French_Guina */
    WLAN_COUNTRY_FRENCH_POLYNESIA                                = MK_CNTRY( 'P', 'F' ),             /* PF French_Polynesia */
    WLAN_COUNTRY_FRENCH_SOUTHERN_TERRITORIES                     = MK_CNTRY( 'T', 'F' ),             /* TF French_Southern_Territories */
    WLAN_COUNTRY_GABON                                           = MK_CNTRY( 'G', 'A' ),             /* GA Gabon */
    WLAN_COUNTRY_GAMBIA                                          = MK_CNTRY( 'G', 'M' ),             /* GM Gambia */
    WLAN_COUNTRY_GEORGIA                                         = MK_CNTRY( 'G', 'E' ),             /* GE Georgia */
    WLAN_COUNTRY_GERMANY                                         = MK_CNTRY( 'D', 'E' ),             /* DE Germany */
    WLAN_COUNTRY_GHANA                                           = MK_CNTRY( 'G', 'H' ),             /* GH Ghana */
    WLAN_COUNTRY_GIBRALTAR                                       = MK_CNTRY( 'G', 'I' ),             /* GI Gibraltar */
    WLAN_COUNTRY_GREECE                                          = MK_CNTRY( 'G', 'R' ),             /* GR Greece */
    WLAN_COUNTRY_GREENLAND                                       = MK_CNTRY( 'G', 'L' ),             /* GL Greenland*/
    WLAN_COUNTRY_GRENADA                                         = MK_CNTRY( 'G', 'D' ),             /* GD Grenada */
    WLAN_COUNTRY_GUADELOUPE                                      = MK_CNTRY( 'G', 'P' ),             /* GP Guadeloupe */
    WLAN_COUNTRY_GUAM                                            = MK_CNTRY( 'G', 'U' ),             /* GU Guam */
    WLAN_COUNTRY_GUATEMALA                                       = MK_CNTRY( 'G', 'T' ),             /* GT Guatemala */
    WLAN_COUNTRY_GUERNSEY                                        = MK_CNTRY( 'G', 'G' ),             /* GG Guernsey */
    WLAN_COUNTRY_GUINEA                                          = MK_CNTRY( 'G', 'N' ),             /* GN Guinea */
    WLAN_COUNTRY_GUINEA_BISSAU                                   = MK_CNTRY( 'G', 'W' ),             /* GW Guinea-bissau */
    WLAN_COUNTRY_GUYANA                                          = MK_CNTRY( 'G', 'Y' ),             /* GY Guyana */
    WLAN_COUNTRY_HAITI                                           = MK_CNTRY( 'H', 'T' ),             /* HT Haiti */
    WLAN_COUNTRY_HEARD_ISLAND_AND_MCDONALD_ISLAND                = MK_CNTRY( 'H', 'M' ),             /* HM Heard Island and Mcdonald Island */
    WLAN_COUNTRY_HOLY_SEE_VATICAN_CITY_STATE                     = MK_CNTRY( 'V', 'A' ),             /* VA Holy_See_(Vatican_City_State) */
    WLAN_COUNTRY_HONDURAS                                        = MK_CNTRY( 'H', 'N' ),             /* HN Honduras */
    WLAN_COUNTRY_HONG_KONG                                       = MK_CNTRY( 'H', 'K' ),             /* HK Hong_Kong */
    WLAN_COUNTRY_HUNGARY                                         = MK_CNTRY( 'H', 'U' ),             /* HU Hungary */
    WLAN_COUNTRY_ICELAND                                         = MK_CNTRY( 'I', 'S' ),             /* IS Iceland */
    WLAN_COUNTRY_INDIA                                           = MK_CNTRY( 'I', 'N' ),             /* IN India */
    WLAN_COUNTRY_INDONESIA                                       = MK_CNTRY( 'I', 'D' ),             /* ID Indonesia */
    WLAN_COUNTRY_IRAN_ISLAMIC_REPUBLIC_OF                        = MK_CNTRY( 'I', 'R' ),             /* IR Iran,_Islamic_Republic_Of */
    WLAN_COUNTRY_IRAQ                                            = MK_CNTRY( 'I', 'Q' ),             /* IQ Iraq */
    WLAN_COUNTRY_IRELAND                                         = MK_CNTRY( 'I', 'E' ),             /* IE Ireland */
    WLAN_COUNTRY_ISRAEL                                          = MK_CNTRY( 'I', 'L' ),             /* IL Israel */
    WLAN_COUNTRY_ITALY                                           = MK_CNTRY( 'I', 'T' ),             /* IT Italy */
    WLAN_COUNTRY_JAMAICA                                         = MK_CNTRY( 'J', 'M' ),             /* JM Jamaica */
    WLAN_COUNTRY_JAPAN                                           = MK_CNTRY( 'J', 'P' ),             /* JP Japan */
    WLAN_COUNTRY_JERSEY                                          = MK_CNTRY( 'J', 'E' ),             /* JE Jersey */
    WLAN_COUNTRY_JORDAN                                          = MK_CNTRY( 'J', 'O' ),             /* JO Jordan */
    WLAN_COUNTRY_KAZAKHSTAN                                      = MK_CNTRY( 'K', 'Z' ),             /* KZ Kazakhstan */
    WLAN_COUNTRY_KENYA                                           = MK_CNTRY( 'K', 'E' ),             /* KE Kenya */
    WLAN_COUNTRY_KIRIBATI                                        = MK_CNTRY( 'K', 'I' ),             /* KI Kiribati */
    WLAN_COUNTRY_KOREA_DEMOCRATIC_PEOPLES_REPUBLIC_OF            = MK_CNTRY( 'K', 'P' ),             /* KP Korea, Democratic People's Repbuic */
    WLAN_COUNTRY_KOREA_REPUBLIC_OF                               = MK_CNTRY( 'K', 'R' ),             /* KR Korea,_Republic_Of */
    WLAN_COUNTRY_KOSOVO                                          = MK_CNTRY( '0', 'A' ),             /* 0A Kosovo */
    WLAN_COUNTRY_KUWAIT                                          = MK_CNTRY( 'K', 'W' ),             /* KW Kuwait */
    WLAN_COUNTRY_KYRGYZSTAN                                      = MK_CNTRY( 'K', 'G' ),             /* KG Kyrgyzstan */
    WLAN_COUNTRY_LAO_PEOPLES_DEMOCRATIC_REPUBIC                  = MK_CNTRY( 'L', 'A' ),             /* LA Lao_People's_Democratic_Repubic */
    WLAN_COUNTRY_LATVIA                                          = MK_CNTRY( 'L', 'V' ),             /* LV Latvia */
    WLAN_COUNTRY_LEBANON                                         = MK_CNTRY( 'L', 'B' ),             /* LB Lebanon */
    WLAN_COUNTRY_LESOTHO                                         = MK_CNTRY( 'L', 'S' ),             /* LS Lesotho */
    WLAN_COUNTRY_LIBERIA                                         = MK_CNTRY( 'L', 'R' ),             /* LR Liberia */
    WLAN_COUNTRY_LIBYAN_ARAB_JAMAHIRIYA                          = MK_CNTRY( 'L', 'Y' ),             /* LY Libyan_Arab_Jamahiriya */
    WLAN_COUNTRY_LIECHTENSTEIN                                   = MK_CNTRY( 'L', 'I' ),             /* LI Liechtenstein */
    WLAN_COUNTRY_LITHUANIA                                       = MK_CNTRY( 'L', 'T' ),             /* LT Lithuania */
    WLAN_COUNTRY_LUXEMBOURG                                      = MK_CNTRY( 'L', 'U' ),             /* LU Luxembourg */
    WLAN_COUNTRY_MACAO                                           = MK_CNTRY( 'M', 'O' ),             /* MO Macao */
    WLAN_COUNTRY_MACEDONIA_FORMER_YUGOSLAV_REPUBLIC_OF           = MK_CNTRY( 'M', 'K' ),             /* MK Macedonia,_Former_Yugoslav_Republic_Of */
    WLAN_COUNTRY_MADAGASCAR                                      = MK_CNTRY( 'M', 'G' ),             /* MG Madagascar */
    WLAN_COUNTRY_MALAWI                                          = MK_CNTRY( 'M', 'W' ),             /* MW Malawi */
    WLAN_COUNTRY_MALAYSIA                                        = MK_CNTRY( 'M', 'Y' ),             /* MY Malaysia */
    WLAN_COUNTRY_MALDIVES                                        = MK_CNTRY( 'M', 'V' ),             /* MV Maldives */
    WLAN_COUNTRY_MALI                                            = MK_CNTRY( 'M', 'L' ),             /* ML Mali */
    WLAN_COUNTRY_MALTA                                           = MK_CNTRY( 'M', 'T' ),             /* MT Malta */
    WLAN_COUNTRY_MAN_ISLE_OF                                     = MK_CNTRY( 'I', 'M' ),             /* IM Man,_Isle_Of */
    WLAN_COUNTRY_MARTINIQUE                                      = MK_CNTRY( 'M', 'Q' ),             /* MQ Martinique */
    WLAN_COUNTRY_MAURITANIA                                      = MK_CNTRY( 'M', 'R' ),             /* MR Mauritania */
    WLAN_COUNTRY_MAURITIUS                                       = MK_CNTRY( 'M', 'U' ),             /* MU Mauritius */
    WLAN_COUNTRY_MAYOTTE                                         = MK_CNTRY( 'Y', 'T' ),             /* YT Mayotte */
    WLAN_COUNTRY_MEXICO                                          = MK_CNTRY( 'M', 'X' ),             /* MX Mexico */
    WLAN_COUNTRY_MICRONESIA_FEDERATED_STATES_OF                  = MK_CNTRY( 'F', 'M' ),             /* FM Micronesia,_Federated_States_Of */
    WLAN_COUNTRY_MOLDOVA_REPUBLIC_OF                             = MK_CNTRY( 'M', 'D' ),             /* MD Moldova,_Republic_Of */
    WLAN_COUNTRY_MONACO                                          = MK_CNTRY( 'M', 'C' ),             /* MC Monaco */
    WLAN_COUNTRY_MONGOLIA                                        = MK_CNTRY( 'M', 'N' ),             /* MN Mongolia */
    WLAN_COUNTRY_MONTENEGRO                                      = MK_CNTRY( 'M', 'E' ),             /* ME Montenegro */
    WLAN_COUNTRY_MONTSERRAT                                      = MK_CNTRY( 'M', 'S' ),             /* MS Montserrat */
    WLAN_COUNTRY_MOROCCO                                         = MK_CNTRY( 'M', 'A' ),             /* MA Morocco */
    WLAN_COUNTRY_MOZAMBIQUE                                      = MK_CNTRY( 'M', 'Z' ),             /* MZ Mozambique */
    WLAN_COUNTRY_MYANMAR                                         = MK_CNTRY( 'M', 'M' ),             /* MM Myanmar */
    WLAN_COUNTRY_NAMIBIA                                         = MK_CNTRY( 'N', 'A' ),             /* NA Namibia */
    WLAN_COUNTRY_NAURU                                           = MK_CNTRY( 'N', 'R' ),             /* NR Nauru */
    WLAN_COUNTRY_NEPAL                                           = MK_CNTRY( 'N', 'P' ),             /* NP Nepal */
    WLAN_COUNTRY_NETHERLANDS                                     = MK_CNTRY( 'N', 'L' ),             /* NL Netherlands */
    WLAN_COUNTRY_NETHERLANDS_ANTILLES                            = MK_CNTRY( 'A', 'N' ),             /* AN Netherlands_Antilles */
    WLAN_COUNTRY_NEW_CALEDONIA                                   = MK_CNTRY( 'N', 'C' ),             /* NC New_Caledonia */
    WLAN_COUNTRY_NEW_ZEALAND                                     = MK_CNTRY( 'N', 'Z' ),             /* NZ New_Zealand */
    WLAN_COUNTRY_NICARAGUA                                       = MK_CNTRY( 'N', 'I' ),             /* NI Nicaragua */
    WLAN_COUNTRY_NIGER                                           = MK_CNTRY( 'N', 'E' ),             /* NE Niger */
    WLAN_COUNTRY_NIGERIA                                         = MK_CNTRY( 'N', 'G' ),             /* NG Nigeria */
    WLAN_COUNTRY_NIUE                                            = MK_CNTRY( 'N', 'U' ),             /* NU Niue */
    WLAN_COUNTRY_NORFOLK_ISLAND                                  = MK_CNTRY( 'N', 'F' ),             /* NF Norfolk_Island */
    WLAN_COUNTRY_NORTHERN_MARIANA_ISLANDS                        = MK_CNTRY( 'M', 'P' ),             /* MP Northern_Mariana_Islands */
    WLAN_COUNTRY_NORWAY                                          = MK_CNTRY( 'N', 'O' ),             /* NO Norway */
    WLAN_COUNTRY_OMAN                                            = MK_CNTRY( 'O', 'M' ),             /* OM Oman */
    WLAN_COUNTRY_PAKISTAN                                        = MK_CNTRY( 'P', 'K' ),             /* PK Pakistan */
    WLAN_COUNTRY_PALAU                                           = MK_CNTRY( 'P', 'W' ),             /* PW Palau */
    WLAN_COUNTRY_PALESTINIAN_TERRITORY                           = MK_CNTRY( 'P', 'S' ),             /* PS Palestinian Territory, Occupied */
    WLAN_COUNTRY_PANAMA                                          = MK_CNTRY( 'P', 'A' ),             /* PA Panama */
    WLAN_COUNTRY_PAPUA_NEW_GUINEA                                = MK_CNTRY( 'P', 'G' ),             /* PG Papua_New_Guinea */
    WLAN_COUNTRY_PARAGUAY                                        = MK_CNTRY( 'P', 'Y' ),             /* PY Paraguay */
    WLAN_COUNTRY_PERU                                            = MK_CNTRY( 'P', 'E' ),             /* PE Peru */
    WLAN_COUNTRY_PHILIPPINES                                     = MK_CNTRY( 'P', 'H' ),             /* PH Philippines */
    WLAN_COUNTRY_PITCAIRN                                        = MK_CNTRY( 'P', 'N' ),             /* PN Pitcairn */
    WLAN_COUNTRY_POLAND                                          = MK_CNTRY( 'P', 'L' ),             /* PL Poland */
    WLAN_COUNTRY_PORTUGAL                                        = MK_CNTRY( 'P', 'T' ),             /* PT Portugal */
    WLAN_COUNTRY_PUETO_RICO                                      = MK_CNTRY( 'P', 'R' ),             /* PR Pueto_Rico */
    WLAN_COUNTRY_QATAR                                           = MK_CNTRY( 'Q', 'A' ),             /* QA Qatar */
    WLAN_COUNTRY_REUNION                                         = MK_CNTRY( 'R', 'E' ),             /* RE Reunion */
    WLAN_COUNTRY_ROMANIA                                         = MK_CNTRY( 'R', 'O' ),             /* RO Romania */
    WLAN_COUNTRY_RUSSIAN_FEDERATION                              = MK_CNTRY( 'R', 'U' ),             /* RU Russian_Federation */
    WLAN_COUNTRY_RWANDA                                          = MK_CNTRY( 'R', 'W' ),             /* RW Rwanda */
    WLAN_COUNTRY_SAINT_HELENA                                    = MK_CNTRY( 'S', 'H' ),             /* SH Saint Henena */
    WLAN_COUNTRY_SAINT_KITTS_AND_NEVIS                           = MK_CNTRY( 'K', 'N' ),             /* KN Saint_Kitts_and_Nevis */
    WLAN_COUNTRY_SAINT_LUCIA                                     = MK_CNTRY( 'L', 'C' ),             /* LC Saint_Lucia */
    WLAN_COUNTRY_SAINT_PIERRE_AND_MIQUELON                       = MK_CNTRY( 'P', 'M' ),             /* PM Saint_Pierre_and_Miquelon */
    WLAN_COUNTRY_SAINT_VINCENT_AND_THE_GRENADINES                = MK_CNTRY( 'V', 'C' ),             /* VC Saint_Vincent_and_The_Grenadines */
    WLAN_COUNTRY_SAMOA                                           = MK_CNTRY( 'W', 'S' ),             /* WS Samoa */
    WLAN_COUNTRY_SAN_MARINO                                      = MK_CNTRY( 'S', 'M' ),             /* SM San Marino */
    WLAN_COUNTRY_SANIT_MARTIN_SINT_MARTEEN                       = MK_CNTRY( 'M', 'F' ),             /* MF Sanit_Martin_/_Sint_Marteen */
    WLAN_COUNTRY_SAO_TOME_AND_PRINCIPE                           = MK_CNTRY( 'S', 'T' ),             /* ST Sao_Tome_and_Principe */
    WLAN_COUNTRY_SAUDI_ARABIA                                    = MK_CNTRY( 'S', 'A' ),             /* SA Saudi_Arabia */
    WLAN_COUNTRY_SENEGAL                                         = MK_CNTRY( 'S', 'N' ),             /* SN Senegal */
    WLAN_COUNTRY_SERBIA                                          = MK_CNTRY( 'R', 'S' ),             /* RS Serbia */
    WLAN_COUNTRY_SEYCHELLES                                      = MK_CNTRY( 'S', 'C' ),             /* SC Seychelles */
    WLAN_COUNTRY_SIERRA_LEONE                                    = MK_CNTRY( 'S', 'L' ),             /* SL Sierra_Leone */
    WLAN_COUNTRY_SINGAPORE                                       = MK_CNTRY( 'S', 'G' ),             /* SG Singapore */
    WLAN_COUNTRY_SLOVAKIA                                        = MK_CNTRY( 'S', 'K' ),             /* SK Slovakia */
    WLAN_COUNTRY_SLOVENIA                                        = MK_CNTRY( 'S', 'I' ),             /* SI Slovenia */
    WLAN_COUNTRY_SOLOMON_ISLANDS                                 = MK_CNTRY( 'S', 'B' ),             /* SB Solomon_Islands */
    WLAN_COUNTRY_SOMALIA                                         = MK_CNTRY( 'S', 'O' ),             /* SO Somalia */
    WLAN_COUNTRY_SOUTH_AFRICA                                    = MK_CNTRY( 'Z', 'A' ),             /* ZA South_Africa */
    WLAN_COUNTRY_SPAIN                                           = MK_CNTRY( 'E', 'S' ),             /* ES Spain */
    WLAN_COUNTRY_SRI_LANKA                                       = MK_CNTRY( 'L', 'K' ),             /* LK Sri_Lanka */
    WLAN_COUNTRY_SUDAN                                           = MK_CNTRY( 'S', 'D' ),             /* SD Sudan */
    WLAN_COUNTRY_SURINAME                                        = MK_CNTRY( 'S', 'R' ),             /* SR Suriname */
    WLAN_COUNTRY_SVALBARD_AND_JAN_MAYEN                          = MK_CNTRY( 'S', 'J' ),             /* SJ Svalbard and Jan Mayen */
    WLAN_COUNTRY_SWAZILAND                                       = MK_CNTRY( 'S', 'Z' ),             /* SZ Swaziland */
    WLAN_COUNTRY_SWEDEN                                          = MK_CNTRY( 'S', 'E' ),             /* SE Sweden */
    WLAN_COUNTRY_SWITZERLAND                                     = MK_CNTRY( 'C', 'H' ),             /* CH Switzerland */
    WLAN_COUNTRY_SYRIAN_ARAB_REPUBLIC                            = MK_CNTRY( 'S', 'Y' ),             /* SY Syrian_Arab_Republic */
    WLAN_COUNTRY_TAIWAN_PROVINCE_OF_CHINA                        = MK_CNTRY( 'T', 'W' ),             /* TW Taiwan,_Province_Of_China */
    WLAN_COUNTRY_TAJIKISTAN                                      = MK_CNTRY( 'T', 'J' ),             /* TJ Tajikistan */
    WLAN_COUNTRY_TANZANIA_UNITED_REPUBLIC_OF                     = MK_CNTRY( 'T', 'Z' ),             /* TZ Tanzania,_United_Republic_Of */
    WLAN_COUNTRY_THAILAND                                        = MK_CNTRY( 'T', 'H' ),             /* TH Thailand */
    WLAN_COUNTRY_TIMOR_LESTE                                     = MK_CNTRY( 'T', 'L' ),             /* TL Timor-Leste (East Timor) */
    WLAN_COUNTRY_TOGO                                            = MK_CNTRY( 'T', 'G' ),             /* TG Togo */
    WLAN_COUNTRY_TOKELAU                                         = MK_CNTRY( 'T', 'K' ),             /* TK Tokelau*/
    WLAN_COUNTRY_TONGA                                           = MK_CNTRY( 'T', 'O' ),             /* TO Tonga */
    WLAN_COUNTRY_TRINIDAD_AND_TOBAGO                             = MK_CNTRY( 'T', 'T' ),             /* TT Trinidad_and_Tobago */
    WLAN_COUNTRY_TUNISIA                                         = MK_CNTRY( 'T', 'N' ),             /* TN Tunisia */
    WLAN_COUNTRY_TURKEY                                          = MK_CNTRY( 'T', 'R' ),             /* TR Turkey */
    WLAN_COUNTRY_TURKMENISTAN                                    = MK_CNTRY( 'T', 'M' ),             /* TM Turkmenistan */
    WLAN_COUNTRY_TURKS_AND_CAICOS_ISLANDS                        = MK_CNTRY( 'T', 'C' ),             /* TC Turks_and_Caicos_Islands */
    WLAN_COUNTRY_TUVALU                                          = MK_CNTRY( 'T', 'V' ),             /* TV Tuvalu */
    WLAN_COUNTRY_UGANDA                                          = MK_CNTRY( 'U', 'G' ),             /* UG Uganda */
    WLAN_COUNTRY_UKRAINE                                         = MK_CNTRY( 'U', 'A' ),             /* UA Ukraine */
    WLAN_COUNTRY_UNITED_ARAB_EMIRATES                            = MK_CNTRY( 'A', 'E' ),             /* AE United_Arab_Emirates */
    WLAN_COUNTRY_UNITED_KINGDOM                                  = MK_CNTRY( 'G', 'B' ),             /* GB United_Kingdom */
    WLAN_COUNTRY_UNITED_STATES                                   = MK_CNTRY( 'U', 'S' ),             /* US United_States */
    WLAN_COUNTRY_UNITED_STATES_1                                 = MK_CNTRY( 'Q', '1' ),             /* Q1 United States */
    WLAN_COUNTRY_UNITED_STATES_MINOR_OUTLYING_ISLANDS            = MK_CNTRY( 'U', 'M' ),             /* UM United_States_Minor_Outlying_Islands */
    WLAN_COUNTRY_URUGUAY                                         = MK_CNTRY( 'U', 'Y' ),             /* UY Uruguay */
    WLAN_COUNTRY_UZBEKISTAN                                      = MK_CNTRY( 'U', 'Z' ),             /* UZ Uzbekistan */
    WLAN_COUNTRY_VANUATU                                         = MK_CNTRY( 'V', 'U' ),             /* VU Vanuatu */
    WLAN_COUNTRY_VENEZUELA                                       = MK_CNTRY( 'V', 'E' ),             /* VE Venezuela */
    WLAN_COUNTRY_VIET_NAM                                        = MK_CNTRY( 'V', 'N' ),             /* VN Viet_Nam */
    WLAN_COUNTRY_VIRGIN_ISLANDS_BRITISH                          = MK_CNTRY( 'V', 'G' ),             /* VG Virgin_Islands,_British */
    WLAN_COUNTRY_VIRGIN_ISLANDS_US                               = MK_CNTRY( 'V', 'I' ),             /* VI Virgin_Islands,_U.S. */
    WLAN_COUNTRY_WALLIS_AND_FUTUNA                               = MK_CNTRY( 'W', 'F' ),             /* WF Wallis_and_Futuna */
    WLAN_COUNTRY_WESTERN_SAHARA                                  = MK_CNTRY( 'E', 'H' ),             /* EH Western_Sahara */
    WLAN_COUNTRY_YEMEN                                           = MK_CNTRY( 'Y', 'E' ),             /* YE Yemen */
    WLAN_COUNTRY_ZAMBIA                                          = MK_CNTRY( 'Z', 'M' ),             /* ZM Zambia */
    WLAN_COUNTRY_ZIMBABWE                                        = MK_CNTRY( 'Z', 'W' ),             /* ZW Zimbabwe */
} wlan_country_code_t;
#endif

/**
 * @brief set wifi country code
 *
 * @param region_code the region code to set
 *
 * @param regrev the regulatory revision number
 *
 * @return 0: ok, !=0: error
 *
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_set_countrycode(wlan_country_code_t country_code, int regrev);
#endif

/**
 * @brief set wifi region
 *
 * @param region_code the region code to set
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_region(u16 region_code);

/**
 * @brief get wifi region
 *
 * @param region_code the region code to get
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_get_region(u16 *region_code);
/**
 * @brief scan current net (active scan)
 *
 * @param pUserScanIn pointer to scan configuration parameters
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_scan_networks(const void* pUserScanIn,int priv_index);

/**
 * @brief scan current net
 *
 * @param pUserScanIn pointer to scan configuration parameters
 *
 * @param priv_index
 *
 * @param passive 0:active scan, !=0: passive scan
 *
 * @param channel channel to be scanned. 0: all available channel
 *
 * @return 0: ok, !=0: error
 *
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_scan_networks_ex(const void* pUserScanIn,int priv_index, int passive, u16 channel);
#endif

//scan ap list. this api is only for w8782
int wlan_scan_networks_list(ap_info_t* ap_list,int *ap_list_cnt,int ap_list_max, int priv_index);

/**
 * @brief set WEP key
 *
 * @param key WEP key string
 * @param len the length of wep key
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_encode_nonwpa(char *key, int length, char * ssid_name, int encrypt_type, int priv_index);

int wlan_wpa_pmk_phrase(char *ssid_name, char *ssid_key, unsigned char* pskpmk);
int wlan_set_encode_nonwpa_pmk(char *key,unsigned char *ssid_pmkkey,char * ssid_name, int encrypt_type, int priv_index);

/**
 * @brief get mac address
 *
 * @param mac_addr mac adress to get
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_get_mac(u8 *mac_addr);

/**
 * @brief set mac address, it must be called before wifi is started.
 *
 * @param mac_addr mac adress to set
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_mac(u8 *mac_addr);


/**
 * @brief set sta's mac address, it must be called before sta is connected to ap.
 *
 * @param mac_addr mac adress to set
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_stamac(u8 *mac_addr);

/**
 * @brief find the appointed ssid in ssid list
 *
 * @param ssid the appointed ssid
 *
 * @return the ssid index in ssid list
 *
 */
int wlan_find_essid(u8 *ssid,int priv_index);

/**
 * @brief get the wifi security requirement
 *
 * @param ssid_index the appointed ssid index
 *
 * @return 0: appointed ssid does not require security,
 * @return 1: appointed ssid requires WEP64
 * @return 2: appointed ssid requires WEP128
 * @return 3: appointed ssid requires WEP
 * @return 4: appointed ssid requires WEP2
 *
 */
int wlan_get_privacy(char * ssid_name,int encrypt_type,int priv_index);

/**
 * @brief get the wifi encryption requirement
 *
 * @param ssid_index the appointed ssid index
 *
 * @return 0: appointed ssid not encryption,
 * @return 1: appointed ssid requires AES encryption
 * @return 2: appointed ssid requires TKIP encryption
 *
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_get_encryption(char *ssid_name, int priv_index);
#endif

/**
 * @brief set the message handle callback function
 *
 * @param msg_hdl_cb the message handle callback function
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_set_msg_hdl_cb(int (*msg_hdl_cb)(int, void*));

/**
 * @brief get the message handle callback function
 *
 * @return the message handle callback function
 *
 */
void* wlan_get_msg_hdl_cb(void);

/**
 * @brief set the read callback function
 *
 * @param network_read_cb the callback function
 *
 * @return 0: ok, !=0: error
 *
 */
int wlan_network_device_set_read_cb(int (*network_read_cb)(u8*, int,int));

/**
 * @brief get the read callback function
 *
 * @return the read callback function
 *
 */
void* wlan_network_device_get_read_cb(void);

/**
 * @brief if wlan send buf is full then return 1
 *
 *
 * @return 0: not full , 1: full
 *
 */
int wlan_sendbuf_isfull(void);

/**
 * @brief clean wlan send buf full flag
 *
 *
 * @return void
 *
 */
void wlan_sendbuf_clean(void);

/**
 * @brief get priv role
 *
 *
 * @return priv role
 *
 */
mlan_bss_role wlan_get_role(int priv_index);

/**
 * @brief set priv role
 *
 *
 * @return void
 *
 */
void wlan_set_role(int priv_index, mlan_bss_role role);

/**
 * @brief get priv number
 *
 *
 * @return priv role
 *
 */
int wlan_get_priv_num(void);


/**
 * @brief MFG Calibration
 * send host cmd to firmware
 *
 *
 * @return void
 *
 */
void wlan_function_test(int forever);

/**
 * @brief MFG Calibration
 * libtool uart interface
 *
 *
 *
 * @return the uart char, if the uart char is belong to mfg msg then reutn 0;
 *
 */
u8 wlan_mfg_uart_handle(void);

/**
 * @brief set wifi module to mfg mode,
 * it must be called before wifi_init();
 *
 *
 *
 * @return void
 *
 */
void wlan_set_mfgmode(void);

/**
 * @brief disconnect wifi net
 *
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_disconnect_net(void);

#ifdef CONFIG_WIFIMODULE_W8782
#ifdef CONFIG_APP_STATION
int wlan_active_hs(void);
int wlan_set_sysclock(int clock);
int wlan_ioctl_deauth(void);
int wlan_ioctl_deepsleep(int flag);
#endif //CONFIG_APP_STATION

#ifdef CONFIG_APP_AP
/**
 * @brief wlan_ed_mac_ctrl
 *
 * @param enable [in] 	0x0:disable
 *						0x1:enable
 * 						0xFFFF:use default or value
 * @param offset	  [in]	0x0:no offset
 * 						0x1:offset to 1
 * 						0xFFFF:offset to -1
 * @return 0: ok, !=0: error
 */
int wlan_ed_mac_ctrl(u32 enable,u32 offset);

/**
 * @brief set ap tx rates
 *    max rates[14]
 *
 *   example:
 	when encrypt type is not open or wpa2.
 	rates[14];
	memset(rates,0,sizeof(rates));
      rates[0]=0x02;
 *
 *  uaputl.conf
     Rate=0x82,0x84,0x8b,0x96,0x0C,0x12,0x18,0x24,0x30,0x48,0x60,0x6c
                                    # Set of data rate that a station
                                    # in the BSS may use
                                    # (in unit of 500 kilobit/s)
 * @return 0: ok, !=0: error
 */
int wlan_set_current_txrates(u8* rates,u8 size);

#endif

/**
 * @brief enable hibernate mode, must reconnect after exit
 *
 * @param flag 0:into hibernate 1:exit hibernate
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_enable_hibernate(int flag);

/**
 * @brief check hibernate mode
 *
 * @return 0 : out hibernate mode
 * @return 1 : in hibernate mode
 */
int wlan_is_hibernate(void);

#if 1
/** This enum defines various wakeup events
 * for which wakeup will occur */
enum wifi_wakeup_event_t {
    /** Wakeup on ARP broad cast  */
    WIFI_WAKE_ON_ALL_BROADCAST = 1,
    /** Wakeup on uniccast  */
    WIFI_WAKE_ON_UNICAST = 1<<1,
    /** Wakeup on MAC event  */
    WIFI_WAKE_ON_MAC_EVENT = 1<<2,
    /** Wakeup on multicast  */
    WIFI_WAKE_ON_MULTICAST = 1<<3,
    /** Wakeup on  broad cast  */
    WIFI_WAKE_ON_ARP_BROADCAST = 1 << 4,
    /** Wakeup on receiving a management frame  */
    WIFI_WAKE_ON_MGMT_FRAME = 1<<6,
};
/**
 * @brief enable sleep mode, sleep auto and wakeup by wifi-events above
 *
 * @return 0 :success
 * @return !=0:failed
 */
int wlan_enable_sleep(u8 enable);
int wlan_is_sleep(void);
#endif

#endif //CONFIG_WIFIMODULE_W8782

/**
 * @brief scan wps enable ap
 *
 *
 *
 * ssid record find ap ssid.
 * @return 0 :success
 * @return -1:failed
 */
int wlan_wps_scan_networks(char *ssid, wlan_scan_filter_fun scan_filter, void *ctx);

/**
 * @brief associate wps enable ap
 *
 *
 *
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_wps_associate_networks(char *ssid);

/**
 * @brief associate wps enable ap with extra control
 *
 * @param ssid ssid to set
 *
 * @param ctrl_flag control flag, Should be 0
 *
 * @param tmo timeout (in second), 0: never timeout, -1: default timeout (20 sec)
 *
 * @return 0 :success
 * @return -1:failed
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_wps_associate_networks_ex(char *ssid, unsigned int ctrl_flag, int tmo);
#endif
/**
 * @brief disassociate wps enable ap
 *
 *
 *
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_wps_disassociate_networks(char *ssid);


int wlan_get_chip_ver(char *verbuf, int buflen);

int wlan_get_drv_ver(char *verbuf, int buflen);

int wlan_get_firmware_ver(char *verbuf, int buflen);

/**
 * @brief get current mcs index
 *
 * @param mcsidx mcs index output, -1 means the current rate is not a 11n rate.
 *
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_get_mcsidx( int *mcsidx);

/**
 * @brief get current wifi data rate
 *
 * @param rate data rate output. the granulation is 500kbps.
 *
 *
 * @return 0 :success
 * @return -1:failed
 */
int wlan_get_rate( int *rate);

int wlan_get_dtimskip(int *skip);

int wlan_set_dtimskip(int skip);

int wlan_get_rssi(int *rssi);

int wlan_get_SignalStrengthInd(int *SignalStrengthInd);

int wlan_apcmd_ssid_broadcast_enable(int priv_index,int enable, int set);

int wlan_apcmd_sys_cfg_tx_power(int priv_index,u8 *power, int set);

int wlan_apcmd_sys_info(void);

int wlan_apcmd_hostcmd(int priv_index,char *config, char* cmdname);

int wlan_set_uap_cfg(char *config);

typedef struct _wlan_cnt{
	unsigned int txframe;
	unsigned int txbyte;
	unsigned int txretrans;
	unsigned int txretry;	/*  dot11RetryCount  */
	unsigned int rxframe;
	unsigned int rxbyte;
	unsigned int rxerror;
	unsigned int txfail;	/*  dot11FailedCount  */
}wlan_cnt_t;
int wlan_get_counters( wlan_cnt_t *cnt);

/**
 * @brief get counters
 *
 * @param buf buffer for counters string
 *
 * @param buflen length of buf, minimum length is 2880.
 *
 *
 * @return >=0 :success, length of counters string
 * @return -1:failed
 */
#ifdef CONFIG_WIFIMODULE_BRCM4334X
int wlan_get_counters_string(char *buf, int buflen);
#endif

//read/write 4334 internal IO variables.
//such as "pm_dur", "mpc_dur"
int wlan_iovar_getint( char* name, int *val);
int wlan_iovar_setint( char* name, int val);

int wlan_para_restore2(u8 * buf,int check_version);
int wlan_para_restore(u8 *buf);
int wlan_para_save(u8 *buf);
void wlan_para_getbuf(u8 **buf, u32 *len);


//return 1 : means is run before
//return 0 : means is not run before.
int wlan_run_flag(void);
//return 0 : means not connected
//retrun 1 : means is connected
int wlan_is_connected(void);
/** @} */ //end lib_wifi

//To set/get wifi control parameter "MaxMACRetryCount".
/**
 * @brief Get short retry limit
 *
 * @param srl Returned limit value
 *
 * @return 0: ok
 *
 */
int wlan_get_short_retry_limit(int *srl);

/**
 * @brief Set short retry limit
 *
 * @param srl new limit value
 *
 * @return 0: ok
 *
 */
 int wlan_set_short_retry_limit(int srl);

#ifdef CONFIG_WIFIMODULE_BRCM4334X
/**
 * @brief Get long retry limit
 *
 * @param lrl Returned limit value
 *
 * @return 0: ok
 *
 */
int wlan_get_long_retry_limit(int *lrl);
/**
 * @brief Set long retry limit
 *
 * @param lrl New retry limit
 *
 * @return 0: ok
 *
 */
int wlan_set_long_retry_limit(int lrl);
#endif
/**
 * @brief Get/Set antenna diversity for rx
 *
 * @param antdiv New antenna diversity. (0/1/3: force ant0/force ant1/auto select)
 *
 * @return 0: ok
 *
 */
int wlan_get_ant_diversity(int *antdiv);
int wlan_set_ant_diversity(int antdiv);

/**
 * @brief Get/Set the transmit antenna
 *
 * @param txant New transmit antenna. (0/1/3: force ant0/force ant1/use the RX antenna selection that was in force during)
 *
 * @return 0: ok
 *
 */
int wlan_get_tx_ant(int *txant);
int wlan_set_tx_ant(int txant);

// call wlan_down shut down 4334.  Call dhd_wl_up is required to restore 4334 to working state.
int wlan_up(void);
int wlan_down(void);

//set/get power mode, PM_NONE=0 (no power saving), PM_MAX=1 (max power saving) and PM_FAST=2 (power saving with good throughput).
int wlan_getpm(int *pm);
int wlan_setpm(int pm);

/**
 * @brief set suspend
 *
 * @param val,0: disable,1: enable
 *
 * @param force,  force suspend
 *
 * @return 0: ok
 *
 */
int wlan_setsuspend(int val, int force);

//return wlan sdio irq status, =1 has sdio interrupt, =0 no sdio interrupt.
int wlan_irq_status(void);

// set keepaliveperiod, msec >= 0 , if msec = 0 it means close keep alive function.
int wlan_set_keepaliveperiod(int msec);
// get keepaliveperiod, msec >= 0
int wlan_get_keepaliveperiod(int *msec);

/**
 * @brief Set tcp heartbeat
 *
 * @param intervaltime heart beat interval time(s). =0 will disable tcp hearbeat.
 * @resendtime  heart beat resent time(s).
 * @pkt			tcp package (ethernet package)
 * @pktlen 		tcp package len. tcp max payload = 1000
 *
 * @return 0: ok
 *		   !=0: failed.
 */
int wlan_set_tcpkeepalive(u16 intervaltime, u16 resendtime, char *pkt, int pktlen);

#ifdef CONFIG_WIFIMODULE_BRCM4334X
/**
 * @brief Set Keepalive packet
 *
 * @param msec keepalive packet period, if msec=0 it means disable keepalive
 *
 * @param pkt keepalive packet, should be a full ethernet frame.
 *
 * @param pktlen keepalive packet
 *
 * @return 0: ok
 *
 */
int wlan_set_keepalive_pkt(int msec, char *pkt, int pktlen);
/**
 * @brief Set second Keepalive packet
 *
 * @param msec keepalive packet period, if msec=0 it means disable keepalive
 *
 * @param pkt keepalive packet, should be a full ethernet frame.
 *
 * @param pktlen keepalive packet
 *
 * @return 0: ok
 *
 */
int wlan_set_keepalive_pkt2(int msec, char *pkt, int pktlen);
/**
 * @brief Set third Keepalive packet
 *
 * @param msec keepalive packet period, if msec=0 it means disable keepalive
 *
 * @param pkt keepalive packet, should be a full ethernet frame.
 *
 * @param pktlen keepalive packet
 *
 * @return 0: ok
 *
 */
int wlan_set_keepalive_pkt3(int msec, char *pkt, int pktlen);
/**
 * @brief Set Keepalive packet
 *
 * @param index the index of keepalive pkt.
 *
 * @param msec keepalive packet period, if msec=0 it means disable keepalive
 *
 * @param pkt keepalive packet, should be a full ethernet frame.
 *
 * @param pktlen keepalive packet
 *
 * @return 0: ok
 *
 */
int wlan_set_keepalive_pkt_index(int index, int msec, char *pkt, int pktlen);
/**
 * @brief get Keepalive packet period
 *
 * @param index the index of keepalive pkt.
 *
 * @param msec keepalive packet period.
 *
 */
int wlan_get_keepaliveperiod_index(int index, int *msec);
#endif

/*  Get/Set current wifi channel  */
int wlan_get_current_channel(int *channel);
/*  set ap channel, it will wl_down and then wl_up. */
int wlan_set_current_channel(int channel);
/*  switch current ap channel, it only set ssid again
 *  if channel set 0, it will auto select a channel*/
int wlan_switch_channel(int channel);
/**
 * @brief Wait wifi adapter enter idle state
 *
 * @param timeout_ms Maximum wait time (0: default, minimum value is 50ms)
 *
 * @return 0: ok
 * @return -1: timeout
 * @return -16: A new packet received
 *
 */
int wlan_wait_idle(int timeout_ms);

//get the buffer fill percentage of the Broadcom��s Wi-Fi chipset
int wlan_get_txbuf_level(u32 *percentage);

#ifdef CONFIG_WIFIMODULE_BRCM4334X
/**
 * @brief Set ampdu_rr_retry_limit_tid
 *
 * @param tid Traffic id [0-7]
 *
 * @param limit Retry limit. The available range is determined by wifi chip firmware.
 *
 * @return 0: ok
 *
 */
int wlan_set_ampdu_rr_retry_limit_tid(int tid, int limit);


/**
 * @brief Get ampdu_rr_retry_limit_tid
 *
 * @param tid Traffic id [0-7]
 *
 * @param limit Returned retry limit
 *
 * @return 0: ok
 *
 */
int wlan_get_ampdu_rr_retry_limit_tid(int tid, int *limit);

/**
 * @brief Set ampdu_retry_limit_tid
 *
 * @param tid Traffic id [0-7]
 *
 * @param limit Retry limit. The available range is determined by wifi chip firmware.
 *
 * @return 0: ok
 *
 */
int wlan_set_ampdu_retry_limit_tid(int tid, int limit);


/**
 * @brief Get ampdu_retry_limit_tid
 *
 * @param tid Traffic id [0-7]
 *
 * @param limit Returned retry limit
 *
 * @return 0: ok
 *
 */
int wlan_get_ampdu_retry_limit_tid(int tid, int *limit);


/*  Interference mitigation options  */
#define INTERFERE_OVRRIDE_OFF		-1
#define INTERFERE_NONE				 0
#define NON_WLAN					 1
#define WLAN_MANUAL					 2
#define WLAN_AUTO					 3
#define WLAN_AUTO_W_NOISE			 4
#define AUTO_ACTIVE			   (1 << 7)

/**
 * @brief Get current interference mode
 *
 * @param interference interference mode. If bit7 is set, then Auto is currently active
 *
 * @return 0: ok
 *
 */
int wlan_get_interference_mode(int *interference);

/**
 * @brief Set interference mode
 *
 * @param interference interference mitigation option
 *
 * @return 0: ok
 *
 */
int wlan_set_interference_mode(int interference);

#endif

/*
 * Setup pktfilter, enable when host hibernate.
 * Note that current pktfilter we design it as "whitelist" style!! (pkt_filter_mode=1)
 *
 * Input format: <id> <polarity> <type> <offset> <bitmask> <pattern>
 * <id> User filter entry id must starts from 100, to distinguish from default filter
 * <polarity> Should be 0
 * <type> Should be 0
 * <offset> The offset of bitmask in Ethernet packet.
 *
 * sample:
 * 1.Filter to allow all Unicast
 * "105 0 0 0 0x01 0x00"
 * 2.Filter to allow IPv4 ARP packet to host IP, not filter out as Broadcast
 * "106 0 0 12 0xFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000FFFFFFFF 0x0806000108000604000100000000000000000000000000000000c0a80102"
 */
/**
 * @brief Set custom single entry for additional packet filter, with filter enable/disable
 *
 * @param filter the filter which will be added and enable/disable.
 * @param enable 0: Disable, 1: Enable
 *
 * @return 0: ok
 */
int wlan_set_user_pkt_filter_entry(char *filter, int enable);

/**
 * @brief Set packet filter group, default add a filter for unicast packet.
 *
 * @param ip point to local host ip address, if ip is NULL, it will not add a filter for any ARP packet.
 * 		     if ip is not NULL, it will add a filter for ARP query packet for this ip address.
 *
 * @return 0: ok
 *
 */
int wlan_set_pkt_filter(u8 const *ip);
/**
 * @brief Set packet filter group, default add a filter for unicast packet.
 *
 * @param srcip : pointer to src ip address. add src ip limit to packet filter.
 * 				=NULL, the wakeup filter ignore this option.
 * @param dstip : pointer to dst ip address. add dst ip limit to packet filter.
 * 				=NULL, the wakeup filter ignore this option.
 * @param srcport : src port, add src port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param dstport : dst port, add dst port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param tcpudp : protocol type, add protocol limit to packet filter,
 * 				=1 udp, =2 tcp, =3 icmp, =0 will ignore this option.
 * @param arpreply: flag for arp request wake up filter.
 * 				=1, enable this filter. = 0 disable this filter.
 *
 * @return 0: ok
 *
 */
int wlan_set_pkt_filter2(u8 const * srcip,u8 const * dstip,u16 srcport,u16 dstport,u8 tcpudp,u8 arpreply);
/**
 * @brief Set packet filter enable/disable
 *
 * @param enable 0: Disable, 1: Enable
 *
 * @return 0: ok
 *
 */
int wlan_set_pkt_filter_enable(int enable);

#define	WLAN_ETHER_ADDR_LEN		6
#define WLAN_MAX_ASSOC_CLIENT	5
#define WLAN_MCSSET_LEN			16	/* 16-bits per 8-bit set to give 128-bits bitmap of MCS Index */
#define	WLAN_MAXRATES_IN_SET	16	/* max # of rates in a rateset */
#define WLAN_STA_ANT_MAX		4	/* max possible rx antennas */

typedef struct wlan_ether_addr_s {
	u8 octet[WLAN_ETHER_ADDR_LEN];
} wlan_ether_addr_t;

/*
 * Associated list structure
 */
typedef struct wlan_maclist_s {
	u32 count;										/* number of MAC addresses */
	wlan_ether_addr_t ea[WLAN_MAX_ASSOC_CLIENT];	/* array of MAC addresses */
} wlan_maclist_t;

/*
 * Rate set structure
 */
typedef struct wlan_rateset_s {
	u32	count;							/* # rates in this set */
	u8	rates[WLAN_MAXRATES_IN_SET];	/* rates in 500kbps units w/hi bit set if basic */
} wlan_rateset_t;

/*
 * BSS info structure
 * Applications MUST CHECK ie_offset field and length field to access IEs and
 * next bss_info structure in a vector
 * (Note: Sync with wl_bss_info_t !!!)
 */
typedef struct wlan_bss_info_s {
	u32		version;			/* version field */
	u32		length;				/* byte length of data in this record,
						 		 * starting at version and including IEs
						 		 */
	wlan_ether_addr_t BSSID;
	u16		beacon_period;		/* units are Kusec */
	u16		capability;			/* Capability information */
	u8		SSID_len;
	u8		SSID[32];

	struct {
		u32	count;				/* # rates in this set */
		u8	rates[16];			/* rates in 500kbps units w/hi bit set if basic */
	} rateset;					/* supported rates */

	u16		chanspec;			/* chanspec for bss */
	u16		atim_window;		/* units are Kusec */
	u8		dtim_period;		/* DTIM period */
	s16		RSSI;				/* receive signal strength (in dBm) */
	s8		phy_noise;			/* noise (in dBm) */

	u8		n_cap;				/* BSS is 802.11N Capable */
	u32		nbss_cap;			/* 802.11N+AC BSS Capabilities */
	u8		ctl_ch;				/* 802.11N BSS control channel number */
	u8		padding1[3];		/* explicit struct alignment padding */
	u16		vht_rxmcsmap;		/* VHT rx mcs map (802.11ac VHT_CAP_MCS_MAP_*) */
	u16		vht_txmcsmap;		/* VHT tx mcs map (802.11ac VHT_CAP_MCS_MAP_*) */
	u8		flags;				/* flags */
	u8		vht_cap;			/* BSS is vht capable */
	u8		reserved[2];		/* Reserved for expansion of BSS properties */
	u8		basic_mcs[WLAN_MCSSET_LEN];	/* 802.11N BSS required MCS set */

	u16		ie_offset;			/* offset at which IEs start, from beginning */
	u32		ie_length;			/* byte length of Information Elements */
	s16		SNR;				/* average SNR of during frame reception */
} wlan_bss_info_t;

/*
 * STA info structure
 * Pay attention to version if structure changes
 * (Note: Sync with sta_info_t !!!)
 */
typedef struct wlan_sta_info_s {
	u16			ver;						/* version of this struct */
	u16			len;						/* length in bytes of this structure */
	u16			cap;						/* sta's advertised capabilities */
	u32			flags;						/* flags defined below */
	u32			idle;						/* time since data pkt rx'd from sta */
	wlan_ether_addr_t	ea;					/* Station address */
	wlan_rateset_t		rateset;			/* rateset in use */
	u32			in;							/* seconds elapsed since associated */
	u32			listen_interval_inms;		/* Min Listen interval in ms for this STA */
	u32			tx_pkts;					/* # of user packets transmitted (unicast) */
	u32			tx_failures;				/* # of user packets failed */
	u32			rx_ucast_pkts;				/* # of unicast packets received */
	u32			rx_mcast_pkts;				/* # of multicast packets received */
	u32			tx_rate;					/* Rate used by last tx frame */
	u32			rx_rate;					/* Rate of last successful rx frame */
	u32			rx_decrypt_succeeds;		/* # of packet decrypted successfully */
	u32			rx_decrypt_failures;		/* # of packet decrypted unsuccessfully */
	u32			tx_tot_pkts;				/* # of user tx pkts (ucast + mcast) */
	u32			rx_tot_pkts;				/* # of data packets recvd (uni + mcast) */
	u32			tx_mcast_pkts;				/* # of mcast pkts txed */
	u32			reserved_bytes_counters[12];/* reserved byte counters */
	s8			rssi[WLAN_STA_ANT_MAX];		/* average rssi per antenna
							 			 	 * of data frames
							 			 	 */
	s8			nf[WLAN_STA_ANT_MAX];		/* per antenna noise floor */
	u16			aid;						/* association ID */
	u16			ht_capabilities;			/* advertised ht caps */
	u16			vht_flags;					/* converted vht flags */
	u32			tx_pkts_retried;			/* # of frames where a retry was
							 		 		 * necessary (obsolete)
							 		 		 */
	u32			tx_pkts_retry_exhausted; 	/* # of user frames where a retry
							  			     * was exhausted
							  			     */
	s8			rx_lastpkt_rssi[WLAN_STA_ANT_MAX];	/* Per antenna RSSI of last
								  				     * received data frame.
								  				     */
	/* TX WLAN retry/failure statistics:
	 * Separated for host requested frames and WLAN locally generated frames.
	 * Include unicast frame only where the retries/failures can be counted.
	 */
	u32			tx_pkts_total;				/* # user frames sent successfully */
	u32			tx_pkts_retries;			/* # user frames retries */
	u32			tx_pkts_fw_total;			/* # FW generated sent successfully */
	u32			tx_pkts_fw_retries;			/* # retries for FW generated frames */
	u32			tx_pkts_fw_retry_exhausted;	/* # FW generated where a retry
								 		     * was exhausted
								 			 */
	u32			rx_pkts_retried;			/* # rx with retry bit set */
	u32			tx_rate_fallback;			/* lowest fallback TX rate */
} wlan_sta_info_t;

/**
 * @brief Get the list of associated MAC addresses
 *
 * @param assoclist associated list to get
 *
 * @return 0: ok
 *
 */
int wlan_get_assoclist(wlan_maclist_t *assoclist);
/**
 * @brief add or del custom verdor IE
 *
 * @param oui 	   vendor specific oui string, the format of oui string is like  "00:A0:40"
 *
 * @param subdata  vendor specific sub string, is include Sub-type and Elements.
 * 					the format of subdata is "040601"
 *
 * @return 0: ok
 *
 */
int wlan_add_ap_custom_vendorIE(char *oui, char *subdata);
int wlan_del_ap_custom_vendorIE(char *oui, char *subdata);
/**
 * @brief Get the information about current network association
 *
 * @param bss_info bss status info to get
 *
 * @return 0: ok
 *
 */
int wlan_get_status(wlan_bss_info_t *bss_info);
/**
 * @brief Get the information about current network association
 *
 * @param bss_info bss status info to get
 * @param len 	bss_info's buf total len, len must >= sizeof(wlan_bss_info_t)
 *
 * @return 0: ok
 *
 */
int wlan_get_apstatus(wlan_bss_info_t *bss_info,int len);
/**
 * @brief Get the information about connected station
 *
 * @param sta_info station info to get
 *
 * @param sta_mac input station mac address (should be in AP's assoclist)
 *
 * @return 0: ok
 *
 */
int wlan_get_sta_info(wlan_sta_info_t *sta_info, wlan_ether_addr_t *sta_mac);

/**
 * @brief Get the RSSI of the connected station
 *
 * @param sta_rssi RSSI value to get
 *
 * @param sta_mac input station mac address (should be in AP's assoclist)
 *
 * @return 0: ok
 *
 */
int wlan_get_sta_rssi(int *sta_rssi, wlan_ether_addr_t *sta_mac);

/**
 * @brief Check if current FW config as AP mode
 *
 * @param void
 *
 * @return 0: STA mode; 1: AP mode
 *
 */
int wlan_is_ap_mode(void);

/*
 * @brief Stop AP mode
 *
 * @param void
 *
 * @return =0 success ; !=0 failed.
 *
 */
int wlan_stop_ap(void);
/**
 * @brief Set wifi to arp auto reply mode.
 *
 * @param ipaddr : pointer to uip_ipaddr_t parameter.
 * 				=NULL, disable arp auto reply mode.
 * 				!=NULL, enable arp auto reply mode.
 *
 */
void wlan_set_arpoe(u8 const *ipaddr);
/**
 * @brief Set wifi to sleep mode, and enable a wakeup packet filter.
 *
 * @param ipaddr : pointer to uip_ipaddr_t parameter.
 * 				=NULL, the wakeup filter only support unicast packet.
 * 				!=NULL, the wakeup filter add arp query packet for the setting ip address.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default 10.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep(u8 const *ipaddr, int dtimskip);
/**
 * @brief Set wifi to sleep mode, and enable a unicast ip packet filter.
 *
 * @param ipaddr : pointer to uip_ipaddr_t parameter.
 * 				=NULL, disable autoarp mode.
 * 				!=NULL, enable autoarp mode of this ip.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default value.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep_autoarp(u8 const *ipaddr, int dtimskip);
/**
 * @brief Set wifi to sleep mode, and enable a wakeup packet filter.
 *
 * @param srcip : pointer to src ip address. add src ip limit to packet filter.
 * 				=NULL, the wakeup filter ignore this option.
 * @param dstip : pointer to dst ip address. add dst ip limit to packet filter.
 * 				=NULL, the wakeup filter ignore this option.
 * @param srcport : src port, add src port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param dstport : dst port, add dst port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param tcpudp : protocol type, add protocol limit to packet filter,
 * 				=1 udp, =2 tcp, =3 icmp, =0 will ignore this option.
 * @param arpreply: flag for arp request wake up filter.
 * 				=1, enable this filter. = 0 disable this filter.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default 10.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep2(u8 const * srcip,u8 const * dstip,u16 srcport,u16 dstport,u8 tcpudp, u8 arpreply, int dtimskip);
/**
 * @brief Set wifi to sleep mode, and enable a wakeup packet filter.
 *
 * @param srcip : pointer to src ip address. add src ip limit to packet filter.
 * 				=NULL, the wakeup filter ignore this option.
 * @param dstip : pointer to dst ip address. add dst ip limit to packet filter, and set autoarp mode of this ip.
 * 				=NULL, the wakeup filter ignore this option, and will not set autoarp mode.
 * @param srcport : src port, add src port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param dstport : dst port, add dst port limit to packet filter.
 * 				=0, the wakeup filter ignore this option.
 * @param tcpudp : protocol type, add protocol limit to packet filter,
 * 				=1 udp, =2 tcp, =3 icmp, =0 will ignore this option.
 * @param arpreply: flag for arp request wake up filter.
 * 				=1, enable this filter. = 0 disable this filter.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default value.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep2_autoarp(u8 const * srcip,u8 const * dstip,u16 srcport,u16 dstport,u8 tcpudp, u8 arpreply, int dtimskip);

/**
 * @brief just only set wifi to sleep mode, didn't enable any filter.
 *
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default 10.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep_nofilter(int dtimskip);
/**
 * @brief just only set wifi to sleep mode, didn't enable any filter.
 *
 * @param ipaddr: point to ipaddr. =NULL, will not set autoarp mode. otherwise will set autoarp mode of
 * 					this ipaddr.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default value.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
void wlan_set_sleep_nofilter_autoarp(u8 const * ipaddr, int dtimskip);
/**
 * @brief Set mask for WiFi events from chip when host is in Suspend/Resume condition.
 *
 * @param void
 *
 * @return =0 success ; !=0 failed.
 *
 */
int wlan_set_eventmsg_when_suspend(void);
int wlan_set_eventmsg_when_resume(void);


#ifdef CONFIG_WIFIMODULE_W8782
/**
 * @brief reset the wifi moudle by command
 *
 * @return 0: ok
 *
 */
//int wlan_soft_reset(void);
#endif

/**
 * @brief WLAN test command process (for DEBUG only !!)
 *
 * @param op1/op2/op3/op4/op5/op6 cmd argv
 *
 * @return void
 *
 */
void wlan_cmd_proc(char *op1, char *op2, char *op3, char *op4, char *op5, char *op6);

//connect to wps router
/**
 * @brief wps station handle.
 *
 * @param ssid 		[out] 	return ap's ssid
 * @param key 		[out] 	return ap's password
 * @param keylen	[out]	return ap's password length.
 * @return 0: ok, !=0: error
 *
 */
int wlan_connnect_wps(char *ssid, char *key, unsigned char *keylen);
/**
 * @brief wps station no block handle.
 *
 * @param ssid 		[out] 	return ap's ssid
 * @param key 		[out] 	return ap's password
 * @param keylen	[out]	return ap's password length.
 * @stop			[int]	=0 resume wps process; !=0 stop wps process;
 * @return 0: ok, >0 resume,  <0: error
 *
 */
int wlan_connnect_wps_noblock(char *ssid, char *key, unsigned char *keylen, int stop);
//server for wps client
int wlan_router_wps(char *ssid, char *key, unsigned char *keylen);

void sleepinit(void);
void msleep(int msec);
/**
 * @brief return the remaining data count in the BCM4334’s pool buffer.
 *
 * @return >=0 the correct value,  <0: error
 *
 */
int wlan_get_pool(void);
/**
 * @brief set BRCM4334W's HT40 function.
 * @param enable	[in]	=0 disable HT40, !=0 enable HT40
 * @return =0 success,  <0: error
 *
 */
int wlan_set_brcm4334w_HT40(int enable);
/**
 * @brief get BRCM4334W's HT40 status.
 * @ enable			[out]  =0 HT40 is disabled, !=0 HT40 is enabled.
 * @return =0 success,  <0: error
 *
 */
int wlan_get_brcm4334w_HT40(int *enable);
/**
 * @brief according ie info get wps status.
 * @ pie		[in]  ie buf.
 * @ ie_length	[in]  ie length.
 *
 * @return =-1 wps no support,  =0: wps support, =1 wps-pbc mode, = 2 wps pin mode
 *
 */
int check_wps(u8 *pie, u32 ie_length);

//set/get rtsthresh value, the default value is 2347.
int wlan_getrtsthresh(int *rts_val);
int wlan_setrtsthresh(int rts_val);

//set/get ioctl cmd, max buf len =1050
int dhd_wl_iovar_setbuf(char *iovar, void *buf, int buflen);
int dhd_wl_iovar_getbuf(char *iovar, void *buf, int buflen);

#endif //__WIFI_BCM4334_H__
