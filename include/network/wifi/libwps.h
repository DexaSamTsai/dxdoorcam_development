/*******************************************************************************
 * libwps.h
 *
 * History:
 *   2013-8-13 - [WeiboHuang] created file
 *
 * Copyright (C) 2008-2019, SKY-LIGHT Tech. ShenZhen Co,Ltd
 *
 * All rights reserved. No Part of this file may be reproduced, stored
 * in a retrieval system, or transmitted, in any form, or by any means,
 * electronic, mechanical, photocopying, recording, or otherwise,
 * without the prior consent of SKY-LIGHT Tech.
 *
 ******************************************************************************/

#ifndef __LIBWPS_H__
#define __LIBWPS_H__

int wlan_connnect_wps(char *ssid, char *key, unsigned char *keylen);

/*
 * Internal data structure for wpabuf. Please do not touch this directly from
 * elsewhere. This is only defined in header file to allow inline functions
 * from this file to access data.
 */
struct wpabuf {
	size_t size; /* total size of the allocated buffer */
	size_t used; /* length of data in the buffer */
	u8 *buf; /* pointer to the head of the buffer */
	unsigned int flags;
	/* optionally followed by the allocated buffer */
};

#define WPS_OVERALL_TIMEOUT			120

int wps_supplicant_init(u8 *mac, u8* bssid, u8* ssid, u8* key, u8* keylen,u8* pin);
int wps_supplicant_deinit(void);
void* wps_supplicant_process(struct wpabuf *revbuf);
int is_wps_supplicant_done(void);
int is_wps_pbc(u8 *pie, u32 ie_length);
void wpabuf_free(struct wpabuf *buf);
u32 eapol_send(struct wpabuf *buf, u8* outbuf);
struct wpabuf * eapol_recv(u8* buf, u32 len);
#endif
