#ifndef _LIBWIFI_API_H_
#define _LIBWIFI_API_H_
#include "wifi_bcm4334.h"
/* NOTE: this file is the only API application can call,
   all the APIs are thread-safe.
 */

/**************** main status control APIs ************
  app don't call these APIs directly in most cases, app should call network_xxx() instead .
  these APIs are only for network.c only.
  *****************************************************/
typedef struct s_wifi_conf
{
	int* broadcast;
	int* txpower;
	unsigned char* pmkkey;
	char* txpwrlimit_cfg;
	char* contry_code;
	char* vndr_oui; // vendor specific oui, the format of oui string is like  "00:A0:40"
	char* vndr_data; //vendor specific sub string, is include Sub-type and Elements.
				//	the format of subdata is "040601"
	u32 *ap_assoc_client;
	int *security; // =0 none; =1 wep; =3 wap-psk/wap2-psk
	int *wepauthtype; // =0 open system; =1 shared key
	int *fixedchannel; // if fixed channel !=NULL, station will only scan this channel.
	int *fixedheartbeat; // fixed heart beat index, is !=NULL, this index heartbeat will not be clean, after wakekup.
}t_wifi_conf;

typedef struct s_wifi_scan_result
{
	char ssid[32];
	char bssid[6];
	int rssi;
	int channel;
	int security;	// 0:open, 1:wep, 2:wpa, 3:invalid, 4:wpa2, 5:wpa-aes, 6:wpa2-tkip, 7:wps_open, 8: wps; -1: unknown.
	int bss_type;   // 0:infra, 1:adhoc, 2:any; -1:unkown.
	int wps;		// 0:yes, 1:pbc, 2:pin; -1: no
}t_wifi_scan_result;

typedef struct s_wifi_keepalive_pkt
{
	int id;
	char * buf;
	int len;
}t_wifi_keepalive_pkt;

typedef struct s_wifi_tcpkeepalive_pkt
{
	int resendtime;  // tcp heartbeat resendtime. unit ms.
	char * buf;
	int len;
}t_wifi_tcpkeepalive_pkt;

typedef struct s_wifi_nrate_set
{
	char *legacy_set_val;
	char *mcs_set_val;
	char *stf_set_val;
	int mcs_only;
	int override;
}t_wifi_nrate_set;


#define WIFI_SUCCESS	 	0
#define WIFI_FAILED		1

int libwifi_init(int sdio_id, u8 * mac, int mfg, t_wifi_conf * conf );

int libwifi_connect(char *ssid, char * key, u32 keylen, int *channel, int block, t_wifi_conf* conf);
int libwifi_scanconnect(char *ssid, int ssidlen, u8 *bssid, u8* key, u32 keylen, int keyindex, u8 *pmk,
		u16 *chanspec,wifi_scan_security_t *scan_security, wifi_scan_encryption_t *scan_encryption,
		int block, t_wifi_conf* conf);
int libwifi_ap(char *ssid, u8* key, u32 keylen,int encrypt, int security, int *channel, t_wifi_conf* conf);

int libwifi_reconnect(void);

int libwifi_stop(void);
int libwifi_down(void);
int libwifi_up(void);

/***************** apis for tcpip stack ************/

//called by wifi driver, called when input data ready
void libwifi_thread_input_notify(void);

//called by lwip, for output data in lowlevel
int libwifi_packet_write(u8* wr_buf, int len, int priv_index);
/*
 * "early packets" means the packets received during wlan sleep. 
 * early packets will be readed and bufferred after wlan is waked/inited. 
 * User should call libwifi_receive_early_packets() to receive them to lwip stack after your (listen)  sockets is ready.  
 */
int libwifi_receive_early_packets(void);

/***************** ioctl *****************/
#define WIOCGETMAC		0x00010001	///< get mac. arg0: u8 * macaddr
#define WIOCPARAGETBUF	0x00010002	///< para get buf. arg0: u8 ** buf, arg1: u32 * len
#define WIOCPARASAVE	0x00010003	///< para seve. arg0: u8 * buf
#define WIOCPARARESTORE	0x00010004	///< para restore. arg0: u8 * buf, default it will check driver version.
#define WIOCGETPOLLINT	0x00010005	///< get polling interval. ret: >0 polling interval, <=0 don't use polling.
#define WIOCSETSLEEP	0x00010006	///< enter sleep mode. arg0: int dtimskip.
#define WIOCGETRSSI		0x00010007	///< get rssi. arg0: int * rssi
#define WIOCIOVARGETINT	0x00010008	///< get iovar by int. arg0: char * name, arg1: int * val.
#define WIOCIOVARSETINT	0x00010009	///< set iovar by int. arg0: char * name, arg1: int val.
#define WIOCSCANRESULTS 0x0001000a  ///< wlan scan results. arg0: t_wifi_scan_results * results, arg1: int * cnt.
#define WIOCSETFILTERBYIP	0x0001000b	///< set wlan pkt filter by host ip. arg0: char * ip, arg1: int enable. 
#define WIOCSETFILTERBYUSER	0x0001000c	///< set wlan user defined pkt filter. arg0: char * filter, arg1: int enable.
#define WIOCKEEPALIVEPKT	0x0001000d	///< set wlan keepalive pkt. arg0: t_wifi_keepalive_pkt * pkt, arg1: u32 interval, unit ms.
#define WIOCGETCOUNTERS 0x0001000e  ///< get counters by string. arg0: char * buf, arg1: int len.
#define WIOCGETINTERFERENCE 0x0001000f  ///< get interference mode. arg0: int * val.
#define WIOCSETINTERFERENCE 0x00010010  ///< set interference mode. arg0: int val.
#define WIOCSCAN 0x00010011  ///< wlan scan results. arg0: ap_info_t * results, arg1: ap_info_num_t * cnt.
#define WIOCSETAUTOARPSLEEP 0x00010012 ///enter sleep mode. arg0: u8* local ip address, arg1: int dtimskip.
#define WIOCTCPKEEPALIVEPKT	0x00010013	///< set wlan tcp keepalive pkt. arg0: t_wifi_tcpkeepalive_pkt * pkt, arg1: u32 interval, unit ms.
#define WIOCTEST			0x00010014  ///for test.
#define WIOCSETPM			0x00010015	///set pm
#define WIOCGETPM			0x00010016  ///get pm
#define WIOCSETCOUNTRY		0x00010017	///set country
#define WIOCGETCOUNTRY		0x00010018  ///get country
#define WIOCSETNRATE		0x00010019	///set nrate
#define WIOCGETNRATE		0x00010020	///get nrate
#define WIOCADDVNDR			0x00010021  ///add customer vendor ie
#define WIOCDELVNDR			0x00010022	///del customer vendor ie
#define WIOCPARARESTORECKVER 0x00010023 ///para restore with check version. arg0: u8 * buf, arg1: u32 check version flag, =0 it will not check driver version.
#define WIOCIOCTLGETINT		0x00010024	///ioctl interface get int
#define WIOCIOCTLSETINT		0x00010025	///ioctl interface set int
#define WIOCGETAPSTATUS		0x00010026	///get the associcated ap's status.

int libwifi_ioctl(int ioc, void * arg0, void * arg1);

/** for API compatible only **/
#define libwifi_para_getbuf(buf, len)	libwifi_ioctl(WIOCPARAGETBUF, (void *)(buf), (void *)(len))

#define libwifi_para_save(buf) libwifi_ioctl(WIOCPARASAVE, (void *)(buf), NULL)

#define libwifi_para_restore(buf) libwifi_ioctl(WIOCPARARESTORE, (void *)(buf), NULL)

#define libwifi_para_restore2(buf, checkversion) libwifi_ioctl(WIOCPARARESTORECKVER, (void *)(buf), (void *)(checkversion))

#define libwifi_get_mac(macaddr) libwifi_ioctl(WIOCGETMAC, (void *)(macaddr), NULL)

#define libwifi_get_rssi(rssi) libwifi_ioctl(WIOCGETRSSI, (void *)(rssi), NULL)

#define libwifi_iovar_getint(name, val)		libwifi_ioctl(WIOCIOVARGETINT, (name), (void *)(val))
#define libwifi_iovar_setint(name, val)		libwifi_ioctl(WIOCIOVARSETINT, (name), (void *)(val))
#define libwifi_get_counters(buf, len)		libwifi_ioctl(WIOCGETCOUNTERS, buf, (void *)(len))
#define libwifi_get_interference(val)	libwifi_ioctl(WIOCGETINTERFERENCE, val, NULL)
#define libwifi_set_interference(val)	libwifi_ioctl(WIOCSETINTERFERENCE, val, NULL)
#define libwifi_set_pm(val)				libwifi_ioctl(WIOCSETPM, val, NULL)
#define libwifi_get_pm(val)				libwifi_ioctl(WIOCGETPM, val, NULL)
#define libwifi_set_country(val)		libwifi_ioctl(WIOCSETCOUNTRY,val, NULL)
#define libwifi_get_country(val)		libwifi_ioctl(WIOCGETCOUNTRY,val, NULL)
#define libwifi_set_nrate(val)			libwifi_ioctl(WIOCSETNRATE,val,NULL)
#define libwifi_get_nrate(val)			libwifi_ioctl(WIOCGETNRATE,val,NULL)
#define libwifi_add_custom_vendorIE(oui,subdata)	libwifi_ioctl(WIOCADDVNDR,oui,subdata)
#define libwifi_del_custom_vendorIE(oui,subdata)	libwifi_ioctl(WIOCDELVNDR,oui,subdata)
#define libwifi_ioctl_getint(id, val)	libwifi_ioctl(WIOCIOCTLGETINT,(id),(void *)(val))
#define libwifi_ioctl_setint(id, val)	libwifi_ioctl(WIOCIOCTLSETINT,(id),(void *)(val))
#define libwifi_test()	libwifi_ioctl(WIOCTEST, NULL, NULL)
#define libwifi_get_apstatus(buf, val)		libwifi_ioctl(WIOCGETAPSTATUS,buf, val)

//libwifi_scan_results(t_wifi_scan_results * results, int * cnt);
#define libwifi_scan_results(results, cnt)	libwifi_ioctl(WIOCSCANRESULTS, (results), (void *)(cnt))
#define libwifi_scan(results, cnt)	libwifi_ioctl(WIOCSCAN, (results), (void *)(cnt))

#define libwifi_keepalive_pkt(pkt, interval)		libwifi_ioctl(WIOCKEEPALIVEPKT, (pkt), (void *)(interval))

#define libwifi_tcpkeepalive_pkt(pkt, interval)		libwifi_ioctl(WIOCTCPKEEPALIVEPKT, (pkt), (void *)(interval))


/**
 * @brief Set packet filter group, default add a filter for unicast packet.
 *
 * @param ip point to local host ip address, if ip is NULL, it will not add a filter for any ARP packet.
 * 		     if ip is not NULL, it will add a filter for ARP query packet for this ip address.
 *
 * @return 0: ok
 *
 */
#define libwifi_set_filter_by_ip(ip, enable)	libwifi_ioctl(WIOCSETFILTERBYIP, (ip), (void *)(enable))
/*
 * Setup pktfilter, enable when host hibernate.
 * Note that current pktfilter we design it as "whitelist" style!! (pkt_filter_mode=1)
 *
 * Input format: <id> <polarity> <type> <offset> <bitmask> <pattern>
 * <id> User filter entry id must be from 100 to 102
 * <polarity> Should be 0
 * <type> Should be 0
 * <offset> The offset of bitmask in Ethernet packet.
 *
 * sample:
 * 1.Filter to allow all Unicast
 * "100 0 0 0 0x01 0x00"
 * 2.Filter to allow IPv4 ARP packet to host IP, not filter out as Broadcast
 * "101 0 0 12 0xFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000FFFFFFFF 0x0806000108000604000100000000000000000000000000000000c0a80102"
 */
/**
 * @brief Set custom single entry for additional packet filter, with filter enable/disable
 *
 * @param filter the filter which will be added and enable/disable.
 * @param enable 0: Disable, 1: Enable
 *
 * @return 0: ok
 */
#define libwifi_set_filter_by_user(filter,enable)	libwifi_ioctl(WIOCSETFILTERBYUSER, (filter), (void *)(enable))
/**
 * @brief set wifi to sleep mode.
 *
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default 10.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
#define libwifi_set_sleep(dtimskip)			libwifi_ioctl(WIOCSETSLEEP, (void *)(dtimskip), NULL)

/**
 * @brief set wifi to sleep mode with autoarp.
 *
 * @param ipaddr: the valaue of local ip address.
 * @param dtimskip: the value of dtimskip need >1 && <20
 * 				=0, the dtimskip value will be default 10.
 * 				!=0, the dtimskip value will be this parameter.
 * @return 0: ok
 *
 */
#define libwifi_set_autoarp_sleep(ipaddr,dtimskip)			libwifi_ioctl(WIOCSETAUTOARPSLEEP,(ipaddr), (dtimskip))

/*
 * Following API doesn't depend on wifi thread to run.
 */

int libwifi_wps_start(int sdio_id, u8 * mac, int mfg, t_wifi_conf * conf);
int libwifi_wps_stop(void);

int libwifi_mfg_start(int sdio_id, u8 * mac, t_wifi_conf * conf);
int libwifi_mfg_stop(void);

#endif
