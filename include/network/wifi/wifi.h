#ifndef _WIFI_H_
#define _WIFI_H_

#include "libwifi.h"

/* NOTE: this file is the WIFI driver API, all the WIFI driver should implement these API
   The APIs don't need thread-safe.
   */

typedef enum _WLAN_802_11_MESSAGE
{
	WLAN_MSG_CONNECTED = 0x1,
	WLAN_MSG_DISCONNECTED,
	WLAN_MSG_AP_INIT_SUCCESS,
	WLAN_MSG_AP_INIT_FAILURE,
	WLAN_MSG_CONNECTED_AP,
	WLAN_MSG_DISCONNECTED_AP,
	WLAN_MSG_CALLBACK_PMK,
	WLAN_MSG_CONNECTED_ALREADY,
	WLAN_MSG_END
} WLAN_802_11_MESSAGE;

#define WIFI_SUCCESS	 	0
#define WIFI_FAILED		1

typedef struct s_wifi_init_arg
{
	int sdio_id;
	u8 * mac;
	int mfg;
	int (*msg_hdl_cb)(int, void*);
	int (*network_read_cb)(u8*, int, int);

	t_wifi_conf * conf;
}t_wifi_init_arg;
/**
 * @brief init wifi module.
 *
 * @param sdio_id 	[in] 	sdio port id.
 * @param mac 		[in] 	mac!=null && *mac!=0, wifi module will set this mac address.
 *					[out]	return wifi module's mac address.
 * @param mfg 		[in] 	mfg=1, enable mfg mode, mfg=0, disable mfg mode.
 * @param msg_hdl_cb[in]	msg handle callback function
 * @param network_read_cb[in]	data read handle callback function
 * @return 0: ok, !=0: error
 */
int WIFI_init(t_wifi_init_arg * arg);
int WIFI_is_inited(void);

typedef struct s_wifi_connect_arg
{
	char *ssid;
	u8* key;
	u32 keylen;
	int *channel;
	int block;

	t_wifi_conf* conf;
}t_wifi_connect_arg;
/**
 * @brief connect to ap.
 *
 * @param ssid         [in]     the ssid will connect to.
 * @param channel     [in]     channel!=null && 14>*channel>0, will only scan ssid in this channel.
 *                    [out]    return ssid's channel.
 * @param key         [in]     password
 * @param keylen    [in]    password length.
 * @param block     [in]    block!=0, it will continue scan ap until connect success. otherwise it will return no matter connect is success or failed.
 * @param conf      [in]    more detail wifi configuration can be set in this structure. it also can be set NULL.
 * @return 0: ok, !=0: error
 *
 */
int WIFI_connect(t_wifi_connect_arg * arg);

typedef struct s_wifi_scanconnect_arg
{
	char *ssid;
	int ssidlen;
	u8 *bssid;
	u8* key;
	u32 keylen;
	int keyindex;
	u8 *pmk;
	u16 *chanspec;
	wifi_scan_security_t *scan_security;
	wifi_scan_encryption_t *scan_encryption;
	int block;
	t_wifi_conf* conf;
}t_wifi_scanconnect_arg;
/**
 * @brief scan & connect an ap with some special parameters, this is block api.
 *
 * @param ssid 		[in] 	the ssid which will be connected
 * @param bssid		[in/out] a pointer to ap's bssid, =NULL, function will scan ssid.
 * 							if not equal null, when connect success, it will return valid value.
 * @param chansped	[in/out] a pointer to ap's chanspec, =NULL or it's value = 0, function will scan ssid.
 * 							if != NULL, when connect success, it will return valid value.
 * @param scan_security		[in/out] a pointer to ap's scan_security, =NULL, function will scan ssid.
 * 									 if != NULL, when connect success, it will return valid value.
 * @param scan_encryption	[in/out] a pointer to ap's scan_encryption, =NULL, function will scan ssid.
 * 									 if != NULL, when connect success, it will return valid value.
 * @param key		[in]	ap's key
 * @param keylen	[in]	ap's keylen
 * @param keyindex	[in]	wep keyindex, only valid for wep mode.
 * @param pmk		[in/out]wpa/wpa2 pmk, pmk can be NULL or a 32 bytes array.
 * 							when connect success, it will return the right pmk value.
 * @param conf		[in]	a pointer to wifi_conf, when conf->security and conf->wepauthtype is not null, the station will connect ap with these two parameters.
 * @return 0: ok, >0 resume, <0: error
 *
 */
int WIFI_scanconnect( t_wifi_scanconnect_arg *arg);

typedef struct s_wifi_ioctl_arg
{
	int ioc;
	void * arg0;
	void * arg1;
}t_wifi_ioctl_arg;

int WIFI_ioctl(t_wifi_ioctl_arg * arg);

/**
 * @brief mfg process.
 *
 * @param forever 	[in] 	=1, WIFI_mfg() will not return.
 * @return 0: ok, !=0: error
 */
extern int WIFI_mfg( int forever);

int WIFI_stop(void *arg); //link down wifi.
int WIFI_up(void *arg);   //link up wifi.
int WIFI_exit(void * arg);
int WIFI_status(void);

/**
 * @brief read data from wifi module
 *
 * @param void
 *
 * @return data length that be read
 *
 */
int wlan_network_device_read(void);

/**
 * @brief write data to wifi module
 *
 * @param wr_buf the data buffer that be written
 * @param len data length that be written
 *
 * @return 0: ok, !=0: error
 *
 */
typedef struct s_wifi_write_arg
{
	u8* wr_buf;
	int len;
	int priv_index;
}t_wifi_write_arg;
int wlan_network_device_write(t_wifi_write_arg * arg);


/**
 * @brief build a ap.
 *
 * @param ssid 		[in] 	the ssid will be created.
 * @param channel 	[in] 	channel!=null it will set this channel value. otherwise use default channel.
 * @param key 		[in] 	password
 * @param keylen	[in]	password length.
 * @param encrypt   [in]	encrypt type.
 * @param broadcast [in]	=0, will disable ssid broadcast. =1 will enable ssid broadcast.
 * @param txpower 	[in]	if not null, it will use *txpower value to set tx power.
 * @param wifi_txpwrlimit_cfg [in]	if not null, it will use the configuration data to set Tx power limitation.
 * @return 0: ok, !=0: error
 *
 */
typedef struct s_wifi_ap_arg
{
	char *ssid;
	u8* key;
	u32 keylen;
	int encrypt;
	int security;
	int *channel;
	t_wifi_conf* conf;
}t_wifi_ap_arg;
int WIFI_ap( t_wifi_ap_arg* arg);

#if 0
/**
 * @brief build a adhoc.
 *
 * @param ssid 		[in] 	the ssid will be created.
 * @param channel 	[in] 	channel!=null it will set this channel value. otherwise use default channel.
 * @param key 		[in] 	password
 * @param keylen	[in]	password length.
 * @param encrypt   [in]	encrypt type.
 * @return 0: ok, !=0: error
 *
 */
extern int WIFI_adhoc( char *ssid, u8* key, u32 keylen, int *channel, int encrypt, t_wifi_conf* conf);
/**
 * @brief wps process.
 *
 * @param ssid 		[out] 	return ap's ssid
 * @param key 		[out] 	return ap's password
 * @param keylen	[out]	return ap's password length.
 * @return 0: ok, !=0: error
 *
 */
extern int WIFI_wps( char *ssid, u8* key, u8 * keylen);
/**
 * @brief wps no block process.
 *
 * @param ssid 		[out] 	return ap's ssid
 * @param key 		[out] 	return ap's password
 * @param keylen	[out]	return ap's password length.
 * @param stop		[in]	=0 resume wps process, !=0 stop wps process.
 * @return 0: ok, >0 resume, <0: error
 *
 */
extern int WIFI_wps_noblock( char *ssid, u8* key, u8 * keylen, int stop);

/**
 * @brief stop wifi and power down wifi module.
 *
 * @return
 *
 */
extern int WIFI_stop(void *arg);
/**
 * @brief return wifi status.
 * @return 0: not connect, =1: station mode and connected, =2: ap mode and ap is up.
 */
extern int WIFI_status(void);

#endif

#endif
