#ifndef _BOARD_H_
#define _BOARD_H_

//#define WriteReg32(addr,data) (*(volatile unsigned long *)(addr)=(data))
//#define WriteReg16(addr,data) (*( volatile unsigned short *)(addr)=(data))
//#define WriteReg8(addr,data) (*( volatile unsigned char *)(addr)=(data))

#ifdef CHIP_ECO1
#define WriteReg32(addr,data) (*(volatile unsigned long *)(addr)=(data))
#define WriteReg16(addr,data) (*( volatile unsigned short *)(addr)=(data))
#define WriteReg8(addr,data) (*( volatile unsigned char *)(addr)=(data))
#else
#define BUS_DELAY 50
#define WriteReg32(addr,data) do{ (*(volatile unsigned long *)(addr)=(data)); asm("dsb"); int delayloop; for(delayloop=0; delayloop<BUS_DELAY; delayloop++){asm("nop");} }while(0)
#define WriteReg16(addr,data) do{ (*( volatile unsigned short *)(addr)=(data)); asm("dsb"); int delayloop; for(delayloop=0; delayloop<BUS_DELAY; delayloop++){asm("nop");}}while(0)
#define WriteReg8(addr,data) do{ (*( volatile unsigned char *)(addr)=(data)); asm("dsb"); int delayloop; for(delayloop=0; delayloop<BUS_DELAY;  delayloop++){asm("nop");}}while(0)
#endif

#define ReadReg32(addr) (*( volatile unsigned long *)(addr))
#define ReadReg16(addr) (*( volatile unsigned short *)(addr))
#define ReadReg8(addr) (*(volatile unsigned char *)(addr))

#include "board_reg.h"

//INTERNAL_START
#define CONFIG_ICACHE
#define CONFIG_DCACHE

#define MEMBASE_DDR		(0x10100000)

////////////////////////////////////////////////////
////////////////////////////////////////////////////

/*********************************************
	Interrupt Number
*********************************************/
/**
 * @defgroup IRQ_BIT
 * @brief interrupt bits
 * @ingroup libirq
 * @{
 */
//INTERNAL_START
#define REG_INT_BASE    0xc0006600
//INTERNAL_END

/**
 * @REAL_FORMAT #define ENABLE_TICK
 * @brief enable system tick function
 * @details this enable chip response the system tick.
 *
 * @ingroup libtick
 */
//INTERNAL_START
#define ENABLE_TICK do{}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT #define DISABLE_TICK
 * @brief disable system tick function
 * @details this disable chip response the system tick, no matter the TICK is enabled or not.
 *
 * @ingroup libtick
 */
//INTERNAL_START
#define DISABLE_TICK do{}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT #define ENABLE_INTERRUPT
 * @brief enable system interrupt function
 * @details this enable chip response the system interrupt.
 *          when interupt trigger, hardware will first check system interrupt enable bit, if enabled, then check interrupt mask bit, if this bit is enable, then dispatch the interrupt to firmware.
            so when this bit(system interrupt enable bit) is disabled, no interrupt will be dispatched to firmware.
 *
 * @ingroup libirq
 */
//INTERNAL_START
#define ENABLE_INTERRUPT asm("cpsie i")
//INTERNAL_END

/**
 * @REAL_FORMAT #define DISABLE_INTERRUPT
 * @brief disable system interrupt function
 * @details this disable chip response the system interrupt, no matter the interrupt mask bits are ON or OFF.
 *          when call ENABLE_INTERRUPT again, the interrupt mask bits will keep the same.
 *
 * @ingroup libirq
 */
//INTERNAL_START
#define DISABLE_INTERRUPT asm("cpsid i")
//INTERNAL_END

/**
 * @REAL_FORMAT spin_lock_irqsave(flags) 
 * @brief lock the chip bus so only current CPU can access memory, disable interrupt and tick and save status to flags.
 * @details used in audio decoder and recorder where dual BA are used
 * @param flags  save status for spin_unlock_irqrestore()
 * @ingroup libirq
 */
//INTERNAL_START
#define spin_lock_irqsave(flags)  do{local_irq_save(flags);  BUS_LOCK;}while(0)
//INTERNAL_END

/**
 * @REAL_FORMAT spin_unlock_irqrestore(flags) 
 * @brief  unlock the chip bus that locked by spin_lock_irqsave() and restore interrupt disabled by spin_lock_irqsave()
 * @details used in audio decoder and recorder where dual BA are used
 * @param flags  interrupt status saved by spin_lock_irqsave()
 * @ingroup libirq
 */
 //INTERNAL_START
#define spin_unlock_irqrestore(flags)  do{BUS_UNLOCK; local_irq_restore(flags);}while(0)
//INTERNAL_END

/**
 * @brief resetthe whole chip.
 * @details reset the whole chip and reboot according to the boot pin.
 * @ingroup lib_cpu
 */
void reboot(void);
#endif
