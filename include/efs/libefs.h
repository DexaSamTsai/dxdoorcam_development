#ifndef _LIBEFS_H_
#define _LIBEFS_H_

//INTERNAL_START
struct s_efs;
struct s_efs_file;
struct s_efs_dirent;
struct s_efs_stat;
//INTERNAL_END

/**
%en * @brief this is write cache structure. APP set this structure before using the function of writing data with cache by calling EFS_DECLARE_WRITECACHE. It is accelerate writing speed when use cache.
%zh * @brief 这是writing cache结构体.在使用writing cache功能前,应用程序需要设置这个结构体,可以通过调用EFS_DECLARE_WRITECACHE实现.使用cache可以加速写速度.
 */
typedef struct s_efs_writecache
{
	int size;	        ///< cache sector counts, not cache size.
	unsigned char *buf; ///< cache buffer, 512*size bytes
//INTERNAL_START
	int used; ///< internal parameter
//INTERNAL_END
}t_efs_writecache;

//INTERNAL_START
typedef struct s_efs_iobuf_history{
	u32 sector;
	u8  status;
	u8  used_cnt;
	u8  pading0;
	u8  pading1;
}t_efs_iobuf_history;

#define IOBUF_HISTORY_MAX 3
typedef struct s_efs_iobuf
{
	struct s_efs_iobuf_history history[IOBUF_HISTORY_MAX]; //8 * 3 bytes
	u32 sector;
	u8 status;
	u8 used_cnt;
	u8 ref_cnt;
	u8 history_used; //8 bytes, total 32 bytes.
	u8 cache_mem[512];
}t_efs_iobuf;

#if 0
typedef struct s_efs_nwabuf_sta
{
	u8 *buf_start;
	u8 *cache_start;
	u32 cache_len;
	u32 cache_sec;
}t_efs_nwabuf_sta;

typedef struct s_efs_nwa
{
	//public, should set before use
	u32 CLUSTER_INDEX_BUF;
	u32 NEW_SECTOR_CNT;
	u32 FAT_END_FLAG; //0xffffffff
	u8 noidx;

	//internal
	u8 f_wrfat; //1: write fat to sd, 0: skip
	t_efs_nwabuf_sta bs[3];
	u32 first_cluster; //available after file_close(0

	//for index
	//public, should set before use
	u16 *idx_offset_buf;
	u32 IDX_WSIZE;
	//internal
	u32 idx_offset_start;
	u16 idx_offset_count;
	u16 idx_offset_1p5G;
	u16 idx_offset_1G;
	u16 idx_offset_512M;
	u32 first_idx_offset;
	u32 first_idx_sec;

	//public, should set before use
	u8 *idx_buf;
	//internal
	u32 idx_size; //idx_buf used
	u32 idx_len; //total idx_len
}t_efs_nwa;
#endif

typedef struct s_efs_part{
	u32 LBA_offset;
	u32 sector_cnt;
	u8 type;
}t_efs_part;
//INTERNAL_END

/**
%en * @brief This is BIOS parameter block(BPB) parameters of embedded file system.
 * Normally, these parameters aren't be modified by the users, they are read-only.
%zh * @brief 这是文件系统中BPB(BIOS Parameter Block)参数结构体.通常情况下,这些参数是只读的. 
 */
typedef struct s_efs_bpb{
	u8 sectors_per_cluster;   ///<sectors count of every cluster
	u8 number_of_fats;        ///<fat table number
	u16 bytes_per_sector;     ///<sector size
	u16 reserved_sectors;     ///<reserved sectors count
	u16 root_entries;         ///<root entries count, it is used only in fat12/fat16, it is 0 in fat32
	u16 sectors_fat16;        ///<sectors count, it is used only in fat12/16. it is 0 in fat32
	u16 fat_sectors_fat16;    ///<sectors count of fat table, it is only used in fat12/16. it is 0 in fat32
	u32 sectors;              ///<sectors count, it is used in fat32
	u32 fat_sectors;          ///<sectors count of fat table, it is used in fat32
	u32 root_cluster;         ///<root cluster number, it is used in fat32
}t_efs_bpb;

//INTERNAL_START
typedef struct s_clusterfullbuf
{
	u32 start; //cluster
	u32 length;
}t_clusterfullbuf; //sorted by start
//INTERNAL_END

/**
%en * @brief This is data structure for long file name.
%zh * @brief 这是处理长文件名的结构体.
 */
typedef struct s_efs_lf{
	unsigned short lname[255]; 			///<buffer pointer to long file name, the maximal length is less than 255
//INTERNAL_START
	char is_cfname;
	unsigned short * cname;  ///< backup
#define MODE_FILE_OPEN   (0x0)
#define MODE_FILE_REMOVE (0x1)
#define MODE_FILE_RMS    (0x2)
	char mode;                      //mode.                   

	///< private parameters
	char str_state;
	unsigned char checksum;
	unsigned char locOffset;
	unsigned int locSector;
//INTERNAL_END
}t_efs_lf;

//INTERNAL_START
typedef struct s_efs_ccc{
	u32 disc;
	u32 logic;
	u8 linear;
}t_efs_ccc;

#define EFS_CCC_SIZE 6
typedef struct s_efs_cluster{
	u8 linear;
	u32 logic;
	u32 disc;
	u32 first;
	u32 last;
	u32 cnt;
	t_efs_ccc ccc[EFS_CCC_SIZE]; //two cluster chain cache for file_fread, 0 for video, 1 for audio
}t_efs_cluster;
//INTERNAL_END

/**
 * @name attribute
%en_start
 * @details attribute paramter
%en_end
%zh_start
 * @details 属性参数
%zh_end
 * @{
 */
#define ATTR_FILE       0x00
#define ATTR_READ_ONLY  0x01      
#define ATTR_HIDDEN     0x02     
#define ATTR_SYSTEM     0x04     
#define ATTR_VOLUME_ID  0x08
#define ATTR_DIRECTORY  0x10
#define ATTR_ARCHIVE    0x20
/**
%en * @brief This is file entry structure. file entry is 32 bytes to define the file attribute.
%zh * @brief 这是文件目录项结构体.每个文件目录项都是32字节. 
 */
typedef struct s_efs_dirent{
	u8 filename[11];             ///<full file name, the first eight bytes is file name, and after three bytes is extension.
	u8 Attribute;               ///<file attribute
	u8 reserved;                ///<reserved
	u8 ts;                      ///<10ms bit of created time
	u16 create_time;            ///<created time
	u16 create_date;            ///<created date
	u16 access_date;            ///<last access date 
	u16 clus_high;              ///<the higher 16bit of file first cluster
	u16 modify_time;            ///<modified time
	u16 modify_date;            ///<modified data
	u16 clus_low;               ///<the low 16bit of file first cluster
	u32 size;                   ///<file length
}t_efs_dirent;

//INTERNAL_START
typedef struct s_efs_stat{
	u32 Sector;
	u8 Offset;
	u8 Attribute;
}t_efs_stat;
//INTERNAL_END

/**
%en * @brief This is s_efs_file structure
%zh * @brief 这是文件结构体
 */
typedef struct s_efs_file
{
	struct s_efs_writecache * wc;   ///<pointer to writecache structure
	struct s_efs * efs;             ///<pointer to embedded file system structure
	struct s_efs_dirent dirent;     ///<pointer to file entry structure
	struct s_efs_stat loc;          ///<pointer to file location structure 
	struct s_efs_cluster cluster;   ///<pointer to cluster chain structure
	u32 size;                       ///<file length
	u32 ptr;                        ///<file position 
	u8 status;                      ///<file status, open or write
	u8 wc_from_heap;                ///<write cache may be allocated by dynamically(wc_from_heap=1), or allocated statically.
}t_efs_file;

//INTERNAL_START
typedef struct s_efs_nand_badblock{
	u32 count;
	u32 * id; //bad block page start address, sort by id.
}t_efs_nand_badblock;

struct s_efs_nand_sparecache;
typedef struct s_efs_nand_sparecache{
	u32 maxnum; //cache use hash, maxnum should be 2^n
	u32 *data;//BIT31: valid? BIT30: 0xff or 'a', other bits: row_adr
	int (*cb_cache_read_spare)(struct s_efs_nand_sparecache *sc, u32 row_adr, unsigned char * dst_adr, int size);
	int (*cb_cache_write_spare)(struct s_efs_nand_sparecache *sc, u32 row_adr);
}t_efs_nand_sparecache;

typedef struct s_efs_nand{
	struct s_efs_nand_badblock * bb;

	struct s_efs_nand_sparecache * sc;

	//for nand
	unsigned int start_page;//FAT start page
	unsigned int tmpblk0;
	unsigned int tmpblk1; //tmpblk page address
	u32 sectorCount;

	int (*cb_efs_hw_nand_update_secs)(unsigned char *buf, int secnum, int count, int tmpblk);
	void (*cb_efs_hw_nand_read_flush)(struct s_efs * efs);
}t_efs_nand;
//INTERNAL_END

/**
 * @name file system type 
%en_start
 * @details file system type
%en_end
%zh_start
 * @details 文件系统类型
%zh_end
 * @{
 */
#define FAT12 1
#define FAT16 2
#define FAT32 3
/** @} */

enum{
	EFS_HW_TYPE_SCIF,
	EFS_HW_TYPE_NAND,
	EFS_HW_TYPE_UHOST,
};

typedef struct s_efs_hw
{
	int type;
	int (*init)(struct s_efs * efs, void * hw_arg);
	int (*read)(struct s_efs * efs, u32 addr, u8 * buf, int count);
	int (*write)(struct s_efs * efs, u32 addr, u8 * buf, int count);
	int (*exit)(struct s_efs * efs);
}t_efs_hw;

#define EFS_WRITE_SECS_DIRECT  (0x01)
#define EFS_WRITE_SECS_CACHE   (0x02)
#define EFS_READ_SECS_DIRECT   (0x03)
#define EFS_READ_SECS_CACHE    (0x04)
#define EFS_FLUSH_SECS         (0x05)

typedef struct s_efs_msg{
	int msg;
	int dst_addr;
	int src_addr;
	int count;
	t_ertos_taskhandler task_handler;
}t_efs_msg;

typedef struct s_efs_task
{
	u8 prio;
	u8 queue_cnt;
	u8 msg_cnt;
	t_ertos_queuehandler efs_queue;
	u32 queue_timeout;
	t_efs_msg *efs_msg;
}t_efs_task;


/**
%en * @brief This is embedded file system structure
%zh * @brief 这是文件系统结构体
 */
typedef struct s_efs
{
	//general info
	u32 fat_sectors;          ///< sectors count of fat table
	u32 sectors;              ///< total sectors count of file system
	u32 clusters;             ///< total clusters count of file system
	u32 root_first_sector;    ///< first sector of root directory
	u32 root_first_cluster;   ///< first cluster of root directory
	u32 curdir_first_sector;  ///< first sector of current directory
	u32 curdir_first_cluster; ///< first cluster of current directory
	u32 clusters_free;        ///< total free clusters
	u32 next_free_cluster;    ///< next free cluster
	u8 type;                  ///< file system type, options: see the group macro "file system type" 
//INTERNAL_START
	//efs_init() just load partition table, don't init fat32
	u8 load_part_only;

	//write pre alloc
	u16 pre_alloc_clusters;

	u32 fat_start_sector; //used by nwa
	u32 fat_end_sector; //used by nwa

	//partition table
	t_efs_part part_all[4];
	t_efs_part part;
//INTERNAL_END
	t_efs_bpb bpb;          ///< pointer to BIOS Parameters block(bpb) structure

//INTERNAL_START
	//ncf
	u32 ncf_max_sec_cnt;
	u32 ncf_buf_addr;
	u8 ncf_en; //if 0, only efs_calc_free() will use ncf. if 1, guess_free_cluster() also use ncf.
//INTERNAL_END

	u8 listdir_filterparent_disable; ///< set to 1 to disable filter of '.' and '..' when list files. root directory doesn't have '.' and '..', so the parameters shoud be set to 1 when list file of root directory. 
	u8 fastwrite_en;                 ///< set to 1 when write many continuous cluster when writing size is larger than one cluster.
//INTERNAL_START
	u8 parent_flag;                  ///< check '.' && '..'
//INTERNAL_END

	u8 ccc_en;                       ///< set to 1 when use cluster cache.

//INTERNAL_START
	//fast seek
	u8 fastseek_en;                  ///< set to 1 when use cluster fast seeking 
	u32 fastseek_addr;               
	u32 fastseek_count;
//INTERNAL_END

	t_efs_lf lf;                    ///< long file structure
	u8 lf_en;                       ///< set to 1 when supports long file name
	u8 ch_en;

	u8 cfb_en;                      ///< set to 1 when fast finding of next free cluster
	u8 cfb_total_cnt;               ///< array count.
//INTERNAL_START
	u8 cfb_cnt;
	u32 cfb_lastfullcluster;
	u32 cfb_lastfulllength;
//INTERNAL_END
	t_clusterfullbuf *cfb_buf;      ///< buffer pointer to \ref t_clusterfullbuf structure, it is used to save the continuous clusters, it should be \ref cfb_total_cnt size

	u8 * unlink_buf;           ///< buffer for accelerate the speed of deleting file. buffer size should be multiple of 512 bytes.                    
	u32 unlink_buf_cnt;        ///< buffer count = buffer size/512
	u8 unlink_update_nextfree; ///< the flag indicates to update \ref next_free_cluster when delete file

	u8 sync;                       ///<update file size in time when write file.
	u16 iobuf_cnt;                 ///< IO buffer count
	struct s_efs_iobuf * iobuf;    ///< pointer to IO buffer

	t_efs_task task_param;
//INTERNAL_START
#define ERR_EFS_MUTEX_TIMEOUT     (-7)
	t_ertos_mutexhandler mutex;   ///< mutex handler
	u32 mutex_timeout;

	//nwa
	//t_efs_nwa * nwa;               

	//nand
	struct s_efs_nand * nand;
	
	//hw
#define EFS_HW_STATUS_UNMOUNT (0)
#define EFS_HW_STATUS_MOUNT   (1) 
	u8 hw_status; //hardware status(EFS_HW_STATUS_MOUNT/EFS_HW_STATUS_UNMOUNT)
	void * hw_arg; //for scif_drv
	t_efs_hw * efs_hw; // pointer to efs hardware
	int sector_cnt; //total sector count for the hw

	u8 boot_stage;

	//callbacks for patch
	void (*cb_part_sel)(struct s_efs *);
	int (*cb_sec_rel)(struct s_efs * efs, u8 * buf);
	u8 * (*cb_sec_get)(struct s_efs * efs, u32 addr, u8 mode);
	int (*cb_init_part_pre)(struct s_efs * efs);
	int (*cb_init_part_post)(struct s_efs * efs);
	u32 (*cb_fat_get_sector_addr)(struct s_efs * efs, u32 cluster);
	int (*cb_dir_find_file)(struct s_efs * efs, char * filename, struct s_efs_stat * loc, u32 * last);
	char * (*cb_file_fatname)(struct s_efs * efs, char * filename, char * fatname, u16 * flen);
	int (*cb_fat_cluster_seek)(struct s_efs * efs, t_efs_cluster * cluster, u32 offset);
	int (*cb_fat_cluster_fastseek)(struct s_efs * efs, t_efs_cluster * cluster, u32 offset);
	u32 (*cb_dir_find_indir)(struct s_efs * efs, char * fatname, u16 flen, u32 firstcluster, t_efs_stat * loc, u8 mode);
	u32 (*cb_dir_find_file_inbuf)(struct s_efs * efs, u8 *buf, char * fatname, u16 flen, u32 sec, t_efs_stat * loc);
	u8 (*cb_dir_find_file_entry)(struct s_efs * efs, u8 *data, char *fatname, u16 flen);
	u32 (*cb_dir_find_free_inbuf)(struct s_efs * efs, u8 *buf, char * fatname, u16 flen, u32 sec, t_efs_stat * loc);
	u8 (*cb_dir_find_free_entry)(struct s_efs * efs, u8 *data, char *fatname, u16 flen);
	void (*cb_dir_lname2sname)(struct s_efs * efs, char *lname, char *sname);
	int (*cb_dir_fatname)(struct s_efs * efs, char * filename, char * fatfilename, char mode);
	int (*cb_direct_read)(struct s_efs * efs, u32 addr, u8* buf);
	int (*cb_file_create)(t_efs_file * file, struct s_efs * efs, char * filename);
	int (*cb_dir_find_free_withparent)(struct s_efs * efs, char * filename, t_efs_stat * loc, u32 parent);
	int (*cb_fat_cluster_alloc)(struct s_efs * efs, t_efs_cluster * cluster, u32 num_clusters);
	int (*cb_fat_cluster_alloc_post)(struct s_efs * efs, t_efs_cluster * cluster, u32 num_clusters);
	u32 (*cb_time)(void); //return the second since 1970-1-1
	u32 (*cb_fat_find_free_cluster)(struct s_efs * efs);
	u32 (*cb_fat_guess_free_cluster)(struct s_efs * efs);
	int (*cb_dir_list_dirent)(struct s_efs * efs, u8 *data, int (*list)(const struct dirent *));
	u32 (*cb_wc_write)(t_efs_file *pfile, u8 * buf, u32 size);
	void (*cb_wc_flush)(t_efs_file * pfile);
	u32 (*cb_file_write_no_wc)(t_efs_file *file, u8 *buf, u32 size);
	int (*cb_efs_unlink)(struct s_efs * efs, char * filename, u32 * fsize);
	int (*cb_efs_unlink_post)(struct s_efs * efs, char * filename, u32 fsize);
	int (*cb_fat_cluster_unlink)(struct s_efs * efs, t_efs_cluster * cluster);
	void (*cb_dir_time_to_timedate)(struct s_efs * efs, u32 time, u16 * filetime, u16 * filedate);
	int (*cb_efs_mkfat32_1)(struct s_efs * efs, char * volume_label, u8 * buf, u32 force_c, u32 c, u32 ns, u32 fs);
#if 0
	void (*cb_nwa_idx_offset_insert)(struct s_efs * efs, u32 offset);
	u32 (*cb_nwa_inidx)(struct s_efs * efs, u32 offset);
	int (*cb_nwa_bs_write)(struct s_efs * efs, u32 sec, u8 *buf);
	u32 (*cb_nwa_fat32_gen_list)(struct s_efs * efs, u32 firstcluster, u32 filesize);
	int (*cb_nwa_close)(t_efs_file *file);
#endif
	u32 (*cb_dir_l2s_algo)(struct s_efs * efs, char *lname, u8 flen);
	char * (*cb_file_ch_fatname)(struct s_efs * efs, char * filename, char * fatname, u16 * flen);
	void (*cb_dir_ch_lname2sname)(struct s_efs * efs, short *lname, char *sname);
	u32 (*cb_dir_ch_l2s_algo)(struct s_efs * efs, short *lname, u8 flen);
	int (*cb_efs_rename)(struct s_efs * efs, char *filename, char *newname);
	int (*cb_efs_access)(struct s_efs * efs, const char *pathname, int mode);
	int (*cb_efs_chdir)(struct s_efs * efs, const char * path);
	int (*cb_efs_status)(t_efs_file *file, struct stat * buf);
//INTERNAL_END
}t_efs;

//INTERNAL_START
/***************** internal functions ***************/
int iobuf_init(t_efs * efs);
int iobuf_flush(t_efs * efs);
#define IOBUF_MODE_RO	1
#define IOBUF_MODE_RW	2
#define IOBUF_MODE_NOREF	4
u8 * iobuf_get(t_efs * efs, u32 addr, u8 mode);
int iobuf_release(t_efs * efs, u8* buf);
int iobuf_read(t_efs * efs, u32 addr, u8* buf); //get and memcpy
int iobuf_write(t_efs * efs,u32 addr, u8* buf);

int efs_init_part(t_efs * efs);

//get sector of of partition, addr: sector offset in a partition
u8 * sec_get(t_efs * efs, u32 addr, u8 mode);
int sec_rel(t_efs * efs, u8 * buf);

u32 fat_get_sector_addr(t_efs * efs, u32 cluster);
u32 fat_get_next(t_efs * efs, u32 cluster);
u32 fat_get_next_inbuf(t_efs * efs, u32 cluster, u8 * buf);
void fat_set_next_inbuf(t_efs * efs, u32 cluster, u32 next_cluster, u8 * buf);
void fat_set_next(t_efs * efs, u32 cluster, u32 next_cluster);
int fat_cluster_seek(t_efs * efs, t_efs_cluster * cluster, u32 offset);
u32 fat_cluster_last(t_efs * efs, t_efs_cluster * cluster);
int fat_cluster_fastseek(t_efs * efs, t_efs_cluster * cluster, u32 offset);
int fat_cluster_alloc(t_efs * efs, t_efs_cluster * cluster, u32 num_clusters);
int fat_cluster_unlink(t_efs * efs, t_efs_cluster * cluster);
u32 fat_find_free_cluster(t_efs * efs);
u32 fat_guess_free_cluster(t_efs * efs);
u32 fat_endmark(t_efs * efs);
int fat_is_end(t_efs * efs, u32 c);

u32 part_get_LBA(t_efs * efs, u32 addr);
u32 part_cluster_addr(t_efs * efs, u32 cluster); //return sector offset in part of a cluster
int part_cluster_clear(t_efs * efs, u32 cluster);

int ncf_get_fsinfo(t_efs * efs, u8 force_update);
u32 ncf_find_free_cluster(t_efs * efs, u32 firstcluster);
u32 efs_calc_free(t_efs * efs, u32 indexbuf, u32 indexbufcnt);

int cfb_check(t_efs * efs, u32 c);
void cfb_insert(t_efs * efs, u32 cstart, u32 clen);
void cfb_delete(t_efs * efs, u32 cstart, u32 clen);

char * file_fatname(t_efs * efs, char * filename, char * fatname, u16 * flen);
u8 file_fatname_char(u8 c);
u32 file_read(t_efs_file *file, u8 *buf, u32 size);
u32 file_write_no_wc(t_efs_file *file, u8 *buf, u32 size);
u32 file_write_no_nwa(t_efs_file *file, u8 *buf, u32 size);
int file_fclose_no_nwa(t_efs_file * file);

//u32 nwa_write(t_efs_file *file, u8 *buf, u32 size);
//int nwa_close(t_efs_file *file);

u32 wc_write(t_efs_file *pfile, u8 * buf, u32 size);
void wc_flush(t_efs_file * pfile);

int dir_fatname(t_efs * efs, char * filename, char * fatfilename, char mode);
void dir_stat_to_dirent(t_efs * efs, t_efs_dirent * dirent, t_efs_stat * loc);
void dir_dirent_to_stat(t_efs * efs, t_efs_dirent * dirent, t_efs_stat * loc);
void dir_stat_update_firstcluster(t_efs * efs, t_efs_stat * loc, u32 cluster_addr);
void dir_buf_to_dirent(u8 *c, t_efs_dirent * dent);
void dir_dirent_to_buf(t_efs_dirent * dent, u8 *c);
void dir_dirent_init(t_efs * efs, t_efs_dirent * dirent, char * fatfilename);
void dir_set_file_size(t_efs * efs, t_efs_stat * loc, u32 numbytes);
void dir_set_modify_date(t_efs * efs, t_efs_stat * loc, u32 time);
int dir_find_free_withparent(t_efs * efs, char * filename, t_efs_stat * loc, u32 parent);
void dir_cluster_init(t_efs * efs, t_efs_cluster * cache, u32 firstcluster);

int fsinfo_get(t_efs * efs);
int fsinfo_set(t_efs * efs);

int efs_init_internal(t_efs * efs, void * hw_arg);

void ncf_reinit(t_efs * efs, u32 indexbuf, u32 indexbuf_cnt);
int file_write_direct(t_efs * efs, u32 address, u8 *buf, int count);
int file_read_direct(t_efs * efs, u32 address, u8 *buf, int count);
int file_read_cache(t_efs * efs, u32 addr, u8 * buf, u32 count);
int file_write_cache(t_efs * efs, u32 addr, u8 * buf, u32 count);
int file_flush(t_efs * efs);
u32 dir_rand(u32 seed);
void dir_dec2hex(u32 dec, char *hex);
char * file_ch_fatname(t_efs * efs, char * filename, char * fatname, u16 * flen);
void dir_ch_lname2sname(t_efs * efs, short * lname, char * sname);
//INTERNAL_END

/**
%en * @brief initialize file system
%zh * @brief 初始化文件系统
 *
 * @param efs_hw  pointer to structure \ref t_efs_hw
 * @param arg     pointer to hardware argument
 * @param buf     the buffer is used to find free cluster and count the total free clusters.
 * @param buf_size the minimal size is 512 bytes.
 *
 * @return 0 : ok. < 0 failed
 */
int efs_init(t_efs_hw * efs_hw, void * arg, u32 buf, u32 buf_size);

/**
%en * @brief exit file system
%zh * #brief 退出文件系统
 * 
 * @param efs pointer to structure \t_efs
 * 
 * @return 0 : ok. < 0 failed
 */
int efs_exit(t_efs * efs);

/**
%en * @brief mount a hardware for file system
%zh * @brief 挂载一个硬件作为文件系统
 *
 * @param efs_hw  pointer to structure \ref t_efs_hw
 * @param arg     pointer to hardware argument
 *
 * @return 0 : ok. < 0 failed
 */
int efs_mount(t_efs * efs, void * hw_arg);

/**
%en * @brief unmount a hardware 
%zh * #brief 卸载一个硬件
 * 
 * @param efs pointer to structure \t_efs
 * 
 * @return 0 : ok. < 0 failed
 */
int efs_unmount(t_efs * efs);

		
/**
%en * @brief read data from a file
%zh * @brief 这个函数是用来从文件中读数据.
 *
 * @param file  pointer to structure \ref t_efs_file
 * @param buf   buffer for the data are read from a file
 * @param size  read bytes
 *
 * @return real read bytes if read successfully
 *         -1 if read failure
 */
u32 efs_read(t_efs_file *file, u8 *buf, u32 size);

/**
%en * @brief write data to a file
%zh * @brief 这个函数是用来写数据到文件中.
 *
 * @param file  pointer to structure \ref t_efs_file
 * @param buf   buffer for the data which are written to a file 
 * @param size  written bytes
 *
 * @return real written bytes if write successfully
 *         -1 if write failure
 */
u32 efs_write(t_efs_file *file, u8 * buf, u32 size);


/**
 * @name mode_type
%en_start 
 * @details file open mode
%en_end
%zh_start
 * @details 文件操作类型
%zh_end
 * @{
 */
#define MODE_READ 0x72
#define MODE_WRITE 0x77
#define MODE_APPEND 0x61
/** @} */

/**
%en * @brief open a file with writecache
%zh * @brief 打开一个文件，如果是写文件要带write cache.
 * @details the file could exist or not exist.use EFS_DECLARE_WRITECACHE to declare buffer for write cache before call this function. if mode is MODE_READ, doesn't need write cache.
 *
 * @param efs    pointer to structure \ref t_efs 
 * @param file   pointer to structure \ref t_efs_file
 * @param fname  file name, it is MSDOS8.3 format, also is long file name
 * @param mode   only support 'MODE_READ','MODE_WRITE','MODE_APPEND'
 * @param wc     pointer to structure \ref t_efs_writecache
 * 
 * @return 0:ok; <0:failed
 */
int efs_fopen_with_wc(t_efs * efs, t_efs_file* file, char * filename, char mode, t_efs_writecache * wc);

/**
 * @REAL_FORMAT int efs_open(t_efs * efs, t_efs_file * file, char * fname, char mode);
%en  * @brief open a file
%zh  * @brief 打开一个存在或者不存在的文件. 
 * @details the file could exist or not exist
 *
 * @param efs    pointer to structure \ref t_efs 
 * @param file   pointer to structure \ref t_efs_file
 * @param fname  file name, it is MSDOS8.3 format, also is long file name
 * @param mode   only support 'MODE_READ','MODE_WRITE','MODE_APPEND'
 * 
 * @return 0:ok; <0:failed
 */
//INTERNAL_START
#define efs_open(efs, file, fname, mode) efs_fopen_with_wc(efs, file, fname, mode, NULL)
//INTERNAL_END

/**
%en * @brief close a file
%zh * @brief 关闭一个文件
 *
 * @param file  pointer to structure \ref t_efs_file
 *
 * @return 0:ok; <0:failed
 */
int efs_close(t_efs_file *file);

/**
%en * @brief list files or sub-directories of a appointed directory
%zh * @brief 列出一个指定目录下的所有文件和子目录. 
%en_start
 * @details list all files or sub-directories with MS-DOS8.3 format file name or long file name under one directory.
%en_end
%zh_start
 * @details 列出一个指定目录下的所有文件和子目录,支持MS-DOS8.3格式的文件名和长文件名.
%zh_end
 *
 * @param efs        pointer to structure \ref t_efs
 * @param dirname    directory path, it could MSDOS8.3 or long file name
 * @param list       callback function for handling each file entry
 *
 * @return 		     >0 number of files/directories in the given directory 
 *                   -1 if directory does not exist
 *                   -2 wrong directory without '.' and '..' except root directory
 */
//int efs_listdir(t_efs * efs, char * dirname, int (*list)(t_efs_dirent *));
int efs_listdir(t_efs_file * fp, u8 * data, int (*list)(const struct dirent *), struct dirent * drent);
int efs_readdir(t_efs_file *dir, u8 *buf, int size);
int efs_opendir(t_efs * efs, t_efs_file * dir, char * dirname);

//INTERNAL_START
/**
%en * @brief find a appointed file or directory  in file system
%zh * @brief 找到一个指定的文件或者目录.支持MS-DOS8.3格式文件名和长文件名
 *
 * @param efs        pointer to structure \ref t_efs
 * @param filename   file or directory name, it could be MSDOS8.3 or long file name
 *
 * @return 0         the file or directory doesn't exist
 *         1         the file exists
 *         2         the directory exists
 */
int efs_stat(t_efs * efs, char * filename);
//INTERNAL_END

/**
%en * @brief create a new directory
%zh * @brief 创建一个新的目录
 *
 * @param efs        pointer to structure \ref t_efs
 * @param dirname    directory path, it could be MSDOS8.3 or long file
 *
 * @return 0         ok
 *         < 0       failed
 */
int efs_mkdir(t_efs * efs,char * dirname);

/**
%en * @brief set file position
%zh * @brief 设置文件读写的开始位置.
 * @details set a appointed position in a file to read or write
 *
 * @param file       pointer to structure \ref t_efs_file
 * @param pos        position
 * @param whence     SEEK_SET(0), SEEK_CUR(1), SEEK_END(2)
 *
 * @return 0         ok
 *         -1        position exceeds the file length
 */
int efs_setpos(t_efs_file *file, u32 pos, int whence);

/**
%en * @brief get file position
%zh * @brief 获取文件读写的当前位置.
 * @details get the current position in a file to read or write
 *
 * @param file       pointer to structure \ref t_efs_file
 *
 * @return 0         ok
 *         -1        position exceeds the file length
 */
int efs_getpos(t_efs_file *file);

//INTERNAL_START
int efs_unlink_fsize(t_efs * efs, char * filename, u32 * fsize); //return: 0: ok, <0: error.
//INTERNAL_END

/**
%en * @brief delete a existing file
%zh * @brief 删除一个已经存在的文件.支持MS-DOS8.3格式文件名或者长文件名.
 * @details support the file with MS-DOS8.3 format file name or long file name
 *
 * @param efs        pointer to structure \ref t_efs
 * @param fname      file name, it could be MS-DOS8.3 or long file
 *
 * @return > 0       deleted file size
 *           0       failed
 */
int efs_unlink(t_efs * efs, char * fname);

/**
%en * @brief flush all data in io buffer to external storage device
%zh * @brief 将io buffer中的内容写入外部存储器.
%en_start
 * @details this function should be called after writing operation(such as write file(call it after efs_close()), or delete file, format file system and so on)
%en_end
%zh_start
 * @details 在所有对外部储存器进行写操作之后必须调用此函数.例如(写一个文件,在efs_close后调用efs_flush)
%zh_end
 *
 * @param efs        pointer to structure \ref t_efs
 *
 * @return 0         ok
 *         -1        failed
 */
int efs_flush(t_efs * efs);

//INTERNAL_START
/**
%en * @brief modify DBR/BPB setting.
%zh * @brief 格式化文件系统,这个函初始化文件系统DBR/BPB区且只适用于FAT32,
 * @details this function only is used for FAT32
 *
 * @param efs            pointer to structure \ref t_efs
 * @param volume_label   volume label
 * @param buf            internal buffer
 * @param buf_size       buffer size, it is 512 bytes
 * @param force_c        sectors of every cluster
 *
 * @return 0             ok
 *         < 0           failed
 */
int efs_mkfat32(t_efs * efs, char * volume_label, u8 *buf, u32 buf_size, u8 force_c); 

/**
%en * @brief format filesystem. clear FAT table and root directory
%zh * @brief 格式化文件系统,这个函数初始化FAT表和根目录项.
%en_start
 * @details don't change DBP/BPB setting
%en_end
%zh_start
 * @details 不改变文件系统DBR/BPB区.
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param buf            internal buffer
 * @param buf_size       buffer size, it should be the mutiple of 512 bytes
 *
 * @return 0             ok
 *         < 0           failed
 */
int efs_format_fat(t_efs * efs, u8 *buf, u32 buf_size);
//INTERNAL_END

/**
%en * @brief move a directory or a file to another directory.
%zh * @brief 移动一个文件或者一个目录到另一个指定目录.
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param srcname        source directory name or file name
 * @param dstname        destination directory
 *
 * @return 0             ok
 *         < 0           failed
 */
int efs_move(t_efs * efs, char * srcname, char * dstname);

/**
%en * @brief rename file name or directory name.
%zh * @brief 更改文件名或者目录名.
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param filename       file path
 * @param newname        new name, doesn't includes full path
 *
 * @return  -1: file or directory doesn't exist 
 *           0: ok
 */
int efs_rename(t_efs * efs, char *filename, char *newname);

/**
%en * @brief check the permission of a existed file
%zh * @brief 检测某一存在的文件的权限。
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param pathname       file path
 * @param mode           permission mode 
 *
 * @return  -1: don't pass the permission checking  
 *           0: ok
 */
#define FILE_ACCESS_OK  (0)
#define FILE_WRITE_OK   (2)
#define FILE_READ_OK    (4)
int efs_access(t_efs * efs, const char *pathname, int mode);

/**
%en * @brief change the current working directory 
%zh * @brief 改变当前工作目录。
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param path           path
 *
 * @return  -1: don't pass the permission checking  
 *           0: ok
 */
int efs_chdir(t_efs * efs, const char * path);

/**
%en * @brief get the information of current file or directory  
%zh * @brief 改变当前工作目录。
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param path           path
 *
 * @return  -1: don't pass the permission checking  
 *           0: ok
 */
int efs_status(t_efs_file * fp, struct stat * buf);

/**
%en * @brief modify the attribution of a file or a directory  
%zh * @brief 改变一个文件或者一个目录的属性。
%en_start
 * @details this function only is used for FAT32
%en_end
%zh_start
 * @details 这个函数只适用于FAT32
%zh_end
 *
 * @param efs            pointer to structure \ref t_efs
 * @param fname          path name
 * @param attr           attribution
 *
 * @return  -1: don't pass the permission checking  
 *           0: ok
 */
int efs_attr_set(t_efs * efs, char * fname,  unsigned char attr);

u32 efs_unicode_to_gbk(t_efs * efs, u16 uni_fname);
u32 efs_utf8_to_unicode(t_efs * efs, char * str, int * len);

//INTERNAL_START
/**
%en * @brief find a appointed file or directory  in file system
%zh * @brief 寻找一个指定的文件或目录.
 *
 * @param efs        pointer to structure \ref t_efs
 * @param filename   file or directory name, it could be MSDOS8.3 or long file name
 * @param loc        pointer to file location structure \ref t_efs_stat
 * @param last       previous directory
 *
 * @return 0         the file or directory doesn't exist
 *         1         the file exists
 *         2         the directory exists
 */
int dir_find_file(t_efs * efs, char * filename, t_efs_stat * loc, u32 * last);
int cache_read_spare(struct s_efs_nand_sparecache *sc, unsigned int row_adr, unsigned char * dst_adr, int size);
int cache_write_spare(struct s_efs_nand_sparecache *sc, unsigned int row_adr);
int efs_hw_nand_read(t_efs * efs, u32 address, u8* buf, int count);
int efs_hw_nand_write(t_efs * efs, u32 address, u8* buf, int count);

int efs_nand_init(t_efs * efs, t_efs_nand_badblock *bb, t_efs_nand_sparecache *sc);
//INTERNAL_END

/**
 * @REAL_FORMAT void EFS_DECLARE_WRITECACHE(char *name, u32 size); 
%en * @brief initialize writing cache
%zh * @brief 初始化writing cache. 
 *
 * @param name         writing cache name
 * @param size         writing cache size, it is multiple of 512
 * 
 * @return void
 * @ingroup lib_efs
 */
//INTERNAL_START
#define EFS_DECLARE_WRITECACHE(name, SIZE) \
static u8 _efs_wc_buf_##name[SIZE]; \
static struct s_efs_writecache name = { \
	.size = (SIZE)/512, \
	.buf = _efs_wc_buf_##name, \
};
//INTERNAL_END

#define EFS_CHNAME_EN(efs) \
do{\
	(efs)->ch_en = 1;\
	(efs)->cb_file_ch_fatname = file_ch_fatname;\
	(efs)->cb_dir_ch_lname2sname = dir_ch_lname2sname;\
}while(0)

//INTERNAL_START
extern t_efs_hw g_efs_hw_scif;
extern t_efs_hw g_efs_hw_nand;
extern t_efs_hw g_efs_hw_uhost;

//INTERNAL_END
#define EFS_HW_SCIF (&g_efs_hw_scif)
#define EFS_HW_NAND (&g_efs_hw_nand)
#define EFS_HW_UHOST (&g_efs_hw_uhost)


extern t_efs sh_efs;


/** @} */ //end lib_efs
#endif
