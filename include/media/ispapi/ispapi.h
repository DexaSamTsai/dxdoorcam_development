#include "includes.h"

// AEC/AGC control :  
// Exp: Numbers of exposured lines
// Gain: 0x10: 1x; 0x20: 2x ...
void ispapi_aecagc_control(u8 manual, u16 exp, u16 gain);

// Set max agc gain
void ispapi_set_maxagc(int max_gain);

// AEC Window Size Control: 
// Blank margin of left = left*image_width/128; others are in the same way.
void ispapi_set_aec_win_size(u8 left, u8 top, u8 width, u8 height);

void ispapi_set_aec_win_weight(u8 level[13]);

// Hue Control : Use CCM for hue correction
// Order: B G R
void ispapi_set_CCM(s16 value[9]) ;

// Denoise
// level 0, 1, 2, 3, 4
void ispapi_set_denoise(int level);

// Sharpness
u32 ispapi_set_sharpness(int level);

// ispapi_max_aec_control: Enable anti-flicker
// 1: Enable max AEC limitation
// 0: Disable max AEC limitation (default)
void ispapi_max_aec_control(u8 limit);

void ispapi_set_ytarget_width(u8 level);

// EDR
// level 0, 1, 2, 3, 4
void ispapi_set_edr(u8 level);
