#ifndef _TS_ENC_H_
#define _TS_ENC_H_

#define TS_PACKET_SIZE			188
#define ENCVIDEO_PES_PKT_HEAD_SIZE	8
#define PES_PTS_LEN				5
#define PES_DTS_LEN				5

#define AVC_COMPOSITION_OFFSET    0

/* pid & table ids P44 Table 2-26*/

#define PAT_PID                 0x0000	/*Program Assiciation Table*/
#define PAT_TID   0x00
#define PMT_TID   0x02

//FIX; only for get vlc high piority
#define DEFAULT_START_PID		/*0x0100*/0x102
#define DEFAULT_AUDIO_START_PID		/*0x0100*/0x101

/*set for option funcitons*/
#define DEFAULT_SID				0x0001	
#define DEFAULT_PMT_START_PID   0x0100

#define TS_SYNC_BYTE		0x47
#define TS_TSC_NOTSC		0x00 /*transport_scrambling_control,the adaptation field when present, shall not be scrambled*/


/*P32 Table 2-18*/
#define TS_STREAM_ID_VIDEO	0xe0
#define TS_STREAM_ID_AUDIO	0xc0

#define PES_PKT_START_CODE		0x000001
/*P48,Table 2-29  Stream type assignments*/
#define STREAM_TYPE_VIDEO_ENCVIDEO      0x1b
#define STREAM_TYPE_AUDIO_F4      0x0f       //AAC

#define TS_NOT_SCRAMBLED	0x00

/*we trans the pcr at this rate*/
//#define MUX_MAX_DELAY	 3000 /*one frame pcr, (1000 * 90000) /fps */
/*we retransmit the SI info at this rate  */
#define PAT_RETRANS_TIME 100	/*ms*/ 
#define PCR_RETRANS_TIME 20		/*ms*/

#define SCRAMLBE_CTRL_BIT		(6)
#define ADAPT_FIELD_CTRL_BIT	(4)
//typedef struct
//{
//	s32 pts;
//	s32 dts;
//	int size;
//	u8	*data;
//	enum CodecID id;
//}t_ts_stream;

typedef struct
{
	u8	*head;
	int used_cnt;
}t_ts_pkt;

typedef enum {
	Reserved = 0,
	PayLoad_Only,
	Apt_Only,
	Apt_Fed_PayLoad,
}e_afc; 		/*adaptation_field_control*/

typedef enum
{
	TYPE_TS = 0,
	TYPE_SEC,
	TYPE_PKT,
}mepgts_stream_type;

typedef struct
{
	int pid;
	int tid;
	int cc; /*continuity counter 0~f*/
	int cnt;
	int period;
}t_ts_sec;

typedef struct
{
	t_ts_sec 	pat;
	t_ts_sec    pmt;
	u32 			mux_rate;
	u32             cur_pcr;
	int 			pcr_period;
	u32				pcr_cnt;
	u32				delay;
	int 			stream_pid;
	int				stream_cc;
	u32 			start_tick;
}t_ts;

extern t_ts *ts; 

/*
 * brief: init some ts info
 *
 *	input : a pointer to struct t_ts_av ;
 *
 *	retutn NULL;
 */
#if 0
void libts_enc_start(int fps, int bit_rate );

/*
 * brief: get one PES TS packet
 *	input : 
 *		v_stream : pointer to struct t_ts_stream 
 *		is_start : if the v_stream->data is pointer to the video frame start data , then set it to 1 ,else 0
 *		pkt_buf  : mem space for put the PES TS Packet , at least 188 Bytes
 *	return:
 *		>= 0, payload len , that means how may video frame data have been encapture into this TS stream 
 *		< 0 : error
 */

int  libts_enc_get_pes_pkt(t_ts_stream *v_stream,int is_start,int frame_seq,u8 *pkt_buf);

/*
 *	brief: get ts section funcs
 */

void libts_enc_write_pmt( u8 *pkt_buf );

void libts_enc_write_pat( u8 *pkt_buf );

void libts_enc_insert_pcr( u8 *pkt_buf );
#endif
#endif
