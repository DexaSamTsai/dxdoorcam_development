#ifndef __ENCV_TS_H__
#define __ENCV_TS_H__

#define ENCV_TS_SYNC	// It is not necessary if single channel only.	

//#include "libts_enc.h"


/**
 * @defgroup libts
 * @ingroup lib_midware
 * @brief Library for encapsulate the encoded stream to TS for transferring by usb or wifi .<br><br>
 * -# <b>Source Codes</b>:
 * -# <b>Depends On</b>:
 * -# <b>Used By</b>:
 * -# <b>Design Doc</b>
 * -# <b>Sample codes</b>
\code
\endcode
 * -# <b>NOTES</b>
 * -# <b>FAQ</b>
 * @{
 */
//INTERNAL_START

#include "libts_enc.h"

typedef enum TS_SEG_FLG
{
	SEG_NULL,
	SEG_START,
	SEG_MID,
	SEG_END,
}ts_seg_flag_e;

typedef struct
{
	int vfrm_len;		// video frame length
	int vfrm_offset;	// video frame offset
	int vfrm_num;		// video frame number
	int afrm_num;		// audio frame number
	int payload_len;	// packet payload size except header

	u32 mux_rate;		// mux rate

	u32 pat_pkt_cnt;	// pat packet count
	u32 pat_pkt_period;	// pat packet period
	int pat_cc;			// pat continuity counter
	int pmt_cc;			// pmt continuity conunter
	int cc;				// ts header continuity counter
	int audio_cc;				// ts header continuity counter

	int start;			// frame start
	int add_seperate;	// add seperate flag

#ifdef ENCV_TS_SYNC
	u32 dts;			// decode time stamp
	u32 pts;			// presentation time stamp

	u32 pcr;			// program clock reference
	u32 pcr_pkt_cnt;	// pcr packet count
	u32 pcr_pkt_period;	// pcr_packet_period

	int start_tick;		// start tick
	u32 delay;			// for dts, pts
#endif
}t_encv_ts;

//static t_encv_ts g_encvts_cfg;

typedef struct LIBTS_CFG
{
/*in*/
	int vs_id;   //video channel id
	
	int seg_intval;  //segment interval. 1unit is 10ms. If one segment is 5s, should be set 500.
	int frmrate;     //frame rate
	int bitrate;     //bitrate
	u8 tspkt_cnt; //count of ts packet.
	
/*out*/
	int seg_flag;    //segment flag 
	u32 frm_ts;      //to get ts of current frame
	u32 frm_flag;      //to get frame info , first frame or last frame or NODATA

/*private */
	u32 last_ts; //last segment ts
	u32 last_frm_ts; //last frame ts
	t_encv_ts encvts; //internal parameters
	
	FRAME *vpfrm;
	FRAME *apfrm;
	FRAME *pfrm;
	u8 frm_audio;
	u8 fill_done;

	u8 *tsbuf;
	u8 tsbuf_start;
	u8 tsbuf_num;
	int afrm_num;		// audio frame number
	u32 vstart_ts;		
	u32 astart_ts;		
}t_ts_cfg;

/**
 * @REAL_FORMAT #define LIBTS_DECLARE_START(_ts_id, _pkt_cnt)
 * @brief <b>Declare start of a ts stream.</b>
 * @details A ts stream should be declared firstly before used as param of libts_init(_ts_id). Once declared, _ts_id should be regarded as a global variable of t_ts_cfg type.<br>
 *
 * @param _ts_id, ts stream id. Can be used in libts_init and libts_get_data as first parameter. <br>
 * @param _pkt_cnt, Packet count of current ts stream. One pakcet size is 188. When LIBTS_DECLARE_START have been called. A ts stream buffer will be allocated, the size of buffer is TS_PACKET_SIZE * _pkt_cnt + 10. <br>
 *
 */
#define LIBTS_DECLARE_START(_ts_id, _pkt_cnt)\
static u8 tsbuf_##_ts_id[TS_PACKET_SIZE*_pkt_cnt + 10];\
t_ts_cfg g_tscfg_##_ts_id = \
{\
	.tsbuf = tsbuf_##_ts_id,	\
	.tspkt_cnt = _pkt_cnt,


/**
 * @REAL_FORMAT #define LIBTS_DECLARE_END(_ts_id)
 * @brief <b>Declare end of a ts stream.</b>
 * @details A ts stream should be declared firstly before used as param of libts_init(_ts_id). Once declared, _ts_id should be regarded as a global variable of t_ts_cfg type.<br>
 *
 * @param _ts_id, ts stream id. Should be same with LIBTS_DECLARE_START  <br>
 *
 */

#define LIBTS_DECLARE_END(_ts_id)\
};\
t_ts_cfg *_ts_id = &g_tscfg_##_ts_id;\

//INTERNAL_END

/**
 * @brief initialize TS's encapsulation
 * 
 * @param void
 * @reutrn none
 */

void libts_init(t_ts_cfg *ts_cfg);

/**
 * @brief initialize TS's encapsulation
 * 
   @param ts_cfg[in] t_ts_cfg point 
 * @param buf[in] input buffer, if return > 0, then buffer will be filled by ts stream. 
 * @param maxlen[in] input buffer size, max len can be got. 
 * @reutrn <0 err nodata 
 * @reutrn >0 get data ok, return value is data size.
 * @reutrn =0, means get a segment end flag.
 				ts_cfg->seg_flag is SEG_END now.
 */
int libts_get_data(t_ts_cfg *ts_cfg, char *buf, int max_len);


#endif
