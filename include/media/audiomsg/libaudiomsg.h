#ifndef _LIBAUDIOMSG_H_
#define _LIBAUDIOMSG_H_

typedef struct s_libaudiomsg_cfg
{
	int (*found_cb)(unsigned char * buf, int len);
}t_libaudiomsg_cfg;

extern t_libaudiomsg_cfg g_libaudiomsg_cfg;

void libaudiomsg_init(void);
int libaudiomsg_handle(short * in, int len);

#endif
