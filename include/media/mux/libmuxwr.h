#ifndef _LIBMUXWR_H_
#define _LIBMUXWR_H_

typedef enum
{
	MUX_FMT_AUTO,
	MUX_FMT_RAW,
	MUX_FMT_MP4,
}e_libmux_fmt;

struct s_libmuxwr_cfg;
typedef struct s_libmux_cfg
{
	int (*header_write)(struct s_libmuxwr_cfg * cfg);
	int (*frame_write)(struct s_libmuxwr_cfg * cfg, FRAME * frame);
	int (*tail_write)(struct s_libmuxwr_cfg * cfg);
}t_libmux_cfg;

typedef struct s_libmuxwr_buf
{
	//public config
	u8 * buf; //if it's NULL and idx_buf_size is not 0, will do malloc
	u32 size; //idx buffer size.
	u8 use_file; //size = 0, use_file = 0: not allowed
					 //size = 0, use_file = 1: fp will be used.
					 //size = 1, use_file = 0: only buf will be used. no fp.
					 //size = 1, use_file = 1: fp will be created if buf full.
	//private
	u8 buf_fromheap;
	u32 used;
	u32 total_len;
	FILE * fp;
	char fname[16]; //0.tmp, ..., 99.tmp
}t_libmuxwr_buf;

typedef enum
{
	MUX_STREAM_NONE,
	MUX_VIDEO_ENCV, 
	MUX_VIDEO_MIMG,
	MUX_AUDIO_PCM_S16LE,
	MUX_AUDIO_ADPCM_IMA,
	MUX_AUDIO_AF4,
}MUX_STREAM_TYPE;

#define MUX_AUDIO_START MUX_AUDIO_PCM_S16LE

typedef struct{
	///< audio only
	int sample_rate; ///< samples per second
	int channels; ///< number of audio channel
	int bps;	  ///< bit per sample
}t_aparam;

typedef struct{
	///< video only
	int width;
	int height;
	int frame_rate;
}t_vparam;

typedef struct s_libmuxwr_cfg
{
	//public setting
	e_libmux_fmt fmt; //0 for auto detect, so it's not required to set this
	//some muxer, need the stream informations to generate the header.
	MUX_STREAM_TYPE vtype;       ///< video type. set by APP, used by libmux_xxx
	MUX_STREAM_TYPE atype;       ///< audio type. set by APP, used by libmux_xxx
	//buf config. some muxer need buffer to improve performance
	t_libmuxwr_buf idx; //idx buf config
	t_libmuxwr_buf tail; //tail buf config

	//public status
	u32 total_vframecnt;
	u32 total_aframecnt;

	//private
	const t_libmux_cfg * mux_cfg;
	FILE * fp;

	void * mux_private; //used for mux private arg, malloced in mux_cfg->header_write
	t_libmuxwr_buf tmpbuf;
	t_libmuxwr_buf * cur;

	t_vparam vparam;
	t_aparam aparam;
}t_libmuxwr_cfg;

//return 0: ok.
int libmuxwr_open(t_libmuxwr_cfg * cfg, char * filename);

//return 0: ok.
int libmuxwr_write_frame(t_libmuxwr_cfg * cfg, FRAME * frame);

//return 0: ok.
int libmuxwr_close(t_libmuxwr_cfg * cfg);

//INTERNAL_START
int muxwr_buf_init(t_libmuxwr_buf * buf);
int muxwr_buf_addstr(t_libmuxwr_buf * buf, u8 * str, u32 len);
unsigned char * muxwr_buf_read(t_libmuxwr_buf * buf, int offset, int size);
int muxwr_buf_write(t_libmuxwr_buf * buf, u8 * str, int offset, int size);
int muxwr_buf_free(t_libmuxwr_buf * buf, u8 * str);
int muxwr_buf_flush(t_libmuxwr_buf * buf);
void muxwr_buf_error_exit(t_libmuxwr_buf * buf);
//INTERNAL_END


#endif
