#ifndef _LIBDATAPATH_H_
#define _LIBDATAPATH_H_

int libdatapath_init(int dpcfg_id);

int libdatapath_start(void);

int libdatapath_stop(void);

int libdatapath_exit(void);

int libdatapath_fastinit(int dpcfg_id);

int libdatapath_faststart(void);

/**
 * @brief check libdatapath fastboot status
 * @return FAST_STAGE_XXX
 */
int libdatapath_fast_status(void);

/**
 * @brief setup the fps2 fps
 */
int libdatapath_fast_fps2fps(int fps);

int libdatapath_fast_fps2start(void);

int libdatapath_fast_simu(int dpcfg_id);

#endif
