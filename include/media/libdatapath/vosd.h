/** 
 * @file    R3_fw/libmpu_linux/include/vosd.h
 * @brief   headers for VOSD routines
 *
 * This header contains all VOSD(Video On-Screen Display) related routine to interface with MPU.
 *
 * @date	2016/12/14
 */
/** 
 * @defgroup        libvosd	VOSD API Group
 */
#ifndef __VIDEO_OSD_H__
#define __VIDEO_OSD_H__

typedef struct s_osd4l_win
{
	u8 win_no;
//INTERNAL_START
	u8 mem_id;
	u8 reserved1;
	u8 padding0;
//INTERNAL_END
	u16 win_width;
	u16 win_height;
	u16 win_hstart;
	u16 win_vstart;
	u32 win_memaddr;
	u32 win_max_offset;
	u32 color[4]; ///< (4LUT):osd color value. 0---23: is YUV420 color.  24--32 is transparence, option is 0--7. 0: indicates transparent and display backgroud layer, 7: indicates opaque and display menu osd layer.
}t_osd4l_win;

typedef struct s_osd4l_cfg
{
	t_osd4l_win win;
}t_osd4l_cfg;

typedef struct s_osd4l_timestr
{
	char timestr[24]; ///<buffer for current date string.
	u32 prev_ticks;
}t_osd4l_timestr;

/**
 * @brief				Get the current time string.
 * 
 * @param[out]	timestr	the current time string. Format: YYYY-MM-DD HH:MM:SS
 * @retval		1		OK.
 * @retval		0		failed
 * @ingroup				libvosd
 */
int libosd4l_timestr_get(t_osd4l_timestr * timestr);

typedef struct s_osd4l_drawled_cfg
{
	u8 font_color;
	u8 bk_color;  //background color should be the same with the color of window
	u16 fontsize; //0:small, for VGA resolution.  1:medium, for 720P resolution. 2:large, for 1080P resolution.
}t_osd4l_drawled_cfg;

/**
 * @brief				Draw the string of time stamp in SRAM.
 * @details				ARM is responsible for drawing the string.
 * 
 * @param[in]	cfg		window structure of OSD4L
 * @param[in]	drawled_cfg	the parameters of LED font
 * @param[in]	str		time string
 * @retval		1		OK.
 * @retval		0		failed
 * @ingroup				libvosd
 */
int libosd4l_drawled(t_osd4l_cfg * cfg, t_osd4l_drawled_cfg * drawled_cfg, char * str);

///< match with mpu/include/libdatapath/dpm_osdrlc.h
typedef struct s_osdrlc_res_item
{
//INTERNAL_START
	u8 win_no;
	u8 mem_id;
	u8 reserved0;
	u8 padding0;
//INTERNAL_END
	u16 width;
	u16 height;
	u16 hstart;   ///< for R3 new, BIT15: 1-->window start x is out of background picture, 0-->window start x is in picture.BIT0---BIT14 for start address
	u16 vstart;   ///< for R3 new, BIT15: 1-->window start y is out of background picture, 0-->window start y is in picture.BIT0---BIT14 for start address
//INTERNAL_START
	u32 reserved1;
	u32 reserved2;
//INTERNAL_END
	u32 size;
	u32 color[8];     ///< (8LUT):[23--16]Y,[15--8]U,[7--0]V
	u32 offset;
}t_osdrlc_res_item;

struct s_osdrlc_cfg;
typedef struct s_osdrlc_resload_hw
{
	int (*cb_osdrlc_resitem_load)(struct s_osdrlc_cfg * cfg, u32 item_no);
	int (*cb_osdrlc_res_load)(struct s_osdrlc_cfg * cfg, int group);
}t_osdrlc_resload_hw;

typedef struct s_osdrlc_res
{
	unsigned int item_count;
	unsigned int offset_base;
	struct s_osdrlc_resload_hw * hw;
	struct s_osdrlc_res_item * item;
}t_osdrlc_res;

typedef struct s_osdrlc_cfg
{
	void * arg;
	u32 mem_base;
	t_osdrlc_res * osdrlc_res;
	int (*cb_osdrlc_fill)(struct s_osdrlc_cfg * cfg, u32 item_no, u8 win_no, u32 offset);
}t_osdrlc_cfg;

extern t_osdrlc_resload_hw g_resload_scif;

#define OSDRLC_RES_LOAD_SCIF  (&g_resload_scif)

#define LIBOSDRLC_RES_DECLARE(resnum, hwname) \
static t_osdrlc_res_item _osdrlc_resitems[resnum]; \
t_osdrlc_res g_osdrlc_res =  \
{ \
	.item_count = resnum, \
	.offset_base = 0, \
	.hw = hwname,\
	.item = &_osdrlc_resitems,\
};


/**
 * @brief				Get the window structure of OSD4L from MPU.
 * 
 * @param[out]	cfg		data structure of OSD4L window
 * @param[in]	vs_id	vstream ID. For the single stream, ID shoule be 0. For the multi-streams, get the correct ID from mpu/ram/full/xxx.config
 * @param[in]	win_no	window number
 * @retval		>0		OK.
 * @retval		0		failed.
 * @ingroup				libvosd
 */
int libosd4l_get(t_osd4l_cfg * cfg, u8 vs_id, u8 win_no);

/**
 * @brief				Update the window parameters of OSD4L to MPU.
 * 
 * @param[in]	cfg		data structure of OSD4L window
 * @param[in]	vs_id	vstream ID. For the single stream, ID shoule be 0. For the multi-streams, get the correct ID from mpu/ram/full/xxx.config
 *
 * @retval		>=0		OK.
 * @retval		<0		failed
 * @ingroup				libvosd
 */
int libosd4l_update(t_osd4l_cfg * cfg, u8 vs_id);

/**
 * @brief				Fill OSD4L memory with 2bit data of each pixel
 *
 * @param[in]   hw_arg  pointer to t_osd4l_cfg * cfg 
 * @param[in]	x		horizontal position of pixel
 * @param[in]	y		vertical position of pixel
 * @param[in]	color	the color of pixel.format is 2bit, that is, value is 0-1-2-3 
 * @ingroup				libvosd
 */
int libosd4l_gui_set_pixel(void * hw_arg, int x, int y, u32 color);

/**
 * @brief				load resource from external storage
 * 
 * @param[in]	cfg		data structure of RLC window
 * @param[in]	group_no	the group number of resource 
 *
 * @retval		>0		OK.
 * @retval		<0		failed.
 * @ingroup				libvosd
 */
int libosdrlc_res_load(t_osdrlc_cfg *cfg, u32 group_no);

/**
 * @brief				Get the window structure of OSDRLC from MPU.
 * 
 * @param[out]	cfg		data structure of OSDRLC window
 * @param[in]	vs_id	vstream ID. For the single stream, ID shoule be 0. For the multi-streams, get the correct ID from mpu/ram/full/xxx.config
 * @param[in]	win_no	window number
 *
 * @retval		>0		OK.
 * @retval		0		failed.
 * @ingroup				libvosd
 */
int libosdrlc_get(t_osdrlc_cfg * cfg, u8 vs_id, u8 win_no);

/**
 * @brief				Fill the resource data from external storage to DDR
 * 
 * @param[in]	cfg		data structure of OSDRLC window
 * @param[in]	item_no	number of resource item
 * @param[in]	win_no	window number
 * @param[in]	offset	window position (window_hstart<<16)|(window_vstart)
 * @retval		>=0		OK.
 * @retval		<0		failed
 * @retval				libvosd
 */
int libosdrlc_fill(t_osdrlc_cfg * cfg, u32 item_no, u8 win_no, u32 offset);

/**
 * @brief				update the window parameters of OSDRLC to MPU.
 * 
 * @param[in]	cfg		data structure of OSDRLC window
 * @param[in]	vs_id	vstream ID. For the single stream, ID shoule be 0. For the multi-streams, get the correct ID from mpu/ram/full/xxx.config
 * @param[in]	item_no	the number of resource item
 * 
 * @retval		>=0		OK.
 * @retval		<0		failed
 * @ingroup				libvosd
 */
int libosdrlc_update(t_osdrlc_cfg * cfg, u8 vs_id, u32 item_no);
#endif ///__VIDEO_OSD_H__
