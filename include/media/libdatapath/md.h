/** 
 * @file    R3_fw/libmpu_linux/include/md.h
 * @brief   headers for MD(Motion Detection) routines
 *
 * This header contains all MD(Motion Detection) related routine to interface with MPU.
 *
 *
 * @date	2016/12/14
 */
/** 
 * @defgroup        libmd	MD API Group
 */
#ifndef __DATAPATHA_MD_H_
#define __DATAPATHA_MD_H_ 
#include "libmd_common.h"

#define DEFAULT_MD_TIMER 500 //if no motion detect for 5s then set md_sta to 0
#define MAX_MD_WINDOW 8

typedef struct s_libmd_cfg
{
	u8 sta;								///< BIT7-BIT0 = window7-window0.  1 = motion, 0 = still 
	t_md_win_cfg window[MAX_MD_WINDOW];	///< can support 8 windows as most.
}t_libmd_cfg;


/**
 * @brief					libmotion detect config window. 
 * 
 * @param[in]	md_id		md_id. Now only support one md channel. 
 * @param[in]	wind_num	window num. 
 * @param[in]	*win_cfg	window config param. Include window size area and threshold enable info.
 * @retval		0			configure success.
 * @retval		!=0			configure fail.
 * @ingroup					libmd
 */
u32 libmd_cfg(int md_id, int wind_num, t_md_win_cfg *win_cfg);

/**
 * @brief					libmotion detect get status. 
 * 
 * @param[in]	md_id		md_id. Now only support one md channel. 
 * @param[in]	*md_cfg		current configure struct of md 
 * @return					status of current md.  BIT7-BIT0 = window7-window0.  1 = motion, 0 = still 
 * @ingroup				libmd
 */
u32 libmd_get_status(int md_id, t_libmd_cfg *md_cfg);

/**
 * @brief					libmotion detect set window. 
 * 
 * @param[in]	md_id		md_id. Now only support one md channel. 
 * @param[in]	wind_cnt	window of current md. BIT7-BIT0 = window7-window0. 1 = enable, 0 = disable
 * @return					0 : set success. !=0 set fail.
 * @ingroup				libmd
 */
u32 libmd_wndcnt_set(int md_id, int wind_cnt);

/**
 * @brief                   get the md buf
 * @details					if Video Analysis is not started, this API is non block, the 'timeout' is not used. If Video Analysis is started, please call video_analysis_get_buf() API instead.
 *
 * @param[in]	md_id		md_id. Now only support one md channel. 
 * @param[in]  timeout      not used if video analysis is not started.
 * @return                  pointer to the buffer structure. NULL if not ready.
 * @ingroup				libmd
 */
t_md_algo_buf * libmd_get_buf(int md_id, u32 timeout);

#endif
