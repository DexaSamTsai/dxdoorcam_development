/** 
 * @file    R3_fw/libmpu_linux/include/libvs.h
 * @brief   headers for video routines
 *
 * @details	This header contains all Video related routine to interface with MPU.
 *
 * Video Encoder Rate Control API instruction:
 *
 * 1. Constant bit rate control: libvs_set_bitrate(int vs_id, u32 bitrate);
 *
 * 2. Variant bit rate control: VBR is realised by limiting the encoding QP on the base of CBR control. 
 * The lower the QP is, the better the video quality will be, together with higher the bitrate. Vice versa. 
 * There are two cases for VBR:
 *    -# Make the actual bps lower than the upper limit when there is little motion or simple scene.
 *        -# call "libvs_set_min_qp(int vs_id, u32 min_qp)" to set minimum QP(no need to set maximum QP).
 *        -# call "libvs_get_min_qp(int vs_id)" to get the current minimum QP.
 *        -# to switch back to CBR after config minimum QP, call libvs_set_min_qp(vs_id, 0). 
 *    -# Make the actual bps higher than the upper limit to maintain a reasonable quality in violent motion or complicated
 *        scene: 
 *        -# call "libvs_set_max_qp(int vs_id, u32 max_qp)" to set maximum QP(no need to set minimum QP).
 *        -# call "libvs_get_max_qp(int vs_id)" to get the current miaximum QP.
 *        -# to switch back to CBR after config maximum QP, call libvs_set_max_qp(vs_id, 0). 
 *    - NOTE: 
 *        -# Please configure bitrate upper limit first (calling "libvs_set_bitrate(int vs_id, u32 bitrate)") before setting QP limit.
 *        -# Normally, please keep (p_qp - i_qp) > 0. e.g. Set minimum i_qp set to 20 and minimum p_qp set to 22, or maximum i_qp set to 38 and maximum p_qp set to 40.
 * 
 * 3. GOP: libvs_set_gop(int vs_id, u32 gop_size);
 *
 * 4. Setting the differentce between I QP and P QP: libvs_set_qpdelta(int vs_id, u32 qp_delta);
 *   I frame as reference need lower QP than P frame. Increase the QP difference between I frame and P frame may help to 
 *   lower the bitrate and improve quality, but might increase the variance of the bitrate. By default the difference is 1. 
 *   It is recommended to set to 2 ~ 3.
 *
 * @date	2016/12/14
 */
/** 
 * @defgroup		libvs	Video API Group
 */
#ifndef _LIBVS_H_
#define _LIBVS_H_

#include "frame.h"
#include "libvs_common.h"

#define VS_STATUS_IDLE		0
#define VS_STATUS_RUNNING	1   
#define VS_STATUS_STOPPED	2
#define VS_FRMBUF_CNT		8

//INTERNAL_START
/**
 * @struct		s_videostream
 * @details		Structure of video stream
 * @ingroup		libvs
 */
typedef struct s_videostream
{
	int status;				///< IDLE/RUNNING/STOPPED
	t_ertos_eventhandler notempty_evt;

	//for get_data. MPU only provide get_frame.
	int lastoffset;
	int lasttotlen;
	FRAME *cur_frm;

	char * last_addrs[VS_FRMBUF_CNT];
	FRAME * last_frms[VS_FRMBUF_CNT];
	int last_cnt;
}t_videostream;
//INTERNAL_END

typedef struct _VIDEO_DATA
{
    u8 *pAddr;
    u32 thisLen;
    u32 frameLen;
    u32 frameType;
    u32 timeStamp;

    u8 *pRel;
}VIDEO_DATA;

/**************** The video stream init/start/stop/exit API *****************/
/**
 * @brief				init video stream
 * @details				must call before libvs_start();
 * @param[in]	vs_id	video stream id
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_init(int vs_id);

/**
 * @brief				start video stream
 * @param[in]	vs_id	video stream id
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_start(int vs_id);

/**
 * @brief				stop video stream
 * @details				after stop, the previous video frames still available.
 * @param[in]	vs_id	video stream id
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_stop(int vs_id);

/**
 * @brief				exit video stream
 * @details				after exit, the previous video frames will be dropped. must call after libvs_stop()
 * @param[in] 	vs_id	video stream id
 * @retval      0       OK
 * @retval      !=0     ERROR
 * @ingroup             libvs
 */
int libvs_exit(int vs_id);

/**************** The video stream data API *****************/
/**
 * @brief				get one frame from video stream
 * @details				get one frame. The frame must be released by libvs_remove_frame(), otherwise will cause memory leakage.
 * @param[in]	vs_id	video stream id
 * @param[in]	timeout	timeout in ticks(10ms).
 * @return				FRAME
 * @retval      NULL	timeout
 * @ingroup             libvs
 */
FRAME * libvs_get_frame(int vs_id, u32 timeout);

/**
 * @brief				release one frame from video stream
 * @param[in]	vs_id	video stream id
 * @param[in]	frame	frame to be removed
 * @retval		0		OK
 * @retval		!=0		FAILED
 * @ingroup             libvs
 */
int libvs_remove_frame(int vs_id, FRAME * frame);

/* for libvs_get_data */
#define VDATA_FLAG_FIRSTPKT	BIT0
#define VDATA_FLAG_IDR		BIT1
#define VDATA_FLAG_I		BIT2
#define VDATA_FLAG_LASTPKT	BIT3
#define VDATA_FLAG_IMG      BIT4
#define VDATA_FLAG_NODATA   BIT5

/**
 * @brief				get data from video encoder
 * @details				this func get data from video encoder. Should call libvs_remove_data to remove video frame when get LASTPKT of current frame. 
 * @param[in]	vs_id	input the video stream channel 
 * @param[out]	buf		output the buf address
 * @param[in]	maxlen	input the max length to get
 * @param[out]	flags	VDATA_FLAG_FIRSTPKT for first packet of one frame
 *						VDATA_FLAG_LASTPKT for last packet of current frame 
 *						VDATA_FLAG_IDR for IDR frame.
 *						VDATA_FLAG_I for I frame.
 * @param[out]	frm_len	get the frame size of current frame if this packet is the first packet of current frame.
 *							if current packet is not first packet ,this param will be 0 
 * @param[out]	ts		get the ts of current frame
 * @param[in]	timeout	input the timeout for get video frame from BA22.
 * @return				return the length of packet available.
 * @ingroup				libvs
 *
 */
int libvs_get_data(int vs_id, char **buf, int maxlen, u32 *flags, u32 *frm_len, u32 *ts, u32 timeout);

/**
 * @brief				release data from video encoder
 * @details				this func release data from video encoder. Should be called when get LASTPKT of current frame to remove current frame.
 * @param[in]	vs_id	input the video stream channel 
 * @param[in]	addr	data addr. 
 * @retval		0		release data ok
 * @ingroup				libvs
 *
 */
int libvs_remove_data(int vs_id, char *addr);

int video_get_data(int vs_id, VIDEO_DATA *p_data, u32 maxlen, u32 timeout);

int video_remove_data(int vs_id, u8 *p_rel);

/**************** The video stream ioctl API *****************/

//INTERNAL_START
/**
 * @brief				set the vs
 * @param[in]	vs_id	video stream id
 * @param[in]	request	VS_IOC, see mpu/export_include/libvs_common.h for detailed
 * @param[in]	arg		see mpu/export_include/libvs_common.h for detailed
 * @return				see mpu/export_include/libvs_common.h for detailed
 * @ingroup             libvs
 */
int libvs_ioctl(int vs_id, int request, void * arg);
//INTERNAL_END

/**
 * @brief				set framerate
 * @param[in]	vs_id	video stream id
 * @param		framerate	framerate to set
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_framerate(int vs_id, u32 framerate);

/**
 * @brief				set video stream framerate, this function must be called after libvs_init and before libvs_exit
 * @details				sensor input frame rate no change, encoder evenly drop frames to change VS framerate
 * @param[in]	vs_id	video stream id
 * @param[in]	framerate	VS output frame rate, 0 ~ sensor_fps, sensor_fps should be smaller than 32 
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_vs_framerate(int vs_id, u32 framerate);

/**
 * @brief				get framerate
 * @param[in]	vs_id	video stream id
 * @return				framerate
 * @ingroup             libvs
 */
int libvs_get_framerate(int vs_id);

/**
 * @brief				set crop coordinate, call this function before libvs_start is called or after libvs_stop return 0 
 * @param[in]	vs_id	video stream id
 * @param		coordinate	(x<<16)|y.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_crop_coordinate(int vs_id, u32 coordinate);

/**
 * @brief				set crop size, call this function before libvs_start is called or after libvs_stop return 0 
 * @param[in]	vs_id	video stream id
 * @param		cropsize	(width<<16)|height. The mininum cropsize is 64*64;
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_crop_size(int vs_id, u32 cropsize);
			
/**
 * @brief				get the resolution of frame
 * @param[in]	vs_id	video stream id
 * @return 				(width<<16)|height.
 * @retval		<=0		ERROR
 * @ingroup             libvs
 */
int libvs_get_resolution(int vs_id);

/**
 * @brief				set the resolution of frame, call this function before libvs_start is called or after libvs_stop return 0
 * @param[in]	vs_id	video stream id
 * @param[in]	resolution	(width<<16)|height
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_resolution(int vs_id, u32 resolution);

/**
 * @brief				set light freq of stream
 * @param[in]	vs_id	video stream id
 * @param[in]	lightfreq	0:disable. 1:50hz. 2:60hz
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_lightfreq(int vs_id, u32 lightfreq);

/**
 * @brief				set VS brightness level
 * @param[in]	vs_id	video stream id
 * @param[in]	brightness VS brightness level (1~255, default is 128)
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup				libvs
 */
int libvs_set_brightness(int vs_id, u32 brightness);

/**
 * @brief					Set VS ISP SDE
 * @details					Use this API to assign Special Digital Effect in ISP configuration
 * @param[in]	vs_id		video stream id
 * @param[in]	sde_effect_No	SDE Effect Number
 * @retval		0			OK
 * @retval		!=0			ERROR
 * @ingroup					libvs
 */
int libvs_set_sde(int vs_id, int sde_effect_No);
/**
 * @brief					Set VS ISP day/night Mode
 * @details					Use this routine to switch between day/night/auto mode
 * @param[in]	vs_id		video stream id
 * @param[in]	daynight	0: Day; 1: Night; 2: Auto
 * @retval		0			OK
 * @retval		!=0			ERROR
 * @ingroup					libvs
 */
int libvs_set_daynight(int vs_id, int daynight);

/**
 * @brief					Set VS ISP exposure 
 * @param[in]	vs_id		video stream id
 * @param[in]	exp			ISP exposure
 * @retval		0			OK
 * @retval		!=0			ERROR
 * @ingroup					libvs
 */
int libvs_set_exp(int vs_id, u32 exp);

/**
 * @brief					Set VS ISP gain 
 * @param[in]	vs_id		video stream id
 * @param[in]	gain		ISP gain
 * @retval		0			OK
 * @retval		!=0			ERROR
 * @ingroup					libvs
 */
int libvs_set_gain(int vs_id, u32 gain);

/**
 * @brief				set video mirror
 * @param[in]	vs_id	video stream id
 * @param[in]	mirror	0:no mirror, 1:mirror
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup				libvs
 */
int libvs_set_mirror(int vs_id, u32 mirror);

/**
 * @brief				set video flip
 * @param[in]	vs_id	video stream id
 * @param[in]	flip	0:no flip. 1:flip.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup				libvs
 */
int libvs_set_flip(int vs_id, u32 flip);

/**
 * @brief				set video mirror and flip
 * @param[in]	vs_id	video stream id
 * @param[in]	mirror_flip	0:no mirror and flip. 1:mirror and flip.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_mirror_flip(int vs_id, u32 mirror_flip);

/**
 * @brief				set contrast
 * @param[in]	vs_id	video stream id
 * @param[in]	contrast (0~4, default is 2)
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_contrast(int vs_id, u32 contrast);

/**
 * @brief				set saturation
 * @param[in]	vs_id	video stream id
 * @param[in]	saturation (0~7, default is 3)
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_saturation(int vs_id, u32 saturation);

/**
 * @brief				get video stream type
 * @param[in]	vs_id	video stream id
 * @retval		<0		vs_not_exist
 * @retval		0		others
 * @retval		1		ve
 * @retval		2		img
 * @ingroup             libvs
 */
int libvs_get_type(int vs_id);

/**
 * @brief				force video stream to encode an I frame
 * @param[in]	vs_id	video stream id
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_force_iframe(int vs_id);

/**
 * @brief				set bitrate, CBR
 * @param[in]	vs_id	video stream id
 * @param[in]	bitrate	bits per second.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_bitrate(int vs_id, u32 bitrate);

/**
 * @brief				set video GOP size, recommended size: frame_rate ~ frame_rate * 2
 * @param[in]	vs_id	video stream id
 * @param[in]	gop_size	gop size
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_gop(int vs_id, u32 gop_size);

/**
 * @brief				set video GOP duration, recommended size: frame_rate ~ frame_rate * 2
 * @param[in]	vs_id	video stream id
 * @param[in]	gop_duration	duration_time(seconde) * 30, e.g. 60 for 2 secondes, 45 for 1.5 secondes
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_gop_duration(int vs_id, u32 gop_duration);

/**
 * @brief				set initial video GOP size for the first frames, after (init_gop_num*init_gop_size) frames, vs will switch to normal gop size
 * @param[in]	vs_id	video stream id
 * @param[in]	init_gop	(init_gop_num<<16)|init_gop_size
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_init_gop(int vs_id, u32 init_gop);

/**
 * @brief				set minimum QP of I frame and P frame, for VBR control, make bitrate lower than configured value when scene is simple and motion is little.
 * @param[in]	vs_id	video stream id
 * @param[in]	min_qp	(i_qp<<16)|p_qp. qp range from 1 ~ 51, recommended iqp: 20~26, pqp: 22~30, (pqp - iqp): 2 ~ 6 
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_min_qp(int vs_id, u32 min_qp);

/**
 * @brief				get minimum QP of I frame and P frame, if VS is not enabled, the returned value is not valid.
 * @param[in]	vs_id	video stream id
 * @return				(i_qp<<16)|p_qp.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_get_min_qp(int vs_id);

/**
 * @brief				set maximum QP of I frame and P frame, for VBR control, bitrate may exceed configured value to maintain a reasonable quality.
 * @param[in]	vs_id	video stream id
 * @param[in]	max_qp	(i_qp<<16)|p_qp. qp range from 1 ~ 51, recommended iqp > 34, pqp > 38, (pqp - iqp): 2 ~ 6
 * @retval		0		ERROR
 * @ingroup             libvs
 */
int libvs_set_max_qp(int vs_id, u32 max_qp);

/**
 * @brief				get maximum QP of I frame and P frame, if VS is not enabled, the returned value is not valid.
 * @param[in]	vs_id	video stream id
 * @return				(i_qp<<16)|p_qp. 
 * @retval		0		ERROR
 * @ingroup             libvs
 */
int libvs_get_max_qp(int vs_id);

/**
 * @brief				set the QP difference between I frame and P frame 
 * @param[in]	vs_id	video stream id
 * @param[in]	qp_delta	qp_delta = (PQP - IQP), range from 1 ~ 12, recommended 2 ~ 4,  higher value is recommended when bitrate is lower
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_qpdelta(int vs_id, u32 qp_delta);

/**
 * @brief				set H.264 profile, has to be called before libvs_start is called or after libvs_stop return 0 
 * @param[in]	vs_id	video stream id
 * @param[in]	video_profile	100: high profile, 77: main profile, 66: base line.
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_vprofile(int vs_id, u32 video_profile);

/**
 * @brief				get current vs time stamp 
 * @param[in]	vs_id	video stream id
 * @return				timestamp in minisecond.
 * @retval		0		ERROR
 * @ingroup             libvs
 */
int libvs_get_timestamp(int vs_id);

/**
 * @brief				get vs sps/pps data 
 * @param[in]	vs_id	video stream id
 * @return				sps/pps buffer base addr
 * @ingroup             libvs
 */
int libvs_get_sps_pps(int vs_id);

/**
 * @brief				enable color of background of OSD
 * @param[in]	vs_id	video stream id
 * @param[in]	win_no	window number
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_osd_enable(int vs_id, int win_no);

/**
 * @brief				disable color of background of OSD
 * @param[in]	vs_id	video stream id
 * @param[in]	win_no	window number
 * @retval		0		OK
 * @retval 		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_osd_disable(int vs_id, int win_no);

/**
 * @brief				get number of vs lost frames 
 * @param[in]	vs_id	video stream id
 * @return				number of frames that has been dropped after libvs has been started
 * @ingroup             libvs
 */
int libvs_get_dropped_frm_num(int vs_id);

 /**
 * @brief enable.disable meta info in video 
 * @param[in]	vs_id	video stream id
 * @param[in]	enable	enable/disable meta info
 * @return		0
 * @ingroup				libvs
 */
int libvs_enable_meta(int vs_id, int enable);

/**
 * @brief				get number of frames in current VS buffer 
 * @param[in]	vs_id	video stream id
 * @return				the total number of frames in current buffer 
 * @ingroup             libvs
 */
int libvs_get_buf_frm_num(int vs_id);

/**
 * @brief				get number of vs encoded frames 
 * @param[in]	vs_id	video stream id
 * @return				number of frames that has been encoded after libvs has been started
 * @ingroup             libvs
 */
int libvs_get_encoded_frm_num(int vs_id);

/**
 * @brief				get number of vs encoded frames that has been read from bs buffer
 * @param[in]	vs_id	video stream id
 * @return				number of frames that has been read after libvs has been started
 * @ingroup             libvs
 */
int libvs_get_read_frm_num(int vs_id);

/**
 * @brief				get vs stream buffer size 
 * @param[in]	vs_id	video stream id
 * @return				total vs stream buffer size in bytes
 * @ingroup             libvs
 */
int libvs_get_total_strm_buf_size(int vs_id);

/**
 * @brief				get vs stream used buffer size 
 * @param[in]	vs_id	video stream id
 * @return 				used vs stream buffer size in bytes 
 * @ingroup             libvs
 */
int libvs_get_used_strm_buf_size(int vs_id);

/**
 * @brief				the threshold to drop old frames
 * @param[in]	vs_id	video stream id
 * @param[in]	drop_th threshold to drop old frames
 *              [24:0]:0 --- drop new frames; >0 drop old frames. drop_th = frame_rate * N secs. N secs means saving duration.
                [31:24]:0-- don't drop the existed old frames when switch to drop new frames.
				        1-- drop all existed old frames when switch to drop new frames
 * @retval		0		Do not drop old frames	
 * @return				drop old frames when frames in buffer is larger than configured threshold
 * @ingroup             libvs
 */
int libvs_set_oldfrmdrop_th(int vs_id, int drop_th);
	
/**
 * @brief				set max node number in VS frame list 
 * @param[in]	vs_id	video stream id
 * @param[in]	num	 	> 0: set max node in list, 0: set max node in list to default total frame list number
 * @retval		0		OK
 * @retval 		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_max_node_number(int vs_id, int num);

/**
 * @brief				set img qs
 * @param[in]	vs_id	video stream id
 * @param[in]	qs 		4~60, 4 is hightest quality, 60 is lowest quality
 * @retval		0		OK
 * @retval 		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_img_qs(int vs_id, int qs);

/**
 * @brief				set WDR strength
 * @param[in]	vs_id	video stream id
 * @param[in]	wdr 	WDR strength (0~4, default is 2)
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_wdr(int vs_id, u32 wdr);

/**
 * @brief				set MCTF
 * @param[in]	vs_id	video stream id
 * @param[in]	mctf 	MCTF (0~4, default is 2)
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_mctf(int vs_id, u32 mctf);

/**
 * @brief				set sharpness 
 * @param[in]	vs_id	video stream id
 * @param[in]	sharpness 	sharpness (0~4, default is 2)	
 * @retval		0		OK
 * @retval		!=0		ERROR
 * @ingroup             libvs
 */
int libvs_set_sharpness(int vs_id, u32 sharpness);

/**
 * @brief				get buf addr
 * @param[in]	vs_id	video stream id
 * @return				buf addr
 * @ingroup             libvs
 */
u32 libvs_get_bufaddr(int vs_id);

/**
 * @brief				get buf max length
 * @param[in]	vs_id	video stream id
 * @return				buf max length
 * @ingroup             libvs
 */
u32 libvs_get_bufmaxlen(int vs_id);

#endif
