/** 
 * @file	R3_fw/include/media/va/video_analysis.h
 * @brief	headers for video_analysis routines
 *
 * This header contains all video analysis related routine to interface 
 *
 * VA algo will get the Y data from our HW motion detection module. The APIs and format of Y data is as follow:
 * Y data format(Little Endian): Get the four bytes Y data every 8 bytes data. 
 * Y0 Y1 Y2 Y3 X X X X Y4 Y5 Y6 Y7 X X X X
 * .......
 * Yn-7 Yn-6 Yn-5 Yn-4 X X X X Yn-3 Yn-2 Yn-1 Yn X X X X
 * 
 * APIs  video_analysis_get_buf().
 * Usage:
 * while(1){ 
 *     t_md_algo_buf * md_buf = video_analysis_get_buf(0);
 *     if(md_buf != NULL){
 *         ///< TODO:fetch the Y data from the memory md_buf->addr_cur
 *         debug_printf("buf addr:%x, size:%d x %d\n", md_buf->addr_cur, md_buf->width, md_buf->height);
 *     
 *     }
 *     usleep(100*1000); //sleep 100ms.
 * }
 * @date	2016/04/04
 */
/** 
 * @defgroup        VA API Group
 */

#ifndef _VIDEO_ANALYSIS_
#define _VIDEO_ANALYSIS_

typedef struct s_video_analysis_cfg{
	int vs_id;		   // YUV stream id
	int sched_priority;	// priority of VA thread

	int (* run_cb)(void);	// call video_analysis_result to get va result in this callback. This callback will be called every VA result is updated.

	void * para;	// parameters for specific algo
}t_video_analysis_cfg;

#define VA_RES_OBJNUM		25
typedef struct s_video_analysis_res{
	//FIXME: only for temp
	char * str;

	//internal
	int obj_num;
	unsigned int obj_index[VA_RES_OBJNUM];	// index for roi
	unsigned int obj_type[VA_RES_OBJNUM];
	unsigned int obj_left[VA_RES_OBJNUM];
	unsigned int obj_top[VA_RES_OBJNUM];
	unsigned int obj_right[VA_RES_OBJNUM];
	unsigned int obj_bottom[VA_RES_OBJNUM];
	unsigned int obj_brightness[VA_RES_OBJNUM];
	unsigned int obj_motion[VA_RES_OBJNUM];	// FIXME: motion status filterred by sensitivity input (not useful anymore)
	unsigned int obj_count[VA_RES_OBJNUM];	// indicates pixcels' count of the strong moving area (Note: not the total pixcels' count of all moving area). It is multiple of 64. 

	//internal
	int is_motion;	// 1 : motion
	int is_motion_people;	// 1 : human, 0 : other motion
	int is_true_alarm;	// 1: true alarm, 0: false alarm //bitmap
	unsigned int motion_obj_type[VA_RES_OBJNUM];
	unsigned int motion_obj_left[VA_RES_OBJNUM];
	unsigned int motion_obj_top[VA_RES_OBJNUM];
	unsigned int motion_obj_right[VA_RES_OBJNUM];
	unsigned int motion_obj_bottom[VA_RES_OBJNUM];
	unsigned int motion_obj_brightness[VA_RES_OBJNUM];
}t_video_analysis_res;

/********** VA main API **********/
/**
 * @brief				Initialize and start VA.
 * @details				Use this routine to enable VA 
 *
 * @retval		0		Successfully did nothing.
 * @retval		-1		Create video analysis thread error
 * @ingroup				va
 */
int video_analysis_start(t_video_analysis_cfg * cfg);

/**
 * @brief				Stop VA.
 * @details				Use this routine to disable VA 
 *
 * @return				0 : OK
 * @ingroup				va	
 */
int video_analysis_stop(void);
/**
 * @brief				Get VA algo parameters
 * @param				para, parameters for specific algo
 *
 * @return				0 : OK,  <0: Fail
 * @ingroup				va	
 */
int video_analysis_get_para(void * para);
/**
 * @brief				Set VA algo parameters
 * @param				para, parameters for specific algo
 *
 * @return				0 : OK,  <0: Fail
 * @ingroup				va	
 */
int video_analysis_set_para(void * para);

/**
 * @brief				Get VA result.
 * @details				output va result to res struct, call this function in callback function. 
 *
 * @return				0 : OK
 * @ingroup				va
 */
int video_analysis_result(t_video_analysis_res * res);

/**
 * @brief                   get the latest Y data
 * @details					This API is non block, it'll return the latest Y data, no matter it's updated or not. So application should not call this API continuously, to avoid occupied 100% CPU resource. Or this API can be called in t_video_analysis_res->run_cb().
 *
 * @param[in]  timeout        not used now. it's non-block and return immediately.
 * @return                  pointer to the buffer structure.
 * @ingroup					va
 */
t_md_algo_buf * video_analysis_get_buf(u32 timeout);

#endif

