#ifndef _2D_H_
#define _2D_H_

/**
 * 2D Graphics
 */
/**
 * @brief  Draw the arbitrary line
 *
 * @param  x0     the start X position of arbitrary line
 * @param  y0     the start Y position of arbitrary line
 * @param  x1     the end X position of arbitrary line
 * @param  y1     the end Y position of arbitrary line
 * @param  color  line color
 *note : x1 >= x0
 */
void gui_draw_line(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color);

/**
 * @brief   Draw the rectangle
 *
 * @param   x0    the X position of left-top corner of rectangle
 * @param   y0    the Y position of left-top corner of rectangle
 * @param   x1    the X position of right-button corner of rectangle
 * @param   y1    the Y position of right-button corner of rectangle
 * @param   color the rect color
 */

void gui_draw_rect(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color);

 /**
  * @brief   draw the circle
  *
  * @param  x0     the X position of the center of circle
  * @param  y0     the Y position of the center of circle
  * @param  r      the radius of circle
  *	@param   color  cicle color
  */

void gui_draw_circle(t_sguictx * gui, int x0, int y0, int r, u32 color);

/**
 * @brief   Fill the circle
 *
 * @param  x0     the X position of the center of circle
 * @param  y0     the Y position of the center of circle
 * @param  r      the radius of circle
 * @parsm color   the fill color
 */
void gui_fill_circle(t_sguictx * gui, int x0, int y0, int r, u32 color);

/**
 * @brief   Fill the rectangle
 *
 * @param   x0    the X position of left-top corner of rectangle
 * @param   y0    the Y position of left-top corner of rectangle
 * @param   x1    the X position of right-button corner of rectangle
 * @param   y1    the Y position of right-button corner of rectangle
 * @param  color  if the color == TRANS, then it will clear rhe rect
 */

void gui_fill_rect(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color);

#endif
