#ifndef _FONT_H_
#define _FONT_H_

typedef struct s_gui_font
{
	u32 (*get_char)(const u8 *str, int * len); //str_encoding -> font_encoding translation.
	int (*get_char_width)(u32 charcode); //ret width pixels
	int (*draw_char)(struct s_sguictx * gui, u32 charcode, int x, int y, int w_max, u32 color); //return width drawed
	void * arg; //if intfont,  t_intfont, if extfont, t_extfont
}t_gui_font;

u32 gui_get_char_utf8(const u8 *str, int * len);

//just draw front color, do nothing with backcolor
int gui_draw_string(struct s_sguictx * gui, const char *s, int x0,int y0, int w_max, u32 color);

#endif
