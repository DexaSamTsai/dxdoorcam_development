#ifndef _BITMAP_H_
#define _BITMAP_H_

void gui_draw_fontbitmap(struct s_sguictx * gui, u8 *data, int width, int height, int x, int y, int w_max, u32 color);

#endif
