#ifndef _SGUI_H_
#define _SGUI_H_

struct s_sguictx;

#include "bitmap.h"
#include "intfont.h"
#include "extfont.h"
#include "font.h"

typedef struct s_sguictx
{
	int xsize;
	int ysize;
	void (*set_pixel)(void * hw_arg, int x,int y,u32 color);
	u32	 (*get_pixel)(void * hw_arg, int x,int y);
	t_gui_font *font;
	void * hw_arg;	
}t_sguictx;

int gui_init(t_sguictx * gui);

#include "2d.h"

#endif
