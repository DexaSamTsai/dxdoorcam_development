#ifndef _EXTFONT_H_
#define _EXTFONT_H_

typedef struct s_extfont_index1
{
	unsigned short first;
	unsigned short last;
	unsigned int firstoffset;
}t_extfont_index1;

typedef struct s_extfont
{
	u32 (*getdata)(struct s_sguictx * gui, u32 offset, u32 size, void *buf); //set before gui_extfont_init by main()

	unsigned char * cinfo; //tmp buffer for one char, set before gui_extfont_init by main()
	unsigned int cinfo_size; //tmp buffer size, set before gui_extfont_init by main()
	t_extfont_index1 * index1; //tmp buffer for index1, set before gui_extfont_init by main()
	unsigned int index1_count; //index1 tmp buffer count(size/8), set before gui_extfont_init by main(), changed after gui_extfont_init

	void *arg; //if sdfont, File *, if nand, (u32) nand_start

	//private
	unsigned char height;
	unsigned char maxwidth;
}t_extfont;

int extfont_draw_char(struct s_sguictx * gui, u32 charcode, int x, int y, int w_max, u32 color);
int gui_extfont_init(struct s_sguictx * gui);

//default nand getdata func
u32 gui_nandfont_getdata(struct s_sguictx * gui, u32 offset, u32 size, void *buf);
u32 gui_sdfont_getdata(struct s_sguictx * gui, u32 offset, u32 size, void *buf);

#endif
