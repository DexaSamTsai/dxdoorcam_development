#ifndef _INTFONT_H_
#define _INTFONT_H_

typedef struct s_intfont
{
	u8 height;
	u8 width;
	u8 start;
	u8 end;
	const u8 **data;
}t_intfont;

extern const t_intfont defintfont;
int intfont_draw_char(struct s_sguictx * gui, u32 charcode, int x, int y, int w_max, u32 color);

#endif
