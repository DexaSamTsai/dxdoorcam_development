/** 
 * @file   include/media/libas/afdist.h
 * @brief  Audio Frame Distributor
 *
 * @details distributing audio frames as different streams
 *
 * Audio Frame Distributor Instruction:
 *
 * 1. Each real stream need to be distributed will be allocated a distributor in mpu. 
 *
 * 2. A5 will access the distributed stream frame as from different streams. They are virtual streams all linked to one single stream.
 *
 * 3. Distributor will recorded the status of each virtual stream, start the real stream when start_as is first called and stop the
 *    stream when all virtual as have been called to stop. 
 *
 * 4. Distributor will manage the frame list of the real stream. 
 *
 * 5. The allocated frame list number for distributor should be the same as that of the real stream.
 *
 * 6. Supported up to 32 virtual streams.
 *
 * @by MannaGao
 * @2017/01/23
 */
#ifndef _LIBAFDIST_H_
#define _LIBAFDIST_H_

#define VIRTUAL_AS_NOT_DONE		0x7f	

typedef struct s_afdist_frm
{
	FRAME* vfrm;
	u32	as_flag;///<待读取标记，添加进列表时，只有enabled的虚拟码流才会在对应位置上(1<<as_id)标记为1
	u32	reading_flag;///<正在读取标记 
	struct s_afdist_frm* next;
}t_afdist_frm;

typedef struct s_AFDIST
{
	u32 num;	///< total frame list buffer number
	t_pool * pool;
	u8 * poolbuf;	///< frame list pool address
	u32 cnt;///< number of frames in current frame list
	
	t_afdist_frm * head;	
	t_afdist_frm * tail;

	u32	running_mask;///< if as enable, its corresponding position(1<<as_id) in the running_mask will be set to 1
	u32	init_mask;///< if as initialized, its corresponding position(1<<as_id) in the running_mask will be set to 1

	pthread_mutex_t mutex;
}AFDIST;	//AUDIO FRAME DISTRIBUTOR

typedef struct s_virtual_as
{
	u8	id;	///should be less than 32
	u32	mask;
	AFDIST	* afdist;
}t_virtual_as;


#define AFDIST_DECLARE(name, listnum) \
static unsigned char  __attribute__ ((aligned(4))) g_afdistlist_##name##_poolbuf[POOL_OVERHEAD_SIZE + sizeof(t_afdist_frm) * listnum + 4]; \
static AFDIST name = \
{ \
	.poolbuf = g_afdistlist_##name##_poolbuf, \
	.num = listnum, \
};

int virtual_as_init(t_virtual_as* vt_as);
int virtual_as_start(t_virtual_as* vt_as);
int virtual_as_stop(t_virtual_as* vt_as);
FRAME* virtual_as_get_frm(t_virtual_as* vt_as);
int virtual_as_free_frm(t_virtual_as* vt_as, FRAME* as_frm);
int virtual_as_exit(t_virtual_as* vt_as);
#endif
