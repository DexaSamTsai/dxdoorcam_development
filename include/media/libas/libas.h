/** 
 * @file    include/media/libas/libas.h
 * @brief   headers for Audio Stream
 *
 * This header contains all Audio related routines to interface with MPU.
 *
 *
 * @date    2016/12/15
 */
/** 
 * @defgroup        libas	AUDIO API Group
 */
#ifndef __LIBAS_H__
#define __LIBAS_H__

#include "liba_common.h"

/**
 * @brief					init audio encoding
 *
 * @param[in]				inp: parameters for initializing audio encoder
 * @param[in]				as_id	audio stream id
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libas
 */
int libas_init(int as_id, t_arec_inpara *inp);

/**
 * @brief					start audio encoding
 *
 * @param[in]				as_id	audio stream id
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libas
 */
int libas_start(int as_id);

/**
 * @brief					stop audio encoding
 *
 * @param[in]				as_id	audio stream id
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libas
 */
int libas_stop(int as_id);

/**
 * @brief					clear all buffer used by audio encoding
 *
 * @param[in]				as_id	audio stream id
 * @retval		0			Ok
 * @retval		<0			err
 * @ingroup					libas
 */
int libas_exit(int as_id);

/**
 * @brief					get one available encoded audio frame from linklist
 *
 * @param[in]				as_id	audio stream id
 * @param[in]	timeout		wait time if buffer empty
 * @return					frame pointer, NULL if no new frame.
 * @ingroup					libas
 */
FRAME * libas_get_frame(int as_id, u32 timeout);

/**
 * @brief					remove one frame from audio encoded linklist
 *
 * @param[in]				as_id	audio stream id
 * @param[in]				frm: the return pointer from libas_get_frame
 * @return					void
 * @ingroup					libas
 */
int libas_remove_frame(int as_id, FRAME *frm);


#endif /// __LIBAS_H__
