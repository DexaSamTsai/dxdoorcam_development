#ifndef _RTP_H_
#define _RTP_H_

#ifdef CONFIG_RTP_TCP
#define RTP_HEADER_SIZE 16
#define RTSP_PART_SIZE 4
#else
#define RTP_HEADER_SIZE 12
#endif

#define RTP_DYNAMIC_PT 96
#define RTP_SERVER_NAME "omnivision"

typedef struct rtp_header
{
#ifdef CONFIG_RTP_TCP
	u8 maigc_num;
	u8 channel;
	u16 length;
#endif
#ifdef RTP_LITTLE_ENDIAN
	u8 cc:4;
	u8 x:1;
	u8 p:1;
	u8 v:2;
	u8 pt:7;
	u8 m:1;
	u16 sn;
	u32 ts;
	u32 ssrc;
//	u32 csrc[16];
#else
	u8 v:2;
	u8 p:1;
	u8 x:1;
	u8 cc:4;
	u8 m:1;
	u8 pt:7;
	u16 sn;
	u32 ts;
	u32 ssrc;
//	u32 csrc[16];
#endif

}__attribute__((packed)) rtp_header_t;

typedef struct RTP_STA
{
	u32 dyn_pt;
}mux_rtp_sta_t;

#endif

