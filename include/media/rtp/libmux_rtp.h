#ifndef _LIBMUX_RTP_H_
#define _LIBMUX_RTP_H_
//#include "librtp/libmux_rtp_encv.h"
//#include "librtp/libmux_rtp_f4.h"
/**
 * @defgroup libmux_rtp
 * @ingroup librtp
 * @brief MUX RTP Library
%en * @details Libmux_rtp is a mux rtp library to provide a rtp packet of different media stream type.Specific usage can refer to share/librtp/librtp.c . <br>
%zh * @details Libmux_rtp是一个mux rtp库.用来封装不同媒体类型的rtp数据包.具体用法可以参照share/librtp/librtp.c<br>
 * -# <b>Design Doc:</b> 
 	<ul>
%en		<li>To build libmux_rtp.c in project, libmux_rtp should be enabled by menuconfig tool. The configuration is as below:<br>
 %zh	<li>如果需要编译libmux_rtp.c,需要在menuconfig中选取libmux_rtp.配置方法如下：<br>
			<ul>
			<li>	RTP support --->
			<li>	 [*] enable libmux_rtp
			</ul>
%en		<li>If want to enable RTCP the RTCP can be configured like this, then user can call libmux_rtcp_get_data to get rtcp data. <br>
%zh		<li>如果需要RTCP，可以按如下配置选择rtcp enable。这样可以使用libmux_rtcp_get_data来获取每个会话的rtcp数据包.<br>
			<ul>
			<li>	 [*] enable libmux_rtp
			<li>	 	[*] enable RTCP
			</ul>
%en_start		
		<li>One mux rtp session can generate one data packet of the stream. For audio and video streams, there two sessions should be declared.<br>
		    mux_sess_a and mux_sess_v will be declared as a global variable for other function using. The type is libmux_rtp_cfg_t *.<br>
			User should provide a stream payload type. Libmux_rtp can support payload type string "f4" and "encv" now. <br>
			The last parameter of this declare is the stream index of vfdist or afdist. We can support multi client for transmission of one stream. The usage can refer to doc of vfdist or afdist.<br>
%en_end		
%zh_start		
		<li>一个mux rtp 会话可以封装一种媒体类型的数据包.如果需要传输audio和video码流需要声明两个mux rtp session. <br>
			a.如下声明会声明两个全局变量mux_sess_a 和 mux_sess_v, 这两个变量需要在libmux_rtp相关函数中作为参数使用.这两个变量的类型是libmux_rtp_cfg_t * .<br> 
			c.用户需要提供码流负载类型.libmux_rtp现在只支持两种类型码流."f4"和 "encv".<br>
			d.最后一个参数是fdist的index。一路码流数据可以支持多路传输。只需要建立多个mux rtp会话.具体index的使用可以参照vfdist或者afdist<br>
%en		<li>Before start mux rtp session, should call libmux_rtp_init functin first to initialize the internal parameter of librtp. This function need to be called once
%zh		<li>在开始启动mux rtp会话之前需要调用libmux_rtp_init函数来初始化libmuxrtp的内部参数.该函数只需要调用一次.<br>
			<ul>
			<li>libmux_rtp_init();
			</ul>
%en_start		
		<li>To start mux rtp session user should call libmux_rtp_create to start one mux rtp session. <br>
			Libmux_rtp uses dynamic payload type number now. The first dynamic paylod type number is 96, the payload type will be increased one every time when libmux_rtp_create has been called. 
%en_end		
%zh_start		
		<li>开始muxrtp会话需要调用libmux_rtp_create函数。<br>
		Librtp使用动态payload类型.Playload类型值从96开始,每建立一个新的会话，这个值加一。<br>
%zh_end		
			<ul>
			<li>libmux_rtp_create(mux_sess_a);
			<li>libmux_rtp_create(mux_sess_v);
			</ul>
%en		<li>User can call libmux_rtp_get_data to get rtp stream packet, and use libmux_rtp_parse_data to parse rtp packet that received from network. <br>
%zh		<li>可以调用libmux_rtp_get_data来获取需要传输的mux rtp数据包.并且可以使用libmux_rtp_parse_data来解析网络收到的数据.具体可以参照sample code<br>
%en		<li>If rtcp has been enabled user can call libmux_rtcp_get_data to get rtp packet. The rtcp sending internal is 5s now as default. Can use librtpmux_rtcp_parse_data to parse rtcp packet that received from network<br>
%zh		<li>如果选择了rtcp,可以调用libmux_rtp_get_data来获取需要传输的mux rctp数据包.并且可以使用libmux_rtcp_parse_data来解析网络收到的数据.具体可以参照sample code。<br>
%en		<li>Parsing data function libmux_rtp_parse_data and libmux_rtcp_parse_data are very simple now, only can parse protocol version.
%zh		<li>libmux_rtp_parse_data和libmux_rtcp_parse_data函数很简单，暂时只能解析协议版本.
%en		<li>To get mux rtp stream info, such as payload type, timestamp, sscr,sequence number, user can call libmux_rtp_get_xxxx function at anytime once the session has been created.<br>
%zh		<li>用户可以在调用libmux_rtp_create之后调用如下函数来获取该会话的ssrc，payload type，timestamp，sequence number等相关信息.<br>
			<ul>
			<li>libmux_rtp_get_ssrc(mux_sess_a); //get ssrc of session_a
			<li>libmux_rtp_get_pt(mux_sess_v); //get payload type of session_v
			</ul>
%en		<li>User can call libmux_rtp_release to release mux rtp session<br><br>
%zh		<li>可以调用libmux_rtp_release函数来释放该会话.<br>
			<ul>
			<li>libmux_rtp_release(mux_sess_a);
			<li>libmux_rtp_release(mux_sess_v);
			</ul>
	</ul>
 * -# <b>Sample Codes :</b>
%en 	* -# Declare configuration struct of rtp, payload stream type, and fdist index of stream. Specific usage can refer to share/librtp/librtp.c
\code
	libmux_rtp_cfg_t mux_sess_a;
	libmux_rtp_cfg_t mux_sess_v;
\endcode
 	* -# Initialize libmuxrtp and start mux rtp session
\code
	libmux_rtp_init();
	mux_sess_a.payload_type = mux_rtp_f4;
	mux_sess_a.fdist = 0;  //audio_fdist<<4|video_fdist;
	mux_sess_a.asamplerate = 44100;
	mux_sess_a.achannel = 1;  //fdist id. 
	
	mux_sess_v.payload_type = mux_rtp_encv;
	mux_sess_v.vs_id;  //video stream id
	mux_sess_v.fdist = 0;  //audio_fdist<<4|video_fdist;
	mux_sess_v.framerate = 30;  
	libmux_rtp_create(&mux_sess_a);
	libmux_rtp_create(&mux_sess_v);
\endcode
	* -# Get mux rtp packet, then send data throgh network
\code
	char  *data;
	int  data_size;
	libmux_rtp_get_data(mux_sess_a, &data, &data_size);
\endcode
	* -# Get mux rtcp packet, then send data throgh network
\code
	char *data;
	int data_size;
	libmux_rtcp_get_data(mux_sess_a, &data, &data_size);
\endcode
	* -# Parse mux rtp packet data that is received data from network.
\code
	char  data[xxx];
	int  data_size;
	libmux_rtp_parse_data(mux_sess_a, data, data_size);
\endcode
	* -# Parse mux rtcp packet data that is received data from network.
\code
	char  data[xxx];
	int  data_size;
	libmux_rtcp_parse_data(mux_sess_a, data, data_size);
\endcode
	* -# Release mux rtp session
\code
	librtp_release(mux_sess_a);
	librtp_release(mux_sess_v);
\endcode
 	
 * @{
 */


/**
 * @REAL_FORMAT #define RTP_DATA_SIZE 
 *
 * @brief data length of rtp packet
 */
//INTERNAL_START
#define RTP_DATA_SIZE 1460
//INTERNAL_END

/**
 * @brief Type defined for rtp stream 
 * @details Used to define rtp stream parameter.<br> 
 */
typedef struct RTP_STREAM
{
	u8 sdata[RTP_DATA_SIZE]; ///<rtp packet data
	u32 slen;
	u32 pt;
	u32 seq;
	u32 ssrc;
	u32 ts;         //rtp ts calculate from stream_ts;
	u32 mark;
	u32 sent_packet_cnt;
}rtp_stream_t;

/**
 * @brief Type defined for rtcp stream 
 * @details Used to define rtcp stream parameter.<br> 
 */
#ifdef CONFIG_RTCP_EN
typedef struct RTCP_STREAM
{
	u8 sdata[RTP_DATA_SIZE];
	u8 slen;
	u32 send_timer;
	u32 recv_timer; //recved rtcp packet tick.
}rtcp_stream_t;
#endif


/**
 * @brief Type defined for mux rtp payload type
 * @details Used payload type string in MUX_RTP_DECLARE.
 */

typedef enum MUX_RTP_PT
{
	MUX_RTP_ENCV = 1, 
	MUX_RTP_MIMG,  
	MUX_RTP_F4,   
	MUX_RTP_MP2T, //no support now
}e_mux_rtp_pt;

/**
 * @brief Type defined for mux rtp session 
 * @details Used to define mux rtp session.<br> 
 */
typedef struct LIBMUX_RTP_CFG
{
	rtp_stream_t rtp_stream;
#ifdef CONFIG_RTCP_EN
	rtcp_stream_t rtcp_stream;
#endif
	void *payload_arg;
	int (*payload_fill)(void *arg, unsigned char *dst, int max_len, u32 *ts, u32 *mark); //return payload_len;
	
	int payload_type;  //MUX_RTP_ENCV, MUX_RTP_MIMG or MUX_RTP_F4 support now.
	int vs_id;  //video stream id
	//TODO:need to support multi user later.
	int av_fdist;  //audio_fdist<<4|video_fdist.//if use ts format neet to support fdist of both audio and video
	int framerate;  
	u32 resolution;

	int asample_rate;
	int achannels;  //fdist id. 
//internal 
	int rtp_dlen;
	int rtp_frm;
	u32 curtick;
	struct LIBMUX_RTP_CFG *next;

}libmux_rtp_cfg_t;

/**
 * @REAL_FORMAT 
 * @brief <b>Declare mux rtp as a single rtp stream .</b>
 * @details A mux_id should be declared firstly before used as param of libmux_rtp_create(mux_id). Once declared, it will be regarded as a global variable.<br>
 *
 * @param mux_id MUX rtp configuration structure point. <br>
 * @param plstr The payload type string. "f4" is for mux_rtp_f4. "encv" is for mux_rtp_encv. The enum variables is defined in libmux_rtp.h  
 * @param fdist_idx Fdist index of audio or video stream. 
 *
 */
//INTERNAL_END

/**
 * @brief <b>Init rtp internal parameters.</b>
 * @param NULL 
 * @return void 
 */
void libmux_rtp_init(void);

/**
 * @brief <b>create a rtp mux session.</b>
 * @details This fuction will create a mux rtp container.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return -1: mux_rtp created fail<br>
 * 			0: mux_rtp created success<br>
 */
int libmux_rtp_create(libmux_rtp_cfg_t *rtp_cfg);

/**
 * @brief <b>release a mux rtp session.</b>
 * @details This fuction will release a mux rtp container.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return NULL <br> 
 */
void libmux_rtp_release(libmux_rtp_cfg_t *rtp_cfg);

/**
 * @brief <b>parse rtp data.</b>
 * @details This fuction can parse rtp data received data from libnet<br>
 * @param rtp_cfg The mux_rtp session point. 
 * @param buf[in] The received rtp packet buffer 
 * @param size[in] The buffer size of received rtp packet 
 *
 * @return 1: parse data success 
  		   0: parse data fail 
 */
int libmux_rtp_parse_data(libmux_rtp_cfg_t *rtp_cfg, char *buf, int size);

/**
 * @brief <b>get rtp data.</b>
 * @details: To get rtp packet data of mux_id.<br>
 * @param rtp_cfg The mux_rtp session point. 
 * @param dst[out] The send buffer point
 * @param size[out] Buffer size of send packet 
 *
 * @return Buffer size 
 */
int libmux_rtp_get_data(libmux_rtp_cfg_t *rtp_cfg, char **dst, int *size);

/**
 * @brief <b>get payload type of a rtp session.</b>
 * @details To get the payload type number of current mux_rtp session.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return payload_type 
 */
int libmux_rtp_get_pt(libmux_rtp_cfg_t *mux_rtp_cfg);

/**
 * @brief <b>get seq of a rtp session.</b>
 * @details To get the sequence number of current mux_rtp session.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return sequence of current rtp session 
 */
int libmux_rtp_get_seq(libmux_rtp_cfg_t *mux_rtp_cfg);

/**
 * @brief <b>Get ssrc of a rtp session.</b>
 * @details To get current ssrc number of current mux_rtp session.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return ssrc of current rtp session 
 */
int libmux_rtp_get_ssrc(libmux_rtp_cfg_t *mux_rtp_cfg);

/**
 * @brief <b>get ts of a rtp session.</b>
 * @details To get the timestamp of current mux_rtp session.<br>
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return timestamp of current rtp session 
 */
int libmux_rtp_get_ts(libmux_rtp_cfg_t *mux_rtp_cfg);

#ifdef CONFIG_RTCP_EN
/**
 * @brief <b>Parse rtcp data.</b>
 * @details To parse rtcp data of mux_rtp session.<br>
 * @param rtp_cfg The mux_rtp session point. 
 * @param buf[in] The buffer address of received rtcp packet
 * @param size[in] Buffer size of received rtcp packet
 *
 * @return 1: parse data success 
  		   0: parse data fail 
 */

int libmux_rtcp_parse_data(libmux_rtp_cfg_t *rtp_cfg, char *buf, int size);

/**
 * @brief <b>Get a rtcp packet.</b>
 * @details To get rtcp packet data<br> 
 * @param rtp_cfg The mux_rtp session point. 
 * @param dst[out] The send rtcp buffer point
 * @param size[out] Buffer size of rtcp packet 
 *
 * @return Buffer size 
 */
int libmux_rtcp_get_data(libmux_rtp_cfg_t *rtp_cfg, char **dst, int *size);

/**
 * @brief <b>Check a rtcp packet timeout.</b>
 * @details To check if received rtcp packet timeout <br> 
 * @param rtp_cfg The mux_rtp session point. 
 *
 * @return 1:timeout 0:no timeout 
 */
int libmux_rtcp_timeout(libmux_rtp_cfg_t* mux_rtp_cfg);
#endif
/** @} */ //end libnet_ifconfig

#endif

