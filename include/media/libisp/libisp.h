/** 
 * @file	R3_fw/libmpu_linux/include/libisp.h
 * @brief	headers for ISP routines
 *
 * This header contains all ISP related routine to interface with MPU.
 *
 * @date	2016/04/04
 */
/** 
 * @defgroup        libisp	ISP API Group
 */
#ifndef _LIBISP_H_
#define _LIBISP_H_

/********** ISP main API **********/
/**
 * @brief					Initialize ISP.
 * @details					Use this routine to enable ISP
 *
 * @retval		0			Successfully did nothing.
 * @retval		-1			datapath exception
 * @retval		-2			sensor exception
 * @ingroup					libisp
 */
int libisp_init(void);
/**
 * @brief					Deinitialize ISP.
 * @details					Use this routine to disable ISP
 *
 * @return					return value of MPUCMD_ISP_EXIT command from MPU
 * @ingroup					libisp
 */
int libisp_exit(void);
/**
 * @brief					Get Lower ISP Address
 * @details					Use this routine to get address
 *
 * @return
 * @ingroup					libisp
 */
u32 libisp_get_laddr(void);
/**
 * @brief					Get Higher ISP Address
 * @details					Use this routine to get address
 *
 * @return
 * @ingroup					libisp
 */
u32 libisp_get_raddr(void);
/**
 * @brief					Initialize ISP.
 * @details					Use this routine to enable ISP faster
 *
 * @retval		0			always returns 0
 * @ingroup					libisp
 */
int libisp_fastinit(void);

/********** ISP ioctl **********/
/**
 * @brief               	Set ISP buffer address.
 * @details             	Assign two addresses for both of ISP channels
 *
 * @param[in]	l_addr		Left (channel 1) address
 * @param[in]	r_addr		Right (channel 2) address
 * @retval      0       	always returns 0
 * @ingroup					libisp
 */
int libisp_set_bufaddr(unsigned int l_addr, unsigned int r_addr);
/**
 * @brief					Set ISP brightness settings
 * @details					Use this API to assign ISP brightness level
 *
 * @param[in]	isp id		ISP ID
 * @param[in]	brightness	ISP brightness level
 * @retval		0			always returns 0
 * @ingroup					libisp
 */
int libisp_set_brightness(int isp_id, int brightness);
/**
 * @brief					Set ISP sharpness settings
 * @details					Use this API to assign ISP sharpness level
 *
 * @param[in]	isp id		ISP ID
 * @param[in]	sharpness	ISP sharpness level
 * @retval		0			always returns 0
 * @ingroup					libisp
 */
int libisp_set_sharpness(int isp_id, int sharpness);
/**
 * @brief					Set ISP SDE
 * @details					Use this API to assign Special Digital Effect in ISP configuration
 *
 * @param[in]	isp id		ISP ID
 * @param[in]	sde_effect_No	SDE Effect Number
 * @retval		0			always returns 0
 * @ingroup					libisp
 */
int libisp_set_sde(int isp_id, int sde_effect_No);
/**
 * @brief					Set ISP Mode
 * @details					Use this routine to switch between day/night/auto mode
 *
 * @param[in]	isp_id		ISP ID
 * @param[in]	daynight	0: Day; 1: Night; 2: Auto
 * @retval		0			always returns 0
 * @ingroup					libisp
 */
int libisp_set_daynight(int isp_id, int daynight);
int libisp_set_autofps(int isp_id, int autofps_enable,int max_fps,int min_fps);
int libisp_set_channel(int isp_id, int channel);
int libisp_get_lum(int isp_id);
int libisp_set_exp(int isp_id, u32 exp);
int libisp_set_gain(int isp_id, u32 gain);
/**
 * @brief					Get ISP Exposure
 * @details					Use this routine to get ISP exposure
 * 
 * @param[in]	isp_id		The ID of ISP
 * @return					ISP exposure
 * @ingroup					libisp
 */
int libisp_get_exp(int isp_id);
/**
 * @brief					Get ISP Gain
 * @details					Use this routine to get ISP gain
 * 
 * @param[in]	isp_id		The ID of ISP
 * @return					ISP gain
 * @ingroup					libisp
 */
int libisp_get_gain(int isp_id);
int libisp_set_awb_b_gain(int isp_id, u32 awb);
int libisp_set_awb_g_gain(int isp_id, u32 awb);
int libisp_set_awb_r_gain(int isp_id, u32 awb);
int libisp_get_awb_b_gain(int isp_id);
int libisp_get_awb_g_gain(int isp_id);
int libisp_get_awb_r_gain(int isp_id);
int libisp_set_calib_awb(int isp_id, u32 addr);
int libisp_set_calib_lenc_atbl(int isp_id, u32 addr);
int libisp_set_calib_lenc_ctbl(int isp_id, u32 addr);
int libisp_set_calib_lenc_dtbl(int isp_id, u32 addr);
int libisp_get_rgbir_cgain(int isp_id);
int libisp_get_aec_stable(int isp_id);

/**
 * @internal
 * @brief					Set ISP Coefficient
 * @details					Use 60 fps exp as 15fps exp init value. But when 60fps,the IR LED is off,while when 15fps,IR LED is on. So set coeff/10 as the divider for IR LED reason
 *
 * @param[in]	isp_id		The ID of ISP
 * @param[in]	val			Coefficient value
 * @ingroup					libisp
 */
int libisp_set_coeff(int isp_id, u32 val);
/**
 * @internal
 * @brief					Set ISP Threshold	
 * @details					The saturation will be adjusted when current compensation gain larger thanthreshold 
 * 							There is one threshold value can be configured in this function. 
 * 							Saturation will be auto adjusted from 0 to full color when rgbir_cgain is higher than sat_thr. 
 * 							The lower lux, the rgbir_cgain higher. So if want to auto adjust saturation from high lux, then should set sat_thr lower.
 *
 * @param[in]	isp_id		is always 0 in current project.
 * @param[in]	val			saturation threshold. range is from 0x110 to 0x180, default is 0x120
 * @ingroup					libisp
 */
int libisp_set_sat_thr(int isp_id, u32 val);

/**
 * @internal
 * @brief					Set Coefficient Threashold
 * @details					Set threshold of rgbir_cgain to switch full/half coeffient mode
 *
 * @param[in]	isp_id		The ID of ISP
 * @param[in]	val			coeff_thr_low<<16|coeff_thr_high, default coeff_thr_low = 0x140, coeff_thr_high = 0x1d8
 * @ingroup					libisp
 */
int libisp_set_coeff_thr(int isp_id, u32 val);
/**
 * @internal
 * @brief					Set low lux coeff value
 *
 * @param[in]	isp_id		The ID of ISP
 * @param[in]	coeff	
 * @ingroup					libisp
 * */
int libisp_set_lowlux_coeff(int isp_id, u32 coeff);

/**
 * @internal
 * @brief					Set HDR  mode or nonHDR mode
 *
 * @param[in]	isp_id		The ID of ISP
 * @param[in]	val			HDR enable	
 * @ingroup					libisp
 * */
int libisp_set_hdr(int isp_id, u32 val);

/**
 * @internal
 * @brief					Get histogram buffer
 *
 * @param[in]	isp_id		The ID of ISP
 * @return					histogram buffer address
 * @ingroup					libisp
 * */
u32 libisp_get_hist_buf(int isp_id);

typedef struct s_roi{
    u32 enable;
    u32 x1;
    u32 y1;
    u32 width;
    u32 height;
}t_roi;

/**
 * @internal
 * @brief					Set ROI
 *
 * @param[in]	isp_id		The ID of ISP
 * @param[in]	roi			address 
 * @ingroup					libisp
 * */
int libisp_set_roi(int isp_id, u32 roi);

/**
 * @internal
 * @brief					Get Y mean	
 *
 * @param[in]	isp_id		The ID of ISP
 * @return					mean Y value
 * @ingroup					libisp
 * */
int libisp_get_y(int isp_id);

/**
 * @internal
 * @brief					Get day/night mode 
 *
 * @param[in]	isp_id		The ID of ISP
 * @return      			day/night mode
 * @ingroup					libisp
 * */
int libisp_get_daynight(int isp_id);

/**
 * @internal
 * @brief					Get HDR/nonHDR mode 
 *
 * @param[in]	isp_id		The ID of ISP
 * @return					HDR/nonHDR mode
 * @ingroup					libisp
 * */
int libisp_get_hdr(int isp_id);


/**
 * @internal
 * @brief					Get HDR stable 
 *
 * @param[in]	isp_id		The ID of ISP
 * @retval		1			stable
 * @retval		0			not stable
 * @ingroup					libisp
 * */
int libisp_get_hdr_stable(int isp_id);

/**
 * @internal
 * @brief
 *
 * @param[in]   argc
 * @param[in]	argv
 * @return
 * @ingroup					libisp
 * */
int isp_debug_cmd(int argc, char ** argv);

/**
 * @internal
 * @brief
 *
 * @param[in]   isp_id
 * @param[in]   request
 * @param[in]	arg
 * @return
 * @ingroup					libisp
 * */
int libisp_ioctl(int isp_id, int request, void * arg);

/**
 * @internal
 * @brief					Read from SCCB registers
 *
 * @param[in]   isp_id		ISP ID
 * @param[in]   sensorid
 * @param[in]   sccbaddr	SCCB address
 * @return
 * @ingroup             	libisp
 * */
int libisp_sccbrd(int isp_id, int sensorid, int sccbaddr);

/**
 * @internal
 * @brief					Write to SCCB registers
 *
 * @param[in]   isp_id		ISP ID
 * @param[in]   sensorid
 * @param[in]   sccbaddr	SCCB address
 * @return
 * @ingroup					libisp
 * */
int libisp_sccbwr(int isp_id, int sensorid, int sccbaddr, int sccbval);

int libisp_set_aec_manual(int isp_id, u32 enable);
int libisp_set_aec_manual_gain(int isp_id, u32 gain);
int libisp_set_aec_manual_exp(int isp_id, u32 exp);
int libisp_set_awb_manual(int isp_id, u32 enable);


#if (!defined(__UBOOT__) && (!defined __LINUX__))
#include "isp_iq.h"
#endif

#endif
