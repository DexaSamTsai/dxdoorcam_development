#ifndef _CALFW_IQ_H_
#define _CALFW_IQ_H_

#define SHADING_D_TABLE     (0)
#define SHADING_C_TABLE     (1)
#define SHADING_A_TABLE     (2)
void iq_shading_update(int table_num);

void iq_set_golden_value(int bg, int rg);

void iq_calibration_update(void);

u32 detect_old_awb_calibration(void);

#endif
