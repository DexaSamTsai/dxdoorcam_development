#ifndef _PTHREAD_H_
#define _PTHREAD_H_

#include "sys/types.h"
#include "sched.h" 
#include "ertos/ertos.h"

/**
 * @defgroup pthread
 * @ingroup api
 * @brief multi-threads support
%en * @details ertos support multi-threads. thread support priority from 1~30, high priority(low priority number) threads run first. If there are several threads have the same priority, then each thread run for 10ms and switch to other threads.
%zh * @details 支持多个任务，每个任务可以设置0~30的优先级，高优先级(数字小)先执行，直到主动让出CPU。优先级相同的任务使用相同的时间片调度。
 * the multi-threads API follow the pthreads API. Detailed see document/OV798 thread programming guide(chinese).pdf, but chinese only.

 * @{
 */

#define ETASKLIST_VOLATILE //volatile
#define ETASKLIST_MAXVALUE 0xffffffffUL

#define PTHREAD_CANCEL_ENABLE  0
#define PTHREAD_CANCEL_DISABLE 1
#define PTHREAD_CANCEL_DEFERRED 0
#define PTHREAD_CANCEL_ASYNCHRONOUS 1
#define PTHREAD_CANCELED ((void *) -1)

#define PTHREAD_CREATE_DETACHED 0
#define PTHREAD_CREATE_JOINABLE  1

#define PTHREAD_MUTEX_DEFAULT    3
#define PTHREAD_MUTEX_ERRORCHECK 2
#define PTHREAD_MUTEX_NORMAL     0
#define PTHREAD_MUTEX_RECURSIVE  1

#define PTHREAD_PRIO_NONE    0
#define PTHREAD_PRIO_INHERIT 1
#define PTHREAD_PRIO_PROTECT 2

#define PTHREAD_PROCESS_PRIVATE 0
#define PTHREAD_PROCESS_SHARED  1

#define PTHREAD_SCOPE_PROCESS 0
#define PTHREAD_SCOPE_SYSTEM  1

#define PTHREAD_RWLOCK_INITIALIZER  ((pthread_rwlock_t) 0xFFFFFFFF)

/*********** pthread ************/
typedef struct {
	void * stackaddr;
	size_t stacksize;
	int detachstate;
	struct sched_param schedparam;
}pthread_attr_t;

typedef struct {
}pthread_barrier_t;

typedef struct {
}pthread_barrierattr_t;

typedef struct _s_pthread_cond{
	t_ertos_queue event;
	u8 data[1];
}pthread_cond_t;

typedef struct {
}pthread_condattr_t;

typedef void * pthread_key_t;

typedef t_ertos_queue pthread_mutex_t;

typedef struct {
}pthread_mutexattr_t;

typedef struct {
	pthread_mutex_t mutex;
	pthread_cond_t cond;
	volatile int v;
}pthread_once_t;
#define PTHREAD_ONCE_INIT { \
	.mutex = PTHREAD_MUTEX_INITIALIZER, \
	.cond = PTHREAD_COND_INITIALIZER, \
	.v = 0, \
}

typedef uint32_t pthread_rwlock_t;

typedef struct {
}pthread_rwlockattr_t;

typedef uint32_t pthread_spinlock_t;

typedef void * pthread_t;

/**************** list of mandatory functions ***************/
int pthread_attr_init(pthread_attr_t * attr);
int pthread_attr_destroy(pthread_attr_t * attr);
int pthread_attr_setstack(pthread_attr_t * attr, void *stackaddr, size_t stacksize);
int pthread_attr_setstacksize(pthread_attr_t * attr, size_t stacksize);
int pthread_attr_getstack(pthread_attr_t *attr, void **stackaddr, size_t *stacksize);
int pthread_attr_getstacksize(pthread_attr_t *attr, size_t *stacksize);
int pthread_attr_getdetachstate(const pthread_attr_t * attr, int * detachstate);
int pthread_attr_setdetachstate(pthread_attr_t * attr, int detachstate);
int pthread_attr_getschedparam(const pthread_attr_t * attr, struct sched_param * param);
int pthread_attr_setschedparam(pthread_attr_t * attr, const struct sched_param * param);

/* 如果attr==NULL, 默认stack 512*4 Bytes, 如果ertos还没有起来，优先级16. 否则优先级跟当前调用者优先级一样。线程必须被detach或者被join，否则会有内存泄露。 */
int pthread_create(pthread_t * thread, const pthread_attr_t * attr, void *(*start_routine)(void*), void * arg);
/* pthread只有被detach之后，运行结束才会释放pthread_t的内存。 */
int pthread_detach(pthread_t thread);
/* 等待线程结束, 如果value_ptr不为NULL，保存线程返回值。已经被detach的线程不能join。函数返回后pthread_t已经被释放，不能再访问。*/
int pthread_join(pthread_t thread, void **value_ptr);
pthread_t pthread_self(void);
//set pthread name
char * pthread_setname(pthread_t thread, char * name);


#define PTHREAD_MUTEX_INITIALIZER  { \
	.from_heap = 0, \
	.inited = 0, \
}
int pthread_mutex_init(pthread_mutex_t * mutex, const pthread_mutexattr_t * attr);
int pthread_mutex_destroy(pthread_mutex_t * mutex);
int pthread_mutex_lock(pthread_mutex_t * mutex);
int pthread_mutex_timedlock(pthread_mutex_t * mutex, const struct timespec * abs_timeout);
int pthread_mutex_unlock(pthread_mutex_t * mutex);
int pthread_mutex_trylock(pthread_mutex_t * mutex);


#define PTHREAD_COND_INITIALIZER  { \
	.event.from_heap = 0, \
	.event.inited = 0, \
}
int pthread_cond_init(pthread_cond_t * cond, const pthread_condattr_t * attr);
int pthread_cond_destroy(pthread_cond_t * cond);
int pthread_cond_wait(pthread_cond_t * cond, pthread_mutex_t * mutex);
int pthread_cond_timedwait(pthread_cond_t * cond, pthread_mutex_t * mutex, const struct timespec * abs_timeout);
int pthread_cond_signal(pthread_cond_t * cond);
int pthread_cond_broadcast(pthread_cond_t * cond);

int pthread_cancel(pthread_t thread);
void pthread_testcancel(void);
int pthread_setcancelstate(int state, int *oldstate);
int pthread_setcanceltype(int type, int *oldtype);

void pthread_cleanup_pop(int execute);
void pthread_cleanup_push(void (*routine) (void *), void *arg);

int pthread_equal(pthread_t t1, pthread_t t2);

int pthread_once(pthread_once_t *once_control, void (*init_routine)(void));

/*
int   pthread_atfork(void (*)(void), void (*)(void), void(*)(void));
int   pthread_attr_getguardsize(const pthread_attr_t *restrict, size_t *restrict);
int   pthread_attr_setguardsize(pthread_attr_t *, size_t);
int   pthread_barrier_destroy(pthread_barrier_t *);
int   pthread_barrier_init(pthread_barrier_t *restrict, const pthread_barrierattr_t *restrict, unsigned);
int   pthread_barrier_wait(pthread_barrier_t *);
int   pthread_barrierattr_destroy(pthread_barrierattr_t *);
int   pthread_barrierattr_init(pthread_barrierattr_t *);
int   pthread_condattr_destroy(pthread_condattr_t *);
int   pthread_condattr_getclock(const pthread_condattr_t *restrict, clockid_t *restrict);
int   pthread_condattr_init(pthread_condattr_t *);
int   pthread_condattr_setclock(pthread_condattr_t *, clockid_t);
void  pthread_exit(void *);
*/
void *pthread_getspecific(pthread_key_t);
int   pthread_setspecific(pthread_key_t, const void *);
int   pthread_key_create(pthread_key_t *, void (*)(void*));
int   pthread_key_delete(pthread_key_t);
/*
int   pthread_mutex_consistent(pthread_mutex_t *);
int   pthread_mutexattr_destroy(pthread_mutexattr_t *);
int   pthread_mutexattr_getrobust(const pthread_mutexattr_t *restrict, int *restrict);
int   pthread_mutexattr_gettype(const pthread_mutexattr_t *restrict, int *restrict);
int   pthread_mutexattr_init(pthread_mutexattr_t *);
int   pthread_mutexattr_setrobust(pthread_mutexattr_t *, int);
int   pthread_mutexattr_settype(pthread_mutexattr_t *, int);
int   pthread_rwlock_destroy(pthread_rwlock_t *);
int   pthread_rwlock_init(pthread_rwlock_t *restrict, const pthread_rwlockattr_t *restrict);
int   pthread_rwlock_rdlock(pthread_rwlock_t *);
int   pthread_rwlock_timedrdlock(pthread_rwlock_t *restrict, const struct timespec *restrict);
int   pthread_rwlock_timedwrlock(pthread_rwlock_t *restrict, const struct timespec *restrict);
int   pthread_rwlock_tryrdlock(pthread_rwlock_t *);
int   pthread_rwlock_trywrlock(pthread_rwlock_t *);
int   pthread_rwlock_unlock(pthread_rwlock_t *);
int   pthread_rwlock_wrlock(pthread_rwlock_t *);
int   pthread_rwlockattr_destroy(pthread_rwlockattr_t *);
int   pthread_rwlockattr_init(pthread_rwlockattr_t *);
int   pthread_spin_destroy(pthread_spinlock_t *);
int   pthread_spin_init(pthread_spinlock_t *, int);
int   pthread_spin_lock(pthread_spinlock_t *);
int   pthread_spin_trylock(pthread_spinlock_t *);
int   pthread_spin_unlock(pthread_spinlock_t *);
*/

/**************** list of C extension functions ***************/
/*
int   pthread_attr_getinheritsched(const pthread_attr_t *restrict, int *restrict);
int   pthread_attr_getschedpolicy(const pthread_attr_t *restrict, int *restrict);
int   pthread_attr_getscope(const pthread_attr_t *restrict, int *restrict);
int   pthread_attr_setinheritsched(pthread_attr_t *, int);
int   pthread_attr_setschedpolicy(pthread_attr_t *, int);
int   pthread_attr_setscope(pthread_attr_t *, int);
int   pthread_barrierattr_getpshared( const pthread_barrierattr_t *restrict, int *restrict);
int   pthread_barrierattr_setpshared(pthread_barrierattr_t *, int);
int   pthread_condattr_getpshared(const pthread_condattr_t *restrict, int *restrict);
int   pthread_condattr_setpshared(pthread_condattr_t *, int);
int   pthread_getconcurrency(void);
int   pthread_getcpuclockid(pthread_t, clockid_t *);
int   pthread_getschedparam(pthread_t, int *restrict, struct sched_param *restrict);
int   pthread_mutex_getprioceiling(const pthread_mutex_t *restrict, int *restrict);
int   pthread_mutex_setprioceiling(pthread_mutex_t *restrict, int, int *restrict);
int   pthread_mutexattr_getprioceiling( const pthread_mutexattr_t *restrict, int *restrict);
int   pthread_mutexattr_getprotocol(const pthread_mutexattr_t *restrict, int *restrict);
int   pthread_mutexattr_getpshared(const pthread_mutexattr_t *restrict, int *restrict);
int   pthread_mutexattr_setprioceiling(pthread_mutexattr_t *, int);
int   pthread_mutexattr_setprotocol(pthread_mutexattr_t *, int);
int   pthread_mutexattr_setpshared(pthread_mutexattr_t *, int);
int   pthread_rwlockattr_getpshared( const pthread_rwlockattr_t *restrict, int *restrict);
int   pthread_rwlockattr_setpshared(pthread_rwlockattr_t *, int);
int   pthread_setconcurrency(int);
int   pthread_setschedparam(pthread_t, int, const struct sched_param *);
int   pthread_setschedprio(pthread_t, int);
*/

/** @} */

#endif
