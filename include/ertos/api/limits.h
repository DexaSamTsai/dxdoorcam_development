#ifndef _LIMITS_H_
#define _LIMITS_H_

#define AIO_LISTIO_MAX          2
#define AIO_MAX                 1
#define AIO_PRIO_DELTA_MAX      0
#define DELAYTIMER_MAX          32
#define MQ_OPEN_MAX             8
#define MQ_PRIO_MAX             32
#define PAGESIZE                (1<<12)
#define RTSIG_MAX               8
#define SEM_NSEMS_MAX           256
#define SEM_VALUE_MAX           32767
#define SIGQUEUE_MAX            32
#define STREAM_MAX              8
#define TIMER_MAX               32
#define TZNAME_MAX              6
#define CHILD_MAX				6
#define HOST_NAME_MAX			255
#define IOV_MAX					Error_Undefined 
#define LOGIN_NAME_MAX			Error_Undefined 
#define OPEN_MAX				16
#define PTHREAD_DESTRUCTOR_ITERATIONS		4
#define PTHREAD_KEYS_MAX		128
#define PTHREAD_STACK_MIN		64
#define PTHREAD_THREADS_MAX		64
#define RE_DUP_MAX				255
#define SS_REPL_MAX			Error_Undefined 
#define SYMLOOP_MAX				8
#define TTY_NAME_MAX			9

#define FILESIZEBITS	32
#define	LINK_MAX		32767
#define	MAX_CANON		  255
#define	MAX_INPUT		  255
#define	NAME_MAX		  255
#define	NGROUPS_MAX		   16
#define	PATH_MAX		 1024
#define PIPE_BUF			Error_Undefined 
#define SYMLINK_MAX			Error_Undefined 
#define BC_BASE_MAX			Error_Undefined 
#define BC_DIM_MAX			Error_Undefined 
#define BC_SCALE_MAX			Error_Undefined 
#define BC_STRING_MAX			Error_Undefined 
#define CHARCLASS_NAME_MAX			Error_Undefined 
#define COLL_WEIGHTS_MAX			Error_Undefined 
#define EXPR_NEST_MAX			Error_Undefined 
#define LINE_MAX			Error_Undefined 

#define CHAR_BIT 8
#define CHAR_MIN (-128)
#define CHAR_MAX 127

#define __INT_MAX__ 2147483647
#define INT_MIN (-INT_MAX-1)
#define INT_MAX __INT_MAX__

#define __LONG_LONG_MAX__ 9223372036854775807LL
#define LLONG_MIN (-LLONG_MAX-1)
#define LLONG_MAX __LONG_LONG_MAX__
#define LONG_LONG_MIN (-LONG_LONG_MAX-1)
#define LONG_LONG_MAX __LONG_LONG_MAX__
#define ULONG_LONG_MAX (LONG_LONG_MAX * 2ULL + 1)

#define LONG_BIT 32
#define __LONG_MAX__ 2147483647L
#define LONG_MAX __LONG_MAX__
#define LONG_MIN (-LONG_MAX-1)

#define MB_LEN_MAX    1

#define SCHAR_MIN (-128)
#define SCHAR_MAX 127
#define SHRT_MAX 32767
#define SHRT_MIN (-32767-1)
#define SSIZE_MAX LONG_MAX
#define UCHAR_MAX 255
#define UINT_MAX (INT_MAX * 2U + 1)
#define ULLONG_MAX (LLONG_MAX * 2ULL + 1)
#define ULONG_MAX (LONG_MAX * 2UL + 1)
#define USHRT_MAX 65535
#define WORD_BIT 32
#define NL_ARGMAX 32
#define	NZERO	0

#endif
