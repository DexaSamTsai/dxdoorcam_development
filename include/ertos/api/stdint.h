#ifndef _C_H_
#define _C_H_

#include "limits.h"

typedef signed char int8_t ;
typedef unsigned char uint8_t ;
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef signed long int32_t;
typedef unsigned long uint32_t;
typedef signed long long int64_t;
typedef unsigned long long uint64_t;
typedef long intmax_t;
typedef unsigned long long uintmax_t;

typedef long __intptr_t;
typedef unsigned long __uintptr_t;
typedef __intptr_t intptr_t;
typedef __uintptr_t uintptr_t;

#define INTPTR_MAX (LONG_MAX)
#define INTPTR_MIN (-(LONG_MAX) - 1)
#define UINTPTR_MAX ((LONG_MAX) * 2UL + 1)

#define INT8_MIN 	-128
#define INT8_MAX 	 127
#define UINT8_MAX 	 255

#define INT16_MIN 	-32768
#define INT16_MAX 	 32767
#define UINT16_MAX 	 65535

#define INT32_MIN 	 (-2147483647L-1)
#define INT32_MAX 	 2147483647L
#define UINT32_MAX       4294967295UL

#define INT64_MIN 	(-9223372036854775807LL-1LL)
#define INT64_MAX 	 9223372036854775807LL
#define UINT64_MAX 	18446744073709551615ULL

#define SIZE_MAX ((LONG_MAX) * 2UL + 1)

#define INT8_C(x)	x
#define UINT8_C(x)	x
#define INT16_C(x)	x
#define UINT16_C(x)	x
#define INT32_C(x)	x##L
#define UINT32_C(x)	x##UL
#define INT64_C(x)	x##LL
#define UINT64_C(x)	x##ULL
#define INTMAX_C(x)	x##LL
#define UINTMAX_C(x)	x##ULL

#endif
