#ifndef _MATH_H_
#define _MATH_H_

double rint(double x);
double fabs(double x);

double sqrt(double x);
double log(double x);

double cos(double x);
double sin(double x);

double tan(double x);
double pow(double x, double y);
double floor(double x);

double scalbn (double x, int n);

double exp(double x);
#endif
