#ifndef _ARPA_INET_H_
#define _ARPA_INET_H_

#if LWIP_VERSION < 0x02000000
const char *inet_ntop(int, const void *, char *, socklen_t);
#endif

#endif
