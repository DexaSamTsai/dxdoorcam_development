#ifndef _STDDEF_H_
#define _STDDEF_H_

#ifndef NULL
#define NULL 0L
#endif

typedef int ptrdiff_t;
typedef unsigned long size_t;

#include "sys/types.h"

#define offsetof(type,ident) ((size_t)&(((type*)0)->ident))

#endif
