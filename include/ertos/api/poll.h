#ifndef _POLL_H_
#define _POLL_H_

struct pollfd
{
	int fd;	
	short events;
	short revents;
};

typedef unsigned int nfds_t;

#define POLLIN		0x001
#define POLLPRI		0x002
#define POLLOUT		0x004
#define POLLERR		0x008
#define POLLHUP		0x010
#define POLLNVAL	0x020
#define POLLRDNORM	0x040
#define POLLRDBAND	0x080
#define POLLWRNORM	0x100
#define POLLWRBAND	0x200
#define POLLMSG		0x400
#define POLLREMOVE	0x1000
#define POLLRDHUP	0x2000

/**************** list of mandatory functions ***************/
/* poll() defined in "share/network/lwip/src/include/lwip/sockets.h", poll() only support sockets */
/* int poll(struct pollfd *fds, nfds_t nfds, int timeout); */

/**************** list of C extension functions ***************/

#endif
