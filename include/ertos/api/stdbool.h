#ifndef _STDBOOL_H_
#define _STDBOOL_H_

#ifndef __BOOL__
#define __BOOL__
typedef unsigned char  		bool;
#define true 1
#define false 0
#define __bool_true_false_are_defined  1
#endif

#endif
