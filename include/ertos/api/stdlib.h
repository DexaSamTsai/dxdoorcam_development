#ifndef _STDLIB_H_
#define _STDLIB_H_

#include "stddef.h"

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define RAND_MAX 0xffffffff

#define MB_CUR_MAX locale_mb_cur_max()

typedef struct 
{
  int quot;
  int rem;
} div_t;

typedef struct 
{
  long quot;
  long rem;
} ldiv_t;

typedef struct
{
  long long int quot;
  long long int rem;
} lldiv_t;


/**************** list of mandatory functions ***************/
double atof(const char * ptr);
int atoi(const char * s);
long atol(const char * s);
long long atoll(const char * str);

void abort(void);
void exit(int c);
void _Exit(int c);
int atexit(void (* func)(void));

int loadenv(void); //new API, load env from CONFIG_ENV_SOURCE, buffer length CONFIG_ENV_MAXLEN
int loadenv_sf(char * buf, int maxlen); //new API
int saveenv(void); //new API
int saveenv_sf(void); //new API
int env_set(int argc, char ** argv); //new API
int env_export(int argc, char ** argv); //new API
int env_unset(int argc, char ** argv); //new API
int env_get(int argc, char ** argv); //new API

char * getenv(const char * key);
int setenv(const char *name, const char *value, int overwrite);
int unsetenv(const char *name);
int putenv(char *string);
void printenv(void);

int abs(int i);
long labs(long x);
long long llabs(long long j);

div_t div( int num, int denom);
ldiv_t ldiv(long num, long denom);
lldiv_t lldiv(long long numer, long long denom);

void * malloc(size_t size);
void free(void * ptr);
void * calloc(size_t nmemb, size_t size);
void * realloc(void *ptr, size_t size);
/*
int           mblen(const char *, size_t);
size_t        mbstowcs(wchar_t *restrict, const char *restrict, size_t);
int           mbtowc(wchar_t *restrict, const char *restrict, size_t);
size_t        wcstombs(char *restrict, const wchar_t *restrict, size_t);
int           wctomb(char *, wchar_t);
*/
void *bsearch(const void *key, const void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));
void qsort(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *));
void qsort_r(void *base, size_t nmemb, size_t size, int (*compar)(const void *, const void *, void *), void *arg);
int           rand(void);
int           rand_r(unsigned *);
void          srand(unsigned);
double        strtod(const char * nptr, char ** endptr);
/*
float         strtof(const char *restrict, char **restrict);
long double   strtold(const char *restrict, char **restrict);
*/
long strtol(const char * nptr, char ** endptr, int base);
unsigned long strtoul(const char * ptr, char ** endptr, int base);
long long strtoll(const char * nptr, char ** endptr, int base);
unsigned long long strtoull(const char * nptr, char ** endptr, int base);
//int           system(const char *); //NOT to implement

/**************** list of C extension functions ***************/
/*
long          a64l(const char *);
double        drand48(void);
double        erand48(unsigned short [3]);
int           getsubopt(char **, char *const *, char **);
int           grantpt(int);
char         *initstate(unsigned, char *, size_t);
long          jrand48(unsigned short [3]);
char         *l64a(long);
void          lcong48(unsigned short [7]);
long          lrand48(void);
char         *mkdtemp(char *);
int           mkstemp(char *);
long          mrand48(void);
long          nrand48(unsigned short [3]);
char         *ptsname(int);
*/
long          random(void);
/*
char         *realpath(const char *restrict, char *restrict);
unsigned short *seed48(unsigned short [3]);
void          setkey(const char *);
char         *setstate(char *);
void          srand48(long);
*/
void          srandom(unsigned);
/*
int           unlockpt(int);
*/

#endif
