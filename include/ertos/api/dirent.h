#ifndef _DIRENT_H_
#define _DIRENT_H_

#include "sys/types.h"
#include "limits.h"

struct dirent
{
	ino_t  d_ino; //[XSI]
	u32 create_time;
#define DIRENT_ATTR_FILE        (0x00)
#define DIRENT_ATTR_READ_ONLY   (0x01)
#define DIRENT_ATTR_DIRECTORY   (0x10)
#define DIRENT_FLAG_SNAME       (0x10000)
#define DIRENT_FLAG_LNAME       (0x20000)
	u32  attribute;
	u32 size;
	u32 namelen;
	char d_name[NAME_MAX+1];
};

typedef struct s_DIR
{
	//struct dirent dirent;
	char * dd_buf;
	unsigned int dd_size;
	unsigned int dd_loc;
	unsigned int nitems;
	void * hw_arg;
	int hw_arg_fromheap;
	struct dirent *drent;
	int (*cb_select)(const struct dirent *);
}t_DIR;

typedef t_DIR DIR;

#if 0
struct dirent
{
	ino_t  d_ino; //[XSI]
	char   d_name[NAME_MAX+1];
	u8  Attribute;
	u16 create_time;
	u16 create_date;
	u32 size;
};
#endif

/**************** list of mandatory functions ***************/
//TODO

//int            alphasort(const struct dirent **, const struct dirent **);
int            closedir(DIR *);
//int            dirfd(DIR *);
//DIR           *fdopendir(int);
DIR           *opendir(const char *);
struct dirent *readdir(DIR *);
//int            readdir_r(DIR *restrict, struct dirent *restrict,
//				                   struct dirent **restrict);
//void           rewinddir(DIR *);

int            scandir(const char *, struct dirent ***, int (*)(const struct dirent *), int (*)(const struct dirent **, const struct dirent **));

/**************** list of C extension functions ***************/
/*
void           seekdir(DIR *, long);
long           telldir(DIR *);
*/


#endif
