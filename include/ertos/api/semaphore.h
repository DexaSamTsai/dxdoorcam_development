#ifndef _SEMAPHORE_H_
#define _SEMAPHORE_H_

typedef struct
{
	struct s_ertos_queue queue;
	unsigned int padding;
}sem_t;

#define SEM_FAILED	((sem_t *) 0)

/**************** list of mandatory functions ***************/
int sem_init(sem_t *sem, int pshared, unsigned int value);
int sem_post(sem_t *sem);
int sem_wait(sem_t *sem);
int sem_trywait(sem_t *sem);
int sem_timedwait(sem_t *sem, const struct timespec *abs_timeout);
int sem_getvalue(sem_t *sem, int *sval);
int sem_destroy(sem_t *sem);

//not implemented and return errno=ENOSYS
sem_t *sem_open(const char *name, int oflag, ...);
int sem_close(sem_t *sem);
int sem_unlink(const char *name);

/**************** list of C extension functions ***************/

#endif
