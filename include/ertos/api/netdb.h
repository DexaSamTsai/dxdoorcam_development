#ifndef _NETDB_H_
#define _NETDB_H_


#include "lwip/netdb.h"

#  define NI_MAXHOST      1025
#  define NI_MAXSERV      32

# define NI_NUMERICHOST 1       /* Don't try to look up hostname.  */
# define NI_NUMERICSERV 2       /* Don't convert port number to name.  */
# define NI_NOFQDN      4       /* Only return nodename portion.  */
# define NI_NAMEREQD    8       /* Don't return numeric addresses.  */
# define NI_DGRAM       16      /* Look up UDP service rather than TCP.  */


struct servent {
   char  *s_name;       /* official service name */
   char **s_aliases;    /* alias list */
   int    s_port;       /* port number */
   char  *s_proto;      /* protocol to use */
};

struct servent *getservent(void);

struct servent *getservbyname(const char *name, const char *proto);

struct servent *getservbyport(int port, const char *proto);

void setservent(int stayopen);

void endservent(void);

int ioctlnet(int fd, unsigned long request, ...);

#endif
