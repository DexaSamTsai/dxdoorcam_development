#ifndef _STDIO_H_
#define _STDIO_H_

#include "stdarg.h"
#include "stddef.h"
#include "sys/types.h"

typedef struct s_FILE
{
	ssize_t (*hw_read)(void * arg, void *buf, size_t count);
	ssize_t (*hw_write)(void * arg, const void *buf, size_t count);
	int (*hw_seek)(void * arg, long offset, int whence);
	long (*hw_tell)(void * arg);
	int (*hw_fcntl)(void * arg, int cmd, int val);
	int (*hw_close)(void * arg);
	void * hw_arg;

	int fd;
#define FILE_FLAGS_RD	0x00000001
#define FILE_FLAGS_WR	0x00000002
#define FILE_FLAGS_ERR	0x00000010
#define FILE_FLAGS_EOF	0x00000020
	unsigned int flags;

	struct s_FILE * next; //for fd linklist.

	unsigned char * rdbuf; //used for sscanf only
	int rdbuf_r; //read pointer
	int rdbuf_size; //total size

#define FILE_UNGETC_BUFMAX 4
	unsigned char ungetc_buf[FILE_UNGETC_BUFMAX];
	int ungetc_buf_len;

	unsigned char hw_arg_fromheap;
}t_FILE;

typedef t_FILE FILE;
typedef long fpos_t;

#define	_IOFBF	0
#define	_IOLBF	1
#define	_IONBF	2

#ifndef SEEK_SET
#define	SEEK_SET	0
#endif
#ifndef SEEK_CUR
#define	SEEK_CUR	1
#endif
#ifndef SEEK_END
#define	SEEK_END	2
#endif

#define	FILENAME_MAX	255

#define	EOF	(-1)

extern FILE * g_stdin;
extern FILE * g_stdout;
extern FILE * g_stderr;

//stdin/stdout/stderr only can be used after ertos_init()
//TODO: how to set current task's stdin ?
#define stdin (*((g_ertos == NULL || g_ertos->cur_task == NULL || g_ertos->cur_task->_stdin == NULL) ? &g_stdin : &g_ertos->cur_task->_stdin ))
#define stdout (*((g_ertos == NULL || g_ertos->cur_task == NULL || g_ertos->cur_task->_stdout == NULL) ? &g_stdout : &g_ertos->cur_task->_stdout ))
#define stderr (*((g_ertos == NULL || g_ertos->cur_task == NULL || g_ertos->cur_task->_stderr == NULL) ? &g_stderr : &g_ertos->cur_task->_stderr ))

/**************** list of mandatory functions ***************/
void     clearerr(FILE *);
int      fclose(FILE * fp);
int      feof(FILE *);
int      ferror(FILE *);
int      fflush(FILE * fp);
int      fgetc(FILE * fp);
int      fgetpos(FILE * fp, fpos_t * pos);
char    *fgets(char * s, int size, FILE * fp);
FILE *fopen(const char *path, const char *mode);
int      fprintf(FILE *fp, const char *fmt0, ...);
int      fputc(int c, FILE * fp);
int      fputs(const char * s, FILE * fp);
size_t fread(void * buf, size_t size, size_t count, FILE * fp);
/*
FILE    *freopen(const char *restrict, const char *restrict, FILE *restrict);
*/
int fscanf(FILE * fp, const char * fmt, ...);
int      fseek(FILE * fp, long offset, int whence);
int      fsetpos(FILE * fp, const fpos_t * pos);
long     ftell(FILE * fp);
size_t fwrite(const void * buf, size_t size, size_t count, FILE * fp);
#define getc(fp) fgetc(fp)
#define getchar() fgetc(stdin)
void perror(const char * s);
#define putc(c, fp) fputc(c, fp)
#define putchar(c) fputc(c, stdout)
#define puts(s) fputs(s, stdout)
int remove(const char * pathname);
int rename(const char *oldpath, const char *newpath);
void rewind(FILE * fp);
/*
void     setbuf(FILE *restrict, char *restrict);
int      setvbuf(FILE *restrict, char *restrict, int, size_t);
*/
int snprintf(char *str, size_t size, const char *format, ...);
int sprintf(char *buf, const char *fmt0, ...);
int vfscanf(FILE * fp, const char * fmt, va_list ap);
int vsscanf(const char *str, const char *fmt, va_list ap);
int sscanf(const char *str, const char *format, ...);
/*
char    *tempnam(const char *, const char *); //[OB]
FILE    *tmpfile(void);
char    *tmpnam(char *); //[OB]
*/
int      ungetc(int c, FILE * fp);

int      vfprintf(FILE *, const char *, va_list);
int      vprintf(const char *, va_list);
int      vsnprintf(char *, size_t, const char *, va_list);
int      vsprintf(char *, const char *, va_list);


//NOT to support
/*
int      scanf(const char *restrict, ...);
int      vscanf(const char *restrict, va_list);
char    *gets(char *); //[OB]
*/
//int      printf(const char *restrict, ...);
#define printf(x...) fprintf(stdout, x)

/**************** list of C extension functions ***************/
/*
char    *ctermid(char *);
int      dprintf(int, const char *restrict, ...)
*/
FILE *fdopen(int fd, const char *mode);
int fileno(FILE * fp);
/*
void     flockfile(FILE *);
FILE    *fmemopen(void *restrict, size_t, const char *restrict);
int      fseeko(FILE *, off_t, int);
off_t    ftello(FILE *);
int      ftrylockfile(FILE *);
void     funlockfile(FILE *);
int      getc_unlocked(FILE *);
int      getchar_unlocked(void);
ssize_t  getdelim(char **restrict, size_t *restrict, int, FILE *restrict);
ssize_t  getline(char **restrict, size_t *restrict, FILE *restrict);
FILE    *open_memstream(char **, size_t *);
int      pclose(FILE *);
FILE    *popen(const char *, const char *);
int      putc_unlocked(int, FILE *);
int      putchar_unlocked(int);
int      renameat(int, const char *, int, const char *);
int      vdprintf(int, const char *restrict, va_list);
*/

#endif
