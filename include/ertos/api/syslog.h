#ifndef _SYSLOG_H_
#define _SYSLOG_H_

#define	LOG_EMERG	0
#define	LOG_ALERT	1
#define	LOG_CRIT	2
#define	LOG_ERR		3
#define	LOG_WARNING	4
#define	LOG_NOTICE	5		//only used in interrupt & only print one char, for log not in interrupt plase use WARNING, INFO or DEBUG
#define	LOG_INFO	6		//for FAE or customer debug
#define	LOG_DEBUG	7		//for internal development debug

#define	LOG_MASK(pri)	(1 << (pri))
#define	LOG_UPTO(pri)	((1 << ((pri)+1)) - 1)

/* the 'define' below are not used, just for compatible */
#define	LOG_PID		0x01
#define	LOG_CONS	0x02
#define	LOG_ODELAY	0x04
#define	LOG_NDELAY	0x08
#define	LOG_NOWAIT	0x10
#define	LOG_PERROR	0x20

#define	LOG_KERN	(0<<3)
#define	LOG_USER	(1<<3)
#define	LOG_MAIL	(2<<3)
#define	LOG_DAEMON	(3<<3)
#define	LOG_AUTH	(4<<3)
#define	LOG_SYSLOG	(5<<3)
#define	LOG_LPR		(6<<3)
#define	LOG_NEWS	(7<<3)
#define	LOG_UUCP	(8<<3)
#define	LOG_CRON	(9<<3)
#define	LOG_AUTHPRIV	(10<<3)
#define	LOG_FTP		(11<<3)
#define	LOG_LOCAL0	(16<<3)
#define	LOG_LOCAL1	(17<<3)
#define	LOG_LOCAL2	(18<<3)
#define	LOG_LOCAL3	(19<<3)
#define	LOG_LOCAL4	(20<<3)
#define	LOG_LOCAL5	(21<<3)
#define	LOG_LOCAL6	(22<<3)
#define	LOG_LOCAL7	(23<<3)
/* not used above */

/**************** list of mandatory functions ***************/
void openlog(const char *ident, int option, int facility);
void syslog(int priority, const char *format, ...);
void closelog(void);
int setlogmask(int mask);

/**************** list of C extension functions ***************/
/**
 * @brief type menuconfig to configure ERTOS_SYSLOG_BUFLEN, if ERTOS_SYSLOG_BUFLEN is larger than zero, log messages are stored in syslog buffer,meanwhile, they are printed on hyperterminal.The messages stored in syslog buffer could be read for other purpose.If syslog buffer is full, the content won't be overwritten currently.
 */
typedef struct s_syslog_buf
{
	char * base;
	int size;
	int w;
	int r;
	int remcnt;
}t_syslog_buf;

typedef struct s_syslog_cfg
{
#define SYSLOG_UNLOCK   (0)
#define SYSLOG_LOCK     (1)
	unsigned char syslog_islock;
	t_syslog_buf * syslog_buf;
}t_syslog_cfg;

extern t_syslog_cfg g_syslog_cfg;

/**
 * @brief lock syslog buffer
 * @details if syslog buffer is locked, new messages can't be stored to syslog buffer.
 * @return < 0 failed, >= 0 OK.
 */
int syslog_buflock(void);

/**
 * @brief unlock syslog buffer
 * @details if syslog buffer is unlocked, new messages could be stored to syslog buffer.
 * @return < 0 failed, >= 0 OK.
 */
int syslog_bufunlock(void);

/**
 * @brief read messages from syslog buffer.
 * @param buf pointer to syslog buffer
 * @param maxlen the read length
 * @return < 0 failed, >= 0 OK.
 */
int syslog_bufread(char ** buf, unsigned int maxlen);

/**
 * @brief configure which log message should be stored according to the log level.
 * @param mask    log level
 * @return < 0 failed, >= 0 OK.
 */
int setlogbufmask(int mask);

/**
 * @brief clear the content of syslog buffer after read them.
 * @param buf pointer to syslog buffer
 * @param maxlen the read length
 * @return none
 */
void syslog_bufclear(char * buf, unsigned int maxlen);
#endif
