#ifndef _TIME_H_
#define _TIME_H_

#include "sys/types.h"

/**
 * @brief the same name for compatible
 */
struct tm
{
	int tm_sec;			///< Seconds.	[0-60] (1 leap second)
	int tm_min;			///< Minutes.	[0-59]
	int tm_hour;		///< Hours.	[0-23]
	int tm_mday;		///< Day.		[1-31]
	int tm_mon;			///< Month.	[0-11]
	int tm_year;		///< Year	- 1900. 
	int tm_wday;		///< Day of week.	[0-6]
	int tm_yday;		///< Days in year.[0-365]
	int tm_isdst;		///< DST.		[-1/0/1]
};

struct timespec {
	time_t  tv_sec;
	long    tv_nsec;
};

struct itimerspec {
	struct timespec it_interval;
	struct timespec it_value;
};

#define CLOCKS_PER_SEC 100

#define TIMER_ABSTIME	4

extern int    daylight;
extern long   timezone;
extern char  *tzname[];

/**************** list of mandatory functions ***************/
clock_t    clock(void);
double difftime(time_t time1, time_t time0);
size_t strftime(char * s, size_t maxsize, const char * format, const struct tm * tm);
struct tm * gmtime(const time_t *timep);
struct tm * localtime(const time_t *timep);
time_t mktime(struct tm * tm);
time_t time(time_t *);
int stime(time_t *t);

/**************** list of C extension functions ***************/
struct tm * gmtime_r(const time_t *timep, struct tm * result);
struct tm * localtime_r(const time_t * timep, struct tm * result);
void tzset_str(char * tzenv); //new API. set tz from string in argument, not from env.
void tzset(void); //note: the env 'TZ' should be set

char *asctime(const struct tm *tm);
char *asctime_r(const struct tm *tm, char *buf);
char *ctime(const time_t *timep);
char *ctime_r(const time_t *timep, char *buf);

int nanosleep(const struct timespec *, struct timespec *);
/*
int        clock_getcpuclockid(pid_t, clockid_t *);
int        clock_getres(clockid_t, struct timespec *);
int        clock_gettime(clockid_t, struct timespec *);
int        clock_nanosleep(clockid_t, int, const struct timespec *, struct timespec *);
int        clock_settime(clockid_t, const struct timespec *);
struct tm *getdate(const char *);
size_t     strftime_l(char *restrict, size_t, const char *restrict, const struct tm *restrict, locale_t);
*/
char *strptime(const char *, const char *, struct tm *);
/*
int        timer_create(clockid_t, struct sigevent *restrict, timer_t *restrict);
int        timer_delete(timer_t);
int        timer_getoverrun(timer_t);
int        timer_gettime(timer_t, struct itimerspec *);
int        timer_settime(timer_t, int, const struct itimerspec *restrict, struct itimerspec *restrict);
*/


#endif
