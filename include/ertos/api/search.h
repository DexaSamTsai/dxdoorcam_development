#ifndef _SEARCH_H_
#define _SEARCH_H_

#include "sys/queue.h"

typedef struct entry {
	char *key;
	void *data;
} ENTRY;

typedef enum {
	FIND, ENTER
} ACTION;

typedef enum {
	preorder,
	postorder,
	endorder,
	leaf
} VISIT;

struct internal_entry {
	SLIST_ENTRY(internal_entry) link;
	ENTRY ent;
};
SLIST_HEAD(internal_head, internal_entry);

struct hsearch_data
{
	struct internal_head *htable;
	size_t htablesize;
};

/**************** list of mandatory functions ***************/
int hcreate_r(size_t nel, struct hsearch_data *htab);
void hdestroy_r(struct hsearch_data *htab);
int hsearch_r(ENTRY item, ACTION action, ENTRY **retval, struct hsearch_data *htab);

int hcreate(size_t nel);
void hdestroy(void);
int hsearch(ENTRY item, ACTION action, ENTRY **retval);

/*
TODO1:
void * lfind(const void *key, const void *base, size_t *nmemb, size_t size, int(*compar)(const void *, const void *));
void * lsearch(const void *key, void *base, size_t *nmemb, size_t size, int(*compar)(const void *, const void *));
*/

void *tsearch(const void *key, void **rootp, int (*compar)(const void *, const void *));
void *tfind(const void *key, const void **rootp, int (*compar)(const void *, const void *));
void *tdelete(const void *key, void **rootp, int (*compar)(const void *, const void *));
void twalk(const void *root, void (*action)(const void *nodep, const VISIT which, const int depth));
void tdestroy(void *root, void (*free_node)(void *nodep));

/**************** list of C extension functions ***************/

#endif
