#ifndef _STRING_H_
#define _STRING_H_

#include "stddef.h"

/**************** list of mandatory functions ***************/
void *memchr(const void *s, int c, size_t n);
int memcmp(const void *s1, const void *s2, size_t n);
void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dest, const void *src, size_t n);
void *memset(void *d, int c, size_t n);

char *strcat(char *dest, const char *src);
char *strchr(const char *s, int c);
int strcmp(const char *s1, const char *s2);
#define strcoll strcmp
char *strcpy(char *dest, const char *src);
size_t strcspn(const char * s, const char * reject);
char *strerror(int errnum);
size_t strlen(const char * c);
char *strncat(char *dest, const char *src, size_t n);
int strncmp(const char *s1, const char *s2, size_t len);
char * strncpy(char *dst, const char *src, size_t len);
char * strpbrk(const char * s1, const char * s2);
char * strrchr(const char *s, int c);
size_t strspn(const char * s, const char * accept);
char * strtok(char * str, const char * delim);
char * strstr (const char *haystack, const char *needle);
size_t strxfrm(char *dest, const char *src, size_t n);


/**************** list of C extension functions ***************/
void *memccpy(void *dest, const void *src, int c, size_t n);
char *stpcpy(char *dest, const char *src);
char *stpncpy(char *dest, const char *src, size_t n);
/*
int      strcoll_l(const char *, const char *, locale_t);
*/
char * strdup(const char * s);
/*
char    *strerror_l(int, locale_t);
*/
int strerror_r(int errnum, char *buf, size_t buflen);
char *strndup(const char *s, size_t n);
size_t strnlen(const char *s, size_t maxlen);
#define strsignal Error_not_defined
char * strtok_r(char * str, const char * delim, char ** saveptr);
/*
size_t   strxfrm_l(char *restrict, const char *restrict, size_t, locale_t);
*/

/***************** 4.4 BSD **************************/
char *strsep(char **s, const char *ct);

#endif
