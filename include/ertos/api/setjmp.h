#ifndef _SETJMP_H_
#define _SETJMP_H_

//for ARM only
#define _JBLEN 24
typedef	int jmp_buf[_JBLEN];

/**************** list of mandatory functions ***************/
int setjmp(jmp_buf env);
void longjmp(jmp_buf env, int val);

/**************** list of C extension functions ***************/

#endif
