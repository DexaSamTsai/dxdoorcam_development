#ifndef _SYS_FCNTL_H_
#define _SYS_FCNTL_H_

#define	F_DUPFD		0
#define	F_DUPFD_CLOEXEC	14
#define	F_GETFD		1
#define	F_SETFD		2
#define	F_GETFL		3
#define	F_SETFL		4
#define	F_GETLK  	7
#define	F_SETLK  	8
#define	F_SETLKW 	9
#define	F_GETOWN 	5
#define	F_SETOWN 	6

#define F_FLUSH		123 /* self-defined */

#define	FD_CLOEXEC	1

#define	F_RDLCK		1
#define	F_WRLCK		2
#define	F_UNLCK		3


#define	O_RDONLY	0
#define	O_WRONLY	1
#define	O_RDWR		2

#define F_OK 0
#define W_OK 2
#define R_OK 4

#define	O_ACCMODE	(O_RDONLY|O_WRONLY|O_RDWR)

#define	_FAPPEND	0x0008
#define	_FMARK		0x0010
#define	_FDEFER		0x0020
#define	_FASYNC		0x0040
#define	_FSHLOCK	0x0080
#define	_FEXLOCK	0x0100
#define	_FCREAT		0x0200
#define	_FTRUNC		0x0400
#define	_FEXCL		0x0800
#define	_FNBIO		0x1000
#define	_FSYNC		0x2000
#define	_FNONBLOCK	0x4000
#define	_FNDELAY	_FNONBLOCK
#define	_FNOCTTY	0x8000
#define _FBINARY        0x10000
#define _FBINARY        0x10000
#define _FTEXT          0x20000
#define _FNOINHERIT	0x40000
#define _FDIRECT        0x80000
#define _FNOFOLLOW      0x100000
#define _FDIRECTORY     0x200000
#define _FEXECSRCH      0x400000

#define	O_APPEND	_FAPPEND
#define	O_CREAT		_FCREAT
#define	O_TRUNC		_FTRUNC
#define	O_EXCL		_FEXCL
#define O_SYNC		_FSYNC
#define	O_NONBLOCK	_FNONBLOCK
#define	O_NOCTTY	_FNOCTTY
#define O_BINARY	_FBINARY
#define O_TEXT		_FTEXT
#define O_CLOEXEC	_FNOINHERIT
#define O_DIRECT        _FDIRECT
#define O_NOFOLLOW      _FNOFOLLOW
#define O_DSYNC         _FSYNC
#define O_RSYNC         _FSYNC
#define O_DIRECTORY     _FDIRECTORY
#define O_EXEC          _FEXECSRCH
#define O_SEARCH        _FEXECSRCH

struct flock {
	short	l_type;		
	short	l_whence;	
	long	l_start;	
	long	l_len;		
	short	l_pid;		
};

#include "sys/types.h"

/**************** list of mandatory functions ***************/
//TODO
int fcntl(int fd, int cmd, int arg);
int open(const char * filename, int flags, ...);
/*
int  creat(const char *, mode_t);
int  openat(int, const char *, int, ...);
*/

#endif
