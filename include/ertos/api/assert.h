#ifndef _ASSERT_H_
#define _ASSERT_H_

void __assert_func(const char *, int, const char *, const char *);

#define NDEBUG

/**************** list of mandatory functions ***************/

#ifdef NDEBUG           /* required by ANSI standard */
# define assert(__e) ((void)0)
#else
# define assert(__e) ((__e) ? (void)0 : __assert_func (__FILE__, __LINE__, \
						       __FUNCTION__, #__e))
#endif


/**************** list of C extension functions ***************/

#endif
