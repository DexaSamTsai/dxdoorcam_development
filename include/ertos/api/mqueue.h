#ifndef _MQUEUE_H_
#define _MQUEUE_H_

struct mq_attr {
	long mq_flags;       /* Flags: 0 or O_NONBLOCK */
	long mq_maxmsg;      /* Max. # of messages on queue */
	long mq_msgsize;     /* Max. message size (bytes) */
	long mq_curmsgs;     /* # of messages currently in queue */
};

struct _s_mqd
{
	t_ertos_queue q;
	unsigned int padding;
	char * name;

	struct mq_attr attr;
	unsigned char * buf;
	unsigned int buf_len;

	//private
	struct _s_mqd * next;
};

typedef struct _s_mqd * mqd_t;

/**************** list of mandatory functions ***************/
/* if oflag & O_CREAT, the API is: mqd_t mq_open(const char *name, int oflag, mode_t mode, struct mq_attr *attr); */
/* if ! oflag & O_CREAT, the API is: mqd_t mq_open(const char *name, int oflag); */
mqd_t mq_open (const char *name, int oflag, ...);
int mq_close(mqd_t mqdes);
int mq_unlink(const char *name);
int mq_send(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned msg_prio);
int mq_timedsend(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned msg_prio, const struct timespec *abs_timeout);
ssize_t mq_receive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned *msg_prio);
ssize_t mq_timedreceive(mqd_t mqdes, char *msg_ptr, size_t msg_len, unsigned *msg_prio, const struct timespec *abs_timeout);
int mq_getattr(mqd_t mqdes, struct mq_attr *attr);
int mq_setattr(mqd_t mqdes, struct mq_attr *newattr, struct mq_attr *oldattr);
/*
int      mq_notify(mqd_t, const struct sigevent *);
*/

/**************** list of C extension functions ***************/

#endif
