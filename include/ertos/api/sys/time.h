#ifndef _SYS_TIME_H_
#define _SYS_TIME_H_

/*************** **************/
struct timeval {
	long tv_sec;		  /* seconds */
	long tv_usec;		  /* microseconds */
};

/*************** Obsolescent **********/
//struct itimerval

int gettimeofday(struct timeval* tv, void* timezone);

int settimeofday(const struct timeval *tv, void * tz);

#endif
