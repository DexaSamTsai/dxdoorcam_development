#ifndef _SYS_TYPES_H_
#define _SYS_TYPES_H_

#include "stddef.h"
#include "stdint.h"

typedef	unsigned char	u_char;
typedef	unsigned short	u_short;
typedef	unsigned int	u_int;
typedef	unsigned long	u_long;

typedef long blkcnt_t;
typedef long blksize_t ;
typedef unsigned long clock_t ;
typedef unsigned long clockid_t ;
typedef long dev_t ;
typedef unsigned long fsblkcnt_t ;
typedef unsigned long fsfilcnt_t ;
typedef int gid_t ;
typedef int id_t ;
typedef	unsigned long	ino_t;
typedef long key_t ;
typedef unsigned int mode_t;
typedef unsigned int nlink_t;
typedef long off_t;
typedef int pid_t ;
typedef int ssize_t;
typedef long suseconds_t;
typedef unsigned int time_t ;
typedef long timer_t ;
//trace_attr_t 
//trace_event_id_t 
//trace_event_set_t 
//trace_id_t 
typedef long uid_t ;

typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef signed short int __int16_t;
typedef unsigned short int __uint16_t;
typedef signed int __int32_t;
typedef unsigned int __uint32_t;
typedef unsigned long long __uint64_t;

typedef unsigned long useconds_t;

/* deprecated */
typedef __uint8_t	u_int8_t;
typedef __uint16_t	u_int16_t;
typedef __uint32_t	u_int32_t;
typedef __uint64_t	u_int64_t;

#endif
