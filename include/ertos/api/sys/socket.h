#ifndef _SYS_SOCKET_H_
#define _SYS_SOCKET_H_

#if LWIP_VERSION < 0x02000000
typedef u8 sa_family_t;

struct sockaddr_storage {
	u8 sa_len;
	sa_family_t sa_family;
	char sa_data[14];
};
#endif

#include "lwip/sockets.h"

struct hostent *gethostbyaddr(const void *addr, socklen_t len, int type);
int gethostbyaddr_r(const void *addr, socklen_t len, int type, struct hostent *ret, char *buf, size_t buflen, struct hostent **result, int *h_errnop);

#endif
