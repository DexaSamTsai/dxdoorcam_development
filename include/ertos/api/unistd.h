#ifndef _UNISTD_H_
#define _UNISTD_H_

#define	F_OK	0
#define	R_OK	4
#define	W_OK	2
#define	X_OK	1

#define STDIN_FILENO    0
#define STDOUT_FILENO   1
#define STDERR_FILENO   2


/**************** list of mandatory functions ***************/
int access(const char *pathname, int mode);
int chdir(const char *path);
int close(int fd);
ssize_t write(int fd, const void *buf, size_t count);
ssize_t read(int fd, void *buf, size_t count);
void _exit(int c);
int unlink(const char *path);
off_t lseek(int fd, off_t offset, int whence);
int rmdir(const char *pathname);
unsigned int sleep(unsigned int seconds);
/*
//TODO
int          ftruncate(int, off_t);
int          truncate(const char *, off_t);
*/

/******* NOT to support
unsigned     alarm(unsigned);
int          chown(const char *, uid_t, gid_t);
size_t       confstr(int, char *, size_t);
int          dup(int);
int          dup2(int, int);
int          execl(const char *, const char *, ...);
int          execle(const char *, const char *, ...);
int          execlp(const char *, const char *, ...);
int          execv(const char *, char *const []);
int          execve(const char *, char *const [], char *const []);
int          execvp(const char *, char *const []);
int          faccessat(int, const char *, int, int);
int          fchdir(int);
int          fchown(int, uid_t, gid_t);
int          fchownat(int, const char *, uid_t, gid_t, int);
int          fexecve(int, char *const [], char *const []);
pid_t        fork(void);
long         fpathconf(int, int);
char        *getcwd(char *, size_t);
*/
gid_t        getegid(void);
uid_t        geteuid(void);
gid_t        getgid(void);
/*
int          getgroups(int, gid_t []);
int          gethostname(char *, size_t);
char        *getlogin(void);
int          getlogin_r(char *, size_t);
int          getopt(int, char * const [], const char *);
pid_t        getpgid(pid_t);
pid_t        getpgrp(void);
*/
pid_t        getpid(void);
/*
pid_t        getppid(void);
pid_t        getsid(pid_t);
*/
uid_t        getuid(void);
/*
int          isatty(int);
int          lchown(const char *, uid_t, gid_t);
int          link(const char *, const char *);
int          linkat(int, const char *, int, const char *, int);
long         pathconf(const char *, int);
int          pause(void);
int          pipe(int [2]);
ssize_t      pread(int, void *, size_t, off_t);
ssize_t      pwrite(int, const void *, size_t, off_t);
ssize_t      readlink(const char *restrict, char *restrict, size_t);
ssize_t      readlinkat(int, const char *restrict, char *restrict, size_t);
int          setegid(gid_t);
int          seteuid(uid_t);
int          setgid(gid_t);
int          setpgid(pid_t, pid_t);
pid_t        setsid(void);
int          setuid(uid_t);
int          symlink(const char *, const char *);
int          symlinkat(const char *, int, const char *);
long         sysconf(int);
pid_t        tcgetpgrp(int);
int          tcsetpgrp(int, pid_t);
char        *ttyname(int);
int          ttyname_r(int, char *, size_t);
int          unlinkat(int, const char *, int);
*/

/**************** list of C extension functions ***************/
void sync(void);
int fsync(int fd);
int fdatasync(int fd);
/*
char        *crypt(const char *, const char *);
void         encrypt(char [64], int);
long         gethostid(void);
int          lockf(int, int, off_t);
int          nice(int);
pid_t        setpgrp(void);
int          setregid(gid_t, gid_t);
int          setreuid(uid_t, uid_t);
void         swab(const void *restrict, void *restrict, ssize_t);
*/

/* obselete */
int usleep(useconds_t usec);

#endif
