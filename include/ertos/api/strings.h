#ifndef _STRINGS_H_
#define _STRINGS_H_


/**************** list of mandatory functions ***************/
int strcasecmp(const char *s1, const char *s2);
int strncasecmp(const char *s1, const char *s2, size_t n);

/**************** list of C extension functions ***************/
int ffs(int n);
void bzero(void *s, size_t n);
int bcmp(const void *s1, const void *s2, size_t n);
void bcopy(const void *src, void *dest, size_t n);
char *index(const char *s, int c);
char *rindex(const char *s, int c);
/*
int    strcasecmp_l(const char *, const char *, locale_t);
int    strncasecmp_l(const char *, const char *, size_t, locale_t);
*/

#endif
