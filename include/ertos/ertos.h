#ifndef _ERTOS_H_
#define _ERTOS_H_

#include "stdio.h"

/**
 * @defgroup ertos
 * @breif ertos is OmniVision's RTOS for OV798.
 * @details 
 * @{
 */

#define TICKS_PER_SEC 100	///< ticks per second

/* critical section API 1 */
u32 local_irq_disable (void);
void local_irq_restore(u32 flags);
#define local_irq_save(flags) do{ flags=local_irq_disable(); }while(0)

/* critical section API 2 */
void sys_enter_critical(void);
void sys_exit_critical(void);

/** @} */

//INTERNAL_START
/**
 * @defgroup heap
 * @ingroup ertos
 * @brief 堆。用于malloc/free系列函数。
 * @details COFNIG_HEAP_EN使能堆。有三种方式来定义堆的位置: CONFIG_ERTOS_HEAP_IN_LD使用定义在ld文件里的堆起始和结束位置。CONFIG_ERTOS_HEAP_IN_ARRAY使用定义成数组的堆，大小为CONFIG_ERTOS_HEAP_SIZE。CONFIG_ERTOS_HEAP_IN_CONFIG由menuconfig来定义堆的起始地址和大小。
   在使用堆之前，必须已经调用ertos_init()
   支持的函数为ertos_malloc, ertos_free, ertos_calloc, ertos_realloc，跟标准的接口一样，函数名前多ertos_
 * @{
 */

extern u8 * rom_ertos_heap_start;
extern int rom_ertos_heap_size;
void * ertos_malloc(u32 size);
void ertos_free(void * ptr);
void * ertos_calloc(u32 nmemb, u32 size);
void * ertos_realloc(void *ptr, u32 size);
int ertos_heap_cur_free(void);
int ertos_heap_min_free(void);
/** @} */
//INTERNAL_END

//INTERNAL_START
/**
 * @defgroup tasks
 * @ingroup ertos
 * @brief 任务
 * @details ertos支持多个任务，每个任务可以设置0~30的优先级，高优先级(数字小)先执行，直到主动让出CPU。优先级相同的任务使用相同的时间片调度。
 每个任务需要一定的内存用作结构存储和栈，这个空间可以静态创建 也可以由系统自动调用malloc动态创建。
 在ertos_start之前，必须先创建至少一个任务. 创建任务必须在ertos_init()之后。

 静态数据结构创建任务:
 ERTOS_TASK_DECLARE(handler, stksize)
 ertos_task_create_fromhandler(handler, func_main, name, func_arg, prio);

 从malloc创建任务(CONFIG_ERTOS_HEAP_EN必须使能)
 t_ertos_taskhandler * handler = ertos_task_create(func_main, name, stk_depth, arg, prio);
 * @{
 */

#define ETASKLIST_VOLATILE //volatile
#define ETASKLIST_MAXVALUE 0xffffffffUL

typedef struct s_etasklist_item
{
	ETASKLIST_VOLATILE u32 value; //for sort
	struct s_etasklist_item * ETASKLIST_VOLATILE next;
	struct s_etasklist_item * ETASKLIST_VOLATILE prev;
	void * arg;
	struct s_etasklist * ETASKLIST_VOLATILE list;
}t_etasklist_item;

typedef struct s_etasklist
{
	ETASKLIST_VOLATILE int num;
	t_etasklist_item * ETASKLIST_VOLATILE index;
	t_etasklist_item end;
}t_etasklist;

#define ERTOS_TASK_NAMELEN 16
#define ERTOS_PRIO_MAX 32

typedef enum e_ertos_notify_status
{
	ERTOS_TASK_NOTIFY_NOTWAIT = 0,
	ERTOS_TASK_NOTIFY_WAIT,
	ERTOS_TASK_NOTIFY_NOTIFIED,
}t_ertos_notify_status;

typedef enum e_ertos_task_status
{
	ERTOS_TASK_RUNNING = 0,
	ERTOS_TASK_READY,
	ERTOS_TASK_DELAYED,
	ERTOS_TASK_SUSPEND,
	ERTOS_TASK_DELETED,
}t_ertos_task_status;

typedef struct s_ertos_task_atexit{
	void (*func)(void);
	void (*func_cleanup)(void * arg); //for pthread_cleanup_
	void * arg; //for pthread_cleanup_
	struct s_ertos_task_atexit * next;
}t_ertos_task_atexit;

typedef struct s_ertos_task {
	//Stk Ptr should be at first because assemble codes call it
    u32          *cur_stk;

	t_etasklist_item task_listitem;
	t_etasklist_item evt_listitem;

	u32 prio; //priority, from 0~30. 0 highest, 30 lowest.
	u32 * stk_start; //start of the stack
	u32 * stk_end; //end of the stack
	char name[ERTOS_TASK_NAMELEN]; //for debug only, name of the task

	//stdio
	struct s_FILE * _stdin;
	struct s_FILE * _stdout;
	struct s_FILE * _stderr;
	int _errno;
	t_ertos_task_atexit * atexit_funcs;
	u32 syslog_mask; //because default is all enabled, so 0 means enabled, 1 means disabled. BIT31 means the syslog is enabled
	char syslog_name[ERTOS_TASK_NAMELEN];

	//for priority change
#if 0
	//u32 base_prio;
#endif
	u32 mutex_hold;
	u32 stat_all_tickcnt; //total run ticks
	u32 stat_all_cyclecnt; //total run cycles
	u32 stat_cur_tickcnt; //ticks in current second
	u32 stat_cur_cyclecnt; //cycles in current second
	u32 stat_cur_tickstart; //current second start tick
	u32 stat_last_permil; //last second task CPU permillage
	u32 stat_start_cycle; //start run cycle
	u32 stat_start_tick; //start run tick
	u32 pid;
	//notification.
	volatile u32 notify_value;
	volatile t_ertos_notify_status notify_state;

	void * ret;
	void * ret_event;

#define _TASK_FLAGS_FROMHEAP	0x0001
#define _TASK_FLAGS_STKFROMHEAP	0x0002
#define _TASK_FLAGS_FROMPT		0x0004
#define _TASK_FLAGS_PT_DETACHED	0x0008
#define _TASK_FLAGS_PT_ENDED	0x0010
#define _TASK_FLAGS_CANCEL_DISABLED	0x0100
#define _TASK_FLAGS_CANCEL_DEFERRED	0x0200
#define _TASK_FLAGS_CANCEL_SET		0x0400
#define _TASK_FLAGS_CONDBROADCAST	0x1000
	u32 flags;

	u32 print_cnt; //for vfprintf(), re-entrant

	u32 watchdog_timeout; //in ticks
	u32 watchdog_lastrun_ticks; //last run ticks
	void * tsd; //thread-specific data
	u32 reserved3;
}t_ertos_task;

/**
 * t_ertos_taskhandler is task type reference.
 */
typedef void * t_ertos_taskhandler;

/**
 * @brief start task schedule. only called once after ertos_init. The codes after ertos_start() cannot run because ertos_start() will jump to the highest priority task.
 * //TODO
 */
void ertos_start (void);

//create task. before ertos_start, at least one ertos_task_create() must be called.
int ertos_task_create_internal(void * (*task_main)(void * arg), char * name, int stk_depth, void * arg, u32 prio, t_ertos_taskhandler * handler, u32 *stk, t_ertos_task * tcb);

/**
 * @REAL_FORMAT ERTOS_TASK_DECLARE(handler_name, stack_size);
 * @brief 静态声明task handler及task stack. 这个宏必须在函数体外使用（类似全局变量）
 * @param handler_name t_ertos_taskhandler结构的task handler，可以用于ertos_task_xxx()函数.
 * @param stack_size task stack size. The stack will be defined as array in 'data' section.
 */
#define ERTOS_TASK_DECLARE(handler, stksize) \
static u32 _task_##handler##_stk[stksize]; \
static t_ertos_task _task_##handler##_tcb = { \
	.flags = 0, \
}; \
static int _task_##handler##_stksize = stksize; \
t_ertos_taskhandler handler = & _task_##handler##_tcb;

/**
 * @REAL_FORMAT int ertos_task_create_fromhandler(t_ertos_taskhandler handler, void (*task_main)(void * arg), char * name, void * arg, u32 prio);
 * //TODO
 */
#define ertos_task_create_fromhandler(handler, func, name, func_arg, prio) ertos_task_create_internal(func, name, _task_##handler##_stksize, func_arg, prio, NULL, &_task_##handler##_stk[0], &_task_##handler##_tcb)

/**
 * @brief create task. the task stack is from heap. the task stack will also freed after ertos_task_delete().
 */
t_ertos_taskhandler ertos_task_create(void * (*task_main)(void * arg), char * name, int stk_depth, void * arg, u32 prio);

/**
 * @brief delete task. If handler == NULL, delete current task. If stack is from heap, free the stack.
 */
int ertos_task_delete(t_ertos_taskhandler handler);
void ertos_task_testdelete(void);
int ertos_task_set_deletestate(int disable);
int ertos_task_set_deletetype(int deferred);

/**
 * @brief suspend task. If handler == NULL, suspend current task. The suspended task only can be wakeup by ertos_task_resume
 */
void ertos_task_suspend(t_ertos_taskhandler handler);

/**
 * @brief resume task.
 */
int ertos_task_resume(t_ertos_taskhandler handler);

//return task status
t_ertos_task_status ertos_task_status(t_ertos_taskhandler handler);
//return task name
char * ertos_task_name(t_ertos_taskhandler handler);
//set the task name
char * ertos_task_set_name(t_ertos_taskhandler handle, char * name);

/**
 * @details set the task watchdog timeout
 * @param handle task handler. NULL for current task.
 * @param timeout timeout in ticks. 0:disable watchdog
 * @return 0:OK
 */
int ertos_task_set_watchdog(t_ertos_taskhandler handler, u32 timeout);
#ifdef CONFIG_ERTOS_WATCHDOG_EN
#ifndef CONFIG_ERTOS_WATCHDOG_TIMEOUT
#define CONFIG_ERTOS_WATCHDOG_TIMEOUT 20
#endif
#endif

/**
 * @brief return task priority
 */
u32 ertos_task_priority(t_ertos_taskhandler handler);

/**
 * @brief re-set task priority
 */
void ertos_task_priority_set(t_ertos_taskhandler handler, u32 new_prio);

/**
 * @brief delay current task.
 * 任务可以以ticks单位(10ms)进行休眠。当任务进入休眠后，会退出运行，定时器到时继续运行。 等待100ms: ertos_timedelay(10);
 * @param delayticks ticks to delay(1 tick = 10ms)
 */
void ertos_timedelay(u32 delayticks);

/**
 * @brief delay current task to *last_wakeup+inc_ticks. 
 * 休眠当前任务到*last_wakeup+inc_ticks
 * @param last_wakeup [in] set to ticks, or return value of previous timedelay_until
 * @param last_wakeup [out] return timedelay ticks value.
 */
void ertos_timedelay_until(u32 * last_wakeup, u32 inc_ticks);

void * ertos_task_idle (void * arg); //default idle task implementation
int ertos_task_listprint(int argc, char ** argv);

int ertos_watchdog_start(void);
int ertos_watchdog_stop(void);

/** @} */
//INTERNAL_END

//INTERNAL_START
/**
 * @brief queue类型结构，t_ertos_queuehandler
 */
typedef void * t_ertos_queuehandler ;
typedef enum e_ertos_queuetype
{
	ERTOS_QUEUE_QUEUE = 0,
	ERTOS_QUEUE_MUTEX,
	ERTOS_QUEUE_SEM,
	ERTOS_QUEUE_EVENT,
}t_ertos_queuetype;

typedef enum e_ertos_queuepos
{
	ERTOS_QUEUE_TOBACK = 0,
	ERTOS_QUEUE_TOFRONT = 1,
	ERTOS_QUEUE_OVERWRITE = 2,
}t_ertos_queuepos;
#define _ERTOS_QUEUEPOS_MASK		0x03
#define ERTOS_QUEUE_BROADCAST	0x80 //if 'pos' with this bit set, means broadcast.

typedef struct s_ertos_queue
{
	u8 * head; //null for mutex
	u8 * tail;
	u8 * write;
	u8 * read;
	u32 recurmutex_cnt;
	t_etasklist tasks_write;
	t_etasklist tasks_read;

	u32 depth;
	u32 itemsize;
	volatile u32 msgcnt;

	volatile u32 locked_read;
	volatile u32 locked_write;

	t_ertos_queuetype type;

	struct s_ertos_queue * qset;

	u8 from_heap; //if from heap, free when delete(0
	u8 inited;
}t_ertos_queue;

//INTERNAL_END

//INTERNAL_START
/**
 * @defgroup queue
 * @ingroup ertos
 * @brief 消息队列
 * @details 消息队列是任务间通信的一种，一般一个任务等待队列里的消息，其他任务或者中断处理函数往队列里添加消息。消息队列有2个参数:depth消息队列的深度（可以存储的消息个数）和itemsize每个消息的字节数。发送、获取消息都是内存拷贝整个消息到队列中，不是指针引用。
 消息队列数据结构可以静态分配，也可以动态分配。

 静态分配消息队列数据结构：
ERTOS_QUEUE_DECLARE(handler, depth, itemsize)
ertos_queue_init(handler);

 动态分配事件数据结构(CONFIG_ERTOS_HEAP_EN必须使能)
t_ertos_queuehandler handler = ertos_queue_create(depth, itemsize);

 添加消息
ertos_queue_send(handler, msg, timeout); //timeout:ticks为单位, msg:发送的消息，返回1成功，0失败

 等待消息
ertos_queue_recv(handler, msg, timeout); //msg:收到的消息，返回1成功，0失败

 删除消息队列
ertos_queue_delete(handler)
 * @{
 */

//@param buf [in] must >= (sizeof(t_ertos_queue)+depth*itemsize+1) bytes.

///< 如果timeout为WAIT_INF，意味着一直等待，不检查timeout
#define WAIT_INF 0xffffffffUL

t_ertos_queuehandler ertos_queue_create_internal(u8 * buf, u32 depth, u32 itemsize, t_ertos_queuetype type);
int ertos_queue_send_internal(t_ertos_queuehandler handler, void * item, int item_len, u32 timeout, u32 pos);
int ertos_queue_recv_internal(t_ertos_queuehandler handler, void * item, int * item_len, u32 timeout, int peek);
/**
 * @brief 静态定义queue, handler为全局变量, t_ertos_queuhandler结构
 * @param itemsize 消息的字节数
 */
#define ERTOS_QUEUE_DECLARE(handler, depth, itemsize) \
static u8 __attribute__  ((aligned(4))) _ertos_queuebuf_##handler[sizeof(t_ertos_queue) + depth * itemsize + 1]; \
t_ertos_queuehandler handler = (t_ertos_queuehandler)(&_ertos_queuebuf_##handler[0]); \
static u32 _ertos_queuedepth_##handler = depth; \
static u32 _ertos_queuesize_##handler = itemsize; 
/**
 * @brief 初始化queue, 清空队列
 */
#define ertos_queue_init(handler) ertos_queue_create_internal(handler, _ertos_queuedepth_##handler, _ertos_queuesize_##handler, ERTOS_QUEUE_QUEUE)
/**
 * @brief 动态申请并初始化queue
 */
t_ertos_queuehandler ertos_queue_create(u32 depth, u32 itemsize);
/**
 * @brief 发送消息到队列
 */
#define ertos_queue_send(handler, item, timeout) ertos_queue_send_internal(handler, item, -1, timeout, ERTOS_QUEUE_TOBACK)
/**
 * @brief 从队列接收消息
 */
#define ertos_queue_recv(handler, item, timeout) ertos_queue_recv_internal(handler, item, NULL, timeout, 0)
/**
 * @brief 删除队列
 */
void ertos_queue_delete(t_ertos_queuehandler handler);
/** @} */
//INTERNAL_END

//INTERNAL_START

//semaphore
/**
 * @defgroup semaphore
 * @ingroup ertos
 * @brief 信号量
 * @details 信号量是任务间通信的一种，一般一个任务等待一个信号，其他任务或者中断处理函数触发信号。信号量有计数，一次触发唤醒一次。
 信号量数据结构可以静态分配，也可以动态分配。

 静态分配信号量数据结构：
ERTOS_SEM_DECLARE(handler) 
ertos_sem_init(handler);

 动态分配信号量数据结构(CONFIG_ERTOS_HEAP_EN必须使能)
t_ertos_semhandler handler = ertos_sem_create();

 发送信号
ertos_sem_send(handler);

 等待信号
ertos_sem_recv(handler, timeout);

 删除信号量
ertos_sem_delete(handler)
 * @{
 */
/**
 * @brief semaphore类型结构，t_ertos_semhandler
 */
typedef t_ertos_queuehandler t_ertos_semhandler;
/**
 * @brief 静态定义semaphore, handler为全局变量, t_ertos_semhandler结构
 */
#define ERTOS_SEM_DECLARE(handler) \
static u8 __attribute__  ((aligned(4))) _ertos_sembuf_##handler[sizeof(t_ertos_queue) + 1]; \
t_ertos_semhandler handler = (t_ertos_semhandler)(&_ertos_sembuf_##handler[0]);
/**
 * @brief 初始化semaphore, 信号量为空
 */
#define ertos_sem_init(handler) ertos_queue_create_internal(handler, 1, 0, ERTOS_QUEUE_SEM)
/**
 * @brief 发送信号量
 */
#define ertos_sem_send(handler) ertos_queue_send_internal((handler), NULL, -1, 0, ERTOS_QUEUE_TOBACK)
/**
 * @brief 等待信号量,不能在中断里调用!
 * @ret 1: ready. 0: timeout.
 */
#define ertos_sem_recv(handler, timeout) ertos_queue_recv_internal((handler), NULL, NULL, (timeout), 0)

/**
 * @brief 动态申请并初始化信号量，信号量默认为空
 */
t_ertos_semhandler ertos_sem_create(void);
/**
 * @brief 删除信号量
 */
#define ertos_sem_delete ertos_queue_delete
/** @} */
//INTERNAL_END

//INTERNAL_START

//mutex
/**
 * @defgroup mutex
 * @ingroup ertos
 * @brief 互斥锁
 * @details 互斥锁

 静态分配互斥锁数据结构：
ERTOS_MUTEX_DECLARE(handler) 
ertos_mutex_init(handler);

 动态分配互斥锁数据结构(CONFIG_ERTOS_HEAP_EN必须使能)
t_ertos_mutexhandler handler = ertos_mutex_create();
 
 请求互斥锁
ertos_mutex_lock(handler, timeout)

 释放互斥锁
ertos_mutex_unlock(handler)

 删除互斥锁
ertos_mutex_delete(handler)
 * @{
 */
/**
 * @brief mutex类型结构，t_ertos_mutexhandler
 */
typedef t_ertos_queuehandler t_ertos_mutexhandler;
/**
 * @brief 静态定义mutex, handler为全局变量, t_ertos_mutexhandler结构
 */
#define ERTOS_MUTEX_DECLARE(handler) \
static u8 __attribute__  ((aligned(4))) _ertos_mutexbuf_##handler[sizeof(t_ertos_queue)]; \
t_ertos_mutexhandler handler = (t_ertos_mutexhandler)(&_ertos_mutexbuf_##handler[0]);
/**
 * @brief 初始化mutex
 */
t_ertos_queuehandler ertos_queue_create_mutex_internal(u8 * buf, t_ertos_queuetype type);
#define ertos_mutex_init(handler) ertos_queue_create_mutex_internal(handler, ERTOS_QUEUE_MUTEX)
/**
 * @brief 动态申请并初始化mutex
 */
t_ertos_mutexhandler ertos_mutex_create(void);
/**
 * @brief 请求mutex锁
 */
#define ertos_mutex_lock(handler, timeout) ertos_sem_recv(handler, timeout)
/**
 * @brief 释放mutex锁
 */
#define ertos_mutex_unlock(handler) ertos_sem_send(handler)
/**
 * @brief 删除mutex锁
 */
#define ertos_mutex_delete ertos_queue_delete

/** @} */
//INTERNAL_END

//INTERNAL_START

//event
/**
 * @defgroup event
 * @ingroup ertos
 * @brief 事件
 * @details 事件是任务间通信的一种，一般一个任务等待一个事件，其他任务或者中断处理函数触发事件。事件没有计数，一次等待成功将所有触发的都清零。
 事件数据结构可以静态分配，也可以动态分配。

 静态分配事件数据结构：
ERTOS_EVENT_DECLARE(handler) //声明事件
ertos_event_init(handler);

 动态分配事件数据结构(CONFIG_ERTOS_HEAP_EN必须使能)
t_ertos_eventhandler handler = ertos_event_create();

 使用事件
ertos_event_signal(handler); //发送事件
ertos_event_wait(handler, 100); //等待事件

 删除事件
ertos_event_delete(handler)
 * @{
 */
/**
 * @brief event类型结构，t_ertos_eventhandler
 */
typedef t_ertos_queuehandler t_ertos_eventhandler;
/**
 * @brief 静态定义event, handler为全局变量, t_ertos_eventhandler结构
 */
#define ERTOS_EVENT_DECLARE(handler) \
static u8 __attribute__  ((aligned(4))) _ertos_eventbuf_##handler[sizeof(t_ertos_queue) + 1]; \
t_ertos_eventhandler handler = (t_ertos_eventhandler)(&_ertos_eventbuf_##handler[0]);
/**
 * @brief 初始化event,事件为空 
 */
#define ertos_event_init(handler) ertos_queue_create_internal((u8 *)(handler), 1, 0, ERTOS_QUEUE_EVENT)
/**
 * @brief 发送事件，如果有多个线程wait的，唤醒其中一个。
 */
#define ertos_event_signal(handler) ertos_queue_send_internal((handler), NULL, -1, 0, ERTOS_QUEUE_OVERWRITE)
/**
 * @brief 广播事件，如果有多个线程wait的，全部唤醒
 */
#define ertos_event_broadcast(handler) ertos_queue_send_internal((handler), NULL, -1, 0, ERTOS_QUEUE_OVERWRITE | ERTOS_QUEUE_BROADCAST )
/**
 * @brief 等待事件,不能在中断里调用!
 * @ret 1: ready. 0: timeout.
 */
#define ertos_event_wait(handler, timeout) ertos_queue_recv_internal((handler), NULL, NULL, (timeout), 0)
/**
 * @brief 动态申请并初始化事件，事件默认为空
 */
t_ertos_eventhandler ertos_event_create(void);
/**
 * @brief 删除事件
 */
#define ertos_event_delete ertos_queue_delete
/** @} */
//INTERNAL_END

//INTERNAL_START

//qset
/**
 * @defgroup qset
 * @ingroup ertos
 * @brief 消息组
 * @details 消息组用来让一个task等待多个消息(queue或者semaphore)
参考代码:
qset = ertos_qset_create();
q1 = ertos_queue_create();
q2 = ertos_queue_create();
q3 = ertos_sem_create();
ertos_qset_add(q1, qset);
ertos_qset_add(q2, qset);
ertos_qset_add(q3, qset);
while(1){
	void * ret = ertos_qset_select(qset, 1000);
	if(ret == q1){
		//q1有事件
		ertos_queue_recv(ret, &v1, 0);
	}else if(ret == q2){
		//q2有事件
		ertos_queue_recv(ret, &v1, 0);
	}else if(ret == q3){
		//q3有事件
		ertos_sem_recv(ret, 0);
	}else{
		//timeout
	}
}
 * @{
 */
/**
 * @brief qset类型结构，t_ertos_qsethandler
 */
typedef t_ertos_queuehandler t_ertos_qsethandler;
/**
 * @brief 静态定义qset, handler为全局变量, t_ertos_qsethandler结构
 * @param depth 最多保存消息的个数。一般建议qset里每个queue/semaphore的深度和。
 */
#define ERTOS_QSET_DECLARE(handler, depth) \
static u8 __attribute__  ((aligned(4))) _ertos_qsetbuf_##handler[sizeof(t_ertos_queue) + depth * sizeof(t_ertos_queuehandler) + 1]; \
t_ertos_qsethandler handler = (t_ertos_qsethandler)(&_ertos_qsetbuf_##handler[0]); \
static u32 _ertos_qsetdepth_##handler = depth; \

#define ERTOS_QUEUE_QSET ERTOS_QUEUE_QUEUE
/**
 * @brief 初始化qset, 清空队列
 */
#define ertos_qset_init(handler) ertos_queue_create_internal(handler, _ertos_qsetdepth_##handler, sizeof(t_ertos_queuehandler), ERTOS_QUEUE_QSET)
/**
 * @brief 动态申请并初始化qset
 */
t_ertos_qsethandler ertos_qset_create(u32 depth);
/**
 * @brief 添加queue/semaphore到qset
 * @return 0:failed. 1:success
 */
int ertos_qset_add(void * queue_sem, t_ertos_qsethandler qset);
/**
 * @brief 从qset删除queue/semaphore
 * @return 0:failed. 1:success
 */
int ertos_qset_del(void * queue_sem, t_ertos_qsethandler qset);
/**
 * @brief 等待qset
 * @return t_ertos_queuehandler或者t_ertos_semhandler,或者NULL(timeout)
 */
void * ertos_qset_select(t_ertos_qsethandler qset, u32 timeout);
/** @} */
//INTERNAL_END

//INTERNAL_START

//notifications
/**
 * @defgroup notifications
 * @ingroup ertos
 * @brief 通知
 * @details
 每个task有一个32bits的通知值。一个task可以等通知，另外的task可以通过发送通知让等通知的task继续运行。

 通知比队列、信号量简单，因为不需要建立这些变量。速度也更快。

 信号通知是通知的一种简化版本，只是用来等待、唤醒，类似event。是一种没有值传递的通知。

 通知用法:
 发送通知
 ertos_notify_send
 等待通知
 ertos_notify_recv

 信号通知用法:
 发送信号通知
 ertos_notify_signal
 等待信号通知
 ertos_notify_wait
 * @{
 */
typedef enum e_ertos_notify_action
{
	ERTOS_NOTIFY_NOACTION = 0, ///< 不更新通知值
	ERTOS_NOTIFY_SETBIT, ///< 设置通知值的某些比特
	ERTOS_NOTIFY_INCREMENT, ///< 增加通知值
	ERTOS_NOTIFY_SETOVERWRITE, ///< 设置通知值，如果有未读通知值，覆盖
	ERTOS_NOTIFY_SET, ///< 设置通知值，如果有未读通知值，返回错误
}t_ertos_notify_action;

/**
 * @brief 发送通知
 * @return 1:OK. 0:failed
 */
int ertos_notify_send(t_ertos_taskhandler handler, u32 value, t_ertos_notify_action act);

/**
 * @brief 等待通知
 * @param bits_clear_entry 等待前要清除的通知值比特，如果为0xffffffff就是清零。
 * @param bits_clear_exit 等待到以后要清除的通知值比特
 * @param value 返回获取到的通知值，可以为NULL
 * @param timeout 要等待的tick, WAIT_INF就是一直等待，0就是不等待
 * @return 1:OK, 0:failed
 */
int ertos_notify_recv(u32 bits_clear_entry, u32 bits_clear_exit, u32 * value, u32 timeout);

/**
 * @brief 发送信号通知
 */
#define ertos_notify_signal(task) ertos_notify_send((task), 0, ERTOS_NOTIFY_INCREMENT)

/**
 * @brief 等待信号通知
 * @return 0:timeout >0:notify ready
 */
u32 ertos_notify_wait(int clearall, u32 timeout);
/** @} */
//INTERNAL_END

/**
 * @defgroup irq
 * @ingroup ertos
%en * @brief interrupt
%zh * @brief 中断
%en * @details support dynamic interrupt handler reg/dereg. Support one interrupt handler for one IRQ.
%zh * @details 中断处理函数可以动态注册、删除。一个中断只支持注册一个中断处理函数。中断必须在ertos_init之后才可以使用。
 * @{
 */
/**
%en * @brief interrupt handler type, t_irq_handler
%zh * @brief 中断处理函数类型结构，t_irq_handler
 */
typedef int (*t_irq_handler)(void *);

//INTERNAL_START
typedef struct s_irqaction
{
	t_irq_handler handler;
	char name[ERTOS_TASK_NAMELEN]; //for debug only, name of the irq
	void * arg;
	unsigned int cnt; //stat
}t_irqaction;
//INTERNAL_END

/**
%en_start
 * @brief register interrupt handler
 * @param irq IRQ number
 * @param handler interrupt handler
 * @param name string name, just for debug purpose
 * @param arg argument for interrupt handler
%en_end
%zh_start
 * @brief 注册中断处理函数
 * @param irq 中断号
 * @param handler 中断处理函数
 * @param name 中断名，调试用
 * @param arg 传递给中断处理函数的参数
%zh_end
 * @return 0:success <0:failed
 */
int irq_request(unsigned int irq, t_irq_handler handler, char * name, void * arg);

/**
%en * @brief deregister interrupt handler
%zh * @brief 删除中断处理函数
%en * @param irq IRQ number
%zh * @param irq 中断号
 * @return 0:success <0:failed
 */
int irq_free(unsigned int irq);

/** @} */

//INTERNAL_START
//timer
/**
 * @defgroup timer
 * @ingroup ertos
 * @brief Timer定时器
 * @details

定义初始化timer:
静态定义:
ERTOS_TIMER_DECLARE
ertos_timer_init
动态申请(HEAP):
ertos_timer_create

启动timer
ertos_timer_start

停止timer
ertos_timer_stop

释放timer
ertos_timer_delete
 * @{
 */
#define TIMER_QUEUE_LENGTH 8
typedef void * t_ertos_timerhandler;
typedef void (* t_ertos_timer_callback)(t_ertos_timerhandler timer);
typedef void (* t_ertos_timer_pended_func)(void *, u32);

#define TIMER_CMD_EXEC				(-1)
#define TIMER_CMD_START_NOTRACE		(0)
#define TIMER_CMD_START				(1)
#define TIMER_CMD_RESET				(2)
#define TIMER_CMD_STOP				(3)
#define TIMER_CMD_CHANGE_PERIOD		(4)
#define TIMER_CMD_DELETE			(5)


typedef struct s_ertos_timer
{
	char name[ERTOS_TASK_NAMELEN];
	t_etasklist_item listitem;
	u32 period;
	void * arg;
	t_ertos_timer_callback func;
	u8 reload;
	u8 from_heap;
}t_ertos_timer;

typedef struct s_ertos_timer_param
{
	u32 value;
	t_ertos_timer * timer;
}t_ertos_timer_param;

typedef struct s_ertos_timer_pended_param
{
	t_ertos_timer_pended_func func;
	void * param1;
	u32 param2;
}t_ertos_timer_pended_param;

typedef struct s_ertos_timer_queue_msg
{
	int msg_id;
	union
	{
		t_ertos_timer_param param;
		t_ertos_timer_pended_param pended_param;
	};
}t_ertos_timer_queue_msg;
void * ertos_task_timer (void * arg);
t_ertos_timerhandler ertos_timer_create_internal(u8 * buf, char * name, u32 period, u8 reload, void * arg, t_ertos_timer_callback func);
int timer_command_internal(t_ertos_timer * pt, int cmd_id, u32 value, int * woken, u32 timeout);

/**
 * @brief 静态定义timer, handler为全局变量, t_ertos_timerhandler结构
 */
#define ERTOS_TIMER_DECLARE(handler) \
static u8 __attribute__  ((aligned(4))) _timer_buf_##handler[sizeof(t_timer)]; \
t_ertos_timerhandler handler = (t_ertos_timerhandler)(&_timer_buf_##handler[0]);

/**
 * @brief 初始化timer
 */
#define ertos_timer_init(handler, name, period, reload, arg, func) ertos_timer_create_internal(handler, name, period, reload, arg, func)

/**
 * @brief 动态申请timer(需要HEAP使能)
 * @param period 间隔，ticks单位
 * @param reload 1:重复执行 0:执行一次
 * @param arg 参数，给ertos_timer_get_arg()使用
 * @param func 回调函数, void func(t_ertos_timerhandler)结构
 * @return NULL:失败, !NULL:成功
 */
t_ertos_timerhandler ertos_timer_create(char * name, u32 period, u8 reload, void * arg, t_ertos_timer_callback func);

/**
 * @brief timer是否启动
 * @return 1: is active 0:not active
 */
int ertos_timer_is_active(t_ertos_timerhandler t);

/**
 * @brief 启动timer
 * @param timer t_ertos_timerhandler
 * @param timeout 等待启动的timeout,因为启动timer是向timer task发消息，所以可能timeout
 * @return 0:启动失败,timeout 1:成功
 */
#define ertos_timer_start(timer, timeout) timer_command_internal((t_ertos_timer *)(timer), TIMER_CMD_START, ticks, NULL, timeout)

/**
 * @brief 停止timer
 * @param timer t_ertos_timerhandler
 * @param timeout 等待停止的timeout,因为停止timer是向timer task发消息，所以可能timeout
 * @return 0:停止失败,timeout 1:成功
 */
#define ertos_timer_stop(timer, timeout) timer_command_internal((t_ertos_timer *)(timer), TIMER_CMD_STOP, 0, NULL, timeout)

/**
 * @brief 更改timer周期
 * @param timer t_ertos_timerhandler
 * @param newperiod new period in ticks
 * @param timeout
 * @return 0:失败,timeout 1:成功
 */
#define ertos_timer_change_period(timer, newperiod, timeout) timer_command_internal((t_ertos_timer *)(timer), TIMER_CMD_CHANGE_PERIOD, newperiod, NULL, timeout)

/**
 * @brief 删除timer
 * @param timer t_ertos_timerhandler
 * @param timeout timeout,向timer task发消息，所以可能timeout
 * @return 0:失败,timeout 1:成功
 */
#define ertos_timer_delete(timer, timeout) timer_command_internal((t_ertos_timer *)(timer), TIMER_CMD_DELETE, 0, NULL, timeout)

/**
 * @brief 重新启动timer
 * @param timer t_ertos_timerhandler
 * @param timeout timeout,向timer task发消息，所以可能timeout
 * @return 0:失败,timeout 1:成功
 */
#define ertos_timer_reset(timer, timeout) timer_command_internal((t_ertos_timer *)(timer), TIMER_CMD_RESET, ticks, NULL, timeout)

/**
 * @brief 获取arg
 * @details 同一个timer回调函数func可能需要不同的参数，就是在ertos_timer_create()或者ertos_timer_init()函数里的arg参数。但是由于func回调函数的参数是一个t_ertos_timerhandler结构，所以要获取传进去的arg，需要ertos_timer_get_arg函数
 * @param t t_ertos_timerhandler
 * @return void * arg
 */
void * ertos_timer_get_arg(t_ertos_timerhandler * t);

/**
 * @brief 设置arg
 */
void ertos_timer_set_arg(t_ertos_timerhandler * t, void * arg);
/** @} */
//INTERNAL_END

/**
 * @defgroup cmdshell
 * @ingroup ertos
%en * @brief command shell
%zh * @brief 命令行
%en * @details command shell to debug/test rtos and run application.
%zh * @details 命令行接口用来调试/测试ertos
 * @{
 */
#define ERTOS_CMDSHELL_CMDLEN 16
#define ERTOS_CMDSHELL_ARGMAX 8
typedef struct s_cmdshell_cmd
{
	char cmd[ERTOS_CMDSHELL_CMDLEN];
	int (*handler)(int argc, char ** argv);
	char * comment; //help string
	struct s_cmdshell_cmd * next;
}t_cmdshell_cmd;

//INTERNAL_START
typedef struct s_cmdshell_history
{
	char * buf;
	int onelen; //one command history length
	int maxcnt; //max command history count
	int curpos; //current position in command history, 0~maxcnt-1
	int cur; //0: use input buf. 1~maxcnt: history copied to input buf.
}t_cmdshell_history;

typedef struct s_cmdshell
{
	//cmdshell only buffer one line input from UART.
	char * buf;
	int bufsize;
	int buf_cnt;
	int buf_full;
	t_ertos_eventhandler event; //wait in cmdshell_task() and send in cmdshell_input()
	struct s_cmdshell_cmd * handlers;
	//history
	struct s_cmdshell_history * history;
	//esc + [ + A = UP, B=DOWN, C=RIGHT, D=LEFT
	int escmode; //1:esc, 2:esc+[, 0:normal
	//used for command parsing
	char * argv[ERTOS_CMDSHELL_ARGMAX];
	int argc;
#define CMDSHELL_INBUF_SIZE	0x20
	char inbuf[CMDSHELL_INBUF_SIZE];
	int inbuf_cnt;
}t_cmdshell;

#ifndef CONFIG_ERTOS_CMDSHELL_HISTORY_CNT
#define CONFIG_ERTOS_CMDSHELL_HISTORY_CNT 3
#endif

extern t_cmdshell * g_cmdshell; //defined in ROM and pointer to share/
int cmdshell_init(t_cmdshell * s); //used by share/ertos/init.c to init cmdshell
int cmdshell_register_all(void); //used by share/ertos/init.c to register all the cmds declared by CMDSHELL_DECLARE
void * cmdshell_task(void * arg); //used by share/ertos/init.c to create the cmdshell task
int cmdshell_input_char(char c); //used by input(UART) RX IRQ handler.
int cmdshell_input_notify(void); //used by input(UART) RX IRQ handler

#define __cmdshell_cmd 		__attribute__ ((__section__ (".cmdshells")))

#define CMDSHELL_DECLARE(name) \
__cmdshell_cmd t_cmdshell_cmd __cmdshell_dec_##name = {

/**
 * @brief register a cmdshell
 */
int cmdshell_register(t_cmdshell_cmd * cmd);

/**
 * @brief deregister a cmdshell
 */
int cmdshell_deregister(t_cmdshell_cmd * cmd);

/**
 * @brief add a cmdshell
 * @details this API use malloc to malloc a t_cmdshell_cmd struct, and call cmdshell_register()
 */
int cmdshell_add(char * cmd, int (*handler)(int argc, char ** argv), char * comment );

/**
 * @brief delete a cmdshell
 * @details this API call cmdshell_deregister first and then free the t_cmdshell_cmd struct
 */
int cmdshell_del(char * cmd);
//INTERNAL_END

/** @} */

#include "api/time.h"

//INTERNAL_START
typedef struct s_ertos
{
	//these must not be changed, reset.S used them
	volatile t_ertos_task * cur_task;
	volatile t_ertos_task * next_task;
#ifdef CHIP_R2
	volatile u8 inirq;
	u8 unused;
	u8 unused1;
	u8 unused2;
#else
	volatile u32 inirq; //32bits inirq
#endif

	u8 schedule_running;
	//reset.S used above

	//public setting
#define ERTOS_DEBUG_STACK_CHECK_RUNTIME	BIT0
#define ERTOS_DEBUG_STACK_STATISTICS	BIT1
#define ERTOS_DEBUG_MALLOC_BZERO		BIT2
#define ERTOS_DEBUG_STAT				BIT3
#define ERTOS_DEBUG_WATCHDOG			BIT4
	u8 ertos_debug;
	void (*stack_overflow_cb)(t_ertos_taskhandler handler);
	void (*cb_start_schedule)(void);

	//public read
	u32 tasks_cnt; //current tasks

	//private
	u32 tasks_prio_ready; //bit31: prio0, bit0: prio31
	t_etasklist tasks_ready[ERTOS_PRIO_MAX];
	t_etasklist tasks_delay_list;
	t_etasklist tasks_overdelay_list; //overflow delayed list. handle for tick overflow.
	t_etasklist tasks_toready_list;
	t_etasklist tasks_waitcancel_list;
	t_etasklist tasks_suspend_list;

	volatile u32 waitcancel_cnt;
	volatile u32 next_delayready_ticks;

	u32 pid;

	t_irqaction irqactions[CHIP_IRQ_CNT];

	t_etasklist timer_list1;
	t_etasklist timer_list2;
	t_etasklist * timer_cur_list;
	t_etasklist * timer_overflow_list;
	t_ertos_queue * timer_q;
	u32 timer_last_switch;

	//debug
	u32 text_start;
	u32 text_end;
	u32 data_start;
	u32 data_end;
	u32 bss_start;
	u32 bss_end;
	t_ertos_task * idletask;
	void (* malloc_failure_hook)(u32 size);
	void (* free_failure_hook)(void * ptr, int reason);
	u32 heap_debug_extrasize;
	void (* idle_loop)(void);
	u8 initial_cpsr;
	u8 loglevel_default;
	u8 reserved1;
	u8 syslog_to_stdout;
	void (* cb_syslog_ertos)(int priority, const char * fmt0, va_list ap);
	struct s_syslog_cfg * syslog_cfg;
	u32 cycles_per_tick;
	void (* cb_debug_stat)(void);

	//for api
	char * _strtok_last;
	struct tm _localtime_buf;
	char _asctime_buf[26];

	u32 reserved11;
	u32 reserved12;
}t_ertos;

extern t_ertos * g_ertos; //in ROM
int rom_ertos_init(t_ertos * cfg);
int irq_listprint(int argc, char ** argv);
extern u8 syslog_noos_level;

typedef struct s_rom_callbacks
{
	int (* cb_rom_ertos_heap_init)(void);
	void * (* cb_ertos_malloc)(u32 size);
	void (* cb_ertos_free)(void * ptr);
}t_rom_callbacks;

extern t_rom_callbacks * g_rom_callbacks;

int ertos_init(void);
//INTERNAL_END

#endif
