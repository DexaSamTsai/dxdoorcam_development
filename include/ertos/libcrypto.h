#ifndef _LIBCRYPTO_H_
#define _LIBCRYPTO_H_

void libcrypto_hmacsha1(u8 *data, int data_length, u8 *key, int key_length, u8 *digest);

int urlencode(char * outbuf, int outbuf_len, char * inbuf);
int urldecode(char * outbuf, int outbuf_len, char * inbuf); //it's OK that outbuf and inbuf are same.

/**
 * @brief base64_encode
 * @details this func do base64 encode
 * @param outbuf
 * @param outlen
 * @param inbuf
 * @param inlen
 * @ingroup lib_util
 */
int libcrypto_base64_encode(char *outbuf, int *outlen, const u8 *inbuf, int inlen );

int libcrypto_base64_decode( u8 *outbuf, int *outlen, const char *inbuf, int inlen );

unsigned short crc16_ccitt(const void *buf, int len);

/**
 * @brief SHA512
 */
typedef struct s_sha512_ctx {
    u32 Intermediate_Hash[16]; //64/4
    u32 Length[4];
    u32 Message_Block_Index;
    u8 Message_Block[128]; //SHA512_Message_Block_Size
    int Computed;
    int Corrupted;
} t_sha512_ctx;

enum {
    shaSuccess = 0,
    shaNull,            /* Null pointer parameter */
    shaInputTooLong,    /* input data too long */
    shaStateError,      /* called Input after FinalBits or Result */
    shaBadParam         /* passed a bad parameter */
};

int sha512_reset(t_sha512_ctx *context);
int sha512_input(t_sha512_ctx *context, u8 * msg, unsigned int length);
int sha512_result(t_sha512_ctx *context, u8 * digest);

/**
 * @brief ed25519
 */
int ed25519_gen_keypair(unsigned char *pk,unsigned char *sk); /* pk/sk: 32 bytes, if sk[0] != 0, don't generate sk */
int ed25519_verify(unsigned char *m, int mlen, unsigned char *sign, const unsigned char *pk);
int ed25519_sign(unsigned char *m, int mlen, unsigned char * pk, unsigned char * sk, unsigned char * sign);


#endif
