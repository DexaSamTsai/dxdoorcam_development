#ifndef _SYSTRACE_H_
#define _SYSTRACE_H_

void systrace_start(void);
void systrace_add(u32 tag);
void systrace_print(void);

#endif
