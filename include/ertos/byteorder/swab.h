#ifndef _SWAB_H
#define _SWAB_H

#define __attribute_const__     __attribute__((__const__))

#define ___constant_swab16(x) ((u16)(				\
	(((u16)(x) & (u16)0x00ffU) << 8) |			\
	(((u16)(x) & (u16)0xff00U) >> 8)))

#define ___constant_swab32(x) ((u32)(				\
	(((u32)(x) & (u32)0x000000ffUL) << 24) |		\
	(((u32)(x) & (u32)0x0000ff00UL) <<  8) |		\
	(((u32)(x) & (u32)0x00ff0000UL) >>  8) |		\
	(((u32)(x) & (u32)0xff000000UL) >> 24)))

#define ___constant_swahw32(x) ((u32)(			\
	(((u32)(x) & (u32)0x0000ffffUL) << 16) |		\
	(((u32)(x) & (u32)0xffff0000UL) >> 16)))

#define ___constant_swahb32(x) ((u32)(			\
	(((u32)(x) & (u32)0x00ff00ffUL) << 8) |		\
	(((u32)(x) & (u32)0xff00ff00UL) >> 8)))

static inline __attribute_const__ u16 __fswab16(u16 val)
{
	return ___constant_swab16(val);
}

static inline __attribute_const__ u32 __fswab32(u32 val)
{
	return ___constant_swab32(val);
}

static inline __attribute_const__ u32 __fswahw32(u32 val)
{
	return ___constant_swahw32(val);
}

static inline __attribute_const__ u32 __fswahb32(u32 val)
{
	return ___constant_swahb32(val);
}

#define __swab16(x)				\
	(__builtin_constant_p((u16)(x)) ?	\
	___constant_swab16(x) :			\
	__fswab16(x))

#define __swab32(x)				\
	(__builtin_constant_p((u32)(x)) ?	\
	___constant_swab32(x) :			\
	__fswab32(x))

#define __swahw32(x)				\
	(__builtin_constant_p((u32)(x)) ?	\
	___constant_swahw32(x) :		\
	__fswahw32(x))

#define __swahb32(x)				\
	(__builtin_constant_p((u32)(x)) ?	\
	___constant_swahb32(x) :		\
	__fswahb32(x))

#define swab16 __swab16
#define swab32 __swab32
#define swahw32 __swahw32
#define swahb32 __swahb32

#endif
