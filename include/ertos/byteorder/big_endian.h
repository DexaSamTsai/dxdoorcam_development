#ifndef _BYTEORDER_BIG_ENDIAN_H
#define _BYTEORDER_BIG_ENDIAN_H

#ifndef __BIG_ENDIAN
#define __BIG_ENDIAN 4321
#endif

#ifndef __BIG_ENDIAN_BITFIELD
#define __BIG_ENDIAN_BITFIELD
#endif

#include "swab.h"

#define __constant_htonl(x) (( __be32)(u32)(x))
#define __constant_ntohl(x) (( u32)(__be32)(x))
#define __constant_htons(x) (( __be16)(u16)(x))
#define __constant_ntohs(x) (( u16)(__be16)(x))
#define __constant_cpu_to_le32(x) (( __le32)___constant_swab32((x)))
#define __constant_le32_to_cpu(x) ___constant_swab32(( u32)(__le32)(x))
#define __constant_cpu_to_le16(x) (( __le16)___constant_swab16((x)))
#define __constant_le16_to_cpu(x) ___constant_swab16(( u16)(__le16)(x))
#define __constant_cpu_to_be32(x) (( __be32)(u32)(x))
#define __constant_be32_to_cpu(x) (( u32)(__be32)(x))
#define __constant_cpu_to_be16(x) (( __be16)(u16)(x))
#define __constant_be16_to_cpu(x) (( u16)(__be16)(x))
#define __cpu_to_le32(x) (( __le32)__swab32((x)))
#define __le32_to_cpu(x) __swab32(( u32)(__le32)(x))
#define __cpu_to_le16(x) (( __le16)__swab16((x)))
#define __le16_to_cpu(x) __swab16(( u16)(__le16)(x))
#define __cpu_to_be32(x) (( __be32)(u32)(x))
#define __be32_to_cpu(x) (( u32)(__be32)(x))
#define __cpu_to_be16(x) (( __be16)(u16)(x))
#define __be16_to_cpu(x) (( u16)(__be16)(x))

#endif
