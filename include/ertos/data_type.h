#ifndef _DATA_TYPE_H_
#define _DATA_TYPE_H_

/**
 * @defgroup data type
 * @brief data type definition
 * @{
 */

#define BIG_ENDIAN 4321
#define LITTLE_ENDIAN 1234

#ifndef __u8__
#define __u8__
typedef unsigned char  		u8;
#endif

#ifndef __s8__
#define __s8__
typedef signed   char  		s8;
#endif

#ifndef __u16__
#define __u16__
typedef unsigned short 		u16;
#endif

#ifndef __s16__
#define __s16__
typedef signed   short		s16;
#endif

#ifndef __u32__
#define __u32__
typedef unsigned int  		u32;
#endif

#ifndef __s32__
#define __s32__
typedef signed   int  		s32;
#endif

#ifndef __u64__
#define __u64__
typedef unsigned long long 	u64;
#endif

#ifndef __s64__
#define __s64__
typedef signed   long long 	s64;
#endif

#ifndef __u8_t__
#define __u8_t__
typedef unsigned char 	u8_t;
#endif

#ifndef __s8_t__
#define __s8_t__
typedef signed char		s8_t;
#endif

#ifndef __u16_t__
#define __u16_t__
typedef unsigned short	u16_t;
#endif

#ifndef __s16_t__
#define __s16_t__
typedef signed short	s16_t;
#endif

#ifndef __u32_t__
#define __u32_t__
typedef unsigned int 	u32_t;
#endif

#ifndef __s32_t__
#define __s32_t__
typedef signed int 		s32_t;
#endif

//u*le type indicate that we hope data variable is little endian
#ifndef __u16le__
#define __u16le__
typedef unsigned short		u16le;
#endif

#ifndef __u32le__
#define __u32le__
typedef unsigned int  		u32le;
#endif

//u*be type indicate that we hope data variable is big endian
#ifndef __u16be__
#define __u16be__
typedef unsigned short		u16be;
#endif

#ifndef __u32be__
#define __u32be__
typedef unsigned int  		u32be;
#endif

////////////////////////////////////////////////////
#define   BIT0 		 0x00000001
#define   BIT1 		 0x00000002
#define   BIT2 		 0x00000004
#define   BIT3 		 0x00000008
#define   BIT4 		 0x00000010
#define   BIT5 		 0x00000020
#define   BIT6 		 0x00000040
#define   BIT7 		 0x00000080
#define   BIT8           0x00000100
#define   BIT9           0x00000200
#define   BIT10          0x00000400
#define   BIT11          0x00000800
#define   BIT12          0x00001000
#define   BIT13          0x00002000
#define   BIT14          0x00004000
#define   BIT15          0x00008000
#define   BIT16          0x00010000
#define   BIT17          0x00020000
#define   BIT18          0x00040000
#define   BIT19          0x00080000
#define   BIT20          0x00100000
#define   BIT21          0x00200000
#define   BIT22          0x00400000
#define   BIT23          0x00800000
#define   BIT24          0x01000000
#define   BIT25          0x02000000
#define   BIT26          0x04000000
#define   BIT27          0x08000000
#define   BIT28          0x10000000
#define   BIT29          0x20000000
#define   BIT30          0x40000000
#define   BIT31          0x80000000


typedef enum {
	VIDEO_SIZE_160_112 = (160<<16) | 112,
	VIDEO_SIZE_QQVGA = (160<<16) | 120,
	VIDEO_SIZE_160_128 = (160<<16) | 128,
	VIDEO_SIZE_QCIF = (176<<16) | 144,
	VIDEO_SIZE_256_144 = (256<<16) | 144,
	VIDEO_SIZE_272_144 = (272<<16) | 144,
	VIDEO_SIZE_320_180 = (320<<16) | 180,
	VIDEO_SIZE_320_192 = (320<<16) | 192,
	VIDEO_SIZE_QVGA = (320<<16) | 240,
	VIDEO_SIZE_CIF = (352<< 16) | 288,
	VIDEO_SIZE_368_208 = (368<<16) | 208,
	VIDEO_SIZE_416_240 = (416<<16) | 240,
	VIDEO_SIZE_428_362 = (428<<16) | 362,
	VIDEO_SIZE_480_272 = (480<<16) | 272,
	VIDEO_SIZE_624_352 = (624<<16) | 352,
	VIDEO_SIZE_HVGA = (640<<16) | 240,
	VIDEO_SIZE_HVGA_PAL = (640<<16) | 288,
	VIDEO_SIZE_Q720P = (640<<16) | 352,
	VIDEO_SIZE_640_352 = (640<<16) | 352,
	VIDEO_SIZE_640_360 = (640<<16) | 360,
	VIDEO_SIZE_640_400 = (640<<16) | 400,
	VIDEO_SIZE_VGA = (640<<16) | 480,
	VIDEO_SIZE_WVGA = (768<<16) | 480,
	VIDEO_SIZE_848_480 = (848<<16) | 480,
	VIDEO_SIZE_2M = (1600<<16) | 1152,
	VIDEO_SIZE_720P = (1280<<16) | 720,
	VIDEO_SIZE_1280_800 = (1280<<16) | 800,
	VIDEO_SIZE_1080P = (1920<<16) | 1080,
	VIDEO_SIZE_5M = (2592<<16) | 1944,
	VIDEO_SIZE_12M = (4000<<16) | 3000,
	VIDEO_SIZE_16M = (4608<<16) | 3456,
	VIDEO_SIZE_480P = (1280<<16) | 480,
} VIDEO_SIZE;

#define __unused      __attribute__ ((unused))
#define __lundriver_cfg      __attribute__ ((__section__ (".lundriver_cfg")))

extern unsigned int ram_bss_start;
extern unsigned int ram_bss_end;
extern unsigned int rom_bss_start;
extern unsigned int rom_bss_end;
extern unsigned int ram_vector_start;
extern unsigned int ram_text_end;
extern unsigned int ram_data_start;
extern unsigned int ram_data_end;

#define BSS_CLEAR() \
do{ \
	unsigned int *addr; \
	for(addr = &ram_bss_start; addr < &ram_bss_end; addr ++) { \
		*((volatile unsigned int *)addr) = 0; \
	} \
	for(addr = &rom_bss_start; addr < &rom_bss_end; addr ++) { \
		*((volatile unsigned int *)addr) = 0; \
	} \
}while(0)

#define DDR_BSS_CLEAR() \
do{ \
	extern u32 ddr_bss_start, ddr_bss_end; \
	unsigned int *addr; \
	for(addr = &ddr_bss_start; addr < &ddr_bss_end; addr ++) { \
		*((volatile unsigned int *)addr) = 0; \
	} \
}while(0)

/* @} */
static inline int ff1(unsigned int x)
{
	unsigned int xx = (x & -x);
	int n;

	__asm__("clz %0, %1":"=r"(n):"r"(xx));

	return 32 - n;
}

#endif /* _DATA_TYPE_H_ */
