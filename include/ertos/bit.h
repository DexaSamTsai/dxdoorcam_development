#ifndef _BIT_H_
#define _BIT_H_

//bit[31:0]
 int bit_mask(int high, int low);

//bit[31:0]
int set_bit (unsigned int input, int set_bitnumber, int value);

//bit[31:0]
int set_nbits( unsigned int input, int high, int low, int value);

//bit[31:0]
int get_bit (unsigned int input, int get_bitnumber);

//bit[31:0]
int get_nbits(unsigned int input, int high, int low);

#endif

