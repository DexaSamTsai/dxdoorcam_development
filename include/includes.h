#ifndef _INCLUDES_H_
#define _INCLUDES_H_

//#define MANUFACTORY_BOOTTEST	100			// do 100 times self boot test for manufactory

#define MANUFACTORY_BOOT_TOTAL (0x900cffd0)
#define MANUFACTORY_BOOT_FAIL  (0x900cffd4)

#include "ertos/data_type.h"
#define BYTE_ORDER LITTLE_ENDIAN
#include "ertos/byteorder/byteorder.h"
#include "board.h"

#include "ertos/irq_handler.h"
#include "ertos/ertos.h"

#include "ertos/api.h"

#include "ertos/debug.h"
#include "ertos/libcrypto.h"

#include "rom_funcs.h"

#include "drivers/ddr.h"
#include "drivers/watchdog.h"
#include "drivers/clock.h"
#include "drivers/uart/uart.h"
#include "drivers/uart/lowlevel.h"
#include "drivers/jtag.h"
#include "drivers/cpu/tick.h"
#include "drivers/cpu/libclock.h"
#include "drivers/cpu/libcache.h"

#include "drivers/gpio/libgpio.h"
#include "drivers/PWM/PWM.h"

#include "sccb.h"
#include "libsccb.h"
#include "libsccb_old.h"

#include "drivers/dma/libdma.h"

#include "drivers/sif/libsif.h"
#include "drivers/sif/libgpio_sif.h"
#include "drivers/sflash/sflash.h"
#include "drivers/sflash/libsfc_flash_base.h"
#include "drivers/sflash/sfc_flash.h"
#include "drivers/sflash/libsif_flash.h"

#include "efs/libefs.h"

#include "network/wifi/libwifi.h"
#include "network/eth/libeth.h"

#include "board_ioctl_func.h"
#include "drivers/sflash/partition_sf.h"
#include "drivers/nand/partition_nand.h"

#include "drivers/scif/scif.h"
#include "drivers/scif/scif_hw.h"

#include "drivers/libmpu/libmbx.h"
#include "drivers/libmpu/libmpu.h"

#include "media/libdatapath/libdatapath.h"
#include "media/libdatapath/vosd.h"
#include "media/libdatapath/md.h"
#include "media/libvs/libvs.h"
#include "media/libas/libas.h"
#include "media/libas/mem_pool.h"
#include "media/libas/afdist.h"
#include "media/rtp/libmux_rtp.h"
#include "media/rtp/mux_rtp.h"
#include "media/ts/libts.h"
#include "media/ts/libts_enc.h"
#include "media/libisp/libisp.h"
#include "media/audiomsg/libaudiomsg.h"
#include "media/sgui/sgui.h"
#include "media/va/video_analysis.h"
#include "media/mux/libmuxwr.h"
#include "drivers/audio/liba_dec.h"
#include "drivers/audio/liba_enc.h"
#include "drivers/audio/libacodec.h"

#include "drivers/nand/nand.h"
#include "drivers/nand/ecc_bch.h"
#include "drivers/cryption/cryption.h"

#include "drivers/libusb/libusb.h"
#include "drivers/libhost/libhost.h"

#include "drivers/mcu/msp430/msp430_bsl.h"

#if !defined(LWIP_HDR_OPT_H) && !defined(__LWIP_OPT_H__)
#include "lwip/opt.h"
#include "lwip/icmp.h"
#include "lwip/inet_chksum.h"
#include "lwip/sockets.h"
#include "lwip/mem.h"
#include "lwip/inet.h"
#include "lwip/netdb.h"
#include "netif/etharp.h"
#include "lwip/ip.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#include "lwip/dhcpserver.h"
#include "lwip/ping.h"
#include "lwip/dns.h"
#include "lwip/udp.h"

#if LWIP_VERSION >= 0x02000000
#include "lwip/prot/dhcp.h"
#endif
#include "lwip/ip_addr.h"
#include "lwip/api.h"
#include "arpa/inet.h"
#include "sys/socket.h"
#endif

#include "network/network.h"

#ifdef CONFIG_NETWORK_HTTPD_EN
#include "share/network/httpd/httpd.h"
#endif

#ifdef CONFIG_NETWORK_IPERF_EN
#include "network/iperf/iperf.h"
#endif

#ifdef CONFIG_NETWORK_IPERF3_EN
#include "network/iperf3/iperf.h"
#endif

#ifdef CONFIG_NETWORK_THROUGHPUT_EN
#include "network/throughput/throughput.h"
#endif

#include "network/rtspd/rtpd.h"
#include "network/rtspd/rtspd.h"

#include "network/aplayd/aplayd.h"
#include "network/telnetd/telnetd.h"

#ifdef CONFIG_VIDEO_ANALYSIS_VA1_MD
#include "va1_md/ovf_algo.h"
#endif
#ifdef CONFIG_BOARD_EXTRA_HEADER
#include CONFIG_BOARD_EXTRA_HEADER
#endif

int dprintf_rom(const char *fmt0, ...);

// Static assert
#define ENABLE_STATIC_ASSERT        1

#if ENABLE_STATIC_ASSERT
#define _STATIC_ASSERT(EXP,MSG)      _Static_assert(EXP,MSG)
#else
#define _STATIC_ASSERT(EXP,MSG)
#endif //ENABLE_STATIC_ASSERT

#include "media/ispapi/ispapi.h"

#endif
