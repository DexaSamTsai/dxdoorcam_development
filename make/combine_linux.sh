#!/bin/bash

#$1: chip
#$2: prj

make combine_linux -f Makefile.tools > /dev/null || exit 1

chip=$1
prj=$2

for file in "$( ls ../prj/$prj/prj_cfg*.txt 2>/dev/null )" ; do
	prjfile=$file
done
if [ -z $prjfile ] ; then
	exit 0
fi
echo combine_linux INFO: using $prjfile

echo ./combine_linux $prj $prjfile
../build/make/combine_linux $prj $prjfile
