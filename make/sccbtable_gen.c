#include "stdio.h"
#include "stdlib.h"
#include "string.h"

static const unsigned short crc16tab[256]= {
	0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
	0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
	0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
	0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
	0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
	0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
	0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
	0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
	0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
	0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
	0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
	0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
	0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
	0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
	0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
	0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
	0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
	0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
	0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
	0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
	0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
	0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
	0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
	0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
	0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
	0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
	0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
	0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
	0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
	0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
	0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
	0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};
static unsigned short crc16_ccitt(const void *buf, int len)
{
	register int counter;
	register unsigned short crc = 0;
	for( counter = 0; counter < len; counter++)
		crc = (crc<<8) ^ crc16tab[((crc>>8) ^ *(char *)buf++)&0x00FF];
	return crc;
}
static int util_atoi(const char *c)
{
    int ret = 0;
    int base;
    int negative = 0;
    const char * s;

    if (c[0] == '0' && (c[1] == 'x' || c[1] == 'X'))
    {
        base = 16;
        s = c + 2;
    }
    else
    {
        base = 10;
        s = c;
        if (c[0] == '-')
        {
            negative = 1;
            s++;
        }
    }
    for (;  *s; s++)
    {
        if (*s >= '0' &&  *s <= '9')
        {
            ret *= base;
            ret += (*s - '0');
        }
        else if (base == 16 &&  *s >= 'a' &&  *s <= 'f')
        {
            ret *= base;
            ret += (*s - 'a' + 10);
        }
        else if (base == 16 &&  *s >= 'A' &&  *s <= 'F')
        {
            ret *= base;
            ret += (*s - 'A' + 10);
        }
        else
        {
            break;
        }
    }
    if (negative)
    {
        ret = 0-ret;
    }

    return ret;
}

typedef unsigned char  		u8; 
typedef unsigned int  		u32; 
typedef int  		s32; 
typedef unsigned short 		u16; 

typedef struct s_st_cfg{
	u32 magic_num;
	u16 crc16;
	u16 st_cnt;
	u32 cfg1;
	u32 cfg2;
	u32 sys_speed;
	u32 sccb_speed;
	u8 snr_settings[1];
}t_st_cfg;
t_st_cfg *p_st_cfg;
static int sccb_mode;

#define SCCBG_CMD_START 1
#define SCCBG_CMD_STOP  1<<1
#define SCCBG_CMD_RESTART  BIT2
#define SCCBG_CMD_DELAY  BIT7
#define SCCBG_SECTYPE_WRITE 0x0
#define SCCBG_SECTYPE_CMD 0x2
#define SCCBG_SECTYPE_CFG 0x3
#define SCCB_MODE8 0x0
#define SCCB_MODE16 0x1
typedef struct s_libsccb_cfg
{
	u8 mode; ///<SCCB_MODE8 or SCCB_MODE16
	u8 retry; ///<retry count when no ACK or other error, 0:no retry.
	u8 id; ///<sccb device ID
	u8 wr_check; ///<1:after write, read back the data to see if write correct.
	u8 rd_mode; //RD_MODE_NORMAL: send stop bit; RD_MODE_RESTART: 1-step read
	u32 clk; ///<sccb speed, eg: 400*1024 for 400kbps.
	u32 timeout; ///<timeout value for retry, 0:use sys default
	u32 nonblock; ///<default 0 useblock mode, 1 use nonblock mode. Can use libsccb_check_done to check w/r status. 
	u32 sysclk; ///<system clock, for calc clk. in most cases, set it to IN_CLK

	u32 buf_addr;   // data combine address for group write
	u32 buf_len;   // data len of combine data for group write
	u32 max_len;   // max_len of combine buffer 

	u8 sccb_num; /*must. There are 4 sccb: 0, 1, 2, 3, 0xff*/
//	volatile e_libsccb_sta status;

#define SCCB_NUM_0		0x00
#define SCCB_NUM_1		0x01
#define SCCB_NUM_2		0x02
#define SCCB_NUM_3		0x03
#define SCCB_NUM_GPIO	0xff

	u16 gpio_sim_pin_scl;
	u16 gpio_sim_pin_sda;

#ifndef BUILD_FOR_BA
	pthread_mutex_t sccb_mutex;
	pthread_cond_t sccb_cond; 
#endif	
}t_libsccb_cfg;
t_libsccb_cfg g_sccb_cfg;

static int sccbgrp_sec_combine(t_libsccb_cfg* sccb_cfg, int sec_cnt, u8* type, u8* data)
{	
	// clear send buffer
	u32 send_data; 
	if(sec_cnt > 3){
		// error
		return -1;
	}else{
//	debug_printf("buf_addr:%x len:%x\n",sccb_cfg->buf_addr, sccb_cfg->buf_len << 2);
//	printf("d:%x:%x:%x ",data[0],data[1],data[2]);
		switch(sec_cnt){
			case 0:
				break;
			case 1:
				send_data = (sec_cnt << 30) | (type[0] << 28 | data[0] << 20);
				break;
			case 2:
				send_data = (sec_cnt << 30) | (type[0] << 28 | data[0] << 20) | (type[1] << 18 | data[1] << 10);
				break;
			case 3:
				send_data = (sec_cnt << 30) | (type[0] << 28 | data[0] << 20) |
								   			(type[1] << 18 | data[1] << 10) | (type[2] << 8 | data[2]);
				break;
			default:
				break;
		}
	}
 	u32 * buf = (u32 *)((u32)sccb_cfg->buf_addr + (sccb_cfg->buf_len << 2));
//	printf("r:%x \n",send_data);
	buf[0] = send_data;
	sccb_cfg->buf_len += 1;
	return 0;
}

static void sccbgrp_data_combine(t_libsccb_cfg* sccb_cfg, s32 len, u8* type, u8* data)
{
	s32 index = 0;
	while(len > 3){
		// combine 3 commands
		sccbgrp_sec_combine(sccb_cfg, 3, &type[index], &data[index]);
		if(len >= 3){
			index += 3;
			len -= 3;
		}else{
			break;
		}
	}
	if(len > 0){
		// left 1 or 2 commands
		sccbgrp_sec_combine(sccb_cfg, len, &type[index], &data[index]);
	}else{
		return;
	}
}
static int sccbgrp_combine16(t_libsccb_cfg *sccb_cfg, u16 addr, int data)
{
	u8 send_type[6];
	u8 send_data[6];
	send_type[0] = SCCBG_SECTYPE_CMD;
	send_data[0] = SCCBG_CMD_START; 
	send_type[1] = SCCBG_SECTYPE_WRITE;
	send_data[1] = sccb_cfg->id;
	send_type[2] = SCCBG_SECTYPE_WRITE;
	send_data[2] = (u8)((addr >> 8) & 0xff);
	send_type[3] = SCCBG_SECTYPE_WRITE;
	send_data[3] = (u8)(addr & 0xff);
	send_type[4] = SCCBG_SECTYPE_WRITE;
	send_data[4] = data;
	send_type[5] = SCCBG_SECTYPE_CMD;
	send_data[5] = SCCBG_CMD_STOP; 
	
	sccbgrp_data_combine(sccb_cfg, 6, send_type, send_data);

	return 0;
}
static int sccbgrp_combine8(t_libsccb_cfg *sccb_cfg, u8 addr, int data)
{
	u8 send_type[5];
	u8 send_data[5];
	send_type[0] = SCCBG_SECTYPE_CMD;
	send_data[0] = SCCBG_CMD_START; 
	send_type[1] = SCCBG_SECTYPE_WRITE;
	send_data[1] = sccb_cfg->id;
	send_type[2] = SCCBG_SECTYPE_WRITE;
	send_data[2] = (u8)(addr & 0xff);
	send_type[3] = SCCBG_SECTYPE_WRITE;
	send_data[3] = data;
	send_type[4] = SCCBG_SECTYPE_CMD;
	send_data[4] = SCCBG_CMD_STOP; 
	
	sccbgrp_data_combine(sccb_cfg, 5, send_type, send_data);

	return 0;
}

int sccbgrp_combine_setting(t_libsccb_cfg* sccb_cfg, u8 *dst, void *config, u32 len)
{
	int i;
	sccb_cfg->buf_len = 0;
	u32 addrback = sccb_cfg->buf_addr;
	sccb_cfg->buf_addr = (u32)dst;
	if(sccb_cfg->mode == SCCB_MODE16)
	{
		u16 *src = config;
		for(i=0; i<len; i++){
//			debug_printf(":%x:%x\n", src[i*2], src[i*2+1]);
			sccbgrp_combine16(sccb_cfg, src[i*2], src[i*2+1]);
		}
	}
	else
	{
		u8 *src = config;
		for(i=0; i<len; i++){
			sccbgrp_combine8(sccb_cfg, src[i*2], src[i*2+1]);
		}
	}
	sccb_cfg->buf_addr = addrback;
	return sccb_cfg->buf_len;
}

static int config_parse(t_st_cfg *st_cfg, unsigned char *src, int len)
{
	
	st_cfg->magic_num = 0x53435442;
	unsigned char * cur = src;
	unsigned char * buf;
	unsigned char  * c;
	unsigned char * value;
	while((cur - src)<len)
	{
		buf = cur;
		cur++;
		c = strchr(buf, '=');
		if(c == NULL) 
		{
			continue;
		}
		for(value=c+1; value[0]==' ' || value[0] == '\t'; value ++);
		for(c--; c[0] == ' ' || c[0]=='\t'; c--);
		c[1] = '\0';
		if(!strcmp(buf, "SCCB_NUM")){
			st_cfg->cfg1 = g_sccb_cfg.sccb_num = util_atoi(value) & 0x3;
		//	printf("num:%x",st_cfg->cfg1);
		}else if(!strcmp(buf, "SCCB_ID")){
			g_sccb_cfg.id = (util_atoi(value) & 0xff);
			st_cfg->cfg1 |= g_sccb_cfg.id << 24;
		}else if(!strcmp(buf, "SCCB_MODE")){
			sccb_mode = util_atoi(value);
			if(sccb_mode == 16)
			{
				st_cfg->cfg1 |= 1<<2;
				g_sccb_cfg.mode = SCCB_MODE16;
			}
			else if(sccb_mode == 8)
			{
				st_cfg->cfg1 &= (~(1<<2));
				g_sccb_cfg.mode = SCCB_MODE8;
			}
		//	printf("add mode:%x",st_cfg->cfg1);
		}else if(!strcmp(buf, "SCCB_SPEED")){
			g_sccb_cfg.clk = st_cfg->sccb_speed = util_atoi(value);
	//		printf("speed:%d",st_cfg->sccb_speed);
		}else if(!strcmp(buf, "SYS_SPEED")){
			g_sccb_cfg.sysclk = st_cfg->sys_speed = util_atoi(value);
	//		printf("sysspeed:%d",st_cfg->sys_speed);
		}
	}
	return (sizeof(t_st_cfg) - sizeof(st_cfg->snr_settings));
}

static int setting_fill(t_st_cfg *st_cfg, unsigned char *src, int len)
{
	unsigned char * cur = src;
	unsigned char * buf;
	unsigned char  * c;
	unsigned char * value;
	//unsigned char * dst = st_cfg->snr_settings;
	u32 retlen;
	u16 *dst16 = (u16 *)st_cfg->snr_settings;
	u8 *dst8 = st_cfg->snr_settings;
	g_sccb_cfg.buf_addr = (u32)st_cfg->snr_settings;

	while((cur - src)<len)
	{
		buf = cur;
		cur ++;
		if(buf[0] == ';' || buf[0] == '\n' || buf[0] == '\r' || buf[0] == '\0' || buf[0] == ' ') continue;
		if(buf[0] == '/' && buf[1] == '/')
		{
		//	for(; cur[0] != '\0' && cur[0] != '\n' && cur[0] != '\r'; cur++);
			continue;
		}
		c = strchr(buf, '{');
		if(c == NULL) 
		{
			continue;
		}
		for(value=c+1; value[0]==' '; value ++);
		if(sccb_mode == 16)
		{
			u16 addr, data;
			addr =util_atoi(value);;
			c = strchr(value, ',');
			for(value=c+1; value[0]==' '; value ++);
			data =util_atoi(value);;
//			printf("v:%x:%x",addr, data);	
			sccbgrp_combine16(&g_sccb_cfg, addr, data);
			retlen += 4;	
		}
		else if(sccb_mode == 8)
		{	
			u8 addr, data;
			addr =util_atoi(value);;
			c = strchr(value, ',');
			for(value=c+1; value[0]==' '; value ++);
			data =util_atoi(value);;
			retlen += 2;	
//			printf("v8:%x:%x",addr, data);	
			sccbgrp_combine8(&g_sccb_cfg, addr, data);
		}
		
	}

//	printf("l:%d\n",g_sccb_cfg.buf_len);
	//return retlen;
	return g_sccb_cfg.buf_len * 4;
}

//int parse_sccb_table(unsigned char *src, unsigned char *dst, int len)
static int parse_sccb_table(t_st_cfg *st_cfg, unsigned char *src, int len)
{
	unsigned char * cur = src;
	unsigned char * buf;
	unsigned char  * c;
	unsigned char *cfg_off, *set_off, *set_end;
	cfg_off = set_off = set_end = 0;
	u32 cfg_len, set_len;
	while((cur - src)<len)
	{
		buf = cur;
		for(; cur[0] != '\0' && cur[0] != '\n' && cur[0] != '\r'; cur++);
		cur[0] = '\0';
		cur ++;
		if(buf[0] == ';' || buf[0] == '\n' || buf[0] == '\r' || buf[0] == '\0' || buf[0] == ' ') continue;
		if(buf[0] == '/' && buf[1] == '/') continue;
		if(buf[0] == '['){
			if(!strncmp(buf+1, "SCCB_CFG", 8)){
				cfg_off = buf; 
			}
			if(!strncmp(buf+1, "SCCB_SETTING", 12)){
				set_off = buf; 
			}
			if(!strncmp(buf+1, "SCCB_END", 8)){
				set_end = buf; 
			}
			continue;
		}
		
	}
	int ret;
	cfg_len = (u32)set_off - (u32)cfg_off;
	ret = config_parse(st_cfg, cfg_off, cfg_len);
	set_len = (u32)set_end - (u32)set_off;
	ret += setting_fill(st_cfg, set_off, set_len);
	return ret;
}

static int _parse_sccb_cfg(void)
{
	FILE * sccb_cfg_fp = fopen("sccb_cfg.tmp", "r");
	if(sccb_cfg_fp == NULL){
		printf("cannot read sccb_cfg.tmp\n");
		return -1;
	}

#define ITEM_SIZE		50

	char item[ITEM_SIZE];
	while(fgets(item, ITEM_SIZE, sccb_cfg_fp)){
		char * name = item;
		char * value = strchr(item, '=');
		if(value == NULL){
			return 0;
		}
		*(value++) = '\0';

		if(!strcmp(name, "SCCB_CFG_CLK")){
			g_sccb_cfg.clk = atoi(value);
		}else if(!strcmp(name, "SCCB_CFG_SYSCLK")){
			g_sccb_cfg.sysclk = atoi(value);
		}
	}

#undef ITEM_SIZE
	return 0;
}

int gensccb_txt(FILE *in, FILE *in2, char * setting_filename){

	FILE *tmp1;
	FILE *tmp2;

	unsigned char * c;
	unsigned char * c1;

	unsigned char * sbuf = malloc(sizeof(unsigned char)*1024);
	unsigned char * sbuf1 = malloc(sizeof(unsigned char)*1024);
	
	unsigned char find_def_end = 0;
	unsigned char read_setting = 0;
	unsigned int find_setting = 0;

	tmp1 = fopen(setting_filename, "w");
	if(tmp1 == NULL){
		printf("cannot read %s\n", setting_filename);
	}
	
	tmp2 = fopen("tmp2.txt", "w+");
	if(tmp2 == NULL){
		printf("cannot write tmp2.txt\n");
	}

	_parse_sccb_cfg();
	
	///< parse config
	while(fgets(sbuf, 1024, in2)){
		if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
		if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
		for(c=sbuf; *c==' ' || *c=='\t'; c++);
		if(strncmp(c, "#define", 7) == 0){
			c+=7;	
			for(;*c==' ' || *c == '\t'; c++);
			int n=0;
			for(;*c!=' ';c++){
				sbuf1[n++] = *c;	
			}
			sbuf1[n] = '\0';
			fprintf(tmp2, "%s\n", sbuf1);		
		}
	}

	while(fgets(sbuf, 1024, in)){
		if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
		if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
		for(c=sbuf; *c==' ' || *c=='\t'; c++);
		if(strncmp(c, "//", 2) == 0){
			continue;
		}
		if(*c == '#' && find_def_end == 0){
			c++;
			if(strncmp(c, "define", 6) == 0){
				c+=6;	
				for(;*c==' ' || *c == '\t'; c++);
				if(strncmp(c, "SENSOR_SCCB_MODE_INTERNAL", 25) == 0){
					c+=25;
					for(;*c==' ' || *c == '\t'; c++);
					if(strncmp(c, "SCCB_MOD8", 9) == 0){
						g_sccb_cfg.mode = 8;
						c+=8;
					}else if(strncmp(c, "SCCB_MODE16", 10) == 0){
						g_sccb_cfg.mode = 16;
						c+=16;
					}
				//	printf("mode=%d\n", g_sccb_cfg.mode);
				}else if(g_sccb_cfg.id == 0 && strncmp(c, "SENSOR_SCCB_ID_INTERNAL", 23) == 0){
					c+=23;
					for(;*c==' ' || *c == '\t'; c++);
					g_sccb_cfg.id = strtoul(c, NULL, 16);
				//	printf("id=%d,%s\n", g_sccb_cfg.id, c);
				}else{
					if(strstr(c, "INTERNAL") == NULL){
						fseek(tmp2, 0, SEEK_END);
						fprintf(tmp2, "%s\n", c);		
					}	
				}
			}else if(strncmp(c, "ifdef", 5) == 0){
				unsigned int find_id = 0;
				c+=5;
				for(;*c==' ' || *c == '\t'; c++);
				fseek(tmp2, 0, SEEK_SET);
				while(fgets(sbuf1, 1024, tmp2)){
					if((c1=strchr(sbuf1,'\n'))!=NULL) *c1='\0';
					if((c1=strchr(sbuf1,'\r'))!=NULL) *c1='\0';
					for(c1=sbuf1; *c1==' ' || *c1=='\t'; c1++);
					unsigned char *tmp_str = c;
					if(strncmp(tmp_str, c1, strlen(c1)) == 0){
						find_id = 1;	
						break;
					}
				}
				if(!find_id){
					while(fgets(sbuf, 1024, in)){
						if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
						if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
						for(c=sbuf; *c==' ' || *c=='\t'; c++);
						if(strncmp(c, "#else", 5) == 0||strncmp(c, "#endif", 6) == 0){
							find_id = 1;	
							break;
						}
					}
				}
				if(find_id == 1){
					continue;
				}
			}
		}
		if(strncmp(c, "SENSOR_SETTING_TABLE", 20) == 0){
			c+=20;
			for(;*c==' ' || *c == '\t'; c++);
			if(strncmp(c, "sensor__init[][2]", 17) == 0){
				find_def_end = 1;	
			}
			
			if(find_def_end == 1){
				if(g_sccb_cfg.clk == 0){
					g_sccb_cfg.clk = 400000;
				}
				if(g_sccb_cfg.sysclk == 0){
					g_sccb_cfg.sysclk = 24000000;
				}
				fprintf(tmp1, "[SCCB_CFG]\n");
				fprintf(tmp1, "SCCB_NUM = %d\n", g_sccb_cfg.sccb_num);
				fprintf(tmp1, "SCCB_ID = 0x%x\n", g_sccb_cfg.id);
				fprintf(tmp1, "SCCB_MODE = %d\n", g_sccb_cfg.mode);
				fprintf(tmp1, "SCCB_SPEED = %d\n", g_sccb_cfg.clk);
				fprintf(tmp1, "SYS_SPEED = %d\n", g_sccb_cfg.sysclk);
				fprintf(tmp1, "[SCCB_SETTING]\n");
	
				unsigned int def_cnt = 0;
				unsigned int def1_cnt = 0;

				while(fgets(sbuf, 1024, in)){
					if((c=strchr(sbuf,'\n'))!=NULL) *c='\0';
					if((c=strchr(sbuf,'\r'))!=NULL) *c='\0';
					for(c=sbuf; *c==' ' || *c=='\t'; c++);
					if(strstr(c, "};") != NULL){
						if(find_setting == 0){
							find_setting = 0xffffffff;
							break;
						}
					}
					if(strncmp(c, "//", 2) == 0){
						continue;
					}
					if(*c == '#'){
						c++;
						if(strncmp(c, "ifdef", 5) == 0 || strncmp(c, "if", 2) == 0 || strncmp(c, "elif", 4) == 0){
							if(def_cnt > 0){
								if(((find_setting>>(def_cnt-1))&0x1)==0){
									//omit
									def1_cnt++;
									while(fgets(sbuf1, 1024, in)){
										if((c1=strchr(sbuf1,'\n'))!=NULL) *c1='\0';
										if((c1=strchr(sbuf1,'\r'))!=NULL) *c1='\0';
										for(c1=sbuf1; *c1==' ' || *c1=='\t'; c1++);
										if(*c1 == '#'){
											c1++;
											if(strncmp(c1, "ifdef", 5)==0 || strncmp(c1, "if", 2)==0 ){
												def1_cnt++;	
											}else if(strncmp(c1, "endif", 5)==0){
												def1_cnt--;
											}
											if(def1_cnt == 0){
												break;
											}
										}
									}
									continue;
								}
							}
						}
						if(strncmp(c, "ifdef", 5) == 0){
							def_cnt++;
							c+=5;
							for(;*c==' ' || *c == '\t'; c++);
							fseek(tmp2, 0, SEEK_SET);
							while(fgets(sbuf1, 1024, tmp2)){
								if((c1=strchr(sbuf1,'\n'))!=NULL) *c1='\0';
								if((c1=strchr(sbuf1,'\r'))!=NULL) *c1='\0';
								for(c1=sbuf1; *c1==' ' || *c1=='\t'; c1++);
								unsigned char *tmp_str = c;
								if(strncmp(tmp_str, c1, strlen(c1)) == 0){
									find_setting |= (1<<(def_cnt-1));
									break;
								}
							}
						}else if(strncmp(c, "if", 2) == 0){
							def_cnt++;
							c+=2;
							for(;*c==' ' || *c == '\t'; c++);
							u32 tmp = atoi(c);
							if(tmp != 0){
								find_setting |= (1<<(def_cnt-1));
							}
						}else if(strncmp(c, "elif", 4) == 0){
							for(;*c==' ' || *c == '\t'; c++);
							if(strncmp(c, "defined", 7) == 0){
								while(fgets(sbuf1, 1024, tmp2)){
									if((c1=strchr(sbuf1,'\n'))!=NULL) *c1='\0';
									if((c1=strchr(sbuf1,'\r'))!=NULL) *c1='\0';
									for(c1=sbuf1; *c1==' ' || *c1=='\t'; c1++);
									unsigned char *tmp_str = c;
									if(strncmp(tmp_str, c1, strlen(c1)) == 0){
										find_setting |= (1<<(def_cnt-1));
										break;
									}
								}
							}	
						}else if(strncmp(c, "else", 4) == 0){
							if((find_setting >> (def_cnt-1))&0x1){
								find_setting &= (~(1<<(def_cnt-1)));

							}else{
								find_setting |= (1<<(def_cnt-1));
							}
						}else if(strncmp(c, "endif", 5) == 0){
							if(def_cnt > 0){
								if((find_setting >> (def_cnt-1))&0x1){
									find_setting &= (~(1<<(def_cnt-1)));
								}
								def_cnt--;
							}
						}
						continue;
					}
					
					if((def_cnt == 0) || (find_setting >> (def_cnt-1))){
						c1 = strstr(c, "{");
						if(c1 != NULL){
							fprintf(tmp1, "%s\n", c1);
						}
					}
				}
				if(find_setting == 0xffffffff){
					fprintf(tmp1, "[SCCB_END]\n");
					break;
				}
			}
		}
	}
	free(sbuf);
	free(sbuf1);
	fclose(tmp1);
	fclose(tmp2);
//	remove("tmp2.txt");
}

int main(int argc, char ** argv)
{
	FILE * in;
	FILE * in2;
	FILE * out;
	
	in = fopen(argv[1], "r");
	if(in == NULL){
		printf("cannot read %s\n", argv[1]);
		return -1;
	}
	
	in2 = fopen(argv[2], "r");
	if(in2 == NULL){
		printf("cannot read %s\n", argv[2]);
	}

	memset(&g_sccb_cfg, 0, sizeof(g_sccb_cfg));

	char sccb_txt[2048];
	sprintf(sccb_txt, "%s_setting", argv[3]);

	gensccb_txt(in, in2, sccb_txt);

	fclose(in);
	if(in2){
		fclose(in2);
	}

	in = fopen(sccb_txt, "r");
	if(in == NULL){
		printf("cannot read tmp1.txt\n");
	}
	
	fseek(in, 0, SEEK_END);
	int len = ftell(in);
	fseek(in, 0, SEEK_SET);
	unsigned char *src = malloc(sizeof(unsigned char)*len);
	unsigned char *dst = malloc(sizeof(unsigned char)*len * 2);
	fread(src, len, 1, in);

	p_st_cfg = dst;
	int cfg_len =  parse_sccb_table(p_st_cfg, src, len);	
	out = fopen(argv[3], "w");
	if(out == NULL){
		printf("cannot write %s\n", argv[3]);
		return -1;
	}
	p_st_cfg->st_cnt = g_sccb_cfg.buf_len;
//	printf("st_cnt:%x\n", p_st_cfg->st_cnt);
	p_st_cfg->crc16 = crc16_ccitt(&(p_st_cfg->st_cnt), (p_st_cfg->st_cnt * 4 + 0x12));
	fwrite(dst, cfg_len, 1, out);

	free(src);
	free(dst);
	fclose(in);
	fclose(out);
//	remove(sccb_txt);

	return 0;
}
