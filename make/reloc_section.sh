#!/bin/bash

help()
{
	echo "usage: $0 prefix input output"
	exit
}

prefix=$1
input=$2
output=$3
crosscompile=arm-none-eabi-
if [ ! -z $4 ] ; then
	crosscompile=$4
fi

prefix_len=${#prefix}

if [ "$input" != "$output" ] ; then
	cp -a $input $output
fi

tmpfile=${output}.header
tmpfile1=${output}.list
tmpfile2=${output}.sh

${crosscompile}objdump -h $output > $tmpfile

rm -f $tmpfile1
while read a sec c ; do
	if [ "${sec:0:1}" != "." ] ; then
		continue;
	fi
	if [ "${sec:1:${prefix_len}}" == "$prefix" ] ; then
		continue;
	fi
	newsec=.$prefix$sec
	echo "--rename-section $sec=$newsec " >> $tmpfile1
#	${crosscompile}objcopy --rename-section $sec=$newsec $output
done < $tmpfile
cat $tmpfile1 | sort | uniq > $tmpfile2
echo -n "#!/bin/bash" > $tmpfile1
echo -n "${crosscompile}objcopy " > $tmpfile1
while read a ; do
	echo -n "$a " >> $tmpfile1
done < $tmpfile2
echo $output >> $tmpfile1

chmod a+x $tmpfile1
source $tmpfile1

rm -f $tmpfile
rm -f $tmpfile1
rm -f $tmpfile2
