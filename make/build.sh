#!/bin/bash
if [ -z $CROSS_COMPILE ] ; then
	CROSS_COMPILE=/opt/ba-elf-new/bin/ba-elf-
fi
export CROSS_COMPILE

export FOR_SDKRELEASE=1

if [ -z $LIBSDIR ] ; then
	if [ -d R3_libs/ ] ; then
		echo -n "R3_libs/ exist, input [y/Y] if wants to build with R3_libs: [n] "
		read answer
		if [ "$answer" != "y" ] && [ "$answer" != "Y" ] ; then
			echo "Quit, not building"
			exit
		fi
		echo

		LIBSDIR=R3_libs
	else
		LIBSDIR=.
	fi
fi
export LIBSDIR

usage ()
{
	echo "=======firmware build script======="
	echo "APP: APP is one firmware binary of ram/xxx/ "
	echo "PRJ: PRJ is group of APPs with specific Defines in prj/xxx/yyy.config"
	echo "1. ./autobuild.sh all #build all APPs in all PRJ"
	echo "2. ./autobuild.sh APP #build APP in default PRJ"
	echo "3. ./autobuild.sh PRJ #build all APPs in PRJ"
	echo "4. ./autobuild.sh PRJ APP #build APP in PRJ"
	exit 1
}

if [ $# -eq 0 ] ; then
	usage
fi

if [ -z $chip ] ; then
	echo "please run ./autobuild.sh"
	exit 1
fi

#check gcc version
gccv=`${CROSS_COMPILE}gcc -dumpversion | sed -e "s/\.//g"`
if [ -z $gccv ] ; then
	echo "${CROSS_COMPILE}gcc not found"
	exit 1
fi
gccfile=`which ${CROSS_COMPILE}gcc`
gccdir=`dirname $gccfile`
COMPILE_ADD_UNDERSCORE=0
if [ "$chip" == "r2" ] ; then
	if [ $gccv -gt 440 ] ; then
		COMPILE_ADD_UNDERSCORE=1
		link_libgccdir="-L$gccdir/../lib/gcc/ba-elf/4.7.3/ -L$gccdir/../ba-elf/lib/ -lgcc -lgcc_us"
	else
		echo "[ERROR] This SDK cannot compiled by old toolchain, please upgrade toolchain"
#		exit 1
		link_libgccdir="-L$gccdir/../lib/gcc/ba-elf/4.4.0/ba2/ -L$gccdir/../ba-elf/lib/ba2 -lgcc"
	fi
else
	link_libgccdir="-L$gccdir/../lib/gcc/arm-none-eabi/`${CROSS_COMPILE}gcc -dumpversion`/v7-a/ -lgcc"
fi
export COMPILE_ADD_UNDERSCORE
export link_libgccdir

if [ ! -e build ] ; then
	mkdir build
fi

usage_menuconfig ()
{
	echo "usage: ./autobuild.sh menuconfig PRJ [APP | library]"
	exit 1
}

build_prj_app ()
{
	if [ ! -d ram/$3 ] ; then
		echo "APP $3 not exist, skip";
		return 0;
	fi
	if [ ! -e prj/$2/$3.config ] ; then
		echo "build.sh prj/$2/$3.config not found";
		exit 1;
	fi

	if [ -e prj/$2/$3.ld.S ] ; then
		ldf="../../prj/$2/$3.ld"
	fi

	if [ ! -z $LDFILE ] ; then
		ldf=$LDFILE
	fi

#check if need to build the MPU firmware
	mpubin=`grep CONFIG_MPU_EN prj/$2/$3.config | sed -e "s/^.*=//g"`
	if [[ $mpubin != "" ]] ; then
		CHIP=$1 PRJ=$2 APP=$3 LDFILE=$ldf make cfg_gen -f Makefile.all -C make/ --no-print-directory || exit 1
		#FIXME: mpu never USEROM
		cd mpu/; USEROM=0 V=$V CHIPV=$CHIPV ./autobuild.sh $2 $3 || exit 1 ; cd - &> /dev/null || exit 1
	fi

	mkf="Makefile.all"

	CHIP=$1 PRJ=$2 APP=$3 LDFILE=$ldf make combine_h_gen all -f $mkf -C make/ --no-print-directory || exit 1
}

build_prj_app_only ()
{
	if [ ! -d ram/$3 ] ; then
		echo "APP $3 not exist, skip";
		return 0;
	fi

	if [ ! -e prj/$2/$3.config ] ; then
		echo "build.sh prj/$2/$3.config not found";
		exit 1;
	fi

	if [ -e prj/$2/$3.ld.S ] ; then
		ldf="../../prj/$2/$3.ld"
	fi

	if [ ! -z $LDFILE ] ; then
		ldf=$LDFILE
	fi

	mkf="Makefile.all"

	IGNORE_SHARE=1 CHIP=$1 PRJ=$2 APP=$3 LDFILE=$ldf make combine_h_gen all -f $mkf -C make/ --no-print-directory || exit 1
}

# menuconfig
if [ "$1" == "menuconfig" ] ; then
	echo "start menuconfig"
	if [ -z $2 ] ; then
		usage_menuconfig
	fi
	if [ -z $3 ] ; then
		usage_menuconfig
	fi
	if [ ! -d "prj/$2" ] ; then
		usage_menuconfig
	fi

	if [ "$chip" == "r2" ] ; then
		chip_r2="y"
		chip_r3="n"
	fi
	if [ "$chip" == "r3" ] ; then
		chip_r2="n"
		chip_r3="y"
	fi

	if [ ! -e "ram/$3/Kconfig" ] ; then
		appname="make/Kconfig.empty"
		appkconfig="n"
	else
		appname="ram/$3/Kconfig"
		appkconfig="y"
	fi
	CHIP_R2=$chip_r2 CHIP_R3=$chip_r3 APPNAME=$appname APP_KCONFIG=$appkconfig KCONFIG_CONFIG=prj/$2/$3.config KCONFIG_AUTOCONFIG=prj/$2/.$3.autoconf KCONFIG_AUTOHEADER=prj/$2/.$3.auto.h KCONFIG_TRISTATE=prj/$2/.$3.triconf ./make/mconf include/Kconfig
	exit
fi

# clean
build_clean=0
if [ `echo "$1" | grep clean` ] ; then
	build_clean=1
fi
if [ ! -z "$2" ] ; then
	if [ `echo "$2" | grep clean` ] ; then
		build_clean=1
	fi
fi
if [ $build_clean -ne 0 ] ; then
	if [ -e mpu/make/Makefile.clean ] ; then
		CHIP=$chip make clean -f Makefile.clean -C mpu/make/ --no-print-directory || exit 1
	fi
	CHIP=$chip make clean -f Makefile.clean -C make/ --no-print-directory || exit 1
	exit 0
fi

# check build chip revision
cur_rev="revA"
LIB_POSTFIX=""
if [ ! -z $CHIPV ] ; then
	if [ "$CHIPV" == "ECO1" ] ; then
		cur_rev="revB"
		LIB_POSTFIX="_ECO1"
	fi
else
	CHIPV=""
fi
export LIB_POSTFIX
if [ -e build/chiprev.txt ] ; then
	pre_rev=`cat build/chiprev.txt`
	if [ "$pre_rev" != "$cur_rev" ] ; then
		echo "build with $cur_rev conflict with last $pre_rev build"
		echo "'rm -rf build/ mpu/build/' and try again"
		exit 1
	fi
else
	echo "$cur_rev" > build/chiprev.txt
fi

# build all share libraries
echo "  [RUNLIB] share/ start"
if [ -z $2 ] ; then
	RUNLIB=1 LIBNEEDLINK=0 CHIP=$chip OBJDIR=share/ make all -f Makefile.dir -C make/ --no-print-directory || exit 1
else
	RUNAPP=1 RUNLIB=1 LIBNEEDLINK=0 CHIP=$chip OBJDIR=share/ make all -f Makefile.dir -C make/ --no-print-directory || exit 1
fi
echo "  [RUNLIB] share/ done"

generate_prj_bin ()
{
	if [ -e make/combine.sh ] ; then
		cd make
		./combine.sh $1 $2 || exit 1
		cd -
	fi
}

# loop build all applications
prjfile="prj/$1"
if [ -d $prjfile ] ; then
	prj=$1
	if [ $# -eq 1 ] ; then
		app="all"
	else
		app=$2
	fi
else
	if [ $1 == "all" ] ; then
		for filed in prj/* ; do
			for file in $filed/*.config ; do
				prj=`echo $filed | sed -e "s/^prj\///"`
				app=`echo $file | sed -e "s/^prj\/$prj\///" | sed -e "s/\.config$//"`
                sharelib=`echo $file | sed -e "s/^prj\/$prj\///" | sed -e "s/\.config$//"| grep "^lib"`

                if [ `echo $sharelib | grep "^lib"` ]; then
                continue;
                fi

                if [ "$app" == "*" ] ; then
                    continue;
				fi
	echo "======= prjfile ======="
				build_prj_app $chip $prj $app
			done
			generate_prj_bin $chip $prj
		done
		exit 0
	else
		prj="default"
		app=$1
	fi
fi

if [ $app == "all" ] ; then
	for file in prj/$prj/*.config ; do
		app=`echo $file | sed -e "s/^prj\/$prj\///" | sed -e "s/\.config$//"`
        sharelib=`echo $file | sed -e "s/^prj\/$prj\///" | sed -e "s/\.config$//"| grep "^lib"`

        if [ `echo $sharelib | grep "^lib"` ] ; then
            continue;
        fi

        if [ "$app" == "*" ] ; then
            continue;
		fi

        build_prj_app $chip $prj $app
	done
	generate_prj_bin $chip $prj
else
	if [ $prj == "default" ] ; then
		appfile="prj/default/$app.config"
		if [ ! -e "$appfile" ] ; then
			echo "$appfile not exist"
			exit 1
		fi
	fi
	echo "======= not all ======="
	build_prj_app_only $chip $prj $app
	generate_prj_bin $chip $prj
fi

exit 0

