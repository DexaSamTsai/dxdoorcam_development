#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include <stdlib.h>

#define isvalid(x) (((x)>='a' && (x)<='z') || ((x)>='A' && (x)<='Z') || ((x)>='0' && (x)<='9') || ((x) == '_') )

int main(int argc, char **argv)
{
	char buf[1024];
	char sensors[1024];
	sensors[0] = '\0';

	int gen_h = 0;
	if(argc > 1){
		if(!strcmp(argv[1], "autoconf")){
			gen_h = 1;
		}
	}

	FILE *fp;
	FILE *fp1;

	int tmpfile=1;
	char tmpfilename[32];
	char tmpfilename1[32];
	do{
		sprintf(tmpfilename, "convftmp.%d", tmpfile);
		fp=fopen(tmpfilename, "r");
		if(fp != NULL){
			fclose(fp);
		}else{
			fp=fopen(tmpfilename, "w");
			if(fp != NULL){
				break;
			}
		}
		tmpfile++;
		if(tmpfile >= 100000){
			fprintf(stderr, "Cannot generate tmpfile\n");
			exit(0);
		}
	}while(1);

	do{
		sprintf(tmpfilename1, "convftmp.%d", tmpfile);
		fp1=fopen(tmpfilename1, "r");
		if(fp1 != NULL){
			fclose(fp1);
		}else{
			fp1=fopen(tmpfilename1, "w");
			if(fp1 != NULL){
				break;
			}
		}
		tmpfile++;
		if(tmpfile >= 100000){
			fprintf(stderr, "Cannot generate tmpfile\n");
			exit(0);
		}
	}while(1);

	if(!gen_h){
	printf("###################################################\n");
	printf("#  This file is Auto-generated from prj/$(PRJ)/$(APP).config, DON'T EDIT !!!\n");
	printf("#\n");
	printf("# If you want to change options, edit prj/$(PRJ)/$(APP).config\n");
	printf("# or, use \"autobuild.sh menuconfig PRJ APP\" to generate $(APP).config\n");
	printf("###################################################\n");

	printf("\n\n######## old style configs ########\n\n");
	}

	while(fgets(buf, 1024, stdin)){
		char *c ;
		if((c=strchr(buf, '\r'))!=NULL) *c='\0';
		if((c=strchr(buf, '\n'))!=NULL) *c='\0';
		char *name = buf;
		if(name[0] == '#' || name[0] == '\0'){
			continue;
		}
		for(c=name; isvalid(*c); c++);
		if(*c=='\0') continue;
		char *nameend=c;
		c=strchr(c,'=');
		if(c==NULL) continue;
		c++;
		*nameend='\0';
		for(;*c==' ' || *c=='\t'; c++);
		if(*c=='\0') continue;
		char *value = c;
		if((c=strchr(value, '#'))!=NULL) *c='\0';
		while(value[strlen(value)-1] == ' ' || value[strlen(value)-1] == '\t'){
			value[strlen(value)-1] = '\0';
		}

		if(strncmp(name, "CONFIG_", 7)){
			continue;
		}
		name += 7;

#define rep(x, y) if(!strncmp(name, x, strlen(x))){ \
			if(value[0] != 'y') continue; \
			if(!gen_h){ \
				printf(y " := %s\n", name + strlen(x)); \
			} \
			fprintf(fp, "CONFIG_" y " := %s\n", name + strlen(x)); \
			fprintf(fp1,"#define CONFIG_%s 1\n", name); \
			continue; \
		}

		rep("NANDCHIP_", "NANDFLASH");
		rep("AUDIO_CODEC_EN_", "AUDIO_CODEC");
		rep("AUDIO_DAC_EN_", "AUDIO_CODEC_DAC");
		rep("AUDIO_ADC_EN_", "AUDIO_CODEC_ADC");

		if(!strncmp(name, "SENSOR_EN_", 10)){
			if(value[0] != 'y') continue;
			if(sensors[0] != '\0') strcat(sensors, " ");
			strcat(sensors, name+10);
			fprintf(fp1,"#define CONFIG_%s 1\n", name); \
			continue;
		}

		if(value[0] == 'y'){
			if(!gen_h){
				printf("%s := 1\n", name);
			}
			fprintf(fp,"CONFIG_%s := 1\n", name);
			fprintf(fp1,"#define CONFIG_%s 1\n", name);
			continue;
		}

		if(value[0] == '\"'){
			value ++;
			if(value[strlen(value)-1] == '\"'){
				value[strlen(value)-1] = '\0';
			}
			if(value[0] == '\0') continue;
			if(!gen_h){
				printf("%s := %s\n", name, value);
			}
			fprintf(fp, "CONFIG_%s := %s\n", name, value);
			fprintf(fp1,"#define CONFIG_%s \"%s\"\n", name, value);
			continue;
		}
/*
		if(value[0] == '('){
			value ++;
			if(value[strlen(value)-1] != ')'){
				continue;
			}

			if(value[0] == '\0') {
				continue;
			}

			if(!gen_h){
				printf("%s := %s\n", name, value);
			}
			fprintf(fp, "CONFIG_%s := %s\n", name, value);
			fprintf(fp1,"#define CONFIG_%s \"%s\"\n", name, value);
			continue;
		}

		if(value[0] >= '0' && value[0] <= '9'){
			if(!gen_h){
				printf("%s := %s\n", name, value);
			}
			fprintf(fp, "CONFIG_%s := %s\n", name, value);
			fprintf(fp1,"#define CONFIG_%s %s\n", name, value);
			continue;
		}
*/
		if(!gen_h){
			printf("%s := %s\n", name, value);
		}

		fprintf(fp, "CONFIG_%s := %s\n", name, value);
		fprintf(fp1,"#define CONFIG_%s %s\n", name, value);

		continue;
	}
	if(sensors[0]){
		if(!gen_h){
			printf("SENSORS := %s\n", sensors);
		}
		fprintf(fp, "CONFIG_SENSORS := %s\n", sensors);
	}

	fclose(fp);
	fclose(fp1);

	fp = fopen(tmpfilename, "r");
	fp1 = fopen(tmpfilename1, "r");
	if(fp == NULL || fp1==NULL){
		printf("#cannot open fp/fp1\n");
		exit(0);
	}
	if(!gen_h){
		printf("\n\n######## new style configs ########\n\n");
		while(fgets(buf, 1024, fp)){
			printf("%s", buf);
		}
	}
	fclose(fp);

	if(gen_h){
		while(fgets(buf, 1024, fp1)){
			printf("%s", buf);
		}
	}
	fclose(fp1);

	sprintf(buf, "rm -f %s\n", tmpfilename);
	system(buf);
	sprintf(buf, "rm -f %s\n", tmpfilename1);
	system(buf);

	return 0;
}
