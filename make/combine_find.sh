#!/bin/bash

chip=r3
prj=$1
fname=$2
filename_ret=

rm -f ../build/$prj/.find_fname.txt

checkwififw()
{
	postfix=$1

	if [ ! -z $filename_ret ] ; then
		return
	fi

	wifi_configs=`grep -r CONFIG_WIFIMODULE_ ../prj/$prj/*.config | grep "=y" | sed -e "s/^.*:CONFIG_WIFIMODULE_//g" | sed -e "s/=y.*$//g" | sort | uniq`

	for wifichip in $wifi_configs ; do
		if [ -e ../prj/$prj/wifi_fw_${wifichip}_${postfix}.bin ] ; then
			filename_ret=../prj/$prj/wifi_fw_${wifichip}_${postfix}.bin
			return
		fi
		if [ -e ../share/network/libwlan_chip/fw/wifi_fw_${wifichip}_${postfix}.bin ] ; then
			filename_ret=../share/network/libwlan_chip/fw/wifi_fw_${wifichip}_${postfix}.bin
			return
		fi
	done
}

find_fname()
{
	if [ `expr match "$fname" "res/"` -gt 0 ] ; then
#it's res/, find the directory
		if [ -d ../prj/$prj/$fname ] ; then
			filename_ret=../prj/$prj/$fname
		else
			echo "ERROR: resources directory ../prj/$prj/$fname not found"
			exit -1
		fi
		return
	fi

	if [ -e ../prj/$prj/$fname ] ; then
		filename_ret=../prj/$prj/$fname
	fi
	if [ -z $filename_ret ] ; then
		if [ -e ../build/$prj/$fname ] ; then
			filename_ret=../build/$prj/$fname
		fi
	fi
	if [ -z $filename_ret ] ; then
		for file in "$( ls ../prj/$prj/${fname/./*.} 2>/dev/null )" ; do
			filename_ret=$file
		done
	fi

	if [ -z $filename_ret ] ; then
		if [ `expr match "$fname" "sensor"` -gt 0 ] ; then
			if [ -e ../mpu/share/sensor_setting/$fname ] ; then
				filename_ret=../mpu/share/sensor_setting/$fname
			fi
		fi
	fi

	if [ -z $filename_ret ] ; then
		if [ "$fname" == "WiFi_FW_1" ] ; then
			checkwififw 1
		fi
		if [ "$fname" == "WiFi_FW_2" ] ; then
			checkwififw 2
		fi
		if [ "$fname" == "WiFi_FW_mfg_1" ] ; then
			checkwififw mfg_1
			checkwififw 1
		fi
		if [ "$fname" == "WiFi_FW_mfg_2" ] ; then
			checkwififw mfg_2
			checkwififw 2
		fi
	fi

	if [ "$filename_ret" == "" ] ; then
		echo "ERROR: ${fname/./*.} not found"
		exit 1
	fi
}

find_fname $fname

	if [ "$filename_ret" == "" ] ; then
		echo "ERROR: ${fname/./*.} not found"
		exit 1
	fi

echo -n $filename_ret > ../build/$prj/.find_fname.txt

exit 0
