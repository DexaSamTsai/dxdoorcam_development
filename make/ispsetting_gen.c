#include "stdio.h"
#include <stdlib.h>
#include "string.h"

typedef struct s_regoffset
{
	unsigned int offset;
	char * name;
}t_regoffset;

typedef struct s_reggroup
{
	unsigned int start;
	unsigned int len;
	char * name;
	unsigned int start2;
	char * name2;
	t_regoffset offsets[1024];
}t_reggroup;

static t_reggroup regs[] =
{
	{
	.start = 0x10032000,
	.len = 0x2000,
	.name = "L_BASE_ADDRESS",
	},

	{
	.start = 0x10034000,
	.len = 0x2000,
	.name = "R_BASE_ADDRESS",
	},

	{
	.start = 0xe0088100,
	.len = 0x80,
	.name = "ISP_LENC_ADDR_L",
	.start2 = 0xe008c100,
	.name2 = "ISP_LENC_ADDR_R",
	.offsets = {
		{
		.offset = 0x8,
		.name = "ISP_LENC_OFFSET_HVSCALE",
		},
	},
	},
};

//#define PARSE_DEFINE

static void help(char * argv0)
{
	printf("ERROR: usage: %s input.txt\n", argv0);
	exit(-1);
}

#ifdef PARSE_DEFINE

#define MAXDEF	64
char deftable[MAXDEF][256];
int def_num = 0;
#define DEFDEEP	16
int defskip_value[DEFDEEP];
int defskip_level = 0;

static int def_needskip(void)
{
	if(defskip_level && defskip_value[defskip_level-1]){
		return 1;
	}
	return 0;
}

static int defskip_add(int skip)
{
	defskip_value[defskip_level] = skip;
	defskip_level ++;
}

static int defskip_revert(void)
{
	if(defskip_level > 0){
		defskip_value[defskip_level-1] = 1 - defskip_value[defskip_level-1];
	}
	return 0;
}

static int defskip_del(void)
{
	if(defskip_level > 0){
		defskip_level --;
	}
}

static char * def_find(char * c)
{
	int i;
	for(i=0; i<def_num; i++){
		if(!strcmp(deftable[i], c)){
			return deftable[i];
		}
	}
	return NULL;
}

static int def_add(char * c)
{
	if(strlen(c) > 256){
		printf("ERROR: define too long:%s\n", c);
		return -1;
	}
	if(def_num >= MAXDEF){
		printf("ERROR: define too many:%s\n", c);
		return -1;
	}
	if(def_find(c)){
		return 0;
	}
	strcpy(deftable[def_num], c);
	def_num ++;
	return 0;
}
#endif

int main(int argc, char ** argv)
{
	if(argc < 2){
		help(argv[0]);
	}

	FILE * fp;
	fp = fopen(argv[1], "r");
	if(fp == NULL){
		printf("ERROR: cannot open %s\n", argv[1]);
		return -2;
	}

	char buf[1024];
	while(fgets(buf, 1024, fp)){
		char comment[1024];
		comment[0] = '\0';
		char * c = strchr(buf, '\n');
		if(c){
			*c = '\0';
		}
		c = strchr(buf, '\r');
		if(c){
			*c = '\0';
		}
		c = strchr(buf, ';');
		if(c){
			if(c != buf){
				//not from head
				sprintf(comment, "	//%s", c+1);
			}else{
				sprintf(comment, "//%s", c+1);
			}
			*c = '\0';
		}

#define C_SKIPBLANK do{ while(*c==' ' || *c=='\t') c++; }while(0)
		c=buf;
		C_SKIPBLANK;
		if(c[0] == '\0'){
			printf("%s", buf);
			goto check_comment;
		}
		if(c[0] == '#'){
#ifdef PARSE_DEFINE
			if(!strncmp(c, "#define", 7)){
				c+=7;
				C_SKIPBLANK;
				def_add(c);
			}else if(!strncmp(c, "#ifdef", 6)){
				c+=6;
				C_SKIPBLANK;
				if(!def_find(c)){
					defskip_add(1);
				}else{
					defskip_add(0);
				}
			}else if(!strncmp(c, "#else", 5)){
				c+=5;
				defskip_revert();
			}else if(!strncmp(c, "#endif", 6)){
				c+=6;
				defskip_del();
			}else{
				printf("ERROR: unknown: %s\n", c);
			}
			continue;
#else
			printf("%s", c);
			goto check_comment;
#endif
		}else{
#ifdef PARSE_DEFINE
			if(def_needskip()){
				continue;
			}

#endif
		}
		char id_s[64];
		char addr_s[64];
		char value_s[64];
		if(sscanf(c, "%s %s %s", id_s, addr_s, value_s) != 3){
			printf("ERROR: unknown %s\n", c);
			continue;
		}
		unsigned int addr = strtoul(addr_s, NULL, 16);
		unsigned int value = strtoul(value_s, NULL, 16);

		if((addr >= 0xe0000000 && addr <= 0xf0000000) || (addr >= 0x10032000 && addr < 0x10036000)){
			int i;
			int found = 0;
			for(i=0; i < sizeof(regs)/sizeof(regs[0]); i++){
				char * group_name;
				unsigned int group_base;
				if(addr >= regs[i].start && addr < (regs[i].start + regs[i].len)){
					group_name = regs[i].name;
					group_base = regs[i].start;
				}else if(addr >= regs[i].start2 && addr < (regs[i].start2 + regs[i].len)){
					group_name = regs[i].name2;
					group_base = regs[i].start2;
				}else{
					continue;
				}
				int j;
				char * offset_name = NULL;
				for(j=0; j < sizeof(regs[i].offsets)/sizeof(regs[i].offsets[0]); j++){
					if(regs[i].offsets[j].name == NULL){
						break;
					}
					if(addr - group_base == regs[i].offsets[j].offset){
						offset_name = regs[i].offsets[j].name;
						break;
					}
				}
				if(offset_name){
					printf("	WriteReg32(%s + %s, 0x%08x);", group_name, offset_name, value);
				}else{
					printf("	WriteReg32(%s + 0x%04x, 0x%08x);", group_name, addr - group_base, value);
				}
				found = 1;
				break;
			}
			if(!found){
				printf("	WriteReg32(0x%08x, 0x%08x);", addr, value);
			}
		}else{
			printf("	//TODO: WriteReg32(0x%08x, 0x%08x);", addr, value);
		}

check_comment:
		printf("%s\n", comment);
		continue;
	}

	fclose(fp);

	return 0;
}
