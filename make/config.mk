#########################################################################

ifndef CROSS_COMPILE
CROSS_COMPILE = ba-elf-
endif

ifdef V
Q:=
else
Q:=@
endif

#########################################################################

SHELL	:= $(shell if [ -x "$$BASH" ]; then echo $$BASH; \
		    else if [ -x /bin/bash ]; then echo /bin/bash; \
		    else echo sh; fi ; fi)

HOSTCC		= gcc
HOSTCFLAGS	= -Wall -Wstrict-prototypes -O2 -fomit-frame-pointer

MAKE = make

TOPDIR := $(shell if [ "$$PWD" != "" ]; then echo $$PWD; else pwd; fi)

CODEDIR = $(TOPDIR)/..

BLDDIR = $(CODEDIR)/build

#########################################################################

AS	= $(CROSS_COMPILE)as
LD	= $(CROSS_COMPILE)ld
CC	= $(CROSS_COMPILE)gcc
AR	= $(CROSS_COMPILE)ar
NM	= $(CROSS_COMPILE)nm
STRIP	= $(CROSS_COMPILE)strip
OC      = $(CROSS_COMPILE)objcopy
OD      = $(CROSS_COMPILE)objdump
RANLIB	= $(CROSS_COMPILE)ranlib

CFLAGS += -I$(CODEDIR)/ -I$(CODEDIR)/include -I$(CODEDIR)/mpu/export_include -I$(CODEDIR)/include/ertos/api/
################ lwIP config here ############
###  use lwIP 1.4.1
#LWIP_SRCDIR=lwip
#LWIP_VERSION=0x010401ff
###  use lwIP 2.0.0
LWIP_SRCDIR=lwip200
LWIP_VERSION=0x020000ff
##############################################
CFLAGS += -I$(CODEDIR)/share/network/$(LWIP_SRCDIR)/src/include/ -I$(CODEDIR)/share/network/$(LWIP_SRCDIR)/port/ -I$(CODEDIR)/share/network/$(LWIP_SRCDIR)/src/include/ipv4/ -DLWIP_VERSION=$(LWIP_VERSION)
CFLAGS += -I$(CODEDIR)/share/network/libcurl/include/ -I$(CODEDIR)/share/network/sntp/ -I$(CODEDIR)/share/network/libnetkits/
CFLAGS += -I$(CODEDIR)/share/media/jansson/ -I$(CODEDIR)/share/media/qrcode/quirc/ -I$(CODEDIR)/share/media/video_analysis/	-I$(CODEDIR)/share/media/sgui

ifeq ($(COMPILE_ADD_UNDERSCORE),1)
CFLAGS += -DCOMPILE_ADD_UNDERSCORE
endif

include $(TOPDIR)/$(CHIP)/mk.chip

ifdef BUILD_NO_ROM
CFLAGS += -DBUILD_NO_ROM
endif

ifdef FOR_FPGA
CFLAGS += -DFOR_FPGA
endif

#########################################################################
