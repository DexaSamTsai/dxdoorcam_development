#include "stdio.h"
#include <stdlib.h>

static void help(void)
{
	printf("usage: mpubin2array in.bin out.c\n");
	exit(-1);
}

int main(int argc, char ** argv)
{
	if(argc < 3){
		help();
	}

	FILE * fpin = fopen(argv[1], "r");
	if(fpin == NULL){
		help();
	}
	FILE * fpout = fopen(argv[2], "w");
	if(fpout == NULL){
		help();
	}

	fprintf(fpout, "#include \"includes.h\"\n");
	fprintf(fpout, "__attribute__ ((__section__ (\".mpubin\"))) const unsigned char _mpubin_code[]={\n");
	int c;
	while(1){
		c = fgetc(fpin);
		if(c == EOF){
			break;
		}
		fprintf(fpout, "0x%02x,", c);
	}
	fprintf(fpout, "\n};");

	fclose(fpin);
	fclose(fpout);
}
