#!/bin/bash

#$1: chip
#$2: prj

chip=$1
prj=$2

make combine -f Makefile.tools > /dev/null || exit 1
rm combine.h

for file in "$( ls ../prj/$prj/prj_cfg*.txt 2>/dev/null )" ; do
	prjfile=$file
done
if [ -z $prjfile ] ; then
	exit 0
fi
echo combine INFO: using $prjfile

echo ./combine $prj $prjfile
../build/make/combine $prj $prjfile
