#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>

/* CRC16 implementation acording to CCITT standards */
static const unsigned short crc16tab[256]= {
	0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
	0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
	0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
	0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
	0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
	0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
	0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
	0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
	0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
	0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
	0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
	0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
	0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
	0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
	0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
	0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
	0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
	0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
	0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
	0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
	0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
	0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
	0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
	0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
	0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
	0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
	0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
	0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
	0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
	0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
	0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
	0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};
  
static unsigned short crc16_ccitt(const void *buf, int len)
{
	register int counter;
	register unsigned short crc = 0;
	for( counter = 0; counter < len; counter++)
		crc = (crc<<8) ^ crc16tab[((crc>>8) ^ *(char *)buf++)&0x00FF];
	return crc;
}

static void save(char * filename, char * buf, int len)
{
	FILE * fp = fopen(filename, "w");
	if (fp == NULL){
		printf("ERROR: open %s for write fail\n", filename);
		exit(-1);
	}
	if (fwrite(buf, 1, len, fp) != len){
		printf("ERROR: write %s fail\n", filename);
		exit(-1);
	}
	fclose(fp);
	printf("INFO: Write %s success, length:%d\n", filename, len);
}

/*************************************************************************/

/*********** nand only **********/
static int nandflash_en = 0;
#define ROMNAND_CFG0_USEREAD30	0x01
#define ROMNAND_CFG0_USEDMA		0x02
#define ROMNAND_CFG0_ECC		0x04
#define ROMNAND_CFG0_BCH		0x08
static unsigned char nand_config[16];

/********** sflash only **********/
static int sflash_en = 0;
static unsigned int sflash_config[4];

/********** both nand and sflash ***********/
#define VALUE_INT(x) strtoul(x, NULL, 0)

static unsigned int page_size = 0;
static unsigned int block_size = 0;

typedef struct s_partition
{
	unsigned int magic_number;
	unsigned int bootloader_offset;
	unsigned int bootloader_ram;
	unsigned int bootloader_length;
	unsigned int system_config;
	unsigned int bootloader2_offset;
	unsigned int bootloader2_ram;
	unsigned int bootloader2_length;
	unsigned int fw_offset;
	unsigned int fw_length;
	unsigned int para_offset;
	unsigned int para_length;
	unsigned int fw_backup_offset;
	unsigned int fw_backup_length;
	unsigned int para_backup_offset;
	unsigned int para_backup_length;
	unsigned int calib_offset;
	unsigned int calib_length;
	unsigned int other_offset;
	unsigned int other_length;
	unsigned int sccbtable_id2;
	unsigned int quickset_id2;

	//not in header
	int backup_en;
	int fw_area_len;
	char * fw_area;
	int para_area_len;
	char * para_area;
	int single_len;
	char * single;
	int upgrade_len;
	char * upgrade;
}t_partition;

static t_partition g_part;

static void W16(char * buf, unsigned short n)
{
	buf[0] = (n >> 0) & 0xff;
	buf[1] = (n >> 8) & 0xff;
}

static void W32(char * buf, unsigned int n)
{
	buf[0] = (n >> 0) & 0xff;
	buf[1] = (n >> 8) & 0xff;
	buf[2] = (n >> 16) & 0xff;
	buf[3] = (n >> 24) & 0xff;
}

/********* file ********/
typedef struct s_file
{
	int item_id2;
	char item_mcu_bl[FILENAME_MAX];
	char item_mcu_fw[FILENAME_MAX];
	char item_name[FILENAME_MAX];
	char item_filename[FILENAME_MAX];

	char item_realfilename[FILENAME_MAX];
}t_file;

static t_file g_file;

/* real file */
#define MAXFILECNT 10

typedef struct s_fileitem
{
	char filename[FILENAME_MAX];
	unsigned int addr;
	unsigned int len;
	char * data;
}t_fileitem;

static int maxid2;
static t_fileitem fileitem[MAXFILECNT + 1]; //MAXFILECNT for para
static char prj_name[30];
static char boot_name[30];

static int version = 0;

/*********** others ************/
static int parse_file(void)
{
	if(g_file.item_filename[0] == '\0'){
		return 0;
	}

	if (!strcmp(g_file.item_filename, boot_name)) {
		strcpy(g_file.item_realfilename, boot_name);
	}
	if (!strcasecmp(g_file.item_name, "sccb_table")){
		strcpy(g_file.item_realfilename, "sccb_table.bin");
		g_part.sccbtable_id2 = g_file.item_id2;
	} else 	if (!strcasecmp(g_file.item_name, "quickset")){
		strcpy(g_file.item_realfilename, "quickset.bin");
		g_part.quickset_id2 = g_file.item_id2;
	} else 	if (!strcasecmp(g_file.item_name, "ba")){
		strcpy(g_file.item_realfilename, "ba.bin");
	} else 	if (!strcasecmp(g_file.item_name, "ramdisk")){
		strcpy(g_file.item_realfilename, "ramdisk.bin");
	} else 	if (!strcasecmp(g_file.item_name, "uImage")){
		strcpy(g_file.item_realfilename, "uImage");
	}

	if (g_file.item_id2 > maxid2) {
		maxid2 = g_file.item_id2;
	}
	
	strcpy(fileitem[g_file.item_id2].filename, g_file.item_realfilename);
	printf("==>INFO file id: %d,name:%s\n", g_file.item_id2, fileitem[g_file.item_id2].filename);

	/* clear */
	memset(&g_file, 0, sizeof(g_file));
}

#define ALIGN_BLOCK(x) (((x) + block_size - 1) & (~(block_size -1)))

static int load_file(t_fileitem *item)
{
	struct stat st;

	if (item->filename == '\0'){
		return -1;
	}
	
	if (stat(item->filename, &st) != 0) {
		printf("ERROR: stat file %s error\n", item->filename);
		exit(-1);
	}
	
	item->len = st.st_size;
	item->data = malloc(st.st_size);
	if (item->data == NULL) {
		printf("ERROR: malloc %ld fail\n", st.st_size);
		exit(-1);
	}
	
	printf("	file name:%s,len:%d\n", item->filename, item->len);

	FILE *fp;
	fp = fopen(item->filename, "r");
	if (fp == NULL) {
		printf("ERROR: open file %s error\n", item->filename);
		exit(-1);
	}
	if (fread(item->data, 1, st.st_size, fp) != st.st_size) {
		printf("ERROR: read file %s error\n", item->filename);
		exit(-1);
	}
	fclose(fp);

	return 0;
}

static void combine(void)
{
	int i;
	int k = 0;
	
	g_part.bootloader_ram = 0x10000000;
	g_part.bootloader2_ram = 0x10000000;

	if (fileitem[0].filename[0] == '\0'){
		printf("ERROR: bootloader not exist(should have arg with id2=0)\n");
		exit(-1);
	}

	//load file
	for (i = 0; i <= MAXFILECNT; i++) {
		if (strlen(fileitem[i].filename) == 0) {
			continue;
		}

		printf("fileitem%d,finename:%s\n", i, fileitem[i].filename);
		load_file(&fileitem[i]);
	}

	if (fileitem[0].len > g_part.bootloader_length){
		printf("WARNING: increase BlSize from %d to %d\n", g_part.bootloader_length, fileitem[0].len);
		g_part.bootloader_length = fileitem[0].len;
	}

	// init partition
	g_part.bootloader_offset = block_size;
	g_part.bootloader2_offset = g_part.bootloader_offset + ALIGN_BLOCK(g_part.bootloader_length);
	g_part.fw_offset = g_part.bootloader2_offset + ALIGN_BLOCK(g_part.bootloader2_length);
	g_part.para_offset = g_part.fw_offset + ALIGN_BLOCK(g_part.fw_length);

/*
 * header+uboot+fw(para): 512KB
 * 				  ba.bin: 512KB
 * 				  uImage: 4MB
 * 			 ramdisk.bin: 9MB
 * 	total: 14MB
 */
	g_part.single_len = 0xE00000;  ///
	printf("single_len:0x%x\n", g_part.single_len);

	g_part.single = (char *)malloc(g_part.single_len);
	if (g_part.single == NULL){
		printf("ERROR: malloc %d fail\n", g_part.bootloader_offset + g_part.bootloader_length);
		exit(-1);
	}
	memset(g_part.single, 0, g_part.single_len);

	g_part.fw_area = (char *)malloc(g_part.fw_length);
	if (g_part.fw_area == NULL){
		printf("ERROR: malloc %d fail\n", g_part.fw_length);
		exit(-1);
	}

	//generate FW area
	memset(g_part.fw_area, 0, g_part.fw_length);
	g_part.fw_area_len = 0x30 + (maxid2 - 1)*8;
	printf("maxid2:%d,fw_area_len:0x%x\n", maxid2,g_part.fw_area_len);

	for (i = 4; i <= maxid2; i++) {
		if (fileitem[i].filename[0] == '\0'){
			continue;
		}
		if (fileitem[i].data == NULL) {
			continue;
		}

		//FIXME: put this before 'memcpy', otherwise, Segment Fault may happen.
		if (g_part.fw_area_len + fileitem[i].len > g_part.fw_length){
			printf("ERROR: fw_area length exceed (%d > %d by id %d)\n", g_part.fw_area_len + fileitem[i].len, g_part.fw_length, i);
			exit(-1);
		}

		fileitem[i].addr = g_part.fw_area_len;
		memcpy(g_part.fw_area + g_part.fw_area_len, fileitem[i].data, fileitem[i].len);
		g_part.fw_area_len += fileitem[i].len;
		g_part.fw_area_len = (g_part.fw_area_len + 3) & ~3;

		W32(g_part.fw_area + 0x10 + (i-2)*8, fileitem[i].addr);
		W32(g_part.fw_area + 0x14 + (i-2)*8, fileitem[i].len);

		printf("file:%s, addr:0x%x,len:0x%x,fw_area_len:0x%x\n", fileitem[i].filename, fileitem[i].addr, fileitem[i].len, g_part.fw_area_len);
	}

	//FW area header
	W32(g_part.fw_area + 0, 0x4f565442);
	W32(g_part.fw_area + 4, version);
	W32(g_part.fw_area + 8, g_part.fw_area_len);
	W32(g_part.fw_area + 0xc, maxid2 - 1);
	W32(g_part.fw_area + 0x10 + (maxid2 - 1)*8, 0/*res_id2*/);
	g_part.fw_area[0x2e + (maxid2 - 1)*8] = 0x56;
	g_part.fw_area[0x2f + (maxid2 - 1)*8] = 0x4f;
	unsigned short crc = crc16_ccitt(g_part.fw_area, 0x2c + (maxid2 - 1)*8);
	g_part.fw_area[0x2c + (maxid2 - 1)*8] = (crc >> 0) & 0xff;
	g_part.fw_area[0x2d + (maxid2 - 1)*8] = (crc >> 8) & 0xff;

	printf("bootloader_offset:0x%x len:0x%x\n",g_part.bootloader_offset, g_part.bootloader_length);
	printf("bootloader2_offset:0x%x len2:0x%x\n",g_part.bootloader2_offset, g_part.bootloader2_length);
	printf("fw_offset:0x%x fw_length:0x%x fw_area_len:0x%x\n", g_part.fw_offset, g_part.fw_length, g_part.fw_area_len);
	printf("para_offset:0x%x len:0x%x\n",g_part.para_offset, g_part.para_length);

////generate single.bin
	memset(g_part.single, 0, g_part.para_offset + g_part.para_length);

////copy uboot.bin to 0x20000
	memcpy(g_part.single + g_part.bootloader_offset, fileitem[0].data, fileitem[0].len);
////copy sccb_table.bin and quickset.bin to 0x60000
	memcpy(g_part.single + g_part.fw_offset, g_part.fw_area, g_part.fw_area_len);
////copy ba.bin to 0x80000
	for (i = 1; i < 4; i++) {
		printf("No.%d ", i);
		if (fileitem[i].filename[0] == '\0'){
			continue;
		}
		if (fileitem[i].data == NULL) {
			continue;
		}

		if (!strcmp(fileitem[i].filename, "ba.bin")) {
			printf("ba.bin len:%d", fileitem[i].len);
			memcpy(g_part.single + 0x80000, fileitem[i].data, fileitem[i].len);
		} else if (!strcmp(fileitem[i].filename, "uImage")) {
			printf("uImage len:%d", fileitem[i].len);
			memcpy(g_part.single + 0x100000, fileitem[i].data, fileitem[i].len);
		} else if (!strcmp(fileitem[i].filename, "ramdisk.bin")) {
			printf("ramdisk.bin len:%d", fileitem[i].len);
			memcpy(g_part.single + 0x500000, fileitem[i].data, fileitem[i].len);
		}
		printf(" success!\n");
	}

	printf("quickset_id2:0x%x,sccbtable_id2:0x%x\n",g_part.quickset_id2,g_part.sccbtable_id2);
	W32(g_part.single + 0x00, 0x4f565449);
	W32(g_part.single + 0x04, g_part.quickset_id2); //quickset id2
	W32(g_part.single + 0x08, 0); //reserved
	W32(g_part.single + 0x0c, g_part.sccbtable_id2); //sccbtbl id2
	W32(g_part.single + 0x10, 0); //reserved
	W32(g_part.single + 0x14, 0); //reserved
	W32(g_part.single + 0x18, g_part.bootloader_offset); //bootloader_offset
	W32(g_part.single + 0x1c, g_part.bootloader_ram); //bootloader_ram
	W32(g_part.single + 0x20, fileitem[0].len); //bootloader_len
	crc = crc16_ccitt(fileitem[0].data, fileitem[0].len);
	W32(g_part.single + 0x24, crc); //bootloader CRC
	printf("bootloader_offset:0x%x\n", g_part.bootloader_offset);
	printf("bootloader_ram:0x%x\n", g_part.bootloader_ram);
	printf("fileitem[0].len:%d\n", fileitem[0].len);
///	printf("crc:0x%x\n", crc);

	if (nandflash_en){
		memcpy(g_part.single + 0x28, nand_config, 16);
/*	for (k = 0; k < 16; k++)
		printf("0x%x ",nand_config[k]);
	printf("\n");
*/
	} else {
		memcpy(g_part.single + 0x28, sflash_config, 16);
	}

	W32(g_part.single + 0x38, 0); //bootloader2_offset
	W32(g_part.single + 0x3c, 0); //bootloader2_ram
	W32(g_part.single + 0x40, 0); //bootloader2_len
	W32(g_part.single + 0x44, 0); //bootloader2 CRC
	W32(g_part.single + 0x48, /*0x00090200*/0); //TODO: system config
	W32(g_part.single + 0x4c, g_part.fw_offset);
	W32(g_part.single + 0x50, g_part.fw_length);
	W32(g_part.single + 0x54, 0); //para_offset
	W32(g_part.single + 0x58, 0); //para_length
	W32(g_part.single + 0x5c, 0); //fw_backup_offset
	W32(g_part.single + 0x60, 0); //fw_backup_length
	W32(g_part.single + 0x64, 0); //para_backup_offset
	W32(g_part.single + 0x68, 0); //para_backup_length
	W32(g_part.single + 0x6c, 0); //calib_offset
	W32(g_part.single + 0x70, 0); //calib_length
	W32(g_part.single + 0x74, 0); //other_offset
	W32(g_part.single + 0x78, 0); //other_length
	
	crc = crc16_ccitt(g_part.single, 0x7c);
	W32(g_part.single + 0x7c, crc); //header CRC

	save("SINGLE.bin", g_part.single, g_part.single_len);

	free(g_part.fw_area);
	free(g_part.single);

	printf("========SINGLE.bin OK=======\n");
}

static void help()
{
	printf("usage: ./combine_linux prj_cfg.txt u-boot.bin.\n");
}

//remove prefix ' '
#define REMOVE_CHAR(str, ch) do{ \
			while (*str == ch) str ++; \
			while (str[strlen(str)-1] == ch) str[strlen(str)-1]='\0';  \
}while (0)

int main(int argc, char ** argv)
{
	int sf_sector_size = 0; 
	unsigned int nand_page_size = 0;
	unsigned int nand_block_size = 0;

	if (argc != 3){
		help(argv[0]);
	}

	memset(prj_name, 0, 30);
	memset(boot_name, 0, 30);

	if (argc < 2)
		strcpy(prj_name, "prj_cfg.txt");
	else
		strcpy(prj_name, argv[1]);

	if (argc < 3)
		strcpy(boot_name, "u-boot.bin");
	else
		strcpy(boot_name, argv[2]);

	printf("prj_cfg name:%s, u-boot name:%s\n", prj_name, boot_name);

	FILE *fp = fopen(prj_name, "r");
	if (fp == NULL){
		printf("failed open1 %s\n", argv[1]);
		return -1;
	}
	FILE *fp_boot = fopen(boot_name, "r");
	if (fp_boot == NULL){
		printf("failed open2 %s\n", argv[2]);
		return -1;
	}

	maxid2 = 0;
	memset(&fileitem[0], 0, sizeof(fileitem));
	memset(nand_config, 0, 16);
	memset(sflash_config, 0, 16);
	memset(&g_part, 0, sizeof(g_part));

	char buf[256];
	while (fgets(buf, 255, fp)) {
		char *c;
		if ((c=strchr(buf, '\n')) != NULL) 
			*c = '\0';
		if ((c=strchr(buf, '\r')) != NULL) 
			*c = '\0';
		if ((c=strchr(buf, ';')) != NULL) 
			*c = '\0';
		if ((c=strchr(buf, '#')) != NULL) 
			*c = '\0';
		if ((c=strchr(buf, '/')) != NULL) {
			if (c[1] == '/') {
				*c = '\0';
			}
		}

		if (buf[0] == '\0') {
			continue;
		}

		c = strchr(buf, '=');
		if (c) {
			char * name = buf;
			char * value = c+1;
			
			*c = '\0';
			REMOVE_CHAR(name, ' ');
			REMOVE_CHAR(value, ' ');
			REMOVE_CHAR(name, '\t');
			REMOVE_CHAR(value, '\t');

			if (!strcasecmp(name, "HasNand")) {
				nandflash_en = VALUE_INT(value);
			} else if (!strcasecmp(name, "HasSFlash")) {
				sflash_en = VALUE_INT(value);
			} else if (!strcasecmp(name, "Nand_OOBCMD")) {
				nand_config[0] = VALUE_INT(value);
			} else if (!strcasecmp(name, "Nand_ColSize")) {
				nand_config[1] = VALUE_INT(value);
			} else if (!strcasecmp(name, "Nand_RowSize")) {
				nand_config[2] = VALUE_INT(value);
			} else if (!strcasecmp(name, "Nand_Read30")) {
				nand_config[3] |= ((VALUE_INT(value) > 0) ? ROMNAND_CFG0_USEREAD30 : 0);
			} else if (!strcasecmp(name, "Nand_useDMA")) {
				nand_config[3] |= ((VALUE_INT(value) > 0) ? ROMNAND_CFG0_USEDMA : 0);
			} else if (!strcasecmp(name, "Nand_PageSize")) {
				nand_page_size = VALUE_INT(value);
				W16(nand_config + 8, nand_page_size);
			} else if (!strcasecmp(name, "Nand_BlockSize")) {
				nand_block_size = VALUE_INT(value);
				W32(nand_config + 12, nand_block_size);
			} else if (!strcasecmp(name, "SFlash_SectorSize")){
				sf_sector_size = VALUE_INT(value);
			} else if (!strcasecmp(name, "SFlash_DMA")){
				if (VALUE_INT(value) != 0){
					sflash_config[0] |= 0x1;
				}
			} else if (!strcasecmp(name, "SFlash_Mode")){
				if (VALUE_INT(value) != 0){
					sflash_config[0] |= ((VALUE_INT(value)&0x7)<<1);
				}
			} else if (!strcasecmp(name, "SFlash_Dummy_EN")){
				if (VALUE_INT(value) != 0){
					sflash_config[0] |= 0x10;
				}
			} else if (!strcasecmp(name, "SFlash_Dummy")){
				if (VALUE_INT(value) != 0){
					sflash_config[0] |= ((VALUE_INT(value)&0xf)<<5);
				}
			} else if (!strcasecmp(name, "SFlash_Quadcheck_EN")){
				if (VALUE_INT(value) != 0){
					sflash_config[0] |= ((VALUE_INT(value)&0xf)<<9);
				}
			} else if (!strcasecmp(name, "SFlash_config1")){
				if (VALUE_INT(value) != 0){
					sflash_config[1] |= VALUE_INT(value);
				}
			}else if(!strcasecmp(name, "SFlash_config2")){
				if (VALUE_INT(value) != 0){
					sflash_config[2] |= VALUE_INT(value);
				}
			}else if(!strcasecmp(name, "SFlash_config3")){
				if (VALUE_INT(value) != 0){
					sflash_config[3] |= VALUE_INT(value);
				}
			} else if (!strcasecmp(name, "Part_BlSize")) {
				g_part.bootloader_length = VALUE_INT(value);
			} else if (!strcasecmp(name, "Part_Bl2Size")) {
				g_part.bootloader2_length = VALUE_INT(value);
			}else if(!strcasecmp(name, "Part_FwSize")){
				g_part.fw_length = VALUE_INT(value);
			} else if (!strcasecmp(name, "Part_CalibSize")) {
				g_part.calib_length = VALUE_INT(value);
			} else if (!strcasecmp(name, "Part_OtherSize")) {
				g_part.other_length = VALUE_INT(value);
			} else if (!strcasecmp(name, "Part_BackupEnable")) {
				g_part.backup_en = VALUE_INT(value);
			} else if (!strcasecmp(name, "File_id2")) {
				g_file.item_id2 = VALUE_INT(value);
			} else if (!strcasecmp(name, "File_MCU_BL")) {
				strcpy(g_file.item_mcu_bl, value);
			} else if (!strcasecmp(name, "File_MCU_FW")) {
				strcpy(g_file.item_mcu_fw, value);
			} else if (!strcasecmp(name, "File_name")) {
				printf("File_name:%s\n", value);
				strcpy(g_file.item_name, value);
//				printf("File_name:%s\n", g_file.item_name);
			} else if (!strcasecmp(name, "File_filename")) {
				printf("File_filename:%s\n", value);
				strcpy(g_file.item_filename, value);
			}
		} else {
			if (!strncasecmp(buf, "[FILE", 5)) {
				printf("[FILE] item_id2:%d,item_name:%s\n", g_file.item_id2, g_file.item_name);
				if (parse_file() < 0) {
					return -1;
				}
				printf("\n");
			}
		}
	}
	
	fclose(fp);
	fclose(fp_boot);

	if (parse_file() < 0) {
		return -1;
	}

	if (nandflash_en) {
		block_size = nand_block_size;
		page_size = nand_page_size;
	} else {
		block_size = sf_sector_size; /*Sector size, but not Block size*/
	}

	combine();

	return 0;
}

