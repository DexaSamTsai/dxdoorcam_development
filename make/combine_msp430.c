#include "stdio.h"
#include "string.h"

void help(char * arg0)
{
	printf("Usage:%s bootloader.hex fw.hex output\n", arg0);
}

#define debug(x...) printf(x)

int main(int argc, char ** argv)
{
	if(argc != 4){
		help(argv[0]);
		return -1;
	}

	FILE * fp_bl = fopen(argv[1], "r");
	if(fp_bl == NULL){
		printf("cannot open %s\n", argv[1]);
		return -1;
	}
	FILE * fp = fopen(argv[2], "r");
	if(fp == NULL){
		printf("cannot open %s\n", argv[2]);
		return -1;
	}
	FILE * fpo = fopen(argv[3], "w");
	if(fp == NULL){
		printf("cannot write %s\n", argv[3]);
		return -1;
	}

	char buf[1024];
	//get bootloader len
	int bllen = -1;
	while(fgets(buf, 1024, fp_bl)){
		if(buf[0] == '@'){
			if(bllen == -1){
				bllen = 0;
				continue;
			}else{
				break;
			}
		}else{
			bllen += (strlen(buf) / 3);
		}
	}
	fseek(fp_bl, 0, SEEK_SET);
	bllen -= 0x10;
	debug("MSP430 BootLoader len:%d\n", bllen);

	//write bootloader
	int lines = 0;
	while(fgets(buf, 1024, fp_bl)){
		if(buf[0] == 'q'){
			break;
		}
		if(lines == 1){
			fwrite(buf, 1, 6*3, fpo); //write first 6 bytes
			fprintf(fpo, "%02x %02x ", (bllen) & 0xff, (bllen >> 8) & 0xff); //write bllen
			fwrite(buf+8*3, 1, strlen(buf+8*3), fpo); //write left data
		}else{
			fwrite(buf, 1, strlen(buf), fpo);
		}
		lines ++;
	}

	//get firmware len
	int fwlen = -2;
	while(fgets(buf, 1024, fp)){
		if(buf[0] == '@'){
			fwlen ++;
			continue;
		}
		if(buf[0] == 'q'){
			break;
		}
		if(fwlen >= 0){
			fwlen += (strlen(buf) / 3);
		}
	}
	fseek(fp, 0, SEEK_SET);
	fwlen += 0x5a;
	debug("MSP430 FW len:%d\n", fwlen);

	//write firmware
	lines = 0;
	while(fgets(buf, 1024, fp)){
		if(buf[0] == 'q'){
			break;
		}
		if(lines == 1){
			fwrite(buf, 1, 6*3, fpo); //write first 6 bytes
			fprintf(fpo, "%02x %02x ", (fwlen) & 0xff, (fwlen >> 8) & 0xff); //write bllen
			fwrite(buf+8*3, 1, strlen(buf+8*3), fpo); //write left data
		}else{
			fwrite(buf, 1, strlen(buf), fpo);
		}
		lines ++;
	}
	fseek(fp, 0, SEEK_SET);

	//write firmware backup
	lines = 0;
	while(fgets(buf, 1024, fp)){
		if(buf[0] == 'q'){
			break;
		}
		if(lines == 1){
			fwrite(buf, 1, 6*3, fpo); //write first 6 bytes
			fprintf(fpo, "%02x %02x ", (fwlen) & 0xff, (fwlen >> 8) & 0xff); //write bllen
			fwrite(buf+8*3, 1, strlen(buf+8*3), fpo); //write left data
		}else if(lines == 0){
			fprintf(fpo, "@e400\r\n");
		}else if(lines == 2){
			fprintf(fpo, "@e46a\r\n");
		}else{
			fwrite(buf, 1, strlen(buf), fpo);
		}
		lines ++;
	}

	//write end
	fprintf(fpo, "q\r\n");

	fclose(fpo);
	fclose(fp);
	fclose(fp_bl);

	return 0;
}
