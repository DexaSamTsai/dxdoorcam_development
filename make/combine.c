#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include "combine.h"

#define RES_MAX_GROUP    10
#define MAX_ITEM_INGROUP 1000
#define u32 unsigned int


static int localfile_only = 0;
static int res_id2 = 0;
unsigned int groupoffset[RES_MAX_GROUP+2];
unsigned int itemoffset[MAX_ITEM_INGROUP*512+16];

/* CRC16 implementation acording to CCITT standards */

static const unsigned short crc16tab[256]= {
    0x0000,0x1021,0x2042,0x3063,0x4084,0x50a5,0x60c6,0x70e7,
    0x8108,0x9129,0xa14a,0xb16b,0xc18c,0xd1ad,0xe1ce,0xf1ef,
    0x1231,0x0210,0x3273,0x2252,0x52b5,0x4294,0x72f7,0x62d6,
    0x9339,0x8318,0xb37b,0xa35a,0xd3bd,0xc39c,0xf3ff,0xe3de,
    0x2462,0x3443,0x0420,0x1401,0x64e6,0x74c7,0x44a4,0x5485,
    0xa56a,0xb54b,0x8528,0x9509,0xe5ee,0xf5cf,0xc5ac,0xd58d,
    0x3653,0x2672,0x1611,0x0630,0x76d7,0x66f6,0x5695,0x46b4,
    0xb75b,0xa77a,0x9719,0x8738,0xf7df,0xe7fe,0xd79d,0xc7bc,
    0x48c4,0x58e5,0x6886,0x78a7,0x0840,0x1861,0x2802,0x3823,
    0xc9cc,0xd9ed,0xe98e,0xf9af,0x8948,0x9969,0xa90a,0xb92b,
    0x5af5,0x4ad4,0x7ab7,0x6a96,0x1a71,0x0a50,0x3a33,0x2a12,
    0xdbfd,0xcbdc,0xfbbf,0xeb9e,0x9b79,0x8b58,0xbb3b,0xab1a,
    0x6ca6,0x7c87,0x4ce4,0x5cc5,0x2c22,0x3c03,0x0c60,0x1c41,
    0xedae,0xfd8f,0xcdec,0xddcd,0xad2a,0xbd0b,0x8d68,0x9d49,
    0x7e97,0x6eb6,0x5ed5,0x4ef4,0x3e13,0x2e32,0x1e51,0x0e70,
    0xff9f,0xefbe,0xdfdd,0xcffc,0xbf1b,0xaf3a,0x9f59,0x8f78,
    0x9188,0x81a9,0xb1ca,0xa1eb,0xd10c,0xc12d,0xf14e,0xe16f,
    0x1080,0x00a1,0x30c2,0x20e3,0x5004,0x4025,0x7046,0x6067,
    0x83b9,0x9398,0xa3fb,0xb3da,0xc33d,0xd31c,0xe37f,0xf35e,
    0x02b1,0x1290,0x22f3,0x32d2,0x4235,0x5214,0x6277,0x7256,
    0xb5ea,0xa5cb,0x95a8,0x8589,0xf56e,0xe54f,0xd52c,0xc50d,
    0x34e2,0x24c3,0x14a0,0x0481,0x7466,0x6447,0x5424,0x4405,
    0xa7db,0xb7fa,0x8799,0x97b8,0xe75f,0xf77e,0xc71d,0xd73c,
    0x26d3,0x36f2,0x0691,0x16b0,0x6657,0x7676,0x4615,0x5634,
    0xd94c,0xc96d,0xf90e,0xe92f,0x99c8,0x89e9,0xb98a,0xa9ab,
    0x5844,0x4865,0x7806,0x6827,0x18c0,0x08e1,0x3882,0x28a3,
    0xcb7d,0xdb5c,0xeb3f,0xfb1e,0x8bf9,0x9bd8,0xabbb,0xbb9a,
    0x4a75,0x5a54,0x6a37,0x7a16,0x0af1,0x1ad0,0x2ab3,0x3a92,
    0xfd2e,0xed0f,0xdd6c,0xcd4d,0xbdaa,0xad8b,0x9de8,0x8dc9,
    0x7c26,0x6c07,0x5c64,0x4c45,0x3ca2,0x2c83,0x1ce0,0x0cc1,
    0xef1f,0xff3e,0xcf5d,0xdf7c,0xaf9b,0xbfba,0x8fd9,0x9ff8,
    0x6e17,0x7e36,0x4e55,0x5e74,0x2e93,0x3eb2,0x0ed1,0x1ef0
};

unsigned short crc16_ccitt(const void *buf, int len)
{
    register int counter;
    register unsigned short crc = 0;
    for( counter = 0; counter < len; counter++)
        crc = (crc<<8) ^ crc16tab[((crc>>8) ^ *(char *)buf++)&0x00FF];
    return crc;
}

#define getu32l(x) (x) // (((x)>>24) | ((((x)>>16) & 0xff) << 8) | ((((x)>>8) & 0xff) << 16) | ((((x)) & 0xff) << 24) )
int iilen = 0;
int item_start = 0;
char * item_buf;
int itemcnt;
int itemindex_offset = 0;

static int gen_res_item_reset(void)
{
    item_start = 0;
    itemcnt = 0;
    itemindex_offset = 4;

    item_buf = malloc(16*1024*1024);
    if(item_buf == NULL){
        printf("ERROR:malloc error 3\n");
        return -1;
    }
    return 0;
}

static void gen_res_item_done(void)
{
    free(item_buf);
}

static int gen_res_item_add(FILE * fpr, char * key)
{
    //get length
    fseek(fpr, 0, SEEK_END);
    int file_len = ftell(fpr);
    fseek(fpr, 0, SEEK_SET);

    //set item offset
    itemoffset[itemindex_offset] = getu32l(file_len);
    itemoffset[itemindex_offset + 1] = item_start;

    if(iilen){
        itemindex_offset += (iilen / 4);
    }else{
        if(key == NULL){
            return -1;
        }
        itemoffset[itemindex_offset + 2] = strlen(key) + 1;
        strcpy((char *)(&itemoffset[itemindex_offset + 3]), key);
        itemindex_offset += (3 + (itemoffset[itemindex_offset + 2] + 3) / 4);
    }

    //load
    fread(item_buf + item_start, 1, file_len, fpr);
    item_start += file_len;

    itemcnt ++;

    if(key){
        printf("INFO: added %s to res\n", key);
    }

    return 0;
}

static int gen_res_dirscan(char * rootpath, char * subpath)
{
    DIR * dir;
    char path[2048];
    sprintf(path, "%s/%s", rootpath, subpath);
    dir = opendir(path);
    if(dir == NULL){
        printf("ERROR: opendir %s failed\n", path);
        return -1;
    }

    struct dirent * dt;
    while(1){
        dt = readdir(dir);
        if(dt == NULL){
            break;
        }
        if(dt->d_name[0] == '.'){
            continue;
        }
        struct stat s;
        char fullpath[2048];
        sprintf(path, "%s/%s", subpath, dt->d_name);
        sprintf(fullpath, "%s%s", rootpath, path);
        if(lstat(fullpath, &s) < 0){
            printf("ERROR: lstat %s failed\n", fullpath);
            continue;
        }
        if(S_ISDIR(s.st_mode)){
            if(gen_res_dirscan(rootpath, path) < 0){
                return -1;
            }
        }else{
            FILE * fpr = fopen(fullpath, "rb");
            if(fpr == NULL){
                printf("ERROR: open %s failed\n", fullpath);
            }else{
                if(gen_res_item_add(fpr, path) < 0){
                    return -1;
                }
                fclose(fpr);
            }
        }
    }
    return 0;
}

static int gen_res_group(char * group_output, char *group_input)
{
    FILE * fpo = fopen(group_output, "w");
    if(fpo == NULL){
        printf("ERROR: open %s for write fail\n", group_output);
        return -1;
    }

    int i;
    //check if /index.html exist
    char testdir[1024];
    sprintf(testdir, "%s/index.html", group_input);
    FILE * fptest = fopen(testdir, "rb");
    if(fptest == NULL){
        iilen = 8;
    }else{
        iilen = 0;
        fclose(fptest);
    }

    if(gen_res_item_reset() < 0){
        return -1;
    }

    if(iilen){
        //not html
        int item;
        for(item=0; item<MAX_ITEM_INGROUP; item++){
            char frdir[1024];

            sprintf(frdir, "%s/%d.wav", group_input, item+1);
            FILE * fpr = fopen(frdir, "rb");
            if(fpr == NULL){
                sprintf(frdir, "%s/%d.res", group_input, item+1);
                fpr = fopen(frdir, "rb");
                if(fpr == NULL){
                    break;
                }
            }

            if(gen_res_item_add(fpr, NULL) < 0){
                return -1;
            }

            fclose(fpr);
        }
    }else{
        //html
        if(gen_res_dirscan(group_input, "") < 0){
            return -1;
        }
    }

    if(itemcnt == 0){
        return 0;
    }

    //write group header
    itemoffset[0] = getu32l(0x72657369);
    itemoffset[1] = getu32l(itemcnt);
    itemoffset[2] = getu32l((itemindex_offset - 4)*4);
    itemoffset[3] = getu32l(iilen);

    //fix the offset
    if(iilen){
        for(i=0; i<itemcnt; i++){
            itemoffset[i*(iilen/4)+4+1] += itemindex_offset*4;
        }
    }else{
        int curoff = 4;
        for(i=0; i<itemcnt; i++){
            itemoffset[curoff + 1] += itemindex_offset*4;
            curoff += (3 + ((itemoffset[curoff + 2] + 3)/4) );
        }
    }

    fwrite(itemoffset, 1, itemindex_offset*4, fpo);
    fwrite(item_buf, 1, item_start, fpo);

    fclose(fpo);

    gen_res_item_done();

    return item_start + itemindex_offset*4;
}

static int gen_res(char * output_file, char * input_dir)
{
    FILE * fpo = fopen(output_file, "w");
    if(fpo == NULL){
        printf("ERROR: open %s for write fail\n", output_file);
        return -1;
    }

    int i;
    int group;
    int group_start = 0;
    char * group_buf = malloc(8*1024*1024);
    if(group_buf == NULL){
        printf("ERROR:malloc error 4\n");
        return -1;
    }

    for(group=0; group<RES_MAX_GROUP; group++){
        char group_output[1024];
        char group_input[1024];
        sprintf(group_output, "%s/_%d.res.bin", input_dir, group+1);
        sprintf(group_input, "%s/%d", input_dir, group+1);

        int ret = gen_res_group(group_output, group_input);
        if(ret <= 0){
            break;
        }
        //load
        FILE * fpr = fopen(group_output, "rb");
        if(fpr == NULL){
            printf("ERROR: cannot load %s\n", group_output);
            break;
        }
        fread(group_buf + group_start, 1, ret, fpr);
        fclose(fpr);

        //set length
        groupoffset[group+2] = group_start;
        group_start += ret;
    }

    groupoffset[0] = getu32l(0x5f726573);
    groupoffset[1] = getu32l(group);
    for(i=0; i<group; i++){
        groupoffset[i+2] += group*4+8;
        groupoffset[i+2] = getu32l(groupoffset[i+2]);
    }

    fwrite(groupoffset, 1, group*4+8, fpo);
    fwrite(group_buf, 1, group_start, fpo);
    free(group_buf);
    fclose(fpo);
    return 0;
}

static char save_prefix[1024];

void save(char * filename, char * buf, int len)
{
    char fullfn[1024];
    sprintf(fullfn, "%s/%s", save_prefix, filename);
    FILE * fp = fopen(fullfn, "w");
    if(fp == NULL){
        printf("ERROR: open %s for write fail\n", fullfn);
        exit(-1);
    }
    if(fwrite(buf, 1, len, fp) != len){
        printf("ERROR: write %s fail\n", fullfn);
        exit(-1);
    }
    fclose(fp);
    printf("INFO: Write %s success, length:%d\n", fullfn, len);
}


/*************************************************************************/
#define _OFFSET(NAME)  NAME##_OFFSET
#define _SIZE(NAME)    NAME##_SIZE
#define _MAGICNUM(NAME)    NAME##_MAGICNUM
#define SET_PARTITONE(NAME) do{\
                                partinfo[NAME].magic_num = _MAGICNUM(NAME);\
                                partinfo[NAME].length = _SIZE(NAME);\
                                partinfo[NAME].offset = _OFFSET(NAME);\
                             }while(0)
/*********** nand only **********/
static int nandflash_en = 0;
#define ROMNAND_CFG0_USEREAD30  0x01
#define ROMNAND_CFG0_USEDMA     0x02
#define ROMNAND_CFG0_ECC        0x04
#define ROMNAND_CFG0_BCH        0x08
static unsigned char nand_config[16];

/********** sflash only **********/
static int sflash_en = 0;
static unsigned int sflash_config[4];

/********** both nand and sflash ***********/
#define VALUE_INT(x) strtoul(x, NULL, 0)

unsigned int page_size = 0;
unsigned int block_size = 0; //sector_size

typedef enum{
    //System main partition
    CONFIG_HW_HEADER=0,
    CONFIG_BOOTLOADER1,
    CONFIG_BOOTLOADER2,
    CONFIG_FW_AREA_1,
    CONFIG_FW_AREA_2,
    MAX_SYS_MAINPARTITION = CONFIG_FW_AREA_2,
    //DK main partition
    CONFIG_CALIBRATION_AREA,
    CONFIG_USER_CONFIG,
    MAX_DK_MAINPARTITION = CONFIG_USER_CONFIG,
    //Sub partition
    CONFIG_HCONF,
    CONFIG_LDC,
    CONFIG_SYSTEM_ENV,
    CONFIG_WIFI_PARAMETER,
    CONFIG_DKFRAMEWORK_CONF,
    CONFIG_DKFRAMEWORK_CONF_BK,
    CONFIG_DKFRAMEWORK_TEMP,
    CONFIG_DXDOORCAM_CONF,
    CONFIG_DXDOORCAM_CONF_BK,
    CONFIG_DXDOORCAM_TEMP,
    MAX_SUBPARTITION = CONFIG_DXDOORCAM_TEMP,
    MAX_MAINPARTITION = MAX_DK_MAINPARTITION,
    MAX_PARTITION = MAX_SUBPARTITION,
    DKMAINPARTITION_NUM = MAX_DK_MAINPARTITION - MAX_SYS_MAINPARTITION,
}t_partition_id;

typedef struct s_partition
{
    unsigned int system_config;
    unsigned int sccbtable_id2;
    unsigned int quickset_id2;

    //not in header
    int          single_len;
    char *       single;
    int          singletest_len;
    char *       singletest;
    int          singlebackup_len;
    char *       singlebackup;
    int          upgrade_len;
    char *       upgrade;
    unsigned int Max_flashsize;
}t_partition;

typedef struct s_DK_PART_header{
    u32 magic_num;    
    u32 max_len;
    u32 crc;
}t_DK_PART_header; 

typedef struct s_HW_header{
    u32 magic_num;    //0
    u32 qs_id2;//4
    u32 rsvd0;//8
    u32 st_id2;//c
    u32 st_dst_addr;//10
    u32 rsvd1;//14
    u32 bl_src_addr;//18
    u32 bl_dst_addr;//1c
    u32 bl_len;//20
    u32 bl_crc;//24
    u32 sf_cfg[4];//28-37
    u32 bl2_src_addr;//38
    u32 bl2_dst_addr;
    u32 bl2_len;
    u32 bl2_crc;
    u32 sys_cfg;
    u32 fw_addr;
    u32 fw_len;
    u32 user_config_addr;
    u32 user_config_len;
    u32 fw2_addr;
    u32 fw2_len;
    u32 rsvpar01_addr;
    u32 rsvpar01_len;
    u32 cali_addr;
    u32 cali_len;
    u32 rsvpar02_addr;
    u32 rsvpar02_len;
    u32 hdr_crc;
}t_HW_header; 

typedef struct s_DKHW_partinfo{
    u32 magic_num;
    u32 offset;
    u32 len;
}t_DKHW_partinfo; 

typedef struct s_DKHW_header{
    u32 dk_magic_num;
    u32 dk_part_num;
    u32 crc;
    u32 dk_to_start_fwarea;
    t_DKHW_partinfo dk_partinfo[ DKMAINPARTITION_NUM ];
}t_DKHW_header; 

typedef struct s_partition_info
{
    //header
    u32 magic_num;    
    //Set info form /prj/DxDoorCam_XXXX.config
    unsigned int   offset;
    unsigned int   length;
    unsigned int   ram;
    //Packet data
    int            area_len; 
    char *         area_addr;
}t_partition_info;

static t_HW_header      hw_header;
static t_DKHW_header    dkhw_header;
static t_partition      g_part;
static t_partition_info partinfo[]={
//Main part
{ _MAGICNUM(CONFIG_HW_HEADER),           _OFFSET(CONFIG_HW_HEADER),              _SIZE(CONFIG_HW_HEADER),            0,          0, 0},
{ _MAGICNUM(CONFIG_BOOTLOADER1),         _OFFSET(CONFIG_BOOTLOADER1),            _SIZE(CONFIG_BOOTLOADER1),          0x10000000, 0, 0},
{ _MAGICNUM(CONFIG_BOOTLOADER2),         _OFFSET(CONFIG_BOOTLOADER2),            _SIZE(CONFIG_BOOTLOADER2),          0x10000000, 0, 0},
{ _MAGICNUM(CONFIG_FW_AREA_1),           _OFFSET(CONFIG_FW_AREA_1),              _SIZE(CONFIG_FW_AREA_1),            0,          0, 0},
{ _MAGICNUM(CONFIG_FW_AREA_2),           _OFFSET(CONFIG_FW_AREA_2),              _SIZE(CONFIG_FW_AREA_2),            0,          0, 0},
{ _MAGICNUM(CONFIG_CALIBRATION_AREA),    _OFFSET(CONFIG_CALIBRATION_AREA),       _SIZE(CONFIG_CALIBRATION_AREA),     0,          0, 0},
{ _MAGICNUM(CONFIG_USER_CONFIG),         _OFFSET(CONFIG_USER_CONFIG),            _SIZE(CONFIG_USER_CONFIG),          0,          0, 0},
//Sub part
{ 0,                                     _OFFSET(CONFIG_HCONF),                  _SIZE(CONFIG_HCONF),                0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_LDC),                    _SIZE(CONFIG_LDC),                  0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_SYSTEM_ENV),             _SIZE(CONFIG_SYSTEM_ENV),           0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_WIFI_PARAMETER),         _SIZE(CONFIG_WIFI_PARAMETER),       0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DKFRAMEWORK_CONF),       _SIZE(CONFIG_DKFRAMEWORK_CONF),     0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DKFRAMEWORK_CONF_BK),    _SIZE(CONFIG_DKFRAMEWORK_CONF),     0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DKFRAMEWORK_TEMP),       _SIZE(CONFIG_DKFRAMEWORK_CONF),     0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DXDOORCAM_CONF),         _SIZE(CONFIG_DXDOORCAM_CONF),       0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DXDOORCAM_CONF_BK),      _SIZE(CONFIG_DXDOORCAM_CONF),       0,          0, 0},
{ 0,                                     _OFFSET(CONFIG_DXDOORCAM_TEMP),         _SIZE(CONFIG_DXDOORCAM_CONF),       0,          0, 0},
{ 0, 0, 0, 0, 0, 0},
};


static void W16(char * buf, unsigned short n)
{
    buf[0] = (n >> 0) & 0xff;
    buf[1] = (n >> 8) & 0xff;
}
static void W32(char * buf, unsigned int n)
{
    buf[0] = (n >> 0) & 0xff;
    buf[1] = (n >> 8) & 0xff;
    buf[2] = (n >> 16) & 0xff;
    buf[3] = (n >> 24) & 0xff;
}
/********* file ********/
typedef struct s_file
{
    int item_id2;
    char item_mcu_bl[FILENAME_MAX];
    char item_mcu_fw[FILENAME_MAX];
    char item_name[FILENAME_MAX];
    char item_filename[FILENAME_MAX];

    char item_realfilename[FILENAME_MAX];
}t_file;

static t_file g_file;

/* real file */
#define MAXFILECNT 0x20

typedef struct s_fileitem
{
    char filename[FILENAME_MAX];
    unsigned int addr;
    unsigned int len;
    char * data;
}t_fileitem;

static int maxid2;
static t_fileitem fileitem[MAXFILECNT + 1]; //MAXFILECNT for para
static t_fileitem envfile;
static t_fileitem fastapp_file;

/*********** others ************/
static char prj[256];
static int version=0;

static char * find_filename(char * relative_fname)
{
    static char abs_filename[FILENAME_MAX];
    abs_filename[0] = '\0';

    if(localfile_only){
        return relative_fname;
    }

    char cmdbuf[FILENAME_MAX*2+256];
    sprintf(cmdbuf, "./combine_find.sh %s %s", prj, relative_fname);
    if(system(cmdbuf) < 0){
        printf("%s not found\n", relative_fname);
        return NULL;
    }
    sprintf(cmdbuf, "../build/%s/.find_fname.txt", prj);
    FILE * fp = fopen(cmdbuf, "r");
    if(fp == NULL){
        printf("%s not found1\n", relative_fname);
        return NULL;
    }
    fgets(abs_filename, FILENAME_MAX-1, fp);
    fclose(fp);

    if(abs_filename[0] != '\0'){
        //printf("INFO: find filename %s -> %s\n", relative_fname, abs_filename);
        return abs_filename;
    }
    printf("%s not found2\n", relative_fname);
    return NULL;
}

static int parse_file(void)
{
    if(g_file.item_filename[0] == '\0'){
        return 0;
    }
    if(!strcasecmp(g_file.item_name, "sccb_table")){
        if(localfile_only){
            strcpy(g_file.item_realfilename, g_file.item_filename);
        }else{
            char cmdbuf[FILENAME_MAX*2+256];
            char * filename = find_filename(g_file.item_filename);
            if(filename == NULL){
                return -1;
            }
            sprintf(g_file.item_realfilename, "../build/%s/sccb_table.bin", prj);
            //make sccbtable_gen -f Makefile.tools > /dev/null || exit 1
            if(fastapp_file.filename[0] == '\0'){
                printf("ERROR: 'Fast_App_config' not defined in prj_cfg.txt\n");
                return -1;
            }

            sprintf(cmdbuf, "./sccbtable_gen.sh %s %s %s\n", filename, fastapp_file.filename, g_file.item_realfilename);
            if(system(cmdbuf) < 0){
                return -1;
            }
        }
        g_part.sccbtable_id2 = g_file.item_id2;
    }else if(!strcasecmp(g_file.item_name, "quickset")){
        if(localfile_only){
            strcpy(g_file.item_realfilename, g_file.item_filename);
        }else{
            char cmdbuf[FILENAME_MAX*2+256];
            char * filename = find_filename(g_file.item_filename);
            if(filename == NULL){
                return -1;
            }
            sprintf(g_file.item_realfilename, "../build/%s/quickset.bin", prj);
            sprintf(cmdbuf, "./quickset_gen.sh %s %s\n", filename, g_file.item_realfilename);
            if(system(cmdbuf) < 0){
                return -1;
            }
        }
        g_part.quickset_id2 = g_file.item_id2;
    }else if(localfile_only){
        //in linux case, only handle sccb_table and quickset above.
        strcpy(g_file.item_realfilename, g_file.item_filename);
    }else if(!strcasecmp(g_file.item_name, "mpu.bin")){
        sprintf(g_file.item_realfilename, "../mpu/build/%s/%s.bin", prj, g_file.item_filename);
    }else if(!strcasecmp(g_file.item_name, "MSP430V2")){
        /* fw */
        if(g_file.item_mcu_fw[0] == '\0'){
            return -1;
        }
        char * filename = find_filename(g_file.item_mcu_fw);
        if(filename == NULL){
            return -1;
        }
        strcpy(g_file.item_mcu_fw, filename);

        /* bl */
        if(g_file.item_mcu_bl[0] == '\0'){
            return -1;
        }
        filename = find_filename(g_file.item_mcu_bl);
        if(filename == NULL){
            return -1;
        }
        strcpy(g_file.item_mcu_bl, filename);

        /* combine */
        char cmdbuf[FILENAME_MAX*2+256];
        sprintf(g_file.item_realfilename, "../build/%s/mcu.bin", prj);
        sprintf(cmdbuf, "./combine_msp430.sh %s %s %s\n", g_file.item_mcu_bl, g_file.item_mcu_fw, g_file.item_realfilename);
        if(system(cmdbuf) < 0){
            return -1;
        }
    }else if(!strcasecmp(g_file.item_name, "resources")){
        char *filename = find_filename(g_file.item_filename);
        if(filename == NULL){
            return -1;
        }
        sprintf(g_file.item_realfilename, "../build/%s/res.bin", prj);
        if(gen_res(g_file.item_realfilename, filename) < 0){
            printf("ERROR: gen resource from %s to %s failed\n", filename, g_file.item_realfilename);
            return -1;
        }
        res_id2 = g_file.item_id2;
    }else{
        char * filename = find_filename(g_file.item_filename);
        if(filename == NULL){
            return -1;
        }
        strcpy(g_file.item_realfilename, filename);
    }

    /* add to fileitem */
    if(g_file.item_id2 >= MAXFILECNT){
        printf("ERROR: please increase MAXFILECNT in combine.c\n");
        return -1;
    }
    if(g_file.item_id2 > maxid2){
        maxid2 = g_file.item_id2;
    }

    strcpy(fileitem[g_file.item_id2].filename, g_file.item_realfilename);

    printf("INFO: FW add bin %d : %s\n", g_file.item_id2, g_file.item_realfilename);

    /* clear */
    memset(&g_file, 0, sizeof(g_file));

    return 0;
}

#define ALIGN_BLOCK(x) (((x) + block_size - 1) & (~(block_size -1)))

static int load_file(t_fileitem * item, int align16)
{
    struct stat st;
    if(stat(item->filename, &st) != 0){
        printf("ERROR: stat file %s error\n", item->filename);
        exit(-1);
    }
    if(align16){
        item->len = ((st.st_size+15)>>4)<<4;
    }else{
        item->len = st.st_size;
    }

    item->data = malloc(st.st_size);
    if(item->data == NULL){
        printf("ERROR: malloc %ld fail\n", st.st_size);
        exit(-1);
    }
    FILE * fp;
    fp = fopen(item->filename, "r");
    if(fp == NULL){
        printf("ERROR: open file %s error\n", item->filename);
        exit(-1);
    }
    if(fread(item->data, 1, st.st_size, fp) != st.st_size){
        printf("ERROR: read file %s error\n", item->filename);
        exit(-1);
    }
    fclose(fp);
    return 0;
}

static unsigned int get_subpartition_size( int mainpartid )
{
    unsigned int totalsize, mainpartstart, mainpartend;
    int count;

    totalsize=0;
    mainpartstart = partinfo[mainpartid].offset;
    mainpartend = partinfo[mainpartid].offset + partinfo[mainpartid].length - 1;

    for ( count = 0 ; count <= MAX_PARTITION ; count++ )
    {
        if(( count > MAX_MAINPARTITION ) &&
           ( partinfo[count].offset >= mainpartstart ) &&
           (( partinfo[count].offset + partinfo[count].length - 1) <= mainpartend ))
        {
            totalsize = totalsize + partinfo[count].length;
        }   
    }
    return totalsize;
}

static int check_partition_overlay( void )
{
    unsigned int totalsize, partstart, subpartsize, partend, countpartstart, countpartend;
    int nowtest, count;

    totalsize = 0;
    for ( nowtest = 0 ; nowtest <= MAX_PARTITION ; nowtest++ )
    {
        //Check partigion info set
         if (( partinfo[nowtest].offset == 0 ) &&
             ( partinfo[nowtest].length == 0))
         {
            printf("ERROR: Tatol Partitions is [%d], but only set [%d] Partitions\n",
                    ( MAX_PARTITION + 1 ) ,nowtest);
            return 0;
         }
        
        //Check main partition
        if ( nowtest <= MAX_MAINPARTITION )
        {
            partstart = partinfo[nowtest].offset;
            partend = partstart + partinfo[nowtest].length;
            totalsize = totalsize + partinfo[nowtest].length;
            
            if( (partstart != partend) )
            {
                partend--;  

                for ( count = 0 ; count <= MAX_PARTITION ; count++ )
                {   
                    if ( ( count != nowtest ) && ( count <= MAX_MAINPARTITION) )
                    {
                        countpartstart = partinfo[count].offset;
                        if ( partinfo[count].length != 0)
                        {
                            countpartend = countpartstart + partinfo[count].length - 1;
    
                            if ( ( partstart >= countpartstart ) && ( partstart <= countpartend) ){
                                printf("ERROR: Partition[%d(0x%x)] start in Partition[%d(0x%x~0x%x)]\n", 
                                       nowtest, partstart, count, countpartstart, countpartend);
                                return 0;
                            }else if ( ( partend >= countpartstart ) && ( partend <= countpartend) ){
                                printf("ERROR: Partition[%d(0x%x)] end in Partition[%d(0x%x~0x%x)]\n", 
                                      nowtest, partstart, count, countpartstart, countpartend);
                                return 0;
                            }
                        }
                    }
                }
            }

            //Check subpartition in main partition set
            subpartsize = get_subpartition_size( nowtest );
            if ( subpartsize > partinfo[nowtest].length )
            {
                printf("ERROR: Main Partition[%d] size too small(0x%x), because sub Partition size[0x%x]\n", 
                        nowtest, partinfo[nowtest].length, subpartsize);
                return 0;
            }
        }
    }

    //Check total partitions < flash size
    if ( totalsize > g_part.Max_flashsize )
    {
        printf( "ERROR:Total partition size(%d) > Flash size(%d)\n", totalsize, g_part.Max_flashsize );
        return 0;
    }
    return 1;
}

static int generate_bootloader(int partid, int bootloaderID)
{
    partinfo[partid].area_len  = fileitem[bootloaderID].len;
    partinfo[partid].area_addr = fileitem[bootloaderID].data;
    if(partinfo[partid].area_len){
        if(partinfo[partid].area_len > partinfo[partid].length){
            printf("WARNING: increase Bl2Size from %d to %d\n", partinfo[partid].length, partinfo[partid].area_len);
            partinfo[partid].length = partinfo[partid].area_len;
        }

        if(! localfile_only){
            unsigned char * blbase8 = (unsigned char *)partinfo[partid].area_addr;
            unsigned int bl_baseaddr = blbase8[0x20] | (blbase8[0x21] << 8) | (blbase8[0x22] << 16) | (blbase8[0x23] << 24);
            if(bl_baseaddr != 0){
                partinfo[partid].ram = bl_baseaddr;
            }
        }
    }
}

static int generate_partition( int partid )
{
    switch( partid ){

        case CONFIG_HW_HEADER:
            if(( partinfo[CONFIG_BOOTLOADER1].area_len <= 0 ) ||
               ( partinfo[CONFIG_FW_AREA_1].area_len <= 0 ))
            {
                printf("ERROR:No Bootloader or FW\n");
                exit(-1);
            }

            //Process HW Header CONFIG_HW_HEADER
            hw_header.magic_num = partinfo[CONFIG_HW_HEADER].magic_num;
            hw_header.qs_id2 = g_part.quickset_id2; 
            hw_header.st_id2 = g_part.sccbtable_id2;
            hw_header.bl_src_addr = partinfo[CONFIG_BOOTLOADER1].offset;
            hw_header.bl_dst_addr = partinfo[CONFIG_BOOTLOADER1].ram;
            hw_header.bl_len = partinfo[CONFIG_BOOTLOADER1].area_len;
            hw_header.bl_crc = crc16_ccitt(partinfo[CONFIG_BOOTLOADER1].area_addr, partinfo[CONFIG_BOOTLOADER1].area_len);
            memcpy( hw_header.sf_cfg, sflash_config, 16 );
            if(partinfo[CONFIG_BOOTLOADER2].area_len){
                hw_header.bl2_src_addr = partinfo[CONFIG_BOOTLOADER2].offset;
                hw_header.bl2_dst_addr = partinfo[CONFIG_BOOTLOADER2].ram;
                hw_header.bl2_len = partinfo[CONFIG_BOOTLOADER2].area_len;
                hw_header.bl2_crc = crc16_ccitt(partinfo[CONFIG_BOOTLOADER2].area_addr, partinfo[CONFIG_BOOTLOADER2].area_len);
            }
            hw_header.fw_addr = partinfo[CONFIG_FW_AREA_1].offset;
            hw_header.fw_len = partinfo[CONFIG_FW_AREA_1].length;
            hw_header.fw2_addr = partinfo[CONFIG_FW_AREA_2].offset;
            hw_header.fw2_len = partinfo[CONFIG_FW_AREA_2].length;
            hw_header.hdr_crc = crc16_ccitt(&hw_header, 0x7c);

            dkhw_header.dk_magic_num = partinfo[CONFIG_HW_HEADER].magic_num;
            dkhw_header.dk_part_num = DKMAINPARTITION_NUM;
            dkhw_header.dk_to_start_fwarea = 0;
            dkhw_header.crc = crc16_ccitt( dkhw_header.dk_partinfo, ( sizeof(t_DKHW_partinfo) * DKMAINPARTITION_NUM ));
            
            partinfo[partid].area_addr = (char *)malloc(partinfo[partid].length);
            if(partinfo[partid].area_addr == NULL){
                printf( "ERROR: malloc %d fail\n", partinfo[partid].length );
                exit(-1);
            }
            memset( partinfo[partid].area_addr, 0, partinfo[partid].length );
            memcpy( partinfo[partid].area_addr, &hw_header, sizeof(hw_header));
            memcpy( ( partinfo[partid].area_addr + sizeof(hw_header) ), &dkhw_header, sizeof(dkhw_header));
            partinfo[partid].area_len = sizeof(hw_header) + sizeof(dkhw_header);
            break;
        
        case CONFIG_BOOTLOADER1:
            generate_bootloader( partid, 0 );
            break;
            
        case CONFIG_BOOTLOADER2:
            generate_bootloader( partid, 1 );
            break;

        case CONFIG_FW_AREA_1:
            partinfo[partid].area_addr = (char *)malloc(partinfo[partid].length);
            if(partinfo[partid].area_addr == NULL){
                printf("ERROR: malloc %d fail\n", partinfo[partid].length);
                exit(-1);
            }
    
            //generate FW area
            unsigned short fw_crc;
            int i;
            
            memset(partinfo[partid].area_addr, 0, partinfo[partid].length);
            partinfo[partid].area_len = 0x30 + (maxid2 - 1)*8;
            
            for(i=2; i<=maxid2; i++){
                if(fileitem[i].filename[0] == '\0'){
                    continue;
                }
                if(fileitem[i].data == NULL){
                    continue;
                }
        
                //FIXME: put this before 'memcpy', otherwise, Segment Fault may happen.
                if(partinfo[partid].area_len + fileitem[i].len > partinfo[partid].length){
                    printf("ERROR: fw_area length exceed (%d > %d by id %d)\n", partinfo[partid].area_len + fileitem[i].len, partinfo[partid].length, i);
                    exit(-1);
                }
        
                fileitem[i].addr = partinfo[partid].area_len;
                memcpy(partinfo[partid].area_addr + partinfo[partid].area_len, fileitem[i].data, fileitem[i].len);
                partinfo[partid].area_len += fileitem[i].len;
                partinfo[partid].area_len = (partinfo[partid].area_len + 3) & ~3;
        
                W32(partinfo[partid].area_addr + 0x10 + (i-2)*8, fileitem[i].addr);
                W32(partinfo[partid].area_addr + 0x14 + (i-2)*8, fileitem[i].len);
            }
        
            //FW area header
            W32(partinfo[partid].area_addr + 0, partinfo[partid].magic_num);
            W32(partinfo[partid].area_addr + 4, version);
            W32(partinfo[partid].area_addr + 8, partinfo[partid].area_len);
            W32(partinfo[partid].area_addr + 0xc, maxid2 - 1);
            W32(partinfo[partid].area_addr + 0x10 + (maxid2 - 1)*8, res_id2);
            partinfo[partid].area_addr[0x2e + (maxid2 - 1)*8] = 0x56;
            partinfo[partid].area_addr[0x2f + (maxid2 - 1)*8] = 0x4f;
            fw_crc = crc16_ccitt(partinfo[CONFIG_FW_AREA_1].area_addr, 0x2c + (maxid2 - 1)*8);
            partinfo[partid].area_addr[0x2c + (maxid2 - 1)*8] = (fw_crc >> 0) & 0xff;
            partinfo[partid].area_addr[0x2d + (maxid2 - 1)*8] = (fw_crc >> 8) & 0xff;
            break;
            
        case CONFIG_FW_AREA_2:
            if ( ( partinfo[CONFIG_FW_AREA_1].area_addr != NULL ) &&
                 ( partinfo[CONFIG_FW_AREA_1].length == partinfo[partid].length))
            {
                partinfo[partid].area_addr = (char *)malloc(partinfo[partid].length);
                if(partinfo[partid].area_addr == NULL){
                    printf("ERROR: malloc %d fail\n", partinfo[partid].length);
                    exit(-1);
                }
                memset( partinfo[partid].area_addr, 0, partinfo[partid].length);
                memcpy( partinfo[partid].area_addr, partinfo[CONFIG_FW_AREA_1].area_addr, partinfo[CONFIG_FW_AREA_1].area_len);
                partinfo[partid].area_len = partinfo[CONFIG_FW_AREA_1].area_len;
            }
            else
            {
                printf("ERROR: Create FW area 1, First!\n");
                exit(-1);
            }
            break;
            
        default: //No date to wirte partition
            if (( partid > MAX_SYS_MAINPARTITION ) &&
                ( partid <= MAX_MAINPARTITION ))
            {
                int dkpartid;
                dkpartid = partid - ( MAX_SYS_MAINPARTITION + 1 );

                //Main partinfo add to hw header
                dkhw_header.dk_partinfo[dkpartid].magic_num = partinfo[partid].magic_num;
                dkhw_header.dk_partinfo[dkpartid].offset= partinfo[partid].offset;
                dkhw_header.dk_partinfo[dkpartid].len = partinfo[partid].length;

                //Main partition to add header    
                t_DK_PART_header dkpart_header;
                dkpart_header.magic_num = partinfo[partid].magic_num;;
                dkpart_header.max_len = partinfo[partid].offset;
                dkpart_header.crc = crc16_ccitt(&dkpart_header, 8);
                
                partinfo[partid].area_addr = (char *)malloc(CONFIG_PARTITION_HEADER_SIZE);
                if( partinfo[partid].area_addr == NULL)
                {
                    printf("ERROR: malloc %d fail\n", CONFIG_PARTITION_HEADER_SIZE);
                    exit(-1);
                }
                memset( partinfo[partid].area_addr, 0, CONFIG_PARTITION_HEADER_SIZE);
                partinfo[partid].area_len=sizeof(dkpart_header);
                memcpy(partinfo[partid].area_addr, &dkpart_header, sizeof(dkpart_header));
            }
            else
            {
                partinfo[partid].area_addr=NULL;
                partinfo[partid].area_len=0;
            }
            break;
            
    }
    return 1;
}

static void combine(void)
{
    int count, i, single_maxsize;
    unsigned short crc;

    memset( &hw_header, 0x0, sizeof(hw_header) );

    if( check_partition_overlay() != 1 )
    {
        printf( "ERROR: Partition overlay\n" );
        exit( -1 );
    }

    if( fileitem[0].filename[0] == '\0' ){
        printf( "ERROR: bootloader not exist(should have arg with id2=0)\n" );
        exit( -1 );
    }

    //load file
    for( count = 0; count <= MAXFILECNT; count++ ){
        if( fileitem[count].filename[0] == '\0' ){
            continue;
        }
        load_file( &fileitem[count], 1 );
    }

    //generate all partition
    for ( count = 1; count <= MAX_PARTITION; count++)
    {
        generate_partition( count );
    }
    //HW Header MUST last generate, because it need other partition info. 
    generate_partition ( CONFIG_HW_HEADER );
  
    //generate single.bin
    single_maxsize = partinfo[MAX_PARTITION].offset + partinfo[MAX_PARTITION].length;
    if( single_maxsize < 32*1024*1024 ){
        single_maxsize = 32*1024*1024;
    }
    g_part.single = (char *)malloc( single_maxsize );
    if( g_part.single == NULL ){
        printf( "ERROR: malloc %d fail\n", single_maxsize );
        exit( -1 );
    }
    memset(g_part.single, 0, single_maxsize);

    for( count = 0; count <= MAX_PARTITION; count++ )
    {
        if(partinfo[count].area_len){
            memcpy(g_part.single + partinfo[count].offset, partinfo[count].area_addr, partinfo[count].area_len);
            printf("WRITE PART_ID[%d(0x0%x~0x0%x)]/AREA_LEN[%d]\n",
                count,partinfo[count].offset,
                ( partinfo[count].offset + partinfo[count].length - 1),
                partinfo[count].area_len);
        }
    }
    g_part.single_len = partinfo[MAX_PARTITION].offset + partinfo[MAX_PARTITION].area_len;
    save("SINGLE.bin", g_part.single, g_part.single_len);
 
    //generate single_test.bin
    single_maxsize = partinfo[CONFIG_FW_AREA_1].offset + partinfo[CONFIG_FW_AREA_1].length;
    if(single_maxsize < 32*1024*1024){
        single_maxsize = 32*1024*1024;
    }
    g_part.singletest = (char *)malloc(single_maxsize);
    if(g_part.singletest == NULL){
        printf("ERROR: malloc %d fail\n", single_maxsize);
        exit(-1);
    }
    memset(g_part.singletest, 0, single_maxsize);
        for( count = 0; count <= CONFIG_FW_AREA_1; count++ )
    {
        if(partinfo[count].area_len){
            memcpy(g_part.singletest + partinfo[count].offset, partinfo[count].area_addr, partinfo[count].area_len);
            //printf("WRITE PART_ID[%d(0x0%x~0x0%x)]-AREA_len[%d]\n",
            //    count,partinfo[count].offset,
            //    ( partinfo[count].offset + partinfo[count].length - 1),
            //    partinfo[count].area_len);
        }
    }
    g_part.singletest_len = partinfo[CONFIG_FW_AREA_1].offset + partinfo[CONFIG_FW_AREA_1].area_len;
    save("SINGLE_TEST.bin", g_part.singletest, g_part.singletest_len);

    //generate upgrade_fw.bin
    g_part.upgrade = (char *)malloc(partinfo[CONFIG_USER_CONFIG].offset + partinfo[CONFIG_USER_CONFIG].length);
    if(g_part.upgrade == NULL){
        printf("ERROR: malloc %d fail\n", partinfo[CONFIG_USER_CONFIG].offset + partinfo[CONFIG_USER_CONFIG].length);
        exit(-1);
    }

    memset(g_part.upgrade, 0, 0x40 + partinfo[CONFIG_FW_AREA_1].area_len + partinfo[CONFIG_USER_CONFIG].area_len);
    g_part.upgrade_len = 0x40 + partinfo[CONFIG_FW_AREA_1].area_len;
    memcpy(g_part.upgrade + 0x40, partinfo[CONFIG_FW_AREA_1].area_addr, partinfo[CONFIG_FW_AREA_1].area_len);
    W32(g_part.upgrade + 0x0, 0x4f565457);
    W32(g_part.upgrade + 0x4, version);
    W32(g_part.upgrade + 0x8, 0x40);
    W32(g_part.upgrade + 0xc, partinfo[CONFIG_FW_AREA_1].area_len);
    crc = crc16_ccitt(g_part.upgrade + 0x40, partinfo[CONFIG_FW_AREA_1].area_len);
    g_part.upgrade[0x30] = (crc >> 8) & 0xff;
    g_part.upgrade[0x31] = (crc >> 0) & 0xff;
    save("UPGRADE_FW.bin", g_part.upgrade, g_part.upgrade_len );

    //For OTA TEST
    g_part.singlebackup = (char *)malloc( g_part.single_len + g_part.upgrade_len );
    if(g_part.singlebackup == NULL){
        printf("ERROR: malloc %d fail\n", g_part.single_len + g_part.upgrade_len );
        exit(-1);
    }
    g_part.singlebackup_len = g_part.single_len + g_part.upgrade_len ;
    memset( g_part.singlebackup, 0x0, g_part.singlebackup_len );
    memcpy( g_part.singlebackup, g_part.single, g_part.single_len);
    memcpy((g_part.singlebackup + g_part.single_len ), g_part.upgrade, g_part.upgrade_len);

    W32( g_part.singlebackup + 0x74, g_part.single_len );//OTA offset
    W32( g_part.singlebackup + 0x78, g_part.upgrade_len);//OTA len
    crc = crc16_ccitt( g_part.singlebackup, 0x7c);
    W32( g_part.singlebackup + 0x7c, crc); //header CRC
    save("SINGLE_BACKUP_OTAFW.bin", g_part.singlebackup, g_part.singlebackup_len);

    //generate upgrade_fw_para.bin
    g_part.upgrade_len = 0x40 + partinfo[CONFIG_FW_AREA_1].area_len + partinfo[CONFIG_USER_CONFIG].area_len;
    if( partinfo[CONFIG_USER_CONFIG].area_len ){
        memcpy(g_part.upgrade + 0x40 + partinfo[CONFIG_FW_AREA_1].area_len, partinfo[CONFIG_USER_CONFIG].area_addr, partinfo[CONFIG_USER_CONFIG].area_len);
    }
    W32(g_part.upgrade + 0x10, 0x40 + partinfo[CONFIG_FW_AREA_1].area_len);
    W32(g_part.upgrade + 0x14, partinfo[CONFIG_USER_CONFIG].area_len);
    crc = crc16_ccitt(g_part.upgrade + 0x40, partinfo[CONFIG_FW_AREA_1].area_len + partinfo[CONFIG_USER_CONFIG].area_len);
    g_part.upgrade[0x30] = (crc >> 8) & 0xff;
    g_part.upgrade[0x31] = (crc >> 0) & 0xff;
    save("UPGRADE_FW_USERCONFIG.bin", g_part.upgrade, g_part.upgrade_len);

    for( count = 0; count <= MAX_PARTITION; count++ )
    {
        if( partinfo[count].area_len ){
            free( partinfo[count].area_addr );
            partinfo[count].area_len=0;
        }
    }

    free( g_part.single );
    free( g_part.singlebackup );
    free( g_part.singletest );
    free( g_part.upgrade );

}

static void help(char * argv0)
{
    if(localfile_only){
        printf("ERROR: usage: %s [ prj_cfg.txt ]\n", argv0);
    }else{
        printf("ERROR: usage: %s PRJ prj_cfg.txt\n", argv0);
    }
    exit(-1);
}

int main(int argc, char ** argv)
{
    if(strstr(argv[0], "linux")){
        localfile_only = 1;
    }
    char * prj_filename = NULL;

    if(localfile_only){
        if(argc == 2){
            prj_filename = argv[1];
        }else{
            prj_filename = "prj_cfg.txt";
        }
        strcpy(save_prefix, ".");
    }else{
        if(argc < 3){
            help(argv[0]);
        }
        prj_filename = argv[2];
        strcpy(prj, argv[1]);
        sprintf(save_prefix, "../build/%s", prj);
    }

    int sf_sector_size = 0;
    unsigned int nand_page_size = 0;
    unsigned int nand_block_size = 0;

    FILE * fp = fopen(prj_filename, "r");
    if(fp == NULL){
        printf("failed to open %s\n", prj_filename);
        return -1;
    }

    FILE * sccb_cfg_fp = fopen("sccb_cfg.tmp", "w");
    if(sccb_cfg_fp == NULL){
        printf("cannot write sccb_cfg.tmp file\n");
        return -1;
    }

    maxid2 = 0;
    memset(&fileitem[0], 0, sizeof(fileitem));
    memset(&envfile, 0, sizeof(envfile));
    memset(nand_config, 0, 16);
    memset(sflash_config, 0, 16);
    memset(&g_part, 0, sizeof(g_part));

    char buf[256];
    while(fgets(buf, 255, fp)){
        char * c;
        if((c=strchr(buf, '\n')) != NULL) *c = '\0';
        if((c=strchr(buf, '\r')) != NULL) *c = '\0';
        if((c=strchr(buf, ';')) != NULL) *c = '\0';
        if((c=strchr(buf, '#')) != NULL) *c = '\0';
        if((c=strchr(buf, '/')) != NULL) *c = '\0';

        if(buf[0] == '\0'){
            continue;
        }

        c = strchr(buf, '=');
        if(c){
            char * name = buf;
            char * value = c+1;
            *c = '\0';
            //remove prefix ' '
#define REMOVE_CHAR(str, ch) do{ while(*str == ch) str ++; while(str[strlen(str)-1] == ch) str[strlen(str)-1]='\0';  }while(0)

            REMOVE_CHAR(name, ' ');
            REMOVE_CHAR(value, ' ');
            REMOVE_CHAR(name, '\t');
            REMOVE_CHAR(value, '\t');

            if(!strcasecmp(name, "HasNand")){
                nandflash_en = VALUE_INT(value);
            }else if(!strcasecmp(name, "HasSFlash")){
                sflash_en = VALUE_INT(value);
            }else if(!strcasecmp(name, "Nand_OOBCMD")){
                nand_config[0] = VALUE_INT(value);
            }else if(!strcasecmp(name, "Nand_ColSize")){
                nand_config[1] = VALUE_INT(value);
            }else if(!strcasecmp(name, "Nand_RowSize")){
                nand_config[2] = VALUE_INT(value);
            }else if(!strcasecmp(name, "Nand_Read30")){
                nand_config[3] |= ((VALUE_INT(value) > 0) ? ROMNAND_CFG0_USEREAD30 : 0);
            }else if(!strcasecmp(name, "Nand_useDMA")){
                nand_config[3] |= ((VALUE_INT(value) > 0) ? ROMNAND_CFG0_USEDMA : 0);
            }else if(!strcasecmp(name, "Nand_PageSize")){
                nand_page_size = VALUE_INT(value);
                W16(nand_config + 8, nand_page_size);
            }else if(!strcasecmp(name, "Nand_BlockSize")){
                nand_block_size = VALUE_INT(value);
                W32(nand_config + 12, nand_block_size);
            }else if(!strcasecmp(name, "SFlash_SectorSize")){
                sf_sector_size = VALUE_INT(value);
            }else if(!strcasecmp(name, "SFlash_DMA")){
                if(VALUE_INT(value) != 0){
                    sflash_config[0] |= 0x1;
                }
            }else if(!strcasecmp(name, "SFlash_Mode")){
                if(VALUE_INT(value) != 0){
                    sflash_config[0] |= ((VALUE_INT(value)&0x7)<<1);
                }
            }else if(!strcasecmp(name, "SFlash_Dummy_EN")){
                if(VALUE_INT(value) != 0){
                    sflash_config[0] |= 0x10;
                }
            }else if(!strcasecmp(name, "SFlash_Dummy")){
                if(VALUE_INT(value) != 0){
                    sflash_config[0] |= ((VALUE_INT(value)&0xf)<<5);
                }
            }else if(!strcasecmp(name, "SFlash_Quadcheck_EN")){
                if(VALUE_INT(value) != 0){
                    sflash_config[0] |= ((VALUE_INT(value)&0xf)<<9);
                }
            }else if(!strcasecmp(name, "SFlash_config1")){
                if(VALUE_INT(value) != 0){
                    sflash_config[1] |= VALUE_INT(value);
                }
            }else if(!strcasecmp(name, "SFlash_config2")){
                if(VALUE_INT(value) != 0){
                    sflash_config[2] |= VALUE_INT(value);
                }
            }else if(!strcasecmp(name, "SFlash_config3")){
                if(VALUE_INT(value) != 0){
                    sflash_config[3] |= VALUE_INT(value);
                }
            }else if(!strcasecmp(name, "SFlash_Size")){
                if(VALUE_INT(value) != 0){
                    g_part.Max_flashsize = VALUE_INT(value);
                }
            }else if(!strcasecmp(name, "File_id2")){
                g_file.item_id2 = VALUE_INT(value);
            }else if(!strcasecmp(name, "File_MCU_BL")){
                strcpy(g_file.item_mcu_bl, value);
            }else if(!strcasecmp(name, "File_MCU_FW")){
                strcpy(g_file.item_mcu_fw, value);
            }else if(!strcasecmp(name, "File_name")){
                strcpy(g_file.item_name, value);
            }else if(!strcasecmp(name, "File_filename")){
                strcpy(g_file.item_filename, value);
            }else if(!strcasecmp(name, "ENV_filename")){
                char * filename = find_filename(value);
                if(filename == NULL){
                    return -1;
                }
                strcpy(envfile.filename, filename);
            }else if(!strcasecmp(name, "SCCB_CFG_CLK")){
                fprintf(sccb_cfg_fp, "SCCB_CFG_CLK=%s\n", value);
            }else if(!strcasecmp(name, "SCCB_CFG_SYSCLK")){
                fprintf(sccb_cfg_fp, "SCCB_CFG_SYSCLK=%s\n", value);
            }else if(!strcasecmp(name, "Fast_App_config")){
                char fastbootapp[512];
                sprintf(fastbootapp, "%s/.autoconf.h", value);
                char * filename = find_filename(fastbootapp);
                if(filename != NULL){
                    sprintf(fastapp_file.filename, "%s", filename);
                }
            }
        }else{
            if(!strncasecmp(buf, "[FILE", 5)){
                if(!strcasecmp(g_file.item_name, "sccb_table")){
                    fclose(sccb_cfg_fp);
                    sccb_cfg_fp = NULL;
                }

                if(parse_file() < 0){
                    return -1;
                }
            }
        }
    }
    fclose(fp);
//    remove("sccb_cfg.tmp");
    if(sccb_cfg_fp){
        fclose(sccb_cfg_fp);
    }

    if(parse_file() < 0){
        return -1;
    }

    if(nandflash_en){
        block_size = nand_block_size;
        page_size = nand_page_size;
    }else{
        block_size = sf_sector_size;
    }

    combine();

    remove("sccb_cfg.tmp");

    return 0;
}
