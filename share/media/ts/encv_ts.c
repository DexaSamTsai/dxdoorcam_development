#include "includes.h"

#define GET_FRAME_TIMEOUT 100

#define tsbuf_addr (ts_cfg->tsbuf + ( ((ts_cfg->tsbuf_start+ts_cfg->tsbuf_num)>=ts_cfg->tspkt_cnt)?(ts_cfg->tsbuf_start+ts_cfg->tsbuf_num-ts_cfg->tspkt_cnt):(ts_cfg->tsbuf_start+ts_cfg->tsbuf_num) ) * TS_PACKET_SIZE)


static u32 crc_table[257] = {
        0x00000000, 0xB71DC104, 0x6E3B8209, 0xD926430D, 0xDC760413, 0x6B6BC517,
        0xB24D861A, 0x0550471E, 0xB8ED0826, 0x0FF0C922, 0xD6D68A2F, 0x61CB4B2B,
        0x649B0C35, 0xD386CD31, 0x0AA08E3C, 0xBDBD4F38, 0x70DB114C, 0xC7C6D048,
        0x1EE09345, 0xA9FD5241, 0xACAD155F, 0x1BB0D45B, 0xC2969756, 0x758B5652,
        0xC836196A, 0x7F2BD86E, 0xA60D9B63, 0x11105A67, 0x14401D79, 0xA35DDC7D,
        0x7A7B9F70, 0xCD665E74, 0xE0B62398, 0x57ABE29C, 0x8E8DA191, 0x39906095,
        0x3CC0278B, 0x8BDDE68F, 0x52FBA582, 0xE5E66486, 0x585B2BBE, 0xEF46EABA,
        0x3660A9B7, 0x817D68B3, 0x842D2FAD, 0x3330EEA9, 0xEA16ADA4, 0x5D0B6CA0,
        0x906D32D4, 0x2770F3D0, 0xFE56B0DD, 0x494B71D9, 0x4C1B36C7, 0xFB06F7C3,
        0x2220B4CE, 0x953D75CA, 0x28803AF2, 0x9F9DFBF6, 0x46BBB8FB, 0xF1A679FF,
        0xF4F63EE1, 0x43EBFFE5, 0x9ACDBCE8, 0x2DD07DEC, 0x77708634, 0xC06D4730,
        0x194B043D, 0xAE56C539, 0xAB068227, 0x1C1B4323, 0xC53D002E, 0x7220C12A,
        0xCF9D8E12, 0x78804F16, 0xA1A60C1B, 0x16BBCD1F, 0x13EB8A01, 0xA4F64B05,
        0x7DD00808, 0xCACDC90C, 0x07AB9778, 0xB0B6567C, 0x69901571, 0xDE8DD475,
        0xDBDD936B, 0x6CC0526F, 0xB5E61162, 0x02FBD066, 0xBF469F5E, 0x085B5E5A,
        0xD17D1D57, 0x6660DC53, 0x63309B4D, 0xD42D5A49, 0x0D0B1944, 0xBA16D840,
        0x97C6A5AC, 0x20DB64A8, 0xF9FD27A5, 0x4EE0E6A1, 0x4BB0A1BF, 0xFCAD60BB,
        0x258B23B6, 0x9296E2B2, 0x2F2BAD8A, 0x98366C8E, 0x41102F83, 0xF60DEE87,
        0xF35DA999, 0x4440689D, 0x9D662B90, 0x2A7BEA94, 0xE71DB4E0, 0x500075E4,
        0x892636E9, 0x3E3BF7ED, 0x3B6BB0F3, 0x8C7671F7, 0x555032FA, 0xE24DF3FE,
        0x5FF0BCC6, 0xE8ED7DC2, 0x31CB3ECF, 0x86D6FFCB, 0x8386B8D5, 0x349B79D1,
        0xEDBD3ADC, 0x5AA0FBD8, 0xEEE00C69, 0x59FDCD6D, 0x80DB8E60, 0x37C64F64,
        0x3296087A, 0x858BC97E, 0x5CAD8A73, 0xEBB04B77, 0x560D044F, 0xE110C54B,
        0x38368646, 0x8F2B4742, 0x8A7B005C, 0x3D66C158, 0xE4408255, 0x535D4351,
        0x9E3B1D25, 0x2926DC21, 0xF0009F2C, 0x471D5E28, 0x424D1936, 0xF550D832,
        0x2C769B3F, 0x9B6B5A3B, 0x26D61503, 0x91CBD407, 0x48ED970A, 0xFFF0560E,
        0xFAA01110, 0x4DBDD014, 0x949B9319, 0x2386521D, 0x0E562FF1, 0xB94BEEF5,
        0x606DADF8, 0xD7706CFC, 0xD2202BE2, 0x653DEAE6, 0xBC1BA9EB, 0x0B0668EF,
        0xB6BB27D7, 0x01A6E6D3, 0xD880A5DE, 0x6F9D64DA, 0x6ACD23C4, 0xDDD0E2C0,
        0x04F6A1CD, 0xB3EB60C9, 0x7E8D3EBD, 0xC990FFB9, 0x10B6BCB4, 0xA7AB7DB0,
        0xA2FB3AAE, 0x15E6FBAA, 0xCCC0B8A7, 0x7BDD79A3, 0xC660369B, 0x717DF79F,
        0xA85BB492, 0x1F467596, 0x1A163288, 0xAD0BF38C, 0x742DB081, 0xC3307185,
        0x99908A5D, 0x2E8D4B59, 0xF7AB0854, 0x40B6C950, 0x45E68E4E, 0xF2FB4F4A,
        0x2BDD0C47, 0x9CC0CD43, 0x217D827B, 0x9660437F, 0x4F460072, 0xF85BC176,
        0xFD0B8668, 0x4A16476C, 0x93300461, 0x242DC565, 0xE94B9B11, 0x5E565A15,
        0x87701918, 0x306DD81C, 0x353D9F02, 0x82205E06, 0x5B061D0B, 0xEC1BDC0F,
        0x51A69337, 0xE6BB5233, 0x3F9D113E, 0x8880D03A, 0x8DD09724, 0x3ACD5620,
        0xE3EB152D, 0x54F6D429, 0x7926A9C5, 0xCE3B68C1, 0x171D2BCC, 0xA000EAC8,
        0xA550ADD6, 0x124D6CD2, 0xCB6B2FDF, 0x7C76EEDB, 0xC1CBA1E3, 0x76D660E7,
        0xAFF023EA, 0x18EDE2EE, 0x1DBDA5F0, 0xAAA064F4, 0x738627F9, 0xC49BE6FD,
        0x09FDB889, 0xBEE0798D, 0x67C63A80, 0xD0DBFB84, 0xD58BBC9A, 0x62967D9E,
        0xBBB03E93, 0x0CADFF97, 0xB110B0AF, 0x060D71AB, 0xDF2B32A6, 0x6836F3A2,
        0x6D66B4BC, 0xDA7B75B8, 0x035D36B5, 0xB440F7B1, 0x00000001
};

/**********************************/
/********** Function **************/
/**********************************/

static u32 bswap_32(u32 x)
{
	x= ((x<<8)&0xFF00FF00) | ((x>>8)&0x00FF00FF);
	x= (x>>16) | (x<<16);
	
	return x;
}

static u32 libts_get_crc(u8 *buf, u32 size)
{
	u32 crc = -1;
	u8 *end = buf + size;
	while(buf < end){
		crc = crc_table[((u8 )crc) ^ *buf++] ^ (crc >> 8);
	}

	return bswap_32(crc);
}

static int libts_write_pat(t_ts_cfg *ts_cfg, u8 *buf)
{
	memset(buf, 0xff, TS_PACKET_SIZE);
	u8 *q = buf;

	*q++ = TS_SYNC_BYTE;
	*q++ = PAT_PID >> 8 | 0x40;
	*q++ = PAT_PID;
	*q++ = 0x10 | ts_cfg->encvts.pat_cc;
	*q++ = 0;	// pad

	u8 *crc_check = q;	
	*q++ = PAT_TID;
	*q++ = 0xb0;	/* flags */
	*q++ = 0x0d;	/* 5 byte header + 4 byte crc + 4 byte len */
	*q++ = 0x00;	/* id */
	*q++ = 0x01;
	*q++ = 0xc1 | (0 << 1);	/* version */
	*q++ = 0x00;	/* section number */
	*q++ = 0x00;	/* last section number */
	*q++ = DEFAULT_SID >> 8;
	*q++ = DEFAULT_SID;
	u16 val = 0xe000 | DEFAULT_PMT_START_PID;
	*q++ = val >> 8;
	*q++ = val;

	u32 crc = libts_get_crc(crc_check, q - crc_check);
	*q++ = (crc >> 24) & 0xff;
	*q++ = (crc >> 16) & 0xff;
	*q++ = (crc >> 8) & 0xff;
	*q++ = crc & 0xff;

	ts_cfg->encvts.pat_cc = (ts_cfg->encvts.pat_cc + 1) & 0xf;
	return (q - buf);
}

static int libts_write_pmt(t_ts_cfg *ts_cfg, u8 *buf)
{
	memset(buf, 0xff, TS_PACKET_SIZE);
	u8 *q = buf;

	*q++ = TS_SYNC_BYTE;
	*q++ = (DEFAULT_PMT_START_PID >> 8) | 0x40;
	*q++ = (DEFAULT_PMT_START_PID & 0xff);
	*q++ = 0x10 | ts_cfg->encvts.pmt_cc;
	*q++ = 0;	// pad

	u8 *crc_check = q;
	*q++ = PMT_TID;
	*q++ = 0xb0;	/* flags */
#if (defined(ENCV_TS_SYNC)&&defined(CONFIG_AUDIO_EN))
	*q++ = 0x17;	/* 5 byte header *2 + 4 byte crc + 9 byte len */
#else
	*q++ = 0x12;	/* 5 byte header + 4 byte crc + 9 byte len */
#endif
	*q++ = DEFAULT_SID >> 8;	/* id */
	*q++ = DEFAULT_SID;
	*q++ = 0xc1 | (0 << 1);	/* version */
	*q++ = 0x00;	/* section number */
	*q++ = 0x00;	/* last section number */
	u16 val = 0xe000 | DEFAULT_START_PID;
	*q++ = val >> 8;
	*q++ = val;
	*q++ = 0xf0;	/* ES info, description len = 0 */
	*q++ = 0x00;

#if (defined(ENCV_TS_SYNC)&&defined(CONFIG_AUDIO_EN))
	*q++ = STREAM_TYPE_AUDIO_F4;
	val = 0xe000 | DEFAULT_AUDIO_START_PID;
	*q++ = val >> 8;
	*q++ = val;
	*q++ = 0xf0;	/* ES info, description len = 0 */
	*q++ = 0x00;
#endif
	
	*q++ = STREAM_TYPE_VIDEO_ENCVIDEO;
	val = 0xe000 | DEFAULT_START_PID;
	*q++ = val >> 8;
	*q++ = val;
	*q++ = 0xf0;	/* ES info, description len = 0 */
	*q++ = 0x00;

	u32 crc = libts_get_crc(crc_check, q - crc_check);
	*q++ = (crc >> 24) & 0xff;
	*q++ = (crc >> 16) & 0xff;
	*q++ = (crc >> 8) & 0xff;
	*q++ = crc & 0xff;

	ts_cfg->encvts.pmt_cc = (ts_cfg->encvts.pmt_cc + 1) & 0xf;
	return (q - buf);
}

#ifdef ENCV_TS_SYNC
static void libts_write_pts(u8 *buf, int fourbits, u32 pts)
{
	u8 *q = buf;
	int val;
	val = fourbits << 4 | (((pts >> 30) & 0x07) << 1) | 1;
	*q++ = val;
	val = (((pts >> 15) & 0x7fff) << 1) | 1;
	*q++ = val >> 8;
	*q++ = val;
	val = (((pts) & 0x7fff) << 1) | 1;
	*q++ = val >> 8;
	*q++ = val;
}

static u32 fill_len = 0;
static int libts_write_pcr(t_ts_cfg *ts_cfg, u8 *buf)
{
	u8 *q = buf;
//	ts_cfg->encvts.pcr = (ts_cfg->pfrm->ts/10) * 900;
	ts_cfg->encvts.pcr = ts_cfg->encvts.start_tick*900 + (ts_cfg->encvts.vfrm_num * (1000 * 90 / ts_cfg->frmrate));

	*q++ = 0x07 + fill_len; //pcr header length
	*q++ = 0x10; //pcr flag
	*q++ = ts_cfg->encvts.pcr >> 25;
	*q++ = ts_cfg->encvts.pcr >> 17;
	*q++ = ts_cfg->encvts.pcr >> 9;
	*q++ = ts_cfg->encvts.pcr >> 1;
	*q++ = (ts_cfg->encvts.pcr & 1) << 7;
	*q++ = 0x00;

	q += fill_len;
	return (q - buf);
}
#endif

static int libts_write_pes(t_ts_cfg *ts_cfg, u8 *buf)
{
	u8 *q = buf;

	u32 pts = 0;
	u32 dts = 0;

#ifdef ENCV_TS_SYNC
	if(ts_cfg->encvts.dts != 0){
			/* Why add delay here??*/
	//	dts = ts_cfg->encvts.dts + ts_cfg->encvts.delay;
		dts = ts_cfg->encvts.dts;
	}
	if(ts_cfg->encvts.pts != 0){
	//	pts = ts_cfg->encvts.pts + ts_cfg->encvts.delay;
		pts = ts_cfg->encvts.pts;
	}
#endif

	*q++ = 0x00;	// packet start code prefix 0x000001
	*q++ = 0x00;
	*q++ = 0x01;
	if(ts_cfg->frm_audio){
		*q++ = TS_STREAM_ID_AUDIO;
	}else{
		*q++ = TS_STREAM_ID_VIDEO;
	}

	int header_len = 0;
	int flags = 0;

#ifdef ENCV_TS_SYNC
	if(pts != 0){
		header_len += 5;
		flags |= 0x80;
	}
	if(dts != 0 && pts != 0 && ts_cfg->frm_audio == 0){//video frame need pts and dts
		header_len += 5;
		flags |= 0x40;
	}
#endif
	u32 len = ts_cfg->encvts.vfrm_len + header_len + 3;

	if(!ts_cfg->frm_audio){
	ts_cfg->encvts.add_seperate = 1;
	len += 6;	/* seperate stream */
	}

	if(len > 0xffff){
		len = 0;
	}

	*q++ = len >> 8;
	*q++ = len;
	*q++ = 0x80;
	*q++ = flags;
	*q++ = header_len;

#ifdef ENCV_TS_SYNC
	if(pts != 0){
		libts_write_pts(q, flags >> 6, pts);
		q += 5;
	}

	if(dts != 0 && pts != 0 && ts_cfg->frm_audio == 0){
		libts_write_pts(q, 1, dts);
		q += 5;
	}
#endif

	return (q - buf);
}

static int libts_audio_header(t_ts_cfg *ts_cfg, u8 *buf, int start)
{
	u8 val;
	u8 *q = buf;

	*q++ = TS_SYNC_BYTE;
	val = ( DEFAULT_AUDIO_START_PID >> 8);
	if(start){
		val |= 0x40;
	}
	*q++ = val;
	*q++ = (DEFAULT_AUDIO_START_PID & 0xff);
	*q++ = 0x10 | ts_cfg->encvts.audio_cc;
	ts_cfg->encvts.audio_cc = (ts_cfg->encvts.audio_cc + 1) & 0xf;

	return (q - buf);
}

static int libts_packet_header(t_ts_cfg *ts_cfg, u8 *buf, int start)
{
	u8 val;
	u8 *q = buf;

	*q++ = TS_SYNC_BYTE;
	val = ( DEFAULT_START_PID >> 8);
	if(start){
		val |= 0x40;
	}
	*q++ = val;
	*q++ = (DEFAULT_START_PID & 0xff);
	*q++ = 0x10 | ts_cfg->encvts.cc; // payload indicator + cc
	ts_cfg->encvts.cc = (ts_cfg->encvts.cc + 1) & 0xf;

	return (q - buf);
}

static void libts_write_payload_data(t_ts_cfg *ts_cfg, u8 *buf)
{
	u32 addr;
	u32 size = ts_cfg->encvts.payload_len;
	u32 offset = ts_cfg->encvts.vfrm_offset;
	u32 end = offset + size;

	if(offset < ts_cfg->pfrm->size){
		if(end < ts_cfg->pfrm->size){
			addr = (ts_cfg->pfrm->addr + offset) | BIT31;
			memcpy(buf, (void *)addr, size);
			ts_cfg->encvts.vfrm_offset += size;
		}else{
			if(ts_cfg->pfrm->addr1 == 0){
				addr = (ts_cfg->pfrm->addr + offset) | BIT31;
				memcpy(buf, (void *)addr, (ts_cfg->pfrm->size - offset));
				ts_cfg->encvts.vfrm_offset += (ts_cfg->pfrm->size - offset);
			}else{
				size = ts_cfg->pfrm->size - offset;
				addr = (ts_cfg->pfrm->addr + offset) | BIT31;
				memcpy(buf, (void *)addr, size);
				addr = ts_cfg->pfrm->addr1 | BIT31;
				memcpy((buf + size), (void *)addr, (ts_cfg->encvts.payload_len - size));
				ts_cfg->encvts.vfrm_offset += ts_cfg->encvts.payload_len;
			}
		}
	}else{
		offset = offset - ts_cfg->pfrm->size;	// addr1's offset
		if(end < ts_cfg->encvts.vfrm_len){
			addr = (ts_cfg->pfrm->addr1 + offset) | BIT31;
			memcpy(buf, (void *)addr, size);
			ts_cfg->encvts.vfrm_offset += size;
		}else{
			addr = (ts_cfg->pfrm->addr1 + offset) | BIT31;
			memcpy(buf, (void *)addr, (ts_cfg->pfrm->size1 - offset));
			ts_cfg->encvts.vfrm_offset += (ts_cfg->pfrm->size1 - offset);
		}
	}
}

static void libts_write_stuffing(u8 *buf, u32 stuffing_len, u32 header_len)
{
	memmove(buf + 4 + stuffing_len, buf + 4, header_len - 4);
	buf[3] |= 0x20;
	buf[4] = stuffing_len - 1;
	if(stuffing_len >= 2){
		buf[5] = 0;
		memset(buf + 6, 0xff, stuffing_len - 2);
	}
}

static u32 libts_add_seperate(u8 *buf)
{
	u8 *q = buf;

	*q++ = 0x0;
	*q++ = 0x0;
	*q++ = 0x0;
	*q++ = 0x1;
	*q++ = 0x09;
	*q++ = 0xe0;

	return(q -buf);
}

static u32 fill_len_2 = 0;
static u32 libts_filldata_internal(t_ts_cfg *ts_cfg, u8 *pkt_buf, u32 ts)
{
	u8 *buf = pkt_buf;
	memset(buf, 0xff, TS_PACKET_SIZE);

#ifdef ENCV_TS_SYNC
//	if(ts_cfg->encvts.start_tick == 0){
//		//ts_cfg->encvts.start_tick = g_libvenc_cfg.reftm;
//		//ts_cfg->encvts.start_tick = ts; 
//		ts_cfg->encvts.start_tick = 40; 
//	}
	ts_cfg->encvts.start_tick = 0; 

	if(ts_cfg->pfrm->ts == 0){
		ts_cfg->pfrm->ts = 1;
	}
	ts_cfg->encvts.pts = 90 * (ts_cfg->pfrm->ts) + ts_cfg->encvts.start_tick*900;
	//debug_printf("pts:%d t:%d:%d, s:%d ",ts_cfg->encvts.pts, ts_cfg->pfrm->ts,ts,ts_cfg->encvts.start_tick);
	ts_cfg->encvts.dts = ts_cfg->encvts.pts;

	ts_cfg->encvts.pcr_pkt_cnt++;
#endif

	if(ts_cfg->frm_audio){
		buf += libts_audio_header(ts_cfg, buf, ts_cfg->encvts.start);
	}else{
		buf += libts_packet_header(ts_cfg, buf, ts_cfg->encvts.start);
	}
	if(ts_cfg->encvts.start){
#ifdef ENCV_TS_SYNC
		fill_len = 0;
		fill_len_2 = 0;
		u32 total_len = 0;
		u32 a_size = ts_cfg->pfrm->size;
		if(ts_cfg->frm_audio){
			total_len = a_size + 22;      // pcr + pes(pts) = 22;
		}
		else{
			total_len = a_size + 27 + 6;      // pcr + pes(pts,dts) + separate_byte= 27
		}

		if(ts_cfg->encvts.pcr_pkt_cnt >= ts_cfg->encvts.pcr_pkt_period){
			if(total_len < 184 ){
				fill_len = 184 - total_len;
 			}
			/* write pcr */
			ts_cfg->encvts.pcr_pkt_cnt = 0;
			pkt_buf[3] |= 0x20;	// set adaptation field flag
			buf += libts_write_pcr(ts_cfg, buf);
		}else{
			total_len -= 8;     //pcr_len = 8
			if(total_len < 184 ){
				fill_len = 184 - total_len;
				if(fill_len == 1){
					*buf++ = 0x00;
				}
				else if(fill_len ==2){
					*buf++ = 0x01;
					*buf++ = 0x00;
				}
				else{
					*buf++ = fill_len - 1;
					*buf++ = 0x00;
					buf += fill_len - 2;
				}
			}
		}
#endif
		buf += libts_write_pes(ts_cfg, buf);
	}
	
	/* header len */
	u32 header_len = buf - pkt_buf;
	/* seperate len */
	u32 seperate_len = ts_cfg->encvts.add_seperate ? 6 : 0;
	/* data len */
	ts_cfg->encvts.payload_len = TS_PACKET_SIZE - header_len - seperate_len;
	/* stuffing data */
	if(ts_cfg->encvts.payload_len > (ts_cfg->encvts.vfrm_len - ts_cfg->encvts.vfrm_offset)){
		pkt_buf[3] |= 0x20;
		u32 stuffing_len = ts_cfg->encvts.payload_len - (ts_cfg->encvts.vfrm_len - ts_cfg->encvts.vfrm_offset);
		ts_cfg->encvts.payload_len = ts_cfg->encvts.vfrm_len - ts_cfg->encvts.vfrm_offset;
		if(stuffing_len == 1){
			*buf++ = 0x00;
 		}
		else if(stuffing_len == 2){
			*buf++ = 0x01;
			*buf++ = 0x00;
		}
		else{
			*buf++ = stuffing_len - 1;
			*buf++ = 0x00;
			buf += stuffing_len - 2;
		}
 	}
	/* payload buf begin addr*/
	buf = pkt_buf + TS_PACKET_SIZE - ts_cfg->encvts.payload_len;
	/* add seperate */
	if(ts_cfg->encvts.add_seperate){
		ts_cfg->encvts.add_seperate = 0;
		libts_add_seperate(buf - seperate_len);
	}

	libts_write_payload_data(ts_cfg, buf);
	if(ts_cfg->encvts.start == 1){
		if(!ts_cfg->frm_audio){
			ts_cfg->encvts.vfrm_num++;
		}else{
			ts_cfg->encvts.afrm_num++;
		}
		ts_cfg->encvts.start = 0;
	}

	return 0;
}

static void libts_addpat(t_ts_cfg *ts_cfg, u8* buf){
	ts_cfg->encvts.pat_pkt_cnt = 0;
	libts_write_pat(ts_cfg, buf);
	libts_write_pmt(ts_cfg, (buf+TS_PACKET_SIZE));
}

void libts_init(t_ts_cfg *ts_cfg)
{
	ts_cfg->tsbuf_start = 0;
	ts_cfg->tsbuf_num = 0;
	memset(&ts_cfg->encvts, 0, sizeof(t_encv_ts));
	int fps = ts_cfg->frmrate;
	int bit_rate = ts_cfg->bitrate;
	ts_cfg->encvts.mux_rate = bit_rate + ((19 + 184 / 2) * 8 / fps);
	ts_cfg->encvts.pat_pkt_cnt = 0;
	ts_cfg->encvts.pat_pkt_period = (ts_cfg->encvts.mux_rate * PAT_RETRANS_TIME) / (TS_PACKET_SIZE * 8 * 1000);
	ts_cfg->encvts.pat_cc = 0;
	ts_cfg->encvts.pmt_cc = 0;
	ts_cfg->encvts.cc = 0;
	ts_cfg->encvts.audio_cc = 0;

#ifdef ENCV_TS_SYNC
	ts_cfg->encvts.delay = 1000 * 90 / fps;
	ts_cfg->encvts.start_tick = 0;
	ts_cfg->encvts.pcr = 0;
	ts_cfg->encvts.pcr_pkt_period = (ts_cfg->encvts.mux_rate * PCR_RETRANS_TIME) / (TS_PACKET_SIZE * 8 * 1000);
	ts_cfg->encvts.pcr_pkt_cnt = ts_cfg->encvts.pcr_pkt_period;
#endif
	libts_write_pat(ts_cfg, tsbuf_addr);
	ts_cfg->tsbuf_num++;

	libts_write_pmt(ts_cfg, tsbuf_addr);
	ts_cfg->tsbuf_num++;

	ts_cfg->fill_done = 1;
}

FRAME* avfdist_get_ts_data(t_ts_cfg *ts_cfg)
{
	if(ts_cfg->vpfrm != NULL && (ts_cfg->frm_audio == 0)){
		libvs_remove_frame(ts_cfg->vs_id, ts_cfg->vpfrm);
		ts_cfg->vpfrm = NULL;
	}

#ifdef CONFIG_AUDIO_EN
	if(ts_cfg->apfrm != NULL && (ts_cfg->frm_audio)){
		//afdist_release_frame(a_id);
		libaenc_remove_frame(ts_cfg->apfrm);
		ts_cfg->apfrm = NULL;
	}
#endif

	if(ts_cfg->vpfrm == NULL){
		ts_cfg->vpfrm = libvs_get_frame(ts_cfg->vs_id, GET_FRAME_TIMEOUT);
		if(ts_cfg->vpfrm){
	//		debug_printf("vfrm:%d, %d, %x, %d, %d\n", ts_cfg->vpfrm->status, ts_cfg->vpfrm->type, ts_cfg->vpfrm->addr, ts_cfg->vpfrm->size, ts_cfg->vpfrm->ts);
				//video may drop frame, so use its ts as the time stamp

#if (defined(ENCV_TS_SYNC)&&defined(CONFIG_AUDIO_EN))
			if(ts_cfg->vstart_ts == 0){
				u32 ref_time = 0;
				
			//	if(g_libvenc_cfg.reftm>g_liba_ecfg.reftm){      //TODO:reftm
			//		ref_time = 10*(g_libvenc_cfg.reftm - g_liba_ecfg.reftm);    //ms
			//	}
				ts_cfg->vstart_ts = ts_cfg->vpfrm->ts - ref_time;
				ts_cfg->vpfrm->ts = ref_time;
			}else{
				ts_cfg->vpfrm->ts -= ts_cfg->vstart_ts;
			}
#endif
		}
	}
#ifdef CONFIG_AUDIO_EN
	if(ts_cfg->apfrm == NULL){
		ts_cfg->apfrm = libaenc_get_frame(GET_FRAME_TIMEOUT);
		if(ts_cfg->apfrm){
			//audio use the sample rate to caculate the time stamp
//			if(g_libvenc_cfg.reftm<g_liba_ecfg.reftm){
//				ts_cfg->apfrm->ts += (g_liba_ecfg.reftm - g_libvenc_cfg.reftm)*10;
//			}
//			ts_cfg->apfrm->ts = 0;
			ts_cfg->afrm_num++;
			if(ts_cfg->afrm_num == 1){
				ts_cfg->apfrm->ts = 0;
			}
			if(ts_cfg->astart_ts == 0){
				u32 ref_time = 0;
				
			//	if(g_libvenc_cfg.reftm>g_liba_ecfg.reftm){      //TODO:reftm
			//		ref_time = 10*(g_libvenc_cfg.reftm - g_liba_ecfg.reftm);    //ms
			//	}
				ts_cfg->astart_ts = ts_cfg->apfrm->ts - ref_time;
				ts_cfg->apfrm->ts = ref_time;
			}else{
				ts_cfg->apfrm->ts -= ts_cfg->astart_ts;
			}
		}
	}
#endif

	if(ts_cfg->vpfrm == NULL && ts_cfg->apfrm == NULL){
		return NULL;
	}else if( ts_cfg->vpfrm && (ts_cfg->apfrm == NULL || ts_cfg->vpfrm->ts <= ts_cfg->apfrm->ts)){
		ts_cfg->frm_audio = 0;
//		debug_printf("t:%d",ts_cfg->vpfrm->ts);
		return ts_cfg->vpfrm;
	}
#ifdef CONFIG_AUDIO_EN
	else if(ts_cfg->apfrm && (ts_cfg->vpfrm == NULL || ts_cfg->vpfrm->ts > ts_cfg->apfrm->ts)){
		ts_cfg->frm_audio = 1;
//		debug_printf("a:%d",ts_cfg->apfrm->ts);
		return ts_cfg->apfrm;
	}
#endif
	return NULL;
}


void libfdist_ts_filldata(t_ts_cfg *ts_cfg, void (*filldone)(void), u32* frm_flags, u32* frm_ts, u8 use_avfdist)
{
	if(ts_cfg->tsbuf_num > (ts_cfg->tspkt_cnt - 3)){
		return;
	}

	u32 tmpflags = 0;
	if(ts_cfg->pfrm == NULL)
	{
		if(ts_cfg->fill_done){
			ts_cfg->pfrm = avfdist_get_ts_data(ts_cfg);
			if(ts_cfg->pfrm == NULL){
				tmpflags = VDATA_FLAG_NODATA;
				if(frm_flags != NULL){
					*frm_flags = tmpflags;
				}
				return;
			}
			//debug_printf("vfrm %d, %d, %x, %d, %d\n", ts_cfg->pfrm->status, ts_cfg->pfrm->type, ts_cfg->pfrm->addr, ts_cfg->pfrm->size, ts_cfg->pfrm->ts);
			ts_cfg->fill_done = 0;
			tmpflags |= VDATA_FLAG_FIRSTPKT;
			if(ts_cfg->pfrm->type == FRAME_TYPE_VIDEO_I){
				tmpflags |= VDATA_FLAG_I;
			}
			if(ts_cfg->pfrm->type == FRAME_TYPE_VIDEO_IDR){
				tmpflags |= VDATA_FLAG_IDR;
			}
			if(frm_ts != NULL){
				*frm_ts = ts_cfg->pfrm->ts;
			//	debug_printf("frmts:%d",*frm_ts);
			}

			ts_cfg->encvts.vfrm_len = ts_cfg->pfrm->size;
			if(ts_cfg->pfrm->addr1){
				ts_cfg->encvts.vfrm_len += ts_cfg->pfrm->size1;
			}
			ts_cfg->encvts.vfrm_offset = 0;
			ts_cfg->encvts.start = 1;

		}
	}
	if(ts_cfg->pfrm != NULL)
	{ 
		if(++ts_cfg->encvts.pat_pkt_cnt == ts_cfg->encvts.pat_pkt_period){
			ts_cfg->encvts.pat_pkt_cnt = 0;

			libts_write_pat(ts_cfg, tsbuf_addr);
			ts_cfg->tsbuf_num++;

			libts_write_pmt(ts_cfg, tsbuf_addr);
			ts_cfg->tsbuf_num++;
		}

		libts_filldata_internal(ts_cfg, tsbuf_addr, *frm_ts);
		ts_cfg->tsbuf_num++;

		if(ts_cfg->encvts.vfrm_offset >= ts_cfg->encvts.vfrm_len){
			ts_cfg->fill_done = 1;
			tmpflags |= VDATA_FLAG_LASTPKT;
			ts_cfg->pfrm = NULL;
		}
	}

	if(use_avfdist){
		if(frm_flags != NULL){
			*frm_flags = tmpflags;
		}
	}
}

void libts_copypkt(t_ts_cfg *ts_cfg, u8 cpy_cnt)
{
	ts_cfg->tsbuf_num -= cpy_cnt;
	ts_cfg->tsbuf_start += cpy_cnt;

	if(ts_cfg->tsbuf_start >= ts_cfg->tspkt_cnt){
		ts_cfg->tsbuf_start -= ts_cfg->tspkt_cnt;
	}
}

static int check_seg_end(t_ts_cfg *ts_cfg, u32 ts)
{
	if(ts_cfg->seg_intval == 0)
	{
		return 0;
	}
	if(ts_cfg->last_ts == 0)
	{
		ts_cfg->last_ts = ts;	
	}
	//debug_printf("ts:%d last:%d", ts, ts_cfg->last_ts);
	if((ts - ts_cfg->last_ts)/10 > (ts_cfg->seg_intval - 10))
	{
		ts_cfg->last_ts = ts;	
		return 1;
	}
	else
	{
		return 0;
	}
}
int libts_get_data(t_ts_cfg *ts_cfg, char *buf, int max_len)
{
	if(max_len < TS_PACKET_SIZE*ts_cfg->tspkt_cnt + 10) 
	{
		debug_printf("buffer size is small, should be large than %d bytes\n", sizeof(ts_cfg->tsbuf));
		return -1;
	}
	u32 size = 0;
	u8 tsb = ts_cfg->tsbuf_start;
	u32 tmp_flags = 0;
	u32 cur_flags = 0;
	ts_cfg->frm_flag = cur_flags;
	u32 tmp_ts = 0;
	//ts_cfg->frm_ts = 0;
	if(ts_cfg->seg_flag == SEG_END)
	{
		libts_addpat(ts_cfg, (u8*)(buf));
		ts_cfg->seg_flag = SEG_START;
		return 188*2;
	}
	if(ts_cfg->seg_flag != SEG_START) //data have been filled in last call, so donn't need call fill data again
	{
		while(ts_cfg->tsbuf_num < (ts_cfg->tspkt_cnt-3)){                //fill data once, may use 3 pkt buf.
			/* // do not need, IDR frame size should be more than 1880
			if(new_frm != -1 && (tmp_flags & VDATA_FLAG_IDR)){
				//wait IDR frame done. 			break;
			}*/
			libfdist_ts_filldata(ts_cfg, NULL, &tmp_flags, &tmp_ts, 1);

			if(tmp_flags & VDATA_FLAG_FIRSTPKT){
				cur_flags = tmp_flags;
				ts_cfg->frm_ts = tmp_ts;
				ts_cfg->frm_flag = cur_flags;
			}else if(tmp_flags == VDATA_FLAG_NODATA){
				cur_flags = tmp_flags;
				ts_cfg->frm_flag = cur_flags;
				return -1;
			}
			if(tmp_flags & VDATA_FLAG_LASTPKT){
				cur_flags = tmp_flags;
				cur_flags |= VDATA_FLAG_LASTPKT;
				ts_cfg->frm_flag = cur_flags;
				ts_cfg->last_frm_ts = ts_cfg->frm_ts;
				break;
			}
		}
		if((cur_flags & VDATA_FLAG_FIRSTPKT) && (cur_flags & VDATA_FLAG_IDR)){
			if(check_seg_end(ts_cfg, ts_cfg->last_frm_ts))
			{
				ts_cfg->seg_flag = SEG_END;
				ts_cfg->frm_flag = cur_flags;
				return 0;
			}
		}
	}
	else
	{
		ts_cfg->seg_flag = SEG_MID;
	}
	if(ts_cfg->tsbuf_num > 0){
		size = (ts_cfg->tsbuf_num)*TS_PACKET_SIZE;
		if((tsb+ts_cfg->tsbuf_num)<ts_cfg->tspkt_cnt){
			memcpy(buf, &ts_cfg->tsbuf[tsb*TS_PACKET_SIZE], size);
		}else{
			u32 first_size = (ts_cfg->tspkt_cnt - tsb)*TS_PACKET_SIZE;
			memcpy(buf, &ts_cfg->tsbuf[tsb*TS_PACKET_SIZE], first_size);
			memcpy(buf+first_size, &ts_cfg->tsbuf[0], (size - first_size));
		}
		libts_copypkt(ts_cfg, ts_cfg->tsbuf_num);
		return size;
	}
	else
	{
		return -1;
	}
}


