#include "includes.h"

int libosd4l_timestr_get(t_osd4l_timestr * timestr)
{

	time_t os_time = time(NULL);
	if(os_time - timestr->prev_ticks >= 1){
		char data_tmp[24];
		struct tm cur_tm;
		
		localtime_r(&os_time, &cur_tm);

		sprintf(data_tmp, "%.4d-%.2d-%.2d %.2d:%.2d:%.2d\n", 1900+cur_tm.tm_year, cur_tm.tm_mon+1, cur_tm.tm_mday, cur_tm.tm_hour, cur_tm.tm_min, cur_tm.tm_sec);
		strcpy(timestr->timestr, data_tmp);

		timestr->prev_ticks = os_time;
		return 1;
	}
	return 0;
}
