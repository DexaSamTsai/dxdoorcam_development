#include "includes.h"

void gui_draw_fontbitmap(t_sguictx * gui, u8 *data, int width, int height, int x, int y, int w_max, u32 color){
	int i, j;
	int bw = (width + 7)>>3;

	for(j=0; j<height; j++){
		for(i=0; i<w_max && i<width; i++){
			if(data[j*bw+i/8] & ( 1 << ( 7 - (i & 7)))){
				gui->set_pixel(gui->hw_arg, x+i, y+j, color);
			}
		}
	}

	return;
}
