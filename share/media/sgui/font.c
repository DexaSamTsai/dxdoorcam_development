#include "includes.h"

//default get_char func
u32 gui_get_char_utf8(const u8 *str, int * len){

  u32 r;
  unsigned char c = *str;

  if (c == '\0'){
	*len = 0;
	return 0;
  }

  if ((c & 0x80) == 0) {
  /* 1 byte */
    r = c;
	*len = 1;
  } else if ((c & 0xe0) == 0xc0) {
  /* 2 bytes */
    r = (c & 0x1f) << 6;
    c = *(++str);
    c &= 0x3f;
    r |= c;
	*len = 2;
  } else if ((c & 0xf0) == 0xe0) {
  /* 3 bytes */
    r = (c & 0x0f) << 12;
    c = *(++str);
    c &= 0x3f;
    r |= (c << 6);
    c = *(++str);
    c &= 0x3f;
    r |= c;
	*len = 3;
  } else {
    r = c;
	*len = 1;
  }

	return r;
}

int gui_draw_string(struct s_sguictx * gui, const char *s, int x0,int y0, int w_max, u32 color){
	int cur_x;
	int len;
	u32 c;

	if(gui->font == NULL || gui->font->draw_char == NULL){
		return -1;
	}

	cur_x = x0;

	while(1){
		if(cur_x >= x0 + w_max) break;

		if(gui->font->get_char == NULL){
			c = *(const u8 *)(s);
			len = 1;
		}else{
			c = gui->font->get_char(s, &len);
		}

		if(len == 0){
			break;
		}

		s += len;

		cur_x += gui->font->draw_char(gui, c, cur_x, y0, x0 + w_max - cur_x, color);
	}

	return 0;
}
