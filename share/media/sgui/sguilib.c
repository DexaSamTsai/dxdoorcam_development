#include "includes.h"

int gui_init(t_sguictx * gui){

	if(gui->set_pixel == NULL){
		syslog(LOG_ERR, "[ERR] gui->set_pixel NULL\n");
		return -1;
	}

	if(gui->font->get_char == NULL){
		gui->font->get_char = gui_get_char_utf8;
	}

	return 0;
}
