#include "includes.h"

int gui_extfont_init(t_sguictx * gui){

	if(gui == NULL || gui->font == NULL){
		return -1;
	}

	t_extfont * pext = (t_extfont *)gui->font->arg;

	if(pext->getdata == NULL){
		return -2;
	}

	unsigned char tmpbuf[16];
	int index1_count;
	int i;

	if( pext->getdata(gui, 0, 16, tmpbuf) < 16 ) return -2;

	pext->height = tmpbuf[0];
	pext->maxwidth = tmpbuf[1];

	//init the index1

	index1_count = (tmpbuf[8] << 24) | (tmpbuf[9] << 16) | (tmpbuf[10] << 8) | tmpbuf[11] ;
	if(index1_count & 0x7) return -3;
	index1_count >>= 3;

	if(pext->index1_count > index1_count){
		pext->index1_count = index1_count;
	}

	for(i=0; i < pext->index1_count; i ++ ){
		if( pext->getdata(gui, 16 + i*8, 8, tmpbuf) < 8 ){
			continue;
		}
		pext->index1[i].first = (tmpbuf[0] << 8) | tmpbuf[1];
		pext->index1[i].last = (tmpbuf[2] << 8) | tmpbuf[3];
		pext->index1[i].firstoffset = (tmpbuf[4] << 24) | (tmpbuf[5] << 16) | (tmpbuf[6] << 8) | tmpbuf[7] ;
	}

	return 0;
}

int _ext_getchardata(t_sguictx * gui, u32 c, int datasize, unsigned char *cinfo)
{
	unsigned int index2;
	unsigned char index2buf[4];

	t_extfont * pext = (t_extfont *)gui->font->arg;

	//get index1
	int i;
	for(i=0; i < pext->index1_count; i++){
		if ((c >= pext->index1[i].first) && (c <= pext->index1[i].last))
		break;
	}
	if(i >= pext->index1_count){
		return 0;
	}

	//get index2
	if(pext->getdata(gui, pext->index1[i].firstoffset + (c - pext->index1[i].first)*4, 4, index2buf) < 4)
		return 0;
	index2 = (index2buf[0] << 24) | (index2buf[1] << 16) | (index2buf[2] << 8) | index2buf[3] ;

	if(index2 == 0) return 0; //character not exist

	return pext->getdata(gui, index2, datasize, cinfo);

}

#define CINFO_HEADSIZE 3
int extfont_draw_char(t_sguictx * gui, u32 charcode, int x, int y, int w_max, u32 color){

	if(gui->font == NULL){
		return 0;
	}

	t_extfont * pext = (t_extfont *)gui->font->arg;

	//assume the width is not larger than height, 8bits aligned
	int datasize = ((pext->maxwidth + 7 ) >> 3)*(pext->height);
	if(CINFO_HEADSIZE+datasize > pext->cinfo_size) datasize = pext->cinfo_size;
	else datasize += CINFO_HEADSIZE;

	if(_ext_getchardata(gui, charcode, datasize, pext->cinfo) < CINFO_HEADSIZE){
		return 0;
	}

	gui_draw_fontbitmap(gui, pext->cinfo + CINFO_HEADSIZE, pext->cinfo[0], pext->height, x, y, w_max, color);

	return pext->cinfo[0];
}

//default nand getdata func
u32 gui_nandfont_getdata(t_sguictx * gui, u32 offset, u32 size, void *buf){

	t_extfont * pext = (t_extfont *)gui->font->arg;
	u32 nandfont_start = (u32)(pext->arg);

	int todo = size;
	int n;
	u32 start = nandfont_start + offset;

	while(todo){
		n = (g_nc->page_size) - (start & (g_nc->page_size - 1 ));
		if(n > todo) n = todo;

		nand_read_page((start & (g_nc->page_size - 1 )), start / g_nc->page_size, (int)buf, n, 0);

		todo -= n;
		start += n;
		buf += n;
	}

	return size;
}

