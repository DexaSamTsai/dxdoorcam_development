#include "includes.h"

int intfont_draw_char(t_sguictx * gui, u32 charcode, int x, int y, int w_max, u32 color){
	if(gui->font == NULL){
		return 0;
	}

	const t_intfont * arg = (const t_intfont *)(gui->font->arg);

	if(charcode < arg->start || charcode > arg->end){
		return 0;
	}

	gui_draw_fontbitmap(gui, (u8 *)arg->data[charcode - arg->start], arg->width, arg->height, x, y, w_max, color);

	return arg->width;
}

