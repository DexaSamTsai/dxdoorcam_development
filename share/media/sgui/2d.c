#include "includes.h"
#include "media/sgui/sgui.h"

void gui_draw_hline(t_sguictx * gui, int x0, int x1, int y, u32 color)
{
	int dx;
	int addx = 1;
	
	dx = x1 - x0;

	if( dx < 0)
	{
		addx = -1;	
	}

	while(x0 != x1)
	{
		gui->set_pixel(gui->hw_arg, x0, y, color);
		x0 += addx;
	}

	gui->set_pixel(gui->hw_arg, x0, y, color);
}

void gui_draw_vline(t_sguictx * gui, int x, int y0, int y1, u32 color)
{
	int dy;
	int addy = 1;

	dy = y1 - y0;

	if(dy < 0 ) addy = -1;

	while(y0 != y1)
	{
		gui->set_pixel(gui->hw_arg, x, y0, color);
		y0 += addy;
	}
	
	gui->set_pixel(gui->hw_arg, x, y0, color);
}

void gui_draw_line(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color)
{
	int dx,dy;
	int addx = 1,addy = 1;
	int dy_2 = 0;
	int dx_2 = 0;
	int di;

	dx = x1 - x0;
	dy = y1 - y0;
	
	//no line
	if((dx == 0 ) && (dy == 0)){
		return;
	}

	//hline
	if(dy == 0){
		gui_draw_hline(gui,x0,x1,y0,color);
		return;
	}
	
	//vline
	if(dx == 0){
		gui_draw_vline(gui,x0,y0,y1,color);
		return;
	}

	// judge the y add driection
	if(dy < 0){
		addy = -1;
		dy = abs(dy);
	}

	// judge the x add direction
	if(dx < 0){
		addx = -1;
		dx = abs(dx);
	}
	
	dx_2 = dx * 2;
	dy_2 = dy * 2;


	if(dy >= dx){
		di = dx_2 - dy;
		
		while(y0 != y1){
			gui->set_pixel(gui->hw_arg,x0,y0,color);
			y0 += addy;
			
			if(di < 0){
				di += dx_2;
			}else{
				di += dx_2 - dy_2;
				x0 += addx;
			}
		}
	}else{
		di = dy_2 - dx;
		
		while(x0 != x1){
			gui->set_pixel(gui->hw_arg,x0,y0,color);
			x0 += addx;
			
			if(di < 0){
				di += dy_2;	
			}else{
				di += dy_2 - dx_2;
				y0 += addy;
			}
		}
	}

	//draw the last point	
	gui->set_pixel(gui->hw_arg,x0,y0,color);

	return;
}

void gui_draw_rect(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color)
{
	int dx;
	
	dx = x1 - x0;

	gui_draw_vline(gui,x0,y0,y1,color);
	gui_draw_vline(gui,x1,y0,y1,color);
	
	if(dx < 0){
		x0 -= 1;
		x1 += 1;
	}else{
		x0 += 1;
		x1 -= 1;
	}

	gui_draw_hline(gui,x0,x1,y0,color);
	gui_draw_hline(gui,x0,x1,y1,color);
	
	return;
}


static void inline  _gui_draw_circle_point(t_sguictx * gui,int x0,int y0,int dx,int dy,u32 color)
{
	gui->set_pixel(gui->hw_arg, x0+dx,y0+dy, color);
	gui->set_pixel(gui->hw_arg, x0+dx,y0-dy, color);
	gui->set_pixel(gui->hw_arg, x0-dx,y0+dy, color);
	gui->set_pixel(gui->hw_arg, x0-dx,y0-dy, color);
	gui->set_pixel(gui->hw_arg, x0+dy,y0+dx, color);
	gui->set_pixel(gui->hw_arg, x0+dy,y0-dx, color);
	gui->set_pixel(gui->hw_arg, x0-dy,y0+dx, color);
	gui->set_pixel(gui->hw_arg, x0-dy,y0-dx, color);
}

void gui_draw_circle(t_sguictx * gui, int x0, int y0, int r,u32 color)
{
	int x,y,f;

	x = 0;
	y = r;
	f = 3 - 2*r;

	while(x < y){
		_gui_draw_circle_point(gui,x0,y0,x,y,color);
		
		if(f < 0){
			f += 4*x + 6;
		}else{
			f += 4*(x-y) + 10;
			y--;
		}
		x++;
	}

	if(x == y){
		_gui_draw_circle_point(gui,x0,y0,x,y,color);
	}
	
	return;
}


void gui_fill_rect(t_sguictx * gui, int x0, int y0, int x1, int y1, u32 color)
{
	int dx;
	int add;

	dx = x1 - x0;
	
	if(dx >= 0){
		add = 1;
	}else{
		add = -1;
	}

	while(x0 != x1)
	{
		gui_draw_vline(gui, x0,y0,y1,color);
		x0 += add;
	}

	gui_draw_vline(gui, x0,y0,y1,color);
	
	return;
}

void gui_fill_circle(t_sguictx * gui, int x0, int y0, int r, u32 color)
{
	int x,y;
	int dx,dy;
	int d;
	int xi;

	x= 0;
	y = r;
	dx = 3;
	dy = 2-2*r;
	d = 1- r;
	
	gui->set_pixel(gui->hw_arg, x0+x,y0+y, color);
	gui->set_pixel(gui->hw_arg, x0+x,y0-y, color);

	for(xi = x0-r; xi<=x0+r; xi++){
		gui->set_pixel(gui->hw_arg, xi,y0, color);
	}

	while(x<y){
		if(d < 0){
			d += dx;
			dx += 2;
			x++;
		}else{
			d += dx + dy;
			dx +=2;
			dy += 2;
			x++;
			y--;
		}

		for(xi = x0 - x; xi <= x0+ x; xi++ )
		{
			gui->set_pixel(gui->hw_arg, xi,y0-y,color);
			gui->set_pixel(gui->hw_arg, xi,y0+y,color);
		}
	
		for(xi = x0 -y; xi <= x0 + y; xi++ )
		{
			gui->set_pixel(gui->hw_arg, xi,y0-x,color);
			gui->set_pixel(gui->hw_arg, xi,y0+x,color);
		}
	}
	
	return;
}
