#ifndef _ALGO_OVF_H_
#define _ALGO_OVF_H_

typedef struct s_ovf_algo_cfg{
	//FIXME: unused input sample
	int format;
	int width;
	int height;
}t_ovf_algo_cfg;

typedef struct s_ovf_algo_output{
	int is_true_alarm;	// 1: true alarm, 0: false alarm
}t_ovf_algo_output;

int ovf_algo_open(t_ovf_algo_cfg * cfg);
int ovf_algo_process(char * buf_y, char * buf_uv, t_ovf_algo_output * output);
int ovf_algo_close(void);

#endif
