#include "includes.h"
#include "ovf_algo.h"

static t_ovf_algo_cfg _ovf_algo_cfg;

int ovf_algo_open(t_ovf_algo_cfg * cfg)
{
	if(cfg != NULL){
		memcpy(&_ovf_algo_cfg, cfg, sizeof(t_ovf_algo_cfg));
	}

	return 0;
}

//FIXME: This is a pesudo algo to judge the 'alarm' and output
#define PROBABILITY_FACTOR		50
extern u32 getcycle(void);
int ovf_algo_process(char * buf_y, char * buf_uv, t_ovf_algo_output * output)
{
	srand(getcycle());
	u32 ret =  rand();
	if(ret % PROBABILITY_FACTOR == 1){
		output->is_true_alarm = 1;
	}else{
		output->is_true_alarm = 0;
	}
}

int ovf_algo_close(void)
{
	return 0;
}
