#include "includes.h"
#include "media/va/video_analysis.h"
#include "ovf_algo.h"

#define YUV_WIDTH_DFT	320
#define YUV_HEIGHT_DFT	180

#define EFS_BUFFER  (MEMBASE_DDR + 0xa00000)
#define EFS_BUFFER_SIZE   (1024*1024)

//#define YUV_WRITE_SD        //Config this if need to store YUV to SD

static t_video_analysis_cfg _va_cfg;

static u32 va_status = 0; //0:IDLE, 1:running, 2:to_stop
static pthread_cond_t _va_stop_cond = PTHREAD_COND_INITIALIZER;
static pthread_t va_thread = NULL;
u32 va_stat_ms = 0;

static void _va_open( int wc, int hc, int ws, int hs )
{
	//FIXME: only a sample of va algo init
	t_ovf_algo_cfg cfg;
	cfg.format = 1;
	cfg.width = wc;
	cfg.height = hc;
	ovf_algo_open(&cfg);

	va_stat_ms = 0;
}

static void _va_close(void)
{
	//FIXME: only a sample of va algo exit
	ovf_algo_close();
}

t_ovf_algo_output _ovf_algo_output;
static void _va_run( unsigned char *buf_Y, unsigned char *buf_UV )
{
    u32 start_ms = ticks * 10 + tick1_get_cycle() * 10 / ReadReg32(REG_INT_BASE + 0x88);

	//FIXME: only a pesudo sample of va algo process
	ovf_algo_process(buf_Y, buf_UV, &_ovf_algo_output);

    u32 end_ms = ticks * 10 + tick1_get_cycle() * 10 / ReadReg32(REG_INT_BASE + 0x88);

	va_stat_ms += end_ms - start_ms;

	if(_va_cfg.run_cb != NULL){
		_va_cfg.run_cb();
	}
}

int video_analysis_result(t_video_analysis_res * res)
{
	if(res == NULL){
		return -1;
	}

	//FIXME: only a pesudo sample of getting va algo result
	res->is_true_alarm = _ovf_algo_output.is_true_alarm;

	//test
	res->obj_type[0] = 0;
	res->obj_left[0] = 80;
	res->obj_top[0] = 100;
	res->obj_right[0] = 240;
	res->obj_bottom[0] = 180;
	res->obj_num = 1;

	return 0;
}

static void * _video_analysis_routine(void* arg)
{
	pthread_detach(pthread_self());
	pthread_setname(NULL, "VideoAnalysis");

	va_status = 1;

#ifdef YUV_WRITE_SD
    efs_init(EFS_HW_SCIF, 0, EFS_BUFFER, EFS_BUFFER_SIZE);
    debug_printf("  SD card capacity: %d sectors\n", sh_efs.sectors);
    debug_printf("  Filesystem sectors per Cluster: %d\n", sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem sectors count: %d, cluster count:%d\n", sh_efs.sectors, sh_efs.sectors/sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem Free Clusters: %d\n", sh_efs.clusters_free);
    debug_printf("  Filesystem first Free Cluster: %d\n", sh_efs.next_free_cluster);

	u8 g_file_name[30];
    sprintf((char *)g_file_name, "vs%dtest.txt", 0);

    FILE * fp_yuv = fopen(g_file_name, "w");
    if(fp_yuv == NULL){
        debug_printf("open file %s failed\n", g_file_name);
    }else{
        debug_printf("open file %s\n", g_file_name);
    }
#endif

	u32 ret = 0;
	u32 vs_id = _va_cfg.vs_id;

	if(vs_id >= 0){
		ret = libvs_init(vs_id);
		ret |= libvs_start(vs_id);

		if(ret != 0){
			syslog(LOG_ERR, "[ERROR]: VA start vs %d fail.\n", vs_id);
			return;
		}

		u32 resolution = libvs_get_resolution(vs_id);
		u16 width = (resolution >> 16) & 0xffff;
		u16 height = resolution & 0xffff;

		if(width == 0 || height == 0){
			width = YUV_WIDTH_DFT;
			height = YUV_HEIGHT_DFT;
		}

		_va_open(width, height, width, height);
		syslog(LOG_INFO, "[VA]: Video Analysis is started with (%d*%d)\n", width, height);
	}

	while(va_status == 1){
		if(vs_id >= 0){
			FRAME *frame = libvs_get_frame(vs_id, 20);
			if(frame == NULL){
				continue;
			}

#ifdef YUV_WRITE_SD
			int bytes;
    		bytes = fwrite(frame->addr, 1, frame->size, fp_yuv);
    		if(frame->size1){
    		    bytes += fwrite(frame->addr1, 1, frame->size1, fp_yuv);
    		}
    		debug_printf("YUV Write(0x%x:%d)\n", frame->addr, bytes);
#endif
			//debug_printf("VA: addr: %x-%x, size: %d-%d\n", frame->addr, frame->addr1, frame->size, frame->size1);
			_va_run( frame->addr, frame->addr1 );
			libvs_remove_frame(vs_id, frame);
		}
	}

	if(vs_id >= 0){
		libvs_stop(vs_id);
		libvs_exit(vs_id);	
		_va_close();
	}

#ifdef YUV_WRITE_SD
    fclose(fp_yuv);
    fp_yuv = NULL;
#endif

	va_status = 0;
	pthread_cond_signal(&_va_stop_cond);
}

#define VA_STACKSIZE 10240
static char __attribute__((aligned(8))) _va_stack[VA_STACKSIZE];

int video_analysis_start(t_video_analysis_cfg * cfg)
{
	// clear env
	memset(&_va_cfg, 0, sizeof(_va_cfg));

	// store args
	if(cfg != NULL){
		memcpy(&_va_cfg, cfg, sizeof(_va_cfg));
	}

	// create thread
	pthread_attr_t va_attr;

	pthread_attr_init(&va_attr);
	va_attr.schedparam.sched_priority = 17;
	if(_va_cfg.sched_priority > 0){
		va_attr.schedparam.sched_priority = _va_cfg.sched_priority;
	}
	pthread_attr_setstack(&va_attr, _va_stack, VA_STACKSIZE);
	int ret = pthread_create(&va_thread, &va_attr, _video_analysis_routine, NULL);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Create video analysis thread fail!\n");
		return -1;
	}

	return 0;
}

int video_analysis_stop(void)
{
	if(va_status == 0){
		return 0;
	}

	va_status = 2;

	if(pthread_self() != va_thread){
		//it's called from VA thread, so cannot wait
		pthread_cond_wait(&_va_stop_cond, NULL);
	}

	return 0;
}
