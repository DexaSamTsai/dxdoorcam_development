// Struct Parameter Tracking
#ifndef _STRUCT_PARAMETER_TRACKING_H_
#define _STRUCT_PARAMETER_TRACKING_H_

//==================================================================================================

typedef struct {
	unsigned short TitleX;
	unsigned short TitleY;
			  char Text[40];
} Struct_ViewName;

//==================================================================================================

typedef struct {
	struct {
		unsigned int CameraTamper               :1;
		unsigned int OpticalSelfDiagnostic      :1;
		unsigned int MotionDetection            :1;
		unsigned int ShadowRejection            :1;
		unsigned int SmallVibrationRejection    :1;
		unsigned int ObjectSizeFilter           :1;
		unsigned int ForbiddenZone              :1;
		unsigned int UninterestedZone			:1;
		unsigned int CrowdDetection				:1;
		unsigned int Trajectory					:1;
		unsigned int Tracking					:1;
		unsigned int Loitering					:1;
		unsigned int B12						:1;
		unsigned int B13						:1;
		unsigned int B14						:1;
		unsigned int B15						:1;
		unsigned int VehicleCounting			:1;
		unsigned int B17						:1;
		unsigned int VehicleSpeedMeasurement	:1;
		unsigned int ObjectCounting				:1;
		unsigned int ObjectSpeed				:1;
		unsigned int B21						:1;
		unsigned int B22						:1;
		unsigned int B23						:1;
		unsigned int B24						:1;
		unsigned int B25						:1;
		unsigned int B26						:1;
		unsigned int B27						:1;
		unsigned int B28						:1;
		unsigned int B29						:1;
		unsigned int B30						:1;
		unsigned int B31						:1;
	} Enable;
	struct {
		unsigned int CameraTamper				:1;
		unsigned int OpticalSelfDiagnostic		:1;
		unsigned int MotionDetection  			:1;
		unsigned int CameraTamper_Type			:1;
		unsigned int OpticalSelfDiagnostic_Type	:1;
		unsigned int ObjectSizeFilter			:1;
		unsigned int ForbiddenZone	    		:1;
		unsigned int UninterestedZone			:1;
		unsigned int CrowdDetection				:1;
		unsigned int Trajectory					:1;
		unsigned int Tracking					:1;
		unsigned int Loitering					:1;
		unsigned int Loitering_Count			:1;
		unsigned int B13						:1;
		unsigned int B14						:1;
		unsigned int B15						:1;
		unsigned int VehicleCounting			:1;
		unsigned int VehicleCounting_Total		:1;
		unsigned int VehicleSpeedMeasurement	:1;
		unsigned int ObjectCounting				:1;
		unsigned int ObjectSpeed				:1;
		unsigned int B21						:1;
		unsigned int B22						:1;
		unsigned int B23						:1;
		unsigned int B24						:1;
		unsigned int B25						:1;
		unsigned int B26						:1;
		unsigned int B27						:1;
		unsigned int B28						:1;
		unsigned int B29						:1;
		unsigned int B30						:1;
		unsigned int B31						:1;
	} Show;
	struct {
		unsigned int CameraTamper				:1;
		unsigned int OpticalSelfDiagnostic		:1;
		unsigned int MotionDetection			:1;
		unsigned int CameraTamper_Type			:1;
		unsigned int OpticalSelfDiagnostic_Type	:1;
		unsigned int ObjectSizeFilter			:1;
		unsigned int ForbiddenZone			    :1;
		unsigned int B07						:1;
		unsigned int CrowdDetection				:1;
		unsigned int Trajectory				    :1;
		unsigned int Tracking					:1;
		unsigned int Loitering					:1;
		unsigned int B12						:1;
		unsigned int B13						:1;
		unsigned int B14						:1;
		unsigned int B15						:1;
		unsigned int VehicleCounting			:1;
		unsigned int VehicleCounting_Total		:1;
		unsigned int VehicleSpeedMeasurement	:1;
		unsigned int ObjectCounting				:1;
		unsigned int ObjectSpeed				:1;
		unsigned int B21						:1;
		unsigned int B22						:1;
		unsigned int B23						:1;
		unsigned int B24						:1;
		unsigned int B25						:1;
		unsigned int B26						:1;
		unsigned int B27						:1;
		unsigned int B28						:1;
		unsigned int B29						:1;
		unsigned int B30						:1;
		unsigned int B31						:1;
	} Send;
} Struct_SystemControl;

//==================================================================================================

typedef struct {
	short          ObjMinSizeW;
	short          ObjMinSizeH;
	short          ObjMaxSizeW;
	short          ObjMaxSizeH;
	float          ObjEffectPixelRatio;

	int            Sensitivity_Automatic;
	int            Sensitivity_Tolerance_NIR;
	int            Sensitivity_Tolerance_Normal;
	unsigned char  Sensitivity[2][6];

	int            ObjUpdateTime;

	int            CameraTamper_Continuous;
	int            CameraTamper_WarningPeriod;
	int            CameraTamper_MinDuration;
	float          CameraTamper_MaxRealAreaRatio;
	float          CameraTamper_MaxRectAreaRatio;

	unsigned int   InsufficientCaptureBrightness_Sensitivity;
	unsigned int   ExcessiveCaptureBrightness_Sensitivity;
	float		   HighNoiseRatio_AreaRatio;
	unsigned int   ColorCast_Deviation;
	unsigned int   ScreenFreeze_Tolerance;
	unsigned int   ScreenFreeze_MinTime;
	float		   OutOfFocus_Threshold;

	unsigned int   NearInfrared_Sensitivity;
	float          NearInfrared_AreaRatio;

	float          ShadowRejection_MaxV;

	unsigned int   SmallVibrationRejection_Level;

	unsigned short ObjRectRangeX;
	unsigned short ObjRectRangeY;

	unsigned int   Trajectory_WarningPeriod;
} Struct_MotionDetection;

//==================================================================================================

#define MaxTrackingNumber 10

typedef struct {
	/*
	short   Tracking_ObjMinSizeW;
	short   Tracking_ObjMinSizeH;
	short   Tracking_ObjMaxSizeW;
	short   Tracking_ObjMaxSizeH;
	*/
	//short Tracking_ExpandSizeW;
	//short Tracking_ExpandSizeH;
	int     Tracking_MinExist;
} Struct_Tracking;

//==================================================================================================

#define MaxObjectSizeFilterNumber 4
#define MaxObjectSizeFilterPointNumber 2

typedef struct {
	unsigned int  MaxNumber;
	struct {
		unsigned char  Enable;
				 char  Text[15];
		unsigned short ObjFGSizeW;
		unsigned short ObjFGSizeH;
		unsigned short ObjBGSizeW;
		unsigned short ObjBGSizeH;
				 float ObjTolerance;
		//unsigned char Enable:1;
		//unsigned char PointNum:7;
		//unsigned int PointX[MaxObjectSizeFilterPointNumber];
		//unsigned int PointY[MaxObjectSizeFilterPointNumber];
	} SF[MaxObjectSizeFilterNumber];
} Struct_ObjectSizeFilter;

//==================================================================================================

#define MaxForbiddenZoneNumber 12
#define MaxForbiddenZonePointNumber 9

typedef struct {
	unsigned short MaxNumber;
	unsigned short MaxPointNumber;
	struct {
		unsigned char  Enable;
		unsigned char  PointNum;
		unsigned char  Type;
		unsigned char  Direction;
		unsigned int   UseFrame;
		unsigned short ObjMinSizeW;
		unsigned short ObjMinSizeH;
		unsigned short ObjMaxSizeW;
		unsigned short ObjMaxSizeH;
				 float ObjTolerance;
		unsigned int   PointX[MaxForbiddenZonePointNumber+1];
		unsigned int   PointY[MaxForbiddenZonePointNumber+1];
	} FZ[MaxForbiddenZoneNumber];
} Struct_ForbiddenZone;

//==================================================================================================

#define MaxUninterestedZoneNumber 4
#define MaxUninterestedZonePointNumber 9

typedef struct {
	unsigned short MaxNumber;
	unsigned short MaxPointNumber;
	struct {
		unsigned short Enable;
		unsigned short PointNum;
		unsigned int   PointX[MaxUninterestedZonePointNumber+1];
		unsigned int   PointY[MaxUninterestedZonePointNumber+1];
	} UZ[MaxUninterestedZoneNumber];
} Struct_UninterestedZone;

//==================================================================================================

#define MaxCrowdDetectionNumber 2
#define MaxCrowdDetectionPointNumber 9

typedef struct {
	unsigned char MaxNumber;
	unsigned char MaxPointNumber;
	struct {
		unsigned short Enable;
		unsigned short PointNum;
		unsigned short ResultX;
		unsigned short ResultY;
		unsigned int PointX[MaxCrowdDetectionPointNumber+1];
		unsigned int PointY[MaxCrowdDetectionPointNumber+1];
	} CD[MaxCrowdDetectionNumber];
} Struct_CrowdDetection;

//==================================================================================================

#define MaxLoiteringNumber 2

typedef struct {
	char  MaxNumber;
	char  Full_Enable;
	short Full_Time;
	struct {
		       char  Enable;
		signed char  Rule;
		       short Time;
		//int FZWarn;
	} LT[MaxLoiteringNumber];
} Struct_Loitering;

//==================================================================================================

#define MaxObjectCountingNumber 6

typedef struct  {
	unsigned int MaxNumber;
	struct {
		short Enable;
		short Rule;
		short SplitX;
		short SplitY;
		short ShowX;
		short ShowY;
		char  Title[16];
	} OC[MaxObjectCountingNumber];
} Struct_ObjectCounting;

//==================================================================================================

#define MaxObjectSpeedNumber 6

typedef struct  {
	unsigned int MaxNumber;
	float		 FPS;
	struct {
		         short Enable;
		  signed char  Rule1;
		  signed char  Rule2;
		         int   Distance;
		unsigned char  LowestLimit;
		unsigned char  LowerLimit;
		unsigned char  HigherLimit;
		unsigned char  HighestLimit;
	} OS[MaxObjectSpeedNumber];
} Struct_ObjectSpeed;

//==================================================================================================

typedef struct {

		Struct_ViewName         Para_ViewName;
		Struct_SystemControl    Para_SystemControl;
		Struct_MotionDetection  Para_MotionDetection;
		Struct_Tracking         Para_Tracking;
		Struct_ObjectSizeFilter Para_ObjectSizeFilter;
		Struct_ForbiddenZone    Para_ForbiddenZone;
		Struct_UninterestedZone Para_UninterestedZone;
		Struct_CrowdDetection   Para_CrowdDetection;
		Struct_Loitering        Para_Loitering;
		Struct_ObjectCounting   Para_ObjectCounting;
		Struct_ObjectSpeed      Para_ObjectSpeed;

} Struct_Parameter_Tracking;

//==================================================================================================

#endif /* _STRUCT_PARAMETER_TRACKING_H_ */
