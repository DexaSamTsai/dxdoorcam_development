// Struct Metadata Tracking
#ifndef _STRUCT_METADATA_TRACKING_H_
#define _STRUCT_METADATA_TRACKING_H_

//==================================================================================================

typedef struct {

	unsigned char  DetectCT_Count;
	unsigned char  DetectCT_Type;	// 0: Normal Status, 1: Blind Detection, 2: Scene Change, 3: Sudden Lighting Change, 4: Light On, 5: Light Off
	unsigned char  DetectOD_Count;
	unsigned char  DetectOD_Type;	// 0: Normal Capture Brightness, 1: Insufficient Capture Brightness, 2: Excessive Capture Brightness, 3: Signal Loss, 4: High Noise Ratio, 5: Color Cast, 6: Screen Freeze, 7: Out of Focus
	unsigned char  DetectCD[4];
	unsigned int   DetectFZ;		// 0: No Detected Forbidden Zone, 1: Detected Forbidden Zone

	unsigned int   DetectLT_Full;	// The Result of Loitering for Full Scene
	unsigned int   DetectLT_Area[2];// The Result of Loitering
	unsigned int   DetectLT;		// The Result of Loitering

	unsigned int   Obj_Num;
	         char  Obj_Used[20];
	         char  Obj_Type[20];	// -1: None, 0: Obj0, 1: Obj1, 2: Obj2, 3: Obj3
	unsigned int   Obj_ID  [20];	// The Unique Number of Detected Object. The Number is Cumulative.
	unsigned short Obj_cxLT[20];
	unsigned short Obj_cyLT[20];
	unsigned short Obj_cxRB[20];
	unsigned short Obj_cyRB[20];

	unsigned int   Counting[8];		// The Result of Object Counting
	unsigned int   Count_TK[8];		// 0: No Detected Counting, 1: Detected Counting

	unsigned int   DetectOS;		// 0: No Detected Object Speed, 1: Detected Object Speed
	unsigned char  Speed[20];		// The Result of Object Speed (Unit: km/h)

} Struct_Metadata_Tracking;

//==================================================================================================

#endif /* _STRUCT_METADATA_TRACKING_H_ */
