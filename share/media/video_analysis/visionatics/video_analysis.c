#include "includes.h"
#include "Visionatics_MotionTracking.h"
#include "media/va/video_analysis.h"

#define EFS_BUFFER  (MEMBASE_DDR + 0xa00000)
#define EFS_BUFFER_SIZE   (1024*1024)

/* Enable either one or both */
#define VA_LOITERING_TEST
//#define VA_HUMAN_MOTION

//#define YUV_WRITE_SD        //Config this if need to store YUV to SD

static t_video_analysis_cfg _va_cfg;

static u32 _va_stop = 0;
static pthread_cond_t _va_stop_cond = PTHREAD_COND_INITIALIZER;

static void _va_open(int wc, int hc, int ws, int hs);
static void _va_close(void);
static void _va_run( unsigned char *buf_Y, unsigned char *buf_UV );
static void _va_para_init( Struct_Parameter_Tracking *para );

static void * _video_analysis_routine(void* arg)
{
	pthread_detach(pthread_self());
	pthread_setname(NULL, "VideoAnalysis");

#ifdef YUV_WRITE_SD
    efs_init(EFS_HW_SCIF, 0, EFS_BUFFER, EFS_BUFFER_SIZE);
    debug_printf("  SD card capacity: %d sectors\n", sh_efs.sectors);
    debug_printf("  Filesystem sectors per Cluster: %d\n", sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem sectors count: %d, cluster count:%d\n", sh_efs.sectors, sh_efs.sectors/sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem Free Clusters: %d\n", sh_efs.clusters_free);
    debug_printf("  Filesystem first Free Cluster: %d\n", sh_efs.next_free_cluster);

	u8 g_file_name[30];
    sprintf((char *)g_file_name, "vs%dtest.txt", 0);

    FILE * fp_yuv = fopen(g_file_name, "w");
    if(fp_yuv == NULL){
        debug_printf("open file %s failed\n", g_file_name);
    }else{
        debug_printf("open file %s\n", g_file_name);
    }
#endif

	u32 ret = 0;
	u32 vs_id = _va_cfg.vs_id;

	ret = libvs_init(vs_id);
	ret |= libvs_start(vs_id);

	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: VA start vs %d fail.\n", vs_id);
		return;
	}

	u32 resolution = libvs_get_resolution(vs_id);
	u16 width = (resolution >> 16) & 0xffff;
	u16 height = resolution & 0xffff;

	_va_open(width, height, width, height);
	syslog(LOG_INFO, "Video Analysis is started with (%d*%d)\n", width, height);

	while(!_va_stop){
		FRAME *frame = libvs_get_frame(vs_id, 20);
		if(frame == NULL){
			continue;
		}

#ifdef YUV_WRITE_SD
		int bytes;
    	bytes = fwrite(frame->addr, 1, frame->size, fp_yuv);
    	if(frame->size1){
    	    bytes += fwrite(frame->addr1, 1, frame->size1, fp_yuv);
    	}
    	debug_printf("YUV Write(0x%x:%d)\n", frame->addr, bytes);
#endif
		//debug_printf("VA: addr: %x-%x, size: %d-%d\n", frame->addr, frame->addr1, frame->size, frame->size1);
		_va_run( frame->addr, frame->addr1 );
		libvs_remove_frame(vs_id, frame);
	}

	libvs_stop(vs_id);
	libvs_exit(vs_id);	
	_va_close();

#ifdef YUV_WRITE_SD
    fclose(fp_yuv);
    fp_yuv = NULL;
#endif


	pthread_cond_signal(&_va_stop_cond);
}

#define VA_STACKSIZE 10240
static char __attribute__((aligned(8))) _va_stack[VA_STACKSIZE];

int video_analysis_start(t_video_analysis_cfg * cfg)
{
	// clear env
	memset(&_va_cfg, 0, sizeof(_va_cfg));

	// store args
	if(cfg != NULL){
		memcpy(&_va_cfg, cfg, sizeof(_va_cfg));
	}

	// create thread
	pthread_t va_thread;
	pthread_attr_t va_attr;

	pthread_attr_init(&va_attr);
	pthread_attr_setstack(&va_attr, _va_stack, VA_STACKSIZE);
	int ret = pthread_create(&va_thread, &va_attr, _video_analysis_routine, NULL);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Create video analysis thread fail!\n");
		return -1;
	}

	return 0;
}

int video_analysis_stop(void)
{
	if(_va_stop == 1){
		return 0;
	}

	_va_stop = 1;
	pthread_cond_wait(&_va_stop_cond, NULL);
	_va_stop = 0;

	return 0;
}

static void _va_para_init( Struct_Parameter_Tracking *para )
{
	// Clear Parameter
	memset( para, 0, sizeof( Struct_Parameter_Tracking ) );



	// View Name
	strcpy( para->Para_ViewName.Text, "Visionatics" );
	para->Para_ViewName.TitleX = 10;
	para->Para_ViewName.TitleY = 10;



	//// EM-Lite
	// System Control
	para->Para_SystemControl.Enable.MotionDetection            = 1;
	para->Para_SystemControl.Show  .MotionDetection            = 1;
	para->Para_SystemControl.Send  .MotionDetection            = 1;
	para->Para_SystemControl.Enable.CameraTamper               = 1;
	para->Para_SystemControl.Show  .CameraTamper               = 1;
	para->Para_SystemControl.Send  .CameraTamper               = 1;
	para->Para_SystemControl.Show  .CameraTamper_Type          = 1;
	para->Para_SystemControl.Send  .CameraTamper_Type          = 1;
	para->Para_SystemControl.Enable.OpticalSelfDiagnostic      = 1;
	para->Para_SystemControl.Show  .OpticalSelfDiagnostic      = 1;
	para->Para_SystemControl.Send  .OpticalSelfDiagnostic      = 1;
	para->Para_SystemControl.Show  .OpticalSelfDiagnostic_Type = 1;
	para->Para_SystemControl.Send  .OpticalSelfDiagnostic_Type = 1;

#if defined(VA_LOITERING_TEST) && defined(VA_HUMAN_MOTION)

	// Min/Max Detected Object Size
	para->Para_MotionDetection.ObjMinSizeW = 16;
	para->Para_MotionDetection.ObjMinSizeH = 9;
	para->Para_MotionDetection.ObjMaxSizeW = 320;
	para->Para_MotionDetection.ObjMaxSizeH = 180;
	para->Para_MotionDetection.ObjEffectPixelRatio = 0.3f;

	// Background Model Updating
	para->Para_MotionDetection.ObjUpdateTime = 100;

	// Merge Rectangle
	para->Para_MotionDetection.ObjRectRangeX = 16;
	para->Para_MotionDetection.ObjRectRangeY = 40;

	// Automatically Adjust Sensitivity Parameter for Motion Detection
	para->Para_MotionDetection.Sensitivity_Automatic        = 1;
	para->Para_MotionDetection.Sensitivity_Tolerance_NIR    = 0;
	para->Para_MotionDetection.Sensitivity_Tolerance_Normal = -5;

#elif defined(VA_LOITERING_TEST)

	// for General Setting
	// Min/Max Detected Object Size
	para->Para_MotionDetection.ObjMinSizeW = 16;
	para->Para_MotionDetection.ObjMinSizeH = 9;
	para->Para_MotionDetection.ObjMaxSizeW = 320;
	para->Para_MotionDetection.ObjMaxSizeH = 180;
	para->Para_MotionDetection.ObjEffectPixelRatio = 0.3f;

	// Background Model Updating
	para->Para_MotionDetection.ObjUpdateTime = 100;

	// Merge Rectangle
	para->Para_MotionDetection.ObjRectRangeX = 16;
	para->Para_MotionDetection.ObjRectRangeY = 40;

	// Automatically Adjust Sensitivity Parameter for Motion Detection
	para->Para_MotionDetection.Sensitivity_Automatic        = 1;
	para->Para_MotionDetection.Sensitivity_Tolerance_NIR    = 0;
	para->Para_MotionDetection.Sensitivity_Tolerance_Normal = 0;

#elif defined(VA_HUMAN_MOTION)

	// for Object Size Filter - People
	// Min/Max Detected Object Size
	para->Para_MotionDetection.ObjMinSizeW = 16;
	para->Para_MotionDetection.ObjMinSizeH = 80;
	para->Para_MotionDetection.ObjMaxSizeW = 120;
	para->Para_MotionDetection.ObjMaxSizeH = 180;
	para->Para_MotionDetection.ObjEffectPixelRatio = 0.3f;

	// Background Model Updating
	para->Para_MotionDetection.ObjUpdateTime = 30;

	// Merge Rectangle
	para->Para_MotionDetection.ObjRectRangeX = 16;
	para->Para_MotionDetection.ObjRectRangeY = 40;

	// Automatically Adjust Sensitivity Parameter for Motion Detection
	para->Para_MotionDetection.Sensitivity_Automatic        = 1;
	para->Para_MotionDetection.Sensitivity_Tolerance_NIR    = 0;
	para->Para_MotionDetection.Sensitivity_Tolerance_Normal = -5;

#endif /* VA_LOITERING_TEST && VA_HUMAN_MOTION */

	// Motion Detection Sensitivity at Near Infrared or Normal Mode
	// Motion Detection Sensitivity at NIR Mode
	para->Para_MotionDetection.Sensitivity[0][0] = 10;
	para->Para_MotionDetection.Sensitivity[0][1] = 30;
	para->Para_MotionDetection.Sensitivity[0][2] = 10;
	para->Para_MotionDetection.Sensitivity[0][3] = 30;
	para->Para_MotionDetection.Sensitivity[0][4] = 10;
	para->Para_MotionDetection.Sensitivity[0][5] = 30;

	// Motion Detection Sensitivity at Normal Lighting Mode
	para->Para_MotionDetection.Sensitivity[1][0] = 10;
	para->Para_MotionDetection.Sensitivity[1][1] = 50;
	para->Para_MotionDetection.Sensitivity[1][2] = 10;
	para->Para_MotionDetection.Sensitivity[1][3] = 50;
	para->Para_MotionDetection.Sensitivity[1][4] = 10;
	para->Para_MotionDetection.Sensitivity[1][5] = 50;

	// Enable/Disable Near Infrared (NIR) Mode
	para->Para_MotionDetection.NearInfrared_Sensitivity = 3;
	para->Para_MotionDetection.NearInfrared_AreaRatio   = 0.7f;

	// Camera Tamper Detection
	para->Para_MotionDetection.CameraTamper_Continuous       = 0;
	para->Para_MotionDetection.CameraTamper_WarningPeriod    = 30;
	para->Para_MotionDetection.CameraTamper_MinDuration      = 0;
	para->Para_MotionDetection.CameraTamper_MaxRealAreaRatio = 0.6f;
	para->Para_MotionDetection.CameraTamper_MaxRectAreaRatio = 0.8f;

	// Insufficient Capture Brightness
	para->Para_MotionDetection.InsufficientCaptureBrightness_Sensitivity = 40;

	// Excessive Capture Brightness
	para->Para_MotionDetection.ExcessiveCaptureBrightness_Sensitivity    = 208;

	// High Noise Ratio
	para->Para_MotionDetection.HighNoiseRatio_AreaRatio = 0.6f;

	// Out Of Focus
	para->Para_MotionDetection.OutOfFocus_Threshold = 1.0f;



	//// EM-Ext. 2.5
	// System Control
	para->Para_SystemControl.Enable.ShadowRejection         = 1;
	para->Para_SystemControl.Enable.SmallVibrationRejection = 1;
	para->Para_SystemControl.Enable.ObjectSizeFilter        = 1;
	para->Para_SystemControl.Show  .ObjectSizeFilter        = 1;
	para->Para_SystemControl.Send  .ObjectSizeFilter        = 1;
	para->Para_SystemControl.Enable.ForbiddenZone           = 1;
	para->Para_SystemControl.Show  .ForbiddenZone           = 1;
	para->Para_SystemControl.Send  .ForbiddenZone           = 1;
	para->Para_SystemControl.Enable.UninterestedZone        = 1;
	para->Para_SystemControl.Show  .UninterestedZone        = 1;
	para->Para_SystemControl.Enable.CrowdDetection          = 1;
	para->Para_SystemControl.Show  .CrowdDetection          = 1;
	para->Para_SystemControl.Send  .CrowdDetection          = 1;

	// Shadow Rejection
	para->Para_MotionDetection.ShadowRejection_MaxV = 0.25f;

	// Small Vibration Rejection
	para->Para_MotionDetection.SmallVibrationRejection_Level = 1;

#ifdef VA_HUMAN_MOTION
	// Object Size Filter
	// for Object Size Filter - People
	para->Para_ObjectSizeFilter.SF[0].Enable = 1;
	strcpy( para->Para_ObjectSizeFilter.SF[0].Text, "People" );
	para->Para_ObjectSizeFilter.SF[0].ObjFGSizeW = 60;
	para->Para_ObjectSizeFilter.SF[0].ObjFGSizeH = 160;
	para->Para_ObjectSizeFilter.SF[0].ObjBGSizeW = 45;
	para->Para_ObjectSizeFilter.SF[0].ObjBGSizeH = 120;

#ifdef VA_LOITERING_TEST
	para->Para_ObjectSizeFilter.SF[0].ObjTolerance = 0.2f;
#else
	para->Para_ObjectSizeFilter.SF[0].ObjTolerance = 0.3f;
#endif /* VA_LOITERING_TEST */

	para->Para_ObjectSizeFilter.SF[1].Enable = 1;
	strcpy( para->Para_ObjectSizeFilter.SF[1].Text, "People" );
	para->Para_ObjectSizeFilter.SF[1].ObjFGSizeW = 24;
	para->Para_ObjectSizeFilter.SF[1].ObjFGSizeH = 110;
	para->Para_ObjectSizeFilter.SF[1].ObjBGSizeW = 18;
	para->Para_ObjectSizeFilter.SF[1].ObjBGSizeH = 80;
#ifdef VA_LOITERING_TEST
	para->Para_ObjectSizeFilter.SF[1].ObjTolerance = 0.2f;
#else
	para->Para_ObjectSizeFilter.SF[1].ObjTolerance = 0.3f;
#endif /* VA_LOITERING_TEST */
#endif /* VA_HUMAN_MOTION */

	// Forbidden Zone
	para->Para_ForbiddenZone.FZ[0].Enable      = 1;
	para->Para_ForbiddenZone.FZ[0].Type        = 2;
	para->Para_ForbiddenZone.FZ[0].Direction   = 0;
	para->Para_ForbiddenZone.FZ[0].ObjMinSizeW = 16;
	para->Para_ForbiddenZone.FZ[0].ObjMinSizeH = 9;
	para->Para_ForbiddenZone.FZ[0].ObjMaxSizeW = 320;
	para->Para_ForbiddenZone.FZ[0].ObjMaxSizeH = 180;
	para->Para_ForbiddenZone.FZ[0].ObjTolerance = 0.0f;

	para->Para_ForbiddenZone.FZ[0].PointNum  = 4;
	para->Para_ForbiddenZone.FZ[0].PointX[0] = 80;
	para->Para_ForbiddenZone.FZ[0].PointY[0] = 100;
	para->Para_ForbiddenZone.FZ[0].PointX[1] = 240;
	para->Para_ForbiddenZone.FZ[0].PointY[1] = 100;
	para->Para_ForbiddenZone.FZ[0].PointX[2] = 240;
	para->Para_ForbiddenZone.FZ[0].PointY[2] = 180;
	para->Para_ForbiddenZone.FZ[0].PointX[3] = 80;
	para->Para_ForbiddenZone.FZ[0].PointY[3] = 180;

	// Uninterested Zone
	/*
	para->Para_UninterestedZone.UZ[0].Enable    = 1;

	para->Para_UninterestedZone.UZ[0].PointNum  = 4;
	para->Para_UninterestedZone.UZ[0].PointX[0] = 170;
	para->Para_UninterestedZone.UZ[0].PointY[0] = 10;
	para->Para_UninterestedZone.UZ[0].PointX[1] = 310;
	para->Para_UninterestedZone.UZ[0].PointY[1] = 10;
	para->Para_UninterestedZone.UZ[0].PointX[2] = 310;
	para->Para_UninterestedZone.UZ[0].PointY[2] = 80;
	para->Para_UninterestedZone.UZ[0].PointX[3] = 170;
	para->Para_UninterestedZone.UZ[0].PointY[3] = 80;
	*/

	// Crowd Detection
	/*
	para->Para_CrowdDetection.CD[0].Enable  = 1;
	para->Para_CrowdDetection.CD[0].ResultX = 12;
	para->Para_CrowdDetection.CD[0].ResultY = 102;

	para->Para_CrowdDetection.CD[0].PointNum  = 4;
	para->Para_CrowdDetection.CD[0].PointX[0] = 10;
	para->Para_CrowdDetection.CD[0].PointY[0] = 100;
	para->Para_CrowdDetection.CD[0].PointX[1] = 310;
	para->Para_CrowdDetection.CD[0].PointY[1] = 100;
	para->Para_CrowdDetection.CD[0].PointX[2] = 310;
	para->Para_CrowdDetection.CD[0].PointY[2] = 170;
	para->Para_CrowdDetection.CD[0].PointX[3] = 10;
	para->Para_CrowdDetection.CD[0].PointY[3] = 170;
	*/



	//// EM-Ext. 3.0
	// System Control
	para->Para_SystemControl.Enable.Tracking  = 1;
	para->Para_SystemControl.Show  .Tracking  = 1;
	para->Para_SystemControl.Send  .Tracking  = 1;

#ifdef VA_LOITERING_TEST
	para->Para_SystemControl.Enable.Loitering = 1;
	para->Para_SystemControl.Show  .Loitering = 1;
	para->Para_SystemControl.Send  .Loitering = 1;

	// Tracking
	para->Para_Tracking.Tracking_MinExist = 10;

	// Loitering
	para->Para_Loitering.Full_Enable = 0;
	para->Para_Loitering.Full_Time   = 300;

	para->Para_Loitering.LT[0].Enable = 1;
	para->Para_Loitering.LT[0].Rule   = 0;	// Forbidden Zone Set No.: 0~11
	para->Para_Loitering.LT[0].Time   = 45;
#endif /* VA_LOITERING_TEST */
}

//FIXME: only for test
//#define PARA_UPDATE_FROMENV

#ifdef PARA_UPDATE_FROMENV
static double _atof(const char * str)
{
	assert(str != NULL);

	char * ptr = str;

	// skip space
	while(isspace(*ptr)){
		ptr++;
	}

	int sign = 1;
	// check sign
	if(*ptr == '-'){
		sign = -1;
		ptr++;
	}else if(*ptr == '+'){
		ptr++;
	}

	double val = 0;

	// val before point
	while(*ptr != '\0' && *ptr != '.'){
		val = val * 10 + (*ptr - '0');
		ptr++;
	}
	ptr++;

	// val after point
	double temp = 10.0;
	while(*ptr != '\0'){
		val = val + (*ptr - '0')/temp;
		temp = temp * 10;
		ptr++;
	} 

	if(sign == -1){
		val = val * -1;
	}

	return val;
}

#define _update_para(para,type,is_float,env_str)				\
			do{													\
 				char * str =  getenv(env_str);					\
				if(str == NULL){								\
					break;										\
				}												\
				if(is_float == 1){								\
					type val = _atof(str);						\
					para = val;									\
					debug_printf("[Para] "env_str" to %f\n", para);	\
				}else{											\
					type val = atoi(str);						\
					para = val;									\
					debug_printf("[Para] "env_str" to %d\n", para);	\
				}												\
			}while(0)											
											 
static void _va_para_update_fromenv( Struct_Parameter_Tracking *para )
{

    // Min/Max Detected Object Size
	_update_para(para->Para_MotionDetection.ObjEffectPixelRatio, float, 1, "ObjEffectPixelRatio");

    // Background Model Updating
	_update_para(para->Para_MotionDetection.ObjUpdateTime, int, 0, "ObjUpdateTime");

    // Camera Tamper Detection
    _update_para(para->Para_MotionDetection.CameraTamper_WarningPeriod, int, 0, "CameraTamper_WarningPeriod");
    _update_para(para->Para_MotionDetection.CameraTamper_MaxRealAreaRatio, float, 1, "CameraTamper_MaxRealAreaRatio");
    _update_para(para->Para_MotionDetection.CameraTamper_MaxRectAreaRatio, float, 1, "CameraTamper_MaxRectAreaRatio");

    // Insufficient Capture Brightness
    _update_para(para->Para_MotionDetection.InsufficientCaptureBrightness_Sensitivity, unsigned int, 0, "InsufficientCaptureBrightness_Sensitivity"); 
    
    // Excessive Capture Brightness
    _update_para(para->Para_MotionDetection.ExcessiveCaptureBrightness_Sensitivity, unsigned int, 0, "ExcessiveCaptureBrightness_Sensitivity");
    
    // High Noise Ratio
    _update_para(para->Para_MotionDetection.HighNoiseRatio_AreaRatio, float, 1, "HighNoiseRatio_AreaRatio");

    // Motion Detection Sensitivity at Normal Lighting Mode
    _update_para(para->Para_MotionDetection.Sensitivity[1][0], unsigned char, 0, "Sensitivity[1][0]");
    _update_para(para->Para_MotionDetection.Sensitivity[1][1], unsigned char, 0, "Sensitivity[1][1]");
    _update_para(para->Para_MotionDetection.Sensitivity[1][2], unsigned char, 0, "Sensitivity[1][2]");
    _update_para(para->Para_MotionDetection.Sensitivity[1][3], unsigned char, 0, "Sensitivity[1][3]");
    _update_para(para->Para_MotionDetection.Sensitivity[1][4], unsigned char, 0, "Sensitivity[1][4]");
    _update_para(para->Para_MotionDetection.Sensitivity[1][5], unsigned char, 0, "Sensitivity[1][5]");
}
#endif

static void _va_open( int wc, int hc, int ws, int hs )
{	
	Struct_BufferSize bufsize;
	bufsize.BufW_Buffer    = wc;  bufsize.BufH_Buffer    = hc;
	bufsize.BufW_Display   = wc;  bufsize.BufH_Display   = hc;
	bufsize.BufW_Analysis  = wc;  bufsize.BufH_Analysis  = hc;
	bufsize.BufW_Streaming = ws;  bufsize.BufH_Streaming = hs;
	
	Struct_Parameter_Tracking para;
	_va_para_init( &para );

#ifdef PARA_UPDATE_FROMENV
	_va_para_update_fromenv(&para);
#endif
	
	VideoAnalysis_Open( 0, &bufsize, &para );
}

static void _va_close(void)
{	
	VideoAnalysis_Close( 0 );	
}

//TODO: use string for result later
/*
static u8 * _detect_CT_str[] = {
	"Normal Status", 
	"Blind Detection", 
	"Scene Change", 
	"Sudden Lighting Change", 
	"Light On", 
	"Light Off",
};

static u8 * _detect_OD_str[] = {
	"Normal Capture Brightness",
	"Insufficient Capture Brightness",
	"Excessive Capture Brightness", 
	"Signal Loss", 
	"High Noise Ratio", 
	"Color Cast", 
	"Screen Freeze", 
	"Out of Focus",
};

static u8 * _detect_result = "Normal";
*/
static Struct_Metadata_Tracking _meta_data;
static void _va_run( unsigned char *buf_Y, unsigned char *buf_UV )
{
	VideoAnalysis_Run( 0, buf_Y, buf_UV );
	VideoAnalysis_ShowResult( 0, buf_Y, buf_UV );
	
	GetMetadata( 0, &_meta_data);

	if(_va_cfg.run_cb != NULL){
		_va_cfg.run_cb();
	}

	//FIXME: is this the result??
#if 0 
	debug_printf("/////////////////////\n");
	debug_printf("DetectCT_Count %d Type %d DetectOD_Count %d Type %d\n", meta.DetectCT_Count, meta.DetectCT_Type, meta.DetectOD_Count, meta.DetectOD_Type);
	debug_printf("Obj_Num: %d\n", meta.Obj_Num);
	int i;
	for(i = 0; i< meta.Obj_Num; i++){
		debug_printf("Used %d Type %d ID %d %d-%d-%d-%d\n", meta.Obj_Used[i], meta.Obj_Type[i],meta.Obj_ID[i], meta.Obj_cxLT[i], meta.Obj_cyLT[i], meta.Obj_cxRB[i], meta.Obj_cyRB[i]);
	}

	debug_printf("DetectOS: %d Speed %s\n", meta.DetectOS, meta.Speed);
	debug_printf("/////////////////////\n");
#endif
}

int video_analysis_result(t_video_analysis_res * res)
{
	if(res == NULL){
		return -1;
	}

	int num = 0;
	int i;

#ifdef VA_LOITERING_TEST
	//FIXME: result for loitering as below
	//FIXME: many hardcode, only for temp demo purpose.

	// ForbiddenZone Rect
	res->obj_type[0] = -1;
	res->obj_left[0] = 80;
	res->obj_top[0] = 100;
	res->obj_right[0] = 240;
	res->obj_bottom[0] = 180;
	num = 1;

	if(_meta_data.DetectLT_Area[0] != 0){
		/*
		debug_printf("[COOLEN]: Loitering Detected!!\n");

		int i;
		debug_printf("DetectFZ 0x%x, Full 0x%x, DetectLT_Area 0x%x-0x%x, LT 0x%x\n", _meta_data.DetectFZ, _meta_data.DetectLT_Full, _meta_data.DetectLT_Area[0], _meta_data.DetectLT_Area[1], _meta_data.DetectLT);
		debug_printf("Obj_Num: %d\n", _meta_data.Obj_Num);
		for(i = 0; i< 10; i++){
			if(_meta_data.Obj_Used[i] != 0){
				debug_printf("i %d Used %d Type %d ID %d %d-%d-%d-%d\n", i, _meta_data.Obj_Used[i], _meta_data.Obj_Type[i],_meta_data.Obj_ID[i], _meta_data.Obj_cxLT[i], _meta_data.Obj_cyLT[i], _meta_data.Obj_cxRB[i], _meta_data.Obj_cyRB[i]);
			}
		}
		*/

		for(i = 0; i < 10; i++){
			if((_meta_data.DetectLT_Area[0] & (0x1 << i)) == 0 ){
				continue;
			}
			res->obj_type[num] = _meta_data.Obj_Type[i];
			res->obj_left[num] = _meta_data.Obj_cxLT[i];
			res->obj_top[num] = _meta_data.Obj_cyLT[i];
			res->obj_right[num] = _meta_data.Obj_cxRB[i];
			res->obj_bottom[num] = _meta_data.Obj_cyRB[i];
			num++;
		}
	}
#endif /* VA_LOITERING_TEST */

#ifdef VA_HUMAN_MOTION
	//FIXME: result for human detection as below
	for(i = 0; i < _meta_data.Obj_Num; i++){
		if(_meta_data.Obj_Type[i] != 0 && _meta_data.Obj_Type[i] != 1){
			continue;
		}

		//FIXME: Visionatic sometimes report human object without coordinate information. (all zero)
		//       Skip this case to avoid it pass to OSD update but draw nothing.
		if(_meta_data.Obj_cxLT[i] == 0 && _meta_data.Obj_cyLT[i] == 0 &&
		   _meta_data.Obj_cxRB[i] == 0 && _meta_data.Obj_cyRB[i] == 0 ){
			continue;
		}

		res->obj_type[num] = _meta_data.Obj_Type[i];
		res->obj_left[num] = _meta_data.Obj_cxLT[i];
		res->obj_top[num] = _meta_data.Obj_cyLT[i];
		res->obj_right[num] = _meta_data.Obj_cxRB[i];
		res->obj_bottom[num] = _meta_data.Obj_cyRB[i];
		num++;
	}
#endif /* VA_HUMAN_MOTION */

	res->obj_num = num;
	return 0;
}
