// Visionatics_Motion Tracking
#ifndef _VISIONATICS_MOTION_TRACKING_H_
#define _VISIONATICS_MOTION_TRACKING_H_

//==================================================================================================

// Parameter Struct
#include "Visionatics_Struct_Parameter.h"
// Metadata Struct
#include "Visionatics_Struct_Metadata.h"

//==================================================================================================

typedef struct {
	unsigned int BufW_Buffer;
	unsigned int BufH_Buffer;
	unsigned int BufW_Display;
	unsigned int BufH_Display;
	unsigned int BufW_Analysis;
	unsigned int BufH_Analysis;
	unsigned int BufW_Streaming;
	unsigned int BufH_Streaming;
} Struct_BufferSize;

//==================================================================================================

#ifndef WIN32
#define __declspec(dllexport)
#endif

__declspec(dllexport) void VideoAnalysis_SetDefaultParameterFilePath( int Channel, char *FilePath );
__declspec(dllexport) int  VideoAnalysis_Open      ( int Channel, Struct_BufferSize *BufSize, void *pPara );
__declspec(dllexport) int  VideoAnalysis_Close     ( int Channel );
__declspec(dllexport) int  VideoAnalysis_Run       ( int Channel, unsigned char *pBuf, unsigned char *pUVBuf );
__declspec(dllexport) int  VideoAnalysis_ShowResult( int Channel, unsigned char *pBuf, unsigned char *pUVBuf );

__declspec(dllexport) int  VideoAnalysis_Tracking_LoadParameter( char *FilePath, Struct_Parameter_Tracking *Para );
__declspec(dllexport) int  VideoAnalysis_Tracking_SaveParameter( char *FilePath, Struct_Parameter_Tracking *Para );
__declspec(dllexport) int  VideoAnalysis_Reset     ( int Channel );
__declspec(dllexport) int  VideoAnalysis_PreProcess( int Channel );

__declspec(dllexport) void Calibration_Initial          ( int Channel, Struct_Parameter_Tracking *Para );
__declspec(dllexport) void Calibration_Run_Struct_to_Lib( int Channel, Struct_Parameter_Tracking *Para );
__declspec(dllexport) void Calibration_Run_Lib_to_Struct( int Channel, Struct_Parameter_Tracking *Para );

__declspec(dllexport) int  GetMetadata( int Channel, Struct_Metadata_Tracking *Metadata );

__declspec(dllexport) void VideoAnalysis_CameraTamper_Unlock     ( int Channel );
__declspec(dllexport) void VideoAnalysis_NearInfrared_Get_NIRMode( int Channel, int *Value );	// 0: Near Infrared Mode, 1: Normal Lighting Mode
__declspec(dllexport) void VideoAnalysis_Tracking_Set_UniqueID   ( int Channel, int  Value );
__declspec(dllexport) void VideoAnalysis_Tracking_Get_UniqueID   ( int Channel, int *Value );
__declspec(dllexport) void VideoAnalysis_ObjectCounting_Reset    ( int Channel );
__declspec(dllexport) void VideoAnalysis_ObjectCounting_Set      ( int Channel, int Index, int Value );

//==================================================================================================

#endif /* _VISIONATICS_MOTION_TRACKING_H_ */
