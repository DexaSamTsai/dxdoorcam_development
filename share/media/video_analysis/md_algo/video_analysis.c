#include "includes.h"
#include "media/va/video_analysis.h"
#include "MD_API.h"

#define EFS_BUFFER  (MEMBASE_DDR + 0xa00000)
#define EFS_BUFFER_SIZE   (1024*1024)

//#define USE_YUV_STREAM
//#define YUV_WRITE_SD        //Config this if need to store YUV to SD

#define MD_GET_BUF_TIMEOUT (10)

static MDInt_Outputs _md_output;
static t_video_analysis_cfg _va_cfg;

static u32 va_status = 0; //0:IDLE, 1:running, 2:to_stop
static pthread_cond_t _va_stop_cond = PTHREAD_COND_INITIALIZER;
static pthread_t va_thread = NULL;
extern t_ertos_eventhandler mdbuf_notempty_evt;
u32 va_stat_ms = 0;

#ifdef USE_YUV_STREAM
static void _va_open( int wc, int hc, int ws, int hs )
{
	//TODO: init the VA
}

static void _va_close(void)
{
	//TODO: release the VA
}

static void _va_run( unsigned char *buf_Y, unsigned char *buf_UV )
{
	//TODO: VA parse one frame

	if(_va_cfg.run_cb != NULL){
		_va_cfg.run_cb();
	}
}
#endif

static void _va_open_md_algo(MDInt_Inputs * mdInput, u32 addr,u32 addr_prev, u32 width, u32 height )
{
	mdInput->SourceIn.iImgWidth = width;
	mdInput->SourceIn.iImgHeight = height;
	mdInput->SourceIn.iImgWStep = width * 2;
	MDInt_Set(MD_SRCIN,mdInput);

	mdInput->PackedSourceIn.pPrevPackedImg = addr_prev;
	mdInput->PackedSourceIn.pPackedImg = addr;
	mdInput->PackedSourceIn.iImgWStep = width * 2;
	MDInt_Set(MD_PACKEDSRCIN,mdInput);
	
	va_stat_ms = 0;
}

#define PARA_UPDATE_FROMENV

#ifdef PARA_UPDATE_FROMENV

#define _update_para(mdInput,type,is_float,env_str)				\
			do{													\
 				char * str =  getenv(env_str);					\
				if(str == NULL){								\
					break;										\
				}												\
				if(is_float == 1){								\
					type val = atof(str);						\
					mdInput = val;									\
					debug_printf("[Para] "env_str" to %f\n", mdInput);	\
				}else{											\
					type val = atoi(str);						\
					mdInput = val;									\
					debug_printf("[Para] "env_str" to %d\n", mdInput);	\
				}												\
			}while(0)											
											 
static void _va_mdInput_update_fromenv( MDInt_Inputs * mdInput)
{

#if 1
	// set default value, TODO : clean???
	mdInput->PDParam.bEnablePDet=3;//1;//1: use haar method; 2: use hist method; 3: use cnn method 
	mdInput->PDParam.nWidthRange=16;
	mdInput->PDParam.nHeightRange=32;
	mdInput->PDParam.dGradeTh=2.0;
	mdInput->PDParam.dTopRatio=0.2;
	mdInput->PDParam.dBottomRatio=0.9;
	mdInput->PDParam.nMinWidth=6;
	mdInput->PDParam.nMinHeight=6;
	mdInput->PDParam.nAreaTh=768;
	mdInput->PDParam.nHBRatioTh1=10;
	mdInput->PDParam.nHBRatioTh2=7;
	mdInput->PDParam.nWHRatioTh1=16;
	mdInput->PDParam.nWHRatioTh2=4;

	mdInput->HedParam.bEnableHed=1;
	mdInput->HedParam.bEnableExpend=0;
	mdInput->HedParam.nExpendSize=16;
	mdInput->HedParam.bEnableKeep=1;
	mdInput->HedParam.dKeepTh=0.85;
	mdInput->HedParam.nEdgeThresh_1a=200;
	mdInput->HedParam.nLabelCntThresh=32;
	mdInput->HedParam.nMinSize=30;
	mdInput->HedParam.nMaxSize=128;
	mdInput->HedParam.nLineNum=8;

	mdInput->ObjParam.FramePeriod=20;
	mdInput->ObjParam.FramePercentage=0.15;
#endif

	_update_para(mdInput->PDParam.bEnablePDet,unsigned char, 0, "EnablePDet");

	_update_para(mdInput->PDParam.nWidthRange, unsigned short, 0, "WidthRange");
    _update_para(mdInput->PDParam.nHeightRange, unsigned short, 0, "HeightRange");
    _update_para(mdInput->PDParam.dGradeTh, double, 1, "dGradeTh");
    _update_para(mdInput->PDParam.dTopRatio, double, 1, "dTopRatio");
    _update_para(mdInput->PDParam.dBottomRatio, double, 1, "dBottomRatio");
    _update_para(mdInput->PDParam.nMinWidth, unsigned char, 0, "MinWidth");
    _update_para(mdInput->PDParam.nMinHeight, unsigned char, 0, "MinHeight");
    _update_para(mdInput->PDParam.nAreaTh, int, 0, "AreaTh");
    _update_para(mdInput->PDParam.nHBRatioTh1, unsigned char, 0, "HBRatioTh1");
    _update_para(mdInput->PDParam.nHBRatioTh2, unsigned char, 0, "HBRatioTh2");
    _update_para(mdInput->PDParam.nWHRatioTh1, unsigned char, 0, "WHRatioTh1");
    _update_para(mdInput->PDParam.nWHRatioTh2, unsigned char, 0, "WHRatioTh2");
    MDInt_Set(MD_PDPARAM, mdInput);

	_update_para(mdInput->HedParam.bEnableHed,unsigned char, 0, "EnableHed");
	_update_para(mdInput->HedParam.bEnableExpend, unsigned char, 0, "EnableExpend");
    _update_para(mdInput->HedParam.nExpendSize, int, 0, "ExpendSize");
    _update_para(mdInput->HedParam.bEnableKeep, unsigned char, 0, "EnableKeep");
    _update_para(mdInput->HedParam.dKeepTh, double, 1, "dKeepTh");
    _update_para(mdInput->HedParam.nEdgeThresh_1a, int, 0, "EdgeThresh");
    _update_para(mdInput->HedParam.nLabelCntThresh,int, 0, "LabelCntThresh");
	_update_para(mdInput->HedParam.nMinSize, int, 0, "MinSize");
    _update_para(mdInput->HedParam.nMaxSize, int, 0, "MaxSize");
    _update_para(mdInput->HedParam.nLineNum, int, 0, "LineNum");
    MDInt_Set(MD_HEDPARAM,mdInput);
	
    _update_para(mdInput->ObjParam.FramePeriod, int, 0, "FramePeriod");
    _update_para(mdInput->ObjParam.FramePercentage, double, 1, "FramePercentage");
    MDInt_Set(MD_OBJPARAM,mdInput);

}
#endif

static t_md_algo_buf * md_buf = NULL;
static u32 prev_index_cnt = 0;
static int prev_use_1 = 0;
static t_md_algo_buf mdbuf_ret;

static void _va_run_md_algo()
{
	md_buf = libmd_get_buf(0, MD_GET_BUF_TIMEOUT);
	if(md_buf == NULL){
		return;
	}
	//Maybe not necessary as the md_buf will never changed.
	memcpy(&mdbuf_ret, md_buf, sizeof(mdbuf_ret));

	///< memcpy
	u8 * cur = NULL;
	u8 * prev = NULL;

#if 0 
	memcpy(md_buf->addr_cur, md_buf->addr_ori, md_buf->width*md_buf->height*2);
	if(prev_index_cnt == 3){
		memcpy(md_buf->addr_prev0, md_buf->addr_prev1, md_buf->width*md_buf->height*2);
		memcpy(md_buf->addr_prev1, md_buf->addr_cur, md_buf->width*md_buf->height*2);
		prev_index_cnt = 0;
	}
	prev_index_cnt ++;
#else
	//new call, only memcpy() once in all the cases.
	if(prev_index_cnt == 3){
		if(prev_use_1 == 0){
			//current use prev0
			memcpy(md_buf->addr_prev0, md_buf->addr_ori, md_buf->width*md_buf->height*2);
			cur = md_buf->addr_prev0;
			prev_use_1 = 1;
		}else{
			//current use prev0
			memcpy(md_buf->addr_prev1, md_buf->addr_ori, md_buf->width*md_buf->height*2);
			cur = md_buf->addr_prev1;
			prev_use_1 = 0;
		}
		prev_index_cnt = 0;	
	}else{
		memcpy(md_buf->addr_cur, md_buf->addr_ori, md_buf->width*md_buf->height*2);
		cur = md_buf->addr_cur;
		prev_index_cnt ++;
	}

	if(prev_use_1 == 0){
		prev = md_buf->addr_prev0;
	}else{
		prev = md_buf->addr_prev1;
	}
#endif

    u32 start_ms = ticks * 10 + tick1_get_cycle() * 10 / ReadReg32(REG_INT_BASE + 0x88);

	MDInt_Proc(cur, prev);

	MDInt_Get(MD_DOBJECTS,&_md_output);

    u32 end_ms = ticks * 10 + tick1_get_cycle() * 10 / ReadReg32(REG_INT_BASE + 0x88);

	va_stat_ms += end_ms - start_ms;
//	debug_printf("MD ms = %d\n",end_ms-start_ms);
	
	if(_va_cfg.run_cb != NULL){
		_va_cfg.run_cb();
	}
}

int video_analysis_result(t_video_analysis_res * res)
{
	if(res == NULL){
		return -1;
	}

	res->obj_num = _md_output.DetectObjects.ObjNumber;
    int i;
	for(i = 0; i < res->obj_num; i++){
		res->obj_type[i] = 0;
		res->obj_left[i] = _md_output.DetectObjects.Objects[i].left;
		res->obj_top[i] = _md_output.DetectObjects.Objects[i].top;
		res->obj_right[i] = _md_output.DetectObjects.Objects[i].right;
		res->obj_bottom[i] = _md_output.DetectObjects.Objects[i].bottom;
		res->obj_brightness[i] = _md_output.DetectObjects.Objects[i].avgBrightness;
	}

	res->is_motion = _md_output.DetectObjects.IsMotion;
	res->is_motion_people = _md_output.DetectObjects.IsMotionPeople;
	/* TODO: add by stiger 20170812, temporary */
	if(res->is_motion_people){
		res->is_true_alarm = 1;
	}else{
		res->is_true_alarm = 0;
	}
    res->motion_obj_type[0] = 0;
	res->motion_obj_left[0] = _md_output.DetectObjects.MotionObject[0].left;
	res->motion_obj_top[0] = _md_output.DetectObjects.MotionObject[0].top;
	res->motion_obj_right[0] = _md_output.DetectObjects.MotionObject[0].right;
	res->motion_obj_bottom[0] = _md_output.DetectObjects.MotionObject[0].bottom;
    res->motion_obj_brightness[0] = _md_output.DetectObjects.MotionObject[0].avgBrightness;

	return 0;
}

static void * _video_analysis_routine(void* arg)
{
	pthread_detach(pthread_self());
	pthread_setname(NULL, "VideoAnalysis");

	va_status = 1;

#ifdef YUV_WRITE_SD
    efs_init(EFS_HW_SCIF, 0, EFS_BUFFER, EFS_BUFFER_SIZE);
    debug_printf("  SD card capacity: %d sectors\n", sh_efs.sectors);
    debug_printf("  Filesystem sectors per Cluster: %d\n", sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem sectors count: %d, cluster count:%d\n", sh_efs.sectors, sh_efs.sectors/sh_efs.bpb.sectors_per_cluster);
    debug_printf("  Filesystem Free Clusters: %d\n", sh_efs.clusters_free);
    debug_printf("  Filesystem first Free Cluster: %d\n", sh_efs.next_free_cluster);

	u8 g_file_name[30];
    sprintf((char *)g_file_name, "vs%dtest.txt", 0);

    FILE * fp_yuv = fopen(g_file_name, "w");
    if(fp_yuv == NULL){
        debug_printf("open file %s failed\n", g_file_name);
    }else{
        debug_printf("open file %s\n", g_file_name);
    }
#endif

#ifdef USE_YUV_STREAM
	u32 resolution;
	u16 width, height;

	u32 ret = 0;
	int vs_id = _va_cfg.vs_id;

	if(vs_id >= 0){
		ret = libvs_init(vs_id);
		ret |= libvs_start(vs_id);

		if(ret != 0){
			syslog(LOG_ERR, "[ERROR]: VA start vs %d fail.\n", vs_id);
			return;
		}

		resolution = libvs_get_resolution(vs_id);
		width = (resolution >> 16) & 0xffff;
		height = resolution & 0xffff;

		_va_open(width, height, width, height);
	}
#endif

	//get first frame
	while(1){
		md_buf = libmd_get_buf(0, MD_GET_BUF_TIMEOUT);
		if(md_buf != NULL){
			break;
		}
	}
	prev_use_1 = 0;
	memcpy(md_buf->addr_prev0, md_buf->addr_ori, md_buf->width*md_buf->height*2);
	memcpy(md_buf->addr_prev1, md_buf->addr_prev0, md_buf->width*md_buf->height*2);
	memcpy(&mdbuf_ret, md_buf, sizeof(mdbuf_ret));

	MDInt_Inputs mdInput;
	_va_open_md_algo(&mdInput, md_buf->addr_cur,md_buf->addr_prev0, md_buf->width, md_buf->height);

#ifdef PARA_UPDATE_FROMENV
	_va_mdInput_update_fromenv(&mdInput);
#endif

	syslog(LOG_INFO, "Video Analysis of MD Algo is started with MD(%d*%d)\n", md_buf->width, md_buf->height);

	while(va_status == 1){
		// md algo buf process
		_va_run_md_algo();

#ifdef USE_YUV_STREAM
		if(vs_id >= 0){
			// yuv frame process
			FRAME *frame = libvs_get_frame(vs_id, 20);
			if(frame == NULL){
				continue;
			}

#ifdef YUV_WRITE_SD
			int bytes;
    		bytes = fwrite(frame->addr, 1, frame->size, fp_yuv);
    		if(frame->size1){
    		    bytes += fwrite(frame->addr1, 1, frame->size1, fp_yuv);
    		}
    		debug_printf("YUV Write(0x%x:%d)\n", frame->addr, bytes);
#endif
			//debug_printf("VA: addr: %x-%x, size: %d-%d\n", frame->addr, frame->addr1, frame->size, frame->size1);
			_va_run( frame->addr, frame->addr1 );
			libvs_remove_frame(vs_id, frame);
		}
#endif
	}

#ifdef USE_YUV_STREAM
	if(vs_id >= 0){
		libvs_stop(vs_id);
		libvs_exit(vs_id);	
		_va_close();
	}
#endif

#ifdef YUV_WRITE_SD
    fclose(fp_yuv);
    fp_yuv = NULL;
#endif

	va_status = 0;
	pthread_cond_signal(&_va_stop_cond);
}

#define VA_STACKSIZE 10240
static char __attribute__((aligned(8))) _va_stack[VA_STACKSIZE];

int video_analysis_start(t_video_analysis_cfg * cfg)
{
	// clear env
	memset(&_va_cfg, 0, sizeof(_va_cfg));

	// store args
	if(cfg != NULL){
		memcpy(&_va_cfg, cfg, sizeof(_va_cfg));
	}

	mdbuf_notempty_evt = ertos_event_create();
	if(mdbuf_notempty_evt == NULL){
		return -2;
	}

	//reg event
	if(mpu_event_reg(MPUEVT_MDBUF_NOTEMPTY, mdbuf_notempty_evt) != 0){
		return -3;
	}

	// create thread
	pthread_attr_t va_attr;

	pthread_attr_init(&va_attr);
	va_attr.schedparam.sched_priority = 17;
	pthread_attr_setstack(&va_attr, _va_stack, VA_STACKSIZE);
	int ret = pthread_create(&va_thread, &va_attr, _video_analysis_routine, NULL);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Create video analysis thread fail!\n");
		return -1;
	}

	return 0;
}

int video_analysis_stop(void)
{
	if(va_status == 0){
		return 0;
	}

	va_status = 2;

	mpu_event_dereg(MPUEVT_MDBUF_NOTEMPTY);
		
	ertos_event_signal(mdbuf_notempty_evt);

	if(pthread_self() != va_thread){
		//it's called from VA thread, so cannot wait
		pthread_cond_wait(&_va_stop_cond, NULL);
	}

	ertos_event_delete(mdbuf_notempty_evt);
	mdbuf_notempty_evt = NULL;
	
	MDInt_Close();
	return 0;
}

t_md_algo_buf * video_analysis_get_buf(u32 timeout)
{
	return &mdbuf_ret;
}
