#ifndef _MD_API_H
#define _MD_API_H

//MD API
#ifdef __cplusplus
extern "C" {
#endif

#include "GlobalDef.h"

	/*****************************************************************************************/
	enum MD_INPUTS{MD_SRCIN,MD_PACKEDSRCIN,MD_FORBIDZONE,MD_OBJPARAM,MD_CBHANDLE,MD_SRCCHR,MD_RESET,MD_PDPARAM, MD_HEDPARAM};

	typedef struct{
		u8 *pCurImg;
		u8 *pBgrImg;
		u8 *pMaskImg;
		s32 iImgWidth;
		s32 iImgHeight;
		s32 iImgWStep;
	}MDInt_Source;

	typedef struct{
		u8 *pPrevPackedImg;
		u8 *pPackedImg;
		s32 iImgWStep;
		u8 *pPackedMask;
		s32 iMaskWStep;
	}MDInt_PackedSource;

	typedef struct{
		u8 *pU;
		u8 *pV;
		s32 iChrWidth;
		s32 iChrHeight;
		s32 iChrWStep;
	}MDInt_ChrSource;

	typedef struct{
		u8 enabled;
		u8 ptnum; //up to 8 points polygon supported
		s32 x[8];
		s32 y[8];
		u8 sensitivity;//0~100
	}MDInt_ROI;

	typedef struct{
		s8 mode; //0: forbidden mode 1: roi mode
		s8 zoneNums;//up to 4 zones supported
		MDInt_ROI zones[4];
	}MDInt_ForbiddenZone;

	typedef struct
	{
		u8 bEnbMDet;
		int FramePeriod;
		double FramePercentage;
	}MDInt_ObjParam;

	typedef struct{
		u8 bEnablePDet;
		u16 nWidthRange;//expend size
		u16 nHeightRange;
		double dGradeTh;//grade image gray value threshold
		double dTopRatio;//top point percenter 0~1
		double dBottomRatio;//bottom point percenter 0~1, must be bigger than dTopRatio
		u8 nMinWidth;
		u8 nMinHeight;
		int nAreaTh;
		u8 nHBRatioTh1;
		u8 nHBRatioTh2;
		u8 nWHRatioTh1;
		u8 nWHRatioTh2;
	}MDInt_PDParam;

	typedef struct{
		u8 bEnableHed;
		u8 bEnableExpend;
		int nExpendSize;
		u8 bEnableKeep;
		double dKeepTh;
		int nEdgeThresh_1a;
		int nLabelCntThresh;
		int nMinSize;
		int nMaxSize;
		int nLineNum;
    }MDInt_HedParam;

	typedef struct{
		MDInt_Source SourceIn;
		MDInt_PackedSource PackedSourceIn;
		MDInt_ChrSource  ChrSourceIn;
		MDInt_ForbiddenZone fzone;
		MDInt_ObjParam ObjParam;
		MDInt_PDParam PDParam;
		MDInt_HedParam HedParam;
		void *pCallbackhandle;
	}MDInt_Inputs;

	//set MD function inputs
	void MDInt_Set(s32 InputID,const MDInt_Inputs *pInput);

	/*****************************************************************************************/
	enum MD_OUTPUTS{MD_DSTATE,MD_DOBJECTS};
	typedef struct{
		u8 bMotionDetected;
		u8 ucHoGDist; //for internal use only
	}MDInt_DState;

	typedef struct{
		s16 left;
		s16 right;
		s16 top;
		s16 bottom;
		s16 hspeed;
		s16 vspeed;
		u8 motionState;// if the motion area bigger than the sensitivity threshold, report as 1; or else is 0;
		u8 isMotion;// 0: don't have motion; 1: have motion
		u8 avgBrightness;
	}MDInt_Object;

	typedef struct{
		u8 ObjNumber; /*0~4*/
		MDInt_Object Objects[4];
		u8 IsMotion;
		u8 IsMotionPeople;
		MDInt_Object MotionObject[1];
	}MDInt_DObjects;

	typedef struct{
		MDInt_DState DetectState;
		MDInt_DObjects DetectObjects;
	}MDInt_Outputs;

	//Get MD function outputs
	void MDInt_Get(s32 OutputID,MDInt_Outputs *pOutput);

	/*****************************************************************************************/
	//MD  processing function
	void MDInt_Proc(u8 * pPackedImg, u8 * pPrevPackedImg);
	
	//MD Open
	void MDInt_Open(void);
	
	//MD Close
	void MDInt_Close(void);
	/******************************************************************************************/
	//Call back function, for validation in OpenCV
#define _ENB_CALLBACK_  0

#if _ENB_CALLBACK_
	void HOGDistCallback(void *pCBHandle,int px,int py,int iHOGDist,int m);
	void HOGCenterCallback(void *pCBHandle,int px,int py,int sx,int sy);
	void HOGCenterCallback2(void *pCBHandle,int px,int py,int spdx,int spdy);
    void UpdateMaskCallback(void *pCBHandle,int px,int py,int iMaskCount);
    void UpdatePixelCallback(void *pCBHandle,int px,int py,int iPixel);	
#endif

#define _ENB_PACKEDINPUT_ 1

#define _LITTLE_ENDIAN_ 1

#ifdef __cplusplus
}
#endif

#endif
