#ifndef _GLOBAL_DEF
#define _GLOBAL_DEF

#include "Platform.h"

#define _MAX(a,b) ((a)>(b)?(a):(b))
#define _MIN(a,b) ((a)<(b)?(a):(b))
#define _ABS(a) ((a)>0?(a):-(a))
#define _CLIP(x,min,max) ((x)<(min)?(min):(x)>(max)?(max):(x))

typedef struct
{
	s16 x;
	s16 y;
}_MD_POINT;

//mask buffer shift, to compensate HW postprocessing shift
#define MASK_SHF_X  15
#define MASK_SHF_Y  15

#endif