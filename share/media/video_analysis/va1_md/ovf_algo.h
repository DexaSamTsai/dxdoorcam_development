#ifndef _OVF_ALGO_H_
#define _OVF_ALGO_H_

typedef struct s_md_algo_mask_zone{
	u32 enabled;
	u32 point_num;
	int left;
	int top;
	int right;
	int bottom;
	u8  thread;
	u8 sensitivity;
}t_md_algo_mask_zone;

#define MD_ALGO_MASK_MAXNUM		(25)
typedef struct s_md_algo_mask{
	int mode;	// 0: forbidden	 1: roi
	int num;
	t_md_algo_mask_zone zones[MD_ALGO_MASK_MAXNUM];
}t_md_algo_mask;

typedef struct s_ovf_algo_cfg{
	//FIXME: unused input sample
	int format;
	int width;
	int height;
	t_md_algo_mask mask;

	float ratio_h; // the higher limit of the ration height/width
	float ratio_l; // the lower limit of the ration height/width
	unsigned char with_inactROI;
	int inact_ROI_l;
	int inact_ROI_r;
	int inact_ROI_t;
	int inact_ROI_b;
	int motion_size_thre; //the threshold of motion size

}t_ovf_algo_cfg;

typedef struct s_ovf_algo_output{
	int is_true_alarm;	// 1: true alarm, 0: false alarm
	int intensity[MD_ALGO_MASK_MAXNUM];
	int cnt;
	int unknow0_truel_false2;
	int last_is_true_alarm;
	int g_rec[MD_ALGO_MASK_MAXNUM];
	int motion_flag[MD_ALGO_MASK_MAXNUM]; //0 : motion<thread, 1: motion>=thread
	int obj_left[MD_ALGO_MASK_MAXNUM];
	int obj_right[MD_ALGO_MASK_MAXNUM];
	int obj_top[MD_ALGO_MASK_MAXNUM];
	int obj_bottom[MD_ALGO_MASK_MAXNUM];
	int n_obj;
	unsigned char do_act;
}t_ovf_algo_output;

int ovf_algo_open(t_ovf_algo_cfg * cfg);
int ovf_algo_process(t_ovf_algo_cfg * cfg, unsigned char * buf_y, unsigned char * buf_uv, t_ovf_algo_output * output);
int ovf_algo_close(void);

#endif
