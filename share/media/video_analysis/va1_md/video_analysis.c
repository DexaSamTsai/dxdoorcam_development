//==========================================================================
// Include File
//==========================================================================
# include "includes.h"
# include "media/va/video_analysis.h"
# include "ovf_algo.h"

//==========================================================================
// Type Declaration
//==========================================================================
# define VA_STACKSIZE	10240
# define YUV_WIDTH_DFT	320
# define YUV_HEIGHT_DFT	180

# define EFS_BUFFER			( MEMBASE_DDR + 0xa00000 )
# define EFS_BUFFER_SIZE		( 1024 * 1024 )

//#define YUV_WRITE_SD        //Config this if need to store YUV to SD

//==========================================================================
// Static Params
//==========================================================================
static t_video_analysis_cfg		va_cfg;
static t_md_algo_mask			va_mask;
static t_ovf_algo_cfg			ovf_algo_cfg;
static t_ovf_algo_output		ovf_algo_output;

static pthread_cond_t	va_stop_cond	= PTHREAD_COND_INITIALIZER;
static pthread_t		va_thread		= NULL;


static u32 va_status = 0; //0:IDLE, 1:running, 2:to_stop
static int va_para_update_flag = 0;

//==========================================================================
// Static Functions
//==========================================================================
static void
va_open( int wc, int hc, int ws, int hs )
{
	ovf_algo_cfg.format		= 1;
	ovf_algo_cfg.width		= wc;
	ovf_algo_cfg.height		= hc;
	ovf_algo_cfg.motion_size_thre	= 85;
	ovf_algo_cfg.ratio_h	= 8.0;
	ovf_algo_cfg.ratio_l	= 0.15;

	//ROI para
	ovf_algo_open( &ovf_algo_cfg );
}

static void
va_run( unsigned char *buf_Y, unsigned char *buf_UV )
{
	if ( va_para_update_flag == 1 ) {
		memcpy( &ovf_algo_cfg.mask, &va_mask, sizeof( t_md_algo_mask ) );
		va_para_update_flag = 0;
	}

	//FIXME: only a pesudo sample of va algo process
	ovf_algo_process( &ovf_algo_cfg, buf_Y, buf_UV, &ovf_algo_output );

	if ( va_cfg.run_cb != NULL ) {
		va_cfg.run_cb();
	}
}

static void *
va_routine( void *arg )
{
	pthread_setname( NULL, "va_routine" );

	u32 vs_id = va_cfg.vs_id;

	va_status = 1;

	if ( vs_id >= 0 ) {
		if ( libvs_init( vs_id ) ||
			 libvs_start( vs_id ) ) {
			syslog( LOG_ERR, "[ERROR]: VA start vs %d fail.\n", vs_id );
			return NULL;
		}

		u32 resolution = libvs_get_resolution( vs_id );
		u16 width = ( resolution >> 16 ) & 0xffff;
		u16 height = resolution & 0xffff;

		if ( width == 0 || height == 0 ) {
			width	= YUV_WIDTH_DFT;
			height	= YUV_HEIGHT_DFT;
		}

		va_open( width, height, width, height );
	}

	u32 pre_addr, curr_addr;

	while ( va_status == 1 ) {
		if ( vs_id >= 0 ) {
			FRAME *frame = libvs_get_frame( vs_id, 20 );

			if ( frame == NULL ) {
				continue;
			}

			curr_addr	= ( frame->addr & ~BIT31 );
			pre_addr	= ( frame->addr1 & ~BIT31 );

			dc_invalidate_range( curr_addr, ( curr_addr + frame->size ) );
			dc_invalidate_range( pre_addr, ( pre_addr + frame->size1 ) );

			va_run( ( u8 * )curr_addr, ( u8 * )pre_addr );

			libvs_remove_frame( vs_id, frame );
		}

		msleep( 10 );
	}

	if ( vs_id >= 0 ) {
		libvs_stop( vs_id );
		libvs_exit( vs_id );

		ovf_algo_close();
	}

	va_status = 0;

	pthread_cond_signal( &va_stop_cond );
	pthread_detach( pthread_self() );

	return NULL;
}

//==========================================================================
// APIs
//==========================================================================
int
video_analysis_result( t_video_analysis_res *res )
{
	if ( res == NULL ) {
		return -1;
	}

	memset( res, 0x00, sizeof( t_video_analysis_res ) );

	/* GET ROI motion result */
	if ( ovf_algo_output.do_act ) {
		int i;

		for( i = 0; i < MD_ALGO_MASK_MAXNUM; i ++ ) {
			if ( va_mask.zones[ i ].enabled ) {
				res->obj_type[ i ]		= ovf_algo_output.motion_flag[ i ];
				res->obj_left[ i ]		= ovf_algo_output.obj_left[ i ];
				res->obj_right[ i ]		= ovf_algo_output.obj_right[ i ];
				res->obj_top[ i ]		= ovf_algo_output.obj_top[ i ];
				res->obj_bottom[ i ]	= ovf_algo_output.obj_bottom[ i ];

				if ( ovf_algo_output.motion_flag[ i ] ) {
				    res->is_true_alarm	|= ( 1 << i );
				}
			}
		}
	}

	return 0;
}

int
video_analysis_start( t_video_analysis_cfg *cfg )
{
	if ( cfg == NULL ) {
		syslog( LOG_ERR, "[ERROR]: Create video analysis thread fail!\n" );
		return -1;
	}

	memset( &ovf_algo_cfg, 0, sizeof( t_ovf_algo_cfg ) );
	memset( &ovf_algo_output, 0, sizeof( t_ovf_algo_output ) );

	// store args
	memcpy( &va_cfg, cfg, sizeof( t_video_analysis_cfg ) );

	if ( va_cfg.para != NULL ) {
		memcpy( &va_mask, va_cfg.para, sizeof( t_md_algo_mask ) );
	} else {
		memset( &va_mask, 0x00, sizeof( t_md_algo_mask ) );
	}

	va_cfg.para = &va_mask;

	// create thread
	pthread_attr_t va_attr;

	pthread_attr_init( &va_attr );
	va_attr.schedparam.sched_priority = 6;
	if ( va_cfg.sched_priority > 0 ) {
		va_attr.schedparam.sched_priority	= va_cfg.sched_priority;
	}

	char *va_stack = ( char * )malloc( VA_STACKSIZE );

	if ( va_stack == NULL ) {
		syslog( LOG_ERR, "[ERROR]: malloc fail!\n" );
		pthread_attr_destroy( &va_attr );
		return -1;
	}

	pthread_attr_setstack( &va_attr, va_stack, VA_STACKSIZE );

	if ( pthread_create( &va_thread, &va_attr, va_routine, NULL ) != 0 ) {
		syslog( LOG_ERR, "[ERROR]: Create video analysis thread fail!\n" );
		pthread_attr_destroy( &va_attr );
		return -1;
	}

	pthread_attr_destroy( &va_attr );

	return 0;
}

int
video_analysis_stop( void )
{
	if ( va_status == 0 ) {
		return 0;
	}

	va_status = 2;

	if ( pthread_self() != va_thread ) {
		//it's called from VA thread, so cannot wait
		pthread_cond_wait( &va_stop_cond, NULL );
	}

	return 0;
}

int
video_analysis_get_para( void *para )
{
	if ( para == NULL ) {
		return -1;
	}
	//FIXME: This API is only effective if cfg->para is not NULL when video_analysis_start(cfg) is called.
	if ( va_cfg.para == NULL ) {
		return -1;
	}

	memcpy( para, va_cfg.para, sizeof( t_md_algo_mask ) );

	return 0;
}

int
video_analysis_set_para( void *para )
{
	if ( para == NULL ) {
		return -1;
	}
	//FIXME: This API is only effective if cfg->para is not NULL when video_analysis_start(cfg) is called.
	if ( va_cfg.para == NULL ) {
		return -1;
	}

	memcpy( va_cfg.para, para, sizeof( t_md_algo_mask ) );

	va_para_update_flag = 1;

	return 0;
}
