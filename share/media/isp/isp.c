#include "includes.h"

int libisp_init_internal(void);

#define ISP_ADDR_ALIGN	64
#ifdef __LINUX__
extern char * ddr_buffer;
extern char * iram_buffer;
//SRAM not used. 0x10060000 is VideoEngineBuffer.
static u32 isp_laddr = 0x1005c000;
static u32 isp_raddr = 0x1005e000;
#else
static unsigned char __attribute__  ((aligned(ISP_ADDR_ALIGN))) isp_laddr[0x2000];
static unsigned char __attribute__  ((aligned(ISP_ADDR_ALIGN))) isp_raddr[0x2000];
#endif

int libisp_init(void)
{
	libisp_set_bufaddr((u32)isp_laddr, (u32)isp_raddr);
	return libisp_init_internal();
}

int libisp_fastinit(void)
{
	libisp_set_bufaddr((u32)isp_laddr, (u32)isp_raddr);
	return 0;
}

int libisp_exit(void)
{
	return mpu_cmd(MPUCMD_ISP_EXIT, 0);
}

u32 libisp_get_laddr(void)
{
#ifdef __LINUX__
	return (u32)(iram_buffer + isp_laddr - 0x10000000);
#else
	return (u32)(isp_laddr)|BIT31;
#endif
}

u32 libisp_get_raddr(void)
{
#ifdef __LINUX__
	return (u32)(iram_buffer + isp_raddr - 0x10000000);
#else
	return (u32)(isp_raddr)|BIT31;
#endif
}

int libisp_set_sde(int isp_id, int sde_effect_No)
{
	return mpu_cmd(MPUCMD_ISP_SET_SDE | (isp_id << 12), sde_effect_No);
}
int libisp_set_brightness(int isp_id, int brightness_level)
{
	return mpu_cmd(MPUCMD_ISP_SET_BRIGHTNESS | (isp_id << 12), brightness_level);
}

int libisp_set_sharpness(int isp_id, int sharpness_level)
{
	return mpu_cmd(MPUCMD_ISP_SET_SHARPNESS | (isp_id << 12), sharpness_level);
}

int libisp_ioctl(int isp_id, int request, void * arg)
{
	return mpu_cmd(MPUCMD_TYPE_ISP | (isp_id << 12) | request, (u32)arg);
}

int libisp_set_daynight(int isp_id, int daynight)
{
	return mpu_cmd(MPUCMD_ISP_SET_DAYNIGHT | (isp_id << 12), daynight);
}

int libisp_set_autofps(int isp_id, int autofps_enable,int max_fps,int min_fps)
{
	return mpu_cmd(MPUCMD_ISP_SET_AUTOFPS | (isp_id << 12), ((autofps_enable<<16)&0xff0000)|((max_fps << 8) & 0xff00) |(min_fps & 0xff));
}

int libisp_set_channel(int isp_id, int channel)
{
	return mpu_cmd(MPUCMD_ISP_ISPOUT_SEL | (isp_id << 12), channel);
}

int libisp_sccbrd(int isp_id, int sensorid, int sccbaddr)
{
	return mpu_cmd(MPUCMD_ISP_SCCBRD | (isp_id << 12), (sensorid << 24) | sccbaddr);
}

int libisp_sccbwr(int isp_id, int sensorid, int sccbaddr, int sccbval)
{
	return mpu_cmd(MPUCMD_ISP_SCCBWR | (isp_id << 12), (sensorid << 24) | (sccbaddr<<8)|(sccbval&0xff));
}

static u32 * isp_lumaddr = NULL;
int libisp_get_lum(int isp_id)
{
	if(isp_lumaddr == NULL){
		isp_lumaddr = (u32 *)mpu_cmd(MPUCMD_ISP_GET_LUMADDR | (isp_id << 12), 0);
	}

	if(isp_lumaddr){
		return ReadReg32(isp_lumaddr);
	}else{
		return 0;
	}
}

int libisp_set_exp(int isp_id, u32 exp)
{
	return mpu_cmd(MPUCMD_ISP_SET_EXP | (isp_id << 12), exp);
}

int libisp_set_gain(int isp_id, u32 gain)
{
	return mpu_cmd(MPUCMD_ISP_SET_GAIN | (isp_id << 12), gain);
}

int libisp_set_awb_b_gain(int isp_id, u32 awb)
{
	return mpu_cmd(MPUCMD_ISP_SET_AWB_B_GAIN | (isp_id << 12), awb);
}

int libisp_set_awb_g_gain(int isp_id, u32 awb)
{
	return mpu_cmd(MPUCMD_ISP_SET_AWB_G_GAIN | (isp_id << 12), awb);
}

int libisp_set_awb_r_gain(int isp_id, u32 awb)
{
	return mpu_cmd(MPUCMD_ISP_SET_AWB_R_GAIN | (isp_id << 12), awb);
}

int libisp_get_exp(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_EXP | (isp_id << 12), 0);
}

int libisp_get_gain(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_GAIN | (isp_id << 12), 0);
}

int libisp_get_awb_b_gain(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_AWB_B_GAIN | (isp_id << 12), 0);
}

int libisp_get_awb_g_gain(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_AWB_G_GAIN | (isp_id << 12), 0);
}

int libisp_get_awb_r_gain(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_AWB_R_GAIN | (isp_id << 12), 0);
}

int libisp_set_calib_awb(int isp_id, u32 addr)
{
	return mpu_cmd(MPUCMD_ISP_SET_CALI_AWB | (isp_id << 12), addr);
}

int libisp_set_calib_lenc_atbl(int isp_id, u32 addr)
{
	return mpu_cmd(MPUCMD_ISP_SET_CALI_LENC_A | (isp_id << 12), addr);
}

int libisp_set_calib_lenc_ctbl(int isp_id, u32 addr)
{
	return mpu_cmd(MPUCMD_ISP_SET_CALI_LENC_C | (isp_id << 12), addr);
}

int libisp_set_calib_lenc_dtbl(int isp_id, u32 addr)
{
	return mpu_cmd(MPUCMD_ISP_SET_CALI_LENC_D | (isp_id << 12), addr);
}
int libisp_get_rgbir_cgain(int isp_id)
{
	int ret = mpu_cmd(MPUCMD_ISP_GET_RGBIR_CGAIN | (isp_id << 12), 0);
	return ret;
}
int libisp_set_lowlux_coeff(int isp_id, u32 val)
{
	int ret = mpu_cmd(MPUCMD_ISP_SET_COEFF | (isp_id << 12), val);
	return ret;
}

int libisp_set_sat_thr(int isp_id, u32 val)
{
	int ret = mpu_cmd(MPUCMD_ISP_SET_SAT_THR | (isp_id << 12), val);
	return ret;
}

int libisp_set_coeff_thr(int isp_id, u32 val)
{
	int ret = mpu_cmd(MPUCMD_ISP_SET_COEFF_THR | (isp_id << 12), val);
	return ret;
}

int libisp_set_hdr(int isp_id, u32 val)
{
	int ret = mpu_cmd(MPUCMD_ISP_SET_HDR | (isp_id << 12), val);
	return ret;
}

int libisp_get_aec_stable(int isp_id)
{
    int ret = mpu_cmd(MPUCMD_ISP_GET_AEC_STABLE | (isp_id << 12), 0);
    return ret;
}

u32 libisp_get_hist_buf(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_HIST | (isp_id << 12), 0);
}

int libisp_set_roi(int isp_id, u32 roi)
{
	return mpu_cmd(MPUCMD_ISP_SET_ROI | (isp_id << 12), roi);
}

int libisp_get_y(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_MEANY| (isp_id << 12), 0);
}

int libisp_get_daynight(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_DAYNIGHT | (isp_id << 12), 0);
}

int libisp_get_hdr(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_HDR | (isp_id << 12), 0);
}

int libisp_get_hdr_stable(int isp_id)
{
	return mpu_cmd(MPUCMD_ISP_GET_HDR_STABLE | (isp_id << 12), 0);
}

int libisp_set_aec_manual(int isp_id, u32 enable)//1:manual 0:auto
{
	debug_printf("AEC manual (%d)\n",enable);
	return mpu_cmd(MPUCMD_TYPE_ISP | (isp_id << 12) | MPUCMD_ISP_SET_AEC_MANUAL_EN, enable);
}

int libisp_set_aec_manual_gain(int isp_id, u32 gain)
{
	debug_printf("AEC Manual Gain 0x%x\n", gain);
    return mpu_cmd(MPUCMD_TYPE_ISP | (isp_id << 12) | MPUCMD_ISP_SET_AEC_MANUAL_GAIN, gain);
}

int  libisp_set_aec_manual_exp(int isp_id, u32 exp)
{
	debug_printf("AEC Manual Exp 0x%x\n", exp);
	return mpu_cmd(MPUCMD_TYPE_ISP | (isp_id << 12) | MPUCMD_ISP_SET_AEC_MANUAL_EXP, exp);
}

int  libisp_set_awb_manual(int isp_id, u32 enable) //1:manual 0:auto
{
	debug_printf("AWB Manual %d\n", enable);
	return mpu_cmd(MPUCMD_TYPE_ISP | (isp_id << 12) | MPUCMD_ISP_SET_AWB_MANUAL_EN, enable);
}
