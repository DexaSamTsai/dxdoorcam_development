#include "includes.h"

static int _cmd_rdispraddr(int argc, char ** argv)
{
	if(argc != 3){
		syslog(LOG_ERR, "Usage: rdispraddr <bytes 1/2/4> <addr(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
    u32 reg_start = libisp_get_raddr(); 
	switch(bytes){
		case 1:
			val = ReadReg8(reg+reg_start);
			syslog(LOG_INFO, "Read ispraddr+[0x%x]:%x\n", reg, val);
			break;
		case 2: 
			val = ReadReg16(((reg+reg_start)>>1)<<1);
			syslog(LOG_INFO, "Read ispraddr+[0x%x]:%x\n", (reg>>1)<<1, val);
			break;
		case 4:
			val = ReadReg32(((reg_start+reg)>>2)<<2);
			syslog(LOG_INFO, "Read ispraddr+[0x%x]:%x\n", (reg>>2)<<2, val);
			break;
		default:
			syslog(LOG_ERR, "the wrong read bytes!\n");
			return -2;
	}
	return 0;
}


static int _cmd_rdispladdr(int argc, char ** argv)
{
	if(argc != 3){
		syslog(LOG_ERR, "Usage: rdispladdr <bytes 1/2/4> <addr(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
    u32 reg_start = libisp_get_laddr(); 
	switch(bytes){
		case 1:
			val = ReadReg8(reg+reg_start);
			syslog(LOG_INFO, "Read ispladdr+[0x%x]:%x\n", reg, val);
			break;
		case 2: 
			val = ReadReg16(((reg+reg_start)>>1)<<1);
			syslog(LOG_INFO, "Read ispladdr+[0x%x]:%x\n", (reg>>1)<<1, val);
			break;
		case 4:
			val = ReadReg32(((reg_start+reg)>>2)<<2);
			syslog(LOG_INFO, "Read ispladdr+[0x%x]:%x\n", (reg>>2)<<2, val);
			break;
		default:
			syslog(LOG_ERR, "the wrong read bytes!\n");
			return -2;
	}
	return 0;
}

static int _cmd_wrispraddr(int argc, char ** argv)
{
	if(argc != 4){
		syslog(LOG_ERR, "Usage: wrispladdr <bytes1/2/4> <addr(hex)> <data(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
	val = strtoul(argv[3], NULL, 16);
    u32 reg_start = libisp_get_raddr(); 
	syslog(LOG_INFO, "Write 0x%x to ispraddr+[0x%x]\n", val, reg);
	switch(bytes){
		case 1:
			WriteReg8(reg+reg_start, val);
			break;
		case 2: 
			WriteReg16(reg+reg_start, val);
			break;
		case 4:
			WriteReg32(reg+reg_start, val);
			break;
		default:
			syslog(LOG_ERR, "the wrong write bytes!\n");
			return -2;
	}
	return 0;

}


static int _cmd_wrispladdr(int argc, char ** argv)
{
	if(argc != 4){
		syslog(LOG_ERR, "Usage: wrispladdr <bytes1/2/4> <addr(hex)> <data(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
	val = strtoul(argv[3], NULL, 16);
    u32 reg_start = libisp_get_laddr(); 
	syslog(LOG_INFO, "Write 0x%x to ispladdr+[0x%x]\n", val, reg);
	switch(bytes){
		case 1:
			WriteReg8(reg+reg_start, val);
			break;
		case 2: 
			WriteReg16(reg+reg_start, val);
			break;
		case 4:
			WriteReg32(reg+reg_start, val);
			break;
		default:
			syslog(LOG_ERR, "the wrong write bytes!\n");
			return -2;
	}
	return 0;

}

CMDSHELL_DECLARE(rdispladdr)
	.cmd = {'r', 'd','i','s','p', 'l','a','d','d','r','\0'},
	.handler = _cmd_rdispladdr,
	.comment = "rdispreg",
};

CMDSHELL_DECLARE(wrispladdr)
	.cmd = {'w', 'r','i','s','p', 'l','a','d','d','r','\0'},
	.handler = _cmd_wrispladdr,
	.comment = "wrispreg",
};

CMDSHELL_DECLARE(rdispraddr)
	.cmd = {'r', 'd','i','s','p', 'r','a','d','d','r','\0'},
	.handler = _cmd_rdispraddr,
	.comment = "rdispreg",
};

CMDSHELL_DECLARE(wrispraddr)
	.cmd = {'w', 'r','i','s','p', 'r','a','d','d','r','\0'},
	.handler = _cmd_wrispraddr,
	.comment = "wrispreg",
};
