#include "includes.h"

#ifdef CONFIG_PRJSHARE_HEADERFILE
#include CONFIG_PRJSHARE_HEADERFILE
#endif

#define BG_OFFSET	(256)
#define RG_OFFSET	(512)
#define BLC			(16) /* 10bits */
#define GOLDEN_BG	(_golden_bg)
#define GOLDEN_RG	(_golden_rg)

/* default golden value : BG=530, RG=462 */
#ifdef CALI_ID_AWB
static int _golden_bg=530;
static int _golden_rg=462;
#endif
void iq_set_golden_value(int bg, int rg)
{
#ifdef CALI_ID_AWB
	_golden_bg = bg;
	_golden_rg = rg;
#endif
}

#ifdef CALI_ID_AWB
static u8 * _convert_lens_table(u8 * p)
{
	u8 awb[4];
	if(partition_cali_load_sf(CALI_ID_AWB, awb) > 0){
		syslog(LOG_INFO, "golden data: %d, %d\n", GOLDEN_BG, GOLDEN_RG);
		syslog(LOG_INFO, "awb calibration data: %x, %x, %x, %x\n", awb[0], awb[1], awb[2], awb[3]);
		if((awb[0] & 0xc0) == 0xc0){
			int i;
			unsigned long r32, g32, b32;
			b32 = ((awb[0] & 0x3f) << 4) | ((awb[1] & 0xf0) >> 4);
			g32 = ((awb[1] & 0x0f) << 6) | ((awb[2] & 0xfc) >> 2);
			r32 = ((awb[2] & 0x03) << 8) | ((awb[3] & 0xff) >> 0);
			syslog(LOG_DEBUG, "b32 %x, g32 %x, r32 %x\n", b32, g32, r32);

			u32 rg = (r32 - BLC) * 1024 / (g32 - BLC);
			u32 bg = (b32 - BLC) * 1024 / (g32 - BLC);
			syslog(LOG_DEBUG, "bg %d, rg %d\n", bg, rg);

			u32 b_gain = GOLDEN_BG * 1024 / bg;
			u32 r_gain = GOLDEN_RG * 1024 / rg;
			syslog(LOG_INFO, "bgain %x, rgain %x\n", b_gain, r_gain);

			u32 gain;
			if(b_gain < 0x400 || r_gain < 0x400){
				// convert Y
				if(b_gain < r_gain){
					gain = b_gain;
				}else{
					gain = r_gain;
				}
				for(i=0;i<256;i++){
					p[i] = (p[i] + 64) * 1024 / gain - 64;
				}
			}
			for(i=0;i<256;i++){
				// convert bg
				syslog(LOG_DEBUG, "i %d, bg: %x,  before: %x, ", i, b_gain, p[i+BG_OFFSET]);
				p[i+BG_OFFSET] = (p[i+BG_OFFSET] * b_gain) / 1024;
				syslog(LOG_DEBUG, "after %x\n", p[i+BG_OFFSET]);
			}

			for(i=0;i<256;i++){
				// convert rg
				syslog(LOG_DEBUG, "i %d, rg: %x,  before: %x, ", i, r_gain, p[i+RG_OFFSET]);
				p[i+RG_OFFSET] = (p[i+RG_OFFSET] * r_gain) / 1024;
				syslog(LOG_DEBUG, "after %x\n", p[i+RG_OFFSET]);
			}
		}
	}
	return p;
}
#endif

void iq_shading_update(int table_num)
{
		u32 reg_start = libisp_get_laddr();
		u8 *_lens = (u8 *)malloc(768);
		if(_lens == NULL){
				syslog(LOG_ERR, "ERROR: malloc(768) fail in %s\n", __func__);
				return;
		}
		// dtable
		u8* p = (u8*)((u32)_lens | BIT31);
		switch(table_num){
				case SHADING_D_TABLE:
#ifdef CALI_ID_SHADING_DTABLE
						if(partition_cali_load_sf(CALI_ID_SHADING_DTABLE, p) < 0){
								p = (u8 *)(reg_start+0xd00);
						}
#else
						return;
#endif
						break;
				case SHADING_C_TABLE:
#ifdef CALI_ID_SHADING_CTABLE
						if(partition_cali_load_sf(CALI_ID_SHADING_CTABLE, p) < 0){
								p = (u8 *)(reg_start+0xa00);
						}
#else
						return;
#endif
						break;
				case SHADING_A_TABLE:
#ifdef CALI_ID_SHADING_ATABLE
						if(partition_cali_load_sf(CALI_ID_SHADING_ATABLE, p) < 0){
								p = (u8 *)(reg_start+0x700);
						}
#else
						return;
#endif
						break;
				default:
						return;
		}

#ifdef CALI_ID_AWB
		p = _convert_lens_table(p);
#endif

		libisp_set_calib_lenc_dtbl(0, (u32)p);
		libisp_set_calib_lenc_ctbl(0, (u32)p);
		libisp_set_calib_lenc_atbl(0, (u32)p);

		free(_lens);
}

#ifdef CALI_ID_AWB
u32 detect_old_awb_calibration(void)
{
#define OLD_GOLDEN_BG	(538)
#define OLD_GOLEDN_RG	(469)
#define PERC			(10)

	u32 flag=0;	// != 0 : NG, =0 OK. (BG_FLAG << 24| BG_PERC << 16| RG_FLAG << 8| RG_PERC)
	u8 *_awb = (u8 *)malloc(4);
	if(_awb == NULL){
		syslog(LOG_ERR, "ERROR: malloc(4) fail in %s\n", __func__);
		return -1;
	}
	u8* p = (u8*)((u32)_awb | BIT31);
	if(partition_cali_load_sf(CALI_ID_AWB, p) > 0){
		if((p[0] & 0xc0) != 0xc0){
			syslog(LOG_DEBUG, "correct old awb param\n"); 
			syslog(LOG_DEBUG, "awb %x, %x, %x, %x\n", p[0], p[1], p[2], p[3]);

			u32 rg_reg = p[0] << 8 | p[1];
			u32 rg = OLD_GOLEDN_RG * 1024 / rg_reg;
			u32 real_rg = rg + 512;
			u32 real_rg_golden = GOLDEN_RG + 512;
			u8  rg_perc = real_rg * 100 / real_rg_golden;

			u32 bg_reg = p[2] << 8 | p[3];
			u32 bg = OLD_GOLDEN_BG * 1024 / bg_reg;
			u32 real_bg = bg + 512;
			u32 real_bg_golden = GOLDEN_BG + 512;
			u8  bg_perc = real_bg * 100 / real_bg_golden;

			if((rg_perc < (100 - PERC)) || (rg_perc > (100 + PERC))){
				flag = (flag & 0xffff0000) | BIT8 | rg_perc;
			}
			if((bg_perc < (100 - PERC)) || (bg_perc > (100 + PERC))){
				flag = (flag & 0x0000ffff) | BIT24 | bg_perc << 16;
			}

			syslog(LOG_DEBUG, "rg_reg : %x, rg %d, perc %d\n", rg_reg, rg, rg_perc);
			syslog(LOG_DEBUG, "bg_reg : %x, bg %d, perc %d\n", bg_reg, bg, bg_perc);
			syslog(LOG_DEBUG, "result 0x%x\n", flag);
			if(flag){
				syslog(LOG_WARNING, "wrong awb calibration data, not used\n");
			}else{
				syslog(LOG_WARNING, "used old awb calibration data\n");
				libisp_set_calib_awb(0, (u32)p);
			}
		}else{
			syslog(LOG_DEBUG, "new calibration data, skip\n");
		}
	}

	free(_awb);

	return flag;
}
#endif


void iq_calibration_update(void)
{
   	u32 reg_start = libisp_get_laddr(); 
	u8 *_lens = (u8 *)malloc(768);
	if(_lens == NULL){
		syslog(LOG_ERR, "ERROR: malloc(768) fail in %s\n", __func__);
		return;
	}

#ifdef CALI_ID_SHADING_DTABLE
	// dtable
	u8* p = (u8*)((u32)_lens | BIT31);
	if(partition_cali_load_sf(CALI_ID_SHADING_DTABLE, p) < 0){
		p = (u8 *)(reg_start+0xd00);
	}
#ifdef CALI_ID_AWB
	p = _convert_lens_table(p);
#endif
	libisp_set_calib_lenc_dtbl(0, (u32)p);
#endif

#ifdef CALI_ID_SHADING_ATABLE
	// atable
	if(partition_cali_load_sf(CALI_ID_SHADING_ATABLE, p) < 0){
		p = (u8 *)(reg_start+0x700);
	}
#ifdef CALI_ID_AWB
	p = _convert_lens_table(p);
#endif
	libisp_set_calib_lenc_atbl(0, (u32)p);
#endif

#ifdef CALI_ID_SHADING_CTABLE
	// ctable
	if(partition_cali_load_sf(CALI_ID_SHADING_CTABLE, p) < 0){
		p = (u8 *)(reg_start+0xa00);
	}
#ifdef CALI_ID_AWB
	p = _convert_lens_table(p);
#endif
	libisp_set_calib_lenc_ctbl(0, (u32)p);
#endif

	free(_lens);

#ifdef CALI_ID_AWB
	detect_old_awb_calibration();
#endif
}
