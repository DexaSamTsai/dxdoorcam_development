#include "includes.h"

static int ispcmd_help(void)
{
	printf("ISP debug commands:\n");
	printf("    isp br|brightness INTEGER: set isp brightness\n");
	printf("    isp sde INTEGER: set isp sde\n");
	printf(" 		0: no sde\n");
	printf(" 		1: monochrome\n");
	printf(" 		2: negative\n");
	printf(" 		3: sepia\n");
	printf(" 		4: sketch\n");
	printf(" 		5: watercolor\n");
	printf(" 		6: ink\n");
	printf(" 		7: aqua\n");
	printf(" 		8: blackboard\n");
	printf(" 		9: cartoon\n");
	printf(" 		10: colorink\n");
	printf(" 		11: poster\n");
	printf(" 		12: soloarize\n");
	printf(" 		13: whiteboard\n");
	printf("    isp dn|daynight INTEGER, INTEGER: set isp daynight mode, autofps enable\n");
	printf(" 		0: day\n");
	printf(" 		1: night\n");
	printf(" 		2: auto switch\n");
	printf("    isp out INTEGER: set isp output channel\n");
	printf(" 		0: channel 0\n");
	printf(" 		1: channel 1\n");
	printf("    isp sccbrd SENSORID ADDR: read sensor sccb\n");
	printf("    isp sccbwr SENSORID ADDR DATA: write sensor sccb\n");
	printf("    isp lightfreq vs_id value: change lightfreq for anti-banding\n");
	printf(" 		0: anti-banding off\n");
	printf(" 		1: anti-banding on for 50hz light source\n");
	printf(" 		2: anti-banding on for 60hz light source\n\n");
	printf("    isp mirror vs_id value: \n");
	printf(" 		0: horizontal mirror off\n");
	printf(" 		1: horizontal mirror on\n");
	printf("    isp flip vs_id value: \n");
	printf(" 		0: horizontal flip off\n");
	printf(" 		1: horizontal flip on\n");
	printf("    isp ir value: \n");
	printf(" 		0: ir on\n");
	printf(" 		1: ir off\n");
	printf("    isp sat_thr value: \n");
	printf(" 		value: threshold of auto saturation sat_thr_low<<16|sat_thr_high\n");
	printf("    isp coeff_thr value: \n");
	printf(" 		value: threshold of half coeff control coeff_thr_low<<16|coeff_thr_high\n");
	printf("	isp hdr value:");
	printf(" 		0: hdr off\n");
	printf(" 		1: hdr on\n");
	printf("	isp hist\n");
	printf("	isp roi enable, x1, y1, width, height\n");
	printf("    isp sat|saturation INTEGER: set isp saturation\n");
	printf("    isp con|contrast INTEGER: set isp contrast\n");
	printf("    isp y\n");
	printf("    isp sha|sharpness INTEGER: set isp sharpness\n");
	printf("    isp wdr INTEGER: set isp wdr strength\n");
	printf("    isp mctf INTEGER: set isp MCTF\n");

	printf("  note: INTEGER: oct or hex(with 0x or 0X prefix)\n");
	return 0;
}

int isp_debug_cmd(int argc, char ** argv)
{
	if(argc < 2){
		return ispcmd_help();
	}

	if(!strcmp(argv[1], "br") || !strcmp(argv[1], "brightness")){
		if(argc < 4){
			return ispcmd_help();
		}
		//int v = strtol(argv[2], NULL, 0);
		//return libisp_set_brightness(0, v);
		int vs_id = strtol(argv[2],NULL,0);
		u32 brightness = strtol(argv[3],NULL,0);
		return libvs_set_brightness(vs_id,  brightness);
	}else if (!strcmp(argv[1], "dn") || !strcmp(argv[1], "daynight")){
		if(argc < 4){
			return ispcmd_help();
		}
	    int v = strtol(argv[2], NULL, 0);
	    int f = strtol(argv[3], NULL, 0);
		if (v==1){
			libisp_set_autofps(0,f,15,3);
		}else if(v==0){
			libisp_set_autofps(0,f,24,5);
		}
	    return libisp_set_daynight(0, v);
#ifdef PIN_IRLED_CTL
	}else if (!strcmp(argv[1], "ir")){
		if(argc < 3){
			return ispcmd_help();
		}
	    int v = strtol(argv[2], NULL, 0);
		if (v){
			libgpio_write(PIN_IRLED_CTL, 1);
		}else{
			libgpio_write(PIN_IRLED_CTL, 0);
		}
#endif
	}else if (!strcmp(argv[1], "sde") ){
		if(argc < 3){
			return ispcmd_help();
		}
	    int v = strtol(argv[2], NULL, 0);
	    return libisp_set_sde(0, v);
	}else if (!strcmp(argv[1], "out") ){
		if(argc < 3){
			return ispcmd_help();
		}
	    int v = strtol(argv[2], NULL, 0);
		return libisp_set_channel(0, v);
	}else if (!strcmp(argv[1], "sccbrd") ){
		if(argc < 4){
			return ispcmd_help();
		}
	    int sensorid = strtol(argv[2], NULL, 0);
	    int v = strtol(argv[3], NULL, 0);
		printf("RD: 0x%x\n", libisp_sccbrd(0, sensorid, v) );
		return 0;
	}else if (!strcmp(argv[1], "sccbwr") ){
		if(argc < 4){
			return ispcmd_help();
		}
	    int sensorid = strtol(argv[2], NULL, 0);
	    int reg = strtol(argv[3], NULL, 0);
	    int val = strtol(argv[4], NULL, 0);
		printf("WR: 0x%x\n", libisp_sccbwr(0, sensorid, reg, val) );
		return 0;
	}else if (!strcmp(argv[1], "lightfreq") ){
		if (argc < 3){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 lightfreq = strtol(argv[3],NULL,0);
		return libvs_set_lightfreq(vs_id, lightfreq);
	}else if (!strcmp(argv[1], "mirror") ){
		if (argc < 3){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 mirror = strtol(argv[3],NULL,0);
    	return libvs_set_mirror( vs_id,  mirror);
	}else if (!strcmp(argv[1], "flip") ){
		if (argc < 3){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 flip = strtol(argv[3],NULL,0);
    	return libvs_set_flip( vs_id,  flip);
	}else if (!strcmp(argv[1], "coeff") ){
			if (argc < 3){
					return ispcmd_help();
			}
			u32 val = strtol(argv[2],NULL,16);
    		return libisp_set_lowlux_coeff(0,  val);
	}else if (!strcmp(argv[1], "sat_thr") ){
		if (argc < 3){
			return ispcmd_help();
		}
		u32 val = strtol(argv[2],NULL,16);
    	return libisp_set_sat_thr(0,  val);
	}else if (!strcmp(argv[1], "coeff_thr") ){
		if (argc < 3){
			return ispcmd_help();
		}
		u32 val = strtol(argv[2],NULL,16);
    	return libisp_set_coeff_thr(0,  val);
	}else if (!strcmp(argv[1], "hdr")){
		if(argc < 3){
			return ispcmd_help();
		}
		u32 val = strtol(argv[2], NULL, 0);
		libisp_set_hdr(0, val);
		return 0;
	}else if(!strcmp(argv[1], "hist")){
		if(argc < 2){
			return ispcmd_help();
		}
		u32 *pbuf = NULL; 
		pbuf = libisp_get_hist_buf(0);
		if(pbuf){
			syslog(LOG_ERR, "histogram : [0x10] %d, [0x20] %d, [0x30] %d, [0x40] %d ...\n", pbuf[0x10], pbuf[0x20], pbuf[0x30], pbuf[0x40]);
		}
		return 0;
	}else if(!strcmp(argv[1], "roi")){
		if(argc < 7){
			return ispcmd_help();
		}
		t_roi roi;
		roi.enable = strtol(argv[2],NULL,0);
		roi.x1 = strtol(argv[3],NULL,0);
		roi.y1 = strtol(argv[4],NULL,0);
		roi.width = strtol(argv[5],NULL,0);
		roi.height = strtol(argv[6],NULL,0);

		dc_flush_range((u32)&roi, (u32)&roi + sizeof(roi));

		return libisp_set_roi(0, (u32) &roi);
	}else if(!strcmp(argv[1], "contrast") || !strcmp(argv[1], "con")){
		if(argc < 4){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 contrast = strtol(argv[3],NULL,0);
		return libvs_set_contrast(vs_id,  contrast);
	}else if(!strcmp(argv[1], "saturation") || !strcmp(argv[1], "sat")){
		if(argc < 4){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 saturation = strtol(argv[3],NULL,0);
		return libvs_set_saturation(vs_id,  saturation);
	}else if(!strcmp(argv[1], "y")){
		if(argc < 2){
			return ispcmd_help();
		}
		int y = libisp_get_y(0);
		syslog(LOG_ERR, "MeanY : %d\n", y);
		return 1;
	}else if(!strcmp(argv[1], "wdr")){
		if(argc < 4){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 wdr = strtol(argv[3],NULL,0);
		return libvs_set_wdr(vs_id,  wdr);
	}else if(!strcmp(argv[1], "mctf")){
		if(argc < 4){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 mctf = strtol(argv[3],NULL,0);
		return libvs_set_mctf(vs_id,  mctf);
	}else if(!strcmp(argv[1], "sharpness") || !strcmp(argv[1], "sha")){
		if(argc < 4){
			return ispcmd_help();
		}
		int vs_id = strtol(argv[2],NULL,0);
		u32 sharpness = strtol(argv[3],NULL,0);
		return libvs_set_sharpness(vs_id,  sharpness);
	}else{
		return ispcmd_help();
	}

	return 0;
}

CMDSHELL_DECLARE(isp)
	.cmd = {'i', 's', 'p', '\0'},
	.handler = isp_debug_cmd,
	.comment = "isp debug, run 'isp help' for detailed",
};
