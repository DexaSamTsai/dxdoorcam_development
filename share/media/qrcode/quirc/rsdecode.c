/* Complete RS decoder.
Galois fields: GF(2**8), starting indices for the generator polynomial is [0~254].
*/

#include "quirc_internal.h"

#include <string.h>
#include <stdlib.h>
#include "rsdecode.h"

#define POLY_SIZE 256



/*in GF(2^8), m(x)=x^8+x^4+x^3+x^2+1, g(x)=x 
the mpoly is 0x1D: 0x0001 1101*/
void inittable_gf256(gf_256 *gf,uint8_t mpoly)
{
  int i = 0;
  unsigned p = 1;

  //get the table of g(x)^k.
  for(i=0;i<255;i++){
    gf->exp[i] = p;
    p = (p<<1);
    if(p & 0x100)
       p ^= (0x100 | mpoly);
  }
  gf->exp[255]=1;

//method 2
//   for(i=0;i<255;i++){
//     gf->exp[i] = p;
//     p=((p<<1)^(-(p>>7)&mpoly))&0xFF;
//   }
//   gf->exp[255]=1;


  /*get the log table*/
  for(i=0;i<256;i++)//
     gf->log[gf->exp[i]]=i;
  /*definite gf->log[0]=0 below.*/
  gf->log[0]=0;


/*get inverse table of exp*/
  for(i=0;i<256;i++)
     gf->inverse_exp[i]=gf->exp[255-gf->log[i]];
  
}

/*rs decoder correction */
/*calculate syndromes */
static int codeword_syndrome(const uint8_t *data, int len, int nerrl, const gf_256 *gf, uint8_t *s)
{
  int snozero = 0;
  int i, j;
  for(i=0;i<nerrl;i++){
    unsigned si = 0;
    for(j=0;j<len;j++)
       si=data[j]^(si==0?0:gf->exp[((int)gf->log[si]+i)%255]);//sum(data[j]*(x^i)^j)
       //si^=(data[j]==0?0:gf->exp[((int)gf->log[data[j]]+i)%255]);//error, which cause the sigma and R to be zero
    s[i]=si;
    if(si)
       snozero ++;
  }


  return snozero;
}

/*Berlekamp-Massey algorithm for finding error locator polynomials.
Here, we use the Cramer's rule for solving a system of linear equations*/
static int berlekamp_massey(const uint8_t *s, int nerrl, const gf_256 *gf, uint8_t *sigma)
{
  memset(sigma, 0, (nerrl+1)*sizeof(uint8_t));
  sigma[0] = 1;

  uint8_t tempsigma[256];
  memcpy(tempsigma, sigma, nerrl+1);
  
  int nerr=0;
  int k=0;
  int n,i;
  for(n=1; n<=nerrl; n++)
  {
    //multiple polynomial by x, step is n-k
    memmove(tempsigma+1, tempsigma, (n-k)*sizeof(uint8_t));
    tempsigma[0]=0;
    unsigned d = 0;
    for(i=0;i<=nerr;i++)
       d^=(sigma[i]==0||s[n-1-i]==0 ? 0:gf->exp[(gf->log[sigma[i]] + gf->log[s[n-1-i]])%255]);//sum(sigma[i]S[t-i])

    if(d!=0)
    {
      //uint8_t arc_d = gf->log[d];
      if(nerr<n-k)
      {
        for(i=0;i<=n-k;i++)
        {
           uint8_t tempsigma_t = tempsigma[i];
           tempsigma[i] = (sigma[i]==0?0:gf->exp[((int)gf->log[sigma[i]]+(255-gf->log[d]))%255]);
	   sigma[i]^= (tempsigma_t==0?0:gf->exp[(gf->log[tempsigma_t] + gf->log[d])%255]);
        }
        int temp=n-k; k=n-nerr; nerr=temp;
      }
      else
      {
         for(i=0; i<=nerr;i++)
           sigma[i]^=(tempsigma[i]==0?0:gf->exp[((int)gf->log[tempsigma[i]]+gf->log[d])%255]);
      }
    }
  }


  return nerr;
}
/*construct error value polynomial*/
static void poly_mult(const uint8_t *s, const uint8_t *sigma, int nerrl, int nerr, uint8_t *r, const gf_256 *gf)
{
   memset(r, 0, nerrl*sizeof(uint8_t));
   //int err = nerr+1 < nerrl ? nerr+1:nerrl;
   int i, j;
   for(i=0; i<=nerr; i++)
   {
      if(sigma[i]!=0){
         int k = nerrl-i;
         for(j=0; j<k; j++)
            r[i+j]^=(s[j]==0?0:gf->exp[((int)gf->log[s[j]]+gf->log[sigma[i]])%255]);
      }
   }

}

/*calculate error location by the error-location polynomials*/
static int calculate_pos(const uint8_t *sigma, int len, int nerr, uint8_t *pos, const gf_256 *gf)
{
   uint8_t rootnum = 0;
   int j, i;
   for(j=0;j<len;j++)
   {
      uint8_t delta=0;
      uint8_t expji=0;
      for(i=0;i<=nerr;i++)
      {
         delta^=(sigma[nerr-i]==0?0:gf->exp[(gf->log[sigma[nerr-i]]+expji)%255]);
         //expji = i*j;//which has over flow issue
         //expji = gf->log[gf->exp[(expji+j)%255]];//
         expji = (expji+j)%255;
      }
      if(delta==0)
         pos[rootnum++]=j;
   }
   return rootnum;
}
/*calculate the value of error and update the error bits*/
static void calculate_errs(const uint8_t *r, const uint8_t *sigma, const uint8_t *pos, uint8_t *data, int len, int nerrl, int nerr, const gf_256 *gf)
{
	int i, j;
   for(i=0;i<nerr; i++)
   {
      /*the pos index is from end to 0*/
      uint8_t numerator = 0;//int type will cause over flow issue
      uint8_t expj = 0;
      for(j=0;j<nerrl;j++)
      {
         numerator^=(r[j]==0?0:gf->exp[(gf->log[r[j]]+expj)%255]);
         //expj = (255-pos[i])*j;//use this will have over flow issue. it will be big than 255
         expj = (expj+(255-pos[i]))%255;
      }

      uint8_t denominator = 0;
      expj = 255-pos[i];
      for(j=1;j<nerrl;j+=2)
      {
         denominator^=(sigma[j]==0?0:gf->exp[(gf->log[sigma[j]]+expj)%255]);
         //expj = (255-pos[i])*j;
         expj = gf->log[gf->exp[(expj+((255-pos[i])*2)%255)%255]];
      }

      //add the error value to error pos
//      uint8_t srcdata = data[len-1-pos[i]];
      data[len-1-pos[i]]^=(numerator==0?0:gf->exp[(gf->log[numerator]+255-gf->log[denominator])%255]);

   }

}

int rs_decode(uint8_t *data,int len, int nerrl, const gf_256 *gf)
{
    
    //calculate s[i]
    uint8_t s[POLY_SIZE];
    int zeronum = 0;
    zeronum = codeword_syndrome(data, len, nerrl, gf, s);
    if(zeronum == 0)
    {
#ifdef PCTEST_DBG
      printf("no error in rs decode\n");
#endif
      return 0;
    }
    
    //have error
    uint8_t sigma[POLY_SIZE]; 
    int errornum = 0;
    errornum = berlekamp_massey(s, nerrl, gf, sigma);
    if(errornum == 0)
    {
#ifdef PCTEST_DBG
    	printf("There are so many errors, which is more than the max correction number");
#endif
        //return -1;//mean there are more than nerrl error
    }

    //calculate error value polynomial
    uint8_t r[POLY_SIZE];
    poly_mult(s, sigma, nerrl, errornum, r, gf);

    //find error location/value, correct the codeword
    uint8_t errpos[POLY_SIZE];
    int posnum = 0;
    posnum = calculate_pos(sigma, len, errornum, errpos, gf);
    calculate_errs(r, sigma, errpos, data, len, nerrl, posnum, gf);

    if(posnum != errornum)
    {
#ifdef PCTEST_DBG
       printf("Decoding error! The location number of polynomial:%d is different with the true error number:%d\n", posnum, errornum);
#endif
       return -1; //means there is decoding error
    }

    //after correction, use the syndrome to check them again
    zeronum = codeword_syndrome(data, len, nerrl, gf, s);
    if(zeronum)
    {
#ifdef PCTEST_DBG
       printf("Correction failed, the corrected codeword non-zero syndrome number is: %d\n",zeronum);
#endif
       return -1;
    }
    else
       return posnum;
}


