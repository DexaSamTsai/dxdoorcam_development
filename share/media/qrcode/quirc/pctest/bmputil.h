#ifndef BMP_UTIL_H_
#define BMP_UTIL_H_

#include <stdio.h>

#define BMP_FLAG 19778

int bmp_isbitmap(FILE *fp);
long bmp_getwidth(FILE *fp);
long bmp_getheight(FILE *fp);
unsigned short bmp_getbit(FILE *fp);
unsigned int bmp_getoffset(FILE *fp);
unsigned char bmp_rgb2gray(unsigned char r, unsigned char g, unsigned char b);
int bmp_convert2graydata(FILE *fp, unsigned char* pgray);
int bmp_savegraydata(unsigned char* pgray, int width, int height);

#endif
