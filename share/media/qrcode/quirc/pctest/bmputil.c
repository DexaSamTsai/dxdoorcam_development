#include "bmputil.h"

int bmp_isbitmap(FILE *fp)
{
	unsigned short s;
	fread(&s, 1, 2, fp);
	if (s == BMP_FLAG)
		return 1;
	else
		return 0;
}

long bmp_getwidth(FILE *fp)
{
    long width;
    fseek(fp,18,SEEK_SET);
    fread(&width,1,4,fp);
    return width;
}

long bmp_getheight(FILE *fp)
{
    long height;
    fseek(fp,22,SEEK_SET);
    fread(&height,1,4,fp);
    return height;
}

unsigned short bmp_getbit(FILE *fp)
{
    unsigned short bit;
    fseek(fp,28,SEEK_SET);
    fread(&bit,1,2,fp);
    return bit;
}

unsigned int bmp_getoffset(FILE *fp)
{
    unsigned int OffSet;
    fseek(fp,10L,SEEK_SET);
    fread(&OffSet,1,4,fp);
    return OffSet;
}

unsigned char bmp_rgb2gray(unsigned char r, unsigned char g, unsigned char b)
{
	unsigned char gray = (int)( (float)b * 0.114 + (float)g * 0.587 + (float)r * 0.299 );
	return gray;
}

int bmp_convert2graydata(FILE *fp, unsigned char* pgray)
{
	if(pgray == NULL)
		return -1;

	long width, height;
	width = bmp_getwidth(fp);
	height = bmp_getheight(fp);

	int stride;
	stride = (24 * width + 31) / 8;
	stride = stride / 4 * 4;

	unsigned char* pdata = (unsigned char *) malloc(width*height);
	unsigned char* pix = (unsigned char *) malloc(stride);

	//read bmp
	int i, j = 0;
	fseek(fp, bmp_getoffset(fp), SEEK_SET);
	for (j = 0; j < height; j++)
	{
		//read line
		fread(pix, 1, stride, fp);
		for (i = 0; i < width; i++)
		{
			//convert
			*(pdata + i + width*j) = bmp_rgb2gray(pix[i * 3 + 2], pix[i * 3 + 1], pix[i * 3]);
		}
	}

	//swap data
	for(j = height-1; j >= 0; j--)
	{
		for (i = 0; i < width; i++)
		{
			*(pgray + i + width*j) = *(pdata + i + width*(height-1-j));
		}
	}

	free(pdata);
	free(pix);
	return 0;
}

int bmp_savegraydata(unsigned char* pgray, int width, int height)
{
	FILE* fp = fopen("./data/compare/bmpgraydata.txt", "w+");
	if(fp == NULL)
	{
		printf("save gray data failed\n");
		return -1;
	}

	int i,j;
	unsigned char c;
	for(j = 0; j < height; j++)
	{
		for(i = 0; i < width; i++)
		{
			c = *(pgray+ i + j * width);
			//fputc(c, fp);
			fprintf(fp,"%d ", c);
		}

		//fputc('\n', fp);
		fprintf(fp,"\n");
	}

	fclose(fp);
	printf("save gray data success\n");
	return 0;
}












