#include "includes.h"
#include "../quirc.h"
#include "../quirc_internal.h"

#define CLOCK_TO_MS(c) ((c) / (CLOCKS_PER_SEC / 1000))
//#define BMPMATRIX_SHOW
#define IDENTIFYTIME_SHOW

unsigned char dbg_img[1280][720];
unsigned char dbg_img_gray[1280][720];
char * ori_fname;
char dbgfname[1024];
char refname[1024];
unsigned char re_img[512][512];
unsigned int reimg_size;

void dbgimg_name(char * filepath, char * newfname, char * postfix)
{
	strcpy(newfname, filepath);
	char * dot = strrchr(newfname, '.');
	if(dot){
		memmove(dot + strlen(postfix), dot, strlen(dot) + 1);
		memcpy(dot, postfix, strlen(postfix));
	}else{
		strcat(newfname, postfix);
	}

	ori_fname = filepath;
}

void dbg_save(void)
{
	printf("SaveGray:%s\n", dbgfname);

	FILE * fp = fopen(ori_fname, "rb");
	if(fp == NULL){
		printf("ERROR: cannot open read\n");
		return;
	}
	FILE * fpo = fopen(dbgfname, "wb");
	if(fpo == NULL){
		printf("ERROR: cannot open write\n");
		return;
	}
	FILE * fpre = fopen(refname, "wb");
	if(fpre == NULL){
		printf("ERROR: cannot open write\n");
		return;
	}
	if (!bmp_isbitmap(fp)) {
		printf("ERROR: not bmp file");
		return;
	}
	int width, height, bitcount;
	width = bmp_getwidth(fp);
	height = bmp_getheight(fp);
	int hlen = bmp_getoffset(fp);

	unsigned char buf[1024];
    fseek(fp,0,SEEK_SET);
	fread(buf, 1, hlen, fp);
	fwrite(buf, 1, hlen, fpo);
	fwrite(buf, 1, hlen, fpre);
	fclose(fp);

	int hratio = height / reimg_size;
	int wratio = width / reimg_size;
	if(hratio < wratio){
		wratio = hratio;
	}

	int i, j;
	for(j = height-1; j >= 0; j--)
	{
		for (i = 0; i < width; i++)
		{
			if(dbg_img[j][i] == QUIRC_PIXEL_BLACK){
				buf[0] = 0;
				buf[1] = 0;
				buf[2] = 0;
			}else if(dbg_img[j][i] == QUIRC_PIXEL_WHITE){
				buf[0] = 0xff;
				buf[1] = 0xff;
				buf[2] = 0xff;
			}else if(dbg_img[j][i] == QUIRC_PIXEL_STONEC){
				buf[0] = 0x00;
				buf[1] = 0x00;
				buf[2] = 0xff;
			}else if(dbg_img[j][i] == QUIRC_PIXEL_CELL){
				if(dbg_img_gray[j][i] == QUIRC_PIXEL_BLACK){
					buf[0] = 0x00;
					buf[1] = 0xff;
					buf[2] = 0xff;
				}else{
					buf[0] = 0x00;
					buf[1] = 0x4f;
					buf[2] = 0x4f;
				}
			}else{
				buf[0] = 0x7f;
				buf[1] = 0x7f;
				buf[2] = 0x7f;
			}
			fwrite(buf, 1, 3, fpo);
			//re
			if(i < wratio * reimg_size && j < wratio * reimg_size){
				if(re_img[j/wratio][i/wratio] == 1){
					buf[0] = 0;
					buf[1] = 0;
					buf[2] = 0;
				}else{
					buf[0] = 0xff;
					buf[1] = 0xff;
					buf[2] = 0xff;
				}
			}else{
				buf[0] = 0xff;
				buf[1] = 0xff;
				buf[2] = 0xff;
			}
			fwrite(buf, 1, 3, fpre);
		}
		int stride = (24 * width + 31) / 8;
		stride = stride / 4 * 4;
		if(stride - 3*width > 0){
			fwrite(buf, 1, stride - 3*width, fpo);
		}
	}

	fclose(fpo);
	fclose(fpre);
	printf("SaveGray:%s %s Done\n", dbgfname, refname);
}

int qridentify_handler(char* filepath)
{
	//read bmp file
	FILE* fp = fopen(filepath, "rb");
	if (fp == NULL) {
		printf("couldn't open file");
		return -1;
	}

	if (!bmp_isbitmap(fp)) {
		printf("not bmp file");
		return -1;
	}

	int width, height, bitcount;
	width = bmp_getwidth(fp);
	height = bmp_getheight(fp);
	bitcount = bmp_getbit(fp);
	printf("bmp para: %d, %d, %d\n", width, height, bitcount);

	if (bitcount != 24) {
		printf("not support %d bit bmp, need be 24 bit", bitcount);
		return -1;
	}

	//covert to gray
	unsigned char* pgray = malloc(width * height);
	bmp_convert2graydata(fp, pgray);

	dbgimg_name(filepath, dbgfname, "_dbg");
	dbgimg_name(filepath, refname, "_re");

#ifdef IDENTIFYTIME_SHOW
	clock_t starttime, endtime;
	starttime = clock();
#endif

	//qr
	struct quirc *qr;
	qr = quirc_new();
	if (!qr) {
		perror("faild to alloc memory");
		return -2;
	}

	if (quirc_resize(qr, width, height, 0) < 0) {
		perror("faild to alloc memory");
		return -2;
	}

	uint8_t* image = quirc_begin(qr, NULL, NULL);
	memcpy(image, pgray, width * height);
#ifdef BMPMATRIX_SHOW
	bmp_savegraydata(image, width, height);
#endif
	/**************************/
	quirc_end(qr);

	int num_codes = quirc_count(qr);
	printf("code num:%d\n", num_codes);

	int i;
	for (i = 0; i < num_codes; i++) {
		struct quirc_code code;
		struct quirc_data data;
		quirc_decode_error_t err;

		quirc_extract(qr, i, &code);

		err = quirc_decode(&code, &data);
		if (err)
			printf("decode failed: %s\n", quirc_strerror(err));
		else
			printf("data: %s\n", data.payload);

	}

	dbg_save();

#ifdef IDENTIFYTIME_SHOW
	endtime = clock();
	printf("identify time:%ldms\n", CLOCK_TO_MS(endtime-starttime));
#endif

	//free
	quirc_destroy(qr);
	free(pgray);
	fclose(fp);
}

int main(int argc, char ** argv)
{
	printf("QR code identification start...\n");
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <*.bmp>\n", argv[0]);
		return -1;
	}

	printf("input bmp file:%s\n", argv[1]);
	qridentify_handler(argv[1]);


	printf("QR identification end.\n");
	return 0;
}
