#ifndef RSDECODE_H
#define RSDECODE_H

typedef struct t_galois_field{
  /*In GF, any polynomial is represnted by g(x)^k.
  Get an exp table based on it*/
  uint8_t exp[256];
  /*In order to get the value of k when g(x)^k is known
   log table is Got*/
  uint8_t log[256];
 /*Inverse table is got, used in polynomial div*/
  uint8_t inverse_exp[256];
}gf_256;

void inittable_gf256(gf_256 *gf,uint8_t mpoly);
int rs_decode(uint8_t *data,int len, int nerrl, const gf_256 *gf);

#endif
