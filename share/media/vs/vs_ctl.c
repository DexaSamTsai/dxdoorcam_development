#include "includes.h"

#define VENC_MAX_STREAMS	8 

static t_videostream g_vscfg[VENC_MAX_STREAMS];

#ifdef __LINUX__
extern char * ddr_buffer;
extern char * iram_buffer;
#endif

int libvs_init(int vs_id)
{
	if(vs_id >= VENC_MAX_STREAMS){
		return 1;
	}

	if(g_vscfg[vs_id].status != VS_STATUS_IDLE && g_vscfg[vs_id].status != VS_STATUS_STOPPED){
		return 0;
	}
	memset(&g_vscfg[vs_id], 0, sizeof(t_videostream));

	//alloc event
	g_vscfg[vs_id].notempty_evt = ertos_event_create();
	if(g_vscfg[vs_id].notempty_evt == NULL){
		return 2;
	}

	int ret = mpu_cmd(MPUCMD_VS_INIT | (vs_id << 12), 0);
	
	g_vscfg[vs_id].status = VS_STATUS_IDLE;

	return ret;
}

int libvs_start(int vs_id)
{
	if(vs_id >= VENC_MAX_STREAMS){
		return 1;
	}

	if(g_vscfg[vs_id].status != VS_STATUS_IDLE){
		return 0;
	}

	g_vscfg[vs_id].lastoffset = 0;
	g_vscfg[vs_id].lasttotlen = 0;
	g_vscfg[vs_id].cur_frm = NULL;

	int i;
	for(i=0; i<VS_FRMBUF_CNT; i++){
		g_vscfg[vs_id].last_addrs[i] = NULL;
		g_vscfg[vs_id].last_frms[i] = NULL;
	}
	g_vscfg[vs_id].last_cnt = 0;

	//reg event
	if(mpu_event_reg(MPUEVT_VS0_NOTEMPTY + vs_id, g_vscfg[vs_id].notempty_evt) != 0){
		return 4;
	}

	int ret = mpu_cmd(MPUCMD_VS_START | (vs_id << 12), 0);
	if(ret != 0){
		//if fail, delete event
#ifndef __LINUX__
		mpu_event_dereg(MPUEVT_VS0_NOTEMPTY + vs_id);
#endif
	}else{
		g_vscfg[vs_id].status = VS_STATUS_RUNNING;
	}
	
	return ret;
}

int libvs_stop(int vs_id)
{
	if(vs_id >= VENC_MAX_STREAMS){
		return 1;
	}

	if(g_vscfg[vs_id].status == VS_STATUS_IDLE){
		return 2;
	}

	int i;
	int ret;
	for(i=0; i<20; i++){
		ret = mpu_cmd(MPUCMD_VS_STOP | (vs_id << 12), 0);

		if(ret == 0){
			mpu_event_dereg(MPUEVT_VS0_NOTEMPTY + vs_id);
		
			g_vscfg[vs_id].status = VS_STATUS_IDLE;

			ertos_event_signal(g_vscfg[vs_id].notempty_evt);
			break;
		}else if(ret < 0){
			break;
		}else{
#ifdef __LINUX__
			ertos_timedelay(50); //delay 100ms everytime.
#else
			ertos_timedelay(10); //delay 100ms everytime.
#endif
		}
	}
	
	g_vscfg[vs_id].status = VS_STATUS_STOPPED;

	return ret;
}

int libvs_exit(int vs_id)
{
	if(vs_id >= VENC_MAX_STREAMS){
		return 1;
	}
	
	if(g_vscfg[vs_id].status == VS_STATUS_IDLE){
		return 2;
	}

	ertos_event_delete(g_vscfg[vs_id].notempty_evt);

	int ret = mpu_cmd(MPUCMD_VS_EXIT | (vs_id << 12), 0);

	g_vscfg[vs_id].lastoffset = 0;
	g_vscfg[vs_id].lasttotlen = 0;
	g_vscfg[vs_id].cur_frm = NULL;

	int i;
	for(i=0; i<VS_FRMBUF_CNT; i++){
		g_vscfg[vs_id].last_addrs[i] = NULL;
		g_vscfg[vs_id].last_frms[i] = NULL;
	}
	g_vscfg[vs_id].last_cnt = 0;

	g_vscfg[vs_id].status = VS_STATUS_IDLE;

	return ret;
}

FRAME * libvs_get_frame(int vs_id, u32 timeout)
{
	int ret = mpu_cmd(MPUCMD_VS_GETFRM | (vs_id << 12), 0);
	FRAME * frame = NULL;

	if(ret == 2){
		return NULL;
	}
	if(ret != 0){
		goto FRAME_DONE;
	}

	if(g_vscfg[vs_id].status != VS_STATUS_IDLE){
		//no frame now, wait data ready
		ertos_event_wait(g_vscfg[vs_id].notempty_evt, timeout);

		if(g_vscfg[vs_id].status != VS_STATUS_IDLE){
			ret = mpu_cmd(MPUCMD_VS_GETFRM | (vs_id << 12), 0);
			if(ret == 0 || ret == 2){
				return NULL;
			}
		}
	}

FRAME_DONE:
	frame = (FRAME *)ret;
#ifdef __LINUX__
	//physical to virtual
	frame = (FRAME *)(iram_buffer + ret - 0x90000000);
	frame->addr = (u32)(ddr_buffer + frame->addr - 0x90100000);
	if(frame->size1){
		frame->addr1 = (u32)(ddr_buffer + frame->addr1 - 0x90100000);
	}
#endif
	return frame;
}

int libvs_get_data(int vs_id, char **buf, int maxlen, u32 *flags, u32 *frm_len, u32 *ts, u32 timeout)
{
	u32 tmpflags = 0;
	int ret;

	if(maxlen == 0){
		return 0;
	}

	if(g_vscfg[vs_id].last_cnt >= VS_FRMBUF_CNT){
		syslog(LOG_ERR, "[ERR] vs_get_data: keep too many frames\n");
		return 0;
	}

	if(g_vscfg[vs_id].cur_frm)
	{
		if(g_vscfg[vs_id].lastoffset && (g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].lasttotlen)){
			syslog(LOG_ERR, "[ERR] vs_get_data: lastoffset > lasttotlen\n");
			return 0;
		}
	}
	
	if(g_vscfg[vs_id].cur_frm == NULL)
	{
		g_vscfg[vs_id].cur_frm = libvs_get_frame(vs_id, timeout);
		if(g_vscfg[vs_id].cur_frm == NULL)
		{
			return 0;
		}
		g_vscfg[vs_id].lastoffset = 0;
	}

	if(g_vscfg[vs_id].lastoffset == 0)
	{
		tmpflags |= VDATA_FLAG_FIRSTPKT;
		if(g_vscfg[vs_id].cur_frm->type == FRAME_TYPE_VIDEO_I){
			tmpflags |= VDATA_FLAG_I;
		}
		if(g_vscfg[vs_id].cur_frm->type == FRAME_TYPE_VIDEO_IDR){
			tmpflags |= VDATA_FLAG_IDR;
		}
		g_vscfg[vs_id].lasttotlen = g_vscfg[vs_id].cur_frm ->size + (g_vscfg[vs_id].cur_frm->addr1 ? g_vscfg[vs_id].cur_frm ->size1 : 0) ;
	}

	*frm_len = g_vscfg[vs_id].lasttotlen;
	if(g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].cur_frm->size){
		*buf = (char *)(g_vscfg[vs_id].cur_frm->addr1 + g_vscfg[vs_id].lastoffset - g_vscfg[vs_id].cur_frm->size) ;
		ret = g_vscfg[vs_id].lasttotlen - g_vscfg[vs_id].lastoffset;
	}else{
		*buf = (char *)(g_vscfg[vs_id].cur_frm->addr + g_vscfg[vs_id].lastoffset);
		ret = g_vscfg[vs_id].cur_frm->size - g_vscfg[vs_id].lastoffset;
	}

	if(ret > maxlen){
		ret = maxlen;
	}
	g_vscfg[vs_id].lastoffset += ret;
	
	*ts = g_vscfg[vs_id].cur_frm->ts;

	if(g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].lasttotlen){
		tmpflags |= VDATA_FLAG_LASTPKT;

		int i;
		for(i=0; i<VS_FRMBUF_CNT; i++){
			if(g_vscfg[vs_id].last_addrs[i] == NULL){
				g_vscfg[vs_id].last_addrs[i] = (char *)(*buf);
				g_vscfg[vs_id].last_frms[i] = g_vscfg[vs_id].cur_frm;
				g_vscfg[vs_id].last_cnt ++;
				break;
			}
		}
		if(i == VS_FRMBUF_CNT){
			syslog(LOG_ERR, "[ERR] vs_get_data, last_cnt:%d, not find NULL last_addrs\n", g_vscfg[vs_id].last_cnt);
		}

		g_vscfg[vs_id].cur_frm = NULL;
	}

	*flags = tmpflags;
	return ret;
}

int libvs_remove_data(int vs_id, char *addr)
{
	if(addr == NULL){
		return -1;
	}

	if(g_vscfg[vs_id].last_cnt <= 0){
		return 0;
	}

	int i;
	for(i=0; i<VS_FRMBUF_CNT; i++){
		if(g_vscfg[vs_id].last_addrs[i] == addr){
			libvs_remove_frame(vs_id, g_vscfg[vs_id].last_frms[i]);
			g_vscfg[vs_id].last_addrs[i] = NULL;
			g_vscfg[vs_id].last_cnt --;
			return 0;
		}
	}

	if(i == VS_FRMBUF_CNT){
		return 0;
	}

	return -1;
}

int video_get_data(int vs_id, VIDEO_DATA *p_data, u32 maxlen, u32 timeout)
{
    int ret;
    if((maxlen == 0) || (p_data == NULL))   return 0;

    memset(p_data, 0, sizeof(VIDEO_DATA));

    if(g_vscfg[vs_id].cur_frm)
    {
        if(g_vscfg[vs_id].lastoffset && (g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].lasttotlen))
        {
		    g_vscfg[vs_id].lastoffset = 0;
            g_vscfg[vs_id].cur_frm = NULL;
        }
    }

    //new frame
    if(g_vscfg[vs_id].cur_frm == NULL)
    {
        g_vscfg[vs_id].cur_frm = libvs_get_frame(vs_id, timeout);
        if(g_vscfg[vs_id].cur_frm == NULL)
            return 0;

        g_vscfg[vs_id].lastoffset = 0;
        p_data->frameType |= VDATA_FLAG_FIRSTPKT;
        g_vscfg[vs_id].lasttotlen = g_vscfg[vs_id].cur_frm->size + (g_vscfg[vs_id].cur_frm->addr1 ? g_vscfg[vs_id].cur_frm->size1 : 0);
    }

    if(g_vscfg[vs_id].cur_frm->type == FRAME_TYPE_VIDEO_IDR)
        p_data->frameType |= VDATA_FLAG_IDR;
    else if(g_vscfg[vs_id].cur_frm->type == FRAME_TYPE_VIDEO_I)
        p_data->frameType |= VDATA_FLAG_I;

    p_data->frameLen = g_vscfg[vs_id].lasttotlen;
    if(g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].cur_frm->size)
    {
        p_data->pAddr = (char *)(g_vscfg[vs_id].cur_frm->addr1 + g_vscfg[vs_id].lastoffset - g_vscfg[vs_id].cur_frm->size);
        ret = g_vscfg[vs_id].lasttotlen - g_vscfg[vs_id].lastoffset;
    }
    else
    {
        p_data->pAddr = (char *)(g_vscfg[vs_id].cur_frm->addr + g_vscfg[vs_id].lastoffset);
        ret = g_vscfg[vs_id].cur_frm->size - g_vscfg[vs_id].lastoffset;
    }

    if(ret > maxlen)        ret = maxlen;
    g_vscfg[vs_id].lastoffset += ret;

    if(g_vscfg[vs_id].lastoffset >= g_vscfg[vs_id].lasttotlen)
    {
        p_data->frameType |= VDATA_FLAG_LASTPKT;
        p_data->pRel = (u8*)g_vscfg[vs_id].cur_frm;
    }

    p_data->thisLen = ret;
    p_data->timeStamp = g_vscfg[vs_id].cur_frm->ts;

    return ret;
}

int video_remove_data(int vs_id, u8 *p_rel)
{
    if(p_rel)
    {
        libvs_remove_frame(vs_id, (FRAME*)p_rel);
    }

    return 0;
}

int libvs_set_vs_framerate(int vs_id, u32 framerate)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_VS_FRMRATE_SET) | (vs_id << 12), framerate);
	return ret;
}

int libvs_set_gop(int vs_id, u32 gop_size)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_GOP_SET) | (vs_id << 12), gop_size);
	return ret;
}

int libvs_set_gop_duration(int vs_id, u32 gop_duration)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_GOP_DURATION_SET) | (vs_id << 12), gop_duration);
	return ret;
}

int libvs_set_init_gop(int vs_id, u32 init_gop)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_INIT_GOP_SET) | (vs_id << 12), init_gop);
	return ret;
}

int libvs_set_min_qp(int vs_id, u32 min_qp)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_MINQP_SET) | (vs_id << 12), min_qp);
	return ret;
}

int libvs_get_min_qp(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_MINQP_GET) | (vs_id << 12), 0);
	return ret;
}

int libvs_set_max_qp(int vs_id, u32 max_qp)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS |  VSIOC_MAXQP_SET) | (vs_id << 12), max_qp);
	return ret;
}

int libvs_get_max_qp(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_MAXQP_GET) | (vs_id << 12), 0);
	return ret;
}

int libvs_set_qpdelta(int vs_id, u32 qp_delta)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_QPDELTA_SET) | (vs_id << 12), qp_delta);
	return ret;
}

int libvs_set_vprofile(int vs_id, u32 video_profile)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VIDEOPROFILE_SET) | (vs_id << 12), video_profile);
	return ret;
}

int libvs_get_timestamp(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_TIMESTAMP_GET) | (vs_id << 12), 0);
	return ret;
}

int libvs_get_sps_pps(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SPS_PPS_GET) | (vs_id << 12), 0);
	return ret;
}

int libvs_osd_enable(int vs_id, int win_no)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_OSD_ENABLE_SET) | (vs_id << 12), win_no);
	return ret;
}

int libvs_osd_disable(int vs_id, int win_no)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_OSD_DISABLE_SET) | (vs_id << 12), win_no);
	return ret;
}

int libvs_set_mirror_flip(int vs_id, u32 mirror_flip)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VIDEOMIRRORFLIP_SET) | (vs_id << 12), mirror_flip);
	return ret;
}

int libvs_set_contrast(int vs_id, u32 contrast)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_CONTRAST_SET) | (vs_id << 12), contrast);
	return ret;
}

int libvs_set_saturation(int vs_id, u32 saturation)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SATURATION_SET) | (vs_id << 12), saturation);
	return ret;
}

int libvs_get_dropped_frm_num(int vs_id){
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_DROP_FRM_GET) | (vs_id << 12), 0);
	return ret;
}

int libvs_enable_meta(int vs_id, int enable)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_ENABLE_META) | (vs_id <<12),enable);
	return ret;
}

int libvs_set_oldfrmdrop_th(int vs_id, int drop_th)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_DROPFRM_TH_SET) | (vs_id <<12), drop_th);
	return ret;
}

int libvs_set_sde(int vs_id, int sde_effect_No)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_SDE) | (vs_id <<12), sde_effect_No);
	return ret;
}

int libvs_set_daynight(int vs_id, int daynight)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_DAYNIGHT) | (vs_id <<12), daynight);
	return ret;
}

int libvs_set_exp(int vs_id, u32 exp)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_AEC_MANUAL_EXP) | (vs_id <<12), exp);
	return ret;
}

int libvs_set_gain(int vs_id, u32 gain)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_AEC_MANUAL_GAIN) | (vs_id <<12), gain);
	return ret;
}

int libvs_get_buf_frm_num(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VS_FRM_IN_BUF_GET) | (vs_id <<12), 0);
	return ret;
}

int libvs_get_encoded_frm_num(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VS_FRM_TOTAL_GET) | (vs_id <<12), 0);
}		

int libvs_get_read_frm_num(int vs_id){
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VS_FRM_READ_GET) | (vs_id <<12), 0);
}

int libvs_set_max_node_number(int vs_id, int num){
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_VS_MAX_NODE_SET) | (vs_id <<12), num);
}

int libvs_set_img_qs(int vs_id, int qs)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_IMG_QS_SET) | (vs_id <<12), qs);
}

int libvs_set_wdr(int vs_id, u32 wdr)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_WDR) | (vs_id << 12), wdr);
	return ret;
}

int libvs_set_mctf(int vs_id, u32 mctf)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_MCTF) | (vs_id << 12), mctf);
	return ret;
}

int libvs_set_sharpness(int vs_id, u32 sharpness)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_SET_SHARPNESS) | (vs_id << 12), sharpness);
	return ret;
}
