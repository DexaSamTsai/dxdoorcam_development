#include "includes.h"

int libvs_ioctl(int vs_id, int request, void * arg)
{
	//if arg is pointer, need dc_flush_range()
	switch(request){
	default:
		break;
	}

	return mpu_cmd(MPUCMD_TYPE_VS | (vs_id << 12) | request, (u32)arg);
	//if return is pointer, need dc_invalidate_range()
}

int libvs_get_framerate(int vs_id)
{
	int ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_FRMRATE_GET) | (vs_id << 12), 0);
	return ret;
}

u32 libvs_get_bufaddr(int vs_id)
{
	u32 ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_GET_BUFADDR) | (vs_id << 12), 0);
	return ret;
}

u32 libvs_get_bufmaxlen(int vs_id){
	u32 ret = mpu_cmd((MPUCMD_TYPE_VS | VSIOC_GET_BUFMAXLEN) | (vs_id << 12), 0);
	return ret;
}

