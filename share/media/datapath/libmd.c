#include "includes.h"

t_ertos_eventhandler mdbuf_notempty_evt = NULL;

u32 libmd_wndcnt_set(int md_id, int wind_cnt)
{
	return mpu_cmd(MPUCMD_DATAPATH_MDWNDCNT_SET | (md_id << 12), wind_cnt);
}

t_md_algo_buf * libmd_get_buf(int md_id, u32 timeout)
{
	u32 ret = mpu_cmd(MPUCMD_DATAPATH_MD_GET_BUF | (md_id << 12), 0);
	if(ret != NULL){
		goto FRAME_DONE;
	}

	if(mdbuf_notempty_evt){
		ertos_event_wait(mdbuf_notempty_evt, timeout);
	}

	ret = mpu_cmd(MPUCMD_DATAPATH_MD_GET_BUF | (md_id << 12), 0);
FRAME_DONE:
	return (t_md_algo_buf *)ret;
}

