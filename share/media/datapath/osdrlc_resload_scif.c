#include "includes.h"

static int _osdrlc_resitem_load_from_scif(t_osdrlc_cfg * cfg, u32 item_no)
{
	t_osdrlc_res * r = cfg->osdrlc_res;

	FILE * fp;

	char *fname = (char*)(cfg->arg);
	
	fp = fopen(fname, "r");
	if(fp == NULL){
		syslog(LOG_ERR,"err to open the file:%s\n", fname);
		return -1;
	}
	u32 pos = r->offset_base + r->item[item_no-1].offset;
	fseek(fp, pos, SEEK_SET);
	
	unsigned int buf[32/4];
	fread(buf, 1, 4, fp);
	
	if(buf[0] != 0x4F4C5938){ //"OLY8"
//	if(buf[0] != 0x38594C4F){ //"OLY8"
		return -2; ///< wrong data magic
	}
	
	fread(buf, 1, 32, fp);
	int n;
	for(n=0; n<8; n++){
		r->item[item_no-1].color[n] = buf[n];
	}

	fread(buf, 1, 8, fp);
	r->item[item_no-1].width = (buf[0]>>16);
	r->item[item_no-1].height = (buf[0]&0xffff);
	r->item[item_no-1].size = buf[1];

	memset((void *)cfg->mem_base, 0, ((r->item[item_no-1].size+7)&(~7)));
	fread((void *)cfg->mem_base, 1, r->item[item_no-1].size, fp);
	fclose(fp);
	//WriteReg32(cfg->mem_base+0x4, 0x2be30000); //osdrlc big endian
	//WriteReg32(cfg->mem_base, 0x0);
//	WriteReg32(cfg->mem_base+0x0, 0x0000e32b); //osdrlc little endian
//	WriteReg32(cfg->mem_base+0x4, 0x0);
	return 0;
}

static int _osdrlc_res_load_from_scif(t_osdrlc_cfg * cfg, int group)
{
	t_osdrlc_res * r = (t_osdrlc_res *)(cfg->osdrlc_res);

	char * fname = (char *)(cfg->arg);
	FILE * fp;
	fp = fopen(fname, "r");
	if(fp == NULL){
		syslog(LOG_ERR,"err to open the file:%s\n", fname);
		return -1;
	}
	
	unsigned int buf[16/4];
	fread(buf, 1, 8, fp);
	
	if(buf[0] != 0x5f726573){
		return -3; ///< RES magic not found
	}
	if(group > buf[1]){
		return -4; ///< group > RES group
	}

	u32 pos = ftell(fp);
	fseek(fp, pos+(group-1)*4, SEEK_SET);
	fread(buf, 1, 4, fp);
	r->offset_base = buf[0];
	
	fseek(fp, r->offset_base, SEEK_SET);
	fread(buf, 1, 8, fp);

	if(buf[0] != 0x72657369){
		return -5;  ///< ITEM magic not found
	}
	
	if(buf[1] != r->item_count){
		if(r->item_count > buf[1]){
			r->item_count = buf[1];
		}else{
			return -6;
		}
	}

	int i;
	for(i=0; i<r->item_count; i++){
		fread(buf, 1, 4, fp);
		r->item[i].offset = buf[0];
	}
	fclose(fp);
	return 0;
}


t_osdrlc_resload_hw g_resload_scif = 
{
	.cb_osdrlc_resitem_load = _osdrlc_resitem_load_from_scif,
	.cb_osdrlc_res_load = _osdrlc_res_load_from_scif,
};
