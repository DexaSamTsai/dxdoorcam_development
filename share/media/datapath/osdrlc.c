#include "includes.h"

int libosdrlc_res_load(t_osdrlc_cfg *cfg, u32 group_no)
{
	if(cfg == NULL){
		return -1;
	}
	if(cfg->osdrlc_res && cfg->osdrlc_res->hw && cfg->osdrlc_res->hw->cb_osdrlc_res_load){
		return cfg->osdrlc_res->hw->cb_osdrlc_res_load(cfg, group_no);
	}else{
		return -2;
	}
}

int libosdrlc_get(t_osdrlc_cfg * cfg, u8 vs_id, u8 win_no)
{
	if(cfg == NULL){
		return -1;
	}
	u32 ret = mpu_cmd((MPUCMD_TYPE_VS|VSIOC_OSDRLC_BUFGET|(vs_id<<12)), win_no);
	if(ret){
		cfg->mem_base = (ret|BIT31);
		return 0;
	}
	return -1;
}


int libosdrlc_fill(t_osdrlc_cfg * cfg, u32 item_no, u8 win_no, u32 offset)
{
	int ret = 0;
	if(cfg && cfg->cb_osdrlc_fill){
		ret = cfg->cb_osdrlc_fill(cfg, item_no, win_no, offset);
		if(ret){
			return ret;
		}
	}
	
	if(cfg == NULL||cfg->mem_base == 0){
		return -1;
	}
	
	if(cfg->osdrlc_res && cfg->osdrlc_res->hw && cfg->osdrlc_res->hw->cb_osdrlc_resitem_load){
		ret = cfg->osdrlc_res->hw->cb_osdrlc_resitem_load(cfg, item_no);
		if(ret < 0){
			return -1;
		}
		cfg->osdrlc_res->item[item_no-1].win_no = win_no;
		cfg->osdrlc_res->item[item_no-1].hstart = ((offset>>16)&0xffff);
		cfg->osdrlc_res->item[item_no-1].vstart = (offset&0xffff);
	}
	return 0;
}

int libosdrlc_update(t_osdrlc_cfg * cfg, u8 vs_id, u32 item_no){
	
	if(cfg == NULL){
		return -1;
	
	}
	dc_flush_range((u32)(&cfg->osdrlc_res->item[item_no-1]), (u32)(&cfg->osdrlc_res->item[item_no-1])+sizeof(t_osdrlc_res_item));
	return mpu_cmd((MPUCMD_TYPE_VS|VSIOC_OSDRLC_UPDATE|(vs_id<<12)), (u32)(&cfg->osdrlc_res->item[item_no-1]));
}

