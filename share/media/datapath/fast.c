#include "includes.h"

int libdatapath_fastinit(int dpcfg_id)
{
	int ret = mpu_cmd(MPUCMD_DATAPATH_FASTINIT, dpcfg_id);

	return ret;
}

int libdatapath_faststart(void)
{
	int ret = mpu_cmd(MPUCMD_DATAPATH_FASTSTART, 0);

	return ret;
}

int libdatapath_fast_fps2fps(int fps)
{
	int ret = mpu_cmd(MPUCMD_DATAPATH_FASTFPS2FPS, fps);

	return ret;
}
int libdatapath_fast_fps2start(void)
{
	int ret = mpu_cmd(MPUCMD_DATAPATH_FASTFPS2START, 0);
	return ret;
}

int libdatapath_fast_simu(int dpcfg_id)
{
	int ret = mpu_cmd(MPUCMD_DATAPATH_FAST_SIMU, 0);

	return ret;
}

