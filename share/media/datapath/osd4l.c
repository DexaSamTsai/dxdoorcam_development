#include "includes.h"

int libosd4l_gui_set_pixel(void * hw_arg, int x, int y, u32 color){

	t_osd4l_cfg * cur_osd4l_cfg = (t_osd4l_cfg *)(hw_arg);
	if(cur_osd4l_cfg == NULL){
		return -1;
	}

	if((x < 0) || (y < 0) || (x > cur_osd4l_cfg->win.win_width) || (y > cur_osd4l_cfg->win.win_height)){
		return 1;
	}

    u32 byte = (y * cur_osd4l_cfg->win.win_width + x) >> 4; 
	byte = byte << 2;
    u32 bit = ((y * cur_osd4l_cfg->win.win_width + x) & 0xf) << 1; 

    WriteReg32(cur_osd4l_cfg->win.win_memaddr + byte, 
            (ReadReg32(cur_osd4l_cfg->win.win_memaddr + byte) & (~(0x3 << bit))) | (color << bit));
	return 1;
}

int libosd4l_get(t_osd4l_cfg * cfg, u8 vs_id, u8 win_no)
{
	if(cfg == NULL){
		return -1;
	}
	u32 ret = mpu_cmd((MPUCMD_TYPE_VS|VSIOC_OSD4L_PARAGET|(vs_id<<12)), win_no);
	if(ret){
		t_osd4l_win * win_cfg = (t_osd4l_win *)(ret|BIT31);
		cfg->win.win_no = win_cfg->win_no;
		cfg->win.mem_id = win_cfg->mem_id;
		cfg->win.win_width = win_cfg->win_width;
		cfg->win.win_height = win_cfg->win_height;
		cfg->win.win_hstart = win_cfg->win_hstart;
		cfg->win.win_vstart = win_cfg->win_vstart;
		cfg->win.win_max_offset = win_cfg->win_max_offset;
		cfg->win.win_memaddr = ((win_cfg->mem_id==0)?((win_cfg->win_memaddr+MEMBASE_SRAM)|BIT31):((win_cfg->win_memaddr+(win_cfg->win_max_offset)+MEMBASE_SRAM)|BIT31));
		//cfg->win.win_memaddr = ((win_cfg->win_memaddr+MEMBASE_SRAM)|BIT31);
		int n;
		for(n=0; n<4; n++){
			cfg->win.color[n] = win_cfg->color[n];
		}
	}
	return ret;
}


int libosd4l_update(t_osd4l_cfg * cfg, u8 vs_id)
{
	if(cfg == NULL){
		return -1;
	}
	
	dc_flush_range((u32)(&cfg->win), (u32)(&cfg->win) + sizeof(t_osd4l_win));
	return mpu_cmd((MPUCMD_TYPE_VS|VSIOC_OSD4L_UPDATE|(vs_id<<12)), (int)(&cfg->win));
}
