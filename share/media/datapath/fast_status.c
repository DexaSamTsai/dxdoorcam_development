#include "includes.h"

typedef struct s_mpu_cmd{
	volatile u32 used;     ///<command status,1:command in used 0:command not in used
	volatile u32 arg;               ///<command argument
	volatile u32 id;         ///<command id
	volatile u32 ret;        ///<command return
	volatile u32 ret_ready;	///<0: not ready, 1:ready from MPU, 2:cond send to app
}t_mpu_cmd;

typedef struct s_mpu_sig{
	volatile u32 status;
}t_mpu_sig;

#define g_mpucmd (*(t_mpu_cmd *)(0x900cffe0))
#define g_mpusig (*(t_mpu_sig *)(0x900cfff8))

int libdatapath_fast_status(void)
{
	int i;
	for( i = MPUEVT_FASTBOOT_STATUS + FAST_STAGE_LAST; i >= MPUEVT_FASTBOOT_STATUS; i--){
		if(g_mpusig.status & (1 << i) ){
			return i - MPUEVT_FASTBOOT_STATUS;
		}
	}
	return 0;
}
