#ifndef _LIBAVIMOV_ENC_INTERNAL_H_
#define _LIBAVIMOV_ENC_INTERNAL_H_

//#define MP4_PLAYBACK_ONLINE //support playback via URL by online
#define MODE_MP4 0x01
#define MODE_MOV 0x02
#define MODE_IPOD 0x03
#define MODE_3GP 0x04
#define MAX_STREAMS 2

typedef struct av_codec_context{
	u8 *extradata;
	int extradata_size;

	MUX_STREAM_TYPE type;

	union{
		struct{
			///< audio only
			int sample_rate; ///< samples per second
			int channels; ///< number of audio channel
			int bps;	  ///< bit per sample
		}aparam;
		struct{
			///< video only
			int width;
			int height;
			int frame_rate;
		}vparam;
	};
} av_codec_context ;

typedef struct movi_entry{
	u32 size;
	u32 pos;
	u32 samplesInChunk;
	u16 key_frame;
	u16 trackID;
	u32 entries;
	u32 cts;
	u32 dts;
}movi_entry;

typedef struct s_mov_tag{
	u32 pos;
	u32 size;
}t_mov_tag;

typedef struct s_moov_hdr{
	t_mov_tag trak_tag;
	t_mov_tag mdia_tag;
	t_mov_tag minf_tag;
	t_mov_tag stbl_tag;
	t_mov_tag stsc_tag;
	t_mov_tag stts_tag;
	t_mov_tag stsc_idx;
	int stts_entries;
#ifdef CONFIG_SPHERICAL_METADATA
	t_mov_tag uuid_tag;
#endif
}t_moov_hdr;

typedef struct movi_track
{
	int mode;
	int entry;

	//int total_entry;
	u32 timescale;
	u32 time;
	u32 trackDuration;
	u32 sampleCount;
	u32 sampleSize;
	u32 hasKeyframes;
	u32 hasBframes;
	u32 language;
	u32 trackID;
	int tag; ///< stsd fourcc
	av_codec_context enc;

	movi_entry cluster; //only save the first cluster 
	t_moov_hdr moov_hdr;
	u32 stco_oft;
	int audio_vbr;
}movi_track;

typedef struct movi_context{
	int mode;
	u32 time;
	int nb_streams;
	u32 mdat_pos;
	u32 mdat_offset;
	u32 mdat_size;
	u32 moov_pos;
	u32 moov_size;
	int timescale;
	u8 video_index;
	u8 audio_index;
	u8 mimg_index;
	u32 global_time_scale;
	movi_track tracks[MAX_STREAMS];
}movi_context;

typedef struct s_movi_packet{
	int stream_index;
	int size;

	///< must be converted to true pts/dts before it is stored in AVPacket
	u32 pts; ///< pts of the current frame
	u32 dts; ///< dts of the current frame

	int duration;
	int pkt_flag_key;

#define MP4_VIDEO_DATA_FLAG 0x0
#define MP4_AUDIO_DATA_FLAG 0x1
#define MP4_MIMG_DATA_FLAG  0x2

	u8 data_flag;
}movi_packet;

#endif
