#include "includes.h"

extern const t_libmux_cfg g_libmux_raw_cfg;
#ifdef CONFIG_MUX_MP4_EN
extern const t_libmux_cfg g_libmux_mp4_cfg;
#endif

void muxwr_buf_error_exit(t_libmuxwr_buf * buf)
{
	syslog(LOG_DEBUG, "muxwr_buf_error_exit buf:0x%x,buf_fromheap:0x%x,fp:0x%x\n", buf->buf,buf->buf_fromheap,buf->fp);

	if(buf->buf && buf->buf_fromheap){
		free(buf->buf);
		buf->buf = NULL;
	}
	if(buf->fp){
		fclose(buf->fp);
		buf->fp = NULL;
		if(buf->fname[0]){
			unlink(buf->fname);
		}
		buf->fname[0] = '\0';
	}
}

void muxwr_error_exit(t_libmuxwr_cfg * cfg)
{
	if(cfg->fp){
		fclose(cfg->fp);
		cfg->fp = NULL;
	}
}

static int muxwr_buf_create_fp(t_libmuxwr_buf *buf, char *postfix)
{
	int i;

	syslog(LOG_DEBUG, "muxwr_buf_create_fp fname:%s\n", buf->fname);
	for (i = 0; i < 100; i++) {
		sprintf(buf->fname, "%d.%s", i, postfix ? postfix : "idx");
		FILE * fp = fopen(buf->fname, "r");
		if (fp != NULL) {
			fclose(fp);
			continue;
		}
		buf->fp = fopen(buf->fname, "w");
		if (buf->fp == NULL) {
			syslog(LOG_ERR, "muxwr open buf file %s failed\n", buf->fname);
			return -1;
		} else {
			return 0;
		}
	}

	syslog(LOG_DEBUG, "muxwr open buf file failed, 0~99.idx all used\n");

	return -1;
}

int muxwr_buf_init(t_libmuxwr_buf *buf)
{
	syslog(LOG_DEBUG, "muxwr_buf_init size:0x%x, use_file:%d\n", buf->size, buf->use_file);
	if (buf->size != 0) {
		if (buf->buf == NULL){
			buf->buf = malloc(buf->size);
			if (buf->buf == NULL) {
				syslog(LOG_ERR, "muxwr malloc buf %d failed\n", buf->size);

				return -10;
			}
			syslog(LOG_DEBUG, "muxwr_buf_init buf:0x%x\n", buf->buf);
			buf->buf_fromheap = 1;
		}

		buf->used = 0;
	} else if (buf->use_file) {
		//create index file here, only if size == 0
		if (muxwr_buf_create_fp(buf, NULL) < 0) {
			return -12;
		}
	}

	return 0;
}

int muxwr_buf_addstr(t_libmuxwr_buf * buf, u8 * str, u32 len)
{
	///TODO: support file.
//	syslog(LOG_DEBUG, "muxwr_buf_addstr len:%d, total_len:%d, size:%d, used:%d\n", len, buf->total_len,buf->size, buf->used);

	buf->total_len += len;

	if (buf->buf){
		//buffer enabled
		if (buf->size - buf->used < len){
			//buffer not enough, see if can write to file
			if (buf->use_file == 0){
				syslog(LOG_ERR, "muxwr_buf_str, buf full\n");
				return -1;
			} else {
				//flush buf to file
				//create index file here, only if idx_buf_size == 0
				if (buf->fp == NULL){
					if(muxwr_buf_create_fp(buf, NULL) < 0){
						return -12;
					}
				}
				syslog(LOG_ERR, "fwrite buf full\n");
				if(fwrite(buf->buf, buf->used, 1, buf->fp) > 0){
					buf->used = 0;
				}else{
					syslog(LOG_ERR, "muxwr_buf_str, flush buf to fp fail\n");
					return -13;
				}
			}
		}

		if (buf->size - buf->used < len) {
			if (buf->used == 0 && buf->fp){
				syslog(LOG_DEBUG, "fwrite buf write to fp\n");

				//if idx buf empty, and fp exist, can write to fp directly
				if(fwrite(str, len, 1, buf->fp) <= 0){
					syslog(LOG_ERR, "muxwr_buf_str, write to fp fail\n");
					return -2;
				}else{
					syslog(LOG_WARNING, "muxwr_buf_str, buf not enough, write to fp\n");
					return 0;
				}
			}else{
				syslog(LOG_ERR, "muxwr_buf_str, buf full\n");
				return -1;
			}
		}

		//add to buffer
		memcpy(buf->buf + buf->used, str, len);
		buf->used += len;
	}else if(buf->fp){
		//fp enabled
		if(fwrite(str, len, 1, buf->fp) <= 0){
			syslog(LOG_ERR, "muxwr_buf_str, write to fp fail\n");
			return -2;
		}
	}else{
		syslog(LOG_ERR, "muxwr_buf_str, no buf and fp\n");
		return -3;
	}

	return 0;
}

int muxwr_buf_flush(t_libmuxwr_buf * buf)
{
	if(buf->fp && buf->buf && buf->used){
		if(fwrite(buf->buf, buf->used, 1, buf->fp) > 0){
			buf->used = 0;
		}else{
			syslog(LOG_ERR, "muxwr_buf_flush, flush buf to fp fail\n");
			return -1;
		}
	}
	return 0;
}

unsigned char *muxwr_buf_read(t_libmuxwr_buf * buf, int offset, int size)
{
	//flush idx
	if (muxwr_buf_flush(buf) != 0){
		return NULL;
	}

	if (buf->fp) {
		u8 *hdrbuf = buf->buf;
		if (buf->size < size) {
			hdrbuf = malloc(size);
			if (hdrbuf == NULL){
				syslog(LOG_ERR, "malloc hdrbuf failed\n");
				return NULL;
			}
		}

		//use idxfp, then load the header to the buffer
		fseek(buf->fp, offset, SEEK_SET);
		if (fread(hdrbuf, 1, size, buf->fp) <= 0) {
			syslog(LOG_ERR, "read hdrbuf from file fail\n");
			if (hdrbuf != buf->buf){
				free(hdrbuf);
			}

			return NULL;
		}
		return hdrbuf;
	} else {
		if (offset < buf->used) {
			syslog(LOG_ERR, "offset >= buf->used\n");
			return NULL;
		}

		return buf->buf + offset;
	}
}

int muxwr_buf_write(t_libmuxwr_buf * buf, u8 * str, int offset, int size)
{
	if (buf->fp) {
		fseek(buf->fp, offset, SEEK_SET);
		if (fwrite(str, 1, size, buf->fp) <= 0) {
			syslog(LOG_ERR, "write hdrbuf to file fail\n");
			if (str != buf->buf){
				free(str);
			}

			return -4;
		}
		fseek(buf->fp, 0, SEEK_END); //set to end

		if (str != buf->buf){
			free(str);
		}
	}

	return 0;
}

int muxwr_buf_free(t_libmuxwr_buf * buf, u8 * str)
{
	if (str != buf->buf){
		free(str);
	}

	return 0;
}

int libmuxwr_open(t_libmuxwr_cfg * cfg, char * filename)
{
	if (cfg == NULL){
		return -1;
	}

	syslog(LOG_DEBUG, "libmuxwr_open FMT:%d\n", cfg->fmt);

	//check file format
	if (cfg->fmt == MUX_FMT_AUTO) {
		char * postfix = strrchr(filename, '.');
		if (postfix == NULL){
			syslog(LOG_ERR, "muxwr: Cannot detect mux fmt\n");
			return -2;
		}

		if (!strcasecmp(postfix + 1, "h264")
			|| !strcasecmp(postfix + 1, "264")
			|| !strcasecmp(postfix + 1, "pcm")
			|| !strcasecmp(postfix + 1, "raw")
			|| !strcasecmp(postfix + 1, "aac")) {
			cfg->fmt = MUX_FMT_RAW;
		} else if (!strcasecmp(postfix + 1, "mp4")
			|| !strcasecmp(postfix + 1, "mov")
			|| !strcasecmp(postfix + 1, "m4a")) {
			cfg->fmt = MUX_FMT_MP4;
		} else {
			syslog(LOG_ERR, "muxwr: Cannot detect mux fmt\n");
			return -2;
		}
	}

	//set mux_cfg according to file format
	if (cfg->fmt == MUX_FMT_RAW){
		syslog(LOG_DEBUG, "MUX_FMT_RAW\n");
		cfg->mux_cfg = &g_libmux_raw_cfg;
#ifdef CONFIG_MUX_MP4_EN
	} else if (cfg->fmt == MUX_FMT_MP4){
		syslog(LOG_DEBUG, "MUX_FMT_MP4\n");
		cfg->mux_cfg = &g_libmux_mp4_cfg;

		if ((cfg->atype != MUX_STREAM_NONE) || (cfg->vtype != MUX_STREAM_NONE)) {
			if ((cfg->atype != MUX_STREAM_NONE) && (!cfg->aparam.sample_rate || !cfg->aparam.channels || !cfg->aparam.bps)) {
				syslog(LOG_ERR, "muxwr check audio param failed!\n");
				return -2;
			}

			if ((cfg->vtype != MUX_STREAM_NONE) && (!cfg->vparam.frame_rate || !cfg->vparam.height || !cfg->vparam.width)) {
				syslog(LOG_ERR, "muxwr check video param failed!\n");
				return -2;
			}
		} else {
			syslog(LOG_ERR, "muxwr check param failed!\n");
			return -2;
		}
#endif
	} else {
		syslog(LOG_ERR, "muxwr format %d not supported\n", cfg->fmt);
		return -3;
	}

	//open the stream file
	cfg->fp = fopen(filename, "w+");
	if (cfg->fp == NULL) {
		syslog(LOG_ERR, "muxwr open file %s failed\n", filename);
		muxwr_error_exit(cfg);
		return -4;
	}

	//write stream header
	if (cfg->mux_cfg->header_write) {
		if (cfg->mux_cfg->header_write(cfg) < 0) {
			muxwr_error_exit(cfg);
			return -5;
		}
	}

	return 0;
}

int libmuxwr_write_frame(t_libmuxwr_cfg * cfg, FRAME * frame){
	if (frame->type >= FRAME_TYPE_AUDIO_ENCAUDIO){
		cfg->total_aframecnt += 1;
	} else {
		cfg->total_vframecnt += 1;
//		syslog(LOG_WARNING, "libmuxwr_write_frame framecnt:%d\n", cfg->total_vframecnt);
	}

	if (cfg->mux_cfg->frame_write){
		return cfg->mux_cfg->frame_write(cfg, frame);
	} else {
		int ret = 0;
		//write data
		if (fwrite((u8 *)frame->addr, frame->size, 1, cfg->fp) <= 0) {
			syslog(LOG_ERR, "muxwr_write_frame, write frm %d fail\n", frame->size);
			return -2;
		}
		ret += frame->size;
		if (frame->addr1 && frame->size1) {
			if (fwrite((u8 *)frame->addr1, frame->size1, 1, cfg->fp) <= 0) {
				syslog(LOG_ERR, "muxwr_write_frame, write frm1 %d fail\n", frame->size1);
				return -2;
			}

			ret += frame->size1;
		}

		return ret;
	}
}

int libmuxwr_close(t_libmuxwr_cfg *cfg)
{
	if (cfg->fp == NULL){
		syslog(LOG_ERR, "muxwr closed already, cannot close\n");
		return -1;
	}

	//tail write
	if (cfg->mux_cfg->tail_write) {
		if(cfg->mux_cfg->tail_write(cfg)) {
			muxwr_error_exit(cfg);
			return -5;
		}
	}

	syslog(LOG_DEBUG, "libmuxwr_close\n");

	//close stream file, done
	fclose(cfg->fp);
	cfg->fp = NULL;
	syslog(LOG_DEBUG, "libmuxwr_close OK\n");

	return 0;
}
