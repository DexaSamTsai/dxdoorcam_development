#include "includes.h"
#include "movenc_internal.h"

typedef struct s_movenc_private
{
	movi_context mov;
	movi_packet avp;
	u8 spspps_buf[64];
}t_movenc_private;

static void movenc_error_exit(t_libmuxwr_cfg * cfg)
{
	muxwr_buf_error_exit(&cfg->idx);
	muxwr_buf_error_exit(&cfg->tmpbuf);
	muxwr_buf_error_exit(&cfg->tail);

	if(cfg->mux_private){
		free(cfg->mux_private);
		cfg->mux_private = NULL;
	}
}

static void movwr_char(t_libmuxwr_cfg * cfg, u8 val){
	u8 str[1];
	str[0] = val;
	//syslog(LOG_WARNING, "movwr_char\n");
	if(muxwr_buf_addstr(cfg->cur, &str[0], 1) < 0){
		syslog(LOG_ERR, "While(1)\n");
		while(1);
	}
}

static void movwr_be32(t_libmuxwr_cfg * cfg, u32 val)
{
	movwr_char(cfg, val >> 24);
	movwr_char(cfg, val >> 16);
	movwr_char(cfg, val >> 8);
	movwr_char(cfg, val & 0xff);
}

static void movwr_be16(t_libmuxwr_cfg * cfg, u32 val){
	movwr_char(cfg, val >> 8);
	movwr_char(cfg, val & 0xff);
}

static void movwr_be24(t_libmuxwr_cfg * cfg, u32 val){
	movwr_char(cfg, val >> 16);
	movwr_char(cfg, val >> 8);
	movwr_char(cfg, val & 0xff);
}

static void movwr_string(t_libmuxwr_cfg * cfg, char *str){
//	syslog(LOG_WARNING, "movwr_string\n");
	if(muxwr_buf_addstr(cfg->cur, (u8 *)str, strlen(str)) < 0){
		syslog(LOG_ERR, "movwr_string While(1)\n");
		while(1);
	}
}

static void movwr_buf(t_libmuxwr_cfg * cfg, u8 *buf, int size){
//	syslog(LOG_WARNING, "movwr_buf\n");
	if(muxwr_buf_addstr(cfg->cur, buf, size) < 0){
		syslog(LOG_ERR, "movwr_buf While(1)\n");
		while(1);
	}
}

static int movwr_set_ptr(t_libmuxwr_cfg * cfg, u32 set_oft){
	//TODO: support file.

	if(set_oft > cfg->cur->size){
		return -1;
	}

	cfg->cur->used = set_oft;
	return 0;
}

static u32 movwr_offset_get(t_libmuxwr_cfg * cfg)
{
	//TODO: support file.
	return cfg->cur->used;
}

static u32 movwr_offset_set(t_libmuxwr_cfg * cfg, u32 oft)
{
	return movwr_set_ptr(cfg, oft);
}

static int movwr_start_tag(t_libmuxwr_cfg * cfg, char *tag, u32 size){
	u32 tag_start = cfg->cur->used;
	movwr_be32(cfg, size);
	movwr_string(cfg, tag);
	return tag_start;
}

static void movwr_end_tag(t_libmuxwr_cfg * cfg, u32 tag_start)
{
	u32 tag_end = cfg->cur->used;

	movwr_set_ptr(cfg, tag_start);
	movwr_be32(cfg, tag_end - tag_start);
	movwr_set_ptr(cfg, tag_end);
}

static void movwr_mdat_tag(t_libmuxwr_cfg * cfg, movi_context *mov)
{
	mov->mdat_pos = movwr_offset_get(cfg);
	movwr_be32(cfg, 0);
	movwr_string(cfg, "mdat");
	mov->mdat_offset = movwr_offset_get(cfg);
	syslog(LOG_DEBUG, "movwr_mdat_tag mdat_pos:0x%x,mdat_offset:0x%x\n", mov->mdat_pos, mov->mdat_offset);
}

static void movwr_ftyp_tag(t_libmuxwr_cfg * cfg, movi_context *mov)
{
	u8 has_encv = 0, has_video = 0;
	int minor = 0x200;
	int i = 0;

	for (i = 0; i < mov->nb_streams; i++)
	{
		if (mov->tracks[i].enc.type < MUX_AUDIO_START)
		{
			has_video = 1;
		}
		if (mov->tracks[i].enc.type == MUX_VIDEO_ENCV)
		{
			has_encv = 1;
		}
		if (mov->tracks[i].enc.type >= MUX_AUDIO_START)
		{
			mov->tracks[i].sampleSize = (mov->tracks[i].enc.aparam.bps >> 3) * mov->tracks[i].enc.aparam.channels;
			if (mov->tracks[i].enc.type == MUX_AUDIO_AF4)
			{
				mov->tracks[i].sampleSize = 0;
			}
		}
	}

	syslog(LOG_DEBUG, "movwr_ftyp_tag nb_streams:%d,mode:%d,%d,%d,used:%d\n", mov->nb_streams, mov->mode, has_video, has_encv, cfg->cur->used);

	u32 ftyp_pos = movwr_start_tag(cfg, "ftyp", 0);
	if (mov->mode == MODE_3GP)
	{
		movwr_string(cfg, has_encv ? "3gp6" : "3gp4");
		minor = has_encv? 0x100: 0x200;
	} else if (mov->mode == MODE_MP4)
	{
		movwr_string(cfg, "isom");
	} else if (mov->mode == MODE_IPOD)
	{
		movwr_string(cfg, has_video? "M4V ":"M4A ");
	} else
	{
		movwr_string(cfg, "qt  ");
	}
	syslog(LOG_DEBUG, "cfg->cur->used:0x%x\n", cfg->cur->used);
	movwr_be32(cfg, minor);

	if (mov->mode == MODE_MOV)
	{
		movwr_string(cfg, "qt  ");
	} else {
		movwr_string(cfg, "isom");
		movwr_string(cfg, "iso2");
		if (has_encv)
		{
			movwr_string(cfg, "avc1");
		}
	}

	if (mov->mode == MODE_3GP)
	{
		movwr_string(cfg, has_encv? "3gp6" : "3gp4");
	} else if (mov->mode == MODE_MP4) {
		movwr_string(cfg, "mp41");
	}

	syslog(LOG_DEBUG, "cfg->cur->used:0x%x,ftyp_pos:0x%x\n", cfg->cur->used, ftyp_pos);
	movwr_end_tag(cfg, ftyp_pos);
}

static void movwr_header(t_libmuxwr_cfg * cfg, movi_context * mov)
{
	int i = 0;

	///< ftyp ccc
	movwr_ftyp_tag(cfg, mov);

	for (i = 0; i < mov->nb_streams; i++)
	{
		movi_track * track = & mov->tracks[i];

		syslog(LOG_DEBUG, "movwr_header nb_streams:%d,scale:0x%x,enc.type:%d\n", mov->nb_streams, mov->global_time_scale, track->enc.type);
		track->language = 0;
		track->mode = mov->mode;
		if (track->enc.type < MUX_AUDIO_START) {
			track->timescale = mov->global_time_scale;
		} else if (track->enc.type >= MUX_AUDIO_START) {
 			track->timescale = track->enc.aparam.sample_rate;
			track->audio_vbr = 0;
		}
	}

#ifdef MP4_PLAYBACK_ONLINE
	//movwr_offset_set(0);
#endif

	movwr_mdat_tag(cfg, mov);
	mov->time += 0x7c25b080;///< 1970 based -> 1904 based
}

int movenc_header_write(t_libmuxwr_cfg *cfg)
{
	syslog(LOG_DEBUG, "movenc_header_write\n");

	//malloc private
	cfg->mux_private = malloc(sizeof(t_movenc_private));
	if (cfg->mux_private == NULL) {
		syslog(LOG_ERR, "movenc private malloc fail\n");
		return -1;
	}
	memset(cfg->mux_private, 0, sizeof(t_movenc_private));

	t_movenc_private *priv = (t_movenc_private *)cfg->mux_private;

	//init buf
	if (cfg->idx.use_file){
		movenc_error_exit(cfg);
		syslog(LOG_ERR, "cfg->idx cannot use file\n");
		return -1;
	}

	if(cfg->idx.size == 0){
		movenc_error_exit(cfg);
		syslog(LOG_ERR, "ERR: mux->idx.size not set\n");
		return -1;
	}
	if (muxwr_buf_init(&cfg->idx) < 0){
		movenc_error_exit(cfg);
		return -1;
	}
	memset(cfg->idx.buf, 0, sizeof(cfg->idx.size));

//	cfg->idx.use_file = 1;
/*	if (muxwr_buf_init(&cfg->idx) < 0){
		movenc_error_exit(cfg);
		return -1;
	}
*/
	priv->mov.nb_streams = 0;
	priv->mov.mode = MODE_MP4;
	priv->mov.global_time_scale = 1000;

	syslog(LOG_DEBUG, "vtype:%d,atype:%d\n", cfg->vtype, cfg->atype, priv->mov.nb_streams);

	//init streams
	if (cfg->vtype != 0)
	{
		//TODO: get from video config
		priv->mov.tracks[priv->mov.nb_streams].enc.vparam.width = cfg->vparam.width;
		priv->mov.tracks[priv->mov.nb_streams].enc.vparam.height = cfg->vparam.height;
		priv->mov.tracks[priv->mov.nb_streams].enc.vparam.frame_rate = cfg->vparam.frame_rate; ////24 framerate for sensor ov9756
		priv->mov.tracks[priv->mov.nb_streams].enc.type = cfg->vtype;
		//mov.tracks[mov.nb_streams].frm_entry = (movi_entry *)sdram_addr;
		priv->mov.video_index = priv->mov.nb_streams;
		priv->mov.nb_streams++;
	}

	if (cfg->atype != 0) {
		u32 audio_bitrate = 0;

		if (cfg->atype == MUX_AUDIO_PCM_S16LE)
		{
			//a_enc.codec_id = CODEC_ID_PCM_S16LE;
			//a_enc.bps = 16;
			//audio_bitrate = a_enc.bps*g_libmux_movenc_cfg.acfg->asample_rate*g_libmux_movenc_cfg.acfg->achannels;
		}
		else if (cfg->atype == MUX_AUDIO_AF4)
		{
			//a_enc.codec_id = CODEC_ID_AF4;
			//a_enc.bps = 16;
			//audio_bitrate = 48000;
		}

		priv->mov.tracks[priv->mov.nb_streams].enc.type = cfg->atype;
		//TODO: get from audio config
		priv->mov.tracks[priv->mov.nb_streams].enc.aparam.bps = cfg->aparam.bps;
		priv->mov.tracks[priv->mov.nb_streams].enc.aparam.channels = cfg->aparam.channels;
		priv->mov.tracks[priv->mov.nb_streams].enc.aparam.sample_rate = cfg->aparam.sample_rate;

		//priv->mov.tracks[mov.nb_streams].frm_entry = (movi_entry *)sdram_addr;
		priv->mov.audio_index = priv->mov.nb_streams;
		priv->mov.nb_streams++;
	}

	priv->mov.time = 0xc563e495;

	//init tmpbuf
	cfg->tmpbuf.size = 4096;
	if (muxwr_buf_init(&cfg->tmpbuf) < 0){
		movenc_error_exit(cfg);

		return -1;
	}

	//write to tmpbuf now.
	cfg->cur = &cfg->tmpbuf;

#ifdef MP4_PLAYBACK_ONLINE
	//create dat file
#if 0
	//TODO
	if(file_fopen(&_g_mov_file, &sh_efs.myFs,(char *)g_mov_name, MODE_WRITE) != 0){
		file_fclose(&_g_mov_file);
	}
	g_libmuxwr_cfg.fp->wc = NULL;
	g_libmuxwr_cfg.fp = &_g_mov_file;
	efs_file_set_wc(&_g_mov_file, &fs_ncf_mywc);
#endif
#endif

	movwr_header(cfg, &priv->mov);

	//write header to fp
	if (fwrite(cfg->cur->buf, 1, cfg->cur->used, cfg->fp) <= 0){
		syslog(LOG_ERR, "mov write header error\n");
		movenc_error_exit(cfg);
		return -1;
	}

	//release
//	muxwr_buf_error_exit(&cfg->tmpbuf);
//	cfg->cur = NULL;

	syslog(LOG_DEBUG, "movenc_header_write OK\n");

	return 0;
}

static u32 avc_parse_nal_units(u8 type, u8 *buf, u32 size, u8 *frm_hdr, u32 * frm_hdr_length)
{//need to rewrite sps/pps/frm info header
	const u8 *p = buf;
	const u8 *end = p + 128;//suppose the max sps/pps/frm_hdr < 128bytes
	const u8 *nal_start, *nal_end;
	u8 sps_info = 0;
	u32 i = 4, index = 0;

//	syslog(LOG_WARNING, "type:%d,size:0x%x\n",type, size);
//	syslog(LOG_WARNING, "frame:0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\n", buf[0],buf[1],buf[2],buf[3],buf[4],buf[5],buf[6],buf[7]);

	nal_start = buf+4;//0000 0001 
	if (type != FRAME_TYPE_VIDEO_IDR) {
		if (size != 32) {
			frm_hdr[0] = (((size-4)>>24)&0xff);
			frm_hdr[1] = (((size-4)>>16)&0xff);
			frm_hdr[2] = (((size-4)>>8)&0xff);
			frm_hdr[3] = (((size-4))&0xff);
		} else {
			if (buf[16] == 0x0 && buf[17] == 0x0 && buf[18] == 0x0 && buf[19] == 0x1){
				///<empty frame total 32bytes. Need set 0x00000001 to 0x0000000c
				memcpy(frm_hdr, buf, 20);
				frm_hdr[3] = 0xc;
				frm_hdr[19] = 0xc;
				*frm_hdr_length = 20;

				return 20;
			} else {
				frm_hdr[0] = (((size-4)>>24)&0xff);
				frm_hdr[1] = (((size-4)>>16)&0xff);
				frm_hdr[2] = (((size-4)>>8)&0xff);
				frm_hdr[3] = (((size-4))&0xff);
			}
		}

//		syslog(LOG_WARNING, "frm_hdr2:0x%x 0x%x 0x%x 0x%x\n",frm_hdr[0], frm_hdr[1],frm_hdr[2],frm_hdr[3]);

		return 4;
	} else {
		i = 0;

		/*SPS: 0x00 0x00 0x00 0x01 0x67*/
		while (1) {
			if (p[i] == 0x0 && p[i + 1] == 0x0 && p[i + 3] == 0x1) {
				break;
			}
			i++;
		}
		i += 4;
		if (p[i] == 0x67)
			sps_info = 1;

		/*PPS: 0x00 0x00 0x00 0x01 0x68*/
		while (1) {
			if (p[i] == 0x0 && p[i + 1] == 0x0 && p[i + 3] == 0x1) {
				break;
			}
			i++;
		}
		i += 4;

		/*Media data:0x00 0x00 0x00 0x00 0x45*/
		while (1) {
			if (p[i] == 0x0 && p[i + 1] == 0x0 && p[i + 3] == 0x1) {
				break;
			}
			i++;
		}

		//write to head
		if (sps_info == 1){
			index = i;
			frm_hdr[0] = (((size - 4 - i) >> 24) & 0xff);
			frm_hdr[1] = (((size - 4 - i) >> 16) & 0xff);
			frm_hdr[2] = (((size - 4 - i) >> 8) & 0xff);
			frm_hdr[3] = (((size - 4 - i)) & 0xff);

			syslog(LOG_WARNING, "frm_hdr3:0x%x 0x%x 0x%x 0x%x\n",frm_hdr[0], frm_hdr[1],frm_hdr[2],frm_hdr[3]);
			syslog(LOG_WARNING, "i:0x%x,index:0x%x,0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\\n",i, index, p[i],p[i+1],p[i+2],p[i+3],p[i+4],p[i+5],p[i+6],p[i+7]);

			return index + 4;
		}

#if 0
		while (nal_start < end)
		{
			while (1) {
				if (p[i] == 0x0 && p[i+1] == 0x0 && p[i+3] == 0x1){
					break;
				}
				i++;
			}
			if (nal_start == end) {
				break;
			}

			nal_end = p + i;

			//write to head
			if (sps_info == 1){
				index = i;
				frm_hdr[0] = (((size-4-i)>>24)&0xff);
				frm_hdr[1] = (((size-4-i)>>16)&0xff);
				frm_hdr[2] = (((size-4-i)>>8)&0xff);
				frm_hdr[3] = (((size-4-i))&0xff);

				syslog(LOG_WARNING, "frm_hdr3:0x%x 0x%x 0x%x 0x%x\n",frm_hdr[0], frm_hdr[1],frm_hdr[2],frm_hdr[3]);
				syslog(LOG_WARNING, "i:0x%x,index:0x%x,0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\\n",i, index, p[i],p[i+1],p[i+2],p[i+3],p[i+4],p[i+5],p[i+6],p[i+7]);

				return index + 4;
			}

			sps_info = 1;
			nal_start = nal_end+4;
			index = i;
			i+=4;
		}

#endif
		return 0;
	}
}

static int movwr_frame_info(t_libmuxwr_cfg * cfg, movi_context *mov, movi_packet *pkt)
{
	movi_track *trk = &mov->tracks[pkt->stream_index];
	unsigned int samplesInChunk = 0;

	if (pkt->size == 0)
		return 0;

	if (trk->sampleSize)
	{
		samplesInChunk = pkt->size/trk->sampleSize;
	} else {
		samplesInChunk = 1;
	}

	movi_entry entry;
	memset(&entry, 0, sizeof(entry));

	entry.trackID = pkt->stream_index + 1;
	entry.pos = mov->mdat_offset;
	entry.samplesInChunk = samplesInChunk;
	entry.size = pkt->size;
	entry.entries = samplesInChunk;
	entry.dts = pkt->dts;
	entry.cts = pkt->pts - pkt->dts;
	entry.key_frame = pkt->pkt_flag_key;

	trk->sampleCount += samplesInChunk;

	trk->trackDuration = pkt->dts;//pkt->dts - trk->cluster.dts + pkt->duration;

/*	if (trk->enc.type == MUX_VIDEO_ENCV)
		trk->trackDuration = (trk->sampleCount + 1) * trk->timescale/trk->enc.vparam.frame_rate;
	else
		trk->trackDuration = (trk->sampleCount + 1) * pkt->duration * 1000/trk->enc.aparam.sample_rate;
*/
#ifdef MP4_PLAYBACK_ONLINE
	//g_idxfile_size += pkt->size;///< size
#endif
	//g_idxfile_size += 8;///< offset and size

	if (entry.key_frame)
	{
		trk->hasKeyframes++;
		//g_idxfile_size += 4;///< stss size
	}

	if (trk->entry == 0)
	{
		memcpy((u8 *)&trk->cluster, (u8 *)&entry, sizeof(movi_entry));
	}

//	syslog(LOG_WARNING, "movwr_frame_info\n");

	//add to index
	if (muxwr_buf_addstr(&cfg->idx, (u8 *)(&entry), sizeof(movi_entry)) < 0){
		return -1;
	}

	trk->entry++;
//	trk->trackDuration = trk->sampleCount*1024*1000/trk->timescale;

	mov->mdat_size += pkt->size;
	mov->mdat_offset += pkt->size;

//	syslog(LOG_DEBUG, "movwr_frame_info mdat_size:0x%x,sampleCount:%d,timescale:%d\n",mov->mdat_size, trk->sampleCount,trk->timescale);
//	syslog(LOG_DEBUG, "dts:%d,pts:%d,cluster.dts:%d,duration:%d,trackDuration:%d\n",pkt->dts,pkt->pts,trk->cluster.dts,pkt->duration,trk->trackDuration);

	return 0;
}

/*
 *
#define FRAME_TYPE_VIDEO_IDR       0
#define FRAME_TYPE_VIDEO_I         1
#define FRAME_TYPE_VIDEO_P         2
#define FRAME_TYPE_VIDEO_MIMG      3
#define FRAME_TYPE_AUDIO_ENCAUDIO  4
 *
 */
int movenc_frame_write(t_libmuxwr_cfg * cfg, FRAME * frame)
{
	int ret = 0;
	u32 frm_hdr_length;
	u8  frm_hdr[20]; //max hdr 20 bytes;
	u32 frm_offset = 0;

	t_movenc_private *priv = (t_movenc_private *)cfg->mux_private;
	memset(&priv->avp, 0, sizeof(priv->avp));

//	syslog(LOG_DEBUG, "FRAME type:%d,stream_index:0x%x,enc.type:0x%x\n", frame->type, priv->avp.stream_index, priv->mov.tracks[priv->avp.stream_index].enc.type);

	if ((frame->type == FRAME_TYPE_VIDEO_IDR) || (frame->type == FRAME_TYPE_VIDEO_I) || (frame->type == FRAME_TYPE_VIDEO_P)) {
		
		priv->avp.stream_index = priv->mov.video_index;
		priv->avp.pts = frame->ts;
		priv->avp.dts = priv->avp.pts - 20;
		priv->avp.duration = (priv->mov.global_time_scale/ priv->mov.tracks[priv->avp.stream_index].enc.vparam.frame_rate);

		priv->avp.size = frame->size + (frame->addr1 ? frame->size1 : 0);

		if (frame->type == FRAME_TYPE_VIDEO_IDR && priv->mov.tracks[priv->avp.stream_index].enc.extradata == NULL){
			memcpy(priv->spspps_buf, (u8 *)frame->addr, sizeof(priv->spspps_buf));
			priv->mov.tracks[priv->avp.stream_index].enc.extradata = priv->spspps_buf;
		}

		frm_hdr_length = 4;

		frm_offset = avc_parse_nal_units(frame->type, (u8 *)frame->addr, priv->avp.size, frm_hdr, &frm_hdr_length);
		priv->avp.size -= frm_offset;
		priv->avp.size += frm_hdr_length;

//		syslog(LOG_DEBUG, "movenc_frame_write size:0x%x,%d,size1:%d,len:%d,frm_off:0x%x\n", priv->avp.size, frame->size, frame->size1, frm_hdr_length, frm_offset);
//		syslog(LOG_DEBUG, "-----movenc_frame_write Video pts:%d,size:%d\n", frame->ts, priv->avp.size);

		if (frame->type == FRAME_TYPE_VIDEO_IDR || frame->type == FRAME_TYPE_VIDEO_I) {
			priv->avp.pkt_flag_key = 1;
        } else {
			priv->avp.pkt_flag_key = 0;
        }

		priv->avp.data_flag = MP4_VIDEO_DATA_FLAG;

	} else if (frame->type == FRAME_TYPE_VIDEO_MIMG){
		frm_hdr_length = 0;

		priv->avp.stream_index = priv->mov.mimg_index;
		priv->avp.pts = frame->ts;
		priv->avp.dts = priv->avp.pts-20;
		priv->avp.duration = (priv->mov.global_time_scale/ priv->mov.tracks[priv->avp.stream_index].enc.vparam.frame_rate);
		priv->avp.size = frame->size + (frame->addr1 ? frame->size1 : 0);

		priv->avp.data_flag = MP4_MIMG_DATA_FLAG;
	} else if (frame->type == FRAME_TYPE_AUDIO_ENCAUDIO){
		frm_hdr_length = 0;
		priv->avp.stream_index = priv->mov.audio_index;
		priv->avp.size = frame->size + (frame->addr1 ? frame->size1 : 0);;

		int input_len = 0;
		if (priv->mov.tracks[priv->avp.stream_index].enc.type == MUX_AUDIO_PCM_S16LE)
		{
			input_len = 0x100;
		} else if (priv->mov.tracks[priv->avp.stream_index].enc.type == MUX_AUDIO_AF4) {
			input_len = 1024;
			priv->avp.size -= 7;
			frm_offset = 7;
		}

		priv->avp.pts = frame->ts;
		priv->avp.dts = frame->ts;

//		syslog(LOG_DEBUG, "+++++movenc_frame_write Audio pts:%d,size:%d\n", frame->ts, priv->avp.size);

		//(timescale*input_len/(g_libmux_movenc_cfg.acfg->asample_rate)) *(g_libmuxwr_cfg.mux_cfg->total_aframecount);
		priv->avp.duration = input_len/* / 2*/; //(timescale*input_len/(g_libmux_movenc_cfg.acfg->asample_rate));

		priv->avp.data_flag = MP4_AUDIO_DATA_FLAG;
	} else {
		syslog(LOG_ERR, "movwr frame->type not support\n");
		return -1;
	}

	movwr_frame_info(cfg, &priv->mov, &priv->avp);

	if (frm_hdr_length){
		if (fwrite(frm_hdr, 1, frm_hdr_length, cfg->fp) <= 0){
			syslog(LOG_ERR, "movwr frm_hdr err\n");
			return -1;
		}
		ret += frm_hdr_length;
	}

	if (fwrite((u8 *)(frame->addr + frm_offset), 1, frame->size - frm_offset, cfg->fp) <= 0){
		syslog(LOG_ERR, "movwr frm_1 err\n");
		return -1;
	}

	ret += frame->size - frm_offset;
	if (frame->addr1) {
		if (fwrite((u8 *)(frame->addr1), 1, frame->size1, cfg->fp) <= 0) {
			syslog(LOG_ERR, "movwr frm_2 err\n");
			return -1;
		}
	}
	ret += frame->size1;

	return ret;
}

static int movwr_header_fix(t_libmuxwr_cfg *cfg)
{
	t_movenc_private *priv = (t_movenc_private *)cfg->mux_private;

#ifndef MP4_PLAYBACK_ONLINE
//	syslog(LOG_DEBUG, "movwr_header_fix mdata_pos:0x%x mdata_size:0x%x\n", priv->mov.mdat_pos,priv->mov.mdat_size);
	movwr_offset_set(cfg, priv->mov.mdat_pos);
#endif

	movwr_be32(cfg, priv->mov.mdat_size + 8);

	return 0;
}

static int movwr_mvhd_tag(t_libmuxwr_cfg *cfg)
{
	t_movenc_private * priv = (t_movenc_private *)cfg->mux_private;
	movi_context * mov = &priv->mov;

	int maxTrackID = 1, i;
	u32 maxTrackLenTemp = 0, maxTrackLen = 0;

	for(i = 0; i < mov->nb_streams; i++)
	{
		if(mov->tracks[i].entry > 0)
		{
			maxTrackLenTemp = mov->tracks[i].trackDuration*mov->tracks[i].timescale/mov->global_time_scale;
			syslog(LOG_DEBUG, "mvhd trackDuration:%d,timescale:%d,global:%d,len:%d\n",
					mov->tracks[i].trackDuration, mov->tracks[i].timescale, mov->global_time_scale, maxTrackLenTemp);

			if(maxTrackLen < maxTrackLenTemp)
			{
				maxTrackLen = maxTrackLenTemp;
			}
			if(maxTrackID < mov->tracks[i].trackID)
			{
				maxTrackID = mov->tracks[i].trackID;
			}
		}
	}
	movwr_be32(cfg,0x6c);///< mvhd size
	movwr_string(cfg, "mvhd");
	movwr_char(cfg, 0);
	movwr_be24(cfg, 0);
	movwr_be32(cfg,mov->time);///< creation time
	movwr_be32(cfg,mov->time);///< modification time
	movwr_be32(cfg,mov->timescale);///< timescale
	movwr_be32(cfg, mov->tracks[0].trackDuration);///< duration of longest track
	movwr_be32(cfg,0x10000);///< reserved (preferred rate) 1.0 = normal
	movwr_be16(cfg, 0x100);///< reserved (preferred volume) 1.0 = normal
	movwr_be16(cfg, 0x0); ///< reserved
	movwr_be32(cfg,0x0); ///< reserved
	movwr_be32(cfg,0x0); ///< reserved

	///< Matrix structure
	movwr_be32(cfg,0x10000); ///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x10000);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x40000000);///< reserved

	movwr_be32(cfg,0x0);///< preview time
	movwr_be32(cfg,0x0);///< preview duration
	movwr_be32(cfg,0x0);///< poster time
	movwr_be32(cfg,0x0);///< selection time
	movwr_be32(cfg,0x0);///< selectiom duration
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,maxTrackID + 1);///< Next track id

	return 0x6c;
}

static void movwr_thkd_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	t_movenc_private * priv = (t_movenc_private *)cfg->mux_private;
	movi_context * mov = &priv->mov;

	movwr_be32(cfg,0x5c);///< thkd size
	movwr_string(cfg, "tkhd");
	movwr_char(cfg, 0);
	movwr_be24(cfg, 0xf);///< flags track enabled

	movwr_be32(cfg,track->time); ///< creation time
	movwr_be32(cfg,track->time); ///< modification time
	movwr_be32(cfg,track->trackID); ///< track-id
	movwr_be32(cfg,0); ///< reserved
	movwr_be32(cfg, track->trackDuration/*/(track->timescale/mov->global_time_scale)*/); ///< reserved
	movwr_be32(cfg,0); ///< reserved
	movwr_be32(cfg,0); ///< reserved
	movwr_be32(cfg,0); ///< reserved (layer & alternate group)
	if(track->enc.type >= MUX_AUDIO_START)
	{
		movwr_be16(cfg, 0x100); ///< Volume
	}else{
		movwr_be16(cfg, 0x0);
	}
	movwr_be16(cfg, 0x0);///< reserved

	///< Matrix structure
	movwr_be32(cfg,0x10000); ///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x10000);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x0);///< reserved
	movwr_be32(cfg,0x40000000);///< reserved

	if(track->enc.type < MUX_AUDIO_START)
	{
		movwr_be32(cfg,track->enc.vparam.width*0x10000);///< width
		movwr_be32(cfg,track->enc.vparam.height*0x10000);///< height
	}else{
		movwr_be32(cfg,0);
		movwr_be32(cfg,0);
	}
}

static void movwr_mdhd_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	t_movenc_private * priv = (t_movenc_private *)cfg->mux_private;
	movi_context * mov = &priv->mov;

	movwr_be32(cfg,0x20); ///< mdhd size
	movwr_string(cfg, "mdhd");
	movwr_char(cfg, 0);
	movwr_be24(cfg, 0);///< flags
	movwr_be32(cfg,track->time); ///< creation time
	movwr_be32(cfg,track->time); ///< modification time
	movwr_be32(cfg,track->timescale);///< timescale(sample rate for audio)
	movwr_be32(cfg,track->trackDuration*track->timescale/mov->global_time_scale);
	movwr_be16(cfg, track->language);///< language
	movwr_be16(cfg, 0);///< reserved (quality)
}

static void movwr_hdlr_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	char *descr, *hdlr, *hdlr_type;

	if(!track)
	{
		hdlr = "dhlr";
		hdlr_type = "url ";
		descr = "DataHandler";
	}else{
		hdlr = "\0\0\0\0";
		if(track->enc.type < MUX_AUDIO_START)
		{
			hdlr_type = "vide";
			descr = "VideoHandler";
		}else if(track->enc.type >= MUX_AUDIO_START)
		{
			hdlr_type = "soun";
			descr = "SoundHandler";
		}
	}

	u32 hdlr_pos = movwr_start_tag(cfg, "hdlr", 0);
	movwr_be32(cfg,0);///< version & flags
	movwr_buf(cfg, (u8 *)hdlr, 4);
	movwr_string(cfg, hdlr_type);
	movwr_be32(cfg,0); ///< reserved
	movwr_be32(cfg,0); ///< reserved
	movwr_be32(cfg,0); ///< reserved
	movwr_char(cfg, strlen(descr));
	movwr_string(cfg, descr);
	movwr_end_tag(cfg, hdlr_pos);
}

static void movwr_vmhd_tag(t_libmuxwr_cfg * cfg)
{
	movwr_be32(cfg,0x14); ///< size
	movwr_string(cfg, "vmhd");
	movwr_be32(cfg,0x1); ///< version & flags
	movwr_be32(cfg,0x0); ///< graphics mode
	movwr_be32(cfg,0x0); ///< graphics mode
}

static void movwr_smhd_tag(t_libmuxwr_cfg * cfg)
{
	movwr_be32(cfg,16); ///< size
	movwr_string(cfg, "smhd");
	movwr_be32(cfg,0x0); ///< version & flags
	movwr_be16(cfg, 0x0); ///< balance
	movwr_be16(cfg, 0x0); ///< reserved
}

static void movwr_dref_tag(t_libmuxwr_cfg * cfg)
{
	movwr_be32(cfg,28); ///< size
	movwr_string(cfg, "dref");
	movwr_be32(cfg,0x0); ///< version & flags
	movwr_be32(cfg,0x1); ///< entry count
	movwr_be32(cfg,0xc); ///< size
	movwr_string(cfg, "url ");
	movwr_be32(cfg,1);///<version & flags
}

static void movwr_dinf_tag(t_libmuxwr_cfg * cfg)
{
	u32 dinf_pos = movwr_start_tag(cfg, "dinf", 0);
	movwr_dref_tag(cfg);
	movwr_end_tag(cfg, dinf_pos);
}

static void movwr_avcc_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	u32 avcc_pos = movwr_start_tag(cfg, "avcC", 0);
	u8 sps[32];
	u8 pps[6];
	int sps_size =0, pps_size = 0;
	int i;
	u32 start, end;
	u8 *addr = track->enc.extradata;

	//TODO::::: generate avcC from spspps, addr is from IDR frame
	//vrec_get_video_sps(track->enc->width, track->enc->height, sps, &sps_size);
	//vrec_get_video_pps(pps, &pps_size);
//	pps[1] = 0xee;

	if (addr && ((addr[0]<<24)|(addr[1]<<16)|(addr[2]<<8)|(addr[3]))==0x00000001){
		start = 4;	
		i = 4;
		while (i<48){
			if (((addr[i]<<24)|(addr[i+1]<<16)|(addr[i+2]<<8)|(addr[i+3]))==0x00000001){
				end = i;
				break;
			}

			sps[i-4] = addr[i];
			i++;
		}
		sps_size = end - start;
	}

	pps_size = 4;
	memcpy(pps, addr + i + 4, pps_size);

	movwr_char(cfg, 1);///< version
	movwr_char(cfg, sps[1]); ///< profile
	movwr_char(cfg, sps[2]); ///< profile compat
	movwr_char(cfg, sps[3]); ///< level
	movwr_char(cfg, 0xff); ///< 6bits reserved ( 111111 ) + 2bit nal size length-1
	movwr_char(cfg, 0xe1);///< 3bits reserved(111) + 5 bits number of sps
	movwr_be16(cfg, sps_size);///< sps size
	movwr_buf(cfg, sps, sps_size);
	movwr_char(cfg, 1); ///< number of pps
	movwr_be16(cfg, pps_size);
	movwr_buf(cfg, pps, pps_size);
	movwr_end_tag(cfg, avcc_pos);
}

static void movwr_video_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	u32 video_pos =0;
	
	if(track->enc.type == MUX_VIDEO_ENCV)
	{
		video_pos = movwr_start_tag(cfg, "avc1", 0);
	}else{
		video_pos = movwr_start_tag(cfg, "jpeg", 0);
	}
	movwr_be32(cfg,0);///< reserved
	movwr_be16(cfg, 0);///< reserved
	movwr_be16(cfg, 1);///< Data-reference index

	movwr_be16(cfg, 0);///< codec stream version
	movwr_be16(cfg, 0);///< codec stream revision
	movwr_be32(cfg,0);///< reserved
	if(track->enc.type == MUX_VIDEO_ENCV)
	{
		movwr_be32(cfg,0x0);///< reserved
		movwr_be32(cfg,0x0);///< reserved
	}else{
		movwr_be32(cfg,0x200);///< reserved
		movwr_be32(cfg,0x200);///< reserved
	}
	movwr_be16(cfg, track->enc.vparam.width);///< video width
	movwr_be16(cfg, track->enc.vparam.height);///< video height
	movwr_be32(cfg,0x480000);///< horizontal resolution 72dpi
	movwr_be32(cfg,0x480000);///< vertical resolution 72dpi
	movwr_be32(cfg,0);///< data size = 0
	movwr_be16(cfg, 1);///< frame count =1
	char compressor_name[32] = {0x05, 0x6d,0x6a, 0x70, 0x65, 0x67};
	if(track->enc.type == MUX_VIDEO_ENCV)
	{
		memset(compressor_name, 0, 32);
		movwr_char(cfg, strlen(compressor_name));///< fixme not sure, iso 14496-1 draft where it shall be set ot 0
		movwr_buf(cfg, (u8 *)compressor_name, 31);	
	}else{
		movwr_buf(cfg, (u8 *)compressor_name, 32);	
	}
	movwr_be16(cfg, 0x18);///< reserved
	movwr_be16(cfg, 0xffff);///< reserved
	if(track->enc.type == MUX_VIDEO_ENCV)
	{
		movwr_avcc_tag(cfg, track);
	}else{
		//movwr_mp4v_esds_tag(track);
	}
	movwr_end_tag(cfg, video_pos);
}

static void movwr_esds_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	u32 esds_pos = movwr_start_tag(cfg, "esds", 0);

	unsigned char aac_esds[28] = 
	{
		0x03, 0x19, 0x00, 0x00, 0x00, 0x04, 0x11, 0x40, 0x15, 0x00, 0x03, 0x00, 0x00, 0x00, 0xbb, 0x80, 0x00, 0x00, 0xbb, 0x80, 0x05, 0x02, 0x15, 0x08, 0x06, 0x01, 0x02
	};
	unsigned int srIndexTable[12] = {96000,88200,64000,48000,44100,32000,24000,22050,16000,12000,11025,8000};
	u8 i, sample_idx = 0;
	for (i = 0; i< sizeof(srIndexTable)/sizeof(srIndexTable[0]);i++)
	{
		if (track->enc.aparam.sample_rate == srIndexTable[i])
		{
			sample_idx = i;
			break;
		}
	}
	u16 *buf = (u16 *)&aac_esds[22];
	aac_esds[22] = BIT4|((sample_idx & 0xF) >> 1);
	aac_esds[23] = ((sample_idx & 0x1) << 7)|((track->enc.aparam.channels&0xf)<<3);;
	syslog(LOG_DEBUG, "esds idx:0x%x channels:%d, 0x%x\n", sample_idx,track->enc.aparam.channels, *buf);
	movwr_be32(cfg,0);
	movwr_buf(cfg, (u8 *)aac_esds, 27);
	movwr_end_tag(cfg, esds_pos);
}

static void movwr_audio_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	u32 audio_pos;
	if(track->enc.type == MUX_AUDIO_PCM_S16LE)
	{
		audio_pos = movwr_start_tag(cfg, "sowt", 0);
	}else if(track->enc.type == MUX_AUDIO_AF4)
	{
		audio_pos = movwr_start_tag(cfg, "mp4a", 0);
	}else{
		syslog(LOG_WARNING, "mp4 audio tag format not support\n");
		return;
	}
	movwr_be32(cfg,0);///< reserved
	movwr_be16(cfg, 0);///< reserved
	movwr_be16(cfg, 1);///< data reference index = 1
	///< sound description
	movwr_be16(cfg, 0); ///< version
	movwr_be16(cfg, 0); ///< revision level
	movwr_be32(cfg,0); ///< reserved
	
	movwr_be16(cfg, track->enc.aparam.channels);  //////channel number
	movwr_be16(cfg, track->enc.aparam.bps);  ///sample size (bit)
	movwr_be16(cfg, 0);
	movwr_be16(cfg, 0); ///< packet size
	movwr_be16(cfg, track->timescale);
	movwr_be16(cfg, 0);
	if(track->enc.type == MUX_AUDIO_AF4)
	{
		movwr_esds_tag(cfg, track); ///< for mp4a
	}
	movwr_end_tag(cfg, audio_pos);
}

static void movwr_stsd_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	u32 stsd_pos = movwr_start_tag(cfg, "stsd", 0);
	movwr_be32(cfg,0x0); ///< version & flags
	movwr_be32(cfg,0x1); ///< entry count
	if (track->enc.type < MUX_AUDIO_START)
	{
		movwr_video_tag(cfg, track);
	} else if (track->enc.type >= MUX_AUDIO_START) {
		movwr_audio_tag(cfg, track);
	}
	movwr_end_tag(cfg, stsd_pos);
}

///< time to sample atom
static void movwr_stts_tag(t_libmuxwr_cfg *cfg, movi_track *track)
{
	movi_entry *last_entry = NULL;
	int entries = 0;
	int last_duration = 0;
	int last_count = 0;
	u32 duration;
	t_movenc_private * priv = (t_movenc_private *)cfg->mux_private;

	track->moov_hdr.stts_tag.pos = movwr_start_tag(cfg, "stts", 0);

	//movwr_be32(cfg,atom_size);
	//movwr_string(cfg, "stts");
	movwr_be32(cfg,0); ///< version & flags
//	movwr_be32(cfg,0); ///< entries, fix later: TODO
 	//movwr_be32(cfg,entries);

	if ((track->enc.type == MUX_VIDEO_ENCV) || (track->enc.type == MUX_AUDIO_AF4)){
		entries = 1; ///for video, fixed later //lanting

		movwr_be32(cfg,entries);
		movwr_be32(cfg,track->sampleCount);
		if (track->enc.type == MUX_VIDEO_ENCV) {
			movwr_be32(cfg, track->timescale/track->enc.vparam.frame_rate);//track->timescale/track->enc.vparam.frame_rate);
		} else {
			movwr_be32(cfg, priv->avp.duration);//track->timescale/track->enc.vparam.frame_rate);
		}

#if 0
		//IDX didnot use file, so idx.buf is the full movi_entry.
		movi_entry * e = (movi_entry *)cfg->idx.buf;
		for (; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
			if (e->trackID != track->trackID){
				continue;
			}
			if(last_entry == NULL){
				last_entry = e;
			}else{
				duration = e->dts - last_entry->dts;
				if(last_count == 0){
					last_count ++;
					last_duration = duration;
				}else{
					if(last_duration == duration){
						last_count ++;
					}else{
						movwr_be32(cfg,last_count);
						movwr_be32(cfg,last_duration);
						entries ++;
						last_duration = duration;
						last_count = 1;
					}
				}
			}
		}

		if (last_entry) {
			if (last_count) {
				movwr_be32(cfg,last_count);
				movwr_be32(cfg,last_duration);
				entries ++;
			}
			duration = track->trackDuration - last_entry->dts;
			movwr_be32(cfg,1);
			movwr_be32(cfg,duration);
			entries ++;
		}
#endif
	} else if (track->enc.type >= MUX_AUDIO_START) {
		//other audio format
		movwr_be32(cfg,track->sampleCount); //count
		movwr_be32(cfg,1); //duration
		entries = 1;
	}

	track->moov_hdr.stts_tag.size = 16 + (entries * 8)/*movwr_offset_get(cfg) - track->moov_hdr.stts_tag.pos*/;
	track->moov_hdr.stts_entries = entries;
}

static void movwr_stss_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	movwr_start_tag(cfg, "stss", 16 + track->hasKeyframes*4);
	movwr_be32(cfg,0);///< version & flags
 	movwr_be32(cfg,track->hasKeyframes); ///< key frames

	int i = 0;
	int entries = 0;
	movi_entry * e = (movi_entry *)cfg->idx.buf;
	for (; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
		if(e->trackID != track->trackID){
			continue;
		}
		i ++;
		if(e->key_frame == 1){
			if(entries > track->hasKeyframes){
				syslog(LOG_ERR, "stss entries > keyframes: %x, %x, %x, %d\n", cfg->idx.buf, cfg->idx.used, e, entries);
				break;
			}
			movwr_be32(cfg,i);
			entries ++;
		}
	}
	syslog(LOG_DEBUG, "stss entries > keyframes: %x, %x, %x, %d\n", cfg->idx.buf, cfg->idx.used, e, entries);
	if(entries < track->hasKeyframes){
		syslog(LOG_DEBUG, "WARN: stss, fill lost key frames:%d->%d\n", entries, track->hasKeyframes);
		for(;entries < track->hasKeyframes; entries ++){
			movwr_be32(cfg,1);
		}
	}
}

///< sample to chunk atom
static void movwr_stsc_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	track->moov_hdr.stsc_tag.pos = movwr_start_tag(cfg, "stsc", 0);

	movwr_be32(cfg,0);///< version & flags
	track->moov_hdr.stsc_idx.pos = movwr_offset_get(cfg);
	movwr_be32(cfg,0); ///< dummy index
	//movwr_be32(cfg,track->entry);///< entry count

	int i=0;
	int oldval = -1;
	int samplesInChunk = 0;
	int entries = 0;
	movi_entry * e = (movi_entry *)cfg->idx.buf;
	for(; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
		if(e->trackID != track->trackID){
			continue;
		}
		i ++;
		samplesInChunk = e->samplesInChunk;
		if(oldval != samplesInChunk){
			movwr_be32(cfg,i); ///< first chunk
			movwr_be32(cfg,samplesInChunk);///< samples per chunk
			movwr_be32(cfg,0x1);///< sample description index
			oldval = samplesInChunk;
			entries ++;
		}
	}
	syslog(LOG_DEBUG, "movwr_stsc_tag > keyframes: %x, %x, %x, %d\n", cfg->idx.buf, cfg->idx.used, e, entries);
	track->moov_hdr.stsc_idx.size = entries;
	track->moov_hdr.stsc_tag.size = movwr_offset_get(cfg) - track->moov_hdr.stsc_tag.pos;
}

static void movwr_stsz_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	int entries = 0;

	if(track->enc.type >= MUX_AUDIO_START && track->enc.type != MUX_AUDIO_AF4)
	{
		movi_entry * e = (movi_entry *)cfg->idx.buf;
		for(; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
			if(e->trackID != track->trackID){
				continue;
			}
			entries += e->entries;
		}
		movwr_start_tag(cfg, "stsz", 20);
		movwr_be32(cfg,0);///<version & flag
		int size = 0;
		if(track->cluster.entries){
			size = track->cluster.size/track->cluster.entries;
		}
		movwr_be32(cfg,size);
		movwr_be32(cfg,entries);
	} else {
		movwr_start_tag(cfg, "stsz", 20 + track->entry*4);
		movwr_be32(cfg,0);///<version & flag
		movwr_be32(cfg,0);///< sample size
		movwr_be32(cfg,track->entry);///< sample count
		movi_entry * e = (movi_entry *)cfg->idx.buf;
		for (; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
			if (e->trackID != track->trackID){
				continue;
			}
			int size = 0;
			if (e->entries){
				size = e->size / e->entries;
			}
			movwr_be32(cfg,size);
			entries++;
		}

		if (entries != track->entry){
			syslog(LOG_DEBUG, "stsz, entries != entry, %d, %d\n", entries, track->entry);
		}
	}
}

static void movwr_stco_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	movwr_start_tag(cfg, "stco", 16 + track->entry*4);
	movwr_be32(cfg,0x0);
	movwr_be32(cfg,track->entry);///< entry count

	//stco start
	track->stco_oft = movwr_offset_get(cfg);

#ifdef MP4_PLAYBACK_ONLINE 
	//TODO
//	movbuf_oft += (track->entry*4);
//	return;
#endif

	int entries = 0;
	movi_entry * e = (movi_entry *)cfg->idx.buf;
	for(; e < (movi_entry *)(cfg->idx.buf + cfg->idx.used); e++){
		if(e->trackID != track->trackID){
			continue;
		}
		movwr_be32(cfg,e->pos);
		entries ++;
	}
	if(entries != track->entry){
		syslog(LOG_DEBUG, "stco, entries != entry, %d, %d\n", entries, track->entry);
	}
}

static void movwr_stbl_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	track->moov_hdr.stbl_tag.pos = movwr_start_tag(cfg, "stbl", 0);

	movwr_stsd_tag(cfg, track);

	movwr_stts_tag(cfg, track);

	if(track->enc.type == MUX_VIDEO_ENCV && track->hasKeyframes && track->hasKeyframes < track->entry)
	{
		movwr_stss_tag(cfg, track);
	}
	movwr_stsc_tag(cfg, track);
	movwr_stsz_tag(cfg, track);
	movwr_stco_tag(cfg, track);

	track->moov_hdr.stbl_tag.size = movwr_offset_get(cfg) - track->moov_hdr.stbl_tag.pos;
}

static void movwr_minf_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	track->moov_hdr.minf_tag.pos = movwr_start_tag(cfg, "minf", 0);
	if(track->enc.type < MUX_AUDIO_START)
	{
		movwr_vmhd_tag(cfg);
	}else if(track->enc.type >= MUX_AUDIO_START)
	{
		movwr_smhd_tag(cfg);
	}
	movwr_dinf_tag(cfg);
	movwr_stbl_tag(cfg, track);

	track->moov_hdr.minf_tag.size = movwr_offset_get(cfg) - track->moov_hdr.minf_tag.pos;
}

static void movwr_mdia_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	track->moov_hdr.mdia_tag.pos = movwr_start_tag(cfg, "mdia", 0);

	movwr_mdhd_tag(cfg, track);
	movwr_hdlr_tag(cfg, track);
	movwr_minf_tag(cfg, track);

	track->moov_hdr.mdia_tag.size = movwr_offset_get(cfg) - track->moov_hdr.mdia_tag.pos;
}

#ifdef CONFIG_SPHERICAL_METADATA
#define SPHERICAL_UUID_LENGTH    (16)
u8 uuid[SPHERICAL_UUID_LENGTH] = {0xFF, 0xCC, 0x82, 0x63, 0xF8, 0x55, 0x4A, 0x93, 0x88, 0x14, 0x58, 0x7A, 0x02, 0x52, 0x1F, 0xDD};

#define SPHERICAL_XML_HEADER \
    "<?xml version=\"1.0\"?>"\
    "<rdf:SphericalVideo\n"\
    "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"\
    "xmlns:GSpherical=\"http://ns.google.com/videos/1.0/spherical/\">"

#define SPHERICAL_XML_CONTENTS \
    "<GSpherical:Spherical>true</GSpherical:Spherical>"\
    "<GSpherical:Stitched>true</GSpherical:Stitched>"\
    "<GSpherical:StitchingSoftware>"\
    "OmniVision Spherical Metadata"\
    "</GSpherical:StitchingSoftware>"\
    "<GSpherical:ProjectionType>equirectangular</GSpherical:ProjectionType>"

#define SPHERICAL_XML_FOOTER "</rdf:SphericalVideo>"

static void movwr_uuid_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	char xmp_buf[500];
	int xmp_buf_size = 0;

	strcpy(xmp_buf, SPHERICAL_XML_HEADER);
	strcat(xmp_buf, SPHERICAL_XML_CONTENTS);
	strcat(xmp_buf, SPHERICAL_XML_FOOTER);
	xmp_buf_size = strlen(xmp_buf);

	track->moov_hdr.uuid_tag.pos = movwr_start_tag(cfg, "uuid", 8 + SPHERICAL_UUID_LENGTH + xmp_buf_size);
	movwr_buf(cfg, uuid, SPHERICAL_UUID_LENGTH);
	movwr_buf(cfg, xmp_buf, xmp_buf_size);

	track->moov_hdr.uuid_tag.size = movwr_offset_get(cfg) - track->moov_hdr.uuid_tag.pos;
}
#endif

static void movwr_trak_tag(t_libmuxwr_cfg * cfg, movi_track *track)
{
	track->moov_hdr.trak_tag.pos = movwr_start_tag(cfg, "trak", 0);

	movwr_thkd_tag(cfg, track);
	movwr_mdia_tag(cfg, track);
	
#ifdef CONFIG_SPHERICAL_METADATA
	if(track->enc.type == MUX_VIDEO_ENCV)
	{
		movwr_uuid_tag(cfg, track);
	}
#endif

	track->moov_hdr.trak_tag.size = movwr_offset_get(cfg) - track->moov_hdr.trak_tag.pos;
}

static void moov_tag_fix(t_libmuxwr_cfg * cfg, int tag_start, int tag_size) {
	int curlen = movwr_offset_get(cfg);
	movwr_offset_set(cfg, tag_start);

	movwr_be32(cfg,tag_size);

	movwr_offset_set(cfg, curlen);
}

static void movwr_moov_tag(t_libmuxwr_cfg * cfg)
{
	int i;
	t_movenc_private *priv = (t_movenc_private *)cfg->mux_private;
	movi_context *mov = &priv->mov;

	mov->moov_pos = movwr_start_tag(cfg, "moov", 0);
	mov->timescale = priv->mov.global_time_scale;

	for (i = 0; i < mov->nb_streams; i++)
	{
		if (mov->tracks[i].entry <= 0) {
			continue;
		}

		mov->tracks[i].time = mov->time;
		mov->tracks[i].trackID = i+1;
	}
	
	movwr_mvhd_tag(cfg);
	syslog(LOG_DEBUG, "movwr_moov_tag %d, timescale:%d\n", mov->nb_streams, mov->timescale);
	for (i = 0; i < mov->nb_streams; i++)
	{
		if (mov->tracks[i].entry > 0) {
			movwr_trak_tag(cfg, &(mov->tracks[i]));
		}
	}

	mov->moov_size = movwr_offset_get(cfg) - mov->moov_pos;

	moov_tag_fix(cfg, mov->moov_pos, mov->moov_size);

	//make cluster aligned
#ifdef MP4_PLAYBACK_ONLINE
	/*
	u32 clustersize = FSSectorsPerCluster*FSBytesPerSector;
	if(clustersize < 0x2000) clustersize = 0x2000;
	u32 needadd = clustersize-(movwr_offset_get(cfg) % clustersize);
	if(needadd){
		movwr_be32(cfg,needadd);
		movwr_string(cfg, "free");
		memset(&(movbuf_start[movbuf_oft]), 0, needadd-8);
		movbuf_oft += (needadd - 8);
	}
	*/
#endif

	//fix moov pos, track pos, mdia pos, minf pos, stbl pos
	for (i = 0; i < mov->nb_streams; i++)
	{
		if (mov->tracks[i].entry > 0) {
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.trak_tag.pos, 
							mov->tracks[i].moov_hdr.trak_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.mdia_tag.pos, 
							mov->tracks[i].moov_hdr.mdia_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.minf_tag.pos, 
							mov->tracks[i].moov_hdr.minf_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.stbl_tag.pos, 
							mov->tracks[i].moov_hdr.stbl_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.stsc_tag.pos, 
							mov->tracks[i].moov_hdr.stsc_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.stts_tag.pos, 
							mov->tracks[i].moov_hdr.stts_tag.size);
			moov_tag_fix(cfg, mov->tracks[i].moov_hdr.stsc_idx.pos, 
							mov->tracks[i].moov_hdr.stsc_idx.size);
#ifdef MP4_PLAYBACK_ONLINE
			//TODO: movwr_stco_tag_fix(&(mov->tracks[i]));
#endif
		}
	}
}

int movenc_tail_write(struct s_libmuxwr_cfg * cfg)
{
	//TODO: MP4_PLAYBACK_ONLINE: fix header first, then tail
	//otherwise, fix tail first, then header
	syslog(LOG_DEBUG, "\nmovenc_tail_write\n");

	/***  fix tail ***/
	if (cfg->tail.use_file) {
		syslog(LOG_ERR, "cfg->tail cannot use file\n");
		movenc_error_exit(cfg);

		return -1;
	}

	if(cfg->tail.size == 0){
		movenc_error_exit(cfg);
		syslog(LOG_ERR, "ERR: mux->tail.size not set\n");
		return -1;
	}
	if (muxwr_buf_init(&cfg->tail) < 0){
		movenc_error_exit(cfg);
		return -1;
	}
	memset(cfg->tail.buf, 0, sizeof(cfg->tail.size));

	cfg->cur = &cfg->tail;

#ifdef MP4_PLAYBACK_ONLINE
	//movwr_idx_fix(cfg);
#endif

	movwr_moov_tag(cfg);

	/*** append tail to fp ***/
	cfg->cur = NULL;
	if (fwrite(cfg->tail.buf, 1, cfg->tail.used, cfg->fp) <= 0) {
		syslog(LOG_ERR, "append tail err\n");
		movenc_error_exit(cfg);
		return -1;
	}

	//done, release tmpbuf
	muxwr_buf_error_exit(&cfg->tmpbuf);

	/***  fix header ***/
	memset(&cfg->tmpbuf, 0, sizeof(cfg->tmpbuf));
	cfg->tmpbuf.size = 512;
	if (muxwr_buf_init(&cfg->tmpbuf) < 0) {
		movenc_error_exit(cfg);
		return -1;
	}

	//write to tmpbuf now.
	cfg->cur = &cfg->tmpbuf;

	int ret = 0;
	//read from fp
	ret = fseek(cfg->fp, 0, SEEK_SET);
	syslog(LOG_DEBUG, "fseek ret:%d,size:%d,buf:0x%x\n", ret,cfg->tmpbuf.size,cfg->cur->buf);
	if (fread(cfg->cur->buf, 1, cfg->tmpbuf.size, cfg->fp) <= 0){
		syslog(LOG_ERR, "read hdrbuf from file fail flags:0x%x\n", cfg->fp->flags); ///error-----------------
		return -1;
	}

	movwr_header_fix(cfg);

	//write to fp
	fseek(cfg->fp, 0, SEEK_SET);
	if (fwrite(cfg->cur->buf, 1, cfg->tmpbuf.size, cfg->fp) <= 0){
		syslog(LOG_ERR, "write hdrbuf to file fail flag:0x%x\n", cfg->fp->flags);
		return -1;
	}

	//done, release tmpbuf
	muxwr_buf_error_exit(&cfg->tmpbuf);
	cfg->cur = NULL;

	//flush idx
	muxwr_buf_flush(&cfg->idx);

#if 0
	/*** append idx ***/
	if (cfg->idx.fp){
		fseek(cfg->idx.fp, 0, SEEK_SET);
		char *catbuf = malloc(512);
		if (catbuf == NULL) {
			syslog(LOG_ERR, "malloc catbuf 512 failed\n");
			movenc_error_exit(cfg);
			return -3;
		}
		while (1) {
			int n = fread(catbuf, 1, 512, cfg->idx.fp);
			if (n <= 0){
				break;
			}
			if (fwrite(catbuf, 1, n, cfg->fp) <= 0){
				syslog(LOG_ERR, "write catbuf fail\n");
				free(catbuf);
				movenc_error_exit(cfg);
				return -3;
			}
		}

		free(catbuf);
		fclose(cfg->idx.fp);
		cfg->idx.fp = NULL;

		//remove idx file
		unlink(cfg->idx.fname);
	} else {
//		syslog(LOG_DEBUG, "cfg->idx.buf used:%d\n", cfg->idx.used);
		if (fwrite(cfg->idx.buf, 1, cfg->idx.used, cfg->fp) <= 0){
			syslog(LOG_ERR, "write idx to file fail\n");
			movenc_error_exit(cfg);
			return -1;
		}
	}
#endif

	syslog(LOG_DEBUG, "\nmovenc_tail_write OK\n");

	return 0;
}

const t_libmux_cfg g_libmux_mp4_cfg =
{
	.header_write = movenc_header_write,
	.frame_write = movenc_frame_write,
	.tail_write = movenc_tail_write,
};
