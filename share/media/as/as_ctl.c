#include "includes.h"

#define AENC_MAX_STREAMS	8 
#define AFDIST_FRMLIST_NUM 	192	//frame list number should be no less than that configured in mpu/ram/full/datapath.c

AFDIST_DECLARE(afdist_as, AFDIST_FRMLIST_NUM);

static t_virtual_as vt_as[AENC_MAX_STREAMS];

int libas_init(int as_id, t_arec_inpara *inp)
{
	if(as_id >= AENC_MAX_STREAMS){
		return 1;
	}
	vt_as[as_id].id = as_id;
	vt_as[as_id].afdist = &afdist_as;

	u32 ret = virtual_as_init(&vt_as[as_id]);

	if(ret != VIRTUAL_AS_NOT_DONE){
		return ret;	
	}else{
		return libaenc_init(inp);
	}
}

int libas_start(int as_id)
{
	if(as_id >= AENC_MAX_STREAMS){
		return 1;
	}
	u32 ret = virtual_as_start(&vt_as[as_id]);

	if(ret != VIRTUAL_AS_NOT_DONE){
		return ret;	
	}else{
		return libaenc_start();
	}
}

int libas_stop(int as_id)
{
	if(as_id >= AENC_MAX_STREAMS){
		return 1;
	}

	u32 ret = virtual_as_stop(&vt_as[as_id]);

	if(ret != VIRTUAL_AS_NOT_DONE){
		return ret;	
	}else{
		return libaenc_stop();
	}
}

int libas_exit(int as_id)
{
	if(as_id >= AENC_MAX_STREAMS){
		return 1;
	}

	u32 ret = virtual_as_exit(&vt_as[as_id]);	

	if(ret != VIRTUAL_AS_NOT_DONE){
		return ret;	
	}else{
		return libaenc_exit();
	}
}

extern t_ertos_eventhandler arecevt;
FRAME * libas_get_frame(int as_id, u32 timeout)
{
	if(as_id >= AENC_MAX_STREAMS){
		return NULL;
	}

	FRAME* frame = virtual_as_get_frm(&vt_as[as_id]);
	if (frame == NULL && arecevt != 0){
		//no frame buf now, wait empty frm buf ready
		ertos_event_wait(arecevt, timeout);

		frame = virtual_as_get_frm(&vt_as[as_id]);
	}
	return frame;
}

int libas_remove_frame(int as_id, FRAME *frm)
{
	if(as_id >= AENC_MAX_STREAMS){
		return NULL;
	}
	return virtual_as_free_frm(&vt_as[as_id], frm);
}
