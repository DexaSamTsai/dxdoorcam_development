#include "includes.h"

static int afdist_init(AFDIST* afdist){
	int ret = 0;	
	afdist->pool = pool_init(sizeof(t_afdist_frm), afdist->num, (u8 *)(((u32)afdist->poolbuf)|BIT31) );
	afdist->cnt = 0;
	afdist->head = NULL;
	afdist->tail = NULL;
	afdist->running_mask = 0;
	afdist->init_mask = 0;
	pthread_mutex_init(&afdist->mutex, NULL);
	return ret;
}

int virtual_as_init(t_virtual_as* vt_as){
	u32 ret = 0;	
	if(vt_as == NULL || vt_as->afdist == NULL){
		syslog(LOG_ERR, "ERR: vt_as or afdist NULL\n");	
		return -1;	
	}
	if(vt_as->id > 31){
		syslog(LOG_ERR, "ERR: AS id more than 31\n");	
		return -1;	
	}
	vt_as->mask = 1<<vt_as->id;//每一路码流都有自己独立的标识位，作为该码流使能状态以及每一帧的读取状态的标记
	if(vt_as->afdist->init_mask == 0){
		//当前虚拟码流初始化是实际码流的首次初始化，需要设置特别标志去初始化实际码流
		ret = VIRTUAL_AS_NOT_DONE;
		afdist_init(vt_as->afdist);	
	}
	vt_as->afdist->init_mask |= vt_as->mask;//标记该虚拟码流已经初始化
	return ret;	
}

int virtual_as_start(t_virtual_as* vt_as){
	u32 ret = 0;	
	if(vt_as == NULL || vt_as->afdist == NULL){
		syslog(LOG_ERR, "ERR: vt_as or afdist NULL\n");	
		return -1;	
	}
	if(vt_as->mask == 0){
		syslog(LOG_ERR, "AS %d not initalized\n", vt_as->id);	
		return -1;	
	}

	pthread_mutex_lock(&vt_as->afdist->mutex);

	if((vt_as->afdist->running_mask & vt_as->mask) == 0){
		if(vt_as->afdist->running_mask  == 0){
			//running_mask为零，代表当前虚拟码流enable是实际码流的首次enable，需要返回特定标志去enable实际码流
			ret = VIRTUAL_AS_NOT_DONE;
		}
		//else: running_mask非零，实际码流已经在running，无需再启动
		vt_as->afdist->running_mask |= vt_as->mask;//标记该虚拟码流已经启动
	}

	pthread_mutex_unlock(&vt_as->afdist->mutex);

	return ret;	
}

int virtual_as_stop(t_virtual_as* vt_as){
	if(vt_as == NULL || vt_as->afdist == NULL){
		syslog(LOG_ERR, "ERR: vt_as or afdist NULL\n");	
		return -1;	
	}
	if(vt_as->mask == 0){
		syslog(LOG_WARNING, "AS%d not initalized\n", vt_as->id);	
		return -1;	
	}

	pthread_mutex_lock(&vt_as->afdist->mutex);

	int ret = 0;
	if((vt_as->afdist->running_mask & vt_as->mask) != 0){
		vt_as->afdist->running_mask &= ~(vt_as->mask);//标记该虚拟码流已经停止
		if(vt_as->afdist->running_mask == 0){
			//running_mask为零，代表所有虚拟码流都已经停止，返回特定标志请求停止实际码流	
			ret = VIRTUAL_AS_NOT_DONE;	
		}
	}

	pthread_mutex_unlock(&vt_as->afdist->mutex);

	return ret;
}

static t_afdist_frm * afdist_alloc(AFDIST * afdist){
	t_afdist_frm * ret = (t_afdist_frm *)pool_element_alloc(afdist->pool);
	if(ret){
		memset(ret, 0, sizeof(t_afdist_frm));
		afdist->cnt ++;
	}
	return ret;
}

static int afdist_frm_add(AFDIST* afdist, FRAME* as_frm){
	t_afdist_frm* frm = afdist_alloc(afdist);	
	if(frm == NULL){
		syslog(LOG_ERR, "afdist alloc error! %d, %d\n", afdist->cnt, afdist->num);
		return -2;
	}
	frm->as_flag = afdist->running_mask;
	//as_flag的每一位代表对应码流是否需要读取该帧,1为需要读取，0为不需要读取.每一帧的free都必须等到as_flag为零
	frm->vfrm = as_frm;
	if(afdist->head == NULL){
		afdist->head = frm;
		afdist->tail = frm;
	}else{
		afdist->tail->next = frm;	
		afdist->tail = frm;
	}
	return 0;
}

FRAME* virtual_as_get_frm(t_virtual_as* vt_as){
	if(vt_as == NULL || vt_as->afdist == NULL){
		syslog(LOG_ERR, "ERR: vt_as or afdist NULL\n");	
		return NULL;	
	}
	if(vt_as->mask == 0){
		syslog(LOG_WARNING, "AS%d not initalized\n", vt_as->id);	
		return NULL;	
	}

	pthread_mutex_lock(&vt_as->afdist->mutex);

	FRAME* as_frm = NULL;
	//先往AFDIST链表里面添加有效的stream frame
	if((vt_as->afdist->running_mask & vt_as->mask) != 0 //只有该码流在enable状态才可能往afdist链表里添加有效节点	
		&& (vt_as->afdist->num > vt_as->afdist->cnt) //AFDIST链表尚有空余节点
		&& ((vt_as->afdist->tail == NULL) //tail为空意味着当前AFDIST链表为空，需要添加节点
			|| (vt_as->afdist->tail->as_flag & vt_as->mask) == 0)){	
		//当前链表里最末一帧没有待读取标记，意味该码流start后AFDIST尚未加入新的frame，因此需要get stream frame
		as_frm = libaenc_get_frame(0);
		if(as_frm != NULL){
			if(afdist_frm_add(vt_as->afdist, as_frm) != 0){
				goto err_unlock;
			}	
		}else{
			goto err_unlock;
		}
	}
	as_frm = NULL;
	t_afdist_frm* frm = (t_afdist_frm*)vt_as->afdist->head;	
	while(frm != NULL){
		if((frm->as_flag & vt_as->mask) != 0 && (frm->reading_flag & vt_as->mask) == 0){
			//这一帧对应的当前虚拟码流待读取标记为1，并且这一帧不在reading状态，返回该有效帧	
			frm->reading_flag |= vt_as->mask;	//标记为reading状态,下次读取时若为reading会继续往后搜索链表
			as_frm = frm->vfrm;
			break;	
		}
		frm = frm->next;
	}

	pthread_mutex_unlock(&vt_as->afdist->mutex);
	return as_frm;

err_unlock:
	pthread_mutex_unlock(&vt_as->afdist->mutex);
	return NULL;
}

static int afdist_frm_clean(AFDIST* afdist){
	//清空链表里所有待读取标记为零的frame	
	while(afdist->head && afdist->head->as_flag == 0){
		t_afdist_frm* relframe = afdist->head;
		libaenc_remove_frame((FRAME*)relframe->vfrm);
		if(relframe->next == NULL){
			afdist->tail = NULL;	
		}
		afdist->head = relframe->next;
		pool_element_free(afdist->pool, relframe);
		afdist->cnt--;
	}
	return 0;
}

int virtual_as_free_frm(t_virtual_as* vt_as, FRAME* as_frm){
	if(vt_as == NULL || vt_as->afdist == NULL || vt_as->mask == 0 || as_frm == NULL){
		syslog(LOG_ERR, "ERR: vt_as/afdist/as_frm NULL or not initalized\n");	
		return -1;	
	}

	pthread_mutex_lock(&vt_as->afdist->mutex);

	t_afdist_frm* frm = (t_afdist_frm*)vt_as->afdist->head;
	while(frm != NULL){
		if(frm->vfrm == as_frm){
			frm->as_flag &= ~(vt_as->mask);//清空待读取标记
			frm->reading_flag &= ~(vt_as->mask);//清除reading标记
			break;
		}
		frm = frm->next;
	}
	if(frm == NULL){
		syslog(LOG_ERR, "free frm not found!\n");
		pthread_mutex_unlock(&vt_as->afdist->mutex);
		return -2;
	}

	int ret = afdist_frm_clean((AFDIST*)vt_as->afdist);

	pthread_mutex_unlock(&vt_as->afdist->mutex);
	return ret;
}

int virtual_as_exit(t_virtual_as* vt_as){
	u32 ret = 0;
	if(vt_as == NULL || vt_as->afdist == NULL){
		syslog(LOG_ERR, "ERR: vt_as or afdist NULL\n");	
		return -1;	
	}
	if(vt_as->mask == 0){
		syslog(LOG_WARNING, "AS%d not initalized\n", vt_as->id);	
		return -1;	
	}

	pthread_mutex_lock(&vt_as->afdist->mutex);

	t_afdist_frm* frm = (t_afdist_frm*)vt_as->afdist->head;
	while(frm != NULL){
		//退出码流时把链表里的所有待读取标记和reading标记全部清掉
		frm->as_flag &= ~(vt_as->mask);
		frm->reading_flag &= ~(vt_as->mask);
		frm = frm->next;
	}
	afdist_frm_clean(vt_as->afdist);	//清掉AFDIST里所有读取完成的frame
	vt_as->afdist->init_mask &= ~(vt_as->mask);//清空init状态，标记该码流已经退出
	if(vt_as->afdist->init_mask == 0){
		ret = VIRTUAL_AS_NOT_DONE;//所有虚拟码流都已经exit，返回特定标记请求退出实际码流
	}
	vt_as->mask = 0;

	pthread_mutex_unlock(&vt_as->afdist->mutex);

	return ret;	
}
