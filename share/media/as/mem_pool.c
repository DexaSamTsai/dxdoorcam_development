#include "includes.h"

t_pool *pool_init(u32 element_size, u32 element_init_count, u8 *pbuf)
{
	u32 i;
	u32 malloc_size;

	t_pool_element *head;
	t_pool_element *element;

    t_pool *pnew_pool;

    
    pnew_pool = (t_pool *) pbuf;
    pbuf += sizeof(t_pool);
	
	if (element_size >= sizeof(t_pool_element)) {
		malloc_size = element_size;
	} else {
		malloc_size = sizeof(t_pool_element);
	}
	
    head = (t_pool_element *) pbuf;
    pbuf += sizeof(malloc_size * element_init_count);
	
	pnew_pool->head = head;
	pnew_pool->element_size = malloc_size;
	pnew_pool->element_total_cnt = element_init_count;
	pnew_pool->element_ref_cnt = 0;

	element = head;
	for (i=0; i<(element_init_count - 1); i++) {
		element->next = (t_pool_element *)((u32)element + malloc_size);
		element = element->next;
	}
	element->next = NULL;

	return pnew_pool;
}

void *pool_element_alloc(t_pool *pool)
{
	t_pool_element *element;

	if (pool->head != NULL) {
		element = pool->head;
	} else {
		return NULL;
    }
	
	pool->head = pool->head->next;
	pool->element_ref_cnt += 1;	

	return ((void *)element);
}

void pool_element_free(t_pool *pool, void *element)
{
	((t_pool_element *)element)->next = pool->head;
	pool->head = (t_pool_element *)element;
	pool->element_ref_cnt -= 1;	
}
