#include "includes.h"
#ifndef __UBOOT__
#include <math.h>
#endif


static t_ertos_eventhandler adecevt = NULL;
static u32 adec_started = 0;

#ifdef __LINUX__
extern char * ddr_buffer;
extern char * iram_buffer;
#endif

int libadec_init(t_adec_inpara *inp)
{
	int ret;

	if (adec_started || adecevt){
		return 0;
	}

#if defined(__LINUX__) || defined(__UBOOT__)
#else
	u32 lsclk = clockget_ls();
	if(lsclk%12000000 != 0){
		syslog(LOG_ERR, "Warning:lsclk(%dHz) is not multiple of 12M. audio sample rate will not be correct \n",lsclk);
	}
#endif

	adecevt = ertos_event_create();
	if (adecevt == NULL){
		syslog(LOG_ERR, "adecevt created failed\n");
		return -1;
	}

	if (mpu_event_reg(MPUEVT_ADEC_NOTFULL, adecevt) != 0){
		ertos_event_delete(adecevt);
		adecevt = NULL;
		syslog(LOG_ERR, "adecevt register  failed\n");
		return -1;
	}

#if defined(__LINUX__) || defined(__UBOOT__)
	ret = mpu_cmd(MPUCMD_APLAY_INIT, (u32)0x100CF100| BIT31);
#else
	dc_flush_range((u32)inp,(u32)inp+sizeof(t_adec_inpara));
	ret = mpu_cmd(MPUCMD_APLAY_INIT, (u32)inp | BIT31);
#endif

	if(ret < 0){
#ifndef __LINUX__
		mpu_event_dereg(MPUEVT_ADEC_NOTFULL);
#endif
		ertos_event_delete(adecevt);
		adecevt = NULL;
		syslog(LOG_ERR, "MPUCMD_APLAY_INIT cmd  failed\n");
		return -1;
	}
	return ret;
}


int libadec_start(void)
{
	if(!adec_started){
		mpu_cmd(MPUCMD_APLAY_START,0);
		adec_started = 1;
	}
	return 0;
}

int libadec_stop(void)
{
	if (adec_started){
		mpu_cmd(MPUCMD_APLAY_STOP,0);
		adec_started = 0;
	}

	return 0;
}

int libadec_exit(void)
{
	if (adecevt){
		mpu_cmd(MPUCMD_APLAY_EXIT,0);
#ifndef __LINUX__
		mpu_event_dereg(MPUEVT_ADEC_NOTFULL);
#endif
		ertos_event_delete(adecevt);
		adecevt = NULL;
	}

	return 0;
}


FRAME *libadec_get_frame(u32 timeout)
{
	int ret = 0;
	FRAME *frame = NULL;

	if (!adec_started){
		return NULL;
	}

	ret = mpu_cmd(MPUCMD_APLAY_GETFRM, 0);
	if (ret == 2){
		return NULL;
	}
	if (ret != 0){
		goto FRAME_DONE;
	}

	if (adecevt != 0){
		//no frame buf now, wait empty frm buf ready
		ertos_event_wait(adecevt, timeout);

		ret = mpu_cmd(MPUCMD_APLAY_GETFRM, 0);
		if (ret == 0 || ret == 2){
			return NULL;
		}
	}

FRAME_DONE:
	frame = (FRAME *)ret;

#ifdef __LINUX__
	//physical to virtual
	frame = (FRAME *)(iram_buffer + ret - 0x90000000);
	frame->addr = (u32)(ddr_buffer + frame->addr - 0x90100000);
	if (frame->size1) {
		frame->addr1 = (u32)(ddr_buffer + frame->addr1 - 0x90100000);
	}
#endif

	return frame;
}

#if !defined(__LINUX__) && !defined(__UBOOT__)
#define M_PI 3.14159265358979323846
#define FLOAT_TO_FIX(f, p) ((long)((f) * (1 << (p))))

// Q value for band-pass filters 1.2247=(3/2)^(1/2) gives 4dB suppression @ Fc*2 and Fc/2
static const double Q_FAC=1.2247449; 

static void bp2(int* a, int* b, double fc, double q)
{
	double th = 2 * M_PI * fc;
	double xt = tan(th * q / 2);
	double C= (1 - xt) / (1 + xt);

	a[0]= FLOAT_TO_FIX((1+C)*cos(th), FIXPOINT_A);
	a[1]= FLOAT_TO_FIX(-C, FIXPOINT_A);

	b[0]= FLOAT_TO_FIX((1.0-C)/2, FIXPOINT_B); 
	b[1]= FLOAT_TO_FIX(-1.0050, FIXPOINT_B); 
}

static void eqinit(int sample_rate,t_eq_params *para, t_eq_settings *settings)
{
	int i, j, k;
	double max_gain = 0;

	para->valid_bands=settings->bands;

	while ((settings->bandfreqs[para->valid_bands - 1]) > sample_rate/Q_FAC/2)
		para->valid_bands--;

	for (k=0;k<para->valid_bands;k++)
		bp2(para->a_weights[k], para->b_weights[k], (settings->bandfreqs[k])/(double)sample_rate, Q_FAC);

	for(i=0; i<AF_NCH; i++)
		for(j=0; j<para->valid_bands; j++)
		{
			double gain = pow(10.0, settings->bandgains[j]/20.0);

			para->gain[i][j] = FLOAT_TO_FIX(gain - 1.0, FIXPOINT_G);

			if (gain > max_gain)
			{
				max_gain = gain;
			}
		}

	para->max_volume = (int)(T_EQ_MAX_VOLUME / max_gain);
}

s32 libadec_setEQ(t_eq_settings *settings, int sr)
{
	int ret;	
	t_eq_params s_para;
	t_eq_params *para = (t_eq_params *)(((u32)&s_para)|BIT31);
	para->pending_update = 1;

	eqinit(sr,para,settings);

#if 0
	syslog(LOG_INFO,"EQ para is below\n");
	syslog(LOG_INFO,"para.a_weights[0][0] = %d\n",para->a_weights[0][0]);
	syslog(LOG_INFO,"para.a_weights[0][1] = %d\n",para->a_weights[0][1]);
	syslog(LOG_INFO,"para.a_weights[1][0] = %d\n",para->a_weights[1][0]);
	syslog(LOG_INFO,"para.a_weights[1][1] = %d\n",para->a_weights[1][1]);
	syslog(LOG_INFO,"para.a_weights[2][0] = %d\n",para->a_weights[2][0]);
	syslog(LOG_INFO,"para.a_weights[2][1] = %d\n",para->a_weights[2][1]);
	syslog(LOG_INFO,"para.b_weights[0][0] = %d\n",para->b_weights[0][0]);
	syslog(LOG_INFO,"para.b_weights[0][1] = %d\n",para->b_weights[0][1]);
	syslog(LOG_INFO,"para.b_weights[1][0] = %d\n",para->b_weights[1][0]);
	syslog(LOG_INFO,"para.b_weights[1][1] = %d\n",para->b_weights[1][1]);
	syslog(LOG_INFO,"para.b_weights[2][0] = %d\n",para->b_weights[2][0]);
	syslog(LOG_INFO,"para.b_weights[2][1] = %d\n",para->b_weights[2][1]);
	syslog(LOG_INFO,"para.gain[0][0] = %d\n",para->gain[0][0]);
	syslog(LOG_INFO,"para.gain[0][1] = %d\n",para->gain[0][1]);
	syslog(LOG_INFO,"para.gain[0][2] = %d\n",para->gain[0][2]);
#endif
	ret = mpu_cmd(MPUCMD_APLAY_SETEQ, (u32)para);
	if(ret < 0){
		syslog(LOG_ERR, "libadec_setEQ failed\n");
		return -1;
	}

	return 0;
}
#endif
