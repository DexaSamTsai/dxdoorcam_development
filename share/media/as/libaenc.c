#include "includes.h"


t_ertos_eventhandler arecevt = NULL;
static u32 aenc_started = 0;

typedef struct  _AENC_STREAM
{
    int lastoffset;
    int lasttotlen;
    FRAME *cur_frm;
}AENC_STREAM;

static AENC_STREAM g_aenc_stream;

#ifdef __LINUX__
extern char * ddr_buffer;
extern char * iram_buffer;
#endif

#if defined(__LINUX__) || defined(__UBOOT__)
extern u8  *p_adc_ppbuf;
#else
static  u8 __attribute__  ((aligned(4))) g_adc_ppbuf[AI_PINGPONGBUF_SIZE*2*2];	
#endif


s32 libaenc_init(t_arec_inpara *inp)
{
	int ret;

	if(aenc_started || arecevt){
		return 0;
	}

#if defined(__LINUX__) || defined(__UBOOT__)
#else
	u32 lsclk = clockget_ls();
	if(lsclk%12000000 != 0){
		syslog(LOG_ERR, "Warning:lsclk(%dHz) is not multiple of 12M. audio sample rate will not be correct \n",lsclk);
	}
#endif

	arecevt = ertos_event_create();
	if(arecevt == NULL){
		syslog(LOG_ERR, "arecevt created failed\n");
		return -1;
	}

	if(mpu_event_reg(MPUEVT_AENC_NOTEMPTY, arecevt) != 0){
		ertos_event_delete(arecevt);
		arecevt = NULL;
		syslog(LOG_ERR, "arecevt register  failed\n");
		return -1;
	}

	//when using digital mic, if the adc ppbuffer address is greater than 0x10080000,
	//it cannot record correct data. MPU's base address is often around 0x10080000,
	//So let ARM define the adc ppbufer,then tell BA22 the buffer address
#if defined(__LINUX__) || defined(__UBOOT__)
	mpu_cmd(MPUCMD_ARECORD_SETADCPPBUFADDR, (u32)0x100CF200 | BIT31);

	ret = mpu_cmd(MPUCMD_ARECORD_INIT, (u32)0x100CF000| BIT31);
#else
	mpu_cmd(MPUCMD_ARECORD_SETADCPPBUFADDR, (u32)g_adc_ppbuf);

	dc_flush_range((u32)inp,(u32)inp+sizeof(t_arec_inpara));

	ret = mpu_cmd(MPUCMD_ARECORD_INIT, (u32)inp | BIT31);
#endif

	if (ret < 0) {
#ifndef __LINUX__
		mpu_event_dereg(MPUEVT_AENC_NOTEMPTY);
#endif
		ertos_event_delete(arecevt);
		arecevt = NULL;
		syslog(LOG_ERR, "MPUCMD_ARECORD_INT cmd  failed\n");
		return -1;
	}

	return ret;
}

int libaenc_start(void)
{
	int ret;

	if(!aenc_started){
		ret = mpu_cmd(MPUCMD_ARECORD_START,0);
		if(ret != 0) {
			syslog(LOG_ERR, "MPUCMD_ARECORD_START cmd failed\n");
			return -1;
		}

		aenc_started = 1;

		memset(&g_aenc_stream, 0, sizeof(g_aenc_stream));
	}

	return 0;
}

int libaenc_stop(void)
{
	if(aenc_started){
		mpu_cmd(MPUCMD_ARECORD_STOP,0);
		aenc_started = 0;
	}

	return 0;
}

int libaenc_exit(void)
{
	if(arecevt){
		mpu_cmd(MPUCMD_ARECORD_EXIT,0);
#ifndef __LINUX__
		mpu_event_dereg(MPUEVT_AENC_NOTEMPTY);
#endif
		ertos_event_delete(arecevt);

		arecevt = NULL;
	}

	return 0;
}


FRAME * libaenc_get_frame(u32 timeout)
{
	int ret = 0;
	FRAME *frame = NULL;

	if (!aenc_started){
		return NULL;
	}

	ret = mpu_cmd(MPUCMD_ARECORD_GETFRM, 0);
	if (ret == 2){
		return NULL;
	}
	if (ret != 0){
		goto FRAME_DONE;
	}

	if (arecevt != 0 && timeout != 0){
		//no frame buf now, wait empty frm buf ready
		ertos_event_wait(arecevt, timeout);

		ret = mpu_cmd(MPUCMD_ARECORD_GETFRM, 0);
		if (ret == 0 || ret == 2){
			return NULL;
		}
	}else{
		return NULL;	//return NULL if timeout == 0 and get no frame
	}

FRAME_DONE:
	frame = (FRAME *)ret;

#ifdef __LINUX__
	//physical to virtual
	frame = (FRAME *)(iram_buffer + ret - 0x90000000);
	frame->addr = (u32)(ddr_buffer + frame->addr - 0x90100000);
	if (frame->size1) {
		frame->addr1 = (u32)(ddr_buffer + frame->addr1 - 0x90100000);
	}
#endif

	return frame;
}

#if !defined(__LINUX__) && !defined(__UBOOT__)
int libaenc_set_sr(s32 sr)
{
	if(aenc_started){
		mpu_cmd(MPUCMD_ARECORD_SETSR,sr);
	}

	libacodec_set_sr(sr);

	return 0;
}
#endif

u32 libaenc_get_timestamp(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETREFTM,0);

	return ret;
}

u32 libaenc_get_droppedms(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETDROPPED,0);

	return ret;
}


u32 libaenc_get_totfrmsintostreambuf(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETTOTFRMSINLIST,0);

	return ret;

}

u32 libaenc_get_totfrmsfromstreambuf(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETTOTFRMSOUTLIST,0);

	return ret;

}

u32 libaenc_get_totstreambufsz(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETTOTLISTBUFSIZE,0);

	return ret;

}

u32 libaenc_get_usedstreambufsz(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETUSEDLISTBUFSIZE,0);

	return ret;

}

u32 libaenc_get_dropfrmmode(void)
{
	if(!aenc_started){
		return 0;
	}

	u32 ret = mpu_cmd(MPUCMD_ARECORD_GETDFMODE,0);

	return ret;

}

u32 libaenc_set_dropfrmmode(E_DROPFRM_MODE mode)
{
	if(!aenc_started){
		return 0;
	}

	mpu_cmd(MPUCMD_ARECORD_SETDFMODE,mode);

	return 0;

}

u32 libaenc_set_listfrmdur(u32 dur_ms)
{
	if(!aenc_started){
		return 0;
	}

	mpu_cmd(MPUCMD_ARECORD_SETOLDFRMDUR,dur_ms);

	return 0;

}

u32 libaenc_get_listfrmdur(void)
{
	if(!aenc_started){
		return 0;
	}

	return mpu_cmd(MPUCMD_ARECORD_GETOLDFRMDUR,0);
}

int libaenc_get_data(AENC_DATA *p_data, int maxlen, u32 timeout)
{
    int ret;
    if((maxlen == 0) || (p_data == NULL))       return 0;

    memset(p_data, 0, sizeof(AENC_DATA));
    if(g_aenc_stream.cur_frm)
    {
        if(g_aenc_stream.lastoffset && (g_aenc_stream.lastoffset >= g_aenc_stream.lasttotlen))
        {
		    g_aenc_stream.lastoffset = 0;
            g_aenc_stream.cur_frm = NULL;
        }
    }

    if(g_aenc_stream.cur_frm == NULL)
    {
        g_aenc_stream.cur_frm = libaenc_get_frame(timeout);
        if(g_aenc_stream.cur_frm == NULL)
            return 0;

        g_aenc_stream.lastoffset = 0;
        p_data->frameType |= ADATA_FLAG_FIRSTPKT;
        g_aenc_stream.lasttotlen = g_aenc_stream.cur_frm ->size + (g_aenc_stream.cur_frm->addr1 ? g_aenc_stream.cur_frm ->size1 : 0) ;
    }


    p_data->frameLen = g_aenc_stream.lasttotlen;
    if(g_aenc_stream.lastoffset >= g_aenc_stream.cur_frm->size)
    {
        p_data->pAddr = (char *)(g_aenc_stream.cur_frm->addr1 + g_aenc_stream.lastoffset - g_aenc_stream.cur_frm->size) ;
        ret = g_aenc_stream.lasttotlen - g_aenc_stream.lastoffset;
    }
    else
    {
        p_data->pAddr = (char *)(g_aenc_stream.cur_frm->addr + g_aenc_stream.lastoffset);
        ret = g_aenc_stream.cur_frm->size - g_aenc_stream.lastoffset;
    }

    if(ret > maxlen)    ret = maxlen;

    g_aenc_stream.lastoffset += ret;

    if(g_aenc_stream.lastoffset >= g_aenc_stream.lasttotlen)
    {
        p_data->frameType |= ADATA_FLAG_LASTPKT;
        p_data->pRel = (u8*)g_aenc_stream.cur_frm;
    }

    p_data->thisLen = ret;
    p_data->timeStamp = g_aenc_stream.cur_frm->ts;

    return ret;
}

int libaenc_remove_data(u8 *p_rel)
{
    if(p_rel)
    {
        libaenc_remove_frame((FRAME*)p_rel);
    }
}
