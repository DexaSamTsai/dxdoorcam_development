/*
 ============================================================================
 Name        : dxOSTestOVRtos.c
 Author      : 
 Version     :
 Copyright   : Dexatek Technology Ltd
 Description : DxBSP Unit Test
 ============================================================================
 */
#include "includes.h"
#include "dxOS.h"
#include "dxBSP.h"
#include "dxMD5.h"

#define JOIN_RETURN_SUCCCESS		"SUCESS"
#define JOIN_RETURN_FAILED			"FAILED"

#define	THREAD_STACK_SIZE			(5*1024)
#define	THREAD_PRIORITY				DXTASK_PRIORITY_NORMAL
#define MAX_TEST_STRING_SIZE		128

#define GPIO_TEST					0
#define     GPIO_A                      (3)
#define     GPIO_B                      (4)
#define UART_TEST					0
#define     UART_PORT_TO_TEST           DXUART_PORT2        //README!!! - This is a loopback test, make sure this defined UART port RX and TX is shorted.
#define     UART_TEST1_TEXT             "A" //"Uart Test1 Text"
#define     UART_TEST2_TEXT_PART1       "Uart Test2"
#define     UART_TEST2_TEXT_PART2       " Part2"
#define     UART_TEST2_TEXT_ALL         "Uart Test2 Part2" //Make sure its UART_TEST2_TEXT_PART1 + UART_TEST2_TEXT_PART2
#define     UART_TEST3_TEXT_PART1       "Uart Test3"
#define     UART_TEST3_TEXT_PART2_DUMMY " Part2 Dummy"
#define     UART_TEST3_TEXT_ALL         "Uart Test3 Part2 Dummy" //Make sure its UART_TEST3_TEXT_PART1 + UART_TEST3_TEXT_PART2_DUMMY
#define APPDATA_TEST					0
#define MAINIMAGE_TEST					1
#define SUBIMAGE_TEST					0
#define FWIMAGE_TEST					0
#define MTD_TEST					    0

/*
 * Global Variable
 */

//README FIRST!!!
//GPIO_A and GPIO_B is a lookback test!,
//Make SURE these 2 GPIO are shorted together
static dxGPIO_t TestGPIOConfig[] =
{
    {
        NULL,
        GPIO_A,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        GPIO_B,
        DXPIN_INPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    
    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};

typedef struct
{
    uint32_t         nTotalReceiveBytes;    // File size in bytes
    uint32_t         nDownloadBytes;        // Current received bytes
    uint8_t         *pszFileMD5Sum;         // The MD5 checksum result of file, in hexadecimal string format.
                                            // For example, 0x41 => "41" (character " is not included)

    MD5_CTX         *pmd5cx;                // MD5 checksum context
    uint8_t         *pmd5sum;               // Current MD5 checksum data

    uint8_t         nSubImgBlockID;         // Reserved for expandability

    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
} dxSubImgInfoAccessCtrl_t;


typedef struct
{
    uint16_t        Size;                   // Size including sizeof(dxAppInfo_t) + all data behind pData[0]
    uint16_t        CRC16;                  // Using CRC16 with polynomial 0x8005 to calculate data from pData[0]

    // Example user data. Declare your data here.
    uint8_t         pData[0];               // NOTE: If you have dynamic length data, you can put your data to pData[0].
                                            //       pData[0] should be the last element of structure dxAppInfo_t. Or
                                            //       a compiler error will occur.
                                            //
                                            //       Use offsetof(dxAppInfo_t, pData) to get the address of pData[0].
                                            //       The value of offsetof(dxAppInfo_t, pData) should be 4
} dxAppInfo_t;                              // sizeof(dxAppInfo_t) should be 4 bytes

dxUART_t* UartHdl = NULL;


/*
 * Function Definition
 */
static void* UartThreadTest_TaskReceive( void* arg );
static void* UartThreadTest_TaskSend( void* arg );

/*********************************************************
 *             Color printing definitions
 *
 * If you want to print RED:
 * G_SYS_DBG_LOG_V(RED("- RED Color -\r\n"));
 * G_SYS_DBG_LOG_V1(RED("- RED Color - %d\r\n"), color);
 *********************************************************/
#define ESC_COLOR_NORMAL    "\033[m"
#define ESC_COLOR_BLACK     "\033[30m"
#define ESC_COLOR_RED       "\033[31m"
#define ESC_COLOR_GREEN     "\033[32m"
#define ESC_COLOR_YELLOW    "\033[33m"
#define ESC_COLOR_BLUE      "\033[34m"
#define ESC_COLOR_MAGENTA   "\033[35m"
#define ESC_COLOR_CYAN      "\033[36m"
#define ESC_COLOR_WHITE     "\033[37m"

#define NORMAL( x )         ESC_COLOR_NORMAL x
#define BLACK( x )          ESC_COLOR_BLACK  x ESC_COLOR_NORMAL
#define RED( x )            ESC_COLOR_RED    x ESC_COLOR_NORMAL
#define GREEN( x )          ESC_COLOR_GREEN  x ESC_COLOR_NORMAL
#define BLUE( x )           ESC_COLOR_BLUE   x ESC_COLOR_NORMAL
#define YELLOW( x )         ESC_COLOR_YELLOW   x ESC_COLOR_NORMAL

int dxBSPTest_main(void) 
{

    printf("DxBSPTest_main start\n");

#if FWIMAGE_TEST

    FILE *f;
    
    f = fopen("/mnt/dxfw.bin", "rb");
    if (f == NULL){
        printf("FW file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t FWSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Image Size=%zu\n",FWSize);

    uint8_t * dxFWBuf=malloc(FWSize);

    fread(dxFWBuf, FWSize, 1, f);

    int ret = dxFWParserWrite(dxFWBuf);

    printf("dxFWParserWrite=%d\n",ret);

    if (!ret)
        ret=dxFWIpcamBootSwitch();
    
    if (ret >= 0)
        printf("Boot Switch to %d\n",ret);

    // For testing, so switch again!
    ret=dxFWIpcamBootSwitch();
    
    if (ret >= 0)
        printf("Boot Switch to %d\n",ret);

    free(dxFWBuf);
    fclose(f);

#endif //FWIMAGE_TEST

#if MTD_TEST
    FILE *f = fopen("/mnt/vmlinuz.img", "rb");
    if (f == NULL){
        printf("Kernel file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t fwKernelSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Kenrel Image Size=%zu\n",fwKernelSize);

    dxFWStorage_t * dxFWKernel=malloc(sizeof(dxFWStorage_t)+fwKernelSize);
    dxFWKernel->Type=DXSTORAGETYPE_DXMTD;
    dxFWKernel->Size=fwKernelSize;

    memset(dxFWKernel->Data,0,dxFWKernel->Size);
    fread(dxFWKernel->Data, dxFWKernel->Size, 1, f);

    close(f);

    unsigned char md5str[32+1];
    dxMD5(dxFWKernel->Data, dxFWKernel->Size,dxFWKernel->MD5);
    dxMD5_convert_hex(dxFWKernel->MD5,md5str);
    printf ("File_MD5=%s Size=%zu\n", md5str,dxFWKernel->Size);
    
    sprintf(dxFWKernel->Location,"/dev/mtd3");
    dxFWStoreMTD_Init(dxFWKernel);
    dxFWStoreMTD_Erase(dxFWKernel);
    dxFWStoreMTD_Write(dxFWKernel);
    dxFWStoreMTD_MD5(dxFWKernel);
    free(dxFWKernel);


    f = fopen("/mnt/rootfs.bin", "rb");
    if (f == NULL){
        printf("Rootfs file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t fwRootfsSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Rootfs Image Size=%zu\n",fwRootfsSize);

    dxFWStorage_t * dxFWRootfs=malloc(sizeof(dxFWStorage_t)+fwRootfsSize);
    dxFWRootfs->Type=DXSTORAGETYPE_DXMTD;
    dxFWRootfs->Size=fwRootfsSize;

    memset(dxFWRootfs->Data,0,fwRootfsSize);
    fread(dxFWRootfs->Data, fwRootfsSize, 1, f);

    close(f);

    dxMD5(dxFWRootfs->Data, dxFWRootfs->Size,dxFWRootfs->MD5);
    dxMD5_convert_hex(dxFWRootfs->MD5,md5str);
    printf ("File_MD5=%s Size=%zu\n", md5str,dxFWRootfs->Size);

    sprintf(dxFWRootfs->Location,"/dev/mtd4");
    dxFWStoreMTD_Init(dxFWRootfs);
    dxFWStoreMTD_Erase(dxFWRootfs);
    dxFWStoreMTD_Write(dxFWRootfs);
    dxFWStoreMTD_MD5(dxFWRootfs);
    free(dxFWRootfs);

#endif //MTD_TEST

#if MAINIMAGE_TEST
    printf("MAINIMAGE_TEST Start...!\n");

    #define BIN_TEMPADDR (void *)(MEMBASE_DDR + 0x800000)
    partition_Get_Backup_FW(BIN_TEMPADDR);

    unsigned char mdstr[32 + 1];

    dxMainImage_Init();
    dxMainImage_Start(mdstr,0);
    dxMainImage_Write(BIN_TEMPADDR, 0);
    dxMainImage_Stop();

    printf("MAINIMAGE_TEST End...!\n");

#endif //MAINIMAGE_TEST

#if SUBIMAGE_TEST

    unsigned char md5[64];
    unsigned char mdstr[32 + 1];
    FILE *f = fopen("/mnt/ble.fw", "rb");

    fseek(f, 0, SEEK_END);
    uint32_t fwbleSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW ble Image Size=%zu\n",fwbleSize);

    uint8_t * fwReadBuffer = malloc(fwbleSize);
    memset(fwReadBuffer,0xff,fwbleSize);
    fread(fwReadBuffer, fwbleSize, 1, f);
    fclose(f);

    MD5_CTX ctx;
    dxMD5_Starts(&ctx);
    dxMD5_Update(&ctx,fwReadBuffer, fwbleSize);
    dxMD5_Finish(md5,&ctx);
    dxMD5_convert_hex(md5,mdstr);
    printf ("File_MD5=%s\n", mdstr);

    dxSubImgInfoAccessCtrl_t *dxSubImgInfoAccessCtrl = dxSubImage_Init(DXSUBIMGBLOCK_ONE);
    dxSubImage_Start(dxSubImgInfoAccessCtrl,mdstr,fwbleSize);

    int i;
    int line_size = 0x1000;

    dxMD5_Starts(&ctx);

    uint32_t address=0;
    for (i=0;i<(fwbleSize/line_size);i++){
        address=i*line_size;
        //printf("address=%d,line_size=%d\n",address,line_size);
        dxMD5_Update(&ctx,fwReadBuffer+address, line_size);
        dxSubImage_Write(dxSubImgInfoAccessCtrl,fwReadBuffer+address, line_size);
    }

    address=i*line_size;
    line_size=fwbleSize-(i*line_size);
    //printf("address=%d,line_size=%d\n",address,line_size);

    dxMD5_Update(&ctx,fwReadBuffer+address, line_size);
    dxSubImage_Write(dxSubImgInfoAccessCtrl,fwReadBuffer+address, line_size);

    dxMD5_Finish(md5,&ctx);
    dxMD5_convert_hex(md5,mdstr);
    printf ("READ_MD5=%s\n", mdstr);

    printf("Image Size=%d\n",dxSubImage_ReadImageSize(dxSubImgInfoAccessCtrl));

    memset(fwReadBuffer,0xff,fwbleSize);
    uint32_t readsize=0;
    dxSubImage_Read(dxSubImgInfoAccessCtrl,
                    0,
                    fwbleSize,
                    fwReadBuffer,
                    &readsize);

    printf("readsize=%zu\n",readsize);

    dxMD5_Starts(&ctx);
    dxMD5_Update(&ctx,fwReadBuffer, readsize);
    dxMD5_Finish(md5,&ctx);
    dxMD5_convert_hex(md5,mdstr);
    printf ("File_MD5=%s\n", mdstr);

    dxSubImage_Uninit(dxSubImgInfoAccessCtrl);
    dxSubImage_Stop();

    free(fwReadBuffer);

#endif //SUBIMAGE_TEST

#if APPDATA_TEST
    printf("---------------------------\n");

    int ret,i,len=16;
    uint8_t Data[]={0x01,0x01,0x01,0x01,0x04,0x05,0x06,0x07,0x08,0x09,0x00,0x01,0x02,0x03,0x04,0x09};
  
    dxAppInfo_t *apinfotest = (dxAppInfo_t*)malloc(sizeof(dxAppInfo_t) + len);
    //apinfotest->Size=len;

    //memcpy(apinfotest->pData, Data, len);

    printf("[APPDATA_TEST] data=");
    for (i=0;i<len;i++)
        printf("%X:",Data[i]);
    printf("\n");

    printf("[APPDATA_TEST] dxAppData_Init START\n");

    dxAppDataHandle_t phandle = NULL;
    printf("[APPDATA_TEST] DXBLOCKID=%d\n",DXBLOCKID_SYSINFO);
    phandle = dxAppData_Init(DXBLOCKID_SYSINFO);

    printf("[APPDATA_TEST] dxAppData_Write START\n");
    ret=dxAppData_Write(phandle,Data,len);
    printf("[APPDATA_TEST] ret=%d\n",ret);

    printf("[APPDATA_TEST] dxAppData_ReadDataSize START\n");

    ret=dxAppData_ReadDataSize(phandle);

    printf("[APPDATA_TEST] dxAppData_ReadDataSize Size=%d\n",ret);

    printf("[APPDATA_TEST] dxAppData_Read START\n");

    printf("[APPDATA_TEST] clean data=");
    for (i=0;i<len;i++){
        apinfotest->pData[i]=0;
        printf("%X:",apinfotest->pData[i]);
    }
    printf("\n");

    ret=dxAppData_Read(phandle,apinfotest,&len);

    printf("[APPDATA_TEST] ret=%d\n",ret);

    printf("[APPDATA_TEST] len=%d\n",len);

    printf("[APPDATA_TEST] size=%d data=",apinfotest->Size);

    for (i=0;i<apinfotest->Size;i++)
        printf("%X:",apinfotest->pData[i]);
    printf("\n");
#endif //APPDATA_TEST

#if GPIO_TEST

	printf("---------------------------\n");

    printf("[GPIO_TEST] dxGPIO_Init START\n");
    if (dxGPIO_Init(TestGPIOConfig) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Init RESULT = FAILED!\n"));
        goto TestExit;
    }
    
    printf("[GPIO_TEST] dxGPIO_Write and dxGPIO_read START\n");
    if (dxGPIO_Write(GPIO_A, DXPIN_HIGH) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Write GPIO_A FAILED!\n"));
        goto TestExit;
    }
 
    sleep(1);
    
    if (dxGPIO_Read(GPIO_B) != 1)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Read GPIO_B FAILED!\n"));
        goto TestExit;
    }
    
    if (dxGPIO_Write(GPIO_A, DXPIN_LOW) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Write GPIO_A FAILED!\n"));
        goto TestExit;
    }
    
    sleep(1);
    
    if (dxGPIO_Read(GPIO_B) != 0)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Read GPIO_B FAILED!\n"));
        goto TestExit;
    }
    
    printf(GREEN("[GPIO_TEST] dxGPIO_Write and dxGPIO_read PASS\n"));
    
    
    printf("[GPIO_TEST] dxGPIO_Set_Direction START\n");

    if (dxGPIO_Set_Direction(GPIO_A, DXPIN_INPUT) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Set_Direction GPIO_A FAILED!\n"));
        goto TestExit;
    }
    
    if (dxGPIO_Set_Direction(GPIO_B, DXPIN_OUTPUT) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Set_Direction GPIO_B FAILED!\n"));
        goto TestExit;
    }
    
    if (dxGPIO_Write(GPIO_B, DXPIN_HIGH) != DXBSP_SUCCESS)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Write GPIO_B FAILED!\n"));
        goto TestExit;
    }
    
    sleep(1);
    
    if (dxGPIO_Read(GPIO_A) != 1)
    {
        printf(RED("[GPIO_TEST] dxGPIO_Read GPIO_A FAILED!\n"));
        goto TestExit;
    }

    printf(GREEN("[GPIO_TEST] dxGPIO_Set_Direction PASS\n"));

    printf("---------------------------\n");
    
#endif
    
    
#if UART_TEST
    
    printf("[UART_TEST] dxUART_Open START\n");
    UartHdl = dxUART_Open(DXUART_PORT2, 115200, DXUART_PARITY_NONE, 8, 1, DXUART_FLOWCONTROL_NONE, NULL);
    if (UartHdl == NULL)
    {
        printf(RED("[UART_TEST] dxUART_Open FAILED!\n"));
        goto TestExit;
    }
    printf(GREEN("[UART_TEST] dxUART_Open PASS\n"));

    
    printf("[UART_TEST] TX/RX LoopBack Test START\n");
    dxTaskHandle_t UartThreadTest_Task1Hdl;
    dxOS_RET_CODE UartTaskResult = dxTaskCreate(UartThreadTest_TaskReceive,
                                                 "UartThreadTest_TaskReceive",
                                                 THREAD_STACK_SIZE,
                                                 UartHdl,
                                                 THREAD_PRIORITY,
                                                 &UartThreadTest_Task1Hdl);
    if (UartTaskResult != DXOS_SUCCESS)
    {
        printf(RED("[MUTEX_TEST]  TX/RX LoopBack Test FAILED (UartThreadTest_TaskReceive Task Create Fail)\n"));
    }
    
    dxTaskHandle_t UartThreadTest_Task2Hdl;
    dxOS_RET_CODE UartTask2Result = dxTaskCreate(UartThreadTest_TaskSend,
                                                "UartThreadTest_TaskSend",
                                                THREAD_STACK_SIZE,
                                                UartHdl,
                                                THREAD_PRIORITY,
                                                &UartThreadTest_Task2Hdl);
    if (UartTask2Result != DXOS_SUCCESS)
    {
        printf(RED("[MUTEX_TEST]  TX/RX LoopBack Test FAILED (UartThreadTest_TaskSend Task Create Fail)\n"));
    }
    
    //Just wait until UartThreadTest_TaskReceive terminated
    void* RetVal;
    pThread_info_t* pUartThread2Info = (pThread_info_t*)UartThreadTest_Task1Hdl;
    pthread_join(pUartThread2Info->threadId, &RetVal);
    
#endif
    
    

TestExit:

	return EXIT_SUCCESS;
}


static void* UartThreadTest_TaskSend( void* arg )
{
    printf("[UART_TEST] dxTaskCreate UartThreadTest_TaskSend Says Hello!\n");
    uint32_t AllSuccess = 1;
    
    if (arg)
    {
    
        dxUART_t*	ArgUartHdl = (dxUART_t*)arg;
    
        sleep(1);
        
        printf("[UART_TEST] Test1 dxUART_Send_Blocked START\n");
        int TotalByteSent = dxUART_Send_Blocked(ArgUartHdl, (uint8_t*)UART_TEST1_TEXT, strlen(UART_TEST1_TEXT));
        if (TotalByteSent != strlen(UART_TEST1_TEXT))
        {
            printf(RED("[UART_TEST] dxUART_Send_Blocked FAILED (Expected %dByte, ActualSentByte %dByte\n"), strlen(UART_TEST1_TEXT), TotalByteSent);
            AllSuccess = 0;
            goto TaskSendExit;
        }
        else
        {
            printf(GREEN("[UART_TEST] Test1 dxUART_Send_Blocked DONE\n"));
        }
        
        sleep(1);

        printf("[UART_TEST] Test2 Part1 dxUART_Send_Blocked START\n");
        TotalByteSent = dxUART_Send_Blocked(ArgUartHdl, (uint8_t*)UART_TEST2_TEXT_PART1, strlen(UART_TEST2_TEXT_PART1));
        if (TotalByteSent != strlen(UART_TEST2_TEXT_PART1))
        {
            printf(RED("[UART_TEST] dxUART_Send_Blocked FAILED (Expected %dByte, ActualSentByte %dByte\n"), strlen(UART_TEST2_TEXT_PART1), TotalByteSent);
            AllSuccess = 0;
            goto TaskSendExit;
        }
        else
        {
            printf(GREEN("[UART_TEST] Test2 Part1 dxUART_Send_Blocked DONE\n"));
        }
        
        
        sleep(1);
        printf("[UART_TEST] Test2 Part2 dxUART_Send_Blocked START\n");
        TotalByteSent = dxUART_Send_Blocked(ArgUartHdl, (uint8_t*)UART_TEST2_TEXT_PART2, strlen(UART_TEST2_TEXT_PART2));
        if (TotalByteSent != strlen(UART_TEST2_TEXT_PART2))
        {
            printf(RED("[UART_TEST] Test2 Part2 dxUART_Send_Blocked FAILED (Expected %dByte, ActualSentByte %dByte\n"), strlen(UART_TEST2_TEXT_PART2), TotalByteSent);
            AllSuccess = 0;
            goto TaskSendExit;
        }
        else
        {
            printf(GREEN("[UART_TEST] Test2 Part2 dxUART_Send_Blocked DONE\n"));
        }
        
        
        sleep(1);
        
        printf("[UART_TEST] Test3 Part1 dxUART_Send_Blocked START\n");
        TotalByteSent = dxUART_Send_Blocked(ArgUartHdl, (uint8_t*)UART_TEST3_TEXT_PART1, strlen(UART_TEST3_TEXT_PART1));
        if (TotalByteSent != strlen(UART_TEST3_TEXT_PART1))
        {
            printf(RED("[UART_TEST] dxUART_Send_Blocked FAILED (Expected %dByte, ActualSentByte %dByte\n"), strlen(UART_TEST3_TEXT_PART1), TotalByteSent);
            AllSuccess = 0;
            goto TaskSendExit;
        }
        else
        {
            printf(GREEN("[UART_TEST] Test3 Part1 dxUART_Send_Blocked DONE (will not send Part2)\n"));
        }
        
        
    }
    else
    {
        AllSuccess = 0;
    }
    
TaskSendExit:
    
    if (AllSuccess)
        return JOIN_RETURN_SUCCCESS;
    else
        return JOIN_RETURN_FAILED;
}


static void* UartThreadTest_TaskReceive( void* arg )
{
    printf("[UART_TEST] dxTaskCreate UartThreadTest_TaskReceive Says Hello!\n");
    uint32_t AllSuccess = 1;
    
    if (arg)
    {
    
        uint8_t Buff[256];
        dxUART_t*	ArgUartHdl = (dxUART_t*)arg;
        
        printf("[UART_TEST] Test1 dxUART_Receive_Blocked START\n");
        memset(Buff, 0, sizeof(Buff));
        int ByteReceive = dxUART_Receive_Blocked(ArgUartHdl, Buff, strlen(UART_TEST1_TEXT), 2000);
        
        if (ByteReceive > 0)
        {
            if (strcmp(UART_TEST1_TEXT, (char*)Buff) == 0)
            {
                printf(GREEN("[UART_TEST] Test1 dxUART_Receive_Blocked PASS\n"));
            }
            else
            {
                AllSuccess = 0;
                printf(RED("[UART_TEST] Test1 dxUART_Receive_Blocked FAILED (Received Data Not Matched)\n"));
                goto TaskReceiveExit;
            }
        }
        else if (ByteReceive == DXUART_TIMEOUT)
        {
            AllSuccess = 0;
            printf(RED("[UART_TEST] Test1 dxUART_Receive_Blocked FAILED (TimeOut!)\n"));
            goto TaskReceiveExit;
        }
        else
        {
            AllSuccess = 0;
            printf(RED("[UART_TEST] Test1 dxUART_Receive_Blocked FAILED (Error!)\n"));
            goto TaskReceiveExit;
        }
         
        
        printf("[UART_TEST] Test2 dxUART_Receive_Blocked START\n");
        memset(Buff, 0, sizeof(Buff));
        ByteReceive = dxUART_Receive_Blocked(ArgUartHdl, Buff, strlen(UART_TEST2_TEXT_ALL), 3000);
        
        if (ByteReceive > 0)
        {
            if (strcmp(UART_TEST2_TEXT_ALL, (char*)Buff) == 0)
            {
                printf(GREEN("[UART_TEST] Test2 dxUART_Receive_Blocked PASS\n"));
            }
            else
            {
                AllSuccess = 0;
                printf(RED("[UART_TEST] Test2 dxUART_Receive_Blocked FAILED (Received Data Not Matched)\n"));
                goto TaskReceiveExit;
            }
        }
        else if (ByteReceive == DXUART_TIMEOUT)
        {
            AllSuccess = 0;
            printf(RED("[UART_TEST] Test2 dxUART_Receive_Blocked FAILED (TimeOut!)\n"));
            goto TaskReceiveExit;
        }
        else
        {
            AllSuccess = 0;
            printf(RED("[UART_TEST] Test2 dxUART_Receive_Blocked FAILED (Error!)\n"));
            goto TaskReceiveExit;
        }

        printf("[UART_TEST] Test3 dxUART_Receive_Blocked START (Expect TimeOut)\n");
        memset(Buff, 0, sizeof(Buff));
        ByteReceive = dxUART_Receive_Blocked(ArgUartHdl, Buff, strlen(UART_TEST3_TEXT_ALL), 3000);
        
        if (ByteReceive == DXUART_TIMEOUT)
        {
            printf(GREEN("[UART_TEST] Test3 dxUART_Receive_Blocked PASS (Expected TimeOut Received)\n"));
            
        }
        else
        {
            AllSuccess = 0;
            printf(RED("[UART_TEST] Test3 dxUART_Receive_Blocked FAILED (We expect TimeOut, TotalByteReceived=%d)\n"), ByteReceive);
            goto TaskReceiveExit;
        }
        
    }
    else
    {
        AllSuccess = 0;
    }
    
TaskReceiveExit:
    
    if (AllSuccess)
        return JOIN_RETURN_SUCCCESS;
    else
        return JOIN_RETURN_FAILED;
}


