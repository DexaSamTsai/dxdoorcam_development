/*
 ============================================================================
 Name        : dxOSTestLinux.c
 Author      : Aaron Lee
 Version     :
 Copyright   : Dexatek Technology Ltd
 Description : DxOS Unit Test
 ============================================================================
 */

#include "dxOS.h"
#include "dxBSP.h"
#include "dxMD5.h"
#include "dxFW.h"

/*
 * Global Variable
 */

/*
 * Function Definition
 */

#define CREATE_FW       1
#define PARSER_FW       0
#define TEST_MTD_FW     0
#define TEST_MTD_RELOC  0

#define IPCAM_KERNEL_IMAGE "/mnt/vmlinuz.img"
#define IPCAM_ROOTFS_IMAGE "/mnt/rootfs.bin"
#define IPCAM_MAIN_IMAGE "/mnt/dxfw.bin"

#define IPCAM_KERNEL_MTD "/dev/mtd3"
#define IPCAM_ROOTFS_MTD "/dev/mtd4"

int main(void) {

#if TEST_MTD_RELOC
    int part;
    
    for (part=0;part<8;part++){
        sprintf(dxFWStorage->Location,"/dev/mtd%d",part);
        dxFWStoreMTD_Init(dxFWStorage);
    }
#endif //TEST_MTD_RELOC

    FILE *f;
    dxFW_t * dxFW = NULL;
    unsigned char md5str[32+1];

#if CREATE_FW
    dxFW = malloc(sizeof(dxFW_t));
    dxFW->Tag=DX_FWIMAGE_MARK;
    dxFW->nTotoalSize=sizeof(dxFW_t);
    dxFW->nPackageCount=0;

    dxFWPackage_t * dxFWPkgKernel = malloc(sizeof(dxFWPackage_t));

    dxFWPackage_t * dxFWPkgRootfs = malloc(sizeof(dxFWPackage_t));

    dxFWStorage_t * dxFWKernel=NULL;
    dxFWStorage_t * dxFWRootfs=NULL;
    uint32_t fwKernelSize;
    uint32_t fwRootfsSize;

    f = fopen(IPCAM_KERNEL_IMAGE, "rb");
    if (f != NULL){
        fseek(f, 0, SEEK_END);
        fwKernelSize = ftell(f);
        fseek(f, 0, SEEK_SET);  //same as rewind(f);

        dxFWKernel = malloc(sizeof(dxFWStorage_t)+fwKernelSize);

        dxFWKernel->Type = DXSTORAGETYPE_DXMTD;
        sprintf(dxFWKernel->Location,IPCAM_KERNEL_MTD);
        dxFWKernel->Size = fwKernelSize;
  
        fread(dxFWKernel->Data, dxFWKernel->Size, 1, f);
        fclose(f);

        dxMD5(dxFWKernel->Data, dxFWKernel->Size,dxFWKernel->MD5);
        dxMD5_convert_hex(dxFWKernel->MD5,md5str);

        dxFWPkgKernel->No = 0;
        dxFWPkgKernel->Type = DXFWTYPE_IMAGE;
        dxFWPkgKernel->Storage = dxFWKernel;
   
        dxFW->nPackageCount = dxFW->nPackageCount + 1 ;
        dxFW->nTotoalSize = dxFW->nTotoalSize + sizeof(dxFWPackage_t) - sizeof(dxFWStorage_t *) + 
                                                sizeof(dxFWStorage_t) + fwKernelSize;

        printf("FW: Tag=%llx, Size=%ld, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount);
        printf("PKG: No=%d, Type=%d\n", dxFWPkgKernel->No, dxFWPkgKernel->Type);
        printf("PKG->STORE: Type=%d, Location=%s, Size=%ld\n", 
                dxFWPkgKernel->Storage->Type, dxFWPkgKernel->Storage->Location, dxFWPkgKernel->Storage->Size);
        printf("STORE: MD5=%s\n", md5str);
    }

    f = fopen(IPCAM_ROOTFS_IMAGE, "rb");
    if (f != NULL){
        fseek(f, 0, SEEK_END);
        fwRootfsSize = ftell(f);
        fseek(f, 0, SEEK_SET);  //same as rewind(f);

        dxFWRootfs = malloc(sizeof(dxFWStorage_t)+fwRootfsSize);

        dxFWRootfs->Type = DXSTORAGETYPE_DXMTD;
        sprintf(dxFWRootfs->Location,IPCAM_ROOTFS_MTD);
        dxFWRootfs->Size = fwRootfsSize;

        fread(dxFWRootfs->Data, dxFWRootfs->Size, 1, f);
        fclose(f);

        dxMD5(dxFWRootfs->Data, dxFWRootfs->Size,dxFWRootfs->MD5);
        dxMD5_convert_hex(dxFWRootfs->MD5,md5str);

        dxFWPkgRootfs->No = dxFW->nPackageCount;
        dxFWPkgRootfs->Type = DXFWTYPE_IMAGE;
        dxFWPkgRootfs->Storage = dxFWRootfs;

        dxFW->nPackageCount = dxFW->nPackageCount + 1 ;
        dxFW->nTotoalSize = dxFW->nTotoalSize + sizeof(dxFWPackage_t) - sizeof(dxFWStorage_t *) + 
                                                sizeof(dxFWStorage_t) + fwRootfsSize;

        printf("FW: Tag=%llx, Size=%ld, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount);
        printf("PKG: No=%d, Type=%d\n", dxFWPkgRootfs->No, dxFWPkgRootfs->Type);
        printf("PKG->STORE: Type=%d, Location=%s, Size=%ld\n", 
                dxFWPkgRootfs->Storage->Type, dxFWPkgRootfs->Storage->Location, dxFWPkgRootfs->Storage->Size);
        printf("STORE: MD5=%s\n", md5str);
    }

    
    f = fopen(IPCAM_MAIN_IMAGE, "wb");
    if (f != NULL){

        fwrite(dxFW,sizeof(dxFW_t),1,f);

        if (dxFW->nPackageCount == 2) { 
            fwrite(dxFWPkgKernel,sizeof(dxFWPackage_t)-sizeof(dxFWStorage_t *),1,f);
            fwrite(dxFWKernel,sizeof(dxFWStorage_t) + fwKernelSize,1,f);
        }

        fwrite(dxFWPkgRootfs,sizeof(dxFWPackage_t)-sizeof(dxFWStorage_t *),1,f);
        fwrite(dxFWRootfs,sizeof(dxFWStorage_t) + fwRootfsSize,1,f);

        fclose(f);
        printf("Create dxMainImage = %s\n",IPCAM_MAIN_IMAGE);
    }

    if (dxFWKernel != NULL)
        free(dxFWKernel);

    if (dxFWRootfs != NULL)
        free(dxFWRootfs);

    free(dxFW);
    if (dxFWPkgKernel != NULL)
        free(dxFWPkgKernel);

    if (dxFWPkgRootfs != NULL)
        free(dxFWPkgRootfs);

#endif //CREATE_FW

#if PARSER_FW
    f = fopen(IPCAM_MAIN_IMAGE, "rb");
    if (f == NULL){
        printf("FW file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t FWSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Image Size=%ld\n",FWSize);

    uint8_t * dxFWBuf=malloc(FWSize);

    fread(dxFWBuf, FWSize, 1, f);

    uint32_t dxFWOffset=0;

    dxFW = malloc(sizeof(dxFW_t));
    memcpy(dxFW,dxFWBuf,sizeof(dxFW_t));
    printf("FW: Tag=%llx, Size=%ld, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount);

    dxFWOffset+=sizeof(dxFW_t);
    
    dxFWPackage_t * dxFWPkg;
    dxFWStorage_t * dxFWStor;
    uint8_t * dxFWData;
    unsigned char md5[16];
    int p;

    for (p=0;p<dxFW->nPackageCount;p++){
        dxFWPkg = malloc(sizeof(dxFWPackage_t));
        memcpy(dxFWPkg,dxFWBuf+dxFWOffset,sizeof(dxFWPackage_t));
        printf("PKG: No=%d, Type=%d\n", dxFWPkg->No, dxFWPkg->Type);

        dxFWOffset+=sizeof(dxFWPackage_t);

        dxFWStor = malloc(sizeof(dxFWStorage_t));
        memcpy(dxFWStor,dxFWBuf+dxFWOffset,sizeof(dxFWStorage_t));
        printf("STORE: Type=%d, Location=%s, Size=%ld\n", 
                dxFWStor->Type, dxFWStor->Location, dxFWStor->Size);

        dxMD5_convert_hex(dxFWStor->MD5,md5str);
        printf("STORE: MD5=%s\n", md5str);

        dxFWOffset+=sizeof(dxFWStorage_t);

        dxFWData = malloc(dxFWStor->Size);
        memcpy(dxFWData,dxFWBuf+dxFWOffset,dxFWStor->Size);
        dxMD5(dxFWData,dxFWStor->Size,md5);
        dxMD5_convert_hex(md5,md5str);
        printf("DATA: MD5=%s\n", md5str);

        dxFWOffset+=dxFWStor->Size;

        free(dxFWPkg);
        free(dxFWStor);
        free(dxFWData);
    }

    free(dxFW);
    free(dxFWBuf);
    fclose(f);

#endif //PARSER_FW

#if TEST_MTD_FW
    FILE *f = fopen(IPCAM_KERNEL_IMAGE, "rb");
    if (f == NULL){
        printf("Kernel file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t fwKernelSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Kenrel Image Size=%ld\n",fwKernelSize);

    dxFWStorage_t * dxFWKernel=malloc(sizeof(dxFWStorage_t)+fwKernelSize);
    dxFWKernel->Type=DXSTORAGETYPE_DXMTD;
    dxFWKernel->Size=fwKernelSize;

    memset(dxFWKernel->Data,0,dxFWKernel->Size);
    fread(dxFWKernel->Data, dxFWKernel->Size, 1, f);

    close(f);

    dxMD5(dxFWKernel->Data, dxFWKernel->Size,dxFWKernel->MD5);
    dxMD5_convert_hex(dxFWKernel->MD5,md5str);
    printf ("File_MD5=%s Size=%ld\n", md5str,dxFWKernel->Size);
    
    sprintf(dxFWKernel->Location,IPCAM_KERNEL_MTD);
    dxFWStoreMTD_Init(dxFWKernel);
    dxFWStoreMTD_Erase(dxFWKernel);
    dxFWStoreMTD_Write(dxFWKernel);
    dxFWStoreMTD_MD5(dxFWKernel);
    free(dxFWKernel);


    f = fopen(IPCAM_ROOTFS_IMAGE, "rb");
    if (f == NULL){
        printf("Rootfs file not find!\n");
        return -1;
    }

    fseek(f, 0, SEEK_END);
    uint32_t fwRootfsSize = ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    printf("FW Rootfs Image Size=%ld\n",fwRootfsSize);

    dxFWStorage_t * dxFWRootfs=malloc(sizeof(dxFWStorage_t)+fwRootfsSize);
    dxFWRootfs->Type=DXSTORAGETYPE_DXMTD;
    dxFWRootfs->Size=fwRootfsSize;

    memset(dxFWRootfs->Data,0,fwRootfsSize);
    fread(dxFWRootfs->Data, fwRootfsSize, 1, f);

    close(f);

    dxMD5(dxFWRootfs->Data, dxFWRootfs->Size,dxFWRootfs->MD5);
    dxMD5_convert_hex(dxFWRootfs->MD5,md5str);
    printf ("File_MD5=%s Size=%ld\n", md5str,dxFWRootfs->Size);

    sprintf(dxFWRootfs->Location,IPCAM_ROOTFS_MTD);
    dxFWStoreMTD_Init(dxFWRootfs);
    dxFWStoreMTD_Erase(dxFWRootfs);
    dxFWStoreMTD_Write(dxFWRootfs);
    dxFWStoreMTD_MD5(dxFWRootfs);
    free(dxFWRootfs);

#endif //TEST_MTD_FW

}

