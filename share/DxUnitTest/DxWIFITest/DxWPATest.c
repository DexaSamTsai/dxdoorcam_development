#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <sys/un.h>

#include "dxWIFI.h"
#include "dxWPACtrl.h"

static void scan_results_cb(void *arg)
{
    WpaScanResultObj_t* pWpaScanResultHdl = (WpaScanResultObj_t*)arg;
    
    pWpaScanResultHdl->pScanResultBuff[pWpaScanResultHdl->ScanResultSize] = '\0';    
	
	printf("user_date=%s\n",pWpaScanResultHdl->userData);
	
	char *rs0,*rs1,*rs2,*rs3,*rs4;
	rs0=(char*)malloc(17);
	rs1=(char*)malloc(32);
	rs2=(char*)malloc(32);
	rs3=(char*)malloc(64);
	rs4=(char*)malloc(32);

	int count=0;
	char *p, *temp;
	dxWiFi_Security_t st;
	
	p = strtok_r(pWpaScanResultHdl->pScanResultBuff, "\n", &temp);
	do {
		//printf("line %d = %s\n", count,p);
		if (count > 0 ){
			sscanf(p,"%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\t%s\t%s\t%s\t%[^\t\n]",&rs0[0],&rs0[1], &rs0[2], &rs0[3], &rs0[4], &rs0[5],rs1,rs2,rs3,rs4);
			printf("r=%d %hhx:%hhx:%hhx:%hhx:%hhx:%hhx ",count,rs0[0],rs0[1],rs0[2],rs0[3],rs0[4],rs0[5]);
			printf(" %s(%d) %s(%d)\n",rs3,strlen(rs3),rs4,strlen(rs4));
			st = wpa_ctrl_security_convert(rs3);
			printf("flag=%d",st);
			
			printf("\n");
		}
		count ++;
	} while ((p = strtok_r(NULL, "\n", &temp)) != NULL);
    
    dxMemFree(pWpaScanResultHdl->pScanResultBuff);
    dxMemFree(pWpaScanResultHdl);
}

int main(int argc, char** argv)
{
        char buf[WPA_BUF];
        struct wpa_ctrl *ctrl;
        int ret,len;
	char * rets;
	rets=(char*)malloc(32);

	wpa_ctrl_genconf(DXWIFI_MODE_STA,DXWIFI_CHANNEL_PLAN_JAPAN,DXWIFI_ADAPTIVITY_DISABLE);
	wpa_ctrl_runwpa();

        ctrl = wpa_ctrl_open(WPA_CTRL_WLAN0);
        if (!ctrl){
                printf("Could not get ctrl interface!\n");
                return -1;
        }

        ret = wpa_ctrl_command(ctrl,"STATUS");
        wpa_ctrl_close(ctrl);

	printf("----------------------------------\n");

        ret = wpa_ctrl_scan();
	printf("----------------------------------\n");

	char user_data[]="12345";
	printf("user_data=%s\n",user_data);

        ret = wpa_ctrl_scan_results(scan_results_cb,&user_data[0]);
	printf("----------------------------------\n");

	free(rets);
	return 0;

	memcpy(rets, wpa_ctrl_get_mac(), 6);
        printf("mac=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\n",rets[0],rets[1],rets[2],rets[3],rets[4],rets[5]);

	sleep(5);
	ret=wpa_ctrl_disable_network();
	printf("ret=%d\n",ret);
	printf("----------------------------------\n");

	char ssid[]="DK_SWRD_Test";
	char password[]="0000055555";
	//char ssid[]="JoeyMi";
	//char password[]="09374872810911266435";
	ret=wpa_ctrl_wifi_connect(ssid, DXWIFI_SECURITY_WPA2_AES_PSK , password, 0);
	printf("ret=%d\n",ret);

	if (ret ==0){
		printf("check : /var/conf/dhcpc.script\n");
		ret=dxDHCPC_Init();
	}
	printf("ret=%d\n",ret);

//	memcpy(rets, wpa_ctrl_get_ip(), 4);
//        printf("ip=%u.%u.%u.%u\n",(uint8_t)rets[0],(uint8_t)rets[1],(uint8_t)rets[2],(uint8_t)rets[3]);

	memcpy(rets, wpa_ctrl_get_bssid(), 6);
        printf("bssid=%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\n",rets[0],rets[1],rets[2],rets[3],rets[4],rets[5]);

        printf("SSID=%s\n",wpa_ctrl_get_ssid());

	int rssi;
	wpa_ctrl_get_rssi(&rssi);

	printf("RSSI=%d\n",rssi);

	printf("dxDHCPC_Status=%d\n",dxDHCPC_Status());

        return 0;
}

