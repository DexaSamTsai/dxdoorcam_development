#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <sys/un.h>
#include "tiny-AES128.h"
#include "Utils.h"
#include "WifiCommManager.h"
#include "dxSysDebugger.h"

#define WIFIMANAGER_SCAN_TEST                   1
#define WIFIMANAGER_CONNECT_TEST                1
#define     CONNECT_TEST_SSID                       ("Aaron")    //("DK_SWRD_Test")
#define     CONNECT_TEST_PASSWORD                   ("8888888888")      //("0000055555")
#define WIFIMANAGER_DIRECT_CONNECT_TEST         1
#define     DIRECT_CONNECT_TEST_SSID                ("DK_SWRD_Test")
#define     DIRECT_CONNECT_TEST_PASSWORD            ("0000055555")
#define     DIRECT_CONNECT_TEST_SECURITY_TYPE       DXWIFI_SECURITY_UNKNOWN //DXWIFI_SECURITY_WPA2_AES_PSK


void WifiEventFunction(void* arg)
{
    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case WIFI_COMM_EVENT_COMMUNICATION:
        {
            switch (EventHeader.ResultState)
            {
                case WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID:
                case WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED:
                case WIFI_COMM_MANAGER_WRONG_PASSWORD:
                case WIFI_COMM_MANAGER_PASSWORD_REQUIRED:
                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED_BUT_UNABLE_TO_REACH_WWW:
                {
                    G_SYS_DBG_LOG_V(  "WifiEventFunction- Unable to connect to Wifi (ResultState = %d)\r\n", EventHeader.ResultState );                                  
                }
                break;

                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED:
                {
                    dxIP_Addr_t ConnectedIpAddress;

                    if (WifiCommManagerGetIpAddress(&ConnectedIpAddress) == WIFI_COMM_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_V(  "WifiEventFunction- Connected Local Addr is at %u.%u.%u.%u\r\n",  (uint8_t)ConnectedIpAddress.v4[0],
                                                                                                            (uint8_t)ConnectedIpAddress.v4[1],
                                                                                                            (uint8_t)ConnectedIpAddress.v4[2],
                                                                                                            (uint8_t)ConnectedIpAddress.v4[3]);  



                    }
                    else
                    {
                        G_SYS_DBG_LOG_V(  "WifiEventFunction- Wifi connected but unable to obtain IP\r\n" );                                   
                    }
                }
                break;

                case WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER:
                case WIFI_COMM_MANAGER_CONNECTION_IS_DOWN:
                {
                    G_SYS_DBG_LOG_V(  "WifiEventFunction- Wifi disconnected!\r\n" );                                         
                }
                break;

                default:
                {
                    G_SYS_DBG_LOG_V(  "WifiEventFunction- Unhandled State Wifi ResultState = %d\r\n", EventHeader.ResultState );                     
                }
                break;
            }

        }
        break;

        case WIFI_COMM_EVENT_SCAN_AP:
        {
            if (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_REPORT)
            {
                //G_SYS_DBG_LOG_V(  "WifiEventFunction - WIFI_COMM_MANAGER_SCAN_AP_REPORT\r\n");           
                
                dxWiFi_Scan_Details_t* ApInfo = WifiCommManagerParseScanApReportEventObj(arg);
                if ((ApInfo) && (ApInfo->SSID.val[0] != NULL_STRING))
                {
                    G_SYS_DBG_LOG_V(  "WifiEventFunction - SSID=%s, SignalStr=%d\r\n", ApInfo->SSID.val, ApInfo->signal_strength);                       
                }
                

            }
            else if (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_REPORT_ENDED)
            {
                G_SYS_DBG_LOG_V(  "WifiEventFunction - Scan Ended\r\n");                  
            }
            else if ((EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_TIME_OUT) || (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_INTERNAL_ERROR))
            {
                G_SYS_DBG_LOG_V(  "WifiEventFunction - Scan Error (%d)\r\n", EventHeader.ResultState);                 
            }
            
        }
        break;
        
        default:
        {
            G_SYS_DBG_LOG_V(  "WifiEventFunction- Unhandled EVENT Wifi ReportEvent = %d\r\n", EventHeader.ReportEvent );       
        }
            break;
    }    
}

int main(int argc, char **argv)
{

    int c;
    char *ivalue = NULL;
    char *pvalue = NULL;
    int action=0;
    int value=0;

    for (;;) {
        c = getopt( argc, argv, "scdi:p:");
        if (c == EOF) break;
        switch (c) {
        case 's':
             printf("scan\n");
             action=1;
             value=1;
        break;
        case 'c':
             printf("connect\n");
             action=2;
        break;
        case 'd':
             printf("direct connect\n");
             action=3;
        break;
        case 'i':
             printf("ssid=");
             ivalue = optarg;
             printf("%s\n",ivalue);
             value++;
        break;
        case 'p':
             printf("passwd=");
             pvalue = optarg;
             printf("%s\n",pvalue);
             value++;
        break;
        default:  // show usage
             action=0;
             value=0;
        }
    }

    if ((action==0) || (value==0)){
        printf( "Usage: [-s] : scan\n");
        printf( "Usage: [-c -i ssid -p password] : connect to ssid / password\n");
        printf( "Usage: [-d -i ssid -p password] : direct connect to ssid / password\n");
        exit(1);
    }

    if ((action==2) && (value!=2)){
        printf( "Usage: [-c -i ssid -p password] : connect to ssid / password\n");
        exit(1);
    }

    if ((action==3) && (value!=2)){
        printf( "Usage: [-d -i ssid -p password] : direct connect to ssid / password\n");
        exit(1);
    }

    char buf[1024];
    int ret,len;

    GSysDebuggerInit();

    WifiCommManager_result_t dxret;

    uint16_t SourceOfOrigin=1;
    uint32_t TaskIdentificationNumber=12345;

    printf("[dxWIFITest]---------------------------------\n");
    dxret=WifiCommManagerInit(WifiEventFunction);
    printf("[dxWIFITest] WifiCommManagerInit ret=%d\n",dxret);
    
    sleep(1);

    if (action == 1){
#if WIFIMANAGER_SCAN_TEST
	dxret=WifiCommManagerScanSSIDTask_Asynch(SourceOfOrigin,TaskIdentificationNumber);
	printf("[dxWIFITest] WifiCommManagerScanSSIDTask_Asynch ret=%d\n",dxret);
        sleep(5);
#endif    

    }else if ((action == 2 ) && (value==2)){
#if WIFIMANAGER_CONNECT_TEST
        dxret=WifiCommManagerConnectToSSIDTask_Asynch(  ivalue,
                                                        strlen(ivalue),
                                                        pvalue,
                                                        strlen(pvalue),
                                                        SourceOfOrigin,
                                                        TaskIdentificationNumber);
        printf("[dxWIFITest] WifiCommManagerConnectToSSIDTask_Asynch ret=%d\n",dxret);
        sleep(15);
#endif    
    }else if ((action == 3 ) && (value==2)){
#if WIFIMANAGER_DIRECT_CONNECT_TEST    
        dxret=WifiCommManagerConnectToSSIDDirectTask_Asynch(  ivalue,
                                                              strlen(ivalue),
                                                              pvalue,
                                                              strlen(pvalue),
                                                              DIRECT_CONNECT_TEST_SECURITY_TYPE,
                                                              SourceOfOrigin,
                                                              TaskIdentificationNumber);
        printf("[dxWIFITest] WifiCommManagerConnectToSSIDDirectTask_Asynch ret=%d\n",dxret);
        sleep(15);
#endif    
    }
        
    return;

    //TODO: Add TEST CASE for below
    ret=WifiCommManagerGetWifiConnectionStatus();
    printf("[dxWIFITest] WifiCommManagerGetWifiConnectionStatus ret=%d\n",ret);
	
    dxret=WifiCommManagerGetWifiRSSI();
    printf("[dxWIFITest] WifiCommManagerGetWifiRSSI ret=%d\n",dxret);

    return 0;
}
