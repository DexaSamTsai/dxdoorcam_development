//============================================================================
// File: BLEConsoleAPI.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/28
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _BLECONSOLEAPI_H
#define _BLECONSOLEAPI_H

#pragma once

#ifdef DK_GATEWAY
#endif
#include "dxOS.h"

#include "BLEManager.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int clear_scan(int argc, char *argv[]);
int clear_info(int argc, char *argv[]);
int disp_scan(int argc, char *argv[]);
int scan_all(int argc, char *argv[]);
int scan_paired(int argc, char *argv[]);
int scan_open_pair(int argc, char *argv[]);
int pair(int argc, char *argv[]);
int unpair(int argc, char *argv[]);
int renew(int argc, char *argv[]);
int clear_security(int argc, char *argv[]);
int control(int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif // _BLECONSOLEAPI_H
