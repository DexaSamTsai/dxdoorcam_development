//============================================================================
// File: DKScanList.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/31
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "DKScanList.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct _tagDKScanList DKScanList_t;

typedef struct _tagDKScanList
{
    DKScanList_t    *Next;
    UIDeviceEntry_t *pEntry;
} DKScanList_t;

typedef struct _tagDKScanListHandle
{
    DKScanList_t    *pDKListHead;
    DKScanList_t    *pDKListTail;
} DKScanListHandle_t;

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int DKScanList_Init(void)
{
    DKScanListHandle_t *handle = calloc(1, sizeof(DKScanListHandle_t));
    return (int)handle;
}

void DKScanList_Empty(int handle)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    DKScanList_t *p = NULL;

    if (h == NULL) return;

    p = h->pDKListHead;
    while (p)
    {
        UIDeviceEntry_t *q = p->pEntry;

        if (q)
        {
            free(q);
            q = NULL;
        }

        h->pDKListHead = h->pDKListHead->Next;
        free(p);
        p = h->pDKListHead;
    }

    h->pDKListHead = NULL;
    h->pDKListTail = NULL;
}

void DKScanList_Save(int handle, UIDeviceEntry_t *pEntry)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    uint8_t found = 0;

    if (pEntry == NULL ||
        h == NULL)
    {
        return ;
    }

    DKScanList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        if (memcmp(pNode->pEntry->DevAddress.Addr, pEntry->DevAddress.Addr, sizeof(pEntry->DevAddress.Addr)) == 0)
        {
            // found
            found = 1;
            pNode->pEntry->ProductID = pEntry->ProductID;
            pNode->pEntry->PairFlag = pEntry->PairFlag;
            pNode->pEntry->EncryptFlag = pEntry->EncryptFlag;
            break;
        }

        pNode = pNode->Next;
    }

    if (found == 0)
    {
        DKScanList_t *pDataToSave = calloc(1, sizeof(DKScanList_t));
        if (pDataToSave == NULL)
        {
            return ;
        }

        pDataToSave->pEntry = calloc(1, sizeof(UIDeviceEntry_t));
        if (pDataToSave->pEntry == NULL)
        {
            free(pDataToSave);
            pDataToSave = NULL;
            return ;
        }

        memcpy(pDataToSave->pEntry->DevAddress.Addr, pEntry->DevAddress.Addr, sizeof(pEntry->DevAddress.Addr));
        pDataToSave->pEntry->ProductID = pEntry->ProductID;
        pDataToSave->pEntry->PairFlag = pEntry->PairFlag;
        pDataToSave->pEntry->EncryptFlag = pEntry->EncryptFlag;

        if (h->pDKListTail == NULL)
        {
            h->pDKListTail = pDataToSave;
            h->pDKListTail->Next = NULL;
            h->pDKListHead = pDataToSave;
            h->pDKListHead->Next = NULL;
        }
        else
        {
            h->pDKListTail->Next = pDataToSave;
            h->pDKListTail = pDataToSave;
        }
    }
}

void DKScanList_Remove(int handle, DevAddress_t *pDevAddr)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL ||
        pDevAddr == NULL)
    {
        return ;
    }

    DKScanList_t *pNode = h->pDKListHead;
    DKScanList_t *pPreNode = NULL;
    while (pNode)
    {
        if (memcmp(pNode->pEntry->DevAddress.Addr, pDevAddr->Addr, sizeof(pDevAddr->Addr)) == 0)
        {
            // found
            if (pPreNode == NULL)
            {
                // We are removing the top element in DKScanList
                pPreNode = pNode;
                h->pDKListHead = pNode->Next;

                DKScanList_Free(pPreNode->pEntry);
                free(pPreNode);
                pPreNode = NULL;
            }
            else
            {
                pPreNode->Next = pNode->Next;

                DKScanList_Free(pNode->pEntry);
                free(pNode);
                pNode = NULL;
            }

            break;
        }

        pPreNode = pNode;
        pNode = pNode->Next;
    }
}

UIDeviceEntry_t *DKScanList_GetAt(int handle, int index)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    UIDeviceEntry_t *pEntry = NULL;
    int i = 0;

    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL)
    {
        return NULL;
    }

    DKScanList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        if (i == index)
        {
            pEntry = pNode->pEntry;
            break;
        }

        pNode = pNode->Next;
        i++;
    }

    return pEntry;
}

size_t DKScanList_Count(int handle)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    size_t count = 0;

    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL)
    {
        return 0;
    }

    DKScanList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        count++;

        pNode = pNode->Next;
    }

    return count;
}

void DKScanList_Free(UIDeviceEntry_t *pEntry)
{
    if (pEntry)
    {
        free(pEntry);
        pEntry = NULL;
    }
}

void DKScanList_Uninit(int handle)
{
    DKScanListHandle_t *h = (DKScanListHandle_t *)handle;
    if (h)
    {
        DKScanList_Empty(handle);
        h->pDKListHead = NULL;
        h->pDKListTail = NULL;

        free(h);
        h = NULL;
    }
}
