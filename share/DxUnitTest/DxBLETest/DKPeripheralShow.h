//============================================================================
// File: DKPeripheralShow.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/08/07
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/02
//     1) Description: Support motion sensor
//        Modified:
//            - DKPeripheralShow_AggregateData()
//
//        Added:
//            - Declare AggregateItemFormat1_t structure
//
//     Version: 0.3
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DKPERIPHERALSHOW_H
#define _DKPERIPHERALSHOW_H

#pragma once

#ifdef DK_GATEWAY
#endif

#include "BLEManager.h"
#include "BLEHandler.h"
#include "DKPeripheralDataList.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
#if !defined(AggregateItem_t)
typedef struct _tagAggregateItem
{
    uint16_t        nDataID;
    uint16_t        nDuration;
    uint32_t        nDataValue;
} AggregateItem_t;
#endif // AggregateItem_t

#if !defined(AggregateItemFormat1_t)
typedef struct _tagAggregateItemFormat1
{
    uint32_t        nTime;
    uint8_t         nDataID;
    uint8_t         nType;
    uint16_t        nDuration;
    uint32_t        nDataValue;
} AggregateItemFormat1_t;
#endif // AggregateItemFormat1_t

typedef union AggregatedHeaderFormatInfo_u  AggregatedHeaderFormatInfo_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void DKPeripheralShow_AdData(PeripheralDataType_t *pValue);
void DKPeripheralShow_AggregateData(AggregatedHeaderFormatInfo_t AgHFormat, void *pAgD);

#ifdef __cplusplus
}
#endif

#endif // _DKPERIPHERALSHOW_H
