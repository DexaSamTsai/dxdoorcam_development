//============================================================================
// File: DKScanList.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/31
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DKSCANLIST_H
#define _DKSCANLIST_H

#pragma once

#ifdef DK_GATEWAY
#endif

#include "BLEManager.h"
#include "dk_Peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/
typedef enum _enuPairFlag
{
    UI_DEVICE_PAIRFLAG_UNKNOWN,
    UI_DEVICE_PAIRFLAG_PAIRED,
    UI_DEVICE_PAIRFLAG_UNPAIR,
} PairFlag_t;

typedef enum _enuEncryptionFlag
{
    UI_ENCRYPTFLAG_ENCRYPTED,
    UI_ENCRYPTFLAG_UNENCRYPTED,
} EncryptionFlag_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct _tagUIDeviceEntry
{
    DevAddress_t        DevAddress;
    uint8_t             ProductID;
    PairFlag_t          PairFlag;
    EncryptionFlag_t    EncryptFlag;
} UIDeviceEntry_t;

/******************************************************
 *               Function Definitions
 ******************************************************/
int DKScanList_Init(void);
void DKScanList_Empty(int handle);
void DKScanList_Save(int handle, UIDeviceEntry_t *pEntry);
void DKScanList_Remove(int handle, DevAddress_t *pDevAddr);
UIDeviceEntry_t *DKScanList_GetAt(int handle, int index);
size_t DKScanList_Count(int handle);
void DKScanList_Free(UIDeviceEntry_t *pEntry);
void DKScanList_Uninit(int handle);

#ifdef __cplusplus
}
#endif

#endif // _DKSCANLIST_H
