//============================================================================
// File: BLEHandler.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/28
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/02
//     1) Description: Support motion sensor
//        Modified:
//            - BleManagerEvent_DeviceTaskReadAggregatedDataReport()
//
//     Version: 0.3
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "dxOS.h"

#include "Utils.h"
#include "BLEManager.h"
#include "BLEHandler.h"
#include "DKScanList.h"
#include "DKPeripheralDataList.h"
#include "DKPeripheralShow.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
//BLE Central FW update
#define BLE_FW_UPDATE_WRITE_BLOCK_HEADER_SIZE               2
#define BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE                 32
#define BLE_FW_UPDATE_WRITE_BLOCK_CRC_SIZE                  1
#define BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE                (BLE_FW_UPDATE_WRITE_BLOCK_HEADER_SIZE + BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE + BLE_FW_UPDATE_WRITE_BLOCK_CRC_SIZE)
#define BLE_FW_UPDATE_WRITE_BLOCK_HEADER_INDEX              0
#define BLE_FW_UPDATE_WRITE_BLOCK_DATA_INDEX                2
#define BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX                 34
#define BLE_FW_UPDATE_WATCH_DOG_TIME                        (1*SECONDS)
#define BLE_FW_UPDATE_RETRY_MAX                             (10)

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct
{
    uint32_t        FwSize;
    uint32_t        CurrentReadPosition;
    uint32_t        RetryCount;
    uint8_t         FwBuffer[64];

} BleFwUpdate_t;


/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Static Function Declarations
 ******************************************************/
static dxOS_RET_CODE BleManagerEventReportHandler(void* arg);
static void BLEHostRemoteMessageProcess(uint8_t MessageType, uint8_t MessagePayloadLen, uint8_t* DataInTransferPayload);

dxOS_RET_CODE BleManagerEvent_UnpairDeviceReport(void *arg);
dxOS_RET_CODE BleManagerEvent_KeepersDeviceDataReport(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceTaskAccessWithUpdateDataReport(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceTaskAccessReport(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceTaskReadAggregatedDataReport(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceSecurityDisabled(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceNewSecurityExchanged(void *arg);
dxOS_RET_CODE BleManagerEvent_NewDeviceAdded(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceUnpaired(void *arg);
dxOS_RET_CODE BleManagerEvent_PeripheralRoleDataInTransferPayload(void *arg);
dxOS_RET_CODE BleManagerEvent_DeviceSystemInfo(void *arg);
dxOS_RET_CODE BleManagerEvent_FWBlockResponseVerify(void *arg);
dxOS_RET_CODE BleManagerEvent_FWUpdateRebootToAppReport(void *arg);

void ParsingDeviceInfoReport(DeviceInfoReport_t *pDevInfoReport);

/******************************************************
 *               Variable Definitions
 ******************************************************/
//FW Download
BleFwUpdate_t               BleFwUpdateInfo;
static int                  hMACAddrList = 0;
static int                  hPeripheralDataList = 0;

/******************************************************
 *               Function Definitions
 ******************************************************/
uint16_t GetNumberOfAdDataValue(uint8_t *pAdData, uint16_t AdDataLen)
{
    uint16_t count = 0;
    uint16_t i = 0;

    while (i < AdDataLen)
    {
        uint8_t n = DKPeripheral_GetDataTypeLenForDataTypeID(pAdData[i]);
        if (n == 0)
        {
            break;
        }

        count ++;
        i += (1 + n);
    }

    return count;
}

static dxOS_RET_CODE BleManagerEventReportHandler(void* arg)
{
    dxOS_RET_CODE  Result = DXOS_SUCCESS;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ResultState)
    {
        case DEV_MANAGER_TASK_FAILED_CONNECTION_UNSTABLE:
            NPRINT("DEV_MANAGER_TASK_FAILED_CONNECTION_UNSTABLE!!!\n");
            break;
        case DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED:
            NPRINT("DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED!!!\n");
            break;
        case DEV_MANAGER_TASK_DEVICE_IN_KEEPER_LIST_BUT_FOUND_UNPAIRED:
            break;
        case DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_MISSING:
            NPRINT("DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_MISSING!!!\n");
            break;
        case DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_IS_NOT_VALID:
            NPRINT("DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_IS_NOT_VALID!!!\n");
            break;
        case DEV_MANAGER_TASK_DEVICE_PIN_CODE_IS_NOT_VALID:
            NPRINT("DEV_MANAGER_TASK_DEVICE_PIN_CODE_IS_NOT_VALID!!!\n");
            break;
        case DEV_MANAGER_TASK_TIME_OUT:
            NPRINT("DEV_MANAGER_TASK_TIME_OUT!!!\n");
            break;
        case DEV_MANAGER_TASK_INTERNAL_ERROR:
            NPRINT("DEV_MANAGER_TASK_INTERNAL_ERROR!!!\n");
            break;
        case DEV_MANAGER_TASK_INTERNAL_FATAL_ERROR:
            NPRINT("DEV_MANAGER_TASK_INTERNAL_FATAL_ERROR!!!\n");
            break;
        default:
            break;
    }

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_UNPAIR_DEVICE_REPORT:
            Result = BleManagerEvent_UnpairDeviceReport(arg);
            break;

        case DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT:
            Result = BleManagerEvent_KeepersDeviceDataReport(arg);
            break;

        case DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT:
            Result = BleManagerEvent_DeviceTaskAccessWithUpdateDataReport(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT:
            Result = BleManagerEvent_DeviceTaskAccessReport(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT:
            Result = BleManagerEvent_DeviceTaskReadAggregatedDataReport(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED:
            Result = BleManagerEvent_DeviceSecurityDisabled(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED:
            Result = BleManagerEvent_DeviceNewSecurityExchanged(arg);
            break;

        case DEV_MANAGER_EVENT_NEW_DEVICE_ADDED:
            Result = BleManagerEvent_NewDeviceAdded(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_UNPAIRED:
            Result = BleManagerEvent_DeviceUnpaired(arg);
            break;

        case DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD:
            Result = BleManagerEvent_PeripheralRoleDataInTransferPayload(arg);
            break;

        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO:
            Result = BleManagerEvent_DeviceSystemInfo(arg);
            break;

        case DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY:
            Result = BleManagerEvent_FWBlockResponseVerify(arg);
            break;

        case DEV_MANAGER_EVENT_FW_UPDATE_REBOOT_TO_APP_REPORT:
            Result = BleManagerEvent_FWUpdateRebootToAppReport(arg);
            break;

        default:
            break;
    }

    //Always clean the arg object from event
    BleManagerCleanEventArgumentObj(arg);

    return Result;
}

static void BLEHostRemoteMessageProcess(uint8_t MessageType, uint8_t MessagePayloadLen, uint8_t* DataInTransferPayload)
{
    NPRINT("Host Remove Message Arrived\n");
    NPRINT("---------------------------\n");
    NPRINT("Message type = 0x%x, PayloadLen = %d\n", MessageType, MessagePayloadLen);

    switch (MessageType)
    {
        case DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_REPORT_REQ:
            break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG:
            break;

        case DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE:
            break;

        case DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_TURN_DOWN_REQ:
            break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REQ:
            break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_EXIT_SETUP_MODE:
            break;

        case DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_REQ:
            break;

        case DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_REQ:
            break;

        default:
            NPRINT("BLEHostRemoteMessageProcess unknown messagetype\n");
        break;
    }
}

dxOS_RET_CODE BleManagerEvent_UnpairDeviceReport(void *arg)
{
    DeviceInfoReport_t* UnpairDevInfo = NULL;
    UnpairDevInfo = BleManagerParseUnPairPeripheralReportEventObj(arg);

    if (UnpairDevInfo == NULL)
    {
        return DXOS_ERROR;
    }

    NPRINT("UnPair Device Address Found :");

    PRINT_BIN(UnpairDevInfo->DevAddress.Addr, sizeof(UnpairDevInfo->DevAddress.Addr), ":");

    NPRINT("\n\tProductID = %d, BattLevel = %d, AdvLen = %d", UnpairDevInfo->DkAdvertisementData.Header.ProductID,
                                                              UnpairDevInfo->DkAdvertisementData.BatteryInfo,
                                                              UnpairDevInfo->DkAdvertisementData.AdDataLen);

    uint8_t encrypted = (DK_FLAG_IS_PAYLOAD_ENCRYPTED(UnpairDevInfo->DkAdvertisementData.Header.Flag) ==  0) ? 0 : 1;
    NPRINT(", RSSI = %d, Encrypted: %d\n", UnpairDevInfo->rssi, encrypted);

    UIDeviceEntry_t e = { UnpairDevInfo->DevAddress,
                          UnpairDevInfo->DkAdvertisementData.Header.ProductID,
                          UI_DEVICE_PAIRFLAG_UNPAIR,
                          UnpairDevInfo->Status.bits.Status_NoSecurity_b1 };

    DKScanList_Save(hMACAddrList, &e);

    dxTimeDelayMS(100);     // To slow down NPRINT.

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_KeepersDeviceDataReport(void *arg)
{
    DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);

    ParsingDeviceInfoReport(DeviceKeeperInfoReport);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceTaskAccessWithUpdateDataReport(void *arg)
{
    DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);

    NPRINT("BleManagerEvent_DeviceTaskAccessWithUpdateDataReport, DevHandlerID: %d, rssi: %d\n", DeviceKeeperInfoReport->DevHandlerID,
                                                                                                 DeviceKeeperInfoReport->rssi);

    ParsingDeviceInfoReport(DeviceKeeperInfoReport);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceTaskAccessReport(void *arg)
{
    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    NPRINT("BLE Task Report ResultCode = %d\n", EventHeader.ResultState);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceTaskReadAggregatedDataReport(void *arg)
{
    AggregatedReportObj_t AggData = BleManagerParseAggregatedDataReportEventObj(arg);

    NPRINT("Read Aggregated Data : ");

    PRINT_BIN(AggData.Header.DevAddress.Addr, sizeof(AggData.Header.DevAddress.Addr), ":");

    NPRINT(", Total Items = %d\n", AggData.Header.TotalAggregatedItem);

    int i = 0;

    uint8_t AfDFormat = AggData.Header.AggregatedItemHeader.FormatInfo.bits.AfDFormat_b0_3;

    for (i = 0; i < AggData.Header.TotalAggregatedItem; i++)
    {
        if (AfDFormat == 0)
        {
            AggregateItem_t *pAgItem = (AggregateItem_t *)&AggData.pAggregatedItem[i * sizeof(AggregateItem_t)];
            if (pAgItem)
            {
                NPRINT("\t");
                DKPeripheralShow_AggregateData(AggData.Header.AggregatedItemHeader.FormatInfo, (void *)pAgItem);
            }
        }
        else if (AfDFormat == 1)
        {
            AggregateItemFormat1_t *pAgItem = (AggregateItemFormat1_t *)&AggData.pAggregatedItem[i * sizeof(AggregateItemFormat1_t)];
            if (pAgItem)
            {
                NPRINT("\t");
                DKPeripheralShow_AggregateData(AggData.Header.AggregatedItemHeader.FormatInfo, (void *)pAgItem);
            }
        }
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceSecurityDisabled(void *arg)
{
    int16_t DevIndex = BleManagerParseDeviceIndexReportEventObj(arg);

    if (DevIndex >= 0)
    {
        NPRINT("Device Security Disabled with Index %d\n", DevIndex);
    }
    else
    {
        NPRINT("WARNING: Dev man Event invalid DevIndex (%d)\n", DevIndex);
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceNewSecurityExchanged(void *arg)
{
    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
    {
        int16_t DevIndex = BleManagerParseDeviceIndexReportEventObj(arg);

        if (DevIndex >= 0)
        {
            const DeviceInfoKp_t* DevInfoKeeper = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

            if (DevInfoKeeper)
            {
                NPRINT("Security Key Exchange : ");

                PRINT_BIN(DevInfoKeeper->DevAddress.pAddr, B_ADDR_LEN_8, ":");

                NPRINT("   rssi: %d\n", DevInfoKeeper->rssi);
            }
        }
        else
        {
            NPRINT("WARNING: Dev man Event invalid DevIndex (%d)\n", DevIndex);
        }
    }
    else
    {
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_NewDeviceAdded(void *arg)
{
    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
    {
        DeviceInfoReport_t* PeripheralInfo = BleManagerParseNewPeripheralAddedReportEventObj(arg);
        if (PeripheralInfo)
        {
            char DeviceName[30];
            memset( DeviceName, 0, 30 );

            switch (PeripheralInfo->DkAdvertisementData.Header.ProductID)
            {
                case DK_PRODUCT_TYPE_WEATHER_ID:
                    memcpy(DeviceName, "WeatherCube", sizeof("WeatherCube"));
                    break;

                case DK_PRODUCT_TYPE_HOME_DOOR_ID:
                    memcpy(DeviceName, "HomeDoor", sizeof("HomeDoor"));
                    break;

                case DK_PRODUCT_TYPE_POWERPLUG_ID:
                    memcpy(DeviceName, "PowerPlug", sizeof("PowerPlug"));
                    break;

                case DK_PRODUCT_TYPE_LIGHTBULB_ID:
                    memcpy(DeviceName, "LightBulb", sizeof("LightBulb"));
                    break;

                case DK_PRODUCT_TYPE_SMOKE_DETECTOR:
                    memcpy(DeviceName, "SmokeDetector", sizeof("SmokeDetector"));
                    break;

                case DK_PRODUCT_TYPE_SHOCK_SENSE_DETECTOR:
                    memcpy(DeviceName, "ShockDetector", sizeof("ShockDetector"));
                    break;

                case DK_PRODUCT_TYPE_INLET_SWITCH_ID:
                    memcpy(DeviceName, "Inlet Switch", sizeof("Inlet Switch"));

                default:
                    memcpy(DeviceName, "MyDevice", sizeof("MyDevice"));
                    break;
            }

            //Get the security key
            const SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndex(PeripheralInfo->DevHandlerID);

            //Get the FwVersion
            const FwVersion_t* DevFwVersion = BLEDevKeeper_GetFwVersionFromIndex(PeripheralInfo->DevHandlerID);
            char  DeviceVersionStr[16];
            if (DevFwVersion)
            {
                memset( &DeviceVersionStr, 0, sizeof(DeviceVersionStr) );
                snprintf( DeviceVersionStr , sizeof(DeviceVersionStr), "%d.%d.%d", DevFwVersion->Major, DevFwVersion->Middle, DevFwVersion->Minor );
            }

            unsigned short SignalStrength;
            CONVERT_SIGNAL_STREGTH(PeripheralInfo->rssi, SignalStrength);

            NPRINT("MAC: ");

            PRINT_BIN(PeripheralInfo->DevAddress.Addr, sizeof(PeripheralInfo->DevAddress.Addr), ":");

            NPRINT(", DevName: %s, ProductID: %d, DevFwVersion: %s\n", DeviceName,
                                                                       PeripheralInfo->DkAdvertisementData.Header.ProductID,
                                                                       (DevFwVersion) ? DeviceVersionStr:"1.0");

            NPRINT("\tSignalStrength: %d, BatteryInfo: %d, SecurityKey: 0x", SignalStrength,
                                                                           PeripheralInfo->DkAdvertisementData.BatteryInfo);

            PRINT_BIN(SecurityKey->value, sizeof(SecurityKey->value), ",");

            NPRINT("\n");

            UIDeviceEntry_t e = { PeripheralInfo->DevAddress,
                                  PeripheralInfo->DkAdvertisementData.Header.ProductID,
                                  UI_DEVICE_PAIRFLAG_PAIRED,
                                  PeripheralInfo->Status.bits.Status_NoSecurity_b1 };

            DKScanList_Save(hMACAddrList, &e);
        }
    }
    else
    {
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceUnpaired(void *arg)
{
    DeviceInfoReport_t* UnpairedPeripheralInfo = BleManagerParseUnpairedPeripheralReportEventObj(arg);

    NPRINT("Device Unpaired : ");

    PRINT_BIN(UnpairedPeripheralInfo->DevAddress.Addr, sizeof(UnpairedPeripheralInfo->DevAddress.Addr), ":");

    NPRINT("\n");

    UIDeviceEntry_t e = {UnpairedPeripheralInfo->DevAddress, UI_DEVICE_PAIRFLAG_UNPAIR, UI_ENCRYPTFLAG_ENCRYPTED};
    DKScanList_Save(hMACAddrList, &e);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_PeripheralRoleDataInTransferPayload(void *arg)
{
    static uint8_t* MessagePayloadCollectionBuffer = NULL;
    static uint8_t* pMessagePayloadCollectionBufferWorkPtr = NULL;
    static uint8_t  MessageType;
    static uint8_t  MessagePayloadLen;
    static uint8_t  MessagePayloadLenLeft;
    dxBOOL          MessageCleanup = dxFALSE;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
    {
        uint8_t* DataInTransferPayload = BleManagerParsePeripheralRoleDataInPayloadReportEventObj(arg);

        uint16_t PayloadPreamble = BUILD_UINT16(DataInTransferPayload[0], DataInTransferPayload[1]);

        if (PayloadPreamble == DATA_TRANSFER_PREAMBLE)
        {
            if (MessagePayloadCollectionBuffer)
            {
                free(MessagePayloadCollectionBuffer);
                MessagePayloadCollectionBuffer = NULL;
                pMessagePayloadCollectionBufferWorkPtr = NULL;
                NPRINT( "WARNING: Peripheral Role Message In was not fully received before new message is found!\n");
            }

            MessageType = DataInTransferPayload[2];
            MessagePayloadLen = DataInTransferPayload[3];

            if (MessagePayloadLen)
            {
                if (MessagePayloadLen > (DATA_TRANSFER_SIZE_PER_CHUNK - 4))
                {
                    MessagePayloadCollectionBuffer = calloc(1, MessagePayloadLen);
                    pMessagePayloadCollectionBufferWorkPtr = MessagePayloadCollectionBuffer;

                    memcpy(pMessagePayloadCollectionBufferWorkPtr, &DataInTransferPayload[4], (DATA_TRANSFER_SIZE_PER_CHUNK - 4));

                    pMessagePayloadCollectionBufferWorkPtr += (DATA_TRANSFER_SIZE_PER_CHUNK - 4);
                    MessagePayloadLenLeft = MessagePayloadLen - (DATA_TRANSFER_SIZE_PER_CHUNK - 4);
                }
            }

            //We don't need to collect more payload, this is all for this message so we process it
            if (MessagePayloadCollectionBuffer == NULL)
            {
                BLEHostRemoteMessageProcess(MessageType, MessagePayloadLen, &DataInTransferPayload[4]);
            }

        }
        else if (MessagePayloadCollectionBuffer)
        {

            uint8_t SizeToCopy = (MessagePayloadLenLeft > DATA_TRANSFER_SIZE_PER_CHUNK) ? DATA_TRANSFER_SIZE_PER_CHUNK:MessagePayloadLenLeft;

            memcpy( pMessagePayloadCollectionBufferWorkPtr,
                    &DataInTransferPayload[0],
                    SizeToCopy);

            if (MessagePayloadLenLeft >= SizeToCopy)
            {
                MessagePayloadLenLeft -= SizeToCopy;
                if (MessagePayloadLenLeft)
                {
                    pMessagePayloadCollectionBufferWorkPtr += SizeToCopy;
                }
                else
                {
                    BLEHostRemoteMessageProcess(MessageType, MessagePayloadLen, MessagePayloadCollectionBuffer);
                    MessageCleanup = dxTRUE;
                }
            }
            else
            {
                NPRINT( "WARNING: Peripheral Role Data Transfer MessagePayloadLenLeft is less than SizeToCopy\n");
                MessageCleanup = dxTRUE;
            }
        }
    }
    else
    {
        NPRINT( "WARNING: Peripheral Role Data Transfer error, message received in progress will be erased!\n");
        MessageCleanup = dxTRUE;
    }

    if (MessageCleanup == dxTRUE)
    {
        if (MessagePayloadCollectionBuffer)
            free(MessagePayloadCollectionBuffer);

        MessagePayloadCollectionBuffer = NULL;
        pMessagePayloadCollectionBufferWorkPtr = NULL;
        MessageType = 0;
        MessagePayloadLen = 0;
        MessagePayloadLenLeft = 0;
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_DeviceSystemInfo(void *arg)
{
    SystemInfoReportObj_t* pSystemInfo = BleManagerParsePeripheralSystemInfoReportEventObj(arg);

    if (pSystemInfo)
    {
        NPRINT("BLE System Info ImgType = %d Ver = %s\n", pSystemInfo->ImageType, pSystemInfo->Version);
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_FWBlockResponseVerify(void *arg)
{
    uint8_t  DataLen = 0;
    uint8_t* pData = BleManagerParseFwBlockWriteVerifyReportEventObj(arg, &DataLen);

    if (pData && DataLen)
    {


    }
    else
    {
        NPRINT( "WARNING: Central FW block rsp no data found\n");
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE BleManagerEvent_FWUpdateRebootToAppReport(void *arg)
{
    return DXOS_SUCCESS;
}

int MACAddrList_GetHandle(void)
{
    return hMACAddrList;
}

int PeripheralDataList_GetHandle(void)
{
    return hPeripheralDataList;
}

BLEManager_result_t BLESDK_Init(void)
{
    BLEManager_result_t result = BleManagerInit(BleManagerEventReportHandler);

    if (result == DEV_MANAGER_SUCCESS)
    {
        hMACAddrList = DKScanList_Init();
        hPeripheralDataList = DKPeripheralList_Init();

        BleManagerSetRoleToCentral();
    }
    return result;
}

void ParsingDeviceInfoReport(DeviceInfoReport_t *pDevInfoReport)
{
    // Save Header
    UIPeripheralAdDataHeader_t PeripheralDataHeader;
    memcpy(PeripheralDataHeader.DevAddress.Addr, pDevInfoReport->DevAddress.Addr, sizeof(pDevInfoReport->DevAddress.Addr));
    PeripheralDataHeader.ProductID = pDevInfoReport->DkAdvertisementData.Header.ProductID;
    PeripheralDataHeader.rssi = pDevInfoReport->rssi;
    PeripheralDataHeader.Flag = pDevInfoReport->DkAdvertisementData.Header.Flag;
    PeripheralDataHeader.BatteryInfo = pDevInfoReport->DkAdvertisementData.BatteryInfo;

    PeripheralDataHeader.TotalDataValueList = GetNumberOfAdDataValue(pDevInfoReport->DkAdvertisementData.AdData, pDevInfoReport->DkAdvertisementData.AdDataLen);

    // Save Body
    PeripheralDataType_t *pDataValueList = (PeripheralDataType_t *)calloc(1, sizeof(PeripheralDataType_t) * PeripheralDataHeader.TotalDataValueList);

    if (pDataValueList == NULL)
    {
        return ;
    }

    uint8_t DataLenOut = 0;
    uint8_t DataOut[30];

    int index = 0;

    if (pDevInfoReport)
    {
        switch(pDevInfoReport->DkAdvertisementData.Header.ProductID)
        {
        case DK_PRODUCT_TYPE_PHOTOKEY_ID:           // 0x01
            break;
        case DK_PRODUCT_TYPE_WEATHER_ID:            // 0x02
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_ATMOSPHERIC_PRESSURE, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 2)
                {
                    uint16_t *pValue = (uint16_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_ATMOSPHERIC_PRESSURE;
                    pDataValue->ValueType = VALUE_TYPE_UINT16;
                    pDataValue->UINT16 = *pValue;

                    index++;
                }
            }
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_TEMPERATURE, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 2)
                {
                    int16_t *pValue = (int16_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_TEMPERATURE;
                    pDataValue->ValueType = VALUE_TYPE_INT16;
                    pDataValue->INT16 = *pValue;

                    index++;
                }
            }
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_SHORT_RELATIVE_HUMILITY, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_SHORT_RELATIVE_HUMILITY;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_IBEACON_ID:            // 0x03
            break;
        case DK_PRODUCT_TYPE_HOME_DOOR_ID:          // 0x04
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_DOOR_STATUS, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_DOOR_STATUS;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_POWERPLUG_ID:          // 0x05
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_POWER_PLUG_STATE, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_POWER_PLUG_STATE;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_VOLTAGE_RMS, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 2)
                {
                    uint16_t *pValue = (uint16_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_VOLTAGE_RMS;
                    pDataValue->ValueType = VALUE_TYPE_UINT16;
                    pDataValue->UINT16 = *pValue;

                    index++;
                }
            }
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_POWER_IN_WATT, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 4)
                {
                    float *pValue = (float *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_POWER_IN_WATT;
                    pDataValue->ValueType = VALUE_TYPE_FLOAT;
                    pDataValue->FLOAT = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_LIGHTBULB_ID:          // 0x06
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_LIGHT_BULB_DIMMER, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_LIGHT_BULB_DIMMER;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_SMOKE_DETECTOR:        // 0x07
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_SMOKE_DETECTOR_STATUS, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_SMOKE_DETECTOR_STATUS;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_SHOCK_SENSE_DETECTOR:  // 0x08
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_STATUS, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_STATUS;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_SHOCK_SENSE_ZFORCE_THRESHOLD, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 2)
                {
                    uint16_t *pValue = (uint16_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_SHOCK_SENSE_ZFORCE_THRESHOLD;
                    pDataValue->ValueType = VALUE_TYPE_UINT16;
                    pDataValue->UINT16 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_INLET_SWITCH_ID:       // 0x09
            if (BleManagerDeviceUtils_GetDataTypeValue(pDevInfoReport, DK_DATA_TYPE_INLET_SWITCH_STATUS, &DataLenOut, DataOut) == DEV_MANAGER_SUCCESS)
            {
                if (DataLenOut == 1)
                {
                    uint8_t *pValue = (uint8_t *)&DataOut[0];

                    PeripheralDataType_t *pDataValue = &pDataValueList[index];
                    pDataValue->DataTypeID = DK_DATA_TYPE_INLET_SWITCH_STATUS;
                    pDataValue->ValueType = VALUE_TYPE_UINT8;
                    pDataValue->UINT8 = *pValue;

                    index++;
                }
            }
            break;
        case DK_PRODUCT_TYPE_GATEWAY_PERIPHERAL_ID: // 0x20
            break;
        case DK_PRODUCT_TYPE_GATEWAY_BROADCASTER_ID:// 0x21
            break;
        default:
            break;
        }
    }

    DKPeripheralList_Save(hPeripheralDataList, &PeripheralDataHeader, pDataValueList, PeripheralDataHeader.TotalDataValueList);

    free(pDataValueList);
    pDataValueList = NULL;
}
