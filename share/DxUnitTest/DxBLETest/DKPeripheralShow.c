//============================================================================
// File: DKPeripheralShow.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/08/07
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/02
//     1) Description: Support motion sensor
//        Modified:
//            - AdShowTable structure
//            - AggregateShowTable structure
//            - DKPeripheralShow_AggregateData()
//
//     Version: 0.3
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.4
//     Date: 2016/03/09
//     1) Description: Due to RTK platform doesn't support '%f' format while using vsnprintf(),
//                     we need to using snprintf() to print to a buffer before using dxSysDebugger
//        Added:
//            - VPRINT()
//        Modified:
//            - DKPeripheralShow_AdData()
//            - DKPeripheralShow_AggregateData()
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "DKPeripheralShow.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct _tagPeripheralShowAdTable
{
    uint8_t     DataTypeID;
    char        *pTitle;
    char        *pFormat;
    float       unit;
} PeripheralShowAdTable_t;

typedef struct _tagPeripheralShowAggregateTable
{
    uint16_t     DataID;
    char        *pTitle;
    char        *pFormat;
    float       unit;
} PeripheralShowAggregateTable_t;

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/
static const PeripheralShowAdTable_t AdShowTable[] =
{
    // Why using "%%%%" in printf formater?
    // In case of "%%", after calling snprintf(), the result will be single "%" character.
    // That will cause vsnprintf() fail to parse formater.
    { DK_DATA_TYPE_TEMPERATURE,                     "Temperature in Celsius: ",         "%.2f",             0.01 },
    { DK_DATA_TYPE_ATMOSPHERIC_PRESSURE,            "Atmospheric Pressure: ",           "%.1f hPa",         0.1 },
    { DK_DATA_TYPE_RELATIVE_HUMILITY,               "Relative Humidity: ",              "%.2f %%%%",        0.01 },
    { DK_DATA_TYPE_SHORT_RELATIVE_HUMILITY,         "Short Relative Humidity: ",        "%.0f %%%%",        1 },
    { DK_DATA_TYPE_AIR_QUALITY_VOC,                 "Air Quality (VOC): ",              "%.0f ppm",         1 },
    { DK_DATA_TYPE_DOOR_STATUS,                     "Door Status (Closed:1, Open:2): ", "%.0f",             1 },
    { DK_DATA_TYPE_POWER_PLUG_STATE,                "Power Plug State (Off:0, On:1): ", "%.0f",             1 },
    { DK_DATA_TYPE_VOLTAGE_RMS,                     "Voltage: ",                        "%.2f V",           0.01 },
    { DK_DATA_TYPE_POWER_IN_WATT,                   "Power in Watt: ",                  "%f Watt",          1 },
    { DK_DATA_TYPE_AC_SOURCE_FREQUENCY,             "AC Frequency: ",                   "%.0f Hz",          1 },
    { DK_DATA_TYPE_LIGHT_BULB_DIMMER,               "Light Bulb Dimmer: ",              "%.0f %%%%",        1 },
    { DK_DATA_TYPE_SMOKE_DETECTOR_STATUS,           "Smoke Detector Status: ",          "%.0f",             1 },
    { DK_DATA_TYPE_SHOCK_SENSE_ZFORCE_THRESHOLD,    "Shock Sensor Threshold: ",         "%.0f mg",          1 },
    { DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_STATUS,     "Shock Sensor Status: ",            "%.0f",             1 },
    { DK_DATA_TYPE_MOTION_SENSE_STATUS,             "Motion Sensor State: ",            "%.0f",             1 },
    { DK_DATA_TYPE_MOTION_SENSE_SENSITIVITY,        "Motion Sensor Sensitivity: ",      "%.0f",             1 },
    { DK_DATA_TYPE_MOTION_SENSE_TRIGGER_DELAY_CTRL, "Motion Sensor Trigger Delay: ",    "%.0f second(s)",   1 }
};

static const PeripheralShowAggregateTable_t AggregateShowTable[] =
{
    { 1,        "Joules: ",                             "%f",           1 },
    { 10,       "Temperature in Celsius: ",             "%f",           1 },
    { 11,       "Humidity: ",                           "%f %%%%",      1 },
    { 12,       "Atmospheric Pressure: ",               "%f psi",       1 },
    { 20,       "Motion Frequency: ",                   "%.0f",         1 },
    { 21,       "Motion Activity Duration: ",           "%.0f",         1 },
};

/******************************************************
 *               Function Definitions
 ******************************************************/
void VPRINT(const char *fmt, float value)
{
    uint8_t szPrintBuf[128] = {0};
    int cbBufSize = sizeof(szPrintBuf);
    int n = 0;

    n = snprintf((char*)szPrintBuf, cbBufSize, fmt, value);

    if (n > 0)
    {
        G_SYS_DBG_LOG_NV(szPrintBuf);
    }
}

void DKPeripheralShow_AdData(PeripheralDataType_t *pValue)
{
    int i = 0;
    int count = sizeof(AdShowTable) / sizeof(AdShowTable[0]);

    for (i = 0; i < count; i++)
    {
        if (AdShowTable[i].DataTypeID == pValue->DataTypeID)
        {
            // Found!!!
            NPRINT(AdShowTable[i].pTitle);

            switch(pValue->ValueType)
            {
            case VALUE_TYPE_INT8:
                VPRINT(AdShowTable[i].pFormat, pValue->INT8 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_UINT8:
                VPRINT(AdShowTable[i].pFormat, pValue->UINT8 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_INT16:
                VPRINT(AdShowTable[i].pFormat, pValue->INT16 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_UINT16:
                VPRINT(AdShowTable[i].pFormat, pValue->UINT16 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_INT32:
                VPRINT(AdShowTable[i].pFormat, pValue->INT32 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_UINT32:
                VPRINT(AdShowTable[i].pFormat, pValue->UINT32 * AdShowTable[i].unit);
                break;
            case VALUE_TYPE_FLOAT:
                VPRINT(AdShowTable[i].pFormat, pValue->FLOAT * AdShowTable[i].unit);
                break;
            default:
                break;
            }

            break;
        }
    }
}

void DKPeripheralShow_AggregateData(AggregatedHeaderFormatInfo_t AgHFormat, void *pAgD)//(AggregateItem_t *pAgItem)
{
    int i = 0;
    int count = sizeof(AggregateShowTable) / sizeof(AggregateShowTable[0]);

    if (pAgD == NULL) return ;

    for (i = 0; i < count; i++)
    {
        if (AgHFormat.bits.AfDFormat_b0_3 == 0)
        {
            AggregateItem_t *pAgItem = (AggregateItem_t *)pAgD;

            if (AggregateShowTable[i].DataID == pAgItem->nDataID)
            {
                float value = 0;
                memcpy(&value, &pAgItem->nDataValue, sizeof(value));

                // Found!!!
                NPRINT(AggregateShowTable[i].pTitle);
                VPRINT(AggregateShowTable[i].pFormat, value * AggregateShowTable[i].unit);
                VPRINT(", Duration: %u\n", pAgItem->nDuration);
                break;
            }
        }
        else if (AgHFormat.bits.AfDFormat_b0_3 == 1)
        {
            AggregateItemFormat1_t *pAgItem = (AggregateItemFormat1_t *)pAgD;

            if (AggregateShowTable[i].DataID == pAgItem->nDataID)
            {
                // Found!!!
                NPRINT("Format of Time: %d, Time: %u, ", AgHFormat.bits.TimeFormat_b7, (unsigned int)pAgItem->nTime);

                NPRINT(AggregateShowTable[i].pTitle);

                switch(pAgItem->nType)
                {
                case VALUE_TYPE_INT8:
                case VALUE_TYPE_UINT8:
                {
                    uint8_t value = 0;
                    memcpy(&value, &pAgItem->nDataValue, sizeof(value));
                    VPRINT(AggregateShowTable[i].pFormat, value * AggregateShowTable[i].unit);
                }
                    break;
                case VALUE_TYPE_INT16:
                case VALUE_TYPE_UINT16:
                {
                    uint16_t value = 0;
                    memcpy(&value, &pAgItem->nDataValue, sizeof(value));
                    VPRINT(AggregateShowTable[i].pFormat, value * AggregateShowTable[i].unit);
                }
                    break;
                case VALUE_TYPE_INT32:
                case VALUE_TYPE_UINT32:
                {
                    uint32_t value = 0;
                    memcpy(&value, &pAgItem->nDataValue, sizeof(value));
                    VPRINT(AggregateShowTable[i].pFormat, value * AggregateShowTable[i].unit);
                }
                    break;
                case VALUE_TYPE_FLOAT:
                {
                    float value = 0;
                    memcpy(&value, &pAgItem->nDataValue, sizeof(value));
                    VPRINT(AggregateShowTable[i].pFormat, value * AggregateShowTable[i].unit);
                }
                    break;
                default:
                    NPRINT("(Unknown type");
                    break;
                }

                VPRINT(", Duration: %u\n", pAgItem->nDuration);
                break;
            }
        }
    }
}
