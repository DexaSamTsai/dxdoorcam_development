//============================================================================
// File: DKPeripheralDataList.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/08/07
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "DKPeripheralDataList.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct _tagDKPeripheralList DKPeripheralList_t;

typedef struct _tagDKPeripheralList
{
    DKPeripheralList_t      *Next;
    UIPeripheralAdData_t    *pEntry;
} DKPeripheralList_t;

typedef struct _tagDKListHandle
{
    DKPeripheralList_t  *pDKListHead;
    DKPeripheralList_t  *pDKListTail;
} DKPeripheralListHandle_t;

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int DKPeripheralList_Init(void)
{
    DKPeripheralListHandle_t *handle = calloc(1, sizeof(DKPeripheralListHandle_t));
    return (int)handle;
}

void DKPeripheralList_Empty(int handle)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    DKPeripheralList_t *p = NULL;

    if (h == NULL) return;

    p = h->pDKListHead;
    while (p)
    {
        UIPeripheralAdData_t *q = p->pEntry;

        if (q)
        {
            free(q);
            q = NULL;
        }

        h->pDKListHead = h->pDKListHead->Next;
        free(p);
        p = h->pDKListHead;
    }

    h->pDKListHead = NULL;
    h->pDKListTail = NULL;
}

void DKPeripheralList_Save(int handle, UIPeripheralAdDataHeader_t *pHeader, PeripheralDataType_t *pValueList, int NumOfValueList)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    uint8_t found = 0;

    if (pValueList == NULL)
    {
        NumOfValueList = 0;
    }

    if (pHeader == NULL ||
        h == NULL)
    {
        return ;
    }

    DKPeripheralList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        if (memcmp(pNode->pEntry->Header.DevAddress.Addr, pHeader->DevAddress.Addr, sizeof(pHeader->DevAddress.Addr)) == 0)
        {
            // found
            found = 1;
            pNode->pEntry->Header.ProductID = pHeader->ProductID;
            pNode->pEntry->Header.rssi = pHeader->rssi;
            pNode->pEntry->Header.Flag = pHeader->Flag;
            pNode->pEntry->Header.BatteryInfo = pHeader->BatteryInfo;

            if (NumOfValueList != pNode->pEntry->Header.TotalDataValueList)
            {
                free(pNode->pEntry->pDataValueList);
                pNode->pEntry->pDataValueList = NULL;

                pNode->pEntry->Header.TotalDataValueList = 0;
            }

            if (pNode->pEntry->Header.TotalDataValueList == 0)
            {
                pNode->pEntry->pDataValueList = calloc(1, sizeof(PeripheralDataType_t) * NumOfValueList);
            }

            if (pNode->pEntry->pDataValueList)
            {
                PeripheralDataType_t *pDataEntry = (PeripheralDataType_t *)&(pNode->pEntry->pDataValueList[0]);
                memcpy(pDataEntry, pValueList, sizeof(PeripheralDataType_t) * NumOfValueList);
                pNode->pEntry->Header.TotalDataValueList = NumOfValueList;
            }

            break;
        }

        pNode = pNode->Next;
    }

    if (found == 0)
    {
        DKPeripheralList_t *pDataToSave = calloc(1, sizeof(DKPeripheralList_t));
        if (pDataToSave == NULL)
        {
            return ;
        }

        pDataToSave->pEntry = calloc(1, sizeof(UIPeripheralAdData_t));
        if (pDataToSave->pEntry == NULL)
        {
            free(pDataToSave);
            pDataToSave = NULL;
            return ;
        }

        memcpy(pDataToSave->pEntry->Header.DevAddress.Addr, pHeader->DevAddress.Addr, sizeof(pHeader->DevAddress.Addr));
        pDataToSave->pEntry->Header.ProductID = pHeader->ProductID;
        pDataToSave->pEntry->Header.rssi = pHeader->rssi;
        pDataToSave->pEntry->Header.Flag = pHeader->Flag;
        pDataToSave->pEntry->Header.BatteryInfo = pHeader->BatteryInfo;
        pDataToSave->pEntry->pDataValueList = calloc(1, sizeof(PeripheralDataType_t) * NumOfValueList);
        if (pDataToSave->pEntry->pDataValueList == NULL)
        {
            free(pDataToSave->pEntry);
            pDataToSave->pEntry = NULL;
            free(pDataToSave);
            pDataToSave = NULL;
            return ;
        }

        PeripheralDataType_t *pDataEntry = (PeripheralDataType_t *)&(pDataToSave->pEntry->pDataValueList[0]);
        memcpy(pDataEntry, pValueList, sizeof(PeripheralDataType_t) * NumOfValueList);
        pDataToSave->pEntry->Header.TotalDataValueList = NumOfValueList;

        if (h->pDKListTail == NULL)
        {
            h->pDKListTail = pDataToSave;
            h->pDKListTail->Next = NULL;
            h->pDKListHead = pDataToSave;
            h->pDKListHead->Next = NULL;
        }
        else
        {
            h->pDKListTail->Next = pDataToSave;
            h->pDKListTail = pDataToSave;
        }
    }
}

void DKPeripheralList_Remove(int handle, DevAddress_t *pDevAddr)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL ||
        pDevAddr == NULL)
    {
        return ;
    }

    DKPeripheralList_t *pNode = h->pDKListHead;
    DKPeripheralList_t *pPreNode = NULL;
    while (pNode)
    {
        if (memcmp(pNode->pEntry->Header.DevAddress.Addr, pDevAddr->Addr, sizeof(pDevAddr->Addr)) == 0)
        {
            // found
            if (pPreNode == NULL)
            {
                // We are removing the top element in DKPeripheralList_t
                pPreNode = pNode;
                h->pDKListHead = pNode->Next;

                DKPeripheralList_Free(pPreNode->pEntry);
                free(pPreNode);
                pPreNode = NULL;
            }
            else
            {
                pPreNode->Next = pNode->Next;

                DKPeripheralList_Free(pNode->pEntry);
                free(pNode);
                pNode = NULL;
            }

            break;
        }

        pPreNode = pNode;
        pNode = pNode->Next;
    }
}

const UIPeripheralAdData_t *DKPeripheralList_GetAt(int handle, int index)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    UIPeripheralAdData_t *pEntry = NULL;
    int i = 0;

    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL)
    {
        return NULL;
    }

    DKPeripheralList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        if (i == index)
        {
            pEntry = pNode->pEntry;
            break;
        }

        pNode = pNode->Next;
        i++;
    }

    return pEntry;
}

size_t DKPeripheralList_Count(int handle)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    size_t count = 0;

    if (h == NULL ||
        h->pDKListHead == NULL ||
        h->pDKListTail == NULL)
    {
        return 0;
    }

    DKPeripheralList_t *pNode = h->pDKListHead;
    while (pNode)
    {
        count++;

        pNode = pNode->Next;
    }

    return count;
}

void DKPeripheralList_Uninit(int handle)
{
    DKPeripheralListHandle_t *h = (DKPeripheralListHandle_t *)handle;
    if (h)
    {
        DKPeripheralList_Empty(handle);
        h->pDKListHead = NULL;
        h->pDKListTail = NULL;

        free(h);
        h = NULL;
    }
}

void DKPeripheralList_Free(UIPeripheralAdData_t *pEntry)
{
    if (pEntry)
    {
        if (pEntry->pDataValueList)
        {
            free(pEntry->pDataValueList);
            pEntry->pDataValueList = NULL;
        }

        free(pEntry);
        pEntry = NULL;
    }
}
