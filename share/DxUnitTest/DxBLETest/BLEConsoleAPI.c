//============================================================================
// File: BLEConsoleAPI.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/28
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/02
//     1) Description: Support motion sensor
//        Modified:
//            - ControlTable structure
//            - control()
//
//     Version: 0.3
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#include <stdlib.h>
#include <inttypes.h>

#include "Utils.h"
#include "BLEManager.h"
#include "BLEHandler.h"
#include "DKScanList.h"
#include "DKPeripheralDataList.h"
#include "DKPeripheralShow.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagServCharControlTable
{
    char        *pOperationName;
    uint16_t    ServiceUUID;
    uint16_t    CharacteristicUUID;
    uint8_t     DataLen;
} ServCharControlTable;

/******************************************************
 *                    Structures
 ******************************************************/
ServCharControlTable ControlTable[] =
{
    { "PowerPlug On/Off",           DK_SERVICE_POWER_PLUG_UUID,     DK_CHARACTERISTIC_POWER_CONFIG_UUID,                                    4 },
    { "LightBulb Dimmer",           DK_SERVICE_LIGHT_BULB_UUID,     DK_CHARACTERISTIC_LIGHT_BULB_DIMMER_UUID,                               1 },
    { "Smoke Dector Alarm",         DK_SERVICE_SMOKE_DETECTOR_UUID, DK_CHARACTERISTIC_ALART_CONTROL_UUID,                                   2 },
    { "Shock Dector Threshold",     0xFAB0,                         0xFBB0,                                                                 2 },
    { "Inlet Switch On/Off",        DK_SERVICE_INLET_SWITCH_UUID,   DK_CHARACTERISTIC_INLETSWITCH_CONTROL_UUID,                             1 },
    { "Aggregate Data",             DK_SERVICE_SYSTEM_UTILS_UUID,   DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID, 0 },
    { "Motion Sensor Sensitivity",  DK_SERVICE_MOTION_SENSOR_UUID,  DK_CHARACTERISTIC_MOTION_SENSITIVITY_CONTROL_UUID,                      1 },
    { "Motion Sensor Trigger Delay",DK_SERVICE_MOTION_SENSOR_UUID,  DK_CHARACTERISTIC_MOTION_DELAY_CONTROL_UUID,                            1 },
};

/******************************************************
 *               Static Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
void disp_operation_command(void)
{
    NPRINT("-------+----------------------+------+------+--------\n");
    NPRINT("       |                      | Serv | Char |  Data\n");
    NPRINT(" Index | Operation Name       | UUID | UUID | Length\n");
    NPRINT("-------+----------------------+------=------+--------\n");

    int i = 0;
    for (i = 0; i < sizeof(ControlTable) / sizeof(ControlTable[0]); i++)
    {
        NPRINT(" %-5d | %-20s | %04X | %04X | %-3d\n",
               i,
               ControlTable[i].pOperationName,
               (unsigned int)(ControlTable[i].ServiceUUID & 0x0FFFF),
               (unsigned int)(ControlTable[i].CharacteristicUUID & 0x0FFFF),
               (unsigned int)ControlTable[i].DataLen);
    }
}

int clear_scan(int argc, char *argv[])
{
    int handle = MACAddrList_GetHandle();
    if (handle)
    {
        DKScanList_Empty(handle);
    }

    return 0;
}

int clear_info(int argc, char *argv[])
{
    int handle = PeripheralDataList_GetHandle();
    if (handle)
    {
        DKPeripheralList_Empty(handle);
    }

    return 0;
}

int disp_scan(int argc, char *argv[])
{
    int i = 0;
    int handle = MACAddrList_GetHandle();
    if (handle)
    {
        int n = DKScanList_Count(handle);

        NPRINT("+-----+-------------------------+-----+--------+----------+\n");
        NPRINT("|     |                         |     | Paired | Security |\n");
        NPRINT("| Idx | MAC Address             | PID | 1-YES  | 0-YES    |\n");
        NPRINT("|     |                         |     | 2-NO   | 1-NO     |\n");
        NPRINT("+-----+-------------------------+-----+--------+----------+\n");

        for (i = 0; i < n; i++)
        {
            UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, i);
            NPRINT("| %-3d | %02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X | %-3d | %-6d | %-8d |\n",
                   i,
                   pEntry->DevAddress.Addr[0],
                   pEntry->DevAddress.Addr[1],
                   pEntry->DevAddress.Addr[2],
                   pEntry->DevAddress.Addr[3],
                   pEntry->DevAddress.Addr[4],
                   pEntry->DevAddress.Addr[5],
                   pEntry->DevAddress.Addr[6],
                   pEntry->DevAddress.Addr[7],
                   (int)pEntry->ProductID,
                   pEntry->PairFlag,
                   pEntry->EncryptFlag);
            dxTimeDelayMS(100);
        }
    }

    return 0;
}

int scan_all(int argc, char *argv[])
{
    int nSeconds = atoi(argv[1]);

    BLEManager_result_t result = BleManagerStartReportingPeripheral();
    if (result != DEV_MANAGER_SUCCESS)
    {
        NPRINT("Error BleManagerStartReportingPeripheral\n");

        return result;
    }

    if (nSeconds > 0)
    {
        dxTimeDelayMS(nSeconds * 1000);

        BleManagerStopReportingPeripheral();
    }

    return result;
}

int scan_paired(int argc, char *argv[])
{
    int handle = PeripheralDataList_GetHandle();

    if (handle == 0)
    {
        NPRINT("Something wrong!\n");
        return 0;
    }

    int count = DKPeripheralList_Count(handle);
    if (count <= 0)
    {
        NPRINT("Please wait for data ready!\n");
        return 0;
    }

    int i = 0;
    int j = 0;
    for (i = 0; i < count; i++)
    {
        const UIPeripheralAdData_t *pPeripheralData = DKPeripheralList_GetAt(handle, i);
        if (pPeripheralData == NULL)
        {
            continue;
        }

        NPRINT("MAC Address: %02X:%02X:%02X:%02X:%02X:%02X:%02X:%02X, ProductID: %d, RSSI: %d, Flag: 0x%X, BatteryInfo: %d %%\n",
               pPeripheralData->Header.DevAddress.Addr[0],
               pPeripheralData->Header.DevAddress.Addr[1],
               pPeripheralData->Header.DevAddress.Addr[2],
               pPeripheralData->Header.DevAddress.Addr[3],
               pPeripheralData->Header.DevAddress.Addr[4],
               pPeripheralData->Header.DevAddress.Addr[5],
               pPeripheralData->Header.DevAddress.Addr[6],
               pPeripheralData->Header.DevAddress.Addr[7],
               pPeripheralData->Header.ProductID,
               pPeripheralData->Header.rssi,
               pPeripheralData->Header.Flag,
               pPeripheralData->Header.BatteryInfo);

        for (j = 0; j < pPeripheralData->Header.TotalDataValueList; j++)
        {
            PeripheralDataType_t *pDataValue = &pPeripheralData->pDataValueList[j];
            NPRINT("\t");
            DKPeripheralShow_AdData(pDataValue);
            NPRINT("\n");
        }

        NPRINT("\n");

        dxTimeDelayMS(100);
    }

    return 0;
}

int scan_open_pair(int argc, char *argv[])
{
    int nSeconds = atoi(argv[1]);

    BLEManager_result_t result = BleManagerStartReportingUnPairPeripheral();
    if (result != DEV_MANAGER_SUCCESS)
    {
        NPRINT("Error BleManagerStartReportingUnPairPeripheral\n");

        return result;
    }

    if (nSeconds > 0)
    {
        dxTimeDelayMS(nSeconds * 1000);

        BleManagerStopReportingUnPairPeripheral();
    }

    return result;
}

int pair(int argc, char *argv[])
{
    BLEManager_result_t result = DEV_MANAGER_INVALID_PARAMETER;
    int handle = MACAddrList_GetHandle();
    int index = atoi(argv[1]);
    stPairingInfo_t PairingInfo;

    UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, index);
    uint8_t *pPINCODE = (uint8_t *)argv[2];

    if (pEntry && strlen((char *)pPINCODE) <= sizeof(PairingInfo.SecurityCode))
    {
        memset(&PairingInfo, 0x00, sizeof(PairingInfo));
        memcpy(PairingInfo.Addr, pEntry->DevAddress.Addr, sizeof(PairingInfo.Addr));
        memcpy(PairingInfo.SecurityCode, pPINCODE, strlen((char *)pPINCODE));

        result = BleManagerPairDeviceTask_Asynch(&PairingInfo, 10000, 0, 0);
    }

    return result;
}

int unpair(int argc, char *argv[])
{
    BLEManager_result_t result = DEV_MANAGER_INVALID_PARAMETER;
    int handle = MACAddrList_GetHandle();
    int index = atoi(argv[1]);

    UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, index);

    if (pEntry)
    {
        result = BleManagerUnPairDeviceTask_Asynch(pEntry->DevAddress, 10000, 0, 0);
    }

    return result;
}

int renew(int argc, char *argv[])
{
    BLEManager_result_t result = DEV_MANAGER_INVALID_PARAMETER;
    int handle = MACAddrList_GetHandle();
    int index = atoi(argv[1]);
    stRenewSecurityInfo_t SecurityInfo;

    UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, index);
    uint8_t *pPINCODE = (uint8_t *)argv[2];

    if (pEntry && strlen((char *)pPINCODE) <= sizeof(SecurityInfo.SecurityCode))
    {
        memset(&SecurityInfo, 0x00, sizeof(SecurityInfo));
        memcpy(SecurityInfo.Addr, pEntry->DevAddress.Addr, sizeof(SecurityInfo.Addr));
        memcpy(SecurityInfo.SecurityCode, pPINCODE, strlen((char *)pPINCODE));

        result = BleManagerSetRenewSecurityTask_Asynch(&SecurityInfo, 10000, 0, 0);
    }

    return result;
}

int clear_security(int argc, char *argv[])
{
    BLEManager_result_t result = DEV_MANAGER_INVALID_PARAMETER;
    int handle = MACAddrList_GetHandle();
    int index = atoi(argv[1]);

    UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, index);

    if (pEntry)
    {
        result = BleManagerClearSecurityTask_Asynch(pEntry->DevAddress, 10000, 0, 0);
    }

    return result;
}

int control(int argc, char *argv[])
{
    BLEManager_result_t result = DEV_MANAGER_INVALID_PARAMETER;
    int handle = MACAddrList_GetHandle();

    if (argc < 3 ||
        argc > 5)
    {
        return result;
    }

    int index = atoi(argv[1]);

    UIDeviceEntry_t *pEntry = DKScanList_GetAt(handle, index);

    if (pEntry == NULL)
    {
        NPRINT("Device not found! please run scan first!\n");
        return result;
    }

    int opindex = atoi(argv[2]);
    if (opindex < 0 ||
        opindex > sizeof(ControlTable) / sizeof(ControlTable[0]))
    {
        disp_operation_command();
        return result;
    }

    int nTotalDataLength = sizeof(BleStandardJobExecHeader_t) + ControlTable[opindex].DataLen;

    // Point to "Header & Body"
    uint8_t *pDataHeaderBody = (uint8_t *)calloc(1, nTotalDataLength);
    if (pDataHeaderBody == NULL)
    {
        NPRINT("Failed to call calloc()!\n");
        return result;
    }

    // Prepare "Header"
    BleStandardJobExecHeader_t *pHeaderPart = (BleStandardJobExecHeader_t *)pDataHeaderBody;

    memcpy(pHeaderPart->PeripheralDeviceMacAddress.Addr, pEntry->DevAddress.Addr, sizeof(pEntry->DevAddress.Addr));
    pHeaderPart->CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
    pHeaderPart->ServiceUUID = ControlTable[opindex].ServiceUUID;
    pHeaderPart->CharacteristicUUID = ControlTable[opindex].CharacteristicUUID;
    pHeaderPart->DataLen = ControlTable[opindex].DataLen;

    // Point to "Body"
    uint8_t *pDataBody = &pDataHeaderBody[sizeof(BleStandardJobExecHeader_t)];

    switch(opindex)
    {
    case 0: // Power Plug On/Off
    {
        // Prepare Body. In this case, Body is type of UINT32
        uint32_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    case 1: // LightBulb Dimmer
    {
        // Prepare Body. In this case, Body is type of UINT8
        uint8_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    case 2: // Smoke Dector Alarm
    {
        // Prepare Body. In this case, Body is type of UINT16
        uint16_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    case 3: //"Shock Detector Threshold"
    {
        // Prepare Body. In this case, Body is type of UINT16
        uint16_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
        //pHeaderPart->CharFlag.bits.ReadWriteMode_b1_3 = WRITE_ONLY;
    }
        break;
    case 4: // Inlet Switch On/Off
    {
        // Prepare Body. In this case, Body is type of UINT8
        uint8_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    case 5: // "Aggregate Data"
    {
        pHeaderPart->CharFlag.bits.ReadWriteMode_b1_3 = READ_FOLLOW_BYAGGREGATIONUPDATE;
    }
        break;
    case 6: // "Motion Sensor Sensitivity"
    {
        // Prepare Body. In this case, Body is type of UINT8. Value is in range 0 to 4. Others is illegal.
        uint8_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    case 7: // "Motion Sensor Trigger Delay"
    {
        // Prepare Body. In this case, Body is type of UINT8. Value is in range 0 to 4. Others is illegal.
        uint8_t Value = atoi(argv[3]);
        memcpy(pDataBody, &Value, sizeof(Value));
    }
        break;
    default:
        break;
    }

    result = BleManagerDeviceAccessWriteTaskExt_Asynch(pDataHeaderBody, 10000, 0, 0);

    free(pDataHeaderBody);
    pDataHeaderBody = NULL;

    return result;
}