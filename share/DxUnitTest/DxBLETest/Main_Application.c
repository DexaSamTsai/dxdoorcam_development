//============================================================================
// File: Dexatek_Application.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "BLEHandler.h"
#include "BLEConsoleAPI.h"

//============================================================================
// Constants
//============================================================================
#define DKTHREAD_PRIORITY                           (DXTASK_PRIORITY_NORMAL + 1)
#define DKTHREAD_NAME                               "APPM"
#define DKTHREAD_STACK_SIZE                         4 * 1280
#define MAX_ARGC                                    10

#define GPIO_A                                      3
#define GPIO_B                                      4

//============================================================================
// Typedef
//============================================================================

typedef void (*MenuFunctionCall_t)(void *arg);

typedef struct
{
    char                MenuName[64];
    MenuFunctionCall_t  MenuFunc;
    
} MenuItems_t;

//============================================================================
// Function declaration
//============================================================================
void fBTCS(void *arg);
void fBTCI(void *arg);
void fBTDS(void *arg);
void fBTSA(void *arg);
void fBTSP(void *arg);
void fBTSO(void *arg);
void fBTPP(void *arg);
void fBTUP(void *arg);
void fBTRN(void *arg);
void fBTCK(void *arg);
void fBTCP(void *arg);
void fBTMD(void *arg);
void fBTLD(void *arg);

//============================================================================
// Global Variable
//============================================================================

static dxGPIO_t TestGPIOConfig[] =
{
    {
        NULL,
        GPIO_A,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        GPIO_B,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    
    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};


static MenuItems_t at_ble_items[] = {
    
    {"BleScanResultClear", fBTCS,},       // BLE clear scan - "BleScanResultClear", claer the list of saved scan result.
    
    {"BlePairListClear", fBTCI,},	    // BLE clear information - "BlePairListClear", claer the list of paired peripheral.
    
    {"BleScanResult", fBTDS,},       // BLE display scan result - "BleScanResult", show all previously scan result
    
    {"BleScanAll", fBTSA,}, // BLE scan all peripheral - "BleScanAll=N", Where N is number of seconds to scan
    
    {"BleGetPaired", fBTSP,},       // BLE get all paired peripheral -  "BleGetPaired", get all paired peripheral list
    
    {"BleScanOpenPair", fBTSO,},       // BLE scan open pair peripheral -  "BleScanOpenPair=N", Where N is number of seconds to scan open pair peripherals
    
    {"BlePairDev", fBTPP,},       // BLE pair peripheral - "BlePairDev=Index,PinCode", Where Index is the Idx from scan result, and PinCode is 6 character pincode of the peripheral (eg, ABC123)
    
    {"BleUnpair", fBTUP,},       // BLE unpair peripheral - "BleUnpair=Index", Where Index is the Idx from scan result to unpair the peripheral (if it was already paired).
    
    {"BleSecurityKeyRenew", fBTRN,},       // BLE renew security key - "BleSecurityKeyRenew=Index,PinCode", Where Index is the Idx from scan result, and PinCode is 6 character pincode of the peripheral (eg, ABC123) to renew the security
    
    {"BleSecurityClear", fBTCK,},       // BLE clear security - "BleSecurityClear=Index", Where Index is the Idx from scan result to clear the security of the peripheral (if it was already paired).
    
    {"BleDevControl", fBTCP,},       // BLE peripheral control - "BleDevControl=Index,OpCode,ValueToWrite", Where Index is the Idx from scan result, OpCode is the operational code to control the devices (list all OpCode by "BleDevControl 0,-1,0"), and ValueToWrite is the value written to the devices (see Peripheral document for detail).
    
    {"BLEInit", fBTMD,},    // BLE Centrol RESET (BLE's RESET pin) - "BLEInit=2", Launch BLE Manager.
    
    {"BleGpioTest", fBTLD,},       // BLE GPIO Test (Internal) - "BleGpioTest=N", Where N is the GPIO level (eg, 0 LEVEL_LOW, 1 LEVEL_HIGH)
    
};

//============================================================================
// Util()
//============================================================================

int ConverStrToLowerCase(char* StringToConvert, int SizeofStr)
{
    int i = 0;
    for(i = 0; i < SizeofStr; i++){
        StringToConvert[i] = tolower(StringToConvert[i]);
    }
    
    return 1;
}

int parse_param(char *buf, char **argv)
{
    int argc = 1;
    while((argc < MAX_ARGC) && (*buf != '\0')) {
        while((*buf == '[')||(*buf == ' '))
            buf ++;
        if((*buf == ']')||(*buf == '\0'))
            break;
        argv[argc] = buf;
        argc ++;
        buf ++;
        
        while((*buf != ',')&&(*buf != '[')&&(*buf != ']')&&(*buf != '\0'))
            buf ++;
        
        while((*buf == ',')||(*buf == '[')||(*buf == ']')) {
            *buf = '\0';
            buf ++;
        }
    }
    return argc;
}



void HEX_Dump_Data(uint8_t *pData, int len)
{
    char szPrintBuff[64];
    int i = 0;
    memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
    
    while (i < len)
    {
        sprintf(szPrintBuff, "%s%02X", szPrintBuff, (pData[i] & 0x0FF));
        i++;
        if (((i % 16) == 0) ||
            (i == len))
        {
            strcat(szPrintBuff, "\n");
            
            dxTimeDelayMS(20);
            
            G_SYS_DBG_LOG_NV(szPrintBuff);
            memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        }
        else
        {
            strcat(szPrintBuff, " ");
        }
    }
}

int GetUserInput(char* userInputBuff, int SizeOfuserInputBuff)
{
    int  TotalChar = 0;
    char userChar;
    memset(userInputBuff, 0, SizeOfuserInputBuff);
    char* pUserInputBuff = userInputBuff;
    while ((userChar = getchar()) != 0x0A)
    {
        *pUserInputBuff = userChar;
        pUserInputBuff++;
        TotalChar++;
    }
    
    return TotalChar;
}

int GetCommandAndParameterPointer(char* userInput, char** pointerToCommand, char** PointerToParam)
{
    
    unsigned int UserInputIndex = 0;
    while (*(userInput + UserInputIndex) != '\0')
    {
        if (*(userInput + UserInputIndex) == '=')
        {
            *PointerToParam = userInput + UserInputIndex + 1;
            
            *pointerToCommand = userInput;
            *(userInput + UserInputIndex) = '\0';
            break;
        }
        
        UserInputIndex++;
    }
    
    if ((*PointerToParam == NULL) && (UserInputIndex != 0))
    {
        *pointerToCommand = userInput;
    }
    
    return 0;
}


MenuFunctionCall_t GetMenuFunctionCall(char* FuncCallName)
{
    
    MenuFunctionCall_t FuncCall = NULL;
    
    if (FuncCallName)
    {
        uint16_t i = 0;
        for (i = 0; i < (sizeof(at_ble_items)/sizeof(MenuItems_t)); i++)
        {
            if (strcasecmp(FuncCallName, at_ble_items[i].MenuName) == 0)
            {
                FuncCall = at_ble_items[i].MenuFunc;
                break;
            }
        }
    }
    
    return FuncCall;
}

//============================================================================
// Application_Main()
//============================================================================
// BLE clear scan
void fBTCS(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    clear_scan(argc, argv);
}

// BLE clear information
void fBTCI(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    clear_info(argc, argv);
}

// BLE display scan result
void fBTDS(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    disp_scan(argc, argv);
}

// BLE scan all peripheral
void fBTSA(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    scan_all(argc, argv);
}

// BLE scan paired peripheral
void fBTSP(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    scan_paired(argc, argv);
}

// BLE scan open pair peripheral
void fBTSO(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    scan_open_pair(argc, argv);
}

// BLE pair peripheral
void fBTPP(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    pair(argc, argv);
}

// BLE unpair peripheral
void fBTUP(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    unpair(argc, argv);
}

// BLE renew
void fBTRN(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    renew(argc, argv);
}

// BLE clear security
void fBTCK(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    clear_security(argc, argv);
}

// BLE peripheral control
void fBTCP(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    if (arg)
    {
        strcpy(buf, arg);
        argc = parse_param(buf, argv);
    }
    
    control(argc, argv);
}

// BLE Centrol RESET (BLE's RESET pin)
void fBTMD(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    strcpy(buf, arg);

    argc = parse_param(buf, argv);


    if (argc == 2) {

        int n = atoi(argv[1]);

        if (n == 0) {

            dxGPIO_Write(DK_BLE_RESET, DXPIN_LOW);
        }
        else if (n == 1) {

            dxGPIO_Write(DK_BLE_RESET, DXPIN_HIGH);
        }
        else {
            BLESDK_Init();
        }
    }
}

// BLE LED test
void fBTLD(void *arg)
{
    char buf[64] = {0};
    int argc = 0;
    char *argv[MAX_ARGC];

    strcpy(buf, arg);

    argc = parse_param(buf, argv);


    if (argc == 2)
    {
        int n = atoi(argv[1]);
        
        int l = DXPIN_LOW;

        if (n > 0) {

            l = DXPIN_HIGH;
        }
        
        printf("\n[%s] Pin(GPIO_A): %X, value: %d\n", __FUNCTION__, GPIO_A, l);
        dxGPIO_Write(GPIO_A, l);
        
        dxTimeDelayMS(2000);
        
        printf("\n[%s] Pin(GPIO_B): %X, value: %d\n", __FUNCTION__, GPIO_B, l);
        dxGPIO_Write(GPIO_B, l);

    }
}

int main(void)
{

    dxOS_Init();
    
    dxGPIO_Init(TestGPIOConfig);
    
    GSysDebugger_result_t SysDbgRet = GSysDebuggerInit();
    if (SysDbgRet != G_SYS_DBG_SUCCESS)
    {
        printf("GSysDebuggerInit failed = %d\n", SysDbgRet);
    }

    dxBSP_Init();

    /**
     * How to use BLEManager Control?
     * Start a terminal app and enter the following command
     *
     * 1) BTMD=2
     * 2) BTSA=10
     * 3) BTDS
     * 4) BTPP=n,888888
     *    n should be index value of the result of "BTDS" command
     * 5) BTSP
     */
    
    
    int     userWantOut = 0;
    char    userInputBuff[256];
    while (!userWantOut)
    {
        printf("**Menu**\n");
        printf("'BleScanResultClear', clear the list of saved scan result.n");
        printf("'BlePairListClear', clear the list of paired peripheral.\n");
        printf("'BleScanResult', show all previously scan result\n");
        printf("'BleScanAll=N', Where N is number of seconds to scan\n");
        printf("'BleGetPaired', get all paired peripheral list\n");
        printf("'BleScanOpenPair=N', Where N is number of seconds to scan open pair peripherals\n");
        printf("'BlePairDev=Index,PinCode', Where Index is the Idx from scan result, and PinCode is 6 character pincode of the peripheral (eg, ABC123)\n");
        printf("'BleUnpair=Index', Where Index is the Idx from scan result to unpair the peripheral (if it was already paired).\n");
        printf("'BleSecurityKeyRenew=Index,PinCode', Where Index is the Idx from scan result, and PinCode is 6 character pincode of the peripheral (eg, ABC123) to renew the security\n");
        printf("'BleSecurityClear=Index', Where Index is the Idx from scan result to clear the security of the peripheral (if it was already paired).\n");
        printf("'BleDevControl=Index,OpCode,ValueToWrite', Where Index is the Idx from scan result, OpCode is the operational code to control the devices (list all OpCode by 'BleDevControl 0,-1,0'), and ValueToWrite is the value written to the devices (see Peripheral document for detail).\n");
        printf("'BLEInit=2', Launch BLE Manager.\n");
        printf("'BleGpioTest=N', Internal Test Function, Where N is the GPIO level (eg, 0 LEVEL_LOW, 1 LEVEL_HIGH)\n");
        printf("\n");
        printf("Initialization Sequence (Example)\n");
        printf("1) BLEInit=2 to initializa BLE before any command can be used\n");
        printf("2) BleScanAll=8 to scan all surrounding peripherals for 8 seconds\n");
        printf("3) BleScanResult to list all scanned results\n");
        printf("4) BlePairDev=1,ABC123 to pair peripheral of index 1 from scan result and given PinCode of that device as ABC123\n");
        
        
        printf("Enter Your CMD: ");
        if (GetUserInput(userInputBuff, sizeof(userInputBuff)))
        {
            char* pointerToCommand = NULL;
            char* PointerToParam = NULL;
            GetCommandAndParameterPointer(userInputBuff, &pointerToCommand, &PointerToParam);
            
            //printf("pointerToCommand=%s, PointerToParam=%s\n", pointerToCommand, PointerToParam);
            
            MenuFunctionCall_t MenuFuncCall = GetMenuFunctionCall(pointerToCommand);
            if (MenuFuncCall)
            {
                MenuFuncCall(PointerToParam);
            }
            else
            {
                if (pointerToCommand)
                {
                    if (strcasecmp(pointerToCommand, "Exit") == 0)
                        break;
                    else
                        printf("Unknown Command....\n");
                }
            }
            
        }
    }

    printf("Bye Bye~\n");
    return 0;
}
