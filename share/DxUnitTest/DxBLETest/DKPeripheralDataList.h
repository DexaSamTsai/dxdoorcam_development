//============================================================================
// File: DKPeripheralDataList.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/08/06
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DKPERIPHERALDATALIST_H
#define _DKPERIPHERALDATALIST_H

#pragma once

#ifdef DK_GATEWAY
#endif

#include "BLEManager.h"
#include "dk_Peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagPeripheralDataType
{
    uint8_t                     DataTypeID;
    enum eConditionValueType    ValueType;
    union {
        int8_t                  INT8;
        uint8_t                 UINT8;
        int16_t                 INT16;
        uint16_t                UINT16;
        int32_t                 INT32;
        uint32_t                UINT32;
        float                   FLOAT;
        uint8_t                 ARRAY[4];
    };
} PeripheralDataType_t;

typedef struct _tagUIPeripheralADDataHeader
{
    DevAddress_t            DevAddress;
    uint8_t                 ProductID;
    int8_t                  rssi;
    uint16_t                Flag;
    uint8_t                 BatteryInfo;
    uint8_t                 TotalDataValueList;
} UIPeripheralAdDataHeader_t;

typedef struct _tagUIPeripheralADData
{
    UIPeripheralAdDataHeader_t  Header;
    PeripheralDataType_t        *pDataValueList;
} UIPeripheralAdData_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int DKPeripheralList_Init(void);
void DKPeripheralList_Empty(int handle);
void DKPeripheralList_Save(int handle, UIPeripheralAdDataHeader_t *pHeader, PeripheralDataType_t *pValueList, int NumOfValueList);
void DKPeripheralList_Remove(int handle, DevAddress_t *pDevAddr);
const UIPeripheralAdData_t *DKPeripheralList_GetAt(int handle, int index);
size_t DKPeripheralList_Count(int handle);
void DKPeripheralList_Uninit(int handle);
void DKPeripheralList_Free(UIPeripheralAdData_t *pEntry);

#ifdef __cplusplus
}
#endif

#endif // _DKPERIPHERALDATALIST_H
