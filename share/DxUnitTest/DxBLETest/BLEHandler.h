//============================================================================
// File: BLEHandler.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/07/28
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _BLEHANDLER_H
#define _BLEHANDLER_H

#pragma once

#ifdef DK_GATEWAY
#endif
#include "dxOS.h"

#include "BLEManager.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/
#define configUSE_CUSTOM_PRINTF			0

#if configUSE_CUSTOM_PRINTF
#define APRINT                      G_SYS_DBG_LOG_V
#define NPRINT                      G_SYS_DBG_LOG_NV
#else // configUSE_CUSTOM_PRINTF
#define APRINT                      printf
#define NPRINT                      printf
#endif // configUSE_CUSTOM_PRINTF

#define PRINT_BIN(x, y, z)          \
{   \
    int i = 0; \
    int len = strlen(z); \
    char *szBIN = calloc(1, (2 + len) * y); \
    memset(szBIN, 0x00, ((2 + len) * y)); \
    for (i = 0; i < y; i++) \
    { \
        sprintf(&szBIN[i * (2 + len)], "%02X", x[i]); \
        if (len > 0 && i + 1 < y) \
        { \
            sprintf(&szBIN[i * (2 + len) + 2], "%s", z); \
        } \
    } \
    NPRINT("%s", szBIN);  \
    free(szBIN); \
}


/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int MACAddrList_GetHandle(void);
int PeripheralDataList_GetHandle(void);
BLEManager_result_t BLESDK_Init(void);

#ifdef __cplusplus
}
#endif

#endif // _BLEHANDLER_H
