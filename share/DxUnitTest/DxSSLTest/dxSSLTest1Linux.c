/* ------------------------------------------------------------------------- *
 * file:        dxSSLTestLinux.c                                             *
 * purpose:     Example code for test SSL communication through              *
 *              dxSSL API                                                    *
 *                                                                           *
 * gcc dxSSLTestLinux.c dxOS.c dxNET.c dxSSL.c -lssl -lcrypto -lpthread -lrt *
 * ------------------------------------------------------------------------- */

#include <sys/sysinfo.h>
#include "dxNET.h"
#include "dxSSL.h"

static char *pszUserLogin =
    "POST /3.6/UserLogin.php HTTP/1.1\r\n" \
    "Host: sigmacasaqaserver.dexatekwebservice.net\r\n" \
    "X-DK-Application-Id: ODM-SIGMACASA-HUBRTK-0ba4f32555214\r\n" \
    "X-DK-API-Key: ce8f2bd73b478e816a49fe68979089d\r\n" \
    "Content-Type: application/json\r\n" \
    "Content-Length: 115\r\n\r\n" \
    "{\"GatewayLogin\":{\"GatewayKey\":\"54478e5d8c4ce378800c82d6a6e5931e1c5a5a43e51fe\",\"GMACAddress\":\"4353465712627417091\"}}";

#define SHOW_MEMORY_INFO            SHOW_MyMemoryInfo

void SHOW_MyMemoryInfo(const char *fname, int lineno)
{
    struct sysinfo info;

    if ( sysinfo( &info ) == 0 )
    {
        printf("[%s] Line %d\n", fname, lineno);
        printf("up time:    %lu\n", info.uptime);
        printf("loads:      %lu %lu %lu\n", info.loads[0], info.loads[1], info.loads[2]);
        printf("total ram:  %lu\n", info.totalram);
        printf("free ram:   %lu\n", info.freeram);
        printf("shared ram: %lu\n", info.sharedram);
        printf("buffer ram: %lu\n", info.bufferram);
        printf("total swap: %lu\n", info.totalswap);
        printf("free swap:  %lu\n", info.freeswap);
        printf("process #:  %u\n", info.procs);
        printf("\n");
    }

	FILE *fp;
	char buf[64];

    // VmPeak: Peak virtual memory usage
    // VmSize: Current virtual memory usage
    // VmLck: Current mlocked memory
    // VmHWM: Peak resident set size
    // VmRSS: Resident set size
    // VmData: Size of "data" segment
    // VmStk: Size of stack
    // VmExe: Size of "text" segment
    // VmLib: Shared library usage
    // VmPTE: Pagetable entries size
    // VmSwap: Swap space used

	fp = fopen("/proc/self/status","r");
	if (fp)
	{
    	while (1)
    	{
    		memset(buf, 0x00, sizeof(buf));
    		if (fgets(buf, sizeof(buf), fp) == NULL)
    		{
    			break;
            }
    		if (strncmp(buf, "Vm", 2) == 0)
    		{
    			printf("%s",buf);
    		}
    	}

    	fclose(fp);
        printf("\n");
	}
}

int main()
{
    dxIP_Addr_t HostIP;

    HostIP.version = DXIP_VERSION_V4;
    HostIP.v4[0] = 54;
    HostIP.v4[1] = 77;
    HostIP.v4[2] = 184;
    HostIP.v4[3] = 135;

    SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

    int ret = 0;
    int i = 0;
    for (i = 0; i < 100; i++)
    {
        dxSSLHandle_t *pSSL = dxSSL_Connect(&HostIP, 443, 0, SSL_VERIFY_NONE);
        printf("Line %d, pSSL=%p\n", __LINE__, pSSL);
        if (pSSL == NULL) goto Exit;

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

        ret = dxSSL_Send(pSSL, pszUserLogin, strlen(pszUserLogin));
        printf("Line %d, dxSSL_Send()=%d\n", __LINE__, ret);
        if (ret <= 0) goto Exit;

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

        uint8_t szBuff[4096];
        memset(szBuff, 0x00, sizeof(szBuff));

        ret = dxSSL_ReceiveTimeout(pSSL, szBuff, sizeof(szBuff), 1000);
        printf("Line %d, dxSSL_Receive()=%d\n", __LINE__, ret);
        if (ret <= 0) goto Exit;
        szBuff[ret] = 0x00;

        printf("%s\n", szBuff);

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

        ret = dxSSL_Close(pSSL);
        printf("Line %d, dxSSL_Close()=%d\n", __LINE__, ret);

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

        printf("\n========== #%d ==========\n", i);
    }

Exit:
    return 0;
}
