/* ------------------------------------------------------------------------- *
 * file:        dxSSLTestLinux.c                                             *
 * purpose:     Example code for test SSL communication through              *
 *              dxSSL API                                                    *
 *                                                                           *
 * gcc dxSSLTestLinux.c dxOS.c dxNET.c dxSSL.c -lssl -lcrypto -lpthread -lrt *
 * ------------------------------------------------------------------------- */

#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

static char szBuffer[4096];
static uint32_t nAPI = 0;

const char *pGETSERVER_TIME = "GET /testing/GetServerTime.php HTTP/1.1\r\n" \
                              "Host: sigmacasaqaserver.dexatekwebservice.net\r\n\r\n";

#define SHOW_MEMORY_INFO            SHOW_MyMemoryInfo

void SHOW_MyMemoryInfo(const char *fname, int lineno)
{
    struct sysinfo info;

    if ( sysinfo( &info ) == 0 )
    {
        printf("[%s] Line %d\n", fname, lineno);
        printf("up time:    %lu\n", info.uptime);
        printf("loads:      %lu %lu %lu\n", info.loads[0], info.loads[1], info.loads[2]);
        printf("total ram:  %lu\n", info.totalram);
        printf("free ram:   %lu\n", info.freeram);
        printf("shared ram: %lu\n", info.sharedram);
        printf("buffer ram: %lu\n", info.bufferram);
        printf("total swap: %lu\n", info.totalswap);
        printf("free swap:  %lu\n", info.freeswap);
        printf("process #:  %u\n", info.procs);
        printf("\n");
    }

	FILE *fp;
	char buf[64];

    // VmPeak: Peak virtual memory usage
    // VmSize: Current virtual memory usage
    // VmLck: Current mlocked memory
    // VmHWM: Peak resident set size
    // VmRSS: Resident set size
    // VmData: Size of "data" segment
    // VmStk: Size of stack
    // VmExe: Size of "text" segment
    // VmLib: Shared library usage
    // VmPTE: Pagetable entries size
    // VmSwap: Swap space used

	fp = fopen("/proc/self/status","r");
	if (fp)
	{
    	while (1)
    	{
    		memset(buf, 0x00, sizeof(buf));
    		if (fgets(buf, sizeof(buf), fp) == NULL)
    		{
    			break;
            }
    		if (strncmp(buf, "Vm", 2) == 0)
    		{
    			printf("%s",buf);
    		}
    	}

    	fclose(fp);
        printf("\n");
	}
}

void Create_GetServerTime_REQ()
{
    memset(szBuffer, 0x00, sizeof(szBuffer));
    memcpy(szBuffer, pGETSERVER_TIME, strlen(pGETSERVER_TIME));
}

int main()
{
    SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        printf("socket() FAILED\r\n");
        return -1;
    }

    struct sockaddr_in dest_addr;

    bzero(&dest_addr,sizeof(dest_addr));
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(80);
    dest_addr.sin_addr.s_addr = inet_addr("52.17.216.64");


    // Try to make the host connect here
    if (connect(sockfd, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr)) == -1)
    {
        printf("connect() FAILED\r\n");
        return -1;
    }

    int ret = 0;

    uint32_t msec = 1000;
    struct timeval interval = { .tv_sec = msec / 1000,
                                .tv_usec = (msec % 1000) * 1000 };

    ret = setsockopt(sockfd,
                     SOL_SOCKET,
                     SO_RCVTIMEO,
                     (struct timeval *)&interval,
                     sizeof(struct timeval));

    int i = 0;
    for (i = 0; i < 300; i++)
    {
        Create_GetServerTime_REQ();

        ret = send(sockfd, szBuffer, strlen(szBuffer), 0);

        printf("Line %d, send()=%d\n", __LINE__, ret);

        if (ret <= 0)
        {
             continue;
        }

        printf("%s\n", szBuffer);

        memset(szBuffer, 0x00, sizeof(szBuffer));

        struct timeval tv1, tv2;

        gettimeofday(&tv1, NULL);
        ret = recv(sockfd, szBuffer, sizeof(szBuffer), 0);
        gettimeofday(&tv2, NULL);

        double v = tv2.tv_sec + tv2.tv_usec / 1000000 - tv1.tv_sec - tv1.tv_usec / 1000000;

        printf("Line %d, recv()=%d, Use %f seconds\n", __LINE__, ret, v);

        if (ret <= 0)
        {
             continue;
        }

        szBuffer[ret] = 0x00;

        printf("%s\n", szBuffer);

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);
        nAPI++;

        printf("\n========== #%d ==========\n", nAPI);
    }

    ret = close(sockfd);
    printf("Line %d, dxSSL_Close()=%d\n", __LINE__, ret);

    SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

    return 0;
}
