/* ------------------------------------------------------------------------- *
 * file:        dxSSLTestLinux.c                                             *
 * purpose:     Example code for test SSL communication through              *
 *              dxSSL API                                                    *
 *                                                                           *
 * gcc dxSSLTestLinux.c dxOS.c dxNET.c dxSSL.c -lssl -lcrypto -lpthread -lrt *
 * ------------------------------------------------------------------------- */

#include "dxNET.h"
#include "dxSSL.h"

#include <sys/sysinfo.h>
#include <time.h>

static char szBuffer[4096];
static char szSessionToken[64];
static uint32_t nGatewayID = 0;
static uint32_t nAPI = 0;

const char *pGatewayKey = "54478e5d8c4ce378800c82d6a6e5931e1c5a5a43e51fe";
const char *pGatewayMAC = "12439527110970923672";

const char *pUSERLOGIN =
    "POST /testing/UserLogin.php HTTP/1.1\r\n"                \
    "Host: sigmacasaqaserver.dexatekwebservice.net\r\n"       \
    "X-DK-Application-Id: ODM-SIGMACASA-IPCAM-81d75b9\r\n"    \
    "X-DK-API-Key: 7a5183d20140ce32b33d4660624e7a3b\r\n"      \
    "Content-Type: application/json\r\n"                      \
    "Content-Length: 116\r\n\r\n"                             \
    "{\"GatewayLogin\":{\"GatewayKey\":\"%s\",\"GMACAddress\":\"%s\"}}";

const char *pSESSION_TOKEN_PATTERN = "\"sessionToken\":\"";

const char *pGETPERIPHERALBYGMAC =
    "POST /testing/GetPeripheralByGMACAddress.php HTTP/1.1\r\n"      \
    "Host: sigmacasaqaserver.dexatekwebservice.net\r\n"              \
    "X-DK-Application-Id: ODM-SIGMACASA-IPCAM-81d75b9\r\n"           \
    "X-DK-API-Key: 7a5183d20140ce32b33d4660624e7a3b\r\n"             \
    "Content-Type: application/json\r\n"                             \
    "Content-Length: 49\r\n"                                         \
    "X-DK-Session-Token: %s\r\n\r\n"    \
    "{\"Data\":[{\"GMACAddress\":\"%s\"}]}";

const char *pGATEWAYID_PATTERN = "{\"status\":{\"code\":0},\"results\":[{\"ObjectID\":";

const char *pGETNEWJOBS =
    "POST /testing/GetNewJobs.php HTTP/1.1\r\n"                    \
    "Host: sigmacasaqaserver.dexatekwebservice.net\r\n"            \
    "X-DK-Application-Id: ODM-SIGMACASA-IPCAM-81d75b9\r\n"         \
    "X-DK-API-Key: 7a5183d20140ce32b33d4660624e7a3b\r\n"           \
    "Content-Type: application/json\r\n"                           \
    "Content-Length: 26\r\n"                                       \
    "X-DK-Session-Token: %s\r\n\r\n"
    "{\"GatewayID\":%u,\"Max\":0}";

const char *pTIMEOUT = "GET /testing/15K?sleep=15 HTTP/1.1\r\n" \
                       "Host: sigmacasaqaserver.dexatekwebservice.net\r\n\r\n";

#define SHOW_MEMORY_INFO            SHOW_MyMemoryInfo

void SHOW_MyMemoryInfo(const char *fname, int lineno)
{
    struct sysinfo info;

    if ( sysinfo( &info ) == 0 )
    {
        printf("[%s] Line %d\n", fname, lineno);
        printf("up time:    %lu\n", info.uptime);
        printf("loads:      %lu %lu %lu\n", info.loads[0], info.loads[1], info.loads[2]);
        printf("total ram:  %lu\n", info.totalram);
        printf("free ram:   %lu\n", info.freeram);
        printf("shared ram: %lu\n", info.sharedram);
        printf("buffer ram: %lu\n", info.bufferram);
        printf("total swap: %lu\n", info.totalswap);
        printf("free swap:  %lu\n", info.freeswap);
        printf("process #:  %u\n", info.procs);
        printf("\n");
    }

	FILE *fp;
	char buf[64];

    // VmPeak: Peak virtual memory usage
    // VmSize: Current virtual memory usage
    // VmLck: Current mlocked memory
    // VmHWM: Peak resident set size
    // VmRSS: Resident set size
    // VmData: Size of "data" segment
    // VmStk: Size of stack
    // VmExe: Size of "text" segment
    // VmLib: Shared library usage
    // VmPTE: Pagetable entries size
    // VmSwap: Swap space used

	fp = fopen("/proc/self/status","r");
	if (fp)
	{
    	while (1)
    	{
    		memset(buf, 0x00, sizeof(buf));
    		if (fgets(buf, sizeof(buf), fp) == NULL)
    		{
    			break;
            }
    		if (strncmp(buf, "Vm", 2) == 0)
    		{
    			printf("%s",buf);
    		}
    	}

    	fclose(fp);
        printf("\n");
	}
}

void Create_UserLogin_REQ(void)
{
    memset(szBuffer, 0x00, sizeof(szBuffer));
    sprintf(szBuffer, pUSERLOGIN, pGatewayKey, pGatewayMAC);
}

void Parse_UserLogin_SessionToken(void)
{
    char *pTokenPos = strstr(&szBuffer[0], pSESSION_TOKEN_PATTERN);
    if (pTokenPos)
    {
        char *p = strstr(&pTokenPos[strlen(pSESSION_TOKEN_PATTERN)], "\"");
        if (p)
        {
            int len = (int)(p - pTokenPos - strlen(pSESSION_TOKEN_PATTERN));
            memset(szSessionToken, 0x00, sizeof(szSessionToken));
            memcpy(szSessionToken, &pTokenPos[strlen(pSESSION_TOKEN_PATTERN)], len);
        }
    }
}

void Create_GetPeripheralByGMAC_REQ(void)
{
    memset(szBuffer, 0x00, sizeof(szBuffer));
    sprintf(szBuffer, pGETPERIPHERALBYGMAC, szSessionToken, pGatewayMAC);
}

void Parse_GetPeripheralByGMAC_GatewayID(void)
{
    char *pPos = strstr(&szBuffer[0], pGATEWAYID_PATTERN);
    if (pPos)
    {
        nGatewayID = atoi(&pPos[strlen(pGATEWAYID_PATTERN)]);
    }
}

void Create_GetNewJobs_REQ()
{
    memset(szBuffer, 0x00, sizeof(szBuffer));
    sprintf(szBuffer, pGETNEWJOBS, szSessionToken, nGatewayID);
}

void Create_RecvTimeout_REQ()
{
    memset(szBuffer, 0x00, sizeof(szBuffer));
    memcpy(szBuffer, pTIMEOUT, strlen(pTIMEOUT));
}

int main()
{
    dxIP_Addr_t HostIP;

    HostIP.version = DXIP_VERSION_V4;
    HostIP.v4[0] = 54;
    HostIP.v4[1] = 77;
    HostIP.v4[2] = 184;
    HostIP.v4[3] = 135;

    SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

    dxSSLHandle_t *pSSL = dxSSL_Connect(&HostIP, 443, 0, SSL_VERIFY_NONE);
    if (pSSL == NULL) goto Exit;
    if (pSSL == 0) goto Exit;

    int ret = 0;
    int i = 0;
    for (i = 0; i < 100; i++)
    {
        if (nAPI == 0)
        {
            Create_UserLogin_REQ();
        }
        else if (nAPI == 1)
        {
            Create_GetPeripheralByGMAC_REQ();
        }
        else
        {
            Create_GetNewJobs_REQ();
            //Create_RecvTimeout_REQ();
        }

        ret = dxSSL_Send(pSSL, szBuffer, strlen(szBuffer));
        printf("Line %d, dxSSL_Send()=%d\n", __LINE__, ret);

        if (ret <= 0)
        {
             //goto Exit;
        }

        printf("%s\n", szBuffer);

        memset(szBuffer, 0x00, sizeof(szBuffer));

        struct timeval tv1, tv2;

        gettimeofday(&tv1, NULL);
        ret = dxSSL_ReceiveTimeout(pSSL, szBuffer, sizeof(szBuffer), 1000);
        gettimeofday(&tv2, NULL);

        double v = tv2.tv_sec + tv2.tv_usec / 1000000 - tv1.tv_sec - tv1.tv_usec / 1000000;

        printf("Line %d, dxSSL_Receive()=%d, Use %f seconds\n", __LINE__, ret, v);

        if (ret <= 0)
        {
             //goto Exit;
        }

        szBuffer[ret] = 0x00;

        printf("%s\n", szBuffer);

        if (nAPI == 0)
        {
            Parse_UserLogin_SessionToken();
        }
        else if (nAPI == 1)
        {
            Parse_GetPeripheralByGMAC_GatewayID();
        }

        if (szSessionToken[0] == 0x00)
        {
            continue;
        }

        SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);
        nAPI++;

        printf("\n========== #%d ==========\n", nAPI);
    }

    ret = dxSSL_Close(pSSL);

    printf("Line %d, dxSSL_Close()=%d\n", __LINE__, ret);

    SHOW_MEMORY_INFO(__FUNCTION__, __LINE__);

Exit:
    return 0;
}
