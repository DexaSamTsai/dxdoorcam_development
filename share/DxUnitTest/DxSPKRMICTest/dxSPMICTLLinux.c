/*
 * Realtek Semiconductor Corp.
 *
 * example/example_audio_ctrl.c
 *
 * Copyright (C) 2016      Wind Han<wind_han@realsil.com.cn>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rtstream.h>
#include <rtsamixer.h>

int main(int argc, char *argv[])
{
	int value, ret = 0;

	if (argc < 2)
		return -1;

	if (0 == strcasecmp("pget", argv[1])) {
		ret = rts_audio_get_playback_volume(&value);
		if (ret) {
			printf("fail to get playback volume\n");
			goto out;
		}
		printf("the current playback volume is %d\n", value);
	} else if (0 == strcasecmp("pset", argv[1])) {
		value = atoi(argv[2]);
		ret = rts_audio_set_playback_volume(value);
		if (ret) {
			printf("fail to set playback volume\n");
			goto out;
		}
		ret = rts_audio_get_playback_volume(&value);
		if (ret) {
			printf("fail to get playback volume\n");
			goto out;
		}
		printf("the current playback volume is %d\n", value);
	} else if (0 == strcasecmp("cget", argv[1])) {
		ret = rts_audio_get_capture_volume(&value);
		if (ret) {
			printf("fail to get capture volume\n");
			goto out;
		}
		printf("the current capture volume is %d\n", value);
	} else if (0 == strcasecmp("cset", argv[1])) {
		value = atoi(argv[2]);
		ret = rts_audio_set_capture_volume(value);
		if (ret) {
			printf("fail to set capture volume\n");
			goto out;
		}
		ret = rts_audio_get_capture_volume(&value);
		if (ret) {
			printf("fail to get capture volume\n");
			goto out;
		}
		printf("the current capture volume is %d\n", value);
	} else if (0 == strcasecmp("pmute", argv[1])) {
		ret = rts_audio_playback_mute();
		if (ret) {
			printf("fail to set playback mute\n");
			goto out;
		}
		printf("the current playback is mute\n");
	} else if (0 == strcasecmp("punmute", argv[1])) {
		ret = rts_audio_playback_unmute();
		if (ret) {
			printf("fail to set playback unmute\n");
			goto out;
		}
		printf("the current playback is unmute\n");
	} else if (0 == strcasecmp("cmute", argv[1])) {
		ret = rts_audio_capture_mute();
		if (ret) {
			printf("fail to set capture mute\n");
			goto out;
		}
		printf("the current capture is mute\n");
	} else if (0 == strcasecmp("cunmute", argv[1])) {
		ret = rts_audio_capture_unmute();
		if (ret) {
			printf("fail to set capture unmute\n");
			goto out;
		}
		printf("the current capture is unmute\n");
	}

out:
	return ret;
}
