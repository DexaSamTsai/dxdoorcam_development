//============================================================================
// File: Dexatek_Application.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dk_Peripheral.h"
#include "dk_PeripheralAux.h"
#include "dxSysDebugger.h"
#include "HybridPeripheralManager.h"

//============================================================================
// Declaration
//============================================================================

#define TEST_CONTAINER_M_TYPE           0xFF00
#define TEST_CONTAINER_D_TYPE           0xFF01
#define TEST_CONTAINER_LVALUE           "dxHPClientToServerTest LValue String"
#define TEST_CONTAINER_LVALUE_SIZE      (sizeof(TEST_CONTAINER_LVALUE))

//============================================================================
// Function declaration
//============================================================================
void MainEvent_HpClientEventReportHandler(void* arg);

//============================================================================
// Global Variable
//============================================================================

dxBOOL      bContainerFromCloudAvailable = dxFALSE;
uint64_t    ContMID = 0;
uint64_t    ContDID = 0;

//============================================================================
// Local Function
//============================================================================

int GetUserInput(char* userInputBuff, int SizeOfuserInputBuff)
{
    int  TotalChar = 0;
    char userChar;

    memset(userInputBuff, 0, SizeOfuserInputBuff);

    char* pUserInputBuff = userInputBuff;
    while ((userChar = getchar()) != 0x0A)
    {
        *pUserInputBuff = userChar;
        pUserInputBuff++;
        TotalChar++;
    }

    return TotalChar;
}

//============================================================================
// Main Function
//============================================================================

int main(void)
{

    dxOS_Init();

    dxBSP_Init();
                
    HPManager_result_t HpRes = HpManagerInit_Client(MainEvent_HpClientEventReportHandler);
    if (HpRes != HP_MANAGER_SUCCESS)
    {
        printf("HpManagerInit_Client failed (Err=%d)\n", HpRes);
    }
    
    char    userInputBuff[256];
    while (1)
    {
        printf("\nTo Exit type 'Exit': ");
        if (GetUserInput(userInputBuff, sizeof(userInputBuff)))
        {        
            if (strcasecmp(userInputBuff, "Exit") == 0)        
            {
                break;
            }
        }
    }
    
    printf("\nBye Bye~\n");
    
    return 0;
}


//============================================================================
// Callback Function
//============================================================================

void MainEvent_HpClientEventReportHandler(void* arg)
{
    //G_SYS_DBG_LOG_V("%s IN\r\n", __FUNCTION__);

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch(pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_IDLE_EXITING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_IDLE_EXITING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_EXITING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_EXITING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_EXITING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_EXITING\n");
            break;
        case HP_CMD_FRW_STATUS_REPORT_ST_SETUP_EXITING:
            printf("HP_CMD_FRW_STATUS_REPORT_ST_SETUP_EXITING\n");
            break;
        
        case HP_CMD_FRW_STATUS_REPORT_SERVER_READY:
        {
            printf("HP_CMD_FRW_STATUS_REPORT_SERVER_READY\n");
            
            /* We request Streaming URL once we receive server ready */            
            HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
            pCmdMessage->Priority = 0; //Normal
            pCmdMessage->MessageID = HP_CMD_STREAMING_URL_REQUEST; //Make the message ID same as the command request
            pCmdMessage->CommandMessage = HP_CMD_STREAMING_URL_REQUEST;
            pCmdMessage->PayloadData = NULL;
            pCmdMessage->PayloadSize = 0;
                            
            //Send the message to HybridPeripheral Server
            HpManagerSendTo_Server(pCmdMessage);
            HpManagerCleanEventArgumentObj(pCmdMessage);
            
            if (bContainerFromCloudAvailable == dxFALSE)
            {
               HP_CMD_DataExchange_CreateFullContainerData( HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ, 
                                                            TEST_CONTAINER_M_TYPE, 
                                                            TEST_CONTAINER_D_TYPE, 
                                                            strlen("NA"), 
                                                            "NA", 
                                                            strlen("NA"), 
                                                            "NA", 
                                                            0, 
                                                            NULL, 
                                                            TEST_CONTAINER_LVALUE_SIZE, 
                                                            TEST_CONTAINER_LVALUE,
                                                            0); 
            }
            else
            {
                HP_CMD_DataExchange_ContainerDataRequest(HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ, ContMID, ContDID, 0);                
            }
                           
        }
            break;        
        
        case HP_CMD_STREAMING_URL_RES:
        {
            printf("HP_CMD_STREAMING_URL_RES\n");
            
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpStreamingUrlInfo_t* pStreamingUrlRes = (HpStreamingUrlInfo_t*)pHpCmdMessage->PayloadData;
                
                if (pStreamingUrlRes->UrlLen)
                {
                    printf("HP_CMD_STREAMING_URL_RES: %s\n", pStreamingUrlRes->pUrl);
                }
                else
                {
                    printf("HP_CMD_STREAMING_URL_RES: NULL!\n");
                }

                /* We request Event URL */    
                HpVideoImageEvtUrlReq_t* pEvtUrlRequestInfo = dxMemAlloc("pEvtUrlRequestInfo", 1, sizeof(HpVideoImageEvtUrlReq_t));
                pEvtUrlRequestInfo->EventType = HP_EVENT_TYPE_VIDEO;
                pEvtUrlRequestInfo->FileFormat = HP_FILE_TYPE_MP4;
                pEvtUrlRequestInfo->DateTime = 0;
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST; //Make the message ID same as the command request
                pCmdMessage->CommandMessage = HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST;
                pCmdMessage->PayloadData = (uint8_t*)pEvtUrlRequestInfo;
                pCmdMessage->PayloadSize = sizeof(HpVideoImageEvtUrlReq_t);
                            
                //Send the message to HybridPeripheral Server
                HpManagerSendTo_Server(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);                                 
                 
            }
        }
            break;


        case HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT:
        {
            printf("HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT\n");

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcContainerDataReport_t* pContainerReport = (HpDataExcContainerDataReport_t*)pHpCmdMessage->PayloadData;
                
                bContainerFromCloudAvailable = dxTRUE;
                
                printf("Container Info\n");
                printf("==============\n");
                printf("ContMID = %ld\n", pContainerReport->ContMID);
                printf("ContMType = %d\n", pContainerReport->ContMType);
                printf("ContDID = %ld\n", pContainerReport->ContDID);
                printf("ContDType = %d\n", pContainerReport->ContDType);
                printf("DateTime = %d\n", pContainerReport->DateTime);
                printf("SValueLen = %d\n", pContainerReport->SValueLen);
                printf("LValueLen = %d\n", pContainerReport->LValueLen);
                printf("ContLVCRC = %d\n", pContainerReport->ContLVCRC);  
                if ((pContainerReport->LValueLen) &&
                    (pContainerReport->ContMType == TEST_CONTAINER_M_TYPE) &&
                    (pContainerReport->ContDType == TEST_CONTAINER_D_TYPE))
                {
                    uint8_t* pLvalue = pContainerReport->CombinedValue + pContainerReport->SValueLen;
                    printf("LVALUE = %s\n", pLvalue);  
                }
                else
                {
                    printf("LVALUE = N/A or Unknown Format\n"); 
                }       

                ContMID = pContainerReport->ContMID;
                ContDID = pContainerReport->ContDID;
                
            }
        }
            break;
                
        case HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES:
        {
            printf("HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES\n");

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpVideoImageEvtUrlInfo_t* pEventUrlInfo = (HpVideoImageEvtUrlInfo_t*)pHpCmdMessage->PayloadData;
                
                if (pEventUrlInfo->UploadUrlLen)
                {
                    printf("Event UploadUrl = %s\n", pEventUrlInfo->CombineUrl);
                }
                
                if (pEventUrlInfo->DownloadUrlLen)
                {
                    printf("Event DownloadUrl = %s\n", &pEventUrlInfo->CombineUrl[pEventUrlInfo->UploadUrlLen]);
                }
                
                /* We request Event Notification */
                uint16_t    ResourceLen = strlen("https://s3-us-west-2.amazonaws.com/notificationvideos/recipe2.mp4");
                uint16_t    EvtNotificationInfoSize = sizeof(HpVideoImageEvtNotificationReq_t) + ResourceLen + 1;
                HpVideoImageEvtNotificationReq_t* pEvtNotificationInfo = dxMemAlloc("pEvtNotificationInfo", 
                                                                                    1, 
                                                                                    EvtNotificationInfoSize);
                                                                                         
                pEvtNotificationInfo->MessageCategoryType = HP_EVENT_TYPE_VIDEO;
                pEvtNotificationInfo->MessageCategoryTemplate = HP_TEMPLATE_STD;
                pEvtNotificationInfo->MessageCategoryFileType = HP_FILE_TYPE_MP4;
                pEvtNotificationInfo->MessageResourceLen = ResourceLen + 1; //Including NULL
                uint8_t*    pCombinedPayload = pEvtNotificationInfo->CombinePayload;
                memcpy(pCombinedPayload, "https://s3-us-west-2.amazonaws.com/notificationvideos/recipe2.mp4", ResourceLen);
                pCombinedPayload += ResourceLen + 1;
                
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_REQUEST; //Make the message ID same as the command request
                pCmdMessage->CommandMessage = HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_REQUEST;
                pCmdMessage->PayloadData = (uint8_t*)pEvtNotificationInfo;
                pCmdMessage->PayloadSize = EvtNotificationInfoSize;
                            
                //Send the message to HybridPeripheral Server
                HpManagerSendTo_Server(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);                                 
                
            }            
        }
            break;

        case HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_RES:
        {
            printf("HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_RES\n");

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpVideoImageGenericRes_t* pEventNotificationRes = (HpVideoImageGenericRes_t*)pHpCmdMessage->PayloadData;
                if (pEventNotificationRes->Status == HP_VIDEO_IMAGE_ACCESS_SUCCESS)
                {
                    printf("HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_RES Success\n");
                }
                else
                {
                    printf("HP_CMD_VIDEO_IMAGE_EVENT_NOTIFICATION_SEND_RES Failed = %d\n", pEventNotificationRes->Status);                    
                }
            }
          
        }
            break;
        
        case HP_CMD_CONTROL_ACCESS_WRITE_REQUEST:
        {
            printf("HP_CMD_CONTROL_ACCESS_WRITE_REQUEST\n");
            
            if (pHpCmdMessage->PayloadData && pHpCmdMessage->PayloadSize)
            {
                printf("TimeStamp @%d\n", pHpCmdMessage->TimeStamp);
                
                HpControlAccessReq_t* pControlAccessReq = (HpControlAccessReq_t*)pHpCmdMessage->PayloadData;  
                            
                G_SYS_DBG_LOG_V( "SourceOfOrigin = %d, IdentificationNumber = %ld \r\n",  pControlAccessReq->SourceOfOrigin, pControlAccessReq->IdentificationNumber);
                G_SYS_DBG_LOG_V( "Service UUID = " );
                uint8_t i = 0;
                for (i = 0; i < 16; i++)
                {
                    G_SYS_DBG_LOG_NV( "%x",  pControlAccessReq->ServiceUUID[i]);
                }

                G_SYS_DBG_LOG_V( "\r\nCharacteristic UUID = " );
                for (i = 0; i < 16; i++)
                {
                    G_SYS_DBG_LOG_NV( "%x",  pControlAccessReq->CharacteristicUUID[i]);
                }
                
                G_SYS_DBG_LOG_V( "\r\nPayloadLen = %d Payload = %s",  pControlAccessReq->DataLen, pControlAccessReq->pDataPayload);
                
                
                //Report&Clear IPCam Recording Event's Historic Event data payload to framework
                {
                    G_SYS_DBG_LOG_V( "\r\Report&Clear IPCam Recording Event's Historic Event data payload to framework" );
                    uint8_t RecordingEventDataTypeSize = 1;
                    uint8_t RecordingEventDataSize = 1;
                    uint16_t HistoricEventDataSize = sizeof(HpDataExcHistoryEventDetailData_t) + RecordingEventDataTypeSize + RecordingEventDataSize;
                    HpDataExcHistoryEventDetailData_t* pHistoricEventData = (HpDataExcHistoryEventDetailData_t*)dxMemAlloc("", 1, HistoricEventDataSize);
                    pHistoricEventData->PayloadDataSize = RecordingEventDataTypeSize + RecordingEventDataSize;
                    pHistoricEventData->PayloadData[0] = DK_DATA_TYPE_RECORDING_EVENT;
                    pHistoricEventData->PayloadData[1] = EVENT_VIDEO_RECORD;
                    
                    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                    pCmdMessage->Priority = 0; //Normal
                    pCmdMessage->MessageID = HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ; //Make the message ID same as the command request
                    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ;
                    pCmdMessage->PayloadData = (uint8_t*)pHistoricEventData;
                    pCmdMessage->PayloadSize = HistoricEventDataSize;
                                
                    //Send the event video record message to HybridPeripheral Server
                    HpManagerSendTo_Server(pCmdMessage);
                    
                    sleep(1);

                    //Send the event none message to HybridPeripheral Server                
                    pHistoricEventData->PayloadData[1] = EVENT_NONE;
                    HpManagerSendTo_Server(pCmdMessage);
                    
                    HpManagerCleanEventArgumentObj(pCmdMessage);                                                                                 
                }  
                
            }                    
        }
            break;
            
        case HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST:
        {
            printf("HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST\n");
        }
            break;

        case HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST:
        {
            printf("HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST\n");
        }
            break;
        
        case HP_INTERNAL_REPORT_CLIENT_ERROR_TERMINATED:
        {
            printf("HP_INTERNAL_REPORT_CLIENT_ERROR_TERMINATED\n");
            
            //Re-Launch again or shut down entire process (let the main monitor process to handle it)
            HPManager_result_t HpRes = HpManagerInit_Client(MainEvent_HpClientEventReportHandler);
            if (HpRes != HP_MANAGER_SUCCESS)
            {
                printf("HpManagerInit_Client failed (Err=%d)\n", HpRes);
            }            
        }
            break;
            
        default:
        break;
    }

    //Always clean the arg object from event
    HpManagerCleanEventArgumentObj(arg);

}