/* ------------------------------------------------------------ *
 * file:        dxDNSTestLinux.c                                *
 * purpose:     Example code for test SSL communication through *
 *              dxSSL API                                       *
 *                                                              *
 * gcc dxDNSTestLinux.c dxNET.c dxDNS.c -lssl -lcrypto          *
 * ------------------------------------------------------------ */
#include "dxNET.h"
#include "dxDNS.h"
#include <netdb.h>

#define HOSTNAME                "sigmacasa.dexatekwebservice.net"

int dxDNSTest_main()
{
    printf("dxDNSTest_main start\n");
    return 0; //for test

    dxIPv4_t dxip;
    memset(&dxip, 0x00, sizeof(dxip));
    dxNET_RET_CODE ret = dxDNS_GetHostByName(HOSTNAME, &dxip);

    printf("Query %s with result(%d)\n", HOSTNAME, ret);

    if (ret == DXNET_SUCCESS)
    {
        //printf("IP of %s=%d.%d.%d.%d\n", HOSTNAME, dxip.ip_addr[0], dxip.ip_addr[1], dxip.ip_addr[2], dxip.ip_addr[3]);
    }

    return 0;
}

/*int main()
{
    struct hostent *hp = gethostbyname(HOSTNAME);

    if (hp == NULL) 
    {
    	printf("gethostbyname() failed\n");
    } 
    else 
    {
        printf("%s = ", (char *)hp->h_name);
        unsigned int i=0;
        while (hp -> h_addr_list[i] != NULL) 
        {
            printf( "%s ", inet_ntoa( *( struct in_addr*)( hp -> h_addr_list[i])));
            i++;
        }
        printf("\n");
    }

}*/


