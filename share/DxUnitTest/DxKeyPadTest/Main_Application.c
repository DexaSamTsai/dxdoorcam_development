//============================================================================
// File: Dexatek_Application.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "dxKeypadManager.h"

//============================================================================
// Declaration
//============================================================================

#define MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS     (5)

//============================================================================
// Function declaration
//============================================================================
void MainEvent_KeypadManagerEventReportHandler(void* arg);

//============================================================================
// Global Variable
//============================================================================

static dxGPIO_t TestGPIOConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXPIN_INPUT,
        DXPIN_MODE_UNSUPPORTED
    },

    {
        NULL,
        DK_LED_YELLOW,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    
    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};


static dxKeypad_t TestKeypadConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXKEYPAD_TRIGGER_LEVEL_LOW,
        MAIN_BUTTON_REPORT_ID,
        DX_GENERAL_INTERVAL_ONE_SECOND,
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG
    }
};

//============================================================================
// Local Function
//============================================================================

int GetUserInput(char* userInputBuff, int SizeOfuserInputBuff)
{
    int  TotalChar = 0;
    char userChar;

    memset(userInputBuff, 0, SizeOfuserInputBuff);

    char* pUserInputBuff = userInputBuff;
    while ((userChar = getchar()) != 0x0A)
    {
        *pUserInputBuff = userChar;
        pUserInputBuff++;
        TotalChar++;
    }

    return TotalChar;
}


//============================================================================
// Main Function
//============================================================================

int main(void)
{

    dxOS_Init();
    
    dxGPIO_Init(TestGPIOConfig);
    
    GSysDebuggerInit();

    dxBSP_Init();

    dxKEYPAD_RET_CODE keypadret = dxKeypad_Init( TestKeypadConfig, MainEvent_KeypadManagerEventReportHandler);
    if (keypadret != DXKEYPAD_SUCCESS)
    {
        printf("\ndxKeypad_Init FAILED (%d)", keypadret);        
    }
    
    char    userInputBuff[256];
    while (1)
    {
        printf("\nTo Exit type 'Exit': ");
        if (GetUserInput(userInputBuff, sizeof(userInputBuff)))
        {        
            if (strcasecmp(userInputBuff, "Exit") == 0)        
            {
                break;
            }
        }
    }
    
    printf("\nBye Bye~\n");
    
    return 0;
}


//============================================================================
// Callback Function
//============================================================================

void MainEvent_KeypadManagerEventReportHandler(void* arg)
{

    dxKeypadReport_t* KeyPadEvent = (dxKeypadReport_t*)arg;

    static uint8_t  KeyHoldTimeInSec = 0;
    static dxBOOL   MainButtonPressed = dxFALSE;

    switch (KeyPadEvent->KeyStateEvent)
    {
        case DXKEYPAD_KEY_PRESSED:
        {
            if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID)
            {
                printf("\nDXKEYPAD_KEY_PRESSED");
                MainButtonPressed = dxTRUE;
            }
            KeyHoldTimeInSec = 0;
        }
        break;

        case DXKEYPAD_KEY_RELEASED:
        {
            if (KeyHoldTimeInSec == 0)
            {
                //Button is a lesss than 1 second tap       
                printf("\nDXKEYPAD_KEY_RELEASED (less than 1 second button Tap)");         
            }

            if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID)
            {
                MainButtonPressed = dxFALSE;
                printf("\nDXKEYPAD_KEY_RELEASED");
            }
            
            KeyHoldTimeInSec = 0;
        }
        break;

        case DXKEYPAD_KEY_HELD:
        {
            printf("\nDXKEYPAD_KEY_HELD");
            if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID && MainButtonPressed == dxTRUE)
            {
                KeyHoldTimeInSec++;
                if (KeyHoldTimeInSec == MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS)
                {
                    //Button is pressed and HOLD for N Seconds (defined by MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS)
                    printf("\nDXKEYPAD_KEY_HELD for %d Seconds", MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS);
                }
            }
        }
        break;

        default:
        {
            printf("\nWARNING: Not supported MainEvent_KeypadManagerEventReportHandler (%d)", KeyPadEvent->ReportIdentifier );
        }
        break;
    }

    //Always clean the arg object from event
    dxKeypad_CleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_KeypadManagerEventReportHandler OUT\r\n");

}