/* ------------------------------------------------------------------------- *
 * file:        ServerManagerTestLinux.c                                     *
 * purpose:     Example code for test SSL communication through              *
 *              dxSSL API                                                    *
 *                                                                           *
 * make clean;make                                                           *
 * ------------------------------------------------------------------------- */
#include <stdint.h>
#include "dxOS.h"
#include "dxNET.h"
#include "dxSSL.h"
#include <stdarg.h>
#include "dxSysDebugger.h"
#include "dkjsn.h"
#include "DKServerManager.h"
#include "HTTPManager.h"

#define MAIN_PRINT                          G_SYS_DBG_LOG_V
#define MAX_GETNEWJOBS_BEFORE_DISCONNECT    0               // Specify 0 to disable dxSSL_Close() & dxSSL_Connect()
//============================================================================
// Login Information
//============================================================================
typedef struct dxLoginAccount
{
    char *HostName;
    char *APIVer;
    char *AccessKey;
    char *APPID;
    char *APIKey;
} dxLoginAccount_t;

dxLoginAccount_t dxAccount[] = {
                                   {
                                       "sigmacasaqaserver.dexatekwebservice.net",
                                       "/testing",
                                       "54478e5d8c4ce378800c82d6a6e5931e1c5a5a43e51fe",
                                       "ODM-SIGMACASA-549b8a901b79d",
                                       "e37aa5883f2e0f291cd8301ca38ba605"
                                   },
                                   {
                                       "sigmacasa.dexatekwebservice.net",
                                       "/3.6",
                                       "54478e5d8c4ce378800c82d6a6e5931e1c5a5a43e51fe",
                                       "ODM-SIGMACASA-HUBRTK-0ba4f32555214",
                                       "ce8f2bd73b478e816a49fe68979089d"
                                   }
                               };

static int hidx = 0;            // Use in dxAccount[hidx]


//============================================================================
// Device Default Values
//============================================================================
uint8_t DeviceMAC[] = { 0x3C, 0x6A, 0x9D, 0xFF, 0xFF, 0x00, 0x00, 0x03 };

//============================================================================
// Variable Definition
//============================================================================
uint16_t SourceOfOrigin     = 100;
uint64_t TaskIdentificationNumber = 30000;

//============================================================================
// Local Function
//============================================================================
uint16_t GetSourceOfOrigin()
{
    return (++SourceOfOrigin);
}

uint64_t GetTaskIdNumber()
{
    return (++TaskIdentificationNumber);
}

static uint32_t GetRandomValue(void)
{
    dxTime_t time;

    dxTimeDelayMS(10);

    time = dxTimeGetMS();

    srand((unsigned int)time);
    dxTimeDelayMS(0);

    srand((unsigned int)rand());

    return (uint32_t)rand();
}

//============================================================================
// DKServerEventReportHandler()
//============================================================================
static void DKServerEventReportHandler(void *arg)
{
    //dxOS_RET_CODE result = DXOS_SUCCESS;
    static int fLogin = 0;
    static int nNumberOfGetNewJobs = 0;
    static int nSuspendedTimes = 0;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Line %d, MType:%d, HTTP:%d, SCode:%d, Source:%d, TaskId:%u\n",
                     __FUNCTION__,
                     __LINE__,
                     EventHeader.MessageType,
                     EventHeader.HTTPStatusCode,
                     EventHeader.s.ReportStatusCode,
                     EventHeader.SourceOfOrigin,
                     (unsigned int)(EventHeader.TaskIdentificationNumber & 0x0FFFFFFFF));

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Line %d, Current State:%d\n",
                     __FUNCTION__,
                     __LINE__,
                     DKServerManager_GetCurrentState());

    if (EventHeader.HTTPStatusCode == HTTP_OK &&
    	  EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_UNAUTHORIZE_LOGIN)
    {
        DKServerManager_CleanEventArgumentObj(arg);

        return ;
    }
    else if (EventHeader.HTTPStatusCode == HTTP_OK &&
    	       EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN)
    {
        DKServerManager_CleanEventArgumentObj(arg);

        return ;
    }
    else if (EventHeader.HTTPStatusCode == HTTP_OK &&
    	       EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_ACCESS_DENIED)
    {
        return ;
    }
    else if (EventHeader.HTTPStatusCode == HTTP_OK &&
    	       EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND)
    {
        return ;
    }

    switch(EventHeader.MessageType)
    {
    case DKMSG_RENEWSESSIONTOKEN:           // 2
        break;

    case DKMSG_GATEWAYLOGIN:                // 3
        if (EventHeader.HTTPStatusCode == HTTP_OK &&
        	  EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
        {
            fLogin = 1;
            nNumberOfGetNewJobs = 0;

            DKServerManager_GetPeripheralByMAC_Asynch(GetSourceOfOrigin(),
                                                      GetTaskIdNumber());
        }
        break;

    case DKMSG_ADDGATEWAY:                  // 4
        break;

    case DKMSG_GETPERIPHERALBYGMACADDRESS:  // 7
        break;

    case DKMSG_NEWJOBS:                     // 18
        break;

    case DKMSG_GETADVJOBBYGATEWAY:          // 22
        break;

    case DKMSG_GETLASTFIRMWAREINFO:         // 27
        break;

    case DKMSG_FIRMWAREDOWNLOAD:            // 28
        break;

    case DKMSG_GETSERVERINFO:               // 29
        break;

    case DKMSG_NUMOFACTIVEUSERS:            // 31
        if (EventHeader.HTTPStatusCode == HTTP_OK &&
        	EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
        {
            nNumberOfGetNewJobs++;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, nNumberOfGetNewJobs:%d\n",
                             __FUNCTION__,
                             __LINE__,
                             nNumberOfGetNewJobs);

            if (MAX_GETNEWJOBS_BEFORE_DISCONNECT > 0 &&
                nNumberOfGetNewJobs > MAX_GETNEWJOBS_BEFORE_DISCONNECT)
            {
                DKServerManager_State_Suspend();
            }
        }
        break;

    case DKMSG_GETCURRENTUSER:              // 32
        break;

    case DKMSG_GETDATACONTAINERBYID:        // 34
        break;

    case DKMSG_CREATECONTAINERMDATA:        // 35
        break;

    case DKMSG_UPDATECONTAINERMDATA:        // 36
        break;

    case DKMSG_INSERTCONTAINERDDATA:        // 37
        break;

    case DKMSG_UPDATECONTAINERDDATA:        // 38
        break;

    case DKMSG_DELETECONTAINERDDATA:        // 39
        break;

    case DKMSG_DELETECONTAINERMDATA:        // 40
        break;

    case DKMSG_GETSERVERTIME:
        break;

    case DKMSG_SYSTEM_ERROR:                // 60
        break;

    case DKMSG_STATE_CHANGED:               // 63
        if (EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_READY)
        {
            if (fLogin == 0)
            {
                DKServerManager_GatewayLogin_Asynch(GetSourceOfOrigin(),
                                                    GetTaskIdNumber(),
                                                    (unsigned char *)dxAccount[hidx].AccessKey);
            }
        }
        else if (EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
        {
            fLogin = 0;
            nSuspendedTimes++;
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, nSuspendedTimes:%d\n",
                             __FUNCTION__,
                             __LINE__,
                             nSuspendedTimes);
            DKServerManager_State_Resume();
        }
        break;

    default:
        break;
    }

    DKServerManager_CleanEventArgumentObj(arg);

    return ;
}

//============================================================================
// main()
//============================================================================
int main()
{
    GSysDebuggerInit();

    DKServerManager_Init(DKServerEventReportHandler,
                         (unsigned char *)DeviceMAC,
                         sizeof(DeviceMAC),
                         (uint8_t *)dxAccount[hidx].HostName,
                         (uint8_t *)dxAccount[hidx].APPID,
                         (uint8_t *)dxAccount[hidx].APIKey,
                         (uint8_t *)dxAccount[hidx].APIVer);

    MAIN_PRINT("[%s] Line %d, GetRandomValue()=%d AccessKey=%s\n",
               __FUNCTION__,
               __LINE__,
               GetRandomValue(),
               dxAccount[hidx].AccessKey);

    dxTimeDelayMS(2000);
    DKServerManager_State_Resume();


    dxBOOL bExit = dxFALSE;
    while (bExit == dxFALSE)
    {
        dxTimeDelayMS(1000);
        //MAIN_PRINT("+");fflush(stdout);
    }

    MAIN_PRINT("[%s] Line %d, End of thread\n",
               __FUNCTION__,
               __LINE__);

    return 0;
}
