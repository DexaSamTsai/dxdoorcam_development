/*
 ============================================================================
 Name        : dxOSTestOVRtos.c
 Author      : 
 Version     :
 Copyright   : Dexatek Technology Ltd
 Description : DxOS Unit Test
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "dxOS.h"
#include "dxNET.h"

#define JOIN_RETURN_SUCCCESS		"SUCESS"
#define JOIN_RETURN_FAILED			"FAILED"

#define MAX_TEST_STRING_SIZE		128

#define TIME_TEST						0
#define ALLOC_TEST						0
#define THREAD_TEST						0
#define		THREAD_STACK_SIZE				(5*1024)
#define		THREAD_PRIORITY					DXTASK_PRIORITY_NORMAL
#define MESSAGE_QUEUE_TEST				0
#define WORKER_THREAD_TEST				0
#define 	WORKER_THREAD_QUEUE_SIZE		(10)
#define     WORKER_THREAD_TOTAL_TEST_EVT    (9)
#define MUTEX_TEST						0
#define SEMAPHORE_TEST					0
#define 	SEM_TEST_MAX_COUNT				(6)
#define 	SEM_TEST_INIT_VAL				(1)
#define TIMER_TEST						0
#define 	TOTAL_TIMER_CONCURRENT_TEST		(10)  //Make sure its 2 or greater
#define 	TIMER_BASE_PERIOD_MS			(100) //The base starting period for first timer
#define		TIMER_INTERVAL_INC				(100) //Incremental period after first timer
#define NETWORK_TEST                    1
#define     DNS_TEST                    0
#define     MULTICAST_TEST              1
#define     MULTICAST_SERVER_TEST             0
#define     UDP_TEST                    0
#define     UDP_SERVER_TEST             0
#define     TCP_TEST                    0
#define     TCP_SERVER_TEST             0



/*
 * Global Variable
 */
dxQueueHandle_t		MqTest1 = NULL;

char				MqString[][MAX_TEST_STRING_SIZE] = {"MqString01", "MqString02", "MqString03", "MqString04", "MqString05",
													    "MqString06", "MqString07", "MqString08", "MqString09", "MqString10"};

dxMutexHandle_t		MutexTest1 = NULL;
dxSemaphoreHandle_t SemaphoreTest1 = NULL;

uint32_t			WorkerThreadEventArgCount = 0;

/*
 * Function Definition
 */
static void* ThreadTest_Task1( void* arg );
static void* MQThreadTest_Task1( void* arg );
static void* MQThreadTest_Task2( void* arg );
static void* MutexThreadTest_TaskLock( void* arg );
static void* MutexThreadTest_TaskUnLock( void* arg );
static void* SemThreadTest_TaskGive( void* arg );
static void* SemThreadTest_TaskTake( void* arg );
void WorkerThreadTest_EventFunction(void* arg);
void TimerTestCallBack(void* arg);

/*********************************************************
 *             Color printing definitions
 *
 * If you want to print RED:
 * G_SYS_DBG_LOG_V(RED("- RED Color -\r\n"));
 * G_SYS_DBG_LOG_V1(RED("- RED Color - %d\r\n"), color);
 *********************************************************/
#define ESC_COLOR_NORMAL    "\033[m"
#define ESC_COLOR_BLACK     "\033[30m"
#define ESC_COLOR_RED       "\033[31m"
#define ESC_COLOR_GREEN     "\033[32m"
#define ESC_COLOR_YELLOW    "\033[33m"
#define ESC_COLOR_BLUE      "\033[34m"
#define ESC_COLOR_MAGENTA   "\033[35m"
#define ESC_COLOR_CYAN      "\033[36m"
#define ESC_COLOR_WHITE     "\033[37m"

#define NORMAL( x )         ESC_COLOR_NORMAL x
#define BLACK( x )          ESC_COLOR_BLACK  x ESC_COLOR_NORMAL
#define RED( x )            ESC_COLOR_RED    x ESC_COLOR_NORMAL
#define GREEN( x )          ESC_COLOR_GREEN  x ESC_COLOR_NORMAL
#define BLUE( x )           ESC_COLOR_BLUE   x ESC_COLOR_NORMAL
#define YELLOW( x )         ESC_COLOR_YELLOW   x ESC_COLOR_NORMAL



int dxOSTest_main(void) {

#if TIME_TEST

	printf("---------------------------\n");

	uint32_t TestDelaySec = 2;
	dxTime_t StartSystemTime = dxTimeGetMS();

	printf("[TIME TEST] dxTimeGetMS START (Expect Time Difference of %dms)\n", TestDelaySec*1000);
	if (StartSystemTime == 0)
	{
		printf(RED("[TIME TEST] dxTimeGetMS RESULT = FAILED! (dxTimeGetMS return 0)\n"));
	}
	else
	{
		sleep(TestDelaySec);

		dxTime_t CurrentSystemTime = dxTimeGetMS();
		if ((CurrentSystemTime - StartSystemTime) < TestDelaySec)
		{
			printf(RED("[TIME TEST] dxTimeGetMS RESULT = FAILED! (CurrentSystemTime - StartSystemTime should be greater than %d Sec)\n"), TestDelaySec);
		}
		else
		{
			printf(GREEN("[TIME TEST] dxTimeGetMS RESULT = PASS! (CurrentSystemTime - StartSystemTime difference is %dms)\n"), (unsigned int)(CurrentSystemTime - StartSystemTime));
		}
	}

	uint32_t TestDelaySecMs = 666;
	printf("[TIME TEST] dxTimeDelayMS START (Expect Time Difference of %dms)\n", TestDelaySecMs);
	StartSystemTime = dxTimeGetMS();
	if (StartSystemTime == 0)
	{
		printf(RED("[TIME TEST] dxTimeDelayMS RESULT = FAILED! (dxTimeGetMS return 0)\n"));
	}
	else
	{
		dxTimeDelayMS(TestDelaySecMs);

		dxTime_t CurrentSystemTime = dxTimeGetMS();
		if ((CurrentSystemTime - StartSystemTime) < TestDelaySec)
		{
			printf(RED("[TIME TEST] dxTimeDelayMS RESULT = FAILED! (CurrentSystemTime - StartSystemTime should be greater than %dms)\n"), TestDelaySecMs);
		}
		else
		{
			printf(GREEN("[TIME TEST] dxTimeDelayMS RESULT = PASS! (CurrentSystemTime - StartSystemTime difference is %dms)\n"), (unsigned int)(CurrentSystemTime - StartSystemTime));
		}
	}

	printf("---------------------------\n");

#endif

#if ALLOC_TEST

	printf("---------------------------\n");

	size_t SizeToAlloc = 2*1024*1024;
	void * MemAllocPool1 = dxMemAlloc("ALLOCTEST", 1, SizeToAlloc);

	if (MemAllocPool1)
		printf(GREEN("[ALLOC_TEST] dxMemAlloc PASS\n"));
	else
		printf(RED("[ALLOC_TEST] dxMemAlloc FAILED\n"));

	void * MemAllocPool2 = dxMemReAlloc("ALLOCTEST", MemAllocPool1, SizeToAlloc*2);
	if (MemAllocPool2)
		printf(GREEN("[ALLOC_TEST] dxMemReAlloc PASS\n"));
	else
		printf(RED("[ALLOC_TEST] dxMemReAlloc FAILED\n"));

	//TODO: We should test dxMemGetFreeHeapSize as well

	printf("---------------------------\n");

#endif

#if THREAD_TEST

	printf("---------------------------\n");

	dxTaskHandle_t ThreadTest_Task1Hdl;
	dxOS_RET_CODE TaskResult = dxTaskCreate(ThreadTest_Task1,
                          	 	 	 		"ThreadTest_Task1",
											THREAD_STACK_SIZE,
											0,
											THREAD_PRIORITY,
											&ThreadTest_Task1Hdl);
    if (TaskResult != DXOS_SUCCESS)
    {
    	printf(RED("[THREAD_TEST] dxTaskCreate FAILED\n"));
    }
    else
    {
		void* RetVal;
		pThread_info_t* pThread1Info = (pThread_info_t*)ThreadTest_Task1Hdl;
		pthread_join(pThread1Info->threadId, &RetVal);

		if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
		{
			printf(GREEN("[THREAD_TEST] dxTaskCreate PASS\n"));
		}
		else
		{
			printf(RED("[THREAD_TEST] dxTaskCreate FAILED (Unknown Response)\n"));
		}
    }

	printf("---------------------------\n");

#endif


#if MESSAGE_QUEUE_TEST

	printf("---------------------------\n");

	uint32_t  TotalQueueLen = sizeof(MqString)/sizeof(MqString[0]);

	MqTest1 = dxQueueCreate(TotalQueueLen, sizeof(MqString[0]));

	if (MqTest1)
	{
		dxTaskHandle_t MQThreadTest_Task1Hdl;
		dxOS_RET_CODE MqTaskResult = dxTaskCreate(MQThreadTest_Task1,
												"MQThreadTest_Task1",
												THREAD_STACK_SIZE,
												MqTest1,
												THREAD_PRIORITY,
												&MQThreadTest_Task1Hdl);
		if (MqTaskResult != DXOS_SUCCESS)
		{
			printf(RED("[MESSAGE_QUEUE_TEST] dxTaskCreate FAILED\n"));
		}
		else
		{
			void* RetVal;
			pThread_info_t* pMqThread1Info = (pThread_info_t*)MQThreadTest_Task1Hdl;
			pthread_join(pMqThread1Info->threadId, &RetVal);

			if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
			{
				dxTaskHandle_t MQThreadTest_Task2Hdl;
				dxOS_RET_CODE MqTask2Result = dxTaskCreate(MQThreadTest_Task2,
														"MQThreadTest_Task2",
														THREAD_STACK_SIZE,
														MqTest1,
														THREAD_PRIORITY,
														&MQThreadTest_Task2Hdl);
				if (MqTask2Result != DXOS_SUCCESS)
				{
					printf(RED("[MESSAGE_QUEUE_TEST] dxTaskCreate FAILED\n"));
				}
				else
				{
					//Just wait until it is terminated
					pThread_info_t* pMqThread2Info = (pThread_info_t*)MQThreadTest_Task2Hdl;
					pthread_join(pMqThread2Info->threadId, &RetVal);

					printf("[MESSAGE_QUEUE_TEST] dxQueueDelete START\n");
					if (dxQueueDelete(MqTest1) == DXOS_SUCCESS)
					{
						printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueDelete PASS\n"));
					}
					else
					{
						printf(RED("[MESSAGE_QUEUE_TEST] dxQueueDelete FAILED\n"));
					}

				}
			}
		}
	}
	else
	{
		printf(RED("[MESSAGE_QUEUE_TEST] dxQueueCreate FAILED\n"));
	}

	printf("---------------------------\n");

#endif

#if WORKER_THREAD_TEST

	printf("---------------------------\n");

	printf("[WORKER_THREAD_TEST] dxWorkerTaskCreate START\n");
	dxWorkweTask_t WorkerTask;
	if (dxWorkerTaskCreate(&WorkerTask, THREAD_PRIORITY, THREAD_STACK_SIZE, WORKER_THREAD_QUEUE_SIZE) != DXOS_SUCCESS)
	{
		printf(RED("[WORKER_THREAD_TEST] dxWorkerTaskCreate FAILED\n"));
		goto WorkerthreadtestExit;
	}

	printf(GREEN("[WORKER_THREAD_TEST] dxWorkerTaskCreate PASS\n"));

	printf("[WORKER_THREAD_TEST] dxWorkerTaskSendAsynchEvent START\n");
	uint32_t wi = 0;
	WorkerThreadEventArgCount = 0;
	for (wi = 0; wi < WORKER_THREAD_TOTAL_TEST_EVT; wi++)
	{
		if (dxWorkerTaskSendAsynchEvent(&WorkerTask, WorkerThreadTest_EventFunction, &WorkerThreadEventArgCount) != DXOS_SUCCESS)
		{
			printf(RED("[WORKER_THREAD_TEST] dxWorkerTaskSendAsynchEvent(innerLoop) FAILED\n"));
			goto WorkerthreadtestExit;
		}
	}

	//Sleep a bit so that all worker thread finish the process
	sleep(2);

	if (WorkerThreadEventArgCount == WORKER_THREAD_TOTAL_TEST_EVT)
		printf(GREEN("[WORKER_THREAD_TEST] dxWorkerTaskSendAsynchEvent PASS\n"));
	else
		printf(RED("[WORKER_THREAD_TEST] dxWorkerTaskSendAsynchEvent FAILED\n"));

	printf("[WORKER_THREAD_TEST] dxWorkerTaskDelete START\n");
	if (dxWorkerTaskDelete( &WorkerTask) == DXOS_SUCCESS)
		printf(GREEN("[WORKER_THREAD_TEST] dxWorkerTaskDelete PASS\n"));
	else
		printf(RED("[WORKER_THREAD_TEST] dxWorkerTaskDelete FAILED\n"));

	printf("---------------------------\n");

WorkerthreadtestExit:

#endif

#if MUTEX_TEST

	printf("---------------------------\n");

	MutexTest1 = dxMutexCreate();

	if (MutexTest1)
	{
		dxTaskHandle_t MutexThreadTest_Task1Hdl;
		dxOS_RET_CODE MutexTaskResult = dxTaskCreate(MutexThreadTest_TaskLock,
												"MutexThreadTest_TaskLock",
												THREAD_STACK_SIZE,
												MutexTest1,
												THREAD_PRIORITY,
												&MutexThreadTest_Task1Hdl);
		if (MutexTaskResult != DXOS_SUCCESS)
		{
			printf(RED("[MUTEX_TEST] dxTaskCreate FAILED\n"));
		}
		else
		{
			//Wait until MutexThreadTest_TaskLock is terminated
			void* RetVal;
			pThread_info_t* pMutexThread1Info = (pThread_info_t*)MutexThreadTest_Task1Hdl;
			pthread_join(pMutexThread1Info->threadId, &RetVal);
			if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
			{
				dxTaskHandle_t MutexThreadTest_Task2Hdl;
				dxOS_RET_CODE MutexTask2Result = dxTaskCreate(MutexThreadTest_TaskUnLock,
														"MutexThreadTest_TaskUnLock",
														THREAD_STACK_SIZE,
														MutexTest1,
														THREAD_PRIORITY,
														&MutexThreadTest_Task2Hdl);
				if (MutexTask2Result != DXOS_SUCCESS)
				{
					printf(RED("[MUTEX_TEST] dxTaskCreate FAILED\n"));
				}
				else
				{
					//Just wait until it is terminated
					pThread_info_t* pMutexThread2Info = (pThread_info_t*)MutexThreadTest_Task2Hdl;
					pthread_join(pMutexThread2Info->threadId, &RetVal);
					if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
					{
						printf("[MUTEX_TEST] dxMutexDelete START\n");
						if (dxMutexDelete(MutexTest1) == DXOS_SUCCESS)
						{
							printf(GREEN("[MUTEX_TEST] dxMutexDelete PASS\n"));
						}
						else
						{
							printf(RED("[MUTEX_TEST] dxMutexDelete FAILED\n"));
						}
					}
				}
			}
		}

	}
	else
	{
		printf(RED("[MUTEX_TEST] dxMutexCreate FAILED\n"));
	}

	printf("---------------------------\n");

#endif

#if SEMAPHORE_TEST

	printf("---------------------------\n");

	printf("[SEMAPHORE_TEST] dxOS_Init START (dxOS_Init is required)\n");
	if  (dxOS_Init() != DXOS_SUCCESS)
	{
		printf(RED("[SEMAPHORE_TEST] dxOS_Init FAILED\n"));
		goto TestExit;
	}
	printf(GREEN("[SEMAPHORE_TEST] dxOS_Init PASS\n"));


	SemaphoreTest1 = dxSemCreate(SEM_TEST_MAX_COUNT, SEM_TEST_INIT_VAL);

	if (SemaphoreTest1)
	{
		dxTaskHandle_t SemThreadTest_Task1Hdl;
		dxOS_RET_CODE SemTaskResult = dxTaskCreate(SemThreadTest_TaskTake,
												"SemThreadTest_TaskTake",
												THREAD_STACK_SIZE,
												SemaphoreTest1,
												THREAD_PRIORITY,
												&SemThreadTest_Task1Hdl);
		if (SemTaskResult != DXOS_SUCCESS)
		{
			printf(RED("[SEMAPHORE_TEST] dxTaskCreate FAILED\n"));
		}
		else
		{
			//Wait until SemThreadTest_TaskTake is terminated
			void* RetVal;
			pThread_info_t* pSemThread1Info = (pThread_info_t*)SemThreadTest_Task1Hdl;
			pthread_join(pSemThread1Info->threadId, &RetVal);
			if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
			{
				dxTaskHandle_t SemThreadTest_Task2Hdl;
				dxOS_RET_CODE SemTask2Result = dxTaskCreate(SemThreadTest_TaskGive,
														"SemThreadTest_TaskGive",
														THREAD_STACK_SIZE,
														SemaphoreTest1,
														THREAD_PRIORITY,
														&SemThreadTest_Task2Hdl);
				if (SemTask2Result != DXOS_SUCCESS)
				{
					printf(RED("[SEMAPHORE_TEST] dxTaskCreate FAILED\n"));
				}
				else
				{
					//Just wait until it is terminated
					pThread_info_t* pSemThread2Info = (pThread_info_t*)SemThreadTest_Task2Hdl;
					pthread_join(pSemThread2Info->threadId, &RetVal);
					if (strcmp(JOIN_RETURN_SUCCCESS,(char*)RetVal) == 0)
					{
						printf("[SEMAPHORE_TEST] dxSemDelete START\n");
						if (dxSemDelete(SemaphoreTest1) == DXOS_SUCCESS)
						{
							printf(GREEN("[SEMAPHORE_TEST] dxSemDelete PASS\n"));
						}
						else
						{
							printf(RED("[SEMAPHORE_TEST] dxSemDelete FAILED\n"));
						}
					}
				}
			}
		}

	}
	else
	{
		printf(RED("[SEMAPHORE_TEST] dxSemCreate FAILED\n"));
	}


	printf("---------------------------\n");

#endif

#if TIMER_TEST

	static dxTimerHandle_t TimerHdlArray[TOTAL_TIMER_CONCURRENT_TEST];
	static uint32_t TimerArg[TOTAL_TIMER_CONCURRENT_TEST];
	uint32_t i = 0;
	uint32_t TimerPeriod = TIMER_BASE_PERIOD_MS;

	printf("[TIMER_TEST] dxTimerCreate START\n");
	for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
	{
		TimerHdlArray[i] = NULL;
		TimerArg[i] = 0;
		TimerPeriod = TIMER_BASE_PERIOD_MS + (TIMER_INTERVAL_INC*i);
		TimerHdlArray[i] = dxTimerCreate("MyTimer",
										 TimerPeriod,
										 dxTRUE,
										 &TimerArg[i],
										 TimerTestCallBack);

		if (TimerHdlArray[i] == NULL)
		{
			printf(RED("[TIMER_TEST] dxTimerCreate FAILED\n"));
			goto TestExit;
		}
	}
	printf(GREEN("[TIMER_TEST] dxTimerCreate DONE\n"));

	printf("[TIMER_TEST] dxTimerStart START\n");
	for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
	{
		if (dxTimerStart(TimerHdlArray[i], 0) != DXOS_SUCCESS)
		{
			printf(RED("[TIMER_TEST] dxTimerStart FAILED\n"));
			goto TestExit;
		}
	}
	printf(GREEN("[TIMER_TEST] dxTimerStart DONE\n"));

	//Timer started, we sleep to let timer take effect to count Arg.
	uint32_t TimeToSleep = (TIMER_BASE_PERIOD_MS + (TIMER_INTERVAL_INC*TOTAL_TIMER_CONCURRENT_TEST))*3;
	dxTimeDelayMS(TimeToSleep);

	printf("[TIMER_TEST] dxTimerIsTimerActive START\n");
	for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
	{
		if (dxTimerIsTimerActive(TimerHdlArray[i]) != dxTRUE)
		{
			printf(RED("[TIMER_TEST] dxTimerIsTimerActive FAILED (%d)\n"), i);
			goto TestExit;
		}
	}
	printf(GREEN("[TIMER_TEST] dxTimerIsTimerActive PASS\n"));

	printf("[TIMER_TEST] dxTimerStop START\n");
	for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
	{
		if (dxTimerStop(TimerHdlArray[i], 0) != DXOS_SUCCESS)
		{
			printf(RED("[TIMER_TEST] dxTimerStop FAILED\n"));
			goto TestExit;
		}
	}
	printf(GREEN("[TIMER_TEST] dxTimerStop DONE\n"));

	//After above timer stop we sleep again which the upcoming test will check total count of each timer arg to make sure timer is indeed stopped.
	dxTimeDelayMS(TimeToSleep);

	printf("[TIMER_TEST] Checking All created timer START (count within +-1 tolerance)\n");
	uint32_t	CheckAllPass = 1;
	for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
	{
		uint32_t TimerActualCount = TimerArg[i];
		uint32_t TimerExpectedCount = TimeToSleep / (TIMER_BASE_PERIOD_MS+(TIMER_INTERVAL_INC*i));

		//Just make sure the expected zero count is at least 1
		if (TimerExpectedCount == 0)
			TimerExpectedCount++;

		if ((TimerActualCount < (TimerExpectedCount-1)) ||
			(TimerActualCount > (TimerExpectedCount+1)) )
		{
			printf(RED("[TIMER_TEST] Timer %d Check FAILED (TimerActualCount=%d, TimerExpectedCount=%d)\n"), i, TimerActualCount, TimerExpectedCount);
			CheckAllPass = 0;
		}
		else
		{
			printf(GREEN("[TIMER_TEST] Timer %d Check PASS (TimerActualCount=%d, TimerExpectedCount=%d)\n"), i, TimerActualCount, TimerExpectedCount);
		}
	}
	printf(GREEN("[TIMER_TEST] Timer Check All DONE (See above result)\n"));

	if (CheckAllPass)
	{
		printf("[TIMER_TEST] dxTimerRestart START\n");
		for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
		{
			TimerArg[i] = 0;
			TimerPeriod = TIMER_BASE_PERIOD_MS + (TIMER_INTERVAL_INC*i*2);
			if (dxTimerRestart(TimerHdlArray[i], TimerPeriod, 0) != DXOS_SUCCESS)
			{
				printf(RED("[TIMER_TEST] dxTimerRestart FAILED\n"));
				goto TestExit;
			}
		}
		printf(GREEN("[TIMER_TEST] dxTimerRestart DONE\n"));

		//Timer Re-Started, we sleep to let timer take effect to count Arg.
		dxTimeDelayMS(TimeToSleep);

		printf("[TIMER_TEST] Checking All restarted timer START (count within +-1 tolerance)\n");
		for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
		{
			uint32_t TimerActualCount = TimerArg[i];
			uint32_t TimerExpectedCount = TimeToSleep / (TIMER_BASE_PERIOD_MS+(TIMER_INTERVAL_INC*i*2));

			//Just make sure the expected zero count is at least 1
			if (TimerExpectedCount == 0)
				TimerExpectedCount++;

			if ((TimerActualCount < (TimerExpectedCount-1)) ||
				(TimerActualCount > (TimerExpectedCount+1)) )
			{
				printf(RED("[TIMER_TEST] RestartTimer %d Check FAILED (TimerActualCount=%d, TimerExpectedCount=%d)\n"), i, TimerActualCount, TimerExpectedCount);

			}
			else
			{
				printf(GREEN("[TIMER_TEST] RestartTimer %d Check PASS (TimerActualCount=%d, TimerExpectedCount=%d)\n"), i, TimerActualCount, TimerExpectedCount);
			}
		}
		printf(GREEN("[TIMER_TEST] Restart Timer Check All DONE (See above result)\n"));

		printf("[TIMER_TEST] dxTimerDelete START\n");
		for (i= 0; i < TOTAL_TIMER_CONCURRENT_TEST; i++)
		{
			if (dxTimerDelete(TimerHdlArray[i], 0) != DXOS_SUCCESS)
			{
				printf(RED("[TIMER_TEST] dxTimerDelete FAILED\n"));
				goto TestExit;
			}
		}
		printf(GREEN("[TIMER_TEST] dxTimerDelete PASS\n"));

	}

#endif

#if NETWORK_TEST
    #define DXWIFI_MODE_STA 1
    int netret=0;
    static char *test_ssid	= "DK_DOORCAM_TEST";
    static char *test_pwd	= "1234567890";
    static char test_security	= 7;

    printf("[NETWORK_TEST]Start!\n");
    printf("[NETWORK_TEST][WIFI TEST]Start!\n");

    netret = dxWiFi_On( DXWIFI_MODE_STA );

    if(netret < 0)
    {
        printf("[ERROR][dxWiFi_On][%d]\n",netret);
        printf("[NETWORK_TEST][WIFI TEST]FAIL!\n");
        return -1;
    }

    netret = dxWiFi_Connect( test_ssid, test_security, test_pwd, 0, NULL );
    
    if(netret < 0)
    {
        printf("[ERROR][dxWiFi_On][%d]\n",netret);
        printf("[NETWORK_TEST][WIFI TEST]FAIL!\n");
        return -1;
    }

    printf("[NETWORK_TEST][WIFI TEST]PASS!\n");

#if DNS_TEST

    printf("[NETWORK_TEST][DNS TEST]Start!\n");
    gethostbyname("www.google.com.tw");
    printf("[NETWORK_TEST][DNS TEST]FINSH!\n");
#endif

#if MULTICAST_TEST

    printf("[NETWORK_TEST][DK_MCAST_TEST]Start!\n");

    dxUDPHandle_t      Mcast_Socke;
    dxIP_Addr_t Mcast_IpGroup;
    
    uint16_t Mcast_bufsize = 256;
    uint8_t* Mcast_Buf = dxMemAlloc( "Mcast_bufsize", 1, Mcast_bufsize);
    
    memset( &Mcast_IpGroup, 0, sizeof(Mcast_IpGroup) );
    Mcast_IpGroup.version = DXIP_VERSION_V4;
    Mcast_IpGroup.v4[0] = 224;
    Mcast_IpGroup.v4[1] = 6;
    Mcast_IpGroup.v4[2] = 6;
    Mcast_IpGroup.v4[3] = 6;

    dxMCAST_Init();
    Mcast_Socke = dxMCAST_Join(Mcast_IpGroup, 59780);

    int Mcast_i=0;
#if 0
    while(1)
    {
    sprintf(Mcast_Buf, "QQQ%d", Mcast_i);
        if (dxMCAST_SendTo(Mcast_Socke, Mcast_Buf, Mcast_bufsize, NULL) <= 0)
        {
                printf("UDP packet send failed\n");
        }
        Mcast_i++;
        if(Mcast_i >=1000){
            Mcast_i=0;
        }
        sleep(1);
    }
  #endif  
#endif

#if MULTICAST_SERVER_TEST
    
    #define UDPM_DATA "BAD_CAT_UDPM_TEST"
    char *McastSer_buf = malloc(512);
    int McastSer_socketfd; 
    int McastSer_size;
    int McastSer_n;
    int McastSer_yes=1;
    struct ip_mreq McastSer_mreq;
    struct sockaddr_in McastSer_addr;
            
    printf("[NETWORK_TEST][UDP MC TEST]Start!\n");
    if ((McastSer_socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        printf("[ERROR][socket][%d]\n",netret);
        printf("[NETWORK_TEST][UDP MC TEST]FAIL!\n");
        return -1;
    }
    printf("[NETWORK_TEST][UDP MC TEST]TEST4!\n");
    
    bzero((char *)&McastSer_addr, sizeof(McastSer_addr));
    McastSer_addr.sin_family = AF_INET;
    McastSer_addr.sin_port = htons(5001);
    McastSer_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if (setsockopt(McastSer_socketfd, SOL_SOCKET, SO_REUSEADDR, &McastSer_yes, sizeof(McastSer_yes)) < 0) {
        perror("reuseaddr setsockopt");
        exit(1);
    }
    
    if (bind(McastSer_socketfd, (struct sockaddr*)&McastSer_addr, sizeof(McastSer_addr)) < 0) {
        perror ("bind");
        exit(1);
    }
    
    /* Preparatios for using Multicast */ 
    McastSer_mreq.imr_multiaddr.s_addr = inet_addr("224.5.5.5");
    McastSer_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    
    /* Tell the kernel we want to join that multicast group. */
    if (setsockopt(McastSer_socketfd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &McastSer_mreq, sizeof(McastSer_mreq)) < 0) {
        perror ("add_membership setsockopt");
        exit(1);
    }
    
    printf("[NETWORK_TEST][UDP MC TEST]TEST1!\n");
    for (;;) {
        McastSer_size=sizeof(McastSer_addr);
        printf("[NETWORK_TEST][UDP MC TEST]TEST2!\n");
        if ( McastSer_n=recvfrom(McastSer_socketfd, McastSer_buf, 512, 0, (struct sockaddr*)&McastSer_addr, &McastSer_size) < 0) {
            perror ("recv");
            printf("[NETWORK_TEST][UDP MC TEST]TEST3!\n");
            //exit(1);
        }
        McastSer_buf[McastSer_n] = '\0'; /* null-terminate string */
        printf("Received message [%s] size=%d !!\n", McastSer_buf, McastSer_n);
    }
    printf("[NETWORK_TEST][TCP MC TEST]FINSH!\n");
        
#endif

#if UDP_TEST

    #define UDP_DATA "BAD_CAT_UDP_TEST"
    char *udp_buf = malloc(512);
    int udp_socketfd; 
    int udp_size;
    struct sockaddr_in udp_addr;
    
    printf("[NETWORK_TEST][UDP TEST]Start!\n");

    if ((udp_socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        printf("[ERROR][socket][%d]\n",netret);
        printf("[NETWORK_TEST][UDP TEST]FAIL!\n");
        return -1;
    }
    
    bzero((char *)&udp_addr, sizeof(udp_addr));
    udp_addr.sin_family = AF_INET;
    udp_addr.sin_port = htons(4001);
    udp_addr.sin_addr.s_addr = inet_addr("172.16.0.238");
    
    udp_size = sizeof(udp_addr);
    if (sendto(udp_socketfd, UDP_DATA, sizeof(UDP_DATA), 0, (struct sockaddr*)&udp_addr, udp_size) == -1) {
        printf("[ERROR][sendto][%d]\n",netret);
        printf("[NETWORK_TEST][UDP TEST]FAIL!\n");
        return -1;
    }
    
    printf("[%s]\n", udp_buf);
    printf("[NETWORK_TEST][TCP TEST]FINSH!\n");

#endif


#if UDP_SERVER_TEST

    int udpsvr_socketfd;   
    int udpsvr_length;
    int udpsvr_nbytes; 
    char *udpsvr_buf = malloc(128);
    struct sockaddr_in udpsvr_addr_server;
    struct sockaddr_in udpsvr_addr_cleint; 
    printf("[NETWORK_TEST][UDP SERVER TEST]Start!\n");

    if ((udpsvr_socketfd = socket(AF_INET, SOCK_DGRAM, 0)) <0) {
        perror ("socket failed");
        exit(EXIT_FAILURE);
    }
    
    bzero ((char *)&udpsvr_addr_server, sizeof(udpsvr_addr_server));
    udpsvr_addr_server.sin_family = AF_INET;
    udpsvr_addr_server.sin_addr.s_addr = htonl(INADDR_ANY);
    udpsvr_addr_server.sin_port = htons(3001);
    
    if (bind(udpsvr_socketfd, (struct sockaddr *)&udpsvr_addr_server, sizeof(udpsvr_addr_server)) <0) {
        perror ("bind failed\n");
        exit(1);
    }
    
    udpsvr_length = sizeof(udpsvr_addr_server);
    printf("Server is ready to receive !!\n");
    while (1) {
        if ((udpsvr_nbytes = recvfrom(udpsvr_socketfd, udpsvr_buf, 128, 0, (struct sockaddr*)&udpsvr_addr_cleint, (socklen_t *)&udpsvr_length)) <0) {
            perror ("could not read datagram!!");
            continue;
        }
       printf("[LEON_TEST]001\n");
        printf("%s\n", udpsvr_buf);
    
    }

#endif

#if TCP_TEST
    printf("[NETWORK_TEST][TCP TEST]Start!\n");

#define HTTP_HEADER "GET / HTTP/1.1\r\nHost: www.google.com.tw\r\nAccept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8\r\nAccept-Encoding: gzip, deflate\r\nAccept-Language: zh-TW,zh;q=0.9,en-US;q=0.8,en;q=0.7\r\nUser-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36\r\n"
      #define TCP_DATA "BAD_CAT_TCP_TEST"

//Setp1 Init socket 
    char *tcp_buf = malloc(512);
    int tcp_socketfd=0;

    netret = socket(AF_INET, SOCK_STREAM, 0);
    if(netret < 0)
    {
        printf("[ERROR][socket][%d]\n",netret);
        printf("[NETWORK_TEST][TCP TEST]FAIL!\n");
        return -1;
    }
    tcp_socketfd=netret;
    
    struct sockaddr_in tcp_addr;
    bzero(&tcp_addr,sizeof(tcp_addr));
    tcp_addr.sin_family = AF_INET;
    tcp_addr.sin_port = htons(8888);
    tcp_addr.sin_addr.s_addr = inet_addr("172.16.0.238");

    //Setp2 connect server
    netret = connect(tcp_socketfd, (struct sockaddr *)&tcp_addr, sizeof(struct sockaddr));

    if(netret < 0)
    {
        printf("[ERROR][connect][%d]\n",netret);
        printf("[NETWORK_TEST][TCP TEST]FAIL!\n");
        return -1;
    }

    netret = send(tcp_socketfd, HTTP_HEADER, sizeof(HTTP_HEADER), 0);
    if(netret < 0)
    {
        printf("[ERROR][send][%d]\n",netret);
        printf("[NETWORK_TEST][TCP TEST]FAIL!\n");
        return -1;
    }
  
    netret = recv(tcp_socketfd, tcp_buf, 512, 0);
    if(netret < 0)
    {
        printf("[ERROR][recv][%d]\n",netret);
        printf("[NETWORK_TEST][TCP TEST]FAIL!\n");
        return -1;
    }

    printf("GET [%s]\n", tcp_buf);
    printf("[NETWORK_TEST][TCP TEST]FINSH!\n");
#endif     

#if TCP_SERVER_TEST

    char *tcpser_buf = malloc(512);
    int tcpser_socketfd_server=0;
    int tcpser_socketfd_client=0;
    int tcpser_len=0;

    if ((tcpser_socketfd_server = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Socket()\n");
        return -1;
    }

    /*
     * Bind the socket to the server address.
     */
     struct sockaddr_in tcpser_server;
     struct sockaddr_in tcpser_cleint;
    tcpser_server.sin_family = AF_INET;
    tcpser_server.sin_port   = htons(80);
    tcpser_server.sin_addr.s_addr = INADDR_ANY;

    if (bind(tcpser_socketfd_server, (struct sockaddr *)&tcpser_server, sizeof(tcpser_server)) < 0)
    {
        printf("Bind()\n");
        return -1;
    }

    /*
     * Listen for connections. Specify the backlog as 1.
     */
    if (listen(tcpser_socketfd_server, 1) != 0)
    {
        printf("Listen()\n");
        return -1;
    }

    /*
     * Accept a connection.
     */
    tcpser_len = sizeof(tcpser_cleint);
    if ((tcpser_socketfd_client = accept(tcpser_socketfd_server, (struct sockaddr *)&tcpser_cleint, &tcpser_len)) == -1)
    {
        printf("Accept()\n");
        return -1;
    }

#if 0
    
    /*
     * Receive the message on the newly connected socket.
     */
    if (recv(tcpser_socketfd_client, tcpser_buf, sizeof(tcpser_buf), 0) == -1)
    {
        printf("Recv()\n");
		return -1;
	}
    

    /*
     * Send the message back to the client.
     */
    if (send(tcpser_socketfd_client, tcpser_buf, sizeof(tcpser_buf), 0) < 0)
    {
        printf("Send()\n");
		return -1;
	}
#endif     
    printf("Disconnect......\n");
    sleep(10);
    close(tcpser_socketfd_client);
    close(tcpser_socketfd_server);

    printf("Server ended successfully\n");
#endif

    sleep(15);
    printf( "WIFI OFF" );
    dxWiFi_Off();
    printf( "WIFI OFF OK" );
#endif

TestExit:

	return EXIT_SUCCESS;
}

void TimerTestCallBack(void* arg)
{
	uint32_t* pCount = (uint32_t*)arg;
	*pCount += 1;

	return;
}


void WorkerThreadTest_EventFunction(void* arg)
{
	uint32_t* pCount = (uint32_t*)arg;
	*pCount += 1;

	return;
}

static void* SemThreadTest_TaskTake( void* arg )
{
	printf("[SEMAPHORE_TEST] dxTaskCreate SemThreadTest_TaskTake Says Hello!\n");
	uint32_t AllSuccess = 1;

	if (arg)
	{
		dxSemaphoreHandle_t	ArgSemTest = (dxSemaphoreHandle_t)arg;

		printf("[SEMAPHORE_TEST] dxSemTake START\n");

		uint32_t i = 0;
		for (i = 0; i < SEM_TEST_INIT_VAL; i++)
		{
			if (dxSemTake(ArgSemTest, 1000) != DXOS_SUCCESS)
			{
				printf(RED("[SEMAPHORE_TEST] dxSemTake FAILED\n"));
				AllSuccess = 0;
				goto SemThreadTakeExit;
			}
		}

		printf(GREEN("[SEMAPHORE_TEST] dxSemTake PASS\n"));


		printf("[SEMAPHORE_TEST] dxSemTake TimeOut START (@%dms)\n", (unsigned int)dxTimeGetMS());
		if (dxSemTake(ArgSemTest, 1000) != DXOS_TIMEOUT)
		{
			printf(RED("[SEMAPHORE_TEST] dxSemTake TimeOut FAILED Expected TimeOut (@%dms)\n"), (unsigned int)dxTimeGetMS());
			AllSuccess = 0;
			goto SemThreadTakeExit;
		}
		else
		{
			printf(GREEN("[SEMAPHORE_TEST] dxSemTake TimeOut PASS (@%dms)\n"), (unsigned int)dxTimeGetMS());
		}

	}
	else
	{
		AllSuccess = 0;
	}


SemThreadTakeExit:
	if (AllSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;
}


static void* SemThreadTest_TaskGive( void* arg )
{
	printf("[SEMAPHORE_TEST] dxTaskCreate SemThreadTest_TaskGive Says Hello!\n");
	uint32_t AllSuccess = 1;

	if (arg)
	{
		dxSemaphoreHandle_t	ArgSemTest = (dxSemaphoreHandle_t)arg;

		printf("[SEMAPHORE_TEST] dxSemGive All START\n");

		//Give Semaphore "exceeding" the max count
		uint32_t i = 0;
		for (i = 0; i < (SEM_TEST_MAX_COUNT + 2); i++)
		{
			if (dxSemGive(ArgSemTest) != DXOS_SUCCESS)
			{
				printf(RED("[SEMAPHORE_TEST] dxSemGive All FAILED\n"));
				AllSuccess = 0;
				goto SemThreadGiveExit;
			}
		}

		printf(GREEN("[SEMAPHORE_TEST] dxSemGive All PASS\n"));

		printf("[SEMAPHORE_TEST] dxSemTake All START\n");
		//Take the semaphore back up to max count
		for (i = 0; i < (SEM_TEST_MAX_COUNT); i++)
		{
			if (dxSemTake(ArgSemTest, 1000) != DXOS_SUCCESS)
			{
				printf(RED("[SEMAPHORE_TEST] dxSemTake All FAILED\n"));
				AllSuccess = 0;
				goto SemThreadGiveExit;
			}
		}
		printf(GREEN("[SEMAPHORE_TEST] dxSemTake All PASS\n"));

		printf("[SEMAPHORE_TEST] dxSemTake TimeOut START (We expect to TimeOut) (@%d)\n", (unsigned int)dxTimeGetMS());
		if (dxSemTake(ArgSemTest, 1000) == DXOS_TIMEOUT)
		{
			printf(GREEN("[SEMAPHORE_TEST] dxSemTake TimeOut PASS (@%d)\n"), (unsigned int)dxTimeGetMS());
		}
		else
		{
			printf(RED("[SEMAPHORE_TEST] dxSemTake TimeOut FAILED (@%d)\n"), (unsigned int)dxTimeGetMS());
			AllSuccess = 0;
			goto SemThreadGiveExit;
		}
	}

SemThreadGiveExit:
	if (AllSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;
}



static void* MutexThreadTest_TaskLock( void* arg )
{
	printf("[MUTEX_TEST] dxTaskCreate MutexThreadTest_TaskLock Says Hello!\n");
	uint32_t AllSuccess = 1;

	if (arg)
	{
		printf("[MUTEX_TEST] dxMutexTake START\n");
		dxMutexHandle_t	ArgMutexTest = (dxMutexHandle_t)arg;
		if (dxMutexTake(ArgMutexTest, 1000) != DXOS_SUCCESS)
		{
			AllSuccess = 0;
			printf(RED("[MUTEX_TEST] dxMutexTake FAILED\n"));
		}
		else
		{
			printf(GREEN("[MUTEX_TEST] dxMutexTake SUCCESS\n"));
		}
	}
	else
	{
		AllSuccess = 0;
	}

	if (AllSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;
}


static void* MutexThreadTest_TaskUnLock( void* arg )
{
	printf("[MUTEX_TEST] dxTaskCreate MutexThreadTest_TaskUnLock Says Hello!\n");
	uint32_t AllSuccess = 1;

	if (arg)
	{
		printf("[MUTEX_TEST] dxMutexTake TimeOut START (@%dms)\n", (unsigned int)dxTimeGetMS());
		dxMutexHandle_t	ArgMutexTest = (dxMutexHandle_t)arg;
		if (dxMutexTake(ArgMutexTest, 1000) == DXOS_TIMEOUT)
		{
			printf(GREEN("[MUTEX_TEST] dxMutexTake TimeOut SUCESS (@%dms)\n"), (unsigned int)dxTimeGetMS());

			printf("[MUTEX_TEST] dxMutexGive START\n");
			if (dxMutexGive(ArgMutexTest) == DXOS_SUCCESS)
			{
				printf(GREEN("[MUTEX_TEST] dxMutexGive PASS\n"));

				printf("[MUTEX_TEST] dxMutexTake ReTake START\n");
				if (dxMutexTake(ArgMutexTest, 1000) == DXOS_SUCCESS)
				{
					printf(GREEN("[MUTEX_TEST] dxMutexTake ReTake PASS\n"));
					dxMutexGive(ArgMutexTest); //We release it so that it can be destroyed after exiting this thread
				}
				else
				{
					AllSuccess = 0;
					printf(RED("[MUTEX_TEST] dxMutexTake ReTake FAILED\n"));
				}
			}
			else
			{
				AllSuccess = 0;
				printf(RED("[MUTEX_TEST] dxMutexGive FAILEDT\n"));
			}
		}
		else
		{
			AllSuccess = 0;
			printf(RED("[MUTEX_TEST] dxMutexTake TimeOut FAILED (Expect to timeout taking Mutex)(@%d)\n"), (unsigned int)dxTimeGetMS());
		}
	}
	else
	{
		AllSuccess = 0;
	}

	if (AllSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;

}


static void* MQThreadTest_Task1( void* arg )
{
	printf("[MESSAGE_QUEUE_TEST] dxTaskCreate MQThreadTest_Task1 Says Hello!\n");
	uint32_t AllSuccess = 1;

	if (arg)
	{
		uint32_t i = 0;
		dxQueueHandle_t	ArgMqTest = (dxQueueHandle_t)arg;

		uint32_t  TotalQueueToSend = sizeof(MqString)/sizeof(MqString[0]);

		printf("[MESSAGE_QUEUE_TEST] dxQueueSpacesAvailable START\n");
		uint32_t AvailableSpace = dxQueueSpacesAvailable(ArgMqTest);
		if (AvailableSpace == TotalQueueToSend)
		{
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueSpacesAvailable PASS\n"));
		}
		else
		{
			AllSuccess = 0;
			printf(RED("[MESSAGE_QUEUE_TEST] dxQueueSpacesAvailable FAILED\n"));
		}

		printf("[MESSAGE_QUEUE_TEST] dxQueueIsEmpty START\n");
		if (dxQueueIsEmpty(ArgMqTest) == dxTRUE) //We expect as TRUE
		{
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueIsEmpty PASS\n"));
		}
		else
		{
			AllSuccess = 0;
			printf(RED("[MESSAGE_QUEUE_TEST] dxQueueIsEmpty FAILED\n"));
		}

		printf("[MESSAGE_QUEUE_TEST] dxQueueIsFull START\n");
		if (dxQueueIsFull(ArgMqTest) == dxFALSE) //We expect as FALSE
		{
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueIsFull PASS\n"));
		}
		else
		{
			AllSuccess = 0;
			printf(RED("[MESSAGE_QUEUE_TEST] dxQueueIsFull FAILED\n"));
		}


		printf("[MESSAGE_QUEUE_TEST] dxQueueSend START\n");
		//Send all message queue
		for (i = 0; i < TotalQueueToSend; i++)
		{
			dxOS_RET_CODE MqSendRes = dxQueueSend(	ArgMqTest,
													MqString[i],
													1000);

			if (MqSendRes != DXOS_SUCCESS)
			{
				AllSuccess = 0;
				printf(RED("[MESSAGE_QUEUE_TEST] dxQueueSend FAILED\n"));
				break;
			}
		}

		if (AllSuccess)
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueSend DONE (Awaiting Receive Confirmation)\n"));

	}
	else
	{
		AllSuccess = 0;
	}

	if (AllSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;

}

static void* MQThreadTest_Task2( void* arg )
{

	printf("[MESSAGE_QUEUE_TEST] dxTaskCreate MQThreadTest_Task2 Says Hello!\n");
	uint32_t AllCheckSuccess = 1;

	if (arg)
	{
		uint32_t i = 0;

		char pvBuffer[MAX_TEST_STRING_SIZE];
		dxQueueHandle_t	ArgMqTest = (dxQueueHandle_t)arg;

		uint32_t  TotalQueueToSend = sizeof(MqString)/sizeof(MqString[0]);

		printf("[MESSAGE_QUEUE_TEST] dxQueueReceive START\n");
		//Receive all message queue
		for (i = 0; i < TotalQueueToSend; i++)
		{
			memset(pvBuffer, 0, MAX_TEST_STRING_SIZE);
			dxOS_RET_CODE MqRcvRes = dxQueueReceive(ArgMqTest,
													pvBuffer,
													10000);

			if (MqRcvRes != DXOS_SUCCESS)
			{
				AllCheckSuccess = 0;
				printf(RED("[MESSAGE_QUEUE_TEST] dxQueueSend FAILED\n"));
				break;
			}
			else
			{
				if (strcmp(pvBuffer, MqString[i]) != 0)
				{
					AllCheckSuccess = 0;
					printf(RED("[MESSAGE_QUEUE_TEST] dxQueueSend Verify FAILED (Possible Send Issue)\n"));
					printf(RED("[MESSAGE_QUEUE_TEST] dxQueueReceive Check FAILED\n"));
					break;
				}
			}
		}

		if (AllCheckSuccess)
		{
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueSend Verify PASS!\n"));
			printf(GREEN("[MESSAGE_QUEUE_TEST] dxQueueReceive Check PASS!\n"));
		}
	}
	else
	{
		AllCheckSuccess = 0;
	}

	if (AllCheckSuccess)
		return JOIN_RETURN_SUCCCESS;
	else
		return JOIN_RETURN_FAILED;
}

static void* ThreadTest_Task1( void* arg )
{
	printf("[THREAD_TEST] dxTaskCreate ThreadTest_Task1 Says Hello!\n");
	return JOIN_RETURN_SUCCCESS;
}
