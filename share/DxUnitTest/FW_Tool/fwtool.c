/*
 ============================================================================
 Name        : dxOSTestLinux.c
 Author      : Aaron Lee
 Version     :
 Copyright   : Dexatek Technology Ltd
 Description : DxOS Unit Test
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/vfs.h>
#include <sys/time.h>
#include <fcntl.h>
#include <limits.h>
#include <bits/local_lim.h>
#include <linux/rtc.h>
#include <sys/ioctl.h>

#include <openssl/md5.h>

#include "dxFW.h"

/*
 * Global Variable
 */

/*
 * Function Definition
 */

#define CREATE_FW       1

#define IPCAM_KERNEL_IMAGE "./image/vmlinuz.img"
#define IPCAM_ROOTFS_IMAGE "./image/rootfs.bin"
#define IPCAM_MAIN_IMAGE "./image/dxfw.bin"

#define IPCAM_KERNEL_MTD "/dev/mtd3"
#define IPCAM_ROOTFS_MTD "/dev/mtd4"

void dxMD5(const unsigned char *input, size_t ilen, unsigned char output[16])
{
    MD5(input, ilen, output);
}

static const char hex_chars[] = "0123456789abcdef";

void dxMD5_convert_hex(unsigned char *md, unsigned char *mdstr)
{
    int i;
    int j = 0;
    unsigned int c;

    for (i = 0; i < 16; i++) {
        c = (md[i] >> 4) & 0x0f;
        mdstr[j++] = hex_chars[c];
        mdstr[j++] = hex_chars[md[i] & 0x0f];
    }
    mdstr[32] = '\0';
}

int main(void) {

    FILE *f;
    dxFW_t * dxFW = NULL;
    unsigned char md5str[32+1];

    dxFW = malloc(sizeof(dxFW_t));
    dxFW->Tag=DX_FWIMAGE_MARK;
    dxFW->nTotoalSize=sizeof(dxFW_t);
    dxFW->nPackageCount=0;

    dxFWPackage_t * dxFWPkgKernel = malloc(sizeof(dxFWPackage_t));

    dxFWPackage_t * dxFWPkgRootfs = malloc(sizeof(dxFWPackage_t));

    dxFWStorage_t * dxFWKernel=NULL;
    dxFWStorage_t * dxFWRootfs=NULL;
    uint32_t fwKernelSize=0;
    uint32_t fwRootfsSize=0;

    f = fopen(IPCAM_KERNEL_IMAGE, "rb");
    if (f != NULL){
        fseek(f, 0, SEEK_END);
        fwKernelSize = ftell(f);
        fseek(f, 0, SEEK_SET);  //same as rewind(f);

        dxFWKernel = malloc(sizeof(dxFWStorage_t)+fwKernelSize);
 
        dxFWKernel->Type = DXSTORAGETYPE_DXMTD;
        sprintf(dxFWKernel->Location,IPCAM_KERNEL_MTD);
        dxFWKernel->Size = fwKernelSize;

        if (fread(dxFWKernel->Data, dxFWKernel->Size, 1, f) == 1){

            dxMD5(dxFWKernel->Data, dxFWKernel->Size,dxFWKernel->MD5);
            dxMD5_convert_hex(dxFWKernel->MD5,md5str);

            dxFWPkgKernel->No = 0;
            dxFWPkgKernel->Type = DXFWTYPE_IMAGE;
            dxFWPkgKernel->Storage = dxFWKernel;
   
            dxFW->nPackageCount = dxFW->nPackageCount + 1 ;
            dxFW->nTotoalSize = dxFW->nTotoalSize + sizeof(dxFWPackage_t) - sizeof(dxFWStorage_t *) + 
                                                    sizeof(dxFWStorage_t) + fwKernelSize;

            printf("FW: Tag=%llx, Size=%lu, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount);
            printf("PKG: No=%d, Type=%d\n", dxFWPkgKernel->No, dxFWPkgKernel->Type);
            printf("PKG->STORE: Type=%d, Location=%s, Size=%lu\n", 
                    dxFWPkgKernel->Storage->Type, dxFWPkgKernel->Storage->Location, dxFWPkgKernel->Storage->Size);
            printf("STORE: MD5=%s\n", md5str);
        }
        fclose(f);
    }

    f = fopen(IPCAM_ROOTFS_IMAGE, "rb");
    if (f != NULL){
        fseek(f, 0, SEEK_END);
        fwRootfsSize = ftell(f);
        fseek(f, 0, SEEK_SET);  //same as rewind(f);

        dxFWRootfs = malloc(sizeof(dxFWStorage_t)+fwRootfsSize);

        dxFWRootfs->Type = DXSTORAGETYPE_DXMTD;
        sprintf(dxFWRootfs->Location,IPCAM_ROOTFS_MTD);
        dxFWRootfs->Size = fwRootfsSize;

        if (fread(dxFWRootfs->Data, dxFWRootfs->Size, 1, f) == 1){

            dxMD5(dxFWRootfs->Data, dxFWRootfs->Size,dxFWRootfs->MD5);
            dxMD5_convert_hex(dxFWRootfs->MD5,md5str);
    
            dxFWPkgRootfs->No = dxFW->nPackageCount;
            dxFWPkgRootfs->Type = DXFWTYPE_IMAGE;
            dxFWPkgRootfs->Storage = dxFWRootfs;
    
            dxFW->nPackageCount = dxFW->nPackageCount + 1 ;
            dxFW->nTotoalSize = dxFW->nTotoalSize + sizeof(dxFWPackage_t) - sizeof(dxFWStorage_t *) + 
                                                    sizeof(dxFWStorage_t) + fwRootfsSize;
    
            printf("FW: Tag=%llx, Size=%lu, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount);
            printf("PKG: No=%d, Type=%d\n", dxFWPkgRootfs->No, dxFWPkgRootfs->Type);
            printf("PKG->STORE: Type=%d, Location=%s, Size=%lu\n", 
                    dxFWPkgRootfs->Storage->Type, dxFWPkgRootfs->Storage->Location, dxFWPkgRootfs->Storage->Size);
            printf("STORE: MD5=%s\n", md5str);
        }
        fclose(f);
    }

    
    f = fopen(IPCAM_MAIN_IMAGE, "wb");
    if (f != NULL){

        fwrite(dxFW,sizeof(dxFW_t),1,f);

        if (dxFW->nPackageCount == 2) { 
            fwrite(dxFWPkgKernel,sizeof(dxFWPackage_t)-sizeof(dxFWStorage_t *) ,1,f);
            fwrite(dxFWKernel,sizeof(dxFWStorage_t) + fwKernelSize,1,f);
        }

        fwrite(dxFWPkgRootfs,sizeof(dxFWPackage_t)-sizeof(dxFWStorage_t *) ,1,f);
        fwrite(dxFWRootfs,sizeof(dxFWStorage_t) + fwRootfsSize,1,f);

        fclose(f);
        printf("Create dxMainImage = %s\n",IPCAM_MAIN_IMAGE);
    }

    if (dxFWKernel != NULL)
        free(dxFWKernel);

    if (dxFWRootfs != NULL)
        free(dxFWRootfs);

    free(dxFW);
    if (dxFWPkgKernel != NULL)
        free(dxFWPkgKernel);

    if (dxFWPkgRootfs != NULL)
        free(dxFWPkgRootfs);

    return 0;
}

