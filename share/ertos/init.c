#include "includes.h"

#ifdef CONFIG_ERTOS_HEAP_EN

#ifdef CONFIG_ERTOS_HEAP_IN_ARRAY
static u8 ertos_heap[CONFIG_ERTOS_HEAP_SIZE];
#endif

#endif

extern void S_start_schedule(void); //assemble func
t_ertos g_ertos_cfg =
{
	.cb_start_schedule = S_start_schedule,
};

ERTOS_EVENT_DECLARE(_cmdshell_event)
#define CMDSHELL_BUF_LEN 128
static char _cmdshell_buf[CMDSHELL_BUF_LEN];
#if CONFIG_ERTOS_CMDSHELL_HISTORY_CNT > 0
static char _cmdshell_history_buf[CMDSHELL_BUF_LEN * CONFIG_ERTOS_CMDSHELL_HISTORY_CNT];
static t_cmdshell_history _cmdshell_history =
{
	.buf = &_cmdshell_history_buf[0],
	.onelen = CMDSHELL_BUF_LEN,
	.maxcnt = CONFIG_ERTOS_CMDSHELL_HISTORY_CNT,
};
#endif
static t_cmdshell _cmdshell_cfg =
{
	.buf = &_cmdshell_buf[0],
	.bufsize = CMDSHELL_BUF_LEN,
	.event = (t_ertos_eventhandler)(&_ertos_eventbuf__cmdshell_event[0]),
#if CONFIG_ERTOS_CMDSHELL_HISTORY_CNT > 0
	.history = &_cmdshell_history,
#endif
};
ERTOS_TASK_DECLARE(_taskhandler_cmdshell, 1024)

ERTOS_QUEUE_DECLARE(_timer_queue, TIMER_QUEUE_LENGTH, sizeof(t_ertos_timer_queue_msg) );

DECLARE_DEBUGPRINTF_BUF(2048)
t_debugprintf_hw g_debugprintf_hw_null = {};
#if defined(CONFIG_ERTOS_SYSLOG_BUFLEN) && (CONFIG_ERTOS_SYSLOG_BUFLEN > 0)
static char syslog_buf[CONFIG_ERTOS_SYSLOG_BUFLEN];
t_syslog_buf g_syslog_buf = {
	.base = syslog_buf,
	.size = CONFIG_ERTOS_SYSLOG_BUFLEN,
	.w = 0,
	.r = 0,
	.remcnt = 0,
};
t_syslog_cfg g_syslog_cfg = {
	.syslog_buf = &g_syslog_buf,
};
#else
t_syslog_cfg g_syslog_cfg = {
	.syslog_buf = NULL,
};
#endif

#ifdef CONFIG_ERTOS_DEBUG_EN
static void malloc_failed(u32 size){
	syslog(LOG_ERR, "[ERROR] malloc failed: %d\n", size);
}
#endif

#define WATCHDOG_BOOT_TIME (240000)    //boot time 100ms
void reboot(void)
{
	DISABLE_INTERRUPT;
	DISABLE_TICK;
	disable_cache();

	WATCHDOG_SET(WATCHDOG_BOOT_TIME);
	while(1);
}
extern void rom_tzinit(void);
int ertos_init(void)
{
	rom_tzinit();

#ifdef CONFIG_MPU_BIN_INARRAY
	/* because _mpubin_code[] share the same memory space as heap
	   so must init the mpu before init heap
	   */
	mpu_init_fromarray();
#endif

#ifdef CONFIG_ERTOS_HEAP_EN

#ifdef CONFIG_ERTOS_HEAP_IN_ARRAY
	rom_ertos_heap_start = ertos_heap;
	rom_ertos_heap_size = sizeof(ertos_heap);
#endif
#ifdef CONFIG_ERTOS_HEAP_IN_CFG
#if CONFIG_ERTOS_HEAP_START == 0
	/* if CONFIG_ERTOS_HEAP_IN_CFG is defined and CONFIG_ERTOS_HEAP_START is zero */
#ifdef CONFIG_DDR_FW_EN
	/* if DDR enabled, then heap start from ddr_bss_end */
	extern u32 ddr_bss_end;
	rom_ertos_heap_start = (u8 *)((u32)((&ddr_bss_end) + 31) & ~31);
#else
	/* else if DDR not enabled, then heap start from ld_heap_start, which is same as ram_bss_end */
	extern u32 ld_heap_start;
	rom_ertos_heap_start = (u8 *)((u32)((&ld_heap_start) + 31) & ~31);
#endif
#else
	rom_ertos_heap_start = (u8 *)CONFIG_ERTOS_HEAP_START;
#endif
	rom_ertos_heap_size = CONFIG_ERTOS_HEAP_SIZE;
#endif
#ifdef CONFIG_ERTOS_HEAP_IN_LD
	extern u32 ld_heap_start, ld_heap_end;
	rom_ertos_heap_start = (u8 *)&ld_heap_start;
	rom_ertos_heap_size = ((u32)(&ld_heap_end) & ~BIT31) - ((u32)(&ld_heap_start) & ~BIT31);
#endif

#else
	rom_ertos_heap_start = NULL;
#endif

	g_ertos_cfg.cycles_per_tick = clockget_cpu() / TICKS_PER_SEC;

	tick1_set_rt(g_ertos_cfg.cycles_per_tick);

#ifdef CONFIG_ERTOS_CMDSHELL_EN
	cmdshell_init(&_cmdshell_cfg);
	cmdshell_register_all();
#endif

#ifdef CONFIG_ERTOS_DEBUG_EN
	g_ertos_cfg.ertos_debug = ERTOS_DEBUG_STACK_STATISTICS | ERTOS_DEBUG_STACK_CHECK_RUNTIME | ERTOS_DEBUG_STAT;
	g_ertos_cfg.text_start = (u32)(&ram_vector_start);
	g_ertos_cfg.text_end = (u32)(&ram_text_end);
	g_ertos_cfg.data_start = (u32)(&ram_data_start);
	g_ertos_cfg.data_end = (u32)(&ram_data_end);
	g_ertos_cfg.bss_start = (u32)(&ram_bss_start);
	g_ertos_cfg.bss_end = (u32)(&ram_bss_end);
	g_ertos_cfg.malloc_failure_hook = malloc_failed;
#endif

#if defined(CONFIG_SYSLOG_LEVEL) && (CONFIG_SYSLOG_LEVEL > 0)
	g_ertos_cfg.loglevel_default = CONFIG_SYSLOG_LEVEL;
#endif
#ifdef BOOT_TIME_TEST
	g_ertos_cfg.loglevel_default = 1;
#endif
extern void syslog_ertos(int priority, const char * fmt0, va_list ap);
	g_ertos_cfg.cb_syslog_ertos = syslog_ertos;
	g_ertos_cfg.syslog_cfg = &g_syslog_cfg;

	int ret = rom_ertos_init(&g_ertos_cfg);
	if(ret == 0){
		//timer q
		ertos_queue_init(_timer_queue);
		g_ertos_cfg.timer_q = _timer_queue;
	}

	return ret;
}

ERTOS_TASK_DECLARE(_taskhandler_idle, 256)

extern void ertos_start_schedule_internal(void);

ERTOS_TASK_DECLARE(_taskhandler_timer, 512)

void ertos_start(void)
{
	ertos_task_create_fromhandler(_taskhandler_idle, ertos_task_idle, "taskIDLE", NULL, 31);

	g_ertos_cfg.idletask = _taskhandler_idle;

	ertos_start_schedule_internal();
}

extern unsigned long except_tick(void);
extern void ertos_except_tick (void);
extern void ertos_watchdog_check(void);
static int _irqhandler_tick(void * arg)
{
	//TODO 2: tick_hook:  except_tick();
	ertos_except_tick();

	ertos_watchdog_check();

	return 0;
}

int lowlevel_irq_handler_generic(void * arg)
{
    u8 ch;

	//uart TX interrupt for debug_printf
	//TODO 2: now direct mode used.
#ifdef CONFIG_UART_THRI_EN
	lowlevel_tx_interrupt_handler();
#endif

	//uart RX input
#ifdef CONFIG_ERTOS_CMDSHELL_EN
	while (lowlevel_getc_nob(&ch) == 0) {
		cmdshell_input_char(ch);
	}
	cmdshell_input_notify();
#else
	while (lowlevel_getc_nob(&ch) == 0) {
		lowlevel_putc(ch);
	}
#endif
	return 0;
}

#ifdef CONFIG_DDR_FW_EN
//put load_ddr_bin in another function to save the stack size - xbuff not retained in thread_premain()
static int load_ddr_bin(void)
{
	unsigned char xbuff[1030];
int xmodemReceive(unsigned char *xbuff, unsigned char *dest, int destsz);
	return xmodemReceive(xbuff, (u8 *)MEMBASE_DDR, 0x1000000);
}
#endif

extern void * thread_main(void * arg);

static void * thread_premain(void * arg)
{
#ifdef CONFIG_UART_THRI_EN
	debug_printf_init(DEBUG_PRINTF_MODE_TRUNCATE | DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart,
					(1 << 1) //use tx interrupt mode, use less resource.
//					(0 << 1) //use tx polling mode, uart debug thread cost more resource.

#ifdef CONFIG_CONSOLE_UART1
					| (1 << 0) //use UART 1
#else
					| (0 << 0) //use UART 0
#endif		// #ifdef CONFIG_CONSOLE_UART1
					| (1 << 8) //uart debug thread priority 1.
					); //buffer print mode
#else

	debug_printf_init(DEBUG_PRINTF_MODE_DIRECT | DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_uart,

#ifdef CONFIG_CONSOLE_UART1
					1
#else
					0
#endif		// #ifdef CONFIG_CONSOLE_UART1
					); //direct print mode
#endif		// #ifdef CONFIG_UART_THRI_EN

#ifdef CONFIG_CONSOLE_UART1
	irq_request(IRQ_BIT_UART1, lowlevel_irq_handler_generic, "UART1", NULL);
#else
	irq_request(IRQ_BIT_UART0, lowlevel_irq_handler_generic, "UART0", NULL);
#endif

	//SF+DMA only can be used after ertos start
#ifdef CONFIG_HAS_SF

	if(g_sflash_fun_init.init == NULL){
		//if sflash not inited in HW_EARLY_INIT
		sf_init(SF_SOURCE_SFC, NULL); //auto detect sflash
	}

#endif

#ifdef CONFIG_MPU_BIN_INSF

#if CONFIG_MPU_BIN_ID2 == 0
#error CONFIG_MPU_BIN_ID2 not set in menuconfig
#endif

	mpu_init_from_sf(CONFIG_MPU_BIN_ID2);

#endif

#ifdef CONFIG_MPU_BIN_FROMUART
	mpu_init_from_uart();
#endif

#ifdef CONFIG_ENV_SOURCE_SF
	if(loadenv() < 0){
		syslog(LOG_WARNING, "WARNING: loadenv failed\n");
	}

#ifdef MANUFACTORY_BOOTTEST
#define MANUFACTORY_BOOT_STATUS   (0x900ce924)  ///< used the address of romnand_cfg

	char * para = getenv("M_boottest");
	int val = 0;
	if(para != NULL)
	{
		val = strtol(para, NULL, 0);
	}

	int sta = ReadReg32(MANUFACTORY_BOOT_STATUS);
	if(val == 0 || (val == 1 && sta == 0)){
		//test not started, or first time bootup and test not finished
		syslog(LOG_ERR, "Bootup testing...\n");
		WriteReg32(MANUFACTORY_BOOT_STATUS, 1);
		WriteReg32(MANUFACTORY_BOOT_TOTAL, 0);
		WriteReg32(MANUFACTORY_BOOT_FAIL, 0);
		setenv("M_boottest", "1", 1);
		val = 1;
		saveenv();
	}

	if(val == 1){
		//running
		int total = ReadReg32(MANUFACTORY_BOOT_TOTAL);
		if(total < MANUFACTORY_BOOTTEST){
			//do testing
			//total ++
			WriteReg32(MANUFACTORY_BOOT_TOTAL, total + 1);
			//reset:
			DISABLE_INTERRUPT;
			DISABLE_TICK;
			extern int irq_free_all(void);
			irq_free_all();
			/*
			disable_cache();
			*/

			//FIXME: turn down MBUS clock to fix the boot issue, need more test.
			WriteReg32(SC_BASE_ADDR+0x58,(ReadReg32(SC_BASE_ADDR+0x58) &~ 0x1f)| (2));

			BOOT_SRAM();
			asm("dsb");
			asm("wfe");
			asm("wfe");
			//boot failed here::
			while(1){
				uart_forceputc('H');
				{volatile int d; for(d=0; d<10; d++);}

				WriteReg32(MANUFACTORY_BOOT_FAIL, ReadReg32(MANUFACTORY_BOOT_FAIL) + 1);
				BOOT_SRAM();
				asm("dsb");
				asm("wfe");
				asm("wfe");

				WriteReg32(MANUFACTORY_BOOT_TOTAL,  ReadReg32(MANUFACTORY_BOOT_TOTAL) + 1);
			}
		}else{
			//test done
			syslog(LOG_ERR, "Bootup test done: %d/%d\n", ReadReg32(MANUFACTORY_BOOT_FAIL), ReadReg32(MANUFACTORY_BOOT_TOTAL) );
			char fcnt[16];
			sprintf(fcnt, "%d", ReadReg32(MANUFACTORY_BOOT_FAIL));
			setenv("M_bootfail", fcnt, 1);
			setenv("M_boottest", "2", 1);
			saveenv();
		}
	}
#endif

#endif

#ifdef CONFIG_USB_SERIAL
extern const t_debugprintf_hw g_debugprintf_hw_usb;
	debug_printf_set(DEBUG_PRINTF_MODE_TRUNCATE | DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_usb, 5);
#endif

	board_ioctl(BIOC_HW_INIT, 0, NULL);

#ifdef CONFIG_DDR_FW_EN
	int ddrbin_len = 0;
	u8 boot_pin = ReadReg8(REG_SC_STATUS) & 0x7;
#if defined(CONFIG_HAS_SF) && defined(CONFIG_DDRBIN_ID2) && CONFIG_DDRBIN_ID2 > 0
	if(boot_pin == 0){
		ddrbin_len = partition_load_sf(CONFIG_DDRBIN_ID2, (u8 *)MEMBASE_DDR);
	}
#endif
#if defined(CONFIG_HAS_NAND) && defined(CONFIG_DDRBIN_ID2) && CONFIG_DDRBIN_ID2 > 0
	if(boot_pin == 4){
		nand_init(NAND_INTERFACE_RB1);
		ddrbin_len = partition_load_nand(CONFIG_DDRBIN_ID2, (u8 *)MEMBASE_DDR);
	}else if((boot_pin==5)||(boot_pin==7))
	{
		WriteReg16(RTC_BASE_ADDR + 0x42, ReadReg16(RTC_BASE_ADDR + 0x42) | BIT8 | BIT9 | BIT10);
		int gpiov = (ReadReg16(RTC_BASE_ADDR + 0x40) >> 8) & 7;
		if(gpiov ==6)
		{
			nand_init(NAND_INTERFACE_RB2);
			ddrbin_len = partition_load_nand(CONFIG_DDRBIN_ID2, (u8 *)MEMBASE_DDR);
		}
	}
#endif
	if(ddrbin_len <= 0){
		lowlevel_printf("DDR firmware enabled, please xmodem download XXX_ddr.bin\n");
		ddrbin_len = load_ddr_bin();
	}
	dc_flush_all();

	//flush icache because load the new DDR code.
	//althrough DDR code is not called before, but because of pre-fetch, the wrong DDR content maybe loaded into icache already. So still need flush
	ic_disable();
	ic_enable();

	if(ddrbin_len <= 0){
		syslog(LOG_ERR, "ERROR: DDR firmware load failed\n");
	}
#endif

	//FIXME:  Set default loglevel 0 for system tasks.
#if defined(CONFIG_SYSLOG_LEVEL) && (CONFIG_SYSLOG_LEVEL > 0)
	g_ertos_cfg.loglevel_default = 0;
#endif

#ifdef CONFIG_ERTOS_CMDSHELL_EN
	ertos_task_create_fromhandler(_taskhandler_cmdshell, cmdshell_task, "CmdShell", NULL, 3);
#endif

	ertos_task_create_fromhandler(_taskhandler_timer, ertos_task_timer, "Timer", NULL, 4);

	// Recover default loglevel that user defines.
#if defined(CONFIG_SYSLOG_LEVEL) && (CONFIG_SYSLOG_LEVEL > 0)
	g_ertos_cfg.loglevel_default = CONFIG_SYSLOG_LEVEL;
#endif

	ertos_watchdog_start();

	return thread_main(NULL);
}

int main(void)
{
	lowlevel_forceputc('\n');
	lowlevel_forceputc('F');
    lowlevel_forceputc('F');
    lowlevel_forceputc('F');

	WATCHDOG_DISABLE;

#if defined(CONFIG_ICACHE) || defined(CONFIG_DCACHE)
	tlb_set_bufaddr((u32 *)0x100c8000);
#endif

#ifdef CONFIG_ICACHE
	ic_enable();
#endif

#ifdef CONFIG_DCACHE
#ifndef CONFIG_CPU_DCACHE_OFF
	dc_enable();
#endif
#endif

#ifdef MANUFACTORY_BOOTTEST
#define MANUFACTORY_BOOT_STATUS   (0x900ce924)  ///< used the address of romnand_cfg
	int sta = ReadReg32(MANUFACTORY_BOOT_STATUS);
#endif

	BSS_CLEAR();
#ifdef CONFIG_DDR_FW_EN
	DDR_BSS_CLEAR();
#endif

#ifdef MANUFACTORY_BOOTTEST
#define MANUFACTORY_BOOT_STATUS   (0x900ce924)  ///< used the address of romnand_cfg
	WriteReg32(MANUFACTORY_BOOT_STATUS, sta);
#endif

#if defined(CONFIG_SYSLOG_LEVEL) && (CONFIG_SYSLOG_LEVEL > 0)
	//for EARLY_INIT cal use syslog()
	syslog_noos_level = CONFIG_SYSLOG_LEVEL;
#endif

	if(board_ioctl(BIOC_HW_EARLY_INIT, 0, NULL) < 0){
		//FIXME: if not implemented, set default to 360M with DDR3 400M
		//in most cases, the ram/ should implement the clock config in board_xxx.c BIOC_HW_EARLY_INIT !!! Don't use the default one
#ifndef FOR_FPGA
		clockconfig_360M();
		ddrset_ddr3_400M();
//		ddrset_lpddr2_400M();
#endif
	}

#ifndef CONFIG_FAST_BOOT_EN
	//init uart
	lowlevel_config(115200, UART_IER_RDI);
#endif

	ertos_init();

	ertos_task_create( thread_premain, "thread_premain", 81720, NULL, 2 );

	irq_request(IRQ_BIT_TICK1, _irqhandler_tick, "TICK", NULL);

	ertos_start();

	while(1);
}
