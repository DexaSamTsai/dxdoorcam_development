#include "includes.h"

#ifdef CONFIG_ENV_MAXLEN
static char _envbuf[CONFIG_ENV_MAXLEN];
#endif

static pthread_mutex_t env_mutex = PTHREAD_MUTEX_INITIALIZER;

int loadenv(void)
{
#ifdef CONFIG_ENV_SOURCE_SF
	pthread_mutex_lock(&env_mutex);
	int ret = loadenv_sf(_envbuf, CONFIG_ENV_MAXLEN);
	pthread_mutex_unlock(&env_mutex);
	return ret;
#else
	return -1;
#endif
}

int saveenv(void)
{
#ifdef CONFIG_ENV_SOURCE_SF
	pthread_mutex_lock(&env_mutex);
	int ret = saveenv_sf();
	pthread_mutex_unlock(&env_mutex);
	return ret;
#endif
	return 0;
}

#ifndef CONFIG_ENV_DISABLE
int env_set(int argc, char ** argv)
{
	if(argc == 1)
	{
		pthread_mutex_lock(&env_mutex);
		printenv();
		pthread_mutex_unlock(&env_mutex);
	}
	return 0;
}

int env_export(int argc, char ** argv)
{
	if(argc < 2)
	{
		syslog(LOG_ERR, "Usage: export <env>=<data> <env1>=<data1>...");
		return -1;
	}
	pthread_mutex_lock(&env_mutex);
	int i;
	for(i = 1; i < argc; i++)
	{
		if(*argv[i]=='=')
		{
			pthread_mutex_unlock(&env_mutex);
			syslog(LOG_ERR, "export: %s :not a valid indetifier", argv[i]);
			syslog(LOG_ERR, "Usage: export <env>=<data> <env1>=<data1>...");
			return -1;
		}
		putenv(argv[i]);
	}
	pthread_mutex_unlock(&env_mutex);
	saveenv();
	return 0;
}

int env_unset(int argc, char ** argv)
{

	if(argc < 2)
	{
		syslog(LOG_ERR, "Usage: unset <env>...");
		return -1;
	}
	pthread_mutex_lock(&env_mutex);
	int i;
	for(i = 1; i < argc; i++)
	{
		unsetenv(argv[i]);
	}
	pthread_mutex_unlock(&env_mutex);
	saveenv();
	return 0;
}

CMDSHELL_DECLARE(set)
	.cmd = {'s', 'e', 't', '\0'},
	.handler = env_set,
	.comment = "set cmd, print all env",
};

CMDSHELL_DECLARE(export)
	.cmd = {'e', 'x', 'p', 'o', 'r', 't', '\0'},
	.handler = env_export,
	.comment = "export command , to set env",
};

CMDSHELL_DECLARE(unset)
	.cmd = {'u', 'n', 's', 'e', 't', '\0'},
	.handler = env_unset,
	.comment = "unset env",
};
#endif
