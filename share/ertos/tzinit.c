#include "includes.h"

typedef struct s_tz_set
{
  char ch;
  int m;
  int n;
  int d;
  int s;
  time_t change;
  long offset;
} t_tz_set;

typedef struct s_tzinfo
{
  int north;
  int year;
  t_tz_set sets[2];
} t_tzinfo;

extern char * tzname[2];
extern t_tzinfo * rom_tzinfo; //defined in ROM: TODO

static t_tzinfo mytzinfo = {1, 0,
    { {'J', 0, 0, 0, 0, (time_t)0, 0L },
      {'J', 0, 0, 0, 0, (time_t)0, 0L } 
    } 
};

void rom_tzinit(void)
{
	rom_tzinfo = &mytzinfo;
	tzname[0] = "GMT";
	tzname[1] = "GMT";
}
