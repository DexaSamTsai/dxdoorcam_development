#include "includes.h"

static int _cmd_wrreg(int argc, char ** argv)
{
	if(argc != 4){
		syslog(LOG_ERR, "Usage: rdreg <bytes1/2/4> <addr(hex)> <data(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
	val = strtoul(argv[3], NULL, 16);
	syslog(LOG_INFO, "Write 0x%x to Reg[0x%x]\n", val, reg);
	switch(bytes){
		case 1:
			WriteReg8(reg, val);
			break;
		case 2: 
			WriteReg16(reg, val);
			break;
		case 4:
			WriteReg32(reg, val);
			break;
		default:
			syslog(LOG_ERR, "the wrong write bytes!\n");
			return -2;
	}
	return 0;

}

CMDSHELL_DECLARE(wrreg)
	.cmd = {'w', 'r','r','e','g','\0'},
	.handler = _cmd_wrreg,
	.comment = "wrreg",
};
