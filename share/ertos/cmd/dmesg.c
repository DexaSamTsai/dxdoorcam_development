#include "includes.h"

#if defined(CONFIG_ERTOS_SYSLOG_BUFLEN) && (CONFIG_ERTOS_SYSLOG_BUFLEN > 0)
int _cmd_dmesg(int argc, char ** argv);

CMDSHELL_DECLARE(dmesg)
	.cmd = {'d', 'm', 'e', 's', 'g', '\0'},
	.handler = _cmd_dmesg,
	.comment = "dmesg",
};
#endif
