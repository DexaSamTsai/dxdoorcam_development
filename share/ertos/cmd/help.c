#include "includes.h"

int _cmd_help_handler(int argc, char ** argv);

CMDSHELL_DECLARE(help)
	.cmd = {'h', 'e', 'l', 'p', '\0'},
	.handler = _cmd_help_handler,
};
