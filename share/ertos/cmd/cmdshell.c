#include "includes.h"

int cmdshell_register_all(void)
{
extern t_cmdshell_cmd cmdshells_start;
extern t_cmdshell_cmd cmdshells_end;

	t_cmdshell_cmd * cmds;

	int ret = 0;

	for(cmds = (t_cmdshell_cmd *)(&cmdshells_start); cmds < (t_cmdshell_cmd *)(&cmdshells_end); cmds ++){
		if(cmdshell_register(cmds) == 0){
			ret ++;
		}
	}

	return ret ;
}
