#include "includes.h"

CMDSHELL_DECLARE(top)
	.cmd = {'t', 'o', 'p', '\0'},
	.handler = ertos_task_listprint,
	.comment = "list tasks",
};
