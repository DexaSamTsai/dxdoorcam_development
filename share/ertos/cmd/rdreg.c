#include "includes.h"

static int _cmd_rdreg(int argc, char ** argv)
{
	if(argc != 3){
		syslog(LOG_ERR, "Usage: rdreg <bytes 1/2/4> <addr(hex)>");
		return -1;	
	}
	unsigned int reg, bytes, val;
	bytes = atoi(argv[1]);
	reg = strtoul(argv[2], NULL, 16);
	switch(bytes){
		case 1:
			val = ReadReg8(reg);
			break;
		case 2: 
			val = ReadReg16(reg);
			break;
		case 4:
			val = ReadReg32(reg);
			break;
		default:
			syslog(LOG_ERR, "the wrong read bytes!\n");
			return -2;
	}
	syslog(LOG_INFO, "Read Reg[0x%x]:%x\n", reg, val);
	return 0;
}

CMDSHELL_DECLARE(rdreg)
	.cmd = {'r', 'd', 'r','e','g','\0'},
	.handler = _cmd_rdreg,
	.comment = "rdreg",
};
