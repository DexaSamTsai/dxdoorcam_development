#include "includes.h"

CMDSHELL_DECLARE(free)
	.cmd = {'f', 'r', 'e', 'e', '\0'},
	.handler = ertos_task_listprint,
	.comment = "list tasks",
};
