#include "includes.h"

CMDSHELL_DECLARE(ps)
	.cmd = {'p', 's', '\0'},
	.handler = ertos_task_listprint,
	.comment = "list tasks",
};
