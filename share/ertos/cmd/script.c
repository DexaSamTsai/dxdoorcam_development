#include "includes.h"

#define cmdshell_printf(x...) printf(x)
extern t_cmdshell * g_cmdshell;
static void script_runcmd(char * cmd)
{
	//parse argc,argv
	int i;
	g_cmdshell->argc = 0;
	memset(g_cmdshell->argv, 0, sizeof(g_cmdshell->argv));

	int len=strlen(cmd);
	for(i=0; i<len+1; i++){
		if(cmd[i] == '\0'){
			if(g_cmdshell->argv[g_cmdshell->argc] != NULL){
				g_cmdshell->argc ++;
			}
			break;
		}
		if(cmd[i] == ' ' || cmd[i] == '\t'){
			if(g_cmdshell->argv[g_cmdshell->argc] == NULL){
				//skip empty characters
				continue;
			}else{
				//end current argv
				cmd[i] = '\0';
				g_cmdshell->argc ++;
				g_cmdshell->argv[g_cmdshell->argc] = NULL;
				if(g_cmdshell->argc >= ERTOS_CMDSHELL_ARGMAX){
					break;
				}else{
					continue;
				}
			}
		}
		//other characters
		if(g_cmdshell->argv[g_cmdshell->argc] == NULL){
			g_cmdshell->argv[g_cmdshell->argc] = &cmd[i];
		}
	}

	if(g_cmdshell->argc > 0){
		//none empty command
		t_cmdshell_cmd * tmp = g_cmdshell->handlers;
		while(tmp){
			if(strcasecmp(tmp->cmd, g_cmdshell->argv[0]) == 0){
				//found command, run handler
				if(tmp->handler){
					tmp->handler(g_cmdshell->argc, g_cmdshell->argv);
				}
				break;
			}
			tmp = tmp->next;
		}
		if(tmp == NULL){
			//command not found
			cmdshell_printf("command '%s' not found, 'help' to list all commands", g_cmdshell->argv[0]);
		}
	}
}

static int _cmd_script(int argc, char ** argv)
{
	if(argc != 2){
		syslog(LOG_ERR, "Usage: script TEMP_MEMORY_ADDR(hex, eg:0x10b00000)");
		return -1;	
	}
	unsigned int memaddr = strtoul(argv[1], NULL, 16);
	syslog(LOG_ERR, "please xmodem download script.txt\n");
	int len = load_from_xmodem(memaddr);
	if(len <= 0){
		syslog(LOG_ERR, "xmodem receive failed\n");
		return -1;
	}
	syslog(LOG_INFO, "\n%d bytes script received, at memory 0x%x\n", len, memaddr);
	char * c = (char *)memaddr;
	c[len] = '\0';
	int finished = 0;
	char * end = c;
	while(1){
		if(finished){
			break;
		}
		c = end;
		while(1){
			char cc = *end;
			if(cc == '\0' || cc == '\r' || cc=='\n' || (cc >= 0x10 && cc <= 0x1f)){
				if(cc == '\0'){
					finished = 1;
					break;
				}
				*end = '\0';
				end ++;
				break;
			}
			end ++;
		}
		if(strlen(c) < 2){
			continue;
		}
		if(c[0] == ';' || c[0] == '#'){
			continue;
		}
		syslog(LOG_INFO, "RUN: %s\n", c);
		script_runcmd(c);
	}
	return 0;
}

CMDSHELL_DECLARE(script)
	.cmd = {'s', 'c','r','i','p','t','\0'},
	.handler = _cmd_script,
	.comment = "run cmdshell script",
};
