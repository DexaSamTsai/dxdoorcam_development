#include "includes.h"

CMDSHELL_DECLARE(irq)
	.cmd = {'i', 'r', 'q', '\0'},
	.handler = irq_listprint,
	.comment = "list interrupt status",
};
