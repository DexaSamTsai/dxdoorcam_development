#include "includes.h"

#define ERTOS_WATCHDOG_HW_TIMEOUT	(360000000 * 2) //in cycle, 2 second. It's 30bits for OV798's internal Watchdog.
#define ERTOS_WATCHDOG_CHECK_INTERVAL	50 //in ticks, 50 means 0.5 second.

int ertos_task_set_watchdog(t_ertos_taskhandler handler, u32 timeout)
{
	u32 flags;
	local_irq_save(flags);
	
	t_ertos_task * tcb;
	if(handler == NULL){
		tcb = (t_ertos_task *)g_ertos->cur_task;
	}else{
		tcb = (t_ertos_task *)handler;
	}

	tcb->watchdog_timeout = timeout;
	tcb->watchdog_lastrun_ticks = ticks;

	local_irq_restore(flags);

	return 0;
}

static u32 ertos_watchdog_check_lasttick = 0;
int ertos_watchdog_start(void)
{
	g_ertos->ertos_debug |= ERTOS_DEBUG_WATCHDOG;
	ertos_watchdog_check_lasttick = ticks;

	watchdog_set(ERTOS_WATCHDOG_HW_TIMEOUT);

	return 0;
}

int ertos_watchdog_stop(void)
{
	watchdog_set(0);

	g_ertos->ertos_debug &= ~ERTOS_DEBUG_WATCHDOG;

	return 0;
}

/* called by ertos only, when switched to a new task */
void ertos_watchdog_update(void)
{
	if (g_ertos->schedule_running) {
		u32  flags;
		local_irq_save(flags);

		if(g_ertos->ertos_debug & ERTOS_DEBUG_WATCHDOG){
			g_ertos->cur_task->watchdog_lastrun_ticks = ticks;
		}

		local_irq_restore(flags);
	}

}

static int hang_num = 0;
static int ertos_watchdog_check_one(t_ertos_task * task, t_ertos_task_status status)
{
	if(status == ERTOS_TASK_DELETED){
		return 0; //don't check the deleted tasks.
	}
	if(task->watchdog_timeout == 0){
		return 0;
	}
	if(ticks - task->watchdog_lastrun_ticks > task->watchdog_timeout){
		syslog(LOG_ERR, "task %s WATCHDOG timeout\n", task->name);

#define print16b(x) { \
		dprintf_rom("  "); \
		dputint(x); \
		dprintf_rom(": "); \
		dputint(ReadReg32(x + 0)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 4)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 8)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 12)); \
		dprintf_rom("\n"); \
}

		dprintf_rom("\n  g_ertos:");
		dputint((u32)g_ertos);
		dprintf_rom("\n  suspend_list:");
		dputint((u32)&g_ertos->tasks_suspend_list);
		dprintf_rom("\n  ticks:");
		dputint(ticks);
		dprintf_rom("\n  watchdog to:");
		dputint(task->watchdog_timeout);
		dprintf_rom("\n  lastrun_tick:");
		dputint(task->watchdog_lastrun_ticks);
		dprintf_rom("\n  task value(to):");
		dputint(task->task_listitem.value);
		dprintf_rom("\n  task list:");
		dputint((u32)task->task_listitem.list);
		dprintf_rom("\n  Stack Dump:\n");
		u32 tmpaddr = (((u32)task->cur_stk - 16) & (~15));
		for(; tmpaddr < (u32)task->stk_end; tmpaddr += 16){
			print16b(tmpaddr);
		}

		hang_num ++;
		return 1;
	}else{
		return 0;
	}
}

/* called by TICK interrupt only */
void ertos_watchdog_check(void)
{
	//if not enabled, don't check.
	if(! (g_ertos->ertos_debug & ERTOS_DEBUG_WATCHDOG)){
		return;
	}

	//check every ERTOS_WATCHDOG_CHECK_INTERVAL ticks
	if(ticks - ertos_watchdog_check_lasttick < ERTOS_WATCHDOG_CHECK_INTERVAL){
		return;
	}
	ertos_watchdog_check_lasttick = ticks;

	//check
	u32  flags;
	local_irq_save(flags);

	hang_num = 0;

extern void ertos_task_list(int (*print_cb)(t_ertos_task *, t_ertos_task_status));
	ertos_task_list(ertos_watchdog_check_one);

	local_irq_restore(flags);

	if(hang_num > 0){
		//do nothing, not feed the watchdog, wait for watchdog reset itself
		//TODO: tell MCU the reset, and reset immediately.

		while(1){
			{volatile int d; for(d=0; d<0x100000; d++);}
			lowlevel_putc('.');
		}
	}else{
		//everything is OK. feed the watchdog.
		watchdog_feed();
	}
}
