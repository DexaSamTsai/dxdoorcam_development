#include "includes.h"

/* saved_regs: r13, cpsr, spsr, r0-r12, r14 */
void exception_reset(u32 * saved_regs)
{
	int i;

	dprintf_rom("\nException, saved regs:");
	dprintf_rom("\n  R13(sp): "); dputint(saved_regs[0]);
	saved_regs ++;
	dprintf_rom("\n  CPSR: "); dputint(saved_regs[0]);
	dprintf_rom("\n  SPSR: "); dputint(saved_regs[1]);
	for(i=0; i<10; i++){
		dprintf_rom("\n    R%d: ", i); dputint(saved_regs[2+i]);
	}
	dprintf_rom("\n   R10(sl): "); dputint(saved_regs[12]);
	dprintf_rom("\n   R11(fp): "); dputint(saved_regs[13]);
	dprintf_rom("\n   R12(ip): "); dputint(saved_regs[14]);
	dprintf_rom("\n    LR: "); dputint(saved_regs[15]);
	dprintf_rom("\n\nLR memory dump:\n");

#define print16b(x) { \
		dprintf_rom("  "); \
		dputint(x); \
		dprintf_rom(": "); \
		dputint(ReadReg32(x + 0)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 4)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 8)); \
		dprintf_rom(" "); \
		dputint(ReadReg32(x + 12)); \
		dprintf_rom("\n"); \
}

	/*
	u32 lr = (saved_regs[15] - 16) & (~15);
	for(i=0; i<4; i++){
		print16b(lr + i * 16);
	}
	*/

	dprintf_rom("ERTOS: 0x");
	dputint((u32)g_ertos);
	if(g_ertos->inirq){
		dprintf_rom("\n  In IRQ\n");

#ifndef CONFIG_VBUF_BASE
#define CONFIG_VBUF_BASE 0x10060000
#endif
		for(i=0; i<0x3000; i+=16){
			print16b(CONFIG_VBUF_BASE - 0x3000 + i);
		}
	}else{
		dprintf_rom("\n  CurTask:");
		dputint((u32)g_ertos->cur_task);
		dprintf_rom("\n    Prio:");
		dputint(g_ertos->cur_task->prio);
		dprintf_rom("\n    CurStack:");
		dputint((u32)g_ertos->cur_task->cur_stk);
		dprintf_rom("\n    Name:");
void lowlevel_putc(unsigned char c);
		for(i=0; i<ERTOS_TASK_NAMELEN && g_ertos->cur_task->name[i]; i++){
			lowlevel_putc(g_ertos->cur_task->name[i]);
		}
		dprintf_rom("\n  Stack Dump:\n");
		u32 tmpaddr = (((u32)g_ertos->cur_task->stk_start) & (~15));
		for(; tmpaddr < (u32)g_ertos->cur_task->stk_end; tmpaddr += 16){
			print16b(tmpaddr);
		}
	}

	u32 lr = (saved_regs[15] - 16) & (~15);
	for(i=0; i<4; i++){
		print16b(lr + i * 16);
	}

	while(1){
		{volatile int d; for(d=0; d<0x100000; d++);}
		lowlevel_putc('.');
	}
}
