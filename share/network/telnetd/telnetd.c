#include "includes.h"

static t_debugprintf_hw g_debugprintf_hw_telnet = {
};

extern t_debugprintf * cur_dp;

#define TELNETD_RWBUF_MAX 1024
static unsigned char rwbuf[TELNETD_RWBUF_MAX];

void * telnetd_routine(void* arg)
{
	t_telnetd_cfg * cfg = (t_telnetd_cfg *)arg;

	pthread_detach(pthread_self());

	pthread_setname(NULL, "TelnetD");

	if(cfg->listen_port == 0){
		cfg->listen_port = 23;
	}

	cfg->socket = socket(AF_INET, SOCK_STREAM, 0);
	if( cfg->socket < 0 ){
		syslog(LOG_ERR, "ERROR: telnetd socket() fail.\n");
		return NULL;
	}
	
	struct sockaddr_in local_addr; // local
	memset(&local_addr, 0, sizeof(struct sockaddr_in));
	local_addr.sin_family = AF_INET;
	local_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	local_addr.sin_port = htons(cfg->listen_port); 
	if (bind(cfg->socket, (struct sockaddr *)&local_addr, sizeof(struct sockaddr_in)) == -1){
		syslog(LOG_ERR, "ERROR: telnetd bind() fail.\n");
		close(cfg->socket);
		return NULL;
	}

	//listen
	if (listen(cfg->socket, 1) < 0){
		syslog(LOG_ERR, "[ERROR] telnetd listen error\n");
		close(cfg->socket);
		return NULL;
	}

	syslog(LOG_DEBUG, "telnetd create socket:%x\n", cfg->socket);

	while(1){
		//accept
		int fd = accept(cfg->socket, NULL, NULL);
		if(fd < 0){
			syslog(LOG_ERR, "[ERROR] telnetd accept error\n");
			continue;
		}

		int ret;
		unsigned char iac[3];
		int iac_step = 0;

		//backup old dp
		t_debugprintf_hw * old_hw = cur_dp->hw;
		void * old_hwarg = cur_dp->hw_arg;
		u32 old_flag = cur_dp->flag;

		//set to new debugprintf interface
		debug_printf_set(DEBUG_PRINTF_MODE_TRUNCATE | DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_telnet, 0);

		while(1){
			//print out all the chars
			while(1){
				int n = debug_printf_getbuf(cur_dp, rwbuf, TELNETD_RWBUF_MAX);
				if(n > 0){
					write(fd, rwbuf, n);
				}else{
					break;
				}
			}

			//select for 100ms
			fd_set rfds;
			struct timeval tv;
			int retval;

			FD_ZERO(&rfds);
			FD_SET(fd, &rfds);
			tv.tv_sec = 0;
			tv.tv_usec = 100000;
			retval = select(fd + 1, &rfds, NULL, NULL, &tv);
			if(retval <0){
				break;
			}else if(retval == 0){
				continue;
			}else{
				if(! FD_ISSET(fd, &rfds)){
					continue;
				}
			}

			ret = read(fd, rwbuf, 256);
			if(ret == 0){
				break;
			}else if(ret < 0){
				if(errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK){
					continue;
				}
				syslog(LOG_ERR, "[ERROR] telnetd read err\n");
				break;
			}

			int i;
			for(i=0; i<ret; i++){
				if(iac_step == 0){
#define TELNET_IAC 0xff
					if(rwbuf[i] == TELNET_IAC){
						iac[iac_step] = TELNET_IAC;
						iac_step = 1;
					}else{
#if 0
						if(!isprint(rwbuf[i])){
							debug_printf("recv:%x\n", rwbuf[i]);
						}
#endif
						cmdshell_input_char(rwbuf[i]);
					}
				}else{
					iac[iac_step] = rwbuf[i];
					iac_step ++;
					if(iac_step == 3){
#define TELNET_WILL	251
#define TELNET_WONT	252
#define TELNET_DO	253
#define TELNET_DONT	254
						if(iac[1] == TELNET_WILL){ //will
							iac[1] = TELNET_DONT;  //don't
						}else if(iac[1] == TELNET_DO){
							iac[1] = TELNET_WONT;
						}else if(iac[1] == TELNET_WONT){
							iac[1] = TELNET_DONT;
						}else{
							iac[1] = TELNET_WONT;
						}

						iac_step = 0;
					}
				}
			}
			cmdshell_input_notify();
		}

		//connect lost, switch to old debuginterface
		debug_printf_set(old_flag, old_hw, old_hwarg);

		close(fd);
		continue;
	}

	return NULL;
}

int telnetd_start(t_telnetd_cfg * cfg)
{
	int ret = pthread_create(&cfg->pt, NULL, telnetd_routine, cfg);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: telnetd thread create error\n");
		return -4;
	}
	return 0;
}


int cmd_telnetd(int argc, char ** argv)
{
	static t_telnetd_cfg telnetd_cfg;
	syslog(LOG_INFO, "Start Telnetd\n");
	memset(&telnetd_cfg, 0, sizeof(telnetd_cfg));
	if(telnetd_start(&telnetd_cfg) < 0){
		syslog(LOG_ERR, "[ERROR]: start telnetd failed\n");
	}
}

CMDSHELL_DECLARE(telnetd)
	.cmd = {'t', 'e', 'l', 'n', 'e', 't', 'd', '\0'},
	.handler = cmd_telnetd,
	.comment = "start telnetd",
};
