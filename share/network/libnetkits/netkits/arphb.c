/*
 * arphb.c
 *
 *  Created on: 2017年7月13日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "heartbeat.h"
#include "libnetkits.h"
#include "lwip/tcp.h"





struct arp_hdr {
  struct eth_hdr ethhdr;
  u16_t hwtype;
  u16_t protocol;
  u8_t hwlen;
  u8_t protolen;
  u16_t opcode;
  struct eth_addr shwaddr;
  u32_t sipaddr;
  struct eth_addr dhwaddr;
  u32_t dipaddr;
}__attribute__((packed));

#define ARP_REQUEST 1
#define ARP_REPLY   2
#define ARP_HWTYPE_ETH 1
#define ARP_PKT_LEN	42
#define UIP_ETHTYPE_ARP 0x0806
#define UIP_ETHTYPE_IP  0x0800
u8 arpbuf[ARP_PKT_LEN];


int create_arprespone_package_withip(struct netif *netif, ip4_addr_t * destip, u8 **buf)
{
	struct arp_hdr * BUF   = (struct arp_hdr *)arpbuf;
	struct eth_addr  ethaddr;
	int ret;
	struct eth_addr eth_broadcast = {{0xff,0xff,0xff,0xff,0xff,0xff}};
	ip4_addr_t  ip_broadcast = {{0xffffffff}};
	struct eth_addr eth_ret;
	ip4_addr_t  ip_ret;
	int t1 = 4;

	while(t1 && etharp_lookup(netif, destip, &eth_ret, &ip_ret) != 0){
		msleep(500);
		t1--;
	}


	debug_printf("uip_arp_lookup ip %x, %x:%x:%x:%x:%x:%x\n", ip_ret, eth_ret.addr[0],eth_ret.addr[1],eth_ret.addr[2],eth_ret.addr[3],eth_ret.addr[4],eth_ret.addr[5]);
	/* The reply opcode is 2. */
	BUF->opcode = htons(ARP_REPLY);

	memcpy(BUF->dhwaddr.addr, eth_ret.addr, 6);
    memcpy(BUF->shwaddr.addr, netif->hwaddr, 6);
    memcpy(BUF->ethhdr.src.addr, netif->hwaddr, 6);
    memcpy(BUF->ethhdr.dest.addr, BUF->dhwaddr.addr, 6);

    BUF->sipaddr = netif_ip4_addr(netif)->addr;
    BUF->dipaddr=ip_ret.addr;

	BUF->ethhdr.type = htons(UIP_ETHTYPE_ARP);

    BUF->hwtype = htons(ARP_HWTYPE_ETH);
    BUF->protocol = htons(UIP_ETHTYPE_IP);
	BUF->hwlen = 6;
    BUF->protolen = 4;
    if(buf) *buf= arpbuf;
	return ARP_PKT_LEN; /*sizeof(struct arp_hdr)*/
}

