/*
 * heartbeat.c
 *
 *  Created on: 2017年4月25日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "heartbeat.h"
#include "libnetkits.h"
#include "lwip/tcp.h"

static HB_CB_FN heartbeat_callback = NULL;
static t_ertos_semhandler sem1=NULL;
static netif_linkoutput_fn old_fn=NULL;
static char *heartbeat_buf=NULL;
static int heartbeat_len=0;
static int hbsock;
#define PLAYLOAD_BUFLEN 300
static char playload_buf[PLAYLOAD_BUFLEN];
static int playload_datalen=0;

static err_t netif_wifi_low_level_output_for_heartbeat( struct netif *netif, struct pbuf *p )
{
	err_t eret = ERR_OK;

	if(heartbeat_callback && heartbeat_callback(hbsock,sem1,heartbeat_buf,&heartbeat_len,p) == ERR_OK)
	{
		heartbeat_callback = NULL;
		return ERR_OK;
	}

	pbuf_ref(p);

	//real sendout to hardware
	/* p->payload, p->len, p->next */
	int ret =0,err_count =0;
	struct pbuf *myp = p;
	while(1)
	{
		ret = libwifi_packet_write(myp->payload, myp->len, 0);
		if(ret ==0 && (myp->next!= NULL))
			myp = myp->next;
		else if(ret!=0)
		{
			// break out after some waiting to avoid tcpip thread being blocked.
			if(err_count > 5){
				syslog(LOG_ERR, "wifi packet write timeout\n");
				eret = ERR_TIMEOUT;
				break;
			}
			extern int libwifi_sendbuf_wait(void);
			libwifi_sendbuf_wait();

			err_count ++;
			continue;
		}
		else
			break;
	}
	//release
	pbuf_free(p);

	return eret;
}


static err_t heartbeat_handle(int socket, t_ertos_semhandler sem,char *hb_buf,int *hb_len, struct pbuf *p )
{
	struct eth_hdr *peth_hdr=NULL;
	struct ip_hdr * pip_hdr=NULL;
	struct tcp_hdr * ptcp_hdr=NULL;
	struct udp_hdr * pudp_hdr=NULL;
	struct etharp_hdr *parp_hdr=NULL;
	char *playload=NULL;
	struct netconn * conn=NULL;
	struct tcp_pcb * tcppcb=NULL;
	struct udp_pcb * udppcb=NULL;

	conn = lwip_netconn(hbsock);
	if(!conn) return ERR_VAL;
	if(!sem || !hb_buf || !hb_len || !*hb_len || !p) return ERR_VAL;
	peth_hdr = (struct eth_hdr *)p->payload;
	if(htons(peth_hdr->type) == ETHTYPE_IP)
	{
		pip_hdr = (struct ip_hdr *)&peth_hdr[1];
		if(pip_hdr->_proto == IP_PROTO_UDP)
		{
			pudp_hdr = (struct udp_hdr *)&pip_hdr[1];
			playload = (char*)&pudp_hdr[1];
			udppcb = conn->pcb.udp;
			if(!ip_addr_cmp(&udppcb->remote_ip, &pip_hdr->dest)	||
					udppcb->remote_port != ntohs(pudp_hdr->dest) ||
					udppcb->local_port != ntohs(pudp_hdr->src))
				return ERR_VAL;
		}
		if(pip_hdr->_proto == IP_PROTO_TCP)
		{

			ptcp_hdr = (struct tcp_hdr *)&pip_hdr[1];
			playload = (char *)&ptcp_hdr[1];
			tcppcb = conn->pcb.tcp;
			if(!ip_addr_cmp(&tcppcb->remote_ip , &pip_hdr->dest) ||
					tcppcb->remote_port != ntohs(ptcp_hdr->dest) ||
					tcppcb->local_port != ntohs(ptcp_hdr->src))
				return ERR_VAL;
		}
		if(!memcmp(playload_buf,playload,playload_datalen))
		{
			*hb_len = p->len>*hb_len?*hb_len:p->len;
			memcpy(hb_buf,p->payload,*hb_len);
			ertos_sem_send(sem);
			return ERR_OK;
		}
	}
	if(peth_hdr->type == ETHTYPE_ARP)
	{
		parp_hdr = (struct etharp_hdr *)&peth_hdr[1];
	}
	return ERR_VAL;
}

static err_t DHCP_hb_handle(int socket, t_ertos_semhandler sem,char *hb_buf,int *hb_len, struct pbuf *p )
{
	struct eth_hdr *peth_hdr=NULL;
	struct ip_hdr * pip_hdr=NULL;
	struct tcp_hdr * ptcp_hdr=NULL;
	struct udp_hdr * pudp_hdr=NULL;
	struct etharp_hdr *parp_hdr=NULL;
	char *playload=NULL;
	struct netconn * conn=NULL;
	struct tcp_pcb * tcppcb=NULL;
	struct udp_pcb * udppcb=NULL;
	if(!sem || !hb_buf || !hb_len || !*hb_len || !p) return ERR_VAL;
	peth_hdr = (struct eth_hdr *)p->payload;
	if(htons(peth_hdr->type) == ETHTYPE_IP)
	{
		pip_hdr = (struct ip_hdr *)&peth_hdr[1];
		if(pip_hdr->_proto == IP_PROTO_UDP)
		{
			pudp_hdr = (struct udp_hdr *)&pip_hdr[1];
			playload = (char*)&pudp_hdr[1];
			if(	ntohs(pudp_hdr->dest) == 67 &&
				ntohs(pudp_hdr->src) == 68 &&
				playload[0]==1)
			{
				*hb_len = p->len>*hb_len?*hb_len:p->len;
				memcpy(hb_buf,p->payload,*hb_len);
				ertos_sem_send(sem);
				return ERR_OK;
			}
		}

	}
	return ERR_VAL;
}

static int heartbeat_cb_set(struct netif *netif,HB_CB_FN hb_cb)
{
	if(netif && !heartbeat_callback)
	{
		heartbeat_callback = hb_cb?hb_cb:heartbeat_handle;
		old_fn = netif->linkoutput;
		netif->linkoutput = netif_wifi_low_level_output_for_heartbeat;
		if(!sem1)
			sem1 = ertos_sem_create();
		return 0;
	}
	return -1;
}

static int heartbeat_cb_unset(struct netif *netif)
{
	if(netif && heartbeat_callback)
	{
		heartbeat_callback = NULL;
		if(old_fn)
			netif->linkoutput = old_fn;
		ertos_sem_delete(sem1);
		return 0;
	}
	return -1;
}

//ret >0 buflen. 0: failed.
int heartbeat_get(struct netif *netif,HB_CB_FN hb_cb, int sockfd, char *buf, int buflen, const char *payload, int payloadlen)
{
	int ret;
	if(heartbeat_cb_set(netif,hb_cb)==-1)
		return 0;
	if(!buf||buflen==0||payloadlen>buflen||payloadlen>PLAYLOAD_BUFLEN) return -1;
	heartbeat_buf = buf;
	heartbeat_len = buflen;
	memcpy(playload_buf,payload,payloadlen);
	playload_datalen = payloadlen;
	hbsock = sockfd;
	send(sockfd, payload,payloadlen,0);
	ret = ertos_sem_recv(sem1,3000);
	if(ret)
	{
		ret = heartbeat_len;
	}else{
		printf("heartbeat_get timeout\n");
	}
	heartbeat_cb_unset(netif);
	return ret;
}

//ret >0 buflen. 0: failed.
int DHCP_hb_get(struct netif *netif,HB_CB_FN hb_cb, char *buf, int buflen)
{
	int ret;
	debug_printf("DHCP_hb_get\n");
	if(heartbeat_cb_set(netif,hb_cb)==-1)
		return 0;
	if(!buf||buflen==0) return -1;
	heartbeat_buf = buf;
	heartbeat_len = buflen;
	hbsock = 0;
	debug_printf("dhcp_renew\n");
	dhcp_renew(netif);
	ret = ertos_sem_recv(sem1,3000);
	if(ret)
	{
		ret = heartbeat_len;
	}else{
		printf("heartbeat_get timeout\n");
	}
	heartbeat_cb_unset(netif);
	return ret;
}


struct s_heartbeat_thread_arg{
	int argc;
	char **argv;
} ;
ERTOS_EVENT_DECLARE(heartbeat_evt_handler)
#define netif_dhcp_data(netif) ((struct dhcp*)netif_get_client_data(netif, LWIP_NETIF_CLIENT_DATA_INDEX_DHCP))
extern int create_arprespone_package_withip(struct netif *netif, ip4_addr_t * destip, u8 **buf);
void * heartbeat_thread(void * arg)
{
	struct s_heartbeat_thread_arg *o_arg = (struct s_heartbeat_thread_arg*)arg;
	int argc = o_arg->argc;
	char **argv = o_arg->argv;
	char playload[PLAYLOAD_BUFLEN];
	int playload_len=0;
	char hb_buf[1500];
	int hb_len;
	int udpflag = 0;
	int arpflag = 0;
	int DHCPflag = 0;
	int interval = 1;
	int cleanall = 0;
	int resendtime = 4;
	int index = 1;
	int ret;
	int soketfd;
	struct sockaddr_in remote_address;
	struct netif *netif;
	t_wifi_keepalive_pkt pkt;
	t_wifi_tcpkeepalive_pkt tcppkt;
	struct s_getopt opt;
	ip4_addr_t arp_target;
	u8 * arp_buf;

	memset(&opt,0,sizeof(opt));

    while(1) {
        int c = getopt2(&opt, argc, argv, "Daci::hup:d::r::");
        if(c == -1) break;
        switch(c) {
			case 'D':
				DHCPflag=1;
				break;
    		case 'a':
    			arpflag=1;
    			break;
        	case 'u':
        		udpflag=1;
        		break;
        	case 'c':
        		cleanall=1;
        		break;
        	case 'd':
            	if(opt.optarg)
            		index = atoi(opt.optarg);
            	else
            		index = 1;
        		break;
        	case 'r':
            	if(opt.optarg)
            		resendtime = atoi(opt.optarg);
            	else
            		resendtime = 4;
        		break;
            case 'p':
            	strcpy(playload,opt.optarg);
            	playload_len = strlen(opt.optarg);
            	break;
            case 'i':
            	if(opt.optarg)
            		interval = atoi(opt.optarg);
            	else
            		interval = 1;
            	break;
            case 'h':
            default:
            	debug_printf("heartbeat usage:\n");
            	debug_printf("heartbeat add [options] [ip] [port]");
            	debug_printf("-u: udp heartbeat,default tcp heartbeat\n");
            	debug_printf("-D: dhcp renew heartbeat\n");
            	debug_printf("-p: heartbeat playload\n");
            	debug_printf("-i: interval, default 1s, unit second.\n");
            	debug_printf("-d: heartbeat index, default 1, 1~9\n");
            	debug_printf("-r: tcp resend time, default 4, unit second.\n");
            	debug_printf("-c: clean all heartbeat\n");
            	debug_printf("-a: [ip] arp heartbeat, default is gw.\n");
            	debug_printf("-h: help\n");
            	ertos_event_signal(heartbeat_evt_handler);
            	return NULL;
            	break;
        }
    }
    if(cleanall)
    {
    	debug_printf("clean all keepalive pkt\n");
    	libwifi_keepalive_pkt(NULL,0);
    	ertos_event_signal(heartbeat_evt_handler);
    	return NULL;
    }

    debug_printf("opt.optind=%d, interval %d\n",opt.optind, interval);
//    debug_printf("argv[opt.optind] %s\n",argv[opt.optind]);
//    debug_printf("argv[opt.optind+1] %s\n",argv[opt.optind+1]);
    if(interval)
    {
    	if(DHCPflag)
    	{
    		netif = netif_find("wl0");
    		hb_len = DHCP_hb_get(netif,DHCP_hb_handle,hb_buf, sizeof(hb_buf));
			//dump((u8*)hb_buf,hb_len);
    	}else if(arpflag)
    	{
    		netif = netif_find("wl0");
			if(argv[opt.optind])
			{
				arp_target.addr = inet_addr(argv[opt.optind]);
				ret = create_arprespone_package_withip(netif,&arp_target, &arp_buf);
			}else{
				ret = create_arprespone_package_withip(netif,NULL, &arp_buf);
			}
			if(ret)
			{
				memcpy(hb_buf, arp_buf, ret);
				hb_len = ret;
			}

    	}else{

			if(!argv[opt.optind] || !argv[opt.optind+1] ||!playload_len)
			{
				printf("parameters error!\n");
				ertos_event_signal(heartbeat_evt_handler);
				return NULL;
			}


			remote_address.sin_family = AF_INET;
			if(argv[opt.optind])
				remote_address.sin_addr.s_addr = inet_addr(argv[opt.optind]);
			if(argv[opt.optind+1])
				remote_address.sin_port = htons(atoi(argv[opt.optind+1]));

			printf("%s connect to %s:%s,interval %d, playload %s",udpflag?"UDP":"TCP",argv[opt.optind],argv[opt.optind+1],interval, playload);


			if(udpflag)
				soketfd = socket(AF_INET, SOCK_DGRAM, 0);
			else
				soketfd = socket(AF_INET, SOCK_STREAM, 0);

			ret = connect(soketfd, (struct sockaddr *) &remote_address, sizeof(remote_address));
			if (ret == -1) {
				printf("failed\n");
				return NULL;
			}else
				printf("success\n");

			netif = netif_find("wl0");
			hb_len=heartbeat_get(netif,NULL,soketfd, hb_buf, sizeof(hb_buf), playload, playload_len);
			//dump((u8*)hb_buf,hb_len);
    	}
    }
    if(arpflag || DHCPflag)
    {
		pkt.buf = NULL;
		pkt.len = 0;
		if(interval)
		{
			pkt.buf = hb_buf;
			pkt.len = hb_len;
			if(DHCPflag && interval ==1){
				struct dhcp *dhcp;
				dhcp = netif_dhcp_data(netif);
				interval = dhcp->offered_t1_renew;
				if(interval > 2592000) //30day
					interval = 2592000;
				if(interval == 0) interval = 60;
				debug_printf("dhcp renew time %ds, interval %ds\n",dhcp->offered_t1_renew, interval);
			}
		}
		pkt.id = index;
		libwifi_keepalive_pkt(&pkt,interval*1000);
    }else if(udpflag)
	{
		pkt.buf = NULL;
		pkt.len = 0;
		if(interval)
		{
			pkt.buf = hb_buf;
			pkt.len = hb_len;
		}
		pkt.id = index;
		libwifi_keepalive_pkt(&pkt,interval*1000);
		close(soketfd);
	}
	else{
		tcppkt.buf = NULL;
		tcppkt.len = 0;
		tcppkt.resendtime = 0;
		if(interval)
		{
			tcppkt.buf = hb_buf;
			tcppkt.len = hb_len;
			tcppkt.resendtime = resendtime*1000;
		}
		libwifi_tcpkeepalive_pkt(&tcppkt,interval*1000);
	}


	ertos_event_signal(heartbeat_evt_handler);
	return NULL;
}

int cmd_heartbeat(int argc, char ** argv)
{

	struct s_heartbeat_thread_arg s_arg;
	s_arg.argc=argc;
	s_arg.argv=&argv[0];
	ertos_event_init(heartbeat_evt_handler);
	ertos_task_create(heartbeat_thread, "heartbeat thread", 10*1024, &s_arg,  20);
	ertos_event_wait(heartbeat_evt_handler, WAIT_INF);
	ertos_event_delete(heartbeat_evt_handler);

	return 0;
}

