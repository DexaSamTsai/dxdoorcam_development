/*
 * netutils.c
 *
 *  Created on: 2017年3月14日
 *      Author: Cox.Yang
 */

#include "includes.h"
#include "libnetkits.h"
extern struct netif *netif_list;

void netif_ifconfig(char *ifname, char *ipaddr, char *netmask, char *gw, char *dns)
{
	int i=0;
	const ip_addr_t *ip;
	struct netif *netif;
	ip4_addr_t ip1;
	for (netif = netif_list; netif != NULL; netif = netif->next) {
		if(ifname)
		{
			if(ifname[0]!=netif->name[0] || ifname[1]!=netif->name[1] || netif->num != atoi(&ifname[2])) continue;
			else
			{
				void netif_set_ipaddr(struct netif *netif, const ip4_addr_t *ipaddr);
				void netif_set_netmask(struct netif *netif, const ip4_addr_t *netmask);
				void netif_set_gw(struct netif *netif, const ip4_addr_t *gw);
				if(ipaddr && ipaddr_aton(ipaddr, &ip1))
				{
					netif_set_ipaddr(netif,&ip1);
					ipaddr_aton("255.255.255.0", &ip1);
					netif_set_netmask(netif,&ip1);
				}
				if(netmask && ipaddr_aton(netmask, &ip1))
				{
					netif_set_netmask(netif,&ip1);
				}
				if(gw && ipaddr_aton(gw, &ip1))
				{
					netif_set_gw(netif,&ip1);
				}
			}
		}
		debug_printf("%c%c%d\tHWaddr %x:%x:%x:%x:%x:%x  MTU:%d\n",
				netif->name[0],netif->name[1],netif->num,
				netif->hwaddr[0],netif->hwaddr[1],netif->hwaddr[2],
				netif->hwaddr[3],netif->hwaddr[4],netif->hwaddr[5], netif->mtu);
		debug_printf("\tinet addr:%d.%d.%d.%d  Mask:%d.%d.%d.%d  gw:%d.%d.%d.%d\n",
				ip4_addr1(&netif->ip_addr),ip4_addr2(&netif->ip_addr),ip4_addr3(&netif->ip_addr),ip4_addr4(&netif->ip_addr),
				ip4_addr1(&netif->netmask),ip4_addr2(&netif->netmask),ip4_addr3(&netif->netmask),ip4_addr4(&netif->netmask),
				ip4_addr1(&netif->gw),ip4_addr2(&netif->gw),ip4_addr3(&netif->gw),ip4_addr4(&netif->gw));
		if(dns && ipaddr_aton(dns, &ip1)) dns_setserver(0,&ip1);
		for(i=0;i<DNS_MAX_SERVERS;i++)
		{
			ip = dns_getserver(i);
			debug_printf("\tdns%d:%s  ",i, ip4addr_ntoa(ip));
		}
		debug_printf("\n\n");
	}
	return ;
}

int cmd_ifconfig(int argc, char ** argv)
{
	char *netmask=NULL;
	char *gateway=NULL;
	char *dnsserver=NULL;
	char *ifname=NULL;
	char *ipaddr=NULL;

	struct s_getopt opt;
	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "m:g:d:h");
        if(c == -1) break;
        switch(c) {
            case 'm':
            	netmask = opt.optarg;
            	break;
            case 'g':
            	gateway = opt.optarg;
            	break;
            case 'd':
            	dnsserver = opt.optarg;
            	break;
        	case 'h':
            default:
        		printf("Usage:\n");
        		printf("ifconfig [option] ifname ipaddr\n");
        		printf("-m netmask, set netmask\n");
        		printf("-g gateway, set gateway\n");
        		printf("-d dnsserver, set dns server\n");
        		printf("-h , help\n");
        		return 0;
            	break;
	}
    }
	ifname = argv[opt.optind];
	ipaddr = argv[opt.optind+1];

	netif_ifconfig(ifname,ipaddr,netmask,gateway,dnsserver);
	return 0;

}

int cmd_ping(int argc, char ** argv)
{
	int c;
	int count=5;
	int help =0;
	struct s_getopt opt={0};
	struct ping_option ping_opt={0};
    while((c = getopt2(&opt, argc, argv, "c:h"))!=-1) {
        switch(c) {
            case 'c':
            	count=atoi(opt.optarg);
            	debug_printf("set count %d\n",count);
            	break;
            case 'h':
            	help=1;
            	break;
            default:
//                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }
    if(opt.optind>=argc)
    {
    	help=1;goto help;
    }
	ip_addr_t addr={0};
	resolve(argv[1],&addr);

    ping_opt.count = count;
    ping_opt.ip = addr.addr;
    ping_opt.coarse_time = 1000;

    if(ping_start(&ping_opt))
    {
    	debug_printf("start ping %s\n",ip4addr_ntoa((const ip4_addr_t *)&ping_opt.ip));
    }else{
    	debug_printf("start ping failed\n");
    }


help:
	if(help)
	{
		debug_printf("Usage: ping [-c count] destination\n");
	}
	return 0;

}

int getnameinfo(const struct sockaddr *sa, socklen_t salen,
                       char *host, socklen_t hostlen,
                       char *serv, socklen_t servlen, int flags)
{
	struct sockaddr_in * addr = (struct sockaddr_in *)sa;
	ip4_addr_t ip4addr;
	if(!addr) return EAI_FAIL;
	ip4addr.addr = addr->sin_addr.s_addr;
	if(host) ip4addr_ntoa_r(&ip4addr,host,hostlen);
	if(serv) snprintf(serv,servlen,"%d",ntohs(addr->sin_port));
	return 0;
}

int create_filter(int vargc, char **vargv, char *cmd,char *filter, int filter_len)
{
	char *dst_ip_mask = "00000000";
	char *dst_ip_unmask = "FFFFFFFF";
	char *dst_port_mask = "0000";
	char *dst_port_unmask = "FFFF";
	char *tcp_filter="%d 0 0 23 0xFF000000000000%s0000%s00000000000000000000000000000000%s 0x06000000000000%s0000%s00000000000000000000000000000000%s";
	char *udp_filter="%d 0 0 23 0xFF000000000000%s0000%s00000000%s 0x11000000000000%s0000%s00000000%s";
	char *icmp_filter="%d 0 0 23 0xFF000000000000%sFF 0x01000000000000%s08";
	char dstport_str[5]="0000";
	char dstip_str[9]="00000000";
	char *playload=NULL;
	int playloadlen = 0;
	char *playload_mask=NULL;
	u16 dstport=0;
	int udp_flag=0;
	int icmp_flag = 0;
	int dstip_flag=1;
	int index = 100;


#define margv_num 10
	char *margv[margv_num] = {0};

	int argc;
	char **argv;
	int ret=0;


	if(cmd){
		argc = str2argv(cmd, &margv[1],margv_num-1);
		argc++;
		margv[0]="create_filter";
		argv = margv;
	}else
	{
		argc = vargc;
		argv = vargv;
	}

	struct s_getopt opt;
	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "umi::p:P:hb");
        if(c == -1) break;
        switch(c) {
            case 'u':
            	udp_flag = 1;
            	break;
            case 'm':
            	icmp_flag = 1;
            	break;
            case 'i':
            	if(opt.optarg)
            		index = atoi(opt.optarg);
            	else index  = 100;
            	break;
            case 'p':
            	if((dstport = atoi(opt.optarg)))
            	{
            		snprintf(dstport_str,5,"%02x%02x",(dstport>>8)&0xff,dstport&0xff);
            	}
            	break;
            case 'P':
            	playload = opt.optarg;
            	playloadlen = strlen(playload);
            	playload_mask = malloc(playloadlen+1);
            	memset(playload_mask,'F',playloadlen);
            	playload_mask[playloadlen]=0;
            	break;
            case 'b':
            	dstip_flag = 0 ; // ignore dest ip addr.
            	break;
            case 'h':
            	debug_printf("filter usage:\n");
            	debug_printf("-p: dest port\n");
            	debug_printf("-u: udp filter,default tcp\n");
            	debug_printf("-m: icmp filter,default tcp\n");
            	debug_printf("-P: playload, wakeup magic data\n");
            	debug_printf("-b: ignore dest ip addr\n");
            	debug_printf("-i: filter index\n");
            	goto exit;
            	break;
            default:
//                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }
    if(filter)
    {
    	struct netif * netif;
    	netif = netif_find("wl0");
    	if(netif)
    	{
    		snprintf(dstip_str,9,"%02x%02x%02x%02x",(netif->ip_addr.addr)&0xff,
    				(netif->ip_addr.addr>>8)&0xff,(netif->ip_addr.addr>>16)&0xff,(netif->ip_addr.addr>>24)&0xff);
    	}
    	if(icmp_flag)
    	{
    		ret = snprintf(filter,filter_len,icmp_filter,index,
    				dstip_flag?dst_ip_unmask:dst_ip_mask,
    				dstip_flag?dstip_str:dst_ip_mask);
    	}else if(udp_flag){
			ret = snprintf(filter,filter_len,udp_filter,index,
					dstip_flag?dst_ip_unmask:dst_ip_mask,
					dstport?dst_port_unmask:dst_port_mask, playload?playload_mask:"",
					dstip_flag?dstip_str:dst_ip_mask,
					dstport?dstport_str:dst_port_mask, playload?playload:"");
    	}else
    	{
			ret = snprintf(filter,filter_len,tcp_filter,index,
					dstip_flag?dst_ip_unmask:dst_ip_mask,
					dstport?dst_port_unmask:dst_port_mask, playload?playload_mask:"",
					dstip_flag?dstip_str:dst_ip_mask,
					dstport?dstport_str:dst_port_mask, playload?playload:"");
    	}
    }

exit:
	if(playload_mask)
		free(playload_mask);
    return ret;

}

int cmd_filter(int argc, char ** argv)
{
	int ret;
	char filter[1024];
	filter[0]=0;
	ret = create_filter(argc, argv, NULL,filter, sizeof(filter));
	debug_printf("filter len %d: %s",ret,filter);
	return ret;
}


struct dns_cb_arg{
	pthread_cond_t dns_cond;
	ip_addr_t *addr;
};
static void dns_found_cb(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
	struct dns_cb_arg *arg = (struct dns_cb_arg*) callback_arg;

	if(ipaddr)
	{
		if(arg->addr)
			*arg->addr = *ipaddr;
	}else
	{
		arg->addr = NULL;
	}

	pthread_cond_signal(&arg->dns_cond);
	return ;
}



err_t resolve(const char *hostname, ip_addr_t *addr)
{
	err_t err;
	struct dns_cb_arg arg;

	if(!addr || !hostname) return ERR_ARG;
	pthread_cond_init(&arg.dns_cond, NULL);
	addr->addr=0;
	arg.addr = addr;
	err = dns_gethostbyname(hostname, addr, dns_found_cb, &arg);
	if (err == ERR_INPROGRESS) {
		pthread_cond_wait(&arg.dns_cond, NULL);
		if(arg.addr) err = ERR_OK;
		else	err = ERR_TIMEOUT;
	}
	pthread_cond_destroy(&arg.dns_cond);
	return err;
}


int cmd_dns(int argc, char ** argv)
{
	int ret;
	ip_addr_t addr={0};
	ret = resolve(argv[1],&addr);
	debug_printf("ret %d, get host %s ip %s\n",ret, argv[1], ipaddr_ntoa(&addr));
	return ret;
}


int netkit_tcp_connect(char *url, u16 port)
{
	int skt;
	int ret;
	struct sockaddr_in remote_address;
	ip_addr_t ipaddr;

	ret = -1;
	if(!url)
		goto exit;

	ret = resolve(url,&ipaddr);
	if(ret != 0)
	{
		debug_printf("reslove %s failed,giveup\n",url);
		ret= -1;
		goto exit;
	}else
	{
		debug_printf("get %s ip address %s\n",url, inet_ntoa(ipaddr));
	}

	remote_address.sin_family = AF_INET;
	remote_address.sin_addr.s_addr = ipaddr.addr;
	remote_address.sin_port = htons(port);

	skt = socket(AF_INET, SOCK_STREAM, 0);
	if(skt==-1)
	{
		ret = -1;
		goto exit;
	}

	ret = connect(skt, (struct sockaddr *) &remote_address, sizeof(remote_address));
	if (ret == -1) {
		debug_printf("tcp connect failed\n");
		close(skt);
		goto exit;
	}else
		debug_printf("tcp connect success\n");

	return skt;
exit:
	return ret;
}


int netkit_tcp_sendonce_msg(u8 *msg, int msglen, char *url, u16 port)
{
	int ret;
	int len;
	int offset=0;
	int skt;

	ret = netkit_tcp_connect(url, port);
	if(ret == -1) return 0;
	skt = ret;
	len = msglen;
	while( len>0 )
	{
		ret = send(skt,msg+offset,len,0);
		if(ret < 0)
			goto exit;
		len -= ret;
		offset += ret;
	}
exit:
	close(skt);
	return msglen-len;
}

