/*
 * tun.c
 *
 *  Created on: 2017年3月14日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "lwip/tcpip.h"
#include "lwip/debug.h"
#include "lwip/netif.h"
#include "lwip/igmp.h"
#include "lwip/snmp.h"
#include "netif/etharp.h"
#include "netif/tun.h"
#include "lwip/tcp.h"



struct s_tun_thread_arg{
	int argc;
	char **argv;
} ;

ERTOS_EVENT_DECLARE(tun_test_evt_handler)

static int flag = 1;
void * tun_test_thread(void * arg)
{
	struct s_tun_thread_arg *o_arg = (struct s_tun_thread_arg*)arg;
	int argc = o_arg->argc;
	char **argv = o_arg->argv;
	struct ifreq ifr;
	int ret;
	int fd;
	u8 buf[1500];
	unsigned char ip[4];


	fd_set fdset;
	struct timeval t1;

	if(argc>2)
	{
		if(!strcmp(argv[1],"open") && argv[2])
		{
			if(strcmp(argv[2],"tun") && strcmp(argv[2],"tap"))
			{
				debug_printf("error type!\n");
				return NULL;
			}
			debug_printf("create a %s netif\n",argv[2]);
			fd = tun_open("net/tun",O_RDWR);
			ifr.ifr_flags = IFF_NO_PI;
			if(!strcmp("tun",argv[2]))
			{
				ifr.ifr_flags |= IFF_TUN;
			}else{
				ifr.ifr_flags |= IFF_TAP;
			}
			strncpy(ifr.ifr_name, argv[2], IFNAMSIZ-1);

	        /*
	         * Use special ioctl that configures tun/tap device with the parms
	         * we set in ifr
	         */
	        if (tun_ioctl(fd, TUNSETIFF, (int) &ifr) < 0)
	        {
	           debug_printf("ERROR: Cannot ioctl TUNSETIFF %s",argv[2]);
	        }
	        ertos_event_signal(tun_test_evt_handler);

	        while(1)
	        {
				t1.tv_sec=10;
	        	t1.tv_usec=0;
				FD_ZERO((fd_set *)&fdset);
				FD_SET(fd, &fdset);
				ret = select(fd+1, (fd_set *)&fdset, (fd_set *) 0, (fd_set *) 0, (struct timeval *)&t1);
				if(ret < 0)
				{
					printf("select error\n");
				}
				else if(ret >0)
				{

					if (FD_ISSET(fd,  &fdset))
					{
						ret = read(fd, buf, sizeof(buf));
						if (ret < 0)
							break;
						extern void dump(u8* buf, u32 len);
						dump(buf,ret);
						memcpy(ip, &buf[12], 4);
						memcpy(&buf[12], &buf[16], 4);
						memcpy(&buf[16], ip, 4);
						buf[20] = 0;
						*((u16*)(&buf[22])) += 8;
						ret = write(fd, buf, ret);
						debug_printf("write %d bytes\n", ret);
					}


				}else
				{

					debug_printf("select timeout\n");

				}

	        }
	        close(fd);
	     }

	}
	return NULL;
}



int cmd_tun(int argc, char ** argv)
{

	if(argc>2)
	{
		if(!strcmp(argv[1],"open") && argv[2])
		{
			struct s_tun_thread_arg s_arg;
			s_arg.argc=argc;
			s_arg.argv=argv;
			ertos_event_init(tun_test_evt_handler);
			ertos_task_create(tun_test_thread, "tun_test", 10*1024, &s_arg,  20);
			ertos_event_wait(tun_test_evt_handler, WAIT_INF);
			ertos_event_delete(tun_test_evt_handler);
		}
		if(!strcmp(argv[1],"read") && argv[2])
		{
			flag = 1;
		}

	}


	return 0;

}





