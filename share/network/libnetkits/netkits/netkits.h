/*
 * netutils.h
 *
 *  Created on: 2017年3月14日
 *      Author: Cox.Yang
 */

#ifndef _NETKITS_H_
#define _NETKITS_H_

int getnameinfo(const struct sockaddr *sa, socklen_t salen,
                       char *host, socklen_t hostlen,
                       char *serv, socklen_t servlen, int flags);
void netif_ifconfig(char *ifname, char *ipaddr, char *netmask, char *gw, char *dns);
int create_filter(int vargc, char **vargv, char *cmd,char *filter, int filter_len);
err_t resolve(const char *hostname, ip_addr_t *addr);
int netkit_tcp_connect(char *url, u16 port);
int netkit_tcp_sendonce_msg(u8 *msg, int msglen, char *url, u16 port);
#endif /* SHARE_NETWORK_TUN_NETUTILS_H_ */
