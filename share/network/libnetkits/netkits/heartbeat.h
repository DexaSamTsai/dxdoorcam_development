/*
 * heartbeat.h
 *
 *  Created on: 2017年4月25日
 *      Author: Cox.Yang
 */

#ifndef SHARE_NETWORK_LIBNETKITS_NETKITS_HEARTBEAT_H_
#define SHARE_NETWORK_LIBNETKITS_NETKITS_HEARTBEAT_H_

typedef err_t (*HB_CB_FN)(int socket, t_ertos_semhandler sem,char *hb_buf,int *hb_len, struct pbuf *p);
int heartbeat_get(struct netif *netif,HB_CB_FN hb_cb, int sockfd, char *buf, int buflen, const char *payload, int payloadlen);

#endif /* SHARE_NETWORK_LIBNETKITS_NETKITS_HEARTBEAT_H_ */
