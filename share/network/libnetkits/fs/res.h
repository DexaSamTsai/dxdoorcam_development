#ifndef _RES_H_
#define _RES_H_

int res_read(int handle, char * buf, int length);
int res_seek( int handle, long offset, int whence  );
int res_close( int handle );
int res_open( char *filename);
char *res_gets(char *s, int size, int handle);
int res_eof(int handle);
int res_getc(int handle);
int res_getpos(int handle, fpos_t * pos);
int res_access(const char *filename, int mode);
#endif
