/*
 * fileop.h
 *
 *  Created on: 2017年3月6日
 *      Author: Cox.Yang
 */

#ifndef RAM_WIFI_TEST_LIB_FS_FILEOP_H_
#define RAM_WIFI_TEST_LIB_FS_FILEOP_H_
int fs_add_cmds(void);
int fs_init(void);
#endif /* RAM_WIFI_TEST_LIB_FS_FILEOP_H_ */
