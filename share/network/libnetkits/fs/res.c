#include "includes.h"
#include "res.h"

struct res_fd_t{
	int group;
	int item;
	char key[32];
	int skip;
	int totallen;

};

int res_open(char *filename)
{
	int ret;
	struct res_fd_t *res = calloc(1,sizeof(struct res_fd_t));
	char *ptr;
	int group;
	char *file = calloc(1,strlen(filename)+1);
	char buf[1];
	int totallen;
	if(!res || !file) return 0;
	if(strncmp(filename,"/res/",strlen("/res/")))
		return 0;
	ptr = &filename[strlen("/res/")];
	ret = sscanf(ptr, "%d%s", &group, file );
	debug_printf("sscanf return ret=%d, group %d, file %s\n",ret, group, file);
	res->group = group;
	res->item = 0;
	strncpy(res->key,file,sizeof(res->key)-1);
	free(file);
	res->skip = 0;
	debug_printf("key: %s\n",res->key);
	ret=partition_res_load_sflash(res->group,res->item,res->key,buf,res->skip,sizeof(buf),&res->totallen);
	debug_printf("partition_res_load_sflash return %d, len %d\n",ret, res->totallen);

	return (int)res;
}

int res_access(const char *filename, int mode)
{
	int ret;
	int group;
	char *ptr;
	char *file = calloc(1,strlen(filename)+1);
	char buf[1];
	if(!file) return -1;
	if(strncmp(filename,"/res/",strlen("/res/")))
		return -1;
	ptr = &filename[strlen("/res/")];
	ret = sscanf(ptr, "%d%s", &group, file );
	debug_printf("sscanf return ret=%d, group %d, file %s\n",ret, group, file);
	ret=partition_res_load_sflash(group,0,file,buf,0,sizeof(buf),NULL);
	free(file);
	return ret?0:-1;
}

int res_close( int handle )
{
	if(handle)
		free(handle);
	return 0;
}

int res_seek( int handle, long offset, int whence )
{
	struct res_fd_t *res = (struct res_fd_t *)handle;
	if(SEEK_SET == whence) res->skip = offset;
	if(SEEK_CUR == whence) res->skip += offset;
	if(SEEK_END == whence) res->skip = res->totallen+offset;
	if(res->skip < 0) res->skip = 0;
	if(res->skip > res->totallen) res->skip = res->totallen;
	return 0;
}

int res_read(int handle, char * buf, int length)
{
	int ret;
	struct res_fd_t *res = (struct res_fd_t *)handle;
//	debug_printf("res->skip=%d\n",res->skip);
	if(res->skip>=res->totallen) return 0;
	ret = partition_res_load_sflash(res->group,res->item,res->key,buf,res->skip,length,NULL);
	if(ret>=0)
	{
		res->skip+=ret;
	}
	return ret;
}

char *res_gets(char * s, int size, int handle)
{
	int ret;
	char *ptr;
	struct res_fd_t *res = (struct res_fd_t *)handle;
	if(res->skip>=res->totallen) return NULL;
	ret = partition_res_load_sflash(res->group,res->item,res->key,s,res->skip,size-1,NULL);
	if(ret>=0)
	{
		s[size]=0;
		ptr = strchr(s,'\n');
		if(ptr)
		{
			res->skip+=ptr-s+1;
			*(ptr+1)=0;
		}
		else{
			res->skip+=ret;
			s[ret]=0;
		}
		return s;
	}
	return NULL;
}

int res_eof(int handle)
{
	struct res_fd_t *res = (struct res_fd_t *)handle;
	if(res->skip >= res->totallen)
	{
		return 1;
	}
	return 0;
}

int res_getc(int handle)
{
	int ret;
	char ch;
	struct res_fd_t *res = (struct res_fd_t *)handle;
	ret = partition_res_load_sflash(res->group,res->item,res->key,&ch,res->skip,1,NULL);
	if(ret>=0)
	{
		ret = ch;
		res->skip+=1;
		return ret;
	}
	return EOF;
}

int res_getpos(int handle, fpos_t * pos)
{
	int ret;
	struct res_fd_t *res = (struct res_fd_t *)handle;
	if(pos) *pos = res->skip;
	return 0;
}


int cmd_res(int argc, char ** argv)
{
	int fd,ret;
	char buf[300];
	char *ptr;
	if(argv[1])
	fd = res_open(argv[1]);
//	while(1)
//	{
//		ret = res_read(fd,buf,sizeof(buf)-1);
//		if(ret==0) break;
//		buf[ret]=0;
//		debug_printf("%s",buf);
//	}

	while(1)
	{
		ptr = res_gets(buf,sizeof(buf)-1,fd);
		if(!ptr) break;
		debug_printf("%s",buf);
	}

	debug_printf("\n");
	return 0;
}

