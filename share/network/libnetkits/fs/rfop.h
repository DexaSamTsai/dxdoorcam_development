#ifndef _RFOP_H_
#define _RFOP_H_

typedef struct rf_t{
	int type; // =1, sdcard file system; =0, flash res system.
	int fd; // it can be FILE* or res fd.
}RESFILE;

RESFILE * rfopen(const char *path, const char *mode);
size_t rfread(void * buf, size_t size, size_t count, RESFILE * fp);
int rfclose(RESFILE * fp);
size_t rfwrite(const void * buf, size_t size, size_t count, RESFILE * fp);
int rfseek(RESFILE * fp, long offset, int whence);
char *rfgets(char * s, int size, RESFILE * fp);
int rfeof(RESFILE * fp);
int rferror(RESFILE *fp);
int rfflush(RESFILE * fp);
int rfgetc(RESFILE * fp);
int rfgetpos(RESFILE * fp, fpos_t * pos);
long rftell(RESFILE * fp);
int rfaccess(const char *pathname, int mode);
#endif
