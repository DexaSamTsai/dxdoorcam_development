/*
 * fileop.c
 *
 *  Created on: 2017年3月6日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "libnetkits.h"
static int init_status_fs = 0;
static char dir[500]={"/"};
#define  SD_DETECT_PIN  PIN_GPIO_08
int fs_init(void)
{
	int ret;
	if(!(ret = libgpio_read(SD_DETECT_PIN))){
		debug_printf("No SD card!\n");
		return 1;
	}
    if(init_status_fs == 1){
    	//debug_printf("the fs has already been initialized\n");
    	return 0;
    }
	ret = efs_init(EFS_HW_SCIF, NULL, CONFIG_LIBEFS_BUFFER_ADDRESS, CONFIG_LIBEFS_BUFFER_SIZE);
	if (!ret) {
		init_status_fs = 1;
	} else {
		init_status_fs = 0;
		debug_printf("no sdcard\n");
		return ret;
	}
	if( sh_efs.sectors == 0){
		debug_printf("bad sdcard\n");
		init_status_fs = 0;
		return ret;
	}else{
		init_status_fs = 1;
	}
//	debug_printf("  SD card capacity: %d sectors\n", sh_efs.sectors);
//	debug_printf("  Filesystem sectors per Cluster: %d\n",sh_efs.bpb.sectors_per_cluster);
//	debug_printf("  Filesystem sectors count: %d, cluster count:%d\n",sh_efs.sectors, sh_efs.sectors / sh_efs.bpb.sectors_per_cluster);
//	debug_printf("  Filesystem Free Clusters: %d\n", sh_efs.clusters_free);
//	debug_printf("  Filesystem first Free Cluster: %d\n",sh_efs.next_free_cluster);
//	//设置（./..）是否可用, 为0 不可用，为1 可用
//	debug_printf("Test whether the filter root = %d\n", sh_efs.listdir_filterparent_disable);
	return ret;
}

int cmd_fs_ls(int argc, char ** argv)
{


	DIR *dirptr = NULL;
	int k;
	int n;
	struct dirent *entry;
	char *path;
	if(fs_init())
	{
		return 1;
	}
	if(argv[1])
		path = argv[1];
	else
		path = dir;
	if ((dirptr = opendir(path)) == NULL) {
		debug_printf("open dir is failed\n");
		return 0;
	} else {
		k=0;
		while ((entry = readdir(dirptr)) != NULL) {
			if(entry->attribute == 0) continue;
			k++;
			debug_printf("%ds\t%dB\t",entry->create_time, entry->size);
			n = 0;
			u32 namelen = entry->namelen;
			//debug_printf("the attribute is %x\n", entry->attribute);
			if(entry->attribute & DIRENT_ATTR_DIRECTORY)
				debug_printf("[");
			if((entry->attribute & DIRENT_FLAG_LNAME) == DIRENT_FLAG_LNAME) {
				while (entry->d_name[n] != 0x00 || entry->d_name[n + 1] != 0x00) {
					if (!namelen) {
						break;
					}
					if (entry->d_name[n] == 0x00&& entry->d_name[n + 1] != 0x00) {
						debug_printf("%c", entry->d_name[n + 1]);
					} else {
						debug_printf("%x",((entry->d_name[n] << 8)| (entry->d_name[n + 1])) & 0xffff);
					}
					n += 2;
					namelen -= 2;
				}

			} else if((entry->attribute & DIRENT_FLAG_SNAME) == DIRENT_FLAG_SNAME){
				while(n<8)
				{
					if(entry->d_name[n]==0x20) break;
					debug_printf("%c", entry->d_name[n]);
					n++;
				}
				if(entry->d_name[9]!=0x20)
				{
					debug_printf("%s", &entry->d_name[8]);
				}
			}else{
			}
			if(entry->attribute & DIRENT_ATTR_DIRECTORY)
				debug_printf("]");
			debug_printf("\n");
		}
		debug_printf("total %d\n",k);
		closedir(dirptr);
	}
	return 0;
}

int cmd_fs_cd(int argc, char ** argv)
{

	int len;
	DIR *dirptr = NULL;
	char *path,*ptr;

	if(fs_init())
	{
		return 1;
	}

	if(argv[1])
		path = argv[1];
	else
		path = "/";

	if(path==NULL) return 0;
	len = strlen(dir)+strlen(path)+2;
	if(len > sizeof(dir)) {
		debug_printf("change path failed, path is too long!\n");
		return 0;
	}
	char *cdir=malloc(len);


	if(path[0]!='/')
	{
		strcpy(cdir,dir);

		if(!strcmp(path,".."))
		{
			if(cdir[strlen(cdir)-1]=='/') cdir[strlen(cdir)-1]=0;
			for(ptr = &cdir[strlen(cdir)-1];(*ptr!='/')&&ptr!=cdir;ptr--);
			*(++ptr)=0;
		}else{
			if(cdir[strlen(dir)-1]!='/')
			{
				cdir[strlen(dir)+1]=0;
				cdir[strlen(dir)]='/';
			}
			strcat(cdir,path);
		}
	}else
	{
			strcpy(cdir,path);
	}

	if ((dirptr = opendir(cdir)) == NULL) {
		debug_printf("change dir %s is failed\n", cdir);
	} else {
		closedir(dirptr);
		strcpy(dir,cdir);
	}
	debug_printf("cd %s\n",dir);
	free(cdir);
	return 0;
}

int cmd_fs_pwd(int argc, char ** argv)
{
	if(fs_init())
	{
		return 1;
	}
	debug_printf("%s\n",dir);
	return 0;
}

int cmd_fs_cat(int argc, char ** argv)
{
	FILE *fp;
	char *file;
	char *filename;
	int len;
	int ret;
	char *content;
	struct s_getopt opt={0};
	int dumpflag = 0;
	if(fs_init())
	{
		return 1;
	}

    while(1) {
        int c = getopt2(&opt, argc, argv, "b");
        if(c == -1) break;
        switch(c) {
            case 'b': dumpflag=1; break;
            default: break;
        }
    }



	file=argv[opt.optind];
	if (file == NULL) return 0;


	len = strlen(file)+strlen(dir)+2;
	filename = malloc(len);
	if(!filename) return 0;
	if(file[0]!='/')
	{
		strcpy(filename,dir);
		if(filename[strlen(filename)-1]!= '/')
		{
			filename[strlen(filename)+1] = 0;
			filename[strlen(filename)] = '/';
		}
		strcat(filename,file);
	}else
		strcpy(filename, file);
	debug_printf("cat %s\n",filename);
	fp = fopen(filename, "r");
	if(fp == NULL){
		debug_printf("the file does not exits\n");
		goto exit;
	}
	content = malloc(64*1024);
	if(content)
	{
		while(!feof(fp))
		{
			ret = fread(content, sizeof(u8), 64*1024-1, fp);
			content[ret]=0;
			if(dumpflag)
				dump((u8*)content,ret);
			else
				debug_printf("%s",content);
		}
		debug_printf("\n");
		free(content);
	}else{
		debug_printf("memory is not enough\n");
	}
	fclose(fp);
exit:
	free(filename);
	return 0;
}

int cmd_fs_echo(int argc, char ** argv)
{
	FILE *fp=NULL;
	char *file;
	char *filename;
	int len;
	if(fs_init())
	{
		return 1;
	}

	if(argc == 2)
	{
		debug_printf("%s\n",argv[1]);
		return 0;
	}
	if(argc == 4 && (!strcmp(argv[2],">") || !strcmp(argv[2],">>")))
	{
		file=argv[3];

		len = strlen(file)+strlen(dir)+2;
		filename = malloc(len);
		if(!filename) return 0;
		if(file[0]!='/')
		{
			strcpy(filename,dir);
			if(filename[strlen(filename)-1]!= '/')
			{
				filename[strlen(filename)+1] = 0;
				filename[strlen(filename)] = '/';
			}
			strcat(filename,file);
		}else
			strcpy(filename,file);

		if(!strcmp(argv[2],">"))
		{
			fp = fopen(filename, "w");
		}
		if(!strcmp(argv[2],">>"))
		{
			fp = fopen(filename, "a");
		}
		if(fp)
		{
			fwrite(argv[1],strlen(argv[1]),1,fp);
			fclose(fp);
		}
		else{
			debug_printf("open file %s failed\n",filename);
		}
		free(filename);
		return 0;
	}
	debug_printf("Usage:\necho [content] [>/>>] [filename]\n");
	return 0;
}

int cmd_fs_rm(int argc, char ** argv)
{
	char *file;
	char *filename;
	int len;
	if(fs_init())
	{
		return 1;
	}

	if(argc != 2)
	{
		debug_printf("Usage:\nrm [filename]\n");
		return 0;
	}

	file=argv[1];

	len = strlen(file)+strlen(dir)+2;
	filename = malloc(len);
	if(!filename) return 0;
	if(file[0]!='/')
	{
		strcpy(filename,dir);
		if(filename[strlen(filename)-1]!= '/')
		{
			filename[strlen(filename)+1] = 0;
			filename[strlen(filename)] = '/';
		}
		strcat(filename,file);
	}else
		strcpy(filename,file);

	debug_printf("rm %s\n",filename);
	remove(filename);
	free(filename);
	return 0;
}

int cmd_fs_cp(int argc, char ** argv)
{
	FILE *srcfp,*dstfp;
	char *srcfile;
	char *srcfilename;
	char *dstfile;
	char *dstfilename;
	int len;
	int ret;
	char content[100];
	if(fs_init())
	{
		return 1;
	}

	if(argc != 3)
	{
		debug_printf("Usage:\ncp [srcfile] [dstfile]\n");
		return 0;
	}
	srcfile=argv[1];
	dstfile=argv[2];

	len = strlen(srcfile)+strlen(dir)+2;
	srcfilename = malloc(len);
	if(!srcfilename)
	{
		debug_printf("memory is not enough\n");
		return 0;
	}
	if(srcfile[0]!='/')
	{
		strcpy(srcfilename,dir);
		if(srcfilename[strlen(srcfile)-1]!= '/')
		{
			srcfilename[strlen(srcfile)+1] = 0;
			srcfilename[strlen(srcfile)] = '/';
		}
		strcat(srcfilename,srcfile);
	}else
		strcpy(srcfilename,srcfile);

	len = strlen(dstfile)+strlen(dir)+2;
	dstfilename = malloc(len);
	if(!dstfilename) {
		free(srcfilename);
		debug_printf("memory is not enough\n");
		return 0;
	}
	if(dstfile[0]!='/')
	{
		strcpy(dstfilename,dir);
		if(dstfilename[strlen(dstfile)-1]!= '/')
		{
			dstfilename[strlen(dstfile)+1] = 0;
			dstfilename[strlen(dstfile)] = '/';
		}
		strcat(dstfilename,dstfile);
	}else
		strcpy(dstfilename,dstfile);

	srcfp = fopen(srcfilename, "r");
	if(srcfp == NULL){
		debug_printf("fail open %s\n",srcfilename);
		goto exit;
	}
	dstfp = fopen(dstfilename, "w");
	if(dstfp == NULL){
		debug_printf("fail open %s\n",dstfilename);
		fclose(srcfp);
		goto exit;
	}

	while(!feof(srcfp))
	{
		ret = fread(content, sizeof(u8), sizeof(content), srcfp);
		fwrite(content,sizeof(u8),ret,dstfp);
	}
	fclose(srcfp);
	fclose(dstfp);
exit:
	free(dstfilename);
	free(srcfilename);
	return 0;
}

int fs_add_cmds(void)
{
	int ret;
	ret = cmdshell_add("ls", cmd_fs_ls, "ls");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init ls cmd failed\n");
	}
	ret = cmdshell_add("cd", cmd_fs_cd, "cd");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cd cmd failed\n");
	}
	ret = cmdshell_add("cat", cmd_fs_cat, "cat");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cat cmd failed\n");
	}
	ret = cmdshell_add("pwd", cmd_fs_pwd, "pwd");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init pwd cmd failed\n");
	}
	ret = cmdshell_add("echo", cmd_fs_echo, "echo");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init echo cmd failed\n");
	}
	ret = cmdshell_add("rm", cmd_fs_rm, "rm");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init rm cmd failed\n");
	}
	ret = cmdshell_add("cp", cmd_fs_cp, "cp");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cp cmd failed\n");
	}
	return 0;
}
