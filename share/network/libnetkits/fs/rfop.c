#include "includes.h"
#include "res.h"
#include "rfop.h"

RESFILE * rfopen(const char *path, const char *mode)
{
	int fd;
	RESFILE * rfile = calloc(1,sizeof(RESFILE));
	FILE *file;
	if(!rfile) return NULL;
	fd = res_open(path);
	if(fd)
	{
		rfile->fd = fd;
		rfile->type = 0;
		return rfile;
	}
	file = fopen(path,mode);
	if(file)
	{
		rfile->fd = (int)file;
		rfile->type = 1;
		return rfile;
	}
	if(rfile) free(rfile);
	return NULL;
}
size_t rfread(void * buf, size_t size, size_t count, RESFILE * fp)
{
	if(fp->type) return fread(buf,size,count,(FILE*)fp->fd);
	return res_read(fp->fd, buf, size*count);
}
int rfclose(RESFILE * fp)
{
	int ret;
	if(!fp) return 0;
	if(fp->type) ret = fclose((FILE*)fp->fd);
	else ret = res_close(fp->fd);
	free(fp);
	return ret;
}
size_t rfwrite(const void * buf, size_t size, size_t count, RESFILE * fp)
{
	if(fp->type) return fwrite(buf,size,count,(FILE*)fp->fd);
	else return 0;
}
int rfseek(RESFILE * fp, long offset, int whence)
{
	if(fp->type) return fseek((FILE*)fp->fd,offset,whence);
	else return res_seek(fp->fd,offset,whence);
}
char *rfgets(char * s, int size, RESFILE * fp)
{
	if(fp->type) return fgets(s,size,(FILE*)fp->fd);
	else return res_gets(s,size,fp->fd);
}

int rfeof(RESFILE * fp)
{
	if(fp->type) return feof((FILE*)fp->fd);
	else return res_eof(fp->fd);
}
int rferror(RESFILE *fp)
{
	if(fp->type) return ferror((FILE*)fp->fd);
	else return 0;
}

int rfflush(RESFILE * fp)
{
	if(fp->type) return fflush((FILE*)fp->fd);
	else return 0;
}
int rfgetc(RESFILE * fp)
{
	if(fp->type) return fgetc((FILE*)fp->fd);
	else return res_getc(fp->fd);
}
int rfgetpos(RESFILE * fp, fpos_t * pos)
{
	if(fp->type) return fgetpos((FILE*)fp->fd,pos);
	else return res_getpos(fp->fd,pos);
}
long rftell(RESFILE * fp)
{
	long pos;
	rfgetpos(fp,&pos);
	return pos;
}

int rfaccess(const char *pathname, int mode)
{
	int ret;
	ret = res_access(pathname, mode);
	if(ret)
	{
		return access(pathname, mode);
	}
	return ret;
}

