#include "includes.h"

#include "lwip/opt.h"
#include "lwip/icmp.h"
#include "lwip/inet_chksum.h"
#include "lwip/sockets.h"
#include "lwip/mem.h"
#include "lwip/inet.h"
#include "netif/etharp.h"
#include "lwip/ip.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#include "sntp.h"

t_network_wifi_cfg g_wifi_cfg;
static struct netif wifi_netif;
static struct dhcp wifi_dhcp_ctrl;

static void lwip_init_done(void * arg)
{
	ertos_event_signal(arg);
}

static int network_inited = 0;
static int wifi_inited = 0;
static int wifinet_up = 0;


int wifinetwork_link_callback(int evt)
{
	//FIXME:  This callback is to handle AP lost and reconnecting, so it should only effect after network is firstly started.
	if(network_inited == 0){
		return 0;
	}

	struct netif * netif;
	netif = netif_find("wl0");

	switch(evt){
		case NETIF_LINK_STATUS_OFF:
			syslog(LOG_WARNING, "wifi station is disconnected!!\nDo reconnect...");
			libwifi_reconnect();
			break;
		case NETIF_LINK_STATUS_ON:
			syslog(LOG_WARNING, "wifi station is connected!!\n");
			break;
		case NETIF_LINK_STATUS_UP:
			syslog(LOG_WARNING, "wifi adapter is up!!\n");
			break;
		case NETIF_LINK_STATUS_DOWN:
			syslog(LOG_WARNING, "wifi adapter is down!!\n");
			break;
		default:
			break;
	}

	return 0;
}




#if LWIP_NETIF_LINK_CALLBACK
static void wifi_link_callback(struct netif * netif)
{
	if(g_network_cfg.wifi.netif_link_callback == NULL){
		return;
	}

	if(netif_is_link_up(netif)){
		g_network_cfg.wifi.netif_link_callback(NETIF_LINK_STATUS_ON);
	}else{
		g_network_cfg.wifi.netif_link_callback(NETIF_LINK_STATUS_OFF);
	}
}
#endif

#if LWIP_NETIF_STATUS_CALLBACK
static void wifi_status_callback(struct netif * netif)
{
	if(g_network_cfg.wifi.netif_link_callback == NULL){
		return;
	}

	if(netif_is_up(netif)){
		g_network_cfg.wifi.netif_link_callback(NETIF_LINK_STATUS_UP);
	}else{
		g_network_cfg.wifi.netif_link_callback(NETIF_LINK_STATUS_DOWN);
	}
}
#endif

int wifinetwork_init(void)
{
	if(network_inited == 1){
		syslog(LOG_ERR, "[ERROR]: network stack doesn't support multi init currently.\n");
		return -1;
	}
	// Init tcpip thread.
	t_ertos_eventhandler handler;
	handler = ertos_event_create();
	if(handler == NULL){
		syslog(LOG_ERR, "[ERROR]: network init create event failed\n");
		return -1;
	}
		tcpip_init(lwip_init_done, handler);
	// don't continue until tcpip thread is ready.
	int ret = ertos_event_wait(handler, 100000);
	if( ret != 1){
		syslog(LOG_ERR, "[ERROR]: Lwip Init Fail, Fatal Error!!\n");
		return -1;
	}
	ertos_event_delete(handler);
	network_inited = 1;
	return 0;
}
int wifiadapter_init(void)
{
	if(wifi_inited == 1){
		syslog(LOG_ERR, "[ERROR]: wifi adapter has been up .\n");
		return -1;
	}
	// Init wifi thread.

	extern int wifi_thread_init(void);
	wifi_thread_init();

	debug_printf("wifinetwork_up....\n");
	//init wifi chip
#ifdef CONFIG_WIFIMODULE_INTERFACE_SCIF_02
	int scif_if = SCIF_MODULE_02;
#elif CONFIG_WIFIMODULE_INTERFACE_SCIF_03
	int scif_if = SCIF_MODULE_03;
#else
	int scif_if = SCIF_MODULE_01;
#endif

	t_wifi_conf conf;
	memset(&conf, 0, sizeof(t_wifi_conf));
	conf.contry_code = g_network_cfg.wifi.country_code;

	if(libwifi_init(scif_if, g_network_cfg.wifi.mac, 0, &conf) != WIFI_SUCCESS){
		syslog(LOG_ERR, "[ERROR]: Wifi Init Fail, Fatal Error!!\n");
		return -1;
	}

	wifi_inited = 1;
	return 0;
}

int wifiadapter_up(void)
{
	libwifi_up();
	return 0;
}
int wifiadapter_down(void)
{
	libwifi_down();
	return 0;
}

int wifinetwork_up(void)
{
	wifiadapter_up();
	if(wifinet_up) return 0;
	// add net interface
	ip_addr_t ipaddr, netmask, gw;
	ip_addr_set_zero( &gw );
	ip_addr_set_zero( &ipaddr );
	ip_addr_set_zero( &netmask );
	memset(&wifi_netif, 0, sizeof(wifi_netif));

	extern err_t netif_wifi_init(struct netif * netif);
	struct netif * netif_ret = netif_add(&wifi_netif, &ipaddr, &netmask, &gw, NULL, netif_wifi_init, ethernet_input);
	if( netif_ret == NULL){
		syslog(LOG_ERR, "[ERROR]: Netif Add Fail, Fatal Error!!\n");
		return -1;
	}
	netif_set_up(&wifi_netif);
#if LWIP_NETIF_LINK_CALLBACK
	netif_set_link_callback(&wifi_netif, wifi_link_callback);
#endif
#if LWIP_NETIF_STATUS_CALLBACK
	netif_set_status_callback(&wifi_netif, wifi_status_callback);
#endif
	wifinet_up = 1;
	return 0;
}

int wifinetwork_down(void)
{
	libwifi_down();
	netif_remove(&wifi_netif);
	wifinet_up = 0;
	return 0;
}

int envsave_dhcpc_ip(const ip_addr_t *ipaddr,const ip_addr_t *netmask,const ip_addr_t *gateway,const ip_addr_t *dns)
{
	if(ipaddr && netmask && gateway && dns)
	{
		if((!g_network_cfg.wifi.dhcpc_ip_cfg.ipaddr || strcmp(g_network_cfg.wifi.dhcpc_ip_cfg.ipaddr,inet_ntoa(*ipaddr))) ||
			(!g_network_cfg.wifi.dhcpc_ip_cfg.netmask || strcmp(g_network_cfg.wifi.dhcpc_ip_cfg.netmask,inet_ntoa(*netmask)))||
			(!g_network_cfg.wifi.dhcpc_ip_cfg.gateway || strcmp(g_network_cfg.wifi.dhcpc_ip_cfg.gateway,inet_ntoa(*gateway))) ||
			(!g_network_cfg.wifi.dhcpc_ip_cfg.dnsserver || strcmp(g_network_cfg.wifi.dhcpc_ip_cfg.dnsserver,inet_ntoa(*dns))))
		{
			setenv("dhcpc_ipaddr",inet_ntoa(*ipaddr),1);
			setenv("dhcpc_netmask",inet_ntoa(*netmask),1);
			setenv("dhcpc_gateway",inet_ntoa(*gateway),1);
			setenv("dhcpc_dnsserver",inet_ntoa(*dns),1);
			debug_printf("save dhcpc ip to env\n");
			saveenv();
		}
	}
}
int envload_dhcpc_ip(struct netif * netif)
{
	ip_addr_t ipaddr, netmask, gateway, dns;
	g_network_cfg.wifi.dhcpc_ip_cfg.ipaddr = getenv("dhcpc_ipaddr");
	g_network_cfg.wifi.dhcpc_ip_cfg.netmask = getenv("dhcpc_netmask");
	g_network_cfg.wifi.dhcpc_ip_cfg.gateway = getenv("dhcpc_gateway");
	g_network_cfg.wifi.dhcpc_ip_cfg.dnsserver = getenv("dhcpc_dnsserver");

	if(g_network_cfg.wifi.dhcpc_ip_cfg.ipaddr &&
	   g_network_cfg.wifi.dhcpc_ip_cfg.netmask &&
	   g_network_cfg.wifi.dhcpc_ip_cfg.gateway &&
	   g_network_cfg.wifi.dhcpc_ip_cfg.dnsserver){
		if(inet_aton(g_network_cfg.wifi.dhcpc_ip_cfg.ipaddr, &ipaddr)  &&
				inet_aton(g_network_cfg.wifi.dhcpc_ip_cfg.netmask, &netmask)  &&
				inet_aton(g_network_cfg.wifi.dhcpc_ip_cfg.gateway, &gateway)  &&
				inet_aton(g_network_cfg.wifi.dhcpc_ip_cfg.dnsserver, &dns)){
			netif_set_addr(netif, &ipaddr, &netmask, &gateway);
			dns_setserver(0, &dns);
			netif_set_up(netif);
			debug_printf("load dhcpc ip for env\n");
			debug_printf("set %c%c%d: addr:%s",netif->name[0],netif->name[1],netif->num,
					  ip4addr_ntoa(&netif->ip_addr));
			  debug_printf(" Mask:%s", ip4addr_ntoa(&netif->netmask));
			  debug_printf(" gw:%s", ip4addr_ntoa(&netif->gw));
			  debug_printf(" dns:%s\n", ip4addr_ntoa(dns_getserver(0)));
		}
	}
}


static int wifinetwork_ipset(u8 ipmode, struct netif * netif)
{
	//start dhcp client ?
#ifdef CONFIG_NETWORK_DHCPC_EN
	if(ipmode == NETWORK_IPMODE_AUTO || ipmode == NETWORK_IPMODE_DHCPC){
		envload_dhcpc_ip(netif);
		dhcpc_set_cb(envsave_dhcpc_ip);
		dhcp_start(netif);
		return 0;
	}
#endif

#ifdef CONFIG_NETWORK_DHCPS_EN
	//start dhcp server ?
	if(ipmode == NETWORK_IPMODE_AUTO || ipmode == NETWORK_IPMODE_DHCPS){

		ip_addr_t ipaddr, netmask, gateway;
		ip_addr_set_zero( &ipaddr );
		ip_addr_set_zero( &netmask );
		ip_addr_set_zero( &gateway );

		g_network_cfg.wifi.dhcps_ip_cfg.ipaddr = getenv("dhcps_ipaddr");
		g_network_cfg.wifi.dhcps_ip_cfg.netmask = getenv("dhcps_netmask");
		g_network_cfg.wifi.dhcps_ip_cfg.gateway = getenv("dhcps_gateway");
		g_network_cfg.wifi.dhcps_ip_cfg.dnsserver = getenv("dhcps_dnsserver");
		if(g_network_cfg.wifi.dhcps_ip_cfg.ipaddr == NULL ||
		   g_network_cfg.wifi.dhcps_ip_cfg.netmask == NULL ||
		   g_network_cfg.wifi.dhcps_ip_cfg.gateway == NULL){
			g_network_cfg.wifi.dhcps_ip_cfg.ipaddr = "192.168.1.1";
			g_network_cfg.wifi.dhcps_ip_cfg.netmask = "255.255.255.0";
			g_network_cfg.wifi.dhcps_ip_cfg.gateway = "192.168.1.1";
			g_network_cfg.wifi.dhcps_ip_cfg.dnsserver = "192.168.1.1";
		}

		if(inet_aton(g_network_cfg.wifi.dhcps_ip_cfg.ipaddr, &ipaddr) != 1 ||
				inet_aton(g_network_cfg.wifi.dhcps_ip_cfg.netmask, &netmask) != 1 ||
				inet_aton(g_network_cfg.wifi.dhcps_ip_cfg.gateway, &gateway) != 1){
			syslog(LOG_ERR, "[ERROR]:  Manual Ipconfig Error!!\n");
			return -1;
		}

  		netif_set_addr(netif, &ipaddr, &netmask, &gateway);
  		netif_set_up(netif);

		syslog(LOG_INFO, "%c%c0 Manual Ipconfig Done!!\n"
			"IpAddr: %u.%u.%u.%u\n"
			"NetMask: %u.%u.%u.%u\n"
			"GateWay: %u.%u.%u.%u\n",
			netif->name[0], netif->name[1],
			(netif->ip_addr.addr & 0xff), ((netif->ip_addr.addr >> 8)& 0xff), ((netif->ip_addr.addr >> 16)& 0xff), ((netif->ip_addr.addr >> 24)& 0xff),
			(netif->netmask.addr & 0xff), ((netif->netmask.addr >> 8)& 0xff), ((netif->netmask.addr >> 16)& 0xff), ((netif->netmask.addr >> 24)& 0xff),
			(netif->gw.addr & 0xff), ((netif->gw.addr >> 8)& 0xff), ((netif->gw.addr >> 16)& 0xff), ((netif->gw.addr >> 24)& 0xff)
			);


		debug_printf("start dhcp server\n");

		struct ip_info info;
		info.ip = netif->ip_addr;
		info.netmask = netif->netmask;
		info.gw = netif->gw;
		dhcps_start(netif, &info);
		return 0;
	}
#endif

	if(ipmode == NETWORK_IPMODE_MANUAL){
		ip_addr_t ipaddr, netmask, gateway;
		ip_addr_set_zero( &ipaddr );
		ip_addr_set_zero( &netmask );
		ip_addr_set_zero( &gateway );

		t_network_ipmode_manual_cfg * manual_ip_cfg;
		if(netif->name[0] == 'w' && netif->name[1] == 'l'){
			manual_ip_cfg = &g_network_cfg.wifi.manual_ip_cfg;
		}else if(netif->name[1] == 'e' && netif->name[1] == 'n'){
			manual_ip_cfg = &g_network_cfg.eth.manual_ip_cfg;
		}else{
			syslog(LOG_ERR, "[ERROR]: netif error in network_ip_config().\n");
			return -1;
		}

		if(inet_aton(manual_ip_cfg->ipaddr, &ipaddr) != 1 ||
				inet_aton(manual_ip_cfg->netmask, &netmask) != 1 ||
				inet_aton(manual_ip_cfg->gateway, &gateway) != 1){
			syslog(LOG_ERR, "[ERROR]:  Manual Ipconfig Error!!\n");
			return -1;
		}

  		netif_set_addr(netif, &ipaddr, &netmask, &gateway);
  		netif_set_up(netif);

		syslog(LOG_INFO, "%c%c0 Manual Ipconfig Done!!\n"
			"IpAddr: %u.%u.%u.%u\n"
			"NetMask: %u.%u.%u.%u\n"
			"GateWay: %u.%u.%u.%u\n",
			netif->name[0], netif->name[1],
			(netif->ip_addr.addr & 0xff), ((netif->ip_addr.addr >> 8)& 0xff), ((netif->ip_addr.addr >> 16)& 0xff), ((netif->ip_addr.addr >> 24)& 0xff),
			(netif->netmask.addr & 0xff), ((netif->netmask.addr >> 8)& 0xff), ((netif->netmask.addr >> 16)& 0xff), ((netif->netmask.addr >> 24)& 0xff),
			(netif->gw.addr & 0xff), ((netif->gw.addr >> 8)& 0xff), ((netif->gw.addr >> 16)& 0xff), ((netif->gw.addr >> 24)& 0xff)
			);
		return 0;
	}

	syslog(LOG_ERR, "[ERROR] %c%c0 ipconfig error\n", netif->name[0], netif->name[1]);
	return -1;
}



int wifinetwork_connect(t_network_wifi_cfg* cfg, t_wifi_conf *conf)
{
	t_network_wifi_cfg *wificfg;
	u32 t1;
	int ret;
	if(cfg)
		wificfg = cfg;
	else
		wificfg= &g_wifi_cfg;
	//connect wifi
	u8 conn_retries = 0;
#define WIFI_MAX_RETRIES		3
	while(conn_retries < WIFI_MAX_RETRIES){
		t1 = ticks;
		if(wificfg->chanspec==NULL)
		{
			debug_printf("libwifi_connect\n");
			ret = libwifi_connect(wificfg->ssid, wificfg->key, wificfg->keylen, NULL, 0, conf);
		}
		else
		{
			debug_printf("libwifi_scanconnect\n");
			ret = libwifi_scanconnect(wificfg->ssid,wificfg->ssidlen,wificfg->bssid,
					(u8*)wificfg->key,wificfg->keylen,wificfg->keyindex,
					wificfg->pmk,wificfg->chanspec,
					&wificfg->scan_security, &wificfg->scan_encryption, 0, conf);
		}
		if(ret == 0){
			syslog(LOG_WARNING, "WIFI Connected!\n");
			debug_printf("connect spend %d ticks\n",ticks - t1);
			break;
		}else{
			if(wificfg->chanspec) *wificfg->chanspec=0;
			if(!wificfg->fix_bssid) memset(wificfg->bssid,0,sizeof(wificfg->bssid));
			syslog(LOG_WARNING, "WIFI Connect %dth Retry Fail.\n", conn_retries);
			conn_retries ++;
		}
	}

	if(conn_retries == WIFI_MAX_RETRIES){
		syslog(LOG_ERR, "[ERROR]: Wifi Connect Fail!!\n");
		return -1;
	}
	//start config ip address
	ret = wifinetwork_ipset(wificfg->ipmode, &wifi_netif);
	if(ret < 0){
		return ret;
	}
	return 0;
}

int wifinetwork_ap(t_network_wifi_cfg* cfg, t_wifi_conf *conf)
{
	t_network_wifi_cfg *wificfg;
	int ret;
	if(cfg)
		wificfg = cfg;
	else
		wificfg= &g_wifi_cfg;

	ret = libwifi_ap(wificfg->ap_ssid,
			wificfg->ap_key,wificfg->ap_keylen,
			wificfg->ap_encrypt, wificfg->ap_security,&wificfg->ap_channel, conf);
	if(ret == 0)
	{
		debug_printf("AP mode:init success \n");
	}
	else
	{
		debug_printf("AP mode:init failure \n");
		return -1;
	}

	//start config ip address
	ret = wifinetwork_ipset(wificfg->ipmode, &wifi_netif);
	if(ret < 0){
		return ret;
	}
	return 0;
}


