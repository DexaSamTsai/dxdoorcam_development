#include "includes.h"
#include "libnetkits.h"
#include "mcu_ctrl.h"
#ifdef CONFIG_LIBWPS_EN
int cmd_wifi_wps(int argc, char ** argv)
{

	//init wifi chip
#ifdef CONFIG_WIFIMODULE_INTERFACE_SCIF_02
	int scif_if = SCIF_MODULE_02;
#elif CONFIG_WIFIMODULE_INTERFACE_SCIF_03
	int scif_if = SCIF_MODULE_03;
#else
	int scif_if = SCIF_MODULE_01;
#endif
	extern int wifi_thread_pause(void);
	extern int wifi_thread_resume(void);
	wifi_thread_pause();
	libwifi_wps_start(scif_if, g_network_cfg.wifi.mac, 0, NULL);
	while(g_network_cfg.wifi.wps.is_done==0) sleep(1);
	if(g_network_cfg.wifi.wps.is_done==1)
	{
		debug_printf("wps ssid %s\n",g_network_cfg.wifi.wps.ssid);
		debug_printf("wps key %s\n",g_network_cfg.wifi.wps.key);
	}
	libwifi_wps_stop();
	wifi_thread_resume();
	return 0;
}
#endif

int cmd_wifi_connect(int argc, char ** argv)
{
	t_network_wifi_cfg cfg;
	t_wifi_conf conf;
	char tmpbuf[128];
	char *tmpstr;
	char *bssidstr;
	char *pmkstr;
	int security;
	int wepauthtype;
	u16 chanspec=0;
	u32 len;
	int reconnectflag=0;
	int autoreconnectflag=0;
	int oldapiflag = 0;
	int bgonly = 0;
	struct s_getopt opt;
	int ret=0;

	memset(&cfg,0,sizeof(cfg));
	memset(&conf,0,sizeof(conf));
	memset(&opt,0,sizeof(opt));

    while(1) {
        int c = getopt2(&opt, argc, argv, "as:k:b:hS:A:i:roBg");
        if(c == -1) break;
        switch(c) {
    		case 'B':
    			cfg.fix_bssid=1;
    			break;
    		case 'g':
    			bgonly = 1;
    			break;
        	case 'r':
        		reconnectflag=1;
        		break;
            case 's':
            	cfg.ssid = opt.optarg;
            	break;
            case 'k':
            	cfg.key = opt.optarg;
            	break;
            case 'b':
        		cfg.bssid[0]= strtol(opt.optarg,&tmpstr,16);
        		tmpstr++;
        		cfg.bssid[1]= strtol(tmpstr,&tmpstr,16);
        		tmpstr++;
        		cfg.bssid[2]= strtol(tmpstr,&tmpstr,16);
        		tmpstr++;
        		cfg.bssid[3]= strtol(tmpstr,&tmpstr,16);
        		tmpstr++;
        		cfg.bssid[4]= strtol(tmpstr,&tmpstr,16);
        		tmpstr++;
        		cfg.bssid[5]= strtol(tmpstr,&tmpstr,16);
            	break;
            case 'i':
            	cfg.keyindex = atoi(opt.optarg);
            	break;

            case 'S':
            	printf("S: %s\n",opt.optarg);
            	security = atoi((const char*)opt.optarg);
            	conf.security =  &security;
            	printf("security = %d\n",security);
				break;
            case 'A':
            	printf("A: %s\n",opt.optarg);
            	wepauthtype = atoi(opt.optarg);
            	conf.wepauthtype = &wepauthtype;
            	printf("wepauthtype = %d\n",wepauthtype);
            	break;
            case 'h':
            	debug_printf("connect usage:\n");
            	debug_printf("wifi connect [options] [ssid] [key]");
            	debug_printf("-r: disconnect and reconnect\n");
            	debug_printf("-s: set ssid\n");
            	debug_printf("-k: set passwd\n");
            	debug_printf("-i: set wep key index\n");
            	debug_printf("-b: set bssid\n");
            	debug_printf("-S: set security type\n");
            	debug_printf("\t0:none, 1:wep, 2: wpa/wpa2-psk\n");
            	debug_printf("-A: set wep auth type\n");
            	debug_printf("\t0:open system, 1: shared key\n");
            	debug_printf("-a: auto reconnect\n");
            	debug_printf("-o: old api libwifi_connect will be called\n");
            	debug_printf("-B: fixed bssid, only connect the fixed bssid ap.\n");
            	debug_printf("-g: set only bg mode.\n");
            	return 0;
            	break;
            case 'a':
            	autoreconnectflag = 1;
            	break;
            case 'o':
            	oldapiflag = 1;
            	break;
            default:
                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }





    if(argv[opt.optind] && !cfg.ssid) cfg.ssid = argv[opt.optind];
    if(argv[opt.optind+1] && !cfg.key) cfg.key = argv[opt.optind+1];


	cfg.wifi_mode = WIFI_MODE_STATION;

	if(!cfg.ssid) cfg.ssid = getenv("wifi_ssid");

	cfg.ssidlen = cfg.ssid?strlen(cfg.ssid):0;
	debug_printf("ssid: %s, len %d\n",cfg.ssid,cfg.ssidlen);

	if(!cfg.key) cfg.key = getenv("wifi_key");
	cfg.keylen = cfg.key?strlen(cfg.key):0;

	if(!cfg.keyindex)
		cfg.keyindex = getenv("wifi_key_index")?atoi(getenv("wifi_key_index")):0;
	debug_printf("key: %s, len %d, index %d\n",cfg.key?cfg.key:"[NULL]",cfg.keylen,cfg.keyindex);


	if(!(cfg.bssid[0]||cfg.bssid[1]||cfg.bssid[2]||cfg.bssid[3]||cfg.bssid[4]||cfg.bssid[5]))
	{
		bssidstr = getenv("wifi_bssid");
		if(bssidstr)
		{
			cfg.bssid[0]= strtol(bssidstr,&tmpstr,16);
			tmpstr++;
			cfg.bssid[1]= strtol(tmpstr,&tmpstr,16);
			tmpstr++;
			cfg.bssid[2]= strtol(tmpstr,&tmpstr,16);
			tmpstr++;
			cfg.bssid[3]= strtol(tmpstr,&tmpstr,16);
			tmpstr++;
			cfg.bssid[4]= strtol(tmpstr,&tmpstr,16);
			tmpstr++;
			cfg.bssid[5]= strtol(tmpstr,&tmpstr,16);
		}
	}
	debug_printf("bssid:\n");
	dump(cfg.bssid,sizeof(cfg.bssid));

	if(!conf.security){
		if(getenv("wifi_security")){
			security = atoi(getenv("wifi_security"));
			conf.security = &security;
		}
	}

	if(!conf.wepauthtype){
		if(getenv("wifi_wepauthtype"))
		{
			wepauthtype = atoi(getenv("wifi_wepauthtype"));
			conf.wepauthtype = &wepauthtype;
		}
	}

	debug_printf("security: %d, wepauthtype: %d\n",conf.security?*conf.security:-1,conf.wepauthtype?*conf.wepauthtype:-1);



	//if wifi_chanspec is defined, chanspec is no NULL, it will choose wifi_scanconnect
	//if wifi_chanspec is undefined, chanspec will be NULL, it will choose old connect api.
	//if chanspec is set 0, it will call wifi_scanconnect, and do scan firstly.

	if(getenv("wifi_chanspec"))
	{
		chanspec = atoi(getenv("wifi_chanspec"));
	}else chanspec = 0;
	if(!oldapiflag)
	{
		cfg.chanspec = &chanspec;
	}
	if(!cfg.chanspec)
		debug_printf("chanspec is NULL\n");
	else
		debug_printf("chanspec %d\n",*cfg.chanspec);

	memset(cfg.pmk,0,sizeof(cfg.pmk));
	pmkstr = getenv("wifi_pmk");
	if(pmkstr){
		len = sizeof(cfg.pmk);
		Base64_Decode(cfg.pmk,&len, (unsigned char *)pmkstr,strlen(pmkstr));
	}
	dump(cfg.pmk,sizeof(cfg.pmk));

	cfg.scan_security = getenv("wifi_scan_security")?strtol(getenv("wifi_scan_security"),NULL,0):0;
	cfg.scan_encryption = getenv("wifi_scan_encryption")?strtol(getenv("wifi_scan_encryption"),NULL,0):0;
	debug_printf("scan_security: %d, scan_encryption: %d\n",cfg.scan_security, cfg.scan_encryption);

    g_network_cfg.wifi.netif_link_callback = NULL;

	if(reconnectflag)
		wifiadapter_down();
	if(bgonly)
	{
		libwifi_iovar_getint("nmode",&bgonly);
		if(bgonly)
		{
			wifiadapter_down();
			libwifi_iovar_setint("nmode",0);
		}
	}
	if(wifinetwork_connect(&cfg,&conf) == 0)
	{
		if(cfg.chanspec){
			if(cfg.ssid)
				setenv("wifi_ssid",cfg.ssid,1);
			if(cfg.key)
				setenv("wifi_key",cfg.key,1);

			debug_printf("chanspec 0x%x\n",*cfg.chanspec);
			sprintf(tmpbuf,"%d",*cfg.chanspec);
			setenv("wifi_chanspec",tmpbuf,1);

			debug_printf("pmk:\n");
			dump(cfg.pmk,sizeof(cfg.pmk));
			len  = sizeof(tmpbuf);
			Base64_Encode((unsigned char *)tmpbuf,&len,cfg.pmk,sizeof(cfg.pmk));
			setenv("wifi_pmk",tmpbuf,1);
			debug_printf("pmkstr:%s, len %d\n",tmpbuf,len);

			sprintf(tmpbuf,"%x:%x:%x:%x:%x:%x",cfg.bssid[0],cfg.bssid[1],cfg.bssid[2],cfg.bssid[3],cfg.bssid[4],cfg.bssid[5]);
			setenv("wifi_bssid",tmpbuf,1);

			debug_printf("scan_security: %d, scan_encryption: %d\n",cfg.scan_security, cfg.scan_encryption);
			sprintf(tmpbuf,"%d",cfg.scan_security);
			setenv("wifi_scan_security",tmpbuf,1);
			sprintf(tmpbuf,"%d",cfg.scan_encryption);
			setenv("wifi_scan_encryption",tmpbuf,1);
			saveenv();
		}
		ret = 0;
	}else
	{
		ret =  -1;
	}

exit:
	if(autoreconnectflag) g_network_cfg.wifi.netif_link_callback = wifinetwork_link_callback;
	return ret;
}

int cmd_wifi_nrate_set(int argc, char ** argv)
{
	t_wifi_nrate_set cfg;
	int autoflag = 0;
	int readflag = 1;
	struct s_getopt opt;
	int ret=0;
	memset(&cfg,0,sizeof(cfg));
	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "r:m:s:wba");
        if(c == -1) break;
        switch(c) {
    		case 'r':
    			cfg.legacy_set_val = opt.optarg;
    			readflag = 0;
    			break;
        	case 'm':
        		cfg.mcs_set_val = opt.optarg;
        		readflag = 0;
        		break;
            case 's':
            	cfg.stf_set_val = opt.optarg;
            	readflag = 0;
            	break;
            case 'w':
            	cfg.mcs_only = 1;
            	readflag = 0;
            	break;
            case 'a':
            	autoflag = 1;
            	readflag = 0;
            	break;
            default:
                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }
    if(readflag) return libwifi_get_nrate(NULL);
    if(autoflag) memset(&cfg,0,sizeof(cfg));
	return libwifi_set_nrate(&cfg);
}
#if 0
int cmd_wifi_ap(int argc, char ** argv)
{
	int i;
	t_network_wifi_cfg cfg ;
	t_wifi_conf conf ;
	char *wifi_encrypt_name[]={"OPEN","WEP64","WEP128","WPA","WPA2",NULL};
	char *wifi_security_name[]={"NONE","AES","TKIP",NULL};
	char *tmpstr;
	char ssidbuf[33];

	memset(&cfg,0,sizeof(cfg));
	memset(&conf,0,sizeof(conf));

	cfg.wifi_mode = WIFI_MODE_AP;
	cfg.ipmode = NETWORK_IPMODE_DHCPS;

	if(argv[1]) cfg.ap_ssid = argv[1];
	else cfg.ap_ssid = getenv("wifi_ap_ssid");
	if(!cfg.ap_ssid)
	{
		sprintf(ssidbuf,"AP_%x%x%x",g_network_cfg.wifi.mac[3],g_network_cfg.wifi.mac[4],g_network_cfg.wifi.mac[5]);
		cfg.ap_ssid = ssidbuf;
	}

	if(argv[2])	cfg.ap_key = argv[2];
	else cfg.ap_key = getenv("wifi_ap_key");

	cfg.ap_keylen = cfg.ap_key?strlen(cfg.ap_key):0;

	if(argv[3]) tmpstr = argv[3];
	else tmpstr = getenv("wifi_ap_encrypt");
	cfg.ap_encrypt = 0;
	if(tmpstr){
		for(i=0;wifi_encrypt_name[i];i++)
			if(!strcasecmp(tmpstr,wifi_encrypt_name[i]))
			{
				cfg.ap_encrypt = i;
				break;
			}
	}

	if(argv[4]) tmpstr = argv[4];
	else tmpstr = getenv("wifi_ap_security");
	cfg.ap_security = 0;
	if(tmpstr){
		for(i=0;wifi_security_name[i];i++)
			if(!strcasecmp(tmpstr,wifi_security_name[i]))
			{
				cfg.ap_security = i;
				break;
			}
	}

	if(argv[5]) tmpstr = argv[5];
	else tmpstr = getenv("wifi_ap_channel");
	if(tmpstr)	cfg.ap_channel = strtol(tmpstr,NULL,0);
	else cfg.ap_channel = 0;

	debug_printf("AP: %s, key %s, channel %d, security: %s %s\n",cfg.ap_ssid, cfg.ap_key?cfg.ap_key:"NULL",
			cfg.ap_channel, wifi_encrypt_name[cfg.ap_encrypt], wifi_security_name[cfg.ap_security]);

	if(wifinetwork_ap(&cfg,&conf) == 0)
	{
		debug_printf("ap success, channel %d\n",cfg.ap_channel);

	}else{
		debug_printf("ap failed\n");
		return -1;
	}
	return 0;
}
#else
int cmd_wifi_ap(int argc, char ** argv)
{
	int i;
	t_network_wifi_cfg cfg ;
	t_wifi_conf conf ;
	char *wifi_encrypt_name[]={"OPEN","WEP64","WEP128","WPA","WPA2",NULL};
	char *wifi_security_name[]={"NONE","AES","TKIP",NULL};
	char *ap_encrypt=NULL;
	char *ap_security=NULL;
	char *ap_channel=NULL;
	char *vndr_oui=NULL;
	char *vndr_data=NULL;

	char *tmpstr;
	char ssidbuf[33];

	struct s_getopt opt;
	int ret=0;

	memset(&cfg,0,sizeof(cfg));
	memset(&conf,0,sizeof(conf));
	memset(&opt,0,sizeof(opt));

	while(1) {
	    int c = getopt2(&opt, argc, argv, "S:e:s:k:hc:O:D");
	    if(c == -1) break;
	    switch(c) {
			case 'S':
				ap_security = opt.optarg;
				break;
	    	case 'e':
	    		ap_encrypt=opt.optarg;
	    		break;
	        case 's':
	        	cfg.ap_ssid = opt.optarg;
	        	break;
	        case 'k':
	        	cfg.ap_key = opt.optarg;
	        	break;
	        case 'c':
	        	ap_channel = opt.optarg;
	        	break;
	        case 'O':
	        	conf.vndr_oui = opt.optarg;
	        	break;
	        case 'D':
	        	conf.vndr_data = opt.optarg;
	        	break;
	        case 'h':
	        	debug_printf("connect usage:\n");
	        	debug_printf("wifi ap [options] [ssid] [key]");
	        	debug_printf("-s: set ssid\n");
	        	debug_printf("-k: set passwd\n");
	        	debug_printf("-c: set channel\n");
	        	debug_printf("-S: set security type\n");
	        	debug_printf("\t0:none, 1:AES, 2: TKIP\n");
	        	debug_printf("-e: set encrypt type\n");
	        	debug_printf("\t0:OPEN, 1: WEP64, 2: WEP128, 3: WPA, 4: WPA2\n");
	        	debug_printf("-O: vndr_oui\n");
	        	debug_printf("-D: vndr_data\n");
	        	return 0;
	        	break;
	        default:
	            debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
	        	break;
	    }
	}





	if(argv[opt.optind] && !cfg.ap_ssid) cfg.ap_ssid = argv[opt.optind];
	if(argv[opt.optind+1] && !cfg.ap_key) cfg.ap_key = argv[opt.optind+1];

	cfg.wifi_mode = WIFI_MODE_AP;
	cfg.ipmode = NETWORK_IPMODE_DHCPS;

	if(! cfg.ap_ssid) cfg.ap_ssid = getenv("wifi_ap_ssid");
	if(!cfg.ap_ssid)
	{
		sprintf(ssidbuf,"AP_%x%x%x",g_network_cfg.wifi.mac[3],g_network_cfg.wifi.mac[4],g_network_cfg.wifi.mac[5]);
		cfg.ap_ssid = ssidbuf;
	}

	if(!cfg.ap_key)	cfg.ap_key = getenv("wifi_ap_key");

	cfg.ap_keylen = cfg.ap_key?strlen(cfg.ap_key):0;

	if(!ap_encrypt) ap_encrypt = getenv("wifi_ap_encrypt");
	cfg.ap_encrypt = 0;
	if(ap_encrypt){
		for(i=0;wifi_encrypt_name[i];i++)
			if(!strcasecmp(ap_encrypt,wifi_encrypt_name[i]))
			{
				cfg.ap_encrypt = i;
				break;
			}
	}

	if(!ap_security) ap_security = getenv("wifi_ap_security");
	cfg.ap_security = 0;
	if(ap_security){
		for(i=0;wifi_security_name[i];i++)
			if(!strcasecmp(ap_security,wifi_security_name[i]))
			{
				cfg.ap_security = i;
				break;
			}
	}

	if(!ap_channel) ap_channel = getenv("wifi_ap_channel");
	if(ap_channel)	cfg.ap_channel = strtol(ap_channel,NULL,0);
	else cfg.ap_channel = 0;

	debug_printf("AP: %s, key %s, channel %d, security: %s %s \n",cfg.ap_ssid, cfg.ap_key?cfg.ap_key:"NULL",
			cfg.ap_channel, wifi_encrypt_name[cfg.ap_encrypt], wifi_security_name[cfg.ap_security]);

	if(wifinetwork_ap(&cfg,&conf) == 0)
	{
		debug_printf("ap success, channel %d\n",cfg.ap_channel);

	}else{
		debug_printf("ap failed\n");
		return -1;
	}
	return 0;
}

#endif

int cmd_wifi_sleep(int argc, char ** argv)
{
	int i;
	int pir=15;
	struct s_getopt opt;
	int ret=0;
	memset(&opt,0,sizeof(opt));

	while(1) {
	    int c = getopt2(&opt, argc, argv, "p:h");
	    if(c == -1) break;
	    switch(c) {
			case 'p':
				pir = atoi(opt.optarg);
				break;
	        case 'h':
	        	debug_printf("connect usage:\n");
	        	debug_printf("wifi sleep [options]\n");
	        	debug_printf("-p: set pir\n");
	        	debug_printf("-h: help\n");

	        	return 0;
	        	break;
	        default:
	            debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
	        	break;
	    }
	}

	struct netif * netif = netif_find("wl0");
	u32 ipaddr = netif->ip_addr.addr;
	libwifi_set_autoarp_sleep(&ipaddr,0);
	mcu_set_pirsensitive(pir);
	mcu_set_mode(MODE_STANDBY);
	mcu_set_led(LED_COLOR_RED, LED_STATUS_FAST);
	mcu_power_off();
	return 0;
}

extern int cmd_heartbeat(int argc, char ** argv);
int cmd_wifi(int argc, char ** argv)
{
	int i,ret;
	u32 val,val1;
	if (argc < 2) return 0;
#ifdef CONFIG_LIBWPS_EN
	if(!strcmp(argv[1],"wps"))
	{
		cmd_wifi_wps(argc-1,&argv[1]);
	}else 
#endif
	if(!strcmp(argv[1],"init"))
	{
		debug_printf("wifinetwork_init\n");
		wifinetwork_init();
		debug_printf("wifiadapter_init\n");
		wifiadapter_init();
	}else if(!strcmp(argv[1],"up"))
	{
		debug_printf("wifinetwork_up\n");
		wifinetwork_up();
	}else if(!strcmp(argv[1],"filter"))
	{
		char filter[1024];
		ret = create_filter(argc-1, &argv[1],NULL,filter,sizeof(filter));
		if(ret)
			libwifi_set_filter_by_user(filter, 1);
	}
#ifdef CONFIG_MCU_EN 
	else if(!strcmp(argv[1],"sleep"))
	{
		cmd_wifi_sleep(argc-1, &argv[1]);
	}
	else if(!strcmp(argv[1],"bsleep"))
	{
		cmd_exec(cmd_heartbeat,"-D -d1",NULL);
		cmd_exec(cmd_heartbeat,"-a -d2 -i60 255.255.255.255",NULL);
		cmd_exec(cmd_heartbeat,"-u -d3 -i25 10.6.15.1 5000 -p 1234",NULL);
		cmd_exec(cmd_wifi, "filter -m -i100",NULL);
		cmd_exec(cmd_wifi, "filter -p 80 -i101",NULL);
		cmd_exec(cmd_wifi_sleep,"sleep -p 15",NULL);
	}
#endif
	else if(!strcmp(argv[1],"down"))
	{
		wifinetwork_down();
	}else if(!strcmp(argv[1],"ap"))
	{
		cmd_wifi_ap(argc-1, &argv[1]);
	}else if(!strcmp(argv[1],"vendor"))
	{
		debug_printf("%s, %s, %s, %s\n",argv[1],argv[2],argv[3],argv[4]);
		if(!strcmp(argv[2],"add") && argv[3] && argv[4])
		{
			libwifi_add_custom_vendorIE(argv[3],argv[4]);
		}else if(!strcmp(argv[2],"del") && argv[3] && argv[4])
		{
			libwifi_del_custom_vendorIE(argv[3],argv[4]);
		}
	}
	else if(!strcmp(argv[1],"scan"))
	{
		static ap_info_t  ap_list[30]={0};
		static ap_info_num_t cnt = {0};
		cnt.max_cnt=30;
		libwifi_scan(ap_list,&cnt);
		debug_printf("ap num %d\n",cnt.ap_cnt);
		for( i = 0; i< cnt.ap_cnt; i++)
		{
			debug_printf("[%d] %s\t %x-%x-%x-%x-%x-%x\t %d\t %d\t, %s,%s,%s, %s \n", i,
							ap_list[i].ssid,ap_list[i].bssid[0],ap_list[i].bssid[1],ap_list[i].bssid[2],
							ap_list[i].bssid[3],ap_list[i].bssid[4],ap_list[i].bssid[5],ap_list[i].channel,ap_list[i].rssi,
							BSS_TYPE_STR[ap_list[i].bss_type],SCAN_SECURITY_STR[ap_list[i].scan_security],
							SCAN_ENCRYPTION_STR[ap_list[i].scan_encryption],WPS_STATUS_STR[ap_list[i].wps]);
		}
	}else if(!strcmp(argv[1],"set"))
	{
		if(argv[2] && argv[3])
		{
			val = atoi(argv[3]);
			debug_printf("set argv[2]=%d\n",val);
			libwifi_iovar_setint(argv[2],val);
		}
	}
	else if(!strcmp(argv[1],"get"))
	{
		if(argv[2])
		{
			libwifi_iovar_getint(argv[2],&val);
			debug_printf("argv[2]=%d\n",val);
		}
	}else if(!strcmp(argv[1],"iset"))
	{
		if(argv[2] && argv[3])
		{
			val = atoi(argv[3]);
			val1 = atoi(argv[2]);
			debug_printf("set ioctl cmd %d=%d\n",val1, val);
			libwifi_ioctl_setint(val1,val);
		}
	}
	else if(!strcmp(argv[1],"iget"))
	{
		if(argv[2])
		{
			val1 = atoi(argv[2]);
			libwifi_ioctl_getint(val1,&val);
			debug_printf("get ioctl cmd %d=%d\n",val1, val);
		}
	}
	else if(!strcmp(argv[1],"iovarlist"))
	{
		debug_printf("2g_rate  set fixed rate for 802.11bg, val range: 36 24 ...\n");
		debug_printf("assoc_listen set  listen interval\n");
		debug_printf("nmdoe enable or disable 802.11n mode\n");
	}
	else if(!strcmp(argv[1],"pm"))
	{
		if(argv[2])
		{
			val = atoi(argv[2]);
			libwifi_set_pm(val);
		}else{
			libwifi_get_pm(&val);
			debug_printf("pm = %d\n",val);
		}
	}else if(!strcmp(argv[1],"country"))
	{
		char * country[4];
		if(argv[2])
		{
			if(!strcmp(argv[2],"list"))
				libwifi_set_country(NULL);
			else
				libwifi_set_country(argv[2]);
		}else
		{
			libwifi_get_country(country);
			debug_printf("country code: %s\n",country);
		}

	}else if(!strcmp(argv[1],"nrate"))
	{
		cmd_wifi_nrate_set(argc-1,&argv[1]);
	}
	else if(!strcmp(argv[1],"test"))
	{
		libwifi_test();
	}
	else if(!strcmp(argv[1],"connect"))
	{
		cmd_wifi_connect(argc-1, &argv[1]);
	}else if(!strcmp(argv[1],"apstatus"))
	{
		wlan_bss_info_t info, *infop ;
		libwifi_get_apstatus(&info, sizeof(info));
		infop=malloc(info.length);
		libwifi_get_apstatus(infop, info.length);
		dump(((u8*)infop)+infop->ie_offset, infop->ie_length);
		free(infop);
	}
	else{
		debug_printf("usage:\n");
		debug_printf("init\n");
		debug_printf("up\n");
		debug_printf("down\n");
		debug_printf("connect [ssid] [key]\n");
		debug_printf("ap [ssid] [key] -e [OPEN,WEP64,WEP128,WPA,WPA2] -S [NONE,AES,TKIP] -c [channel]\n");
		debug_printf("scan\n");
		debug_printf("country [list] [country code]\n");
		debug_printf("nrate -r [val] -m [val] -s [val] -w -b\n");
		debug_printf("pm [val]\n");
		debug_printf("iovarlist get support iovar cmd\n");
		debug_printf("filter set filter\n");

	}

	return 0;
}



