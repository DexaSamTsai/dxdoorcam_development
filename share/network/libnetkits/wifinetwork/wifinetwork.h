/*
 * wifinetwork.h
 *
 *  Created on: 2016��10��13��
 *      Author: Cox.Yang
 */

#ifndef _WIFINETWORK_H_
#define _WIFINETWORK_H_

int wifinetwork_init(void); // init tcpip stack.
int wifiadapter_init(void); // init wifi adapter.
int wifiadapter_up(void); // linkup wifi adapter
int wifiadapter_down(void); // linkdown wifi adapter
int wifinetwork_up(void); //  linkup wifi adapter and add wifi adapter to tcpip stack.
int wifinetwork_down(void); // linkdown wifi adapter and remove wifi adapter from tcpip stack.
int wifinetwork_connect(t_network_wifi_cfg* cfg, t_wifi_conf *conf); //connect ap
int wifinetwork_ap(t_network_wifi_cfg* cfg, t_wifi_conf *conf);//set ap.
int wifinetwork_link_callback(int evt);
int cmd_wifi(int argc, char ** argv);
#endif /* _WIFINETWORK_H_ */
