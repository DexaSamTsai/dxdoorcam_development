#ifndef _LIBNETKITS_MCU_CMD_H_
#define _LIBNETKITS_MCU_CMD_H_
#include "includes.h"
#include "mcu_ctrl.h"
t_mcu_evt msp_boot_evt(void);
int cmd_msp(int argc, char ** argv);
#endif
