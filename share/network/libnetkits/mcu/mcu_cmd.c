#include "includes.h"
#include "mcu_cmd.h"

static char *event_name[] = {
	{"EVT_NULL"},
	{"EVT_WIFI"},
	{"EVT_WPS_CLICK"},	 		/* short click */
	{"EVT_PIR"},
	{"EVT_DAY_MODE"},
	{"EVT_NIGHT_MODE"},
	{"EVT_WPS_LONGPRESS"},
	{"EVT_WPS_LONGRELEASE"},
	{"EVT_KEY2_PRESSED"}, 	 	/* short click */
	{"EVT_WPS_DOUBLECLICK"},  	/* double click */
	{"EVT_TIMER_EXPIRED"},
};

static t_mcu_evt _boot_evt=0;
static int _boot_evt_got = 0;
// Get the event which wake up the system.
t_mcu_evt msp_boot_evt(void)
{
	if(_boot_evt_got == 1){
		return _boot_evt;
	}
	// PIN_MCU_EVENT HIGH before GET_EVENT indicates a PIR event.
	_boot_evt_got = 1;
	if(mcu_event_ready() == 1){
		_boot_evt = EVT_PIR;
	}else{
		_boot_evt = mcu_get_event();
	}
	if(_boot_evt)
		syslog(LOG_NOTICE, "%s WAKEUP SYSTEM!!\n", event_name[_boot_evt]);
	return _boot_evt;
}
// Get the current event.
t_mcu_evt msp_get_evt(void)
{
	t_mcu_evt evt;
	evt = mcu_get_event();
	if(!_boot_evt_got) {
		_boot_evt_got = 1;
		_boot_evt = evt;
	}
	syslog(LOG_NOTICE, "%s RECEIVED!!\n", event_name[evt]);
//	mcu_get_mode();
//	mcu_get_event();
	return evt;
}




int cmd_msp(int argc, char ** argv)
{
	int i,j;
	if (argc < 2){
		debug_printf("usage:\n");
		debug_printf("msp init\n");
		debug_printf("msp setmode [mode]\n");
		debug_printf("msp getmode\n");
		debug_printf("msp setled [color] [status]\n");
		debug_printf("msp setpirst [0~15]; 15: disable PIR\n");
		debug_printf("msp getpir\n");
		debug_printf("msp getlum\n");
		debug_printf("msp setnmode [mode];0:force day, 1:force night, 2~f:threshold, 2:more day, f:more night\n");
		debug_printf("msp setbound [bound]\n");
		debug_printf("msp shortsleep [seconds]\n");
		debug_printf("msp getvbat\n");
		debug_printf("msp bootevent\n");
		debug_printf("msp getevent\n");
		debug_printf("msp pirgpio\n");
		debug_printf("msp poweroff\n");
	}else if(!strcmp(argv[1],"init"))
	{
		debug_printf("init mcu\n");
		mcu_init();
	}else if(!strcmp(argv[1],"setmode") && argv[2])
	{
		i = strtol(argv[2],NULL,0);
		debug_printf("mcu set mode %d\n",i);
		mcu_set_mode(i);
	}else if(!strcmp(argv[1],"getmode"))
	{
		i=mcu_get_mode();
		debug_printf("mcu get mode %d\n",i);
	}else if(!strcmp(argv[1],"setled") && (argc==4))
	{
		i = strtol(argv[2],NULL,0);
		j = strtol(argv[3],NULL,0);
		debug_printf("mcu set mode %d\n",i);
		mcu_set_led(i,j);
	}else if(!strcmp(argv[1],"setpirst") && argv[2])
	{
		i = strtol(argv[2],NULL,0);
		debug_printf("mcu set pirsensitive %d\n",i);
		mcu_set_pirsensitive(i);
	}else if(!strcmp(argv[1],"getpir"))
	{
		debug_printf("mcu get pirsensitive %d\n",mcu_get_pir());
	}else if(!strcmp(argv[1],"getlum"))
	{
		debug_printf("mcu get lum %d\n",mcu_get_lum());
	}else if(!strcmp(argv[1],"setnmode") && argv[2])
	{
		i = strtol(argv[2],NULL,0);
		mcu_set_nightmode(i);
	}else if(!strcmp(argv[1],"setbound") && argv[2])
	{
		i = strtol(argv[2],NULL,0);
		debug_printf("mcu set bound %d\n",i);
		mcu_set_boundary(i);
	}else if(!strcmp(argv[1],"shortsleep") && argv[2])
	{
		i = strtol(argv[2],NULL,0);
		debug_printf("mcp short sleep %dseconds\n",i);
		mcu_short_sleep(i);
	}else if(!strcmp(argv[1],"getvbat"))
	{
		i=mcu_get_vbat();
		debug_printf("mcu get vbat %d\n",i);
	}else if(!strcmp(argv[1],"bootevent"))
	{
		i=msp_boot_evt();
		debug_printf("mcu get boot event %d\n",i);
	}else if(!strcmp(argv[1],"getevent"))
	{
		i=msp_get_evt();
		debug_printf("mcu get current event %d\n",i);
	}else if(!strcmp(argv[1],"pirgpio"))
	{
		debug_printf("mcu get pir gpio %d\n",mcu_event_ready());
	}else if(!strcmp(argv[1],"poweroff"))
	{
		debug_printf("mcu power off\n");
		mcu_power_off();
	}else
		debug_printf("msp error cmd\n");

	return 0;
}




