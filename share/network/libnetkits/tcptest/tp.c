#include "includes.h"
#include "tp.h"
#include "libnetkits.h"


struct s_tp_thread_arg{
	int argc;
	char **argv;
} ;
ERTOS_EVENT_DECLARE(tp_evt_handler)


static int tp_senddata(tp_info_t *tpinfo)
{
	int ret;
//	u32 speed = tpinfo->ratetype=='M'?tpinfo->clientspeed*1024*1024/8:tpinfo->clientspeed*1024/8;
	tp_head_t *sptr=(tp_head_t *)tpinfo->tcp_appdata;
	sptr->magic = htonl(THROUGHPUT_MAGIC);
	sptr->type = TYPE_DATA;
	sptr->sseq = htonl(tpinfo->cur_sseq);
	tpinfo->cur_sseq++;
//	memcpy(&tpinfo->tcp_appdata[sizeof(tp_head_t)],tp_senddata,tpinfo->plen -sizeof(tp_head_t));
	memset(&tpinfo->tcp_appdata[sizeof(tp_head_t)],0xaa,tpinfo->plen -sizeof(tp_head_t));


	ret = send(tpinfo->socketfd,tpinfo->tcp_appdata,tpinfo->plen,0);
	if(ret!=tpinfo->plen)
		debug_printf("send return %d\n",ret);
	return ret;
}
static int tp_sendstartcmd(tp_info_t *tpinfo)
{
	int ret;
	tp_head_t *sptr=(tp_head_t *)tpinfo->tcp_appdata;
	printf("send start cmd\n");
	sptr->sratetype = tpinfo->ratetype;
	sptr->magic = htonl(THROUGHPUT_MAGIC);
	sptr->type = TYPE_SEND;
	sptr->sseq = htonl(tpinfo->cur_sseq);
	sptr->slen = htonl(tpinfo->plen);
	sptr->ltime = htonl(1);
	sptr->srate = htonl(tpinfo->serverspeed);
	sptr->stime = htonl(999999);
	sptr->tcpwindow = htons(16);



	tpinfo->cur_sseq++;
	ret = send(tpinfo->socketfd, tpinfo->tcp_appdata,tpinfo->plen, 0);
	return ret;
}



static int exit_flag = 0;

void * tp_thread(void * arg)
{

	struct s_tp_thread_arg *o_arg = (struct s_tcptest_thread_arg*)arg;
	int argc = o_arg->argc;
	char **argv = o_arg->argv;
	int ret;
	struct sockaddr_in remote_address;
	int addrlen;
	int timeout=50000;
	int sleepus = 0;
    struct timeval tcptimeout;
    tp_info_t tpinfo;
    tp_head_t *rptr=(tp_head_t *)tpinfo.tcp_appdata;
	struct s_getopt opt;
	memset(&tpinfo,0,sizeof(tpinfo));
	tpinfo.plen = 1460;
	tpinfo.ratetype = 'M';
	tpinfo.clientspeed = 30;

	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "t:huc:s:p:l:ki:");
        if(c == -1) break;
        switch(c) {
        	case 'u':
        		tpinfo.udpmode =1;
        		break;
        	case 'l':
        		tpinfo.plen = atoi(opt.optarg);
        		break;
        	case 'c':
        		tpinfo.clientspeed = atoi(opt.optarg);
        		break;
        	case 's':
        		tpinfo.serverspeed = atoi(opt.optarg);
        		break;
        	case 'k':
        		tpinfo.ratetype = 'K';
        		break;
        	case 'i':
        		sleepus = atoi(opt.optarg);
        		break;
            case 'h':
            default:
            	debug_printf("tp usage:\n");
            	debug_printf("tp [options] [ip] [port]");
            	debug_printf("-u: udp\n");
            	debug_printf("-t: target ip\n");
            	debug_printf("-p: target port\n");
            	debug_printf("-c: client speed\n");
            	debug_printf("-s: server speed\n");
            	debug_printf("-l: package len\n");
            	debug_printf("-k: set speed unit to k\n");
            	debug_printf("-i: sleep time us\n");
            	debug_printf("-h: help\n");
            	ertos_event_signal(tp_evt_handler);
            	return NULL;
            	break;
        }
    }


	if(!argv[opt.optind] || !argv[opt.optind+1])
	{
		printf("parameters error!\n");
		ertos_event_signal(tp_evt_handler);
		return NULL;
	}

	remote_address.sin_family = AF_INET;
	if(argv[opt.optind])
		remote_address.sin_addr.s_addr = inet_addr(argv[opt.optind]);
	if(argv[opt.optind+1])
		remote_address.sin_port = htons(atoi(argv[opt.optind+1]));

	printf("%s connect to %s:%s\n",tpinfo.udpmode?"UDP":"TCP",argv[opt.optind],argv[opt.optind+1]);


	if(tpinfo.udpmode)
		tpinfo.socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	else
		tpinfo.socketfd = socket(AF_INET, SOCK_STREAM, 0);

	ret = connect(tpinfo.socketfd, (struct sockaddr *) &remote_address, sizeof(remote_address));
	if (ret == -1) {
		printf("failed\n");
		return NULL;
	}else
		printf("success, fd %d\n",tpinfo.socketfd);

    ertos_event_signal(tp_evt_handler);

    while(1)
    {
    	if(exit_flag) break;
    	tcptimeout.tv_sec=0;
    	tcptimeout.tv_usec=timeout;
    	FD_ZERO((fd_set *)&tpinfo.tcpreadfds);
    	FD_SET(tpinfo.socketfd, &tpinfo.tcpreadfds);
    	ret = select(tpinfo.socketfd+1, &tpinfo.tcpreadfds, NULL, NULL, &tcptimeout);
    	if(ret<0)
    	{
    		debug_printf("error\n");
    		break;
    	}else if(ret>0)
    	{
    		if(FD_ISSET(tpinfo.socketfd, &tpinfo.tcpreadfds))
    		{
    			addrlen=sizeof(remote_address);
    			ret = recvfrom(tpinfo.socketfd,tpinfo.tcp_appdata,sizeof(tpinfo.tcp_appdata),0,(struct sockaddr *)&remote_address,(socklen_t *)&addrlen);
    			debug_printf("recvfrom len %d , frome %s:%d\n",ret, inet_ntoa(remote_address.sin_addr.s_addr),ntohs(remote_address.sin_port));
    			if(ret<=0) break;

				if(rptr->magic == ntohl(THROUGHPUT_MAGIC))
				{
					if(rptr->type == TYPE_RESP)
					{
						printf("get response\n");
						tpinfo.respflag = 1;
						if(tpinfo.clientspeed !=0 ) timeout=0;
					}
				}
    		}
    	}else
    	{
			if(tpinfo.respflag == 0)
			{
				if(ticks - tpinfo.ticks > 100)
				{
					tp_sendstartcmd(&tpinfo);
					tpinfo.ticks = ticks;
				}
			}else {
				if(sleepus)
					usleep(sleepus);
				ret = tp_senddata(&tpinfo);
				if(ret<0)
					break;
			}
    	}

    }
    close(tpinfo.socketfd);
    debug_printf("exit tcptest_thread, socketfd %d\n",tpinfo.socketfd);
	return NULL;
}

static int tpconnect(int argc, char ** argv)
{

	struct s_tp_thread_arg s_arg;
	s_arg.argc=argc;
	s_arg.argv=&argv[0];
	ertos_event_init(tp_evt_handler);
	ertos_task_create(tp_thread, "tp thread", 10*1024, &s_arg,  20);
	ertos_event_wait(tp_evt_handler, WAIT_INF);
	ertos_event_delete(tp_evt_handler);

	return 0;
}


//extern int drop_count;
int cmd_tp(int argc, char ** argv)
{
	if(argv[1])
	{
		if(!strcmp(argv[1],"connect"))
		{
			exit_flag = 0;
			tpconnect(argc-1,&argv[1]);
		}else if(!strcmp(argv[1],"stop"))
		{
			exit_flag = 1;
		}
/*
		else if(!strcmp(argv[1],"drop"))
		{
			if(argv[2]){
				debug_printf("set drop_count %s\n",argv[2]);
				drop_count=atoi(argv[2]);
			}
			debug_printf("drop_count=%d\n",drop_count);
		}
*/
	}else{
		struct timeval time;
		while(1)
		{
			usleep(500);
			gettimeofday(&time,NULL);
			debug_printf("time.sec = %d   time.usec=%d\n",time.tv_sec, time.tv_usec);
		}
	}

	return 0;
}
