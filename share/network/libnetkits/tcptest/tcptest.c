#include "includes.h"
#include "tcptest.h"
#include "libnetkits.h"


struct s_tcptest_thread_arg{
	int argc;
	char **argv;
} ;
ERTOS_EVENT_DECLARE(tcptest_evt_handler)
void * tcptest_thread(void * arg)
{
	struct s_tcptest_thread_arg *o_arg = (struct s_tcptest_thread_arg*)arg;
	int argc = o_arg->argc;
	char **argv = o_arg->argv;
	int udpflag = 0;
	int ret;
	int socketfd;
	struct sockaddr_in remote_address;
	int addrlen;
    static struct timeval tcptimeout;
    static fd_set tcpreadfds;;
    char buf[1600];
	struct s_getopt opt;

	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "ci::hup:d::r::");
        if(c == -1) break;
        switch(c) {
        	case 'u':
        		udpflag=1;
        		break;
            case 'h':
            default:
            	debug_printf("heartbeat usage:\n");
            	debug_printf("heartbeat add [options] [ip] [port]");
            	debug_printf("-u: udp heartbeat,default tcp heartbeat\n");
            	debug_printf("-h: help\n");
            	ertos_event_signal(tcptest_evt_handler);
            	return NULL;
            	break;
        }
    }

//    debug_printf("argv[opt.optind] %s\n",argv[opt.optind]);
//    debug_printf("argv[opt.optind+1] %s\n",argv[opt.optind+1]);

    {

		if(!argv[opt.optind] || !argv[opt.optind+1])
		{
			printf("parameters error!\n");
			ertos_event_signal(tcptest_evt_handler);
			return NULL;
		}

		remote_address.sin_family = AF_INET;
		if(argv[opt.optind])
			remote_address.sin_addr.s_addr = inet_addr(argv[opt.optind]);
		if(argv[opt.optind+1])
			remote_address.sin_port = htons(atoi(argv[opt.optind+1]));

		printf("%s connect to %s:%s\n",udpflag?"UDP":"TCP",argv[opt.optind],argv[opt.optind+1]);


		if(udpflag)
			socketfd = socket(AF_INET, SOCK_DGRAM, 0);
		else
			socketfd = socket(AF_INET, SOCK_STREAM, 0);

		ret = connect(socketfd, (struct sockaddr *) &remote_address, sizeof(remote_address));
		if (ret == -1) {
			printf("failed\n");
			return NULL;
		}else
			printf("success, fd %d\n",socketfd);

    }
    ertos_event_signal(tcptest_evt_handler);

    while(1)
    {
    	tcptimeout.tv_sec=0;
    	tcptimeout.tv_usec=50000;
    	FD_ZERO((fd_set *)&tcpreadfds);
    	FD_SET(socketfd, &tcpreadfds);
    	ret = select(socketfd+1, &tcpreadfds, NULL, NULL, &tcptimeout);
    	if(ret<0)
    	{
    		debug_printf("error\n");
    		break;
    	}else if(ret>0)
    	{
    		if(FD_ISSET(socketfd, &tcpreadfds))
    		{
    			addrlen=sizeof(remote_address);
    			ret = recvfrom(socketfd,buf,sizeof(buf),0,(struct sockaddr *)&remote_address,(socklen_t *)&addrlen);
    			debug_printf("recvfrom len %d , frome %s:%d\n",ret, inet_ntoa(remote_address.sin_addr.s_addr),ntohs(remote_address.sin_port));
    			if(!ret) break;
    		}
    	}else
    	{
    		//debug_printf("timeout\n");
    		//send(socketfd,tcptest_thread,1024,0);
    	}

    }
    close(socketfd);
    debug_printf("exit tcptest_thread, socketfd %d\n",socketfd);
	return NULL;
}

static int fdconnect(int argc, char ** argv)
{

	struct s_tcptest_thread_arg s_arg;
	s_arg.argc=argc;
	s_arg.argv=&argv[0];
	ertos_event_init(tcptest_evt_handler);
	ertos_task_create(tcptest_thread, "heartbeat thread", 10*1024, &s_arg,  20);
	ertos_event_wait(tcptest_evt_handler, WAIT_INF);
	ertos_event_delete(tcptest_evt_handler);

	return 0;

}

static int fdsend(int argc, char ** argv)
{
	int ret;
	int socketfd=0;
	struct s_getopt opt;

	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "i:");
        if(c == -1) break;
        switch(c) {
        	case 'i':
        		socketfd = atoi(opt.optarg);
        		break;
            case 'h':
            default:
            	debug_printf("send usage:\n");
            	debug_printf("send [option] [playload]");
            	debug_printf("-i: soketid\n");
            	debug_printf("-h: help\n");
            	return NULL;
            	break;
        }
    }
    if(argv[opt.optind])
    {
    	ret = send(socketfd,argv[opt.optind],strlen(argv[opt.optind]),0);
    	debug_printf("send return %d\n",ret);
    }
	return 0;
}

static int fdclose(int argc, char ** argv)
{
	int ret;
	int socketfd=0;
	struct s_getopt opt;

	memset(&opt,0,sizeof(opt));
    while(1) {
        int c = getopt2(&opt, argc, argv, "i:");
        if(c == -1) break;
        switch(c) {
        	case 'i':
        		socketfd = atoi(opt.optarg);
        		break;
            case 'h':
            default:
            	debug_printf("close usage:\n");
            	debug_printf("close [options]");
            	debug_printf("-i: soketid\n");
            	debug_printf("-h: help\n");
            	return NULL;
            	break;
        }
    }

	ret = close(socketfd);
	return 0;
}

//extern int drop_count;
int cmd_tcp(int argc, char ** argv)
{

	if(!strcmp(argv[1],"connect"))
	{
		fdconnect(argc-1,&argv[1]);
	}else if(!strcmp(argv[1],"send"))
	{
		fdsend(argc-1,&argv[1]);
	}else if(!strcmp(argv[1],"close"))
	{
		fdsend(argc-1,&argv[1]);
	}
/*	else if(!strcmp(argv[1],"drop"))
	{
		if(argv[2]){
			debug_printf("set drop_count %s\n",argv[2]);
			drop_count=atoi(argv[2]);
		}
		debug_printf("drop_count=%d\n",drop_count);
	}
*/
	return 0;
}
