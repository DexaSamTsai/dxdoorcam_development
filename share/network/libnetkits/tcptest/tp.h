#ifndef _TP_H_
#define _TP_H_

#include "includes.h"


typedef struct _tp_info{
	int udpmode;
	unsigned int clientspeed;
	unsigned int serverspeed;
	unsigned int plen;
	unsigned int cur_sseq;
	char ratetype;
	int respflag;
	unsigned int ticks;
	int socketfd;
	fd_set tcpreadfds;
	char tcp_appdata[2000];
} tp_info_t;



#define THROUGHPUT_MAGIC 0x12345678
#define MAXSEQ	0xffffffff

#define TYPE_SEND   1
#define TYPE_STOP	2
#define TYPE_RESP	3
#define TYPE_DATA   0
typedef struct __attribute__ ((packed)) _throughput_head{
	unsigned int  magic; //0x12345678
	unsigned int sseq; //
	unsigned int resv;
	unsigned char type; // 0: data, 1:request data send, 2:stop data send
	unsigned char sratetype; // 1: 'M', 2: 'K'
	unsigned short tcpwindow; // for tcp
	unsigned int srate; // 1: 1M/K bps, 2: 2M/K bps. ...
	unsigned int slen; // send data len.
	unsigned int ltime; //0: no print log; 1: pring log per 1s. ....
	unsigned int stime; // send data time.
}tp_head_t;



typedef struct __attribute__ ((packed)) _tp_status{
	int			 refresh_flag; // ==0 should record first_seq,and then set to 1.
	unsigned int first_seq;
	unsigned int last_seq;
	unsigned int packets_recv;
	unsigned int datalen_recv;
	unsigned int packets_send;
	unsigned int packets_resend;
	unsigned int datalen_send;
	unsigned int datalen_resend;
	unsigned int send_num;
	unsigned int poll_num;
}tp_status_t;


#endif
