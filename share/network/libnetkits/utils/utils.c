/*
 * utils.c
 *
 *  Created on: 2016��11��21��
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "libnetkits.h"

void ertos_queue_show(t_ertos_queuehandler queuehandler)
{
	t_ertos_queue * queue = (t_ertos_queue *)queuehandler;
	debug_printf("queue: depth %d, itemsize %d, msgcnt %d\n",queue->depth,queue->itemsize,queue->msgcnt);
}

void dump(u8* buf, u32 len)
{
     u32 row,col,i,j,pos;
     u8* sz = (u8* )buf;
     u8 tmp_sz[128];
     col=(len&0xf)?((len+0xf)>>4):(len>>4);
     for(i=0;i<col;i++)
     {
         memset(tmp_sz,0,sizeof(tmp_sz));
         j=i*0x10;
         if(j <=0xffff && j>=0xfff)
             sprintf((char*)tmp_sz,"%x",j);
         if(j <=0xfff && j>=0xff)
             sprintf((char*)tmp_sz,"0%x",j);
         if(j <=0xff && j>=0xf)
             sprintf((char*)tmp_sz,"00%x",j);
         if(j <=0xf)
             sprintf((char*)tmp_sz,"000%x",j);
         for(row=0;row<0x10;row++)
         {
             pos=i*0x10+row;
             if(pos>=len) break;
             if(((row&0x7)==0)&&row) sprintf((char*)(tmp_sz+strlen((char*)tmp_sz))," -");
//            debug_printf("(%x)",sz[pos]&0xff);
             if((sz[pos]&0xff)>0x0f)
             {
                 sprintf((char*)(tmp_sz+strlen((char*)tmp_sz))," %x",sz[pos]&0xff);
             }
             else
             {
                 sprintf((char*)(tmp_sz+strlen((char*)tmp_sz))," 0%x",sz[pos]&0xff);
             }
         }
         debug_printf("%s\n",tmp_sz);
     }
}

int str2argv(char * str, char *margv[], int margc)
{
	char argc;
	char *ptr;
	int wordflag,sentenceflag,i;
	memset(margv,0,sizeof(char*)*margc);
	argc = 0;
	ptr = str;
	wordflag=0;
	sentenceflag=0;
	while(*ptr){
		switch(*ptr)
		{
			case '\n':
			case ' ':
				if(sentenceflag==0)
				{
					*ptr=0;
					wordflag=0;
				}
				break;
			case '\"':
				if(sentenceflag==0)
				{
					sentenceflag=1;
					wordflag=1;
					argc++;

				}else{
					sentenceflag=0;
					wordflag=0;
				}
				*ptr=0;
				break;
			default:
				if(wordflag==0)
				{
					wordflag=1;
					argc++;
				}
				break;
		}
		ptr++;
	}
	if(argc > margc) argc = margc;
	ptr = str;
	for(i=0;i<argc;i++)
	{
		for(;*ptr==0;ptr++);
		margv[i]=ptr;
		for(;*ptr!=0;ptr++);
	}
	return argc;
}
#if 1
void cmd_exec(int (*cmd_func)(int , char ** ), char * parameters, char *cmd_name)
{
#define argv_num 10
	char *argv[argv_num] = {0};
	char argc;
	argc = str2argv(parameters, &argv[1], argv_num-1);
	argc++;
	if(cmd_name) argv[0] = cmd_name;
	else argv[0]="cmd_name";
	cmd_func(argc,argv);
}
#else
void cmd_exec(int (*cmd_func)(int , char ** ), char * parameters, char *cmd_name)
{
#define margv_num 10
	char *margv[margv_num] = {0};
	char **argv;
	char argc;
	char *ptr;
	int wordflag,sentenceflag,i;

	argc = 1;
	ptr = parameters;
	wordflag=0;
	sentenceflag=0;
	if(ptr)
	{
		while(*ptr){
			switch(*ptr)
			{
				case ' ':
					if(sentenceflag==0)
					{
						*ptr=0;
						wordflag=0;
					}
					break;
				case '\"':
					if(sentenceflag==0)
					{
						sentenceflag=1;
						wordflag=1;
						argc++;

					}else{
						sentenceflag=0;
						wordflag=0;
					}
					*ptr=0;
					break;
				default:
					if(wordflag==0)
					{
						wordflag=1;
						argc++;
					}
					break;
			}
			ptr++;
		}
	}
	if(argc > margv_num) argv = calloc(1,sizeof(char *)*argc);
	else	argv = margv;

	if(cmd_name) argv[0] = cmd_name;
	else argv[0]="cmd_name";
	ptr = parameters;
	for(i=1;i<argc;i++)
	{
		for(;*ptr==0;ptr++);
		argv[i]=ptr;
		for(;*ptr!=0;ptr++);
	}

	cmd_func(argc,argv);
	if(argv != margv)
		free(argv);
}
#endif

int cmd_date(int argc, char ** argv)
{

	int setflag=0;
	time_t t1;
    /* check arguments */
    optind = 0;
    while(1) {
        int c = getopt(argc, argv, "hs:");
        if(c == -1) break;
        switch(c) {
            case 'h': goto usage; break;
            case 's':
            	t1 = atol(optarg);
            	setflag = 1;
            	break;
            default:
//                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }

    if(setflag)
    {
    	stime(&t1);
    }
    time(&t1);
    debug_printf("%s", ctime(&t1));
    return 0;
usage:
	debug_printf("Usage: date [-s second]\n");
	debug_printf("\t-s: set time with seconds since 1970-01-01 00:00:00 UTC\n");
	return 0;
}
int cmd_reboot(int argc, char ** argv)
{
	reboot();
	return 0;
}

