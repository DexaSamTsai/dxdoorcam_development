
#include "includes.h"
#include <errno.h>
#include "getopt.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#define PRINT_ERROR ((p_getopt->opterr) && (*options != ':'))

#define FLAG_PERMUTE    0x01    /* permute non-options to the end of argv */
#define FLAG_ALLARGS    0x02    /* treat non-options as args to option "-1" */
#define FLAG_LONGONLY   0x04    /* operate as getopt_long_only */

/* return values */
#define BADCH       (int)'?'
#define BADARG      ((*options == ':') ? (int)':' : (int)'?')
#define INORDER     (int)1

#define EMSG        ""

static int getopt_internal(struct s_getopt *p_getopt, int, char * const *, const char *,
               const struct option *, int *, int);
static int parse_long_options(struct s_getopt *p_getopt, char * const *, const char *,
                  const struct option *, int *, int);
static int gcd(int, int);
static void permute_args(int, int, int, char * const *);



/* Error messages */
static const char recargchar[] = "option requires an argument -- %c\n";
static const char recargstring[] = "option requires an argument -- %s\n";
static const char ambig[] = "ambiguous option -- %.*s\n";
static const char noarg[] = "option doesn't take an argument -- %.*s\n";
static const char illoptchar[] = "unknown option -- %c\n";
static const char illoptstring[] = "unknown option -- %s\n";

/*
 * Compute the greatest common divisor of a and b.
 */
static int
gcd(int a, int b)
{
    int c;

    c = a % b;
    while (c != 0) {
        a = b;
        b = c;
        c = a % b;
    }

    return (b);
}

/*
 * Exchange the block from nonopt_start to nonopt_end with the block
 * from nonopt_end to opt_end (keeping the same order of arguments
 * in each block).
 */
static void
permute_args(int panonopt_start, int panonopt_end, int opt_end,
    char * const *nargv)
{
    int cstart, cyclelen, i, j, ncycle, nnonopts, nopts, pos;
    char *swap;

    /*
     * compute lengths of blocks and number and size of cycles
     */
    nnonopts = panonopt_end - panonopt_start;
    nopts = opt_end - panonopt_end;
    ncycle = gcd(nnonopts, nopts);
    cyclelen = (opt_end - panonopt_start) / ncycle;

    for (i = 0; i < ncycle; i++) {
        cstart = panonopt_end+i;
        pos = cstart;
        for (j = 0; j < cyclelen; j++) {
            if (pos >= panonopt_end)
                pos -= nnonopts;
            else
                pos += nopts;
            swap = nargv[pos];
            /* LINTED const cast */
            ((char **) nargv)[pos] = nargv[cstart];
            /* LINTED const cast */
            ((char **)nargv)[cstart] = swap;
        }
    }
}

/*
 * parse_long_options --
 *  Parse long options in argc/argv argument vector.
 * Returns -1 if short_too is set and the option does not match long_options.
 */
static int
parse_long_options(struct s_getopt *p_getopt, char * const *nargv, const char *options,
    const struct option *long_options, int *idx, int short_too)
{
    char *current_argv, *has_equal;
    size_t current_argv_len;
    int i, match;

    current_argv = p_getopt->place;
    match = -1;

    p_getopt->optind++;

    if ((has_equal = strchr(current_argv, '=')) != NULL) {
        /* argument found (--option=arg) */
        current_argv_len = has_equal - current_argv;
        has_equal++;
    } else
        current_argv_len = strlen(current_argv);

    for (i = 0; long_options[i].name; i++) {
        /* find matching long option */
        if (strncmp(current_argv, long_options[i].name,
            current_argv_len))
            continue;

        if (strlen(long_options[i].name) == current_argv_len) {
            /* exact match */
            match = i;
            break;
        }
        /*
         * If this is a known short option, don't allow
         * a partial match of a single character.
         */
        if (short_too && current_argv_len == 1)
            continue;

        if (match == -1)    /* partial match */
            match = i;
        else {
            /* ambiguous abbreviation */
            if (PRINT_ERROR)
                fprintf(stderr,
                        ambig, (int)current_argv_len,
                        current_argv);
            p_getopt->optopt = 0;
            return (BADCH);
        }
    }
    if (match != -1) {      /* option found */
        if (long_options[match].has_arg == no_argument
            && has_equal) {
            if (PRINT_ERROR)
                fprintf(stderr,
                        noarg, (int)current_argv_len,
                        current_argv);
            /*
             * XXX: GNU sets optopt to val regardless of flag
             */
            if (long_options[match].flag == NULL)
            	p_getopt->optopt = long_options[match].val;
            else
            	p_getopt->optopt = 0;
            return (BADARG);
        }
        if (long_options[match].has_arg == required_argument ||
            long_options[match].has_arg == optional_argument) {
            if (has_equal)
            	p_getopt->optarg = has_equal;
            else if (long_options[match].has_arg ==
                required_argument) {
                /*
                 * optional argument doesn't use next nargv
                 */
            	p_getopt->optarg = nargv[p_getopt->optind++];
            }
        }
        if ((long_options[match].has_arg == required_argument)
            && (p_getopt->optarg == NULL)) {
            /*
             * Missing argument; leading ':' indicates no error
             * should be generated.
             */
            if (PRINT_ERROR)
                fprintf(stderr,
                        recargstring,
                        current_argv);
            /*
             * XXX: GNU sets optopt to val regardless of flag
             */
            if (long_options[match].flag == NULL)
            	p_getopt->optopt = long_options[match].val;
            else
            	p_getopt->optopt = 0;
            --p_getopt->optind;
            return (BADARG);
        }
    } else {            /* unknown option */
        if (short_too) {
            --p_getopt->optind;
            return (-1);
        }
        if (PRINT_ERROR)
            fprintf(stderr, illoptstring, current_argv);
        p_getopt->optopt = 0;
        return (BADCH);
    }
    if (idx)
        *idx = match;
    if (long_options[match].flag) {
        *long_options[match].flag = long_options[match].val;
        return (0);
    } else
        return (long_options[match].val);
}

/*
 * getopt_internal --
 *  Parse argc/argv argument vector.  Called by user level routines.
 */
static int
getopt_internal(struct s_getopt *p_getopt, int nargc, char * const *nargv, const char *options,
    const struct option *long_options, int *idx, int flags)
{
    char *oli;              /* option letter list index */
    int optchar, short_too;
    static int posixly_correct = -1;

    if (options == NULL)
        return (-1);

    /*
     * Disable GNU extensions if POSIXLY_CORRECT is set or options
     * string begins with a '+'.
     */
    if (posixly_correct == -1)
        posixly_correct = (getenv("POSIXLY_CORRECT") != NULL);
    if (posixly_correct || *options == '+')
        flags &= ~FLAG_PERMUTE;
    else if (*options == '-')
        flags |= FLAG_ALLARGS;
    if (*options == '+' || *options == '-')
        options++;

    /*
     * XXX Some GNU programs (like cvs) set optind to 0 instead of
     * XXX using optreset.  Work around this braindamage.
     */
    if (p_getopt->optind == 0)
    	p_getopt->optind = p_getopt->optreset = 1;

    p_getopt->optarg = NULL;
    if (p_getopt->optreset)
    {
    	p_getopt->nonopt_start = p_getopt->nonopt_end = -1;
    	p_getopt->opterr = 1;     /* if error message should be printed */
    	p_getopt->optind = 1;     /* index into parent argv vector */
    	p_getopt->optopt = '?';       /* character checked for validity */
    	p_getopt->place = EMSG; /* option letter processing */
    }
start:
    if (p_getopt->optreset || !*p_getopt->place) {      /* update scanning pointer */
    	p_getopt->optreset = 0;
        if (p_getopt->optind >= nargc) {          /* end of argument vector */
        	p_getopt->place = EMSG;
            if (p_getopt->nonopt_end != -1) {
                /* do permutation, if we have to */
                permute_args(p_getopt->nonopt_start, p_getopt->nonopt_end,
                		p_getopt->optind, nargv);
                p_getopt->optind -= p_getopt->nonopt_end - p_getopt->nonopt_start;
            }
            else if (p_getopt->nonopt_start != -1) {
                /*
                 * If we skipped non-options, set optind
                 * to the first of them.
                 */
            	p_getopt->optind = p_getopt->nonopt_start;
            }
            p_getopt->nonopt_start = p_getopt->nonopt_end = -1;
            return (-1);
        }
        if (*(p_getopt->place = nargv[p_getopt->optind]) != '-' ||
            (p_getopt->place[1] == '\0' && strchr(options, '-') == NULL)) {
        	p_getopt->place = EMSG;       /* found non-option */
            if (flags & FLAG_ALLARGS) {
                /*
                 * GNU extension:
                 * return non-option as argument to option 1
                 */
            	p_getopt->optarg = nargv[p_getopt->optind++];
                return (INORDER);
            }
            if (!(flags & FLAG_PERMUTE)) {
                /*
                 * If no permutation wanted, stop parsing
                 * at first non-option.
                 */
                return (-1);
            }
            /* do permutation */
            if (p_getopt->nonopt_start == -1)
            	p_getopt->nonopt_start = p_getopt->optind;
            else if (p_getopt->nonopt_end != -1) {
                permute_args(p_getopt->nonopt_start, p_getopt->nonopt_end,
                		p_getopt->optind, nargv);
                p_getopt->nonopt_start = p_getopt->optind -
                    (p_getopt->nonopt_end - p_getopt->nonopt_start);
                p_getopt->nonopt_end = -1;
            }
            p_getopt->optind++;
            /* process next argument */
            goto start;
        }
        if (p_getopt->nonopt_start != -1 && p_getopt->nonopt_end == -1)
        	p_getopt->nonopt_end = p_getopt->optind;

        /*
         * If we have "-" do nothing, if "--" we are done.
         */
        if (p_getopt->place[1] != '\0' && *++p_getopt->place == '-' && p_getopt->place[1] == '\0') {
        	p_getopt->optind++;
        	p_getopt->place = EMSG;
            /*
             * We found an option (--), so if we skipped
             * non-options, we have to permute.
             */
            if (p_getopt->nonopt_end != -1) {
                permute_args(p_getopt->nonopt_start, p_getopt->nonopt_end,
                		p_getopt->optind, nargv);
                p_getopt->optind -= p_getopt->nonopt_end - p_getopt->nonopt_start;
            }
            p_getopt->nonopt_start = p_getopt->nonopt_end = -1;
            return (-1);
        }
    }

    /*
     * Check long options if:
     *  1) we were passed some
     *  2) the arg is not just "-"
     *  3) either the arg starts with -- we are getopt_long_only()
     */
    if (long_options != NULL && p_getopt->place != nargv[p_getopt->optind] &&
        (*p_getopt->place == '-' || (flags & FLAG_LONGONLY))) {
        short_too = 0;
        if (*p_getopt->place == '-')
        	p_getopt->place++;        /* --foo long option */
        else if (*p_getopt->place != ':' && strchr(options, *p_getopt->place) != NULL)
            short_too = 1;      /* could be short option too */

        optchar = parse_long_options(p_getopt, nargv, options, long_options,
            idx, short_too);
        if (optchar != -1) {
        	p_getopt->place = EMSG;
            return (optchar);
        }
    }

    if (((optchar = (int)*p_getopt->place++) == (int)':') ||
        (optchar == (int)'-' && *p_getopt->place != '\0') ||
        (oli = strchr(options, optchar)) == NULL) {
        /*
         * If the user specified "-" and  '-' isn't listed in
         * options, return -1 (non-option) as per POSIX.
         * Otherwise, it is an unknown option character (or ':').
         */
        if (optchar == (int)'-' && *p_getopt->place == '\0')
            return (-1);
        if (!*p_getopt->place)
            ++p_getopt->optind;
        if (PRINT_ERROR)
            fprintf(stderr, illoptchar, optchar);
        p_getopt->optopt = optchar;
        return (BADCH);
    }
    if (long_options != NULL && optchar == 'W' && oli[1] == ';') {
        /* -W long-option */
        if (*p_getopt->place)         /* no space */
            /* NOTHING */;
        else if (++p_getopt->optind >= nargc) {    /* no arg */
        	p_getopt->place = EMSG;
            if (PRINT_ERROR)
                fprintf(stderr, recargchar, optchar);
            p_getopt->optopt = optchar;
            return (BADARG);
        } else              /* white space */
        	p_getopt->place = nargv[p_getopt->optind];
        optchar = parse_long_options(p_getopt, nargv, options, long_options,
            idx, 0);
        p_getopt->place = EMSG;
        return (optchar);
    }
    if (*++oli != ':') {            /* doesn't take argument */
        if (!*p_getopt->place)
            ++p_getopt->optind;
    } else {                /* takes (optional) argument */
    	p_getopt->optarg = NULL;
        if (*p_getopt->place)         /* no white space */
        	p_getopt->optarg = p_getopt->place;
        /* XXX: disable test for :: if PC? (GNU doesn't) */
        else if (oli[1] != ':') {   /* arg not optional */
            if (++p_getopt->optind >= nargc) { /* no arg */
            	p_getopt->place = EMSG;
                if (PRINT_ERROR)
                    fprintf(stderr, recargchar, optchar);
                p_getopt->optopt = optchar;
                return (BADARG);
            } else
            	p_getopt->optarg = nargv[p_getopt->optind];
        } else if (!(flags & FLAG_PERMUTE)) {
            /*
             * If permutation is disabled, we can accept an
             * optional arg separated by whitespace so long
             * as it does not start with a dash (-).
             */
            if (p_getopt->optind + 1 < nargc && *nargv[p_getopt->optind + 1] != '-')
            	p_getopt->optarg = nargv[++p_getopt->optind];
        }
        p_getopt->place = EMSG;
        ++p_getopt->optind;
    }
    /* dump back option letter */
    return (optchar);
}


/*
 * getopt --
 *  Parse argc/argv argument vector.
 *
 * [eventually this will replace the BSD getopt]
 */
int
getopt2(struct s_getopt *p_getopt, int nargc, char * const *nargv, const char *options)
{

    /*
     * We don't pass FLAG_PERMUTE to getopt_internal() since
     * the BSD getopt(3) (unlike GNU) has never done this.
     *
     * Furthermore, since many privileged programs call getopt()
     * before dropping privileges it makes sense to keep things
     * as simple (and bug-free) as possible.
     */
    return (getopt_internal(p_getopt, nargc, nargv, options, NULL, NULL, FLAG_PERMUTE));
}


/*
 * getopt_long --
 *  Parse argc/argv argument vector.
 */
int
getopt_long2(struct s_getopt *p_getopt, int nargc, char * const *nargv, const char *options,
    const struct option *long_options, int *idx)
{

    return (getopt_internal(p_getopt, nargc, nargv, options, long_options, idx,
        FLAG_PERMUTE));
}

/*
 * getopt_long_only --
 *  Parse argc/argv argument vector.
 */
int
getopt_long_only2(struct s_getopt *p_getopt, int nargc, char * const *nargv, const char *options,
    const struct option *long_options, int *idx)
{

    return (getopt_internal(p_getopt, nargc, nargv, options, long_options, idx,
        FLAG_PERMUTE|FLAG_LONGONLY));
}


int cmd_getopt2_test(int argc, char ** argv)
{
	int i;
	struct s_getopt opt={0};
    for( i=0;i<argc;i++)
    	printf("argv[%d]=%s\n",i,argv[i]);

    debug_printf("-------------------------------------\n");
    /* check arguments */

    while(1) {
        int c = getopt2(&opt, argc, argv, "ifrhv::o:");
        if(c == -1) break;
        switch(c) {
            case 'i': debug_printf("i\n"); break;
            case 'f': debug_printf("f\n"); break;
            case 'r': debug_printf("r\n"); break;

            case 'h': debug_printf("h\n"); break;

            case 'v': debug_printf("v=%s\n",opt.optarg); break;
            case 'o': debug_printf("o=%s\n",opt.optarg); break;
            case 1: debug_printf("1=%s\n",opt.optarg); break;
            default:
//                debug_printf("Option '%c' (%d) with '%s'\n", c, c, optarg);
            	break;
        }
    }
    debug_printf("-----------------%d--------------------\n",opt.optind);
    for( i=0;i<argc;i++)
    	printf("argv[%d]=%s\n",i,argv[i]);

    return 0;
}




