/*
 * utils.h
 *
 *  Created on: 2016��11��21��
 *      Author: Cox.Yang
 */

#ifndef RAM_WIFI_TEST_LIB_UTILS_UTILS_H_
#define RAM_WIFI_TEST_LIB_UTILS_UTILS_H_

void ertos_queue_show(t_ertos_queuehandler queuehandler);
void dump(u8* buf, u32 len);
void cmd_exec(int (*cmd_func)(int , char ** ), char * parameters, char *cmd_name);
int cmd_date(int argc, char ** argv);
int str2argv(char * str, char *margv[], int margc);
#include "getopt.h"

#endif /* RAM_WIFI_TEST_LIB_UTILS_UTILS_H_ */
