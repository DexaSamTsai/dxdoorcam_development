/*
 * base64.h
 *
 *  Created on: 2016��10��13��
 *      Author: Cox.Yang
 */

#ifndef _BASE64_H_
#define _BASE64_H_

/*
 * Decode a base64-formatted buffer
 */
int Base64_Decode( unsigned char *dst, u32 *dlen, const unsigned char *src, u32 slen );
/*
 * Encode a buffer into base64 format
 */
int Base64_Encode( unsigned char *dst, u32 *dlen, const unsigned char *src, u32 slen );
#endif /* RAM_WIFI_TEST_BASE64_H_ */
