#include "includes.h"
#include "libnetkits.h"

int libnetkits_init(void)
{
	extern int cmd_openssl_test(int argc, char **argv);
	extern int cmd_lzo_test(int argc, char **argv);
	extern int cmd_ifconfig(int argc, char ** argv);
	extern int cmd_tun(int argc, char ** argv);
	extern int cmd_ping(int argc, char ** argv);
	extern int brcm_wl(int argc, char **argv);
	extern int cmd_openvpn(int argc, char **argv);
	extern int cmd_getopt2_test(int argc, char ** argv);
	extern int cmd_getopt_test(int argc, char ** argv);
	extern int cmd_date(int argc, char ** argv);
	extern int cmd_wifi(int argc, char ** argv);
	extern int cmd_heartbeat(int argc, char ** argv);
	extern int cmd_filter(int argc, char ** argv);
	extern int cmd_res(int argc, char ** argv);
	extern int cmd_tcp(int argc, char ** argv);
	extern int cmd_tp(int argc, char ** argv);
	extern int cmd_reboot(int argc, char ** argv);
	extern int cmd_video(int argc, char ** argv);
	extern int cmd_dns(int argc, char ** argv);
	int ret;
#ifdef LIBNETKITS_TEST
	ret = cmdshell_add("dns", cmd_dns, "cmd_dns");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_dns cmd failed\n");
	}
	ret = cmdshell_add("video", cmd_video, "cmd_video");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_video cmd failed\n");
	}
	ret = cmdshell_add("reboot", cmd_reboot, "cmd_reboot");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_reboot cmd failed\n");
	}
	ret = cmdshell_add("tp", cmd_tp, "cmd_tp");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_tp cmd failed\n");
	}
	ret = cmdshell_add("tcp", cmd_tcp, "cmd_tcp");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_tcp cmd failed\n");
	}
#endif
	ret = cmdshell_add("res", cmd_res, "cmd_res");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_res cmd failed\n");
	}
	ret = cmdshell_add("filter", cmd_filter, "cmd_filter tool for networks");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init cmd_filter cmd failed\n");
	}
	ret = cmdshell_add("hb", cmd_heartbeat, "heartbeat tool for networks");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init heartbeat cmd failed\n");
	}
#ifdef CONFIG_NETWORK_IPERF_EN
	ret = cmdshell_add("iperf", iperf_cmd, "iperf tool for networks");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init iperf cmd failed\n");
	}
#endif
#ifdef CONFIG_NETWORK_IPERF3_EN
	ret = cmdshell_add("iperf3", iperf3_cmd, "iperf3 tool for networks");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init iperf3 cmd failed\n");
	}
#endif
#ifdef CONFIG_TELNETD_EN
	ret = cmdshell_add("telnetd", cmd_telnetd, "start telnetd");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init telnetd cmd failed\n");
	}
#endif
#ifdef CONFIG_NETWORK_EN
	ret = cmdshell_add("ifconfig", cmd_ifconfig, "ifconfig");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init ifconfig cmd failed\n");
	}
	ret = cmdshell_add("tun", cmd_tun, "tun");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init tun cmd failed\n");
	}
	ret = cmdshell_add("ping", cmd_ping, "ping");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init ping cmd failed\n");
	}
#endif
#ifdef CONFIG_BRCM_WL_EN
	ret = cmdshell_add("wl", brcm_wl, "wl");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init wl cmd failed\n");
	}
#endif
#ifdef CONFIG_OPENVPN_EN
	ret = cmdshell_add("openvpn", cmd_openvpn, "openvpn");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init openvpn cmd failed\n");
	}
#endif
#ifdef CONFIG_OPENSSL_EN
#ifdef OPENSSL_TEST
	ret = cmdshell_add("openssl", cmd_openssl_test, "openssl test cmd");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init openssl cmd failed\n");
	}
#endif
#endif
#ifdef CONFIG_LZO_EN
#ifdef LZO_TEST
	ret = cmdshell_add("lzo", cmd_lzo_test, "lzo test cmd");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init lzo cmd failed\n");
	}
#endif
#endif
#ifdef CONFIG_NETWORK_THROUGHPUT_EN
	ret = cmdshell_add("throughput", throughput_cmd, "throughput tool for networks");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init throughput cmd failed\n");
	}
#endif
#ifdef CONFIG_LIBEFS_EN
	fs_add_cmds();
#endif

	ret = cmdshell_add("getopt", cmd_getopt_test, "getopt test");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init getopt test cmd failed\n");
	}
	ret = cmdshell_add("getopt2", cmd_getopt2_test, "getopt2 test");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init getopt2 test cmd failed\n");
	}
	ret = cmdshell_add("date", cmd_date, "date");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init date cmd failed\n");
	}
#ifdef CONFIG_WIFI_EN
	ret = cmdshell_add("wifi", cmd_wifi, "wifi");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init wifi cmd failed\n");
	}
#endif
#ifdef CONFIG_MCU_EN
	ret = cmdshell_add("msp", cmd_msp, "msp");
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Init msp cmd failed\n");
	}
#endif
	return 0;
}
