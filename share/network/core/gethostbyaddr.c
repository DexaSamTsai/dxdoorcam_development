#include "includes.h"

struct hostent *gethostbyaddr(const void *addr, socklen_t len, int type)
{
	if(type != AF_INET){
		h_errno = NO_RECOVERY;
		return NULL;
	}

	struct in_addr * f = (struct in_addr *)addr;
	char rname[64];

	sprintf(rname, "%d.%d.%d.%d.in-addr.arpa", (ntohl(f->s_addr) & 0xff), ((ntohl(f->s_addr) >> 8) & 0xff), ((ntohl(f->s_addr) >> 16) & 0xff), ((ntohl(f->s_addr) >> 24) & 0xff) );

	return gethostbyname(rname);
}

int gethostbyaddr_r(const void *addr, socklen_t len, int type, struct hostent *ret, char *buf, size_t buflen, struct hostent **result, int *h_errnop)
{
	if(type != AF_INET){
		h_errno = NO_RECOVERY;
		return NULL;
	}

	struct in_addr * f = (struct in_addr *)addr;
	char rname[64];

	sprintf(rname, "%d.%d.%d.%d.in-addr.arpa", (ntohl(f->s_addr) & 0xff), ((ntohl(f->s_addr) >> 8) & 0xff), ((ntohl(f->s_addr) >> 16) & 0xff), ((ntohl(f->s_addr) >> 24) & 0xff) );

	return gethostbyname_r(rname, ret, buf, buflen, result, h_errnop);
}

