#include "includes.h"

#if LWIP_VERSION < 0x02000000

static char * inet_ntop4 (const unsigned char *src, char *dst, size_t size)
{
	char tmp[sizeof "255.255.255.255"];
	size_t len;

	tmp[0] = '\0';
	(void)snprintf(tmp, sizeof(tmp), "%d.%d.%d.%d",
			((int)((unsigned char)src[0])) & 0xff,
			((int)((unsigned char)src[1])) & 0xff,
			((int)((unsigned char)src[2])) & 0xff,
			((int)((unsigned char)src[3])) & 0xff);

	len = strlen(tmp);
	if(len == 0 || len >= size) {
		errno = ENOSPC;
		return (NULL);
	}
	strcpy(dst, tmp);
	return dst;
}

const char *inet_ntop(int af, const void * src, char * buf, socklen_t size)
{
	switch (af) {
	case AF_INET:
		return inet_ntop4((const unsigned char*)src, buf, size);
/*
	//TODO
	case AF_INET6:
*/
	default:
		errno = EAFNOSUPPORT;
		return NULL;
	}
}

#endif
