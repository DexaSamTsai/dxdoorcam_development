#include "includes.h"
#include "data_demux.h"

#define FRAME_AVERAGE_TIME     ((512*1000)/(8000*2))
#define MAX_LIST_WINDOW		   20

static void aplayd_fill_data(u32 list_window)
{
    int pl_len;
    int size, offset;
    char *p_pl;
    FRAME *afrm;

    offset = size = 0;
    p_pl = NULL;
    data_demux_get_pl(&p_pl, &pl_len, list_window);
    while(p_pl && (offset < pl_len))
    {
        afrm = libadec_get_frame(1);
        if(afrm)
        {
//            debug_printf("a(%d,%d):%x:%x\n",afrm->size, pl_len);
            size = ((afrm->size + afrm->size1) > (pl_len-offset)) ? (pl_len-offset) : (afrm->size + afrm->size1);
            if(size > afrm->size)
            {
                memcpy(afrm->addr, p_pl+offset, afrm->size);
                memcpy(afrm->addr1, p_pl+offset+afrm->size, size-afrm->size);
                afrm->size1 = size-afrm->size;
            }
            else
            {
                memcpy(afrm->addr, p_pl+offset, size);
                afrm->size = size;
                afrm->size1 = 0;
            }
            libadec_add_frame(afrm);

            offset += size;
        }
    };
    data_demux_release_pl(p_pl);
}

void *aplayd_routine(void* arg)
{
	t_aplayd_cfg * cfg = (t_aplayd_cfg *)arg;
	u32 stop_count;

	pthread_detach(pthread_self());
	pthread_setname(NULL, "APlayD");

	if(cfg->listen_port == 0)
	{
#define APLAYD_DEFAULT_LISTEN_PORT	5000
		syslog(LOG_WARNING, "WARN: aplayd use default port %d\n", APLAYD_DEFAULT_LISTEN_PORT);
		cfg->listen_port = APLAYD_DEFAULT_LISTEN_PORT;
	}

	cfg->socket = socket(AF_INET, SOCK_DGRAM, 0);
    if( cfg->socket < 0 )
    {
		syslog(LOG_ERR, "ERROR: aplayd udp socket() fail.\n");
		return NULL;
    }
    
	struct sockaddr_in local_addr; // local
    memset(&local_addr, 0, sizeof(struct sockaddr_in));
    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
    local_addr.sin_port = htons(cfg->listen_port); 
    if(bind(cfg->socket, (struct sockaddr *)&local_addr, sizeof(struct sockaddr_in)) == -1)
    {
    	syslog(LOG_ERR, "ERROR: aplayd udp bind() fail.\n");
    	close(cfg->socket);
		return NULL;
    }
	syslog(LOG_DEBUG, "aplayd create socket:%x\n", cfg->socket);

	struct timeval tv;
	tv.tv_sec = cfg->adec_stop_ms / 1000;
	tv.tv_usec = (cfg->adec_stop_ms % 1000) * 1000;

	struct sockaddr_in remote_addr;
	int remote_addr_len = sizeof(remote_addr);
	int len;
	fd_set readfds;

	cfg->adec_running = 0;
	cfg->exit = 0;

	data_demux_init();

	while(1)
	{
	    if(cfg->exit)        break;

		FD_ZERO(&readfds);
		FD_SET(cfg->socket, &readfds);
		select(cfg->socket+1, &readfds, NULL, NULL, cfg->adec_stop_ms == 0 ? NULL : &tv);

		if(FD_ISSET(cfg->socket, &readfds))
		{
			len = recvfrom(cfg->socket, cfg->recvbuf, sizeof(cfg->recvbuf), 0,(struct sockaddr *)&remote_addr, &remote_addr_len );
			if(len > 0)//data
			{
				int fmt;
				int pl_len = data_demux_parse_pl(cfg->recvbuf,len,&fmt);
				if(pl_len > 0)
				{
					if(!cfg->adec_running)
					{
						if(cfg->adec_start)     cfg->adec_start(fmt);
						cfg->adec_running = 1;

		                tv.tv_sec = (FRAME_AVERAGE_TIME*MAX_LIST_WINDOW) / 1000;
		                tv.tv_usec = ((FRAME_AVERAGE_TIME*MAX_LIST_WINDOW) % 1000) * 1000;
					}

					stop_count = 0;
					aplayd_fill_data(MAX_LIST_WINDOW);
				}
			}
		}
		else
		{
			if(cfg->adec_running)
			{
			    stop_count ++;
			    aplayd_fill_data((MAX_LIST_WINDOW > stop_count) ? (MAX_LIST_WINDOW - stop_count) : 1);
//			    syslog(LOG_WARNING, "to:%d, %d, %d\n", ticks, stop_count, data_demux_pl_count());

                tv.tv_sec = FRAME_AVERAGE_TIME / 1000;
                tv.tv_usec = (FRAME_AVERAGE_TIME % 1000) * 1000;

			    if(stop_count > (cfg->adec_stop_ms/FRAME_AVERAGE_TIME))
			    {
                    data_demux_clear_pl();
                    cfg->adec_running = 0;
                    if(cfg->adec_stop)  cfg->adec_stop();
                    if(cfg->adec_exit)  cfg->adec_exit();

                    tv.tv_sec = cfg->adec_stop_ms / 1000;
                    tv.tv_usec = (cfg->adec_stop_ms % 1000) * 1000;
			    }
			}
		}
	}

	//exit
	close(cfg->socket);

	data_demux_exit();

	cfg->exit = 2;

	return NULL;
}

int aplayd_start(t_aplayd_cfg * cfg)
{
    pthread_attr_t aplayd_attr;
    pthread_attr_init(&aplayd_attr);
    pthread_attr_setstacksize(&aplayd_attr, 4*1024);
	int ret = pthread_create(&cfg->pt, &aplayd_attr, aplayd_routine, cfg);
	if(ret != 0)
	{
		syslog(LOG_ERR, "[ERROR]: aplayd thread create error\n");
		return -4;
	}
	return 0;
}

int aplayd_stop(t_aplayd_cfg * cfg)
{
    cfg->exit = 1;

    return 0;
}
