/*
 * data_demux.h
 *
 *
 *
 */

#ifndef DATA_DEMUX_H_
#define DATA_DEMUX_H_

extern void data_demux_init(void);
extern int data_demux_parse_pl(char *buf, int len,unsigned int *flag);
extern void data_demux_get_pl(char **pp_pl, int *p_len, u32 list_window);
extern void data_demux_release_pl(char *p_pl);
extern u32 data_demux_pl_count(void);
extern void data_demux_clear_pl(void);
extern void data_demux_exit(void);

#endif /* DATA_DEMUX_H_ */
