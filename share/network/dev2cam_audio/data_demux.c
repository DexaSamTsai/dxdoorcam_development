#include "includes.h"
#include "data_demux.h"
#include "list.h"

#define SN_MAX_INTERVAL     40

typedef struct _DATA_PL
{
    struct list_head node;
    char *p_pl;
    u16 pl_len;
    u16 sn;
    u32 ts;
    struct timeval tv;
}DATA_PL;

typedef struct _DATA_PL_DEMUX
{
	u16 pl_sn;
	u16 reserve;
	u32 count;
    DATA_PL data_pl;
}DATA_PL_DEMUX;

static DATA_PL_DEMUX g_data_pl_demux;

/*********************************************************************************/
void data_demux_init(void)
{
    memset(&g_data_pl_demux, 0, sizeof(g_data_pl_demux));
    INIT_LIST_HEAD(&(g_data_pl_demux.data_pl.node));
}

int data_demux_parse_pl(char *buf, int len, unsigned int *flag)
{
    struct list_head *ptr;
    DATA_PL *p_item;
    DATA_PL *p_node;

	if(len > sizeof(rtp_header_t))
	{
		rtp_header_t *rtp_h;
		rtp_h = (rtp_header_t *)buf;

		*flag = 5; //TODO: parse fmt

		p_item = (DATA_PL*)malloc(sizeof(DATA_PL));
		if(p_item)
		{
		    INIT_LIST_HEAD(&(p_item->node));
		    p_item->pl_len = len - sizeof(rtp_header_t);
		    p_item->p_pl = (char*)malloc(p_item->pl_len);
		    p_item->sn = rtp_h->sn;
		    p_item->ts = rtp_h->ts;
		}

		if(p_item && p_item->p_pl)
		{
			gettimeofday(&(p_item->tv), NULL);
		    memcpy(p_item->p_pl, buf + sizeof(rtp_header_t), p_item->pl_len);

		    if(list_empty(&(g_data_pl_demux.data_pl.node)))
		    {
		        list_add_tail(&(p_item->node), &(g_data_pl_demux.data_pl.node));
		        g_data_pl_demux.count++;
		    }
		    else
		    {
		        p_node = list_first_entry(&(g_data_pl_demux.data_pl.node), DATA_PL, node);

		        list_for_each_prev(ptr, &(g_data_pl_demux.data_pl.node))
	            {
		        	p_node = list_get_entry(ptr, DATA_PL, node);
	                if(rtp_h->sn >= p_node->sn)
	                {
	                	//add after p_node
	                    list_add(&(p_item->node), &(p_node->node));
	                    g_data_pl_demux.count++;
	                    break;
	                }
	            }

                if(rtp_h->sn < p_node->sn)
                {
                    if((rtp_h->sn + SN_MAX_INTERVAL) <= p_node->sn)
                    {
                    	//exception sn
                        list_add_tail(&(p_item->node), &(g_data_pl_demux.data_pl.node));
                    }
                    else
                    {
                    	//add to head
                        list_add(&(p_item->node), &(g_data_pl_demux.data_pl.node));
                    }
                    g_data_pl_demux.count++;

#if 0
                    //for debug
                    p_item = list_last_entry(&(g_data_pl_demux.data_pl.node), DATA_PL, node);
                    syslog(LOG_WARNING, "SN:%d,%d, %d, %d\n", rtp_h->sn, p_node->sn, p_item->sn, g_data_pl_demux.count);
#endif
                }
		    }
//		    syslog(LOG_WARNING, "malloc:%x,%x,%d\n", p_item, p_item->p_pl, g_data_pl_demux.count);
		}
		else
		{
		    syslog(LOG_ERR, "list malloc fail\n");
		    while(1);
		}

		return (len - sizeof(rtp_header_t));
	}

	return 0;
}

void data_demux_get_pl(char **pp_pl, int *p_len, u32 list_window)
{
    DATA_PL *p_item;
    p_item = list_first_entry(&(g_data_pl_demux.data_pl.node), DATA_PL, node);

    if(p_item && p_item->p_pl && (g_data_pl_demux.count >= list_window))
    {
        *pp_pl = p_item->p_pl;
        *p_len = p_item->pl_len;

        if(g_data_pl_demux.pl_sn != p_item->sn)
        {
        	syslog(LOG_WARNING, "PL:%x,%d,%d,%d,%d,%d\n", p_item->p_pl, p_item->pl_len, g_data_pl_demux.pl_sn, p_item->sn, p_item->tv.tv_sec, p_item->tv.tv_usec);
        	g_data_pl_demux.pl_sn = p_item->sn;
        }
        g_data_pl_demux.pl_sn++;
    }
    else
    {
        *pp_pl = NULL;
        *p_len = 0;
    }
}

void data_demux_release_pl(char *p_pl)
{
    struct list_head *ptr;
    DATA_PL *p_item;

    if(p_pl == NULL)		return;

    list_for_each(ptr, &(g_data_pl_demux.data_pl.node))
    {
        p_item = list_get_entry(ptr, DATA_PL, node);
        if(p_item->p_pl == p_pl)
        {
            list_del(&(p_item->node));
            g_data_pl_demux.count--;

//            syslog(LOG_WARNING, "free:%x,%x,%d\n", p_item, p_item->p_pl, g_data_pl_demux.count);

            free(p_item->p_pl);
            p_item->p_pl = NULL;
            free(p_item);
            p_item = NULL;

            break;
        }
    }
}

u32 data_demux_pl_count(void)
{
	return g_data_pl_demux.count;
}

void data_demux_clear_pl(void)
{
    DATA_PL *p_item;

    while(!list_empty(&(g_data_pl_demux.data_pl.node)))
    {
        p_item = list_first_entry(&(g_data_pl_demux.data_pl.node), DATA_PL, node);
        if(p_item)
        {
            list_del(&(p_item->node));
            g_data_pl_demux.count--;

            free(p_item->p_pl);
            p_item->p_pl = NULL;
            free(p_item);
            p_item = NULL;
        }
    }

    g_data_pl_demux.pl_sn = 0;

    if(g_data_pl_demux.count != 0)
    	syslog(LOG_ERR, "ERR:%d\n", g_data_pl_demux.count);
}

void data_demux_exit(void)
{
    data_demux_clear_pl();
    memset(&g_data_pl_demux, 0, sizeof(g_data_pl_demux));
}
