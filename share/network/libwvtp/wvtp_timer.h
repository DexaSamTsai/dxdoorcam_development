#ifndef _WVTP_TIMER1_H_
#define _WVTP_TIMER1_H_




void wvtp_timer_init(wvtp_context_t * pcontext);
void  wvtp_timer_insert(wvtp_context_t * pcontext,u8 seq,u32 len, u8 sendtype);
s8   wvtp_timer_check_expired(wvtp_context_t * pcontext,u8* seq);
void wvtp_timer_cancel(wvtp_context_t * pcontext,u8 seq);
void wvtp_timer_cal(wvtp_context_t * pcontext,u8 acknum);
#endif
