#ifndef _WVTP_APP_H_
#define _WVTP_APP_H_

extern u32 wvtp_app_make_frame(void * handle,u8 * pdata, u8 index);
extern u32 wvtp_app_save_frame(void * handle,u8 * pdata, u32 len, u8 index);
extern s8 wvtp_app_sbuf_fill_data(void * handle,u8 buf_index);
extern void wvtp_app_sbuf_release_data(void * handle,u8 buf_index);
extern int wvtp_app_rbuf_handle(void * handle,u8 buf_index);


#endif //_WVTP_APP_H_
