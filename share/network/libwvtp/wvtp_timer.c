#include "includes.h"
#include "wvtp.h"
#include "wvtp_timer.h"


#define K 	4
#define beta	 4  // 1/4
#define alpha	 8  //1/8

extern unsigned int clock_time(void);

void  RTT_init(wvtp_context_t * pcontext,int R)
{
	pcontext->SRTT=R;
	pcontext->RTTVAR=R/2;
	pcontext->RTO=pcontext->SRTT + K*pcontext->RTTVAR;
	return ;
}

void RTT_cal(wvtp_context_t * pcontext,u32 R)
{

	int delta = R - pcontext->SRTT;

	pcontext->SRTT = pcontext->SRTT + ((delta)/beta);
	if(delta <0)
		delta = -delta;
	pcontext->RTTVAR = pcontext->RTTVAR + ((delta - pcontext->RTTVAR)/alpha);
	pcontext->RTO = pcontext->SRTT + K*pcontext->RTTVAR;
	return ;
}




void wvtp_timer_cal(wvtp_context_t * pcontext,u8 acknum)
{
	u32 current_time,M;

	if(pcontext->sendflag[acknum % pcontext->NR_BUFS].num == 1)
	{
		current_time = clock_time();
		if(current_time >= pcontext->sendflag[acknum % pcontext->NR_BUFS].time)
			M = current_time - pcontext->sendflag[acknum % pcontext->NR_BUFS].time;
		else{
			M = (u32)0xffffffff - pcontext->sendflag[acknum % pcontext->NR_BUFS].time;
			M += current_time + 1;
		}
		if(M==0)
			M=2;
		pcontext->RTT1=M;
		pcontext->RTT = (7*pcontext->RTT + M)/8;

		RTT_cal(pcontext,M);

		pcontext->maxretranstime=1;
	}
}


void wvtp_timer_init(wvtp_context_t * pcontext)
{
	int i;
	for (i = 0; i < pcontext->NR_BUFS; i++)
	{
		pcontext->sendflag[i].time = 0;
		pcontext->sendflag[i].num = 0;
		pcontext->sendflag[i].nak = 0;
		pcontext->sendflag[i].seq = 0;
		pcontext->sendflag[i].flag = 0;
	}
	pcontext->wvtp_timer_current = 0;
	pcontext->resendnum = 0;
	pcontext->resendnak = 0;
	pcontext->senddatalen = 0;
	pcontext->recvdatalen = 0;
	pcontext->data_timeout = 0;
	pcontext->maxretranstime = 0;
	pcontext->RTT = 10;
	pcontext->RTT1=10;
	pcontext->sbufresendnum=0;
	RTT_init(pcontext,10);
	pcontext->NAK_FLAG = 0;

}


void wvtp_timer_print(wvtp_context_t * pcontext)
{

	debug_printf("------------------------------\n");
//	for(i=0; i<pcontext->NR_BUFS; i++)
//	{
//		debug_printf("%d: flag=%d, seq=%d, num=%d, time=%d\n",
//			i, sendflag[i].flag,  sendflag[i].seq,sendflag[i].num,sendflag[i].time);
//	}
	debug_printf("RTT1=%d, RTO=%d, RTT=%d\n",pcontext->RTT1,pcontext->RTO,pcontext->RTT);
	debug_printf("max_buffer=%d, bufed=%d, sbufresendnum=%d\n",pcontext->max_buffer,pcontext->nbuffered,pcontext->sbufresendnum);
	debug_printf("maxretranstime=%d, data_timeout=%d\n",pcontext->maxretranstime,pcontext->data_timeout);
	debug_printf("seq=%x, ack=%x, ackrev=%x\n",pcontext->wvtp_next_seq_to_send,pcontext->wvtp_ack_expected,pcontext->ack_rcved);

	debug_printf("send pkg num =%d, get data fail= %d\n",pcontext->sendpkgnum, pcontext->getfailnum);
	debug_printf("poll num=%d, send buf full= %d, empty= %d\n",pcontext->pollnum, pcontext->buffullnum,pcontext->pollnum-pcontext->buffullnum);
	debug_printf("resend num=%d,%d timeout num=%d, recv nak num =%d\n",pcontext->resendnum,pcontext->resendnak ,pcontext->resendnum-pcontext->recvnaknum, pcontext->recvnaknum);
	debug_printf("Send Rate=%dkb/s; recv Rate=%dkb/s\n",pcontext->senddatalen*8/1024,pcontext->recvdatalen*8/1024);
	u8 * ptr = (u8*)&pcontext->remote_ip;
	debug_printf("send to %d.%d.%d.%d:%d\n",ptr[0],ptr[1],ptr[2],ptr[3],ntohs(pcontext->remote_port));
	pcontext->pollnum =0;
	pcontext->sendpkgnum=0;
	pcontext->getfailnum = 0;
	pcontext->buffullnum = 0;
	pcontext->resendnum = 0;
	pcontext->resendnak = 0;
	pcontext->senddatalen =0;
	pcontext->recvnaknum = 0;
	pcontext->recvdatalen = 0;
	debug_printf("------------------------------\n");
}
void  wvtp_timer_insert(wvtp_context_t * pcontext,u8 seq,u32 len, u8 sendtype)
{
	u8 index = seq%pcontext->NR_BUFS;
	pcontext->sendflag[index].time=clock_time();
	if(sendtype == ALDG_DATA)
	{
		pcontext->sendflag[index].num = 0;
		pcontext->sendflag[index].nak = 0;
	}
	else if(sendtype == ALDG_DATA_BYTIMEOUT)
	{
		pcontext->sendflag[index].num++;
		pcontext->resendnum++;
	}
	else if(sendtype == ALDG_DATA_BYNAK)
	{
		pcontext->sendflag[index].nak++;
		pcontext->resendnak++;
	}
	pcontext->sendflag[index].seq = seq;
	pcontext->sendflag[index].flag = 1;
	if(pcontext->sendflag[index].num > pcontext->maxretranstime )
		pcontext->maxretranstime = pcontext->sendflag[index].num;


	//pcontext->sendflag[index].timeout = 2*pcontext->RTO*(1<<pcontext->sendflag[index].num);
	pcontext->sendflag[index].timeout = 2*pcontext->RTT*(1<<pcontext->sendflag[index].num);
	if(pcontext->sendflag[index].timeout >500)
		pcontext->sendflag[index].timeout = 500;
	if(pcontext->sendflag[index].timeout <30)
		pcontext->sendflag[index].timeout = 30;


	if(pcontext->sendflag[index].num > 0)
	{
		if(pcontext->sendflag[index].num == 1 )
			pcontext->sbufresendnum++;
		//debug_printf("timer insert seq=%d, num=%d, time=%d, maxretranstime=%d, data_timeout=%d\n",
			//seq,sendflag[index].num,sendflag[index].time, maxretranstime,data_timeout);
	}else if(pcontext->sendflag[index].num==0){
		pcontext->senddatalen +=len;
	}
}

s8   wvtp_timer_check_expired(wvtp_context_t * pcontext,u8* seq)
{
	u32 i;
	u32 current_time=clock_time();
	u32 temptime;
	u32 index;
	pcontext->wvtp_timer_current = pcontext->wvtp_ack_expected;
	for( i = 0 ; i< pcontext->NR_BUFS; i++ )
	{

		index = (pcontext->wvtp_timer_current++) % pcontext->NR_BUFS;

#if 0
		static u8 temp_index = 0xff;
		if(temp_index==0xff && pcontext->NAK_FLAG)
		{
			temp_index = index>0? (index-1):(pcontext->NR_BUFS-1) ;
		}else
			pcontext->NAK_FLAG =0;

#endif

		if (pcontext->sendflag[index].flag == 0)
			continue;
#if 0
		if(temp_index != 0xff && temp_index != index)
		{
			*seq = pcontext->sendflag[index].seq;

			return 0;
		}else{
			temp_index =0xff;
		}
#endif

		if(current_time >= pcontext->sendflag[index].time )
		{
			temptime = current_time - pcontext->sendflag[index].time;
		}else
		{
			temptime = 0xffffffff - pcontext->sendflag[index].time;
			temptime += current_time + 1;
			debug_printf("check timer expired error seq=%d, temptime=%d, data_timeout=%d\n",pcontext->sendflag[index].seq, temptime, pcontext->data_timeout);
		}

		if(temptime > pcontext->sendflag[index].timeout)
		{
			*seq = pcontext->sendflag[index].seq;
			//wvtp_timer_cancel(*seq);
			return 0;
		}

	}
	return -1;
}

void wvtp_timer_cancel(wvtp_context_t * pcontext,u8 seq)
{
	u8 index = seq%pcontext->NR_BUFS;
	if(pcontext->sendflag[index].num>1)
		pcontext->sbufresendnum--;

	pcontext->sendflag[index].seq=0;
	pcontext->sendflag[index].num=0;
	pcontext->sendflag[index].nak = 0;
	pcontext->sendflag[index].time=0;
	pcontext->sendflag[index].timeout = 0;
	pcontext->sendflag[index].flag =0;
}


