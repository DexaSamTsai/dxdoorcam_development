#include "includes.h"
#include "wvtp.h"
#include "wvtp_timer.h"
#include "wvtp_app.h"
#include "lwip/udp.h"
#include "libDxDoorCam.h"

#if 1
#define WVTP_DEBUG(x...)
#else
#define WVTP_DEBUG	debug_printf
#endif

//#define FLOW_MONITOR_ENABLE		// monitor the framerate and bitrate

#ifdef FLOW_MONITOR_ENABLE
#define FLOW_CHECK_PERIOD	100		//1s
typedef struct s_flow_monitor{
	u32 start_time;
	u32 frames_cnt;
	u32 bytes_cnt;
}t_flow_monitor;
static t_flow_monitor	g_flow;
#endif

#define WVTP_NUM 1
wvtp_context_t wvtp_context[WVTP_NUM];

struct udp_pcb *wvtp_pcb;

unsigned int link_timeout;
int wvtp_max_num;

static pthread_mutex_t g_mutex = PTHREAD_MUTEX_INITIALIZER;

extern ip_addr_t g_server_ip;
extern int g_server_port;

int wvtp_exit = 0;

#if 0
#define inc(k) if ((k) < MAX_SEQ) k = k + 1; else k = 0
#define dec(k) if ((k) >0 ) k = k - 1; else k = MAX_SEQ
#else
#define inc(k) {((k)<MAX_SEQ) ? (k++) : (k=0);}
#define dec(k) {((k)>0) ? (k--) : (k=MAX_SEQ);}
#endif

unsigned int clock_time(void)
{
	unsigned int val;
	unsigned int val1;

	struct timeval time;
	gettimeofday(&time,NULL);

	val = (unsigned int)time.tv_sec;
	val1 = (unsigned int)time.tv_usec;

	val = val*1000;
	val1 = val1/1000;

	val += val1;
	return val;
}

static bool between(u8 a, u8 b, u8 c)
{
	/* Same as between in protocol5, but shorter and more obscure. */
	return ((a <= b) && (b < c)) || ((c < a) && (a <= b)) || ((b < c) && (c < a));
}

void wvtp_udp_init(ip_addr_t *server_ip, int (*handle)(int))
{
	wvtp_pcb = udp_new();
	udp_bind(wvtp_pcb, IP_ADDR_ANY, 0);
	udp_connect(wvtp_pcb, server_ip, 2001);
	udp_recv(wvtp_pcb, handle, NULL);

	return;
}

void wvtp_init_with_buf(ip_addr_t *server_ip, int (*handle)(int), unsigned int timeout, int wvtp_num, WVTP_SBUF * out_buf, SENDFLAG * sendflag, int sendbuf_cnt)
{
	memset(wvtp_context, 0 ,sizeof(wvtp_context[0]) * wvtp_num);
	wvtp_udp_init(server_ip,handle);
	link_timeout = timeout;
	wvtp_max_num = wvtp_num;

	int i;

	for(i=0; i<wvtp_num; i++){
		wvtp_context[i].out_buf = out_buf + (sendbuf_cnt * i);
		wvtp_context[i].sendflag = sendflag + (sendbuf_cnt * i);
		wvtp_context[i].sendbuf_cnt = sendbuf_cnt;
	}
	//debug_printf("wvtp timeout %d\n",timeout);
	return ;
}

static u8_t is_wvtp_msg(struct pbuf *p)
{
	PWVTP_HEAD pwvtp_head = ( PWVTP_HEAD) p->payload;
	if(pwvtp_head->flowid != WVTP_MAGIC_NUM)
	{
		syslog(LOG_ERR, "not wvtp msg\n");
		return 0;
	}
	return 1;
}

void wvtp_set_linktimeout(u32 timeout)
{
	link_timeout = timeout;
}

void wvtp_cancel_context(wvtp_context_t * pcontext)
{
	int i;
	if(wvtp_handler.cb_parg_release){
		wvtp_handler.cb_parg_release(pcontext->parg);
		pcontext->parg = NULL;
	}
	WVTP_DEBUG("release seq %d, addr=0x%x\n",pcontext->wvtp_ack_expected,pcontext->out_buf[pcontext->wvtp_ack_expected % pcontext->NR_BUFS].data_ptr);
	if(pcontext->app_sbuf_release)
	{
		for( i=0; i<pcontext->NR_BUFS; i++ )
		{
			if(pcontext->sendflag[i].flag)
				(*pcontext->app_sbuf_release)(pcontext, pcontext->sendflag[i].seq % pcontext->NR_BUFS);
		}
	}
	void * out_buf_back = pcontext->out_buf;
	void * sendflag_back = pcontext->sendflag;
	int cnt_back = pcontext->sendbuf_cnt;
	memset(pcontext, 0, sizeof(wvtp_context_t));
	pcontext->out_buf = out_buf_back;
	pcontext->sendflag = sendflag_back;
	pcontext->sendbuf_cnt = cnt_back;

	return;
}
void wvtp_init_context(wvtp_context_t * pcontext,int nr_bufs)
{
	int i;

	if(wvtp_handler.cb_parg_new){
		pcontext->parg = wvtp_handler.cb_parg_new();
	}

	if (nr_bufs <= pcontext->sendbuf_cnt)
		pcontext->NR_BUFS = nr_bufs;
	else
		pcontext->NR_BUFS = pcontext->sendbuf_cnt;

	pcontext->no_nak=TRUE;
	pcontext->wvtp_ack_expected=0;
	pcontext->wvtp_next_seq_to_send=0;
	pcontext->wvtp_seq_expected=0;
	pcontext->wvtp_too_far=NR_BUFR;
	pcontext->nbuffered=0;
	for (i = 0; i < NR_BUFR; i++)
	{
		pcontext->arrived[i] = FALSE;
	}

	pcontext->make_frame_from_app_sbuf=wvtp_app_make_frame;
	pcontext->save_frame_to_app_rbuf=wvtp_app_save_frame;
	pcontext->app_sbuf_fill=wvtp_app_sbuf_fill_data;
	pcontext->app_sbuf_release=wvtp_app_sbuf_release_data;
	pcontext->app_rbuf_handle=wvtp_app_rbuf_handle;


	pcontext->getfailnum=0;
	pcontext->buffullnum=0;
	pcontext->recvnaknum=0;
	pcontext->pollnum=0;
	pcontext->sendpkgnum=0;


	pcontext->max_buffer = pcontext->NR_BUFS;


	pcontext->time = clock_time();
	wvtp_timer_init(pcontext);
}

wvtp_context_t * wvtp_find_context(int unused)
{
	int i;
	for(i=0; i< wvtp_max_num; i++)
	{
		if(wvtp_context[i].remote_port == g_server_port &&
				ip_addr_cmp(&wvtp_context[i].remote_ip,&g_server_ip))
			{
				return &wvtp_context[i];
			}
	}
	for(i=0; i< wvtp_max_num; i++)
	{
		if(wvtp_context[i].remote_port == 0 )
		{
			debug_printf("index:%d, remote port=%x\n", i, g_server_port);
			wvtp_context[i].remote_port = g_server_port;
			ip_addr_set(&wvtp_context[i].remote_ip,&g_server_ip);
			wvtp_init_context(&wvtp_context[i], wvtp_context[i].sendbuf_cnt);
			return &wvtp_context[i];
		}
	}
	return NULL;
}
void * wvtp_new_context(int unused)
{
	int i;
	for(i=0; i< wvtp_max_num; i++)
	{
		if(wvtp_context[i].remote_port == g_server_port &&
				ip_addr_cmp(&wvtp_context[i].remote_ip,&g_server_ip))
			{
				wvtp_cancel_context(&wvtp_context[i]);
				wvtp_context[i].remote_port = g_server_port;
				ip_addr_set(&wvtp_context[i].remote_ip,&g_server_ip);
				wvtp_init_context(&wvtp_context[i],wvtp_context[i].sendbuf_cnt);
				return &wvtp_context[i];
			}
	}
	for(i=0; i< wvtp_max_num; i++)
	{
		if(wvtp_context[i].remote_port == 0 )
		{
			debug_printf("remote port=%x\n",g_server_port);
			wvtp_context[i].remote_port = g_server_port;
			ip_addr_set(&wvtp_context[i].remote_ip,&g_server_ip);
			wvtp_init_context(&wvtp_context[i],wvtp_context[i].sendbuf_cnt);
			return &wvtp_context[i];
		}
	}
	return NULL;
}
void wvtp_send(wvtp_context_t *pcontext, u8 type, u8 seq_tosend, u8 seq_expected)
{
	struct pbuf *wvtp_pbuf;
	wvtp_pbuf = pbuf_alloc(PBUF_TRANSPORT, 1500, PBUF_RAM);

	if (!wvtp_pbuf) {
		syslog(LOG_ERR, "wvtp_send get buffer failed\n");
		return;
	}

	s8 index;
	u32 len;
	u8 * pdata = (u8 *)wvtp_pbuf->payload;
	WVTP_HEAD * pwvtp_head = (WVTP_HEAD *)wvtp_pbuf->payload;
	pwvtp_head->type = type;
	if(type == ALDG_DATA_BYTIMEOUT ||type == ALDG_DATA_BYNAK)
		pwvtp_head->type  = ALDG_DATA;
	pwvtp_head->sequence = seq_tosend;
	pwvtp_head->acknum = seq_expected;
	pwvtp_head->flowid = WVTP_MAGIC_NUM;
	pdata += WVTP_HEAD_LEN;
	len = WVTP_HEAD_LEN;

	index = seq_tosend%pcontext->NR_BUFS;
	if(type == ALDG_DATA||type == ALDG_DATA_BYTIMEOUT ||type == ALDG_DATA_BYNAK)
	{

		len += (*pcontext->make_frame_from_app_sbuf)(pcontext, pdata, index);
	}

	if(type == ALDG_NAK)
		pcontext->no_nak = FALSE;

	if (len > 1500)
		syslog(LOG_ERR, "wvtp_send send too big package\n");

	pbuf_realloc(wvtp_pbuf, len);

	udp_sendto(wvtp_pcb, wvtp_pbuf, &pcontext->remote_ip, pcontext->remote_port);

	pbuf_free(wvtp_pbuf);

	if(type == ALDG_DATA||type == ALDG_DATA_BYTIMEOUT ||type == ALDG_DATA_BYNAK)
	{
		wvtp_timer_insert(pcontext, seq_tosend,len,type);
	}

	//wvtp_timer_cancel(ACK_TIME_INDEX, TIME_TYPE_ACK);

	return;
}


//#define MAX_SBUF_CHANGE
extern u32	sbufresendnum ;
s8 wvtp_send_data(wvtp_context_t *pcontext)
{
	//WVTP_DEBUG("wvtp_send_data nbuffered=%d\n",pcontext->nbuffered);

#ifdef MAX_SBUF_CHANGE
//	max_buffer = NR_BUFS >= 2*sbufresendnum ? (NR_BUFS - 2*sbufresendnum ) : 0;
	pcontext->max_buffer = pcontext->sbufresendnum > 1 ? 1 : pcontext->NR_BUFS;
	if(pcontext->max_buffer < pcontext->NR_BUFS)
		pcontext->max_buffer++;
#endif

	if(pcontext->nbuffered < pcontext->max_buffer)
	{
		if((*pcontext->app_sbuf_fill)(pcontext, pcontext->wvtp_next_seq_to_send % pcontext->NR_BUFS) != -1)
		{
#ifdef FLOW_MONITOR_ENABLE
			static int video_start = 0;
		
			if(video_start == 0){
				memset(&g_flow, 0, sizeof(g_flow));
				g_flow.start_time = ticks;
				video_start = 1;
			}
			u32 seq_to_send = pcontext->wvtp_next_seq_to_send % pcontext->NR_BUFS;
			u32 len = pcontext->out_buf[seq_to_send].datahead_len + pcontext->out_buf[seq_to_send].data_len;
			if(len == 0){
				debug_printf("ERROR: suppose impossible.\n");
			}
		
			g_flow.bytes_cnt += len;
			if(pcontext->out_buf[seq_to_send].type == 3){ //EM_FLAG_VIDEO_TAIL 
				g_flow.frames_cnt++;
			}
		
			if(ticks - g_flow.start_time > FLOW_CHECK_PERIOD){
				int period_s = (FLOW_CHECK_PERIOD * 10)/1000;	//period in seconds
				debug_printf("###	fps:%d. bitrate:%d.	 ###\n",g_flow.frames_cnt/period_s, 8*g_flow.bytes_cnt/period_s);
				memset(&g_flow, 0, sizeof(g_flow));
				g_flow.start_time = ticks;
			}
#endif
			WVTP_DEBUG("get data seq = %d, addr=0x%x \n",pcontext->wvtp_next_seq_to_send,pcontext->out_buf[pcontext->wvtp_next_seq_to_send % pcontext->NR_BUFS].data_ptr);
			WVTP_DEBUG("send data seqnum=%d, acknum=%d\n",pcontext->wvtp_next_seq_to_send,pcontext->wvtp_seq_expected);
			wvtp_send(pcontext, ALDG_DATA, pcontext->wvtp_next_seq_to_send, pcontext->wvtp_seq_expected);

			pcontext->nbuffered++;
			inc(pcontext->wvtp_next_seq_to_send);
			pcontext->sendpkgnum++;
			return 0;
		}else
			pcontext->getfailnum++;


	}else
	{
		pcontext->buffullnum++;
	}
	return -1;
}


static u8_t wvtp_parse_msg(wvtp_context_t *pcontext, struct pbuf *p)
{
	u8 acknum,seqnum,type;
	int ret = 0;
	int i;
	PWVTP_HEAD pwvtp_head = ( PWVTP_HEAD) p->payload;
	u8 * pdata = (u8 * ) p->payload;
	if(pwvtp_head->flowid != WVTP_MAGIC_NUM)
		return 0;
	pdata += WVTP_HEAD_LEN;
	acknum = pwvtp_head->acknum;
	seqnum = pwvtp_head->sequence;
	type = pwvtp_head->type;

	dec(acknum);
	wvtp_timer_cal(pcontext,acknum);
	pcontext->ack_rcved = acknum;
	while (between(pcontext->wvtp_ack_expected, acknum, pcontext->wvtp_next_seq_to_send)) {
		pcontext->nbuffered = pcontext->nbuffered - 1;	/* handle piggybacked ack */

		WVTP_DEBUG("release seq %d, addr=0x%x\n",pcontext->wvtp_ack_expected,pcontext->out_buf[pcontext->wvtp_ack_expected % pcontext->NR_BUFS].data_ptr);
		wvtp_timer_cancel(pcontext,pcontext->wvtp_ack_expected);	/* frame arrived intact */
		if(pcontext->app_sbuf_release)
			(*pcontext->app_sbuf_release)(pcontext, pcontext->wvtp_ack_expected % pcontext->NR_BUFS);

		inc(pcontext->wvtp_ack_expected);	/* advance lower edge of sender's window */
	}

	if(type == ALDG_DATA)
	{
		//debug_printf("seq %d ,wvtp_seq_expected %d, wvtp_too_far %d\n",seqnum, pcontext->wvtp_seq_expected,pcontext->wvtp_too_far);
//		if((seqnum != pcontext->wvtp_seq_expected) && pcontext->no_nak)
//		{
//			wvtp_send(pcontext, ALDG_NAK, pcontext->wvtp_seq_expected, pcontext->wvtp_seq_expected);
//		}else{
//			//wvtp_timer_insert(ACK_TIME_INDEX ,TIME_TYPE_ACK, ack_timeout);
//		}
		if( between(pcontext->wvtp_seq_expected, seqnum, pcontext->wvtp_too_far) &&
				pcontext->arrived[seqnum % NR_BUFR] == FALSE)
		{
			/* Frames may be accepted in any order. */
			pcontext->arrived[seqnum % NR_BUFR] = TRUE;	/* mark buffer as full */
			(*pcontext->save_frame_to_app_rbuf)(pcontext, pdata, p->tot_len, seqnum % NR_BUFR);/* insert data into buffer */
			if(pcontext->arrived[pcontext->wvtp_seq_expected % NR_BUFR])
			{
				while (pcontext->arrived[pcontext->wvtp_seq_expected % NR_BUFR]) {
					/* Pass frames and advance window. */

					if(pcontext->app_rbuf_handle)
						ret = (*pcontext->app_rbuf_handle)(pcontext, pcontext->wvtp_seq_expected % NR_BUFR);
					else
						ret = 1;
					pcontext->arrived[pcontext->wvtp_seq_expected % NR_BUFR] = FALSE;
					if(ret <= 0)
					{
						debug_printf("data handle return error\n");
						break;
					}
					pcontext->no_nak = TRUE;
					inc(pcontext->wvtp_seq_expected);	/* advance lower edge of receiver's window */
					inc(pcontext->wvtp_too_far);	/* advance upper edge of receiver's window */
					if(pcontext->arrived[pcontext->wvtp_seq_expected % NR_BUFR]==FALSE)
					{
						WVTP_DEBUG("send ack acknum=%d\n",pcontext->wvtp_seq_expected);
						wvtp_send(pcontext, ALDG_ACK, 0, pcontext->wvtp_seq_expected );
					}
					//wvtp_timer_insert(ACK_TIME_INDEX ,TIME_TYPE_ACK, ack_timeout);	/* to see if (a separate ack is needed */
				}
			}
			else
				wvtp_send(pcontext, ALDG_NAK, pcontext->wvtp_seq_expected, pcontext->wvtp_seq_expected);
		}
	}else{
		if(type == ALDG_ACK)
			WVTP_DEBUG("recv ACK acknum=%d\n",acknum);
		else if(type == ALDG_NAK)
		{
			pcontext->NAK_FLAG = 1;
			//recvnaknum++;
			//debug_printf("recv NAK seqnum=%d, acknum=%d\n",seqnum,acknum);
		}
	}

	if(type == ALDG_NAK && between(pcontext->wvtp_ack_expected, (seqnum)%(MAX_SEQ+1), pcontext->wvtp_next_seq_to_send))
	{
		//debug_printf("resend(nak) seqnum=%d, acknum=%d\n",(seqnum)%(MAX_SEQ+1),wvtp_seq_expected);
		wvtp_send(pcontext, ALDG_DATA_BYNAK, (seqnum)%(MAX_SEQ+1), pcontext->wvtp_seq_expected);
		pcontext->recvnaknum++;

	}

	WVTP_DEBUG(" wvtp_ack_expected=%d, wvtp_next_seq_to_send=%d\n",pcontext->wvtp_ack_expected, pcontext->wvtp_next_seq_to_send);

  return 1;
}

static s8 wvtp_check_timer(wvtp_context_t *pcontext)
{
	u8 new;
	s8 ret;
	ret = wvtp_timer_check_expired(pcontext, &new);
	if(ret != -1)
	{
		if(new <= MAX_SEQ)
		{
			//debug_printf("timeout resend %d, wvtp_next_seq_to_send=%d\n",new,wvtp_next_seq_to_send);
			WVTP_DEBUG("resend(timeout) seqnum=%d, acknum=%d\n",new,pcontext->wvtp_seq_expected);
			wvtp_send(pcontext, ALDG_DATA_BYTIMEOUT, new, pcontext->wvtp_seq_expected);
		}
	}

	return ret;
}
int wvtp_context_isfull(void)
{
	int i;
	int ret=1;
	for(i=0; i< wvtp_max_num; i++)
	{
		ret &= wvtp_context[i].remote_port?1:0;
	}
	return ret;
}
wvtp_context_t * wvtp_poll_context(void)
{
	static int index = 0;
	int i;
	i=index;
	do{
		i=(i+1)%wvtp_max_num;
		if(wvtp_context[i].remote_port!=0)
		{
			index = i;
			return &wvtp_context[i];
		}

	}while(i!=index);
	return NULL;
}

int wvtp_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port)
{
	wvtp_context_t *pcontext;

	if(is_wvtp_msg(p) == 0)
		return 0;
	//WVTP_DEBUG("wvtp_call get event_newdata\n");
	pcontext = wvtp_find_context(0);
	if(pcontext == NULL)
		return 0;

	pthread_mutex_lock(&g_mutex);
	pcontext->time = clock_time();

	if (wvtp_parse_msg(pcontext, p)==0) {
		pthread_mutex_unlock(&g_mutex);
		return 0;
	} else {
		pthread_mutex_unlock(&g_mutex);
		return 1;
	}
}

int wvtp_poll()
{
	wvtp_context_t *pcontext;
	pcontext = wvtp_poll_context();

	if(pcontext == NULL)
	{
		return 1;
	}

	pthread_mutex_lock(&g_mutex);

	s32 time = clock_time() - pcontext->time;
	if(time > (s32)link_timeout)
	{
		debug_printf("wvtp_cancel_context %x\n",pcontext);
		wvtp_cancel_context(pcontext);
		wvtp_exit = 1;
		pthread_mutex_unlock(&g_mutex);
		return 1;
	}
	pcontext->pollnum++;
	//WVTP_DEBUG("wvtp_call get event_poll\n");

	if( wvtp_send_data(pcontext) != -1 )
	{
		//debug_printf("wvtp_call get timer out\n");
		pthread_mutex_unlock(&g_mutex);
		return 1;
	}
	wvtp_check_timer(pcontext);
	pthread_mutex_unlock(&g_mutex);
	return 1;
}
