#include "includes.h"
#include "wvtp.h"
#include "wvtp_app.h"

WVTP_HANDLER wvtp_handler;


u32 wvtp_app_make_frame(void * handle, u8 * pdata, u8 index)
{
	wvtp_context_t * pcontext = (wvtp_context_t *)handle;
	u32	len=0;
	if(pcontext->out_buf[index].datahead_len > 0)
	{
		memcpy( pdata , pcontext->out_buf[index].datahead, pcontext->out_buf[index].datahead_len);
		len = pcontext->out_buf[index].datahead_len;
		pdata += pcontext->out_buf[index].datahead_len;
	}
	if(pcontext->out_buf[index].data_len > 0)
	{
		memcpy( pdata ,(char *)(pcontext->out_buf[index].data_ptr),pcontext->out_buf[index].data_len );
		len += pcontext->out_buf[index].data_len;
	}
	return len;
}

u32 wvtp_app_save_frame(void * handle, u8 * pdata, u32 len, u8 index)
{
	wvtp_context_t * pcontext = (wvtp_context_t *)handle;
	if( len > MAX_SEND_DATA_LEN )
		len = MAX_SEND_DATA_LEN;
	memcpy( pcontext->in_buf[index%NR_BUFR].data, pdata, len);
	pcontext->in_buf[index%NR_BUFR].len = len;
	return len;
}

int wvtp_app_rbuf_handle(void * handle, u8 buf_index)
{
	wvtp_context_t * pcontext = (wvtp_context_t *)handle;
	pcontext->recvdatalen +=pcontext->in_buf[buf_index%NR_BUFR].len;
	if(wvtp_handler.cb_inbuf_handle)
		return wvtp_handler.cb_inbuf_handle(pcontext->parg, (char *)pcontext->in_buf[buf_index%NR_BUFR].data, pcontext->in_buf[buf_index%NR_BUFR].len);
	else
		return 1;
}

s8 wvtp_app_sbuf_fill_data(void * handle, u8 buf_index)
{
	wvtp_context_t * pcontext = (wvtp_context_t *)handle;
	int ret = -1;
	pcontext->out_buf[buf_index].datahead_len=DATA_HEAD_LEN;
//	ret = 1;
	if(wvtp_handler.cb_outbuf_get)
		ret = wvtp_handler.cb_outbuf_get(pcontext->parg, pcontext->out_buf[buf_index].datahead, &pcontext->out_buf[buf_index].datahead_len, &pcontext->out_buf[buf_index].data_ptr , &pcontext->out_buf[buf_index].data_len , MAX_SEND_DATA_LEN, &pcontext->out_buf[buf_index].type, &pcontext->out_buf[buf_index].rel);

	if(ret)
	{
//		debug_printf("len=%d\n",out_buf[buf_index].data_len);
		return ret; //buf_index;
	}
	else
	{
//		uart_putc('N');
	}

	return -1;
}

void wvtp_app_sbuf_release_data(void * handle, u8 buf_index)
{
	wvtp_context_t * pcontext = (wvtp_context_t *)handle;
	if(wvtp_handler.cb_outbuf_release)
		wvtp_handler.cb_outbuf_release(pcontext->parg, pcontext->out_buf[buf_index].data_ptr,pcontext->out_buf[buf_index].type, pcontext->out_buf[buf_index].rel);
}


