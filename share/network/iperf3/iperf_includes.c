#include "iperf_includes.h"

/*
void* malloc(size_t size)
{
	return (void*) ertos_malloc(size);
}

void* realloc(void* ptr, size_t newsize)
{
	ertos_free(ptr);
	return (void*) ertos_malloc(newsize);
}

void* calloc(size_t numElements, size_t sizeOfElement)
{
	size_t size=(numElements*sizeOfElement);
	void* ptr=ertos_malloc(size);
	memset(ptr,0,size);
	return ptr;
}
*/

#ifndef CONFIG_SIMU

int getrusage(int who, struct rusage *usage)
{
 	memset(usage,0,sizeof(struct rusage));
	return 0;
}

void gethostname(char *name, size_t len)
{
	struct netif * net_if = netif_find("wl0");
	u32 ip_addr = ntohl(*(u32 *)(&net_if->ip_addr));
/*	
	sprintf(name,"%d.%d.%d.%d",ip_addr&0xff,(ip_addr>>8)&0xff,
		(ip_addr>>16)&0xff,(ip_addr>>24)&0xff);
*/
	sprintf(name,"%d.%d.%d.%d",(ip_addr>>24)&0xff,(ip_addr>>16)&0xff,
		(ip_addr>>8)&0xff,ip_addr&0xff);
}


#define xchar(i)             ((i) < 10 ? '0' + (i) : 'A' + (i) - 10)

char *
ip6addr_ntoa_r(const ip6_addr_t *addr, char *buf, int buflen)
{
  u32_t current_block_index, current_block_value, next_block_value;
  s32_t i;
  u8_t zero_flag, empty_block_flag;

  i = 0;
  empty_block_flag = 0; /* used to indicate a zero chain for "::' */

  for (current_block_index = 0; current_block_index < 8; current_block_index++) {
    /* get the current 16-bit block */
    current_block_value = lwip_htonl(addr->addr[current_block_index >> 1]);
    if ((current_block_index & 0x1) == 0) {
      current_block_value = current_block_value >> 16;
    }
    current_block_value &= 0xffff;

    /* Check for empty block. */
    if (current_block_value == 0) {
      if (current_block_index == 7 && empty_block_flag == 1) {
        /* special case, we must render a ':' for the last block. */
        buf[i++] = ':';
        if (i >= buflen) {
          return NULL;
        }
        break;
      }
      if (empty_block_flag == 0) {
        /* generate empty block "::", but only if more than one contiguous zero block,
         * according to current formatting suggestions RFC 5952. */
        next_block_value = lwip_htonl(addr->addr[(current_block_index + 1) >> 1]);
        if ((current_block_index & 0x1) == 0x01) {
            next_block_value = next_block_value >> 16;
        }
        next_block_value &= 0xffff;
        if (next_block_value == 0) {
          empty_block_flag = 1;
          buf[i++] = ':';
          if (i >= buflen) {
            return NULL;
          }
          continue; /* move on to next block. */
        }
      } else if (empty_block_flag == 1) {
        /* move on to next block. */
        continue;
      }
    } else if (empty_block_flag == 1) {
      /* Set this flag value so we don't produce multiple empty blocks. */
      empty_block_flag = 2;
    }

    if (current_block_index > 0) {
      buf[i++] = ':';
      if (i >= buflen) {
        return NULL;
      }
    }

    if ((current_block_value & 0xf000) == 0) {
      zero_flag = 1;
    } else {
      buf[i++] = xchar(((current_block_value & 0xf000) >> 12));
      zero_flag = 0;
      if (i >= buflen) {
        return NULL;
      }
    }

    if (((current_block_value & 0xf00) == 0) && (zero_flag)) {
      /* do nothing */
    } else {
      buf[i++] = xchar(((current_block_value & 0xf00) >> 8));
      zero_flag = 0;
      if (i >= buflen) {
        return NULL;
      }
    }

    if (((current_block_value & 0xf0) == 0) && (zero_flag)) {
      /* do nothing */
    }
    else {
      buf[i++] = xchar(((current_block_value & 0xf0) >> 4));
      zero_flag = 0;
      if (i >= buflen) {
        return NULL;
      }
    }

    buf[i++] = xchar((current_block_value & 0xf));
    if (i >= buflen) {
      return NULL;
    }
  }

  buf[i] = 0;

  return buf;
}

void iperf_wait_ticks(unsigned int wait_ticks)
{
	t_ertos_eventhandler wait_evt;
	wait_evt=ertos_event_create();
	ertos_event_wait(wait_evt, wait_ticks);
	ertos_event_delete(wait_evt);
}

#endif //!CONFIG_SIMU

void (*signal(int signo, void (*func)(int)))(int)
{


}

int getopt_long_iperf(int argc, char *const *argv, const char *shortopts,
                const struct option *longopts, int *longind,const char **optarg)
{
	int flag_iperf=-1;
	*optarg=0;
	while((*longind)<argc){
		if(argv[(*longind)][0]=='-')
		{
			flag_iperf=argv[(*longind)][1];
			++(*longind);
			if((*longind)<argc)
			{
				if(argv[(*longind)][0]!='-')
				{
					*optarg=argv[(*longind)];
					++(*longind);
				}			
			}

			while(longopts->name)
			{
				if(longopts->val==flag_iperf)
				{
					if(longopts->has_arg==no_argument)
					{
						return (*optarg==NULL)?flag_iperf:-1;
					}
					if(longopts->has_arg==required_argument)
					{
						return (*optarg==NULL)?-1:flag_iperf;
					}
				}
				longopts++;
			};
			return -1;
		}
		else
		{
			++(*longind);
		}
	}
	return -1;
}


void dump_binary(void *buf, int len)
{

	int i,j;
	unsigned char* sz = (unsigned char* )buf;
	unsigned char c = 0;
//	debug_printf("dumpbuf:0x%x len:%d\n",buf,len);
	unsigned char tmp_sz[64];
	int tmp_len;
	
	memset(tmp_sz,0,sizeof(tmp_sz));
	for(i=0; i < len; i++)
	{
		if(i&&((i&0x0f)==0))
		{
			debug_printf("%s\n",tmp_sz);
			memset(tmp_sz,0,sizeof(tmp_sz));
			if(i <=0xffffffff && i>=0xfffffff)
				sprintf((char*)tmp_sz,"%x ",i);
			if(i <=0xfffffff && i>=0xffffff)
				sprintf((char*)tmp_sz,"0%x ",i);
			if(i <=0xffffff && i>=0xfffff)
				sprintf((char*)tmp_sz,"00%x ",i);
			if(i <=0xfffff && i>=0xffff)
				sprintf((char*)tmp_sz,"000%x ",i);
			if(i <=0xffff && i>=0xfff)
				sprintf((char*)tmp_sz,"0000%x ",i);
			if(i <=0xfff && i>=0xff)
				sprintf((char*)tmp_sz,"00000%x ",i);
			if(i <=0xff && i>=0xf)
				sprintf((char*)tmp_sz,"000000%x ",i);
			if(i <=0xf)
				sprintf((char*)tmp_sz,"0000000%x ",i);
		}
		
		tmp_len=strlen((char*)tmp_sz);
		c=sz[i];
		if ((c&0xf0)==0)
		{
			tmp_sz[tmp_len++]='0';
			if (c>=0x0a)
				tmp_sz[tmp_len++]=c-10+'a';
			else
				tmp_sz[tmp_len++]=c+'0';
			tmp_sz[tmp_len++]=' ';
		}
		else
		{
			c=(sz[i]&0xf0)>>4;
			if (c>=0x0a)
				tmp_sz[tmp_len++]=c-10+'a';
			else
				tmp_sz[tmp_len++]=c+'0';
			c=sz[i]&0x0f;
			if (c>=0x0a)
				tmp_sz[tmp_len++]=c-10+'a';
			else
				tmp_sz[tmp_len++]=c+'0';
			tmp_sz[tmp_len++]=' ';
		}
	}
	debug_printf("%s\n",tmp_sz);
}



