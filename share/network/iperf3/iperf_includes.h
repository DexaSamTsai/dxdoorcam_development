#ifndef __IPERF_INCLIDES_H___
#define __IPERF_INCLIDES_H___

//#define iperf_printf debug_printf
#define iperf_printf(msg...) //
#define DBL_EPSILON        2.2204460492503131E-016
#define	TCP_MAXSEG	 2	/* Set maximum segment size  */
#define TCP_CONGESTION	 13	/* Congestion control algorithm.  */

void dump_binary(void *buf, int len);

#define iperf_err(f,msg...) \
	do{ \
		debug_printf(msg);\
	} while(0)

#define iperf_errexit(f,msg...) \
	do{ \
		debug_printf(msg);\
		debug_printf("%s %d exit!\n",__FUNCTION__,__LINE__);\
		return 1;\
	} while(0)


#ifdef CONFIG_SIMU

#include <stdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <linux/types.h>
#include <sys/select.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <sys/time.h>
#include <sys/select.h>
#include <netinet/tcp.h>
#include <sys/resource.h>
#include <sys/mman.h>
#include <math.h>
#include <getopt.h>


#include "iperf_api.h"
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned char u8;
typedef int s32;
typedef short s16;
typedef char s8;

#else

#include "includes.h"
#include "lwip/sockets.h"
#include "lwip/inet.h"
#include "lwipopts.h"

typedef unsigned int __u32;
typedef unsigned short __u16;
typedef unsigned char __u8;
typedef int __s32;
typedef short __s16;
typedef char __s8;

typedef signed long int int64_t;
typedef unsigned long long uint64_t;

/*
#define exit(x) \
	do{ \
		debug_printf("%s %d exit!\n",__FUNCTION__,__LINE__);\
		return x;\
	} while(0)
*/

#define exit(x) \
	do{ \
		return x;\
	} while(0)

#define fprintf(f,msg...) \
	do{ \
		debug_printf(msg);\
	} while(0)

extern void* malloc(size_t size);
extern void* realloc(void* ptr, size_t newsize);  
extern void* calloc(size_t numElements, size_t sizeOfElement); 

/* Signals.  */
#define	SIGHUP		1	/* Hangup (POSIX).  */
#define	SIGINT		2	/* Interrupt (ANSI).  */
#define	SIGQUIT		3	/* Quit (POSIX).  */
#define	SIGILL		4	/* Illegal instruction (ANSI).  */
#define	SIGTRAP		5	/* Trace trap (POSIX).  */
#define	SIGABRT		6	/* Abort (ANSI).  */
#define	SIGIOT		6	/* IOT trap (4.2 BSD).  */
#define	SIGBUS		7	/* BUS error (4.2 BSD).  */
#define	SIGFPE		8	/* Floating-point exception (ANSI).  */
#define	SIGKILL		9	/* Kill, unblockable (POSIX).  */
#define	SIGUSR1		10	/* User-defined signal 1 (POSIX).  */
#define	SIGSEGV		11	/* Segmentation violation (ANSI).  */
#define	SIGUSR2		12	/* User-defined signal 2 (POSIX).  */
#define	SIGPIPE		13	/* Broken pipe (POSIX).  */
#define	SIGALRM		14	/* Alarm clock (POSIX).  */
#define	SIGTERM		15	/* Termination (ANSI).  */
#define	SIGSTKFLT	16	/* Stack fault.  */
#define	SIGCLD		SIGCHLD	/* Same as SIGCHLD (System V).  */
#define	SIGCHLD		17	/* Child status has changed (POSIX).  */
#define	SIGCONT		18	/* Continue (POSIX).  */
#define	SIGSTOP		19	/* Stop, unblockable (POSIX).  */
#define	SIGTSTP		20	/* Keyboard stop (POSIX).  */
#define	SIGTTIN		21	/* Background read from tty (POSIX).  */
#define	SIGTTOU		22	/* Background write to tty (POSIX).  */
#define	SIGURG		23	/* Urgent condition on socket (4.2 BSD).  */
#define	SIGXCPU		24	/* CPU limit exceeded (4.2 BSD).  */
#define	SIGXFSZ		25	/* File size limit exceeded (4.2 BSD).  */
#define	SIGVTALRM	26	/* Virtual alarm clock (4.2 BSD).  */
#define	SIGPROF		27	/* Profiling alarm clock (4.2 BSD).  */
#define	SIGWINCH	28	/* Window size change (4.3 BSD, Sun).  */
#define	SIGPOLL		SIGIO	/* Pollable event occurred (System V).  */
#define	SIGIO		29	/* I/O now possible (4.2 BSD).  */
#define	SIGPWR		30	/* Power failure restart (System V).  */
#define SIGSYS		31	/* Bad system call.  */
#define SIGUNUSED	31

#define PROT_READ	0x1		/* Page can be read.  */
#define PROT_WRITE	0x2		/* Page can be written.  */
#define PROT_EXEC	0x4		/* Page can be executed.  */
#define PROT_NONE	0x0		/* Page can not be accessed.  */
#define PROT_GROWSDOWN	0x01000000	/* Extend change to start of
					   growsdown vma (mprotect only).  */
#define PROT_GROWSUP	0x02000000	/* Extend change to start of
					   growsup vma (mprotect only).  */

/* Sharing types (must choose one and only one of these).  */
#define MAP_SHARED	0x01		/* Share changes.  */
#define MAP_PRIVATE	0x02		/* Changes are private.  */
#define MAP_FAILED	((void *) -1)

struct rusage {
  struct timeval ru_utime; /* user time used */
  struct timeval ru_stime; /* system time used */
  long ru_maxrss; /* maximum resident set size */
  long ru_ixrss; /* integral shared memory size */
  long ru_idrss; /* integral unshared data size */
  long ru_isrss; /* integral unshared stack size */
  long ru_minflt; /* page reclaims */
  long ru_majflt; /* page faults */
  long ru_nswap; /* swaps */
  long ru_inblock; /* block input operations */
  long ru_oublock; /* block output operations */
  long ru_msgsnd; /* messages sent */
  long ru_msgrcv; /* messages received */
  long ru_nsignals; /* signals received */
  long ru_nvcsw; /* voluntary context switches */
  long ru_nivcsw; /* involuntary context switches */
 };

#define   RUSAGE_SELF     0
#define   RUSAGE_CHILDREN     -1

int getrusage(int who, struct rusage *usage);

void gethostname(char *name, size_t len);

void (*signal(int signo, void (*func)(int)))(int);

extern char *ip6addr_ntoa_r(const ip6_addr_t *addr, char *buf, int buflen);

#define no_argument     0
#define required_argument   1
#define optional_argument   2

struct option
{
  const char *name;
  /* has_arg can't be an enum because some compilers complain about
     type mismatches in all the code that assumes it is an int.  */
  int has_arg;
  int *flag;
  int val;
};

extern int getopt_long_iperf(int argc, char *const *argv, const char *shortopts,
                const struct option *longopts, int *longind, const char** optarg);


#define RUN_WAIT_THREAD_TICKS (3)
void iperf_wait_ticks(unsigned int wait_ticks);
int iperf_cmd_msg(int argc, char ** argv);

#endif //!CONFIG_SIMU
#endif //!__IPERF_INCLIDES_H___
