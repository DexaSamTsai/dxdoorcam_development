#include "iperf_includes.h"

#ifndef CONFIG_SIMU

typedef struct s_iperf_thread_cfg
{
	u8 iperf_thread_priority;
	u8 iperf_thread_stack[4096];
	t_ertos_eventhandler 	evt_init;	
	t_ertos_eventhandler 	evt_done;
	t_ertos_queuehandler 	queue;
}__attribute__((packed)) t_iperf_thread_cfg;

typedef struct s_iperf_thread_msg
{
	int argc;
	char **argv;
}__attribute__((packed)) t_iperf_thread_msg;

//////////////////////////////////////////////////
static t_iperf_thread_cfg g_iperf_thread_cfg={0};

extern int iperf_main(int argc, char **argv);
void iperf_thread_loop(u8*  arg)
{	
	pthread_detach(pthread_self());
	pthread_setname(NULL, __FUNCTION__);
	
	t_iperf_thread_cfg* cfg = (t_iperf_thread_cfg*) arg;
	
	if(cfg->queue== NULL){
		cfg->queue  = ertos_queue_create(1, sizeof(t_iperf_thread_msg));	
		if(cfg->queue == NULL){
			debug_printf("[DSIF] %d failure\n",__FUNCTION__);
			return;
		}
	}
	
	if(cfg->evt_init){
		ertos_event_signal(cfg->evt_init);
	}
	
	t_iperf_thread_msg msg;
	while(1)
	{
		memset(&msg,0,sizeof(t_iperf_thread_msg));
		ertos_queue_recv(cfg->queue, &msg, WAIT_INF);
		if(cfg->evt_done)
		{
			iperf_main(msg.argc,msg.argv);
			ertos_event_wait(cfg->evt_done, WAIT_INF);
			ertos_event_delete(cfg->evt_done);
			cfg->evt_done=0;
		}
	}
}

int iperf_thread_init(t_iperf_thread_cfg* cfg)
{
	u32 thread_ret;
	u32 thread_priority=cfg->iperf_thread_priority;
	u8* thread_stack=cfg->iperf_thread_stack;
	u32 thread_size=sizeof(cfg->iperf_thread_stack);

	if(cfg->evt_init==0){
		cfg->evt_init = ertos_event_create();
	}

	pthread_attr_t thread_addr;
	pthread_t thread;

	pthread_attr_init(&thread_addr);
	pthread_attr_setstack(&thread_addr, thread_stack, thread_size);
	thread_addr.schedparam.sched_priority = thread_priority;
	thread_ret = pthread_create(&thread, &thread_addr, iperf_thread_loop, cfg);
	if(thread_ret != 0){
		debug_printf("%s failure \n",__FUNCTION__);
		return -1;
	}

	if(cfg->evt_init){
		ertos_event_wait(cfg->evt_init, WAIT_INF);
		ertos_event_delete(cfg->evt_init);
		cfg->evt_init=0;
	}
	return 0;
}

u32 iperf_thread_send(int argc, char ** argv)
{
	t_iperf_thread_cfg* cfg=&g_iperf_thread_cfg;
	if(cfg->evt_done)
	{
		debug_printf("iperf3: previous cmd not done\n");
		return 1;
	}
	cfg->evt_done=ertos_event_create();

	t_iperf_thread_msg msg;
	memset(&msg,0,sizeof(t_iperf_thread_msg));
	msg.argc=argc;
	msg.argv=argv;

	u32 irqflags;
	local_irq_save(irqflags);
	ertos_queue_send(cfg->queue, &msg, WAIT_INF);
	local_irq_restore(irqflags);

	ertos_event_signal(cfg->evt_done);
	
	return 0;
}

int iperf_cmd_msg(int argc, char ** argv)
{
	t_iperf_thread_cfg* cfg=&g_iperf_thread_cfg;
	if(cfg->queue==0)
	{
		cfg->iperf_thread_priority=9;
		if(iperf_thread_init(cfg)!=0)
		{
			debug_printf("%s %d error\n",__FUNCTION__,__LINE__);
			return -1;
		}
	}
	return iperf_thread_send(argc,argv);
}

#endif //!CONFIG_SIMU

