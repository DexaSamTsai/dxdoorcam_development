
#include "iperf.h"
#include "iperf_config.h"
#include "net.h"
#include "iperf_api.h"
#include "iperf_udp.h"
#include "iperf_tcp.h"

int fmap_init(char* sname,int blksize,char** buffer)
{
	*buffer=(char*)malloc(blksize);
	if(*buffer==0)
		return -1;
	memset(*buffer,0,blksize);
	return 0;
}

void fmap_uninit(char* buffer,int blksize)
{
	free(buffer);
}

/**************************************************************************/
void
iperf_free_stream(struct iperf_stream *sp)
{
	struct iperf_interval_results *irp, *nirp;
    	fmap_uninit(sp->buffer, sp->test->settings->blksize);
	close(sp->buffer_fd);
	if (sp->diskfile_fd >= 0)
		close(sp->diskfile_fd);
	for (irp = TAILQ_FIRST(&sp->result->interval_results); irp != NULL; irp = nirp) {
		nirp = TAILQ_NEXT(irp, irlistentries);
		free(irp);
	}
	free(sp->result);
	if (sp->send_timer != NULL)
		tmr_cancel(sp->send_timer);
	free(sp);
}

/**************************************************************************/
struct iperf_stream *
iperf_new_stream(struct iperf_test *test, int s)
{

    int i;
    struct iperf_stream *sp;
    
    char tmp_template[64];
    if (test->tmp_template) {
        snprintf(tmp_template, sizeof(tmp_template) / sizeof(char), "%s", test->tmp_template);
    } else {
        //char buf[] = "/tmp/iperf3.XXXXXX";
	char buf[] = PACKAGE_STRING;
        snprintf(tmp_template, sizeof(tmp_template) / sizeof(char), "%s", buf);
    }

    sp = (struct iperf_stream *) malloc(sizeof(struct iperf_stream));
    if (!sp) {
        i_errno = IECREATESTREAM;
	iperf_printf("%s %d\n",__FUNCTION__,__LINE__);
        return NULL;
    }

    memset(sp, 0, sizeof(struct iperf_stream));
    sp->test = test;
    sp->settings = test->settings;
    sp->result = (struct iperf_stream_result *) malloc(sizeof(struct iperf_stream_result));
    if (!sp->result) {
        free(sp);
        i_errno = IECREATESTREAM;
	iperf_printf("%s %d\n",__FUNCTION__,__LINE__);
        return NULL;
    }

    memset(sp->result, 0, sizeof(struct iperf_stream_result));
    TAILQ_INIT(&sp->result->interval_results);

#if 0
    /* Create and randomize the buffer */
    sp->buffer_fd = mkstemp(tmp_template);
    if (sp->buffer_fd == -1) {
        i_errno = IECREATESTREAM;
        free(sp->result);
        free(sp);
        return NULL;
    }
    if (unlink(tmp_template) < 0) {
        i_errno = IECREATESTREAM;
        free(sp->result);
        free(sp);
        return NULL;
    }
    if (ftruncate(sp->buffer_fd, test->settings->blksize) < 0) {
        i_errno = IECREATESTREAM;
        free(sp->result);
        free(sp);
        return NULL;
    }
	
    sp->buffer = (char *) mmap(NULL, test->settings->blksize, PROT_READ|PROT_WRITE, MAP_PRIVATE, sp->buffer_fd, 0);
    if (sp->buffer == MAP_FAILED) {
        i_errno = IECREATESTREAM;
        free(sp->result);
        free(sp);
        return NULL;
    }
#endif

	sp->buffer_fd = fmap_init(tmp_template,test->settings->blksize,&sp->buffer);
	if(sp->buffer_fd<0)
	{
		i_errno = IECREATESTREAM;
		free(sp->result);
		free(sp);
		iperf_printf("%s %d\n",__FUNCTION__,__LINE__);
		return NULL;
	}

    srandom(time(NULL));
    for (i = 0; i < test->settings->blksize; ++i)
        sp->buffer[i] = random();

    /* Set socket */
    sp->socket = s;

    sp->snd = test->protocol->send;
    sp->rcv = test->protocol->recv;

    if (test->diskfile_name == (char*) 0)
    {
        sp->diskfile_fd = -1;
    }
    else
    {
    	iperf_printf("%s %d diskfile_name:%s\n",__FUNCTION__,__LINE__,test->diskfile_name);
    }

    /* Initialize stream */
    if (iperf_init_stream(sp, test) < 0) {
        close(sp->buffer_fd);
       fmap_uninit(sp->buffer, sp->test->settings->blksize);
        free(sp->result);
        free(sp);
	iperf_printf("%s %d\n",__FUNCTION__,__LINE__);
        return NULL;
    }
    iperf_add_stream(test, sp);

    return sp;
}

