/*
 * wpa_supplicant/hostapd / Internal implementation of OS specific functions
 * Copyright (c) 2005-2006, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 *
 * This file is an example of operating system specific  wrapper functions.
 * This version implements many of the functions internally, so it can be used
 * to fill in missing functions from the target system C libraries.
 *
 * Some of the functions are using standard C library calls in order to keep
 * this file in working condition to allow the functions to be tested on a
 * Linux target. Please note that OS_NO_C_LIB_DEFINES needs to be defined for
 * this file to work correctly. Note that these implementations are only
 * examples and are not optimized for speed.
 */

#include "includes.h"


#include "os.h"

void os_sleep(os_time_t sec, os_time_t usec)
{
	if(sec){
		sleep(sec);
	}

	if(usec){
		usleep(usec);
	}
}

int os_get_time(struct os_time *t)
{
	struct timeval tv;
	gettimeofday(&tv, NULL);

	t->sec = tv.tv_sec;
	t->usec = tv.tv_usec;

	return 0;
}

int os_mktime(int year, int month, int day, int hour, int min, int sec,
	      os_time_t *t)
{
	return 0;
}


int os_gmtime(os_time_t t, struct os_tm *tm)
{
	return 0;
}

int os_daemonize(const char *pid_file)
{
	return 0;
}


void os_daemonize_terminate(const char *pid_file)
{
	return ;
}

extern u32 getcycle(void);
unsigned long os_random(void)
{
	srand(getcycle());
	return rand();
}

int os_get_random(unsigned char *buf, size_t len)
{
	int i;
	for(i = 0;i < len;i++){
		buf[i] = (u8)os_random();
	}
	return len;
}

char * os_rel2abs_path(const char *rel_path)
{
	return NULL;
}


int os_program_init(void)
{
	return 0;
}


void os_program_deinit(void)
{
}


int os_setenv(const char *name, const char *value, int overwrite)
{
	return 0;
}


int os_unsetenv(const char *name)
{
	return 0;
}


char * os_readfile(const char *name, size_t *len)
{
	return NULL;
}


void * os_zalloc(size_t size)
{
	void *n = os_malloc(size);
	if (n)
		os_memset(n, 0, size);
	return n;
}

size_t os_strlcpy(char *dest, const char *src, size_t siz)
{
	const char *s = src;
	size_t left = siz;

	if (left) {
		/* Copy string up to the maximum size of the dest buffer */
		while (--left != 0) {
			if ((*dest++ = *s++) == '\0')
				break;
		}
	}

	if (left == 0) {
		/* Not enough room for the string; force NUL-termination */
		if (siz != 0)
			*dest = '\0';
		while (*s++)
			; /* determine total src string length */
	}

	return s - src - 1;
}
