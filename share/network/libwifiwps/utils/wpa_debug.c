/*
 * wpa_supplicant/hostapd / Debug prints
 * Copyright (c) 2002-2007, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "wpa_debug.h"

int wpa_debug_level = MSG_INFO;
//int wpa_debug_level = MSG_EXCESSIVE;

int wpa_debug_show_keys = 1;
int wpa_debug_timestamp = 1;

#ifdef CONFIG_DEBUG_FILE
static FILE *out_file = NULL;
#endif /* CONFIG_DEBUG_FILE */

void wpa_debug_print_timestamp(void)
{
	struct os_time tv;
	if (!wpa_debug_timestamp)
		return;
	os_get_time(&tv);
	debug_printf("%d.%d: ",  tv.sec, tv.usec);
}


/**
 * wpa_printf - conditional printf
 * @level: priority level (MSG_*) of the message
 * @fmt: printf format string, followed by optional arguments
 *
 * This function is used to print conditional debugging and error messages. The
 * output may be directed to stdout, stderr, and/or syslog based on
 * configuration.
 *
 * Note: New line '\n' is added to the end of the text when printing to stdout.
 */
void wpa_printf(int level, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	if (level >= wpa_debug_level) {
		wpa_debug_print_timestamp();
		vprintf(fmt, ap);
		debug_printf("\n");
	}
	va_end(ap);
}



static void _wpa_hexdump(int level, const char *title, const u8 *buf,
			 size_t len, int show)
{

	if (level < wpa_debug_level)
		return;
	wpa_debug_print_timestamp();
	debug_printf("%s - hexdump(len=%d):\n", title,  len);
	if (buf == NULL) {
		debug_printf(" [NULL]");
	} else if (show) {
		dumpbuf((void *)buf,len);
	} else {
		debug_printf(" [REMOVED]");
	}
	debug_printf("\n");
}

void wpa_hexdump(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump(level, title, buf, len, 1);
}


void wpa_hexdump_key(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump(level, title, buf, len, wpa_debug_show_keys);
}


static void _wpa_hexdump_ascii(int level, const char *title, const u8 *buf,
			       size_t len, int show)
{

	if (level < wpa_debug_level)
		return;

	wpa_debug_print_timestamp();
	if (!show) {
		debug_printf("%s - hexdump_ascii(len=%d): [REMOVED]\n",
		       title, (unsigned long) len);
		return;
	}
	if (buf == NULL) {
		debug_printf("%s - hexdump_ascii(len=%d): [NULL]\n",
		       title, (unsigned long) len);
		return;
	}
	debug_printf("%s - hexdump_ascii(len=%d):\n", title, (unsigned long) len);
	dumpbuf((void *)buf,len);
}


void wpa_hexdump_ascii(int level, const char *title, const u8 *buf, size_t len)
{
	_wpa_hexdump_ascii(level, title, buf, len, 1);
}


void wpa_hexdump_ascii_key(int level, const char *title, const u8 *buf,
			   size_t len)
{
	_wpa_hexdump_ascii(level, title, buf, len, wpa_debug_show_keys);
}




int wpa_debug_reopen_file(void)
{
	return 0;

}


int wpa_debug_open_file(const char *path)
{
	return 0;
}


void wpa_debug_close_file(void)
{
}


void wpa_msg(void *ctx, int level, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	if (level >= wpa_debug_level) {
		wpa_debug_print_timestamp();
		debug_printf("wpa_msg: ");
		vprintf(fmt, ap);
		debug_printf("\n");
	}
	va_end(ap);
}

void wpa_msg_ctrl(void *ctx, int level, const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	if (level >= wpa_debug_level) {
		wpa_debug_print_timestamp();
		debug_printf("wpa_msg_ctrl: ");
		vprintf(fmt, ap);
		debug_printf("\n");
	}
	va_end(ap);
}

void hostapd_logger(void *ctx, const u8 *addr, unsigned int module, int level,
		    const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	if (level >= wpa_debug_level) {
		wpa_debug_print_timestamp();
		if(addr)
			debug_printf("hostapd_logger: STA " MACSTR " - ",
			   MAC2STR(addr));
		else
			debug_printf("hostapd_logger: ");
		vprintf(fmt, ap);
		debug_printf("\n");
	}
	va_end(ap);
}
