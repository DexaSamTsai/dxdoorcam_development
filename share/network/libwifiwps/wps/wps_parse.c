#include "includes.h"
//#include "libwifi.h"
#include "wpa_supplicant/wps_supplicant.h"

#ifndef CONFIG_WPS_STRICT
#define WPS_WORKAROUNDS
#endif /* CONFIG_WPS_STRICT */

#define WPS_IE_VENDOR_TYPE 0x0050f204

#define WLAN_EID_VENDOR_SPECIFIC 221
u16 wps_parse(const u8 * wpsie, u32 ie_length)
{
	const u8 *pos, *end;
	u8* dev_passwd_id=NULL, *selected_registrar=NULL;
	u16 type, len;
#ifdef WPS_WORKAROUNDS
	u16 prev_type = 0;
#endif /* WPS_WORKAROUNDS */

	pos = wpsie;
	end = pos + ie_length;

	while (pos < end) {
		if (end - pos < 4) {
			debug_printf( "WPS: Invalid message - %d bytes remaining\n", (u32) (end - pos));
			return -1;
		}

		type = WPA_GET_BE16(pos);
		pos += 2;
		len = WPA_GET_BE16(pos);
		pos += 2;
		//debug_printf("WPS: attr type=0x%x len=%d\n", type, len);
		if (len > end - pos) {
			debug_printf("WPS: Attribute overflow\n");
#ifdef WPS_WORKAROUNDS
			/*
			 * Some deployed APs seem to have a bug in encoding of
			 * Network Key attribute in the Credential attribute
			 * where they add an extra octet after the Network Key
			 * attribute at least when open network is being
			 * provisioned.
			 */
			if ((type & 0xff00) != 0x1000 &&
			    prev_type == ATTR_NETWORK_KEY) {
				debug_printf("WPS: Workaround - try to skip unexpected octet after Network Key\n");
				pos -= 3;
				continue;
			}
#endif /* WPS_WORKAROUNDS */
			return -1;
		}

#ifdef WPS_WORKAROUNDS
		if (type == 0 && len == 0) {
			/*
			 * Mac OS X 10.6 seems to be adding 0x00 padding to the
			 * end of M1. Skip those to avoid interop issues.
			 */
			int i;
			for (i = 0; i < end - pos; i++) {
				if (pos[i])
					break;
			}
			if (i == end - pos) {
				debug_printf("WPS: Workaround - skip unexpected message padding\n");
				break;
			}
		}
#endif /* WPS_WORKAROUNDS */

		switch (type) {
		case ATTR_DEV_PASSWORD_ID:
			if (len != 2) {
				debug_printf("WPS: Invalid Device Password ID length %d\n", len);
				return -1;
			}
			dev_passwd_id = (u8*)pos;
			break;
		case ATTR_SELECTED_REGISTRAR:
			if (len != 1) {
				debug_printf("WPS: Invalid Selected Registrar length %d\n", len);
				return -1;
			}
			selected_registrar = (u8*)pos;
			break;
		}

#ifdef WPS_WORKAROUNDS
		prev_type = type;
#endif /* WPS_WORKAROUNDS */
		pos += len;
	}

	if(dev_passwd_id&&selected_registrar&&*selected_registrar!=0)
	{
		if(WPA_GET_BE16(dev_passwd_id)==DEV_PW_PUSHBUTTON)
			return 1;
		else
			return 2;
	}

	return 0;
}

//extern void dumpbufwithname(char* title, void *buf, int len);
int is_wps_pbc(u8 *pie, u32 ie_length)
{
	const u8 *end, *pos;

	pos = (const u8 *) pie;
	end = pos + ie_length;

	while (pos + 1 < end) {
		if (pos + 2 + pos[1] > end)
			break;
		if (pos[0] == WLAN_EID_VENDOR_SPECIFIC && pos[1] >= 4 &&
			WPS_IE_VENDOR_TYPE == WPA_GET_BE32(&pos[2]))
		{
			int prase=wps_parse(&pos[6],pos[1]-4);
			//dumpbufwithname("ie",pos,ie_length);
			if(prase==1)
				return 1;
			else if(prase==2)
				return 2;
			else
				return 0;

		}
		pos += 2 + pos[1];
	}

	return 0;
}
