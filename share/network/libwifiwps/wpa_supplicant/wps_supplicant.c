/*
 * wpa_supplicant / WPS integration
 * Copyright (c) 2008-2012, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "eloop.h"
#include "uuid.h"
#include "crypto/crypto.h"
#include "crypto/random.h"
#include "crypto/dh_group5.h"
#include "common/ieee802_11_defs.h"
#include "common/ieee802_11_common.h"
#include "common/wpa_common.h"
#include "common/wpa_ctrl.h"
#include "eap_common/eap_wsc_common.h"
#include "eap_peer/eap.h"
#include "eap_peer/eap_i.h"
#include "eapol_supp/eapol_supp_sm.h"
#include "wps/wps_attr_parse.h"
#include "common/eapol_common.h"
#include "wps/wps_i.h"

#include "wps_supplicant.h"


#ifndef WPS_PIN_SCAN_IGNORE_SEL_REG
#define WPS_PIN_SCAN_IGNORE_SEL_REG 3
#endif /* WPS_PIN_SCAN_IGNORE_SEL_REG */

struct eap_sm  eapsm;
static u8* wps_ssid;
static u8* wps_key;
static u8* wps_keylen;
static u8* wps_pin;
static int wpa_supplicant_wps_cred(void *ctx,
				   const struct wps_credential *cred)
{

	int key_len;
	wpa_hexdump_key(MSG_DEBUG, "WPS: Received Credential attribute",
			cred->cred_attr, cred->cred_attr_len);

	wpa_hexdump_ascii(MSG_DEBUG, "WPS: SSID", cred->ssid, cred->ssid_len);
	wpa_printf(MSG_DEBUG, "WPS: Authentication Type 0x%x",
		   cred->auth_type);
	wpa_printf(MSG_DEBUG, "WPS: Encryption Type 0x%x", cred->encr_type);
	wpa_printf(MSG_DEBUG, "WPS: Network Key Index %d", cred->key_idx);
	wpa_hexdump_key(MSG_DEBUG, "WPS: Network Key",
			cred->key, cred->key_len);
	wpa_printf(MSG_DEBUG, "WPS: MAC Address " MACSTR,
		   MAC2STR(cred->mac_addr));
	key_len=cred->key_len;
	if (cred->auth_type == WPS_AUTH_OPEN &&
	    cred->encr_type == WPS_ENCR_NONE)
		key_len=0;
	if(wps_keylen)
			*wps_keylen=(u8)key_len;
	if(wps_key)
	{
		memcpy(wps_key,cred->key,key_len);
		wps_key[key_len]=0;
	}

	if(wps_ssid)
		memcpy(wps_ssid,cred->ssid,cred->ssid_len);
	wps_ssid[cred->ssid_len]=0;
	return 0;
}

static void wpa_supplicant_wps_event(void *ctx, enum wps_event event,
				     union wps_event_data *data)
{

	switch (event) {
	case WPS_EV_M2D:
		eapsm.EAP_state = EAP_FAILURE;
		debug_printf("recv event: WPS_EV_M2D\n");
		break;
	case WPS_EV_FAIL:
		eapsm.EAP_state = EAP_FAILURE;
		debug_printf("recv event: WPS_EV_FAIL\n");
		break;
	case WPS_EV_SUCCESS:
		eapsm.EAP_state = EAP_SUCCESS;
		debug_printf("recv event: WPS_EV_SUCCESS\n");
		break;
	case WPS_EV_PWD_AUTH_FAIL:
#ifdef CONFIG_AP
		debug_printf("recv event: WPS_EV_PWD_AUTH_FAIL\n");
#endif /* CONFIG_AP */
		break;
	case WPS_EV_PBC_OVERLAP:
		debug_printf("recv event: WPS_EV_PBC_OVERLAP\n");
		break;
	case WPS_EV_PBC_TIMEOUT:
		debug_printf("recv event: WPS_EV_PBC_TIMEOUT\n");
		break;
	case WPS_EV_ER_AP_ADD:
		debug_printf("recv event: WPS_EV_ER_AP_ADD\n");
		break;
	case WPS_EV_ER_AP_REMOVE:
		debug_printf("recv event: WPS_EV_ER_AP_REMOVE\n");
		break;
	case WPS_EV_ER_ENROLLEE_ADD:
		debug_printf("recv event: WPS_EV_ER_ENROLLEE_ADD\n");
		break;
	case WPS_EV_ER_ENROLLEE_REMOVE:
		debug_printf("recv event: WPS_EV_ER_ENROLLEE_REMOVE\n");
		break;
	case WPS_EV_ER_AP_SETTINGS:
		debug_printf("recv event: WPS_EV_ER_AP_SETTINGS\n");
		break;
	case WPS_EV_ER_SET_SELECTED_REGISTRAR:
		debug_printf("recv event: WPS_EV_ER_SET_SELECTED_REGISTRAR\n");
		break;
	case WPS_EV_AP_PIN_SUCCESS:
		debug_printf("recv event: WPS_EV_AP_PIN_SUCCESS\n");
		break;
	}
}



int wpas_wps_start_pbc(const u8 *bssid, int p2p_group)
{
	//wpa_config_set(ssid, "phase1", "\"pbc=1\"", 0);
	return 0;
}


int wpas_wps_start_pin(const u8 *bssid, const char *pin, int p2p_group, u16 dev_pw_id)
{
	char val[128];
	unsigned int rpin = 0;

	if (pin)
		os_snprintf(val, sizeof(val), "\"pin=%s dev_pw_id=%d\"",
			    pin, dev_pw_id);
	else {
		rpin = wps_generate_pin();
		os_snprintf(val, sizeof(val), "\"pin=%d dev_pw_id=%d\"",
			    rpin, dev_pw_id);
	}
//	wpa_config_set(ssid, "phase1", val, 0);

	return rpin;
}





#if 0

static int wpas_wps_new_psk_cb(void *ctx, const u8 *mac_addr, const u8 *psk,
			       size_t psk_len)
{
	wpa_printf(MSG_DEBUG, "WPS: Received new WPA/WPA2-PSK from WPS for "
		   "STA " MACSTR, MAC2STR(mac_addr));
	wpa_hexdump_key(MSG_DEBUG, "Per-device PSK", psk, psk_len);

	/* TODO */

	return 0;
}


static void wpas_wps_pin_needed_cb(void *ctx, const u8 *uuid_e,
				   const struct wps_device_data *dev)
{
	char uuid[40], txt[400];
	int len;
	char devtype[WPS_DEV_TYPE_BUFSIZE];
	if (uuid_bin2str(uuid_e, uuid, sizeof(uuid)))
		return;
	wpa_printf(MSG_DEBUG, "WPS: PIN needed for UUID-E %s", uuid);
	len = os_snprintf(txt, sizeof(txt), "WPS-EVENT-PIN-NEEDED %s " MACSTR
			  " [%s|%s|%s|%s|%s|%s]",
			  uuid, MAC2STR(dev->mac_addr), dev->device_name,
			  dev->manufacturer, dev->model_name,
			  dev->model_number, dev->serial_number,
			  wps_dev_type_bin2str(dev->pri_dev_type, devtype,
					       sizeof(devtype)));
	if (len > 0 && len < (int) sizeof(txt))
		wpa_printf(MSG_INFO, "%s", txt);
}


static void wpas_wps_set_sel_reg_cb(void *ctx, int sel_reg, u16 dev_passwd_id,
				    u16 sel_reg_config_methods)
{
#ifdef CONFIG_WPS_ER
	struct wpa_supplicant *wpa_s = ctx;

	if (wpa_s->wps_er == NULL)
		return;
	wpa_printf(MSG_DEBUG, "WPS ER: SetSelectedRegistrar - sel_reg=%d "
		   "dev_password_id=%u sel_reg_config_methods=0x%x",
		   sel_reg, dev_passwd_id, sel_reg_config_methods);
	wps_er_set_sel_reg(wpa_s->wps_er, sel_reg, dev_passwd_id,
			   sel_reg_config_methods);
#endif /* CONFIG_WPS_ER */
}

#endif

static u16 wps_fix_config_methods(u16 config_methods)
{
#ifdef CONFIG_WPS2
	if ((config_methods &
	     (WPS_CONFIG_DISPLAY | WPS_CONFIG_VIRT_DISPLAY |
	      WPS_CONFIG_PHY_DISPLAY)) == WPS_CONFIG_DISPLAY) {
		wpa_printf(MSG_INFO, "WPS: Converting display to "
			   "virtual_display for WPS 2.0 compliance");
		config_methods |= WPS_CONFIG_VIRT_DISPLAY;
	}
	if ((config_methods &
	     (WPS_CONFIG_PUSHBUTTON | WPS_CONFIG_VIRT_PUSHBUTTON |
	      WPS_CONFIG_PHY_PUSHBUTTON)) == WPS_CONFIG_PUSHBUTTON) {
		wpa_printf(MSG_INFO, "WPS: Converting push_button to "
			   "virtual_push_button for WPS 2.0 compliance");
		config_methods |= WPS_CONFIG_VIRT_PUSHBUTTON;
	}
#endif /* CONFIG_WPS2 */

	return config_methods;
}


static void wpas_wps_set_uuid(u8* mac, struct wps_context *wps)
{
	wpa_printf(MSG_DEBUG, "WPS: Set UUID for interface" );

	uuid_gen_mac_addr(mac, wps->uuid);
	wpa_hexdump(MSG_DEBUG, "WPS: UUID based on MAC "
			"address", wps->uuid, WPS_UUID_LEN);

}




static char conf_device_name[]={'N','T','G','R','D','E','V', '\0'}; //this name is special for negear basestation
static char conf_manufacturer[]="broadcom technology, Inc.";
static char conf_model_name[]="SX-10WG";
static char conf_model_number[]="0000";
static char conf_serial_number[]="1234";
static char conf_device_type[]={0x00,0x10,0x50,0x00,0x04,0xf2,0x00,0x01};
static char conf_vendor_ext_m1[]="000137100100020001";
static char conf_identity[]= WSC_ID_ENROLLEE;
static char conf_phase1[24];
//static char conf_phase1[]= "pbc=1";
static char conf_methods[96];
//static char conf_methods[]="push_button|display|keypad";
static u8 conf_os_version[]={0,0,0,0};

struct wps_context wps_ctx;
int wpas_wps_init(u8 *mac_addr, u8 *bssid)
{
	struct wps_context *wps;
#ifdef EAP_SERVER_WSC
	struct wps_registrar_config rcfg;
#endif
//	struct hostapd_hw_modes *modes;
//	u16 m;

	wps = &wps_ctx;
	if (wps == NULL)
		return -1;

	wps->cred_cb = wpa_supplicant_wps_cred;
	wps->event_cb = wpa_supplicant_wps_event;
	wps->cb_ctx = NULL;

	memset(conf_phase1,0,sizeof(conf_phase1));
	if(wps_pin)
	{
//		sprintf(conf_identity,"%s",WSC_ID_ENROLLEE);
		sprintf(conf_phase1,"pin=%s",wps_pin);
		sprintf(conf_methods,"display|keypad");
	}
	else
	{
//		sprintf(conf_identity,"%s",WSC_ID_ENROLLEE);
		sprintf(conf_phase1,"pbc=1");
		sprintf(conf_methods,"push_button|display|keypad");
	}

	wps->dev.identity = conf_identity;
	wps->dev.phase1 = conf_phase1;
	wps->dev.device_name = conf_device_name;
	wps->dev.manufacturer = conf_manufacturer;
	wps->dev.model_name = conf_model_name;
	wps->dev.model_number = conf_model_number;
	wps->dev.serial_number = conf_serial_number;
	wps->config_methods = wps_config_methods_str2bin(conf_methods);
	if ((wps->config_methods & (WPS_CONFIG_DISPLAY | WPS_CONFIG_LABEL)) ==
	    (WPS_CONFIG_DISPLAY | WPS_CONFIG_LABEL)) {
		wpa_printf(MSG_ERROR, "WPS: Both Label and Display config "
			   "methods are not allowed at the same time");
		os_free(wps);
		return -1;
	}
	wps->config_methods = wps_fix_config_methods(wps->config_methods);
	wps->dev.config_methods = wps->config_methods;
	os_memcpy(wps->dev.pri_dev_type, conf_device_type,WPS_DEV_TYPE_LEN);


//  this is for p2p
//	wps->dev.num_sec_dev_types = wpa_s->conf->num_sec_device_types;
//	os_memcpy(wps->dev.sec_dev_type, wpa_s->conf->sec_device_type,
//		  WPS_DEV_TYPE_LEN * wps->dev.num_sec_dev_types);


	wps->dev.vendor_ext_m1 = wpabuf_alloc_ext_data((u8*)conf_vendor_ext_m1,sizeof(conf_vendor_ext_m1));

	wps->dev.os_version = WPA_GET_BE32(conf_os_version);

	wps->dev.rf_bands |= WPS_RF_24GHZ;

	wps->dev.rf_bands |= WPS_RF_50GHZ;

	if (wps->dev.rf_bands == 0) {
		/*
		 * Default to claiming support for both bands if the driver
		 * does not provide support for fetching supported bands.
		 */
		wps->dev.rf_bands = WPS_RF_24GHZ | WPS_RF_50GHZ;
	}
	os_memcpy(wps->dev.mac_addr, mac_addr, ETH_ALEN);
	os_memcpy(wps->bssid, bssid, ETH_ALEN);
	wpas_wps_set_uuid(mac_addr, wps);

	wps->auth_types = WPS_AUTH_WPA2PSK | WPS_AUTH_WPAPSK;
	wps->encr_types = WPS_ENCR_AES | WPS_ENCR_TKIP;

#ifdef EAP_SERVER_WSC
	wps->ap = 1;
	os_memset(&rcfg, 0, sizeof(rcfg));
	extern int wlan_wps_set_ie_cb(void *ctx, struct wpabuf *beacon_ie,struct wpabuf *probe_resp_ie);
	rcfg.set_ie_cb = wlan_wps_set_ie_cb;
//	rcfg.new_psk_cb = wpas_wps_new_psk_cb;
//	rcfg.pin_needed_cb = wpas_wps_pin_needed_cb;
//	rcfg.set_sel_reg_cb = wpas_wps_set_sel_reg_cb;
//	rcfg.cb_ctx = wpa_s;
	wps->registrar = wps_registrar_init(wps, &rcfg);
	if (wps->registrar == NULL) {
		wpa_printf(MSG_DEBUG, "Failed to initialize WPS Registrar");
		os_free(wps);
		return -1;
	}
#endif
	return 0;
}


void wpas_wps_deinit(void)
{
	struct wps_context *wps;

	wps = &wps_ctx;
#ifdef EAP_SERVER_WSC
	wps_registrar_deinit(wps->registrar);
#endif
	wpabuf_free(wps->dh_pubkey);
	wpabuf_free(wps->dh_privkey);
	wpabuf_free(wps->dev.vendor_ext_m1);
	os_free(wps->network_key);

}

typedef enum { WS_Start,WS_Wps,WS_Over } WPS_SUPPLICANT_STA;

int _wps_pin_str_valid(const char *pin)
{
	const char *p;
	size_t len;
	if(pin==0) return 0;
	p = pin;
	while (*p >= '0' && *p <= '9')
		p++;
	if (*p != '\0')
		return 0;

	len = p - pin;
	return len == 4 || len == 8;
}

int wps_supplicant_init(u8 *mac, u8* bssid, u8* ssid, u8* key, u8* keylen, u8* pin)
{
	if(_wps_pin_str_valid(pin)==0) 
	{
		wps_pin=0;
		wpa_printf(MSG_INFO, "WPS: Init pbc mode");
	}
	else 
	{
		wps_pin=pin;
		wpa_printf(MSG_INFO, "WPS: Init pin mode");
	}
	random_init(NULL);
    wpas_wps_init(mac,bssid);
    wsc_init(&wps_ctx);
    memset(&eapsm, 0 ,sizeof(eapsm));
    eapsm.wps = &wps_ctx;
    eapsm.EAP_state = EAP_INITIALIZE;
    wps_key=key;
    wps_keylen=keylen;
    wps_ssid=ssid;
    return 0;
}

int wps_supplicant_deinit(void)
{
	wpas_wps_deinit();
	wsc_deinit();
    return 0;
}
struct wpabuf * wps_alloc_eapol(u8 type,const void *data, u16 data_len)
{
	struct ieee802_1x_hdr *hdr;
	struct wpabuf * sbuf;
	u32 msg_len = sizeof(*hdr) + data_len;
	sbuf = wpabuf_alloc(msg_len);
	if (sbuf == NULL)
		return NULL;
	hdr = (struct ieee802_1x_hdr *)wpabuf_head(sbuf);
	wpabuf_put_u8(sbuf,2);
	wpabuf_put_u8(sbuf,type);
	wpabuf_put_be16(sbuf,data_len);


	if (data)
		wpabuf_put_data(sbuf,data,data_len);
	else
		wpabuf_set_val(sbuf,0,data_len);

	return sbuf;
}
u8 wps_parse_eapol(struct wpabuf *eapoldata)
{
	struct ieee802_1x_hdr *hdr=(struct ieee802_1x_hdr *)wpabuf_head(eapoldata);
	debug_printf("EAPOL: VERSION: %d, TYPE: %d, LEN %d\n",hdr->version,hdr->type,be_to_host16(hdr->length));
	wpabuf_cuthead(eapoldata,sizeof(*hdr));
	return hdr->type;
}
static void eap_notify_status(struct eap_sm *sm, const char *status,
				      const char *parameter)
{
	wpa_printf(MSG_DEBUG, "EAP: Status notification: %s (param=%s)",
		   status, parameter);
}


static void eap_sm_parseEapReq(const struct wpabuf *req)
{
	const struct eap_hdr *hdr;
	size_t plen;
	const u8 *pos;
	struct eap_sm  *sm = &eapsm;
	sm->rxReq = sm->rxResp = sm->rxSuccess = sm->rxFailure = FALSE;
	sm->reqId = 0;
	sm->reqMethod = EAP_TYPE_NONE;
	sm->reqVendor = EAP_VENDOR_IETF;
	sm->reqVendorMethod = EAP_TYPE_NONE;

	if (req == NULL || wpabuf_len(req) < sizeof(*hdr))
		return;

	hdr = wpabuf_head(req);
	plen = be_to_host16(hdr->length);
	if (plen > wpabuf_len(req)) {
		wpa_printf(MSG_DEBUG, "EAP: Ignored truncated EAP-Packet "
			   "(len=%d plen=%d)",
			   (unsigned long) wpabuf_len(req),
			   (unsigned long) plen);
		return;
	}

	sm->reqId = hdr->identifier;

	if (sm->workaround) {
		const u8 *addr[1];
		addr[0] = wpabuf_head(req);
		md5_vector(1, addr, &plen, sm->req_md5);
	}

	switch (hdr->code) {
	case EAP_CODE_REQUEST:
		if (plen < sizeof(*hdr) + 1) {
			wpa_printf(MSG_DEBUG, "EAP: Too short EAP-Request - "
				   "no Type field");
			return;
		}
		sm->rxReq = TRUE;
		pos = (const u8 *) (hdr + 1);
		sm->reqMethod = *pos++;
		if (sm->reqMethod == EAP_TYPE_IDENTITY) {
			if (eap_hdr_len_valid(req, 1))
				sm->EAP_state = EAP_IDENTITY;
		}else if(sm->reqMethod == EAP_TYPE_EXPANDED)
		{
			sm->EAP_state = EAP_RECEIVED;
		}else if (sm->reqMethod == EAP_TYPE_EXPANDED) {
			if (plen < sizeof(*hdr) + 8) {
				wpa_printf(MSG_DEBUG, "EAP: Ignored truncated "
					   "expanded EAP-Packet (plen=%d)",
					   (unsigned long) plen);
				return;
			}
			sm->reqVendor = WPA_GET_BE24(pos);
			pos += 3;
			sm->reqVendorMethod = WPA_GET_BE32(pos);
		}
		wpa_printf(MSG_DEBUG, "EAP: Received EAP-Request id=%d "
			   "method=%d vendor=%d vendorMethod=%d",
			   sm->reqId, sm->reqMethod, sm->reqVendor,
			   sm->reqVendorMethod);
		break;
	case EAP_CODE_RESPONSE:
		if (sm->selectedMethod == EAP_TYPE_LEAP) {
			/*
			 * LEAP differs from RFC 4137 by using reversed roles
			 * for mutual authentication and because of this, we
			 * need to accept EAP-Response frames if LEAP is used.
			 */
			if (plen < sizeof(*hdr) + 1) {
				wpa_printf(MSG_DEBUG, "EAP: Too short "
					   "EAP-Response - no Type field");
				return;
			}
			sm->rxResp = TRUE;
			pos = (const u8 *) (hdr + 1);
			sm->reqMethod = *pos;
			wpa_printf(MSG_DEBUG, "EAP: Received EAP-Response for "
				   "LEAP method=%d id=%d",
				   sm->reqMethod, sm->reqId);
			break;
		}
		wpa_printf(MSG_DEBUG, "EAP: Ignored EAP-Response");
		break;
	case EAP_CODE_SUCCESS:
		wpa_printf(MSG_DEBUG, "EAP: Received EAP-Success");
		eap_notify_status(sm, "completion", "success");
		sm->rxSuccess = TRUE;
		break;
	case EAP_CODE_FAILURE:
		wpa_printf(MSG_DEBUG, "EAP: Received EAP-Failure");
		eap_notify_status(sm, "completion", "failure");
		wps_revfail_msg(sm->wps);
		sm->rxFailure = TRUE;
		break;
	default:
		wpa_printf(MSG_DEBUG, "EAP: Ignored EAP-Packet with unknown "
			   "code %d", hdr->code);
		break;
	}
}

int is_wps_supplicant_done(void)
{
	if(eapsm.EAP_state == EAP_SUCCESS)
		return 1;
	else if(eapsm.EAP_state == EAP_FAILURE )
		return 2;
	else
		return 0;
}

static struct wpabuf *sendData=NULL;
void* wps_supplicant_process(struct wpabuf *eapdata)
{
	struct wpabuf *seapData;
	static u32 t1=0;
	struct eap_sm  *sm = &eapsm;

	if(eapdata)
	{
		wpa_printf(MSG_DEBUG, "WPS: Recv WPS msg len %d\n",wpabuf_len(eapdata));
		wps_parse_eapol(eapdata);
		eap_sm_parseEapReq(eapdata);
	}
	switch(eapsm.EAP_state)
	{
	case EAP_INITIALIZE:
		wpabuf_free(sendData);sendData=NULL;
		if(ticks - t1 > WPS_RETRANSMISSION_TIMEOUT*100){
			wpabuf_free(sendData); sendData=NULL;
			sendData=wps_alloc_eapol(IEEE802_1X_TYPE_EAPOL_START, (u8 *) "", 0);
			t1 = ticks;
			wpa_printf(MSG_EXCESSIVE,"send IEEE802_1X_TYPE_EAPOL_START len %d\n",wpabuf_len(sendData));
			goto senddata;
		}
		break;
	case EAP_IDENTITY:
		if(eapdata ){
			t1=ticks;
			eap_sm_processIdentity(sm, eapdata);
			seapData = eap_sm_buildIdentity(sm, sm->reqId, 0);
			wpabuf_free(sendData); sendData=NULL;
			sendData=wps_alloc_eapol(IEEE802_1X_TYPE_EAP_PACKET, wpabuf_head(seapData), wpabuf_len(seapData));
			wpabuf_free(seapData);
			wpa_printf(MSG_EXCESSIVE,"send IEEE802_1X_TYPE_EAP_PACKET len %d\n",wpabuf_len(sendData));
			goto senddata;
		}else if(ticks - t1 > WPS_RETRANSMISSION_TIMEOUT*100)
		{
			t1=ticks;
			wpa_printf(MSG_EXCESSIVE,"resend IEEE802_1X_TYPE_EAP_PACKET len %d\n",wpabuf_len(sendData));
			goto senddata;
		}
		break;
	case EAP_RECEIVED:
		if(eapdata)
		{
#ifdef EAP_WSC
			if((seapData=wsc_process(eapdata))==NULL)
				break;
			wpabuf_free(sendData); sendData=NULL;
#endif
#ifdef EAP_SERVER_WSC
			extern void eap_wsc_process(struct eap_sm *sm, void *priv,struct wpabuf *respData);
			//FIXME: only to make sure the ret arg is not NULL 
			struct eap_method_ret ret;
			eap_wsc_process(NULL,&ret,eapdata);
#endif
			sendData=wps_alloc_eapol(IEEE802_1X_TYPE_EAP_PACKET, wpabuf_head(seapData), wpabuf_len(seapData));
			wpabuf_free(seapData);
			t1=ticks;
			wpa_printf(MSG_EXCESSIVE,"send IEEE802_1X_TYPE_EAP_PACKET len %d\n",wpabuf_len(sendData));
			goto senddata;
		}else if(ticks - t1 > WPS_RETRANSMISSION_TIMEOUT*100)
		{
			t1=ticks;
			wpa_printf(MSG_EXCESSIVE,"resend IEEE802_1X_TYPE_EAP_PACKET len %d\n",wpabuf_len(sendData));
			goto senddata;
		}
		break;

	default:

		break;
	}
	return NULL;
senddata:
	wpa_printf(MSG_DEBUG, "WPS: Send WPS msg len %d\n",wpabuf_len(sendData));
	return sendData;
}



u32  eapol_send(struct wpabuf * buf, u8* outbuf)
{
	u8* ptr=outbuf;
	u32 len;
	memcpy(ptr,wps_ctx.bssid,6);
	ptr+=6;
	memcpy(ptr,wps_ctx.dev.mac_addr,6);
	ptr+=6;
	*ptr=0x88; ptr++;
	*ptr=0x8e; ptr++;
	memcpy(ptr,wpabuf_head(buf),wpabuf_len(buf));
	len=wpabuf_len(buf)+14;
	return len;
}
struct wpabuf * eapol_recv(u8* buf, u32 len)
{
//	struct wpabuf * wbuf;
	u8* ptr=buf;
	if(ptr==NULL)
		return NULL;
	if(memcmp(ptr,wps_ctx.dev.mac_addr,6))
		return NULL;
	ptr+=6;
	if(memcmp(ptr,wps_ctx.bssid,6))
		return NULL;
	ptr+=6;
	if(ptr[0]!=0x88||ptr[1]!=0x8e)
		return NULL;
	ptr+=2;
	len-=14;
	return wpabuf_alloc_ext_data(ptr,len);

}
