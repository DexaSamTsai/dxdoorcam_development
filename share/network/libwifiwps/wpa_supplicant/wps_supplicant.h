/*
 * wpa_supplicant / WPS integration
 * Copyright (c) 2008-2012, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#ifndef WPS_SUPPLICANT_H
#define WPS_SUPPLICANT_H

#include "utils/common.h"
#include "random.h"
#include "wps/wps.h"
#include "wps/wps_defs.h"
#include "eap_peer/eap_wsc.h"

#define WPS_RETRANSMISSION_TIMEOUT	2			/* 2 seconds */
#define WPS_PER_PACKET_TIMEOUT		20			/* 20 seconds */
#define WPS_OVERALL_TIMEOUT			120

int wps_supplicant_init(u8 *mac, u8* bssid, u8* ssid, u8* key, u8* keylen, u8* pin);
void* wps_supplicant_process(struct wpabuf *revbuf);
int is_wps_supplicant_done(void);
int is_wps_pbc(u8 *pie, u32 ie_length);
u32 eapol_send(struct wpabuf *buf, u8* outbuf);
struct wpabuf * eapol_recv(u8* buf, u32 len);
#endif /* WPS_SUPPLICANT_H */
