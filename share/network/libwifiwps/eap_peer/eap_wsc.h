#ifndef _EAP_WSC_H_
#define _EAP_WSC_H_
int wsc_init(void *wps_ctx);
void wsc_deinit(void);
struct wpabuf * wsc_process(void *reqData);
#endif
