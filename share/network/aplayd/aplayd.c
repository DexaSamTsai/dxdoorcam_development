#include "includes.h"

void * aplayd_routine(void* arg)
{
	t_aplayd_cfg * cfg = (t_aplayd_cfg *)arg;

	pthread_detach(pthread_self());

	pthread_setname(NULL, "APlayD");
#ifdef CONFIG_ERTOS_WATCHDOG_EN
	if(cfg->adec_stop_ms){
		u32 timeout_ticks = (cfg->adec_stop_ms/10) * 2; //2 times of select timeout
		if(timeout_ticks < CONFIG_ERTOS_WATCHDOG_TIMEOUT * 100){
			timeout_ticks = CONFIG_ERTOS_WATCHDOG_TIMEOUT * 100;
		}
		ertos_task_set_watchdog(NULL, timeout_ticks);
	}
#endif

	if(cfg->listen_port == 0){
#define APLAYD_DEFAULT_LISTEN_PORT	5000
		syslog(LOG_WARNING, "WARN: aplayd use default port %d\n", APLAYD_DEFAULT_LISTEN_PORT);
		cfg->listen_port = APLAYD_DEFAULT_LISTEN_PORT;
	}

	cfg->socket = socket(AF_INET, SOCK_DGRAM, 0);
    if( cfg->socket < 0 ){
		syslog(LOG_ERR, "ERROR: aplayd udp socket() fail.\n");
		return NULL;
    }
    
	struct sockaddr_in local_addr; // local
    memset(&local_addr, 0, sizeof(struct sockaddr_in));
    local_addr.sin_family = AF_INET;
    local_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
    local_addr.sin_port = htons(cfg->listen_port); 
    if (bind(cfg->socket, (struct sockaddr *)&local_addr, sizeof(struct sockaddr_in)) == -1){
    	syslog(LOG_ERR, "ERROR: aplayd udp bind() fail.\n");
    	close(cfg->socket);
		return NULL;
    }
	syslog(LOG_DEBUG, "aplayd create socket:%x\n", cfg->socket);

	struct timeval tv;
	tv.tv_sec = cfg->adec_stop_ms / 1000;
	tv.tv_usec = (cfg->adec_stop_ms % 1000) * 1000;

	struct sockaddr_in remote_addr;
	int remote_addr_len = sizeof(remote_addr);
	int len;
	fd_set readfds;

	cfg->adec_running = 0;
	while(1)
	{
		FD_ZERO(&readfds);
		FD_SET(cfg->socket, &readfds);
		select(cfg->socket+1, &readfds, NULL, NULL, cfg->adec_stop_ms == 0 ? NULL : &tv);

		if(FD_ISSET(cfg->socket, &readfds))
		{
			len = recvfrom(cfg->socket, cfg->recvbuf, sizeof(cfg->recvbuf), 0,(struct sockaddr *)&remote_addr, (socklen_t *)&remote_addr_len );
			if(len > 0)//data
			{
int aplayd_demux_getpayload(char * buf, int len, char * * payload_start, unsigned int * flag);
				char * payload_start;
				int fmt;
				int payload_len = aplayd_demux_getpayload((char *)cfg->recvbuf, len, &payload_start, (unsigned int *)&fmt);
				if(payload_len > 0){
					if(!cfg->adec_running){
						cfg->adec_running = 1;
						if(cfg->adec_start){
							cfg->adec_start(fmt);
						}
					}

					//TODO: get adec frame size, and if payload_len > frame size, add_frame() for multiple times.
					FRAME *afrm = NULL;
					afrm = libadec_get_frame(0);
					if(afrm)
					{
						//debug_printf("a(%d,%d):%x:%x:%x\n",afrm->size,payload_len,payload_start[0],payload_start[1],payload_start[2]);
						memcpy((void *)afrm->addr, payload_start, payload_len);
						afrm->size = payload_len;
						libadec_add_frame(afrm);
					}
				}
			}
		}
		else
		{
			if(cfg->adec_running)
			{
				cfg->adec_running = 0;
				if(cfg->adec_stop){
					cfg->adec_stop();
				}
				if(cfg->adec_exit){
					cfg->adec_exit();
				}
			}
		}
	}

	return NULL;
}

int aplayd_start(t_aplayd_cfg * cfg)
{
    pthread_attr_t aplayd_attr;
    pthread_attr_init(&aplayd_attr);
    pthread_attr_setstacksize(&aplayd_attr, 4*1024);
	int ret = pthread_create(&cfg->pt, &aplayd_attr, aplayd_routine, cfg);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: aplayd thread create error\n");
		return -4;
	}
	return 0;
}
