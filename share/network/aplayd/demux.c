#include "includes.h"

#define RTP_HEADER_SIZE 12

int aplayd_demux_getpayload(char * buf, int len, char * * payload_start, unsigned int * flag)
{
	if(len > RTP_HEADER_SIZE){
#if 0
		rtp_header_t *rtp_h;
		rtp_h = (rtp_header_t *)buf;
		//debug_printf("seq:%x\n",rtp_h->sn);
#endif

		*payload_start = buf + RTP_HEADER_SIZE;
		*flag = 5; //TODO: parse fmt
		return len - RTP_HEADER_SIZE;
	}

	return 0;
}

