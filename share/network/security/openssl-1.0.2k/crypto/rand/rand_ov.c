#include <stdio.h>

#include "cryptlib.h"
#include <openssl/rand.h>
#include "rand_lcl.h"

int RAND_poll(void)
{
    u32  i;
    unsigned char buf[ENTROPY_NEEDED];
    struct timeval tv;

    for (i = 0; i < sizeof(buf); i++) {
    	gettimeofday(&tv, NULL);
        buf[i] = (char)tv.tv_usec;
    }
    RAND_add(buf, sizeof(buf), ENTROPY_NEEDED);
    return 1;

}
