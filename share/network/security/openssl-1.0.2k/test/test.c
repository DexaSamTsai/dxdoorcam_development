/*
 * test.c
 *
 *  Created on: 2017年3月3日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "e_os.h"

#include <openssl/bio.h>

extern  int cmd_asn1test(int argc, char **argv);
extern  int cmd_bad_dtls_test(int argc, char **argv);
extern  int cmd_bftest(int argc, char **argv);
extern  int cmd_bntest(int argc, char **argv);
extern  int cmd_casttest(int argc, char **argv);
extern  int cmd_clienthellotest(int argc, char **argv);
extern  int cmd_constant_time_test(int argc, char **argv);
extern  int cmd_destest(int argc, char **argv);
extern  int cmd_dhtest(int argc, char **argv);
extern  int cmd_dsatest(int argc, char **argv);
extern  int cmd_dtlstest(int argc, char **argv);
extern  int cmd_dummytest(int argc, char **argv);
extern  int cmd_ecdhtest(int argc, char **argv);
extern  int cmd_ecdsatest(int argc, char **argv);
extern  int cmd_ectest(int argc, char **argv);
extern  int cmd_enginetest(int argc, char **argv);
extern  int cmd_evp_extra_test(int argc, char **argv);
extern  int cmd_evp_test(int argc, char **argv);
extern  int cmd_exptest(int argc, char **argv);
extern  int cmd_heartbeat_test(int argc, char **argv);
extern  int cmd_hmactest(int argc, char **argv);
extern  int cmd_ideatest(int argc, char **argv);
extern  int cmd_igetest(int argc, char **argv);
extern  int cmd_jpaketest(int argc, char **argv);
extern  int cmd_md2test(int argc, char **argv);
extern  int cmd_md4test(int argc, char **argv);
extern  int cmd_md5test(int argc, char **argv);
extern  int cmd_mdc2test(int argc, char **argv);
extern  int cmd_randtest(int argc, char **argv);
extern  int cmd_rc2test(int argc, char **argv);
extern  int cmd_rc4test(int argc, char **argv);
extern  int cmd_rc5test(int argc, char **argv);
extern  int cmd_rmdtest(int argc, char **argv);
extern  int cmd_rsa_test(int argc, char **argv);
extern  int cmd_sha1test(int argc, char **argv);
extern  int cmd_sha256t(int argc, char **argv);
extern  int cmd_sha512t(int argc, char **argv);
extern  int cmd_shatest(int argc, char **argv);
extern  int cmd_srptest(int argc, char **argv);
extern  int cmd_ssltest(int argc, char **argv);
extern  int cmd_sslv2conftest(int argc, char **argv);
extern  int cmd_v3nametest(int argc, char **argv);
extern  int cmd_verify_extra_test(int argc, char **argv);
extern  int cmd_wp_test(int argc, char **argv);

struct s_openssl_thread_arg{
	int argc;
	char **argv;
} ;

ERTOS_EVENT_DECLARE(openssl_test_evt_handler)


void openssl_test_thread(void * arg)
{
	struct s_openssl_thread_arg *o_arg = (struct s_openssl_thread_arg*)arg;
	int argc = o_arg->argc;
	char **argv = o_arg->argv;
	if(argv[1])
	{
		if((!strcmp(argv[1],"test")))
		{
			fprintf(stderr,
		                    "ignoring -dhe1024dsa, since I'm compiled without DH\n");
			fprintf(stdout,
		                    "ignoring -dhe1024dsa, since I'm compiled without DH\n");
		}
		if(!strcmp(argv[1],"asn1test"))
		{
			cmd_asn1test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"bad_dtls_test"))
		{
			cmd_bad_dtls_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"bftest"))
		{
			cmd_bftest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"bntest"))
		{
			cmd_bntest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"casttest"))
		{
			cmd_casttest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"clienthellotest"))
		{
			cmd_clienthellotest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"constant_time_test"))
		{
			cmd_constant_time_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"destest"))
		{
			cmd_destest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"dhtest"))
		{
			cmd_dhtest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"dsatest"))
		{
			cmd_dsatest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"dtlstest"))
		{
			cmd_dtlstest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"dummytest"))
		{
			cmd_dummytest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"ecdhtest"))
		{
			cmd_ecdhtest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"ecdsatest"))
		{
			cmd_ecdsatest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"ectest"))
		{
			cmd_ectest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"enginetest"))
		{
			cmd_enginetest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"evp_extra_test"))
		{
			cmd_evp_extra_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"evp_test"))
		{
			cmd_evp_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"exptest"))
		{
			cmd_exptest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"heartbeat_test"))
		{
			cmd_heartbeat_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"hmactest"))
		{
			cmd_hmactest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"ideatest"))
		{
			cmd_ideatest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"igetest"))
		{
			cmd_igetest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"jpaketest"))
		{
			cmd_jpaketest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"md2test"))
		{
			cmd_md2test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"md4test"))
		{
			cmd_md4test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"md5test"))
		{
			cmd_md5test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"mdc2test"))
		{
			cmd_mdc2test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"randtest"))
		{
			cmd_randtest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"rc2test"))
		{
			cmd_rc2test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"rc4test"))
		{
			cmd_rc4test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"rc5test"))
		{
			cmd_rc5test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"rmdtest"))
		{
			cmd_rmdtest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"rsa_test"))
		{
			cmd_rsa_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"sha1test"))
		{
			cmd_sha1test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"sha256t"))
		{
			cmd_sha256t(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"sha512t"))
		{
			cmd_sha512t(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"shatest"))
		{
			cmd_shatest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"srptest"))
		{
			cmd_srptest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"ssltest"))
		{
			cmd_ssltest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"sslv2conftest"))
		{
			cmd_sslv2conftest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"v3nametest"))
		{
			cmd_v3nametest(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"verify_extra_test"))
		{
			cmd_verify_extra_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"wp_test"))
		{
			cmd_wp_test(argc-1,&argv[1]);
		}
		if(!strcmp(argv[1],"help"))
		{
			debug_printf("usage:\n");
			debug_printf("openssl [cmd] ...\n");
			debug_printf("cmd list:\n");
			debug_printf("asn1test\n");
			debug_printf("bad_dtls_test\n");
			debug_printf("bftest\n");
			debug_printf("bntest\n");
			debug_printf("casttest\n");
			debug_printf("clienthellotest\n");
			debug_printf("constant_time_test\n");
			debug_printf("destest\n");
			debug_printf("dhtest\n");
			debug_printf("dsatest\n");
			debug_printf("dtlstest\n");
			debug_printf("dummytest\n");
			debug_printf("ecdhtest\n");
			debug_printf("ecdsatest\n");
			debug_printf("ectest\n");
			debug_printf("enginetest\n");
			debug_printf("evp_extra_test\n");
			debug_printf("evp_test\n");
			debug_printf("exptest\n");
			debug_printf("heartbeat_test\n");
			debug_printf("hmactest\n");
			debug_printf("ideatest\n");
			debug_printf("igetest\n");
			debug_printf("jpaketest\n");
			debug_printf("md2test\n");
			debug_printf("md4test\n");
			debug_printf("md5test\n");
			debug_printf("mdc2test\n");
			debug_printf("randtest\n");
			debug_printf("rc2test\n");
			debug_printf("rc4test\n");
			debug_printf("rc5test\n");
			debug_printf("rmdtest\n");
			debug_printf("rsa_test\n");
			debug_printf("sha1test\n");
			debug_printf("sha256t\n");
			debug_printf("sha512t\n");
			debug_printf("shatest\n");
			debug_printf("srptest\n");
			debug_printf("ssltest\n");
			debug_printf("sslv2conftest\n");
			debug_printf("v3nametest\n");
			debug_printf("verify_extra_test\n");
			debug_printf("wp_test\n");
		}
	}
	ertos_event_signal(openssl_test_evt_handler);
}

int cmd_openssl_test(int argc, char **argv)
{
	struct s_openssl_thread_arg s_arg;
	s_arg.argc=argc;
	s_arg.argv=argv;
	ertos_event_init(openssl_test_evt_handler);
	ertos_task_create(openssl_test_thread, "openssl_test", 10*1024, &s_arg,  20);
	ertos_event_wait(openssl_test_evt_handler, WAIT_INF);
	return 1;
}
