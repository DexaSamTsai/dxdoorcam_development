#include "includes.h"

#include "lwip/tcpip.h"
#include "lwip/debug.h"
#include "lwip/netif.h"
#include "lwip/igmp.h"
#include "lwip/snmp.h"
#include "netif/etharp.h"
#include "network/eth/libeth.h"

/*
 *FIXME: 
 *  1, Do we only support one net_if?  
 *  2, Shall we make sure eth is inited before this?
 *  3, Is it good to move eth_init, eth_connect here?
 */
static void netif_eth_low_level_init(struct netif *netif )
{
	netif->hwaddr_len = (u8_t) ETHARP_HWADDR_LEN;

	u8 mac[ETHARP_HWADDR_LEN];
	memset(mac, 0, ETHARP_HWADDR_LEN);
	libeth_get_mac(mac);
	memcpy(netif->hwaddr, mac, ETHARP_HWADDR_LEN);

	netif->mtu = 1500;
	netif->flags = (u8_t) ( NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP );

#if LWIP_IGMP
	netif->flags |= NETIF_FLAG_IGMP;
	netif_set_igmp_mac_filter(netif, lwip_igmp_mac_filter);
#endif

}

static err_t netif_eth_low_level_output( struct netif *netif, struct pbuf *p )
{
	pbuf_ref(p);

	//real sendout to hardware
	/* p->payload, p->len, p->next */
	int ret =0,err_count =0;
	struct pbuf *myp = p;
	while(1)
	{
		libeth_packet_write(myp->payload, myp->len);
		if(ret ==0 && (myp->next!= NULL))
			myp = myp->next;
		else if(ret!=0)
		{
			err_count ++;
			continue;
		}
		else
			break;
	}
	//release
	pbuf_free(p);

	return ERR_OK;
}

static struct netif * priv_eth_netif = NULL;

err_t netif_eth_init(struct netif * netif)
{
#if LWIP_NETIF_HOSTNAME
	netif->hostname = "ethhost";
#endif
	priv_eth_netif = netif;
	NETIF_INIT_SNMP(netif, snmp_ifType_ethernet_csmacd, 54*1024*1024*8);

	//wl0
	netif->name[0] = 'e';
	netif->name[1] = 'n';

	netif->output = etharp_output;
#if LWIP_IPV6
	netif->output_ip6 = ethip6_output;
#endif /* LWIP_IPV6 */
	netif->linkoutput = netif_eth_low_level_output;

	netif_eth_low_level_init( netif );
	
	//TODO
	netif_set_default(netif);

	return ERR_OK;
}

//called by eth_thread
void netif_eth_input_process(u8 * buf, int len)
{
	if(priv_eth_netif == NULL){
		debug_printf("input eth data but netif_eth_init not called\r\n");
		return;
	}

	struct pbuf * p;

	while(1){
		p = pbuf_alloc(PBUF_RAW, len, PBUF_RAM);
		if(p != NULL){
			break;
		}
		ertos_timedelay(10);
	}
	memcpy(p->payload, buf, len);

	if(tcpip_input(p, priv_eth_netif) != ERR_OK){
		pbuf_free(p);
	}
}
