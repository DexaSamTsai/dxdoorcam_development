#include "includes.h"

#include "lwip/debug.h"
#include "lwip/def.h"
#include "lwip/sys.h"
#include "lwip/mem.h"
#include "lwip/stats.h"

#include "lwip/tcpip.h"


ERTOS_MUTEX_DECLARE(_lwip_mutex);

sys_prot_t sys_arch_protect( void )
{
	ertos_mutex_lock(_lwip_mutex, WAIT_INF);
	//return local_irq_disable();
	return 0;
}

void sys_arch_unprotect( sys_prot_t flags)
{
	ertos_mutex_unlock(_lwip_mutex);
	//local_irq_restore(flags);
}

#define LWIP_ARCH_MBOX_LENGTH (24)

err_t sys_mbox_new(sys_mbox_t *mbox, int size)
{
	(void) size;

	*mbox = ertos_queue_create(LWIP_ARCH_MBOX_LENGTH, 4);

	if(*mbox == NULL){
		return ERR_MEM;
	}

	return ERR_OK;
}

void sys_mbox_free(sys_mbox_t *mbox)
{
	ertos_queue_delete(*mbox);
}

void sys_mbox_post( sys_mbox_t *mbox, void *msg )
{
	int ret = ertos_queue_send(*mbox, &msg, 1000); //block 1000 ticks
	if(ret == 0){
		debug_printf("ERR: sys_mbox_post failed\n");
	}
}

u32_t sys_arch_mbox_fetch(sys_mbox_t *mbox, void **msg, u32_t timeout)
{
	u32 starttime = ticks;
	if(timeout == 0){
		timeout = 0xffffffff;
	}else{
		timeout /= 10;
	}

	if(ertos_queue_recv(*mbox, msg, timeout)){
		u32 t = (ticks - starttime) * 10;
		if(t == 0){
			t = 1;
		}
		return t;
	}else{
		//timeout
		if(msg != NULL){
			*msg = NULL;
		}
		return SYS_ARCH_TIMEOUT;
	}
}

u32_t sys_arch_mbox_tryfetch(sys_mbox_t *mbox, void **msg)
{
	if(ertos_queue_recv(*mbox, msg, 0)){
		return 0;
	}else{
		return SYS_MBOX_EMPTY;
	}
}

err_t sys_sem_new(sys_sem_t *sem, u8_t count)
{
	if(sem == NULL){
		return ERR_VAL;
	}

	*sem = ertos_sem_create();

	if(*sem == NULL){
		return ERR_MEM;
	}

	while(count > 0){
		ertos_sem_send(*sem);
		count --;
	}

	return ERR_OK;
}

u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeout)
{
	u32 starttime = ticks;
	if(timeout == 0){
		timeout = 0xffffffff;
	}else{
		timeout /= 10;
	}

	if(ertos_sem_recv(*sem, timeout)){
		u32 t = ticks - starttime;
		if(t == 0){
			t = 1;
		}
		return t;
	}else{
		//timeout
		return SYS_ARCH_TIMEOUT;
	}
}

void sys_sem_signal( sys_sem_t *sem )
{
	ertos_sem_send(*sem);
}

void sys_sem_free( sys_sem_t *sem )
{
	ertos_sem_delete(*sem);
}

void sys_init( void )
{
	ertos_mutex_init(_lwip_mutex);
}

void sys_deinit( void )
{
}

int sys_mbox_valid( sys_mbox_t *mbox )
{
	return (int)( *mbox != 0 );
}

int sys_sem_valid( sys_sem_t *sem )
{
	return (int)( *sem != 0 );
}

void sys_mbox_set_invalid( sys_mbox_t *mbox )
{
	( *(int*) mbox ) = 0;
}

void sys_sem_set_invalid( sys_sem_t *sem )
{
	( *(int*) sem ) = 0;
}

err_t sys_mbox_trypost( sys_mbox_t *mbox, void *msg )
{
	//TODO: ....
	int ret = ertos_queue_send(*mbox, &msg, 0); //block 1000 ticks
	if(ret == 1){
		return ERR_OK;
	}else{
#if SYS_STATS
		lwip_stats.sys.mbox.err++;
#endif
		//debug_printf("ERR: sys_mbox_trypost failed\n");
		return ERR_MEM;
	}
}

sys_thread_t sys_thread_new( const char *name, lwip_thread_fn thread, void *arg, int stacksize, int prio )
{
# if 0
	t_ertos_taskhandler handler;

	handler = ertos_task_create((void *(*)(void *))thread, (char *)name, stacksize, arg, prio);

	return handler;
# else
    pthread_t thread_id;

    if ( pthread_create( &thread_id, NULL, thread, arg ) != 0 ) {
        printf( "normal thread create error: %s\n", strerror( errno ) );
        return NULL;
    }

    pthread_detach( thread_id );
	return thread_id;
# endif
}

u32_t sys_now(void)
{
	return ticks * 10;	// Returns the current time in milliseconds,
}

extern u32 getcycle(void);
u32 sys_random(void)
{
	srand(getcycle());
	return rand();
}
