#ifndef _LWIP_ARCH_CC_H_
#define _LWIP_ARCH_CC_H_

#include "includes.h"

typedef u32_t            mem_ptr_t;
typedef u32              sys_prot_t;


#define U16_F "d"
#define S16_F "d"
#define X16_F "x"
#define U32_F "d"
#define S32_F "d"
#define X32_F "x"

#define PACK_STRUCT_BEGIN
#define PACK_STRUCT_STRUCT __attribute__ ((__packed__))
#define PACK_STRUCT_END
#define PACK_STRUCT_FIELD(x) x

#define LWIP_PLATFORM_DIAG(x)    do{debug_printf x;}while(0)
#define LWIP_PLATFORM_ASSERT(x) do{}while(0)

extern u32 sys_random(void);
#define LWIP_RAND()		sys_random() 

#define LWIP_TIMEVAL_PRIVATE 0

#endif
