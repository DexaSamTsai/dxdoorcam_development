#ifndef _LWIP_SYSARCH_H_
#define _LWIP_SYSARCH_H_

typedef t_ertos_queuehandler	sys_mbox_t;
typedef t_ertos_semhandler		sys_sem_t;
typedef t_ertos_taskhandler		sys_thread_t;

#endif
