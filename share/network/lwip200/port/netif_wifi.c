#include "includes.h"

#include "lwip/tcpip.h"
#include "lwip/debug.h"
#include "lwip/netif.h"
#include "lwip/igmp.h"
#include "lwip/snmp.h"
#include "netif/etharp.h"
#include "network/wifi/libwifi.h"

/*
 *FIXME: Do we only support one net_if?
 */
static void netif_wifi_low_level_init(struct netif *netif )
{
	netif->hwaddr_len = (u8_t) ETHARP_HWADDR_LEN;

	u8 mac[ETHARP_HWADDR_LEN];
	memset(mac, 0, ETHARP_HWADDR_LEN);
	libwifi_get_mac(mac);
	memcpy(netif->hwaddr, mac, ETHARP_HWADDR_LEN);

	netif->mtu = 1500;
	netif->flags = (u8_t) ( NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP );

#if LWIP_IGMP
	netif->flags |= NETIF_FLAG_IGMP;
	//netif_set_igmp_mac_filter(netif, lwip_igmp_mac_filter);
#endif

}

#if 0
static err_t multicast_ip_to_mac(ip_addr_t *ip_addr, uint8_t *mac_addr)
{
    if ((NULL == ip_addr) || (NULL == mac_addr)) {
        return ERR_ARG;
    }

    mac_addr[0] = 0x01;
    mac_addr[1] = 0x00;
    mac_addr[2] = 0x5E;
    mac_addr[3] = ip4_addr2_16(ip_addr) & ~0x80;
    mac_addr[4] = ip4_addr3_16(ip_addr);
    mac_addr[5] = ip4_addr4_16(ip_addr);
    return ERR_OK;
}

static err_t lwip_igmp_mac_filter(struct netif *netif, ip_addr_t *group, u8_t action)
{
    uint32_t devNumber = 0;
    uint32_t hash;

    #if LWIP_HAVE_LOOPIF
    devNumber = netif->num - 1;
    #else
    devNumber = netif->num;
    #endif

    uint8_t multicastMacAddr[6];
    multicast_ip_to_mac(group, multicastMacAddr);

    LWIP_DEBUGF(IGMP_DEBUG, ("mac filtering ip: %d hw: % hash: %d", );

    switch (action)
    {
        case NETIF_ADD_MAC_FILTER:
            return ENET_DRV_AddMulticastGroup(devNumber, (uint8_t *) multicastMacAddr, &hash);

        case NETIF_DEL_MAC_FILTER:
            return ENET_DRV_LeaveMulticastGroup(devNumber, (uint8_t *) multicastMacAddr);
        default:
            return ERR_IF;
  }

}
#endif

static err_t netif_wifi_low_level_output( struct netif *netif, struct pbuf *p )
{
	err_t eret = ERR_OK;

	pbuf_ref(p);

	//real sendout to hardware
	/* p->payload, p->len, p->next */
	int ret =0,err_count =0;
	struct pbuf *myp = p;
	while(1)
	{
		ret = libwifi_packet_write(myp->payload, myp->len, 0);
		if(ret ==0 && (myp->next!= NULL))
			myp = myp->next;
		else if(ret!=0)
		{
			// break out after some waiting to avoid tcpip thread being blocked.
			if(err_count > 5){
				syslog(LOG_ERR, "wifi packet write timeout\n");
				eret = ERR_TIMEOUT;
				break;
			}
			extern int libwifi_sendbuf_wait(void);
			libwifi_sendbuf_wait();

			err_count ++;
			continue;
		}
		else
			break;
	}
	//release
	pbuf_free(p);

	return eret;
}

static struct netif * priv_wifi_netif = NULL;

err_t netif_wifi_init(struct netif * netif)
{
#if LWIP_NETIF_HOSTNAME
	netif->hostname = "wifihost";
#endif
	priv_wifi_netif = netif;
	NETIF_INIT_SNMP(netif, snmp_ifType_ethernet_csmacd, 54*1024*1024*8);

	//wl0
	netif->name[0] = 'w';
	netif->name[1] = 'l';

	netif->output = etharp_output;
#if LWIP_IPV6
	netif->output_ip6 = ethip6_output;
#endif /* LWIP_IPV6 */
	netif->linkoutput = netif_wifi_low_level_output;

	netif_wifi_low_level_init( netif );

	//TODO
	netif_set_default(netif);

	return ERR_OK;
}

int netif_wifi_set_link_up(void)
{
	if(priv_wifi_netif == NULL){
		return 0;
	}
	return tcpip_callback((void (*)(void *ctx))netif_set_link_up, priv_wifi_netif);
}

int netif_wifi_set_link_down(void)
{
	if(priv_wifi_netif == NULL){
		return 0;
	}

	return tcpip_callback((void (*)(void *ctx))netif_set_link_down, priv_wifi_netif);
}

//called by wifi_thread
void netif_wifi_input_process(u8 * buf, int len)
{
	if(priv_wifi_netif == NULL){
		debug_printf("input wifi data but netif_wifi_init not called\r\n");
		return;
	}

	struct pbuf * p;

	while(1){
		p = pbuf_alloc(PBUF_RAW, len, PBUF_RAM);
		if(p != NULL){
			break;
		}
		ertos_timedelay(10);
	}
	memcpy(p->payload, buf, len);

	if(tcpip_input(p, priv_wifi_netif) != ERR_OK){
		pbuf_free(p);
	}
}
