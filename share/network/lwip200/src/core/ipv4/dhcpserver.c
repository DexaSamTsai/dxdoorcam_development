#include "lwip/opt.h"


#include "lwip/stats.h"
#include "lwip/mem.h"
#include "lwip/udp.h"
#include "lwip/ip_addr.h"
#include "lwip/netif.h"
#include "lwip/def.h"
#include "lwip/dhcp.h"
#include "lwip/autoip.h"
#include "lwip/dns.h"
#include "lwip/etharp.h"
#include "lwip/inet.h"
#include "lwip/err.h"
#include "lwip/pbuf.h"
#include "lwip/dhcpserver.h"


////////////////////////////////////////////////////////////////////////////////////
//static const uint8_t xid[4] = {0xad, 0xde, 0x12, 0x23};
//static u8_t old_xid[4] = {0};
static const uint32_t magic_cookie = 0x63538263;
static struct udp_pcb *pcb_dhcps = NULL;
static ip_addr_t broadcast_dhcps;
static ip_addr_t server_address;
static ip_addr_t client_address;//added

static struct dhcps_lease dhcps_lease;
//static bool dhcps_lease_flag = true;
static list_node *plist = NULL;
static uint8_t offer = 0xFF;
static bool renew = false;
#define DHCPS_LEASE_TIME_DEF	(120)
uint32_t dhcps_lease_time = DHCPS_LEASE_TIME_DEF;  //minute

void dhcps_client_leave(u8 *bssid, ip_addr_t *ip,bool force);
uint32_t dhcps_client_update(u8 *bssid, ip_addr_t *ip);

/******************************************************************************
 * FunctionName : node_insert_to_list
 * Description  : insert the node to the list
 * Parameters   : arg -- Additional argument to pass to the callback function
 * Returns      : none
*******************************************************************************/
void node_insert_to_list(list_node **phead, list_node* pinsert)
{
	list_node *plist = NULL;
	struct dhcps_pool *pdhcps_pool = NULL;
	struct dhcps_pool *pdhcps_node = NULL;
	if (*phead == NULL)
		*phead = pinsert;
	else {
		plist = *phead;
		pdhcps_node = pinsert->pnode;
		pdhcps_pool = plist->pnode;

		if(pdhcps_node->ip.addr < pdhcps_pool->ip.addr) {
		    pinsert->pnext = plist;
		    *phead = pinsert;
		} else {
            while (plist->pnext != NULL) {
                pdhcps_pool = plist->pnext->pnode;
                if (pdhcps_node->ip.addr < pdhcps_pool->ip.addr) {
                    pinsert->pnext = plist->pnext;
                    plist->pnext = pinsert;
                    break;
                }
                plist = plist->pnext;
            }

            if(plist->pnext == NULL) {
                plist->pnext = pinsert;
            }
		}
	}
}

/******************************************************************************
 * FunctionName : node_delete_from_list
 * Description  : remove the node from list
 * Parameters   : arg -- Additional argument to pass to the callback function
 * Returns      : none
*******************************************************************************/
void  node_remove_from_list(list_node **phead, list_node* pdelete)
{
	list_node *plist = NULL;

	plist = *phead;
	if (plist == NULL){
		*phead = NULL;
	} else {
		if (plist == pdelete){
			*phead = plist->pnext;
			pdelete->pnext = NULL;
		} else {
			while (plist != NULL) {
				if (plist->pnext == pdelete){
					plist->pnext = pdelete->pnext;
					pdelete->pnext = NULL;
				}
				plist = plist->pnext;
			}
		}
	}
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * add_msg_type
 *
 * @param optptr --
 * @param type --
 *
 * @return uint8_t*
 */
///////////////////////////////////////////////////////////////////////////////////
static uint8_t*  add_msg_type(uint8_t *optptr, uint8_t type)
{

        *optptr++ = DHCP_OPTION_MSG_TYPE;
        *optptr++ = 1;
        *optptr++ = type;
        return optptr;
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * add_offer_options
 *
 * @param optptr --
 *
 * @return uint8_t*
 */
///////////////////////////////////////////////////////////////////////////////////
static uint8_t*  add_offer_options(uint8_t *optptr)
{
        ip_addr_t ipadd;

        ipadd.addr = server_address.addr;

#ifdef USE_CLASS_B_NET
        *optptr++ = DHCP_OPTION_SUBNET_MASK;
        *optptr++ = 4;  //length
        *optptr++ = 255;
        *optptr++ = 240;	
        *optptr++ = 0;
        *optptr++ = 0;
#else
        *optptr++ = DHCP_OPTION_SUBNET_MASK;
        *optptr++ = 4;  
        *optptr++ = 255;
        *optptr++ = 255;	
        *optptr++ = 255;
        *optptr++ = 0;
#endif

        *optptr++ = DHCP_OPTION_LEASE_TIME;
        *optptr++ = 4;  
        *optptr++ = ((DHCPS_LEASE_TIMER * 60) >> 24) & 0xFF;
        *optptr++ = ((DHCPS_LEASE_TIMER * 60) >> 16) & 0xFF;
        *optptr++ = ((DHCPS_LEASE_TIMER * 60) >> 8) & 0xFF;
        *optptr++ = ((DHCPS_LEASE_TIMER * 60) >> 0) & 0xFF;

        *optptr++ = DHCP_OPTION_SERVER_ID;
        *optptr++ = 4;  
        *optptr++ = ip4_addr1( &ipadd);
        *optptr++ = ip4_addr2( &ipadd);
        *optptr++ = ip4_addr3( &ipadd);
        *optptr++ = ip4_addr4( &ipadd);



        if (dhcps_router_enabled(offer)){
        	struct ip_info if_ip;
			memset(&if_ip,0, sizeof(struct ip_info));
//			wifi_get_ip_info(SOFTAP_IF, &if_ip);

			*optptr++ = DHCP_OPTION_ROUTER;
			*optptr++ = 4;
			*optptr++ = ip4_addr1( &if_ip.gw);
			*optptr++ = ip4_addr2( &if_ip.gw);
			*optptr++ = ip4_addr3( &if_ip.gw);
			*optptr++ = ip4_addr4( &if_ip.gw);
        }

#ifdef USE_DNS
	    *optptr++ = DHCP_OPTION_DNS_SERVER;
	    *optptr++ = 4;
	    *optptr++ = ip4_addr1( &ipadd);
		*optptr++ = ip4_addr2( &ipadd);
		*optptr++ = ip4_addr3( &ipadd);
		*optptr++ = ip4_addr4( &ipadd);
#endif

#ifdef CLASS_B_NET
        *optptr++ = DHCP_OPTION_BROADCAST_ADDRESS;
        *optptr++ = 4;  
        *optptr++ = ip4_addr1( &ipadd);
        *optptr++ = 255;
        *optptr++ = 255;
        *optptr++ = 255;
#else
        *optptr++ = DHCP_OPTION_BROADCAST_ADDRESS;
        *optptr++ = 4;  
        *optptr++ = ip4_addr1( &ipadd);
        *optptr++ = ip4_addr2( &ipadd);
        *optptr++ = ip4_addr3( &ipadd);
        *optptr++ = 255;
#endif

        *optptr++ = DHCP_OPTION_INTERFACE_MTU;
        *optptr++ = 2;  
#ifdef CLASS_B_NET
        *optptr++ = 0x05;	
        *optptr++ = 0xdc;
#else
        *optptr++ = 0x02;	
        *optptr++ = 0x40;
#endif

        *optptr++ = DHCP_OPTION_PERFORM_ROUTER_DISCOVERY;
        *optptr++ = 1;  
        *optptr++ = 0x00; 

        *optptr++ = 43;	
        *optptr++ = 6;	

        *optptr++ = 0x01;	
        *optptr++ = 4;  
        *optptr++ = 0x00;
        *optptr++ = 0x00;
        *optptr++ = 0x00;
        *optptr++ = 0x02; 	

        return optptr;
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * add_end
 *
 * @param optptr --
 *
 * @return uint8_t*
 */
///////////////////////////////////////////////////////////////////////////////////
static uint8_t*  add_end(uint8_t *optptr)
{

        *optptr++ = DHCP_OPTION_END;
        return optptr;
}
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
static void  create_msg(struct dhcps_msg *m)
{
        ip_addr_t client;

        client.addr = client_address.addr;

        m->op = DHCPS_REPLY;
        m->htype = DHCP_HTYPE_ETHERNET;
        m->hlen = 6;  
        m->hops = 0;
//        memcpy((char *) xid, (char *) m->xid, sizeof(m->xid));
        m->secs = 0;
        m->flags = htons(BOOTP_BROADCAST); 

        memcpy((char *) m->yiaddr, (char *) &client.addr, sizeof(m->yiaddr));

        memset((char *) m->ciaddr, 0, sizeof(m->ciaddr));
        memset((char *) m->siaddr, 0, sizeof(m->siaddr));
        memset((char *) m->giaddr, 0, sizeof(m->giaddr));
        memset((char *) m->sname, 0, sizeof(m->sname));
        memset((char *) m->file, 0, sizeof(m->file));

        memset((char *) m->options, 0, sizeof(m->options));

//For xiaomi crash bug
        uint32_t magic_cookie1 = magic_cookie;
        memcpy((char *) m->options, &magic_cookie1, sizeof(magic_cookie1));
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * send_offer
 *
 * @param --
 */
///////////////////////////////////////////////////////////////////////////////////
static void  send_offer(struct dhcps_msg *m)
{
        uint8_t *end;
	    struct pbuf *p, *q;
	    u8_t *data;
	    u16_t cnt=0;
	    u16_t i;
		err_t SendOffer_err_t;
        create_msg(m);

        end = add_msg_type(&m->options[4], DHCPOFFER);
        end = add_offer_options(end);
        end = add_end(end);

	    p = pbuf_alloc(PBUF_TRANSPORT, sizeof(struct dhcps_msg), PBUF_RAM);
#if DHCPS_DEBUG
		debug_printf("udhcp: send_offer>>p->ref = %d\n", p->ref);
#endif
	    if(p != NULL){
	       
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_offer>>pbuf_alloc succeed\n");
	        debug_printf("dhcps: send_offer>>p->tot_len = %d\n", p->tot_len);
	        debug_printf("dhcps: send_offer>>p->len = %d\n", p->len);
#endif
	        q = p;
	        while(q != NULL){
	            data = (u8_t *)q->payload;
	            for(i=0; i<q->len; i++)
	            {
	                data[i] = ((u8_t *) m)[cnt++];
	            }

	            q = q->next;
	        }
	    }else{
	        
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_offer>>pbuf_alloc failed\n");
#endif
	        return;
	    }
        SendOffer_err_t = udp_sendto( pcb_dhcps, p, &broadcast_dhcps, DHCPS_CLIENT_PORT );
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_offer>>udp_sendto result %x\n",SendOffer_err_t);
#endif
	    if(p->ref != 0){	
#if DHCPS_DEBUG
	        debug_printf("udhcp: send_offer>>free pbuf\n");
#endif
	        pbuf_free(p);
	    }
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * send_nak
 *
 * @param m
 */
///////////////////////////////////////////////////////////////////////////////////
static void  send_nak(struct dhcps_msg *m)
{

    	u8_t *end;
	    struct pbuf *p, *q;
	    u8_t *data;
	    u16_t cnt=0;
	    u16_t i;
		err_t SendNak_err_t;
        create_msg(m);

        end = add_msg_type(&m->options[4], DHCPNAK);
        end = add_end(end);

	    p = pbuf_alloc(PBUF_TRANSPORT, sizeof(struct dhcps_msg), PBUF_RAM);
#if DHCPS_DEBUG
		debug_printf("udhcp: send_nak>>p->ref = %d\n", p->ref);
#endif
	    if(p != NULL){
	        
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_nak>>pbuf_alloc succeed\n");
	        debug_printf("dhcps: send_nak>>p->tot_len = %d\n", p->tot_len);
	        debug_printf("dhcps: send_nak>>p->len = %d\n", p->len);
#endif
	        q = p;
	        while(q != NULL){
	            data = (u8_t *)q->payload;
	            for(i=0; i<q->len; i++)
	            {
	                data[i] = ((u8_t *) m)[cnt++];
	            }

	            q = q->next;
	        }
	    }else{
	        
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_nak>>pbuf_alloc failed\n");
#endif
	        return;
    	}
        SendNak_err_t = udp_sendto( pcb_dhcps, p, &broadcast_dhcps, DHCPS_CLIENT_PORT );
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_nak>>udp_sendto result %x\n",SendNak_err_t);
#endif
 	    if(p->ref != 0){
#if DHCPS_DEBUG			
	        debug_printf("udhcp: send_nak>>free pbuf\n");
#endif
	        pbuf_free(p);
	    }
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * send_ack
 *
 * @param
 */
///////////////////////////////////////////////////////////////////////////////////
static void  send_ack(struct dhcps_msg *m)
{

		u8_t *end;
	    struct pbuf *p, *q;
	    u8_t *data;
	    u16_t cnt=0;
	    u16_t i;
		err_t SendAck_err_t;
        create_msg(m);


        end = add_msg_type(&m->options[4], DHCPACK);
        end = add_offer_options(end);
        end = add_end(end);
	    
	    p = pbuf_alloc(PBUF_TRANSPORT, sizeof(struct dhcps_msg), PBUF_RAM);
#if DHCPS_DEBUG
		debug_printf("udhcp: send_ack>>p->ref = %d\n", p->ref);
#endif
	    if(p != NULL){
	        
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_ack>>pbuf_alloc succeed\n");
	        debug_printf("dhcps: send_ack>>p->tot_len = %d\n", p->tot_len);
	        debug_printf("dhcps: send_ack>>p->len = %d\n", p->len);
#endif
	        q = p;
	        while(q != NULL){
	            data = (u8_t *)q->payload;
	            for(i=0; i<q->len; i++)
	            {
	                data[i] = ((u8_t *) m)[cnt++];
	            }

	            q = q->next;
	        }
	    }else{
	    
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_ack>>pbuf_alloc failed\n");
#endif
	        return;
	    }
        SendAck_err_t = udp_sendto( pcb_dhcps, p, &broadcast_dhcps, DHCPS_CLIENT_PORT );
#if DHCPS_DEBUG
	        debug_printf("dhcps: send_ack>>udp_sendto result %x\n",SendAck_err_t);
#endif
	    
	    if(p->ref != 0){
#if DHCPS_DEBUG
	        debug_printf("udhcp: send_ack>>free pbuf\n");
#endif
	        pbuf_free(p);
	    }
}
///////////////////////////////////////////////////////////////////////////////////
/*
 * parse_options
 *
 * @param
 * @return uint8_t
 */
///////////////////////////////////////////////////////////////////////////////////
static uint8_t  parse_options(uint8_t *optptr, int16_t len)
{
        ip_addr_t client;
    	bool is_dhcp_parse_end = false;
    	struct dhcps_state s;

        client.addr = *( (uint32_t *) &client_address);// 要锟斤拷锟斤拷锟紻HCP锟酵伙拷锟剿碉拷IP

        u8_t *end = optptr + len;
        u16_t type = 0;

        s.state = DHCPS_STATE_IDLE;

        while (optptr < end) {
#if DHCPS_DEBUG
        	debug_printf("dhcps: (int16_t)*optptr = %d\n", (int16_t)*optptr);
#endif
        	switch ((int16_t) *optptr) {

                case DHCP_OPTION_MSG_TYPE:	//53
                        type = *(optptr + 2);
                        break;

                case DHCP_OPTION_REQ_IPADDR://50
                        //debug_printf("dhcps:0x%08x,0x%08x\n",client.addr,*(uint32_t*)(optptr+2));
                        if( memcmp( (char *) &client.addr, (char *) optptr+2,4)==0 ) {
#if DHCPS_DEBUG
                    		debug_printf("dhcps: DHCP_OPTION_REQ_IPADDR = 0 ok\n");
#endif
                            s.state = DHCPS_STATE_ACK;
                        }else {
#if DHCPS_DEBUG
                        	debug_printf("dhcps: DHCP_OPTION_REQ_IPADDR != 0 err\n");
#endif
                            s.state = DHCPS_STATE_NAK;
                        }
                        break;
                case DHCP_OPTION_END:
			            {
			                is_dhcp_parse_end = true;
			            }
                        break;
            }

		    if(is_dhcp_parse_end){
		            break;
		    }

            optptr += optptr[1] + 2;
        }

        switch (type){
        
        	case DHCPDISCOVER://1
                s.state = DHCPS_STATE_OFFER;
#if DHCPS_DEBUG
                debug_printf("dhcps: DHCPD_STATE_OFFER\n");
#endif
                break;

        	case DHCPREQUEST://3
                if ( !(s.state == DHCPS_STATE_ACK || s.state == DHCPS_STATE_NAK) ) {
                    if(renew == true) {
                        s.state = DHCPS_STATE_ACK;
                    } else {
                        s.state = DHCPS_STATE_NAK;
                    }
#if DHCPS_DEBUG
                		debug_printf("dhcps: DHCPD_STATE_NAK\n");
#endif
                }
                break;

			case DHCPDECLINE://4
                s.state = DHCPS_STATE_IDLE;
#if DHCPS_DEBUG
            	debug_printf("dhcps: DHCPD_STATE_IDLE\n");
#endif
                break;

        	case DHCPRELEASE://7
                s.state = DHCPS_STATE_RELEASE;
#if DHCPS_DEBUG
            	debug_printf("dhcps: DHCPD_STATE_IDLE\n");
#endif
                break;
        }
#if DHCPS_DEBUG
    	debug_printf("dhcps: return s.state = %d\n", s.state);
#endif
        return s.state;
}
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
static int16_t  parse_msg(struct dhcps_msg *m, u16_t len)
{
    if(memcmp((char *)m->options,&magic_cookie,sizeof(magic_cookie)) == 0){
        ip_addr_t ip;
        memcpy(&ip.addr,m->ciaddr,sizeof(ip.addr));
        client_address.addr = dhcps_client_update(m->chaddr,&ip);
        int16_t ret = parse_options(&m->options[4], len);

        if(ret == DHCPS_STATE_RELEASE) {
            dhcps_client_leave(m->chaddr,&ip,TRUE); // force to delete
            client_address.addr = ip.addr;
        }

        if(ret == DHCPS_STATE_ACK)
        {
            debug_printf("%x:%x:%x:%x:%x:%x get ip addr %d.%d.%d.%d\n",
            		m->chaddr[0],m->chaddr[1],m->chaddr[2],m->chaddr[3],m->chaddr[4],m->chaddr[5],
            		ip4_addr1(&client_address),
            		ip4_addr2(&client_address),ip4_addr3(&client_address),ip4_addr4(&client_address));
        }

        return ret;
    }
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
static void handle_dhcp(void *arg,
									struct udp_pcb *pcb, 
									struct pbuf *p, 
									ip_addr_t *addr,
									uint16_t port)
{
		struct dhcps_msg *pmsg_dhcps = NULL;
		int16_t tlen = 0;
        u16_t i = 0;
	    u16_t dhcps_msg_cnt = 0;
	    u8_t *p_dhcps_msg = NULL;
	    u8_t *data = NULL;

#if DHCPS_DEBUG
    	debug_printf("dhcps: handle_dhcp-> receive a packet\n");
#endif
	    if (p==NULL) return;

	    pmsg_dhcps = (struct dhcps_msg *)calloc(1,sizeof(struct dhcps_msg));
	    if (NULL == pmsg_dhcps){
	    	pbuf_free(p);
	    	return;
	    }
	    p_dhcps_msg = (u8_t *)pmsg_dhcps;
		tlen = p->tot_len;
	    data = p->payload;

#if DHCPS_DEBUG
	    debug_printf("dhcps: handle_dhcp-> p->tot_len = %d\n", tlen);
	    debug_printf("dhcps: handle_dhcp-> p->len = %d\n", p->len);
#endif		

	    for(i=0; i<p->len; i++){
	        p_dhcps_msg[dhcps_msg_cnt++] = data[i];
	    }
		
		if(p->next != NULL) {
#if DHCPS_DEBUG
	        debug_printf("dhcps: handle_dhcp-> p->next != NULL\n");
	        debug_printf("dhcps: handle_dhcp-> p->next->tot_len = %d\n",p->next->tot_len);
	        debug_printf("dhcps: handle_dhcp-> p->next->len = %d\n",p->next->len);
#endif
			
	        data = p->next->payload;
	        for(i=0; i<p->next->len; i++){
	            p_dhcps_msg[dhcps_msg_cnt++] = data[i];
			}
		}

		/*
	     * DHCP 锟酵伙拷锟斤拷锟斤拷锟斤拷锟斤拷息锟斤拷锟斤拷
	    */
#if DHCPS_DEBUG
    	debug_printf("dhcps: handle_dhcp-> parse_msg(p)\n");
#endif
		
        switch(parse_msg(pmsg_dhcps, tlen - 240)) {

	        case DHCPS_STATE_OFFER://1
#if DHCPS_DEBUG            
            	 debug_printf("dhcps: handle_dhcp-> DHCPD_STATE_OFFER\n");
#endif			
	             send_offer(pmsg_dhcps);
	             break;
	        case DHCPS_STATE_ACK://3
#if DHCPS_DEBUG
            	 debug_printf("dhcps: handle_dhcp-> DHCPD_STATE_ACK\n");
#endif			
	             send_ack(pmsg_dhcps);
	             break;
	        case DHCPS_STATE_NAK://4
#if DHCPS_DEBUG            
            	 debug_printf("dhcps: handle_dhcp-> DHCPD_STATE_NAK\n");
#endif
	             send_nak(pmsg_dhcps);
	             break;
			default :
				 break;
        }
#if DHCPS_DEBUG
    	debug_printf("dhcps: handle_dhcp-> pbuf_free(p)\n");
#endif
        pbuf_free(p);
        free(pmsg_dhcps);
        pmsg_dhcps = NULL;
}
///////////////////////////////////////////////////////////////////////////////////
static void  init_dhcps_lease(uint32_t ip)
{
	uint32_t softap_ip = 0,local_ip = 0;
	uint32_t start_ip = 0;
	uint32_t end_ip = 0;

	if (dhcps_lease.enable == TRUE) {
		softap_ip = htonl(ip);
		start_ip = htonl(dhcps_lease.start_ip.addr);
		end_ip = htonl(dhcps_lease.end_ip.addr);
		/*config ip information can't contain local ip*/
		if ((start_ip <= softap_ip) && (softap_ip <= end_ip)) {
			dhcps_lease.enable = FALSE;
		} else {
			/*config ip information must be in the same segment as the local ip*/
			softap_ip >>= 8;
			if (((start_ip >> 8 != softap_ip) || (end_ip >> 8 != softap_ip))
					|| (end_ip - start_ip > DHCPS_MAX_LEASE)) {
				dhcps_lease.enable = FALSE;
			}
		}
	}

	if (dhcps_lease.enable == FALSE) {
		local_ip = softap_ip = htonl(ip);
		softap_ip &= 0xFFFFFF00;
		local_ip &= 0xFF;
		if (local_ip >= 0x80)
			local_ip -= DHCPS_MAX_LEASE;
		else
			local_ip ++;

		memset(&dhcps_lease,0, sizeof(dhcps_lease));
		dhcps_lease.start_ip.addr = softap_ip | local_ip;
		dhcps_lease.end_ip.addr = softap_ip | (local_ip + DHCPS_MAX_LEASE - 1);
		debug_printf("dhcp server start_ip = %d.%d.%d.%d, end_ip = %d.%d.%d.%d\n",
				ip4_addr4(&dhcps_lease.start_ip),ip4_addr3(&dhcps_lease.start_ip),ip4_addr2(&dhcps_lease.start_ip),ip4_addr1(&dhcps_lease.start_ip),
				ip4_addr4(&dhcps_lease.end_ip),ip4_addr3(&dhcps_lease.end_ip),ip4_addr2(&dhcps_lease.end_ip),ip4_addr1(&dhcps_lease.end_ip));
		dhcps_lease.start_ip.addr = htonl(dhcps_lease.start_ip.addr);
		dhcps_lease.end_ip.addr= htonl(dhcps_lease.end_ip.addr);
	}
}
///////////////////////////////////////////////////////////////////////////////////
void  dhcps_start(struct netif *apnetif, struct ip_info *info)
{
	if(pcb_dhcps != NULL) {
        udp_remove(pcb_dhcps);
    }

	pcb_dhcps = udp_new();
	if (pcb_dhcps == NULL || info ==NULL) {
		debug_printf("dhcps_start(): could not obtain pcb\n");
	}


	IP_ADDR4(&broadcast_dhcps, 255, 255, 255, 255);

	server_address = info->ip;
	init_dhcps_lease(server_address.addr);

	udp_bind(pcb_dhcps, IP_ADDR_ANY, DHCPS_SERVER_PORT);
	udp_recv(pcb_dhcps, handle_dhcp, NULL);
#if DHCPS_DEBUG
	debug_printf("dhcps:dhcps_start->udp_recv function Set a receive callback handle_dhcp for UDP_PCB pcb_dhcps\n");
#endif
		
}

void  dhcps_stop(struct netif *apnetif)
{

	udp_disconnect(pcb_dhcps);
    if(pcb_dhcps != NULL) {
        udp_remove(pcb_dhcps);
        pcb_dhcps = NULL;
    }

	list_node *pnode = NULL;
	list_node *pback_node = NULL;
	struct dhcps_pool* dhcp_node = NULL;
	ip_addr_t ip_zero;

	memset(&ip_zero,0x0,sizeof(ip_zero));
	pnode = plist;
	while (pnode != NULL) {
		pback_node = pnode;
		pnode = pback_node->pnext;
		node_remove_from_list(&plist, pback_node);
		dhcp_node = (struct dhcps_pool*)pback_node->pnode;
		//dhcps_client_leave(dhcp_node->mac,&dhcp_node->ip,TRUE); // force to delete
		free(pback_node->pnode);
		pback_node->pnode = NULL;
		free(pback_node);
		pback_node = NULL;
	}
}

/******************************************************************************
 * FunctionName : set_dhcps_lease
 * Description  : set the lease information of DHCP server
 * Parameters   : please -- Additional argument to set the lease information,
 * 							Little-Endian.
 * Returns      : true or false
*******************************************************************************/
bool  set_dhcps_lease(struct dhcps_lease *please)
{
	struct ip_info info;
	uint32_t softap_ip = 0;
	uint32_t start_ip = 0;
	uint32_t end_ip = 0;



	if (please == NULL )
		return false;

	if(please->enable) {
		memset(&info,0, sizeof(struct ip_info));
//		wifi_get_ip_info(SOFTAP_IF, &info);
		softap_ip = htonl(info.ip.addr);
		start_ip = htonl(please->start_ip.addr);
		end_ip = htonl(please->end_ip.addr);

		/*config ip information can't contain local ip*/
		if ((start_ip <= softap_ip) && (softap_ip <= end_ip))
			return false;

		/*config ip information must be in the same segment as the local ip*/
		softap_ip >>= 8;
		if ((start_ip >> 8 != softap_ip)
				|| (end_ip >> 8 != softap_ip)) {
			return false;
		}

		if (end_ip - start_ip > DHCPS_MAX_LEASE)
			return false;

		memset(&dhcps_lease, 0, sizeof(dhcps_lease));
//		dhcps_lease.start_ip.addr = start_ip;
//		dhcps_lease.end_ip.addr = end_ip;
		dhcps_lease.start_ip.addr = please->start_ip.addr;
		dhcps_lease.end_ip.addr = please->end_ip.addr;
	}
	dhcps_lease.enable = please->enable;
//	dhcps_lease_flag = false;
	return true;
}

/******************************************************************************
 * FunctionName : get_dhcps_lease
 * Description  : get the lease information of DHCP server
 * Parameters   : please -- Additional argument to get the lease information,
 * 							Little-Endian.
 * Returns      : true or false
*******************************************************************************/
bool  get_dhcps_lease(struct dhcps_lease *please)
{

	if (NULL == please)
		return false;


	if (dhcps_lease.enable == FALSE){
			return false;
	} else {
//		os_bzero(please, sizeof(dhcps_lease));
//		if (dhcps_status() == DHCP_STOPPED){
//			please->start_ip.addr = htonl(dhcps_lease.start_ip.addr);
//			please->end_ip.addr = htonl(dhcps_lease.end_ip.addr);
//		}
	}

//	if (dhcps_status() == DHCP_STARTED){
//		os_bzero(please, sizeof(dhcps_lease));
//		please->start_ip.addr = dhcps_lease.start_ip.addr;
//		please->end_ip.addr = dhcps_lease.end_ip.addr;
//	}
	please->start_ip.addr = dhcps_lease.start_ip.addr;
	please->end_ip.addr = dhcps_lease.end_ip.addr;
	return true;
}

static void  kill_oldest_dhcps_pool(void)
{
	list_node *pre = NULL, *p = NULL;
	list_node *minpre = NULL, *minp = NULL;
	struct dhcps_pool *pdhcps_pool = NULL, *pmin_pool = NULL;
	pre = plist;
	p = pre->pnext;
	minpre = pre;
	minp = p;
	while (p != NULL){
		pdhcps_pool = p->pnode;
		pmin_pool = minp->pnode;
		if (pdhcps_pool->lease_timer < pmin_pool->lease_timer){
			minp = p;
			minpre = pre;
		}
		pre = p;
		p = p->pnext;
	}
	minpre->pnext = minp->pnext;
	free(minp->pnode);
	minp->pnode = NULL;
	free(minp);
	minp = NULL;
}

void  dhcps_coarse_tmr(void)
{
	uint8_t num_dhcps_pool = 0;
	list_node *pback_node = NULL;
	list_node *pnode = NULL;
	struct dhcps_pool *pdhcps_pool = NULL;
	pnode = plist;
	while (pnode != NULL) {
		pdhcps_pool = pnode->pnode;
		if ( pdhcps_pool->type == DHCPS_TYPE_DYNAMIC) {
			pdhcps_pool->lease_timer --;
		}
		if (pdhcps_pool->lease_timer == 0){
			pback_node = pnode;
			pnode = pback_node->pnext;
			node_remove_from_list(&plist,pback_node);
			free(pback_node->pnode);
			pback_node->pnode = NULL;
			free(pback_node);
			pback_node = NULL;
		} else {
			pnode = pnode ->pnext;
			num_dhcps_pool ++;
		}
	}

	if (num_dhcps_pool >= MAX_STATION_NUM)
		kill_oldest_dhcps_pool();
}

bool  set_dhcps_offer_option(uint8_t level, void* optarg)
{
	bool offer_flag = true;
	uint8_t option = 0;
	if (optarg == NULL )
		return false;

	if (level <= OFFER_START || level >= OFFER_END)
		return false;

	switch (level){
		case OFFER_ROUTER:
			offer = (*(uint8_t *)optarg) & 0x01;
			offer_flag = true;
			break;
		default :
			offer_flag = false;
			break;
	}
	return offer_flag;
}

bool  set_dhcps_lease_time(uint32_t minute)
{

    if(minute == 0) {
        return false;
    }
    dhcps_lease_time = minute;
    return true;
}

bool  reset_dhcps_lease_time(void)
{

    dhcps_lease_time = DHCPS_LEASE_TIME_DEF;
    return true;
}

uint32_t  get_dhcps_lease_time(void) // minute
{
    return dhcps_lease_time;
}

void  dhcps_client_leave(u8 *bssid, ip_addr_t *ip,bool force)
{
    struct dhcps_pool *pdhcps_pool = NULL;
    list_node *pback_node = NULL;

    if ((bssid == NULL) || (ip == NULL)) {
        return;
    }

    for (pback_node = plist; pback_node != NULL;pback_node = pback_node->pnext) {
        pdhcps_pool = pback_node->pnode;
        if (memcmp(pdhcps_pool->mac, bssid, sizeof(pdhcps_pool->mac)) == 0){
            if (memcmp(&pdhcps_pool->ip.addr, &ip->addr, sizeof(pdhcps_pool->ip.addr)) == 0) {
                if ((pdhcps_pool->type == DHCPS_TYPE_STATIC) || (force)) {
                    if(pback_node != NULL) {
                        node_remove_from_list(&plist,pback_node);
                        free(pback_node);
                        pback_node = NULL;
                    }

                    if (pdhcps_pool != NULL) {
                        free(pdhcps_pool);
                        pdhcps_pool = NULL;
                    }
                } else {
                    pdhcps_pool->state = DHCPS_STATE_OFFLINE;
                }

                ip_addr_t ip_zero;
                memset(&ip_zero,0x0,sizeof(ip_zero));
                break;
            }
        }
    }
}

uint32_t  dhcps_client_update(u8 *bssid, ip_addr_t *ip)
{
    struct dhcps_pool *pdhcps_pool = NULL;
    list_node *pback_node = NULL;
    list_node *pmac_node = NULL;
    list_node *pip_node = NULL;
    bool flag = FALSE;
    uint32_t start_ip = dhcps_lease.start_ip.addr;
    uint32_t end_ip = dhcps_lease.end_ip.addr;
    dhcps_type_t type = DHCPS_TYPE_DYNAMIC;
    if (bssid == NULL) {
        return IPADDR_ANY;
    }

    if (ip) {
        if (IPADDR_BROADCAST == ip->addr) {
            return IPADDR_ANY;
        } else if (IPADDR_ANY == ip->addr) {
            ip = NULL;
        } else {
            type = DHCPS_TYPE_STATIC;
        }
    }

    renew = FALSE;
    for (pback_node = plist; pback_node != NULL;pback_node = pback_node->pnext) {
        pdhcps_pool = pback_node->pnode;
        //debug_printf("mac:"MACSTR"bssid:"MACSTR"\r\n",MAC2STR(pdhcps_pool->mac),MAC2STR(bssid));
        if (memcmp(pdhcps_pool->mac, bssid, sizeof(pdhcps_pool->mac)) == 0){
            pmac_node = pback_node;
            if (ip == NULL) {
                flag = TRUE;
                break;
            }
        }
        if (ip != NULL) {
            if (memcmp(&pdhcps_pool->ip.addr, &ip->addr, sizeof(pdhcps_pool->ip.addr)) == 0) {
                pip_node = pback_node;
            }
        } else if (flag == FALSE){
            if (memcmp(&pdhcps_pool->ip.addr, &start_ip, sizeof(pdhcps_pool->ip.addr)) != 0) {
                flag = TRUE;
            } else {
                start_ip = htonl((ntohl(start_ip) + 1));
            }
        }
    }

    if ((ip == NULL) && (flag == FALSE)) {
        if (plist == NULL) {
            if (start_ip <= end_ip) {
                flag = TRUE;
            } else {
                return IPADDR_ANY;
            }
        } else {
            if (start_ip > end_ip) {
                return IPADDR_ANY;
            }
            //start_ip = htonl((ntohl(start_ip) + 1));
            flag = TRUE;
        }
    }

    if (pmac_node != NULL) { // update new ip
        if (pip_node != NULL){
            pdhcps_pool = pip_node->pnode;

            if (pip_node != pmac_node) {
                if(pdhcps_pool->state != DHCPS_STATE_OFFLINE) { // ip is used
                    return IPADDR_ANY;
                }

                // mac exists and ip exists in other node,delete mac
                node_remove_from_list(&plist,pmac_node);
                free(pmac_node->pnode);
                pmac_node->pnode = NULL;
                free(pmac_node);
                pmac_node = pip_node;
                memcpy(pdhcps_pool->mac, bssid, sizeof(pdhcps_pool->mac));
            } else {
                renew = true;
                type = DHCPS_TYPE_DYNAMIC;
            }

            pdhcps_pool->lease_timer = DHCPS_LEASE_TIMER;
            pdhcps_pool->type = type;
            pdhcps_pool->state = DHCPS_STATE_ONLINE;

        } else {
            pdhcps_pool = pmac_node->pnode;
            if (ip != NULL) {
                pdhcps_pool->ip.addr = ip->addr;
            } else if (flag == TRUE) {
                pdhcps_pool->ip.addr = start_ip;
            } else {    // no ip to distribute
                return IPADDR_ANY;
            }

            node_remove_from_list(&plist,pmac_node);
            pdhcps_pool->lease_timer = DHCPS_LEASE_TIMER;
            pdhcps_pool->type = type;
            pdhcps_pool->state = DHCPS_STATE_ONLINE;
            node_insert_to_list(&plist,pmac_node);
        }
    } else { // new station
        if (pip_node != NULL) { // maybe ip has used
            pdhcps_pool = pip_node->pnode;
            if (pdhcps_pool->state != DHCPS_STATE_OFFLINE) {
                return IPADDR_ANY;
            }
            memcpy(pdhcps_pool->mac, bssid, sizeof(pdhcps_pool->mac));
            pdhcps_pool->lease_timer = DHCPS_LEASE_TIMER;
            pdhcps_pool->type = type;
            pdhcps_pool->state = DHCPS_STATE_ONLINE;
        } else {
            pdhcps_pool = (struct dhcps_pool *)calloc(1,sizeof(struct dhcps_pool));
            if (ip != NULL) {
                pdhcps_pool->ip.addr = ip->addr;
            } else if (flag == TRUE) {
                pdhcps_pool->ip.addr = start_ip;
            } else {    // no ip to distribute
                free(pdhcps_pool);
                return IPADDR_ANY;
            }
            if (pdhcps_pool->ip.addr > end_ip) {
                free(pdhcps_pool);
                return IPADDR_ANY;
            }
            memcpy(pdhcps_pool->mac, bssid, sizeof(pdhcps_pool->mac));
            pdhcps_pool->lease_timer = DHCPS_LEASE_TIMER;
            pdhcps_pool->type = type;
            pdhcps_pool->state = DHCPS_STATE_ONLINE;
            pback_node = (list_node *)calloc(1,sizeof(list_node ));
            pback_node->pnode = pdhcps_pool;
            pback_node->pnext = NULL;
            node_insert_to_list(&plist,pback_node);
        }
    }


    return pdhcps_pool->ip.addr;
}

