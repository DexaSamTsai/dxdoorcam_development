/*
 * tun.c
 *
 *  Created on: 2017年3月14日
 *      Author: Cox.Yang
 */
#include "includes.h"
#include "lwip/tcpip.h"
#include "lwip/debug.h"
#include "lwip/netif.h"
#include "lwip/igmp.h"
#include "lwip/snmp.h"
#include "netif/etharp.h"
#include "netif/tun.h"

extern void event_callback_withfd(int fd, enum netconn_evt evt, u16_t len);
int _fdlist_insert(FILE * fp);
int _fdlist_remove(int fd);
FILE * fd_to_fp(int fd);

struct tun_struct{
	struct netif netif;
	t_ertos_queuehandler read_q;
	int unblock_flag;
	int flag;
	int fd;
};

#if LWIP_SOCKET_SET_ERRNO
#ifndef set_errno
#define set_errno(err) do { if (err) { errno = (err); } } while(0)
#endif
#else /* LWIP_SOCKET_SET_ERRNO */
#define set_errno(err)
#endif /* LWIP_SOCKET_SET_ERRNO */

/** This is overridable for the rare case where more than 255 threads
 * select on the same socket...
 */
#ifndef SELWAIT_T
#define SELWAIT_T u8_t
#endif

/** Contains all internal pointers and states used for a socket */
struct lwip_sock {
  /** sockets currently are built on netconns, each socket has one netconn */
  struct tun_struct *tun;
  /** data that was left from the previous read */
  void *lastdata;
  /** offset in the data that was left from the previous read */
  u16_t lastoffset;
  /** number of times data was received, set by event_callback(),
      tested by the receive and select functions */
  s16_t rcvevent;
  /** number of times data was ACKed (free send buffer), set by event_callback(),
      tested by select */
  u16_t sendevent;
  /** error happened for this socket, set by event_callback(), tested by select */
  u16_t errevent;
  /** last error that occurred on this socket (in fact, all our errnos fit into an u8_t) */
  u8_t err;
  /** counter of how many threads are waiting for this socket using select */
  SELWAIT_T select_waiting;
  int fd;
};


static err_t netif_tun_output_ipv4(struct netif *netif, struct pbuf *p,const ip4_addr_t *ipaddr)
{
	return netif->linkoutput(netif,p);
}
#if LWIP_IPV6
static err_t netif_tun_output_ipv6(struct netif *netif, struct pbuf *p,const ip6_addr_t *ipaddr)
{
	return netif->linkoutput(netif,p);
}
#endif
extern void ertos_queue_show(t_ertos_queuehandler queuehandler);
static err_t tun_low_level_output(struct netif *netif, struct pbuf *p)
{
	err_t eret = ERR_OK;
	struct pbuf *p1;
	struct tun_struct * tun = (struct tun_struct *) netif->state;

	while(1)
	{
		if(!p) break;
//		debug_printf("tun_low_level_output p %x\n", p);
		p1 = pbuf_alloc(PBUF_RAW, p->len, PBUF_RAM);
		if(!p1){
			errno=ERR_MEM;
			return -1;
		}
		memcpy(p1->payload, p->payload, p->len);
		if(!ertos_queue_send(tun->read_q, &p1, 0)){
			eret = ERR_TIMEOUT;
			pbuf_free(p1);
			debug_printf("ertos_queue_send error\n");
			return -1;
		}
//		ertos_queue_show(tun->read_q);
		p=p->next;
		if(p)
			debug_printf("tun_low_level_output: get mutli pbuf\n");
	}
	event_callback_withfd(tun->fd,NETCONN_EVT_RCVPLUS,0);
	return eret;
}
static err_t netif_tun_input(struct pbuf *p, struct netif *inp)
{
	err_t eret;
	eret = tcpip_input(p, inp);
	if(eret != ERR_OK){
		pbuf_free(p);
	}
	return eret;
}

static err_t netif_tun_init(struct netif * netif)
{
#if LWIP_NETIF_HOSTNAME
	netif->hostname = "tunhost";
#endif

	/* initialize the snmp variables and counters inside the struct netif
	* ifSpeed: no assumption can be made!
	*/
	MIB2_INIT_NETIF(netif, snmp_ifType_softwareLoopback, 0);

	netif->name[0] = 't';
	netif->name[1] = 'u';
	debug_printf("add tun netif\n");
	#if LWIP_IPV4
	  netif->output = netif_tun_output_ipv4;
	#endif
	#if LWIP_IPV6
	  netif->output_ip6 = netif_tun_output_ipv6;
	#endif
	netif->linkoutput = tun_low_level_output;

	netif->hwaddr_len = (u8_t) ETHARP_HWADDR_LEN;
	memset(netif->hwaddr, 0, ETHARP_HWADDR_LEN);
	netif->mtu = 1500;
	netif->flags = (u8_t) ( NETIF_FLAG_BROADCAST | NETIF_FLAG_LINK_UP );

    return ERR_OK;
}


static err_t netif_tap_init(struct netif * netif)
{
#if LWIP_NETIF_HOSTNAME
	netif->hostname = "taphost";
#endif
	char *macaddr="\x8c\xdc\xd4\x33\x37\x2c"

	/* initialize the snmp variables and counters inside the struct netif
	* ifSpeed: no assumption can be made!
	*/
	MIB2_INIT_NETIF(netif, snmp_ifType_softwareLoopback, 0);

	netif->name[0] = 't';
	netif->name[1] = 'a';
	debug_printf("add tap netif\n");
	#if LWIP_IPV4
	  netif->output = etharp_output;
	#endif
	#if LWIP_IPV6
	  netif->output_ip6 = ethip6_output;
	#endif
	netif->linkoutput = tun_low_level_output;

	netif->hwaddr_len = (u8_t) ETHARP_HWADDR_LEN;
	memcpy(netif->hwaddr, macaddr, ETHARP_HWADDR_LEN);
	netif->mtu = 1500;
	netif->flags = (u8_t) ( NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP | NETIF_FLAG_LINK_UP );

	return ERR_OK;
}

static int netif_tun_add(struct tun_struct *tun)
{
	struct netif * netif_ret;
	// add net interface
	ip_addr_t ipaddr, netmask, gw;
	ipaddr_aton("10.168.1.6",&ipaddr);
	ipaddr_aton("255.255.255.0",&netmask);
	ipaddr_aton("10.168.1.5",&gw);
	memset(&tun->netif, 0, sizeof(tun->netif));
	debug_printf("add tun if\n");
	netif_ret = netif_add(&tun->netif, &ipaddr, &netmask, &gw, tun, netif_tun_init, netif_tun_input);
	if( netif_ret == NULL){
		debug_printf("Netif tun Add Fail, Fatal Error!!\n");
		return -1;
	}

	netif_set_link_up(netif_ret);
	netif_set_up(netif_ret);
	return 0;
}

static int netif_tap_add(struct tun_struct *tun)
{
	struct netif * netif_ret;
	ip_addr_t ipaddr, netmask, gw;
	ipaddr_aton("10.1.0.1",&ipaddr);
	ipaddr_aton("255.255.255.0",&netmask);
	ipaddr_aton("10.1.0.2",&gw);
	memset(&tun->netif, 0, sizeof(tun->netif));
	debug_printf("add tap if\n");
	netif_ret = netif_add(&tun->netif, &ipaddr, &netmask, &gw, tun, netif_tap_init, netif_tun_input);
	if( netif_ret == NULL ){
		debug_printf("Netif tun Add Fail, Fatal Error!!\n");
		return -1;
	}
	netif_set_link_up(netif_ret);
	netif_set_up(netif_ret);
	return 0;
}

static int tun_hw_fcntl(void * arg, int cmd, int val)
{
//	debug_printf("tun_hw_fcntl, arg %x,cmd %d, val %d\n",arg,cmd,val);
	struct tun_struct *tun = ((struct lwip_sock *)arg)->tun;
	struct ifreq *ifr = (struct ifreq *)val;
	t_ertos_queue * queue = (t_ertos_queue *)tun->read_q;
	switch(cmd)
	{
	case TUNSETIFF:
		if(ifr->ifr_flags & IFF_TUN)
			netif_tun_add(tun);
		else
			netif_tap_add(tun);
		break;
	case TUNPEEKREADQUEUE:
		ifr->ifr_qlen = queue->msgcnt;
		break;
	case TUNSETPERSIST:

		break;
	case F_SETFL:
		if(val == O_NONBLOCK)
		{
			debug_printf("set tun NONBLOCK\n");
			tun->unblock_flag = 1;
		}else{
			return 0;
		}
		break;
	default:
		return 0;
		break;
	}
	return 1;
}



static int tun_hw_close(void * arg)
{
	struct tun_struct *tun = ((struct lwip_sock *)arg)->tun;
	netif_remove(&tun->netif);
	ertos_queue_delete(tun->read_q);
	free(tun);
	return 0;
}



static ssize_t tun_hw_read(void * arg, void *buf, size_t count)
{
	struct tun_struct *tun = ((struct lwip_sock *)arg)->tun;
	struct pbuf *p;
	int len;
	int t1;

	if(ertos_queue_recv(tun->read_q, &p, tun->unblock_flag?0:WAIT_INF)){
		len = count > p->len? p->len:count;
		memcpy(buf,p->payload,len);
		pbuf_free(p);
		event_callback_withfd(tun->fd,NETCONN_EVT_RCVMINUS,0);
		return len;
	}
	set_errno(EAGAIN);
	return -1;
}



static ssize_t tun_hw_write(void * arg, const void *buf, size_t count)
{
	struct pbuf * p;
	struct tun_struct *tun = ((struct lwip_sock *)arg)->tun;
	int len;
	len = count > tun->netif.mtu? tun->netif.mtu:count;
	p = pbuf_alloc(PBUF_RAW, len, PBUF_RAM);
	if(!p){
		errno=ERR_MEM;
		return -1;
	}
	memcpy(p->payload, buf, len);
	if((errno=tun->netif.input(p,&tun->netif))!=ERR_OK)
	{
		return -1;
	}
	return len;
}


/**
 * Allocate a new socket for a given netconn.
 *
 * @param newconn the netconn for which to allocate a socket
 * @param accepted 1 if socket has been created by accept(),
 *                 0 if socket has been created by socket()
 * @return the index of the new socket; -1 on error
 */
static int
alloc_socket(struct tun_struct *newtun)
{
	struct lwip_sock * s = (struct lwip_sock *)malloc(sizeof(struct lwip_sock));
	if(s == NULL){
		errno = ENOMEM;
		return -1;
	}
	FILE * fp = (FILE *)malloc(sizeof(FILE));
	if(fp == NULL){
		errno = ENOMEM;
		free(s);
		return -1;
	}
	memset(fp, 0, sizeof(FILE));

	int fd = _fdlist_insert(fp);
	if(fd < 0){
		free(fp);
		free(s);
		return -1;
	}

	fp->flags |= FILE_FLAGS_RD;
	fp->flags |= FILE_FLAGS_WR;

	fp->hw_read = (ssize_t (*)(void * arg, void *buf, size_t count)) tun_hw_read;
	fp->hw_write = (ssize_t (*)(void * arg, const void *buf, size_t count)) tun_hw_write;
	fp->hw_close = (int (*)(void * arg)) tun_hw_close;
	fp->hw_fcntl = (int (*)(void * arg, int cmd, int val)) tun_hw_fcntl;
	fp->hw_seek = NULL;
	fp->hw_tell = NULL;

	fp->hw_arg = s;

    s->tun       = newtun;
    s->lastdata   = NULL;
    s->lastoffset = 0;
    s->rcvevent   = 0;
    s->sendevent  = 1;
    s->errevent   = 0;
    s->err        = 0;
    s->select_waiting = 0;
	s->fd = fd;

	return fd + LWIP_SOCKET_OFFSET;
}
int tun_open(const char * filename, int flags)
{
	int i;
	struct tun_struct *tun = calloc(sizeof(struct tun_struct),1);
	if(!tun)
	{
		debug_printf("tun open failed\n");
		errno = ENOMEM;
		return -1;
	}
	tun->read_q = ertos_queue_create(24, 4);

	i = alloc_socket(tun);

	if (i == -1) {
		tun_hw_close(tun);
		set_errno(ENFILE);
		return -1;
	}
	tun->fd = i;
	LWIP_DEBUGF(SOCKETS_DEBUG, ("%d\n", i));
	set_errno(0);
	return i;
}

ssize_t tun_write(int fd, void *buf, size_t count)
{
	FILE* fp;
	fp = fd_to_fp(fd);
	if(fp)
		return tun_hw_write(fp->hw_arg,buf,count);
	else
		return -1;
}

ssize_t tun_read(int fd, void *buf, size_t count)
{
	FILE* fp;
	fp = fd_to_fp(fd);
	if(fp)
		return tun_hw_read(fp->hw_arg,buf,count);
	else
		return -1;
}

int tun_close(int fd)
{
	FILE* fp;
	fp = fd_to_fp(fd);
	if(fp)
	{
		tun_hw_close(fp->hw_arg);
		free(fp->hw_arg);
		return 0;
	}
	else
		return -1;
}

int tun_ioctl(int fd, int cmd, int val)
{
	FILE* fp;
	fp = fd_to_fp(fd);
	if(fp)
		return tun_hw_fcntl(fp->hw_arg,cmd,val);
	else
		return -1;
}






