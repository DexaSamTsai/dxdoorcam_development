/*
 * tun.h
 *
 *  Created on: 2017年3月14日
 *      Author: Cox.Yang
 */

#ifndef SHARE_NETWORK_TUN_TUN_H_
#define SHARE_NETWORK_TUN_TUN_H_

struct ifreq {
#define IFNAMSIZ        16
        union
        {
                char    ifrn_name[IFNAMSIZ];            /* if name, e.g. "en0" */
        } ifr_ifrn;

        union {
                short   ifru_flags;
                int     ifru_ivalue;
        } ifr_ifru;
};

# define ifr_name       ifr_ifrn.ifrn_name      /* interface name       */
# define ifr_flags      ifr_ifru.ifru_flags     /* flags                */
# define ifr_qlen       ifr_ifru.ifru_ivalue    /* queue length         */

#include "if_tun.h"


int tun_open(const char * filename, int flags);
//tun_write can be replaced by write
ssize_t tun_write(int fd, void *buf, size_t count);
//tun_read can be replaced by read
ssize_t tun_read(int fd, void *buf, size_t count);
//tun_close can be replaced by close
int tun_close(int fd);
int tun_ioctl(int fd, int cmd, int val);

#endif /* SHARE_NETWORK_TUN_TUN_H_ */
