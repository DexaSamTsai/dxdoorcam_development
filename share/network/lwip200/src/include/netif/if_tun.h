/*
 *  Universal TUN/TAP device driver.
 *  Copyright (C) 1999-2000 Maxim Krasnyansky <max_mk@yahoo.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 */

#ifndef __IF_TUN_H
#define __IF_TUN_H

/* Read queue size */
#define TUN_READQ_SIZE	500
/* TUN device type flags: deprecated. Use IFF_TUN/IFF_TAP instead. */
#define TUN_TUN_DEV 	IFF_TUN
#define TUN_TAP_DEV	IFF_TAP
#define TUN_TYPE_MASK   0x000f

/* Ioctl defines */
#define TUNSETNOCSUM  200
#define TUNSETDEBUG   201
#define TUNSETIFF     202
#define TUNSETPERSIST 203
#define TUNSETOWNER   204
#define TUNSETLINK    205
#define TUNSETGROUP   206
#define TUNGETFEATURES 207
#define TUNSETOFFLOAD  208
#define TUNSETTXFILTER 209
#define TUNGETIFF       210
#define TUNGETSNDBUF    211
#define TUNSETSNDBUF   212
#define TUNATTACHFILTER 213
#define TUNDETACHFILTER 214
#define TUNGETVNETHDRSZ  215
#define TUNSETVNETHDRSZ 216
#define TUNSETQUEUE  217
#define TUNSETIFINDEX	218
#define TUNGETFILTER  219
#define TUNSETVNETLE 220
#define TUNGETVNETLE  221
/* The TUNSETVNETBE and TUNGETVNETBE ioctls are for cross-endian support on
 * little-endian hosts. Not all kernel configurations support them, but all
 * configurations that support SET also support GET.
 */
#define TUNSETVNETBE 222
#define TUNGETVNETBE  223

#define TUNPEEKREADQUEUE  224 //set get tun read queue msgcnt.

/* TUNSETIFF ifr flags */
#define IFF_TUN		0x0001
#define IFF_TAP		0x0002
#define IFF_NO_PI	0x1000
/* This flag has no real effect */
//#define IFF_ONE_QUEUE	0x2000
#define IFF_VNET_HDR	0x4000
#define IFF_TUN_EXCL	0x8000
#define IFF_MULTI_QUEUE 0x0100
#define IFF_ATTACH_QUEUE 0x0200
#define IFF_DETACH_QUEUE 0x0400
/* read-only flag */
#define IFF_PERSIST	0x0800
#define IFF_NOFILTER	0x1000

/* Socket options */
#define TUN_TX_TIMESTAMP 1

/* Features for GSO (TUNSETOFFLOAD). */
#define TUN_F_CSUM	0x01	/* You can hand me unchecksummed packets. */
#define TUN_F_TSO4	0x02	/* I can handle TSO for IPv4 packets */
#define TUN_F_TSO6	0x04	/* I can handle TSO for IPv6 packets */
#define TUN_F_TSO_ECN	0x08	/* I can handle TSO with ECN bits. */
#define TUN_F_UFO	0x10	/* I can handle UFO packets */

/* Protocol info prepended to the packets (when IFF_NO_PI is not set) */
#define TUN_PKT_STRIP	0x0001
struct tun_pi {
	u16  flags;
	u16  proto;
};

/*
 * Filter spec (used for SETXXFILTER ioctls)
 * This stuff is applicable only to the TAP (Ethernet) devices.
 * If the count is zero the filter is disabled and the driver accepts
 * all packets (promisc mode).
 * If the filter is enabled in order to accept broadcast packets
 * broadcast addr must be explicitly included in the addr list.
 */
#define TUN_FLT_ALLMULTI 0x0001 /* Accept all multicast packets */
struct tun_filter {
	u16  flags; /* TUN_FLT_ flags see above */
	u16  count; /* Number of addresses */
	u8   addr[0][6];
};

#endif /* __IF_TUN_H */
