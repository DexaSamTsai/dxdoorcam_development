#include "includes.h"
#include "network/wifi/wifi.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

//NOTE: arg cannot be in stack because it'll be send to other task.
typedef struct s_wifi_thread_msg
{
	int (*func)(void * arg);
	void * arg;
	t_ertos_queuehandler ret;
}t_wifi_thread_msg;

typedef enum e_wifi_thread_status
{
	WIFI_THREAD_STATUS_NULL = 0,
	WIFI_THREAD_STATUS_INIT,
	WIFI_THREAD_STATUS_START,		// Wlan chip is working(inited).
	WIFI_THREAD_STATUS_STOP,
	WIFI_THREAD_STATUS_PAUSE,
	WIFI_THREAD_STATUS_EXIT,
}t_wifi_thread_status;

ERTOS_QUEUE_DECLARE(wifi_thread_queue, 1, sizeof(t_wifi_thread_msg))

//NOTE: '_wifi_thread_status' should only be changed in wifi thread.
static volatile t_wifi_thread_status _wifi_thread_status = 0;
static volatile int _wifi_thread_input_check = 0;

/*
 * "early packets" means the packets received during wlan sleep.
 * early packets will be readed and bufferred after wlan is waked/inited.
 * User should call libwifi_receive_early_packets() to receive them to lwip stack after your (listen) sockets is ready.
 */
typedef struct s_wifi_pkt
{
	u8 * buf;
	u32 len;
	struct s_wifi_pkt * next;
}t_wifi_pkt;

static t_wifi_pkt * _wifi_early_packets = NULL;

// For thread synchronization.
static t_ertos_eventhandler _thread_init_evt = NULL;
static t_ertos_queuehandler _thread_exit_evt = NULL;
// For sendbuf synchronization.
static t_ertos_eventhandler _sendbuf_evt = NULL;
static int _sendbuf_iswait = 0;

static int _sendbuf_update(void)
{
	extern int wlan_sendbuf_isfull(void);
	if(_sendbuf_iswait == 1 && wlan_sendbuf_isfull() != 1){
		ertos_event_signal(_sendbuf_evt);
	}
	return 0;
}

// For wifi reconnect
static t_wifi_connect_arg  _wifi_connect_arg;
static t_wifi_scanconnect_arg  _wifi_scanconnect_arg;
static int t_wifi_scanconnect_flag = 0;
static int _wifi_reconnect_flag = 0;

static void * wifi_thread_main(void * arg)
{
	t_wifi_thread_msg msg;
	int polling_interval = (int)arg;
	int ret;
	_wifi_thread_status = WIFI_THREAD_STATUS_INIT;

#ifdef CONFIG_ERTOS_WATCHDOG_EN
	ertos_task_set_watchdog(NULL, CONFIG_ERTOS_WATCHDOG_TIMEOUT * 100); //this task not use WAIT_INF. The queue_recv timeout is only several ticks.
#endif

	if ( _thread_init_evt != NULL ) {
		ertos_event_signal( _thread_init_evt );
	}

	while( 1 ) {
		if(ertos_queue_recv(wifi_thread_queue, &msg, polling_interval) != 0){
			//msg available
			int ret = 0;
			if(msg.func){
				ret = msg.func(msg.arg);
			}
			if(msg.ret){
				//return value
				if(ertos_queue_send(msg.ret, &ret, 10000) == 0){ //TODO: NOTIMEOUT
					debug_printf("ERROR: wifi_thread queue ret timeout\n");
				}
			}

			if(_wifi_thread_status == WIFI_THREAD_STATUS_EXIT){
				_wifi_thread_status = WIFI_THREAD_STATUS_NULL;

				if(_thread_exit_evt != NULL){
					ertos_event_signal(_thread_exit_evt);
				}
				break;
			}
		}

		if(_wifi_thread_status != WIFI_THREAD_STATUS_START){  // Just 'continue' if Wlan chip is not inited yet.
			continue;
		}

		if(_wifi_reconnect_flag == 1 && WIFI_status() == 0){

			if(t_wifi_scanconnect_flag)
			{
				ret = WIFI_scanconnect(&_wifi_scanconnect_arg);
			}else{
				ret = WIFI_connect(&_wifi_connect_arg);
			}
			if(ret== 0){
				_wifi_reconnect_flag = 0;
			}else{
				sleep(1);	//FIXME: a delay is a must here??
				if(_wifi_scanconnect_arg.chanspec)
					_wifi_scanconnect_arg.chanspec = 0;
				continue;
			}

		}
		while(wlan_network_device_read());
		_sendbuf_update();
	}
	return NULL;
}

static int
wifi_create_thread( void* thrd, void *arg )
{
    pthread_t thread_id;

    if ( pthread_create( &thread_id, NULL, thrd, arg ) != 0 ) {
        printf( "normal thread create error: %s\n", strerror( errno ) );
        return -1;
    }

    pthread_detach( thread_id );

    return 0;
}

void libwifi_thread_input_notify(void)
{
	t_wifi_thread_msg msg;
	msg.func = NULL;
	msg.arg = NULL;
	msg.ret = NULL;

	_wifi_thread_input_check = 1;
	//notify without msg
	ertos_queue_send(wifi_thread_queue, &msg, 0);
}

static int wifi_thread_call(int(*func)(void * arg), void * arg)
{
	if(_wifi_thread_status == WIFI_THREAD_STATUS_NULL){
		//debug_printf("ERROR: %s() is aborted because wifi thread doesn't exist.\n", __func__);
		return -1;
	}

	t_ertos_queuehandler msgret = ertos_queue_create(1, sizeof(int));
	if(msgret == NULL){
		return -2;
	}
	t_wifi_thread_msg msg;
	msg.func = func;
	msg.arg = arg;
	msg.ret = msgret;

	if(ertos_queue_send(wifi_thread_queue, &msg, 10000) == 0){
		//queue timeout
		return -3;
	}

	int ret;
	while(ertos_queue_recv(msgret, &ret, 10000) == 0); //check return value without timeout, TODO: use NOTIMEOUT

	ertos_queue_delete(msgret);

	return ret;
}

#define MALLOC_ARG_OR_EXIT(name) \
	t_wifi_##name##_arg * arg = ertos_malloc(sizeof(t_wifi_##name##_arg)); \
	if(arg == NULL){ \
		return -1; \
	} \
	memset(arg, 0, sizeof(t_wifi_##name##_arg));

#define WIFI_THREAD_CALL(name) \
	int ret = wifi_thread_call((int(*)(void *)) name, (void *)(arg)); \
	ertos_free(arg); \
	return ret;

#define WIFI_THREAD_CALL_WITHARG(name, arg) \
	int ret = wifi_thread_call((int(*)(void *)) name, (void *)(arg)); \
	return ret;

static t_wifi_pkt * _wifi_early_packets_tail = NULL;	// a helper for enqueue fast
static int _wifi_enqueue_early_packet(u8 * buf, u32 len)
{
	t_wifi_pkt * pkt = (t_wifi_pkt *)malloc(sizeof(t_wifi_pkt));
	if(pkt == NULL){
		return -1;
	}
	pkt->buf = (u8 *)malloc(len);
	if(pkt->buf == NULL){
		return -1;
	}
	memcpy(pkt->buf, buf, len);
	pkt->len = len;
	pkt->next = NULL;

	if(_wifi_early_packets == NULL){
		_wifi_early_packets = _wifi_early_packets_tail = pkt;
		return 0;
	}

	_wifi_early_packets_tail->next = pkt;
	_wifi_early_packets_tail = pkt;

	return 0;
}

void netif_wifi_input_process(u8 * buf, int len);
extern void dumpbufwithname(char* title, void *buf, int len);
static int wlan_read_cb(u8* buf, int len, int index)
{
	if(_wifi_thread_status != WIFI_THREAD_STATUS_START){
		//dumpbufwithname("[Early Enqu]", buf, len);
		_wifi_enqueue_early_packet(buf, len);
		return 0;
	}

	netif_wifi_input_process(buf,len);
	return 0;
}

static int wlan_msg_hdl_cb(int message, void* args)
{
    debug_printf("%s(%d)\n", __func__,  message);

	extern int netif_wifi_set_link_up(void);
	extern int netif_wifi_set_link_down(void);
    switch(message){
        case WLAN_MSG_DISCONNECTED:
			netif_wifi_set_link_down();
            break;
        case WLAN_MSG_CONNECTED:
			netif_wifi_set_link_up();
            break;
        case WLAN_MSG_CONNECTED_AP:
            break;
        case WLAN_MSG_DISCONNECTED_AP:
            break;
        default:
            break;
    }

	return 0;
}

static int _wifi_receive_early_packets(void * arg)
{
	t_wifi_pkt ** pkt_p = &_wifi_early_packets;
	while((*pkt_p) != NULL){
		t_wifi_pkt * pkt = *pkt_p;
		*pkt_p = pkt->next;

		//dumpbufwithname("[Early Recv]", pkt->buf, pkt->len);
		netif_wifi_input_process(pkt->buf, pkt->len);

		free(pkt->buf);
		free(pkt);
	}

	_wifi_early_packets_tail = NULL;

	return 0;
}
int libwifi_receive_early_packets(void)
{
	return wifi_thread_call(_wifi_receive_early_packets, NULL);
}

int libwifi_packet_write(u8* wr_buf, int len, int priv_index)
{
	MALLOC_ARG_OR_EXIT(write);

	arg->wr_buf = wr_buf;
	arg->len = len;
	arg->priv_index = priv_index;

	WIFI_THREAD_CALL(wlan_network_device_write);
}

int libwifi_sendbuf_wait(void)
{
	_sendbuf_iswait = 1;
	ertos_event_wait(_sendbuf_evt, 100);
	_sendbuf_iswait = 0;

	return 0;
}

static int _wifi_init_func(void * arg)
{
	if(_wifi_thread_status != WIFI_THREAD_STATUS_INIT && _wifi_thread_status != WIFI_THREAD_STATUS_STOP){
		return 0;
	}

	int ret = WIFI_init(arg);
	if(ret) return ret;
#ifdef CONFIG_WIFIMODULE_BRCM4334X
	// 'wlan_setpm(0)' must be called for brcm4334, otherwise, the network performance will be very bad.
	extern int wlan_setpm(int pm);
	wlan_setpm(0);
#endif

	// read out early packets that received during wifi sleep.
	while(wlan_network_device_read());

	_wifi_thread_status = WIFI_THREAD_STATUS_START;
	_wifi_reconnect_flag = 0;

	return ret;
}

int libwifi_init(int sdio_id, u8 * mac, int mfg, t_wifi_conf * conf )
{
	MALLOC_ARG_OR_EXIT(init);

	arg->sdio_id = sdio_id;
	arg->mac = mac;
	arg->mfg = mfg;
	arg->msg_hdl_cb = wlan_msg_hdl_cb;
	arg->network_read_cb = wlan_read_cb;
	arg->conf = conf;

	memset(&_wifi_connect_arg, 0, sizeof(t_wifi_connect_arg));

	WIFI_THREAD_CALL(_wifi_init_func);
}

int libwifi_connect(char *ssid, char * key, u32 keylen, int *channel, int block, t_wifi_conf* conf)
{
	MALLOC_ARG_OR_EXIT(connect);

	static char m_ssid[33];
	static u8  m_key[128];
	static t_wifi_conf m_conf;
	static int mc_security;
	static int mc_wepauthtype;

	arg->ssid = ssid;
	arg->key = (u8 *)key;
	arg->keylen = keylen;
	arg->channel = channel;
	arg->block = block;
	arg->conf = conf;
	memset(&_wifi_connect_arg,0,sizeof(t_wifi_connect_arg));
	if(ssid)
	{
		strncpy(m_ssid, ssid, sizeof(m_ssid)-1);
		_wifi_connect_arg.ssid = m_ssid;
	}
	_wifi_connect_arg.keylen = keylen;

	if(key)
	{
		strncpy((char*)m_key,key,sizeof(m_key)-1);
		_wifi_connect_arg.key = m_key;
	}

	if(conf)
	{
		if(conf->wepauthtype) {
			mc_wepauthtype = *conf->wepauthtype;
			m_conf.wepauthtype = &mc_wepauthtype;
		}
		if(conf->wepauthtype) {
			mc_wepauthtype = *conf->wepauthtype;
			m_conf.wepauthtype = &mc_wepauthtype;
		}
		if(conf->security){
			mc_security = *conf->security;
			m_conf.security = &mc_security;
		}
	}
	_wifi_scanconnect_arg.block = block;

	WIFI_THREAD_CALL(WIFI_connect);
}

int libwifi_scanconnect(char *ssid, int ssidlen, u8 *bssid, u8* key, u32 keylen, int keyindex, u8 *pmk,
		u16 *chanspec,wifi_scan_security_t *scan_security, wifi_scan_encryption_t *scan_encryption,
		int block, t_wifi_conf* conf)
{
	static char m_ssid[33];
	static u8	m_bssid[6];
	static u8	m_key[128];
	static u8	m_pmk[32];
	static u16	m_chanspec;
	static wifi_scan_security_t m_scan_security;
	static wifi_scan_encryption_t m_scan_encryption;
	static t_wifi_conf m_conf;
	static int mc_security; // =0 none; =1 wep; =3 wap-psk/wap2-psk
	static int mc_wepauthtype; // =0 open system; =1 shared key


	MALLOC_ARG_OR_EXIT(scanconnect);

	memset(&_wifi_scanconnect_arg,0,sizeof(t_wifi_scanconnect_arg));
	_wifi_scanconnect_arg.ssidlen = ssidlen;
	if(ssid)
	{
		strncpy(m_ssid,ssid,sizeof(m_ssid)-1);
		_wifi_scanconnect_arg.ssid = m_ssid;
	}
	if(bssid)
	{
		memcpy(m_bssid,bssid,6);
		_wifi_scanconnect_arg.bssid = m_bssid;
	}
	_wifi_scanconnect_arg.keylen = keylen;
	if(key)
	{
		strncpy((char*)m_key,key,sizeof(m_key)-1);
		_wifi_scanconnect_arg.key = m_key;
	}
	if(pmk)
	{
		memcpy(m_pmk,pmk,sizeof(m_pmk));
		_wifi_scanconnect_arg.pmk = m_pmk;
	}
	if(chanspec)
	{
		m_chanspec = *chanspec;
		_wifi_scanconnect_arg.chanspec = &m_chanspec;
	}
	if(scan_security)
	{
		m_scan_security = *scan_security;
		_wifi_scanconnect_arg.scan_security = &m_scan_security;
	}
	if(scan_encryption)
	{
		m_scan_encryption = *scan_encryption;
		_wifi_scanconnect_arg.scan_encryption = &m_scan_encryption;
	}
	if(conf)
	{
		if(conf->wepauthtype) {
			mc_wepauthtype = *conf->wepauthtype;
			m_conf.wepauthtype = &mc_wepauthtype;
		}
		if(conf->security){
			mc_security = *conf->security;
			m_conf.security = &mc_security;
		}
		_wifi_scanconnect_arg.conf = &m_conf;
	}

	_wifi_scanconnect_arg.keyindex = keyindex;
	_wifi_scanconnect_arg.block = block;

	t_wifi_scanconnect_flag = 1;


	arg->ssid = ssid;
	arg->ssidlen = ssidlen;
	arg->bssid = bssid;
	arg->key = key;
	arg->keylen = keylen;
	arg->keyindex = keyindex;
	arg->pmk = pmk;
	arg->chanspec = chanspec;
	arg->scan_encryption = scan_encryption;
	arg->scan_security = scan_security;
	arg->conf = conf;
	arg->block = block;

	WIFI_THREAD_CALL(WIFI_scanconnect);

	if(arg->ssid)
		strncpy(_wifi_scanconnect_arg.ssid,arg->ssid,sizeof(_wifi_scanconnect_arg.ssid)-1);
	if(arg->bssid)
		memcpy(_wifi_scanconnect_arg.bssid,arg->bssid,6);
	if(arg->pmk)
		memcpy(_wifi_scanconnect_arg.pmk,arg->pmk,32);
	if(arg->chanspec)
		_wifi_scanconnect_arg.chanspec = arg->chanspec;
	if(arg->scan_encryption)
		_wifi_scanconnect_arg.scan_encryption = arg->scan_encryption;
	if(arg->scan_security)
		_wifi_scanconnect_arg.scan_security = arg->scan_security;



}

int libwifi_ap(char *ssid, u8* key, u32 keylen,int encrypt, int security, int *channel, t_wifi_conf* conf)
{
	MALLOC_ARG_OR_EXIT(ap);

	arg->ssid = ssid;
	arg->key = key;
	arg->keylen = keylen;
	arg->encrypt = encrypt;
	arg->security = security;
	arg->channel = channel;
	arg->conf = conf;

	WIFI_THREAD_CALL(WIFI_ap);
}

static int _wifi_reconnect_func(void * arg)
{
	if(_wifi_reconnect_flag != 0 || WIFI_status() != 0){
		return -1;
	}

	_wifi_reconnect_flag = 1;
	return 0;
}

int libwifi_reconnect(void)
{
	t_wifi_thread_msg msg;
	msg.func = _wifi_reconnect_func;
	msg.arg = NULL;
	msg.ret = NULL;

	if(ertos_queue_send(wifi_thread_queue, &msg, 10000) == 1){
		return -1;
	}

	return 0;
}

static int _wifi_stop_func(void * arg)
{
	if(_wifi_thread_status != WIFI_THREAD_STATUS_START){
		return 0;
	}

	_wifi_thread_status = WIFI_THREAD_STATUS_STOP;
    while(wlan_network_device_read());
	return WIFI_exit(arg);
}

int libwifi_stop(void)
{
	WIFI_THREAD_CALL_WITHARG(_wifi_stop_func, NULL);
}

int libwifi_down(void)
{
	return wifi_thread_call( WIFI_stop, NULL );
}

int libwifi_up(void)
{
	return wifi_thread_call( WIFI_up, NULL );
}

int libwifi_ioctl(int ioc, void * arg0, void * arg1)
{
	t_wifi_ioctl_arg * arg = ertos_malloc(sizeof(t_wifi_ioctl_arg));
	if(arg == NULL){
		return -1;
	}

	arg->ioc = ioc;
	arg->arg0 = arg0;
	arg->arg1 = arg1;

	int ret = wifi_thread_call( (int(*)(void * arg))WIFI_ioctl, (void *)arg );
	ertos_free(arg);
	return ret;
}

int wifi_thread_init(void)
{
	if(_wifi_thread_status != WIFI_THREAD_STATUS_NULL){
		dbg_warm( "_wifi_thread_status != WIFI_THREAD_STATUS_NULL" );
		return 0;
	}

	if(ertos_queue_init(wifi_thread_queue) == NULL){
		dbg_warm( "ertos_queue_init(wifi_thread_queue) == NULL" );
		return -1;
	}

	if((_thread_init_evt = ertos_event_create()) == NULL){
		dbg_warm( "(_thread_init_evt = ertos_event_create()) == NULL" );
		return -1;
	}

	t_wifi_ioctl_arg arg;
	arg.ioc = WIOCGETPOLLINT;
	int wifi_polling_interval = WIFI_ioctl(&arg);

	wifi_create_thread( wifi_thread_main, ( void * )wifi_polling_interval );
	if(ertos_event_wait(_thread_init_evt, 100000) != 1){
		dbg_warm( "ertos_event_wait(_thread_init_evt, 100000) != 1" );
		return -1;
	}

	ertos_event_delete(_thread_init_evt);
	_thread_init_evt = NULL;

	// create sendbuf synchronization event
	if((_sendbuf_evt = ertos_event_create()) == NULL){
		dbg_warm( "(_sendbuf_evt = ertos_event_create()) == NULL" );
		return -1;
	}

	dbg_warm();
	return 0;
}



static int _wifi_thread_exit_func(void * arg)
{
	_wifi_thread_status = WIFI_THREAD_STATUS_EXIT;
	return 0;
}

int wifi_thread_exit(void)
{
	dbg_warm();
	if(_wifi_thread_status == WIFI_THREAD_STATUS_NULL){
		return 0;
	}

	if(_wifi_thread_status == WIFI_THREAD_STATUS_START){
		//should 'stop' it first before 'exit', if ever 'start'
		debug_printf("WARNING: wifi_thread_exit() is called while wifi thread is running.\n");
		libwifi_stop();
	}

	// exit wifi thread
	if((_thread_exit_evt = ertos_event_create()) == NULL){
		return -1;
	}

	wifi_thread_call(_wifi_thread_exit_func, NULL);

	if(ertos_event_wait(_thread_exit_evt, 100000) != 1){
		return -1;
	}

	ertos_event_delete(_thread_exit_evt);
	_thread_exit_evt = NULL;

	// delete wifi thread queue
	ertos_queue_delete(wifi_thread_queue);

	// delete sendbuf synchronization event
	ertos_event_delete(_sendbuf_evt);
	_sendbuf_evt = NULL;

	return 0;
}

static void* wifi_msg_hdl_cb = NULL;
static void* wifi_packet_read_cb = NULL;
int wifi_thread_pause(void)
{
	if(_wifi_thread_status == WIFI_THREAD_STATUS_START)
	{
		_wifi_thread_status = WIFI_THREAD_STATUS_PAUSE;
		wifi_msg_hdl_cb = wlan_get_msg_hdl_cb();
		wifi_packet_read_cb = wlan_network_device_get_read_cb();
		wlan_set_msg_hdl_cb(NULL);
		wlan_network_device_set_read_cb(NULL);
	}
	return 0;
}

int wifi_thread_resume(void)
{
	if(_wifi_thread_status == WIFI_THREAD_STATUS_PAUSE)
	{
		_wifi_thread_status = WIFI_THREAD_STATUS_START;
		if(wifi_msg_hdl_cb)
			wlan_set_msg_hdl_cb(wifi_msg_hdl_cb);
		if(wifi_packet_read_cb)
			wlan_network_device_set_read_cb(wifi_packet_read_cb);
		wifi_msg_hdl_cb = NULL;
		wifi_packet_read_cb = NULL;
	}
	return 0;
}
