#include "includes.h"
#include "network/wifi/wifi.h"
#include "network/wifi/wifi_bcm4334.h"

static int _mfg_stop = 0;	// 1: stopping : -1: stopped

// mfg main func
static void * _mfg_process(void * arg)
{
	pthread_detach(pthread_self());

	debug_printf("\n!!! MFG MODE Ready !!!\n");

	while(_mfg_stop == 0){
		WIFI_mfg(0);
	}

	_mfg_stop = -1;
	return NULL;
}

static int _uart_irq_handler(void * arg)
{
    wlan_mfg_uart_handle();
	return 0;
}

#define MFG_STACKSIZE 10240
static char __attribute__((aligned(8))) _mfg_stack[MFG_STACKSIZE];

int libwifi_mfg_start(int sdio_id, u8 * mac, t_wifi_conf * conf)
{
	// FIXME: Should check conflicting using with other wifi threads that might exist.
	wlan_para_restore(NULL);

	// Init WIFI
	t_wifi_init_arg	arg;

	arg.sdio_id = sdio_id;
	arg.mac = mac;
	arg.mfg = 1;
	arg.msg_hdl_cb = NULL;
	arg.network_read_cb = NULL;
	arg.conf = conf;

	if(WIFI_init(&arg) != WIFI_SUCCESS){
		syslog(LOG_ERR, "[ERROR]: Wifi Init For MFG Fail, Fatal Error!!\n");
		return -1;
	}

#ifdef CONFIG_WIFIMODULE_BRCM4334X
	// 'wlan_setpm(0)' must be called for brcm4334, otherwise, the network performance will be very bad.
	//TODO: fix:  should call in WIFI_init
	extern int wlan_setpm(int pm);
	wlan_setpm(0);
#endif

	// Cleanup WIFI
    wlan_set_role(0,MLAN_BSS_ROLE_STA);

    if(wlan_is_ap_mode()){
        wlan_stop_ap();
    }else if(wlan_is_connected()){
		wlan_disconnect_net();
    }

	// Init MFG UART handler
#ifdef CONFIG_CONSOLE_UART1
	irq_free(IRQ_BIT_UART1);
	irq_request(IRQ_BIT_UART1, _uart_irq_handler, "UART", NULL);
#else
	irq_free(IRQ_BIT_UART0);
	irq_request(IRQ_BIT_UART0, _uart_irq_handler, "UART", NULL);
#endif

	// Cleanup mfg structure
	_mfg_stop = 0;

	// Start MFG thread
	pthread_attr_t mfg_attr;
	pthread_t mfg_thread;

	pthread_attr_init(&mfg_attr);
	pthread_attr_setstack(&mfg_attr, _mfg_stack, MFG_STACKSIZE);
	int ret = pthread_create(&mfg_thread, &mfg_attr, _mfg_process, NULL);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Create mfg_thread Fail, Fatal Error!\n");
		return -1;
	}

	return 0;
}

int libwifi_mfg_stop(void)
{
	if(_mfg_stop != 0){
		return 0;
	}

	_mfg_stop = 1;

	// FIXME: 
	while(_mfg_stop != -1){
		usleep(100000);		// 100ms
	}
	return 0;
}
