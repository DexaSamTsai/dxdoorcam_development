#include "includes.h"
#include "libeasycam.h"
#include "network/wifi/wifi.h"
#include "network/wifi/wifi_bcm4334.h"
#include "network/wifi/libwps.h"

static pthread_t _wps_thread;
static u8 _wps_wlan_buf[1500];
static u32 _wps_wlan_len = 0;

static u8 _wps_ssid[32];
static u8 _wps_key[64];
static u8 _wps_pin[9];
static int _wps_security;
static u8 _wps_bssid[6];


static int _wps_stop = 0;	// 1: stopping : -1: stopped
static int _wps_wlan_associated = 0;	// 1: associated; 0: disassociated

extern void dumpbuf(void *buf, int len);
extern void dumpbufwithname(char* title, void *buf, int len);

static void _wps_scan_filter(void *ctx, wlan_scan_resp_t *pbss)
{
	char buf[33];
	my_aplist_t *paplist = (my_aplist_t *) ctx;
	my_ap_t *pap;
	int i;int ret;
	pap = &paplist->ap_array[0];

	for (i=0; i< paplist->apcnt && i < MY_APLIST_MAX_APCNT;) {

		if (memcmp(pbss->bssid, pap->bssid, sizeof(pap->bssid)) == 0) {
			/*  The ap info already here  */
			if (!(pap->flags &WLAN_SCAN_RES_FG_BEACON) && (pbss->flags &WLAN_SCAN_RES_FG_BEACON)) {
				/*  DO NOT update a probe response with a beacon  */
				return;
			}
			/*  Update ap information */
			if (pap->rssi < pbss->rssi)
				pap->rssi = pbss->rssi;

			if ((pap->flags &WLAN_SCAN_RES_FG_BEACON) && !(pbss->flags &WLAN_SCAN_RES_FG_BEACON)) {
				/*  Update IE */
			}
			pap->flags = pbss->flags;
			return;
		}
		pap = &paplist->ap_array[++i];
	}
	if (paplist->apcnt >= MY_APLIST_MAX_APCNT) {
		/*  Too many ap  */
		syslog(LOG_ERR, "WPS error: %s() too many ap", __func__);
		return;
	}
/*
	syslog(LOG_INFO, "bss%d: fg=0x%x, rssi=%d ch=%d band=%s bssid=%x:%x:%x:%x:%x:%x, ssid = %s,len=%d\n", paplist->apcnt, pbss->flags,
			pbss->rssi, pbss->channel, (pbss->flags & WLAN_SCAN_RES_FG_BAND_5G) ? "5G" : "2.4G",
			pbss->bssid[0], pbss->bssid[1], pbss->bssid[2], pbss->bssid[3], pbss->bssid[4], pbss->bssid[5],
			pbss->ssid,pbss->ssid_len);
*/
//    debug_printf(".");
    ret=is_wps_pbc(pbss->pie, pbss->ie_length);
/*
if(ret==2)
{
	syslog(LOG_INFO, "bss%d: fg=0x%x, rssi=%d ch=%d band=%s bssid=%x:%x:%x:%x:%x:%x, ssid = %s,len=%d\n", paplist->apcnt, pbss->flags,
			pbss->rssi, pbss->channel, (pbss->flags & WLAN_SCAN_RES_FG_BAND_5G) ? "5G" : "2.4G",
			pbss->bssid[0], pbss->bssid[1], pbss->bssid[2], pbss->bssid[3], pbss->bssid[4], pbss->bssid[5],
			pbss->ssid,pbss->ssid_len);
	dumpbuf(pbss->pie, pbss->ie_length);
}
*/
    if(ret==2)
    {
    	if(strlen(_wps_pin))
    	{
		goto scan_find;
    	}
    }
    if(ret==1)
	{
		goto scan_find;
	}


scan_find:
    if(ret>0){
        /*  A new AP  */
    	memcpy(buf,pbss->ssid,pbss->ssid_len);
    	buf[pbss->ssid_len]=0;
		/*
		syslog(LOG_INFO, "bss%d: fg=0x%x, rssi=%d ch=%d band=%s bssid=%x:%x:%x:%x:%x:%x, ssid = %s,len=%d\n", paplist->apcnt, pbss->flags,
				pbss->rssi, pbss->channel, (pbss->flags & WLAN_SCAN_RES_FG_BAND_5G) ? "5G" : "2.4G",
				pbss->bssid[0], pbss->bssid[1], pbss->bssid[2], pbss->bssid[3], pbss->bssid[4], pbss->bssid[5],
				buf,pbss->ssid_len);
		*/

        memcpy(pap->ssid, pbss->ssid, pbss->ssid_len);
        pap->ssid_len=pbss->ssid_len;
        memcpy(pap->bssid, pbss->bssid, sizeof(pap->bssid));
        pap->beacon_period = pbss->beacon_period;
        pap->capability  = pbss->capability;
        pap->security = pbss->security;
        pap->scan_security = pbss->scan_security;
        pap->scan_encryption = pbss->scan_encryption;
        pap->rssi = pbss->rssi;
        pap->channel = pbss->channel;
        pap->flags = pbss->flags;
        paplist->apcnt++;
    }
	return;
scan_failure:
	return;
}

static my_aplist_t aplist;
// wps main func
static void * _wps_process(void * arg)
{
	pthread_detach(pthread_self());

	pthread_setname(NULL, "WPS");

    // Scan for an AP
	u32 wps_scan_timeout;
	u32 wps_starttime;

wps_restart:
	wps_scan_timeout = g_network_cfg.wifi.wps.scan_timeout * TICKS_PER_SEC;
    wps_starttime = ticks;
    while( (wps_scan_timeout == 0 || (ticks - wps_starttime) < wps_scan_timeout) && _wps_stop == 0){
        memset(&aplist, 0, sizeof(aplist));
        wlan_wps_scan_networks(NULL, _wps_scan_filter, (void*) &aplist);
        if(aplist.apcnt > 0){
			break;
		}
		sleep(2);
    }

	if(_wps_stop == 1){
		goto scan_fail;
	}

    if(aplist.apcnt <= 0)
    {
		syslog(LOG_ERR, "WPS error: scan timeout\n");
		goto scan_fail;
    }

	if(aplist.apcnt > 1){
		syslog(LOG_ERR, "WPS error: multi AP scanned(%d)", aplist.apcnt); 
		int i;
		for(i = 0;i < aplist.apcnt; i++){
			syslog(LOG_ERR, ":%s", aplist.ap_array[i].ssid);
		}
		syslog(LOG_ERR, "\n");

		goto scan_fail;
	}

    memcpy(_wps_ssid, aplist.ap_array[0].ssid, aplist.ap_array[0].ssid_len);
    _wps_ssid[aplist.ap_array[0].ssid_len] = 0;

    memcpy(_wps_bssid, aplist.ap_array[0].bssid, sizeof(aplist.ap_array[0].bssid));

	wifi_scan_security_t	scan_security;
	wifi_scan_encryption_t	scan_encryption;
    syslog(LOG_INFO, "WPS find AP(ssid %s, bssid %x:%x:%x:%x:%x:%x,scan_security:%d, scan_encryption:%d, security:%d)\n", _wps_ssid, _wps_bssid[0],_wps_bssid[1],_wps_bssid[2],_wps_bssid[3],_wps_bssid[4],_wps_bssid[5],
    		aplist.ap_array[0].scan_security, aplist.ap_array[0].scan_encryption,aplist.ap_array[0].security);

    _wps_security = -1;
    if(aplist.ap_array[0].scan_security==WIFI_SCAN_SECURITY_OPEN)
    	_wps_security=0;
    else if(aplist.ap_array[0].scan_security==WIFI_SCAN_SECURITY_WEP)
    	_wps_security=1;
    else if(aplist.ap_array[0].scan_security >= WIFI_SCAN_SECURITY_WPA && aplist.ap_array[0].scan_security <= WIFI_SCAN_SECURITY_WPA_WPA2_PSK)
    	_wps_security=2;



	// Associate the scanned AP
    if(wlan_wps_associate_networks((char *)_wps_ssid) != 0){
        syslog(LOG_ERR, "WPS error: associate AP \"%s\" failed\n", _wps_ssid);
		goto scan_fail;
    }

	_wps_wlan_associated = 1;

	// Init WPS
	u8 mac[6];
    wlan_get_mac(mac);
	u8 keylen;
    wps_supplicant_init(mac,_wps_bssid,(u8*)_wps_ssid,(u8*)_wps_key,&keylen,_wps_pin);

	// Process WPS
    int ret = 0;
	struct wpabuf * wparbuf, *wpasbuf;

	#define WPS_DEFAULT_TIMEOUT		120  // 2min
	u32 wps_timeout = WPS_DEFAULT_TIMEOUT * TICKS_PER_SEC;

    wps_starttime = ticks;
    while((ticks - wps_starttime) < wps_timeout && _wps_stop == 0){
		wparbuf = NULL;
		wpasbuf = NULL;

        _wps_wlan_len = 0;
        wlan_network_device_read();
        if(_wps_wlan_len > 0){
			//dumpbufwithname("[Coolen_Recv]", _wps_wlan_buf, _wps_wlan_len);
        	wparbuf = eapol_recv(_wps_wlan_buf,_wps_wlan_len);
        }
        wpasbuf = wps_supplicant_process(wparbuf);
        wpabuf_free(wparbuf);

        if(wpasbuf && wpasbuf->used){
        	_wps_wlan_len = eapol_send(wpasbuf,_wps_wlan_buf);
			t_wifi_write_arg arg;
			arg.wr_buf = _wps_wlan_buf;
			arg.len = _wps_wlan_len;
			arg.priv_index = 0;
			//dumpbufwithname("[COOLEN_Send]", _wps_wlan_buf, _wps_wlan_len);
        	wlan_network_device_write(&arg);
        }

        if((ret = is_wps_supplicant_done()) != 0){
			if(ret == 1){
				//success
        		if(wpasbuf && wpasbuf->used && _wps_wlan_len > 0){
#define WPS_WSCDONE_INTERVAL 0.5
#define WPS_WSCDONE_RETRY 5
					int wscdone_retry = 0;
					for(; wscdone_retry < WPS_WSCDONE_RETRY; wscdone_retry ++){
						ertos_timedelay(WPS_WSCDONE_INTERVAL * 100); //delay

						t_wifi_write_arg arg;
						arg.wr_buf = _wps_wlan_buf;
						arg.len = _wps_wlan_len;
						arg.priv_index = 0;
        				wlan_network_device_write(&arg);
					}
				}else{
					syslog(LOG_WARNING, "WPS WSCDone send retry failed\n");
				}
				sleep(1); //wait for last WSC_Done message be sent out.
			}
			break;
		}

		if(_wps_wlan_associated == 0){
			wps_supplicant_deinit();
			syslog(LOG_INFO, "WPS restart for unexpected disassociation!\n"); 
			goto wps_restart;
		}
    }

	if(_wps_stop == 1){
		goto wps_fail;
	}

	if(ret == 1){
		strncpy((char *)g_network_cfg.wifi.wps.ssid, (char *)_wps_ssid, sizeof(_wps_ssid));
		strncpy((char *)g_network_cfg.wifi.wps.key, (char *)_wps_key, sizeof(_wps_key));
		g_network_cfg.wifi.wps.security = _wps_security;
		memcpy(g_network_cfg.wifi.wps.bssid, _wps_bssid, sizeof(_wps_bssid));
		g_network_cfg.wifi.wps.is_done = 1;
		goto exit;
	}else if(ret == 2){
		syslog(LOG_ERR, "WPS error: process fail\n");
	}else if(ret == 0){
		syslog(LOG_ERR, "WPS error: process timeout\n");
	}else{
	}

wps_fail:
	wps_supplicant_deinit();

scan_fail:
	g_network_cfg.wifi.wps.is_done = -1;

exit:
	if(wlan_is_connected()){
        wlan_wps_disassociate_networks(NULL);
	}
	_wps_stop = -1;
	return NULL;
}

static int _wps_wlan_read_cb(u8* buf, int len, int index)
{
    memcpy(_wps_wlan_buf, buf, len);	
	_wps_wlan_len = len;
	return 0;
}

static int _wps_wlan_msg_hdl_cb(int message, void* args)
{
    debug_printf("%s(%d)\n", __func__,  message);

    switch(message){
        case WLAN_MSG_DISCONNECTED:
			if(_wps_wlan_associated == 1){
				_wps_wlan_associated = 0;
			}
            break;
        case WLAN_MSG_CONNECTED:
            break;
        case WLAN_MSG_CONNECTED_AP:
            break;
        case WLAN_MSG_DISCONNECTED_AP:
            break;
        default:
            break;
    }

	return 0;
}
//
#define WPS_STACKSIZE 40960
static char __attribute__((aligned(8))) _wps_stack[WPS_STACKSIZE];

int libwifi_wps_start(int sdio_id, u8 * mac, int mfg, t_wifi_conf * conf)
{
	// Init WIFI
	t_wifi_init_arg	arg;

	arg.sdio_id = sdio_id;
	arg.mac = mac;
	arg.mfg = mfg;
	arg.msg_hdl_cb = _wps_wlan_msg_hdl_cb;
	arg.network_read_cb = _wps_wlan_read_cb;
	arg.conf = conf;
	DxDoorCam_wlan_restore_para();
	if(WIFI_init(&arg) != WIFI_SUCCESS){
		syslog(LOG_ERR, "[ERROR]: Wifi Init For WPS Fail, Fatal Error!!\n");
		return -1;
	}
	easycam_wlan_save_para();
#ifdef CONFIG_WIFIMODULE_BRCM4334X
	// 'wlan_setpm(0)' must be called for brcm4334, otherwise, the network performance will be very bad.
	//TODO: fix:  should call in WIFI_init
	extern int wlan_setpm(int pm);
	wlan_setpm(0);
#endif

	// Cleanup WIFI
    wlan_set_role(0,MLAN_BSS_ROLE_STA);

    if(wlan_is_ap_mode()){
        wlan_stop_ap();
    }else if(wlan_is_connected()){
        wlan_wps_disassociate_networks(NULL);
    }

	// Cleanup wps structure
	memset(g_network_cfg.wifi.wps.ssid, 0, sizeof(g_network_cfg.wifi.wps.ssid));
	memset(g_network_cfg.wifi.wps.key, 0, sizeof(g_network_cfg.wifi.wps.key));
	g_network_cfg.wifi.wps.is_done = 0;

	memset(_wps_ssid, 0, sizeof(_wps_ssid));
	memset(_wps_key, 0, sizeof(_wps_key));
	memset(_wps_pin, 0, sizeof(_wps_pin));
	_wps_stop = 0;
	_wps_wlan_associated = 0;

	if(g_network_cfg.wifi.wps.pin[0] != '\0'){
		strncpy(_wps_pin, g_network_cfg.wifi.wps.pin, 8);
		syslog(LOG_INFO, "[INFO]:  wps_thread wps pin[%s]\n",_wps_pin);
	}
	// Start WPS thread
	pthread_attr_t wps_attr;

	pthread_attr_init(&wps_attr);
	pthread_attr_setstack(&wps_attr, _wps_stack, WPS_STACKSIZE);
	int ret = pthread_create(&_wps_thread, &wps_attr, _wps_process, NULL);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR]: Create wps_thread Fail, Fatal Error!\n");
		return -1;
	}

	return 0;
}

int libwifi_wps_stop(void)
{
	if(_wps_stop != 0){
		return 0;
	}

	_wps_stop = 1;

	// FIXME: 
	while(_wps_stop != -1){
		usleep(100000);		// 100ms
	}

	pthread_detach(_wps_thread);

	// unInit Wlan chip
	WIFI_exit(NULL);

	return 0;
}

//FIXME: Used by libwifiwps/wpa_supplicant/wps_supplicant.c
int wlan_wps_set_ie_cb(void *ctx, struct wpabuf *beacon_ie,struct wpabuf *probe_resp_ie)
{
	return 0;
}
