#ifndef __BRCM_WL_INCLUDE__
#define __BRCM_WL_INCLUDE__


#define wl_chspec_to_legacy __wl_chspec_to_legacy
#define wl_chspec_to_driver __wl_chspec_to_driver
#define wl_chspec32_to_driver __wl_chspec32_to_driver

#define wl_chspec_from_driver __wl_chspec_from_driver
#define wl_chspec_from_legacy _wl_chspec_from_legacy
#define wl_chspec32_from_driver __wl_chspec32_from_driver

#define rate_string2int __rate_string2int
#define rate_int2string __rate_int2string

#define wl_format_ssid __wl_format_ssid
#define dump_rateset __dump_rateset

#define wf_chspec_aton __wf_chspec_aton
#define wf_channel2mhz __wf_channel2mhz
#define wf_chspec_malformed __wf_chspec_malformed
#define wf_channel2chspec __wf_channel2chspec
#define wf_chspec_ctlchspec __wf_chspec_ctlchspec
#define wf_chspec_ctlchan __wf_chspec_ctlchan
#define wf_chspec_malformed __wf_chspec_malformed
#define wf_mhz2channel __wf_mhz2channel
#define wf_chspec_ntoa __wf_chspec_ntoa
#define wf_chspec_valid __wf_chspec_valid

#define cca_per_chan_summary __cca_per_chan_summary
#define wl_cntbuf_to_xtlv_format __wl_cntbuf_to_xtlv_format
#define cca_analyze __cca_analyze


////////////////////////////////////////////////////////////

struct sockaddr_ll
{
	unsigned short int sll_family;
	unsigned short int sll_protocol;
	int sll_ifindex;
	unsigned short int sll_hatype;
	unsigned char sll_pkttype;
	unsigned char sll_halen;
	unsigned char sll_addr[8]; 
};


#define	PF_PACKET	17	/* Packet family.  */
#define	AF_PACKET	PF_PACKET
#define SIOCGIFINDEX	0x8933		/* name -> if_index mapping	*/

typedef int sig_atomic_t;


#define IFHWADDRLEN	6
#define IFNAMSIZ	16

struct ifreq 
{
	int ifr_ifindex;
	
	union
	{
		char	ifrn_name[IFNAMSIZ];		/* if name, e.g. "en0" */
	} ifr_ifrn;
	
	union {
		struct	sockaddr ifru_addr;
		struct	sockaddr ifru_dstaddr;
		struct	sockaddr ifru_broadaddr;
		struct	sockaddr ifru_netmask;
		struct  sockaddr ifru_hwaddr;
		short	ifru_flags;
		int	ifru_ivalue;
		int	ifru_mtu;
//		struct  ifmap ifru_map;
		char	ifru_slave[IFNAMSIZ];	/* Just fits the size */
		char	ifru_newname[IFNAMSIZ];
		void *	ifru_data;
//		struct	if_settings ifru_settings;
	} ifr_ifru;
};

#define ifr_name	ifr_ifrn.ifrn_name

int ioctl (int __fd, unsigned long int __request, ...);

#endif
