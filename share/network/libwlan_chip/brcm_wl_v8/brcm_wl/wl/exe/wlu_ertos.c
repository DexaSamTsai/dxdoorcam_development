/*
 * Linux port of wl command line utility
 *
 * Copyright (C) 2011, Broadcom Corporation
 * All Rights Reserved.
 * 
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied
 * or duplicated in any form, in whole or in part, without the prior
 * written permission of Broadcom Corporation.
 *
 * $Id: wlu_linux.c 301294 2011-12-07 13:04:13Z $
 */
// For TARGETENV_ertos
#include "includes.h"
#include "network/wifi/wifi.h"
#include "network/wifi/wifi_bcm4334.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//#include <sys/ioctl.h>
//#include <net/if.h>
#include <proto/ethernet.h>
#include <proto/bcmip.h>

//#include <linux/sockios.h>
//#include <linux/ethtool.h>
//#include <signal.h>
#include <typedefs.h>
#include <wlioctl.h>
#include <bcmutils.h>
//#include <sys/wait.h>
#include <netdb.h>
//#include <netinet/in.h>
#include "wlu.h"
#include <bcmcdc.h>
#include "wlu_remote.h"
#include "wlu_client_shared.h"
#include "wlu_pipe.h"
#include <miniopt.h>

#define DEV_TYPE_LEN					3 /* length for devtype 'wl'/'et' */
#define INTERACTIVE_NUM_ARGS			15
#define INTERACTIVE_MAX_INPUT_LENGTH	256
#define NO_ERROR						0
#define RWL_WIFI_JOIN_DELAY				5

/* Function prototypes */
static cmd_t *wl_find_cmd(char* name);
static int do_interactive(struct ifreq *ifr);
static int wl_do_cmd(struct ifreq *ifr, char **argv);
int process_args(struct ifreq* ifr, char **argv);
extern int g_child_pid;
/* RemoteWL declarations */
int remote_type = NO_REMOTE;
int rwl_os_type = LINUX_OS;
//static bool rwl_dut_autodetect = TRUE;
//static bool debug = FALSE;
extern char *g_rwl_buf_mac;
extern char* g_rwl_device_name_serial;
unsigned short g_rwl_servport;
char *g_rwl_servIP = NULL;
unsigned short defined_debug = DEBUG_ERR | DEBUG_INFO;
static uint interactive_flag = 0;
extern char *remote_vista_cmds[];
extern char g_rem_ifname[IFNAMSIZ];
#if 0
static void
syserr(char *s)
{
	fprintf(stderr, "%s: ", wlu_av0);
	perror(s);
	exit(errno);
}
#endif

int
wl_ioctl(void *wl, int cmd, void *buf, int len, bool set)
{
	extern int dhd_wl_ioctl_raw(void * wl, int cmd, void * buf, int len, bool set);
	return dhd_wl_ioctl_raw(wl, cmd, buf, len, set);
}

static int
wl_find(struct ifreq *ifr)
{
	strncpy(ifr->ifr_name, "wl0", IFNAMSIZ);
	return BCME_OK;

	//FIXME: we can use 'lwip netif' to simulate the ifreq, but that will make brcm_wl depend on lwip. 
#if 0
	int status;

#define NETIF_DEFAULT	"wl0" 		
	if(netif_find(NETIF_DEFAULT) != NULL){
		strncpy(ifr->ifr_name, NETIF_DEFAULT, IFNAMSIZ);
		status =  BCME_OK;
	}else{
		memset(ifr->ifr_name, 0, IFNAMSIZ);
		status = BCME_ERROR;
	}

	return status;
#endif
}


static int
ioctl_queryinformation_fe(void *wl, int cmd, void* input_buf, unsigned long *input_len)
{
	int error = NO_ERROR;

	if (remote_type == NO_REMOTE) {
		error = wl_ioctl(wl, cmd, input_buf, *input_len, FALSE);
	} else {
		error = rwl_queryinformation_fe(wl, cmd, input_buf,
		              input_len, 0, REMOTE_GET_IOCTL);

	}
	return error;
}

static int
ioctl_setinformation_fe(void *wl, int cmd, void* buf, unsigned long *input_len)
{
	int error = 0;

	if (remote_type == NO_REMOTE) {
		error = wl_ioctl(wl,  cmd, buf, *input_len, TRUE);
	} else {
		error = rwl_setinformation_fe(wl, cmd, buf,
			input_len, 0, REMOTE_SET_IOCTL);
	}

	return error;
}

int
wl_get(void *wl, int cmd, void *buf, int len)
{
	int error = 0;
	unsigned long input_len = len;

	if ((rwl_os_type == WIN32_OS || rwl_os_type == WINVISTA_OS) && remote_type != NO_REMOTE)
		cmd += WL_OID_BASE;
	error = (int)ioctl_queryinformation_fe(wl, cmd, buf, &input_len);

	if (error == SERIAL_PORT_ERR)
		return SERIAL_PORT_ERR;
	else if (error == BCME_NODEVICE)
		return BCME_NODEVICE;
	else if (error != 0)
		return BCME_IOCTL_ERROR;

	return 0;
}

int
wl_set(void *wl, int cmd, void *buf, int len)
{
	int error = 0;
	unsigned long input_len = len;

	if ((rwl_os_type == WIN32_OS || rwl_os_type == WINVISTA_OS) && remote_type != NO_REMOTE)
		cmd += WL_OID_BASE;
	error = (int)ioctl_setinformation_fe(wl, cmd, buf, &input_len);

	if (error == SERIAL_PORT_ERR)
		return SERIAL_PORT_ERR;
	else if (error == BCME_NODEVICE)
		return BCME_NODEVICE;
	else if (error != 0)
		return BCME_IOCTL_ERROR;

	return 0;
}


/* Main client function */
int
brcm_wl(int argc, char **argv)
{
	struct ifreq ifr;
	char *ifname = NULL;
	int err = 0;
	int help = 0;
	int status = CMD_WL;

	wlu_av0 = argv[0];

	if((argc == 2 && strcmp(argv[1], "--init") == 0) ||
		(argc == 4 && (!strcmp(argv[1], "--init") || !strcmp(argv[3], "--init")) )
					){
		extern int wl_ertos_wlan_init(void);
		wl_ertos_wlan_init();
		return err;
	}

	extern int wl_ertos_wlan_is_inited(void);
	if(wl_ertos_wlan_is_inited() != 1){
		debug_printf("Please init wlan before running wl cmd!!\n");
		return -1;
	}

	wlu_init();
	memset(&ifr, 0, sizeof(ifr));
	(void)*argv++;

	if ((status = wl_option(&argv, &ifname, &help)) == CMD_OPT) {
		if (ifname){
#if 0
			strncpy(ifr.ifr_name, ifname, IFNAMSIZ);
#endif
		}
		/* Bug fix: If -h is used as an option, the above function call
		 * will notice it and raise the flag but it won't be processed
		 * in this function so we undo the argv increment so that the -h
		 * can be spotted by the next call of wl_option. This will ensure
		 * that wl -h [cmd] will function as desired.
		 */
		if (help){
			(void)*argv--;
		}
	}

	err = process_args(&ifr, argv);
	return err;
}

/*
 * Function called for  'local' execution and for 'remote' non-interactive session
 * (shell cmd, wl cmd)
 */
int
process_args(struct ifreq* ifr, char **argv)
{
	char *ifname = NULL;
	int help = 0;
	int status = 0;
	int vista_cmd_index;
	int err = 0;
	cmd_t *cmd = NULL;
#ifdef RWL_WIFI
	int retry;
#endif

	while (*argv) {
		if ((strcmp (*argv, "sh") == 0) && (remote_type != NO_REMOTE)) {
			(void)*argv++; /* Get the shell command */
			if (*argv) {
				/* Register handler in case of shell command only */
				err = rwl_shell_cmd_proc((void*)ifr, argv, SHELL_CMD);
			} else {
				DPRINT_ERR(ERR, "Enter the shell "
				           "command, e.g. ls(Linux) or dir(Win CE)\n");
				err = -1;
			}
			return err;
		}

#ifdef RWLASD
		if ((strcmp (*argv, "asd") == 0) && (remote_type != NO_REMOTE)) {
			(void)*argv++; /* Get the asd command */
			if (*argv) {
				err = rwl_shell_cmd_proc((void*)ifr, argv, ASD_CMD);
			} else {
				DPRINT_ERR(ERR, "Enter the ASD command, e.g. ca_get_version\n");
				err = -1;
			}
			return err;
		}
#endif
		if (rwl_os_type == WINVISTA_OS) {
			for (vista_cmd_index = 0; remote_vista_cmds[vista_cmd_index] &&
				strcmp(remote_vista_cmds[vista_cmd_index], *argv);
				vista_cmd_index++);
			if (remote_vista_cmds[vista_cmd_index] != NULL) {
				err = rwl_shell_cmd_proc((void *)ifr, argv, VISTA_CMD);
				if ((remote_type == REMOTE_WIFI) && ((!strcmp(*argv, "join")))) {
#ifdef RWL_WIFI
					DPRINT_INFO(OUTPUT,
						"\nChannel will be synchronized by Findserver\n\n");
					sleep(RWL_WIFI_JOIN_DELAY);
					for (retry = 0; retry < RWL_WIFI_RETRY; retry++) {
						if ((rwl_find_remote_wifi_server(ifr,
							&g_rwl_buf_mac[0]) == 0)) {
						break;
					}
				}
#endif /* RWL_WIFI */
			}
			return err;
			}
		}

		if ((status = wl_option(&argv, &ifname, &help)) == CMD_OPT) {
			if (help)
				break;
#if 0
			if (ifname) {
				if (remote_type == NO_REMOTE) {
					strncpy((*ifr).ifr_name, ifname, IFNAMSIZ);
				}
				else {
					strncpy(g_rem_ifname, ifname, IFNAMSIZ);
				}
			}
#endif
			continue;
		}
		/* parse error */
		else if (status == CMD_ERR)
			break;

		if (remote_type == NO_REMOTE) {
			/* use default interface */
			if (!*(*ifr).ifr_name)
				wl_find(ifr);
			/* validate the interface */
			if (!*(*ifr).ifr_name || wl_check((void *)ifr)) {
				fprintf(stderr, "%s: wl driver adapter not found\n", wlu_av0);
#ifdef TARGETENV_ertos
				return -1;
#endif
				exit(1);
			}

			if ((strcmp (*argv, "--interactive") == 0) || (interactive_flag == 1)) {
				err = do_interactive(ifr);
				return err;
			}
		 }
		/* search for command */
		cmd = wl_find_cmd(*argv);
		/* if not found, use default set_var and get_var commands */
		if (!cmd) {
			cmd = &wl_varcmd;
		}
#ifdef RWL_WIFI
		if (!strcmp(cmd->name, "findserver")) {
			remote_wifi_ser_init_cmds((void *) ifr);
		}
#endif /* RWL_WIFI */

		/* RWL over Wifi supports 'lchannel' command which lets client
		 * (ie *this* machine) change channels since normal 'channel' command
		 * applies to the server (ie target machine)
		 */
		if (remote_type == REMOTE_WIFI)	{
#ifdef RWL_WIFI
			if (!strcmp(argv[0], "lchannel")) {
				strcpy(argv[0], "channel");
				rwl_wifi_swap_remote_type(remote_type);
				err = (*cmd->func)((void *) ifr, cmd, argv);
				rwl_wifi_swap_remote_type(remote_type);
			} else {
				err = (*cmd->func)((void *) ifr, cmd, argv);
			}
			/* After join cmd's gets exeuted on the server side , client needs to know
			* the channel on which the server is associated with AP , after delay of
			* few seconds client will intiate the scan on diffrent channels by calling
			* rwl_find_remote_wifi_server fucntion
			*/
			if ((!strcmp(cmd->name, "join") || ((!strcmp(cmd->name, "ssid") &&
				(*(++argv) != NULL))))) {
				DPRINT_INFO(OUTPUT, "\n Findserver is called to synchronize the"
				"channel\n\n");
				sleep(RWL_WIFI_JOIN_DELAY);
				for (retry = 0; retry < RWL_WIFI_RETRY; retry++) {
					if ((rwl_find_remote_wifi_server(ifr,
					&g_rwl_buf_mac[0]) == 0)) {
						break;
					}
				}
			}
#endif /* RWL_WIFI */
		} else {
			/* do command */
			err = (*cmd->func)((void *) ifr, cmd, argv);
		}
		break;
	} /* while loop end */

/* provide for help on a particular command */
	if (help && *argv) {
		cmd = wl_find_cmd(*argv);
		if (cmd) {
			wl_cmd_usage(stdout, cmd);
		} else {
			DPRINT_ERR(ERR, "%s: Unrecognized command \"%s\", type -h for help\n",
			                                                          wlu_av0, *argv);
		}
#ifdef TARGETENV_ertos
	}else if(help){
		wl_usage(stdout, NULL);
		wl_cmds_usage(stdout, NULL);
#endif
	} else if (!cmd)
		wl_usage(stdout, NULL);
	else if (err == BCME_USAGE_ERROR)
		wl_cmd_usage(stderr, cmd);
	else if (err == BCME_IOCTL_ERROR)
		wl_printlasterror((void *) ifr);
	else if (err == BCME_NODEVICE)
		DPRINT_ERR(ERR, "%s : wl driver adapter not found\n", g_rem_ifname);

	return err;
}

#ifdef TARGETENV_ertos

#define fgets(s,size,stream)	 _cmdshell_fgets(s, size)

extern int cmdshell_check_input(void);
extern t_cmdshell * g_cmdshell;

static char * _cmdshell_fgets(char *s, int size)
{
	if(s == NULL || size <= 0){
		return NULL;
	}

	// clear g_cmdshell
	g_cmdshell->buf_cnt = 0;
	g_cmdshell->buf_full = 0;

	// check input
	while(1){
         cmdshell_check_input();
         if(g_cmdshell->buf_full){
            //debug_printf("cmd received:%d, %s\n", g_cmdshell->buf_cnt, g_cmdshell->buf);
			break;
		 }else{
		 	//wait for new command
		 	if(g_cmdshell->event){
		 		ertos_event_wait(g_cmdshell->event, 6000);
		 	}
         }
    }

	if(size < g_cmdshell->buf_cnt + 2){
		return NULL;
	}

	if(g_cmdshell->buf_cnt > 0){
		strncpy(s, g_cmdshell->buf, size);
	}
	s[g_cmdshell->buf_cnt] = '\n';
	s[g_cmdshell->buf_cnt + 1] = '\0';

	//FIXME: How can cmdshell input EOF char? (return NULL). 
	return s;
}

#endif
/* Function called for 'local' interactive session and for 'remote' interactive session */
static int
do_interactive(struct ifreq *ifr)
{
	int err = 0;

#ifdef RWL_WIFI
	int retry;
#endif

	while (1) {
		char *fgsret;
		char line[INTERACTIVE_MAX_INPUT_LENGTH];
		fprintf(stdout, "> ");
		fgsret = fgets(line, sizeof(line), stdin);

		/* end of file */
		if (fgsret == NULL)
			break;
		if (line[0] == '\n')
			continue;

		if (strlen (line) > 0) {
			/* skip past first arg if it's "wl" and parse up arguments */
			char *argv[INTERACTIVE_NUM_ARGS];
			int argc;
			char *token;
			argc = 0;

			while ((argc < (INTERACTIVE_NUM_ARGS - 1)) &&
			       ((token = strtok(argc ? NULL : line, " \t\n")) != NULL)) {

				/* Specifically make sure empty arguments (like SSID) are empty */
				if (token[0] == '"' && token[1] == '"') {
				    token[0] = '\0';
				}

				argv[argc++] = token;
			}
			argv[argc] = NULL;
#ifdef RWL_WIFI
		if (!strcmp(argv[0], "findserver")) {
			remote_wifi_ser_init_cmds((void *) ifr);
		}
#endif /* RWL_WIFI */

			if (strcmp(argv[0], "q") == 0 || strcmp(argv[0], "exit") == 0) {
				break;
			}

			if ((strcmp(argv[0], "sh") == 0) && (remote_type != NO_REMOTE))  {
				if (argv[1]) {
					process_args(ifr, argv);
				} else {
					DPRINT_ERR(ERR, "Give shell command");
					continue;
				}
			} else { /* end shell */
				/* check for lchannel support,applicable only for wifi transport.
				* when lchannel is called remote type is swapped by calling swap_
				* remote_type.This is done to change, the remote type to local,
				* so that local machine's channel can be executed and
				* returned to the user.
				* To get back the original remote type, swap is recalled.
				*/
				if (remote_type == REMOTE_WIFI) {
#ifdef RWL_WIFI
					if (!strcmp(argv[0], "lchannel")) {
						strcpy(argv[0], "channel");
						rwl_wifi_swap_remote_type(remote_type);
						err = wl_do_cmd(ifr, argv);
						rwl_wifi_swap_remote_type(remote_type);
					} else {
						err = wl_do_cmd(ifr, argv);
					}
				/* After join cmd's gets exeuted on the server side, client
				 * needs to know the channel on which the server is associated
				 * with AP , after delay of few seconds client will intiate the
				 * scan on diffrent channels by calling
				 * rwl_find_remote_wifi_server function
				 */
					if ((!strcmp(argv[0], "join")) ||
						(!strcmp(argv[0], "ssid"))) {
						DPRINT_INFO(OUTPUT, "\n Findserver is called"
						"after the join issued to remote \n \n");
						sleep(RWL_WIFI_JOIN_DELAY);
						for (retry = 0; retry < RWL_WIFI_RETRY; retry++) {
							if ((rwl_find_remote_wifi_server(ifr,
							&g_rwl_buf_mac[0]) == 0)) {
								break;
							}
						}
					}
#endif /* RWL_WIFI */
				} else {
					err = wl_do_cmd(ifr, argv);
				}
			} /* end of wl */
		} /* end of strlen (line) > 0 */
	} /* while (1) */

	return err;
}

/*
 * find command in argv and execute it
 * Won't handle changing ifname yet, expects that to happen with the --interactive
 * Return an error if unable to find/execute command
 */
static int
wl_do_cmd(struct ifreq *ifr, char **argv)
{
	cmd_t *cmd = NULL;
	int err = 0;
	int help = 0;
	char *ifname = NULL;
	int status = CMD_WL;

	/* skip over 'wl' if it's there */
	if (*argv && strcmp (*argv, "wl") == 0) {
		argv++;
	}

	/* handle help or interface name changes */
	if (*argv && (status = wl_option (&argv, &ifname, &help)) == CMD_OPT) {
		if (ifname) {
			fprintf(stderr,
			        "Interface name change not allowed within --interactive\n");
		}
	}

	/* in case wl_option eats all the args */
	if (!*argv) {
		return err;
	}

	if (status != CMD_ERR) {
		/* search for command */
		cmd = wl_find_cmd(*argv);

		/* defaults to using the set_var and get_var commands */
		if (!cmd) {
			cmd = &wl_varcmd;
		}
		/* do command */
		err = (*cmd->func)((void *)ifr, cmd, argv);
	}
	/* provide for help on a particular command */
	if (help && *argv) {
	  cmd = wl_find_cmd(*argv);
	 if (cmd) {
		wl_cmd_usage(stdout, cmd);
	} else {
			DPRINT_ERR(ERR, "%s: Unrecognized command \"%s\", type -h for help\n",
			       wlu_av0, *argv);
	       }
	} else if (!cmd)
		wl_usage(stdout, NULL);
	else if (err == BCME_USAGE_ERROR)
		wl_cmd_usage(stderr, cmd);
	else if (err == BCME_IOCTL_ERROR)
		wl_printlasterror((void *)ifr);
	else if (err == BCME_NODEVICE)
		DPRINT_ERR(ERR, "%s : wl driver adapter not found\n", g_rem_ifname);

	return err;
}

/* Search the wl_cmds table for a matching command name.
 * Return the matching command or NULL if no match found.
 */
static cmd_t *
wl_find_cmd(char* name)
{
	cmd_t *cmd = NULL;

	/* search the wl_cmds for a matching name */
	for (cmd = wl_cmds; cmd->name && strcmp(cmd->name, name); cmd++);

	if (cmd->name == NULL)
		cmd = NULL;

	return cmd;
}

void def_handler(int signum)
{
}
/* Create a child that waits for Ctrl-C at the client side
 */
int rwl_shell_createproc(void *wl)
{
	return 0;
}

/* In case if the server shell command exits normally
 * then kill the thread that was waiting for Ctr-C to happen
 * at the client side
 */
void rwl_shell_killproc(int pid)
{
}

CMDSHELL_DECLARE(wl)
   .cmd = {'w','l', '\0'},
   .handler = brcm_wl,
   .comment = "brcm wl tool (local)",
};
