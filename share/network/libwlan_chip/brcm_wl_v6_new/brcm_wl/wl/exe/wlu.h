/*
 * Common code for wl command line utility
 *
 * Copyright 2002, Broadcom Corporation
 * All Rights Reserved.
 *
 * This is UNPUBLISHED PROPRIETARY SOURCE CODE of Broadcom Corporation;
 * the contents of this file may not be disclosed to third parties, copied or
 * duplicated in any form, in whole or in part, without the prior written
 * permission of Broadcom Corporation.
 *
 * $Id: wlu.h 380263 2013-01-22 04:13:41Z $
 */

#ifndef _wlu_h_
#define _wlu_h_

#include "wlu_cmd.h"

extern const char *wlu_av0;

/* parse common option */
extern int wl_option(char ***pargv, char **pifname, int *phelp);
extern void wl_cmd_init(void);
extern void wlu_init(void);

/* print usage */
extern void wl_cmd_usage(FILE *fid, cmd_t *cmd);
extern void wl_usage(FILE *fid, cmd_t *port_cmds);
extern void wl_cmds_usage(FILE *fid, cmd_t *port_cmds);

/* print helpers */
extern void wl_printlasterror(void *wl);
extern void wl_printint(int val);

/* pretty print an SSID */
extern int wl_format_ssid(char* buf, uint8* ssid, int ssid_len);

/* pretty hex print a contiguous buffer */
extern void wl_hexdump(uchar *buf, uint nbytes);

/* check driver version */
extern int wl_check(void *wl);

/* wl functions used by the ndis wl. */
extern void dump_rateset(uint8 *rates, uint count);
extern uint freq2channel(uint freq);
extern int wl_ether_atoe(const char *a, struct ether_addr *n);
extern char *wl_ether_etoa(const struct ether_addr *n);
struct ipv4_addr;	/* forward declaration */
extern int wl_atoip(const char *a, struct ipv4_addr *n);
extern char *wl_iptoa(const struct ipv4_addr *n);
extern cmd_func_t wl_int;
extern cmd_func_t wl_varint;
extern void wl_dump_raw_ie(bcm_tlv_t *ie, uint len);

extern void wl_printlasterror(void *wl);
extern bool wc_cmd_check(const char *cmd);


/* functions for downloading firmware to a device via serial or other transport */

/* WLOTA_EN START */
#define WL_OTA_STRING_MAX_LEN           100
#define WL_OTA_CMDSTREAM_MAX_LEN        200

/* test_setup cmd argument ordering */
enum {
	WL_OTA_SYNC_TIMEOUT = 1,        /* Timeout in seconds */
	WL_OTA_SYNCFAIL_ACTION,         /* Fail actio -1/0/1 */
	WL_OTA_SYNC_MAC,                /* Mac address for sync */
	WL_OTA_TX_MAC,                  /* Mac address for tx test */
	WL_OTA_RX_MAC,                  /* Mac address for rx test */
	WL_OTA_LOOP_TEST                /* Put test into loop mode */
};

/* ota_tx / ota_rx format ordering */
enum {
	WL_OTA_CUR_TEST,                /* ota_tx or ota_rx */
	WL_OTA_CHAN,                    /* cur channel */
	WL_OTA_BW,                      /* cur bandwidth */
	WL_OTA_CONTROL_BAND,            /* cur control band */
	WL_OTA_RATE,                    /* cur rate */
	WL_OTA_STF_MODE,                /* cur stf mode */
	WL_OTA_TXANT,                   /* tx ant to be used */
	WL_OTA_RXANT,                   /* rx ant to be used */
	WL_OTA_TX_IFS,                  /* ifs */
	WL_OTA_TX_PKT_LEN,              /* pkt length */
	WL_OTA_TX_NUM_PKT,              /* num of packets */
	WL_OTA_PWR_CTRL_ON,             /* power control on/off */
	WL_OTA_PWR_SWEEP                /* start:delta:stop */
};

/* Various error checking options */
enum {
	WL_OTA_SYNCFAILACTION,
	WL_OTA_CTRLBANDVALID,
	WL_OTA_TXANTVALID,
	WL_OTA_RXANTVALID,
	WL_OTA_PWRCTRLVALID
};

extern int wl_ota_validate_string(uint8 arg, void* value);
extern int wl_ota_pwrinfo_parse(const char *tok_bkp, wl_ota_test_args_t *test_arg);
extern int wl_ota_parse_test_init(wl_ota_test_vector_t * init_info, char * tok, uint16 cnt);
extern int wl_ota_test_parse_test_option(wl_ota_test_args_t *test_arg, char * tok, uint16 cnt,
	char rt_string[]);
extern int wl_ota_test_parse_rate_string(wl_ota_test_args_t *test_arg, char rt_string[100]);
extern int wl_ota_test_parse_arg(char line[], wl_ota_test_vector_t *ota_test_vctr, uint16 *test_cnt,
	uint8 *ota_sync_found);


#ifdef BCMDLL
#ifdef LOCAL
extern FILE *dll_fd;
#else
extern void * dll_fd_out;
extern void * dll_fd_in;
#endif
#undef printf
#undef fprintf
#define printf printf_to_fprintf	/* printf to stdout */
#define fprintf fprintf_to_fprintf	/* fprintf to stderr */
extern void fprintf_to_fprintf(FILE * stderror, const char *fmt, ...);
extern void printf_to_fprintf(const char *fmt, ...);
extern void raw_puts(const char *buf, void *dll_fd_out);
#define	fputs(buf, stdout) raw_puts(buf, dll_fd_out)
#endif /* BCMDLL */

#define RAM_SIZE_4325  0x60000
#define RAM_SIZE_4329  0x48000
#define RAM_SIZE_43291 0x60000
#define RAM_SIZE_4330_a1  0x3c000
#define RAM_SIZE_4330_b0  0x48000

/* useful macros */
#ifndef ARRAYSIZE
#define ARRAYSIZE(a)  (sizeof(a)/sizeof(a[0]))
#endif /* ARRAYSIZE */

/* buffer length needed for wl_format_ssid
 * 32 SSID chars, max of 4 chars for each SSID char "\xFF", plus NULL
 */
#define SSID_FMT_BUF_LEN (4*32+1)	/* Length for SSID format string */

#define USAGE_ERROR  -1		/* Error code for Usage */
#define IOCTL_ERROR  -2		/* Error code for Ioctl failure */
#define BAD_PARAM -3 /* Error code for bad params, but don't dump cmd_help */
#define CMD_DEPRECATED -4 /* Commands that are functionally deprecated or don't provide
			   * a useful value to a specific OS port of wl
			   */

/* integer output format */
#define INT_FMT_DEC	0	/* signed integer */
#define INT_FMT_UINT	1	/* unsigned integer */
#define INT_FMT_HEX	2	/* hexdecimal */

/* command line argument usage */
#define CMD_ERR	-1	/* Error for command */
#define CMD_OPT	0	/* a command line option */
#define CMD_WL	1	/* the start of a wl command */

#endif /* _wlu_h_ */
