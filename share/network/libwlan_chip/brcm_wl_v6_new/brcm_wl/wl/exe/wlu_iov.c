/*
 * Automatically generated IOVariable merge information
 * $Copyright: (c) 2006, Broadcom Corp.
 * All Rights Reserved.$
 * $Id: wlu_iov.c 409431 2013-06-25 12:31:24Z $
 *
 * Contains user IO variable information
 * Note that variable type information comes from the driver
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include <typedefs.h>
#include <bcmutils.h>
#include <wlioctl.h>

#include "wlu.h"
