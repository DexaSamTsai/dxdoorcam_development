/*
 * $Copyright Open Broadcom Corporation$
 *
 * $Id: epivers.h.in,v 13.33 2010-09-08 22:08:53 $
 *
*/

#ifndef _epivers_h_
#define _epivers_h_

#define	EPI_MAJOR_VERSION	6

#define	EPI_MINOR_VERSION	25

#define	EPI_RC_NUMBER		134

#define	EPI_INCREMENTAL_NUMBER	20

#define EPI_BUILD_NUMBER	1

#define	EPI_VERSION		6, 25, 134, 20

#define	EPI_VERSION_NUM		0x06198614

#define EPI_VERSION_DEV		6.25.134


#define	EPI_VERSION_STR		"6.25.134.20 (r630785)"

#endif 
