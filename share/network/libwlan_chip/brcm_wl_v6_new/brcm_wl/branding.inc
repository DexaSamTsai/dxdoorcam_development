# $ Copyright Open Broadcom Corporation $
#  
ifeq ($(origin BRAND), undefined)
# undefine BRAND
BRAND=
endif

DIAG_FILE_BASE	    = bcm42dia
DIAG_A0_NIC_BASE    = bcm42da0
DIAG_B0_NIC_BASE    = bcm42db0
DIAG_HWACCESS_BASE  = bcm42ioa
DIAG_HWDEV_BASE	    = bcm42xhw
DIAG_CTLDLL_BASE    = bcm42ctl

export RELAY_FILE_BASE=bcm42rly
export NIC_NDIS30_DRIVER=bcm42xx3
export NIC_NDIS40_DRIVER=bcm42xx4
export NIC_NDIS50_DRIVER=bcm42xx5
export USB_NDIS50_DRIVER=bcm42u
export NDI_PCI_DLL=bcm42ndi
export NDI_USB_DLL=bcmndiu
