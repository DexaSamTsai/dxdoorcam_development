#include "includes.h"
#include "network/wifi/wifi.h"
#include "network/wifi/wifi_bcm4334.h"

int wl_ertos_wlan_is_inited(void)
{
	return WIFI_is_inited();
}

/*
 * Do "wl --init"
 * 		Init wl driver adapter(wlan chip). 
 * 		Used only when wlan chip is not initted yet before run wl commands.
 */
//TODO: check wlan init status automatically.
int wl_ertos_wlan_init(void)
{
	if(WIFI_is_inited() == 1){
		fprintf(stdout, "Wlan is inited already for brcm wl !\n");
		return 0;
	}

	wlan_para_restore(NULL);

	// Init WIFI
#ifdef CONFIG_WIFIMODULE_INTERFACE_SCIF_02
	int scif_if = SCIF_MODULE_02;
#elif CONFIG_WIFIMODULE_INTERFACE_SCIF_03
	int scif_if = SCIF_MODULE_03;
#else
	int scif_if = SCIF_MODULE_01;
#endif

	t_wifi_init_arg	arg;

	arg.sdio_id = scif_if;
	arg.mac = NULL;
	arg.mfg = 1;
	arg.msg_hdl_cb = NULL;
	arg.network_read_cb = NULL;
	arg.conf = NULL;

	if(WIFI_init(&arg) != WIFI_SUCCESS){
		fprintf(stderr, "Wlan init for brcm wl fail, fatal error!\n");
		return -1;
	}

	return 0;
}

/**
 * @brief execute brcm_wl command and export the result string to an outer buffer.
 *
 * @details	
 * 		this is an redirection version of "int brcm_wl(int argc, char **argv);".
 * @param argc argc for brcm_wl.
 * @param argv argv for brcm_wl.
 * @param out_buf the outer buffer for brcm_wl's result string.
 * @param out_len the length of brcm_wl's result string.
 * @param out_maxlen the max length of brcm_wl's result string. Usually the outer buffer size.
 *
 * @return 0: OK
 */
int brcm_wl_export(int argc, char **argv, char * out_buf, int * out_len, int out_maxlen)
{
	if(argc <= 1){
		return 0;
	}
	if(out_buf == NULL || out_len == NULL || out_maxlen <= 0){
		return 0;
	}

	//FIXME: use debug_printf_set() to redirect printing to the out_buf. Don't use this for thead safe consideration.
#if 0
	extern t_debugprintf * cur_dp;

	t_debugprintf_hw * old_hw = cur_dp->hw;
	void * old_hwarg = cur_dp->hw_arg;
	u32 old_flag = cur_dp->flag;

	debug_printf_set(DEBUG_PRINTF_MODE_TRUNCATE | DEBUG_PRINTF_FLAG_CRLF, &g_debugprintf_hw_null, 0);

	extern int brcm_wl(int argc, char **argv);
	int ret = brcm_wl(argc, argv);
	if(ret == 0){
		*out_len = debug_printf_getbuf(cur_dp, out_buf, out_maxlen);
	}

	debug_printf_set(old_flag, old_hw, old_hwarg);
#endif

	//TODO: here is only a temporary implementation. 
	//
#define BRCM_WL_OUTBUF_SIZE		2048
	extern u8 brcm_wl_outbuf[];
	memset(brcm_wl_outbuf, 0, BRCM_WL_OUTBUF_SIZE);

	int len = out_maxlen;
	if(BRCM_WL_OUTBUF_SIZE < len){
		len = BRCM_WL_OUTBUF_SIZE;
	}

	argv[0] = "wl_export";

	extern int brcm_wl(int argc, char **argv);
	int ret = brcm_wl(argc, argv);
	if(ret == 0){
		strncpy(out_buf, brcm_wl_outbuf, len);
		*out_len = strlen(out_buf);
	}
	return ret;
}
