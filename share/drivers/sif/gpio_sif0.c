#include "includes.h"

static t_libsif_cfg *cfg_inner;

void libsif0_gpio_simu_init(t_libsif_cfg *cfg)
{
	if(libsif0_use_gpio == 1){
		//use sif0p0, shared with scif
		if(!cfg->nocs){
			if(!cfg->pin_cs){
				cfg->pin_cs = PIN_GPIO_68;
			}
		}
		if(!cfg->pin_clk){
			cfg->pin_clk = PIN_GPIO_69;
		}
		if(!cfg->pin_miso){
			cfg->pin_miso = PIN_GPIO_70;
		}
		if(!cfg->pin_mosi){
			cfg->pin_mosi = PIN_GPIO_71;
		}
	}else if(libsif0_use_gpio == 2){
		//use sif0p1, shared with scio
		if(! cfg->nocs){
			if(!cfg->pin_cs){
				cfg->pin_cs = PIN_GPIO_58;
			}
		}
		if(!cfg->pin_clk){
			cfg->pin_clk = PIN_GPIO_59;
		}
		if(!cfg->pin_miso){
			cfg->pin_miso = PIN_GPIO_60;
		}
		if(!cfg->pin_mosi){
			cfg->pin_mosi = PIN_GPIO_61;
		}
	}else{
		//use sif0p2, shared with jtag
		if(! cfg->nocs){
			if(!cfg->pin_cs){
				cfg->pin_cs = PIN_GPIO_64;
			}
		}
		if(!cfg->pin_clk){
			cfg->pin_clk = PIN_GPIO_65;
		}
		if(!cfg->pin_miso){
			cfg->pin_miso = PIN_GPIO_66;
		}
		if(!cfg->pin_mosi){
			cfg->pin_mosi = PIN_GPIO_67;
		}
	}

	if(!cfg->delay){
		cfg->delay = 10;
	}
	cfg->bus_width = WIRE_4;
	cfg->data_seq = MSB_FIRST;
	if(cfg->cpol == 0)
	{
		if(cfg->cpha == 0)
		{
			cfg->sif_mode = SIF_MODE0;
		}else
		{
			cfg->sif_mode = SIF_MODE1;
		}
	}else{
		if(cfg->cpha == 0)
		{
			cfg->sif_mode = SIF_MODE2;
		}else
		{
			cfg->sif_mode = SIF_MODE3;
		}
	}
	cfg_inner = cfg;
}

void libsif0_gpio_cs_low(void)
{
	libsif_gpio_cs_low(cfg_inner);
}

void libsif0_gpio_cs_high(void)
{
	libsif_gpio_cs_high(cfg_inner);
}
