#include "dsif_inc.h"

//EVB-J32
/*NAND_D3/SN0_Y3/GPIO41*/
/*
//set in menuconfig
g_dsif_irq.gpio_in=PIN_GPIO_41;
g_dsif_irq.gpio_out=PIN_GPIO_43;
g_dsif_irq.gpio_rst=PIN_GPIO_45;
*/
typedef struct s_dsif_irq
{
	u8 					gpio_in;
	u8 					gpio_out;
	t_ertos_eventhandler	gpio_evt;
	u8 					triger_in;
	u8 					sif_if;
	u32 					sif_clk;
	void 				(*sif_dma_cb)(void);
	t_ertos_eventhandler 	pause_evt;
	u32 					timeout;
	t_ertos_eventhandler	timeout_evt;
	pthread_mutex_t 		mutex;
} __attribute__((packed)) t_dsif_irq;

static t_dsif_irq g_dsif_irq={0};

u32 dsif_irq_gpio_uninit(void)
{
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('-');
	libgpio_irq_free(g_dsif_irq.gpio_in);
	return 0;
}

u32 dsif_irq_gpio_in(u8* arg)
{
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('&');
	g_dsif_irq.triger_in=1-g_dsif_irq.triger_in;
	libgpio_config_interrupt(g_dsif_irq.gpio_in,0,0,g_dsif_irq.triger_in);
	dsif_sif_irq_send(DSIF_SIF_IRQ_GPIO,0);
	return 0;
}

void dsif_sif_dma_cb(void)
{
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('@');
	dsif_sif_irq_send(DSIF_SIF_IRQ_DMA,0);
}

u32 dsif_irq_gpio_init(void)
{	
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('+');
	g_dsif_irq.triger_in=1;
	libgpio_config_interrupt(g_dsif_irq.gpio_in,0,0,g_dsif_irq.triger_in);
	libgpio_irq_request(g_dsif_irq.gpio_in,dsif_irq_gpio_in,"DSIF.GPIO.IN",&g_dsif_irq);
}

u32 dsif_gpio_init_hw(void)
{
	libgpio_config(g_dsif_irq.gpio_in, PIN_DIR_INPUT);
	libgpio_config(g_dsif_irq.gpio_out, PIN_DIR_OUTPUT);
	libgpio_write(g_dsif_irq.gpio_out, PIN_LVL_LOW);
	return 0;
}

u32 dsif_gpio_out_high(void)
{
//	dsif_dbg(DSIF_DBG_IRQ,"%s %d\n",__FUNCTION__,__LINE__);
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('<');
	libgpio_write(g_dsif_irq.gpio_out, PIN_LVL_HIGH);
	return 0;
}

u32 dsif_gpio_out_low(void)
{
//	dsif_dbg(DSIF_DBG_IRQ,"%s %d\n",__FUNCTION__,__LINE__);
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('>');
	libgpio_write(g_dsif_irq.gpio_out, PIN_LVL_LOW);	
	return 0;
}

u32 dsif_gpio_in_status(void)
{
	u32 pin_lvl=libgpio_read(g_dsif_irq.gpio_in);
	if(pin_lvl==PIN_LVL_HIGH)
	{
		if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('[');
	}
	else
	{
		if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc(']');
	}	
//	dsif_dbg(DSIF_DBG_IRQ,"%s %d pin_lvl:%d\n",__FUNCTION__,__LINE__,pin_lvl);
	return pin_lvl;
}

//#define DSIF_IRQ_GPIO_TIMEOUT
#ifdef DSIF_IRQ_GPIO_TIMEOUT
u32 dsif_irq_gpio_timeout(u8* arg)
{
	dsif_irq_gpio_uninit();
	if(g_dsif_irq.timeout_evt){
		ertos_event_signal(g_dsif_irq.timeout_evt);
	}
	return 0;
}
#endif

u32 dsif_gpio_reset(u32 timeout_ticks)
{
	dsif_irq_gpio_uninit();
	dsif_gpio_out_low();

#ifdef DSIF_IRQ_GPIO_TIMEOUT
	if(timeout_ticks)
	{
		libgpio_config_interrupt(g_dsif_irq.gpio_in,0,0,0);
		libgpio_irq_request(g_dsif_irq.gpio_in,dsif_irq_gpio_timeout,"DSIF.GPIO.IN",&g_dsif_irq);

		if(g_dsif_irq.timeout_evt==0){
			g_dsif_irq.timeout_evt=ertos_event_create();
		}
		while(ertos_event_wait(g_dsif_irq.timeout_evt, timeout_ticks)==0)
		{
			dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s timeout, wait %d ticks\n",__FUNCTION__,timeout_ticks);
		}

		if(g_dsif_irq.timeout_evt){
			ertos_event_delete(g_dsif_irq.timeout_evt);
			g_dsif_irq.timeout_evt=0;
		}
	}
#else
#if 0
	while(timeout_ticks)
	{
		u32 delay_ticks=timeout_ticks;
		while(delay_ticks--)
		{
			if(dsif_gpio_in_status()==PIN_LVL_LOW) break;
			ertos_timedelay(1);
		}
		if(delay_ticks==0)
		{
			dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s timeout, wait %d ticks\n",__FUNCTION__,timeout_ticks);
			continue;
		}
		break;
	}
#endif
#endif
	dsif_gpio_init_hw();
	if(g_dsif_irq.gpio_evt){
		if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('/');
		ertos_event_signal(g_dsif_irq.gpio_evt);
	}else{
		if(g_dsif_irq.sif_dma_cb)
		{
//			if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('\\');
//			ertos_timedelay(timeout_ticks);
			dsif_irq_gpio_init();
		}
	}
	dsif_dbg(DSIF_DBG_IRQ, "[DSIF] %s done\n",__FUNCTION__);
}

extern u32 dsif_sif_data_init(u8 sif_master, t_dsif_sif_cb* sif_cb,u32 timeout_ticks);
u32 dsif_init(t_dsif_cfg* cfg)
{
	dsif_dbg_enable(cfg->dbg_flag);
	memset(&g_dsif_irq,0,sizeof(g_dsif_irq));	
	switch(cfg->sif_if)
	{
		case DSIF_SIF0_P0:
			g_dsif_irq.sif_if=DSIF_SIF0_P0;
			break;
		case DSIF_SIF0_P1:
			g_dsif_irq.sif_if=DSIF_SIF0_P1;
			break;			
		case DSIF_SIF0_P2:
			g_dsif_irq.sif_if=DSIF_SIF0_P2;
			break;		
		case DSIF_SIF2_P0:
			g_dsif_irq.sif_if=DSIF_SIF2_P0;
			break;		
		case DSIF_SIF2_P1:
			g_dsif_irq.sif_if=DSIF_SIF2_P1;
			break;			
		default:
			break;
	}

	g_dsif_irq.gpio_in=cfg->gpio_in;
	g_dsif_irq.gpio_out=cfg->gpio_out;
	g_dsif_irq.sif_clk=cfg->sif_clk;
	if(cfg->sif_master==0)
	{
		g_dsif_irq.sif_dma_cb=dsif_sif_dma_cb;
	}
	g_dsif_irq.timeout=cfg->timeout_ticks;
	dsif_dbg(DSIF_DBG_INFO,"[DSIF] sif config clk:%d dma:0x%x\n",
		g_dsif_irq.sif_clk,g_dsif_irq.sif_dma_cb);
	dsif_dbg(DSIF_DBG_INFO,"[DSIF] gpio config in:%d out:%d timeout_ticks:%d\n",
		g_dsif_irq.gpio_in,g_dsif_irq.gpio_out,g_dsif_irq.timeout);

	pthread_mutex_init(&g_dsif_irq.mutex, NULL);
	dsif_sif_init_hw(g_dsif_irq.sif_if,g_dsif_irq.sif_clk,g_dsif_irq.sif_dma_cb);
	dsif_gpio_init_hw();
	dsif_irq_gpio_init();
	dsif_sif_data_init(cfg->sif_master,&cfg->sif_cb,cfg->timeout_ticks);
	dsif_sif_thread_init();
	dsif_dbg(DSIF_DBG_INFO,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

u32 dsif_sif_lock(void)
{
	pthread_mutex_lock(&g_dsif_irq.mutex);
	dsif_dbg(DSIF_DBG_PROTOCOL,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

u32 dsif_sif_unlock(void)
{
	dsif_dbg(DSIF_DBG_PROTOCOL,"[DSIF] %s\n",__FUNCTION__);
	pthread_mutex_unlock(&g_dsif_irq.mutex);
	return 0;
}

u32 dsif_resume(void)
{
	dsif_sif_unlock();
	return 0;
}

u32 dsif_suspend(void)
{
	dsif_sif_lock();
	return 0;
}

u32 dsif_exit(void)
{
	dsif_sif_thread_exit();
	dsif_irq_gpio_uninit();
	pthread_mutex_destroy(&g_dsif_irq.mutex);
	dsif_dbg(DSIF_DBG_PROTOCOL,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

u32 dsif_master_notify(void)
{
	if(dsif_sif_staus()!=DSIF_SIF_STATUS_IDLE)
	{
		if(g_dsif_irq.gpio_evt==0)
			g_dsif_irq.gpio_evt=ertos_event_create();
		dsif_sif_irq_send(DSIF_SIF_IRQ_RESET,0);
		ertos_event_wait(g_dsif_irq.gpio_evt, WAIT_INF);
		ertos_event_delete(g_dsif_irq.gpio_evt);
		g_dsif_irq.gpio_evt=0;
	}
	dsif_irq_gpio_init();
	
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('?');
	if(g_dsif_irq.sif_dma_cb==0)
	{
		dsif_sif_irq_send(DSIF_SIF_IRQ_MASTER,0);
	}else{
		dsif_dbg(DSIF_DBG_PROTOCOL,"[DSIF] %s error, sif slave mode\n",__FUNCTION__);
	}
	return 0;
}

