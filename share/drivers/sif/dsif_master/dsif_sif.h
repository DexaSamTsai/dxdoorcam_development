#ifndef __DSIF_SIF_H__
#define __DSIF_SIF_H__

u32 dsif_sif_init_hw(u8 sif_if,u32 sif_clk,void (*sif_dma_cb)(void));
u32 dsif_sif_setclk(u32 sif_clk);

u32 dsif_sif_thread_init(void);
u32 dsif_sif_thread_exit(void);

/**************************************************/

typedef enum {
	DSIF_SIF_IRQ_NONE=0,
	DSIF_SIF_IRQ_GPIO,
	DSIF_SIF_IRQ_DMA,
	DSIF_SIF_IRQ_RESET,
	DSIF_SIF_IRQ_MASTER,
	DSIF_SIF_IRQ_UART,
} e_dsif_irq;

typedef struct s_dsif_sif_irq{
	u8 	irq;
	u8* 	data;
} __attribute__((packed)) t_dsif_sif_irq;

u32 dsif_sif_irq_send(u8 irq, u8* data);
u32 dsif_sif_send(u8 *addr, u32 size);
u32 dsif_sif_recv(u8 *addr, u32 size);
u32 dsif_sif_staus(void);

/**************************************************/
typedef enum
{
	DSIF_SIF_STATUS_IDLE = 0,
	DSIF_SIF_STATUS_CMD,
	DSIF_SIF_STATUS_DATA,
	DSIF_SIF_STATUS_ACK,
	DSIF_SIF_STATUS_DONE,
}e_dsif_sif_status;

typedef struct s_dsif_sif_status{
	u8 status;
	u8 gpio;
	u8 dma;
	u32 cmd_size;
	u32 data_size;
	u32 ack_size;
} __attribute__((packed)) t_dsif_sif_status;

/*
typedef enum {
	DSIF_SIF_MAGIC_NONE = 0x00000000,
	DSIF_SIF_MAGIC_CMD = 0xFFFFFF00,
	DSIF_SIF_MAGIC_ACK = 0xFFFFFF01,
} e_dsif_sif_magic;
*/

//DSIF_SIF_SIZE_HDR must be 32 and sizeof(t_dsif_sif_hdr) must be DSIF_SIF_SIZE_HDR
#define DSIF_SIF_SIZE_HDR (32)
typedef u8 __attribute__((packed))  t_dsif_sif_hdr_data[DSIF_SIF_SIZE_HDR];
typedef struct s_dsif_sif_hdr{
	t_dsif_sif_hdr_data data;
}__attribute__((packed)) t_dsif_sif_hdr;

#endif
