#ifndef __DSIF_INIT_H__
#define __DSIF_INIT_H__

//u32 dsif_init(t_dsif_cfg* cfg);
u32 dsif_resume(void);
u32 dsif_suspend(void);
u32 dsif_exit(void);
u32 dsif_master_notify(void);

u32 dsif_gpio_reset(u32 timeout_ticks);
u32 dsif_gpio_out_high(void);
u32 dsif_gpio_out_low(void);
u32 dsif_gpio_in_status(void);

u32 dsif_sif_lock(void);
u32 dsif_sif_unlock(void);

#endif
