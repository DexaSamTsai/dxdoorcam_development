#include "dsif_inc.h"

typedef struct s_dsif_sif_hw
{
	t_libsif_cfg 			g_libsif_cfg;
	u8 					sif_if;
	u32 					sif_clk;
	void (*sif_dma_cb)(void);
} __attribute__((packed)) t_dsif_sif_hw;

static t_dsif_sif_hw g_dsif_sif_hw;

u32 _dsif_sif_hw_init(void)
{
	t_libsif_cfg* p_libsif_cfg=&g_dsif_sif_hw.g_libsif_cfg;
	u32 sif_dma=0;
	u32 sif_master=0;
	u8 sif_if=g_dsif_sif_hw.sif_if;
	u32 sif_clk=g_dsif_sif_hw.sif_clk; //clk = xM 
	if(g_dsif_sif_hw.sif_dma_cb==0) {
		sif_master=1;sif_dma=0;
	}else{
		sif_master=0;sif_dma=1;
	}
//	debug_printf("%s %d %d %d %d 0x%x \n",__FUNCTION__,__LINE__,
//		sif_dma,sif_master,sif_clk,g_dsif_sif_hw.sif_dma_cb);

	switch(sif_if)
	{
		case DSIF_SIF0_P0:
			SIF0_USE_P0;
			libsif0_init(p_libsif_cfg, sif_dma, sif_master, sif_clk);
			break;
		case DSIF_SIF0_P1:
			SIF0_USE_P1;
			libsif0_init(p_libsif_cfg, sif_dma, sif_master, sif_clk);
			break;
		case DSIF_SIF0_P2:
			SIF0_USE_P2;
			libsif0_init(p_libsif_cfg, sif_dma, sif_master, sif_clk);
			break;
		case DSIF_SIF2_P0:
			SIF2_USE_P0;
			libsif2_init(p_libsif_cfg, sif_dma, sif_master, sif_clk);
			break;
		case DSIF_SIF2_P1:
			SIF2_USE_P1;
			libsif2_init(p_libsif_cfg, sif_dma, sif_master, sif_clk);
			break;
		default:
			break;
	}

	if(sif_dma)
	{
		libdma_init();
		p_libsif_cfg->op_dma = 2;				 
		p_libsif_cfg->dma_wait = 0x0;
		p_libsif_cfg->dma_irq_done_callback = g_dsif_sif_hw.sif_dma_cb;
	}	
	return 0;
}

u32 dsif_sif_setclk(u32 sif_clk)
{
	g_dsif_sif_hw.sif_clk=sif_clk;
	dsif_sif_lock();
	_dsif_sif_hw_init();
	dsif_sif_unlock();
	dsif_dbg(DSIF_DBG_INFO,"[DSIF] set sif_clk: %d\n",g_dsif_sif_hw.sif_clk);
	return 0;
}

u32 dsif_sif_send(u8 *addr, u32 size)
{
	u32 ret;
	dsif_sif_lock();
	_dsif_sif_hw_init();
	ret = libsif_write(&g_dsif_sif_hw.g_libsif_cfg, addr, dsif_align32(size));
	dsif_sif_unlock();
	//dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s 0x%x  0x%x  0x%x  \n",__FUNCTION__, (u32)&g_dsif_sif_hw.g_libsif_cfg, addr, size);
	return ret;
}

u32 dsif_sif_recv(u8 *addr, u32 size)
{
	u32 ret=0;
	dsif_sif_lock();
	_dsif_sif_hw_init();
	ret = libsif_read(&g_dsif_sif_hw.g_libsif_cfg, addr, dsif_align32(size));
	dsif_sif_unlock();
	//dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s 0x%x  0x%x  0x%x  \n",__FUNCTION__, (u32)&g_dsif_sif_hw.g_libsif_cfg, addr, size);
	return ret;
}

u32 dsif_sif_init_hw(u8 sif_if,u32 sif_clk,void (*sif_dma_cb)(void))
{
	memset(&g_dsif_sif_hw,0,sizeof(g_dsif_sif_hw));
	g_dsif_sif_hw.sif_if=sif_if;
	g_dsif_sif_hw.sif_clk=sif_clk;
	g_dsif_sif_hw.sif_dma_cb=sif_dma_cb;
//	dsif_dbg(DSIF_DBG_INFO,"[DSIF] %s sif_if:%d sif_clk:%d sif_dma_cb:0x%x\n",__FUNCTION__,
//		g_dsif_sif_hw.sif_if,g_dsif_sif_hw.sif_clk,g_dsif_sif_hw.sif_dma_cb);
	dsif_sif_lock();
	_dsif_sif_hw_init();
	dsif_sif_unlock();
	return 0;
}

////////////////////////////////////////////////////////////
#define DSIF_THREAD_PRIORITY_SIF 		(2)
#define DSIF_THREAD_STACK_SIZE_SIF 		(5120)
static u8 dsif_thread_stack_sif[DSIF_THREAD_STACK_SIZE_SIF];

typedef struct s_dsif_sif_thread
{
	t_ertos_eventhandler 	queue;
	t_ertos_eventhandler 	evt_init;	
	t_ertos_eventhandler 	evt_exit;	
} __attribute__((packed)) t_dsif_sif_thread;

t_dsif_sif_thread g_dsif_sif_thread;

u32 dsif_sif_irq_send(u8 irq, u8* data)
{
	t_dsif_sif_irq sif_dsif;
	sif_dsif.irq = irq;
	sif_dsif.data = data;
		
	u32 irqflags;
	local_irq_save(irqflags);
	ertos_queue_send(g_dsif_sif_thread.queue, &sif_dsif, WAIT_INF);
	local_irq_restore(irqflags);
	return 0;
}

extern u32 dsif_sif_irq_loop(u8 irq, u8* data);
static void dsif_sif_irq_thread(u8*  arg)
{
	pthread_detach(pthread_self());
	pthread_setname(NULL, "dsif_sif_irq");

	if(g_dsif_sif_thread.queue== NULL){
		g_dsif_sif_thread.queue  = ertos_queue_create(3, sizeof(t_dsif_sif_irq));	
		if(g_dsif_sif_thread.queue == NULL){
			dsif_dbg(DSIF_DBG_ERR,"[DSIF] %s failed!\n",__FUNCTION__);
			return ;
		}
//		dsif_dbg(DSIF_DBG_INFO,"[DSIF] %s create!:%x\n", __FUNCTION__, g_dsif_sif_thread.queue);
	}
	
	if(g_dsif_sif_thread.evt_init){
		ertos_event_signal(g_dsif_sif_thread.evt_init);
	}

	t_dsif_sif_irq dsif_irq;
	while(1)
	{
		if(g_dsif_sif_thread.evt_exit){
			ertos_event_signal(g_dsif_sif_thread.evt_exit);
			break;
		}
		memset(&dsif_irq,0,sizeof(t_dsif_sif_irq));
		ertos_queue_recv(g_dsif_sif_thread.queue, &dsif_irq, WAIT_INF);
		dsif_sif_irq_loop(dsif_irq.irq,dsif_irq.data);
	}
}

u32 dsif_sif_thread_init(void)
{
	memset(&g_dsif_sif_thread,0,sizeof(t_dsif_sif_thread));
	
	if(g_dsif_sif_thread.evt_init==0){
		g_dsif_sif_thread.evt_init= ertos_event_create();
	}
	
	pthread_attr_t dsif_sif_attr;
	pthread_t dsif_sif_thread;

	pthread_attr_init(&dsif_sif_attr);
	pthread_attr_setstack(&dsif_sif_attr, dsif_thread_stack_sif, DSIF_THREAD_STACK_SIZE_SIF);
	dsif_sif_attr.schedparam.sched_priority = DSIF_THREAD_PRIORITY_SIF;
	u32 ret = pthread_create(&dsif_sif_thread, &dsif_sif_attr, dsif_sif_irq_thread, NULL);
	if(ret != 0){
		dsif_dbg(DSIF_DBG_ERR,"[DSIF] %s failed!\n",__FUNCTION__);
		while(1);
	}

	if(g_dsif_sif_thread.evt_init){
		ertos_event_wait(g_dsif_sif_thread.evt_init, WAIT_INF);
		ertos_event_delete(g_dsif_sif_thread.evt_init);
		g_dsif_sif_thread.evt_init=0;
	}
	
	dsif_dbg(DSIF_DBG_IRQ,"[DSIF] %s\n",__FUNCTION__);
	return ret;
}

u32 dsif_sif_thread_exit(void)
{
	if(g_dsif_sif_thread.evt_exit==0){
		g_dsif_sif_thread.evt_exit= ertos_event_create();
	}
	ertos_event_wait(g_dsif_sif_thread.evt_exit, WAIT_INF);
	ertos_event_delete(g_dsif_sif_thread.evt_exit);
	g_dsif_sif_thread.evt_exit=0;
	ertos_queue_delete(g_dsif_sif_thread.queue);

	memset(&g_dsif_sif_thread,0,sizeof(t_dsif_sif_thread));
	dsif_dbg(DSIF_DBG_IRQ,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

