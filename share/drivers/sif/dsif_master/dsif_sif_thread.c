#include "dsif_inc.h"

typedef struct s_dsif_sif_data_cfg{
	u8					sif_master;
	dsif_sif_cb			sif_cmd_cb;
	dsif_sif_cb			sif_data_cb;
	dsif_sif_cb			sif_ack_cb;
	t_dsif_sif_status		sif_status;
	u8*					cache_addr;
	u32					cache_len;
	t_ertos_timerhandler	timeout_handler;
	u32 					timeout_ticks;
} __attribute__((packed)) t_dsif_sif_data_cfg;

static t_dsif_sif_data_cfg g_dsif_sif_data_cfg={0};

u32 dsif_sif_staus(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	return cfg->sif_status.status;
}

void dsif_sif_timer_callback(t_ertos_timerhandler t)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('~');
	dsif_dbg(DSIF_DBG_ERR,"%s timeout, status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);

	dsif_sif_irq_send(DSIF_SIF_IRQ_RESET,0);
}

void dsif_sif_timer_start(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	if(cfg->timeout_ticks==0) return;
	if(cfg->timeout_handler)
	{
//		dsif_dbg(DSIF_DBG_ERR, "[DSIF] sif irq timer unmatch\n");
		return;
	}

	cfg->timeout_handler=
		ertos_timer_create("dsif_irq_timer", cfg->timeout_ticks, 0, 0, dsif_sif_timer_callback);
	if(cfg->timeout_handler)
	{
		if(ertos_timer_start(cfg->timeout_handler,cfg->timeout_ticks)==0)
		{
			dsif_dbg(DSIF_DBG_LOOP, "[DSIF] %s timeout\n",__FUNCTION__);
		}
	}
	
	dsif_dbg(DSIF_DBG_LOOP,"%s handle:0x%x status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->timeout_handler,cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);
}

void dsif_sif_timer_stop(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	if(cfg->timeout_ticks==0) return;
	if(cfg->timeout_handler)
	{
		dsif_dbg(DSIF_DBG_LOOP,"%s handle:0x%x status:%d gpio:%d dma:%d\n",__FUNCTION__,
			cfg->timeout_handler,cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);
		if(ertos_timer_stop(cfg->timeout_handler,cfg->timeout_ticks)==0)
		{
			dsif_dbg(DSIF_DBG_LOOP, "[DSIF] sif irq timer stop timeout\n");
		}
		ertos_timer_delete(cfg->timeout_handler,cfg->timeout_ticks);
		cfg->timeout_handler=0;
	}
}

u32 dsif_sif_staus_reset(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s to idle\n",__FUNCTION__);
	dsif_sif_timer_stop();
	dsif_gpio_reset(cfg->timeout_ticks);
	memset(&cfg->sif_status,0,sizeof(t_dsif_sif_status));
//	cfg->sif_status.gpio=dsif_gpio_in_status();
//	cfg->sif_status.status=DSIF_SIF_STATUS_IDLE;
	return 0;
}

u32 dsif_sif_data_init(u8 sif_master, t_dsif_sif_cb* sif_cb,u32 timeout_ticks)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;
	memset(cfg,0,sizeof(t_dsif_sif_data_cfg));

	if(((u32)sif_cb->cache_addr)&0x1f)
	{
		dsif_dbg(DSIF_DBG_ERR, "[DSIF] sif cache address not 32bytes aligned\n");
		while(1);
	}
	
	if(((u32)sif_cb->cache_len)<=0x40)
	{
		dsif_dbg(DSIF_DBG_ERR, "[DSIF] sif cache size too small, should larger than 64\n");
		while(1);
	}

	cfg->sif_master=sif_master;

	cfg->sif_cmd_cb=sif_cb->cmd_cb;
	cfg->sif_data_cb=sif_cb->data_cb;
	cfg->sif_ack_cb=sif_cb->ack_cb;
	
	cfg->cache_len=sif_cb->cache_len;
	cfg->cache_addr=sif_cb->cache_addr;
	
	cfg->timeout_ticks=timeout_ticks;

	return 0;
}

u32 dsif_sif_prepare_send(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;

	t_dsif_sif_hdr* sif_hdr_send = (t_dsif_sif_hdr*) cfg->cache_addr;
	return cfg->sif_ack_cb(sif_hdr_send,cfg->sif_status.ack_size,
		&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
}

u32 dsif_sif_status_loop(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;

	t_dsif_sif_hdr* sif_hdr_send = (t_dsif_sif_hdr*) cfg->cache_addr;
	t_dsif_sif_hdr* sif_hdr_recv = (t_dsif_sif_hdr*) cfg->cache_addr;
//	u8* sif_data_send = ((u8*)sif_hdr_send)+sizeof(t_dsif_sif_hdr);
//	u8* sif_data_recv = ((u8*)sif_hdr_recv)+sizeof(t_dsif_sif_hdr);
	u8* sif_data_send = cfg->cache_addr;
	u8* sif_data_recv = cfg->cache_addr;
	
	dsif_dbg(DSIF_DBG_LOOP,"%s head status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);

	switch(cfg->sif_status.status)
	{
		case DSIF_SIF_STATUS_IDLE:
			if(cfg->sif_status.gpio==PIN_LVL_HIGH)
			{
				cfg->sif_status.status=DSIF_SIF_STATUS_CMD;
			}
			if(cfg->sif_status.gpio==PIN_LVL_LOW && cfg->sif_status.dma==0)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s idle\n",__FUNCTION__);
				break;
			}
//			break;
		case DSIF_SIF_STATUS_CMD:
			if(cfg->sif_status.gpio==PIN_LVL_HIGH&& cfg->sif_status.dma==0)
			{
				dsif_sif_timer_start();
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_recv hdr size 0x%x\n",__FUNCTION__,__LINE__,sizeof(t_dsif_sif_hdr));
				dsif_sif_recv(sif_hdr_recv,sizeof(t_dsif_sif_hdr));
				dsif_gpio_out_high();
			}
			if(cfg->sif_status.gpio==PIN_LVL_LOW && cfg->sif_status.dma==1)
			{
				cfg->sif_cmd_cb(sif_hdr_recv,sizeof(t_dsif_sif_hdr),
					&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
//				debug_printf("%s %d 0x%x\n",__FUNCTION__,__LINE__,cfg->sif_cmd_cb);
				cfg->sif_status.dma=0;
				if(cfg->sif_status.data_size>cfg->cache_len)
				{
					dsif_dbg(DSIF_DBG_LOOP,"[DSIF] %s %d parse hdr error 0x%x\n",__FUNCTION__,__LINE__);
					dsif_sif_staus_reset();
					break;
				}
				
				if(cfg->sif_status.data_size)
				{
					cfg->sif_status.status=DSIF_SIF_STATUS_DATA;
				}else{
					cfg->sif_status.status=DSIF_SIF_STATUS_ACK;
					dsif_sif_prepare_send();
				}
				dsif_gpio_out_low();
			}
			break;
		case DSIF_SIF_STATUS_DATA:
			if(cfg->sif_status.gpio==PIN_LVL_HIGH&& cfg->sif_status.dma==0)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_recv data size 0x%x\n",__FUNCTION__,__LINE__,cfg->sif_status.data_size);
				dsif_sif_recv(sif_data_recv,cfg->sif_status.data_size);
				dsif_gpio_out_high();
			}

			if(cfg->sif_status.gpio==PIN_LVL_LOW && cfg->sif_status.dma==1)
			{
				cfg->sif_data_cb(sif_data_recv,cfg->sif_status.data_size,
					&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
				cfg->sif_status.dma=0;
				cfg->sif_status.status=DSIF_SIF_STATUS_ACK;
				dsif_sif_prepare_send();
				dsif_gpio_out_low();
			}			
			break;
		case DSIF_SIF_STATUS_ACK:
			if(cfg->sif_status.gpio==PIN_LVL_HIGH&& cfg->sif_status.dma==0)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_send ack size 0x%x\n",__FUNCTION__,__LINE__,cfg->sif_status.ack_size);
//				dsif_dumpbuf(sif_hdr_send,cfg->sif_status.ack_size);
				dsif_sif_send(sif_hdr_send,cfg->sif_status.ack_size);
				dsif_gpio_out_high();
			//	lowlevel_getc();
			}
			if(cfg->sif_status.gpio==PIN_LVL_LOW && cfg->sif_status.dma==1)
			{
				dsif_sif_timer_stop();
				cfg->sif_status.dma=0;
				cfg->sif_status.status=DSIF_SIF_STATUS_IDLE;
				dsif_gpio_out_low();
				if(dsif_dbg_valid(DSIF_DBG_IRQ)) dsif_dbg(DSIF_DBG_IRQ,"\n");
			}
			break;
		default:
			break;
	}

	dsif_dbg(DSIF_DBG_LOOP,"%s tail status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);

	return cfg->sif_status.status;
}

//#define DSIF_SIF_TIMEOUT_TEST
#ifdef DSIF_SIF_TIMEOUT_TEST
u32 try_timeout_test=0;
#endif

u32 dsif_sif_status_loop_master(void)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;

	t_dsif_sif_hdr* sif_hdr_send = (t_dsif_sif_hdr*) cfg->cache_addr;
	t_dsif_sif_hdr* sif_hdr_recv = (t_dsif_sif_hdr*) cfg->cache_addr;
//	u8* sif_data_send = ((u8*)sif_hdr_send)+sizeof(t_dsif_sif_hdr);
//	u8* sif_data_recv = ((u8*)sif_hdr_recv)+sizeof(t_dsif_sif_hdr);
	u8* sif_data_send = cfg->cache_addr;
	u8* sif_data_recv = cfg->cache_addr;
	
	dsif_dbg(DSIF_DBG_LOOP,"%s head status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);

	switch(cfg->sif_status.status)
	{
		case DSIF_SIF_STATUS_IDLE:
//			cfg->sif_status.status=DSIF_SIF_STATUS_CMD;
			break;
		case DSIF_SIF_STATUS_CMD:
			//salve idle->data
			if(cfg->sif_status.gpio==PIN_LVL_LOW)
			{
				dsif_sif_timer_start();
				//send cmd callback for up layer to fill sif_data_send
				cfg->sif_cmd_cb(sif_hdr_send,sizeof(t_dsif_sif_hdr),
					&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
				//set out high and wait slave high
				dsif_gpio_out_high();
			}

			//slave high,  send data
			if(cfg->sif_status.gpio==PIN_LVL_HIGH)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s %d sif_hdr_send hdr size 0x%x\n",__FUNCTION__,__LINE__,sizeof(t_dsif_sif_hdr));
				dsif_sif_send(sif_hdr_send,sizeof(t_dsif_sif_hdr));
				//check stage data or ack 
				if(cfg->sif_status.data_size)
				{
					cfg->sif_status.status=DSIF_SIF_STATUS_DATA;
				}else{
					cfg->sif_status.status=DSIF_SIF_STATUS_ACK;
				}
				dsif_gpio_out_low();
			}
			break;
		case DSIF_SIF_STATUS_DATA:
			//cmd stage over, prepare to send data
			if(cfg->sif_status.gpio==PIN_LVL_LOW)
			{
				dsif_gpio_out_high();
			}

			//salve ready to recv data
			if(cfg->sif_status.gpio==PIN_LVL_HIGH)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_send data size 0x%x\n",__FUNCTION__,__LINE__,cfg->sif_status.data_size);
				//send data callback for up layer to fill sif_data_send
				cfg->sif_data_cb(sif_data_send,cfg->sif_status.data_size,
					&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
				dsif_sif_send(sif_data_send,cfg->sif_status.data_size);
				cfg->sif_status.status=DSIF_SIF_STATUS_ACK;
				dsif_gpio_out_low();
			}
			break;
		case DSIF_SIF_STATUS_ACK:
			if(cfg->sif_status.gpio==PIN_LVL_LOW)
			{
				dsif_gpio_out_high();
#ifdef DSIF_SIF_TIMEOUT_TEST
				try_timeout_test++;
				if((try_timeout_test%0x10)==0) 
				{
					ertos_timedelay(cfg->timeout_ticks);
					debug_printf("==================\n");
					dsif_sif_staus_reset();
					return 0;
				}
#endif
			}
			
			if(cfg->sif_status.gpio==PIN_LVL_HIGH)
			{
				dsif_dbg(DSIF_DBG_LOOP,"%s %d sif_data_recv ack size 0x%x\n",__FUNCTION__,__LINE__,cfg->sif_status.ack_size);
				dsif_sif_recv(sif_data_recv,cfg->sif_status.ack_size);
				cfg->sif_status.status=DSIF_SIF_STATUS_DONE;
				dsif_gpio_out_low();
				if(dsif_dbg_valid(DSIF_DBG_IRQ)) dsif_dbg(DSIF_DBG_IRQ,"\n");
			}
			break;
		case DSIF_SIF_STATUS_DONE:
			if(cfg->sif_status.gpio==PIN_LVL_LOW)
			{
				dsif_sif_timer_stop();
				cfg->sif_status.status=DSIF_SIF_STATUS_IDLE;
				//recv ack callback for up layer to recv ack data
				cfg->sif_ack_cb(sif_hdr_send,cfg->sif_status.ack_size,
						&cfg->sif_status.data_size,&cfg->sif_status.ack_size);
			}
			break;
		default:
			break;
	}

	dsif_dbg(DSIF_DBG_LOOP,"%s tail status:%d gpio:%d dma:%d\n",__FUNCTION__,
		cfg->sif_status.status,cfg->sif_status.gpio,cfg->sif_status.dma);

	return cfg->sif_status.status;
}

u32 dsif_sif_irq_loop(u8 irq, u8* data)
{
	t_dsif_sif_data_cfg* cfg=&g_dsif_sif_data_cfg;

	dsif_dbg(DSIF_DBG_LOOP,"%s irq type:%d \n",__FUNCTION__,irq);
	if(cfg->sif_master)
	{
		if(irq==DSIF_SIF_IRQ_MASTER)
		{
			cfg->sif_status.status=DSIF_SIF_STATUS_CMD;
			dsif_sif_status_loop_master();
		}
		if(irq==DSIF_SIF_IRQ_GPIO)
		{
			cfg->sif_status.gpio=dsif_gpio_in_status();
			dsif_sif_status_loop_master();
		}
	}else
	{
		if(irq==DSIF_SIF_IRQ_DMA)
		{
			cfg->sif_status.dma=1;
			dsif_sif_status_loop();
		}
		if(irq==DSIF_SIF_IRQ_GPIO)
		{
			cfg->sif_status.gpio=dsif_gpio_in_status();
			dsif_sif_status_loop();
		}	
	}

	if(irq==DSIF_SIF_IRQ_RESET)
	{
		dsif_sif_staus_reset();
	}

	return cfg->sif_status.status;
}

