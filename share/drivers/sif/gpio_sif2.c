#include "includes.h"

static t_libsif_cfg *sif2_cfg_inner;

void libsif2_gpio_simu_init(t_libsif_cfg *cfg)
{
	if(libsif2_use_gpio == 1){
		//use sif2p0, shared with bt1120
		if(!cfg->nocs){
			if(!cfg->pin_cs){
				cfg->pin_cs = PIN_GPIO_86;
			}
		}
		if(!cfg->pin_clk){
			cfg->pin_clk = PIN_GPIO_87;
		}
		if(!cfg->pin_miso){
			cfg->pin_miso = PIN_GPIO_85;
		}
		if(!cfg->pin_mosi){
			cfg->pin_mosi = PIN_GPIO_84;
		}
	}else{
		//use sif2p1
		if(! cfg->nocs){
			if(!cfg->pin_cs){
				cfg->pin_cs = PIN_GPIO_49;
			}
		}
		if(!cfg->pin_clk){
			cfg->pin_clk = PIN_GPIO_48;
		}
		if(!cfg->pin_miso){
			cfg->pin_miso = PIN_GPIO_46;
		}
		if(!cfg->pin_mosi){
			cfg->pin_mosi = PIN_GPIO_47;
		}
	}

	if(!cfg->delay){
		cfg->delay = 10;
	}
	cfg->bus_width = WIRE_4;
	cfg->data_seq = MSB_FIRST;
	if(cfg->cpol == 0)
	{
		if(cfg->cpha == 0)
		{
			cfg->sif_mode = SIF_MODE0;
		}else
		{
			cfg->sif_mode = SIF_MODE1;
		}
	}else{
		if(cfg->cpha == 0)
		{
			cfg->sif_mode = SIF_MODE2;
		}else
		{
			cfg->sif_mode = SIF_MODE3;
		}
	}
	sif2_cfg_inner = cfg;
}

void libsif2_gpio_cs_low(void)
{
	libsif_gpio_cs_low(sif2_cfg_inner);
}

void libsif2_gpio_cs_high(void)
{
	libsif_gpio_cs_high(sif2_cfg_inner);
}
