#include "dsif_inc.h"

static int dsif_gpio_irqin(void * arg)
{
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('&');

	dsif_cfg->triger_in = 1 - dsif_cfg->triger_in;
	libgpio_config_interrupt(dsif_cfg->gpio_in, 0, 0, dsif_cfg->triger_in);

	dsif_thread_signal();

	return 0;
}

void dsif_gpio_init(void)
{
	//in/out
	libgpio_config(dsif_cfg->gpio_in, PIN_DIR_INPUT);
	libgpio_config(dsif_cfg->gpio_out, PIN_DIR_OUTPUT);
	libgpio_write(dsif_cfg->gpio_out, PIN_LVL_LOW);

	//interrupt
	dsif_cfg->triger_in=1;
	libgpio_config_interrupt(dsif_cfg->gpio_in, 0, 0, dsif_cfg->triger_in);
	libgpio_irq_request(dsif_cfg->gpio_in, dsif_gpio_irqin, "DSIF.GPIO.IN", dsif_cfg);
}

static void dsif_gpio_uninit(void)
{
	libgpio_irq_free(dsif_cfg->gpio_in);
}

void dsif_gpio_out_high(void)
{
//	dsif_dbg(DSIF_DBG_IRQ,"%s %d\n",__FUNCTION__,__LINE__);
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('<');
	libgpio_write(dsif_cfg->gpio_out, PIN_LVL_HIGH);
}

void dsif_gpio_out_low(void)
{
//	dsif_dbg(DSIF_DBG_IRQ,"%s %d\n",__FUNCTION__,__LINE__);
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('>');
	libgpio_write(dsif_cfg->gpio_out, PIN_LVL_LOW);
}

void dsif_gpio_reset(void)
{
	dsif_gpio_uninit();
	dsif_gpio_out_low();

	dsif_gpio_init();

	dsif_dbg(DSIF_DBG_IRQ, "[DSIF] %s done\n",__FUNCTION__);
}
