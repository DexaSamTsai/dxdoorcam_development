#include "dsif_inc.h"

static void dsif_timer_start(void)
{
	dsif_cfg->start_ticks = ticks;
}

static void dsif_timer_stop(void)
{
	dsif_cfg->start_ticks = 0;
}

static void dsif_status_reset(void)
{
	dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s to idle\n",__FUNCTION__);

	dsif_timer_stop();
	dsif_gpio_reset();

	dsif_cfg->status = DSIF_STATUS_IDLE;
	dsif_cfg->dma_done = 0;

	dsif_rellock();
}

//ret: 0: all event handled, wait for sig. 1: still need loop
static int dsif_loop_slave(void)
{
	t_dsif_sif_hdr* sif_hdr_send = (t_dsif_sif_hdr*) dsif_cfg->sif_cb.cache_addr;
	t_dsif_sif_hdr* sif_hdr_recv = (t_dsif_sif_hdr*) dsif_cfg->sif_cb.cache_addr;
	u8* sif_data_send = dsif_cfg->sif_cb.cache_addr;
	u8* sif_data_recv = dsif_cfg->sif_cb.cache_addr;

do_next_status:
	//update gpio in status for every loop
	dsif_cfg->gpio_v = libgpio_read(dsif_cfg->gpio_in);
	
	dsif_dbg(DSIF_DBG_LOOP,"%s head status:%d gpio:%d dma:%d\n",__FUNCTION__,
		dsif_cfg->status, dsif_cfg->gpio_v, dsif_cfg->dma_done);

	if(dsif_cfg->start_ticks){
		if(ticks - dsif_cfg->start_ticks > dsif_cfg->timeout_ticks){
			dsif_dbg(DSIF_DBG_ERR,"%s timeout, status:%d gpio:%d dma:%d\n",__FUNCTION__,
				dsif_cfg->status, dsif_cfg->gpio_v, dsif_cfg->dma_done);

			dsif_status_reset();
			goto do_next_status;
		}
	}

	switch(dsif_cfg->status)
	{
		case DSIF_STATUS_IDLE:
			if(dsif_cfg->gpio_v == PIN_LVL_HIGH)
			{
				/* if A high, prepare the SIF and set GPIO B high*/
				dsif_getlock();

				dsif_timer_start();

				dsif_sif_recv(sif_hdr_recv, sizeof(t_dsif_sif_hdr));

				dsif_gpio_out_high();

				dsif_cfg->status = DSIF_STATUS_CMD;
				goto do_next_status;
			}else{
				/* if A low, wait for signal */
				break;
			}
		case DSIF_STATUS_CMD:
			if(dsif_cfg->gpio_v == PIN_LVL_LOW && dsif_cfg->dma_done == 1)
			{
				/* if A low, and DMA done, means header ready */
				dsif_cfg->dma_done = 0;

				/* handle command */
				dsif_cfg->sif_cb.cmd_cb(sif_hdr_recv, sizeof(t_dsif_sif_hdr),
					&dsif_cfg->data_size, &dsif_cfg->ack_size);

				if(dsif_cfg->data_size > dsif_cfg->sif_cb.cache_len)
				{
					dsif_dbg(DSIF_DBG_LOOP,"[DSIF] %s %d parse hdr error 0x%x\n", __FUNCTION__, __LINE__, dsif_cfg->data_size);
					dsif_status_reset();
					break;
				}
				
				if(dsif_cfg->data_size)
				{
					dsif_cfg->status = DSIF_STATUS_WAITDATA;
				}else{
					dsif_cfg->status = DSIF_STATUS_WAITACK;

					/* prepare ack */
					dsif_cfg->sif_cb.ack_cb(sif_hdr_send, dsif_cfg->ack_size,
						&dsif_cfg->data_size, &dsif_cfg->ack_size);
				}
				dsif_gpio_out_low();
			}
			break;
		case DSIF_STATUS_WAITDATA:
			if(dsif_cfg->gpio_v == PIN_LVL_HIGH)
			{
				/* A high */
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_recv data size 0x%x\n", __FUNCTION__, __LINE__, dsif_cfg->data_size);
				dsif_sif_recv(sif_data_recv, dsif_cfg->data_size);
				dsif_gpio_out_high();

				dsif_cfg->status = DSIF_STATUS_DATA;
			}
			break;
		case DSIF_STATUS_DATA:
			if(dsif_cfg->gpio_v == PIN_LVL_LOW && dsif_cfg->dma_done == 1)
			{
				/* if A low, and DMA done, means data ready */
				dsif_cfg->dma_done = 0;

				/* handle data */
				dsif_cfg->sif_cb.data_cb(sif_data_recv, dsif_cfg->data_size,
					&dsif_cfg->data_size, &dsif_cfg->ack_size);

				dsif_cfg->status = DSIF_STATUS_WAITACK;

				/* prepare ack */
				dsif_cfg->sif_cb.ack_cb(sif_hdr_send, dsif_cfg->ack_size,
					&dsif_cfg->data_size, &dsif_cfg->ack_size);

				dsif_gpio_out_low();
			}			
			break;
		case DSIF_STATUS_WAITACK:
			if(dsif_cfg->gpio_v == PIN_LVL_HIGH)
			{
				/* A high */
				dsif_dbg(DSIF_DBG_LOOP,"%s %d dsif_sif_send ack size 0x%x\n", __FUNCTION__, __LINE__, dsif_cfg->ack_size);
				dsif_sif_send(sif_hdr_send, dsif_cfg->ack_size);
				dsif_gpio_out_high();

				dsif_cfg->status = DSIF_STATUS_ACK;
			}
			break;
		case DSIF_STATUS_ACK:
			if(dsif_cfg->gpio_v == PIN_LVL_LOW && dsif_cfg->dma_done == 1)
			{
				/* if A low, and DMA done, means data ready */
				dsif_cfg->dma_done = 0;

				dsif_timer_stop();

				dsif_cfg->status = DSIF_STATUS_IDLE;

				dsif_gpio_out_low();

				dsif_rellock();

				dsif_dbg(DSIF_DBG_IRQ,"\n");
			}
			break;
		default:
			break;
	}

	dsif_dbg(DSIF_DBG_LOOP,"%s tail status:%d gpio:%d dma:%d\n",__FUNCTION__,
		dsif_cfg->status, dsif_cfg->gpio_v, dsif_cfg->dma_done);

	return 0;
}

static int dsif_loop(void)
{
	return dsif_loop_slave();
}

////////////////////////////////////////////////////////////
#define DSIF_THREAD_PRIORITY_SIF 		(2)
#define DSIF_THREAD_STACK_SIZE_SIF 		(5120)
static u8 dsif_thread_stack_sif[DSIF_THREAD_STACK_SIZE_SIF];

int dsif_thread_signal(void)
{
	ertos_event_signal(dsif_cfg->thread_evt);
	return 0;
}

int dsif_getlock(void)
{
	pthread_mutex_lock(&dsif_cfg->thread_mutex);
	return 0;
}

int dsif_rellock(void)
{
	pthread_mutex_unlock(&dsif_cfg->thread_mutex);
	return 0;
}

static void dsif_thread(u8*  arg)
{
	pthread_detach(pthread_self());
	pthread_setname(NULL, "dsif_thread");

	while(1)
	{
		while(1){
			if( dsif_loop() == 0){
				break;
			}
		}

		ertos_event_wait(dsif_cfg->thread_evt, dsif_cfg->timeout_ticks);
	}
}

int dsif_thread_init(void)
{
	pthread_attr_t dsif_sif_attr;
	pthread_t dsif_sif_thread;

	pthread_attr_init(&dsif_sif_attr);
	pthread_attr_setstack(&dsif_sif_attr, dsif_thread_stack_sif, DSIF_THREAD_STACK_SIZE_SIF);
	dsif_sif_attr.schedparam.sched_priority = DSIF_THREAD_PRIORITY_SIF;
	int ret = pthread_create(&dsif_sif_thread, &dsif_sif_attr, dsif_thread, NULL);
	if(ret != 0){
		dsif_dbg(DSIF_DBG_ERR,"[DSIF] %s failed!\n",__FUNCTION__);
		return -1;
	}

	dsif_dbg(DSIF_DBG_IRQ,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

int dsif_thread_exit(void)
{
	//TODO
	return 0;
}

