#ifndef __DSIF_GPIO_H__
#define __DSIF_GPIO_H__

void dsif_gpio_reset(void);
void dsif_gpio_out_high(void);
void dsif_gpio_out_low(void);
void dsif_gpio_init(void);

#endif
