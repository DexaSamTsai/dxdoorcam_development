#include "dsif_inc.h"

static void dsif_sif_dma_cb(void)
{
	if(dsif_dbg_valid(DSIF_DBG_IRQ)) lowlevel_putc('@');

	dsif_cfg->dma_done = 1;

	dsif_thread_signal();
}

int dsif_sif_init(void)
{
	memset(&dsif_cfg->sif_cfg, 0, sizeof(dsif_cfg->sif_cfg));
	int sif_dma;
	if(dsif_cfg->sif_master){
		sif_dma = 0;
	}else{
		sif_dma = 1;
	}

	switch(dsif_cfg->sif_if)
	{
		case DSIF_SIF0_P0:
			SIF0_USE_P0;
			libsif0_init(&dsif_cfg->sif_cfg, sif_dma, dsif_cfg->sif_master, dsif_cfg->sif_clk);
			break;
		case DSIF_SIF0_P1:
			SIF0_USE_P1;
			libsif0_init(&dsif_cfg->sif_cfg, sif_dma, dsif_cfg->sif_master, dsif_cfg->sif_clk);
			break;
		case DSIF_SIF0_P2:
			SIF0_USE_P2;
			libsif0_init(&dsif_cfg->sif_cfg, sif_dma, dsif_cfg->sif_master, dsif_cfg->sif_clk);
			break;
		case DSIF_SIF2_P0:
			SIF2_USE_P0;
			libsif2_init(&dsif_cfg->sif_cfg, sif_dma, dsif_cfg->sif_master, dsif_cfg->sif_clk);
			break;
		case DSIF_SIF2_P1:
			SIF2_USE_P1;
			libsif2_init(&dsif_cfg->sif_cfg, sif_dma, dsif_cfg->sif_master, dsif_cfg->sif_clk);
			break;
		default:
			break;
	}

	if(sif_dma)
	{
		libdma_init();
		dsif_cfg->sif_cfg.op_dma = 2;				 
		dsif_cfg->sif_cfg.dma_wait = 0x0;
		dsif_cfg->sif_cfg.dma_irq_done_callback = dsif_sif_dma_cb;
	}
	return 0;
}

int dsif_sif_send(u8 *addr, u32 size)
{
	u32 ret;

	dsif_sif_init(); //workround for HW buf

	ret = libsif_write(&dsif_cfg->sif_cfg, addr, dsif_align32(size));

	return ret;
}

int dsif_sif_recv(u8 *addr, u32 size)
{
	u32 ret=0;

	dsif_sif_init(); //workround for HW buf

	ret = libsif_read(&dsif_cfg->sif_cfg, addr, dsif_align32(size));
	//dsif_dbg(DSIF_DBG_ERR, "[DSIF] %s 0x%x  0x%x  0x%x  \n",__FUNCTION__, (u32)&g_dsif_sif_hw.g_libsif_cfg, addr, size);
	return ret;
}

