#ifndef __DSIF_SIF_H__
#define __DSIF_SIF_H__

int dsif_sif_init(void);

int dsif_sif_send(u8 *addr, u32 size);
int dsif_sif_recv(u8 *addr, u32 size);

//DSIF_SIF_SIZE_HDR must be 32 and sizeof(t_dsif_sif_hdr) must be DSIF_SIF_SIZE_HDR
#define DSIF_SIF_SIZE_HDR (32)
typedef u8 __attribute__((packed))  t_dsif_sif_hdr_data[DSIF_SIF_SIZE_HDR];
typedef struct s_dsif_sif_hdr{
	t_dsif_sif_hdr_data data;
} t_dsif_sif_hdr;

#endif
