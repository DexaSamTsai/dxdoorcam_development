#ifndef __DSIF_THREAD_H__
#define __DSIF_THREAD_H__

int dsif_thread_signal(void);
int dsif_thread_init(void);
int dsif_thread_exit(void);

int dsif_getlock(void);
int dsif_rellock(void);

#endif
