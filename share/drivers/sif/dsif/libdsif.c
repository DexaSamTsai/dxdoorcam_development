#include "dsif_inc.h"

t_dsif_cfg * dsif_cfg = NULL;

int libdsif_init(t_dsif_cfg* cfg)
{
	dsif_cfg = cfg;

	//TODO
	dsif_cfg->dbg_flag = DSIF_DBG_ERR|DSIF_DBG_INFO|DSIF_DBG_IRQ;
//	dsif_cfg->dbg_flag = -1;

	dsif_dbg(DSIF_DBG_INFO, "[DSIF] sif config clk:%d\n",
		dsif_cfg->sif_clk);
	dsif_dbg(DSIF_DBG_INFO, "[DSIF] gpio config in:%d out:%d timeout_ticks:%d\n",
		dsif_cfg->gpio_in, dsif_cfg->gpio_out, dsif_cfg->timeout_ticks);

	dsif_cfg->thread_evt = ertos_event_create();
	if(dsif_cfg->thread_evt == NULL){
		dsif_dbg(DSIF_DBG_ERR, "[DSIF] create thread_evt fail\n");
		return -10;
	}
	pthread_mutex_init(&dsif_cfg->thread_mutex, NULL);

	//check cfg
	if(((u32)dsif_cfg->sif_cb.cache_addr) & 0x1f)
	{
		dsif_dbg(DSIF_DBG_ERR, "[DSIF] sif cache address not 32bytes aligned\n");
		return -1;
	}
	if(((u32)dsif_cfg->sif_cb.cache_len) <= 0x40)
	{
		dsif_dbg(DSIF_DBG_ERR, "[DSIF] sif cache size too small, should larger than 64\n");
		return -2;
	}
	if(dsif_cfg->sif_cb.cmd_cb == NULL || dsif_cfg->sif_cb.data_cb == NULL || dsif_cfg->sif_cb.ack_cb == NULL){
		return -11;
	}

	//init hardware
	if(dsif_sif_init() < 0){
		return -3;
	}
	dsif_gpio_init();

	//init thread
	if(dsif_thread_init() < 0){
		return -4;
	}

	dsif_dbg(DSIF_DBG_INFO,"[DSIF] %s\n",__FUNCTION__);
	return 0;
}

int libdsif_suspend(void)
{
	return dsif_getlock();
}

int libdsif_resume(void)
{
	return dsif_rellock();
}

int libdsif_exit(void)
{
	//TODO
	return 0;
}


void dsif_dbg_enable(u32 flag)
{
	//TODO

}

