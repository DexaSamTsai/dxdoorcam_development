#include "includes.h"

void libsif_cs_low(t_libsif_cfg *cfg)
{
	if(cfg->pin_clk){
		libsif_gpio_cs_low(cfg);
	}else{
		WriteReg32(cfg->base_addr, ReadReg32(cfg->base_addr) | (0x3 << 4));
	}
}

void libsif_cs_high(t_libsif_cfg *cfg)
{
	if(cfg->pin_clk){
		libsif_gpio_cs_high(cfg);
	}else{
		volatile u32 to;
		to = 5;
		WriteReg32(cfg->base_addr, ReadReg32(cfg->base_addr) & ~(0x3 << 4));
		while(to --);
	}
}
