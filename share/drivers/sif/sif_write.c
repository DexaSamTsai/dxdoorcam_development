#include "includes.h"

static int libsif_dma_done(t_libsif_cfg *cfg)
{
	if(cfg->dma_wait) //when non-block mode, cannot disalbe dma bits in irq function, or redundancy dma irq will occur again in next read
	{
		//set to non-dma mode
		WriteReg32(cfg->base_addr+0x00, (ReadReg32(cfg->base_addr+0x0) & ~(0x3<<8)) | (SIF_TRANS_RT << 8));
		WriteReg32(cfg->base_addr+0x10, ReadReg32(cfg->base_addr+0x10) & (~(0x3<<13)) ); //disable dma r/w
		return 1; // return 1 when dma is blocked
	}
	return 0; // return 0 when dma is not blocked
}

int libsif_write_dmadone(t_libsif_cfg *cfg)
{
	return libsif_dma_done(cfg);
}

static void libsif_write_dmadone_cb(t_libsif_cfg *cfg)
{
	libsif_write_dmadone(cfg);
	if(cfg->dma_irq_done_callback){
		cfg->dma_irq_done_callback();
	}
}

s32 libsif_write(t_libsif_cfg *cfg, unsigned char *src_adr, u32 len)
{
	u8 __attribute__ ((unused)) data_out = 0;
	s32 i = 0;

	if(cfg->pin_clk){
		return libsif_gpio_write((t_libsif_cfg *)cfg, src_adr, len);
	}

	//don't change op_dma
	int usedma = cfg->op_dma;

	/*judge if 16 byte aligned when use DMA, if yes, use dma, if not, use cpu mode instead of dma mode*/
	if(usedma){
		if((((u32)src_adr & 0xf) != 0) || ((len & 0xf) != 0) ){
			syslog(LOG_INFO, "WARN: libsif_write dma not 16bytes aligned\n");
			if(cfg->op_dma == 2){
				return -1;
			}
			usedma = 0;
		}
	}

	if(usedma){
		//disable SIF
		WriteReg32(cfg->base_addr+0x00, ReadReg32(cfg->base_addr+0x00)&(~(1<<15)));
		//set to transmit-only
		WriteReg32(cfg->base_addr+0x00, (ReadReg32(cfg->base_addr+0x0) & ~(0x3<<8)) | (SIF_TRANS_OT << 8));

		/* enable sif */
		WriteReg32(cfg->base_addr+0x10, (ReadReg32(cfg->base_addr+0x10) & (~(0x3<<13)) ) | (0x02<<13));
		WriteReg32(cfg->base_addr+0x1c, len/16);

#if 0
		debug_printf("sif dma reg00:%x\n", ReadReg32(cfg->base_addr+0x00));
		debug_printf("sif dma reg10:%x\n", ReadReg32(cfg->base_addr+0x10));
		debug_printf("sif dma reg1c:%x, %x\n", ReadReg32(cfg->base_addr+0x1c), len);
#endif

		//enable SIF
		WriteReg32(cfg->base_addr+0x00, ReadReg32(cfg->base_addr+0x00)|(1<<15));
		
		dc_flush_range((u32)src_adr, (u32)src_adr + len);

		if(cfg->base_addr == SIF2_BASE){
			i = dma_config((u32)src_adr, SYSDMA_DEV_SIF2, len, cfg->dma_wait, cfg->dma_irq_done_callback ? (void (*)(void *))libsif_write_dmadone_cb : NULL, cfg);
		}else{
			i = dma_config((u32)src_adr, SYSDMA_DEV_SIF, len, cfg->dma_wait, cfg->dma_irq_done_callback ? (void (*)(void *))libsif_write_dmadone_cb : NULL, cfg);
		}
		if(i < 0){
			len = i;
		}
		if((cfg->dma_wait == 0)&&(cfg->op_mode == SIF_MODE_SLAVE))
		{
			while(ReadReg32(cfg->base_addr+0x18)&BIT8);		//wait send fifo not empty
		}
	}else{
		//set to receive-transmit
		WriteReg32(cfg->base_addr+0x0, (ReadReg32(cfg->base_addr+0x0) & ~(0x3<<8)) | (SIF_TRANS_RT << 8));

		WriteReg32(cfg->base_addr+0x10, ReadReg32(cfg->base_addr+0x10) & (~(0x03<<13)));
		for(i = 0; i <len; i++){
			while(BIT9 & ReadReg32(cfg->base_addr+0x18));
			WriteReg8(cfg->base_addr+0x14, *(src_adr+i));
			data_out = ReadReg8(cfg->base_addr+0x14);
		}
	}

	if((!usedma) || (usedma && (cfg->dma_wait != 0))){
		/* wait until fifo is empty */
		while(BIT8 != (BIT8 & ReadReg32(cfg->base_addr+0x18)));

		/* read fifo to make it empty, or read error in the next time */
		while(BIT0 != (BIT0 & ReadReg32(cfg->base_addr+0x18))){
			data_out = ReadReg8(cfg->base_addr+0x14);
		}

		if(usedma){
			libsif_dma_done(cfg);
		}
	}

	return len;
}

