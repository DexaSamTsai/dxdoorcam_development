#include "includes.h"

int libsif2_use_gpio = 0;

void libsif2_gpio_simu_init(t_libsif_cfg *cfg);
s32 libsif_init_internal(s32 in_clk, t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk);
s32 libsif2_init(t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk)
{
	u32 clk_in = 0;

#ifdef FOR_FPGA
	WriteReg32(REG_SC_ADDR + 0x134, ReadReg32(REG_SC_ADDR + 0x134) | 0xfe00000); //sif2 clock divider, from pad
	clk_in = 48000000;
#else
	//TODO: set more pre dividers for sif input clk according to different target clks.
	u32 pre_div;    
	if(clk >= 6000000){
		pre_div = 1;	// 6Mhz <= clk < 50Mhz(?)
	}else if(clk >= 1000000 && clk < 6000000){	
		pre_div = 8;	// 1Mhz <= clk < 6Mhz
	}else{
		pre_div = 31;	// 200Khz(?) <= clk < 1Mhz
	}
	WriteReg32(REG_SC_ADDR + 0x134, ((ReadReg32(REG_SC_ADDR + 0x134) & (~(0x7f<<21))) | ((pre_div & 0x7f) << 21) | BIT26));
	clk_in = clockget_ddrpll_ext() / pre_div;
#endif
	
	cfg->base_addr = SIF2_BASE;

	if(libsif2_use_gpio){
		libsif2_gpio_simu_init(cfg);
	}else{
		cfg->pin_clk = 0;
	}

	return libsif_init_internal(clk_in, cfg, dma, master, clk);
}

void libsif2_use_p0(void)
{
#ifdef CONFIG_SIF2_USE_GPIO
	libsif2_use_gpio = 1;
#endif

	WriteReg32(REG_SC_ADDR + 0x1c, ReadReg32(REG_SC_ADDR + 0x1c) | BIT9);
	SC_PIN1_EN_MUL(SC_PINEN1_BT1120_D7 | SC_PINEN1_BT1120_D6 | SC_PINEN1_BT1120_D5 | SC_PINEN1_BT1120_D4);

	if(libsif2_use_gpio){
		SC_PINSEL2_GPIO_MUL(SC_PINSEL2_BT1120_DATA1);
	}else{
		SC_PINSEL2_FUNC_MUL(SC_PINSEL2_BT1120_DATA1);
	}
}

void libsif2_use_p1(void)
{
#ifdef CONFIG_SIF2_USE_GPIO
	libsif2_use_gpio = 2;
#endif

	WriteReg32(REG_SC_ADDR + 0x1c, ReadReg32(REG_SC_ADDR + 0x1c) | BIT8);
	SC_PIN0_EN_MUL(SC_PINEN0_DVP_D8 | SC_PINEN0_DVP_D9 | SC_PINEN0_DVP_HREF | SC_PINEN0_DVP_VSYNC);

	if(libsif2_use_gpio){
		SC_PINSEL0_GPIO_MUL(SC_PINSEL0_SNR_D9 | SC_PINSEL0_SNR_D8 | SC_PINSEL0_SNR_HREF | SC_PINSEL0_SNR_VSYNC);
	}else{
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SNR_D9 | SC_PINSEL0_SNR_D8 | SC_PINSEL0_SNR_HREF | SC_PINSEL0_SNR_VSYNC);
	}
}

void libsif2_gpio_cs_low(void);
void libsif2_gpio_cs_high(void);

void libsif2_cs_low(void)
{
	if(libsif2_use_gpio){
		libsif2_gpio_cs_low();
	}else{
		WriteReg32(SIF2_BASE, ReadReg32(SIF2_BASE) | (0x3 << 4));
	}
}

void libsif2_cs_high(void)
{
	if(libsif2_use_gpio){
		libsif2_gpio_cs_high();
	}else{
		WriteReg32(SIF2_BASE, ReadReg32(SIF2_BASE) & ~(0x3 << 4));
		volatile u32 to;
		to = 5;
		while(to --);
	}
}
