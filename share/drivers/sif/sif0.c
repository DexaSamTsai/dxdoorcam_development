#include "includes.h"

int libsif0_use_gpio = 0;
int _libsif0_nocs = 0;

s32 libsif_init_internal(s32 in_clk, t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk);
s32 libsif0_init(t_libsif_cfg *cfg, s32 dma, s32 master, u32 clk)
{
	u32 clk_in;

	cfg->base_addr = SIF0_BASE;

#ifdef FOR_FPGA
	WriteReg32(REG_SC_ADDR + 0x48, ReadReg32(REG_SC_ADDR + 0x48) | 0xff000000); //spi clock divider, from pad
	clk_in = 48000000;
#else
	//TODO: set more pre dividers for sif input clk according to different target clks.
	u32 pre_div;    
	if(clk >= 6000000){
		pre_div = 1;	// 6Mhz <= clk < 50Mhz(?)
	}else if(clk >= 1000000 && clk < 6000000){	
		pre_div = 8;	// 1Mhz <= clk < 6Mhz
	}else{
		pre_div = 31;	// 200Khz(?) <= clk < 1Mhz
	}
	WriteReg32(REG_SC_ADDR + 0x48, ((ReadReg32(REG_SC_ADDR + 0x48) & (~(0x1f<<24))) | ((pre_div & 0x1f) << 24) | BIT29 ));
	clk_in = clockget_ddrpll_ext() / pre_div;
#endif

	if(libsif0_use_gpio){
		if(_libsif0_nocs){
			cfg->nocs = 1;
		}else{
			cfg->nocs = 0;
		}
		libsif0_gpio_simu_init(cfg);
	}else{
		cfg->pin_clk = 0;
	}

	return libsif_init_internal(clk_in, cfg, dma, master, clk);
}

void libsif0_cs_low(void)
{
	if(libsif0_use_gpio){
		libsif0_gpio_cs_low();
	}else{
		WriteReg32(SIF0_BASE, ReadReg32(SIF0_BASE) | (0x3 << 4));
	}
}

void libsif0_cs_high(void)
{
	if(libsif0_use_gpio){
		libsif0_gpio_cs_high();
	}else{
		WriteReg32(SIF0_BASE, ReadReg32(SIF0_BASE) & ~(0x3 << 4));
		volatile u32 to;
		to = 5;
		while(to --);
	}
}

void libsif0_use_p0(void)
{
#ifdef CONFIG_SIF0_USE_GPIO
	libsif0_use_gpio = 1;
#endif

	WriteReg32(REG_SC_ADDR + 0x1c, ReadReg32(REG_SC_ADDR + 0x1c) | BIT0 | BIT1 | BIT2 | BIT3);
	SC_PIN0_EN_MUL(SC_PINEN0_SD_D3 | SC_PINEN0_SD_D2 | SC_PINEN0_SD_D1 | SC_PINEN0_SD_D0);

	if(libsif0_use_gpio){
		SC_PINSEL0_GPIO_MUL(SC_PINSEL0_SD_D3 | SC_PINSEL0_SD_D2 | SC_PINSEL0_SD_D1 | SC_PINSEL0_SD_D0);
	}else{
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SD_D3 | SC_PINSEL0_SD_D2 | SC_PINSEL0_SD_D1 | SC_PINSEL0_SD_D0);
	}
}

void libsif0_use_p1(void)
{
#ifdef CONFIG_SIF0_USE_GPIO
	libsif0_use_gpio = 2;
#endif

	_libsif0_nocs = 0;

	WriteReg32(REG_SC_ADDR + 0x1c, ReadReg32(REG_SC_ADDR + 0x1c) | BIT4 | BIT5 | BIT6 | BIT7);
	SC_PIN0_EN_MUL(SC_PINEN0_SDIO_D2 | SC_PINEN0_SDIO_D1 | SC_PINEN0_SDIO_D0);
	SC_PIN1_EN_MUL(SC_PINEN1_SDIO_D3);

	if(libsif0_use_gpio){
		SC_PINSEL0_GPIO_MUL(SC_PINSEL0_SDIO_D3 | SC_PINSEL0_SDIO_D2 | SC_PINSEL0_SDIO_D1 | SC_PINSEL0_SDIO_D0);
	}else{
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SDIO_D3 | SC_PINSEL0_SDIO_D2 | SC_PINSEL0_SDIO_D1 | SC_PINSEL0_SDIO_D0);
	}
}

void libsif0_use_p1_nocs(void)
{
#ifdef CONFIG_SIF0_USE_GPIO
	libsif0_use_gpio = 2;
#endif

	_libsif0_nocs = 1;

	WriteReg32(REG_SC_ADDR + 0x1c, ReadReg32(REG_SC_ADDR + 0x1c) | BIT5 | BIT6 | BIT7);
	SC_PIN0_EN_MUL(SC_PINEN0_SDIO_D2 | SC_PINEN0_SDIO_D1);
	SC_PIN1_EN_MUL(SC_PINEN1_SDIO_D3);

	if(libsif0_use_gpio){
		SC_PINSEL0_GPIO_MUL(SC_PINSEL0_SDIO_D3 | SC_PINSEL0_SDIO_D2 | SC_PINSEL0_SDIO_D1);
	}else{
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_SDIO_D3 | SC_PINSEL0_SDIO_D2 | SC_PINSEL0_SDIO_D1);
	}
}

void libsif0_use_p2(void)
{
#ifdef CONFIG_SIF0_USE_GPIO
	libsif0_use_gpio = 3;
#endif

	WriteReg32(REG_SC_ADDR + 0x08, ReadReg32(REG_SC_ADDR + 0x08) | BIT14);
	SC_PIN0_EN_MUL(SC_PINEN0_JTAG_TDO | SC_PINEN0_JTAG_TDI | SC_PINEN0_JTAG_TMS | SC_PINEN0_JTAG_TCLK);

	if(libsif0_use_gpio){
		SC_PINSEL0_GPIO_MUL(SC_PINSEL0_JTAG_TDO | SC_PINSEL0_JTAG_TDI | SC_PINSEL0_JTAG_TMS | SC_PINSEL0_JTAG_CLK);
	}else{
		SC_PINSEL0_FUNC_MUL(SC_PINSEL0_JTAG_TDO | SC_PINSEL0_JTAG_TDI | SC_PINSEL0_JTAG_TMS | SC_PINSEL0_JTAG_CLK);
	}
}

