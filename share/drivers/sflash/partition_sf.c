#include "includes.h"

#define CALI_MAX_FW (64)
#define SF_SUCCESS   0
#define SF_FAIL     -1
#define SD_FAIL2    -2

struct cali_id2table
{
	u32 addr;
	u32 len;
	u32 maxlen;
	u32 valid;	// 1 : valid, 0: invalid
};

typedef struct cali_header
{
	// from 0x00
	u32 magicnumber;
	u32 version;
	u32 total_len;
	u32 reserved[5];

	// from 0x20
	struct cali_id2table id2table[CALI_MAX_FW];
}t_cali_header;

int partition_cali_has_sf(void)
{
	t_sflash * flash = sf_info();
	if( flash == NULL ){
		return SF_FAIL;
	}

	//check header
	t_sf_hdr sf_hdr;
	sf_read( (u8*)&sf_hdr, sizeof( t_sf_hdr ), 0 );
	if( sf_hdr.magic_num != CONFIG_HW_HEADER_MAGICNUM || sf_hdr.cali_addr == 0 || sf_hdr.cali_addr == 0xffffffff ){
		return SF_FAIL;
	}

	t_cali_header cali_hdr;

	sf_read( (u8*)&cali_hdr, sizeof( cali_hdr ), sf_hdr.cali_addr );
	if( cali_hdr.magicnumber != CONFIG_CALIBRATION_AREA_MAGICNUM ){
		return SF_FAIL;
	}

	return SF_SUCCESS;
}

int partition_cali_load_sf(int id, u8 * buf)
{
	t_sflash * flash = sf_info();
	if( flash == NULL ){
		return SF_FAIL;
	}

	//check header
	t_sf_hdr sf_hdr;
	sf_read( (u8*)&sf_hdr, sizeof( t_sf_hdr ), 0 );
	if( sf_hdr.magic_num != CONFIG_HW_HEADER_MAGICNUM || sf_hdr.cali_addr == 0 || sf_hdr.cali_addr == 0xffffffff ){
		syslog( LOG_ERR,"no cali area\n" );
		return SF_FAIL;
	}

	t_cali_header cali_hdr;

	sf_read( (u8*)&cali_hdr, sizeof( cali_hdr ), sf_hdr.cali_addr );
	if( cali_hdr.magicnumber != CONFIG_CALIBRATION_AREA_MAGICNUM ){
		return SF_FAIL;
	}
	if( cali_hdr.id2table[id].valid == 1 ){
		sf_read( (u8*)buf, cali_hdr.id2table[id].len, cali_hdr.id2table[id].addr );
	}else{
		return SF_FAIL;
	}
	return cali_hdr.id2table[id].len;
}


struct s_upgrade_header
{
	u32 magicnumber; //0x00
	u32 version; //0x04
	u32 fw_offset; //0x08
	u32 fw_len; //0x0c
	u32 user_config_offset; //0x10
	u32 user_config_len; //0x14
	char jump; //0x18
	char reserved01[0x17];//0x19~0x2f
	char crc[2];//0x30
	char reserved02[0x0e];//0x32-0x3f
};

static int upgrade_area(void * addr, int len, u32 area_start, u32 area_len)
{
	int i, j;
	u8 * check_data;
	u8 sf_data[2048];

	syslog( LOG_INFO,"update fw : addr %x, len %x, area_addr %x, area_len %x\n", addr, len, area_start, area_len );

	if( len > area_len ){
		syslog( LOG_ERR,"len is too large\n" );
		return SF_FAIL;
	}

	t_sflash * p_sf = sf_info();

	u32 lasttick = ticks;

#define SHOW_PROGRESS( str, cent, all ) \
	if( ticks - lasttick > 100 ){ \
		syslog( LOG_WARNING,"%s [%d / %d]\n", str, cent, all ); \
		lasttick = ticks; \
	}

	// erase area
	for( i = 0; i < area_len; i += p_sf->sector_size ){
		SHOW_PROGRESS( "ERASE", i / p_sf->sector_size, area_len / p_sf->sector_size);
		sf_erase( area_start + i, ERASE_SECTOR );
	}

	// copy area without magic number
	for( i = 0; i < len; i += p_sf->page_size ){
		SHOW_PROGRESS( "PROG", i / p_sf->page_size, len / p_sf->page_size );
		if(i == 0){
			sf_program( (u8*)addr + 4, p_sf->page_size - 4, area_start + 4 );
			check_data = (u8*)addr;
			sf_read( sf_data, p_sf->page_size, area_start );
			for( j = 4; j<p_sf->page_size; j++ ){
				if( sf_data[j] != check_data[j] ){
					syslog( LOG_ERR,"check data error [0x%x] : 0x%x, 0x%x\n", area_start + j, sf_data[j], check_data[j] );
					return SF_FAIL;
				}
			}
		}else{
			sf_program( (u8*)(addr + i), p_sf->page_size, area_start + i );
			check_data = (u8*)addr + i;
			sf_read( sf_data, p_sf->page_size, area_start + i );
			for( j = 0; j < p_sf->page_size; j++ ){
				if( sf_data[j] != check_data[j] ){
					syslog( LOG_ERR,"check data error [0x%x] : 0x%x, 0x%x\n", area_start + i + j, sf_data[j], check_data[j] );
					return SF_FAIL;
				}
			}
		}
	}

	// copy magic number
	sf_program( (u8*)addr, 4, area_start );
	check_data = (u8*)addr;
	sf_read( sf_data, 4, area_start );
	for( j = 0; j < 4; j++){
		if(sf_data[j] != check_data[j]){
			syslog( LOG_ERR,"check data error [0x%x] : 0x%x, 0x%x\n", area_start + j, sf_data[j], check_data[j] );
			return SF_FAIL;
		}
	}

    syslog(LOG_INFO,"update done\n");

    return SF_SUCCESS;

}

static int upgrade_para_add(void * addr, int len, u32 area_start, u32 area_len)
{
	syslog(LOG_INFO,"para : addr %x, len %x, area_addr %x, area_len %x\n", addr, len, area_start, area_len);

	// header[0] : magicnumber
	// header[1] : length
	// header[2] : crc << 16
	
	u32 header[3];
	sf_read( (u8 *)header, 12, area_start );

	t_sflash * p_sf = sf_info();

	if( header[0] != CONFIG_USER_CONFIG_MAGICNUM){
		if( upgrade_area( addr, len, area_start, area_len ) < 0){
			return SF_FAIL;
		}else{
			return SF_SUCCESS;
		}
	}else{
		if(( header[1] + len ) > area_len){
			return SF_FAIL;
		}
		u32 data_len = len - p_sf->sector_size;
		u32 data_addr = (u32)addr + p_sf->sector_size;
		// erase header
		sf_erase( area_start, ERASE_SECTOR );

		// upgrade data
		// last sector of old para
		int i, j;
		u32 sf_addr;
		u32 offset = 0;
		u8 last_sector[2048];
		u8 * check_data;
		u8 sf_data[p_sf->page_size];
		for( i = 0; i < header[1]; i += p_sf->sector_size ){
			if( ( header[1] - i ) > p_sf->sector_size ){
				continue;
			}
			offset = header[1] - i;
		}
		sf_addr = area_start + p_sf->sector_size + header[1] - offset;
		sf_read( (u8*)last_sector, p_sf->sector_size, sf_addr );
		u32 copy_len = p_sf->sector_size - offset;
		memcpy( (u8*)(last_sector + offset), (void *)data_addr, copy_len );
		for( i = 0; i < p_sf->sector_size; i += p_sf->page_size ){
			if(( i % p_sf->sector_size ) == 0){
				sf_erase( sf_addr, ERASE_SECTOR );
			}
			sf_program((u8*)last_sector + i, p_sf->page_size, sf_addr + i);
			check_data = (u8*)last_sector + i;
			sf_read( sf_data, p_sf->page_size, sf_addr + i );
			for( j = 0; j < p_sf->page_size; j++ ){
				if( sf_data[j] != check_data[j] ){
					syslog( LOG_ERR,"check data error [0x%x] : 0x%x, 0x%x\n", sf_addr + i + j, sf_data[j], check_data[j] );
					return SF_FAIL;
				}
			}
		}

			// rest new para
		sf_addr += p_sf->sector_size;
		u32 rest_len = data_len - copy_len;
		u32 rest_addr = data_addr + copy_len;
		for( i = 0; i < rest_len; i += p_sf->page_size){
			if(( i % p_sf->sector_size ) == 0){
				sf_erase( sf_addr, ERASE_SECTOR );
			}
			sf_program( (u8*)rest_addr + i, p_sf->page_size, sf_addr + i );
			check_data = (u8*)rest_addr + i;
			sf_read( sf_data, p_sf->page_size, sf_addr + i );
			for( j = 0; j < p_sf->page_size; j++ ){
				if(sf_data[j] != check_data[j]){
					syslog(LOG_ERR,"check data error [0x%x] : 0x%x, 0x%x\n", sf_addr + i + j, sf_data[j], check_data[j]);
					return SF_FAIL;
				}
			}
		}

		// write header
	    header[0] = CONFIG_USER_CONFIG_MAGICNUM;
		header[1] += data_len;
		header[2] = (crc16_ccitt(header, 8) << 16);
		sf_program((u8 *)(&header[1]), 8, area_start);
		sf_program((u8 *)(&header[0]), 4, area_start);

		syslog(LOG_INFO,"para add done\n");
		return SF_SUCCESS;
	}
}

int partition_upgrade_sf(void *p_buf)
{
    //TODO:WatchDog Set
	struct s_upgrade_header * h = p_buf;
	syslog(LOG_INFO,"UPGRADE INFO : [Ver:%d][FW:0x%x(%d)][UC:0x%x(%d)][CRC:0x%x]\n", h->version,h->fw_offset,h->fw_len,h->user_config_offset,h->user_config_len,h->crc);

	if(h->magicnumber != 0x4f565457){
		return SF_FAIL;
	}

	if(h->jump){
		void (*fff)(void) = (void(*)(void))h->jump;
		fff();
		return SF_SUCCESS;
	}

	t_sf_hdr sf_h;
	sf_read((u8*)&sf_h, sizeof(t_sf_hdr), 0);

	t_DKHW_header sf_dk_h;
	sf_read((u8*)&sf_dk_h, sizeof(t_DKHW_header), sizeof(t_sf_hdr));
	int userconfig_id;
    userconfig_id = CONFIG_USER_CONFIG - MAX_SYS_MAINPARTITION - 1;

	// check crc16
	unsigned short crc = crc16_ccitt((char *)p_buf+0x40, h->fw_len + h->user_config_len);
	if(h->crc[0] != ((crc>>8)&0xff) || h->crc[1] != (crc&0xff)){
		syslog(LOG_ERR,"ERROR:upgrade crc16[(0x%x)(0x%x)] error !!!\n",h->crc,crc);
		return SF_FAIL;
	}

	if(h->fw_offset && h->fw_len){
		if(upgrade_area(p_buf + h->fw_offset, h->fw_len, sf_h.fw2_addr, sf_h.fw2_len) < 0){
			return SF_FAIL;
		}
		partition_boot_area_change();
	}

	if(h->user_config_offset && h->user_config_len){
		if(h->user_config_offset & BIT31){
			// add to old para
			if(upgrade_para_add(p_buf + h->user_config_offset, h->user_config_len, sf_dk_h.dk_partinfo[userconfig_id].offset, sf_dk_h.dk_partinfo[userconfig_id].len) < 0){
				return SF_FAIL;
			}
		}else{
			// replace old para
			if(upgrade_area(p_buf + h->user_config_offset, h->user_config_len, sf_dk_h.dk_partinfo[userconfig_id].offset, sf_dk_h.dk_partinfo[userconfig_id].len) < 0){
				return SF_FAIL;
			}
		}
	}
	syslog(LOG_INFO,"upgrade all OK\n");
	return SF_SUCCESS;
}

int partition_load_sf(int id2, unsigned char *buf)
{
	u32 start;
	int len = partition_get_fwlen_sf(id2, &start);
	if(len < 0){
		return len;
	}

#ifdef BOOTTIME_STAT_UART1
	uarts_forceputc('B');
#endif

	syslog(LOG_INFO, "id2:%d addr = %x, len = %x\n", id2, start, len);
	//to use DMA,the length should be aligned to 128Bytes caused be chip level design
	sf_read(buf, (len+0x7f)&0xffffff80, start);

	return len;
}

static int upgrade_bl_area(void * addr, int len, u32 area_start, u32 area_len)
{
	int i, j;
	u8 * check_data;
	u8 sf_data[2048];

	syslog(LOG_DEBUG,"upgrade_bl_area(0x%x, 0x%x, 0x%x, 0x%x)\n", addr, len, area_start, area_len);

	t_sflash * p_sf = sf_info();

	if(len > area_len){
		syslog(LOG_ERR,"upgrade_bl_area: len is too large\n");
		return SF_FAIL;
	}

	u32 lasttick = ticks;

#undef SHOW_PROGRESS
#define SHOW_PROGRESS(str, cent, all) \
	if(ticks - lasttick > 100){ \
		syslog(LOG_INFO,"%s [%d / %d]\n", str, cent, all); \
		lasttick = ticks; \
	}

	// erase area
	for(i=0; i<area_len; i+=p_sf->sector_size){
		SHOW_PROGRESS("ERASE", i/p_sf->sector_size, area_len/p_sf->sector_size);
		sf_erase(area_start + i, ERASE_SECTOR);
	}

	// copy area
	for(i=0; i<len; i+=p_sf->page_size){
		SHOW_PROGRESS("PROG", i/p_sf->page_size, len/p_sf->page_size);
		sf_program((u8*)(addr + i), p_sf->page_size, area_start + i);
		check_data = (u8*)addr + i;
		sf_read(sf_data, p_sf->page_size, area_start + i);
		for(j=0; j<p_sf->page_size; j++){
			if(sf_data[j] != check_data[j]){
				syslog(LOG_ERR,"upgrade_bl_area: check data error [0x%x] : 0x%x, 0x%x\n", area_start + i + j, sf_data[j], check_data[j]);
				return SF_FAIL;
			}
		}
	}

	return SF_SUCCESS;
}
#if 0
// FIXME: This API is added for customer's special request. Please never use this unless you are clear about what you are doing!!
int partition_upgrade_bl(void *buf, int size)
{
	/*
	 * For upgrading the bootlooder safely, we regard one area among bl and bl2 partition as the 'using area'(to which bl_src_addr and bl2_src_addr both point), and another as the 'copy area'.
	 * The <buf, size> data will be updated to the 'copy area' only, and only after a successful updating of 'copy area', the bl_src_addr and bl2_src_addr in t_sf_hdr will be updated to use the 'copy area'.
	 */

	t_sf_hdr sf_h;
	sf_read((u8*)&sf_h, sizeof(t_sf_hdr), 0);

	// upgrade the buf data to 'copy area'
	u32 bl_copy_addr, bl_copy_len;

	if(sf_h.bl_src_addr != sf_h.bl2_src_addr){
		// the first upgrading after burning the SINGLE.bin
		sf_h.rsvd0 = sf_h.bl2_src_addr - sf_h.bl_src_addr;	// rsvd0 store bl partition length
		sf_h.rsvd2 = sf_h.fw_addr - sf_h.bl2_src_addr;		// rsvd2 store bl2 partition length
		bl_copy_addr = sf_h.bl2_src_addr;	// take bl2 partition as 'copy area'
		bl_copy_len = sf_h.rsvd2;
	}else{
		if(sf_h.bl_src_addr + sf_h.rsvd2 == sf_h.fw_addr){
			// 'using area' is bl2 partition, 'copy area' is bl partition
			bl_copy_addr = sf_h.bl_src_addr - sf_h.rsvd0;
			bl_copy_len = sf_h.rsvd0;
		}else{
			// 'using area' is bl partition, 'copy area' is bl2 partition
			bl_copy_addr = sf_h.bl_src_addr + sf_h.rsvd0;
			bl_copy_len = sf_h.rsvd2;
		}
	}

	if(upgrade_bl_area(buf, size, bl_copy_addr, bl_copy_len) < 0){
		return SF_FAIL;
	}

	// upgrade t_Sf_hdr area

	sf_h.bl_src_addr = sf_h.bl2_src_addr = bl_copy_addr;
	u8 * bl_buf = (u8 *)buf;
	sf_h.bl_dst_addr = sf_h.bl2_dst_addr = bl_buf[0x20]|(bl_buf[0x21] << 8)|(bl_buf[0x22] << 16)|(bl_buf[0x23] << 24);
	sf_h.bl_len = sf_h.bl2_len = size;
	sf_h.bl_crc = sf_h.bl2_crc = crc16_ccitt(buf, size);

	// FIXME: The upgrading of t_sf_hdr area cost about 5 ticks(50ms), which is the risk window of this API.
	if(upgrade_bl_area(&sf_h, sizeof(t_sf_hdr), 0, sizeof(t_sf_hdr)) < 0){
		syslog(LOG_ERR,"[ERROR]: upgrade hardware header in SFlash fail ! Fatal Error!!\n");
		return SF_FAIL;
	}

	return SF_SUCCESS;
}
#endif
#ifndef CONFIG_CODE_BASEADDR
#define CONFIG_CODE_BASEADDR 0x0
#endif

#if CONFIG_CODE_BASEADDR == 0
#undef CONFIG_CODE_BASEADDR
#define CONFIG_CODE_BASEADDR MEMBASE_SRAM
#endif

extern int fw_switch(u8 * buf, u32 len, u8 * tmpbuf);
extern int irq_free_all(void);

int partition_boot_sf(int id2)
{
	lowlevel_putc('\n');
	lowlevel_putc('L');
	lowlevel_putc('F');
	lowlevel_putc('W');
#ifdef CONFIG_ERTOS_EN
	DISABLE_INTERRUPT;
	DISABLE_TICK;
	irq_free_all();

	g_sflash.dma_en = 0;
	sf_init(SF_SOURCE_SFC, &g_sflash);
#endif

	if(partition_check_sf() < 0){
		partition_boot_area_change();
	}

#if CONFIG_CODE_BASEADDR == MEMBASE_SRAM
	lowlevel_putc('\n');
	lowlevel_putc('D');
	lowlevel_putc('D');
	lowlevel_putc('R');
	u8 * load_addr = (u8 *)MEMBASE_DDR + 0x500000;
#else
	lowlevel_putc('\n');
	lowlevel_putc('S');
	lowlevel_putc('R');
	lowlevel_putc('A');
	lowlevel_putc('M');
	u8 * load_addr = (u8 *)MEMBASE_SRAM;
#endif

	int len = partition_load_sf(id2, load_addr);
	if(len < 0){
		return len;
	}

	lowlevel_putc('\n');
	lowlevel_putc('L');
	lowlevel_putc('F');
	lowlevel_putc('W');
	lowlevel_putc('o');

#if CONFIG_CODE_BASEADDR == MEMBASE_SRAM
	lowlevel_putc('\n');
	lowlevel_putc('D');
	lowlevel_putc('D');
	lowlevel_putc('R');
	//fw_switch(load_addr, len, (u8 *)((MEMBASE_DDR + len + 31) & ~31));
	fw_switch(load_addr, len, (u8 *)((MEMBASE_DDR + 0x500000 + len + 31) & ~31));
#else
	//disable cache, call inst
	disable_cache();

	//reset
	while(1){
		//FIXME: turn down MBUS clock to fix the boot issue, need more test.
		WriteReg32(SC_BASE_ADDR+0x58,(ReadReg32(SC_BASE_ADDR+0x58) &~ 0x1f)| (2));

		BOOT_SRAM();
		asm("dsb");
		asm("wfe");
		asm("wfe");

		uart_forceputc('G');
		{volatile int d; for(d=0; d<100; d++);}
	}
#endif

	return SF_SUCCESS;
}

static int partition_copy(t_sflash * flash, u32 dst, u32 src, u32 len)
{
	int i, j;
	u8 magic[4];
#define MAX_PAGE_SIZE 2048
	u8 sf_data[MAX_PAGE_SIZE];
	u8 sf_data_check[MAX_PAGE_SIZE];

	syslog(LOG_DEBUG, "partition copy dst %x, src %x, len %x\n", dst, src, len);

	// erase area
	for(i=0; i<len; i+=flash->page_size){
		if(((dst + i) & (flash->sector_size - 1)) == 0){
			sf_erase(dst + i, ERASE_SECTOR);
		}
	}

	// copy area without magic number
	for(i=0; i<len; i+=flash->page_size){
		sf_read(sf_data, flash->page_size, src+i);
		if(i == 0){
			memcpy(magic, sf_data, sizeof(magic));
			sf_program(sf_data + 4, flash->page_size - 4, dst + 4);
			memset(sf_data_check, 0, flash->page_size);
			sf_read(sf_data_check, flash->page_size, dst);
			for(j=4; j<flash->page_size; j++){
				if(sf_data[j] != sf_data_check[j]){
					syslog(LOG_ERR, "check data error [0x%x] : 0x%x, 0x%x\n", dst + j, sf_data[j], sf_data_check[j]);
					return SF_FAIL;
				}
			}
		}else{
			sf_program(sf_data, flash->page_size, dst+i);
			memset(sf_data_check, 0, flash->page_size);
			sf_read(sf_data_check, flash->page_size, dst+i);
			for(j=0; j<flash->page_size; j++){
				if(sf_data[j] != sf_data_check[j]){
					syslog(LOG_ERR, "check data error [0x%x] : 0x%x, 0x%x\n", dst + i + j, sf_data[j], sf_data_check[j]);
					return SF_FAIL;
				}
			}
		}
	}

	// copy magic number
	sf_program((u8*)magic, 4, dst);
	memset(sf_data_check, 0, 4);
	sf_read(sf_data_check, 4, dst);
	for(j=0; j<4; j++){
		if(sf_data_check[j] != magic[j]){
			syslog(LOG_ERR, "check data error [0x%x] : 0x%x, 0x%x\n", dst + j, sf_data_check[j], magic[j]);
			return SF_FAIL;
		}
	}

	syslog(LOG_DEBUG, "partition copy done\n");
    return SF_SUCCESS;
}

int partition_Get_Backup_FW(void * buf)
{
	syslog(LOG_INFO, "Get Backup FW Start...\n");

	t_sflash * flash = sf_info();
	if(flash == NULL){
		return SF_FAIL;
	}

	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);

	if(sf_hdr.rsvpar02_addr != 0 && sf_hdr.rsvpar02_addr != 0xffffffff){
		sf_read((u8*)buf, sf_hdr.rsvpar02_len, sf_hdr.rsvpar02_addr);
	}
	syslog(LOG_INFO, "Get Backup FW OK!\n");
	return SF_SUCCESS;
}

int partition_boot_area_change(void)
{
	syslog(LOG_INFO, "Change Boot Partition Start...\n");

	t_sflash * flash = sf_info();
	if(flash == NULL){
		return SF_FAIL;
	}

	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);

	if(sf_hdr.fw2_addr != 0 && sf_hdr.fw2_addr != 0xffffffff){
		//check fw header
		u32 header[3];
		sf_read((u8*)header, 12, sf_hdr.fw2_addr);
		if(header[0] == CONFIG_FW_AREA_1_MAGICNUM){
			syslog(LOG_INFO, "Changing Boot Partition...NextBoot(0x%x)->(0x%x)\n",sf_hdr.fw_addr, sf_hdr.fw2_addr);
			u32 temp[2];
	        temp[0]=sf_hdr.fw_addr;
			temp[1]=sf_hdr.fw_len;
			sf_hdr.fw_addr=sf_hdr.fw2_addr;
			sf_hdr.fw_len=sf_hdr.fw2_len;
			sf_hdr.fw2_addr=temp[0];
			sf_hdr.fw2_len=temp[1];
			sf_write((u8*)&sf_hdr, sizeof(t_sf_hdr), 0)	;
		}
		else{
			syslog(LOG_ERR, "Changing Boot Partition failed(1)\n");
			return SF_FAIL;
		}
	}
	else{
	    syslog(LOG_ERR, "Changing Boot Partition failed(2)\n");
	    return SF_FAIL;

	}
	return SF_SUCCESS;
}

int partition_recovery_sf(void)
{
	syslog(LOG_INFO, "Partition recovery start\n");

	t_sflash * flash = sf_info();
	if(flash == NULL){
		return SF_FAIL;
	}

	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);

	if(sf_hdr.fw_addr != 0 && sf_hdr.fw_addr != 0xffffffff){
		//check fw header
		u32 header[3];
		sf_read((u8*)header, 12, sf_hdr.fw_addr);
		if(header[0] != CONFIG_FW_AREA_1_MAGICNUM){
			if(sf_hdr.fw2_addr && sf_hdr.fw2_len){
				syslog(LOG_INFO, "Partition recovery FW\n");
				if(partition_copy(flash, sf_hdr.fw_addr, sf_hdr.fw2_addr, sf_hdr.fw2_len) < 0){
					syslog(LOG_ERR, "Partition recovery FW failed\n");
					return SF_FAIL;
				}
			}
		}
	}

	syslog(LOG_INFO, "Partition recovery Done, Rebooting\n");
	watchdog_set(100);
	while(1){
		{volatile int d; for(d=0; d<0x10000; d++); }
		lowlevel_putc('.');
	}

	return SF_SUCCESS;
}

int partition_get_hwhead(t_sf_hdr* sf_hdr)
{
	sf_read((u8*)sf_hdr, sizeof(t_sf_hdr), 0);

	//check header
	if(sf_hdr->magic_num != CONFIG_HW_HEADER_MAGICNUM){
		return SF_FAIL;
	}
	else
	{
        return SF_SUCCESS;
	}
}

int partition_get_fwlen_sf(int id2, u32 * offset)
{
	//addrlen[0]: addr
	//addrlen[1]: len
	u32 addrlen[3];

	if(id2 == 0){
		sf_read((u8*)addrlen, 12, 0x18);
		addrlen[1] = addrlen[2];
	}else if(id2 == 1){
		sf_read((u8*)addrlen, 12, 0x38);
		addrlen[1] = addrlen[2];
	}else{
		u32 fw_offset;
		sf_read((u8*)&fw_offset, 4, 0x4c);
		sf_read((u8*)addrlen, 8, fw_offset + 0x10 + 8*(id2-2));
		addrlen[0] += fw_offset;
	}

	if(addrlen[1] == 0 || addrlen[1] == 0xffffffff){
		return SF_FAIL;
	}

	if(offset){
		*offset = addrlen[0];
	}

	return addrlen[1];
}

int partition_load_part_sf(int id2, unsigned char *buf, int offset, int maxlen)
{
	u32 start;
	int len = partition_get_fwlen_sf(id2, &start);
	if(len < 0){
		return len;
	}
	if(offset >= len){
		return SF_SUCCESS;
	}

	syslog(LOG_INFO, "id2:%d addr = %x, len = %x\n", id2, start, len);

	len -= offset;
	if(len > maxlen){
		len = maxlen;
	}

	sf_read(buf, len, start + offset);

	return len;
}

int partition_check_sf(void)
{
	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);

	//check header
	if(sf_hdr.magic_num != CONFIG_HW_HEADER_MAGICNUM){
		return SF_FAIL;
	}

	if(sf_hdr.fw_addr != 0 && sf_hdr.fw_addr != 0xffffffff){
		//check fw header
		u32 header[3];
		sf_read((u8*)header, 12, sf_hdr.fw_addr);
		if( header[0] != CONFIG_FW_AREA_1_MAGICNUM ){
			return SF_FAIL;
		}
	}

	return 0;
}

//Don't modify input value and function, becasue rtos will to use it.
//For only ENV save, Not save User config 
int partition_conf_save_sf(u8 * buf, u32 len)
{
#ifndef CONFIG_ENV_DISABLE
	t_sflash * flash = sf_info();
	if(flash == NULL){
		return SF_FAIL;
	}

    if( CONFIG_SYSTEM_ENV_SIZE > 0 ){
        if( len > CONFIG_SYSTEM_ENV_SIZE ){
		syslog(LOG_ERR, "[partition_conf_save_sf][01]conf size > para area size\n");
		return SF_FAIL;
	    }
    }else{
        if( len > flash->sector_size ){
		syslog(LOG_ERR, "[partition_conf_save_sf][02]conf size > para area size\n");
		return SF_FAIL;
	    }
    }

    // [0] maigc
	// [1] max length
	// [2] crc
	u32 header[3];

	
	sf_read( (u8*)header, 12, CONFIG_CALIBRATION_AREA_OFFSET );
    if( header[0] != CONFIG_CALIBRATION_AREA_MAGICNUM )
    {
        syslog( LOG_ERR, "para header error\n" );
        return SF_FAIL;
    }

   if( sf_write( buf, len, CONFIG_SYSTEM_ENV_OFFSET ) <0 )
   {
        syslog( LOG_ERR, "flash write error\n" );
        return SF_FAIL;
   }

	return SF_SUCCESS;
#else
    return SF_FAIL;
#endif
}

//Don't modify input value and function, becasue rtos will to use it.
//For only ENV save, Not save User config 
int partition_conf_load_sf(u8 * buf, u32 maxlen)
{
#ifndef CONFIG_ENV_DISABLE
    int ret;

    t_sflash * flash = sf_info();
	if(flash == NULL){
		return SF_FAIL;
	}

    // [0] maigc
    // [1] max length
    // [2] crc
     u32 header[3];
	
    sf_read((u8*)header, 12, CONFIG_CALIBRATION_AREA_OFFSET );
    if(header[0] != CONFIG_CALIBRATION_AREA_MAGICNUM )
    {
        syslog(LOG_ERR, "para header error\n");
        return SF_FAIL;
    }

    //check conf_size
    //syslog(LOG_ERR, "ENV_len[%d];flash->block_size:[%d];maxlen:[%d]\n",CONFIG_SYSTEM_ENV_SIZE,flash->block_size,maxlen);
    if( CONFIG_SYSTEM_ENV_SIZE > maxlen)
    {
        syslog(LOG_ERR, "config area > buf max\n");
        return SF_FAIL;
    }

    //start read
    //syslog(LOG_ERR, "sf_hdr.user_config_addr[0x%x];flash->block_size:[0x%x];
    //        flash->sector_size:[0x%x]\n",sf_hdr.dk_partinfo[CONFIG_USER_CONFIG].offset,flash->block_size,flash->sector_size);
    ret=sf_read(buf, maxlen, CONFIG_SYSTEM_ENV_OFFSET);

	return header[3];
#else
    return SF_FAIL;
#endif
}

int partition_write_sf(u32 offset, u8 * buf, u32 len)
{
	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);

    //check header
	if(sf_hdr.magic_num != CONFIG_HW_HEADER_MAGICNUM){
        syslog(LOG_ERR, "Flash read HW header fail\n");
        return SF_FAIL;
    }

   if(sf_write(buf, len, offset)<0)
   {
        syslog(LOG_ERR, "flash write error\n");
        return SF_FAIL;
   }
 
	return SF_SUCCESS;
}

int partition_read_sf(u32 offset, u8 * buf, u32 maxlen)
{
    int ret;

	// [0] maigc
	// [1] length
	// [2] crc16
	u32 header[3];

	//check header
	t_sf_hdr sf_hdr;
	sf_read((u8*)&sf_hdr, sizeof(t_sf_hdr), 0);


    if(sf_hdr.magic_num != CONFIG_HW_HEADER_MAGICNUM)
    {
        syslog(LOG_ERR, "Flash read HW header fail\n");
        return SF_FAIL;
    }
    //start read
    ret=sf_read(buf, maxlen, offset);

	return ret;
}

