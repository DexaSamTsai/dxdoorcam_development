/*sflash.c - a top driver for serial flash based on sif flash(sif_flash.c) and sfc flash(sfc_flash.c)*/
#include "includes.h"

t_sflash *p_sflash  = NULL;
u8       *sflashbuf = NULL;

extern u32 sif_flash_readid(t_sflash *sflash);
extern u32 sfc_flash_readid(t_sflash *sflash);

t_sflash *sf_detect(SF_SOURCE source)
{
	if(p_sflash != NULL)
		return p_sflash;

	t_sflash * tmp;

	u32 id = 0;

extern t_sflash sflash_cfg_start;
extern t_sflash sflash_cfg_end;
	for(tmp = (t_sflash *)(&sflash_cfg_start); tmp < (t_sflash *)(&sflash_cfg_end) ; tmp ++ ){
		if(source == SF_SOURCE_SIF0_P0 ||
				source == SF_SOURCE_SIF0_P1 ||
				source == SF_SOURCE_SIF0_P2 ||
				source == SF_SOURCE_SIF2_P0 ||
				source == SF_SOURCE_SIF2_P1
		  ){
			id = sif_flash_readid(tmp);
		}else{
			id = sfc_flash_readid(tmp);
		}

		syslog(LOG_DEBUG, "sf_detect %s: %x(%x)\n", tmp->name ? tmp->name : "NOName", id, tmp->deviceID);

		if(id == 0 || id == 0xff || id == 0xffffff){
			continue;
		}

		if(id == tmp->deviceID){
			syslog(LOG_INFO, "sflash found: %s\n", tmp->name ? tmp->name : "NOName");
			p_sflash = tmp;
			g_sflash = *p_sflash; //add by zxf
			return p_sflash;
		}else{
			continue;
		}
	}

	syslog(LOG_ERR, "sf detect failed, readid: %x\n", id);

	//just for error handler, use the 1st one if not found
	p_sflash = &g_sflash;

	return NULL;
}

s32 sf_init(SF_SOURCE source, t_sflash *sflash_inner)
{
	if(sflash_inner == NULL){
		//already detected.
		sflash_inner = p_sflash;
	}

	if(sflash_inner == NULL){
		//try to detected.
		sflash_inner = sf_detect(source);
	}

	if(sflash_inner == NULL){
		//didnot detected, force to use g_sflash
		sflash_inner = &g_sflash;
	}

	if(source == SF_SOURCE_SIF0_P0 ||
			source == SF_SOURCE_SIF0_P1 ||
			source == SF_SOURCE_SIF0_P2 ||
			source == SF_SOURCE_SIF2_P0 ||
			source == SF_SOURCE_SIF2_P1
			){
		g_sflash_fun_init.init = sif_flash_init;
		g_sflash_fun_init.read = sif_flash_read;
		g_sflash_fun_init.program = sif_flash_page_program;
		g_sflash_fun_init.write = sif_flash_write;
		g_sflash_fun_init.update = sif_flash_update;
		g_sflash_fun_init.erase = sif_flash_erase;
		g_sflash_fun_init.info = sif_flash_info;
		sflash_inner->sif_pinsel = source;
	}else if(source == SF_SOURCE_SFC){
		g_sflash_fun_init.init = sfc_flash_init;
		g_sflash_fun_init.read = sfc_flash_read;
		g_sflash_fun_init.program = sfc_flash_page_program;
		g_sflash_fun_init.write = sfc_flash_write;
		g_sflash_fun_init.update = sfc_flash_update;
		g_sflash_fun_init.erase = sfc_flash_erase;
		g_sflash_fun_init.info = sfc_flash_info;
	}else{
		syslog(LOG_ERR, "error: cannot find specified flash source");
		return -EPERM;
	}

	if(g_sflash_fun_init.init == NULL){
		syslog(LOG_ERR, "error: g_sflash_fun_init.init is NULL\n");
		return -EPERM;
	}

	int ret = g_sflash_fun_init.init(sflash_inner);

	if(p_sflash == NULL){
		p_sflash = sflash_inner;
	}

	return ret;
}

extern pthread_mutex_t sf_mutex;

s32 sf_read(u8 *data_out, u32 data_len, u32 flash_addr)
{
	if(g_sflash_fun_init.read == NULL){
		syslog(LOG_ERR, "error: g_sflash_fun_init.read is NULL\n");
		return -EPERM;
	}
	if(data_len == 0){
		syslog(LOG_ERR, "error: data length is forbidden as 0 when read serial flash\n");
		return -ESRCH;
	}
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_lock(&sf_mutex);
	}
	s32 ret = g_sflash_fun_init.read(data_out, data_len, flash_addr);
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_unlock(&sf_mutex);
	}
	return ret;
}

s32 sf_program(u8* data_in, u32 data_len, u32 flash_addr)
{
	if(g_sflash_fun_init.program == NULL){
		syslog(LOG_ERR, "error: g_sflash_fun_init.program is NULL\n");
		return -EPERM;
	}
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_lock(&sf_mutex);
	}
	s32 ret = g_sflash_fun_init.program(data_in, data_len, flash_addr);
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_unlock(&sf_mutex);
	}
	return ret;
}

s32
sf_write( u8* data_in, u32 data_len, u32 flash_addr )
{
    if ( g_sflash_fun_init.write == NULL ) {
        syslog( LOG_ERR, "error: g_sflash_fun_init.write is NULL\n" );
        return -EPERM;
    }

    t_sflash *flash = sf_info();
    if ( flash == NULL ) {
        syslog( LOG_ERR, "error: flash write not get info\n" );
        return -EPERM;
    }

    if ( sflashbuf == NULL ) {
        sflashbuf = malloc( flash->sector_size );
        if ( sflashbuf == NULL ) {
            syslog( LOG_ERR, "error: flash init not get buffer(size:0x%x)\n", flash->sector_size );
            return -EPERM;
        }
    }

    //program content
    u32 sector_pos, sector_offset, writed_len = 0;
    s32 len, ret;

    sector_pos		= flash->sector_size * ( flash_addr / flash->sector_size );
    sector_offset	= flash_addr % flash->sector_size;

    while ( writed_len < data_len ) {
        ret = sf_read( sflashbuf, flash->sector_size, sector_pos );

        if ( ret != flash->sector_size ) {
            syslog( LOG_ERR, "error: flash read fail\n" );
            return -EPERM;
        }

        if ( ( ( data_len - writed_len ) + sector_offset ) >= flash->sector_size ) {
            len = flash->sector_size - sector_offset;
        } else {
            len = data_len - writed_len;
        }

        memcpy( ( sflashbuf + sector_offset ), ( data_in + writed_len ), len );

        if ( g_ertos && g_ertos->schedule_running ) {
            pthread_mutex_lock( &sf_mutex );
        }

        ret = g_sflash_fun_init.write( sflashbuf, flash->sector_size, sector_pos );

        if ( g_ertos && g_ertos->schedule_running ) {
            pthread_mutex_unlock( &sf_mutex );
        }

        if ( ret != flash->sector_size ) {
            syslog(LOG_ERR, "error: flash write fail\n");
            return -EPERM;
        }

        writed_len += len;
        sector_pos += flash->sector_size;
        sector_offset = 0;
    }

    return writed_len;
}

s32 sf_update(u8* data_in, u32 data_len, u32 flash_addr)
{
	if(g_sflash_fun_init.update == NULL){
		syslog(LOG_ERR, "error: g_sflash_fun_init.update is NULL\n");
		return -EPERM;
	}
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_lock(&sf_mutex);
	}
	s32 ret = g_sflash_fun_init.update(data_in, data_len, flash_addr);
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_unlock(&sf_mutex);
	}
	return ret;
}

s32 sf_erase(u32 flash_addr, ERASE_TYPE erase_type)
{
	if(g_sflash_fun_init.erase == NULL){
		syslog(LOG_ERR, "error: g_sflash_fun_init.erase is NULL\n");
		return -EPERM;
	}

	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_lock(&sf_mutex);
	}
	s32 ret = g_sflash_fun_init.erase(flash_addr, erase_type);
	if(g_ertos && g_ertos->schedule_running){
		pthread_mutex_unlock(&sf_mutex);
	}
	return ret;
}
