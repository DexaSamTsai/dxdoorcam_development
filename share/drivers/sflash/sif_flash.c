#include "includes.h"

s32 sif_flash_init_internal(t_sflash *sflash);

s32 sif_flash_init(t_sflash *sflash)
{
	sflash->timeout = 0x100000;

	switch(sflash->sif_pinsel){
		case SF_SOURCE_SIF0_P0:
			SIF0_USE_P0;
			break;
		case SF_SOURCE_SIF0_P1:
			SIF0_USE_P1;
			break;
		case SF_SOURCE_SIF0_P2:
			SIF0_USE_P2;
			break;
		case SF_SOURCE_SIF2_P0:
			SIF2_USE_P0;
			break;
		case SF_SOURCE_SIF2_P1:
			SIF2_USE_P1;
			break;
		default:
			break;
	}

	if(sflash->sif_pinsel == SF_SOURCE_SIF0_P0 ||
			sflash->sif_pinsel == SF_SOURCE_SIF0_P1 ||
			sflash->sif_pinsel == SF_SOURCE_SIF0_P2){
		//libsif0_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk);
		libsif0_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk); //TODO : donnot support dma mode for now
	}else if(sflash->sif_pinsel == SF_SOURCE_SIF2_P0 ||
			sflash->sif_pinsel == SF_SOURCE_SIF2_P1){
		//libsif2_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk);
		libsif2_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk); //TODO : donnot support dma mode for now
	}

	sif_flash_init_internal(sflash);
	
	u8 id = sif_flash_read_deviceid();
	debug_printf("sif flash id:0x%x, id in setting:0x%x\n", id, sflash->deviceID);
	if(id != sflash->deviceID){
		return -ENXIO;
	}

	if(sflash->dma_en == 1){
		libdma_init();
	}

	return 0;
}

u32 sif_flash_readid(t_sflash *sflash)
{
	sflash->timeout = 0x100000;

	switch(sflash->sif_pinsel){
		case SF_SOURCE_SIF0_P0:
			SIF0_USE_P0;
			break;
		case SF_SOURCE_SIF0_P1:
			SIF0_USE_P1;
			break;
		case SF_SOURCE_SIF0_P2:
			SIF0_USE_P2;
			break;
		case SF_SOURCE_SIF2_P0:
			SIF2_USE_P0;
			break;
		case SF_SOURCE_SIF2_P1:
			SIF2_USE_P1;
			break;
		default:
			break;
	}

	if(sflash->sif_pinsel == SF_SOURCE_SIF0_P0 ||
			sflash->sif_pinsel == SF_SOURCE_SIF0_P1 ||
			sflash->sif_pinsel == SF_SOURCE_SIF0_P2){
		//libsif0_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk);
		libsif0_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk); //TODO : donnot support dma mode for now
	}else if(sflash->sif_pinsel == SF_SOURCE_SIF2_P0 ||
			sflash->sif_pinsel == SF_SOURCE_SIF2_P1){
		//libsif2_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk);
		libsif2_init(&(sflash->libsif_cfg_sif_flash), sflash->dma_en, 1, sflash->clk); //TODO : donnot support dma mode for now
	}

	sif_flash_init_internal(sflash);
	
	u8 id = sif_flash_read_deviceid();

	return id;
}
