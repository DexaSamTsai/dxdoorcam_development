#include "includes.h"

/*sfc flash command definition*/
#define	SFC_CMD_JEDECID          0x9f
#define	SFC_CMD_QUAD_IO_ID       0x94
#define	SFC_CMD_RD               0x03
#define	SFC_CMD_FAST_RD          0x0B
#define	SFC_CMD_FAST_RD_DUAL_O   0x3B
#define	SFC_CMD_FAST_RD_DUAL_IO  0xBB
#define	SFC_CMD_FAST_RD_QUAD_O   0x6B
#define	SFC_CMD_FAST_RD_QUAD_IO  0xEB
#define	SFC_CMD_RD_STA_REG1      0x05
#define	SFC_CMD_RD_STA_REG2      0x35
#define	SFC_CMD_WIRTE_STA_REG1   0x01
#define	SFC_CMD_WIRTE_STA_REG2   0x31
#define	SFC_CMD_PAGE_PROGRAM     0x02
#define	SFC_CMD_QUAD_PAGE_PROGRAM 0x32
#define	SFC_CMD_SEC_ERASE        0x20
#define	SFC_CMD_BOCLK_ERASE_32K  0x52
#define	SFC_CMD_BOCLK_ERASE_64K  0xD8
#define	SFC_CMD_CHIP_ERASE       0xC7
#define	SFC_CMD_WRITE_EN         0x06
#define	SFC_CMD_WRITE_DIS        0x04
#define	SFC_CMD_POWERDOWN        0xB9
#define	SFC_CMD_POWERDOWN_DIS    0xAB

/*config flash parameters and flash command here*/
SFLASH_DECLARE(W25Q32FV)
	.trans_mode = 0,
#ifdef CONFIG_FAST_BOOT_EN
	.clk = 45*1000000,
#else
	.clk = 15*1000000,
#endif
	.rx_delay_cycle = 1,
	.ce_active_setuptime = 1,
	.ce_active_holdtime = 1,
	.ce_inactivetime = 0x10,
	.DQ2_mode = DQ2_WP_PIN,
	.DQ2_value = 1,
	.DQ3_mode = DQ3_HOLD_PIN,
	.DQ3_value = 1,

	.page_size = 256,
	.sector_size = 4096,
	.block_size = 64*1024,
	.chip_size = 4*1024*1024,
	.dma_en = 1,
	.post_init = NULL,
	.check_id_en = 1,
	.deviceID = 0x1640ef,

	/*status registers configure*/
	.sta_reg_num = 3,
	.sta_reg_readcmdnum = 3,
	.sta_reg_writecmdnum = 3,
	.sta_reg_qe_bit = STA_REG2_BIT1,

	.cmd_deviceid = {
		.cmd_id = SFC_CMD_JEDECID,
		.addr_len = 0,
		.dummy_cys = 0,
		.data_dummy_cycles = 1,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
#if 1
	.cmd_cur_rd = { 
		.cmd_id = SFC_CMD_FAST_RD_QUAD_IO,
		.addr_len = 3,
		.dummy_cys = 6,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_QUAD_QUAD,
	},
#else
	.cmd_cur_rd = { 
		.cmd_id = SFC_CMD_RD,
		.addr_len = 3,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
#endif
	.cmd_rd_sta_reg1 = {
		.cmd_id = SFC_CMD_RD_STA_REG1,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_rd_sta_reg2 = {
		.cmd_id = SFC_CMD_RD_STA_REG2,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_sta_reg1 = {
		.cmd_id = SFC_CMD_WIRTE_STA_REG1,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_sta_reg2 = {
		.cmd_id = SFC_CMD_WIRTE_STA_REG2,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},

#if 0
	.cmd_cur_write = {
		.cmd_id = SFC_CMD_PAGE_PROGRAM,
		.addr_len = 3,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
#else
	.cmd_cur_write = {
		.cmd_id = SFC_CMD_QUAD_PAGE_PROGRAM,
		.addr_len = 3,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_QUAD,
	},
#endif
	.cmd_sec_erase = {
		.cmd_id = SFC_CMD_SEC_ERASE,
		.addr_len = 3,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_block_erase = {
		.cmd_id = SFC_CMD_BOCLK_ERASE_64K,   
		.addr_len = 3,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_chip_erase = {
		.cmd_id = SFC_CMD_CHIP_ERASE,  	
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_en = {
		.cmd_id = SFC_CMD_WRITE_EN,  	
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_dis ={ 
		.cmd_id = SFC_CMD_WRITE_DIS, 	
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
};

