#include "includes.h"

/*sfc flash command definition*/
#define	SFC_CMD_JEDECID           0x9f
#define	SFC_CMD_QUAD_IO_ID        0xaf

#define	SFC_CMD_RD                0x13
#define	SFC_CMD_FAST_RD           0x0C
#define	SFC_CMD_FAST_RD_DUAL_O    0x3C
#define	SFC_CMD_FAST_RD_DUAL_IO   0xBC
#define	SFC_CMD_FAST_RD_QUAD_O    0x6C
#define	SFC_CMD_FAST_RD_QUAD_IO   0xEC

#define	SFC_CMD_RD_STA_REG1       0x05
#define	SFC_CMD_RD_STA_REG2       0x15
#define SFC_CMD_RD_STA_REG3       0x2B
#define	SFC_CMD_WIRTE_STA_REG1    0x01

#define	SFC_CMD_PAGE_PROGRAM      0x12
#define	SFC_CMD_QUAD_PAGE_PROGRAM 0x3E

#define	SFC_CMD_SEC_ERASE         0x21
#define	SFC_CMD_BOCLK_ERASE_32K   0x5C
#define	SFC_CMD_BOCLK_ERASE_64K   0xDC
#define	SFC_CMD_CHIP_ERASE        0xC7

#define	SFC_CMD_WRITE_EN          0x06
#define	SFC_CMD_WRITE_DIS         0x04
//enter and exit 4B
#define SFC_CMD_ENTER_4B          0xB7
#define SFC_CMD_EXIT_4B           0xE9

/*config flash parameters and flash command here*/
SFLASH_DECLARE(MX25U25645G)
	.trans_mode = 0,
#ifdef CONFIG_FAST_BOOT_EN
	.clk = 40*1000000,
#else
	.clk = 40*1000000,
#endif
    .xip_mode = 0,
	.rx_delay_cycle = 1,
	.ce_active_setuptime = 1,
	.ce_active_holdtime = 1,
	.ce_inactivetime = 0x10,
	.DQ2_mode = DQ2_WP_PIN,
	.DQ2_value = 1,
	.DQ3_mode = DQ3_HOLD_PIN,
	.DQ3_value = 1,

	.page_size = 256,
	.sector_size = 4096,
	.block_size = 64*1024,
	.chip_size = 32*1024*1024,
#ifdef CONFIG_FAST_BOOT_EN
	.dma_en = 1,
#else
	.dma_en = 0,
#endif
	.post_init = NULL,
	.check_id_en = 1,
	.deviceID = 0xc23925c2,
	/*status registers configure*/
	.sta_reg_num = 3,
	.sta_reg_readcmdnum = 3,
	.sta_reg_writecmdnum = 1,
	.sta_reg_qe_bit = STA_REG1_BIT6,
	.cmd_deviceid = {
		.cmd_id = SFC_CMD_JEDECID,
		.addr_len = 0,
		.dummy_cys = 0,
		.data_dummy_cycles = 1,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_cur_rd = {
		.cmd_id = SFC_CMD_FAST_RD_QUAD_IO,
		.addr_len = 4,
		.dummy_cys = 6,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_QUAD_QUAD,
	},
	.cmd_cur_write = {
		.cmd_id = SFC_CMD_QUAD_PAGE_PROGRAM,
		.addr_len = 4,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_QUAD_QUAD,
	},
	.cmd_rd_sta_reg1 = {
		.cmd_id = SFC_CMD_RD_STA_REG1,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_rd_sta_reg2 = {
		.cmd_id = SFC_CMD_RD_STA_REG2,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_rd_sta_reg3 = {
			.cmd_id = SFC_CMD_RD_STA_REG3,
			.addr_len = 0,
			.dummy_cys = 0,
			.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
		},
	.cmd_write_sta_reg1 = {
		.cmd_id = SFC_CMD_WIRTE_STA_REG1,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_sec_erase = {
		.cmd_id = SFC_CMD_SEC_ERASE,
		.addr_len = 4,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_block_erase = {
		.cmd_id = SFC_CMD_BOCLK_ERASE_64K,
		.addr_len = 4,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_chip_erase = {
		.cmd_id = SFC_CMD_CHIP_ERASE,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_en = {
		.cmd_id = SFC_CMD_WRITE_EN,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
	.cmd_write_dis ={
		.cmd_id = SFC_CMD_WRITE_DIS,
		.addr_len = 0,
		.dummy_cys = 0,
		.sfc_sif_mode = SFC_SIF_MODE_SINGLE_SINGLE_SINGLE,
	},
};

