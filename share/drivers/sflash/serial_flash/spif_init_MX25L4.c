#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define WRITE_STATUS_REG_CMD        0x01
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define PAGE_PROGRAM_CMD            0x02
#define ERASE_4K_CMD         	    0x20
#define ERASE_64K_CMD               0xd8
#define CHIP_ERASE_CMD              0xc7
#define POWER_DOWN_CMD              0xb9
#define RELEASE_POWER_DOWN_CMD      0xab
#define DEVICEID_CMD                0xab
#define MANUFACTURER_CMD            0x90

SFLASH_DECLARE(MX25L4)
	.page_size = 256,
	.sector_size = 4096,
	.block_size = 64*1024,
	.chip_size = 0,
	.fDMA = 0,
	.timeout = 0x100000,
	.deviceID = 0x12,
	.cmd_read_status_reg1 = READ_STATUS_REG1_CMD,
	.cmd_write_enable = WRITE_ENABLE_CMD,
	.deviceid_dummy_bytes = 3,
	.cmd_deviceid = DEVICEID_CMD,
	.cmd_chip_erase = CHIP_ERASE_CMD,
	.cmd_erase_sector = ERASE_4K_CMD,
	.cmd_erase_block = ERASE_64K_CMD,
	.cmd_fast_read = FAST_READ_CMD,
	.cmd_read_data = READ_DATA_CMD,
	.cmd_page_program = PAGE_PROGRAM_CMD,
	.cmd_write_status_reg = WRITE_STATUS_REG_CMD,
};

int sf_init_MX25L4(t_libsif_cfg *cfg){

	sf_libsif_cfg = cfg;
	g_sf = &sf;

	return sf_init_internal(1);
}
