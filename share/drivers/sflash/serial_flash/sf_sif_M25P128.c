#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define WRITE_STATUS_REG_CMD        0x01
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define PAGE_PROGRAM_CMD            0x02
#define ERASE_SECTOR_CMD    	    0xd8
#define ERASE_CHIP_CMD              0xc7
#define JEDECID_CMD                 0x9f
#define MANUFACTURER_CMD            0x90

SFLASH_DECLARE(M25P128)
	.page_size = 256,
	.sector_size = 256*1024,
	.chip_size = 0,
	.clk = 1000000,
	.dma_en = 0,
	.timeout = 0x100000,
	.deviceID = 0x20,
	.cmd_rd_sta_reg1 = {
		.cmd_id = READ_STATUS_REG1_CMD,
	},
	.cmd_write_en = {
		.cmd_id = WRITE_ENABLE_CMD,
	},
	.cmd_deviceid = {
		.cmd_id = JEDECID_CMD,
		.data_dummy_cycles = 1,
	},
	.cmd_chip_erase = {
		.cmd_id = ERASE_CHIP_CMD,
	},
	.cmd_sec_erase = {
		.cmd_id = ERASE_SECTOR_CMD,
	},
	.cmd_fast_read = {
		.cmd_id = FAST_READ_CMD,
	},
	.cmd_cur_rd = {
		.cmd_id = READ_DATA_CMD,
		.fast_read_en = 0,
	},
	.cmd_cur_write = {
		.cmd_id = PAGE_PROGRAM_CMD,
	},
	.cmd_write_sta_reg = {
		.cmd_id = WRITE_STATUS_REG_CMD,
	},
};

/*
int sf_init_PM25LD(t_libsif_cfg *cfg){

	sf_libsif_cfg = cfg;

	return sf_init_internal(1);
}
*/
