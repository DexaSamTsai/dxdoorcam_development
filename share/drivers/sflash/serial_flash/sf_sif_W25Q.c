#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define READ_STATUS_REG2_CMD        0x35
#define WRITE_STATUS_REG_CMD        0x01
#define PAGE_PROGRAM_CMD            0x02
#define QUAD_PAGE_PROGRAM_CMD       0x32
#define ERASE_64K_CMD         0xd8
#define ERASE_32K_CMD         0x52
#define ERASE_4K_CMD         0x20
#define CHIP_ERASE_CMD              0xc7
#define ERASE_SUSPEND_CMD           0x75
#define ERASE_RESUME_CMD            0x7a
#define POWER_DOWN_CMD              0xb9
#define HIGH_PERF_MODE_CMD          0xa3
#define CONT_READ_MODE_RESET_CMD    0xff
#define RELEASE_POWER_DOWN_CMD      0xab
#define DEVICEID_CMD                0xab
#define MANUFACTURER_CMD            0x90
#define READUNIQUEID_CMD            0x4b
#define JEDECID_CMD                 0x9f
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define FAST_READ_DUALO_CMD         0x3b
#define FAST_READ_DUALIO_CMD        0xbb
#define FAST_READ_QUADO_CMD         0x6b
#define FAST_READ_QUADIO_CMD        0xeb
#define QCTAL_WORD_READ_QUADIO_CMD  0xe3

SFLASH_DECLARE(W25Q)
};

int sf_init_W25Q(t_libsif_cfg *cfg){

	sf_libsif_cfg = cfg;
	g_sf = &sf;

	return sf_init_internal(1);
}
