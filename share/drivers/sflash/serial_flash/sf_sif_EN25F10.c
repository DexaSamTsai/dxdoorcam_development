#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define WRITE_STATUS_REG_CMD        0x01
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define PAGE_PROGRAM_CMD            0x02
#define ERASE_4K_CMD         	    0x20
#define ERASE_32K_CMD               0xd8
#define CHIP_ERASE_CMD              0xc7
#define POWER_DOWN_CMD              0xb9
#define RELEASE_POWER_DOWN_CMD      0xab
#define DEVICEID_CMD                0xab
#define MANUFACTURER_CMD            0x90

SFLASH_DECLARE(EN25F10)
};

int sf_init_EN25F10(t_libsif_cfg *cfg){

	sf_libsif_cfg = cfg;
	g_sf = &sf;

	return sf_init_internal(1);
}
