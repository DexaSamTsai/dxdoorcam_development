#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define READ_STATUS_REG2_CMD        0x35
#define WRITE_STATUS_REG_CMD        0x01
#define PAGE_PROGRAM_CMD            0x02
#define QUAD_PAGE_PROGRAM_CMD       0x32
#define ERASE_64K_CMD         0xd8
#define ERASE_32K_CMD         0x52
#define ERASE_4K_CMD         0x20
#define CHIP_ERASE_CMD              0xc7
#define ERASE_SUSPEND_CMD           0x75
#define ERASE_RESUME_CMD            0x7a
#define POWER_DOWN_CMD              0xb9
#define HIGH_PERF_MODE_CMD          0xa3
#define CONT_READ_MODE_RESET_CMD    0xff
#define RELEASE_POWER_DOWN_CMD      0xab
#define DEVICEID_CMD                0xab
#define MANUFACTURER_CMD            0x90
#define READUNIQUEID_CMD            0x4b
#define JEDECID_CMD                 0x9f
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define FAST_READ_DUALO_CMD         0x3b
#define FAST_READ_DUALIO_CMD        0xbb
#define FAST_READ_QUADO_CMD         0x6b
#define FAST_READ_QUADIO_CMD        0xeb
#define QCTAL_WORD_READ_QUADIO_CMD  0xe3

SFLASH_DECLARE(W25Q32DW)
	.page_size = 256,
	.sector_size = 4096,
	.block_size = 32 * 1024,
	.chip_size = 0,
	.clk = 18000000,
	.dma_en = 0,
	.timeout = 0x100000,
	.deviceID = 0xef,
	.cmd_rd_sta_reg1 = {
		.cmd_id = READ_STATUS_REG1_CMD,
	},
	.cmd_write_en = {
		.cmd_id = WRITE_ENABLE_CMD,
	},
	.cmd_deviceid = {
		.cmd_id = JEDECID_CMD,
		.data_dummy_cycles = 0,
	},
	.cmd_chip_erase = {
		.cmd_id = CHIP_ERASE_CMD,
	},
	.cmd_sec_erase = {
		.cmd_id = ERASE_4K_CMD,
	},
	.cmd_block_erase = {
		.cmd_id = ERASE_64K_CMD,
	},
	.cmd_fast_read = {
		.cmd_id = FAST_READ_CMD,
	},
	.cmd_cur_rd = {
		.cmd_id = READ_DATA_CMD,
		.fast_read_en = 0,
	},
	.cmd_cur_write = {
		.cmd_id = PAGE_PROGRAM_CMD,
	},
	.cmd_write_sta_reg = {
		.cmd_id = WRITE_STATUS_REG_CMD,
	},
};
