#include "includes.h"

#define WRITE_ENABLE_CMD            0x06
#define WRITE_DISABLE_CMD           0x04
#define READ_STATUS_REG1_CMD        0x05
#define WRITE_STATUS_REG_CMD        0x01
#define READ_DATA_CMD               0x03
#define FAST_READ_CMD               0x0b
#define PAGE_PROGRAM_CMD            0x02
#define ERASE_SECTOR_4K_CMD    	    0x20
#define ERASE_BLOCK_64K_CMD               0xd8
#define ERASE_CHIP_CMD              0xc7
#define POWER_DOWN_CMD              0xb9
#define RELEASE_POWER_DOWN_CMD      0xab
#define MANUFACTURER_CMD            0x9f

SFLASH_DECLARE(MX25L1)
	.page_size = 32,
	.sector_size = 4096,
	.block_size = 64*1024,
	.op_size = 0,
	.clk = 1000000,
	.dma_en = 0,
	.fDMA = 0,
	.timeout = 0x100000,
	.deviceID = 0xc2,
	.cmd_read_status_reg1 = READ_STATUS_REG1_CMD,
	.cmd_write_enable = WRITE_ENABLE_CMD,
	.deviceid_dummy_bytes = 0,
	.cmd_deviceid =  MANUFACTURER_CMD,
	.cmd_chip_erase = ERASE_CHIP_CMD,
	.cmd_erase_sector = ERASE_SECTOR_4K_CMD,
	.cmd_erase_block = ERASE_BLOCK_64K_CMD
	.cmd_fast_read = FAST_READ_CMD,
	.fast_read_en = 1,
	.cmd_read_data = READ_DATA_CMD,
	.cmd_page_program = PAGE_PROGRAM_CMD,
	.cmd_write_status_reg = WRITE_STATUS_REG_CMD,
};
#if 0
int sf_init_MX25L1(t_libsif_cfg *cfg){

	sf_libsif_cfg = cfg;
	g_sf = &sf;

	//sf_write_status_reg(0x0);

	return sf_init_internal(1);
}
#endif
