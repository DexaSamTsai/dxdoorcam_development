/*sfc_flash.c - a driver for sfc flash, based on sfc_flash_internal.c, but only include init funciton*/
#include "includes.h"

/*init sfc flash*/
s32 sfc_flash_init_internal(t_sflash *sflash);
s32 sfc_flash_init(t_sflash *sflash)
{
	u32 pre_value = 0;	

#ifdef FOR_FPGA
	WriteReg32(REG_SC_DIV5, ReadReg32(REG_SC_DIV5)| BIT11| BIT12);
	sflash->module_clk = 48000000;
#else
	//move to clockconfig_xxx.c
	//WriteReg32(REG_SC_DIV5, ReadReg32(REG_SC_DIV5) | BIT11 & (~BIT12));
	sflash->module_clk = clockget_sfcsys();
#endif

	s32 ret = sfc_flash_init_internal(sflash);
	sflash->cmd_cur_write.sfc_mode = SFC_MODE_CSR;

	if(sflash->dma_en == 1){
		libdma_init();
		if(sflash->dma_timeout == 0){
			sflash->dma_timeout = 0x1000000;
		}
	}

	/*enable qe bit*/
	if((sflash->sta_reg_qe_bit&0xf0)>>4 == 1){ //if 1, qe locate in status reg1
		if(sflash->sta_reg_writecmdnum > 1){
			pre_value = libsfc_flash_read_status_reg1() & 0xff;
			ret = libsfc_flash_write_status_reg1( pre_value | (0x1<<(sflash->sta_reg_qe_bit&0xf)) );
		}else{
			if(sflash->sta_reg_readcmdnum > 1){
				pre_value = libsfc_flash_read_status_reg1() | 
					((sflash->sta_reg_num>1) ? (libsfc_flash_read_status_reg2()<<8) : 0)  |
					((sflash->sta_reg_num>2) ? (libsfc_flash_read_status_reg3()<<16) : 0);
			}else{
				pre_value = libsfc_flash_read_status_reg1();
			}
			ret = libsfc_flash_write_status_reg1(pre_value | (0x1<<(sflash->sta_reg_qe_bit&0xf)) );
		}
	}else if( ((sflash->sta_reg_qe_bit&0xf0)>>4) == 2 ){//if 2, qe locate in status reg2
		if(sflash->sta_reg_writecmdnum > 1){
			if(sflash->sta_reg_readcmdnum > 1){
				pre_value = libsfc_flash_read_status_reg2();
			}else{
				pre_value = (libsfc_flash_read_status_reg1()>>8)&0xff;
			}
			ret = libsfc_flash_write_status_reg2(pre_value |
					(0x1<<(sflash->sta_reg_qe_bit&0xf)) );
		}else{
			if(sflash->sta_reg_readcmdnum > 1){
				pre_value = libsfc_flash_read_status_reg1() | 
					(libsfc_flash_read_status_reg2() << 8)  |
					((sflash->sta_reg_num == 3) ? (libsfc_flash_read_status_reg3()<<16) : 0);
			}else{
				pre_value = libsfc_flash_read_status_reg1();
			}
			ret = libsfc_flash_write_status_reg1(pre_value | ((0x1<<(sflash->sta_reg_qe_bit&0xf))<<8) );
		}
	}else if( ((sflash->sta_reg_qe_bit&0xf0)>>4) == 3 ){//if 3, qe locate in status reg3
		if(sflash->sta_reg_writecmdnum > 1){
			if(sflash->sta_reg_readcmdnum > 1){
				pre_value = libsfc_flash_read_status_reg3();
			}else{
				pre_value = (libsfc_flash_read_status_reg1()>>16)&0xff;
			}
			ret = libsfc_flash_write_status_reg3(pre_value |
					(0x1<<(sflash->sta_reg_qe_bit&0xf)) );
		}else{
			if(sflash->sta_reg_readcmdnum > 1){
				pre_value = libsfc_flash_read_status_reg1() | 
					(libsfc_flash_read_status_reg2() << 8)   | 
					(libsfc_flash_read_status_reg3() << 16);
			}else{
				pre_value = libsfc_flash_read_status_reg1();
			}
			ret = libsfc_flash_write_status_reg1(pre_value | ((0x1<<(sflash->sta_reg_qe_bit&0xf))<<16) );
		}
	}

	if(ret < 0) return ret;

	if(sflash->post_init != NULL){
		ret = sflash->post_init();
	}

	/*read id*/
	if(sflash->check_id_en == 1){
		u8 id = sfc_flash_read_deviceid();
		if(id > 0 && id != 0xff){
			if(id != sflash->deviceID){
				syslog(LOG_WARNING, "flash id not match, read:0x%x(0x%x)\n", id, sflash->deviceID);
			}
			ret = 0;
		}else{
			return -ENXIO;
		}
	}
	
	return ret;
}

u32 sfc_flash_readid(t_sflash *sflash)
{
#ifdef FOR_FPGA
	WriteReg32(REG_SC_DIV5, ReadReg32(REG_SC_DIV5)| BIT11| BIT12);
	sflash->module_clk = 48000000;
#else
	//move to clockconfig_xxx.c
	//WriteReg32(REG_SC_DIV5, ReadReg32(REG_SC_DIV5) | BIT11 & (~BIT12));
	sflash->module_clk = clockget_sfcsys();
#endif

	s32 ret = sfc_flash_init_internal(sflash);
	if(ret < 0) return 0;

	if(sflash->post_init != NULL){
		ret = sflash->post_init();
		if(ret < 0) return 0;
	}

	/*read id*/
	u32 id = sfc_flash_read_deviceid();
	return id;
}
