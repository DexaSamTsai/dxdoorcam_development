#include "includes.h"

static pthread_mutex_t cache_mutex = PTHREAD_MUTEX_INITIALIZER;

/* cache */
static int res_id2 = 0;
static u32 res_offset = 0;

typedef struct s_res_group_cache
{
	unsigned int group_id;
	unsigned int offset;
	unsigned int itemcount;
	unsigned int iitlen;
	unsigned int iilen;
}t_res_group_cache;

typedef struct s_res_item_cache
{
	unsigned int group_id;
	unsigned int item_id;
	unsigned int size;
	unsigned int offset;
	unsigned int keylen;
	unsigned int index_offset; //this offset is absolute from flash offset 0
#define ITEMCACHE_KEYSIZE 32
	char key[ITEMCACHE_KEYSIZE];
}t_res_item_cache;

#define GROUPCACHE_CNT 4
static t_res_group_cache groupcache[GROUPCACHE_CNT];
#define ITEMCACHE_CNT 16
static t_res_item_cache itemcache[ITEMCACHE_CNT];

static t_res_group_cache * groupcache_find(int group)
{
	int i;

	if(g_ertos->schedule_running){
		pthread_mutex_lock(&cache_mutex);
	}

	//find in cache
	int notusedgroup = -1;
	for(i=0; i<GROUPCACHE_CNT; i++){
		if(groupcache[i].group_id == group){
			notusedgroup = i;
			goto exit;
		}
		if(notusedgroup == -1 && groupcache[i].group_id == 0){
			notusedgroup = i;
		}
	}

	//not found in cache
	if(notusedgroup == -1){
		//all are used, use the last cache
		notusedgroup = GROUPCACHE_CNT - 1;
	}
	groupcache[notusedgroup].group_id = 0;

	//fetch to groupcache
	sf_read((u8*)&groupcache[notusedgroup].offset, 4, res_offset + (group-1)*4 + 8);
	sf_read((u8*)&groupcache[notusedgroup].itemcount, 12, res_offset + groupcache[notusedgroup].offset + 4);
	groupcache[notusedgroup].group_id = group;

exit:
	if(g_ertos->schedule_running){
		pthread_mutex_unlock(&cache_mutex);
	}

	return &groupcache[notusedgroup];
}

static t_res_item_cache * itemcache_find(t_res_group_cache * group, int item, char * key)
{
	int i;
	int keylen = 0;

	if(item == 0){
		keylen = strlen(key) + 1;
	}

	if(g_ertos->schedule_running){
		pthread_mutex_lock(&cache_mutex);
	}

	//find in cache
	int notuseditem = -1;
	for(i=0; i<ITEMCACHE_CNT; i++){
		if(item != 0){
			if(itemcache[i].group_id == group->group_id && itemcache[i].item_id == item){
				notuseditem = i;
				goto exit;
			}
		}else{
			if(itemcache[i].group_id == group->group_id){
				if(itemcache[i].keylen == keylen){
					if(keylen < ITEMCACHE_KEYSIZE){
						if(!strcmp(key, itemcache[i].key)){
							notuseditem = i;
							goto exit;
						}
					}else{
						if(!strncmp(key, itemcache[i].key, ITEMCACHE_KEYSIZE - 1)){
							char * fullkeybuf = malloc(keylen);
							if(fullkeybuf == NULL){
								syslog(LOG_ERR, "ERROR: itemcache_find malloc failed\n");
								notuseditem = -1;
								goto exit;
							}
							sf_read((u8 *)fullkeybuf, keylen, itemcache[i].index_offset + 0xc);
							if(!strcmp(key, fullkeybuf)){
								free(fullkeybuf);
								notuseditem = i;
								goto exit;
							}else{
								free(fullkeybuf);
							}
						}
					}
				}
			}
		}
		if(notuseditem == -1 && itemcache[i].group_id == 0){
			notuseditem = i;
		}
	}

	//not found in cache
	if(notuseditem == -1){
		//all are used, use the last cache
		notuseditem = ITEMCACHE_CNT - 1;
	}
	itemcache[notuseditem].group_id = 0;

	//fetch to itemcache
	if(item != 0){
		sf_read((u8 *)&itemcache[notuseditem].size, 8, res_offset + group->offset + 0x10 + group->iilen * (item - 1));
		goto exit;
	}else{
		char * fullkeybuf = malloc(keylen);
		if(fullkeybuf == NULL){
			syslog(LOG_ERR, "ERROR: itemcache_find malloc failed\n");
			notuseditem = -1;
			goto exit;
		}
		u32 itemindex_offset = res_offset + group->offset + 0x10;
		for(i=0; i<group->itemcount; i++){
			sf_read((u8 *)&itemcache[notuseditem].size, 12, itemindex_offset);
			if(itemcache[notuseditem].keylen == keylen){
				sf_read((u8 *)fullkeybuf, keylen, itemindex_offset + 0xc);
				if(!strcmp(key, fullkeybuf)){
					itemcache[notuseditem].index_offset = itemindex_offset;
					strncpy(itemcache[notuseditem].key, key, ITEMCACHE_KEYSIZE - 1);
					itemcache[notuseditem].key[ITEMCACHE_KEYSIZE - 1] = '\0';

					free(fullkeybuf);
					goto exit;
				}
			}
			itemindex_offset += ( ((itemcache[notuseditem].keylen + 3) & ~3) + 0xc);
		}
		free(fullkeybuf);
		notuseditem = -1;
		goto exit;
	}

exit:
	if(g_ertos->schedule_running){
		pthread_mutex_unlock(&cache_mutex);
	}

	if(notuseditem < 0){
		return NULL;
	}else{
		return &itemcache[notuseditem];
	}
}

int partition_res_load_sflash(int group, int item, char * key, unsigned char * buf, int skip, int maxlen, int * totallen)
{
	if(g_ertos->schedule_running){
		pthread_mutex_lock(&cache_mutex);
	}

	if(res_id2 == 0){
		u32 fw_offset;
		sf_read((u8*)&fw_offset, 4, 0x4c);

		u32 fw_bin_cnt;
		sf_read((u8*)&fw_bin_cnt, 4, fw_offset + 0xc);
		
		sf_read((u8*)&res_id2, 4, fw_offset + 0x10 + fw_bin_cnt*8);
		res_id2 &= 0xff;

		if(res_id2 < 2){
			//res not exist
			res_id2 = 0;

			if(g_ertos->schedule_running){
				pthread_mutex_unlock(&cache_mutex);
			}

			return -1;
		}

		sf_read((u8*)&res_offset, 4, fw_offset + 0x10 + (res_id2-2)*8);
		res_offset += fw_offset;
	}

	if(g_ertos->schedule_running){
		pthread_mutex_unlock(&cache_mutex);
	}

	t_res_group_cache * ginfo = groupcache_find(group);
	if(ginfo == NULL){
		return -2;
	}

	if(item != 0){
		if(ginfo->iilen == 0){
			syslog(LOG_ERR, "ERROR: partition_res_load_sflash: item !=0, iilen == 0\n");
			return -3;
		}
	}else{
		if(key == NULL || ginfo->iilen != 0){
			syslog(LOG_ERR, "ERROR: partition_res_load_sflash: item ==0, iilen != 0\n");
			return -4;
		}
	}

	t_res_item_cache * iinfo = itemcache_find(ginfo, item, key);
	if(iinfo == NULL){
		return -10;
	}

	if(totallen){
		*totallen = iinfo->size;
	}

	int n = maxlen;
	if(maxlen > iinfo->size - skip){
		n = iinfo->size - skip;
	}
	if(n){
		sf_read(buf, n, res_offset + ginfo->offset + iinfo->offset + skip);
	}

	return n;
}
