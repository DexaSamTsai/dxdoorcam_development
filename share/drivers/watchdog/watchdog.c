#include "includes.h"

int watchdog_init(void)
{
	return 0;
}

static u32 wd_to = 0;

int watchdog_set(u32 timeout)
{
	if(timeout == 0){
		wd_to = timeout;
		WriteReg32(REG_SC_WATCHDOG, 0x3fffffff);
		return 0;
	}else{
		if(timeout >= (1 << 30)){
			syslog(LOG_ERR, "watchdog_set: timeout too large\n");
			return -1;
		}
		wd_to = timeout;
		WriteReg32(REG_SC_WATCHDOG, (BIT31 | BIT30 | (timeout)));
		WriteReg32(REG_SC_WATCHDOG, (BIT30 | (timeout)));
		return 0;
	}
}

int watchdog_feed(void)
{
	if(wd_to){
		WriteReg32(REG_SC_WATCHDOG, (BIT31 | BIT30 | (wd_to)));
		WriteReg32(REG_SC_WATCHDOG, (BIT30 | (wd_to)));
		return 0;
	}else{
		return -1;
	}
}
