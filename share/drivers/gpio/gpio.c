#include "includes.h"

#define GPIO_RTC_ADDR	0xE0010000

int libgpio_pulldown_disable(int pin)
{
	if(pin >= PIN_GPIO_00 && pin <= PIN_GPIO_11){
		WriteReg16(GPIO_RTC_ADDR + 0x4c, ReadReg16(GPIO_RTC_ADDR + 0x4c) | (1 << pin));
		return 0;
	}else{
		return -1;
	}
}

int libgpio_pulldown_enable(int pin)
{
	if(pin >= PIN_GPIO_00 && pin <= PIN_GPIO_11){
		WriteReg16(GPIO_RTC_ADDR + 0x4c, ReadReg16(GPIO_RTC_ADDR + 0x4c) & ~(1 << pin));
		return 0;
	}else{
		return -1;
	}
}

void libgpio_pulldown_disableall(void)
{
	WriteReg16(GPIO_RTC_ADDR + 0x4c, 0x7ff);
}
