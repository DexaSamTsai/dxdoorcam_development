#include "includes.h"

//#define DEBUG_EN
t_msp430_bsl_cfg g_msp430_bsl_cfg;

#define SUCCESSFUL_OPERATION 0x00
#define FLASH_ACK         SUCCESSFUL_OPERATION
#define BSL_DATA_REPLY    0x3A

#define RX_DATA_BLOCK  0x10
#define RX_PASSWORD    0x11
#define ERASE_SEGMENT  0x12
#define MASS_ERASE     0x15
#define LOAD_PC        0x17
#define TX_DATA_BLOCK  0x18
#define SET_BAUDRATE	   0x52

#define BSL_DELAY(x) {volatile int delay; for(delay=0; delay<x;delay++);}

static void _bsl_rx_clear(void)
{
	while(1){
		{volatile int d; for(d=0;d<1000000;d++);}
		int i = uarts_testgetc();
		if(i < 0){
			break;
		}
	}
}

static u16 _bsl_crc;
static void _bsl_crc_in(unsigned char data)
{
	u16 x;
	x = ((_bsl_crc>>8) ^ data) & 0xff;
	x ^= x>>4;
	_bsl_crc = (_bsl_crc << 8) ^ (x << 12) ^ (x <<5) ^ x;
}

static unsigned char _bsl_txbuf(u8 * buf, int len)
{
	unsigned int i;
	unsigned char answer;

	_bsl_crc = 0xFFFF;

	uarts_putc(0x80);
	uarts_putc((unsigned char)(len&0xFF) );
	uarts_putc((unsigned char)((len>>8)&0xFF) );

#ifdef DEBUG_EN
	debug_printf("!!!!!!!!tx data:");
#endif
	for( i = 0; i < len; i++ )
  	{
#ifdef DEBUG_EN
		debug_printf("0x%x ", buf[i]);
#endif
		uarts_putc(buf[i]);
		_bsl_crc_in(buf[i]);
	}
	uarts_putc((unsigned char)(_bsl_crc&0xFF));
	uarts_putc((unsigned char)((_bsl_crc>>8)&0xFF));

  	answer = uarts_getc();
  	if( answer != FLASH_ACK )
	{
#ifdef DEBUG_EN
		debug_printf("txbuf rx clear:%d", answer);
#endif
		_bsl_rx_clear();
 	}

#ifdef DEBUG_EN
  	debug_printf("bsl answer:%d\n",answer);
#endif

	return answer;
}

#define TIMEOUT_ERROR    0xEE
#define HEADER_INCORRECT 0x51
#define CHECKSUM_INCORRECT 0x52 

static unsigned char _bsl_rxchar(void)
{
#define READ_TIMES 8000000
	unsigned int i;
	for(i=0;i<READ_TIMES;i++)
	{
		int rece =0;
		rece = uarts_testgetc();
		if(rece != -1)
		{
#ifdef DEBUG_EN
			debug_printf("R%x", rece);
#endif
			return rece & 0xff;
		}
	}
	if(i == READ_TIMES)
	{
		debug_printf("uarts read timeout\n");
		return TIMEOUT_ERROR;
	}
	return 0;
}

static unsigned char _bsl_rxbuf(u8 * buf, u32 * len)
{
	unsigned int i;
	unsigned char scrap_var;
	unsigned char RX_checksum_low = 0x00;
	unsigned char RX_checksum_high = 0x00;
	unsigned char RX_header;

	do{
		RX_header = _bsl_rxchar();
	}while(RX_header == 0);
	if( RX_header == TIMEOUT_ERROR )
	{
		debug_printf("response timeout\n");
		return TIMEOUT_ERROR;
	}
	else if(RX_header != 0x80)
	{
		debug_printf("Header != 0x80:%x\n", RX_header);
		_bsl_rx_clear();
		return HEADER_INCORRECT;
	}
	scrap_var = _bsl_rxchar();
	if( scrap_var == TIMEOUT_ERROR )
	{
		return TIMEOUT_ERROR;
	}
	*len = scrap_var;
	scrap_var = _bsl_rxchar();
	if( scrap_var == TIMEOUT_ERROR )
	{
		return TIMEOUT_ERROR;
	}
	*len |= scrap_var<<8;
//	debug_printf("response size:%d\n",db->size);
	_bsl_crc = 0xFFFF;
	for( i = 0; i < *len; i++ )
 	{
		buf[i] = _bsl_rxchar();
		_bsl_crc_in(buf[i]);
	}
	//checksun low
	RX_checksum_low = _bsl_rxchar();
	if( RX_checksum_low != (_bsl_crc &0xff))
	{
#ifdef DEBUG_EN
		debug_printf("error checksum low byte,read:%x,crc calculated:%x\n",RX_checksum_low, _bsl_crc);
#endif
		_bsl_rx_clear();
		return CHECKSUM_INCORRECT;
	}
	RX_checksum_high = _bsl_rxchar();
	if( RX_checksum_high != ((_bsl_crc>>8) &0xff))
	{
#ifdef DEBUG_EN
		debug_printf("error checksum high byte,read:%x,crc calculated:%x\n",RX_checksum_high, _bsl_crc);
#endif
		_bsl_rx_clear();
		return CHECKSUM_INCORRECT;
	}
	return FLASH_ACK;
}

static unsigned char _bsl_get_resp( unsigned char PI_response )
{
	if( PI_response != FLASH_ACK )
	{
		return PI_response;
	}
	else
	{
		u8 rxbuf[256];
		u32 len;
		PI_response = _bsl_rxbuf(rxbuf, &len);
		if( PI_response != FLASH_ACK )
		{
			debug_printf("FAIL(%x)\n",PI_response);
			return PI_response;
		}
		else if ( rxbuf[1] != FLASH_ACK )
		{
			return rxbuf[1];
		}
		else
		{
			return FLASH_ACK;
		}
	}
}

static u8 c2i(u8 p)
{
	if((p <='F')&&(p >='A'))
		return (p-'A'+10);
	if((p <='f')&&(p >='a'))
		return (p-'a'+10);
	if((p <='9')&&(p >='0'))
		return (p-'0');
	return 0;
}

static int _titxt_cmpblheader(u8 * buf, int len, u8 * blheader){
	u8 * c = buf + 5;

	int i = 0;
	while(1){
		if(*c == '\0' || *c == '@'){
			break;
		}
		if(*c == '\r' || *c == '\n' || *c == ' '){
			c++;
			continue;
		}
		if( ((c2i(*c) << 4) | c2i(*(c+1))) != blheader[i] ){
			return 1;
		}
		c+=2;
		i++;
		if(i >= 16){
			break;
		}
		continue;
	}
	if(i >= 16){
		return 0;
	}
	return -1;
}

static int _titxt_getpw(u8 * buf, int len, u8 * pw)
{
	u8 * c = buf;
	while(*c != '\0'){
		if(*c == '@'){
			if(strncmp((char *)c, "@ffce", 5) == 0){
				break;
			}
		}
		c++;
	}
	if(*c != '@'){
		return -1;
	}
	c+=5;
	int i = -18;
	while(1){
		if(*c == '\0' || *c == '@'){
			break;
		}
		if(*c == '\r' || *c == '\n' || *c == ' '){
			c++;
			continue;
		}
		if(i < 0){
			c+=2;
			i++;
			continue;
		}
		pw[i] = (c2i(*c) << 4) | c2i(*(c+1));
		c+=2;
		i++;
		if(i >= 32){
			break;
		}
		continue;
	}
	if(i >= 32){
		return 0;
	}
	return 1;
}

//return buf used
static int _bsl_read_TIText(u8 *inbuf, int maxoutlen, u8 *outbuf, u32 *outlen, u32 * outaddr)
{
	u32 inbuf_used = 0;
	*outlen = 0;

	while(1){
		//handle one line
		u32 linelen = 0;
		for(;linelen < 256 && inbuf[linelen] != '\n'; linelen ++);
		linelen ++;

		if(inbuf[0] == '@'){
			if(*outlen == 0){
				*outaddr = (c2i(inbuf[1])<<12)+(c2i(inbuf[2])<<8)+(c2i(inbuf[3])<<4)+c2i(inbuf[4]);
				inbuf += linelen;
				inbuf_used += linelen;
				continue;
			}else{
				break;
			}
		}else if(inbuf[0] == 'q' || inbuf[0] == 'Q'){
			return 6553600;
		}
		if(isxdigit(inbuf[0])){
			int i;
			for(i=0; i<linelen-3; i+=3){
				outbuf[*outlen] = (c2i(inbuf[i]) << 4) | c2i(inbuf[i+1]);
				(*outlen) = (*outlen) + 1;
			}
		}
		inbuf += linelen;
		inbuf_used += linelen;

		if(*outlen >= maxoutlen){
			break;
		}

		continue;
	}

	return inbuf_used;
}
static int msp430_bsl_set_baudrate(int baudrate)
{
	u8 txbuf[16];
	u8 resp;

	txbuf[0] = SET_BAUDRATE;
	switch (baudrate)
	{
		case 9600:	txbuf[1]=2;break;
		case 19200:	txbuf[1]=3;break;
		case 38400:	txbuf[1]=4;break;
		case 57600:	txbuf[1]=5;break;
		case 115200:
		default:	txbuf[1]=6;break;
	}
	resp = _bsl_txbuf(txbuf, 2);
	_bsl_rx_clear();
	if(resp != 0){
		debug_printf("Set baudrate failed\n");
		return 1;
	}

	uarts_init(clockget_bus(), baudrate, 0);
	WriteReg32(UART_BASE_S + UART_LCR, (UART_LCR_WLEN8 & ~(UART_LCR_STOP )) | UART_LCR_PARITY | BIT4);
	debug_printf("Set baudrate to %d\n",baudrate);
	return 0;	
}

static int msp430_bsl_start(u8 * buf)
{
	//config spi pin as input,for on ng board spi data and uarts are connected together.
	//set here for tmp,need fix
	libgpio_config(PIN_GPIO_60,PIN_DIR_INPUT);  	 
	libgpio_config(PIN_GPIO_61,PIN_DIR_INPUT);

	//init second uart
	//FIXME: 48000000 is a must for test on R3 FPGA board
	uarts_init(clockget_bus(), 9600, 0);

	WriteReg32(UART_BASE_S + UART_LCR, (UART_LCR_WLEN8 & ~(UART_LCR_STOP )) | UART_LCR_PARITY | BIT4);
	//delay for stable and clear buffers
	//FIXME: orig 25000000, shorten it only for convinient of test
	BSL_DELAY(25000);

	//bsl entry sequence
	libgpio_config(g_msp430_bsl_cfg.pin_bsl_rst, PIN_DIR_OUTPUT);
	libgpio_config(g_msp430_bsl_cfg.pin_bsl_jtag, PIN_DIR_OUTPUT);
	libgpio_write(g_msp430_bsl_cfg.pin_bsl_rst, g_msp430_bsl_cfg.bsl_rst_level);

	if(buf == NULL){
		debug_printf("788 reset 430 now,release hardware reset and input enter key:\n ");
		while('\r' !=lowlevel_getc());
	}

	BSL_DELAY(25000);
	libgpio_write(g_msp430_bsl_cfg.pin_bsl_jtag, 0);
	BSL_DELAY(25000);
	//start BSL program execution
	libgpio_write(g_msp430_bsl_cfg.pin_bsl_jtag, 1);
	BSL_DELAY(25000);

	libgpio_write(g_msp430_bsl_cfg.pin_bsl_jtag, 0);
	BSL_DELAY(25000);

	libgpio_write(g_msp430_bsl_cfg.pin_bsl_jtag, 1);
	BSL_DELAY(25000);

	libgpio_write(g_msp430_bsl_cfg.pin_bsl_rst, !(g_msp430_bsl_cfg.bsl_rst_level));
	BSL_DELAY(25000);

	libgpio_write(g_msp430_bsl_cfg.pin_bsl_jtag, 0);
	//FIXME: orig 25000000, shorten it only for convinient of test
	BSL_DELAY(25000);

	//clear buffer
	_bsl_rx_clear();
	
	msp430_bsl_set_baudrate(115200);

	return 0;
}

/* v2: use bootloader + fw + fw_back in MSP430 flash
   detailed please see the MSP430 doorcam_bootloader project
   */
__unused static int msp430_bsl_update_v2(u8 * buf, int len)
{
	u8 txbuf[256];
	u8 resp;

	if(buf == NULL){
		return 1;
	}

	//Try Rx Password 1st round
	debug_printf("getpw\n");
	int ret = _titxt_getpw(buf, len, txbuf+1);
	debug_printf("getpwdone\n");
	if(ret < 0){
		debug_printf("WARN: mcu.bin not correct, do mass erase\n");
		return 1;
	}
	txbuf[0] = RX_PASSWORD;
	resp = _bsl_get_resp(_bsl_txbuf(txbuf, 33));
	if(resp != 0){
		debug_printf("Password not match\n");
		return 1;
	}

	debug_printf("Password match\n");
	//Password match, read 16 bytes bootloader header
	txbuf[0] = TX_DATA_BLOCK;
#define BL_HEADER_ADDR 0xc000
	txbuf[1] = (BL_HEADER_ADDR) & 0xff;
	txbuf[2] = (BL_HEADER_ADDR >> 8) & 0xff;
	txbuf[3] = (BL_HEADER_ADDR >> 16) & 0xff;
	txbuf[4] = 16; //length: 16 bytes
	txbuf[5] = 0;
	resp = _bsl_txbuf(txbuf, 6);
	if( resp != FLASH_ACK ){
		debug_printf("readheader fail:%d\n", resp);
		return 1;
	}
	u8 rxbuf[256];
	u32 rxlen;
	resp = _bsl_rxbuf(rxbuf, &rxlen);
	if( resp != FLASH_ACK ){
		debug_printf("readheader fail1:%d\n", resp);
		return 1;
	}
	if (rxbuf[0] != BSL_DATA_REPLY){
		debug_printf("readheader fail2:%x\n", rxbuf[0]);
		return 1;
	}
	if( _titxt_cmpblheader(buf, len, rxbuf+1) != 0){
		debug_printf("blheader not match, erase\n");
		return 1;
	}

	//erase fw area
#define FLASH_SEGSIZE 512
	u16 fw_start = (rxbuf[11] | (rxbuf[12] << 8)) + 0xc000;
	u16 fw_end = (rxbuf[13] | (rxbuf[14] << 8)) + 0xc000;
	u16 eraseaddr;
	debug_printf("erasefw:%x -> %x\n", fw_start, fw_end - 1);
	for(eraseaddr = fw_start; eraseaddr < fw_end; eraseaddr += 512){
		txbuf[0] = ERASE_SEGMENT;
		txbuf[1] = eraseaddr & 0xff;
		txbuf[2] = (eraseaddr >> 8) & 0xff;
		txbuf[3] = 0;

		resp = _bsl_get_resp(_bsl_txbuf(txbuf, 4));
		if(resp != 0)
		{
			debug_printf("segment erase failed:reply:%x:%x\n", eraseaddr, resp);
			return -1;
		}else{
			debug_printf("segment erase:%x\n", eraseaddr);
		}
	}

	//upgrade fw area only
#define BSL_BUFFERSIZE 240 //must multiple of 16
	u32 rdlen = 0;
	u32 txaddr = 0;
	while(rdlen < len){
		txbuf[0] = RX_DATA_BLOCK;
		u32 txlen = 0;

		rdlen += _bsl_read_TIText(buf + rdlen, BSL_BUFFERSIZE, txbuf + 4, &txlen, &txaddr);

		if(txlen && txaddr >= fw_start && txaddr < fw_end){
			if(txaddr == fw_start){
				//don't burn magic number now
				txaddr += 4;
				txlen -= 4;
				memmove(txbuf + 4, txbuf + 8, txlen);
			}

			txbuf[1] = (txaddr & 0xff);
			txbuf[2] = ((txaddr >> 8) & 0xff);
			txbuf[3] = ((txaddr >> 16) & 0xff);

			debug_printf("TX:%x:%d, %x\n", txaddr, txlen, getu32_be(txbuf + 4));
			resp = _bsl_get_resp(_bsl_txbuf(txbuf, txlen + 4));
			if(resp != 0)
			{
				debug_printf("Tx Data failed:reply:%d\n", resp);
				return -1;
			}
		}
		//increase txaddr
		txaddr += txlen;
	}

	//burn magic number
	txbuf[0] = RX_DATA_BLOCK;
	txbuf[1] = (fw_start & 0xff);
	txbuf[2] = ((fw_start >> 8) & 0xff);
	txbuf[3] = 0;
	txbuf[4] = 0x65;
	txbuf[5] = 0x54;
	txbuf[6] = 0x56;
	txbuf[7] = 0x4f;
	debug_printf("TX:%x:%d, %x\n", fw_start, 4, getu32_be(txbuf + 4));
	resp = _bsl_get_resp(_bsl_txbuf(txbuf, 8));
	if(resp != 0)
	{
		debug_printf("Tx Data failed:reply:%d\n", resp);
		return -1;
	}

	return 0;
}

static int msp430_bsl_mass_erase(void)
{	
	u8 resp;
//Mass Erase
	u8 txbuf = MASS_ERASE;

	resp = _bsl_txbuf(&txbuf, 1);//5869 Mass errase has no response
	_bsl_rx_clear();
	if(resp != 0)
	{
		debug_printf("mass erase failed:reply:%d\n", resp);
		return -1;  	
	}else
	{
		debug_printf("mass erase done\n");
		_bsl_rx_clear();
		msp430_bsl_start((u8 *)1);
	}

	return 0;
}
//do mass erase only when mass_erase!=0
static int msp430_bsl_update_full(u8 * buf, int len)
{
	int i;
	u8 txbuf[256];
	u8 resp;
	
	txbuf[0] = RX_PASSWORD;
	if(g_msp430_bsl_cfg.force_masserase)
	{
		int ret = msp430_bsl_mass_erase();	
		if(ret !=0)
		{
			debug_printf("mass erase failed!\n");
			return ret;
		}
		//if do mass erase,use 0xff as password	
		memset(&txbuf[1],0xff,32);
	}else
	{
		for( i = 0; i < 32; i++ )
		{
			txbuf[i+1] = g_msp430_bsl_cfg.password[i]; 
		}
	}

	if(buf == NULL){
		debug_printf("Erase done, while(1)\n");
		while(1);
	}

	resp = _bsl_get_resp(_bsl_txbuf(txbuf, 33));
	if(resp != 0)
	{
		debug_printf("RX password failed:reply:%d\n", resp);
		return -1;
	}else{
		debug_printf("RX password done\n");
	}

	//Update full
	u32 rdlen = 0;
	u32 txaddr = 0;
	while(rdlen < len){
		txbuf[0] = RX_DATA_BLOCK;
		u32 txlen = 0;

		int i= _bsl_read_TIText(buf + rdlen, BSL_BUFFERSIZE, txbuf + 4, &txlen, &txaddr);
		if(txlen){
			txbuf[1] = (txaddr & 0xff);
			txbuf[2] = ((txaddr >> 8) & 0xff);
			txbuf[3] = ((txaddr >> 16) & 0xff);

			debug_printf("TX:%x:%d, %x\n", txaddr, txlen, getu32_be(txbuf + 4));
			resp = _bsl_get_resp(_bsl_txbuf(txbuf, txlen + 4));
			if(resp != 0)
			{
				debug_printf("Tx Data failed:reply:%d\n", resp);
				return -1;
			}
			if(i == 6553600)
				break;
			else
			{

				rdlen += i;// _bsl_read_TIText(buf + rdlen, BSL_BUFFERSIZE, txbuf + 4, &txlen, &txaddr);
			}
			//increase txaddr
			txaddr += txlen;
		}
	}

	return 0;
}

static int msp430_bsl_loadpc(u8 * buf, int len)
{
	u32 loadpc = 0xc000;
	u8 txbuf[256];

	int ret = _titxt_getpw(buf, len, txbuf+1);
	if(ret < 0){
		debug_printf("WARN: mcu.bin not correct, get PC failed\n");
		return -1;
	}
	loadpc = txbuf[31] | (txbuf[32] << 8); //set loadpc

	debug_printf("LoadPC: %x\n", loadpc);
	//load PC
	txbuf[0] = LOAD_PC;
	txbuf[1] = (loadpc & 0xff);
	txbuf[2] = ((loadpc >> 8) & 0xff);
	txbuf[3] = ((loadpc >> 16) & 0xff);
	if(_bsl_txbuf(txbuf, 4) < 0){
		debug_printf("load PC fail\n");
		return -1;
	}

	return 0;
}

int msp430_bsl_burn(u8 * buf, int len)
{
	msp430_bsl_start(buf);

	int ret = 0;

	ret = msp430_bsl_update_full(buf, len);
	if(ret<0)
	{
		debug_printf("msp430_bsl_update failed!\n");
		return ret;
	}

	if(g_msp430_bsl_cfg.startup_after_burn){
		ret = msp430_bsl_loadpc(buf, len);
	}

	return ret;
}

int msp430_bsl_burn_full(u8 * buf, int len)
{
	int i=0;
	msp430_bsl_start(buf);
	
	int ret = msp430_bsl_update_full(buf, len);
	if(ret < 0){
		return ret;
	}

	if(g_msp430_bsl_cfg.startup_after_burn){
		ret = msp430_bsl_loadpc(buf, len);
	}
	//save the password for use next time
	u8 txbuf[32];
	int tmp = _titxt_getpw(buf, len, txbuf);
	if(tmp < 0){
		debug_printf("WARN: mcu.bin not correct, get password failed,password saved may not work next time!\n");
	}
	else
	{
		for(i=0;i<32;i++)
		{
//			debug_printf("0x%x ",txbuf[i]);
			g_msp430_bsl_cfg.password[i] = txbuf[i];
		}
	}

	return ret;
}
