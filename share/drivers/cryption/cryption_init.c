#include "includes.h"

t_crp_config * crp_cfg;
void crp_init(t_crp_config crp_config)
{
	SC_RESET2_RELEASE(CRYPTION);
	SC_RRESET1_RELEASE(CRYPTION);

	crp_cfg = &crp_config;
}

static int crp_irq_handler(void * arg)
{
	int reg_int = ReadReg32(REG_CRP_INT);

//	debug_printf("int:0x%x\n",tmp);
	if(reg_int&BIT8)
	{
		if(crp_cfg->cb_irq_handler)
		{
			crp_cfg->cb_irq_handler(crp_cfg->irq_arg);
		}
	
		if(crp_cfg->crp_evt)
		{
			ertos_event_signal(crp_cfg->crp_evt);
		}
		WriteReg32(REG_CRP_INT,ReadReg32(REG_CRP_INT)|BIT8);	//clear status	
	}
	
	dc_invalidate_range((u32)crp_cfg->data_addr+MEMBASE_DDR,(u32)crp_cfg->data_addr+MEMBASE_DDR+(crp_cfg->data_length<<3));
	return 0;
}

int enable_crp_irq(void)
{
	if(crp_cfg->crp_evt == NULL)
	{
		crp_cfg->crp_evt = ertos_event_create();
		if(crp_cfg->crp_evt == NULL)
		{
			debug_printf("create crytption event failed!!!\n");
			return -1;
		}
	}

	int ret = irq_request(IRQ_BIT_CRYPT,crp_irq_handler,"CRP_IRQ",crp_cfg->irq_arg);
	return ret;
}

void disable_crp_irq(void)
{
	irq_free(IRQ_BIT_CRYPT);
}


