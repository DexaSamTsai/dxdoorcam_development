#include "includes.h"

int crp_config(t_crp_config * p_crp_cfg)
{	
	int i=0;
	unsigned char reg0=0,reg1=0;
	char key_lenth =3;
	if(p_crp_cfg->alg ==AES)
	{
		switch(p_crp_cfg->key_bits)
		{
			case 128:
				key_lenth = 0;
				break;
			case 192:
				key_lenth = 1;
				break;
			case 256:
				key_lenth = 2;
				break;
			default:
				key_lenth  = 3;
				syslog(LOG_ERR, "ERROR:unsupported AES key length!!!");
				break;
		}
	}
	if(p_crp_cfg->alg ==DES)
	{
		p_crp_cfg->key_bits = 64;
		key_lenth = 0;
	}
	if(p_crp_cfg->alg == DES3_KEY2){
		p_crp_cfg->key_bits = 128;
		key_lenth = 1;
	}
	if(p_crp_cfg->alg == DES3_KEY3)
	{
		p_crp_cfg->key_bits = 192;
		key_lenth = 2;
	}
	if(p_crp_cfg->alg != AES)
	{
		reg0 = ((key_lenth &0x3)<<6) + ((p_crp_cfg->mode &0x07)<<3) +p_crp_cfg->crp_dir;
	}
	else
	{
		reg0 = ((key_lenth &0x3)<<6) + ((p_crp_cfg->mode &0x07)<<3) + (0x01<<1) +p_crp_cfg->crp_dir;
	}
	
	reg1 =0x30;
	if(p_crp_cfg->mode == CFB || p_crp_cfg->mode ==OFB)
	{
		switch(p_crp_cfg->fb_seglen)
		{
			case 1:
				reg1 = 0;
				break;
			case 8:
				reg1 = 0x10;
				break;
			case 64:
			case 128:
				reg1 = 0x20;
				break;
			default:
				reg1 = 0x30;
				syslog(LOG_ERR, "ERROR:unsupported CFB/OFB segment length!\n");
				break;
		}
	}
	WriteReg32(REG_CRP_CONFIG,reg0 | (reg1<<8)|BIT18);
	WriteReg32(REG_CRP_START_ADDR, (u32)p_crp_cfg->data_addr);	//output data will be saved at the same address of input
	WriteReg32(REG_CRP_LENGTH,p_crp_cfg->data_length);		//in the test it is found that the output address is the same with the input address
	
	//write key
	int key_bytes = p_crp_cfg->key_bits/8;
	if(key_bytes == 0)
	{
		syslog(LOG_ERR, "ERROR:key length should not be 0!!!\n");
		return -1;
	}
	for(i=0;i<key_bytes;i++)
	{
		WriteReg8(REG_CRP_KEY0+i,p_crp_cfg->key_addr[i]);
	}
	WriteReg32(REG_CRP_CONFIG,ReadReg32(REG_CRP_CONFIG)|BIT17);  //load AES key?without this line,can not finish crpt
	WriteReg32(REG_CRP_INT,ReadReg32(REG_CRP_INT)|BIT0);	//enable interrupt	
	
	dc_flush_range((u32)p_crp_cfg->data_addr+MEMBASE_DDR,(u32)p_crp_cfg->data_addr+MEMBASE_DDR+(p_crp_cfg->data_length<<3));	

	return 0;
}

void crp_start(void)
{
	//kick start
	WriteReg32(REG_CRP_CONFIG,ReadReg32(REG_CRP_CONFIG)|BIT16);
}
