#include "includes.h"

#define  USB_DESCRIPTOR_TYPE_DEVICE                     1
#define  USB_DESCRIPTOR_TYPE_CONFIGURATION              2
#define  USB_DESCRIPTOR_TYPE_INTERFACE                  4
#define  USB_DESCRIPTOR_TYPE_ENDPOINT                   5

#define  ERR_MS_CMD_FAILED       10
#define  ERR_BAD_CONFIGURATION   11
#define  ERR_NO_MS_INTERFACE     12
#define  ERR_MS_NO_READY     13

char local_buffer[1024];
u32 MS_BlkSize;
u32 MS_BlkCount;
int usb_control_transfer(unsigned char dir, unsigned char request_type, unsigned char request,  unsigned short value, unsigned short index, void *buf, int size);
static u32 tag_cnt = 0;

u32 umass_app_init(void)
{
	u32  retry, rc;

	if(rc = MS_GetMaxLUN()){                                                    /* Get maximum logical unit number   */
		debug_printf("MS_GetMaxLUN error,do not support multiple lun\n");
		return rc;
	}
	
	retry = 100;
	while(--retry) {
		rc = MS_TestUnitReady();                                       /* Test whether the unit is ready    */
		if (rc == 0) {
			break;
		}
		MS_GetSenseInfo();                                             /* Get sense information             */
		{volatile int delay; for(delay=0;delay<10000;delay++); } 
	}

	rc = MS_ReadCapacity();                                            /* Read capacity of the disk         */
	if (rc != 0) {
		debug_printf("umass read capacity fail\n");
	}

	return rc;
}

static void fill_word_by_char(u8 *buf, u32 data)
{
	buf[3] = (u8)((data&0xff000000)>>24);
	buf[2] = (u8)((data&0xff0000)>>16);
	buf[1] = (u8)((data&0xff00)>>8);
	buf[0] = (u8)(data&0xff);
}

static void fill_short_by_char(u8 *buf, u16 data)
{
	buf[1] = (u8)((data&0xff00)>>8);
	buf[0] = (u8)((data&0xff));
}

static u32 swap_word(u32 input)
{
	u32 temp;

	temp = ((input&0xFF)<<24)|((input&0xFF00)<<8)|((input&0xFF0000)>>8)|((input&0xFF000000)>>24);
	return temp;
}

static u16 swap_short(u16 input)
{
	u16 temp;

	temp = ((input&0xFF)<<8)|((input&0xFF00)>>8);
	return temp;
}

/*
**************************************************************************************************************
*                                         FILL MASS STORAGE COMMAND
*
* Description: This function is used to fill the mass storage command
*
* Arguments  : None
*
* Returns    : OK		              if Success
*              ERR_INVALID_BOOTSIG    if Failed
*
**************************************************************************************************************
*/
static void  fill_scsi_command (u8 *buffer,
				u32   block_number,
				u32   block_size,
				u16   num_blocks,
				MS_DATA_DIR  direction,
				u8   scsi_cmd,
				u8   scsi_cmd_len)
{
	u32 data_len, i;
		
	for (i = 0; i < CBW_SIZE; i++) {
		buffer[i] = 0;
	}

	switch(scsi_cmd) {
		case SCSI_CMD_TEST_UNIT_READY:
			data_len = 0;
			break;
		case SCSI_CMD_READ_CAPACITY:
			data_len = 8;
			break;
		case SCSI_CMD_REQUEST_SENSE:
			data_len = 18;
			break;
		default:
			data_len = block_size*num_blocks;
			break;
	}
	
	fill_word_by_char(buffer, CBW_SIGNATURE);
	tag_cnt++;
	fill_word_by_char(buffer+4, tag_cnt);
	fill_word_by_char(buffer+8, data_len);
    
	buffer[12] = (u8)((direction == MS_DATA_DIR_NONE) ? 0 : direction);
	buffer[14] = scsi_cmd_len;                                   /* Length of the CBW                 */
	buffer[15] = scsi_cmd;
	if (scsi_cmd == SCSI_CMD_REQUEST_SENSE) {
		buffer[19] = (u8)data_len;
	} else {
		fill_word_by_char(buffer+17, swap_word(block_number));
    }
	fill_short_by_char(buffer +22, swap_short(num_blocks));

#if 0
	for (i = 0; i < CBW_SIZE; i++) {
		debug_printf("%x  ", buffer[i]);
	}
	debug_printf("\nfill command\n");
#endif
}

/*
**************************************************************************************************************
*                                         GET MAXIMUM LOGICAL UNIT
*
* Description: This function returns the maximum logical unit from the device
*
* Arguments  : None
*
* Returns    : OK		              if Success
*              ERR_INVALID_BOOTSIG    if Failed
*
**************************************************************************************************************
*/

u32 MS_GetMaxLUN(void)
{
	char buffer[8];
	int ret;

	debug_printf("get max lun\n");
	ret = usb_control_transfer(0x80,
						0xa1,
						MS_GET_MAX_LUN_REQ,
						0x0,
						0x0,
						buffer,
						0x1);
	if(ret == -1)  {
		return 1; 
	}

	return 0;
}

#define MS_MAX_TRANS_H 512
#define MS_MAX_TRANS_F  64

int send_bulk_trans(u8 *buffer, int size);
int recv_bulk_trans(u8 *buffer, int size);

int ms_bulk_transfer(u32 type, u8* buffer, u32 trans_size)
{
	int ret, i, size, off;
	int max_trans_size, length;
	struct usb_device *udev = &g_libhost_cfg.udev;
	
	if(udev->speed == USB_SPEED_HIGH) {
		max_trans_size = MS_MAX_TRANS_H;
	}else{
		max_trans_size = MS_MAX_TRANS_F;
	}

	debug_printf("Bulk Transfer...(%x)\n", trans_size);
	off = 0;
	length = trans_size;
	for(i=0; length>0; i += max_trans_size){
		if(trans_size < max_trans_size){
			size = trans_size;
		}
		else {
			size = max_trans_size;
		}
		if(type == 0x80){
			ret = recv_bulk_trans(buffer+off, size);
		}else {
			ret = send_bulk_trans(buffer+off, size);
		}
		length -= ret;
		off += ret;
	}
	return ret;
}

/*
**************************************************************************************************************
*                                          GET SENSE INFORMATION
*
* Description: This function is used to get sense information from the device
*
* Arguments  : None
*
* Returns    : OK       if Success
*              ERROR    if Failed
*
**************************************************************************************************************
*/

u32 MS_GetSenseInfo(void)
{
	u8* buffer;
	int ret, i;

	debug_printf("MS_GetSenseInfo\n");
	buffer = local_buffer;
	fill_scsi_command(buffer, 0, 0, 0, MS_DATA_DIR_IN, SCSI_CMD_REQUEST_SENSE, 6);
	
	ret = ms_bulk_transfer(0x00, buffer, CBW_SIZE);
	if(ret == -1) {
		debug_printf("CBW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80,buffer,18);
	if(ret == -1) {
		debug_printf("transfer err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80,buffer,CSW_SIZE);
	if(ret == -1) {
		debug_printf("CSW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = 0;
	if(buffer[12]){
		debug_printf("get sense info fail, status %x \n", buffer[12]);
		for (i = 0; i < CSW_SIZE; i++) {
			debug_printf("%x  ",buffer[i]);
		}
		debug_printf("\n");
		ret = ERR_MS_NO_READY;
	}

	return ret;
}

/*
**************************************************************************************************************
*                                           TEST UNIT READY
*
* Description: This function is used to test whether the unit is ready or not
*
* Arguments  : None
*
* Returns    : OK       if Success
*              ERROR    if Failed
*
**************************************************************************************************************
*/

u32 MS_TestUnitReady (void)
{
	u8* buffer;
	int ret, i;

	debug_printf("MS_TestUnitReady\n");
	buffer = local_buffer;
    fill_scsi_command(buffer, 0, 0, 0, MS_DATA_DIR_NONE, SCSI_CMD_TEST_UNIT_READY, 6);
	
	ret = ms_bulk_transfer(0x00, buffer, CBW_SIZE);
	if(ret == -1) {
		debug_printf("CBW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80, buffer, CSW_SIZE);
	if(ret == -1) {
		debug_printf("CSW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = 0;
	if(buffer[12]){
		debug_printf("test ready fail, status %x \n", buffer[12]);
		for (i = 0; i < CSW_SIZE; i++) {
			debug_printf("%x  ",buffer[i]);
		}
		debug_printf("\n");
		ret = ERR_MS_NO_READY;
	}

    return ret;
}

/*
**************************************************************************************************************
*                                            READ CAPACITY
*
* Description: This function is used to read the capacity of the mass storage device
*
* Arguments  : None
*
* Returns    : OK       if Success
*              ERROR    if Failed
*
**************************************************************************************************************
*/

u32 MS_ReadCapacity (void)
{
	u8* buffer;
    int ret, i;
	u32 block_num;

	debug_printf("MS_ReadCapacity\n");
	buffer = local_buffer;
    fill_scsi_command(buffer, 0, 0, 0, MS_DATA_DIR_IN, SCSI_CMD_READ_CAPACITY, 10);
	
	ret = ms_bulk_transfer(0x00, buffer, CBW_SIZE);
	if(ret == -1) {
		debug_printf("CBW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80, buffer, 8);
	if(ret == -1) {
		debug_printf("transfer err\n");
		return ERR_MS_CMD_FAILED;
	}

	for(i=0;i<8;i++){
		debug_printf("0x%x ",buffer[i]);
	}

    MS_BlkSize = (buffer[4]<<24)+(buffer[5]<<16)+(buffer[6]<<8)+buffer[7];
    block_num = (buffer[0]<<24)+(buffer[1]<<16)+(buffer[2]<<8)+buffer[3];
	block_num = block_num + 1;
	MS_BlkCount = block_num;
	debug_printf("block size is 0x%x, block num is 0x%x\n",MS_BlkSize,block_num);

	ret = ms_bulk_transfer(0x80, buffer, CSW_SIZE);
	if(ret == -1) {
		debug_printf("CSW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	if(buffer[12]){
		ret = ERR_MS_CMD_FAILED;
	}
	else{
		ret = 0;
	}
    return ret;
}

u32 MS_bulk_read(u32 block_number, u16 num_blocks, u8 *user_buffer)
{
	int ret = 0;
	u8 *buffer;

	debug_printf("-------------bulk read(%x %x)-------------\n", block_number, num_blocks);

	buffer = local_buffer;
	fill_scsi_command(buffer, block_number, MS_BlkSize, num_blocks, MS_DATA_DIR_IN, SCSI_CMD_READ_10, 10);

	ret = ms_bulk_transfer(0x00, buffer, CBW_SIZE);
	if(ret == -1) {
		debug_printf("CBW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80, (u8 *)user_buffer, MS_BlkSize*num_blocks);
	if(ret == -1) {
		debug_printf("transfer err\n");
		return ERR_MS_CMD_FAILED;
	}

#if 0
	buffer = (u8 *)user_buffer;
	for(i=0;i<512;i++)
	{
		if((i%0x20)==0)
				debug_printf("\n");
		debug_printf("0x%x ",buffer[i]);
	}
	debug_printf("\n");
#endif

	ret = ms_bulk_transfer(0x80, buffer,CSW_SIZE);
	if(ret == -1) {
		debug_printf("CSW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	if(buffer[12]){
		ret = ERR_MS_CMD_FAILED;
	}else{
		ret = 0;
	}

    return ret;
}

u32 MS_bulk_write(u32 block_number, u16 num_blocks, u8 *user_buffer)
{
	int ret = 0;
	u8 *buffer;

	debug_printf("-------------bulk write(%x %x)-------------\n", block_number, num_blocks);

	buffer = local_buffer;
	fill_scsi_command(buffer, block_number, MS_BlkSize, num_blocks, MS_DATA_DIR_OUT, SCSI_CMD_WRITE_10, 10);

	ret = ms_bulk_transfer(0x00, buffer, CBW_SIZE);
	if(ret == -1) {
		debug_printf("CBW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x00,(u8 *)user_buffer,MS_BlkSize*num_blocks);
	if(ret == -1) {
		debug_printf("transfer err\n");
		return ERR_MS_CMD_FAILED;
	}

	ret = ms_bulk_transfer(0x80, buffer, CSW_SIZE);
	if(ret == -1) {
		debug_printf("CSW status err\n");
		return ERR_MS_CMD_FAILED;
	}

	if(buffer[12]){
		ret = ERR_MS_CMD_FAILED;
	}else{
		ret = 0;
	}

	return ret;
}

