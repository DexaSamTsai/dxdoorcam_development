#include "includes.h"

int usb_control_transfer(unsigned char dir, unsigned char request_type, unsigned char request,  unsigned short value, unsigned short index, void *buf, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;
	int pipe, ret;

	pipe = dir ? usb_rcvctrlpipe(udev, 0) : usb_sndctrlpipe(udev, 0);

	ret = usb_control_msg(udev,
						pipe,
						request_type,
						request,
						value,
						index,
						buf,
						size);

	return ret;
}

static int usb_bulk_transfer(struct usb_device *udev, int dir, char *buffer, int size )
{
	int ret;
	u8 ep_in, ep_out;
	u16 ep_in_mps, ep_out_mps;
	struct usb_host_endpoint *host_ep_in, *host_ep_out;
	
	debug_printf("usb bulk transfer %x %x...\n", size, dir);

	if(dir) {
		ep_in = usb_get_ep(udev, USB_ENDPOINT_XFER_BULK, USB_DIR_IN) | USB_DIR_IN;
		if(!ep_in) {
			debug_printf("usb device don't exist bulk-in endpoint.\n");
			return 0;
		}
		host_ep_in = usb_pipe_endpoint(udev, usb_sndbulkpipe(udev, ep_in));
		if(!host_ep_in) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_in);
			return 0;
		}
		ep_in_mps = host_ep_in->ep_mps; 
		ret = usb_bulk_read(udev, buffer, size);
		if(ret < 0) {
			debug_printf("bulk read fail.\n");
			while(1);
		}
	}else {
		ep_out = usb_get_ep(udev, USB_ENDPOINT_XFER_BULK, USB_DIR_OUT);
		if(!ep_out) {
			debug_printf("usb device don't exist bulk-out endpoint.\n");
			return 0;
		}
		host_ep_out = usb_pipe_endpoint(udev, usb_rcvbulkpipe(udev, ep_out));
		if(!host_ep_out) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_out);
			return 0;
		}
		ep_out_mps = host_ep_out->ep_mps; 
		ret = usb_bulk_write(udev, buffer, size);
		if(ret < 0) {
			debug_printf("bulk write fail.\n");
			while(1);
		}
	}
	return ret;
}

int send_bulk_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_bulk_transfer(udev, 0x00, (char *)buffer, size);
}

int recv_bulk_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_bulk_transfer(udev, 0x80, (char *)buffer, size);
}

static int usb_intr_transfer(struct usb_device *udev, int dir, char *buffer, int size )
{
	int ret;
	u8 ep_in, ep_out;
	u16 ep_in_mps, ep_out_mps;
	u8 mult_size;
	struct usb_host_endpoint *host_ep_in, *host_ep_out;

	debug_printf("interrupt transfer %x %x...\n", size, dir);

	if(dir) {
		ep_in = usb_get_ep(udev, USB_ENDPOINT_XFER_INT, USB_DIR_IN) | USB_DIR_IN;
		if(!ep_in) {
			debug_printf("usb device don't exist interrupt-in endpoint.\n");
			return 0;
		}

		host_ep_in = usb_pipe_endpoint(udev, usb_sndintpipe(udev, ep_in));
		if(!host_ep_in) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_in);
			return 0;
		}
		ep_in_mps = host_ep_in->ep_mps; 
	}else {

		ep_out = usb_get_ep(udev, USB_ENDPOINT_XFER_INT, USB_DIR_OUT);
		if(!ep_out) {
			debug_printf("usb device don't exist interrupt-out endpoint.\n");
			return 0;
		}
		host_ep_out = usb_pipe_endpoint(udev, usb_rcvintpipe(udev, ep_out));
		if(!host_ep_out) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_out);
			return 0;
		}
		ep_out_mps = host_ep_out->ep_mps; 
	}

	if(le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize) != le16_to_cpu(host_ep_out->ep_desc.wMaxPacketSize)) {
		debug_printf("device in-ep(mps=0x%x) and out-ep(mps=0x%x) descriptor maxpacketsize is different.", le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize), le16_to_cpu(host_ep_out->ep_desc.wMaxPacketSize));
		return 0;
	}
	mult_size = ((le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize) & 0x1800) >> 11) + 1;

	if(dir) {
		ret = usb_rw(udev, usb_sndintpipe(udev, ep_out), buffer, size); 
		if(ret < 0) {
			debug_printf("interrupt write fail.\n");
			while(1);
		}
	}else {
		ret = usb_rw(udev, usb_rcvintpipe(udev, ep_in), buffer, size);
		if(ret < 0) {
			debug_printf("interrupt read fail.\n");
			while(1);
		}
	}

	debug_printf("\ninterrupt transfer done......\n");
	return ret;
}

int send_intr_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_intr_transfer(udev, 0x00, buffer, size);
}

int recv_intr_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_intr_transfer(udev, 0x80, buffer, size);
}

static int usb_iso_transfer(struct usb_device *udev, int dir, char *buffer, int size )
{
	int ret;
	u8 ep_in, ep_out;
	u16 ep_in_mps, ep_out_mps;
	u8 mult_size;
	struct usb_host_endpoint *host_ep_in, *host_ep_out;

	debug_printf("ISO transfer %x %x...\n", size, dir);

	if(dir) {
		ep_in = usb_get_ep(udev, USB_ENDPOINT_XFER_ISOC, USB_DIR_IN) | USB_DIR_IN;
		if(!ep_in) {
			debug_printf("usb device don't exist iso-in endpoint.\n");
			return 0;
		}

		host_ep_in = usb_pipe_endpoint(udev, usb_sndisocpipe(udev, ep_in));
		if(!host_ep_in) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_in);
			return 0;
		}
		ep_in_mps = host_ep_in->ep_mps; 
	}else {
		ep_out = usb_get_ep(udev, USB_ENDPOINT_XFER_ISOC, USB_DIR_OUT);
		if(!ep_out) {
			debug_printf("usb device don't exist iso-out endpoint.\n");
			return 0;
		}

		host_ep_out = usb_pipe_endpoint(udev, usb_rcvisocpipe(udev, ep_out));
		if(!host_ep_out) {
			debug_printf("cannot get host endpoint struct, NULL pointer. ep_num:0x%x\n", ep_out);
			return 0;
		}
		ep_out_mps = host_ep_out->ep_mps; 
	}

	if(le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize) != le16_to_cpu(host_ep_out->ep_desc.wMaxPacketSize)) {
		debug_printf("device in-ep(mps=0x%x) and out-ep(mps=0x%x) descriptor maxpacketsize is different.", le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize), le16_to_cpu(host_ep_out->ep_desc.wMaxPacketSize));
		return 0;
	}
	mult_size = ((le16_to_cpu(host_ep_in->ep_desc.wMaxPacketSize) & 0x1800) >> 11) + 1;

	if(dir) {
		ret = usb_rw(udev, usb_sndisocpipe(udev, ep_out), buffer, size); 
		if(ret < 0) {
			debug_printf("iso write fail.\n");
			while(1);
		}
	}else {
		ret = usb_rw(udev, usb_rcvisocpipe(udev, ep_in), buffer, size);
		if(ret < 0) {
			debug_printf("iso read fail.\n");
			while(1);
		}
	}

	debug_printf("\niso transfer done......\n");
	return ret;
}

int send_iso_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_iso_transfer(udev, 0x00, buffer, size);
}

int recv_iso_trans(u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	return usb_iso_transfer(udev, 0x80, buffer, size);
}

void usb_tranfser(u8 ep_type, u8 dir, u8 *buffer, int size)
{
	struct usb_device *udev = &g_libhost_cfg.udev;

	switch(ep_type) {
		//case USB_ENDPOINT_XFER_CONTROL:
		//	usb_control_transfer(udev, dir, buffer, size);
		//	break;
		case USB_ENDPOINT_XFER_BULK:
			usb_bulk_transfer(udev, dir, buffer, size);
			break;
		case USB_ENDPOINT_XFER_INT:
			usb_intr_transfer(udev, dir, buffer, size);
			break;
		case USB_ENDPOINT_XFER_ISOC:
			usb_iso_transfer(udev, dir, buffer, size);
			break;
		default:
			debug_printf("usb transfer, unknown ep_type.\n");
			break;
	}
}

