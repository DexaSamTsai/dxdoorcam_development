#include "includes.h"

int usb_driver_register(struct usb_driver *udrv)
{
	g_libhost_cfg.udrv = udrv;

	debug_printf("%s driver register ok!\n", udrv->name);
	return 0;
}

int hcd_driver_register(struct hc_driver *hcdrv)
{
	if(hcdrv->speed == HCD_USB20) {
		g_libhost_cfg.ehci_driver = hcdrv;
	}else if(hcdrv->speed == HCD_USB11) {
		g_libhost_cfg.ohci_driver = hcdrv;
	}else {
		debug_printf("unknown hcd type, hcd speed=%d\n", hcdrv->speed);
		return -1;
	}

	debug_printf("%s driver register ok!\n", hcdrv->hcd_name);
	return 0;
}

static int usb_match_device(struct usb_device *udev, const struct usb_device_id *id)
{
	debug_printf("idVendor: 0x%x - 0x%x\n", id->idVendor, le16_to_cpu(udev->device_desc.idVendor));
	debug_printf("idProduct: 0x%x - 0x%x\n", id->idProduct, le16_to_cpu(udev->device_desc.idProduct));
	if((id->match_flags & USB_DEVICE_ID_MATCH_VENDOR) && (id->idVendor != le16_to_cpu(udev->device_desc.idVendor))) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_PRODUCT) && (id->idProduct != le16_to_cpu(udev->device_desc.idProduct))) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_DEV_LO) && (id->bcdDevice_lo > le16_to_cpu(udev->device_desc.bcdDevice))) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_DEV_HI) && (id->bcdDevice_hi < le16_to_cpu(udev->device_desc.bcdDevice))) {
		return 0;
	}
	
	if((id->match_flags & USB_DEVICE_ID_MATCH_DEV_CLASS) && (id->bDeviceClass != le16_to_cpu(udev->device_desc.bDeviceClass))) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_DEV_SUBCLASS) && (id->bDeviceSubClass != le16_to_cpu(udev->device_desc.bDeviceSubClass))) {
		return 0;
	}
	
	if((id->match_flags & USB_DEVICE_ID_MATCH_DEV_PROTOCOL) && (id->bDeviceProtocol != le16_to_cpu(udev->device_desc.bDeviceProtocol))) {
		return 0;
	}

	return 1;
}

static int usb_match_one_id(struct usb_device *udev, struct usb_host_interface *interface, const struct usb_device_id *id)
{
	if(id == NULL) {
		debug_printf("usb device id is NULL!\n");
		return NULL;
	}

	if(!usb_match_device(udev, id)) {
		debug_printf("usb not match device. match_flags=0x%x\n", id->match_flags);
		return 0;
	}
	
	if((id->match_flags & USB_DEVICE_ID_MATCH_INT_CLASS) && (id->bInterfaceClass != interface->intf_desc.bInterfaceClass)) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_INT_SUBCLASS) && (id->bInterfaceSubClass != interface->intf_desc.bInterfaceSubClass)) {
		return 0;
	}

	if((id->match_flags & USB_DEVICE_ID_MATCH_INT_PROTOCOL) && (id->bInterfaceProtocol != interface->intf_desc.bInterfaceProtocol)) {
		return 0;
	}

	return 1;
}

const struct usb_device_id *usb_match_id(struct usb_device *udev, struct usb_host_interface *interface, const struct usb_device_id *id)
{
	if(id == NULL) {
		debug_printf("usb device id array is NULL!\n");
		return NULL;
	}

	for(; id->idVendor || id->idProduct || id->bDeviceClass || id->bInterfaceClass; id++) {
		if(usb_match_one_id(udev, interface, id)) {
			return id;
		}
	}

	return NULL;
}


