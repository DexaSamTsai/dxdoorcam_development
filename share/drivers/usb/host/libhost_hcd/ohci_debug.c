#include "includes.h"

#define ohci_debug	debug_printf

void ohci_ed_debug(t_ohci_ed_hdl ed)
{
	ohci_debug("-------------ED--------------\n");
	ohci_debug("ed addr: 0x%x\n", ed);
	ohci_debug("ed info: 0x%x\n", ed->hwINFO);
	ohci_debug("ed tailP: 0x%x\n", ed->hwTailP);
	ohci_debug("ed headP: 0x%x\n", ed->hwHeadP);
	ohci_debug("ed nextED: 0x%x\n", ed->hwNextED);
	ohci_debug("=============================\n");
}

void ohci_td_debug(t_ohci_td_hdl td)
{
	ohci_debug("-------------TD--------------\n");
	ohci_debug("td addr: 0x%x\n", td);
	ohci_debug("td info: 0x%x\n", td->hwINFO);
	ohci_debug("td CBP: 0x%x\n", td->hwCBP);
	ohci_debug("td nextTD: 0x%x\n", td->hwNextTD);
	ohci_debug("td BE Ptr: 0x%x\n", td->hwBE);
	ohci_debug("=============================\n");
}

void ohci_itd_debug(t_ohci_itd_hdl itd)
{
	ohci_debug("-------------ITD--------------\n");
	ohci_debug("itd addr: 0x%x\n", itd);
	ohci_debug("itd info: 0x%x\n", itd->hwINFO);
	ohci_debug("itd BP0: 0x%x\n", itd->hwBP0);
	ohci_debug("itd nextTD: 0x%x\n", itd->hwNextTD);
	ohci_debug("itd BE Ptr: 0x%x\n", itd->hwBE);
	ohci_debug("itd Off[0]: 0x%x\n", itd->hwPSW[0]);
	ohci_debug("itd Off[1]: 0x%x\n", itd->hwPSW[1]);
	ohci_debug("itd Off[2]: 0x%x\n", itd->hwPSW[2]);
	ohci_debug("itd Off[3]: 0x%x\n", itd->hwPSW[3]);
	ohci_debug("=============================\n");
}

