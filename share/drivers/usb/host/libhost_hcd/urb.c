#include "includes.h"

extern int urb_dma_unmapping(struct urb *urb);

#define MAX_URB_CNT		8	
static u8 __attribute__ ((aligned(4))) urb_pool_buf[sizeof(struct urb) * MAX_URB_CNT + 4];
static u8 urb_index=0;

void usb_urb_pool_init(void)
{
	u8 i;
	struct urb *urb;

	for(i=0; i<MAX_URB_CNT; i++) {
		urb = (struct urb *)(&urb_pool_buf) + i;
		urb->urb_status = URB_STATUS_IDLE;
	}

	urb_index = 0;
}

struct urb *usb_alloc_urb(void)
{
	struct urb *urb;
	u8 i;

	for(i=0; i<MAX_URB_CNT; i++) {
		if(urb_index >= MAX_URB_CNT) {
			urb_index = 0;
		}

		urb = (struct urb *)(&urb_pool_buf) + urb_index;
		memset(urb, 0x00, sizeof(struct urb));
		urb_index++;
		if(urb->urb_status == URB_STATUS_IDLE) {
			return urb;
		}
	}

	if(i == MAX_URB_CNT) {
		debug_printf("urb alloc fail, urb total num=%d. maybe you need increase urb counts!\n", i);
	}

	return NULL;
}

void usb_free_urb(struct urb *urb)
{
	if(urb) {
		urb->urb_status = URB_STATUS_IDLE;
	}
}

void urb_common_complete_blocking(struct urb *urb)
{
	urb_dma_unmapping(urb);

	urb->urb_status = URB_STATUS_COMPLETE;
}

static void usb_delay_1ms(void) {
	{volatile u32 dd; for(dd=0; dd<5000; dd++);}
}

//timeout == 0 is infinite loop
int wait_urb_complete_timeout(struct urb *urb, u32 timeout)
{
	u32 i = 0;
	int retv = 0;

	while(urb->urb_status != URB_STATUS_COMPLETE) {
		if(timeout) {//TODO maybe be replaced by tick in the future
			usb_delay_1ms();
			if(i++ > timeout) {
				debug_printf("urb status:%d\n", urb->urb_status);
				debug_printf("ehci status:0x%x\n", ReadReg32(EHCI_REG_STATUS));
				retv =  -ETIMEDOUT; 
				break;
			}
		}
	}

	if(urb->urb_status == URB_STATUS_COMPLETE) {
		retv = urb->actual_length;
		debug_printf("urb actual_length:0x%x\n", urb->actual_length);
	}

	usb_free_urb(urb);

	return retv;
}

int urb_dma_mapping(struct urb *urb)
{
	struct hc_driver *hcdrv;

	if(urb->udev->speed == USB_SPEED_HIGH) {
		hcdrv = g_libhost_cfg.ehci_driver;
	}else {
		hcdrv = g_libhost_cfg.ohci_driver;
	}

	if(usb_pipecontrol(urb->pipe)) {
		urb->setup_dma = hcdrv->dma_alloc(sizeof(struct usb_ctrlrequest));
		if(!urb->setup_dma) {
			debug_printf("setup dma alloc fail.\n");
			return -ENOMEM;
		}

		memcpy((u8 *)urb->setup_dma, urb->setup_packet, sizeof(struct usb_ctrlrequest));
	}

	if(urb->transfer_buffer_length) {
		urb->transfer_dma = hcdrv->dma_alloc(urb->transfer_buffer_length);
		if(!urb->transfer_dma) {
			debug_printf("urb transfer buffer dma alloc fail.\n");
			if(usb_pipecontrol(urb->pipe)) {
				hcdrv->dma_free(urb->setup_dma);
			}
			return -ENOMEM;
		}

		if(usb_pipeout(urb->pipe)) {
			memcpy((u8 *)urb->transfer_dma, urb->transfer_buffer, urb->transfer_buffer_length);
		}
	}

	return 0;
}

int urb_dma_unmapping(struct urb *urb)
{
	struct hc_driver *hcdrv;

	if(urb->udev->speed == USB_SPEED_HIGH) {
		hcdrv = g_libhost_cfg.ehci_driver;
	}else {
		hcdrv = g_libhost_cfg.ohci_driver;
	}

	if(urb->setup_dma) {
		hcdrv->dma_free(urb->setup_dma);
	}

	if(urb->transfer_dma) {
		if(usb_pipein(urb->pipe)) {
			memcpy(urb->transfer_buffer, (u8 *)urb->transfer_dma, urb->actual_length);
			//add by jet, flush data to buffer
			dc_flush_range(urb->transfer_buffer, urb->transfer_buffer+((urb->actual_length+0xf)&0xfffffff0));
		}
		hcdrv->dma_free(urb->transfer_dma);
	}

	return 0;
}

int usb_submit_urb(struct urb *urb)
{
	int ret;
	struct usb_host_endpoint *ep;

	struct hc_driver *hcdrv;

	if(urb->udev->speed == USB_SPEED_HIGH) {
		hcdrv = g_libhost_cfg.ehci_driver;
	}else {
		hcdrv = g_libhost_cfg.ohci_driver;
	}

	if(!urb) {
		debug_printf("urb is NULL!\n");
		return -ENOMEM;
	}

	ep = usb_pipe_endpoint(urb->udev, urb->pipe);
	if(!ep) {
		debug_printf("don't get ep of pipe.\n");
		return -EEXIST;
	}

	urb->ep = ep;
	urb->actual_length = 0;
	urb->urb_status = URB_STATUS_WORKING;

	if(is_hub_control(urb)) {
		if(hcdrv->hub_control) {
			ret = hcdrv->hub_control(urb);
		}else {
			debug_printf("hc driver hasn't define hub_control!\n");
			return -EEXIST;
		}
	}else {
		ret = urb_dma_mapping(urb);
		if(ret) {
			debug_printf("urb dma mapping fail.\n");
			return ret;
		}

		if(hcdrv->urb_enqueue) {
			ret = hcdrv->urb_enqueue(urb);
		}else {
			debug_printf("hc driver hasn't define urb_enqueue!\n");
			return -EEXIST;
		}
	}

	return ret;
}

