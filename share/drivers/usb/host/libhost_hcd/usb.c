#include "includes.h"

static void host_dump_data(unsigned char *pbuf, int cnt)
{
	  int i;

	  debug_printf("Dump Data(0x%x): %x\n", pbuf, cnt);
	  for(i=0; i<cnt; i++)
      {
	       debug_printf(" %x", *(pbuf+i));
	       if((i & 0xf) == 0xf)
	          debug_printf("\n");
	  }
	  debug_printf("\n");
}

//return '0' if enumeration ok
int usb_enumerate_device(unsigned short dev_addr)
{
	struct usb_device *udev = &g_libhost_cfg.udev;
	u8 buffer[USB_DT_DEVICE_SIZE];
	unsigned short size;
	int ret;

	debug_printf("host enumerate %s device begin ......\n", ((udev->speed == USB_SPEED_HIGH) ? "high" : "full"));

	if(dev_addr > 0x80) {
		debug_printf("device addr(0x%x) can't bigger than 0x80.\n", dev_addr);
		return -1;
	}

	memset(buffer, 0, sizeof(buffer));
	size = USB_DT_DEVICE_SIZE;
	ret = usb_get_device_descriptor(udev, buffer, size);
	if(ret < 0) {
		debug_printf("get device descriptor fail. addr=%d, ret=%d\n", udev->addr, ret);
		goto failtag;
	}
	host_dump_data(buffer, ret);

	//set device address
	ret = usb_set_address(udev, dev_addr);
	if(ret < 0) {
		debug_printf("get device descriptor fail. ret=%d\n", ret);
		goto failtag;
	}

	//get device descriptor for new address dev_addr
	memset(buffer, 0, sizeof(buffer));
	size = USB_DT_DEVICE_SIZE;
	ret = usb_get_device_descriptor(udev, buffer, size);
	if(ret < 0) {
		debug_printf("get device descriptor fail. addr=%d, ret=%d\n", udev->addr, ret);
		goto failtag;
	}
	memcpy(&udev->device_desc, buffer, size);
	debug_printf("device descriptor content:\n");
	host_dump_data(buffer, ret);

	//get config descriptor
	ret = usb_get_config(udev);
	if(ret<0) {
		debug_printf("get config fail. ret=%d\n", ret);
		goto failtag;
	}

#ifdef CONFIG_USB_OTG_EN
	//get otg descriptor
	memset(buffer, 0, sizeof(buffer));
	size = USB_DT_OTG_SIZE;
	ret = usb_get_otg_descriptor(udev, buffer, size); 
	if(ret < 0) {
		debug_printf("get otg descriptor fail. addr=%d, ret=%d\n", udev->addr, ret);
		goto failtag;
	}
	memcpy(&udev->otg_desc, buffer, size);
	debug_printf("otg descriptor content:\n");

	if(udev->otg_desc.bmAttributes & USB_OTG_HNP) {
		udev->hnp_en = 1;
	}
#endif

	//set config
	ret = usb_set_config(udev, 1);
	if(ret < 0) {
		debug_printf("set config fail. ret=%d\n", ret);
		goto failtag;
	}

//	struct usb_driver *udrv = g_libhost_cfg.udrv;
//	if(udrv->probe) {
//		ret = udrv->probe(udev, &udev->config.intf, udrv->id_table); 
//	}
//	if(ret < 0) {
//		debug_printf("usb driver(%s) probe fail. ret=%d\n", udrv->name, ret);
//		goto failtag;
//	}

	debug_printf("host enumelate device end. device is: %s. passed!\n", (udev->speed == USB_SPEED_HIGH) ? "highspeed":"fullspeed");

failtag:

	return ret;
}

int usb_get_device_descriptor(struct usb_device *udev, void *buf, unsigned short size)
{
	int ret;

	debug_printf("host get device descriptor(address=%d). len=%d\n", udev->addr, size);

	ret = usb_get_descriptor(udev, USB_DT_DEVICE, 0, buf, size);

	return ret;
}

int usb_get_config_descriptor(struct usb_device *udev, void *buf, unsigned short size)
{
	int ret;

	debug_printf("host get configuration descriptor. len=%d\n", size);

	ret = usb_get_descriptor(udev, USB_DT_CONFIG, 0, buf, size);

	return ret;
}

int usb_get_otg_descriptor(struct usb_device *udev, void *buf, unsigned short size)
{
	int ret;

	debug_printf("host get otg descriptor. len=%d\n", size);

	ret = usb_get_descriptor(udev, USB_DT_OTG, 0, buf, size);

	return ret;
}

int usb_get_descriptor(struct usb_device *udev, unsigned char type, unsigned char index, void *buf, unsigned short size)
{
	int ret;

	ret = usb_control_msg(udev,
						  usb_rcvctrlpipe(udev, 0), 
						  USB_DIR_IN, 
						  USB_REQ_GET_DESCRIPTOR, 
						  (type<<8) + index, 
						  0, 
						  buf, 
						  size); 

	return ret; 
}

static int usb_parse_configuration(struct usb_host_config *config, unsigned char *pbuff, int size)
{
	unsigned char *p = pbuff;
	int i;
	u8 j=0;

	for(i=0; i<size; ) {
		switch(p[i+1]) {
		case USB_DT_CONFIG:
			break;
		case USB_DT_INTERFACE:
			memcpy(&config->intf.intf_desc, p+i, USB_DT_INTERFACE_SIZE); 
			config->intf.ep_cnt = p[i+4];
			break;
		case USB_DT_ENDPOINT:
			if(config->intf.ep_cnt > MAXENDPOINTCOUNT) {
				debug_printf("endpoint count is overflow. cnt=%d, max=%d\n", config->intf.ep_cnt, MAXENDPOINTCOUNT);
			}
			if(j < config->intf.ep_cnt) {
				memcpy(&config->intf.eps[j].ep_desc, p+i, USB_DT_ENDPOINT_SIZE);
				config->intf.eps[j].ep_type = config->intf.eps[j].ep_desc.bmAttributes & USB_ENDPOINT_XFERTYPE_MASK;	
				config->intf.eps[j].ep_num = config->intf.eps[j].ep_desc.bEndpointAddress & USB_ENDPOINT_NUMBER_MASK;	
				config->intf.eps[j].ep_mps = le16_to_cpu(config->intf.eps[j].ep_desc.wMaxPacketSize) & 0x7ff;
				config->intf.eps[j].toggle = 0;
				j++;
			}else {
				debug_printf("ep count is overflow!\n");
				return -3;
			}
			break;
		case USB_DT_DESC_HID:
			break;
		default:
			debug_printf("unknown descriptor type=0x%x\n", p[i+1]);
			return -2;
		}

		i += p[i];
	}

	return 0;
}

int usb_get_config(struct usb_device *udev)
{
#define CONFIGDESCRIPTORMAXLENGTH		0x40
	u8 buffer[CONFIGDESCRIPTORMAXLENGTH];
	u16 size;
	int ret;

	//get config descriptor
	memset(buffer, 0, sizeof(buffer));
	size = USB_DT_CONFIG_SIZE;
	ret = usb_get_config_descriptor(udev, buffer, size);
	if(ret < 0) {
		debug_printf("get config descriptor fail. ret=%d\n", ret);
		goto cfgfail;
	}
	//host_dump_data(buffer, size);
	host_dump_data(buffer, ret);
	memcpy(&udev->config.cfg_desc, buffer, size);
	udev->config.cfg_size = le16_to_cpu(udev->config.cfg_desc.wTotalLength);
	if(udev->config.cfg_size > CONFIGDESCRIPTORMAXLENGTH) {
		debug_printf("config descriptor total length-0x%x is more than buffer size-0x%x, need increase buffer size!\n", udev->config.cfg_size, CONFIGDESCRIPTORMAXLENGTH);
		return -1;
	}
//	debug_printf("config descriptor content:\n");	
//	host_dump_data(buffer, size);

	//get the whole configuration descriptor
	memset(buffer, 0, sizeof(buffer));
	size = udev->config.cfg_size;
	ret = usb_get_config_descriptor(udev, buffer, size);
	if(ret < 0) {
		debug_printf("get the whole config descriptor fail. ret=%d\n", ret);
		goto cfgfail;
	}
	debug_printf("config descriptor total len=0x%x - 0x%x, content:\n", udev->config.cfg_size, udev->config.cfg_desc.wTotalLength);
	host_dump_data(buffer, size);

	if(udev->state != USB_STATE_CONFIGURED) {
		ret = usb_parse_configuration(&udev->config, buffer, size);
		if(ret < 0) {
			debug_printf("parse configuration context error. ret=%d\n", ret);
			goto cfgfail;
		}
	}

cfgfail:

	return ret;
}

int usb_set_address(struct usb_device *udev, unsigned short dev_addr)
{
	int ret;

	debug_printf("host set address\n");

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_DIR_OUT,
						  USB_REQ_SET_ADDRESS,
						  dev_addr,
						  0,
						  NULL,
						  0);

	if(!ret) {
		udev->addr = dev_addr;
		udev->state = USB_STATE_ADDRESS;
	}

	return ret;
}

int usb_set_config(struct usb_device *udev, unsigned short config_no)
{
	int ret;

	debug_printf("host set config\n");

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_DIR_OUT,
						  USB_REQ_SET_CONFIGURATION,
						  config_no,
						  0,
						  NULL,
						  0);

	if(!ret) {
		udev->state = USB_STATE_CONFIGURED;
	}

	return ret;
}

int usb_send_vendor_cmd(struct usb_device *udev, 
						unsigned char dir, 
						unsigned char cmd,
						unsigned short value,
						unsigned short index,
						void *buf,
						unsigned short size) 
{
	int ret;
	int pipe;

	pipe = dir ? usb_rcvctrlpipe(udev, 0) : usb_sndctrlpipe(udev, 0);

	ret = usb_control_msg(udev,
						  pipe, 
						  USB_TYPE_VENDOR | dir, 
						  cmd, 
						  value, 
						  index, 
						  buf, 
						  size); 

	return ret; 
}

int usb_send_class_cmd(struct usb_device *udev, 
						unsigned char dir, 
						unsigned char cmd,
						unsigned short value,
						unsigned short index,
						void *buf,
						unsigned short size) 
{
	int ret;
	int pipe;

	pipe = dir ? usb_rcvctrlpipe(udev, 0) : usb_sndctrlpipe(udev, 0);

	ret = usb_control_msg(udev,
						  pipe, 
						  USB_TYPE_CLASS | dir, 
						  cmd, 
						  value, 
						  index, 
						  buf, 
						  size); 

	return ret; 
}

static int usb_match_ep(struct usb_device *udev, u8 ep_num)
{
	int i;
	struct usb_host_interface *intf = &udev->config.intf;

	for(i=0; i<intf->ep_cnt; i++) {
		if(intf->eps[i].ep_num == ep_num) {
			return 1;
		}
	}

	return 0;
}

//get endpoint number by property 
unsigned char usb_get_ep(struct usb_device *udev, unsigned char type, unsigned char dir)
{
	int i;
	struct usb_host_interface *intf = &udev->config.intf;

	for(i=0; i<intf->ep_cnt; i++) {
		if((intf->eps[i].ep_type == type) && ((intf->eps[i].ep_desc.bEndpointAddress&0x80) == dir)) {
			return intf->eps[i].ep_num;
		}
	}

	return 0;
}

struct usb_host_endpoint *usb_pipe_endpoint(struct usb_device *udev, int pipe)
{
	int i;
	struct usb_host_interface *intf = &udev->config.intf;
	u8 ep_num = usb_pipeendpoint(pipe);

	if(usb_pipecontrol(pipe)) {
		if(usb_pipein(pipe)) {
			return udev->ep0_in;
		}else {
			return udev->ep0_out;
		}
	}else {
		for(i=0; i<intf->ep_cnt; i++) {
			if(intf->eps[i].ep_num == ep_num) {
				return (intf->eps + i);
			}
		}
	}

	return NULL;
}

//the function is blocking, cannot use in interrupt routine
int usb_control_msg(struct usb_device *udev,
					int pipe, 
					unsigned char reqtype, 
					unsigned char req, 
					unsigned short value, 
					unsigned short index, 
					void *data, 
					unsigned short size)
{
	int ret;
	struct urb *urb;
	struct usb_ctrlrequest cr;

	cr.bRequestType = reqtype;
	cr.bRequest = req;
	cr.wValue = cpu_to_le16(value);
	cr.wIndex = cpu_to_le16(index);
	cr.wLength = cpu_to_le16(size);

	int i;
	u8 *c = (u8 *)&cr;
	debug_printf("setup text(0x%x):\n", c);
	for(i=0; i<8; i++){
		debug_printf("%x ", c[i]);
	}
	debug_printf("\n");

	urb = usb_alloc_urb();
	if(!urb) {
		debug_printf("urb alloc fail.\n");
		return -ENOMEM;
	}

	usb_fill_control_urb(urb, udev, pipe, (unsigned char *)&cr, data, size, urb_common_complete_blocking);
	
	ret = usb_submit_urb(urb);

	ret = wait_urb_complete_timeout(urb, USB_CTRL_TIMEOUT);

	debug_printf("control msg retval=%d\n\n", ret);

	return ret;
}

//the function is blocking, cannot use in interrupt routine
int usb_bulk_msg(struct usb_device *udev,
				 int pipe,
				 void *data,
				 unsigned int size,
				 unsigned int *actual_len)
{
	int ret;
	struct urb *urb;
	char s[5];
	struct usb_host_endpoint *ep;
	
	ep = usb_pipe_endpoint(udev, pipe);
	if(!ep) {
		debug_printf("host endpoint is NULL.\n");
		return -1;
	}

	urb = usb_alloc_urb();
	if(!urb) {
		debug_printf("urb alloc fail.\n");
		return -ENOMEM;
	}

	if(usb_pipeint(pipe)) {
		usb_fill_int_urb(urb, udev, pipe, data, size, ep->ep_desc.bInterval, urb_common_complete_blocking);
	}else {
		usb_fill_bulk_urb(urb, udev, pipe, data, size, urb_common_complete_blocking);
	}

	ret = usb_submit_urb(urb);
	if(!ret) {
		if(actual_len) {
			*actual_len = urb->actual_length;
		}
	}

	ret = wait_urb_complete_timeout(urb, USB_BULK_TIMEOUT);

	strcpy(s, (usb_pipein(pipe) ? "in" : "out")); 
	if(usb_pipeint(pipe)) {
		debug_printf("interrupt-%s msg retval=%d\n\n", s, ret);
	}else {
		debug_printf("bulk-%s msg retval=%d\n\n", s, ret);
	}

	return ret;
}

//the function is blocking, cannot use in interrupt routine
int usb_interrupt_msg(struct usb_device *udev,
					  int pipe,
					  void *data,
					  unsigned int size,
					  unsigned int *actual_len)
{
	return usb_bulk_msg(udev, pipe, data, size, actual_len);
}

//the function is blocking, cannot use in interrupt routine
int usb_iso_msg(struct usb_device *udev,
				int pipe,
				void *data,
				unsigned int size,
				unsigned int *actual_len)
{
	int ret;
	struct urb *urb;
	char s[5];
	struct usb_host_endpoint *ep;

	ep = usb_pipe_endpoint(udev, pipe);
	if(!ep) {
		debug_printf("host endpoint is NULL.\n");
		return -1;
	}

	urb = usb_alloc_urb();
	if(!urb) {
		debug_printf("urb alloc fail.\n");
		return -ENOMEM;
	}

	usb_fill_iso_urb(urb, udev, pipe, data, size, ep->ep_desc.bInterval, urb_common_complete_blocking);

	ret = usb_submit_urb(urb);
	if(!ret) {
		if(actual_len) {
			*actual_len = urb->actual_length;
		}
	}

	ret = wait_urb_complete_timeout(urb, USB_ISOC_TIMEOUT);

	strcpy(s, (usb_pipein(pipe) ? "in" : "out")); 
	debug_printf("isoc-%s msg retval=%d\n\n", s, ret);

	return ret;
}

//the function is blocking, cannot use in interrupt routine
int usb_rw(struct usb_device *udev,
			 int pipe,
			 void *data,
			 unsigned int size)
{
	int ret = -1;
	unsigned int actual_len=0;

	u8 ep_num = usb_pipeendpoint(pipe);
	if(!usb_match_ep(udev, ep_num)) {
		debug_printf("usb device don't exist ep_num:0x%x.\n", ep_num);
		return -1;
	}

	switch(usb_pipetype(pipe)) {
	case PIPE_ISOCHRONOUS:
		ret = usb_iso_msg(udev, pipe, data, size, &actual_len);
		break;
	case PIPE_INTERRUPT:
		ret = usb_interrupt_msg(udev, pipe, data, size, &actual_len);
		break;
	case PIPE_BULK:
		ret = usb_bulk_msg(udev, pipe, data, size, &actual_len);
		break;
	case PIPE_CONTROL:
		debug_printf("for control transfer, please use usb_get_config type function.\n");
		return 0;
		break;
	default:
		debug_printf("unknown pipe type.\n");
		break;
	}
	
	if(!ret) {
		return actual_len;
	}

	return ret;
}

//the function don't support more bulk-in or bulk-out endpoints case,
//the function is blocking, cannot use in interrupt routine
int usb_bulk_read(struct usb_device *udev, void *data, unsigned int size)
{
	int ret=-1;
	unsigned int actual_len=0;

	u8 ep_num = usb_get_ep(udev, USB_ENDPOINT_XFER_BULK, USB_DIR_IN);
	if(!ep_num) {
		debug_printf("usb device don't exist bulk-in endpoint.\n");
		return ret;
	}
	
	ret = usb_bulk_msg(udev, usb_rcvbulkpipe(udev, ep_num), data, size, &actual_len);

	if(!ret) {
		return actual_len;
	}

	return ret;
}

//the function don't support more bulk-in or bulk-out endpoints case,
//the function is blocking, cannot use in interrupt routine
int usb_bulk_write(struct usb_device *udev, void *data, unsigned int size)
{
	int ret=-1;
	unsigned int actual_len=0;
	
	u8 ep_num = usb_get_ep(udev, USB_ENDPOINT_XFER_BULK, USB_DIR_OUT);
	if(!ep_num) {
		debug_printf("usb device don't exist bulk-out endpoint.\n");
		return ret;
	}

	ret = usb_bulk_msg(udev, usb_sndbulkpipe(udev, ep_num), data, size, &actual_len);

	if(!ret) {
		return actual_len;
	}

	return ret;
}

int usb_set_feature(struct usb_device *udev, int target, int feature, int selector)
{
	int ret = -1;

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_DIR_OUT | USB_TYPE_STANDARD | target,
						  USB_REQ_SET_FEATURE,
						  feature,
						  (selector << 8),
						  NULL,
						  0);

	return ret;
}

int usb_clear_feature(struct usb_device *udev, int target, int feature, int selector)
{
	int ret = -1;

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_DIR_OUT | USB_TYPE_STANDARD | target,
						  USB_REQ_CLEAR_FEATURE,
						  feature,
						  (selector << 8),
						  NULL,
						  0);

	return ret;
}
 
int usb_get_status(struct usb_device *udev, int target, int featureselector, u16 *status)
{
	int ret = -1;
	u8 size = 2;
	u8 data[2];

	memset(data, 0, 2);

//	if(featureselector == USB_DEVICE_OTG_STATUS) {
//		size = 1;
//	}

	*status = 0;
	ret = usb_control_msg(udev,
						  usb_rcvctrlpipe(udev, 0),
						  USB_DIR_IN | USB_TYPE_STANDARD | target,
						  USB_REQ_GET_STATUS,
						  0,
						  featureselector,
						  data,
						  size);
	if(!ret) {
		*status = (data[0] | (data[1] << 8));
	}else {
		*status = 0x00;
	}

	return ret;
}

