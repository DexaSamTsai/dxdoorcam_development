#include "includes.h"

void td_init(struct ohci_td *td)
{
	memset(td, 0, sizeof(struct ohci_td));

	td->urb = NULL;
	td->td_next = NULL;
	td->td_state = TD_STATE_IDLE;
}

struct ohci_td *td_alloc(void)
{
	struct ohci_td *td;
	struct __ohci_hw_pool *pool = &g_libhost_cfg.ohci_driver->ohci_pool;
	u8 i;

	for(i=0; i<pool->td_pool->cnt; i++) {
		if(pool->td_pool->index >= pool->td_pool->cnt) {
			pool->td_pool->index = 0;
		}
		td = (struct ohci_td *)(pool->td_pool->base) + pool->td_pool->index;

		pool->td_pool->index++;
		if(td->td_state != TD_STATE_LINKED) {
			td_init(td);
			return td;
		}
	}

	if(i == pool->td_pool->cnt) {
		debug_printf("td alloc fail. cnt=%d\n", i);
	}

	return NULL;
}

void td_free(struct ohci_td *td)
{
	if(!td) {
		debug_printf("td is NULL, cannot be free!\n");
		return;
	}

	td->td_state = TD_STATE_UNKNOWN;
}

static int ohci_hcd_dma_init(struct hc_driver *hcdrv)
{
	u8 i, index;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<hcdrv->hcd_dma_cnt; i++) {
		dma = hcdrv->hcd_dmas + i;
		pool = &(dma->dma_pool);
		if(pool->cnt > MAX_ELEMENT_NUM_OF_POOL) {
			debug_printf("dma pool count is overflow!\n");
			return -ENOMEM;
		}

		pool->index = 0;
		for(index=0; index<pool->cnt; index++) {
			dma->dma_element[index].dma_addr = pool->base + (pool->step * index);
			dma->dma_element[index].dma_state = DMA_STATE_IDLE;
		}
	}

	return 0;
}

u32 ohci_dma_alloc(u32 len)
{
	u8 i, j;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<g_libhost_cfg.ohci_driver->hcd_dma_cnt; i++) {
		dma = g_libhost_cfg.ohci_driver->hcd_dmas + i;
		pool = &(dma->dma_pool);
		if(len < pool->step) {
			for(j=0; j<pool->cnt; j++) {
				if(dma->dma_element[j].dma_state == DMA_STATE_IDLE) {
					dma->dma_element[j].dma_state = DMA_STATE_USED;
					return dma->dma_element[j].dma_addr;
				}
			}
		}
	}

	return 0;
}

void ohci_dma_free(u32 dma_addr)
{
	u8 i, j;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<g_libhost_cfg.ohci_driver->hcd_dma_cnt; i++) {
		dma = g_libhost_cfg.ohci_driver->hcd_dmas + i;
		pool = &(dma->dma_pool);
		for(j=0; j<pool->cnt; j++) {
			if(dma_addr == dma->dma_element[j].dma_addr) {
				dma->dma_element[j].dma_state = DMA_STATE_IDLE;
			}
		}
	}
}

struct ohci_ed *ed_alloc(void)
{
	struct ohci_ed *ed;
	struct ohci_td *td_terminate;
	struct __ohci_hw_pool *pool = &g_libhost_cfg.ohci_driver->ohci_pool;

	if(pool->ed_pool->index >= pool->ed_pool->cnt) {
		goto ed_fail;
	}
	ed = (struct ohci_ed *)(pool->ed_pool->base) + pool->ed_pool->index;
	pool->ed_pool->index++;
	
	td_terminate = td_alloc();
	if(!td_terminate) {
		debug_printf("ed alloc terminate td fail.\n");
		ed_free(ed);
		return NULL;
	}
	
	td_terminate->td_state = TD_STATE_LINKED;
	memset(ed, 0, sizeof(struct ohci_ed));
	ed->hwTailP = td_terminate;
	ed->hwHeadP = td_terminate;

	return ed;

ed_fail:
	debug_printf("ed alloc fail.\n");

	return NULL;
}

void ed_free(struct ohci_ed *ed)
{
	if(!ed) {
		debug_printf("ed is NULL, cannot be free!\n");
		return;
	}

	ed->ed_state = ED_STATE_UNKNOWN;
}

void ohci_mem_init(void)
{
	u8 i;
	struct __hw_pool *pool;
	struct hc_driver *hcdrv = g_libhost_cfg.ohci_driver;

	if(hcdrv) {
		ohci_hcd_dma_init(hcdrv);

		pool = hcdrv->ohci_pool.ed_pool;
		debug_printf("ED --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		debug_printf("ed flag: %x\n", ((struct ohci_ed *)(pool->base))->ed_state);

		pool = hcdrv->ohci_pool.td_pool;
		debug_printf("TD --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		debug_printf("td flag: %x\n", ((struct ohci_td *)(pool->base))->td_state);

		pool = hcdrv->ohci_pool.itd_pool;
		debug_printf("ITD --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		debug_printf("DMA cnt:%d\n", hcdrv->hcd_dma_cnt);
		for(i=0; i<hcdrv->hcd_dma_cnt; i++) {
			pool = &(hcdrv->hcd_dmas[i].dma_pool);
			debug_printf("OHCI dma%d --base:0x%x, num:%d, total_size:0x%x\n", pool->step, pool->base, pool->cnt, (pool->step * pool->cnt));
		}
	}

}

