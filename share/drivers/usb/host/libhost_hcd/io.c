#include "includes.h"

#define USB_OVT_VENDOR_ID	0x05A9
#define USB_OVT_PRODUCT_ID	0x0788

#define    MS_GET_MAX_LUN_REQ            0xFE
#define    MASS_STORAGE_CLASS            0x08
#define    MASS_STORAGE_SUBCLASS_SCSI    0x06
#define    MASS_STORAGE_PROTOCOL_BO      0x50

extern struct usb_driver host_iotest_driver;

static const struct usb_device_id host_iotest_id_table[] = {
	{
		.match_flags = USB_DEVICE_ID_MATCH_PRODUCT,//USB_DEVICE_ID_MATCH_DEVICE,

		.idVendor = USB_OVT_VENDOR_ID,
		.idProduct = USB_OVT_PRODUCT_ID,

		.bInterfaceClass = MASS_STORAGE_CLASS,
		.bInterfaceSubClass = MASS_STORAGE_SUBCLASS_SCSI,
		.bInterfaceProtocol = MASS_STORAGE_PROTOCOL_BO,
	},
	{},
};

int host_iotest_probe(struct usb_device *udev, struct usb_host_interface *intf, const struct usb_device_id *id)
{
	struct usb_host_endpoint *ep, *in_ep, *out_ep;
	u8 i;

	if(!usb_match_id(udev, intf, id)) {
		debug_printf("usb not match id.\n");
		return -1;
	}

	in_ep = NULL;
	out_ep = NULL;
	for(i=0; i<intf->ep_cnt; i++) {
		ep = intf->eps + i;
		if(ep->ep_desc.bEndpointAddress & USB_DIR_IN) {
			in_ep = ep;
		}else {
			out_ep = ep;
		}

		if(in_ep && out_ep) {//find in and out endpoint
			if(in_ep->ep_type != out_ep->ep_type) {
				debug_printf("device's in and out ep is invalid.\n");
				return -1;
			}else {
				in_ep = NULL;
				out_ep = NULL;
			}
		}
	}

	return 0;
}

struct usb_driver host_iotest_driver = 
{
	.name = "IO Test",
	.probe = host_iotest_probe,
	.id_table = host_iotest_id_table,
};

int hostclass_init(void)
{
	usb_driver_register(&host_iotest_driver);

	return 0;
}


