#include "includes.h"


int ehci_hub_control(struct urb *urb)
{
	u16 typeReq, wValue, wIndex, wLength;
	struct usb_ctrlrequest *ctrl_req;
	u32 port_status;
	u8 selector;

	int ports = HCS_N_PORTS(ReadReg32(EHCI_REG_HCSPARAMS));

	ctrl_req = (struct usb_ctrlrequest *)(urb->setup_packet);
	typeReq = (ctrl_req->bRequestType << 8) | ctrl_req->bRequest;
	wValue = le16_to_cpu(ctrl_req->wValue);//feature selector
	wIndex = le16_to_cpu(ctrl_req->wIndex);//selector(only for indicator) + port number
	wLength = le16_to_cpu(ctrl_req->wLength);

	if(wLength > urb->transfer_buffer_length) {
		debug_printf("ehci_hub_control: setup packet length(0x%x) is more than urb buffer length(0x%x)!\n", wLength, urb->transfer_buffer_length);
		return -1;
	}

	switch(typeReq) {
	case ClearPortFeature:
		if(!wIndex || ((wIndex & 0xff) > ports)) {
			debug_printf("ClearPortFeature-- port number(%d) is empty or more than total ports(%d).", wIndex, ports);
			return -2;
		}
		port_status = ReadReg32(EHCI_REG_PORTSTS);

		switch(wValue) {
		case USB_PORT_FEAT_SUSPEND:// port resume
			if(!(port_status & PORT_SUSPEND)) {
				break;
			}
			if((port_status & PORT_PE) == 0) {
				debug_printf("port %d not be enabled when suspend clear operation.\n", wIndex);
				return -2;
			}

			port_status &= ~(PORT_RWC_BITS);
			WriteReg32(EHCI_REG_PORTSTS, port_status | PORT_RESUME);
			//caller must wait more than 20ms, then call GetPortStatus
			break;
		case USB_PORT_FEAT_TEST://test mode 
			selector = (u8)(wIndex >> 8);
			if(!selector || selector > 5) {
				debug_printf("this test item=0x%x don't be support now!\n", selector);
				return -2;
			}

			if(ReadReg32(EHCI_REG_STATUS) & STS_HALT) {
				WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_RESET);//terminate and exit test mode 
				{volatile int dd; for(dd=0; dd<900000; dd++);}
			}else {
				debug_printf("hc isn't halt, so cannot clear test mode.\n");
			}
			debug_printf("port status: 0x%x, usbcmd: 0x%x, usbsts: 0x%x\n", ReadReg32(EHCI_REG_PORTSTS), ReadReg32(EHCI_REG_CMD), ReadReg32(EHCI_REG_STATUS));
			break;
		default:
			debug_printf("clearFeature--this port feature=%d don't be support now!\n", wValue);
			//TODO: maybe need handle other port features
			break;
		}
		break;
	case GetPortStatus:
		if(!wIndex || ((wIndex & 0xff) > ports)) {
			debug_printf("GetPortStatus-- port number(%d) is empty or more than total ports(%d).", wIndex, ports);
			return -2;
		}
		port_status = ReadReg32(EHCI_REG_PORTSTS);

		u32 *buf = (u32 *)urb->transfer_buffer;
		debug_printf("reset-2: port_status=0x%x\n", ReadReg32(EHCI_REG_PORTSTS));

		//whoever resume must GetPortStatus to complete it.
		if(port_status & PORT_RESUME) {//ready clear resume bit
			debug_printf("be ready to release resume signal.\n");
			port_status &= ~(PORT_RWC_BITS | PORT_RESUME);
			WriteReg32(EHCI_REG_PORTSTS, port_status);//release resume signal
			while(ReadReg32(EHCI_REG_PORTSTS) & PORT_RESUME);
			debug_printf("port status=0x%x\n", ReadReg32(EHCI_REG_PORTSTS));
		}

		//whoever reset must GetPortStatus to complete it.
		if(port_status & PORT_RESET) {//ready to clear reset it
			debug_printf("be ready to release reset signal.\n");
			port_status &= ~(PORT_RWC_BITS | PORT_RESET);
			WriteReg32(EHCI_REG_PORTSTS, port_status);//release reset signal
			while(ReadReg32(EHCI_REG_PORTSTS) & (PORT_RESET|PORT_PE) == PORT_RESET);
		}
		debug_printf("get_port_status: port status=0x%x\n", ReadReg32(EHCI_REG_PORTSTS));

		*buf = 0x00;//TODO: you must modify this line to get correct port status

		break;
	case SetPortFeature:
		if(!wIndex || ((wIndex & 0xff) > ports)) {
			debug_printf("SetPortFeature-- port number(%d) is empty or more than total ports(%d).", wIndex, ports);
			return -2;
		}

		port_status = ReadReg32(EHCI_REG_PORTSTS);
		//TODO:
//		if(port_status & PORT_OWNER) {//usb1.1 to handle this port.
//			debug_printf("A companion host controller to handle this port.\n");
//			break;
//		}

		switch(wValue) {
		case USB_PORT_FEAT_SUSPEND://port suspend
			if((port_status & PORT_PE) == 0) {
				debug_printf("port %d not be enabled when suspend set operation.\n", (wIndex & 0xff));
				return -2;
			}

			port_status &= ~(PORT_RWC_BITS);
			debug_printf("pre-suspend:0x%x\n", ReadReg32(EHCI_REG_PORTSTS));
			WriteReg32(EHCI_REG_PORTSTS, port_status | PORT_SUSPEND);
			while((ReadReg32(EHCI_REG_PORTSTS) & PORT_SUSPEND) != PORT_SUSPEND);//wait suspend bit be set to 1.
			//caller must wait more than 10ms
			break;
		case USB_PORT_FEAT_RESET://port reset
			if(port_status & PORT_RESUME) {
				debug_printf("port %d is doing resume. port status=0x%x\n", (wIndex & 0xff), ReadReg32(EHCI_REG_PORTSTS));
				return -2;
			}

			debug_printf("reset-1: port_status=0x%x\n", ReadReg32(EHCI_REG_PORTSTS));
			port_status &= ~(PORT_RWC_BITS);
			port_status &= ~(PORT_PE);
			port_status |= PORT_RESET;
			WriteReg32(EHCI_REG_PORTSTS, port_status);
			//caller must wait more than 50ms, then call GetPortStatus
			break;
		case USB_PORT_FEAT_TEST://test mode
			selector = (u8)(wIndex >> 8);
			if(!selector || selector > 5) {
				debug_printf("this test item=0x%x don't be support now!\n", selector);
				return -2;
			}

			//step.1 disable PSE and ASE
			WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) & (~(CMD_PSE | CMD_ASE)));

			//step.2 place port into suspend state
			WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS) | PORT_SUSPEND);
			while((ReadReg32(EHCI_REG_PORTSTS) & PORT_SUSPEND) != PORT_SUSPEND);//wait suspend bit be set to 1.

			//step.3 stop ehci controller and entry halt
			WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) & (~CMD_RUN));
			while(!(ReadReg32(EHCI_REG_STATUS) & STS_HALT));

			//step.4 action test item
			WriteReg32(EHCI_REG_PORTSTS, port_status | (selector << 16));

			if(selector == TEST_FORCE_EN) {
				{volatile int dd; for(dd=0; dd<200; dd++);}
				WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_RUN);
				{volatile int dd; for(dd=0; dd<200; dd++);}
			}

			debug_printf("port status: 0x%x, usbcmd: 0x%x, usbsts: 0x%x\n", ReadReg32(EHCI_REG_PORTSTS), ReadReg32(EHCI_REG_CMD), ReadReg32(EHCI_REG_STATUS));
			break;
		default:
			debug_printf("setFeature--this port feature=%d don't be support now!\n", wValue);
			break;
		}

		break;
	default:
		debug_printf("this typeReq=0x%x don't be support now!\n", typeReq);
		return -3;
	}

	if(urb->urb_callback) {
		urb->actual_length = 0;
		urb->urb_callback(urb);
	}

	return 0;
}

