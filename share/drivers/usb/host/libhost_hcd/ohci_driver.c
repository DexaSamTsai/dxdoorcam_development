#include "includes.h"


struct __hw_pool __ed_pool = {
	.base = OHCI_ED_POOL_BASE,
	.step = sizeof(struct ohci_ed),
	.cnt = OHCI_ED_POOL_NUM,
	.index = 0,
};

struct __hw_pool __td_pool = {
	.base = OHCI_TD_POOL_BASE,
	.step = sizeof(struct ohci_td),
	.cnt = OHCI_TD_POOL_NUM,
	.index = 0,
};

struct __hw_pool __itd_pool_fs = {
	.base = OHCI_ITD_POOL_BASE,
	.step = sizeof(struct ohci_itd),
	.cnt = OHCI_ITD_POOL_NUM,
	.index = 0,
};

struct __hcd_dma __hcd_dma_pool_fs[] = {
	{
		.dma_pool = {
			.base = OHCI_DATA64_DMA_BASE,
			.step = OHCI_DATA64_DMA_STEP,
			.cnt = OHCI_DATA64_DMA_NUM,
		},
	},

	{
		.dma_pool = {
			.base = OHCI_DATA512_DMA_BASE,
			.step = OHCI_DATA512_DMA_STEP,
			.cnt = OHCI_DATA512_DMA_NUM,
		},
	},

	{
		.dma_pool = {
			.base = OHCI_DATA1024_DMA_BASE,
			.step = OHCI_DATA1024_DMA_STEP,
			.cnt = OHCI_DATA1024_DMA_NUM,
		},
	},
};

struct ohci_hcd hcd = {
	.ohci_hcca_memory_base = OHCI_HCCA_BASE,	
};

struct hc_driver ohci_hc_driver = {
	.hcd_name = "OHCI HCD",
	.product_name = "ROBIN OHCI Host Controller",

	.speed = HCD_USB11,

	.irq = ohci_irq,

	.port_detect = ehci_check_port_status,
	.urb_enqueue = ohci_urb_enqueue,
	.hub_control = ehci_hub_control,

	.ohci_pool = {
		.ed_pool = &__ed_pool,
		.td_pool = &__td_pool, 
		.itd_pool = &__itd_pool_fs, 
	},

	.ohci_hcd = &hcd,

	.hcd_dmas = &__hcd_dma_pool_fs[0],
	.hcd_dma_cnt = sizeof(__hcd_dma_pool_fs)/sizeof(__hcd_dma_pool_fs[0]),

	.dma_alloc = ohci_dma_alloc,
	.dma_free = ohci_dma_free,
};

//memory and hw init, hcd register
void ohci_driver_init(void)
{
	struct hc_driver *hcdrv = &ohci_hc_driver;

	hcdrv->ohci_hcd->hcca = (struct ohci_hccd *)hcdrv->ohci_hcd->ohci_hcca_memory_base;
	hcdrv->ohci_hcd->ed_ctrltail = NULL;
	hcdrv->ohci_hcd->ed_bulktail = NULL;

	hcd_driver_register(hcdrv);
}

