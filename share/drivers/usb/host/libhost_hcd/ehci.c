#include "includes.h"

#define CONFIG_EHCI_DEBUG

#define EHCI_TUNE_CEER	 3		/* 0-3 qtd retries; 0 == don't stop */

static int qh_completion(struct ehci_qh *qh)
{
	struct ehci_qtd *qtd_cur, *qtd_next;
	struct urb *urb;
	u32 token;
	u8 error = 0;

#ifdef	CONFIG_EHCI_DEBUG
	ehci_qh_debug(qh);
#endif

	qtd_next = qh->qtd_head;
	do {
		qtd_cur = qtd_next;
		if(qtd_cur == qh->qtd_tail) {//the last terminate qtd
			lowlevel_putc('E');
			break;
		}

#ifdef	CONFIG_EHCI_DEBUG
		ehci_qtd_debug(qtd_cur);
#endif

		token = cpu_to_le32(qtd_cur->hw_token);
		if(token & QTD_STS_ACTIVE) {//hc don't modify active bit
			debug_putc('e');
			break;
		}else {
			if(((token & 0x7f) > 0x01) && (QTD_CERR(token) == 0)) {//transfer error
				debug_printf("QTD error: hw_token=0x%x, left length=0x%x\n", token, QTD_LENGTH(token));
				error = 1;
			}
			
			urb = qtd_cur->urb;
			if(QTD_PID(token) != TD_PID_SETUP) {
				urb->actual_length += qtd_cur->length - QTD_LENGTH(token);
			}

			urb->qtd_cnt--;
			if(urb->qtd_cnt == 0) {
				if(urb->urb_callback) {
					urb->urb_callback(urb);
				}
			}

			qtd_next = qtd_cur->qtd_next;
			qtd_free(qtd_cur);
			qh->qtd_head = qtd_next;
	
			if(error) {
				return -EIO;
			}
		}
	}while(qtd_next);

	return 0;
}
  
static void scan_async(void)
{
	struct ehci_qh *qh, *qh_prev;

	qh_prev = g_libhost_cfg.ehci_driver->async_head;
	qh = qh_prev->qh_next;
	if(qh) {
		do {
			if(qh->qtd_head != qh->qtd_tail) {
				qh_completion(qh);
			}

			//unlink qh from qh list
			qh_prev->qh_next = qh->qh_next;
			qh_prev->hw_next = le32_to_cpu(qh->hw_next);

			qh = qh->qh_next;
		}while(qh);
	}
}

static int itd_completion(struct ehci_itd *itd)
{
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;
	int status;
	u8 error = 0;
	
#ifdef	CONFIG_EHCI_DEBUG
	ehci_itd_debug(itd);
#endif

restart:
	status = cpu_to_le32(itd->hw_transaction[0]);
	if(status & EHCI_ISOC_ACTIVE) {
		debug_printf("this itd still active. transaction[0]=0x%x\n", status);
		goto restart;
	}

	if(status & 0xf0000000) {
		debug_printf("this itd transfer fail. transaction[0]=0x%x\n", status);
		error = 1;
	}else {//success
		struct urb *urb = itd->urb;

		urb->actual_length = EHCI_ITD_LENGTH(status);
		if(urb->urb_callback) {
			urb->urb_callback(urb);
		}
	}

	//terminate periodic frame list of this itd
	*(u32 *)(hcdrv->periodic_list_addr + (itd->period_frame << 2)) = le32_to_cpu(0x00000001);	
	hcdrv->period_link_cnt--;

	itd_free(itd);
	
	if(error) {
		return -EIO;
	}

	return 0;
}

static void scan_periodic(void)
{
	struct ehci_qh *qh, *qh_prev;
	struct ehci_itd *itd_prev, *itd;
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;

	//scan interrupt qh info 
	qh_prev = g_libhost_cfg.ehci_driver->intr_head;
	qh = qh_prev->qh_next; 
	if(qh) {
		do {
			if(qh->qtd_head != qh->qtd_tail) {
				qh_completion(qh);
			}

			//unlink qh from qh list
			qh_prev->qh_next = qh->qh_next;
			qh_prev->hw_next = le32_to_cpu(qh->hw_next);
			hcdrv->period_link_cnt--;

			qh = qh->qh_next;
		}while(qh);
	}

	//scan periodic isoc itd info
	itd_prev = g_libhost_cfg.ehci_driver->itd_head;
	itd = itd_prev->itd_next;
	if(itd) {
		do {
			itd_completion(itd);

			//unlink itd from itd list
			itd_prev->itd_next = itd->itd_next;

			itd = itd->itd_next;
		}while(itd);
	}
}

void ehci_work(void)
{
	scan_async();
	scan_periodic();
}

static void qtd_list_free(struct ehci_qtd *qtd_head)
{
	struct ehci_qtd *cur, *tmp;

	cur = qtd_head;
	while(cur) {
		if(cur->qtd_next) {
			tmp = cur;
			cur = cur->qtd_next;
			qtd_free(tmp);
		}else {
			qtd_free(cur);
			break;
		}
	}
}

//return count of the buffer address be queued
static int qtd_fill(struct urb *urb, struct ehci_qtd *qtd, u32 token, u32 dma_addr, u32 dma_size, u32 mps)
{
	u32 buf_addr, count;
	u8 i;

	buf_addr = dma_addr;

	//fill qtd buffer content
	qtd->hw_buf[0] = le32_to_cpu(buf_addr);

	count = 0x1000 - (buf_addr & 0xfff);//rest memory size of this page
	if(count < dma_size) {//need more page to transfer
		dma_addr &= ~0xfff;// 4k bytes aligned
		dma_addr += 0x1000; //address increase 4k offset

		for(i=1; ((count < dma_size) && (i < 5)); i++) {//fill page buffer info
			buf_addr = dma_addr;
			qtd->hw_buf[i] = le32_to_cpu(buf_addr);
			dma_addr += 0x1000; //address increase 4k offset
			if((count + 0x1000) < dma_size) {
				count += 0x1000;
			}else {
				count = dma_size;
			}
		}

		/* short packet only be used to terminate transfer */
		if(count != dma_size) {
			count -= (count % mps);
		}
	}else {
		count = dma_size;
	}

	qtd->hw_token = le32_to_cpu(token | (count << 16));

	qtd->qtd_state = QTD_STATE_LINKED;
	qtd->length = count;
	qtd->urb = urb;
	urb->qtd_cnt++;
	urb->qtd_tail = qtd;

	return count;
}

//create qtd list for this urb, not link to qh, return qtd head
struct ehci_qtd *qtd_list_build(struct urb *urb)
{
	struct ehci_qtd *qtd, *qtd_prev, *qtd_head;
	u32 qtd_token;
	u32 maxpacketsize;

	qtd = qtd_alloc();
	if(!qtd) {
		return NULL;
	}

	qtd_head = qtd;
	qtd_token = QTD_STS_ACTIVE;
	qtd_token |= (EHCI_TUNE_CEER << 10);

	if(usb_pipebulk(urb->pipe) || usb_pipeint(urb->pipe)) {
		qtd_token |= (urb->ep->toggle << 31);//last toggle
	}

	if(usb_pipecontrol(urb->pipe)) {
		qtd_fill(urb,
				 qtd,
				 qtd_token | (TD_PID_SETUP << 8),	//setup packet
				 (u32)urb->setup_dma,
				 sizeof(struct usb_ctrlrequest),
				 8);

		qtd_token ^= QTD_TOGGLE;
		qtd_prev = qtd;
		qtd = qtd_alloc();
		if(!qtd) {
			goto qtd_fail;	
		}
		qtd_prev->hw_next = le32_to_cpu((u32)qtd);
		qtd_prev->qtd_next = qtd;

		if(urb->transfer_buffer_length == 0) {//for zero data stage, status packet always is PID_IN
			qtd_token |= (TD_PID_IN << 8);
			qtd_fill(urb, qtd, qtd_token, 0, 0, 0); //'0' packet
		}
	}

	maxpacketsize = urb->ep->ep_mps;

	u32 buf_addr = urb->transfer_dma;
	int buf_len = urb->transfer_buffer_length;
	u32 this_qtd_len;
	u32 pktnum = 0;

	if(usb_pipein(urb->pipe)){
		qtd_token |= (TD_PID_IN << 8);
	}

	while(buf_len) {
		this_qtd_len = qtd_fill(urb,
								qtd,
							    qtd_token,
							    buf_addr,
							    buf_len,
								maxpacketsize);
		buf_len -= this_qtd_len;
		buf_addr += this_qtd_len;

		pktnum = (this_qtd_len + maxpacketsize - 1)/maxpacketsize;
		if((pktnum & 0x1) == 1) {
			qtd_token ^= QTD_TOGGLE;
		}

		if(buf_len <= 0) {
			break;
		}

		qtd_prev = qtd;
		qtd = qtd_alloc();
		if(!qtd) {
			goto qtd_fail;
		}
		qtd_prev->hw_next = le32_to_cpu((u32)qtd);
		qtd_prev->qtd_next = qtd;
	}

	if(usb_pipecontrol(urb->pipe) && urb->transfer_buffer_length) {
		qtd_token ^= 0x0100;//PID -- "in" <--> "out"
		qtd_token |= QTD_TOGGLE;//force DATA1 PID

		qtd_prev = qtd;
		qtd = qtd_alloc();
		if(!qtd) {
			goto qtd_fail;
		}
		qtd_prev->hw_next = le32_to_cpu((u32)qtd);
		qtd_prev->qtd_next = qtd;
	
		qtd_fill(urb, qtd, qtd_token, 0, 0, 0);//'0' packet
	}

	if(usb_pipebulk(urb->pipe) || usb_pipeint(urb->pipe)) {
		urb->ep->toggle = (u8)(qtd_token >> 31);//save to last toggle
	}

	qtd->hw_token |= le32_to_cpu(QTD_IOC);//enable ioc for last qtd

	return qtd_head;

qtd_fail:
	qtd_list_free(qtd_head);
	return NULL;
}

static void qh_update(struct ehci_qh *qh)
{
	if(qh->qh_state != QH_STATE_IDLE) {
		debug_printf("qh state isn't idle. state:%d\n", qh->qh_state);
		return;
	}

	if(qh->qtd_head == qh->qtd_tail) {//qh's qtd_list only have a terminate qtd
		qh->hw_qtd_next = le32_to_cpu(((u32)qh->qtd_head) & (~0x0000001f)); 
		qh->hw_alt_next = le32_to_cpu((u32)0x00000001);
		qh->hw_token = le32_to_cpu((u32)0x00000000);
	}else {
		if((u32)qh->qtd_head == cpu_to_le32(qh->hw_current))	{//qtd being handle by hc
			return;
		}else {
			qh->hw_qtd_next = le32_to_cpu(((u32)qh->qtd_head) & (~0x0000001f)); 
			qh->hw_alt_next = le32_to_cpu((u32)0x00000001);
			qh->hw_token = le32_to_cpu((u32)0x00000000);
		}
	}
}

static struct ehci_qh *qh_build(struct urb *urb)
{
	struct ehci_qh *qh;
	u32 info1 = 0, info2 = 0;
	u8 mult = 1, i;

	qh = qh_alloc();
	if(!qh) {
		return NULL;
	}

	if(usb_pipeint(urb->pipe)) {
		mult = (urb->transfer_buffer_length + (urb->ep->ep_mps - 1))/urb->ep->ep_mps;
		if(mult > 3) {
			mult = 3;
			debug_printf("ehci interrupt qh mult:%d overflow.\n", mult);
		}
	}

	info1 = usb_pipedevice(urb->pipe) | (usb_pipeendpoint(urb->pipe) << 8) | (0x02 << 12) | (0x01 << 14) | (urb->ep->ep_mps << 16);
	info2 = (mult << 30) | (0x01 << 23);
	if(usb_pipeint(urb->pipe)) {
		info2 |= 0x80;//uFrame SMASK 
	}

	qh->qh_state = QH_STATE_IDLE;

	qh->hw_info1 = le32_to_cpu(info1);
	qh->hw_info2 = le32_to_cpu(info2);
	qh->hw_qtd_next = le32_to_cpu(0x00000001);
	qh->hw_alt_next = le32_to_cpu(0x00000001);
	qh->hw_current = 0x00;
	qh->hw_next = 0x00;
	for(i=0; i<5; i++) {
		qh->hw_buf[i] = NULL; 
	}

	qh_update(qh);

	return qh;
}

static struct ehci_qh *qh_append_qtds(struct urb *urb, void **ptr)
{
	struct ehci_qh *qh;
	struct ehci_qtd *tmp;
	u8 state;
	u32 token;

	qh = (struct ehci_qh *)(*ptr);
	if(qh == NULL) {
		qh = qh_build(urb); 
		*ptr = qh; //save qh to ep->hcpriv
	}

	if(qh) {
		qh->hw_info1 &= le32_to_cpu(~0x7f); //clear the old address info
		qh->hw_info1 += le32_to_cpu(usb_pipedevice(urb->pipe));//update device address info
		if(usb_pipecontrol(urb->pipe) && (usb_pipedevice(urb->pipe) == 0)) {
			qh->hw_info1 &= le32_to_cpu(~0x7f); //revert to address 0 after usb reset device
		}

		if(urb->qtd_head) {//add urb's qtds to qh's qtds
			if(qh->qtd_head == qh->qtd_tail) {
				qh->qtd_head = urb->qtd_head;
				
				tmp = qh->qtd_tail;
				urb->qtd_tail->qtd_next = tmp; 
				qh->qh_state = le32_to_cpu(QH_STATE_IDLE);
			}else {
				debug_printf("qtd->head: %x\n", qh->qtd_head);
				tmp = qh->qtd_head;
				tmp->hw_next = urb->qtd_head;
				tmp = qh->qtd_tail;
				urb->qtd_tail->qtd_next = tmp; 
			}
		}
	}

	return qh;
}

static void qh_link_async(struct ehci_qh *qh)
{
	struct ehci_qh *head = g_libhost_cfg.ehci_driver->async_head;

	qh_update(qh);

#ifdef CONFIG_EHCI_DEBUG
	ehci_qh_debug(qh);
	struct ehci_qtd *qtd = qh->qtd_head;
	while(1) {
		ehci_qtd_debug(qtd);
		qtd = qtd->qtd_next;
		if(qtd == NULL) {	
			break;
		}
	}
#endif

	qh->qh_next = head->qh_next;
	qh->hw_next = le32_to_cpu(head->hw_next);

	head->qh_next = qh;
	head->hw_next = le32_to_cpu((u32)qh);

	qh->qh_state = QH_STATE_LINKED;

	//enable async transfer
	if(!(ReadReg32(EHCI_REG_CMD) & CMD_ASE)) {
		WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_ASE);
		while((ReadReg32(EHCI_REG_CMD) & CMD_ASE) == 0);
	}
}

int submit_async(struct urb *urb)
{
	struct ehci_qh *qh;
	u32 flags;

	local_irq_save(flags);
	qh = qh_append_qtds(urb, &urb->ep->hcpriv);
	if(!qh) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

	if(qh->qh_state == QH_STATE_IDLE) {//new qh will be link to async
		qh_link_async(qh);
	}

	local_irq_restore(flags);
	return 0;
}

static void qh_link_periodic(struct ehci_qh *qh)
{
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;
	struct ehci_qh *head = hcdrv->intr_head;

	qh_update(qh);

#ifdef CONFIG_EHCI_DEBUG
	ehci_qh_debug(qh);
#endif

	struct urb *urb = qh->qtd_head->urb;
	if(urb) {
		qh->qh_next = head->qh_next;
		qh->hw_next = le32_to_cpu(QH_TERMINATE);//set terminate flag
		head->qh_next = qh;
	
		int interval = hcdrv->interval;//we use const periodic interval (ms)
		u8 div = hcdrv->periodic_list_size/interval;//divide periodic frame list into "div" groups
		u8 i;

		for(i=0; i<div; i++) {//link qh to periodic frame list
			*(u32 *)(hcdrv->periodic_list_addr + ((i * interval + hcdrv->period_link_cnt) << 2)) = le32_to_cpu(((u32)qh & ~0x1f) | QH_TYPE_QH);	
		}

		hcdrv->period_link_cnt++;
		qh->qh_state = QH_STATE_LINKED;
	
		//enable periodic transfer
		if(!(ReadReg32(EHCI_REG_CMD) & CMD_PSE)) {
			WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_PSE);
			while((ReadReg32(EHCI_REG_CMD) & CMD_PSE) == 0);
		}
	}
}

int submit_intr(struct urb *urb)
{
	u32 flags;

	local_irq_save(flags);

	struct ehci_qh *qh = qh_append_qtds(urb, &urb->ep->hcpriv);
	if(!qh) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

	if(qh->qh_state == QH_STATE_IDLE) {//new qh will be link to periodic frame list
		qh_link_periodic(qh);
	}

	local_irq_restore(flags);
	return 0;
}

static void itd_fill(struct urb *urb, struct ehci_itd *itd)
{
	u8 dir_in, mult;

	itd->hw_transaction[0] = le32_to_cpu(EHCI_ISOC_ACTIVE |
									    (urb->transfer_buffer_length << 16) | 
										EHCI_ITD_IOC | 
										(0x00 << 12) | 
										(urb->transfer_dma & 0xfff));               
	itd->hw_bufp[0] = le32_to_cpu((urb->transfer_dma & ~(0x00000fff)) | (urb->ep->ep_num << 8) | (urb->udev->addr));
	dir_in = usb_pipein(urb->pipe) ? 0x01 : 0x00;
	itd->hw_bufp[1] = le32_to_cpu((0x00 << 12) | (dir_in << 11) | urb->ep->ep_mps);
	mult = (urb->transfer_buffer_length + urb->ep->ep_mps - 1)/urb->ep->ep_mps;
	if(mult > 3) {
		debug_printf("ehci itd transaction mult:%d is overflow.\n", mult);
		mult = 3;
	}
	itd->hw_bufp[2] = le32_to_cpu((0x00 << 12) | mult);

	itd->urb = urb;
}

static struct ehci_itd *itd_build(struct urb *urb)
{
	struct ehci_itd *itd;

	itd = itd_alloc();
	if(!itd) {
		return NULL;
	}

	itd_fill(urb, itd);

	return itd;
}

static void itd_link_periodic(struct ehci_itd *itd)
{
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;
	struct ehci_itd *head = hcdrv->itd_head;

#ifdef CONFIG_EHCI_DEBUG
	ehci_itd_debug(itd);
#endif

	itd->itd_next = head->itd_next;
	head->itd_next = itd;

	int interval = hcdrv->interval;//we use const periodic interval (ms)
	u8 div = hcdrv->periodic_list_size / interval;//divide periodic frame list into "div" groups
	u32 frameidx = (ReadReg32(EHCI_REG_FRAMEINDEX) >> 3) % hcdrv->periodic_list_size;
	
	u8 i;
	for(i=1; i<=div; i++) {
		if(frameidx < (interval * i)) {
			itd->period_frame = (interval * i) + hcdrv->period_link_cnt;//base is "interval * i"
			break;
		}
	}
	if(i == div) {
		itd->period_frame = hcdrv->period_link_cnt;//base is '0'
	}

	//link itd to periodic frame list
	*(u32 *)(hcdrv->periodic_list_addr + (itd->period_frame << 2)) = le32_to_cpu(((u32)itd & ~0x1f) | QH_TYPE_ITD);	
	hcdrv->period_link_cnt++;

	itd->itd_state = ITD_STATE_LINKED;

	//enable periodic transfer
	if(!(ReadReg32(EHCI_REG_CMD) & CMD_PSE)) {
		WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_PSE);
		while((ReadReg32(EHCI_REG_CMD) & CMD_PSE) == 0);
	}
}

int submit_isoc(struct urb *urb)
{
	u32 flags;

	local_irq_save(flags);
	struct ehci_itd *itd = itd_build(urb);
	if(!itd) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

	if(itd->itd_state == ITD_STATE_IDLE) {//new itd will be link to periodic frame list
		itd_link_periodic(itd);
	}
	local_irq_restore(flags);


	return 0;
}


