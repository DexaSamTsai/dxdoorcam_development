#include "includes.h"

struct __hw_pool __qh_pool = {
	.base = EHCI_QH_POOL_BASE,
	.step = sizeof(struct ehci_qh),
	.cnt = EHCI_QH_POOL_NUM,
	.index = 0,
};

struct __hw_pool __qtd_pool = {
	.base = EHCI_QTD_POOL_BASE,
	.step = sizeof(struct ehci_qtd),
	.cnt = EHCI_QTD_POOL_NUM,
	.index = 0,
};

struct __hw_pool __itd_pool = {
	.base = EHCI_ITD_POOL_BASE,
	.step = sizeof(struct ehci_itd),
	.cnt = EHCI_ITD_POOL_NUM,
	.index = 0,
};

struct __hcd_dma __hcd_dma_pool[] = {
	{
		.dma_pool = {
			.base = EHCI_DATA64_DMA_BASE,
			.step = EHCI_DATA64_DMA_STEP,
			.cnt = EHCI_DATA64_DMA_NUM,
		},
	},

	{
		.dma_pool = {
			.base = EHCI_DATA512_DMA_BASE,
			.step = EHCI_DATA512_DMA_STEP,
			.cnt = EHCI_DATA512_DMA_NUM,
		},
	},

	{
		.dma_pool = {
			.base = EHCI_DATA3072_DMA_BASE,
			.step = EHCI_DATA3072_DMA_STEP,
			.cnt = EHCI_DATA3072_DMA_NUM,
		},
	},
};

struct hc_driver ehci_hc_driver = {
	.hcd_name = "EHCI HCD",
	.product_name = "ROBIN EHCI Host Controller",

	.speed = HCD_USB20,

	.irq = ehci_irq,

	.port_detect = ehci_check_port_status,
	.urb_enqueue = ehci_urb_enqueue,
	.hub_control = ehci_hub_control,

	.periodic_list_size = EHCI_PERIODIC_FRAME_LIST_SIZE,
	.periodic_list_addr = EHCI_PERIODIC_MEMORY_BASE,
	.period_link_cnt = 0,
	.interval = 256, //256ms

	.ehci_pool = {
		.qh_pool = &__qh_pool,
		.qtd_pool = &__qtd_pool, 
		.itd_pool = &__itd_pool, 
	},

	.hcd_dmas = &__hcd_dma_pool[0],
	.hcd_dma_cnt = sizeof(__hcd_dma_pool)/sizeof(__hcd_dma_pool[0]),

	.dma_alloc = ehci_dma_alloc,
	.dma_free = ehci_dma_free,
};

void ehci_driver_init(void)
{
	hcd_driver_register(&ehci_hc_driver);
}

