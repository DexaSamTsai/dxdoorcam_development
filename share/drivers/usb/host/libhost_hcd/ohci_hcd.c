#include "includes.h"

void ohci_hw_init(void)
{
	int i;
	struct ohci_hcca *hcca = g_libhost_cfg.ohci_driver->ohci_hcd->hcca;

	debug_printf("\n++ ohci_hw_init\n");

	// enable ehci port power -- port status and control register
	WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS)|PORT_POWER);
	debug_printf("EHCI port_status: 0x%x\n", ReadReg32(EHCI_REG_PORTSTS));

	// port router -- default routes all ports to this host controller 
	WriteReg32(EHCI_REG_CFGFLAG, FLAG_CF);
	debug_printf("EHCI cfg_flag: 0x%x\n", ReadReg32(EHCI_REG_CFGFLAG));

	//EHCI Rev Register
	debug_printf("EHCI + 0x0: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x0));
	debug_printf("EHCI + 0x4: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x4));
	debug_printf("EHCI + 0x8: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x8));
	debug_printf("EHCI + 0x40: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x40));

    // init OHCI ED
	WriteReg32(OHCI_REG_CONTROL, 0x0);//0x04
	WriteReg32(OHCI_REG_CTRLHEAD, 0x0);//0x20 -- physical address of the first ED of the control list
	WriteReg32(OHCI_REG_BULKHEAD, 0x0);//0x28 -- physical address of the first ED of the bulk list

    // reset ohci
	WriteReg32(OHCI_REG_CMDSTS, OHCI_HCR);//0x08

	WriteReg32(OHCI_REG_FMINTERVAL, DEFAULT_FMINTERVAL);//0x34 -- 0x27782EDF

	//set ohci to operational state
	WriteReg32(OHCI_REG_CONTROL, (ReadReg32(OHCI_REG_CONTROL) & ~(OHCI_CTRL_HCFS)) | OHCI_USB_OPER);//0x04
	WriteReg32(OHCI_REG_RHSTS, RH_HS_LPSC);//0x50

    // init hcca area
	for(i = 0; i< NUM_INTS; i++) {
		hcca->int_table[i] = 0;
	}
    hcca->frame_no = 0;
    hcca->done_head = 0;
	for(i = 0; i< 116; i++) {
		hcca->reserved_for_hc[i] = 0;
	}
	for(i = 0; i< 4; i++) {
		hcca->what[i] = 0;
	}

    WriteReg32(OHCI_REG_HCCA, (u32)hcca & ~0x80000000);//0x18   	

	// OHCI intr clear
	WriteReg32(OHCI_REG_INTRSTS, ReadReg32(OHCI_REG_INTRSTS));//0x0c  -- write '1' to clear interrupt

	// OHCI intr enable
	WriteReg32(OHCI_REG_INTREN, OHCI_INTR_MASK);

    //OHCI Rev Register
	debug_printf("OHCI 0x00: %x\n", ReadReg32(OHCI_REG_VER));
	debug_printf("OHCI 0x08: %x\n", ReadReg32(OHCI_REG_CMDSTS));
	debug_printf("OHCI 0x48: %x\n", ReadReg32(OHCI_REG_RHA));//root hub only has one downstream port
	debug_printf("OHCI 0x4c: %x\n", ReadReg32(OHCI_REG_RHB));
	debug_printf("OHCI 0x50: %x\n", ReadReg32(OHCI_REG_RHSTS));
	debug_printf("OHCI 0x54: %x\n", ReadReg32(OHCI_REG_RHPORTSTS));//root hub port power is on
	debug_printf("OHCI 0x34: %x\n", ReadReg32(OHCI_REG_FMINTERVAL));
	debug_printf("OHCI 0x3c: %x\n", ReadReg32(OHCI_BASE_ADDR + 0x3C));
}

volatile u32 g_ohci_status;
void ohci_irq(void)
{
	u32 status;

	status = ReadReg32(OHCI_REG_INTRSTS);
	if(status == ~(u32)0) {
		debug_printf("ohci dead.\n");
		return;
	}

	status &= OHCI_INTR_MASK;

	if(status != g_ohci_status) {
		g_ohci_status = status;
		debug_printf("OHCI intr_status: 0x%x\n", status);
	}

	//clear interrupts
	if(status & OHCI_INTR_WDH) {
		dl_done_list();
		WriteReg32(OHCI_REG_INTRSTS, OHCI_INTR_WDH);
	}

	if(status & OHCI_INTR_MIE) {
		debug_putc('E');
		debug_putc('I');
	}
}

int ohci_urb_enqueue(struct urb *urb)
{
	switch(usb_pipetype(urb->pipe)) {
	case PIPE_CONTROL:
		urb->td_head = td_list_build(urb);
		if(!urb->td_head) {
			debug_printf("create td list fail!\n");
			return -ENOMEM;
		}
		return submit_ctrl(urb);
	case PIPE_BULK:
		urb->td_head = td_list_build(urb);
		if(!urb->td_head) {
			debug_printf("create td list fail!\n");
			return -ENOMEM;
		}
		return submit_bulk(urb);
	case PIPE_INTERRUPT:
		urb->td_head = td_list_build(urb);
		if(!urb->td_head) {
			debug_printf("create td list fail!\n");
			return -ENOMEM;
		}
		return submit_periodic(urb);
	case PIPE_ISOCHRONOUS:
		urb->itd_head = td_list_build(urb);
		if(!urb->itd_head) {
			debug_printf("create itd list fail!\n");
			return -ENOMEM;
		}
		return submit_periodic(urb);
	default:
		debug_printf("pipe type error. pipe=0x%x\n", urb->pipe);
		return -EINVAL;
	}
	
	return 0;
}
