#include "includes.h"

void ehci_hw_init(void)
{
	int i;
	u32 *frame_list_ptr;

	debug_printf("ehci_hw_init\n");
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;
	if(!hcdrv) {
		debug_printf("ehci driver struct is NULL.\n");
		while(1);
	}
	
	// init periodic table 
	frame_list_ptr = (u32 *)hcdrv->periodic_list_addr;
	debug_printf("frame list: %x %x\n", frame_list_ptr, hcdrv->periodic_list_size);
	for(i=0; i<hcdrv->periodic_list_size; i++) {
		WriteReg32(frame_list_ptr+i, 0x00000001);//set terminate to '1'
	}
	
	debug_printf("frame list init ok\n");
	
	// reset echi
	WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_RESET);
	while((ReadReg32(EHCI_REG_CMD) & CMD_RESET) != 0);
	
	// EHCI Rev Registers
	debug_printf("EHCI + 0x0: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x0));
	debug_printf("EHCI + 0x4: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x4));
	debug_printf("EHCI + 0x8: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x8));
	debug_printf("EHCI + 0xc: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0xc));
	debug_printf("EHCI + 0x94: 0x%x\n", ReadReg32(EHCI_BASE_ADDR + 0x94));
	
	// assert port number == 1
	if(HCS_N_PORTS(ReadReg32(EHCI_REG_HCSPARAMS)) != 1) {
		debug_printf("port number != 1, %x\n", ReadReg32(EHCI_REG_HCSPARAMS));
		debug_printf("EHCI HCSPARAM: %x\n", HCS_N_PORTS(ReadReg32(EHCI_REG_HCSPARAMS)));
		while(1);
	}
	
	// config USB IP(ECHI) out buffer size 
	WriteReg32(EHCI_BASE_ADDR + 0x94, (ReadReg32(EHCI_BASE_ADDR + 0x94) & 0xffff) | 0x800080);
	// disable async park mode 
	//WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) & ~(CMD_PARK));
	
	switch(hcdrv->periodic_list_size) {
	case 1024:
		WriteReg32(EHCI_REG_CMD, (ReadReg32(EHCI_REG_CMD) & ~0xc));
		break;
	case 512:
		WriteReg32(EHCI_REG_CMD, (ReadReg32(EHCI_REG_CMD) & ~0xc) | 0x4);
		break;
	case 256:
		WriteReg32(EHCI_REG_CMD, (ReadReg32(EHCI_REG_CMD) & ~0xc) | 0x8);
		break;
	}
	
	// init async & periodic list
	if(hcdrv->periodic_list_addr & 0xfff) {
		debug_printf("periodic list base address must be 4k aligned! addr:0x%x\n", hcdrv->periodic_list_addr);
		while(1);
	}
	WriteReg32(EHCI_REG_ASYNCNEXT, (u32)hcdrv->async_head & 0x7fffffff);
	WriteReg32(EHCI_REG_FRAMELIST, hcdrv->periodic_list_addr & 0x7fffffff);
	
	// port power enable: 1 ports
	WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS) | PORT_POWER); 
	
	// enable ehci
	if((ReadReg32(EHCI_REG_CMD) & CMD_RUN) == 0) {
		WriteReg32(EHCI_REG_CMD, ReadReg32(EHCI_REG_CMD) | CMD_RUN);
	}
	
	/*
	* Start, enabling full USB 2.0 functionality ... usb 1.1 devices
	* are explicitly handed to companion controller(s), so no TT is
	* involved with the root hub.  (Except where one is integrated,
	* and there's no companion controller unless maybe for USB OTG.)
	*/
	WriteReg32(EHCI_REG_CFGFLAG, FLAG_CF);
	
	// enable ehci intr
	WriteReg32(EHCI_REG_INTREN, EHCI_INTR_MASK);
	debug_printf("reg status:0x%x, intr:0x%x\n", ReadReg32(EHCI_REG_STATUS), ReadReg32(EHCI_REG_INTREN));
	WriteReg32(EHCI_REG_STATUS, ReadReg32(EHCI_REG_STATUS));
	
	debug_printf("ehci cmd: %x\n", ReadReg32(EHCI_REG_CMD));
	debug_printf("status: 0x%x\n", ReadReg32(EHCI_REG_PORTSTS));
}

#define EHCI_PORT_POWEREN(x)  WriteReg32(EHCI_REG_PORTSTS, ReadReg32((EHCI_REG_PORTSTS + (x<<2)))|PORT_POWER) 
#define EHCI_PORT_RESET(x)   WriteReg32(EHCI_REG_PORTSTS, ReadReg32((EHCI_REG_PORTSTS + (x<<2)))|PORT_RESET) 
#define EHCI_PORT_RESETDIS(x)   WriteReg32(EHCI_REG_PORTSTS, ReadReg32((EHCI_REG_PORTSTS + (x<<2)))&~(PORT_RESET)) 
#define EHCI_PORT_POWERDIS(x)  WriteReg32(EHCI_REG_PORTSTS, ReadReg32((EHCI_REG_PORTSTS + (x<<2)))&~(PORT_POWER)) 

volatile int delay_test;
volatile int ohci_port_status;

int ehci_check_port_status(u8 port_index)
{
	volatile int status;
	
	if(++delay_test>100000) {
		debug_putc('Z');
		delay_test = 0;
	}

	// we just have one port
	status = ReadReg32(EHCI_REG_PORTSTS) & 0xfffff3ff;

	if(status & PORT_CSC) {
		WriteReg32(EHCI_REG_STATUS, STS_PCD);
		WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS) | PORT_CSC);//clear PORT_CSC flag
		{volatile int d; for(d=0; d<2000; d++); }
		
		if(status & PORT_CONNECT) { //device attach
			if((status & PORT_RESET) == 0) {
				if((status & PORT_PE) == PORT_PE) {
					return USB_ATTACH_DEVSPD_HIGH;
				}

				/* start 20 msec resume signaling from this port,
				* and make khubd collect PORT_STAT_C_SUSPEND to
				* stop that signaling.
				*/
				WriteReg32(EHCI_REG_STATUS, STS_FLR);

				//TODO: 0x0, enumeration ok, 0x333 => 0x40c failed.
				WriteReg32(EHCI_REG_FRAMEINDEX, (0x0<<3));
				{volatile int d; for(d=0; d<500; d++); }
				EHCI_PORT_RESET(0); 
				debug_printf("port 0[EHCI] reseting ...\n");
				g_libhost_cfg.udev.addr = 0;
				{volatile int d; for(d=0; d<200000; d++); }
				EHCI_PORT_RESETDIS(0); 
				debug_printf("port 0[EHCI] reset done\n");

				return USB_DEVSPD_UNKNOWN;
			}
		}else { // device detach
			if(!(status & PORT_OWNER)) {
				return USB_DETACH_DEVSPD_HIGH;
			}
		}
	}

	if((status & PORT_CONNECT) == PORT_CONNECT) {
		if((status & PORT_PE) == PORT_PE) {
			return USB_ATTACH_DEVSPD_HIGH;
		}else {
			WriteReg32(EHCI_REG_CFGFLAG, ReadReg32(EHCI_REG_CFGFLAG)& ~FLAG_CF); // route the control to companion hc
			WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS)|PORT_OWNER);// 1 means a companion hc
			debug_putc('-');
			debug_putc('F');

			status = ReadReg32(OHCI_REG_INTRSTS);//get hc interrupt status
			if(ohci_port_status != status) {
				debug_printf("ohci port: 0x%x %x\n", status, ReadReg32(OHCI_REG_INTREN));
				ohci_port_status = status;
			}
			if(status & OHCI_INTR_RHSC) {//root hub status change or root hub port status change
				WriteReg32(OHCI_REG_INTRSTS, OHCI_INTR_RHSC);//clear interrupt
				if(ReadReg32(OHCI_REG_RHPORTSTS) & RH_PS_CSC) {//connect status change
					WriteReg32(OHCI_REG_INTRSTS, RH_PS_CSC);//clear interrupt
					if(ReadReg32(OHCI_REG_RHPORTSTS) & RH_PS_CCS) {//current connect status -- '1' is device connneted
						WriteReg32(OHCI_REG_INTRSTS, RH_PS_CCS);//clear interrupt
						WriteReg32(OHCI_REG_RHPORTSTS, RH_PS_PRS);//set port reset
						debug_printf("reset OHCI port 0\n");
						while((ReadReg32(OHCI_REG_RHPORTSTS)&RH_PS_PRS));
						{volatile int d; for(d=0; d<3000000; d++); }
						WriteReg32(OHCI_REG_RHPORTSTS, RH_PS_PES);//set port enable
						debug_printf("fs usb dev is connected: %x\n", ReadReg32(OHCI_REG_RHPORTSTS));
						if(RH_PS_LSDA == (ReadReg32(OHCI_REG_RHPORTSTS)&RH_PS_LSDA)){
							return USB_ATTACH_DEVSPD_LOW;
						}
						else {
							return USB_ATTACH_DEVSPD_FULL;
						}
					} else {
						WriteReg32(EHCI_REG_CFGFLAG, FLAG_CF); // route the control to companion hc
						WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS)& ~PORT_OWNER);//'1' mean is a companion hc own and control port
						if(status & PORT_OWNER) {
							return USB_DETACH_DEVSPD_NOHIGH;
						}else {
							return USB_DETACH_DEVSPD_HIGH;
						}
					}
				}
		 	}
		}
	}

	if(ReadReg32(OHCI_REG_RHPORTSTS) & RH_PS_CSC) {//connect status change
		WriteReg32(OHCI_REG_INTRSTS, RH_PS_CSC);//clear interrupt
		if(!(ReadReg32(OHCI_REG_RHPORTSTS) & RH_PS_CCS)) {//current connect status -- '1' is device connneted
			WriteReg32(EHCI_REG_CFGFLAG, FLAG_CF); // route the control to companion hc
			WriteReg32(EHCI_REG_PORTSTS, ReadReg32(EHCI_REG_PORTSTS)& ~PORT_OWNER);//'1' mean is a companion hc own and control port
			if(status & PORT_OWNER) {
				return USB_DETACH_DEVSPD_NOHIGH;
			}else {
				return USB_DETACH_DEVSPD_HIGH;
			}
		}
	}

	if((ReadReg32(EHCI_REG_PORTSTS) & PORT_SUSPEND) == PORT_SUSPEND) {
		//debug_printf("a hs-speed device detach on port 0...\n");
		lowlevel_putc('-');
		lowlevel_putc('H');
		lowlevel_putc('S');
		return USB_DETACH_DEVSPD_HIGH;
	}

	return -1;
}

void ehci_irq(void)
{
    u32 status;

	status = ReadReg32(EHCI_REG_STATUS);
	if(status == ~(u32)0)	{
		debug_printf("ehci dead\n");
		return;
	}

	status &= EHCI_INTR_MASK;
	if(!status) {
		if(ReadReg32(EHCI_REG_STATUS) & STS_FLR) {//clear the first frame list rollover interrupt
			WriteReg32(EHCI_REG_STATUS, STS_FLR);
		}
		return;
	}
	debug_printf("EHCI intr_status:0x%x\n", status);

	//clear interrupts
	WriteReg32(EHCI_REG_STATUS, status);

	if(status & STS_VBUS) {
		debug_putc('E');
		debug_putc('I');
		debug_putc('7');
	}

	/* normal [4.15.1.2] completion */
	if(status & (STS_INT | STS_ERR)) {
		ehci_work();
	}

	/* AHB errors [4.15.2.4] */
	if(status & STS_FATAL) {
		debug_putc('E');
		debug_putc('I');
		debug_putc('4');
	}

	/* complete the unlinking of some qh [4.15.2.3] */
	if(status & STS_IAA) {
		debug_putc('E');
		debug_putc('I');
		debug_putc('5');
	}

	/* remote wakeup [4.3.1] */
	if(status & STS_PCD) {
		debug_putc('E');
		debug_putc('I');
		debug_putc('2');
	}
}

int ehci_urb_enqueue(struct urb *urb)
{
	switch(usb_pipetype(urb->pipe)) {
		case PIPE_CONTROL:
		case PIPE_BULK:
			urb->qtd_head = qtd_list_build(urb);
			if(!urb->qtd_head) {
				debug_printf("BC: create qtd list fail!\n");
				return -ENOMEM;
			}
			return submit_async(urb);
		case PIPE_INTERRUPT:
			urb->qtd_head = qtd_list_build(urb);
			if(!urb->qtd_head) {
				debug_printf("Intr: create qtd list fail!\n");
				return -ENOMEM;
			}
			return submit_intr(urb);
		case PIPE_ISOCHRONOUS:
			return submit_isoc(urb);
		default:
			debug_printf("pipe type error. pipe=0x%x\n", urb->pipe);
			return -EINVAL;
	}
}

