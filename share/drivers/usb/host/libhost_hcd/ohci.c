#include "includes.h"

static void td_list_free(struct ohci_td *td_head)
{
	struct ohci_td *cur;
	struct ohci_td *tmp;

	cur = td_head;
	while(cur) {
		if(cur->td_next) {
			tmp = cur;
			cur = cur->td_next;
			td_free(tmp);
		}else {
			td_free(cur);
			break;
		}
	}
}

//reverse done list tds
static struct ohci_td *dl_reverse_done_list(void)
{
	struct ohci_hcca *hcca = g_libhost_cfg.ohci_driver->ohci_hcd->hcca;
	struct ohci_td *td = NULL, *td_prev = NULL;

	td = (struct ohci_td *)le32_to_cpu(hcca->done_head);
	hcca->done_head = 0;

	while(td) {
		//debug_printf("reverse td: %x\n", td);
		td->next_dl_td = td_prev;
		td_prev = td;
		td = (struct ohci_td *)le32_to_cpu(td->hwNextTD);
	}

	return td_prev;
}

static void takeback_itd(struct ohci_itd *itd)
{
	u32 info;
	int cc = 0, i;
	struct urb *urb = itd->urb;

	info = le32_to_cpu(itd->hwINFO);
	cc = TD_CC_GET(info);
	if(cc == 0) {//td no error
		if(usb_pipein(urb->pipe)) {
			//urb->actual_length = 0;
		}else {
			urb->actual_length = urb->transfer_buffer_length;
		}
		for(i=0; i<MAXPSW; i++) {
			if((itd->hwPSW[i] & 0xf000)==0) {
				if(usb_pipein(urb->pipe)) {
					urb->actual_length += itd->hwPSW[i]&0x7ff;
				}else {
					urb->actual_length -= (itd->hwPSW[i]&0x7ff);
				}
			}else {
				debug_printf("itd frm[%x]: 0x%x\n", i, itd->hwPSW[i]);
			}
		}
		debug_printf("iso actual: %x\n", urb->actual_length);
		urb->itd_cnt--;
		if(urb->itd_cnt == 1) {
			urb->urb_status = URB_STATUS_COMPLETE;
			if(urb->urb_callback) {
				urb->urb_callback(urb);
			}
		}
	}else if(cc == 0xf) {//td not accessed
		debug_putc('c');
		return;
	}else {//itd error
		debug_printf("itd error:0x%x\n", cc);
		while(1);
	}
}

static void takeback_td(struct ohci_td *td)
{
	int cc = 0;
	u32 info;
	struct urb *urb = td->urb;

	info = le32_to_cpu(td->hwINFO);
	cc = TD_CC_GET(info);

	if(cc == 0) {//td no error
		//int type = usb_pipetype(urb->pipe);
		u32 pid = info & TD_DP;//data pid
		u32 tdBE = le32_to_cpu(td->hwBE);

		if((pid != TD_DP_SETUP) && (tdBE != 0)) {
			if(td->hwCBP == 0) {
				urb->actual_length += tdBE - td->data_dma + 1;
			}else {
				urb->actual_length += le32_to_cpu(td->hwCBP) - td->data_dma;
			}
			debug_printf("actual: %x\n", urb->actual_length);
		}

		urb->td_cnt--;
		if(urb->td_cnt == 1) {
			urb->urb_status = URB_STATUS_COMPLETE;
			if(urb->urb_callback) {
				urb->urb_callback(urb);
			}
			td_list_free(urb->td_head);
		}
	}else if(cc == 0xf) {//td not accessed
		debug_putc('c');
		return;
	}else {//td error
		debug_printf("td error:0x%x\n", cc);
	}
}

//process all done list tds
void dl_done_list(void)
{
	struct ohci_td *td = dl_reverse_done_list();
	struct ohci_itd *itd;
	struct urb *urb = td->urb;

	debug_printf("++ dl_done_list\n");
	//debug_printf("intri3 enable: 0x%x 0x%x 0x%x\n", ReadReg32(OHCI_REG_INTREN), ReadReg32(OHCI_REG_INTRSTS), ReadReg32(OHCI_REG_CONTROL));

	if(td == NULL) {
		debug_printf("dl_done_list: td = NULL\n");
		while(1);
	}

	if(usb_pipeisoc(urb->pipe)) {
		itd = td;
		urb->ep->toggle = itd->ed->hwHeadP & 0x2; 
		//while(itd) {
		{
			ohci_itd_debug(itd);
			takeback_itd(itd);
			//itd = itd->itd_next;
		}
	}else {
		urb->ep->toggle = td->ed->hwHeadP & 0x2; 
		while(td) {
			ohci_td_debug(td);
			takeback_td(td);
			td = td->next_dl_td;
		}
	}
}

static void itd_fill(struct urb *urb, struct ohci_itd *itd, u32 info, u32 dma_addr, u32 dma_size) 
{
	int i, len;

	itd->hwINFO = cpu_to_le32(info);
	itd->hwBP0 = (u32)(dma_addr & ~0x0000fff);
	itd->hwBE = ((u32)dma_addr & ~0x000000f) + dma_size - 1;
	for(i=0; i<MAXPSW; i++) {
		itd->hwPSW[i] = 0;
	}

	for(i=0, len=0; len<dma_size; ) {
		if(i & 0x1) {
			itd->hwPSW[i>>1] |= (((len & 0x7ff)+(dma_addr&0xfff)) | (0x7<<13))<<16;
		} else{
			itd->hwPSW[i>>1] = ((len & 0x7ff)+(dma_addr&0xfff)) | (0x7<<13);//(0x7<<13) defautl: set completion codes to Not Accessed
		}
		
		if((++i > 7) || (len >= 1024)) {
			break;
		}
		len = dma_size;

		debug_printf("i: %x, len: %x\n", i, dma_size);
	}
	if(i > 0) {
		itd->hwINFO = itd->hwINFO | ITD_FC_SET(i-1);
	}
	itd->urb = urb;
	urb->itd_cnt++;
	urb->itd_tail = itd;
}

static void td_fill(struct urb *urb, struct ohci_td *td, u32 info, u32 dma_addr, u32 dma_size) 
{
	td->hwINFO = cpu_to_le32(info);

	if(dma_size) {
		td->hwCBP = cpu_to_le32(dma_addr);
		td->hwBE = cpu_to_le32(dma_addr + dma_size - 1);
	}else {
		td->hwCBP = 0; 
		td->hwBE = 0;
	}

	td->data_dma = dma_addr;
	td->next_dl_td = NULL;
	td->td_state = TD_STATE_LINKED;
	td->urb = urb;
	urb->td_cnt++;
	urb->td_tail = td;
}

struct ohci_td *td_list_build(struct urb *urb)
{
#define OHCI_MAX_PACKET_LEN			4096
	struct ohci_td *td=NULL, *td_prev=NULL, *td_head=NULL;
	struct ohci_itd *itd=NULL, *itd_prev=NULL, *itd_head = NULL;

	u32 buf_addr = urb->transfer_dma;
	u32 buf_len = urb->transfer_buffer_length;
	u32 info = 0;

	switch(usb_pipetype(urb->pipe))	{
	case PIPE_CONTROL:
		td = td_alloc();//get first td
		if(!td)	{
	 		goto td_fail;
		}
		td_head = td;
		if(buf_len > OHCI_MAX_PACKET_LEN) {
			debug_printf("urb transfer length is overflow!\n, len:%d, max:%d\n", buf_len, OHCI_MAX_PACKET_LEN);
			goto td_fail;	
		}
		//setup packet
		info = TD_CC | TD_DP_SETUP | TD_T_DATA0 | TD_DI_SET(6);//only last td cause irq immediately, other tds need wait 6 frames
		td_fill(urb, td, info, urb->setup_dma, 8);

		debug_printf("setup data: %x %x\n", buf_addr, buf_len);
		if(buf_len) {
			td_prev = td;
			td = td_alloc();//data packet
			if(!td) goto td_fail;
			info = TD_CC | TD_R | TD_T_DATA1 | TD_DI_SET(6);
			info |= usb_pipein(urb->pipe) ? TD_DP_IN : TD_DP_OUT;
			td_fill(urb, td, info, buf_addr, buf_len);

			td_prev->hwNextTD = cpu_to_le32((u32)td);
			td_prev->td_next = td;
		}

		td_prev = td;
		td = td_alloc();//ack packet
		if(!td) goto td_fail;
		info = (usb_pipeout(urb->pipe) || (buf_len == 0))
				? (TD_CC | TD_DP_IN | TD_T_DATA1)
				: (TD_CC | TD_DP_OUT | TD_T_DATA1); 
		td_fill(urb, td, info, 0, 0);
		td_prev->hwNextTD = cpu_to_le32((u32)td);
		td_prev->td_next = td;

		td_prev = td;
		td = td_alloc();//dummy td
		if(!td) goto td_fail;
		td_fill(urb, td, 0, 0, 0);
		td_prev->hwNextTD = cpu_to_le32((u32)td);
		td_prev->td_next = td;
		break;
	case PIPE_INTERRUPT:
	case PIPE_BULK:
		td = td_alloc();//get first td
		if(!td)	{
	 		goto td_fail;
		}
		td_head = td;
		if(usb_pipeint(urb->pipe)) {
			debug_printf("intr data: %x %x\n", buf_addr, buf_len);
		}else {
			debug_printf("bulk data: %x %x\n", buf_addr, buf_len);
		}
		info = usb_pipein(urb->pipe)
				? (TD_CC | TD_R | TD_DP_IN | TD_T_TOGGLE)
				: (TD_CC | TD_R | TD_DP_OUT | TD_T_TOGGLE);
		while(buf_len > OHCI_MAX_PACKET_LEN) {
			td_fill(urb, td, info, buf_addr, OHCI_MAX_PACKET_LEN);
			buf_addr += OHCI_MAX_PACKET_LEN;
			buf_len -= OHCI_MAX_PACKET_LEN;

			td_prev = td;
			td = td_alloc();//next td
			if(!td) goto td_fail;
			td_prev->hwNextTD = cpu_to_le32((u32)td);
			td_prev->td_next = td;
		}
		td_fill(urb, td, info, buf_addr, buf_len);

		td_prev = td;
		td = td_alloc();//dummy td
		if(!td) goto td_fail;
		td_fill(urb, td, 0, 0, 0);
		td_prev->hwNextTD = cpu_to_le32((u32)td);
		td_prev->td_next = td;
		break;
	case PIPE_ISOCHRONOUS:
		itd = itd_alloc();//get first itd
		if(!itd)	{
	 		goto td_fail;
		}
		itd_head = itd;
		debug_printf("iso data: %x %x %x\n", buf_addr, buf_len, ReadReg32(OHCI_REG_FMNUMBER));
		urb->number_of_packets++;

		info = TD_CC; // ((ReadReg32(OHCI_REG_FMNUMBER) + 0xb0) & 0xffff);
		itd_fill(urb, itd, info, buf_addr, buf_len);

		itd_prev = itd;
		itd = itd_alloc();//get dummy itd
		if(!itd)	{
	 		goto td_fail;
		}
		itd_fill(urb, itd, 0, 0, 0);
		itd_prev->hwNextTD = cpu_to_le32((u32)itd);
		itd_prev->itd_next = itd;
		urb->number_of_packets++;

		td_head = itd_head;
		break;
	default: 
		debug_printf("error, unknown usb pipe type!\n");
		goto td_fail;
	}

	return td_head;

td_fail:
	td_list_free(td_head);
	return NULL;
}

static struct ohci_ed *ed_get(struct urb *urb)
{
	struct ohci_ed *ed = NULL;

	ed = ed_alloc();

	return ed;
}

static void ed_fill(struct urb *urb, struct ohci_ed *ed) 
{
	struct ohci_td *tmp;
	struct ohci_itd *itmp;
	u32 info = 0;

	//debug_printf("pipe: %x %x\n", urb->pipe, urb->ep->toggle);
	info = usb_pipedevice(urb->pipe);//append device address
	info |= usb_pipeendpoint(urb->pipe) << 7;//append endpoint number
	info |= urb->ep->ep_mps << 16;//append endpoint MaxPacketSize
	if(urb->udev->speed == USB_SPEED_LOW) {
		info |= ED_LOWSPEED;//append low speed
	}

	if(!usb_pipecontrol(urb->pipe)) {//control endpoint use TD's PIDs
		info |= usb_pipein(urb->pipe) ? ED_IN : ED_OUT;//append PID 
		if(usb_pipeisoc(urb->pipe)) {
			info |= ED_ISO;
		}
	}
	ed->hwINFO = cpu_to_le32(info);

	if(usb_pipeisoc(urb->pipe)) {
		ed->hwHeadP = urb->itd_head;
		ed->hwTailP = urb->itd_tail;

		for(itmp=urb->itd_head; itmp; ) {
			itmp->ed = ed;
			itmp = itmp->itd_next;
		}
		ed->interval = 2;
	}
	else {
		ed->hwHeadP = (int)(urb->td_head) + (int)(urb->ep->toggle);
		ed->hwTailP = urb->td_tail;

		for(tmp=urb->td_head; tmp; ) {
			tmp->ed = ed;
			tmp = tmp->td_next;
		}
		ed->interval = 4;
	}
	ed->ed_state = ED_STATE_IDLE;
}

static struct ohci_ed *ed_append_tds(struct urb *urb, void **ptr)
{
	struct ohci_ed *ed = NULL;

	ed = (struct ohci_ed *)(*ptr);
	if(ed == NULL) {
		ed = ed_get(urb);
		*ptr = ed;//save ed to ep->hcpriv
	}

	if(ed) {//add urb tds to ed td list
		ed_fill(urb, ed);
	}

	return ed;
}

int submit_ctrl(struct urb *urb)
{
	u32 flags;
	struct ohci_ed *ed;

	local_irq_save(flags);

	ed = ed_append_tds(urb, &urb->ep->hcpriv);
	if(!ed) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

	//////////////////////////////////////////
#if 0
	ohci_ed_debug(ed);
	struct ohci_td *td = urb->td_head;
	int i;
	while(td) {
		ohci_td_debug(td);
		td = td->td_next;
		if(++i>5){
			break;
		}
	}
#endif
	//////////////////////////////////////////

	if(ed->ed_state == ED_STATE_IDLE) {//add ed to control head
		ed->ed_state = ED_STATE_LINKED;

		WriteReg32(OHCI_REG_CTRLHEAD, (u32)ed & (~0x0000000f));
		WriteReg32(OHCI_REG_CMDSTS, ReadReg32(OHCI_REG_CMDSTS) | OHCI_CLF);//control list fill
		WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) | OHCI_CTRL_CLE);//control list enable
	}

	local_irq_restore(flags);
	return 0;
}

int submit_bulk(struct urb *urb)
{
	u32 flags;
	struct ohci_ed *ed;

	local_irq_save(flags);
	
	ed = ed_append_tds(urb, &urb->ep->hcpriv);
	if(!ed) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

#if 0 
	ohci_ed_debug(ed);
	struct ohci_td *td = urb->td_head;
	int i;
	while(td) {
		ohci_td_debug(td);
		td = td->td_next;
		if(++i>5){
			break;
		}
	}
#endif
	//////////////////////////////////////////
	if(ed->ed_state == ED_STATE_IDLE) {//add ed to bulk head
		ed->ed_state = ED_STATE_LINKED;

		WriteReg32(OHCI_REG_BULKHEAD, (u32)ed & (~0x0000000f));
		WriteReg32(OHCI_REG_CMDSTS, ReadReg32(OHCI_REG_CMDSTS) | OHCI_BLF);//bulk list fill
		WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) | OHCI_CTRL_BLE);//bulk list enable
	}

	local_irq_restore(flags);
	return 0;
}

static int ohci_periodic_link(t_ohci_ed_hdl ed)
{
	struct ohci_hcca *hcca = g_libhost_cfg.ohci_driver->ohci_hcd->hcca;
	struct ohci_ed *here;
	u32 *prev;
	int i;

	debug_printf("int table1: %x %x\n", hcca->int_table[0], hcca->int_table[16]);
	for(i = 0 ; i < NUM_INTS; i += ed->interval) {
		prev = &hcca->int_table[i];
		here = (t_ohci_ed_hdl)*prev;

		//search ed
		while(here && ed != here){
			lowlevel_putc('-');
			if(ed->interval > here->interval)
				break;
			if(here->hwNextED) {
				prev = &here->hwNextED; // & 0x7ffffff0);
				here = (t_ohci_ed_hdl )*prev;
			} else{
				break;
			}
		}

		//link ed to int table
		if(ed != here) {
			lowlevel_putc('+');
			debug_printf("%x ", prev - hcca->int_table);
			if(here) {
				ed->hwNextED = *prev;
			}
			*prev = (u32)ed & ~0x0000000f;
		}
	}
	debug_printf("\nint table2: %x %x\n", hcca->int_table[0], hcca->int_table[16]);
}

int submit_periodic(struct urb *urb)
{
	u32 flags;
	struct ohci_ed *ed;
	volatile int i;

	local_irq_save(flags);

	ed = ed_append_tds(urb, &urb->ep->hcpriv);
	if(!ed) {
		local_irq_restore(flags);
		return -ENOMEM;
	}

#if 0 
	ohci_ed_debug(ed);
	if(usb_pipeisoc(urb->pipe)) {
		struct ohci_itd *itd = urb->itd_head;
		while(itd) {
			ohci_itd_debug(itd);
			itd = itd->hwNextTD;
			if(++i>5){
				break;
			}
		}
	}else {
		struct ohci_td *td = urb->td_head;
		while(td) {
			ohci_td_debug(td);
			td = td->hwNextTD;
			if(++i>5){
				break;
			}
		}
	}
	debug_printf("ed interval: %x\n", ed->interval);
#endif

	//////////////////////////////////////////
	if(ed->ed_state == ED_STATE_IDLE) {
		ed->ed_state = ED_STATE_LINKED;
		switch(usb_pipetype(urb->pipe))	{
			case PIPE_INTERRUPT:
				WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) & ~OHCI_CTRL_PLE);//disable periodic list process
				break;
			case PIPE_ISOCHRONOUS:
				WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) & ~(OHCI_CTRL_IE | OHCI_CTRL_PLE));//disble iso and periodic list process
				break;
			default:
				debug_printf("pipe type error: %x\n", usb_pipetype(urb->pipe));
				while(1);
				break;
		}

		ohci_periodic_link(ed);
		switch(usb_pipetype(urb->pipe))	{
			case PIPE_INTERRUPT:
				WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) | OHCI_CTRL_PLE);//enable periodic list process
				break;
			case PIPE_ISOCHRONOUS:
				debug_printf("frm %x\n", ReadReg32(OHCI_REG_FMNUMBER));
				urb->itd_head->hwINFO |= ((ReadReg32(OHCI_REG_FMNUMBER) + 0x500) & 0xffff);
				WriteReg32(OHCI_REG_CONTROL, ReadReg32(OHCI_REG_CONTROL) | (OHCI_CTRL_IE | OHCI_CTRL_PLE));//enable iso and periodic list process
				break;
			default:
				debug_printf("pipe type error: %x\n", usb_pipetype(urb->pipe));
				while(1);
				break;
		}
	}
	local_irq_restore(flags);
	return 0;
}
