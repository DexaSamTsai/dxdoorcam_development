#include "includes.h"

int hosthcd_init(void)
{
	debug_printf("USB HOST init.\n");

	WriteReg32(REG_SC_ADDR + 0xbc, ReadReg32(REG_SC_ADDR + 0xbc)|(0x4<<10));
	WriteReg32(REG_SC_ADDR + 0xc0, ReadReg32(REG_SC_ADDR + 0xc0)|BIT28|BIT29);

	// realse AHB mbus reset
	WriteReg32(REG_SC_RST_LG0, ReadReg32(REG_SC_RST_LG0)& (~BIT3));

	//reset usb host and host phy
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)|BIT22|BIT23);

	{volatile int d; for(d = 0; d < 100; d++); }
	//release usb host and host phy
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)&(~(BIT22|BIT23)));

	debug_printf("USB HOST done.\n");

	ohci_driver_init();

	ohci_mem_init();
	ohci_hw_init();

	ehci_driver_init();

	ehci_mem_init();
	ehci_hw_init();

	return 0;
}

