#include "includes.h"


int is_hub_control(struct urb *urb)
{
	u16 typeReq;
	struct usb_ctrlrequest *ctrl_req;

	if((usb_pipecontrol(urb->pipe)) && (urb->setup_packet)) {
		ctrl_req = (struct usb_ctrlrequest *)(urb->setup_packet);
		typeReq = (ctrl_req->bRequestType << 8) | ctrl_req->bRequest;	

		switch(typeReq) {
		case ClearPortFeature:
		case GetPortStatus:
		case SetPortFeature:
			return TRUE;
		default:
			return FALSE;
		}
	}

	return FALSE;
}

static int clear_port_feature(struct usb_device *udev, int feature, int selector)
{
	int ret;

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_RT_PORT,
						  USB_REQ_CLEAR_FEATURE,
						  feature,
						  udev->port | (selector << 8),
						  NULL,
						  0);
	
	return ret;
}

static int set_port_feature(struct usb_device *udev, int feature, int selector)
{
	int ret;

	ret = usb_control_msg(udev,
						  usb_sndctrlpipe(udev, 0),
						  USB_RT_PORT,
						  USB_REQ_SET_FEATURE,
						  feature,
						  udev->port | (selector << 8),
						  NULL,
						  0);
	
	return ret;
}

static int get_port_status(struct usb_device *udev, struct usb_port_status *portst)
{
	int ret;
	
	ret = usb_control_msg(udev,
						  usb_rcvctrlpipe(udev, 0),
						  USB_DIR_IN | USB_RT_PORT,
						  USB_REQ_GET_STATUS,
						  0,
						  udev->port,
						  portst,
						  sizeof(*portst));

	return ret;
}

int usb_port_status(struct usb_device *udev, u16 *status, u16* change)
{
	int ret;
	struct usb_port_status portst;

	ret = get_port_status(udev, &portst); 
	if(ret) {//fail
		debug_printf("get port %d status fail. retval=%d\n", udev->port, ret);
		*status = 0;
		*change = 0;
	}else {
		*status = le16_to_cpu(portst.wPortStatus);
		*change = le16_to_cpu(portst.wPortChange);
	}

	return ret;
}

int usb_port_suspend(struct usb_device *udev)
{
	int ret;

	WriteReg32(REG_SC_ADDR + 0xc0, ReadReg32(REG_SC_ADDR + 0xc0) | BIT12);
	ret = set_port_feature(udev, USB_PORT_FEAT_SUSPEND, 0); 
	if(!ret) {//success
		//device has up to 10ms to fully suspend
		ertos_timedelay(2);
		WriteReg32(REG_SC_ADDR + 0xc0, ReadReg32(REG_SC_ADDR + 0xc0) & ~BIT12);
		debug_printf("port %d set suspend success!\n", udev->port);
	}else {
		debug_printf("can't suspend port %d, retval=%d\n", udev->port, ret);
	}

	return ret;
}

//app shall use this routine to control port resume, not direct use clear_port_feature
int usb_port_resume(struct usb_device *udev)
{
	int ret;
	u16 portstatus, portchange;

	ret = clear_port_feature(udev, USB_PORT_FEAT_SUSPEND, 0);
	if(ret) {//fail
		debug_printf("can't resume port %d, retval=%d\n", udev->port, ret);
	}else {
		//drive resume for at least 20ms
		debug_printf("usb port %d resume.\n", udev->port);
		ertos_timedelay(2);

		//send USB_REQ_GET_STATUS to release resume signal 
		ret = usb_port_status(udev, &portstatus, &portchange);
		{volatile int dd; for(dd=0; dd<200000; dd++);}
		ertos_timedelay(2);
	}
	
	return ret;
}

//app shall use this routine to control port reset, not direct use set_port_feature
int usb_port_reset(struct usb_device *udev)
{
	int i, ret;
	u16 portstatus, portchange;

	ret = set_port_feature(udev, USB_PORT_FEAT_RESET, 0);
	if(ret) {//fail
		debug_printf("can't reset port %d, retval=%d\n", udev->port, ret);
	}else {
		debug_printf("port %d be reset.\n", udev->port);
		ertos_timedelay(2);

		//send USB_RET_GET_STATUS to release reset signal
		ret = usb_port_status(udev, &portstatus, &portchange);
		ertos_timedelay(2);

		udev->addr = 0;
	}

	return ret;
}

int usb_port_testmode(struct usb_device *udev, int selector)
{
	int ret;

	if((!selector) || (selector >5)) {
		debug_printf("you selected test item cannot be supported! selector=%d\n", selector);
		return -1;
	}

	ret = set_port_feature(udev, USB_PORT_FEAT_TEST, selector);
	if(ret) {//fail
		debug_printf("can't reset port %d, retval=%d\n", udev->port, ret);
	}

	return ret;
}

int usb_port_testmode_exit(struct usb_device *udev, int selector)
{
	int ret;

	ret = clear_port_feature(udev, USB_PORT_FEAT_TEST, selector);
	if(ret) {//fail
		debug_printf("clear usb test mode fail.\n");
	}

	return ret;
}

