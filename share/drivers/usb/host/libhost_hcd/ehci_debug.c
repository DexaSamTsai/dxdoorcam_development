#include "includes.h"

#define ehci_debug	debug_printf

void ehci_qh_debug(struct ehci_qh *qh)
{
	ehci_debug("-----------QUEUE HEAD-----------\n");
    ehci_debug("qh addr: 0x%x\n", qh);
	ehci_debug("hw_next: 0x%x\n", cpu_to_le32(qh->hw_next));
	ehci_debug("hw_info1: 0x%x\n", cpu_to_le32(qh->hw_info1));
	ehci_debug("hw_info2: 0x%x\n", cpu_to_le32(qh->hw_info2));
	ehci_debug("current: 0x%x\n", cpu_to_le32(qh->hw_current));
	ehci_debug("hw_qtd_next: 0x%x\n", cpu_to_le32(qh->hw_qtd_next));
	ehci_debug("hw_alt_next: 0x%x\n", cpu_to_le32(qh->hw_alt_next));
	ehci_debug("hw_token: 0x%x\n", cpu_to_le32(qh->hw_token));
	ehci_debug("buf ptr0: 0x%x\n", cpu_to_le32(qh->hw_buf[0]));
	ehci_debug("buf ptr1: 0x%x\n", cpu_to_le32(qh->hw_buf[1]));
	ehci_debug("buf ptr2: 0x%x\n", cpu_to_le32(qh->hw_buf[2]));
	ehci_debug("buf ptr3: 0x%x\n", cpu_to_le32(qh->hw_buf[3]));
	ehci_debug("buf ptr4: 0x%x\n", cpu_to_le32(qh->hw_buf[4]));
	ehci_debug("===============================\n");
}

void ehci_qtd_debug(struct ehci_qtd *qtd)
{
	ehci_debug("-------------qTD--------------\n");
	ehci_debug("qtd addr: 0x%x\n", qtd);
	ehci_debug("hw_next = 0x%x\n", cpu_to_le32(qtd->hw_next));
	ehci_debug("alt_next = 0x%x\n", cpu_to_le32(qtd->hw_alt_next));
	ehci_debug("hw_token = 0x%x\n", cpu_to_le32(qtd->hw_token));
	ehci_debug("buf ptr0 = 0x%x\n", cpu_to_le32(qtd->hw_buf[0]));
	ehci_debug("buf ptr1 = 0x%x\n", cpu_to_le32(qtd->hw_buf[1]));
	ehci_debug("buf ptr2 = 0x%x\n", cpu_to_le32(qtd->hw_buf[2]));
	ehci_debug("buf ptr3 = 0x%x\n", cpu_to_le32(qtd->hw_buf[3]));
	ehci_debug("buf ptr4 = 0x%x\n", cpu_to_le32(qtd->hw_buf[4]));
	ehci_debug("==============================\n");
}

void ehci_itd_debug(struct ehci_itd *itd)
{
	ehci_debug("-------------iTD--------------\n");
	ehci_debug("itd addr: 0x%x\n", itd);
	ehci_debug("hw_next = 0x%x\n", cpu_to_le32(itd->hw_next));
	ehci_debug("hw_transaction0 = 0x%x\n", cpu_to_le32(itd->hw_transaction[0]));
	ehci_debug("hw_transaction1 = 0x%x\n", cpu_to_le32(itd->hw_transaction[1]));
	ehci_debug("hw_transaction2 = 0x%x\n", cpu_to_le32(itd->hw_transaction[2]));
	ehci_debug("hw_transaction3 = 0x%x\n", cpu_to_le32(itd->hw_transaction[3]));
	ehci_debug("hw_transaction4 = 0x%x\n", cpu_to_le32(itd->hw_transaction[4]));
	ehci_debug("hw_transaction5 = 0x%x\n", cpu_to_le32(itd->hw_transaction[5]));
	ehci_debug("hw_transaction6 = 0x%x\n", cpu_to_le32(itd->hw_transaction[6]));
	ehci_debug("hw_transaction7 = 0x%x\n", cpu_to_le32(itd->hw_transaction[7]));
	ehci_debug("hw_bufp0 = 0x%x\n", cpu_to_le32(itd->hw_bufp[0]));
	ehci_debug("hw_bufp1 = 0x%x\n", cpu_to_le32(itd->hw_bufp[1]));
	ehci_debug("hw_bufp2 = 0x%x\n", cpu_to_le32(itd->hw_bufp[2]));
	ehci_debug("hw_bufp3 = 0x%x\n", cpu_to_le32(itd->hw_bufp[3]));
	ehci_debug("hw_bufp4 = 0x%x\n", cpu_to_le32(itd->hw_bufp[4]));
	ehci_debug("hw_bufp5 = 0x%x\n", cpu_to_le32(itd->hw_bufp[5]));
	ehci_debug("hw_bufp6 = 0x%x\n", cpu_to_le32(itd->hw_bufp[6]));
	ehci_debug("==============================\n");
}


