#include "includes.h"

t_libhost_cfg g_libhost_cfg;

struct usb_host_endpoint ep0_in_cfg = {
	.ep_num = 0,
	.ep_type = USB_ENDPOINT_XFER_CONTROL,
	.ep_mps = 0x40,
};

struct usb_host_endpoint ep0_out_cfg = {
	.ep_num = 0,
	.ep_type = USB_ENDPOINT_XFER_CONTROL,
	.ep_mps = 0x40,
};

int libhost_init(void)
{
	memset(&g_libhost_cfg, 0x00, sizeof(g_libhost_cfg));

	hostclass_init();
	hosthcd_init();

	usb_urb_pool_init();

	g_libhost_cfg.udev.port = 1;//robin chip has only one port
	g_libhost_cfg.udev.state = USB_STATE_NOTATTACHED;
	g_libhost_cfg.udev.speed = USB_SPEED_UNKNOWN;

	//ep0 parameter information
	g_libhost_cfg.udev.ep0_in = &ep0_in_cfg;
	g_libhost_cfg.udev.ep0_out = &ep0_out_cfg;

	g_libhost_cfg.endian = BIG_ENDIAN_TYPE;
	
	return 0;
}

