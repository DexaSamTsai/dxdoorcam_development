#include "includes.h"


void qtd_init(struct ehci_qtd *qtd)
{
	memset(qtd, 0, sizeof(struct ehci_qtd));

	qtd->hw_token = le32_to_cpu(QTD_STS_HALT);
	qtd->hw_next = le32_to_cpu(0x00000001);
	qtd->hw_alt_next = le32_to_cpu(0x00000001);
	
	qtd->urb = NULL;
	qtd->qtd_next = NULL;
	qtd->qtd_state = QTD_STATE_IDLE;
}

struct ehci_qtd *qtd_alloc(void)
{
	struct ehci_qtd *qtd;
	struct __ehci_hw_pool *pool = &g_libhost_cfg.ehci_driver->ehci_pool;
	u8 i;

	for(i=0; i<pool->qtd_pool->cnt; i++) {
		if(pool->qtd_pool->index >= pool->qtd_pool->cnt) {
			pool->qtd_pool->index = 0;
		}
		qtd = (struct ehci_qtd *)(pool->qtd_pool->base) + pool->qtd_pool->index;
		pool->qtd_pool->index++;
		if(qtd->qtd_state != QTD_STATE_LINKED) {
			qtd_init(qtd);

			return qtd;
		}
	}

	if(i == pool->qtd_pool->cnt) {
		debug_printf("qtd alloc fail. cnt=%d\n", i);
	}

	return NULL;
}

void qtd_free(struct ehci_qtd *qtd)
{
	if(!qtd) {
		debug_printf("qtd is NULL, cannot be free!\n");
		return;
	}

	qtd->qtd_state = QTD_STATE_UNKNOWN;
}

struct ehci_qh *qh_alloc(void)
{
	struct ehci_qh *qh;
	struct ehci_qtd *qtd_terminate;
	struct __ehci_hw_pool *pool = &g_libhost_cfg.ehci_driver->ehci_pool;

	if(pool->qh_pool->index >= pool->qh_pool->cnt) {
		goto qh_fail;
	}
	qh = (struct ehci_qh *)(pool->qh_pool->base) + pool->qh_pool->index;
	pool->qh_pool->index++;

	qtd_terminate = qtd_alloc();
	if(!qtd_terminate) {
		debug_printf("qh alloc terminate qtd fail.\n");
		qh_free(qh);
		return NULL;
	}

	qtd_terminate->qtd_state = QTD_STATE_LINKED;
	qh->qtd_head = qtd_terminate;
	qh->qtd_tail = qtd_terminate;

	return qh;

qh_fail:
	debug_printf("qh alloc fail.\n");
	return NULL;
}

void qh_free(struct ehci_qh *qh)
{
	if(!qh) {
		debug_printf("qh is NULL, cannot be free!\n");
		return;
	}

	qh->qh_state = QH_STATE_UNKNOWN;
}

static void itd_init(struct ehci_itd *itd)
{
	memset(itd, 0, sizeof(struct ehci_itd));

	itd->hw_next = le32_to_cpu(QH_TYPE_ITD | QH_TERMINATE);

	itd->itd_state = ITD_STATE_IDLE;
}

struct ehci_itd *itd_alloc(void)
{
	struct ehci_itd *itd;
	struct __ehci_hw_pool *pool = &g_libhost_cfg.ehci_driver->ehci_pool;
	u8 i;

	for(i=0; i<pool->itd_pool->cnt; i++) {
		if(pool->itd_pool->index >= pool->itd_pool->cnt) {
			pool->itd_pool->index = 0;
		}
		itd = (struct ehci_itd *)(pool->itd_pool->base) + pool->itd_pool->index;
		pool->itd_pool->index++;

		if(itd->itd_state != ITD_STATE_LINKED) {
			itd_init(itd);

			return itd;
		}
	}

	if(i == pool->itd_pool->cnt) {
		debug_printf("itd alloc fail. cnt=%d\n", i);
	}

	return NULL;
}

void itd_free(struct ehci_itd *itd)
{
	if(!itd) {
		debug_printf("itd is NULL, cannot be free!\n");
		return;
	}

	itd->itd_state = ITD_STATE_UNKNOWN;
}

static int ehci_hcd_dma_init(struct hc_driver *hcdrv)
{
	u8 i, index;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<hcdrv->hcd_dma_cnt; i++) {
		dma = hcdrv->hcd_dmas + i;
		pool = &(dma->dma_pool);
		if(pool->cnt > MAX_ELEMENT_NUM_OF_POOL) {
			debug_printf("dma pool count is overflow!\n");
			return -ENOMEM;
		}

		pool->index = 0;
		for(index=0; index<pool->cnt; index++) {
			dma->dma_element[index].dma_addr = pool->base + (pool->step * index);
			dma->dma_element[index].dma_state = DMA_STATE_IDLE;
		}
	}

	return 0;
}

u32 ehci_dma_alloc(u32 len)
{
	u8 i, j;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<g_libhost_cfg.ehci_driver->hcd_dma_cnt; i++) {
		dma = g_libhost_cfg.ehci_driver->hcd_dmas + i;
		pool = &(dma->dma_pool);
		if(len < pool->step) {
			for(j=0; j<pool->cnt; j++) {
				if(dma->dma_element[j].dma_state == DMA_STATE_IDLE) {
					dma->dma_element[j].dma_state = DMA_STATE_USED;
					return dma->dma_element[j].dma_addr;
				}
			}
		}
	}

	return 0;
}

void ehci_dma_free(u32 dma_addr)
{
	u8 i, j;
	struct __hcd_dma *dma;
	struct __hw_pool *pool;

	for(i=0; i<g_libhost_cfg.ehci_driver->hcd_dma_cnt; i++) {
		dma = g_libhost_cfg.ehci_driver->hcd_dmas + i;
		pool = &(dma->dma_pool);
		for(j=0; j<pool->cnt; j++) {
			if(dma_addr == dma->dma_element[j].dma_addr) {
				dma->dma_element[j].dma_state = DMA_STATE_IDLE;
			}
		}
	}
}

static void dummy_head_init(struct hc_driver *hcdrv)
{
	if(hcdrv) {
		//create dummy async qh head
		struct ehci_qh *dummy_qh = qh_alloc();
		if(!dummy_qh) {
			debug_printf("alloc dummy_qh async qh fail.\n");
			return;
		}
		dummy_qh->qh_next = NULL;//for async software circle queue
		dummy_qh->hw_next = le32_to_cpu((u32)dummy_qh);//for hc circle queue
		dummy_qh->hw_info1 = le32_to_cpu(QH_HEAD);
		dummy_qh->hw_token = le32_to_cpu(QTD_STS_HALT);
		dummy_qh->hw_qtd_next = le32_to_cpu(0x00000001);
		dummy_qh->hw_alt_next = le32_to_cpu(0x00000001);
		dummy_qh->qh_state = QH_STATE_LINKED;
		hcdrv->async_head = dummy_qh; 
//		ehci_qh_debug(hcdrv->async_head);
//		ehci_qtd_debug(hcdrv->async_head->qtd_head);

		//create dummy interrupt qh head
		dummy_qh = qh_alloc();
		if(!dummy_qh) {
			debug_printf("alloc dummy_qh interrupt qh fail.\n");
			return;
		}
		dummy_qh->qh_next = NULL;//for interrupt software circle queue
		dummy_qh->hw_next = le32_to_cpu(0x00000001);
		dummy_qh->hw_info1 = le32_to_cpu(QH_HEAD);
		dummy_qh->hw_token = le32_to_cpu(QTD_STS_HALT);
		dummy_qh->hw_qtd_next = le32_to_cpu(0x00000001);
		dummy_qh->hw_alt_next = le32_to_cpu(0x00000001);
		dummy_qh->qh_state = QH_STATE_LINKED;
		hcdrv->intr_head = dummy_qh; 
//		ehci_qh_debug(hcdrv->intr_head);
//		ehci_qtd_debug(hcdrv->intr_head->qtd_head);

		//create dummy itd head
		struct ehci_itd *dummy_itd = itd_alloc();
		if(!dummy_itd) {
			debug_printf("alloc periodic dummy itd fail.\n");
			return;
		}
		dummy_itd->itd_next = NULL;//for isoc software circle queue
		dummy_itd->itd_state = ITD_STATE_LINKED;
		hcdrv->itd_head = dummy_itd;
	}
}

void ehci_mem_init(void)
{
	struct hc_driver *hcdrv = g_libhost_cfg.ehci_driver;
	u8 i;

	if(hcdrv) {
		dummy_head_init(hcdrv);

		ehci_hcd_dma_init(hcdrv);

		struct __hw_pool *pool = hcdrv->ehci_pool.qh_pool;
		debug_printf("QH --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		pool = hcdrv->ehci_pool.qtd_pool;
		debug_printf("QTD --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		pool = hcdrv->ehci_pool.itd_pool;
		debug_printf("ITD --base:0x%x, size:%d, num:%d, total_size:0x%x\n", pool->base, pool->step, pool->cnt, (pool->step * pool->cnt));

		debug_printf("DMA cnt:%d\n", hcdrv->hcd_dma_cnt);
		for(i=0; i<hcdrv->hcd_dma_cnt; i++) {
			pool = &(hcdrv->hcd_dmas[i].dma_pool);
			debug_printf("EHCI dma%d --base:0x%x, num:%d, total_size:0x%x\n", pool->step, pool->base, pool->cnt, (pool->step * pool->cnt));
		}
	}
}

