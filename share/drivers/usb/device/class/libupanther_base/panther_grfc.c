#include "includes.h"

#ifdef CONFIG_UPANTHER_HW
#define UPANTHER_BUFFER    CONFIG_UPANTHER_HW_MEMBASE 

void udcif_grfc_cfg(int addr, int length);
int upanther_camera_convert(u8 *buffer);

int upanther_grfc_cb(void)
{
	int length;

	length = upanther_camera_convert(UPANTHER_BUFFER);
	if(length) {
		length = (length+255-UVC_YUV_PLHEADER_SIZE)&(~0xff);
		//debug_printf("upanther grfc cb: %x\n", length);
		udcif_grfc_cfg(UPANTHER_BUFFER - MEMBASE_DDR, length);
	}

	return 1;
}

int upanther_grfc_init(void)
{
	debug_printf("@upanther grfc init\n");

	DISABLE_VIDEO_EP;
	{volatile int d; for(d=0; d<10; d++); }
	WriteReg32(UDCIF_VIDEO_CFG, (ReadReg32(UDCIF_VIDEO_CFG) & 0xffff3fff) | 0x4000);//app mod for uvc raw
	ENABLE_VIDEO_EP;
	udcif_grfc_cfg(UPANTHER_BUFFER - MEMBASE_DDR, 1024);

	return 1;
}
#endif
