#include "includes.h"

#define PANTHER_CMD_SYS                 0x02
#define PANTHER_CMD_I2C16               0x05
#define PANTHER_CMD_I2C08               0x04
#define PANTHER_CMD_I2C_ID              0x42

int panther_stage;
u8 panther_f1_i2c = 0x20;
u16 panther_i2c_addr;
u8 panther_i2c_data;

void upanther_bulk_in(void)
{
	//debug_printf("++ bulk in\n");
#if 1
	g_libuprinter_cfg->tx_len = g_libuprinter_cfg->read(&g_libuprinter_cfg->tx_buffer, SIZE_2K);
#else
	g_libuprinter_cfg->tx_len = 0x800;
#endif

	udc_writeEP(g_libuprinter_cfg->ep_in, g_libuprinter_cfg->tx_buffer, g_libuprinter_cfg->tx_len);
	ENABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
}

void upanther_bulk_out(void)
{
	u32 len;

	len = udc_readEP(g_libuprinter_cfg->ep_out, g_libuprinter_cfg->rx_buffer);
	if(len) {
		debug_printf("rx len: %x\n", len);
	}
}

void usbclass_HandleSetif(int if_id, int alt)
{

}

static u8 sir_id;
static u8 sir_data_bytes;
static u32 sir_addr;
u32 app_reg_wr(u8 sir_id, u32 sir_addr, u32 sir_data);
u32 app_reg_rd(u8 sir_id, u32 sir_addr, u8 sir_data_bytes);

int usbclass_HandleRequest(void)
{
	if((g_libusb_cfg.req.request.bmRequestType & 0x60) != 0x40){
		//not vendor req
		return -1;
	}

//	debug_printf("Type:%x,Req:%x,V_H:%x,V_L:%x,I_H:%x,I_L:%x,L:%x\n", g_libusb_cfg.req.request.bmRequestType, g_libusb_cfg.req.request.bRequest, g_libusb_cfg.req.request.wValue_H,g_libusb_cfg.req.request.wValue_L,g_libusb_cfg.req.request.wIndex_H, g_libusb_cfg.req.request.wIndex_L, g_libusb_cfg.req.wLength);
			if(g_libusb_cfg.req.wLength){
//				debug_printf("DATA:");
				//int ii; for(ii=0; ii<g_libusb_cfg.req.wLength; ii++){ debug_printf("%x ", g_libusb_cfg.req.databuf[ii]); };
//				debug_printf("\n");
			}

	if(g_libusb_cfg.req.wLength < 9){
		debug_printf("ERROR: wLength < 9\n");
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		return 1;
	}

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;

	if(g_libusb_cfg.req.request.bmRequestType == 0x41) {
		//write
		u8 wcmd = g_libusb_cfg.req.databuf[5];
		sir_id = g_libusb_cfg.req.databuf[0];
		if(sir_id == 0x02){
			if(g_libusb_cfg.req.databuf[2] == 0x8d){
				debug_printf("start video\n");
				ENABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
			}
		}
		sir_addr = (g_libusb_cfg.req.databuf[1] << 24) | (g_libusb_cfg.req.databuf[2] << 16) | (g_libusb_cfg.req.databuf[3] << 8) | (g_libusb_cfg.req.databuf[4]);
		sir_data_bytes = ((wcmd >> 1) & 0x7);
		if(sir_data_bytes!=1 && sir_data_bytes!=2 && sir_data_bytes!=4){
			debug_printf("ERROR: read data count not correct:%x,%x\n", wcmd, sir_data_bytes);
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			return 1;
		}
		if(sir_id != 0x48){
			if(sir_data_bytes != 1){
				debug_printf("ERROR: sccb data count not 1:%x,%x\n", wcmd, sir_data_bytes);
				g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
				return 1;
			}
		}
		
		if(wcmd & 1){
			//write data
			u32 sir_data;
#ifdef CONFIG_IPTEDITOR_LITTLE_ENDIAN
                        if(sir_data_bytes == 1){
                                sir_data = g_libusb_cfg.req.databuf[9];
                        }else if(sir_data_bytes == 2){
                                sir_data = (g_libusb_cfg.req.databuf[9]) | (g_libusb_cfg.req.databuf[10] << 8);
                        }else if(sir_data_bytes == 4){
                                sir_data = (g_libusb_cfg.req.databuf[9]) | (g_libusb_cfg.req.databuf[10] << 8) | (g_libusb_cfg.req.databuf[11] << 16) | (g_libusb_cfg.req.databuf[12]<<24);
#else
			if(sir_data_bytes == 1){
				sir_data = g_libusb_cfg.req.databuf[9];
			}else if(sir_data_bytes == 2){
				sir_data = (g_libusb_cfg.req.databuf[9] << 8) | (g_libusb_cfg.req.databuf[10]);
			}else if(sir_data_bytes == 4){
				sir_data = (g_libusb_cfg.req.databuf[9] << 24) | (g_libusb_cfg.req.databuf[10] << 16) | (g_libusb_cfg.req.databuf[11] << 8) | (g_libusb_cfg.req.databuf[12]);
#endif
			}else{
				debug_printf("ERROR: write data count not correct:%x,%x\n", wcmd, sir_data_bytes);
				g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
				return 1;
			}
			app_reg_wr(sir_id, sir_addr, sir_data);
		}else{
			//write addr for read
		}
	}else if(g_libusb_cfg.req.request.bmRequestType == 0xc1) {
		memset(g_libusb_cfg.req.databuf, 0, g_libusb_cfg.req.wLength);
		u32 rval = app_reg_rd(sir_id, sir_addr, sir_data_bytes);
		g_libusb_cfg.req.databuf[1] = rval & 0xff;
		g_libusb_cfg.req.databuf[2] = (rval >> 8) & 0xff;
		g_libusb_cfg.req.databuf[3] = (rval >> 16) & 0xff;
		g_libusb_cfg.req.databuf[4] = (rval >> 24) & 0xff;
		g_libusb_cfg.req.wLength -= 8;
	}else {
		debug_printf("class request err: %x\n", g_libusb_cfg.req.request.bmRequestType);
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
	}

	return 1;
}

void usb_panther_init(struct s_libuprinter_cfg *cfg)
{
	debug_printf("++usb_panther_init\n");
	panther_stage = 0;
}

void usb_panther_handle_bulk(u32 *args)
{
    u8 ep_in, ep_out;	

	ep_in  = (g_libuprinter_cfg->ep_in) & EPNUM_MASK;
	ep_out = (g_libuprinter_cfg->ep_out) & EPNUM_MASK; 

	//if(args & MASK_OF_EPINTR(ep_in))  //EP in 
	{ 
		if(EPBUF_IS_EMPTY(ep_in))
		{
			*args = *args & ~(MASK_OF_EPINTR(ep_in));
			upanther_bulk_in();
		}
	}

	if(*args & MASK_OF_EPINTR(ep_out)) //EP out
	{
		if(EPBUF_IS_NOTEMPTY(ep_out))  
		{
			*args = *args & ~(MASK_OF_EPINTR(ep_out));
			upanther_bulk_out();
		}
	}
}

/* this func will be called before USB start, for init some class-specific data */
int usbclass_init(int first)
{
	usb_panther_init(NULL);
	return 0;
}

void usbclass_clean(void)
{
	DISABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
}
