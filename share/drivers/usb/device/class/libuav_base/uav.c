#include "includes.h"

void usbclass_HandleSetif(int if_id, int alt)
{
	int i,k;

	for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			for(i=0; i<g_libuav_cfg->iad[k].vstrmif_cnt; i++) {
				if(if_id == g_libuav_cfg->iad[k].vstrmif[i].STRMIF_ID) {
					uvc_setif(g_libuav_cfg->iad[k].vstrmif + i, alt);
				}
			}
		}
		else if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
			for(i=0; i<g_libuav_cfg->iad[k].astrmif_cnt; i++) {
				if(if_id == g_libuav_cfg->iad[k].astrmif[i].STRMIF_ID) {
					if(g_libuav_cfg->iad[k].astrmif[i].setif) {
						g_libuav_cfg->iad[k].astrmif[i].setif(g_libuav_cfg->iad[k].astrmif + i, alt);
					}
				}
			}
		}
	}
}

extern int uac_HandleRequest(void);
extern int uvc_HandleRequest(void);
int usbclass_HandleRequest(void)
{
	int ret;

	ret = uvc_HandleRequest();
	if(ret != -1) 
		return ret;

	return uac_HandleRequest();
}

void usbclass_HandleUAV(u32 *args)
{
	int i, k;
	int ep;

	for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			for(i=0; i<g_libuav_cfg->iad[k].vstrmif_cnt; i++){
				ep = g_libuav_cfg->iad[k].vstrmif[i].STRM_EP & 0xf;
				if((*args & MASK_OF_EPINTR(ep))&& EPBUF_IS_EMPTY(ep))	{
					*args = *args & ~(MASK_OF_EPINTR(ep));
					if(g_libuav_cfg->iad[k].vstrmif[i].encode){
						uvc_filldata(g_libuav_cfg->iad[k].vstrmif + i);
					}
				}
			}
		}

		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
			for(i=0; i<g_libuav_cfg->iad[k].astrmif_cnt; i++){
				ep = g_libuav_cfg->iad[k].astrmif[i].STRM_EP & 0xf;
				if((*args & MASK_OF_EPINTR(ep))&& EPBUF_IS_EMPTY(ep))	{
					*args = *args & ~(MASK_OF_EPINTR(ep));
					if(g_libuav_cfg->iad[k].astrmif[i].filldata){
						g_libuav_cfg->iad[k].astrmif[i].filldata(g_libuav_cfg->iad[k].astrmif + i);
					}
				}
			}
		}
	}
}

static void uvc_gen_pucontrol(u8 *buf, u16 offset)
{
	if(offset == 0) return;

	u8 len = buf[offset];
	if(len == 0) return;

	int i;
	u32 map = 0;

#if 0
	t_sensoreffect_one *tso;
	for(i=0; i<18; i++){ //now max 18bits PU bmControl
		tso = sensor_effect_getinfo(i);
		if(tso != NULL){
			map |= (0x1 << i);
		}
	}
#endif
	debug_printf("PU bmControl: %x\n", map);

	for(i=0; i<len; i++){
		buf[offset + i + 1] = ((map >> (i*8) ) & 0xff) ;
	}
}

/* this func will be called before USB start, for init some class-specific data */
int usbclass_init(int first)
{
	t_libuav_cfg *cfg;
	t_libvmsc_cfg *vmsc_cfg;
	u8 k;

	if(first){
		uvc_framecnt_fix();

		cfg = (t_libuav_cfg *)(g_libusbclass_cfg_hs.arg);
		vmsc_cfg = (t_libvmsc_cfg *)cfg->extra_cfg;
		if(cfg) {
			for(k=0; k<cfg->iad_cnt; k++) {
				if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
					if(cfg->iad[k].vctrlif) {
						uvc_gen_pucontrol(cfgConf_HI, g_uvc_cp_param_hs.offset[k].pu_start);
		    			cfg->iad[k].vctrlif->encv_init = 1;
					}
				}
			}
		}

		cfg = (t_libuav_cfg *)(g_libusbclass_cfg_fs.arg);
		if(cfg) {
			for(k=0; k<cfg->iad_cnt; k++) {
				if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
					if(cfg->iad[k].vctrlif) {
						uvc_gen_pucontrol(cfgConf_FL, g_uvc_cp_param_fs.offset[k].pu_start);
			    		cfg->iad[k].vctrlif->encv_init = 1;
					}
				}
			}
		}

		if(vmsc_cfg && vmsc_cfg->init) {
			vmsc_cfg->init(vmsc_cfg);
		}
	}

	return 0;
}

void usbclass_clean(void)
{
	int k,i;
	t_libuav_cfg *cfg;
	
	cfg = (t_libuav_cfg *)(g_libusbclass_cfg_hs.arg);
	if(cfg){
		for(k=0; k<cfg->iad_cnt; k++) {
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
				for(i=0; i<cfg->iad[k].vstrmif_cnt; i++){
				cfg->iad[k].vstrmif[i].v_status =UVC_STATUS_IDLE;
				}
			}
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
				for(i=0; i<cfg->iad[k].astrmif_cnt; i++){
					cfg->iad[k].astrmif[i].a_status = UVC_STATUS_IDLE;
				}
			}
		}
	}

	cfg = (t_libuav_cfg *)(g_libusbclass_cfg_fs.arg);
	if(cfg){
		for(k=0; k<cfg->iad_cnt; k++) {
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
				for(i=0; i<cfg->iad[k].vstrmif_cnt; i++){
				cfg->iad[k].vstrmif[i].v_status =UVC_STATUS_IDLE;
				}
			}
			if(cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
				for(i=0; i<cfg->iad[k].astrmif_cnt; i++){
					cfg->iad[k].astrmif[i].a_status = UVC_STATUS_IDLE;
				}
			}
		}
	}
}

