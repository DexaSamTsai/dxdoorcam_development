#include "includes.h"

void uac_Handle_SU(void)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch(g_libusb_cfg.req.request.bRequest)
	{
		case UAV_GET_CUR:
			WriteReg32(tmp_addr, 0x1);
			break;
		case UAV_SET_CUR:
			break;
		case UAV_GET_RES:
			WriteReg32(tmp_addr, 0x1);
			break;
		default:
			debug_printf("unsupport UAC Select unit\n");
			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			break;
	}
}

void uac_Handle_FU(t_libuac_ctrlif * actrlif)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch(g_libusb_cfg.req.request.wValue_H)
	{
		case AUDIO_CTRL_MUTE:
			if(g_libusb_cfg.req.request.bRequest == UAV_GET_CUR)
			{
				WriteReg32(tmp_addr, actrlif->mute);
			}
			else if(g_libusb_cfg.req.request.bRequest == UAV_SET_CUR)
			{
				actrlif->mute = g_libusb_cfg.req.databuf[0];
			}
			else{
			   g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			}
			break;
		case AUDIO_CTRL_VOLUME:
			switch(g_libusb_cfg.req.request.bRequest)
			{
		       case UAV_GET_CUR:
					WriteReg32(tmp_addr, actrlif->volume);
					break;
		       case UAV_SET_CUR:
					actrlif->volume = ((g_libusb_cfg.req.databuf[1] & 0xff) << 8) | (g_libusb_cfg.req.databuf[0] & 0xff);
					break;
		       case UAV_GET_MIN:
					WriteReg32(tmp_addr, UAC_VOLUME_MIN);
					break;
		       case UAV_GET_MAX:
			        WriteReg32(tmp_addr, UAC_VOLUME_MAX);
                    break;
		       case UAV_GET_RES:
			        WriteReg32(tmp_addr, UAC_VOLUME_RES);
                    break;
		       default:
			        g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			        break;
		    }
		    break;
		case AUDIO_CTRL_AGC:
			switch(g_libusb_cfg.req.request.bRequest)
			{
				case UAV_GET_CUR:
					WriteReg32(tmp_addr, actrlif->agc);
					break;
				case UAV_SET_CUR:
					actrlif->agc = g_libusb_cfg.req.databuf[0];
					break;
				default:
					g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
					break;
			}
			break;
		case AUDIO_CTRL_BASS_BOOST:
			switch(g_libusb_cfg.req.request.bRequest)
			{
				case UAV_GET_CUR:
					WriteReg32(tmp_addr, 0);
					break;
				case UAV_SET_CUR:
					break;
				default:
					g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
					break;
			}
			break;
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			break;
	}

	if(actrlif->ac_fu_handler){
		actrlif->ac_fu_handler(g_libusb_cfg.req.request.wValue_H, g_libusb_cfg.req.request.bRequest, ((g_libusb_cfg.req.databuf[1] & 0xff) << 8) | (g_libusb_cfg.req.databuf[0] & 0xff));
	}
}

int uac_HandleControlRequest(t_libuac_ctrlif *actrlif)
{
	if((g_libusb_cfg.req.request.wIndex_H <= actrlif->FU_ID_MAX) &&
	  (g_libusb_cfg.req.request.wIndex_H >= actrlif->FU_ID_MIN))
	{
			uac_Handle_FU(actrlif);
	}
	else if (g_libusb_cfg.req.request.wIndex_H == actrlif->SU_ID){
		uac_Handle_SU();
	}
	else{
		g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
	}

	return 1;
}

int uac_HandleStreamRequest(t_libuac_strmif *strmif)
{
	u32 asample_rate = 0;	
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
    if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR ){
		//TODO
	    //HandleRequest_AS_EndpointControl();
     	asample_rate = ((g_libusb_cfg.req.databuf[1] & 0xff) << 8) | (g_libusb_cfg.req.databuf[0] & 0xff);
		debug_printf("AudioSample: %d\n", asample_rate);
		if(strmif->setsamplerate){
			return strmif->setsamplerate(strmif, asample_rate);
		}
		else {
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
			return 1;
		}
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR ) {
		if(strmif->getsamplerate){
			WriteReg32(tmp_addr, strmif->getsamplerate());
			return 0;
		}else {
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
			return 1;
		}
	}
	else{
		g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
		return 1;
	}
}

//return: 0:ok, 1:err, -1:not to this request
int uac_HandleRequest(void)
{
	int i,k;

	for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
			if(g_libuav_cfg->iad[k].actrlif && (g_libuav_cfg->iad[k].actrlif->CTRLIF_ID == g_libusb_cfg.req.request.wIndex_L))
			{
				return uac_HandleControlRequest(g_libuav_cfg->iad[k].actrlif);
			}
			for(i=0; i<g_libuav_cfg->iad[k].astrmif_cnt; i++){
				if(((g_libuav_cfg->iad[k].astrmif[i].STRMIF_ID == g_libusb_cfg.req.request.wIndex_L) && ((g_libusb_cfg.req.request.bmRequestType & USB_RECIP_MASK) == USB_RECIP_INTERFACE))//send to stream if
				  || ((g_libuav_cfg->iad[k].astrmif[i].STRM_EP == g_libusb_cfg.req.request.wIndex_L) && ((g_libusb_cfg.req.request.bmRequestType & USB_RECIP_MASK) == USB_RECIP_ENDPOINT))) //or send to stream ep
				{
					return uac_HandleStreamRequest(g_libuav_cfg->iad[k].astrmif + i);
				}
			}
		}
	}

	return -1;
}
