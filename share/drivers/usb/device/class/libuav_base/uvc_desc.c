#include "includes.h"

//cfgConf_HI
u8 cfgConf_HI[1024] __attribute__((aligned(4))) = {

	// --- Configuration Descriptor ---
	0x09, // Length
	0x02, // Type
	0xd9, // Total Length
	0x00, 
	0x02, // Number of Interface
	0x01, // Configuration Value
	0x00, // String : Configuration index
	0x80, // Attributes
	0x40, // Maximal power : mA

	// --- Standard Video Interface Collection IAD ---
	0x08, // Length
	0x0b, // Type
	0x00, // First interface number
	0x02, // Interface count
	0x0e, // Function class
	0x03, // Function subclass
	0x00, // not used
	0x02, // interface string descriptor index

	// --- Standard VC Interface Descriptor ---
	0x09, // Length
	0x04, // Type
	0x00, // interface number
	0x00, // index of the alternate setting
	0x01, // number of endpoint
	0x0e, // interface class
	0x01, // interface subclass
	0x00, // interface protocol
	0x02, // interface string descriptor index

	// --- Class-Specific VC Interface Descriptor ---
	0x0d, // Length
	0x24, // Type
	0x01, // subtype:VC_HEADER
	0x00, // revision of class specification -- uvc1.0 spec
	0x01, 
	0x4d, // total size of class-specification descriptor
	0x00, 
	0x80, // clock frequency
	0xc3, 
	0xc9, 
	0x01, 
	0x01, // number of streaming interface
	0x01, // streaming interface id

	// --- Input Terminal Descriptor(camera) ---
	0x12, // Length
	0x24, // Type
	0x02, // subtype:VC_INPUT_TERMINAL
	0x01, // terminal ID
	0x01, // itt_camera terminal type
	0x02, 
	0x00, // no association
	0x00, // unused
	0x00, // no optical zoom supported
	0x00, 
	0x00, // no optical zoom supported
	0x00, 
	0x00, // no optical zoom supported
	0x00, 
	0x03, // control size
	0x00, // bmControl
	0x00, // bmControl
	0x00, // bmControl

	// --- Ouput Terminal Descriptor ---
	0x09, // Length
	0x24, // Type
	0x03, // subtype:VC_OUTPUT_TERMINAL
	0x02, // terminal ID
	0x01, // tt_streaming terminal type
	0x01, 
	0x00, // association terminal
	0x07, // source ID
	0x00, // unused

	// --- Processing Unit Descriptor ---
	0x0b, // Length
	0x24, // Type
	0x05, // subtype:VC_PROCESSING_UNIT
	0x07, // unit ID
	0x01, // source ID(input terminal ID)
	0x00, // unused, not support digital multiplier
	0x00, 
	0x02, // control size
	0x00, // controls(Effects)
	0x00, 
	0x02, // string index

	// --- Extension Unit Descriptor ---
	0x1a, // Length
	0x24, // Type
	0x06, // subtype:VC_EXTENSION_UNIT
	0x08, // unit ID
	0x8a, // DD880F8A-1CBA-4954-8A25-F7875967F0F7
	0x0f, 
	0x88, 
	0xdd, 
	0xba, 
	0x1c, 
	0x54, 
	0x49, 
	0x8a, 
	0x25, 
	0xf7, 
	0x87, 
	0x59, 
	0x67, 
	0xf0, 
	0xf7, 
	0x02, // number of controls in extension unit
	0x01, // input pins number
	0x07, // source ID
	0x01, // contorl size
	0x03, // bmControl
	0x02, // string index

	// --- standard interrupt endpoint descriptor ---
	0x07, // Length
	0x05, // Type
	0x82, // 
	0x03, // interrupt transfer type
	0x40, // max packet size
	0x00, 
	0x10, // polling interval -- 128ms

	// --- class-specific interrupt endpoint descriptor ---
	0x05, // Length
	0x25, // Type
	0x03, // ep_interrupt
	0x00, // max packet size
	0x04, 

	// --- standard VS Interface Descriptor(alt 0) ---
	0x09, // Length
	0x04, // Type
	0x01, // index of this interface
	0x00, // index of this alternate setting
	0x00, // number of endpoint(ISO)
	0x0e, // cc_video, interface class
	0x02, // sc_videostreaming, subclass
	0x00, // pc_protocol_undefined
	0x00, // unused

	// --- Class-Specific VS Interface Input Header Descriptor ---
	0x0e, // Length
	0x24, // Type
	0x01, // subtype:VS_INPUT_HEADER
	0x01, // number of video payload format
	0x4d, // total size of class-specific videostreaming interface descriptor
	0x00, 
	0x83, // address of the iso endpoint used for video data
	0x00, // no dynamic format change supported
	0x02, // terminal link ID(ouput terminal)
	0x00, // StillCaptureMethod: no
	0x00, // TriggerSupport: no
	0x00, // TriggerUsage: no
	0x01, // Control size
	0x00, // no videostreaming specific control - format 1

	// --- class-specific vs format descriptor ---
	0x1b, // Length
	0x24, // Type
	0x04, // subtype: VS_FORMAT_UNCOMPRESSED
	0x01, // Index of this format descriptor
	0x01, // Number of frame descriptors following that correspond to this format
	0x59, // GUID of stream-encoding format,YUY2(YUV422) <<32595559-0000-0010-8000-00AA00389B71>>
	0x55, 
	0x59, 
	0x32, 
	0x00, 
	0x00, 
	0x10, 
	0x00, 
	0x80, 
	0x00, 
	0x00, 
	0xaa, 
	0x00, 
	0x38, 
	0x9b, 
	0x71, 
	0x10, // Number of bits per pixel used to specify color in the decoded video frame
	0x01, // DefaultFrameIndex: force 1
	0x00, // The X dimension of the picture aspect ratio
	0x00, // The Y dimension of the picture aspect ratio
	0x00, // Specifies interlace information
	0x00, // CopyProtect

	// --- class-specific vs frame descriptor ---
	0x1e, // Length
	0x24, // Type
	0x05, // subtype: format+1
	0x01, // Index of this frame descriptor
	0x00, // not support still image,and fixed frame rate
	0x80, // width
	0x02, 
	0xe0, // height
	0x01, 
	0x00, // bitrate min
	0x00, 
	0xca, 
	0x08, 
	0x00, // bitrate max
	0x00, 
	0xca, 
	0x08, 
	0x00, // framesize
	0x60, 
	0x09, 
	0x00, 
	0x15, // default frame interval
	0x16, 
	0x05, 
	0x00, 
	0x01, // number of frame interval
	0x15, // fps
	0x16, 
	0x05, 
	0x00, 

	// --- color matching descriptor ---
	0x06, // Length
	0x24, // Type
	0x0d, // subtype:VS_COLORFORMAT
	0x01, // ColorPrimaries:This defines the color primaries and the reference white. 1: BT.709, sRGB (default)
	0x01, // TransferCharacteristics:This field defines the opto-electronic transfer characteristic of the source picture also called the gamma function. 1: BT.709 (default)
	0x04, // MatrixCoefficients: Matrix used to compute luma and chroma values from the color primaries. 1: BT. 709

	// --- standard VS Interface Descriptor(alt 1) ---
	0x09, // Length
	0x04, // Type
	0x01, // index of this interface
	0x01, // index of this alternate setting
	0x01, // number of endpoint
	0x0e, // cc_video, interface class
	0x02, // sc_videostreaming, subclass
	0x00, // pc_protocol_undefined
	0x00, // unused

	// --- standard vs iso video data endpoint descriptor ---
	0x07, // Length
	0x05, // Type
	0x83, // endpoint address
	0x05, // iso transfer type
	0x00, // wMaxPacketSize
	0x14, 
	0x01, // ep interval
};

u16 __uvc_it_start_hs_0[] = 
{
	53,
};

t_uvc_cp_offset __uvc_cp_offset_hs[] = 
{
	{
		.pu_start = 73,
		.it_start = &__uvc_it_start_hs_0[0],
		.it_cnt = sizeof(__uvc_it_start_hs_0)/sizeof(__uvc_it_start_hs_0[0]),
	},
};

t_uvc_cp_param g_uvc_cp_param_hs = 
{
	.offset = &__uvc_cp_offset_hs[0],
	.offset_cnt = sizeof(__uvc_cp_offset_hs)/sizeof(__uvc_cp_offset_hs[0]),
};

//cfgConf_FL
u8 cfgConf_FL[1024] __attribute__((aligned(4))) = {

	// --- Configuration Descriptor ---
	0x09, // Length
	0x02, // Type
	0xd9, // Total Length
	0x00, 
	0x02, // Number of Interface
	0x01, // Configuration Value
	0x00, // String : Configuration index
	0x80, // Attributes
	0x40, // Maximal power : 512mA

	// --- Standard Video Interface Collection IAD ---
	0x08, // Length
	0x0b, // Type
	0x00, // First interface number
	0x02, // Interface count
	0x0e, // Function class
	0x03, // Function subclass
	0x00, // not used
	0x02, // interface string descriptor index

	// --- Standard VC Interface Descriptor ---
	0x09, // Length
	0x04, // Type
	0x00, // interface number
	0x00, // index of the alternate setting
	0x01, // number of endpoint
	0x0e, // interface class
	0x01, // interface subclass
	0x00, // interface protocol
	0x02, // interface string descriptor index

	// --- Class-Specific VC Interface Descriptor ---
	0x0d, // Length
	0x24, // Type
	0x01, // subtype:VC_HEADER
	0x00, // revision of class specification -- uvc1.0 spec
	0x01, 
	0x4d, // total size of class-specification descriptor
	0x00, 
	0x80, // clock frequency
	0xc3, 
	0xc9, 
	0x01, 
	0x01, // number of streaming interface
	0x01, // streaming interface id

	// --- Input Terminal Descriptor(camera) ---
	0x12, // Length
	0x24, // Type
	0x02, // subtype:VC_INPUT_TERMINAL
	0x01, // terminal ID
	0x01, // itt_camera terminal type
	0x02, 
	0x00, // no association
	0x00, // unused
	0x00, // no optical zoom supported
	0x00, 
	0x00, // no optical zoom supported
	0x00, 
	0x00, // no optical zoom supported
	0x00, 
	0x03, // control size
	0x00, // bmControl
	0x00, // bmControl
	0x00, // bmControl

	// --- Ouput Terminal Descriptor ---
	0x09, // Length
	0x24, // Type
	0x03, // subtype:VC_OUTPUT_TERMINAL
	0x02, // terminal ID
	0x01, // tt_streaming terminal type
	0x01, 
	0x00, // association terminal
	0x07, // source ID
	0x00, // unused

	// --- Processing Unit Descriptor ---
	0x0b, // Length
	0x24, // Type
	0x05, // subtype:VC_PROCESSING_UNIT
	0x07, // unit ID
	0x01, // source ID(input terminal ID)
	0x00, // unused, not support digital multiplier
	0x00, 
	0x02, // control size
	0x00, // controls(Effects)
	0x00, 
	0x02, // string index

	// --- Extension Unit Descriptor ---
	0x1a, // Length
	0x24, // Type
	0x06, // subtype:VC_EXTENSION_UNIT
	0x08, // unit ID
	0x8a, // DD880F8A-1CBA-4954-8A25-F7875967F0F7
	0x0f, 
	0x88, 
	0xdd, 
	0xba, 
	0x1c, 
	0x54, 
	0x49, 
	0x8a, 
	0x25, 
	0xf7, 
	0x87, 
	0x59, 
	0x67, 
	0xf0, 
	0xf7, 
	0x02, // number of controls in extension unit
	0x01, // input pins number
	0x01, // source ID
	0x01, // contorl size
	//0x03, // bmControl
	0x00, // bmControl
	0x02, // string index

	// --- standard interrupt endpoint descriptor ---
	0x07, // Length
	0x05, // Type
	0x81, // 
	0x03, // interrupt transfer type
	0x40, // max packet size
	0x00, 
	0x10, // polling interval -- 128ms

	// --- class-specific interrupt endpoint descriptor ---
	0x05, // Length
	0x25, // Type
	0x03, // ep_interrupt
	0x40, // max packet size
	0x00, 

	// --- standard VS Interface Descriptor(alt 0) ---
	0x09, // Length
	0x04, // Type
	0x01, // index of this interface
	0x00, // index of this alternate setting
	0x00, // number of endpoint(ISO)
	0x0e, // cc_video, interface class
	0x02, // sc_videostreaming, subclass
	0x00, // pc_protocol_undefined
	0x00, // unused

	// --- Class-Specific VS Interface Input Header Descriptor ---
	0x0e, // Length
	0x24, // Type
	0x01, // subtype:VS_INPUT_HEADER
	0x01, // number of video payload format
	0x4d, // total size of class-specific videostreaming interface descriptor
	0x00, 
	0x83, // address of the iso endpoint used for video data
	0x00, // no dynamic format change supported
	0x02, // terminal link ID(ouput terminal)
	0x00, // StillCaptureMethod: no
	0x00, // TriggerSupport: no
	0x00, // TriggerUsage: no
	0x01, // Control size
	0x00, // no videostreaming specific control - format 1

	// --- class-specific vs format descriptor ---
	0x1b, // Length
	0x24, // Type
	0x04, // subtype: VS_FORMAT_UNCOMPRESSED
	0x01, // Index of this format descriptor
	0x01, // Number of frame descriptors following that correspond to this format
	0x59, // GUID of stream-encoding format,YUY2(YUV422) <<32595559-0000-0010-8000-00AA00389B71>>
	0x55, 
	0x59, 
	0x32, 
	0x00, 
	0x00, 
	0x10, 
	0x00, 
	0x80, 
	0x00, 
	0x00, 
	0xaa, 
	0x00, 
	0x38, 
	0x9b, 
	0x71, 
	0x10, // Number of bits per pixel used to specify color in the decoded video frame
	0x01, // DefaultFrameIndex: force 1
	0x00, // The X dimension of the picture aspect ratio
	0x00, // The Y dimension of the picture aspect ratio
	0x00, // Specifies interlace information
	0x00, // CopyProtect

	// --- class-specific vs frame descriptor ---
	0x1e, // Length
	0x24, // Type
	0x05, // subtype: format+1
	0x01, // Index of this frame descriptor
	0x00, // not support still image,and fixed frame rate
	0x40, // width
	0x01, 
	0xf0, // height
	0x00, 
	0x00, // bitrate min
	0x80, 
	0x32, 
	0x02, 
	0x00, // bitrate max
	0x80, 
	0x32, 
	0x02, 
	0x00, // framesize
	0x58, 
	0x02, 
	0x00, 
	0x15, // default frame interval
	0x16, 
	0x05, 
	0x00, 
	0x01, // number of frame interval
	0x15, // fps
	0x16, 
	0x05, 
	0x00, 

	// --- color matching descriptor ---
	0x06, // Length
	0x24, // Type
	0x0d, // subtype:VS_COLORFORMAT
	0x01, // ColorPrimaries:This defines the color primaries and the reference white. 1: BT.709, sRGB (default)
	0x01, // TransferCharacteristics:This field defines the opto-electronic transfer characteristic of the source picture also called the gamma function. 1: BT.709 (default)
	0x04, // MatrixCoefficients: Matrix used to compute luma and chroma values from the color primaries. 1: BT. 709

	// --- standard VS Interface Descriptor(alt 1) ---
	0x09, // Length
	0x04, // Type
	0x01, // index of this interface
	0x01, // index of this alternate setting
	0x01, // number of endpoint
	0x0e, // cc_video, interface class
	0x02, // sc_videostreaming, subclass
	0x00, // pc_protocol_undefined
	0x00, // unused

	// --- standard vs iso video data endpoint descriptor ---
	0x07, // Length
	0x05, // Type
	0x83, // endpoint address
	0x05, // iso transfer type
	0x80, // wMaxPacketSize
	0x03, 
	0x01, // ep interval
};

u16 __uvc_it_start_fs_0[] = 
{
	53,
};

t_uvc_cp_offset __uvc_cp_offset_fs[] = 
{
	{
		.pu_start = 73,
		.it_start = &__uvc_it_start_fs_0[0],
		.it_cnt = sizeof(__uvc_it_start_fs_0)/sizeof(__uvc_it_start_fs_0[0]),
	},
};

t_uvc_cp_param g_uvc_cp_param_fs = 
{
	.offset = &__uvc_cp_offset_fs[0],
	.offset_cnt = sizeof(__uvc_cp_offset_fs)/sizeof(__uvc_cp_offset_fs[0]),
};


u32 cfgDevice_HI [5] = { 0x02000112, 0x400102ef, 0x788405a9, 0x02010100, 0x0100 };
u32 cfgDevice_FL [5] = { 0x01100112, 0x400102ef, 0x788405a9, 0x02010100, 0x0100 };
u32 cfgQualifier [3] = { 0x0200060a, 0x400102ef, 0x0001 }; 

t_csrdata g_csrdata[12] = {
 {0x00000000, 0x40002000, 0x0000000c },
 {0x00000001, 0x50000000, 0x0000000c },
 {0x00000002, 0x02000080, 0x0000000c },
 {0x00030003, 0x00000000, 0x0000000c },
 {0x00030004, 0x200088b3, 0x0000000c },
 {0x00030005, 0x200000f2, 0x0000000c },
 {0x00000000, 0x00000000, 0x00000030 }
};

//cfgStr1
u32 cfgStr1[15] = {
	0x004f033c, 
	0x006e006d, 
	0x00760069, 
	0x00730069, 
	0x006f0069, 
	0x0020006e, 
	0x00650054, 
	0x00680063, 
	0x006f006e, 
	0x006f006c, 
	0x00690067, 
	0x00730065, 
	0x0020002c, 
	0x006e0049, 
	0x002e0063, 
};

//cfgStr2
u32 cfgStr2[7] = {
	0x0055031c, 
	0x00420053, 
	0x004f0020, 
	0x00200056, 
	0x00610043, 
	0x0065006d, 
	0x00610072, 
};

//cfgStr3
u32 cfgStr3[7] = {
	0x0055031a, 
	0x00420053, 
	0x004f0020, 
	0x00200056, 
	0x00750041, 
	0x00690064, 
	0x0000006f, 
};

u32 cfgStr0[1] = { 0x04090304 };

