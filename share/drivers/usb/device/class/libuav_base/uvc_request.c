#include "includes.h"

static u8 interval_to_framerate(u32 frtmp)
{
	return ((10000000/frtmp) & 0xff);
}

static u32 framerate_to_interval(u32 frame_rate)
{ 
	return (10000000/frame_rate);
}

void HandleRequest_SU_InputSelect(void)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch(g_libusb_cfg.req.request.bRequest)
	{
		case UAV_SET_CUR:
			//FIXME: 
			break;
		case UAV_GET_CUR:
		case UAV_GET_MIN:
		case UAV_GET_MAX:
		case UAV_GET_RES:
		case UAV_GET_DEF:
			//BIG ENDIAN, so write to the last byte
			WriteReg32(tmp_addr, 0x01);
			break;
		case UAV_GET_INFO:
			WriteReg32(tmp_addr, 0x03);
			break;
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			break;
	}
}

#define e_sensor_effect int

void HandleRequest_PU(t_libuvc_ctrlif *vctrlif, e_sensor_effect effect)
{
#if 0
	u32 TempValue;
	t_sensoreffect_one *tso;
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	tso = sensor_effect_getinfo(effect);
	if(tso == NULL){
		g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
		g_libusb_cfg.req.wLength = 0;
		return;
	}
	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		//FIXME: some are 16bits data
		TempValue = g_libusb_cfg.req.databuf[0];
		debug_printf("level:%d\n",TempValue );
		if( TempValue >= tso->min && TempValue <= tso->max )
		{
			sensor_effect_req(effect, TempValue );
			if(vctrlif->DO_SNR_EFFECT_PROC) {
				sensor_effect_proc();
			}
		}
		else {
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
		}
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR )
	{
		WriteReg32(tmp_addr, tso->cur);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_MIN )
	{
		WriteReg32(tmp_addr, tso->min);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_MAX )
	{
		WriteReg32(tmp_addr, tso->max);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_LEN )
	{
		WriteReg32(tmp_addr, sizeof(tso->max));
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_RES )
	{
		WriteReg32(tmp_addr, 0x1);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_INFO )
	{
		WriteReg32(tmp_addr, 0x3);
		g_libusb_cfg.req.wLength = 1;
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_DEF )
	{
		WriteReg32(tmp_addr, tso->def);
	}
	else {
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		return;
	}
#endif
}

int HandleRequest_VS_Probe(t_libuvc_strmif * vstrmif)
{
	u8 i;
	u8 formatindex, frameindex, framerate = 0;
	u32 frtmp = 0, tmp_size = 0,frame_cnt = 0;
	u8 *buffer = g_libusb_cfg.req.databuf;
	u32 pl_size = 0, pl_cnt = 0; 
	u16 u16width, u16height;

	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		if( g_libusb_cfg.req.wLength > MAX_EP0_PKTSIZE )
		{
#ifdef USB_UART_PRINT
			debug_printf("wTableSize > Packet_Size\n");
#endif
			return 1;
		}

		//handle framerate
		frtmp = buffer[4] | (buffer[5] << 8) | (buffer[6] << 16) | (buffer[7] << 24);
		framerate = interval_to_framerate(frtmp);
		if(framerate == 0){
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
#ifdef USB_UART_PRINT
			debug_printf("Probe: invalid frame interval: %x\n", frtmp);
#endif
			return 1;
		}

		//handle format
		formatindex = buffer[2];
		if( formatindex != 1 )
		{
			//only support one format in one stream now
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
#ifdef USB_UART_PRINT
			debug_printf("Probe(Format): out of range:%d\n", formatindex);
#endif
			return 1;
		}

		//handle frame
		frameindex = buffer[3];
		if(VS_FORMAT_MPEG2TS != vstrmif->format)	
		{		
			if( frameindex <= 0 || frameindex > vstrmif->frame_cnt)
			{
				g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
#ifdef USB_UART_PRINT
				debug_printf("Probe(Frame): out of range:%d\n", frameindex);
#endif
				return 1;
			}
		}

		vstrmif->frameindex = frameindex - 1;
		vstrmif->framerate = framerate;

		g_libusb_cfg.req.wLength = 0;

		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR || g_libusb_cfg.req.request.bRequest == UAV_GET_MIN || g_libusb_cfg.req.request.bRequest == UAV_GET_MAX)
	{
		memset(buffer, 0, sizeof(g_libusb_cfg.req.databuf));
		buffer[0] = 0x01;
		buffer[1] = 0;
		buffer[2] = 1;

		frame_cnt = vstrmif->frame_cnt;
		
		if(g_libusb_cfg.req.request.bRequest == UAV_GET_CUR){
			tmp_size = vstrmif->frame[vstrmif->frameindex].size;
		}
		else if(g_libusb_cfg.req.request.bRequest == UAV_GET_MAX){
			if(vstrmif->max_size)
				tmp_size = vstrmif->max_size;
			else
				tmp_size = vstrmif->frame[0].size;
		}
		else if(g_libusb_cfg.req.request.bRequest == UAV_GET_MIN){
			if(vstrmif->min_size)
				tmp_size = vstrmif->min_size;
			else
				tmp_size = vstrmif->frame[frame_cnt-1].size;
		}

		u16width = (u16)((tmp_size>>16) & 0xffff);
		u16height = (u16)(tmp_size & 0xffff);

		if(g_libusb_cfg.req.request.bRequest == UAV_GET_CUR) {
			frtmp = framerate_to_interval(vstrmif->framerate);
		}
		else if(g_libusb_cfg.req.request.bRequest == UAV_GET_MAX){
			if(vstrmif->framerate_max)
				frtmp = framerate_to_interval(vstrmif->framerate_max);
			else
				frtmp = FRAMEINTERVAL_30;
		}
		else if(g_libusb_cfg.req.request.bRequest == UAV_GET_MIN){
			if(vstrmif->framerate_min)
				frtmp = framerate_to_interval(vstrmif->framerate_min);
			else
				frtmp = FRAMEINTERVAL_5;
	    }

		if(VS_FORMAT_MPEG2TS != vstrmif->format)	
		{		
			buffer[3] = vstrmif->frameindex + 1;

			buffer[4] = frtmp & 0xff;
			buffer[5] = (frtmp >> 8) & 0xff;
			buffer[6] = (frtmp >> 16)& 0xff;
			buffer[7] = (frtmp >> 24) & 0xff;

			if( vstrmif->frameindex > vstrmif->frame_cnt){
				g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
#ifdef USB_UART_PRINT
			   	debug_printf("Probe(Frame): out of range\n");
#endif
				return 1;
			}
			else{
				frtmp = u16width*u16height*2;
			}
		}	
		else {
			frtmp = 640*480*2;
		}

		buffer[18] = frtmp & 0xff;
		buffer[19] = (frtmp >> 8) & 0xff;
		buffer[20] = (frtmp >> 16)& 0xff;
		buffer[21] = (frtmp >> 24) & 0xff;

		for(i=0; i<g_libusbclass_cfg->epnum; i++)
		{
			if(vstrmif->STRM_EP == g_libusbclass_cfg->epcfg[i].ep)
			{
				pl_size = g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff;
				pl_cnt  = ((g_libusbclass_cfg->epcfg[i].maxpktsize>>11) & 0x3) + 1;
				if((g_libusbclass_cfg->epcfg[i].type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_ISO) {
					frtmp = pl_size * pl_cnt;
				}else if((g_libusbclass_cfg->epcfg[i].type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_BULK) {
					frtmp = u16width*u16height*2 + 0x200;
				}

				//TODO: need to fix (jet)
				//pl_size = vstrmif->STRM_PYLSIZE & 0x7ff;
				//pl_cnt  = ((vstrmif->STRM_PYLSIZE>>11) & 0x3) + 1;
				//frtmp   = pl_size * pl_cnt;

				buffer[22] = frtmp & 0xff;
				buffer[23] = (frtmp >> 8) & 0xff;
				buffer[24] = (frtmp >> 16) & 0xff;
				buffer[25] = frtmp >> 24;
			}
		}
#ifdef UVC_V1_1
		buffer[26] = ((TICKS_PER_SEC) & 0xff);
		buffer[27] = ((TICKS_PER_SEC) >> 8) & 0xff;
#else
		buffer[26] = ((1000) & 0xff);
		buffer[27] = ((1000) >> 8) & 0xff;
#endif

		return 0;
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
#ifdef USB_UART_PRINT
		debug_printf("Probe: invalid request\n");
#endif
		return 1;
	}

}

static t_libusbep_cfg*  uvc_get_epcfg(t_libuvc_strmif *strmif)
{
	int i;

    for(i=0; i<g_libusbclass_cfg->epnum; i++){
        if(strmif->STRM_EP == g_libusbclass_cfg->epcfg[i].ep) {
              return (g_libusbclass_cfg->epcfg+i);
         }
   }
   return NULL;
}

int HandleRequest_VS_Commit(t_libuvc_strmif * vstrmif)
{
	u8 frame_rate;
	u8 *buffer = g_libusb_cfg.req.databuf;
	u32 frtmp;

	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		if( g_libusb_cfg.req.wLength >  MAX_EP0_PKTSIZE)
		{
#ifdef USB_UART_PRINT
			debug_printf("wTableSize > Packet_Size\n");
#endif
			return 1;
		}

		frtmp = buffer[4] | (buffer[5] << 8) | (buffer[6] << 16) | (buffer[7] << 24);
		frame_rate = interval_to_framerate(frtmp);

		if((VS_FORMAT_MPEG2TS != vstrmif->format) && (VS_FORMAT_FRAME_BASED != vstrmif->format)) {		
			if( 1 != buffer[2] || vstrmif->frameindex != buffer[3] - 1 || frame_rate != vstrmif->framerate)
			{
				g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
#ifdef USB_UART_PRINT
				debug_printf("Commit(frame & format): out of range\n");
#endif
				return 1;
			}
		}

		vstrmif->ep_cfg = uvc_get_epcfg(vstrmif);
		uvc_frame_set(vstrmif);

		return 0;
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
#ifdef USB_UART_PRINT
		debug_printf("Commit: invalid request\n");
#endif
		return 1;
	}
}

int HandleRequest_VS_Still_Probe(t_libuvc_strmif * vstrmif)
{
	u8 i;
	u8 formatindex, frameindex, compression_index;
	u32 frtmp,tmp_size;
	u16 width, height;
	u8 *buffer = g_libusb_cfg.req.databuf;
	u32 pl_size = 0, pl_cnt = 0; 

	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		if( g_libusb_cfg.req.wLength > MAX_EP0_PKTSIZE )
		{
#ifdef USB_UART_PRINT
			debug_printf("still_probe: wTableSize > Packet_Size\n");
#endif
			return 1;
		}

		formatindex = buffer[0];
		frameindex = buffer[1];
		compression_index = buffer[2];
		tmp_size = buffer[3] | (buffer[4] << 8) | (buffer[5] << 16) | (buffer[6] << 24);

		vstrmif->still_image->size = tmp_size;
		vstrmif->still_image->frameindex = frameindex;
		vstrmif->still_image->compression_index = compression_index;

		g_libusb_cfg.req.wLength = 0;

		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR || g_libusb_cfg.req.request.bRequest == UAV_GET_MIN || g_libusb_cfg.req.request.bRequest == UAV_GET_MAX)
	{
		memset(buffer, 0, MAX_EP0_PKTSIZE);
		frameindex = vstrmif->still_image->frameindex;
		buffer[0] = 0x01;
		buffer[1] = frameindex;
		buffer[2] = 1;

		width = ((vstrmif->still_image->frame[frameindex-1].size)>>16) & 0xfffff;
		height = (vstrmif->still_image->frame[frameindex-1].size)&0xffff;
		vstrmif->still_image->size = width * height * 2;
		frtmp = vstrmif->still_image->size;
			
		buffer[3] = frtmp & 0xff;
		buffer[4] = (frtmp >> 8) & 0xff;
		buffer[5] = (frtmp >> 16)& 0xff;
		buffer[6] = (frtmp >> 24) & 0xff;


		for(i=0; i<g_libusbclass_cfg->epnum; i++)
		{
			if(vstrmif->STRM_EP == g_libusbclass_cfg->epcfg[i].ep)
			{
				pl_size = g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff;
				pl_cnt  = ((g_libusbclass_cfg->epcfg[i].maxpktsize>>11) & 0x3) + 1;
				frtmp   = pl_size * pl_cnt;

				buffer[7] = frtmp & 0xff;
				buffer[8] = (frtmp >> 8) & 0xff;
				buffer[9] = (frtmp >> 16) & 0xff;
				buffer[10] = frtmp >> 24;
			}
		}
		return 0;
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
#ifdef USB_UART_PRINT
		debug_printf("still_probe: invalid request\n");
#endif
		return 1;
	}

}

int HandleRequest_VS_Still_Commit(t_libuvc_strmif * vstrmif)
{
	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		return 0;
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
#ifdef USB_UART_PRINT
		debug_printf("still_commit: invalid request\n");
#endif
		return 1;
	}
}

int HandleRequest_VS_Still_Trigger(t_libuvc_strmif * vstrmif)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		vstrmif->still_image->still_ctrl = g_libusb_cfg.req.databuf[0];
		if(vstrmif->still_image->frame_set)	{
			uvc_frame_set(vstrmif);
		}
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR ){
		WriteReg32(tmp_addr, vstrmif->still_image->still_ctrl);
		return 0;
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
#ifdef USB_UART_PRINT
		debug_printf("still trigger: invalid request\n");
#endif
		return 1;
	}
}

void HandleRequest_Power_Control(t_libuvc_ctrlif *vctrlif)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{	
		vctrlif->powermode = g_libusb_cfg.req.databuf[0];
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR )
	{
		WriteReg32(tmp_addr, vctrlif->powermode);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_INFO )
	{
		WriteReg32(tmp_addr, 0x03);		
	}
	else {
		debug_printf("unsupport power control request\n");
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
	}
}

void HandleRequest_Error_Code_Control(void)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR )
	{
		WriteReg32(tmp_addr, g_libusb_cfg.req.last_errcode);
		debug_printf("Last_errorCode:%x\n",g_libusb_cfg.req.last_errcode);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_INFO )
	{
		WriteReg32(tmp_addr, 0x01);		
	}
	else {
		g_libusb_cfg.req.wLength = 0;
		g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
	}
}		

extern void HandleRequest_XU_ReadUSBRegister(void);
int uvc_HandleControlRequest(t_libuvc_ctrlif *vctrlif)
{
	u8 i;

	//video control request
	if(g_libusb_cfg.req.request.wIndex_H == 0)//power mode request or error request code
	{
#ifdef USB_UART_PRINT
		debug_printf("Control interface request\n");
#endif
		//interface control requests
		switch( g_libusb_cfg.req.request.wValue_H )
		{
		   case 0x01:
			   HandleRequest_Power_Control(vctrlif);
			   return 0;
		   case 0x02: 
			   HandleRequest_Error_Code_Control();	
			   return 0;
		   default:
			   debug_printf("unsupport control interface request: %x\n", g_libusb_cfg.req.request.wValue_H);
			   g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			   return 1;
		}
	}

	if(g_libusb_cfg.req.request.wIndex_H == vctrlif->SU_ID)
	{
#ifdef USB_UART_PRINT
		debug_printf("SU\n");
#endif
		switch( g_libusb_cfg.req.request.wValue_H )
		{
		case SU_INPUT_SELECT_CONTROL: // Input Select Control
			HandleRequest_SU_InputSelect();
			return 0;
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			g_libusb_cfg.req.wLength = 0;
			return 1;
		}
	}

	if(g_libusb_cfg.req.request.wIndex_H == vctrlif->PU_ID)
	{
#ifdef USB_UART_PRINT
		debug_printf("PU:%x\n", g_libusb_cfg.req.request.wValue_H);
#endif
		switch(g_libusb_cfg.req.request.wValue_H)
		{
#if 0
		case 0x02:
			HandleRequest_PU(vctrlif, SNR_EFFECT_BRIGHT);
			break;
		case 0x03:
			HandleRequest_PU(vctrlif, SNR_EFFECT_CONTRAST);
			break;
		case 0x04:
			HandleRequest_PU(vctrlif, SNR_EFFECT_GAIN);
			break;
		case 0x05:
			HandleRequest_PU(vctrlif, SNR_EFFECT_LIGHT_FREQ);
			break;
		case 0x06:
			HandleRequest_PU(vctrlif, SNR_EFFECT_HUE);
			break;
		case 0x07:	// Saturation
			HandleRequest_PU(vctrlif, SNR_EFFECT_SATURATION);
			break;
		case 0x08:	// Sharpness
			HandleRequest_PU(vctrlif, SNR_EFFECT_SHARPNESS);
			break;
		case 0x09:	// Gamma
			HandleRequest_PU(vctrlif, SNR_EFFECT_GAMMA);
			break;
		case 0x0a:
			HandleRequest_PU(vctrlif, SNR_EFFECT_WHITE_BALANCE_TEMP);
			break;
		case 0x0b:
			HandleRequest_PU(vctrlif, SNR_EFFECT_WHITE_BALANCE_TEMP_AUTO);
			break;
#endif
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			g_libusb_cfg.req.wLength = 0;
			return 1;
		}
		return 0;
	}

	if(g_libusb_cfg.req.request.wIndex_H == vctrlif->XU_ID)
	{
#ifdef USB_UART_PRINT
		debug_printf("XU\n");
#endif
		switch( g_libusb_cfg.req.request.wValue_H )
		{
#ifdef USB_DEBUGREG
		case 0x01:
		case 0x02:
			//debug_printf("unsupport ReadUSBRegister\n");
			HandleRequest_XU_ReadUSBRegister();
			break;
#endif
		default:
			if(vctrlif->xu_handle_cb){
				return vctrlif->xu_handle_cb();
			}

			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			g_libusb_cfg.req.wLength = 0;
			return 1;
		}
		return 0;
	}

	if(g_libusb_cfg.req.request.wIndex_H == vctrlif->H_XU_ID)
	{
		u32 ret = 0;
#ifdef USB_UART_PRINT
		debug_printf("encv XU\n");
#endif
		if(vctrlif->encv_req_handle)
		{
			ret = vctrlif->encv_req_handle(vctrlif);	
		}

		return ret;
	}

	for(i=0; i<vctrlif->tid_cnt; i++) 
	{
		if(g_libusb_cfg.req.request.wIndex_H == vctrlif->tid[i].IT_ID)
		{
#ifdef USB_UART_PRINT
			debug_printf("IT_ID\n");
#endif
			switch(g_libusb_cfg.req.request.wValue_H)
			{
#ifdef CT_PANTILT_EN
				case 0x0d:
					HandleRequest_CT_Pantilt_Absolute();
					break;
#endif
				default:
					g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
					g_libusb_cfg.req.wLength = 0;
					return 1;
			}
			return 0;
		}
	}

#ifdef USB_UART_PRINT
	debug_printf("Invalid class unit request\n");
#endif
	g_libusb_cfg.req.error_code = UDEV_INVALID_UNIT;
	return 1;
}

int uvc_HandleStreamRequest(t_libuvc_strmif * strmif)
{
	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch( g_libusb_cfg.req.request.wValue_H )
	{
		case VS_PROBE_CONTROL:
#ifdef USB_UART_PRINT
			debug_printf("probe\n");
#endif
			return HandleRequest_VS_Probe(strmif);
		case VS_COMMIT_CONTROL:
#ifdef USB_UART_PRINT
			debug_printf("commit\n");
#endif
			return HandleRequest_VS_Commit(strmif);
		case VS_STILL_PROBE_CONTROL:
			return HandleRequest_VS_Still_Probe(strmif);
		case VS_STILL_COMMIT_CONTROL:
			return HandleRequest_VS_Still_Commit(strmif);
		case VS_STILL_IMAGE_TRIGGER_CONTROL:
			return HandleRequest_VS_Still_Trigger(strmif);
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
#ifdef USB_UART_PRINT
			debug_printf("Invalid stream interface control CS\n");
#endif
			return 1;
	}
	return 1;
}

//return code: 0:ok, 1:err, -1:not to this request
int uvc_HandleRequest(void)
{
	int i, k;

	for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			if(g_libuav_cfg->iad[k].vctrlif && (g_libuav_cfg->iad[k].vctrlif->CTRLIF_ID == g_libusb_cfg.req.request.wIndex_L))
			{
				return uvc_HandleControlRequest(g_libuav_cfg->iad[k].vctrlif);
			}
			for(i=0; i<g_libuav_cfg->iad[k].vstrmif_cnt; i++){
				if(((g_libuav_cfg->iad[k].vstrmif[i].STRMIF_ID == g_libusb_cfg.req.request.wIndex_L) && ((g_libusb_cfg.req.request.bmRequestType & USB_RECIP_MASK) == USB_RECIP_INTERFACE))//send to stream if
				  || ((g_libuav_cfg->iad[k].vstrmif[i].STRM_EP == g_libusb_cfg.req.request.wIndex_L) && ((g_libusb_cfg.req.request.bmRequestType & USB_RECIP_MASK) == USB_RECIP_ENDPOINT))) //or send to stream ep
				{
					if(g_libusb_cfg.req.request.wIndex_H != 0){
#ifdef USB_UART_PRINT
						debug_printf("stream request only to interface control\n");
#endif
						g_libusb_cfg.req.error_code = UDEV_INVALID_UNIT;
						return 1;
					}
					else {
						return uvc_HandleStreamRequest(g_libuav_cfg->iad[k].vstrmif + i);
					}
				}
			}
		}
	}
	return -1;
}
