#include "includes.h"

typedef struct s_uvcx_video_config
{
	uvcx_video_config_probe_commit_t def;
	uvcx_video_config_probe_commit_t max;
	uvcx_video_config_probe_commit_t min;
	uvcx_video_config_probe_commit_t cur;
}t_uvcx_video_config;

t_uvcx_video_config g_encv_video_config;

static u32 framerate_to_interval(u32 frame_rate)
{ 
		u32 frtmp;
			if(frame_rate <= 1){
				frtmp = FRAMEINTERVAL_1;	
			}else if(frame_rate <= 3){
				frtmp = FRAMEINTERVAL_3;
			}else if(frame_rate <= 5){
				frtmp = FRAMEINTERVAL_5;
			}else if(frame_rate <= 7){
				frtmp = FRAMEINTERVAL_7;
			}else if(frame_rate <= 10){
				frtmp = FRAMEINTERVAL_10;
			}else if(frame_rate <= 12){
				frtmp = FRAMEINTERVAL_12;
			}else if(frame_rate <= 15){
				frtmp = FRAMEINTERVAL_15;
			}else if(frame_rate <= 20){
				frtmp = FRAMEINTERVAL_20;
			}else if(frame_rate <= 24){
				frtmp = FRAMEINTERVAL_24;	
			}else if(frame_rate <= 30){
				frtmp = FRAMEINTERVAL_30;
			}else if(frame_rate <= 60){
				frtmp = FRAMEINTERVAL_60;
			}else{
				frtmp = 10000000 / frame_rate;
			}

			return frtmp;
}

void init_encv_config(void)
{
	u32 min_size = 0,max_size = 0, frtmp;
	u32 i,k,frame_cnt;

	frtmp = framerate_to_interval(g_libvenc_cfg.FRAME_RATE);
	g_encv_video_config.def.wWidth =  g_libvenc_cfg.video_width;
	g_encv_video_config.def.wHeight =  g_libvenc_cfg.video_height;
	g_encv_video_config.def.dwBitRate = g_libvenc_cfg.BIT_RATE;
	g_encv_video_config.def.dwFrameInterval =  frtmp;

	debug_printf(" init encv XU, default width is %d, height is %d, bitrate is %d, frame interval is %d\n",g_encv_video_config.def.wWidth, g_encv_video_config.def.wHeight, g_encv_video_config.def.dwBitRate, g_encv_video_config.def.dwFrameInterval);

	for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
		if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			for(i=0; i<g_libuav_cfg->iad[k].vstrmif_cnt; i++)
			{
				if(g_libuav_cfg->iad[k].vstrmif[i].format == VS_FORMAT_FRAME_BASED )
				{
					frame_cnt = g_libuav_cfg->iad[k].vstrmif[i].frame_cnt;
					max_size = g_libuav_cfg->iad[k].vstrmif[i].frame[0].size;
					min_size = g_libuav_cfg->iad[k].vstrmif[i].frame[frame_cnt-1].size;
				}
			}
		}
	}

		g_encv_video_config.min.wWidth = (u16)((min_size>>16) & 0xffff);
		g_encv_video_config.min.wHeight = (u16)(min_size & 0xffff);
		g_encv_video_config.min.dwFrameInterval = FRAMEINTERVAL_5;
		g_encv_video_config.min.dwBitRate = 0x5dc00; //0xf4240;
		g_encv_video_config.min.wSliceUnits = 0xffff;
		g_encv_video_config.min.wSliceMode = 0xffff;
		g_encv_video_config.min.wProfile = 0xffff;
		g_encv_video_config.min.wIFramePeriod = 0xffff;
		g_encv_video_config.min.wEstimatedVideoDelay = 0xffff;
		g_encv_video_config.min.wEstimatedMaxConfigDelay = 0xffff;
		g_encv_video_config.min.bUsageType = 0xff;
		g_encv_video_config.min.bRateControlMode = 0xff;
		g_encv_video_config.min.bTemporalScaleMode = 0x01;
		g_encv_video_config.min.bSpatialScaleMode = 0x00;
		g_encv_video_config.min.bSNRScaleMode = 0x00;
		g_encv_video_config.min.bStreamMuxOption = 0x00;
		g_encv_video_config.min.bStreamFormat = 0xff;
		g_encv_video_config.min.bEntropyCABAC = 0xff;
		g_encv_video_config.min.bTimestamp = 0xff;
		g_encv_video_config.min.bNumOfReorderFrames = 0xff;
		g_encv_video_config.min.bPreviewFlipped = 0xff;
		g_encv_video_config.min.bView = 0x00;

		memcpy(&(g_encv_video_config.max),&(g_encv_video_config.min),sizeof(struct _uvcx_video_config_probe_commit_t));

		g_encv_video_config.max.wWidth = (u16)((max_size>>16) & 0xffff);
		g_encv_video_config.max.wHeight = (u16)(max_size & 0xffff);
		g_encv_video_config.max.dwFrameInterval = FRAMEINTERVAL_30;
		g_encv_video_config.max.dwBitRate = 0x4c4b40; //5000000

		g_encv_video_config.def.wSliceUnits = 0x0000;
		g_encv_video_config.def.wSliceMode = 0x0000;
		g_encv_video_config.def.wProfile = 0x4200;
		g_encv_video_config.def.wIFramePeriod = 0x03e8;
		g_encv_video_config.def.wEstimatedVideoDelay = 0x0064;
		g_encv_video_config.def.wEstimatedMaxConfigDelay = 0x00fa;
		g_encv_video_config.def.bUsageType = 0x01;
		g_encv_video_config.def.bRateControlMode = 0x01;
		g_encv_video_config.def.bTemporalScaleMode = 0x01;
		g_encv_video_config.def.bSpatialScaleMode = 0x01;
		g_encv_video_config.def.bSNRScaleMode = 0x01;
		g_encv_video_config.def.bStreamMuxOption = 0x00;
		g_encv_video_config.def.bStreamFormat = 0x00;
		g_encv_video_config.def.bEntropyCABAC = 0x00;
		g_encv_video_config.def.bTimestamp = 0x00;
		g_encv_video_config.def.bNumOfReorderFrames = 0x00;
		g_encv_video_config.def.bPreviewFlipped = 0x00;
		g_encv_video_config.def.bView = 0x00;
      
		memcpy(&(g_encv_video_config.cur),&(g_encv_video_config.def),sizeof(struct _uvcx_video_config_probe_commit_t));

}

uvcx_video_config_probe_commit_t get_encv_config(u8 req)
{
	if(req == GET_MIN)
		return g_encv_video_config.min;
	if(req == GET_MAX||req == GET_RES)
		return g_encv_video_config.max;
	if(req == GET_DEF)
		return g_encv_video_config.def;
	if(req == GET_CUR)
		return g_encv_video_config.cur;
	return g_encv_video_config.def; //fix warning
}

int HandleRequest_XU_encv_Probe(t_libuvc_ctrlif *vctrlif)
{
	u8 i,k;
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;
	u32 frtmp, brtmp, tmpsize;
	uvcx_video_config_probe_commit_t tmp_config;
	u8 *TempBuf = g_libusb_cfg.req.databuf ;

#ifdef USB_UART_PRINT
	debug_printf("encv XU Probe\n");
#endif

	if(vctrlif->encv_init){
		init_encv_config();
		vctrlif->encv_init = 0;
	}

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;

	if( g_libusb_cfg.req.request.bRequest == GET_LEN )
	{
		WriteReg32(tmp_addr, 0x2a);
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == GET_INFO )
	{
		WriteReg32(tmp_addr, 0x3);
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == SET_CUR )
	{
        frtmp = TempBuf[0] | (TempBuf[1] << 8) | (TempBuf[2] << 16) | TempBuf[3];
		brtmp = TempBuf[4] | (TempBuf[5] << 8) | (TempBuf[6] << 16) | TempBuf [7];
		g_encv_video_config.cur.dwFrameInterval = frtmp;
		g_encv_video_config.cur.dwBitRate = brtmp;
		g_encv_video_config.cur.wWidth = TempBuf[12]+(u16)(TempBuf[13]<<8);
		g_encv_video_config.cur.wHeight = TempBuf[14]+(u16)(TempBuf[15]<<8);
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == GET_CUR || g_libusb_cfg.req.request.bRequest == GET_MIN || g_libusb_cfg.req.request.bRequest == GET_MAX || g_libusb_cfg.req.request.bRequest == GET_RES || g_libusb_cfg.req.request.bRequest == GET_DEF)
	{
		tmp_config = get_encv_config(g_libusb_cfg.req.request.bRequest);
		memset(TempBuf, 0, MAX_EP0_PKTSIZE );
		
		if(g_libusb_cfg.req.request.bRequest == GET_CUR)
		{
			 for(k=0; k<g_libuav_cfg->iad_cnt; k++) {
				if(g_libuav_cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
					 for(i=0; i<g_libuav_cfg->iad[k].vstrmif_cnt; i++)
					 {
						if(g_libuav_cfg->iad[k].vstrmif[i].format == VS_FORMAT_FRAME_BASED )
						{		
							tmp_config.dwFrameInterval = framerate_to_interval(g_libuav_cfg->iad[k].vstrmif[i].framerate);
							tmpsize = g_libuav_cfg->iad[k].vstrmif[i].frame[g_libuav_cfg->iad[k].vstrmif[i].frameindex].size;
							//debug_printf("encv XU Probe get cur size is %x\n",tmpsize);
							tmp_config.wWidth = (u16)((tmpsize>>16) & 0xffff);
							tmp_config.wHeight = (u16)(tmpsize & 0xffff);
						}
					 }
				}
			 }
		}

		TempBuf[0] = tmp_config.dwFrameInterval & 0xff;
		TempBuf[1] = (tmp_config.dwFrameInterval >> 8 ) & 0xff; 
		TempBuf[2] = (tmp_config.dwFrameInterval >> 16) & 0xff;
		TempBuf[3] = (tmp_config.dwFrameInterval >> 24) & 0xff;

		TempBuf[4] = tmp_config.dwBitRate & 0xff; 
		TempBuf[5] = (tmp_config.dwBitRate >> 8) & 0xff; 
		TempBuf[6] = (tmp_config.dwBitRate >> 16) & 0xff; 
		TempBuf[7] = (tmp_config.dwBitRate >> 24) & 0xff; 
		
		TempBuf[8] = 0x00; //0x01; hint
		TempBuf[9] = 0x20; //0x00;
		TempBuf[10] = 0xff;
		TempBuf[11] = 0xff;
		
		TempBuf[12] = tmp_config.wWidth & 0xff;
		TempBuf[13] = (tmp_config.wWidth >> 8 ) & 0xff;

		TempBuf[14] = tmp_config.wHeight & 0xff;
		TempBuf[15] = (tmp_config.wHeight >> 8 ) & 0xff;
		TempBuf[16] = tmp_config.wSliceUnits & 0xff;
		TempBuf[17] = (tmp_config.wSliceUnits >> 8 ) & 0xff;
		TempBuf[18] = tmp_config.wSliceMode & 0xff;
		TempBuf[19] = (tmp_config.wSliceMode >> 8 ) & 0xff;
		TempBuf[20] = tmp_config.wProfile & 0xff;
		TempBuf[21] = (tmp_config.wProfile >> 8 ) & 0xff;
		TempBuf[22] = tmp_config.wIFramePeriod & 0xff;
		TempBuf[23] = (tmp_config.wIFramePeriod >> 8 ) & 0xff;
		TempBuf[24] = tmp_config.wEstimatedVideoDelay & 0xff;
		TempBuf[25] = (tmp_config.wEstimatedVideoDelay >> 8 ) & 0xff;
		TempBuf[26] = tmp_config.wEstimatedMaxConfigDelay & 0xff;
		TempBuf[27] = (tmp_config.wEstimatedMaxConfigDelay >> 8 ) & 0xff;
		TempBuf[28] = tmp_config.bUsageType;
		TempBuf[29] = tmp_config.bRateControlMode;
		TempBuf[30] = tmp_config.bTemporalScaleMode;
		TempBuf[31] = tmp_config.bSpatialScaleMode;
		TempBuf[32] = tmp_config.bSNRScaleMode;
		TempBuf[33] = tmp_config.bStreamMuxOption;
		TempBuf[34] = tmp_config.bStreamFormat;
		TempBuf[35] = tmp_config.bEntropyCABAC;
		TempBuf[36] = tmp_config.bTimestamp;
		TempBuf[37] = tmp_config.bNumOfReorderFrames;
		TempBuf[38] = tmp_config.bPreviewFlipped;
		TempBuf[39] = tmp_config.bView;
		TempBuf[40] = 0x00;
		TempBuf[41] = 0x00;

		endian_swap_char((unsigned char *)g_libusb_cfg.req.buffer, g_libusb_cfg.req.wLength);

		return 0;
	}
	else {
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		g_libusb_cfg.req.wLength = 0;
#ifdef USB_UART_PRINT
		debug_printf("Probe: invalid request\n");
#endif
		return 1;
	}

}

void encv_change_bitrate(u32 bitrate)
{
	debug_printf("encv change bitrare to %d\n",bitrate);
	//g_libvenc_cfg.BIT_RATE = bitrate;//change bit rate

	//libvenc_bitrate_config(g_libvenc_cfg.BIT_RATE, g_libvenc_cfg.FRAME_RATE, 0);
}

int HandleRequest_XU_encv_Commit(void)
{
	u32 brtmp;
	u8 *TempBuf = g_libusb_cfg.req.databuf ;
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

#ifdef USB_UART_PRINT
	debug_printf("encv XU Commit\n");
#endif

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;

	if( g_libusb_cfg.req.request.bRequest == GET_LEN )
	{
		WriteReg32(tmp_addr, 0x2a);
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == GET_INFO )
	{
		WriteReg32(tmp_addr, 0x3);
		return 0;
	}
	else if( g_libusb_cfg.req.request.bRequest == SET_CUR )
	{
		brtmp = TempBuf[4] | (TempBuf[5] << 8) | (TempBuf[6] << 16) | (TempBuf [7] << 24);
		//g_encv_video_config.cur.dwBitRate = brtmp;
		encv_change_bitrate(brtmp);
		return 0;
	}
	else {
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		g_libusb_cfg.req.wLength = 0;
#ifdef USB_UART_PRINT
		debug_printf("Commit: invalid request\n");
#endif
		return 1;
	}
}

int HandleRequest_XU_encv(t_libuvc_ctrlif *vctrlif)
{
	switch( g_libusb_cfg.req.request.wValue_H )
    {
		case 0x01:
			HandleRequest_XU_encv_Probe(vctrlif);
			break;	
		case 0x02:
			HandleRequest_XU_encv_Commit();
			break;
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
			g_libusb_cfg.req.wLength = 0;
        	return 1;
    }
	 return 0;

}
