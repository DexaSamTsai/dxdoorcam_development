#include "includes.h"

#ifdef CONFIG_FMT_YUV_HS
	#define CONFIG_YUV_MODE_HW
#endif

#ifdef CONFIG_FMT_MIMG_HS
	//#define CONFIG_MIMG_MODE_HW
	#define CONFIG_MIMG_MODE_SW
#endif

#ifdef CONFIG_FMT_ENCV_HS
	//#define CONFIG_ENCV_STD
	#define CONFIG_ENCV_DEMO
#endif

#ifdef CONFIG_USB_UAC
	#define CONFIG_AUDIO_EFMT_F1
	#define CONFIG_AUDIO_CHANNELS	1
	#define CONFIG_AUDIO_ENC_EN
#endif

//video terminal and unit ID
#define VC_INPUT1_TERMINAL_ID			0x01
#define VC_OUTPUT1_TERMINAL_ID			0x02
#define VC_INPUT2_TERMINAL_ID			0x03
#define VC_OUTPUT2_TERMINAL_ID			0x04
#define VC_OUTPUT3_TERMINAL_ID			0x05
#define VC_OUTPUT4_TERMINAL_ID			0x06
#define VC_PROCESS_UNIT_ID				0x07
#define VC_EXTENSION_UNIT_ID			0x08
#define VC_SELECTOR_UNIT_ID				0x09
#define VC_ENCV_EXTENSION_UNIT_ID		0x0a

//audio unit id for topoloy
#define AC_INPUT1_TERMINAL_ID			0x01
#define AC_OUTPUT1_TERMINAL_ID			0x02
#define AC_INPUT2_TERMINAL_ID			0x03
#define AC_OUTPUT2_TERMINAL_ID			0x04
#define AC_OUTPUT3_TERMINAL_ID			0x05
#define AC_FEATURE_UNIT_ID				0x06
#define AC_SELECTOR_UNIT_ID				0x07
#define AC_INPUT3_TERMINAL_ID			0x08

//stream interface
#define UVC_CONTROL_INTERFACE			0x00
#ifdef CONFIG_USB_UVC
#define UAC_CONTROL_INTERFACE_HS		(sizeof(_libuvc_strmif_hs)/sizeof(_libuvc_strmif_hs[0]) + 1)
#define VENDOR_MSC_INTERFACE_HS			(sizeof(_libuvc_strmif_hs)/sizeof(_libuvc_strmif_hs[0]) + sizeof(_libuac_strmif_hs)/sizeof(_libuac_strmif_hs[0]) + 2)
#ifdef CONFIG_USB11_EN
#define UAC_CONTROL_INTERFACE_FS		(sizeof(_libuvc_strmif_fs)/sizeof(_libuvc_strmif_fs[0]) + 1)
#endif
#else//audio only
#define UAC_CONTROL_INTERFACE_HS		0x00
#define UAC_CONTROL_INTERFACE_FS		0x00
#endif

//stream endpoint
//the following 2 macro define only one be used at the same time 
#define UVC_STATUS_ENDPOINT				0x81
#define UVC_STREAM0_IN_ENDPOINT			0x81
#define UVC_STREAM1_IN_ENDPOINT			0x82
#define UVC_STREAM2_IN_ENDPOINT			0x84
//the following 4 macro define only one be used at the same time 
#define UVC_STREAM3_IN_ENDPOINT			0x82
#define UVC_STREAM_OUT_ENDPOINT			0x82
#define UAC_STREAM1_OUT_ENDPOINT		0x02
#define VENDOR_MSC_BULK_OUT_ENDPOINT	0x02
#ifndef CONFIG_AUDIO_EFMT_F1
#define VENDOR_MSC_BULK_IN_ENDPOINT		0x85
#else
#define VENDOR_MSC_BULK_IN_ENDPOINT		0x86
#endif
#define UAC_STREAM1_IN_ENDPOINT			0x85
#define UAC_STREAM2_IN_ENDPOINT			0x86
#define UAC_STREAM2_OUT_ENDPOINT		0x06

//stream payload size
#ifdef CONFIG_YUV_MODE_SW
//#define HS_PYLSIZE_YUV					HS_MAX_PKTSIZE_BULK
#define HS_PYLSIZE_YUV					HS_MAX_PKTSIZE_ISO
#else
#define HS_PYLSIZE_YUV					0x1400
#endif
#define FS_PYLSIZE_YUV					0x0380
#define HS_PYLSIZE_MIMG					HS_MAX_PKTSIZE_ISO 
#define FS_PYLSIZE_MIMG					FS_MAX_PKTSIZE_ISO
#define HS_PYLSIZE_ENCV					HS_MAX_PKTSIZE_ISO 
#define FS_PYLSIZE_ENCV					FS_MAX_PKTSIZE_ISO
#define HS_PYLSIZE_PCM					0x0200
#define FS_PYLSIZE_PCM					0x0040
#define HS_PYLSIZE_BULK					0x0200
#define FS_PYLSIZE_BULK					0x0040


#define UVC_XU_GUID						"DD880F8A-1CBA-4954-8A25-F7875967F0F7"


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef CONFIG_USB_UVC

void usbclass_HandleUAV(u32 *args);
int HandleRequest_XU_encv(t_libuvc_ctrlif *vctrlif);

#ifdef CONFIG_YUV_MODE_SW
int yuv_encode_data(void *data, char *buffer, int length);
int yuv_frameset_default(u32 frame_size, u8 frame_rate);
#endif

#ifdef CONFIG_YUV_MODE_HW
int hwyuv_encode_data(void *data, char *buffer, int length);
int hwyuv_frameset_default(u32 frame_size, u8 frame_rate);
#endif

#ifdef CONFIG_MIMG_MODE_HW
int mimg_encode_data(void *data, char *buffer, int length);
int mimg_frameset_default(u32 frame_size, u8 frame_rate);
#endif

#if defined(CONFIG_FMT_ENCV_HS) || defined(CONFIG_MIMG_MODE_SW)
int dpath_encode_data(void *data, char *buffer, int length);
int dpath_frameset_default(u32 frame_size, u8 frame_rate);

#ifdef CONFIG_MIMG_MODE_SW
	#define mimg_encode_data dpath_encode_data
	#define mimg_frameset_default dpath_frameset_default
#endif

#ifdef CONFIG_FMT_ENCV_HS
	#define encv_encode_data dpath_encode_data
	#define encv_frameset_default dpath_frameset_default
#endif

#endif
		
t_libuvc_strmif _libuvc_strmif_hs[] =
{
#ifdef CONFIG_FMT_YUV_HS
	{
		.STRMIF_ID 	= UVC_CONTROL_INTERFACE + 1,
		.STRM_EP 	= UVC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE  = HS_PYLSIZE_YUV,
		.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
		.STRM_APP = VIDEO_STREAM_APP_ENC,	

		.format		= VS_FORMAT_UNCOMPRESSED, 
		.v_status 	= UVC_STATUS_IDLE,
		.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
		.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

		.filldata	= uvc_filldata,
#ifdef CONFIG_YUV_MODE_SW
		.frame_set 	= yuv_frameset_default,
		.encode 	= yuv_encode_data,
#else
		.frame_set 	= hwyuv_frameset_default,
		.encode 	= hwyuv_encode_data,
#endif
		.framerate = 30,

		.max_size = VIDEO_SIZE_VGA,
		.min_size = VIDEO_SIZE_160_112,
		.framerate_max = 30,
		.framerate_min = 1,

		.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
		.still_trigger = 0,
	},
#endif

#ifdef CONFIG_FMT_MIMG_HS
	{
		#ifdef CONFIG_FMT_YUV_HS
		.STRMIF_ID = UVC_CONTROL_INTERFACE + 2,
		.STRM_EP = UVC_STREAM3_IN_ENDPOINT,
		.STRMLINK_ID = VC_OUTPUT2_TERMINAL_ID,
		#else
		.STRMIF_ID = UVC_CONTROL_INTERFACE + 1,
		.STRM_EP = UVC_STREAM1_IN_ENDPOINT,
		.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
		#endif
		.STRM_PYLSIZE = HS_PYLSIZE_MIMG,
		.STRM_APP = VIDEO_STREAM_APP_ENC,	

		.format		= VS_FORMAT_MIMG, 
		.v_status = UVC_STATUS_IDLE,
		.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
		.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

		.filldata	= uvc_filldata,
		#ifdef CONFIG_MIMG_SRC_DECODE
		.frame_set = mimg_dec_frameset_default,
		#else
			#ifdef CONFIG_FMT_ENCV_HS
			.frame_set = dual_mimg_frameset_default,
			.encode = dual_mimg_encode_data,
			#else
			.frame_set = mimg_frameset_default,
				#ifndef CONFIG_MIMG_MODE_HW
				.encode = mimg_encode_data,
				#endif
			#endif
		#endif
		.framerate = 30,

		.max_size = VIDEO_SIZE_720P,
		.min_size = VIDEO_SIZE_160_112,
		.framerate_max = 30,
		.framerate_min = 1,

		.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
		.still_trigger = 0,
	},
#endif

#ifdef CONFIG_FMT_ENCV_HS
	#if defined(CONFIG_FMT_YUV_HS) && defined(CONFIG_FMT_MIMG_HS)//"yuv+jpg+encv" three streaming video
			 {
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 3,
				.STRM_EP = UVC_STREAM2_IN_ENDPOINT,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				.STRMLINK_ID = VC_OUTPUT3_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_ENC,	
				
				.v_status = UVC_STATUS_IDLE,
				.format = VS_FORMAT_FRAME_BASED,
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c
				
				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,

				.frame_set = encv_frameset_default,
				.encode = encv_encode_data,
				.filldata	= uvc_filldata,

				.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
				.still_trigger = 0,
			},
		#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
		#else
			#ifdef CONFIG_ENCV_STD_DIR_OUT
			#else
			#endif
		#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
	#else//not (CONFIG_FMT_YUV_HS && CONFIG_FMT_MIMG_HS)
		#if defined(CONFIG_FMT_YUV_HS) || defined(CONFIG_FMT_MIMG_HS)//"yuv+encv" or "jpg+encv" two streaming video
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
			{//for in
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 2,
				.STRM_EP = UVC_STREAM2_IN_ENDPOINT,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				.STRMLINK_ID = VC_OUTPUT2_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_ENC,	
				
				.v_status = UVC_STATUS_IDLE,
				.format = VS_FORMAT_FRAME_BASED,
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c
				
				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,

				.frame_set = encv_frameset_default,
				.encode = encv_encode_data,
				.filldata	= uvc_filldata,

				.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
				.still_trigger = 0,
			},
			{//for out
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 3,
				.STRM_EP = UVC_STREAM_OUT_ENDPOINT,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				.STRMLINK_ID = VC_INPUT2_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_DEC,	
				
				.v_status = UVC_STATUS_IDLE,
				.format = VS_FORMAT_FRAME_BASED,
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,

			},
			#else//not CONFIG_ENCV_STD_DIR_DUPLEX 
				#ifdef CONFIG_ENCV_STD_DIR_OUT
				{//for out
					.STRMIF_ID = UVC_CONTROL_INTERFACE + 2,
					.STRM_EP = UVC_STREAM_OUT_ENDPOINT,
					.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
					.STRMLINK_ID = VC_INPUT2_TERMINAL_ID,
					.STRM_APP = VIDEO_STREAM_APP_DEC,	
					
					.v_status = UVC_STATUS_IDLE,
					.format = VS_FORMAT_FRAME_BASED,
					.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
					.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

					.max_size = VIDEO_SIZE_720P,
					.min_size = VIDEO_SIZE_160_128,
					.framerate_max = 30,
					.framerate_min = 1,
				},
				#else // single "encv" video stream
				{
					.STRMIF_ID = UVC_CONTROL_INTERFACE + 2,
					.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
					.STRM_EP = UVC_STREAM2_IN_ENDPOINT,
					.STRMLINK_ID = VC_OUTPUT2_TERMINAL_ID,
					.STRM_APP = VIDEO_STREAM_APP_ENC,	
					
					.v_status = UVC_STATUS_IDLE,
					#if defined(CONFIG_ENCV_STD) || defined(CONFIG_ENCV_DEMO)
					.format = VS_FORMAT_FRAME_BASED,
					#else
					.format = VS_FORMAT_MPEG2TS,
					#endif
					.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
					.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

					.max_size = VIDEO_SIZE_720P,
					.min_size = VIDEO_SIZE_160_128,
					.framerate_max = 30,
					.framerate_min = 1,

					.frame_set = encv_frameset_default,
					.encode = encv_encode_data,
					.filldata	= uvc_filldata,

					.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
					.still_trigger = 0,
				},
				#endif//end CONFIG_ENCV_STD_DIR_OUT
			#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
		#else//not (CONFIG_FMT_YUV_HS || CONFIG_FMT_MIMG_HS)//"encv" only streaming video
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
			{//for in
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 1,
				.STRM_EP = UVC_STREAM1_IN_ENDPOINT,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_ENC,	
				
				.v_status = UVC_STATUS_IDLE,
				.format = VS_FORMAT_FRAME_BASED,
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c
				
				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,

				.frame_set = encv_frameset_default,
				.encode = encv_encode_data,

				.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
				.still_trigger = 0,
			},
			{//for out
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 2,
				.STRM_EP = UVC_STREAM_OUT_ENDPOINT,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				.STRMLINK_ID = VC_INPUT2_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_DEC,	
				
				.v_status = UVC_STATUS_IDLE,
				.format = VS_FORMAT_FRAME_BASED,
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,
			},
			#else//not CONFIG_ENCV_STD_DIR_DUPLEX 
			{
				.STRMIF_ID = UVC_CONTROL_INTERFACE + 1,
				.STRM_PYLSIZE = HS_PYLSIZE_ENCV,
				#ifdef CONFIG_ENCV_STD_DIR_OUT
				.STRM_EP = UVC_STREAM_OUT_ENDPOINT,
				.STRMLINK_ID = VC_INPUT1_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_DEC,	
				#else
				.STRM_EP = UVC_STREAM1_IN_ENDPOINT,
				.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
				.STRM_APP = VIDEO_STREAM_APP_ENC,	
				#endif
				
				.v_status = UVC_STATUS_IDLE,
				#if defined(CONFIG_ENCV_STD) || defined(CONFIG_ENCV_DEMO)
				.format = VS_FORMAT_FRAME_BASED,
				#else
				.format = VS_FORMAT_MPEG2TS,
				#endif
				.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
				.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

				.max_size = VIDEO_SIZE_720P,
				.min_size = VIDEO_SIZE_160_128,
				.framerate_max = 30,
				.framerate_min = 1,

				#ifdef CONFIG_ENCV_STD_DIR_OUT
				#else
					.frame_set = encv_frameset_default,
					.encode = encv_encode_data,
					.filldata = uvc_filldata,

					.still_image = NULL,//fixed in ram/xxx/board_xxx.c if you need support still picture function
					.still_trigger = 0,
				#endif
			},
			#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
		#endif//end (CONFIG_FMT_YUV_HS || CONFIG_FMT_MIMG_HS)
	#endif//end (CONFIG_FMT_YUV_HS && CONFIG_FMT_MIMG_HS)
#endif//end CONFIG_FMT_ENCV_HS
};

t_libuvc_tid _libuvc_tid_hs[] = 
{
#ifdef CONFIG_FMT_YUV_HS
	{
		.IT_ID = VC_INPUT1_TERMINAL_ID,
		.OT_ID = VC_OUTPUT1_TERMINAL_ID,
	},
#endif


#ifdef CONFIG_FMT_MIMG_HS
	#ifdef CONFIG_FMT_YUV_HS
	{
		.IT_ID = VC_INPUT1_TERMINAL_ID,
		.OT_ID = VC_OUTPUT2_TERMINAL_ID,
	},
	#else
	{
		.IT_ID = VC_INPUT1_TERMINAL_ID,
		.OT_ID = VC_OUTPUT1_TERMINAL_ID,
	},
	#endif
#endif
	

#ifdef CONFIG_FMT_ENCV_HS
	#if defined(CONFIG_FMT_YUV_HS) && defined(CONFIG_FMT_MIMG_HS)
		#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
		{//for in
			.IT_ID = VC_INPUT1_TERMINAL_ID,
			.OT_ID = VC_OUTPUT3_TERMINAL_ID,
		},
		{//for out
			.IT_ID = VC_INPUT2_TERMINAL_ID,
			.OT_ID = VC_OUTPUT4_TERMINAL_ID,
		},
		#else
			#ifdef CONFIG_ENCV_STD_DIR_OUT
			{
				.IT_ID = VC_INPUT2_TERMINAL_ID,
				.OT_ID = VC_OUTPUT3_TERMINAL_ID,
			},
			#else
			{
				.IT_ID = VC_INPUT1_TERMINAL_ID,
				.OT_ID = VC_OUTPUT3_TERMINAL_ID,
			},
			#endif
		#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
	#else//not (CONFIG_FMT_YUV_HS && CONFIG_FMT_MIMG_HS)
		#if defined(CONFIG_FMT_YUV_HS) || defined(CONFIG_FMT_MIMG_HS)
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
			{//for in
				.IT_ID = VC_INPUT1_TERMINAL_ID,
				.OT_ID = VC_OUTPUT2_TERMINAL_ID,
			},
			{//for out
				.IT_ID = VC_INPUT2_TERMINAL_ID,
				.OT_ID = VC_OUTPUT3_TERMINAL_ID,
			},
			#else
				#ifdef CONFIG_ENCV_STD_DIR_OUT
				{
					.IT_ID = VC_INPUT2_TERMINAL_ID,
					.OT_ID = VC_OUTPUT2_TERMINAL_ID,
				},
				#else
				{
					.IT_ID = VC_INPUT1_TERMINAL_ID,
					.OT_ID = VC_OUTPUT2_TERMINAL_ID,
				},
				#endif
			#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
		#else
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX 
			{//for in
				.IT_ID = VC_INPUT1_TERMINAL_ID,
				.OT_ID = VC_OUTPUT1_TERMINAL_ID,
			},
			{//for out
				.IT_ID = VC_INPUT2_TERMINAL_ID,
				.OT_ID = VC_OUTPUT2_TERMINAL_ID,
			},
			#else
			{
				.IT_ID = VC_INPUT1_TERMINAL_ID,
				.OT_ID = VC_OUTPUT1_TERMINAL_ID,
			},
			#endif
		#endif//end (CONFIG_FMT_YUV_HS || CONFIG_FMT_MIMG_HS)
	#endif//end (CONFIG_FMT_YUV_HS && CONFIG_FMT_MIMG_HS)
#endif//end CONFIG_FMT_ENCV_HS
};

t_libuvc_ctrlif _libuvc_ctrlif_hs =
{
	.DO_SNR_EFFECT_PROC = 1,
	.CTRLIF_ID 			= UVC_CONTROL_INTERFACE,
	.SU_ID 				= VC_SELECTOR_UNIT_ID,
	.PU_ID 				= VC_PROCESS_UNIT_ID,
	.XU_ID 				= VC_EXTENSION_UNIT_ID,
	.XU_bmControl		= 0x07,
	.XU_GUID			= UVC_XU_GUID,
#ifdef CONFIG_ENCV_STD
	.H_XU_ID			= VC_ENCV_EXTENSION_UNIT_ID,	
	.encv_init			= 1,
	.encv_req_handle	= HandleRequest_XU_encv,
#endif

	.tid 				= &_libuvc_tid_hs[0],
	.tid_cnt 			= sizeof(_libuvc_tid_hs)/sizeof(_libuvc_tid_hs[0]),
};


#ifdef CONFIG_USB11_EN
t_libuvc_strmif _libuvc_strmif_fs[] =
{
#ifdef CONFIG_FMT_YUV_FS
	{
		.STRMIF_ID 	= UVC_CONTROL_INTERFACE + 1,
		.STRM_EP 	= UVC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE 	= FS_PYLSIZE_YUV,
		.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
		.STRM_APP = VIDEO_STREAM_APP_ENC,	

		.format		= VS_FORMAT_UNCOMPRESSED, 
		.v_status 	= UVC_STATUS_IDLE,
		.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
		.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

		.filldata	= uvc_filldata,
#ifdef CONFIG_YUV_MODE_SW
		.frame_set 	= yuv_frameset_default,
		.encode 	= yuv_encode_data,
#else
		.frame_set 	= hwyuv_frameset_default,
		.encode 	= hwyuv_encode_data,
#endif
		.framerate = 10,
	},
#endif

#ifdef CONFIG_FMT_MIMG_FS
	{
		.STRMIF_ID 	= UVC_CONTROL_INTERFACE + 1,
		.STRM_EP 	= UVC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE 	= FS_PYLSIZE_MIMG,
		.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
		.STRM_APP = VIDEO_STREAM_APP_ENC,	

		.format		= VS_FORMAT_MIMG, 
		.v_status 	= UVC_STATUS_IDLE,
		.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
		.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

		.frame_set 	= mimg_frameset_default,
		.encode 	= mimg_encode_data,
		.filldata	= uvc_filldata,
		.framerate = 10,
	},
#endif

#ifdef CONFIG_FMT_ENCV_FS
	{
		.STRMIF_ID 	= UVC_CONTROL_INTERFACE + 1,
		.STRM_EP 	= UVC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE 	= FS_PYLSIZE_ENCV,
		.STRMLINK_ID = VC_OUTPUT1_TERMINAL_ID,
		.STRM_APP = VIDEO_STREAM_APP_ENC,	

		.format 	= VS_FORMAT_FRAME_BASED,
		.v_status 	= UVC_STATUS_IDLE,
		.frame 		= NULL,//fixed in ram/xxx/board_xxx.c
		.frame_cnt 	= 0,//fixed in ram/xxx/board_xxx.c

		.frame_set = encv_frameset_default,
		.encode = encv_encode_data,
		.framerate = 10,
	},
#endif
};

t_libuvc_tid _libuvc_tid_fs[] = 
{
#if defined(CONFIG_FMT_YUV_FS) || defined(CONFIG_FMT_MIMG_FS) || defined(CONFIG_FMT_ENCV_FS)
	{
		.IT_ID = VC_INPUT1_TERMINAL_ID,
		.OT_ID = VC_OUTPUT1_TERMINAL_ID,
	},
#endif
};

t_libuvc_ctrlif _libuvc_ctrlif_fs =
{
	.DO_SNR_EFFECT_PROC = 1,
	.CTRLIF_ID 			= UVC_CONTROL_INTERFACE,
	.SU_ID 				= VC_SELECTOR_UNIT_ID,
	.PU_ID 				= VC_PROCESS_UNIT_ID,
	.XU_ID 				= VC_EXTENSION_UNIT_ID,
	.XU_bmControl		= 0x07,
	.XU_GUID			= UVC_XU_GUID,
#ifdef CONFIG_ENCV_STD
	.H_XU_ID			= VC_ENCV_EXTENSION_UNIT_ID,	
	.encv_init			= 1,
	.encv_req_handle	= HandleRequest_XU_encv,
#endif

	.tid 				= &_libuvc_tid_fs[0],
	.tid_cnt 			= sizeof(_libuvc_tid_fs)/sizeof(_libuvc_tid_fs[0]),
};
#endif//CONFIG_USB11_EN
#endif //CONFIG_USB_UVC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef CONFIG_USB_UAC

// pcm encode interface
void uac_epcm_HandleSetif(t_libuac_strmif *strmif, int alt);
void uac_pcm_filldata(t_libuac_strmif *strmif);
int uac_epcm_HandleSetSampleRate(t_libuac_strmif *strmif, u32 asample_rate);
u32 uac_epcm_HandleGetSampleRate(void);
u32 uac_epcm_HandleFu(u32 id, short value);

// pcm decode interface
void uac_dpcm_HandleSetif(t_libuac_strmif *strmif, int alt);
void uac_pcm_draindata(t_libuac_strmif *strmif);
int uac_dpcm_HandleSetSampleRate(t_libuac_strmif *strmif, u32 asample_rate);
u32 uac_dpcm_HandleGetSampleRate(void);

///////////////////////////////////////////////////////////////////
#ifdef CONFIG_AUDIO_DEC_EN
u32 _libuac_strmif_sr_spk[] = 
{
	#ifdef CONFIG_SPK_SR_48000
	48000,
	#endif
	#ifdef CONFIG_SPK_SR_44100
	44100,
	#endif
	#ifdef CONFIG_SPK_SR_32000
	32000,
	#endif
	#ifdef CONFIG_SPK_SR_24000
	24000,
	#endif
	#ifdef CONFIG_SPK_SR_22050
	22050,
	#endif
	#ifdef CONFIG_SPK_SR_16000
	16000,
	#endif
	#ifdef CONFIG_SPK_SR_12000
	12000,
	#endif
	#ifdef CONFIG_SPK_SR_11025
	11025,
	#endif
	#ifdef CONFIG_SPK_SR_8000
	8000,
	#endif
};
#endif //CONFIG_AUDIO_DEC_EN

#if defined(CONFIG_AUDIO_ENC_EN)
u32 _libuac_strmif_sr_mic[] = 
{
	#ifdef CONFIG_MIC_SR_48000
	48000,
	#endif
	#ifdef CONFIG_MIC_SR_44100
	44100,
	#endif
	#ifdef CONFIG_MIC_SR_32000
	32000,
	#endif
	#ifdef CONFIG_MIC_SR_24000
	24000,
	#endif
	#ifdef CONFIG_MIC_SR_22050
	22050,
	#endif
	#ifdef CONFIG_MIC_SR_16000
	16000,
	#endif
	#ifdef CONFIG_MIC_SR_12000
	12000,
	#endif
	#ifdef CONFIG_MIC_SR_11025
	11025,
	#endif
	#ifdef CONFIG_MIC_SR_8000
	8000,
	#endif
};
#endif //CONFIG_AUDIO_ENC_EN
///////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////
//generate audio streaming struct
#if defined(CHIP_R1) && defined(CONFIG_MIMG_SRC_DECODE)
t_libuac_strmif _libuac_strmif_hs[] = 
{
	{
		.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 1,
		.STRM_EP = UAC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		.STRMLINK_ID = AC_OUTPUT1_TERMINAL_ID,

		.STRM_APP = AUDIO_STREAM_APP_MIC,		
		.format = UAC_FORMAT_TYPE_I_PCM,	

        .sr_all = &_libuac_strmif_sr_mic[0],
        .sr_cnt = sizeof(_libuac_strmif_sr_mic) / sizeof(_libuac_strmif_sr_mic[0]),
		.chn_cnt = 1,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_dec_HandleSetif,
		.filldata = uac_dec_filldata,
		.setsamplerate = uac_dec_HandleSetSampleRate,
		.getsamplerate = uac_dec_HandleGetSampleRate,
	},
};

#else
t_libuac_strmif _libuac_strmif_hs[] = 
{
#ifdef CONFIG_AUDIO_DFMT_F1
	{
		#if defined(CONFIG_UVC_ENCVTS_MIMG) && defined(CHIP_R1)
		.STRMIF_ID = 0x03,
		#else
		.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 1,
		#endif
		.STRM_EP = UAC_STREAM1_OUT_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		.STRMLINK_ID = AC_INPUT1_TERMINAL_ID,

		.STRM_APP = AUDIO_STREAM_APP_SPK,
		.format = UAC_FORMAT_TYPE_I_PCM,

		.sr_all =  &_libuac_strmif_sr_spk[0],
		.sr_cnt = sizeof(_libuac_strmif_sr_spk)/sizeof(_libuac_strmif_sr_spk[0]),
		.chn_cnt = CONFIG_AUDIO_CHANNELS,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_dpcm_HandleSetif,
		.filldata = uac_pcm_draindata,
		.setsamplerate = uac_dpcm_HandleSetSampleRate,
		.getsamplerate = uac_dpcm_HandleGetSampleRate,
	},
#endif

#if defined(CONFIG_AUDIO_DFMT_F3) && !defined(CHIP_R1)
	{
		#ifdef CONFIG_AUDIO_DFMT_F1
		.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 2,
		#else
		.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 1,
		#endif
		.STRM_EP = UAC_STREAM2_OUT_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		#ifdef CONFIG_AUDIO_DFMT_F1
		.STRMLINK_ID = AC_INPUT3_TERMINAL_ID,
		#else
		.STRMLINK_ID = AC_INPUT1_TERMINAL_ID,
		#endif

		.STRM_APP = AUDIO_STREAM_APP_SPK,
//		.format = UAC_FORMAT_TYPE_II_MPEG,
		.format = UAC_FORMAT_TYPE_I_PCM8,

		.sr_all =  &_libuac_strmif_sr_spk[0],
		.sr_cnt = sizeof(_libuac_strmif_sr_spk)/sizeof(_libuac_strmif_sr_spk[0]),
		.chn_cnt = CONFIG_AUDIO_CHANNELS,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_dpcm_HandleSetif,
		.filldata = uac_pcm_draindata,
		.setsamplerate = uac_dpcm_HandleSetSampleRate,
		.getsamplerate = uac_dpcm_HandleGetSampleRate,
	},
#endif

#ifdef CONFIG_AUDIO_EFMT_F1
	{
		#if defined(CONFIG_UVC_ENCVTS_MIMG) && defined(CHIP_R1)
		.STRMIF_ID = 0x03,
		#else
			#if defined(CONFIG_AUDIO_DFMT_F1) && defined(CONFIG_AUDIO_DFMT_F3) 
			.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 3,
			#else
				#if defined(CONFIG_AUDIO_DFMT_F1) || defined(CONFIG_AUDIO_DFMT_F3)	
				.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 2,
				#else
				.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 1,
				#endif
			#endif
		#endif
		.STRM_EP = UAC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		#if defined(CONFIG_AUDIO_DFMT_F1) || defined(CONFIG_AUDIO_DFMT_F3)	
		.STRMLINK_ID = AC_OUTPUT2_TERMINAL_ID,
		#else
		.STRMLINK_ID = AC_OUTPUT1_TERMINAL_ID,
		#endif

		.STRM_APP = AUDIO_STREAM_APP_MIC,
		.format = UAC_FORMAT_TYPE_I_PCM,

		.sr_all =  &_libuac_strmif_sr_mic[0],
		.sr_cnt = sizeof(_libuac_strmif_sr_mic)/sizeof(_libuac_strmif_sr_mic[0]),
		.chn_cnt = CONFIG_AUDIO_CHANNELS,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_epcm_HandleSetif,
		#if defined(CONFIG_DWEBSDK_PROJECT) && defined(CHIP_R2) 
		.filldata = NULL,
		#else
		.filldata = uac_pcm_filldata,
		#endif
		.setsamplerate = uac_epcm_HandleSetSampleRate,
		.getsamplerate = uac_epcm_HandleGetSampleRate,
	},
#endif

#ifdef CONFIG_AUDIO_EFMT_F4
	{
		#if defined(CONFIG_UVC_ENCVTS_MIMG) && defined(CHIP_R1)
		.STRMIF_ID = 0x03,
		#else
			#if defined(CONFIG_AUDIO_DFMT_F1) && defined(CONFIG_AUDIO_EFMT_F1)
			.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 3,
			#else
				#if defined(CONFIG_AUDIO_DFMT_F1) || defined(CONFIG_AUDIO_EFMT_F1)
				.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 2,
				#else
				.STRMIF_ID = UAC_CONTROL_INTERFACE_HS + 1,
				#endif
			#endif
		#endif
		.STRM_EP = UAC_STREAM2_IN_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		#if defined(CONFIG_AUDIO_DFMT_F1) && defined(CONFIG_AUDIO_EFMT_F1)
		.STRMLINK_ID = AC_OUTPUT3_TERMINAL_ID,
		#else
			#if defined(CONFIG_AUDIO_DFMT_F1) || defined(CONFIG_AUDIO_EFMT_F1)
			.STRMLINK_ID = AC_OUTPUT2_TERMINAL_ID,
			#else
			.STRMLINK_ID = AC_OUTPUT1_TERMINAL_ID,
			#endif
		#endif

		.STRM_APP = AUDIO_STREAM_APP_MIC,
		.format = UAC_FORMAT_TYPE_I_PCM8,

		.sr_all =  &_libuac_strmif_sr_mic[0],
		.sr_cnt = sizeof(_libuac_strmif_sr_mic)/sizeof(_libuac_strmif_sr_mic[0]),
		.chn_cnt = CONFIG_AUDIO_CHANNELS,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_epcm_HandleSetif,
		.filldata = uac_pcm_filldata,
		.setsamplerate = uac_epcm_HandleSetSampleRate,
		.getsamplerate = uac_epcm_HandleGetSampleRate,
	},
#endif
};
#endif//defined(CHIP_R1) && defined(CONFIG_MIMG_SRC_DECODE)

///////////////////////////////////////////////////////////////////

	
#if defined(CHIP_R1) && defined(CONFIG_MIMG_SRC_DECODE)
t_libuac_tid _libuac_tid_hs[] = 
{
	{
		.IT_ID = AC_INPUT1_TERMINAL_ID,
		.OT_ID = AC_OUTPUT1_TERMINAL_ID,
	},
};

#else
t_libuac_tid _libuac_tid_hs[] = 
{
#ifdef CONFIG_AUDIO_DFMT_F1
	{
		.IT_ID = AC_INPUT1_TERMINAL_ID,
		.OT_ID = AC_OUTPUT1_TERMINAL_ID,
	},
#endif

#if defined(CONFIG_AUDIO_DFMT_F3) && !defined(CHIP_R1)
	{
		#ifdef CONFIG_AUDIO_DFMT_F1
		.IT_ID = AC_INPUT3_TERMINAL_ID,
		#else
		.IT_ID = AC_INPUT1_TERMINAL_ID,
		#endif

		.OT_ID = AC_OUTPUT1_TERMINAL_ID,
	},
#endif

#ifdef CONFIG_AUDIO_EFMT_F1
	#if defined(CONFIG_AUDIO_DFMT_F1) || defined(CONFIG_AUDIO_DFMT_F3)	
	{
		.IT_ID = AC_INPUT2_TERMINAL_ID,
		.OT_ID = AC_OUTPUT2_TERMINAL_ID,
	},
	#else
	{
		.IT_ID = AC_INPUT1_TERMINAL_ID,
		.OT_ID = AC_OUTPUT1_TERMINAL_ID,
	},
	#endif
#endif


#ifdef CONFIG_AUDIO_EFMT_F4
	#if defined(CONFIG_AUDIO_DFMT_F1) && defined(CONFIG_AUDIO_EFMT_F1)
	{
		.IT_ID = AC_INPUT2_TERMINAL_ID,
		.OT_ID = AC_OUTPUT3_TERMINAL_ID,
	},
	#else
		#ifdef CONFIG_AUDIO_DFMT_F1
		{
			.IT_ID = AC_INPUT2_TERMINAL_ID,
			.OT_ID = AC_OUTPUT2_TERMINAL_ID,
		},
		#else
			#ifdef CONFIG_AUDIO_EFMT_F1
			{
				.IT_ID = AC_INPUT1_TERMINAL_ID,
				.OT_ID = AC_OUTPUT2_TERMINAL_ID,
			},
			#else
			{
				.IT_ID = AC_INPUT1_TERMINAL_ID,
				.OT_ID = AC_OUTPUT1_TERMINAL_ID,
			},
			#endif//CONFIG_AUDIO_EFMT_F1
		#endif//CONFIG_AUDIO_DFMT_F1
	#endif//defined(CONFIG_AUDIO_DFMT_F1) && defined(CONFIG_AUDIO_EFMT_F1)
#endif//CONFIG_AUDIO_EFMT_F4
};
#endif//defined(CHIP_R1) && defined(CONFIG_MIMG_SRC_DECODE)

t_libuac_ctrlif _libuac_ctrlif_hs = 
{
	.CTRLIF_ID = UAC_CONTROL_INTERFACE_HS,
	.FU_ID_MIN = AC_FEATURE_UNIT_ID,
	.FU_ID_MAX = AC_FEATURE_UNIT_ID,
	.SU_ID = AC_SELECTOR_UNIT_ID,

	.tid = &_libuac_tid_hs[0],
	.tid_cnt = sizeof(_libuac_tid_hs)/sizeof(_libuac_tid_hs[0]),

#ifdef CONFIG_AUDIO_ENC_EN
	.ac_fu_handler = uac_epcm_HandleFu,
#endif
};

#ifdef CONFIG_USB11_EN
t_libuac_strmif _libuac_strmif_fs[] = 
{
#ifdef CONFIG_AUDIO_EFMT_F1
	{
		.STRMIF_ID = UAC_CONTROL_INTERFACE_FS + 1,
		.STRM_EP = UAC_STREAM1_IN_ENDPOINT,
		.STRM_PYLSIZE = HS_PYLSIZE_PCM,
		.STRMLINK_ID = AC_OUTPUT1_TERMINAL_ID,

		.STRM_APP = AUDIO_STREAM_APP_MIC,
		.format = UAC_FORMAT_TYPE_I_PCM,

		.sr_all =  &_libuac_strmif_sr_mic[0],
		.sr_cnt = sizeof(_libuac_strmif_sr_mic)/sizeof(_libuac_strmif_sr_mic[0]),
		.chn_cnt = CONFIG_AUDIO_CHANNELS,

		.a_status = UVC_STATUS_IDLE,

		.setif = uac_epcm_HandleSetif,
		.filldata = uac_pcm_filldata,
		.setsamplerate = uac_epcm_HandleSetSampleRate,
		.getsamplerate = uac_epcm_HandleGetSampleRate,
	},
#endif
};

t_libuac_tid _libuac_tid_fs[] = 
{
	{
		.IT_ID = AC_INPUT1_TERMINAL_ID,
		.OT_ID = AC_OUTPUT1_TERMINAL_ID,
	},
};

t_libuac_ctrlif _libuac_ctrlif_fs = 
{
	.CTRLIF_ID = UAC_CONTROL_INTERFACE_FS,
	.FU_ID_MIN = AC_FEATURE_UNIT_ID,
	.FU_ID_MAX = AC_FEATURE_UNIT_ID,
	.SU_ID = AC_SELECTOR_UNIT_ID,

	.tid = &_libuac_tid_fs[0],
	.tid_cnt = sizeof(_libuac_tid_fs)/sizeof(_libuac_tid_fs[0]),

#ifdef CONFIG_AUDIO_ENC_EN
	.ac_fu_handler = uac_epcm_HandleFu,
#endif
};
#endif //end CONFIG_USB11_EN
#endif //end CONFIG_USB_UAC
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef CONFIG_USB_VENDOR_MSC

void Vendor_MSC_Init(struct s_libvmsc_cfg *vmsc_cfg);
void Vendor_MSC_Handle_scif(void);

t_libvmsc_cfg _libvmsc_cfg_hs = 
{
	.ep_in				= VENDOR_MSC_BULK_IN_ENDPOINT,
	.ep_out				= VENDOR_MSC_BULK_OUT_ENDPOINT,

	.MSCIF_ID			= VENDOR_MSC_INTERFACE_HS,

	.init				= Vendor_MSC_Init,

	.Handle_VMSC		= Vendor_MSC_Handle_scif,
};
#endif
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

t_libiad_cfg _libiad_cfg_hs[] =
{
#ifdef CONFIG_USB_UVC
	{
		.intr_ep = UVC_STATUS_ENDPOINT,    

		.iad_app_type = USB_IAD_APP_VIDEO,

		{{
		.ctrlif 		= &_libuvc_ctrlif_hs,
		.strmif 		= &_libuvc_strmif_hs[0],
		.strmif_cnt 	= sizeof(_libuvc_strmif_hs)/sizeof(_libuvc_strmif_hs[0]),
		}}
	},
#endif

#ifdef CONFIG_USB_UAC
	{
		.iad_app_type = USB_IAD_APP_AUDIO,
		
		{{
		.ctrlif 		= &_libuac_ctrlif_hs,
		.strmif 		= &_libuac_strmif_hs[0],
		.strmif_cnt 	= sizeof(_libuac_strmif_hs)/sizeof(_libuac_strmif_hs[0]) ,
		}},
	},
#endif
};

t_libuav_cfg _libuav_cfg_hs =
{
	.iad = &_libiad_cfg_hs[0],
	.iad_cnt = sizeof(_libiad_cfg_hs)/sizeof(_libiad_cfg_hs[0]),

#ifdef CONFIG_USB_VENDOR_MSC
	.extra_cfg = &_libvmsc_cfg_hs,
#endif
};

#ifdef CONFIG_USB11_EN
t_libiad_cfg _libiad_cfg_fs[] =
{
#ifdef CONFIG_USB_UVC
	{
		.intr_ep = UVC_STATUS_ENDPOINT,    

		.iad_app_type = USB_IAD_APP_VIDEO,

		{{
		.ctrlif 		= &_libuvc_ctrlif_fs,
		.strmif 		= &_libuvc_strmif_fs[0],
		.strmif_cnt 	= sizeof(_libuvc_strmif_fs)/sizeof(_libuvc_strmif_fs[0]),
	 	}}
	},
#endif
	
#ifdef CONFIG_USB_UAC
	{
		.iad_app_type = USB_IAD_APP_AUDIO,
		
		{{
		.ctrlif 		= &_libuac_ctrlif_fs,
		.strmif 		= &_libuac_strmif_fs[0],
		.strmif_cnt 	= sizeof(_libuac_strmif_fs)/sizeof(_libuac_strmif_fs[0]),
		}},
	},
#endif
};

t_libuav_cfg _libuav_cfg_fs =
{
	.iad = &_libiad_cfg_fs[0],
	.iad_cnt = sizeof(_libiad_cfg_fs)/sizeof(_libiad_cfg_fs[0]),
};
#endif//CONFIG_USB11_EN

/******************************************************
 * USB class configuration which should match Descriptor
******************************************************/

t_libusbep_cfg libusbep_cfg_hs[] = {
#ifdef CONFIG_USB_UVC
	#ifdef CONFIG_FMT_YUV_HS
	{
		.ep 		= UVC_STREAM1_IN_ENDPOINT,
		.maxpktsize = HS_PYLSIZE_YUV,
#ifdef CONFIG_YUV_MODE_SW
		//.type 		= EPTYPE_BULK,
		.type 		= EPTYPE_ISO,
#else
		.type 		= EPTYPE_HW | EPTYPE_ISO,
#endif
		.baseaddr 	= 0x0,
		.buffernum 	= 6,//6k
		.interval = 0x01,
	},
	#endif

	#ifdef CONFIG_FMT_MIMG_HS
		#ifdef CONFIG_FMT_YUV_HS
		{
			.ep = UVC_STREAM3_IN_ENDPOINT,
			.type = EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_YUV,
			.intr_th = 0x2,
			.baseaddr = 0x12,
			.buffernum = 0x1,//3k
			.interval = 0x04,
		},
		#else
		{
			.ep = UVC_STREAM1_IN_ENDPOINT,
			.maxpktsize = HS_PYLSIZE_MIMG,
		#ifdef CONFIG_MIMG_MODE_HW
			.type 		= EPTYPE_HW | EPTYPE_ISO,
			.intr_th = 0x0,
		#else
			//.type 		= EPTYPE_BULK,
			.type 		= EPTYPE_ISO,
			.intr_th = 0x1,
		#endif
			.baseaddr = 0x0,
			#ifdef CONFIG_FMT_ENCV_HS
			.buffernum = 0x4,//4k
			#else
			.buffernum = 0x2,//6k
			#endif
			.interval = 0x01,
		},
		#endif
	#endif

	#ifdef CONFIG_FMT_ENCV_HS
		#if defined(CONFIG_FMT_YUV_HS) || defined(CONFIG_FMT_MIMG_HS)
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX
			{//for in
				.ep = UVC_STREAM2_IN_ENDPOINT,
				.type = EPTYPE_ISO,
				.maxpktsize = HS_PYLSIZE_ENCV,
				.intr_th = 0x3,
				#ifdef CHIP_R1
				.baseaddr = 0x6,
				.buffernum = 0x3,//1.5k
				#else
				.baseaddr = 0x8,
				.buffernum = 0x2,//2k
				#endif
				.interval = 0x01,
			},
			{//for out
				.ep = UVC_STREAM_OUT_ENDPOINT,
				.type = EPTYPE_ISO,
				.maxpktsize = HS_PYLSIZE_ENCV,
				.intr_th = 0x3,
				#ifdef CHIP_R1
				.baseaddr = 0x9,
				.buffernum = 0x3,//1.5k
				#else
				.baseaddr = 0xc,
				.buffernum = 0x2,//2k
				#endif
				.interval = 0x01,
			},
			#else
				#ifdef CONFIG_ENCV_STD_DIR_OUT
				{//for out
					.ep = UVC_STREAM_OUT_ENDPOINT,
					.type = EPTYPE_ISO,
					.maxpktsize = HS_PYLSIZE_ENCV,
					.intr_th = 0x3,
					#ifdef CHIP_R1
					.baseaddr = 0x6,
					.buffernum = 0x3,//1.5k
					#else
					.baseaddr = 0xc,
					.buffernum = 0x2,//2k
					#endif
					.interval = 0x01,
				},
				#else
				{//for in
					.ep = UVC_STREAM2_IN_ENDPOINT,
					.type = EPTYPE_ISO,
					.maxpktsize = HS_PYLSIZE_ENCV,
					.intr_th = 0x2,
					#ifdef CHIP_R1
					.baseaddr = 0x6,
					.buffernum = 0x3,//1.5k
					#else
						#ifdef CONFIG_FMT_YUV_HS
						.baseaddr = 0xc,
						#else
						.baseaddr = 0x0a,
						#endif
					.buffernum = 0x3,//2k
					#endif
					.interval = 0x01,
				},
				#endif
			#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
		#else//encv only
			#ifdef CONFIG_ENCV_STD_DIR_DUPLEX
			{//for in
				.ep = UVC_STREAM1_IN_ENDPOINT,
				.type = EPTYPE_ISO,
				.maxpktsize = HS_PYLSIZE_ENCV,
				.intr_th = 0x2,
				.baseaddr = 0x0,
				.buffernum = 0x4,//2k/4k
				.interval = 0x01,
			},
			{//for out
				.ep = UVC_STREAM_OUT_ENDPOINT,
				.type = EPTYPE_ISO,
				.maxpktsize = HS_PYLSIZE_ENCV,
				.intr_th = 0x2,
				.baseaddr = 0x8,
				.buffernum = 0x4,//2k/4k
				.interval = 0x01,
			},
			#else
				#ifdef CONFIG_ENCV_STD_DIR_OUT
				{//for out
					.ep = UVC_STREAM_OUT_ENDPOINT,
					.type = EPTYPE_ISO,
					.maxpktsize = HS_PYLSIZE_ENCV,
					.intr_th = 0x3,
					.baseaddr = 0x0,
					.buffernum = 0x6,//3k/6k
					.interval = 0x01,
				},
				#else
				{//for in
					.ep = UVC_STREAM1_IN_ENDPOINT,
					.type = EPTYPE_ISO,
					.maxpktsize = HS_PYLSIZE_ENCV,
					.intr_th = 0x3,
					.baseaddr = 0x0,
					.buffernum = 0x4,//2k/4k
					.interval = 0x01,
				},
				#endif
			#endif//end CONFIG_ENCV_STD_DIR_DUPLEX
		#endif//end CONFIG_FMT_YUV_HS || CONFIG_FMT_MIMG_HS
	#endif//end CONFIG_FMT_ENCV_HS

	{
		.ep 		= UVC_STATUS_ENDPOINT,
		.type 		= EPTYPE_INTR,
		.maxpktsize = HS_MAX_PKTSIZE_INTR,
		.intr_th 	= 1,
		#ifdef CHIP_R1
		.baseaddr 	= 0xc,
		#else
			#if defined(CONFIG_FMT_YUV_HS) && defined(CONFIG_FMT_MIMG_HS)
			.baseaddr 	= 0x13,
			#else
			.baseaddr 	= 0x0e,
			#endif
		#endif
		.buffernum 	= 1,  //no data
		.interval = 0x10,
	},
#endif//end CONFIG_USB_UVC

#ifdef CONFIG_USB_UAC
	#ifdef CHIP_R1
		#ifdef CONFIG_MIMG_SRC_DECODE
		{
			.ep 		= UAC_STREAM1_IN_ENDPOINT,
			.type 		= EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th 	= 1,
			.baseaddr 	= 0xc,
			.buffernum 	= 1,
			.interval = 0x04,
		},
		#else
		{
			#ifdef CONFIG_AUDIO_DEC_EN
			.ep 		= UAC_STREAM1_OUT_ENDPOINT,
			#endif
			#ifdef CONFIG_AUDIO_ENC_EN
			.ep 		= UAC_STREAM1_IN_ENDPOINT,
			#endif
			.type 		= EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th 	= 1,
			.baseaddr 	= 0xc,
			.buffernum 	= 1,
			.interval = 0x04,
		},
		#endif
	#else
		#ifdef CONFIG_AUDIO_DFMT_F1
		{
			.ep = UAC_STREAM1_OUT_ENDPOINT,
			.type = EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th = 0x1,
			.baseaddr = 0x13,
			.buffernum = 0x1,
			.interval = 0x04,
		},
		#endif

		#ifdef CONFIG_AUDIO_DFMT_F3
		{
			.ep = UAC_STREAM2_OUT_ENDPOINT,
			.type = EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th = 0x1,
			.baseaddr = 0x13,
			.buffernum = 0x1,
			.interval = 0x04,
		},
		#endif

		#ifdef CONFIG_AUDIO_EFMT_F1
		{
			.ep = UAC_STREAM1_IN_ENDPOINT,
			.type = EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th = 0x1,
			#ifdef CONFIG_USB_VENDOR_MSC
			.baseaddr = 0x1f,
			#else
			.baseaddr = 0x24,
			#endif
			.buffernum = 0x1,
			.interval = 0x04,
		},
		#endif

		#ifdef CONFIG_AUDIO_EFMT_F4
		{
			.ep = UAC_STREAM2_IN_ENDPOINT,
			.type = EPTYPE_ISO,
			.maxpktsize = HS_PYLSIZE_PCM,
			.intr_th = 0x1,
			#ifdef CONFIG_USB_VENDOR_MSC
			.baseaddr = 0x0f,
			#else
			.baseaddr = 0x12,
			#endif
			.buffernum = 0x1,//0.5k
			.interval = 0x04,
		},
		#endif
	#endif
#endif//CONFIG_USB_UAC

#ifdef CONFIG_USB_VENDOR_MSC
	{
		.ep = VENDOR_MSC_BULK_IN_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = HS_PYLSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x10,
		.buffernum = 0x2,//1k
		.interval = 0x00,
	},
	{
		.ep = VENDOR_MSC_BULK_OUT_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = HS_PYLSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x12,
		.buffernum = 0x2,//1k
		.interval = 0x00,
	},
#endif//CONFIG_USB_VENDOR_MSC
};

#ifdef CONFIG_USB11_EN
t_libusbep_cfg libusbep_cfg_fs[] = {
#ifdef CONFIG_USB_UVC
	#ifdef CONFIG_FMT_YUV_FS
	{
		.ep 		= UVC_STREAM1_IN_ENDPOINT,
		.maxpktsize = FS_PYLSIZE_YUV,
		#ifdef CONFIG_YUV_MODE_SW
		.type 		= EPTYPE_ISO,
		#else 	
		.type 		= EPTYPE_HW | EPTYPE_ISO,
		#endif
		.baseaddr 	= 0x0, 
		.buffernum 	= 6,
		.interval = 0x01,
	},
	#endif

	#ifdef CONFIG_FMT_MIMG_FS
	{
		.ep 		= UVC_STREAM1_IN_ENDPOINT,
		.maxpktsize = FS_PYLSIZE_MIMG,
		#ifdef CONFIG_MIMG_MODE_HW
		.type 		= EPTYPE_HW | EPTYPE_ISO,
		#else
		.type 		= EPTYPE_ISO,
		#endif
		.baseaddr 	= 0x0, 
		.buffernum 	= 6,
		.interval = 0x01,
	},
	#endif

	#ifdef CONFIG_FMT_ENCV_FS
	{
		.ep 		= UVC_STREAM1_IN_ENDPOINT,
		.type 		= EPTYPE_ISO,
		.maxpktsize = FS_PYLSIZE_ENCV,
		.baseaddr 	= 0x0, 
		.buffernum 	= 6,
		.interval = 0x01,
	},
	#endif

	{
		.ep 		= UVC_STATUS_ENDPOINT,
		.type 		= EPTYPE_INTR,
		.maxpktsize = FS_MAX_PKTSIZE_INTR,
		.intr_th 	= 1,
		.baseaddr 	= 0xc,
		.buffernum 	= 1,  //no data
		.interval = 0x10,
	},
#endif//CONFIG_USB_UVC

#ifdef CONFIG_USB_UAC
	{
		.ep 		= UAC_STREAM1_IN_ENDPOINT,
		.type 		= EPTYPE_ISO,
		.maxpktsize = FS_PYLSIZE_PCM,
		.intr_th 	= 1,
		.baseaddr 	= 0xc,
		.buffernum 	= 1,
		.interval = 0x01,
	}
#endif
};
#endif//CONFIG_USB11_EN

t_libusbclass_cfg g_libusbclass_cfg_hs =
{
	.epcfg 	= &libusbep_cfg_hs[0],
	.epnum 	= sizeof(libusbep_cfg_hs)/sizeof(libusbep_cfg_hs[0]),
	.arg 	= &_libuav_cfg_hs,
	.handleEP = usbclass_HandleUAV
};

#ifdef CONFIG_USB11_EN
t_libusbclass_cfg g_libusbclass_cfg_fs =
{
	.epcfg 	= &libusbep_cfg_fs[0],
	.epnum 	= sizeof(libusbep_cfg_fs)/sizeof(libusbep_cfg_fs[0]),
	.arg 	= &_libuav_cfg_fs,
	.handleEP = usbclass_HandleUAV,
};
#else
t_libusbclass_cfg g_libusbclass_cfg_fs;
#endif

