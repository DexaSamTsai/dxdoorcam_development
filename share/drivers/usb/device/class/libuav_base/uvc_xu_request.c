#include "includes.h"

//#undef USB_DEBUGREG
#define USB_DEBUGREG

#ifdef CT_PANTILT_EN
#define CT_PAN_MIN (0)
#define CT_PAN_MAX (1)
#define CT_PAN_DEF 0
#define CT_PAN_RES 3600
#define CT_TILT_MIN (0)
#define CT_TILT_MAX (1)
#define CT_TILT_DEF 0
#define CT_TILT_RES 3600

static s32 g_CT_PanLevel = CT_PAN_DEF;
static s32 g_CT_TiltLevel = CT_TILT_DEF;
static s32  g_CT_PanLevel_last = CT_PAN_DEF;
static s32 g_CT_TiltLevel_last = CT_TILT_DEF;
static u8 g_effect_pan_tilt = CT_PAN_DEF |(CT_TILT_DEF << 1);

void HandleRequest_CT_Pantilt_Absolute(void)	
{
	u32 TempValue;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	if( g_libusb_cfg.req.request.bRequest == UAV_SET_CUR )
	{
		//FIXME: some are 16bits data
		g_CT_PanLevel = g_libusb_cfg.req.databuf[0];
		g_CT_PanLevel |= g_libusb_cfg.req.databuf[1]<<8;
		g_CT_PanLevel |= g_libusb_cfg.req.databuf[2]<<16;
		g_CT_PanLevel |= g_libusb_cfg.req.databuf[3]<<24;
		g_CT_PanLevel /= CT_PAN_RES;
		g_CT_TiltLevel = g_libusb_cfg.req.databuf[4];
		g_CT_TiltLevel |= g_libusb_cfg.req.databuf[5]<<8;
		g_CT_TiltLevel |= g_libusb_cfg.req.databuf[6]<<16;
		g_CT_TiltLevel |= g_libusb_cfg.req.databuf[7]<<24;
		g_CT_TiltLevel /= CT_TILT_RES;
		if((g_CT_PanLevel >= CT_PAN_MIN) && (g_CT_PanLevel <= CT_PAN_MAX) )
		{
			g_CT_PanLevel_last = g_CT_PanLevel;
		}
		if((g_CT_TiltLevel >= CT_TILT_MIN) && (g_CT_TiltLevel <= CT_TILT_MAX) )
		{
			g_CT_TiltLevel_last = g_CT_TiltLevel;
		}

		if(g_effect_pan_tilt != (g_CT_PanLevel_last|(g_CT_TiltLevel_last << 1)))
		{
			g_effect_pan_tilt = g_CT_PanLevel_last|(g_CT_TiltLevel_last << 1);
			debug_printf("pan:%d tilt:%d \n",g_CT_PanLevel, g_CT_TiltLevel);
			sensor_effect_req(SNR_EFFECT_FLIP_MIR, g_effect_pan_tilt);
		}
		else if(((g_CT_TiltLevel <= CT_TILT_MIN) || (g_CT_TiltLevel >= CT_TILT_MAX) )&&((g_CT_PanLevel >= CT_PAN_MIN) && (g_CT_PanLevel <= CT_PAN_MAX) ))
		{
			g_libusb_cfg.req.error_code = UDEV_OUT_OF_RANGE;
		}
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_CUR )
	{
		WriteReg32(g_libusb_cfg.req.databuf, g_CT_PanLevel_last*CT_PAN_RES);
		WriteReg32(g_libusb_cfg.req.databuf+4, g_CT_TiltLevel_last*CT_TILT_RES);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_MIN )
	{
		WriteReg32(g_libusb_cfg.req.databuf, CT_PAN_MIN*CT_PAN_RES);
		WriteReg32(g_libusb_cfg.req.databuf+4, CT_TILT_MIN*CT_TILT_RES);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_MAX )
	{
		WriteReg32(g_libusb_cfg.req.databuf, CT_PAN_MAX*CT_PAN_RES);
		WriteReg32(g_libusb_cfg.req.databuf+4, CT_TILT_MAX*CT_TILT_RES);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_RES )
	{
		WriteReg32(g_libusb_cfg.req.databuf, CT_PAN_RES);
		WriteReg32(g_libusb_cfg.req.databuf+4, CT_TILT_RES);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_INFO )
	{
		WriteReg32(g_libusb_cfg.req.databuf, 0x3);
	}
	else if( g_libusb_cfg.req.request.bRequest == UAV_GET_DEF )
	{
		WriteReg32(g_libusb_cfg.req.databuf, CT_PAN_DEF*CT_PAN_RES);
		WriteReg32(g_libusb_cfg.req.databuf+4, CT_TILT_DEF*CT_TILT_RES);
	}
	else{
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		g_libusb_cfg.req.wLength = 0;
	}
}
#endif

#ifdef USB_DEBUGREG
#define ReadUSB					0x01
#define WriteUSB				0x02
#define ReadSensor				0x04
#define WriteSensor				0x08

u8 XU_TempValue[3];
u32 RegisterValue;
u32 RegisterAdd;
u8 countread;

u32 g_WriteSensorStep = 0;
u16 g_WriteSensorAddr;
t_libsccb_cfg g_libsccbcfg_sensor;

void HandleRequest_XU_ReadUSBRegister(void)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	g_libusb_cfg.req.databuf[0] = 0x00;
	g_libusb_cfg.req.databuf[1] = 0x00;
	g_libusb_cfg.req.databuf[2] = 0x00;
	g_libusb_cfg.req.databuf[3] = 0x00;
	return;

	switch( g_libusb_cfg.req.request.bRequest ){
	case UAV_SET_CUR:
		debug_printf("XU set:%x,%x,%x,%x\n", g_libusb_cfg.req.databuf[0], g_libusb_cfg.req.databuf[1], g_libusb_cfg.req.databuf[2], g_libusb_cfg.req.databuf[3] );
		debug_printf("XU set:%x,%x,%x,%x\n", g_libusb_cfg.req.request.bmRequestType, g_libusb_cfg.req.databuf[1], g_libusb_cfg.req.request.wValue_L);

		XU_TempValue[0] = g_libusb_cfg.req.databuf[0];
		XU_TempValue[1] = g_libusb_cfg.req.databuf[1];
		XU_TempValue[2] = g_libusb_cfg.req.databuf[2];

		switch(g_libusb_cfg.req.databuf[0] & 0x0f){
		case ReadUSB:
			switch(g_libusb_cfg.req.databuf[0] & 0xf0){
			case 0x40:
				RegisterAdd = (g_libusb_cfg.req.databuf[2] << 8) | g_libusb_cfg.req.databuf[1];
				break;
			case 0x80:
				RegisterAdd |= (g_libusb_cfg.req.databuf[2] << 24) | (g_libusb_cfg.req.databuf[1] << 16);
				RegisterValue = ReadReg32(RegisterAdd);
				break;
			default:
				break;
			}
			countread = 0;
			break;
		case WriteUSB:
			switch(g_libusb_cfg.req.databuf[0] & 0xf0){
			case 0x10:
				RegisterValue = (g_libusb_cfg.req.databuf[2] << 8) | g_libusb_cfg.req.databuf[1];
				break;
			case 0x20:
				RegisterValue |= (g_libusb_cfg.req.databuf[2] << 24) | (g_libusb_cfg.req.databuf[1] << 16);
				break;
			case 0x40:
				RegisterAdd = (g_libusb_cfg.req.databuf[2] << 8) | g_libusb_cfg.req.databuf[1];
				break;
			case 0x80:
				RegisterAdd |= (g_libusb_cfg.req.databuf[2] << 24) | (g_libusb_cfg.req.databuf[1] << 16);
				WriteReg32(RegisterAdd, RegisterValue);
				break;
			}
			break;
		case WriteSensor:
			sccb_set(&g_libsccbcfg_sensor);
			if(g_libsccbcfg_sensor.mode == SCCB_MODE8){
				sccb_wr(&g_libsccbcfg_sensor, g_libusb_cfg.req.databuf[1], g_libusb_cfg.req.databuf[2]);
			}
			else {			
				switch(g_WriteSensorStep)
				{
				case 0:
					if(g_libusb_cfg.req.databuf[0] == 0x18)
					{
						g_WriteSensorAddr = g_libusb_cfg.req.databuf[1] | (g_libusb_cfg.req.databuf[2]<<8);
						g_WriteSensorStep ++; 
					}
					else
					{
						g_WriteSensorStep = 0;
					}
//					debug_printf("ZZ: g_WriteSensorAddr = 0x%x\n", g_WriteSensorAddr);	
					break;
				case 1:
					if(g_libusb_cfg.req.databuf[0] == 0x28)
					{
						sccb_wr(&g_libsccbcfg_sensor, g_WriteSensorAddr, g_libusb_cfg.req.databuf[2]);
						g_WriteSensorStep = 0;
					}
					else{
						g_WriteSensorStep = 0;
					}
//					debug_printf("ZZ: value = 0x%x\n", g_libusb_cfg.req.databuf[2]);
					break;
				default:
					break;
				}
			}
			break;
		case ReadSensor:
			sccb_set(&g_libsccbcfg_sensor);
			if(g_libsccbcfg_sensor.mode == SCCB_MODE8){
				RegisterValue = sccb_rd(&g_libsccbcfg_sensor,g_libusb_cfg.req.databuf[1]);
			}
			else {
				RegisterValue = sccb_rd(&g_libsccbcfg_sensor,g_libusb_cfg.req.databuf[1] | (g_libusb_cfg.req.databuf[2]<<8));
			}
			break;
		default:
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			return;
		}
		break;
	case UAV_GET_CUR:
//		debug_printf("XU get:%x,%x,%x,%x\n", g_libusb_cfg.req.request.bmRequestType, g_libusb_cfg.req.request.bRequest, g_libusb_cfg.req.request.wValue_L, RegisterValue );
		switch(XU_TempValue[0] & 0x0f){
		case ReadUSB:
			if(countread == 0){
				g_libusb_cfg.req.databuf[3] = 0x11;
				g_libusb_cfg.req.databuf[2] = (RegisterValue & 0xff);
				g_libusb_cfg.req.databuf[1] = (RegisterValue >> 8);
				countread = 1;
			}else{
				g_libusb_cfg.req.databuf[3] = 0x21;
				g_libusb_cfg.req.databuf[2] = (RegisterValue >> 16);
				g_libusb_cfg.req.databuf[1] = (RegisterValue >> 24);
			}
			break;
		case ReadSensor:
			g_libusb_cfg.req.databuf[3] = XU_TempValue[0];
			g_libusb_cfg.req.databuf[2] = XU_TempValue[1];
			g_libusb_cfg.req.databuf[1] = (RegisterValue & 0xff);
			break;
		}
		break;
	case UAV_GET_MIN:
		WriteReg32(tmp_addr, 0x0);
		break;
	case UAV_GET_MAX:
		WriteReg32(tmp_addr, 0x3);
		break;
	case UAV_GET_LEN:
		WriteReg32(tmp_addr, 0x3);
		break;
	case UAV_GET_RES:
		WriteReg32(tmp_addr, 0x1);
		break;
	case UAV_GET_INFO:
		WriteReg32(tmp_addr, 0x3);
		break;
	case UAV_GET_DEF:
		WriteReg32(tmp_addr, 0x0);
		break;
	}
}
#endif
