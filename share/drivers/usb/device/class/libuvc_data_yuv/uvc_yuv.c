#include "includes.h"

#define  RAMIMG_BASE_ADDR    0x10100000

int colorbar_data[8] = {
	0xeb80eb80,
	0x91d210d2,
	0x10a9a4a9,
	0x24903790,
	0xdb6ac86a,
	0xf0515b51,
	0x6e29f029,
	0x80108010,
};

extern void udcif_grfc_cfg(int addr, int length);
int hwyuv_grfc_cb(void)
{
	udcif_grfc_cfg(0x2000, 640*480*2);
	return 1;
}

//fill the colorbar data to DDR 
static void colorbar_init(char *addr)
{
	int i, j;
	int start;

	start = addr;
	for(i=0; i<8; i++) {
		for(j=0; j<40; j++) {
			*((int* )(start+i*160+j*4)) = colorbar_data[i];
		}
	}
	debug_printf("color: %x %x\n", ReadReg32(addr), ReadReg32(addr+4));

	for(i=0; i<480; i++) {
		memcpy(start+i*1280, start, 1280);
	}
}

static int uvc_dp_init(int dpcfg_id)
{
	int ret = libdatapath_init(dpcfg_id);
	debug_printf("datapath %d init: %d\n", dpcfg_id, ret);
	ret |= libdatapath_start();
	debug_printf("datapath start: %d\n", ret);
	ret |= libisp_init();
	debug_printf("isp_init: %d\n", ret);

	ret |= libvs_init(0);
	ret |= libvs_start(0);
	debug_printf("vs start: %d\n", ret);

	return ret;
}

static void colorbar_config(int width, int height)
{
	WriteReg8(COLORBAR_BASE_ADDR+0x1, width&0xff);
	WriteReg8(COLORBAR_BASE_ADDR+0x2, (width>>8)&0xff);
	WriteReg8(COLORBAR_BASE_ADDR+0x3, height&0xff);
	WriteReg8(COLORBAR_BASE_ADDR+0x4, (height>>8)&0xff);
	WriteReg8(COLORBAR_BASE_ADDR+0x5, 0x01);
	WriteReg8(COLORBAR_BASE_ADDR+0x6, 0x01);
	WriteReg8(COLORBAR_BASE_ADDR+0x7, 0x10);
	WriteReg8(COLORBAR_BASE_ADDR+0x8, 0x30);
	WriteReg8(COLORBAR_BASE_ADDR, ReadReg8(0xc0088800) | BIT0 | BIT2 |(0x1<<4)|(0x2<<6)); //color bar
}

int hwyuv_encode_data(void *data, u8 *buffer, int length)
{
	//TODO: nothing
	return UVC_FLAG_NODATA;
}

int hwyuv_frameset_default(u32 frame_size, u8 frame_rate)
{
	debug_printf("YUY2(HW) frame set: %x, %d\n", frame_size, frame_rate);

	DISABLE_VIDEO_EP;

	//usb ep config
	WriteReg32(UDCIF_VP_CSR, 0x200088b2);
	WriteReg32(UDCIF_ISOEP_ADDR, 0x04050607);
	WriteReg32(UDCIF_CSR_CTRL, ReadReg32(UDCIF_CSR_CTRL)|BIT5|BIT12);

#if 1 
#if 1
	// color bar 
	int width, height;

	WriteReg32(REG_SC_ADDR+0x30, ReadReg32(REG_SC_ADDR+0x30) | BIT0);//open CIF0 clock
	WriteReg32(REG_SC_ADDR+0x20, ReadReg32(REG_SC_ADDR+0x20) & (~BIT1));//release CIF0 reset

	//[31:24] -- div			
	WriteReg32(REG_SC_ADDR+0x4c, (ReadReg32(REG_SC_ADDR+0x4c)&0xffffff)|(0x8<<24)|BIT31);			
	switch(frame_size){
		case VIDEO_SIZE_VGA:
			width = 0x280;
			height = 0x1e0;
			break;
		case VIDEO_SIZE_QVGA:
			width = 0x140;
			height = 0xf0;
			break;
		case VIDEO_SIZE_QQVGA:
			width = 0xa0;
			height = 0x78;
			break;
		case VIDEO_SIZE_CIF:
			width = 0x160;
			height = 0x120;
			break;
		default:
			debug_printf("unsupport frame size: %d\n", frame_size);
			break;
	}

	colorbar_config(width, height);
	ENABLE_UVC_DP_YUV_COLOR;

	//src mux: 0 snr, 1 colorbar
	WriteReg32(REG_SC_ADDR+0xc, (ReadReg32(REG_SC_ADDR+0xc) | BIT0));
#else

#if 0
	//dvp
extern int uvc_dp_init(int dpcfg_id);
	uvc_dp_init(0);

	ENABLE_UVC_DP_YUV_SCALE;
#else
	//sel mipirx0
	WriteReg32(REG_SC_ADDR + 0xc, (ReadReg32(0xc0006400 + 0x1c) & 0xcfffffff) | (0x0<<28)); 

extern int uvc_dp_init(int dpcfg_id);
	uvc_dp_init(0);
			
	ENABLE_UVC_DP_YUV_SCALE;
#endif  //yuv
#endif  //colorbar

#else
	colorbar_init(RAMIMG_BASE_ADDR+0x2000);

	g_libusbclass_cfg->handleGRFC = hwyuv_grfc_cb;
	hwyuv_grfc_cb();
#endif //grfc

	// endian input swap
	WriteReg32(UDCIF_VIDEO_CFG, (ReadReg32(UDCIF_VIDEO_CFG) & 0xffff3fff) | 0x4000);
	ENABLE_VIDEO_EP;

	return 1;
}
