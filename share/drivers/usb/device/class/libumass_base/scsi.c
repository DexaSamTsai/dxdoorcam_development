#include "includes.h"

t_msc_cbw CBW __attribute__((aligned(4)));                   /* Command Block Wrapper */
t_msc_csw CSW __attribute__((aligned(4)));                   /* Command Status Wrapper */
u8 msc_bulk_buf[MSC_MAX_TOTAL_BUF] __attribute((aligned(4))); 

extern u32 lundriver_cfg_start;
extern u32 lundriver_cfg_end;
extern t_lundriver_cfg g_nulldisk_lundriver;

static void handle_msc_csw(void)
{
	DISABLE_EPINTR(g_libmsc_cfg.MSC_EP_IN & EPNUM_MASK);

	CSW.signature = MSC_CSW_SIG;
	udc_writeEP(MSC_ENDPOINT_BULKIN, (u8 *)&CSW, MSC_CSW_SIZE);
	g_libmsc_cfg.msc_stage = MSC_CBW;
	g_libmsc_cfg.scsi_cmd = 0xff;  //NULL SCSI CMD
}

static void handle_datain_transfer (void) 
{
	u32 length;

	length = g_libmsc_cfg.block_rw_length;
	if (length > CBW.data_length) {
		length = CBW.data_length;
	}
	
	length = udc_writeEP(MSC_ENDPOINT_BULKIN, g_libmsc_cfg.tx_buffer, length);
	g_libmsc_cfg.msc_stage = MSC_DATA_IN_LAST;
	
	CSW.data_residue -= length;
	CSW.status = CSW_CMD_PASSED;
	ENABLE_EPINTR(g_libmsc_cfg.MSC_EP_IN & EPNUM_MASK);
}

static bool handle_rw_setup (void) 
{
	u32 length;
				  
	g_libmsc_cfg.block_rw_offset = 0;

	/* Logical Block Address of First Block */
	length = (CBW.cbdata[2] << 24) |
			(CBW.cbdata[3] << 16) |
			(CBW.cbdata[4] <<  8) |
			(CBW.cbdata[5] <<  0);
										
	g_libmsc_cfg.block_rw_start = length;  // *g_libmsc_cfg.msc_lundriver[CBW.lun]->block_size;
										  
	/* Number of Blocks to transfer */
	length = (CBW.cbdata[7] <<  8) |
			(CBW.cbdata[8] <<  0);
	
	g_libmsc_cfg.block_rw_length = length * g_libmsc_cfg.msc_lundriver[CBW.lun]->block_size;

#if UMASS_CMD_DEBUG
	if((CBW.cbdata[0] == CMD_READ10) || (CBW.cbdata[0] == CMD_WRITE10 )) {
		debug_printf("RWSetup: 0x%x 0x%x\n", g_libmsc_cfg.block_rw_start, g_libmsc_cfg.block_rw_length);
	}
#endif

	g_libmsc_cfg.block_rw_offset = 0;
	g_libmsc_cfg.tx_length = 0;

	if (CBW.data_length != g_libmsc_cfg.block_rw_length) {
#if UMASS_WARNING
		debug_printf("MSC_RWsetup: len 0x%x != cnt 0x%x\n", CBW.data_length, g_libmsc_cfg.block_rw_length);
#endif
		CSW.status = CSW_STAGE_ERR;
		handle_msc_csw();
		udc_stallEP(MSC_ENDPOINT_BULKIN);
		udc_stallEP(MSC_ENDPOINT_BULKOUT);
		libusb_dump_data((unsigned char *)&CBW , 31);
		return UMASS_FALSE;
	}

	return UMASS_TRUE;
}


bool datain_format_flag(void)
{
	if (CBW.data_length == 0) {
		CSW.status = CSW_STAGE_ERR;
		handle_msc_csw();
		return UMASS_FALSE; 
	}

	if ((CBW.flags & 0x80) == 0) {
		udc_stallEP(MSC_ENDPOINT_BULKOUT);
		CSW.status = CSW_STAGE_ERR;
		handle_msc_csw();
		return UMASS_FALSE;
	}

	return UMASS_TRUE;
}

static void handle_testunitready(void)
{
	if (CBW.data_length != 0) {
		if ((CBW.flags & 0x80) != 0) {
			udc_stallEP(MSC_ENDPOINT_BULKIN);
	    } 
		else {
			udc_stallEP(MSC_ENDPOINT_BULKOUT);
		}
	}

	if(g_libmsc_cfg.msc_lun_cnt) {
		CSW.status = CSW_CMD_PASSED;
	}else {
		CSW.status = CSW_CMD_FAILED;
	}
	handle_msc_csw();
}

static void handle_requestsense(void)
{
	u32 i;

    if(!datain_format_flag()) {
		return;
	}

	if(g_libmsc_cfg.msc_lun_cnt == 0) {
	 	g_libmsc_cfg.request_sense_code = 0x023A00; //errcode: no media
	}

	for(i=0; i<18; i++)
	 	g_libmsc_cfg.tx_buffer[i] = 0;  	

	g_libmsc_cfg.tx_buffer[0] = 0x70;
	g_libmsc_cfg.tx_buffer[7] = 0x0a;
	if(g_libmsc_cfg.request_sense_code)
	{
		g_libmsc_cfg.tx_buffer[ 2] = (g_libmsc_cfg.request_sense_code & 0xff0000)>>16;       /* Sense Key */
		g_libmsc_cfg.tx_buffer[12] = (g_libmsc_cfg.request_sense_code & 0xff00)>>8;          /* ASC */
		g_libmsc_cfg.tx_buffer[13] = (g_libmsc_cfg.request_sense_code & 0xff);               /* ASCQ */
	}

	g_libmsc_cfg.block_rw_length = 18;
    handle_datain_transfer(); 

	g_libmsc_cfg.request_sense_code = 0;
}

static void handle_modesense6(void)
{
    if(!datain_format_flag()) {
		return;
	}
   
	g_libmsc_cfg.tx_buffer[0] = 0x03; 
	g_libmsc_cfg.tx_buffer[1] = 0x00;  
    if(g_libmsc_cfg.msc_lundriver[CBW.lun]->lun_param & UMASS_LUN_WRPROTECT){
		g_libmsc_cfg.tx_buffer[2] = 0x80;  
	}else {		
		g_libmsc_cfg.tx_buffer[2] = 0x00;  
	}
	g_libmsc_cfg.tx_buffer[3] = 0x00;  

	g_libmsc_cfg.block_rw_length = 4;
    handle_datain_transfer(); 
}

static void handle_modesense10(void)
{
	int i;

    if(!datain_format_flag())
		return;
   
	for(i=0; i<8; i++)
	   g_libmsc_cfg.tx_buffer[i] = 0x00;  

	g_libmsc_cfg.block_rw_length = 8;
    handle_datain_transfer(); 
}

static void handle_inquiry(void)
{
    if(!datain_format_flag())
		return;
   
    if(g_libmsc_cfg.msc_lundriver[CBW.lun]->lun_param & UMASS_LUN_REMOVABLE)   {
		g_libmsc_cfg.tx_buffer[0] = 0x00;   /* Direct Access Device */
		g_libmsc_cfg.tx_buffer[1] = 0x80;   /* RMB = 1: Removable Medium */
		g_libmsc_cfg.tx_buffer[2] = 0x00;   /* Version: No conformance claim to standard */
		g_libmsc_cfg.tx_buffer[3] = 0x01;
		g_libmsc_cfg.tx_buffer[5] = 0x80;
    }
     else {
		g_libmsc_cfg.tx_buffer[0] = 0x05;
		g_libmsc_cfg.tx_buffer[1] = 0x80;
		g_libmsc_cfg.tx_buffer[2] = 0x00;
		g_libmsc_cfg.tx_buffer[3] = 0x02;
		g_libmsc_cfg.tx_buffer[5] = 0x00;
    }
   
    g_libmsc_cfg.tx_buffer[4] = 32;
	g_libmsc_cfg.tx_buffer[6] = 0x00;
	g_libmsc_cfg.tx_buffer[7] = 0x00;

	/* Vendor Identification */
    g_libmsc_cfg.tx_buffer[8] = strMscVendor[0];
    g_libmsc_cfg.tx_buffer[9] = strMscVendor[1];
	g_libmsc_cfg.tx_buffer[10] = strMscVendor[2];
	g_libmsc_cfg.tx_buffer[11] = strMscVendor[3];
    g_libmsc_cfg.tx_buffer[12] = strMscVendor[4];
    g_libmsc_cfg.tx_buffer[13] = strMscVendor[5];
	g_libmsc_cfg.tx_buffer[14] = strMscVendor[6];
	g_libmsc_cfg.tx_buffer[15] = strMscVendor[7];

	/* Product Identification */
	memset(g_libmsc_cfg.tx_buffer+16, 0x00, 20);
	if((g_libmsc_cfg.msc_lundriver[CBW.lun]->product_info != NULL) && (g_libmsc_cfg.msc_lundriver[CBW.lun]->product_info[0] != 0)){
		strncpy(g_libmsc_cfg.tx_buffer+16, g_libmsc_cfg.msc_lundriver[CBW.lun]->product_info, 20);
	}

	g_libmsc_cfg.block_rw_length = 36;
    handle_datain_transfer(); 
}

static void handle_readtoc(void)
{
   if((CBW.cbdata[7] == 0x00) && (CBW.cbdata[8] == 0x0c)) {
	    /* TOC data length */
	    g_libmsc_cfg.tx_buffer[0] = 0x00;
	    g_libmsc_cfg.tx_buffer[1] = 0x0a;
	    g_libmsc_cfg.tx_buffer[2] = 0x01;
	    g_libmsc_cfg.tx_buffer[3] = 0x01;

		/* TOC track descriptor */
	    g_libmsc_cfg.tx_buffer[4] = 0x00;
	    g_libmsc_cfg.tx_buffer[5] = 0x14;
	    g_libmsc_cfg.tx_buffer[6] = 0x01;
	    g_libmsc_cfg.tx_buffer[7] = 0x00;
	    g_libmsc_cfg.tx_buffer[8] = 0x00;
	    g_libmsc_cfg.tx_buffer[9] = 0x00;
	    g_libmsc_cfg.tx_buffer[10] = 0x02;
	    g_libmsc_cfg.tx_buffer[11] = 0x00;

	    g_libmsc_cfg.block_rw_length = 12;
        handle_datain_transfer(); 
   }
   else if((CBW.cbdata[7] == 0x03) && (CBW.cbdata[8] == 0x24)) {
	    g_libmsc_cfg.tx_buffer[0] = 0x00;
	    g_libmsc_cfg.tx_buffer[1] = 0x23;
	    g_libmsc_cfg.tx_buffer[2] = 0x01;
	    g_libmsc_cfg.tx_buffer[3] = 0x01;

		/* TOC track descriptor */
	    g_libmsc_cfg.tx_buffer[4] = 0x01;
	    g_libmsc_cfg.tx_buffer[5] = 0x14;
	    g_libmsc_cfg.tx_buffer[6] = 0x00;
	    g_libmsc_cfg.tx_buffer[7] = 0xa0;
	    g_libmsc_cfg.tx_buffer[8] = 0x00;
	    g_libmsc_cfg.tx_buffer[9] = 0x00;
	    g_libmsc_cfg.tx_buffer[10] = 0x00;
	    g_libmsc_cfg.tx_buffer[11] = 0x00;
	    g_libmsc_cfg.tx_buffer[12] = 0x01;
	    g_libmsc_cfg.tx_buffer[13] = 0x00;
	    g_libmsc_cfg.tx_buffer[14] = 0x00;
	    g_libmsc_cfg.tx_buffer[15] = 0x01;

	    g_libmsc_cfg.tx_buffer[16] = 0x14;
	    g_libmsc_cfg.tx_buffer[17] = 0x00;
	    g_libmsc_cfg.tx_buffer[18] = 0xa1;
	    g_libmsc_cfg.tx_buffer[19] = 0x00;
	    g_libmsc_cfg.tx_buffer[20] = 0x00;
	    g_libmsc_cfg.tx_buffer[21] = 0x00;
	    g_libmsc_cfg.tx_buffer[22] = 0x00;
	    g_libmsc_cfg.tx_buffer[23] = 0x01;

	    g_libmsc_cfg.tx_buffer[24] = 0x00;
	    g_libmsc_cfg.tx_buffer[25] = 0x00;
	    g_libmsc_cfg.tx_buffer[26] = 0x01;
	    g_libmsc_cfg.tx_buffer[27] = 0x14;
	    g_libmsc_cfg.tx_buffer[28] = 0x00;
	    g_libmsc_cfg.tx_buffer[29] = 0xa2;
	    g_libmsc_cfg.tx_buffer[30] = 0x00;
	    g_libmsc_cfg.tx_buffer[31] = 0x00;

	    g_libmsc_cfg.tx_buffer[32] = 0x00;
	    g_libmsc_cfg.tx_buffer[33] = 0x00;
	    g_libmsc_cfg.tx_buffer[34] = 0x0f;
	    g_libmsc_cfg.tx_buffer[35] = 0x03;
	    g_libmsc_cfg.tx_buffer[36] = 0x00;

	    g_libmsc_cfg.block_rw_length = 37;
        handle_datain_transfer(); 
     }
      else {
#if UMASS_FATAL_ERROR
	    debug_printf("read toc error: %x %x\n", CBW.cbdata[7], CBW.cbdata[8]);
#endif
    }
}

static void handle_readformatcapacity(void)
{
	u32 block_cnt, block_size;

    if(!datain_format_flag())
		return;

	block_cnt = g_libmsc_cfg.msc_lundriver[CBW.lun]->block_cnt-1;
	block_size = g_libmsc_cfg.msc_lundriver[CBW.lun]->block_size;

	g_libmsc_cfg.tx_buffer[0] = 0x00; 
	g_libmsc_cfg.tx_buffer[1] = 0x00;  
	g_libmsc_cfg.tx_buffer[2] = 0x00;  
	g_libmsc_cfg.tx_buffer[3] = 0x00;  

	g_libmsc_cfg.tx_buffer[4] = (block_cnt>>24) & 0xff; 
	g_libmsc_cfg.tx_buffer[5] = (block_cnt>>16) & 0xff;  
	g_libmsc_cfg.tx_buffer[6] = (block_cnt>>8) & 0xff;  
	g_libmsc_cfg.tx_buffer[7] = block_cnt & 0xff;  

	g_libmsc_cfg.tx_buffer[8] = (block_size>>24) & 0xff; 
	g_libmsc_cfg.tx_buffer[9] = (block_size>>16) & 0xff; 
	g_libmsc_cfg.tx_buffer[10] = (block_size>>8) & 0xff; 
	g_libmsc_cfg.tx_buffer[11] = (block_size) & 0xff; 

	g_libmsc_cfg.block_rw_length = 12;
    handle_datain_transfer(); 
}

static void handle_readcapacity(void)
{
	u32 block_cnt, block_size;

    if(!datain_format_flag())
		return;

	block_cnt = g_libmsc_cfg.msc_lundriver[CBW.lun]->block_cnt-1;
	block_size = g_libmsc_cfg.msc_lundriver[CBW.lun]->block_size;

	g_libmsc_cfg.tx_buffer[0] = (block_cnt>>24) & 0xff; 
	g_libmsc_cfg.tx_buffer[1] = (block_cnt>>16) & 0xff;  
	g_libmsc_cfg.tx_buffer[2] = (block_cnt>>8) & 0xff;  
	g_libmsc_cfg.tx_buffer[3] = block_cnt & 0xff;  

	g_libmsc_cfg.tx_buffer[4] = (block_size>>24) & 0xff; 
	g_libmsc_cfg.tx_buffer[5] = (block_size>>16) & 0xff; 
	g_libmsc_cfg.tx_buffer[6] = (block_size>>8) & 0xff; 
	g_libmsc_cfg.tx_buffer[7] = (block_size) & 0xff; 

	g_libmsc_cfg.block_rw_length = 8;
    handle_datain_transfer(); 
}

static void handle_verify10(void)
{
	u32 block_cnt;

	block_cnt = ((CBW.cbdata[2] << 24) |
		        (CBW.cbdata[3] << 16) |
		        (CBW.cbdata[4] <<  8) |
		        (CBW.cbdata[5] <<  0));
	
	if(block_cnt <= g_libmsc_cfg.msc_lundriver[CBW.lun]->block_cnt)
	{
	    CSW.status = CSW_CMD_PASSED;		
	}
	  else{
	    debug_printf("verify10(Err): %x\n", block_cnt);
	    CSW.status = CSW_CMD_FAILED;		
	}

	handle_msc_csw();
}

////////////////////////////////////////////////////////////////////////
static void msc_data_read (void) 
{
	u32 len, cnt, ret;
				    
	if(g_libmsc_cfg.tx_length == 0)	{
		if ((g_libmsc_cfg.block_rw_length - g_libmsc_cfg.block_rw_offset) > MSC_MAX_TOTAL_BUF) {
			len = MSC_MAX_TOTAL_BUF;
		} 
		else {
			len = g_libmsc_cfg.block_rw_length - g_libmsc_cfg.block_rw_offset; 
		}

		 //TODO: lun data read
		 // length: len, offset: g_libmsc_cfg.block_rw_offset, buffer: g_libmsc_cfg.tx_buffer
		 //debug_printf("scif read: %x %x %x\n", g_libmsc_cfg.block_rw_start, g_libmsc_cfg.block_rw_offset, len);
		ret = g_libmsc_cfg.msc_lundriver[CBW.lun]->msc_read(g_libmsc_cfg.msc_lundriver[CBW.lun]->lun_arg,
						g_libmsc_cfg.block_rw_start+(g_libmsc_cfg.block_rw_offset>>9), 
						(u32)g_libmsc_cfg.tx_buffer, 
						len);

		g_libmsc_cfg.tx_length = len;
		g_libmsc_cfg.tx_offset = 0;
	}
	  
	cnt = udc_writeEP(MSC_ENDPOINT_BULKIN, 
				(u8 *)(g_libmsc_cfg.tx_buffer + g_libmsc_cfg.tx_offset), 
				g_libmsc_cfg.tx_length - g_libmsc_cfg.tx_offset);
	g_libmsc_cfg.tx_offset += cnt;

	if(g_libmsc_cfg.tx_offset >= g_libmsc_cfg.tx_length)
	{
		//TODO: tx_offset != tx_length
		g_libmsc_cfg.block_rw_offset += g_libmsc_cfg.tx_offset;
		g_libmsc_cfg.tx_offset = 0;
		g_libmsc_cfg.tx_length = 0;
		len = g_libmsc_cfg.block_rw_length - g_libmsc_cfg.block_rw_offset;
		if(len == 0) {
			g_libmsc_cfg.msc_stage = MSC_DATA_IN_LAST;
		}
		CSW.data_residue = len;

		if (g_libmsc_cfg.msc_stage != MSC_DATA_IN) {
			 if(g_libmsc_cfg.msc_lun_cnt) {
		    	CSW.status = CSW_CMD_PASSED;
			 }
			 else {
				CSW.status = CSW_CMD_FAILED;
			 }
		}
	}

	ENABLE_EPINTR(g_libmsc_cfg.MSC_EP_IN & EPNUM_MASK);
}

static void msc_data_write(void) 
{
     u32 len, ret;
     u32 flag, start;

	 len = udc_readEP(MSC_ENDPOINT_BULKOUT, g_libmsc_cfg.rx_buffer + g_libmsc_cfg.rx_offset);
	 if(len == 0)
	 {
	     debug_printf("buf byte = 0!!!!\n");
	     return;
	 }

	 //TODO: rx_offset > total size
	 if ((g_libmsc_cfg.block_rw_start + ((g_libmsc_cfg.block_rw_offset + g_libmsc_cfg.rx_offset + len)>>9)) > 
					  g_libmsc_cfg.msc_lundriver[CBW.lun]->size) 
	 {
#if UMASS_FATAL_ERROR
		 debug_printf("data write: rx size overflow: %x %x %x\n", g_libmsc_cfg.msc_lundriver[CBW.lun]->size, g_libmsc_cfg.block_rw_offset, g_libmsc_cfg.rx_offset);
#endif
	     len = g_libmsc_cfg.msc_lundriver[CBW.lun]->size - ((g_libmsc_cfg.rx_offset + g_libmsc_cfg.block_rw_offset)>>9) - g_libmsc_cfg.block_rw_start;
		 len = len*512;

		 udc_stallEP(MSC_ENDPOINT_BULKOUT);

		 g_libmsc_cfg.request_sense_code = 0x052100;
		 CSW.status = CSW_CMD_FAILED;
		 CSW.data_residue = len;
		 handle_msc_csw();
		 g_libmsc_cfg.fixbug_flag = 0;

	 	 len = udc_readEP(MSC_ENDPOINT_BULKOUT, g_libmsc_cfg.rx_buffer);

		 return;
	 }

	 g_libmsc_cfg.rx_offset += len;
		
	 len = g_libmsc_cfg.block_rw_length - g_libmsc_cfg.block_rw_offset - g_libmsc_cfg.rx_offset;
	 CSW.data_residue = len;

	 // rx data: less than full num buf
	 flag = 0;
	 if(g_libusb_cfg.bHighSpeed) {
		 if((MSC_MAX_TOTAL_BUF - g_libmsc_cfg.rx_offset) < 0x800) {
			 flag = 1;
		 }
	 }
	  else  {
		 if((MSC_MAX_TOTAL_BUF - g_libmsc_cfg.rx_offset) < 0x200) {
		     flag = 1;
		 }
	 }

	 if( (len == 0) || (flag == 1) ) {
		  ret = g_libmsc_cfg.msc_lundriver[CBW.lun]->msc_write(g_libmsc_cfg.msc_lundriver[CBW.lun]->lun_arg, 
				g_libmsc_cfg.block_rw_start + (g_libmsc_cfg.block_rw_offset>>9), 
				(int)g_libmsc_cfg.rx_buffer, 
				(g_libmsc_cfg.rx_offset - (g_libmsc_cfg.rx_offset & 0x1ff)));  
		  if(ret)	{
			  debug_printf("scif_write(%x): %x %x\n", ret, g_libmsc_cfg.block_rw_start+(g_libmsc_cfg.block_rw_offset>>9), g_libmsc_cfg.rx_offset);
		  }

		  // handle reside data
		  g_libmsc_cfg.block_rw_offset += ((g_libmsc_cfg.rx_offset>>9)<<9);
		  if(g_libmsc_cfg.rx_offset & 0x1ff)
		  {
			  start =(u32)(g_libmsc_cfg.rx_buffer + g_libmsc_cfg.rx_offset - (g_libmsc_cfg.rx_offset & 0x1ff)); 
		      memcpy( g_libmsc_cfg.rx_buffer, 
		              (u8 *)start, 
					  g_libmsc_cfg.rx_offset & 0x1ff);
		  }
	      g_libmsc_cfg.rx_offset = g_libmsc_cfg.rx_offset & 0x1ff;
	  }

	  if(MSC_MAX_TOTAL_BUF < g_libmsc_cfg.rx_offset) 
	  {
#if UMASS_FATAL_ERROR
		   debug_printf("size(%x): over %x\n", g_libmsc_cfg.rx_length, len);
		   debug_printf("size(%x, %x): \n", g_libmsc_cfg.rx_offset, g_libmsc_cfg.block_rw_offset);
#endif
		   while(1);
      }

	  if ((g_libmsc_cfg.block_rw_offset >= g_libmsc_cfg.block_rw_length) ||
		  (g_libmsc_cfg.msc_stage == MSC_CSW)) 
	  {
	      CSW.status = CSW_CMD_PASSED;
	      handle_msc_csw();
		  return;
	  }
}

////////////////////////////////////////////////////////////////////////

static void handle_scsi_cmd(void)
{
#if UMASS_CMD_DEBUG
	debug_printf("cmd: %x\n", g_libmsc_cfg.scsi_cmd);
#endif
	switch(g_libmsc_cfg.scsi_cmd)
	{
		case CMD_TEST_UNIT_READY:
			handle_testunitready();
			break;

		case CMD_REQUEST_SENSE:
			handle_requestsense();
			break;

		case CMD_INQUIRY:
			handle_inquiry();
			break;

		case CMD_START_STOP_UNIT:
			break;

		case CMD_MEDIA_REMOVAL:
			if(g_libmsc_cfg.msc_lun_cnt) {
				CSW.status = CSW_CMD_FAILED;
			}else {
				CSW.status = CSW_CMD_PASSED;
			}
			handle_msc_csw();
			break;

		case CMD_MODE_SENSE6:
			handle_modesense6();
			break;

		case CMD_MODE_SENSE10:
			handle_modesense10();
			break;

		case CMD_READ_TOC:
			handle_readtoc();
			break;

		case CMD_READ_FORMAT_CAPACITIES:
			handle_readformatcapacity();
			break;
	
		case CMD_READ_CAPACITY:
			handle_readcapacity();
			break;

		case CMD_READ10:
			if (handle_rw_setup()) {
				if ((CBW.flags & 0x80) != 0) {
					g_libmsc_cfg.msc_stage = MSC_DATA_IN;
					ENABLE_EPINTR(g_libmsc_cfg.MSC_EP_IN & EPNUM_MASK);
				}
					else {
#if UMASS_FATAL_ERROR
					debug_printf("scsi_read10 error\n");
#endif
					udc_stallEP(MSC_ENDPOINT_BULKOUT);
					CSW.status = CSW_STAGE_ERR;
					handle_msc_csw();
				}
			}
			break;

		case CMD_WRITE10:
			if (handle_rw_setup()) {
				if ((CBW.flags & 0x80) == 0) {
					g_libmsc_cfg.msc_stage = MSC_DATA_OUT;
				}
				else {
#if UMASS_FATAL_ERROR
					debug_printf("scsi_write10 error\n");
#endif
					udc_stallEP(MSC_ENDPOINT_BULKIN);
					CSW.status = CSW_STAGE_ERR;
					handle_msc_csw();
				}
			}
			break;

		case CMD_VERIFY10:
			handle_verify10();
			break;

		case CMD_FORMAT_UNIT:
		case CMD_MODE_SELECT6:
		case CMD_MODE_SELECT10:
			break;

		default:
			debug_printf("unsupported scsi cmd(%x): %x\n", g_libmsc_cfg.scsi_cmd, CBW.cbdata[0]);
	 		CSW.status = CSW_CMD_FAILED;
			handle_msc_csw();
	 		g_libmsc_cfg.request_sense_code = 0x052000; //errcode: invalid cmd op 
			break;
	}
}

static u32 handle_msc_cbw(void)
{
	u32 len;

    len = udc_readEP(MSC_ENDPOINT_BULKOUT, (u8 *)&CBW);
	if((len != MSC_CBW_SIZE) || (CBW.signature != MSC_CBW_SIG)) {
		debug_printf("CBW err: len 0x%x, signature 0x%x\n", len, CBW.signature);
		libusb_dump_data((unsigned char *)&CBW, 31);
		return UMASS_FAILED;
	}

	//debug_printf("data length: %x\n", CBW.data_length);
	CSW.tag = CBW.tag;
	CSW.data_residue = CBW.data_length;

	if ((CBW.lun > 1) || (CBW.cb_length < 1) || (CBW.cb_length > 16)) {
		debug_printf("cmd (0x%x) not support: %x %x %x\n", CBW.signature, CBW.tag, CBW.data_length, CBW.flags);
		debug_printf("cmd (0x%x) not support: %x %x\n", CBW.cbdata[0], CBW.lun, CBW.cb_length);
		libusb_dump_data((unsigned char *)&CBW, 31);
		CSW.status = CSW_CMD_FAILED;
		handle_msc_csw();
		return UMASS_FAILED;
    }
	g_libmsc_cfg.scsi_cmd = CBW.cbdata[0];

	handle_scsi_cmd();

	return UMASS_OK;
}

void handle_msc_bulkin(void)
{
	switch(g_libmsc_cfg.msc_stage) {
		case MSC_DATA_IN:
			if(g_libmsc_cfg.scsi_cmd == CMD_READ10){
				msc_data_read();
			}
			break;

		case MSC_DATA_IN_LAST:
			handle_msc_csw();
			break;

		case MSC_DATA_IN_LAST_STALL:
  			udc_stallEP(MSC_ENDPOINT_BULKIN);
			handle_msc_csw();
			break;

		case MSC_CSW:
			g_libmsc_cfg.msc_stage = MSC_CBW;
			break;
	}
}

void handle_msc_bulkout()
{
	switch(g_libmsc_cfg.msc_stage) {
		case MSC_CBW:
			handle_msc_cbw();
			break;

		case MSC_DATA_OUT:
			if(g_libmsc_cfg.scsi_cmd == CMD_WRITE10) {
				msc_data_write();
			}
			else if(g_libmsc_cfg.scsi_cmd == CMD_VERIFY10) {
				handle_verify10();
			}
			break;

		default:
#if UMASS_FATAL_ERROR
			debug_printf("MSC_BulkOut: stage error, %d\n", g_libmsc_cfg.msc_stage);
#endif
			udc_readEP(MSC_ENDPOINT_BULKOUT, g_libmsc_cfg.rx_buffer);
			libusb_dump_data(g_libmsc_cfg.rx_buffer, 31);
			udc_stallEP(MSC_ENDPOINT_BULKOUT);
			CSW.status = CSW_STAGE_ERR;
			handle_msc_csw();
	}
}

int umass_add_lun(void)
{
	t_lundriver_cfg *lundriver_cfg;
	t_lundriver_cfg tmp_lundriver_cfg;

	lundriver_cfg = (t_lundriver_cfg *)(&lundriver_cfg_start);
	for(; lundriver_cfg<(t_lundriver_cfg *)(&lundriver_cfg_end); lundriver_cfg++){
		if(g_libmsc_cfg.msc_lun_cnt >= MSC_MAX_LUNCNT){
			break;
		}

		memcpy((u8 *)&tmp_lundriver_cfg, (u8 *)lundriver_cfg, sizeof(t_lundriver_cfg));
		if(tmp_lundriver_cfg.lun_id != UMASS_ID_SCIF){
			continue;
		}

		if(tmp_lundriver_cfg.init() < 0){
			break;
		}

		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt] = lundriver_cfg;
		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt]->lun_param |= UMASS_LUN_WORKING;
		g_libmsc_cfg.msc_lun_cnt++;
	}

	return g_libmsc_cfg.msc_lun_cnt;
}

int umass_remove_lun(void)
{
	int i;

	for(i=0; i<g_libmsc_cfg.msc_lun_cnt; i++) {
		if(g_libmsc_cfg.msc_lundriver[i]->lun_id == UMASS_ID_SCIF) {
			if(g_libmsc_cfg.msc_lundriver[i]->lun_param & UMASS_LUN_WORKING) {
				g_libmsc_cfg.msc_lundriver[i]->lun_param &= ~UMASS_LUN_WORKING;
				g_nulldisk_lundriver.init();
				g_libmsc_cfg.msc_lundriver[i] = &g_nulldisk_lundriver;
				if(g_libmsc_cfg.msc_lun_cnt) {
					--g_libmsc_cfg.msc_lun_cnt;
				}
			}
			break;
		}
	}

	return g_libmsc_cfg.msc_lun_cnt;
}

void msc_reset (void)
{
    g_libmsc_cfg.msc_stage = MSC_CBW;
}

int umass_init(void)
{
	t_lundriver_cfg *lundriver_cfg;
	t_lundriver_cfg tmp_lundriver_cfg;

    debug_printf("++umass_init\n");
#if 0
	lundriver_cfg = (t_lundriver_cfg *)(&lundriver_cfg_start);

	for(; lundriver_cfg<(t_lundriver_cfg *)(&lundriver_cfg_end); lundriver_cfg++){
		if(g_libmsc_cfg.msc_lun_cnt >= MSC_MAX_LUNCNT){
			break;
		}

		memcpy((u8 *)&tmp_lundriver_cfg, (u8 *)lundriver_cfg, sizeof(t_lundriver_cfg));
		if(tmp_lundriver_cfg.init() < 0){
			continue;
		}

		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt] = lundriver_cfg;
		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt]->lun_param |= UMASS_LUN_WORKING;
		g_libmsc_cfg.msc_lun_cnt++;
	}
#else
		extern t_lundriver_cfg g_camera_lundriver, g_scif_lundriver; 
		g_libmsc_cfg.msc_lun_cnt = 0;
#if 0
		if(g_camera_lundriver.init() < 0){
			debug_printf("error: camera init failed\n");
			while(1);
		}
		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt] = &g_camera_lundriver;
#else
		if(g_scif_lundriver.init() < 0){
			debug_printf("error: scif init failed\n");
			while(1);
		}
		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt] = &g_scif_lundriver;
#endif
		g_libmsc_cfg.msc_lundriver[g_libmsc_cfg.msc_lun_cnt]->lun_param |= UMASS_LUN_WORKING;
		g_libmsc_cfg.msc_lun_cnt++;
#endif
	if(g_libmsc_cfg.msc_lun_cnt == 0) {
		g_nulldisk_lundriver.init();
		g_libmsc_cfg.msc_lundriver[0] = &g_nulldisk_lundriver;
	}

    g_libmsc_cfg.tx_buffer = msc_bulk_buf;
	g_libmsc_cfg.rx_buffer = msc_bulk_buf;
	msc_reset();
	return 1;
}

