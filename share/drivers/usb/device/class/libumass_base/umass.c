#include "includes.h"

void usbclass_HandleSetif(int if_id, int alt)
{
}

int usbclass_HandleRequest(void)
{
	u32 tmp_addr = (u32)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch(g_libusb_cfg.req.request.bmRequestType & 0x1f) 
	{
		case 1:
			if (g_libusb_cfg.req.request.wIndex_L == 0)	{
            	switch (g_libusb_cfg.req.request.bRequest) {
                	case MSC_REQ_RESET:
                    	break;

                  	case MSC_REQ_GET_MAX_LUN:
						if(g_libmsc_cfg.msc_lun_cnt){
							WriteReg32(tmp_addr, g_libmsc_cfg.msc_lun_cnt - 1);
						}else {
							WriteReg32(tmp_addr, 0x00);
						}
    					g_libmsc_cfg.msc_stage = MSC_CBW;
						ENABLE_EPINTR(g_libmsc_cfg.MSC_EP_OUT);
						return 0;
                }
			}
		default:
			break;
	}
	g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
	g_libusb_cfg.req.wLength = 0;

	return 1;
}

void usbclass_HandleMSC(u32 *args)
{
	u8 ep_in, ep_out;

	ep_in  = (g_libmsc_cfg.MSC_EP_IN) & EPNUM_MASK;
	ep_out = (g_libmsc_cfg.MSC_EP_OUT) & EPNUM_MASK;

	if(*args & MASK_OF_EPINTR(ep_in))  //EP in
	{
		if(EPBUF_IS_EMPTY(ep_in))	{
			*args = *args & ~(MASK_OF_EPINTR(ep_in));
			g_libmsc_cfg.msc_bulk_in();
		}
	}

	if(*args & MASK_OF_EPINTR(ep_out)) //EP out
	{
		if(EPBUF_IS_NOTEMPTY(ep_out)) {
			*args = *args & ~(MASK_OF_EPINTR(ep_out));
			g_libmsc_cfg.msc_bulk_out();
		}
	}
}

/* called before USB start, for initing class-specific data */
int usbclass_init(int first)
{
	if(first){
		((t_libmsc_cfg *)(g_libusbclass_cfg_hs.arg))->msc_init();
	}

	return 0;
}

void usbclass_clean(void)
{

}
