#include "includes.h"

void usbclass_HandleMSC(u32 args);
int umass_init(void);

/******************************************************
 * USB class configuration which should match Descriptor
******************************************************/
t_libusbep_cfg libusbep_cfg_hs[] = 
{
	{
		.ep         = MSC_ENDPOINT_BULKOUT,
		.type       = EPTYPE_BULK,
		.maxpktsize = HS_MAX_PKTSIZE_BULK,
		.intr_th    = 0x4,
		.baseaddr   = 0x0,
		.buffernum  = 0x4,
	},
	{
		.ep         = MSC_ENDPOINT_BULKIN,
		.type       = EPTYPE_BULK,
		.maxpktsize = HS_MAX_PKTSIZE_BULK,
		.intr_th    = 0x4,
		.baseaddr   = 0x4,
		.buffernum  = 0x4,
	},
};

t_libusbep_cfg libusbep_cfg_fs[] = 
{
	{
		.ep         = MSC_ENDPOINT_BULKOUT,
		.type       = EPTYPE_BULK,
		.maxpktsize = FS_MAX_PKTSIZE_BULK,
		.intr_th    = 0x4,
		.baseaddr   = 0x0,
		.buffernum  = 0x4,
	},
	{
		.ep         = MSC_ENDPOINT_BULKIN,
		.type       = EPTYPE_BULK,
		.maxpktsize = FS_MAX_PKTSIZE_BULK,
		.intr_th    = 0x4,
		.baseaddr   = 0x4,
		.buffernum  = 0x4,
	},
};

t_libmsc_cfg g_libmsc_cfg =
{
	.MSC_EP_IN      = MSC_ENDPOINT_BULKIN,  // bulk in endpoint
	.MSC_EP_OUT     = MSC_ENDPOINT_BULKOUT, // bulk out endpoint

	.msc_init       = umass_init,

	.msc_bulk_in    = handle_msc_bulkin,
	.msc_bulk_out   = handle_msc_bulkout,

	.msc_lun_cnt = 0,
	.fixbug_flag = 0,
};

t_libusbclass_cfg g_libusbclass_cfg_hs =
{
	.epcfg = &libusbep_cfg_hs[0],
	.epnum = sizeof(libusbep_cfg_hs)/sizeof(libusbep_cfg_hs[0]),
	.arg = &g_libmsc_cfg,
	.handleEP = usbclass_HandleMSC,
};

t_libusbclass_cfg g_libusbclass_cfg_fs =
{
	.epcfg = &libusbep_cfg_fs[0],
	.epnum = sizeof(libusbep_cfg_fs)/sizeof(libusbep_cfg_fs[0]),
	.arg = &g_libmsc_cfg,
	.handleEP = usbclass_HandleMSC,
};

/******************************************************
 * USB Descriptor
******************************************************/
// Types for GET_DESCRIPTOR queries from the host: high_speed
u8 cfgConf_HI[] __attribute__((aligned(4))) =
{
     0x09, 0x02, 0x20, 0x00, 0x01, 0x01, 0x01, 0x80, 0x32,
     0x09, 0x04, 0x00, 0x00, 0x02, MSC_CLASS, MSC_SUBCLASS_SCSI, MSC_PROTO_BO, 0x1,
	 0x07, 0x05, MSC_ENDPOINT_BULKOUT, 0x02, 0x00, 0x02, 0x00,
	 0x07, 0x05, MSC_ENDPOINT_BULKIN, 0x02, 0x00, 0x02, 0x00,
};

// Types for GET_DESCRIPTOR queries from the host: full_speed
u8 cfgConf_FL[] __attribute__((aligned(4))) =
{
	 0x09, 0x02, 0x20, 0x00, 0x01, 0x01, 0x01, 0x80, 0x32,
	 0x09, 0x04, 0x00, 0x00, 0x02, MSC_CLASS, MSC_SUBCLASS_SCSI, MSC_PROTO_BO, 0x1,
	 0x07, 0x05, MSC_ENDPOINT_BULKOUT, 0x02, 0x40, 0x00, 0x00,
	 0x07, 0x05, MSC_ENDPOINT_BULKIN, 0x02, 0x40, 0x00, 0x00,
};

/* Notice: g_csrdata is not modified */
t_csrdata g_csrdata[] = {
	//-------------------------------------------------------------------------------
	{0x00030000, 0x40002000, 0x0000000c },	//[31:16] device descriptor addr Pointer
											 //[15: 0] setup command addr pointer
	//-------------------------------------------------------------------------------
	{0x00000001, 0x50000000, 0x0000000c }, //[31:16] qualifier descriptor address pointer
											//[15: 0] reserved
	//-------------------------------------------------------------------------------
	{0x00000002, 0x02000080, 0x0000000c }, //[31:30] reserved for non-iso in endpoint
											//[29:19] maxpktsize
											//[18:15] alterate setting to which this endpoint belongs
											//[14:11] interface number to which this endpoint belongs
											//[10: 7] configuration number to which this endpoint belongs 
											//[ 6: 5] endpoint type;00 ctrl,01 iso,10 bulk,11 interrupt
											//[	4] endpoint direction 0:out, 1 in
											//[ 3: 0] endpoint number

	// dummy data
	{0x00030003, 0x00000000, 0x0000000c },

	//-------------------------------------------------------------------------------
	{0x00030003, 0x000000d0 | (MSC_ENDPOINT_BULKIN & EPNUM_MASK) | (HS_MAX_PKTSIZE_BULK<<19), 0x0000000c }, //bulk in 512

	//-------------------------------------------------------------------------------
	{0x00030004, 0x000000c0 | (MSC_ENDPOINT_BULKOUT & EPNUM_MASK) | (HS_MAX_PKTSIZE_BULK<<19), 0x0000000c }, //bulk out 512

	//-------------------------------------------------------------------------------
	{0x00000000, 0x00000000, 0x00000030 } //end flag
};

// device descriptor: high speed
u32 cfgDevice_HI[] = {
  	//0x12, 0x01, 0x00, 0x02, | 0x00, 0x00, 0x00, 0x40, | 0x51, 0xc2, 0x03, 0x13, | 0x00, 0x01, 0x01, 0x02, | 0x00, 0x01 FIXME
  	0x02000112, 0x40000000, 0xa78805a9, 0x02010100, 0x0103
};

// device descriptor: full speed
u32 cfgDevice_FL[] = {
  //0x12, 0x01, 0x10, 0x01, 0xef, 0x02, 0x01, 0x08, 0xa9, 0x05, 0x39, 0x05, 0x00, 0x01, 0x01, 0x02, 0x00, 0x01 FIXME
 	0x01100112, 0x40000000, 0xa78805a9, 0x02010100, 0x0103
};

// FIXME
// string 0
u32 cfgStr0[] = {
   //0x04, 0x03, 0x09, 0x04
   0x04090304
};

// FIXME
// string 1
u32 cfgStr1[] = {
   // OmniVision Technologies, Inc.
   //0x3c, 0x03, 0x4f, 0x00, 0x6d, 0x00, 0x6e, 0x00,
   //0x69, 0x00, 0x56, 0x00, 0x69, 0x00, 0x73, 0x00,
   //0x69, 0x00, 0x6f, 0x00, 0x6e, 0x00, 0x20, 0x00,
   //0x54, 0x00, 0x65, 0x00, 0x63, 0x00, 0x68, 0x00,
   //0x6e, 0x00, 0x6f, 0x00, 0x6c, 0x00, 0x6f, 0x00,
   //0x67, 0x00, 0x69, 0x00, 0x65, 0x00, 0x73, 0x00,
   //0x2c, 0x00, 0x20, 0x00, 0x49, 0x00, 0x6e, 0x00,
   //0x63, 0x00, 0x2e, 0x00
   0x004f033c, 0x006e006d, 0x00560069, 0x00730069,
   0x006f0069, 0x0020006e, 0x00650054, 0x00680063,
   0x006f006e, 0x006f006c, 0x00690067, 0x00730065,
   0x0020002c, 0x006e0049, 0x002e0063
};

// FIXME
// string 2 
u32 cfgStr2[] = {
   // USB Mass Storage
   //0x24, 0x03, 0x55, 0x00, 0x53, 0x00, 0x42, 0x00,
   //0x20, 0x00, 0x43, 0x00, 0x61, 0x00, 0x6d, 0x00,
   //0x65, 0x00, 0x72, 0x00, 0x61, 0x00, 0x5f, 0x00,
   //0x4f, 0x00, 0x56, 0x00, 0x30, 0x00, 0x37, 0x00,
   //0x38, 0x00, 0x30, 0x00,
   0x00550324, 0x00420053, 0x004d0020, 0x00730061,
   0x00200073, 0x00740053, 0x0072006f, 0x00670061,
   0x00200065,
};

// string 3 
u32 cfgStr3[] = {
   // 788 
   //0x08, 0x03, '7', '8', '8', '0', '0', '0', 
   0x38370308, 0x30303038,
};

u32 cfgQualifier[] = {
   //0x0a, 0x06, 0x00, 0x02, 0xef, 0x02, 0x01, 0x08,
   //0x01, 0x00
   0x0200060a, 0x080102ef, 0x0001
};

u32 cfgConf_Other[] = {
   //0x09, 0x07, 0x27, 0x00, 0x01, 0x01, 0x00, 0x80, 0xfa,   // configuration: 1 interfaces
   //0x09, 0x04, 0x00, 0x00, 0x03, 0xff, 0x00, 0x00, 0x00,   // vendor spec: 3 endpoints
   //0x07, 0x05, 0x81, 0x02, 0x40, 0x00, 0x00,         		 // endpoint 1: bulk in; 64 bytes
   //0x07, 0x05, 0x02, 0x02, 0x40, 0x00, 0x00,         		 // endpoint 2: bulk out; 64 bytes
   //0x07, 0x05, 0x83, 0x03, 0x08, 0x00, 0x0a,         		 // endpoint 3: intr in; 8 bytes; 10 frames intv
   0x00270709, 0x80000101, 0x000409fa, 0x00ff0300,
   0x05070000, 0x00400281, 0x02050700, 0x00004002,
   0x03830507, 0x0a0008
};


