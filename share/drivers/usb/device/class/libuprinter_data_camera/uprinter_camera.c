#include "includes.h"

#define UMASS_CAMERA_FORMAT_YVYU 0
#define UMASS_CAMERA_FORMAT_RAW8 1
#define UMASS_CAMERA_FORMAT_AREC 2
#define UMASS_CAMERA_FORMAT_IMG  3

#ifdef CONFIG_GET_STREAM_RAW_EN
#define UMASS_CAMERA_FB_RAW_ID CONFIG_UMASS_CAMERA_FB_RAW_ID
#else
#define UMASS_CAMERA_FB_RAW_ID  (0)
#endif


#ifdef CONFIG_GET_STREAM_YUV_EN
#define UMASS_CAMERA_FB_YUV_ID CONFIG_UMASS_CAMERA_FB_YUV_ID
#else
#define UMASS_CAMERA_FB_YUV_ID  (1)
#endif


#ifdef CONFIG_GET_STREAM_IMG_EN
#define UMASS_CAMERA_FB_IMG_ID CONFIG_UMASS_CAMERA_FB_IMG_ID
#else
#define UMASS_CAMERA_FB_IMG_ID  (2)
#endif


static int last_fb_id = -1;

static u32 fb_y_base = 0;
static u32 fb_offset = 0;
static u32 fb_y_len;
static u32 fb_uv_base = 0;
static u32 fb_uv_len;

static u32 frameticks=0;
static u32 pltoggle=0;

volatile u8 g_umass_camera_fb_id = 0; //0: FB0 raw preview, 1: FB1 yuv preview, 2: FB2 raw capture.
volatile int replace_fb_id = -1;
FRAME * uprinter_frm = NULL;
int dp_running = 0;
u8 g_umass_camera_format ; //0: YVYU, 1: RAW8

void uprinter_grfc_init(void);

void fb_get_frame(u32 * y_addr_base, u32 * y_len, u32 * uv_addr_base, u32 *uv_len)
{
	int timeout = 5;
	u32 i = 0;
	while(1){
		uprinter_frm = libvs_get_frame(g_umass_camera_fb_id, 200);
		last_fb_id = g_umass_camera_fb_id;
		if(uprinter_frm){
			*y_addr_base = uprinter_frm->addr;	
			*y_len = uprinter_frm->size;
			*uv_addr_base = uprinter_frm->addr1;
			*uv_len = uprinter_frm->size1;
			break;
		}else{
			*y_len = 0;
			timeout--;
			if(timeout == 0){
				break;
			}else{
				continue;
			}
		}
	}
}

static int _format_change = -1; //0: YUV, 1: RAW8
void set_format(int fmt)
{
	if(dp_running){
		dp_running = 0;
	}
	if(fmt >= 0){
		dp_running = 1;
		if(fb_offset == 0){
			g_umass_camera_format = fmt;
			if(replace_fb_id != -1){
				g_umass_camera_fb_id = replace_fb_id;
			}else if(g_umass_camera_format == UMASS_CAMERA_FORMAT_RAW8){//set preview fb id
				g_umass_camera_fb_id = UMASS_CAMERA_FB_RAW_ID;//raw
			}else if(g_umass_camera_format == UMASS_CAMERA_FORMAT_IMG) {
				g_umass_camera_fb_id = UMASS_CAMERA_FB_IMG_ID;//img
			}else{
				g_umass_camera_fb_id = UMASS_CAMERA_FB_YUV_ID;//yuv
			}
#ifdef CONFIG_UPRINTER_HW
			uprinter_grfc_init();
#endif
		}else{
			_format_change = fmt;
#if 0
			if((last_fb_id != -1) && uprinter_frm) {
				libvs_remove_frame(last_fb_id, uprinter_frm);
				fb_offset = 0x00;
			}
#endif
		}
	}
}

#define FBRETBUF_MAX	4096
static u8 __attribute__  ((aligned(4))) fbretbuf[FBRETBUF_MAX];

#if 0
/// add by zxf for usb bulk transfer
static int mytest = 1;
static u8 my_data[2050] ={0x1,0x3,0x5,0x7,0x9,0x11};
void set_my_data(const u32 *data, u32 length){
	fb_y_base = data;
	fb_y_len = length;
}
#endif
int uprinter_camera_read(u8 **outbuf, u32 maxlen)
{
	u32 ret = 0, sof;

	if(!dp_running){
		return 0;
	}

	if(fb_offset == 0x00) {
		if(g_libuprinter_cfg->get_frame){
			g_libuprinter_cfg->get_frame(&fb_y_base, &fb_y_len, &fb_uv_base, &fb_uv_len);
		}/*else if(mytest == 1){
			set_my_data(my_data, 2050);
		}*/else{
			if(!pltoggle){
			  fb_get_frame(&fb_y_base, &fb_y_len, &fb_uv_base, &fb_uv_len);//get new image frame
			  }else{
				  pltoggle = 1 - pltoggle;
				  return 0;
			  }
		}

		if(fb_y_len == 0){
			return 0;
		}

//		debug_printf("Frame:%x(%x) + %x(%x)\n", fb_y_base, fb_y_len, fb_uv_base, fb_uv_len);

		if(frameticks == 0) {
			frameticks = ticks;
		}
	}

	u8 * buf = fbretbuf;
	*outbuf = buf;

	if(maxlen > FBRETBUF_MAX){
		maxlen = FBRETBUF_MAX;
	}
	if(maxlen > fb_y_len + fb_uv_len - fb_offset + UVC_YUV_PLHEADER_SIZE){
		maxlen = fb_y_len + fb_uv_len - fb_offset + UVC_YUV_PLHEADER_SIZE;
	}
	if(maxlen <= UVC_YUV_PLHEADER_SIZE){
		return 0;
	}

	memset(buf, 0xff, maxlen);

	pUvcPayloadHeader pHeader;
	pHeader = (UvcPayloadHeader *)buf;
	pHeader->bHeaderLen = UVC_YUV_PLHEADER_SIZE;
	pHeader->bBitmap = 0x8c | pltoggle;
	pHeader->dwPTS[0] = frameticks & 0xff;
	pHeader->dwPTS[1] = (frameticks >> 8) & 0xff;
	pHeader->dwPTS[2] = (frameticks >> 16) & 0xff;
	pHeader->dwPTS[3] = (frameticks >> 24) & 0xff;
	pHeader->dwSCR[0] = ticks & 0xff;
	pHeader->dwSCR[1] = (ticks >> 8) & 0xff;
	pHeader->dwSCR[2] = (ticks >> 16) & 0xff;
	pHeader->dwSCR[3] = (ticks >> 24) & 0xff;

	sof = (ReadReg32(UDCIF_UDC_STA)>>16) & 0x7ff;
	pHeader->wSOF[0] = sof & 0xff;
	pHeader->wSOF[1] = (sof >> 8) ;

	//debug_printf("Read buf: %x %x\n", off, len);
	if( g_umass_camera_format == UMASS_CAMERA_FORMAT_RAW8 || g_umass_camera_format == UMASS_CAMERA_FORMAT_AREC /*|| mytest == 1*/){
		memcpy( (u8 *)(buf + UVC_YUV_PLHEADER_SIZE), (u8 *)fb_y_base + fb_offset, maxlen - UVC_YUV_PLHEADER_SIZE);
	}else if(g_umass_camera_format == UMASS_CAMERA_FORMAT_IMG){
// FIXME : don't convert yuy2 here for improve performance
		if((fb_offset < fb_y_len) && (fb_y_len - fb_offset) > (maxlen - UVC_YUV_PLHEADER_SIZE)){
			memcpy( (u8 *)(buf + UVC_YUV_PLHEADER_SIZE), (u8 *)fb_y_base + fb_offset, maxlen - UVC_YUV_PLHEADER_SIZE);
		}else if(fb_offset < fb_y_len){
			memcpy( (u8 *)(buf + UVC_YUV_PLHEADER_SIZE), (u8 *)fb_y_base + fb_offset, fb_y_len - fb_offset);
			memcpy( (u8 *)(buf + UVC_YUV_PLHEADER_SIZE + (fb_y_len - fb_offset)), (u8 *)fb_uv_base, maxlen - UVC_YUV_PLHEADER_SIZE - (fb_y_len - fb_offset));
		}else{
			memcpy( (u8 *)(buf + UVC_YUV_PLHEADER_SIZE), (u8 *)fb_uv_base + (fb_offset - fb_y_len), maxlen - UVC_YUV_PLHEADER_SIZE);
		}
	}else{
		u8 * p = (buf + UVC_YUV_PLHEADER_SIZE);
		u8 * s = (u8 *)fb_y_base + fb_offset / 2;
		u8 * s1 = (u8 *)fb_uv_base + fb_offset / 2;
		int i = 0;

		if(fb_offset & 0xf){
			int n = 16 - (fb_offset & 0xf) ;
			s += n/2;
			s1 += n/2;
			s -= 8;
			s1 -= 8;
			if(n >= 12){
				p[i+0] = s[2];
				p[i+1] = s1[5];
				p[i+2] = s[3];
				p[i+3] = s1[1];
				p += 4;
				n -= 4;
			}
			if(n >= 8){
				p[i+0] = s[4];
				p[i+1] = s1[6];
				p[i+2] = s[5];
				p[i+3] = s1[2];
				p += 4;
				n -= 4;
			}
			if(n >= 4){
				p[i+0] = s[6];
				p[i+1] = s1[7];
				p[i+2] = s[7];
				p[i+3] = s1[3];
				p += 4;
				n -= 4;
			}
			s += 8;
			s1 += 8;
		}
		for(; i < maxlen - UVC_YUV_PLHEADER_SIZE; i+=16){
			//YYYYYYYY + UUUUVVVV -> YVYUYVYUYVYUYVYU convert
			p[i+0] = s[0];
			p[i+1] = s1[4];
			p[i+2] = s[1];
			p[i+3] = s1[0];
			p[i+4] = s[2];
			p[i+5] = s1[5];
			p[i+6] = s[3];
			p[i+7] = s1[1];
			p[i+8] = s[4];
			p[i+9] = s1[6];
			p[i+10] = s[5];
			p[i+11] = s1[2];
			p[i+12] = s[6];
			p[i+13] = s1[7];
			p[i+14] = s[7];
			p[i+15] = s1[3];

			s += 8;
			s1 += 8;
		}
	}

	fb_offset += maxlen - UVC_YUV_PLHEADER_SIZE;

	if(fb_offset >= (fb_y_len + fb_uv_len) ) {
		pHeader->bBitmap |= 0x02;
		frameticks = 0;
		pltoggle = 1 - pltoggle;

		fb_offset = 0;

		if(g_libuprinter_cfg->get_frame == NULL && uprinter_frm){
			libvs_remove_frame(g_umass_camera_fb_id, uprinter_frm);//free frame
			last_fb_id = -1;
		}
		if(uprinter_frm){
			dc_flush_range(uprinter_frm, uprinter_frm+sizeof(FRAME));
			dc_invalidate_range(uprinter_frm, uprinter_frm+sizeof(FRAME));
		}
		if(_format_change != -1){
			g_umass_camera_format = _format_change;
			_format_change = -1;
			if(replace_fb_id != -1){
				g_umass_camera_fb_id = replace_fb_id;
			}else if(g_umass_camera_format == UMASS_CAMERA_FORMAT_RAW8){//set preview fb id
				g_umass_camera_fb_id = UMASS_CAMERA_FB_RAW_ID;//raw
			}else if(g_umass_camera_format == UMASS_CAMERA_FORMAT_IMG) {
				g_umass_camera_fb_id = UMASS_CAMERA_FB_IMG_ID;//img
			}else{
				g_umass_camera_fb_id = UMASS_CAMERA_FB_YUV_ID;//yuv
			}
		}
	}

	return maxlen;
}

