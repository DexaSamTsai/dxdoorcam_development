#include "includes.h"

static u8 serial_state_response[SERIAL_STATE_NUM+2] __attribute__((aligned(4)))=    
{   
     0xa1, 0x20, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00,  
};   

void usbclass_HandleSetif(int if_id, int alt)
{

}

int usbclass_HandleRequest(void)
{
	u8 *tmp_addr = (u8 *)g_libusb_cfg.req.databuf;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
	switch(g_libusb_cfg.req.request.bRequest) 
	{
		case ACM_GET_LINECODING:	
			tmp_addr[0] = g_libcdc_cfg.baud_rate & 0xff;
			tmp_addr[1] = (g_libcdc_cfg.baud_rate>>8) & 0xff;
			tmp_addr[2] = (g_libcdc_cfg.baud_rate>>16) & 0xff;
			tmp_addr[3] = (g_libcdc_cfg.baud_rate>>24) & 0xff;
			tmp_addr[4] = 0;  // 1 stop bit
			tmp_addr[5] = 0;  // none parity
			tmp_addr[6] = 8;  // 8 bits

			g_libusb_cfg.req.wLength = 7;
			debug_printf("Get: rate %d\n", g_libcdc_cfg.baud_rate);
			//ENABLE_EPINTR(g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK);
			break;

		case ACM_SET_LINECODING:
			g_libcdc_cfg.baud_rate = (tmp_addr[0] | (tmp_addr[1]<<8) | (tmp_addr[2]<<16) | (tmp_addr[3]<<24));
			g_libcdc_cfg.status = CDC_STATUS_READY;
			debug_printf("Set: rate %d, Stop %d, Parity %d, Data %d\n", g_libcdc_cfg.baud_rate, tmp_addr[4], tmp_addr[5], tmp_addr[6]);
			break;

		case ACM_SET_CTRLLINESTATE:
			debug_printf("control signal: 0x%x\n", (g_libusb_cfg.req.request.wValue_H<<8) + g_libusb_cfg.req.request.wValue_L);	
			break;

		default:
			debug_printf("unsupport cmd: %x\n", g_libusb_cfg.req.request.bRequest);
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			g_libusb_cfg.req.wLength = 0;
			break;
	}

	return 1;
}

void usbclass_HandleCDC(u32 *args)
{
	u8 ep_in, ep_out;

	ep_in  = (g_libcdc_cfg.CDC_EP_IN) & EPNUM_MASK;
	ep_out = (g_libcdc_cfg.CDC_EP_OUT) & EPNUM_MASK; 

//	if(args & MASK_OF_EPINTR(ep_in))  //EP in
	{
		if(EPBUF_IS_EMPTY(ep_in)) {
			*args = *args & ~(MASK_OF_EPINTR(ep_in));
			g_libcdc_cfg.cdc_bulk_in();
		}
	}

	if(*args & MASK_OF_EPINTR(ep_out)) //EP out
	{
		if(EPBUF_IS_NOTEMPTY(ep_out)){
			*args = *args & ~(MASK_OF_EPINTR(ep_out));
			g_libcdc_cfg.cdc_bulk_out();
		}
	}
}
