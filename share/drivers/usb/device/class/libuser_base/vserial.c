#include "includes.h"

#define VSER_BUFF_SIZE 0x200
///< for the data from stdin
typedef struct s_vser_buffer{
	u8 buffer[VSER_BUFF_SIZE];
	int off_wr; 
	int off_rd;
	int length;
}t_vser_buffer;

t_vser_buffer rx_buffer, tx_buffer; 

typedef struct s_usbdebug
{
#define VSERIAL_USBBUF  (512)
	u8 * buf;
	pthread_t threadid;
	int askexit; //0:running. 1:askexit. 2:exited
	t_ertos_eventhandler exit_event;
	t_debugprintf * dp;
	int usbirq;
}t_usbdebug;

void handle_cdc_bulkin(void)
{
#ifdef CDC_TEST
	int len, size;

	if(tx_buffer.length){
		if((tx_buffer.off_rd < tx_buffer.off_wr) && tx_buffer.length) {
			len = tx_buffer.off_wr - tx_buffer.off_rd; 
			len = udc_writeEP(VSER_EP_BULKIN, tx_buffer.buffer + tx_buffer.off_rd, len);
			if(len > 0){
				tx_buffer.off_rd = 0;
				tx_buffer.off_wr = 0;
				tx_buffer.length -= len;
			}
		}else if(tx_buffer.off_rd > tx_buffer.off_wr) {
			len = VSER_BUFF_SIZE - tx_buffer.off_rd; 
			len = udc_writeEP(VSER_EP_BULKIN, tx_buffer.buffer, len);
			if(len > 0){
				tx_buffer.off_rd = 0;
				tx_buffer.length -= len;
			}
		}
		if(tx_buffer.length){
			ENABLE_EPINTR(g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK);
			return;
		}
	}
#endif
}

void handle_cdc_bulkout(void)
{
	int len, n;
	u8 notify = 0;
	int ret = 0;

#ifdef CDC_TEST
	len = udc_readEP(VSER_EP_BULKOUT, tx_buffer.buffer + tx_buffer.off_wr);
	tx_buffer.off_wr += len;
	tx_buffer.length += len;
#else
#ifdef CONFIG_ERTOS_CMDSHELL_EN
	len = udc_readEP(VSER_EP_BULKOUT, tx_buffer.buffer);
	for(n=0; n<len;){
		ret = cmdshell_input_char(tx_buffer.buffer[n]);
		if(ret == -2 && notify == 0){
			notify = 1;
			cmdshell_input_notify();
		}else{
			notify = 0;
			n++;
		}
	}
	if(notify == 0){
		cmdshell_input_notify();
	}
#else
	len = udc_readEP(VSER_EP_BULKOUT, tx_buffer.buffer);
	for(n=0; n<len; n++){
		debug_printf("%c",tx_buffer.buffer[n]);
	}
#endif
#endif
	if(!EPINTR_IS_ENABLED((g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK))){
		ENABLE_EPINTR(g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK);
	}
}

int usbclass_init(int first)
{
	tx_buffer.off_wr = rx_buffer.off_wr = 0;
	tx_buffer.off_rd = rx_buffer.off_rd = 0;
	tx_buffer.length = rx_buffer.length = 0;

	debug_printf("usb serial baud rate(DEF): %x\n", g_libcdc_cfg.baud_rate);
}

void usbclass_clean(void)
{

}

static void udev_ctrlsetup_irq_handler(t_usbdebug * ud)
{
	WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)&~BIT16);
	libusb_chip_isr |= BIT3;
	ud->usbirq = 1;
	pthread_cond_signal(&ud->dp->cond);
}

static void udev_ctrldata_irq_handler(t_usbdebug * ud)
{
	libusb_chip_isr |= BIT9;
	ud->usbirq = 1;
	pthread_cond_signal(&ud->dp->cond);
}

static void udev_ctrlsts_irq_handler(void)
{
	udcif_request_sendack(SEND_ACK_NEXTSTATUS, 1); 
}

volatile int udc_val=0;
static void udev_suspend_irq_handler(t_usbdebug * ud)
{
	if(udc_val != ReadReg32(UDCIF_UDC_STA)){
		udc_val = ReadReg32(UDCIF_UDC_STA);
		if(ud != NULL){
			ud->usbirq = 1;
		}
	}
}

static void udev_ep_irq_handler(t_usbdebug * ud)
{
	libep_irq |= ReadReg32(UDCIF_P_IRQ) & 0x3f;
	DISABLE_EPINTR(1);
	DISABLE_EPINTR(2);
	DISABLE_EPINTR(3);
	DISABLE_EPINTR(4);
	DISABLE_EPINTR(5);
	DISABLE_EPINTR(6);
	WriteReg32(UDCIF_P_IRQ, libep_irq);
	libusb_chip_isr |= BIT19;
	ud->usbirq = 1;
	pthread_cond_signal(&ud->dp->cond);
}

static void udev_sta_irq_handler(t_usbdebug * ud)
{
	libudc_irq |= ReadReg32(UDCIF_UDC_IRQ) & (BIT2|BIT3|BIT23|BIT24|BIT26) ;
	WriteReg32(UDCIF_UDC_IRQ, BIT2|BIT3|BIT23|BIT24|BIT26);
	libusb_chip_isr |= BIT20;
	ud->usbirq = 1;
	pthread_cond_signal(&ud->dp->cond);
}

int interrupt_handler_init(t_usbdebug * ud)
{
	irq_free(IRQ_BIT_USBSETUP);
	irq_request(IRQ_BIT_USBSETUP, udev_ctrlsetup_irq_handler, "UDEV.setup", ud);
	irq_free(IRQ_BIT_USB);
	irq_request(IRQ_BIT_USB, udev_ctrldata_irq_handler, "UDEV.data", ud);
	irq_free(IRQ_BIT_USBSTA);
	irq_request(IRQ_BIT_USBSTA, udev_ctrlsts_irq_handler, "UDEV.sts", NULL);
	irq_free(IRQ_BIT_USBUDCEP);
	irq_request(IRQ_BIT_USBUDCEP, udev_ep_irq_handler, "UDEV.ep", ud);
	irq_free(IRQ_BIT_USBUDC);
	irq_request(IRQ_BIT_USBUDC, udev_sta_irq_handler, "UDEV.sta", ud);
	irq_free(IRQ_BIT_USBSUSPEND);
	irq_request(IRQ_BIT_USBSUSPEND, udev_suspend_irq_handler, "UDEV.suspend", NULL);

	return 0;
}

void * usbprint_main(void * arg)
{
	t_usbdebug *ud = (t_usbdebug *)(arg);
	int len;

	pthread_detach(pthread_self());
	pthread_setname(NULL, "USBDBG");

	while(1){
		if(ud->askexit == 1){
			break;
		}
		pthread_cond_wait(&ud->dp->cond, NULL);
		
		if(ud->usbirq){
			libusb_loop();
			ud->usbirq = 0;
		}

		if(!EPBUF_IS_EMPTY((VSER_EP_BULKIN&EPNUM_MASK))) {  // buffer not empty
			continue;
		}

		if(g_libusb_cfg.status != UDEVSTS_CONFIG || g_libcdc_cfg.status == CDC_STATUS_IDLE){
			DISABLE_EPINTR(g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK);
			continue;
		}
		
		int n = debug_printf_getbuf(ud->dp, ud->buf, sizeof(ud->buf));
	
		if(n > 0){
			len = udc_writeEP(VSER_EP_BULKIN, ud->buf, n);
			ENABLE_EPINTR(g_libcdc_cfg.CDC_EP_IN & EPNUM_MASK);
		}
	}
	
	ud->askexit = 2;

	ertos_event_signal(ud->exit_event);

	return NULL;
}

static int usb_debug_init(void * arg, t_debugprintf * dpcfg , void ** hw_arg){

	t_usbdebug * ud = (t_usbdebug *)malloc(sizeof(t_usbdebug));
	if(ud == NULL){
		syslog(LOG_ERR, "ERR: usb debug malloc fail\n");
		return -1;
	}
	memset(ud, 0, sizeof(t_usbdebug));
	
	ud->buf = (u8 *)malloc(VSERIAL_USBBUF);
	if(ud->buf == NULL){
		syslog(LOG_ERR, "ERR: usbbuf malloc fail\n");
		return -1;
	}
	ud->dp = dpcfg;
	
	*hw_arg = (void *)ud;
	
	if((dpcfg->flag & DEBUGPRINTF_MODE_VALID_VAL) == DEBUG_PRINTF_MODE_DIRECT){
		return 0;
	}
	
	libusb_init();
	interrupt_handler_init(ud);
	
	ud->exit_event = ertos_event_create();
	if(ud->exit_event == NULL){
		syslog(LOG_ERR, "[ERROR] create uart debug exit_event error\n");
		free(ud->buf);
		free(ud);
		return -1;
	}

	pthread_attr_t thread_attr;
	pthread_attr_init(&thread_attr);
	int prio = (int)(arg);

	if(prio != 0){
		thread_attr.schedparam.sched_priority = prio; //prio
	}
	int ret = pthread_create(&ud->threadid, &thread_attr, usbprint_main, ud);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR] create uart debug thread error\n");
		ertos_event_delete(ud->exit_event);
		free(ud->buf);
		free(ud);
		return -1;
	}
	
	return 0;
}


static int usb_debug_exit(void * hw_arg, t_debugprintf * dpcfg)
{
	t_usbdebug * ud = (t_usbdebug *)(hw_arg);

	if((dpcfg->flag & DEBUGPRINTF_MODE_VALID_VAL) == DEBUG_PRINTF_MODE_DIRECT){
		free(ud);
		return 0;
	}

	ud->askexit = 1;
	pthread_cond_signal(&ud->dp->cond);
	ertos_event_wait(ud->exit_event, WAIT_INF);
	ertos_event_delete(ud->exit_event);
	free(ud->buf);
	free(ud);
	return 0;
}

const t_debugprintf_hw g_debugprintf_hw_usb = {
	.hw_init = usb_debug_init,
	.hw_exit = usb_debug_exit,
};
