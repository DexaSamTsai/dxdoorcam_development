#include "includes.h"


#define UPRINTER_MAXBUF_SIZE		(0x1000 + 32)
static unsigned char UPRINTER_EP_BUF[UPRINTER_MAXBUF_SIZE]; 
extern volatile int replace_fb_id;

void usbclass_HandleSetif(int if_id, int alt)
{

}
extern void app_reg_wr(u8 sccb_id, u16 addr, u8 data);
extern int app_reg_rd(u8 sccb_id, u16 addr);
int firmware_addr = 0;
static void read_data()
{	
	unsigned int addr;

	addr = g_libusb_cfg.req.request.wIndex_L + (g_libusb_cfg.req.request.wIndex_H<<8);
	addr = addr<<16;
	addr += g_libusb_cfg.req.request.wValue_L + (g_libusb_cfg.req.request.wValue_H<<8);

	if(g_libusb_cfg.req.wLength == 4){
		u32 * reg = (u32 *)g_libusb_cfg.req.databuf;
		*reg = ReadReg32(addr);
	}else if(g_libusb_cfg.req.wLength == 2){
		u16 * reg = (u16 *)g_libusb_cfg.req.databuf;
		*reg = ReadReg16(addr);
	}else if(g_libusb_cfg.req.wLength == 1){
		u8 * reg = (u8 *)g_libusb_cfg.req.databuf;
		*reg = ReadReg8(addr);
	}else{
		memcpy(g_libusb_cfg.req.databuf, addr, g_libusb_cfg.req.wLength);
	}
}

static void write_data()
{
	unsigned int addr;

	addr = g_libusb_cfg.req.request.wIndex_L + (g_libusb_cfg.req.request.wIndex_H<<8);
	addr = addr<<16;
	addr += g_libusb_cfg.req.request.wValue_L + (g_libusb_cfg.req.request.wValue_H<<8);

	if(g_libusb_cfg.req.wLength == 4){
		u32 data = *((u32 *)g_libusb_cfg.req.databuf);
		WriteReg32(addr, data);
	}else if(g_libusb_cfg.req.wLength == 2){
		u16 data = *((u16*)g_libusb_cfg.req.databuf);
		WriteReg16(addr, data);
	}else if(g_libusb_cfg.req.wLength == 1){
		u8 data = *((u8 *)g_libusb_cfg.req.databuf);
		WriteReg8(addr, data);
	}else{
		memcpy(addr, g_libusb_cfg.req.databuf, g_libusb_cfg.req.wLength);
	}
}

extern volatile u8 g_umass_camera_fb_id;

#if 0
#define MAX_DATA_LENGTH (1024*1024)
static u32 total_data = 0;
static u32 length_send_data = 0;
static u32 length_rece_data = 0;
u8 data[130];
u8 *p_rece_data = NULL;
u8 *p_data = NULL;
u8 *p_temp = NULL;


void set_data_length(u32 length){
    if(length > MAX_DATA_LENGTH){
    	debug_printf("no more space!\n");
    	return;
    }
    length_send_data = length;
	g_libusb_cfg.req.databuf[0] =  length_send_data & 0xff;
	g_libusb_cfg.req.databuf[1] = (length_send_data & 0xff00) >> 8;
	g_libusb_cfg.req.databuf[2] = (length_send_data & 0xff0000) >> 16;
	g_libusb_cfg.req.databuf[3] = (length_send_data & 0xff000000) >> 24;
}

static void send_datatohost(u8 **data){
	if (data == NULL) {
		debug_printf("the point data is null\n");
		return;
	}
	debug_printf("the data = %d\n", **data);
	memcpy(g_libusb_cfg.req.databuf, *data, g_libusb_cfg.req.wLength);
	*data += g_libusb_cfg.req.wLength;
}
static void rece_datafromhost(u8 **data){
	if (data == NULL) {
		debug_printf("the point data is null\n");
		return;
	}
	memcpy(*data, g_libusb_cfg.req.databuf,g_libusb_cfg.req.wLength);
	*data += g_libusb_cfg.req.wLength;
}
void show_data(const char*data, u32 length){
	u32 i = 0;
	for(i = 0; i < length; i++){
		debug_printf("[%x] ", data[i]);
		if(i % 16 == 0 && i != 0){
			debug_printf("\n");
		}
	}
}
#endif
int usbclass_HandleRequest(void)
{
	u32 length = g_libusb_cfg.req.wLength;
	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;
       // debug_printf("g_libusb_cfg.req.request.bRequest = %x\n",g_libusb_cfg.req.request.bRequest);
	if((g_libusb_cfg.req.request.bmRequestType == 0x22)||(g_libusb_cfg.req.request.bmRequestType == 0x23)) {
		if(g_libusb_cfg.req.request.bRequest == 0x42) {
			write_data();
			g_libusb_cfg.req.wLength = 0;
		}else if(g_libusb_cfg.req.request.bRequest == 0x44) {//for bulk transfer info
			firmware_addr = g_libusb_cfg.req.request.wIndex_L + (g_libusb_cfg.req.request.wIndex_H<<8);
			firmware_addr = firmware_addr<<16;
			firmware_addr += g_libusb_cfg.req.request.wValue_L + (g_libusb_cfg.req.request.wValue_H<<8);

			ENABLE_EPINTR(g_libuprinter_cfg->ep_out & EPNUM_MASK);

			g_libuprinter_cfg->rx_len = g_libusb_cfg.req.wLength;
			
			g_libusb_cfg.req.wLength = 0;

		}else if(g_libusb_cfg.req.request.bRequest == 0x45) {//for get raw frames info  -- clear FB buffers 
			u8 frame_cnt, i;
			int timeout = 5;
			FRAME * frm = NULL;
			frame_cnt = g_libusb_cfg.req.request.wValue_L;
//			debug_printf("frame_cnt=%d, fb_id=%d\n", frame_cnt, g_umass_camera_fb_id);
			for(i=0; i<frame_cnt; i++) {
				while(1) {
					frm = libvs_get_frame(g_umass_camera_fb_id, 20);
					if(frm) {
						libvs_remove_frame(g_umass_camera_fb_id, frm);//free frame
						timeout=5;
						break;
					}else {
						if(timeout-- <= 0) {
							debug_printf("timeout: index=%d\n", i+1);
							break;
						}
					}
				}
			}//end for

			g_libusb_cfg.req.wLength = 0;
		}else if(g_libusb_cfg.req.request.bRequest == 0x46) {//select FB datapath
			if(g_libusb_cfg.req.request.wValue_L & 0x80){ // 0x80, force replace FB ID while set_format
				replace_fb_id = g_libusb_cfg.req.request.wValue_L = g_libusb_cfg.req.request.wValue_L & 0x7F;
			}else{
				replace_fb_id = -1;
			}
			g_umass_camera_fb_id = g_libusb_cfg.req.request.wValue_L;
//			debug_printf("FB datapath value=%d\n", g_umass_camera_fb_id);
			g_libusb_cfg.req.wLength = 0;
			switch(g_umass_camera_fb_id) {
			case 0://raw preview
				libvs_stop(2);//stop raw capture FB2
				libvs_exit(2);
				break;
			case 1://yuv preview
				libvs_stop(3);//stop yuv capture FB3
				libvs_exit(3);
				break;
			case 2://raw capture
				libvs_init(2);
				libvs_start(2);
				break;
			case 3://yuv capture
				libvs_init(3);
				libvs_start(3);
				break;
			default:
				break;
			}
		}
		return 1;
	}else if((g_libusb_cfg.req.request.bmRequestType == 0xa2)||(g_libusb_cfg.req.request.bmRequestType == 0xa3)) {
		if(g_libusb_cfg.req.request.bRequest == 0x41) {
//			lowlevel_putc('H');
			read_data();
		}
		return 1;
	}
	if(g_libusb_cfg.req.request.bRequest == 0x04){
		if(DATA_FROM_HOST){
			u8 sccb_id = g_libusb_cfg.req.databuf[0];
			u16 sccb_addr = (g_libusb_cfg.req.request.wValue_H << 8) | g_libusb_cfg.req.request.wValue_L ;
			u8 sccb_v = g_libusb_cfg.req.databuf[1];

			app_reg_wr(sccb_id, sccb_addr, sccb_v);

		}
	}else if(g_libusb_cfg.req.request.bRequest > 5 && g_libusb_cfg.req.request.bRequest< 100 ){
		u16 sccb_addr = (g_libusb_cfg.req.request.wValue_H << 8) | g_libusb_cfg.req.request.wValue_L ;
		u8 sccb_id = g_libusb_cfg.req.request.bRequest;

		if(DATA_TO_HOST){
			int sccb_v = app_reg_rd(sccb_id, sccb_addr);
			if(sccb_v < 0){
				g_libusb_cfg.req.databuf[0] = 0xff;
				g_libusb_cfg.req.databuf[1] = 0x04;
			}else{
				g_libusb_cfg.req.databuf[0] = sccb_v & 0xff;
				g_libusb_cfg.req.databuf[1] = 0x00;
			}

			g_libusb_cfg.req.wLength = 2;
		}else{
			//TODO
			debug_printf("set sccb reg not implemented\n");
		}
	}else if(g_libusb_cfg.req.request.bRequest == 0x66){
#if 0
		length_rece_data = g_libusb_cfg.req.databuf[3] <<24 | g_libusb_cfg.req.databuf[2] << 16 |  g_libusb_cfg.req.databuf[1] << 8 | g_libusb_cfg.req.databuf[0];
		debug_printf("the length_data = %d\n",length_rece_data);
		p_data = malloc(1024 * 1024);
		if (p_data == NULL) {
			debug_printf("malloc failed\n");
		}
		p_temp = p_data;
#endif
	}else if(g_libusb_cfg.req.request.bRequest == 0x68){
#if 0
		if (length_rece_data > MAX_DATA_LENGTH) {
			debug_printf("no more space!\n");
			return -1;
		}
		rece_datafromhost(&p_temp);
		total_data += length;
#endif
	}else if(g_libusb_cfg.req.request.bRequest == 0x69){
#if 0
		debug_printf("enter the command = [0x%x]\n",g_libusb_cfg.req.request.bRequest);
		set_data_length(130);
		debug_printf("init data!\n");
		u32 i;
		for (i = 0; i < 130; i++) {
			data[i] = i + 1;
		}
		p_rece_data = data;
#endif
	}else if(g_libusb_cfg.req.request.bRequest == 0x71){
#if 0
		debug_printf("enter the command = [0x%x]\n",g_libusb_cfg.req.request.bRequest);
		send_datatohost(&p_rece_data);
#endif
	}else{
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
		g_libusb_cfg.req.wLength = 0;
	}
#if 0
	if(total_data == length_rece_data){
		debug_printf("enter the 0x67\n");
		if(p_data != NULL)
		show_data(p_data,length_rece_data);
		free(p_data);
		p_data = NULL;

	}
#endif
	return 1;
}

/* this func will be called before USB start, for init some class-specific data */
int usbclass_init(int first)
{
	t_libuprinter_cfg *cfg = (t_libuprinter_cfg *)(g_libusbclass_cfg_hs.arg);

	if(first)
	{
		if(cfg) {
			cfg->printer_init(cfg);
		}
	}

	return 0;
}

void usb_printer_init(struct s_libuprinter_cfg *cfg)
{
	cfg->rx_buffer = UPRINTER_EP_BUF;
	cfg->tx_buffer = UPRINTER_EP_BUF;

	//disable ep
	WriteReg32(UDCIF_EP_CFG0(cfg->ep_in & EPNUM_MASK), (ReadReg32(UDCIF_EP_CFG0(cfg->ep_in & EPNUM_MASK)) & 0x7fffffff));
	WriteReg32(UDCIF_EP_CFG0(cfg->ep_out & EPNUM_MASK), (ReadReg32(UDCIF_EP_CFG0(cfg->ep_out & EPNUM_MASK)) & 0x7fffffff));
	{volatile int dd; for(dd=0; dd<5000; dd++);}
	//enable ep
	WriteReg32(UDCIF_EP_CFG0(cfg->ep_in & EPNUM_MASK), (ReadReg32(UDCIF_EP_CFG0(cfg->ep_in & EPNUM_MASK)) | 0x80000000));
	WriteReg32(UDCIF_EP_CFG0(cfg->ep_out & EPNUM_MASK), (ReadReg32(UDCIF_EP_CFG0(cfg->ep_out & EPNUM_MASK)) | 0x80000000));

	ENABLE_EPINTR(cfg->ep_in & EPNUM_MASK);
	ENABLE_EPINTR(cfg->ep_out & EPNUM_MASK);
}

void uprinter_bulk_in(void)
{
	g_libuprinter_cfg->tx_len = g_libuprinter_cfg->read(&g_libuprinter_cfg->tx_buffer, SIZE_2K);

	if(g_libuprinter_cfg->tx_len == 0){
		//debug_printf("%s,%d, %d\n", __FUNCTION__, __LINE__, g_libuprinter_cfg->tx_len);
		udc_writeEP(g_libuprinter_cfg->ep_in, g_libuprinter_cfg->tx_buffer, g_libuprinter_cfg->tx_len);
	}else if(g_libuprinter_cfg->tx_len % 512 == 0 && g_libuprinter_cfg->tx_len < SIZE_2K){
		//debug_printf("%s,%d, %d\n", __FUNCTION__, __LINE__, g_libuprinter_cfg->tx_len);
	    udc_writeEP(g_libuprinter_cfg->ep_in, g_libuprinter_cfg->tx_buffer, g_libuprinter_cfg->tx_len+1);
	}else{
		//debug_printf("%s,%d, %d\n", __FUNCTION__, __LINE__, g_libuprinter_cfg->tx_len);
	    udc_writeEP(g_libuprinter_cfg->ep_in, g_libuprinter_cfg->tx_buffer, g_libuprinter_cfg->tx_len);
	}
	 // udc_writeEP(g_libuprinter_cfg->ep_in, g_libuprinter_cfg->tx_buffer, g_libuprinter_cfg->tx_len);
	ENABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
}

void uprinter_bulk_out(void)
{
	u32 len;

	len = udc_readEP(g_libuprinter_cfg->ep_out, g_libuprinter_cfg->rx_buffer);
	if(len) {
		memcpy(firmware_addr, g_libuprinter_cfg->rx_buffer, len);
		firmware_addr += len;
	}
}

void usb_printer_handle_bulk(u32 *args)
{
    u8 ep_in, ep_out;	

	ep_in  = (g_libuprinter_cfg->ep_in) & EPNUM_MASK;
	ep_out = (g_libuprinter_cfg->ep_out) & EPNUM_MASK; 

	if(*args & MASK_OF_EPINTR(ep_in))  //EP in 
	{ 
		if(EPBUF_IS_EMPTY(ep_in))
		{
			*args = *args & ~(MASK_OF_EPINTR(ep_in));
			uprinter_bulk_in();
		}
	}

	if(*args & MASK_OF_EPINTR(ep_out)) //EP out
	{
		if(EPBUF_IS_NOTEMPTY(ep_out))  
		{
			*args = *args & ~(MASK_OF_EPINTR(ep_out));
			uprinter_bulk_out();
		}
	}
}


void usbclass_clean(void)
{
}
