#include "includes.h"

#ifdef CONFIG_UPRINTER_HW

#define UPRINTER_BUFFER    CONFIG_UPRINTER_HW_MEMBASE 

void udcif_grfc_cfg(int addr, int length);
int uprinter_camera_convert(u8 *buffer);

int uprinter_grfc_cb(void)
{
	int length;

	length = uprinter_camera_convert(UPRINTER_BUFFER);
	if(length) {
		//TODO: avoid to buffer overflow on calitool
		//length = (length+255-UVC_YUV_PLHEADER_SIZE)&(~0xff);
		//debug_printf("frame size: %x\n", length);
		udcif_grfc_cfg(UPRINTER_BUFFER - 0x10100000, length);
	}

	return 1;
}

int uprinter_grfc_init(void)
{
	debug_printf("@uprinter grfc init\n");

	DISABLE_EPINTR(g_libuprinter_cfg->ep_in & EPNUM_MASK);
	WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) & ~BIT_VIDEO_EN);
	{volatile int d; for(d=0; d<10; d++); }
	WriteReg32(UDCIF_VIDEO_CFG, (ReadReg32(UDCIF_VIDEO_CFG) & 0xffff3fff) | 0x4000);//app mod for uvc raw
	WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) | BIT_VIDEO_EN);
	udcif_grfc_cfg(UPRINTER_BUFFER - 0x10100000, 1024);

	return 1;
}
#endif
