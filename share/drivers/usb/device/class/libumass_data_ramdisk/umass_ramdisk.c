/*----------------------------------------------------------------------------
 *	  U S B  -  K e r n e l
 *----------------------------------------------------------------------------
 *	  Name:	umass_ramdisk.c
 *	  Purpose: Mass Storage Class Custom User Module for ram-disk 
 *	  Version: V1.10
 *---------------------------------------------------------------------------*/
#include "includes.h"

#define RAMDISK_BASE_ADDR  (0x10100000)
#define RAMDISK_SIZE  0x1000000

extern __lundriver_cfg t_lundriver_cfg g_ramdisk_lundriver;

static int ramdisk_rd(u32 arg, u32 off, int buf, u32 len) 
{
	memcpy((u8 *)buf, (u8 *)(arg + (off<<9)), len);
	return 0;
}

static int ramdisk_wr(u32 arg, u32 off, int buf, u32 len)
{   
	memcpy((u8 *)(arg + (off<<9)), (u8 *)buf, len);
	return 0;
}

static int msc_ramdisk_init (void) 
{
	debug_printf("++msc_ramdisk_init\n");

	g_ramdisk_lundriver.lun_param = UMASS_LUN_REMOVABLE;
	g_ramdisk_lundriver.block_cnt = (RAMDISK_SIZE>>9);
	g_ramdisk_lundriver.block_size = 0x200;
	g_ramdisk_lundriver.size = g_ramdisk_lundriver.block_cnt*0x200;
	g_ramdisk_lundriver.lun_arg = RAMDISK_BASE_ADDR;

	debug_printf("Ramdisk size: %d K, sector: 0x%x, sectors: 0x%x\n",(g_ramdisk_lundriver.size)>>10,
	  										g_ramdisk_lundriver.block_size, g_ramdisk_lundriver.block_cnt);

	return 0;
}

__lundriver_cfg t_lundriver_cfg g_ramdisk_lundriver = { 
	.product_info = "ramdisk disk",
	.lun_id = UMASS_ID_RAM,
	.init = msc_ramdisk_init,
	.msc_read = ramdisk_rd,
	.msc_write = ramdisk_wr,
};
