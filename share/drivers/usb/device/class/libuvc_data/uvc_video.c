#include "includes.h"

extern s32 board_ioctl(u32 cmd, u32 arg, void * ret_arg);

/* uvc set if*/
void uvc_setif(t_libuvc_strmif *strmif, int alt)
{
	if(alt){
		strmif->v_status = UVC_STATUS_RUN;
		board_ioctl(BIOC_UVC_START_VSTREAM, (u32)strmif, NULL);
		ENABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}else{
    	board_ioctl(BIOC_UVC_STOP_VSTREAM, (u32)strmif, NULL);
		strmif->v_status = UVC_STATUS_IDLE;
		DISABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}
}

/* uvc frame set*/
void uvc_frame_set(t_libuvc_strmif * strmif)
{
    uvc_encode_init(strmif);

	// change fill_data callback
    board_ioctl(BIOC_UVC_FILL_DATA, (u32)strmif, NULL);

	// change frame-set callback
    if(board_ioctl(BIOC_UVC_SET_VSTREAM, (u32)strmif, NULL) < 0)  {
		strmif->frame_set(strmif->frame[strmif->frameindex].size, strmif->framerate);
    }

	/* bulk didn't support set-if  */
	if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_BULK) {
		strmif->v_status = UVC_STATUS_RUN;
	}
}

