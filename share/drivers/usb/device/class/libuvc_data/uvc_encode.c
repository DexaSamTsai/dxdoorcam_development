#include "includes.h"

//suppose HS's buffer is larger than LS
static u8 uvc_tx_buf[HS_MAX_PKTSIZE_ISO*3+0x40];

void uvc_encode_init(t_libuvc_strmif *strmif)
{
	memset(&(strmif->uvc), 0x00, sizeof(t_libuvc_param));
	strmif->uvc.status = UVC_BUFF_STATUS_END;

	if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_ISO) {
		switch(strmif->format) {
			case VS_FORMAT_MPEG2TS:
			case VS_FORMAT_VENDOR_ENCV:
				strmif->uvc.header_size = UVC_ENCV_PLHEADER_SIZE;
				break;

			default:
				strmif->uvc.header_size = UVC_MIMG_PLHEADER_SIZE;
				break;
		}
	}else if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_BULK) {
		strmif->uvc.header_size = UVC_BULK_PLHEADER_SIZE;
	}
}

static void uvc_encode_start(t_libuvc_strmif *strmif, int length)
{
	t_libuvc_param *uvc = &(strmif->uvc);

	uvc->status = 0;
	uvc->offset = 0;
	if((strmif->format == VS_FORMAT_MPEG2TS) || (strmif->format == VS_FORMAT_VENDOR_ENCV)) {
		uvc->size = 0x1ffffff;
	}else {
		uvc->size = length;
	}
	uvc->pl_frmticks = ticks;
}

static void uvc_encode_header(t_libuvc_strmif *strmif, char *buffer, int length)
{
	pUvcPayloadHeader pHeader;
	t_libuvc_param *uvc = &(strmif->uvc);
	u16 sof;

	pHeader = (UvcPayloadHeader *)buffer;
	if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_ISO) {
		switch(strmif->format) {
			case VS_FORMAT_MPEG2TS:
			case VS_FORMAT_VENDOR_ENCV:
				pHeader->bHeaderLen = UVC_ENCV_PLHEADER_SIZE;
				pHeader->bBitmap = 0x00;
				break;

			default:	
				if(uvc->offset == 0) {
					uvc->pl_frmticks = ticks;
				}

				pHeader->bHeaderLen = UVC_MIMG_PLHEADER_SIZE;
				pHeader->bBitmap = 0x8c | uvc->pl_toggle;
				if(uvc->pl_sti) {
					pHeader->bBitmap |= UVC_HDR_STI;
				}

				if((uvc->offset + length) >= uvc->size) {
					pHeader->bBitmap |= UVC_HDR_EOF;   //the last packet
				}

				pHeader->dwPTS[0] = uvc->pl_frmticks & 0xff;
				pHeader->dwPTS[1] = (uvc->pl_frmticks >> 8) & 0xff;
				pHeader->dwPTS[2] = (uvc->pl_frmticks >> 16) & 0xff;
				pHeader->dwPTS[3] = (uvc->pl_frmticks >> 24) & 0xff;

				pHeader->dwSCR[0] = ticks & 0xff;
				pHeader->dwSCR[1] = (ticks >> 8) & 0xff;
				pHeader->dwSCR[2] = (ticks >> 16) & 0xff;
				pHeader->dwSCR[3] = (ticks >> 24) & 0xff;

				sof = (ReadReg32(UDCIF_UDC_STA)>>16) & 0x7ff;
				pHeader->wSOF[0] = sof & 0xff;
				pHeader->wSOF[1] = (sof >> 8) ;
				break;
		}	
	}
	else if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_BULK) {
		if(uvc->offset == 0) { // payload size = 1 video frame_size
			uvc->header_size = UVC_BULK_PLHEADER_SIZE;
			pHeader->bHeaderLen = UVC_BULK_PLHEADER_SIZE;
			pHeader->bBitmap = 0x82 | uvc->pl_toggle;
		}else {
			uvc->header_size = 0x0;
		}
	}
}

static int uvc_encode_data(t_libuvc_strmif *strmif, void *data, char *buffer, int length)
{
	return strmif->encode(data, buffer, length);
}

static void uvc_encode_end(t_libuvc_strmif *strmif, int length)
{
	t_libuvc_param *uvc = &(strmif->uvc);

	if((strmif->format == VS_FORMAT_MPEG2TS) || (strmif->format == VS_FORMAT_VENDOR_ENCV)) {
		if(uvc->offset == 0) {
			uvc->offset = length;
		}
		return;
	}

	uvc->offset += length;
	if(uvc->offset >= uvc->size) {
		uvc->pl_toggle = 1 - uvc->pl_toggle;
		uvc->offset = 0;
		uvc->size = 0;
		uvc->status = UVC_BUFF_STATUS_END;
		WriteReg32(UDCIF_EP_CFG1((strmif->STRM_EP) & 0xf), (ReadReg32(UDCIF_EP_CFG1((strmif->STRM_EP) & 0xf)) & ~0xf)); //disable intr
	}else {
		WriteReg32(UDCIF_EP_CFG1((strmif->STRM_EP) & 0xf), (ReadReg32(UDCIF_EP_CFG1((strmif->STRM_EP) & 0xf)) | 0x2)); //enable intr
	}

	if((strmif->ep_cfg->type & USB_ENDPOINT_XFERTYPE_MASK) == EPTYPE_BULK) {
		uvc->header_size = 0;
	}
}

void uvc_filldata(t_libuvc_strmif * strmif)
{
	t_libuvc_param *uvc = &(strmif->uvc);
	u16 pl_size, pl_cnt;
	u16 i, flag, total;
	u32 offset, length;

	if((strmif->v_status == UVC_STATUS_IDLE) || (strmif->ep_cfg == NULL)){
		return;
	}

	if(EPBUF_IS_NOTEMPTY(strmif->STRM_EP & 0xf)) { 
		return;
	}

	pl_size = strmif->ep_cfg->maxpktsize & 0x7ff;
	if(strmif->ep_cfg->maxpktsize == 0x1400) {
		//need to fix
		pl_size = 0xc00;
	}
	pl_cnt  = ((ReadReg32(UDCIF_EP_CFG0(strmif->STRM_EP & 0xf)))>>12)&0x1f;
	if(pl_cnt > 3) {
		pl_cnt = 3;
	}
	if((pl_size != 0x400) && (pl_size != 0x200)) {
		pl_cnt = 1;
	}

	if(uvc->offset == 0) {
		uvc_encode_header(strmif, uvc_tx_buf, pl_size);
	}

	flag = 0;
	total = 0;
	for(i=0; i<pl_cnt; i++) {
		length = pl_size - uvc->header_size;
		offset = uvc->offset;
		length = uvc_encode_data(strmif, &offset, uvc_tx_buf + i*pl_size + uvc->header_size, length);
		if(length & UVC_FLAG_NODATA) {
			break;
		}
		else if(length & UVC_FLAG_FRM_END) {  
			uvc->size = uvc->offset + (length&0x1fffffff);
			flag = 1;
		}

		if(length & UVC_FLAG_FRM_START) {
//			uart_putc('s');
			uvc_encode_start(strmif, offset);
		}

		length &= 0x1fffffff;
		if(length != (pl_size - uvc->header_size)) { // short packet
			flag = 1;
		}

		uvc_encode_header(strmif, uvc_tx_buf+i*pl_size, length);
		total = total + length + uvc->header_size;
		uvc_encode_end(strmif, length);
		if(flag) {
			break;
		}
	}

	if(total) {
		if(pl_size != 0xc00) {
			total = udc_writeEP(strmif->STRM_EP, uvc_tx_buf, total);
		}else {
			total = udc_writeISO(strmif->STRM_EP, uvc_tx_buf, total);
		}
		ENABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}
}
