#include "includes.h"

#define MAX_DUMMY_PKT 20
#define MIN_DUMMY_PKT 10
#define DUMMY_LSTPKT_STEP 1

u8 dummy_data, last_off;
u32 dummy_size, dummy_offset;

int dummy_encode_data(void *data, u8 *buffer, int length)
{
	int retcode;

	memset(buffer, dummy_data, length);
	dummy_data += 1;

	retcode = 0;
	if(dummy_offset == 0) {
		retcode += UVC_FLAG_FRM_START;
		*(int *)data += dummy_size*1024;
	}
	++dummy_offset;
	if(dummy_offset >= dummy_size) {
		retcode += UVC_FLAG_FRM_END+last_off;
		last_off += DUMMY_LSTPKT_STEP;
		dummy_offset = 0;
		if(++dummy_size > MAX_DUMMY_PKT) {
			dummy_size = MIN_DUMMY_PKT;
		}
	}else {
		retcode += length;
	}

	return retcode;
}

int dummy_frameset_default(u32 frame_size, u8 frame_rate)
{
	debug_printf("dummy frame set: 0x%x, %d\n", frame_size, frame_rate);

	dummy_data = 0;
	last_off = 0;
	dummy_offset = 0;
	dummy_size = MIN_DUMMY_PKT;

	return 1;
}
