#include "includes.h"

#define UMASS_CAMERA_FORMAT_YVYU 0
#define UMASS_CAMERA_FORMAT_RAW8 1

#define UCAM_DATA_LEN (SIZE_2K - UVC_YUV_PLHEADER_SIZE)

static u32 frame_toggle = 1;

extern FRAME *upanther_frm;
extern int dp_running;
extern u8 g_umass_camera_format ; //0: YVYU, 1: RAW8
void fb_get_frame(u32 * y_addr_base, u32 *y_len, u32 *uv_addr_base, u32 *uv_len);

static int yuy2_convert(u8 *buffer, u8 *ybuf, u8 *uvbuf, int y_len) 
{
	pUvcPayloadHeader pHeader;
	int i, n, k, frameticks;
	u8 *p, *s, *s1;
	int sof, offset, fb_offset;
	int count, length, size;

	offset = 0;
	fb_offset = 0;
	frameticks = ticks;
	size = y_len*2;
	count = size/UCAM_DATA_LEN;

	for(k=0; k<count+1; k++) 
	{
		p = buffer + offset;
		s = (u8 *)ybuf + fb_offset / 2;
		s1 = (u8 *)uvbuf + fb_offset / 2;
		i = 0;

		if(k < count) {
			length = UCAM_DATA_LEN;	
		}else {
			length = size - count*UCAM_DATA_LEN;	
		}

		if(fb_offset & 0xf) {
			n = 16 - (fb_offset & 0xf);
			s += n/2;
			s1 += n/2;
			s -= 8;
			s1 -= 8;
			if(n >= 12){
				p[i+0] = s[2];
				p[i+1] = s1[5];
				p[i+2] = s[3];
				p[i+3] = s1[1];
				p += 4;
				n -= 4;
			}
			if(n >= 8){
				p[i+0] = s[4];
				p[i+1] = s1[6];
				p[i+2] = s[5];
				p[i+3] = s1[2];
				p += 4;
				n -= 4;
			}
			if(n >= 4){
				p[i+0] = s[6];
				p[i+1] = s1[7];
				p[i+2] = s[7];
				p[i+3] = s1[3];
				p += 4;
				n -= 4;
			}
			s += 8;
			s1 += 8;
		}

		for(; i <length; i+=16){
			//YYYYYYYY + UUUUVVVV -> YVYUYVYUYVYUYVYU convert
			p[i+0] = s[0];
			p[i+1] = s1[4];
			p[i+2] = s[1];
			p[i+3] = s1[0];
			p[i+4] = s[2];
			p[i+5] = s1[5];
			p[i+6] = s[3];
			p[i+7] = s1[1];
			p[i+8] = s[4];
			p[i+9] = s1[6];
			p[i+10] = s[5];
			p[i+11] = s1[2];
			p[i+12] = s[6];
			p[i+13] = s1[7];
			p[i+14] = s[7];
			p[i+15] = s1[3];

			s += 8;
			s1 += 8;
		}
		offset += length; 
		fb_offset += length;

		if(k >= count) {
			pHeader = (UvcPayloadHeader *)(buffer + offset - length - UVC_YUV_PLHEADER_SIZE);
			pHeader->bBitmap |= 0x02;
			frame_toggle = 1 - frame_toggle;
			return offset;
		}

		pHeader = (UvcPayloadHeader *)(buffer + offset);
		pHeader->bHeaderLen = UVC_YUV_PLHEADER_SIZE;
		pHeader->bBitmap = 0x8c | frame_toggle;
		pHeader->dwPTS[0] = frameticks & 0xff;
		pHeader->dwPTS[1] = (frameticks >> 8) & 0xff;
		pHeader->dwPTS[2] = (frameticks >> 16) & 0xff;
		pHeader->dwPTS[3] = (frameticks >> 24) & 0xff;
		pHeader->dwSCR[0] = ticks & 0xff;
		pHeader->dwSCR[1] = (ticks >> 8) & 0xff;
		pHeader->dwSCR[2] = (ticks >> 16) & 0xff;
		pHeader->dwSCR[3] = (ticks >> 24) & 0xff;

		sof = (ReadReg32(UDCIF_UDC_STA)>>16) & 0x7ff;
		pHeader->wSOF[0] = sof & 0xff;
		pHeader->wSOF[1] = (sof >> 8) ;

		offset += UVC_YUV_PLHEADER_SIZE;
	}

	return offset;
}

static int raw_convert(u8 *buffer, u8 *ybuf, int y_len) 
{
	pUvcPayloadHeader pHeader;
	int k, frameticks;
	u8 *d, *s;
	int sof, offset, fb_offset;
	int count, length, size;

	offset = 0;
	fb_offset = 0;
	frameticks = ticks;
	size = y_len;
	count = size/UCAM_DATA_LEN;

	for(k=0; k<count+1; k++) 
	{
		if(k < count) {
			length = UCAM_DATA_LEN;	
		}else {
			length = size - count*UCAM_DATA_LEN;	
		}

		d = (u8 *)buffer + offset;
		s = (u8 *)ybuf + fb_offset;
		memcpy(d, s, length);
		offset += length;
		fb_offset += length;
		if(k >= count) {
			pHeader = (UvcPayloadHeader *)(buffer + offset - length -UVC_YUV_PLHEADER_SIZE);
			pHeader->bBitmap |= 0x02;
			frame_toggle = 1 - frame_toggle;
			return offset;
		}

		pHeader = (UvcPayloadHeader *)(buffer + offset);
		pHeader->bHeaderLen = UVC_YUV_PLHEADER_SIZE;
		pHeader->bBitmap = 0x8c | frame_toggle;
		pHeader->dwPTS[0] = frameticks & 0xff;
		pHeader->dwPTS[1] = (frameticks >> 8) & 0xff;
		pHeader->dwPTS[2] = (frameticks >> 16) & 0xff;
		pHeader->dwPTS[3] = (frameticks >> 24) & 0xff;
		pHeader->dwSCR[0] = ticks & 0xff;
		pHeader->dwSCR[1] = (ticks >> 8) & 0xff;
		pHeader->dwSCR[2] = (ticks >> 16) & 0xff;
		pHeader->dwSCR[3] = (ticks >> 24) & 0xff;

		sof = (ReadReg32(UDCIF_UDC_STA)>>16) & 0x7ff;
		pHeader->wSOF[0] = sof & 0xff;
		pHeader->wSOF[1] = (sof >> 8) ;

		offset += UVC_YUV_PLHEADER_SIZE;
	}
	return offset;
}

int upanther_camera_convert(u8 *buffer)
{
	u32 y_base = 0, y_len;
	u32 uv_base = 0, uv_len;
	u32 length, timeout;

	if(!dp_running){
		return 0;
	}

	y_len = 0;
	timeout = 20;
	do {
		fb_get_frame(&y_base, &y_len, &uv_base, &uv_len);//get new image frame
		if(y_len == 0){
			debug_printf("fb_get_frame error\n");
			ertos_timedelay(5);
			continue;
		}
		break;
	}while(--timeout);
	
	if(timeout == 0) {
		return 0;
	}

	if(g_umass_camera_format == UMASS_CAMERA_FORMAT_RAW8){
		length = raw_convert(buffer, y_base & (~BIT31), y_len);
	}else {
		length = yuy2_convert(buffer, y_base & (~BIT31), uv_base & (~BIT31), y_len);
	}
	dc_flush_all();
	dc_invalidate_all();

	if(upanther_frm != NULL) {
		if(g_umass_camera_format == UMASS_CAMERA_FORMAT_RAW8){
			libvs_remove_frame(0, upanther_frm);//free frame
		}else{
			libvs_remove_frame(1, upanther_frm);//free frame
		}
		upanther_frm = NULL;
	}

	return length;
}
