/*----------------------------------------------------------------------------
 *	  U S B  -  K e r n e l
 *----------------------------------------------------------------------------
 *	  Name:	umass_nulldisk.c
 *	  Purpose: Mass Storage Class Custom User Module for NULL 
 *	  Version: V1.10
 *---------------------------------------------------------------------------*/
#include "includes.h"

#define RAMDISK_BASE_ADDR  (0x10100000)
#define RAMDISK_SIZE  0x10000

extern t_lundriver_cfg g_nulldisk_lundriver;

static int nulldisk_rd(u32 arg, u32 off, int buf, u32 len) 
{
	memcpy((u8 *)buf, (u8 *)(arg + (off<<9)), len);
	return 0;
}

static int nulldisk_wr(u32 arg, u32 off, int buf, u32 len)
{   
	memcpy((u8 *)(arg + (off<<9)), (u8 *)buf, len);
	return 0;
}

static int msc_nulldisk_init (void) 
{
	debug_printf("++msc_nulldisk_init\n");

	g_nulldisk_lundriver.lun_param = UMASS_LUN_REMOVABLE;
	g_nulldisk_lundriver.block_cnt = (RAMDISK_SIZE>>9);
	g_nulldisk_lundriver.block_size = 0x200;
	g_nulldisk_lundriver.size = g_nulldisk_lundriver.block_cnt*0x200;
	g_nulldisk_lundriver.lun_arg = RAMDISK_BASE_ADDR;

	debug_printf("Null disk size: 0K, sector: 0x%x, sectors: 0\n", g_nulldisk_lundriver.block_size);

	return 0;
}

t_lundriver_cfg g_nulldisk_lundriver = { 
	.product_info = "null disk",
	.lun_id = UMASS_ID_NULL,
	.init = msc_nulldisk_init,
	.msc_read = nulldisk_rd,
	.msc_write = nulldisk_wr,
};
