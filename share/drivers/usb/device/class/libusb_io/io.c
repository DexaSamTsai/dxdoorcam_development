#include "includes.h"

#define UIO_BULK_IN_ENDPOINT    0x85
#define UIO_BULK_OUT_ENDPOINT   0x6
#define UIO_INTR_IN_ENDPOINT    0x83
#define UIO_INTR_OUT_ENDPOINT   0x4
#define UIO_ISO_IN_ENDPOINT    0x81
#define UIO_ISO_OUT_ENDPOINT   0x2

/******************************************************
 * USB class configuration which should match Descriptor
******************************************************/
t_libusbep_cfg libusbep_cfg_hs[] = {
	{
		.ep = UIO_BULK_IN_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = HS_MAX_PKTSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x0,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_BULK_OUT_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = HS_MAX_PKTSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x2,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_INTR_IN_ENDPOINT,
		.type = EPTYPE_INTR,
		.maxpktsize = HS_MAX_PKTSIZE_INTR,
		.intr_th = 0x2,
		.baseaddr = 0x4,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_INTR_OUT_ENDPOINT,
		.type = EPTYPE_INTR,
		.maxpktsize = HS_MAX_PKTSIZE_INTR,
		.intr_th = 0x2,
		.baseaddr = 0x8,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_ISO_IN_ENDPOINT,
		.type = EPTYPE_ISO,
		.maxpktsize = HS_MAX_PKTSIZE_ISO,
		.intr_th = 0x2,
		.baseaddr = 0xc,
		.buffernum = 0x1,
	},
	{
		.ep = UIO_ISO_OUT_ENDPOINT,
		.type = EPTYPE_ISO,
		.maxpktsize = HS_MAX_PKTSIZE_ISO,
		.intr_th = 0x2,
		//.baseaddr = 0xe,
		//.buffernum = 0x3,
		.baseaddr = 0x12,
		.buffernum = 0x1,
	}
};

t_libusbep_cfg libusbep_cfg_fs[] = {
	{
		.ep = UIO_BULK_IN_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = FS_MAX_PKTSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x0,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_BULK_OUT_ENDPOINT,
		.type = EPTYPE_BULK,
		.maxpktsize = FS_MAX_PKTSIZE_BULK,
		.intr_th = 0x2,
		.baseaddr = 0x2,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_INTR_IN_ENDPOINT,
		.type = EPTYPE_INTR,
		.maxpktsize = FS_MAX_PKTSIZE_INTR,
		.intr_th = 0x2,
		.baseaddr = 0x4,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_INTR_OUT_ENDPOINT,
		.type = EPTYPE_INTR,
		.maxpktsize = FS_MAX_PKTSIZE_INTR,
		.intr_th = 0x2,
		.baseaddr = 0x6,
		.buffernum = 0x2,
	},
	{
		.ep = UIO_ISO_IN_ENDPOINT,
		.type = EPTYPE_ISO,
		.maxpktsize = FS_MAX_PKTSIZE_ISO,
		.intr_th = 0x2,
		.baseaddr = 0x8,
		.buffernum = 0x1,
	},
	{
		.ep = UIO_ISO_OUT_ENDPOINT,
		.type = EPTYPE_ISO,
		.maxpktsize = FS_MAX_PKTSIZE_ISO,
		.intr_th = 0x2,
		.baseaddr = 0x9,
		.buffernum = 0x3,
	},
};

t_libusbclass_cfg g_libusbclass_cfg_hs =
{
	.epcfg = &libusbep_cfg_hs[0],
	.epnum = sizeof(libusbep_cfg_hs)/sizeof(libusbep_cfg_hs[0]),
	.arg = NULL,
	.handleEP = NULL,
};

t_libusbclass_cfg g_libusbclass_cfg_fs =
{
	.epcfg = &libusbep_cfg_fs[0],
	.epnum = sizeof(libusbep_cfg_fs)/sizeof(libusbep_cfg_fs[0]),
	.arg = NULL,
	.handleEP = NULL,
};

// configure descriptor: high speed
u8 cfgConf_HI[] __attribute__((aligned(4))) =
{
    0x09, 0x02, 0x3c, 0x00, 0x01, 0x01, 0x00, 0x80, 0x64,
	0x09, 0x04, 0x00, 0x00, 0x06, 0xff, 0x00, 0x00, 0x00,
	0x07, 0x05, UIO_BULK_IN_ENDPOINT, 0x02, (HS_MAX_PKTSIZE_BULK&0xff), (HS_MAX_PKTSIZE_BULK>>8)&0xff, 0x00,
	0x07, 0x05, UIO_BULK_OUT_ENDPOINT, 0x02, (HS_MAX_PKTSIZE_BULK&0xff), (HS_MAX_PKTSIZE_BULK>>8)&0xff, 0x00,
	0x07, 0x05, UIO_INTR_IN_ENDPOINT, 0x03, (HS_MAX_PKTSIZE_INTR&0xff), (HS_MAX_PKTSIZE_INTR>>8)&0xff, 0x4,
	0x07, 0x05, UIO_INTR_OUT_ENDPOINT, 0x03, (HS_MAX_PKTSIZE_INTR&0xff), (HS_MAX_PKTSIZE_INTR>>8)&0xff, 0x4,
	//0x07, 0x05, UIO_ISO_IN_ENDPOINT, 0x0d, (HS_MAX_PKTSIZE_ISO&0xff), (HS_MAX_PKTSIZE_ISO>>8)&0xff, 0x04,
	//0x07, 0x05, UIO_ISO_OUT_ENDPOINT, 0x0d, (HS_MAX_PKTSIZE_ISO&0xff), (HS_MAX_PKTSIZE_ISO>>8)&0xff, 0x04,
	0x07, 0x05, UIO_ISO_IN_ENDPOINT, 0x01, 0x00, 0x14, 0x02,
	0x07, 0x05, UIO_ISO_OUT_ENDPOINT, 0x01, 0x00, 0x14, 0x02,
};

// configure descriptor: full speed
u8 cfgConf_FL[] __attribute__((aligned(4))) =
{
    0x09, 0x02, 0x3c, 0x00, 0x01, 0x01, 0x00, 0x80, 0x64,
	0x09, 0x04, 0x00, 0x00, 0x06, 0xff, 0x00, 0x00, 0x00,
	0x07, 0x05, UIO_BULK_IN_ENDPOINT, 0x02, (FS_MAX_PKTSIZE_BULK&0xff), (FS_MAX_PKTSIZE_BULK>>8)&0xff, 0x00,
	0x07, 0x05, UIO_BULK_OUT_ENDPOINT, 0x02, (FS_MAX_PKTSIZE_BULK&0xff), (FS_MAX_PKTSIZE_BULK>>8)&0xff, 0x00,
	0x07, 0x05, UIO_INTR_IN_ENDPOINT, 0x03, (FS_MAX_PKTSIZE_INTR&0xff), (FS_MAX_PKTSIZE_INTR>>8)&0xff, 0x4,
	0x07, 0x05, UIO_INTR_OUT_ENDPOINT, 0x03, (FS_MAX_PKTSIZE_INTR&0xff), (FS_MAX_PKTSIZE_INTR>>8)&0xff, 0x4,
	0x07, 0x05, UIO_ISO_IN_ENDPOINT, 0x0d, (FS_MAX_PKTSIZE_ISO&0xff), (FS_MAX_PKTSIZE_ISO>>8)&0xff, 0x01,
	0x07, 0x05, UIO_ISO_OUT_ENDPOINT, 0x0d, (FS_MAX_PKTSIZE_ISO&0xff), (FS_MAX_PKTSIZE_ISO>>8)&0xff, 0x01,
};

/* Notice: g_csrdata is not modified */
t_csrdata g_csrdata[] = {
	//-------------------------------------------------------------------------------
	{0x00000000, 0x40002000, 0x0000000c },	//[31:16] device descriptor addr Pointer
											 //[15: 0] setup command addr pointer
	//-------------------------------------------------------------------------------
	{0x00000001, 0x50000000, 0x0000000c }, //[31:16] qualifier descriptor address pointer
											//[15: 0] reserved
	//-------------------------------------------------------------------------------
	{0x00000002, 0x02000080, 0x0000000c }, //[31:30] reserved for non-iso in endpoint
											//[29:19] maxpktsize
											//[18:15] alterate setting to which this endpoint belongs
											//[14:11] interface number to which this endpoint belongs
											//[10: 7] configuration number to which this endpoint belongs 
											//[ 6: 5] endpoint type;00 ctrl,01 iso,10 bulk,11 interrupt
											//[	4] endpoint direction 0:out, 1 in
											//[ 3: 0] endpoint number
	//-------------------------------------------------------------------------------
	{0x00030003, 0x00000000, 0x0000000c }, //dummy ep 

	//-------------------------------------------------------------------------------
	{0x00030004, 0x000000d0 | (UIO_BULK_IN_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_BULK<<19), 0x0000000c }, //bulk in 512

	//FIXME-------------------------------------------------------------------------------
	{0x00030005, 0x000000c0 | (UIO_BULK_OUT_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_BULK<<19), 0x0000000c }, //bulk out 512

	//-------------------------------------------------------------------------------
	{0x00030006, 0x000000f0 | (UIO_INTR_IN_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_INTR<<19), 0x0000000c }, //intr in 512

	//FIXME-------------------------------------------------------------------------------
	{0x00030007, 0x000000e0 | (UIO_INTR_OUT_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_INTR<<19), 0x0000000c }, //intr out 512

	//-------------------------------------------------------------------------------
	{0x00030008, 0x200000b0 | (UIO_ISO_IN_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_ISO<<19), 0x0000000c }, //iso in 512

	//FIXME-------------------------------------------------------------------------------
	{0x00030009, 0x200000a0 | (UIO_ISO_OUT_ENDPOINT & 0xf) | (HS_MAX_PKTSIZE_ISO<<19), 0x0000000c }, //iso out 512
	
	//-------------------------------------------------------------------------------
	{0x00000000, 0x00000000, 0x00000030 } //end flag
};

// device descriptor: high speed
u32 cfgDevice_HI[] = {
  //0x12, 0x01, 0x00, 0x02, 0xef, 0x02, 0x01, 0x40, 0xa9, 0x05, 0x39, 0x05, 0x00, 0x01, 0x01, 0x02, 0x00, 0x01
  0x02000112, 0x40000000, 0xf78805a9, 0x03010100, 0x0100
};

// device descriptor: full speed
u32 cfgDevice_FL[] = {
  //0x12, 0x01, 0x10, 0x01, 0xef, 0x02, 0x01, 0x08, 0xa9, 0x05, 0x39, 0x05, 0x00, 0x01, 0x01, 0x02, 0x00, 0x01
  0x01100112, 0x40000000, 0xf78805a9, 0x03010100, 0x0100
};

// string 0
u32 cfgStr0[] = {
   //0x04, 0x03, 0x09, 0x04
   0x04090304
};

// string 1
u32 cfgStr1 [] = {
   	// XXXXXXXXXX Technologies, Inc.
   	//0x3c, 0x03, 0x4f, 0x00, 0x6d, 0x00, 0x6e, 0x00,
	//0x69, 0x00, 0x56, 0x00, 0x69, 0x00, 0x73, 0x00,
	//0x69, 0x00, 0x6f, 0x00, 0x6e, 0x00, 0x20, 0x00,
	//0x54, 0x00, 0x65, 0x00, 0x63, 0x00, 0x68, 0x00,
	//0x6e, 0x00, 0x6f, 0x00, 0x6c, 0x00, 0x6f, 0x00,
	//0x67, 0x00, 0x69, 0x00, 0x65, 0x00, 0x73, 0x00,
	//0x2c, 0x00, 0x20, 0x00, 0x49, 0x00, 0x6e, 0x00,
	//0x63, 0x00, 0x2e, 0x00
	0x0058033c, 0x00580058, 0x00580058, 0x00580058,
    0x00580058, 0x00200058, 0x00650054, 0x00680063,
    0x006f006e, 0x006f006c, 0x00690067, 0x00730065,
    0x0020002c, 0x006e0049, 0x002e0063
};

// string 2 
u32 cfgStr2 [] = {
   //USB Mass IO 
   //0x24, 0x03, 0x55, 0x00, 0x53, 0x00, 0x42, 0x00,
   //0x20, 0x00, 0x43, 0x00, 0x61, 0x00, 0x6d, 0x00,
   //0x65, 0x00, 0x72, 0x00, 0x61, 0x00, 0x5f, 0x00,
   //0x4f, 0x00, 0x56, 0x00, 0x30, 0x00, 0x37, 0x00,
   //0x38, 0x00, 0x30, 0x00,
   0x00550318, 0x00420053, 0x004d0020, 0x00730061,
   0x00200073, 0x004f0049, 0x00200020, 
};

// string 3
u32 cfgStr3 [] = {
   //USB IO TEST 
   0x00550318, 0x00420053, 0x00490020, 0x0020004f,
   0x00450054, 0x00540053
};

u32 cfgQualifier[] = {
   //0x0a, 0x06, 0x00, 0x02, 0xef, 0x02, 0x01, 0x08,
   //0x01, 0x00
   0x0200060a, 0x080102ef, 0x0001
};

u32 cfgConf_Other[] = {
   //0x09, 0x07, 0x27, 0x00, 0x01, 0x01, 0x00, 0x80, 0xfa,   // configuration: 1 interfaces
   //0x09, 0x04, 0x00, 0x00, 0x03, 0xff, 0x00, 0x00, 0x00,   // vendor spec: 3 endpoints
   //0x07, 0x05, 0x81, 0x02, 0x40, 0x00, 0x00,         // endpoint 1: bulk in; 64 bytes
   //0x07, 0x05, 0x02, 0x02, 0x40, 0x00, 0x00,         // endpoint 2: bulk out; 64 bytes
   //0x07, 0x05, 0x83, 0x03, 0x08, 0x00, 0x0a,         // endpoint 3: intr in; 8 bytes; 10 frames intv
   0x00270709, 0x80000101, 0x000409fa, 0x00ff0300,
   0x05070000, 0x00400281, 0x02050700, 0x00004002,
   0x03830507, 0x0a0008
};

void usbclass_HandleSetif(int if_id, int alt)
{

}

int usbclass_HandleRequest(void)
{
	return 0;
}

/* this func will be called before USB start, for init some class-specific data */
int usbclass_init(int first)
{
	return 0;
}

void usbclass_clean(void)
{

}
