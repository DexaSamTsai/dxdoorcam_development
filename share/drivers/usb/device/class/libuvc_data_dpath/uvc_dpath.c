#include "includes.h"
#define GET_FRAME_TIMEOUT 10

typedef struct{
	u32 length;
	u32 offset;
	u32 left_size0;
	u32 left_size1;
	FRAME *vfrm;
}t_venc_info;

static t_venc_info venc_info;

static FRAME* venc_payload_init()
{
	u32 timeout;

	if(venc_info.vfrm) {
		debug_printf("find a lost frame: %x\n", venc_info.vfrm);
		libvs_remove_frame(0, venc_info.vfrm);
		venc_info.vfrm = NULL;
	}

	timeout = 0;
	do{
		venc_info.vfrm = libvs_get_frame(0, 100);
		if(venc_info.vfrm == NULL) {
			debug_printf("data_path get data failed: %d\n", timeout);
			if(++timeout > GET_FRAME_TIMEOUT) {
				return NULL;
			}else {
				ertos_timedelay(5);
				continue;
			}
		}
		break;
	}while(1);

	venc_info.offset = 0;
	venc_info.length = venc_info.vfrm->size + venc_info.vfrm->size1;
	venc_info.left_size0 = venc_info.vfrm->size;
	venc_info.left_size1 = venc_info.vfrm->size1;
	//debug_printf("venc size: %x\n", venc_info.length);

	return venc_info.vfrm;
}

static u32 venc_payload_get(FRAME *vfrm, u8 *buffer, int length)
{
	u32 size, left, off;

	off = 0;
	if(venc_info.left_size0 > 0){
		size = (venc_info.left_size0 > length) ? length : venc_info.left_size0;
		memcpy(buffer, (u8 *)(vfrm->addr+(vfrm->size - venc_info.left_size0)), size);
		venc_info.left_size0 -= size;
		left = length - size;
		off = size;
	}else {
		left = length;
		off = 0;
	}

	if(left && (venc_info.left_size1 > 0)){
		size = (venc_info.left_size1 > left) ? left : venc_info.left_size1;
		memcpy(buffer+off, (u8 *)(vfrm->addr1+(vfrm->size1 - venc_info.left_size1)), size);
		venc_info.left_size1 -= size;
		left = left - size;
		off += size;
	}

	return off;
}

static u32 _first_frameset = 0;
#ifdef CONFIG_DSIF_EN
#include "dsif_uvc_dpath.c"
#else
int dpath_encode_data(void *data, u8 *buffer, int length)
{
	int copy_size, offset;
	u32 retcode;

	retcode = 0;
	offset = *(int *)data;
	if(offset == 0) {
		//lowlevel_putc('+');
		if(venc_payload_init() == NULL) {
			return UVC_FLAG_NODATA;
		}
		//debug_printf("img size: %x, %x\n", venc_info.length, venc_info.vfrm->addr);
		*(int *)data = venc_info.length;
		retcode = UVC_FLAG_FRM_START;
	}

	copy_size = venc_payload_get(venc_info.vfrm, buffer, length);
	retcode += copy_size;

	if(offset + copy_size >= venc_info.length) {
		retcode |= UVC_FLAG_FRM_END;
		//lowlevel_putc('-');
		libvs_remove_frame(0, venc_info.vfrm);
		venc_info.vfrm = NULL;
	}

	return retcode;
}

int dpath_frameset_default(u32 frame_size, u8 frame_rate)
{
	int ret;
	
	if((ReadReg32(REG_SC_ADDR+0x60)&BIT0)==0){
		while(libvs_stop(0) == 1); //
		libvs_exit(0);
	}

	board_ioctl(BIOC_LOAD_DCPC_LUT, frame_size, NULL);

	debug_printf("data path(SW) frame set: %x, %d\n", frame_size, frame_rate);
	
	if(_first_frameset == 0){
		ret = libdatapath_init(0);
		ret = libdatapath_start();
		debug_printf("datapath start: %d\n", ret);
	
		//0: ve, 1: mimg
		libisp_init(); // added - prevent green color
		_first_frameset = 1;
	}

	if( frame_size != 0 )
	{
        ret = libvs_init(0);
        libvs_set_framerate(0, frame_rate);
        libvs_set_resolution(0, frame_size);
        ret += libvs_start(0);
        debug_printf("libvs start: %d\n", ret);
	}
	return 1;
}
#endif
