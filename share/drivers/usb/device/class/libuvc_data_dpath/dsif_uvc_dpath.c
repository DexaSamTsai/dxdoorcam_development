
/*
copy to \share\drivers\usb\device\class\libuvc_data_dpath

add in uvc_dpath.c
#include "dsif_uvc_dpath.c"

rename in uvc_dpath.c
dpath_encode_data->dpath_encode_data2
dpath_frameset_default->dpath_frameset_default2

*/
extern int dsif_venc_payload_init(FRAME** vfrm);
int dpath_encode_data(void *data, u8 *buffer, int length)
{
	int copy_size, offset;
	u32 retcode;

	retcode = 0;
	offset = *(int *)data;
	if(offset == 0) {
		if(dsif_venc_payload_init(&venc_info.vfrm) == 0) {
			return UVC_FLAG_NODATA;
		}
		venc_info.offset=0;
		venc_info.length = venc_info.vfrm->size + venc_info.vfrm->size1;
		venc_info.left_size0 = venc_info.vfrm->size;
		venc_info.left_size1 = venc_info.vfrm->size1;
		
		*(int *)data = venc_info.length;
		retcode = UVC_FLAG_FRM_START;
	}

	copy_size = venc_payload_get(venc_info.vfrm, buffer, length);
	retcode += copy_size;

	if(offset + copy_size >= venc_info.length) {
		retcode |= UVC_FLAG_FRM_END;
		if(venc_info.vfrm->addr)
		{
			dsif_free((u8*)venc_info.vfrm->addr);
			venc_info.vfrm->addr=0;
		}
		if(venc_info.vfrm)
		{
			dsif_free((u8*)venc_info.vfrm);
			venc_info.vfrm = NULL;
		}
	}

	return retcode;
}

int dpath_frameset_default(u32 frame_size, u8 frame_rate)
{
	if((ReadReg32(REG_SC_ADDR+0x60)&BIT0)==0){
		//while(libvs_stop(0) == 1); //
		//libvs_exit(0);
		debug_printf("%s\n",__FUNCTION__);
	}
	debug_printf("data path(SW) UVC\n");
	return 1;
}

