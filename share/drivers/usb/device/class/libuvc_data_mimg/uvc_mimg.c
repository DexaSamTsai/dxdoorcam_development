#include "includes.h"

int mimg_encode_data(void *data, u8 *buffer, int length)
{
	//TODO: nothing
	return UVC_FLAG_NODATA;
}

int mimg_frameset_default(u32 frame_size, u8 frame_rate)
{
	int ret;

	debug_printf("MIMG(HW) frame set: %x, %d\n", frame_size, frame_rate);

	libvs_stop(0);
	libvs_exit(0);
	libdatapath_stop();
	WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) & ~BIT_VIDEO_EN);

	//usb ep config
	WriteReg32(UDCIF_VP_CSR, 0x200088b2);
	WriteReg32(UDCIF_ISOEP_ADDR, 0x04050607);
	WriteReg32(UDCIF_CSR_CTRL, ReadReg32(UDCIF_CSR_CTRL)|BIT5|BIT12);

	ret = libdatapath_init(0);
	debug_printf("datapath 0 init: %d\n", ret);
	ret = libdatapath_start();
	debug_printf("datapath start: %d\n", ret);
	ret = libisp_init();
	debug_printf("isp_init: %d\n", ret);

	ret = libvs_init(0);
	ret = libvs_start(0);
	debug_printf("vs start: %d\n", ret);

	WriteReg32(REG_IMG_BASE + 0x40, ReadReg32(REG_IMG_BASE + 0x40) | BIT31);
	ENABLE_UVC_DP_MIMG;

	// endian input swap
	WriteReg32(UDCIF_VIDEO_CFG, ReadReg32(UDCIF_VIDEO_CFG) | BIT_VIDEO_EN);

	return 1;
}
