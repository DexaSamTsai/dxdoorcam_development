#include "includes.h"

typedef struct{
	u32 length;
	u32 offset;
	u32 left_size0;
	u32 left_size1;
	FRAME *afrm;
	u32 sample_rate;
}t_aenc_info;

static t_aenc_info aenc_info;

#define GET_FRAME_TIMEOUT 500 

char aenc_buffer[1024];

static FRAME* aenc_payload_init()
{
	u32 timeout;

	if(aenc_info.afrm == NULL) 
	{
		timeout = 0;
		do{
			aenc_info.afrm = libas_get_frame(0,0);
			if(aenc_info.afrm == NULL){
				if(++timeout < GET_FRAME_TIMEOUT) {
					{volatile int d; for(d=0; d<500;d++);}
					continue;
				}
				else {
					debug_printf("get audio frame failed\n");
					return NULL;
				}
			}
			break;
		}while(1);

		aenc_info.offset = 0;
		aenc_info.length = aenc_info.afrm->size + aenc_info.afrm->size1;
		aenc_info.left_size0 = aenc_info.afrm->size;
		aenc_info.left_size1 = aenc_info.afrm->size1;
		return aenc_info.afrm;
	}

	return aenc_info.afrm;
}

static u32 aenc_payload_get(FRAME *afrm, u8 *buffer, int length)
{
	u32 size, left, off;

	off = 0;
	if(aenc_info.left_size0 > 0){
		size = (aenc_info.left_size0 > length) ? length : aenc_info.left_size0;
		memcpy(buffer, (u8 *)(afrm->addr+(afrm->size - aenc_info.left_size0)), size);
		aenc_info.left_size0 -= size;
		left = length - size;
		off = size;
	}else {
		left = length;
		off = 0;
	}

	if(left && (aenc_info.left_size1 > 0)){
		size = (aenc_info.left_size1 > left) ? left : aenc_info.left_size1;
		memcpy(buffer+off, (u8 *)(afrm->addr1+(afrm->size1 - aenc_info.left_size1)), size);
		aenc_info.left_size1 -= size;
		off += size;
	}

	return off;
}


void uac_pcm_filldata(t_libuac_strmif *strmif)
{
	u32 len;

	if(strmif->a_status == UVC_STATUS_IDLE){
		return;
	}

	if(EPBUF_IS_NOTEMPTY(strmif->STRM_EP & EPNUM_MASK)) {
		return;
	}

	if(aenc_payload_init()) {
		len = aenc_payload_get(aenc_info.afrm, aenc_buffer, 128);
		len = udc_writeEP(strmif->STRM_EP, aenc_buffer, len);
		aenc_info.offset += len;

		if(aenc_info.offset >= aenc_info.length) {
			libas_remove_frame(0, aenc_info.afrm);
			aenc_info.afrm = NULL;
		}
		ENABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}
}

void uac_epcm_HandleSetif(t_libuac_strmif *strmif, int alt)
{
	if(alt){
	    debug_printf("start enc audio !!! %d\n", alt);	
		strmif->a_status = UVC_STATUS_RUN;
		ENABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}else{
	    debug_printf("stop enc audio !!! %d\n", alt);
		strmif->a_status = UVC_STATUS_IDLE;
		DISABLE_EPINTR(strmif->STRM_EP & 0xf); 
	}
}

int uac_epcm_HandleSetSampleRate(t_libuac_strmif *strmif, u32 asample_rate)
{
	debug_printf("audio sample rate: %d\n", asample_rate);
	libaenc_set_sr(asample_rate);
	aenc_info.sample_rate = asample_rate; 
	return 0;
}

u32 uac_epcm_HandleGetSampleRate(void)
{
	return aenc_info.sample_rate; 
}

u32 uac_epcm_HandleFu(u32 id, u32 type, int value)
{
	s32 volume_level;

	switch (id){
        case AUDIO_CTRL_VOLUME:
		    if(type == UAV_SET_CUR){
                volume_level = 10*((short)value - (short)UAC_VOLUME_MIN)/((short)UAC_VOLUME_MAX - (short)UAC_VOLUME_MIN);
				libacodec_set_adcvol(volume_level);
                debug_printf("uac_fu_handler(voulme)---set:%d\n", volume_level);
            }
            break;
        default:
            debug_printf("others uac_fu_handler:%x, %x\n", id, value);
            break;
    }
    return 0;
}
