/*----------------------------------------------------------------------------
 *	  U S B  -  K e r n e l
 *----------------------------------------------------------------------------
 *	  Name:	umass.c
 *	  Purpose: Mass Storage Class Custom User Module for scif
 *	  Version: V1.10
 *---------------------------------------------------------------------------*/
#include "includes.h"

extern struct t_scif_drv sh_scif_drv; //defined in libfs_scif
extern __lundriver_cfg t_lundriver_cfg g_scif_lundriver;

static int scif_rd(u32 arg, u32 off, int buf, u32 len) 
{
	 return scif_read((t_scif_drv *)arg, off, buf, 0x200, (len>>9)); 
}

static int scif_wr(u32 arg, u32 off, int buf, u32 len)
{   
	 return scif_write((t_scif_drv *)arg, buf, off, len); 
}

static int msc_scif_init (void) 
{
	u32 ret = 0;

	debug_printf("++msc_scif_init\n");
	if(g_scif_lundriver.lun_private == SCIF_MODULE_01){
		ret = libscif_init(SCIF_MODULE_01);
	}else{
		ret = libscif_init(SCIF_MODULE_02);
	}
	
	debug_printf("init SCIF %d\n", ret);
	if(ret < 512){
		return -1;
	}

	g_scif_lundriver.lun_param = UMASS_LUN_REMOVABLE;
	g_scif_lundriver.block_cnt = ret;
	g_scif_lundriver.block_size = 0x200;
	g_scif_lundriver.size = ret*0x200;
	g_scif_lundriver.lun_arg = (u32)&sh_scif_drv;

	debug_printf("SCIF size: %d M, sector: 0x%x, sectors: 0x%x\n",(g_scif_lundriver.size)>>20,
	  										g_scif_lundriver.block_size, g_scif_lundriver.block_cnt);

	return 0;
}

//__lundriver_cfg t_lundriver_cfg g_scif_lundriver = { 
t_lundriver_cfg g_scif_lundriver = { 
	.product_info = "scif disk",
	.lun_id = UMASS_ID_SCIF,
	.lun_private = SCIF_MODULE_01,
	.init = msc_scif_init,
	.msc_read = scif_rd,
	.msc_write = scif_wr,
};
