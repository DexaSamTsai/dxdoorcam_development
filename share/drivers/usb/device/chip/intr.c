#include "includes.h"

static int udev_ctrlsetup_irq_handler(void * arg)
{
	// disable status intr
	WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)&~BIT16);

	lowlevel_putc('3');
	libusb_chip_isr |= BIT3;
	ertos_event_signal(evt_udc);

	return 0;
}

static int udev_ctrldata_irq_handler(void * arg)
{
	lowlevel_putc('9');
	libusb_chip_isr |= BIT9;
	ertos_event_signal(evt_udc);

	return 0;
}

static int udev_ctrlsts_irq_handler(void * arg)
{
	lowlevel_putc('1');
	lowlevel_putc('7');
	udcif_request_sendack(SEND_ACK_NEXTSTATUS, 1); 

	return 0;
}

static int udev_ep_irq_handler(void * arg)
{
	int i;

	//lowlevel_putc('1');
	//lowlevel_putc('9');
	libep_irq |= ReadReg32(UDCIF_P_IRQ) & 0x3f;
	WriteReg32(UDCIF_P_IRQ, libep_irq);
	for(i=1; i<7; i++) {
		if(libep_irq & MASK_OF_EPINTR(i))  {
			DISABLE_EPINTR(i);
		}
	}
	libusb_chip_isr |= BIT19;
	ertos_event_signal(evt_udc);

	return 0;
}

static int udev_sta_irq_handler(void * arg)
{
	lowlevel_putc('2');
	lowlevel_putc('0');
	libudc_irq |= ReadReg32(UDCIF_UDC_IRQ) & (BIT2|BIT3|BIT23|BIT24|BIT26) ;
	WriteReg32(UDCIF_UDC_IRQ, BIT2|BIT3|BIT23|BIT24|BIT26);
	libusb_chip_isr |= BIT20;
	ertos_event_signal(evt_udc);

	return 0;
}

static int udev_resume_irq_handler(void * arg)
{
	debug_printf("++resume\n");
	WriteReg32(REG_SC_ALG, ReadReg32(REG_SC_ALG)|BIT20);
	WriteReg32(REG_SC_ADDR+0x98, ReadReg32(REG_SC_ADDR+0x98)|BIT10);
	WriteReg32(REG_INT_BASE+0x0, ReadReg32(REG_INT_BASE+0x0)|BIT1); //disable resume
	WriteReg32(REG_INT_BASE+0x4, BIT1);

	return 0;
}

static int udev_suspend_irq_handler(void * arg)
{
#if 0
	//TODO: suspend/resume test
	debug_printf("++suspend\n");
	if(g_libusb_cfg.status == UDEVSTS_CONFIG){
		debug_printf("++ low power\n");
		WriteReg32(REG_SC_ALG, ReadReg32(REG_SC_ALG)&(~BIT20));
		WriteReg32(REG_SC_ADDR+0x98, ReadReg32(REG_SC_ADDR+0x98)&(~BIT10));
		WriteReg32(REG_INT_BASE+0x4, BIT1);
		WriteReg32(REG_INT_BASE+0x0, ReadReg32(REG_INT_BASE+0x0)&(~BIT1)); //enable resume
		WriteReg32(REG_INT_BASE+0x4, BIT2);
	}
#endif
	static int udc_val;

	lowlevel_putc('7');
	if(udc_val != ReadReg32(UDCIF_UDC_STA)){
		udc_val = ReadReg32(UDCIF_UDC_STA);
		debug_printf("++suspend: %x\n", udc_val);
		debug_printf("ctrl: %x %x\n", ReadReg32(UDCIF_CTRL_CFG), ReadReg32(UDCIF_CTRL_CTRL));
	}

	return 0;
}

int udcif_interrupt_handler_init(void)
{
	irq_request(IRQ_BIT_USBSETUP, udev_ctrlsetup_irq_handler, "UDEV.setup", NULL);
	irq_request(IRQ_BIT_USB, udev_ctrldata_irq_handler, "UDEV.data", NULL);
	irq_request(IRQ_BIT_USBSTA, udev_ctrlsts_irq_handler, "UDEV.sts", NULL);
	irq_request(IRQ_BIT_USBUDCEP, udev_ep_irq_handler, "UDEV.ep", NULL);
	irq_request(IRQ_BIT_USBUDC, udev_sta_irq_handler, "UDEV.sta", NULL);
	irq_request(IRQ_BIT_USBSUSPEND, udev_suspend_irq_handler, "UDEV.suspend", NULL);
	irq_request(IRQ_BIT_USBEARLYSUSPEND, udev_resume_irq_handler, "UDEV.resume", NULL);
	WriteReg32(REG_INT_BASE+0x0, ReadReg32(REG_INT_BASE+0x0)|BIT1); //disable resume

	return 0;
}
