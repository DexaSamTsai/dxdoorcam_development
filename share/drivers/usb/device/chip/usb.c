#include "includes.h"

t_libusb_cfg g_libusb_cfg;
t_libusbclass_cfg *g_libusbclass_cfg;
t_ertos_eventhandler evt_udc;
volatile int libusb_chip_isr = 0;
volatile int libudc_irq = 0;
volatile int libep_irq = 0;
extern void libusb_handlereq(t_udev_ctrl_stage stage);
extern int udcif_interrupt_handler_init(void);

static void udcif_setCSR(void)
{
	int i;

	for(i=0; g_csrdata[i].ctrl != 0x30; i++){
		WriteReg32(UDCIF_CSR_ADDR, g_csrdata[i].addr);
		WriteReg32(UDCIF_CSR_DATA, g_csrdata[i].data);
		debug_printf("set csr(%d): 0x%x 0x%x\n", i, g_csrdata[i].addr , g_csrdata[i].data);
		WriteReg32(UDCIF_CSR_CTRL, g_csrdata[i].ctrl);
	}
	WriteReg32(UDCIF_CSR_CTRL, 0x30);
}

static unsigned int bootfromusb_array[] =
{
	0xe59fd008, //100002a4:	e59fd008 	ldr	sp, [pc, #8]	; 100002b4 <usbboot_stack>
	0xe59f2000, //100002a8:	e59f2000 	ldr	r2, [pc]	; 100002b0 <usbboot_base>
	0xe12fff12, //100002ac:	e12fff12 	bx	r2
	0x0041c3a4, //100002b0:	0041c3a4 	.word	0x0041c3a4
	0x100c8000, //100002b4:	100c8000 	.word	0x100c8000
};

void boot_from_usb(void)
{
#if 0
	///< the following code can't work in OV798, because usb boot of OV798 needs fw's help.
	// open usb clk
	WriteReg32(REG_SC_CLK2, ReadReg32(REG_SC_CLK2)|BIT0|BIT1);
	// usbphy reference clk
	WriteReg32(REG_SC_DIV3, (ReadReg32(REG_SC_DIV3)& 0xffffff80)|0x60);
	// release usb suspend phy
	WriteReg32(REG_SC_ALG, ReadReg32(REG_SC_ALG)|BIT20|BIT15);
	// reset usb phy
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)|BIT0);
	// generate 60M system clock for usb
	WriteReg32(REG_SC_DIV0, (ReadReg32(REG_SC_DIV0)& 0xfc07ffff)|(0x24<<19));
	// reset usb's controller's phy_clk and other
	WriteReg32(REG_SC_RST_RG1, ReadReg32(REG_SC_RST_RG1)|BIT0|BIT1);
	// reset usb's controller's app_clk domain
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)|BIT1|BIT2);
	{volatile int d; for(d=0; d<10; d++); }
	// reset usb's controller's phy_clk and other
	WriteReg32(REG_SC_RST_RG1, ReadReg32(REG_SC_RST_RG1)&~(BIT0|BIT1));
	// reset usb's controller's app_clk domain
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)&~(BIT0|BIT1|BIT2));
	// disable soft_dis, no scale down
	WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG)&0xff7ffbff)|BIT2|BIT3|BIT4);

	//enable soft_discconnect
	WriteReg32(UDCIF_UDC_CFG, ReadReg32(UDCIF_UDC_CFG) | BIT10);
	{volatile int d; for(d=0; d<10; d++); }
	//disable soft_disconnect
	//enable 16bit-phyif, reset irq
	WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG) & (~BIT10)) | BIT30);

	debug_printf("++enable usb_debug_en\n");
#else
#ifdef CONFIG_ERTOS_WATCHDOG_EN
	ertos_watchdog_stop();
#endif

	//< TODO:Why need to enable BA22 clock?
	WriteReg32(REG_SC_ADDR + 0x60, (ReadReg32(REG_SC_ADDR+0x60)&(~BIT8)));
	ddr_disable();
	///<run load_from_usb which is in ROM to implement usb boot.
	WriteReg32(REG_SC_ALG, ReadReg32(REG_SC_ALG)|BIT20|BIT15);
	
	DISABLE_INTERRUPT;
	DISABLE_TICK;
extern int irq_free_all(void);
	irq_free_all();

	//disable cache, call inst
	disable_cache();
	void (*ff)(void) = (void (*)(void))(bootfromusb_array);
	ff();
	while(1);

#endif

}

void udcif_grfc_cfg(int addr, int length)
{
	WriteReg32(UDCIF_UDC_BASE+0xc4, addr>>3);
	WriteReg32(UDCIF_UDC_BASE+0xcc, length);
	WriteReg32(UDCIF_UDC_BASE+0xc8, BIT31|BIT30|BIT29|BIT28|(8<<20)|(8<<12)|(0<<4)|BIT1|BIT0);
	WriteReg32(UDCIF_UDC_BASE+0xc8, ReadReg32(UDCIF_UDC_BASE+0xc8)&~BIT0);
}

void udcif_request_sendack(u8 type, u8 loop)
{
	WriteReg32(UDCIF_CTRL_CTRL, ReadReg32(UDCIF_CTRL_CTRL) | type);
}

void udc_setcfg(void)
{
	int i, plysize;
	u32 size;

	if( g_libusb_cfg.bHighSpeed == 1 ){
		debug_printf("++ HS\n");
		g_libusbclass_cfg = &g_libusbclass_cfg_hs;
	}else{
		debug_printf("++ FS\n");
		g_libusbclass_cfg = &g_libusbclass_cfg_fs;
	}

	for(i=0; i<g_libusbclass_cfg->epnum; i++){
		if(g_libusbclass_cfg->epcfg[i].type & EPTYPE_HW){

		#ifdef USB_UART_PRINT
			debug_printf("set usb, hw_yuv ep set\n");
		#endif
			 
			// cal hw buf size
			if((g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff) > 0x200) {
				size = g_libusbclass_cfg->epcfg[i].buffernum;
			}
			else {
				size = g_libusbclass_cfg->epcfg[i].buffernum>>1;
			}
			size = (size>MAX_UDCIF_BUFSIZE) ? MAX_UDCIF_BUFSIZE:size;
			size = (size<MIN_UDCIF_BUFSIZE) ? MIN_UDCIF_BUFSIZE:size;
			if(g_libusbclass_cfg->epcfg[i].type & EPTYPE_ISO){
				WriteReg32(UDCIF_VIDEO_CFG0, 0x20d80000|((g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK)<<24) | ((size-MIN_UDCIF_BUFSIZE)<<16) | (g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff));
				plysize = (((g_libusbclass_cfg->epcfg[i].maxpktsize>>11) & 0x7)+1) *(g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff);
			}
			else {
				WriteReg32(UDCIF_VIDEO_CFG0, BIT13|0x80d80000|((g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK)<<24) | ((size-MIN_UDCIF_BUFSIZE)<<16) | (g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff));
				plysize = (g_libusbclass_cfg->epcfg[i].maxpktsize & 0x7ff);
			}
			WriteReg32(UDCIF_VIDEO_CFG1, plysize>>2);
		}else{
			WriteReg32(UDCIF_EP_CFG1(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK), 0x1);
			WriteReg32(UDCIF_EP_CFG0(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK), 0x20222200 | ((g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK) << 24) );

			{volatile int delay; for(delay=0; delay<100; delay++);}
			//to avoid the threshhold = 0
			if(g_libusbclass_cfg->epcfg[i].intr_th == 1) {
				g_libusbclass_cfg->epcfg[i].intr_th = 2;
			}

			WriteReg32(UDCIF_EP_CFG0(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK), 0x80000000 | ((g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK) << 24) | (g_libusbclass_cfg->epcfg[i].type << 28) | ((g_libusbclass_cfg->epcfg[i].ep & 0x80)?(1<<30):(0)) | ((g_libusbclass_cfg->epcfg[i].maxpktsize>0x200)?(1<<11):(0)) | (g_libusbclass_cfg->epcfg[i].maxpktsize &0x7ff) | (g_libusbclass_cfg->epcfg[i].baseaddr << 18) | (g_libusbclass_cfg->epcfg[i].buffernum<<12)); 
			if(g_libusbclass_cfg->epcfg[i].maxpktsize > 0x400) {
				WriteReg32(UDCIF_EP_CFG1(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK), (4<<25)|BIT24|(0xc00<<11) |(0x2<<9)|((g_libusbclass_cfg->epcfg[i].intr_th-1)<<4) );
				//need to fix: 3 ISO/uframe
				WriteReg32(UDCIF_EP2_CSR, 0x200088b2);
				WriteReg32(UDCIF_CSR_CTRL, ReadReg32(UDCIF_CSR_CTRL)|BIT10|BIT5);
			}
			else {
				WriteReg32(UDCIF_EP_CFG1(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK), (4<<25)|((g_libusbclass_cfg->epcfg[i].maxpktsize&0x7ff)<<11) | ((g_libusbclass_cfg->epcfg[i].intr_th-1)<<4) );
			}
			debug_printf("ep %x cfg1: %x, maxpkt: %x\n", g_libusbclass_cfg->epcfg[i].ep, ReadReg32(UDCIF_EP_CFG1(g_libusbclass_cfg->epcfg[i].ep & EPNUM_MASK)), g_libusbclass_cfg->epcfg[i].maxpktsize);
		}
	}
}

u32 udc_readEP0(u8 *buffer, u8 length)
{
	u8 i, j, tmp;
	u32 temp_data, timeout;

	timeout = 0;
	while(((ReadReg32(UDCIF_CTRL_CFG) >> 8) & 0x7f) != length){
		debug_printf("not ready::%x, %x\n", ReadReg32(UDCIF_CTRL_CFG), length );
		if(++timeout > 10) {
			debug_printf("setup data error: %x\n", length);
			memset(buffer, 0xff, length);
			return 0;
		}
	}//wait for data ready

	tmp = length>>2;
	WriteReg32(UDCIF_CTRL_ADDR, 0);
	for( i = 0; i < tmp; i ++ )
	{
		temp_data = ReadReg32(UDCIF_CTRL_DATA);
		buffer[i*4] = temp_data & 0xff;
		buffer[i*4+1] = (temp_data >> 8) & 0xff;
		buffer[i*4+2] = (temp_data >> 16) & 0xff;
		buffer[i*4+3] = (temp_data >> 24) & 0xff;
	}
	if(length & 0x3){
		temp_data = ReadReg32(UDCIF_CTRL_DATA);
		tmp = (length>>2) + 1;
		i = i<<2;
		for(j = 0; i<length; i++, j++){
			buffer[i] = (temp_data >> (8*j)) & 0xff;
		}
	}

	return length;
}

u32 udc_writeEP0(u32 *buffer, u8 length, u8 swap)
{
	u8 i, tmp;

	// reset buffer pointer
	WriteReg32(UDCIF_CTRL_ADDR, 0);

	tmp = (length+3)>>2;
	if(swap){
		libusb_swap_char((u8 *)buffer, tmp);
	}

	for( i=0; i<tmp; i++ ) {
		WriteReg32(UDCIF_CTRL_DATA, *(buffer + i));
	}

	WriteReg32(UDCIF_CTRL_ADDR, length);
	{volatile int d; for(d=0; d<200; d++); }

	return length;
}

u32 udc_writeISO(u32 EPNum, u8 *buffer, u32 length) 
{
	u32 addr_cfg2, addr_cfg3;
	int packet_index, byte_index;
	u32 base_addr, data, data_cfg0;
	volatile int write_addr;
	int len, res, total, max_packet_size;
	u32 *pbuf, buf_len, writebuf_num;	

	EPNum &= EPNUM_MASK;
	if(EPBUF_IS_NOTEMPTY(EPNum)) {  // buffer not empty
		//debug_printf("buf(v) must be empty: %x\n", data);
		return 0;
	}

	addr_cfg2 = UDCIF_EP_CFG2(EPNum);
	addr_cfg3 = UDCIF_EP_CFG3(EPNum);
	data = ReadReg32(addr_cfg3);
	buf_len = (data>>26) & 0x1f;
	max_packet_size = 3072;

	data_cfg0 = ReadReg32(UDCIF_EP_CFG0(EPNum)) ;
	writebuf_num = (data_cfg0>>12) & 0x1f;
	writebuf_num = writebuf_num - buf_len;

	base_addr = (data_cfg0>>18) & 0x1f;
	write_addr = ((data>>21) & 0x1f)*((max_packet_size>0x200) ? 0xc00:0x200) + base_addr*0x200 + UDC_RAM_BASE;
	//debug_printf("write iso: %x %x\n", write_addr, length);
	
	//now begin write data
	if(length < (writebuf_num*max_packet_size)) {
		total = length;

		len = length / max_packet_size;
		res = length - max_packet_size*len;
		pbuf = (u32 *)buffer;
		
		for(packet_index=0; packet_index<len; packet_index++)  {
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				pbuf++;
				write_addr +=4;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
			write_addr = ((ReadReg32(addr_cfg3)>>21) & 0x1f)*((max_packet_size>0x200) ? 0xc00:0x200) + base_addr*0x200 + UDC_RAM_BASE;
		}
		total -=(max_packet_size*len);

		if(res)  {
			res = res>>2;
			for(byte_index=0; byte_index<res; byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				//debug_printf(" 0x%x", getu32_le((unsigned char *)pbuf));
				pbuf++;
				write_addr +=4;
			}
			total -=(res*4);
			
			data = 0;
			len = length & 0x3;
			if(len) {
				packet_index = length & 0xffffffc;
				switch(len) {
				case 1:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
#else
					data |= (u32)(buffer[packet_index]<<24);
#endif
					WriteReg32(write_addr, data);
					//debug_printf(" 0x%x\n", data);
					break;

				case 2:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
					data |= (u32)(buffer[packet_index+1]<<8);
#else
					data |= (u32)(buffer[packet_index]<<24);
					data |= (u32)(buffer[packet_index+1]<<16);
#endif
					WriteReg32(write_addr, data);
#ifdef USB_UART_PRINT				
					//debug_printf(" 0x%x\n", data);
#endif				
					break;

				case 3:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
					data |= (u32)(buffer[packet_index+1]<<8);
					data |= (u32)(buffer[packet_index+2]<<16);
#else
					data |= (u32)(buffer[packet_index]<<24);
					data |= (u32)(buffer[packet_index+1]<<16);
					data |= (u32)(buffer[packet_index+2]<<8);
#endif
					WriteReg32(write_addr, data);
					//debug_printf(" 0x%x\n", data);
					break;
		 		}
				total -=len;
			}
			WriteReg32(addr_cfg2, ((length % max_packet_size)) + (1<<16) + (1<<12));
		}
	}
	else{
		total = length;

		pbuf = (u32 *)buffer;
		for(packet_index=0; packet_index<writebuf_num; packet_index++)  {
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				write_addr +=4;
				pbuf++;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
			write_addr = ((ReadReg32(addr_cfg3)>>21) & 0x1f)*((max_packet_size>0x200) ? 0xc00:0x200) + base_addr*0x200 + UDC_RAM_BASE;
		}
		total -=(max_packet_size*writebuf_num);
	}

	//debug_printf(" --%x ", length-total);
	return (length - total);
}

u32 udc_writeEP(u32 EPNum, u8 *buffer, u32 length) 
{
	u32 addr_cfg2, addr_cfg3;
	int packet_index, byte_index;
	u32 base_addr, data, data_cfg0;
	volatile int write_addr;
	int len, res, total, max_packet_size;
	u32 *pbuf, buf_len, writebuf_num;	

	EPNum &= EPNUM_MASK;

	// send zero packet
	if(length == 0) {
		WriteReg32(UDCIF_EP_CFG2(EPNum), (1<<16) + (1<<12));
		return 0;
	}

	if(EPBUF_IS_NOTEMPTY(EPNum)) {  // buffer not empty
		//debug_printf("buf(v) must be empty: %x\n", data);
		return 0;
	}

	addr_cfg2 = UDCIF_EP_CFG2(EPNum);
	addr_cfg3 = UDCIF_EP_CFG3(EPNum);
	data = ReadReg32(addr_cfg3);

	data_cfg0 = ReadReg32(UDCIF_EP_CFG0(EPNum));
	max_packet_size = data_cfg0 & 0x7ff;
	
	buf_len = (data>>26) & 0x1f;
	writebuf_num = (data_cfg0>>12) & 0x1f;
	writebuf_num = writebuf_num - buf_len;

	base_addr = (data_cfg0>>18) & 0x1f;
	write_addr = ((data>>21) & 0x1f)*((max_packet_size>0x200) ? 0x400:0x200) + base_addr*0x200 + UDC_RAM_BASE;
	
	//now begin write data
	if(length < (writebuf_num*max_packet_size)) {
		total = length;

		len = length / max_packet_size;
		res = length - max_packet_size*len;
		pbuf = (u32 *)buffer;
		
		for(packet_index=0; packet_index<len; packet_index++)  {
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				pbuf++;
				write_addr +=4;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
			write_addr = ((ReadReg32(addr_cfg3)>>21) & 0x1f)*((max_packet_size>0x200) ? 0x400:0x200) + base_addr*0x200 + UDC_RAM_BASE;
		}
		total -=(max_packet_size*len);

		if(res)  {
			res = res>>2;
			for(byte_index=0; byte_index<res; byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				//debug_printf(" 0x%x", getu32_le((unsigned char *)pbuf));
				pbuf++;
				write_addr +=4;
			}
			total -=(res*4);
			
			data = 0;
			len = length & 0x3;
			if(len) {
				packet_index = length & 0xffffffc;
				switch(len) {
				case 1:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
#else
					data |= (u32)(buffer[packet_index]<<24);
#endif
					WriteReg32(write_addr, data);
					//debug_printf(" 0x%x\n", data);
					break;

				case 2:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
					data |= (u32)(buffer[packet_index+1]<<8);
#else
					data |= (u32)(buffer[packet_index]<<24);
					data |= (u32)(buffer[packet_index+1]<<16);
#endif
					WriteReg32(write_addr, data);
#ifdef USB_UART_PRINT				
					//debug_printf(" 0x%x\n", data);
#endif				
					break;

				case 3:
#ifdef USB_BIG_ENDIAN
					data |= (u32)(buffer[packet_index]);
					data |= (u32)(buffer[packet_index+1]<<8);
					data |= (u32)(buffer[packet_index+2]<<16);
#else
					data |= (u32)(buffer[packet_index]<<24);
					data |= (u32)(buffer[packet_index+1]<<16);
					data |= (u32)(buffer[packet_index+2]<<8);
#endif
					WriteReg32(write_addr, data);
					//debug_printf(" 0x%x\n", data);
					break;
		 		}
				total -=len;
			}
			WriteReg32(addr_cfg2, (length & (max_packet_size-1)) + (1<<16) + (1<<12));
		}
	}
	else{
		total = length;

		pbuf = (u32 *)buffer;
		for(packet_index=0; packet_index<writebuf_num; packet_index++)  {
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
#ifdef USB_BIG_ENDIAN
				WriteReg32(write_addr, getu32_le((u8 *)pbuf));
#else
				WriteReg32(write_addr, *pbuf);
#endif
				write_addr +=4;
				pbuf++;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
			write_addr = ((ReadReg32(addr_cfg3)>>21) & 0x1f)*((max_packet_size>0x200) ? 0x400:0x200) + base_addr*0x200 + UDC_RAM_BASE;
		}
		total -=(max_packet_size*writebuf_num);
	}

	//debug_printf(" --%x ", length-total);

	return (length - total);
}

u32 udc_readEP(u32 EPNum, u8 *buffer) 
{
	int addr_cfg2, addr_cfg3, data_cfg0, data, packet_index, byte_index;
	int readbuf_num, base_addr; 
	volatile int read_addr;
	int len, res, total, cnt, max_packet_size;
	unsigned int *pbuf;

	EPNum &= EPNUM_MASK;

	addr_cfg2 = UDCIF_EP_CFG2(EPNum);
	addr_cfg3 = UDCIF_EP_CFG3(EPNum);

	data = ReadReg32(addr_cfg3);
	readbuf_num = (data>>26) & 0x1f;
	cnt = data & 0x1fff;
	if(!cnt || !readbuf_num) {
		//debug_printf("data cfg3(zero): 0x%x\n", ReadReg32(UDCIF_EP_CFG3(EPNum)));
		return  0;
	}
	
	data_cfg0 = ReadReg32(UDCIF_EP_CFG0(EPNum)) ;
	base_addr = (data_cfg0>>18) & 0x1f;
	max_packet_size = data_cfg0 & 0x7ff;
	if(max_packet_size > 0x200) {
		read_addr = ((data>>16) & 0x1f)*0x400 + base_addr*0x200 + UDC_RAM_BASE;
	}
	else {
		read_addr = ((data>>16) & 0x1f)*0x200 + base_addr*0x200 + UDC_RAM_BASE;
	}

	//now begin read data
	total = 0;
	if(cnt<(readbuf_num * max_packet_size)) {
		len = cnt/max_packet_size;
		res = ((cnt & (max_packet_size-1))+3)>>2;
		pbuf = (u32 *)buffer;
		for(packet_index=0; packet_index<len; packet_index++)  {
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
				*pbuf = ReadReg32(read_addr);
				pbuf++;
				read_addr +=4;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
			read_addr = ((ReadReg32(addr_cfg3)>>16) & 0x1f)*((max_packet_size>0x200) ? 0x400:0x200) + base_addr*0x200 + UDC_RAM_BASE;
		}
		total +=(max_packet_size*len);
		   
		if(res) {
			for(byte_index=0; byte_index<res; byte_index++) {
				*pbuf = ReadReg32(read_addr);
				pbuf++;
				read_addr +=4;
			}
			res = cnt & (max_packet_size-1);
			total += res;
			WriteReg32(addr_cfg2, ((cnt & (max_packet_size -1)) + (1<<16)));
		}
		else{
			WriteReg32(addr_cfg2, (1<<16));
		}
	}
	else {
		pbuf = (u32 *)buffer;
		res = 0;

		for(packet_index=0; packet_index<readbuf_num; packet_index++)  {
			read_addr = ((ReadReg32(addr_cfg3)>>16) & 0x1f)*((max_packet_size>0x200) ? 0x400:0x200) + base_addr*0x200 + UDC_RAM_BASE;
			for(byte_index=0; byte_index<(max_packet_size>>2); byte_index++) {
				*pbuf = ReadReg32(read_addr);
				pbuf++;
				read_addr +=4;
			}
			WriteReg32(addr_cfg2, (max_packet_size + (1<<16)));
		}
		total += readbuf_num*max_packet_size;
	}
		
#if USB_EPIN_DATA
	 if((cnt & 0x1ff) == 0) {
		debug_printf("EP Out(0x%x): %d\n", buffer, cnt);
		for(byte_index=0; byte_index<cnt; byte_index++) {
			debug_printf(" %x", *(buffer+byte_index));
			if((byte_index & 0xf) == 0xf)      
				debug_printf("\n");
		}
		debug_printf("\n");
	 }
#endif

	//debug_printf(" ++%x ", total);

	return total;
}

void udc_stallEP(u32 EPNum) 
{
	debug_printf("UDC stall endpoint %d\n", EPNum & EPNUM_MASK);
}

void libusb_init(void)
{
	memset(&g_libusb_cfg, 0, sizeof(g_libusb_cfg));

	//must initilized class, also for delay
	usbclass_init(!g_libusb_cfg.bInitOnce);

	// config usb device initilize parameter
	//g_libusb_cfg.config |= UDEVCFG_FLSPEED; 

	debug_printf("++ libusb_init\n");
	debug_printf("01: [%x] %x\n", ReadReg32(REG_SC_RST_LG1), ReadReg32(REG_SC_RST_RG1));
	debug_printf("02: [%x] %x\n", ReadReg32(REG_SC_DIV3), ReadReg32(REG_SC_CLK2));
	debug_printf("03: [%x] %x\n", ReadReg32(REG_SC_DIV0), ReadReg32(REG_SC_ALG));

	evt_udc = ertos_event_create();
	if(evt_udc == NULL){
		debug_printf("libusb_init: error to create evt_udc\n");
		return;
	}

	// open usb clk
	WriteReg32(REG_SC_CLK2, ReadReg32(REG_SC_CLK2)|BIT0|BIT1);
	// usbphy reference clk
	WriteReg32(REG_SC_DIV3, (ReadReg32(REG_SC_DIV3)& 0xffffff80)|0x60);
	// release usb suspend phy
	WriteReg32(REG_SC_ALG, ReadReg32(REG_SC_ALG)|BIT20|BIT15);
	
	//wait for stable clk
	{volatile int d; for(d=0; d<2000; d++); }
	// generate 60M system clock for usb
	WriteReg32(REG_SC_DIV0, (ReadReg32(REG_SC_DIV0)& 0xfc07ffff)|(0x24<<19));

	// reset usb's controller's phy_clk and other
	WriteReg32(REG_SC_RST_RG1, ReadReg32(REG_SC_RST_RG1)|BIT0|BIT1);
	// reset usb's controller's app_clk domain
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)|BIT1|BIT2);
	{volatile int d; for(d=0; d<1000; d++); }
	WriteReg32(REG_SC_RST_RG1, ReadReg32(REG_SC_RST_RG1)&~(BIT0|BIT1));
	WriteReg32(REG_SC_RST_LG1, ReadReg32(REG_SC_RST_LG1)&~(BIT0|BIT1|BIT2));

	// fix: clk-stable for fs-speed
	WriteReg32(REG_SC_ADDR + 0x98, ReadReg32(REG_SC_ADDR+ 0x98) | BIT10);
	// fix: usb 5m_cable
	WriteReg32(REG_SC_ADDR + 0x98, ReadReg32(REG_SC_ADDR+ 0x98) | BIT11);
	// fix: hs calibration
	WriteReg32(REG_SC_ADDR + 0x94, ReadReg32(REG_SC_ADDR+ 0x94) | (BIT13|BIT14|BIT21|BIT22|BIT23));

	//WriteReg32(REG_SC_ADDR + 0x08, ReadReg32(REG_SC_ADDR+ 0x08) | (BIT4|BIT5|BIT6|BIT7));

	//swap descriptor just for one time
	if(g_libusb_cfg.bInitOnce == 0) {
		if(g_libusb_cfg.config & UDEVCFG_SELPOWER) {
			cfgConf_HI[7] = 0x40;
			cfgConf_HI[8] = 0x01;
		}

#if 1
		u32 * tmp = (u32 *)cfgConf_HI;
		debug_printf("libusb_init desc: 0x%x\n", *tmp);
#else
		debug_printf("libusb_init desc: 0x%x\n", *((u32 *)cfgConf_HI));
#endif

		g_libusb_cfg.bInitOnce = 1;
	}
 
	// TODO: reset usb module if cold reboot

	//enable soft_disconnect
	WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG) & (~BIT23)));

	//enable app_device disconnect
	WriteReg32(UDCIF_UDC_CFG, ReadReg32(UDCIF_UDC_CFG) | BIT10);

	//use user-defined csr&desc
	WriteReg32(UDCIF_CSR_CTRL, 0x0);
	WriteReg32(UDCIF_CTRL_CFG, BIT1);

	//disable soft_disconnect
	//enable 16bit-phyif, set-config, set-intf, reset irq
	WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG) & (~BIT10)) | BIT26 | BIT27 | BIT30);

	g_libusb_cfg.bHandled = 0;
	g_libusb_cfg.status = UDEVSTS_INIT;
	memset(&g_libusb_cfg.req, 0, sizeof(g_libusb_cfg.req));

	if(g_libusb_cfg.config & UDEVCFG_FLSPEED){
		//USB speed -> FS
		WriteReg32(UDCIF_UDC_CFG, ReadReg32(UDCIF_UDC_CFG) | BIT8);
		g_libusb_cfg.bHighSpeed = 0;
		debug_putc('F');
	}

	if(g_libusb_cfg.config & UDEVCFG_SELPOWER){
		//USB status -> self-powered
		WriteReg32(UDCIF_UDC_CFG, ReadReg32(UDCIF_UDC_CFG) | BIT1);
		debug_putc('S');
	}

#if 0
	//TODO: endian mode
	g_libusb_cfg.config |= UDEVCFG_BIGENDIAN; 
	WriteReg32(UDCIF_CTRL0, ReadReg32(UDCIF_CTRL0) & (~BIT2)); //reset ENDIAN mode
#ifndef USB_BIG_ENDIAN
	//disable big_endian mode
	g_libusb_cfg.config &= ~UDEVCFG_BIGENDIAN; 
	WriteReg32(UDCIF_CTRL0, ReadReg32(UDCIF_CTRL0) | BIT2);
	debug_putc('E');
#endif
#endif

	// enable vendor ctrl request
	WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)|BIT18);
	// ping ack enable
	WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)|BIT2);

	// clear config intr && reset intr
	WriteReg32(UDCIF_UDC_IRQ, BIT2|BIT23);

	udcif_setCSR();

	g_libusb_cfg.intr_enable = (BIT9 | BIT20);
	libusb_chip_isr = 0;
	libudc_irq = 0;
	g_libusb_cfg.bHighSpeed = 1;

	udcif_interrupt_handler_init();

	//set uvc_xu callback or others
	board_ioctl(BIOC_UDEV_POST_INIT, NULL, NULL);

#ifdef USB_UART_PRINT
	debug_printf("libusb init done\n");
	debug_printf("1: %x [%x]\n", ReadReg32(REG_SC_RST_LG1), ReadReg32(REG_SC_RST_RG1));
	debug_printf("2: %x [%x]\n", ReadReg32(REG_SC_DIV3), ReadReg32(REG_SC_CLK2));
	debug_printf("3: %x [%x]\n", ReadReg32(REG_SC_DIV0), ReadReg32(REG_SC_ALG));
	debug_printf("4: %x %x\n", ReadReg32(REG_SC_ADDR+0x98), ReadReg32(REG_SC_ADDR+0x94));
	debug_printf("5: %x\n", ReadReg32(UDCIF_UDC_CFG));
#endif
}

void libusb_exit(void)
{
	debug_printf("libusb_exit\n");

	//enable app_device disconnect
	WriteReg32(UDCIF_UDC_CFG, ReadReg32(UDCIF_UDC_CFG) | BIT10 );
	{volatile int delay; for(delay=0; delay<1000; delay++);}
	WriteReg32(REG_SC_RESET, ReadReg32(REG_SC_RESET) | (BIT0|BIT2));

	///<TODO: need to implement this fuction
}

void libusb_resume(void)
{
	debug_printf("libusb_resume\n");
	//TODO: hardware have done all precedure.
}

void libusb_reset(void)
{
#ifndef CHIP_ECO1
	if((g_libusb_cfg.config & UDEVCFG_FLSPEED) == 0) {
		WriteReg32(REG_SC_ADDR + 0x94, ReadReg32(REG_SC_ADDR+ 0x94) | BIT29);
	}
#endif
}

void libusb_suspend(void)
{
    //TODO:
	debug_printf("$$ usb suspend $$\n");
	return;

	// disable usb reset intr
	WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG) & ~BIT30));

	g_libusb_cfg.status = UDEVSTS_SUSPEND;

	debug_putc('S');
	debug_putc('O');
	debug_putc('S');

	{volatile int d; for(d=0; d<1000; d++);}
}

void udc_ISR(void)
{
	volatile int k;
	u32 flags;

	if(libusb_chip_isr & BIT3)//handle setup stage
	{
#ifdef USB_UART_PRINT			
		debug_putc('U');
		debug_putc('_');
#endif
		local_irq_save(flags);
		libusb_chip_isr &= ~BIT3;
		local_irq_restore(flags);

		k = ReadReg32(UDCIF_CTRL_CFG);
		if ( (k & BIT4) && (((ReadReg32(UDCIF_CTRL_CFG) >> 8) & 0x3f) == 8) )//usb setup command
		{
#ifdef USB_UART_PRINT			
			debug_putc('8');
#endif
			WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)&~BIT16);

			//get setup packet data
			udc_readEP0(g_libusb_cfg.req.databuf, 8);

			libusb_handlereq(UDEV_SETUP_STAGE);
		}
	}
	if(libusb_chip_isr & BIT9)//handle setup data stage
	{
#ifdef USB_UART_PRINT			
		debug_putc('U');
		debug_putc('_');
#endif
		local_irq_save(flags);
		libusb_chip_isr &= ~BIT9;
		local_irq_restore(flags);

		if(DATA_FROM_HOST && g_libusb_cfg.req.wLength)
		{
#ifdef USB_UART_PRINT			
			debug_putc('r');
#endif
			udc_readEP0(g_libusb_cfg.req.databuf, g_libusb_cfg.req.wLength);

			libusb_handlereq(UDEV_DATA_STAGE);
		}
		else if(DATA_TO_HOST && g_libusb_cfg.req.wLength)
		{
#ifdef USB_UART_PRINT			
			debug_putc('t');
#endif
			libusb_handlereq(UDEV_DATA_STAGE);
		}else {
			udcif_request_sendack(SEND_ACK_NEXTDATA, 0);
		}
	}

	if(libusb_chip_isr & BIT17)//handle setup status stage
	{
#ifdef USB_UART_PRINT			
		debug_putc('U');
		debug_putc('_');
#endif

		local_irq_save(flags);
		libusb_chip_isr &= ~BIT17;
		local_irq_restore(flags);

		debug_putc('q');
		k = ReadReg32(UDCIF_CTRL_CTRL);
		if (k & BIT5) {	//usb high speed flag
			g_libusb_cfg.bHighSpeed = 1;
		}
		else {
			g_libusb_cfg.bHighSpeed = 0;
		}
	}

	if(libusb_chip_isr & BIT19)//usb non_ctrl ep interrupt
	{
		local_irq_save(flags);
		libusb_chip_isr &= ~BIT19;
		local_irq_restore(flags);

		if(g_libusbclass_cfg->handleEP)	{
			//debug_printf("ep: %x\n", libep_irq);
			g_libusbclass_cfg->handleEP((u32 *)&libep_irq);

			// add irq if ep irq exist(eg: buffer full)
			if(libep_irq){
				libusb_chip_isr |= BIT19;
			}
		}

		for(k=1; k<7; k++) {
			if((ReadReg32(UDCIF_EP_CFG0(k)) & BIT30)==0) { 
				ENABLE_EPINTR(k);
			}
		}
	}

	if(libusb_chip_isr & BIT20)	//usb UDC interrupt
	{
		local_irq_save(flags);
		libusb_chip_isr &= ~BIT20;
		local_irq_restore(flags);

		k = ReadReg32(UDCIF_UDC_IRQ);
		k += libudc_irq;
		if(k & BIT2) {	//usb set config interrupt
#ifdef USB_UART_PRINT			
			debug_putc('U');
			debug_putc('_');
			debug_putc('c');
#endif
			WriteReg32(UDCIF_UDC_IRQ, BIT2);
#ifdef USB_UART_PRINT			
			debug_printf("Set config: %d\n", (k>>4)&0xf);
#endif
			if((k>>4)&0xf) {
				g_libusb_cfg.status = UDEVSTS_CONFIG;
				// enable usb reset irq
				WriteReg32(UDCIF_UDC_CFG, (ReadReg32(UDCIF_UDC_CFG) | BIT30));
				udc_setcfg();
				libep_irq = 0;
			}
		}
		if(k & BIT3){//usb set interface interrupt
#ifdef USB_UART_PRINT			
			debug_putc('U');
			debug_putc('_');
			debug_putc('i');
#endif

			WriteReg32(UDCIF_UDC_IRQ, BIT3);
#ifdef USB_UART_PRINT
			debug_printf("Set intf: %d, alt intf : %d\n", (k>>12)&0xf, (k>>8)&0xf);
#endif
			usbclass_HandleSetif( ((k>>12) & 0xf), (k>>8) & 0xf);
		}

	    if(k & BIT23)//usb reset interrupt
		{
			debug_putc('R');
			debug_putc('s');
			debug_putc('t');
			WriteReg32(UDCIF_UDC_IRQ, BIT23);
			
			libusb_reset();
	    }

	    if(k & BIT24)//usb GRFC interrupt
		{
			//debug_putc('G');
			//debug_putc('R');
			//debug_putc('F');
			//debug_putc('C');
			if(g_libusbclass_cfg->handleGRFC)	{
				g_libusbclass_cfg->handleGRFC();
			}
		}else {
			k = ReadReg32(UDCIF_CTRL_CTRL);
			if (k & BIT5) {	//usb high speed flag
				g_libusb_cfg.bHighSpeed = 1;
				debug_putc('H');
			}
			else {
				debug_putc('F');
				g_libusb_cfg.bHighSpeed = 0;
			}
		}

		libudc_irq &= ~(BIT2|BIT3|BIT23|BIT24|BIT26); 
	}
}

t_usbisrret libusb_loop(void)
{
	if(g_libusb_cfg.status == UDEVSTS_SUSPEND){
		libusb_resume();
		g_libusb_cfg.status = UDEVSTS_UNKNOWN;

		//TODO: don't need to reboot, we will reload firmare
	}

	while(libusb_chip_isr){
		udc_ISR();
	}

	return USBISRRET_OK;
}

void libusb_start(void)
{
	debug_printf("usb device start...\n");
	while (1) {
		if(ertos_event_wait(evt_udc, -1)){
			libusb_loop();
			board_ioctl(BIOC_UDEV_SET_LOOP, NULL, NULL);
		}
	}
}

