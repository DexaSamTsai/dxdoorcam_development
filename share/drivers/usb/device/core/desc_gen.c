#include "includes.h"

extern char * _Vendor_String;
extern char * _SerialNumber_String;
extern char * _Product_String_Audio;
extern char * _Product_String_Video;

//#define printf_info debug_printf
#define printf_info(...)
#define printf_error debug_printf

#define MAX_DESC_LEN 8192 

static unsigned char desc_buf[MAX_DESC_LEN + 2];
static char *desc_buf_str[MAX_DESC_LEN];
static char *desc_section_str[MAX_DESC_LEN];
static int desc_cnt = 0;

static int bigendian=0;
static int comment=1;

#define MAX_PU_CNT	0x03
#define MAX_IT_CNT	0x05

static u16 pu_start[MAX_PU_CNT];
static u16 it_start[MAX_PU_CNT][MAX_IT_CNT];
static u8 pu_cnt = 0; //pu unit count of the whole CFG_Desc
static u8 it_cnt = 0; //it unit count on one VC 

static void desc_addbyte(unsigned char b, char *str)
{
	desc_buf[desc_cnt] = b;
	
	if(str){
		desc_buf_str[desc_cnt] = malloc(strlen(str) + 1);
		if(desc_buf_str[desc_cnt] == NULL){
			printf_error("cannot alloc str\n");
		}
		strcpy(desc_buf_str[desc_cnt], str);
	}
	desc_cnt ++;
	if(desc_cnt >= MAX_DESC_LEN){
		printf_error("desc len overflow\n");
	}
}

static void desc_changeshort(int offset, unsigned short b)
{
	desc_buf[offset] = b & 0xff;
	desc_buf[offset + 1] = (b >> 8) & 0xff;
}

static void desc_addshort(unsigned short b, char *str)
{
	desc_buf[desc_cnt] = b & 0xff;
	desc_buf[desc_cnt + 1] = (b >> 8) & 0xff;
	if(str){
		desc_buf_str[desc_cnt] = malloc(strlen(str) + 1);
		if(desc_buf_str[desc_cnt] == NULL){
			printf_error("cannot alloc str\n");
		}
		strcpy(desc_buf_str[desc_cnt], str);
	}
	desc_cnt += 2;
	if(desc_cnt >= MAX_DESC_LEN){
		printf_error("desc len overflow\n");
	}
}

static void desc_addint(unsigned int b, char *str)
{
	desc_buf[desc_cnt] = b & 0xff;
	desc_buf[desc_cnt + 1] = (b >> 8) & 0xff;
	desc_buf[desc_cnt + 2] = (b >> 16) & 0xff;
	desc_buf[desc_cnt + 3] = (b >> 24) & 0xff;
	if(str){
		desc_buf_str[desc_cnt] = malloc(strlen(str) + 1);
		if(desc_buf_str[desc_cnt] == NULL){
			printf_error("cannot alloc str\n");
		}
		strcpy(desc_buf_str[desc_cnt], str);
	}
	desc_cnt += 4;
	if(desc_cnt >= MAX_DESC_LEN){
		printf_error("desc len overflow\n");
	}
}

static int section_start = 0;

static void desc_startsection(char *str, unsigned char b)
{
	section_start = desc_cnt;

	if(str){
		desc_section_str[desc_cnt] = malloc(strlen(str) + 1);
		if(desc_section_str[desc_cnt] == NULL){
			printf_error("cannot alloc str\n");
		}
		strcpy(desc_section_str[desc_cnt], str);
	}

	desc_addbyte(0, "Length");
	desc_addbyte(b, "Type");
}

static void desc_endsection(void)
{
	desc_buf[section_start] = desc_cnt - section_start;
}

static int startcount;
static void desc_startcount(void)
{
	startcount = desc_cnt;
}
static int desc_endcount(void)
{
	return (desc_cnt - startcount);
}

static void desc_print(void)
{
	int i;

	//fix total len
	desc_buf[2] = desc_cnt & 0xff;
	desc_buf[3] = (desc_cnt >> 8) & 0xff;

	if(bigendian){
		for(i=0; i<desc_cnt; i+=4){
			printf_info("	0x%02x, \n", desc_buf[i+3] );
			printf_info("	0x%02x, \n", desc_buf[i+2] );
			printf_info("	0x%02x, \n", desc_buf[i+1] );
			printf_info("	0x%02x, \n", desc_buf[i+0] );
		}
	}else{
		for(i=0; i<desc_cnt; i++){
			if(comment){
				if(desc_section_str[i]){
					printf_info("\n	// --- %s ---\n", desc_section_str[i]);
				}
				printf_info("	0x%02x, %s%s\n", desc_buf[i],
					desc_buf_str[i] ? "// " : "",
					desc_buf_str[i] ? desc_buf_str[i] : ""
				  );
			}else{
				printf_info("	0x%02x, \n", desc_buf[i] );
			}
		}
	}
}

static t_libusbep_cfg * find_epcfg(t_libusbclass_cfg * usbcfg, u8 ep)
{
	int i;
	for(i=0; i < usbcfg->epnum; i++){
		if(usbcfg->epcfg[i].ep == ep){
			return &usbcfg->epcfg[i];
		}
	}
	return NULL;
}

static u8 get_bitmap_count(int n)
{
	u8 i, cnt;

	cnt = 0;
	for(i=0; i<32; i++) {
		if(n & (0x01<<i)) {
			cnt++;
		}
	}

	return cnt;
}

int host_atoi(char *c, int len) 
{
    int ret = 0, i = len;
    char *s = c;

    for (;  *s&&i; s++, i--)
    {
        ret *= 16;
        if (*s >= '0' &&  *s <= '9')
        {
            ret += (*s - '0');
        }
        else if (*s >= 'a' &&  *s <= 'f')
        {
            ret += (*s - 'a' + 10);
        }
        else if (*s >= 'A' &&  *s <= 'F')
        {
            ret += (*s - 'A' + 10);
        }
        else
        {
            break;
        }
    }

    return ret;
}

int host_endian_swap(int dd)
{
	int d1, d2, d3, d4;

	d1 = dd & 0xff;
	d2 = dd & 0xff00;
	d3 = dd & 0xff0000;
	d4 = dd & 0xff000000;

	if((d2+d3+d4) == 0) {
		return d1;
	}else if((d3+d4) == 0) {
		return ((d1 << 8) | (d2 >> 8)); 
	}else if(d4 == 0) {
		return ((d1 << 16) | d2 | (d3 >> 16)); 
	}else {
		return ((d1 << 24) | (d2 << 8) | (d3 >> 8) | ((d4 >> 24) & 0xff));
	}
}

//guid{DD880F8A-1CBA-4954-8A25-F7875967F0F7}
static void desc_addguid(char *xu_guid)
{
	int dd;	
	char *desc;

	desc = malloc(9);
	if(desc == NULL) {
		printf_error("//#error cannot alloc guid buf.\n");
		return;
	}

	strncpy(desc, xu_guid, 8);
	dd = host_atoi(desc, 8);
	desc_addint(dd, xu_guid);

	strncpy(desc, xu_guid+9, 4);
	dd = host_atoi(desc, 4);
	desc_addshort(dd, NULL);

	strncpy(desc, xu_guid+14, 4);
	dd = host_atoi(desc, 4);
	desc_addshort(dd, NULL);

	strncpy(desc, xu_guid+19, 4);
	dd = host_atoi(desc, 4);
	dd = host_endian_swap(dd);
	desc_addshort(dd, NULL);

	strncpy(desc, xu_guid+24, 8);
	dd = host_atoi(desc, 8);
	dd = host_endian_swap(dd);
	desc_addint(dd, NULL);

	strncpy(desc, xu_guid+32, 4);
	dd = host_atoi(desc, 4);
	dd = host_endian_swap(dd);
	desc_addshort(dd, NULL);

	if(desc) {
		free(desc);
		desc = NULL;
	}
}

static void gen_conf_vctrl(t_libiad_cfg * cfg, t_libusbclass_cfg *usbcfg)
{
	int i,j;
	int cs_size_offset;

	if(cfg->vctrlif->tid_cnt != cfg->vstrmif_cnt) {
		printf_error("//#error video control tid not match stream descriptor. %d -- %d \n", cfg->vctrlif->tid_cnt, cfg->vstrmif_cnt);
		return;
	}

	t_libusbep_cfg *intr_epcfg = find_epcfg(usbcfg, cfg->intr_ep);

	if(pu_cnt > MAX_PU_CNT) {
		printf_error("//#error pu_cnt: %d overflow, you need increase temp pu/it array size.\n", pu_cnt);
		return;
	}
	pu_start[pu_cnt] = 0;
	for(i=0; i<MAX_IT_CNT; i++) {
		it_start[pu_cnt][i] = 0;
	}

	desc_startsection("Standard Video Interface Collection IAD", 0x0b);
	desc_addbyte(cfg->vctrlif->CTRLIF_ID, "First interface number");
	desc_addbyte(cfg->vstrmif_cnt + 1, "Interface count");
	desc_addbyte(0x0e, "Function class");
	desc_addbyte(0x03, "Function subclass");
	desc_addbyte(0x00, "not used");
	desc_addbyte(0x02, "interface string descriptor index");
	desc_endsection();

	desc_startsection("Standard VC Interface Descriptor", 0x04);
	desc_addbyte(cfg->vctrlif->CTRLIF_ID, "interface number");
	desc_addbyte(0x00, "index of the alternate setting");
	desc_addbyte(intr_epcfg ? 0x01 : 0x00, "number of endpoint");
	desc_addbyte(0x0e, "interface class");
	desc_addbyte(0x01, "interface subclass");
	desc_addbyte(0x00, "interface protocol");
	desc_addbyte(0x02, "interface string descriptor index");
	desc_endsection();

	//start count CS_VC length
	desc_startcount();

	desc_startsection("Class-Specific VC Interface Descriptor", 0x24);
	desc_addbyte(0x01, "subtype:VC_HEADER");
	desc_addshort(0x0100, "revision of class specification -- uvc1.0 spec");
	cs_size_offset = desc_cnt;
	desc_addshort(0x0000, "total size of class-specification descriptor");
	desc_addint(30000000, "clock frequency");
	desc_addbyte(cfg->vstrmif_cnt, "number of streaming interface");
	for(i=0; i<cfg->vstrmif_cnt; i++){
		desc_addbyte(cfg->vstrmif[i].STRMIF_ID, "streaming interface id");
	}
	desc_endsection();

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	u8 tmp_id[2][MAX_IT_CNT];
	if(cfg->vctrlif->tid_cnt > MAX_IT_CNT) {
		printf_error("//#error temp array size is < 5   -- tid_cnt: %d\n", cfg->vctrlif->tid_cnt);
		return;
	}
	for(i=0; i<cfg->vctrlif->tid_cnt; i++) {
		tmp_id[0][i] = cfg->vctrlif->tid[i].IT_ID;
		tmp_id[1][i] = cfg->vctrlif->tid[i].OT_ID;
	}
	for(i=0; i<cfg->vctrlif->tid_cnt; i++) {
		for(j=i+1; j<cfg->vctrlif->tid_cnt; j++) {
			if(cfg->vctrlif->tid[i].IT_ID == tmp_id[0][j])	{
				tmp_id[0][j] = 0;
			}
			if(cfg->vctrlif->tid[i].OT_ID == tmp_id[1][j])	{
				tmp_id[1][j] = 0;
			}
		}
	}
	u8 is_enc;
	it_cnt = 0;
	for(i=0; i<cfg->vctrlif->tid_cnt; i++) {
		if(cfg->vstrmif[i].STRM_APP == VIDEO_STREAM_APP_ENC) {
			is_enc = 1;
		}else if(cfg->vstrmif[i].STRM_APP == VIDEO_STREAM_APP_DEC) {
			is_enc = 0;
		}else {
			printf_error("\n\n//################ error t_libuav_cfg vstrmif application type not be indicate! ################\n\n");
			return;
		}

		//for tid struct, same IT_ID Value only use one IT descriptor
		if(tmp_id[0][i] != 0) {
			desc_startsection(is_enc ? "Input Terminal Descriptor(camera)":"Input Terminal Descriptor(streaming)", 0x24);
			desc_addbyte(0x02, "subtype:VC_INPUT_TERMINAL");
			desc_addbyte(tmp_id[0][i], "terminal ID");
			desc_addshort(is_enc ? 0x0201:0x0101, is_enc ? "itt_camera terminal type":"tt_streaming terminal type");
			desc_addbyte(0x00, "no association");
			desc_addbyte(0x00, "unused");
			if(is_enc) {
				desc_addshort(0x0000, "no optical zoom supported");
				desc_addshort(0x0000, "no optical zoom supported");
				desc_addshort(0x0000, "no optical zoom supported");
				it_start[pu_cnt][it_cnt++] = desc_cnt;
				desc_addbyte(0x03, "control size");
				desc_addbyte(0x00, "bmControl");
				desc_addbyte(0x00, "bmControl");
				desc_addbyte(0x00, "bmControl");
			}
			desc_endsection();
		}
		
		//for tid struct, same OT_ID Value only use one OT descriptor
		if(tmp_id[1][i] != 0) {
			desc_startsection("Ouput Terminal Descriptor", 0x24);
			desc_addbyte(0x03, "subtype:VC_OUTPUT_TERMINAL");
			desc_addbyte(tmp_id[1][i], "terminal ID");
			desc_addshort(is_enc ? 0x0101:0x0301, is_enc ? "tt_streaming terminal type":"ott_display, terminal type");
			desc_addbyte(0x00, "association terminal");
			desc_addbyte(cfg->vctrlif->PU_ID, "source ID");
			desc_addbyte(0x00, "unused");
			desc_endsection();
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	desc_startsection("Processing Unit Descriptor", 0x24);
	desc_addbyte(0x05, "subtype:VC_PROCESSING_UNIT");
	desc_addbyte(cfg->vctrlif->PU_ID, "unit ID");
	desc_addbyte(cfg->vctrlif->tid[0].IT_ID, "source ID(input terminal ID)");
	desc_addshort(0x0000, "unused, not support digital multiplier");
	pu_start[pu_cnt++] = desc_cnt;
	desc_addbyte(0x02, "control size");
	desc_addshort(0x0000, "controls(Effects)");
	desc_addbyte(0x02, "string index");
	desc_endsection();

	desc_startsection("Extension Unit Descriptor", 0x24);
	desc_addbyte(0x06, "subtype:VC_EXTENSION_UNIT");
	desc_addbyte(cfg->vctrlif->XU_ID, "unit ID");
	desc_addguid(cfg->vctrlif->XU_GUID);
	desc_addbyte(get_bitmap_count(cfg->vctrlif->XU_bmControl), "number of controls in extension unit");
	desc_addbyte(0x01, "input pins number");
	desc_addbyte(cfg->vctrlif->PU_ID, "source ID");
	desc_addbyte(0x01, "contorl size");
	desc_addbyte(cfg->vctrlif->XU_bmControl, "bmControl");
	desc_addbyte(0x02, "string index");
	desc_endsection();
	
	if(cfg->vctrlif->H_XU_ID) {
		desc_startsection("Encv Extension Unit Descriptor", 0x24);
		desc_addbyte(0x06, "subtype:VC_EXTENSION_UNIT");
		desc_addbyte(cfg->vctrlif->H_XU_ID, "unit ID");
		desc_addint(0xa29e7641, "guid{A29E7641-DE04-47E3-8B2B-F4341AFF003B}");
		desc_addshort(0xde04, NULL);
		desc_addshort(0xe347, NULL);
		desc_addshort(0x2b8b, NULL);
		desc_addint(0xff1a34f4, NULL);
		desc_addshort(0x3b00, NULL);
		desc_addbyte(0x01, "number of controls in extension unit");
		desc_addbyte(0x01, "input pins number");
		desc_addbyte(cfg->vctrlif->XU_ID, "source ID");
		desc_addbyte(0x01, "contorl size");
		desc_addbyte(0x00, "bmControl");
		desc_addbyte(0x00, "string index");
		desc_endsection();
	}

	//change CS_VC length
	desc_changeshort(cs_size_offset, desc_endcount());

	if(intr_epcfg) {
		desc_startsection("standard interrupt endpoint descriptor", 0x05);
		desc_addbyte(intr_epcfg->ep, "");
		desc_addbyte(0x03, "interrupt transfer type");
		desc_addshort(intr_epcfg->maxpktsize, "max packet size");
		desc_addbyte(intr_epcfg->interval, "polling interval -- 128ms");
		desc_endsection();

		desc_startsection("class-specific interrupt endpoint descriptor", 0x25);
		desc_addbyte(0x03, "ep_interrupt");
		desc_addshort(intr_epcfg->maxpktsize, "max packet size");
		desc_endsection();
	}
}

static void gen_conf_vstrm(t_libiad_cfg *cfg, t_libusbclass_cfg *usbcfg)
{
	int i,j;
	int cs_size_offset;

	for(i=0; i<cfg->vstrmif_cnt; i++){
		t_libusbep_cfg *epcfg = find_epcfg(usbcfg, cfg->vstrmif[i].STRM_EP);
		if(epcfg == NULL){
			printf_error("//error Cannot find video epcfg:%d\n", cfg->vstrmif[i].STRM_EP);
			return;
		}

		desc_startsection("standard VS Interface Descriptor(alt 0)", 0x04);
		desc_addbyte(cfg->vstrmif[i].STRMIF_ID, "index of this interface");
		desc_addbyte(0x00, "index of this alternate setting");
		if((epcfg->type & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK) {
			desc_addbyte(0x01, "number of endpoint(BULK)");
		}else{
			desc_addbyte(0x00, "number of endpoint(ISO)");
		}
		desc_addbyte(0x0e, "cc_video, interface class");
		desc_addbyte(0x02, "sc_videostreaming, subclass");
		desc_addbyte(0x00, "pc_protocol_undefined");
		desc_addbyte(0x00, "unused");
		desc_endsection();

		//start count CS_VS length
		desc_startcount();

		if(cfg->vstrmif[i].STRM_APP == VIDEO_STREAM_APP_ENC) {
			desc_startsection("Class-Specific VS Interface Input Header Descriptor", 0x24);
			desc_addbyte(0x01, "subtype:VS_INPUT_HEADER");
			desc_addbyte(0x01, "number of video payload format");
			cs_size_offset = desc_cnt;
			desc_addshort(0x0000, "total size of class-specific videostreaming interface descriptor");
			desc_addbyte(cfg->vstrmif[i].STRM_EP, "address of the iso endpoint used for video data");
			desc_addbyte(0x00, "no dynamic format change supported");
			desc_addbyte(cfg->vstrmif[i].STRMLINK_ID, "terminal link ID(ouput terminal)");
			desc_addbyte(cfg->vstrmif[i].still_image ? 0x02:0x00, "StillCaptureMethod: no");
			desc_addbyte(cfg->vstrmif[i].still_image ? 0x01:0x00, "TriggerSupport: no");
			desc_addbyte(0x00, "TriggerUsage: no");
			desc_addbyte(0x01, "Control size");
			desc_addbyte(0x00, "no videostreaming specific control - format 1");
			desc_endsection();
		}else if(cfg->vstrmif[i].STRM_APP == VIDEO_STREAM_APP_DEC) {
			desc_startsection("Class-Specific VS Interface Output Header Descriptor", 0x24);
			desc_addbyte(0x02, "subtype:VS_OUTPUT_HEADER");
			desc_addbyte(0x01, "number of video payload format");
			cs_size_offset = desc_cnt;
			desc_addshort(0x0000, "total size of class-specific videostreaming interface descriptor");
			desc_addbyte(cfg->vstrmif[i].STRM_EP, "address of the iso endpoint used for video data");
			desc_addbyte(cfg->vstrmif[i].STRMLINK_ID, "terminal link ID(input terminal)");
			//desc_addbyte(0x01, "Control size");//TODO
			//desc_addbyte(0x00, "bmControl");//TODO
			desc_endsection();
		}
		else {
			printf_error("\n\n//################ error t_libuav_cfg vstrmif application type not be indicate! ################\n\n");
			return;
		}

		switch(cfg->vstrmif[i].format) {
		case VS_FORMAT_UNCOMPRESSED:
			desc_startsection("class-specific vs format descriptor", 0x24);
			desc_addbyte(cfg->vstrmif[i].format, "subtype: VS_FORMAT_UNCOMPRESSED");
			desc_addbyte(0x01, "Index of this format descriptor");
			desc_addbyte(cfg->vstrmif[i].frame_cnt, "Number of frame descriptors following that correspond to this format");

			desc_addint(0x32595559, "GUID of stream-encoding format,YUY2(YUV422) <<32595559-0000-0010-8000-00AA00389B71>>");
			desc_addshort(0x0000, NULL);
			desc_addshort(0x0010, NULL);
			desc_addshort(0x0080, NULL);
			desc_addint(0x3800aa00, NULL);
			desc_addshort(0x719b, NULL);
			desc_addbyte(0x10, "Number of bits per pixel used to specify color in the decoded video frame");
			desc_addbyte(0x01, "DefaultFrameIndex: force 1");
			desc_addbyte(0x00, "The X dimension of the picture aspect ratio");
			desc_addbyte(0x00, "The Y dimension of the picture aspect ratio");
			desc_addbyte(0x00, "Specifies interlace information");
			desc_addbyte(0x00, "CopyProtect");
			desc_endsection();
			break;
		case VS_FORMAT_MIMG:
			desc_startsection("class-specific vs format descriptor", 0x24);
			desc_addbyte(cfg->vstrmif[i].format, "subtype: VS_FORMAT_MIMG");
			desc_addbyte(0x01, "Index of this format descriptor");
			desc_addbyte(cfg->vstrmif[i].frame_cnt, "Number of frame descriptors following that correspond to this format");

			desc_addbyte(0x01, "sample size is fixed");
			desc_addbyte(0x01, "DefaultFrameIndex: force 1");
			desc_addbyte(0x00, "The X dimension of the picture aspect ratio");
			desc_addbyte(0x00, "The Y dimension of the picture aspect ratio");
			desc_addbyte(0x00, "Specifies interlace information");
			desc_addbyte(0x00, "CopyProtect");
			desc_endsection();
			break;
		case VS_FORMAT_MPEG2TS:
			desc_startsection("class-specific vs format descriptor", 0x24);
			desc_addbyte(cfg->vstrmif[i].format, "subtype: VS_FORMAT_MPEG2TS");
			desc_addbyte(0x01, "Index of this format descriptor");
			desc_addbyte(0x00, "offset of data");
			desc_addbyte(0xbc, "length of packet");
			desc_addbyte(0xbc, "length of stride");
			desc_endsection();

			goto skipframe;
			break;
		case VS_FORMAT_FRAME_BASED:
			desc_startsection("class-specific vs format descriptor", 0x24);
			desc_addbyte(cfg->vstrmif[i].format, "subtype: VS_FORMAT_FRAME_BASED");
			desc_addbyte(0x01, "Index of this format descriptor");
			desc_addbyte(cfg->vstrmif[i].frame_cnt, "Number of frame descriptors following that correspond to this format");

			desc_addint(0x34363248, "GUID of stream-encoding format, encv-std <<32595559-0000-0010-8000-00AA00389B71>>");
			desc_addshort(0x0000, NULL);
			desc_addshort(0x0010, NULL);
			desc_addshort(0x0080, NULL);
			desc_addint(0x3800aa00, NULL);
			desc_addshort(0x719b, NULL);
			desc_addbyte(0x10, "Number of bits per pixel used to specify color in the decoded video frame");
			desc_addbyte(0x01, "DefaultFrameIndex: force 1");
			desc_addbyte(0x00, "The X dimension of the picture aspect ratio");
			desc_addbyte(0x00, "The Y dimension of the picture aspect ratio");
			desc_addbyte(0x00, "Specifies interlace information");
			desc_addbyte(0x00, "CopyProtect");
			desc_addbyte(0x01, "variable size");
			desc_endsection();
			break;
		default:
			printf_error("\n\n//################ error t_libuav_cfg vstrmif format not match! ################\n\n");
			return;
		}

		if(cfg->vstrmif[i].frame_cnt == 0){
			printf_error("####error, frame is empty, you must add frame descriptor!!!!\n");
		}

		for(j=0; j<cfg->vstrmif[i].frame_cnt; j++){
			desc_startsection("class-specific vs frame descriptor", 0x24);
			desc_addbyte(cfg->vstrmif[i].format + 1, "subtype: format+1");
			desc_addbyte(j+1, "Index of this frame descriptor");
			desc_addbyte(0x00, "not support still image,and fixed frame rate");
			desc_addshort((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff, "width");
			desc_addshort((cfg->vstrmif[i].frame[j].size & 0xffff), "height");

			if(cfg->vstrmif[i].frame[j].framerates) {
				desc_addint( ((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff) * (cfg->vstrmif[i].frame[j].size & 0xffff) * 16 * cfg->vstrmif[i].frame[j].framerates[cfg->vstrmif[i].frame[j].framerate_cnt - 1].framerate, "bitrate min");
				desc_addint( ((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff) * (cfg->vstrmif[i].frame[j].size & 0xffff) * 16 * cfg->vstrmif[i].frame[j].framerates[0].framerate, "bitrate max");
			}else {
				desc_addint( ((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff) * (cfg->vstrmif[i].frame[j].size & 0xffff) * 16 * 1, "bitrate min");
				desc_addint( ((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff) * (cfg->vstrmif[i].frame[j].size & 0xffff) * 16 * 30, "bitrate max");
			}

			if(cfg->vstrmif[i].format != VS_FORMAT_FRAME_BASED) {
				desc_addint( ((cfg->vstrmif[i].frame[j].size >> 16) & 0xffff) * (cfg->vstrmif[i].frame[j].size & 0xffff) * 2, "framesize");
			}

			if(cfg->vstrmif[i].frame[j].framerate_cnt > 0 && cfg->vstrmif[i].frame[j].framerates[0].framerate > 0){
				desc_addint(10000000/cfg->vstrmif[i].frame[j].framerates[0].framerate, "default frame interval");
				desc_addbyte( cfg->vstrmif[i].frame[j].framerate_cnt, "number of frame interval");
				if(cfg->vstrmif[i].format == VS_FORMAT_FRAME_BASED) {
					desc_addint(0x00000000, "the number of bytes per line of video");//only for encv-std format
				}
				int m;
				for(m=0; m<cfg->vstrmif[i].frame[j].framerate_cnt; m++){
					if(cfg->vstrmif[i].frame[j].framerates[m].framerate > 0){
						desc_addint(10000000/cfg->vstrmif[i].frame[j].framerates[m].framerate, "fps");
					}else{
					}
				}
			}else{
				desc_addint(0x00051615, "default frame interval");
				desc_addbyte(1, "number of frame interval");
				if(cfg->vstrmif[i].format == VS_FORMAT_FRAME_BASED) {
					desc_addint(0x00000000, "the number of bytes per line of video");//only for encv-std format
				}
				desc_addint(0x00051615, "fps");
			}

			desc_endsection();
		}

		if(cfg->vstrmif[i].still_image) {
			desc_startsection("still image frame descriptor", 0x24);
			desc_addbyte(0x03, "subtype:VS_STILL_IMAGE_FRAME");
			desc_addbyte(cfg->vstrmif[i].STRM_EP, "method 2 of still image capture is used");
			desc_addbyte(cfg->vstrmif[i].still_image->frame_cnt, "number of image size patterns of this format");
			for(j=0; j<cfg->vstrmif[i].still_image->frame_cnt; j++) {
				desc_addshort((cfg->vstrmif[i].still_image->frame[0].size >> 16) & 0xffff, "width");
				desc_addshort((cfg->vstrmif[i].still_image->frame[0].size & 0xffff), "height");
			}
			desc_addbyte(0x00, "number of commpression pattern of this format");
			desc_endsection();
		}

		desc_startsection("color matching descriptor", 0x24);
		desc_addbyte(0x0d, "subtype:VS_COLORFORMAT");
		desc_addbyte(0x01, "ColorPrimaries:This defines the color primaries and the reference white. 1: BT.709, sRGB (default)");
		desc_addbyte(0x01, "TransferCharacteristics:This field defines the opto-electronic transfer characteristic of the source picture also called the gamma function. 1: BT.709 (default)");
		desc_addbyte(0x04, "MatrixCoefficients: Matrix used to compute luma and chroma values from the color primaries. 1: BT. 709");
		desc_endsection();

		//change CS_VS length
		desc_changeshort(cs_size_offset, desc_endcount());

		if((epcfg->type & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK) {
			desc_startsection("standard vs bulk video data endpoint descriptor", 0x05);
			desc_addbyte(cfg->vstrmif[i].STRM_EP, "endpoint address");
			desc_addbyte(0x02, "bulk transfer type");
			desc_addshort(epcfg->maxpktsize, "wMaxPacketSize");
			desc_addbyte(epcfg->interval, "ep interval");
			//desc_addshort(0x200, "wMaxPacketSize");
			//desc_addbyte(0x1, "ep interval");
			desc_endsection();
		}else {
skipframe:
			desc_startsection("standard VS Interface Descriptor(alt 1)", 0x04);
			desc_addbyte(cfg->vstrmif[i].STRMIF_ID, "index of this interface");
			desc_addbyte(0x01, "index of this alternate setting");
			desc_addbyte(0x01, "number of endpoint");
			desc_addbyte(0x0e, "cc_video, interface class");
			desc_addbyte(0x02, "sc_videostreaming, subclass");
			desc_addbyte(0x00, "pc_protocol_undefined");
			desc_addbyte(0x00, "unused");
			desc_endsection();

			desc_startsection("standard vs iso video data endpoint descriptor", 0x05);
			desc_addbyte(cfg->vstrmif[i].STRM_EP, "endpoint address");
			desc_addbyte(0x05, "iso transfer type");
			desc_addshort(epcfg->maxpktsize, "wMaxPacketSize");
			desc_addbyte(epcfg->interval, "ep interval"); 
			desc_endsection();
		}
	}
}

static void gen_conf_actrl(t_libiad_cfg * cfg, t_libusbclass_cfg *usbcfg)
{
	int i,j;
	int cs_size_offset;
	
	if(cfg->actrlif->tid_cnt != cfg->astrmif_cnt) {
		printf_error("//#error audio control tid not match stream descriptor. %d -- %d \n", cfg->actrlif->tid_cnt, cfg->astrmif_cnt);
		return;
	}

	desc_startsection("Standard Audio Interface Collection IAD", 0x0b);
	desc_addbyte(cfg->actrlif->CTRLIF_ID, "First interface number");
	desc_addbyte(cfg->astrmif_cnt + 1, "Interface count");
	desc_addbyte(0x01, "Function class");
	desc_addbyte(0x01, "Function subclass");
	desc_addbyte(0x00, "not used");
	desc_addbyte(0x03, "interface string descriptor index");
	desc_endsection();

	desc_startsection("Standard AC Interface Descriptor", 0x04);
	desc_addbyte(cfg->actrlif->CTRLIF_ID, "interface number");
	desc_addbyte(0x00, "index of the alternate setting");
	desc_addbyte(0x00, "number of endpoint");
	desc_addbyte(0x01, "interface class");
	desc_addbyte(0x01, "interface subclass");
	desc_addbyte(0x00, "interface protocol");
	desc_addbyte(0x03, "interface string descriptor index");
	desc_endsection();

	//start count CS_VC length
	desc_startcount();

	desc_startsection("Class-Specific AC Interface Descriptor", 0x24);
	desc_addbyte(0x01, "subtype:AC_HEADER");
	desc_addshort(0x0100, "revision of class specification -- uac1.0 spec");
	cs_size_offset = desc_cnt;
	desc_addshort(0x0000, "total size of class-specification descriptor");
	desc_addbyte(cfg->astrmif_cnt, "number of streaming interface");
	for(i=0; i<cfg->astrmif_cnt; i++){
		desc_addbyte(cfg->astrmif[i].STRMIF_ID, "streaming interface id");
	}
	desc_endsection();

	///////////////////////////////////////////////////////////////////////////////////////////////////////////
	u8 tmp_id[2][5];
	if(cfg->actrlif->tid_cnt > 5) {
		printf_error("//#error temp array size is < 5   -- tid_cnt: %d\n", cfg->actrlif->tid_cnt);
		return;
	}
	for(i=0; i<cfg->actrlif->tid_cnt; i++) {
		tmp_id[0][i] = cfg->actrlif->tid[i].IT_ID;
		tmp_id[1][i] = cfg->actrlif->tid[i].OT_ID;
	}
	for(i=0; i<cfg->actrlif->tid_cnt; i++) {
		for(j=i+1; j<cfg->actrlif->tid_cnt; j++) {
			if(cfg->actrlif->tid[i].IT_ID == tmp_id[0][j])	{
				tmp_id[0][j] = 0;
			}
			if(cfg->actrlif->tid[i].OT_ID == tmp_id[1][j])	{
				tmp_id[1][j] = 0;
			}
		}
	}
	//note: cfg->actrlif->tid_cnt "must" == cfg->strmif_cnt
	u8 is_mic,index=0;
	for(i=0; i<cfg->actrlif->tid_cnt; i++) {
		if(cfg->astrmif[i].STRM_APP == AUDIO_STREAM_APP_MIC) {
			is_mic = 1;
			index = i;
		}else if(cfg->astrmif[i].STRM_APP == AUDIO_STREAM_APP_SPK) {
			is_mic = 0;
		}else {
			printf_error("\n\n//################ error t_libuav_cfg astrmif application type not be indicate! ################\n\n");
			return;
		}

		//for tid struct, same IT_ID Value only use one IT descriptor
		if(tmp_id[0][i] != 0) {
			desc_startsection("Input Terminal Descriptor(Microphone)", 0x24);
			desc_addbyte(0x02, "subtype:AC_INPUT_TERMINAL");
			desc_addbyte(tmp_id[0][i], "terminal ID");
			desc_addshort(is_mic ? 0x0201:0x0101, is_mic ? "Terminal Type:Microphone":"tt_streaming terminal type");
			desc_addbyte(0x00, "no association");
			desc_addbyte(cfg->astrmif[i].chn_cnt, "Number of logical channel in the cluster"); //TODO
			desc_addshort(0x0300, "Mono sets no position bits(0x01,0x00)   for pass test");
			desc_addbyte(0x00, "No none-predefined Channel");
			desc_addbyte(0x00, "string index"); //TODO
			desc_endsection();
		}
		
		//for tid struct, same OT_ID Value only use one OT descriptor
		if(tmp_id[1][i] != 0) {
			desc_startsection("Ouput Terminal Descriptor", 0x24);
			desc_addbyte(0x03, "subtype:AC_OUTPUT_TERMINAL");
			desc_addbyte(tmp_id[1][i], "terminal ID");
			desc_addshort(is_mic ? 0x0101:0x0301, is_mic ? "tt_streaming terminal type":"Terminal Type:Speaker");
			desc_addbyte(0x00, "association terminal");
			desc_addbyte(is_mic ? cfg->actrlif->FU_ID_MIN:tmp_id[0][i], "source ID"); 
			desc_addbyte(0x00, "string index"); //TODO
			desc_endsection();
		}
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////

	desc_startsection("Feature unit Descriptor", 0x24);
	desc_addbyte(0x06, "subtype:AC_FEATURE_UNIT");
	desc_addbyte(cfg->actrlif->FU_ID_MIN, "Unit ID");
	desc_addbyte(cfg->actrlif->tid[index].IT_ID, "source ID");
	desc_addbyte(0x02, "control size");
	desc_addshort(0x0003, "bmaControls");
	desc_addbyte(0x00, "string index"); //TODO
	desc_endsection();

	//change CS_VC length
	desc_changeshort(cs_size_offset, desc_endcount());
}

static void gen_conf_astrm(t_libiad_cfg * cfg, t_libusbclass_cfg *usbcfg)
{
	int i,j;

	for(i=0; i<cfg->astrmif_cnt; i++){
		desc_startsection("standard AS Interface Descriptor(alt 0)", 0x04);
		desc_addbyte(cfg->astrmif[i].STRMIF_ID, "index of this interface");
		desc_addbyte(0x00, "index of this alternate setting");
		desc_addbyte(0x00, "number of endpoint");
		desc_addbyte(0x01, "cc_audio, interface class");
		desc_addbyte(0x02, "sc_audiostreaming, subclass");
		desc_addbyte(0x00, "pc_protocol_undefined");
		desc_addbyte(0x00, "unused");
		desc_endsection();

		desc_startsection("standard AS Interface Descriptor(alt 1)", 0x04);
		desc_addbyte(cfg->astrmif[i].STRMIF_ID, "index of this interface");
		desc_addbyte(0x01, "index of this alternate setting");
		desc_addbyte(0x01, "number of endpoint");
		desc_addbyte(0x01, "cc_audio, interface class");
		desc_addbyte(0x02, "sc_audiostreaming, subclass");
		desc_addbyte(0x00, "pc_protocol_undefined");
		desc_addbyte(0x00, "unused");
		desc_endsection();

		desc_startsection("Class-Specific AS Interface Descriptor", 0x24);
		desc_addbyte(UAC_AS_GENERAL, "subtype:AS_GENERAL");
		desc_addbyte(cfg->astrmif[i].STRMLINK_ID, "Terminal Link Number");
		desc_addbyte(0x01, "Interface delay");
		desc_addshort(cfg->astrmif[i].format, "Format:PCM/PCM8/MPEG");
		desc_endsection();

		desc_startsection("Class-Specific AS Format Descriptor", 0x24);
		desc_addbyte(UAC_FORMAT_TYPE, "subtype:AS_FORMAT");
		if(cfg->astrmif[i].format == UAC_FORMAT_TYPE_II_MPEG) {//m_p_3 format 
			desc_addbyte(UAC_FORMAT_TYPE_II, "subtype:FORMAT_TYPE_II");
			desc_addshort(320, "max bit rate per second");
			desc_addshort(1152, "sample numbers per frame");
		}else {//p_c_m 
			desc_addbyte(UAC_FORMAT_TYPE_I, "subtype:FORMAT_TYPE_I");
			desc_addbyte(cfg->astrmif[i].chn_cnt, "Number of physical channel in the audio data stream");
			desc_addbyte(0x02, "Number of SubframeSize(1 to 4)0x02");
			desc_addbyte(0x10, "Sample Resoultion:16 bits Resoultion");
		}
		desc_addbyte(cfg->astrmif[i].sr_cnt, "Number availd of Sampling Rate Frequence");
		for(j=0; j < cfg->astrmif[i].sr_cnt; j++){
			desc_addbyte(cfg->astrmif[i].sr_all[j] & 0xff, "KHz");
			desc_addbyte((cfg->astrmif[i].sr_all[j] >> 8) & 0xff, NULL);
			desc_addbyte((cfg->astrmif[i].sr_all[j] >> 16) & 0xff, NULL);
		}
		desc_endsection();

		if(cfg->astrmif[i].format == UAC_FORMAT_TYPE_II_MPEG) {//m_p_3 format 
			desc_startsection("MPEG Format-Specific Descriptor", 0x24);
			desc_addbyte(UAC_FORMAT_SPECIFIC, "subtype: AS_FORMAT_SPECIFIC");	
			desc_addshort(UAC_FORMAT_TYPE_II_MPEG, "Format: MPEG");
			desc_addshort(0x000c, "MPEG Capabilities bitmap");
			desc_addbyte(0x00, "MPEG feature bitmap");
			desc_endsection();
		}

		t_libusbep_cfg *epcfg = find_epcfg(usbcfg, cfg->astrmif[i].STRM_EP);
		if(epcfg == NULL){
			printf_error("//#error Cannot find audio epcfg:%x\n", cfg->astrmif[i].STRM_EP);
			return;
		}

		desc_startsection("standard as iso audio data endpoint descriptor", 0x05);
		desc_addbyte(cfg->astrmif[i].STRM_EP, "endpoint address");
		desc_addbyte(0x05, "iso transfer type");
		desc_addshort(epcfg->maxpktsize, "MaxPacketSize");
		desc_addbyte(epcfg->interval, "ep interval"); 
		desc_addbyte(0x00, "Refresh");
		desc_addbyte(0x00, "SyncAddress");
		desc_endsection();

		desc_startsection("Class-Specific AS Isochronous Audio Data Endpoint Descriptor", 0x25);
		desc_addbyte(0x01, "subtype:EP_GENERAL");
		desc_addbyte((cfg->astrmif[i].sr_cnt > 1) ? 0x01:0x00, "Attribute:Sampling Frequence");
		desc_addbyte(0x00, "LockdelayUnit:Undefined");
		desc_addshort(0x0000, "Number of LockDelay");
		desc_endsection();
	}
}

static void gen_conf_vendor_msc(t_libvmsc_cfg *msccfg, t_libusbclass_cfg *usbcfg)
{
	t_libusbep_cfg *epcfg;

	desc_startsection("vendor msc Interface Descriptor", 0x04);
	desc_addbyte(msccfg->MSCIF_ID, "index of this interface");
	desc_addbyte(0x00, "index of this alternate setting");
	desc_addbyte(0x02, "number of endpoint");
	desc_addbyte(0xff, "vendor interface class");
	desc_addbyte(0xff, "vendor subclass");
	desc_addbyte(0xff, "vendor_protocol_undefined");
	desc_addbyte(0x00, "unused");
	desc_endsection();

	epcfg = find_epcfg(usbcfg, msccfg->ep_in);
	if(epcfg == NULL){
		printf_error("//error Cannot find video epcfg:%d\n", msccfg->ep_in);
		return;
	}
	desc_startsection("vendor msc data endpoint descriptor", 0x05);
	desc_addbyte(msccfg->ep_in, "endpoint address");
	desc_addbyte(0x02, "bulk transfer type");
	desc_addshort(epcfg->maxpktsize, "wMaxPacketSize");
	desc_addbyte(epcfg->interval, "ep interval"); 
	desc_endsection();

	epcfg = find_epcfg(usbcfg, msccfg->ep_out);
	if(epcfg == NULL){
		printf_error("//error Cannot find video epcfg:%d\n", msccfg->ep_out);
		return;
	}
	desc_startsection("vendor msc data endpoint descriptor", 0x05);
	desc_addbyte(msccfg->ep_out, "endpoint address");
	desc_addbyte(0x02, "bulk transfer type");
	desc_addshort(epcfg->maxpktsize, "wMaxPacketSize");
	desc_addbyte(epcfg->interval, "ep interval"); 
	desc_endsection();
}

static void gen_conf(t_libusbclass_cfg * usbcfg)
{
	int k;
	t_libuav_cfg *cfg;
	u8 if_cnt = 0;

	pu_cnt = 0;
	if(usbcfg == NULL){
		printf_error("//#error g_libusbclass_cfg is NULL.\n");
		return;
	}

	cfg = (t_libuav_cfg *)(usbcfg->arg);
	if(cfg == NULL) {
		printf_error("//#error g_libusbclass_cfg_XX->arg is NULL, not found g_libuav_cfg struct.\n");
		return;
	}

	for(k=0; k<cfg->iad_cnt; k++) {
		if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			if_cnt += cfg->iad[k].vstrmif_cnt + (cfg->iad[k].vctrlif ? 1 : 0);
		}
		else if(cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
			if_cnt += cfg->iad[k].astrmif_cnt + (cfg->iad[k].actrlif ? 1 : 0);
		}
	}

	if(cfg->extra_cfg) {
		if_cnt++;
	}
	if(if_cnt == 0) {
		printf_error("//#error number of interface must be > 0 !\n");
		return;
	}

	desc_startsection("Configuration Descriptor", 0x02);
	desc_addshort(0x0000, "Total Length");
	desc_addbyte(if_cnt, "Number of Interface");
	desc_addbyte(0x01, "Configuration Value");
	desc_addbyte(0x00, "String : Configuration index");
	desc_addbyte(0x80, "Attributes: bus powered");
	desc_addbyte(0x80, "Maximal power: 256mA");
	desc_endsection();

	for(k=0; k<cfg->iad_cnt; k++) {
		if(cfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
			gen_conf_vctrl(cfg->iad + k, usbcfg);
			gen_conf_vstrm(cfg->iad + k, usbcfg);
		}
		else if(cfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
			gen_conf_actrl(cfg->iad + k, NULL);
			gen_conf_astrm(cfg->iad + k, usbcfg);
		}
	}

	if(cfg->extra_cfg) {
		gen_conf_vendor_msc((t_libvmsc_cfg *)cfg->extra_cfg, usbcfg);
	}

	return;
}

#if 0
static void print_buf(char *name, unsigned char *buf, int len)
{
	int i;

	printf_info("//%s\n", name);
	printf_info("u8 %s[%d] __attribute__((aligned(4))) = {\n", name, (len + 3) & (~3) );
	if(bigendian){
		for(i=0; i<len; i+=4){
			printf_info("	0x%02x, \n", buf[i+3] );
			printf_info("	0x%02x, \n", buf[i+2] );
			printf_info("	0x%02x, \n", buf[i+1] );
			printf_info("	0x%02x, \n", buf[i+0] );
		}
	}else{
		for(i=0; i<len; i+=4){
			printf_info("	0x%02x, \n", buf[i+0] );
			printf_info("	0x%02x, \n", buf[i+1] );
			printf_info("	0x%02x, \n", buf[i+2] );
			printf_info("	0x%02x, \n", buf[i+3] );
		}
	}
	printf_info("};\n\n");
}
#endif

static void print_desc_string(char * orig, char * name)
{
	int i;
	unsigned char * buf;
	int len = strlen(orig);

	buf = malloc(len*2+2 + 3);
	if(buf == NULL){
		printf_error("cannot alloc string\n");
	}
	memset(buf, 0, len*2+5);

	buf[0] = len*2+2;
	buf[1] = 3;
	for(i=0; i<len; i++){
		buf[i*2+2] = orig[i];
		buf[i*2+3] = 0;
	}

	len = len * 2 + 2;

	printf_info("//%s\n", name);
	printf_info("u32 %s[%d] = {\n", name, ((len + 3) & (~3)) / 4 );

	if(bigendian){
		for(i=0; i<len; i+=4){
			printf_info("	0x%02x%02x%02x%02x, \n", buf[i+0] , buf[i+1], buf[i+2], buf[i+3] );
		}
	}else{
		for(i=0; i<len; i+=4){
			printf_info("	0x%02x%02x%02x%02x, \n", buf[i+3] , buf[i+2], buf[i+1], buf[i+0] );
		}
	}
	printf_info("};\n\n");

	free(buf);
}

static void reset_desc_buf(void)
{
	int i;

	for(i=0; i<MAX_DESC_LEN; i++){
		if(desc_buf_str[i] != NULL){
			free(desc_buf_str[i]);
			desc_buf_str[i] = NULL;
		}
		if(desc_section_str[i] != NULL){
			free(desc_section_str[i]);
			desc_section_str[i] = NULL;
		}
	}
	memset(desc_buf, 0, sizeof(desc_buf));
	desc_cnt = 0;
}

static void print_desc_buf(char *name)
{
	printf_info("//%s\n", name);
	
	if(desc_cnt == 0){
		printf_info("u8 %s[4] __attribute__((aligned(4))) = {\n", name);
		printf_info("	0x%02x, \n", desc_cnt );
		printf_info("	0x%02x, \n", desc_cnt );
		printf_info("	0x%02x, \n", desc_cnt );
		printf_info("	0x%02x, \n", desc_cnt );
	}else{
		printf_info("u8 %s[%d] __attribute__((aligned(4))) = {\n", name, (desc_cnt + 3) & (~3) );
		desc_print();
	}
	printf_info("};\n\n");

	reset_desc_buf();
}

#if 0
static void gen_device_desc(unsigned short spec, unsigned char maxpktsize)
{
	desc_startsection("Device_Descriptor", 0x01);
	desc_addshort(spec, "Specification Version");
	desc_addbyte(0xef, "Device class");
	desc_addbyte(0x02, "Device sub-class");
	desc_addbyte(0x01, "Device sub-sub-class");
	desc_addbyte(maxpktsize, "Maximum packet size(512)");
	desc_addshort(0x05a9, "Vendor ID");
	desc_addshort(0x058a, "Product ID");
	desc_addshort(0x0100, "Product version");
	desc_addbyte(0x01, "Manufacturer string index");
	desc_addbyte(0x02, "Product string index");
	desc_addbyte(0x00, "Serial number string index");
	desc_addbyte(0x01, "Number of possible Configurations.");
	desc_endsection();
}

static void gen_qualifer_desc(unsigned short spec, unsigned char maxpktsize)
{
	desc_startsection("Device_Qualifer_Descriptor", 0x06);
	desc_addshort(spec, "Specification Version");
	desc_addbyte(0xef, "Qualifer class");
	desc_addbyte(0x02, "Qualifer sub-class");
	desc_addbyte(0x01, "Qualifer Protocol Code");
	desc_addbyte(maxpktsize, "Maximum packet size(64)");
	desc_addbyte(0x01, "Number of Other-speed Configurations");
	desc_addbyte(0x00, "Reserverd for future");
	desc_endsection();
}

#endif
static u8 get_interface_num(u8 ep, t_libusbep_cfg *epcfg, t_libusbclass_cfg *usbcfg)
{
	u8 i,k;
	t_libuav_cfg *uavcfg = (t_libuav_cfg *)(usbcfg->arg);
	t_libvmsc_cfg *vmsccfg = (t_libvmsc_cfg *)uavcfg->extra_cfg;

	//if((epcfg->type & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK) {
	if(((epcfg->type & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_BULK) && (vmsccfg != NULL)) {
		return vmsccfg->MSCIF_ID;
	} else {
		for(k=0; k<uavcfg->iad_cnt; k++) {
			if(uavcfg->iad[k].iad_app_type == USB_IAD_APP_VIDEO) {
				for(i=0; i<uavcfg->iad[k].vstrmif_cnt; i++) {
					if(ep == uavcfg->iad[k].vstrmif[i].STRM_EP) {
						return uavcfg->iad[k].vstrmif[i].STRMIF_ID;
					}
				}
			}
			if(uavcfg->iad[k].iad_app_type == USB_IAD_APP_AUDIO) {
				for(i=0; i<uavcfg->iad[k].astrmif_cnt; i++) {
					if(ep == uavcfg->iad[k].astrmif[i].STRM_EP) {
						return uavcfg->iad[k].astrmif[i].STRMIF_ID;
					}
				}
			}
		}
	}

	return 0;
}

static void gen_csrdata(t_libusbclass_cfg *usbcfg)
{
#define EP_ORIGINAL_DATA_ISO	0x00008080 
#define EP_ORIGINAL_DATA_OTHER	0x00000080 
	t_libusbep_cfg * epcfg ;
	u32 ep_data, ep_addr;
	u8 i;

	printf_info("\nt_csrdata g_csrdata[] = {\n");
	printf_info(" {0x00000000, 0x40002000, 0x0000000c },\n");
	printf_info(" {0x00000001, 0x50000000, 0x0000000c },\n");
	printf_info(" {0x00000002, 0x02000080, 0x0000000c },\n");
	printf_info(" {0x00030003, 0x00000000, 0x0000000c },\n");
	for(i=0; i<usbcfg->epnum; i++) {
		ep_addr = 0x00030004 + i;

		epcfg = &usbcfg->epcfg[i];
		ep_data = (((epcfg->type&USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_ISOC) ? EP_ORIGINAL_DATA_ISO : EP_ORIGINAL_DATA_OTHER) | (epcfg->ep & USB_ENDPOINT_NUMBER_MASK) | ((epcfg->ep & USB_ENDPOINT_DIR_MASK)?0x10:0x00)	\
				  | (epcfg->type<<5) | ((get_interface_num(epcfg->ep, epcfg, usbcfg))<<11) | ((epcfg->maxpktsize & 0x7ff)<<19); 
		g_csrdata[i+4].addr = ep_addr;
		g_csrdata[i+4].data = ep_data;
		g_csrdata[i+4].ctrl = 0x0000000c;
		printf_info(" {0x%08x, 0x%08x, 0x0000000c },\n", ep_addr, ep_data);
	}
	g_csrdata[i+4].addr = 0x00;
	g_csrdata[i+4].data = 0x00;
	g_csrdata[i+4].ctrl = 0x30;
	printf_info(" {0x00000000, 0x00000000, 0x00000030 }\n");
	printf_info("};\n\n");
}

#if 0
	{0x00000000, 0x40002000, 0x0000000c },	//[31:16] device descriptor addr Pointer
											 //[15: 0] setup command addr pointer
	//-------------------------------------------------------------------------------
	{0x00000001, 0x50000000, 0x0000000c }, //[31:16] qualifier descriptor address pointer
											//[15: 0] reserved
	//-------------------------------------------------------------------------------
	{0x00000002, 0x02000080, 0x0000000c }, //[31:30] reserved for non-iso in endpoint
											//[29:19] maxpktsize
											//[18:15] alterate setting to which this endpoint belongs
											//[14:11] interface number to which this endpoint belongs
											//[10: 7] configuration number to which this endpoint belongs 
											//[ 6: 5] endpoint type;00 ctrl,01 iso,10 bulk,11 interrupt
											//[	4] endpoint direction 0:out, 1 in
											//[ 3: 0] endpoint number
	//-------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------
	{0x00030003, 0x000088b0 | (UVC_STREAM_ENDPOINT & EPNUM_MASK)|((HS_PYLSIZE_YUV & 0x7ff)<<19), 0x0000000c}, //iso in 1020
	//-------------------------------------------------------------------------------
	{0x00030004, 0x000098b0 | (UAC_STREAM_ENDPOINT & EPNUM_MASK)|(HS_MAX_PKTSIZE_ISO<<19), 0x0000000c }, //iso in 512
	//-------------------------------------------------------------------------------
	{0x00030005, 0x000000f0 | (UVC_STATUS_ENDPOINT & EPNUM_MASK)|((HS_MAX_PKTSIZE_INTR)<<19), 0x0000000c },
	//-------------------------------------------------------------------------------
	{0x00000000, 0x00000000, 0x00000030 } //end flag
#endif

//is_hs: '1' is usb2.0, '0' is usb1.1
static void gen_cp_paramdata(u8 is_hs)
{
	int i,j;

	if(pu_cnt == 0) {
		printf_info("t_uvc_cp_param g_uvc_cp_param_%s;\n", (is_hs ? "hs":"fs"));
		return;
	}

	for(i=0; i<pu_cnt; i++) {
		printf_info("u16 __uvc_it_start_%s_%d[] = \n", (is_hs ? "hs":"fs"), i);
		printf_info("{\n");
		for(j=0; j<it_cnt; j++) {
			printf_info("	%d,\n", it_start[i][j]);
		}
		printf_info("};\n\n");
	}

	printf_info("t_uvc_cp_offset __uvc_cp_offset_%s[] = \n", (is_hs ? "hs":"fs"));
	printf_info("{\n");
	for(i=0; i<pu_cnt; i++) {
		printf_info("	{\n");
		printf_info("		.pu_start = %d,\n", pu_start[i]);
		printf_info("		.it_start = &__uvc_it_start_%s_%d[0],\n", (is_hs ? "hs":"fs"), i);
		printf_info("		.it_cnt = sizeof(__uvc_it_start_%s_%d)/sizeof(__uvc_it_start_%s_%d[0]),\n", (is_hs ? "hs":"fs"), i, (is_hs ? "hs":"fs"), i);
		printf_info("	},\n");
	}
	printf_info("};\n\n");
			
	printf_info("t_uvc_cp_param g_uvc_cp_param_%s = \n", (is_hs ? "hs":"fs"));
	printf_info("{\n");
	printf_info("	.offset = &__uvc_cp_offset_%s[0],\n", (is_hs ? "hs":"fs"));
	printf_info("	.offset_cnt = sizeof(__uvc_cp_offset_%s)/sizeof(__uvc_cp_offset_%s[0]),\n", (is_hs ? "hs":"fs"), (is_hs ? "hs":"fs"));
	printf_info("};\n\n");
}

#if 0
static void gen_ep_cfg(t_libusbclass_cfg *usbcfg)
{
	t_libusbep_cfg *epcfg;

	printf_info("t_libusbep_cfg libusbep_cfg_hs[] = {");
}
#endif

int desc_main(void)
{
	int i;

	printf_info("++desc_main\n");
	for(i=0; i<MAX_DESC_LEN; i++){
		desc_buf_str[i] = NULL;
		desc_section_str[i] = NULL;
	}
	desc_cnt = 0;

#if 0
	printf_info("/* This file is auto-generated, Don't edit */\n\n");

	printf_info("#include \"includes.h\"\n");
	printf_info("#include \"libusb.h\"\n");
#endif
	printf_info("/* <note: %d, bigendian: %d> */\n", comment, bigendian);
	printf_info("\n\n");
	
	uvc_framecnt_fix();

	gen_conf(&g_libusbclass_cfg_hs);
	memcpy(cfgConf_HI, desc_buf, desc_cnt); 
	cfgConf_HI[2] = desc_cnt & 0xff;
	cfgConf_HI[3] = (desc_cnt >> 8) & 0xff;
	//printf_info("HI data: %x %x\n", *(u32 *)cfgConf_HI, *(u32 *)(cfgConf_HI+4));

	print_desc_buf("cfgConf_HI");
	
	gen_cp_paramdata(1);
 	
	gen_conf(&g_libusbclass_cfg_fs);
	memcpy(cfgConf_FL, desc_buf, desc_cnt); 
	cfgConf_FL[2] = desc_cnt & 0xff;
	cfgConf_FL[3] = (desc_cnt >> 8) & 0xff;
	//printf_info("FL data: %x %x\n", *(u32 *)cfgConf_FL, *(u32 *)(cfgConf_FL+4));
	print_desc_buf("cfgConf_FL");
 	
	gen_cp_paramdata(0);

	printf_info("\n");

	//TODO
	if(bigendian){
		printf_info("u32 cfgDevice_HI [] = { 0x12010002, 0xef020140, 0xa9058378, 0x00010102, 0x0001 };\n");
		printf_info("u32 cfgDevice_FL [] = { 0x12011001, 0xef020140, 0xa9058378, 0x00010102, 0x0001 };\n");
	}else{
		printf_info("u32 cfgDevice_HI [] = { 0x02000112, 0x400102ef, 0x788305a9, 0x02010100, 0x0100 };\n");
		printf_info("u32 cfgDevice_FL [] = { 0x01100112, 0x400102ef, 0x788305a9, 0x02010100, 0x0100 };\n");
	}

	//TODO
	if(bigendian){
		printf_info("u32 cfgQualifier [] = { 0x0a060002, 0xef020140, 0x0100 }; \n");
	}else{
		printf_info("u32 cfgQualifier [] = { 0x0200060a, 0x400102ef, 0x0001 }; \n");
	}

	gen_csrdata(&g_libusbclass_cfg_hs);

	print_desc_string(_Vendor_String, "cfgStr1");
	print_desc_string(_Product_String_Video, "cfgStr2");
	print_desc_string(_Product_String_Audio, "cfgStr3");

	if(bigendian){
		printf_info("u32 cfgStr0 [] = { 0x04030904 };\n");
	}else{
		printf_info("u32 cfgStr0 [] = { 0x04090304 };\n");
	}
	printf_info("\n");

	return 0;
}
