#include "includes.h"

void handlereq_standard(void);

void libusb_handlereq(t_udev_ctrl_stage stage)
{
	if( stage == UDEV_SETUP_STAGE )
	{
		g_libusb_cfg.req.last_errcode = g_libusb_cfg.req.error_code;
		g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;

#ifdef USB_UART_PRINT
#if 1
		u32 tmp = (u32)g_libusb_cfg.req.databuf;
		debug_printf("Setup: %x %x\n", ReadReg32(tmp), ReadReg32(tmp+4));
#else
		debug_printf("Setup: %x %x\n", ReadReg32((u32)g_libusb_cfg.req.databuf), ReadReg32((u32)(g_libusb_cfg.req.databuf+4)) );
#endif
#endif

		g_libusb_cfg.req.request.bmRequestType	= g_libusb_cfg.req.databuf[0];
		g_libusb_cfg.req.request.bRequest	= g_libusb_cfg.req.databuf[1];
		g_libusb_cfg.req.request.wValue_L	= g_libusb_cfg.req.databuf[2];
		g_libusb_cfg.req.request.wValue_H	= g_libusb_cfg.req.databuf[3];
		g_libusb_cfg.req.request.wIndex_L	= g_libusb_cfg.req.databuf[4];
		g_libusb_cfg.req.request.wIndex_H	= g_libusb_cfg.req.databuf[5];
		g_libusb_cfg.req.request.wLength_L	= g_libusb_cfg.req.databuf[6];
		g_libusb_cfg.req.request.wLength_H	= g_libusb_cfg.req.databuf[7];


		g_libusb_cfg.req.wLength =(g_libusb_cfg.req.request.wLength_H << 8) + g_libusb_cfg.req.request.wLength_L;

		//wait for data packet
		if(DATA_FROM_HOST)
		{
			//Host-to-device
			if(g_libusb_cfg.req.wLength == 0)
			{
#ifdef USB_UART_PRINT
				debug_printf("Host2Device with wLength = 0\n");
#endif
				if((g_libusb_cfg.req.request.bmRequestType & USB_TYPE_MASK) == USB_TYPE_CLASS) {
					usbclass_HandleRequest();
				}

				udcif_request_sendack(SEND_ACK_NEXTDATA | SEND_ACK_NEXTSTATUS, 1);
			}
			else{
				//waiting for data packet ready
				if(g_libusb_cfg.req.wLength > MAX_REQBUF_SIZE){
#ifdef USB_UART_PRINT
					debug_printf("req.databuf not enough\n");
					udcif_request_sendack(SEND_STALL_NEXTDATA | SEND_STALL_NEXTSTATUS, 0);
					return;
#endif
				}
		
				udcif_request_sendack(SEND_ACK_NEXTDATA, 1);
			}
			return;
		}else{
			g_libusb_cfg.req.buffer = (u32 *)g_libusb_cfg.req.databuf;
		}
	}

	if((DATA_TO_HOST && stage == UDEV_SETUP_STAGE ) || (DATA_FROM_HOST && stage == UDEV_DATA_STAGE)){
		if(g_libusb_cfg.req.HandleRequest_cb){
			if(g_libusb_cfg.req.HandleRequest_cb() != -1){
				goto req_handled;
			}
		}

		//handle request
		switch(g_libusb_cfg.req.request.bmRequestType & USB_TYPE_MASK) {
			case USB_TYPE_STANDARD:
				handlereq_standard();
				break;
			case USB_TYPE_CLASS:
				usbclass_HandleRequest();
				break;
			case USB_TYPE_VENDOR:
				//VENDOR request should be handled through HandleRequest_cb()
				debug_printf("Vendor request\n");
				break;
			default:
				debug_printf("Reserved request\n");
				break;
		}
	}

req_handled:
	if(g_libusb_cfg.req.error_code != UDEV_NO_ERROR)
	{
		//if error, STALL
#ifdef USB_UART_PRINT
		debug_printf("request STALL\n");
#endif
		if(DATA_FROM_HOST)	{
			udcif_request_sendack(SEND_STALL_NEXTSTATUS, 0);
		}
		else{
			udcif_request_sendack(SEND_STALL_NEXTDATA, 0);
		}
		return;
	}

	//send data packet
	if( DATA_TO_HOST && g_libusb_cfg.req.wLength != 0 )
	{
		if( g_libusb_cfg.req.wLength > MAX_EP0_PKTSIZE )
		{
			udc_writeEP0(g_libusb_cfg.req.buffer, MAX_EP0_PKTSIZE, 0);
			g_libusb_cfg.req.buffer += MAX_EP0_PKTSIZE/4;
			g_libusb_cfg.req.wLength -= MAX_EP0_PKTSIZE;

			udcif_request_sendack(SEND_ACK_NEXTDATA, 0);
		}
		else {
			udc_writeEP0(g_libusb_cfg.req.buffer, g_libusb_cfg.req.wLength, 0);
			g_libusb_cfg.req.wLength = 0;

#if 1
			udcif_request_sendack(SEND_ACK_NEXTDATA, 1);
#else
			udcif_request_sendack(SEND_ACK_NEXTDATA | SEND_ACK_ALLDATA, 0);
			{volatile int d; for(d=0; d<200; d++); }
			udcif_request_sendack(SEND_ACK_NEXTSTATUS, 0);
#endif
		}
	}else {
		//ACK to data from host
#if 1
		//udcif_request_sendack(SEND_ACK_NEXTDATA | SEND_ACK_NEXTSTATUS, 1);
		udcif_request_sendack(SEND_ACK_NEXTSTATUS, 1);
#else
		udcif_request_sendack(SEND_ACK_NEXTDATA, 0);
		{volatile int d; for(d=0; d<200; d++); }
		udcif_request_sendack(SEND_ACK_NEXTSTATUS, 0);
#endif
		WriteReg32(UDCIF_CTRL_CFG, ReadReg32(UDCIF_CTRL_CFG)|BIT16);
		//debug_printf("out: %x\n", ReadReg32(UDCIF_CTRL_CTRL));
	}
}
