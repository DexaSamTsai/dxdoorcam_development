#include "includes.h"

void libusb_swap_char(unsigned char *pbuf, int len)
{
	int i;
	unsigned char tmp, tmp1;

	for(i=0; i<len; i+=4) {
		tmp = pbuf[i+3];
		tmp1 = pbuf[i+2];
		pbuf[i+3] = pbuf[i];
		pbuf[i+2] = pbuf[i+1];
		pbuf[i+1] = tmp1;
		pbuf[i] = tmp;
	}
}

void libusb_dump_data(unsigned char *pbuf, int cnt)
{
	int i;

	debug_printf("Dump Data(0x%x): %x\n", pbuf, cnt);
	for(i=0; i<cnt; i++){
		debug_printf(" %x", *(pbuf+i));
		if((i & 0xf) == 0xf)
			debug_printf("\n");
	}
	debug_printf("\n");
}
