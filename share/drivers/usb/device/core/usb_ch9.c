#include "includes.h"

void handlereq_descriptor(void)
{
	u16 length;

	g_libusb_cfg.req.error_code = UDEV_NO_ERROR;

	switch(g_libusb_cfg.req.request.wValue_H) {
		case USB_DT_DEVICE:	// device
			g_libusb_cfg.req.buffer = (g_libusb_cfg.config & UDEVCFG_FLSPEED) ? cfgDevice_FL:cfgDevice_HI;
			g_libusb_cfg.req.wLength = (*g_libusb_cfg.req.buffer) & 0xff;
			break;
		case USB_DT_CONFIG: // configuration
			g_libusb_cfg.req.buffer = (g_libusb_cfg.bHighSpeed == 1) ? (u32 *)cfgConf_HI:(u32 *)cfgConf_FL;
			g_libusb_cfg.req.wLength = (ReadReg32(g_libusb_cfg.req.buffer)>>16)&0xffff;
			break;
		case USB_DT_STRING:	// string
			if(g_libusb_cfg.req.request.wValue_L == 0)
			    g_libusb_cfg.req.buffer = cfgStr0;
			else if(g_libusb_cfg.req.request.wValue_L == 1)
			    g_libusb_cfg.req.buffer = cfgStr1;
			else if(g_libusb_cfg.req.request.wValue_L == 2)
			    g_libusb_cfg.req.buffer = cfgStr2;
			else if(g_libusb_cfg.req.request.wValue_L == 3)
			    g_libusb_cfg.req.buffer = cfgStr3;

			g_libusb_cfg.req.wLength = (*g_libusb_cfg.req.buffer) & 0xff;
			break;
		case USB_DT_DEVICE_QUALIFIER:	// device qualifier
			g_libusb_cfg.req.buffer = cfgQualifier;
			g_libusb_cfg.req.wLength = (*g_libusb_cfg.req.buffer) & 0xff;
			break;
		case USB_DT_OTHER_SPEED_CONFIG: // other configuration
#ifdef USB_UART_PRINT
			debug_printf("other configuration\n");
#endif
			g_libusb_cfg.req.buffer = (u32 *)cfgConf_FL;
			g_libusb_cfg.req.wLength = (ReadReg8(g_libusb_cfg.req.buffer)<<8) | (ReadReg8( ((u8 *)g_libusb_cfg.req.buffer)+1));
			break;
		default:
			debug_printf("get_descriptor: err cfg\n");
			g_libusb_cfg.req.error_code = UDEV_INVALID_REQUEST;
			return;
	}

	length = (g_libusb_cfg.req.request.wLength_H << 8) + g_libusb_cfg.req.request.wLength_L;
	if( g_libusb_cfg.req.wLength > length )
	{
#ifdef USB_UART_PRINT
		debug_printf("Warning: GetDescriptor length(%x) > request.wLength(%x)\n", g_libusb_cfg.req.wLength, length);
#endif
		g_libusb_cfg.req.wLength = length;
	}
}

void handlereq_standard(void)
{
	if( (g_libusb_cfg.req.request.bmRequestType == 0x80) && (g_libusb_cfg.req.request.bRequest == 0x06) )
	{
		//handle GetDescriptor
		handlereq_descriptor();
	}else{
#ifdef USB_UART_PRINT
		debug_printf("Invalid Standard:%x,%x\n", g_libusb_cfg.req.request.bmRequestType, g_libusb_cfg.req.request.bRequest);
#endif
		g_libusb_cfg.req.error_code = UDEV_INVALID_CONTROL;
	}
}
