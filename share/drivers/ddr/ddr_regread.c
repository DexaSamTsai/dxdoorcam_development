#include "includes.h"

u32 ddr_regread(u32 reg)
{
	u32 flags;
	local_irq_save(flags);

	WriteReg32(DDRCTRL_BASE_ADDR+0x44, 0x00008104);                      // Trigger Register mode
	WriteReg32(DDRCTRL_BASE_ADDR+0x00, 0x00000004);               
	WriteReg32(DDRCTRL_BASE_ADDR+0x44, 0x0000C104);                      // Trigger MRR read
	WriteReg32(DDRCTRL_BASE_ADDR+0x44, 0x00008004);                      // Go back to normal
	WriteReg32(DDRCTRL_BASE_ADDR+0x00, 0x000000C3);               

	u32 ret = ReadReg32(DDRCTRL_BASE_ADDR+0xA4);

	local_irq_restore(flags);

	return ret;
}

u32 ddr_regread_cmdshell(int argc, char ** argv)
{
	debug_printf("MRR4: 0x%x", ddr_regread(0) );
	return 0;
}
