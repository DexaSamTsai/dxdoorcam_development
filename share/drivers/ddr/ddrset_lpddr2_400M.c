#include "includes.h"

int ddrset_lpddr2_400M(void)
{
    r3_ddrpll_set(
                   0,//int prediv, 
                   1,//int divsys,
                   4,//int divisp, 
                   16,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set pll1 to 360MHz
	

	DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25)); 

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 

	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x000000c3);          // const_emr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000006);          // const_emr2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x0C,0x00000001);          // const_emr3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x00000010);          // const_200us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x14,0x000007E0);          // const_400ns 
#ifdef ECO1
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000022);          // use 64MB
#else
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000020);          // use 32MB 
#endif
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000600);          // const_trfc_max 
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00041543);          // const_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x0000100A);          // const_para3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x38,0x0000860D);          // const_para4 
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x0004AA35);          // const_para5 
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x5000D263);          // const_para6 
	WriteReg32(DDRCTRL_BASE_ADDR+0x4C,0x0000000C);          // ddrc_control 
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000A);          // const_tfaw 
	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x0000AA00);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0xd8,0xffff0000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0xdc,0xffff0000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x000002ff);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x33cc001f);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x5555001f);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x168,0x380c2800);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x06060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]

	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i
	// Release DDR PHY Reset 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25)); 

	LPDDR2_WAIT_READY;

	return 0;
}

int ddrset_lpddr2_400M_remap(void)
{
    r3_ddrpll_set(
                   0,//int prediv, 
                   1,//int divsys,
                   4,//int divisp, 
                   16,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set pll1 to 360MHz
	

	DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25)); 

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 

	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x000000c3);          // const_emr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000006);          // const_emr2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x0C,0x00000001);          // const_emr3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x00000010);          // const_200us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x14,0x000007E0);          // const_400ns 
#ifdef ECO1
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000122);          // use 64MB
#else
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000020);          // use 32MB 
#endif
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000600);          // const_trfc_max 
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00041543);          // const_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x0000100A);          // const_para3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x38,0x0000860D);          // const_para4 
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x0004AA35);          // const_para5 
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x5000D263);          // const_para6 
	WriteReg32(DDRCTRL_BASE_ADDR+0x4C,0x0000000C);          // ddrc_control 
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000A);          // const_tfaw 
	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x0000AA00);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0xd8,0xffff0000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0xdc,0xffff0000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x000002ff);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x33cc001f);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x5555001f);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x168,0x380c2800);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x06060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]

	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i
	// Release DDR PHY Reset 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25)); 

	LPDDR2_WAIT_READY;

	return 0;
}

int ddrset_lpddr2_config(void)
{
    /*
     * r3_ddrpll_set( 0,
                        1,
                        4,
                        pdiv,
                        sdiv,
                        0,
                        0,
                        0,
                        0,);
        pdiv = 16 sdiv = 1      ddr clock = 396MHz
        pdiv = 17 sdiv = 1      ddr clock = 420MHz
        pdiv = 18 sdiv = 0      ddr clock = 432MHz
        pdiv = 18 sdiv = 1      ddr clock = 444MHz
        pdiv = 19 sdiv = 0      ddr clock = 456MHz
        pdiv = 19 sdiv = 1      ddr clock = 468MHz
        pdiv = 20 sdiv = 0      ddr clock = 480MHz
        pdiv = 20 sdiv = 1      ddr clock = 492MHz
        pdiv = 21 sdiv = 0      ddr clock = 504MHz
        pdiv = 21 sdiv = 1      ddr clock = 516MHz
        pdiv = 22 sdiv = 0      ddr clock = 528MHz
        pdiv = 22 sdiv = 1      ddr clock = 530MHz
     */
    int pdiv,sdiv;
    pdiv = 22;
    sdiv = 1;
    r3_ddrpll_set(
                   0,//int prediv,
                   1,//int divsys,
                   4,//int divisp,
                   pdiv,//int pdiv,
                   sdiv,//int sdiv,
                   0,//int bypass,
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set pll1 to 360MHz

    DDR_CLEAR_READY;

    // HOLD DDR PHY RESET
    WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25));

    // PROGRAM SETTINGS
    WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset

    WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x000000c3);          // const_emr
    WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000006);          // const_emr2
    WriteReg32(DDRCTRL_BASE_ADDR+0x0C,0x00000001);          // const_emr3
    WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x00000010);          // const_200us
    WriteReg32(DDRCTRL_BASE_ADDR+0x14,0x000007E0);          // const_400ns
#ifdef ECO1
    WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000022);          // use 64MB
#else
    WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000020);          // use 32MB
#endif
    WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000600);          // const_trfc_max
    WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00041543);          // const_para1
    WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x0000100A);          // const_para3
    WriteReg32(DDRCTRL_BASE_ADDR+0x38,0x0000860D);          // const_para4
    WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x0004AA35);          // const_para5
    WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x5000D263);          // const_para6
    WriteReg32(DDRCTRL_BASE_ADDR+0x4C,0x0000000C);          // ddrc_control
    WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000A);          // const_tfaw
    WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x0000AA00);          // const_500us
    WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // const_500us
    WriteReg32(DDRCTRL_BASE_ADDR+0xd8,0xffff0000);          // const_500us
    WriteReg32(DDRCTRL_BASE_ADDR+0xdc,0xffff0000);          // const_500us
    WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x000002ff);         // update PHY r_dfi_opt_rttn_i[3:0]
//  WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x33cc001f);         // update PHY r_dfi_opt_rttn_i[3:0]
    WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x5555001f);         // update PHY r_dfi_opt_rttn_i[3:0]
    WriteReg32(DDRCTRL_BASE_ADDR+0x168,0x380c2800);         // update PHY r_dfi_opt_rttn_i[3:0]
    WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x06060031); // update PHY r_zctrl_trim_vref[7:0]
    WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000); // update PHY r_zctrl_trim_vref[7:0]

    WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i
    // Release DDR PHY Reset
    WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25));

    LPDDR2_WAIT_READY;

    return 0;
}
