#include "includes.h"

int ddrset_ddr3_666M(void)
{
    r3_ddrpll_set(
                   0,//int prediv, 
                   1,//int divsys,
                   4,//int divisp, 
                   27,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set pll1 to 360MHz

	DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25)); 

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 
	WriteReg32(DDRCTRL_BASE_ADDR+0x00,0x00000A51);          // const_mr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x00000008);          // const_emr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000010);          // const_emr2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x00012000);          // const_200us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000015);          // mem_config 
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00005000);          // const_trfc_max 
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00043165);          // const_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x2C,0x00028113);          // const_para2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x00001111);          // const_para3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x000012B4);          // const_para5 
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x5000D293);          // const_para6 
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000E);          // const_tfaw 
	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x0002A000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x58,0x01000626);          // ddr3_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x00000037);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]

	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i 

	// Release DDR PHY Reset 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25)); 

	DDR3_WAIT_READY;

	return 0;
}
