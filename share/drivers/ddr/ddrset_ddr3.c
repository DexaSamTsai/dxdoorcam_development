#include "includes.h"

static const unsigned int ddr3_pattern[][5] = 
{
	{0xffff0000, 0xffff0000, 0x5555001f, 0x00000037, 0x00060031},
	{0x5555aaaa, 0x5555aaaa, 0xaa55001f, 0x00000037, 0x00060031},
	{0xaaaa5555, 0xaaaa5555, 0x55aa001f, 0x00000037, 0x00060031},
	{0x0000ffff, 0x0000ffff, 0xaaaa001f, 0x00000037, 0x00060031},
	{0xaaaa5555, 0x5555aaaa, 0x6699001f, 0x00000037, 0x00060031},
	{0x5555aaaa, 0xaaaa5555, 0x9966001f, 0x00000037, 0x00060031},
	{0xaaaa5555, 0xaaaa5555, 0x55aa001f, 0x00000037, 0x00060031},
	{0x5555aaaa, 0x5555aaaa, 0xaa55001f, 0x00000037, 0x00060031},
	{0xffff0000, 0xffff0000, 0x5555001f, 0x00000037, 0x00060031},
	{0x0000ffff, 0x0000ffff, 0xaaaa001f, 0x00000037, 0x00060031},
	{0xaaaa5555, 0x5555aaaa, 0x6699001f, 0x00000037, 0x00060031},
	{0x5555aaaa, 0xaaaa5555, 0x9966001f, 0x00000037, 0x00060031},
	{0xaaaa5555, 0xaaaa5555, 0x55aa001f, 0x00000037, 0x00060031},
};

int ddr3_retrain(void)
{
	int i;
	for(i=0; i <= sizeof(ddr3_pattern)/sizeof(ddr3_pattern[0]); i++){
		while((ReadReg32(REG_SC_STATUS) & BIT3) == 0); //wait ready
		if(ReadReg32(DDRCTRL_BASE_ADDR+0x190 ) == 0xbb){
			return 0;
		}

		if(i >= sizeof(ddr3_pattern)/sizeof(ddr3_pattern[0])){
			break;
		}

		WriteReg32(DDRCTRL_BASE_ADDR+0xd8, ddr3_pattern[i][0]);
		WriteReg32(DDRCTRL_BASE_ADDR+0xdc, ddr3_pattern[i][1]);
		WriteReg32(DDRCTRL_BASE_ADDR+0x160,ddr3_pattern[i][2]);
		WriteReg32(DDRCTRL_BASE_ADDR+0x15c,ddr3_pattern[i][3]);
		WriteReg32(DDRCTRL_BASE_ADDR+0x178,ddr3_pattern[i][4]);

		WriteReg8(DDRCTRL_BASE_ADDR+0x45,0x90);
	}

	return -1;
}
