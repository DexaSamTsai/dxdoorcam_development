#include "includes.h"

int ddrset_auto(int clock){
	int ret;

	if(clock == 0 || clock >= 666){
		//try ddr3 666M
		ret = ddrset_ddr3_666M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "DDR3 666M\n");	
			return ret;
		}
	}

	if(clock == 0 || clock >= 533){
		//try lpddr2 533M
		ret = ddrset_lpddr2_533M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "LPDDR2 533M\n");	
			return ret;
		}else{
			//try ddr3 533M
			ret = ddrset_ddr3_533M();
			if(ret >= 0){
	    		syslog(LOG_INFO, "DDR3 533M\n");	
				return ret;
			}
		}

	}

	if(clock == 0 || clock >= 400){
		//try lpddr2 400M
		ret = ddrset_lpddr2_400M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "LPDDR2 400M\n");	
			return ret;
		}else{
			//try ddr3 400M
			ret = ddrset_ddr3_400M();
			if(ret >= 0){
	    		syslog(LOG_INFO, "DDR3 400M\n");	
				return ret;
			}

		}
	}

	if(clock == 0 || clock >= 300){
		//try lpddr2 300M
		ret = ddrset_lpddr2_300M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "LPDDR2 300M\n");	
			return ret;
		}
	}

	if(clock == 0 || clock >= 250){
		//try lpddr2 250M
		ret = ddrset_lpddr2_250M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "LPDDR2 250M\n");	
			return ret;
		}
	}


	if(clock == 0 || clock >= 200){
		//try lpddr2 200M
		ret = ddrset_lpddr2_200M();
		if(ret >= 0){
	    	syslog(LOG_INFO, "LPDDR2 200M\n");	
			return ret;
		}
	}

	syslog(LOG_INFO, "DDR AUTO SET failed\n");	
	return -1;
}

