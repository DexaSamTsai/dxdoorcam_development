#include "includes.h"
/*
 * final workaround version, support by jack xie, commnet the macro
 */
#define DDR_CL7_EN
int ddrset_ddr3_533M(void)
{
	//set the clcok 532M
		r3_ddrpll_set( 4,//int prediv,
		               1,//int divsys,
		               4,//int divisp,
		               66,//int pdiv,
		               1,//int sdiv,
		               0,//int bypass,
		               0,//int intn_en,
					   0,
					   0
					   );
		//set the clock 540MHz
	   /* r3_ddrpll_set(
	                   0,//int prediv,
	                   1,//int divsys,
	                   4,//int divisp,
	                   22,//int pdiv,
	                   1,//int sdiv,
	                   0,//int bypass,
	                   0,//int intn_en,
	                   0,//int sscg_en,
	                   0//int dsm
	                   );//set pll1 to 1080MHz*/
    DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25));

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset
#ifdef DDR_CL7_EN
	WriteReg32(DDRCTRL_BASE_ADDR+0x00,0x00000831);          // const_mr
#else
	WriteReg32(DDRCTRL_BASE_ADDR+0x00,0x00000841);          // const_mr
#endif
	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x0000000C);         // const_emr
	WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000008);          // const_emr2
	WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x0000E000);          // const_200us
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000015);          // mem_config bit8: remapping.
//	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000115);          // mem_config bit8: remapping. 
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000600);          // const_trfc_max
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00043165);          // const_para1
	WriteReg32(DDRCTRL_BASE_ADDR+0x2C,0x000280F3);         // const_para2
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x00001111);          // const_para3
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x00001AB4);        // const_para5
#ifdef DDR_CL7_EN
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x50010073);          // const_para6
#else
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x50010083);          // const_para6
#endif
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000E);          // const_tfaw
	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x00022000);          // const_500us
	WriteReg32(DDRCTRL_BASE_ADDR+0x58,0x01000620);          // ddr3_para1
	WriteReg32(DDRCTRL_BASE_ADDR+0x60,ReadReg32(DDRCTRL_BASE_ADDR+0x60)|(BIT4));  // increase training read gap
	WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // disable dataeye
	WriteReg32(DDRCTRL_BASE_ADDR+0xd8,0xffff0000);          //
	WriteReg32(DDRCTRL_BASE_ADDR+0xdc,0xffff0000);          //
//	WriteReg32(DDRCTRL_BASE_ADDR+0x168,0x380c2800);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x06060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x55550016);    // ZQ repeat num =3
	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x000000F7);    // Ron=RZQ/8
	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x00060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]

	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i

	// Release DDR PHY Reset
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25));

	ddr3_retrain();
	 // Change Ron back to `h3(48ohm)
	WriteReg32(DDRCTRL_BASE_ADDR+0x15C,0x00000037);
	 // retrain with Ron=48ohm, no matter whether 1st training pass or fail
	WriteReg8(DDRCTRL_BASE_ADDR+0x45,0x90);

	DDR3_WAIT_READY;

	return 0;
}

int ddrset_ddr3_533M_remap(void)
{
	WriteReg32(SC_BASE_ADDR+0x120,0x0C080016|(ReadReg32(SC_BASE_ADDR+0x120)&0xf000));   //keep ddr_pll div unchanged);

	DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25)); 

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 
	WriteReg32(DDRCTRL_BASE_ADDR+0x00,0x00000841);          // const_mr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x0000000C);         // const_emr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x08,0x00000008);          // const_emr2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x10,0x0000E000);          // const_200us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000115);          // mem_config bit8: remapping. 
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000600);          // const_trfc_max 
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00043165);          // const_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x2C,0x000280F3);         // const_para2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x00001111);          // const_para3 
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x00001AB4);        // const_para5 
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x50010083);          // const_para6 
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000E);          // const_tfaw 
	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x00022000);          // const_500us 
	WriteReg32(DDRCTRL_BASE_ADDR+0x58,0x01000620);          // ddr3_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // disable dataeye
	WriteReg32(DDRCTRL_BASE_ADDR+0xd8,0xffff0000);          // 
	WriteReg32(DDRCTRL_BASE_ADDR+0xdc,0xffff0000);          // 
	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x5555001e);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x168,0x380c2800);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x06060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x00000037);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x00060031);	// update PHY r_zctrl_trim_vref[7:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]
	
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i 

	// Release DDR PHY Reset 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25)); 

	DDR3_WAIT_READY;

	return 0;
}
