#include "includes.h"

int ddrset_ddr2_400M(void)
{
		ddr_disable();
    r3_ddrpll_set(
                   0,//int prediv, 
                   1,//int divsys,
                   4,//int divisp, 
                   16,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set ddr to 396MHz
	
	DDR_CLEAR_READY;

	// HOLD DDR PHY RESET 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)|(BIT25)); 

	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 

	WriteReg32(DDRCTRL_BASE_ADDR+0x00,0x00000A63);          // const_mr 
   	WriteReg32(DDRCTRL_BASE_ADDR+0x04,0x0000002C);         // const_emr 
	WriteReg32(DDRCTRL_BASE_ADDR+0x1C,0x00000015);          // mem_config 
	WriteReg32(DDRCTRL_BASE_ADDR+0x20,0x00000300);
	WriteReg32(DDRCTRL_BASE_ADDR+0x28,0x00041531);          // const_para1 
	WriteReg32(DDRCTRL_BASE_ADDR+0x2C,0x000280B2);         // const_para2 
	WriteReg32(DDRCTRL_BASE_ADDR+0x34,0x0000080A);          
	WriteReg32(DDRCTRL_BASE_ADDR+0x38,0x0000840D);          
	WriteReg32(DDRCTRL_BASE_ADDR+0x3C,0x00040E33);        // const_para5 
	WriteReg32(DDRCTRL_BASE_ADDR+0x40,0x5000D263);          // const_para6
	WriteReg32(DDRCTRL_BASE_ADDR+0x50,0x0000000A);          // const_tfaw 
//	WriteReg32(DDRCTRL_BASE_ADDR+0x54,0x000186B4);          // const_500us 
//	WriteReg32(DDRCTRL_BASE_ADDR+0x58,0x01000618);          // ddr3_para1 

	WriteReg32(DDRCTRL_BASE_ADDR+0x64,0x00000005);          // disable dataeye 

//	WriteReg32(DDRCTRL_BASE_ADDR+0xD8,0xFFFF0000);          
//	WriteReg32(DDRCTRL_BASE_ADDR+0xDC,0xFFFF0000);          
//	WriteReg32(DDRCTRL_BASE_ADDR+0x160,0x5555001E);          

	WriteReg32(DDRCTRL_BASE_ADDR+0x15c,0x00000037);        	// update PHY r_dfi_opt_rttn_i[3:0]
//	WriteReg32(DDRCTRL_BASE_ADDR+0x178,0x00060031);        	// update PHY r_dfi_opt_rttn_i[3:0]
	WriteReg32(DDRCTRL_BASE_ADDR+0x178,ReadReg32(DDRCTRL_BASE_ADDR+0x178)|BIT14); // r_ana_rcv_sel = 1
	WriteReg32(DDRCTRL_BASE_ADDR+0x17c,0x00000000);	// update PHY r_zctrl_trim_vref[7:0]

	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)&(~BIT8));     // release ddr_srst_i 

	// Release DDR PHY Reset 
	WriteReg32(SC_BASE_ADDR+0x84,ReadReg32(SC_BASE_ADDR+0x84)&(~BIT25)); 

	DDR2_WAIT_READY;

	return 0;
}
