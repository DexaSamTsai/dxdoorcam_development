#include "includes.h"

int ddr_disable(void)
{
	int timeout=0x100;
	// PROGRAM SETTINGS 
	WriteReg32(SC_BASE_ADDR+0x78,ReadReg32(SC_BASE_ADDR+0x78)|(BIT8));      // put ddr_srst_i to reset 
	while(timeout--)
	{
		if(ReadReg32(DDRCTRL_BASE_ADDR+0x190) == 0)
		{
			break;
		}
	}
	if(timeout ==0)
	{
		return -1;
	}
	return 0;
}
