#include "includes.h"

static void wm8978_playback_init(t_codec_cpara *cp);
static void wm8978_duplex_init(t_codec_cpara *cp);
#ifndef CHIP_R1
static void wm8978_set_sr(s32 sample_rate);
#endif
static s32 wm8978_set_dac_dvol(s32 vol);
static void wm8978_eq_cfg(s32 eq_type);
static void wm8978_record_init(t_codec_cpara *cp);

static __acodec_cfg t_libacodec_setting g_wm8978_setting;

static t_libsccb_cfg h_wm8978_sccb={
	.sccb_num = 0,
};

int codec_wm8978_init(t_codec_cpara *cp)
{
	E_AI_DIR dir = cp->dir;	
	switch(dir)
	{
		case AI_DIR_RECORD:
			wm8978_record_init(cp);
			break;
		case AI_DIR_PLAY:
			wm8978_playback_init(cp);
			break;
		case AI_DIR_DUPLEX:
			wm8978_duplex_init(cp);
			break;
		default:
			debug_printf("codec_wm8978_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_wm8978_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			{
		        	s32 ch = *(s32 *)arg;	

			    debug_printf(" channels=%d\n",ch);
				if(ch == 2)
					sccb_wr(&h_wm8978_sccb, 0x08, 0x10);	//SA+stereo
				else
					sccb_wr(&h_wm8978_sccb, 0x08, 0x11);	//SA+mono
			}
			break;
		case AI_CTL_SR:
			{//codec working in master mode
				s32 sr = *(s32 *)arg;
				volatile s32 d;
				debug_printf("wm8978 samplerate = %d\n",sr);
				for(d=0;d<10000;d++); //time delay, if not delay, no sound when duplex. reason is unknown.
				wm8978_set_sr(sr);
			}
			break;
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return wm8978_set_dac_dvol(vol);
			}

		case AI_CTL_ADC_DVOL:
			break;

		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			sccb_wr(&h_wm8978_sccb, 0x17, 0x0); //set left dac volume
			sccb_wr(&h_wm8978_sccb, 0x19, 0x0); //set right dac volume	
			break;
		case AI_CTL_EQ:
			{
				E_AI_EQTYPE eq = *(E_AI_EQTYPE *)arg;
				wm8978_eq_cfg(eq);
			}
			break;
		default:
			break;
	}
	return 0;
}


static void wm8978_set_sr(s32 sample_rate)
{
	/* PLL config, in master mode */
	if(sample_rate == 8000){	
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x15);
   		sccb_wr(&h_wm8978_sccb, 0x4a ,0x1b); 
   		sccb_wr(&h_wm8978_sccb, 0x4c ,0xff);
   		sccb_wr(&h_wm8978_sccb, 0x4e ,0xf1);
		sccb_wr(&h_wm8978_sccb, 0x0d ,0x8d); //sel pll, mclkdiv:/4, blckdiv:/8, master mode
   		sccb_wr(&h_wm8978_sccb, 0x0e ,0x0a);//8k
	}
	else if(sample_rate == 48000){
   		sccb_wr(&h_wm8978_sccb, 0x0e ,0x00);//44k
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x08);
   		sccb_wr(&h_wm8978_sccb, 0x4a ,0x0e); 
   		sccb_wr(&h_wm8978_sccb, 0x4c ,0x61);
   		sccb_wr(&h_wm8978_sccb, 0x4e ,0x26);
		sccb_wr(&h_wm8978_sccb, 0x0d ,0x4d); //sel pll, mclkdiv:/4, blckdiv:/8, master mode
	}
	else if(sample_rate == 44100){
   		sccb_wr(&h_wm8978_sccb, 0x0e ,0x00);//44k
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x07);
   		sccb_wr(&h_wm8978_sccb, 0x4a ,0x60); 
   		sccb_wr(&h_wm8978_sccb, 0x4c ,0x61);
   		sccb_wr(&h_wm8978_sccb, 0x4e ,0x26);
		sccb_wr(&h_wm8978_sccb, 0x0d ,0x4d); //sel pll, mclkdiv:/4, blckdiv:/8, master mode
	}
	else if(sample_rate == 32000){
		sccb_wr(&h_wm8978_sccb, 0x0e, 0x2);
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x1a);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0x7c); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0x19);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4f ,0xe1);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x4d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
	else if(sample_rate == 24000){
		sccb_wr(&h_wm8978_sccb, 0x0e, 0x4);
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x18);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0x0c); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0x93);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4e ,0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x4d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
	
	else if(sample_rate == 16000){
		sccb_wr(&h_wm8978_sccb, 0x0e, 0x6);
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x1a);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0x3c); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0xff);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4e ,0xf1);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x8d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
	else if(sample_rate == 12000){
		sccb_wr(&h_wm8978_sccb, 0x0e, 0x8);
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x18);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0x08); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0xff);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4e ,0xf1);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x8d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
	else if(sample_rate == 11025){
		sccb_wr(&h_wm8978_sccb, 0x0e, 0x8);
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x17);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0xa1); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0xff);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4e ,0xf1);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x8d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
   	if(sample_rate == 8000){ 
		sccb_wr(&h_wm8978_sccb, 0x0e ,0x0a);//8k
		sccb_wr(&h_wm8978_sccb, 0x48 ,0x15);
    	sccb_wr(&h_wm8978_sccb, 0x4a ,0x1b); //0x16);
    	sccb_wr(&h_wm8978_sccb, 0x4c ,0xff);//0xe9);
    	sccb_wr(&h_wm8978_sccb, 0x4e ,0xf1);
    	sccb_wr(&h_wm8978_sccb, 0x0d ,0x8d); //sel pll, mclkdiv:/4, blckdiv:/8
	}
}

/*
0000 0000 = Digital Mute
0000 0001 = -127dB
0000 0010 = -126.5dB
... 0.5dB steps up to
1111 1111 = 0dB
*/
static s32 wm8978_set_dac_dvol(s32 vol){
	debug_printf("set dvol = %ddB\n",vol);
	{
		u8 setval;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xff;
			setvol = 0;
			debug_printf("wm8978 not support digital volume bigger than 0 dB, default to 0dB\n");
		}
		else if(vol < -127)
		{
			setval = 0x1;
			setvol = -127;
			debug_printf("wm8978 does not support digital volume less than -127dB,default to -127dB\n");
		}
		else
		{
			setval = (vol + 127)*2 + 1;
			setvol = vol;
		}	
		sccb_wr(&h_wm8978_sccb, 0x17, setval);
		sccb_wr(&h_wm8978_sccb, 0x19, setval);
		return setvol;
	}
}


void wm8978_playback_init (t_codec_cpara *cp)
{
	u32 master = cp->master;
	h_wm8978_sccb.mode = SCCB_MODE8;
	h_wm8978_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_wm8978_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_wm8978_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8978_sccb.wr_check = 0;
	h_wm8978_sccb.timeout = 0;
	h_wm8978_sccb.retry = 1;

	/* set sccb device address '0011010', 8-bit address mode */
	//sccb_set(&h_wm8978_sccb);
	libsccb_init(&h_wm8978_sccb);
	sccb_wr(&h_wm8978_sccb, 0x00, 0x00);
	/* power management, l/R out1 playback SA data */
	sccb_wr(&h_wm8978_sccb, 0x02, 0x3d);
	sccb_wr(&h_wm8978_sccb, 0x05, 0xbc);	//adc not enable
	sccb_wr(&h_wm8978_sccb, 0x06, 0x0f);
	
	if(!master)
	{//codec working in slave mode
		sccb_wr(&h_wm8978_sccb, 0x06*2, 0); //not select PLL, slave mode,
	}
	
    /* dac and output config, LOUT1 and ROUT1 using default config */
	sccb_wr(&h_wm8978_sccb, 0x14, 0x00);	//default value
	sccb_wr(&h_wm8978_sccb, 0x64, 0x1);//enable left dac to left mixer
	sccb_wr(&h_wm8978_sccb, 0x66, 0x1);//enable right dac to right mixer	
	sccb_wr(&h_wm8978_sccb, 0x17, 0xff); //set left dac volume
	sccb_wr(&h_wm8978_sccb, 0x19, 0xff); //set right dac volume	
	sccb_wr(&h_wm8978_sccb, 0x68, 0x3f);//lout1 volum max
	sccb_wr(&h_wm8978_sccb, 0x6a, 0x3f); //rout1 volume max
}

void wm8978_duplex_init(t_codec_cpara *cp)
{
	u32 master = cp->master; 
	h_wm8978_sccb.mode = SCCB_MODE8;
	h_wm8978_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_wm8978_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_wm8978_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8978_sccb.wr_check = 0;
	h_wm8978_sccb.timeout = 0;
	h_wm8978_sccb.retry = 1;

	
	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8978_sccb);

	sccb_wr(&h_wm8978_sccb, 0x00, 0x00);
	/* power management setting */
	sccb_wr(&h_wm8978_sccb, 0x02, 0x3d);
	sccb_wr(&h_wm8978_sccb, 0x05, 0xbf);	//adc enable, l/rout1 disable, inppga enable
	sccb_wr(&h_wm8978_sccb, 0x06, 0x0f);	//enable mixer and dac 
	if(!master)
	{//codec working in slave mode
		sccb_wr(&h_wm8978_sccb, 0x06*2, 0); //not select PLL, slave mode,
	}

	/* adc and input control */
	sccb_wr(&h_wm8978_sccb, 0x1d, 0x00);	//default value, enable high pass
	sccb_wr(&h_wm8978_sccb, 0x1f, 0xff); //left adc volume
	sccb_wr(&h_wm8978_sccb, 0x21, 0xff); //right adc volume
	sccb_wr(&h_wm8978_sccb, 0x24, 0x2c); //eq
	sccb_wr(&h_wm8978_sccb, 0x58, 0x33);
	sccb_wr(&h_wm8978_sccb, 0x5b, 0x2c);//inputPGAVOLL
	sccb_wr(&h_wm8978_sccb, 0x5d, 0x2c);//inputpgavolR
   	/* dac and output control,dac digital volume set default */
	sccb_wr(&h_wm8978_sccb, 0x14, 0x00);	//default value
	sccb_wr(&h_wm8978_sccb, 0x64, 0x1);//enable left dac to left mixer
	sccb_wr(&h_wm8978_sccb, 0x66, 0x1);//enable right dac to right mixer	
	sccb_wr(&h_wm8978_sccb, 0x17, 0xff); //set left dac volume
	sccb_wr(&h_wm8978_sccb, 0x19, 0xff); //set right dac volume	
	sccb_wr(&h_wm8978_sccb, 0x68, 0x3f);//lout1 volum max
	sccb_wr(&h_wm8978_sccb, 0x6a, 0x3f); //rout1 volume max	
}

static void wm8978_eq_cfg(s32 eq_type){

	switch(eq_type){
		case STEREO_LIVE: 
			sccb_wr(&h_wm8978_sccb, 0x25, 0x49); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x26, 0x2c); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x69); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x2c); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x09); ///< 3D control
			break;
		case CLASSICAL:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x2c); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x26, 0x2c); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x4a); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x69); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x2c); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x03); ///< 3D control
			break;
		case DANCE:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x44); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x08); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x69); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x46); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x06); ///< 3D control
			break;
	
		case 0:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x2c); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x26, 0x2c); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x2c); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x2c); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x00); ///< 3D control
			break;
		case DISCO:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x25); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x2a); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2b, 0x6a); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x68); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x00); ///< 3D control
			break;
		case HALL:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x46); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x2a); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2b, 0x4a); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x47); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x07); ///< 3D control
			break;
		case HEAVY:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x46); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x26, 0x2c); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x29, 0x4f); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2b, 0x6a); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x65); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x03); ///< 3D control
			break;
		case POP:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x10); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x07); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x29, 0x49); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x2c); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x29); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x05); ///< 3D control
			break;
		case ROCK:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x47); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x26, 0x2e); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x6b); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x49); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x03); ///< 3D control
			break;
		case SOFT:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x48); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x2b); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2a, 0x2c); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x30); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x00); ///< 3D control
			break;
		case SOUL_JAZZ:
			sccb_wr(&h_wm8978_sccb, 0x25, 0x66); ///< EQ1 -- low shelf
			sccb_wr(&h_wm8978_sccb, 0x27, 0x2a); ///< EQ2 -- peak 1
			sccb_wr(&h_wm8978_sccb, 0x28, 0x2c); ///< EQ3 -- peak 2
			sccb_wr(&h_wm8978_sccb, 0x2b, 0x6a); ///< EQ4 -- peak 3
			sccb_wr(&h_wm8978_sccb, 0x2c, 0x49); ///< EQ5 -- high shelf
			sccb_wr(&h_wm8978_sccb, 0x29, 0x02); ///< 3D control
			break;
	
		default:
			break;
	}
	return ;
}

void wm8978_record_init(t_codec_cpara *cp)
{
	u32 master = cp->master;
	h_wm8978_sccb.mode = SCCB_MODE8;
	h_wm8978_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
//	h_wm8978_sccb.rd_mode = RD_MODE_NORMAL;
	h_wm8978_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8978_sccb.wr_check = 0;
	h_wm8978_sccb.timeout = 0;
	h_wm8978_sccb.retry = 1;

	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8978_sccb);
	sccb_wr(&h_wm8978_sccb, 0x00, 0x00);
	/* l/R record AI data, wm8978 in slave mode */
	sccb_wr(&h_wm8978_sccb, 0x02, 0x3d);
	sccb_wr(&h_wm8978_sccb, 0x04, 0x3f);	//adc enable, l/rout1 disable, inppga enable
	sccb_wr(&h_wm8978_sccb, 0x06, 0x0c);	//enable mixer and dac 

	if(!master){//codec working in slave mode
		sccb_wr(&h_wm8978_sccb, 0x0c, 0x8c);//clock from MCLK(bit8=0),slave mode(bit0=0),	
	}

	/* adc control and input control */
	sccb_wr(&h_wm8978_sccb, 0x1d, 0x00);	//default value, enable high pass
	sccb_wr(&h_wm8978_sccb, 0x1f, 0xff); //left adc volume
	sccb_wr(&h_wm8978_sccb, 0x21, 0xff); //right adc volume
	sccb_wr(&h_wm8978_sccb, 0x24, 0x2c); //eq
	sccb_wr(&h_wm8978_sccb, 0x58, 0x33);
	sccb_wr(&h_wm8978_sccb, 0x5b, 0x2c);//inputPGAVOLL
	sccb_wr(&h_wm8978_sccb, 0x5d, 0x2c);//inputpgavolR
}


static __acodec_cfg t_libacodec_setting g_wm8978_setting = {
	.early_init = NULL,
	.init = codec_wm8978_init,
	.ioctl = codec_wm8978_ioctl
	};


