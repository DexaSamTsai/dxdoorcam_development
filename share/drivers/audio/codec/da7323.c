#include "includes.h"
//#include "upgrade_spif.h"

#define CONFIG_AUDIO_DEC_EN  
#define CONFIG_AUDIO_ENC_EN  

static __acodec_cfg t_libacodec_setting g_da7323_setting;

enum da7323_hal_endpoint {
	DA732X_HAL_endpoint_invalid,
	DA732X_HAL_MIC1,
	DA732X_HAL_MIC4,
	DA732X_HAL_DMICA,
	DA732X_HAL_DMICB,
	DA732X_HAL_HP,
	DA732X_HAL_SPEAKER_A,
	DA732X_HAL_SPEAKER_B,
	da7323_hal_endpoint_first=DA732X_HAL_MIC1,
	da7323_hal_endpoint_final=DA732X_HAL_SPEAKER_B,
};

int hal_abort_reset_framework(void);
int hal_wait_for_srm_to_lock(void);
int hal_endpoint_mute_control(int mute, enum da7323_hal_endpoint endpoint);
int hal_force_firmware(int write_mode,int sample_rate,int system_rate);
int hal_init_firmware(int write_mode,int sample_rate,int system_rate);
int hal_init_dsp_graph(int write_mode, int clock_master,int sample_rate,int system_rate);
int hal_initialize_codec_controls(void);
int hal_resume_control(void);
int hal_get_endpoint_min_vol_mB(enum da7323_hal_endpoint endpoint,int*minimum_mB);
int hal_get_endpoint_max_vol_mB(enum da7323_hal_endpoint endpoint,int*maximum_mB);
int hal_set_endpoint_volume_mB(enum da7323_hal_endpoint endpoint,int volume_mB);
int hal_set_into_bypass(int clock_master,int sample_rate,int system_rate);
int hal_suspend_control(void);
void hal_save_dsp_commands(u8*dsp_firmware_binary,int dsp_firmware_length,int graph_number);
int hal_setup_codec_clocks(int sample_rate,int system_rate);

//#define DA7323_DSP_VERSION_1_0
#define DSP_DA7323_v2_2
#ifdef DSP_DA7323_v2_2
#define DA7323_REC_PB 0x11
#define DA7323_AEC_REC_PB 0x12
#else
#define DA7323_REC_PB 0x0B
#define DA7323_AEC_REC_PB 0x10
#endif

#if defined CONFIG_DA7323_I2C_SINGLE_MODE
#define WRITE_MODE 0
#elif defined CONFIG_DA7323_I2C_BLOCK_MODE
#define WRITE_MODE 1
#elif defined CONFIG_DA7323_I2C_BURST_MODE
#define WRITE_MODE 2
#else
#define WRITE_MODE 0
//#error "the I2C multiple register write mode is not defined"
#endif

#if ! defined CONFIG_DA7323_DSP_BYPASS
#if defined CONFIG_DA7323_DSP_GRAPH_0B
#define DA7323_DSP_GRAPH 0x0B
#elif defined CONFIG_DA7323_DSP_GRAPH_03
#define DA7323_DSP_GRAPH 0x03
#elif defined CONFIG_DA7323_DSP_GRAPH_11
#define DA7323_DSP_GRAPH 0x11
#elif defined CONFIG_DA7323_DSP_GRAPH_12
#define DA7323_DSP_GRAPH 0x12
#else
#error "missing DSP GRAPH number"
#endif
int old_dsp_graph = DA7323_DSP_GRAPH;
#endif
#ifndef CONFIG_SAMPLE_RATE
#define CONFIG_SAMPLE_RATE 11025
#endif
int da7323_sample_rate = CONFIG_SAMPLE_RATE;
int da7323_system_rate = CONFIG_SAMPLE_RATE;

#define DA7323_D_MIC 1
#define DA7323_A_MIC 2
#define DA7323_INPUT DA7323_A_MIC
#if (DA7323_INPUT == DA7323_A_MIC)
#define DA7323_MIC1
//#define DA7323_MIC4
#elif (DA7323_INPUT == DA7323_D_MIC)
#define DA7323_DMICA
//#define DA7323_DMICB
#endif
//#define hal_ticks (*(unsigned int volatile  *)(0x0083ff08))
t_libsccb_cfg h_da7323_sccb;
#undef DEBUG_TRACE
#ifdef CONFIG_DA7323_DIAGNOSTICS
#define DEBUG_TRACE(...)  debug_printf(__VA_ARGS__)
#define DEBUG_PRINTF(...) debug_printf(__VA_ARGS__)
#else
#define DEBUG_TRACE(...) do{}while(0)
#define DEBUG_PRINTF(...) do{}while(0)
#endif
void diag_print(const char *line)
{
	debug_printf(line);
}
void diag_tick_print(void)
{
//	DEBUG_PRINTF("---hal_ticks : %d\n",hal_ticks);
}
int i2c_register_read(u8 reg, u8 *val)
{
	*val = sccb_rd(&h_da7323_sccb,reg);
	return 4;
}
int i2c_register_write(u8 reg, u8 val)
{
	int ret = sccb_wr(&h_da7323_sccb,reg,val);
	//debug_printf("0x%x 0x%x \n",reg,val);
	if (ret < 0) {
		debug_printf("Single WRITE REG fail: 0x%x , 0x%x \n",reg,val);
		return ret;
	}
	return 3;
}
int i2c_multireg_write(u8 reg,u8 num,const u8*buf)
{
	int count = 0;
	while(num-- > 0) {
		int ret = sccb_wr(&h_da7323_sccb,reg++, *buf++);
		count += 3;
		if (ret < 0) {
			debug_printf("Multi WRITE fail : 0x%x \n",--reg);
			return ret;
		}
	}
	return count;
}
#if defined(CONFIG_DA7323_I2C_BLOCK_MODE) || defined(CONFIG_DA7323_I2C_BURST_MODE)
int i2c_blockreg_write(u8 reg,u8 num,const u8*buf)
{
	int ret = sccb_seqwrite(&h_da7323_sccb,reg,num,buf);
	if (ret < 0) {
		debug_printf("Block WRITE fail : 0x%x \n",reg);
		return ret;
	}
	return 2+num;
}
#else
int i2c_blockreg_write(u8 reg,u8 num,const u8*buf)
{
	debug_printf("CONFIG_DA7323_I2C_BLOCK_MODE not configured\n");
	return 0;
}
#endif
#ifdef CONFIG_DA7323_I2C_BURST_MODE
static u8 burst[3+21*3*4];
int i2c_burstreg_write(u16 address,u8 mem_id,u8 num,const u8*buf)
{
	u8 *d = burst;
	u8 i = num;

	if (num > (sizeof(burst) - 3)) {
		debug_printf("BUG: num '%d' too big for number of burstreg_write bytes\n",num);
		return -1;
	}

	*d++ = mem_id|0x20;
	*d++ = (address >> 0) & 0x00FF;
	*d++ = (address >> 8) & 0x00FF;

	while(i--) *d++ = *buf++;

	int ret = sccb_seqwrite(&h_da7323_sccb,0x40,3+num,burst);
	if (ret < 0) {
		debug_printf("Block WRITE REGs fail");
		return ret;
	}
	return 5+num;
}
#else
int i2c_burstreg_write(u16 address,u8 mem_id,u8 num,const u8*buf)
{
	debug_printf("DA7323_I2C_BURST_MODE not configured\n");
	return 0;
}
#endif
#ifdef CONFIG_DA7323_DIAGNOSTICS
static char hexdigit(unsigned char d)
{
	switch(d) {
	case 0x00:	return '0';
	case 0x01:	return '1';
	case 0x02:	return '2';
	case 0x03:	return '3';
	case 0x04:	return '4';
	case 0x05:	return '5';
	case 0x06:	return '6';
	case 0x07:	return '7';
	case 0x08:	return '8';
	case 0x09:	return '9';
	case 0x0A:	return 'A';
	case 0x0B:	return 'B';
	case 0x0C:	return 'C';
	case 0x0D:	return 'D';
	case 0x0E:	return 'E';
	case 0x0F:	return 'F';
	default:	return '?';
	}
}
char*hexchars(u8 v)
{
	static char*h = "HH_";
	h[2] = 0;
	h[1] = hexdigit(v&0xF);
	v >>= 4;
	h[0] = hexdigit(v&0xF);

	return h;
}
static void i2c_print_register_row(u8 reg_base)
{
	DEBUG_PRINTF("registers[%s..] =",hexchars(reg_base));

	int i = 0;
	for(i=0;i<16;i++){
		u8 reg;
		i2c_register_read(reg_base++,&reg);

		DEBUG_PRINTF(" %s",hexchars(reg));
	}
	DEBUG_PRINTF("\n");
}
static void dump_all_registers(char*phase)
{
	DEBUG_PRINTF("%s dumping all registers in da7323\n",phase);
	int i;
	for(i=0;i<16;i++) i2c_print_register_row(i*16);
}
#endif
static void initialize_i2c(void)
{
	memset(&h_da7323_sccb,0,sizeof(h_da7323_sccb));
	h_da7323_sccb.mode = SCCB_MODE8;
	h_da7323_sccb.id = 0x34;
	h_da7323_sccb.rd_mode = RD_MODE_NORMAL;
	h_da7323_sccb.clk = 600*1024;
	h_da7323_sccb.wr_check = 0;
	h_da7323_sccb.timeout = 0;
	h_da7323_sccb.retry = 1;
	//sccb_set(&h_da7323_sccb);
	libsccb_init(&h_da7323_sccb);
}
#define __hal
//#define __hal      __attribute__ ((__section__ (".hal_data")))
extern __hal int da7323_dsp_firmware_binary;
extern __hal int da7323_dsp_firmware_finish;
extern __hal int da7323_dsp_firmware_length;
#ifdef CONFIG_DA7323_DIAGNOSTICS
static void print_i2c_access_and_init_time(int start_ticks,int count,char*message)
{
	int final_ticks = ticks;
	int total_decisecs = final_ticks-start_ticks;
	int total_seconds = total_decisecs/100;
	int fraction = total_decisecs - 100*total_seconds;

	DEBUG_PRINTF("%s used %d I2C accesses took %d.%d secs\n",__func__,count,total_seconds,fraction);
    dump_all_registers(message);
}
#endif
static int initialize_vol_min_max(void)
{
	int count = 0;
	int minimum_mB;
	int maximum_mB;

	count += hal_get_endpoint_min_vol_mB(DA732X_HAL_DMICA,&minimum_mB);
	count += hal_get_endpoint_max_vol_mB(DA732X_HAL_DMICA,&maximum_mB);

	g_da7323_setting.avol.max = maximum_mB/100; //dB
	g_da7323_setting.avol.min = minimum_mB/100; //dB

	count += hal_get_endpoint_min_vol_mB(DA732X_HAL_SPEAKER_A,&minimum_mB);
	count += hal_get_endpoint_max_vol_mB(DA732X_HAL_SPEAKER_A,&maximum_mB);

	g_da7323_setting.dvol.max = maximum_mB/100; //dB
	g_da7323_setting.dvol.min = minimum_mB/100; //dB

	return count;
}

#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
static int da7323_duplex_init(u32 master)
{
	int count = 0;
	DEBUG_PRINTF("\n%s built at %s %s\n\n",__func__,__TIME__,__DATE__);
#ifdef CONFIG_DA7323_DIAGNOSTICS
	int start_ticks = ticks;
#endif

#if defined CONFIG_DA7323_DSP_BYPASS
	count = hal_set_into_bypass(master,da7323_sample_rate,da7323_system_rate);
#else
	count = hal_init_dsp_graph(WRITE_MODE,master,da7323_sample_rate,da7323_system_rate);
#endif

	//count += initialize_vol_min_max();
#ifdef CONFIG_DA7323_DIAGNOSTICS
	print_i2c_access_and_init_time(start_ticks,count,"AfterDuplexInit");
#endif
#if (DA7323_INPUT == DA7323_A_MIC)
    DEBUG_PRINTF("Route Analogue MIC\n");
    // Route Analogue MIC to ADC
    i2c_register_write(0x61, 0x00);
#elif (DA7323_INPUT == DA7323_D_MIC)
    // Route DIGITAL MIC
    i2c_register_write(0x61, 0x0F);
    DEBUG_PRINTF("Route DIGITAL MIC\n");
#endif
	return 0;
}
#endif

#ifdef CONFIG_AUDIO_DEC_EN  
static int da7323_playback_init(u32 master)
{
	int count = 0;
	DEBUG_PRINTF("\n%s built at %s %s\n\n",__func__,__TIME__,__DATE__);
#ifdef CONFIG_DA7323_DIAGNOSTICS
	int start_ticks = ticks;
#endif

#if defined CONFIG_DA7323_DSP_BYPASS
	count = hal_set_into_bypass(master,da7323_sample_rate,da7323_system_rate);
#else
	count = hal_init_dsp_graph(WRITE_MODE,master,da7323_sample_rate,da7323_system_rate);
#endif

	//count += initialize_vol_min_max();
#ifdef CONFIG_DA7323_DIAGNOSTICS
	print_i2c_access_and_init_time(start_ticks,count,"AfterPlaybackInit");
#endif

	return 0;
}
#endif
static int da7323_suspend_control(void)
{
	int count = 0;

	DEBUG_PRINTF("da7323_suspend_control\n");

#if ! defined CONFIG_DA7323_DSP_BYPASS
	count += hal_abort_reset_framework();
#endif

	count += hal_suspend_control();

	return 0;
}

static int da7323_enc_mute_control(int mute)
{
	int count = 0;

	DEBUG_PRINTF("da7323_mute_control(%s)\n",mute==1?"mute":mute==0?"de-mute":"unknown");

#ifdef DA7323_MIC1
	count = hal_endpoint_mute_control(mute,DA732X_HAL_MIC1);
#endif
#ifdef DA7323_MIC4
	count = hal_endpoint_mute_control(mute,DA732X_HAL_MIC4);
#endif
	
	return 0;
}


static int da7323_mute_control(int mute)
{
	int count = 0;

	DEBUG_PRINTF("da7323_mute_control(%s)\n",mute==1?"mute":mute==0?"de-mute":"unknown");

	count = hal_endpoint_mute_control(mute,DA732X_HAL_SPEAKER_A);

	return 0;
}

#ifdef CONFIG_AUDIO_DEC_EN  
static void da7323_set_dac_dvol(s32 vol)
{
	int count = 0;
	u8 read_val;

	DEBUG_PRINTF("set DAC vol = %x %d - min=%d max=%d deciBells\n",119 - (7725 + vol * 100) / 75,vol,g_da7323_setting.dvol.min,g_da7323_setting.dvol.max);
	count += hal_set_endpoint_volume_mB(DA732X_HAL_SPEAKER_A,0);	// units of milliBells // set speaker volume to 0dB
	i2c_register_write(0xBE, 119 - (7725 + vol * 100) / 75); // control the digital gain.	
}
#endif

#ifdef CONFIG_AUDIO_ENC_EN  
static void da7323_set_adc_dvol(s32 vol)
{
	int count = 0;

	DEBUG_PRINTF("set ADC vol = %d - min=%d max=%d deciBells\n",vol,g_da7323_setting.avol.min,g_da7323_setting.avol.max);

	count += hal_set_endpoint_volume_mB(DA732X_HAL_MIC1,100*vol);	// units of milliBells
	DEBUG_PRINTF("setting encoder volume to %dmilliBells took %s i2c accesses\n",100*vol,count);

	//input pga 0dB, for mic1,4 : value 7~31 range 0~12dB/0.5 step
	//corresponding bit [4:0], so 0x07:0dB, 0x08:0.5dB.....0x1F:12dB.
	i2c_register_write(0x54, 0x13); // set to 6dB, please change as you want.

	//mic gain : value 1~6, range 0~30dB/6db step.
	//corresponding bit [2:0], so 0x81(1):0dB, 0x82(2):6dB.....0x86(6):30dB.
#ifdef DA7323_MIC4
	i2c_register_write(0x53, 0x85); //mic1 gain 24dB, please change as you want.
#else // for MIC1
	i2c_register_write(0x52, 0x85); //mic4 gain 24dB, please change as you want.
#endif
}
#endif

static void da7323_set_sr(int sampling_rate)
{
	int master = 1;
	int change_sampling_rate = 0;
#ifdef CONFIG_DA7323_DIAGNOSTICS
	int start_ticks = ticks;
#endif

#if defined CONFIG_DA7323_DSP_BYPASS
	int count = 0;
	if(sampling_rate != da7323_sample_rate){
		da7323_sample_rate = sampling_rate;
		da7323_system_rate = sampling_rate;
		count = hal_set_into_bypass(master,da7323_sample_rate,da7323_system_rate);
	}
#else
	int graph_number;
	int count = 0;

#if DA7323_AEC_REC_PB == DA7323_DSP_GRAPH
	if(sampling_rate == 11025){
			graph_number = DA7323_AEC_REC_PB;
			da7323_sample_rate = sampling_rate;
			da7323_system_rate = 16000;
	}
	else
#endif
	{
			graph_number = DA7323_REC_PB;
			if(da7323_sample_rate != sampling_rate)
				change_sampling_rate = 1;
			da7323_sample_rate = sampling_rate;
			da7323_system_rate = sampling_rate;
	}
	DEBUG_PRINTF(" --- SET SR [%d], SYSTEM SR[%d]\n",da7323_sample_rate,da7323_system_rate);

	if(old_dsp_graph != graph_number)
	{
		DEBUG_PRINTF("Change Graph to [0x%x]\n",graph_number);
		old_dsp_graph = graph_number;
		hal_save_dsp_commands((u8 *)&da7323_dsp_firmware_binary,
									(int)&da7323_dsp_firmware_finish-(int)&da7323_dsp_firmware_binary,
									graph_number);												
		count = hal_init_dsp_graph(WRITE_MODE,master,da7323_sample_rate, da7323_system_rate);
#if (DA7323_INPUT == DA7323_A_MIC)
	    DEBUG_PRINTF("Route Analogue MIC\n");
	    // Route Analogue MIC to ADC
	    i2c_register_write(0x61, 0x00);
#elif (DA7323_INPUT == DA7323_D_MIC)
	    // Route DIGITAL MIC
	    i2c_register_write(0x61, 0x0F);
	    DEBUG_PRINTF("Route DIGITAL MIC\n");
#endif
	}
	else if(change_sampling_rate == 1)
	{
		DEBUG_PRINTF("Change Sample Rate to %d \n",da7323_sample_rate);
		hal_setup_codec_clocks(da7323_sample_rate, da7323_system_rate);
	}
#endif	
	
	if(DA7323_AEC_REC_PB == graph_number)
	{
		DEBUG_PRINTF("Change Sample Rate according to GRAPH \n");
		i2c_register_write(0x30, 0xB7);
		i2c_register_write(0x31, 0x1E);
		i2c_register_write(0x32, 0x28);
	}

	usleep(20*1000);
#ifdef CONFIG_DA7323_DIAGNOSTICS
	print_i2c_access_and_init_time(start_ticks,count,"AfterPlaybackInit");
#endif
}
static int dac_da7323_ioctl(E_AI_IOCTL ioctl, void *arg)
{
//	debug_printf("dac_da7323_ioctl(%d,%x)\n",ioctl,(unsigned int)arg);

	initialize_i2c();

	switch(ioctl)
	{
		case AI_CTL_CHN:
			{
#ifdef CONFIG_DA7323_DIAGNOSTICS
				int channel = *(int*)arg;
				debug_printf("dac_da7323_ioctl does not support set channel %s\n",channel==1?"mono":channel==2?"stereo":"unknown");
#endif
			}
			break;
		case AI_CTL_SR:
			{
				s32 requested_sr = *(s32 *)arg;
				da7323_set_sr((int)requested_sr);
			}
			break;

#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				DEBUG_PRINTF("AI_CTL_ADC_DVOL %d%%\n",vol);
				da7323_set_adc_dvol(vol);
			}
			break;
#endif

#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				DEBUG_PRINTF("AI_CTL_DAC_DVOL %d%%\n",vol);
				da7323_set_dac_dvol(vol);
#ifdef CONFIG_DA7323_DIAGNOSTICS				
				dump_all_registers("AfterAI_CTL_DAC_DVOL");
#endif				
			}
			break;
#endif
		case AI_CTL_MUTE_DAC:
			{
				int mute = *(int*)arg;
				da7323_mute_control(mute);
			}
			break;
		case AI_CTL_PWRDWN:
			{
				da7323_suspend_control();
			}
			break;
		case AI_CTL_EQ:
			{
				debug_printf("equalizer not supported in da7323\n");
			}
			break;
		default:{
				debug_printf("adc_da7323_ioctl does not support unknown ioctl %d\n",ioctl);
			}
			break;
	}
	return 0;
}

#ifdef CONFIG_AUDIO_ENC_EN  
static int da7323_record_init(u32 master)
{
	int count = 0;

	DEBUG_PRINTF("\n%s built at %s %s\n\n",__func__,__TIME__,__DATE__);
#ifdef CONFIG_DA7323_DIAGNOSTICS
	int start_ticks = ticks;
#endif

#if defined CONFIG_DA7323_DSP_BYPASS
	count = hal_set_into_bypass(master,da7323_sample_rate,da7323_system_rate);
#else
	count = hal_init_dsp_graph(WRITE_MODE,master,da7323_sample_rate,da7323_system_rate);
#endif
	//count += initialize_vol_min_max();
#ifdef CONFIG_DA7323_DIAGNOSTICS
	print_i2c_access_and_init_time(start_ticks,count,"AfterRecordInit");
#endif
#if (DA7323_INPUT == DA7323_A_MIC)
    DEBUG_PRINTF("Route Analogue MIC\n");
    // Route Analogue MIC to ADC
    i2c_register_write(0x61, 0x00);
#elif (DA7323_INPUT == DA7323_D_MIC)
    // Route DIGITAL MIC
    i2c_register_write(0x61, 0x0F);
    DEBUG_PRINTF("Route DIGITAL MIC\n");
#endif
	hal_endpoint_mute_control(1,DA732X_HAL_SPEAKER_A);
	return 0;
}
#endif

static int dac_da7323_init(E_AI_DIR dir,u32 master)
{
	//TODO: add GPIO configure code here 
//	libgpio_config(PIN_GPIO_MCU_INT, PIN_DIR_OUTPUT);
//	libgpio_write(PIN_GPIO_MCU_INT, PIN_LVL_HIGH);

#if defined CONFIG_DA7323_DSP_BYPASS	
	DEBUG_PRINTF("-----------------------------------------\n");
	DEBUG_PRINTF(" ** dac_da7323_init - da7323 BYPASS mode\n");
	DEBUG_PRINTF("-----------------------------------------\n");	
#else
#ifdef DA7323_DSP_GRAPH
	DEBUG_PRINTF("-----------------------------------------\n");
	DEBUG_PRINTF(" ** dac_da7323_init - da7323 GRAPH : 0x%x\n",DA7323_DSP_GRAPH);
	DEBUG_PRINTF(" ** Bbuilt by SAMPLING RATE : %d\n",CONFIG_SAMPLE_RATE);	
	DEBUG_PRINTF("-----------------------------------------\n");	
#if DA7323_AEC_REC_PB == DA7323_DSP_GRAPH
	if (da7323_sample_rate == 11025) da7323_system_rate = 16000;
#elif 0x0D == DA7323_DSP_GRAPH // for DSP V1.0 and V2.2
#error "Please check again the GRAPH NUMBER!"
#endif
	hal_save_dsp_commands((u8 *)&da7323_dsp_firmware_binary,
									(int)&da7323_dsp_firmware_finish-(int)&da7323_dsp_firmware_binary,
									DA7323_DSP_GRAPH);
#endif
#endif
	initialize_i2c();
	hal_resume_control();

#ifdef CONFIG_DA7323_DIAGNOSTICS
	dump_all_registers("BeforeInit");
#endif
	switch(dir)
	{
#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_DIR_PLAY:   
			da7323_playback_init(master);
			break;
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_DIR_RECORD:
			da7323_record_init(master);
			break;
#endif
#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
		case AI_DIR_DUPLEX: 
			da7323_duplex_init(master);
			break;
#endif
		default:
			debug_printf("codec_da7323_init configuration error\n");
			return -1;
	}
	return 0;	
}

static __acodec_cfg t_libacodec_setting g_da7323_setting = {
	.early_init = NULL,
	.init = dac_da7323_init,
	.ioctl = dac_da7323_ioctl,
	.dvol.min = -77,
	.dvol.max = +12,
	.avol.min = -83,
	.avol.max = +12,
};
