#include "includes.h"

#define ALC5670_VC

#define ENABLE_DSP

#define CONFIG_AUDIO_DEC_EN
#define CONFIG_AUDIO_ENC_EN
/*********************************************
on v3 board,
outdoor:JP3(speaker) and MIC2(connect R5) 
indoor: JP7(speaker) and MIC1(connect R2) 
outdoor:IN2P/DAC2L
indoor: IN3P/DAC2R
**********************************************/
#define OUTDOOR 
#define INDOOR 

#define I2C_CLOCK (50*1024)
#define SCCB_GROUP

static void alc5670_set_sr(s32 sample_rate);
static void alc5670_set_pll(s32 sample_rate);
#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
static void alc5670_duplex_init(t_codec_cpara *cp);
#endif
#ifdef CONFIG_AUDIO_DEC_EN  
static void alc5670_playback_init(t_codec_cpara *cp);
static s32 alc5670_set_dac_dvol(s32 vol);
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
static void alc5670_record_init(t_codec_cpara *cp);
static s32 alc5670_set_adc_dvol(s32 vol);
#endif

static __acodec_cfg t_libacodec_setting g_alc5670_setting; 

t_libsccb_cfg h_alc5670_sccb={
	.sccb_num = 0,
};
#ifdef SCCB_GROUP
static u8 __attribute__ ((aligned(4))) sccb_grp_buf[32];
#endif

#ifdef BUILD_FOR_BA
void ertos_timedelay(u32 d10ms)
{
	volatile int d;
	for(d=0;d<d10ms*40000;d++);
}
#endif


static void sccb_wr_word(int addr, short data)
{
//	sccb_seqwrite(&h_alc5670_sccb, addr, 2, (u8*)&data);
	u8 tmp[2];
	tmp[0]=data>>8;
	tmp[1]=data&0xff;
	libsccb_seqwrite(&h_alc5670_sccb, addr, 2, tmp);
}

#if 1
static short sccb_rd_word(int addr){
	short ret;
//	sccb_seqread(&h_alc5670_sccb,addr,2, (u8*)&ret);
	u8 tmp[2];
	libsccb_seqread(&h_alc5670_sccb,addr,2, tmp);
	ret = (tmp[0]<<8)|tmp[1];
	return ret;
}
#endif

int codec_alc5670_init(t_codec_cpara *cp)
{
	E_AI_DIR dir = cp->dir;
	u32 master = cp->master;
	g_alc5670_setting.da_dvol.max= 0 ; 
	g_alc5670_setting.da_dvol.min= -65; 
	g_alc5670_setting.ad_dvol.max= 30; 
	g_alc5670_setting.ad_dvol.min= -17; 

	switch(dir)
	{
#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_DIR_PLAY:
			alc5670_playback_init(cp);
			break;
#endif
#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
		case AI_DIR_DUPLEX:
			alc5670_duplex_init(cp);
			break;
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_DIR_RECORD:
			alc5670_record_init(cp);
			break;
#endif
		default:
			debug_printf("codec_alc5670_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_alc5670_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			break;
		case AI_CTL_SR:
#if 0
			if(!host_ai_master)
			{
				s32 sr = *(s32 *)arg;
				volatile s32 d;
				debug_printf("set samplerate = %d\n",sr);
				alc5670_set_sr(sr);
			}else{
				debug_printf("Host AI is master,don't need set samplerate in alc5670\n");
			}
#endif
			break;

#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return alc5670_set_dac_dvol(vol);
			}
#endif

#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return alc5670_set_adc_dvol(vol);
			}
#endif

		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			sccb_wr_word(0x70, 0x8000 );//0 set slave mode

			sccb_wr_word(0x61, 0 );//0 //Power-off I2S1, DAC1_L/R, DAC2_L/R, ADC2_L, ADC1_L/R
			sccb_wr_word(0x62, 0 );//0 //Power-off ADC_Stereo1, ADC_mono_L/R filter, DSP, DAC_Stereo1, DAC_mono_L/R filter & pad power
			sccb_wr_word(0x63, 0xc0 );//0 //Power-off Vref1, main_bias, Lout1, mbias bandgap, HP_L/R, HP_AMP, Vref2 & LDO=1.25V
			sccb_wr_word(0x64, 0 );//0 //Power-off bst1/2/3, micbias1/2, PLL, bst_buffer1,2,3
			sccb_wr_word(0x65, 0x01 );//0 //Power-off outmix_L/R, recmix_L/R, recmix_M & LDO
			sccb_wr_word(0x66, 0 );//0 //Power-off hpomix_L/R, in_vol_L/R & micbias_vrefp_2/1

			debug_printf("alc5670 powered down\n");
			break;

		case AI_CTL_EQ:
			{
				debug_printf("eq not supported in alc5670\n");
			}
			break;

		case AI_CTL_PATH: 
			{
			u32 usecase = *(u32 *)arg;
			switch(usecase){
				case CODEC_PATH_SPKL:  //outdoor speaker
					sccb_wr_word(0x1B, 0x1033 );//0 //unmute DAC2_L, mute DAC2_R, DAC2_L=TxDC_L/R
					debug_printf("CODEC_PATH_SPKL\n");
					break;
				case CODEC_PATH_SPKR: //indoor speaker 
					sccb_wr_word(0x1B, 0x2033 );//0 //mute DAC2_L,unmute DAC2_R, DAC2_L=TxDC_L/R
					debug_printf("CODEC_PATH_SPKR\n");
					break;

				case CODEC_PATH_SPKS: //indoor&outdoor speaker 
					sccb_wr_word(0x1B, 0x0033 );//0 //unmute DAC2_L,unmute DAC2_R, DAC2_L=TxDC_L/R
					debug_printf("CODEC_PATH_SPKS\n");
					break;

				case CODEC_PATH_MICL: //outdoor mic
					sccb_wr_word(0x3C, 0x007B );//0 //Select bst2 IN2P
					sccb_wr_word(0x3E, 0x007B );//0 //Select bst2
					break;
				case CODEC_PATH_MICR: //indoor mic
					sccb_wr_word(0x3C, 0x0077 );//0 //Select bst2 IN3P
					sccb_wr_word(0x3E, 0x0077 );//0 //Select bst2
					break;
				default:
					break;
			}
			}
			break;

		default:
			debug_printf("unsupported ioctl cmd %d\n",ioctl);
			break;
	}
	return 0;
}

#ifdef ENABLE_DSP
#include "alc5670_dsp.c"
#endif

static void codec_init(int sr)
{
//TODO
	////#################Initial settings#####################
	sccb_wr_word(0x63, 0xE8FB );//0
	sccb_wr_word(0xFB, 0x0433 );//0
	sccb_wr_word(0x0A, 0x0005 );//0 //combo jack settings
	sccb_wr_word(0x0B, 0x28A7 );//0 //combo jack settings
#ifdef ALC5670_VC
	sccb_wr_word(0xFA, 0x0091 );//0 //I2S1 RX data from DxDP
#else
	sccb_wr_word(0xFA, 0x0891 );//0 //I2S1 RX data from DxDP
#endif
	sccb_wr_word(0xFC, 0x008C );//0 //MICBIAS manual mode
	sccb_wr_word(0x64, 0x0004 );//0
	sccb_wr_word(0x64, 0x0204 );//0
	sccb_wr_word(0x6A, 0x0037 );//0
	sccb_wr_word(0x6C, 0xFC00 );//0
	sccb_wr_word(0x6A, 0x0077 );//0
	sccb_wr_word(0x6C, 0x9F00 );//0
	sccb_wr_word(0x6A, 0x0037 );//0
	sccb_wr_word(0x6C, 0xB400 );//0
	sccb_wr_word(0x6A, 0x003D );//0
	sccb_wr_word(0x6C, 0x3640 );//0
	sccb_wr_word(0x79, 0x0101 );//0 //IF1_DAC2_L/R source

	//#################Clock settings#####################
	sccb_wr_word(0x80, 0x4000 );//0 //From PLL
	alc5670_set_pll(sr);
	sccb_wr_word(0x73, 0x0770 );//0 //System clock =12.288MHz

	sccb_wr_word(0x7F, 0x1001 );//0 //ASRC settings, DSP track I2S1
	sccb_wr_word(0x83, 0x7FFF );//0 //ASRC enable, I2S1, StereoDAC & all ADC
	sccb_wr_word(0x84, 0x1111 );//0 //ASRC settings, Stereo_DAC & Stereo1_ADC track I2S1
	sccb_wr_word(0x85, 0x1111 );//0 //ASRC settings, up/ down filter, monoL_ADC & monoR_ADC track I2S1

	//#################Power settings#####################
	sccb_wr_word(0x61, 0x98C6 );//0 //Power-on I2S1, DAC1_L/R, DAC2_L/R, ADC2_L, ADC1_L/R
	sccb_wr_word(0x62, 0xFE01 );//0 //Power-on ADC_Stereo1, ADC_mono_L/R filter, DSP, DAC_Stereo1, DAC_mono_L/R filter & pad power
	sccb_wr_word(0x63, 0xF8FB );//0 //Power-on Vref1, main_bias, Lout1, mbias bandgap, HP_L/R, HP_AMP, Vref2 & LDO=1.25V
	sccb_wr_word(0x64, 0xFE70 );//0 //Power-on bst1/2/3, micbias1/2, PLL, bst_buffer1,2,3
	sccb_wr_word(0x65, 0xCE01 );//0 //Power-on outmix_L/R, recmix_L/R, recmix_M & LDO
	sccb_wr_word(0x66, 0x0E00 );//0 //Power-on hpomix_L/R, in_vol_L/R & micbias_vrefp_2/1

}

static void Mixer_setting(int dir,int sr)
{
	//#################MIXER settings, I2C clock has to slower than 96KHz#####################
	if(dir == AI_DIR_RECORD || dir == AI_DIR_DUPLEX ){
		sccb_wr_word(0x0D, 0x0108 );//0 //BST2 gain +20dB
		sccb_wr_word(0x0E, 0x1000 );//0 //BST3 gain +20dB
#ifdef OUTDOOR
		sccb_wr_word(0x3C, 0x007B );//0 //Select bst2 IN2P
		sccb_wr_word(0x3E, 0x007B );//0 //Select bst2
#endif
#ifdef INDOOR
		sccb_wr_word(0x3C, 0x0077 );//0 //Select bst2 IN3P
		sccb_wr_word(0x3E, 0x0077 );//0 //Select bst2
#endif
		sccb_wr_word(0x27, 0x3820 );//0 //ADC1/2 to Stereo_ADC1
		sccb_wr_word(0x1C, 0x2F2F );//0 //Unmute stereo_ADC1, volume=0dB
		sccb_wr_word(0x1E, 0x0000 );//0 //Stereo1_bst=0dB
	}

#ifdef ENABLE_DSP
	if(sr == 16000){
		sccb_wr_word(0x2D, 0x2000 );//0 //REF=IF1_DAC2_L/R, SRC to RxDP bypass, TxDP=L/R, TxDC=L/R, SRC to TxDP bypass, TxDP slot=slot1, DSP bypass
	}else{//bypass
		sccb_wr_word(0x2D, 0x2003 );//0 //REF=IF1_DAC2_L/R, SRC to RxDP bypass, TxDP=L/R, TxDC=L/R, SRC to TxDP bypass, TxDP slot=slot1, DSP bypass
	}
#else //by-pass
	sccb_wr_word(0x2D, 0x2003 );//0 //REF=IF1_DAC2_L/R, SRC to RxDP bypass, TxDP=L/R, TxDC=L/R, SRC to TxDP bypass, TxDP slot=slot1, DSP bypass
#endif

	if(dir == AI_DIR_RECORD || dir == AI_DIR_DUPLEX ){
#ifdef ALC5670_VC
		sccb_wr_word(0xCD, 0x0010 );//0 //I2S1_ADC from Stereo1_ADC
#else
		sccb_wr_word(0xCD, 0x0000 );//0 //I2S1_ADC from Stereo1_ADC
#endif
	}

	if(dir == AI_DIR_PLAY || dir == AI_DIR_DUPLEX ){
#ifdef OUTDOOR
		sccb_wr_word(0x1B, 0x1033 );//0 //unmute DAC2_L, mute DAC2_R, DAC2_L=TxDC_L/R
#endif
#ifdef INDOOR
		sccb_wr_word(0x1B, 0x2033 );//0 //mute DAC2_L,unmute DAC2_R, DAC2_L=TxDC_L/R
#endif
#if defined(INDOOR) && defined(OUTDOOR)
		sccb_wr_word(0x1B, 0x0033 );//0 //mute DAC2_L,unmute DAC2_R, DAC2_L=TxDC_L/R
#endif
		sccb_wr_word(0x2A, 0x4646 );//0 //unmute DAC_L2/R2 to Stereo_DAC_MIXL
		sccb_wr_word(0x8F, 0x3100 );//0
		sccb_wr_word(0x8E, 0x8009 );//0
		sccb_wr_word(0x8E, 0x8019 );//0
		sccb_wr_word(0x90, 0x0772 );//0
		sccb_wr_word(0x91, 0x0E00 );//0 //CP voltage=1.8V
		sccb_wr_word(0x8E, 0x805D );//0
		sccb_wr_word(0x8E, 0x831D );//0
		sccb_wr_word(0xFB, 0x0733 );//0
		sccb_wr_word(0x02, 0x0808 );//0 //unmute HPO, analog gain=0dB
		sccb_wr_word(0x8E, 0x8019 );//0
		sccb_wr_word(0x45, 0xA00A );//0 //Unmute DACL1 to HPMIXL, unmute HPVOL to HPO
	}
	sccb_wr_word(0x70, 0x0000 );//0 //I2S1 master
	sccb_wr_word(0x77, 0x4000 );//0 //by joe, channel length 11:10 00’b: 16bit (For Slave Mode and Master Mode)
//	sccb_wr_word(0x73, 0x4770 );//100 //System clock=4.096MHz, I2S1 LRCK=16KHz
	alc5670_set_sr(sr);

	ertos_timedelay(10);

	sccb_wr_word(0x19, 0x7f7f );//DACL1 DACR1 digital volume -12dB
	sccb_wr_word(0x1a, 0x7f7f );//DACL2 DACR2 digital volume -12dB

//	debug_printf("11reg 0x19=0x%x\n",sccb_rd_word(0x19));
//	debug_printf("11reg 0x1a=0x%x\n",sccb_rd_word(0x1a));
#ifdef ENABLE_DSP
	if(sr == 16000){
		sccb_wr_word(0xE1, 0x22FB );//0 //DSP start
		sccb_wr_word(0xE2, 0x0000 );//0 //DSP start
		sccb_wr_word(0xE0, 0x3FCB );//0 //DSP start
	}
#endif
}

static void alc5670_set_pll(s32 sample_rate)
{
	/* PLL config, in master mode */
	switch(sample_rate){
		case 44100:
		case 22050:
		case 11025:
			sccb_wr_word(0x81 ,0x1F02 );//0 //MCLK=24MHz, PLL=22.5792MHz
			sccb_wr_word(0x82 ,0xF000 );//0 //MCLK=24MHz, PLL=22.5792MHz
			sccb_wr_word(0x82 ,0xF002 );//0 //MCLK=24MHz, PLL=22.5792MHz
			break;

		case 96000:
		case 48000:
		case 24000:
		case 32000:
		case 16000:
		case 12000:
		case 8000:
			sccb_wr_word(0x81, 0x1382 );//0 //MCLK=24MHz, PLL=24.576MHz
			sccb_wr_word(0x82, 0x8000 );//0 //MCLK=24MHz, PLL=24.576MHz
			sccb_wr_word(0x82, 0x8002 );//0 //MCLK=24MHz, PLL=24.576MHz	
			break;

		default:
			debug_printf("unsupported sample rate\n");
			break;
	}

	debug_printf("reg 0x81 = 0x%x\n",sccb_rd_word(0x81));
	debug_printf("reg 0x82 = 0x%x\n",sccb_rd_word(0x82));
}



static void alc5670_set_sr(s32 sample_rate)
{
	/* PLL config, in master mode */
	switch(sample_rate){
		case 44100: sccb_wr_word(0x73 ,0x1770 ); break;//sr = 22579.2/256/2 KHz
		case 22050: sccb_wr_word(0x73 ,0x3770 ); break;//sr = 22579.2/256/4 KHz
		case 11025: sccb_wr_word(0x73 ,0x5770 ); break;//sr = 22579.2/256/8 KHz

		case 48000: sccb_wr_word(0x73, 0x1770 );break;//0 //sr = 24576/256/2 KHz
		case 24000: sccb_wr_word(0x73, 0x3770 );break;//0 //sr = 24576/256/4 KHz
		case 32000: sccb_wr_word(0x73, 0x2770 );break;//0 //sr = 24576/256/3 KHz
		case 16000: sccb_wr_word(0x73, 0x4770 );break;//0 //sr = 24576/256/6 KHz
		case 12000: sccb_wr_word(0x73, 0x5770 );break;//0 //sr = 24576/256/8 KHz
		case 8000: sccb_wr_word(0x73, 0x6770 );break;//0 //sr = 24576/256/12 KHz

		default:
			debug_printf("unsupported sample rate\n");
			break;
	}
}

#ifdef CONFIG_AUDIO_ENC_EN  
/*
Stereo1 ADC Left/Right Channel Volume Control
00’h: -17.625dB
…
2F’h: 0dB
…
7F’h: +30dB, with 0.375dB/Step
*/
static s32 alc5670_set_adc_dvol(s32 vol){
	debug_printf("set adcvol = %ddB\n",vol);
	{
		u8 setval;
		s32 setvol;
		if(vol > 30)
		{
			setval = 0xff;
			setvol = 30;
			debug_printf("alc5670 not support digital volume bigger than 30 dB, default to 30dB\n");
		}
		else if(vol < -17)
		{
			setval = 0x7f;
			setvol = -17;
			debug_printf("alc5670 does not support digital volume less than -17dB,default to -17dB\n");
		}
		else
		{
			setvol = vol;
			setval = 0x2f + (vol*8/3);//0xc3:0dB, 0.5dB step
		}	
		//dODO
		sccb_wr_word(0x1C,(setval<<8)|setval);
		return setvol;
	}
}
#endif


#ifdef CONFIG_AUDIO_DEC_EN  
/*
DAC2 Left/Right Channel Digital Volume
00’h: -65.625dB
…
AF’h: 0dB, with 0.375dB/Step
*/
static s32 alc5670_set_dac_dvol(s32 vol){
	debug_printf("set dacvol = %ddB\n",vol);
	{
		u8 setval;
		u16 setval_16;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xaf;
			setvol = 0;
			debug_printf("alc5670 not support digital volume bigger than 0 dB, default to 0dB\n");
		}
		else if(vol < -65)
		{
			setval = 0x0;
			setvol = -65;
			debug_printf("alc5670 does not support digital volume less than -65dB,default to -65dB\n");
		}
		else
		{
			setval = 0xaf + vol*8/3 ;
			setvol = vol;
		}	


		sccb_wr_word(0x19, (setval<<8)|setval );//DACL1 DACR1 digital volume
		sccb_wr_word(0x1a, (setval<<8)|setval );//DACL2 DACR2 digital volume 

		return setvol;
	}
}

void alc5670_playback_init (t_codec_cpara *cp)
{
	h_alc5670_sccb.mode = SCCB_MODE8;
	h_alc5670_sccb.id = 0x38;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_alc5670_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_alc5670_sccb.clk = I2C_CLOCK;
//	h_sccb.sysclk = 24*1024*1024;
	h_alc5670_sccb.wr_check = 0;
	h_alc5670_sccb.timeout = 0;
	h_alc5670_sccb.retry = 1;
#ifdef SCCB_GROUP
	h_alc5670_sccb.buf_addr = sccb_grp_buf,
	h_alc5670_sccb.max_len = sizeof(sccb_grp_buf), //1k
#endif

	debug_printf("alc5670_playback_init\n");
	

	ertos_timedelay(50);


	libsccb_init(&h_alc5670_sccb);
	
	codec_init(cp->sr);

#ifdef ENABLE_DSP
	if(cp->sr == 16000){
		debug_printf("playback enter DSP\n");
#ifndef ALC5670_VC
		DSP_reset4CG();
		DSP_patch4CG();
#endif
		DSP_setting_initial();
		DSP_setting_AEC();
	}
#endif
	Mixer_setting(AI_DIR_DUPLEX,cp->sr);

}
#endif

#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
void alc5670_duplex_init(t_codec_cpara* cp)
{
	h_alc5670_sccb.mode = SCCB_MODE8;
	h_alc5670_sccb.id = 0x38;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_alc5670_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_alc5670_sccb.clk = I2C_CLOCK;
//	h_sccb.sysclk = 24*1024*1024;
	h_alc5670_sccb.wr_check = 0;
	h_alc5670_sccb.timeout = 0;
	h_alc5670_sccb.retry = 1;

#ifdef SCCB_GROUP
	h_alc5670_sccb.buf_addr = sccb_grp_buf,
	h_alc5670_sccb.max_len = sizeof(sccb_grp_buf), //1k
#endif

	debug_printf("alc5670_duplux_init\n");
	
	ertos_timedelay(50);

	libsccb_init(&h_alc5670_sccb);
	//TODO
	codec_init(cp->sr);

#ifdef ENABLE_DSP
	if(cp->sr == 16000){	
		debug_printf("duplex enter DSP\n");
#ifndef ALC5670_VC
		DSP_reset4CG();
		DSP_patch4CG();
#endif
		DSP_setting_initial();
		DSP_setting_AEC();
	}
#endif
	Mixer_setting(AI_DIR_DUPLEX,cp->sr);


}
#endif


#ifdef CONFIG_AUDIO_ENC_EN  
//#define BYPASS_TO_HP  //if define this, can hear from headphone,but cannot record to AI 
void alc5670_record_init(t_codec_cpara *cp)
{
	h_alc5670_sccb.mode = SCCB_MODE8;
	h_alc5670_sccb.id = 0x38;
//	h_sccb.rd_mode = RD_MODE_RESTART;
//	h_alc5670_sccb.rd_mode = RD_MODE_NORMAL;
	h_alc5670_sccb.clk = I2C_CLOCK;
//	h_sccb.sysclk = 24*1024*1024;
	h_alc5670_sccb.wr_check = 0;
	h_alc5670_sccb.timeout = 0;
	h_alc5670_sccb.retry = 1;

#ifdef SCCB_GROUP
	h_alc5670_sccb.buf_addr = sccb_grp_buf,
	h_alc5670_sccb.max_len = sizeof(sccb_grp_buf), //1k
#endif
	
	debug_printf("alc5670_record_init\n");
	

	ertos_timedelay(50);

	libsccb_init(&h_alc5670_sccb);
	codec_init(cp->sr);

#ifdef ENABLE_DSP
	if(cp->sr == 16000){	
		debug_printf("before init DSP\n");
#ifndef ALC5670_VC
		DSP_reset4CG();
		DSP_patch4CG();
#endif
		DSP_setting_initial();
		DSP_setting_AEC();
	}
#endif
	Mixer_setting(AI_DIR_RECORD,cp->sr);
}
#endif


static __acodec_cfg t_libacodec_setting g_alc5670_setting = {
	.early_init = NULL,
	.init = codec_alc5670_init,
	.ioctl = codec_alc5670_ioctl
};

