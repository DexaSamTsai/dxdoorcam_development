#include "includes.h"

#ifdef __KERNEL__
#include "liba_enc.h"
#include <linux/delay.h>
#include "libacodec.h"
#include "codec_setting_start.h"
//typedef s32 int;
//typedef u8 unsigned char;
#define I2C_DEV_ID 0x1a
#define CONFIG_AUDIO_ENC_EN
#define CONFIG_AUDIO_DEC_EN
#endif
static void wm8960_duplex_init(u32 master);
static void wm8960_set_sr(s32 sample_rate);
#ifdef CONFIG_AUDIO_DEC_EN  
static void wm8960_playback_init(u32 master);
static s32 wm8960_set_dac_dvol(s32 vol);
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
static void wm8960_record_init(u32 master);
static s32 wm8960_set_adc_dvol(s32 vol);
static void wm8960_tristat_adcdata(void);
#endif

static __acodec_cfg t_libacodec_setting g_wm8960_setting;

#ifndef __KERNEL__
t_libsccb_cfg h_wm8960_sccb;
#endif

int codec_wm8960_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_DIR_PLAY:
			wm8960_playback_init(master);
			break;
#endif
		case AI_DIR_DUPLEX:
			wm8960_duplex_init(master);
			break;
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_DIR_RECORD:
			wm8960_record_init(master);
			break;
#endif
		default:
			debug_printf("codec_wm8960_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_wm8960_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			break;
		case AI_CTL_SR:
		//	if(g_liba_dcfg.ai_master == 0){//codec ai working in master
		//	if(g_liba_ecfg.ai_master == 0){
			{
				s32 sr = *(s32 *)arg;
				volatile s32 d;
				debug_printf("samplerate = %d\n",sr);
				for(d=0;d<10000;d++); //time delay, if not delay, no sound when duplex. reason is unknown.
				wm8960_set_sr(sr);
			}
			break;

#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return wm8960_set_dac_dvol(vol);
			}
#endif

#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return wm8960_set_adc_dvol(vol);
			}
#endif

		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
#if 0
 R5   0x008 2wire_9_bit_data Write 0x34 * DAC Digital Soft Mute = Mute
R28   0x094 2wire_9_bit_data Write 0x34 * Enable POBCTRL and BUFDCOPEN and SOFT_ST
R25   0x000 2wire_9_bit_data Write 0x34 * Disable VMID and VREF, (allow output to discharge with S-Curve)
INSERT_DELAY_MS [400]				    * Delay 400mS to discharge HP outputs.
R26   0x000 2wire_9_bit_data Write 0x34 * Disable DACL, DACR, LOUT and ROUT1
R15   0x000 2wire_9_bit_data Write 0x34 *    Reset Device (default registers)
#endif
            sccb_wr(&h_wm8960_sccb, 5*2, 8);
            sccb_wr(&h_wm8960_sccb, 28*2,0x94);
            sccb_wr(&h_wm8960_sccb, 25*2,0);
#ifdef __KERNEL__
			mdelay(400);
#else
			newos_timedelay(40);				
#endif
            sccb_wr(&h_wm8960_sccb, 26*2,0);
            sccb_wr(&h_wm8960_sccb, 15*2,0);
			debug_printf("wm8960 powered down\n");
			break;

		case AI_CTL_EQ:
			{
				debug_printf("eq not supported in wm8960\n");
			}
			break;

#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_TRIS_ADCDAT:
			wm8960_tristat_adcdata();
			break;
#endif

		default:
			debug_printf("unsupported ioctl cmd %d\n",ioctl);
			break;
	}
	return 0;
}


static void wm8960_set_sr(s32 sample_rate)
{
	/* PLL config, in master mode */
	switch(sample_rate){
		case 44100:
		case 22050:
		case 11025:
			debug_printf("wm8960 set sr=%d\n",sample_rate);
			//SYSCLK = 11289600,MCLK=24M, N=7,K=0x86c226
			sccb_wr(&h_wm8960_sccb, 52*2, 0x37);//bit[3:0]=N ,bit4:prediv
			sccb_wr(&h_wm8960_sccb, 53*2, 0x86);//K
			sccb_wr(&h_wm8960_sccb, 54*2, 0xc2);//K
			sccb_wr(&h_wm8960_sccb, 55*2, 0x26);//K
			if(sample_rate==44100){
				sccb_wr(&h_wm8960_sccb, 4*2,0x5 );//DACDIV=0b000:SYSCLK/1,SYSCLKDIV=0b10,clksel=1:from PLL ===> 0b000000101
				//sccb_wr(&h_wm8960_sccb, 8*2,7 );//BCLKDIV=7:SYSCLK/8
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xc7 );//BCLKDIV=7:SYSCLK/8;DCLKDIV=16
			}else if(sample_rate==22050){
				sccb_wr(&h_wm8960_sccb, 4*2,0x15 );//DACDIV=0b010:SYSCLK/2,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000010101
				//sccb_wr(&h_wm8960_sccb, 8*2,0xa );//BCLKDIV=0xa:SYSCLK/16
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xca );//BCLKDIV=0xa:SYSCLK/16,DCLKDIV=16
			}else{//11025
				sccb_wr(&h_wm8960_sccb, 4*2,0x25 );//DACDIV=0b100:SYSCLK/4,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000100101
			//	sccb_wr(&h_wm8960_sccb, 8*2,0xf );//BCLKDIV=0xf:SYSCLK/32
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xcf );//BCLKDIV=0xf:SYSCLK/32,DCLKDIV=16
				debug_printf("sample rate=%d\n", sample_rate);
			}
			break;

		case 96000:
		case 48000:
		case 24000:
		case 32000:
		case 16000:
		case 12000:
		case 8000:
			//SYSCLK = 12288000,MCLK=24M, N=8,K=0x3126e8
			sccb_wr(&h_wm8960_sccb, 52*2, 0x38);//bit[3:0]=N ,bit4:prediv
			sccb_wr(&h_wm8960_sccb, 53*2, 0x31);//K
			sccb_wr(&h_wm8960_sccb, 54*2, 0x26);//K
			sccb_wr(&h_wm8960_sccb, 55*2, 0xe8);//K
			if(sample_rate==96000){
				sccb_wr(&h_wm8960_sccb, 4*2,0x1 );//DACDIV=0b000:SYSCLK/1,SYSCLKDIV=0b00,clksel=1:from PLL ===> 0b000000001
				//sccb_wr(&h_wm8960_sccb, 8*2,7 );//BCLKDIV=7:SYSCLK/8
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xc7 );//BCLKDIV=7:SYSCLK/8;DCLKDIV=16
			}else if(sample_rate==48000){
				sccb_wr(&h_wm8960_sccb, 4*2,0x5 );//DACDIV=0b000:SYSCLK/1,SYSCLKDIV=0b10,clksel=1:from PLL ===> 0b000000101
				//sccb_wr(&h_wm8960_sccb, 8*2,7 );//BCLKDIV=7:SYSCLK/8
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xc7 );//BCLKDIV=7:SYSCLK/8;DCLKDIV=16
			}else if(sample_rate==24000){
				sccb_wr(&h_wm8960_sccb, 4*2,0x15 );//DACDIV=0b010:SYSCLK/2,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000010101
				//sccb_wr(&h_wm8960_sccb, 8*2,0xa );//BCLKDIV=0xa:SYSCLK/16
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xca );//BCLKDIV=0xa:SYSCLK/16;DCLKDIV=16
			}else if(sample_rate==12000){
				sccb_wr(&h_wm8960_sccb, 4*2,0x25 );//DACDIV=0b100:SYSCLK/4,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000100101
				//sccb_wr(&h_wm8960_sccb, 8*2,0xf );//BCLKDIV=0xf:SYSCLK/32
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xcf );//BCLKDIV=0xf:SYSCLK/32;DCLKDIV=16
			}else if(sample_rate==32000){
				sccb_wr(&h_wm8960_sccb, 4*2,0xd );//DACDIV=0b001:SYSCLK/1.5,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000001101
				//sccb_wr(&h_wm8960_sccb, 8*2,0x9 );//BCLKDIV=0x9:SYSCLK/12
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xc9 );//BCLKDIV=9:SYSCLK/12;DCLKDIV=16
			}else if(sample_rate==16000){
				sccb_wr(&h_wm8960_sccb, 4*2,0x1d );//DACDIV=0b011:SYSCLK/3,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000011101
			//	sccb_wr(&h_wm8960_sccb, 8*2,0xc );//BCLKDIV=0xc:SYSCLK/24
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xcc );//BCLKDIV=0xc:SYSCLK/24;DCLKDIV=16
			}else{//8000
				sccb_wr(&h_wm8960_sccb, 4*2,0x35 );//DACDIV=0b110:SYSCLK/6,SYSCLKDIV=0b10,clksel=1,from PLL ===> 0b000110101
				//sccb_wr(&h_wm8960_sccb, 8*2,0xc );//BCLKDIV=0xc:SYSCLK/24			
				sccb_wr(&h_wm8960_sccb, 8*2+1,0xcc );//BCLKDIV=0xc:SYSCLK/24;DCLKDIV=16
			}
			break;
		default:
			debug_printf("unsupported sample rate\n");
			break;
	}
		
}

#ifdef CONFIG_AUDIO_ENC_EN  
static s32 wm8960_set_adc_dvol(s32 vol){
	debug_printf("set adcvol = %ddB\n",vol);
	//if(g_wm8960_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 30)
		{
			setval = 0xff;
			setvol = 30;
			debug_printf("wm8960 not support digital volume bigger than 30 dB, default to 30dB\n");
		}
		else if(vol < -97)
		{
			setval = 0;
			setvol = -97;
			debug_printf("wm8960 does not support digital volume less than -97dB,default to -97dB\n");
		}
		else
		{
			setvol = vol;
			setval = 0xc3 + (vol*2);//0xc3:0dB, 0.5dB step
		}	
		sccb_wr(&h_wm8960_sccb, 21*2+1, setval);
		sccb_wr(&h_wm8960_sccb, 22*2+1, setval);
		//g_wm8960_setting.dvol.cur = setvol;
		//g_liba_ecfg.adc_plus = setvol;
		return setvol;
	}
}
#endif


#ifdef CONFIG_AUDIO_DEC_EN  
/*
0000 0000 = Digital Mute
0000 0001 = -127dB
0000 0010 = -126.5dB
... 0.5dB steps up to
1111 1111 = 0dB
*/
static s32 wm8960_set_dac_dvol(s32 vol){
	debug_printf("set dacvol = %ddB\n",vol);
	//if(g_wm8960_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xff;
			setvol = 0;
			debug_printf("wm8960 not support digital volume bigger than 0 dB, default to 0dB\n");
		}
		else if(vol < -127)
		{
			setval = 0x0;
			setvol = -127;
			debug_printf("wm8960 does not support digital volume less than -127dB,default to -127dB\n");
		}
		else
		{
			setval = (vol + 127)*2 ;
			setvol = vol;
		}	
		//sccb_wr(&h_wm8960_sccb, 0x17, setval);
		//sccb_wr(&h_wm8960_sccb, 0x19, setval);
		sccb_wr(&h_wm8960_sccb, 10*2+1, setval);//LDAC volume 
		sccb_wr(&h_wm8960_sccb, 11*2+1, setval);//RDAC volume 
		//g_wm8960_setting.dvol.cur = setvol;
		//g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}
}


void wm8960_playback_init (u32 master)
{
#ifndef __KERNEL__
	h_wm8960_sccb.mode = SCCB_MODE8;
	h_wm8960_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
	h_wm8960_sccb.rd_mode = RD_MODE_NORMAL;
	h_wm8960_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8960_sccb.wr_check = 0;
	h_wm8960_sccb.timeout = 0;
	h_wm8960_sccb.retry = 1;
#endif

	debug_printf("wm8960_playback_init\n");
	
	g_wm8960_setting.dvol.max = 0; 	 //dB
	g_wm8960_setting.dvol.min = -127; //dB	

	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8960_sccb);
	sccb_wr(&h_wm8960_sccb, 0x0f*2, 0x00);//reset
	
    sccb_wr(&h_wm8960_sccb, 28*2, 0x94);//R28   0x094      0x34       *    Enable POBCTRL, SOFT_ST and BUFDCOPEN
	
	//sccb_wr(&h_wm8960_sccb, 26*2, 0x62);//R26   0x062      0x34       *    Enable LOUT1, ROUT1 and OUT3
	sccb_wr(&h_wm8960_sccb, 26*2, 0x7a);//R26   0x062      0x34       *    Enable LOUT1, ROUT1 SPKL, SPLR and OUT3
#ifdef __KERNEL__
	mdelay(50);
#else
	newos_timedelay(5);				//INSERT_DELAY_MS [50]
#endif
	sccb_wr(&h_wm8960_sccb, 25*2, 0x80);//R25   0x080      0x34       *    Enable VMID SEL = 2x50K Ohm Divider
#ifdef __KERNEL__
	mdelay(100);
#else
	newos_timedelay(10);				//INSERT_DELAY_MS [100]	      *    Allow VMID to charge
#endif
	sccb_wr(&h_wm8960_sccb, 25*2, 0xC0);//R25   0x0C0      0x34       *    Enable VREF. VMID SEL = 2x50K Ohm Divider remain.
    sccb_wr(&h_wm8960_sccb, 28*2, 0x10);//R28   0x010      0x34       *    Disable POBCTRL and SOFT_ST. BUFDCOPEN remain enabled

	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xe2);//R26   0x1E2      0x34       *    Enable DACL, DACR. LOUT1 ROUT1 and OUT3 remain enabled 
	sccb_wr(&h_wm8960_sccb, 26*2+1, 0xfa);//R26   0x1E2      0x34       *    Enable DACL, DACR. SPKL, SPKR, LOUT1 ROUT1 and OUT3 remain enabled 
	
    sccb_wr(&h_wm8960_sccb, 47*2, 0xc);//R47   0x00C      0x34       *    Enable left and right output mixers
			
	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xe1);//R26  0x1E1 2wire_9_bit_data  Write     0x34
	sccb_wr(&h_wm8960_sccb, 26*2+1, 0xf9);//R26  0x1E1 2wire_9_bit_data  Write     0x34
	sccb_wr(&h_wm8960_sccb, 51*2, 0xad);

	sccb_wr(&h_wm8960_sccb, 9*2,0x40 );//////////////////////////////////this is important,other wise master mode, no sound output
	sccb_wr(&h_wm8960_sccb, 34*2+1, 0x50);//R34   0x150      0x34       *    Enable Left DAC to left mixer
	sccb_wr(&h_wm8960_sccb, 37*2+1, 0x50);//R37   0x150      0x34       *    Enable Right DAC to right mixer

	sccb_wr(&h_wm8960_sccb, 2*2+1, 0x65);//R2   0x065      0x34       *    LOUT1VOL (HP) = -20dB
    sccb_wr(&h_wm8960_sccb, 3*2+1, 0x65);//R3   0x165      0x34       *    ROUT1VOL (HP) = -20dB, Enable OUT1VU, load volume settings to both left and right channels
	sccb_wr(&h_wm8960_sccb, 40*2+1, 0x7f);//Left speaker volume 
	sccb_wr(&h_wm8960_sccb, 41*2+1, 0x7f);//right speaker  volume 
    sccb_wr(&h_wm8960_sccb, 5*2, 0x0);//R5   0x000      0x34       *    DAC Digital Soft Mute = Unmute	
	
	////////////////////////////////////////
	
	/* power management, l/R out1 playback AI data */

	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xff);//enable DAC, pll 

	debug_printf("master = %d\n",master);
	if(master){//codec ai working in master
		sccb_wr(&h_wm8960_sccb, 7*2, 0x42); //16bits AI master
		//sccb_wr(&h_wm8960_sccb, 24*2, 0);//0xc);//switch to headphone 
	}
	else
	{//slave mode
		sccb_wr(&h_wm8960_sccb, 7*2, 0x2); //16bits AI slave,
		//sccb_wr(&h_wm8960_sccb, 24*2, 0xc);//switch to headphone 
	}
#if 0	
	sccb_wr(&h_wm8960_sccb, 10*2+1, 0xff);//LDAC volume 
	sccb_wr(&h_wm8960_sccb, 11*2+1, 0xff);//RDAC volume 
	//sccb_wr(&h_wm8960_sccb, 0x2*2, 0xff);//LOUT1 volume 
	//sccb_wr(&h_wm8960_sccb, 0x3*2, 0xff);//ROUT1 volume
	//sccb_wr(&h_wm8960_sccb, 0x5*2, 0x84);//DAC soft mute disable, enable de-emphasis

	sccb_wr(&h_wm8960_sccb, 40*2+1, 0x7f);//Left speaker volume 
	sccb_wr(&h_wm8960_sccb, 41*2+1, 0x7f);//right speaker  volume 
//	sccb_wr(&h_wm8960_sccb, 34*2+1, 0x00);//left DAC to left output mixer enable 
//	sccb_wr(&h_wm8960_sccb, 37*2+1, 0x00);//right DAC to right output mixer enable 
//	sccb_wr(&h_wm8960_sccb, 47*2, 0x0c);//enalbe left and right output mixer 
//	sccb_wr(&h_wm8960_sccb, 25*2, 0xff);//enalbe VREF 
	sccb_wr(&h_wm8960_sccb, 23*2, 1);//Slow clock enabled
	sccb_wr(&h_wm8960_sccb, 27*2, 0x8);// 
//	sccb_wr(&h_wm8960_sccb, 28*2, 0x80);// 
#endif
//    sccb_wr(&h_wm8960_sccb, 49*2, 0xf7); //R40 SPK_OP_EN

}
#endif

void wm8960_duplex_init(u32 master)
{
#ifndef __KERNEL__
	h_wm8960_sccb.mode = SCCB_MODE8;
	h_wm8960_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
	h_wm8960_sccb.rd_mode = RD_MODE_NORMAL;
	h_wm8960_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8960_sccb.wr_check = 0;
	h_wm8960_sccb.timeout = 0;
	h_wm8960_sccb.retry = 1;
#endif

	debug_printf("wm8960_duplux_init\n");
	
	g_wm8960_setting.dvol.max = 0; 	 //dB
	g_wm8960_setting.dvol.min = -127; //dB	

	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8960_sccb);
	sccb_wr(&h_wm8960_sccb, 0x0f*2, 0x00);//reset
	
    sccb_wr(&h_wm8960_sccb, 28*2, 0x94);//R28   0x094      0x34       *    Enable POBCTRL, SOFT_ST and BUFDCOPEN
	
	//sccb_wr(&h_wm8960_sccb, 26*2, 0x62);//R26   0x062      0x34       *    Enable LOUT1, ROUT1 and OUT3
    sccb_wr(&h_wm8960_sccb, 26*2, 0x7a);//R26   0x062      0x34       *    Enable LOUT1, ROUT1 SPKL, SPLR and O
#ifdef __KERNEL__
	mdelay(50);
#else
	newos_timedelay(5);				//INSERT_DELAY_MS [50]
#endif
	sccb_wr(&h_wm8960_sccb, 25*2, 0x80);//R25   0x080      0x34       *    VMID=50K, Enable AINL,AINR and MICBIAS,ADCL ADCR power up, VREF power up
#ifdef __KERNEL__
	mdelay(100);
#else
	newos_timedelay(10);				//INSERT_DELAY_MS [100]	      *    Allow VMID to charge
#endif
	sccb_wr(&h_wm8960_sccb, 25*2, 0xfe);//R25   0x0C0      0x34       *    Enable VREF. VMID SEL = 2x50K Ohm Divider remain.
    sccb_wr(&h_wm8960_sccb, 28*2, 0x10);//R28   0x010      0x34       *    Disable POBCTRL and SOFT_ST. BUFDCOPEN remain enabled

	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xe2);//R26   0x1E2      0x34       *    Enable DACL, DACR. LOUT1 ROUT1 and OUT3 remain enabled 
	sccb_wr(&h_wm8960_sccb, 26*2+1, 0xfa);//R26   0x1E2      0x34       *    Enable DACL, DACR. SPKL, SPKR, LOU
    
    sccb_wr(&h_wm8960_sccb, 47*2, 0x3c);//R47   0x00C      0x34       *    Enable left and right output mixers,Left/Right Input PGA 
			
	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xe1);//R26  0x1E1 2wire_9_bit_data  Write     0x34
	sccb_wr(&h_wm8960_sccb, 26*2+1, 0xf9);//R26  0x1E1 2wire_9_bit_data  Write     0x34
	sccb_wr(&h_wm8960_sccb, 51*2, 0xad);		

	sccb_wr(&h_wm8960_sccb, 9*2,0x40 );//////////////////////////////////this is important,other wise master mode, no sound output
	sccb_wr(&h_wm8960_sccb, 34*2+1, 0x50);//R34   0x150      0x34       *    Enable Left DAC to left mixer
	sccb_wr(&h_wm8960_sccb, 37*2+1, 0x50);//R37   0x150      0x34       *    Enable Right DAC to right mixer

	sccb_wr(&h_wm8960_sccb, 2*2+1, 0x65);//R2   0x065      0x34       *    LOUT1VOL (HP) = -20dB
    sccb_wr(&h_wm8960_sccb, 3*2+1, 0x65);//R3   0x165      0x34       *    ROUT1VOL (HP) = -20dB, Enable OUT1VU, load volume settings to both left and right channels
	sccb_wr(&h_wm8960_sccb, 40*2+1, 0x7f);//Left speaker volume 
	sccb_wr(&h_wm8960_sccb, 41*2+1, 0x7f);//right speaker  volume    
    sccb_wr(&h_wm8960_sccb, 5*2, 0x0);//R5   0x000      0x34       *    DAC Digital Soft Mute = Unmute	
	
	////////////////////////////////////////
	
	/* power management, l/R out1 playback AI data */

	//sccb_wr(&h_wm8960_sccb, 26*2+1, 0xff);//enable DAC, pll 

	if(master){//codec ai working in master
		sccb_wr(&h_wm8960_sccb, 7*2, 0x42); //16bits AI master
		//sccb_wr(&h_wm8960_sccb, 24*2, 0);//0xc);//switch to headphone 
	}
	else
	{//slave mode
		sccb_wr(&h_wm8960_sccb, 7*2, 0x2); //16bits AI slave,
		//sccb_wr(&h_wm8960_sccb, 24*2, 0xc);//switch to headphone 
	}
	
	
	//ADC init	
//   sccb_wr(&h_wm8960_sccb, 28*2, 0x94);//R28   0x094        *    Enable POBCTRL, SOFT_ST and BUFDCOPEN
//   sccb_wr(&h_wm8960_sccb, 25*2, 0xfe);//R25   0x0EA        *   VMID=50K, Enable AINL,AINR and MICBIAS,ADCL ADCR power up
//   sccb_wr(&h_wm8960_sccb, 26*2+1, 0x1);//R26   0x040        *   enable PLL, here must power up one DAC or both DAC, otherwise it can not record in master mode, why

//   sccb_wr(&h_wm8960_sccb, 47*2, 0x3c);//R47   0x028        *   Enable left output mixer, Enable Left/Right Input PGA 
   sccb_wr(&h_wm8960_sccb, 32*2+1, 0x78);//R32   0x168        *   Connect LINPUT2 to non-inverting PGA (LMP2), LMICBOOST = +20dB, Connect left input PGA to Left input mixer 
   sccb_wr(&h_wm8960_sccb, 33*2+1, 0x78);//R33   0x168        *   Connect RINPUT2 to non-inverting PGA (RMP2), RMICBOOST = +20dB, Connect right input PGA to Left input mixer 
   sccb_wr(&h_wm8960_sccb, 0*2+1, 0x1d);// R0   0x117        *   Unmute left input PGA (LINMUTE), Left Input PGA Vol = 6dB, Volume Update  
   sccb_wr(&h_wm8960_sccb, 1*2+1, 0x1d);// R1   0x117        *   Unmute right input PGA (RINMUTE), Right Input PGA Vol = 6dB, Volume Update  
 
 //  sccb_wr(&h_wm8960_sccb, 45*2, 0x80);//R45   0x080        *   Left Input Boost mixer to Left output mixer (LB2L0), Vol = 0dB 
 //  sccb_wr(&h_wm8960_sccb, 46*2, 0x80);//R45   0x080        *   Left Input Boost mixer to Left output mixer (LB2L0), Vol = 0dB 

   sccb_wr(&h_wm8960_sccb, 48*2, 0x3);//R48   0x003        *   Microphone Bias Voltage Control = 0.65 * AVDD 

	sccb_wr(&h_wm8960_sccb, 21*2+1, 0xc3);// left ADC volume 0dB  LADCVOL
    sccb_wr(&h_wm8960_sccb, 22*2+1, 0xc3);// right ADC volume 0dB RADCVOL
	
    //both recording channels use data from Left ADC,only left MIC needed 
	sccb_wr(&h_wm8960_sccb, 23*2+1, 0xc4);// ADC Data Output Select: left data = left ADC; right data =left ADC	
    sccb_wr(&h_wm8960_sccb, 49*2, 0xf7); //R40 SPK_OP_EN
}

#ifdef CONFIG_AUDIO_ENC_EN  
//#define BYPASS_TO_HP  //if define this, can hear from headphone,but cannot record to AI 
void wm8960_record_init(u32 master)
{
#ifndef __KERNEL__
	h_wm8960_sccb.mode = SCCB_MODE8;
	h_wm8960_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
//	h_wm8960_sccb.rd_mode = RD_MODE_NORMAL;
	h_wm8960_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8960_sccb.wr_check = 0;
	h_wm8960_sccb.timeout = 0;
	h_wm8960_sccb.retry = 1;
#endif
	
	debug_printf("wm8960_record_init\n");
	
	g_wm8960_setting.dvol.max = 30; 	 //dB
	g_wm8960_setting.dvol.min = -97; //dB	
	
	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8960_sccb);
	sccb_wr(&h_wm8960_sccb, 0x0f*2, 0x00);//reset

//   sccb_wr(&h_wm8960_sccb, 28*2, 0x94);//R28   0x094        *    Enable POBCTRL, SOFT_ST and BUFDCOPEN

   sccb_wr(&h_wm8960_sccb, 25*2, 0xfe);//R25   0x0EA        *   VMID=50K, Enable AINL,AINR and MICBIAS,ADCL ADCR power up
#ifdef BYPASS_TO_HP 
   //bypass to headphone 
   sccb_wr(&h_wm8960_sccb, 26*2, 0x61);//R26   0x040        *   Enable LOUT1 ,ROUT1,en PLL
#else
   sccb_wr(&h_wm8960_sccb, 26*2+1, 0x1);//R26   0x040        *   enable PLL, here must power up one DAC or both DAC, otherwise it can not record in master mode, why
#endif
   sccb_wr(&h_wm8960_sccb, 47*2, 0x3c);//R47   0x028        *   Enable left output mixer, Enable Left Input PGA 
   sccb_wr(&h_wm8960_sccb, 32*2+1, 0x78);//R32   0x168        *   Connect LINPUT2 to non-inverting PGA (LMP2), LMICBOOST = +20dB, Connect left input PGA to Left input mixer 
   sccb_wr(&h_wm8960_sccb, 33*2+1, 0x78);//R33   0x168        *   Connect RINPUT2 to non-inverting PGA (RMP2), RMICBOOST = +20dB, Connect right input PGA to Left input mixer 
   sccb_wr(&h_wm8960_sccb, 0*2+1, 0x1d);// R0   0x117        *   Unmute left input PGA (LINMUTE), Left Input PGA Vol = 6dB, Volume Update  
   sccb_wr(&h_wm8960_sccb, 1*2+1, 0x1d);// R1   0x117        *   Unmute right input PGA (RINMUTE), Right Input PGA Vol = 6dB, Volume Update  
 
   sccb_wr(&h_wm8960_sccb, 45*2, 0x80);//R45   0x080        *   Left Input Boost mixer to Left output mixer (LB2L0), Vol = 0dB 
   sccb_wr(&h_wm8960_sccb, 46*2, 0x80);//R45   0x080        *   Left Input Boost mixer to Left output mixer (LB2L0), Vol = 0dB 

#ifdef BYPASS_TO_HP   
   sccb_wr(&h_wm8960_sccb, 2*2+1, 0x79);// R2   0x179        *   ROUT1 Vol = 6dB, volume update enabled  
   sccb_wr(&h_wm8960_sccb, 3*2+1, 0x79);// R3   0x179        *   ROUT1 Vol = 6dB, volume update enabled  
#endif  
   
   sccb_wr(&h_wm8960_sccb, 48*2, 0x3);//R48   0x003        *   Microphone Bias Voltage Control = 0.65 * AVDD 

	if(master)
	{	// codec working in master mode
		sccb_wr(&h_wm8960_sccb, 7*2, 0x42); //16bits AI master
		//sccb_wr(&h_wm8960_sccb, 24*2, 0);//0xc);//switch to headphone 
	}else{
		//codec working in slave mode
		sccb_wr(&h_wm8960_sccb, 7*2, 0x2); //16bits AI slave,
		//sccb_wr(&h_wm8960_sccb, 24*2, 0xc);//switch to headphone 
	}
	
	sccb_wr(&h_wm8960_sccb, 9*2,0x40 );//////////////////////////////////this is important,other wise master mode, no sound output
	//sccb_wr(&h_wm8960_sccb, 48*2,0x11 ); set gpio reserved
	
	sccb_wr(&h_wm8960_sccb, 21*2+1, 0xc3);// left ADC volume 0dB  LADCVOL
    sccb_wr(&h_wm8960_sccb, 22*2+1, 0xc3);// right ADC volume 0dB RADCVOL
	
	//both channels use data from left ADC,only the left MIC needed
	sccb_wr(&h_wm8960_sccb, 23*2+1, 0xc4);// ADC Data Output Select: left data = left ADC; right data =left ADC
	
   return;
	
}

static void wm8960_tristat_adcdata(void)
{
#ifndef __KERNEL__
	h_wm8960_sccb.mode = SCCB_MODE8;
	h_wm8960_sccb.id = 0x34;
//	h_sccb.rd_mode = RD_MODE_RESTART;
//	h_wm8960_sccb.rd_mode = RD_MODE_NORMAL;
	h_wm8960_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_wm8960_sccb.wr_check = 0;
	h_wm8960_sccb.timeout = 0;
	h_wm8960_sccb.retry = 1;
#endif
	
	debug_printf("wm8960_record_init\n");
	
	g_wm8960_setting.dvol.max = 30; 	 //dB
	g_wm8960_setting.dvol.min = -97; //dB	
	
	/* set sccb device address '0011010', 8-bit address mode */
	libsccb_init(&h_wm8960_sccb);
	sccb_wr(&h_wm8960_sccb, 0x0f*2, 0x00);//reset
	
	sccb_wr(&h_wm8960_sccb, 24*2, 8);//tristate ADCDATA pin
}
#endif


static __acodec_cfg t_libacodec_setting g_wm8960_setting = {
	.early_init = NULL,
	.init = codec_wm8960_init,
	.ioctl = codec_wm8960_ioctl
	};

#ifdef __KERNEL__
#include "codec_setting_end.h"
#endif
