#include "includes.h"


static t_libsccb_cfg h_ti3100_sccb;
static __acodec_cfg t_libacodec_setting g_ti3100_setting;

static void ti3100_playback_init(u32 master);
static s32 ti3100_set_dvol(s32 vol);

int dac_ti3100_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
		case AI_DIR_PLAY:
			ti3100_playback_init(master);
			break;
		default:
			debug_printf("codec_ti3100_init configuration error\n");
			return -1;
	}
	return 0;
}

int dac_ti3100_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN: 
			break;
		case AI_CTL_SR:
			{
				unsigned char NDAC,MDAC;
				s32 sample_rate;
				sample_rate = *(s32*)arg;
				switch(sample_rate)
				{
					case 8000:    MDAC = 4; NDAC = 22; break;
					case 11025:   MDAC = 8; NDAC = 8; break;
					case 12000:   MDAC = 2; NDAC = 29; break;
					case 16000:   MDAC = 2; NDAC = 22; break;
					case 22050:   MDAC = 4; NDAC = 8; break;
					case 24000:   MDAC = 1; NDAC = 29; break;
					case 32000:   MDAC = 2; NDAC = 11; break;
					case 44100:   MDAC = 2; NDAC = 8; break;	
					case 48000:   MDAC = 2; NDAC = 7; break;	
					default:   	MDAC = 2; NDAC = 8; break;				
				}
				NDAC += 0x80;
				MDAC += 0x80;
				sccb_wr(&h_ti3100_sccb, 0x0b, NDAC);
				sccb_wr(&h_ti3100_sccb, 0x0c, MDAC);
			}
			break;
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32 *)arg;
				return ti3100_set_dvol(vol);
			}
			
		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			sccb_wr(&h_ti3100_sccb, 0x40, 0x0c); //left DAC and right DAC muted
			sccb_wr(&h_ti3100_sccb, 0x01, 0x01);//w 30 01 01
			break;
		case AI_CTL_EQ:
			break;
		default:
			break;
	}
	return 0;
}

//DAC digital volume 
/*
0111 1111�C0011 0001: Reserved. Do not use
0011 0000: Digital volume control = 24 dB
0010 1111: Digital volume control = 23.5 dB
0010 1110: Digital volume control = 23 dB
...
0000 0001: Digital volume control = 0.5 dB
0000 0000: Digital volume control = 0 dB
1111 1111: Digital volume control = �C0.5 dB
...
1000 0010: Digital volume control = �C63 dB
1000 0001: Digital volume control = �C63.5 dB
1000 0000: Reserved
*/
static s32 ti3100_set_dvol(s32 vol){
	//if(g_ti3100_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 24)
		{
			setval = 0x30;
			setvol = 24;
			debug_printf("ti3100 not support digital volume bigger than 24 dB(%ddB), default to 24dB\n",vol);
		}
		else if(vol < -63)
		{
			setval = 0x82;
			setvol = -63;
			debug_printf("ti3100 does not support digital volume less than -63dB(%ddB),default to -63dB\n",vol);
		}
		else
		{
			setvol = vol;
			setval = vol*2;
		}	

		sccb_wr(&h_ti3100_sccb, 0x41, setval);
		sccb_wr(&h_ti3100_sccb, 0x42, setval);
		//g_ti3100_setting.dvol.cur = setvol;
		//g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}	
}


static void ti3100_playback_init(u32 master)
{
		h_ti3100_sccb.mode = SCCB_MODE8;
		h_ti3100_sccb.id = 0x30;
		h_ti3100_sccb.clk = 400*1024;
		h_ti3100_sccb.wr_check = 0;
		h_ti3100_sccb.timeout = 0;
		h_ti3100_sccb.retry = 1;
		//libsccb_init_old(0x34, 0);
		libsccb_init(&h_ti3100_sccb);
		debug_printf("dac_ti3100_init\n");
		
		g_ti3100_setting.dvol.max = 24;	//dB
		g_ti3100_setting.dvol.min = -63; //dB
		
		// Key: w 30 XX YY ==> write to SCCB address 0x30, to register 0xXX, data 0xYY
		// // ==> comment delimiter
		//
		// The following list gives an example sequence of items that must be executed in the time
		// between powering the // device up and reading data from the device. Note that there are
		// other valid sequences depending on which features are used.
		// 1. Define starting point:
		// (a) Power up applicable external hardware power supplies
		// (b) Set register page to 0
		//
		
	 	sccb_wr(&h_ti3100_sccb, 0x00, 0x00);//w 30 00 00
		//
		// (c) Initiate SW reset (PLL is powered off as part of reset)
		//
		
		sccb_wr(&h_ti3100_sccb, 0x01, 0x01);//w 30 01 01
		
		//
// 2. Program clock settings
		// (a) Program PLL clock dividers P, J, D, R (if PLL is used)
		//
		// PLL_clkin = MCLK,codec_clkin = PLL_CLK
		sccb_wr(&h_ti3100_sccb, 0x04, 0x03);//w 30 04 03
    // J = 8
		sccb_wr(&h_ti3100_sccb, 0x06, 0x08);//w 30 06 08
		// D = 0000, D(13:8) = 0, D(7:0) = 0
		sccb_wr(&h_ti3100_sccb, 0x07, 0x00);//w 30 07 00 00 
		sccb_wr(&h_ti3100_sccb, 0x08, 0x00);//
		// (b) Power up PLL (if PLL is used)
		// PLL Power up, P = 1, R = 1
		//
		sccb_wr(&h_ti3100_sccb, 0x05, 0x91);//w 30 05 91
		//
		// (c) Program and power up NDAC
		//
		// NDAC is powered up and set to 8
		//
		// (d) Program and power up MDAC
		//
		// MDAC is powered up and set to 2
		//sccb_wr(&h_ti3100_sccb, 0x0c, 0x82);//w 30 0C 82
		//sccb_wr(&h_ti3100_sccb, 0x0b, 0x88);//w 30 0B 88

		//
		// (e) Program OSR value
		//
		// DOSR = 128, DOSR(9:8) = 0, DOSR(7:0) = 128
		sccb_wr(&h_ti3100_sccb, 0x0d, 0x00);////w 30 0D 00 80
		sccb_wr(&h_ti3100_sccb, 0x0e, 0x80);//
		
		// (f) Program AI word length if required (16, 20, 24, 32 bits)
		// and master mode (BCLK and WCLK are outputs)
		//if(master_mode)
		//	sccb_wr(&h_ti3100_sccb, 0x1b, 0x0c);//w 30 1B 0c
		//else
		// mode is AI, wordlength is 16, slave mode(blck is input,wclk is input) 
			sccb_wr(&h_ti3100_sccb, 0x1b, 0x00);//w 30 1B 00
		// (g) Program the processing block to be used
		//
		// Select Processing Block PRB_P11
		sccb_wr(&h_ti3100_sccb, 0x3c, 0x0b);//w 30 3C 0B
		sccb_wr(&h_ti3100_sccb, 0x00, 0x08);//w 30 00 08
		sccb_wr(&h_ti3100_sccb, 0x01, 0x04);//w 30 01 04
		sccb_wr(&h_ti3100_sccb, 0x00, 0x00);//w 30 00 00
		//
		// (h) Miscellaneous page 0 controls
		//
		// DAC => volume control thru pin disable
		sccb_wr(&h_ti3100_sccb, 0x74, 0x00);//w 30 74 00
// 3. Program analog blocks
		//
		// (a) Set register page to 1
		//
		sccb_wr(&h_ti3100_sccb, 0x00, 0x01);//w 30 00 01
		//
		// (b) Program common-mode voltage (defalut = 1.35 V)
		//
		sccb_wr(&h_ti3100_sccb, 0x1f, 0x04);//w 30 1F 04
		//
		// (c) Program headphone-specific depop settings (in case headphone driver is used)
		//
		// De-pop, Power on = 800 ms, Step time = 4 ms
		sccb_wr(&h_ti3100_sccb, 0x21, 0x4e);//w 30 21 4E
		//
		// (d) Program routing of DAC output to the output amplifier (headphone/lineout or speaker)
		//
		// LDAC routed to HPL out, RDAC routed to HPR out
		sccb_wr(&h_ti3100_sccb, 0x23, 0x44);//w 30 23 44
		//
		// (e) Unmute and set gain of output driver
		//
		// Unmute HPL, set gain = 0 db
		sccb_wr(&h_ti3100_sccb, 0x28, 0x06);//w 30 28 06
		// Unmute HPR, set gain = 0 dB
		sccb_wr(&h_ti3100_sccb, 0x29, 0x06);//w 30 29 06
		// Unmute Class-D, set gain = 18 dB
		sccb_wr(&h_ti3100_sccb, 0x2a, 0x1c);//w 30 2A 1C
		//
		// (f) Power up output drivers
		//
		// HPL and HPR powered up
		sccb_wr(&h_ti3100_sccb, 0x1f, 0xc2);//w 30 1F C2
		// Power-up Class-D driver
		sccb_wr(&h_ti3100_sccb, 0x20, 0x86);//w 30 20 86
		// Enable HPL output analog volume, set = -9 dB
		sccb_wr(&h_ti3100_sccb, 0x24, 0x92);//w 30 24 92
		// Enable HPR output analog volume, set = -9 dB
		sccb_wr(&h_ti3100_sccb, 0x25, 0x92);//w 30 25 92
		// Enable Class-D output analog volume, set = -9 dB
		sccb_wr(&h_ti3100_sccb, 0x26, 0x92);//w 30 26 92
		//
// 4. Apply waiting time determined by the de-pop settings and the soft-stepping settings
		// of the driver gain or poll page 1 / register 63
		//
// 5. Power up DAC
		// (a) Set register page to 0
		//
		sccb_wr(&h_ti3100_sccb, 0x00, 0x00);//w 30 00 00
		//
		// (b) Power up DAC channels and set digital gain
		//
		// Powerup DAC left and right channels (soft step enabled)
		sccb_wr(&h_ti3100_sccb, 0x3f, 0xd4);//w 30 3F D4
		//
		// DAC Left gain = -22 dB
		sccb_wr(&h_ti3100_sccb, 0x41, 0xd4);//w 30 41 D4
		// DAC Right gain = -22 dB
		sccb_wr(&h_ti3100_sccb, 0x42, 0xd4);//w 30 42 D4
		//
		// (c) Unmute digital volume control
		//
		// Unmute DAC left and right channels
		sccb_wr(&h_ti3100_sccb, 0x40, 0x00);//w 30 40 00	
		return;
}


static __acodec_cfg t_libacodec_setting g_ti3100_setting = {
	.early_init = NULL,
	.init = dac_ti3100_init,
	.ioctl = dac_ti3100_ioctl
	};


	

