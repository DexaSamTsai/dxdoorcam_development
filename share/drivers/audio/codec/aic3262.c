#include "includes.h"

#define DB

//#define AIC3262_EVM_BOARD

#define OUTPUT_TO_SPEAKER 

#ifdef CONFIG_AIC3262_USE_SIF
#define CODEC_REG_WRITE(A,D) sif_wr(A,D)
#define CODEC_REG_READ(A) sif_rd(A)
#endif

#ifdef CONFIG_AIC3262_USE_SCCB
#define CODEC_REG_WRITE(A,D) sccb_wr(&h_aic3262_sccb,A,D)
#define CODEC_REG_READ(A) sccb_rd(&h_aic3262_sccb,A)
#endif

#ifdef __KERNEL__
#include <linux/delay.h>
#include "libacodec.h"
#include "codec_setting_start.h"
#define I2C_DEV_ID 0x18
#endif

static __acodec_cfg t_libacodec_setting g_aic3262_setting;
static void aic3262_duplex_init(u32 master);
static void aic3262_set_sr(s32 sample_rate);
#ifdef CONFIG_AUDIO_DEC_EN  
static void aic3262_playback_init(u32 master);
static s32 aic3262_set_dac_dvol(s32 vol);
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
static void aic3262_record_init(u32 master);
static s32 aic3262_set_adc_dvol(s32 vol);
#endif

#ifndef __KERNEL__
#ifdef CONFIG_AIC3262_USE_SCCB
t_libsccb_cfg h_aic3262_sccb;
#endif

#ifdef CONFIG_AIC3262_USE_SIF
#include "libsif.h"
static t_libsif_cfg codec_libsif_cfg;
#define CS_LOW do{\
	if(codec_libsif_cfg.base_addr == SIF1_BASE){ \
		libsif1_cs_low();\
	}else{\
		libsif0_cs_low();\
	}\
}while(0)

#define CS_HIGH do{\
	if(codec_libsif_cfg.base_addr == SIF1_BASE){\
		libsif1_cs_high();\
	}else{\
		libsif0_cs_high();\
	}\
}while(0)
//to write a register(addr) with value(data)
void sif_wr(u8 addr, u8 data)
{
	CS_LOW;
	libsif_rw(&codec_libsif_cfg,addr*2);
	libsif_rw(&codec_libsif_cfg,data);
	CS_HIGH;
}

//to read a register
u8 sif_rd(u8 addr)
{
	u8 ret;
	CS_LOW;
	libsif_rw(&codec_libsif_cfg,addr*2+1);
	ret = libsif_rw(&codec_libsif_cfg,0);
	CS_HIGH;
	return ret;
}

//to write a block of continous registers that start with addr, 
//register number is len, values to be written is in data. 
static void sif_wr_block(u8 addr, u8* data,u32 len)
{
	int i;
	CS_LOW;
	libsif_rw(&codec_libsif_cfg,addr*2);
	for(i=0;i<len;i++)
		libsif_rw(&codec_libsif_cfg,data[i]);
	CS_HIGH;
}

static int libsif_init_internal(int in_clk, t_libsif_cfg *cfg, int dma, int master, int clk){
    int div;

	if(master){
		cfg->op_mode = SIF_MODE_MASTER;
	}else{
		cfg->op_mode = SIF_MODE_SLAVE;
	}

	if(dma){
#if 0
		cfg->op_dma = 1;

		//solution one: wait for DMA end
		cfg->dma_wait = 100; //wait for dma end 1 second

		//solution two: wait for SIF int
		cfg->intr_en = 0;
		cfg->sif_dma_irq = sif_dma_irq_handler;
#endif
	}else{
		cfg->op_dma = 0;
	}

	//for r2, sif_clk = (sys_clk/div5_sif)/(((read(SIF_BASE)&0xf)+1)*2)
    div = in_clk/clk;
	div = div/2;
	if(div > 1){
		div = div - 1;
	}
	if(div > 0xf){
		div = 0xf;
	}
	cfg->div = div;
	//in most cases, don't need to change these below
    cfg->trans_mode = SIF_TRANS_RT; //default receive-transmit
//	cfg->cpol = 0;
//	cfg->cpha = 0;
	cfg->slsb = 0;
	cfg->ss_mode = SIF_SS_MODE_1B;
	cfg->endian = 1;
	cfg->intr_fifo_cnt = SIF_INTR_FIFO_4W;
#ifdef CONFIG_DCACHE
	cfg->dc_enabled = 1;
#endif
	cfg->timeout = 0x1000000;
	cfg->signal_trig = NULL;

	//TODO: if cfg->intr_en, use sif end interrupt instead of DMA end interrupt.

	libsif_hw_init(cfg);

	return 0;
}

static int codec_libsif0_init(t_libsif_cfg *cfg, int dma, int master, int clk)
{
//	memset(cfg, 0, sizeof(t_libsif_cfg));
	cfg->base_addr = SIF_BASE;
	
	u32 clk_in;
    if(SIF_SEL_PAD())
	{
		clk_in = 24000000;
	}
	else
	{
		clk_in = mipipllclk_get()/SIF_DIV_GET();
	}

	return libsif_init_internal(clk_in, cfg, dma, master, clk);
}

static int codec_libsif1_init(t_libsif_cfg *cfg, int dma, int master, int clk)
{
//	memset(cfg, 0, sizeof(t_libsif_cfg));
	cfg->base_addr = SIF1_BASE;
	u32 clk_in;
    if(SIF1_SEL_PAD())
	{
		clk_in = 24000000;
	}
	else
	{
		clk_in = mipipllclk_get()/SIF1_DIV_GET();
	}

	return libsif_init_internal(clk_in, cfg, dma, master, clk);
}
#endif //#ifdef CONFIG_AIC3262_USE_SIF
#endif //#ifndef __KERNEL__


int codec_aic3262_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_DIR_PLAY:
			aic3262_playback_init(master);
			break;
#endif
		case AI_DIR_DUPLEX:
			aic3262_duplex_init(master);
			break;
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_DIR_RECORD:
			aic3262_record_init(master);
			break;
#endif

		default:
			debug_printf("codec_aic3262_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_aic3262_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			break;
		case AI_CTL_SR:
			//if(g_liba_dcfg.ai_master == 0)
			{//codec ai working in master
				s32 sr = *(s32 *)arg;
				debug_printf("samplerate = %d\n",sr);
				aic3262_set_sr(sr);
			}
			break;

#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return aic3262_set_dac_dvol(vol);
			}
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return aic3262_set_adc_dvol(vol);
			}
#endif

		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			break;
		case AI_CTL_EQ:
			{
				debug_printf("eq not supported in aic3262\n");
			}
			break;
		default:
			break;
	}
	return 0;
}


static void aic3262_set_sr(s32 sample_rate)
{
	/*configure PLL, MCLK1=24M*/
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x04, 0x33);//w 30 04 33 # Set ADC_CLKIN = PLL_CLK and DAC_CLKIN = PLL_CLK
	CODEC_REG_WRITE( 0x05, 0x00);//w 30 05 00 # Set PLL_CLKIN = MCLK1
	/* PLL config, in master mode */
	switch(sample_rate){
		case 44100:
		case 22050:
		case 11025:
			/*
			   PLL_IN_DIV=1 PLLP=2 PLLR=1 PLLJ=7 PLLD=560
			   PLLCLK=84.672M NDAC=3 MDAC=5 DAC_MOD_CLK=5.6448M,DAC_FS=44100
				w 30 06 91 07 02 30 02 #44K
				w 30 06 a1 07 02 30 02 #22K
				w 30 06 c1 07 02 30 02 #11K
			 */	
			switch(sample_rate){
				case 44100:CODEC_REG_WRITE( 0x06, 0x91);break;//PLLCLK=84.672M
				case 22050:CODEC_REG_WRITE( 0x06, 0xa1);break;//PLLCLK=42.336M
				case 11025:CODEC_REG_WRITE( 0x06, 0xc1);break;//PLLCLK=21.168M
				default: break;
			}
			CODEC_REG_WRITE( 0x07, 0x07);
			CODEC_REG_WRITE( 0x08, 0x02);
			CODEC_REG_WRITE( 0x09, 0x30);
			CODEC_REG_WRITE( 0x0a, 0x02);
			CODEC_REG_WRITE( 0x0b, 0x83);//w 30 0b 83 //NDAC=3
			CODEC_REG_WRITE( 0x0c, 0x85);//w 30 0c 85 //MDAC=5
			CODEC_REG_WRITE( 0x0d, 0x00);
			CODEC_REG_WRITE( 0x0e, 0x80);//w 30 0d 00 80//DOSR=128
			CODEC_REG_WRITE( 0x12, 0x83);//w 30 12 83 #NADC=3 and ADC_CLK pwr
			CODEC_REG_WRITE( 0x13, 0x85);//w 30 13 85 #MADC=5, ADC_M_CLK pwr
			CODEC_REG_WRITE( 0x14, 0x80);//w 30 14 80 #AOSR=128
			//sample_rate=PLLCLK/3/5/128
			break;

			/*
			   PLLR=1 PLLJ=7 PLLD=1680 NDAC=2 MDAC=7
			#w 30 0b 82
			#w 30 0c 87
			#w 30 0d 00 80
			 */
		case 48000:
		case 24000:
		case 32000:
		case 16000:
		case 12000:
		case 8000:
			//w 30 06 91 07 06 90 02//48
			//w 30 06 a1 07 06 90 02//24
			//w 30 06 c1 07 06 90 02//12
			//w 30 06 91 07 06 90 03//32
			//w 30 06 a1 07 06 90 03//16
			//w 30 06 c1 07 06 90 03//8
			CODEC_REG_WRITE( 0x07, 0x07);
			CODEC_REG_WRITE( 0x08, 0x06);
			CODEC_REG_WRITE( 0x09, 0x90);
			CODEC_REG_WRITE( 0x0b, 0x82);//			#w 30 0b 82 NDAC=2
			CODEC_REG_WRITE( 0x0c, 0x87);//			#w 30 0c 87 MDAC=7
			CODEC_REG_WRITE( 0x0d, 0x00);//			#w 30 0d 00 80
			CODEC_REG_WRITE( 0x0e, 0x80);//			#w 30 0d 00 80 DOSR=128
			CODEC_REG_WRITE( 0x12, 0x82);//w 30 12 82 #NADC=2 and ADC_CLK pwr
			CODEC_REG_WRITE( 0x13, 0x87);//w 30 13 87 #MADC=7, ADC_M_CLK pwr
			CODEC_REG_WRITE( 0x14, 0x80);//w 30 14 80 #AOSR=128
			switch(sample_rate){
				case 48000:
					CODEC_REG_WRITE( 0x06, 0x91);
					CODEC_REG_WRITE( 0x0a, 0x02); 
					break;
				case 24000:
					CODEC_REG_WRITE( 0x06, 0xa1);
					CODEC_REG_WRITE( 0x0a, 0x02); 
					break;
				case 12000:
					CODEC_REG_WRITE( 0x06, 0xc1);
					CODEC_REG_WRITE( 0x0a, 0x02); 
					break;
				case 32000:
					CODEC_REG_WRITE( 0x06, 0x91);
					CODEC_REG_WRITE( 0x0a, 0x03); 
					break;
				case 16000:
					CODEC_REG_WRITE( 0x06, 0xa1);
					CODEC_REG_WRITE( 0x0a, 0x03); 
					break;
				case 8000:
					CODEC_REG_WRITE( 0x06, 0xc1);
					CODEC_REG_WRITE( 0x0a, 0x03); 
					break;
				default:break;
			}
			break;

		default:
			debug_printf("unsupported sample rate\n");
			break;
	}
		
}

#ifdef CONFIG_AUDIO_ENC_EN  
/*
Left ADC Channel Volume Control
000 0000-110 0111: Reserved. Do not use.
110 1000: Left ADC Channel Volume = -12.0 dB
110 1001: Left ADC Channel Volume = -11.5 dB
110 1010: Left ADC Channel Volume = -11.0 dB

111 1111: Left ADC Channel Volume = -0.5 dB
000 0000: Left ADC Channel Volume = 0.0 dB
000 0001: Left ADC Channel Volume = 0.5 dB
...
010 0110: Left ADC Channel Volume = 19.0 dB
010 0111: Left ADC Channel Volume = 19.5 dB
010 1000: Left ADC Channel Volume = 20.0 dB
010 1001-111 1111: Reserved. Do not use.
Right ADC Channel is the same
*/
static s32 aic3262_set_adc_dvol(s32 vol){
	//if(g_aic3262_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 20)
		{
			setval = 0x68;
			setvol = 20;
			debug_printf("aic3262 not support digital volume bigger than 20 dB, default to 20dB\n");
		}
		else if(vol < -12)
		{
			setval = 0x28;
			setvol = -12;
			debug_printf("aic3262 does not support digital volume less than -12dB,default to -12dB\n");
		}
		else
		{
			s8 tmp;
			setvol = vol;
			tmp =  (vol*2);//0x0:0dB, 0.5dB step
			setval = (u8)(tmp&0x7f);
		}	
		
		CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
		CODEC_REG_WRITE( 0x53, setval); //Left ADC dvol
		CODEC_REG_WRITE( 0x54, setval); //Right ADC dvol
		//g_aic3262_setting.dvol.cur = setvol;
		//g_liba_ecfg.adc_plus = setvol;
		return setvol;

	}
}

#endif


#ifdef CONFIG_AUDIO_DEC_EN  
/*
0111 1111-0011 0001: Reserved. Do not use.
0011 0000: Digital Volume Control = +24dB
0010 1111: Digital Volume Control = +23.5dB
...
0000 0001: Digital Volume Control = +0.5dB
0000 0000: Digital Volume Control = 0.0dB
1111 1111: Digital Volume Control = -0.5dB
...
1000 0010: Digital Volume Control = -63dB
1000 0001: Digital Volume Control = -63.5dB
1000 0000: Reserved. Do not use.
*/
static s32 aic3262_set_dac_dvol(s32 vol){
	debug_printf("set dacvol = %ddB\n",vol);
	//if(g_aic3262_setting.dvol.cur != vol)
	{
		s8 setval;
		s32 setvol;
		if(vol > 24)
		{
			setval = 0x30;
			setvol = 24;
			debug_printf("aic3262 not support digital volume bigger than 24dB, default to 24dB\n");
		}
		else if(vol < -63)
		{
			setval = 0x82;
			setvol = -63;
			debug_printf("aic3262 does not support digital volume less than -63dB,default to -63dB\n");
		}
		else
		{
			setval = vol*2;
			setvol = vol;
		}	

		CODEC_REG_WRITE( 0x00, 0x00);//	w 30 00 00 # Initialize to Page 0
		CODEC_REG_WRITE( 0x41, setval);//LDAC volume 
		CODEC_REG_WRITE( 0x42, setval);//RDAC volume 
		//g_aic3262_setting.dvol.cur = setvol;
		//g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}
}


void aic3262_playback_init(u32 master)
{
#ifndef __KERNEL__
#ifdef CONFIG_AIC3262_USE_SIF
	u8 use_dma, trans_mode;// write_buf[128], read_buf[128];
	int sif_clk = 1000000;//set sif clock
	//not use dma and ov chip as sif master
	debug_printf("aic3262_playback_init\n");

#ifdef DB
	libgpio_config(21,0);
	libgpio_write(21, 1);

	//reset AIC
	libgpio_config(8,0);
	libgpio_write(8, 0);
	{volatile int d;for(d=0;d<100000;d++);}
	libgpio_write(8, 1);
#endif

	use_dma = SIF_OP_CPU;
	trans_mode = SIF_MODE_MASTER;
	memset(&codec_libsif_cfg, 0, sizeof(t_libsif_cfg));
	codec_libsif_cfg.cpol = 0;
	codec_libsif_cfg.cpha = 1;
#ifdef	CONFIG_AIC3262_SIF0_GPIO_NORMAL
#ifdef DB
	PIN_SIF0_SEL_CS1;
#endif
	PIN_SIF0_GPIO_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SC_NORMAL
	PIN_SIF0_SC_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_NORMAL
	PIN_SIF1_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_SI_NORMAL
	PIN_SIF1_SI_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
	debug_printf("CPOL=%d,CPHA=%d\n",codec_libsif_cfg.cpol,codec_libsif_cfg.cpha);

#endif //#ifdef CONFIG_AIC3262_USE_SIF

#ifdef CONFIG_AIC3262_USE_SCCB
	h_aic3262_sccb.mode = SCCB_MODE8;
	h_aic3262_sccb.id = 0x30;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_aic3262_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_aic3262_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_aic3262_sccb.wr_check = 0;
	h_aic3262_sccb.timeout = 0;
	h_aic3262_sccb.retry = 1;
	libsccb_init(&h_aic3262_sccb);
#endif
#endif

	debug_printf("aic3262_playback_init\n");
	
	g_aic3262_setting.dvol.max = 24; 	 //dB
	g_aic3262_setting.dvol.min = -63; //dB	
/*
#####################################################################################
# Headphone Playback, Setup A, High Performance, High Output Power, 48kHz Sampling Rate
# AVDDx_18, HVDD_18, CPVDD_18 = 1.8V; IOVDD, AVDD3_33, RECVDD_33 = 3.3V;
# SLVDD, SRVDD, SPK_V = 5V, DVdd = 1.8V
# MCLK = 12.288MHz
# PLL Disabled, DOSR = 128, PRB_P1
# Audio Serial Interface #1 used with WCLK & BCLK as inputs to the device
#####################################################################################
*/
/*
######################################################################################
# Software Reset
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//	w 30 00 00 # Initialize to Page 0
	CODEC_REG_WRITE( 0x7f, 0x00);//	w 30 7f 00 # Initialize to Book 0
	CODEC_REG_WRITE( 0x01, 0x01);//	w 30 01 01 # Initialize the device through software reset
	newos_timedelay(1);	//	d 1 # Delay 1 millisecond

/*
######################################################################################
# Power and Analog Configuration
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x01, 0x00);//w 30 01 00 # Disable weak AVDD to DVDD connection, make analog supplies available
	CODEC_REG_WRITE( 0x7a, 0x01);//w 30 7a 01 # REF charging time = 40ms
#if 0
	CODEC_REG_WRITE( 0x21, 0x28);//w 30 21 28 # CP divider = 4, 500kHz, Runs off 8MHz oscillator
	CODEC_REG_WRITE( 0x23, 0x10);//w 30 23 10 # Charge Pump to power up on Ground-Centered Headphone Power-up
	CODEC_REG_WRITE( 0x08, 0x00);//w 30 08 00 # Full chip CM = 0.9V (Setup A)
	CODEC_REG_WRITE( 0x03, 0x00);//w 30 03 00 # PTM_P3, High Performance (Setup A)
	CODEC_REG_WRITE( 0x04, 0x00);//w 30 04 00 # PTM_P3, High Performance (Setup A)
#endif
/*
######################################################################################
# Audio Serial Interface Routing Configuration - Audio Serial Interface #1
# ASI #1 playback
# configure BCLK and WCLK, direction
#####################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x04);//w 30 00 04 # Select Page 4
	CODEC_REG_WRITE( 0x01, 0x00);//w 30 01 00 # SA mode, 16-bit
	CODEC_REG_WRITE( 0x0a, 0x25);//w 30 0a 25 //Dir ASI1->BCLK1, ASI1->WCLK1 Master mode,BCLK and WCLK always on
	CODEC_REG_WRITE( 0x08, 0x50);//w 30 08 50 # Left Channel DAC and Primary ASI's Right channel data to Right Channel DAC
	CODEC_REG_WRITE( 0x0b, 0x01);//w 30 0b 01  //BCLK source DAC_MOD_CLK=5.6448MHz
	CODEC_REG_WRITE( 0x0c, 0x84);//w 30 0c 84  //BCLK DIV =4  BCLK=DAC_MOD_CLK/4
	CODEC_REG_WRITE( 0x0e, 0x00);//w 30 0e 00 //BCLK Out:from ASI1BDIV ,WCLK out:use DAC_FS
	CODEC_REG_WRITE( 0x10, 0x00);//w 30 10 00 //ADC WCLK is same as DAC WCLK,ADC BCLK is same as DAC BCLK (Default 4-wire Interface)

/*
######################################################################################
# Signal Processing Settings
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3c, 0x01);//w 30 3c 01 # Set the DAC Mode to PRB_P1 (Setup A)
	//CODEC_REG_WRITE( 0x3c, 0x07);//w 30 3c 01 # Set the DAC Mode to PRB_P7 (Setup A)
#ifdef OUTPUT_TO_SPEAKER 
/*####################################################################################
# Output Channel Configuration
####################################################################################*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x03, 0x04);//w 30 03 00 # Set PTM mode for Left DAC to PTM_P3 (default, writing here optional)
	CODEC_REG_WRITE( 0x04, 0x04);//w 30 04 00 # Set PTM mode for Right DAC to PTM_P3 (default, writing here optional)
	CODEC_REG_WRITE( 0x16, 0xc3);//w 30 16 c3 # Enable DAC to LOL/R routing and power-up LOL/R
	CODEC_REG_WRITE( 0x2e, 0x00);//w 30 2E 00 # Route LOL to SPK-Left @ 0dB
	CODEC_REG_WRITE( 0x2f, 0x00);//w 30 2F 00 # Route LOR to SPK-Right @ 0dB
	CODEC_REG_WRITE( 0x30, 0x11);//w 30 30 11 # Set Left Speaker Gain @ 6dB, Right Speaker Gain @ 6dB
	CODEC_REG_WRITE( 0x2d, 0x03);//w 30 2D 03 # Power-up Stereo Speaker

	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3f, 0xc0);//w 30 3f c0 # Power up the Left and Right DAC Channels
	CODEC_REG_WRITE( 0x40, 0x00);//w 30 40 00 # Unmute the DAC digital volume control
#else // output to headphone
/*
######################################################################################
# Output Channel Configuration
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3f, 0xc0);//w 30 3f c0 # Power up the Left and Right DAC Channels with route the Primary ASI's left channel data to Left DAC and right channel to Right DAC
	CODEC_REG_WRITE( 0x40, 0x00);//w 30 40 00 # Unmute the DAC digital volume control
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x09, 0x00);//w 30 09 00 # HP Sizing = 100% (Setup A)
	CODEC_REG_WRITE( 0x1f, 0x85);//w 30 1f 85 # Headphone in Ground-centered Mode, HPL Gain=5dB (Setup A)
	CODEC_REG_WRITE( 0x20, 0x85);//w 30 20 85 # HPR To have same gain as HPL, set to 5dB (Setup A)
	CODEC_REG_WRITE( 0x1b, 0x33);//w 30 1b 33 # Enable DAC to HPL/R and power-up HPL/R
#endif
}
#endif

void aic3262_duplex_init(u32 master)
{
#ifndef __KERNEL__
#ifdef CONFIG_AIC3262_USE_SIF
	u8 use_dma, trans_mode;// write_buf[128], read_buf[128];
	int sif_clk = 1000000;//set sif clock
	//not use dma and ov chip as sif master
	debug_printf("aic3262_duplex_init\n");

#ifdef DB
    //en_audio
	libgpio_config(21,0);
	libgpio_write(21, 1);

	//reset AIC
	libgpio_config(8,0);
	libgpio_write(8, 0);
	{volatile int d;for(d=0;d<10000;d++);}
	libgpio_write(8, 1);
#endif

	use_dma = SIF_OP_CPU;
	trans_mode = SIF_MODE_MASTER;
	memset(&codec_libsif_cfg, 0, sizeof(t_libsif_cfg));
	codec_libsif_cfg.cpol = 0;
	codec_libsif_cfg.cpha = 1;
#ifdef	CONFIG_AIC3262_SIF0_GPIO_NORMAL
#ifdef DB
	PIN_SIF0_SEL_CS1;
#endif
	PIN_SIF0_GPIO_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SC_NORMAL
	PIN_SIF0_SC_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_NORMAL
	PIN_SIF1_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_SI_NORMAL
	PIN_SIF1_SI_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
	debug_printf("CPOL=%d,CPHA=%d\n",codec_libsif_cfg.cpol,codec_libsif_cfg.cpha);
#endif

#ifdef CONFIG_AIC3262_USE_SCCB
	h_aic3262_sccb.mode = SCCB_MODE8;
	h_aic3262_sccb.id = 0x30;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_aic3262_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_aic3262_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_aic3262_sccb.wr_check = 0;
	h_aic3262_sccb.timeout = 0;
	h_aic3262_sccb.retry = 1;
	libsccb_init(&h_aic3262_sccb);
#endif
#endif

	debug_printf("aic3262_duplux_init\n");
	
	g_aic3262_setting.dvol.max = 24; 	 //dB
	g_aic3262_setting.dvol.min = -63; //dB	

/*
######################################################################################
# Software Reset
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//	w 30 00 00 # Initialize to Page 0
	CODEC_REG_WRITE( 0x7f, 0x00);//	w 30 7f 00 # Initialize to Book 0
	CODEC_REG_WRITE( 0x01, 0x01);//	w 30 01 01 # Initialize the device through software reset
	newos_timedelay(1);	//	d 1 # Delay 1 millisecond

/*
######################################################################################
# Power and Analog Configuration
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x01, 0x00);//w 30 01 00 # Disable weak AVDD to DVDD connection, make analog supplies available
	CODEC_REG_WRITE( 0x7a, 0x01);//w 30 7a 01 # REF charging time = 40ms
	CODEC_REG_WRITE(0x79,0x33);//w 30 79 33 # Set the quick charge of input coupling cap for analog inputs (joe added for duplex ADC)
#if 0
	CODEC_REG_WRITE( 0x21, 0x28);//w 30 21 28 # CP divider = 4, 500kHz, Runs off 8MHz oscillator
	CODEC_REG_WRITE( 0x23, 0x10);//w 30 23 10 # Charge Pump to power up on Ground-Centered Headphone Power-up
	CODEC_REG_WRITE( 0x08, 0x00);//w 30 08 00 # Full chip CM = 0.9V (Setup A)
	CODEC_REG_WRITE( 0x03, 0x00);//w 30 03 00 # PTM_P3, High Performance (Setup A)
	CODEC_REG_WRITE( 0x04, 0x00);//w 30 04 00 # PTM_P3, High Performance (Setup A)
#endif
/*
######################################################################################
# Audio Serial Interface Routing Configuration - Audio Serial Interface #1
# ASI #1 playback
# configure BCLK and WCLK, direction
#####################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x04);//w 30 00 04 # Select Page 4
	CODEC_REG_WRITE( 0x01, 0x00);//w 30 01 00 # SA mode, 16-bit
	CODEC_REG_WRITE( 0x0a, 0x25);//w 30 0a 25 //Dir ASI1->BCLK1, ASI1->WCLK1 Master mode,BCLK and WCLK always on
	CODEC_REG_WRITE( 0x08, 0x50);//w 30 08 50 # Left Channel DAC and Primary ASI's Right channel data to Right Channel DAC
	CODEC_REG_WRITE( 0x0b, 0x01);//w 30 0b 01  //BCLK source DAC_MOD_CLK=5.6448MHz
	CODEC_REG_WRITE( 0x0c, 0x84);//w 30 0c 84  //BCLK DIV =4  BCLK=DAC_MOD_CLK/4
	CODEC_REG_WRITE( 0x0e, 0x00);//w 30 0e 00 //BCLK Out:from ASI1BDIV ,WCLK out:use DAC_FS
	CODEC_REG_WRITE( 0x10, 0x00);//w 30 10 00 //ADC WCLK is same as DAC WCLK,ADC BCLK is same as DAC BCLK (Default 4-wire Interface)

/*
######################################################################################
# Signal Processing Settings
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3c, 0x01);//w 30 3c 01 # Set the DAC Mode to PRB_P1 (Setup A)
//	CODEC_REG_WRITE( 0x3d, 0x07);// w 30 3d 07 # Set the ADC PRB Mode to PRB_R7(joe add for duplex ADC)
	CODEC_REG_WRITE( 0x3d, 0x01);// w 30 3d 01 # Set the ADC PRB Mode to PRB_R1(joe add for duplex ADC)
#ifdef OUTPUT_TO_SPEAKER
/*####################################################################################
# Output Channel Configuration
####################################################################################*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x03, 0x00);//w 30 03 00 # Set PTM mode for Left DAC to PTM_P3 (default, writing here optional)
	CODEC_REG_WRITE( 0x04, 0x00);//w 30 04 00 # Set PTM mode for Right DAC to PTM_P3 (default, writing here optional)
	CODEC_REG_WRITE( 0x16, 0xc3);//w 30 16 c3 # Enable DAC to LOL/R routing and power-up LOL/R
	CODEC_REG_WRITE( 0x2e, 0x00);//w 30 2E 00 # Route LOL to SPK-Left @ 0dB
	CODEC_REG_WRITE( 0x2f, 0x00);//w 30 2F 00 # Route LOR to SPK-Right @ 0dB
	CODEC_REG_WRITE( 0x30, 0x11);//w 30 30 11 # Set Left Speaker Gain @ 6dB, Right Speaker Gain @ 6dB
	CODEC_REG_WRITE( 0x2d, 0x03);//w 30 2D 03 # Power-up Stereo Speaker
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3f, 0xc0);//w 30 3f c0 # Power up the Left and Right DAC Channels
	CODEC_REG_WRITE( 0x40, 0x00);//w 30 40 00 # Unmute the DAC digital volume control
#else //output to headphone
/*
######################################################################################
# Output Channel Configuration
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x00);//w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x3f, 0xc0);//w 30 3f c0 # Power up the Left and Right DAC Channels with route the Primary ASI's left channel data to Left DAC and right channel to Right DAC
	CODEC_REG_WRITE( 0x40, 0x00);//w 30 40 00 # Unmute the DAC digital volume control
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x09, 0x00);//w 30 09 00 # HP Sizing = 100% (Setup A)
	CODEC_REG_WRITE( 0x1f, 0x85);//w 30 1f 85 # Headphone in Ground-centered Mode, HPL Gain=5dB (Setup A)
	CODEC_REG_WRITE( 0x20, 0x85);//w 30 20 85 # HPR To have same gain as HPL, set to 5dB (Setup A)
	CODEC_REG_WRITE( 0x1b, 0x33);//w 30 1b 33 # Enable DAC to HPL/R and power-up HPL/R
#endif
#if 0
/* joe following added for duplex ADC
######################################################################################
# ADC Input Channel Configuration --- IN2L / IN2R
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x08, 0x04);//w 30 08 04 # Set the input common mode to 0.75V
	CODEC_REG_WRITE( 0x34, 0x20);//w 30 34 20 # Route IN2L and CM1 to LEFT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x36, 0x80);//w 30 36 80
	CODEC_REG_WRITE( 0x37, 0x20);//w 30 37 20 # Route IN2R and CM1 to RIGHT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x39, 0x80);//w 30 39 80
	CODEC_REG_WRITE( 0x3B, 0x0C);//w 30 3B 0C # Left Channel Analog ADC PGA = 6 dB -> Overall Channel Gain of 0dB
	CODEC_REG_WRITE( 0x3c, 0x0c);//  w 30 3C 0C # Right Channel Analog ADC PGA = 6 dB -> Overall Channel Gain of 0dB
	CODEC_REG_WRITE( 0x3d, 0x00);//  w 30 3D 00 # ADC Analog programmed for PTM_R4
	CODEC_REG_WRITE( 0x00, 0x00);//  w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x51, 0xc0);//  w 30 51 C0 # Power-up ADC Channel
	CODEC_REG_WRITE( 0x52, 0x00);//  w 30 52 00 # Unmute ADC channel and Fine Gain = 0dB
#else
/*
######################################################################################
# ADC Input Channel Configuration --- IN1L
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//	w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x08, 0x05);//	w 30 08 00 # Set the input common mode to 0.9V
#ifdef AIC3262_EVM_BOARD
	//on AIC3262 EVM board, use headset_mic to record
	CODEC_REG_WRITE( 0x33, 0x40);//	w 30 33 40 # Mic Bias ext enabled, Source = Avdd, 1.62V
#else
	CODEC_REG_WRITE( 0x33, 0x04);//	w 30 33 40 # Mic Bias enabled, Source = Avdd, 1.62V
#endif
	CODEC_REG_WRITE( 0x34, 0x80);//	w 30 34 80 # Route IN1L and CM1 to LEFT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x36, 0x80);//	w 30 36 80
	CODEC_REG_WRITE( 0x3B, 0x3C);//	w 30 3B 3C # Left Channel Analog ADC PGA = 30 dB
	CODEC_REG_WRITE( 0x3D, 0x00);//	w 30 3D 00 # ADC Analog programmed for PTM_R4
	CODEC_REG_WRITE( 0x00, 0x00);//	w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x51, 0xc0);//	w 30 51 C0 # Power-up ADC Channel
	CODEC_REG_WRITE( 0x52, 0x00);//	w 30 52 00 # Unmute ADC channel and Fine Gain = 0dB
#endif
}

#ifdef CONFIG_AUDIO_ENC_EN  
void aic3262_record_init(u32 master)
{
#ifndef __KERNEL__
#ifdef CONFIG_AIC3262_USE_SIF
	u8 use_dma, trans_mode;// write_buf[128], read_buf[128];
	int sif_clk = 1000000;//set sif clock

#ifdef DB
	libgpio_config(21,0);
	libgpio_write(21, 1);

	//reset AIC
	libgpio_config(8,0);
	libgpio_write(8, 0);
	{volatile int d;for(d=0;d<10000;d++);}
	libgpio_write(8, 1);
#endif


	//not use dma and ov chip as sif master
	use_dma = SIF_OP_CPU;
	trans_mode = SIF_MODE_MASTER;
	memset(&codec_libsif_cfg, 0, sizeof(t_libsif_cfg));
	codec_libsif_cfg.cpol = 0;
	codec_libsif_cfg.cpha = 1;

#ifdef	CONFIG_AIC3262_SIF0_GPIO_NORMAL
#ifdef DB
	PIN_SIF0_SEL_CS1;
#endif
	PIN_SIF0_GPIO_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SC_NORMAL
	PIN_SIF0_SC_NORMAL;	
	codec_libsif0_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_NORMAL
	PIN_SIF1_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
#ifdef	CONFIG_AIC3262_SIF1_SI_NORMAL
	PIN_SIF1_SI_NORMAL;	
	codec_libsif1_init(&codec_libsif_cfg, use_dma, trans_mode, sif_clk);
#endif
	debug_printf("CPOL=%d,CPHA=%d\n",codec_libsif_cfg.cpol,codec_libsif_cfg.cpha);
#endif //#ifdef CONFIG_AIC3262_USE_SIF

#ifdef CONFIG_AIC3262_USE_SCCB
	h_aic3262_sccb.mode = SCCB_MODE8;
	h_aic3262_sccb.id = 0x30;
//	h_sccb.rd_mode = RD_MODE_RESTART;
	h_aic3262_sccb.rd_mode = RD_MODE_NORMAL;
	h_aic3262_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_aic3262_sccb.wr_check = 0;
	h_aic3262_sccb.timeout = 0;
	h_aic3262_sccb.retry = 1;
	libsccb_init(&h_aic3262_sccb);
#endif
#endif
	
	debug_printf("---aic3262_record_init\n");
	
	g_aic3262_setting.dvol.max = 20; 	 //dB
	g_aic3262_setting.dvol.min = -12; //dB	
/*
######################################################################################
# ADC Stereo Record - Lower Power
# SE input signal from IN2L/IN2R
# AVDDx_18, HVDD_18, CPVDD_18 = 1.8V; IOVDD, AVDD3_33, RECVDD_33 = 3.3V
# SLVDD, SRVDD, SPK_V = 5V, DVdd = 1.8V
# MCLK = 12.288, Fs = 48kHz
# PLL Disabled, AOSR = 128, PTM_R1
# CM = 0.75
# Primary AI Interface used with WCLK & BCLK as inputs to the device
######################################################################################
*/

/*
######################################################################################
# Software Reset
######################################################################################
*/
		CODEC_REG_WRITE(0x00,0x00);//w 30 00 00 # Initialize to Page 0
		CODEC_REG_WRITE(0x7f,0x00);//w 30 7f 00 # Initialize to Book 0
		CODEC_REG_WRITE(0x01,0x01);//w 30 01 01 # Initialize the device through software reset
		newos_timedelay(1);	//	d 1 # Delay 1 millisecond
/*
######################################################################################
# Power and Analog Configuration
######################################################################################
*/
		CODEC_REG_WRITE(0x00,0x01);//w 30 00 01 # Select Page 1
		CODEC_REG_WRITE(0x01,0x00);//w 30 01 00 # Disable weak AVDD to DVDD connection, make analog supplies available
		CODEC_REG_WRITE(0x7a,0x01);//w 30 7a 01 # REF charging time = 40ms
		CODEC_REG_WRITE(0x79,0x33);//w 30 79 33 # Set the quick charge of input coupling cap for analog inputs

/*
######################################################################################
# Audio Serial Interface Routing Configuration - Audio Serial Interface #1
# ASI #1 recording
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x04);//w 30 00 04 # Select Page 4
	CODEC_REG_WRITE( 0x01, 0x00);// w 30 01 00 # Audio Serial Interface #1 is set to AI mode, 16-bit
	CODEC_REG_WRITE( 0x0a, 0x25);//w 30 0a 25 //Dir ASI1->BCLK1, ASI1->WCLK1 Master mode,BCLK and WCLK always on
	//  w 30 0a 00 # Route ASI#1 WCLK and BCLK to WCLK1 pin and BCLK1 pin
	CODEC_REG_WRITE( 0x0b, 0x03);//w 30 0b 03//ADC_MOD_CLK
	CODEC_REG_WRITE( 0x0c, 0x84);//w 30 0c 84//DIV=4 BCLK=ADC_MOD_CLK/4
	CODEC_REG_WRITE( 0x0e, 0x01);//w 30 0e 01 //WCLK use ADC_FS,BCLK Out:from ASI1BDIV
	CODEC_REG_WRITE( 0x10, 0x00);//w 30 10 00 //ADC WCLK is same as DAC WCLK,ADC BCLK is same as DAC BCLK (Default 4-wire Interface)
/*
######################################################################################
# Signal Processing Settings
######################################################################################
*/
	 CODEC_REG_WRITE( 0x00, 0x00);// w 30 00 00 # Select Page 0
//	 CODEC_REG_WRITE( 0x3d, 0x07);// w 30 3d 07 # Set the ADC PRB Mode to PRB_R7
	 CODEC_REG_WRITE( 0x3d, 0x01);// w 30 3d 01 # Set the ADC PRB Mode to PRB_R1

#if 0
/*
######################################################################################
# ADC Input Channel Configuration --- IN2L / IN2R
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x08, 0x04);//w 30 08 04 # Set the input common mode to 0.75V
	CODEC_REG_WRITE( 0x34, 0x20);//w 30 34 20 # Route IN2L and CM1 to LEFT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x36, 0x80);//w 30 36 80
	CODEC_REG_WRITE( 0x37, 0x20);//w 30 37 20 # Route IN2R and CM1 to RIGHT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x39, 0x80);//w 30 39 80
	CODEC_REG_WRITE( 0x3B, 0x0C);//w 30 3B 0C # Left Channel Analog ADC PGA = 6 dB -> Overall Channel Gain of 0dB
	CODEC_REG_WRITE( 0x3c, 0x0c);//  w 30 3C 0C # Right Channel Analog ADC PGA = 6 dB -> Overall Channel Gain of 0dB
	CODEC_REG_WRITE( 0x3d, 0x00);//  w 30 3D 00 # ADC Analog programmed for PTM_R4
	CODEC_REG_WRITE( 0x00, 0x00);//  w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x51, 0xc0);//  w 30 51 C0 # Power-up ADC Channel
	CODEC_REG_WRITE( 0x52, 0x00);//  w 30 52 00 # Unmute ADC channel and Fine Gain = 0dB
#else
/*
######################################################################################
# ADC Input Channel Configuration --- IN1L
######################################################################################
*/
	CODEC_REG_WRITE( 0x00, 0x01);//	w 30 00 01 # Select Page 1
	CODEC_REG_WRITE( 0x08, 0x05);//	w 30 08 00 # Set the input common mode to 0.9V
#ifdef AIC3262_EVM_BOARD
	//on AIC3262 EVM board, use headset_mic to record
	CODEC_REG_WRITE( 0x33, 0x40);//	w 30 33 40 # Mic Bias ext enabled, Source = Avdd, 1.62V
#else
	CODEC_REG_WRITE( 0x33, 0x04);//	w 30 33 40 # Mic Bias enabled, Source = Avdd, 1.62V
#endif
	CODEC_REG_WRITE( 0x34, 0x80);//	w 30 34 80 # Route IN1L and CM1 to LEFT ADCPGA with 20K input impedance
	CODEC_REG_WRITE( 0x36, 0x80);//	w 30 36 80
	CODEC_REG_WRITE( 0x3B, 0x3C);//	w 30 3B 3C # Left Channel Analog ADC PGA = 30 dB
	CODEC_REG_WRITE( 0x3D, 0x00);//	w 30 3D 00 # ADC Analog programmed for PTM_R4
	CODEC_REG_WRITE( 0x00, 0x00);//	w 30 00 00 # Select Page 0
	CODEC_REG_WRITE( 0x51, 0xc0);//	w 30 51 C0 # Power-up ADC Channel
	CODEC_REG_WRITE( 0x52, 0x00);//	w 30 52 00 # Unmute ADC channel and Fine Gain = 0dB
#endif
   return;
	
}
#endif
static __acodec_cfg t_libacodec_setting g_aic3262_setting = {
	.early_init = NULL,
	.init = codec_aic3262_init,
	.ioctl = codec_aic3262_ioctl
	};

#ifdef __KERNEL__
#include "codec_setting_end.h"
#endif
