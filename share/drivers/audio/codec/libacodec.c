#include "includes.h"

static u32 codec_inited = 0;

t_libacodec_setting *p_libacodec_setting = NULL;

extern t_libacodec_setting  acodec_cfg_start;
extern t_libacodec_setting  acodec_cfg_end;
void libacodec_earlyinit(void)
{
	if(p_libacodec_setting == NULL){
		if(&acodec_cfg_start != &acodec_cfg_end){
			p_libacodec_setting = (t_libacodec_setting *)(&acodec_cfg_start);
		}else{
			p_libacodec_setting = NULL;
			syslog(LOG_ERR, "No Audio CODEC was selected in config file\n");
		}
	}

	if(p_libacodec_setting && p_libacodec_setting->early_init){
		p_libacodec_setting->early_init();
	}
}

void libacodec_init(t_codec_cpara *cp)
{
	if(p_libacodec_setting == NULL){
		if(&acodec_cfg_start != &acodec_cfg_end){
			p_libacodec_setting = (t_libacodec_setting *)(&acodec_cfg_start);
		}else{
			p_libacodec_setting = NULL;
			syslog(LOG_ERR, "No Audio CODEC was selected in config file\n");
		}
	}

	if(!codec_inited){
		s32 tmp;	
		E_AI_DIR dir = cp->dir; 

		if(p_libacodec_setting && p_libacodec_setting->init){
			p_libacodec_setting->init(cp);
		}

		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			tmp = cp->ch;
			p_libacodec_setting->ioctl(AI_CTL_CHN,&tmp);

			tmp = cp->sr;
			p_libacodec_setting->ioctl(AI_CTL_SR,&tmp);

			if(dir == AI_DIR_PLAY || dir == AI_DIR_DUPLEX){
				tmp = cp->dac_dvol;
				cp->dac_dvol = p_libacodec_setting->ioctl(AI_CTL_DAC_DVOL,&tmp);

				//enable PA
				board_ioctl(BIOC_AU_PA_ON, 1, NULL);	
			}

			if(dir == AI_DIR_RECORD || dir == AI_DIR_DUPLEX){
				tmp = cp->adc_dvol;
				cp->adc_dvol = p_libacodec_setting->ioctl(AI_CTL_ADC_DVOL,&tmp);
			}
		}
		codec_inited = 1;
	}
}


void libacodec_set_dacvol(s32 vol)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_DAC_DVOL,&vol);
		}
	}
}


void libacodec_set_adcvol(s32 vol)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_ADC_DVOL,&vol);
		}
	}
}

void libacodec_mute_adc(s32 mute)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_MUTE_ADC,&mute);
		}
	}
}

void libacodec_mute_dac(s32 mute)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_MUTE_DAC,&mute);
		}
	}
}

void libacodec_set_sr(s32 sr)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_SR,&sr);
		}
	}
}



void libacodec_pwrdown(void)
{
	if(codec_inited){
		if(p_libacodec_setting && p_libacodec_setting->ioctl){
			p_libacodec_setting->ioctl(AI_CTL_PWRDWN,0);
		}

		//disable PA
		board_ioctl(BIOC_AU_PA_ON, 0, NULL);	

		codec_inited = 0;
	}
}

