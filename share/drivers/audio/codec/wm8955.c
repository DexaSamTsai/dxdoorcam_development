#include "includes.h"


static __acodec_cfg t_libacodec_setting g_wm8955_setting;
static t_libsccb_cfg h_wm8955_sccb;
static void wm8955_playback_init(u32 master);
static void wm8955_set_sr(u32 sample_rate);
static s32 wm8955_set_dvol(s32 dvol);

int dac_wm8955_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
		case AI_DIR_PLAY:
			wm8955_playback_init(master);
			break;
		default:
			debug_printf("codec_wm8955_init configuration error\n");
			return -1;
	}
	return 0;
}

int dac_wm8955_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN: 
			break;

		case AI_CTL_SR:
			{//codec working in master mode
				s32 sr = *(s32 *)arg;
				wm8955_set_sr(sr);
			}
			break;

		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return wm8955_set_dvol(vol);
			}
		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			wm8955_set_dvol(0);
			sccb_wr(&h_wm8955_sccb, 0x0a, 0x08);
			break;
		case AI_CTL_EQ:
			break;
		default:
			break;
	}
	return 0;
}

#define PLL_MODE
static void wm8955_set_sr(u32 sample_rate)
{
#ifdef USB_MODE
	switch(sample_rate){
		case 48000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x01);
			break;
		case 44100:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x23);
			break;
		case 32000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x19);
			break;
		case 24000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x39);
			break;
		case 22050:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x37);
			break;
		case 16000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x15);
			break;
		case 12000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x11);
			break;
		case 11025:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x33);
			break;
		case 8000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x05);
			break;
		default:
			break;
	}
#endif
	
#ifdef PLL_MODE 
	switch(sample_rate){
		case 48000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x0);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
		case 44100:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x20); ///< reg8 samplingrate
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);  ///< 2B
			sccb_wr(&h_wm8955_sccb, 0x58, 0xe8);  ///< 2c
			sccb_wr(&h_wm8955_sccb, 0x5a, 0xd8); ///< 2D
			sccb_wr(&h_wm8955_sccb, 0x5c, 0x89);  ///< 2E
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);  ///< 3B
			break;
		case 32000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x18);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
			break;
		case 24000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
			break;
		case 22050:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x34);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x58, 0xe8);  ///< 2c
			sccb_wr(&h_wm8955_sccb, 0x5a, 0xd8); ///< 2D
			sccb_wr(&h_wm8955_sccb, 0x5c, 0x89);  ///< 2E
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);  ///< 3B
			break;
		case 16000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x14);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
			break;
		case 12000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x10);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
			break;
		case 11025:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x30);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x58, 0xe8);  ///< 2c
			sccb_wr(&h_wm8955_sccb, 0x5a, 0xd8); ///< 2D
			sccb_wr(&h_wm8955_sccb, 0x5c, 0x89);  ///< 2E
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);  ///< 3B
			break;
		case 8000:
			sccb_wr(&h_wm8955_sccb, 0x10, 0x4);
			sccb_wr(&h_wm8955_sccb, 0x57, 0x38);
			sccb_wr(&h_wm8955_sccb, 0x59, 0x03);
			sccb_wr(&h_wm8955_sccb, 0x5a, 0x24);
			sccb_wr(&h_wm8955_sccb, 0x5d, 0xba);
			sccb_wr(&h_wm8955_sccb, 0x76, 0x80);
			break;
		default:
			break;
	}
#endif
}

/*
0000 0000 = Digital Mute
0000 0001 = -127dB
0000 0010 = -126.5dB
... 0.5dB steps up to
1111 1111 = 0dB
*/
static s32 wm8955_set_dvol(s32 vol){
	//if(g_wm8955_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xff;
			setvol = 0;
			debug_printf("wm8955 not support digital volume bigger than 0 dB, default to 0dB\n");
		}
		else if(vol < -127)
		{
			setval = 0x1;
			setvol = -127;
			debug_printf("wm8955 does not support digital volume less than -127dB,default to -127dB\n");
		}
		else
		{
			setvol = vol;
			setval = (vol + 127)*2 + 1;
		}	
		sccb_wr(&h_wm8955_sccb, 0x15, setval);
		sccb_wr(&h_wm8955_sccb, 0x17, setval);
		//g_wm8955_setting.dvol.cur = setvol;
		//g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}
}


static void wm8955_playback_init(u32 master){
	h_wm8955_sccb.mode = SCCB_MODE8;
	h_wm8955_sccb.id = 0x34;
	h_wm8955_sccb.clk = 400*1024;
	h_wm8955_sccb.wr_check = 0;
	h_wm8955_sccb.timeout = 0;
	h_wm8955_sccb.retry = 1;
	//libsccb_init_old(0x34, 0);
	libsccb_init(&h_wm8955_sccb);
	//WriteReg32 (REG_SCCB_CTL, 0x1000 | 0x34);	

	g_wm8955_setting.dvol.max = 0; 	 //dB
	g_wm8955_setting.dvol.min = -127; //dB	
	
	///< default value
	sccb_wr(&h_wm8955_sccb, 0x1e, 0x0);
	
	///< DAC control
	sccb_wr(&h_wm8955_sccb, 0x0a, 0x00);
	
	///< default setting of wm8955
	///< power management
	sccb_wr(&h_wm8955_sccb, 0x32, 0xc0);
#if defined(SPEAKER_EN)
	sccb_wr(&h_wm8955_sccb, 0x35, 0x98);  ///< speaker
#elif defined(MONOOUT_EN)
	sccb_wr(&h_wm8955_sccb, 0x35, 0x84);  ///< speaker

	//mono mixer
	sccb_wr(&h_wm8955_sccb, 0x4d, 0x0);
	sccb_wr(&h_wm8955_sccb, 0x4f, 0x0);
	//mono volume
	sccb_wr(&h_wm8955_sccb, 0x54, 0x7f);
#else
	sccb_wr(&h_wm8955_sccb, 0x35, 0xe0);  ///< headphone
#endif
	//sccb_wr(&h_wm8955_sccb, 0x35, 0xf8);   ///< headphone and speaker
	
	///<left mixer and right mixer
	sccb_wr(&h_wm8955_sccb, 0x45, 0x50);
	sccb_wr(&h_wm8955_sccb, 0x4b, 0x50);
	
	///< speaker
	sccb_wr(&h_wm8955_sccb, 0x05, 0x79);
	sccb_wr(&h_wm8955_sccb, 0x07, 0x79);
	//if(!tv_en){
#ifdef SPEAKER_EN
		sccb_wr(&h_wm8955_sccb, 0x51, 0x7f);
		sccb_wr(&h_wm8955_sccb, 0x53, 0x7f);
		sccb_wr(&h_wm8955_sccb, 0x30, 0x50);
#endif
	//}

//	if(master_mode)
//			sccb_wr(&h_wm8955_sccb, 0xe, 0x42); //AI master mode
//	else
	///< audio interface ,AI Slave mode
	sccb_wr(&h_wm8955_sccb, 0xe, 0x02);

	sccb_wr(&h_wm8955_sccb, 0x18, 0x0f);
	//sccb_wr(&h_wm8955_sccb, 0x18, 0x06);
	sccb_wr(&h_wm8955_sccb, 0x20, 0x0f);

	return;
}


static __acodec_cfg t_libacodec_setting g_wm8955_setting = {
	.early_init = NULL,
	.init = dac_wm8955_init,
	.ioctl = dac_wm8955_ioctl
	};
