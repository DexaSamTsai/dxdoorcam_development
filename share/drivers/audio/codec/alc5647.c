#include "includes.h"

#define CONFIG_AUDIO_DEC_EN
#define CONFIG_AUDIO_ENC_EN

#define I2C_CLOCK (50*1024)
#define SCCB_GROUP

static void alc5647_set_sr(s32 sample_rate);
static void alc5647_set_pll(s32 sample_rate);
#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
static void alc5647_duplex_init(t_codec_cpara *cp);
#endif
#ifdef CONFIG_AUDIO_DEC_EN  
static void alc5647_playback_init(t_codec_cpara *cp);
static s32 alc5647_set_dac_dvol(s32 vol);
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
static void alc5647_record_init(t_codec_cpara *cp);
static s32 alc5647_set_adc_dvol(s32 vol);
#endif

static __acodec_cfg t_libacodec_setting g_alc5647_setting; 

t_libsccb_cfg h_alc5647_sccb={
	.sccb_num = 3,
};
#ifdef SCCB_GROUP
static u8 __attribute__ ((aligned(4))) sccb_grp_buf[32];
#endif

#ifdef BUILD_FOR_BA
void ertos_timedelay(u32 d10ms)
{
	volatile int d;
	for(d=0;d<d10ms*40000;d++);
}
#endif


static void sccb_wr_word(int addr, short data)
{
//	sccb_seqwrite(&h_alc5647_sccb, addr, 2, (u8*)&data);
	u8 tmp[2];
	tmp[0]=data>>8;
	tmp[1]=data&0xff;
	libsccb_seqwrite(&h_alc5647_sccb, addr, 2, tmp);
}

#if 1
static short sccb_rd_word(int addr){
	short ret;
//	sccb_seqread(&h_alc5647_sccb,addr,2, (u8*)&ret);
	u8 tmp[2];
	libsccb_seqread(&h_alc5647_sccb,addr,2, tmp);
	ret = (tmp[0]<<8)|tmp[1];
	return ret;
}
#endif

int codec_alc5647_init(t_codec_cpara *cp)
{
	E_AI_DIR dir = cp->dir;
	u32 master = cp->master;
	g_alc5647_setting.da_dvol.max= 0 ; 
	g_alc5647_setting.da_dvol.min= -65; 
	g_alc5647_setting.ad_dvol.max= 30; 
	g_alc5647_setting.ad_dvol.min= -17; 

	h_alc5647_sccb.mode = SCCB_MODE8;
	h_alc5647_sccb.id = 0x36;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_alc5647_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_alc5647_sccb.clk = I2C_CLOCK;
//	h_sccb.sysclk = 24*1024*1024;
	h_alc5647_sccb.wr_check = 0;
	h_alc5647_sccb.timeout = 0;
	h_alc5647_sccb.retry = 1;

	debug_printf("codec alc5647 init\n");
#ifdef SCCB_GROUP
	h_alc5647_sccb.buf_addr = sccb_grp_buf,
	h_alc5647_sccb.max_len = sizeof(sccb_grp_buf), //1k
#endif

	ertos_timedelay(50);
	libsccb_init(&h_alc5647_sccb);

	sccb_wr_word(0x0, 0x000 );//SW reset
	sccb_wr_word(0x73, 0x000 );//16bits(32FS)
	sccb_wr_word(0xfa, 0x2061 );//Enable MCLK gating control:bit0=1

	if(master){
		debug_printf("codec alc5647 master\n");
		sccb_wr_word(0x70, 0x0000 );//I2S master mode ,16bits
	}else{
		debug_printf("codec alc5647 slave\n");
		sccb_wr_word(0x70, 0x8000 );//I2S slave mode,16bits
	}
	switch(dir)
	{
#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_DIR_PLAY:
			alc5647_playback_init(cp);
			break;
#endif
#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
		case AI_DIR_DUPLEX:
			alc5647_duplex_init(cp);
			break;
#endif
#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_DIR_RECORD:
			alc5647_record_init(cp);
			break;
#endif
		default:
			debug_printf("codec_alc5647_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_alc5647_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			break;
		case AI_CTL_SR:
			{
				s32 sr = *(s32 *)arg;
				volatile s32 d;
				alc5647_set_sr(sr);
			}
			break;

#ifdef CONFIG_AUDIO_DEC_EN  
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return alc5647_set_dac_dvol(vol);
			}
#endif

#ifdef CONFIG_AUDIO_ENC_EN  
		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return alc5647_set_adc_dvol(vol);
			}
#endif

		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			debug_printf("alc5647 powered down\n");
			sccb_wr_word(0x0, 0x000 );//SW reset
			break;

		case AI_CTL_EQ:
			{
				debug_printf("eq not supported in alc5647\n");
			}
			break;

		default:
			debug_printf("unsupported ioctl cmd %d\n",ioctl);
			break;
	}
	return 0;
}


static void alc5647_set_pll(s32 sample_rate)
{
	short reg0x64;	

	sccb_wr_word(0x80 ,0x4008 );//MCLK=24MHz,en pre-divider,PLL-IN 12M

	/* PLL config, in master mode 
	   Fout = (PLL-IN*(N+2))/((M+2)*(K+2)) {Typical K=2)
	 */

	sccb_wr_word(0x77 ,0x4000 );
	switch(sample_rate){
		case 44100:
		case 22050:
		case 11025:
			//N=128,M=14,K=2,Fout=24.56
			sccb_wr_word(0x81 ,0x3f02 );//Fout=24.56MHz
			sccb_wr_word(0x82 ,0xf000 );
			break;

		case 96000:
		case 48000:
		case 24000:
		case 32000:
		case 16000:
		case 12000:
		case 8000:
			//N=66,M=7,K=2,Fout=22.667
			sccb_wr_word(0x81, 0x4082 );//0 //MCLK=24MHz, PLL=24.576MHz
			sccb_wr_word(0x82, 0xE000 );//0 //MCLK=24MHz, PLL=24.576MHz
			break;

		default:
			debug_printf("unsupported sample rate\n");
			break;
	}

	//pll power on
	reg0x64 = sccb_rd_word(0x64);
	reg0x64 |=(1<<9);
	sccb_wr_word(0x64, reg0x64 );

#if 0
	debug_printf("reg 0x80 = 0x%x\n",sccb_rd_word(0x80));
	debug_printf("reg 0x81 = 0x%x\n",sccb_rd_word(0x81));
	debug_printf("reg 0x82 = 0x%x\n",sccb_rd_word(0x82));
	debug_printf("reg 0x64 = 0x%x\n",sccb_rd_word(0x64));
	debug_printf("reg 0x70 = 0x%x\n",sccb_rd_word(0x70));
#endif
}



static void alc5647_set_sr(s32 sr)
{
	debug_printf("alc5647 set sample rate %d\n",sr);
	alc5647_set_pll(sr);
	/* PLL config, in master mode */
	switch(sr){
		case 44100: sccb_wr_word(0x73 ,0x1114 ); break;//sr = 22579.2/256/2 KHz
		case 22050: sccb_wr_word(0x73 ,0x3114 ); break;//sr = 22579.2/256/4 KHz
		case 11025: sccb_wr_word(0x73 ,0x5114 ); break;//sr = 22579.2/256/8 KHz

		case 48000: sccb_wr_word(0x73, 0x1114 );break;//0 //sr = 24576/256/2 KHz
		case 24000: sccb_wr_word(0x73, 0x3114 );break;//0 //sr = 24576/256/4 KHz
		case 32000: sccb_wr_word(0x73, 0x2114 );break;//0 //sr = 24576/256/3 KHz
		case 16000: sccb_wr_word(0x73, 0x4114 );break;//0 //sr = 24576/256/6 KHz
		case 12000: sccb_wr_word(0x73, 0x5114 );break;//0 //sr = 24576/256/8 KHz
		case 8000:  sccb_wr_word(0x73, 0x6114 );break;//0 //sr = 24576/256/12 KHz

		default:
			debug_printf("unsupported sample rate\n");
			break;
	}

}

#ifdef CONFIG_AUDIO_ENC_EN  
/*
Stereo1 ADC Left/Right Channel Volume Control
00’h: -17.625dB
…
2F’h: 0dB
…
7F’h: +30dB, with 0.375dB/Step
*/
static s32 alc5647_set_adc_dvol(s32 vol){
	debug_printf("set adcvol = %ddB\n",vol);
	{
		u8 setval;
		s32 setvol;
		if(vol > 30)
		{
			setval = 0xff;
			setvol = 30;
			debug_printf("alc5647 not support digital volume bigger than 30 dB, default to 30dB\n");
		}
		else if(vol < -17)
		{
			setval = 0x7f;
			setvol = -17;
			debug_printf("alc5647 does not support digital volume less than -17dB,default to -17dB\n");
		}
		else
		{
			setvol = vol;
			setval = 0x2f + (vol*8/3);//0xc3:0dB, 0.5dB step
		}	
		//dODO
		sccb_wr_word(0x1C,(setval<<8)|setval);
		return setvol;
	}
}
#endif


#ifdef CONFIG_AUDIO_DEC_EN  
/*
DAC1 Left/Right Channel Digital Volume
00’h: -65.625dB
…
AF’h: 0dB, with 0.375dB/Step
*/
static s32 alc5647_set_dac_dvol(s32 vol){
	debug_printf("set dacvol = %ddB\n",vol);
	{
		u8 setval;
		u16 setval_16;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xaf;
			setvol = 0;
			debug_printf("alc5647 not support digital volume bigger than 0 dB, default to 0dB\n");
		}
		else if(vol < -65)
		{
			setval = 0x0;
			setvol = -65;
			debug_printf("alc5647 does not support digital volume less than -65dB,default to -65dB\n");
		}
		else
		{
			setval = 0xaf + vol*8/3 ;
			setvol = vol;
		}	


		sccb_wr_word(0x19, (setval<<8)|setval );//DACL1 DACR1 digital volume
		sccb_wr_word(0x1a, (setval<<8)|setval );//DACL2 DACR2 digital volume 

		return setvol;
	}
}

void alc5647_playback_init (t_codec_cpara *cp)
{
	debug_printf("alc5647_playback_init\n");
	
	/*
		@start@
	#rng00  #rv0000  #rd0
	#rng73  #rv0000  #rd0
	#rngFA  #rv2061  #rd0
	#rng63  #rvA812  #rd100
	#rng63  #rvE8FA  #rd0
	#rng61  #rv9B01  #rd0
	#rng62  #rv0800  #rd0
	#rng65  #rv3002  #rd0
	#rni3d  #rv2E00  #rd0
	#rng2A  #rv1616  #rd0
	#rng48  #rv3803  #rd0
	#rng01  #rv4848  #rd0
		@end@ 
 	*/
	sccb_wr_word(0x63, 0xa812 );//Power Management Control3
	ertos_timedelay(10);
	sccb_wr_word(0x63, 0xe8fa );//Power Management Control3

	/*
	I2S1 Digital Interface Power on
	Analog DACL1 DACR1 Power on
	Class-D Left & Right Channel Power on 
	Class-D Power on
	 */
	sccb_wr_word(0x61, 0x9b01 );

	sccb_wr_word(0x62, 0x0800 );//Power on DAC stereo1 filter
	sccb_wr_word(0x65, 0x3002 );//SPKMIXL & SPKMIXR Power on, LDO2 Power Control

	/*
		HPOVOLR to REC Right Mixer:-3dB
		INR1 to REC Right Mixer:-9dB
		MONOVOL to REC Mixer :-12dB
	 */
	//sccb_wr_word(0x3d, 0x2e00 );
	sccb_wr_word(0x6A, 0x003d );
	sccb_wr_word(0x6C, 0x2e00 );

	/*
		Mute Stereo DAC2 Left&Right channel
		Mute Stereo DAC1 Right channel to Left Mixer
		Mute Stereo DAC1 Left channel to Right Mixer
 	*/
	sccb_wr_word(0x2a, 0x1616 );

	/*
	   Mute SPKVOLL to SPOMIXL & SPOMIXR
	   Mute BST3 to SPOMIXL & SPOMIXR
	 */
	sccb_wr_word(0x48, 0x3803 );

	/*
	   Mute Control for Left Speaker Volume Channel (SPKVOLL)
	   Left Speaker Channel Volume Control:0dB
	   Mute Control for Right Speaker Volume Channel (SPKVOLR)
	   Right Speaker Channel Volume Control:0dB
	 */
	sccb_wr_word(0x01, 0x4848 );
}
#endif


#if defined(CONFIG_AUDIO_DEC_EN) && defined(CONFIG_AUDIO_ENC_EN)
void alc5647_duplex_init(t_codec_cpara* cp)
{
	debug_printf("alc5647_duplex_init\n");
	
	/*
		@start@
	#rng00  #rv0000  #rd0
	#rng73  #rv0000  #rd0
	#rngFA  #rv2061  #rd0
	#rng63  #rvA812  #rd100
	#rng63  #rvE8FA  #rd0
	#rng61  #rv9B01  #rd0
	#rng62  #rv0800  #rd0
	#rng65  #rv3002  #rd0
	#rni3d  #rv2E00  #rd0
	#rng2A  #rv1616  #rd0
	#rng48  #rv3803  #rd0
	#rng01  #rv4848  #rd0
		@end@ 
 	*/

/*----------------------------playback settings---------------------------------------*/
	sccb_wr_word(0x63, 0xa812 );//Power Management Control3
	ertos_timedelay(10);
	sccb_wr_word(0x63, 0xe8fa );//Power Management Control3

	/*
	I2S1 Digital Interface Power on
	Analog DACL1 DACR1 Power on
	Class-D Left & Right Channel Power on 
	Class-D Power on
	 */
//	sccb_wr_word(0x61, 0x9b01 );

//	sccb_wr_word(0x62, 0x0800 );//Power on DAC stereo1 filter
//	sccb_wr_word(0x65, 0x3002 );//SPKMIXL & SPKMIXR Power on, LDO2 Power Control

	/*
		HPOVOLR to REC Right Mixer:-3dB
		INR1 to REC Right Mixer:-9dB
		MONOVOL to REC Mixer :-12dB
	 */
//	//sccb_wr_word(0x3d, 0x2e00 );
//	sccb_wr_word(0x6A, 0x003d );
//	sccb_wr_word(0x6C, 0x2e00 );

	/*
		Mute Stereo DAC2 Left&Right channel
		Mute Stereo DAC1 Right channel to Left Mixer
		Mute Stereo DAC1 Left channel to Right Mixer
 	*/
	sccb_wr_word(0x2a, 0x1616 );

	/*
	   Mute SPKVOLL to SPOMIXL & SPOMIXR
	   Mute BST3 to SPOMIXL & SPOMIXR
	 */
	sccb_wr_word(0x48, 0x3803 );

	/*
	   Mute Control for Left Speaker Volume Channel (SPKVOLL)
	   Left Speaker Channel Volume Control:0dB
	   Mute Control for Right Speaker Volume Channel (SPKVOLR)
	   Right Speaker Channel Volume Control:0dB
	 */
	sccb_wr_word(0x01, 0x4848 );

/*----------------------------record settings---------------------------------------*/
	/*
	   @start@
	#rng00  #rv0000  #rd0
	#rng73  #rv0000  #rd0
	#rngFA  #rv2061  #rd0
	#rngFB  #rv4050  #rd0
	#rng0D  #rv0540  #rd0
	#rng3C  #rv007B  #rd0
	#rng3E  #rv007B  #rd0
	#rng63  #rvA812  #rd100
	#rng63  #rvE81A  #rd0
	#rng61  #rv8006  #rd0
	#rng62  #rv8000  #rd0
	#rng64  #rv4C20  #rd0
	#rng65  #rv0C02  #rd0
	#rni3d  #rv3800  #rd0
	#rng27  #rv3020  #rd0
		@end@ 
	 */
	sccb_wr_word(0xfb,0x4050);//#rngFB #rv4050 #rd0 default 0x4040, set bit4? but bit4 reserved
	sccb_wr_word(0x0d, 0x0140 );//IN2 boost 20dB,Differential Mode
	sccb_wr_word(0x3c,0x007b);//RECMIXL Control2:Mute all except MIC BST2 to RECMIXL Mute Control
	sccb_wr_word(0x3e,0x007b);//RECMIXR Control2:Mute all except MIC BST2 to RECMIXR Mute Control
	sccb_wr_word(0x63, 0xa812 );//Power Management Control3
	ertos_timedelay(10);
	sccb_wr_word(0x63, 0xe81a );//Power Management Control3
//	sccb_wr_word(0x61, 0x8006 );//Power:I2S1 on, ADCL/ADCR on
//	sccb_wr_word(0x62, 0x8000 );//Stereo1 ADC Digital Filter Power on
	sccb_wr_word(0x64, 0x4c20 );//MIC BST2 Power on,MICBIAS 1&2 Power on,pll power on 
//	sccb_wr_word(0x65, 0x0c02 );//RECMIX L&R Power on ,LDO2 Power on
//	//sccb_wr_word(0x3d, 0x3800 );//HPOVOLR to REC Right Mixer:-3dB,INR1 to REC Right Mixer:-18dB
//	sccb_wr_word(0x6A, 0x003d );
//	sccb_wr_word(0x6C, 0x3800 );


	/*
	unmute Source 1 to Stereo1 ADC Left and right channel,
	mute Source 2 to Stereo1 ADC Left and right channel
	select ADC1 as Stereo1 ADC L/R channel source 1
	 */
	sccb_wr_word(0x27, 0x3020 );


/*----------------------------mixed settings---------------------------------------*/
	sccb_wr_word(0x61, 0x9b07 );
	sccb_wr_word(0x62, 0x8800 );
	sccb_wr_word(0x65, 0x3c02 );
	//sccb_wr_word(0x3d, 0x3e00 );
	sccb_wr_word(0x6A, 0x003d );
	sccb_wr_word(0x6C, 0x3e00 );

}
#endif

#ifdef CONFIG_AUDIO_ENC_EN  
void alc5647_record_init(t_codec_cpara *cp)
{
	debug_printf("alc5647_record_init\n");
	/*
	   @start@
	#rng00  #rv0000  #rd0
	#rng73  #rv0000  #rd0
	#rngFA  #rv2061  #rd0
	#rngFB  #rv4050  #rd0
	#rng0D  #rv0540  #rd0
	#rng3C  #rv007B  #rd0
	#rng3E  #rv007B  #rd0
	#rng63  #rvA812  #rd100
	#rng63  #rvE81A  #rd0
	#rng61  #rv8006  #rd0
	#rng62  #rv8000  #rd0
	#rng64  #rv4C20  #rd0
	#rng65  #rv0C02  #rd0
	#rni3d  #rv3800  #rd0
	#rng27  #rv3020  #rd0
		@end@ 
	 */
#if 0
	sccb_wr_word(0x0, 0x000 );//SW reset
	sccb_wr_word(0x73, 0x000 );//16bits(32FS)
	sccb_wr_word(0xfa, 0x2061 );//Enable MCLK gating control:bit0=1
#endif
	sccb_wr_word(0xfb,0x4050);//#rngFB #rv4050 #rd0 default 0x4040, set bit4? but bit4 reserved
	sccb_wr_word(0x0d, 0x0140 );//IN2 boost 20dB,Differential Mode
	sccb_wr_word(0x3c,0x007b);//RECMIXL Control2:Mute all except MIC BST2 to RECMIXL Mute Control
	sccb_wr_word(0x3e,0x007b);//RECMIXR Control2:Mute all except MIC BST2 to RECMIXR Mute Control
	sccb_wr_word(0x63, 0xa812 );//Power Management Control3
	ertos_timedelay(10);
	sccb_wr_word(0x63, 0xe81a );//Power Management Control3
	sccb_wr_word(0x61, 0x8006 );//Power:I2S1 on, ADCL/ADCR on
	sccb_wr_word(0x62, 0x8000 );//Stereo1 ADC Digital Filter Power on
	sccb_wr_word(0x64, 0x4c20 );//MIC BST2 Power on,MICBIAS 1&2 Power on,pll power on 
	sccb_wr_word(0x65, 0x0c02 );//RECMIX L&R Power on ,LDO2 Power on
	//sccb_wr_word(0x3d, 0x3800 );//HPOVOLR to REC Right Mixer:-3dB,INR1 to REC Right Mixer:-18dB
	sccb_wr_word(0x6A, 0x003d );
	sccb_wr_word(0x6C, 0x3800 );


	/*
	unmute Source 1 to Stereo1 ADC Left and right channel,
	mute Source 2 to Stereo1 ADC Left and right channel
	select ADC1 as Stereo1 ADC L/R channel source 1
	 */
	sccb_wr_word(0x27, 0x3020 );
}
#endif


static __acodec_cfg t_libacodec_setting g_alc5647_setting = {
	.early_init = NULL,
	.init = codec_alc5647_init,
	.ioctl = codec_alc5647_ioctl
};

