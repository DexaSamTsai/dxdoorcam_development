#include "includes.h"


static t_libsccb_cfg h_ti3256_sccb;

static void ti3256_playback_init(u32 master);
static s32 ti3256_set_dvol(s32 vol);
static __acodec_cfg t_libacodec_setting g_ti3256_setting;

int codec_ti3256_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
		case AI_DIR_RECORD:
		case AI_DIR_PLAY:
		case AI_DIR_DUPLEX:
			ti3256_playback_init(master);
			break;
		default:
			debug_printf("codec_ti3256_init configuration error\n");
			return -1;
	}
	return 0;
}

int codec_ti3256_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN: 
			break;
		case AI_CTL_SR:
			{
				unsigned char NDAC,MDAC;
				s32 sample_rate;
				sample_rate = *(s32*)arg;
				switch(sample_rate)
				{
					case 8000:    MDAC = 4; NDAC = 22; break;
					case 11025:   MDAC = 8; NDAC = 8; break;
					case 12000:   MDAC = 2; NDAC = 29; break;
					case 16000:   MDAC = 2; NDAC = 22; break;
					case 22050:   MDAC = 4; NDAC = 8; break;
					case 24000:   MDAC = 1; NDAC = 29; break;
					case 32000:   MDAC = 2; NDAC = 11; break;
					case 44100:   MDAC = 2; NDAC = 8; break;	
					case 48000:   MDAC = 2; NDAC = 7; break;	
					default:   	MDAC = 2; NDAC = 8; break;				
				}
				NDAC += 0x80;
				MDAC += 0x80;
				//sccb_wr(&h_ti3256_sccb, 0x0b, NDAC);
				//sccb_wr(&h_ti3256_sccb, 0x0c, MDAC);
			}
			break;
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32 *)arg;
				return ti3256_set_dvol(vol);
			}
			
		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:
			//sccb_wr(&h_ti3256_sccb, 0x40, 0x0c); //left DAC and right DAC muted
			//sccb_wr(&h_ti3256_sccb, 0x01, 0x01);//w 30 01 01
			break;
		case AI_CTL_EQ:
			break;
		default:
			break;
	}
	return 0;
}

//DAC digital volume 
/*
0111 1111�C0011 0001: Reserved. Do not use
0011 0000: Digital volume control = 24 dB
0010 1111: Digital volume control = 23.5 dB
0010 1110: Digital volume control = 23 dB
...
0000 0001: Digital volume control = 0.5 dB
0000 0000: Digital volume control = 0 dB
1111 1111: Digital volume control = �C0.5 dB
...
1000 0010: Digital volume control = �C63 dB
1000 0001: Digital volume control = �C63.5 dB
1000 0000: Reserved
*/
static s32 ti3256_set_dvol(s32 vol){
	//if(g_ti3256_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 24)
		{
			setval = 0x30;
			setvol = 24;
			debug_printf("ti3256 not support digital volume bigger than 24 dB(%ddB), default to 24dB\n",vol);
		}
		else if(vol < -63)
		{
			setval = 0x82;
			setvol = -63;
			debug_printf("ti3256 does not support digital volume less than -63dB(%ddB),default to -63dB\n",vol);
		}
		else
		{
			setvol = vol;
			setval = vol*2;
		}	

		//sccb_wr(&h_ti3256_sccb, 0x41, setval);
		//sccb_wr(&h_ti3256_sccb, 0x42, setval);
		//g_ti3256_setting.dvol.cur = setvol;
		//g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}	
}


u8 aic_init_reg_page0[][2] =
{
	{0x00, 0x00},
	{0x04, 0x03},
	{0x05, 0xa1},
	{0x06, 0x06},
	{0x07, 0x1b},
	{0x08, 0x78},
	{0x0b, 0xb9},
	{0x0c, 0x81},
	{0x12, 0xb9},
	{0x13, 0x81},
	{0x34, 0x0d},
	{0x3d, 0x01},
	{0x51, 0xC0},
	{0x52, 0x00},
	{0x1b, 0x00},
	{0x3c, 0x08},
	{0x3f, 0xd6},
	{0x40, 0x00},
};

u8 aic_init_reg_page1[][2] =
{
	{0x00, 0x01},
	{0x01, 0x08},
	{0x02, 0x00},
	{0x3d, 0x00},
	{0x47, 0x32},
	{0x7b, 0x01},
	{0x33, 0x40},
	{0x34, 0x04},
	{0x36, 0x40},
	{0x37, 0x04},
	{0x39, 0x40},
	{0x3b, 0x00},
	{0x3c, 0x00},
	{0x09, 0x3c},
	{0x12, 0x00},
	{0x13, 0x00},
	{0x7c, 0x06},
	{0x01, 0x0a},
	{0x02, 0x00},
	{0x7b, 0x05},
	{0x0c, 0x08},
	{0x0d, 0x08},
	{0x0e, 0x08},
	{0x0f, 0x08},
	{0x7d, 0x12},
	{0x10, 0x00},
	{0x11, 0x00},
};

static void ti3256_playback_init(u32 master)
{
	u8 id = 1, reg_cnt, i;
	h_ti3256_sccb.mode = SCCB_MODE8;
	h_ti3256_sccb.id = 0x30;
	h_ti3256_sccb.clk = 400*1024;
	h_ti3256_sccb.wr_check = 0;
	h_ti3256_sccb.timeout = 0;
	h_ti3256_sccb.retry = 0;
	//libsccb_init_old(0x34, 0);
	libsccb_init(&h_ti3256_sccb);
	debug_printf("dac_ti3256_init\n");
	sccb_wr(&h_ti3256_sccb, 0x00, 0x01);
	sccb_wr(&h_ti3256_sccb, 0x09, 0x00);
	
	sccb_wr(&h_ti3256_sccb, 0x00, 0x00);
	sccb_wr(&h_ti3256_sccb, 0x01, 0x01);
	{volatile u32 delay; for(delay=0; delay<0x40000;delay++);};
	//# Delay 1 ms
		
	g_ti3256_setting.dvol.max = 24;	//dB
	g_ti3256_setting.dvol.min = -63; //dB

	sccb_wr(&h_ti3256_sccb, 0x00, 0x00);
	for(i = 0; i < sizeof(aic_init_reg_page0)/sizeof(aic_init_reg_page0[0]); i++)
	{
		sccb_wr(&h_ti3256_sccb, aic_init_reg_page0[i][0], aic_init_reg_page0[i][1]);
	}

	for(i = 0; i < sizeof(aic_init_reg_page1)/sizeof(aic_init_reg_page1[0]); i++)
	{
		sccb_wr(&h_ti3256_sccb, aic_init_reg_page1[i][0], aic_init_reg_page1[i][1]);
	}
	sccb_wr(&h_ti3256_sccb, 0x0, 0x0);
	sccb_wr(&h_ti3256_sccb, 0x04, 0x03);
	sccb_wr(&h_ti3256_sccb, 0x05, 0xa1);
	sccb_wr(&h_ti3256_sccb, 0x06, 0x06);
	sccb_wr(&h_ti3256_sccb, 0x07, 0x1b);
	sccb_wr(&h_ti3256_sccb, 0x08, 0x78);
	sccb_wr(&h_ti3256_sccb, 0x0b, 0xb9);
	sccb_wr(&h_ti3256_sccb, 0x0c, 0x81);
	sccb_wr(&h_ti3256_sccb, 0x12, 0xb9);
	sccb_wr(&h_ti3256_sccb, 0x13, 0x81);
	sccb_wr(&h_ti3256_sccb, 0x0, 0x1);
	sccb_wr(&h_ti3256_sccb, 0x3d, 0x00);
	sccb_wr(&h_ti3256_sccb, 0x47, 0x32);
	sccb_wr(&h_ti3256_sccb, 0x02, 0x00);
	sccb_wr(&h_ti3256_sccb, 0x7b, 0x01);
}


static __acodec_cfg t_libacodec_setting g_ti3256_setting = {
	.early_init = NULL,
	.init = codec_ti3256_init,
	.ioctl = codec_ti3256_ioctl
	};


	

