
#include "includes.h"

/* Stream Header Data */

 #define HDMI_SH_PCM (0x1 << 4)

 #define HDMI_SH_TWO_CHANNELS (0x1 << 0)

 #define HDMI_SH_44KHz (0x2 << 2)

 #define HDMI_SH_48KHz (0x3 << 2)

 #define HDMI_SH_16BIT (0x1 << 0)


// #define SI9022_REG_TPI_RQB 0xC7
// #define HDMI_SYS_CTRL_DATA_REG 0x1A


 #define TPI_SYS_CTRL_OUTPUT_MODE_DVI	(0 << 0)
 #define TPI_SYS_CTRL_OUTPUT_MODE_HDMI	(1 << 0)

 #define TPI_SYS_CTRL_POWER_DOWN		(1 << 4)
 #define TPI_SYS_CTRL_POWER_ACTIVE	(0 << 4)
static void SiI9022_trans_init(void);

static t_libsccb_cfg h_hdmi_sccb;
static __acodec_cfg t_libacodec_setting g_hdmi_SiI9022_setting;

int hdmi_SiI9022_init(E_AI_DIR dir)
{
	switch(dir)
	{
		case AI_DIR_PLAY:
			SiI9022_trans_init();
			break;
		default:
			debug_printf("hdmi_SiI9022_init configuration error\n");
			return -1;
	}
	return 0;
}

int hdmi_SiI9022_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN:
			break;
		case AI_CTL_SR:
			break;
		case AI_CTL_DAC_DVOL:
			break;
		case AI_CTL_MUTE:
			break;
		case AI_CTL_PWRDWN:
			break;
			break;
		default:
			break;
	}
	return 0;
}

 static unsigned char audio_info[15] = {

 	0xC2,	//

 	0x84,	//frame type

 	0x01,	//version

 	0x0A,	//length

 	0x71,	//cksum

 	HDMI_SH_PCM | HDMI_SH_TWO_CHANNELS,

 	HDMI_SH_48KHz | HDMI_SH_16BIT,

 	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

int sii9022_hdmi_audio_ctl(int mute)

 {
 	u8	dat;
 	if( mute ) {
 		//debug_printf(" AI, mute = 0x90, spdif, mute = 0x50, \n");
// 		if( enable_hdmi_spdif )
 //		  i2c_smbus_write_byte_data(sii9022_client, 0x26 ,0x50);
 //		else
 		sccb_wr(&h_hdmi_sccb, 0x26, 0x90);//AI mute 

 		debug_printf("hdmi audio mute\n");
 	}
 	else 
	{ /* unmute */
		int i;
 		// AI, unmute, PCM=0001 = 0x80,  SPDIF = 0x40
 		//i2c_smbus_write_byte_data(sii9022_client, 0x26, 0x80);
 //		if( enable_hdmi_spdif )
 //		  i2c_smbus_write_byte_data(sii9022_client, 0x26, 0x40);
 //		else
 		 sccb_wr(&h_hdmi_sccb, 0x26, 0x80);// i2c_smbus_write_byte_data(sii9022_client, 0x26, 0x80);

		//	i2c_smbus_write_i2c_block_data(sii9022_client, 0xBF, 15, audio_info );
		for(i=0;i<15;i++)		
			sccb_wr(&h_hdmi_sccb, 0xBF+i, audio_info[i]);
 		//print(" back door reG0x2F=2CH \n");
 		sccb_wr(&h_hdmi_sccb, 0xBC, 0x02);// i2c_write_byte_data(sii9022_client, 0xBC, 0x02);
 		sccb_wr(&h_hdmi_sccb, 0xBD, 0x2F);//i2cs_write_byte_data(sii9022_client, 0xBD, 0x2F);
 		dat = sccb_rd(&h_hdmi_sccb, 0xBE);//dat = i2c_read_byte_data(sii9022_client, 0xBE);
 		dat &= 0xFD; /* layout 0 : 2 channel */
		sccb_wr(&h_hdmi_sccb, 0xBE, dat);//i2c__write_byte_data(sii9022_client, 0xBE, dat);

 		debug_printf("hdmi audio unmute\n");
 	}
	
 	return 0;
 }

 

 int sii9022_hdmi_audio_init(void)
 {
 	u8	cksum = 0;
 	int i;

 	sii9022_hdmi_audio_ctl(1);
	sccb_wr(&h_hdmi_sccb, 0x25,0);//i2c_write_byte_data(sii9022_client, 0x25, 0x00);

 	// 24bit, 48kHz, 2 channel  = 0xD9
 	// 16bit, 48kHz, 2 channel  = 0x59
 	// 16bit, 44.1kHz, 2 channel =0x51
 	// refer to stream          = 0x00

 	// SPDIF only?
	 sccb_wr(&h_hdmi_sccb, 0x27,0x59);//i2c_write_byte_data(sii9022_client, 0x27, 0x59);
	 sccb_wr(&h_hdmi_sccb, 0x28,0);//i2c_write_byte_data(sii9022_client, 0x28, 0x00);
	 sccb_wr(&h_hdmi_sccb, 0x1f,0x80);//i2c_write_byte_data(sii9022_client, 0x1F, 0x80);

 	//print(" rising,256Fs,ws-low=left,left justify,msb 1st,1 bit shift \n");
	 sccb_wr(&h_hdmi_sccb, 0x20,0x90);//i2c_write_byte_data(sii9022_client, 0x20, 0x90);

 	// PCM format
	 sccb_wr(&h_hdmi_sccb, 0x21,0);//i2c_write_byte_data(sii9022_client, 0x21, 0x00);
 	// 48K = 0x02, 44.1K = 0x00
	 sccb_wr(&h_hdmi_sccb, 0x24,2);//i2c_write_byte_data(sii9022_client, 0x24, 0x02); //0x22
 
 	// 16bit=0x02 , 24b = 1011 (default), pass basic audio only = 0x00
	 sccb_wr(&h_hdmi_sccb, 0x25,2);//i2c_write_byte_data(sii9022_client, 0x25, 0x02); //0xd2

 	//audio_info[6] = hdmi_sh_value;
 	for( i=1; i < 15; i++ )
 		cksum += audio_info[i];
 	audio_info[4] = 0x100 - cksum;

 	//print(" back door reG0x24=16 BIT \n");
	 sccb_wr(&h_hdmi_sccb, 0xBC,2);//i2c_write_byte_data(sii9022_client, 0xBC, 0x02);
	 sccb_wr(&h_hdmi_sccb, 0xBD,0x24);//i2c_write_byte_data(sii9022_client, 0xBD, 0x24);
	 sccb_wr(&h_hdmi_sccb, 0xBE,2);//i2c_write_byte_data(sii9022_client, 0xBE, 0x02);

 	sii9022_hdmi_audio_ctl(0);

 	return 0;
 }

void SiI9022_video_init()
{
	debug_printf("before write 0x82 = 0x%x\n",sccb_rd(&h_hdmi_sccb,0x82));
	sccb_wr(&h_hdmi_sccb,0x82,0x25);
	debug_printf("0x82 = 0x%x\n",sccb_rd(&h_hdmi_sccb,0x82));
	sccb_wr(&h_hdmi_sccb,0x08,0x36);
	sccb_wr(&h_hdmi_sccb,0x7C,0x14);
	sccb_wr(&h_hdmi_sccb,0xC7,0x00);// reset 
	sccb_wr(&h_hdmi_sccb,0x08,0x30);//Pll set
	sccb_wr(&h_hdmi_sccb,0x00,0x8c);
	sccb_wr(&h_hdmi_sccb,0x01,0x0A);
	sccb_wr(&h_hdmi_sccb,0x02,0x70);
	sccb_wr(&h_hdmi_sccb,0x03,0x17);
	sccb_wr(&h_hdmi_sccb,0x04,0x5A);
	sccb_wr(&h_hdmi_sccb,0x05,0x03);
	sccb_wr(&h_hdmi_sccb,0x06,0x0D);
	sccb_wr(&h_hdmi_sccb,0x07,0x02);

	sccb_wr(&h_hdmi_sccb,0x08,0x30);//color space
	sccb_wr(&h_hdmi_sccb,0x09,0x00);
	sccb_wr(&h_hdmi_sccb,0x0A,0x00);
	sccb_wr(&h_hdmi_sccb,0x60,0x04);
	sccb_wr(&h_hdmi_sccb,0xBC,0x01); 
	sccb_wr(&h_hdmi_sccb,0xBD,0x82);
	sccb_wr(&h_hdmi_sccb,0xBE,0x35);
	sccb_wr(&h_hdmi_sccb,0x01,0x0A);
	sccb_wr(&h_hdmi_sccb,0x00,0x8c);

	sccb_wr(&h_hdmi_sccb,0x1E,0x00);//Power up
	sccb_wr(&h_hdmi_sccb,0x0C,0x14);  //Avi
	sccb_wr(&h_hdmi_sccb,0x1A,0x00);//Enable TMDS
}


static void SiI9022_trans_init(void)
{
 	u8	dat;
	u32 try = 100;
	memset(&h_hdmi_sccb, 0, sizeof(t_libsccb_cfg));
	h_hdmi_sccb.mode = SCCB_MODE8;
	h_hdmi_sccb.id = 0x72;
//	h_sccb.rd_mode = RD_MODE_RESTART;
#ifndef CHIP_R1
	h_hdmi_sccb.rd_mode = RD_MODE_NORMAL;
#endif	
	h_hdmi_sccb.clk = 400*1024;
//	h_sccb.sysclk = 24*1024*1024;
	h_hdmi_sccb.wr_check = 0;
	h_hdmi_sccb.timeout = 0;
	h_hdmi_sccb.retry = 1;
	debug_printf("SiI9022 trank_init\n");
	
	libsccb_init(&h_hdmi_sccb);
 	// Set 9022 in hardware TPI mode on and jump out of D3 state
	sccb_wr(&h_hdmi_sccb,0xC7,0x00);
 //	i2c_smbus_write_byte_data(sii9022_client, SI9022_REG_TPI_RQB, 0x00);		// enable access to 9022 other regs

// read device ID
	try = 100;
 	do{
		if(!try){
 			debug_printf("SII9022 not found !\n");
			return;	
		}
		dat = sccb_rd(&h_hdmi_sccb,0x1B);
 		debug_printf("id=%d\n",dat);
		try--;
 	}while( dat != 0xB0 );
 	//}while(1);
	
	//DETECT SINK
	/* 0x3D[2], 0: not plug, 1=plugged */
		dat = sccb_rd(&h_hdmi_sccb,0x3D);
 		//dat = i2c_smbus_read_byte_data(sii9022_client, 0x3D);
 		if ( dat & 0x07 )
 			debug_printf("SII9022 sink found ! \n");
		else
 			debug_printf("SII9022 sink not found !\n");

			
#if 0
 //	if( cmd & HDMI_CTL_EDID_GET ) 
	{
 		int err = 0;

 		err = sii9022_handle_edid( sii9022_client, edid, sizeof(edid));
 		if ( err == 0 ) {
 			struct fb_monspecs *monspecs = (struct fb_monspecs *)opt;

 			if ( monspecs == NULL )
 				return 0;

 			edid_parse( edid, &sink_monitor, &sink_dvi );
 			fb_edid_to_monspecs(edid, monspecs);

 			mxcfb_dump_modelines(monspecs->modedb, monspecs->modedb_len);
 			return 0;
 		}
 		return -3;
 	}
#endif


 	// Power up, wakeup to D0, otherwise sink will show 'no signal'
	sccb_wr(&h_hdmi_sccb,0x1E,0x00);
 	//i2c_smbus_write_byte_data(sii9022_client, 0x1E, 0x00);
	
 	sii9022_hdmi_audio_init();
//	SiI9022_video_init();

	//TURN on TMDS
// 	dat = (sink_dvi ? TPI_SYS_CTRL_OUTPUT_MODE_DVI : TPI_SYS_CTRL_OUTPUT_MODE_HDMI);
	dat = TPI_SYS_CTRL_OUTPUT_MODE_HDMI;
 	dat |= TPI_SYS_CTRL_POWER_ACTIVE;
	sccb_wr(&h_hdmi_sccb,0x1A,dat);
 	//i2c_smbus_write_byte_data(sii9022_client, HDMI_SYS_CTRL_DATA_REG, dat );

//	debug_printf("dat written to 0x1A = 0x%x\n",sccb_rd(&h_hdmi_sccb,0x1A));
 	// Power up, wakeup to D0 again to take effect of 0x1A[0] sink type!
	sccb_wr(&h_hdmi_sccb,0x1E,0x00);
// 	i2c_smbus_write_byte_data(sii9022_client, 0x1E, 0x00);

//	sccb_wr(&h_hdmi_sccb,0x1A,0x00);//Enable TMDS

}


static __acodec_cfg t_libacodec_setting g_hdmi_SiI9022_setting = {
	.early_init = NULL,
	.init = hdmi_SiI9022_init,
	.ioctl = hdmi_SiI9022_ioctl
	};


