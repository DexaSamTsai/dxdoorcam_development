#include "includes.h"

static void incodec_playback_init(t_codec_cpara *cp);
static void incodec_duplex_init(t_codec_cpara *cp);
static void incodec_record_init(t_codec_cpara *cp);
static int early_inited = 0;
static u8 reg12,reg13,reg14;


static void codecreg_init(void)
{
	//must reset, otherwise changing samplerate will lead to noise
	SC_RESET2_SET(AUDIOCODEC);
	SC_RESET2_RELEASE(AUDIOCODEC);
	SC_RRESET0_SET(AUDIOCODEC);
	SC_RRESET0_RELEASE(AUDIOCODEC);

	WriteReg8(AUDIO_BASE_ADDR + 0x0,0x4);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x5,0xf);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x6,0xff);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x7,0x4f);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x8,0xff);	
	WriteReg8(AUDIO_BASE_ADDR + 0x9,0x2);	
	WriteReg8(AUDIO_BASE_ADDR + 0xa,0x81);	
	WriteReg8(AUDIO_BASE_ADDR + 0xb,0x03);	
	WriteReg8(AUDIO_BASE_ADDR + 0xc,0x17);	

	WriteReg8(AUDIO_BASE_ADDR + 0x10,0x00);	
	WriteReg8(AUDIO_BASE_ADDR + 0x12,0xa);	//DAC PA gain,0dB
	WriteReg8(AUDIO_BASE_ADDR + 0x11,0x0);	
	WriteReg8(AUDIO_BASE_ADDR + 0x13,0x3f);	//ADC gain,29.25dB 
	WriteReg8(AUDIO_BASE_ADDR + 0x14,0x0);	
	WriteReg8(AUDIO_BASE_ADDR + 0x15,0x0);	

#if 0
	{
		int i;
		for(i=0;i<0x24;i++)
			syslog(LOG_ERR, "ReadReg8(AUDIO_BASE_ADDR+0x%x) = 0x%x\n",i,ReadReg8(AUDIO_BASE_ADDR  + i));
	}
#endif
}

static int codec_incodec_early_init(void)
{
	SC_RESET2_SET(AUDIOCODEC);
	SC_RESET2_RELEASE(AUDIOCODEC);
	SC_RRESET0_SET(AUDIOCODEC);
	SC_RRESET0_RELEASE(AUDIOCODEC);

	//init ov798 codec in advance to raise VMID voltage, which can remove dc offset at start of recording
	WriteReg8(AUDIO_BASE_ADDR+0x10 , 0x00  );//        bit[7:6]=00 global power down 
	WriteReg8(AUDIO_BASE_ADDR+0x16 , 0x0c  );//        bit[3:2]=11, VMID ramp up current 400uA
	WriteReg8(AUDIO_BASE_ADDR+0x13 , 0x3f  );//         bit[5:0]=111111  max PGA gain,reduce RC charging const
	WriteReg8(AUDIO_BASE_ADDR+0x14 , 0x08  );//        bit[3]=1 PGA mute, reduce RC charging const

	//remove echo noise in speaker coming from Mic.
	WriteReg8(AUDIO_BASE_ADDR+0x11,0x0);	

	WriteReg8(AUDIO_BASE_ADDR + 0x0,0x4);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x5,0xf);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x6,0xff);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x7,0x4f);	
//	WriteReg8(AUDIO_BASE_ADDR + 0x8,0xff);	
	WriteReg8(AUDIO_BASE_ADDR + 0x9,0x2);	
	WriteReg8(AUDIO_BASE_ADDR + 0xa,0x81);	
	WriteReg8(AUDIO_BASE_ADDR + 0xb,0x03);	
	WriteReg8(AUDIO_BASE_ADDR + 0xc,0x17);	

	WriteReg8(AUDIO_BASE_ADDR + 0x10,0x00);	
	WriteReg8(AUDIO_BASE_ADDR + 0x12,0xa);	//DAC PA gain,0dB
	WriteReg8(AUDIO_BASE_ADDR + 0x15,0x0);	

	early_inited  = 1;
	return 0;
}

int codec_incodec_init(t_codec_cpara *cp)
{
	E_AI_DIR dir = cp->dir;	

	switch(dir)
	{
		case AI_DIR_RECORD:
			incodec_record_init(cp);
			break;
		case AI_DIR_PLAY:
			incodec_playback_init(cp);
			break;
		case AI_DIR_DUPLEX:
			incodec_duplex_init(cp);
			break;
		default:
			syslog(LOG_ERR, "codec_incodec_init configuration error\n");
			return -1;
	}
	if(early_inited){
		WriteReg8(AUDIO_BASE_ADDR + 0x13,0x0);	
		WriteReg8(AUDIO_BASE_ADDR + 0x14,0x0);	
		early_inited = 0;
	}else{
		codecreg_init();
	}

	return 0;
}


/*
PGA gain control,
code from <000000>~<111111>
-17.28~29.25dB,0.75dB step.
*/
static s32 interncodec_set_adc_dvol(s32 vol){
	syslog(LOG_INFO, "set adcvol = %ddB\n",vol);
	{
		u8 setval;
		s32 setvol;
		if(vol > 29)
		{
			setval = 0x3f;
			setvol = 29;
			syslog(LOG_ERR, "internal codec not support PGA gain bigger than 29 dB, default to 29dB\n");
		}
		else if(vol <= -17)
		{
			setval = 0x00;
			setvol = -17;
			syslog(LOG_ERR, "set adc dvol less than -17db, mute adc\n");
			//	Mute ADC: R13=0x00, R14[4:3]=11
			WriteReg8(AUDIO_BASE_ADDR+0x13,setval);	
			WriteReg8(AUDIO_BASE_ADDR+0x14,ReadReg8(AUDIO_BASE_ADDR+0x14)|0x18);	
			reg13 = ReadReg8(AUDIO_BASE_ADDR+0x13);
			reg14 = ReadReg8(AUDIO_BASE_ADDR+0x14);
			return setvol;
		}
		else
		{
			setvol = vol;
			setval = 23 + (vol*4/3);//0xc3:0dB, 0.5dB step
		}	

		/*
		R13 bit[7:6]	PGA GAIN BOOST,
			00	-0.28dB	
			01	12.68dB	
			10	19.72dB	
			11	28.85dB
		 */
		setval |= BIT7; //19.72dB
		WriteReg8(AUDIO_BASE_ADDR + 0x13,setval);
		WriteReg8(AUDIO_BASE_ADDR+0x14,ReadReg8(AUDIO_BASE_ADDR+0x14)&~0x18);	

		reg13 = ReadReg8(AUDIO_BASE_ADDR+0x13);
		reg14 = ReadReg8(AUDIO_BASE_ADDR+0x14);

		return setvol;
	}
}

/*
Left Channel Volume Control
4dB ~ -62dB
111111     -62dB
001010    0dB
000000    4dB
 */
//-62~4dB
const static u8 dac_pagain[]=
{
63 ,62 ,62 ,62 ,62 ,62 ,62 ,61 ,61 ,61 ,61 ,60 ,60 ,60 ,60 ,60 ,/*0-15*/
60 ,59 ,59 ,59 ,59 ,58 ,58 ,58 ,58 ,57 ,57 ,57 ,57 ,56 ,56 ,56 ,/*16-31*/
55 ,55 ,55 ,54 ,54 ,54 ,53 ,53 ,52 ,52 ,51 ,49 ,48 ,47 ,46 ,44 ,/*32-47*/
43 ,42 ,40 ,38 ,36 ,33 ,31 ,28 ,25 ,23 ,20 ,18 ,15 ,13 ,10 ,8  ,/*48-63*/
5  ,3  ,0  
};

static s32 interncodec_set_dac_dvol(s32 vol){
	s32 setvol;
	syslog(LOG_INFO, "set dacvol = %ddB\n",vol);
	if(vol > 4)
	{
		setvol = 4;
		syslog(LOG_ERR, "internal codec not support DAC volume bigger than 29 dB, default to 29dB\n");
	}
	else if(vol <= -62)
	{
		//	Mute DAC: R12  bit[7]=1, bit[5:0]=111111
		setvol = -62;
		syslog(LOG_ERR,"set dac dvol less than -62db, mute dac\n");
		WriteReg8(AUDIO_BASE_ADDR+0x12,ReadReg8(AUDIO_BASE_ADDR+0x12)|0xBF);	
		reg12 = ReadReg8(AUDIO_BASE_ADDR+0x12);

		return setvol;
	}else{
		setvol = vol;
	}

	WriteReg8(AUDIO_BASE_ADDR + 0x12,dac_pagain[setvol+62]);
	reg12 = ReadReg8(AUDIO_BASE_ADDR+0x12);

//	syslog(LOG_ALERT, "OV798 CODEC dac_pagain[setvol+62] 0x%x\n",dac_pagain[setvol+62]);
	return setvol;
}

static s32 interncodec_set_sr(s32 sr){
	u32 sr_conf =0 ;
	syslog(LOG_INFO, "OV798 CODEC sample rate is %d\n",sr);
	switch(sr)
	{
		case 8000:  sr_conf = 0x06;break;/*8 kHz (MCLK/1500)  00110 *    */ 
		case 11025: sr_conf = 0x19;break;/*11.0259kHz (MCLK/1088) 11001  */ 
		case 12000: sr_conf = 0x08;break;/*12 kHz (MCLK/1000) 01000      */ 
		case 16000: sr_conf = 0x0a;break;/*16kHz (MCLK/750) 01010        */ 
		case 22050: sr_conf = 0x1b;break;/*22.0588kHz (MCLK/544) 11011   */ 
		case 24000: sr_conf = 0x1c;break;/*24kHz (MCLK/500) 11100        */ 
		case 32000: sr_conf = 0x0c;break;/*32 kHz (MCLK/375) 01100 *     */ 
		case 44100: sr_conf = 0x11;break;/*44.118 kHz (MCLK/272) 10001 * */ 
		case 48000: sr_conf = 0x00;break;/*48 kHz (MCLK/250) 00000 *     */ 
		case 88200: sr_conf = 0x1f;break;/*88.235kHz (MCLK/136) 11111 *  */ 
		case 96000: sr_conf = 0x0e;break;/*96 kHz (MCLK/125) 01110 *     */ 
		default   : 
			syslog(LOG_ERR, "ov798 internal codec invalid samplerate %d, default t0 44100\n",sr);
			sr_conf = 0x11;
			break;
	}

	WriteReg8(AUDIO_BASE_ADDR + 0xa,(ReadReg8(AUDIO_BASE_ADDR+0xa)&~0x3e)|(sr_conf<<1));	

	return 0;
}
int codec_incodec_ioctl(E_AI_IOCTL ioctl, void *arg)
{

	switch(ioctl)
	{
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return interncodec_set_dac_dvol(vol);
			}

		case AI_CTL_ADC_DVOL:
			{
				s32 vol = *(s32*)arg;
				return interncodec_set_adc_dvol(vol);
			}

		case AI_CTL_SR:
			{
				s32 samplerate = *(s32*)arg;
				return interncodec_set_sr(samplerate);
			}
		case AI_CTL_PWRDWN:
			WriteReg8(AUDIO_BASE_ADDR+0x10, 0xf0);	
			break;

		case AI_CTL_MUTE_DAC:
			{
				s32 mute = *(s32*)arg;
				if(mute){
					syslog(LOG_INFO, "Mute DAC\n");
					reg12 = ReadReg8(AUDIO_BASE_ADDR+0x12);
					WriteReg8(AUDIO_BASE_ADDR+0x12,ReadReg8(AUDIO_BASE_ADDR+0x12)|0xBF);	
				}else{
					syslog(LOG_INFO, "Un-mute DAC\n");
					WriteReg8(AUDIO_BASE_ADDR+0x12,reg12);	
				}
			}
			break;

		case AI_CTL_MUTE_ADC:
			{
				s32 mute = *(s32*)arg;
				if(mute){
					//	Mute ADC: R13=0x00, R14[4:3]=11
					syslog(LOG_INFO, "Mute ADC\n");
					reg13 = ReadReg8(AUDIO_BASE_ADDR+0x13);
					reg14 = ReadReg8(AUDIO_BASE_ADDR+0x14);
					WriteReg8(AUDIO_BASE_ADDR+0x13,0);	
					WriteReg8(AUDIO_BASE_ADDR+0x14,ReadReg8(AUDIO_BASE_ADDR+0x14)|0x18);	
				}else{
					syslog(LOG_INFO, "Un-mute ADC\n");
					WriteReg8(AUDIO_BASE_ADDR+0x13,reg13);	
					WriteReg8(AUDIO_BASE_ADDR+0x14,reg14);	
				}
			}
			break;

		default:
	//		syslog(LOG_ERR, "unsupported ioctl cmd %d\n",ioctl);
			break;
	}
	return 0;
}





void incodec_playback_init(t_codec_cpara *cp)
{
//	syslog(LOG_INFO, "ov798 internal CODEC playback init\n");

	//Audio Codec I2S I/F share settings
	/*
	  [15] I2S1 source 0:Pad 1:codec
	  [24] codec pad in  1:pad 0:I2S 
	*/
	WriteReg32(SC_BASE_ADDR + 0x10, ReadReg32(SC_BASE_ADDR + 0x10)&~BIT24); 

	/*
	[3]  Audio Code Record & playback sel 1:record 0:playback
	[2]  I2S0 source  0:PAD 1:Codec
	[1]  I2S sel  0:I2S0 1:I2S1
	*/
#ifdef AI1REC_AI2PLAY
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)|BIT1); //choose I2S1 for playback
#else
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)&~BIT1);//choose I2S0 for playback
#endif

	//[1]  Codec & I2S use pad 1:codec use pad 0:I2s Or GPIO use pad
	WriteReg32(SC_BASE_ADDR + 0x0, ReadReg32(SC_BASE_ADDR + 0x0) & ~BIT1);
}

void incodec_duplex_init(t_codec_cpara *cp)
{

	syslog(LOG_INFO, "ov798 internal CODEC duplex init\n");

	//Audio Codec I2S I/F share settings
	/*
	  [15] I2S1 source 0:Pad 1:codec
	  [24] codec pad in  1:pad 0:I2S 
	*/
#ifdef AI1REC_AI2PLAY
	WriteReg32(SC_BASE_ADDR + 0x10, ReadReg32(SC_BASE_ADDR + 0x10)&~BIT24); 
#else
	WriteReg32(SC_BASE_ADDR + 0x10, ReadReg32(SC_BASE_ADDR + 0x10)&~BIT24|BIT15); //I2S1 for record, I2S1 source is codec
#endif

	/*
	[3]  Audio Code Record & playback sel 1:record 0:playback
	[2]  I2S0 source  0:PAD 1:Codec
	[1]  I2S sel  0:I2S0 1:I2S1
	*/
#ifdef AI1REC_AI2PLAY
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)|BIT2|BIT1); //choose I2S1 for playback
#else
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)&~BIT1);//choose I2S0 for playback
#endif

	//[1]  Codec & I2S use pad 1:codec use pad 0:I2s Or GPIO use pad
	WriteReg32(SC_BASE_ADDR + 0x0, ReadReg32(SC_BASE_ADDR + 0x0) & ~BIT1);
//	WriteReg32(SC_BASE_ADDR + 0x0, ReadReg32(SC_BASE_ADDR + 0x0) | BIT1);

}


void incodec_record_init(t_codec_cpara *cp)
{
	syslog(LOG_INFO, "ov798 internal CODEC recording init\n");

#if 0
	//select clock source to pllsys
	/*
	  [18:17] ls clock source sel 
	    00: pad clock 01:pll clock
	    10: pll2 clock 11:usb240M
	*/
	syslog(LOG_INFO, "default ReadReg32(SC_BASE_ADDR + 0x40) = 0x%x\n",ReadReg32(SC_BASE_ADDR + 0x40));
	WriteReg32(SC_BASE_ADDR + 0x40, ReadReg32(SC_BASE_ADDR + 0x40)& ~BIT18 | BIT17);

	//set I2S sys clock to 384/125 = 3.072MHz, then Freq(bclk)=1.536M, Freq(lck)=Freq(bclk)/32 = 48Khz
	WriteReg32(SC_BASE_ADDR + 0x4c, ReadReg32(SC_BASE_ADDR + 0x4c)& 0xfffc03ff | 0x0001f400); //[17:10] set to 125(0x7d)
	WriteReg32(SC_BASE_ADDR + 0x4c, ReadReg32(SC_BASE_ADDR + 0x4c)|BIT8 | BIT9);//enable clock
#endif
	
	//Audio Codec I2S I/F share settings
	/*
	  [15] I2S1 source 0:Pad 1:codec
	  [24] codec pad in  1:pad 0:I2S 
	*/
#ifdef AI1REC_AI2PLAY
	WriteReg32(SC_BASE_ADDR + 0x10, ReadReg32(SC_BASE_ADDR + 0x10)&~BIT24); 
#else
	WriteReg32(SC_BASE_ADDR + 0x10, ReadReg32(SC_BASE_ADDR + 0x10)&~BIT24|BIT15); //I2S1 for record, I2S1 source is codec
#endif

	/*
	[3]  Audio Code Record & playback sel 1:record 0:playback
	[2]  I2S0 source  0:PAD 1:Codec
	[1]  I2S sel  0:I2S0 1:I2S1
	*/
#ifdef AI1REC_AI2PLAY
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)|BIT2|BIT1); //choose I2S1 for playback
#else
	WriteReg32(SC_BASE_ADDR + 0xc, ReadReg32(SC_BASE_ADDR + 0xc)&~BIT1);//choose I2S0 for playback
#endif

	//[1]  Codec & I2S use pad 1:codec use pad 0:I2s Or GPIO use pad
	WriteReg32(SC_BASE_ADDR + 0x0, ReadReg32(SC_BASE_ADDR + 0x0) & ~BIT1);
//	WriteReg32(SC_BASE_ADDR + 0x0, ReadReg32(SC_BASE_ADDR + 0x0) | BIT1);

}





__acodec_cfg t_libacodec_setting g_interncodec_setting = {
	.early_init = codec_incodec_early_init,
	.init = codec_incodec_init,
	.ioctl = codec_incodec_ioctl
};


