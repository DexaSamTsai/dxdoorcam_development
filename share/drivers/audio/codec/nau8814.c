#include "includes.h"


static __acodec_cfg t_libacodec_setting g_nau8814_setting;
static t_libsccb_cfg h_nau8814_sccb;

static void nau8814_record_init(u32 master);
static void nau8814_playback_init(u32 master);
static s32 nau8814_set_dvol(s32 vol);

int codec_nau8814_init(E_AI_DIR dir,u32 master)
{
	switch(dir)
	{
		case AI_DIR_RECORD:
			nau8814_record_init(master);
			break;

		case AI_DIR_PLAY:
			nau8814_playback_init(master);
			break;
		default:
			debug_printf("codec_nau8814_init configuration error\n");
			return -1;
	}
	return 0;
}

int SCCB_Write(u8 addr,u16 value)
{
  addr<<=1;
  if(value & 0x100)
    addr|=1;

  return sccb_wr(&h_nau8814_sccb, addr,value & 0xff);
}

int codec_nau8814_ioctl(E_AI_IOCTL ioctl, void *arg)
{
	switch(ioctl)
	{
		case AI_CTL_CHN: 
			break;
		case AI_CTL_SR:
			//slave mode,no need to configure Sample rate
			{
			// sccb_wr(&h_nau8814_sccb,7*2,  0x00);//48K
			}
			break;
		case AI_CTL_DAC_DVOL:
			{
				s32 vol = *(s32 *)arg;
				return nau8814_set_dvol(vol);
			}
			
		case AI_CTL_MUTE_DAC:
			break;
		case AI_CTL_PWRDWN:	
			SCCB_Write(0xA,0); 
			SCCB_Write(0x2,0);
			SCCB_Write(0x3,0);
			break;
		case AI_CTL_EQ:
			break;
		default:
			break;
	}
	return 0;
}

//DAC GAIN, note:we only support the integar gain 
/*
0000 0000: not used
0000 0001: Digital volume control = -127dB
0000 0010: Digital volume control = -126.5dB
0000 0011: Digital volume control = -126dB
0000 0100: Digital volume control = -125.5dB
...
1111 1101: Digital volume control = -1 dB
1111 1110: Digital volume control = -0.5 dB
1111 1111: Digital volume control = 0 dB
*/
static s32 nau8814_set_dvol(s32 vol){
	debug_printf("nau8814_set_dvol set volume is %d\n",vol);
	//if(g_nau8814_setting.dvol.cur != vol)
	{
		u8 setval;
		s32 setvol;
		if(vol > 0)
		{
			setval = 0xff;
			setvol = 0;
			debug_printf("nau8814 not support digital volume bigger than 0 dB(%ddB), default to 0dB\n",vol);
		}
		else if(vol < -127)
		{
			setval = 0x1;
			setvol = -127;
			debug_printf("nau8814 does not support digital volume less than -127dB(%ddB),default to -127dB\n",vol);
		}
		else
		{
			setvol = vol;
			setval = 255 + vol*2;
		}	

		SCCB_Write(0xf,setval);
//		g_liba_dcfg.cur_vol = setvol;
		return setvol;
	}	
}

static void nau8814_playback_init(u32 master)
{
	int wret;
	h_nau8814_sccb.mode = SCCB_MODE8;
	h_nau8814_sccb.id = 0x34;
	h_nau8814_sccb.clk = 400*1024;
	h_nau8814_sccb.wr_check = 0;
	h_nau8814_sccb.timeout = 0;
	h_nau8814_sccb.retry = 1;
	//libsccb_init_old(0x34, 0);
	libsccb_init(&h_nau8814_sccb);
	debug_printf("dac_nau8814_init\n");
	
	g_nau8814_setting.dvol.max = 0;	//dB
	g_nau8814_setting.dvol.min = -127; //dB
		
	SCCB_Write(0,  0x000);   // Reset all registers 
    SCCB_Write(1,  0x020);
    SCCB_Write(2,  0x015);
    SCCB_Write(54,  0xff); // speaker gain 0x79,0dB /0xff, 6dB
   SCCB_Write(56,  0x001);  //DACOUT
   SCCB_Write(4,  0x010);   // 16-bit word length, AI format     
   SCCB_Write(5,  0x000);   // Companding control and loop back mode (all disable) 
   SCCB_Write(6,  0x104);   //Divide by 1, 48K  joe: slave mode, 0x104 or 0x04??
   SCCB_Write(7,  0x000);   // 48K for internal filter cofficients 
   
   // PLL
#if 0 //old  
   SCCB_Write(36,  0x018);//0X24 bit4:0,MCLK not devided;1,MCLK devided by 2
   SCCB_Write(37,  0x00C);//0X25
   SCCB_Write(38,  0x093);//0X26
   SCCB_Write(39,  0x0E9);//0X27
#else  
   SCCB_Write(36,  0x018);
   SCCB_Write(37,  0x031);//0X25
   SCCB_Write(38,  0x026);//0X26 
   SCCB_Write(39,  0x0e9);//0X27
#endif
   
   SCCB_Write(1,  0x025);

   SCCB_Write(3,  0x0E5);   //joe: enable DAC,enable spk mixer,disable mono mixer 
   SCCB_Write(10, 0x028);   // DAC softmute is disabled, 44Khz, DAC oversampling rate is 128x 
  // SCCB_Write(14, 0x108);   // ADC HP filter is disabled, ADC oversampling rate is 128x 
  // SCCB_Write(15, 0x0FF);   // ADC digital volume control 
   SCCB_Write(11, 0x0FF);    //DAC digital volume 
  // SCCB_Write(18, 0x16B); //EQ control
  // SCCB_Write(19, 0x10B); //EQ control
  // SCCB_Write(20, 0x12D); //EQ control
  // SCCB_Write(21, 0x10B); //EQ control
 //  SCCB_Write(22, 0x02D); //EQ control
  // SCCB_Write(44, 0x083); //Input Signal Control
 //  SCCB_Write(47, 0x170); //ADC Boost Control Registers
   SCCB_Write(50, 0x001);   //DAC to Speaker Mixer
   SCCB_Write(54, 0x03F);   // SPKOUT Volume 
  // SCCB_Write(32, 0x138);  //ALC control
  // SCCB_Write(33, 0x02D);  //ALC control
  // SCCB_Write(34, 0x022);  //ALC control
  // SCCB_Write(35, 0x000);  //NOISE GAIN CONTROL REGISTER
   // Enable bias voltage
  // SCCB_Write(3,  0x0FD);  //DAC Enable
  
   //SCCB_Write(0x31,  0x06);  //Speaker Output Boost Stage:(1.5 x VREF) Gain Boost, cannot use
   SCCB_Write(1,  0x03E);

	return;
}

static void nau8814_record_init(u32 master)
{
	int wret;
	h_nau8814_sccb.mode = SCCB_MODE8;
	h_nau8814_sccb.id = 0x34;
	h_nau8814_sccb.clk = 400*1024;
	h_nau8814_sccb.wr_check = 0;
	h_nau8814_sccb.timeout = 0;
	h_nau8814_sccb.retry = 1;
	//libsccb_init_old(0x34, 0);
	libsccb_init(&h_nau8814_sccb);
	debug_printf("dac_nau8814_init\n");
	
	g_nau8814_setting.dvol.max = 0;	//dB
	g_nau8814_setting.dvol.min = -127; //dB
	
   SCCB_Write(0,  0x000);   /* Reset all registers */
   SCCB_Write(1,  0x020);
   SCCB_Write(2,  0x015);
   SCCB_Write(54,  0x179);
   SCCB_Write(56,  0x001);
   SCCB_Write(4,  0x010);   /* 16-bit word length, AI format */      

   SCCB_Write(5,  0x000);   /* Companding control and loop back mode (all disable) */
  // SCCB_Write(6,  0x1AD);   //clock divider,BCLK=0b011:/8
   SCCB_Write(7,  0x000);   /* 48K for internal filter cofficients */

  // PLL
#if 0 //44100: desire output = 11.28960Mhz,11289600/256=44100   
   SCCB_Write(36,  0x007); //bit4=0,MCLK not devided/PLLN = 7
   SCCB_Write(37,  0x021);//0X25
   SCCB_Write(38,  0x161);//0X26 
   SCCB_Write(39,  0x026);//0X27
  // SCCB_Write(6,  0x185); //bit8=1,use PLL output  bit0=1: master mode;bit7-5=100(MCLKSEL): /4,44100/4=11025HZ
#else //48000: desire output = 12.28800Mhz,12288000/256=48000
  // SCCB_Write(36,  0x018); //bit4=1,MCLK devided by 2, PLLN = 8
  // SCCB_Write(37,  0x00c);//0X25
  // SCCB_Write(38,  0x093);//0X26 
  // SCCB_Write(39,  0x0e9);//0X27
  // SCCB_Write(6,  0x1A5);   //bit8=1,use PLL output  bit0=1: master mode;bit7-5=101(MCLKSEL): /6,48000/6=8000HZ
#endif 
   
   SCCB_Write(1,  0x025);
   SCCB_Write(3,  0x0ED);
   SCCB_Write(10, 0x008);   /* DAC softmute is disabled, DAC oversampling rate is 128x */
   SCCB_Write(14, 0x000);   /* ADC HP filter is disabled, ADC oversampling rate is 128x */
   SCCB_Write(15, 0x0FF);   /* ADC digital volume control */
   SCCB_Write(11, 0x0FF);
   SCCB_Write(18, 0x16B);
   SCCB_Write(19, 0x10B);
   SCCB_Write(20, 0x12D);
   SCCB_Write(21, 0x10B);
   SCCB_Write(22, 0x02D);
   SCCB_Write(44, 0x083);
   SCCB_Write(47, 0x170);
   SCCB_Write(50, 0x001);
   SCCB_Write(54, 0x03F);   /* SPKOUT Volume */
   SCCB_Write(32, 0x138);
   SCCB_Write(33, 0x02D);
   SCCB_Write(34, 0x022);
   SCCB_Write(35, 0x000);
   // Enable bias voltage
   SCCB_Write(3,  0x0FD);
   SCCB_Write(1,  0x03E);   
 
  // SCCB_Write(36,  0x018); //bit4=1,MCLK devided by 2, PLLN = 8
  // SCCB_Write(37,  0x00c);//0X25
  // SCCB_Write(38,  0x093);//0X26 
  // SCCB_Write(39,  0x0e9);//0X27 
  // SCCB_Write(6,  0x1AD);   //clock divider,BCLK=0b011:/8
 
	
	return;
}



static __acodec_cfg t_libacodec_setting g_nau8814_setting = {
	.early_init = NULL,
	.init = codec_nau8814_init,
	.ioctl = codec_nau8814_ioctl
	};


	

