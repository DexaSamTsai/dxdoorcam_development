#include "includes.h"

int libsccb_grp_check(int sccb_num)
{
	if(ReadReg8(SCCB_BASE_ADDR(sccb_num) + 0x18)&BIT4)	// check grp done
	{
		WriteReg32(SCCB_BASE_ADDR(sccb_num) + 0x20, 0x00);			//disable grp write mode
		return 1;
	}
	else
	{
		return 0;
	}
}

