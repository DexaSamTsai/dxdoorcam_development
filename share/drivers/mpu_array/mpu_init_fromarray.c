#include "includes.h"


extern u32 ram_mpubin_start;
extern u32 ram_mpubin_end;

#ifndef CONFIG_MPU_BASE
#define CONFIG_MPU_BASE 0x100a0000
#endif

int mpu_init_fromarray(void)
{
	if((ReadReg32(REG_SC_ADDR+0x60)&BIT0) == 0){
		//< ba22 is running now.
		return 0;
	}
	u8 *pba22_code = (u8*)(CONFIG_MPU_BASE | BIT31);

	u32 len = (u32)&ram_mpubin_end - (u32)&ram_mpubin_start;
	if(len == 0){
		printf("mpu_init_fromarray: MPU array length 0\n");
		return -1;
	}

	memcpy(pba22_code, &ram_mpubin_start, (u32)&ram_mpubin_end - (u32)&ram_mpubin_start);

	int i;
	for(i=0;i<10;i++){
		syslog(LOG_INFO,"ba22:0x%x\n",pba22_code[i]);
	}

	return 0;
}
