#include "includes.h"

#ifndef CONFIG_MPU_BASE
#define CONFIG_MPU_BASE 0x100a0000
#endif

int load_from_xmodem_rom(u32 baseaddr);
int mpu_init_from_uart(void)
{
	if((ReadReg32(REG_SC_ADDR+0x60)&BIT0) == 0){
		//< ba22 is running now.
		return 0;
	}

	u8 *pba22_code = (u8*)(CONFIG_MPU_BASE | BIT31);

	debug_printf("Xmodem download mpu.bin:\n");
	load_from_xmodem_rom((u32)pba22_code);

	int i;
	for(i=0;i<10;i++){
		debug_printf("ba22:0x%x\n",pba22_code[i]);
	}

	return 0;
}
