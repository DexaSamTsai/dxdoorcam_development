#include "includes.h"

#ifndef CONFIG_MPU_BASE
#define CONFIG_MPU_BASE 0x100a0000
#endif

int mpu_start_internal(u32 start);
int mbx_irq_handler(void * arg);

int mpu_start(void)
{
	if((ReadReg32(REG_SC_ADDR+0x60)&BIT0) == 0){
		//< ba22 is running now.
		irq_request(IRQ_BIT_MBX, mbx_irq_handler, "MBX", NULL);
		return 0;
	}else{
		return mpu_start_internal(CONFIG_MPU_BASE);
	}
}

int mpu_release(void)
{
	irq_free(IRQ_BIT_MBX);
	return 0;
}
