#include "includes.h"

typedef struct s_mpu_cmd{
	volatile u32 used;     ///<command status,1:command in used 0:command not in used
	volatile u32 arg;               ///<command argument
	volatile u32 id;         ///<command id
	volatile u32 ret;        ///<command return
	volatile u32 ret_ready;	///<0: not ready, 1:ready from MPU, 2:cond send to app
}t_mpu_cmd;

typedef struct s_mpu_sig{
	volatile u32 status;
}t_mpu_sig;

#define g_mpucmd (*(t_mpu_cmd *)(0x900cffe0))
#define g_mpusig (*(t_mpu_sig *)(0x900cfff8))

static pthread_mutex_t mpucmd_mutex_t = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t mpucmd_ret_cond_t = PTHREAD_COND_INITIALIZER;
//static u32 mpusig_prev_status = 0;
int mbx_irq_handler_t(void * arg)
{
	libmbx_m_read();

//	debug_printf("-mbx-");
	if(g_mpucmd.used && g_mpucmd.ret_ready == 1){
		g_mpucmd.ret_ready = 2;
		pthread_cond_signal(&mpucmd_ret_cond_t);
	}

#if 0
	u32 cur_status = g_mpusig.status;
	if(cur_status != mpusig_prev_status){
		int i;
		for(i=0; i<MPUEVT_MAX; i++){
			if(((mpusig_prev_status & (1 << i)) == 0) && ((cur_status & (1 << i)) != 0)){
				//flag set
				if(mpusig_evts[i] != NULL){
					ertos_event_broadcast(mpusig_evts[i]);
				}
			}
		}
		mpusig_prev_status = cur_status;
	}
#endif
	return 0;
}

int mpu_test_cmd(int try_cnt)
{
	int ba22_restart = 0;
	int n;

	irq_free(IRQ_BIT_MBX);
	irq_request(IRQ_BIT_MBX, mbx_irq_handler_t, "MBX", NULL);
	
	struct timespec timeout;
	timeout.tv_sec = 1;
	timeout.tv_nsec = 0;
	
	for(n=0; n<try_cnt; n++){
		ba22_restart = 0;
		if(g_ertos->inirq){
			printf("ERROR: mpu_cmd cannot be called in IRQ handler\n");
			while(1);
		}
		//get lock
		pthread_mutex_lock(&mpucmd_mutex_t);
	
		if(g_mpucmd.used){
			printf("ERROR: mpu_cmd get mutex but used\n");
			while(1);
		}

		//set command
		//'arg' was flushed before mpu_cmd so no cache issue.
		g_mpucmd.id = MPUCMD_SYS_TEST_BA; 
		g_mpucmd.arg = 0;
		g_mpucmd.ret = 0;
		g_mpucmd.ret_ready = 0;
		g_mpucmd.used = 1;


		//send to MPU
		libmbx_m2s(1);

		//wait for ACK
		if(pthread_cond_timedwait(&mpucmd_ret_cond_t, NULL, &timeout) != 0){
			debug_printf("ERROR: mpucmd_ret_cond != 0, continue waiting\n");
			ba22_restart = 1;
		}

		//clear used
		//'ret' will be dcache invalidated after mpu_cmd
		g_mpucmd.used = 0;

		//ret = g_mpucmd.ret;

		//unlock
		pthread_mutex_unlock(&mpucmd_mutex_t);
	
		if(ba22_restart == 0)
		{
			break;
		}

		if(ba22_restart == 1){
			mpu_stop();
			mpu_start();
			irq_free(IRQ_BIT_MBX);
			irq_request(IRQ_BIT_MBX, mbx_irq_handler_t, "MBX", NULL);
		}
	}

	irq_free(IRQ_BIT_MBX);
extern int mbx_irq_handler(void * arg);
	irq_request(IRQ_BIT_MBX, mbx_irq_handler, "MBX", NULL);
	if(ba22_restart == 0){
		return 0;
	}else{
		return -1;
	}
}

