#include "includes.h"

int mpu_setlogmask(char * taskname, int logmask)
{
	int prio = 0;
	if(taskname != NULL){
		dc_flush_range((u32)taskname, (u32)taskname + strlen(taskname) + 1);

		prio = mpu_cmd(MPUCMD_SYS_GETPRIO, (u32)taskname);
		if(prio < 0){
			return -1;
		}
	}else{
		prio = 0xffff;
	}

	int ret = mpu_cmd(MPUCMD_SYS_SETLOGMASK, (prio << 16) | logmask);
	if(ret < 0){
		return -2;
	}

	return 0;
}

static void mpu_logset_help(void)
{
	debug_printf("usage: mpulogset TASKNAME LOGLEVEL\n");
	debug_printf("    TASKNAME: mpu's task name, or \"all\" for all the tasks\n");
	debug_printf("    LOGLEVEL: off, crit, err, warn, notice, info, debug, 0xff\n");
}

int mpu_logset_cmdhandler(int argc, char ** argv)
{
	if(argc != 3){
		mpu_logset_help();
		return -1;
	}

	char * taskname = argv[1];
	if(!strcmp(taskname, "all")){
		taskname = NULL;
	}
	int logmask = 0;
	if(!strcmp(argv[2], "off")){
		logmask = 0;
	}else if(!strcmp(argv[2], "crit")){
		logmask = LOG_UPTO(MPULOG_CRIT);
	}else if(!strcmp(argv[2], "err")){
		logmask = LOG_UPTO(MPULOG_ERR);
	}else if(!strcmp(argv[2], "warn")){
		logmask = LOG_UPTO(MPULOG_WARNING);
	}else if(!strcmp(argv[2], "notice")){
		logmask = LOG_UPTO(MPULOG_NOTICE);
	}else if(!strcmp(argv[2], "info")){
		logmask = LOG_UPTO(MPULOG_INFO);
	}else if(!strcmp(argv[2], "debug")){
		logmask = LOG_UPTO(MPULOG_DEBUG);
	}else if(!strncmp(argv[2], "0x", 2)){
		logmask = (strtoul(argv[2], NULL, 0) & 0xff);
	}else{
		mpu_logset_help();
		return -2;
	}

	int ret = mpu_setlogmask(taskname, logmask);
	if(ret < 0){
		debug_printf("set mpu logmask failed:%d\n", ret);
		if(ret == -1){
			debug_printf("   mpu task name %s not found\n", argv[1]);
		}
		return -3;
	}else{
		debug_printf("mpu task %s logmask set to 0x%x\n", argv[1], logmask);
	}

	return 0;
}

#if defined(CONFIG_MPU_DEBUG_EN) && defined(CONFIG_ERTOS_CMDSHELL_EN)
CMDSHELL_DECLARE(mpulogset)
	.cmd = {'m', 'p', 'u', 'l', 'o', 'g', 's', 'e', 't', '\0'},
	.handler = mpu_logset_cmdhandler,
	.comment = "set MPU loglevel",
};
#endif
