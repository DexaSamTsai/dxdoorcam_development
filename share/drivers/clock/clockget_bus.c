#include "includes.h"

unsigned int clockget_bus(void)
{
#ifdef FOR_FPGA
	return 48000000;
#else
	u32 regdiv6 = ReadReg32(REG_SC_DIV6);
	u32 inclk = 0;

	switch((regdiv6  >> 5) & 3){
	case 0:
		inclk = clockget_cpubus();
		break;
	case 1:
		inclk = clockget_usbpll();
		break;
	case 2:
		inclk = clockget_syspll_isp();
		break;
	case 3:
	default:
		inclk = clockget_mipipll_ext();
		break;
	}

	int div = (regdiv6 & 0x1f) ;
	if(div == 0){
		div = 1;
	}

	return inclk / div;
#endif
}
