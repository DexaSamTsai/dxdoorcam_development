#include "includes.h"

unsigned int clockget_pll_vco(u32 v1, u32 v2)
{
	u32 prediv_sel = (v1 >> 16) & 7;
	u32 prediv = 0;

	switch(prediv_sel){
		case 0: prediv = 24000000/1; break;
		case 1: prediv = 16000000; break;
		case 2: prediv = 24000000/2; break;
		case 3: prediv = 9600000; break;
		case 4: prediv = 24000000/3; break;
		case 5: prediv = 24000000/4; break;
		case 6: prediv = 24000000/6; break;
		case 7: prediv = 24000000/8; break;
		default:prediv = 24000000; break;
	}

	u32 divp = v1 & 0xff;
	u32 divs = (v1 >> 19) & 1;
	u32 dsm  = v2 & 0x000fffff;
	
	if(v2&BIT26){
			return (prediv) * (2 * divp + divs) + (((prediv/5861)* dsm) >> 20)*5861;
	}
	return (prediv) * (2 * divp + divs);
}

unsigned int clockget_pll_sys(unsigned int base)
{
	u32 v1 = ReadReg32(base);
	u32 v2 = ReadReg32(base + 4);

	u32 vco = clockget_pll_vco(v1, v2);

	u32 div = (v1 >> 8) & 0xf;
	switch(div){
		case 2: 
		case 3: 
		case 4: 
		case 5: 
		case 6: 
		case 7: 
		case 8: 
		case 9: 
		case 10: 
			 return (vco/div);
		case 11:
		case 12:
		case 13:
			 return ((vco*2)/3);
		case 14:
		case 15:
			 return ((vco*2)/5);
		case 0:
		case 1:
		default:
		     return (vco);
	}
}

unsigned int clockget_pll_ext(unsigned int base)
{
	u32 v1 = ReadReg32(base);
	u32 v2 = ReadReg32(base + 4);

	u32 vco = clockget_pll_vco(v1, v2);

	u32 div = (v1 >> 12) & 0xf;
	switch(div){
		case 2: 
		case 3: 
		case 4: 
		case 5: 
		case 6: 
		case 7: 
		case 8: 
		case 9: 
		case 10: 
			 return (vco/div);
		case 11:
		case 12:
		case 13:
			 return ((vco*2)/3);
		case 14:
		case 15:
			 return ((vco*2)/5);
		case 0:
		case 1:
		default:
		     return (vco);
	}
}
