#include "includes.h"

static unsigned int clockget_cpupresys(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV0);

	u32 presys = 0;
	if((div0 & 1) == 0){
		presys = 24000000;
	}else{
		switch((div0 >> 1) & 3){
		case 0:
			presys = clockget_syspll_sys();
			break;
		case 1:
			presys = clockget_usbpll();
			break;
		case 2:
			presys = clockget_ddrpll_ext();
			break;
		case 3:
		default:
			presys = clockget_mipipll_ext();
			break;
		}
	}

	return presys;
}

static unsigned int clockget_cpusys(void)
{
//	u32 sysdiv = (ReadReg32(REG_SC_DIV0) >> 7) & 0x1f;
	u32 sysdiv = (ReadReg32(REG_SC_DIV1) >> 24) & 0xff;
	if(sysdiv == 0){
		sysdiv = 1;
	}

	u32 presys = clockget_cpupresys();

	return presys / sysdiv;
}

unsigned int clockget_pram(void)
{
	u32 sysdiv = (ReadReg32(REG_SC_ADDR + 0x3c) >> 8) & 0x7f;
	if(sysdiv == 0){
		sysdiv = 1;
	}

	u32 presys = clockget_cpupresys();

	return presys / sysdiv;
}

unsigned int clockget_cpu(void)
{
#ifdef FOR_FPGA
	return 96000000;
#endif
	u32 div0 = ReadReg32(REG_SC_DIV5);

	if(div0 & BIT29){
		return clockget_cpusys() / 2;
	}else{
		return clockget_cpusys();
	}
}

unsigned int clockget_cpubus(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV5);

	if(div0 & BIT28){
		return clockget_cpusys() / 2;
	}else{
		return clockget_cpusys();
	}
}
