#include "includes.h"

unsigned int clockget_mbus(void)
{
#ifdef FOR_FPGA
	return 48000000;
#endif

	u32 regdiv6 = ReadReg32(REG_SC_DIV6);
	u32 inclk = 0;

	switch((regdiv6  >> 13) & 3){
	case 0:
		inclk = clockget_cpubus();
		break;
	case 1:
		inclk = clockget_usbpll();
		break;
	case 2:
		inclk = clockget_syspll_isp();
		break;
	case 3:
	default:
		inclk = clockget_mipipll_ext();
		break;
	}

	int div = ((regdiv6 >> 8) & 0x1f) ;
	if(div == 0){
		div = 1;
	}

	return inclk / div;
}
