#include "includes.h"

void clocksif0_cfg(int pll_en, int div)
{
	if(pll_en){
		//from ddr_pll_ext
		WriteReg32(REG_SC_ADDR+0x48, (ReadReg32(REG_SC_ADDR+0x48)|BIT29|((div&0xfffff)<<24))&(~BIT30));
		WriteReg32(SC_BASE_ADDR+0x120, (ReadReg32(SC_BASE_ADDR+0x120)&0xffff0fff)|0x4000);		//196M ddr_pll as default
	}else{
		//from pad
		WriteReg32(REG_SC_ADDR+0x48, (ReadReg32(REG_SC_ADDR+0x48)|BIT29|BIT30));
	}
	return ;
}

//< sfc
void clocksif1_cfg(int pll_en, int div){
	if(pll_en){
		//from ddr_pll_ext
		WriteReg32(REG_SC_DIV5, (ReadReg32(REG_SC_DIV5)|BIT11|((div&0xfffff)<<6))&(~BIT12));
		WriteReg32(SC_BASE_ADDR+0x120, (ReadReg32(SC_BASE_ADDR+0x120)&0xffff0fff)|0x4000);		//196M ddr_pll as default
	}else{
		//from pad
		WriteReg32(REG_SC_DIV5, (ReadReg32(REG_SC_DIV5)|BIT11|BIT12));
	}
	return ;
}

void clocksif2_cfg(int pll_en, int div)
{
	if(pll_en){
		//from ddr_pll_ext
		WriteReg32(REG_SC_ADDR+0x134, (ReadReg32(REG_SC_ADDR+0x134)|BIT26|((div&0xfffff)<<21))&(~BIT27));
		WriteReg32(SC_BASE_ADDR+0x120, (ReadReg32(SC_BASE_ADDR+0x120)&0xffff0fff)|0x4000);		//196M ddr_pll as default
	}else{
		//from pad
		WriteReg32(REG_SC_ADDR+0x134, (ReadReg32(REG_SC_ADDR+0x134)|BIT26|BIT27));
	}
	return ;
}

