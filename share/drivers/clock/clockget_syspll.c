#include "includes.h"

unsigned int clockget_syspll_sys(void)
{
	return clockget_pll_sys(REG_SC_ADDR + 0x68);
}

unsigned int clockget_syspll_isp(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x68);
}
