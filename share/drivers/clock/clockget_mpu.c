#include "includes.h"

static unsigned int clockget_mpupresys(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV5);

	u32 presys = 0;
	if((div0 & BIT22) == 0){
		presys = 24000000;
	}else{
		switch((div0 >> 23) & 3){
		case 0:
			presys = clockget_syspll_sys();
			break;
		case 1:
			presys = clockget_usbpll();
			break;
		case 2:
			presys = clockget_ddrpll_ext();
			break;
		case 3:
		default:
			//presys = clockget_mipipll_ext(); //mipipll isn't the source of BA22, it should be mipi rx clock. But mipi rx clock needs sensor, so don't select the source clock.
			break;
		}
	}

	return presys;
}

static unsigned int clockget_mpusys(void)
{
	u32 presys = clockget_mpupresys();

	u32 sysdiv = (ReadReg32(REG_SC_DIV6) >> 23) & 0x3f;
	if(sysdiv == 0){
		sysdiv = 1;
	}

	return presys / sysdiv;
}

unsigned int clockget_mpupm(void)
{
	u32 pmdiv = ReadReg32(REG_SC_ADDR + 0x3c) & 0xff;
	if(pmdiv == 0){
		pmdiv = 1;
	}

	return clockget_mpupresys() / pmdiv;
}

unsigned int clockget_mpu(void)
{
	u32 div0 = ReadReg32(REG_SC_RISC);

	if(div0 & BIT11){
		return clockget_mpusys() / 2;
	}else{
		return clockget_mpusys();
	}
}

unsigned int clockget_mpubus(void)
{
	u32 div0 = ReadReg32(REG_SC_RISC);

	if(div0 & BIT2){
		return clockget_mpusys() / 2;
	}else{
		return clockget_mpusys();
	}
}
