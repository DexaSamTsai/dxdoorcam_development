#include "includes.h"

static unsigned int clockget_fbecif(void)
{
	u32 sel = (ReadReg32(REG_SC_ADDR + 0x130) >> 30) & 3;
	u32 clk = 0;
	switch(sel){
	case 2:
		clk = clockget_syspll_isp();
		break;
	case 3:
		clk = clockget_ddrpll_ext();
		break;
	case 1:
		clk = clockget_mipipll_ext();
		break;
	case 0:
	default:
		clk = clockget_usbpll();
		break;
	}

	return clk;
}

unsigned int clockget_fb0(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x130) >> 0) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_fb1(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x130) >> 5) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_fb2(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x130) >> 10) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_fb3(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x130) >> 15) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_fb4(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x130) >> 20) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_ecif0(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x134) >> 0) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

unsigned int clockget_ecif1(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR + 0x134) >> 5) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_fbecif() / div;
}

static unsigned int clockget_preisp(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV0);
	switch((div0 >> 15) & 3){
	case 0:
		return clockget_cpubus();
	case 1:
		return clockget_mipipll_ext();
	case 2:
		return clockget_syspll_isp();
	case 3:
	default:
		return clockget_ddrpll_ext();
	}
}

unsigned int clockget_d1(void)
{
	u32 div = (ReadReg32(REG_SC_DIV3) >> 18) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_preisp() / div;
}

unsigned int clockget_ispd1(void)
{
	u32 div = (ReadReg32(REG_SC_ADDR+0x130) >> 25) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_preisp() / div;
}

unsigned int clockget_d4(void)
{
	u32 div = (ReadReg32(REG_SC_DIV3) >> 24) & 0x7f;
	if(div == 0){
		div = 1;
	}

	return clockget_preisp() / div;
}

unsigned int clockget_v2(void)
{
	u32 div = (ReadReg32(REG_SC_DIV5) >> 0) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_preisp() / div;
}

unsigned int clockget_isp(void)
{
	u32 div1 = ReadReg32(REG_SC_DIV1);
	if((div1 & BIT6) == 0){
		return 24000000;
	}

	u32 div = div1 & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_preisp() / div;
}

unsigned int clockget_ve(void)
{
	u32 div7 = ReadReg32(REG_SC_DIV7);
	u32 preclk = 0;
	switch((div7 >> 14) & 3){
	case 0:
		preclk = clockget_mipipll_ext();
		break;
	case 1:
		preclk = clockget_ddrpll_ext();
		break;
	case 2:
		preclk = clockget_usbpll();
		break;
	case 3:
	default:
		preclk = clockget_syspll_sys();
		break;
	}

	u32 div = (div7 >> 8) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return preclk/div;
}
