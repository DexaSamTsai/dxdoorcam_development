#include "includes.h"

unsigned int clockget_mipitx(void)
{
	return clockget_pll_sys(REG_SC_ADDR + 0x110);
}

unsigned int clockget_mipipll_ext(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x110);
}
