#include "includes.h"

unsigned int clockget_ls(void)
{
	u32 div0 = ReadReg32(REG_SC_DIV0);

	u32 lsclk = 0;
	switch((div0 >> 17) & 3){
	case 0:
		lsclk = 24000000;
		break;
	case 1:
		lsclk = clockget_syspll_sys();
		break;
	case 2:
		lsclk = clockget_ddrpll_ext();
		break;
	case 3:
	default:
		lsclk = clockget_usbpll();
		break;
	}

	return lsclk;
}

unsigned int clockget_scifsys(void)
{
#ifdef FOR_FPGA
	return 96000000;
#endif

	u32 div2 = ReadReg32(REG_SC_DIV2);
/*
	if((div2 & BIT5) == 0){
		return 0;
	}
*/
	if((div2 & BIT6) != 0){
		return 24000000;
	}

	u32 div = div2 & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_ls() / div;
}

unsigned int clockget_sciosys(void)
{
#ifdef FOR_FPGA
	return 96000000;
#endif

	u32 div2 = ReadReg32(REG_SC_DIV2);
/*
	if((div2 & BIT13) == 0){
		return 0;
	}
*/
	if((div2 & BIT14) != 0){
		return 24000000;
	}

	u32 div = (div2 >> 8) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_ls() / div;
}

unsigned int clockget_sfcsys(void)
{
#ifdef FOR_FPGA
	return 48000000;
#endif

	u32 div5 = ReadReg32(REG_SC_DIV5);
/*
	if((div2 & BIT5) == 0){
		return 0;
	}
*/
	if((div5 & BIT12) != 0){
		return 24000000;
	}

	u32 div = (div5>>6) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return clockget_ddrpll_ext()/div;
}


unsigned int clockget_cclk(void)
{
	u32 div1 = ReadReg32(REG_SC_DIV1);
	u32 div = (div1 >> 8) & 0x1f;
	if(div == 0){
		div = 1;
	}

	u32 sinclk = 0;
	if(div1 & BIT14){
		sinclk = 24000000;
	}else{
		sinclk = clockget_ls();
	}

	return sinclk / div;
}

unsigned int clockget_img(void)
{
	u32 div5 = ReadReg32(REG_SC_DIV5);
	u32 preclk = 0;

	if(div5 & BIT20){
		preclk = 24000000;
	}else{
		preclk = clockget_ls();
	}

	u32 div = (div5 >> 14) & 0x1f;
	if(div == 0){
		div = 1;
	}

	return preclk/div;
}
