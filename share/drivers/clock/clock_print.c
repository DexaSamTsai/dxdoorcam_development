#include "includes.h"

extern unsigned int clockget_pll_vco(u32 v1, u32 v2);

static unsigned int _clockget_pll_vco(unsigned int base)
{
	u32 v1 = ReadReg32(base);
	u32 v2 = ReadReg32(base + 4);

	return clockget_pll_vco(v1, v2);
}

int clock_print(int argc, char ** argv)
{
	debug_printf("-----------------------------\n");
	debug_printf("SYSPLL VCO: %d MHz\n", _clockget_pll_vco(REG_SC_ADDR + 0x68) / 1000000);
	debug_printf("SYSPLL SYS: %d MHz\n", clockget_syspll_sys() / 1000000 );
	debug_printf("SYSPLL ISP: %d MHz\n", clockget_syspll_isp() / 1000000 );
	debug_printf("DDRPLL VCO: %d MHz\n", _clockget_pll_vco(REG_SC_ADDR + 0x120) / 1000000);
	debug_printf("DDR       : %d MHz\n", clockget_ddr() / 1000000 );
	debug_printf("DDRPLL EXT: %d MHz\n", clockget_ddrpll_ext() / 1000000 );
	debug_printf("MPIPLL VCO: %d MHz\n", _clockget_pll_vco(REG_SC_ADDR + 0x110) / 1000000);
	debug_printf("MIPITX    : %d MHz\n", clockget_mipitx() / 1000000 );
	debug_printf("MPIPLL EXT: %d MHz\n", clockget_mipipll_ext() / 1000000);
	debug_printf("USBPLL    : %d MHz\n", clockget_usbpll() / 1000000 );
	debug_printf("\n");
	debug_printf("CPU       : %d MHz\n", clockget_cpu() / 1000000 );
	debug_printf("CPU BUS   : %d MHz\n", clockget_cpubus() / 1000000 );
	debug_printf("MPU       : %d MHz\n", clockget_mpu() / 1000000 );
	debug_printf("MPU BUS   : %d MHz\n", clockget_mpubus() / 1000000 );
	debug_printf("MPU PMCLK : %d MHz\n", clockget_mpupm() / 1000000 );
	debug_printf("PRAM      : %d MHz\n", clockget_pram() / 1000000 );
	debug_printf("BUS       : %d MHz\n", clockget_bus() / 1000000 );
	debug_printf("MBUS      : %d MHz\n", clockget_mbus() / 1000000 );
	debug_printf("\n");
	debug_printf("FB0       : %d MHz\n", clockget_fb0() / 1000000 );
	debug_printf("FB1       : %d MHz\n", clockget_fb1() / 1000000 );
	debug_printf("FB2       : %d MHz\n", clockget_fb2() / 1000000 );
	debug_printf("FB3       : %d MHz\n", clockget_fb3() / 1000000 );
	debug_printf("FB4       : %d MHz\n", clockget_fb4() / 1000000 );
	debug_printf("ECIF0     : %d MHz\n", clockget_ecif0() / 1000000 );
	debug_printf("ECIF1     : %d MHz\n", clockget_ecif1() / 1000000 );
	debug_printf("ISP       : %d MHz\n", clockget_isp() / 1000000 );
	debug_printf("D1/RGBIR  : %d MHz\n", clockget_d1() / 1000000 );
	debug_printf("ISPD1     : %d MHz\n", clockget_ispd1() / 1000000 );
	debug_printf("D4/MIPI   : %d MHz\n", clockget_d4() / 1000000 );
	debug_printf("YUVSCALE  : %d MHz\n", clockget_v2() / 1000000 );
	debug_printf("VE        : %d MHz\n", clockget_ve() / 1000000 );
	debug_printf("IMG       : %d MHz\n", clockget_img() / 1000000 );
	debug_printf("CCLK      : %d MHz\n", clockget_cclk() / 1000000 );
	debug_printf("SCIF SYS  : %d MHz\n", clockget_scifsys() / 1000000 );
	debug_printf("SCIO SYS  : %d MHz\n", clockget_sciosys() / 1000000 );

	debug_printf("-----------------------------\n");

	return 0;
}

#if defined(CONFIG_ERTOS_DEBUG_EN) && defined(CONFIG_ERTOS_CMDSHELL_EN)
CMDSHELL_DECLARE(clock)
	.cmd = {'c', 'l','o','c','k','\0'},
	.handler = clock_print,
	.comment = "show clock config",
};
#endif
