#include "includes.h"
//#define D1_216MHZ
#define LOWPOWER
//#define LEXP_ONLY
#define USE_FB3
#define FB2_REUSE
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//  system ddr sdram clock set 
//  ddrclk_src: 00: pll1_sysclk(default) 01: pll2_clk 10: pad clk  11: usb 240M
//  divider   : divider [2:0]
//  ddr_enable: 0:gate 1:enable
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
static void ddr_clk_set(int ddrclk_src, int divider, int ddr_enable)
{
	volatile int sr;
	sr = ReadReg32(SC_BASE_ADDR+0x40);
	WriteReg32(SC_BASE_ADDR+0x40, ((sr&0xffff8fa7) | (divider << 12) | (ddr_enable << 6) | (ddrclk_src << 3)));
}
*/

//low speed clock src sel
//ls_src_sel : low speed clk source 
//             11: usb 240M
//             10: pll2 clk
//             01: pll1 sys clk
//             00: pad clk
static void lsclk_sel(int ls_src_sel)
{
	volatile int sr;
	sr = ReadReg32(SC_BASE_ADDR+0x40);
	WriteReg32(SC_BASE_ADDR+0x40, ((sr&0xfff9ffff) | (ls_src_sel << 17)) );
}

//#define TEST_SYSPLL348M
void clockconfig_360M_4M_4686(void)
{
    r3_syspll_set(
                   0,//int prediv, //2, VCO step by 12M
                   2,//int divsys,
#ifdef D1_216MHZ
                   2,//15,//2,//int divisp, 
#else
                   3,//15,//2,//int divisp, 
#endif
#ifdef TEST_SYSPLL348M
                   14,//int pdiv,
                   1,//int sdiv,
#else
                   15,//int pdiv,//config to 30 if prediv is set to 2
                   0,//int sdiv,
#endif
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0x289//int dsm
                   );//set pll1 to 360MHz


    WriteReg8(SC_BASE_ADDR+0x3c,0x5);//work around of the timing issue of ba22 interrupt function

    //set A5 risc clock to 360MHz, and PRAM clock to 120MHz
    WriteReg32(SC_BASE_ADDR+0x3c,(ReadReg32(SC_BASE_ADDR+0x3c)&0xffff80ff)|0x2<<8);//set PRAM clock to 120MHz, r_pram_div_o = r_div[15:8]
    WriteReg32(SC_BASE_ADDR+0x40,(ReadReg32(SC_BASE_ADDR+0x40)|BIT31|BIT0));  //sel pllsysclk

    asm("wfe");
    asm("wfe");
 
    //Set DDR PLL as 528M
    //WriteReg32(SC_BASE_ADDR+0x120,0x0C008016);     // 528Mhz
    //WriteReg32(SC_BASE_ADDR+0x120,0x0c008019);     // 600Mhz
    //WriteReg32(SC_BASE_ADDR+0x120,0x0C08801B);     // 660Mhz

	///< the below two setting is for mipitx output
//	WriteReg32(SC_BASE_ADDR+0x110,0xc08240a);		//120Mbps
	//WriteReg32(SC_BASE_ADDR+0x110,0xc08240b);		//120Mbps
	r3_mipitxpll_set(
                   0,//int prediv, 
                   4,//int divsys,
                   2,//int divisp, 
                   11,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0//int dsm
                   );//set pll to 138M

	WriteReg32(SC_BASE_ADDR+0x78, ReadReg32(SC_BASE_ADDR+0x78)&(~BIT1));

    //set ba22 clock to 180MHz
    //ba22 clock div
    WriteReg32(SC_BASE_ADDR+0x58,(ReadReg32(SC_BASE_ADDR+0x58)|0x2<<23));  //r_ba22_div_i=r_div6[28:23]
    //open ba22 fast clock,BIT22
    WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)|BIT22|BIT25));

    //open usb clock
    WriteReg32(REG_SC_CLK2,(ReadReg32(REG_SC_CLK2)|BIT0|BIT1));//open usb clock
    WriteReg32(REG_SC_DIV3,(ReadReg32(REG_SC_DIV3) & 0xffffff80) | 0x60);//usb phy reference clk
    WriteReg32(REG_SC_ALG,(ReadReg32(REG_SC_ALG)|BIT20));//release suspend usb phy

    WriteReg32(REG_SC_DIV2,(ReadReg32(REG_SC_DIV2)&0xffffe0e0)|0x404);	//sd/sdio module clk in use padclk 

#ifdef LOWPOWER
	WriteReg32(REG_SC_ADDR+0x10, ((ReadReg32(REG_SC_ADDR+0x10)&0xfff3ffff)|(3<<18)));//OSD0 select DCPC
	WriteReg32(REG_SC_ADDR+0x18, (ReadReg32(REG_SC_ADDR+0x18)&0xff8fffff)|(2<<20));//yuvscale0 select DCPC
	WriteReg32(REG_SC_ADDR+0x20, 0x7D00FFB6&(~BIT30));//no reset SPI0
	WriteReg32(REG_SC_ADDR+0x24, 0xBFFFFF76);
	WriteReg32(REG_SC_ADDR+0x28, 0x23F7FFFE);
	WriteReg32(REG_SC_ADDR+0x2c, 0x9FFFF7FF&(~BIT28));//no rreset SPI0 
	WriteReg32(REG_SC_ADDR+0x84, 0xF98F1FFE);
	WriteReg32(REG_SC_ADDR+0x30, 0x38000000); 
	WriteReg32(REG_SC_ADDR+0x34, 0x10000019); 
	WriteReg32(REG_SC_ADDR+0x38, 0x1A80);
#else
    //release all resets, enable all clocks
    WriteReg32(SC_BASE_ADDR+0x20,0x0);    //SC reset release: sel_rst_lg0
    WriteReg32(SC_BASE_ADDR+0x24,0x0);    //SC reset release: sel_rst_lg1
    WriteReg32(SC_BASE_ADDR+0x28,0x0);    //SC reset release: sel_rst_rg0
    WriteReg32(SC_BASE_ADDR+0x2c,0x0);    //SC reset release: sel_rst_rg1
    WriteReg32(SC_BASE_ADDR+0x84,0x0);    //SC reset release: sel_rst_lg2 
    WriteReg32(SC_BASE_ADDR+0x30,0xffffffff); //SC clk enable: sel_clk0
    WriteReg32(SC_BASE_ADDR+0x34,0xffffffff); //SC clk enable: sel_clk1
    WriteReg32(SC_BASE_ADDR+0x38,0xffffffff); //SC clk enable: sel_clk2
#endif

#ifdef D1_216MHZ
	u8 fb_div = 0x4;
	u8 fb1_div = 0x6;
	u8 ecif_fb_src = 0;//0: USBPLL, 1: mipitx pll 2: ISPPLL, 3: ddrpll
	u8 ecif_div = 2;
#else
	__unused u8 fb_div = 4;
	__unused u8 fb1_div = 6;
	__unused u8 ecif_fb_src = 3;//0: USBPLL, 1: mipitx pll 2: ISPPLL, 3: ddrpll
	__unused u8 ecif_div = 2;
	__unused u8 fb4_div = 5;
	__unused u8 fb3_div = 5;
#endif
	u8 ecif0_div = 4;
    WriteReg32(SC_BASE_ADDR+0x134,((ecif_div<<5)|ecif0_div));//ecif0_clk div=2
   
   	u8 ispd1_div = 2; 
#ifdef LEXP_ONLY
	ecif_fb_src = 2;
	ispd1_div = 4;
	fb4_div = 8;
	fb3_div = 8;
#endif
	u8 isp_div = ispd1_div * 2;//5 for single stream
    //idc v2 clk
    WriteReg32(SC_BASE_ADDR+0x44,(ReadReg32(SC_BASE_ADDR+0x44)&0xffffffe0)|BIT5|BIT6|(isp_div)); //4:isp_div, isp clk = d1/2
    //set FB clock to 30MHz

#ifdef LOWPOWER
#ifdef LEXP_ONLY
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|fb1_div|(0x1f<<5)|(0x1f<<10)|(fb3_div<<15)|(fb4_div<<20));
    WriteReg32(SC_BASE_ADDR+0x134,((0x1f<<5)|ecif0_div));//ecif0_clk div=2
#else
#ifdef FB2_REUSE
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|fb_div|(0x1f<<5)|(fb1_div<<10)|(fb3_div<<15)|(0x1f<<20));
#elif defined(USE_FB3)
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|fb1_div|(fb3_div<<5)|(fb1_div<<10)|(fb_div<<15)|(fb4_div<<20));
#else
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|fb1_div|(fb_div<<5)|(fb1_div<<10)|(fb3_div<<15)|(fb4_div<<20));
#endif
#endif
#else
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|3|(3<<5)|(5<<10)|(3<<15)|(3<<20));
#endif
	//[31:30], ecif/fb source,[29:25], d1_div
	//fb0~fb4 div r_fb_div[4:0];r_fb_div[9:5];r_fb_div[14:10];r_fb_div[19:15];r_fb_div[24:20]

    lsclk_sel(1);
        
	//set 12M Audio codec clock
	//[22] source sel 0:pad clk 1:usb 240M clk
	//[21] clk gate
	//[20:16] divider
	WriteReg32(SC_BASE_ADDR + 0x44, (ReadReg32(SC_BASE_ADDR + 0x44) & 0xff80ffff));
    WriteReg32(SC_BASE_ADDR + 0x44, (ReadReg32(SC_BASE_ADDR + 0x44) | 0x00220000)); // bit22=0,bit21=1 bit20:16=2 , use pad clock source
//  WriteReg32(SC_BASE_ADDR + 0x44, ReadReg32(SC_BASE_ADDR + 0x44) | 0x00740000); // bit22=1,bit21=1 bit20:16=20, use usbpll source, divided by 20
	
	
    //enable ve clk,BIT13, set ve clock to 180MHz
    WriteReg32(SC_BASE_ADDR+0x5c,ReadReg32(SC_BASE_ADDR+0x5c)|BIT13|(0x2<<14)|(0x1<<8));//ve select USBPLL, 240Mhz
//    WriteReg32(SC_BASE_ADDR+0x5c,ReadReg32(SC_BASE_ADDR+0x5c)|BIT13|(0x3<<14)|(0x2<<8));//set ve clk div = 2
	WriteReg32(SC_BASE_ADDR+0x5c,((ReadReg32(SC_BASE_ADDR+0x5c)&0xffffff)|(0x82<<24)));//enable cryption module,and set divider 2
    
    //enable img clk,BIT19, set img clock to 180MHz
#ifdef LOWPOWER
	WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)& ((~0x7f)<<14))|(0<<19)|(0<<20)|(0xff<<14));
#else
	WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)& ((~0x7f)<<14))|(1<<19)|(0<<20)|(0x2<<14));
#endif
	//bit19: jpeg en; bit20: 0 choose ls clock; bit14~18: img clk div 
        
        
    //Open D1->D4 clock
//    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x00ffffff)|BIT23|BIT31|d1_div<<18|(d1_div * 4)<<24);//d1_clk div=1
//    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x00ffffff)|BIT23|BIT31|d1_div<<18|(d1_div * 2)<<24);//d1_clk div=1
#ifdef D1_216MHZ
	u8 d1_div = 2;
    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x0003ffff)|BIT23|BIT31|(1<<18)|((d1_div * 2)<<24));//d1_clk div=1
    WriteReg32(SC_BASE_ADDR+0x120,(ReadReg32(SC_BASE_ADDR + 0x120)&(~0xf000))|(5<<12));
    WriteReg32(SC_BASE_ADDR+0x40,(ReadReg32(SC_BASE_ADDR+0x40)|BIT0 | (0x1 << 17) | (0x3<<15))); //ispclk in pll2 clk, lsclk in PLL
#else
	u8 d1_div = 1;
    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x0003ffff)|BIT23|BIT31|(d1_div<<18)|((d1_div * 4)<<24));//d1_clk div=1
    WriteReg32(SC_BASE_ADDR+0x40,(ReadReg32(SC_BASE_ADDR+0x40)|BIT0 | (0x1 << 17) | BIT16)); //ispclk in pll2 clk, lsclk in PLL
#endif
//    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x00ffffff)|BIT23|BIT31|d1_div<<18|(3)<<24);//d1_clk div=1
    //Open V2 clock
    WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)&0xffffffe0) | BIT5|isp_div);//yuvscale clk_div=4

	//sif0/sif1/sif2(ddr_pll_ext is 99M)
	clocksif0_cfg(1, 1);
	clocksif1_cfg(1, 1);
	clocksif2_cfg(1, 1);
#ifdef D1_216MHZ
	WriteReg32(SC_BASE_ADDR+0x120,(ReadReg32(SC_BASE_ADDR + 0x120)&(~0xf000))|(5<<12));
#endif
	WriteReg32(0xc0004d08,  ReadReg32(0xc0004d08)&(~(0x3<<6)));//ECIF1 read set to highest priority, ECIF1 port3, ECIF0 port4
#ifdef USE_FB3
	WriteReg32(0xc0004d00,  ReadReg32(0xc0004d00)&(~(0x3<<12)));//FB3 write set to highest priority
#else
	WriteReg32(0xc0004d00,  ReadReg32(0xc0004d00)&(~(0x3<<18)));//FB1 write set to highest priority
#endif
	WriteReg32(0xc0004d00,  ReadReg32(0xc0004d00)&(~(0x3<<10)));//FB0 write set to highest priority
#ifdef LOWPOWER
#ifdef LEXP_ONLY
	WriteReg32(REG_SC_ADDR+0x78, (ReadReg32(REG_SC_ADDR+0x78)|BIT1)); 
	//reset pll2 mipitxpll, VE by default use MIPITX PLL, should not reset mipi TX pll until VE select another source
#endif
	WriteReg32(REG_SC_ADDR+0x40, ReadReg32(REG_SC_ADDR+0x40)&(~BIT24));
	WriteReg32(REG_SC_ADDR+0x4c, ReadReg32(REG_SC_ADDR+0x4c)&(~BIT5));	
	WriteReg32(REG_SC_ADDR+0x48, ReadReg32(REG_SC_ADDR+0x48)&(~(BIT5)));//disable SD
	WriteReg32(REG_SC_ADDR+0x54, ReadReg32(REG_SC_ADDR+0x54)&(~BIT19));//DISABLE JPEG
//	WriteReg32(REG_SC_ADDR+0x54, ReadReg32(REG_SC_ADDR+0x54)&(~BIT11));//DISABLE SPI1
	WriteReg32(REG_SC_ADDR+0x58, ReadReg32(REG_SC_ADDR+0x58)&(~BIT21));
	WriteReg32(REG_SC_ADDR+0x5c, ReadReg32(REG_SC_ADDR+0x5c)&(~(BIT21|BIT31)));
	WriteReg32(REG_SC_ADDR+0xc0, (ReadReg32(REG_SC_ADDR+0xc0)|BIT12)&(~BIT13)); //close pll of usb_host_phy 
#endif

	//set i2s clk to 12MHz
	u32 sysclk = clockget_ls();
	u32 i2s_div = sysclk/12000000;
	WriteReg32(REG_SC_DIV3, (ReadReg32(REG_SC_DIV3)&~(0xff<<10))|BIT8|BIT9|(i2s_div<<10));	
}
