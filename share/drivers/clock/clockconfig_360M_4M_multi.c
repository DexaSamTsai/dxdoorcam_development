#include "includes.h"

//#define LOWPOWER
//low speed clock src sel
//ls_src_sel : low speed clk source 
//             11: usb 240M
//             10: pll2 clk
//             01: pll1 sys clk
//             00: pad clk
static void lsclk_sel(int ls_src_sel)
{
	volatile int sr;
	sr = ReadReg32(SC_BASE_ADDR+0x40);
	WriteReg32(SC_BASE_ADDR+0x40, (sr&0xfff9ffff) | (ls_src_sel << 17) );
}

void clockconfig_360M_4M_multi(void)
{
    r3_syspll_set(
                   0,//int prediv, 
                   2,//int divsys,
                   2,//int divisp, 
                   14,//int pdiv,
                   1,//int sdiv,
                   0,//int bypass, 
                   0,//int intn_en,
                   0,//int sscg_en,
                   0x289//int dsm
                   );//set pll1 to 360MHz


    WriteReg8(SC_BASE_ADDR+0x3c,0x5);//work around of the timing issue of ba22 interrupt function

    //set A5 risc clock to 360MHz, and PRAM clock to 120MHz
    WriteReg32(SC_BASE_ADDR+0x3c,(ReadReg32(SC_BASE_ADDR+0x3c)&0xffff80ff)|0x2<<8);//set PRAM clock to 120MHz, r_pram_div_o = r_div[15:8]
    WriteReg32(SC_BASE_ADDR+0x40,ReadReg32(SC_BASE_ADDR+0x40)|BIT31|BIT0);  //sel pllsysclk

	{volatile int aa; for(aa = 0; aa < 0x100; aa++);}

    asm("wfe");
    asm("wfe");
 
    //Set DDR PLL as 528M
    //WriteReg32(SC_BASE_ADDR+0x120,0x0C008016);     // 528Mhz
    //WriteReg32(SC_BASE_ADDR+0x120,0x0c008019);     // 600Mhz
    //WriteReg32(SC_BASE_ADDR+0x120,0x0C08801B);     // 660Mhz

    //set ba22 clock to 180MHz
    //ba22 clock div
    WriteReg32(SC_BASE_ADDR+0x58,ReadReg32(SC_BASE_ADDR+0x58)|0x2<<23);  //r_ba22_div_i=r_div6[28:23]
    //open ba22 fast clock,BIT22
    WriteReg32(SC_BASE_ADDR+0x54,ReadReg32(SC_BASE_ADDR+0x54) |BIT22|BIT25);

    //open usb clock
    WriteReg32(REG_SC_CLK2,ReadReg32(REG_SC_CLK2)|BIT0|BIT1);//open usb clock
    WriteReg32(REG_SC_DIV3,(ReadReg32(REG_SC_DIV3) & 0xffffff80) | 0x60);//usb phy reference clk
    WriteReg32(REG_SC_ALG,ReadReg32(REG_SC_ALG)|BIT20);//release suspend usb phy

	WriteReg32(REG_SC_DIV2,(ReadReg32(REG_SC_DIV2)&0xffffe0e0)|0x404);	//sd/sdio module clk in use padclk 

#ifdef LOWPOWER
	WriteReg32(REG_SC_ADDR+0x10, ((ReadReg32(REG_SC_ADDR+0x10)&0xfff3ffff)|(3<<18)));//OSD0 select DCPC
	WriteReg32(REG_SC_ADDR+0x18, (ReadReg32(REG_SC_ADDR+0x18)&0xff8fffff)|(2<<20));//yuvscale0 select DCPC
	WriteReg32(REG_SC_ADDR+0x20, 0x7D00FFB6&(~BIT30));//no reset SPI0
	WriteReg32(REG_SC_ADDR+0x24, 0xBFFFFF76);
	WriteReg32(REG_SC_ADDR+0x28, 0x23F7FFFE);
	WriteReg32(REG_SC_ADDR+0x2c, 0x9FFFF7FF&(~BIT28));//no rreset SPI0 
	WriteReg32(REG_SC_ADDR+0x84, 0xF98F1FFE);
	WriteReg32(REG_SC_ADDR+0x30, 0x38000000); 
	WriteReg32(REG_SC_ADDR+0x34, 0x10000019); 
	WriteReg32(REG_SC_ADDR+0x38, 0x1A80);
#else
    //release all resets, enable all clocks
    WriteReg32(SC_BASE_ADDR+0x20,0x0);    //SC reset release: sel_rst_lg0
    WriteReg32(SC_BASE_ADDR+0x24,0x0);    //SC reset release: sel_rst_lg1
    WriteReg32(SC_BASE_ADDR+0x28,0x0);    //SC reset release: sel_rst_rg0
    WriteReg32(SC_BASE_ADDR+0x2c,0x0);    //SC reset release: sel_rst_rg1
    WriteReg32(SC_BASE_ADDR+0x84,0x0);    //SC reset release: sel_rst_lg2 
    WriteReg32(SC_BASE_ADDR+0x30,0xffffffff); //SC clk enable: sel_clk0
    WriteReg32(SC_BASE_ADDR+0x34,0xffffffff); //SC clk enable: sel_clk1
    WriteReg32(SC_BASE_ADDR+0x38,0xffffffff); //SC clk enable: sel_clk2
#endif

	//ecif/fb selet syspll_isp
	u8 ecif_div = 4;
	u8 fb_div = 7;
	u8 ecif_fb_src = 0;//0: USBPLL, 1: mipitx pll 2: ISPPLL, 3: ddrpll
   
    WriteReg32(SC_BASE_ADDR+0x40,ReadReg32(SC_BASE_ADDR+0x40)|BIT0 | (0x1 << 17) | BIT16); //ispclk in pll2 clk, lsclk in PLL
   	u8 ispd1_div = 5; 
    //idc v2 clk
    WriteReg32(SC_BASE_ADDR+0x44,(ReadReg32(SC_BASE_ADDR+0x44)&0xffffffe0)|BIT5|BIT6|(ispd1_div * 2)); //4:isp_div, isp clk = d1/2

#ifdef LOWPOWER
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|0x1f|(0x1f<<5)|(fb_div<<10)|(fb_div<<15)|(fb_div<<20));
    WriteReg32(SC_BASE_ADDR+0x134,(0x1f<<5)|ecif_div);//ecif0_clk div=2
#else
    WriteReg32(SC_BASE_ADDR+0x134,(ecif_div<<5)|ecif_div);//ecif0_clk div=2
    //set FB clock
    WriteReg32(SC_BASE_ADDR+0x130, (ecif_fb_src<<30)|(ispd1_div<<25)|fb_div|(fb_div<<5)|(fb_div<<10)|(fb_div<<15)|(fb_div<<20));
#endif
	//[31:30], ecif/fb source,[29:25], d1_div
	//fb0~fb4 div r_fb_div[4:0];r_fb_div[9:5];r_fb_div[14:10];r_fb_div[19:15];r_fb_div[24:20]
    
    lsclk_sel(1);
        

	//set 12M Audio codec clock
	//[22] source sel 0:pad clk 1:usb 240M clk
	//[21] clk gate
	//[20:16] divider
	WriteReg32(SC_BASE_ADDR + 0x44, ReadReg32(SC_BASE_ADDR + 0x44) & 0xff80ffff);
    WriteReg32(SC_BASE_ADDR + 0x44, ReadReg32(SC_BASE_ADDR + 0x44) | 0x00220000); // bit22=0,bit21=1 bit20:16=2 , use pad clock source
	
    //enable ve clk,BIT13, set ve clock to 180MHz
//    WriteReg32(SC_BASE_ADDR+0x5c,ReadReg32(SC_BASE_ADDR+0x5c)|BIT13|(0x3<<14)|(0x1<<8));//set ve clk div = 1, 360Mhz 
    WriteReg32(SC_BASE_ADDR+0x5c,ReadReg32(SC_BASE_ADDR+0x5c)|BIT13|(0x2<<14)|(0x1<<8));//ve select USBPLL, 240Mhz
	WriteReg32(SC_BASE_ADDR+0x5c,(ReadReg32(SC_BASE_ADDR+0x5c)&0xffffff)|(0x82<<24));//enable cryption module,and set divider 2
    
    //enable img clk,BIT19, set img clock to 180MHz
#ifdef LOWPOWER
	WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)& ((~0x7f)<<14))|(0<<19)|(0<<20)|(0xff<<14));
	WriteReg32(REG_SC_ADDR+0x78, (ReadReg32(REG_SC_ADDR+0x78)|BIT1)); 
	//reset pll2 mipitxpll, VE by default use MIPITX PLL, should not reset mipi TX pll until VE select another source
#else
	WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)& ((~0x7f)<<14))|(1<<19)|(0<<20)|(0x2<<14));
#endif
	//bit19: jpeg en; bit20: 0 choose ls clock; bit14~18: img clk div 
        
    /*
	 NOTE for D1/D4/YUVSCALE/FB clock:
	 1.by default D1 clock =ISP_clock * 2, D4 = ISP_clock/2; but there are other limitations for D1&D4 clock.
	 2.RGBIR(D1) clock should be no less than sensor bps * laneNUM/10, in this case 648Mbps@2Lane, RGBIR(D1) clock should be no less than 130Mhz(However in my test, D1=139Mhz still failed, need to set to 144Mhz). 
	 3.MIPI(D4) clock should not be less than sensor bps * laneNUM/10/4, in this case 32.5Mhz, and should be D1_clock/4.
	 4.If YUVSCALE connect ECIF, should be no less than ECIF_clock/2. If connect ISP, should be no less than ISP_clock.
	 5.If FB connect ISP, should be no less than ISP_clock.
	 6.If FB connect YUVSCALE, should be no less than YUVSCALE clock.
	 7.OSD use D1 clock, too high OSD clock might cause VIF MBL(VE) overflow.
	 */ 
    //Open D1->D4 clock
	u8 d1_div = 5;
    WriteReg32(SC_BASE_ADDR+0x4c,(ReadReg32(SC_BASE_ADDR+0x4c)&0x0003ffff)|BIT23|BIT31|d1_div<<18|(d1_div*4)<<24);//d1_clk div, d4div=d1_div*4
    //Open YUVSCALE clock
	int yuv_div = 10;
#ifdef ECIF_120MHZ
    WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)&0xffffffe0) | BIT5|5);//yuvscale clk_div=ecif_div*2
#else
    WriteReg32(SC_BASE_ADDR+0x54,(ReadReg32(SC_BASE_ADDR+0x54)&0xffffffe0) | BIT5| yuv_div);//yuvscale clk_div=ecif_div*2
#endif
	
	//sif0/sif1/sif2(ddr_pll_ext is 99M)
	clocksif0_cfg(1, 1);
	clocksif1_cfg(1, 1);
	clocksif2_cfg(1, 1);

//	WriteReg32(REG_SC_DIV2, (ReadReg32(REG_SC_DIV2)&(~0x1f))|3);//scif div set to 3

#ifdef LOWPOWER
	WriteReg32(REG_SC_ADDR+0x40, ReadReg32(REG_SC_ADDR+0x40)&(~BIT24));
	WriteReg32(REG_SC_ADDR+0x4c, ReadReg32(REG_SC_ADDR+0x4c)&(~BIT5)&~(0xff<<10)|(10<<10));	//i2s divider 10,for tmp,for low speed form 360->240M,i2s module clk in was fixed to 24M.
	WriteReg32(REG_SC_ADDR+0x48, ReadReg32(REG_SC_ADDR+0x48)&(~(BIT5)));//disable SD
	WriteReg32(REG_SC_ADDR+0x54, ReadReg32(REG_SC_ADDR+0x54)&(~BIT19));//DISABLE JPEG
//	WriteReg32(REG_SC_ADDR+0x54, ReadReg32(REG_SC_ADDR+0x54)&(~BIT11));//DISABLE SPI1
	WriteReg32(REG_SC_ADDR+0x58, ReadReg32(REG_SC_ADDR+0x58)&(~BIT21));
	WriteReg32(REG_SC_ADDR+0x5c, ReadReg32(REG_SC_ADDR+0x5c)&(~(BIT21|BIT31)));
	WriteReg32(REG_SC_ADDR+0xc0, (ReadReg32(REG_SC_ADDR+0xc0)|BIT12)&(~BIT13)); //close pll of usb_host_phy 
#endif


	//set i2s clk to 12MHz
	u32 sysclk = clockget_ls();
	u32 i2s_div = sysclk/12000000;
	WriteReg32(REG_SC_DIV3, (ReadReg32(REG_SC_DIV3)&~(0xff<<10))|BIT8|BIT9|(i2s_div<<10));	
}
