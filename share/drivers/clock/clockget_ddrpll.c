#include "includes.h"

unsigned int clockget_ddr(void)
{
	return clockget_pll_sys(REG_SC_ADDR + 0x120) / 2;
}

unsigned int clockget_ddrpll_ext(void)
{
	return clockget_pll_ext(REG_SC_ADDR + 0x120);
}
