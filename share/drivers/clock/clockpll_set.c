#include "includes.h"

#define PLL_NEW
#define TYPE_SYSPLL    (1)
#define TYPE_DDRPLL    (2)
#define TYPE_MIPITXPLL (3)
static u16 pll_lpf_tab[9] = 
{
	25, 
	36,
	51,
	72,
	101,
	143,
	203,
	288,
	407,
};

static u16 mipipll_lpf_tab[9] = 
{
	12, 
	18,
	27,
	41,
	61,
	90,
	135,
	202,
	301,
};

static u32 clocklpf_get(u8 type, int prediv, int divsys, int pdiv, int sdiv, int frac_en, int dsm)
{
	u32 div = 0;
	u32 lpf = 0;
	u32 prediv_v = 0;
	switch(prediv){
		case 0: prediv_v = 24000000/1; break;
		case 1: prediv_v = 16000000; break;
		case 2: prediv_v = 24000000/2; break;
		case 3: prediv_v = 9600000; break;
		case 4: prediv_v = 24000000/3; break;
		case 5: prediv_v = 24000000/4; break;
		case 6: prediv_v = 24000000/6; break;
		case 7: prediv_v = 24000000/8; break;
		default:prediv_v = 24000000; break;
	}
	if(frac_en){
		div = (2 * pdiv + sdiv) + (((prediv_v/5861)* dsm) >> 20)*5861;
	}else{
		div = 2 * pdiv + sdiv;
	}
	int n;
	if(type == TYPE_SYSPLL || type == TYPE_DDRPLL){
		for(n=0; n<9; n++){
			if(div >= pll_lpf_tab[n] && div < pll_lpf_tab[n+1]){
				lpf = n;
				break;
			}
		}
	}else if(type == TYPE_MIPITXPLL){
		for(n=0; n<9; n++){
			if(div >= mipipll_lpf_tab[n] && div < mipipll_lpf_tab[n+1]){
				lpf = n;
				break;
			}
		}
	}
	return lpf;
}

//SYS PLL setting
//VCO clock = refclk/prediv *(2*pdiv+sdiv)    //[intn_en = 1, r_sscg_en = 0]  Integer-N mode 
//VCO clock = refclk/prediv *(2*pdiv+sdiv+dsm/2^20)    //[intn_en = 0, r_sscg_en = 0] Frac-N Mode 
//prediv = 0/1/2/3/4/5/6/7 the divider is 1/1.5/2/2.5/3/4/6/8
//divsys/divisp = 0/1/2/3/4/5/6/7/8/9/10/11/12/13/14/15 is 1/1/2/3/4/5/6/7/8/9/10/1.5/1.5/1.5/2.5/2.5
//pllsys_clk_o = VCO/(divsys)
//pllisp_clk_o = VCO/divisp

void r3_syspll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm)
{
	volatile int sr;
#ifdef PLL_NEW
	u16 lpf = clocklpf_get(TYPE_SYSPLL, prediv, divsys, pdiv, sdiv, frac_en, dsm);
	sr = ReadReg32(SC_BASE_ADDR+0x68);
	WriteReg32(SC_BASE_ADDR+0x68, ((sr&0xc0f00000)|(0x8000000) | (lpf<<24)|(sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	if(frac_en){
		sr = ReadReg32(SC_BASE_ADDR+0x6c);
		WriteReg32(SC_BASE_ADDR+0x6c, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
	}else{
		WriteReg32(SC_BASE_ADDR+0x6c, 0); 
	}
#else
	sr = ReadReg32(SC_BASE_ADDR+0x68);
	WriteReg32(SC_BASE_ADDR+0x68, ((sr&0xfff00000) | (sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	sr = ReadReg32(SC_BASE_ADDR+0x6c);
	WriteReg32(SC_BASE_ADDR+0x6c, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
#endif
}

void r3_ddrpll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm)
{
	volatile int sr;
#ifdef PLL_NEW
	u16 lpf = clocklpf_get(TYPE_DDRPLL, prediv, divsys, pdiv, sdiv, frac_en, dsm);
	sr = ReadReg32(SC_BASE_ADDR+0x120);
	WriteReg32(SC_BASE_ADDR+0x120, ((sr&0xc0f00000)|(0x8000000) | (lpf<<24)|(sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	if(frac_en){
		sr = ReadReg32(SC_BASE_ADDR+0x124);
		WriteReg32(SC_BASE_ADDR+0x124, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
	}else{
		WriteReg32(SC_BASE_ADDR+0x124, 0); 
	}
#else
	sr = ReadReg32(SC_BASE_ADDR+0x120);
	WriteReg32(SC_BASE_ADDR+0x120, ((sr&0xfff00000) | (sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	sr = ReadReg32(SC_BASE_ADDR+0x124);
	WriteReg32(SC_BASE_ADDR+0x124, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
#endif
}

void r3_mipitxpll_set(int prediv, int divsys,int divisp, int pdiv,int sdiv,int bypass, int frac_en,int sscg_en,int dsm)
{
	volatile int sr;
#ifdef PLL_NEW
	u16 lpf = clocklpf_get(TYPE_MIPITXPLL, prediv, divsys, pdiv, sdiv, frac_en, dsm);
	sr = ReadReg32(SC_BASE_ADDR+0x110);
	WriteReg32(SC_BASE_ADDR+0x110, ((sr&0xc0f00000)|(0x8000000) | (lpf<<24)|(sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	if(frac_en){
		sr = ReadReg32(SC_BASE_ADDR+0x114);
		WriteReg32(SC_BASE_ADDR+0x114, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
	}else{
		WriteReg32(SC_BASE_ADDR+0x114, 0); 
	}
#else
	sr = ReadReg32(SC_BASE_ADDR+0x110);
	WriteReg32(SC_BASE_ADDR+0x110, ((sr&0xfff00000) | (sdiv << 19) | (prediv << 16) | (divisp << 12) | (divsys << 8) | pdiv )); 
	sr = ReadReg32(SC_BASE_ADDR+0x114);
	WriteReg32(SC_BASE_ADDR+0x114, ((sr&0x3b700000) | (bypass << 30) | (frac_en << 26) | (sscg_en << 23) | dsm )); 
#endif
}
