#include "includes.h"

#define UART_TXFIFO_THRE	(8)

void uart_config(u32 baud_rate, u32 irq_en)
{
	u32 inclk = clockget_bus();

	SC_CLK0_EN(UARTM);
	SC_RESET0_SET(UARTM);
	SC_RRESET0_SET(UARTM);

	if((inclk <= 12000000) || ((inclk % (16*baud_rate))*100 > 5*inclk)) {
		int div_val = inclk/baud_rate;
		div_val = (div_val+31)/32;
		uart_init(div_val, inclk/(div_val*baud_rate), irq_en);
	}else {
		uart_init(inclk, baud_rate, irq_en);
	}
}

void uart1_config(u32 baud_rate, u32 irq_en)
{
	u32 inclk = clockget_bus();

	SC_CLK0_EN(UARTS);
	SC_RESET0_SET(UARTS);
	SC_RRESET0_SET(UARTS);
	
	if((inclk <= 12000000) || ((inclk % (16*baud_rate))*100 > 5*inclk)) {
		int div_val = inclk/baud_rate;
		div_val = (div_val+31)/32;
		uarts_init(div_val, inclk/(div_val*baud_rate), irq_en);
	}else {
		uarts_init(inclk, baud_rate, irq_en);
	}
}

void lowlevel_set(int uartid);
void lowlevel_config(u32 baud_rate, u32 irq_en)
{
#ifdef CONFIG_CONSOLE_UART1
	uart1_config(baud_rate, irq_en);
	lowlevel_set(1);
#else
	uart_config(baud_rate, irq_en);
	lowlevel_set(0);
#endif
}

void lowlevel_forceputc(unsigned char c){
#ifdef CONFIG_CONSOLE_UART1
	uarts_forceputc(c);
#else
	uart_forceputc(c);
#endif
}

#define GET_A_CHAR(uart_base_address) \
		(ReadReg32(uart_base_address + UART_LSR) & UART_LSR_DR)
int uarts_getc_nob(unsigned char *pch)
{
	if (GET_A_CHAR(UART_BASE_S)) {
		*pch = ReadReg32(UART_BASE_S + UART_RX);
		return 0;
	} else {
		return -1;
	}
}

int lowlevel_getc_nob(unsigned char *pch)
{
#ifdef CONFIG_CONSOLE_UART1
	return uarts_getc_nob(pch);
#else
	return uart_getc_nob(pch);
#endif
}

int lowlevel_tx_interrupt_handler(void)
{
#ifdef CONFIG_CONSOLE_UART1
	return uart_tx_interrupt_handler(1);
#else
	return uart_tx_interrupt_handler(0);
#endif
}

int xmodemReceive(unsigned char *xbuff, unsigned char *dest, int destsz);
int load_from_xmodem(u32 baseaddr)
{
	unsigned char xbuff[1030];

	t_irqaction irqback;
	int backirq = 0;
	if(g_ertos == NULL){
		return -1;
	}
#ifdef CONFIG_CONSOLE_UART1
#define IRQ_BIT_UARTX	IRQ_BIT_UART1
#else
#define IRQ_BIT_UARTX	IRQ_BIT_UART0
#endif
	if(g_ertos->irqactions[IRQ_BIT_UARTX].handler != NULL){
		memcpy(&irqback, &g_ertos->irqactions[IRQ_BIT_UARTX], sizeof(t_irqaction));
		irq_free(IRQ_BIT_UARTX);
		backirq = 1;
	}

	int ret = xmodemReceive(xbuff, (u8 *)baseaddr, 0x10000000);

	if(backirq){
		irq_request(IRQ_BIT_UARTX, irqback.handler, irqback.name, irqback.arg);
	}

	if(ret < 0) return 0;
	return ret;
}

int uart_debug_init(void * arg, t_debugprintf * dpcfg, void ** hw_arg)
{

	t_uartdebug * ud = (t_uartdebug *)malloc(sizeof(t_uartdebug));
	if(ud == NULL){
		syslog(LOG_ERR, "ERR: uart debug malloc fail\n");
		return -1;
	}
	memset(ud, 0, sizeof(t_uartdebug));

	u32 cfg = (u32)(arg);
	if((cfg & BIT0) == 0){
		ud->uart_base = UART_BASE_M;
		ud_uart0 = ud;
	}else{
		ud->uart_base = UART_BASE_S;
		ud_uart1 = ud;
	}
	ud->dp = dpcfg;
	if(cfg & BIT1){
		ud->txirqmode = 1;
		//FIXME: This is a temp fix for dpcfg->hw cause crash issue in debug_printf_set();
		if(dpcfg->hw != NULL){
			dpcfg->hw->non_direct_notify_bytes = UART_TXFIFO_THRE;
		}
	}else{
		ud->txirqmode = 0;
		if(dpcfg->hw != NULL){
			dpcfg->hw->non_direct_notify_bytes = 1;
		}
	}

	*hw_arg = (void *)ud;
	
	if((dpcfg->flag & DEBUGPRINTF_MODE_VALID_VAL) == DEBUG_PRINTF_MODE_DIRECT){
		return 0;
	}

	//non-direct mode, create thread.
	ud->exit_event = ertos_event_create();
	if(ud->exit_event == NULL){
		syslog(LOG_ERR, "[ERROR] create uart debug exit_event error\n");
		free(ud);
		return -1;
	}

	pthread_attr_t thread_attr;
	pthread_attr_init(&thread_attr);
	if(((cfg >> 8) & 0xff) != 0){
		thread_attr.schedparam.sched_priority = ((cfg >> 8) & 0xff); //prio
	}
	extern void * uartdebug_main(void * arg);
	int ret = pthread_create(&ud->threadid, &thread_attr, uartdebug_main, ud);
	if(ret != 0){
		syslog(LOG_ERR, "[ERROR] create uart debug thread error\n");
		ertos_event_delete(ud->exit_event);
		free(ud);
		return -1;
	}
	return 0;
}
