#include "includes.h"


t_scif_drv sh_scif_drv;
static t_scif_info sh_scif_info;

static int app_scif_irq_handler(void * arg)
{
	if(sh_scif_drv.intr_en){
		scif_irq_handler(&sh_scif_drv);
	}
	return 0;
}
/*
 * detect sd card
 */
static int sd_detected(void){
	return libgpio_read(PIN_GPIO_08);
}

/*
 * modify by zxf, add the auto detect the dll_delay
 */
static u32 median_delay(int module,t_scif_drv *p_scif_drv){
	u32 min_delay = 0xff;
	u32 max_delay = 0x0;
	u32 var = 5;
    u32 tmp;
    u32 ret;
	while(1){
		p_scif_drv->dll_delay = var;
		ret = scif_init(module, clockget_scifsys(), p_scif_drv,	24000000);
		if(ret > 10){
			if( (var - tmp) == 8){
				min_delay = tmp;
				break;
			}
			var++;
		}else{
			var++;
			tmp = var;
			if(tmp > 30){
				min_delay = tmp;
				return min_delay;
			}
		}
	}
	while(1){
		p_scif_drv->dll_delay = var;
		ret = scif_init(module, clockget_scifsys(), p_scif_drv,	24000000);
		if(ret < 10){
			var = var -1;
			max_delay = var;
			break;
		}else{
			if(var > 40){
				max_delay = 40;
				break;
			}
		}
		var++;
	}
 	return (min_delay + max_delay)/2;
}

u32 libscif_init(u8 module)
{
	int ret = 0;

	memset(&sh_scif_drv, 0, sizeof(sh_scif_drv));
	scif_module_config(&(sh_scif_drv), 0);	
	sh_scif_drv.scif_info = &sh_scif_info;	
	//sh_scif_drv.timeout = 0x1000000;
	sh_scif_drv.timeout = 0x10000;
	sh_scif_drv.dma_flag = SCIF_NONE_DMA;
    sh_scif_drv.cmd_chk_num = 1000;
    //default for RD15
    //sh_scif_drv.dll_delay = 0x22;
    //default for RD 18
    //sh_scif_drv.dll_delay = 30;

	sh_scif_drv.intr_en = 1;
	/****add by zxf****/
	sh_scif_drv.dll_delay = median_delay(module, &sh_scif_drv);
	sh_scif_drv.timeout = 0x1000000;
	/******/
	if(module == SCIF_MODULE_01){
		ret = scif_init(module, clockget_scifsys(), &sh_scif_drv,24000000);
	}else{
		ret = scif_init(module, clockget_sciosys(), &sh_scif_drv,24000000);
	}
	if(ret > 10){
		if(sh_scif_drv.intr_en){
			int retval = 0;
			if(module == SCIF_MODULE_01){ 
				retval = irq_request(IRQ_BIT_SCIF1, app_scif_irq_handler, "SCIF1", NULL);
			}else{
				retval = irq_request(IRQ_BIT_SCIF2, app_scif_irq_handler, "SCIF2", NULL);
			}
			if(retval < 0){
				ret = retval;
			}
		}
	}
	if(module == SCIF_MODULE_01){ 
		card_clk_switch(clockget_scifsys(), &sh_scif_drv, &sh_scif_info, 24000000); ///< FIXME: Now run 12M on Xilinux A5
	}else{
		card_clk_switch(clockget_sciosys(), &sh_scif_drv, &sh_scif_info, 24000000); ///< FIXME: Now run 12M on Xilinux A5
	}
	return ret;
}

