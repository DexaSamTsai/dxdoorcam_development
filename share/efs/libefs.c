#include "includes.h"

t_efs sh_efs;
static t_efs_iobuf sh_iombuf[CONFIG_LIBEFS_IOMBUF_CNT];

int efs_init(t_efs_hw * efs_hw, void * arg, u32 buf, u32 buf_size)
{
	memset(&sh_efs, 0, sizeof(sh_efs));
	sh_efs.iobuf = &sh_iombuf[0];
	sh_efs.iobuf_cnt = CONFIG_LIBEFS_IOMBUF_CNT;
	sh_efs.efs_hw = efs_hw;
	sh_efs.ncf_buf_addr = buf;
	sh_efs.ncf_max_sec_cnt = (buf_size/512);

	sh_efs.task_param.prio = CONFIG_LIBEFS_TASK_PRIO;
	sh_efs.task_param.queue_cnt = 10;

	if(efs_hw->type == EFS_HW_TYPE_SCIF){
		arg = &sh_scif_drv;
	}else{
		///< TODO:nand efs initialization	
	}
	if(efs_init_internal(&sh_efs,arg)!=0){
		debug_printf_share("error sh_efs_init\n");
		efs_exit(&sh_efs);
		return -1;
	}
	return 0;
}

