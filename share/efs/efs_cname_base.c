#include "includes.h"

char * file_ch_fatname(t_efs * efs, char * filename, char * fatname, u16 * flen)
{
	u8 c=0, dot=0, vc=0, tmpdot=0;
	u8 lflag=0;
	u16 r=0;
	u32 cnt=0, utf8_cnt=0;
	int len=0;

	efs->lf.cname = &efs->lf.lname[0];
	efs->lf.is_cfname = 0;

	memset(efs->lf.cname, 0, sizeof(efs->lf.lname));
	
	char * tmp = (char *)filename;
	while(tmp[cnt] != '\0' && tmp[cnt] != '/'){
		if(tmp[cnt] == '.'){
			tmpdot ++;
		}
		r=efs_utf8_to_unicode(efs, &tmp[cnt], &len);	
		if(!r){
			break;
		}
		efs->lf.cname[c++]=r;
		cnt+=len;
		utf8_cnt+=len;
		if(len > 1){
			efs->lf.is_cfname = 1;
		}
	}

	if(!efs->lf.is_cfname){
		return NULL;	
	}

	cnt=0;
	c=0;
	short * utmp = (short *)efs->lf.cname;
	while(utmp[cnt] != '\0' && utmp[cnt] != '/'){
		if(utmp[cnt]==' '){
			lflag = 1;
			c++;
		}else if(utmp[cnt]=='.'){
			dot++;
			if(tmpdot > 1){
				lflag = 1;
				if(tmpdot != dot){
					if(c<=7){
						fatname[c] = file_fatname_char((utmp[cnt]&0xff));
					}
					c++;
				}
				if((tmpdot==1)&&(!lflag)) c=8;
			}
		}else{
			if(dot){
				if(c<=10){
					if((utmp[cnt]&0xff00)){
						c++;
						lflag = 1;
					}
				}
				if(c>10) lflag=1;
				c++;
			}else{
				if(c<=7){
					if((utmp[cnt]&0xFF00)){
						c++;
						lflag = 1;
					}
				}
				if(c>7) lflag=1;
				c++; vc++;
			}
		}
		cnt++;
	}
	
	if(lflag){
		*flen=cnt;
	}

	if(filename[utf8_cnt]=='\0'){
		return(filename+utf8_cnt);
	}else{
		return(filename+utf8_cnt+1);
	}
}

u32 dir_ch_l2s_algo(t_efs * efs, short * lname, u8 flen)
{
	if(efs->cb_dir_ch_l2s_algo){
		return efs->cb_dir_ch_l2s_algo(efs, lname, flen);
	}
	
	int i;
	u32 lnum = 0;
	for(i=0; i<flen; i++){
		lnum += lname[i];			
	}
	char tmpname[7];
	memset(tmpname, 0, sizeof(tmpname));
	strncpy(tmpname, (char *)lname, 7);
	lnum += atoi(tmpname);
	lnum = dir_rand(lnum);		
	return lnum;
}

void dir_ch_lname2sname(t_efs * efs, short * lname, char * sname){
	u8 c=0,vc,tmp_len,dot=0;
	u8 finddot=0;
	u8 lname_len=0;
	u8 isfile = 0;
	u8 chcnt = 0;
	u8 sname_head[2];
	u8 postfix_len = 0;
	u32 gbk_code=0;
	memset(sname_head, 0, sizeof(sname_head));

	while(lname[c++]){
		lname_len++;
	}
	tmp_len = lname_len;

	for(c=lname_len-1; c>0; c--){
		if(lname[c]=='.'){
			dot = 1;
			isfile = 1;
			postfix_len = lname_len-c-1;
			break;
		}
	}
	if(dot == 1){
		for(vc=0; vc<c; vc++){
			if(lname[vc] != '.'){
				if((lname[vc]&0xff00)){
					gbk_code = efs_unicode_to_gbk(efs, lname[vc]);
					sname_head[chcnt++] = (gbk_code>>8)&0xff;
					sname_head[chcnt++] = (gbk_code&0xff);
				}else{
					sname_head[chcnt++] = (lname[vc]&0xff);
				}	
				if(chcnt > 2){
					break;
				}
			}
		}
	}else{
		for(vc=0; vc<lname_len-1; vc++){
			if((lname[vc]&0xff00)){
				gbk_code = efs_unicode_to_gbk(efs, lname[vc]);
				sname_head[chcnt++] = (gbk_code>>8)&0xff;
				sname_head[chcnt++] = (gbk_code&0xff);
			}else{
				sname_head[chcnt++] = (lname[vc]&0xff);
			}	
			if(chcnt > 2){
				break;
			}
		}
	}
	
	if(isfile==0)         vc=7;
	else               vc=11;
	c=0;
	if(postfix_len > 3){
		lname_len -= (postfix_len-3);
	}
	while(c<=vc){
		if(lname[lname_len-1] == '.' && finddot == 0){
			sname[vc-c]=(lname[lname_len-1]&0xff);
			c=4;
			finddot = 1;
			lname_len--;
		}else{
			if(dot){
				if(c<3){
					sname[vc-c]=file_fatname_char((lname[lname_len-1]&0xff));
					c++;
					lname_len--;
					continue;
				}
			}
			if(c<=vc){
				if(chcnt > 1){
					if(c==(vc-7)) sname[vc-c]='1';
					else if(c==(vc-6)) sname[vc-c]='~';
					else if(c==vc) sname[vc-c]=sname_head[0];
					else if(c==vc-1) sname[vc-c]=sname_head[1];
					else{
						u32	lnum = dir_ch_l2s_algo(efs,lname, tmp_len);
						dir_dec2hex(lnum, &sname[2]);
						c+=3;
					}
				}else{
					if(c==(vc-6)) sname[vc-c]='1';
					else if(c==(vc-5)) sname[vc-c]='~';
					else if(c==vc) sname[vc-c]=sname_head[0];
					else{
						u32	lnum = dir_ch_l2s_algo(efs,lname, tmp_len);
						dir_dec2hex(lnum, &sname[1]);
						c+=3;
					}
				}
				c++;
			}
		}
	}
	return ;

}

