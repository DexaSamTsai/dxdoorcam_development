#include "includes.h"

int efs_hw_scif_init(struct s_efs * efs, void * hw_arg)
{
	int ret = 0;
	t_scif_drv *scif_drv = (t_scif_drv *) hw_arg;

	ret = app_scif_init();

	if(ret > 10){
		efs->sector_cnt = scif_drv->scif_info->size;
		efs->hw_arg = hw_arg;
		ret = 0;
	}
	return ret;
}

int efs_hw_scif_read(struct s_efs * efs, u32 address, u8 * buf, int count)
{
	int x = 0;
    x = scif_read(efs->hw_arg, address, (u32)buf, 512, count);
	return x;
}

int efs_hw_scif_write(struct s_efs * efs, u32 address, u8 * buf, int count)
{
	int x = 0;
    x = scif_write(efs->hw_arg, (u32)buf, address, 512*count);
	return x;
}

int efs_hw_scif_exit(struct s_efs * efs){
	t_scif_drv *pscif_drv = (t_scif_drv *) (efs->hw_arg);
	if(pscif_drv->ctrl_base_addr == SCIF1_BASE_ADDR){
		irq_free(IRQ_BIT_SCIF1);
		SC_RESET0_SET(SCIF1);
		SC_RRESET1_SET(SCIF1);
	}else if(pscif_drv->ctrl_base_addr == SCIF2_BASE_ADDR){
		irq_free(IRQ_BIT_SCIF2);
		SC_RESET0_SET(SCIF2);
		SC_RRESET1_SET(SCIF2);
	}
	return 0;
}

t_efs_hw g_efs_hw_scif = {
	.type = EFS_HW_TYPE_SCIF,
	.init = efs_hw_scif_init,
	.read = efs_hw_scif_read,
	.write = efs_hw_scif_write,
	.exit = efs_hw_scif_exit,
}; 
