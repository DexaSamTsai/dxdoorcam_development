#include "includes.h"

extern u32 MS_BlkCount;
int efs_hw_uhost_init(struct s_efs * efs, void * hw_arg)
{
	efs->sector_cnt = MS_BlkCount;
	debug_printf("usb storage block num: %x\n", MS_BlkCount);

	return 0;
}

int efs_hw_uhost_read(struct s_efs *efs, u32 address, u8* buf, int count)
{
	int ret = 0;

	ret = MS_bulk_read(address, count, (char *)buf);
	return ret;
}

int efs_hw_uhost_write(struct s_efs *efs, u32 address, u8* buf, int count)
{
	int ret = 0;

	ret = MS_bulk_write(address, count,(char *)buf);
	return ret;
}

int efs_hw_uhost_exit(struct s_efs * efs)
{
	return 0;
}

t_efs_hw g_efs_hw_uhost = {
	.type = EFS_HW_TYPE_UHOST,
	.init = efs_hw_uhost_init,
	.read = efs_hw_uhost_read,
	.write = efs_hw_uhost_write,
	.exit = efs_hw_uhost_exit,
}; 
