/*!
 *  @file      	Project.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __PROJECT_H__
# define __PROJECT_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================

//==========================================================================
// Type Define
//==========================================================================
# define ERTOS_MEM_LIVE_START				CONFIG_FREE_MEM_1_START
# define ERTOS_MEM_LIVE_END					CONFIG_FREE_MEM_1_END
# define ERTOS_MEM_LIVE_SIZE				( ERTOS_MEM_LIVE_END - ERTOS_MEM_LIVE_START )

# define ERTOS_MEM_RECORD_START				CONFIG_FREE_MEM_2_START
# define ERTOS_MEM_RECORD_END				CONFIG_FREE_MEM_2_END
# define ERTOS_MEM_RECORD_SIZE				( ERTOS_MEM_RECORD_END - ERTOS_MEM_RECORD_START )

# define ERTOS_MEM_SNAPSHOT_START			CONFIG_FREE_MEM_3_START
# define ERTOS_MEM_SNAPSHOT_END				CONFIG_FREE_MEM_3_END
# define ERTOS_MEM_SNAPSHOT_SIZE			( ERTOS_MEM_SNAPSHOT_END - ERTOS_MEM_SNAPSHOT_START )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// APIs
//==========================================================================

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// __PROJECT_H__
