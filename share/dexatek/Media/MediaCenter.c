/*
 * MediaCenter.c
 *
 *  Created on: 2017/08/09
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

# include "Camera.h"
# include "DataManagement/DC.h"
# include "DataManagement/Record.h"
# include "DataManagement/Snapshot.h"
# include "Stream/RTMP.h"
# include "Format/H264.h"
# include "MediaCenter.h"

# include "openssl/bio.h"
# include "openssl/ssl.h"
# include "openssl/err.h"
# include "openssl/pem.h"
# include "openssl/x509.h"
# include "openssl/x509v3.h"
# include "openssl/x509_vfy.h"

//==========================================================================
// Type Define
//==========================================================================
# define MC_PRINT_INFO_INTERVAL		3
# define MC_PING_INTERVAL			30
# define MC_TIMEOUT					10
# define MC_VIDEO_REAL_TIME_DELAY_MSEC	1000
# define MC_AUDIO_REAL_TIME_DELAY_MSEC	2000

enum {
	MC_CMD_NONE		= 0,
	MC_CMD_START,
	MC_CMD_STOP,
	MC_CMD_END,
};

enum {
	MC_STATUS_ERROR		= -1,
	MC_STATUS_IDLE		= 0,
	MC_STATUS_WORKING,
	MC_STATUS_READY,
};

typedef struct {
    int         	fd;
    SSL_CTX			*ctx;
    SSL     		*ssl;
	X509			*cert;
} ssl_ctx_t;

typedef struct {
	int					sock;

	ssl_ctx_t			*ssl;

	char				*proto;
	char				*addr;
	char				*uri;

	u32					file_size;
} mc_upload_ctx;

typedef struct {
	BOOL drop;							//TRUE: drop frame, FALSE: keep frame
	BOOL wait;							//FALSE: can turn drop to FALSE,  TRUE: can not turn drop to FALSE
	u32	delay_time;
	u32 timestamp;
} mc_stream_status;

typedef struct {
	RTMPHandle		rtmp;
	u8				cmd;
	int				status;
	char			*shm_name;
	char			*host_name;
	u16				port;
	char			*title;
	char			*url;
	char			*username;
	char			*password;
	mc_stream_status	video;
	mc_stream_status	audio;
	pthread_mutex_t	mutex;
} mc_stream_ctx;

typedef struct {
	u8						cmd;
	int						status;

	MediaCenter_RecordCTX	ctx;

	pthread_mutex_t			mutex;
} mc_recording_ctx;

typedef struct {
	u8						cmd;
	int						status;

	u32						file_size;
	u32						crc32;

	MediaCenter_UploadCTX	ctx;

	pthread_mutex_t			mutex;
} mc_upload_file_ctx;

typedef struct {
	u8			is_player;
	u8			is_audio_init;
	DC_READER	dc;
} stream_ctx_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static u32	max_stream_out_cnt	= 0;

static mc_stream_ctx		*stream_outs	= NULL;
static mc_stream_ctx		stream_in		= { 0, };
static mc_upload_file_ctx	upload_file 	= { 0, };
static mc_recording_ctx		mc_ctx[ MC_UPLOAD_TYPE_MAX ];

static struct {
	u32		video_size;
	u32		video_frame_cnt;

	u32		audio_size;
	u32		audio_frame_cnt;

	time_t	last_update;
	time_t	last_ping;
	time_t	last_ping_ack;

	BOOL	delay;
	BOOL	error;
} stream_upload_info;

//==========================================================================
// Static Functions
//==========================================================================
static int
socket_get_ipaddr( char *addr, char *ipstr, int type )
{
	struct addrinfo hints, *servinfo, *p;
	int status, res = FAILURE;

	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family		= type; // AF_INET ���雓�鞊莎�揭���雓� AF_INET6 ���雓�嚙踝蕭謅��嚙踝嚙踐嚙踝蕭謕蕭��嚙踝蕭��蕭謅��雓�鞊船謆血��雓�嚙踝蕭��嚙踐狐���蕭謖鞊船�����蕭嚙踐��
	hints.ai_socktype	= SOCK_STREAM;

	if ( ( status = getaddrinfo( addr, NULL, &hints, &servinfo ) ) != 0 ) {
		//dbg_line( "%s: %s", addr, gai_strerror( status ) );
		return res;
	}

	for ( p = servinfo; p != NULL; p = p->ai_next ) {
		if ( p->ai_family == type ) {
			if ( p->ai_family == AF_INET ) { // IPv4
				struct sockaddr_in *ipv4 = ( struct sockaddr_in * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv4->sin_addr, ipstr, INET_ADDRSTRLEN );
			} else { // IPv6
# ifdef INET6_ADDRSTRLEN
				struct sockaddr_in6 *ipv6 = ( struct sockaddr_in6 * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv6->sin6_addr, ipstr, INET6_ADDRSTRLEN );
# else
				dbg_error( "Unsupport IPV6!!!" );
# endif
			}
			res = SUCCESS;
			break;
		}
	}

	freeaddrinfo( servinfo );

	return res;
}

static int
sock_conn( char *addr, u16 port, int block )
{
	int fd = 0;
	int on = 1;
	struct sockaddr_in sockaddr;
	socklen_t socklen = sizeof( struct sockaddr_in );
# ifdef INET6_ADDRSTRLEN
	char ipstr[ INET6_ADDRSTRLEN ];
# else
	char ipstr[ INET_ADDRSTRLEN ];
# endif

	if ( socket_get_ipaddr( addr, ipstr, AF_INET ) == FAILURE ) {
		dbg_warm( "Do socket_get_ipaddr Failed!!!" );
		return FAILURE;
	}

	fd = socket( PF_INET, SOCK_STREAM, 0 );

	if ( fd < 0 ) {
		//dbg_line( "%s", strerror( errno ) );
		return FAILURE;
	}

	if ( block == 0 ) {
		fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) );
	} else {
		struct timeval interval;

		interval.tv_sec		= 5;
		interval.tv_usec	= 0;

		setsockopt( fd, SOL_SOCKET, SO_RCVTIMEO, ( struct timeval * )&interval, sizeof( struct timeval ) );
	}

	setsockopt( fd, SOL_SOCKET, SO_KEEPALIVE, &on, sizeof( on ) );
	setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof( on ) );
	setsockopt( fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof( on ) );

	bzero( &sockaddr, socklen );

	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_port			= htons( port );
	sockaddr.sin_addr.s_addr	= inet_addr( ipstr );

	if ( sockaddr.sin_addr.s_addr == -1 ) {
		//dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	}

	connect( fd, ( struct sockaddr * )&sockaddr, socklen );

	if ( getsockname( fd, ( struct sockaddr * )&sockaddr, &socklen ) < 0 ) {
		//dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	}

	return fd;
}

static int
sock_readable( int fd )
{
	fd_set				fds;
	struct timeval		to;

	FD_ZERO( &fds );
	FD_SET( fd, &fds );

	to.tv_sec	= 30;
	to.tv_usec	= 0;

	return select( ( fd + 1 ), &fds, NULL, NULL, &to );
}

static int
sock_writable( int fd )
{
	fd_set				w_fds;
	fd_set				e_fds;
	struct timeval		to;

	FD_ZERO( &w_fds );
	FD_ZERO( &e_fds );
	FD_SET( fd, &w_fds );
	FD_SET( fd, &e_fds );

	to.tv_sec	= 5;
	to.tv_usec	= 0;

	if ( select( ( fd + 1 ), NULL, &w_fds, &e_fds, &to ) != 0 ) {
		if ( FD_ISSET( fd, &e_fds ) ) {
			return -1;
		} else if ( FD_ISSET( fd, &w_fds ) ) {
			return 1;
		} else {
			return -1;
		}
	} else {
		return 0;
	}
}

static int
sock_send( int fd, void *data, int len )
{
	int res, size = 0;

	if ( ( fd < 0 ) || ( data == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd < 0 ) || ( data == NULL ) || ( len <= 0 )" );
		return FAILURE;
	} else {
		u8 *src	= ( u8 * )data;

		while ( size < len ) {
			res = sock_writable( fd );
			if ( res < 0 ) {
				dbg_line( "sock_writable( fd ) < 0" );
				return FAILURE;
			} else if ( res == 0 ) {
				dbg_line( "sock_writable( fd ) = 0" );
				return FAILURE;
			}

			res = write( fd, &src[ size ], len - size );
			//res = send( fd, &src[ size ], len - size, MSG_NOSIGNAL );

			if ( res < 0 ) {
				if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
					//dbg_line( "errno: %s", strerror( errno ) );
					return FAILURE;
				}
			} else {
				size += res;
			}
		}

		return size;
	}
}

static int
sock_recv( int fd, void *data, int len )
{
	if ( ( fd <= 0 ) || ( data == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd <= 0 ) || ( data == NULL ) || ( len <= 0 )" );
		return FAILURE;
	} else {
		int size = 0;
		u8 *dst	= ( u8 * )data;

		while ( size < len ) {
			if ( sock_readable( fd ) > 0 ) {
				int res = recv( fd, &dst[ size ], len - size, MSG_WAITALL );

				if ( res < 0 ) {
					if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
						//dbg_line( "%s", strerror( errno ) );
						return res;
					} else {
						dbg_line( "res: %d", res );
					}
				} else if ( res == 0 ) {
					break;
				} else {
					size += res;
				}
			} else {
				break;
			}
		}

		return size;
	}
}

static void
ssl_close( ssl_ctx_t *ctx )
{
	if ( ctx->cert ) {
		X509_free( ctx->cert );
		ctx->cert	= NULL;
	}

	if ( ctx->ssl ) {
        SSL_shutdown( ctx->ssl );
        SSL_free( ctx->ssl );
		ctx->ssl	= NULL;
	}

	if ( ctx->ctx ) {
        SSL_CTX_free( ctx->ctx );
		ctx->ctx	= NULL;
	}
}

static int
ssl_recv( ssl_ctx_t *ctx, void *data, size_t size )
{
    ERR_clear_error();

	fcntl( ctx->fd, F_SETFL, ( O_NONBLOCK | fcntl( ctx->fd, F_GETFL, 0 ) ) );

	if ( sock_readable( ctx->fd ) ) {
		int res	= SSL_read( ctx->ssl, data, size );

		if ( res <= 0 ) {
			dbg_error( "Do SSL_read Failed!!!" );
			return FAILURE;
		} else {
			return res;
		}
	}

	return FAILURE;
}

static int
ssl_send( ssl_ctx_t *ctx, void *data, size_t size )
{
    ERR_clear_error();

    if ( SSL_write( ctx->ssl, data, size ) <= 0 ) {
		dbg_error( "Do SSL_write Failed!!!" );
		return FAILURE;
    } else {
		return SUCCESS;
	}
}

static int
ssl_open( ssl_ctx_t *ctx )
{
	long res = 0;

	// Try to create a new SSL context
	ctx->ctx = SSL_CTX_new( SSLv23_client_method() );
	if ( ctx->ctx == NULL ) {
		dbg_error( "Do SSL_CTX_new Failed!!!" );
		goto err;
	}

    // Disabling SSLv2 will leave v3 and TSLv1 for negotiation
    SSL_CTX_set_options( ctx->ctx, ( SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 ) );

    // Create new SSL connection state object
    ctx->ssl = SSL_new( ctx->ctx );
    if ( ctx->ssl == NULL ) {
		dbg_error( "Do SSL_new Failed!!!" );
		goto err;
    }

    // Attach the SSL session to the socket descriptor
    SSL_set_fd( ctx->ssl, ctx->fd );

    // Try to SSL-connect here, returns 1 for success
	res	= SSL_connect( ctx->ssl );
    if ( res != 1 ) {
		dbg_error( "Do SSL_connect Failed( %d )!!!", SSL_get_error( ctx->ssl, res ) );
		goto err;
    }

    // Get the remote certificate into the X509 structure
    ctx->cert = SSL_get_peer_certificate( ctx->ssl );
    if ( ctx->cert == NULL ) {
		dbg_error( "Do SSL_get_peer_certificate Failed!!!" );
		goto err;
    }

    // Verify certificate
    SSL_set_verify( ctx->ssl, 0, NULL );

    return SUCCESS;

err:
	ssl_close( ctx );

	return FAILURE;
}

static void
ssl_init( void )
{
	OpenSSL_add_all_algorithms();
	ERR_load_BIO_strings();
	ERR_load_crypto_strings();
	SSL_load_error_strings();

	SSL_library_init();
}

static int
h264_get_set( u8 *src, int size, struct iovec *set, u8 key )
{
	int i = 0;

	set[ 0 ].iov_base	= NULL;
	set[ 0 ].iov_len 	= 0;

	while ( ( i + 4 ) < size ) {
		if ( ( src[ i + 4 ] & 0xE0 ) &&
			 ( ( src[ i + 4 ] & 0x1F ) == key ) &&
			 ( src[ i + 3 ] == 0x01 ) &&
			 ( src[ i + 2 ] == 0x00 ) &&
			 ( src[ i + 1 ] == 0x00 ) &&
			 ( src[ i ] == 0x00 ) ) {
			i += 4;
			set[ 0 ].iov_base = src + i;

			while ( ( i + 3 ) < size ) {
				if ( ( src[ i + 4 ] & 0xE0 ) &&
					 ( src[ i + 3 ] == 0x01 ) &&
					 ( src[ i + 2 ] == 0x00 ) &&
					 ( src[ i + 1 ] == 0x00 ) &&
					 ( src[ i ] == 0x00 ) ) {
					return SUCCESS;
				}

				set[ 0 ].iov_len ++;
				i ++;
			}
			return SUCCESS;
		}
		i ++;
	}

	return FAILURE;
}

static int
mc_rtmp_send_video( RTMPHandle rtmp, AVFrame *frame, u32 timestamp )
{
	if ( ( frame->type & FRAME_MASK ) == FRAME_VIDEO_H264 ) {
		u8 *slice		= frame->buff;
		u32 slice_size	= frame->size;

		if ( ( slice[ 4 ] & 0x1F ) == 0x01 ) {
			slice += 4;
			slice_size -= 4;
			if ( RTMP_SendH264Frame( rtmp, slice, slice_size, timestamp ) == FAILURE ) {
				dbg_line( "Do RTMP_SendH264Frame Failed!!!" );
				return FAILURE;
			}
		} else {
			struct iovec sps, pps;
			int offset;

			h264_get_set( slice, slice_size, &sps, 0x07 );
			offset = ( sps.iov_len + 4 );
			slice += offset;
			slice_size -= offset;

			offset = h264_get_set( slice, slice_size, &pps, 0x08 );
			offset = ( pps.iov_len + 4 );
			slice += offset;
			slice_size -= offset;

			slice += 4;
			slice_size -= 4;

			if ( RTMP_SendH264InitFrame( rtmp, ( u8 * )sps.iov_base, sps.iov_len, pps.iov_base, pps.iov_len, timestamp ) == FAILURE ) {
				dbg_line( "Do RTMP_SendH264InitFrame Failed!!!" );
				return FAILURE;
			} else if ( RTMP_SendH264KeyFrame( rtmp, slice, slice_size, timestamp ) == FAILURE ) {
				dbg_line( "Do RTMP_SendH264KeyFrame Failed!!!" );
				return FAILURE;
			}
		}

		return frame->size;
	} else {
		dbg_warm( "Un-Support Video Type( %08x )!!!", frame->type );
		return FAILURE;
	}
}

static int
mc_rtmp_send_audio( RTMPHandle rtmp, AVFrame *frame, u32 timestamp, int inited )
{
	u8	*buff = frame->buff;
	u32	size = frame->size;

	if ( inited == FALSE ) {
		AudioConf	conf;

		Camera_GetAudioInConf( &conf );

		if ( conf.type == FRAME_AUDIO_AAC ) {
			if ( RTMP_SetAudio( rtmp, RTMP_AUDIO_FMT_AAC, conf.sampling_freq, conf.sample, conf.channels ) == FAILURE ) {
				dbg_line( "Do RTMP_SetAudio Failed!!!" );
				return FAILURE;
			}
		} else if ( conf.type == FRAME_AUDIO_PCMU ) {
			if ( RTMP_SetAudio( rtmp, RTMP_AUDIO_FMT_G711U, conf.sampling_freq, conf.sample, 1 ) == FAILURE ) {
				dbg_line( "Do RTMP_SetAudio Failed!!!" );
				return FAILURE;
			}
		} else if ( conf.type == FRAME_AUDIO_PCMA ) {
			if ( RTMP_SetAudio( rtmp, RTMP_AUDIO_FMT_G711A, conf.sampling_freq, conf.sample, 1 ) == FAILURE ) {
				dbg_line( "Do RTMP_SetAudio Failed!!!" );
				return FAILURE;
			}
		} else if ( conf.type == FRAME_AUDIO_PCM ) {
			dbg_line( "frame->type == FRAME_AUDIO_PCM" );
			if ( RTMP_SetAudio( rtmp, RTMP_AUDIO_FMT_LINEAR_PCM, conf.sampling_freq, conf.sample, conf.channels ) == FAILURE ) {
				dbg_line( "Do RTMP_SetAudio Failed!!!" );
				return FAILURE;
			}
		} else {
			dbg_warm( "Un-Support Audio Type( %08x )!!!", conf.type );
			return FAILURE;
		}
	}

	if ( frame->type == FRAME_AUDIO_AAC ) {
		buff += 7;
		size -= 7;
	}

	if ( RTMP_SendAudio( rtmp, buff, size, timestamp ) == FAILURE ) {
		dbg_line( "Do RTMP_SendAudio Failed!!!" );
		return FAILURE;
	}

	return frame->size;
}

static int
mc_rtmp_server_proc( void *data )
{
	RTMP_DoServe( data );
	RTMP_Close( data );

	return SUCCESS;
}

static int
mc_rtmp_open( RTMPHandle rtmp )
{
	stream_ctx_t *stream = ( stream_ctx_t * )my_malloc( sizeof( stream_ctx_t ) );

	if ( stream ) {
		RTMP_Bind( rtmp, stream );
		create_norm_thread( mc_rtmp_server_proc, rtmp, 0 );
		return SUCCESS;
	} else {
		dbg_error( "Do Malloc Failed!!!" );
		return FAILURE;
	}
}

static int
mc_rtmp_close( RTMPHandle rtmp, void *data )
{
	if ( data ) {
		stream_ctx_t *stream = ( stream_ctx_t * )data;

		if ( stream->dc ) {
			if ( stream->is_player ) {
				DC_RemoveReader( stream->dc );
			} else {
				DC_RemoveWriter( stream->dc );
			}
		}

		my_free( stream );
		RTMP_Bind( rtmp, NULL );
	}

	return SUCCESS;
}

static int
mc_rtmp_connect( RTMP_CTX *ctx, void *data )
{
	if ( ctx ) {
		if ( ctx->stream_name ) {
			dbg_warm( "ctx->stream_name: %s", ctx->stream_name );
		}

		if ( ctx->app ) {
			dbg_warm( "ctx->app: %s", ctx->app );
		}

		if ( ctx->swfUrl ) {
			dbg_warm( "ctx->swfUrl: %s", ctx->swfUrl );
		}

		if ( ctx->tcUrl ) {
			dbg_warm( "ctx->tcUrl: %s", ctx->tcUrl );
		}

		// Todo
	}
	return SUCCESS;
}

static int
mc_rtmp_createStream( char *stream_name, void *data )
{
	return SUCCESS;
}

static int
mc_rtmp_play( char *stream_name, void *data )
{
	if ( stream_name && data ) {
		stream_ctx_t *stream = ( stream_ctx_t * )data;

		stream->dc	= DC_CreateReader( stream_name );
		if ( stream->dc == NULL ) {
			dbg_line( "Do DC_CreateReader Failed!!!" );
			return FAILURE;
		}

		stream->is_player	= 1;

		return SUCCESS;
	}

	return FAILURE;
}

static int
mc_rtmp_send_stream( RTMPHandle rtmp, void *data )
{
	if ( rtmp && data ) {
		stream_ctx_t	*stream = ( stream_ctx_t * )data;
		AVFrame			frame;

		if ( DC_ReadFrame( stream->dc, &frame ) > 0 ) {
			u32 timestamp   = frame.ptime.tv_sec * 1000 + frame.ptime.tv_usec / 1000;

			if ( ( frame.type & FRAME_AV_MASK ) == FRAME_VIDEO ) {
				return mc_rtmp_send_video( rtmp, &frame, timestamp );
			} else {
				int ret = mc_rtmp_send_audio( rtmp, &frame, timestamp, stream->is_audio_init );

				if ( ret != FAILURE ) {
					stream->is_audio_init   = TRUE;
				}

				return ret;
			}
		}
	}

	return 0;
}

static BOOL
mc_time_delay_check( u32 frame_time, u32 curr_time, u32 delay_time )
{
	if ( ( curr_time >= frame_time ) && ( ( curr_time - frame_time ) > delay_time ) ) {
		return TRUE;
	}

	return FALSE;
}

static BOOL
mc_real_time_check( mc_stream_status *p_stream, u32 iskey, u32 curr_time )
{
	if ( p_stream->drop == TRUE ) {
 		if ( p_stream->wait == TRUE ) {
			if ( iskey == TRUE ) {
				p_stream->drop = FALSE;
			}
		}
	} else {
		if ( mc_time_delay_check( p_stream->timestamp, curr_time, p_stream->delay_time ) == TRUE ) {
			p_stream->drop = TRUE;
		}
	}

	return !(p_stream->drop);
}

static int
mc_stream_upload_proc( void *data )
{
	mc_stream_ctx *so	= ( mc_stream_ctx * )data;

	RTMP_CTX	ctx = { 0, };
	DC_READER	reader	= NULL;
	AVFrame		frame;

	int wait_frames		= 0, res;
	int audio_inited	= FALSE;
	char url[ 512 ];

	struct timeval		curr_time;
	u32					curr_timestamp;

	gettimeofday( &curr_time, NULL );

	memset( &stream_upload_info, 0, sizeof( stream_upload_info ) );

	stream_upload_info.last_update		=
	stream_upload_info.last_ping		=
	stream_upload_info.last_ping_ack	= curr_time.tv_sec;

	so->video.delay_time	= MC_VIDEO_REAL_TIME_DELAY_MSEC;
	so->audio.delay_time	= MC_AUDIO_REAL_TIME_DELAY_MSEC;

	while ( 1 ) {
		gettimeofday ( &curr_time, NULL );

		curr_timestamp	= curr_time.tv_sec * 1000 + curr_time.tv_usec / 1000;

		if ( ( curr_time.tv_sec >= ( stream_upload_info.last_update + MC_PRINT_INFO_INTERVAL ) ) ||
			 ( curr_time.tv_sec <= ( stream_upload_info.last_update - MC_PRINT_INFO_INTERVAL ) ) ) {
			dbg_g();
			dbg_g( " Total Frame: %d, Video: %d, Audio: %d",	stream_upload_info.video_frame_cnt + stream_upload_info.audio_frame_cnt,
															stream_upload_info.video_frame_cnt,
															stream_upload_info.audio_frame_cnt );
			dbg_g( " Total Data Size: %d Bytes, Video: %d Bytes, Audio: %d Bytes\n",
															stream_upload_info.video_size + stream_upload_info.audio_size,
															stream_upload_info.video_size,
															stream_upload_info.audio_size );

			stream_upload_info.video_frame_cnt	= 0;
			stream_upload_info.video_size		= 0;
			stream_upload_info.audio_frame_cnt	= 0;
			stream_upload_info.audio_size		= 0;
			stream_upload_info.last_update		= curr_time.tv_sec;
		}

		if ( so->cmd == MC_CMD_STOP ) {
			so->cmd		= MC_CMD_NONE;
			goto conn_err;
		} else if ( so->cmd == MC_CMD_START ) {
			so->status	= MC_STATUS_WORKING;
			so->cmd		= MC_CMD_NONE;
		}

		if ( so->status == MC_STATUS_WORKING ) {
			if ( so->rtmp == NULL ) {
				sprintf( url, "rtmp://%s:%d/%s", so->host_name, so->port, so->url );

				dbg_warm( "Upload to %s, title: %s", url, so->title );

				pthread_mutex_lock( &so->mutex );
				ctx.host_name	= so->host_name;
				ctx.port		= so->port;
				ctx.upload		= RTMP_UPLOAD_LIVE;
				ctx.stream_name	= so->title;
				ctx.app			= so->url;
				ctx.swfUrl		= url;
				ctx.tcUrl		= url;
				pthread_mutex_unlock( &so->mutex );

				so->rtmp = RTMP_Open( &ctx );

				if ( so->rtmp == NULL ) {
					dbg_line( "RTMP_Open: %s Failed!!", url );
					goto conn_err;
				}

				dbg_error( "Start Streaming @ %s", ctime( ( time_t * )&curr_time.tv_sec ) );
			}

			if ( reader == NULL ) {
				reader	= DC_CreateReader( so->shm_name );

				if ( reader == NULL ) {
					dbg_warm( "Do DC_CreateReader( %s ) Failed!!!", so->shm_name );
					MSleep( 100 );
					continue;
				}

				//init stream parameters
				so->video.drop			= FALSE;
				so->video.wait			= FALSE;
				so->audio.drop			= FALSE;
				so->audio.wait			= FALSE;
			}

			stream_upload_info.last_ping_ack	= curr_time.tv_sec;
		} else if ( so->status == MC_STATUS_IDLE ) {
			MSleep( 100 );
			continue;
		} else if ( so->status == MC_STATUS_ERROR ) {
			dbg_error( "so->status == MC_STATUS_ERROR( %s )!!", ctime( ( time_t * )&curr_time.tv_sec ) );
			goto conn_err;
		}

		if ( DC_ReadFrame( reader, &frame ) > 0 ) {
			if ( ( frame.type & FRAME_AV_MASK ) == FRAME_VIDEO ) {
				so->video.timestamp = frame.ptime.tv_sec * 1000 + frame.ptime.tv_usec / 1000;
				if ( mc_real_time_check( &so->video, frame.iskey, curr_timestamp ) == TRUE ) {
					if ( mc_rtmp_send_video( so->rtmp, &frame, so->video.timestamp ) == FAILURE ) {
						so->status = MC_STATUS_ERROR;
					} else {
						stream_upload_info.video_frame_cnt ++;
						stream_upload_info.video_size += frame.size;
					}

					if ( curr_timestamp > ( so->video.timestamp + 2000 ) ) {
						stream_upload_info.delay	= TRUE;
					} else if ( curr_timestamp < ( so->video.timestamp + 500 ) ) {
						stream_upload_info.delay	= FALSE;
					}
				}

				so->video.wait = FALSE;
			} else {
				so->audio.timestamp = frame.ptime.tv_sec * 1000 + frame.ptime.tv_usec / 1000;

				if ( mc_real_time_check( &so->audio, frame.iskey, curr_timestamp ) == TRUE ) {
					if ( mc_rtmp_send_audio( so->rtmp, &frame, so->audio.timestamp, audio_inited ) == FAILURE ) {
						so->status = MC_STATUS_ERROR;
					} else {
						audio_inited = TRUE;

						stream_upload_info.audio_frame_cnt ++;
						stream_upload_info.audio_size += frame.size;
					}
				}

				so->audio.wait = FALSE;
			}

			if ( curr_time.tv_sec > MC_PING_INTERVAL ) {
				if ( ( curr_time.tv_sec >= ( stream_upload_info.last_ping + MC_PING_INTERVAL ) ) ||
					 ( curr_time.tv_sec <= ( stream_upload_info.last_ping - MC_PING_INTERVAL ) ) ) {
					dbg_warm( "curr_time.tv_sec: %lu", curr_time.tv_sec );
					RTMP_PingRequest( so->rtmp );
					dbg_warm( "stream_upload_info.last_ping: %u", stream_upload_info.last_ping );
					stream_upload_info.last_ping		= curr_time.tv_sec;
					stream_upload_info.last_ping_ack	= 0;
				}

				if ( stream_upload_info.last_ping_ack == 0 ) {
					dbg_warm( "RTMP_GetPingRequest" );
					res = RTMP_GetPingRequest( so->rtmp );
					if ( res > 0 ) {
						dbg_warm( "Got Ping Ack at %s", ctime( ( time_t * )&curr_time.tv_sec ) );
						stream_upload_info.last_ping_ack	= curr_time.tv_sec;
					} else if ( res == FAILURE ) {
						so->status = MC_STATUS_ERROR;
					}
					dbg_warm( "RTMP_GetPingRequest" );
				}
			}

			wait_frames = 0;
		} else {
			wait_frames ++;
			so->video.wait = TRUE;
			so->audio.wait = TRUE;

			if ( wait_frames > 30 ) {
				dbg_line( "Waiting for Frame In...." );
			} else {
				stream_upload_info.delay = FALSE;
				MSleep( 30 );
			}
		}

		continue;

conn_err:
		if ( so->rtmp ) {
			RTMP_Close( so->rtmp );
			so->rtmp		= NULL;
			audio_inited	= FALSE;
		}

		if ( reader ) {
			DC_RemoveReader( reader );
			reader	= NULL;
		}

		so->status	= MC_STATUS_IDLE;
	}

	return 0; // Couldn't open file
}

static int
mc_rtmp_audio_in( u8 *buff, u32 size, void *data )
{
	if ( buff && size ) {
		Camera_PlayAudio( buff, size );
	}

	return SUCCESS;
}

static int
mc_rtmp_video_in( u8 *buff, u32 size, void *data )
{
	return SUCCESS;
}

static int
mc_stream_download_proc( void *data )
{
	RTMP_CTX	ctx = { 0, };
	char url[ 512 ];

	RTMP_ServeHandle handle = { 0, };

	handle.audio_in			= mc_rtmp_audio_in;
	handle.video_in			= mc_rtmp_video_in;

	while ( 1 ) {
		if ( stream_in.cmd == MC_CMD_START ) {
			pthread_mutex_lock( &stream_in.mutex );
			stream_in.status	= MC_STATUS_WORKING;
			stream_in.cmd		= MC_CMD_NONE;
			pthread_mutex_unlock( &stream_in.mutex );

			sprintf( url, "rtmp://%s:%d/%s", stream_in.host_name, stream_in.port, stream_in.url );

			dbg_warm( "Upload to %s, title: %s", url, stream_in.title );

			pthread_mutex_lock( &stream_in.mutex );
			ctx.host_name	= stream_in.host_name;
			ctx.port		= stream_in.port;
			ctx.stream_name	= stream_in.title;
			ctx.app			= stream_in.url;
			ctx.swfUrl		= url;
			ctx.tcUrl		= url;
			pthread_mutex_unlock( &stream_in.mutex );

			stream_in.rtmp = RTMP_Play( &ctx, &handle );

			if ( stream_in.rtmp == NULL ) {
				dbg_line( "RTMP_Play: %s Failed!!", url );
			} else {

				//Camera_EnableAudio( 0 );

				RTMP_DoRecv( stream_in.rtmp );

				pthread_mutex_lock( &stream_in.mutex );
				RTMP_Close( stream_in.rtmp );
				stream_in.rtmp = NULL;
				pthread_mutex_unlock( &stream_in.mutex );

				//Camera_EnableAudio( 1 );
			}
		}

		pthread_mutex_lock( &stream_in.mutex );
		stream_in.status	= MC_STATUS_IDLE;
		pthread_mutex_unlock( &stream_in.mutex );

		MSleep( 100 );
	}

	return 0; // Couldn't open file
}

static void
mc_free_ctx( mc_stream_ctx *ctx )
{
# define FREE_CTX( x ) {	\
	if ( ctx->x ) {			\
		my_free( ctx->x );	\
		ctx->x = NULL;		\
	}						\
}

	pthread_mutex_lock( &ctx->mutex );

	FREE_CTX( shm_name );
	FREE_CTX( host_name );
	FREE_CTX( title );
	FREE_CTX( url );
	FREE_CTX( username );
	FREE_CTX( password );

	pthread_mutex_unlock( &ctx->mutex );
}

static int
mc_fill_ctx( mc_stream_ctx *ctx, MediaCenter_StreamCTX *input )
{
# define FILL_CTX( x, need ) {				\
	if ( input->x ) {						\
		ctx->x	= my_strdup( input->x );	\
	} else if ( need ) {					\
		pthread_mutex_unlock( &ctx->mutex );\
		return FAILURE;						\
	}										\
}

	pthread_mutex_lock( &ctx->mutex );

	FILL_CTX( shm_name, 1 );
	FILL_CTX( host_name, 1 );
	FILL_CTX( title, 1 );
	FILL_CTX( url, 1 );
	FILL_CTX( username, 0 );
	FILL_CTX( password, 0 );

	ctx->port		= input->port;

	pthread_mutex_unlock( &ctx->mutex );

	return SUCCESS;
}

static int
mc_upload_open( mc_upload_ctx *ctx, char *filename )
{
	int	fd = 0;
	struct stat			st;

	if ( stat( filename, &st ) ) {
		dbg_error( "File NOT FOUND( %s )!!!", filename );
		return FAILURE;
	} else if ( st.st_size == 0 ) {
		dbg_error( "File is Empty( %s )!!!", filename );
		return FAILURE;
	}

	fd	= open( filename, O_RDWR );
	if ( fd < 0 ) {
		dbg_error( "Open File Failed( %s )!!!", filename );
		return FAILURE;
	}

	ctx->file_size	= st.st_size;

	return fd;
}

static int
mc_upload_conn( mc_upload_ctx *ctx, char *url )
{
	ctx->proto	= url;

	ctx->addr	= strstr( ctx->proto, "://" );
	if ( ctx->addr == NULL ) {
		dbg_error( "URL Format Error( %s )!!!", ctx->proto );
		return FAILURE;
	}

	*ctx->addr	= 0;
	ctx->addr += 3;

	ctx->uri	= strstr( ctx->addr, "/" );
	if ( ctx->uri == NULL ) {
		dbg_error( "URL Format Error( %s )!!!", ctx->proto );
		return FAILURE;
	}

	*ctx->uri	= 0;
	ctx->uri ++;

	if ( strcmp( ctx->proto, "https" ) == 0 ) {
		ctx->ssl		= ( ssl_ctx_t * )my_malloc( sizeof( ssl_ctx_t ) );
		if ( ctx->ssl == NULL ) {
			dbg_error( "Out-of-memory!!!" );
			return FAILURE;
		}

		ctx->ssl->fd	= sock_conn( ctx->addr, 443, TRUE );

		if ( ctx->ssl->fd < 0 ) {
			dbg_error( "Network Connect Failed( %s: 443 )!!!", ctx->addr );
			return FAILURE;
		}

		if ( ssl_open( ctx->ssl ) < 0 ) {
			dbg_error( "Do ssl_open Failed!!!" );
			return FAILURE;
		}
	} else if ( strcmp( ctx->proto, "http" ) == 0 ) {
		ctx->sock	= sock_conn( ctx->addr, 80, FALSE );

		if ( ctx->sock < 0 ) {
			dbg_error( "Network Connect Failed( %s: 80 )!!!", ctx->addr );
			return FAILURE;
		}
	} else {
		dbg_error( "HTTP Protocol Error( %s )!!!", ctx->proto );
		return FAILURE;
	}

	return SUCCESS;
}

static int
mc_upload_send( mc_upload_ctx *ctx, void *data, u32 size )
{
	if ( ctx->ssl ) {
		return ssl_send( ctx->ssl, data, size );
	} else {
		return sock_send( ctx->sock, data, size );
	}
}

static int
mc_upload_recv( mc_upload_ctx *ctx, char *data, u32 size )
{
	if ( ctx->ssl ) {
		size	= ssl_recv( ctx->ssl, data, 1023 );
	} else {
		size	= sock_recv( ctx->sock, data, 1023 );
	}

	if ( size > 0 ) {
		data[ size ]	= 0;
		if ( strstr( data, "HTTP/1.1 200 OK" ) == NULL ) {
			dbg_line( "%s", data );
			return FAILURE;
		}
	} else {
		dbg_error( "Do sock_recv Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static void
mc_upload_close( mc_upload_ctx *ctx )
{
	if ( ctx->ssl ) {
		ssl_close( ctx->ssl );
		my_free( ctx->ssl );
	} else if ( ctx->sock > 0 ) {
		close( ctx->sock );
	}
}

static int
mc_upload_file()
{
	MediaCenter_UploadCTX *ctx	= &upload_file.ctx;
	int	fd = 0, res = FAILURE, size;
	char *url = NULL;
	u8 data[ 1024 ];

	mc_upload_ctx		upload_ctx = { 0, };

	pthread_mutex_lock( &upload_file.mutex );
	url	= my_strdup( ctx->upload_url );
	pthread_mutex_unlock( &upload_file.mutex );

	if ( url == NULL ) {
		dbg_error( "Out-of-memory!!!" );
		return FAILURE;
	}

	fd	= mc_upload_open( &upload_ctx, ctx->path );
	if ( fd < 0 ) {
		goto err;
	}

	if ( mc_upload_conn( &upload_ctx, url ) == FAILURE ) {
		goto err;
	}

	size	= sprintf( ( char * )data,	"PUT /%s HTTP/1.1\r\n"
										"Host: %s\r\n"
										"Connection: close\r\n"
										"Cache-Control: no-cache\r\n"
										"Content-Type: application/octet-stream\r\n"
										"Content-Length: %u\r\n\r\n",
										upload_ctx.uri,
										upload_ctx.addr,
										upload_ctx.file_size );

	if ( mc_upload_send( &upload_ctx, data, size ) == FAILURE ) {
		goto err;
	}

	upload_file.file_size	= upload_ctx.file_size;

	while ( ( size = read( fd, data, 1024 ) ) > 0 ) {
		if ( mc_upload_send( &upload_ctx, data, size ) == FAILURE ) {
			goto err;
		}

		upload_file.crc32		= CRC32_Update( upload_file.crc32, data, size );
	}

	if ( mc_upload_recv( &upload_ctx, ( char * )data, size ) == FAILURE ) {
		goto err;
	}

	dbg_warm( "upload_file.crc32: 0x%08x", upload_file.crc32 );

	res = SUCCESS;

err:
	mc_upload_close( &upload_ctx );

	if ( fd > 0 ) {
		close( fd );
	}

	my_free( url );

	return res;
}

static int
mc_file_upload_proc( void *data )
{
	int res;

	while ( 1 ) {
		if ( upload_file.cmd == MC_CMD_START ) {
			upload_file.status 		= MC_STATUS_WORKING;
			upload_file.cmd			= MC_CMD_NONE;
			upload_file.file_size	= 0;
			upload_file.crc32		= CRC32_INIT_VALUE;

			res = mc_upload_file();

			if ( upload_file.ctx.ready ) {
				upload_file.ctx.ready( res, upload_file.crc32, upload_file.file_size, upload_file.ctx.user_data );
			}

			upload_file.status = MC_STATUS_READY;
		}

		msleep( 100 );
	}

	return 0;
}

static int
mc_video_generate_proc( void *data )
{
	int res;

	Record_FileCTX		ctx;
	mc_recording_ctx	*rec	= &mc_ctx[ MC_UPLOAD_TYPE_VIDEO ];

	AudioConf	conf;

	Camera_GetAudioInConf( &conf );

	ctx.filename	= "/var/DkRecord/DxRecording.mp4";

	system( "mkdir -p /var/DkRecord/" );

	while ( 1 ) {
		if ( rec->cmd == MC_CMD_START ) {
			rec->status 	= MC_STATUS_WORKING;
			rec->cmd		= MC_CMD_NONE;

			unlink( ctx.filename );

			ctx.video.fps			= rec->ctx.fps;

			ctx.audio.sample_rate	= conf.sampling_freq;
			ctx.audio.sample_size	= conf.sample;
			ctx.audio.channels		= conf.channels;

			ctx.target_time	= rec->ctx.target_time;
			ctx.pre_secs	= rec->ctx.pre_secs;
			ctx.post_secs	= rec->ctx.post_secs;

			res = Record_GenMP4File( &ctx );

			if ( rec->ctx.ready ) {
				rec->ctx.ready( res, ctx.filename, rec->ctx.user_data );
			}

			rec->status = MC_STATUS_READY;
		}

		MSleep( 100 );
	}

	return 0;
}

# define SNAPSHOT_PATH			"/var/DkSnapshot/"
# define SNAPSHOT_TGZ_PATH		"/var/DkSnapshot/snapshot.tar.gz"

static int
mc_image_generate_proc( void *data )
{
	int res;

	Snapshot_FileCTX	ctx;
	mc_recording_ctx	*rec	= &mc_ctx[ MC_UPLOAD_TYPE_IMAGE ];

	ctx.filepath	= SNAPSHOT_PATH;

	system( "mkdir -p "SNAPSHOT_PATH );

	while ( 1 ) {
		if ( rec->cmd == MC_CMD_START ) {
			rec->status 	= MC_STATUS_WORKING;
			rec->cmd		= MC_CMD_NONE;

			system( "rm -rf "SNAPSHOT_PATH"*" );

			ctx.target_time	= rec->ctx.target_time;
			ctx.pre_secs	= rec->ctx.pre_secs;
			ctx.post_secs	= rec->ctx.post_secs;

			res = Snapshot_GenPicWithAVI( &ctx );

			if ( res == SUCCESS ) {
				system( "cd "SNAPSHOT_PATH";tar czf "SNAPSHOT_TGZ_PATH" *" );
			}

			if ( rec->ctx.ready ) {
				rec->ctx.ready( res, SNAPSHOT_TGZ_PATH, rec->ctx.user_data );
			}

			rec->status = MC_STATUS_READY;
		}

		MSleep( 100 );
	}

	return 0;
}

static int
mc_record_start( MC_UPLOAD_TYPE type, MediaCenter_RecordCTX *ctx )
{
	if ( mc_ctx[ type ].cmd == MC_CMD_START ) {
		return FAILURE;
	} else if ( mc_ctx[ type ].status == MC_STATUS_WORKING ) {
		return FAILURE;
	} else {
		pthread_mutex_lock( &mc_ctx[ type ].mutex );

		memcpy( &mc_ctx[ type ].ctx, ctx, sizeof( MediaCenter_RecordCTX ) );

		mc_ctx[ type ].cmd				= MC_CMD_START;

		pthread_mutex_unlock( &mc_ctx[ type ].mutex );
	}

	return SUCCESS;
}

//==========================================================================
// APIs
//==========================================================================
int
MediaCenter_StopUpload( u32 id )
{
	u32 start_clock	= seconds();

	if ( id > max_stream_out_cnt ) {
		return FAILURE;
	}

	stream_outs[ id ].cmd	= MC_CMD_STOP;

	while ( stream_outs[ id ].status == MC_STATUS_WORKING ) {
		MSleep( 100 );

		if ( seconds() > ( start_clock + MC_TIMEOUT ) ) {
			dbg_warm( "Do MediaCenter_StopUpload Timeout!!!" );
			return FAILURE;
		}
	}
	mc_free_ctx( &stream_outs[ id ] );

	return SUCCESS;
}

int
MediaCenter_StartUpload( u32 id, MediaCenter_StreamCTX *ctx )
{
	if ( id > max_stream_out_cnt ) {
		dbg_warm( "id > max_stream_out_cnt" );
		return FAILURE;
	} else if ( ctx == NULL ) {
		dbg_warm( "ctx == NULL" );
		return FAILURE;
	} else if ( stream_outs[ id ].status == MC_STATUS_WORKING ) {
		dbg_warm( "stream_outs[ id ].status == MC_STATUS_WORKING" );
		MediaCenter_StopUpload( id );
	}

	mc_free_ctx( &stream_outs[ id ] );
	mc_fill_ctx( &stream_outs[ id ], ctx );

	stream_outs[ id ].cmd	= MC_CMD_START;

	return SUCCESS;
}

BOOL
MediaCenter_ConnectionAvailable( u32 id )
{
	if ( id >= max_stream_out_cnt ) {
		return FALSE;
	} else if ( stream_outs[ id ].cmd == MC_CMD_START ) {
		return TRUE;
	} else if ( stream_outs[ id ].status == MC_STATUS_WORKING ) {
		return TRUE;
	} else {
		return FALSE;
	}
}

int
MediaCenter_StopDownload( void )
{
	if ( stream_in.status == MC_STATUS_WORKING ) {
		u32 start_clock	= seconds();

		pthread_mutex_lock( &stream_in.mutex );
		if ( stream_in.rtmp ) {
			RTMP_Stop( stream_in.rtmp );
		}
		pthread_mutex_unlock( &stream_in.mutex );

		while ( stream_in.status == MC_STATUS_WORKING ) {
			MSleep( 100 );

			if ( seconds() > ( start_clock + MC_TIMEOUT ) ) {
				dbg_warm( "Do MediaCenter_StopUpload Timeout!!!" );
				return FAILURE;
			}
		}
	}

	return SUCCESS;
}

BOOL
MediaCenter_DownloadIsWorking()
{
	int ret = FALSE;

	pthread_mutex_lock( &stream_in.mutex );
	if ( ( stream_in.cmd == MC_CMD_NONE ) &&
		 ( stream_in.status == MC_STATUS_IDLE ) ) {
		ret	= FALSE;
	} else {
		ret = TRUE;
	}
	pthread_mutex_unlock( &stream_in.mutex );

	return ret;
}

int
MediaCenter_StartDownload( MediaCenter_StreamCTX *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	} else if ( stream_in.status == MC_STATUS_WORKING ) {
		MediaCenter_StopDownload();
	}

	mc_free_ctx( &stream_in );
	mc_fill_ctx( &stream_in, ctx );

	pthread_mutex_lock( &stream_in.mutex );
	stream_in.cmd	= MC_CMD_START;
	pthread_mutex_unlock( &stream_in.mutex );

	return SUCCESS;
}

int
MediaCenter_StartRecord( MC_UPLOAD_TYPE type, MediaCenter_RecordCTX *ctx )
{
	if ( ctx ) {
		if ( ( type == MC_UPLOAD_TYPE_VIDEO ) || ( type == MC_UPLOAD_TYPE_IMAGE ) ) {
			return mc_record_start( type, ctx );
		}
	}

	return FAILURE;
}

MC_STREAM_OUT_STATUS
MediaCenter_StreamOutStatus( void )
{
	if ( stream_upload_info.error == TRUE ) {
		dbg_error( "MC_STREAM_OUT_STATUS_ERROR" );
		return MC_STREAM_OUT_STATUS_ERROR;
	} else if ( stream_upload_info.delay == TRUE ) {
		dbg_error( "MC_STREAM_OUT_STATUS_DELAY" );
		return MC_STREAM_OUT_STATUS_DELAY;
	} else {
		return MC_STREAM_OUT_STATUS_NORMAL;
	}
}

int
MediaCenter_UploadFile( MediaCenter_UploadCTX *ctx )
{
	if ( upload_file.cmd == MC_CMD_START ) {
		return FAILURE;
	} else if ( upload_file.status == MC_STATUS_WORKING ) {
		return FAILURE;
	} else {
		pthread_mutex_lock( &upload_file.mutex );

		memcpy( &upload_file.ctx, ctx, sizeof( MediaCenter_RecordCTX ) );

		upload_file.cmd				= MC_CMD_START;

		pthread_mutex_unlock( &upload_file.mutex );
	}

	return SUCCESS;
}

int
MediaCenter_Init( u32 stream_out_cnt )
{
	int i;
	RTMP_ServeHandle handle;

	max_stream_out_cnt	= stream_out_cnt;

	if ( max_stream_out_cnt ) {
		u32 buff_size = sizeof( mc_stream_ctx ) * max_stream_out_cnt;

		stream_outs = ( mc_stream_ctx * )my_malloc( buff_size );
		if ( stream_outs ) {
			for ( i = 0; i < stream_out_cnt; i ++ ) {
				pthread_mutex_init( &stream_outs[ i ].mutex, NULL );
//				create_norm_thread( mc_stream_upload_proc, &stream_outs[ i ], 0 );
				create_sys_thread( mc_stream_upload_proc, &stream_outs[ i ], 5 );
			}
		} else {
			dbg_warm( "Do my_malloc( %u ) Failed!!!", buff_size );
			return FAILURE;
		}
	}

	pthread_mutex_init( &stream_in.mutex, NULL );
	create_norm_thread( mc_stream_download_proc, NULL, 0 );

	memset( &upload_file, 0, sizeof( mc_upload_file_ctx ) );
	pthread_mutex_init( &upload_file.mutex, NULL );
	create_norm_thread( mc_file_upload_proc, NULL, 0 );

	ssl_init();

	for ( i = 0; i < MC_UPLOAD_TYPE_MAX; i ++ ) {
		memset( &mc_ctx[ i ], 0, sizeof( mc_recording_ctx ) );

		pthread_mutex_init( &mc_ctx[ i ].mutex, NULL );
	}

	create_norm_thread( mc_video_generate_proc, NULL, 0 );
	create_norm_thread( mc_image_generate_proc, NULL, 0 );

	memset( &handle, 0, sizeof( RTMP_ServeHandle ) );

	handle.open				= mc_rtmp_open;
	handle.close			= mc_rtmp_close;
	handle.connect			= mc_rtmp_connect;
	handle.createStream		= mc_rtmp_createStream;
	handle.send_stream		= mc_rtmp_send_stream;
	handle.play				= mc_rtmp_play;

	RTMP_CreateServer( 1935, 5, &handle );

	return SUCCESS;
}

