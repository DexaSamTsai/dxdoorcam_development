/*
 * MediaCenter.h
 *
 *  Created on: 2017/08/09
 *      Author: Sam Tsai
 */

#ifndef __MEDIA_CENTER_H__
#define __MEDIA_CENTER_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Types.h"

//==========================================================================
// Type Define
//==========================================================================
typedef enum {
	MC_UPLOAD_TYPE_VIDEO	= 0,
	MC_UPLOAD_TYPE_IMAGE,
	MC_UPLOAD_TYPE_MAX,
} MC_UPLOAD_TYPE;

typedef enum {
	MC_STREAM_OUT_STATUS_ERROR	= -1,
	MC_STREAM_OUT_STATUS_NORMAL,
	MC_STREAM_OUT_STATUS_DELAY,
} MC_STREAM_OUT_STATUS;

typedef struct {
	char	*shm_name;
	char	*host_name;
	u16		port;
	char	*title;
	char	*url;
	char	*username;
	char	*password;
} MediaCenter_StreamCTX;

typedef struct {
	time_t		target_time;
	u32			pre_secs;
	u32			post_secs;

	u32			fps;

    void		*user_data;
    int			( *ready ) ( int, char *, void * );        // Status, Path, User Data
} MediaCenter_RecordCTX;

typedef struct {
	char		*upload_url;
	char		*path;

	void		*user_data;
	int			( *ready ) ( int, u32, u32, void * );        // Status, CRC32, File Size, User Data
} MediaCenter_UploadCTX;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
MediaCenter_StopUpload( u32 id );

extern int
MediaCenter_StartUpload( u32 id, MediaCenter_StreamCTX *ctx );

extern BOOL
MediaCenter_ConnectionAvailable( u32 id );

extern int
MediaCenter_StopDownload( void );

extern int
MediaCenter_StartDownload( MediaCenter_StreamCTX *ctx );

extern int
MediaCenter_StartRecord( MC_UPLOAD_TYPE type, MediaCenter_RecordCTX *ctx );

extern MC_STREAM_OUT_STATUS
MediaCenter_StreamOutStatus( void );

extern int
MediaCenter_Init( u32 stream_out_cnt );

# ifdef __cplusplus
}
# endif // __cplusplus

#endif	// End __MEDIA_CENTER_H__
