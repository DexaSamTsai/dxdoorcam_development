/*
 * Malloc.h
 *
 *  Created on: 2017/08/08
 *      Author: Sam Tsai
 */

# ifndef __MY_MALLOC_H__
# define __MY_MALLOC_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Types.h"

//==========================================================================
// Type Define
//==========================================================================
# define my_strdup( str )			My_StrDup( __FUNCTION__, __LINE__, str )
# define my_malloc( size )			My_Malloc( __FUNCTION__, __LINE__, size )
# define my_free( ptr )				My_Free( __FUNCTION__, __LINE__, ptr )
# define my_realloc( ptr, size )	My_Realloc( __FUNCTION__, __LINE__, ptr, size )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern char *
My_StrDup( const char *func, u32 line, char *str );

extern void *
My_Malloc( const char *func, u32 line, u32 size );

extern void
My_Free( const char *func, u32 line, void *ptr );

extern void *
My_Realloc( const char *func, u32 line, void *ptr, u32 size );

extern void
My_MemDump( void );

extern void
My_MemInit( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __MY_MALLOC_H__
