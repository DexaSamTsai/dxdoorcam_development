/*!
 *  @file       Malloc.c
 *  @version    1.0
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
typedef struct my_memory_t		my_memory_t;

struct my_memory_t {
	my_memory_t		*next;
	char            *func;
	u32				line;
	time_t			time;
	u32				size;
	u8				ptr[ 0 ];
};

//==========================================================================
// Static Param
//==========================================================================
static my_memory_t		*memorys		= NULL;
static my_memory_t		*memorys_end	= NULL;
static pthread_mutex_t	memory_mutex = PTHREAD_MUTEX_INITIALIZER;

//==========================================================================
// Static Functions
//==========================================================================
static int
memory_remove( my_memory_t *memory )
{
	int res = FAILURE;

	pthread_mutex_lock( &memory_mutex );

	if ( memorys == memory ) {
		memorys		= memory->next;
		memorys_end	= memory->next;
		res = SUCCESS;
	} else if ( memorys ) {
		my_memory_t *m	= memorys;

		while ( m->next ) {
			if ( m->next == memory ) {
				m->next		= memory->next;

				if ( memorys_end == memory ) {
					memorys_end	= m;
				}
				res = SUCCESS;
				break;
			}
			m = m->next;
		}
	}

	pthread_mutex_unlock( &memory_mutex );

	return res;
}

static int
memory_insert( my_memory_t *memory )
{
# if 0
	pthread_mutex_lock( &memory_mutex );

	if ( memorys == NULL ) {
		memorys = memory;
	} else if ( memorys_end ) {
		memorys_end->next			= memory;
	} else {
		dbg_error( "Something Wrong!!!" );
	}

	memory->next	= NULL;
	memorys_end		= memory;

	pthread_mutex_unlock( &memory_mutex );

	return SUCCESS;
# else
	int res = FAILURE;

	pthread_mutex_lock( &memory_mutex );

	if ( memorys == NULL ) {
		memorys = memory;
		res = SUCCESS;
	} else {
		my_memory_t *m	= memorys;

		while ( m->next ) {
			m = m->next;
		}

		m->next			= memory;
		memory->next	= NULL;
		res = SUCCESS;
	}

	pthread_mutex_unlock( &memory_mutex );

	return res;
# endif
}

static my_memory_t *
memory_find( void *ptr )
{
	my_memory_t *m;

	pthread_mutex_lock( &memory_mutex );

	m	= memorys;
	while ( m ) {
		if ( m->ptr == ptr ) {
			break;
		}
		m = m->next;
	}

	pthread_mutex_unlock( &memory_mutex );

	return m;
}

//==========================================================================
// APIs
//==========================================================================
char *
My_StrDup( const char *func, u32 line, char *str )
{
	//dbg_line( "Called from:\033[1;31m%s[ %u ]\033[0;39m", func, line );
	if ( str ) {
		my_memory_t *memory;
		int str_len	= strlen( str );
		int size	= sizeof( my_memory_t ) + str_len + 1;

		size += ( -( long )size ) & ( sizeof( void * ) - 1 );
		memory = ( my_memory_t * )malloc( size );

		if ( memory ) {
			memcpy( memory->ptr, str, str_len );

			memory->ptr[ str_len ]	= 0;
			memory->size	= size;
			memory->func	= strdup( func );
			memory->line	= line;
			memory->time    = time( NULL );

			memory_insert( memory );
			//dbg_warm( "New: %p", memory );
			return ( char * )memory->ptr;
		}
	}
	return NULL;
}

void *
My_Malloc( const char *func, u32 line, u32 size )
{
	//dbg_line( "Called from:\033[1;31m%s[ %u ]\033[0;39m", func, line );
	if ( size ) {
		my_memory_t *memory;

		size += sizeof( my_memory_t );
		size += ( -( long )size ) & ( sizeof( void * )-1 );

		memory = ( my_memory_t * )malloc( size );

		if ( memory ) {
			memset( memory, 0, size );

			memory->size	= size;
			memory->func	= strdup( func );
			memory->line	= line;
			memory->time    = time( NULL );

			memory_insert( memory );
			//dbg_warm( "New ptr: %p, size: %u", memory->ptr, size );

			return memory->ptr;
		}
		dbg_warm();
	}

	return NULL;
}

void
My_Free( const char *func, u32 line, void *ptr )
{
	//dbg_line( "Called from:\033[1;31m%s[ %u ]: %p\033[0;39m", func, line, ptr );
	if ( ptr ) {
		my_memory_t *memory = memory_find( ptr );

		if ( memory ) {
			memory_remove( memory );

			if ( memory->func ) {
				free( memory->func );
			}

			free( memory );
		} else {
			dbg_line( "Memory not Found!!!" );
			free( ptr );
		}
	}
}

void *
My_Realloc( const char *func, u32 line, void *ptr, u32 size )
{
	//dbg_line( "Called from:\033[1;31m%s[ %u ]\033[0;39m", func, line );
	if ( ptr && size ) {
		my_memory_t *memory = memory_find( ptr );
		//dbg_line( "Org: %p, %d, ptr: %p", memory, memory->size, memory->ptr );

		if ( memory ) {
			size += sizeof( my_memory_t );
			size += ( -( long )size ) & ( sizeof( void * )-1 );

			if ( size <= memory->size ) {
				return memory->ptr;
			} else {
				my_memory_t *new;

				memory->size	= size;
				memory_remove( memory );

				if ( memory->func ) {
					free( memory->func );
                    memory->func    = NULL;
				}

				new = ( my_memory_t * )realloc( memory, size );
				if ( new ) {
					memory = new;
					memory_insert( memory );

					memory->func	= strdup( func );
					memory->line	= line;
					memory->time	= time( NULL );

					//dbg_line( "New: %p, %d, ptr: %p", memory, memory->size, memory->ptr );
					return memory->ptr;
				} else {
					free( memory );
					dbg_error( "Do realloc Failed!!!" );
				}
			}
		} else {
			dbg_error( "Memory not Found( %p )!!!", ptr );
		}
	}
	return NULL;
}

void
My_MemDump( void )
{
	my_memory_t *m;

	pthread_mutex_lock( &memory_mutex );

	m	= memorys;
	while ( m ) {
		dbg_b( "[%s: %d]size: %d@%s", ( m->func )? m->func: "Unknow", m->line, m->size, ctime( &m->time ) );
		m = m->next;
	}

	pthread_mutex_unlock( &memory_mutex );
}

void
My_MemInit( void )
{
	pthread_mutex_init( &memory_mutex, NULL );
}
