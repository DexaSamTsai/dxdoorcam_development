/*!
 *  @file       IPCamUtils.c
 *  @version    1.0
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Static Param
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
/*!
 * @brief   Get Free Memory Size
 * @return  Free memory size
 */
u32
sys_avail( void )
{
# if 0
    struct sysinfo info;

    if ( sysinfo( &info ) < 0 ) {
        return 0;
    } else {
# if 0
		printf( "------------ Sysinfo --------------\n" );
		printf( "up time:    %ld\n", info.uptime );
		printf( "loads:      %lu %lu %lu\n", info.loads[0], info.loads[1], info.loads[2] );
		printf( "total ram:  %lu\n", info.totalram );
		printf( "free ram:   %u\n", ( u32 )info.freeram );
		printf( "shared ram: %lu\n", info.sharedram );
		printf( "buffer ram: %lu\n", info.bufferram );
		printf( "total swap: %lu\n", info.totalswap );
		printf( "free swap:  %lu\n", info.freeswap );
		printf( "process #:  %u\n", info.procs );
		printf( "-----------------------------------\n" );
# endif
	    return ( u32 )info.freeram;
    }
# else
	return 0;
# endif
}

void
Dump( void *data, u32 size )
{
	int i;
	u8 *src = ( u8 * )data;
	printf( "Dump: %d Bytes\n", size );
	for ( i = 0; i < size; i ++ ) {
		printf( "%02x ", src[ i ] );
		if ( ( i & 0x0F ) == 0x0F ) {
			printf( "\n" );
		}
	}
	printf( "\n" );
}

void
my_toupper( char *string )
{
	if ( string ) {
		while ( *string ) {
			*string	= toupper( *string );
			string ++;
		}
	}
}

void
my_tolower( char *string )
{
	if ( string ) {
		while ( *string ) {
			*string	= tolower( *string );
			string ++;
		}
	}
}
