/*!
 *  @file       Thread.c
 *  @version    1.0
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Static Param
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static int
create_thread( void* thrd, void *arg, u32 priority, u32 size )
{
	pthread_t thread_id;
	int res	= SUCCESS;

	pthread_attr_t      proc_thread_attr;

	pthread_attr_init( &proc_thread_attr );

	if ( size ) {
		char *new_stack = ( char * )malloc( size );

		if ( new_stack == NULL ) {
			pthread_attr_destroy( &proc_thread_attr );
			return FAILURE;
		}

		pthread_attr_setstack( &proc_thread_attr, new_stack, size );
	}

	proc_thread_attr.schedparam.sched_priority = priority;

	if ( pthread_create( &thread_id, &proc_thread_attr, thrd, arg ) != 0 ) {
		printf( "normal thread create error: %s\n", strerror( errno ) );
		res = FAILURE;
	}

	pthread_attr_destroy( &proc_thread_attr );

	pthread_detach( thread_id );

	return res;
}

//==========================================================================
// APIs
//==========================================================================
/*!
 * @brief   Create system thread
 *
 * @param[in]   thrd        pointer to thread entry
 * @param[in]   arg         pointer to argument
 * @param[in]   priority    thread priority, 0 = maximum
 *
 * @return  0 on success, otherwise < 0
 */
int
create_sys_thread( void *thrd, void *arg, u32 priority )
{
    pthread_t thread_id;
    int res	= SUCCESS;

    pthread_attr_t      proc_thread_attr;

    pthread_attr_init( &proc_thread_attr );

	proc_thread_attr.schedparam.sched_priority = priority;

    if ( pthread_create( &thread_id, &proc_thread_attr, thrd, arg ) != 0 ) {
//    if ( pthread_create( &thread_id, NULL, thrd, arg ) != 0 ) {
        printf( "normal thread create error: %s\n", strerror( errno ) );
        res = FAILURE;
    }

    pthread_attr_destroy( &proc_thread_attr );

    pthread_detach( thread_id );

    return res;
}

/*!
 * @brief   Create normal thread
 * @param[in]   thrd        pointer to thread entry
 * @param[in]   arg         pointer to argument
 * @param[in]   size        Stack Size
 * @return  0 on success, otherwise < 0
 */
int
create_norm_thread( void *thrd, void *arg, u32 size )
{
	return create_thread( thrd, arg, 13, size );
}
