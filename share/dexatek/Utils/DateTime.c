/*!
 *  @file       DateTime.c
 *  @version    1.0
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Static Param
//==========================================================================
# ifdef CLOCK_MONOTONIC
static struct timespec tm_start = { 0, 0 };
# else
static struct timeval tm_start = { 0, 0 };
# endif

# define TEST_SLEEP

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
void
MSleep( u32 millisec )
{
# ifdef TEST_SLEEP
	usleep( millisec * 1000 );
# else

//    struct timespec ms_delay;
//    ms_delay.tv_sec		= ( ( millisec ) >= 1000)? ( millisec ) / 1000: 0;
//    ms_delay.tv_nsec	= ( ( millisec ) % 1000) * 1000000;
//    nanosleep( &ms_delay, NULL );

	if ( millisec < 1000 ) {
		sleep( 1 );
	} else {
		sleep( ( millisec ) / 1000 );
	}
# endif
}

u32
seconds( void )
{
# ifdef CLOCK_MONOTONIC
	struct timespec curr_time;

	clock_gettime( CLOCK_MONOTONIC, &curr_time );

	return ( curr_time.tv_sec - tm_start.tv_sec );
# else
	struct timeval curr_time;

	gettimeofday( &curr_time, NULL );

	return ( curr_time.tv_sec - tm_start.tv_sec );
# endif
}

/*
 * @brief       Get ticks (in millisecond)
 *
 * @return      ticks since boot up
 */
u32
Ticks( void )
{
	return ( ticks * 10 );

# ifdef CLOCK_MONOTONIC
	struct timespec curr_time;

	clock_gettime( CLOCK_MONOTONIC, &curr_time );

	return ( ( ( curr_time.tv_sec - tm_start.tv_sec ) * 1000) + \
			( ( curr_time.tv_nsec - tm_start.tv_nsec ) / 1000000 ) );
# else
	struct timeval curr_time;

	gettimeofday( &curr_time, NULL );

	return ( ( ( curr_time.tv_sec - tm_start.tv_sec ) * 1000) + \
			( ( curr_time.tv_usec - tm_start.tv_usec ) / 1000 ) );
# endif
}

/*
 * @brief       Initialize the ticks
 *
 * @return      0 on success, otherwise < 0
 */
int
tick_init( void )
{
# ifdef CLOCK_MONOTONIC
	clock_gettime( CLOCK_MONOTONIC, &tm_start );
# else
	gettimeofday( &tm_start, NULL );
# endif

	return 0;
}

