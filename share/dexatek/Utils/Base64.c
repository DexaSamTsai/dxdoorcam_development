/*
 * Base64.c
 *
 *  Created on: 2017/08/21
 *      Author: Sam Tsai
 */

# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
/**
 * characters used for Base64 encoding
 */
static const char *cb64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

//==========================================================================
// Static Functions
//==========================================================================
/**
 * base64 encode
 */
static void
encodeblock( u8 in[ 3 ], char out[ 4 ], int len )
{
	out[ 0 ] = cb64[ in[ 0 ] >> 2 ];
	out[ 1 ] = cb64[ ( ( in[ 0 ] & 0x03 ) << 4 ) | ( ( in[ 1 ] & 0xf0 ) >> 4 ) ];
	out[ 2 ] = ( u8 )( len > 1? cb64[ ( ( in[ 1 ] & 0x0f ) << 2 ) | ( ( in[ 2 ] & 0xc0 ) >> 6 ) ]: '=' );
	out[ 3 ] = ( u8 )( len > 2? cb64[ in[ 2 ] & 0x3f ]: '=' );
}

/**
 * determine the value of a base64 encoding character
 *
 * @param base64char the character of which the value is searched
 * @return the value in case of success (0-63), -1 on failure
 */
static int 
base64_char_value( char base64char )
{
	if ( base64char >= 'A' && base64char <= 'Z' ) {
		return base64char - 'A';
	}
	
	if ( base64char >= 'a' && base64char <= 'z' ) {
		return base64char - 'a' + 26;
	}
	
	if ( base64char >= '0' && base64char <= '9' ) {
		return base64char - '0' + 2 * 26;
	}
	
	if ( base64char == '+' ) {
		return 2 * 26 + 10;
	}
	
	if ( base64char == '/' ) {
		return 2 * 26 + 11;
	}
	
	return -1;
}

/**
 * decode a 4 char base64 encoded byte triple
 *
 * @param quadruple the 4 characters that should be decoded
 * @param result the decoded data
 * @return lenth of the result (1, 2 or 3), 0 on failure
 */
static int 
base64_decode_triple( char quadruple[ 4 ], unsigned char *result )
{
	int i, triple_value, bytes_to_decode = 3, only_equals_yet = 1;
	int char_value[ 4 ];

	for ( i = 0; i < 4; i ++ ) {
		char_value[ i ] = base64_char_value( quadruple[ i ] );
	}

	/* check if the characters are valid */
	for ( i = 3; i >= 0; i -- ) {
		if ( char_value[ i ] < 0 ) {
			if ( only_equals_yet && quadruple[ i ] == '=' ) {
				/* we will ignore this character anyway, make it something
				 * that does not break our calculations */
				char_value[ i ] = 0;
				bytes_to_decode--;
				continue;
			}
			return 0;
		}
		/* after we got a real character, no other '=' are allowed anymore */
		only_equals_yet = 0;
	}

	/* if we got "====" as input, bytes_to_decode is -1 */
	if ( bytes_to_decode < 0 ) {
		bytes_to_decode = 0;
	}

	/* make one big value out of the partial values */
	triple_value = char_value[ 0 ];
	triple_value *= 64;
	triple_value += char_value[ 1 ];
	triple_value *= 64;
	triple_value += char_value[ 2 ];
	triple_value *= 64;
	triple_value += char_value[ 3 ];

	/* break the big value into bytes */
	for ( i = bytes_to_decode; i < 3; i ++ ) {
		triple_value /= 256;
	}
	
	for ( i = bytes_to_decode - 1; i >= 0; i -- ) {
		result[ i ] = triple_value % 256;
		triple_value /= 256;
	}

	return bytes_to_decode;
}

//==========================================================================
// APIs
//==========================================================================
/*
 * @param in:		the decode data (zero terminated)
 * @param out:		pointer to the target buffer
 * @param out_len:	length of the target buffer
 * @return length of converted data on success, -1 otherwise
 */
int
Decode64( char *in, u8 *out, int out_len )
{
	char *src, *tmpptr;
	char quadruple[ 4 ];
	u8 tmpresult[ 3 ];
	int i, tmplen = 3;
	int converted = 0;

	if ( in == NULL || out == NULL || out_len <= 0 ) {
		return 0;
	}

	memset( out, 0, out_len );

	/* concatinate '===' to the source to handle unpadded base64 data */
	src = ( char * )my_malloc( strlen( in ) + 5 );
	if ( src == NULL ) {
		return -1;
	}

	strcpy( src, in );
	strcat( src, "====" );
	tmpptr = src;

	/* convert as long as we get a full result */
	while ( tmplen == 3 ) {
		/* get 4 characters to convert */
		for ( i = 0; i < 4; i ++ ) {
			/* skip invalid characters - we won't reach the end */
			while ( ( *tmpptr != '=' ) && ( base64_char_value( *tmpptr ) < 0 ) ) {
				tmpptr++;
			}

			quadruple[ i ] = *( tmpptr++ );
		}

		/* convert the characters */
		tmplen = base64_decode_triple( quadruple, tmpresult );

		/* check if the fit in the result buffer */
		if ( out_len < tmplen ) {
			my_free( src );
			return -1;
		}

		/* put the partial result in the result buffer */
		memcpy( out, tmpresult, tmplen );
		out += tmplen;
		out_len -= tmplen;
		converted += tmplen;
	}

	my_free( src );

	return converted;
}

/*
 * @param in:		the encode data (zero terminated)
 * @param in_len:	length of the encode data
 * @param out:		pointer to the target buffer
 * @return length of converted data on success, -1 otherwise
 */
int
Encode64( u8 *in, int in_len, char *out )
{
	u8 src[ 3 ];
	int i;
	int len = ( in_len % 3 );
	char *dst = out;

	for ( i = 0; i < ( in_len / 3 ); i ++ ) {
		encodeblock( in, dst, 3 );
		in += 3;
		dst += 4;
	}

	if ( len != 0 ) {
		if ( len == 1 ) {
			src[ 0 ]	= in[ 0 ];
			src[ 1 ]	= 0;
			src[ 2 ]	= 0;
		} else { /* len == 2 */
			src[ 0 ]	= in[ 0 ];
			src[ 1 ]	= in[ 1 ];
			src[ 2 ]	= 0;
		}
		
		encodeblock( src, dst, len );
		dst += 4;
	}
	
	return ( dst - out );
}
