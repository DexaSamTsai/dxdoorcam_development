/*
 * Thread.h
 *
 *  Created on: 2017/08/08
 *      Author: Sam Tsai
 */

# ifndef __THREAD_H__
# define __THREAD_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Types.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
/*!
 * @brief   Create system thread
 *
 * @param[in]   thrd        pointer to thread entry
 * @param[in]   arg         pointer to argument
 * @param[in]   priority    thread priority, 0 = maximum
 *
 * @return  0 on success, otherwise < 0
 */
extern int
create_sys_thread( void* thrd, void *arg, u32 priority );

/*!
 * @brief   Create normal thread
 * @param[in]   thrd        pointer to thread entry
 * @param[in]   arg         pointer to argument
 * @param[in]   size        Stack Size
 * @return  0 on success, otherwise < 0
 */
extern int
create_norm_thread( void* thrd, void *arg, u32 size );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __THREAD_H__
