/*
 * Codec.c
 *
 *		Created on: 2017
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include <rtstream.h>
# include <rtsavapi.h>
# include <rtscamkit.h>
# include <rtsv4l2.h>
# include <rtsvideo.h>
# include <rtsaudio.h>
# include <rts_io_gpio.h>
# include "Headers.h"
# include "Camera.h"

# include <ft2build.h>
# include FT_FREETYPE_H


//==========================================================================
// Type Declaration
//==========================================================================
//# define PRINT_MD_DATA
//# define MD_CALLBACK
# define	AUDIO_OUT_RESAMPLE

# ifndef RTS_AUDIO_MAX_BUF_NUM
# define RTS_AUDIO_MAX_BUF_NUM			3
# endif

# define LIGTH_SENSOR_HOLD_DURATION		4

# define IMAGE_WIDTH_MAX				1920
# define IMAGE_HEIGHT_MAX				1080

# define MD_AREA_MAX					5
# define MD_GRID_WIDTH					16
# define MD_GRID_HEIGHT					18

# define RTS_AUDIO_DEV					"hw:0,1"
//# define RTS_AUDIO_DEV				"hw:0,0"

# define OSD_DISPALY_LENGTH_MAX			24
# define OSD_FONT_WIDTH_MAX				24
# define OSD_FONT_HEIGHT_MAX			24

# define OSD_FONT_PATH					"/usr/share/dexatek/simsun.ttc"

# define OSD_PIXEL_BYTES				4
# define OSD_WIDTH						( OSD_DISPALY_LENGTH_MAX * OSD_FONT_WIDTH_MAX )
# define OSD_WIDTH_PIXEL				( OSD_WIDTH * OSD_PIXEL_BYTES )
# define OSD_HEIGHT						OSD_FONT_HEIGHT_MAX
# define OSD_BUFFER_SIZE				( OSD_WIDTH * OSD_HEIGHT * OSD_PIXEL_BYTES )

enum {
	RTS_GPIO_GROUP_SYS		= 0,
	RTS_GPIO_GROUP_MCU,
};

# define RTS_GPIO_PIN_IR_LED					2
# 	define RTS_GPIO_PIN_IR_LED_ON				1
# 	define RTS_GPIO_PIN_IR_LED_OFF				0
# define RTS_GPIO_PIN_IR_CTRL_POWER				18
# 	define RTS_GPIO_PIN_IR_CTRL_POWER_ON		0
# 	define RTS_GPIO_PIN_IR_CTRL_POWER_OFF		1
# define RTS_GPIO_PIN_HW_CHECK					19
# define RTS_GPIO_PIN_IR_CTRL					3
# 	define RTS_GPIO_PIN_IR_CTRL_ON				0
# 	define RTS_GPIO_PIN_IR_CTRL_OFF				1
# define RTS_GPIO_PIN_AUDIO_OUT					5
# 	define RTS_GPIO_PIN_AUDIO_OUT_ON			0
# 	define RTS_GPIO_PIN_AUDIO_OUT_OFF			1

# define RTS_GPIO_MODE_INPUT					0
# define RTS_GPIO_MODE_OUTPUT					1

enum {
	CAMERA_OSD_DATE	= 0,
	CAMERA_OSD_TIME,
	CAMERA_OSD_STRING,
};

typedef struct {
	char			*name;
	u32				cmd_id;
	unsigned long	pos;
	int				max;
	int				min;
} ISPTable;

# define FIELD_OFFSET( field )		( ( unsigned long )&( ( ( ImageCtx * )0 )->field ) )

typedef struct __attribute__ ( ( packed ) ) {
	u8		r;
	u8		g;
	u8		b;
	u8		a;
} pixel_t;

typedef struct {
	BOOL					enable;
	u32						x0;
	u32						x1;
	u32						y0;
	u32						y1;
	u32						percentage;
} md_area;

typedef enum {
	CAMERA_VIDEO_CODEC_MJPEG	= 0,
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_MAX,
} CAMERA_VIDEO_CODEC;

typedef enum {
	CAMERA_AUDIO_CODEC_G711A	= 0,
	CAMERA_AUDIO_CODEC_G711U,
	CAMERA_AUDIO_CODEC_AAC,
	CAMERA_AUDIO_CODEC_PCM,
	CAMERA_AUDIO_CODEC_MAX,
	//CAMERA_AUDIO_CODEC_DEF	= CAMERA_AUDIO_CODEC_G711U,
	//CAMERA_AUDIO_CODEC_DEF	= CAMERA_AUDIO_CODEC_PCM,
	CAMERA_AUDIO_CODEC_DEF		= CAMERA_AUDIO_CODEC_AAC,
	//CAMERA_AUDIO_OUT_CODEC_DEF	= CAMERA_AUDIO_CODEC_G711U,
	CAMERA_AUDIO_OUT_CODEC_DEF	= CAMERA_AUDIO_CODEC_PCM,
} CAMERA_AUDIO_CODEC;

typedef struct {
	u32							num;

	char						play;
	char						stop;

	CAMERA_VIDEO_CODEC			video_codec;
	CAMERA_AUDIO_CODEC			audio_codec;

	int							src;
	int							stream;
	int							osd;
	int							aec;
	CodecCtx					codec_ctx;

	struct rts_video_osd2_attr	*osd_attr;
	u32							font_size;
} StreamCtx;

typedef struct {
	int						codec;
	int						stream;

	u32						chunk_bytes;

	struct rts_av_buffer	*buffer[ RTS_AUDIO_MAX_BUF_NUM ];
} DecodeAudioCtx;

typedef struct {
	int	( *set_video )			( StreamCtx *, CodecCtx * );
} CodecHandle;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static CAMERA_VIDEO_CODEC codec_map[ CAMERA_STREAM_VIDEO_MAX ]	= {
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_MJPEG,
};

static struct {
	StreamCtx			stream_ctxs[ CAMERA_STREAM_MAX ];
	DecodeAudioCtx		ao;
	ImageCtx			img_ctx;

	struct {
		char			show_date;
		char			show_time;
		char			show_string;
		u8				*string;

		u32				offset;
	} osd;

	struct {
		md_area						area[ MD_AREA_MAX ];

		struct rts_video_md_attr	*attr;
	} motion;

	struct {
		double	pcm_sum;
		u32		pcm_packet_cnt;
	} audio;

	Stream_CBFunc		stream_cb;
	MD_CBFunc			md_cb;
	Audio_CBFunc		audio_cb;

	FT_Library			library;
	FT_Face				face;

} codec;

static int				is_new_hw			= 0;
static u32				ir_hold_duration	= LIGTH_SENSOR_HOLD_DURATION;
static u32				ir_trigger_clock	= 0;
static u8				osd_image_8888[ OSD_BUFFER_SIZE ];
static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;

static struct {
	u16		rate;
	u16		channel;
	u16		sample;
	u32		rts_type;
} audio_attr[ CAMERA_AUDIO_CODEC_MAX ] = {
	{ 16000,	1, 16, RTS_AUDIO_TYPE_ID_ALAW },
	{ 16000,	1, 16, RTS_AUDIO_TYPE_ID_ULAW },
//	{ 48000,	2, 16, RTS_AUDIO_TYPE_ID_AAC },		// AAC
	{ 16000,	1, 16, RTS_AUDIO_TYPE_ID_AAC },		// AAC
	{ 16000,	1, 16, RTS_AUDIO_TYPE_ID_PCM },
};

static ISPTable ips_tbl[]	= {
	{ "Brightness",		RTS_VIDEO_CTRL_ID_BRIGHTNESS,		FIELD_OFFSET( brightness ), },
	{ "Contrast",		RTS_VIDEO_CTRL_ID_CONTRAST,			FIELD_OFFSET( contrast ), },
	{ "Hue",			RTS_VIDEO_CTRL_ID_HUE,				FIELD_OFFSET( hue ), },
	{ "Saturation",		RTS_VIDEO_CTRL_ID_SATURATION,		FIELD_OFFSET( saturation ), },
	{ "Sharpness",		RTS_VIDEO_CTRL_ID_SHARPNESS,		FIELD_OFFSET( sharpness ), },
	{ "Gamma",			RTS_VIDEO_CTRL_ID_GAMMA,			FIELD_OFFSET( gamma ), },

	{ "WBMode",			RTS_VIDEO_CTRL_ID_AWB_CTRL,			FIELD_OFFSET( awb.mode ), },
	{ "WBLevel",		RTS_VIDEO_CTRL_ID_WB_TEMPERATURE,	FIELD_OFFSET( awb.level ), },
	{ "RGain",			RTS_VIDEO_CTRL_ID_RED_BALANCE,		FIELD_OFFSET( awb.r_gain ), },
	{ "GGain",			RTS_VIDEO_CTRL_ID_GREEN_BALANCE,	FIELD_OFFSET( awb.g_gain ), },
	{ "BGain",			RTS_VIDEO_CTRL_ID_BLUE_BALANCE,		FIELD_OFFSET( awb.b_gain ), },

	{ "BLC",			RTS_VIDEO_CTRL_ID_BLC,				FIELD_OFFSET( blc ), },

	//{ "Gain",			RTS_VIDEO_CTRL_ID_GAIN,				FIELD_OFFSET( gain ), },

	{ "PowerFreq",		RTS_VIDEO_CTRL_ID_PWR_FREQUENCY,	FIELD_OFFSET( power_freq ), },

/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_AF, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_FOCUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_ZOOM, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_PAN, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_TILT, FIELD_OFFSET( brightness ), },
*/
	{ "Roll",			RTS_VIDEO_CTRL_ID_ROLL,				FIELD_OFFSET( roll ), },
	{ "Flip",			RTS_VIDEO_CTRL_ID_FLIP,				FIELD_OFFSET( flip ), },
	{ "Mirror",			RTS_VIDEO_CTRL_ID_MIRROR,			FIELD_OFFSET( mirror ), },
//	{ "Rotate",			RTS_VIDEO_CTRL_ID_ROTATE,			FIELD_OFFSET( rotate ), },

/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_ISP_SPECIAL_EFFECT, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_EV_COMPENSATE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_COLOR_TEMPERATURE_ESTIMATION, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AE_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AWB_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AF_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_LED_TOUCH_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_LED_FLASH_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_ISO, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_SCENE_MODE, FIELD_OFFSET( brightness ), },
*/
//	{ "ROI",			RTS_VIDEO_CTRL_ID_ROI_MODE,			FIELD_OFFSET( roi ), },
/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_3A_STATUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IDEA_EYE_SENSITIVITY, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IDEA_EYE_STATUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IEDA_EYE_MODE, FIELD_OFFSET( brightness ), },
*/
	//{ "GrayMode",		RTS_VIDEO_CTRL_ID_GRAY_MODE,		FIELD_OFFSET( gray_mode ), },

	{ "WDRMode",		RTS_VIDEO_CTRL_ID_WDR_MODE,			FIELD_OFFSET( wdr.mode ), },
	{ "WDRLevel",		RTS_VIDEO_CTRL_ID_WDR_LEVEL,		FIELD_OFFSET( wdr.level ), },

	{ "3DNR",			RTS_VIDEO_CTRL_ID_3DNR,				FIELD_OFFSET( dnr3 ), },
	{ "Dehaze",			RTS_VIDEO_CTRL_ID_DEHAZE,			FIELD_OFFSET( dehaze ), },
	{ "IRMode",			RTS_VIDEO_CTRL_ID_IR_MODE,			FIELD_OFFSET( ir_mode ), },
/*
	{ "Brightness", RTS_VIDEO_CRTL_ID_SMART_IR_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CRTL_ID_SMART_IR_MANUAL_LEVEL, FIELD_OFFSET( brightness ), },
*/

	{ "ExposureMode",	RTS_VIDEO_CTRL_ID_EXPOSURE_MODE,	FIELD_OFFSET( ae.mode ), },
	{ "AEGain",			RTS_VIDEO_CTRL_ID_AE_GAIN,			FIELD_OFFSET( ae.gain ), },
	{ "ExposureTime",	RTS_VIDEO_CTRL_ID_EXPOSURE_TIME,	FIELD_OFFSET( ae.exposure_time ), },
//	{ "Brightness", RTS_VIDEO_CTRL_ID_EXPOSURE_PRIORITY, FIELD_OFFSET( brightness ), },

	{ NULL,	0,	0, },
};

//==========================================================================
// Static Functions
//==========================================================================
# ifdef PRINT_MD_DATA
static void
md_print_data( struct rts_video_md_data *data, int cols, int rows )
{
	uint8_t *ptr;
	int i, j, index = 0;
	int x, y, val;
	uint8_t mask = 0;

	ptr		= data->vm_addr;

	switch ( data->bpp ) {
		case 1:
			mask = 0x01;
			break;

		case 2:
			mask = 0x03;
			break;

		case 4:
			mask = 0x0F;
			break;

		case 8:
			mask = 0xFF;
			break;
	}

	for ( j = 0; j < rows; j ++ ) {
		for ( i = 0; i < cols; i ++, index ++ ) {
			y = ( index * data->bpp ) % 8;
			x = ( index * data->bpp - y ) / 8;

			val = ( ptr[ x ] >> y ) & mask;

			printf( "%d", val );
		}

		printf( "\n" );
	}
}
# endif

static int
md_check_data_rltcur( struct rts_video_md_data *data, int cols, int rows )
{
	uint8_t *ptr	= data->vm_addr;
	int i, j, index = 0, mid, total, percentage;
	int x, y, cnt = 0;

	if ( data->bpp > 1 ) {
		return 0;
	}

	for ( mid = 0; mid < MD_AREA_MAX; mid ++ ) {
		if ( codec.motion.area[ mid ].enable ) {
			total	= 0;
			cnt		= 0;

			for ( j = codec.motion.area[ mid ].y0; j < codec.motion.area[ mid ].y1; j ++ ) {
				for ( i = codec.motion.area[ mid ].x0; i < codec.motion.area[ mid ].x1; i ++ ) {
					index = j * cols + i;

					x = index >> 3;
					y = index & 0x07;

					if ( ptr[ x ] & ( 0x01 << y ) ) {
						cnt ++;
					}

					total ++;
				}
			}

			percentage = ( cnt * 100 ) / total;
dbg_warm( "cnt: %d, total: %d, percentage: %d, ticks(): %u", cnt, total, percentage, ticks() );
			if ( percentage >= codec.motion.area[ mid ].percentage ) {
				codec.md_cb( mid, NULL, 0 );
			}
		}
	}

	return cnt;
}

static u32 md_cnt = 0;
static u8 md_temp[ 7200 ] = { 0, };
static int
md_check_data_avgy( struct rts_video_md_data *data, int cols, int rows )
{
	uint8_t *ptr	= data->vm_addr, diff;
	int i, j, index = 0, mid, total, percentage;
	int cnt = 0;

	if ( md_cnt == 0 ) {
		memcpy( md_temp, ptr, 7200 );
		return 0;
	}

	for ( mid = 0; mid < MD_AREA_MAX; mid ++ ) {
		if ( codec.motion.area[ mid ].enable ) {
			total	= 0;
			cnt		= 0;
			index	= 0;

			for ( j = codec.motion.area[ mid ].y0; j < codec.motion.area[ mid ].y1; j ++ ) {
				for ( i = codec.motion.area[ mid ].x0; i < codec.motion.area[ mid ].x1; i ++ ) {
					index = j * cols + i;

					if ( ptr[ index ] > md_temp[ index ] ) {
						diff = ptr[ index ] - md_temp[ index ];
					} else {
						diff = md_temp[ index ] - ptr[ index ];
					}

					if ( diff > 3 ) {
						//printf( " " );
						cnt ++;
					} else {
						//printf( "0" );
					}

					total ++;
				}
				//printf( "\n" );
			}
			//printf( "\n\n" );

			percentage = ( cnt * 100 ) / total;
			//dbg_warm( "cnt: %d, total: %d, percentage: %d", cnt, total, percentage );
			if ( percentage >= codec.motion.area[ mid ].percentage ) {
				codec.md_cb( mid, NULL, 0 );
			}
		}
	}

	memcpy( md_temp, ptr, 7200 );

	return cnt;
}

static int
md_process_data( struct rts_video_md_result *result, int cols, int rows )
{
	unsigned int i;

	for ( i = 0; i < result->count; i ++ ) {
		if ( ( result->results[ i ].type == RTS_VIDEO_MD_DATA_TYPE_RLTCUR ) && result->results[ i ].data ) {
# ifdef PRINT_MD_DATA
			md_print_data( result->results[ i ].data, cols, rows );
# endif
			md_check_data_rltcur( result->results[ i ].data, cols, rows );
		} else if ( ( result->results[ i ].type == RTS_VIDEO_MD_DATA_TYPE_AVGY ) && result->results[ i ].data ) {
# ifdef PRINT_MD_DATA
			md_print_data( result->results[ i ].data, cols, rows );
# endif
			md_check_data_avgy( result->results[ i ].data, cols, rows );
		}
	}

	md_cnt ++;

	return 0;
}

static int
md_received( int idx, struct rts_video_md_result *result, void *priv )
{
	if ( ( result == NULL ) || ( result->results == NULL ) ) {
		return RTS_RETURN( RTS_E_NULL_POINT );
	} else if ( priv == NULL ) {
		return RTS_RETURN( RTS_E_NULL_POINT );
	} else if ( seconds() < ( ir_trigger_clock + 10 ) ) {
		return RTS_OK;
	} else if ( codec.md_cb ) {
		struct rts_video_md_block	*pblock = ( struct rts_video_md_block * )priv;

		md_process_data( result, pblock->area.size.columns, pblock->area.size.rows );
	}

	return RTS_OK;
}

# ifndef MD_CALLBACK
static void
md_check_status()
{
	struct rts_video_md_result		result;
	struct rts_video_md_attr		*attr = codec.motion.attr;
	struct rts_video_md_block		*pblock = NULL;

	int mid;

	if ( attr ) {
		for ( mid = 0; mid < attr->number; mid ++ ) {
			pblock = &attr->blocks[ mid ];

			if ( pblock->enable == FALSE ) {
				continue;
			}

			rts_av_init_md_result( &result, pblock->data_mode_mask );

			rts_av_get_isp_md_result( attr, mid, &result );

			if ( codec.md_cb ) {
				md_process_data( &result, pblock->area.size.columns, pblock->area.size.rows );
			}

			rts_av_uninit_md_result( &result );
		}
	}
}
# endif

static int
md_init()
{
	int ret	= SUCCESS;

	if ( codec.motion.attr ) {
		return SUCCESS;
	} else if ( rts_av_query_isp_md( &codec.motion.attr, IMAGE_WIDTH_MAX, IMAGE_HEIGHT_MAX ) ) {
		return FAILURE;
	} else if ( codec.motion.attr->number == 0 ) {
		goto err;
	} else {
		struct rts_video_md_block *pblock = &codec.motion.attr->blocks[ 0 ];

		if ( pblock->area.bitmap.vm_addr ) {
			memset( pblock->area.bitmap.vm_addr, 0xFF, pblock->area.bitmap.length );
		} else {
			dbg_warm( "pblock->area.bitmap.vm_addr == NULL!!!" );
			goto err;
		}

		pblock->enable				= 1;

		pblock->detect_mode			= RTS_VIDEO_MD_DETECT_HW;

# ifdef MD_CALLBACK
		pblock->data_mode_mask		= RTS_VIDEO_MD_DATA_TYPE_RLTCUR;
# else
		pblock->data_mode_mask		= RTS_VIDEO_MD_DATA_TYPE_AVGY;
# endif
/*
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_AVGY;
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_RLTPRE;
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_RLTCUR;
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_BACKY;
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_BACKF;
		pblock->data_mode_mask		|= RTS_VIDEO_MD_DATA_TYPE_BACKC;
*/
		pblock->area.cell.width		= MD_GRID_WIDTH;
		pblock->area.cell.height	= MD_GRID_HEIGHT;
		pblock->area.start.x		= 0;
		pblock->area.start.y		= 0;
		pblock->area.size.columns	= IMAGE_WIDTH_MAX / MD_GRID_WIDTH;
		pblock->area.size.rows		= IMAGE_HEIGHT_MAX / MD_GRID_HEIGHT;

# ifdef MD_CALLBACK
		pblock->ops.motion_received	= md_received;
		pblock->ops.priv			= pblock;
		pblock->sensitivity			= CAMERA_MD_SENSITIVITY;
# else
		pblock->sensitivity			= 50;
# endif

		pblock->percentage			= 0;
		pblock->frame_interval		= 5;

		if ( ( ret = rts_av_set_isp_md( codec.motion.attr ) ) ) {
			dbg_warm( "Do rts_av_set_isp_md Failed( %d )!!!", ret );
			return FAILURE;
		}

		return SUCCESS;
	}

err:
	rts_av_release_isp_md( codec.motion.attr );
	codec.motion.attr = NULL;

	return FAILURE;
}

static int
osd_draw_font( u32 font_size )
{
	int row, pixel;
	u8 alpha;

	u8 *start		= osd_image_8888 + ( codec.osd.offset * OSD_PIXEL_BYTES );
	int	top			= font_size - codec.face->glyph->bitmap_top;
	int left		= codec.face->glyph->bitmap_left;
	int pitch		= codec.face->glyph->bitmap.pitch;
	int	max_width	= left + codec.face->glyph->bitmap.width;
	int	max_row		= top + codec.face->glyph->bitmap.rows;
	u8	*bmp		= codec.face->glyph->bitmap.buffer;

	pixel_t		*p;

	if ( max_row > OSD_HEIGHT ) {
		max_row	= OSD_HEIGHT;
	}

	start += ( top * OSD_WIDTH_PIXEL );

	for ( row = top; row < max_row; row ++ ) {
		p = ( pixel_t * )start;
		p += left;

		for ( pixel = left; pixel < max_width; pixel ++, p ++ ) {
			alpha = bmp[ pixel - left ];
			if ( alpha ) {			// RGBA Color
				p->a	= alpha;
			}
		}

		bmp += pitch;

		start += OSD_WIDTH_PIXEL;
	}

	return max_width + 1;
}

static int
osd_generate_font( u16 ucode, u32 font_size )
{
	FT_UInt glyph_index;

	if ( ( ucode >= 0x0590 ) && ( ucode <= 0x05FF ) ) {
		// Hebrew:		0x0590 ~ 0x05FF
	} else if ( ( ucode >= 0x0E00 ) && ( ucode <= 0x0E5F ) ) {
		// Thailand:	0x0E00 ~ 0x0E5F
	}

	if ( FT_Set_Pixel_Sizes( codec.face, font_size, 0 ) ) {
		return FAILURE;
	}

	glyph_index = FT_Get_Char_Index( codec.face, ucode );
	if ( glyph_index == 0 ) {
		return FAILURE;
	}

	if ( FT_Load_Glyph( codec.face, glyph_index, FT_LOAD_DEFAULT ) ) {
		return FAILURE;
	}

	codec.face->glyph->format = FT_GLYPH_FORMAT_OUTLINE;
	FT_Outline_Embolden( &codec.face->glyph->outline, ( font_size << 1 ) );

	if ( FT_Render_Glyph( codec.face->glyph, FT_RENDER_MODE_NORMAL ) ) {
		return FAILURE;
	}

	return SUCCESS;
}

static void
osd_draw_text( char *text, int font_size )
{
	char *src = text;

	if ( codec.osd.offset >= OSD_WIDTH ) {
		return;
	}

	while ( *src ) {
		if ( osd_generate_font( *src, font_size ) == SUCCESS ) {
			if ( ( codec.face->glyph->bitmap_left + codec.face->glyph->bitmap.width + codec.osd.offset ) > OSD_WIDTH ) {
				codec.osd.offset = OSD_WIDTH;
				break;
			} else {
				codec.osd.offset += osd_draw_font( font_size );
			}
		} else {
			codec.osd.offset += ( font_size >> 1 );
		}
		src ++;
	}
}

static int
osd_show_info( StreamCtx *stream, time_t curr_time )
{
	char temp[ 24 ];

	struct rts_video_osd2_block	*pblock = &stream->osd_attr->blocks[ 0 ];

    struct tm t;

	localtime_r( &curr_time, &t );

	memset( osd_image_8888, 0x00, OSD_BUFFER_SIZE );

	codec.osd.offset	= 0;

	if ( codec.osd.show_date ) {
        sprintf( temp, "%4u-%02u-%02u", t.tm_year + 1900, t.tm_mon + 1, t.tm_mday );
		osd_draw_text( temp, stream->font_size );
		codec.osd.offset += ( stream->font_size / 2 );
	}

	if ( codec.osd.show_time ) {
        sprintf( temp, "%02u:%02u:%02u", t.tm_hour, t.tm_min, t.tm_sec );
		osd_draw_text( temp, stream->font_size );
		codec.osd.offset += ( stream->font_size / 2 );
	}

	if ( codec.osd.show_string && codec.osd.string ) {
		osd_draw_text( ( char * )codec.osd.string, stream->font_size );
	}

	pblock->picture.length		= ( OSD_WIDTH_PIXEL * stream->font_size );
	pblock->picture.pdata		= osd_image_8888;
	pblock->picture.pixel_fmt	= RTS_OSD2_BLK_FMT_RGBA8888;

	pblock->rect.left			= 16;
	pblock->rect.top			= 16;

	pblock->rect.right			= OSD_WIDTH + 16;
	pblock->rect.bottom			= stream->font_size + 16;

	pblock->enable				= 1;

	if ( rts_av_set_osd2_single( stream->osd_attr, 0 ) ) {
		dbg_warm( "Do rts_av_set_osd2_single Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static int
osd_hide( struct rts_video_osd2_attr *attr )
{
	struct rts_video_osd2_block	*pblock = &attr->blocks[ 0 ];

	pblock->picture.length		= 0;
	pblock->picture.pdata		= NULL;
	pblock->enable				= 0;

	if ( rts_av_set_osd2_single( attr, 0 ) ) {
		dbg_warm( "Do rts_av_set_osd2_single Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static void
osd_update( time_t curr_time )
{
	int i, res;

	StreamCtx *stream;

	for ( i = 0; i < CAMERA_STREAM_VIDEO_MAX; i ++ ) {
		stream	= &codec.stream_ctxs[ i ];

		if ( stream->osd < 0 ) {
			continue;
		} else {
			if ( !( codec.osd.show_date || codec.osd.show_time || codec.osd.show_string ) ) {
				if ( stream->osd_attr ) {
					osd_hide( stream->osd_attr );
				}
			} else {
				if ( stream->osd_attr == NULL ) {
					if ( ( res = rts_av_query_osd2( stream->osd, &stream->osd_attr ) ) ) {
						dbg_line( "Do rts_av_query_osd2 Failed( %d )!!!", res );
						continue;
					}
				}

				if ( stream->osd_attr->number && codec.face ) {
					osd_show_info( stream, curr_time );
				}
			}
		}
	}
}

static int
light_sensor_read_adc()
{
	char *node = "/sys/devices/platform/rts_saradc.0/in0_input";
	int fd = open( node, O_RDONLY );

	if ( fd > 0 ) {
		char	data[ 16 ];
		int		res = read( fd, data, 16 );

		close( fd );

		if ( res > 0 ) {
			return atoi( data );
		} else {
			return -1;
		}
	} else {
		dbg_line( "Open /sys/devices/platform/rts_saradc.0/in0_input Failed!!!" );
		return -1;
	}
}

static int
ir_cut_enable( char on )
{
	struct rts_gpio				*rts_gpio;

# if 0
	struct rts_video_control	ctrl;
	int ret;

	uint32_t id = RTS_VIDEO_CTRL_ID_IR_MODE;

	if ( on ) {
		dbg_line( "ir_cut_enable ON" );
		ctrl.current_value = 0;
	} else {
		dbg_line( "ir_cut_enable OFF" );
		ctrl.current_value = 2;
	}

	ret = rts_av_set_isp_ctrl( id, &ctrl );
	if ( ret ) {
		dbg_line( "set RTS_VIDEO_CTRL_ID_IR_MODE[%d] fail, ret = %d\n", ctrl.current_value, ret );
		return ret;
	}
# endif

	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_IR_CTRL_POWER );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, RTS_GPIO_PIN_IR_CTRL_POWER_ON );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_MCU, RTS_GPIO_PIN_IR_CTRL );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, on? RTS_GPIO_PIN_IR_CTRL_ON: RTS_GPIO_PIN_IR_CTRL_OFF );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_IR_LED );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, on? RTS_GPIO_PIN_IR_LED_ON: RTS_GPIO_PIN_IR_LED_OFF );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	sleep( 1 );
	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_IR_CTRL_POWER );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, RTS_GPIO_PIN_IR_CTRL_POWER_OFF );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	return 0;
}

static void
img_print( ImageCtx *ctx )
{
	dbg_line( "brightness: %d", ctx->brightness );
	dbg_line( "contrast: %d", ctx->contrast );
	dbg_line( "hue: %d", ctx->hue );
	dbg_line( "saturation: %d", ctx->saturation );
	dbg_line( "sharpness: %d", ctx->sharpness );
	dbg_line( "gamma: %d", ctx->gamma );

	dbg_line( "blc: %d", ctx->blc );
	dbg_line( "gain: %d", ctx->gain );
	dbg_line( "roll: %d", ctx->roll );
	dbg_line( "power_freq: %d", ctx->power_freq );

	dbg_line( "flip: %d", ctx->flip );
	dbg_line( "mirror: %d", ctx->mirror );
	dbg_line( "rotate: %d", ctx->rotate );

	dbg_line( "gray_mode: %d", ctx->gray_mode );

	dbg_line( "awb:" );
	dbg_line( "\tmode: %d", ctx->awb.mode );
	dbg_line( "\tlevel: %d", ctx->awb.level );
	dbg_line( "\tr_gain: %d", ctx->awb.r_gain );
	dbg_line( "\tg_gain: %d", ctx->awb.g_gain );
	dbg_line( "\tb_gain: %d", ctx->awb.b_gain );

	dbg_line( "wdr:" );
	dbg_line( "\tmode: %d", ctx->wdr.mode );
	dbg_line( "\tlevel: %d", ctx->wdr.level );

	dbg_line( "exposure:" );
	dbg_line( "\tmode: %d", ctx->ae.mode );
	dbg_line( "\tgain: %d", ctx->ae.gain );
	dbg_line( "\ttime: %d", ctx->ae.exposure_time );

	dbg_line( "dnr3: %d", ctx->dnr3 );
	dbg_line( "dehaze: %d", ctx->dehaze );
	dbg_line( "ir_mode: %d", ctx->ir_mode );
}

static int
img_get_default( u32 cmd_id, int *max, int *min )
{
	struct rts_video_control ctrl;

	if ( rts_av_get_isp_ctrl( cmd_id, &ctrl ) ) {
		return FAILURE;
	} else {
		*max	= ctrl.maximum;
		*min	= ctrl.minimum;
		return ctrl.default_value;
	}
}

static int
img_set_value( u32 cmd_id, int value )
{
	struct rts_video_control ctrl;

	if ( rts_av_get_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_av_get_isp_ctrl Failed!!!" );
		return FAILURE;
	}

	if ( ( value <= ctrl.maximum ) && ( value >= ctrl.minimum ) ) {
		ctrl.current_value = value;
	} else {
		dbg_line( "value( %d ): %d ~ %d", value, ctrl.minimum, ctrl.maximum );
		return FAILURE;
	}

	if ( rts_av_set_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_av_set_isp_ctrl Failed!!!" );
		return FAILURE;
	} else if ( rts_av_get_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_av_get_isp_ctrl Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static int
img_set_gray_mode( char on )
{
	struct rts_video_control ctrl;
	uint32_t id = RTS_VIDEO_CTRL_ID_GRAY_MODE;
	int ret;

	if ( on ) {
		ctrl.current_value = 1;
	} else {
		ctrl.current_value = 0;
	}

	ret = rts_av_set_isp_ctrl( id, &ctrl );
	if ( ret ) {
		dbg_line( "set gray mode[%d] fail, ret = %d\n", ctrl.current_value, ret );
		return ret;
	}

	return 0;
}

static int
stream_proc( void *data )
{
	int			i, cnt;
	AVFrame		frame;

	struct rts_av_buffer *buffer	= NULL;

	while ( 1 ) {
		cnt = 0;

		for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
			if ( !codec.stream_ctxs[ i ].play ) {
				continue;
			} else if ( codec.stream_ctxs[ i ].stop ) {
				codec.stream_ctxs[ i ].play	= FALSE;
				codec.stream_ctxs[ i ].stop	= FALSE;
				continue;
			} else if ( codec.stream_ctxs[ i ].stream < 0 ) {
				continue;
			} else if ( rts_av_poll( codec.stream_ctxs[ i ].stream ) ) {
				continue;
			} else if ( rts_av_recv( codec.stream_ctxs[ i ].stream, &buffer ) == 0 ) {
				if ( codec.stream_cb ) {
					frame.buff	= buffer->vm_addr;
					frame.size	= buffer->bytesused;

					if ( i == CAMERA_STREAM_AUDIO ) {
						switch ( codec.stream_ctxs[ i ].audio_codec ) {
							case CAMERA_AUDIO_CODEC_G711A:
								frame.type	= FRAME_AUDIO_PCMA;
								break;

							case CAMERA_AUDIO_CODEC_G711U:
								frame.type	= FRAME_AUDIO_PCMU;
								break;

							case CAMERA_AUDIO_CODEC_AAC:
								frame.type	= FRAME_AUDIO_AAC;
								break;

							case CAMERA_AUDIO_CODEC_PCM:
								frame.type	= FRAME_AUDIO_PCM;
								break;

							default:
								dbg_warm( "Un-know Audio Type( %d )...", codec.stream_ctxs[ i ].audio_codec );
						}
					} else {
						if ( codec.stream_ctxs[ i ].video_codec == CAMERA_VIDEO_CODEC_H264 ) {
							if ( buffer->flags & RTSTREAM_PKT_FLAG_KEY ) {
								frame.type	= FRAME_VIDEO_H264_I;
								frame.iskey	= TRUE;
							} else {
								frame.type	= FRAME_VIDEO_H264_P;
								frame.iskey	= FALSE;
							}
						} else {
							frame.type	= FRAME_VIDEO_MJPEG;
							frame.iskey	= TRUE;
						}
					}

					codec.stream_cb( i, &frame );
				}

				rts_av_put_buffer( buffer );
				buffer	= NULL;
				cnt ++;
			}
		}

		if ( cnt == 0 ) {
			msleep( 30 );
		}
	}

	return 0;
}

static void
stream_close( StreamCtx *stream )
{
	pthread_mutex_lock( &mutex );

	if ( stream->src >= 0 ) {
		if ( stream->stream >= 0 ) {
			rts_av_stop_recv( stream->stream );
			rts_av_disable_chn( stream->stream );
			rts_av_destroy_chn( stream->stream );
			stream->stream	= -1;
		}

		if ( stream->osd >= 0 ) {
			if ( stream->osd_attr ) {
				rts_av_release_osd2( stream->osd_attr );
				stream->osd_attr = NULL;
			}

			rts_av_destroy_chn( stream->osd );
			stream->osd	= -1;
		}

		if ( stream->aec >= 0 ) {
			rts_av_destroy_chn( stream->aec );
			stream->aec	= -1;
		}

		rts_av_destroy_chn( stream->src );
		stream->src	= -1;
	}

	pthread_mutex_unlock( &mutex );
}

static void
stream_stop( StreamCtx *stream )
{
	if ( stream->src >= 0 ) {
		if ( stream->stream >= 0 ) {
			stream->stop	= TRUE;

			while ( stream->play ) {
				msleep( 100 );
			}
		}

		stream_close( stream );
	}
}

static int
stream_start( StreamCtx *stream )
{
	if ( stream->stream >= 0 ) {
		int ret = rts_av_start_recv( stream->stream );
		if ( ret ) {
			dbg_warm( "Do rts_av_start_recv Failed( %d )!!!", ret );
			return FAILURE;
		}

		stream->play	= 1;
		return SUCCESS;
	} else {
		return FAILURE;
	}
}

//# define CODEC_VBR
//# define CODEC_CBR
# define CODEC_CVBR

static int
video_set_h264( StreamCtx *stream, CodecCtx *ctx )
{
	struct rts_isp_attr			isp_attr;
	struct rts_av_profile		profile;
	struct rts_h264_attr		cfg;
	struct rts_video_h264_ctrl	*ctrl;

	int res;

	memset( &cfg, 0, sizeof( struct rts_h264_attr ) );

	isp_attr.isp_id			= stream->num;
	isp_attr.isp_buf_num	= 3;

	stream->src = rts_av_create_isp_chn( &isp_attr );
	if ( stream->src < 0 ) {
		dbg_warm( "Do rts_av_create_isp_chn Failed !!!" );
		return FAILURE;
	}

	profile.fmt					= RTS_V_FMT_YUV420SEMIPLANAR;
	profile.video.width			= ctx->width;
	profile.video.height		= ctx->height;
	profile.video.numerator		= 1;
	profile.video.denominator	= ctx->fps;
	if ( ( res = rts_av_set_profile( stream->src, &profile ) ) ) {
		dbg_warm( "Do rts_av_set_profile Failed( %d )!!!", res );
		goto err;
	}

	rts_av_enable_chn( stream->src );

	stream->osd = rts_av_create_osd_chn();
	if ( stream->osd < 0 ) {
		dbg_warm( "Do rts_av_create_osd_chn Failed!!!" );
		goto err;
	}

	cfg.level			= H264_LEVEL_5_1;
	cfg.qp				= -1;
	cfg.bps				= ctx->kbps * 1024;
	cfg.gop				= ctx->fps;
	cfg.videostab		= 0;
	cfg.rotation		= rts_av_rotation_0;

	stream->stream = rts_av_create_h264_chn( &cfg );
	if ( stream->stream < 0 ) {
		dbg_warm( "Do rts_av_create_h264_chn Failed!!!" );
		goto err;
	}

	if ( rts_av_bind( stream->src, stream->osd ) ) {
		dbg_warm( "Do rts_av_bind( stream->src, stream->osd ) Failed!!!" );
		goto err;
	} else if ( rts_av_bind( stream->osd, stream->stream ) ) {
		dbg_warm( "Do  rts_av_bind( stream->osd, stream->stream ) Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->osd ) ) {
		dbg_warm( "Do rts_av_enable_chn( stream->osd ) Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->stream ) ) {
		dbg_warm( "Do rts_av_enable_chn( stream->stream ) Failed!!!" );
		goto err;
	} else if ( rts_av_query_h264_ctrl( stream->stream, &ctrl ) ) {
		dbg_warm( "Do rts_av_query_h264_ctrl Failed!!!" );
		goto err;
	} else {
# ifdef CODEC_VBR
		// VBR
		ctrl->bitrate_mode	= RTS_BITRATE_MODE_VBR;
		/* (high) 30 to 50 (low) */
		ctrl->qp			=
		ctrl->max_qp		=
		ctrl->min_qp		= 30;
# elif defined CODEC_CVBR
		// CVBR
		ctrl->bitrate_mode	= RTS_BITRATE_MODE_C_VBR;

		ctrl->max_bitrate	= ctx->kbps * 1024;
		ctrl->min_bitrate	= ctx->kbps * 512;
# else
		// CBR
		ctrl->bitrate_mode	= RTS_BITRATE_MODE_CBR;
		ctrl->bitrate		= ctx->kbps * 1024;
# endif
		if ( rts_av_set_h264_ctrl( ctrl ) ) {
			dbg_warm( "Do rts_av_query_h264_ctrl Failed!!!" );
			goto err;
		}

		rts_av_release_h264_ctrl( ctrl );
	}

	dbg_line( "cfg.isp_cfg.isp_id: %d, %dx%d", stream->num, ctx->width, ctx->height );

	return SUCCESS;

err:
	stream_close( stream );

	return FAILURE;
}

static int
video_set_mjpeg( StreamCtx *stream, CodecCtx *ctx )
{
	struct rts_isp_attr			isp_attr;
	struct rts_av_profile		profile;
	struct rts_jpgenc_attr		cfg;
	struct rts_video_mjpeg_ctrl	*ctrl;

	int res;

	memset( &cfg, 0, sizeof( struct rts_jpgenc_attr ) );

	isp_attr.isp_id			= stream->num;
	isp_attr.isp_buf_num	= 3;

	stream->src = rts_av_create_isp_chn( &isp_attr );
	if ( stream->src < 0 ) {
		dbg_warm( "Do rts_av_create_isp_chn Failed !!!" );
		return FAILURE;
	}

	profile.fmt					= RTS_V_FMT_YUV420SEMIPLANAR;
	profile.video.width			= ctx->width;
	profile.video.height		= ctx->height;
	profile.video.numerator		= 1;
	profile.video.denominator	= 1;
	if ( ( res = rts_av_set_profile( stream->src, &profile ) ) ) {
		dbg_warm( "Do rts_av_set_profile Failed( %d )!!!", res );
		goto err;
	}

	rts_av_enable_chn( stream->src );

	stream->osd = rts_av_create_osd_chn();
	if ( stream->osd < 0 ) {
		dbg_warm( "Do rts_av_create_osd_chn Failed!!!" );
		goto err;
	}

	cfg.rotation	= rts_av_rotation_0;
	stream->stream	= rts_av_create_mjpeg_chn( &cfg );
	if ( stream->stream < 0 ) {
		dbg_warm( "Do rts_av_create_mjpeg_chn Failed!!!" );
		goto err;
	} else if ( rts_av_bind( stream->src, stream->osd ) ) {
		dbg_warm( "Do rts_av_bind Failed!!!" );
		goto err;
	} else if ( rts_av_bind( stream->osd, stream->stream ) ) {
		dbg_warm( "Do  rts_av_bind( stream->osd, stream->stream ) Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->osd ) ) {
		dbg_warm( "Do rts_av_enable_chn( stream->osd ) Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->stream ) ) {
		dbg_warm( "Do rts_av_enable_chn Failed!!!" );
		goto err;
	} else if ( rts_av_query_mjpeg_ctrl( stream->stream, &ctrl ) ) {
		dbg_warm( "Do rts_av_query_h264_ctrl Failed!!!" );
		goto err;
	} else {
		ctrl->normal_compress_rate = 10;

		if ( rts_av_set_mjpeg_ctrl( ctrl ) ) {
			dbg_warm( "Do rts_av_set_mjpeg_ctrl Failed!!!" );
			goto err;
		}

		rts_av_release_mjpeg_ctrl( ctrl );
	}

	dbg_line( "cfg.isp_cfg.isp_id: %d, %dx%d", stream->num, ctx->width, ctx->height );

	return SUCCESS;

err:
	stream_close( stream );

	return FAILURE;
}

static CodecHandle codec_handle[ CAMERA_VIDEO_CODEC_MAX ]	= {
	{ video_set_mjpeg, },
	{ video_set_h264, },
};

static void
audio_pcm_cb( void *priv, struct rts_av_profile *profile, struct rts_av_buffer *buffer )
{
	if ( buffer->vm_addr && buffer->bytesused ) {
		short *src = ( short * )buffer->vm_addr, *end = src + ( buffer->bytesused / 2 );

		codec.audio.pcm_packet_cnt += ( buffer->bytesused / 2 );

		while ( src < end ) {
			codec.audio.pcm_sum += ( src[ 0 ] * src[ 0 ] );
			src ++;
		}

		if ( codec.audio.pcm_packet_cnt >= 20480 ) {
			u32 result = 10 * log10( ( codec.audio.pcm_sum / codec.audio.pcm_packet_cnt ) );

			codec.audio.pcm_packet_cnt	= 0;
			codec.audio.pcm_sum			= 0;

			if ( codec.audio_cb ) {
				codec.audio_cb( result );
			}
		}
	}
}

static void
audio_dec_cb( void *priv, struct rts_av_profile *profile, struct rts_av_buffer *buffer )
{
	if ( buffer->vm_addr && buffer->bytesused ) {
		buffer->timestamp = 0;
		rts_av_send( codec.ao.stream, buffer );
	}
}

static int
audio_create_stream( StreamCtx *stream, CAMERA_AUDIO_CODEC acodec )
{
	struct rts_audio_attr cfg;
	int res;
	struct rts_av_callback cb;

	struct rts_aec_control *aec_ctrl;

	memset( &cfg, 0, sizeof( struct rts_audio_attr ) );

	snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );

	cfg.rate		= audio_attr[ acodec ].rate;
	cfg.format		= audio_attr[ acodec ].sample;
	cfg.channels	= audio_attr[ acodec ].channel;

	cfg.rate		= 16000;
	cfg.format		= 16;
	cfg.channels	= 1;

	stream->src = rts_av_create_audio_capture_chn( &cfg );
	if ( stream->src < 0 ) {
		dbg_warm( "Do rts_av_create_audio_capture_chn Failed !!!" );
		return FAILURE;
	}

	cb.func		= audio_pcm_cb;
	cb.start	= 0;
	cb.times	= -1;
	cb.interval = 0;
	cb.type		= RTS_AV_CB_TYPE_ASYNC;

	if ( rts_av_set_callback( stream->src, &cb, 0 ) ) {
		dbg_warm( "Do rts_av_set_callback Failed!!!" );
	}

	stream->aec = rts_av_create_audio_aec_chn();
	if ( RTS_IS_ERR( stream->aec ) ) {
		dbg_warm( "Do rts_av_create_audio_capture_chn Failed !!!" );
		return FAILURE;
	}

	stream->stream = rts_av_create_audio_encode_chn( audio_attr[ acodec ].rts_type, 48000 );

	if ( stream->stream < 0 ) {
		dbg_warm( "Do rts_av_create_audio_encode_chn Failed!!!" );
		goto err;
	} else if ( ( res = rts_av_bind( stream->src, stream->aec ) ) ) {
		dbg_warm( "Do rts_av_bind Failed( %d )!!!", res );
		goto err;
	} else if ( ( res = rts_av_bind( codec.ao.stream, stream->aec ) ) ) {
		dbg_warm( "Do rts_av_bind Failed( %d )!!!", res );
		goto err;
	} else if ( ( res = rts_av_bind( stream->aec, stream->stream ) ) ) {
		dbg_warm( "Do rts_av_bind Failed( %d )!!!", res );
		goto err;
	} else if ( rts_av_enable_chn( stream->src ) ) {
		dbg_warm( "Do rts_av_enable_chn Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->aec ) ) {
		dbg_warm( "Do rts_av_enable_chn Failed!!!" );
		goto err;
	} else if ( rts_av_enable_chn( stream->stream ) ) {
		dbg_warm( "Do rts_av_enable_chn Failed!!!" );
		goto err;
	}

	rts_av_query_aec_ctrl( stream->aec, &aec_ctrl );

	if ( aec_ctrl ) {
		aec_ctrl->aec_enable	= 1;
		aec_ctrl->ns_enable		= 1;
		aec_ctrl->ns_level		= 15;

		rts_av_set_aec_ctrl( aec_ctrl );
		rts_av_release_aec_ctrl( aec_ctrl );
	} else {
		dbg_error( "Do rts_av_query_aec_ctrl Failed!!!" );
	}

	return SUCCESS;

err:
	stream_close( stream );

	return FAILURE;
}

# include "RT_AecNs_API.h"
# include "AecNs_Parameters.h"

static int
audio_init( StreamCtx *stream, CAMERA_AUDIO_CODEC codec )
{
	if ( audio_create_stream( stream, codec ) == SUCCESS ) {
		/* Create object */
		return SUCCESS;
	} else {
		return FAILURE;
	}
}

static void
audio_play_stop()
{
	if ( codec.ao.codec >= 0 ) {
		rts_av_disable_chn( codec.ao.codec );
		codec.ao.codec	= -1;
	}

	if ( codec.ao.stream >= 0 ) {
		rts_av_disable_chn( codec.ao.stream );
		codec.ao.stream	= -1;
	}
}

static void
audio_play_recycle_buffer( void *master, struct rts_av_buffer *buffer )
{
	rts_av_delete_buffer( buffer );
}

static int
camera_action_proc( void *data )
{
	time_t	curr_time = 0, update_time = 0;
	u32		ir_counter = 0;
	int		adc_valuem, ir_value = 0;

	img_set_gray_mode( ir_value );
	ir_cut_enable( ir_value );

	struct rts_isp_ae_ctrl *ae = NULL;

	rts_av_query_isp_ae( &ae );

	while ( 1 ) {
		msleep( 100 );

		curr_time	= time( NULL );

		adc_valuem = light_sensor_read_adc();

		if ( adc_valuem < 100 ) {
			if ( ( ir_value == 0 ) && ( ir_counter == 0 ) ) {
				ir_counter = curr_time;
			}
		} else if ( adc_valuem > 200 ) {
			if ( ( ir_value == 1 ) && ( ir_counter == 0 ) ) {
				ir_counter = curr_time;
			}
		} else {
			ir_counter	= 0;
		}

		if ( ir_counter && ( ( ir_counter + ir_hold_duration ) < curr_time ) ) {
			if ( ir_value == 0 ) {
				ir_value = 1;
			} else {
				ir_value = 0;
			}

			ir_trigger_clock	= seconds();

			img_set_gray_mode( ir_value );
			ir_cut_enable( ir_value );

			ir_counter	= 0;
		}

# ifndef MD_CALLBACK
		if ( curr_time >= ( ir_trigger_clock + 10 ) ) {
			md_check_status();
		}
# endif

		if ( curr_time != update_time ) {
			pthread_mutex_lock( &mutex );
			osd_update( curr_time );
			pthread_mutex_unlock( &mutex );

			update_time	= curr_time;

			if ( ae ) {
				rts_av_get_isp_ae( ae );

				if ( ae->mode == RTS_ISP_AE_AUTO ) {
					int i;

					rts_av_refresh_isp_ae_statis( ae );

					ae->_auto.gain_max              = 256;
					ae->_auto.target_delta			= 0;

					for ( i = 0; i < ae->window_num; i ++ ) {
						if ( ae->statis.win_y_means[ i ] > 150 ) {
							ae->_auto.win_weights[ i ]	= 10;
						} else if ( ae->statis.win_y_means[ i ] > 100 ) {
							ae->_auto.win_weights[ i ]	= 3;
						} else if ( ae->statis.win_y_means[ i ] > 10 ) {
							ae->_auto.win_weights[ i ]	= 2;
						} else {
							ae->_auto.win_weights[ i ]	= 1;
						}
					}

					rts_av_set_isp_ae( ae );
				}
			}
		}
	}

	if ( ae ) {
    	rts_av_release_isp_ae( ae );
    }

	return 0;
}

static void
camera_hw_check()
{
	struct rts_gpio *rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_HW_CHECK );

	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_INPUT ) == 0 ) {
			is_new_hw	= rts_io_gpio_get_value( rts_gpio );
			dbg_warm( "value: %d", is_new_hw );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}
}

//==========================================================================
// APIs
//==========================================================================
int
Camera_ShowOSD( OsdCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	} else {
		u8 *string = NULL;

		if ( ctx->show_string && ctx->string && ctx->len ) {
			string	= ( u8 * )my_malloc( ctx->len + 4 );
			if ( string ) {
				memcpy( string, ctx->string, ctx->len );
			} else {
				dbg_error( "Out-of-Momery" );
				return FAILURE;
			}
		}

		pthread_mutex_lock( &mutex );

		if ( codec.osd.string ) {
			my_free( codec.osd.string );
			codec.osd.string	= NULL;
		}

		codec.osd.show_date		= ctx->show_date;
		codec.osd.show_time		= ctx->show_time;
		codec.osd.show_string	= ctx->show_string;
		codec.osd.string		= string;

		pthread_mutex_unlock( &mutex );

		return SUCCESS;
	}

	return SUCCESS;
}

int
Camera_SetMask( u32 id, MaskCtx *ctx )
{
	struct rts_video_mask_attr *attr = NULL;
	int ret	= SUCCESS;

	if ( ctx == NULL ) {
		dbg_warm( "ctx == NULL!!!" );
		return FAILURE;
	} else if ( rts_query_isp_mask_attr( &attr, IMAGE_WIDTH_MAX, IMAGE_HEIGHT_MAX ) ) {
		dbg_warm( "Do rts_query_isp_mask_attr Failed!!!" );
		return FAILURE;
	} else if ( attr->number == 0 ) {
		dbg_warm( "attr->number == 0!!!" );
		ret = FAILURE;
	} else if ( id >= attr->number ) {
		dbg_warm( "id >= attr->number!!!" );
		ret = FAILURE;
	} else {
		struct rts_video_mask_block *pblock = &attr->blocks[ id ];

		pblock->enable			= ctx->enable;

		if ( pblock->enable ) {
			if ( pblock->type == RTS_AV_BLK_TYPE_RECT ) {
				pblock->rect.left		= ctx->x;
				pblock->rect.top		= ctx->y;
				pblock->rect.right		= ctx->x + ctx->width;
				pblock->rect.bottom		= ctx->y + ctx->height;

				if ( pblock->rect.left > pblock->res_size.width ) {
					ret	= FAILURE;
					goto err;
				}

				if ( pblock->rect.top > pblock->res_size.height ) {
					ret	= FAILURE;
					goto err;
				}

				if ( pblock->rect.right > pblock->res_size.width ) {
					pblock->rect.right	= pblock->res_size.width;
				}

				if ( pblock->rect.bottom > pblock->res_size.height ) {
					pblock->rect.bottom	= pblock->res_size.height;
				}
			} else if ( pblock->type == RTS_AV_BLK_TYPE_GRID ) {
				pblock->area.size.columns		= 1;
				pblock->area.size.rows			= 1;

				if ( ctx->x > pblock->res_size.width ) {
					ret	= FAILURE;
					goto err;
				} else {
					pblock->area.start.x		= ctx->x;
				}

				if ( ctx->y > pblock->res_size.height ) {
					ret	= FAILURE;
					goto err;
				} else {
					pblock->area.start.y		= ctx->y;
				}

				if ( ( pblock->area.start.x + ctx->width ) > pblock->res_size.width ) {
					pblock->area.cell.width		= pblock->res_size.width - pblock->area.start.x;
				} else {
					pblock->area.cell.width		= ctx->width;
				}

				if ( ( pblock->area.start.y + ctx->height ) > pblock->res_size.height ) {
					pblock->area.cell.height	= pblock->res_size.height - pblock->area.start.y;
				} else {
					pblock->area.cell.height	= ctx->height;
				}

				if ( pblock->area.cell.width > CAMERA_MASK_WIDTH_LIMIT ) {
					pblock->area.cell.width		= CAMERA_MASK_WIDTH_LIMIT;
				}

				if ( pblock->area.cell.height > CAMERA_MASK_HEIGHT_LIMIT ) {
					pblock->area.cell.height	= CAMERA_MASK_HEIGHT_LIMIT;
				}

				if ( pblock->area.bitmap.vm_addr ) {
					memset( pblock->area.bitmap.vm_addr, 0xFF, pblock->area.bitmap.length );
				} else {
					dbg_warm( "pblock->area.bitmap.vm_addr == NULL!!!" );
					ret	= FAILURE;
					goto err;
				}
			} else {
				dbg_error( "pblock->type == %d!!!", pblock->type );
				ret	= FAILURE;
				goto err;
			}
		}

		attr->color	= 0x00000000;

		if ( rts_set_isp_mask_attr( attr ) ) {
			dbg_warm( "Do rts_set_isp_mask_attr Failed!!!" );
			ret = FAILURE;
		}
	}

err:
	rts_release_isp_mask_attr( attr );

	return ret;
}

int
Camera_SetMDCBFunc( MD_CBFunc cb )
{
	codec.md_cb	= cb;
	return SUCCESS;
}

int
Camera_SetMD( u32 id, MDCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	}

	if ( codec.motion.attr == NULL ) {
		if ( md_init() == FAILURE ) {
			dbg_error( "Do md_init Failed!!" );
			return FAILURE;
		}
	}

	if ( codec.motion.attr->number == 0 ) {
		dbg_error( "codec.motion.attr->number == 0" );
		return FAILURE;
	} else if ( id >= MD_AREA_MAX ) {
		dbg_error( "id >= MD_AREA_MAX" );
		return FAILURE;
	} else {
		struct rts_video_md_block *pblock = &codec.motion.attr->blocks[ 0 ];

		md_area *area = &codec.motion.area[ id ];

		if ( ctx->enable ) {
			if ( ctx->x > 100 ) {
				return FAILURE;
			} else if ( ctx->y > 100 ) {
				return FAILURE;
			} else if ( ctx->width == 0 ) {
				return FAILURE;
			} else if ( ctx->height == 0 ) {
				return FAILURE;
			}

			dbg_error( "MD%d Enabled.", id );

			area->enable		= ctx->enable;

			area->percentage	= ctx->percentage;
			area->x0			= ( ctx->x * pblock->area.size.columns ) / 100;
			area->y0			= ( ctx->y * pblock->area.size.rows ) / 100;
			area->x1			= ( ( ctx->x + ctx->width ) * pblock->area.size.columns ) / 100;
			area->y1			= ( ( ctx->y + ctx->height ) * pblock->area.size.rows ) / 100;

			if ( area->x1 > pblock->area.size.columns ) {
				area->x1	= pblock->area.size.rows;
			}

			if ( area->y1 > pblock->area.size.columns ) {
				area->y1	= pblock->area.size.rows;
			}

			dbg_error( "    percentage: %d", area->percentage );
			dbg_error( "    x: %d ~ %d", area->x0, area->x1 );
			dbg_error( "    y: %d ~ %d", area->y0, area->y1 );
		} else {
			dbg_error( "MD%d Disabled.", id );

			area->enable	= ctx->enable;
		}

		return SUCCESS;
	}
}

int
Camera_SetImage( ImageCtx *ctx, char force )
{
	ISPTable	*isp_item;
	int	*value, *new_value;

	if ( ctx == NULL ) {
		return FAILURE;
	}

	isp_item	= &ips_tbl[ 0 ];
	while ( isp_item->name ) {
		value		= ( int * )( ( void * )&codec.img_ctx + isp_item->pos );
		new_value	= ( int * )( ( void * )ctx + isp_item->pos );

		if ( force || ( *value != *new_value ) ) {
			if ( ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_AE_GAIN ) ||
				 ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_EXPOSURE_TIME ) ) {
				if ( ctx->ae.mode != RTS_ISP_AE_MANUAL ) {
					isp_item ++;
					continue;
				}
				dbg_line( "isp_item->cmd_id = %d", isp_item->cmd_id );
			}

			if ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_WDR_LEVEL ) {
				if ( ctx->wdr.mode != RTS_ISP_WDR_MANUAL ) {
					isp_item ++;
					continue;
				}
				dbg_line( "isp_item->cmd_id = %d", isp_item->cmd_id );
			}

			if ( img_set_value( isp_item->cmd_id, *new_value ) != SUCCESS ) {
				dbg_line( "Do img_set_value(%s: %d) Failed!!!", isp_item->name, *new_value );
			} else {
				*value = *new_value;
			}
		}

		isp_item ++;
	}

	//img_print( ctx );

	int ret;
/*
	struct rts_isp_awb_ctrl *awb;

	ret = rts_av_query_isp_awb( &awb );
	if ( ret ) {
		return SUCCESS;
	}

	awb->mode = ctx->awb.mode;

	if ( awb->mode == RTS_ISP_AWB_AUTO ) {
		awb->_auto.adjustment.r_gain	= ctx->awb.r_gain;
		awb->_auto.adjustment.b_gain	= ctx->awb.b_gain;
	} else if ( awb->mode == RTS_ISP_AWB_TEMPERATURE ) {
		awb->_manual.temperature		= ctx->awb.level;
	} else if ( awb->mode == RTS_ISP_AWB_COMPONENT ) {
		awb->_component.red				= ctx->awb.r_gain;
		awb->_component.green			= ctx->awb.g_gain;
		awb->_component.blue			= ctx->awb.b_gain;
	}

    rts_av_set_isp_awb( awb );

    rts_av_release_isp_awb( awb );
*/
	// AE
    struct rts_isp_ae_ctrl *ae;

	ret = rts_av_query_isp_ae( &ae );
	if ( ret ) {
		return SUCCESS;
	}

	rts_av_get_isp_ae( ae );

	if ( ae->mode == RTS_ISP_AE_AUTO ) {
        ae->_auto.gain_max              = ctx->ae.gain;
	} else if ( ae->mode == RTS_ISP_AE_MANUAL ) {
		ae->_manual.total_gain			= ctx->ae.gain;
    	ae->_manual.exposure_time		= ctx->ae.exposure_time;
	}

	rts_av_set_isp_ae( ae );
    rts_av_release_isp_ae( ae );

	return SUCCESS;
}

int
Camera_GetImage( ImageCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	}

	memcpy( ctx, &codec.img_ctx, sizeof( ImageCtx ) );

	return SUCCESS;
}

int
Camera_SetVideoCodec( CAMERA_STREAM stream, CodecCtx *ctx )
{
	if ( ( stream >= CAMERA_STREAM_VIDEO_MAX ) || ( ctx == NULL ) ) {
		return FAILURE;
	} else if ( memcmp( &codec.stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) ) == 0 ) {
		return SUCCESS;
	}

	if ( stream == 0 ) {
		if ( ( ctx->width > 1920 ) || ( ctx->height > 1080 ) ) {
			dbg_warm( "Stream %d: Width/Height Error: %dx%d", stream, ctx->width, ctx->height );
			return -1;
		}
	} else {
		if ( ( ctx->width > 1280 ) || ( ctx->height > 720 ) ) {
			dbg_warm( "Stream %d: Width/Height Error: %dx%d", stream, ctx->width, ctx->height );
			return -1;
		}
	}

	stream_stop( &codec.stream_ctxs[ stream ] );

	codec.stream_ctxs[ stream ].num			= stream;
	codec.stream_ctxs[ stream ].video_codec	= codec_map[ stream ];

	if ( codec_handle[ codec_map[ stream ] ].set_video( &codec.stream_ctxs[ stream ], ctx ) == FAILURE ) {
		dbg_warm( "Do set_codec Failed!!!" );
		return FAILURE;
	} else if ( stream_start( &codec.stream_ctxs[ stream ] ) ) {
		dbg_warm( "Do stream_start Failed!!!" );
		return FAILURE;
	}

	memcpy( &codec.stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) );

	if ( codec.stream_ctxs[ stream ].codec_ctx.width >= 960 ) {
		codec.stream_ctxs[ stream ].font_size		= 20;
	} else if ( codec.stream_ctxs[ stream ].codec_ctx.width >= 640 ) {
		codec.stream_ctxs[ stream ].font_size		= 12;
	} else {
		codec.stream_ctxs[ stream ].font_size		= 8;
	}

	return SUCCESS;
}

int
Camera_SetVideoBitrate( CAMERA_STREAM stream, u32 bitrate )
{
	if ( ( stream != CAMERA_STREAM_VIDEO_H264_1 ) && ( stream != CAMERA_STREAM_VIDEO_H264_2 ) ) {
		return FAILURE;
	} else if ( codec.stream_ctxs[ stream ].stream < 0 ) {
		return FAILURE;
	} else {
		struct rts_video_h264_ctrl	*ctrl;

		if ( rts_av_query_h264_ctrl( codec.stream_ctxs[ stream ].stream, &ctrl ) ) {
			dbg_warm( "Do rts_av_query_h264_ctrl Failed!!!" );
			return FAILURE;
		} else {
# ifdef CODEC_VBR
			// VBR
			ctrl->bitrate_mode = RTS_BITRATE_MODE_VBR;
			/* (high) 30 to 50 (low) */
			ctrl->qp		=
			ctrl->max_qp	=
			ctrl->min_qp	= 30;
# elif defined CODEC_CVBR
			// CVBR
			ctrl->bitrate_mode	= RTS_BITRATE_MODE_C_VBR;

			ctrl->max_bitrate	= bitrate * 1024;
			ctrl->min_bitrate	= bitrate * 512;
# else
			// CBR
			ctrl->bitrate_mode	= RTS_BITRATE_MODE_CBR;
			ctrl->bitrate		= bitrate * 1024;
# endif
			if ( rts_av_set_h264_ctrl( ctrl ) ) {
				dbg_warm( "Do rts_av_query_h264_ctrl Failed!!!" );
				rts_av_release_h264_ctrl( ctrl );
				return FAILURE;
			}

			rts_av_release_h264_ctrl( ctrl );
		}

		return SUCCESS;
	}
}

int
Camera_SetAudioCBFunc( Audio_CBFunc cb )
{
	codec.audio_cb	= cb;
	return SUCCESS;
}

int
Camera_EnableAudio( char on )
{
	stream_stop( &codec.stream_ctxs[ CAMERA_STREAM_AUDIO ] );

	if ( on ) {
		codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].num			= CAMERA_STREAM_AUDIO;
		codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].audio_codec	= CAMERA_AUDIO_CODEC_DEF;

		if ( audio_init( &codec.stream_ctxs[ CAMERA_STREAM_AUDIO ], CAMERA_AUDIO_CODEC_DEF ) == FAILURE ) {
			dbg_warm( "Do audio_init Failed!!!" );
			return FAILURE;
		} else if ( stream_start( &codec.stream_ctxs[ CAMERA_STREAM_AUDIO ] ) ) {
			dbg_warm( "Do stream_start Failed!!!" );
			return FAILURE;
		}
	}

	return SUCCESS;
}

int
Camera_GetAudioInConf( AudioConf *conf )
{
	if ( conf ) {
		conf->sampling_freq	= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].rate;
		conf->channels		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].channel;
		conf->sample		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].sample;

		if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_AAC ) {
			conf->type	= FRAME_AUDIO_AAC;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711U ) {
			conf->type	= FRAME_AUDIO_PCMU;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711A ) {
			conf->type	= FRAME_AUDIO_PCMA;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_PCM ) {
			conf->type	= FRAME_AUDIO_PCM;
		}

		return SUCCESS;
	}

	return FAILURE;
}

int
Camera_SetAudioInVolume( u32 volume )
{
	if ( volume > 100 ) {
		dbg_line( "Parameter Error( %u )!!!", volume );
		return FAILURE;
	} else if ( rts_audio_set_capture_volume( volume ) ) {
		dbg_line( "Fail to set capture volume!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

int
Camera_SetAudioOutVolume( u32 volume )
{
	if ( volume > 100 ) {
		dbg_line( "Parameter Error( %u )!!!", volume );
		return FAILURE;
	} else if ( rts_audio_set_playback_volume( volume ) ) {
		dbg_line( "Fail to set playback volume!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

int
Camera_OpenAudioOut()
{
	if ( codec.ao.codec < 0 ) {
		struct rts_av_profile	profile;
		struct rts_audio_attr	cfg;
		struct rts_gpio			*rts_gpio;
		struct rts_av_callback cb;

		int res;

		rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_AUDIO_OUT );
		if ( rts_gpio ) {
			if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
				rts_io_gpio_set_value( rts_gpio, RTS_GPIO_PIN_AUDIO_OUT_ON );
			}
			rts_io_gpio_free( rts_gpio );
		} else {
			dbg_line( "Do rts_io_gpio_request Failed!!!" );
		}

		memset( &profile, 0, sizeof( struct rts_av_profile ) );
		memset( &cfg, 0, sizeof( struct rts_audio_attr ) );

		codec.ao.codec = rts_av_create_audio_decode_chn();
		if ( codec.ao.codec < 0 ) {
			dbg_warm( "Do rts_av_create_audio_decode_chn Failed!!!" );
			return FAILURE;
		}

		if ( ( res = rts_av_get_profile( codec.ao.codec, &profile ) ) ) {
			dbg_warm( "Do rts_av_get_profile Failed( %d )!!!", res );
			goto err;
		}

		if ( CAMERA_AUDIO_OUT_CODEC_DEF == CAMERA_AUDIO_CODEC_G711A ) {
			profile.fmt		= RTS_A_FMT_ALAW;
		} else if ( CAMERA_AUDIO_OUT_CODEC_DEF == CAMERA_AUDIO_CODEC_G711U ) {
			profile.fmt		= RTS_A_FMT_ULAW;
		} else if ( CAMERA_AUDIO_OUT_CODEC_DEF == CAMERA_AUDIO_CODEC_AAC ) {
			profile.fmt		= RTS_A_FMT_AAC;
			return FAILURE;
		} else if ( CAMERA_AUDIO_OUT_CODEC_DEF == CAMERA_AUDIO_CODEC_PCM ) {
			profile.fmt		= RTS_A_FMT_AUDIO;
		}

		profile.audio.samplerate	= 16000;
		profile.audio.bitfmt		= 16;
		profile.audio.channels		= 1;

		if ( ( res = rts_av_set_profile( codec.ao.codec, &profile ) ) ) {
			dbg_warm( "Do rts_av_set_profile Failed( %d )!!!", res );
			goto err;
		}

		cb.func		= audio_dec_cb;
		cb.start	= 0;
		cb.times	= -1;
		cb.interval = 0;
		cb.type		= RTS_AV_CB_TYPE_ASYNC;

		if ( rts_av_set_callback( codec.ao.codec, &cb, 0 ) ) {
			dbg_warm( "Do rts_av_set_callback Failed!!!" );
		}

		snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );

		/*
		cfg.rate		= audio_attr[ CAMERA_AUDIO_OUT_CODEC_DEF ].rate;
		cfg.format		= audio_attr[ CAMERA_AUDIO_OUT_CODEC_DEF ].sample;
		cfg.channels	= audio_attr[ CAMERA_AUDIO_OUT_CODEC_DEF ].channel;
		*/
		cfg.rate		= 16000;
		cfg.format		= 16;
		cfg.channels	= 1;

		codec.ao.stream = rts_av_create_audio_playback_chn( &cfg );
		if ( codec.ao.stream < 0 ) {
			dbg_warm( "Do rts_av_create_audio_decode_chn Failed!!!" );
			goto err;
		}
/*
		if ( rts_av_bind( codec.ao.codec, codec.ao.stream ) ) {
			dbg_warm( "Do rts_av_bind( codec.ao.codec, codec.ao.stream ) Failed!!!" );
			goto err;
		}
*/
		if ( rts_av_enable_chn( codec.ao.codec ) ) {
			dbg_warm( "Do rts_av_enable_chn( codec.ao.codec ) Failed!!!" );
			goto err;
		} else if ( rts_av_enable_chn( codec.ao.stream ) ) {
			dbg_warm( "Do rts_av_enable_chn( codec.ao.stream ) Failed!!!" );
			goto err;
		} else if ( rts_av_start_send( codec.ao.codec ) ) {
			dbg_warm( "Do rts_av_start_send( codec.ao.codec ) Failed!!!" );
			goto err;
		}

		rts_av_start_send( codec.ao.stream );
	}

	return SUCCESS;

err:
	audio_play_stop();

	return FAILURE;
}

int
Camera_CloseAudioOut()
{
	struct rts_gpio			*rts_gpio;

	if ( codec.ao.codec >= 0 ) {
		rts_av_stop_send( codec.ao.codec );
	}

	audio_play_stop();

	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_SYS, RTS_GPIO_PIN_AUDIO_OUT );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, RTS_GPIO_MODE_OUTPUT ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, RTS_GPIO_PIN_AUDIO_OUT_OFF );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	return SUCCESS;
}

int
Camera_PlayAudio( u8 *data, u32 size )
{
	if ( ( data == NULL ) || ( size == 0 ) ) {
		dbg_warm( "( data == NULL ) || ( size == 0 )" );
		return FAILURE;
	} else if ( size > codec.ao.chunk_bytes ) {
		dbg_warm( "Audio Over Buffer Size( %d > %d )!!!", size, codec.ao.chunk_bytes );
		return FAILURE;
	} else if ( codec.ao.stream < 0 ) {
		dbg_warm( "codec.ao.stream < 0" );
		return FAILURE;
	} else {
		struct rts_av_buffer *buffer = NULL;
		int res;

		buffer = rts_av_new_buffer( codec.ao.chunk_bytes );

		if ( buffer == NULL ) {
			dbg_warm( "Can not get free buffer( %u )!!!", size );
			return FAILURE;
		}

		rts_av_set_buffer_callback( buffer, NULL, audio_play_recycle_buffer );
		rts_av_get_buffer( buffer );

		memcpy( buffer->vm_addr, data, size );
		buffer->bytesused	= size;
		buffer->timestamp	= 0;

		if ( ( res = rts_av_send( codec.ao.codec, buffer ) ) ) {
			dbg_warm( "Do rts_av_stream_send Failed( %d )!!!", res );
			return FAILURE;
		}

		rts_av_put_buffer( buffer );

		return SUCCESS;
	}
}

int
Camera_SetStreamCBFunc( Stream_CBFunc cb )
{
	codec.stream_cb	= cb;
	return SUCCESS;
}

int
Camera_Init()
{
	ISPTable	*isp_item;
	int	*value, i;

	if ( rts_av_init() ) {
		dbg_warm( "Do rts_av_init Failed !!!" );
		return FAILURE;
	}

	camera_hw_check();

	memset( &codec, 0, sizeof( codec ) );

	for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
		codec.stream_ctxs[ i ].src		= -1;
		codec.stream_ctxs[ i ].osd		= -1;
		codec.stream_ctxs[ i ].stream	= -1;
	}

	codec.ao.codec		= -1;
	codec.ao.stream		= -1;

	create_norm_thread( stream_proc, NULL, 0 );

	isp_item	= &ips_tbl[ 0 ];
	while ( isp_item->name ) {
		value	= ( int * )( ( void * )&codec.img_ctx + isp_item->pos );
		*value	= img_get_default( isp_item->cmd_id, &isp_item->max, &isp_item->min );

		if ( *value < 0 ) {
			dbg_line( "Do img_get_default(%s) Failed!!!", isp_item->name );
			*value = 0;
		}
		//dbg_line( "%s: %d( %d ~ %d )", isp_item->name, *value, isp_item->min, isp_item->max );

		isp_item ++;
	}

	//img_print( &codec.img_ctx );

	create_norm_thread( camera_action_proc, NULL, 0 );

	codec.ao.chunk_bytes		= 4096;
	for ( i = 0; i < RTS_AUDIO_MAX_BUF_NUM; i ++ ) {
		codec.ao.buffer[ i ] = rts_av_new_buffer( codec.ao.chunk_bytes );
		if ( codec.ao.buffer[ i ] == NULL ) {
			dbg_warm( "no memory for audio buffer" );
			break;
		}
	}

	if ( FT_Init_FreeType( &codec.library ) ) {
		dbg_line( "FT_Init_FreeType fail." );
	} else {
		if ( FT_New_Face( codec.library, OSD_FONT_PATH, 0, &codec.face ) ) {
			dbg_line( "FT_New_Face fail.( %s not found or file error %p )", OSD_FONT_PATH, codec.face );
		}
	}

	return SUCCESS;
}

void
Camera_Deinit()
{
	int i;
/*
	for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
		codec.stream_ctxs[ i ].stop	= TRUE;
	}

	msleep( 500 );

	for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
		stream_stop( &codec.stream_ctxs[ i ] );
	}
*/
	if ( codec.motion.attr ) {
		rts_av_release_isp_md( codec.motion.attr );
	}

	Camera_CloseAudioOut();

	for ( i = 0; i < RTS_AUDIO_MAX_BUF_NUM; i ++ ) {
		if ( codec.ao.buffer[ i ] ) {
			rts_av_delete_buffer( codec.ao.buffer[ i ] );
			codec.ao.buffer[ i ] = NULL;
		}
	}

	if ( codec.face ) {
		FT_Done_Face( codec.face );
	}

	FT_Done_FreeType( codec.library );

	rts_av_release();
	dbg_error();
}

