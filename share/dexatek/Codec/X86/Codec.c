/*
 * camera.c
 *
 *  Created on: 2017/10/24
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Camera.h"

//==========================================================================
// Type Declaration
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
int
Camera_ShowOSD( OsdCtx *ctx )
{
	return SUCCESS;
}

int
Camera_SetMask( u32 id, MaskCtx *ctx )
{
	return SUCCESS;
}

int
Camera_SetMDCBFunc( MD_CBFunc cb )
{
	return SUCCESS;
}

int
Camera_SetMD( u32 id, MDCtx *ctx )
{
	return SUCCESS;
}

int
Camera_SetImage( ImageCtx *ctx, char force )
{
	return SUCCESS;
}

int
Camera_GetImage( ImageCtx *ctx )
{
	return SUCCESS;
}

int
Camera_SetVideoCodec( CAMERA_STREAM stream, CodecCtx *ctx )
{
	return SUCCESS;
}

int
Camera_EnableAudio( char on )
{
	return SUCCESS;
}

int
Camera_GetAudioInConf( AudioConf *conf )
{
	return FAILURE;
}

int
Camera_SetAudioInVolume( u32 volume )
{
	return SUCCESS;
}

int
Camera_SetAudioOutVolume( u32 volume )
{
	return SUCCESS;
}

int
Camera_OpenAudioOut()
{
	return SUCCESS;
}

int
Camera_CloseAudioOut()
{
	return SUCCESS;
}

int
Camera_PlayAudio( u8 *data, u32 size )
{
	return SUCCESS;
}

int
Camera_SetStreamCBFunc( Stream_CBFunc cb )
{
	return SUCCESS;
}

int
Camera_Init()
{
	return SUCCESS;
}

void
Camera_Deinit()
{

}

