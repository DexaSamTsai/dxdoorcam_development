/*
 * camera.c
 *
 *  Created on: 2017¦~7¤ë24¤é
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include <rtstream.h>
# include <rtsavapi.h>
# include <rtscamkit.h>
# include <rtsv4l2.h>
# include <rtsvideo.h>
# include <rtsaudio.h>
# include <rts_io_gpio.h>
# include "Headers.h"
# include "Camera.h"

//==========================================================================
// Type Declaration
//==========================================================================
# ifndef RTS_AUDIO_MAX_BUF_NUM
# define RTS_AUDIO_MAX_BUF_NUM			3
# endif

# define LIGTH_SENSOR_HOLD_DURATION		4

# define MD_GRID_WIDTH					40
# define MD_GRID_HEIGHT					40

# define RTS_AUDIO_DEV					"hw:0,1"
//# define RTS_AUDIO_DEV				"hw:0,0"

enum {
	RTS_GPIO_GROUP_SYS		= 0,
	RTS_GPIO_GROUP_MCU,
};

# define RTS_LED_GPIO_PIN		4

enum {
	CAMERA_OSD_DATE	= 0,
	CAMERA_OSD_TIME,
	CAMERA_OSD_STRING,
};

typedef struct {
	char			*name;
	u32				cmd_id;
	unsigned long	pos;
	int				max;
	int				min;
} ISPTable;

# define FIELD_OFFSET( field )		( ( unsigned long )&( ( ( ImageCtx * )0 )->field ) )

typedef enum {
	CAMERA_VIDEO_CODEC_MJPEG	= 0,
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_MAX,
} CAMERA_VIDEO_CODEC;

typedef enum {
	CAMERA_AUDIO_CODEC_G711A	= 0,
	CAMERA_AUDIO_CODEC_G711U,
	CAMERA_AUDIO_CODEC_AAC,
	CAMERA_AUDIO_CODEC_PCM,
	CAMERA_AUDIO_CODEC_MAX,
	//CAMERA_AUDIO_CODEC_DEF	= CAMERA_AUDIO_CODEC_G711U,
	//CAMERA_AUDIO_CODEC_DEF	= CAMERA_AUDIO_CODEC_PCM,
	CAMERA_AUDIO_CODEC_DEF	= CAMERA_AUDIO_CODEC_AAC,
} CAMERA_AUDIO_CODEC;

typedef struct {
	RtStream	( *set_video )			( CAMERA_STREAM, CodecCtx * );
} CodecHandle;

typedef struct {
	char				play;
	char				stop;

	CAMERA_VIDEO_CODEC	video_codec;
	CAMERA_AUDIO_CODEC	audio_codec;

	RtStream			stream;
	CodecCtx			codec_ctx;

	u32					font_size;
} StreamCtx;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static CAMERA_VIDEO_CODEC codec_map[ CAMERA_STREAM_VIDEO_MAX ]	= {
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_MJPEG,
};

static int video_dev_map[ CAMERA_STREAM_VIDEO_MAX ]	= {
	0,
	1,
	3,
};

static StreamCtx		stream_ctxs[ CAMERA_STREAM_MAX ];
static ImageCtx			img_ctx;
static MD_CBFunc		md_cb = NULL;
static Stream_CBFunc	stream_cb = NULL;
static u32				ir_hold_duration	= LIGTH_SENSOR_HOLD_DURATION;

static struct {
	RtStream	stream;
	int			chunk_bytes;

	struct rts_av_buffer	*buffer[ RTS_AUDIO_MAX_BUF_NUM ];
} audio_out;

static struct {
	u16		rate;
	u8		sample;
	u8		channel;
} audio_attr[ CAMERA_AUDIO_CODEC_MAX ] = {
	{ 32000, 16, 2 },
	{ 32000, 16, 2 },
	{ 48000, 16, 2 },		// AAC
	{ 44100, 16, 2 },
};

static ISPTable ips_tbl[]	= {
	{ "Brightness",		RTS_VIDEO_CTRL_ID_BRIGHTNESS,		FIELD_OFFSET( brightness ), },
	{ "Contrast",		RTS_VIDEO_CTRL_ID_CONTRAST,			FIELD_OFFSET( contrast ), },
	{ "Hue",			RTS_VIDEO_CTRL_ID_HUE,				FIELD_OFFSET( hue ), },
	{ "Saturation",		RTS_VIDEO_CTRL_ID_SATURATION,		FIELD_OFFSET( saturation ), },
	{ "Sharpness",		RTS_VIDEO_CTRL_ID_SHARPNESS,		FIELD_OFFSET( sharpness ), },
	{ "Gamma",			RTS_VIDEO_CTRL_ID_GAMMA,			FIELD_OFFSET( gamma ), },

	{ "WBMode",			RTS_VIDEO_CTRL_ID_AWB_CTRL,			FIELD_OFFSET( awb.mode ), },
	{ "WBLevel",		RTS_VIDEO_CTRL_ID_WB_TEMPERATURE,	FIELD_OFFSET( awb.level ), },
	{ "RGain",			RTS_VIDEO_CTRL_ID_RED_BALANCE,		FIELD_OFFSET( awb.r_gain ), },
	{ "GGain",			RTS_VIDEO_CTRL_ID_GREEN_BALANCE,	FIELD_OFFSET( awb.g_gain ), },
	{ "BGain",			RTS_VIDEO_CTRL_ID_BLUE_BALANCE,		FIELD_OFFSET( awb.b_gain ), },

	{ "BLC",			RTS_VIDEO_CTRL_ID_BLC,				FIELD_OFFSET( blc ), },

# ifndef RTS3902
	{ "Gain",			RTS_VIDEO_CTRL_ID_GAIN,				FIELD_OFFSET( gain ), },
# endif
	{ "PowerFreq",		RTS_VIDEO_CTRL_ID_PWR_FREQUENCY,	FIELD_OFFSET( power_freq ), },

/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_AF, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_FOCUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_ZOOM, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_PAN, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_TILT, FIELD_OFFSET( brightness ), },
*/
	{ "Roll",			RTS_VIDEO_CTRL_ID_ROLL,				FIELD_OFFSET( roll ), },
	{ "Flip",			RTS_VIDEO_CTRL_ID_FLIP,				FIELD_OFFSET( flip ), },
	{ "Mirror",			RTS_VIDEO_CTRL_ID_MIRROR,			FIELD_OFFSET( mirror ), },
//	{ "Rotate",			RTS_VIDEO_CTRL_ID_ROTATE,			FIELD_OFFSET( rotate ), },

/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_ISP_SPECIAL_EFFECT, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_EV_COMPENSATE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_COLOR_TEMPERATURE_ESTIMATION, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AE_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AWB_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_AF_LOCK, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_LED_TOUCH_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_LED_FLASH_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_ISO, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_SCENE_MODE, FIELD_OFFSET( brightness ), },
*/
//	{ "ROI",			RTS_VIDEO_CTRL_ID_ROI_MODE,			FIELD_OFFSET( roi ), },
/*
	{ "Brightness", RTS_VIDEO_CTRL_ID_3A_STATUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IDEA_EYE_SENSITIVITY, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IDEA_EYE_STATUS, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CTRL_ID_IEDA_EYE_MODE, FIELD_OFFSET( brightness ), },
*/
	{ "GrayMode",		RTS_VIDEO_CTRL_ID_GRAY_MODE,		FIELD_OFFSET( gray_mode ), },

	{ "WDRMode",		RTS_VIDEO_CTRL_ID_WDR_MODE,			FIELD_OFFSET( wdr.mode ), },
	{ "WDRLevel",		RTS_VIDEO_CTRL_ID_WDR_LEVEL,		FIELD_OFFSET( wdr.level ), },

	{ "3DNR",			RTS_VIDEO_CTRL_ID_3DNR,				FIELD_OFFSET( dnr3 ), },
	{ "Dehaze",			RTS_VIDEO_CTRL_ID_DEHAZE,			FIELD_OFFSET( dehaze ), },
	{ "IRMode",			RTS_VIDEO_CTRL_ID_IR_MODE,			FIELD_OFFSET( ir_mode ), },
/*
	{ "Brightness", RTS_VIDEO_CRTL_ID_SMART_IR_MODE, FIELD_OFFSET( brightness ), },
	{ "Brightness", RTS_VIDEO_CRTL_ID_SMART_IR_MANUAL_LEVEL, FIELD_OFFSET( brightness ), },
*/

	{ "ExposureMode",	RTS_VIDEO_CTRL_ID_EXPOSURE_MODE,	FIELD_OFFSET( ae.mode ), },
	{ "AEGain",			RTS_VIDEO_CTRL_ID_AE_GAIN,			FIELD_OFFSET( ae.gain ), },
	{ "ExposureTime",	RTS_VIDEO_CTRL_ID_EXPOSURE_TIME,	FIELD_OFFSET( ae.exposure_time ), },
//	{ "Brightness", RTS_VIDEO_CTRL_ID_EXPOSURE_PRIORITY, FIELD_OFFSET( brightness ), },

	{ NULL,	0,	0, },
};

//==========================================================================
// Static Functions
//==========================================================================
static void
img_print_md( struct rts_video_grid_bitmap *bitmap )
{
	int idx		= 0;
	int number	= 0;
	int i;

	while ( idx < RTS_DIV_ROUND_UP( bitmap->columns * bitmap->rows, 8 ) ) {
		uint8_t val = bitmap->bitmap[ idx ++ ];

		for ( i = 0; i < 8; i ++ ) {
			printf( "%d", ( val & ( 0x1 << i ) )? 1: 0 );
			number ++;

			if ( number % bitmap->columns == 0 ) {
				printf( "\n" );
			} else {
				printf( " " );
			}

			if ( number == ( bitmap->rows * bitmap->columns ) ) {
				break;
			}
		}

		if ( number == bitmap->rows * bitmap->columns ) {
			break;
		}
	}
}

static int
md_proc( void *data )
{
	struct rts_video_grid_bitmap	bitmap;
	struct rts_video_md_attr		*attr = NULL;
	struct rts_video_md_block		*pblock = NULL;

	int mid;

	while ( 1 ) {
		msleep( 100 );

		if ( rts_query_isp_md_attr( &attr, 1280, 720 ) == 0 ) {
			for ( mid = 0; mid < attr->number; mid ++ ) {
				pblock = &attr->blocks[ mid ];

				if ( pblock->enable == FALSE ) {
					continue;
				} else if ( rts_check_isp_md_status( mid ) == 0 ) {
					continue;
				}

				bitmap.columns	= pblock->area.columns;
				bitmap.rows		= pblock->area.rows;

				rts_get_isp_md_result( mid, &bitmap );

				//img_print_md( &bitmap );

				if ( md_cb ) {
					md_cb( mid, NULL, 0 );
				}
			}
		}

		rts_release_isp_md_attr( attr );
	}

	return 0;
}

static void
img_print_isp( ImageCtx *ctx )
{
	dbg_line( "brightness: %d", ctx->brightness );
	dbg_line( "contrast: %d", ctx->contrast );
	dbg_line( "hue: %d", ctx->hue );
	dbg_line( "saturation: %d", ctx->saturation );
	dbg_line( "sharpness: %d", ctx->sharpness );
	dbg_line( "gamma: %d", ctx->gamma );

	dbg_line( "blc: %d", ctx->blc );
	dbg_line( "gain: %d", ctx->gain );
	dbg_line( "roll: %d", ctx->roll );
	dbg_line( "power_freq: %d", ctx->power_freq );

	dbg_line( "flip: %d", ctx->flip );
	dbg_line( "mirror: %d", ctx->mirror );
	dbg_line( "rotate: %d", ctx->rotate );

	dbg_line( "gray_mode: %d", ctx->gray_mode );

	dbg_line( "awb:" );
	dbg_line( "\tmode: %d", ctx->awb.mode );
	dbg_line( "\tlevel: %d", ctx->awb.level );
	dbg_line( "\tr_gain: %d", ctx->awb.r_gain );
	dbg_line( "\tg_gain: %d", ctx->awb.g_gain );
	dbg_line( "\tb_gain: %d", ctx->awb.b_gain );

	dbg_line( "wdr:" );
	dbg_line( "\tmode: %d", ctx->wdr.mode );
	dbg_line( "\tlevel: %d", ctx->wdr.level );

	dbg_line( "exposure:" );
	dbg_line( "\tmode: %d", ctx->ae.mode );
	dbg_line( "\tgain: %d", ctx->ae.gain );
	dbg_line( "\ttime: %d", ctx->ae.exposure_time );

	dbg_line( "dnr3: %d", ctx->dnr3 );
	dbg_line( "dehaze: %d", ctx->dehaze );
	dbg_line( "ir_mode: %d", ctx->ir_mode );
}

static int
img_get_isp_default( u32 cmd_id, int *max, int *min )
{
	struct rts_video_control ctrl;

	if ( rts_get_isp_ctrl( cmd_id, &ctrl ) ) {
		return FAILURE;
	} else {
		*max	= ctrl.maximum;
		*min	= ctrl.minimum;
		return ctrl.default_value;
	}
}

static int
img_set_isp( u32 cmd_id, int value )
{
	struct rts_video_control ctrl;

	if ( rts_get_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_set_isp_ctrl Failed!!!" );
		return FAILURE;
	}

	if ( ( value <= ctrl.maximum ) && ( value >= ctrl.minimum ) ) {
		ctrl.current_value = value;
	} else {
		dbg_line( "value( %d ): %d ~ %d", value, ctrl.minimum, ctrl.maximum );
		return FAILURE;
	}

	if ( rts_set_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_set_isp_ctrl Failed!!!" );
		return FAILURE;
	} else if ( rts_get_isp_ctrl( cmd_id, &ctrl ) ) {
		dbg_warm( "Do rts_get_isp_ctrl Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static int
light_sensor_read_adc()
{
	char *node = "/sys/devices/platform/rts_saradc.0/in0_input";
	int fd = open( node, O_RDONLY );

	if ( fd > 0 ) {
		char	data[ 16 ];
		int res = read( fd, data, 16 );

		close( fd );

		if ( res > 0 ) {
			return atoi( data );
		} else {
			return -1;
		}
	} else {
		dbg_line( "Open /sys/devices/platform/rts_saradc.0/in0_input Failed!!!" );
		return -1;
	}
}
static int
ir_cut_enable( char on )
{
	struct rts_gpio				*rts_gpio;
	struct rts_video_control	ctrl;

	uint32_t id = RTS_VIDEO_CTRL_ID_IR_MODE;
	int ret;

	if ( on ) {
		dbg_line( "ir_cut_enable ON" );
		ctrl.current_value = 1;
	} else {
		dbg_line( "ir_cut_enable OFF" );
		ctrl.current_value = 0;
	}

	rts_gpio = rts_io_gpio_request( RTS_GPIO_GROUP_MCU, RTS_LED_GPIO_PIN );
	if ( rts_gpio ) {
		if ( rts_io_gpio_set_direction( rts_gpio, 1 ) == 0 ) {
			rts_io_gpio_set_value( rts_gpio, ctrl.current_value );
		}
		rts_io_gpio_free( rts_gpio );
	} else {
		dbg_line( "Do rts_io_gpio_request Failed!!!" );
	}

	ret = rts_set_isp_ctrl( id, &ctrl );
	if ( ret ) {
		dbg_line( "set 3dnr[%d] fail, ret = %d\n", ctrl.current_value, ret );
		return ret;
	}

	return 0;
}

static int
isp_gray_mode_enable( char on )
{
	struct rts_video_control ctrl;
	uint32_t id = RTS_VIDEO_CTRL_ID_GRAY_MODE;
	int ret;

	if ( on ) {
		ctrl.current_value = 1;
	} else {
		ctrl.current_value = 0;
	}

	ret = rts_set_isp_ctrl( id, &ctrl );
	if ( ret ) {
		dbg_line( "set 3dnr[%d] fail, ret = %d\n", ctrl.current_value, ret );
		return ret;
	}

	return 0;
}

static int
stream_proc( void *data )
{
	int i, cnt, adc_valuem, ir_value = 0;
	struct rts_av_buffer *buffer	= NULL;
	AVFrame		frame;
	u32		ir_counter = 0;

	while ( 1 ) {
		cnt = 0;

		for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
			if ( !stream_ctxs[ i ].play ) {
				continue;
			} else if ( stream_ctxs[ i ].stop ) {
				stream_ctxs[ i ].play	= FALSE;
				stream_ctxs[ i ].stop	= FALSE;
				continue;
			} else if ( stream_ctxs[ i ].stream == NULL ) {
				continue;
			} else if ( rts_av_stream_poll( stream_ctxs[ i ].stream ) ) {
				continue;
			} else if ( rts_av_stream_recv( stream_ctxs[ i ].stream, &buffer ) == 0 ) {
				if ( stream_cb ) {
					frame.buff	= buffer->vm_addr;
					frame.size	= buffer->bytesused;

					if ( i == CAMERA_STREAM_AUDIO ) {
						switch ( stream_ctxs[ i ].audio_codec ) {
							case CAMERA_AUDIO_CODEC_G711A:
								frame.type	= FRAME_AUDIO_PCMA;
								break;

							case CAMERA_AUDIO_CODEC_G711U:
								frame.type	= FRAME_AUDIO_PCMU;
								break;

							case CAMERA_AUDIO_CODEC_AAC:
								frame.type	= FRAME_AUDIO_AAC;
								break;

							case CAMERA_AUDIO_CODEC_PCM:
								frame.type	= FRAME_AUDIO_PCM;
								break;

							default:
								dbg_warm( "Un-know Audio Type( %d )...", stream_ctxs[ i ].audio_codec );
						}
					} else {
						if ( stream_ctxs[ i ].video_codec == CAMERA_VIDEO_CODEC_H264 ) {
							if ( buffer->flags & RTSTREAM_PKT_FLAG_KEY ) {
								frame.type	= FRAME_VIDEO_H264_I;
								frame.iskey	= TRUE;
							} else {
								frame.type	= FRAME_VIDEO_H264_P;
								frame.iskey	= FALSE;
							}
						} else {
							frame.type	= FRAME_VIDEO_MJPEG;
							frame.iskey	= TRUE;
						}
					}

					stream_cb( i, &frame );

					if ( ( i == 0 ) && ( frame.iskey == TRUE ) ) {
						adc_valuem = light_sensor_read_adc();

						if ( adc_valuem < 100 ) {
							if ( ( ir_value == 0 ) && ( ir_counter == 0 ) ) {
								ir_counter = seconds();
							}
						} else if ( adc_valuem > 500 ) {
							if ( ( ir_value == 1 ) && ( ir_counter == 0 ) ) {
								ir_counter = seconds();
							}
						} else {
							ir_counter	= 0;
						}

						if ( ir_counter && ( ( ir_counter + ir_hold_duration ) < seconds() ) ) {
							if ( ir_value == 0 ) {
								ir_value = 1;
							} else {
								ir_value = 0;
							}

							ir_cut_enable( ir_value );
							isp_gray_mode_enable( ir_value );

							ir_counter	= 0;
						}
					}
				}

				rts_av_put_buffer( buffer );
				buffer	= NULL;
				cnt ++;
			}
		}

		if ( cnt == 0 ) {
			msleep( 30 );
		}
	}

	return 0;
}

//# define CODEC_VBR
# define CODEC_CBR
//# define CODEC_CVBR
static RtStream
codec_set_h264( CAMERA_STREAM stream_id, CodecCtx *ctx )
{
	struct rts_h264_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_h264_stream_cfg ) );

	cfg.isp_cfg.isp_id			= video_dev_map[ stream_id ];

	if ( cfg.isp_cfg.isp_id == 0 ) {
		if ( ( ctx->width > 1920 ) || ( ctx->height > 1080 ) ) {
			dbg_warm( "codec %d: Width/Height Error: %dx%d", cfg.isp_cfg.isp_id, ctx->width, ctx->height );
			return NULL;
		}
	} else {
		if ( ( ctx->width > 1280 ) || ( ctx->height > 720 ) ) {
			dbg_warm( "codec %d: Width/Height Error: %dx%d", cfg.isp_cfg.isp_id, ctx->width, ctx->height );
			return NULL;
		}
	}

	cfg.isp_cfg.format			= rts_v_fmt_yuv420semiplanar;
	cfg.isp_cfg.width			= ctx->width;
	cfg.isp_cfg.height			= ctx->height;
	cfg.isp_cfg.numerator		= 1;
	cfg.isp_cfg.denominator		= ctx->fps;
	cfg.isp_cfg.isp_buf_num		= 4;

	cfg.profile		= H264_PROFILE_HIGH;
	cfg.level		= H264_LEVEL_5_1;
	cfg.qp			= -1;
	cfg.bps			= ctx->kbps * 1024;
	cfg.gop			= ctx->fps;
	cfg.rotation	= rts_av_rotation_0;

	dbg_line( "cfg.isp_cfg.isp_id: %d, %dx%d, %dKbps, %dFPS", cfg.isp_cfg.isp_id, ctx->width, ctx->height, ctx->kbps, ctx->fps );

	stream	= rts_create_h264_stream( &cfg );

	// Set VBR
	if ( stream ) {
		struct rts_video_h264_ctrl ctrl;

		if ( !rts_video_get_h264_ctrl( stream, &ctrl ) ) {
# ifdef CODEC_VBR
			// VBR
			ctrl.bitrate_mode = RTS_BITRATE_MODE_VBR;
			/* (high) 30 to 50 (low) */
			ctrl.qp		=
			ctrl.max_qp =
			ctrl.min_qp = 30;
# elif defined CODEC_CVBR
			// CVBR
			ctrl.bitrate_mode = RTS_BITRATE_MODE_C_VBR;

			ctrl.max_bitrate = ctx->kbps * 1024;
			ctrl.min_bitrate = ctx->kbps * 512;
# else
			// CBR
			ctrl.bitrate_mode = RTS_BITRATE_MODE_CBR;
# endif
			rts_video_set_h264_ctrl( stream, &ctrl );
		}

	} else {
		dbg_warm( "Do rts_create_h264_stream Failed!!!" );
	}

	return stream;
}

static RtStream
codec_set_mjpeg( CAMERA_STREAM stream_id, CodecCtx *ctx )
{
	struct rts_mjpeg_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_mjpeg_stream_cfg ) );

	cfg.isp_cfg.isp_id			= video_dev_map[ stream_id ];
	cfg.isp_cfg.format			= rts_v_fmt_yuv420semiplanar;
	cfg.isp_cfg.width			= ctx->width;
	cfg.isp_cfg.height			= ctx->height;
	cfg.isp_cfg.numerator		= 1;
	cfg.isp_cfg.denominator		= ctx->fps;
	cfg.isp_cfg.isp_buf_num		= 4;

	cfg.rotation	= rts_av_rotation_0;

	dbg_line( "cfg.isp_cfg.isp_id: %d, %dx%d", cfg.isp_cfg.isp_id, ctx->width, ctx->height );

	stream	= rts_create_mjpeg_stream( &cfg );

	if ( stream ) {
		struct rts_video_mjpeg_ctrl ctrl;

		ctrl.normal_compress_rate = 10;

		rts_video_set_mjpeg_ctrl( stream, &ctrl );
	} else {
		dbg_warm( "Do rts_create_mjpeg_stream Failed!!!" );
	}

	return stream;
}

static RtStream
codec_set_aac()
{
	struct rts_audio_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_audio_stream_cfg ) );

	snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );
	cfg.rate		= audio_attr[ CAMERA_AUDIO_CODEC_AAC ].rate;
	cfg.format		= audio_attr[ CAMERA_AUDIO_CODEC_AAC ].sample;
	cfg.channels	= audio_attr[ CAMERA_AUDIO_CODEC_AAC ].channel;

	stream = rts_create_audio_capture_encode_stream( &cfg );

	if ( stream ) {
		struct rts_encode_control ctrl;

		memset( &ctrl, 0, sizeof( struct rts_encode_control ) );

		ctrl.type		= RTS_AUDIO_TYPE_ID_AAC;
		ctrl.rate		= audio_attr[ CAMERA_AUDIO_CODEC_AAC ].rate;
		ctrl.bitrate 	= 96 * 1024;
		ctrl.channels	= audio_attr[ CAMERA_AUDIO_CODEC_AAC ].channel;

		rts_audio_set_encode_ctrl( stream, &ctrl );
	} else {
		dbg_warm( "Do rts_create_audio_capture_encode_stream Failed!!!" );
	}

	return stream;
}

static RtStream
codec_set_g711a()
{
	struct rts_audio_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_audio_stream_cfg ) );

	snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );
	cfg.rate		= audio_attr[ CAMERA_AUDIO_CODEC_G711A ].rate;
	cfg.format		= audio_attr[ CAMERA_AUDIO_CODEC_G711A ].sample;
	cfg.channels	= audio_attr[ CAMERA_AUDIO_CODEC_G711A ].channel;

	stream = rts_create_audio_capture_encode_stream( &cfg );

	if ( stream ) {
		struct rts_encode_control ctrl;

		memset( &ctrl, 0, sizeof( struct rts_encode_control ) );

		ctrl.type		= RTS_AUDIO_TYPE_ID_ALAW;

		rts_audio_set_encode_ctrl( stream, &ctrl );
	} else {
		dbg_warm( "Do rts_create_audio_capture_encode_stream Failed!!!" );
	}

	return stream;
}

static RtStream
codec_set_g711u()
{
	struct rts_audio_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_audio_stream_cfg ) );

	snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );
	cfg.rate		= audio_attr[ CAMERA_AUDIO_CODEC_G711U ].rate;
	cfg.format		= audio_attr[ CAMERA_AUDIO_CODEC_G711U ].sample;
	cfg.channels	= audio_attr[ CAMERA_AUDIO_CODEC_G711U ].channel;

	stream = rts_create_audio_capture_encode_stream( &cfg );

	if ( stream ) {
		struct rts_encode_control ctrl;

		memset( &ctrl, 0, sizeof( struct rts_encode_control ) );

		ctrl.type		= RTS_AUDIO_TYPE_ID_ULAW;
		rts_audio_set_encode_ctrl( stream, &ctrl );
	} else {
		dbg_warm( "Do rts_create_audio_capture_encode_stream Failed!!!" );
	}

	return stream;
}

static RtStream
codec_set_pcm()
{
	struct rts_audio_stream_cfg cfg;
	RtStream	stream;

	memset( &cfg, 0, sizeof( struct rts_audio_stream_cfg ) );

	snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );
	cfg.rate		= audio_attr[ CAMERA_AUDIO_CODEC_PCM ].rate;
	cfg.format		= audio_attr[ CAMERA_AUDIO_CODEC_PCM ].sample;
	cfg.channels	= audio_attr[ CAMERA_AUDIO_CODEC_PCM ].channel;

	stream = rts_create_audio_capture_stream( &cfg );

	if ( stream ) {
/*
		struct rts_encode_control ctrl;

		memset( &ctrl, 0, sizeof( struct rts_encode_control ) );

		ctrl.type		= RTS_AUDIO_TYPE_ID_PCM;

		rts_audio_set_encode_ctrl( stream, &ctrl );
*/
	} else {
		dbg_warm( "Do rts_create_audio_capture_encode_stream Failed!!!" );
	}

	return stream;
}

#include "RT_AecNs_API.h"
#include "AecNs_Parameters.h"

static RtStream
codec_set_audio( CAMERA_AUDIO_CODEC codec )
{
	RtStream	stream = NULL;

	switch ( codec ) {
		case CAMERA_AUDIO_CODEC_G711A:
			stream = codec_set_g711a();
			break;

		case CAMERA_AUDIO_CODEC_G711U:
			stream = codec_set_g711u();
			break;

		case CAMERA_AUDIO_CODEC_AAC:
			stream = codec_set_aac();
			break;

		case CAMERA_AUDIO_CODEC_PCM:
			stream = codec_set_pcm();
			break;

		default:
			dbg_warm( "Unknow Audio Type" );
			return NULL;
	}

	if ( stream ) {
		struct rts_aec_control pctrl;

		memset( &pctrl, 0, sizeof( struct rts_aec_control ) );

		pctrl.aec_enable	= 1;
		pctrl.ns_enable		= 1;

		/* Create object */
		void *AecNs_API = RTAecNs_API_context_Create(16000);

		AecNs_Para0 Para0;
		AecNs_Para1 Para1;
		AecNs_Para2 Para2;

		if ( AecNs_API == NULL ) {
			dbg_warm( "Do RTAecNs_API_context_Create Failed!!!" );
		}

		/* Set parameters */
		Para0.Enable = TRUE;
		Para1.Enable = TRUE;

		Para2.SpkDelay = 0;
		Para2.MicDelay = 0;

		/* Set AEC Enable */
		RTAecNs_API_Set( AecNs_API, &Para0, 4, 0 );

		/* Set NS Enable */
		RTAecNs_API_Set( AecNs_API, &Para1, 4, 1 );

		/* Set AEC signal delay(in msec) */
		RTAecNs_API_Set( AecNs_API, &Para2, 4, 2 );

		if ( rts_audio_set_aec_ctrl( stream, &pctrl ) ) {
			dbg_warm( "Do rts_audio_set_aec_ctrl Failed!!!" );
		}
	} else {
		dbg_warm( "Do rts_create_audio_capture_encode_stream Failed!!!" );
	}

	return stream;
}

static CodecHandle codec_handle[ CAMERA_VIDEO_CODEC_MAX ]	= {
	{ codec_set_mjpeg, },
	{ codec_set_h264, },
};

static void
camera_stop_stream( StreamCtx *ctx )
{
	if ( ctx->stream ) {
		ctx->stop	= TRUE;

		while ( ctx->play ) {
			msleep( 100 );
		}

		rts_av_cancel( ctx->stream );
		rts_destroy_stream( ctx->stream );

		ctx->stream	= NULL;
	}
}

static int
camera_start_stream( StreamCtx *ctx )
{
	if ( ctx->stream ) {
		int ret = rts_av_apply( ctx->stream );
		if ( ret ) {
			dbg_warm( "Do rts_av_apply Failed( %d )!!!", ret );
			return FAILURE;
		}

		ctx->play	= 1;
		return SUCCESS;
	} else {
		return FAILURE;
	}
}

static int
osd_show_info( struct rts_video_osd_attr *attr, OsdCtx *ctx, u32 font_size )
{
	int i, bid = 0, offset = 10, ret = SUCCESS, osd_char_w, osd_char_h;

	struct rts_video_osd_block	*pblock;
	struct rts_osd_text_t		showtext_ch		= { NULL, 0 };
	struct rts_osd_char_t		*showchar_ch	= NULL;

	attr->osd_char_w = font_size;
	attr->osd_char_h = font_size;

	attr->single_lib_name = "./eng.bin";

	osd_char_w	= ( attr->osd_char_w / 2 + 2 * 2 );
	osd_char_h	= attr->osd_char_h + ( 2 * 2 );

	pblock = &attr->blocks[ bid ];
	if ( ctx->show_date ) {
		pblock->rect.left	= offset;
		pblock->rect.right	= offset + 10 * osd_char_w;

		attr->date_fmt		= osd_date_fmt_10;
		attr->date_blkidx	= bid;
		attr->date_pos		= 0;

		offset = pblock->rect.right + osd_char_w;
		bid ++;
	}

	pblock = &attr->blocks[ bid ];
	if ( ctx->show_time ) {
		pblock->rect.left	= offset;
		pblock->rect.right	= offset + 8 * osd_char_w;

		attr->time_fmt		= osd_time_fmt_24;
		attr->time_blkidx	= bid;
		attr->time_pos		= 0;

		offset = pblock->rect.right + osd_char_w;
		bid ++;
	}

	pblock = &attr->blocks[ bid ];
	if ( ctx->show_string ) {
		if ( ctx->string && ctx->len ) {
			showtext_ch.count	= ctx->len;
			showtext_ch.content	=
			showchar_ch	= ( struct rts_osd_char_t * )malloc( sizeof( struct rts_osd_char_t ) * showtext_ch.count );

			memset( showchar_ch, 0, sizeof( struct rts_osd_char_t ) * showtext_ch.count );

			for ( i = 0; i < ctx->len; i ++ ) {
				showchar_ch[ i ].char_type			= char_type_single;
				showchar_ch[ i ].char_value[ 0 ]	= ( char )ctx->string[ i ];
			}

			pblock->rect.left	= offset;
			pblock->rect.right	= offset + ctx->len * osd_char_w;
			pblock->pshowtext	= &showtext_ch;
			bid ++;
		}
	}

	for ( i = 0; i < bid; i ++ ) {
		pblock = &attr->blocks[ i ];

		pblock->rect.top			= 10;
		pblock->rect.bottom			= pblock->rect.top + osd_char_h;
		pblock->bg_enable			= 0;
		pblock->flick_enable		= 0;
		pblock->ch_color			= 0xFFFFFF;
		pblock->bg_color			= 0x000000;
		pblock->char_color_alpha	= 0;
		pblock->stroke_enable		= 1;
		pblock->stroke_delta		= 128;
	}

	if ( rts_set_isp_osd_attr( attr ) ) {
		dbg_warm( "Do rts_set_isp_osd_attr Failed!!!" );
		ret = FAILURE;
	}

	if ( showchar_ch ) {
		free( showchar_ch );
	}

	return ret;
}

static int
osd_hide( struct rts_video_osd_attr *attr )
{
	int i;
	struct rts_video_osd_block	*pblock;

	for ( i = 0; i < attr->number; i++ ) {
		pblock = &attr->blocks[ i ];
		pblock->pbitmap			= NULL;
		pblock->pshowtext		= NULL;
		pblock->bg_enable		= 0;
		pblock->flick_enable	= 0;
		pblock->stroke_enable	= 0;
	}

	attr->time_fmt	= osd_time_fmt_no;
	attr->date_fmt	= osd_date_fmt_no;
	attr->single_lib_name	= NULL;
	attr->double_lib_name	= NULL;

	if ( rts_set_isp_osd_attr( attr ) ) {
		dbg_warm( "Do rts_set_isp_osd_attr Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

//==========================================================================
// APIs
//==========================================================================
int
Camera_ShowOSD( OsdCtx *ctx )
{
	int i;
	struct rts_video_osd_attr	*attr	= NULL;

	for ( i = 0; i < CAMERA_STREAM_VIDEO_MAX; i ++ ) {
		if ( stream_ctxs[ i ].stream == NULL ) {
			continue;
		} else if ( rts_query_isp_osd_attr( i, &attr ) ) {
			dbg_line( "Do rts_query_isp_osd_attr Failed!!!" );
			return FAILURE;
		}

		if ( attr == NULL ) {
			continue;
		} else if ( attr->number ) {
			if ( i >= attr->number ) {
				dbg_line( "i( %d ) >= attr->number( %d )", i, attr->number );
				continue;
			} else {
				if ( ctx && ( ctx->show_date || ctx->show_time || ctx->show_string ) ) {
					osd_show_info( attr, ctx, stream_ctxs[ i ].font_size );
				} else {
					osd_hide( attr );
				}
			}

			rts_release_isp_osd_attr( attr );
		}
	}

	return SUCCESS;
}

int
Camera_SetMask( u32 id, MaskCtx *ctx )
{
	struct rts_video_mask_attr *attr = NULL;
	int ret	= SUCCESS;

	if ( ctx == NULL ) {
		return FAILURE;
	} else if ( rts_query_isp_mask_attr( &attr, 1280, 720 ) ) {
		return FAILURE;
	} else if ( attr->number == 0 ) {
		ret = FAILURE;
	} else if ( id >= attr->number ) {
		ret = FAILURE;
	} else {
		struct rts_video_mask_block *pblock = &attr->blocks[ id ];

		pblock->enable			= ctx->enable;

		if ( pblock->enable ) {
			if ( pblock->type == RTS_ISP_BLK_TYPE_RECT ) {
				pblock->rect.left		= ctx->x;
				pblock->rect.top		= ctx->y;
				pblock->rect.right		= ctx->x + ctx->width;
				pblock->rect.bottom		= ctx->y + ctx->height;

				if ( pblock->rect.left > pblock->res_width ) {
					pblock->rect.left	= pblock->res_width;
				}

				if ( pblock->rect.top > pblock->res_height ) {
					pblock->rect.top	= pblock->res_height;
				}

				if ( pblock->rect.right > pblock->res_width ) {
					pblock->rect.right	= pblock->res_width;
				}

				if ( pblock->rect.bottom > pblock->res_height ) {
					pblock->rect.bottom	= pblock->res_height;
				}
			} else if ( pblock->type == RTS_ISP_BLK_TYPE_GRID ) {
				pblock->area.columns		= 1;
				pblock->area.rows			= 1;

				if ( ctx->x > pblock->res_width ) {
					pblock->area.left		= pblock->res_width;
				} else {
					pblock->area.left		= ctx->x;
				}

				if ( ctx->y > pblock->res_height ) {
					pblock->area.top		= pblock->res_height;
				} else {
					pblock->area.top		= ctx->y;
				}

				if ( ( pblock->area.left + ctx->width ) > pblock->res_width ) {
					pblock->area.cell.width		= pblock->res_width - pblock->area.left;
				} else {
					pblock->area.cell.width		= ctx->width;
				}

				if ( ( pblock->area.top + ctx->height ) > pblock->res_height ) {
					pblock->area.cell.height	= pblock->res_height - pblock->area.top;
				} else {
					pblock->area.cell.height	= ctx->height;
				}

				if ( pblock->area.cell.width > CAMERA_MASK_WIDTH_LIMIT ) {
					pblock->area.cell.width		= CAMERA_MASK_WIDTH_LIMIT;
				}

				if ( pblock->area.cell.height > CAMERA_MASK_HEIGHT_LIMIT ) {
					pblock->area.cell.height	= CAMERA_MASK_HEIGHT_LIMIT;
				}

				pblock->area.length			= RTS_DIV_ROUND_UP( ( pblock->area.columns * pblock->area.rows ), 8 );
				if ( pblock->area.length > RTS_GRID_BITMAP_SIZE ) {
					pblock->area.length = RTS_GRID_BITMAP_SIZE;
				}

				memset( pblock->area.bitmap, 0xFF, RTS_GRID_BITMAP_SIZE );
			}
		}

		attr->color	= 0x00000000;

		if ( rts_set_isp_mask_attr( attr ) ) {
			dbg_warm( "Do rts_set_isp_mask_attr Failed!!!" );
			ret = FAILURE;
		}
	}

	rts_release_isp_mask_attr( attr );

	return ret;
}

int
Camera_SetMDCBFunc( MD_CBFunc cb )
{
	md_cb	= cb;
	return SUCCESS;
}

int
Camera_SetMD( u32 id, MDCtx *ctx )
{
	struct rts_video_md_attr *attr = NULL;
	int ret	= SUCCESS;

	if ( ctx == NULL ) {
		return FAILURE;
	} else if ( rts_query_isp_md_attr( &attr, 1280, 720 ) ) {
		return FAILURE;
	} else if ( attr->number == 0 ) {
		ret = FAILURE;
	} else if ( id >= attr->number ) {
		ret = FAILURE;
	} else {
		struct rts_video_md_block *pblock = &attr->blocks[ id ];

		pblock->enable			= ctx->enable;

		if ( pblock->enable ) {
			pblock->type			= RTS_ISP_BLK_TYPE_GRID;

			pblock->area.cell.width		= MD_GRID_WIDTH;
			pblock->area.cell.height	= MD_GRID_HEIGHT;


			if ( ctx->x > pblock->res_width ) {
				pblock->area.left		= pblock->res_width;
			} else {
				pblock->area.left		= ctx->x;
			}

			if ( ctx->y > pblock->res_height ) {
				pblock->area.top		= pblock->res_height;
			} else {
				pblock->area.top		= ctx->y;
			}

			if ( ( pblock->area.left + ctx->width ) > pblock->res_width ) {
				pblock->area.columns	= ( pblock->res_width - pblock->area.left ) / MD_GRID_WIDTH;
			} else {
				pblock->area.columns	= ctx->width / MD_GRID_WIDTH;
			}

			if ( ( pblock->area.top + ctx->height ) > pblock->res_height ) {
				pblock->area.rows		= ( pblock->res_height - pblock->area.top ) / MD_GRID_HEIGHT;
			} else {
				pblock->area.rows		= ctx->height / MD_GRID_HEIGHT;
			}

			pblock->area.length			= RTS_DIV_ROUND_UP( ( pblock->area.columns * pblock->area.rows ), 8 );
			if ( pblock->area.length > RTS_GRID_BITMAP_SIZE ) {
				pblock->area.length = RTS_GRID_BITMAP_SIZE;
			}

			memset( pblock->area.bitmap, 0xFF, RTS_GRID_BITMAP_SIZE );

			pblock->sensitivity		= ctx->sensitivity;
			pblock->percentage		= ctx->percentage;
			pblock->frame_interval	= 5;
		}

		if ( rts_set_isp_md_attr( attr ) ) {
			dbg_warm( "Do rts_set_isp_md_attr Failed!!!" );
			ret = FAILURE;
		}
	}

	rts_release_isp_md_attr( attr );

	return ret;
}

int
Camera_SetImage( ImageCtx *ctx, char force )
{
	ISPTable	*isp_item;
	int	*value, *new_value;

	if ( ctx == NULL ) {
		return FAILURE;
	}

	isp_item	= &ips_tbl[ 0 ];
	while ( isp_item->name ) {
		value		= ( int * )( ( void * )&img_ctx + isp_item->pos );
		new_value	= ( int * )( ( void * )ctx + isp_item->pos );

		if ( force || ( *value != *new_value ) ) {
			if ( ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_AE_GAIN ) ||
				 ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_EXPOSURE_TIME ) ) {
				if ( ctx->ae.mode != RTS_ISP_AE_MANUAL ) {
					isp_item ++;
					continue;
				}
				dbg_line( "isp_item->cmd_id = %d", isp_item->cmd_id );
			}

			if ( isp_item->cmd_id == RTS_VIDEO_CTRL_ID_WDR_LEVEL ) {
				if ( ctx->wdr.mode != RTS_ISP_WDR_MANUAL ) {
					isp_item ++;
					continue;
				}
				dbg_line( "isp_item->cmd_id = %d", isp_item->cmd_id );
			}

			if ( img_set_isp( isp_item->cmd_id, *new_value ) != SUCCESS ) {
				dbg_line( "Do img_set_isp(%s: %d) Failed!!!", isp_item->name, *new_value );
			} else {
				*value = *new_value;
			}
		}

		isp_item ++;
	}

	//img_print_isp( ctx );

	return SUCCESS;
}

int
Camera_GetImage( ImageCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	}

	memcpy( ctx, &img_ctx, sizeof( ImageCtx ) );

	return SUCCESS;
}

int
Camera_SetVideoCodec( CAMERA_STREAM stream, CodecCtx *ctx )
{
	if ( ( stream >= CAMERA_STREAM_VIDEO_MAX ) || ( ctx == NULL ) ) {
		return FAILURE;
	} else if ( memcmp( &stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) ) == 0 ) {
		return SUCCESS;
	}

	camera_stop_stream( &stream_ctxs[ stream ] );

	stream_ctxs[ stream ].video_codec	= codec_map[ stream ];
	stream_ctxs[ stream ].stream		= codec_handle[ stream_ctxs[ stream ].video_codec ].set_video( stream, ctx );

	if ( stream_ctxs[ stream ].stream == NULL ) {
		dbg_warm( "Do set_codec Failed!!!" );
		return FAILURE;
	} else if ( camera_start_stream( &stream_ctxs[ stream ] ) ) {
		dbg_warm( "Do camera_start_stream Failed!!!" );
		return FAILURE;
	}

	memcpy( &stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) );

	if ( stream_ctxs[ stream ].codec_ctx.width >= 960 ) {
		stream_ctxs[ stream ].font_size		= 24;
	} else if ( stream_ctxs[ stream ].codec_ctx.width >= 640 ) {
		stream_ctxs[ stream ].font_size		= 12;
	} else {
		stream_ctxs[ stream ].font_size		= 8;
	}

	return SUCCESS;
}

int
Camera_EnableAudio( char on )
{
	camera_stop_stream( &stream_ctxs[ CAMERA_STREAM_AUDIO ] );

	if ( on ) {
		stream_ctxs[ CAMERA_STREAM_AUDIO ].audio_codec	= CAMERA_AUDIO_CODEC_DEF;
		stream_ctxs[ CAMERA_STREAM_AUDIO ].stream		= codec_set_audio( CAMERA_AUDIO_CODEC_DEF );

		if ( stream_ctxs[ CAMERA_STREAM_AUDIO ].stream == NULL ) {
			dbg_warm( "Do set_codec Failed!!!" );
			return FAILURE;
		} else if ( camera_start_stream( &stream_ctxs[ CAMERA_STREAM_AUDIO ] ) ) {
			dbg_warm( "Do camera_start_stream Failed!!!" );
			return FAILURE;
		}
	}

	return SUCCESS;
}

int
Camera_GetAudioInConf( AudioConf *conf )
{
	if ( conf ) {
		conf->sampling_freq	= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].rate;
		conf->channels		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].channel;
		conf->sample		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].sample;

		if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_AAC ) {
			conf->type	= FRAME_AUDIO_AAC;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711U ) {
			conf->type	= FRAME_AUDIO_PCMU;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711A ) {
			conf->type	= FRAME_AUDIO_PCMA;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_PCM ) {
			conf->type	= FRAME_AUDIO_PCM;
		}

		return SUCCESS;
	}

	return FAILURE;
}

int
Camera_SetAudioInVolume( u32 volume )
{
	if ( volume > 100 ) {
		dbg_line( "Parameter Error( %u )!!!", volume );
		return FAILURE;
	} else if ( rts_audio_set_capture_volume( volume ) ) {
		dbg_line( "Fail to set capture volume!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

int
Camera_SetAudioOutVolume( u32 volume )
{
	if ( volume > 100 ) {
		dbg_line( "Parameter Error( %u )!!!", volume );
		return FAILURE;
	} else if ( rts_audio_set_playback_volume( volume ) ) {
		dbg_line( "Fail to set playback volume!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

int
Camera_OpenAudioOut()
{
	if ( audio_out.stream == NULL ) {
		struct rts_audio_stream_cfg		cfg;
		struct rts_amixer_control		ctrl;

		memset( &cfg, 0, sizeof( struct rts_audio_stream_cfg ) );
		memset( &ctrl, 0, sizeof( struct rts_amixer_control ) );

		snprintf( cfg.dev_node, sizeof( cfg.dev_node ), RTS_AUDIO_DEV );
		cfg.channels	= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].channel;
		cfg.rate		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].rate;
		cfg.format		= audio_attr[ CAMERA_AUDIO_CODEC_DEF ].sample;

		audio_out.stream = rts_create_audio_mixer_playback_stream( &cfg );
		if ( audio_out.stream == NULL ) {
			dbg_warm( "Do rts_create_audio_playback_stream Failed!!!" );
			return FAILURE;
		} else if ( rts_av_apply( audio_out.stream ) ) {
			dbg_warm( "Do rts_av_apply( audio_out.stream ) Failed!!!" );
			return FAILURE;
		}

		ctrl.index		= 0;

		if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711A ) {
			ctrl.type		= RTS_AUDIO_TYPE_ID_ULAW;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_G711U ) {
			ctrl.type		= RTS_AUDIO_TYPE_ID_ULAW;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_AAC ) {
			ctrl.type		= RTS_AUDIO_TYPE_ID_AAC;
		} else if ( CAMERA_AUDIO_CODEC_DEF == CAMERA_AUDIO_CODEC_PCM ) {
			ctrl.type		= RTS_AUDIO_TYPE_ID_PCM;
		}

		ctrl.state		= RTS_AUDIO_STATE_ID_START;
		ctrl.flags		&= ~RTS_AUDIO_FLAG_ID_STREAM;

		if ( rts_audio_set_mixer_ctrl( audio_out.stream, &ctrl ) ) {
			dbg_warm( "Do rts_audio_set_mixer_ctrl Failed!!!" );
		}
	}

	return SUCCESS;
}

int
Camera_CloseAudioOut()
{
	if ( audio_out.stream ) {
		rts_av_cancel( audio_out.stream );
		rts_destroy_stream( audio_out.stream );
		audio_out.stream = NULL;
	}

	return SUCCESS;
}

int
Camera_PlayAudio( u8 *data, u32 size )
{
	if ( ( data == NULL ) || ( size == 0 ) ) {
		return FAILURE;
	} else if ( size > audio_out.chunk_bytes ) {
		dbg_warm( "Audio Over Buffer Size( %d > %d )!!!", size, audio_out.chunk_bytes );
		return FAILURE;
	} else if ( audio_out.stream == NULL ) {
		return FAILURE;
	} else {
		struct rts_av_buffer *buffer = NULL;
		int i;

		for ( i = 0; i < RTS_AUDIO_MAX_BUF_NUM; i ++ ) {
			if ( rts_av_get_buffer_refs( audio_out.buffer[ i ] ) == 0 ) {
				buffer = audio_out.buffer[ i ];
				break;
			}
		}

		if ( buffer == NULL ) {
			dbg_warm( "Can not get free buffer( %u )!!!", size );
			return FAILURE;
		}

		memcpy( buffer->vm_addr, data, size );
		buffer->bytesused	= size;
		buffer->timestamp	= 0;
		buffer->index		= 0;

		if ( rts_av_stream_send( audio_out.stream, buffer ) ) {
			dbg_warm( "Do rts_av_stream_send Failed!!!" );
			return FAILURE;
		}

		return SUCCESS;
	}
}

int
Camera_SetStreamCBFunc( Stream_CBFunc cb )
{
	stream_cb	= cb;
	return SUCCESS;
}

int
Camera_Init()
{
	ISPTable	*isp_item;
	int	*value, i;

	memset( stream_ctxs, 0, sizeof( stream_ctxs ) );
	memset( &img_ctx, 0, sizeof( img_ctx ) );

	if ( rts_av_init() ) {
		dbg_warm( "Do rts_av_init Failed !!!" );
		return FAILURE;
	}

	create_norm_thread( stream_proc, NULL, 0 );

	isp_item	= &ips_tbl[ 0 ];
	while ( isp_item->name ) {
		value	= ( int * )( ( void * )&img_ctx + isp_item->pos );
		*value	= img_get_isp_default( isp_item->cmd_id, &isp_item->max, &isp_item->min );

		if ( *value < 0 ) {
			dbg_line( "Do img_get_isp_default(%s) Failed!!!", isp_item->name );
			*value = 0;
		}
		//dbg_line( "%s: %d( %d ~ %d )", isp_item->name, *value, isp_item->min, isp_item->max );

		isp_item ++;
	}

	//img_print_isp( &img_ctx );

	create_norm_thread( md_proc, NULL, 0 );

	audio_out.stream			= NULL;
	audio_out.chunk_bytes		= 4096;
	for ( i = 0; i < RTS_AUDIO_MAX_BUF_NUM; i ++ ) {
		audio_out.buffer[ i ] = rts_av_new_buffer( audio_out.chunk_bytes );
		if ( audio_out.buffer[ i ] == NULL ) {
			dbg_warm( "no memory for audio buffer" );
			break;
		}
	}

	return SUCCESS;
}

void
Camera_Deinit()
{
	int i;

	for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
		stream_ctxs[ i ].stop	= TRUE;
	}

	msleep( 500 );

	for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
		if ( stream_ctxs[ i ].stream ) {
			rts_av_cancel( stream_ctxs[ i ].stream );
			rts_destroy_stream( stream_ctxs[ i ].stream );
		}
	}

	Camera_CloseAudioOut();

	for ( i = 0; i < RTS_AUDIO_MAX_BUF_NUM; i++ ) {
		if ( audio_out.buffer[ i ] ) {
			rts_av_delete_buffer( audio_out.buffer[ i ] );
			audio_out.buffer[ i ] = NULL;
		}
	}

	rts_av_release();
	dbg_error();
}

