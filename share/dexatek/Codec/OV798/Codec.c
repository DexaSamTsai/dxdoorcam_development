/*
 * camera.c
 *
 *  Created on: 2017/10/24
 *      Author: Sam Tsai
 */
//==========================================================================
// Include File
//==========================================================================
# include "Codec.h"

//==========================================================================
// Type Declaration
//==========================================================================
# define STREAMCTRL_AUDIO_IN					0x01
# define STREAMCTRL_AUDIO_OUT					0x02
# define STREAMCTRL_AUDIO_ALL					0x03

# ifdef CONFIG_AUDIO_AEC_EN
#	define AUDIO_SAMPLE_RATE					16000
# else
#	define AUDIO_SAMPLE_RATE					16000
# endif

# define FIELD_OFFSET( field )					( ( unsigned long )&( ( ( ImageCtx * )0 )->field ) )
# define MPUCMD_TYPE_NOSUPPORT					0x00090000

# define IMAGE_AREA_WIDTH						1920
# define IMAGE_AREA_HEIGHT						1080

# define MD_AREA_WIDTH							320
# define MD_AREA_HEIGHT							180

typedef struct {
    char           *name;
    u32             cmd_id;
    int				( *set )	( int, int );
    int				( *get )	( int );
    unsigned long   pos;
    int             max;
    int             min;
} ISPTable;

typedef enum {
	CAMERA_VIDEO_CODEC_MJPEG	= 0,
	CAMERA_VIDEO_CODEC_H264,
	CAMERA_VIDEO_CODEC_MAX,
} CAMERA_VIDEO_CODEC;

typedef enum {
	CAMERA_AUDIO_CODEC_G711A	= 0,
	CAMERA_AUDIO_CODEC_G711U,
	CAMERA_AUDIO_CODEC_AAC,
	CAMERA_AUDIO_CODEC_PCM,
	CAMERA_AUDIO_CODEC_MAX,
	CAMERA_AUDIO_CODEC_DEF		= CAMERA_AUDIO_CODEC_AAC,
	CAMERA_AUDIO_OUT_CODEC_DEF	= CAMERA_AUDIO_CODEC_PCM,
} CAMERA_AUDIO_CODEC;

typedef enum {
    OV798_ISP_AWB_AUTO	= 0,
    OV798_ISP_AWB_MANUAL,
    OV798_ISP_AWB_MAX,
} OV798_ISP_AWB_;

typedef enum {
	ISPCMD_TODO	= 0,
	ISPCMD_SET_BRIGHTNESS,
	ISPCMD_SET_CONTRAST,
	ISPCMD_SET_SATURATION,
	ISPCMD_SET_SHARPNESS,
	ISPCMD_SET_POWERFREQ,//5
	ISPCMD_SET_FLIP,
	ISPCMD_SET_MIRROR,
	ISPCMD_SET_ROI,
	ISPCMD_SET_AWB_MANUAL_EN,
	ISPCMD_SET_AWB_MANUAL_R,//10
	ISPCMD_SET_AWB_MANUAL_G,
	ISPCMD_SET_AWB_MANUAL_B,
	ISPCMD_SET_WDR,
	ISPCMD_SET_DAYNIGHT,
	ISPCMD_SET_AEC_MANUAL_EN,//15
	ISPCMD_SET_AEC_MANUAL_GAIN,
	ISPCMD_SET_AEC_MANUAL_EXP,
	ISPCMD_MAX,
} ISPCMD_CMD;

typedef struct {
	u32							num;

	char						play;
	char						stop;

	CAMERA_VIDEO_CODEC			video_codec;
	CAMERA_AUDIO_CODEC			audio_codec;

	CodecCtx					codec_ctx;

	u32							font_size;
} StreamCtx;

//extern u32 vstream_handle_cmd(u32 cmd, u32 arg);
//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//CONFIG_AUDIO_ENC
static t_arec_inpara		arec_inpara;

//CONFIG_AUDIO_DEC
static t_adec_inpara		adec_inpara;

static t_video_analysis_res	va_res;

static struct {
	StreamCtx			stream_ctxs[ CAMERA_STREAM_MAX ];

	ImageCtx			img_ctx;
	MD_CBFunc			md_cb;
	Stream_CBFunc		stream_cb;
	Audio_CBFunc		audio_cb;

	int					day_night_mode;			//0:day mode;1:night mode;2:auto mode
	int					crop_coordinate;
	int					crop_size;
	int					scale_size;
	int					frame_rate;

	int					av_setting_update;		// value == 1 indicates av setting(scale,crop,etc) needs to be updated

	int					always_on_mode;			// value 1 indicates the audio/video encoder is always on
	int					motion_is_detected;		// motion is detected or not. (Only VENC stream is taken into care currently)
} codec;

//static struct {
//	StreamCtx			stream_ctxs[ CAMERA_STREAM_MAX ];
//	DecodeAudioCtx		ao;
//	ImageCtx			img_ctx;
//
//	struct {
//		char			show_date;
//		char			show_time;
//		char			show_string;
//		u8				*string;
//
//		u32				offset;
//	} osd;
//
//	struct {
//		md_area						area[ MD_AREA_MAX ];
//
//		struct rts_video_md_attr	*attr;
//	} motion;
//
//	struct {
//		double	pcm_sum;
//		u32		pcm_packet_cnt;
//	} audio;
//
//	Stream_CBFunc		stream_cb;
//	MD_CBFunc			md_cb;
//	Audio_CBFunc		audio_cb;
//
//	FT_Library			library;
//	FT_Face				face;
//
//} codec;

static ISPTable ips_tbl[]    = {
	{ "Brightness",		ISPCMD_SET_BRIGHTNESS,		libisp_set_brightness,		NULL,	FIELD_OFFSET( brightness ), },
	{ "Contrast",		ISPCMD_SET_CONTRAST,		libvs_set_contrast,			NULL,	FIELD_OFFSET( contrast ),			4, 0 },
//	{ "Hue",			ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( hue ), },
	{ "Saturation",		ISPCMD_SET_SATURATION,		libvs_set_saturation,		NULL,	FIELD_OFFSET( saturation ),			7, 0 },
	{ "Sharpness",		ISPCMD_SET_SHARPNESS,		libisp_set_sharpness,		NULL,	FIELD_OFFSET( sharpness ),			4, 0 },
//	{ "Gamma",			ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( gamma ), },

//	{ "BLC",            ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( blc ), },
//	{ "Gain",           ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( gain ), },
	{ "PowerFreq",      ISPCMD_SET_POWERFREQ,		libvs_set_lightfreq,		NULL,	FIELD_OFFSET( power_freq ),			2, 0 },

//	{ "Roll",           ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( roll ), },
	{ "Flip",           ISPCMD_SET_FLIP,			libvs_set_flip,				NULL,	FIELD_OFFSET( flip ),				1, 0 },
	{ "Mirror",         ISPCMD_SET_MIRROR,			libvs_set_mirror,			NULL,	FIELD_OFFSET( mirror ),				1, 0 },
//	{ "Rotate",         ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( rotate ), },

	{ "ROI",            ISPCMD_SET_ROI,				libisp_set_roi,				NULL,	FIELD_OFFSET( roi ), },

//	{ "GrayMode",       ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( gray_mode ), },

	{ "WBMode",         ISPCMD_SET_AWB_MANUAL_EN,	libisp_set_aec_manual,		NULL,	FIELD_OFFSET( awb.mode ),			1, 0 },
//	{ "WBLevel",        ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( awb.level ), },
	{ "RGain",          ISPCMD_SET_AWB_MANUAL_R,	libisp_set_awb_r_gain,		NULL,	FIELD_OFFSET( awb.r_gain ), },
	{ "GGain",          ISPCMD_SET_AWB_MANUAL_G,	libisp_set_awb_g_gain,		NULL,	FIELD_OFFSET( awb.g_gain ), },
	{ "BGain",          ISPCMD_SET_AWB_MANUAL_B,	libisp_set_awb_b_gain,		NULL,	FIELD_OFFSET( awb.b_gain ), },

	{ "WDRMode",        ISPCMD_SET_WDR,				libvs_set_wdr,				NULL,	FIELD_OFFSET( wdr.mode ),			4, 0},
//	{ "WDRLevel",       ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( wdr.level ), },

//	{ "3DNR",           ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( dnr3 ), },
//	{ "Dehaze",         ISPCMD_TODO,				NULL,						NULL,	FIELD_OFFSET( dehaze ), },
	{ "IRMode",         ISPCMD_SET_DAYNIGHT,		libisp_set_daynight,		NULL,	FIELD_OFFSET( ir_mode ),			1, 0},

	{ "ExposureMode",   ISPCMD_SET_AEC_MANUAL_EN,	libisp_set_aec_manual,		NULL,	FIELD_OFFSET( ae.mode ),			1, 0 },
	{ "AEGain",         ISPCMD_SET_AEC_MANUAL_GAIN,	libisp_set_aec_manual_gain,	NULL,	FIELD_OFFSET( ae.gain ), },
	{ "ExposureTime",   ISPCMD_SET_AEC_MANUAL_EXP,	libisp_set_aec_manual_exp,	NULL,	FIELD_OFFSET( ae.exposure_time ),	3000, 0 },

	{ NULL,    0,    0, },
};

//==========================================================================
// Static Functions
//==========================================================================
static int
ov_audio_init( StreamCtx *stream, CAMERA_AUDIO_CODEC codec )
{
    //enc
    memset( &arec_inpara, 0, sizeof( t_arec_inpara ) );

    arec_inpara.fmt				= AUDIO_FMT_F4; //aac
    arec_inpara.sr				= AUDIO_SAMPLE_RATE;
    arec_inpara.ch				= 1;
    arec_inpara.dvol			= 10;

	// CONFIG_AUDIO_CODEC_EN_interncodec=y
    arec_inpara.aimode			= AI_REC_MASTER;

    arec_inpara.drop_samples	= 1;

	// CONFIG_AUDIO_DENOISE_EN=y
    arec_inpara.denoise_en		= 1;
    arec_inpara.compressor_en	= 1;

	// CONFIG_AUDIO_AEC_EN=y and CONFIG_AUDIO_DUPLEX_EN=y
    arec_inpara.aec_en			= 1;

    //adc
    //WriteReg8(AUDIO_BASE_ADDR + 0x13, 0xff);
    //28.25+24
    //WriteReg8(AUDIO_BASE_ADDR + 0x13, 0xf7);
    //19.72+20
    WriteReg8( ( AUDIO_BASE_ADDR + 0x13 ), 0xb1 );
    //19.72+26
    //WriteReg8(AUDIO_BASE_ADDR + 0x13, 0xb9);

	return SUCCESS;
}

static int
ov_va_detect_cb( void )
{
	if ( video_analysis_result( &va_res ) != 0 ) {
        dbg_line( "[ERROR]: video_analysis_result fail." );
        return FAILURE;
	}

    if ( va_res.is_true_alarm ) {
        if ( codec.md_cb ) {
        	int i;

        	for ( i = 0; i < MD_ALGO_MASK_MAXNUM; i ++ ) {
        		if ( va_res.is_true_alarm & ( 1 << i ) ) {
        			codec.md_cb( i, 0, 0 );
        		}
        	}
        }
    }

    return SUCCESS;
}

static void
ov_va_start( void )
{
	t_video_analysis_cfg cfg;

	memset( &cfg, 0, sizeof( cfg ) );
	cfg.vs_id	= 3;		// yuv stream id
	cfg.run_cb	= ov_va_detect_cb;

	video_analysis_start( &cfg );
}

static void
ov_va_stop( void )
{
	video_analysis_stop();
}

static int
ov_init( void )
{
    dbg_line( "ov_init start..." );

    if ( libdatapath_init( 0 ) != 0 ) {
        dbg_line( "Do libdatapath_init Failed!!" );
        return FAILURE;
    } else if ( libdatapath_start() != 0 ) {
        dbg_line( "Do libdatapath_start Failed!!" );
        return FAILURE;
    }

    libisp_init();

    //codec
    t_codec_cpara codec_para;

    memset( &codec_para, 0, sizeof( t_codec_cpara ) );
    codec_para.sr		= AUDIO_SAMPLE_RATE;
    codec_para.ch		= 1;
    codec_para.master	= 0;
    codec_para.dac_dvol	= 10;
    codec_para.adc_dvol	= 10;

	// CONFIG_AUDIO_DEC_EN=y and CONFIG_AUDIO_ENC_EN=y
    codec_para.dir		= AI_DIR_DUPLEX;

    libacodec_init( &codec_para );

	ov_va_start();

	libvs_set_mirror_flip( 0, 1 );

    return SUCCESS;
}

static int
stream_video_start( StreamCtx *stream )
{
    dbg_line( "Stream %d init start...\n", stream->num );

    if ( libvs_init( stream->num ) ) {
        dbg_error( "Stream (%d) libvs_init fail\n", stream->num );
        return FAILURE;
    } else if ( libvs_start( stream->num ) ) {
        dbg_error( "Stream (%d) libvs_start fail\n", stream->num );
        return FAILURE;
    }

    stream->play	= TRUE;

    dbg_line( "Stream %d init finish!", stream->num );
    return SUCCESS;
}

static int
stream_audio_start( StreamCtx *stream )
{
    dbg_line( "Stream %d init start...\n", stream->num );

    if ( libaenc_init( &arec_inpara ) < 0 ) {
        dbg_line( "libaenc_init failed\n" );
		return FAILURE;
    } else if ( libaenc_start() ) {
        dbg_line( "libaenc_start failed\n" );
		return FAILURE;
    }

    stream->play	= TRUE;
    dbg_line( "Stream %d init finish!", stream->num );

    return SUCCESS;
}

static int
stream_start( StreamCtx *stream )
{
	if ( stream->num == 4 ) {
		return stream_audio_start( stream );
	} else {
		return stream_video_start( stream );
	}
}

static int
stream_video_close( StreamCtx *stream )
{
    dbg_line( "Stream %d stoping...\n", stream->num );

    if ( libvs_stop( stream->num ) ) {
        dbg_error( "Stream (%d) libvs_stop fail\n", stream->num );
        return FAILURE;
    } else if ( libvs_exit( stream->num ) ) {
        dbg_error( "Stream (%d) libvs_exit fail\n", stream->num );
        return FAILURE;
    }

	stream->play	= FALSE;
    dbg_line( "Stream %d is stop!\n", stream->num );

    return SUCCESS;
}

static int
stream_audio_close( StreamCtx *stream )
{
    dbg_line( "Stream %d stoping...\n", stream->num );

    if ( libaenc_stop() ) {
        dbg_error( " libaenc_stop fail\n" );
        return FAILURE;
    } else if ( libaenc_exit() ) {
        dbg_error( " libaenc_exit fail\n" );
        return FAILURE;
    }

	stream->play	= FALSE;

    return SUCCESS;
}

static int
stream_close( StreamCtx *stream )
{
	if ( stream->num == 4 ) {
		return stream_audio_close( stream );
	} else {
		return stream_video_close( stream );
	}
}

static void
stream_stop( StreamCtx *stream )
{
	if ( stream->play == TRUE ) {
		stream->stop	= TRUE;

		while ( stream->play ) {
			MSleep( 100 );
		}

		stream_close( stream );
	}
}

static int
stream_stop_all( void )
{
    int id	= 0;

    for ( id = 0; id < CAMERA_STREAM_MAX; id ++ ) {
		stream_stop( &codec.stream_ctxs[ id ] );
    }

    return 0;
}

//TODO Add Day/nigh switch API
static void *
stream_proc( void *arg )
{
    AVFrame		avframe;
    FRAME		*frame;
	int			i, cnt;

    pthread_setname( NULL, "stream_proc" );

	for ( i = 0; i < 3; i ++ ) {
	    stream_start( &codec.stream_ctxs[ i ] );

	    if ( i == CAMERA_STREAM_VIDEO_MJPEG ) {
//	    	libvs_set_vs_framerate( codec.stream_ctxs[ i ].num, 1 );
	    } else {
	    	libvs_set_vs_framerate( codec.stream_ctxs[ i ].num, 24 );
	    	libvs_set_gop( codec.stream_ctxs[ i ].num, 24 );
		}
	}

	codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].audio_codec	= CAMERA_AUDIO_CODEC_DEF;

	if ( ov_audio_init( &codec.stream_ctxs[ CAMERA_STREAM_AUDIO ], CAMERA_AUDIO_CODEC_DEF ) == FAILURE ) {
		dbg_warm( "Do ov_audio_init Failed!!!" );
	} else if ( stream_start( &codec.stream_ctxs[ CAMERA_STREAM_AUDIO ] ) ) {
		dbg_warm( "Do stream_start Failed!!!" );
	}

    while ( 1 ) {
		cnt = 0;

        for ( i = 0; i < CAMERA_STREAM_MAX; i ++ ) {
			if ( i == CAMERA_STREAM_AUDIO ) {
        		frame = libaenc_get_frame( 1 );

        		if ( frame ) {
        			frame->type	= FRAME_TYPE_AUDIO_ENCAUDIO;
				}
			} else {
            	frame = libvs_get_frame( codec.stream_ctxs[ i ].num, 20 );
			}

            if ( frame ) {
    			if ( ( codec.stream_ctxs[ i ].play == TRUE ) &&
    				 ( codec.stream_cb != NULL ) ) {
                    avframe.buff	= ( void * )frame->addr;
                    avframe.size	= frame->size;

                    //TODO process if addr1 have date.....
                    if ( ( frame->type == FRAME_TYPE_VIDEO_I ) ||
                    	 ( frame->type == FRAME_TYPE_VIDEO_IDR ) ) {
                    	 if( codec.stream_ctxs[ i ].num == CAMERA_STREAM_VIDEO_MJPEG ){
                            avframe.type    = FRAME_VIDEO_MJPEG;
                    	 }else{
                            avframe.type    = FRAME_VIDEO_H264_I;
                    	 }
                        avframe.iskey	= TRUE;
                    } else if ( frame->type == FRAME_TYPE_VIDEO_P ) {
                        avframe.type	= FRAME_VIDEO_H264_P;
                        avframe.iskey	= FALSE;
                    } else if ( frame->type == FRAME_TYPE_AUDIO_ENCAUDIO ) {
						switch ( codec.stream_ctxs[ i ].audio_codec ) {
							case CAMERA_AUDIO_CODEC_G711A:
								avframe.type	= FRAME_AUDIO_PCMA;
								break;

							case CAMERA_AUDIO_CODEC_G711U:
								avframe.type	= FRAME_AUDIO_PCMU;
								break;

							case CAMERA_AUDIO_CODEC_AAC:
								avframe.type	= FRAME_AUDIO_AAC;
								break;

							case CAMERA_AUDIO_CODEC_PCM:
								avframe.type	= FRAME_AUDIO_PCM;
								break;

							default:
								dbg_warm( "Un-know Audio Type( %d )...", codec.stream_ctxs[ i ].audio_codec );
						}

                        avframe.iskey	= FALSE;
                    } else {
						dbg_line( "frame->type: %d", frame->type );
						avframe.type	= FRAME_UNCOMPLETED;
                    }

                    codec.stream_cb( i, &avframe );
                }

				cnt ++;

				if ( i == CAMERA_STREAM_AUDIO ) {
            		libaenc_remove_frame( frame );
				} else {
                	libvs_remove_frame( codec.stream_ctxs[ i ].num, frame );
				}
            }
        }

		if ( cnt == 0 ) {
			MSleep( 30 );
		}
    }

    return NULL;
}

static int
video_set_h264( StreamCtx *stream, CodecCtx *ctx )
{
    dbg_line("stream[%d]/fps[%d]", stream->num, ctx->fps );

    if ( ctx->fps > 24 ) {
    	libvs_set_vs_framerate( stream->num, 24 );
    	libvs_set_gop( stream->num, 24 );
    } else {
    	libvs_set_vs_framerate( stream->num, ctx->fps );
    	libvs_set_gop( stream->num, ctx->fps );
    }

    libvs_set_bitrate( stream->num, ctx->kbps );

	return SUCCESS;
}

//==========================================================================
// APIs
//==========================================================================
int
Camera_ShowOSD( OsdCtx *ctx )
{
    return SUCCESS;
}

int
Camera_SetMask( u32 id, MaskCtx *ctx )
{
    return SUCCESS;
}

int
Camera_SetMDCBFunc( MD_CBFunc cb )
{
    codec.md_cb = cb;
    return SUCCESS;
}

int
Camera_SetMD( u32 id, MDCtx *ctx )
{
	t_md_algo_mask md;

# define MD_W( w )		( ( ( w ) * MD_AREA_WIDTH ) / 100 )
# define MD_H( h )		( ( ( h ) * MD_AREA_HEIGHT ) / 100 )

	if ( id >= MD_ALGO_MASK_MAXNUM ) {
		dbg_warm( "the max region num is %d.", MD_ALGO_MASK_MAXNUM );
		return -1;
	} else if ( ( ctx->width == 0 ) || ( ctx->height == 0 ) ) {
		dbg_warm( "( ctx->width == 0 ) || ( ctx->height == 0 )" );
		return -1;
	}

	if ( video_analysis_get_para( &md ) == 0 ) {
		md.zones[ id ].enabled		= ctx->enable;
		md.zones[ id ].left			= MD_W( ctx->x );
		md.zones[ id ].top			= MD_H( ctx->y );
		md.zones[ id ].right		= MD_W( ctx->x + ctx->width );
		md.zones[ id ].bottom		= MD_H( ctx->y + ctx->height );
		md.zones[ id ].sensitivity	= ctx->sensitivity;
dbg_warm( "MD%d - enabled: %d, %d, %d, %d, %d, sensitivity: %d", id, md.zones[ id ].enabled, md.zones[ id ].left, md.zones[ id ].top, md.zones[ id ].right, md.zones[ id ].bottom, md.zones[ id ].sensitivity );
		return video_analysis_set_para( &md );
	}

	return FAILURE;
}

//Get/Set Image value, No get image data
int
Camera_SetImage( ImageCtx *ctx, char force )
{
    ISPTable    *isp_item;
    int    *value, *new_value;
    int streamid = 0, ret;

    if ( ctx == NULL ) {
        return FAILURE;
    }

    isp_item    = &ips_tbl[ 0 ];

    while ( isp_item->name ) {
        value        = ( int * )( ( void * )&codec.img_ctx + isp_item->pos );
        new_value    = ( int * )( ( void * )ctx + isp_item->pos );

        if ( force || ( *value != *new_value ) ) {
            //dbg_line("CMD_ID[%d][%d]",isp_item->cmd_id, *new_value );
            if ( isp_item->max != isp_item->min ) {
                if( ( isp_item->max < *new_value ) || ( isp_item->min > *new_value ) ){
                    dbg_line( "CMD_ID( %d )value( %d ): %d ~ %d", isp_item->cmd_id, *new_value, isp_item->min, isp_item->max );
                    //return FAILURE;
	                isp_item ++;
	                continue;
                }
            }

            if ( isp_item->set ) {
            	if ( isp_item->cmd_id == ISPCMD_SET_AWB_MANUAL_EN ) {
            		ret = isp_item->set( streamid, ( *new_value == CAMERA_AWB_MODE_AUTO )? OV798_ISP_AWB_AUTO: OV798_ISP_AWB_MANUAL );
            	} else {
            		ret = isp_item->set( streamid, *new_value );
            	}

            	if ( ret ) {
            		dbg_warm( "Fail to Set %s.", isp_item->name );
            	}
            } else {
            	dbg_warm( "Unsupported Function( %s )", isp_item->name );
            }
        }

        isp_item ++;
    }
dbg_line();
    return SUCCESS;
}

int
Camera_GetImage( ImageCtx *ctx )
{
    if ( ctx == NULL ) {
        return FAILURE;
    }

    memcpy( ctx, &codec.img_ctx, sizeof( ImageCtx ) );

    return SUCCESS;
}

int
Camera_SetVideoCodec( CAMERA_STREAM stream, CodecCtx *ctx )
{
	if ( ( stream >= CAMERA_STREAM_VIDEO_MAX ) || ( ctx == NULL ) ) {
		return FAILURE;
	} else if ( memcmp( &codec.stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) ) == 0 ) {
		return SUCCESS;
	}

    if ( stream != CAMERA_STREAM_VIDEO_MJPEG ) {
	    if ( video_set_h264( &codec.stream_ctxs[ stream ], ctx ) == FAILURE ) {
		    dbg_warm( "Do set_codec Failed!!!" );
		    return FAILURE;
	    }
	}

	memcpy( &codec.stream_ctxs[ stream ].codec_ctx, ctx, sizeof( CodecCtx ) );

	return SUCCESS;
}

int
Camera_SetVideoBitrate( CAMERA_STREAM stream, u32 bitrate )
{
    if ( stream >= CAMERA_STREAM_VIDEO_MAX ) {
        return FAILURE;
    }

    libvs_set_bitrate( codec.stream_ctxs[ stream ].num, bitrate );

    return SUCCESS;
}

int
Camera_SetAudioCBFunc( Audio_CBFunc cb )
{
    codec.audio_cb    = cb;
    return SUCCESS;
}

int
Camera_EnableAudio( char on )
{
	if ( on ) {
		codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].play	= TRUE;
	} else {
		codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].play	= FALSE;
	}

	return SUCCESS;
}

int
Camera_GetAudioInConf( AudioConf *conf )
{
    if ( conf == NULL ) {
    	dbg_error( "conf == NULL" );
        return FAILURE;
    }

	dbg_error( "Todo... Check Value" );
    //TODO Check    conf value
    conf->sampling_freq	= arec_inpara.sr;
    conf->channels		= arec_inpara.ch;
    conf->sample		= arec_inpara.sr;

    if ( arec_inpara.fmt ==  AUDIO_FMT_F4) {
        conf->type    = FRAME_AUDIO_AAC;
    } else if ( arec_inpara.fmt == AUDIO_FMT_F5 ) {
        conf->type    = FRAME_AUDIO_PCMU;
    } else if ( arec_inpara.fmt == AUDIO_FMT_F8 ) {
        conf->type    = FRAME_AUDIO_PCMA;
    } else if ( arec_inpara.fmt == AUDIO_FMT_F1 ) {
        conf->type    = FRAME_AUDIO_PCM;
    }

    return SUCCESS;
}

int
Camera_SetAudioInVolume( u32 volume )
{
    libacodec_set_adcvol( volume );
    return SUCCESS;
}

int
Camera_SetAudioOutVolume( u32 volume )
{
    libacodec_set_dacvol( volume );
    return SUCCESS;
}

int
Camera_OpenAudioOut()
{
    //dec
    memset( &adec_inpara, 0, sizeof( t_adec_inpara ) );

    adec_inpara.sr		= AUDIO_SAMPLE_RATE;
    adec_inpara.ch		= 1;
    adec_inpara.dvol	= 50;

	// CONFIG_AUDIO_DUPLEX_EN=y
    adec_inpara.fmt		= AUDIO_FMT_F5;

	// CONFIG_AUDIO_CODEC_EN_interncodec=y
    adec_inpara.aimode	= AI_PLY_MASTER;

	// CONFIG_AUDIO_DECAGC_EN=y
    //select the corresponding agc algo according to your requirement
    adec_inpara.compressor_en	= 1;

    //TODO Setting Audio
    if ( libadec_init( &adec_inpara ) < 0 ) {
        dbg_line( "libadec_init failed" );
    	return FAILURE;
    } else {
        dbg_line("libadec_init ok\n");
    }

    libadec_start();

    return SUCCESS;
}

int
Camera_CloseAudioOut()
{
    libadec_stop();
    libadec_exit();

    return SUCCESS;
}

 //Maybe is ok.....Leon
int
Camera_PlayAudio( u8 *data, u32 size )
{
    if ( data == NULL || size == 0 ) {
        return FAILURE;
    }

    FRAME *afrm = NULL;
    afrm = libadec_get_frame( 0 );
    if ( afrm ) {
        //debug_printf("a(%d,%d):%x:%x:%x\n",afrm->size,payload_len,payload_start[0],payload_start[1],payload_start[2]);
        memcpy((void *)afrm->addr, data, size);
        afrm->size = size;
        libadec_add_frame(afrm);
    }

    return SUCCESS;
}

int
Camera_SetStreamCBFunc( Stream_CBFunc cb )
{
    codec.stream_cb    = cb;
    return SUCCESS;
}

int
Camera_Init()
{
    memset( &codec, 0, sizeof( codec ) );

	mpu_start();

	if ( ov_init() == FAILURE ) {
		return FAILURE;
	}

	codec.stream_ctxs[ CAMERA_STREAM_VIDEO_H264_1 ].num	= 0;
	codec.stream_ctxs[ CAMERA_STREAM_VIDEO_H264_2 ].num	= 1;
	codec.stream_ctxs[ CAMERA_STREAM_VIDEO_MJPEG ].num	= 2;
	codec.stream_ctxs[ CAMERA_STREAM_AUDIO ].num		= 4;

	create_sys_thread( stream_proc, NULL, 6 );

    return SUCCESS;
}

void
Camera_Deinit()
{
     video_analysis_stop();
     stream_stop_all();
     StreamCtrl_Audio_Stop(STREAMCTRL_AUDIO_ALL);
     return;
}
