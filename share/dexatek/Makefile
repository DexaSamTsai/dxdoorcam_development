###############################################################################:
#
#   Makefile    Version 1.0     2017/07/19
#
#   Written by Sam Tsai
#
###############################################################################
DOORCAM_FW_VERSION	= "0.0.10"
DOORCAM_BLE_VERSION	= "3.0.999"

BUILD_DATE	= $(shell date +%FT%T%Z)
TARGET		= DxDoorCam
DEBUG		=
OPTIMIZE	= -O2

DIR_SOURCE	= $(CODEDIR)/share/dexatek
DIR_DXFRAMEWORK = $(DIR_SOURCE)/DxFramework

WARNING		= -Wall -Wfatal-errors -Wno-unused-function -Wno-unused-result -Wno-deprecated-declarations -Wno-implicit-function-declaration
INCS        += -I$(DIR_SOURCE) -I$(DIR_SOURCE)/Include
INCS		+= -I$(CODEDIR)/share/network/security/openssl-1.0.2k/include/ -I$(CODEDIR)/share/network/security/openssl-1.0.2k/

DEVICE_SOC	= OV798

DEFINE	+= -D$(DEVICE_SOC)
LDFLAGS	+= -s

INCS		+= -I$(DIR_DXFRAMEWORK)/DxOS/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/DxBSP/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/DxSysDebugger/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/SHSCommonSubModule
INCS		+= -I$(DIR_DXFRAMEWORK)/ThirdPartyStack
INCS		+= -I$(DIR_DXFRAMEWORK)/BLEManager
INCS		+= -I$(DIR_DXFRAMEWORK)/WifiCommManager/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/DxKeypadManager/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/UdpCommManager/$(DEVICE_SOC)
INCS		+= -I$(DIR_DXFRAMEWORK)/LEDManager
INCS		+= -I$(DIR_DXFRAMEWORK)/JobManager
INCS		+= -I$(DIR_DXFRAMEWORK)/ServerManager
INCS		+= -I$(DIR_DXFRAMEWORK)/HybridPeripheralManager
INCS		+= -I$(DIR_DXFRAMEWORK)/DxDeviceInfoManager
INCS		+= -I$(DIR_DXFRAMEWORK)/CustomerConfig
INCS		+= -I$(DIR_DXFRAMEWORK)/OfflineSupport
INCS		+= -I$(DIR_DXFRAMEWORK)/MainStateMachine
INCS		+= -I$(DIR_DXFRAMEWORK)/DxContStore
INCS		+= -I$(DIR_DXFRAMEWORK)/DxMemManager

SOURCES     += $(DIR_SOURCE)/main.c
SOURCES		+= $(wildcard $(DIR_SOURCE)/Codec/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Format/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Utils/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Stream/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/DataManagement/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Media/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Config/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/System/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/System/CGIs/*.c)
SOURCES     += $(wildcard $(DIR_SOURCE)/Service/*.c)

SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/SHSCommonSubModule/*.c)

SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxBSP/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxOS/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxMemManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/ThirdPartyStack/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxSysDebugger/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/BLEManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/WifiCommManager/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxKeypadManager/$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/UdpCommManager//$(DEVICE_SOC)/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/LEDManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/JobManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/HybridPeripheralManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/ServerManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxDeviceInfoManager/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/OfflineSupport/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/MainStateMachine/*.c)
SOURCES     += $(wildcard $(DIR_DXFRAMEWORK)/DxContStore/*.c)

OBJS		+= $(SOURCES:.c=.o)
IN_CFLAGS	+= $(INCS)

#LIBS        += -ldl -lm -lpthread -lrt -lssl -lcrypto -lfreetype
#ARFLAGS     = rv

DEFINE		+= -D_GNU_SOURCE
DEFINE		+= -DDK_GATEWAY
DEFINE		+= -DDEVICE_IS_HYBRID=1
DEFINE		+= -DRELEASE_DATE="\"$(BUILD_DATE)\""
DEFINE		+= -DDOORCAM_FW_VERSION="\"$(DOORCAM_FW_VERSION)\""
DEFINE		+= -DDOORCAM_BLE_VERSION="\"$(DOORCAM_BLE_VERSION)\""

#CFLAGS      += $(INCS) $(WARNING) $(OPTIMIZE) $(DEBUG) $(DEFINE) $(LIBS) -std=c99
CFLAGS      += $(WARNING) $(DEFINE) -std=c99
