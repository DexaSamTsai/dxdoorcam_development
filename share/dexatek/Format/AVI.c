/*
 * AVI.c
 *
 *  Created on: 2017/09/26
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "AVI.h"

//==========================================================================
// Type Define
//==========================================================================

# define AVI_PUT8( p, v )				( ( p )[ 0 ] = ( v ) & 0xff )
# define AVI_PUT16( p, v )				( ( p )[ 1 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 0 ] = ( v ) & 0xff )
# define AVI_PUT24( p, v )				( ( p )[ 2 ] = ( ( v ) >> 16 ) & 0xff, ( p )[ 1 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 0 ] = ( v ) & 0xff )
# define AVI_PUT32( p, v )				( ( p )[ 3 ] = ( ( v ) >> 24 ) & 0xff, ( p )[ 2 ] = ( ( v ) >> 16 ) & 0xff, ( p )[ 1 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 0 ] = ( v ) & 0xff )
# define AVI_PUT4CC( p, v )				( ( p )[ 0 ] = ( v )[ 0 ], ( p )[ 1 ] = ( v )[ 1 ], ( p )[ 2 ] = ( v )[ 2 ], ( p )[ 3 ] = ( v )[ 3 ] )

# define AVI_FOURCC_RIFF				"RIFF"
# define AVI_FOURCC_LIST				"LIST"
# define AVI_FOURCC_JUNK				"JUNK"
# define AVI_FOURCC_AVI					"AVI "
# define AVI_FOURCC_HDRL				"hdrl"
# define AVI_FOURCC_AVIH				"avih"
# define AVI_FOURCC_STRL				"strl"
# define AVI_FOURCC_STRH				"strh"
# define AVI_FOURCC_STRF				"strf"
# define AVI_FOURCC_MOVI				"movi"

# define AVI_FOURCC_AUDS				"auds"
# define AVI_FOURCC_VIDS				"vids"
# define AVI_FOURCC_MIDS				"mids"
# define AVI_FOURCC_TXTS				"txts"

# define AVI_FOURCC_MJPG				"MJPG"

# define AVI_FOURCC_IDX1				"idx1"
# define AVI_FOURCC_00DC				"00dc"
# define AVI_FOURCC_01WB				"01wb"

typedef struct __attribute__ ( ( packed ) ) {
	u8		v[ 4 ];
} DWORD;

typedef struct __attribute__ ( ( packed ) ) {
	u8		v[ 2 ];
} WORD;

typedef struct __attribute__ ( ( packed ) ) {
	u8		c[ 4 ];
} FourCC;

typedef struct __attribute__ ( ( packed ) ) {
	WORD		left;
	WORD		top;
	WORD		right;
	WORD		bottom;
} RECT;

typedef struct __attribute__ ( ( packed ) ) {
	FourCC		type;
	DWORD		size;
	FourCC		name;
} container_t;

typedef struct __attribute__ ( ( packed ) ) {
	FourCC		name;
	DWORD		size;						// 本数据结构的大小，不包括最初的8个字节（fcc和cb两个域）
	u8			data[ 0 ];
} chunk_t;

typedef struct __attribute__ ( ( packed ) ) {
	chunk_t		chunk;

	DWORD		dwMicroSecPerFrame;			// 视频帧间隔时间（以毫秒为单位）
	DWORD		dwMaxBytesPerSec;			// 这个AVI文件的最大数据率
	DWORD		dwPaddingGranularity;		// 数据填充的粒度
	DWORD		dwFlags;					// AVI文件的全局标记，比如是否含有索引块等
	DWORD		dwTotalFrames;				// 总帧数
	DWORD		dwInitialFrames;			// 为交互格式指定初始帧数（非交互格式应该指定为0）
	DWORD		dwStreams;					// 本文件包含的流的个数
	DWORD		dwSuggestedBufferSize;		// 建议读取本文件的缓存大小（应能容纳最大的块）
	DWORD		dwWidth;					// 视频图像的宽（以像素为单位）
	DWORD		dwHeight;					// 视频图像的高（以像素为单位）
	DWORD		dwReserved[ 4 ];			// 保留
} avih_t;

typedef struct __attribute__ ( ( packed ) ) {
	chunk_t		chunk;

    FourCC		fccType;             // 流的类型: auds(音频流) vids(视频流) mids(MIDI流) txts(文字流)
    FourCC		fccHandler;          // 指定流的处理者，对于音视频来说就是解码器
    DWORD		dwFlags;              // 标记：是否允许这个流输出？调色板是否变化？
    WORD		wPriority;             // 流的优先级（当有多个相同类型的流时优先级最高的为默认流）
    WORD		wLanguage;             // 语言
    DWORD		dwInitialFrames;      // 为交互格式指定初始帧数
    DWORD		dwScale;              // 每帧视频大小或者音频采样大小
    DWORD		dwRate;               // dwScale/dwRate，每秒采样率
    DWORD		dwStart;              // 流的开始时间
    DWORD		dwLength;             // 流的长度（单位与dwScale和dwRate的定义有关）
    DWORD		dwSuggestedBufferSize;// 读取这个流数据建议使用的缓存大小
    DWORD		dwQuality;            // 流数据的质量指标（0 ~ 10,000）
    DWORD		dwSampleSize;         // Sample的大小
    RECT		rcFrame;               // 指定这个流（视频流或文字流）在视频主窗口中的显示位置，视频主窗口由AVIMAINHEADER结构中的dwWidth和dwHeight决定
} strh_t;

typedef struct __attribute__ ( ( packed ) ) {
	chunk_t		chunk;

    DWORD		biSize;
    DWORD		biWidth;
    DWORD		biHeight;
    WORD		biPlanes;
    WORD		biBitCount;
    FourCC		biCompression;
    DWORD		biSizeImage;
    DWORD		biXPelsPerMeter;
    DWORD		biYPelsPerMeter;
    DWORD		biClrUsed;
    DWORD		biClrImportant;
} strf_v_t;

typedef struct __attribute__ ( ( packed ) ) {
	chunk_t		chunk;

    WORD		wFormatTag;
    WORD		nChannels;               // 声道数
    DWORD		nSamplesPerSec;         // 采样率
    DWORD		nAvgBytesPerSec;        // 每秒的数据量
    WORD		nBlockAlign;             // 数据块对齐标志
    WORD		wBitsPerSample;          // 每次采样的数据量
    WORD		cbSize;                  // 大小
} strf_a_t;

// 索引节点信息
typedef struct __attribute__ ( ( packed ) ) {
    FourCC		dwChunkId;   // 本数据块的四字符码(00dc 01wb)
    DWORD		dwFlags;     // 说明本数据块是不是关键帧、是不是‘rec ’列表等信息
    DWORD		dwOffset;    // 本数据块在文件中的偏移量
    DWORD		dwSize;      // 本数据块的大小
} index_t;

// 索引节点信息
typedef struct __attribute__ ( ( packed ) ) {
	chunk_t		chunk;

    index_t		index[ 0 ];
} idx1_t;

typedef struct __attribute__ ( ( packed ) ) {
	container_t			riff;

	struct {
		container_t			list;
		avih_t				avih;

		struct {
			container_t			list;
			strh_t				strh;
			strf_v_t			strf;
		} strl_v;
	} hdrl;

	struct {
		container_t			list;
	} movi;
} avi_header_t;

# define AVI_DWORD( p, n )				AVI_PUT32( p.v, n )
# define AVI_WORD( p, n )				AVI_PUT16( p.v, n )
# define AVI_FOURCC( p, n )				AVI_PUT4CC( p.c, n )

# define AVI_VIDEO_BUFF_SIZE			1 * 1024 * 1024
# define AVI_AUDIO_BUFF_SIZE			1 * 1024 * 1024
# define AVI_VIDEO_FRAME_MAX			30
# define AVI_AUDIO_FRAME_MAX			( 120 * ( 30 + 30 ) )

enum {
	AVI_TRACK_VIDEO		= 0,
	AVI_TRACK_MAX,
	AVI_TRACK_AUDIO,
};

typedef struct {
	idx1_t			*idx;
	u32				frame_cnt;

	u8				*buff;
	u32				buff_size;
	u32				offset;
} avi_track_t;

typedef struct  {
	int					fd;

	u32					file_size;
	u32					index_size;
	u32					total_frame;

	avi_header_t		header;

	int					w;
	int					h;

	avi_track_t			track[ AVI_TRACK_MAX ];
} avi_handle_t;

# define AVI_CHUNK_SIZE						sizeof( chunk_t )
# define AVI_HANDLE_SIZE					sizeof( avi_handle_t )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
build_contianer( container_t *cont, const char *cont_type, u32 size, const char *cont_name )
{
    AVI_FOURCC( cont->type, cont_type );
    AVI_DWORD( cont->size, size );
    AVI_FOURCC( cont->name, cont_name );
}

static void
build_chunk( chunk_t *chunk, const char *chunk_name, u32 size )
{
    AVI_FOURCC( chunk->name, chunk_name );
    AVI_DWORD( chunk->size, size );
}

static void
build_avih( avih_t *avih, avi_handle_t *avi_handle )
{
	memset( avih, 0, sizeof( avih_t ) );

	build_chunk( &avih->chunk, AVI_FOURCC_AVIH, ( sizeof( avih_t ) - AVI_CHUNK_SIZE ) );

# define BAVI_F_HASINDEX			0x00000010u
# define BAVI_F_MUSTUSEINDEX		0x00000020u
# define BAVI_F_ISINTERLEAVED		0x00000100u
/*Use CKType to find key frames */
# define BAVI_F_TRUSTCKTYPE			0x00000800u
# define BAVI_F_WASCAPTUREFILE		0x00010000u
# define BAVI_F_COPYRIGHTED			0x00020000u

	AVI_DWORD( avih->dwMicroSecPerFrame, 1000000 );
	AVI_DWORD( avih->dwMaxBytesPerSec, 25000 );
	AVI_DWORD( avih->dwPaddingGranularity, 0 );
	AVI_DWORD( avih->dwFlags, ( BAVI_F_HASINDEX | BAVI_F_ISINTERLEAVED | BAVI_F_TRUSTCKTYPE ) );
	AVI_DWORD( avih->dwTotalFrames, avi_handle->total_frame );
	AVI_DWORD( avih->dwInitialFrames, 0 );
	AVI_DWORD( avih->dwStreams, 1 );
	AVI_DWORD( avih->dwSuggestedBufferSize, 1024 * 1024 );
	AVI_DWORD( avih->dwWidth, avi_handle->w );
	AVI_DWORD( avih->dwHeight, avi_handle->h );
}

static void
build_strh( strh_t *strh, avi_handle_t *avi_handle, avi_track_t *track )
{
	memset( strh, 0, sizeof( strh_t ) );

	build_chunk( &strh->chunk, AVI_FOURCC_STRH, ( sizeof( strh_t ) - AVI_CHUNK_SIZE ) );

    AVI_FOURCC( strh->fccType, AVI_FOURCC_VIDS );
    AVI_FOURCC( strh->fccHandler, AVI_FOURCC_MJPG );
    AVI_DWORD( strh->dwFlags, 0 );
    AVI_WORD( strh->wPriority, 0 );
    AVI_WORD( strh->wLanguage, 0 );
    AVI_DWORD( strh->dwInitialFrames, 0 );
    AVI_DWORD( strh->dwScale, 1 );
    AVI_DWORD( strh->dwRate, 1);
    AVI_DWORD( strh->dwStart, 0 );
    AVI_DWORD( strh->dwLength, track->frame_cnt );
    AVI_DWORD( strh->dwSuggestedBufferSize, ( 12 * 1024 ) );
    AVI_DWORD( strh->dwQuality, 0xFFFFFFFF );
    AVI_DWORD( strh->dwSampleSize, 0 );

    AVI_WORD( strh->rcFrame.left, 0 );
    AVI_WORD( strh->rcFrame.top, 0 );
    AVI_WORD( strh->rcFrame.right, avi_handle->w );
    AVI_WORD( strh->rcFrame.bottom, avi_handle->h );
}

static void
build_strf_v( strf_v_t *strf, avi_handle_t *avi_handle )
{
	memset( strf, 0, sizeof( strf_v_t ) );

	build_chunk( &strf->chunk, AVI_FOURCC_STRF, ( sizeof( strf_v_t ) - AVI_CHUNK_SIZE ) );

    AVI_DWORD( strf->biSize, ( sizeof( strf_v_t ) - AVI_CHUNK_SIZE ) );
    AVI_DWORD( strf->biWidth, avi_handle->w );
    AVI_DWORD( strf->biHeight, avi_handle->h );
    AVI_WORD( strf->biPlanes, 1 );
    AVI_WORD( strf->biBitCount, 24 );
    AVI_FOURCC( strf->biCompression, AVI_FOURCC_MJPG );
    AVI_DWORD( strf->biSizeImage, ( avi_handle->w * avi_handle->h * 3 ) );
    AVI_DWORD( strf->biXPelsPerMeter, 0 );
    AVI_DWORD( strf->biYPelsPerMeter, 0 );
    AVI_DWORD( strf->biClrUsed, 0 );
    AVI_DWORD( strf->biClrImportant, 0 );
}

static void
build_header( avi_handle_t *avi_handle )
{
	avi_header_t	*avi	= &avi_handle->header;
	avi_track_t		*track	= &avi_handle->track[ AVI_TRACK_VIDEO ];

	build_contianer( &avi->riff, AVI_FOURCC_RIFF, ( avi_handle->file_size - AVI_CHUNK_SIZE ), AVI_FOURCC_AVI );

	build_contianer( &avi->hdrl.list, AVI_FOURCC_LIST, ( sizeof( avi->hdrl ) - AVI_CHUNK_SIZE ), AVI_FOURCC_HDRL );
	build_avih( &avi->hdrl.avih, avi_handle );

	build_contianer( &avi->hdrl.strl_v.list, AVI_FOURCC_LIST, ( sizeof( avi->hdrl.strl_v ) - AVI_CHUNK_SIZE ), AVI_FOURCC_STRL );
	build_strh( &avi->hdrl.strl_v.strh, avi_handle, track );
	build_strf_v( &avi->hdrl.strl_v.strf, avi_handle );

	build_contianer( &avi->movi.list, AVI_FOURCC_LIST, ( sizeof( avi->movi ) + track->offset - AVI_CHUNK_SIZE ), AVI_FOURCC_MOVI );
}

static u32
avi_put_video( avi_handle_t *avi, av_frame_t *frame )
{
	avi_track_t *track	= &avi->track[ AVI_TRACK_VIDEO ];

	if ( track->frame_cnt > AVI_VIDEO_FRAME_MAX ) {
		dbg_warm( "track->frame_cnt > AVI_VIDEO_FRAME_MAX( %d )!!!", AVI_VIDEO_FRAME_MAX );
		return 0;
	} else if ( track->offset + frame->size > track->buff_size ) {
		dbg_warm( "track->offset( %d ) + frame->size( %d ) > track->buff_size( %d )!!!", track->offset, frame->size, track->buff_size );
		return 0;
	} else {
		chunk_t		*chunk	= ( chunk_t * )&track->buff[ track->offset ];

		build_chunk( chunk, AVI_FOURCC_00DC, frame->size );
		memcpy( chunk->data, frame->buff, frame->size );
	}

	return ( frame->size + AVI_CHUNK_SIZE );
}

static u32
avi_put_frame( avi_handle_t *avi, av_frame_t *frame )
{
	if ( ( frame->type & FRAME_AV_MASK ) == FRAME_VIDEO ) {
		avi_track_t *track	= &avi->track[ AVI_TRACK_VIDEO ];
		u32 res = avi_put_video( avi, frame );

		if ( res ) {
			AVI_FOURCC( track->idx->index[ track->frame_cnt ].dwChunkId, AVI_FOURCC_00DC );
			if ( track->frame_cnt ) {
				AVI_DWORD( track->idx->index[ track->frame_cnt ].dwFlags, 0x00 );
			} else {
				AVI_DWORD( track->idx->index[ track->frame_cnt ].dwFlags, 0x10 );
			}
			AVI_DWORD( track->idx->index[ track->frame_cnt ].dwOffset, ( 4 + track->offset ) );
			AVI_DWORD( track->idx->index[ track->frame_cnt ].dwSize, frame->size );

			track->frame_cnt ++;
			track->offset += res;
		}

		return res;
	} else {
		return 0;
	}
}

static int
avi_write_data( avi_handle_t *avi, void *src, int size )
{
	int res;

	while ( size > 0 ) {
		res = write( avi->fd, src, size );

		if ( res < 0 ) {
			if ( ( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				MSleep( 50 );
				continue;
			}

			dbg_error( "Do write Failed!!!( res = %d, %s )", res, strerror( errno ) );
			return FAILURE;
		} else if ( res == 0 ) {
			MSleep( 50 );
		}

		size -= res;
		src  += res;
	}

	return SUCCESS;
}

static void
avi_free( avi_handle_t *avi )
{
	if ( avi->track[ AVI_TRACK_VIDEO ].buff ) {
		my_free( avi->track[ AVI_TRACK_VIDEO ].buff );
		avi->track[ AVI_TRACK_VIDEO ].buff	= NULL;
	}

	if ( avi->track[ AVI_TRACK_VIDEO ].idx ) {
		my_free( avi->track[ AVI_TRACK_VIDEO ].idx );
		avi->track[ AVI_TRACK_VIDEO ].idx	= NULL;
	}

	if ( avi->fd > 0 ) {
		close( avi->fd );
	}

	my_free( avi );
}

//==========================================================================
// APIs
//==========================================================================
int
AVI_WriteFrame( AVIHandle handle, AVFrame *frame )
{
	avi_handle_t *avi	= ( avi_handle_t * )handle;

	if ( frame && avi ) {
		return avi_put_frame( avi, frame );
	}

	return FAILURE;
}

AVIHandle
AVI_Open( char *filename, AVI_CodecCtx *ctx, int fd )
{
	if ( ctx && ( filename || ( fd > 0 ) ) ) {
		avi_handle_t *avi	= ( avi_handle_t * )my_malloc( AVI_HANDLE_SIZE );

		if ( avi == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			return NULL;
		}

		avi->w	= ctx->video.width;
		avi->h	= ctx->video.height;

		avi->track[ AVI_TRACK_VIDEO ].buff_size		= AVI_VIDEO_BUFF_SIZE;
		avi->track[ AVI_TRACK_VIDEO ].buff			= ( u8 * )my_malloc( AVI_VIDEO_BUFF_SIZE );
		if ( avi->track[ AVI_TRACK_VIDEO ].buff == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			goto err;
		}

		avi->track[ AVI_TRACK_VIDEO ].idx			= ( idx1_t * )my_malloc( sizeof( idx1_t ) + ( AVI_VIDEO_FRAME_MAX * sizeof( index_t ) ) );
		if ( avi->track[ AVI_TRACK_VIDEO ].idx == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			goto err;
		}

		if ( fd > 0 ) {
			avi->fd	= fd;
		} else {
			avi->fd	= open( filename, ( O_CREAT | O_RDWR ), 0755 );
		}

		if ( avi->fd <= 0 ) {
			dbg_warm( "Create File: %s Failed!!!", filename );
			goto err;
		}

		return avi;

err:
		avi_free( avi );
	}

	return NULL;
}

int
AVI_Flush( AVIHandle handle )
{
	avi_handle_t *avi	= ( avi_handle_t * )handle;

	if ( avi ) {
		avi_track_t *track	= &avi->track[ AVI_TRACK_VIDEO ];

		avi->index_size		= ( sizeof( idx1_t ) + ( track->frame_cnt * sizeof( index_t ) ) );
		avi->file_size		= sizeof( avi_header_t ) + track->offset + avi->index_size;
		avi->total_frame	= track->frame_cnt;

		build_header( avi );
		build_chunk( &track->idx->chunk, AVI_FOURCC_IDX1, ( avi->index_size - AVI_CHUNK_SIZE ) );

		if ( avi_write_data( avi, &avi->header, sizeof( avi_header_t ) ) != SUCCESS ) {
			dbg_warm( "Write AVI Header Failed!!!" );
		} else if ( avi_write_data( avi, track->buff, track->offset ) != SUCCESS ) {
			dbg_warm( "Write AVI Media Data Failed!!!" );
		} else if ( avi_write_data( avi, track->idx, avi->index_size ) ) {
			dbg_warm( "Write AVI Index Failed!!!" );
		} else {
			return SUCCESS;
		}
	}

	return FAILURE;
}

void
AVI_Close( AVIHandle handle )
{
	avi_handle_t *avi	= ( avi_handle_t * )handle;

	if ( avi ) {
		avi_free( avi );
	}
}

