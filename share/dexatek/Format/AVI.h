/*
 * AVI.h
 *
 *  Created on: 2017/09/26
 *      Author: Sam Tsai
 */

# ifndef __AVI_H__
# define __AVI_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
typedef void *		AVIHandle;

typedef struct {
	struct {
		u32			fps;
		u16			width;
		u16			height;
	} video;

	struct {
		u32			sample_rate;
		u32			packet_size;
		u8			sample_size;
		u8			channels;
	} audio;
} AVI_CodecCtx;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
AVI_WriteFrame( AVIHandle handle, AVFrame *frame );

extern AVIHandle
AVI_Open( char *filename, AVI_CodecCtx *ctx, int fd );

extern int
AVI_Flush( AVIHandle handle );

extern void
AVI_Close( AVIHandle handle );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __AVI_H__
