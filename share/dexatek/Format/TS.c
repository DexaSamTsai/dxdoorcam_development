/*
 * TS.c
 *
 *  Created on: 2017嚙踝蕭謕蕭豲���蕭蹎抆揭7���雓�鞊莎�揭���雓�31���雓�嚙踝蕭謢察嚙踝蕭謕
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "TS.h"
# include "Utils/CRC32.h"

//==========================================================================
// Type Define
//==========================================================================
# define TS_SYNC_CODE		0x47
# define TS_PACKET_SIZE		188
# define TS_HEAD_LEN		4

enum {
	TS_PID_PAT	= 0x00,
	TS_PID_CAT	= 0x01,
	TS_PID_NIT	= 0x10,
	TS_PID_SDT	= 0x11,
	TS_PID_EIT	= 0x12,
};

enum {
	TS_ADAPTATION_PAYLOAD_ONYL		= 0x01,
	TS_ADAPTATION_FIELD_ONYL		= 0x02,
	TS_ADAPTATION_BOTH				= 0x03,
};

# define TS_PSI_TABLE_PAT				0x00
# define TS_PSI_TABLE_PMT				0x02

# define TS_PID_PMT						0x100
# define TS_PID_PCR						0x40
# define TS_PID_VIDEO_STREAM			0x40
# define TS_PID_AUDIO_STREAM			0x41

# define TS_SID_PROGRAM_STREAM_MAP			0xBC
# define TS_SID_PRIVATE_STREAM_1			0xBD
# define TS_SID_PADDING_STREAM				0xBE
# define TS_SID_PRIVATE_STREAM_2			0xBF
# define TS_SID_AUDIO_STREAM				0xC0 // 110x xxxx, stream number x xxxx
# define TS_SID_VIDEO_STREAM				0xE0 // 1110 xxxx, stream number xxxx
# define TS_SID_ECM_STREAM					0xF0
# define TS_SID_EMM_STREAM					0xF1
# define TS_SID_DSMCC_STREAM				0xF2
# define TS_SID_13522_STREAM				0xF3
# define TS_SID_TYPEA						0xF4
# define TS_SID_TYPEB						0xF5
# define TS_SID_TYPEC						0xF6
# define TS_SID_TYPED						0xF7
# define TS_SID_TYPEE						0xF8
# define TS_SID_ANCILLARY_STREAM			0xF9
// 1111 1010 ~ 1111 1110, reserved data stream
# define TS_SID_PROGRAM_STREAM_DIRECTORY	0xFF

# define STREAM_TYPE_VIDEO_MPEG1		0x01
# define STREAM_TYPE_VIDEO_MPEG2		0x02
# define STREAM_TYPE_AUDIO_MPEG1		0x03		// ISO/IEC 11172-3( PCM )
# define STREAM_TYPE_AUDIO_MPEG2		0x04		// ISO/IEC 13818-3( G.722 )
# define STREAM_TYPE_PRIVATE_SECTION	0x05
# define STREAM_TYPE_PRIVATE_DATA		0x06
# define STREAM_TYPE_AUDIO_AAC			0x0F		// ISO/IEC 13818-7
# define STREAM_TYPE_AUDIO_AAC_LATM		0x11
# define STREAM_TYPE_VIDEO_MPEG4		0x10
# define STREAM_TYPE_METADATA			0x15
# define STREAM_TYPE_VIDEO_H264			0x1b
# define STREAM_TYPE_VIDEO_HEVC			0x24
# define STREAM_TYPE_VIDEO_CAVS			0x42
# define STREAM_TYPE_VIDEO_VC1			0xea
# define STREAM_TYPE_VIDEO_DIRAC		0xd1

# define STREAM_TYPE_AUDIO_AC3			0x81
# define STREAM_TYPE_AUDIO_DTS			0x82
# define STREAM_TYPE_AUDIO_TRUEHD		0x83
# define STREAM_TYPE_AUDIO_EAC3			0x87

enum {
	TS_STREAM_VIDEO	= 0,
	TS_STREAM_AUDIO,
	TS_STREAM_MAX,
};

typedef struct {
	int		pid;
	int		start;
	int		adaptation;
	int		cnt;
} ts_header;

typedef struct {
	u8		*src;
	int		offset;
} parse_attr_t;

typedef struct {
	int		fd;

	u32		pat_cnt;
	u32		pmt_cnt;

	struct {
		u32		sid;
		u32		pid;
		u32		cnt;
		u64		pts;
	} stream[ TS_STREAM_MAX ];

	u64		pcr_time;

	u8		buff[ TS_PACKET_SIZE ];

	struct timeval	start_time;
	ts_header		tsh;
	parse_attr_t	pos;
} ts_handle_t;

# define TS_HANDLE_SIZE			sizeof( ts_handle_t )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
put_bit( parse_attr_t *pos, u32 cnt, u32 value )
{
	u32 bytes	= pos->offset / 8;
	u32 bits	= pos->offset % 8;

	pos->offset += cnt;

	while ( cnt ) {
		cnt --;

		if ( value & ( 0x00000001 << cnt ) ) {
			pos->src[ bytes ] |= ( 0x01 << ( 7 - bits ) );
		} else {
			pos->src[ bytes ] &= ~( 0x01 << ( 7 - bits ) );
		}

		bits ++;

		if ( bits >= 8 ) {
			bytes ++;
			bits = 0;
		}
	}
}

static void
ts_write_ts_header( ts_header *tsh, parse_attr_t *pos )
{
	// TS Header
	put_bit( pos, 8, TS_SYNC_CODE );			// 8,  sync_byte: 0x47
	put_bit( pos, 1, 0 );				// 1,  transport_error_indicator
	put_bit( pos, 1, tsh->start );				// 1,  payload_unit_start_indicator
	put_bit( pos, 1, 0 );				// 1,  transport_priority
	put_bit( pos, 13, tsh->pid );	// 13, PID
	put_bit( pos, 2, 0 );				// 2,  transport_scrambling_control
	put_bit( pos, 2, tsh->adaptation );				// 2,  adaption_field_control
	put_bit( pos, 4, tsh->cnt );	// 4, continuity_counter
}

static int
ts_write_pat( ts_handle_t *ts )
{
	memset( ts->buff, 0xFF, TS_PACKET_SIZE );

	ts->pos.src		= ts->buff;
	ts->pos.offset	= 0;

	ts->tsh.pid			= TS_PID_PAT;
	ts->tsh.start		= 1;
	ts->tsh.adaptation	= TS_ADAPTATION_PAYLOAD_ONYL;
	ts->tsh.cnt			= ( ts->pat_cnt ++ ) & 0x0F;

	// TS Header
	ts_write_ts_header( &ts->tsh, &ts->pos );

	// Adaptation Field Length
	put_bit( &ts->pos, 8, 0 );

	// PSI
	put_bit( &ts->pos, 8, TS_PSI_TABLE_PAT );		// 8,  table_id PAT: 0x00
	put_bit( &ts->pos, 1, 0x01 );					// 1,  section_syntax_indicator: 0x01
	put_bit( &ts->pos, 1, 0x00 );					// 1,  0x00
	put_bit( &ts->pos, 2, 0x03 );					// 2,  Reserved: 0x03
	put_bit( &ts->pos, 12, 13 );					// 12, section_length

	put_bit( &ts->pos, 16, 0x0001 );						// 16, transport_stream_id
	put_bit( &ts->pos, 2, 0x03 );					// 2,  Reserved: 0x03
	put_bit( &ts->pos, 5, 0x00 );					// 5,  version_number
	put_bit( &ts->pos, 1, 0x01 );					// 1,  current_next_indicator
	put_bit( &ts->pos, 8, 0x00 );					// 8,  section_number
	put_bit( &ts->pos, 8, 0x00 );					// 8,  last_section_number

	// Program
	put_bit( &ts->pos, 16, 0x0001 );		// 16, program_number
	put_bit( &ts->pos, 3, 0x07 );			// 3,  Reserved: 0x07
	put_bit( &ts->pos, 13, TS_PID_PMT );	// 13, program_map_PID

	put_bit( &ts->pos, 32, CRC32( &ts->buff[ 5 ], 12 ) );	// 32, CRC

	write( ts->fd, ts->buff, TS_PACKET_SIZE );

	return SUCCESS;
}

static int
ts_write_pmt( ts_handle_t *ts )
{
	memset( ts->buff, 0xFF, TS_PACKET_SIZE );

	ts->pos.src		= ts->buff;
	ts->pos.offset	= 0;

	ts->tsh.pid			= TS_PID_PMT;
	ts->tsh.start		= 1;
	ts->tsh.adaptation	= TS_ADAPTATION_PAYLOAD_ONYL;
	ts->tsh.cnt			= ( ts->pmt_cnt ++ ) & 0x0F;

	// TS Header
	ts_write_ts_header( &ts->tsh, &ts->pos );

	// Adaptation Field Length
	put_bit( &ts->pos, 8, 0 );

	// PMT
	put_bit( &ts->pos, 8, TS_PSI_TABLE_PMT );	// 8,  table_id PMT: 0x02
	put_bit( &ts->pos, 1, 0x01 );				// 1,  section_syntax_indicator: 0x01
	put_bit( &ts->pos, 1, 0x00 );				// 1,  0x01
	put_bit( &ts->pos, 2, 0x03 );				// 2,  reserved: 0x03
	put_bit( &ts->pos, 12, 23 );				// 12, section_length

	put_bit( &ts->pos, 16, 0x0001 );			// 16, program_number
	put_bit( &ts->pos, 2, 0x03 );				// 2,  reserved: 0x03
	put_bit( &ts->pos, 5, 0x00 );				// 5,  version_number
	put_bit( &ts->pos, 1, 0x01 );				// 1,  current_next_indicator
	put_bit( &ts->pos, 8, 0x00 );				// 8,  section_number: 0x00
	put_bit( &ts->pos, 8, 0x00 );				// 8,  last_section_number: 0x00

	put_bit( &ts->pos, 3, 0x07 );				// 3,  reserved: 0x07
	put_bit( &ts->pos, 13, TS_PID_PCR );		// 13, PCR_PID
	put_bit( &ts->pos, 4, 0x0F );				// 4,  reserved: 0x0F
	put_bit( &ts->pos, 12, 0 );					// 12, program_info_length

	// Stream Video
	put_bit( &ts->pos, 8, STREAM_TYPE_VIDEO_H264 );		// 8,  stream_type
	put_bit( &ts->pos, 3, 0x07 );						// 3,  reserved: 0x07
	put_bit( &ts->pos, 13, TS_PID_VIDEO_STREAM );			// 13, elementary_PID
	put_bit( &ts->pos, 4, 0x0F );						// 4,  reserved: 0x0F
	put_bit( &ts->pos, 12, 0x00 );						// 12, ES_info_length

	// Stream Audio
	put_bit( &ts->pos, 8, STREAM_TYPE_AUDIO_AAC );		// 8,  stream_type
	put_bit( &ts->pos, 3, 0x07 );						// 3,  reserved: 0x07
	put_bit( &ts->pos, 13, TS_PID_AUDIO_STREAM );		// 13, elementary_PID
	put_bit( &ts->pos, 4, 0x0F );						// 4,  reserved: 0x0F
	put_bit( &ts->pos, 12, 0x00 );						// 12, ES_info_length

	put_bit( &ts->pos, 32, CRC32( &ts->buff[ 5 ], 22 ) );	// 32, CRC

	write( ts->fd, ts->buff, TS_PACKET_SIZE );

	return SUCCESS;
}

static int
ts_write_pcr( ts_handle_t *ts )
{
	memset( ts->buff, 0xFF, TS_PACKET_SIZE );

	ts->pos.src		= ts->buff;
	ts->pos.offset	= 0;

	ts->tsh.pid			= TS_PID_PCR;
	ts->tsh.start		= 1;
	ts->tsh.adaptation	= TS_ADAPTATION_FIELD_ONYL;
	ts->tsh.cnt			= ( ts->stream[ TS_STREAM_VIDEO ].cnt ++ ) & 0x0F;

	// TS Header
	ts_write_ts_header( &ts->tsh, &ts->pos );

	// Adaptation Field Length
	put_bit( &ts->pos, 8, 0x07 );			// 8,  Number of bytes in the adaptation field immediately following this byte

	// PCR
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Discontinuity indicator
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when the stream may be decoded without errors from this point
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when this stream should be considered "high priority"
	put_bit( &ts->pos, 1, 0x01 );			// 1,  Set when PCR field is present
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when OPCR field is present
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when splice countdown field is present
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when private data field is present
	put_bit( &ts->pos, 1, 0x00 );			// 1,  Set when extension field is present

	put_bit( &ts->pos, 4, ( ( ts->pcr_time >> 25 ) & 0xFF ) );	// 32, PCR[ 32 ~259 ]
	put_bit( &ts->pos, 4, ( ( ts->pcr_time >> 17 ) & 0xFF ) );	// 32, PCR[ 24 ~ 17 ]
	put_bit( &ts->pos, 4, ( ( ts->pcr_time >> 9 ) & 0xFF ) );	// 32, PCR[ 16 ~ 9 ]
	put_bit( &ts->pos, 4, ( ( ts->pcr_time >> 1 ) & 0xFF ) );	// 32, PCR[ 8 ~ 1 ]
	put_bit( &ts->pos, 1, ( ts->pcr_time & 0x01 ) );			// 1,  PCR[ 0 ]
	put_bit( &ts->pos, 6, 0x3F );								// 6,  reserved
	put_bit( &ts->pos, 9, 0x00 );								// 9,  extension

	ts->pcr_time += 90000;

	write( ts->fd, ts->buff, TS_PACKET_SIZE );

	return SUCCESS;
}

static void
ts_write_pes_header( ts_handle_t *ts, u32 sid, u64 pts, u32 size )
{
	ts->pos.src		= ts->buff;
	ts->pos.offset	= 0;

	ts->tsh.pid			= ts->stream[ sid ].pid;
	ts->tsh.start		= 1;
	ts->tsh.adaptation	= TS_ADAPTATION_PAYLOAD_ONYL;
	ts->tsh.cnt			= ( ts->stream[ sid ].cnt ++ ) & 0x0F;

	// TS Header
	ts_write_ts_header( &ts->tsh, &ts->pos );
	put_bit( &ts->pos, 24, 0x000001 );			// 24, packet_start_code_prefix
	put_bit( &ts->pos, 8, ts->stream[ sid ].sid );	// 8,  stream_id

	if ( sid == TS_STREAM_VIDEO ) {
		put_bit( &ts->pos, 16, 0 );				// 16, pes_packet_length
	} else {
		put_bit( &ts->pos, 16, size );			// 16, pes_packet_length
	}

	// Optional PES header
	put_bit( &ts->pos, 2, 0x02 );				// 2,  marker_bits 0x2
	put_bit( &ts->pos, 2, 0 );					// 2,  scrambling_control
	put_bit( &ts->pos, 1, 0 );					// 1,  priority
	put_bit( &ts->pos, 1, 0 );					// 1,  data_alignment_indicator
	put_bit( &ts->pos, 1, 0 );					// 1,  copyright
	put_bit( &ts->pos, 1, 0 );					// 1,  original_or_copy

	put_bit( &ts->pos, 2, 0x02 );				// 2,  11 = both present, 01 is forbidden, 10 = only PTS, 00 = no PTS or DTS
	put_bit( &ts->pos, 1, 0 );					// 1,  escr_flag
	put_bit( &ts->pos, 1, 0 );					// 1,  es_rate_flag
	put_bit( &ts->pos, 1, 0 );					// 1,  dsm_trick_mode_flag
	put_bit( &ts->pos, 1, 0 );					// 1,  additional_copy_info_flag
	put_bit( &ts->pos, 1, 0 );					// 1,  crc_flag
	put_bit( &ts->pos, 1, 0 );					// 1,  extension_flag

	put_bit( &ts->pos, 8, 5 );					// 8,  gives the length of the remainder of the PES header in bytes

	put_bit( &ts->pos, 4, 0x02 );				// 4,  pes_flag
	put_bit( &ts->pos, 3, ( pts >> 30 ) );					// 3,  PTS[32..30]
	put_bit( &ts->pos, 1, 1 );					// 1,  marker1
	put_bit( &ts->pos, 15, ( ( pts >> 15 ) & 0x7FFF ) );					// 15, PTS[29..15]
	put_bit( &ts->pos, 1, 1 );					// 1,  marker2
	put_bit( &ts->pos, 15, ( pts & 0x7FFF ) );			// 15, PTS[14..0]
	put_bit( &ts->pos, 1, 1 );					// 1,  marker3
}

//# define	SEND_DTS

static int
ts_write_pes( ts_handle_t *ts, AVFrame *frame )
{
	u8 *src = frame->buff, *data_end = frame->buff + frame->size;
	u32	packet_size, header_size = 0, dummy_size = 0;
	u64	curr_pts	= ( frame->ptime.tv_sec - ts->start_time.tv_sec ) * 1000 + ( ( frame->ptime.tv_usec - ts->start_time.tv_usec ) / 1000 );

	int sid;

	if ( ( frame->type & FRAME_AV_MASK ) == FRAME_VIDEO ) {
		if ( frame->iskey ) {
			//ts_write_pcr( ts );
		}
		sid = TS_STREAM_VIDEO;
	} else {
		if ( frame->type != FRAME_AUDIO_AAC ) {
			return SUCCESS;
		}

		sid = TS_STREAM_AUDIO;
	}

	memset( ts->buff, 0xFF, TS_PACKET_SIZE );

	curr_pts	*= ts->stream[ sid ].pts;

	// PES
	ts_write_pes_header( ts, sid, curr_pts, frame->size );

	packet_size = ts->pos.offset / 8;
	write( ts->fd, ts->buff, packet_size );

	packet_size = TS_PACKET_SIZE - packet_size;
	write( ts->fd, src, packet_size );

	src += packet_size;

	memset( ts->buff, 0xFF, TS_PACKET_SIZE );

	while ( src < data_end ) {
		packet_size	= data_end - src;

		if ( packet_size > 184 ) {
			packet_size	= 184;
		}

		// TS Header
		ts->pos.src		= ts->buff;
		ts->pos.offset	= 0;

		ts->tsh.pid			= ts->stream[ sid ].pid;
		ts->tsh.start		= 0;
		ts->tsh.cnt			= ( ts->stream[ sid ].cnt ++ ) & 0x0F;

		if ( packet_size < 184 ) {
			ts->tsh.adaptation	= TS_ADAPTATION_BOTH;

			ts_write_ts_header( &ts->tsh, &ts->pos );

			header_size = 6;

			if ( ( header_size + packet_size ) < TS_PACKET_SIZE ) {
				dummy_size = TS_PACKET_SIZE - header_size - packet_size;
			} else {
				dummy_size	= 0;
				packet_size = TS_PACKET_SIZE - header_size;
			}

			put_bit( &ts->pos, 8, dummy_size + 1 );
			put_bit( &ts->pos, 8, 0 );

			header_size += dummy_size;
		} else {
			ts->tsh.adaptation	= TS_ADAPTATION_PAYLOAD_ONYL;

			ts_write_ts_header( &ts->tsh, &ts->pos );

			header_size = 4;
		}

		write( ts->fd, ts->buff, header_size );
		write( ts->fd, src, packet_size );

		src += packet_size;
	}

	return SUCCESS;
}

//==========================================================================
// APIs
//==========================================================================
int
TS_WriteFrame( TSHandle handle, AVFrame *frame )
{
	ts_handle_t *ts	= ( ts_handle_t * )handle;

	if ( frame && ts && ( ts->fd > 0 ) ) {
		if ( ts->start_time.tv_sec == 0 ) {
			ts->start_time	= frame->ptime;
		}

		ts_write_pes( ts, frame );

		return SUCCESS;
	}
	return FAILURE;
}

TSHandle
TS_Open( char *filename, int fd )
{
	if ( filename || fd > 0 ) {
		ts_handle_t *ts	= ( ts_handle_t * )my_malloc( TS_HANDLE_SIZE );

		if ( ts ) {
			memset( ts, 0, TS_HANDLE_SIZE );

			if ( fd > 0 ) {
				ts->fd	= fd;
			} else {
				ts->fd	= open( filename, ( O_CREAT | O_RDWR ), 0755 );
			}

			if ( ts->fd <= 0 ) {
				dbg_warm( "Create File: %s Failed!!!", filename );
				my_free( ts );
				ts	= NULL;
			} else {
				ts->stream[ TS_STREAM_VIDEO ].sid		= TS_SID_VIDEO_STREAM;
				ts->stream[ TS_STREAM_VIDEO ].pid		= TS_PID_VIDEO_STREAM;
				ts->stream[ TS_STREAM_VIDEO ].pts		= 90;

				ts->stream[ TS_STREAM_AUDIO ].sid		= TS_SID_AUDIO_STREAM;
				ts->stream[ TS_STREAM_AUDIO ].pid		= TS_PID_AUDIO_STREAM;
				ts->stream[ TS_STREAM_AUDIO ].pts		= 90;

				ts_write_pat( ts );
				ts_write_pmt( ts );
			}
		}

		return ts;
	}
	return NULL;
}

void
TS_Close( TSHandle handle )
{
	ts_handle_t *ts	= ( ts_handle_t * )handle;

	if ( ts ) {
		if ( ts->fd > 0 ) {
			close( ts->fd );
		}

		my_free( ts );
	}
}

