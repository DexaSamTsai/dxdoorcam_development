/*
 * MP4.h
 *
 *  Created on: 2017/09/11
 *      Author: Sam Tsai
 */

# ifndef __MP4_H__
# define __MP4_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
typedef void *		MP4Handle;

typedef struct {
	struct {
		u32			fps;
	} video;

	struct {
		u32			sample_rate;
		u32			packet_size;
		u8			sample_size;
		u8			channels;
	} audio;
} MP4_CodecCtx;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
MP4_WriteFrame( MP4Handle handle, AVFrame *frame );

extern MP4Handle
MP4_Open( char *filename, MP4_CodecCtx *ctx, int fd );

extern int
MP4_Flush( MP4Handle handle );

extern void
MP4_Close( MP4Handle handle );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __MP4_H__
