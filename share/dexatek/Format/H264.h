/*!
 *  @file       h264.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

#ifndef _H264_H_
#define _H264_H_

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
#define H264_START_CODE			0x00000001
#define H264_NAL_TYPE_MASK		0x1F
#define H264_NAL_TYPE_SPS		0x07
#define H264_NAL_TYPE_PPS		0x08
#define H264_NAL_TYPE_I			0x05
#define H264_NAL_TYPE_SEI		0x06
#define H264_NAL_TYPE_P			0x01

struct h264_info {
	u32		profile_idc;
	u32		level_idc;
	u32 	sps_id;
	u32 	max_frame_num;

	u32 	poc_type;
	u32		delta_pic_order_always_zero_flag;
	u32 	offset_for_non_ref_pic;
	u32 	offset_for_top_to_bottom_field;
	u32 	poc_cycle_length;

	u32 	ref_frame_count;
	u32 	gaps_in_frame_num_allowed_flag;
	u32 	w;
	u32 	h;
};

//==========================================================================
// APIs
//==========================================================================
extern int
h264_parser_config( u8 *config, int size, struct h264_info *h264i );

extern int
h264_get_sps( u8 *src, int size, struct iovec *sps );

extern int
h264_get_pps( u8 *src, int size, struct iovec *pps );

extern int
h264_find_i( u8 *src, int size );

extern int
h264_find_p( u8 *src, int size );

# ifdef __cplusplus
}
# endif // __cplusplus

#endif	// _H264_H_
