/*
 * MP4.c
 *
 *  Created on: 2017/09/11
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "MP4.h"
# include "MP4Box.h"
# include "H264.h"

//==========================================================================
// Type Define
//==========================================================================
# define MP4_VIDEO_TIME_SCALE			90000
# define MP4_AUDIO_TIME_SCALE			48000
# define MP4_VIDEO_DEFAULT_PTS			3003
# define MP4_AUDIO_DEFAULT_PTS			1024
# define MP4_VIDEO_BUFF_SIZE			20 * 1024 * 1024
# define MP4_AUDIO_BUFF_SIZE			2 * 1024 * 1024
# define MP4_VIDEO_FRAME_MAX			( 60 * ( 30 + 30 ) )
# define MP4_AUDIO_FRAME_MAX			( 120 * ( 30 + 30 ) )

# define HEADER_MAX 					64 * 1024
# define MP4_FRAME_MAX 					( 30 * ( 30 + 30 ) )		// 30 FPS x ( Pre_Duration + Poet_Duration )

enum {
	MP4_TRACK_VIDEO		= 0,
	MP4_TRACK_AUDIO,
	MP4_TRACK_MAX
};

typedef struct {
	u32				offset;
	u32				size;
	struct timeval	ptime;
} mp4_frame_t;

typedef struct {
	mp4_frame_t		*frame;
	u32				frame_cnt;
	u32				duration;
	u32				timescale;
	u32				pts;
	u8				track_id;
	u32				pos;

	u8				sample_size;
	u8				channels;

	u8				*buff;
	u32				buff_size;
	u32				offset;
} mp4_track_t;

typedef struct  {
	int					fd;
	struct timeval		start_time;
	struct timeval		end_time;

	u8					header[ HEADER_MAX ];
	u32					header_size;
	u32					media_duration;

	u8					sps[ 128 ];
	u8					pps[ 128 ];
	int					sps_len;
	int					pps_len;
	int					w;
	int					h;

	mp4_track_t			track[ MP4_TRACK_MAX ];

	u32					media_cnt;
} mp4_handle_t;

# define MP4_HANDLE_SIZE					sizeof( mp4_handle_t )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
build_box( box_t *box, int size, char *code )
{
	box->code[ 0 ]	= code[ 0 ];
	box->code[ 1 ]	= code[ 1 ];
	box->code[ 2 ]	= code[ 2 ];
	box->code[ 3 ]	= code[ 3 ];

	box->size = htonl( size );
}

static int
build_ftyp( u8 *data )
{
	ftyp_t *ftyp = ( ftyp_t * )data;
	int size = 0;

	memset( ftyp, 0, sizeof( ftyp_t ) );

//	sprintf( ( char * )ftyp->major_brand, "dash" );
	memcpy( ftyp->major_brand, "mp42", 4 );
	size += 4;

	ftyp->minor_version	= htonl( 0x00000001 );
	size += 4;

//	sprintf( ( char * )ftyp->compatible_brands, "iso6avc1mp41" );
	memcpy( ftyp->compatible_brands, "mp41mp42isom", 12 );
	size += 12;

	size += BOX_SIZE;
	build_box( &ftyp->box, size, "ftyp" );

	return size;
}

static int
build_mvhd( u8 *data, mp4_handle_t *mp4 )
{
	mvhd_t *mvhd = ( mvhd_t * )data;
	int size = sizeof( mvhd_t );

	memset( mvhd, 0, size );

	mvhd->version				= htonl( 0 );
	mvhd->creation_time			= htonl( mp4->start_time.tv_sec );
	mvhd->modification_time		= htonl( mp4->start_time.tv_sec );
	mvhd->timescale				= htonl( MP4_VIDEO_TIME_SCALE );		// Total Display Duration
	mvhd->duration				= htonl( ( mp4->media_duration * MP4_VIDEO_TIME_SCALE ) / 1000 );
	mvhd->rate					= htonl( 0x00010000 );
	mvhd->volume				= htons( 0x0100 );

	mvhd->matrix[ 0 ]			= htonl( 0x00010000 );
	mvhd->matrix[ 4 ]			= htonl( 0x00010000 );
	mvhd->matrix[ 8 ]			= htonl( 0x40000000 );

	mvhd->next_track_ID			= htonl( mp4->media_cnt + 1 );

	build_box( &mvhd->box, size, "mvhd" );

	return size;
}

static int
build_tkhd( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	tkhd_t *tkhd = ( tkhd_t * )data;
	int size = sizeof( tkhd_t );

	memset( tkhd, 0, size );

	tkhd->version				= htonl( 0x00000001 );
	tkhd->creation_time			= htonl( mp4->start_time.tv_sec );
	tkhd->modification_time		= htonl( mp4->start_time.tv_sec );
	tkhd->track_ID				= htonl( track->track_id + 1 );
	tkhd->duration				= htonl( ( mp4->media_duration * MP4_VIDEO_TIME_SCALE ) / 1000 );

	tkhd->layer					= htons( 0 );
	tkhd->alternate_group		= htons( 0 );

	tkhd->matrix[ 0 ]			= htonl( 0x00010000 );
	tkhd->matrix[ 1 ]			= htonl( 0 );
	tkhd->matrix[ 2 ]			= htonl( 0 );
	tkhd->matrix[ 3 ]			= htonl( 0 );
	tkhd->matrix[ 4 ]			= htonl( 0x00010000 );
	tkhd->matrix[ 5 ]			= htonl( 0 );
	tkhd->matrix[ 6 ]			= htonl( 0 );
	tkhd->matrix[ 7 ]			= htonl( 0 );
	tkhd->matrix[ 8 ]			= htonl( 0x40000000 );

	if ( track->track_id == MP4_TRACK_VIDEO ) {
		tkhd->volume				= htons( 0 );	// volume: audio 0x0100, else 0;( 2 )
		tkhd->width					= htonl( mp4->w << 16 );
		tkhd->height				= htonl( mp4->h << 16 );
	} else {
		tkhd->volume				= htons( 0x0100 );	// volume: audio 0x0100, else 0;( 2 )
		tkhd->width					= htonl( 0 );
		tkhd->height				= htonl( 0 );
	}

	build_box( &tkhd->box, size, "tkhd" );

	return size;
}

static int
build_elst( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	elst_t *elst = ( elst_t * )data;
	int size = sizeof( elst_t );

	elst->version				= htonl( 0 );
	elst->entry_cnt				= htonl( 1 );

	elst->entry[ 0 ].seq_duration			= htonl( ( mp4->media_duration * MP4_VIDEO_TIME_SCALE ) / 1000 );

	if ( track->track_id == MP4_TRACK_VIDEO ) {
		elst->entry[ 0 ].media_time			= htonl( track->pts );
	} else {
		elst->entry[ 0 ].media_time			= htonl( 0 );

	}

	elst->entry[ 0 ].media_rate_integer		= htons( 1 );
	elst->entry[ 0 ].media_rate_fraction	= htons( 0 );

	size += sizeof( elst_entry_t );

	build_box( &elst->box, size, "elst" );

	return size;
}

static int
build_edts( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	edts_t *edts = ( edts_t * )data;
	int size = sizeof( edts_t );

	size += build_elst( edts->data, mp4, track );

	build_box( &edts->box, size, "edts" );

	return size;
}

static int
build_mdhd( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	mdhd_t *mdhd = ( mdhd_t * )data;
	int size = sizeof( mdhd_t );

	mdhd->version				= htonl( 0 );
	mdhd->creation_time			= htonl( mp4->start_time.tv_sec );
	mdhd->modification_time		= htonl( mp4->start_time.tv_sec );
	mdhd->timescale				= htonl( track->timescale );
	mdhd->duration				= htonl( track->duration );
	mdhd->language				= htons( 0x55c4 );
	mdhd->quality				= htons( 0 );

	build_box( &mdhd->box, size, "mdhd" );

	return size;
}

static int
build_hdlr( u8 *data, mp4_track_t *track )
{
	hdlr_t *hdlr = ( hdlr_t * )data;
	int size = sizeof( hdlr_t );

	memset( hdlr, 0, sizeof( hdlr_t ) );

	hdlr->version				= htonl( 0 );
	hdlr->pre_defined			= htonl( 0 );
	if ( track->track_id == MP4_TRACK_VIDEO ) {
		hdlr->handler_type			= htonl( 0x76696465 ); // vide
		size += sprintf( hdlr->name, "VideoHandler" );
	} else {
		hdlr->handler_type			= htonl( 0x736F756E ); // vide
		size += sprintf( hdlr->name, "AudioHandler" );
	}

	size ++;

	build_box( &hdlr->box, size, "hdlr" );

	return size;
}

static int
build_url( u8 *data )
{
	url_t *url = ( url_t * )data;
	int size = sizeof( url_t );

	url->version	= htonl( 0x00000001 );

	build_box( &url->box, size, "url " );

	return size;
}

static int
build_dref( u8 *data )
{
	dref_t *dref = ( dref_t * )data;
	int size = 0;

	dref->version				= htonl( 0 );
	dref->entry_count			= htonl( 1 );
	size += 8;

	size += build_url( dref->data );

	size += BOX_SIZE;
	build_box( &dref->box, size, "dref" );

	return size;
}

static int
build_dinf( u8 *data )
{
	dinf_t *dinf = ( dinf_t * )data;
	int size = 0;

	size += build_dref( &dinf->data[ size ] );

	size += BOX_SIZE;
	build_box( &dinf->box, size, "dinf" );

	return size;
}

static int
build_avcC( u8 *data, mp4_handle_t *mp4 )
{
	avcC_t *avcC = ( avcC_t * )data;
	int size = sizeof( avcC_t );
	int	offset = 0;

	memset( avcC, 0, sizeof( avcC_t ) );

	avcC->version				= 0x01;
	avcC->sps[ 0 ]				= mp4->sps[ 1 ];
	avcC->sps[ 1 ]				= mp4->sps[ 2 ];
	avcC->sps[ 2 ]				= mp4->sps[ 3 ];
	avcC->nula_size				= 0xFF;
	avcC->sps_cnt				= 0xE1;

	avcC->data[ offset ++ ]			= ( mp4->sps_len & 0xFF00 ) >> 8;
	avcC->data[ offset ++ ]			= ( mp4->sps_len & 0xFF );

	memcpy( &avcC->data[ offset ], mp4->sps, mp4->sps_len );

	offset += mp4->sps_len;

	avcC->data[ offset ++ ]			= 1;
	avcC->data[ offset ++ ]			= ( mp4->pps_len & 0xFF00 ) >> 8;
	avcC->data[ offset ++ ]			= ( mp4->pps_len & 0xFF );

	memcpy( &avcC->data[ offset ], mp4->pps, mp4->pps_len );

	offset += mp4->pps_len;

	size += offset;

	build_box( &avcC->box, size, "avcC" );

	return size;
}

static int
build_avc( u8 *data, mp4_handle_t *mp4 )
{
	avc_t *avc = ( avc_t * )data;
	int size = sizeof( avc_t );

	memset( avc, 0, sizeof( avc_t ) );

	avc->data_reference			= htons( 0x0001 );
	avc->version				= htons( 0 );
	avc->revision				= htons( 0 );
	avc->vendor					= htonl( 0 );
	avc->temporal_quality		= htonl( 0 );
	avc->spatial_quality		= htonl( 0 );
	avc->width					= htons( mp4->w );
	avc->height					= htons( mp4->h );
	avc->dpi_horizontal			= htonl( 0x00480000 );
	avc->dpi_vertical			= htonl( 0x00480000 );
	avc->data_size				= htonl( 0 );
	avc->frames_per_sample		= htons( 1 );
	avc->depth					= 0x18;
	avc->ctab_id				= htons( 0xFFFF );

	size += build_avcC( avc->data, mp4 );

	build_box( &avc->box, size, "avc1" );

	return size;
}

static int
build_esds( u8 *data, mp4_track_t *track )
{
	esds_t *esds = ( esds_t * )data;
	int size = sizeof( esds_t );

	memset( esds, 0, sizeof( esds_t ) );

	u16 audio_spec	= ( MP4_AAC_TYPE_LC << 11 ) & 0xF800;

	switch ( track->timescale ) {
		case 96000:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_96000HZ << 7 ) & 0x0780;
			break;

		case 88200:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_88200HZ << 7 ) & 0x0780;
			break;

		case 64000:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_64000HZ << 7 ) & 0x0780;
			break;

		case 48000:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_48000HZ << 7 ) & 0x0780;
			break;

		case 44100:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_44100HZ << 7 ) & 0x0780;
			break;

		case 16000:
			audio_spec |= ( MP4_AAC_SAMPLE_RATE_16000HZ << 7 ) & 0x0780;
			break;

		default:
			break;
	}

	audio_spec |= ( track->channels << 3 ) & 0x0078;

	if ( track->pts == 1024 ) {
		audio_spec |= ( MP4_AAC_PACKET_1024 << 2 );
	} else {
		audio_spec |= ( MP4_AAC_PACKET_960 << 2 );
	}

	esds->version	= htonl( 0 );
	esds->tag		= 0x03;
	esds->tag_size	= 0x19;
	esds->track_id	= htons( MP4_TRACK_AUDIO + 1 );
	esds->flag		= 0;

	esds->dec_config_tag		= 0x04;					// 04 (tag)
	esds->dec_config_tag_size	= 0x11;					// 11 (tag size)
	esds->dec_config_type		= 0x40;					// 40 (MPEG-4 audio)
	esds->dec_config_straem		= 0x15;					// 15 (audio stream)

	esds->dec_info_tag			= 0x05;					// 05 (tag)
	esds->dec_info_tag_size		= 0x02;					// 02 (tag size)

	esds->dec_info_spec_config	= htons( audio_spec );	// Audio Specific Config

	esds->sl_tag				= 0x06;					// 06 (tag)
	esds->sl_tag_size			= 0x01;					// 01 (tag size)
	esds->end					= 0x02;					// 02 (always 2 refer from mov_write_esds_tag);

	build_box( &esds->box, size, "esds" );

	return size;
}

static int
build_mp4a( u8 *data, mp4_track_t *track )
{
	mp4a_t *mp4a = ( mp4a_t * )data;
	int size = sizeof( mp4a_t );

	memset( mp4a, 0, sizeof( mp4a_t ) );

	mp4a->data_reference_index		= htons( 0x0001 );
	mp4a->channelcount				= htons( track->channels );
	mp4a->samplesize				= htons( track->sample_size );
	mp4a->samplerate				= htons( track->timescale );

	size += build_esds( mp4a->data, track );

	build_box( &mp4a->box, size, "mp4a" );

	return size;
}

static int
build_stsd( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	stsd_t *stsd = ( stsd_t * )data;
	int size = sizeof( stsd_t );

	stsd->version				= htonl( 0 );
	stsd->sample_entries_size	= htonl( 1 );

	if ( track->track_id == MP4_TRACK_VIDEO ) {
		size += build_avc( stsd->data, mp4 );
	} else {
		size += build_mp4a( stsd->data, track );
	}

	build_box( &stsd->box, size, "stsd" );

	return size;
}

static int
build_stts( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	stts_t *stts = ( stts_t * )data;
	int size = sizeof( stts_t );

	memset( stts, 0, sizeof( stts_t ) );

	stts->time_to_sample_size	= htonl( 1 );

	stts->sample[ 0 ].sample_count	= htonl( track->frame_cnt );
	stts->sample[ 0 ].sample_delta	= htonl( track->pts );

	size += 1 * sizeof( stts_entry_t );

	build_box( &stts->box, size, "stts" );

	return size;
}

static int
build_stss( u8 *data )
{
	stss_t *stss = ( stss_t * )data;
	int size = sizeof( stss_t );

	memset( stss, 0, sizeof( stss_t ) );

	stss->entry_size	= htonl( 1 );

	stss->entry[ 0 ].sample_number	= htonl( 1 );

	size += 1 * sizeof( stss_entry_t );

	build_box( &stss->box, size, "stss" );

	return size;
}

static int
build_stsc( u8 *data, mp4_track_t *track )
{
	stsc_t *stsc = ( stsc_t * )data;
	int size = sizeof( stsc_t );

	memset( stsc, 0, sizeof( stsc_t ) );

	stsc->sample_to_chunk_size	= htonl( 1 );

	stsc->chunk[ 0 ].first_chunk			= htonl( 1 );
	stsc->chunk[ 0 ].samples				= htonl( track->frame_cnt );
	stsc->chunk[ 0 ].sample_description_id	= htonl( 1 );

	size += 1 * sizeof( stsc_entry_t );

	build_box( &stsc->box, size, "stsc" );

	return size;
}

static int
build_stco( u8 *data, mp4_track_t *track )
{
	stco_t *stco = ( stco_t * )data;
	int size = sizeof( stco_t );
	u32	offset;

	memset( stco, 0, sizeof( stco_t ) );

	stco->chunk_offset_size	= htonl( 1 );

	offset = track->frame[ 0 ].offset;
	offset += track->pos;			// ftyp size + media header size

	stco->data[ 0 ]			= htonl( offset );

	size += 1 * sizeof( u32 );

	build_box( &stco->box, size, "stco" );

	return size;
}

static int
build_stsz( u8 *data, mp4_track_t *track )
{
	stsz_t *stsz = ( stsz_t * )data;
	int size = sizeof( stsz_t ), i;

	memset( stsz, 0, sizeof( stsz_t ) );

	stsz->sample_count	= htonl( track->frame_cnt );

	for ( i = 0; i < track->frame_cnt; i ++ ) {
		stsz->frame_size[ i ]	= htonl( track->frame[ i ].size );
	}

	size +=  track->frame_cnt * sizeof( u32 );

	build_box( &stsz->box, size, "stsz" );

	return size;
}

static int
build_stbl( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	stbl_t *stbl = ( stbl_t * )data;
	int size = 0;

	size += build_stsd( &stbl->data[ size ], mp4, track );
	size += build_stts( &stbl->data[ size ], mp4, track );

	if ( track->track_id == MP4_TRACK_VIDEO ) {
		size += build_stss( &stbl->data[ size ] );
	}

	size += build_stsc( &stbl->data[ size ], track );
	size += build_stsz( &stbl->data[ size ], track );
	size += build_stco( &stbl->data[ size ], track );

	size += BOX_SIZE;
	build_box( &stbl->box, size, "stbl" );

	return size;
}

static int
build_vmhd( u8 *data )
{
	vmhd_t *vmhd = ( vmhd_t * )data;
	int size = sizeof( vmhd_t );

	memset( vmhd, 0, sizeof( vmhd_t ) );

	vmhd->version	= htonl( 0x00000001 );

	build_box( &vmhd->box, size, "vmhd" );

	return size;
}

static int
build_smhd( u8 *data )
{
	smhd_t *smhd = ( smhd_t * )data;
	int size = sizeof( smhd_t );

	memset( smhd, 0, sizeof( smhd_t ) );

	smhd->version	= htonl( 0x00000000 );

	build_box( &smhd->box, size, "smhd" );

	return size;
}

static int
build_minf( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	minf_t *minf = ( minf_t * )data;
	int size = 0;

	if ( track->track_id == MP4_TRACK_VIDEO ) {
		size += build_vmhd( &minf->data[ size ] );
	} else {
		size += build_smhd( &minf->data[ size ] );
	}

	size += build_dinf( &minf->data[ size ] );
	size += build_stbl( &minf->data[ size ], mp4, track );

	size += BOX_SIZE;
	build_box( &minf->box, size, "minf" );

	return size;
}

static int
build_mdia( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	mdia_t *mdia = ( mdia_t * )data;
	int size = 0;

	size += build_mdhd( &mdia->data[ size ], mp4, track );
	size += build_hdlr( &mdia->data[ size ], track );
	size += build_minf( &mdia->data[ size ], mp4, track );

	size += BOX_SIZE;
	build_box( &mdia->box, size, "mdia" );

	return size;
}

static int
build_trak( u8 *data, mp4_handle_t *mp4, mp4_track_t *track )
{
	trak_t *trak = ( trak_t * )data;
	int size = 0;

	size += build_tkhd( &trak->data[ size ], mp4, track );
	size += build_edts( &trak->data[ size ], mp4, track );
	size += build_mdia( &trak->data[ size ], mp4, track );

	size += BOX_SIZE;
	build_box( &trak->box, size, "trak" );

	return size;
}

static int
build_moov( u8 *data, mp4_handle_t *mp4 )
{
	moov_t *moov = ( moov_t * )data;
	int size = 0;

	size += build_mvhd( &moov->data[ size ], mp4 );

	if ( mp4->track[ MP4_TRACK_VIDEO ].frame_cnt ) {
		size += build_trak( &moov->data[ size ], mp4, &mp4->track[ MP4_TRACK_VIDEO ] );
	}

	if ( mp4->track[ MP4_TRACK_AUDIO ].frame_cnt ) {
		size += build_trak( &moov->data[ size ], mp4, &mp4->track[ MP4_TRACK_AUDIO ] );
	}

	size += BOX_SIZE;
	build_box( &moov->box, size, "moov" );

	return size;
}

static void
mov_parser_parameter_set( mp4_handle_t *mp4, av_frame_t *frame )
{
	struct iovec	sps, pps;
	struct h264_info	h264i;

	memset( &h264i, 0, sizeof( struct h264_info ) );

	h264_get_sps( frame->buff, frame->size, &sps );
	h264_get_pps( frame->buff, frame->size, &pps );

	if ( sps.iov_base && pps.iov_base ) {
		u8 *sps_start = ( u8 * )sps.iov_base - 4;

		memcpy( mp4->sps, sps.iov_base, sps.iov_len );
		memcpy( mp4->pps, pps.iov_base, pps.iov_len );

		mp4->sps_len = sps.iov_len;
		mp4->pps_len = pps.iov_len;

		if ( h264_parser_config( sps_start, mp4->sps_len + 4 , &h264i ) < 0 ) {
			mp4->w = 1920;
			mp4->h = 1080;
		} else {
			mp4->w = h264i.w;
			mp4->h = h264i.h;
		}
	}
}

static int
mov_put_nal( u8 *dst, u8 *src, u32 size )
{
	if ( GET_32( src ) != H264_START_CODE ) {
		PUT_32( dst, size );
		dst += 4;
		memcpy( dst, src, size );
		return ( size + 4 );
	} else {
		PUT_32( dst, ( size - 4 ) );
		dst += 4;
		memcpy( dst, &src[ 4 ], ( size - 4 ) );
		return size;
	}
}

static u32
mp4_put_audio( mp4_handle_t *mp4, av_frame_t *frame )
{
	mp4_track_t *track	= &mp4->track[ MP4_TRACK_AUDIO ];

	if ( track->frame_cnt > MP4_AUDIO_FRAME_MAX ) {
		dbg_warm( "track->frame_cnt > MP4_AUDIO_FRAME_MAX( %d )!!!", MP4_AUDIO_FRAME_MAX );
		return 0;
	} else if ( track->offset + frame->size > track->buff_size ) {
		dbg_warm( "track->offset( %d ) + frame->size( %d ) >track->buff_size( %d )!!!",	track->offset, frame->size, track->buff_size );
		return 0;
	} else {
		memcpy( &track->buff[ track->offset ], &frame->buff[ 7 ], frame->size - 7 );

		return frame->size - 7;
	}
}

static u32
mp4_put_video( mp4_handle_t *mp4, av_frame_t *frame )
{
	mp4_track_t *track	= &mp4->track[ MP4_TRACK_VIDEO ];
	u8			*dst	= &track->buff[ track->offset ];

	if ( track->frame_cnt > MP4_VIDEO_FRAME_MAX ) {
		dbg_warm( "track->frame_cnt > MP4_VIDEO_FRAME_MAX( %d )!!!", MP4_VIDEO_FRAME_MAX );
		return 0;
	} else if ( track->offset + frame->size > track->buff_size ) {
		dbg_warm( "track->offset( %d ) + frame->size( %d ) > track->buff_size( %d )!!!", track->offset, frame->size, track->buff_size );
		return 0;
	} else if ( ( frame->buff[ 4 ] & H264_NAL_TYPE_MASK ) == H264_NAL_TYPE_SPS ) {
		int offset;

		mov_parser_parameter_set( mp4, frame );

		dst += mov_put_nal( dst, mp4->sps, mp4->sps_len );
		dst += mov_put_nal( dst, mp4->pps, mp4->pps_len );

		offset = dst - &track->buff[ track->offset ];
# if 0
		dst += mov_put_nal( dst, &frame->buff[ offset ], ( frame->size - offset ) );
# else
		mov_put_nal( &track->buff[ track->offset ], &frame->buff[ offset ], ( frame->size - offset ) );
		return ( frame->size - offset );
# endif
	} else {
		mov_put_nal( dst, frame->buff, frame->size );
	}

	return frame->size;
}

static u32
mp4_put_frame( mp4_handle_t *mp4, av_frame_t *frame )
{
	u32 res = 0;
	mp4_track_t *track;

	if ( ( frame->type & FRAME_AV_MASK ) == FRAME_VIDEO ) {
		res		= mp4_put_video( mp4, frame );
		track	= &mp4->track[ MP4_TRACK_VIDEO ];
	} else {
		res		= mp4_put_audio( mp4, frame );
		track	= &mp4->track[ MP4_TRACK_AUDIO ];
	}

	if ( res ) {
		if ( mp4->start_time.tv_sec == 0 ) {
			mp4->start_time	= frame->ptime;
		}

		mp4->end_time	= frame->ptime;

		track->frame[ track->frame_cnt ].ptime	= frame->ptime;
		track->frame[ track->frame_cnt ].size	= res;
		track->frame[ track->frame_cnt ].offset	= track->offset;
		track->frame_cnt ++;

		track->offset += res;
	}

	return res;
}

static int
mp4_write_data( mp4_handle_t *mp4, void *src, int size )
{
	int res;

	while ( size > 0 ) {
		if ( mp4->fd > 0 ) {
			res = write( mp4->fd, src, size );
		} else {
			return FAILURE;
		}

		if ( res < 0 ) {
			if ( ( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				MSleep( 50 );
				continue;
			}

			dbg_error( "Do write Failed!!!( res = %d, %s )", res, strerror( errno ) );
			return FAILURE;
		} else if ( res == 0 ) {
			MSleep( 50 );
		}

		size -= res;
		src  += res;
	}

	return SUCCESS;
}

static int
mp4_build_file_header( mp4_handle_t *mp4 )
{
	u8 *header = mp4->header;

	mp4->header_size = build_ftyp( &header[ mp4->header_size ] );

	return mp4_write_data( mp4, mp4->header, mp4->header_size );
}

static int
mp4_build_media_data( mp4_handle_t *mp4 )
{
	mdat_t	*mdat = ( mdat_t * )mp4->header;
	mp4_track_t *track;

	u32 size = sizeof( mdat_t ) + mp4->track[ MP4_TRACK_VIDEO ].offset + mp4->track[ MP4_TRACK_AUDIO ].offset;
	u32 pos	= 28;
	u32	duration;

	wide_t	*wide = ( wide_t * )mp4->header;

	build_box( &wide->box, sizeof( wide_t ), "wide" );
	mp4->header_size = sizeof( wide_t );
	pos	+= sizeof( wide_t );

	if ( mp4_write_data( mp4, mp4->header, mp4->header_size ) != SUCCESS ) {
		return FAILURE;
	}

	build_box( &mdat->box, size, "mdat" );
	mp4->header_size = sizeof( mdat_t );
	pos	+= sizeof( mdat_t );

	if ( mp4->start_time.tv_sec ) {
		mp4->media_duration	= ( mp4->end_time.tv_sec - mp4->start_time.tv_sec ) * 1000;
	}

	if ( mp4->track[ MP4_TRACK_VIDEO ].frame_cnt ) {
		track	= &mp4->track[ MP4_TRACK_VIDEO ];

		track->pos		= pos;

		duration = ( track->frame[ track->frame_cnt - 1 ].ptime.tv_sec - track->frame[ 0 ].ptime.tv_sec ) * 1000;
		duration += ( track->frame[ track->frame_cnt - 1 ].ptime.tv_usec - track->frame[ 0 ].ptime.tv_usec ) / 1000;

		if ( duration > 47721 ) {
			track->duration	= 0xFFFFFFFF;
		} else {
			track->duration	= ( duration * track->timescale ) / 1000;
		}

		pos += track->offset;
		mp4->media_cnt ++;

		if ( mp4->media_duration ) {
			track->pts			= ( MP4_VIDEO_TIME_SCALE / ( track->frame_cnt / ( mp4->end_time.tv_sec - mp4->start_time.tv_sec ) ) );
		}
	}

	if ( mp4->track[ MP4_TRACK_AUDIO ].frame_cnt ) {
		track	= &mp4->track[ MP4_TRACK_AUDIO ];

		track->pos		= pos;

		duration = ( track->frame[ track->frame_cnt - 1 ].ptime.tv_sec - track->frame[ 0 ].ptime.tv_sec ) * 1000;
		duration += ( track->frame[ track->frame_cnt - 1 ].ptime.tv_usec - track->frame[ 0 ].ptime.tv_usec ) / 1000;

		track->duration	= ( duration * track->timescale ) / 1000;

		pos += track->offset;
		mp4->media_cnt ++;
	}

	if ( mp4_write_data( mp4, mp4->header, mp4->header_size ) != SUCCESS ) {
		return FAILURE;
	}

	if ( mp4->track[ MP4_TRACK_VIDEO ].frame_cnt ) {
		if ( mp4_write_data( mp4, mp4->track[ MP4_TRACK_VIDEO ].buff, mp4->track[ MP4_TRACK_VIDEO ].offset ) != SUCCESS ) {
			return FAILURE;
		}
	}

	if ( mp4->track[ MP4_TRACK_AUDIO ].frame_cnt ) {
		if ( mp4_write_data( mp4, mp4->track[ MP4_TRACK_AUDIO ].buff, mp4->track[ MP4_TRACK_AUDIO ].offset ) != SUCCESS ) {
			return FAILURE;
		}
	}

	return SUCCESS;
}

static int
mp4_build_media_index( mp4_handle_t *mp4 )
{
	mp4->header_size	= build_moov( mp4->header, mp4 );

	return mp4_write_data( mp4, mp4->header, mp4->header_size );
}

static void
mp4_free( mp4_handle_t *mp4 )
{
	if ( mp4->track[ MP4_TRACK_VIDEO ].buff ) {
		my_free( mp4->track[ MP4_TRACK_VIDEO ].buff );
		mp4->track[ MP4_TRACK_VIDEO ].buff	= NULL;
	}

	if ( mp4->track[ MP4_TRACK_VIDEO ].frame ) {
		my_free( mp4->track[ MP4_TRACK_VIDEO ].frame );
		mp4->track[ MP4_TRACK_VIDEO ].frame	= NULL;
	}

	if ( mp4->track[ MP4_TRACK_AUDIO ].buff ) {
		my_free( mp4->track[ MP4_TRACK_AUDIO ].buff );
		mp4->track[ MP4_TRACK_AUDIO ].buff	= NULL;
	}

	if ( mp4->track[ MP4_TRACK_AUDIO ].frame ) {
		my_free( mp4->track[ MP4_TRACK_AUDIO ].frame );
		mp4->track[ MP4_TRACK_AUDIO ].frame	= NULL;
	}

	if ( mp4->fd > 0 ) {
		close( mp4->fd );
	}

	my_free( mp4 );
}

//==========================================================================
// APIs
//==========================================================================
int
MP4_WriteFrame( MP4Handle handle, AVFrame *frame )
{
	mp4_handle_t *mp4	= ( mp4_handle_t * )handle;

	if ( frame && mp4 ) {
		return mp4_put_frame( mp4, frame );
	}

	return FAILURE;
}

MP4Handle
MP4_Open( char *filename, MP4_CodecCtx *ctx, int fd )
{
	if ( ctx && ( filename || ( fd > 0 ) ) ) {
		mp4_handle_t *mp4	= ( mp4_handle_t * )my_malloc( MP4_HANDLE_SIZE );

		if ( mp4 == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			return NULL;
		}

		mp4->track[ MP4_TRACK_VIDEO ].track_id		= MP4_TRACK_VIDEO;
		mp4->track[ MP4_TRACK_VIDEO ].timescale		= MP4_VIDEO_TIME_SCALE;

		if ( ctx->video.fps ) {
			mp4->track[ MP4_TRACK_VIDEO ].pts			= ( MP4_VIDEO_TIME_SCALE / ctx->video.fps );
		} else {
			mp4->track[ MP4_TRACK_VIDEO ].pts			= MP4_VIDEO_DEFAULT_PTS;
		}

		mp4->track[ MP4_TRACK_VIDEO ].buff_size		= MP4_VIDEO_BUFF_SIZE;
		mp4->track[ MP4_TRACK_VIDEO ].buff			= ( u8 * )my_malloc( MP4_VIDEO_BUFF_SIZE );
		if ( mp4->track[ MP4_TRACK_VIDEO ].buff == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			mp4_free( mp4 );
			return NULL;
		}

		mp4->track[ MP4_TRACK_VIDEO ].frame			= ( mp4_frame_t * )my_malloc( MP4_VIDEO_FRAME_MAX * sizeof( mp4_frame_t ) );
		if ( mp4->track[ MP4_TRACK_VIDEO ].frame == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			mp4_free( mp4 );
			return NULL;
		}

		mp4->track[ MP4_TRACK_AUDIO ].track_id		= MP4_TRACK_AUDIO;

		if ( ctx->audio.sample_rate ) {
			mp4->track[ MP4_TRACK_AUDIO ].timescale		= ctx->audio.sample_rate;
		} else {
			mp4->track[ MP4_TRACK_AUDIO ].timescale		= MP4_AUDIO_TIME_SCALE;
		}

		if ( ctx->audio.packet_size ) {
			mp4->track[ MP4_TRACK_AUDIO ].pts			= ctx->audio.packet_size;
		} else {
			mp4->track[ MP4_TRACK_AUDIO ].pts			= MP4_AUDIO_DEFAULT_PTS;
		}

		mp4->track[ MP4_TRACK_AUDIO ].sample_size		= ctx->audio.sample_size;
		mp4->track[ MP4_TRACK_AUDIO ].channels			= ctx->audio.channels;

		mp4->track[ MP4_TRACK_AUDIO ].buff_size		= MP4_AUDIO_BUFF_SIZE;
		mp4->track[ MP4_TRACK_AUDIO ].buff			= ( u8 * )my_malloc( MP4_AUDIO_BUFF_SIZE );
		if ( mp4->track[ MP4_TRACK_AUDIO ].buff == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			mp4_free( mp4 );
			return NULL;
		}

		mp4->track[ MP4_TRACK_AUDIO ].frame			= ( mp4_frame_t * )my_malloc( MP4_AUDIO_FRAME_MAX * sizeof( mp4_frame_t ) );
		if ( mp4->track[ MP4_TRACK_AUDIO ].frame == NULL ) {
			dbg_warm( "Out-of-Memory!!!" );
			mp4_free( mp4 );
			return NULL;
		}

		if ( fd > 0 ) {
			mp4->fd	= fd;
		} else {
			mp4->fd	= open( filename, ( O_CREAT | O_RDWR ), 0755 );
		}

		if ( mp4->fd <= 0 ) {
			dbg_warm( "Create File: %s Failed!!!", filename );
			mp4_free( mp4 );
			return NULL;
		}

		return mp4;
	}

	return NULL;
}

int
MP4_Flush( MP4Handle handle )
{
	mp4_handle_t *mp4	= ( mp4_handle_t * )handle;

	if ( mp4 ) {
		mp4_build_file_header( mp4 );
		mp4_build_media_data( mp4 );
		mp4_build_media_index( mp4 );

		return SUCCESS;
	}

	return FAILURE;
}

void
MP4_Close( MP4Handle handle )
{
	mp4_handle_t *mp4	= ( mp4_handle_t * )handle;

	if ( mp4 ) {
		mp4_free( mp4 );
	}
}

