/*!
 *  @file       mp4enc.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "MP4Box.h"
# include "FMP4.h"
# include "Format/H264.h"

//==========================================================================
// Type Define
//==========================================================================
# define TEST_MODE

# define DEF_TIME_SCALE		1000
# define DEF_TIME_SCALE_PF	( DEF_TIME_SCALE / 30 )

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
build_box( box_t *box, int size, char *code )
{
	box->code[ 0 ]	= code[ 0 ];
	box->code[ 1 ]	= code[ 1 ];
	box->code[ 2 ]	= code[ 2 ];
	box->code[ 3 ]	= code[ 3 ];

	box->size = htonl( size );
}

static int
build_ftyp( u8 *data )
{
	ftyp_t *ftyp = ( ftyp_t * )data;
	int size = 0;

	memset( ftyp, 0, sizeof( ftyp_t ) );

//	sprintf( ( char * )ftyp->major_brand, "dash" );
	sprintf( ( char * )ftyp->major_brand, "iso5" );
	size += 4;

	ftyp->minor_version	= htonl( 0x00000001 );
	size += 4;

	sprintf( ( char * )ftyp->compatible_brands, "avc1iso5dash" );
//	sprintf( ( char * )ftyp->compatible_brands, "iso6avc1mp41" );
	size += 12;

	size += BOX_SIZE;
	build_box( &ftyp->box, size, "ftyp" );

	return size;
}

static int
build_mvhd( u8 *data, mp4_media_t *media )
{
	mvhd_t *mvhd = ( mvhd_t * )data;
	int size = sizeof( mvhd_t );

	memset( mvhd, 0, size );

	mvhd->version				= htonl( 0 );
	mvhd->creation_time			= htonl( media->start_time.tv_sec );
	mvhd->modification_time		= htonl( media->start_time.tv_sec );
	mvhd->timescale				= htonl( DEF_TIME_SCALE );		// Total Display Duration
	mvhd->duration				= htonl( 0 );
	mvhd->rate					= htonl( 0x00010000 );
	mvhd->volume				= htons( 0x0100 );

	mvhd->matrix[ 0 ]			= htonl( 0x00010000 );
	mvhd->matrix[ 4 ]			= htonl( 0x00010000 );
	mvhd->matrix[ 8 ]			= htonl( 0x40000000 );

	mvhd->next_track_ID			= htonl( 2 );

	build_box( &mvhd->box, size, "mvhd" );

	return size;
}

static int
build_mehd( u8 *data )
{
	mehd_t *mehd = ( mehd_t * )data;
	int size = sizeof( mehd_t );

	memset( mehd, 0, size );

	mehd->version			= htonl( 0 );
	mehd->freg_duration		= htonl( 0 );

	build_box( &mehd->box, size, "mehd" );

	return size;
}

static int
build_trex( u8 *data )
{
	trex_t *trex = ( trex_t * )data;
	int size = sizeof( trex_t );

	memset( trex, 0, size );

#ifdef TEST_MODE
	trex->version			= htonl( 0 );
	trex->track_id			= htonl( 0x00000001 );
	trex->sample_desc_index	= htonl( 0x00000001 );
	trex->sample_duration	= htonl( 0 );
	trex->sample_size		= htonl( 0 );
	trex->sample_flags		= htonl( 0 );
#else
	trex->version			= htonl( 0 );
	trex->track_id			= htonl( 0x00000001 );
	trex->sample_desc_index	= htonl( 0x00000001 );
	trex->sample_duration	= htonl( DEF_TIME_SCALE_PF );
	trex->sample_size		= htonl( 0 );
	trex->sample_flags		= htonl( 0x00010000 );
#endif

	build_box( &trex->box, size, "trex" );

	return size;
}

static int
build_mvex( u8 *data )
{
	mvex_t *mvex = ( mvex_t * )data;
	int size = 0;

	size += build_mehd( &mvex->data[ size ] );
	size += build_trex( &mvex->data[ size ] );

	size += BOX_SIZE;
	build_box( &mvex->box, size, "mvex" );

	return size;
}

static int
build_tkhd( u8 *data, mp4_media_t *media )
{
	tkhd_t *tkhd = ( tkhd_t * )data;
	int size = sizeof( tkhd_t );

	memset( tkhd, 0, size );

	tkhd->version				= htonl( 0x00000007 );
	tkhd->creation_time			= htonl( media->start_time.tv_sec );
	tkhd->modification_time		= htonl( media->start_time.tv_sec );
	tkhd->track_ID				= htonl( 1 );
	tkhd->duration				= htonl( 0 );

	tkhd->layer					= htons( 0 );
	tkhd->alternate_group		= htons( 0 );
	tkhd->volume				= htons( 0 );	// volume: audio 0x0100, else 0;( 2 )

	tkhd->matrix[ 0 ]			= htonl( 0x00010000 );
	tkhd->matrix[ 1 ]			= htonl( 0 );
	tkhd->matrix[ 2 ]			= htonl( 0 );
	tkhd->matrix[ 3 ]			= htonl( 0 );
	tkhd->matrix[ 4 ]			= htonl( 0x00010000 );
	tkhd->matrix[ 5 ]			= htonl( 0 );
	tkhd->matrix[ 6 ]			= htonl( 0 );
	tkhd->matrix[ 7 ]			= htonl( 0 );
	tkhd->matrix[ 8 ]			= htonl( 0x40000000 );

	tkhd->width					= htonl( media->w << 16 );
	tkhd->height				= htonl( media->h << 16 );

	build_box( &tkhd->box, size, "tkhd" );

	return size;
}

static int
build_mdhd( u8 *data, mp4_media_t *media )
{
	mdhd_t *mdhd = ( mdhd_t * )data;
	int size = sizeof( mdhd_t );

	mdhd->version				= htonl( 0 );
	mdhd->creation_time			= htonl( media->start_time.tv_sec );
	mdhd->modification_time		= htonl( media->start_time.tv_sec );
	mdhd->timescale				= htonl( 0x0FFFFFFF );
	mdhd->duration				= htonl( 0 );
	mdhd->language				= htons( 0x55c4 );
	mdhd->quality				= htons( 0 );

	build_box( &mdhd->box, size, "mdhd" );

	return size;
}

static int
build_hdlr( u8 *data )
{
	hdlr_t *hdlr = ( hdlr_t * )data;
	int size = sizeof( hdlr_t );

	memset( hdlr, 0, sizeof( hdlr_t ) );

	hdlr->version				= htonl( 0 );
	hdlr->pre_defined			= htonl( 0 );
	hdlr->handler_type			= htonl( 0x76696465 ); // vide
	size += sprintf( hdlr->name, "VideoHandler" );
	size ++;
	build_box( &hdlr->box, size, "hdlr" );

	return size;
}

static int
build_url( u8 *data )
{
	url_t *url = ( url_t * )data;
	int size = sizeof( url_t );

	url->version	= htonl( 0x00000001 );

	build_box( &url->box, size, "url " );

	return size;
}

static int
build_dref( u8 *data )
{
	dref_t *dref = ( dref_t * )data;
	int size = 0;

	dref->version				= htonl( 0 );
	dref->entry_count			= htonl( 1 );
	size += 8;

	size += build_url( dref->data );

	size += BOX_SIZE;
	build_box( &dref->box, size, "dref" );

	return size;
}

static int
build_dinf( u8 *data )
{
	dinf_t *dinf = ( dinf_t * )data;
	int size = 0;

	size += build_dref( &dinf->data[ size ] );

	size += BOX_SIZE;
	build_box( &dinf->box, size, "dinf" );

	return size;
}

static int
build_avcC( u8 *data, mp4_media_t *media )
{
	avcC_t *avcC = ( avcC_t * )data;
	int size = sizeof( avcC_t );
	int	offset = 0;

	memset( avcC, 0, sizeof( avcC_t ) );

	avcC->version				= 0x01;
	avcC->sps[ 0 ]				= media->sps[ 1 ];
	avcC->sps[ 1 ]				= media->sps[ 2 ];
	avcC->sps[ 2 ]				= media->sps[ 3 ];
	avcC->nula_size				= 0xFF;
	avcC->sps_cnt				= 0xE1;

	avcC->data[ offset ++ ]			= ( media->sps_len & 0xFF00 ) >> 8;
	avcC->data[ offset ++ ]			= ( media->sps_len & 0xFF );

	memcpy( &avcC->data[ offset ], media->sps, media->sps_len );

	offset += media->sps_len;

	avcC->data[ offset ++ ]			= 1;
	avcC->data[ offset ++ ]			= ( media->pps_len & 0xFF00 ) >> 8;
	avcC->data[ offset ++ ]			= ( media->pps_len & 0xFF );

	memcpy( &avcC->data[ offset ], media->pps, media->pps_len );

	offset += media->pps_len;

	size += offset;

	build_box( &avcC->box, size, "avcC" );

	return size;
}

static int
build_avc( u8 *data, mp4_media_t *media )
{
	avc_t *avc = ( avc_t * )data;
	int size = sizeof( avc_t );

	memset( avc, 0, sizeof( avc_t ) );

	avc->data_reference			= htons( 0x0001 );
	avc->version				= htons( 0 );
	avc->revision				= htons( 0 );
	avc->vendor					= htonl( 0 );
	avc->temporal_quality		= htonl( 0 );
	avc->spatial_quality		= htonl( 0 );
	avc->width					= htons( media->w );
	avc->height					= htons( media->h );
	avc->dpi_horizontal			= htonl( 0x00480000 );
	avc->dpi_vertical			= htonl( 0x00480000 );
	avc->data_size				= htonl( 0 );
	avc->frames_per_sample		= htons( 1 );
	avc->depth					= 0x18;
	avc->ctab_id				= htons( 0xFFFF );

	size += build_avcC( avc->data, media );

	build_box( &avc->box, size, "avc1" );

	return size;
}

static int
build_stsd( u8 *data, mp4_media_t *media )
{
	stsd_t *stsd = ( stsd_t * )data;
	int size = 0;

	stsd->version				= htonl( 0 );
	stsd->sample_entries_size	= htonl( 1 );
	size += 8;

	size += build_avc( stsd->data, media );

	size += BOX_SIZE;
	build_box( &stsd->box, size, "stsd" );

	return size;
}

static int
build_stts( u8 *data )
{
	stts_t *stts = ( stts_t * )data;
	int size = sizeof( stts_t );

	memset( stts, 0, sizeof( stts_t ) );

	build_box( &stts->box, size, "stts" );

	return size;
}

static int
build_stsc( u8 *data )
{
	stsc_t *stsc = ( stsc_t * )data;
	int size = sizeof( stsc_t );

	memset( stsc, 0, sizeof( stsc_t ) );

	build_box( &stsc->box, size, "stsc" );

	return size;
}

static int
build_stco( u8 *data )
{
	stco_t *stco = ( stco_t * )data;
	int size = sizeof( stco_t );

	memset( stco, 0, sizeof( stco_t ) );

	build_box( &stco->box, size, "stco" );

	return size;
}

static int
build_stsz( u8 *data )
{
	stsz_t *stsz = ( stsz_t * )data;
	int size = sizeof( stsz_t );

	memset( stsz, 0, sizeof( stsz_t ) );

	build_box( &stsz->box, size, "stsz" );

	return size;
}

static int
build_stbl( u8 *data, mp4_media_t *media )
{
	stbl_t *stbl = ( stbl_t * )data;
	int size = 0;

	size += build_stsd( &stbl->data[ size ], media );
	size += build_stts( &stbl->data[ size ] );
	size += build_stsc( &stbl->data[ size ] );
	size += build_stco( &stbl->data[ size ] );
	size += build_stsz( &stbl->data[ size ] );

	size += BOX_SIZE;
	build_box( &stbl->box, size, "stbl" );

	return size;
}

static int
build_vmhd( u8 *data )
{
	vmhd_t *vmhd = ( vmhd_t * )data;
	int size = sizeof( vmhd_t );

	memset( vmhd, 0, sizeof( vmhd_t ) );

	vmhd->version	= htonl( 0x00000001 );

	build_box( &vmhd->box, size, "vmhd" );

	return size;
}

static int
build_minf( u8 *data, mp4_media_t *media )
{
	minf_t *minf = ( minf_t * )data;
	int size = 0;

	size += build_vmhd( &minf->data[ size ] );
	size += build_dinf( &minf->data[ size ] );
	size += build_stbl( &minf->data[ size ], media );

	size += BOX_SIZE;
	build_box( &minf->box, size, "minf" );

	return size;
}

static int
build_mdia( u8 *data, mp4_media_t *media )
{
	mdia_t *mdia = ( mdia_t * )data;
	int size = 0;

	size += build_mdhd( &mdia->data[ size ], media );
	size += build_hdlr( &mdia->data[ size ] );
	size += build_minf( &mdia->data[ size ], media );

	size += BOX_SIZE;
	build_box( &mdia->box, size, "mdia" );

	return size;
}

static int
build_trak( u8 *data, mp4_media_t *media )
{
	trak_t *trak = ( trak_t * )data;
	int size = 0;

	size += build_tkhd( &trak->data[ size ], media );
	size += build_mdia( &trak->data[ size ], media );

	size += BOX_SIZE;
	build_box( &trak->box, size, "trak" );

	return size;
}

static int
build_moov( u8 *data, mp4_media_t *media )
{
	moov_t *moov = ( moov_t * )data;
	int size = 0;

	size += build_mvhd( &moov->data[ size ], media );
	size += build_trak( &moov->data[ size ], media );
	size += build_mvex( &moov->data[ size ] );

	size += BOX_SIZE;
	build_box( &moov->box, size, "moov" );

	return size;
}

static int
build_sidx1( u8 *data, mp4_media_t *media )
{
	sidx_entry_t *se = ( sidx_entry_t * )data;
	int size = 0, i;
	u8 nal_type;

	for ( i = 0; i < media->frame_cnt; i ++ ) {
		size += media->frame[ i ].size;
	}
	se->size		= htonl( 96 + 8 + size );		// moof + mdat
	se->duration	= htonl( media->duration );

	nal_type = media->frame[ 0 ].buff[ 4 ] & H264_NAL_TYPE_MASK;
	if ( nal_type == H264_NAL_TYPE_SPS || nal_type == H264_NAL_TYPE_I ) {
		se->sap			= htonl( 0x90000000 );
	} else {
		se->sap			= htonl( 0 );
	}

	return sizeof( sidx_entry_t );
}

static int
build_sidx( u8 *data, mp4_media_t *media )
{
	sidx_t *sidx = ( sidx_t * )data;
	int size = sizeof( sidx_t );

	memset( sidx, 0, size );

	sidx->subsegment_duration	= htonl( 0x00000001 );
	sidx->time_scale			= htonl( DEF_TIME_SCALE );
	sidx->pre_time				= htonl( media->time_scale );
	sidx->sidx_cnt				= htonl( 0x00000001 );

	size += build_sidx1( sidx->data, media );

	build_box( &sidx->box, size, "sidx" );

	return size;
}

static int
build_mfhd( u8 *data, mp4_media_t *media )
{
	mfhd_t *mfhd = ( mfhd_t * )data;
	int size = sizeof( mfhd_t );

	memset( mfhd, 0, size );

	mfhd->version	= htonl( 0 );
	mfhd->seq_num	= htonl( media->seq );

	build_box( &mfhd->box, size, "mfhd" );

	return size;
}

static int
build_tfhd( u8 *data, mp4_media_t *media )
{
	tfhd_t *tfhd = ( tfhd_t * )data;
	int size = sizeof( tfhd_t );
	u32	*duration = ( u32 * )tfhd->data;

	memset( tfhd, 0, size );

//	tfhd->version	= htonl( 0x00020000 );
	tfhd->version	= htonl( 0x00000008 );
	tfhd->track_id	= htonl( 0x00000001 );

	duration[ 0 ]	= htonl( media->timestamp );
	size += 4;

	build_box( &tfhd->box, size, "tfhd" );

	return size;
}

static int
build_tfdt( u8 *data, mp4_media_t *media )
{
	tfdt_t *tfdt = ( tfdt_t * )data;
	int size = sizeof( tfdt_t );

	memset( tfdt, 0, size );

	tfdt->version					= htonl( 0x00000001 );
	tfdt->base_media_decode_time	= htonl( media->time_scale );

	build_box( &tfdt->box, size, "tfdt" );

	return size;
}

typedef struct __attribute__ ( ( packed ) ) {
	u32		size;
	u32		flag;
	u32		time_offset;
} trun_sample_t;

static int
build_trun( u8 *data, mp4_media_t *media, u8 *moof )
{
	trun_t *trun = ( trun_t * )data;
	int size = sizeof( trun_t ), i;
	u32 timestamp = 0, total_timestamp = 0;
	u8 nal_type;

	memset( trun, 0, size );

	trun->version			= htonl( 0x00000E01 );
	trun->sample_cnt		= htonl( media->frame_cnt );

	trun_sample_t *ts = ( trun_sample_t * )trun->data;

#define set_ts( n, t ) {													\
	ts[ n ].size				= htonl( media->frame[ n ].size );			\
	nal_type = media->frame[ n ].buff[ 4 ] & H264_NAL_TYPE_MASK;			\
	if ( nal_type == H264_NAL_TYPE_SPS || nal_type == H264_NAL_TYPE_I ) {	\
		ts[ n ].flag			= htonl( SAMPLE_FLAG_DEPENDS_NO );			\
	} else {																\
		ts[ n ].flag			= htonl( ( SAMPLE_FLAG_IS_NON_SYNC | SAMPLE_FLAG_DEPENDS_YES ) );	\
	}																		\
	ts[ n ].time_offset			= htonl( t );								\
}

	if ( media->is_stream ) {
		timestamp = media->duration / ( media->frame_cnt );
		for ( i = 0; i < ( media->frame_cnt - 1 ); i ++ ) {
			set_ts( i, timestamp );
			total_timestamp += timestamp;
		}
	} else {
		timestamp	= ( media->frame[ 0 ].ptime.tv_sec - media->pre_time.tv_sec ) * 1000;
		timestamp	+= ( media->frame[ 0 ].ptime.tv_usec - media->pre_time.tv_usec ) / 1000;		// millisecond
		timestamp	= ( timestamp * DEF_TIME_SCALE ) / 1000;
		/*
		if ( timestamp > 1000 ) {
			struct tm local = { 0, };
			localtime_r( &media->pre_time.tv_sec, &local );
			dbg_line( "%04d/%02d/%02d %02d:%02d:%02d",
								local.tm_year + 1900, local.tm_mon + 1, local.tm_mday,
								local.tm_hour, local.tm_min, local.tm_sec );
			dbg_error( "media->pre_time: %lu.%06lu", media->pre_time.tv_sec, media->pre_time.tv_usec );
			localtime_r( &media->frame[ 0 ].ptime.tv_sec, &local );
			dbg_line( "%04d/%02d/%02d %02d:%02d:%02d",
								local.tm_year + 1900, local.tm_mon + 1, local.tm_mday,
								local.tm_hour, local.tm_min, local.tm_sec );
			dbg_error( "media->frame[ 0 ].ptime: %lu.%06lu", media->frame[ 0 ].ptime.tv_sec, media->frame[ 0 ].ptime.tv_usec );
		}
		*/
		total_timestamp = timestamp;

		set_ts( 0, timestamp );

		for ( i = 1; i < ( media->frame_cnt - 1 ); i ++ ) {
			set_ts( i, media->timestamp );
			total_timestamp += media->timestamp;
		}
	}

	//dbg_line( "media->duration = %u, total_timestamp = %u", media->duration, total_timestamp );
	set_ts( ( media->frame_cnt - 1 ), ( media->duration - total_timestamp ) );

	media->pre_time		= media->frame[ ( media->frame_cnt - 1 ) ].ptime;
	media->time_scale	+= media->duration;

	size += sizeof( trun_sample_t ) * media->frame_cnt;

	trun->data_offset	= htonl( ( data - moof ) + size + 8 );			// moof + mdat

	build_box( &trun->box, size, "trun" );

	return size;
}

static int
build_traf( u8 *data, mp4_media_t *media, u8 *moof )
{
	traf_t *traf = ( traf_t * )data;
	int size = 0;

	size += build_tfhd( &traf->data[ size ], media );
	size += build_tfdt( &traf->data[ size ], media );
	size += build_trun( &traf->data[ size ], media, moof );

	size += BOX_SIZE;
	build_box( &traf->box, size, "traf" );

	return size;
}

static int
build_moof( u8 *data, mp4_media_t *media )
{
	moof_t *moof = ( moof_t * )data;
	int size = 0;

	size += build_mfhd( &moof->data[ size ], media );
	size += build_traf( &moof->data[ size ], media, data );

	size += BOX_SIZE;
	build_box( &moof->box, size, "moof" );

	return size;
}

static void
mov_parser_parameter_set( mp4_media_t *media, av_frame_t *frame )
{
	struct iovec	sps, pps;
	struct h264_info	h264i;

	memset( &h264i, 0, sizeof( struct h264_info ) );

	h264_get_sps( frame->buff, frame->size, &sps );
	h264_get_pps( frame->buff, frame->size, &pps );

	if ( sps.iov_base && pps.iov_base ) {
		u8 *sps_start = ( u8 * )sps.iov_base - 4;

		memcpy( media->sps, sps.iov_base, sps.iov_len );
		memcpy( media->pps, pps.iov_base, pps.iov_len );

		media->sps_len = sps.iov_len;
		media->pps_len = pps.iov_len;

		if ( h264_parser_config( sps_start, media->sps_len + 4 , &h264i ) < 0 ) {
			media->w = 1920;
			media->h = 1080;
		} else {
			media->w = h264i.w;
			media->h = h264i.h;
		}
	}
}

static int
mov_put_nal( u8 *dst, u8 *src, u32 size )
{
	if ( GET_32( src ) != H264_START_CODE ) {
		PUT_32( dst, size );
		dst += 4;
		memcpy( dst, src, size );
		return ( size + 4 );
	} else {
		PUT_32( dst, ( size - 4 ) );
		dst += 4;
		memcpy( dst, &src[ 4 ], ( size - 4 ) );
		return size;
	}
}

//==========================================================================
// APIs
//==========================================================================
int
MP4_ResetFramePos( mp4_media_t *media, u8 *org_pos, u8 *new_pos )
{
	if ( media && org_pos && new_pos ) {
		int i;

		for ( i = 0; i < media->frame_cnt; i ++ ) {
			media->frame[ i ].buff = new_pos + ( media->frame[ i ].buff - org_pos );
		}

		return SUCCESS;
	} else {
		return FAILURE;
	}
}

u32
MP4_PutFrame( mp4_media_t *media, av_frame_t *frame, u8 *dst )
{
	if ( media ) {
		u8 *start	= dst;

		if ( media->frame_cnt > MP4_FRAME_MAX ) {
			dbg_line( "media->frame_cnt > MP4_FRAME_MAX( %d )!!!", MP4_FRAME_MAX );
			return 0;
		} else if ( ( frame->buff[ 4 ] & H264_NAL_TYPE_MASK ) == H264_NAL_TYPE_SPS ) {
			int offset;
			mov_parser_parameter_set( media, frame );

			dst += mov_put_nal( dst, media->sps, media->sps_len );
			dst += mov_put_nal( dst, media->pps, media->pps_len );

			offset = dst - start;
			dst += mov_put_nal( dst, &frame->buff[ offset ], ( frame->size - offset ) );
		} else {
			dst += mov_put_nal( dst, frame->buff, frame->size );
		}

		media->frame[ media->frame_cnt ].ptime	= frame->ptime;
		media->frame[ media->frame_cnt ].size	= frame->size;
		media->frame[ media->frame_cnt ].buff	= start;

		media->media_size += frame->size;
		media->frame_cnt ++;

		return frame->size;
	} else {
		return 0;
	}
}

int
MP4_BuildFileHeader( mp4_media_t *media )
{
	if ( media ) {
		u8 *header = media->header;

		media->file_duration	= ( ( media->pre_time.tv_sec - media->start_time.tv_sec ) * DEF_TIME_SCALE ) +
								( ( media->pre_time.tv_usec * DEF_TIME_SCALE ) / 1000000 );

		media->header_size = 0;
		media->header_size += build_ftyp( &header[ media->header_size ] );
		media->header_size += build_moov( &header[ media->header_size ], media );

		return media->header_size;
	} else {
		return FAILURE;
	}
}

int
MP4_BuildMediaHeader( mp4_media_t *media )
{
	if ( media ) {
		mdat_t	*mdat;
		u8		*header = media->header;

		media->seq ++;

		if ( media->frame_cnt > 1 ) {
			u32 nal_type = media->frame[ 0 ].buff[ 4 ] & H264_NAL_TYPE_MASK;

			media->duration	= ( media->frame[ ( media->frame_cnt - 1 ) ].ptime.tv_sec - media->frame[ 0 ].ptime.tv_sec ) * 1000;
			media->duration	+= ( media->frame[ ( media->frame_cnt - 1 ) ].ptime.tv_usec - media->frame[ 0 ].ptime.tv_usec ) / 1000;		// millisecond

			media->duration	= ( media->duration * DEF_TIME_SCALE ) / 1000;

			media->timestamp	= media->duration / ( media->frame_cnt - 1 );

			if ( media->is_stream && ( nal_type == H264_NAL_TYPE_SPS || nal_type == H264_NAL_TYPE_I ) ) {
				media->duration	+= media->timestamp;
			} else {
				u32 timestamp;

				timestamp	= ( media->frame[ 0 ].ptime.tv_sec - media->pre_time.tv_sec ) * 1000;
				timestamp	+= ( media->frame[ 0 ].ptime.tv_usec - media->pre_time.tv_usec ) / 1000;		// millisecond
				timestamp	= ( timestamp * DEF_TIME_SCALE ) / 1000;

				media->duration	+= timestamp;
			}
		} else {
			media->duration	= DEF_TIME_SCALE;
			media->duration	= DEF_TIME_SCALE;
		}

		media->header_size = 0;
		//media->header_size = build_sidx( header, media );
		media->header_size += build_moof( &header[ media->header_size ], media );

		mdat = ( mdat_t * )&header[ media->header_size ];
		build_box( &mdat->box, sizeof( mdat_t ) + media->media_size, "mdat" );
		media->header_size += sizeof( mdat_t );

		return media->header_size;
	} else {
		return FAILURE;
	}
}

