/*!
 *  @file       mp4.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

#ifndef __MP4_H__
#define __MP4_H__

//==========================================================================
// Include Files
//==========================================================================
#include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
#define HEADER_MAX 			8 * 1024
#define MP4_FRAME_MAX 		180

typedef struct {
	u32					time_scale;

	u8					header[ HEADER_MAX ];
	u32					header_size;
	u32					file_duration;
	u32					duration;
	u32					timestamp;

    struct timeval		start_time;
    struct timeval		pre_time;
	int					seq;

	u8					sps[ 128 ];
	u8					pps[ 128 ];
	int					sps_len;
	int					pps_len;
	int					w;
	int					h;

	i16					unchecked_box;
	i16					is_stream;
	int					frame_cnt;
	int					media_size;
	struct {
		u8				*buff;
		u32				size;
		struct timeval	ptime;
		char			is_key;
	} frame[ MP4_FRAME_MAX ];

	int					( *read )			( void *, void *, u32 );
	void				*file_io;
} mp4_media_t;

//==========================================================================
// APIs
//==========================================================================
extern int
MP4_ResetFramePos( mp4_media_t *media, u8 *org_pos, u8 *new_pos );

extern u32
MP4_PutFrame( mp4_media_t *media, av_frame_t *frame, u8 *dst );

extern int
MP4_BuildFileHeader( mp4_media_t *media );

extern int
MP4_BuildMediaHeader( mp4_media_t *media );

extern int
MP4_ReadFileHeader( mp4_media_t *media );

extern int
MP4_ReadMediaHeader( mp4_media_t *media );

#endif // __MP4_H__

