/*!
 *  @file       mp4box.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

#ifndef __MP4BOX_H__
#define __MP4BOX_H__

//==========================================================================
// Include Files
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
typedef struct __attribute__ ( ( packed ) ) {
	u32		size;
	u8		code[ 4 ];
} box_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u8		data[];
} box_grp_t;

# define BOX_SIZE	8

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u8		major_brand[ 4 ];
	u32		minor_version;
	u8		compatible_brands[ 4 * 3 ];
} ftyp_t;

# define	wide_t		box_grp_t

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		creation_time;
	u32		modification_time;
	u32		timescale;
	u32		duration;
	u32		rate;
	u16		volume;
	u8		resv[ 10 ];
	u32		matrix[ 9 ];

	u32		preview_time;
	u32		preview_duration;
	u32		poster_time;
	u32		selection_time;
	u32		selection_duration;
	u32		currect_time;

	u32		next_track_ID;
} mvhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		freg_duration;
} mehd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		track_id;
	u32		sample_desc_index;
	u32		sample_duration;
	u32		sample_size;
	u32		sample_flags;
} trex_t;

# define	mvex_t		box_grp_t

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		creation_time;
	u32		modification_time;
	u32		track_ID;
	u32		resv1;
	u32		duration;
	u32		resv2[ 2 ];

	u16		layer;
	u16		alternate_group;

	u16		volume;
	u16		resv3;

	u32		matrix[ 9 ];
	u32		width;
	u32		height;
} tkhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		creation_time;
	u32		modification_time;
	u32		timescale;
	u32		duration;
	u16		language;
	u16		quality;
} mdhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		pre_defined;
	u32		handler_type;	/* "vide" "soun" "hint" "odsm" "crsm" "sdsm" "m7sm" "ocsm" "ipsm" "mjsm" */
	u32		resv[ 3 ];
	char	name[];
} hdlr_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
} url_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		entry_count;
	u8		data[];
} dref_t;

# define dinf_t		box_grp_t

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u8		version;
	u8		sps[ 3 ];
	u8		nula_size;
	u8		sps_cnt;

	u8		data[];
} avcC_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u8		resv[ 6 ];
	u16		data_reference;
	u16		version;
	u16		revision;
	u32		vendor;
	u32		temporal_quality;
	u32		spatial_quality;
	u16		width;
	u16		height;
	u32		dpi_horizontal;
	u32		dpi_vertical;
	u32		data_size;
	u16		frames_per_sample;
	u8		compressor_name[ 33 ];
	u8		depth;
	u16		ctab_id;

	u8		data[];
} avc_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u8		tag;
	u8		tag_size;
	u16		track_id;
	u8		flag;

	u8		dec_config_tag;					// 04 (tag)
	u8		dec_config_tag_size;			// 11 (tag size)
	u8		dec_config_type;				// 40 (MPEG-4 audio)
	u8		dec_config_straem;				// 15 (audio stream)
	u8		dec_config_buff_size[ 3 ];		// 00 06 00 (Buffersize DB)
	u32		dec_config_max_bitrate;			// 00 01 F4 00 (max bitrate 128000)
	u32		dec_config_evg_bitrate;			// 00 01 F4 00 (avg bitrate 128000)

	u8		dec_info_tag;					// 05 (tag)
	u8		dec_info_tag_size;				// 02 (tag size)
	u16		dec_info_spec_config;			// 12 10 (Audio Specific Config)

	u8		sl_tag;							// 06 (tag)
	u8		sl_tag_size;					// 01 (tag size)
	u8		end;							// 02 (always 2 refer from mov_write_esds_tag);
} esds_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;

	u8		reserved_format[ 6 ];
	u16		data_reference_index;
	u16		version;
	u8		reserved_format2[ 6 ];
	u16		channelcount;
	u16		samplesize;
	u16		compression_id;
	u16		packet_size;
	u16		samplerate;
	u8		reserved_format3[ 2 ];

	u8		data[];
} mp4a_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		sample_entries_size;
	u8		data[];
} stsd_t;

typedef struct __attribute__ ( ( packed ) ) {
	u32		sample_count;
	u32		sample_delta;
} stts_entry_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		time_to_sample_size;
	stts_entry_t	sample[ 0 ];
} stts_t;

typedef struct __attribute__ ( ( packed ) ) {
	u32		sample_number;
} stss_entry_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		entry_size;
	stss_entry_t	entry[ 0 ];
} stss_t;

typedef struct __attribute__ ( ( packed ) ) {
	u32		first_chunk;
	u32		samples;
	u32		sample_description_id;
} stsc_entry_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t			box;
	u32				version;
	u32				sample_to_chunk_size;
	stsc_entry_t	chunk[ 0 ];
} stsc_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		chunk_offset_size;
	u32		data[ 0 ];
} stco_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
    u32		sample_size;
    u32		sample_count;
	u32		frame_size[ 0 ];
} stsz_t;

# define stbl_t		box_grp_t

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
    u16		graphics_mode;
    u16		opcolor[ 3 ];
} vmhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
    u16		balance;
    u16		paused;
} smhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	u32				seq_duration;
    u32				media_time;
    u16				media_rate_integer;
    u16				media_rate_fraction;
} elst_entry_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t			box;
	u32				version;
    u32				entry_cnt;
    elst_entry_t	entry[];
} elst_t;

# define minf_t		box_grp_t
# define edts_t		box_grp_t
# define mdia_t		box_grp_t
# define trak_t		box_grp_t
# define moov_t		box_grp_t

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
} mdat_t;

typedef struct __attribute__ ( ( packed ) ) {
	u32		size;
	u32		duration;
	u32		sap;
} sidx_entry_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;

	u32		reference;
	u32		subsegment_duration;
	u32		time_scale;
	u32		pre_time;
	u32		first_offset;
	u32		sidx_cnt;

	u8		data[];
} sidx_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		seq_num;
} mfhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		track_id;
	u8		data[];
} tfhd_t;

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		base_media_decode_time;
} tfdt_t;

# define DATA_OFFSET_PRESENT							0x000001
# define FIRST_SAMPLE_FALGS_PRESENT						0x000004
# define SAMPLE_DURATION_PRESENT						0x000100
# define SAMPLE_SIZE_PRESENT							0x000200
# define SAMPLE_FLAGS_PRESENT							0x000400
# define SAMPLE_COMPOSITION_TIME_OFFSETS_PRESENT		0x000800  // cts

# define SAMPLE_FLAG_IS_NON_SYNC						0x00010000
# define SAMPLE_FLAG_DEPENDS_YES						0x01000000
# define SAMPLE_FLAG_DEPENDS_NO							0x02000000

typedef struct __attribute__ ( ( packed ) ) {
	box_t	box;
	u32		version;
	u32		sample_cnt;
	u32		data_offset;
	u8		data[];
} trun_t;

#define	traf_t		box_grp_t

#define	moof_t		box_grp_t


enum {
	MP4_AAC_TYPE_MAIN	= 1,
	MP4_AAC_TYPE_LC,			// Low Complexity
	MP4_AAC_TYPE_SSR,			// Scalable Sample Rate
};

enum {
	MP4_AAC_SAMPLE_RATE_96000HZ	= 0,
	MP4_AAC_SAMPLE_RATE_88200HZ,
	MP4_AAC_SAMPLE_RATE_64000HZ,
	MP4_AAC_SAMPLE_RATE_48000HZ,
	MP4_AAC_SAMPLE_RATE_44100HZ,
	MP4_AAC_SAMPLE_RATE_32000HZ,
	MP4_AAC_SAMPLE_RATE_24000HZ,
	MP4_AAC_SAMPLE_RATE_22050HZ,
	MP4_AAC_SAMPLE_RATE_16000HZ,
	MP4_AAC_SAMPLE_RATE_12000HZ,
	MP4_AAC_SAMPLE_RATE_11025HZ,
	MP4_AAC_SAMPLE_RATE_8000HZ,
	MP4_AAC_SAMPLE_RATE_7350HZ,
};

enum {
	MP4_AAC_CHANNEL_DEFINED_IN_AOT	= 0,
	MP4_AAC_CHANNEL_1,
	MP4_AAC_CHANNEL_2,
	MP4_AAC_CHANNEL_3,
};

enum {
	MP4_AAC_PACKET_1024	= 0,
	MP4_AAC_PACKET_960,
};

#endif // __MP4BOX_H__



