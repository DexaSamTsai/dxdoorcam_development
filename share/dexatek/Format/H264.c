/*!
 *  @file       H264.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
#include "H264.h"

//==========================================================================
#define DBG_H264  	0

#if DBG_H264
    #define dbg_h264      		dbg
#else
    #define dbg_h264(...)       // ...
#endif

typedef struct {
	u8		*src;
	int		offset;
	int		error;
	int		max;
} parse_attr_t;

//==========================================================================
// Type Define
//==========================================================================
/*
static u8		ph_i_s[2] = {0x7c, 0x85};
static u8		ph_i_m[2] = {0x7c, 0x05};
static u8		ph_i_e[2] = {0x7c, 0x45};

static u8		ph_p_s[2] = {0x5c, 0x81};
static u8		ph_p_m[2] = {0x5c, 0x01};
static u8		ph_p_e[2] = {0x5c, 0x41};
*/

//==========================================================================
// Static Functions
//==========================================================================
static int
h264_find_set( u8 *src, int size, u8 key )
{
	int i = 0;

	while ( ( i + 4 ) < size ) {
		if ( ( src[ i + 4 ] & 0xE0 ) &&
			 ( ( src[ i + 4 ] & 0x0F ) == key ) &&
			 ( src[ i + 3 ] == 0x01 ) &&
			 ( src[ i + 2 ] == 0x00 ) &&
			 ( src[ i + 1 ] == 0x00 ) &&
			 ( src[ i ] == 0x00 ) ) {
			i += 4;
			return i;
		}
		i ++;
	}

	return -1;
}

static int
h264_get_set( u8 *src, int size, struct iovec *set, u8 key )
{
	int i = 0;

	set[ 0 ].iov_base	= NULL;
	set[ 0 ].iov_len 	= 0;

	while ( ( i + 4 ) < size ) {
		if ( ( src[ i + 4 ] & 0xE0 ) &&
			 ( ( src[ i + 4 ] & 0x0F ) == key ) &&
			 ( src[ i + 3 ] == 0x01 ) &&
			 ( src[ i + 2 ] == 0x00 ) &&
			 ( src[ i + 1 ] == 0x00 ) &&
			 ( src[ i ] == 0x00 ) ) {
			i += 4;
			set[ 0 ].iov_base = src + i;

			while ( ( i + 3 ) < size ) {
				if ( ( src[ i + 4 ] & 0xE0 ) &&
					 ( src[ i + 3 ] == 0x01 ) &&
					 ( src[ i + 2 ] == 0x00 ) &&
					 ( src[ i + 1 ] == 0x00 ) &&
					 ( src[ i ] == 0x00 ) ) {
					return SUCCESS;
				}

				set[ 0 ].iov_len ++;
				i ++;
			}
			return SUCCESS;
		}
		i ++;
	}

	return FAILURE;
}

static int
h264_get_bit( parse_attr_t *pos, int cnt )
{
	if ( pos->error ) {
		return 0;
	} else {
	    u32 bytes	= pos->offset / 8;
	    u32 bits	= pos->offset % 8;
	    u32 value	= 0;

	    pos->offset += cnt;

		if ( pos->offset + cnt >= pos->max ) {
			pos->error = 1;
			return 0;
	    }

		while ( cnt ) {
	    	value <<= 1;
	    	if ( pos->src[ bytes ] & ( 0x00000001 << ( 7 - bits ) ) ) {
	    		value += 1;
	    	}

	    	bits ++;

	        if ( bits >= 8 ) {
				bytes ++;
				bits = 0;
			}
	        cnt --;
	    }

	    return value;
	}
}

//  Read an unsigned value from the bitstream
static u32
uev( parse_attr_t *pos )
{
	int leading_zero_bits = 0;
	int b;
	u32 code_value;

	for ( b = 0; !b; leading_zero_bits ++ ) {
		b = h264_get_bit( pos, 1 );
		if ( pos->error ) {
			return 0;
		}
	}

	// Leading Zeros does not include the last one that you read
	//code_value = h264_get_bit( pos, (leading_zero_bits + 1));

	leading_zero_bits -= 1;
	code_value = ( 1 << leading_zero_bits ) - 1 + h264_get_bit( pos, leading_zero_bits );
	return code_value;
}

//  Read a signed value from the bitstream
static int
sev( parse_attr_t *pos )
{
	int code_value, ceil_code_value, sign;
	//  Retrieve an unsigned value, and then interpret the high bit as the sign
	code_value		= uev( pos );
	ceil_code_value = ( code_value + 1 ) >> 1;
	sign			= ( code_value % 2 ) ? 1 : -1;

	return ( ceil_code_value * sign );
}

//==========================================================================
// APIs
//==========================================================================
int
h264_get_sps( u8 *src, int size, struct iovec *sps )
{
	if ( src && sps ) {
		return h264_get_set( src, size, sps, 0x07 );
	} else {
		return FAILURE;
	}
}

int
h264_get_pps( u8 *src, int size, struct iovec *pps )
{
	if ( src && pps ) {
		return h264_get_set( src, size, pps, 0x08 );
	} else {
		return FAILURE;
	}
}

int
h264_find_i( u8 *src, int size )
{
	if ( src ) {
		return h264_find_set( src, size, 0x05 );
	} else {
		return FAILURE;
	}
}

int
h264_find_p( u8 *src, int size )
{
	if ( src ) {
		return h264_find_set( src, size, 0x01 );
	} else {
		return FAILURE;
	}
}

static void
SPSDecodeScalingList( parse_attr_t *pos, int size)
{
	int i, last = 8, next = 8;
	int matrix = h264_get_bit( pos, 1 );

	if ( matrix ) {
		for ( i = 0; i < size; i++ ) {
			if ( next ) {
				next = ( last + sev( pos ) ) & 0xff;
			}

			if( !i && !next ) { /* matrix not written */
				break;
			}
			last = next ? next : last;
		}
	}
}

int
h264_parser_config( u8 *config, int size, struct h264_info *h264i )
{
	int i;

	parse_attr_t	pos;

	if ( ( config == NULL ) || ( h264i == NULL ) ) {
		return FAILURE;
	} else if ( ( i = h264_find_set( config, size, 0x07 ) ) < 0 ) {
		return FAILURE;
	} else if ( i + 1 != 5 ) {
		return FAILURE;
	}

	pos.src		= &config[ i + 1 ];
	pos.offset	= 0;
	pos.error	= 0;
	pos.max		= size * 8;

	h264i->profile_idc = h264_get_bit( &pos, 8 );		//profile_idc= get_bits(&s->gb, 8);
	dbg_h264( "profile_idc = 0x%x", h264i->profile_idc );

	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set0_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set1_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set2_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set3_flag
	h264_get_bit( &pos, 4 );		// get_bits(&s->gb, 4); // reserved

	h264i->level_idc = h264_get_bit( &pos, 8 );	// level_idc= get_bits(&s->gb, 8);
	dbg_h264( "level_idc = %d", h264i->level_idc );

	h264i->sps_id = uev( &pos );		// sps_id= get_ue_golomb(&s->gb);
	dbg_h264( "sps_id = %d", h264i->sps_id );

	if ( h264i->profile_idc >= 100 ) {		//high profile
		int chroma = uev( &pos );
		if ( chroma == 3 )		//chroma_format_idc
			h264_get_bit( &pos, 1 );  		//get_bits1(&s->gb);  //residual_color_transform_flag

		uev( &pos );  					//get_ue_golomb(&s->gb);  //bit_depth_luma_minus8
		uev( &pos );  					//get_ue_golomb(&s->gb);  //bit_depth_chroma_minus8
		h264_get_bit( &pos, 1 );			//sps->transform_bypass = get_bits1(&s->gb);

		// decode scaling matrices
		int scaling = h264_get_bit( &pos, 1 );
		if ( scaling ) {
			// Decode scaling lists
			SPSDecodeScalingList( &pos, 16 ); // Intra, Y
			SPSDecodeScalingList( &pos, 16 ); // Intra, Cr
			SPSDecodeScalingList( &pos, 16 ); // Intra, Cb
			SPSDecodeScalingList( &pos, 16 ); // Inter, Y
			SPSDecodeScalingList( &pos, 16 ); // Inter, Cr
			SPSDecodeScalingList( &pos, 16 ); // Inter, Cb

			SPSDecodeScalingList( &pos, 64 ); // Intra, Y
			if ( chroma == 3 ) {
				SPSDecodeScalingList( &pos, 64 ); // Intra, Cr
				SPSDecodeScalingList( &pos, 64 ); // Intra, Cb
			}
			SPSDecodeScalingList( &pos, 64 ); // Inter, Y
			if ( chroma == 3 ) {
				SPSDecodeScalingList( &pos, 64 ); // Inter, Cr
				SPSDecodeScalingList( &pos, 64 ); // Inter, Cb
			}
		}
	}

	h264i->max_frame_num = uev( &pos ) + 4;	// sps->log2_max_frame_num= get_ue_golomb(&s->gb) + 4;
	dbg_h264( "max_frame_num = %d", h264i->max_frame_num );

	h264i->poc_type = uev( &pos );		// sps->poc_type= get_ue_golomb(&s->gb);
	dbg_h264( "poc_type = %d", h264i->poc_type );

	if ( h264i->poc_type == 0 ) { //FIXME #define
		uev( &pos );	//sps->log2_max_poc_lsb= get_ue_golomb(&s->gb) + 4;
	} else if ( h264i->poc_type == 1 ) {	//FIXME #define
		h264i->delta_pic_order_always_zero_flag	= h264_get_bit( &pos, 1 );		// sps->delta_pic_order_always_zero_flag= get_bits1(&s->gb);
		h264i->offset_for_non_ref_pic			= sev( &pos );				// sps->offset_for_non_ref_pic= get_se_golomb(&s->gb);
		h264i->offset_for_top_to_bottom_field	= sev( &pos );				// sps->offset_for_top_to_bottom_field= get_se_golomb(&s->gb);
		h264i->poc_cycle_length					= uev( &pos );				//tmp= get_ue_golomb(&s->gb);

		dbg_h264( "poc_cycle_length = %d", h264i->poc_cycle_length );
		for ( i = 0; i < h264i->poc_cycle_length; i ++ ) {
			sev( &pos );		// offset_for_ref_frame
		}
	}

	h264i->ref_frame_count = uev( &pos );		// tmp= get_ue_golomb(&s->gb);
	dbg_h264( "ref_frame_count = %d", h264i->ref_frame_count );
	if ( h264i->ref_frame_count >= 32 ) {
		dbg_h264( "too many reference frames" );
		return -1;
	}

	h264i->gaps_in_frame_num_allowed_flag = h264_get_bit( &pos, 1 );		// sps->gaps_in_frame_num_allowed_flag= get_bits1(&s->gb);
	dbg_h264( "gaps_in_frame_num_allowed_flag = %d, pos = %d", h264i->gaps_in_frame_num_allowed_flag, pos );

	h264i->w		= ( uev( &pos ) + 1 ) * 16;		// mb_width= get_ue_golomb(&s->gb) + 1;
	h264i->h		= ( uev( &pos ) + 1 ) * 16;		// mb_height= get_ue_golomb(&s->gb) + 1;
	dbg_h264( "mb_width = %d, mb_height = %d, pos = %d", h264i->w, h264i->h, pos );

    return pos.error;
}

int
H264_GetResolution( u8 *config, int size, int *w, int *h )
{
	int i;
	struct h264_info	h264i;

	parse_attr_t	pos;

	if ( ( config == NULL ) || ( w == NULL ) || ( h == NULL ) ) {
		return FAILURE;
	} else if ( ( i = h264_find_set( config, size, 0x07 ) ) < 0 ) {
		return FAILURE;
	} else if ( i + 1 != 5 ) {
		return FAILURE;
	}

	pos.src		= &config[ i + 1 ];
	pos.offset	= 0;
	pos.error	= 0;
	pos.max		= size * 8;

	h264i.profile_idc = h264_get_bit( &pos, 8 );		//profile_idc= get_bits(&s->gb, 8);
	dbg_h264( "profile_idc = 0x%x", h264i.profile_idc );

	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set0_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set1_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set2_flag
	h264_get_bit( &pos, 1 );		// get_bits1(&s->gb);   //constraint_set3_flag
	h264_get_bit( &pos, 4 );		// get_bits(&s->gb, 4); // reserved

	h264i.level_idc = h264_get_bit( &pos, 8 );	// level_idc= get_bits(&s->gb, 8);
	dbg_h264( "level_idc = %d", h264i.level_idc );

	h264i.sps_id = uev( &pos );		// sps_id= get_ue_golomb(&s->gb);
	dbg_h264( "sps_id = %d", h264i.sps_id );

	if ( h264i.profile_idc >= 100 ) {		//high profile
		int chroma = uev( &pos );
		if ( chroma == 3 )		//chroma_format_idc
			h264_get_bit( &pos, 1 );  		//get_bits1(&s->gb);  //residual_color_transform_flag

		uev( &pos );  					//get_ue_golomb(&s->gb);  //bit_depth_luma_minus8
		uev( &pos );  					//get_ue_golomb(&s->gb);  //bit_depth_chroma_minus8
		h264_get_bit( &pos, 1 );			//sps->transform_bypass = get_bits1(&s->gb);

		// decode scaling matrices
		int scaling = h264_get_bit( &pos, 1 );
		if ( scaling ) {
			// Decode scaling lists
			SPSDecodeScalingList( &pos, 16 ); // Intra, Y
			SPSDecodeScalingList( &pos, 16 ); // Intra, Cr
			SPSDecodeScalingList( &pos, 16 ); // Intra, Cb
			SPSDecodeScalingList( &pos, 16 ); // Inter, Y
			SPSDecodeScalingList( &pos, 16 ); // Inter, Cr
			SPSDecodeScalingList( &pos, 16 ); // Inter, Cb

			SPSDecodeScalingList( &pos, 64 ); // Intra, Y
			if ( chroma == 3 ) {
				SPSDecodeScalingList( &pos, 64 ); // Intra, Cr
				SPSDecodeScalingList( &pos, 64 ); // Intra, Cb
			}
			SPSDecodeScalingList( &pos, 64 ); // Inter, Y
			if ( chroma == 3 ) {
				SPSDecodeScalingList( &pos, 64 ); // Inter, Cr
				SPSDecodeScalingList( &pos, 64 ); // Inter, Cb
			}
		}
	}

	h264i.max_frame_num = uev( &pos ) + 4;	// sps->log2_max_frame_num= get_ue_golomb(&s->gb) + 4;
	dbg_h264( "max_frame_num = %d", h264i.max_frame_num );

	h264i.poc_type = uev( &pos );		// sps->poc_type= get_ue_golomb(&s->gb);
	dbg_h264( "poc_type = %d", h264i.poc_type );

	if ( h264i.poc_type == 0 ) { //FIXME #define
		uev( &pos );	//sps->log2_max_poc_lsb= get_ue_golomb(&s->gb) + 4;
	} else if ( h264i.poc_type == 1 ) {	//FIXME #define
		h264i.delta_pic_order_always_zero_flag	= h264_get_bit( &pos, 1 );		// sps->delta_pic_order_always_zero_flag= get_bits1(&s->gb);
		h264i.offset_for_non_ref_pic			= sev( &pos );				// sps->offset_for_non_ref_pic= get_se_golomb(&s->gb);
		h264i.offset_for_top_to_bottom_field	= sev( &pos );				// sps->offset_for_top_to_bottom_field= get_se_golomb(&s->gb);
		h264i.poc_cycle_length					= uev( &pos );				//tmp= get_ue_golomb(&s->gb);

		dbg_h264( "poc_cycle_length = %d", h264i.poc_cycle_length );
		for ( i = 0; i < h264i.poc_cycle_length; i ++ ) {
			sev( &pos );		// offset_for_ref_frame
		}
	}

	h264i.ref_frame_count = uev( &pos );		// tmp= get_ue_golomb(&s->gb);
	dbg_h264( "ref_frame_count = %d", h264i.ref_frame_count );
	if ( h264i.ref_frame_count >= 32 ) {
		dbg_h264( "too many reference frames" );
		return -1;
	}

	h264i.gaps_in_frame_num_allowed_flag = h264_get_bit( &pos, 1 );		// sps->gaps_in_frame_num_allowed_flag= get_bits1(&s->gb);
	dbg_h264( "gaps_in_frame_num_allowed_flag = %d, pos = %d", h264i.gaps_in_frame_num_allowed_flag, pos );

	h264i.w		= ( uev( &pos ) + 1 ) * 16;		// mb_width= get_ue_golomb(&s->gb) + 1;
	h264i.h		= ( uev( &pos ) + 1 ) * 16;		// mb_height= get_ue_golomb(&s->gb) + 1;
	dbg_h264( "mb_width = %d, mb_height = %d, pos = %d", h264i.w, h264i.h, pos );

	w[ 0 ]	= h264i.w;
	h[ 0 ]	= h264i.h;

    return pos.error;
}
