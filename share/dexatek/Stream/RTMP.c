/*
 * RTMP.c
 *
 *		Created on:	2017/07/24
 *		Version:	2.0
 *      Author:		Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "RTMP.h"
# include "Utils/Malloc.h"

//==========================================================================
// Type Declaration
//==========================================================================
# define    ENABLE_AUDIO
# define    ENABLE_MULTI_STREAM_CHANNEL
//# define	ENABLE_SOCKET_NONBLOCK_MODE

# define	CHUNK_SIZE_DEFAULT			128
# define	CHUNK_SIZE_MAX				0x1000
# define	BUFFER_LIMIT_SIZE			( 1024 * 1000 )
# define	RTMP_HEADER_MAX				12

# define	RTMP_FMT0					0
# define	RTMP_FMT1					1
# define	RTMP_FMT2					2
# define	RTMP_FMT3					3

# define	RTMP_CHANNEL_DATA			2
# define	RTMP_CHANNEL_INVOKE			3
# define	RTMP_CHANNEL_STREAM			4
# define	RTMP_CHANNEL_AUDIO			6
# define	RTMP_CHANNEL_VIDEO			7


# define	RTMP_TYPE_SET_PACKET_SIZE	0x01
# define	RTMP_TYPE_USER_CONTROL		0x04
# define	RTMP_TYPE_WINDOW_ACK_SIZE	0x05
# define	RTMP_TYPE_PEER_BANDWIDTH	0x06
# define	RTMP_TYPE_AUDIO				0x08
# define	RTMP_TYPE_VIDEO				0x09
# define	RTMP_TYPE_AMF3_INVOKE		0x11
# define	RTMP_TYPE_METADATA			0x12
# define	RTMP_TYPE_AMF0_INVOKE		0x14

# define	RTMP_AMF0_NUMBER			0x00
# define	RTMP_AMF0_BOOLEAN			0x01
# define	RTMP_AMF0_STRING			0x02
# define	RTMP_AMF0_OBJECT			0x03
# define	RTMP_AMF0_NULL				0x05
# define	RTMP_AMF0_ARRAY				0x08

enum {
	RTMP_AAC_TYPE_MAIN	= 0,
	RTMP_AAC_TYPE_1C,
	RTMP_AAC_TYPE_SSR,
	RTMP_AAC_TYPE_LTP,
};

enum {
	RTMP_AAC_SAMPLE_RATE_96000HZ	=	0,
	RTMP_AAC_SAMPLE_RATE_88200HZ,
	RTMP_AAC_SAMPLE_RATE_64000HZ,
	RTMP_AAC_SAMPLE_RATE_48000HZ,
	RTMP_AAC_SAMPLE_RATE_44100HZ,
	RTMP_AAC_SAMPLE_RATE_32000HZ,
	RTMP_AAC_SAMPLE_RATE_24000HZ,
	RTMP_AAC_SAMPLE_RATE_22050HZ,
	RTMP_AAC_SAMPLE_RATE_16000HZ,
	RTMP_AAC_SAMPLE_RATE_12000HZ,
	RTMP_AAC_SAMPLE_RATE_11025HZ,
	RTMP_AAC_SAMPLE_RATE_8000HZ,
	RTMP_AAC_SAMPLE_RATE_7350HZ,
};

enum {
	RTMP_AAC_CHANNEL_AOT_DEFINED			= 0,
	RTMP_AAC_CHANNEL_1,						// front-center
	RTMP_AAC_CHANNEL_2,						// front-left, front-right
	RTMP_AAC_CHANNEL_3,						// front-center, front-left, front-right
	RTMP_AAC_CHANNEL_4,						// front-center, front-left, front-right, back-cente
	RTMP_AAC_CHANNEL_5,						// front-center, front-left, front-right, back-left, back-right
	RTMP_AAC_CHANNEL_6,						// front-center, front-left, front-right, back-left, back-right, LFE-channel
	RTMP_AAC_CHANNEL_8,						// front-center, front-left, front-right, side-left, side-right, back-left, back-right, LFE-channel
};

enum {
	RTMP_VIDEO_FRAME_TYPE_KEY				= 1,		// for AVC, a seekable frame
	RTMP_VIDEO_FRAME_TYPE_INTER,						// for AVC, a non-seekable frame
	RTMP_VIDEO_FRAME_TYPE_DISPOSABLE_INTER,				// H.263 only
	RTMP_VIDEO_FRAME_TYPE_GENERATED,					// reserved for server use only
	RTMP_VIDEO_FRAME_TYPE_VIDEO_INFO,
};

enum {
	RTMP_VIDEO_CODEC_JPEG					= 1,
	RTMP_VIDEO_CODEC_H263,
	RTMP_VIDEO_CODEC_SCREEN_VIDEO,
	RTMP_VIDEO_CODEC_VP6,
	RTMP_VIDEO_CODEC_VP6_WITH_ALPHA,
	RTMP_VIDEO_CODEC_SCREEN_VIDEO_V2,
	RTMP_VIDEO_CODEC_AVC,
};

enum {
	RTMP_SERVE_NONE	= 0,
	RTMP_SERVE_PUBLISH,
	RTMP_SERVE_PLAY,
};

# define PUTB16( p, x ) \
	p[ 0 ] = ( x >> 8 ) & 0xFF;	\
	p[ 1 ] = ( x & 0xFF );

# define PUTB24( p, x ) \
	p[ 0 ] = ( x >> 16 ) & 0xFF;	\
	p[ 1 ] = ( x >> 8 ) & 0xFF;	\
	p[ 2 ] = ( x & 0xFF );

# define PUTB32( p, x ) \
	p[ 0 ] = ( x >> 24 ) & 0xFF;	\
	p[ 1 ] = ( x >> 16 ) & 0xFF;	\
	p[ 2 ] = ( x >> 8 ) & 0xFF;	\
	p[ 3 ] = ( x & 0xFF );

# define PUTL32( p, x ) \
	p[ 0 ] = ( x & 0xFF );		\
	p[ 1 ] = ( x >> 8 ) & 0xFF;	\
	p[ 2 ] = ( x >> 16 ) & 0xFF;	\
	p[ 3 ] = ( x >> 24 ) & 0xFF;

# define GETB16( p ) ( \
	( ( p[ 0 ] & 0xFF ) << 8 ) |	\
	( p[ 1 ] & 0xFF ) 				\
	)

# define GETB24( p ) ( \
	( ( p[ 0 ] & 0xFF ) << 16 ) |	\
	( ( p[ 1 ] & 0xFF ) << 8 ) |	\
	( p[ 2 ] & 0xFF ) 				\
	)

# define GETB32( p ) ( \
	( ( p[ 0 ] & 0xFF ) << 24 ) |	\
	( ( p[ 1 ] & 0xFF ) << 16 ) |	\
	( ( p[ 2 ] & 0xFF ) << 8 ) |	\
	( p[ 3 ] & 0xFF ) 				\
	)

# define GETL32( p ) ( \
	( p[ 0 ] & 0xFF ) |				\
	( ( p[ 1 ] & 0xFF ) << 8 ) |	\
	( ( p[ 2 ] & 0xFF ) << 16 ) |	\
	( ( p[ 3 ] & 0xFF ) << 24 ) 	\
	)

typedef struct {
	u32		timestamp_client;
	u32		timestamp_server;
	u8		randoms[ 1528 ];
} cn_t;

typedef struct {
	u8		type_id;
	u8		len[ 2 ];
	char	data[ 0 ];
} rtmp_body_string_t;

typedef struct {
	u8	type_id;
	u8	boolean;
} rtmp_body_boolean_t;

typedef struct {
	u8	type_id;
	u8	number[ 8 ];
} rtmp_body_number_t;

typedef struct {
	u8	type_id;
	u8	data[ 0 ];
} rtmp_body_object_t;

typedef struct {
	u8	type_id;
	u8	len[ 4 ];
	u8	data[ 0 ];
} rtmp_body_array_t;

typedef struct {
	u8		len[ 2 ];
	char	data[ 0 ];
} rtmp_object_name_t;

typedef struct {
	u8		type_id;
	u8		len[ 2 ];
	char	data[ 0 ];
} rtmp_object_string_t;

typedef struct {
	char	*name;
	char	*string;
	u32		num;
} rtmp_param_t;

typedef struct {
	int		fd;
	u32		msg_num;
	u32		chunk_size;
	u32		buff_size;
	u8		*buff;
	u32		offset;

	u8		fmt;
	u32		chunk_stream_id;
	u32		timestamp;
	u32		body_size;
	u8		msg_type_id;
	u32		msg_stream_id;

	u8					audio_type;
	RTMP_AUDIO_FMT		audio_fmt;
	u32					sample_rate;
	u32					sample_size;
	u8					channels;

	u32					video_last_timestamp;
	u32					audio_last_timestamp;
	char				start;
	char				type;
	RTMP_ServeHandle	handle;
	void				*data;
} rtmp_t;

typedef struct {
	RTMP_ServeHandle	handle;
	int					fd;
} rtmp_serve_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static int
socket_get_ipaddr( char *addr, char *ipstr, int type )
{
	struct addrinfo hints, *servinfo, *p;
	int status, res = FAILURE;

	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family		= type; // AF_INET ���雓�鞊莎�揭���雓� AF_INET6 ���雓�嚙踝蕭謅��嚙踝嚙踐嚙踝蕭謕蕭��嚙踝蕭��蕭謅��雓�鞊船謆血��雓�嚙踝蕭��嚙踐狐���蕭謖鞊船�����蕭嚙踐��
	hints.ai_socktype	= SOCK_STREAM;

	if ( ( status = getaddrinfo( addr, NULL, &hints, &servinfo ) ) != 0 ) {
		//dbg_line( "%s: %s", addr, gai_strerror( status ) );
		return res;
	}

	for ( p = servinfo; p != NULL; p = p->ai_next ) {
		if ( p->ai_family == type ) {
			if ( p->ai_family == AF_INET ) { // IPv4
				struct sockaddr_in *ipv4 = ( struct sockaddr_in * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv4->sin_addr, ipstr, INET_ADDRSTRLEN );
			} else { // IPv6
# ifdef INET6_ADDRSTRLEN
				struct sockaddr_in6 *ipv6 = ( struct sockaddr_in6 * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv6->sin6_addr, ipstr, INET6_ADDRSTRLEN );
# else
				dbg_error( "Unsupport IPV6!!!" );
# endif
			}
			res = SUCCESS;
			break;
		}
	}

	freeaddrinfo( servinfo );

	return res;
}

static int
sock_writable( int fd );

static int
sock_conn( char *addr, u16 port )
{
	int fd = 0;
	int on = 1;
	struct sockaddr_in sockaddr;
	socklen_t socklen = sizeof( struct sockaddr_in );

# ifdef INET6_ADDRSTRLEN
	char ipstr[ INET6_ADDRSTRLEN ];
# else
	char ipstr[ INET_ADDRSTRLEN ];
# endif

	if ( socket_get_ipaddr( addr, ipstr, AF_INET ) == FAILURE ) {
		return FAILURE;
	}

	fd = socket( PF_INET, SOCK_STREAM, 0 );

	if ( fd < 0 ) {
		//dbg_line( "%s", strerror( errno ) );
		return FAILURE;
	}

# ifdef ENABLE_SOCKET_NONBLOCK_MODE
	fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) );
# else
	struct timeval interval;

	interval.tv_sec		= 5;
	interval.tv_usec	= 0;

	setsockopt( fd, SOL_SOCKET, SO_RCVTIMEO, ( struct timeval * )&interval, sizeof( struct timeval ) );
	setsockopt( fd, SOL_SOCKET, SO_SNDTIMEO, ( struct timeval * )&interval, sizeof( struct timeval ) );
# endif

	setsockopt( fd, SOL_SOCKET, SO_KEEPALIVE, &on, sizeof( on ) );
	setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof( on ) );
	setsockopt( fd, IPPROTO_TCP, TCP_NODELAY, &on, sizeof( on ) );

	bzero( &sockaddr, socklen );

	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_port			= htons( port );
	sockaddr.sin_addr.s_addr	= inet_addr( ipstr );
//	sockaddr.sin_addr.s_addr	= inet_addr( addr );

	if ( sockaddr.sin_addr.s_addr == -1 ) {
		//dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	}

	int window_size = 8192; /* 128 kilobytes */
	/* These setsockopt()s must happen before the connect() */
	setsockopt( fd, SOL_SOCKET, SO_SNDBUF, &window_size, sizeof( window_size ) );
	setsockopt( fd, SOL_SOCKET, SO_RCVBUF, &window_size, sizeof( window_size ) );

	connect( fd, ( struct sockaddr * )&sockaddr, socklen );

//	if ( getsockname( fd, ( struct sockaddr * )&sockaddr, &socklen ) < 0 ) {
//		//dbg_line( "%s", strerror( errno ) );
//		close( fd );
//		return FAILURE;
//	}

# ifdef ENABLE_SOCKET_NONBLOCK_MODE
	if ( sock_writable( fd ) <= 0 ) {
		dbg_warm( "sock_writable( fd ) <= 0" );
		close( fd );
		return FAILURE;
	}
# endif

	return fd;
}

static int
sock_serve( u16 port, u32 max_client )
{
	struct sockaddr_in addr;
	int opt = 1;
	int fd;

	addr.sin_family			= AF_INET;
	addr.sin_addr.s_addr	= INADDR_ANY;
	addr.sin_port			= htons( port );

	if ( ( fd = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	if ( fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) ) != 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	if ( setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof( opt ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	if ( bind( fd, ( struct sockaddr * )&addr, sizeof( addr ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	if ( listen( fd, max_client ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	fcntl( fd, F_SETFD, FD_CLOEXEC );

	return fd;
}

static int
sock_readable( int fd, u32 sec )
{
	fd_set				fds;
	struct timeval		to;

	FD_ZERO( &fds );
	FD_SET( fd, &fds );

	to.tv_sec	= sec;
	to.tv_usec	= 0;

	return select( ( fd + 1 ), &fds, NULL, NULL, &to );
}

static int
sock_writable( int fd )
{
	fd_set				w_fds;
	fd_set				e_fds;
	struct timeval		to;

	FD_ZERO( &w_fds );
	FD_ZERO( &e_fds );
	FD_SET( fd, &w_fds );
	FD_SET( fd, &e_fds );

	to.tv_sec	= 10;
	to.tv_usec	= 0;

	if ( select( ( fd + 1 ), NULL, &w_fds, &e_fds, &to ) != 0 ) {
		if ( FD_ISSET( fd, &e_fds ) ) {
			return -1;
		} else if ( FD_ISSET( fd, &w_fds ) ) {
			return 1;
		} else {
			return -1;
		}
	} else {
		return 0;
	}
}

static int
sock_send( int fd, void *data, int len )
{
	int res, size = 0;

	if ( ( fd < 0 ) || ( data == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd < 0 ) || ( data == NULL ) || ( len <= 0 )" );
		return FAILURE;
	} else {
		u8 *src	= ( u8 * )data;

		while ( size < len ) {
			//res = write( fd, &src[ size ], len - size );
# ifdef MSG_NOSIGNAL
			res = send( fd, &src[ size ], len - size, MSG_NOSIGNAL );
# else
			res = send( fd, &src[ size ], len - size, 0 );
# endif
			if ( res < 0 ) {
				if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
					dbg_line( "errno: %s", strerror( errno ) );
					return FAILURE;
				} else if ( sock_writable( fd ) <= 0 ) {
					dbg_line( "sock_writable( fd ) < 0" );
					return FAILURE;
				}
			} else {
				size += res;
			}
		}

		return size;
	}
}

static int
sock_sendmsg( int fd, const struct msghdr *msg )
{
	int res;

	if ( ( fd < 0 ) || ( msg == NULL ) ) {
		dbg_error( "( fd < 0 ) || ( msg == NULL )" );
		return FAILURE;
	} else {
		while ( 1 ) {
			res = sendmsg( fd, msg, 0 );

			if ( res < 0 ) {
				if ( ( errno == EAGAIN ) || ( errno == EWOULDBLOCK ) ) {
					continue;
				} else {
					dbg_line( "errno: %s", strerror( errno ) );
				}
			}

			return res;
		}
	}
}

static int
sock_recv( int fd, void *data, int len )
{
	if ( ( fd <= 0 ) || ( data == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd <= 0 ) || ( data == NULL ) || ( len <= 0 )" );
		return FAILURE;
	} else {
		int size = 0;
		u8 *dst	= ( u8 * )data;
		u32 curr_tisk = ticks;

		while ( size < len ) {
			if ( sock_readable( fd, 30 ) > 0 ) {
				int res = recv( fd, &dst[ size ], len - size, 0 );

				if ( res < 0 ) {
					if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
						dbg_line( "%s", strerror( errno ) );
						return FAILURE;
					} else {
						dbg_line( "res: %d", res );
dbg_warm( "Time: %u", ticks - curr_tisk );
					}
				} else if ( res == 0 ) {
					dbg_line( "res == 0" );
dbg_warm( "Time: %u, len - size = %u", ticks - curr_tisk, len - size );
					return FAILURE;
				} else {
					size += res;
				}
			} else {
				dbg_line( "sock_readable( fd, 30 ) <= 0" );
dbg_warm( "Time: %u", ticks - curr_tisk );
				return FAILURE;
			}
		}

		return size;
	}
}

static int
rtmp_send( rtmp_t *rtmp, void *external_buff, u32 size )
{
	u8 *header	= rtmp->buff;
	int header_len	= 0, send_len = 0;
	u32 curr_tisk = ticks;

	if ( rtmp->body_size > rtmp->chunk_size ) {
		// Todo
		dbg_line( "rtmp->body_size > rtmp->chunk_size!!!" );
	}

	*header ++ = ( rtmp->fmt << 6 ) | rtmp->chunk_stream_id;

	if ( rtmp->fmt < RTMP_FMT3 ) {
		PUTB24( header, rtmp->timestamp );
		header += 3;
	}

	if ( rtmp->fmt < RTMP_FMT2 ) {
		if ( external_buff ) {
			PUTB24( header, ( rtmp->body_size + size ) );
		} else {
			PUTB24( header, rtmp->body_size );
		}

		header += 3;
		*header ++ = rtmp->msg_type_id;
	}

	if ( rtmp->fmt < RTMP_FMT1 ) {
		PUTL32( header, rtmp->msg_stream_id );
		header += 4;
	}

	header_len = header - rtmp->buff;

# ifdef ENABLE_SOCKET_NONBLOCK_MODE
	if ( sock_send( rtmp->fd, rtmp->buff, header_len ) != header_len ) {
		dbg_line( "Send header Failed!!!" );
		return FAILURE;
	} else if ( rtmp->body_size ) {
		if ( sock_send( rtmp->fd, rtmp->buff + RTMP_HEADER_MAX, rtmp->body_size ) != rtmp->body_size ) {
			dbg_line( "Send body Failed!!!" );
			return FAILURE;
		} else if ( external_buff ) {
			if ( sock_send( rtmp->fd, external_buff, size ) != size ) {
				dbg_line( "Send body Failed!!!" );
				return FAILURE;
			}
		}
	}

	dbg_warm( "Time: %u: size: %u", ticks - curr_tisk, size );

# else

	struct msghdr msg;

	struct iovec io[ 3 ];

	msg.msg_iov		= io;
	msg.msg_iovlen	= 0;

	io[ msg.msg_iovlen ].iov_base	= rtmp->buff;
	io[ msg.msg_iovlen ].iov_len	= header_len;

	send_len += io[ msg.msg_iovlen ].iov_len;
	msg.msg_iovlen ++;

	if ( rtmp->body_size ) {
		io[ msg.msg_iovlen ].iov_base	= rtmp->buff + RTMP_HEADER_MAX;
		io[ msg.msg_iovlen ].iov_len	= rtmp->body_size;

		send_len += io[ msg.msg_iovlen ].iov_len;
		msg.msg_iovlen ++;

		if ( external_buff ) {
			io[ msg.msg_iovlen ].iov_base	= external_buff;
			io[ msg.msg_iovlen ].iov_len	= size;

			send_len += io[ msg.msg_iovlen ].iov_len;
			msg.msg_iovlen ++;
		}
	}

	if ( send_len != sock_sendmsg( rtmp->fd, &msg ) ) {
		dbg_warm( "Time: %u: size: %u", ticks - curr_tisk, send_len );
		return FAILURE;
	}

# endif

	return SUCCESS;
}

static int
rtmp_recv_chunk_stream_id( rtmp_t *rtmp )
{
	u8	temp[ 4 ];
	u32 chunk_stream_id;

	if ( sock_recv( rtmp->fd, temp, 1 ) != 1 ) {
		dbg_line( "Do sock_recv Failed!!" );
		return FAILURE;
	}

	rtmp->fmt				= temp[ 0 ] >> 6;

	chunk_stream_id	= temp[ 0 ] & 0x3F;

	if ( chunk_stream_id == 0 ) {
		if ( sock_recv( rtmp->fd,temp, 1 ) != 1 ) {
			dbg_line( "Do sock_recv Failed!!" );
			return FAILURE;
		}

		chunk_stream_id		= temp[ 0 ] + 64;
	} else if ( chunk_stream_id == 1 ) {
		if ( sock_recv( rtmp->fd, temp, 2 ) != 2 ) {
			dbg_line( "Do sock_recv Failed!!" );
			return FAILURE;
		}

		chunk_stream_id		= GETB16( temp ) + 64;
	}

	return chunk_stream_id;
}

static int
rtmp_recv_chunk_header( rtmp_t *rtmp )
{
	u8	temp[ 4 ];
	u32 chunk_stream_id	= rtmp_recv_chunk_stream_id( rtmp );

	if ( chunk_stream_id == FAILURE ) {
		dbg_line( "Do rtmp_recv_chunk_stream_id Failed!!" );
		return FAILURE;
	}

	if ( rtmp->fmt > RTMP_FMT3 ) {
		dbg_line( "rtmp->fmt Error( %d )!!", rtmp->fmt );
		return FAILURE;
	}

	if ( rtmp->fmt == RTMP_FMT3 ) {
		if ( rtmp->chunk_stream_id != chunk_stream_id ) {
			dbg_line( "chunk_stream_id Error: %d -> %d", rtmp->chunk_stream_id, chunk_stream_id );
		}
		return SUCCESS;
	}

	rtmp->chunk_stream_id	= chunk_stream_id;

	if ( sock_recv( rtmp->fd, temp, 3 ) != 3 ) {
		dbg_line( "rtmp_recv Failed!!" );
		return FAILURE;
	}
	rtmp->timestamp	= GETB24( temp );

	if ( rtmp->fmt == RTMP_FMT2 ) {
		return SUCCESS;
	}

	if ( sock_recv( rtmp->fd, temp, 4 ) != 4 ) {
		dbg_line( "rtmp_recv Failed!!" );
		return FAILURE;
	}

	rtmp->offset		= 0;
	rtmp->body_size		= GETB24( temp );
	rtmp->msg_type_id	= temp[ 3 ];

	if ( rtmp->fmt == RTMP_FMT0 ) {
		if ( sock_recv( rtmp->fd, temp, 4 ) != 4 ) {
			dbg_line( "rtmp_recv Failed!!" );
			return FAILURE;
		}

		rtmp->msg_stream_id	= GETL32( temp );
	}

	if ( rtmp->fmt == RTMP_FMT1 ) {
		if ( rtmp->timestamp == 0xFFFFFF ) {
			if ( sock_recv( rtmp->fd, temp, 4 ) != 4 ) {
				dbg_line( "rtmp_recv Failed!!" );
				return FAILURE;
			}

			rtmp->timestamp	= GETL32( temp );
		}
	}

	if ( rtmp->body_size > BUFFER_LIMIT_SIZE ) {
		dbg_line( "Data Size( %d ) out-of-limit-size( %d )!!", rtmp->body_size, BUFFER_LIMIT_SIZE );
		return FAILURE;
	} else if ( rtmp->body_size > rtmp->buff_size ) {
		dbg_line( "rtmp->body_size( %d ) > rtmp->buff_size( %d )!!", rtmp->body_size, rtmp->buff_size );
		u8 *new_buff = ( u8 * )my_malloc( rtmp->body_size + 4 );

		if ( new_buff == NULL ) {
			dbg_line( "Do my_malloc Failed!!" );
			return FAILURE;
		}

		if ( rtmp->buff ) {
			my_free( rtmp->buff );
			rtmp->buff = NULL;
		}

		rtmp->buff			= new_buff;
		rtmp->buff_size		= rtmp->body_size;
	}

	return SUCCESS;
}

static int
rtmp_recv_chunk( rtmp_t *rtmp )
{
	if ( rtmp_recv_chunk_header( rtmp ) == FAILURE ) {
		dbg_line( "Do rtmp_recv_chunk_header Failed!!" );
		return FAILURE;
	}

	if ( rtmp->body_size ) {
		int recv_size;

		if ( ( rtmp->body_size - rtmp->offset ) > rtmp->chunk_size ) {
			recv_size	= rtmp->chunk_size;
		} else {
			recv_size	= rtmp->body_size - rtmp->offset;
		}

		if ( sock_recv( rtmp->fd, &rtmp->buff[ rtmp->offset ], recv_size ) != recv_size ) {
			dbg_line( "rtmp_recv Failed!!" );
			return FAILURE;
		}

		rtmp->offset += recv_size;
	}

	return SUCCESS;
}

static int
rtmp_recv( rtmp_t *rtmp )
{
	rtmp->body_size = 0;

	if ( rtmp_recv_chunk( rtmp ) == FAILURE ) {
		return FAILURE;
	}

	while ( rtmp->offset < rtmp->body_size ) {
		if ( rtmp_recv_chunk( rtmp ) == FAILURE ) {
			return FAILURE;
		}
	}

	rtmp->buff[ rtmp->body_size ]	= 0;

	return SUCCESS;
}

static int
rtmp_body_add_string( u8 *data, char *string )
{
	rtmp_body_string_t	*body	= ( rtmp_body_string_t * )data;
	int str_len = sprintf( body->data, "%s", string );

	body->type_id	= RTMP_AMF0_STRING;
	PUTB16( body->len, str_len );

	return ( 3 + str_len );
}

static int
rtmp_body_add_num( u8 *data, double num )
{
	rtmp_body_number_t	*body	= ( rtmp_body_number_t * )data;

	int *new_num	= ( int * )&num;
	int temp		= new_num[ 0 ];

	new_num[ 0 ] = htonl( new_num[ 1 ] );
	new_num[ 1 ] = htonl( temp );

	body->type_id	= RTMP_AMF0_NUMBER;

	memcpy( body->number, new_num, 8 );

	return 9;
}

static int
rtmp_body_add_name( u8 *data, char *string )
{
	rtmp_object_name_t *name = ( rtmp_object_name_t * )data;
	int str_len	= sprintf( name->data, "%s", string );

	PUTB16( name->len, str_len );

	return ( 2 + str_len );
}

static int
rtmp_body_add_items( u8 *data, rtmp_param_t *params, int len )
{
	int i;
	u8 *dst = data;

	for ( i = 0; i < len; i ++ ) {
		dst += rtmp_body_add_name( dst, params[ i ].name );

		if ( params[ i ].string ) {
			dst += rtmp_body_add_string( dst, params[ i ].string );
		} else {
			dst += rtmp_body_add_num( dst, params[ i ].num );
		}
	}

	PUTB24( dst, 0x09 );
	dst += 3;

	return dst - data;
}

static int
rtmp_body_add_obj( u8 *data, rtmp_param_t *params, int len )
{
	rtmp_body_object_t	*body	= ( rtmp_body_object_t * )data;
	int size = 0;

	body->type_id	= RTMP_AMF0_OBJECT;
	size ++;

	size += rtmp_body_add_items( body->data, params, len );

	return size;
}

static int
rtmp_body_add_array( u8 *data, rtmp_param_t *params, int len )
{
	rtmp_body_array_t	*body	= ( rtmp_body_array_t * )data;
	int size = 0;

	body->type_id	= RTMP_AMF0_ARRAY;
	size ++;

	PUTB32( body->len, len );
	size += 4;

	size += rtmp_body_add_items( body->data, params, len );

	return size;
}

static u32
rtmp_body_get_string( u8 *data, char **string, u32 *len )
{
	rtmp_body_string_t	*body	= ( rtmp_body_string_t * )data;
	u32 size = 0;

	if ( body->type_id != RTMP_AMF0_STRING ) {
		if ( len ) {
			*len	= 0;
		}

		if ( string ) {
			*string	= NULL;
		}
		return 0;
	}

	size	= GETB16( body->len );
	if ( len ) {
		*len	= size;
	}
	size += 3;

	if ( string ) {
		*string	= body->data;
	}

	return size;
}

static u32
rtmp_body_get_bollean( u8 *data, u32 *num )
{
	rtmp_body_boolean_t	*body	= ( rtmp_body_boolean_t * )data;

	if ( body->type_id != RTMP_AMF0_BOOLEAN ) {
		if ( num ) {
			*num	= 0;
		}

		return 0;
	}

	if ( num ) {
		*num	= body->boolean;
	}

	return 2;
}

static u32
rtmp_body_get_num( u8 *data, double *num )
{
	rtmp_body_number_t	*body	= ( rtmp_body_number_t * )data;

	if ( body->type_id != RTMP_AMF0_NUMBER ) {
		if ( num ) {
			*num	= 0;
		}

		return 0;
	}

	if ( num ) {
		int *new_num	= ( int * )&body->number;
		int temp		= new_num[ 0 ];

		new_num[ 0 ] = htonl( new_num[ 1 ] );
		new_num[ 1 ] = htonl( temp );

		memcpy( num, new_num, 8 );
	}

	return 9;
}

static u32
rtmp_body_get_items( u8 *data, rtmp_param_t *params, int len )
{
	int i;
	u32 param_len = 0, name_len, str_len = 0;
	rtmp_object_name_t	*name;
	double	num;

	for ( i = 0; i < len; i ++ ) {
		name		= ( rtmp_object_name_t * )&data[ param_len ];
		name_len	= GETB16( name->len );

		if ( i ) {
			if ( params[ i - 1 ].string ) {
				params[ i - 1 ].string[ str_len ]	= 0;
			}
		}

		if ( GETB24( ( data + param_len ) ) == 0x09 ) {
			param_len += 3;
			break;
		}

		param_len += 2;
		params[ i ].name	= name->data;
		param_len += name_len;

		if ( data[ param_len ] == RTMP_AMF0_STRING ) {
			param_len += rtmp_body_get_string( &data[ param_len ], &params[ i ].string, &str_len );
		} else if ( data[ param_len ] == RTMP_AMF0_NUMBER ) {
			param_len += rtmp_body_get_num( &data[ param_len ], &num );
			params[ i ].num	= num;
		} else if ( data[ param_len ] == RTMP_AMF0_BOOLEAN ) {
			param_len += rtmp_body_get_bollean( &data[ param_len ], &params[ i ].num );
		} else {
			dbg_line( "Unknow-Type" );
		}

		name->data[ name_len ]	= 0;
	}

	return param_len;
}

static u32
rtmp_body_get_obj( u8 *data, rtmp_param_t *params, int len )
{
	rtmp_body_object_t	*body	= ( rtmp_body_object_t * )data;

	if ( params ) {
		memset( params, 0, ( sizeof( rtmp_param_t ) * len ) );
	}

	if ( body->type_id != RTMP_AMF0_OBJECT ) {
		return 0;
	}

	return ( 1 + rtmp_body_get_items( body->data, params, len ) );
}

static u32
rtmp_body_get_array( u8 *data, rtmp_param_t *params, int len )
{
	rtmp_body_array_t	*body	= ( rtmp_body_array_t * )data;
	int items;

	if ( params ) {
		memset( params, 0, ( sizeof( rtmp_param_t ) * len ) );
	}

	if ( body->type_id != RTMP_AMF0_ARRAY ) {
		return 0;
	}

	items	= GETB32( body->len );

	return ( 5 + rtmp_body_get_items( body->data, params, ( len > items )? items: len ) );
}

static int
rtmp_handshark( rtmp_t *rtmp )
{
	int ret;
	u8			ver	= 0x03;
	cn_t 		*data	= ( cn_t * )rtmp->buff;

	data->timestamp_client	= htonl( Ticks() );
	data->timestamp_server	= 0;

	int i;

	for ( i = 0; i < 1528; i ++ ) {
		data->randoms[ i ]	= i & 0xFF;
	}

	if ( ( ret = sock_send( rtmp->fd, &ver, sizeof( ver ) ) ) != sizeof( ver ) ) {
		dbg_line( "Send c0 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_send( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Send c1 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_recv( rtmp->fd, &ver, sizeof( ver ) ) ) != sizeof( ver ) ) {
		dbg_line( "Recv s0 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ver != 0x03 ) {
		dbg_line( "Version Error!!!( %d )", ver );
		return FAILURE;
	} else if ( ( ret = sock_recv( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Recv s1 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_send( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Send c2 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_recv( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Recv s2 Failed!!!( %d )", ret );
		return FAILURE;
	} else {
		return SUCCESS;
	}
}

static int
rtmp_serve_handshark( rtmp_t *rtmp )
{
	int ret;
	u8			ver	= 0x03;
	cn_t 		*data	= ( cn_t * )rtmp->buff;

	int i;

	if ( ( ret = sock_recv( rtmp->fd, &ver, sizeof( ver ) ) ) != sizeof( ver ) ) {
		dbg_line( "Recv c0 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ver != 0x03 ) {
		dbg_line( "Version Error!!!( %d )", ver );
		return FAILURE;
	} else if ( ( ret = sock_recv( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Recv c1 Failed!!!( %d )", ret );
		return FAILURE;
	}

//	data->timestamp_server	= htonl( Ticks() );

	for ( i = 0; i < 1528; i ++ ) {
		data->randoms[ i ]	= i & 0xFF;
	}

	 if ( ( ret = sock_send( rtmp->fd, &ver, sizeof( ver ) ) ) != sizeof( ver ) ) {
		dbg_line( "Send s0 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_send( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Send s1 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_send( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Send s2 Failed!!!( %d )", ret );
		return FAILURE;
	} else if ( ( ret = sock_recv( rtmp->fd, data, sizeof( cn_t ) ) ) != sizeof( cn_t ) ) {
		dbg_line( "Recv c2 Failed!!!( %d )", ret );
		return FAILURE;
	} else {
		return SUCCESS;
	}
}

static int
rtmp_send_h264( rtmp_t *rtmp, u8 *frame, u32 size, u8 is_key, u32 timestamp )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	if ( is_key ) {
		*dst ++	= ( ( RTMP_VIDEO_FRAME_TYPE_KEY << 4 ) | RTMP_VIDEO_CODEC_AVC );
	} else {
		*dst ++	= ( ( RTMP_VIDEO_FRAME_TYPE_INTER << 4 ) | RTMP_VIDEO_CODEC_AVC );
	}

	*dst ++	= 0x01;
	*dst ++	= 0x00;
	*dst ++	= 0x00;
	*dst ++	= 0x00;

	PUTB32( dst, size );
	dst += 4;

# ifdef ENABLE_MULTI_STREAM_CHANNEL
	if ( rtmp->type	== RTMP_SERVE_PLAY ) {
		rtmp->fmt					= RTMP_FMT1;
		rtmp->chunk_stream_id		= RTMP_CHANNEL_VIDEO;
		rtmp->timestamp				= timestamp - rtmp->video_last_timestamp;
	} else {
		rtmp->fmt					= RTMP_FMT0;
		rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
		rtmp->timestamp				= Ticks();
	}
# else
	rtmp->fmt					= RTMP_FMT0;
	rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
	rtmp->timestamp				= Ticks();
# endif

	rtmp->body_size				= dst - body;
	rtmp->msg_type_id			= RTMP_TYPE_VIDEO;
	rtmp->video_last_timestamp	= timestamp;

	return rtmp_send( rtmp, frame, size );
}

static int
rtmp_send_audio_format( rtmp_t *rtmp, u32 timestamp )
{
# ifdef ENABLE_AUDIO
	rtmp->audio_type	= rtmp->audio_fmt << 4;

	if ( rtmp->sample_rate >= 44000 ) {
		rtmp->audio_type	|= ( RTMP_AUDIO_SAMPLE_RATE_44K << 2 );
	} else if ( rtmp->sample_rate >= 22000 ) {
		rtmp->audio_type	|= ( RTMP_AUDIO_SAMPLE_RATE_22K << 2 );
	} else if ( rtmp->sample_rate >= 11000 ) {
		rtmp->audio_type	|= ( RTMP_AUDIO_SAMPLE_RATE_11K << 2 );
	} else {
		rtmp->audio_type	|= ( RTMP_AUDIO_SAMPLE_RATE_5_5 << 2 );
	}

	if ( rtmp->sample_size == 16 ) {
		rtmp->audio_type	|= ( 0x01 << 1 );
	}

	if ( rtmp->channels > 1 ) {
		rtmp->audio_type	|= 0x01;
	}

	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	*dst ++	= rtmp->audio_type;
/*
	enum {
		RTMP_AAC_TYPE_MAIN	= 0,
		RTMP_AAC_TYPE_1C,
		RTMP_AAC_TYPE_SSR,
		RTMP_AAC_TYPE_LTP,
	};
	*/

	if ( rtmp->audio_fmt == RTMP_AUDIO_FMT_AAC ) {
		u16 audio_spec	= ( RTMP_AAC_TYPE_SSR << 11 ) & 0xF800;

		if ( rtmp->sample_rate == 96000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_96000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 88200 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_88200HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 64000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_64000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 48000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_48000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 44100 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_44100HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 32000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_32000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 24000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_24000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 22050 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_22050HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 16000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_16000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 12000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_12000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 11025 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_11025HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 8000 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_8000HZ << 7 ) & 0x0780;
		} else if ( rtmp->sample_rate == 7350 ) {
			audio_spec |= ( RTMP_AAC_SAMPLE_RATE_7350HZ << 7 ) & 0x0780;
		}

		audio_spec |= ( RTMP_AAC_CHANNEL_2 << 3 ) & 0x0078;

		*dst ++	= 0x00;
		PUTB16( dst, audio_spec );
		dst += 2;
		*dst ++	= 0x00;
	}

	rtmp->fmt				= RTMP_FMT0;

# ifdef ENABLE_MULTI_STREAM_CHANNEL
	if ( rtmp->type	== RTMP_SERVE_PLAY ) {
		rtmp->chunk_stream_id		= RTMP_CHANNEL_AUDIO;
		rtmp->timestamp				= rtmp->audio_last_timestamp;
	} else {
		rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
		rtmp->timestamp				= Ticks();
	}
# else
	rtmp->chunk_stream_id	= RTMP_CHANNEL_STREAM;
	rtmp->timestamp			= Ticks();
# endif

	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AUDIO;

	return rtmp_send( rtmp, NULL, 0 );
# else
	return SUCCESS;
# endif
}

static int
rtmp_send_audio( rtmp_t *rtmp, u8 *frame, u32 size, u32 timestamp )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	*dst ++	= rtmp->audio_type;

	if ( rtmp->audio_fmt == RTMP_AUDIO_FMT_AAC ) {
		*dst ++	= 0x01;
	}

	rtmp->fmt					= RTMP_FMT1;

# ifdef ENABLE_MULTI_STREAM_CHANNEL
	if ( rtmp->type	== RTMP_SERVE_PLAY ) {
		rtmp->chunk_stream_id		= RTMP_CHANNEL_AUDIO;
		rtmp->timestamp				= timestamp	- rtmp->audio_last_timestamp;
	} else {
		rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
		rtmp->timestamp				= 0;
	}
# else
	rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
	rtmp->timestamp				= 0;
# endif

	rtmp->audio_last_timestamp	= timestamp;
	rtmp->body_size				= dst - body;
	rtmp->msg_type_id			= RTMP_TYPE_AUDIO;

	return rtmp_send( rtmp, frame, size );
}

static int
rtmp_set_window_size( rtmp_t *rtmp )
{
	u8 *body, *dst;

	dst		=
	body	= rtmp->buff + RTMP_HEADER_MAX;

	PUTB32( dst, 5000000 );
	dst += 4;

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x02;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_WINDOW_ACK_SIZE;

	return rtmp_send( rtmp, NULL, 0 );
}

static int
rtmp_set_peer_bw( rtmp_t *rtmp )
{
	u8 *body, *dst;

	dst		=
	body	= rtmp->buff + RTMP_HEADER_MAX;

	PUTB32( dst, 5000000 );
	dst += 4;

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x02;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_PEER_BANDWIDTH;

	return rtmp_send( rtmp, NULL, 0 );
}

static int
rtmp_set_chunk_size( rtmp_t *rtmp, int size )
{
	u8 *body, *dst;

	if ( size != rtmp->chunk_size ) {
		rtmp->chunk_size	= size;
	}

	dst		=
	body	= rtmp->buff + RTMP_HEADER_MAX;

	PUTB32( dst, rtmp->chunk_size );
	dst += 4;

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x02;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_SET_PACKET_SIZE;

	return rtmp_send( rtmp, NULL, 0 );
}

static int
rtmp_send_connect_result( rtmp_t *rtmp, int error )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	int params = 0;
	rtmp_param_t conn_param[ 5 ];

	memset( conn_param, 0, sizeof( rtmp_param_t ) * 5 );

	if ( error ) {
		dst += rtmp_body_add_string( dst, "_error" );
	} else {
		dst += rtmp_body_add_string( dst, "_result" );
	}

	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );

	conn_param[ params ].name		= "fmsVer";
	conn_param[ params ++ ].string	= "FMLE/3.0 (compatible; FMSc/1.0)";

	conn_param[ params ].name		= "capabilities";
	conn_param[ params ++ ].num		= 31;

	dst += rtmp_body_add_obj( dst, conn_param, params );

	memset( conn_param, 0, sizeof( rtmp_param_t ) * 5 );

	params = 0;

	conn_param[ params ].name		= "level";
	conn_param[ params ++ ].string	= "status";

	conn_param[ params ].name		= "code";

	if ( error ) {
		conn_param[ params ++ ].string	= "NetConnection.Connect.InvalidApp";
	} else {
		conn_param[ params ++ ].string	= "NetConnection.Connect.Success";
	}

	conn_param[ params ].name		= "description";
	conn_param[ params ++ ].string	= "Connection success.";

	conn_param[ params ].name		= "objectEncoding";
	conn_param[ params ++ ].num		= 0;

	dst += rtmp_body_add_obj( dst, conn_param, params );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	return SUCCESS;
}

static int
rtmp_send_create_stream_result( rtmp_t *rtmp, int error )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	if ( error ) {
		dst += rtmp_body_add_string( dst, "_error" );
	} else {
		dst += rtmp_body_add_string( dst, "_result" );
	}

	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );

	*dst ++ = RTMP_AMF0_NULL;
	rtmp->msg_stream_id		= 2;

	dst += rtmp_body_add_num( dst, rtmp->msg_stream_id );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	return SUCCESS;
}

static int
rtmp_send_publish_result( rtmp_t *rtmp, int error )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	int params = 0;
	rtmp_param_t conn_param[ 5 ];

	memset( conn_param, 0, sizeof( rtmp_param_t ) * 5 );

	dst += rtmp_body_add_string( dst, "onStatus" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );

	*dst ++ = RTMP_AMF0_NULL;

	conn_param[ params ].name		= "level";
	conn_param[ params ++ ].string	= "status";

	conn_param[ params ].name		= "code";
	if ( error ) {
		conn_param[ params ++ ].string	= "NetStream.Publish.Failed";
	} else {
		conn_param[ params ++ ].string	= "NetStream.Publish.Start";
	}

	dst += rtmp_body_add_obj( dst, conn_param, params );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	rtmp->start	= 1;
	rtmp->type	= RTMP_SERVE_PUBLISH;

	return SUCCESS;
}

static int
rtmp_send_play_result( rtmp_t *rtmp, int error )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	int params = 0;
	rtmp_param_t conn_param[ 5 ];

	memset( conn_param, 0, sizeof( rtmp_param_t ) * 5 );

	dst += rtmp_body_add_string( dst, "onStatus" );
	dst += rtmp_body_add_num( dst, 0 );

	*dst ++ = RTMP_AMF0_NULL;

	conn_param[ params ].name		= "level";
	conn_param[ params ++ ].string	= "status";

	conn_param[ params ].name		= "code";
	if ( error ) {
		conn_param[ params ++ ].string	= "NetStream.Play.Failed";
	} else {
		conn_param[ params ++ ].string	= "NetStream.Play.Start";
	}

	conn_param[ params ].name		= "description";
	conn_param[ params ++ ].string	= "Start live";

	dst += rtmp_body_add_obj( dst, conn_param, params );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	rtmp->start	= 1;
	rtmp->type	= RTMP_SERVE_PLAY;

	return SUCCESS;
}

static int
rtmp_send_stream_begin( rtmp_t *rtmp )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	PUTB16( dst, 0 );
	dst += 2;

	//rtmp->msg_stream_id	= 1;
	dbg_line( "rtmp->msg_stream_id: %d", rtmp->msg_stream_id );

	PUTB32( dst, rtmp->msg_stream_id );
	dst += 4;

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x02;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_USER_CONTROL;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	return SUCCESS;
}

static int
rtmp_parse_set_chunk_size( rtmp_t *rtmp )
{
	rtmp->chunk_size	= GETB32( rtmp->buff );
	dbg_line( "Chunk Size: %d", rtmp->chunk_size );
	return SUCCESS;
}

static int
rtmp_parse_window_size( rtmp_t *rtmp )
{
	dbg_line( "Window Size: %d", rtmp->chunk_size );
	return SUCCESS;
}

static int
rtmp_parse_peer_bw( rtmp_t *rtmp )
{
	dbg_line( "Peer Bandwidth: %d", GETB32( rtmp->buff ) );
	return SUCCESS;
}

static int
rtmp_parse_invoke( rtmp_t *rtmp )
{
	u8 *body		= rtmp->buff, *p = body;
	char *string	= NULL;
	rtmp_param_t conn_param[ 10 ];
	u32	str_len, res = FAILURE;
	double	num;

	p += rtmp_body_get_string( p, &string, &str_len );

	if ( string == NULL ) {
		return FAILURE;
	} else if ( strcmp( string, "connect" ) == 0 ) {
		if ( rtmp->handle.connect ) {
			RTMP_CTX ctx = { 0, };
			int i;

			p += rtmp_body_get_num( p, &num );

			// Properties
			p += rtmp_body_get_obj( p, conn_param, 10 );

			for ( i = 0; i < 10; i ++ ) {
				if ( conn_param[ i ].name ) {
					if ( strcmp( conn_param[ i ].name, "app" ) == 0 ) {
						ctx.app		= conn_param[ i ].string;
					} else if ( strcmp( conn_param[ i ].name, "swfUrl" ) == 0 ) {
						ctx.swfUrl	= conn_param[ i ].string;
					} else if ( strcmp( conn_param[ i ].name, "tcUrl" ) == 0 ) {
						ctx.tcUrl	= conn_param[ i ].string;
					}
				} else {
					break;
				}
			}

			res = rtmp->handle.connect( &ctx, rtmp->data );

			if ( res == SUCCESS ) {
				if ( rtmp_set_window_size( rtmp ) == FAILURE ) {
					dbg_line( "Do rtmp_set_window_size Failed!!!" );
					return FAILURE;
				}

				if ( rtmp_set_chunk_size( rtmp, 1280000 ) == FAILURE ) {
					dbg_line( "Do rtmp_set_chunk_size Failed!!!" );
					return FAILURE;
				}
			}
		}

		return rtmp_send_connect_result( rtmp, res );
	} else if ( strcmp( string, "releaseStream" ) == 0 ) {
		if ( rtmp->handle.releaseStream ) {
			p += rtmp_body_get_num( p, &num );

			// NULL
			if ( *p ++ != RTMP_AMF0_NULL ) {
				dbg_line( "Format Error!!!" );
				return FAILURE;
			}

			// Stream Name
			p += rtmp_body_get_string( p, &string, &str_len );
			if ( string ) {
				dbg_line( "releaseStream Stream Name: %s", string );
				rtmp->handle.releaseStream( string, rtmp->data );
			} else {
				dbg_line( "Empty Stream Name!!!" );
				return FAILURE;
			}
		}
	} else if ( strcmp( string, "FCPublish" ) == 0 ) {
		p += rtmp_body_get_num( p, &num );

		// NULL
		if ( *p ++ != RTMP_AMF0_NULL ) {
			dbg_line( "Format Error!!!" );
			return FAILURE;
		}

		// Stream Name
		p += rtmp_body_get_string( p, &string, &str_len );
		dbg_line( "FCPublish Stream Name: %s", string );
	} else if ( strcmp( string, "createStream" ) == 0 ) {
		if ( rtmp->handle.createStream ) {
			p += rtmp_body_get_num( p, &num );

			// NULL
			if ( *p ++ != RTMP_AMF0_NULL ) {
				dbg_line( "Format Error!!!" );
				return FAILURE;
			}

			// Stream Name
			p += rtmp_body_get_string( p, &string, &str_len );

			if ( string ) {
				dbg_line( "releaseStream Stream Name: %s", string );
			}

			res	= rtmp->handle.createStream( string, rtmp->data );
		}

		return rtmp_send_create_stream_result( rtmp, res );
	} else if ( strcmp( string, "publish" ) == 0 ) {
		if ( rtmp->handle.publish ) {
			char *stream_name;
			u32 len;
			p += rtmp_body_get_num( p, &num );

			// NULL
			if ( *p ++ != RTMP_AMF0_NULL ) {
				dbg_line( "Format Error!!!" );
				return FAILURE;
			}

			// Stream Name
			p += rtmp_body_get_string( p, &string, &str_len );

			if ( string == NULL ) {
				dbg_line( "Empty Stream Name!!!" );
				return FAILURE;
			}

			dbg_line( "Stream Name: %s", string );
			stream_name = string;
			len			= str_len;

			// Command Name
			p += rtmp_body_get_string( p, &string, &str_len );

			if ( string == NULL ) {
				dbg_line( "Empty Command Name!!!" );
				return FAILURE;
			} else if ( strcmp( string, "record" ) == 0 ) {
				//ctx->upload = RTMP_UPLOAD_RECORD;
			} else if ( strcmp( string, "append" ) == 0 ) {
				//ctx->upload = RTMP_UPLOAD_APPEND;
			} else if ( strcmp( string, "live" ) == 0 ) {
				//ctx->upload = RTMP_UPLOAD_LIVE;
			}
			dbg_line( "Command Name: %s", string );

			stream_name[ len ]	= 0;
			res	= rtmp->handle.publish( stream_name, rtmp->data );
		}

		return rtmp_send_publish_result( rtmp, res );
	} else if ( strcmp( string, "play" ) == 0 ) {
		if ( rtmp->handle.play ) {
			p += rtmp_body_get_num( p, &num );

			// NULL
			if ( *p ++ != RTMP_AMF0_NULL ) {
				dbg_line( "Format Error!!!" );
				return FAILURE;
			}

			// Stream Name
			p += rtmp_body_get_string( p, &string, &str_len );

			if ( string ) {
				dbg_line( "Stream Name: %s", string );
			}

			res	= rtmp->handle.play( string, rtmp->data );
		} else {
			dbg_line( "rtmp->handle.play == NULL" );
			return FAILURE;
		}

		if ( res == SUCCESS ) {
			if ( rtmp_set_chunk_size( rtmp, 1280000 ) == FAILURE ) {
				dbg_line( "Do rtmp_set_chunk_size Failed!!!" );
				return FAILURE;
			}

			if ( rtmp_send_stream_begin( rtmp ) == FAILURE ) {
				dbg_line( "Do rtmp_send_stream_begin Failed!!!" );
				return FAILURE;
			}
		}

		return rtmp_send_play_result( rtmp, res );
	} else {
		dbg_line( "string: %s", string );
	}

	return SUCCESS;
}

static int
rtmp_parse_audio( rtmp_t *rtmp )
{
	if ( rtmp->body_size <= 2 ) {
		return SUCCESS;
	} else if ( rtmp->handle.audio_in ) {
		rtmp->audio_type	= rtmp->buff[ 0 ];
		rtmp->audio_fmt		= ( rtmp->audio_type >> 4 ) & 0X0F;

		if ( rtmp->buff[ 1 ] == 0 ) {
			return SUCCESS;
		} else if ( rtmp->audio_fmt == RTMP_AUDIO_FMT_AAC ) {
			return rtmp->handle.audio_in( rtmp->buff + 2, rtmp->body_size - 2, rtmp->data );
		} else {
			return rtmp->handle.audio_in( rtmp->buff + 1, rtmp->body_size - 1, rtmp->data );
		}
	}

	return SUCCESS;
}

static int
rtmp_parse_video( rtmp_t *rtmp )
{
	if ( rtmp->body_size <= 9 ) {
		return SUCCESS;
	} else if ( rtmp->handle.video_in ) {
		u8 *body = rtmp->buff;

		if ( body[ 0 ] == ( ( RTMP_VIDEO_FRAME_TYPE_KEY << 4 ) | RTMP_VIDEO_CODEC_AVC ) ) {
			u8 *src = body;
			u32 len = 0;

			src ++;

			if ( *src == 0 ) {
				src += 4;

				if ( *src == 1 ) {
					src += 6;
					len = GETB16( src );

					src += 2;

					rtmp->handle.video_in( src, len, rtmp->data );

					src += len;

					if ( *src == 1 ) {
						src += 1;

						len = GETB16( src );

						src += 2;
						rtmp->handle.video_in( src, len, rtmp->data );
					}
				}

				return SUCCESS;
			}
		}

		if ( body[ 1 ] == 1 ) {
			if ( rtmp->body_size > 9 ) {
				return rtmp->handle.video_in( rtmp->buff + 9, rtmp->body_size - 9, rtmp->data );
			}
		}
	}

	return SUCCESS;
}

static int
rtmp_parse_connect_result( rtmp_t *rtmp )
{
	u8 *body		= rtmp->buff, *p = body;
	char *string	= NULL;
	rtmp_param_t conn_param[ 10 ];
	u32	str_len, i = 0;
	double	num;

	p += rtmp_body_get_string( p, &string, &str_len );

	if ( string == NULL ) {
		dbg_line( "Empty String!!!" );
		return FAILURE;
	} else if ( memcmp( string, "_result", str_len ) ) {
		dbg_line( "Not _result!!" );
		return FAILURE;
	}

	p += rtmp_body_get_num( p, &num );

	// Properties
	p += rtmp_body_get_obj( p, conn_param, 10 );

	// Information
	p += rtmp_body_get_obj( p, conn_param, 10 );

	if ( ( p - body ) != rtmp->body_size ) {
		return FAILURE;
	}

	while ( ( i < 10 ) && conn_param[ i ].name ) {
		if ( strcmp( conn_param[ i ].name, "code" ) == 0 ) {
			if ( conn_param[ i ].string ) {
				if ( strcmp( conn_param[ i ].string, "NetConnection.Connect.Success" ) == 0 ) {
					return SUCCESS;
				} else {
					dbg_line( "Code Error: %s", conn_param[ i ].string );
				}
			}
			return FAILURE;
		}
		i ++;
	}

	return SUCCESS;
}

static int
rtmp_parse_create_stream_result( rtmp_t *rtmp )
{
	u8 *body		= rtmp->buff, *p = body;
	char *string	= NULL;
	u32	str_len;
	double	num;

	p += rtmp_body_get_string( p, &string, &str_len );

	if ( string == NULL ) {
		dbg_line( "Empty String!!!" );
		return FAILURE;
	} else if ( memcmp( string, "_result", str_len ) ) {
		dbg_line( "Not _result!!" );
		return FAILURE;
	}

	p += rtmp_body_get_num( p, &num );

	// NULL
	if ( *p ++ != RTMP_AMF0_NULL ) {
		dbg_line( "Format Error!!!" );
		return FAILURE;
	}

	// Information
	p += rtmp_body_get_num( p, &num );

	rtmp->msg_stream_id	= num;

	return SUCCESS;
}

static int
rtmp_parse_publish_result( rtmp_t *rtmp )
{
	u8 *body		= rtmp->buff, *p = body;
	char *string	= NULL;
	rtmp_param_t conn_param[ 10 ];
	u32	str_len, i = 0;
	double	num;

	p += rtmp_body_get_string( p, &string, &str_len );

	if ( string == NULL ) {
		dbg_line( "Empty String!!!" );
		return FAILURE;
	} else if ( memcmp( string, "onStatus", str_len ) ) {
		dbg_line( "Not onStatus!!" );
		return FAILURE;
	}

	p += rtmp_body_get_num( p, &num );

	// NULL
	if ( *p ++ != RTMP_AMF0_NULL ) {
		dbg_line( "Format Error!!!" );
		return FAILURE;
	}

	// Information
	p += rtmp_body_get_obj( p, conn_param, 10 );

	if ( ( p - body ) != rtmp->body_size ) {
		return FAILURE;
	}

	while ( ( i < 10 ) && conn_param[ i ].name ) {
		if ( strcmp( conn_param[ i ].name, "code" ) == 0 ) {
			if ( conn_param[ i ].string ) {
				if ( strcmp( conn_param[ i ].string, "NetStream.Publish.Start" ) == 0 ) {
					return SUCCESS;
				} else {
					dbg_line( "Code Error: %s", conn_param[ i ].string );
				}
			}
			return FAILURE;
		}
		i ++;
	}

	return SUCCESS;
}

static int
rtmp_parse_play_result( rtmp_t *rtmp )
{
	u8 *body		= rtmp->buff, *p = body;
	char *string	= NULL;
	rtmp_param_t conn_param[ 10 ];
	u32	str_len, i = 0;
	double	num;

	p += rtmp_body_get_string( p, &string, &str_len );
	if ( memcmp( string, "onStatus", str_len ) ) {
		dbg_line( "Not onStatus!!" );
		return FAILURE;
	}

	p += rtmp_body_get_num( p, &num );

	// NULL
	if ( *p ++ != RTMP_AMF0_NULL ) {
		dbg_line( "Format Error!!!" );
		return FAILURE;
	}

	// Information
	p += rtmp_body_get_obj( p, conn_param, 10 );

	if ( ( p - body ) != rtmp->body_size ) {
		return FAILURE;
	}

	while ( ( i < 10 ) && conn_param[ i ].name ) {
		if ( strcmp( conn_param[ i ].name, "code" ) == 0 ) {
			if ( conn_param[ i ].string ) {
				if ( strcmp( conn_param[ i ].string, "NetStream.Play.Start" ) == 0 ) {
					return SUCCESS;
				} else {
					dbg_line( "Code Error: %s", conn_param[ i ].string );
				}
			}
			return FAILURE;
		}
		i ++;
	}

	return SUCCESS;
}

static int
rtmp_connect( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "connect" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );

	int params = 0;
	rtmp_param_t conn_param[ 5 ];

	memset( conn_param, 0, sizeof( rtmp_param_t ) * 5 );

	conn_param[ params ].name		= "app";
	conn_param[ params ++ ].string	= ctx->app;

	conn_param[ params ].name		= "type";
	conn_param[ params ++ ].string	= "nonprivate";

	conn_param[ params ].name		= "flashVer";
	conn_param[ params ++ ].string	= "FMLE/3.0 (compatible; FMSc/1.0)";

	conn_param[ params ].name		= "swfUrl";
	conn_param[ params ++ ].string	= ctx->swfUrl;

	conn_param[ params ].name		= "tcUrl";
	conn_param[ params ++ ].string	= ctx->tcUrl;

	dst += rtmp_body_add_obj( dst, conn_param, params );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	while ( 1 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			return FAILURE;
		} else if ( rtmp->body_size == 0 ) {
			return FAILURE;
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_SET_PACKET_SIZE:
					rtmp_parse_set_chunk_size( rtmp );
					break;

				case RTMP_TYPE_WINDOW_ACK_SIZE:
					rtmp_parse_window_size( rtmp );
					break;

				case RTMP_TYPE_PEER_BANDWIDTH:
					rtmp_parse_peer_bw( rtmp );
					break;

				case RTMP_TYPE_AMF0_INVOKE:
					return rtmp_parse_connect_result( rtmp );
			}
		}
	}

	return FAILURE;
}

static int
rtmp_release_stream( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "releaseStream" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );
	*dst ++ = RTMP_AMF0_NULL;
	dst += rtmp_body_add_string( dst, ctx->stream_name );

	rtmp->fmt				= RTMP_FMT1;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	return rtmp_send( rtmp, NULL, 0 );
}

static int
rtmp_fcpublish( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "FCPublish" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );
	*dst ++ = RTMP_AMF0_NULL;
	dst += rtmp_body_add_string( dst, ctx->stream_name );

	rtmp->fmt				= RTMP_FMT1;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	return rtmp_send( rtmp, NULL, 0 );
}

static int
rtmp_create_stream( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "createStream" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );
	*dst ++ = RTMP_AMF0_NULL;

	rtmp->fmt				= RTMP_FMT1;
	rtmp->chunk_stream_id	= 0x03;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	while ( 1 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			return FAILURE;
		} else if ( rtmp->body_size == 0 ) {
			return FAILURE;
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_AMF0_INVOKE:
					return rtmp_parse_create_stream_result( rtmp );
			}
		}
	}

	return SUCCESS;
}

static int
rtmp_publish( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "publish" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );
	*dst ++ = RTMP_AMF0_NULL;
	dst += rtmp_body_add_string( dst, ctx->stream_name );

	if ( ctx->upload == RTMP_UPLOAD_RECORD ) {
		dst += rtmp_body_add_string( dst, "record" );
	} else if ( ctx->upload == RTMP_UPLOAD_APPEND ) {
		dst += rtmp_body_add_string( dst, "append" );
	} else {
		dst += rtmp_body_add_string( dst, "live" );
	}

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x04;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;
	rtmp->msg_stream_id		= 0x01;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	while ( 1 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			return FAILURE;
		} else if ( rtmp->body_size == 0 ) {
			return FAILURE;
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_AMF0_INVOKE:
					return rtmp_parse_publish_result( rtmp );
			}
		}
	}

	return SUCCESS;
}

static int
rtmp_play( rtmp_t *rtmp, RTMP_CTX *ctx )
{
	u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

	dst += rtmp_body_add_string( dst, "play" );
	dst += rtmp_body_add_num( dst, rtmp->msg_num ++ );
	*dst ++ = RTMP_AMF0_NULL;
	dst += rtmp_body_add_string( dst, ctx->stream_name );
	dst += rtmp_body_add_num( dst, 0 );

	rtmp->fmt				= RTMP_FMT0;
	rtmp->chunk_stream_id	= 0x04;
	rtmp->timestamp			= 0;
	rtmp->body_size			= dst - body;
	rtmp->msg_type_id		= RTMP_TYPE_AMF0_INVOKE;
	rtmp->msg_stream_id		= 0x01;

	if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
		return FAILURE;
	}

	while ( 1 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			return FAILURE;
		} else if ( rtmp->body_size == 0 ) {
			return FAILURE;
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_AMF0_INVOKE:
					return rtmp_parse_play_result( rtmp );
			}
		}
	}

	return SUCCESS;
}

static void
rtmp_free( rtmp_t *rtmp )
{
	dbg_line();
	if ( rtmp->buff ) {
		my_free( rtmp->buff );
	}

	if ( rtmp->fd > 0 ) {
		close( rtmp->fd );
	}

	my_free( rtmp );
}

static int
rtmp_serve_publish( rtmp_t *rtmp )
{
	while ( 1 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			dbg_line( "Do rtmp_recv Failed!!!" );
			return FAILURE;
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_AUDIO:
					if ( rtmp_parse_audio( rtmp ) == FAILURE ) {
						dbg_line( "Do rtmp_parse_audio Failed!!!" );
						return FAILURE;
					}
					break;

				case RTMP_TYPE_VIDEO:
					if ( rtmp_parse_video( rtmp ) == FAILURE ) {
						dbg_line( "Do rtmp_parse_video Failed!!!" );
						return FAILURE;
					}

					break;

				default:
					dbg_line( "rtmp->msg_type_id: %d", rtmp->msg_type_id );
			}
		}
	}

	return SUCCESS;
}

static int
rtmp_serve_play( rtmp_t *rtmp )
{
	int res;

	MSleep( 100 );

	if ( rtmp->handle.send_stream == NULL ) {
		dbg_line( "Do rtmp->handle.send_stream Failed!!!" );
		return FAILURE;
	}

	while ( 1 ) {
		if ( sock_writable( rtmp->fd ) == 0 ) {
			dbg_warm( "Can not Streaming to client!!" );
			return FAILURE;
		}

		res = rtmp->handle.send_stream( rtmp, rtmp->data );
		if ( res < 0 ) {
			dbg_line( "Do handle.send_stream Failed!!!" );
			return FAILURE;
		} else if ( res == 0 ) {
			MSleep( 30 );
		}
	}

	return SUCCESS;
}

static int
rtmp_serve_handle( rtmp_t *rtmp )
{
	while ( rtmp->start == 0 ) {
		if ( rtmp_recv( rtmp ) != SUCCESS ) {
			dbg_line( "Do rtmp_recv Failed!!!" );
			return FAILURE;
			/*
		} else if ( rtmp->body_size == 0 ) {
			dbg_line( "rtmp->body_size == 0!!!" );
			return FAILURE;*/
		} else {
			switch ( rtmp->msg_type_id ) {
				case RTMP_TYPE_SET_PACKET_SIZE:
					rtmp_parse_set_chunk_size( rtmp );
					break;

				case RTMP_TYPE_WINDOW_ACK_SIZE:
					rtmp_parse_window_size( rtmp );
					break;

				case RTMP_TYPE_PEER_BANDWIDTH:
					rtmp_parse_peer_bw( rtmp );
					break;

				case RTMP_TYPE_AMF0_INVOKE:
					if ( rtmp_parse_invoke( rtmp ) == FAILURE ) {
						dbg_line( "Do rtmp_parse_invoke Failed!!!" );
						return FAILURE;
					}
					break;

				default:
					dbg_line( "rtmp->msg_type_id: %d", rtmp->msg_type_id );
			}
		}
	}

	return SUCCESS;
}

static int
rtmp_serve( void *data )
{
	rtmp_t *rtmp = NULL;
	rtmp_serve_t *rtmp_s	= ( rtmp_serve_t * )data;
	struct sockaddr_in		client_addr;
	socklen_t addrlen		= sizeof( struct sockaddr_in );

	int clientfd;

	while ( 1 ) {
		if ( sock_readable( rtmp_s->fd, 5 ) > 0 ) {
			clientfd = accept( rtmp_s->fd, ( struct sockaddr * )&client_addr, &addrlen );

			if ( clientfd < 0 ) {
				continue;
			} else if ( rtmp_s->handle.open ) {
				rtmp = ( rtmp_t * )my_malloc( sizeof( rtmp_t ) );
				if ( rtmp == NULL ) {
					dbg_error( "Do my_malloc Failed!!!" );
					close( clientfd );
					continue;
				}

				memset( rtmp, 0, sizeof( rtmp_t ) );

				rtmp->fd			= clientfd;
				rtmp->handle		= rtmp_s->handle;
				rtmp->msg_num		= 1;
				rtmp->buff_size		= CHUNK_SIZE_MAX;
				rtmp->chunk_size	= CHUNK_SIZE_DEFAULT;
				rtmp->buff			= ( u8 * )my_malloc( CHUNK_SIZE_MAX + 4 );
				if ( rtmp->buff == NULL ) {
					dbg_error( "Do my_malloc Failed!!!" );
					rtmp_free( rtmp );
					continue;
				}

				rtmp_s->handle.open( rtmp );
			} else {
				close( clientfd );
			}
		}
	}

	close( rtmp_s->fd );
	my_free( rtmp_s );

	return 0;
}

//==========================================================================
// APIs
//==========================================================================
int
RTMP_SendH264InitFrame( RTMPHandle handle, u8 *sps, u32 sps_size, u8 *pps, u32 pps_size, u32 timestamp )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp && sps && pps ) {
		u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

		*dst ++	= ( ( RTMP_VIDEO_FRAME_TYPE_KEY << 4 ) | RTMP_VIDEO_CODEC_AVC );
		*dst ++	= 0x00;
		*dst ++	= 0x00;
		*dst ++	= 0x00;
		*dst ++	= 0x00;

		// SPS
		*dst ++	= 0x01;
		*dst ++	= sps[ 1 ];
		*dst ++	= sps[ 2 ];
		*dst ++	= sps[ 3 ];
		*dst ++	= 0xFF;
		*dst ++	= 0x01;

		PUTB16( dst, sps_size );
		dst += 2;

		memcpy( dst, sps, sps_size );
		dst += sps_size;

		// PPS
		*dst ++	= 0x01;

		PUTB16( dst, pps_size );
		dst += 2;

		memcpy( dst, pps, pps_size );
		dst += pps_size;

		if ( rtmp->video_last_timestamp == 0 ) {
			rtmp->video_last_timestamp	= timestamp;
		}

# ifdef ENABLE_MULTI_STREAM_CHANNEL
		if ( rtmp->type	== RTMP_SERVE_PLAY ) {
			rtmp->chunk_stream_id		= RTMP_CHANNEL_VIDEO;
			rtmp->timestamp				= rtmp->video_last_timestamp;
		} else {
			rtmp->chunk_stream_id		= RTMP_CHANNEL_STREAM;
			rtmp->timestamp				= 0;
		}
# else
		rtmp->chunk_stream_id	= RTMP_CHANNEL_STREAM;
		rtmp->timestamp			= 0;
# endif

		rtmp->fmt				= RTMP_FMT0;
		rtmp->body_size			= dst - body;
		rtmp->msg_type_id		= RTMP_TYPE_VIDEO;

		return rtmp_send( rtmp, NULL, 0 );
	} else {
		return FAILURE;
	}
}

int
RTMP_SendH264KeyFrame( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;
	if ( rtmp && frame ) {
		return rtmp_send_h264( rtmp, frame, size, TRUE, timestamp );
	} else {
		return FAILURE;
	}
}

int
RTMP_SendH264Frame( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;
	if ( rtmp && frame ) {
		return rtmp_send_h264( rtmp, frame, size, FALSE, timestamp );
	} else {
		return FAILURE;
	}
}

int
RTMP_SetAudio( RTMPHandle handle, RTMP_AUDIO_FMT type, u32 sample_rate, u32 sample_size, u8 channels )
{
	if ( handle ) {
		rtmp_t *rtmp	= ( rtmp_t * )handle;

		rtmp->audio_fmt		= type;
		rtmp->sample_rate	= sample_rate;
		rtmp->sample_size	= sample_size;
		rtmp->channels		= channels;

		return SUCCESS;
	} else {
		return FAILURE;
	}
}

int
RTMP_SendAudio( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp )
{
# ifdef ENABLE_AUDIO
	if ( handle && frame ) {
		rtmp_t *rtmp	= ( rtmp_t * )handle;

		if ( rtmp->audio_last_timestamp == 0 ) {
			rtmp->audio_last_timestamp = timestamp;

			if ( rtmp->type == RTMP_SERVE_PLAY ) {
				if ( rtmp->handle.get_audio_fmt ) {
					if ( rtmp->handle.get_audio_fmt( &rtmp->audio_fmt, &rtmp->sample_rate, &rtmp->sample_size, &rtmp->channels, rtmp->data ) != SUCCESS ) {
						dbg_line( "Do rtmp->handle.get_audio_fmt Failed!!!" );
						return FAILURE;
					}
				}
			}

			if ( rtmp_send_audio_format( rtmp, timestamp ) == FAILURE ) {
				dbg_line( "Do rtmp_send_audio_format Failed!!!" );
				return FAILURE;
			}
		}

		return rtmp_send_audio( ( rtmp_t * )handle, frame, size, timestamp );
	} else {
		return FAILURE;
	}
# else
	return SUCCESS;
# endif
}

RTMPHandle
RTMP_Open( RTMP_CTX *ctx )
{
	rtmp_t *rtmp = NULL;

	if ( ctx == NULL ) {
		dbg_warm( "ctx == NULL" );
		return NULL;
	}

	rtmp = ( rtmp_t * )my_malloc( sizeof( rtmp_t ) );
	if ( rtmp == NULL ) {
		dbg_warm( "rtmp == NULL" );
		return NULL;
	}

	memset( rtmp, 0, sizeof( rtmp_t ) );

	rtmp->chunk_size	= CHUNK_SIZE_DEFAULT;
	rtmp->buff_size		= CHUNK_SIZE_MAX;
	rtmp->buff			= ( u8 * )my_malloc( CHUNK_SIZE_MAX + 4 );
	if ( rtmp->buff == NULL ) {
		dbg_warm( "rtmp->buff == NULL" );
		goto err;
	}

	rtmp->fd			= sock_conn( ctx->host_name, ctx->port );
	rtmp->msg_num		= 1;

	if ( rtmp->fd < 0 ) {
		dbg_warm( "sock_conn Failed!!" );
		goto err;
	} else if ( rtmp_handshark( rtmp ) < 0 ) {
		dbg_warm( "rtmp_handshark Failed!!" );
		goto err;
	} else if ( rtmp_set_chunk_size( rtmp, CHUNK_SIZE_MAX ) == FAILURE ) {
		dbg_warm( "rtmp_set_chunk_size Failed!!" );
		goto err;
	} else if ( rtmp_connect( rtmp, ctx ) == FAILURE ) {
		dbg_warm( "rtmp_connect Failed!!" );
		goto err;
	}

	if ( ctx->upload != RTMP_UPLOAD_NONE ) {
		if ( rtmp_release_stream( rtmp, ctx ) == FAILURE ) {
			dbg_warm( "rtmp_release_stream Failed!!" );
			goto err;
		} else if ( rtmp_fcpublish(rtmp, ctx) == FAILURE ) {
			dbg_warm( "rtmp_fcpublish Failed!!" );
			goto err;
		}
	}

	if ( rtmp_create_stream( rtmp, ctx ) == FAILURE ) {
		dbg_warm( "rtmp_create_stream Failed!!" );
		goto err;
	}

	if ( ctx->upload != RTMP_UPLOAD_NONE ) {
		if ( rtmp_publish( rtmp, ctx ) == FAILURE ) {
			dbg_warm( "rtmp_publish Failed!!" );
			goto err;
		} else if ( rtmp_set_chunk_size( rtmp, 0x100000 ) == FAILURE ) {
			dbg_warm( "rtmp_set_chunk_size Failed!!" );
			goto err;
		}
	}

	return rtmp;

err:
	rtmp_free( rtmp );
	return NULL;
}

RTMPHandle
RTMP_Play( RTMP_CTX *ctx, RTMP_ServeHandle *handle )
{
	rtmp_t *rtmp = NULL;

	if ( ctx == NULL ) {
		dbg_warm( "ctx == NULL" );
		return NULL;
	}

	rtmp = ( rtmp_t * )my_malloc( sizeof( rtmp_t ) );
	if ( rtmp == NULL ) {
		dbg_warm( "rtmp == NULL" );
		return NULL;
	}

	memset( rtmp, 0, sizeof( rtmp_t ) );

	rtmp->chunk_size	= CHUNK_SIZE_DEFAULT;
	rtmp->buff_size		= CHUNK_SIZE_MAX;
	rtmp->buff			= ( u8 * )my_malloc( CHUNK_SIZE_MAX + 4 );
	if ( rtmp->buff == NULL ) {
		dbg_warm( "rtmp->buff == NULL" );
		goto err;
	}

	rtmp->fd			= sock_conn( ctx->host_name, ctx->port );
	rtmp->msg_num		= 1;

	if ( rtmp->fd < 0 ) {
		dbg_warm( "sock_conn Failed!!" );
		goto err;
	} else if ( rtmp_handshark( rtmp ) < 0 ) {
		dbg_warm( "rtmp_handshark Failed!!" );
		goto err;
	} else if ( rtmp_set_chunk_size( rtmp, CHUNK_SIZE_MAX ) == FAILURE ) {
		dbg_warm( "rtmp_set_chunk_size Failed!!" );
		goto err;
	} else if ( rtmp_connect( rtmp, ctx ) == FAILURE ) {
		dbg_warm( "rtmp_connect Failed!!" );
		goto err;
	} else if ( rtmp_create_stream( rtmp, ctx ) == FAILURE ) {
		dbg_warm( "rtmp_create_stream Failed!!" );
		goto err;
	} else if ( rtmp_play( rtmp, ctx ) == FAILURE ) {
		dbg_warm( "rtmp_create_stream Failed!!" );
		goto err;
	}

	if ( handle ) {
		rtmp->handle	= *handle;
	}

	return rtmp;

err:
	rtmp_free( rtmp );
	return NULL;
}

void
RTMP_Close( RTMPHandle handle )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;
	dbg_warm();

	if ( rtmp ) {
		if ( rtmp->handle.close ) {
			rtmp->handle.close( handle, rtmp->data );
		}
		rtmp_free( rtmp );
	}
}

void
RTMP_Stop( RTMPHandle handle )
{
	if ( handle ) {
		rtmp_t *rtmp	= ( rtmp_t * )handle;
		rtmp->start	= 0;
	}
}

void
RTMP_PingRequest( RTMPHandle handle )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp ) {
		u8 *body = rtmp->buff + RTMP_HEADER_MAX, *dst = body;

		PUTB16( dst, 6 );
		dst += 2;

		PUTB32( dst, Ticks() );
		dst += 4;

		rtmp->fmt				= RTMP_FMT0;
		rtmp->chunk_stream_id	= 0x02;
		rtmp->timestamp			= 0;
		rtmp->body_size			= dst - body;
		rtmp->msg_type_id		= RTMP_TYPE_USER_CONTROL;

		if ( rtmp_send( rtmp, NULL, 0 ) != SUCCESS ) {
			dbg_warm( "Send Ping Request Failed!!!" );
		} else {
			time_t curr_time = time( NULL );
			dbg_warm( "Send Ping at %s", ctime( &curr_time ) );
		}
	}
}

int
RTMP_GetPingRequest( RTMPHandle handle )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp ) {
		if ( sock_readable( rtmp->fd, 0 ) > 0 ) {
			if ( rtmp_recv( rtmp ) != SUCCESS ) {
				dbg_warm( "Get Ping Ack Failed!!" );
			} else {
				u8 *dst = rtmp->buff;
				u16	cmd	= GETB16( dst );

				if ( cmd != 7 ) {
					dbg_warm( "Got Wrong Ack!!!" );
					return FAILURE;
				}

				return 1;
			}
		} else {
			return 0;
		}
	}

	return FAILURE;
}

int
RTMP_Bind( RTMPHandle handle, void *data )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp ) {
		rtmp->data	= data;

		return SUCCESS;
	} else {
		return FAILURE;
	}
}

int
RTMP_DoServe( RTMPHandle handle )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp ) {
		if ( rtmp_serve_handshark( rtmp ) < 0 ) {
			dbg_warm( "rtmp_serve_handshark Failed!!" );
			return FAILURE;
		} else if ( rtmp_serve_handle( rtmp ) == FAILURE ) {
			dbg_warm( "rtmp_serve_handle Failed!!" );
			return FAILURE;
		} else {
			if ( rtmp->type == RTMP_SERVE_PLAY ) {
				return rtmp_serve_play( rtmp );
			} else if ( rtmp->type == RTMP_SERVE_PUBLISH ) {
				return rtmp_serve_publish( rtmp );
			} else {
				return FAILURE;
			}
		}
	} else {
		return FAILURE;
	}
}

int
RTMP_DoRecv( RTMPHandle handle )
{
	rtmp_t *rtmp	= ( rtmp_t * )handle;

	if ( rtmp ) {
		rtmp->start	= 1;

		while ( rtmp->start ) {
			if ( sock_readable( rtmp->fd, 1 ) > 0 ) {
				if ( rtmp_recv( rtmp ) != SUCCESS ) {
					dbg_line( "Do rtmp_recv Failed!!!" );
					return FAILURE;
				} else {
					switch ( rtmp->msg_type_id ) {
						case RTMP_TYPE_AUDIO:
							if ( rtmp_parse_audio( rtmp ) == FAILURE ) {
								dbg_line( "Do rtmp_parse_audio Failed!!!" );
								return FAILURE;
							}
							break;

						case RTMP_TYPE_VIDEO:
							if ( rtmp_parse_video( rtmp ) == FAILURE ) {
								dbg_line( "Do rtmp_parse_video Failed!!!" );
								return FAILURE;
							}

							break;

						case RTMP_TYPE_AMF0_INVOKE:
							if ( rtmp_parse_invoke( rtmp ) == FAILURE ) {
								dbg_line( "Do rtmp_parse_invoke Failed!!!" );
								return FAILURE;
							}
							break;

						default:
							dbg_line( "rtmp->msg_type_id: %d", rtmp->msg_type_id );
					}
				}
			}
		}

		return SUCCESS;
	} else {
		return FAILURE;
	}
}

int
RTMP_CreateServer( uint32_t port, uint32_t max_client, RTMP_ServeHandle *handle )
{
	int fd;
	rtmp_serve_t *rtmp_s = NULL;

	if ( handle == NULL ) {
		return FAILURE;
	}

	if ( port == 0 ) {
		port = 1935;
	}

	dbg_line( "Listen: %d", port );

	fd	= sock_serve( port, max_client );
	if ( fd < 0 ) {
		return FAILURE;
	}

	dbg_warm();
	rtmp_s = ( rtmp_serve_t * )my_malloc( sizeof( rtmp_serve_t ) );
	if ( rtmp_s == NULL ) {
		close( fd );
		return FAILURE;
	}

	dbg_warm();
	memset( rtmp_s, 0, sizeof( rtmp_serve_t ) );

	rtmp_s->handle	= *handle;
	rtmp_s->fd		= fd;

	dbg_warm();
//	thread_create( rtmp_serve, rtmp_s );
	create_norm_thread( rtmp_serve, rtmp_s, 0 );

	return SUCCESS;
}

