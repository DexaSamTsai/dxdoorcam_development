# ifndef __RTMP_H__
# define __RTMP_H__

# ifdef __cplusplus
extern "C"
{
# endif

//==========================================================================
// Type Declaration
//==========================================================================
# define RTMPHandle			void *

# define RTMP_UPLOAD_NONE		0
# define RTMP_UPLOAD_LIVE		1
# define RTMP_UPLOAD_RECORD		2
# define RTMP_UPLOAD_APPEND		3

typedef enum {
	RTMP_AUDIO_FMT_LINEAR_PCM	= 0,
	RTMP_AUDIO_FMT_ADPCM,
	RTMP_AUDIO_FMT_MP3,
	RTMP_AUDIO_FMT_LINEAR_PCM_LE,
	RTMP_AUDIO_FMT_16K_MONO,
	RTMP_AUDIO_FMT_8K_MONO,
	RTMP_AUDIO_FMT_NELLYMOSER,
	RTMP_AUDIO_FMT_G711A,
	RTMP_AUDIO_FMT_G711U,

	RTMP_AUDIO_FMT_AAC		= 10,
	RTMP_AUDIO_FMT_SPEEX,
	RTMP_AUDIO_FMT_8K_MP3	= 14,
	RTMP_AUDIO_FMT_DEVICE_SPEC,
} RTMP_AUDIO_FMT;

typedef enum {
	RTMP_AUDIO_SAMPLE_RATE_5_5	= 0,
	RTMP_AUDIO_SAMPLE_RATE_11K,
	RTMP_AUDIO_SAMPLE_RATE_22K,
	RTMP_AUDIO_SAMPLE_RATE_44K,
} RTMP_AUDIO_SAMPLE_RATE;

typedef struct {
	char	*host_name;
	u16		port;

	u8		upload;

	char	*stream_name;
	char	*app;
	char	*swfUrl;
	char	*tcUrl;
} RTMP_CTX;

typedef struct {
	int				( *open )			( RTMPHandle );
	int				( *close )			( RTMPHandle, void * );
	int				( *connect )		( RTMP_CTX *, void * );
	int				( *releaseStream )	( char *, void * );
	int				( *createStream )	( char *, void * );
	int				( *publish )		( char *, void * );
	int				( *play )			( char *, void * );
	int				( *get_audio_fmt )	( RTMP_AUDIO_FMT *, u32 *, u32 *, u8 *, void * );		// RTMP_AUDIO_FMT type, u32 sample_rate, u32 sample_size, u8 channels
	int				( *send_stream )	( RTMPHandle, void * );
	int				( *video_in )		( u8 *, u32, void * );
	int				( *audio_in )		( u8 *, u32, void * );
} RTMP_ServeHandle;

//==========================================================================
// APIs
//==========================================================================
extern RTMPHandle
RTMP_Open( RTMP_CTX *ctx );

extern RTMPHandle
RTMP_Play( RTMP_CTX *ctx, RTMP_ServeHandle *handle );

extern void
RTMP_Stop( RTMPHandle handle );

extern int
RTMP_SendH264InitFrame( RTMPHandle handle, u8 *sps, u32 sps_size, u8 *pps, u32 pps_size, u32 timestamp );

extern int
RTMP_SendH264KeyFrame( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp );

extern int
RTMP_SendH264Frame( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp );

extern int
RTMP_SetAudio( RTMPHandle handle, RTMP_AUDIO_FMT type, u32 sample_rate, u32 sample_size, u8 channels );

extern int
RTMP_SendAudio( RTMPHandle handle, u8 *frame, u32 size, u32 timestamp );

extern void
RTMP_Close( RTMPHandle handle );

extern int
RTMP_Bind( RTMPHandle handle, void *data );

extern int
RTMP_DoServe( RTMPHandle handle );

extern int
RTMP_DoRecv( RTMPHandle handle );

extern int
RTMP_CreateServer( uint32_t port, uint32_t max_client, RTMP_ServeHandle *handle );

# ifdef __cplusplus
};
# endif

# endif
