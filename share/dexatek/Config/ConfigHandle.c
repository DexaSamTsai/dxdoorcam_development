/*!
 *  @file       Config.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "ConfigHandle.h"
# include "IPCamUtils.h"

//==========================================================================
// Type Declaration
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static u64	invalid_field	= ( u64 )0xFFFFFFFFFFFFFFFF;

//==========================================================================
// Static Functions
//==========================================================================
static void
cfg_utf8_to_unicode( u8 *utf8, u16 *unicode, int size )
{
	u32		temp;
	int		cnt = 0;

#define IS_UTF8( x )  ( x >= 0x80 && x <= 0xBF )

	while ( utf8[ 0 ] && ( cnt + 1 < size ) ) {
		if ( utf8[ 0 ] >= 0xF0 ) {			// Bigger then 0x0000FFFF
			unicode[ cnt ++ ] = utf8[ 0 ];
			utf8 ++;
		} else if ( utf8[ 0 ] >= 0xE0 ) {
			temp = utf8[ 0 ];
			utf8 ++;

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			unicode[ cnt ++ ] = ( ( temp & 0x000F0000 ) >> 4 ) | ( ( temp & 0x00003F00 ) >> 2 ) | ( temp & 0x3F );
		} else if ( utf8[ 0 ] >= 0xC0 ) {
			temp = utf8[ 0 ];
			utf8 ++;

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			unicode[ cnt ++ ] = ( ( temp & 0x00001F00 ) >> 2 ) | ( temp & 0x3F );
		} else {
			unicode[ cnt ++ ] = utf8[ 0 ];
			utf8 ++;
		}
	}
	unicode[ cnt ++ ] = 0;
}

static int
cfg_unicode_to_utf8( u16 *unicode, char *utf8, int size )
{
	u32		temp;
	char	*u8_dst = utf8;
	int		cnt = 0;

	while ( unicode[ 0 ] && ( size > 1 ) ) {
		cnt = 0;

		if ( unicode[ 0 ] == 0x20 ) {			// Bigger then 0x0000FFFF
			u8_dst[ cnt ++ ] = unicode[ 0 ];
		} else if ( unicode[ 0 ] < 0x80 ) {
			u8_dst[ cnt ++ ] = unicode[ 0 ];
		} else if ( unicode[ 0 ] < 0x800 ) {
			temp = ( ( 0xC0 | ( unicode[ 0 ] >> 6 ) ) << 8 ) | ( 0x80 | ( unicode[ 0 ] & 0x3F ) );

			u8_dst[ cnt ++ ] = ( temp >> 8 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp & 0xFF );
		} else {
			temp = ( ( 0xE0 | ( unicode[ 0 ] >> 12 ) ) << 16 ) | ( ( 0x80 | ( ( unicode[ 0 ] >> 6 ) & 0x3F ) ) << 8 ) | ( 0x80 | ( unicode[ 0 ] & 0x3F ) );

			u8_dst[ cnt ++ ] = ( temp >> 16 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp >> 8 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp & 0xFF );
		}

		u8_dst += cnt;
		size -= cnt;
		unicode ++;
	}
	u8_dst[ 0 ] = 0;

	return ( u8_dst - utf8 );
}

static void
cfg_input_int( void *pos, i64 value, int value_len )
{
	if ( value_len == sizeof( i8 ) ) {
		i8 *param = ( i8 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( i16 ) ) {
		i16 *param = ( i16 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( i32 ) ) {
		i32 *param = ( i32 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( i64 ) ) {
		i64 *param = ( i64 * )pos;
		param[ 0 ] = value;
	}
}

static void
cfg_input_uint( void *pos, u64 value, int value_len )
{
	if ( value_len == sizeof( u8 ) ) {
		u8 *param = ( u8 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( u16 ) ) {
		u16 *param = ( u16 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( u32 ) ) {
		u32 *param = ( u32 * )pos;
		param[ 0 ] = value;
	} else if ( value_len == sizeof( u64 ) ) {
		u64 *param = ( u64 * )pos;
		param[ 0 ] = value;
	}
}

static i64
cfg_output_int( void *pos, int value_len )
{
	if ( value_len == sizeof( i8 ) ) {
		i8 *param = ( i8 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( i16 ) ) {
		i16 *param = ( i16 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( i32 ) ) {
		i32 *param = ( i32 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( i64 ) ) {
		i64 *param = ( i64 * )pos;
		return param[ 0 ];
	}
	return 0;
}

static u64
cfg_output_uint( void *pos, int value_len )
{
	if ( value_len == sizeof( u8 ) ) {
		u8 *param = ( u8 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( u16 ) ) {
		u16 *param = ( u16 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( u32 ) ) {
		u32 *param = ( u32 * )pos;
		return param[ 0 ];
	} else if ( value_len == sizeof( u64 ) ) {
		u64 *param = ( u64 * )pos;
		return param[ 0 ];
	}
	return 0;
}

static int
cfg_mapping_field( json_value *json, cfg_entry_t *entry, void *cfg )
{
	if ( ( json->type == JSON_NONE ) ||
		 ( json->type == JSON_OBJECT ) ||
		 ( json->type == JSON_NULL ) ) {
		dbg_error( "Type Error!!!" );
		return -1;
	}

	if ( entry->type == CFG_TYPE_INT ) {
		if ( json->type == JSON_STRING ) {
			cfg_input_int( ( cfg + entry->offset ), strtol( json->v.string, NULL, 10 ), entry->value_len );
		} else if ( json->type == JSON_NUMBER ) {
			cfg_input_int( ( cfg + entry->offset ), ( i64 )json->v.number, entry->value_len );
		} else {
			dbg_error( "Type Error!!!" );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_UINT ) {
		if ( json->type == JSON_STRING ) {
			cfg_input_uint( ( cfg + entry->offset ), strtoul( json->v.string, NULL, 10 ), entry->value_len );
		} else if ( json->type == JSON_NUMBER ) {
			cfg_input_uint( ( cfg + entry->offset ), ( u64 )json->v.number, entry->value_len );
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_BOOLEAN ) {
		if ( json->type == JSON_BOOLEAN ) {
			cfg_input_int( ( cfg + entry->offset ), json->v.boolean, entry->value_len );
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_FLOAT ) {
		float *param = ( float * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			param[ 0 ] = atof( json->v.string );
		} else if ( json->type == JSON_NUMBER ) {
			param[ 0 ] = json->v.number;
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_STRING ) {
		char *param = ( char * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			if ( json->v.string ) {
				snprintf( param, ( entry->value_len - 1 ), "%s", json->v.string );
			} else {
				param[ 0 ] = 0;
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_PASSWORD ) {
		char *param = ( char * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			if ( json->v.string ) {
				int data_len;
				u8	base64[ 128 ];

				data_len = Decode64( json->v.string, base64, 128 );
				base64[ data_len ] = 0;

				snprintf( param, entry->value_len, "%s", ( char * )base64 );
			} else {
				param[ 0 ] = 0;
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_U16STRING ) {
		u16 *param = ( u16 * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			if ( json->v.string ) {
				cfg_utf8_to_unicode( ( u8 * )json->v.string, param, entry->value_len );
			} else {
				param[ 0 ] = 0;
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_MAC ) {
		u8 *mac = ( u8 * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			if ( json->v.string ) {
				str_parse_mac( json->v.string, mac );
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_IPADDR ) {
		struct in_addr *ip = ( struct in_addr * )( cfg + entry->offset );

		if ( json->type == JSON_STRING ) {
			if ( json->v.string ) {
				inet_aton( json->v.string, ip );
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_ARRAY ) {
		if ( json->type == JSON_STRING ) {
			if ( entry->array ) {
				int i;

				for ( i = 0; i < entry->array_len; i ++ ) {
					if ( strcmp( entry->array[ i ], json->v.string ) == 0 ) {
						cfg_input_int( ( cfg + entry->offset ), i, entry->value_len );
						break;
					}
				}

				if ( i == entry->array_len ) {
					return -1;
				}
			}
		} else {
			dbg_error( "Type Error( %d )!!!", json->type );
			return -1;
		}
	} else if ( entry->type == CFG_TYPE_DATE ) {

	} else if ( entry->type == CFG_TYPE_TIME ) {

	} else if ( entry->type == CFG_TYPE_DATETIME ) {

	} else if ( entry->type == CFG_TYPE_CB ) {
		if ( entry->cb ) {
			if ( json->type == JSON_STRING ) {
				if ( entry->cb( CFG_ACT_UPDATE, NULL, 0, NULL, entry, json->v.string, 0 ) < 0 ) {
					dbg_line( "entry->cb Failed" );
					return -1;
				}
			} else {
				dbg_error( "Type Error( %d )!!!", json->type );
				return -1;
			}
		}
	} else {
		dbg_error( "Unknow entry->type = %d", entry->type );
		return -1;
	}

	return 0;
}

static int
cfg_start_parser( json_value *json, cfg_entry_t	*entry, void *cfg )
{
	int i, update = 0, res;
	cfg_entry_t	*start;
	json_value *item;

	if ( ( json == NULL ) || ( entry == NULL ) ){
		dbg_error( "( json == NULL ) || ( entry == NULL )" );
		return -1;
	}

	switch ( json->type ) {
		case JSON_NONE:
			dbg_error( "JSON_NONE" );
			break;

		case JSON_OBJECT:
			item = json->v.child;

			while ( item ) {
				start	= entry;

				while ( start->name ) {
					if ( memcmp( item->name, start->name, start->len ) == 0 ) {
						if ( start->type == CFG_TYPE_NONE ) {
							res = cfg_start_parser( item, ( cfg_entry_t * )start->offset, cfg );
							if ( res > 0 ) {
								update += res;
							}
						} else {
							if ( cfg_mapping_field( item, start, cfg ) == SUCCESS ) {
								update ++;
							}
						}
						break;
					}

					start ++;
				}

				item = item->next;
				i ++;
			}

			return update;

		case JSON_ARRAY:
			dbg_error( "JSON_ARRAY" );
			break;

		case JSON_NUMBER:
			dbg_error( "JSON_NUMBER" );
			break;

		case JSON_STRING:
			dbg_error( "JSON_STRING" );
			break;

		case JSON_BOOLEAN:
			dbg_error( "JSON_BOOLEAN" );
			break;

		case JSON_NULL:
			dbg_error( "JSON_NULL" );
			break;

		default:
			dbg_error( "Unknow Type" );
			break;

	}

	return -1;
}

static int
cfg_parser_json( char *data, u32 size, cfg_entry_t *entry, void *cfg )
{
	json_value	*json	= Json_Parse( data, size, NULL, 128 );

	if ( json ) {
		int changed = cfg_start_parser( json, entry, cfg );
		Json_Free( json );
		if ( changed > 0 ) {
			return changed;
		} else {
			return 0;
		}
		return 0;
	} else {
		dbg_line( "Do json_parse Failed!!!" );
		return FAILURE;
	}
}

static int
cfg_generate_json( cfg_entry_t *entry, void *cfg, char *dst, int size, int with_options )
{
	int	len		= 0;
	int total	= 0;

#define dst_shift() {	\
						size	-= len;	\
						dst		+= len;	\
						total	+= len;	\
					}

	while ( entry->name && ( size > 0 ) ) {
		len = 0;

		if ( ( entry->type == CFG_TYPE_NONE ) ) {
			len = snprintf( dst, size, "\"%s\": {\n", entry->name );
			dst_shift();

			len = cfg_generate_json( ( cfg_entry_t * )entry->offset, cfg, dst, size, with_options );
			if ( len < 0 ) {
				return -1;
			}
			dst_shift();
			if ( dst[ -1 ] == ',' ) {
				dst[ -1 ] = '\n';
			}
			len = snprintf( dst, size, "}," );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_INT ) {
			if ( memcmp( ( cfg + entry->offset ), &invalid_field, entry->value_len ) ) {
				len = snprintf( dst, size, "\"%s\": %lld,", entry->name, cfg_output_int( ( cfg + entry->offset ), entry->value_len ) );
				dst_shift();
			}
		} else if ( entry->type == CFG_TYPE_UINT ) {
			if ( memcmp( ( cfg + entry->offset ), &invalid_field, entry->value_len ) ) {
				len = snprintf( dst, size, "\"%s\": %llu,", entry->name, cfg_output_uint( ( cfg + entry->offset ), entry->value_len ) );
				dst_shift();
			}
		} else if ( entry->type == CFG_TYPE_FLOAT ) {
			if ( memcmp( ( cfg + entry->offset ), &invalid_field, entry->value_len ) ) {
				float *value = ( float * )( cfg + entry->offset );
				len = snprintf( dst, size, "\"%s\": \"%0.01f\",", entry->name, value[ 0 ] );
				dst_shift();
			}
		} else if ( entry->type == CFG_TYPE_STRING ) {
			char *value = ( char * )( cfg + entry->offset );
			len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, value );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_PASSWORD ) {
			char *value = ( char * )( cfg + entry->offset );
			char	base64[ 128 ];

			len = snprintf( dst, size, "\"%s\": \",", entry->name );
			dst_shift();

			Encode64( ( u8 * )base64, 128, value );
			len = snprintf( dst, size, "%s", base64 );
			dst_shift();

			len = snprintf( dst, size, "\"," );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_U16STRING ) {
			u16 *value = ( u16 * )( cfg + entry->offset );

			len = snprintf( dst, size, "\"%s\": \"", entry->name );
			dst_shift();

			len = cfg_unicode_to_utf8( value, dst, size );
			dst_shift();

			len = snprintf( dst, size, "\"," );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_IPADDR ) {
			struct in_addr *ip = ( struct in_addr * )( cfg + entry->offset );
			len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, inet_ntoa( ip[ 0 ] ) );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_MAC ) {
			u8 *mac = ( u8 * )( cfg + entry->offset );
			len = snprintf( dst, size, "\"%s\": \"%02X:%02X:%02X:%02X:%02X:%02X\",", entry->name, mac[ 0 ], mac[ 1 ], mac[ 2 ], mac[ 3 ], mac[ 4 ], mac[ 5 ] );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_ARRAY ) {
			u32 value = cfg_output_int( ( cfg + entry->offset ), entry->value_len );

			if ( entry->array && ( value <= entry->array_len ) ) {
				len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, entry->array[ value ] );
				dst_shift();
			} else {
				dbg_line( "value[ 0 ]( %u ) > entry->array_len( %d )", value, entry->array_len );
				len = snprintf( dst, size, "\"%s\": \"\",", entry->name );
				dst_shift();
			}
		} else if ( entry->type == CFG_TYPE_DATE ) {
			u32 *value = ( u32 * )( cfg + entry->offset );
			char tmp_string[ 36 ];

			struct tm t;

			localtime_r( ( time_t * )value, &t );

			len = snprintf( tmp_string, 36, "%4u-%02u-%02u", ( 1900 + t.tm_year ), t.tm_mon + 1, t.tm_mday );
			len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, tmp_string );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_TIME ) {
			u32 *value = ( u32 * )( cfg + entry->offset );
			char tmp_string[ 36 ];
			struct tm t;

			localtime_r( ( time_t * )value, &t );

			len = snprintf( tmp_string, 36, "%02u:%02u:%02u", t.tm_hour, t.tm_min, t.tm_sec );
			len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, tmp_string );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_DATETIME ) {
			u32 *value = ( u32 * )( cfg + entry->offset );
			char tmp_string[ 36 ];

			struct tm t;

			localtime_r( ( time_t * )value, &t );

			len = snprintf( tmp_string, 36, "%4u-%02u-%02u %02u:%02u:%02u", ( 1900 + t.tm_year ), t.tm_mon + 1, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec );
			len = snprintf( dst, size, "\"%s\": \"%s\",", entry->name, tmp_string );
			dst_shift();
		} else if ( entry->type == CFG_TYPE_CB ) {
			if ( entry->cb ) {
//					len = entry->cb( CFG_ACT_LIST, dst, size, parent, entry, NULL, with_options );
				if ( len < 0 ) {
					return -1;
				}
				dst_shift();
			}
		} else if ( entry->type == CFG_TYPE_BOOLEAN ) {
			len = snprintf( dst, size, "\"%s\": %s,", entry->name, ( cfg_output_int( ( cfg + entry->offset ), entry->value_len ) )? "true": "false" );
			dst_shift();
		} else {
			dbg_error( "entry->type = %d", entry->type );
//			return -1;
		}

		entry ++;
	}

	if ( dst[ -1 ] == ',' ) {
		dst[ -1 ] = '\n';
	}

	return total;
}

//==========================================================================
// APIs
//==========================================================================
int
CFG_ParserTxt( char *txt, int size, cfg_entry_t *entry, void *cfg )
{
	if ( txt && entry && cfg ) {
		return cfg_parser_json( txt, size, entry, cfg );
	} else {
		dbg_line( "!( path && entry && cfg )" );
		return FAILURE;
	}
}

int
CFG_GenTxt( cfg_entry_t *entry, void *cfg, char *dst, int size )
{
	if ( dst && entry && cfg ) {
		return cfg_generate_json( entry, cfg, dst, size, 0 );
	} else {
		dbg_line( "!( path && entry && cfg )" );
		return FAILURE;
	}
}

