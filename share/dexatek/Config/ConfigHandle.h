/*!
 *  @file      	ConfigHandle.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __CONFIG_HANDLE_H__
# define __CONFIG_HANDLE_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
enum {
	CFG_TYPE_NONE	= 0,
	CFG_TYPE_BOOLEAN,
	CFG_TYPE_INT,
	CFG_TYPE_UINT,
	CFG_TYPE_FLOAT,
	CFG_TYPE_STRING,
	CFG_TYPE_PASSWORD,
	CFG_TYPE_U16STRING,
	CFG_TYPE_IPADDR,
	CFG_TYPE_MAC,
	CFG_TYPE_DATE,
	CFG_TYPE_TIME,
	CFG_TYPE_DATETIME,
	CFG_TYPE_ARRAY,
	CFG_TYPE_CB,
	CFG_TYPE_MAX
};

enum {
	CFG_CTRL_NONE	= 0,
	CFG_CTRL_RDONLY,
	CFG_CTRL_WRONLY,
	CFG_CTRL_RDWR,
	CFG_CTRL_MAX,
};

enum {
	CFG_ACT_NONE	= 0,
	CFG_ACT_LIST,
	CFG_ACT_UPDATE,
	CFG_ACT_MAX,
};

typedef struct {
	char			*name;
	u16				len;
	u16				type;
	unsigned long	offset;
	u16				value_len;
	int				min;
	int				max;
	char			**array;
	u32				array_len;
	int				( *cb )	( int, char *, int, char *, void *, char *, int );
} cfg_entry_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
CFG_ParserTxt( char *txt, int size, cfg_entry_t *entry, void *cfg );

extern int
CFG_GenTxt( cfg_entry_t *entry, void *cfg, char *dst, int size );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif		// __CONFIG_HANDLE_H__
