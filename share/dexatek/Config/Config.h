/*!
 *  @file      	Config.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __CONFIG_H__
# define __CONFIG_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Camera.h"

//==========================================================================
// Type Define
//==========================================================================
# define CFG_VERSION			"1.0"

# define MAX_MD_SUPPORTED		CAMERA_MD_MAX
# define MAX_MASK_SUPPORTED		1

typedef struct {
	char 				url[ MAX_URI_LENGTH + 1 ];
	u32					port;
	u32					w;
	u32					h;
	u32					fps;
	u32					gop;
	CAMERA_BITRATECTRL	bitrate_ctrl;
	u8					quality;
	u32					bitrate;
	CAMERA_PROFILE		profile_type;
	u32					crc;
} CFG_Stream;

typedef struct {
	CFG_Stream			recording;
	CFG_Stream			live;
	CFG_Stream			snapshot;
} CFG_Video;

typedef struct {
	int					gain;
	int					power_freq;

	int					roi;

	u8					dnr3;
	u8					dehaze;

	struct {
		CAMERA_SWITCH	mode;
		u8				level;
	} blc;

	struct {
		int				brightness;
		int				contrast;
		int				hue;
		int				saturation;
		int				sharpness;
		int				gamma;
		u8				gray_mode;
	} color;

	struct {
		int				roll;
		u8				flip;
		u8				mirror;
		u32				rotate;
		u32				crc;
	} pic;

	struct {
		CAMERA_AWB_MODE		mode;
		u32					level;
		u32					r_gain;
		u32					g_gain;
		u32					b_gain;
	} awb;

	struct {
		CAMERA_SWITCH		mode;
		u32					level;
	} wdr;

	struct {
		CAMERA_AE_MODE		mode;
		u32					gain;
		u32					exposure_time;
	} ae;

	struct {
		CAMERA_IRCUT_MODE	mode;
	} ir_cut;

} CFG_Image;

typedef struct {
	u8		enable;
	u32		x;
	u32		y;
	u32		width;
	u32		height;
	u32		percentage;
} CFG_Area;

typedef struct {
	CFG_Area	area[ MAX_MD_SUPPORTED ];
	u32			sensitivity;
} CFG_MD;

typedef struct {
	u8		enable;
	u8		volume;
} CFG_Audio;

typedef struct {
	u8		show_time;
	u8		show_date;
	u8		show_text;
	char	string[ MAX_NAME_LENGTH + 1 ];
} CFG_OSD;

typedef struct {
	u8					key[ MAX_NAME_LENGTH + 1 ];
	u32					key_len;
} CFG_Security;

typedef struct {
	char				version[ MAX_NAME_LENGTH + 1 ];

	CFG_Security		security;


	CFG_Video			video;
	CFG_Image			image;
	CFG_MD				md;
	CFG_Area			mask[ MAX_MASK_SUPPORTED ];

	u32					md_crc;
	CFG_Audio			audio;
	CFG_OSD				osd;
} CFG_CameraCtx;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
CFG_Save( CFG_CameraCtx *ctx );

extern int
CFG_Load( CFG_CameraCtx *ctx );

extern int
CFG_Generate( CFG_CameraCtx *ctx, char **data );

extern int
CFG_Parser( char *data, u32 data_size, CFG_CameraCtx *ctx );

extern int
CFG_LoadDefault( CFG_CameraCtx *ctx );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif
