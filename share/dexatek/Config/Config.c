/*!
 *  @file       Config.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Config.h"
# include "ConfigHandle.h"

//==========================================================================
// Type Declaration
//==========================================================================
# define FIELD_INFO( field )		( ( unsigned long )&( ( ( CFG_CameraCtx * )0 )->field ) ), sizeof( ( ( CFG_CameraCtx * )0 )->field )

# define FILL_STREAM( s ) \
	{ "Path",			4,	CFG_TYPE_STRING,	FIELD_INFO( s.url ),				0,	0,				NULL,				0,	NULL, },	\
	{ "Port",			4,	CFG_TYPE_UINT,		FIELD_INFO( s.port ),				0,	1024,			NULL,				0,	NULL, },	\
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( s.w ),					0,	1024,			NULL,				0,	NULL, },	\
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( s.h ),					0,	1024,			NULL,				0,	NULL, },	\
	{ "BitrateCtrl",	11,	CFG_TYPE_ARRAY,		FIELD_INFO( s.bitrate_ctrl ),		0,	1024,			str_bitrate_ctrl,	CAMERA_BITRATECTRL_MAX,	NULL, },	\
	{ "Bitrate",		7,	CFG_TYPE_UINT,		FIELD_INFO( s.bitrate ),			0,	1024000,		NULL,				0,	NULL, },	\
	{ "Quality",		7,	CFG_TYPE_UINT,		FIELD_INFO( s.quality ),			0,	1024,			NULL,				0,	NULL, },	\
	{ "FPS",			3,	CFG_TYPE_UINT,		FIELD_INFO( s.fps ),				0,	1024,			NULL,				0,	NULL, },	\
	{ "GOP",			3,	CFG_TYPE_UINT,		FIELD_INFO( s.gop ),				0,	1024,			NULL,				0,	NULL, },	\
	{ "EncProfileType",	14,	CFG_TYPE_UINT,		FIELD_INFO( s.profile_type ),		0,	1024,			NULL,				0,	NULL, },	\
	{ "CRC",			3,	CFG_TYPE_UINT,		FIELD_INFO( s.crc ),				0,	0xFFFFFFFF,		NULL,				0,	NULL, },	\
	{ NULL,				0,	CFG_TYPE_NONE,		0,					0, 				0,	0,				NULL,				0,	NULL, },	\

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static char *str_bitrate_ctrl[ CAMERA_BITRATECTRL_MAX ]		= { "VBR", "CBR", "CVBR" };
static char *str_blc_mode[ CAMERA_SWITCH_MAX ]				= { "Off", "On", };
static char *str_wdr_mode[ CAMERA_SWITCH_MAX ]				= { "Off", "On", };

static char *str_exposure_mode[ CAMERA_AE_MODE_MAX ]		= { "Auto", "Manual", };
static char *str_wb_mode[ CAMERA_AWB_MODE_MAX ]				= { "Temperature", "Auto", "Component" };
static char *str_ir_cut_mode[ CAMERA_IRCUT_MODE_MAX ]		= { "Off", "Auto", "Manual", };
static u8	cfg_buff[ CONFIG_DXDOORCAM_CONF_SIZE + 4 ];

static cfg_entry_t	cfg_recording[] = {
	FILL_STREAM( video.recording )
};

static cfg_entry_t	cfg_live[] = {
	FILL_STREAM( video.live )
};

static cfg_entry_t	cfg_snapshot[] = {
	{ "Path",			4,	CFG_TYPE_STRING,	FIELD_INFO( video.snapshot.url ),		0,	0,			NULL,	0,	NULL, },
	{ "Port",			4,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.port ),		0,	1024,		NULL,	0,	NULL, },
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.w ),			0,	1024,		NULL,	0,	NULL, },
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.h ),			0,	1024,		NULL,	0,	NULL, },
	{ "Bitrate",		7,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.bitrate ),	0,	1024000,	NULL,	0,	NULL, },
	{ "Quality",		7,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.quality ),	0,	1024,		NULL,	0,	NULL, },
	{ "FPS",			3,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.fps ),		0,	1024,		NULL,	0,	NULL, },
	{ "CRC",			3,	CFG_TYPE_UINT,		FIELD_INFO( video.snapshot.crc ),		0,	0xFFFFFFFF,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,					0, 					0,	0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_security[] = {
	{ "Key",			3,	CFG_TYPE_STRING,	FIELD_INFO( security.key ),				0,	0,	NULL,	0,	NULL, },
	{ "KeyLen",			6,	CFG_TYPE_UINT,		FIELD_INFO( security.key_len ),			0,	0,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,					0, 					0,	0,	NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_video[] = {
	{ "Recording",		9,	CFG_TYPE_NONE,		( unsigned long )cfg_recording,	0,	0,	0,	NULL,	0,	NULL, },
	{ "Live",			4,	CFG_TYPE_NONE,		( unsigned long )cfg_live,		0,	0,	0,	NULL,	0,	NULL, },
	{ "Snapshot",		8,	CFG_TYPE_NONE,		( unsigned long )cfg_snapshot,	0,	0,	0,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 	0,	0,	NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_color[] = {
	{ "Brightness",		10,	CFG_TYPE_INT,		FIELD_INFO( image.color.brightness ),	0,		0,		NULL,	0,	NULL, },
	{ "Contrast",		8,	CFG_TYPE_INT,		FIELD_INFO( image.color.contrast ),		0,		0,		NULL,	0,	NULL, },
	{ "Hue",			3,	CFG_TYPE_INT,		FIELD_INFO( image.color.hue ),			0,		0,		NULL,	0,	NULL, },
	{ "Saturation",		10,	CFG_TYPE_INT,		FIELD_INFO( image.color.saturation ),	0,		0,		NULL,	0,	NULL, },
	{ "Sharpness",		9,	CFG_TYPE_INT,		FIELD_INFO( image.color.sharpness ),	0,		0,		NULL,	0,	NULL, },
	{ "Gamma",			5,	CFG_TYPE_INT,		FIELD_INFO( image.color.gamma ),		0,		0,		NULL,	0,	NULL, },
	{ "GrayMode",		8,	CFG_TYPE_BOOLEAN,	FIELD_INFO( image.color.gray_mode ),	0,		0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 						0,		0,		NULL,	0,	NULL, },
};


static cfg_entry_t	cfg_pic[] = {
	{ "Flip",			4,	CFG_TYPE_BOOLEAN,	FIELD_INFO( image.pic.flip ),			0,	1,			NULL,	0,	NULL, },
	{ "Mirror",			6,	CFG_TYPE_BOOLEAN,	FIELD_INFO( image.pic.mirror ),			0,	1,			NULL,	0,	NULL, },
	{ "Rotation",		8,	CFG_TYPE_UINT,		FIELD_INFO( image.pic.rotate ),			0,	360,		NULL,	0,	NULL, },
	{ "Roll",			4,	CFG_TYPE_UINT,		FIELD_INFO( image.pic.roll ),			0,	360,		NULL,	0,	NULL, },
	{ "CRC",			3,	CFG_TYPE_UINT,		FIELD_INFO( image.pic.crc ),			0,	0xFFFFFFFF,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 						0,	0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_awb[] = {
	{ "Mode",			4,	CFG_TYPE_ARRAY,		FIELD_INFO( image.awb.mode ),	0,		0,		str_wb_mode,	CAMERA_AWB_MODE_MAX,	NULL, },
	{ "Level",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.awb.level ),	0,		0,		NULL,	0,	NULL, },
	{ "RGain",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.awb.r_gain ),	0,		0,		NULL,	0,	NULL, },
	{ "GGain",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.awb.g_gain ),	0,		0,		NULL,	0,	NULL, },
	{ "BGain",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.awb.b_gain ),	0,		0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 				0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_wdr[] = {
	{ "Mode",			4,	CFG_TYPE_ARRAY,		FIELD_INFO( image.wdr.mode ),	0,		0,		str_wdr_mode,	CAMERA_SWITCH_MAX,	NULL, },
	{ "Level",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.wdr.level ),	0,		0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 				0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_ae[] = {
	{ "Mode",			4,	CFG_TYPE_ARRAY,		FIELD_INFO( image.ae.mode ),				0,		0,		str_exposure_mode,		CAMERA_AE_MODE_MAX,	NULL, },
	{ "ExposureTime",	12,	CFG_TYPE_UINT,		FIELD_INFO( image.ae.exposure_time ),		0,		0,		NULL,	0,	NULL, },
	{ "Gain",			4,	CFG_TYPE_UINT,		FIELD_INFO( image.ae.gain ),				0,		0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,							0, 				0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_blc[] = {
	{ "Mode",			4,	CFG_TYPE_ARRAY,		FIELD_INFO( image.blc.mode ),	0,		0,		str_blc_mode,	CAMERA_SWITCH_MAX,	NULL, },
	{ "Level",			5,	CFG_TYPE_UINT,		FIELD_INFO( image.blc.level ),	0,		0,		NULL,			0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 				0,		0,		NULL,			0,	NULL, },
};

static cfg_entry_t	cfg_ir_cut[] = {
	{ "Mode",			4,	CFG_TYPE_ARRAY,		FIELD_INFO( image.ir_cut.mode ),		0,		0,		str_ir_cut_mode,	CAMERA_IRCUT_MODE_MAX,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,				0, 						0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_image[] = {
	{ "3DNR",			4,	CFG_TYPE_BOOLEAN,	FIELD_INFO( image.dnr3 ),				0,	1,		NULL,	0,	NULL, },
	{ "Dehaze",			6,	CFG_TYPE_BOOLEAN,	FIELD_INFO( image.dehaze ),				0,	1,		NULL,	0,	NULL, },

	{ "Color",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_color,		0,		0,	0,		NULL,	0,	NULL, },
	{ "Pic",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_pic,		0,		0,	0,		NULL,	0,	NULL, },
	{ "BLC",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_blc,		0,		0,	0,		NULL,	0,	NULL, },
	{ "IRCut",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_ir_cut,	0,		0,	0,		NULL,	0,	NULL, },
	{ "WDR",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_wdr,		0,		0,	0,		NULL,	0,	NULL, },
	{ "AWB",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_awb,		0,		0,	0,		NULL,	0,	NULL, },
	{ "AE",				2,	CFG_TYPE_NONE,		( unsigned long )cfg_ae,		0,		0,	0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,	0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_md0[] = {
	{ "Enabled",		7,	CFG_TYPE_BOOLEAN,	FIELD_INFO( md.area[ 0 ].enable ),		0,		1,			NULL,	0,	NULL, },
	{ "X",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 0 ].x ),			0,		0,			NULL,	0,	NULL, },
	{ "Y",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 0 ].y ),			0,		0,			NULL,	0,	NULL, },
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 0 ].width ),		0,		0,			NULL,	0,	NULL, },
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 0 ].height ),		0,		0,			NULL,	0,	NULL, },
	{ "Percentage",		10,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 0 ].percentage ),	0,		0,			NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,		0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_md1[] = {
	{ "Enabled",		7,	CFG_TYPE_BOOLEAN,	FIELD_INFO( md.area[ 1 ].enable ),		0,		1,			NULL,	0,	NULL, },
	{ "X",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 1 ].x ),			0,		0,			NULL,	0,	NULL, },
	{ "Y",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 1 ].y ),			0,		0,			NULL,	0,	NULL, },
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 1 ].width ),		0,		0,			NULL,	0,	NULL, },
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 1 ].height ),		0,		0,			NULL,	0,	NULL, },
	{ "Percentage",		10,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 1 ].percentage ),	0,		0,			NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,		0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_md2[] = {
	{ "Enabled",		7,	CFG_TYPE_BOOLEAN,	FIELD_INFO( md.area[ 2 ].enable ),		0,		1,			NULL,	0,	NULL, },
	{ "X",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 2 ].x ),			0,		0,			NULL,	0,	NULL, },
	{ "Y",				1,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 2 ].y ),			0,		0,			NULL,	0,	NULL, },
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 2 ].width ),		0,		0,			NULL,	0,	NULL, },
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 2 ].height ),		0,		0,			NULL,	0,	NULL, },
	{ "Percentage",		10,	CFG_TYPE_UINT,		FIELD_INFO( md.area[ 2 ].percentage ),	0,		0,			NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,		0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_md[] = {
	{ "MD0",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_md0,		0,		0,	0,			NULL,	0,	NULL, },
	{ "MD1",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_md1,		0,		0,	0,			NULL,	0,	NULL, },
	{ "MD2",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_md2,		0,		0,	0,			NULL,	0,	NULL, },
	{ "Sensitivity",	11,	CFG_TYPE_UINT,		FIELD_INFO( md.sensitivity ),			0,	0,			NULL,	0,	NULL, },
	{ "CRC",			3,	CFG_TYPE_UINT,		FIELD_INFO( md_crc ),					0,	0xFFFFFFFF,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,	0,			NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_mask0[] = {
	{ "Enabled",		7,	CFG_TYPE_BOOLEAN,	FIELD_INFO( mask[ 0 ].enable ),		0,		1,		NULL,	0,	NULL, },
	{ "X",				1,	CFG_TYPE_UINT,		FIELD_INFO( mask[ 0 ].x ),			0,		0,		NULL,	0,	NULL, },
	{ "Y",				1,	CFG_TYPE_UINT,		FIELD_INFO( mask[ 0 ].y ),			0,		0,		NULL,	0,	NULL, },
	{ "Width",			5,	CFG_TYPE_UINT,		FIELD_INFO( mask[ 0 ].width ),		0,		0,		NULL,	0,	NULL, },
	{ "Height",			6,	CFG_TYPE_UINT,		FIELD_INFO( mask[ 0 ].height ),		0,		0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,							0, 		0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_mask[] = {
	{ "Mask0",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_mask0,		0,		0,	0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0, 		0,	0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_audio[] = {
	{ "Enabled",		7,	CFG_TYPE_BOOLEAN,	FIELD_INFO( audio.enable ),		0,		1,		NULL,	0,	NULL, },
	{ "Volume",			6,	CFG_TYPE_UINT,		FIELD_INFO( audio.volume ),		0,		1,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,						0, 		0,		0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_osd[] = {
	{ "ShowTime",		8,	CFG_TYPE_BOOLEAN,	FIELD_INFO( osd.show_time ),		0,		1,	NULL,	0,	NULL, },
	{ "ShowDate",		8,	CFG_TYPE_BOOLEAN,	FIELD_INFO( osd.show_date ),		0,		1,	NULL,	0,	NULL, },
	{ "ShowText",		8,	CFG_TYPE_BOOLEAN,	FIELD_INFO( osd.show_text ),		0,		1,	NULL,	0,	NULL, },
	{ "Text",			4,	CFG_TYPE_STRING,	FIELD_INFO( osd.string ),			0,		0,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,						0, 			0,		0,	NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_camera[] = {
	{ "Security",		8,	CFG_TYPE_NONE,		( unsigned long )cfg_security,		0,	0,	0,		NULL,	0,	NULL, },
	{ "Video",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_video,			0,	0,	0,		NULL,	0,	NULL, },
	{ "Image",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_image,			0,	0,	0,		NULL,	0,	NULL, },
	{ "MotionDetect",	12,	CFG_TYPE_NONE,		( unsigned long )cfg_md,			0,	0,	0,		NULL,	0,	NULL, },
	{ "Mask",			4,	CFG_TYPE_NONE,		( unsigned long )cfg_mask,			0,	0,	0,		NULL,	0,	NULL, },
	{ "Audio",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_audio,			0,	0,	0,		NULL,	0,	NULL, },
	{ "OSD",			3,	CFG_TYPE_NONE,		( unsigned long )cfg_osd,			0,	0,	0,		NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,									0, 	0,	0,		NULL,	0,	NULL, },
};

static cfg_entry_t	cfg_entrys[] = {
	{ "Version",		7,	CFG_TYPE_STRING,	FIELD_INFO( version ),				0,	0,	NULL,	0,	NULL, },
	{ "Camera",			5,	CFG_TYPE_NONE,		( unsigned long )cfg_camera,	0,	0,	0,	NULL,	0,	NULL, },
	{ NULL,				0,	CFG_TYPE_NONE,		0,								0,	0,	0,	NULL,	0,	NULL, },
};

//==========================================================================
// Static Functions
//==========================================================================
static int
cfg_write_file( const uint32_t pos, CFG_CameraCtx *ctx )
{
	int data_size = 0;
	char *dst = ( char * )cfg_buff;

	sprintf( ctx->version, "%s", CFG_VERSION );

	dst += sprintf( dst, "{" );
	data_size = CFG_GenTxt( cfg_entrys, ctx, dst, CONFIG_DXDOORCAM_CONF_SIZE );

	if ( data_size > 0 ) {
		if ( dst[ data_size - 1 ] == ',' ) {
			dst[ data_size - 1 ] = '\n';
		}

		dst += data_size;
	}

	dst += sprintf( dst, "\n}\n" );

	sf_write( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, pos );

	return SUCCESS;
}

static int
cfg_read_file( const uint32_t pos, CFG_CameraCtx *ctx )
{
	int res = FAILURE;

	sf_read( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, pos );

	cfg_buff[ CONFIG_DXDOORCAM_CONF_SIZE ] = 0;
	dbg_error( "%s", cfg_buff );

	res = CFG_ParserTxt( ( char * )cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, cfg_entrys, ctx );

	if ( strcmp( ctx->version, CFG_VERSION ) ) {
		dbg_error( "CFG Version Error( Expect: %s, Current: %s )!!!", CFG_VERSION, ctx->version );
		res = FAILURE;
	}

	return res;
}

//==========================================================================
// APIs
//==========================================================================
int
CFG_Save( CFG_CameraCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	} else {
	dbg_error();
		sf_read( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, CONFIG_DXDOORCAM_CONF_OFFSET );

		sf_write( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, CONFIG_DXDOORCAM_CONF_BK_OFFSET );

		cfg_write_file( CONFIG_DXDOORCAM_CONF_OFFSET, ctx );

	dbg_error();
		return SUCCESS;
	}
}

int
CFG_Load( CFG_CameraCtx *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	} else {
		if ( cfg_read_file( CONFIG_DXDOORCAM_CONF_OFFSET, ctx ) == FAILURE ) {
			dbg_line( "Do cfg_read_file( %x ) Failed!!!", CONFIG_DXDOORCAM_CONF_OFFSET );
		} else {
			dbg_error( "%s", cfg_buff );
			return SUCCESS;
		}

		if ( cfg_read_file( CONFIG_DXDOORCAM_CONF_BK_OFFSET, ctx ) == FAILURE ) {
			dbg_line( "Do cfg_read_file( %x ) Failed!!!", CONFIG_DXDOORCAM_CONF_BK_OFFSET );
		} else {
			sf_read( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, CONFIG_DXDOORCAM_CONF_BK_OFFSET );

			sf_write( cfg_buff, CONFIG_DXDOORCAM_CONF_SIZE, CONFIG_DXDOORCAM_CONF_OFFSET );

			return SUCCESS;
		}

		return FAILURE;
	}
}

int
CFG_Generate( CFG_CameraCtx *ctx, char **data )
{
	if ( ( data == NULL ) || ( ctx == NULL ) ) {
		return FAILURE;
	} else {
		*data = my_malloc( 1024000 );

		if ( *data ) {
			char *dst = *data;

			dst += sprintf( dst, "{" );
			dst += CFG_GenTxt( cfg_entrys, ctx, dst, 1024000 );

			if ( dst[ -1 ] == ',' ) {
				dst[ -1 ] = '\n';
			}

			dst += sprintf( dst, "\n}\n" );

			return ( dst - *data );
		}

		return FAILURE;
	}
}

int
CFG_Parser( char *data, u32 data_size, CFG_CameraCtx *ctx )
{
	if ( ( data == NULL ) || ( data_size == 0 ) || ( ctx == NULL ) ) {
		return FAILURE;
	} else {
		return CFG_ParserTxt( data, data_size, cfg_entrys, ctx );
	}
}

int
CFG_LoadDefault( CFG_CameraCtx *ctx )
{
	int i;

	if ( ctx == NULL ) {
		return FAILURE;
	}

	memset( ctx, 0, sizeof( CFG_CameraCtx ) );

# define FIELD_VIDEO( field )		ctx->video.field

	sprintf( FIELD_VIDEO( live.url ), "live" );
	FIELD_VIDEO( live.port )					= 554;
	FIELD_VIDEO( live.w )						= 1920;
	FIELD_VIDEO( live.h )						= 1080;
	FIELD_VIDEO( live.fps )						= 24;
	FIELD_VIDEO( live.gop )						= 24;
	FIELD_VIDEO( live.bitrate_ctrl )			= CAMERA_BITRATECTRL_CVBR;
	FIELD_VIDEO( live.quality )					= 50;
	FIELD_VIDEO( live.bitrate )					= 2 * 1024;
	FIELD_VIDEO( live.profile_type )			= CAMERA_PROFILE_HIGH;

	sprintf( FIELD_VIDEO( recording.url ), "recording" );
	FIELD_VIDEO( recording.port )				= 554;
	FIELD_VIDEO( recording.w )					= 1280;
	FIELD_VIDEO( recording.h )					= 720;
	FIELD_VIDEO( recording.fps )				= 24;
	FIELD_VIDEO( recording.gop )				= 24;
	FIELD_VIDEO( recording.bitrate_ctrl )		= CAMERA_BITRATECTRL_CVBR;
	FIELD_VIDEO( recording.quality )			= 50;
	FIELD_VIDEO( recording.bitrate )			= 1024;
	FIELD_VIDEO( recording.profile_type )		= CAMERA_PROFILE_HIGH;

	sprintf( FIELD_VIDEO( snapshot.url ), "snapshot" );
	FIELD_VIDEO( snapshot.port )				= 554;
	FIELD_VIDEO( snapshot.w )					= 1280;
	FIELD_VIDEO( snapshot.h )					= 720;
	FIELD_VIDEO( snapshot.fps )					= 1;
	FIELD_VIDEO( snapshot.quality )				= 50;
	FIELD_VIDEO( snapshot.bitrate )				= 512;

# define FIELD_IMAGE( field )		ctx->image.field

	FIELD_IMAGE( dnr3 )				= TRUE;
	FIELD_IMAGE( dehaze )			= TRUE;

	FIELD_IMAGE( blc.mode )			= CAMERA_SWITCH_OFF;
	FIELD_IMAGE( blc.level )		= 50;

    FIELD_IMAGE( power_freq )       = CAMERA_POWER_FREQ_60HZ;
    FIELD_IMAGE( roi )              = 0;

# define FIELD_COLOR( field )		ctx->image.color.field

	FIELD_COLOR( brightness )		= 128;
	FIELD_COLOR( contrast )			= 2;  //0~4 default 2
	FIELD_COLOR( hue )			    = 0;  //Not support
	FIELD_COLOR( saturation )		= 3;  //0~7 default 3
	FIELD_COLOR( sharpness )		= 4;  //0~4
	FIELD_COLOR( gamma )			= 0;  //Not support
	FIELD_COLOR( gray_mode )		= 0;  //Not support

# define FIELD_PIC( field )			ctx->image.pic.field
	FIELD_PIC( roll )				= 0;  //Not support
	FIELD_PIC( flip )				= TRUE;
	FIELD_PIC( mirror )				= TRUE;
	FIELD_PIC( rotate )				= 0;  //Not support

# define FIELD_AWB( field )			ctx->image.awb.field
	FIELD_AWB( mode )				= CAMERA_AWB_MODE_AUTO; //1:Auto 0,2:Manual
	FIELD_AWB( level )				= 0;
	FIELD_AWB( r_gain )				= 264;
	FIELD_AWB( g_gain )				= 128;
	FIELD_AWB( b_gain )				= 225;

	FIELD_IMAGE( wdr.mode )			= 0; //0~4
	FIELD_IMAGE( wdr.level )		= 0; //Not support

# define FIELD_AE( field )			ctx->image.ae.field
	FIELD_AE( mode )				= CAMERA_AE_MODE_AUTO;
	FIELD_AE( gain )				= 25;
	FIELD_AE( exposure_time )		= 500; //0~3000

	FIELD_IMAGE( ir_cut.mode )		= CAMERA_IRCUT_MODE_OFF;

# define FIELD_MD( n, field )		ctx->md.area[ n ].field
	ctx->md.sensitivity	= CAMERA_MD_SENSITIVITY;

	for ( i = 0; i < MAX_MD_SUPPORTED; i ++ ) {
		if ( i ) {
			FIELD_MD( i, enable )		= FALSE;
		} else {
			FIELD_MD( i, enable )		= TRUE;
		}

		FIELD_MD( i, x )			= 0;
		FIELD_MD( i, y )			= 0;
		FIELD_MD( i, width )		= CAMERA_MD_AREA_WIDTH_MAX;
		FIELD_MD( i, height )		= CAMERA_MD_AREA_HEIGHT_MAX;
		FIELD_MD( i, percentage )	= CAMERA_MD_PERCENTAGE_MIDDLE;
	}

# define FIELD_MASK( n, field )		ctx->mask[ n ].field
	for ( i = 0; i < MAX_MASK_SUPPORTED; i ++ ) {
		FIELD_MASK( i, enable )		= FALSE;
		FIELD_MASK( i, x )			= 0;
		FIELD_MASK( i, y )			= 0;
		FIELD_MASK( i, width )		= 160;
		FIELD_MASK( i, height )		= 90;
	}

# define FIELD_AUDIO( field )		ctx->audio.field
	FIELD_AUDIO( enable )			= TRUE;
	FIELD_AUDIO( volume )			= 80;

# define FIELD_OSD( field )			ctx->osd.field
	FIELD_OSD( show_time )			= FALSE;
	FIELD_OSD( show_date )			= FALSE;
	FIELD_OSD( show_text )			= FALSE;
	FIELD_OSD( string )[ 0 ]		= 0;

	return SUCCESS;
}

