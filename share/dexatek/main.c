
//==========================================================================
// Include File
//==========================================================================
# include "Project.h"
# include "Headers.h"
# include "Media.h"
# include "DataManagement.h"
# include "System/StateMachine.h"
# include "Service/HTTPd.h"


#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "LedManager.h"
#include "dxSysDebugger.h"
#include "dxKeypadManager.h"
#include "CentralStateMachineAutomata.h"
#include "dxDeviceInfoManager.h"

//==========================================================================
// Type Define
//==========================================================================

# define TEST_ACCOUNT
//# define TEST_UART
//# define TEST_SF_RW
//# define TEST_ENV_RW
//# define TEST_WIFI
//# define TEST_WIFI_SCAN

//==========================================================================
// Static Params
//==========================================================================
static time_t	system_time;

static t_ertos_eventhandler		framework_init_evt = NULL;

//==========================================================================
// Static Functions
//==========================================================================
static dxTaskHandle_t   Application_Start_Thread	= NULL;


static dxGPIO_t Rts390XGPIOConfig[] =
{
    {
        NULL,
        DK_BLE_CENTRAL_MODE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_BLE_RESET,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_BLUE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_YELLOW,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXPIN_INPUT,
        DXPIN_MODE_UNSUPPORTED
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};



dxKeypad_t RTS390xKeypadConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXKEYPAD_TRIGGER_LEVEL_LOW,
        MAIN_BUTTON_REPORT_ID,
        DX_GENERAL_INTERVAL_ONE_SECOND,
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG
    }
};
//
//static char *test_ssid	= "DK_SWRD_Test";
//static char *test_pwd	= "0000055555";
//static char test_security	= 7;

static char *test_ssid	= "DK_DOORCAM_TEST";
static char *test_pwd	= "1234567890";
static char test_security	= 7;

//static char *test_ssid	= "DKDemo";
//static char *test_pwd	= "88888888";
//static char test_security	= 7;

//static char *test_ssid	= "SamWin10 SSID";
//static char *test_pwd	= "00000000";
//static char test_security	= 7;

//static char test_security	= 7;
//static char * test_ssid;
//static char * test_pwd;

typedef struct {
	char	*ssid;
	char	*pwd;
	char	security;
} test_wifi_info_t;

static test_wifi_info_t test_wifi_info[] = {
	{ "DK_SWRD_Test", "0000055555", 7 },
	{ "DK_DOORCAM_TEST", "1234567890", 7 },
	{ "DK_DOORCAM_TEST", "1234567890", 7 },
};

static int curr_wifi = 0;
static int max_wifi = sizeof( test_wifi_info ) / sizeof( test_wifi_info_t );


static dxWIFI_RET_CODE
scan_result_handler( dxWiFi_Scan_Result_t *scan_result )
{
    if ( scan_result && !scan_result->scan_complete ) {
		if ( strcmp( ( char * )scan_result->ap_details.SSID.val, test_ssid ) == 0 ) {
			if ( dxWiFi_Connect( ( char * )scan_result->ap_details.SSID.val, scan_result->ap_details.security, test_pwd, 0, NULL ) == DXWIFI_SUCCESS ) {
				dxDHCP_Start();
dbg_error( "dxWiFi_Connect Success" );
			} else {
dbg_error( "dxWiFi_Connect Failed" );
			}
			curr_wifi ++;

			if ( curr_wifi >= max_wifi ) {
				curr_wifi = 0;
			}
		}
    }

    return DXWIFI_SUCCESS;
}

static void *
dxframework_proc( void *arg )
{
	dxPLATFORM_Init();

	dxOS_Init();

	GSysDebuggerInit();

	dxMainImage_Init();

	//    dxGPIO_Init(Rts390XGPIOConfig);
# ifdef TEST_ACCOUNT
	dxDevInfo_Init();

	char temp[ 128 ];
	u32 DeviceAccessKeyBuffSize;

	dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_IntegrityCheck(dxTRUE);

	DevInfoRet = dxDevInfo_GetData( DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, temp, &DeviceAccessKeyBuffSize, dxFALSE );

//	if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
# ifdef TEST_GATEWAYDEVICENAME
		char *GatewayDeviceName	= TEST_GATEWAYDEVICENAME;
		char *GatewayAccessKey	= TEST_GATEWAYACCESSKEY;
# else
		char *GatewayDeviceName	= "DoorCam Test";
		char *GatewayAccessKey	= "59ae59e920a499693f52816d829aee1c4f9f4feea34de";
# endif

		char *SSID			= test_ssid;
		char *Password		= test_pwd;
		char *fw			= DOORCAM_FW_VERSION;
		char *ble_fw		= DOORCAM_BLE_VERSION;
		u32 SecurityType	= DXWIFI_SECURITY_WPA2_AES_PSK;

		dbg_warm( "DevInfoRet != DXDEV_INFO_SUCCESS" );

		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE, &SecurityType, sizeof(int32_t), dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_NAME, GatewayDeviceName, strlen( GatewayDeviceName ) + 1, dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, GatewayAccessKey, strlen( GatewayAccessKey ) + 1, dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID, SSID, strlen( SSID ) + 1, dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD, Password, strlen( Password ) + 1, dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, fw, strlen( fw ), dxFALSE);
		DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, ble_fw, strlen( ble_fw ), dxFALSE);

		DevInfoRet = dxDevInfo_SaveToDisk();
//	}

# endif		// TEST_ACCOUNT

	state_machine_main_event_t	StMachineMainEventFunc	= CentralStateMachine_Init(STATE_IDLE);


	WifiCommManager_result_t	WifiRet					= WifiCommManagerInit(StMachineMainEventFunc.EventWifiManager);
	if ( WifiRet != WIFI_COMM_MANAGER_SUCCESS ) {
		dbg_error( "Fail to WifiCommManagerInit( %d ).", WifiRet );
		exit( -1 );
	}

	LedManager_result_t			LedManResult			= LedManagerInit();
	if ( LedManResult != LED_MANAGER_SUCCESS ) {
		dbg_error( "Fail to LedManagerInit( %d ).", LedManResult );
		exit( -1 );
	}

	BLEManager_result_t			BleManRet				= BleManagerInit(StMachineMainEventFunc.EventBLEManager);
	if ( BleManRet != DEV_MANAGER_SUCCESS ) {
		dbg_error( "Fail to BleManagerInit( %d ).", BleManRet );
		exit( -1 );
	}

	UdpCommManager_result_t		UdpManRet				= UdpCommManagerInit(StMachineMainEventFunc.EventUdpManager);
	if ( UdpManRet != UDP_COMM_MANAGER_SUCCESS ) {
		dbg_error( "Fail to UdpCommManagerInit( %d ).", UdpManRet );
		exit( -1 );
	}

	dxJOB_MANAGER_RET_CODE		JobManRet				= JobManagerInit(StMachineMainEventFunc.EventJobManager);
	if ( JobManRet != DXJOBMANAGER_SUCCESS ) {
		dbg_error( "Fail to JobManagerInit( %d ).", JobManRet );
		exit( -1 );
	}

	const WifiCommDeviceMac_t	*pDeviceMac				= WifiCommManagerGetDeviceMac();

	DKServer_StatusCode_t		ServerManRet			= DKServerManager_Init( StMachineMainEventFunc.EventServerManager,
																			(unsigned char *)pDeviceMac->MacAddr, sizeof(pDeviceMac->MacAddr),
																			(uint8_t *)DK_SERVER_DNS_NAME,
																			(uint8_t *)DK_SERVER_APP_ID,
																			(uint8_t *)DK_SERVER_API_KEY,
																			(uint8_t *)DK_SERVER_API_VERSION);
	if ( ServerManRet != DKSERVER_STATUS_SUCCESS ) {
		dbg_error( "Fail to DKServerManager_Init( %d ).", ServerManRet );
		exit( -1 );
	}

	HPManager_result_t			HpManRet				= HpManagerInit_Server( StMachineMainEventFunc.EventHpManager );
	if ( HpManRet != HP_MANAGER_SUCCESS ) {
		dbg_error( "Fail to HpManagerInit_Server( %d ).", HpManRet );
		exit( -1 );
	}

	//Initialize the keypad, key event will be reported to KeypadEventFunctionCallback
	dxKEYPAD_RET_CODE			dxKeyPadRet				= dxKeypad_Init( RTS390xKeypadConfig, StMachineMainEventFunc.EventKeyPadManager );
	if ( dxKeyPadRet != DXKEYPAD_SUCCESS ) {
		dbg_error( "Fail to dxKeypad_Init( %d ).", dxKeyPadRet );
		exit( -1 );
	}


	//Add main application to stack profiling
	GSysDebugger_RegisterForThreadDiagnostic( &Application_Start_Thread );

	dbg( "=========================================" );
	dbg( "Start DkFramework" );
	dbg( "=========================================" );

	if ( framework_init_evt != NULL ) {
		ertos_event_signal( framework_init_evt );
	}

	/* Gateway Mode Operation Management */
	while ( 1 ) {
		//Process the state machine
		CentralStateMachine_Process();

		//Refresh the watch dog timer here for each main app run-loop
		//dxPLATFORM_WatchDogRefresh();

		//Short sleep.
		dxTimeDelayMS( 30 ); //SUGGEST to KEEP 30ms (due to the need of calling CentralStateMachine_Process)
	}

	dxTaskDelete( NULL );

	return NULL;
}

static void *
dxdoorcam_proc( void *arg )
{
	if ( DC_Init() == FAILURE ) {
		dbg_error( "Do DC_Init Failed !!!" );
		exit( -1 );
	} else if ( Record_Init() == FAILURE ) {
		dbg_error( "Do Record_Init Failed !!!" );
		exit( -1 );
	} else if ( Snapshot_Init() == FAILURE ) {
		dbg_error( "Do Snapshot_Init Failed !!!" );
		exit( -1 );
	} else if ( MediaCenter_Init( 1 ) == FAILURE ) {
		dbg_error( "Do MediaCenter_Init Failed !!!" );
		exit( -1 );
//	} else if ( Camera_OpenAudioOut() == FAILURE ) {
//		dbg_error( "Do Camera_OpenAudioOut Failed !!!" );
////		exit( -1 );
	}

# if 0

    Httpd_ServerContext httpd;

	memset( &httpd, 0, sizeof( Httpd_ServerContext ) );

	httpd.http_port		= 80;
	httpd.https_port	= 443;

	sprintf( httpd.root, "/mnt/www/" );
	sprintf( httpd.homepage, "/mnt/www/index.html" );

	if ( Httpd_ServerStart( &httpd ) == FAILURE ) {
		dbg_error( "Do Httpd_ServerStart Failed !!!" );
//		exit( -1 );
	}

	CGI_Init();

# endif

	StateMachine_Run();

	return NULL;
}

# ifdef TEST_WIFI
static void
test_wifi()
{
	extern int wifi_thread_init( void );
	wifi_thread_init();

	dxWiFi_On( DXWIFI_MODE_STA );

# ifdef TEST_WIFI_SCAN
	sleep( 1 );

	dxWiFi_Scan( scan_result_handler, NULL );

	while ( 1 ) {
		sleep( 2 );
	}

# else
	while ( 1 ) {
		dbg_warm( "test_wifi_info[ curr_wifi ].ssid: %s", test_wifi_info[ curr_wifi ].ssid );

		u32 curr_tick = ticks;

		if ( dxWiFi_Connect( test_wifi_info[ curr_wifi ].ssid, test_wifi_info[ curr_wifi ].security, test_wifi_info[ curr_wifi ].pwd, 0, NULL ) == DXWIFI_SUCCESS ) {
			dbg_error( "Time: %u", ticks - curr_tick );
			curr_tick = ticks;

			if ( dxDHCP_Start() == DXNET_SUCCESS ) {
				dbg_error( "Time: %u", ticks - curr_tick );

				sleep( 2 );

				dbg_warm( "dxDHCP_Stop" );
				dxDHCP_Stop();
				dbg_warm( "dxDHCP_Release" );
				dxDHCP_Release();
			} else {
				dbg_error( "Time: %u", ticks - curr_tick );
			}

			dbg_warm( "dxWiFi_Disconnect" );
			dxWiFi_Disconnect();
		}

		curr_wifi ++;

		if ( curr_wifi >= max_wifi ) {
			curr_wifi = 0;
		}

		sleep( 2 );

	    dbg_line( "ertos_heap_cur_free = %u Bs", ertos_heap_cur_free() );
	    dbg_line( "ertos_heap_min_free = %u Bs", ertos_heap_min_free() );
	    dbg_line( "rom_ertos_heap_size = %u Bs", rom_ertos_heap_size );
	}

# endif
}

# endif

# ifdef TEST_UART

static char *test_string	= "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789\n";
static void *
uart_test_proc( void *arg )
{
	int string_len = strlen( test_string ), i = 0;

	dbg_warm();

	while ( 1 ) {
		for ( i = 0; i < string_len; i ++ ) {
			uarts_putc( test_string[ i ] );
//			uarts_forceputc( test_string[ i ] );
		}
		sleep( 1 );
	}

	return NULL;
}

static char uart_r_buff[ 512 ];
static int	uart_r_len = 0;
static int
lowlevel_irq_handler(void * arg)
{
    u8 ch;

//	lowlevel_tx_interrupt_handler();
# if 1
	while ( uarts_getc_nob( &ch ) == 0 ) {
		if ( ch == 0xDE ) {
			printf( "\n" );
		}

		printf( "%02x ", ch );
	}
# else
	while ( uarts_getc_nob( &ch ) == 0 ) {
		uart_r_buff[ uart_r_len ++ ]	= ch;

		if ( ch == '\n' ) {
			uart_r_buff[ uart_r_len ] = 0;
			printf( "%s", uart_r_buff );
			uart_r_len = 0;
		}
	}
# endif
	return 0;
}

# endif

//==========================================================================
// APIs
//==========================================================================
void
MainProcessRun( void )
{
	dbg( "\n=========================================" );

# ifdef DOORCAM_FW_VERSION
	dbg( "\tFirmware Version: "DOORCAM_FW_VERSION );
# endif

# ifdef RELEASE_DATE
	dbg( "\tRelease Date:     "RELEASE_DATE );
# endif

	dbg( "=========================================" );

	system_time	= time( NULL );

	memset( &g_network_cfg, 0, sizeof( g_network_cfg ) );

# ifdef TEST_MODE_ENABLE

#	ifdef TEST_SF_RW
	char *test_sf_str = "0123456789abcdef";
	char *test_sf_str2 = "ABCDEFGHIJKLMNO";
	char test_sf_pstr[ 64 ];


	char test_sf_pstr2[ 64 ];

	memset( test_sf_pstr, 0x00, 64 );

	sf_read( test_sf_pstr, strlen( test_sf_str ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) );
	dbg_warm( "%s", test_sf_pstr );
	sf_program( test_sf_str, strlen( test_sf_str ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) );
	sf_read( test_sf_pstr, strlen( test_sf_str ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) );

	dbg_warm( "%s", test_sf_pstr );

# define TEST_OFFSET		0x400

	sf_read( test_sf_pstr2, strlen( test_sf_str2 ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) + TEST_OFFSET );
	dbg_warm( "%s", test_sf_pstr2 );
	sf_program( test_sf_str, strlen( test_sf_str2 ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) + TEST_OFFSET );
	sf_read( test_sf_pstr2, strlen( test_sf_str2 ), CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) + TEST_OFFSET );

	dbg_warm( "%s", test_sf_pstr2 );

	sf_read( test_sf_pstr, 64, CONFIG_DKFRAMEWORK_CONF_OFFSET + ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 ) );

	dbg_warm( "%s", test_sf_pstr );
	Dump( test_sf_pstr, 64 );

	return;
#	endif

#	ifdef TEST_ENV_RW
	char *test_env_str = "0123456789abcdef";
	char *test_env_pstr;

//	setenv( "test_env_str", test_env_str, 1 );
	test_env_pstr = getenv( "test_env_str" );

	dbg_warm( "%s", test_env_pstr );

	sleep( 1 );

	saveenv();
	return;
#	endif

#	ifdef TEST_WIFI
	test_wifi();
	return;
#	endif

# endif

# ifdef TEST_UART

//	uarts_init( clockget_bus(), 115200, 0 );
	uart_r_len	= 0;

	uart1_config( 115200, 1 );
# if 1
	irq_request( IRQ_BIT_UART1, lowlevel_irq_handler, "UART1", NULL );

	while ( 1 ) {
		sleep( 1 );
	}
# else
	lowlevel_set(1);

	ertos_task_create( uart_test_proc, "uart_test_proc", 1024, NULL, 2 );

	int string_len = strlen( test_string ), len = 0;
	char buff[ 512 ], ch;
//
	while ( 1 ) {
# if 0
		buff[ len ]	= ( char )uarts_getc();

		if ( buff[ len ] == '\n' ) {
			buff[ len + 1 ] = 0;
			printf( "%s", buff );
			len = 0;
		} else {
			len ++;
		}
# else
		while ( uarts_getc_nob( &ch ) == 0 ) {
			uart_r_buff[ uart_r_len ++ ]	= ch;

			if ( ch == '\n' ) {
				uart_r_buff[ uart_r_len ] = 0;
				printf( "%s", uart_r_buff );
				uart_r_len = 0;
			}
		}
# endif
	}
# endif

	return;

# endif

	My_MemInit();
	tick_init();

	Camera_Init();

	if ( ( framework_init_evt = ertos_event_create() ) == NULL ) {
		dbg_warm( "(framework_init_evt = ertos_event_create()) == NULL" );
		return;
	}

	ertos_task_create( dxframework_proc, "dxframework_proc", ( 1024 * 1024 ), NULL, 10 );
	if ( ertos_event_wait( framework_init_evt, 100000 ) != 1 ) {
		dbg_warm( "ertos_event_wait(framework_init_evt, 100000) != 1" );
		return;
	}

	ertos_event_delete( framework_init_evt );
	framework_init_evt = NULL;

	sleep( 5 );

	ertos_task_create( dxdoorcam_proc, "dxdoorcam_proc", 10240, NULL, 10 );

	while ( 1 ) {
		sleep( 1 );
	}
}
