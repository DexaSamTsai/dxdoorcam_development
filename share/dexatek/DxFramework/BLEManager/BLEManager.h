//============================================================================
// File: BLeManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "BLEDeviceBridgeComm.h"
#include "BLEDeviceCentralControl.h"
#include "BLEDeviceInfoParser.h"
#include "BLEDeviceKeeper.h"
#include "dk_Peripheral.h"
#include "dk_Job.h"
#include "dxSysDebugger.h"
#include "BLEDeviceCentralControl.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/


#define SOLUTION_ID_TI_2541                                         0x00
#define SOLUTION_ID_NORDIC_52832                                    0x01

#define MAXIMUM_DEVICE_IN_JOB_SEQUENCE_ALLOWED                      20
#define DEVICE_SEARCH_QUEUE_REPORT_WAIT_TIME_OUT_MS                 5000
#define DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS          1000



/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

	DEV_MANAGER_SUCCESS                = 0,    					/**< Success */
	DEV_MANAGER_DEVICE_TO_ACESS_NOT_IN_KEEPERS_LIST,
	DEV_MANAGER_DEVICE_UNABLE_TO_FIND_DATA_TYPE,
	DEV_MANAGER_DEVICE_ROLE_SWITCH_NOT_POSSIBLE,
	DEV_MANAGER_DEVICE_PERIPHERALROLE_NOT_CONNECTED,

	DEV_MANAGER_RTOS_ERROR,
	DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE,
	DEV_MANAGER_INVALID_PARAMETER,
    DEV_MANAGER_NOT_AVAILABLE,

} BLEManager_result_t;

typedef enum
{

    DEV_MANAGER_EVENT_UNKNOWN                = 0,
    DEV_MANAGER_EVENT_UNPAIR_DEVICE_REPORT,                                 /**< Use BleManagerParseUnPairPeripheralReportEventObj to obtain device info */
    DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT,
    DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT,           /**< Device Data update Report available during device access*/
    DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT,                            /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT,                  /**< Use BleManagerParseDataReadReportEventObj to obtain the data */
    DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT,              /**< Use BleManagerParseAggregatedDataReportEventObj to obtain the data */
    DEV_MANAGER_EVENT_NEW_DEVICE_ADDED,
    DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD,
    DEV_MANAGER_EVENT_DEVICE_UNPAIRED,                                      /**< Use BleManagerParseUnPairedPeripheralReportEventObj to obtain device info */
    DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST,                               /**< Use  to obtain device keeper list */
    DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED,                        /**< Use BleManagerParseDeviceIndexReportEventObj to obtain the DeviceIndex */
    DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED,                             /**< Use BleManagerParseDeviceIndexReportEventObj to obtain the DeviceIndex */
    DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO,                                   /**< Use BleManagerParsePeripheralSystemInfoReportEventObj to obtain the system info */
    DEV_MANAGER_EVENT_DEVICE_SYSTEM_BD_ADDRESS,                             /**< Use BleManagerParsePeripheralSystemBdAddressReportEventObj to obtain the system info */
    DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY,                             /**< Use BleManagerParseFwBlockWriteVerifyReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_FW_UPDATE_REBOOT_TO_APP_REPORT,                       /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_RTC_UPDATED,                                          /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_BLE_RESET_REPORT,                                     /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_BLE_REPEATER_SETUP_REPORT,                            /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_BLE_REPEATER_MONITOR_REPORT,                          /**< Only event Header available.. No additional data except event header */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_STATUS_REPORT,                  /**< Use BleManagerParseConcurrentPeripheralStatusPayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SECURITY_KEY_REPORT,            /**< Use BleManagerParseConcurrentPeripheralSecurityKeyPayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_ADVERTISE_DATA_REPORT,          /**< Use BleManagerParseConcurrentPeripheralAdvertiseDataPayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SCAN_RESPONSE_REPORT,           /**< Use BleManagerParseConcurrentPeripheralScanResponsePayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SERVICE_CHARACTERISTIC_REPORT,  /**< Use BleManagerParseConcurrentPeripheralServiceCharacteristicPayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SYSTEM_UTILS_REPORT,            /**< Use BleManagerParseConcurrentPeripheralSystemUtilsPayloadReportEventObj to obtain the info */
    DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD,                /**< Use BleManagerParseConcurrentPeripheralDataInPayloadReportEventObj to obtain the info */

} BLEManager_EventType_t;

typedef enum
{

	DEV_MANAGER_TASK_SUCCESS                = 0,    				/**< Success */
	DEV_MANAGER_TASK_FAILED_CONNECTION_UNSTABLE,					/**< Task failed due to connection with device is unstable */
	DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED,							/**< The requested device to pair is already paired with current or another Gateway */
	DEV_MANAGER_TASK_DEVICE_IN_KEEPER_LIST_BUT_FOUND_UNPAIRED,		/**< Possible ResultState report from DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT*/
	DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_MISSING,					/**< There should be one but is missing, access is not possible and repairing is required!*/
	DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_IS_NOT_VALID,				/**< The key is not longer valid, possibly cause by device reset and repaired by other mobile devices*/
	DEV_MANAGER_TASK_DEVICE_PIN_CODE_IS_NOT_VALID,					/**< The security key exchange failed because pin code is not valid*/
	DEV_MANAGER_TASK_TIME_OUT,										/**< Time out of task set by application */
	DEV_MANAGER_TASK_INTERNAL_ERROR,								/**< Internal error but recovable, suggest to try resending the task again */
	DEV_MANAGER_TASK_INTERNAL_FATAL_ERROR,							/**< Internal error, not recovable... manager source need reviced */


} BLEManager_TaskResult_t;


typedef enum
{
	DEV_MANAGER_CENTRAL_STATE_UNKNOWN,
	DEV_MANAGER_CENTRAL_STATE_IDLE,
	DEV_MANAGER_CENTRAL_STATE_SCANNING,
	DEV_MANAGER_CENTRAL_STATE_CONNECTING,
	DEV_MANAGER_CENTRAL_STATE_CONNECTED,
	DEV_MANAGER_CENTRAL_STATE_DISCONNECTING,
	DEV_MANAGER_PERIPHERAL_STATE_IDLE,
	DEV_MANAGER_PERIPHERAL_STATE_ADVERTISING,
	DEV_MANAGER_PERIPHERAL_STATE_CONNECTED,

} BLEManager_States_t;


typedef enum
{
    DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT,
    DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_IDLE,
    DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_ADVERTISING,
    DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_CONNECTED,

    DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_UNKNOWN
} BLEManager_ConcurrentPeripheralState_t;


typedef enum
{
    PROGRAM_FLASH_NRF52_SUCESS = 0,
    PROGRAM_FLASH_NRF52_ERROR = -1,
    PROGRAM_FLASH_NRF52_CANNOT_GET_MEMORY = -2
} ProgramFlashNRF52_result_t;


typedef enum _BleResetStatus {
    DEV_MANAGER_RESET_TO_CENTRAL = 0,
    DEV_MANAGER_RESET_TO_PERIPHERAL,
    DEV_MANAGER_RESET_TO_UNKNOWN,

    DEV_MANAGER_RESET_NUM,
} BleResetStatus;


typedef enum
{
    DEV_MANAGER_ON_JOB_NULL             = 0,
    DEV_MANAGER_ON_JOB_START,
    DEV_MANAGER_ON_JOB_FIND_DEVICE,
    DEV_MANAGER_ON_JOB_START_SCAN,
    DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
    DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION,
    DEV_MANAGER_ON_JOB_CENTRAL_SET_DISCONNECT,
    DEV_MANAGER_ON_JOB_PASSCODE_CHARACTERISTIC_WRITE, //OBSOLETE
    DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
    DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE_GIVEN_KEY,
    DEV_MANAGER_ON_JOB_SECURITY_KEY_CLEAR,
    DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE,
    DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE_AND_SAVE_FOR_REPORT,
    DEV_MANAGER_ON_JOB_DATATYPE_PAYLOAD_READ,
    DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE,
    DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ,
    DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ_AGGREGATED_UPDATE,
    DEV_MANAGER_ON_JOB_FW_VERSION_READ,
    DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST,
    DEV_MANAGER_ON_JOB_REMOVE_DEVICE_FROM_KEEPER_LIST,
    DEV_MANAGER_ON_JOB_REMOVE_ALLDEV_FROM_KEEPER_LIST,
    //DEV_MANAGER_ON_JOB_SEND_DEVICE_KEEPER_LIST_TO_HP,
    DEV_MANAGER_ON_JOB_GET_DEVICE_KEEPER_LIST,
    DEV_MANAGER_ON_JOB_TRANSFER_PERIPHERAL_DATA_OUT,
    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL,
    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER,
    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL,
    DEV_MANAGER_ON_JOB_FW_BLOCK_WRITE,
    DEV_MANAGER_ON_JOB_FW_UPDATE_DONE_BOOT_TO_APP,
    DEV_MANAGER_ON_JOB_RTC_UPDATE,

    DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS,
    DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_DATA_OUT,

    DEV_MANAGER_ON_JOB_CONNECT_TO_REPEATER,
    DEV_MANAGER_ON_JOB_LOAD_REPEATER_HANDLE,
    DEV_MANAGER_ON_JOB_CHECK_REPEATER_STATE,
    DEV_MANAGER_ON_JOB_CHECK_REPEATER_WATCH_LIST,
    DEV_MANAGER_ON_JOB_CHECK_REPEATER_OPEN_PAIRING,
    DEV_MANAGER_ON_JOB_GET_REPEATER_ADVERTISE_INFO,

    //Below State process should never be included to job array sequence, below are meant for internal state handlings
    DEV_MANAGER_ON_JOB_STATE_UNCHANGE   = 100,
    DEV_MANAGER_ON_JOB_NEXT_STATE,
    DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR,
    DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG,
    DEV_MANAGER_ON_JOB_FAILED_DEVICE_ALREADY_PAIRED,
    DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR,
    DEV_MANAGER_ON_JOB_MISSING_SECURITY_KEY_ERROR,
    DEV_MANAGER_ON_JOB_SECURITY_KEY_NOT_VALID_ERROR,
    DEV_MANAGER_ON_JOB_INVALID_PIN_CODE_ERROR,
    DEV_MANAGER_ON_JOB_FAILED_RECEIVING_MSG,
    DEV_MANAGER_ON_JOB_FAILED_AND_RETRY,

} BLEManager_GenJobProcess_t;



/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
	int8_t			DevHandlerID;
	uint16_t  		ServiceUUID;
	uint16_t        CharacteristicUUID;
	char			NeedConvertTo128bit;
	uint16_t		WriteDataLen;
	uint8_t			WriteData[CHAR_READ_WRITE_DATA_MAX_MPAGE];

} DeviceAccess_t;


typedef struct
{
	BLEManager_EventType_t 	ReportEvent;
	BLEManager_TaskResult_t	ResultState;
	uint16_t				SourceOfOrigin;
	uint64_t				IdentificationNumber;

} EventReportHeader_t;

typedef struct
{
	EventReportHeader_t 	Header;
	uint8_t*				pData;

} EventReportObj_t;

typedef struct
{
	uint16_t 			PayloadSize;
	uint8_t				Payload[CHAR_READ_WRITE_DATA_MAX_MPAGE];

} DataReadReportObj_t;


typedef struct
{
	DevAddress_t 			DevAddress;
	AggregatedDataHeader_t	AggregatedItemHeader;
	uint8_t					TotalAggregatedItem;

} AggregatedReportHeader_t;

typedef struct
{
	AggregatedReportHeader_t 	Header;
	uint8_t*					pAggregatedItem;

} AggregatedReportObj_t;

typedef struct
{
	uint8_t 	ImageType;		//See eBleSystemInfoImageType
	uint8_t		Version[16]; 	//NULL Terminated version string in format of x.x.x (eg, 1.1.0), where x is decimal string

} SystemInfoReportObj_t;

typedef struct {
	int16_t     result;
} SystemBdAddressResponse_t;

typedef struct
{
    uint8_t     UUIDNeed128bitConversion;
    uint16_t    ServiceUUID;
    uint16_t    CharacteristicUUID;
    uint16_t    DataWriteLen;                                   //!< Len of the data to write in DataWrite
    uint8_t     DataWrite[CHAR_READ_WRITE_DATA_MAX_MPAGE];      //!< Data container, please note that the size is still 20 (CHAR_READ_WRITE_DATA_MAX) byte per transaction. Howeevr, multi transaction is allowed with up to total of CHAR_READ_WRITE_DATA_MAX_MPAGE

} CharacteristicWriteInJobData_t;

typedef struct
{
    uint8_t     UUIDNeed128bitConversion;
    uint16_t    ServiceUUID;
    uint16_t    CharacteristicUUID;
    uint16_t    DataReadLen;                                    //!< Len of the data to write in DataReadLen
    uint8_t     DataRead[CHAR_READ_EXT_DATA_MAX];               //!< Data container, please note that the size is 40 (CHAR_READ_EXT_DATA_MAX) byte for now per read

} CharacteristicReadInJobData_t;


typedef struct
{
    DeviceAddress_t                                 DeviceAddress;
    FwVersion_t                                     DeviceFwVersion;
    SecurityKeyCodeContainer_t                      SecurityKeyCodeContainer;
    BLEManager_TaskResult_t                         JobResult;
    void*                                           DeviceInfoObj;
    uint16_t                                        DeviceType;
    const BleCentralStates_t*                       CurrentCentralStatus;
    const BleCentralConcurrentPeripheralState_t*    CurrentConcurrentPeripheralState;

    const uint8_t*                                  pJobPayload;
    CharacteristicWriteInJobData_t                  MainCharWriteData;
    CharacteristicReadInJobData_t                   MainCharReadData;

    BLEManager_EventType_t                          EventReport;
    BLEManager_TaskResult_t                         EventResult;
    uint8_t*                                        pEventPayload;  /* NOTE: Event Payload Should NOT be release/free because its going to be released by application  */

} instance_data_t;


typedef BLEManager_GenJobProcess_t blemanager_state_func_t( instance_data_t *data );


typedef struct
{
    BLEManager_GenJobProcess_t  State;
    blemanager_state_func_t*    StateFunction;

} state_func_table_t;


typedef struct
{
    uint16_t    ErrorCode;                  //!< Non Zero if error (Srv_Discovery_Error_t)
    uint16_t    WriteIdHandShake;          //!< A unique ID provided from request side, this same ID will be returned on the response message
    uint16_t    CharHandlerIDToWrite;      //!< Characteristic handler ID to write the data to. Should be NON Zero

} BleDeviceWriteCharRspMsg_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data


typedef struct
{
    uint16_t    ErrorCode;                                      //!< Non Zero if error (Srv_Discovery_Error_t)
    uint16_t    ReadIdHandShake;                                //!< A unique ID provided from request side, this same ID will be returned on the response message
    uint16_t    CharHandlerIDToRead;                            //!< Characteristic handler ID to read the data to. Should be NON Zero
    uint16_t    DataReadLen;                                    //!< Len of the data available to read from in DataRead
    uint8_t     DataRead[CHAR_READ_WRITE_DATA_MAX];             //!< Data container, please note that the size is 20 byte per transaction.

} BleDeviceReadCharRspMsg_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data


typedef struct {
    Concurrent_Peripheral_States_t state;
} BleConcurrentPeripheralGetStatusResponse_t;


typedef struct {
    uint8_t     keyLength;
} BleConcurrentPeripheralSecurityKeyResponse_t;


typedef struct {
    int16_t     totalBytes;
} BleConcurrentPeripheralAdvertiseDataResponse_t;


typedef struct {
    int16_t     totalBytes;
} BleConcurrentPeripheralScanResponseResponse_t;


typedef struct {
    uint16_t    serviceUuid;
    int8_t      characteristicCount;
} BleConcurrentPeripheralServiceCharacteristicResponse_t;


typedef struct {
    uint16_t    enableMask;
} BleConcurrentPeripheralSystemUtilsResponse_t;



/******************************************************
 *                 Type Definitions
 ******************************************************/



/******************************************************
 *                 Global Variables
 ******************************************************/

extern BLEManager_GenJobProcess_t InJobSequence[MAXIMUM_DEVICE_IN_JOB_SEQUENCE_ALLOWED];
extern uint16_t CurrentInJobStageIndex;
extern dxWorkweTask_t   BLEManager_EventWorkerThread;
extern dxEventHandler_t BLEManager_RegisteredEventFunction;



/******************************************************
 *               Function Declarations
 ******************************************************/

/** Initialize BLE Manager, all BLE task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  Initial role is set to Central by default.
 *  NOTE1: Use BleManagerGetEventObjHeader(arg) to obtain the EventReportHeader_t.
 *  NOTE2: According to ReportEvent handle necessary action (see BLEManager_EventType_t)
 *  NOTE3: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE4: At the end of EventFunction call BleManagerCleanEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[in]  EventFunction 	 : 	The EventFunction that will be called when reporting activities
 *
 * @return @ref BLEManager_result_t
 */
// NOTE: Porting to dxOS
//extern BLEManager_result_t BleManagerInit(event_handler_t EventFunction);
extern BLEManager_result_t BleManagerInit(dxEventHandler_t EventFunction);

/** Get current state of BLE Manager
 *
 * @return @ref BLEManager_States_t
 */
extern BLEManager_States_t BleManagerCurrentState();

/**
 * Get concurrent peripheral state of BLE Manager
 *
 * @return @ref BLEManager_ConcurrentPeripheralState_t
 */
extern BLEManager_ConcurrentPeripheralState_t BleManagerConcurrentPeripheralState ();

/** Set BLE Manager to central role
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetRoleToCentral();

/** Set BLE Manager to central role to boot in FW SBL
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetRoleToCentralFwUpdateBoot();

/** Set BLE Manager to fw update mode, this will simply shutdown all auto recovery operation so that it does not affect fw block write process
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetRoleToFwUpdate();

/** Set BLE Manager to Peripheral role
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetRoleToPeripheral();


/** Set BLE Manager to Suspend Mode and put BLE Central into reset, to resume please call BleManagerSetRoleToCentral() or BleManagerSetRoleToPeripheral()
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSuspend();

/** Set BLE Manager to ignore any peripheral scan data handling
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerScanReportSuspend();

/** Set BLE Manager to resume peripheral scan data handling
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerScanReportResume();


/** Reset BLE Manager - This will clean all peripheral datas and free up memories
 *  WARNING: Do NOT call this API while Ble Manager role is set to central... Its is suggested to have BLE manager to enter Suspend state before calling this API
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerReset();

/** Request to query the system info (FW version and Boot type) again
 *
 * @param[in\out]  SystemInfoContainer 	 : 	The syste info container to copy to
 *
 * @return @ref BLEManager_result_t - SystemInfo is valid ONLY if DEV_MANAGER_SUCCESS is returned
 */
extern BLEManager_result_t BleManagerSystemInfoGet(SystemInfoReportObj_t* SystemInfoContainer);

/** Request to query the system info (FW version and Boot type) again
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSystemInfoRequest();


/** Get the event header from reported event obj arg
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported obj argument to obtain the header info
 *
 * @return @ref EventReportHeader_t
 */
extern EventReportHeader_t BleManagerGetEventObjHeader(void* ReportedEventArgObj);


/** All information of Object given by EventFunction will be cleaned
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported obj argument to clean up
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerCleanEventArgumentObj(void* ReportedEventArgObj);


/** Any UnPair Dexatek Peripheral device will be reported to EventFunction registered from BleManagerInit
 *  NOTE1: Once event function is called the obtained Obj can be parsed by calling BleManagerParseUnPairPeripheralReportEventObj
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerStartReportingUnPairPeripheral();


/** Parse reported event obj and return the device index
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref int16_t
 */
extern int16_t BleManagerParseDeviceIndexReportEventObj(void* ReportedEventArgObj);


/** Parse Unknown event (will provide the raw data feed from the bridgecomm, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleBridgeCommMsg_t
 */
extern BleBridgeCommMsg_t* BleManagerParseUnknownlReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DeviceInfoReport_t
 */
extern DeviceInfoReport_t* BleManagerParseUnPairPeripheralReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DeviceInfoReport_t
 */
extern DeviceInfoReport_t* BleManagerParseKeeperPeripheralReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DeviceKeeperListReport_t
 */
extern DeviceKeeperListReport_t* BleManagerParseDeviceKeeperListReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DeviceInfoReport_t
 */
extern DeviceInfoReport_t* BleManagerParseUnpairedPeripheralReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of DeviceInfoReport_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DeviceInfoReport_t
 */
extern DeviceInfoReport_t* BleManagerParseNewPeripheralAddedReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer within AggregatedReportObj_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref AggregatedReportObj_t
 */
extern AggregatedReportObj_t BleManagerParseAggregatedDataReportEventObj(void* ReportedEventArgObj);

/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer within DataReadReportObj_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref DataReadReportObj_t
 */
extern DataReadReportObj_t* BleManagerParseDataReadReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref uint8_t* Buffer that contain payload with fixed size of PERIPHERAL_PAYLOAD_SIZE_PER_TRANSFER
 */
extern uint8_t* BleManagerParsePeripheralRoleDataInPayloadReportEventObj(void* ReportedEventArgObj);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref uint8_t* Buffer that contain payload with fixed size of PERIPHERAL_PAYLOAD_SIZE_PER_TRANSFER
 */
extern uint8_t* BleManagerParseConcurrentPeripheralDataInPayloadReportEventObj (void* ReportedEventArgObj, uint16_t* payloadLen);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralGetStatusResponse_t* structure contains concurrent peripheral status.
 */
extern BleConcurrentPeripheralGetStatusResponse_t* BleManagerParseConcurrentPeripheralStatusPayloadReportEventObj (
    void* ReportedEventArgObj
);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralSecurityKeyResponse_t* structure contains concurrent peripheral response.
 */
extern BleConcurrentPeripheralSecurityKeyResponse_t* BleManagerParseConcurrentPeripheralSecurityKeyPayloadReportEventObj (
    void* ReportedEventArgObj
);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralAdvertiseDataResponse_t* structure contains concurrent peripheral response.
 */
extern BleConcurrentPeripheralAdvertiseDataResponse_t* BleManagerParseConcurrentPeripheralAdvertiseDataPayloadReportEventObj (
    void* ReportedEventArgObj
);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralScanResponseResponse_t* structure contains concurrent peripheral response.
 */
extern BleConcurrentPeripheralScanResponseResponse_t* BleManagerParseConcurrentPeripheralScanResponsePayloadReportEventObj (
    void* ReportedEventArgObj
);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralServiceCharacteristicResponse_t* structure contains concurrent peripheral response.
 */
extern BleConcurrentPeripheralServiceCharacteristicResponse_t* BleManagerParseConcurrentPeripheralServiceCharacteristicPayloadReportEventObj (
    void* ReportedEventArgObj
);


/**
 * Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 * DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref BleConcurrentPeripheralSystemUtilsResponse_t* structure contains concurrent peripheral response.
 */
extern BleConcurrentPeripheralSystemUtilsResponse_t* BleManagerParseConcurrentPeripheralSystemUtilsPayloadReportEventObj (
    void* ReportedEventArgObj
);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref SystemInfoReportObj_t*
 */
extern SystemInfoReportObj_t* BleManagerParsePeripheralSystemInfoReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported event argument to parse
 *
 * @return @ref SystemBdAddressResponse_t*
 */
extern SystemBdAddressResponse_t* BleManagerParsePeripheralSystemBdAddressReportEventObj(void* ReportedEventArgObj);


/** Parse reported event obj, please copy to other variable if returned info will be kept or sent to other location.
 *  DO NOT keep the reference to the returning pointer of uint8_t because all information will be cleaned after calling BleManagerCleanEventArgumentObj
 *
 * @param[in]  	ReportedEventArgObj 	 : 	The reported event argument to parse
 * @param[out]  DataLen 	 : 	The len of the data returned
 *
 * @return @ref uint8_t* (the pointer of data returned)
 */
extern uint8_t* BleManagerParseFwBlockWriteVerifyReportEventObj(void* ReportedEventArgObj, uint8_t* DataLen);


/** Stop reporting Unpair Dexatek Peripheral devices.
 *
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerStopReportingUnPairPeripheral();


/** Check if gateway maximum peripheral support (pair) is reached. If TRUE then gateway will not be able to pair any devices
 *
 *
 * @return @ref dxBOOL
 */
extern dxBOOL BleManagerIsMaximumPeripehralSupportReached();



/** Ask Ble Manager to pair a device (if SingleHostControl is set to true then pair bit will be set by the manager to given device and added to manager's keeper list if not already added).
 *  This task is asynchronous and all asynchronous task related API are queued in first in first out execution... Upon done it will be
 *  reported as DEV_MANAGER_EVENT_NEW_DEVICE_ADDED
 *
 * @param[in]  PairingInfo   :
 * 					->DeviceAddr 		: 	The device address to pair and add to keeper list
 * 					->SecurityCode  	:  	The security code that is needed for exchanging keys, this can be memset to all zeros if device is already added to keeper's list
 * 											previously and key has been exchanged.
 *					->SingleHostControl :  	If this is set to true, Ble Manager will set pair bit to the device and the device will ONLY be accessible by this Gateway.
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for paring device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerPairDeviceTask_Asynch(stPairingInfo_t* PairingInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);


/** Ask Ble Manager to add peripheral device to keeper's list, this simply adds to the keeper list and does not send the pair command to
 *  BLE device. After adding to keeper list, BLE manager will report if the device is not in pair state and application will have the chance to
 *  send pair task. Use this API if BLE belongs to this Gateway and not sure if the BLE device is already in Pair state or not.
 *  This task is asynchronous and all asynchronous task related API are queued in first in first out execution.
 *
 * @param[in]  DeviceAddInfo 	 : 	The device info for adding to keeper list
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for paring device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerAddDeviceTask_Asynch(DeviceAddInfo_t* DeviceAddInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);


/** Ask Ble Manager to Un-pair a device (pair bit will be clear by the manager to given device and removed from manager's keeper list)
 *
 * @param[in]  DeviceHandlerID 	 : 	The device address to to Unpair and remove from keeper list
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for Unparing device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerUnPairDeviceTask_Asynch(DevAddress_t DeviceAddr, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Ask Ble Manager to clean up all device in keeperlist, note that this simply clean up the device list but will not unpair it)
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for Unparing device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerCleanAllDeviceFromListTask_Asynch(dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Ask Ble Manager to send device keeper list to Hybrid Peripheral
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for Unparing device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerGetDeviceKeeperList_Asynch(dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Ask Ble Manager to set/renew security of a device
 *  This task is asynchronous and all asynchronous task related API are queued in first in first out execution... Upon done it will be
 *  reported as DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED
 *
 * @param[in]  SecurityInfo   :
 * 					->DeviceAddr 		: 	The device address to pair and add to keeper list
 * 					->SecurityCode  	:  	The security code that is needed for exchanging keys, must terminate with NULL (0x00)
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for paring device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetRenewSecurityTask_Asynch(stRenewSecurityInfo_t* SecurityInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Ask Ble Manager to Update Peripheral RTC
 *  This task is asynchronous and all asynchronous task related API are queued in first in first out execution... Upon done it will be
 *  reported as DEV_MANAGER_EVENT_RTC_UPDATED
 *
 * @param[in]  UpdateRtcInfo   : Information Containing the peripheral to update the RTC
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for paring device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerUpdateRTCTask_Asynch(stUpdatePeripheralRtcInfo_t* UpdateRtcInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Ask Ble Manager to Clear security of a device
 *  This task is asynchronous and all asynchronous task related API are queued in first in first out execution... Upon done it will be
 *  reported as DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED
 *
 * @param[in]  DeviceAddr   	 :  The device address to clear the security
 *
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for paring device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerClearSecurityTask_Asynch(DevAddress_t DeviceAddr, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);


/** Ask Ble Manager to access (write) device that is in manage's keeper list (paired)
 *
 * @param[in]  DevAccessInfo 	 : 	The device access info. See DeviceAccess_t
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for slow adv interval device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 * @param[in]  WriteOnly 	 	 : 	If 1 - Write only, 0 - Write follow by Advertisement payload read update
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerDeviceAccessWriteTask_Asynch(DeviceAccess_t DevAccessInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t WriteOnly);


/** Ask Ble Manager to access (write) device that is in manage's keeper list (paired)
 *
 * @param[in]  PeripheralJobExecutionData 	 : 	The device access info from Job execution data (does not handle multiple execution, only one is handled)
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * 									recommended timeout for slow adv interval device is 10 seconds.
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerDeviceAccessWriteTaskExt_Asynch(uint8_t* PeripheralJobExecutionData, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Send a block of FW data for update, this follows TI SBL FW update data format (Fw block)
 * 	DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY - this event will be received when central sends its same fw block after it is received. use BleManagerParseFwBlockWriteVerifyReportEventObj
 * 												 to obtain the data that is send from SBL.
 * @param[in]  pFwBlockPayload 	 : 	the FW block data for update
 * @param[in]  len 	 : 	The total len of the pFwBlockPayload.. Note that len is limited to 64 bytes long
 * @param[in]  SourceOfOrigin 	 : 	The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerFwUpdateBlockWriteTask_Asynch(uint8_t* pFwBlockPayload, uint8_t len, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/** Send a command to SBL notifying FW write is done and boot to main Application
 * 	DEV_MANAGER_EVENT_FW_UPDATE_REBOOT_TO_APP_REPORT - this event will be received to aknowledge command received by SBL
 *
 * @param[in]  SourceOfOrigin 	 : 	The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerFwUpdateDoneTask_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);


/** Send a common transfer payload to Gateway ble (as peripheral) while connected to client, the payload will reach to client in chunks of 20 bytes if the
 *  total payload len is more than 20 bytes.
 *
 *  NOTE: Payload does NOT need to include the data transfer header protocol, it is included automatically, in this case the message type is sent as DATA_TRANSFER_MSG_TYPE_COMMON
 *
 * @param[in]  Payload 	 : 	Payload to send as common transfer
 * @param[in]  len 	 : 	The total len of the Payload.. Note that len is limited to 64 bytes long
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerPeripheralRoleCommonDataOutTransferTask_Asynch(uint8_t* Payload, uint8_t len, dxTime_t TimeOutReport, uint64_t TaskIdentificationNumber);


/** Send a transfer payload to Gateway ble (as peripheral) while connected to client, the payload will reach to client in chunks of 20 bytes if the
 *  total payload len is more than 20 bytes.
 *
 *  NOTE: Payload does NOT need to include the data transfer header protocol, it is included automatically, in this case the message type is sent
 *  according to MsgType parameter
 *
 * @param[in]  MsgType 	 : 	MessageType of this transfer
 * @param[in]  Payload 	 : 	Payload to send as common transfer
 * @param[in]  len 	 : 	The total len of the Payload.. Note that len is limited to 64 bytes long
 * @param[in]  TimeOutReport 	 : 	TimeOut, when time reached the operation will be cancelled and message will be sent to event function
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerPeripheralRoleDataOutTransferTask_Asynch(eDataTransferMessageType MsgType, uint8_t* Payload, uint8_t len, dxTime_t TimeOutReport, uint64_t TaskIdentificationNumber);


/** Ask Ble Manager to change custom manufaturer advertisement data while in Peripheral Role
 *  WARNING: Make sure this API is called after BleManagerSetRoleToPeripheral().
 *
 * @param[in]  AdvData      :   Pointer to the new manufacture advertisement data
 * @param[in]  AdvDataLen   :   MAXIMUM of 20 byte, the total len in byte of AdvData. Cannot exceed 20 bytes
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerPeripheralRoleSetAdvManufactureCustomData_Asynch(uint8_t* AdvData, uint8_t AdvDataLen);


/** Ask BLE manager to setup repeater.
 *
 * @param[in]  repeaterIndex   :   The index of repeater
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerSetupRepeaterTask_Asynch (int16_t repeaterIndex, dxTime_t timeOutReport);


/** Ask BLE manager to monitor repeater.
 *
 * @param[in]  repeaterIndex   :   The index of repeater
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerMonitorRepeaterTask_Asynch (int16_t repeaterIndex, dxTime_t timeOutReport);


/**
 * Get the current status of concurrent peripheral mode.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralGetStatusTask_Asynch (void);


/**
 * Set bluetooth device address for concurrent peripheral.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param bleBdAddress              bluetooth device address
 * @param timeOutReport             timeOut, when time reached the operation will be cancelled and message will be sent
 *                                  to event function
 * @param taskIdentificationNumber  the identification number that will be reported back to registered EventFunction
 *                                  when Task is done.
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralCustomizeBdAddressTask_Asynch (
    BleCentralConnect_t* bleBdAddress,
    dxTime_t timeOutReport,
    uint64_t taskIdentificationNumber
);


/**
 * Send a transfer payload to Gateway ble (as concurrent peripheral) while connected to client, the payload will reach
 * to client in chunks of 20 bytes if the total payload len is more than 20 bytes.
 *
 * NOTE: Payload does NOT need to include the data transfer header protocol, it is included automatically, in this case
 * the message type is sent according to MsgType parameter
 *
 * @param MsgType                   messageType of this transfer
 * @param Payload                   payload to send as common transfer
 * @param length                    the total len of the Payload.. Note that len is limited to 64 bytes long
 * @param TimeOutReport             timeOut, when time reached the operation will be cancelled and message will be sent
 *                                  to event function
 * @param TaskIdentificationNumber  the identification number that will be reported back to registered EventFunction
 *                                  when Task is done.
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralDataOutTask_Asynch (
    eDataTransferMessageType MsgType,
    uint8_t* Payload,
    uint16_t length,
    dxTime_t TimeOutReport,
    uint64_t TaskIdentificationNumber
);


/**
 * Set security key for concurrent peripheral.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param key        The pointer of key
 * @param keyLength  The length of key
 * @return           BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralSetSecurityKeyTask_Asynch (
    uint8_t* key,
    uint16_t keyLength
);


/**
 * Set advertise data for concurrent peripheral.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param nameType      BLE_ADVDATA_NO_NAME or BLE_ADVDATA_FULL_NAME
 * @param nameLength    If no name, set it to 0.
 * @param name          Will have this field only when nameLength is not 0.
 * @param flag          Typical value is 0x06 (BLE_GAP_ADB_FLAG_LE_GENEARL_DISC_MODE and BLE_GAP_ADV_FLAG_BR_EDR_NOT_SUPPORTED)
 * @param uuidParam     Bit[7] - 1 means it is the complete list, while 0 means incomplete list.
 *                      Bit[6] - 1 the uuids will be represented by 128 bits (16 bytes).
 *                      Bit[5:0] - number of UUID which shown in the advertise packet.
 * @param uuids         If uuidParam Bit[6] is 0 { content is {uuid_lsb, uuid_msb, uuid_type}, 3 bytes }
 *                      If uuidParam Bit[6] is 1 { content is 128 bits (16 bytes) UUID in little endian }
 * @param interval      Advertising interval (in units of 0.625ms).
 * @param manufLenth    0 means disable manufacturer specific data.
 * @param manuf         Format: { company_identifier(2 bytes), data(N-2) }
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralSetAdvertiseDataTask_Asynch (
    BLE_ADVDATA_NAME_TYPE_t nameType,
    uint8_t                 nameLength,
    char*                   name,
    uint8_t                 flag,
    uint8_t                 uuidParam,
    uint8_t*                uuids,
    uint16_t                interval,
    uint8_t                 manufLength,
    uint8_t*                manuf
);


/**
 * Set scan response for concurrent peripheral.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param nameType      BLE_ADVDATA_NO_NAME or BLE_ADVDATA_FULL_NAME
 * @param nameLength    If no name, set it to 0.
 * @param name          Will have this field only when nameLength is not 0.
 * @param uuidParam     Bit[7] - 1 means it is the complete list, while 0 means incomplete list.
 *                      Bit[6] - 1 the uuids will be represented by 128 bits (16 bytes).
 *                      Bit[5:0] - number of UUID which shown in the advertise packet.
 * @param uuids         If uuidParam Bit[6] is 0 { content is {uuid_lsb, uuid_msb, uuid_type}, 3 bytes }
 *                      If uuidParam Bit[6] is 1 { content is 128 bits (16 bytes) UUID in little endian }
 * @param manufLenth    0 means disable manufacturer specific data.
 * @param manuf         Format: { company_identifier(2 bytes), data(N-2) }
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralSetScanResponseTask_Asynch (
    BLE_ADVDATA_NAME_TYPE_t nameType,
    uint8_t                 nameLength,
    char*                   name,
    uint8_t                 uuidParam,
    uint8_t*                uuids,
    uint8_t                 manufLength,
    uint8_t*                manuf
);


/**
 * Add service/characteristic for concurrent peripheral.
 * The total length of this payload cannot exceed 52 bytes.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param serviceCharacteristic the payload
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralAddServiceCharacteristicTask_Asynch (
    BleServiceCharacteristic_t* serviceCharacteristic
);


/**
 * Enable/disable the variable characteristic of SystemUtils Profile for concurrent peripheral.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @param systemUtilsMask the payload
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralEnableSystemUtilsTask_Asynch (
    uint16_t* systemUtilsMask
);


/**
 * Start concurrent peripheral mode.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralStartTask_Asynch (void);


/**
 * Stop concurrent peripheral mode.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralStopTask_Asynch (void);


/**
 * Disconnect from current connection in concurrent peripheral mode.
 * WARNING: Make sure this API is called after BleManagerSetRoleToCentral ().
 *
 * @return BLEManager_result_t
 */
extern BLEManager_result_t BleManagerConcurrentPeripheralDisconnectTask_Asynch (void);


/** Ask Ble Manager to retrieve the data of DataType
 *
 * @param[in]  DeviceInfoData 	 : 	Pointer to DeviceInfoReport_t to search for the dataType you are looking for
 * @param[in]  DataType 	 : 	DataType to look for, see DK_DATA_TYPE_IDS in BLEPeripheralInfo.h
 * @param[out]  DataLenOut 	 : 	The len of the DataOut, if 0 it means DataType not found
 * @param[out]  DataOut 	 : 	The buffer (should be at least 30 byte of space otherwise system crash might occur) to store the Data
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManagerDeviceUtils_GetDataTypeValue(DeviceInfoReport_t* DeviceInfoData, uint8_t DataType, uint8_t* DataLenOut, uint8_t* DataOut );


/** This is for INTERNAL USE ONLY (Test Purpose)
 *
 * @param[out]  MessageType 	 : 	See Spec for the protocol
 * @param[out]  CommandAux 	 : 	See Spec for the protocol
 * @param[out]  PayloadID 	 : 	See Spec for the protocol
 * @param[out]  PayloadLen 	 : 	See Spec for the protocol
 * @param[out]  Payload 	 : 	See Spec for the protocol
 *
 * @return @ref BLEManager_result_t
 */
extern BLEManager_result_t BleManager_FreeFormMesageSend(uint8_t MessageType, uint8_t CommandAux, uint8_t PayloadID, uint8_t PayloadLen, uint8_t* Payload);


/** This is for identify what BLE solution is in use
 *
 * @return @ref uint8_t : Solution id is defined in define SOLUTION_ID_xxxx
 */
uint8_t BleManager_SolutionID_Get(void);


/** This is for identify what BLE solution is in SBL
 *
 * @return @ref uint8_t : SBL Enable
 */
uint8_t BleManager_SBL_Enable(void);


/** This is for INTERNAL USE ONLY (Test Purpose)
 *
 * @param[in]  @ref eBleSystemInfoImageType image_type 	 : 	image type;

 */
void BleManager_SolutionID_Update(eBleSystemInfoImageType image_type);


/** Add BLE white list from keeper list
 *
 */
void BLEManager_WhiteList_AddFromKeeperList(void);


/** For NRF52 firmware update
 * function 1: BleManager_ProgramFirmware_NRF52_Init()
 * function 2: BleManager_ProgramFirmware_NRF52_Execution()
 * call BleManager_ProgramFirmware_NRF52_Init(), then call BleManager_ProgramFirmware_NRF52_Execution()
 *  for each of the following run. When *p_progress is 100, the firmware is program OK.
 */

/** Initialization of the firmware update of NRF52
 *
 * @param[in]  handle 	@ref dxSubImgInfoHandle_t : input handle for the file which contained the update information.
 *
 * @return @ref ProgramFlashNRF52_result_t
 */
ProgramFlashNRF52_result_t BleManager_ProgramFirmware_NRF52_Init(dxSubImgInfoHandle_t handle);


/** Execution of the firmware update of NRF52
 *
 * @param[out]  p_progrss : firmware update progress, the value is between 0 to 100. 100 means update finished.
 *
 *
 * @return @ref ProgramFlashNRF52_result_t
 */
ProgramFlashNRF52_result_t BleManager_ProgramFirmware_NRF52_Execution(uint8_t *p_progrss);


/** Take reset mutex
 *
 *
 * @return void
 */
void BleManager_TakeBLEResetMutex(void);


/** Give reset mutex
 *
 *
 * @return void
 */
void BleManager_GiveBLEResetMutex(void);


/**
 * Reset BLE Module
 */
void ResetBleModule (void);


/**
 * @brief CleanAllInJobReportQueue
 */
void CleanAllInJobReportQueue(void);


/**
 * @brief BLEManager_HandleAdvertiseInfo
 */
void BLEManager_HandleAdvertiseInfo (uint8_t *adv_info, uint16_t adv_info_length);


/**
 * @brief AllocAndParseDeviceInfoReportFromIndex
 */
DeviceInfoReport_t* AllocAndParseDeviceInfoReportFromIndex(int16_t DevIndex);


/**
 * @brief BleManager_SendAsynchWorkerEvent
 */
dxOS_RET_CODE BleManager_SendAsynchWorkerEvent (void* arg);


/**
 * @brief BleManager_IsDeviceGoingToMissing
 */
dxBOOL BleManager_IsDeviceGoingToMissing (void);


/**
 * @brief BleManager_IsJobQueueEmpty
 */
dxBOOL BleManager_IsJobQueueEmpty (void);


/**
 * @brief BleManagerForceUpdateAdvRxTimeInKeeperList
 */
void BleManagerForceUpdateAdvRxTimeInKeeperList(void);


/**
 * @brief BleGet128bitCharacteristicHandler
 */
BLEManager_GenJobProcess_t BleGet128bitCharacteristicHandler (DeviceAddress_t* DeviceAddressIn, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t* CharHandlerIDOut);


/**
 * @brief BleRead128bitCharacteristic
 */
BLEManager_GenJobProcess_t BleRead128bitCharacteristic (uint16_t CharHandlerID, uint16_t HandShakeID, BleDeviceReadCharRspMsg_t* ResponseData);


/**
 * @brief BleWrite128bitCharacteristic
 */
BLEManager_GenJobProcess_t BleWrite128bitCharacteristic (uint16_t CharHandlerID, uint16_t HandShakeID, uint8_t* DataToWrite, uint16_t DataLen, dxBOOL* OperationSuccess);
