//============================================================================
// File: BLERepeater.c
//
// Author: John Lin
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "dxBSP.h"

#include "SimpleTimedStateMachine.h"

#include "BLEManager.h"
#include "BLEDeviceCentralControl.h"
#include "BLEDeviceBridgeComm.h"
#include "Utils.h"
#include "dk_PeripheralAux.h"
#include "dk_Peripheral.h"
#include "BLEDeviceKeeper.h"
#include "BLERepeater.h"



//============================================================================
//    Define
//============================================================================

#define BLE_REPEATER_STACK_SIZE         768
#define REPEATER_RETRY_COUNT            3
#define CHARS_NG_RETRY_COUNT            2
#define CONNECT_NG_RETRY_COUNT          15
#define DISCONNECT_NG_RETRY_COUNT       5
#define REPEATER_SETUP_TIMEOUT          10000
#define REPEATER_MONITOR_TIMEOUT        200000
#define REPEATER_READ_HANDSHAKE_ID      0x5566
#define REPEATER_WRITE_HANDSHAKE_ID     0x7788
#define REPEATER_ADVERTISE_MAX_NUM      60



//============================================================================
//    Macros
//============================================================================

#define error(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, __VA_ARGS__);
#define debug(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, __VA_ARGS__);
#define trace(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, __VA_ARGS__);



//============================================================================
//    Enumerations
//============================================================================

 typedef enum {
     BLE_REPEATER_INIT = 0,
     BLE_REPEATER_SETUP,
     BLE_REPEATER_MONITOR,
     BLE_REPEATER_WAIT,
     BLE_REPEATER_RESTART,

 } BleRepeaterTimedState_t;



//============================================================================
//    Structures
//============================================================================

typedef DX_PACK_STRUCT_BEGIN {
    uint8_t addr[B_ADDR_LEN_8];
    uint8_t addr_type;
    uint8_t device_index;
} mac_reg_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t sec_key[KEY_SIZE];
    uint8_t device_index;
} sec_key_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t bd_addr[B_ADDR_LEN_8];
    uint8_t addr_type;
    int8_t rssi;
    uint8_t gap_flags;
    uint8_t product_id;
    uint16_t company_id;
    uint16_t dk_flag;
    uint8_t ext_dk_flag;
    uint8_t battery_info;
    uint16_t paging_info;
} adv_header_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t payload[20];
} adv_payload_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t status;
    uint8_t bd_addr[8];
    uint8_t addr_type;
} connect_res_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint16_t service_id;
    uint16_t char_id;
} discover_req_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t status;
    uint16_t handle;
} discover_res_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t operation;
    uint16_t handle;
    uint8_t value_length;
} char_access_req_header_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t payload[CHAR_READ_WRITE_DATA_MAX];
} char_access_req_payload_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t status;
    uint8_t value_length;
} char_access_res_header_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t payload[CHAR_READ_WRITE_DATA_MAX];
} char_access_res_payload_t;


typedef DX_PACK_STRUCT_BEGIN {
    DeviceInfo_t device_info;
    uint8_t payload[40];
} adv_info_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint8_t scan_start;
    uint8_t scan_type;
    uint8_t stop_auto_filter;
} scan_switch_t;



//============================================================================
//    Function Declarations
//============================================================================

/*---- In Job Function -----------------------------------------------------*/

BLEManager_GenJobProcess_t _BleRepeater_InJobFindDevice (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobStartScanDevice (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobSetCentralToIdle (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobCentralConnectToDevice (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_JobSecurityClearance (instance_data_t* data, dxBOOL SecurityKeyInInstanceContainer);
BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityClearance (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityClearanceGivenKey (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyClear (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyExchange (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyExchangeAndSaveForReport (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobDataTypePayloadRead (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicWrite (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicRead (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicReadAggregatedUpdate (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobFwVersionRead (instance_data_t* data);
BLEManager_GenJobProcess_t _BleRepeater_InJobCleanUpAsCentral (instance_data_t* data);


/*---- State Function ------------------------------------------------------*/

SimpleStateMachine_result_t _BleRepeater_StateMachine_Init (void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t _BleRepeater_StateMachine_Setup (void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t _BleRepeater_StateMachine_Monitor (void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t _BleRepeater_StateMachine_Wait (void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t _BleRepeater_StateMachine_Restart (void* data, dxBOOL isFirstCall);


/*---- Callback Function ---------------------------------------------------*/

void _BleRepeater_EventReportHandler (void* arg);


/*---- Private Function ----------------------------------------------------*/

static void _BleRepeater_Task (void *arg);
static BLEManager_GenJobProcess_t _BleRepeater_RegisterWatchList (DeviceAddress_t *p_device_addr, uint8_t *p_key);
static BLEDeviceCtrl_result_t _BleRepeater_SendCommand (uint8_t message_type, uint8_t command_aux, uint8_t payload_len, uint8_t *payload);
static BLEManager_GenJobProcess_t _BleRepeater_ConnectPeripheral (DeviceAddress_t* deviceAddress);
static BLEManager_GenJobProcess_t _BleRepeater_DisconnectPeripheral (DeviceAddress_t* device_addr);
static uint16_t _BleRepeater_GetPeripheralCharacteristicHandle (DeviceAddress_t* device_address_in, uint16_t service_id, uint16_t char_id);
static BLEDeviceCtrl_result_t _BleRepeater_WritePeripheralCharacteristic (uint16_t char_handle, uint8_t write_length, uint8_t *p_content);
static BLEDeviceCtrl_result_t _BleRepeater_ReadPeripheralCharacteristic (uint16_t char_handle, uint8_t *read_length, uint8_t **p_content);
static uint8_t _BleRepeater_IsAddressZero (uint8_t *p_input, uint16_t length);



//============================================================================
//    Global Variable
//============================================================================

/*---- From BLEManager.c ---------------------------------------------------*/

extern dxAesContext_t   Aes128ctx_ble_enc;
extern dxAesContext_t   Aes128ctx_ble_dec;
extern dxQueueHandle_t  BleDeviceInJobCentralStateReportQueue;
extern dxQueueHandle_t  BleDeviceInJobReportQueue;
extern dxBOOL           gDeviceScanReportAllowed;
extern char             ContinueAutoScan;


/*---- Local global variables ----------------------------------------------*/

static dxTaskHandle_t m_ble_repeater_task_handle;
static uint8_t m_repeater_mode_on = FALSE;
static uint8_t m_repeater_delay_init = FALSE;
static uint8_t m_repeater_need_redo_pairing;
static uint8_t m_repeater_request_update_watch_list;
static uint8_t m_repeater_request_open_pairing_scanning;
static uint8_t m_repeater_write_single_watch_list;
static uint8_t m_repeater_open_pairing_scanning;
static uint8_t repeaterAddress[B_ADDR_LEN_8];
static dxTime_t repeaterLoopStartTime = 0;
static dxTime_t repeaterLoopInterval = 0;
static dxBOOL ignoreForceUpdate = dxFALSE;

// Handles
static uint16_t m_mac_reg_handle;
static uint16_t m_read_device_index_handle;
static uint16_t m_adv_header_handle;
static uint16_t m_adv_payload_handle;
static uint16_t m_connect_req_handle;
static uint16_t m_connect_res_handle;
static uint16_t m_discover_req_handle;
static uint16_t m_discover_res_handle;
static uint16_t m_char_access_req_header_handle;
static uint16_t m_char_access_req_payload_handle;
static uint16_t m_char_access_res_header_handle;
static uint16_t m_char_access_res_payload_handle;
static uint16_t m_scan_switch_handle;
static uint16_t m_sec_key_handle;
static uint16_t m_watch_mask_handle;
static uint16_t m_read_condition_index_handle;
static uint16_t m_clear_condition_handle;
static uint16_t m_watch_result_handle;
static uint16_t m_clear_adv_data_handle;
static uint16_t m_svto_handle;
static uint16_t m_dummy_handle;

static int16_t repeaterIndex = DEV_KEEPER_NOT_FOUND;
static uint8_t chars_ng_count = 0;

static BleRepeaterSetupState_t repeaterState = BLE_REPEATER_SETUP_NONE;
static dxMutexHandle_t repeaterMutex;
static uint8_t setupResult = 0;
static uint8_t monitorResult = 0;
static SimpleStateHandler bleRepeaterStateMachine = NULL;
static simple_st_machine_table_t BleRepeaterStateMachine_FuncTable[] = {
    {BLE_REPEATER_INIT, _BleRepeater_StateMachine_Init, 500},
    {BLE_REPEATER_SETUP, _BleRepeater_StateMachine_Setup, 500},
    {BLE_REPEATER_MONITOR, _BleRepeater_StateMachine_Monitor, 500},
    {BLE_REPEATER_WAIT, _BleRepeater_StateMachine_Wait, 500},
    {BLE_REPEATER_RESTART, _BleRepeater_StateMachine_Restart, 500},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};


state_func_table_t RepeaterStateFuncMap[] = {
    {DEV_MANAGER_ON_JOB_FIND_DEVICE, _BleRepeater_InJobFindDevice},
    {DEV_MANAGER_ON_JOB_START_SCAN, _BleRepeater_InJobStartScanDevice},
    {DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE, _BleRepeater_InJobSetCentralToIdle},
    {DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, _BleRepeater_InJobCentralConnectToDevice},
    {DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, _BleRepeater_InJobSecurityClearance},
    {DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE_GIVEN_KEY, _BleRepeater_InJobSecurityClearanceGivenKey},
    {DEV_MANAGER_ON_JOB_SECURITY_KEY_CLEAR, _BleRepeater_InJobSecurityKeyClear},
    {DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE, _BleRepeater_InJobSecurityKeyExchange},
    {DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE_AND_SAVE_FOR_REPORT, _BleRepeater_InJobSecurityKeyExchangeAndSaveForReport},
    {DEV_MANAGER_ON_JOB_DATATYPE_PAYLOAD_READ, _BleRepeater_InJobDataTypePayloadRead},
    {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, _BleRepeater_InJobGenericCharacteristicWrite},
    {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ, _BleRepeater_InJobGenericCharacteristicRead},
    {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ_AGGREGATED_UPDATE, _BleRepeater_InJobGenericCharacteristicReadAggregatedUpdate},
    {DEV_MANAGER_ON_JOB_FW_VERSION_READ, _BleRepeater_InJobFwVersionRead},
    {DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, _BleRepeater_InJobCleanUpAsCentral},
};



//============================================================================
//    Implementation
//============================================================================

/*---- Public Function -----------------------------------------------------*/

void BleRepeater_Init (dxEventHandler_t* eventFunction) {
    dxOS_RET_CODE ret;
    SimpleStateMachine_result_t stret;

    *eventFunction = NULL;
    repeaterMutex = dxMutexCreate ();

    ret = dxTaskCreate (
        _BleRepeater_Task,
        "Repeater_Task",
        BLE_REPEATER_STACK_SIZE,
        NULL,
        DXTASK_PRIORITY_HIGH,
        &m_ble_repeater_task_handle
    );
    if (ret != DXOS_SUCCESS) {
        debug ("FATAL: Fail to create task\r\n");
        goto exit;
    }

    GSysDebugger_RegisterForThreadDiagnostic (&m_ble_repeater_task_handle);

    bleRepeaterStateMachine =  SimpleStateMachine_Init (BleRepeaterStateMachine_FuncTable);
    if (bleRepeaterStateMachine == NULL) {
        debug ("FATAL: bleRepeaterStateMachine init Failed\r\n");
        goto exit;
    }

    stret = SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, BLE_REPEATER_INIT);
    if (stret != SMP_STATE_MACHINE_RET_SUCCESS) {
        debug ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d): %d\r\n", BLE_REPEATER_INIT, stret);
    }

    *eventFunction = _BleRepeater_EventReportHandler;

exit :

    return;
}


uint8_t BleRepeater_IsActive (void) {
    return (repeaterState > BLE_REPEATER_SETUP_FOUND);
}


uint8_t BleRepeater_IsConnected (void) {
    return (repeaterState == BLE_REPEATER_SETUP_CONNECTED);
}


dxOS_RET_CODE BleRepeater_TryActivate (uint8_t *adv_info, uint16_t adv_info_length, int16_t adv_device_index) {
    dxOS_RET_CODE retCode = DXOS_ERROR;
    int16_t device_index;

    if ((repeaterState > BLE_REPEATER_SETUP_FOUND) || ((device_index = BLEDevKeeper_GetRepeaterIndex ()) == EOF) || m_repeater_delay_init) {
        goto exit;
    }

    if (device_index != adv_device_index || (repeaterState != BLE_REPEATER_SETUP_FOUND)) {
        goto exit;
    }

    ResetBleModule ();

    dxMutexTake (repeaterMutex, DXOS_WAIT_FOREVER);
    repeaterState = BLE_REPEATER_SETUP_ACTIVE;
    dxMutexGive (repeaterMutex);

    retCode = DXOS_SUCCESS;
    debug ("repeater activated. \r\n");

exit :

    return retCode;
}


blemanager_state_func_t* BleRepeater_GetStateFunction (BLEManager_GenJobProcess_t input_sequence) {
    blemanager_state_func_t* stateFunction = NULL;
    state_func_table_t* FuncMap = &RepeaterStateFuncMap[0];
    uint16_t i;

    if (!BleRepeater_IsConnected ()) {
        goto exit;
    }

    for (i = 0; i < sizeof (RepeaterStateFuncMap) / sizeof (state_func_table_t); i++) {
        if (FuncMap->State == InJobSequence[CurrentInJobStageIndex]) {
            stateFunction = FuncMap->StateFunction;
            goto exit;
        }
        FuncMap++;
    }

exit :

    return stateFunction;
}


dxOS_RET_CODE BleRepeater_Reset (void) {
    int16_t device_index;

    if (repeaterState > BLE_REPEATER_SETUP_FOUND) {
        dxMutexTake (repeaterMutex, DXOS_WAIT_FOREVER);
        repeaterState = BLE_REPEATER_SETUP_FOUND;
        monitorResult = -1;
        dxMutexGive (repeaterMutex);
    }
    debug ("repeater reset. \r\n");
    ResetBleModule ();
    return DXOS_SUCCESS;
}


void BleRepeater_TurnOn (DeviceAddress_t* deviceAddress) {

    if (repeaterState < BLE_REPEATER_SETUP_FOUND) {
        dxMutexTake (repeaterMutex, DXOS_WAIT_FOREVER);
        repeaterState = BLE_REPEATER_SETUP_FOUND;
        dxMutexGive (repeaterMutex);

        repeaterAddress[0] = deviceAddress->pAddr[7];
        repeaterAddress[1] = deviceAddress->pAddr[6];
        repeaterAddress[2] = deviceAddress->pAddr[5];
        repeaterAddress[3] = deviceAddress->pAddr[4];
        repeaterAddress[4] = deviceAddress->pAddr[3];
        repeaterAddress[5] = deviceAddress->pAddr[2];
        repeaterAddress[6] = deviceAddress->pAddr[1];
        repeaterAddress[7] = deviceAddress->pAddr[0];
    }
}


BLEDeviceCtrl_result_t BleRepeater_UpdateWatchList (void) {
    m_repeater_request_update_watch_list = true;
    m_repeater_open_pairing_scanning = false;
    return DEV_CENTRAL_CTRL_SUCCESS;
}


BLEDeviceCtrl_result_t BleRepeater_StartOpenPairingScanning (void) {
    if (repeaterState <= BLE_REPEATER_SETUP_FOUND) {
        return DEV_CENTRAL_CTRL_SUCCESS;
    }

    m_repeater_request_open_pairing_scanning = true;
    return DEV_CENTRAL_CTRL_SUCCESS;
}


BLEDeviceCtrl_result_t BleRepeater_StopOpenPairingScanning (void) {
    if (repeaterState <= BLE_REPEATER_SETUP_FOUND) {
        return DEV_CENTRAL_CTRL_SUCCESS;
    }

    if (m_repeater_open_pairing_scanning) {
        BleRepeater_UpdateWatchList ();
    }

    m_repeater_request_open_pairing_scanning = false;
    return DEV_CENTRAL_CTRL_SUCCESS;
}


void BleRepeater_NotifyAfterPairingDone (void) {
    m_repeater_need_redo_pairing = FALSE;
}


BLEManager_GenJobProcess_t BleRepeater_Connect (DeviceAddress_t* deviceAddress) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    BLEDeviceCtrl_result_t connectResult = DEV_CENTRAL_CTRL_SUCCESS;
    BleCentralStates_t* states = NULL;
    dxOS_RET_CODE retCode;

    if (!BleRepeater_IsActive ()) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    ResetBleModule ();

    deviceAddress->AddressType = ADDRTYPE_STATIC; // Work-around for repeater
    trace ("connet to repeater addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
            deviceAddress->pAddr[7],
            deviceAddress->pAddr[6],
            deviceAddress->pAddr[5],
            deviceAddress->pAddr[4],
            deviceAddress->pAddr[3],
            deviceAddress->pAddr[2],
            deviceAddress->pAddr[1],
            deviceAddress->pAddr[0]
    );

    connectResult = BLEDevCentralCtrl_ConnectToDevice (deviceAddress);
    if (connectResult == DEV_CENTRAL_CTRL_SUCCESS) {
        retCode = dxQueueReceive (BleDeviceInJobCentralStateReportQueue, &states, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS);
        if (retCode == DXOS_SUCCESS) {
            if (states->CentralState == BLE_STATE_CONNECTED) {
                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        } else {
            error ("InJobCentralConnectToDevicer central state pop TimeOut\r\n");
        }
        if (states) {
            dxMemFree (states);
        }
    } else {
        //Error, either memory alloc error or pushing to queue timeout
        transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }

exit :

    return transitionState;
}


BLEManager_GenJobProcess_t BleRepeater_PreloadHandle (void) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    dxBOOL operationSuccess;
    scan_switch_t scan_switch = {0, 0, 0};

    if (!BleRepeater_IsActive ()) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    m_mac_reg_handle                    = 0x10 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_read_device_index_handle          = 0x12 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_adv_header_handle                 = 0x14 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_adv_payload_handle                = 0x17 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_connect_req_handle                = 0x1a - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_connect_res_handle                = 0x1c - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_discover_req_handle               = 0x1e - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_discover_res_handle               = 0x20 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_char_access_req_header_handle     = 0x22 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_char_access_req_payload_handle    = 0x24 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_char_access_res_header_handle     = 0x26 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_char_access_res_payload_handle    = 0x28 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_scan_switch_handle                = 0x2a - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_sec_key_handle                    = 0x2c - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_watch_mask_handle                 = 0x2e - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_read_condition_index_handle       = 0x30 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_clear_condition_handle            = 0x32 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_watch_result_handle               = 0x34 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_clear_adv_data_handle             = 0x36 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_svto_handle                       = 0x38 - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    m_dummy_handle                      = 0x3a - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;

    // Turn-off scan to speed-up
    transitionState = BleWrite128bitCharacteristic (
        m_scan_switch_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        sizeof (scan_switch_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

exit :

    return transitionState;
}


BLEManager_GenJobProcess_t BleRepeater_PollToRegisterWatchList (void) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    if (m_repeater_write_single_watch_list) {
        m_repeater_request_update_watch_list = false;
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        goto exit;
    }

    if (!m_repeater_request_update_watch_list) {
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        goto exit;
    }

    transitionState = _BleRepeater_RegisterWatchList (NULL, NULL);
    if (transitionState == DEV_MANAGER_ON_JOB_NEXT_STATE) {
        m_repeater_request_update_watch_list = false;
    }

exit :

    return transitionState;
}


BLEManager_GenJobProcess_t BleRepeater_PollToOpenPairing (void) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    dxBOOL operationSuccess;
    scan_switch_t scan_switch = {0, 0, 0};

    if (m_repeater_write_single_watch_list) {
        m_repeater_request_open_pairing_scanning = false;
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        goto exit;
    }

    if (!m_repeater_request_open_pairing_scanning) {
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        goto exit;
    }

    if (!BleRepeater_IsActive ()) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    scan_switch.scan_start = TRUE;
    scan_switch.scan_type = REPEATER_SCAN_TYPE_OPEN_PAIR_SCANNING;//pairing scan

    transitionState = BleWrite128bitCharacteristic (
        m_scan_switch_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        sizeof (scan_switch_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }
    dxTimeDelayMS (50);

    transitionState = BleWrite128bitCharacteristic (
        m_clear_adv_data_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        1,
        &operationSuccess
    );
    if (!operationSuccess) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }
    dxTimeDelayMS (50);

    debug ("BleRepeater_StartOpenPairingScanning.\r\n");
    m_repeater_open_pairing_scanning = true;
    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

exit :

    m_repeater_request_open_pairing_scanning = false;

    return transitionState;
}


BLEManager_GenJobProcess_t BleRepeater_UpdateAdvertiseInfo (dxBOOL forceUpdate) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    const BleCentralStates_t* states = NULL;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxTime_t currentTime;
    adv_header_t adv_header;
    adv_payload_t adv_payload;
    adv_info_t adv_info;
    uint16_t read_length;
    uint8_t *p_read_content;
    uint8_t *p_composer;
    uint8_t check_sum, i;
    uint8_t count;

    if (!gDeviceScanReportAllowed) {
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        debug ("Scan report is not allowed\r\n");
        goto exit;
    }

    // Determine should we enter force update mode
    if (!forceUpdate) {
        if (BleManager_IsDeviceGoingToMissing ()) {
            if (repeaterLoopInterval > DEVICE_STATUS_CHECK_INTERVAL) {
                ignoreForceUpdate = ignoreForceUpdate ? dxFALSE : dxTRUE;  // Toggle this flag
                forceUpdate = ignoreForceUpdate;
            }
            debug ("BleManager_IsDeviceGoingToMissing, repeaterLoopInterval=%u, forceUpdate=%d\r\n", repeaterLoopInterval, forceUpdate);
        }
    }


    count = 0;
    while (count++ < REPEATER_ADVERTISE_MAX_NUM) {

        // Check job queue if not in force update mode
        if (!forceUpdate) {
            // If there is a new job in queue, skip update
            if (!BleManager_IsJobQueueEmpty ()) {
                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                debug ("JobQueue is not empty, exit\r\n");
                goto exit;
            }
        }

        transitionState = BleRead128bitCharacteristic (
            m_adv_header_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            goto exit;
        }
        memcpy (&adv_header, readCharResponse.DataRead, sizeof (adv_header));

        transitionState = BleRead128bitCharacteristic (
            m_adv_payload_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            goto exit;
        }
        memcpy (&adv_payload, readCharResponse.DataRead, sizeof (adv_payload));

        // No more update
        if (_BleRepeater_IsAddressZero (adv_header.bd_addr, B_ADDR_LEN_8)) {
            transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
            debug ("Get zero address, no more update\r\n");
            goto exit;
        }

        trace (
            "found adv pid=0x%x, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
            adv_header.product_id,
            adv_header.bd_addr[0],
            adv_header.bd_addr[1],
            adv_header.bd_addr[2],
            adv_header.bd_addr[3],
            adv_header.bd_addr[4],
            adv_header.bd_addr[5],
            adv_header.bd_addr[6],
            adv_header.bd_addr[7]
        );

        //compose adv_info
        adv_info.device_info.eventType = GAP_ADTYPE_ADV_IND;
        adv_info.device_info.addrType = adv_header.addr_type;
        memcpy (adv_info.device_info.addr, adv_header.bd_addr, B_ADDR_LEN_8);
        adv_info.device_info.rssi = adv_header.rssi;
        adv_info.device_info.dataLen = sizeof (adv_info.payload);

        memset (&adv_info.payload, 0, sizeof (adv_info.payload));//reset payload initially;
        adv_info.payload[0] = 0x02; //length
        adv_info.payload[1] = GAP_ADTYPE_FLAGS;
        adv_info.payload[2] = adv_header.gap_flags;
        adv_info.payload[3] = 31 - 4;
        adv_info.payload[4] = GAP_ADTYPE_MANUFACTURER_SPECIFIC;
        adv_info.payload[5] = (uint8_t) adv_header.company_id;
        adv_info.payload[6] = (uint8_t) (adv_header.company_id >> 8);
        adv_info.payload[7] = adv_header.product_id;
        adv_info.payload[9] = (uint8_t) adv_header.dk_flag;
        adv_info.payload[10] = (uint8_t) (adv_header.dk_flag >> 8);
        p_composer = &adv_info.payload[11];
        if (DK_FLAG_IS_CONTAIN_EXT_FLAG (adv_header.dk_flag)) {
            *p_composer++ = (uint8_t) adv_header.ext_dk_flag;
        }
        if (DK_FLAG_IS_CONTAIN_BATTERY_INFO (adv_header.dk_flag)) {
            *p_composer++ = (uint8_t) adv_header.battery_info;
        }
        if (DK_FLAG_IS_CONTAIN_PAGING_INFO (adv_header.dk_flag)) {
            *p_composer++ = (uint8_t) adv_header.paging_info;
            *p_composer++ = (uint8_t) (adv_header.paging_info >> 8);
        }
        if (DK_FLAG_IS_DEVICE_IN_GATEWAY_OPEN_PAIRING_MODE (adv_header.dk_flag)) {
            debug (
                "device in open pairing mode pid=0x%x, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                adv_header.product_id,
                adv_header.bd_addr[0],
                adv_header.bd_addr[1],
                adv_header.bd_addr[2],
                adv_header.bd_addr[3],
                adv_header.bd_addr[4],
                adv_header.bd_addr[5],
                adv_header.bd_addr[6],
                adv_header.bd_addr[7]
            );
        }
        check_sum = 0;
        for (i = 0; i < sizeof (adv_payload); i++) {
            check_sum ^= adv_payload.payload[i];
        }
        adv_info.payload[8] = check_sum;
        memcpy (p_composer, &adv_payload, sizeof (adv_payload));

        BLEManager_HandleAdvertiseInfo ((uint8_t*) &adv_info, sizeof (adv_info));

        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

        // Check if the loop is completed and calculate repeaterLoopInterval
        if (adv_header.product_id == DK_PRODUCT_TYPE_BLE_REPEATER_ID) {
            if (memcmp (repeaterAddress, adv_header.bd_addr, B_ADDR_LEN_8) == 0) {
                if (repeaterLoopStartTime != 0) {
                    currentTime = dxTimeGetMS ();
                    if (currentTime >= repeaterLoopStartTime) {
                        repeaterLoopInterval = currentTime - repeaterLoopStartTime;
                    } else {
                        repeaterLoopInterval = (UINT32_MAX - repeaterLoopStartTime) + currentTime;
                    }
                    repeaterLoopStartTime = currentTime;
                    trace ("Get repeater repeaterLoopInterval=%u, count=%d\r\n", repeaterLoopInterval, count);
                    goto exit;
                } else {
                    repeaterLoopStartTime = currentTime;
                }
            }
        }

        // Sleep a while for low priority task
        dxTimeDelayMS (10);

        states = BLEDevCentralCtrl_GetLastKnownConnectionStatus ();
        if (states && (states->CentralState == BLE_STATE_IDLE)) {
            error ("Invalid repeater state\r\n");
            goto exit;
        }
    }

exit :

    // After force update, reset repeaterLoopInterval
    if (forceUpdate) {
        repeaterLoopInterval = 0;
    }

    if (transitionState != DEV_MANAGER_ON_JOB_NEXT_STATE) {
        trace ("repeater monitor tick \r\n");
        if (transitionState > DEV_MANAGER_ON_JOB_NEXT_STATE) {
            // Check if error number is greater than CHARS_NG_RETRY_COUNT
            if (++chars_ng_count > CHARS_NG_RETRY_COUNT) {
                chars_ng_count = 0;
                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            } else {
                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        } else {
            // There is nothing wrong, but no adv data, try next time.
            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        }
    } else {
        chars_ng_count = 0;
    }

    return transitionState;
}


/*---- In Job Function -----------------------------------------------------*/

BLEManager_GenJobProcess_t _BleRepeater_InJobFindDevice (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    dxOS_RET_CODE  popResult = DXOS_ERROR;
    uint32_t waitInterval = 800;
    uint32_t waitTime = 0;

    trace ("Enter _BleRepeater_InJobFindDevice (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (BLEDevKeeper_StartFindingDeviceInfoMatchingAddr (data->DeviceAddress.pAddr , ADVERTISING_TYPE_ADV_IND, &BleDeviceInJobReportQueue) == DEV_KEEPER_SUCCESS) {
        m_repeater_write_single_watch_list = true;
        if (_BleRepeater_RegisterWatchList (&data->DeviceAddress, NULL) != DEV_MANAGER_ON_JOB_NEXT_STATE) {
            goto exit;
        }

        while ((popResult != DXOS_SUCCESS) && (waitTime < DEVICE_SEARCH_QUEUE_REPORT_WAIT_TIME_OUT_MS)) {
            // Must enter force update mode
            if (BleRepeater_UpdateAdvertiseInfo (dxTRUE) != DEV_MANAGER_ON_JOB_NEXT_STATE) {
                goto exit;
            }

            popResult = dxQueueReceive (BleDeviceInJobReportQueue,
                                       &data->DeviceInfoObj,
                                       waitInterval);
            waitTime += waitInterval;
       }



        m_repeater_write_single_watch_list = false;

        BleRepeater_UpdateWatchList ();
        if (BleRepeater_PollToRegisterWatchList () != DEV_MANAGER_ON_JOB_NEXT_STATE) {
            goto exit;
        }

        if (popResult == DXOS_SUCCESS) {
            DeviceManufaturerData_t DevManAdvData = BLEDevParser_GetManufacturerData (data->DeviceInfoObj);
            if (BLEDevParser_IsDeviceInOpenPairingMode (&DevManAdvData) || BLEDevParser_IsDeviceInStandAloneMode (&DevManAdvData)) {
                DeviceAddress_t DevAddress = BLEDevParser_GetAddress (data->DeviceInfoObj);
                if (DevAddress.AddrLen){
                    data->DeviceAddress.AddressType = DevAddress.AddressType;
                    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                } else {
                    // There is no address to connect... this should not happend
                    transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                }
            } else {
                transitionState = DEV_MANAGER_ON_JOB_FAILED_DEVICE_ALREADY_PAIRED;
            }
        } else {
            if (data->DeviceInfoObj) {
                dxMemFree (data->DeviceInfoObj);
            }
            data->DeviceInfoObj = NULL;
            BLEDevKeeper_StopFindingDeviceInfo ();
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobFindDevice (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobStartScanDevice (instance_data_t* data) {
    trace ("Enter _BleRepeater_InJobStartScanDevice (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);
    return DEV_MANAGER_ON_JOB_NEXT_STATE;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSetCentralToIdle (instance_data_t* data) {
    trace ("Enter _BleRepeater_InJobSetCentralToIdle (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);
    return DEV_MANAGER_ON_JOB_NEXT_STATE;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobCentralConnectToDevice (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    const DeviceInfoKp_t* deviceInfo;
    uint16_t index;
    uint8_t i;

    trace ("Enter _BleRepeater_InJobCentralConnectToDevice (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    index = BLEDevKeeper_GetRepeaterIndex ();
    deviceInfo = BLEDevKeeper_GetDeviceInfoFromIndex (index);
    if (!memcmp (deviceInfo->DevAddress.pAddr, data->DeviceAddress.pAddr, B_ADDR_LEN_8)) {
        if (data->DeviceAddress.AddressType == ADDRTYPE_STATIC) {
            debug ("connect repeater itself \r\n");
            ContinueAutoScan = 0; //Disable auto scan when stop is detected
            BleRepeater_Reset ();
            m_repeater_delay_init = TRUE;
            BLEDevCentralCtrl_ClearConnectionStatus ();
            while (dxQueueIsEmpty (BleDeviceInJobCentralStateReportQueue) != dxTRUE) {
                BleCentralStates_t* states = NULL;
                dxQueueReceive (BleDeviceInJobCentralStateReportQueue, &states, DXOS_WAIT_FOREVER);
                if (states) {
                    dxMemFree (states);
                }
            }
            dxTimeDelayMS (2000);
            transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
        }
    }

    if (transitionState != DEV_MANAGER_ON_JOB_NEXT_STATE) {
        goto exit;
    }

    transitionState = _BleRepeater_ConnectPeripheral (&data->DeviceAddress);

exit :

    return transitionState;

}


BLEManager_GenJobProcess_t _BleRepeater_JobSecurityClearance (instance_data_t* data, dxBOOL SecurityKeyInInstanceContainer) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_JobSecurityClearance (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        unsigned char   AesDataOut[AES_ENC_DEC_BYTE];
        dxBOOL          SendChallengePhrase = dxFALSE;
        uint8_t read_length;
        uint8_t *p_read_content;

        //Let's read the security access info
        if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
            if (read_length >= sizeof (SecurityAccessInfo_t)) {
                SecurityAccessInfo_t SecurityAccessInfo;
                memcpy (&SecurityAccessInfo, p_read_content, sizeof (SecurityAccessInfo_t));

                if (SecurityAccessInfo.ClearanceAccess == SECURITY_CLEARANCE_SUCCESS) {
                    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                } else { //We may need to challenge for clearance
                    if ((SecurityAccessInfo.SecurityLevel == SECURITY_LEVEL_PIN_CODE_HASH) &&
                        (SecurityAccessInfo.SecurityPayloadType == SECURITY_PAYLOAD_TYPE_CHALLENGE))
                    {
                        SecurityKeyCodeContainer_t* securityKey = NULL;

                        if (SecurityKeyInInstanceContainer == dxTRUE) {
                            securityKey = dxMemAlloc ("securityKey", 1, sizeof (SecurityKeyCodeContainer_t));
                            if (!securityKey) {
                                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                                goto exit;
                            }
                            memcpy (securityKey, &data->SecurityKeyCodeContainer, sizeof (SecurityKeyCodeContainer_t));
                        } else {
                            int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress (data->DeviceAddress);
                            if (DeviceIndex != DEV_KEEPER_NOT_FOUND) {
                                securityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc (DeviceIndex);
                            } else {
                                debug ("WARNING: InJobSecurityClearance DeviceIndex not found\r\n" );
                            }
                        }

                        if (securityKey) {
                            if (securityKey->Size > AES_ENC_DEC_BIT) {
                                debug ("WARNING: Security Key Size found too large\r\n");
                            }

                            dxAESSW_Setkey (Aes128ctx_ble_dec, securityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT);
                            dxAESSW_CryptEcb (Aes128ctx_ble_dec, DXAES_DECRYPT, SecurityAccessInfo.SecurityPayload, AesDataOut);

                            //Get the challenge number (uint32) and add one to it, then send it back
                            uint32_t ChallengeNumber = 0;
                            memcpy (&ChallengeNumber, AesDataOut, sizeof (ChallengeNumber) );
                            ChallengeNumber++;

                            unsigned char   AesDataIn[AES_ENC_DEC_BYTE];
                            memset (AesDataIn, 0 , sizeof (AesDataIn));
                            memcpy (AesDataIn, &ChallengeNumber, sizeof (ChallengeNumber));

                            dxAESSW_Setkey (Aes128ctx_ble_enc, securityKey->value, AES_ENC_DEC_BIT, DXAES_ENCRYPT);
                            dxAESSW_CryptEcb (Aes128ctx_ble_enc, DXAES_ENCRYPT, AesDataIn, AesDataOut);

                            SendChallengePhrase = dxTRUE;

                            dxMemFree (securityKey);

                        } else {

                            debug ("NOTE: Unable to comple security challenge due to missing security key\r\n" );
                            transitionState = DEV_MANAGER_ON_JOB_MISSING_SECURITY_KEY_ERROR;
                        }

                        if (SendChallengePhrase) {
                            SecurityAccessCommand_t SecurityAccessCommand;
                            SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHALLENGE;
                            SecurityAccessCommand.SubCommand = 0;
                            memcpy (SecurityAccessCommand.Payload, AesDataOut, sizeof (SecurityAccessCommand.Payload));

                            if (_BleRepeater_WritePeripheralCharacteristic (CharHandlerID, sizeof (SecurityAccessCommand_t), (uint8_t*)&SecurityAccessCommand) == DEV_CENTRAL_CTRL_SUCCESS) {
                                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                            } else {
                                transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
                            }
                        }
                    } else {
                        debug ("WARNING: unable to handle Securitylevel and/or SecurityPayloadType %d, %d, %d\r\n",
                                        SecurityAccessInfo.ClearanceAccess, SecurityAccessInfo.SecurityLevel, SecurityAccessInfo.SecurityPayloadType );
                        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                    }
                }
            } else {
                debug ("WARNING: Security access read returned (%d) different data len \r\n", read_length);
                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            }

            dxMemFree (p_read_content);
        } else {
            debug ("read char. fail \r\n");
            transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        }
    } else {
        debug ("cannot find handle %d\r\n", CharHandlerID);
        transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }

exit :

    trace ("Leave _BleRepeater_JobSecurityClearance (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityClearance (instance_data_t* data) {
    trace ("Enter _BleRepeater_InJobSecurityClearance (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);
    return _BleRepeater_JobSecurityClearance (data, dxFALSE);
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityClearanceGivenKey (instance_data_t* data) {
    trace ("Enter _BleRepeater_InJobSecurityClearanceGivenKey (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);
    return _BleRepeater_JobSecurityClearance (data, dxTRUE);
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyClear (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    //Need to read security access service first
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_InJobSecurityKeyClear (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        //Now we send the key exchange request providing the random value
        // NOTE: Porting to dxOS
        //wiced_bool_t OperationIsSuccess;
        dxBOOL OperationIsSuccess;
        SecurityAccessCommand_t SecurityAccessCommand;
        SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHANGE_SECURITY;
        SecurityAccessCommand.SubCommand = SYSTEM_UTIL_SA_CS_SUB_SECURITY_METHOD_NONE;
        memset (SecurityAccessCommand.Payload, 0, sizeof (SecurityAccessCommand.Payload));

        // NOTE: Porting to dxOS
        //if (OperationIsSuccess == WICED_TRUE)
        if (_BleRepeater_WritePeripheralCharacteristic (CharHandlerID, sizeof (SecurityAccessCommand_t), (uint8_t*)&SecurityAccessCommand) == DEV_CENTRAL_CTRL_SUCCESS) {
            //After we send the key let's read back the confirmation of encrypted random value for verification if the provided PIN Code is accurate
            uint8_t read_length;
            uint8_t *p_read_content;

            if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
                if (read_length == sizeof (SecurityAccessInfo_t)) {
                    SecurityAccessInfo_t SecurityAccessInfo;
                    memcpy (&SecurityAccessInfo, p_read_content, sizeof (SecurityAccessInfo_t));

                    if ((SecurityAccessInfo.SecurityLevel != SECURITY_LEVEL_NONE) || (SecurityAccessInfo.ClearanceAccess != SECURITY_CLEARANCE_SUCCESS)) {
                        debug ("WARNING: _BleRepeater_InJobSecurityKeyClear failed\r\n");
                    }

                    debug ("_BleRepeater_InJobSecurityKeyClear Done\r\n");

                    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                }

                dxMemFree (p_read_content);
            }
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobSecurityKeyClear (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyExchange (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    //Need to read security access service first
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_InJobSecurityKeyExchange (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        unsigned char   SecurityKey[AES_ENC_DEC_BYTE];
        unsigned char   RandomValue[AES_ENC_DEC_BYTE];
        unsigned char   AesDataInOut[AES_ENC_DEC_BYTE];

        // TODO: We are going to create a random 16 byte value, for now let's simply take the system time and use AES to generate the bytes
        dxTime_t time = dxTimeGetMS ();
        memcpy (AesDataInOut, &time, sizeof (time));
        uint8_t i = 0;
        for (i = 0; i < AES_ENC_DEC_BYTE; i++) {
            time++; SecurityKey[i] = ((uint8_t)time > 0) ? (uint8_t)time:1;
        }

        dxAESSW_Setkey (Aes128ctx_ble_enc, SecurityKey, AES_ENC_DEC_BIT, DXAES_ENCRYPT);
        dxAESSW_CryptEcb (Aes128ctx_ble_enc, DXAES_ENCRYPT, AesDataInOut, RandomValue);

        //Lets first calculate the key

        unsigned char* plainTextBuffer = dxMemAlloc ("PlainText For Hash", 1, AES_ENC_DEC_BYTE+SECURITY_KEY_CODE_MAX_SIZE);
        if (!plainTextBuffer) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            goto exit;
        }

        //Pad the security code to the end of random number
        memcpy (plainTextBuffer, RandomValue, AES_ENC_DEC_BYTE);
        memcpy (&plainTextBuffer[AES_ENC_DEC_BYTE], data->SecurityKeyCodeContainer.value, data->SecurityKeyCodeContainer.Size);

        //Generate the key
        md2_hmac ((unsigned char*)data->SecurityKeyCodeContainer.value, data->SecurityKeyCodeContainer.Size,
                  plainTextBuffer, AES_ENC_DEC_BYTE+data->SecurityKeyCodeContainer.Size,
                  SecurityKey);

        dxMemFree (plainTextBuffer);

        //Now we send the key exchange request providing the random value
        // NOTE: Porting to dxOS
        //wiced_bool_t OperationIsSuccess;
        dxBOOL OperationIsSuccess;
        SecurityAccessCommand_t SecurityAccessCommand;
        memset (&SecurityAccessCommand, 0, sizeof (SecurityAccessCommand));
        SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHANGE_SECURITY;
        SecurityAccessCommand.SubCommand = SYSTEM_UTIL_SA_CS_SUB_SECURITY_LEVEL_PIN_CODE_HASH;
        memcpy (SecurityAccessCommand.Payload, RandomValue, sizeof (SecurityAccessCommand.Payload));

        // NOTE: Porting to dxOS
        //if (OperationIsSuccess == WICED_TRUE)
        if (_BleRepeater_WritePeripheralCharacteristic (CharHandlerID, sizeof (SecurityAccessCommand_t), (uint8_t*)&SecurityAccessCommand) == DEV_CENTRAL_CTRL_SUCCESS) {
            //After we send the key let's read back the confirmation of encrypted random value for verification if the provided PIN Code is accurate
            uint8_t read_length;
            uint8_t *p_read_content;

            if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
                if (read_length == sizeof (SecurityAccessInfo_t)) {
                    SecurityAccessInfo_t SecurityAccessInfo;
                    memcpy (&SecurityAccessInfo, p_read_content, sizeof (SecurityAccessInfo_t));

                    if ((SecurityAccessInfo.SecurityLevel == SECURITY_LEVEL_PIN_CODE_HASH) &&
                        (SecurityAccessInfo.SecurityPayloadType == SECURITY_PAYLOAD_TYPE_NEW_PIN_CODE_HASH_KEY_VERIFY)) {

                        // NOTE: Porting to dxOS
                        //wiced_bool_t KeyExchangeSuccess = WICED_TRUE;
                        dxBOOL KeyExchangeSuccess = dxTRUE;

                        dxAESSW_Setkey (Aes128ctx_ble_dec, SecurityKey, AES_ENC_DEC_BIT, DXAES_DECRYPT);
                        dxAESSW_CryptEcb (Aes128ctx_ble_dec, DXAES_DECRYPT, SecurityAccessInfo.SecurityPayload, AesDataInOut);

                        //Check if the decrypted value is same as the random value we sent previously
                        uint8_t i = 0;
                        for (i = 0; i < AES_ENC_DEC_BYTE; i++) {
                            if (RandomValue[i] != AesDataInOut[i]) {
                                // NOTE: Porting to dxOS
                                //KeyExchangeSuccess = WICED_FALSE;
                                KeyExchangeSuccess = dxFALSE;
                                break;
                            }
                        }

                        // NOTE: Porting to dxOS
                        //if (KeyExchangeSuccess == WICED_TRUE)
                        if (KeyExchangeSuccess == dxTRUE) {
                            //NOTE: Since we are done using the Pin Code, we will now use the SecurityKeyCodeContainer buffer to store Security Key

                            //First clean up the container.
                            memset (&data->SecurityKeyCodeContainer, 0, sizeof (data->SecurityKeyCodeContainer));

                            //NOTE: Currently we only support Pin Code Hash based security and Key size is 16 byte. Bigger key size can be supported in the future and depends on platform (peripheral)
                            //We save the verified security key to SecurityKeyCodeContainer, this will be checked in the next stage (add peripheral to keeper list)
                            data->SecurityKeyCodeContainer.Size = AES_ENC_DEC_BYTE; //AES128 - 16 byte
                            memcpy (data->SecurityKeyCodeContainer.value, SecurityKey, AES_ENC_DEC_BYTE);

                            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                        } else {
                            //Since the PIN code is not valid and we already generated the key, we need to make sure we clear the security on
                            //the peripheral side s that the next key exchange with pin code can happen.
                            _BleRepeater_InJobSecurityKeyClear (data);

                            transitionState = DEV_MANAGER_ON_JOB_INVALID_PIN_CODE_ERROR;
                        }
                    } else {
                        debug ("WARNING: unexpected Securitylevel and/or SecurityPayloadType\r\n" );
                        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                    }

                }

                dxMemFree (p_read_content);
            }
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobSecurityKeyExchange (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobSecurityKeyExchangeAndSaveForReport (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = _BleRepeater_InJobSecurityKeyExchange (data);

    trace ("Enter _BleRepeater_InJobSecurityKeyExchangeAndSaveForReport (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (transitionState == DEV_MANAGER_ON_JOB_NEXT_STATE) {
        //Default to internal error
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;

        int16_t DevIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress (data->DeviceAddress);

        if (DevIndex != DEV_KEEPER_NOT_FOUND) {
            SecurityKeyCodeContainer_t* SecurityKey = (data->SecurityKeyCodeContainer.Size > 0) ? &data->SecurityKeyCodeContainer:NULL;

            BLEDevKeeper_UpdateDeviceSecurityKey (DevIndex, SecurityKey);

            int16_t* deviceIndexData = dxMemAlloc ("Device index Buff", 1, sizeof (int16_t));
            if (!deviceIndexData) {
                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                goto exit;
            }

            *deviceIndexData = DevIndex;
            if (data->pEventPayload) {
                debug ("WARNING: data->pEventPayload NOT NULL, possible mem leak\r\n");
            }
            data->pEventPayload = (uint8_t*) deviceIndexData;
            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobSecurityKeyExchangeAndSaveForReport (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobDataTypePayloadRead (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    uint16_t CharHandlerID;
    uint8_t read_length;
    uint8_t *p_read_content;
    int16_t DeviceIndex;

    trace ("Enter _BleRepeater_InJobDataTypePayloadRead (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

     CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_UUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
            DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress (data->DeviceAddress);

            if (DeviceIndex != DEV_KEEPER_NOT_FOUND) {
                // TODO: For now we only support "single" page adv mode read back... We will support multi page read back in near future
                if ((p_read_content[0] == 0) && (read_length == 20)) {
                    DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex (DeviceIndex);

                    if (DeviceReportInfo) {
                        //We overwrite to current time as we just got it.
                        // NOTE: Porting to dxOS
                        //wiced_time_get_time (&DeviceReportInfo->LastUpdatedTimeMs);
                        DeviceReportInfo->LastUpdatedTimeMs = dxTimeGetMS ();

                        //We overwrite the DataType payload with the latest and send back, Note that we are skipping the first byte which is header
                        DeviceReportInfo->DkAdvertisementData.AdDataLen = read_length - 1;
                        memset (DeviceReportInfo->DkAdvertisementData.AdData, 0, sizeof (DeviceReportInfo->DkAdvertisementData.AdData ) );
                        memcpy (DeviceReportInfo->DkAdvertisementData.AdData, &p_read_content[1], read_length - 1);

                        //If peripheral is encrypted (and SecurityKey found) then it means advertisement data is encrypted, we will need to
                        //decrypt it first. Note that only the first 16 byte is decrypted as the rest is non encrypted data according to
                        //Dexatek one page advertisement specification
                        if (DeviceReportInfo->Status.bits.Status_NoSecurity_b1 == 0) {
                            SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc (DeviceIndex);
                            if (SecurityKey) {
                                unsigned char   AesDataOut[AES_ENC_DEC_BYTE];

                                dxAESSW_Setkey (Aes128ctx_ble_dec, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT);
                                dxAESSW_CryptEcb (Aes128ctx_ble_dec, DXAES_DECRYPT, DeviceReportInfo->DkAdvertisementData.AdData, AesDataOut);

                                //Overwrite back the first 16 byte with decrypted data
                                memcpy (DeviceReportInfo->DkAdvertisementData.AdData, AesDataOut, AES_ENC_DEC_BYTE);
                                dxMemFree (SecurityKey);
                            }
                        }

                        EventReportObj_t* event = dxMemAlloc ("Event Report Msg Buff", 1, sizeof (EventReportObj_t));
                        if (!event) {
                            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                            goto exit;
                        }

                        event->Header.IdentificationNumber = 0;
                        event->Header.ReportEvent = DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT;
                        event->pData = (uint8_t*)DeviceReportInfo;

                        // NOTE: Porting to dxOS
                        //wiced_result_t EventResult = wiced_rtos_send_asynchronous_event (&EventWorkerThread, RegisteredEventFunction, event);
                        //if (EventResult != WICED_SUCCESS)
                        dxOS_RET_CODE EventResult = BleManager_SendAsynchWorkerEvent (event);
                        if (EventResult != DXOS_SUCCESS) {
                            debug ("WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Failed to send Event message back to Application\r\n" );
                            dxMemFree (DeviceReportInfo);
                            dxMemFree (event);
                        }
                    }
                } else {
                    debug ("WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Does NOT support Multi-Paging adv read in BLEManager\r\n" );
                }
            }

            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            dxMemFree (p_read_content);
        } else {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        }
    } else {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }

exit :

    trace ("Leave _BleRepeater_InJobDataTypePayloadRead (data={CentralState=%d}, success)\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicWrite (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_InJobGenericCharacteristicWrite (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (!data->MainCharWriteData.UUIDNeed128bitConversion) {
        debug ("WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, data->MainCharWriteData.ServiceUUID, data->MainCharWriteData.CharacteristicUUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        dxBOOL OperationIsSuccess;
        if (data->MainCharWriteData.DataWriteLen > CHAR_READ_WRITE_DATA_MAX) { //Larger than one transaction allowed?
            MultiTransacDataFormat_t    TransactionData;
            int16_t TotalSizetoTransfer = data->MainCharWriteData.DataWriteLen;
            uint8_t  TotalPage = ((data->MainCharWriteData.DataWriteLen % sizeof (TransactionData.Payload)) == 0) ?
                                        (data->MainCharWriteData.DataWriteLen / sizeof (TransactionData.Payload)) :
                                        (data->MainCharWriteData.DataWriteLen / sizeof (TransactionData.Payload) + 1);

            debug ("BLE Write TotalLen = %d (TotalPage = %d)\r\n", TotalSizetoTransfer, TotalPage);

            TransactionData.HeaderID  = MULTI_TRANSAC_HEADER_ID;
            TransactionData.PageInfo.bits.TotalPage_b0_3 = TotalPage;
            uint8_t CurrentPage = 1;
            uint8_t *pDataToTransac = data->MainCharWriteData.DataWrite;

            do {
                TransactionData.PageInfo.bits.CurrentPage_b4_7 = CurrentPage;
                TransactionData.ValidByte = (TotalSizetoTransfer > sizeof (TransactionData.Payload)) ? sizeof (TransactionData.Payload) : TotalSizetoTransfer;
                memset (TransactionData.Payload, 0, sizeof (TransactionData.Payload) );
                memcpy (TransactionData.Payload, pDataToTransac, TransactionData.ValidByte);

                if (_BleRepeater_WritePeripheralCharacteristic (CharHandlerID, sizeof (TransactionData), &TransactionData) != DEV_CENTRAL_CTRL_SUCCESS) {
                    OperationIsSuccess = FALSE;
                    break;
                } else {
                    OperationIsSuccess = TRUE;
                }

                debug ("BLE Write PageNum = %d, Len = %d)\r\n", CurrentPage, TransactionData.ValidByte);

                TotalSizetoTransfer -= TransactionData.ValidByte;
                if (TotalSizetoTransfer > 0) {
                    CurrentPage++;
                    pDataToTransac += TransactionData.ValidByte;
                }

            } while (TotalSizetoTransfer > 0);

        } else {

            if (_BleRepeater_WritePeripheralCharacteristic(CharHandlerID, data->MainCharWriteData.DataWriteLen, data->MainCharWriteData.DataWrite) != DEV_CENTRAL_CTRL_SUCCESS) {
                OperationIsSuccess = FALSE;
            } else {
                OperationIsSuccess = TRUE;
            }
        }

        if (!OperationIsSuccess) {
            //Error, either memory alloc error or pushing to queue timeout
            transitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        } else {
            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobGenericCharacteristicWrite (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicRead (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    dxBOOL  MorePage = dxTRUE;
    dxBOOL  MultiPageFormat = dxFALSE;
    uint8_t MultiPageTotalReceived = 0;
    uint8_t MultiPageExpectedPage = 1; //Always start at index 1
    DataReadReportObj_t*    readDataPayload = NULL;
    uint8_t*                pReadDataPayloadIndex = NULL;
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_InJobGenericCharacteristicRead (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (!data->MainCharReadData.UUIDNeed128bitConversion) {
        debug ("WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, data->MainCharReadData.ServiceUUID, data->MainCharReadData.CharacteristicUUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        while (MorePage) {
            uint8_t read_length;
            uint8_t *p_read_content;

            if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
                if (readDataPayload == NULL) {
                    readDataPayload = dxMemAlloc ("DataRead Buff", 1, sizeof (DataReadReportObj_t));
                    if (!readDataPayload) {
                        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                        goto exit;
                    }
                    pReadDataPayloadIndex = readDataPayload->Payload;
                }

                if (read_length) {
                    if (MultiPageFormat == dxFALSE) {
                        if (p_read_content[0] == MULTI_TRANSAC_HEADER_ID) {
                            MultiPageFormat = dxTRUE;
                        }
                    }

                    if (MultiPageFormat == dxTRUE) {
                        MultiTransacDataFormat_t* TransactionData = (MultiTransacDataFormat_t*)p_read_content;
                        if (TransactionData->PageInfo.bits.CurrentPage_b4_7 == MultiPageExpectedPage) {
                            uint8_t TotalDataLen = (TransactionData->ValidByte > sizeof (TransactionData->Payload)) ? sizeof (TransactionData->Payload):TransactionData->ValidByte;
                            readDataPayload->PayloadSize += TotalDataLen;

                            if (readDataPayload->PayloadSize > sizeof (readDataPayload->Payload)) {
                                debug ("WARNING: Multipage total data size exceed limitation!\r\n");
                                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                                MorePage = dxFALSE;
                            } else {
                                memcpy (pReadDataPayloadIndex, TransactionData->Payload, TotalDataLen);
                                pReadDataPayloadIndex += TotalDataLen;
                                MultiPageTotalReceived++;
                                MultiPageExpectedPage++;

                                if (MultiPageTotalReceived == TransactionData->PageInfo.bits.TotalPage_b0_3) {
                                    //All page received!
                                    data->pEventPayload = (uint8_t*)readDataPayload;
                                    MorePage = dxFALSE;
                                    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                                }
                            }
                        } else {
                            debug ("WARNING: MultiPage expected page not matched! (%d != %d)\r\n", TransactionData->PageInfo.bits.CurrentPage_b4_7, MultiPageExpectedPage);
                            transitionState = DEV_MANAGER_ON_JOB_FAILED_RECEIVING_MSG;
                            MorePage = dxFALSE;
                        }
                    } else {
                        uint16_t TotalDataLen = (read_length > sizeof (readDataPayload->Payload)) ? sizeof (readDataPayload->Payload):read_length;
                        memcpy (readDataPayload->Payload, p_read_content, TotalDataLen);
                        readDataPayload->PayloadSize = TotalDataLen;

                        data->pEventPayload = (uint8_t*) readDataPayload;
                        MorePage = dxFALSE;
                        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                }

                dxMemFree (p_read_content);
            } else {    //Failed - All or None
                transitionState = DEV_MANAGER_ON_JOB_FAILED_RECEIVING_MSG;
                MorePage = dxFALSE;
            }

        }
    }

    //If pEventPayload is NULL (not assigned) then we check if readDataPayload is allocated, if so we free it as it will not go anywhere since the access failed.
    if (data->pEventPayload == NULL) {
        if (readDataPayload)
            dxMemFree (readDataPayload);
    }

exit :

    trace ("Leave _BleRepeater_InJobGenericCharacteristicRead (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobGenericCharacteristicReadAggregatedUpdate (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    dxBOOL       MorePage = dxTRUE;
    uint8_t      ExpectedPageToRead = 1; //Start at 1 according to spec
    uint8_t      TotalPageAlreadyRead = 0;
    uint8_t*     reportPayload = NULL;
    uint8_t*     pOrgReportPayload = NULL;
    AggregatedReportHeader_t ReportHeader;
    uint16_t CharHandlerID;

    trace ("Enter _BleRepeater_InJobGenericCharacteristicReadAggregatedUpdate (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (!data->MainCharReadData.UUIDNeed128bitConversion) {
        debug ("WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, data->MainCharReadData.ServiceUUID, data->MainCharReadData.CharacteristicUUID);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        memset ((uint8_t*) &ReportHeader, 0 , sizeof (AggregatedReportHeader_t));
        while (MorePage) {

            MorePage = dxFALSE;

            uint8_t read_length;
            uint8_t *p_read_content;

            if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
                //We expect exactly 20 byte of data
                if (read_length >= CHAR_READ_WRITE_DATA_MAX) {
                    AggregatedDataHeader_t AgHeader;
                    memcpy (&AgHeader, p_read_content, sizeof (AggregatedDataHeader_t));

                    if (AgHeader.HeaderID == AGGREGATED_HEADER_ID) {
                        if ((reportPayload == NULL) && (AgHeader.PageInfo.bits.TotalPage_b0_3)) {
                            //Alloc Memory
                            reportPayload = dxMemAlloc ("Aggregated Report Msg Buff", 1, sizeof (AggregatedReportHeader_t) + AgHeader.PageInfo.bits.TotalPage_b0_3 * CHAR_READ_WRITE_DATA_MAX);
                            if (!reportPayload) {
                                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                                goto exit;
                            }

                            //Save the origin pointer
                            pOrgReportPayload = reportPayload;

                            //Copy the address
                            memcpy (ReportHeader.DevAddress.Addr , data->DeviceAddress.pAddr, data->DeviceAddress.AddrLen);

                            //Copy the header to reporter
                            memcpy (&ReportHeader.AggregatedItemHeader, &AgHeader, sizeof (AggregatedDataHeader_t));

                            //Skip the byte (according to size of AggregatedReportHeader_t) and actual aggregate data will start there
                            reportPayload += sizeof (AggregatedReportHeader_t);
                        }

                        if (reportPayload) {
                             if (AgHeader.PageInfo.bits.CurrentPage_b4_7 == ExpectedPageToRead) {
                                int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress (data->DeviceAddress);

                                if (DeviceIndex != DEV_KEEPER_NOT_FOUND) {
                                    DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex (DeviceIndex);

                                    if (DeviceReportInfo) {
                                        unsigned char   DataOut[AES_ENC_DEC_BYTE];
                                        dxBOOL  DataOutAvailable = dxTRUE;
                                        if (DeviceReportInfo->Status.bits.Status_NoSecurity_b1 == 0) {
                                            SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc (DeviceIndex);
                                            if (SecurityKey) {
                                                dxAESSW_Setkey (Aes128ctx_ble_dec, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                                                dxAESSW_CryptEcb (Aes128ctx_ble_dec, DXAES_DECRYPT, p_read_content + sizeof (AggregatedDataHeader_t), DataOut);
                                                dxMemFree (SecurityKey);
                                            } else {
                                                // NOTE: Porting to dxOS
                                                //DataOutAvailable = WICED_FALSE;
                                                DataOutAvailable = dxFALSE;
                                            }
                                        } else {
                                            //Overwrite back the last 16 byte of unencrypted data
                                            memcpy (DataOut, p_read_content + sizeof (AggregatedDataHeader_t), sizeof (DataOut));
                                        }

                                        if (DataOutAvailable) {

                                            //According to spec the Format 0 has fixed size of 8 bytes while Format 1 has fix size of 12 bytes
                                            uint8_t FormatSize = 8; //Default Format Size is 8 (FORMAT 0)
                                            if (ReportHeader.AggregatedItemHeader.FormatInfo.bits.AfDFormat_b0_3 == 1) {
                                                FormatSize = 12;
                                            }

                                            uint8_t  i = 0;
                                            while (i < sizeof (DataOut)) {
                                                if ((DataOut[i] != 0xFF) && (DataOut[i+1] != 0xFF)) {

                                                    memcpy (reportPayload, &DataOut[i], FormatSize);
                                                    reportPayload += FormatSize;
                                                    ReportHeader.TotalAggregatedItem++;
                                                }
                                                i += FormatSize;
                                            }
                                        }

                                        dxMemFree (DeviceReportInfo);
                                    }
                                }

                                TotalPageAlreadyRead++;

                                //Continue to next page if we have more
                                if (AgHeader.PageInfo.bits.CurrentPage_b4_7 < AgHeader.PageInfo.bits.TotalPage_b0_3) {
                                    // NOTE: Porting to dxOS
                                    //MorePage = WICED_TRUE;
                                    MorePage = dxTRUE;
                                    ExpectedPageToRead++;
                                }

                             } else {
                                 debug ("WARNING: Aggregated Data page read mismatched\r\n");
                             }
                        }

                    } else {
                        debug ("WARNING: Aggregated data header id missing\r\n");
                    }

                }

                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                dxMemFree (p_read_content);
            }

        }
    }

    if (reportPayload) {
        //ALL OR NONE
        if (ReportHeader.AggregatedItemHeader.PageInfo.bits.TotalPage_b0_3 == TotalPageAlreadyRead) {
            EventReportObj_t* event = dxMemAlloc ("Event Report Msg Buff", 1, sizeof (EventReportObj_t));
            if (event) {
                event->Header.IdentificationNumber = 0;
                event->Header.ReportEvent = DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT;

                //Copy header to beginning of Payload
                memcpy (pOrgReportPayload , &ReportHeader, sizeof (AggregatedReportHeader_t));

                event->pData = pOrgReportPayload;

                // NOTE: Porting to dxOS
                //wiced_result_t EventResult = wiced_rtos_send_asynchronous_event (&EventWorkerThread, RegisteredEventFunction, event);
                //if (EventResult != WICED_SUCCESS)
                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent (&BLEManager_EventWorkerThread, BLEManager_RegisteredEventFunction, event);
                if (EventResult != DXOS_SUCCESS) {
                    debug ("WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Failed to send Event message back to Application\r\n" );
                    dxMemFree (pOrgReportPayload);
                    dxMemFree (event);
                }
            }
        } else {
            dxMemFree (pOrgReportPayload); //NOTE: pOrgReportPayload = reportPayload
        }

    }

exit :

    trace ("Leave _BleRepeater_InJobGenericCharacteristicReadAggregatedUpdate (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobFwVersionRead (instance_data_t* data) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    uint16_t CharHandlerID;
    uint8_t read_length;
    uint8_t *p_read_content;

    trace ("Enter _BleRepeater_InJobFwVersionRead (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    CharHandlerID = _BleRepeater_GetPeripheralCharacteristicHandle (&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO);
    if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND) {
        if (_BleRepeater_ReadPeripheralCharacteristic (CharHandlerID, &read_length, &p_read_content) == DEV_CENTRAL_CTRL_SUCCESS) {
            if (read_length > 2) {
                uint16_t FwVersion = 0;
                memcpy (&FwVersion, p_read_content, 2);

                uint16_t MajorVer = (FwVersion & 0xFF00) >> 8;
                uint16_t MinorVer = (FwVersion & 0x00FF);

                data->DeviceFwVersion.Major = MajorVer;
                data->DeviceFwVersion.Middle = 0;
                data->DeviceFwVersion.Minor = MinorVer;

                debug ("_BleRepeater_InJobFwVersionRead FwVer = %d.%d.%d\r\n", data->DeviceFwVersion.Major, data->DeviceFwVersion.Middle, data->DeviceFwVersion.Minor);
                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }

            dxMemFree (p_read_content);
        }
    }

exit :

    trace ("Leave _BleRepeater_InJobFwVersionRead (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


BLEManager_GenJobProcess_t _BleRepeater_InJobCleanUpAsCentral (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    uint8_t i;

    trace ("Enter _BleRepeater_InJobCleanUpAsCentral (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        goto exit;
    }

    if (_BleRepeater_DisconnectPeripheral (&data->DeviceAddress) != DEV_MANAGER_ON_JOB_NEXT_STATE) {
        BleRepeater_Reset ();
    }

exit :

    trace ("Leave _BleRepeater_InJobCleanUpAsCentral (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


/*---- State Function ------------------------------------------------------*/

SimpleStateMachine_result_t _BleRepeater_StateMachine_Init (void* data, dxBOOL isFirstCall) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    BleRepeaterTimedState_t nextState = BLE_REPEATER_INIT; // Redo the same state

    trace ("_BleRepeater_StateMachine_Init (isFirstCall=%d)\r\n", isFirstCall);

    if (isFirstCall) {

        dxTimeDelayMS (1000);

        if (m_repeater_delay_init) {
            dxTimeDelayMS (15000);
            m_repeater_delay_init = FALSE;
        }

        m_repeater_request_update_watch_list = true;

    } else {

        if (repeaterState > BLE_REPEATER_SETUP_FOUND) {
            nextState = BLE_REPEATER_SETUP;
        }

    }

    const BleCentralStates_t* states = BLEDevCentralCtrl_GetLastKnownConnectionStatus ();
    trace ("states=%d, repeaterState=%d\r\n", states->CentralState, repeaterState);

    if (nextState != BLE_REPEATER_INIT) {
        if (SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, nextState) != SMP_STATE_MACHINE_RET_SUCCESS) {
            error ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d)\r\n", nextState);
        }
    }

    return result;
}


SimpleStateMachine_result_t _BleRepeater_StateMachine_Setup (void* data, dxBOOL isFirstCall) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    BLEDeviceCtrl_result_t ctrlResult;
    BleRepeaterTimedState_t nextState = BLE_REPEATER_SETUP; // Redo the same state
    static instance_data_t InJobInstanceData;

    trace ("_BleRepeater_StateMachine_Setup (isFirstCall=%d)\r\n", isFirstCall);

    if (isFirstCall) {

        int16_t index = BLEDevKeeper_GetRepeaterIndex ();
        if (index == EOF) {
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

        setupResult = 0;

        BLEManager_result_t managerResult = BleManagerSetupRepeaterTask_Asynch (index, REPEATER_SETUP_TIMEOUT);
        if (managerResult != DEV_MANAGER_SUCCESS) {
            debug ("Fail to call BleManagerSetupRepeaterTask_Asynch ()\r\n");
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

    } else {

        if (setupResult == 1) {
            debug ("repeater initiation done. \r\n");
            dxMutexTake (repeaterMutex, DXOS_WAIT_FOREVER);
            repeaterState = BLE_REPEATER_SETUP_CONNECTED;
            dxMutexGive (repeaterMutex);
            nextState = BLE_REPEATER_MONITOR;
            goto exit;
        }

        if (setupResult != 0) {
            debug ("Invalid setupResult\r\n");
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

    }

exit :

    if (nextState != BLE_REPEATER_SETUP) {
        if (SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, nextState) != SMP_STATE_MACHINE_RET_SUCCESS) {
            debug ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d)\r\n", nextState);
        }
    }

    return result;
}


SimpleStateMachine_result_t _BleRepeater_StateMachine_Monitor (void* data, dxBOOL isFirstCall) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    BleRepeaterTimedState_t nextState = BLE_REPEATER_MONITOR;
    int16_t keeper_list_index;

    trace ("_BleRepeater_StateMachine_Monitor (isFirstCall=%d)\r\n", isFirstCall);

    if (isFirstCall) {

        int16_t index = BLEDevKeeper_GetRepeaterIndex ();
        if (index == EOF) {
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

        monitorResult = 0;

        BLEManager_result_t managerResult = BleManagerMonitorRepeaterTask_Asynch (index, REPEATER_MONITOR_TIMEOUT);
        if (managerResult != DEV_MANAGER_SUCCESS) {
            debug ("Fail to call BleManagerMonitorRepeaterTask_Asynch ()\r\n");
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

    } else {

        if (monitorResult == 1) {
            nextState = BLE_REPEATER_WAIT;
            goto exit;
        }

        if (monitorResult != 0) {
            debug ("Invalid monitorResult\r\n");
            nextState = BLE_REPEATER_RESTART;
            goto exit;
        }

    }

exit :

    if (m_repeater_delay_init && (nextState == BLE_REPEATER_RESTART)) {
        nextState = BLE_REPEATER_INIT;
    }

    if (nextState != BLE_REPEATER_MONITOR) {
        if (SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, nextState) != SMP_STATE_MACHINE_RET_SUCCESS) {
            debug ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d): %d\r\n", nextState);
        }
    }

    return result;
}


SimpleStateMachine_result_t _BleRepeater_StateMachine_Wait (void* data, dxBOOL isFirstCall) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    BleRepeaterTimedState_t nextState = BLE_REPEATER_MONITOR;

    trace ("_BleRepeater_StateMachine_Wait (isFirstCall=%d)\r\n", isFirstCall);

    if (SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, nextState) != SMP_STATE_MACHINE_RET_SUCCESS) {
        debug ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d)\r\n", nextState);
    }

    return result;
}


SimpleStateMachine_result_t _BleRepeater_StateMachine_Restart (void* data, dxBOOL isFirstCall) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    trace ("_BleRepeater_StateMachine_Restart (isFirstCall=%d)\r\n", isFirstCall);

    if (isFirstCall) {

        if (!m_repeater_delay_init) {
            //end of repeater mode
            dxMutexTake (repeaterMutex, DXOS_WAIT_FOREVER);
            repeaterState = BLE_REPEATER_SETUP_NONE;
            dxMutexGive (repeaterMutex);
            ResetBleModule ();
        }

    } else {

        if (SimpleStateMachine_ExecuteState (bleRepeaterStateMachine, BLE_REPEATER_INIT) != SMP_STATE_MACHINE_RET_SUCCESS) {
            debug ("WARNING: SimpleStateMachine_ExecuteState (nextState=%d): %d\r\n", BLE_REPEATER_SETUP);
        }

    }

    return result;
}


/*---- Callback Function ---------------------------------------------------*/

void _BleRepeater_EventReportHandler (void* arg) {

    EventReportObj_t* event = (EventReportObj_t*) arg;

    switch (event->Header.ReportEvent) {

        case DEV_MANAGER_EVENT_BLE_REPEATER_SETUP_REPORT : {
            if (event->Header.ResultState == DEV_MANAGER_TASK_SUCCESS) {
                setupResult = 1;
            } else {
                setupResult = -1;
            }
        }
        break;

        case DEV_MANAGER_EVENT_BLE_REPEATER_MONITOR_REPORT : {
            if (event->Header.ResultState == DEV_MANAGER_TASK_SUCCESS) {
                monitorResult = 1;
            } else {
                monitorResult = -1;
            }
        }
        break;

        default : {
        }
        break;
    }

    //Always clean the arg object from event
    BleManagerCleanEventArgumentObj (arg);

}


/*---- Private Function ----------------------------------------------------*/

static void _BleRepeater_Task (void *arg) {

    while (1) {
        if (SimpleStateMachine_Process (bleRepeaterStateMachine) != SMP_STATE_MACHINE_RET_SUCCESS) {
            debug ("FATAL: SimpleStateMachine_Process failed, L=%d\r\n", __LINE__);
        }
        dxTimeDelayMS (10);
    }

}


static BLEManager_GenJobProcess_t _BleRepeater_RegisterWatchList (DeviceAddress_t* deviceAddress, uint8_t* securityKey) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    const BleCentralStates_t* states = NULL;
    dxBOOL operationSuccess;
    DeviceGenericInfo_t* deviceInfos;
    uint16_t deviceInfoNumber;
    int16_t i, j;
    mac_reg_t mac_reg;
    sec_key_t sec_key;
    scan_switch_t scan_switch = {0, 0, 0};

    if (!BleRepeater_IsActive ()) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    if (BLEDevKeeper_GetRepeaterIndex () == EOF) {
        BleRepeater_Reset ();
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    // Turn-off scan to speed-up
    transitionState = BleWrite128bitCharacteristic (
        m_scan_switch_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        sizeof (scan_switch_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    // Clear all record at first
    memset (&mac_reg, 0, sizeof (mac_reg));
    transitionState = BleWrite128bitCharacteristic (
        m_mac_reg_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &mac_reg,
        sizeof (mac_reg_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    if (!deviceAddress) {

        deviceInfos = dxMemAlloc ("deviceInfos", MAXIMUM_DEVICE_KEEPER_ALLOWED, sizeof (DeviceGenericInfo_t));
        if (deviceInfos == NULL) {
            goto exit;
        }

        // Set indivisual watch list
        deviceInfoNumber = BLEDevKeeper_GetAllDeviceGenericInfoFromKeeperList (deviceInfos, MAXIMUM_DEVICE_KEEPER_ALLOWED);
        for (i = 0; i < deviceInfoNumber; i++) {
            for (j = 0; j < B_ADDR_LEN_8; j++) {
                mac_reg.addr[j] = deviceInfos[i].DevAddress.Addr[7 - j];
            }
            mac_reg.addr_type = ADDRTYPE_PUBLIC;
            mac_reg.device_index = i + 1;//add 1 to avoid 0 in device index

            debug ("register watch list, mag_reg={index=%d, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x}\r\n",
                    mac_reg.device_index,
                    mac_reg.addr[0],
                    mac_reg.addr[1],
                    mac_reg.addr[2],
                    mac_reg.addr[3],
                    mac_reg.addr[4],
                    mac_reg.addr[5],
                    mac_reg.addr[6],
                    mac_reg.addr[7]
            );

            transitionState = BleWrite128bitCharacteristic (
                m_mac_reg_handle,
                REPEATER_WRITE_HANDSHAKE_ID,
                &mac_reg,
                sizeof (mac_reg_t),
                &operationSuccess
            );
            if (!operationSuccess) {
                error ("Fail to call BleWrite128bitCharacteristic (m_mac_reg_handle) \r\n");
                break;
            }
            dxTimeDelayMS (50);

            memcpy (sec_key.sec_key, deviceInfos[i].SecurityKey.value, sizeof (sec_key.sec_key));
            sec_key.device_index = mac_reg.device_index;
            transitionState = BleWrite128bitCharacteristic (
                m_sec_key_handle,
                REPEATER_WRITE_HANDSHAKE_ID,
                &sec_key,
                sizeof (sec_key_t),
                &operationSuccess
            );
            if (!operationSuccess) {
                error ("Fail to call BleWrite128bitCharacteristic (m_sec_key_handle) \r\n");
                break;
            }
            dxTimeDelayMS (50);

            states = BLEDevCentralCtrl_GetLastKnownConnectionStatus ();
            if (states && (states->CentralState == BLE_STATE_IDLE)) {
                error ("Invalid repeater state\r\n");
                break;
            }
        }

        if (deviceInfos != NULL) {
            dxMemFree (deviceInfos);
        }

        // Check if register all entries in watch list
        if (i != deviceInfoNumber) {
            transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
            goto exit;
        }

    } else {

        for (j = 0; j < B_ADDR_LEN_8; j++) {
            mac_reg.addr[j] = deviceAddress->pAddr[7 - j];
        }
        mac_reg.addr_type = deviceAddress->AddressType;
        mac_reg.device_index = 1;

        debug ("register single in watch list, mag_reg={index=%d, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x}\r\n",
                mac_reg.device_index,
                mac_reg.addr[0],
                mac_reg.addr[1],
                mac_reg.addr[2],
                mac_reg.addr[3],
                mac_reg.addr[4],
                mac_reg.addr[5],
                mac_reg.addr[6],
                mac_reg.addr[7]
        );

        transitionState = BleWrite128bitCharacteristic (
            m_mac_reg_handle,
            REPEATER_WRITE_HANDSHAKE_ID,
            &mac_reg,
            sizeof (mac_reg_t),
            &operationSuccess
        );
        if (!operationSuccess) {
            goto exit;
        }

        if (securityKey) {
            memcpy (sec_key.sec_key, securityKey, sizeof (sec_key.sec_key));
            sec_key.device_index = mac_reg.device_index;
            transitionState = BleWrite128bitCharacteristic (
                m_sec_key_handle,
                REPEATER_WRITE_HANDSHAKE_ID,
                &sec_key,
                sizeof (sec_key_t),
                &operationSuccess
            );
            if (!operationSuccess) {
                goto exit;
            }
        }
    }

    // Clear all condition
    transitionState = BleWrite128bitCharacteristic (
        m_clear_condition_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        2,
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    // Start scan
    scan_switch.scan_type = REPEATER_SCAN_TYPE_WATCHED_LIST_SCANNING;
    scan_switch.scan_start = TRUE;
    transitionState = BleWrite128bitCharacteristic (
        m_scan_switch_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        sizeof (scan_switch_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    // Clear adv. data
    transitionState = BleWrite128bitCharacteristic (
        m_clear_adv_data_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &scan_switch,
        1,
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    debug ("BleRepeater_UpdateWatchList () done.\r\n");

exit :

    return transitionState;
}


static BLEDeviceCtrl_result_t _BleRepeater_SendCommand (uint8_t message_type, uint8_t command_aux, uint8_t payload_len, uint8_t *payload) {
    BleBridgeCommMsg_t  SendMsg;
    SendMsg.Header.MessageType = message_type;
    SendMsg.Header.CommandAux = command_aux;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = payload_len;
    if (payload_len) {
        memcpy (SendMsg.Payload, payload, payload_len);
    }
    return SendRequestToMessageQueue (&SendMsg);
}


static BLEManager_GenJobProcess_t _BleRepeater_ConnectPeripheral (DeviceAddress_t* deviceAddress) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxBOOL operationSuccess;
    uint8_t temp_buf[B_ADDR_LEN_8 + 1];
    uint8_t count;
    uint8_t i;
    connect_res_t connect_res;

    trace (
        "Enter _BleRepeater_ConnectPeripheral (deviceAddress={pAddr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x})\r\n",
        deviceAddress->pAddr[7],
        deviceAddress->pAddr[6],
        deviceAddress->pAddr[5],
        deviceAddress->pAddr[4],
        deviceAddress->pAddr[3],
        deviceAddress->pAddr[2],
        deviceAddress->pAddr[1],
        deviceAddress->pAddr[0]
    );

    for (i = 0; i < B_ADDR_LEN_8; i++) {
        temp_buf[i] = deviceAddress->pAddr[7 - i];
    }
    temp_buf[8] = (uint8_t) deviceAddress->AddressType;

    transitionState = BleWrite128bitCharacteristic (
        m_connect_req_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        temp_buf,
        B_ADDR_LEN_8 + 1,
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    count = 0;
    while ((count++ < CONNECT_NG_RETRY_COUNT) && (transitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE)) {
        transitionState = BleRead128bitCharacteristic (
            m_connect_res_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            break;
        }

        if (readCharResponse.DataReadLen < sizeof (connect_res_t)) {
            break;
        }

        memcpy (&connect_res, readCharResponse.DataRead, sizeof (connect_res_t));
        if (connect_res.status == REPEATER_STATUS_PENDING) {
            // TODO: Need to sleep to avoid hard fault error, find out the root cuase in the future.
            dxTimeDelayMS (100);
            continue;
        }

        if (connect_res.status == REMOTE_CONNECTION_CONNECTED
            && !memcmp (connect_res.bd_addr, temp_buf, 8)
            && connect_res.addr_type == (uint8_t) temp_buf[8])
        {
            if (_BleRepeater_GetPeripheralCharacteristicHandle (NULL, DK_SERVICE_SYSTEM_UTILS_UUID, NULL)) {
                debug ("remote connect ok \r\n");
                transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                break;
            } else {
                break;
            }
        } else {
            break;
        }
    }

exit :

    trace (
        "Leave _BleRepeater_ConnectPeripheral (deviceAddress={pAddr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x}), transitionState=%d\r\n",
        deviceAddress->pAddr[7],
        deviceAddress->pAddr[6],
        deviceAddress->pAddr[5],
        deviceAddress->pAddr[4],
        deviceAddress->pAddr[3],
        deviceAddress->pAddr[2],
        deviceAddress->pAddr[1],
        deviceAddress->pAddr[0],
        transitionState
    );

    return transitionState;
}


static BLEManager_GenJobProcess_t _BleRepeater_DisconnectPeripheral (DeviceAddress_t* deviceAddress) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxBOOL operationSuccess;
    uint8_t temp_buf[B_ADDR_LEN_8 + 1];
    uint8_t count;
    uint8_t i;
    connect_res_t connect_res;

    trace (
        "Enter _BleRepeater_DisconnectPeripheral (deviceAddress={pAddr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x})\r\n",
        deviceAddress->pAddr[7],
        deviceAddress->pAddr[6],
        deviceAddress->pAddr[5],
        deviceAddress->pAddr[4],
        deviceAddress->pAddr[3],
        deviceAddress->pAddr[2],
        deviceAddress->pAddr[1],
        deviceAddress->pAddr[0]
    );

    memset (temp_buf, 0 , sizeof (temp_buf));
    transitionState = BleWrite128bitCharacteristic (
        m_connect_req_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        temp_buf,
        B_ADDR_LEN_8 + 1,
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    for (i = 0; i < B_ADDR_LEN_8; i++) {
        temp_buf[i] = deviceAddress->pAddr[7 - i];
    }
    temp_buf[8] = (uint8_t) deviceAddress->AddressType;

    count = 0;
    while ((count++ < DISCONNECT_NG_RETRY_COUNT) && (transitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE)) {
        transitionState = BleRead128bitCharacteristic (
            m_connect_res_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            break;
        }

        if (readCharResponse.DataReadLen < sizeof (connect_res_t)) {
            break;
        }

        memcpy (&connect_res, readCharResponse.DataRead, sizeof (connect_res_t));
        if (connect_res.status == REPEATER_STATUS_PENDING || connect_res.status == REMOTE_CONNECTION_CONNECTED) {
            // TODO: Need to sleep to avoid hard fault error, find out the root cuase in the future.
            dxTimeDelayMS (100);
            continue;
        }

        if (connect_res.status == REMOTE_CONNECTION_DISCONNECTED)
            //&& !memcmp (connect_res.bd_addr, temp_buf, 8)
            //&& connect_res.addr_type == (uint8_t) temp_buf[8])
        {
            debug ("remote disconnect ok \r\n");
            transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            break;
        } else {
            break;
        }
    }

exit :

    trace (
        "Leave _BleRepeater_DisconnectPeripheral (deviceAddress={pAddr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x}), transitionState=%d\r\n",
        deviceAddress->pAddr[7],
        deviceAddress->pAddr[6],
        deviceAddress->pAddr[5],
        deviceAddress->pAddr[4],
        deviceAddress->pAddr[3],
        deviceAddress->pAddr[2],
        deviceAddress->pAddr[1],
        deviceAddress->pAddr[0],
        transitionState
    );

    return transitionState;
}


static uint16_t _BleRepeater_GetPeripheralCharacteristicHandle (DeviceAddress_t* device_address_in, uint16_t service_id, uint16_t char_id) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxBOOL operationSuccess;
    uint16_t handle = 0x0000;
    discover_req_t discover_req;
    discover_res_t discover_res;

    discover_req.service_id = service_id;
    discover_req.char_id = char_id;

    if (device_address_in) {
        if (discover_res.handle = BLEDevKeeper_GetCharacteristicHandlerID (device_address_in, service_id, char_id, 1)) {
            handle = discover_res.handle;
            debug ("remote discover res from DevKeeper 0x%x\r\n", handle);
            goto exit;
        }
    }

    transitionState = BleWrite128bitCharacteristic (
        m_discover_req_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &discover_req,
        sizeof (discover_req_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    while (transitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
        transitionState = BleRead128bitCharacteristic (
            m_discover_res_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            break;
        }

        if (readCharResponse.DataReadLen < sizeof (discover_res_t)) {
            break;
        }

        memcpy (&discover_res, readCharResponse.DataRead, sizeof (discover_res_t));
        if (discover_res.status == REPEATER_STATUS_PENDING) {
            // TODO: Need to sleep to avoid hard fault error, find out the root cuase in the future.
            dxTimeDelayMS (100);
            continue;
        }

        if (discover_res.status == REPEATER_STATUS_SUCCESS
            && discover_res.handle != 0)
        {
            handle = discover_res.handle - CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
            debug ("remote discover res 0x%x\r\n", handle);
            BLEDevKeeper_SaveCharacteristicHandlerID (device_address_in, service_id, char_id, handle, 1);
            break;
        } else {
            break; // error occur
        }
    }

exit :

    return handle;
}


static BLEDeviceCtrl_result_t _BleRepeater_WritePeripheralCharacteristic (uint16_t char_handle, uint8_t write_length, uint8_t* p_content) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxBOOL operationSuccess;
    BLEDeviceCtrl_result_t ctrlResult = DEV_CENTRAL_CTRL_ERROR;
    char_access_req_header_t char_access_req_header;
    char_access_res_header_t char_access_res_header;

    transitionState = BleWrite128bitCharacteristic (
        m_char_access_req_payload_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        p_content,
        write_length,
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    char_access_req_header.operation = 0;//write
    char_access_req_header.handle = char_handle + CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    char_access_req_header.value_length = write_length;
    transitionState = BleWrite128bitCharacteristic (
        m_char_access_req_header_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &char_access_req_header,
        sizeof (char_access_req_header_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    while (transitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
        transitionState = BleRead128bitCharacteristic (
            m_char_access_res_header_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            break;
        }

        if (readCharResponse.DataReadLen < sizeof (char_access_res_header_t)) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            break;
        }

        memcpy (&char_access_res_header, readCharResponse.DataRead, sizeof (char_access_res_header_t));
        if (char_access_res_header.status == REPEATER_STATUS_PENDING) {
            // TODO: Need to sleep to avoid hard fault error, find out the root cuase in the future.
            dxTimeDelayMS (100);
            continue;
        }

        if (char_access_res_header.status == REPEATER_STATUS_SUCCESS) {
            trace ("remote write res \r\n");
            ctrlResult = DEV_CENTRAL_CTRL_SUCCESS;
            break;
        } else {
            break; // error occur
        }
    }

exit :

    return ctrlResult;
}


static BLEDeviceCtrl_result_t _BleRepeater_ReadPeripheralCharacteristic (uint16_t char_handle, uint8_t *read_length, uint8_t **content) {
    BLEManager_GenJobProcess_t  transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    BleDeviceReadCharRspMsg_t readCharResponse;
    dxBOOL operationSuccess;
    BLEDeviceCtrl_result_t ctrlResult = DEV_CENTRAL_CTRL_ERROR;
    char_access_req_header_t char_access_req_header;
    char_access_res_header_t char_access_res_header;
    char_access_res_payload_t char_access_res_payload;

    char_access_req_header.operation = 1;//read
    char_access_req_header.handle = char_handle + CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    char_access_req_header.value_length = 0;//don't care
    transitionState = BleWrite128bitCharacteristic (
        m_char_access_req_header_handle,
        REPEATER_WRITE_HANDSHAKE_ID,
        &char_access_req_header,
        sizeof (char_access_req_header_t),
        &operationSuccess
    );
    if (!operationSuccess) {
        goto exit;
    }
    dxTimeDelayMS (50);

    while (transitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
        transitionState = BleRead128bitCharacteristic (
            m_char_access_res_header_handle,
            REPEATER_READ_HANDSHAKE_ID,
            &readCharResponse
        );
        if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            break;
        }

        if (readCharResponse.DataReadLen < sizeof (char_access_res_header_t)) {
            transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            break;
        }

        memcpy (&char_access_res_header, readCharResponse.DataRead, sizeof (char_access_res_header_t));
        if (char_access_res_header.status == REPEATER_STATUS_PENDING) {
            // TODO: Need to sleep to avoid hard fault error, find out the root cuase in the future.
            dxTimeDelayMS (100);
            continue;
        }

        if (char_access_res_header.status == REPEATER_STATUS_SUCCESS) {
            transitionState = BleRead128bitCharacteristic (
                m_char_access_res_payload_handle,
                REPEATER_READ_HANDSHAKE_ID,
                &readCharResponse
            );
            if (transitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) {
                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                break;
            }
            *read_length = readCharResponse.DataReadLen;
            *content = dxMemAlloc ("read result", 1, readCharResponse.DataReadLen);
            if (*content == NULL) {
                transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                break;
            }
            memcpy (*content, readCharResponse.DataRead, readCharResponse.DataReadLen);

            //debug ("remote read res \r\n");
            ctrlResult = DEV_CENTRAL_CTRL_SUCCESS;
            break;

        } else {
            break; // error occur
        }
    }

exit :

    return ctrlResult;
}


static uint8_t _BleRepeater_IsAddressZero (uint8_t *p_input, uint16_t length) {
    while (length) {
        if (*p_input != 0) {
            break;
        }
        length--;
    }
    return !length;
}
