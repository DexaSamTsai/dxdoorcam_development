//============================================================================
// File: BLEDeviceKeeper.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "BLEDeviceInfoParser.h"
#include "dk_Peripheral.h"
#include "dk_DkManagerConfig.h"
#include "dxSysDebugger.h"

/******************************************************
 *                      Defines
 ******************************************************/

#define CHARACTERISTIC_HANDLER_ID_NOT_FOUND     0

//If set to 0, only device in allowed list will be added/monitored
#define KEEP_ALL_INCOMING_DEVICES           0

#define ADVERTISING_TYPE_ADV_IND            1   /**< All Adv types except SCAN RSP IND */
#define ADVERTISING_TYPE_SCAN_RSP_IND       2   /**< SCAN RSP IND Adv type */

#define DEVICE_INFO_REPORT_FILTER_BLOCK_ALL 0   /**< Don't report */
#define DEVICE_INFO_REPORT_FILTER_UNPAIR    1   /**< Only report Unpair devices */
#define DEVICE_INFO_REPORT_FILTER_NONE      2   /**< Report all device that is NOT in keep list */

#define BLE_RSSI_MISSING                0
#define BLE_RSSI_NORMAL                 -50



/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

    DEV_KEEPER_SUCCESS                          = 0,    /**< Success */
    DEV_KEEPER_DEVICE_ALREADY_IN_LIST           = 1,    /**< Device is already in the keep list */
    DEV_KEEPER_DEVICE_ALREADY_IN_SEARCH         = 2,   /**< Already searching for other device */
    DEV_KEEPER_DEVICE_INFO_STILL_INCOMPLETE     = 3,   /**< The device info is still incomplete, may take few seconds depending on device adv interval to obtain all necesssary information */
    DEV_KEEPER_DEVICE_INFO_IS_COMPLETE          = 4,   /**< The device info is complete */
    DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED     = 5,   /**< The device found is in keeper's list but not paired  */
    DEV_KEEPER_DEVICE_INFO_CHECKSUM_FAILED      = 6,   /**< The device info (payload) check sum failed  */

    DEV_KEEPER_ERROR                            = -1,   /**< Generic Error */
    DEV_KEEPER_NOT_FOUND                        = -2,
    DEV_KEEPER_MAX_DEVICE_REACHED               = -3,   /**< Maximum keep track device reached Error */
    DEV_KEEPER_MEMORY_ALLOCATION_ERROR          = -4,   /**< Memory allocation Error */
    DEV_KEEPER_INVALID_PARAM                    = -5,   /**< Invalid parameter Error */

} BLEDeviceKeeper_result_t;


/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    uint16_t    ServiceUUID;
    uint16_t    CharacteristicUUID;
    uint16_t    HandlerID;

} ServiceCharHandleID_t;

typedef struct
{
    uint16_t    Major;
    uint16_t    Middle;
    uint16_t    Minor;

} FwVersion_t;

typedef struct
{
    DeviceAddress_t         DevAddress;
    FwVersion_t             DevFwVersion;
    int8_t                  rssi;
    uint8_t                 AdvertisingType;        //!< Advertisement Type: @ref GAP_ADVERTISEMENT_TYPE_DEFINES
    uint8_t                 ManufactureAdvdataLen;
    uint8_t*                pManufactureAdvdata;
    dxTime_t                ManufacturerAdvDataLastUpdatedSystemTime;
    uint8_t                 TotalServiceCharHandlerInList;
    ServiceCharHandleID_t*      ServiceCharHandlerList;
    SecurityKeyCodeContainer_t  SecurityKey;

} DeviceInfoKp_t;


typedef struct
{
    DevAddress_t                    DevAddress;
    uint16_t                        DeviceType;
    uint8_t                         SingleHostControl;
    SecurityKeyCodeContainer_t      SecurityKey;

} DeviceAddInfo_t;

typedef struct
{
    DevAddress_t                    DevAddress;
    SecurityKeyCodeContainer_t      SecurityKey;

} OldDeviceGenericInfo_t;

typedef DX_PACK_STRUCT_BEGIN
{
    DevAddress_t                    DevAddress;
    uint16_t                        DeviceType;
    SecurityKeyCodeContainer_t      SecurityKey;

} DeviceGenericInfo_t;


/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

extern BLEDeviceKeeper_result_t BLEDevKeeper_Init();

extern BLEDeviceKeeper_result_t BLEDevKeeper_DeviceInfoFeed(uint8_t* DeviceInfoPayload, uint8_t PayloadLen, int16_t* pDevIndex);

extern dxBOOL BLEDevKeeper_IsMaximumCapacityReached();

extern BLEDeviceKeeper_result_t BLEDevKeeper_AddDeviceToKeeper (DeviceAddress_t DevAddr, uint16_t DeviceType, SecurityKeyCodeContainer_t* SecurityKey, FwVersion_t DeviceFwVersion, int16_t* AddedToIndex);

extern BLEDeviceKeeper_result_t BLEDevKeeper_RemoveDeviceFromKeeper(DeviceAddress_t DevAddr);

extern BLEDeviceKeeper_result_t BLEDevKeeper_RemoveAllDeviceFromKeeperList();

extern uint16_t BLEDevKeeper_GetDeviceCountFromKeeperList();

extern uint16_t BLEDevKeeper_GetAllDeviceGenericInfoFromKeeperList(DeviceGenericInfo_t* pGenericInfoBuff, uint16_t AvailableGenInfoBuffElement);

/** Find the deviceinfo that match the address provided, upon this function is called keeper will first look for
 *  matching address in the white list, if not found it will try to look for all incoming devices info that is not in the white list
 *  Please keep in mind that keeper will stop finding device once it's found, and will not timeout until its found or BLEDevKeeper_StopFindingDeviceInfo is called
 *  Only one device can be searched at a time, will return error if already in the process of searching a device. Call BLEDevKeeper_StopFindingDeviceInfo to cancel
 *
 * @param[in]  DeviceAddr8   :  8 Byte address to look for
 * @param[in]  AdvertisingTypeToReport: ADVERTISING_TYPE_ADV_IND or ADVERTISING_TYPE_SCAN_RSP_IND
 * @param[in]  QueueToReport :  The Queue provided that will push deviceInfo (A.K.A DeviceInfoPayload)  object to caller.. it is callers responsibility to wait for the Queue and
 *                              free the deviceInfo Obj.
 *
 * @return @ref BLEDeviceKeeper_result_t
 */
extern BLEDeviceKeeper_result_t BLEDevKeeper_StartFindingDeviceInfoMatchingAddr(uint8_t* DeviceAddr8, uint8_t AdvertisingTypeToReport, dxQueueHandle_t* QueueToReport);

extern BLEDeviceKeeper_result_t BLEDevKeeper_StopFindingDeviceInfo();

extern BLEDeviceKeeper_result_t BLEDevKeeper_StartReportingUnPairDevices(dxQueueHandle_t* QueueToReport);

extern BLEDeviceKeeper_result_t BLEDevKeeper_StopReportingUnPairDevices();

extern uint16_t BLEDevKeeper_GetCharacteristicHandlerID(DeviceAddress_t* DeviceAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, unsigned char UuidIs128bit);

extern BLEDeviceKeeper_result_t BLEDevKeeper_SaveCharacteristicHandlerID(DeviceAddress_t* DeviceAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t CharacteristicHandlerID,unsigned char UuidIs128bit);

extern BLEDeviceKeeper_result_t BLEDevKeeper_IsDeviceInfoCompleteFromIndex(uint16_t index);

extern DeviceInfoKp_t* BLEDevKeeper_GetDeviceInfoFromIndex(uint16_t index);

extern BLEDeviceKeeper_result_t BLEDevKeeper_UpdateDeviceSecurityKey(uint16_t index, SecurityKeyCodeContainer_t* SecurityKey);

extern SecurityKeyCodeContainer_t* BLEDevKeeper_GetSecurityKeyFromIndexAlloc(uint16_t index);

extern const SecurityKeyCodeContainer_t* BLEDevKeeper_GetSecurityKeyFromIndex(uint16_t index);

extern const FwVersion_t* BLEDevKeeper_GetFwVersionFromIndex(uint16_t index);

extern int16_t BLEDevKeeper_FindDeviceIndexFromGivenAddress(DeviceAddress_t DeviceAddr);


/**
 * @brief BLEDevKeeper_FindDeviceInfoByMac
 */
extern DeviceGenericInfo_t* BLEDevKeeper_FindDeviceInfoByMac (uint8_t* macaddr, int macaddrlen);


extern BLEDeviceKeeper_result_t BLEDevKeeper_DebugShowAllDeviceInfo();

/******************************************************
 * The following function added by David
 ******************************************************/

extern BLEDeviceKeeper_result_t BLEDevKeeper_StartReportingDevices(dxQueueHandle_t* QueueToReport);

extern BLEDeviceKeeper_result_t BLEDevKeeper_StopReportingDevices();


/**
 * @breif BLEDevKeeper_GetRepeaterIndex
 */
extern int16_t BLEDevKeeper_GetRepeaterIndex (void);
