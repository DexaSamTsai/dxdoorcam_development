//============================================================================
// File: BLEDeviceKeeper.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "dxBSP.h"
#include "BLEDeviceKeeper.h"
#include "ObjectDictionary.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
    char            InSearch;
    uint8_t         AdvTypeToReport;
    uint8_t         DevAddress8[B_ADDR_LEN_8];
    dxQueueHandle_t*    DeviceFoundReportQueueMsg;

} DeviceSearch_t;

typedef struct
{
    uint8_t         ReportFilter;
    dxQueueHandle_t*    DeviceReportQueueMsg;

} DeviceReport_t;


/******************************************************
 *               Function Declarations
 ******************************************************/


/******************************************************
 *               Variable Definitions
 ******************************************************/

DeviceInfoKp_t *gDeviceKeeperList = NULL;

DeviceSearch_t DeviceSearchInfo;

DeviceReport_t DeviceInfoReport;

dxSemaphoreHandle_t DevInfoAccessResourceProtectionSemaHandle = NULL;

uint16_t repeaterIndex = EOF;

/******************************************************
 *               Local Function Definitions
 ******************************************************/

int16_t GetFirstAvailableDeviceIndexSlotFromKeeperList()
{
    int16_t     DeviceIndex = DEV_KEEPER_NOT_FOUND;

    uint16_t    i = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_DEVICE_KEEPER_ALLOWED; i++)
    {
        if (gDeviceKeeperList[i].DevAddress.AddrLen == 0)
        {
            DeviceIndex = i;
            break;
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return DeviceIndex;
}


int16_t GetDeviceIndexFromKeeperList(DeviceAddress_t DevAddr)
{
    int16_t     DeviceIndex = DEV_KEEPER_NOT_FOUND;
    uint16_t    i = 0;
    uint16_t    j = 0;


    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_DEVICE_KEEPER_ALLOWED; i++)
    {
        if (gDeviceKeeperList[i].DevAddress.AddrLen == DevAddr.AddrLen)
        {
            dxBOOL DeviceFound = dxTRUE;
            for (j = 0; j < DevAddr.AddrLen; j++)
            {
                if (gDeviceKeeperList[i].DevAddress.pAddr[j] != DevAddr.pAddr[j])
                {
                    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "GetDeviceIndexFromKeeperList index NOT Found\r\n");
                    DeviceFound = dxFALSE;
                    break;
                }
            }

            if (DeviceFound == dxTRUE)
            {
                //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "GetDeviceIndexFromKeeperList index found\r\n" );
                DeviceIndex = i;
                break;
            }
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return DeviceIndex;
}


BLEDeviceKeeper_result_t _BLEDevKeeper_AddDeviceToKeeperList (DeviceAddress_t DevAddr, uint16_t DeviceType, SecurityKeyCodeContainer_t* SecurityKey, FwVersion_t DeviceFwVersion, int16_t* AddedToIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    int16_t AvailableSlot = GetFirstAvailableDeviceIndexSlotFromKeeperList();

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (AvailableSlot != DEV_KEEPER_NOT_FOUND)
    {
        *AddedToIndex = AvailableSlot;

        gDeviceKeeperList[AvailableSlot].DevAddress.AddressType = DevAddr.AddressType;
        gDeviceKeeperList[AvailableSlot].DevAddress.AddrLen = DevAddr.AddrLen;
        gDeviceKeeperList[AvailableSlot].DevAddress.pAddr = dxMemAlloc( "Dev Keeper Address Buff", 1, DevAddr.AddrLen);
        gDeviceKeeperList[AvailableSlot].DevFwVersion.Major = DeviceFwVersion.Major;
        gDeviceKeeperList[AvailableSlot].DevFwVersion.Middle = DeviceFwVersion.Middle;
        gDeviceKeeperList[AvailableSlot].DevFwVersion.Minor = DeviceFwVersion.Minor;

        if (gDeviceKeeperList[AvailableSlot].DevAddress.pAddr)
        {

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "_BLEDevKeeper_AddDeviceToKeeperList add to avail. slot=%d, AddrLen=%d\r\n", AvailableSlot, gDeviceKeeperList[AvailableSlot].DevAddress.AddrLen);
            memcpy(gDeviceKeeperList[AvailableSlot].DevAddress.pAddr, DevAddr.pAddr, DevAddr.AddrLen);

            if (SecurityKey)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "_BLEDevKeeper_AddDeviceToKeeperList SecurityKey added\r\n");
                memcpy(&gDeviceKeeperList[AvailableSlot].SecurityKey, SecurityKey, sizeof(SecurityKeyCodeContainer_t));
            }

            if (!gDeviceKeeperList[AvailableSlot].pManufactureAdvdata) {
                DkAdvData_t advData;
                size_t advDataLen;

                advData.Header.CompanyID = DK_COMPANY_DEXATEK;
                advData.Header.ProductID = (uint8_t) DeviceType;
                advData.Header.Flag = 0x0080;
                advData.Header.XorPayloadChecksum = 0x10; // Precalculated
                advData.ExtDkFlag = 0;
                advData.BatteryInfo = 0;
                advData.PagingInfo = 0x0000;
                advData.AdData[ 0] = DK_DATA_TYPE_SECURITY_MASK;
                advData.AdData[ 1] = 0x00;
                advData.AdData[ 2] = 0x00;
                advData.AdData[ 3] = 0x00;
                advData.AdData[ 4] = 0x00;
                advData.AdData[ 5] = 0x00;
                advData.AdData[ 6] = 0x00;
                advData.AdData[ 7] = 0x00;
                advData.AdData[ 8] = 0x00;
                advData.AdData[ 9] = 0x00;
                advData.AdData[10] = 0x00;
                advData.AdData[11] = 0x00;
                advData.AdData[12] = 0x00;
                advData.AdData[13] = 0x00;
                advData.AdData[14] = 0x00;
                advData.AdData[15] = 0x00;
                advData.AdDataLen = 16;

                advDataLen = sizeof (DeviceDkPeripheralAdvHeader_t) + advData.AdDataLen;

                uint8_t* NewDeviceAdDataPtr = NULL;

                NewDeviceAdDataPtr = dxMemAlloc ("Dev Keeper AdvData Buff", 1, advDataLen);
                if (NewDeviceAdDataPtr) {
                    gDeviceKeeperList[AvailableSlot].pManufactureAdvdata = NewDeviceAdDataPtr;
                    gDeviceKeeperList[AvailableSlot].ManufactureAdvdataLen = advDataLen;
                    memcpy (NewDeviceAdDataPtr, &advData.Header, sizeof (DeviceDkPeripheralAdvHeader_t));
                    NewDeviceAdDataPtr += sizeof (DeviceDkPeripheralAdvHeader_t);
                    memcpy (NewDeviceAdDataPtr, advData.AdData, advData.AdDataLen);
                }
            }

            gDeviceKeeperList[AvailableSlot].ManufacturerAdvDataLastUpdatedSystemTime = dxTimeGetMS ();

        }
        else
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "_BLEDevKeeper_AddDeviceToKeeperList DEV_KEEPER_MEMORY_ALLOCATION_ERROR\r\n");
            Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR;
            gDeviceKeeperList[AvailableSlot].DevAddress.AddrLen = 0;
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "_BLEDevKeeper_AddDeviceToKeeperList DEV_KEEPER_MAX_DEVICE_REACHED\r\n");
        Result = DEV_KEEPER_MAX_DEVICE_REACHED; //No available slot
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}



BLEDeviceKeeper_result_t UpdateScanRspInfoToKeeperIndex(uint8_t* DeviceInfoPayload, int16_t DeviceIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    ScanRspInfo_t stScanRspInfo;
    stScanRspInfo = BLEDevParser_GetScanRspInfo(DeviceInfoPayload);

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    //Only update if rssi is less than 0
    if (stScanRspInfo.rssi < 0)
    {
        gDeviceKeeperList[DeviceIndex].rssi = stScanRspInfo.rssi;
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}



BLEDeviceKeeper_result_t UpdateDeviceManufacturerDataToKeeperIndex(uint8_t* DeviceInfoPayload, int16_t DeviceIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    DeviceManufaturerData_t DevManufacturerData;

    DevManufacturerData = BLEDevParser_GetManufacturerData(DeviceInfoPayload);

    if (DevManufacturerData.ManufacturerDataLen)
    {
        if (!gDeviceKeeperList[DeviceIndex].pManufactureAdvdata)
        {
            gDeviceKeeperList[DeviceIndex].pManufactureAdvdata = dxMemAlloc( "Dev Keeper AdvData Buff", 1, DevManufacturerData.ManufacturerDataLen);

            if (!gDeviceKeeperList[DeviceIndex].pManufactureAdvdata)
            {
                Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR;
            }

        }
        else if (DevManufacturerData.ManufacturerDataLen > gDeviceKeeperList[DeviceIndex].ManufactureAdvdataLen)
        {
            uint8_t* NewDeviceAdDataPtr = NULL;

            NewDeviceAdDataPtr = dxMemReAlloc( "Dev Keeper Name Buff", gDeviceKeeperList[DeviceIndex].pManufactureAdvdata, DevManufacturerData.ManufacturerDataLen);
            gDeviceKeeperList[DeviceIndex].pManufactureAdvdata = NewDeviceAdDataPtr;

            if (!gDeviceKeeperList[DeviceIndex].pManufactureAdvdata)
            {
                Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR;
            }

        }

        if (Result != DEV_KEEPER_MEMORY_ALLOCATION_ERROR)
        {
            memcpy(gDeviceKeeperList[DeviceIndex].pManufactureAdvdata, DevManufacturerData.pManufacturerData, DevManufacturerData.ManufacturerDataLen);
            gDeviceKeeperList[DeviceIndex].ManufactureAdvdataLen = DevManufacturerData.ManufacturerDataLen;
            gDeviceKeeperList[DeviceIndex].AdvertisingType = DevManufacturerData.AdvertisingType;
            gDeviceKeeperList[DeviceIndex].rssi = DevManufacturerData.rssi;
            if (gDeviceKeeperList[DeviceIndex].pManufactureAdvdata[2] == DK_PRODUCT_TYPE_BLE_REPEATER_ID)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "got an adv. packet of paired repeater.\r\n");
                BleRepeater_TurnOn (&gDeviceKeeperList[DeviceIndex].DevAddress);
                repeaterIndex = DeviceIndex;
            }
            gDeviceKeeperList[DeviceIndex].ManufacturerAdvDataLastUpdatedSystemTime = dxTimeGetMS();
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}

BLEDeviceKeeper_result_t UpdateDeviceInfoFromCurrentKeeperList(uint8_t* DeviceInfoPayload, DeviceAddress_t DevAddr)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    int16_t DeviceIndex = GetDeviceIndexFromKeeperList(DevAddr);
    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {
        Result = UpdateScanRspInfoToKeeperIndex(DeviceInfoPayload, DeviceIndex);

        if (Result == DEV_KEEPER_MEMORY_ALLOCATION_ERROR)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - Unable to allocate memory for keeper device name\r\n");
            goto Exit;
        }

        Result = UpdateDeviceManufacturerDataToKeeperIndex(DeviceInfoPayload, DeviceIndex);

        if (Result == DEV_KEEPER_MEMORY_ALLOCATION_ERROR)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - Unable to allocate memory for keeper manufacturer data\r\n" );
            goto Exit;
        }
    }

Exit:
    return Result;
}




/******************************************************
 *               Function Definitions
 ******************************************************/

BLEDeviceKeeper_result_t BLEDevKeeper_Init()
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    if (gDeviceKeeperList == NULL)
    {
        gDeviceKeeperList = dxMemAlloc( "Dev Keeper List Buff", MAXIMUM_DEVICE_KEEPER_ALLOWED, sizeof(DeviceInfoKp_t));

        if (!gDeviceKeeperList)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevKeeper_Init DEV_KEEPER_MEMORY_ALLOCATION_ERROR\r\n");
            Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR; //Memory Alloc Error
        }
    }

    if ((Result == DEV_KEEPER_SUCCESS) &&(DevInfoAccessResourceProtectionSemaHandle == NULL))
    {
        DevInfoAccessResourceProtectionSemaHandle = dxSemCreate(    1,          // uint32_t uxMaxCount
                                                                    1);         // uint32_t uxInitialCount

        if (DevInfoAccessResourceProtectionSemaHandle == NULL)
        {
            Result = DEV_KEEPER_ERROR;
        }
    }

    DeviceSearchInfo.InSearch = 0;

    return Result;

}

dxBOOL BLEDevKeeper_IsMaximumCapacityReached()
{

    dxBOOL Reached = dxFALSE;

    if (GetFirstAvailableDeviceIndexSlotFromKeeperList() == DEV_KEEPER_NOT_FOUND)
    {
        Reached = dxTRUE;
    }

    return Reached;
}

BLEDeviceKeeper_result_t BLEDevKeeper_AddDeviceToKeeper (DeviceAddress_t DevAddr, uint16_t DeviceType, SecurityKeyCodeContainer_t* SecurityKey, FwVersion_t DeviceFwVersion ,int16_t* AddedToIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    //Check if it was already added
    int16_t DeviceIndex = GetDeviceIndexFromKeeperList(DevAddr);

    if (DeviceIndex == DEV_KEEPER_NOT_FOUND)
    {
        Result = _BLEDevKeeper_AddDeviceToKeeperList (DevAddr, DeviceType, SecurityKey, DeviceFwVersion, AddedToIndex);
    }
    else
    {
        Result = DEV_KEEPER_DEVICE_ALREADY_IN_LIST;
    }

    return Result;
}

BLEDeviceKeeper_result_t _BLEDevKeeper_RemoveDeviceFromKeeperGivenIndex(int16_t DevIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    if ((DevIndex != DEV_KEEPER_NOT_FOUND) && (DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED))
    {
        //Let's clean up
        gDeviceKeeperList[DevIndex].DevAddress.AddrLen = 0; //With AddrLen set to 0, we free up this slot and can be used when latter adding new devices
        gDeviceKeeperList[DevIndex].DevAddress.AddressType = 0;
        if (gDeviceKeeperList[DevIndex].DevAddress.pAddr)
        {
            dxMemFree(gDeviceKeeperList[DevIndex].DevAddress.pAddr);
            gDeviceKeeperList[DevIndex].DevAddress.pAddr = NULL;
        }

        gDeviceKeeperList[DevIndex].rssi = BLE_RSSI_MISSING;
        gDeviceKeeperList[DevIndex].AdvertisingType = 0;
        gDeviceKeeperList[DevIndex].ManufactureAdvdataLen = 0;

        if (gDeviceKeeperList[DevIndex].pManufactureAdvdata)
        {
            if (gDeviceKeeperList[DevIndex].pManufactureAdvdata[2] == DK_PRODUCT_TYPE_BLE_REPEATER_ID)
            {
                BleRepeater_Reset ();
                repeaterIndex = EOF;
            }
            dxMemFree(gDeviceKeeperList[DevIndex].pManufactureAdvdata);
            gDeviceKeeperList[DevIndex].pManufactureAdvdata = NULL;
        }

        gDeviceKeeperList[DevIndex].ManufacturerAdvDataLastUpdatedSystemTime = 0;
        gDeviceKeeperList[DevIndex].TotalServiceCharHandlerInList = 0;

        if (gDeviceKeeperList[DevIndex].ServiceCharHandlerList)
        {
            dxMemFree(gDeviceKeeperList[DevIndex].ServiceCharHandlerList);
            gDeviceKeeperList[DevIndex].ServiceCharHandlerList = NULL;
        }

        memset(&gDeviceKeeperList[DevIndex].SecurityKey, 0, sizeof(SecurityKeyCodeContainer_t));
    }

    return Result;
}

BLEDeviceKeeper_result_t BLEDevKeeper_RemoveDeviceFromKeeper(DeviceAddress_t DevAddr)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    int16_t DevIndex = GetDeviceIndexFromKeeperList(DevAddr);

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    _BLEDevKeeper_RemoveDeviceFromKeeperGivenIndex(DevIndex);

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}


BLEDeviceKeeper_result_t BLEDevKeeper_RemoveAllDeviceFromKeeperList()
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;
    uint16_t DevIndex = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (DevIndex = 0; DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED; DevIndex++)
    {
        if (gDeviceKeeperList[DevIndex].DevAddress.AddrLen)
        {
            _BLEDevKeeper_RemoveDeviceFromKeeperGivenIndex(DevIndex);
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return result;
}


uint16_t BLEDevKeeper_GetDeviceCountFromKeeperList()
{
    uint16_t DeviceCount = 0;
    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint16_t DevIndex = 0;
    for (DevIndex = 0; DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED; DevIndex++)
    {
        if (gDeviceKeeperList[DevIndex].DevAddress.AddrLen)
        {
            DeviceCount++;
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return DeviceCount;
}

uint16_t BLEDevKeeper_GetAllDeviceGenericInfoFromKeeperList(DeviceGenericInfo_t* pGenericInfoBuff, uint16_t AvailableGenInfoBuffElement)
{
    uint16_t DevCount = 0;
    uint16_t DevIndex = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (DevIndex = 0; DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED; DevIndex++)
    {
        if (gDeviceKeeperList[DevIndex].DevAddress.AddrLen)
        {
            if (DevCount >= AvailableGenInfoBuffElement)
                break;

            DeviceGenericInfo_t* pGenericInfo = pGenericInfoBuff + DevCount;
            memcpy(pGenericInfo->DevAddress.Addr, gDeviceKeeperList[DevIndex].DevAddress.pAddr, B_ADDR_LEN_8);
            memcpy(&pGenericInfo->SecurityKey, &gDeviceKeeperList[DevIndex].SecurityKey, sizeof(SecurityKeyCodeContainer_t));

            DevCount++;
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return DevCount;
}


BLEDeviceKeeper_result_t BLEDevKeeper_StartFindingDeviceInfoMatchingAddr(uint8_t* DeviceAddr8, uint8_t AdvertisingTypeToReport, dxQueueHandle_t* QueueToReport)
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    if ((DeviceAddr8 == NULL) || (QueueToReport == NULL))
    {
        result = DEV_KEEPER_INVALID_PARAM;
    }
    else if (DeviceSearchInfo.InSearch)
    {
        result = DEV_KEEPER_DEVICE_ALREADY_IN_SEARCH;
    }
    else
    {
        //TODO: Let's first search in white list

        memcpy(DeviceSearchInfo.DevAddress8, DeviceAddr8, B_ADDR_LEN_8);
        DeviceSearchInfo.DeviceFoundReportQueueMsg = QueueToReport;
        DeviceSearchInfo.InSearch = 1;
        DeviceSearchInfo.AdvTypeToReport = AdvertisingTypeToReport;
    }

    return result;
}


BLEDeviceKeeper_result_t BLEDevKeeper_StopFindingDeviceInfo()
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    DeviceSearchInfo.InSearch = 0;

    return result;
}


extern BLEDeviceKeeper_result_t BLEDevKeeper_StartReportingUnPairDevices(dxQueueHandle_t* QueueToReport)
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    if (QueueToReport == NULL)
    {
        result = DEV_KEEPER_INVALID_PARAM;
    }
    else
    {
        DeviceInfoReport.DeviceReportQueueMsg = QueueToReport;
        DeviceInfoReport.ReportFilter = DEVICE_INFO_REPORT_FILTER_UNPAIR;
    }

    return result;

}

extern BLEDeviceKeeper_result_t BLEDevKeeper_StopReportingUnPairDevices()
{

    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    DeviceInfoReport.ReportFilter = DEVICE_INFO_REPORT_FILTER_BLOCK_ALL;

    return result;

}

BLEDeviceKeeper_result_t BLEDevKeeper_DeviceInfoFeed(uint8_t* DeviceInfoPayload, uint8_t PayloadLen, int16_t* pDevIndex)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    DeviceAddress_t stDevAddr = BLEDevParser_GetAddress(DeviceInfoPayload);

    //Only check for device that contain valid Address len and manufacturer data
    if (stDevAddr.AddrLen)
    {
        //Check if it was already added
        int16_t DeviceIndex = GetDeviceIndexFromKeeperList(stDevAddr);
        *pDevIndex = DeviceIndex;
        if (DeviceIndex == DEV_KEEPER_NOT_FOUND)
        {

            if (DeviceInfoReport.ReportFilter)
            {
                uint8_t ReportToQueue = 0;

                //TODO: Note that we can only report Adv data (eg, non RSP) because we cannot report UNPAIR RSP data since UNPAIR information
                //      is in Adv.... Do we really need to report RSP data (may contain name or 128 bit device service UUID)??
                if(!BLEDevParser_IsAdvertisingDataScanRsp(DeviceInfoPayload))
                {
                    DeviceManufaturerData_t ManufacturerData = BLEDevParser_GetManufacturerData(DeviceInfoPayload);

                    switch (DeviceInfoReport.ReportFilter)
                    {
                        case DEVICE_INFO_REPORT_FILTER_UNPAIR:
                        {
                            if (BLEDevParser_IsDeviceInOpenPairingMode(&ManufacturerData))
                                ReportToQueue = 1;
                        }
                            break;

                        case DEVICE_INFO_REPORT_FILTER_NONE:
                        {
                            ReportToQueue = 1;
                        }
                            break;

                        default:
                            break;
                    }
                }

                if (ReportToQueue)
                {
                    if (DeviceInfoReport.DeviceReportQueueMsg != NULL)
                    {

                        if (dxQueueIsFull(*DeviceInfoReport.DeviceReportQueueMsg) != dxTRUE)
                        {
                            void* DeviceReportInfoObj = dxMemAlloc( "DeviceInfoReport Payload Obj", 1, PayloadLen);

                            memcpy(DeviceReportInfoObj, DeviceInfoPayload, PayloadLen);

                            if (dxQueueSend( *DeviceInfoReport.DeviceReportQueueMsg, &DeviceReportInfoObj, 1000 ) != DXOS_SUCCESS)
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                                dxMemFree(DeviceReportInfoObj);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            Result = DEV_KEEPER_DEVICE_INFO_STILL_INCOMPLETE;
            UpdateDeviceInfoFromCurrentKeeperList(DeviceInfoPayload, stDevAddr);

            if(!BLEDevParser_IsAdvertisingDataScanRsp(DeviceInfoPayload))
            {

                if (BLEDevKeeper_IsDeviceInfoCompleteFromIndex(DeviceIndex) == DEV_KEEPER_SUCCESS)
                {
                    Result = DEV_KEEPER_DEVICE_INFO_IS_COMPLETE; //Default

                    DeviceManufaturerData_t ManufacturerData = BLEDevParser_GetManufacturerData(DeviceInfoPayload);

                    if (!BLEDevParser_IsDevicePayloadDataValid(&ManufacturerData))
                    {
                        Result = DEV_KEEPER_DEVICE_INFO_CHECKSUM_FAILED;
                    }
#if 1
                    else if (!BLEDevParser_IsDevicePaired(&ManufacturerData))
                    {
                        //Device is in our keeper list but NOT paired. Let application decide if re-pairing is needed
                        Result = DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED;
                    }
#else //Original - only if we have the reason not pair the device if it's already in our list and the device is broadcasting any other mode except paired.
                    else if (BLEDevParser_IsDeviceInStandAloneMode(&ManufacturerData))
                    {
                        //Device is in our keeper list but NOT paired. Let application decide if re-pairing is needed
                        Result = DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED;
                    }
#endif

                }

            }

        }

        //If we are in search report back matching devices with search address regardless if its already in the keeper list or not
        if (DeviceSearchInfo.InSearch)
        {
            if ( memcmp( DeviceSearchInfo.DevAddress8, stDevAddr.pAddr, B_ADDR_LEN_8 ) == 0 )
            {
                unsigned char DeviceInfoAdvTypeIsScanRsp = BLEDevParser_IsAdvertisingDataScanRsp(DeviceInfoPayload);

                switch (DeviceSearchInfo.AdvTypeToReport)
                {
                    case ADVERTISING_TYPE_SCAN_RSP_IND:
                        if (!DeviceInfoAdvTypeIsScanRsp)
                            goto Exit; //Not the adv data type we are looking for

                        break;

                    case ADVERTISING_TYPE_ADV_IND:
                        if (DeviceInfoAdvTypeIsScanRsp)
                            goto Exit; //Not the adv data type we are looking for

                        break;

                    default:
                        break;
                }

                //Match Found
                if (DeviceSearchInfo.DeviceFoundReportQueueMsg != NULL)
                {

                    if (dxQueueIsFull(*DeviceSearchInfo.DeviceFoundReportQueueMsg) != dxTRUE)
                    {
                        void* DeviceInfoObj = dxMemAlloc( "DeviceInfo Payload Obj", 1, PayloadLen);

                        memcpy(DeviceInfoObj, DeviceInfoPayload, PayloadLen);

                        if (dxQueueSend( *DeviceSearchInfo.DeviceFoundReportQueueMsg, &DeviceInfoObj, 1000 ) != DXOS_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                            dxMemFree(DeviceInfoObj);
                        }

                        DeviceSearchInfo.InSearch = 0; //Clear the search flag
                    }
                    else
                    {
                        //TODO: Queue is full -  If something went wrong we will always arrived here and application level will think
                        //                       device search is not found. Should we notify application level??
                    }
                }
            }
        }

    }

Exit:
    return Result;
}

uint16_t BLEDevKeeper_GetCharacteristicHandlerID(DeviceAddress_t* DeviceAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, unsigned char UuidIs128bit)
{
    uint16_t CharacteristicHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;

    int16_t DeviceIndex = GetDeviceIndexFromKeeperList(*DeviceAddr);

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {
        if (gDeviceKeeperList[DeviceIndex].TotalServiceCharHandlerInList)
        {
            uint8_t i = 0;
            for (i = 0; i < gDeviceKeeperList[DeviceIndex].TotalServiceCharHandlerInList; i++)
            {
                if ((gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[i].ServiceUUID == ServiceUUID) &&
                        (gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[i].CharacteristicUUID == CharacteristicUUID))
                {
                    CharacteristicHandlerID = gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[i].HandlerID;
                    break;
                }
            }
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return CharacteristicHandlerID;
}


BLEDeviceKeeper_result_t BLEDevKeeper_SaveCharacteristicHandlerID(DeviceAddress_t* DeviceAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t CharacteristicHandlerID, unsigned char UuidIs128bit)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    int16_t DeviceIndex = GetDeviceIndexFromKeeperList(*DeviceAddr);

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {
        uint8_t CurrentTotalSerCharHandlerInList = gDeviceKeeperList[DeviceIndex].TotalServiceCharHandlerInList;
        if (CurrentTotalSerCharHandlerInList == 0)
        {
            gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList = dxMemAlloc( "ServiceCharHandler list", 1, sizeof(ServiceCharHandleID_t));
            if (gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList == NULL)
            {
                Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR;
            }
        }
        else
        {

            ServiceCharHandleID_t* NewSerCharHandlerListPtr = NULL;
            uint16_t NewSize = (CurrentTotalSerCharHandlerInList + 1) * sizeof(ServiceCharHandleID_t);
            NewSerCharHandlerListPtr = dxMemReAlloc( "ServiceCharHandler list", gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList, NewSize);

            if (NewSerCharHandlerListPtr)
            {
                gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList = NewSerCharHandlerListPtr;
            }
            else
            {
                Result = DEV_KEEPER_MEMORY_ALLOCATION_ERROR;
            }
        }

        if (Result == DEV_KEEPER_SUCCESS)
        {
            gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[CurrentTotalSerCharHandlerInList].ServiceUUID = ServiceUUID;
            gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[CurrentTotalSerCharHandlerInList].CharacteristicUUID = CharacteristicUUID;
            gDeviceKeeperList[DeviceIndex].ServiceCharHandlerList[CurrentTotalSerCharHandlerInList].HandlerID = CharacteristicHandlerID;
            gDeviceKeeperList[DeviceIndex].TotalServiceCharHandlerInList++;
        }

    }
    else
    {
        Result = DEV_KEEPER_NOT_FOUND;
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}

BLEDeviceKeeper_result_t BLEDevKeeper_IsDeviceInfoCompleteFromIndex(uint16_t index)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_ERROR;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        //Check if we do have Manufacturerdata available
        //NOTE: We do not check device name here because some peripheral may not have local name available in Scan RSP data
        if (gDeviceKeeperList[index].pManufactureAdvdata)
        {
            Result = DEV_KEEPER_SUCCESS;
        }
    }
    else
    {
        Result = DEV_KEEPER_INVALID_PARAM;
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}

DeviceInfoKp_t* BLEDevKeeper_GetDeviceInfoFromIndex(uint16_t index)
{

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    DeviceInfoKp_t *DeviceInfo = NULL;

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        if (gDeviceKeeperList[index].DevAddress.AddrLen)
        {
            DeviceInfo = &gDeviceKeeperList[index];
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return DeviceInfo;
}

extern BLEDeviceKeeper_result_t BLEDevKeeper_UpdateDeviceSecurityKey(uint16_t index, SecurityKeyCodeContainer_t* SecurityKey)
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        if (gDeviceKeeperList[index].DevAddress.AddrLen)
        {
            if (SecurityKey)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Device SecurityKey Updated\r\n");
                memcpy(&gDeviceKeeperList[index].SecurityKey, SecurityKey, sizeof(SecurityKeyCodeContainer_t));
            }
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return Result;
}


SecurityKeyCodeContainer_t* BLEDevKeeper_GetSecurityKeyFromIndexAlloc(uint16_t index)
{

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    SecurityKeyCodeContainer_t *SecurityKey = NULL;

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        if (gDeviceKeeperList[index].DevAddress.AddrLen)
        {
            if (gDeviceKeeperList[index].SecurityKey.Size)
            {
                SecurityKey = dxMemAlloc("Buff", 1, sizeof(SecurityKeyCodeContainer_t));
                memset(SecurityKey, 0 , sizeof(SecurityKeyCodeContainer_t));

                memcpy(SecurityKey, &gDeviceKeeperList[index].SecurityKey, sizeof(SecurityKeyCodeContainer_t));
            }
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return SecurityKey;

}

const SecurityKeyCodeContainer_t* BLEDevKeeper_GetSecurityKeyFromIndex(uint16_t index)
{
    SecurityKeyCodeContainer_t *SecurityKey = NULL;

    //WARM Application, change to  BLEDevKeeper_GetSecurityKeyFromIndexAlloc instead but need to free the returned pointer when done
    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevKeeper_GetSecurityKeyFromIndex is OBSOLETE!\r\n" );

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        if (gDeviceKeeperList[index].DevAddress.AddrLen)
        {
            if (gDeviceKeeperList[index].SecurityKey.Size)
            {

                SecurityKey = &gDeviceKeeperList[index].SecurityKey;
            }
        }
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return SecurityKey;

}

const FwVersion_t* BLEDevKeeper_GetFwVersionFromIndex(uint16_t index)
{

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    FwVersion_t *FwVersion = NULL;

    if (index < MAXIMUM_DEVICE_KEEPER_ALLOWED)
    {
        FwVersion = &gDeviceKeeperList[index].DevFwVersion;
    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    return FwVersion;

}


int16_t BLEDevKeeper_FindDeviceIndexFromGivenAddress(DeviceAddress_t DeviceAddr)
{
    return GetDeviceIndexFromKeeperList(DeviceAddr);
}


DeviceGenericInfo_t* BLEDevKeeper_FindDeviceInfoByMac (uint8_t* macaddr, int macaddrlen)
{
    DeviceGenericInfo_t* deviceInfo = NULL;
    DK_BLEPeripheral_t* peripheral;
    uint32_t objectId = 0;

    objectId = ObjectDictionary_FindPeripheralObjectIdByMac (macaddr, macaddrlen);
    if (!objectId) {
        goto exit;
    }

    peripheral  = ObjectDictionary_GetPeripheral (objectId);
    if (!peripheral) {
        goto exit;
    }

    deviceInfo = dxMemAlloc ("deviceInfo", 1, sizeof (DeviceGenericInfo_t));
    memcpy(deviceInfo->DevAddress.Addr, peripheral->MACAddress, B_ADDR_LEN_8);
    memcpy(deviceInfo->SecurityKey.value, peripheral->szSecurityKey, MAXIMUM_SECURITY_KEY_SIZE_ALLOWED);
    deviceInfo->SecurityKey.Size = peripheral->nSecurityKeyLen;
    deviceInfo->DeviceType = peripheral->DeviceType;

exit :

    return deviceInfo;
}


BLEDeviceKeeper_result_t BLEDevKeeper_DebugShowAllDeviceInfo()
{
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;
    uint16_t i = 0;
    uint16_t j = 0;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "*******************************************************************************\r\n" );
    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DevIndex    AdvType    AdDataLen   RSSI     FW VER   AdvLastT.        Address               \r\n" );

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(DevInfoAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_DEVICE_KEEPER_ALLOWED; i++)
    {
        if (gDeviceKeeperList[i].DevAddress.AddrLen)
        {

            dxTime_t CurrentSystemTime = dxTimeGetMS();

#if (ENABLE_SYSTEM_LOG && ENABLE_SYSTEM_DEBUGGER)
            float WasUpdatedInSecondsAgo = ((float)(CurrentSystemTime - gDeviceKeeperList[i].ManufacturerAdvDataLastUpdatedSystemTime)/1000.0f);
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%.2d           0X%x        %.2d          %d     %.2d        %0.2fS           ",  i,
                                                                                gDeviceKeeperList[i].AdvertisingType,
                                                                                gDeviceKeeperList[i].ManufactureAdvdataLen,
                                                                                gDeviceKeeperList[i].rssi,
                                                                                gDeviceKeeperList[i].pManufactureAdvdata[3],
                                                                                WasUpdatedInSecondsAgo); //Device Index
#endif

            for (j = 0; j < gDeviceKeeperList[i].DevAddress.AddrLen; j++)
            {
                if (j >= (gDeviceKeeperList[i].DevAddress.AddrLen - 1))
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%.2x     ", gDeviceKeeperList[i].DevAddress.pAddr[j] ); //Last Address
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%.2x:", gDeviceKeeperList[i].DevAddress.pAddr[j] ); //Address
                }
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "\r\n");

        }

    }

    // Release control
    dxSemGive(DevInfoAccessResourceProtectionSemaHandle);

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "*******************************************************************************\r\n" );

    return Result;
}

/******************************************************
 * The following function added by David
 ******************************************************/
extern BLEDeviceKeeper_result_t BLEDevKeeper_StartReportingDevices(dxQueueHandle_t* QueueToReport)
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    if (QueueToReport == NULL)
    {
        result = DEV_KEEPER_INVALID_PARAM;
    }
    else
    {
        DeviceInfoReport.DeviceReportQueueMsg = QueueToReport;
        DeviceInfoReport.ReportFilter = DEVICE_INFO_REPORT_FILTER_NONE;
    }

    return result;
}

extern BLEDeviceKeeper_result_t BLEDevKeeper_StopReportingDevices()
{
    BLEDeviceKeeper_result_t result = DEV_KEEPER_SUCCESS;

    DeviceInfoReport.ReportFilter = DEVICE_INFO_REPORT_FILTER_BLOCK_ALL;

    return result;
}


int16_t BLEDevKeeper_GetRepeaterIndex (void) {
    return repeaterIndex;
}
