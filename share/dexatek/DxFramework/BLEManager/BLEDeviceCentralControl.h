//============================================================================
// File: BLEDeviceCentralControl.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "BLEDeviceBridgeComm.h"
#include "BLEDeviceKeeper.h"
#include "dxSysDebugger.h"

/******************************************************
 *                      Defines
 ******************************************************/

/** @defgroup CENTRAL_CONN_UPDATE_EVENT_FLAG_DEFINES
 * @{
 */
#define CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG                   (1 << 0)
#define CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG              (1 << 1)
#define CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG         (1 << 2)
#define CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG             (1 << 3)
#define CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG              (1 << 4)
#define CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG  (1 << 5)

/** @} End CENTRAL_CONN_UPDATE_EVENT_FLAG_DEFINES */


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

    DEV_CENTRAL_CTRL_SUCCESS                = 0,    /**< Success */

    DEV_CENTRAL_CTRL_ERROR                  = -1,   /**< Error - Generic */
    DEV_CENTRAL_CTRL_SEND_MESSAGE_ERROR     = -2,   /**< Error - Failed to send message (Could be TimeOut due to traffic)*/
    DEV_CENTRAL_CTRL_INTERNAL_ERROR         = -3,   /**< Error - Internal error */
    DEV_CENTRAL_CTRL_NOT_CONNECTED_ERROR    = -4,   /**< Error - Device must be connected but its not */
    DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR    = -5,   /**< Error - Invalid parameter */
    DEV_CENTRAL_CTRL_OUT_OF_MEMORY_ERROR    = -6,   /**< Error - Out of system memory */
    DEV_CENTRAL_CTRL_SECURITY_ERROR         = -7    /**< Error - Security Error*/

} BLEDeviceCtrl_result_t;


// BLE Central Application states
typedef enum
{
  BLE_STATE_IDLE,
  BLE_STATE_SCANNING,
  BLE_STATE_CONNECTING,
  BLE_STATE_CONNECTED,
  BLE_STATE_DISCONNECTING,
  BLE_PERIPHERAL_STATE_IDLE,
  BLE_PERIPHERAL_STATE_ADVERTISING,
  BLE_PERIPHERAL_STATE_CONNECTED,

  BLE_STATE_UPPER_LIMIT,

} Central_States_t;


// BLE Concurrent Peripheral States
typedef enum
{
    BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT,
    BLE_CONCURRENT_PERIPHERAL_STATE_IDLE,
    BLE_CONCURRENT_PERIPHERAL_STATE_ADVERTISING,
    BLE_CONCURRENT_PERIPHERAL_STATE_CONNECTED,

    BLE_CONCURRENT_PERIPHERAL_STATE_NUM
} Concurrent_Peripheral_States_t;


// Discovery and service states
typedef enum
{

  BLE_DISC_STATE_NOT_AVAILABLE,       // Not Available because connection is not yet established
  BLE_DISC_STATE_IDLE,                // Idle
  BLE_DISC_STATE_SVC,                 // Service discovery
  BLE_DISC_STATE_CHAR,                // Characteristic discovery
  BLE_DISC_STATE_STATE_WRITING,       // Writting to characteristic
  BLE_DISC_STATE_STATE_READING,       // Reading from characteristic

} Srv_Discovery_States_t;


// Discovery error
typedef enum
{

  BLE_DISC_SERVICE_NOT_FOUND_ERROR = 1,
  BLE_DISC_SERVICE_TIME_OUT_ERROR,
  BLE_DISC_SERVICE_ERROR,
  BLE_DISC_SERVICE_UNABLE_TO_EXECUTE,
  BLE_DISC_CHAR_NOT_FOUND_ERROR,
  BLE_DISC_CHAR_TIME_OUT_ERROR,
  BLE_DISC_CHAR_ERROR,
  BLE_CHAR_WRITE_TIMEOUT,
  BLE_CHAR_WRITE_ERROR,
  BLE_CHAR_WRITE_UNABLE_TO_EXECUTE,
  BLE_CHAR_READ_TIMEOUT,
  BLE_CHAR_READ_ERROR,
  BLE_CHAR_READ_UNABLE_TO_EXECUTE,

} Srv_Discovery_Error_t;


typedef enum {
    BLE_ADVDATA_NO_NAME             = 0,
    BLE_ADVDATA_FULL_NAME           = 2,

    BLE_ADVDATA_NUM
} BLE_ADVDATA_NAME_TYPE_t;



/******************************************************
 *                 Type Definitions
 ******************************************************/


/******************************************************
 *                    Structures
 ******************************************************/

//WARNING: Changing this will ALSO affect BLE central FW as its tight together.
typedef struct
{
    uint16_t                CentralState;               //!< Current Central State (Central_States_t)
    uint16_t                TerminationReason;          //!< According to LL.h for both terminated and link established but with no success.
    uint8_t                 ConnectedAddr[B_ADDR_LEN_8];  //!< Address of connected device, This address will still be valid on first termination message, otherwise Address will be all 0

} BleStates_t;


typedef struct
{
    //!< Concurrent Peripheral State Concurrent_Peripheral_States_t
    uint16_t                ConcurrentPeripheralState;
    //!< According to LL.h for both terminated and link established but with no success.
    uint16_t                TerminationReason;
    //!< Address of connected device, This address will still be valid on first termination message, otherwise Address will be all 0
    uint8_t                 ConnectedAddr[B_ADDR_LEN_8];
} BleConcurrentPeripheralStates_t;


typedef struct
{
    dxTime_t                UpdatedTime;                //!< The system time of current central state update
    uint16_t                CentralState;               //!< Current Central State (Central_States_t)
    uint16_t                TerminationReason;          //!< According to LL.h for both terminated and link established but with no success.
    uint8_t                 ConnectedAddr[B_ADDR_LEN_8];  //!< Address of connected device, This address will still be valid on first termination message, otherwise Address will be all 0

} BleCentralStates_t;


typedef struct {
    dxTime_t                UpdatedTime;                //!< The system time of current central state update
    uint16_t                ConcurrentPeripheralState;  //!< Current Central State (Concurrent_Peripheral_States_t)
    uint16_t                TerminationReason;          //!< According to LL.h for both terminated and link established but with no success.
    uint8_t                 ConnectedAddr[B_ADDR_LEN_8];  //!< Address of connected device, This address will still be valid on first termination message, otherwise Address will be all 0

} BleCentralConcurrentPeripheralState_t;


typedef struct
{
  uint16_t                  AddressType;              //!< Address type of device to connect.
  uint8_t                   ConnectAddr[B_ADDR_LEN_8];  //!< Address of device to connect.
} BleCentralConnect_t;


typedef struct
{
  uint8_t                  Payload[DATA_TRANSFER_SIZE_PER_CHUNK];   //!< Peripheral Data Transfer Payload

#if 0   //OBSOLETE - Data Transfer should only be Payload (free form), if CRC check is needed it should be done from UART transfer protocol layer.
  uint16_t                 PayloadCRC;                                      //!< CRC check of Payload, if CRC is 0 then check is not available
#endif

} BlePeripheralDataTransfer_t;


typedef struct
{
  uint16_t  ServiceDiscoveryState;      //!< Service Discovery State (Srv_Discovery_States_t)
  uint16_t  DiscoveryErrorReason;       //!< When discovery request failed (Srv_Discovery_Error_t)
  uint8_t   ConnectedAddr[B_ADDR_LEN_8];  //!< Address of connected device, if no device is connected Address will be all 0

} BleCentralDiscoveryStates_t;


typedef struct
{
  uint8_t     ServiceUUIDLen;             //!< Service UUID lenght, 2 (16bit) or 16 (128bit). Use UUID_128_SIZE or UUID_16_SIZE
  uint8_t     CharUUIDLen;                //!< Char UUID lenght, 2 (16bit) or 16 (128bit). Use UUID_128_SIZE or UUID_16_SIZE
  uint16_t    DiscoveryIdHandShake;       //!< A unique ID provided from request side, this same ID will be returned on the feedback message
  uint8_t     ServiceUUID[UUID_128_SIZE]; //!< Service UUID although size is 16 byte but actual len is defined by ServiceUUIDLen
  uint8_t     CharUUID[UUID_128_SIZE];    //!< Char UUID although size is 16 byte but actual len is defined by CharUUIDLen

} BleCentralDiscoverChar_t;

typedef struct
{
  uint16_t     DiscoveryIdHandShake;      //!< A unique ID provided from request side, this same ID will be returned on the response message
  uint16_t     CharHandlerID;              //!< Found characteristic handler, use this handler id to send read/write BLE command. Should be NON Zero

} BleCentralDiscoverCharResponse_t;


typedef struct
{
  uint16_t     WriteIdHandShake;          //!< A unique ID provided from request side, this same ID will be returned on the response message
  uint16_t     CharHandlerIDToWrite;      //!< Characteristic handler ID to write the data to. Should be NON Zero
  uint16_t     DataWriteLen;              //!< Len of the data to write in DataWrite
  uint8_t      DataWrite[CHAR_READ_WRITE_DATA_MAX];             //!< Data container, please note that the size is 20 byte per transaction.

} BleCentralWriteChar_t;

typedef struct
{
  uint16_t     WriteIdHandShake;          //!< A unique ID provided from request side, this same ID will be returned on the response message
  uint16_t     CharHandlerIDToWrite;      //!< Characteristic handler ID to write the data to. Should be NON Zero

} BleCentralWriteCharResponse_t;


typedef struct
{
  uint16_t     ReadIdHandShake;          //!< A unique ID provided from request side, this same ID will be returned on the response message
  uint16_t     CharHandlerIDToRead;      //!< Characteristic handler ID to read the data from. Should be NON Zero
} BleCentralReadChar_t;


typedef struct
{
  uint16_t     ReadIdHandShake;           //!< A unique ID provided from request side, this same ID will be returned on the response message
  uint16_t     CharHandlerIDToRead;       //!< Characteristic handler ID to read the data from. Should be NON Zero
  uint16_t     DataReadLen;               //!< Len of the data available to read from in DataRead
  uint8_t      DataRead[CHAR_READ_WRITE_DATA_MAX];              //!< Data container, please note that the size is 20 byte per transaction.

} BleCentralReadCharResponse_t;


//WARNING: Changing this will ALSO affect BLE central FW as its tight together (BLE Central Main App and SBL).
typedef struct
{
    //!< Specify the image type, 0 - BootLoaded, 1 - Main App image
    uint8_t ImageType;
    //!< Specify the FW version (Middle)
    uint8_t VersionMiddle;
    //!< Specify the FW version (Major)
    uint8_t VersionMajor;
    //!< Specify the FW version (Minor)
    uint8_t VersionMinor;

} BleGatewayInfo_t;


typedef DX_PACK_STRUCT_BEGIN
{
	uint8_t			bd_addr[B_ADDR_LEN_8];
	uint8_t			addr_type;
} white_list_register_t;


typedef DX_PACK_STRUCT_BEGIN
{
	uint8_t 		solution_id;
	uint8_t			reserved[9];
} solution_info_t;


typedef struct {
  uint8_t securityKey[16];
} BleConcurrentPeripheralSecurityKey_t;


typedef struct {
    BLE_ADVDATA_NAME_TYPE_t nameType;
    uint8_t                 nameLength;
    char*                   name;
    uint8_t                 flag;
    uint8_t                 uuidParam;
    uint8_t*                uuids;
    uint16_t                interval;
    uint8_t                 manufLength;
    uint8_t*                manuf;
} BleConcurrentPeripheralAdvertiseData_t;


typedef struct {
    BLE_ADVDATA_NAME_TYPE_t nameType;
    uint8_t                 nameLength;
    char*                   name;
    uint8_t                 uuidParam;
    uint8_t*                uuids;
    uint8_t                 manufLength;
    uint8_t*                manuf;
} BleConcurrentPeripheralScanResponse_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint16_t    uuid;
    uint8_t     property;
    uint16_t    reserveSpace;
} BleCharacteristic_t;


typedef struct {
    uint16_t            serviceUuid;
    uint8_t             characteristicCount;
    BleCharacteristic_t characteristics[8];
} BleServiceCharacteristic_t;


typedef struct {
    uint16_t  payloadLen;
    uint8_t   payload[CONCURRENT_PERIPHERAL_DATA_IN_MAX];   //!< Peripheral Data Transfer Payload
} BleConcurrentPeripheralDataTransfer_t;


typedef struct {
    uint16_t                ServiceUUID;
    uint16_t                CharacteristicUUID;
    uint16_t                CCCD;   // Bit 0: notify enable
                                    // Bit 1: indicate enable
                                    // Bit 2-15: reserved
} BleConcurrentPeripheralCccd_t;



/******************************************************
 *               Function Declarations
 ******************************************************/


/** Register function so that even or updates are notified by calling the registered function.. Function argument is uint32_t and
 *  event flag is from CENTRAL_CONN_UPDATE_EVENT_FLAG_DEFINES.
 *  NOTE1:  Make sure to clear event flag right away (BEFORE you code process) when EventFunction is called.
 *  NOTE2:  Get event last known data right away before any process.
 *  NOTE3:  Central always record last status/data of the same event, so if there 2 identical event sent at the same time then
 *          only the latest status/data can be retrieve!. It is not recommend for same event concurrent process, must be ping pong based communication.
 * @param[in]  EventFunction : Function to be called when there is event.
 *
 * @return @ref BLEDeviceConnDiscCtrl_result_t
 */
extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_RegisterForFunctionEvent(dxEventHandler_t EventFunction);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemInfoUpdateReq();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemResetToSBL();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemSBLBootToApp();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectionStatusUpdateReq();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectionStatusUpdateRspHandler(uint8_t* ConnStatusUpdatePayload);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralStatusUpdateRspHandler (uint8_t* CcpConnStatusUpdatePayload);

extern const BleCentralStates_t* BLEDevCentralCtrl_GetLastKnownConnectionStatus (void);

extern const BleCentralConcurrentPeripheralState_t* BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState (void);

//Careful using this function.. See the function implementation for details
extern void BLEDevCentralCtrl_ClearConnectionStatus();

void BLEDevCentralCtrl_TryRecoverUART(void);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StartScan(uint16_t ScanDurationInMilliSeconds);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StopScan();


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectToDevice(const DeviceAddress_t* DeviceAddr);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DisconnectDevice();


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DiscoveryStatusUpdateReq();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DiscoveryStatusUpdateRspHandler(uint8_t* DiscStatusUpdatePayload);

extern const BleCentralDiscoveryStates_t* BLEDevCentralCtrl_GetLastKnownDiscoveryStatus();


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicDiscoveryReq(BleCentralDiscoverChar_t* CharOfServiceToDiscover);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicDiscoveryRspHandler(uint8_t* CharDiscoveryResponsePayload);

extern const BleCentralDiscoverCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicDiscHandler();


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicWrite(BleCentralWriteChar_t* CharWriteInfo);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicWriteRspHandler(uint8_t* CharWriteRspPayload);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicRead(BleCentralReadChar_t* CharReadInfo);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicReadRspHandler(uint8_t* CharReadRspPayload);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralStartAdvertising();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralStopAdvertising();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDisconnect();

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDataTransferWrite(const uint8_t* DataTransferOutPayload, uint8_t len);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck(uint8_t* DataTransferOutRspPayload);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralDataPayloadIntegrityCheck (uint8_t* DataTransferOutRspPayload);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralAdvertisementManufacturerDataUpdate(BlePeripheralDataTransfer_t* AdvData);

extern const BleCentralWriteCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicWriteRsp();

extern const BleCentralReadCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicReadRsp();

extern BleGatewayInfo_t BLEDevCentralCtrl_GetSystemInfoReadRsp(uint8_t* SystemInfoRspPayload, uint8_t PayloadLen);

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_FwBlockWriteMesageSend(uint8_t PayloadLen, const uint8_t* Payload);

/* For Internal Use ONLY (Test Purpose)*/
extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_FreeFormMesageSend(uint8_t MessageType, uint8_t CommandAux, uint8_t PayloadID, uint8_t PayloadLen, uint8_t* Payload);

// white list
BLEDeviceCtrl_result_t BLEDevCentralCtrl_SendRegisterWhiteList(white_list_register_t *reg);
BLEDeviceCtrl_result_t BLEDevCentralCtrl_SendClearWhiteList(void);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_GetConcurrentPeripheralStatus (void);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CustomizeConcurrentPeripheralBdAddress (
    BleCentralConnect_t* bleBdAddress
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralSecurityKey (
    BleConcurrentPeripheralSecurityKey_t* bleSecurityKey
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralAdvertiseData (
    BleConcurrentPeripheralAdvertiseData_t* bleAdvertiseData
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralScanResponse (
    BleConcurrentPeripheralScanResponse_t* bleScanResponse
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_AddConcurrentPeripheralServiceCharacteristic (
    BleServiceCharacteristic_t* serviceCharacteristic
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_EnableConcurrentPeripheralSystemUtils (
    uint16_t* systemUtilsMask
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StartConcurrentPeripheral (void);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StopConcurrentPeripheral (void);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DisconnectConcurrentPeripheral (void);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralDataOut (
    const uint8_t* DataTransferOutPayload,
    uint8_t len
);


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralFlush (void);
