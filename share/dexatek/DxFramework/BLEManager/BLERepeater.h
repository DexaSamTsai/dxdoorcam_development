//============================================================================
// File: BLeRepeater.h
//
// Author: John Lin
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once



//============================================================================
//    Define
//============================================================================

#define REPEATER_SERVICE_UUID                       0xEA01

#define REPEATER_UUID_MAC_REG                       0xEB01
#define REPEATER_UUID_READ_DEVICE_INDEX             0xEB02
#define REPEATER_UUID_ADV_HEADER                    0xEB03
#define REPEATER_UUID_ADV_PAYLOAD                   0xEB04
#define REPEATER_UUID_CONNECT_REQ                   0xEB05
#define REPEATER_UUID_CONNECT_RES                   0xEB06
#define REPEATER_UUID_DISCOVER_REQ                  0xEB07
#define REPEATER_UUID_DISCOVER_RES                  0xEB08
#define REPEATER_UUID_CHAR_ACCESS_REQ_HEADER        0xEB09
#define REPEATER_UUID_CHAR_ACCESS_REQ_PAYLOAD       0xEB0A
#define REPEATER_UUID_CHAR_ACCESS_RES_HEADER        0xEB0B
#define REPEATER_UUID_CHAR_ACCESS_RES_PAYLOAD       0xEB0C
#define REPEATER_UUID_SCAN_SWITCH                   0xEB0D
#define REPEATER_UUID_SEC_KEY                       0xEB0E
#define REPEATER_UUID_WATCH_MASK                    0xEB0F
#define REPEATER_UUID_READ_CONDITION_INDEX          0xEB10
#define REPEATER_UUID_CLEAR_CONDITION               0xEB11
#define REPEATER_UUID_WATCH_RESULT                  0xEB12
#define REPEATER_UUID_CLEAR_ADV_DATA                0xEB13
#define REPEATER_UUID_SVTO                          0xEB14
#define REPEATER_UUID_DUMMY                         0xEB15


#define REPEATER_STATUS_PENDING                     0xFF
#define REPEATER_STATUS_SUCCESS                     0x00

#define KEY_SIZE                                    16



//============================================================================
//    Enumerations
//============================================================================

typedef enum
{
    REMOTE_CONNECTION_CONNECTED = 1,
    REMOTE_CONNECTION_DISCONNECTED = 2,
    REMOTE_CONNECTION_INIT_CONNECT_FAIL = 3,
} connect_status_t;


typedef enum
{
    REPEATER_SCAN_TYPE_WATCHED_LIST_SCANNING = 0,
    REPEATER_SCAN_TYPE_GLOBAL_SCANNING = 1,
    REPEATER_SCAN_TYPE_OPEN_PAIR_SCANNING = 2,
} scan_type_value_t;


typedef enum {
    BLE_REPEATER_SETUP_NONE = 0,
    BLE_REPEATER_SETUP_FOUND,
    BLE_REPEATER_SETUP_ACTIVE,
    BLE_REPEATER_SETUP_CONNECTED,

    BLE_REPEATER_SETUP_NUM,
} BleRepeaterSetupState_t;



//============================================================================
//    Structures
//============================================================================



//============================================================================
//    Function Declarations
//============================================================================

/*---- Public Function -----------------------------------------------------*/

/**
 * @breif BleRepeater_Init
 */
void BleRepeater_Init (dxEventHandler_t* eventFunction);


/**
 * @breif BleRepeater_IsActive
 */
uint8_t BleRepeater_IsActive (void);


/**
 * @breif BleRepeater_IsConnected
 */
uint8_t BleRepeater_IsConnected (void);


/**
 * @breif BleRepeater_TryActivate
 */
dxOS_RET_CODE BleRepeater_TryActivate (uint8_t *adv_info, uint16_t adv_info_length, int16_t adv_device_index);


/**
 * @breif BleRepeater_GetStateFunction
 */
blemanager_state_func_t* BleRepeater_GetStateFunction (BLEManager_GenJobProcess_t input_sequence);


/**
 * @breif BleRepeater_Reset
 */
dxOS_RET_CODE BleRepeater_Reset (void);


/**
 * @breif BleRepeater_TurnOn
 */
void BleRepeater_TurnOn (DeviceAddress_t* deviceAddress);


/**
 * @breif BleRepeater_UpdateWatchList
 */
BLEDeviceCtrl_result_t BleRepeater_UpdateWatchList (void);


/**
 * @breif BleRepeater_StartOpenPairingScanning
 */
BLEDeviceCtrl_result_t BleRepeater_StartOpenPairingScanning (void);


/**
 * @breif BleRepeater_StopOpenPairingScanning
 */
BLEDeviceCtrl_result_t BleRepeater_StopOpenPairingScanning (void);


/**
 * @breif BleRepeater_NotifyAfterPairingDone
 */
void BleRepeater_NotifyAfterPairingDone (void);


/**
 * @breif BleRepeater_UpdateAdvertiseInfo
 */
BLEManager_GenJobProcess_t BleRepeater_UpdateAdvertiseInfo (dxBOOL forceUpdate);


/**
 * @breif BleRepeater_Connect
 */
BLEManager_GenJobProcess_t BleRepeater_Connect (DeviceAddress_t* deviceAddress);


/**
 * @breif BleRepeater_PreloadHandle
 */
BLEManager_GenJobProcess_t BleRepeater_PreloadHandle (void);


/**
 * @breif BleRepeater_PollToRegisterWatchList
 */
BLEManager_GenJobProcess_t BleRepeater_PollToRegisterWatchList (void);


/**
 * @breif BleRepeater_PollToOpenPairing
 */
BLEManager_GenJobProcess_t BleRepeater_PollToOpenPairing (void);
