//============================================================================
// File: BLEDeviceBridgeComm.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "dxOS.h"
#include "dxSysDebugger.h"

/******************************************************
 *                      Defines
 ******************************************************/
#define PAYLOAD_CRC_CALCULATE                       1
#define ENABLE_BLE_MEMORY_LOG                       1
#define ENABLE_PAYLOAD_ID_AS_SEQUENCIAL_MSG_DEBUG   0   //Gateway BLE must also compile as Sequencial debug (TODO: We could send message from here to configure)

#define MAXIMUM_RSP_MESSAGE_QUEUE_DEPT_ALLOWED      10
#define MAXIMUM_SEND_MESSAGE_QUEUE_DEPT_ALLOWED     10

#define MESSAGE_QUEUE_TIMEOUT_GENERIC               1000    //1 Second for generic message queue push/pop timeout

/******************************************************
 *                      Macros
 ******************************************************/

#define HI_UINT16(a) (((a) >> 8) & 0xFF)
#define LO_UINT16(a) ((a) & 0xFF)

//#define DEXATEK_BASE_UUID_128( uuid )   0x13, 0xDB, 0xBB, 0x6E, 0x5A, 0x60, 0xD3, 0xAF, 0x9D, 0x46, 0xD4, 0xF0, LO_UINT16( uuid ), HI_UINT16( uuid ), 0x27, 0x0D


/******************************************************
 *                    Constants
 ******************************************************/
#define GATEWAY_HEADER_PREAMBLE             0xDE
#define GATEWAY_HEADER_PAYLOAD_CRC_NONE     0
#define GATEWAY_HEADER_PAYLOAD_CRC_INIT     0

#define GATEWAY_HEADER_PREAMBLE_SIZE        1
#define GATEWAY_HEADER_PAYLOAD_CRC_SIZE     2
#define GATEWAY_MAXIMUM_PAYLOAD_SIZE        52  //This we already include the 2 byte of payload CRC.. Make sure this matched with BLE side

/*MESSAGE_TYPE_DEF*/
#define MSG_TYPE_LOG                        1
#define MSG_TYPE_SCAN                       2
#define MSG_TYPE_CENTRAL                    3
#define MSG_TYPE_DISCOVERY                  4
#define MSG_TYPE_SEND                       5
#define MSG_TYPE_SYSTEM                     6
#define MSG_TYPE_EXT                        7
#define MSG_TYPE_CONCURRENT_PERIPHERAL      8
#define MSG_TYPE_MAX                        9     //NOTE:  MSG_TYPE_MAX is always the max MSG number, if we add new message type then make sure we also increase MSG_TYPE_MAX

/*COMMAND_AUX_TYPES_DEF*/
#define LOG_AUX_TYPE_STRING                 1
#define LOG_AUX_TYPE_MEMORY_METRICS_DEBUG   2  //Payload See OSALMemoryMetric_t

#define SCAN_AUX_TYPE_DEVICE_INFO           20 //Payload See DeviceInfo_t + Device Info Data patch at the end
#define SCAN_AUX_TYPE_START_REQ             21 //No Payload Required
#define SCAN_AUX_TYPE_STOP_REQ              22 //No Payload required

#define CENTRAL_AUX_TYPE_STATUS_REQ         30 //No Payload required
#define CENTRAL_AUX_TYPE_STATUS_RSP         31 //See BleCentralStates_t
#define CENTRAL_AUX_TYPE_CONNECT_REQ        32 //Payload See BleCentralConnect_t
#define CENTRAL_AUX_TYPE_DISCONNECT_REQ     33 //No Payload required.. Will disconnect currently connected peripheral

#define CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_ADVERTISE_REQ      34 //No Payload required
#define CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_STOP_ADVERTISE_REQ 35 //No Payload required
#define CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_DISCONNECT_REQ     36 //No Payload required
#define CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_SET_ADV_MANUFACTURER_DATA_REQ     37 //Payload see BlePeripheralDataTransfer_t

#define DISCOVERY_AUX_TYPE_STATE_REQ        40 //No Payload required
#define DISCOVERY_AUX_TYPE_STATE_RSP        41 //Payload see BleCentralDiscoveryStates_t
#define DISCOVERY_AUX_TYPE_DISC_CHAR_REQ    42 //Payload see BleCentralDiscoverChar_t
#define DISCOVERY_AUX_TYPE_DISC_CHAR_RSP    43 //Payload see BleCentralDiscoverCharResponse_t

#define SEND_AUX_TYPE_WRITE_CHAR_REQ        50 //Payload see BleCentralWriteChar_t
#define SEND_AUX_TYPE_WRITE_CHAR_RSP        51 //Payload see BleCentralWriteCharResponse_t
#define SEND_AUX_TYPE_READ_CHAR_REQ         52 //Payload see BleCentralReadChar_t
#define SEND_AUX_TYPE_READ_CHAR_RSP         53 //Payload see BleCentralReadCharResponse_t
#define SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_OUT_REQ       54 //Payload See BlePeripheralDataTransfer_t
#define SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_OUT_RSP       55 //Payload See BlePeripheralDataTransfer_t
#define SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_IN_RSP        56 //Payload See BlePeripheralDataTransfer_t

#define SYSTEM_AUX_GET_INFO_REQ             70                                  //Avaiable in SBL and Main App - Payload see BleGatewayInfo_t
#define SYSTEM_AUX_GET_INFO_RSP             (SYSTEM_AUX_GET_INFO_REQ | 1)
#define SYSTEM_AUX_WRITE_FW_BLOCK_REQ       72                                  //Available only in SBL
#define SYSTEM_AUX_WRITE_FW_BLOCK_RSP       (SYSTEM_AUX_WRITE_FW_BLOCK_REQ | 1)
#define SYSTEM_AUX_REBOOT_TO_APP_REQ        74                                  //Available only in SBL
#define SYSTEM_AUX_REBOOT_TO_APP_RSP        (SYSTEM_AUX_REBOOT_TO_APP_REQ | 1)
#define SYSTEM_AUX_REBOOT_TO_SBL_REQ        76                                  //Available only in Main App
#define SYSTEM_AUX_REBOOT_TO_SBL_RSP        (SYSTEM_AUX_REBOOT_TO_SBL_REQ | 1)
#define SYSTEM_AUX_CUSTOMIZE_BD_ADDR_REQ    0x4E
#define SYSTEM_AUX_CUSTOMIZE_BD_ADDR_RSP    0x4F

#define EXT_REGISTER_WHITE_LIST_REQ                         80
#define EXT_REGISTER_WHITE_LIST_RES                         (EXT_REGISTER_WHITE_LIST_REQ | 1)
#define EXT_RESET_WHITE_LIST_REQ                            82
#define EXT_GET_SOLUTION_REQ                                84
#define EXT_GET_SOLUTION_RES                                (EXT_GET_SOLUTION_REQ | 1)

#define CONCURRENT_PERIPHERAL_ADV_SETUP_REQ                 0x60
#define CONCURRENT_PERIPHERAL_ADV_SETUP_RSP                 0x61
#define CONCURRENT_PERIPHERAL_SCAN_RSP_SETUP_REQ            0x62
#define CONCURRENT_PERIPHERAL_SCAN_RSP_SETUP_RSP            0x63
#define CONCURRENT_PERIPHERAL_SECURITY_KEY_REQ              0x64 // See BleConcurrentPeripheralSecurityKey_t
#define CONCURRENT_PERIPHERAL_SECURITY_KEY_RSP              0x65
#define CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_REQ         0x66
#define CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_RSP         0x67
#define CONCURRENT_PERIPHERAL_ACCESSORY_IDENTIFY_RSP        0x69
#define CONCURRENT_PERIPHERAL_SYSTEM_UTILS_ENABLE_MASK_REQ  0x6A
#define CONCURRENT_PERIPHERAL_SYSTEM_UTILS_ENABLE_MASK_RSP  0x6B
#define CONCURRENT_PERIPHERAL_START_REQ                     0x6C
#define CONCURRENT_PERIPHERAL_STOP_REQ                      0x6E
#define CONCURRENT_PERIPHERAL_STATUS_REQ                    0x70
#define CONCURRENT_PERIPHERAL_STATUS_RSP                    0x71 // See BleConcurrentPeripheralStates_t
#define CONCURRENT_PERIPHERAL_DISCONNECT_REQ                0x72
#define CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_OUT_REQ         0x74 // Payload See BlePeripheralDataTransfer_t
#define CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_OUT_RSP         0x75 // Payload See BlePeripheralDataTransfer_t
#define CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_IN_RSP          0x77 // Payload See BlePeripheralDataTransfer_t
#define CONCURRENT_PERIPHERAL_AUX_TYPE_FLUSH_DATA_OUT_REQ   0x78
#define CONCURRENT_PERIPHERAL_AUX_TYPE_FLUSH_DATA_OUT_RSP   0x79 // It contains 2 bytes payload for status while 0 means setting successfully.
#define CONCURRENT_PERIPHERAL_AUX_TYPE_CCCD_SET_DONE_RSP    0x81 // Payload See BleConcurrentPeripheralCccd_t

#define UART_REPORT_ERROR_PATTERN_DMA_ERROR                 0x77
#define UART_REPORT_ERROR_PATTERN_LENGTH_ERROR              0x66

/* Other Misc */

//! Default Public and Random Address Length
//#define B_ADDR_LEN                       6
#define B_ADDR_LEN_8                     8

// The maximum data size of read/write per transfer
#define CHAR_READ_WRITE_DATA_MAX                20  //Per transfer... Do not change other than 20.
#define CHAR_READ_WRITE_DATA_MAX_MPAGE          240 //Per transfer is still 20 but we will allow multi transaction of total 240 byte max.
#define CHAR_READ_EXT_DATA_MAX                  40

#define CONCURRENT_PERIPHERAL_DATA_IN_MAX       240



/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

    DEV_BRIDGECOMM_SUCCESS              = 0,    /**< Success */

    DEV_BRIDGECOMM_RTOS_ERROR           = -1,   /**< Error - Related to usage from system RTOS */
    DEV_BRIDGECOMM_PARAM_ERROR          = -2,   /**< Error - Parameter Related error */

} BLEDeviceBridgeComm_result_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    uint8_t MessageType;    //!< Main Message - See MESSAGE_TYPE_DEF
    uint8_t CommandAux;     //!< Auxiliar or Command from Message Type - See COMMAND_AUX_TYPES_DEF
    uint8_t PayloadID;      //!< Unique Payload ID, in case Payload CRC error this ID will be sent back for re-request
    uint8_t PayloadLen;     //!< Total payload size in byte (not including the payload CRC)
} BleGatewayHeader_t;

typedef struct
{
    BleGatewayHeader_t  Header;                                     //!< Header
    uint8_t             Payload[GATEWAY_MAXIMUM_PAYLOAD_SIZE];      //!< Payload, including the payload CRC at the end of payload
} BleBridgeCommMsg_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes

typedef struct
{
  uint16_t CurrentMemoryAlloc;    //!< Current Memory Allocated
  uint16_t MaxMemmoryAlloc;       //!< Maximum memory allocated

} OSALMemoryMetric_t;


/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

extern BLEDeviceBridgeComm_result_t BLEDevBridgeComm_Init(void);

extern dxQueueHandle_t* BLEDevBridgeComm_GetResponseMsgQueueHdl(void);

extern dxQueueHandle_t* BLEDevBridgeComm_GetSendMsgQueueHdl(void);

void BLEDevBridgeComm_ResetMsgQueue (void);

extern BLEDeviceBridgeComm_result_t BLEDevBridgeComm_Build128UUID(uint8_t* UUID128Out, uint16_t UUID16In);

