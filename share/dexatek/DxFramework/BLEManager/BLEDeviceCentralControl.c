//============================================================================
// File: BLEDeviceCentralControl.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "Utils.h"
#include "dxOS.h"
#include "dxBSP.h"
#include "BLEDeviceCentralControl.h"


/******************************************************
 *                      Defines
 ******************************************************/

#define CENTRAL_CTRL_WORKER_THREAD_PRIORITY             DXTASK_PRIORITY_VERY_HIGH //DX_DEFAULT_WORKER_PRIORITY
#define CENTRAL_CTRL_WORKER_THREAD_STACK_SIZE           512
#define CENTRAL_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE      5

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

static BleCentralWriteCharResponse_t            stCharWriteRsp;
static BleCentralReadCharResponse_t             stCharReadRsp;
static BleCentralDiscoverCharResponse_t         stCharacteristicDiscHdl;
static BleCentralStates_t                       stCurrentConnectionState;
static BleCentralConcurrentPeripheralState_t    stCurrentConcurrentPeripheralState;
static BleCentralDiscoveryStates_t              stCurrentDiscoveryState;
static dxWorkweTask_t                           stEventWorkerThread;
static dxEventHandler_t                         stRegisteredEventFunction;
static uint32_t                                 FunctionEventFlag;



/******************************************************
 *               Local Function Definitions
 ******************************************************/


BLEDeviceCtrl_result_t SendRequestToMessageQueue(BleBridgeCommMsg_t* SendMsg)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    dxQueueHandle_t* MsgSendQueueHdl = BLEDevBridgeComm_GetSendMsgQueueHdl();

    if (MsgSendQueueHdl)
    {

        BleBridgeCommMsg_t* BridgeCommSendMsg = dxMemAlloc( "BridgeComm Send Msg Buff", 1, sizeof(BleBridgeCommMsg_t));
        if (BridgeCommSendMsg)
        {
            memcpy(BridgeCommSendMsg, SendMsg, sizeof(BleBridgeCommMsg_t));

            if (dxQueueSend( *MsgSendQueueHdl, &BridgeCommSendMsg, MESSAGE_QUEUE_TIMEOUT_GENERIC ) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: SendRequestToMessageQueue - Error pushing message to queue (Could be due to timeout) \r\n" );
                Result = DEV_CENTRAL_CTRL_SEND_MESSAGE_ERROR;
                dxMemFree(BridgeCommSendMsg);
            }

        }
        else
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: SendRequestToMessageQueue - Error pushing message to queue (Out of system memory) \r\n" );
            Result = DEV_CENTRAL_CTRL_OUT_OF_MEMORY_ERROR;
        }

    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: SendRequestToMessageQueue - MsgSendQueueHdl is NULL \r\n");
        Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
    }

    return Result;
}



/******************************************************
 *               Function Definitions
 ******************************************************/

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_RegisterForFunctionEvent(dxEventHandler_t EventFunction)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if (EventFunction)
    {
        if (stRegisteredEventFunction == NULL)  //First time, we create worker thread first
        {
            stRegisteredEventFunction = EventFunction;

            dxOS_RET_CODE WorkerThreadResult = dxWorkerTaskCreate("BLECTL W",
                                                                  &stEventWorkerThread,
                                                                  CENTRAL_CTRL_WORKER_THREAD_PRIORITY,
                                                                  CENTRAL_CTRL_WORKER_THREAD_STACK_SIZE,
                                                                  CENTRAL_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE);
            if (WorkerThreadResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "RegisterForFunctionEvent - Failed to create worker thread\r\n" );
                stRegisteredEventFunction = NULL;
                Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
            }
            else
            {
                GSysDebugger_RegisterForThreadDiagnosticGivenName(&stEventWorkerThread.WTask, "BLEDevCentralCtrl Event Thread");
            }
        }
    }

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemInfoUpdateReq()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SYSTEM;
    SendMsg.Header.CommandAux = SYSTEM_AUX_GET_INFO_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);


    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemResetToSBL()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SYSTEM;
    SendMsg.Header.CommandAux = SYSTEM_AUX_REBOOT_TO_SBL_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);


    return Result;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_SystemSBLBootToApp()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SYSTEM;
    SendMsg.Header.CommandAux = SYSTEM_AUX_REBOOT_TO_APP_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);


    return Result;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectionStatusUpdateReq()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_AUX_TYPE_STATUS_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;

}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectionStatusUpdateRspHandler(uint8_t* ConnStatusUpdatePayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleStates_t* ConnState = (BleStates_t*)ConnStatusUpdatePayload;

    stCurrentConnectionState.UpdatedTime = dxTimeGetMS();
    stCurrentConnectionState.CentralState = ConnState->CentralState;
    stCurrentConnectionState.TerminationReason = ConnState->TerminationReason;
    memcpy(stCurrentConnectionState.ConnectedAddr, ConnState->ConnectedAddr, B_ADDR_LEN_8);

    if (stRegisteredEventFunction)
    {

        if (FunctionEventFlag & CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG)
        {
//            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG arrived while previous same event is still not being handled\r\n" );
        }

        FunctionEventFlag |= CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag );
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ConnectionStatusUpdateRspHandler - Failed to Update Event\r\n");
            Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
        }
    }

    return Result;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralStatusUpdateRspHandler (
    uint8_t* CcpConnStatusUpdatePayload
) {
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleConcurrentPeripheralStates_t* ccpState = (BleConcurrentPeripheralStates_t*) CcpConnStatusUpdatePayload;

    stCurrentConcurrentPeripheralState.UpdatedTime = dxTimeGetMS ();
    stCurrentConcurrentPeripheralState.ConcurrentPeripheralState = ccpState->ConcurrentPeripheralState;
    stCurrentConcurrentPeripheralState.TerminationReason = ccpState->TerminationReason;
    memcpy (stCurrentConcurrentPeripheralState.ConnectedAddr, ccpState->ConnectedAddr, B_ADDR_LEN_8);

    if (stRegisteredEventFunction) {

        if (FunctionEventFlag & CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG arrived while previous same event is still not being handled\r\n");
        }

        FunctionEventFlag |= CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent (&stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag);
        if (EventResult != DXOS_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ConnectionStatusUpdateRspHandler - Failed to Update Event\r\n");
            Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
        }
    }

    return Result;
}


void BLEDevCentralCtrl_ClearConnectionStatus()
{
    //NOTE: This function is only intended for internal use and know exactly why this should be cleared..
    //      One usage is when we "RESET" the ble central device and so for state synch safety precaution we clear the state so that the state does not stay
    //      in its previous state value while ble central device does not report its current state (eg, UART communication issue)
    memset (&stCurrentConnectionState, 0, sizeof (stCurrentConnectionState));
    memset (&stCurrentConcurrentPeripheralState, 0, sizeof (stCurrentConcurrentPeripheralState));

}

const BleCentralStates_t* BLEDevCentralCtrl_GetLastKnownConnectionStatus()
{
    return &stCurrentConnectionState;
}


const BleCentralConcurrentPeripheralState_t* BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState (void) {
    return &stCurrentConcurrentPeripheralState;
}


void BLEDevCentralCtrl_TryRecoverUART(void)
{
    BleBridgeCommMsg_t  SendMsg;
    uint8_t count = 30;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_AUX_TYPE_STATUS_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    while (count--)
    {
        SendRequestToMessageQueue(&SendMsg);
        dxTimeDelayMS(10);
    }

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevCentralCtrl_TryRecoverUART\r\n");
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConnectToDevice(const DeviceAddress_t* DeviceAddr)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR; //DEV_CENTRAL_CTRL_SUCCESS;

    if (DeviceAddr->AddrLen == B_ADDR_LEN_8)
    {
        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;

        SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
        SendMsg.Header.CommandAux = CENTRAL_AUX_TYPE_CONNECT_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof(BleCentralConnect_t);
        PayloadPtr = SendMsg.Payload;

        memcpy(PayloadPtr, &DeviceAddr->AddressType, sizeof(DeviceAddr->AddressType)); PayloadPtr += sizeof(DeviceAddr->AddressType);

        //We will need to adjust the address (reverse - LSB in first byte) before sending it to back to central for peripheral connection
        *PayloadPtr = DeviceAddr->pAddr[7]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[6]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[5]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[4]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[3]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[2]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[1]; PayloadPtr++;
        *PayloadPtr = DeviceAddr->pAddr[0]; PayloadPtr++;

        /*
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevCentralCtrl_ConnectToDevice - address len = %d\r\n", DeviceAddr->AddrLen);
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Device Address to connect : ");
        uint16_t i = 0;
        for (i = 0; i < DeviceAddr->AddrLen; i++)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%x:", DeviceAddr->pAddr[i]);
        }

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "\r\n");
        */

        Result = SendRequestToMessageQueue(&SendMsg);
    }

    return Result;

}

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DisconnectDevice()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_AUX_TYPE_DISCONNECT_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_StartScan(uint16_t ScanDurationInMilliSeconds)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;
    (void)ScanDurationInMilliSeconds; //TODO:

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SCAN;
    SendMsg.Header.CommandAux = SCAN_AUX_TYPE_START_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StopScan()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SCAN;
    SendMsg.Header.CommandAux = SCAN_AUX_TYPE_STOP_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_DiscoveryStatusUpdateReq()
{

    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_NOT_CONNECTED_ERROR;

    if (stCurrentConnectionState.CentralState == BLE_STATE_CONNECTED)
    {

        BleBridgeCommMsg_t SendMsg;

        SendMsg.Header.MessageType = MSG_TYPE_DISCOVERY;
        SendMsg.Header.CommandAux = DISCOVERY_AUX_TYPE_STATE_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = 0;

        Result = SendRequestToMessageQueue(&SendMsg);

    }

    return Result;

}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_DiscoveryStatusUpdateRspHandler(uint8_t* DiscStatusUpdatePayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleCentralDiscoveryStates_t* DiscoveryState = (BleCentralDiscoveryStates_t*)DiscStatusUpdatePayload;

    stCurrentDiscoveryState.ServiceDiscoveryState = DiscoveryState->ServiceDiscoveryState;
    stCurrentDiscoveryState.DiscoveryErrorReason = DiscoveryState->DiscoveryErrorReason;
    memcpy(stCurrentDiscoveryState.ConnectedAddr, DiscoveryState->ConnectedAddr, B_ADDR_LEN_8);

    //NOTE: We only report error, otherwise there is no need to report as no one is listening to this status if there is no error
    if ((stCurrentDiscoveryState.DiscoveryErrorReason != 0) && (stCurrentDiscoveryState.DiscoveryErrorReason > BLE_DISC_SERVICE_NOT_FOUND_ERROR))
    {
        if (stRegisteredEventFunction)
        {
            if (FunctionEventFlag & CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG arrived while previous same event is still not being handled\r\n");
            }

            FunctionEventFlag |= CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG;

            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag );
            if (EventResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ConnectionStatusUpdateRspHandler - Failed to Update Event\r\n");
                Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
            }
        }
    }

    return Result;
}

const BleCentralDiscoveryStates_t* BLEDevCentralCtrl_GetLastKnownDiscoveryStatus()
{
    return &stCurrentDiscoveryState;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicDiscoveryReq(BleCentralDiscoverChar_t* CharOfServiceToDiscover)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    uint8_t*            PayloadPtr = NULL;
    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_DISCOVERY;
    SendMsg.Header.CommandAux = DISCOVERY_AUX_TYPE_DISC_CHAR_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = sizeof(BleCentralDiscoverChar_t);
    PayloadPtr = SendMsg.Payload;

    memcpy(PayloadPtr, CharOfServiceToDiscover, sizeof(BleCentralDiscoverChar_t));

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;

}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicDiscoveryRspHandler(uint8_t* CharDiscoveryResponsePayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleCentralDiscoverCharResponse_t* CharDiscovery = (BleCentralDiscoverCharResponse_t*)CharDiscoveryResponsePayload;

    stCharacteristicDiscHdl.DiscoveryIdHandShake = CharDiscovery->DiscoveryIdHandShake;
    stCharacteristicDiscHdl.CharHandlerID = CharDiscovery->CharHandlerID;

    if (stRegisteredEventFunction)
    {
        if (FunctionEventFlag & CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG arrived while previous same event is still not being handled\r\n" );
        }

        FunctionEventFlag |= CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag );
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevCentralCtrl_CharacteristicDiscoveryRspHandler - Failed to Update Event\r\n" );
            Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
        }
    }

    return Result;

}


const BleCentralDiscoverCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicDiscHandler()
{
    return &stCharacteristicDiscHdl;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicWrite(BleCentralWriteChar_t* CharWriteInfo)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if ((!CharWriteInfo) || (CharWriteInfo->DataWriteLen > CHAR_READ_WRITE_DATA_MAX))
    {
        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }
    else
    {
        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;

        SendMsg.Header.MessageType = MSG_TYPE_SEND;
        SendMsg.Header.CommandAux = SEND_AUX_TYPE_WRITE_CHAR_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof(BleCentralWriteChar_t);
        PayloadPtr = SendMsg.Payload;

        memcpy(PayloadPtr, CharWriteInfo, sizeof(BleCentralWriteChar_t));

        Result = SendRequestToMessageQueue(&SendMsg);
    }

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicWriteRspHandler(uint8_t* CharWriteRspPayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleCentralWriteCharResponse_t* WriteRspHdl = (BleCentralWriteCharResponse_t*)CharWriteRspPayload;

    stCharWriteRsp.CharHandlerIDToWrite = WriteRspHdl->CharHandlerIDToWrite;
    stCharWriteRsp.WriteIdHandShake = WriteRspHdl->WriteIdHandShake;

    if (stRegisteredEventFunction)
    {
        if (FunctionEventFlag & CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG arrived while previous same event is still not being handled\r\n" );
        }

        FunctionEventFlag |= CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag );
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevCentralCtrl_CharacteristicWriteRspHandler - Failed to Update Event\r\n");
            Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
        }
    }

    return Result;

}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicRead(BleCentralReadChar_t* CharReadInfo)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if (!CharReadInfo)
    {
        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }
    else
    {
        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;

        SendMsg.Header.MessageType = MSG_TYPE_SEND;
        SendMsg.Header.CommandAux = SEND_AUX_TYPE_READ_CHAR_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof(BleCentralReadChar_t);
        PayloadPtr = SendMsg.Payload;

        memcpy(PayloadPtr, CharReadInfo, sizeof(BleCentralReadChar_t));

        Result = SendRequestToMessageQueue(&SendMsg);
    }

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_CharacteristicReadRspHandler(uint8_t* CharReadRspPayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleCentralReadCharResponse_t* ReadRspHdl = (BleCentralReadCharResponse_t*)CharReadRspPayload;

    stCharReadRsp.CharHandlerIDToRead = ReadRspHdl->CharHandlerIDToRead;
    stCharReadRsp.ReadIdHandShake = ReadRspHdl->ReadIdHandShake;

    stCharReadRsp.DataReadLen = ReadRspHdl->DataReadLen;
    memcpy(stCharReadRsp.DataRead, ReadRspHdl->DataRead, stCharReadRsp.DataReadLen);


    if (stRegisteredEventFunction)
    {
        if (FunctionEventFlag & CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG arrived while previous same event is still not being handled\r\n");
        }

        FunctionEventFlag |= CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&stEventWorkerThread, stRegisteredEventFunction, &FunctionEventFlag);
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEDevCentralCtrl_CharacteristicReadRspHandler - Failed to Update Event\r\n");
            Result = DEV_CENTRAL_CTRL_INTERNAL_ERROR;
        }
    }

    return Result;

}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralStartAdvertising()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_ADVERTISE_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralStopAdvertising()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_STOP_ADVERTISE_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;

}

extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDisconnect()
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
    SendMsg.Header.CommandAux = CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_DISCONNECT_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;

}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_FwBlockWriteMesageSend(uint8_t PayloadLen, const uint8_t* Payload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    uint8_t*            PayloadPtr = NULL;
    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SYSTEM;
    SendMsg.Header.CommandAux = SYSTEM_AUX_WRITE_FW_BLOCK_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = PayloadLen;
    PayloadPtr = SendMsg.Payload;

    memcpy(PayloadPtr, Payload, PayloadLen);

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDataTransferWrite(const uint8_t* DataTransferOutPayload, uint8_t len)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if ((!DataTransferOutPayload) || (len > DATA_TRANSFER_SIZE_PER_CHUNK))
    {
        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }
    else
    {

        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;
        BlePeripheralDataTransfer_t PeripheralDataOut;

        memcpy(PeripheralDataOut.Payload, DataTransferOutPayload, len);
        //crc16_P8005_append((uint8_t*)&PeripheralDataOut, sizeof(BlePeripheralDataTransfer_t), GATEWAY_HEADER_PAYLOAD_CRC_INIT);

        SendMsg.Header.MessageType = MSG_TYPE_SEND;
        SendMsg.Header.CommandAux = SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_OUT_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof(BlePeripheralDataTransfer_t);
        PayloadPtr = SendMsg.Payload;

        memcpy(PayloadPtr, &PeripheralDataOut, sizeof(BlePeripheralDataTransfer_t));

        Result = SendRequestToMessageQueue(&SendMsg);
    }


    return Result;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralAdvertisementManufacturerDataUpdate(BlePeripheralDataTransfer_t* AdvData)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if (!AdvData)
    {
        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }
    else
    {
        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;

        SendMsg.Header.MessageType = MSG_TYPE_CENTRAL;
        SendMsg.Header.CommandAux = CENTRAL_PERIPHERAL_ROLE_AUX_TYPE_SET_ADV_MANUFACTURER_DATA_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof(BlePeripheralDataTransfer_t);
        PayloadPtr = SendMsg.Payload;

        memcpy(PayloadPtr, AdvData, sizeof(BlePeripheralDataTransfer_t));

        Result = SendRequestToMessageQueue(&SendMsg);

    }

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck(uint8_t* DataTransferOutRspPayload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

#if 0   //OBSOLETE

    BlePeripheralDataTransfer_t* DataOutTransferRspHdl = (BlePeripheralDataTransfer_t*)DataTransferOutRspPayload;

    if ((crc16_P8005_check((uint8_t*)DataOutTransferRspHdl, sizeof(BlePeripheralDataTransfer_t), GATEWAY_HEADER_PAYLOAD_CRC_INIT)) || (DataOutTransferRspHdl->PayloadCRC == 0))
    {
        Result = DEV_CENTRAL_CTRL_SUCCESS;
    }
    else
    {
        Result = DEV_CENTRAL_CTRL_ERROR;
    }
#endif


    /*
    G_SYS_DBG_LOG_NV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DataTransferPayload (CRC = %x)\r\n", DataOutTransferRspHdl->PayloadCRC);
    uint8_t Count = 0;
    for (Count = 0; Count < 20; Count++)
    {
        G_SYS_DBG_LOG_NV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%x,", DataOutTransferRspHdl->Payload[Count]);
    }
    G_SYS_DBG_LOG_NV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "\r\n");
    */

    return Result;
}


BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralDataPayloadIntegrityCheck (
    uint8_t* DataTransferOutRspPayload
) {
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    /*
    G_SYS_DBG_LOG_NV1( "DataTransferPayload (CRC = %x)\r\n", DataOutTransferRspHdl->PayloadCRC);
    uint8_t Count = 0;
    for (Count = 0; Count < 20; Count++)
    {
        G_SYS_DBG_LOG_NV1( "%x,", DataOutTransferRspHdl->Payload[Count]);
    }
    G_SYS_DBG_LOG_NV( "\r\n");
    */

    return Result;
}


const BleCentralWriteCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicWriteRsp()
{
    return &stCharWriteRsp;
}


const BleCentralReadCharResponse_t* BLEDevCentralCtrl_GetLastCharacteristicReadRsp()
{
    return &stCharReadRsp;
}


BleGatewayInfo_t BLEDevCentralCtrl_GetSystemInfoReadRsp(uint8_t* SystemInfoRspPayload, uint8_t PayloadLen)
{
    BleGatewayInfo_t gatewayInfo;

    if (PayloadLen == sizeof(BleGatewayInfo_t))
    {
        memcpy(&gatewayInfo, SystemInfoRspPayload, PayloadLen);
    }
    else
    {
        //This is purely for BACKWARD compatibility of older FW.. This is just for very minor amount of FW out there and can be removed once
        //we hit production.
        gatewayInfo.ImageType = SystemInfoRspPayload[0];
        gatewayInfo.VersionMiddle = SystemInfoRspPayload[1];
        gatewayInfo.VersionMajor = SystemInfoRspPayload[2];
        gatewayInfo.VersionMinor = 0;

    }


    return gatewayInfo;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_FreeFormMesageSend(uint8_t MessageType, uint8_t CommandAux, uint8_t PayloadID, uint8_t PayloadLen, uint8_t* Payload)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if (PayloadLen && !Payload)
    {
        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }
    else
    {
        uint8_t*            PayloadPtr = NULL;
        BleBridgeCommMsg_t  SendMsg;

        SendMsg.Header.MessageType = MessageType;
        SendMsg.Header.CommandAux = CommandAux;
        SendMsg.Header.PayloadID = PayloadID;
        SendMsg.Header.PayloadLen = 0;

        if (PayloadLen && Payload)
        {
            SendMsg.Header.PayloadLen = PayloadLen;
            PayloadPtr = SendMsg.Payload;
            memcpy(PayloadPtr, Payload, PayloadLen);
        }

        Result = SendRequestToMessageQueue(&SendMsg);
    }

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_SendRegisterWhiteList(white_list_register_t *reg)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_EXT;
    SendMsg.Header.CommandAux = EXT_REGISTER_WHITE_LIST_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = sizeof(white_list_register_t);

    white_list_register_t *ptr1 = (white_list_register_t *)SendMsg.Payload;
    memcpy(ptr1->bd_addr, reg->bd_addr, B_ADDR_LEN_8);
    ptr1->addr_type = reg->addr_type;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}

BLEDeviceCtrl_result_t BLEDevCentralCtrl_SendClearWhiteList(void)
{
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_EXT;
    SendMsg.Header.CommandAux = EXT_RESET_WHITE_LIST_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = 0;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_GetConcurrentPeripheralStatus (void) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_STATUS_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = 0;

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_CustomizeConcurrentPeripheralBdAddress (
    BleCentralConnect_t* bleBdAddress
) {
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR; //DEV_CENTRAL_CTRL_SUCCESS;

    uint8_t*            PayloadPtr = NULL;
    BleBridgeCommMsg_t  SendMsg;

    SendMsg.Header.MessageType = MSG_TYPE_SYSTEM;
    SendMsg.Header.CommandAux = SYSTEM_AUX_CUSTOMIZE_BD_ADDR_REQ;
    SendMsg.Header.PayloadID = 0;
    SendMsg.Header.PayloadLen = sizeof (BleCentralConnect_t);
    PayloadPtr = SendMsg.Payload;

    memcpy(PayloadPtr, &bleBdAddress->AddressType, sizeof (bleBdAddress->AddressType));
    PayloadPtr += sizeof (bleBdAddress->AddressType);

    //We will need to adjust the address (reverse - LSB in first byte) before sending it to back to central for peripheral connection
    *PayloadPtr = bleBdAddress->ConnectAddr[7]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[6]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[5]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[4]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[3]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[2]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[1]; PayloadPtr++;
    *PayloadPtr = bleBdAddress->ConnectAddr[0]; PayloadPtr++;

    Result = SendRequestToMessageQueue(&SendMsg);

    return Result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralSecurityKey (
    BleConcurrentPeripheralSecurityKey_t* bleSecurityKey
) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;
    uint8_t* payload = NULL;
    uint8_t i;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_SECURITY_KEY_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = sizeof (BleConcurrentPeripheralSecurityKey_t);
    payload = message.Payload;

    for (i = 0; i < 16; i++) {
        payload[i] = bleSecurityKey->securityKey[15 - i];
    }

    result = SendRequestToMessageQueue (&message);

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralAdvertiseData (
    BleConcurrentPeripheralAdvertiseData_t* bleAdvertiseData
) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;
    uint8_t* payload = NULL;
    uint8_t uuidCount;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_ADV_SETUP_REQ;
    message.Header.PayloadID = 0;
    payload = message.Payload;

    *payload = (uint8_t) bleAdvertiseData->nameType;
    payload++;

    *payload = bleAdvertiseData->nameLength;
    payload++;

    if (bleAdvertiseData->nameLength) {
        memcpy (payload, bleAdvertiseData->name, sizeof (char) * bleAdvertiseData->nameLength);
        payload += sizeof (char) * bleAdvertiseData->nameLength;
    }

    *payload = bleAdvertiseData->flag;
    payload++;

    *payload = bleAdvertiseData->uuidParam;
    payload++;

    if (bleAdvertiseData->uuidParam & 0x40) { // 128 bits (16 bytes)
        uuidCount = 16 * (bleAdvertiseData->uuidParam & 0x3F);
    } else {
        uuidCount = 3 * (bleAdvertiseData->uuidParam & 0x3F);
    }
    if (uuidCount) {
        memcpy (payload, bleAdvertiseData->uuids, sizeof (uint8_t) * uuidCount);
        payload += sizeof (uint8_t) * uuidCount;
    }

    memcpy (payload, &bleAdvertiseData->interval, sizeof (uint16_t));
    payload += sizeof (int16_t);

    *payload = bleAdvertiseData->manufLength;
    payload++;

    if (bleAdvertiseData->manufLength) {
        memcpy (payload, bleAdvertiseData->manuf, sizeof (uint8_t) * bleAdvertiseData->manufLength);
        payload += sizeof (uint8_t) * bleAdvertiseData->manufLength;
    }

    message.Header.PayloadLen = (uint8_t) (payload - message.Payload);

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_SetConcurrentPeripheralScanResponse (
    BleConcurrentPeripheralScanResponse_t* bleScanResponse
) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;
    uint8_t* payload = NULL;
    uint8_t uuidCount;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_SCAN_RSP_SETUP_REQ;
    message.Header.PayloadID = 0;
    payload = message.Payload;

    *payload = (uint8_t) bleScanResponse->nameType;
    payload++;

    *payload = bleScanResponse->nameLength;
    payload++;

    if (bleScanResponse->nameLength) {
        memcpy (payload, bleScanResponse->name, sizeof (char) * bleScanResponse->nameLength);
        payload += sizeof (char) * bleScanResponse->nameLength;
    }

    *payload = bleScanResponse->uuidParam;
    payload++;

    if (bleScanResponse->uuidParam & 0x40) { // 128 bits (16 bytes)
        uuidCount = 16 * (bleScanResponse->uuidParam & 0x3F);
    } else {
        uuidCount = 3 * (bleScanResponse->uuidParam & 0x3F);
    }
    if (uuidCount) {
        memcpy (payload, bleScanResponse->uuids, sizeof (uint8_t) * uuidCount);
        payload += sizeof (uint8_t) * uuidCount;
    }

    *payload = bleScanResponse->manufLength;
    payload++;

    if (bleScanResponse->manufLength) {
        memcpy (payload, bleScanResponse->manuf, sizeof (uint8_t) * bleScanResponse->manufLength);
        payload += sizeof (uint8_t) * bleScanResponse->manufLength;
    }

    message.Header.PayloadLen = (uint8_t) (payload - message.Payload);

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_AddConcurrentPeripheralServiceCharacteristic (
    BleServiceCharacteristic_t* serviceCharacteristic
) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;
    uint8_t* payload = NULL;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_REQ;
    message.Header.PayloadID = 0;
    payload = message.Payload;

    memcpy (payload, &serviceCharacteristic->serviceUuid, sizeof (uint16_t));
    payload += sizeof (uint16_t);

    *payload = serviceCharacteristic->characteristicCount;
    payload++;

    if (serviceCharacteristic->characteristicCount > 8) {
        G_SYS_DBG_LOG_V( "Invalid characteristic count %u\r\n", serviceCharacteristic->characteristicCount);
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
        goto fail;
    }

    memcpy (payload, serviceCharacteristic->characteristics, serviceCharacteristic->characteristicCount * sizeof (BleCharacteristic_t));
    payload += serviceCharacteristic->characteristicCount * sizeof (BleCharacteristic_t);

    message.Header.PayloadLen = (uint8_t) (payload - message.Payload);

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

fail :

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_EnableConcurrentPeripheralSystemUtils (
    uint16_t* systemUtilsMask
) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;
    uint8_t* payload = NULL;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_SYSTEM_UTILS_ENABLE_MASK_REQ;
    message.Header.PayloadID = 0;
    payload = message.Payload;

    memcpy (payload, systemUtilsMask, sizeof (uint16_t));
    payload += sizeof (uint16_t);

    message.Header.PayloadLen = (uint8_t) (payload - message.Payload);

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StartConcurrentPeripheral (void) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_START_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = 0;

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_StopConcurrentPeripheral (void) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_STOP_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = 0;

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_DisconnectConcurrentPeripheral (void) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_DISCONNECT_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = 0;

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralDataOut (
    const uint8_t* DataTransferOutPayload,
    uint8_t len
) {
    BLEDeviceCtrl_result_t Result = DEV_CENTRAL_CTRL_SUCCESS;

    if ((!DataTransferOutPayload) || (len > DATA_TRANSFER_SIZE_PER_CHUNK)) {

        Result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;

    } else {

        BleBridgeCommMsg_t  SendMsg;
        BlePeripheralDataTransfer_t PeripheralDataOut;
        uint8_t* PayloadPtr = NULL;

        memcpy (PeripheralDataOut.Payload, DataTransferOutPayload, len);

        SendMsg.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
        SendMsg.Header.CommandAux = CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_OUT_REQ;
        SendMsg.Header.PayloadID = 0;
        SendMsg.Header.PayloadLen = sizeof (BlePeripheralDataTransfer_t);
        PayloadPtr = SendMsg.Payload;

        memcpy (PayloadPtr, &PeripheralDataOut, sizeof (BlePeripheralDataTransfer_t));

        Result = SendRequestToMessageQueue (&SendMsg);
    }


    return Result;
}


extern BLEDeviceCtrl_result_t BLEDevCentralCtrl_ConcurrentPeripheralFlush (void) {
    BLEDeviceCtrl_result_t result = DEV_CENTRAL_CTRL_SUCCESS;
    BleBridgeCommMsg_t message;

    message.Header.MessageType = MSG_TYPE_CONCURRENT_PERIPHERAL;
    message.Header.CommandAux = CONCURRENT_PERIPHERAL_AUX_TYPE_FLUSH_DATA_OUT_REQ;
    message.Header.PayloadID = 0;
    message.Header.PayloadLen = 0;

    if (message.Header.PayloadLen <= GATEWAY_MAXIMUM_PAYLOAD_SIZE) {
        result = SendRequestToMessageQueue (&message);
    } else {
        G_SYS_DBG_LOG_V( "Data out of range\r\n");
        result = DEV_CENTRAL_CTRL_INVALID_PARAM_ERROR;
    }

    return result;
}
