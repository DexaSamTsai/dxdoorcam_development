//============================================================================
// File: BLEDeviceInfoParser.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "dxBSP.h"
#include "BLEDeviceInfoParser.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/


/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *              LOCAL Function Definitions
 ******************************************************/


DeviceDataOut_t AdDataSearchForKey( uint8_t *pAdData, uint8_t AdDataLen, uint8_t AdSearchKey )
{
  DeviceDataOut_t SearchData;
  uint8_t adLen;
  uint8_t adType;
  uint8_t *pData;
  uint8_t *pEnd;

  SearchData.DataLen = 0;
  pData = pAdData;
  pEnd = pData + AdDataLen - 1;

  // While end of data not reached
  while ( pData < pEnd )
  {
    // Get length of next AD item
    adLen = *pData++;
    if ( adLen > 0 )
    {
      adType = *pData;

      // If AD type is what we are looking for
      if (adType == AdSearchKey)
      {
        pData++;
        adLen--;

        SearchData.DataLen = adLen;
        SearchData.pData = pData;

      }
      else
      {
        // Go to next item
        pData += adLen;
      }
    }
  }

  return SearchData;
}


/******************************************************
 *               Function Definitions
 ******************************************************/


extern uint8_t* BLEDevParser_GetDataWithDataTypeID(uint8_t DataTypeID, DkAdvData_t *DkAdvData)
{

    return DkPeripheral_GetDataWithDataTypeIDExt(DataTypeID, DkAdvData->AdDataLen, DkAdvData->AdData);

}


extern dxBOOL BLEDevParser_ReverseAddress(uint8_t *DeviceInfoPayload)
{
    dxBOOL Success = dxFALSE;

    if (DeviceInfoPayload)
    {
        DeviceInfo_t* pDeviceInfo = NULL;
        pDeviceInfo = (DeviceInfo_t*)DeviceInfoPayload;

        if ((pDeviceInfo->addrType == ADDRTYPE_PUBLIC) || (pDeviceInfo->addrType == ADDRTYPE_STATIC))
        {
            uint8_t AddrTemp[B_ADDR_LEN_8];

            memcpy(AddrTemp, pDeviceInfo->addr, B_ADDR_LEN_8);

            pDeviceInfo->addr[0] = AddrTemp[7];
            pDeviceInfo->addr[1] = AddrTemp[6];
            pDeviceInfo->addr[2] = AddrTemp[5];
            pDeviceInfo->addr[3] = AddrTemp[4];
            pDeviceInfo->addr[4] = AddrTemp[3];
            pDeviceInfo->addr[5] = AddrTemp[2];
            pDeviceInfo->addr[6] = AddrTemp[1];
            pDeviceInfo->addr[7] = AddrTemp[0];

            Success = dxTRUE;
        }
    }

    return Success;

}

DeviceAddress_t BLEDevParser_GetAddress(uint8_t *DeviceInfoPayload)
{
    DeviceAddress_t stDevAddress;
    stDevAddress.AddrLen = 0;

    if (DeviceInfoPayload)
    {
        //Since we are returning the address of a locally allocated DeviceInfo (.addr is an array not a pointer) we make sure to its static
        static DeviceInfo_t DeviceInfo;
        memcpy(&DeviceInfo, DeviceInfoPayload, sizeof(DeviceInfo_t));

        if ((DeviceInfo.addrType == ADDRTYPE_PUBLIC) || (DeviceInfo.addrType == ADDRTYPE_STATIC))
        {
            stDevAddress.AddressType = DeviceInfo.addrType;
            stDevAddress.AddrLen = B_ADDR_LEN_8;
            stDevAddress.pAddr = DeviceInfo.addr;
        }
    }

    return stDevAddress;
}

unsigned char BLEDevParser_IsAdvertisingDataScanRsp(uint8_t *DeviceInfoPayload)
{
    unsigned char AdvDataIsScanRsp = 0;

    DeviceInfo_t DeviceInfo;
    memcpy(&DeviceInfo, DeviceInfoPayload, sizeof(DeviceInfo_t));

    if (DeviceInfo.eventType == GAP_ADTYPE_SCAN_RSP_IND)
        AdvDataIsScanRsp = 1;

    return AdvDataIsScanRsp;
}

void BLEDevParser_GetDkAdvData (DkAdvData_t *pDkAdvData, DeviceManufaturerData_t *BroadcastManufactureData, const uint8_t* SecurityKey, uint8_t KeySize) {

    uint8_t*    pWorkingManufactureData;
    uint16_t    TotalDataTypeData = 0;
    dxAesContext_t   Aes128ctx = NULL;

    Aes128ctx =  dxAESSW_ContextAlloc();

    memset (pDkAdvData, 0, sizeof(DkAdvData_t));

    if ((Aes128ctx) && (BroadcastManufactureData->ManufacturerDataLen >= sizeof(DeviceDkPeripheralAdvHeader_t))) {

        pWorkingManufactureData = BroadcastManufactureData->pManufacturerData;

        memcpy(&(pDkAdvData->Header), BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));
        pWorkingManufactureData += sizeof(DeviceDkPeripheralAdvHeader_t);
        TotalDataTypeData = BroadcastManufactureData->ManufacturerDataLen - sizeof(DeviceDkPeripheralAdvHeader_t);

        if (DK_FLAG_IS_CONTAIN_EXT_FLAG(pDkAdvData->Header.Flag)) {
            pDkAdvData->ExtDkFlag = *pWorkingManufactureData;
            pWorkingManufactureData++;
            TotalDataTypeData--;
        } else {
            pDkAdvData->ExtDkFlag = 0; //MUST be cleared here, DO NOT memset pDkAdvData to zero in the biginning because it won't work as casting the header will overwrite this field again with unintended value
        }

        if (DK_FLAG_IS_CONTAIN_BATTERY_INFO(pDkAdvData->Header.Flag)) {
            pDkAdvData->BatteryInfo = *pWorkingManufactureData;
            pWorkingManufactureData++;
            TotalDataTypeData--;
        } else {
            pDkAdvData->BatteryInfo = 0; //This is needed here, same reason as above
        }

        if (DK_FLAG_IS_CONTAIN_PAGING_INFO(pDkAdvData->Header.Flag)) {
            pDkAdvData->PagingInfo = BUILD_UINT16(*(pWorkingManufactureData + 1), *pWorkingManufactureData);
            pWorkingManufactureData += 2;
            TotalDataTypeData -= 2;
        } else {
            pDkAdvData->PagingInfo = 0; //This is needed here, same reason as above
        }

        if (TotalDataTypeData > PERIPHERAL_DATA_TYPE_PLAYLOAD_SIZE) {

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "FATAL: Total data length %d overflow\r\n", TotalDataTypeData);

        } else {

            if (DK_FLAG_IS_PAYLOAD_ENCRYPTED(pDkAdvData->Header.Flag)) {
                unsigned char   AesDataOut[AES_ENC_DEC_BYTE];

                if (SecurityKey && KeySize) {
                    if (KeySize != AES_ENC_DEC_BYTE) {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unsupported KeySize(%d) pDkAdvData PID (0X%x)\r\n", KeySize, pDkAdvData->Header.ProductID );
                    } else {
                        dxAESSW_Setkey( Aes128ctx, SecurityKey, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AESSW Decrypt START\r\n");
                        dxAESSW_CryptEcb( Aes128ctx, DXAES_DECRYPT, pWorkingManufactureData, AesDataOut );
                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AESSW Decrypt END\r\n");
                        memcpy(pDkAdvData->AdData, AesDataOut, AES_ENC_DEC_BYTE);
                    }
                } else {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unable to decrypt (No Key) pDkAdvData PID (0X%x)\r\n", pDkAdvData->Header.ProductID );
                }

                if ((TotalDataTypeData - AES_ENC_DEC_BYTE) > 0) {
                    uint16_t UnEncryptedDataLen = TotalDataTypeData -AES_ENC_DEC_BYTE;
                    pWorkingManufactureData += AES_ENC_DEC_BYTE;
                    memcpy(&(pDkAdvData->AdData[AES_ENC_DEC_BYTE]), pWorkingManufactureData, UnEncryptedDataLen);
                }
            } else {
                memcpy(pDkAdvData->AdData, pWorkingManufactureData, TotalDataTypeData);
            }
        }

        pDkAdvData->AdDataLen = TotalDataTypeData;

    }

    if (Aes128ctx) {
        dxAESSW_ContextDealloc(Aes128ctx);
    }

    return;
}



uint32_t BLEDevParser_GetDkUserPassword(DkAdvData_t *DkAdvData)
{
    uint32_t UserPassword = 0; //0 as not found

    if (DkAdvData->AdDataLen)
    {
        uint8_t* pDataTypeData = BLEDevParser_GetDataWithDataTypeID(DK_DATA_TYPE_USERPASSCODE, DkAdvData);

        if (pDataTypeData)
        {
            UserPassword = BUILD_UINT32(*pDataTypeData, *(pDataTypeData + 1), *(pDataTypeData + 2), *(pDataTypeData + 3));
        }
    }

    return UserPassword;
}

ScanRspInfo_t BLEDevParser_GetScanRspInfo(uint8_t *DeviceInfoPayload)
{
    ScanRspInfo_t stScanRspInfo;

    memset(&stScanRspInfo, 0x00, sizeof(stScanRspInfo));

    DeviceInfo_t DeviceInfo;
    memcpy(&DeviceInfo, DeviceInfoPayload, sizeof(DeviceInfo_t));

    //We are going to check the device name in SCAN RSP only
    if (DeviceInfo.eventType == GAP_ADTYPE_SCAN_RSP_IND)
    {
        DeviceDataOut_t DataOut;
        uint8_t* pAdData = DeviceInfoPayload + sizeof(DeviceInfo_t);
        DataOut = AdDataSearchForKey( pAdData, DeviceInfo.dataLen, GAP_ADTYPE_LOCAL_NAME_COMPLETE);

        //If we can't find complete local name then lets try the short name
        if (DataOut.DataLen == 0)
        {
            DataOut = AdDataSearchForKey( pAdData, DeviceInfo.dataLen, GAP_ADTYPE_LOCAL_NAME_SHORT);
        }

        if (DataOut.DataLen)
        {
            stScanRspInfo.NameLen = DataOut.DataLen;
            stScanRspInfo.pName = DataOut.pData;
            stScanRspInfo.rssi = DeviceInfo.rssi;
        }
    }

    return stScanRspInfo;
}


unsigned char BLEDevParser_IsDeviceInOpenPairingMode(DeviceManufaturerData_t *BroadcastManufactureData)
{
    unsigned char DeviceInOpenPairingMode = 0;

    if (BroadcastManufactureData->ManufacturerDataLen)
    {
        DeviceDkPeripheralAdvHeader_t DkHeader;
        memcpy(&DkHeader, BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));

        if (DK_FLAG_IS_DEVICE_IN_GATEWAY_OPEN_PAIRING_MODE(DkHeader.Flag))
            DeviceInOpenPairingMode = 1;

    }

    return DeviceInOpenPairingMode;
}

unsigned char BLEDevParser_IsDeviceInStandAloneMode(DeviceManufaturerData_t *BroadcastManufactureData)
{
    unsigned char DeviceInStandAloneMode = 0;

    if (BroadcastManufactureData->ManufacturerDataLen)
    {
        DeviceDkPeripheralAdvHeader_t DkHeader;
        memcpy(&DkHeader, BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));

        if (DK_FLAG_IS_DEVICE_IN_STAND_ALONE_MODE(DkHeader.Flag))
            DeviceInStandAloneMode = 1;

    }

    return DeviceInStandAloneMode;
}


unsigned char BLEDevParser_IsDevicePaired(DeviceManufaturerData_t *BroadcastManufactureData)
{
    unsigned char DeviceInStandAloneMode = 0;

    if (BroadcastManufactureData->ManufacturerDataLen)
    {
        DeviceDkPeripheralAdvHeader_t DkHeader;
        memcpy(&DkHeader, BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));

        if (DK_FLAG_IS_DEVICE_PAIRED(DkHeader.Flag))
            DeviceInStandAloneMode = 1;

    }

    return DeviceInStandAloneMode;
}

dxBOOL BLEDevParser_IsDeviceDataEncrypted(DeviceManufaturerData_t *BroadcastManufactureData)
{

    dxBOOL Encrypted = dxFALSE;

    if (BroadcastManufactureData->ManufacturerDataLen)
    {
        DeviceDkPeripheralAdvHeader_t DkHeader;
        memcpy(&DkHeader, BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));

        if (DK_FLAG_IS_PAYLOAD_ENCRYPTED(DkHeader.Flag))
            Encrypted = dxTRUE;
    }


    return Encrypted;
}

unsigned char BLEDevParser_IsDevicePayloadDataValid(DeviceManufaturerData_t *BroadcastManufactureData)
{
    unsigned char AdPayloadDataIsValid = 0;
    DeviceDkPeripheralAdvHeader_t DkAdvDataHeader;
    uint8_t*    pWorkingManufactureData;
    uint16_t    TotalDataTypeData = 0;

    if (BroadcastManufactureData->ManufacturerDataLen  >= sizeof(DeviceDkPeripheralAdvHeader_t))
    {
        memcpy(&DkAdvDataHeader, BroadcastManufactureData->pManufacturerData, sizeof(DeviceDkPeripheralAdvHeader_t));
        pWorkingManufactureData = BroadcastManufactureData->pManufacturerData;
        pWorkingManufactureData += sizeof(DeviceDkPeripheralAdvHeader_t);
        TotalDataTypeData = BroadcastManufactureData->ManufacturerDataLen - sizeof(DeviceDkPeripheralAdvHeader_t);

        if (DK_FLAG_IS_CONTAIN_EXT_FLAG(DkAdvDataHeader.Flag))
        {
            pWorkingManufactureData++;
            TotalDataTypeData--;
        }

        if (DK_FLAG_IS_CONTAIN_BATTERY_INFO(DkAdvDataHeader.Flag))
        {
            pWorkingManufactureData++;
            TotalDataTypeData--;
        }

        if (DK_FLAG_IS_CONTAIN_PAGING_INFO(DkAdvDataHeader.Flag))
        {
            pWorkingManufactureData += 2;
            TotalDataTypeData -= 2;
        }

        uint8_t i = 0;
        uint8_t XorSum = *pWorkingManufactureData;
        pWorkingManufactureData += 1;
        TotalDataTypeData -= 1;

        for (i = 0; i < TotalDataTypeData; i++)
        {
            XorSum ^= *pWorkingManufactureData;
            pWorkingManufactureData += 1;
        }

        if (DkAdvDataHeader.XorPayloadChecksum == XorSum)
        {
            AdPayloadDataIsValid = 1;
        }

    }

    return AdPayloadDataIsValid;
}


DeviceManufaturerData_t BLEDevParser_GetManufacturerData(uint8_t *DeviceInfoPayload)
{
    DeviceManufaturerData_t stDevManufacturerData;
    stDevManufacturerData.ManufacturerDataLen = 0;

    DeviceInfo_t DeviceInfo;
    memcpy(&DeviceInfo, DeviceInfoPayload, sizeof(DeviceInfo_t));

    //We are going to check the device advertising data
    if (    (DeviceInfo.eventType == GAP_ADTYPE_ADV_IND) ||
            (DeviceInfo.eventType == GAP_ADTYPE_ADV_DIRECT_IND) ||
            (DeviceInfo.eventType == GAP_ADTYPE_ADV_DISCOVER_IND) ||
            (DeviceInfo.eventType == GAP_ADTYPE_ADV_NONCONN_IND) )
    {
        DeviceDataOut_t DataOut;
        uint8_t* pAdData = DeviceInfoPayload + sizeof(DeviceInfo_t);
        DataOut = AdDataSearchForKey( pAdData, DeviceInfo.dataLen, GAP_ADTYPE_MANUFACTURER_SPECIFIC);

        if (DataOut.DataLen)
        {
            stDevManufacturerData.AdvertisingType = DeviceInfo.eventType;
            stDevManufacturerData.ManufacturerDataLen = DataOut.DataLen;
            stDevManufacturerData.pManufacturerData = DataOut.pData;
            stDevManufacturerData.rssi = DeviceInfo.rssi;
        }
    }

    return stDevManufacturerData;
}

