//============================================================================
// File: BLEDeviceBridgeComm.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "dxBSP.h"
#include "BLEDeviceBridgeComm.h"
#include "BLEDeviceKeeper.h"
#include "dk_Peripheral.h"
#include "Utils.h"
#include "ProjectCommonConfig.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#ifndef BT_BUS_RX_FIFO_SIZE
#define BT_BUS_RX_FIFO_SIZE (256)
#endif


// Driver thread priority is set to 1 higher than BT transport thread
#define BT_UART_THREAD_PRIORITY         DXTASK_PRIORITY_EXTREAMLY_HIGH
#define BT_UART_THREAD_NAME             "BT UART READ"
#define BT_UART_STACK_SIZE              512

#define BT_UART_WRITE_THREAD_PRIORITY   DXTASK_PRIORITY_HIGH
#define BT_UART_WRITE_THREAD_NAME       "BT UART WRITE"
#define BT_UART_WRITE_STACK_SIZE        768


// LOOK UP TABLE for CRC16 generation
// Polynomial X^16+X^15+X^2+1
const static uint16_t CRC16Lut[] =
{
    0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
    0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
    0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
    0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
    0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
    0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
    0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
    0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
    0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
    0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
    0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
    0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
    0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
    0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
    0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
    0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
    0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
    0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
    0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
    0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
    0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
    0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
    0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
    0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
    0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
    0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
    0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
    0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
    0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
    0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
    0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
    0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
};

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/* RX ring buffer. Bluetooth chip UART receive can be asynchronous, therefore a ring buffer is required */

static volatile uint8_t     rx_data[BT_BUS_RX_FIFO_SIZE];

static dxTaskHandle_t       uart_read_thread;
static dxTaskHandle_t       uart_write_thread;

static dxQueueHandle_t      BridgeCommRspMsgQueue;
static dxQueueHandle_t      BridgeCommSendMsgQueue;

dxUART_t*                   pdxUartObj = NULL;



/******************************************************
 *              LOCAL Function Definitions
 ******************************************************/

uint16_t crc16_8005_calc(const uint8_t *data_in, uint32_t data_len, uint16_t init_val)
{
    uint32_t    i;
    uint16_t crc_sum;


    crc_sum = init_val;
    for(i = 0; i < data_len; i++)
    {
        crc_sum = (crc_sum<<8)^CRC16Lut[((crc_sum>>8)&0xFF) ^ data_in[i]];
    }

    return crc_sum;
}

void BLEDevCentralCtrl_TryRecoverUART(void);
static TASK_RET_TYPE bt_transport_driver_uart_read_thread_main( TASK_PARAM_TYPE arg )
{
    uint8_t dma_error_detect_counter = 0;

    while ( 1 )
    {
        uint8_t Preamble = 0;
        BleGatewayHeader_t Header;

        if (dxUART_Receive_Blocked( pdxUartObj, rx_data, 1, DXOS_WAIT_FOREVER ) > 0)
        {
            Preamble = rx_data[0];
            if (Preamble == GATEWAY_HEADER_PREAMBLE)
            {
                dma_error_detect_counter = 0;
                if (dxUART_Receive_Blocked( pdxUartObj, rx_data, sizeof(BleGatewayHeader_t), 1000 ) > 0)
                {
                    memcpy(&Header, rx_data, sizeof(BleGatewayHeader_t));

#if ENABLE_PAYLOAD_ID_AS_SEQUENCIAL_MSG_DEBUG
                    static uint8_t SeqDbgPayloadID = 0;
                    if (Header.PayloadID == 0)
                    {
                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DGB: Seq PayloadID 0 detected\r\n");
                        SeqDbgPayloadID = 0;
                    }
                    else
                    {
                        if (Header.PayloadID == (SeqDbgPayloadID+1))
                        {
                            SeqDbgPayloadID++;
                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DGB: Seq PayloadID non-Seq detected (Exp=%d, Got=%d)\r\n", SeqDbgPayloadID+1, Header.PayloadID);
                            SeqDbgPayloadID = Header.PayloadID;
                        }
                    }
#endif
                    if (((Header.MessageType > 0) && (Header.MessageType < MSG_TYPE_MAX)))
                    {
                        switch (Header.MessageType)
                        {
                            case MSG_TYPE_LOG:
                            {

#if ENABLE_BLE_MEMORY_LOG
                                if ((Header.CommandAux == LOG_AUX_TYPE_MEMORY_METRICS_DEBUG) && (Header.PayloadLen))
                                {
                                    OSALMemoryMetric_t MemoryDebug;
                                    uint32_t    MemoryDebugDataTotalSize = Header.PayloadLen + GATEWAY_HEADER_PAYLOAD_CRC_SIZE;
                                    if (dxUART_Receive_Blocked( pdxUartObj, (uint8_t*)&MemoryDebug, MemoryDebugDataTotalSize, 1000 ) > 0)
                                    {
                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "[BLE MEM LOG] = CurrentAlloc = %d  TotalAlloc(Max) = %d\r\n", MemoryDebug.CurrentMemoryAlloc, MemoryDebug.MaxMemmoryAlloc );
                                    }
                                    else
                                    {
                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error reading Payload (Could be due to timeout) \r\n");
                                    }

                                }
                                else if ((Header.CommandAux == LOG_AUX_TYPE_STRING) && (Header.PayloadLen))
                                {
                                    char LogString[50];
                                    uint16_t    LogStringSize = sizeof(LogString);
                                    uint32_t    LogDebugDataTotalSize = ((Header.PayloadLen + GATEWAY_HEADER_PAYLOAD_CRC_SIZE) > LogStringSize) ? (LogStringSize - 1) : (Header.PayloadLen + GATEWAY_HEADER_PAYLOAD_CRC_SIZE);

                                    LogString[LogStringSize-1] = '\0'; //Always Pad NULL at the end of string

                                    if (dxUART_Receive_Blocked( pdxUartObj, (uint8_t*)&LogString, LogDebugDataTotalSize, 1000 ) > 0)
                                    {
                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "[BLE STRING LOG] = %s\r\n", LogString );
                                    }
                                    else
                                    {
                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error reading Log Payload (Could be due to timeout) \r\n");
                                    }

                                }


#endif

                            }
                            break;


                            default:
                            {

                                dxBOOL      RspMsgReadyToPush = dxTRUE;
                                BleBridgeCommMsg_t* ResponseMsg = dxMemAlloc( "BridgeComm Rsp Msg Buff", 1, sizeof(BleBridgeCommMsg_t));

                                if (ResponseMsg)
                                {
                                    memcpy(&ResponseMsg->Header, &Header, sizeof(BleGatewayHeader_t));
                                    if (Header.PayloadLen)
                                    {
                                        uint32_t    DeviceInfoTotalSize = Header.PayloadLen + GATEWAY_HEADER_PAYLOAD_CRC_SIZE;
                                        if (DeviceInfoTotalSize <= GATEWAY_MAXIMUM_PAYLOAD_SIZE)
                                        {

                                            if (dxUART_Receive_Blocked( pdxUartObj, ResponseMsg->Payload, DeviceInfoTotalSize, 1000 ) <= 0)
                                            {
                                                RspMsgReadyToPush = dxFALSE;
                                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error reading Payload (Could be due to timeout) \r\n" );
                                                dxMemFree(ResponseMsg);
                                            }
                                            else
                                            {
#if 0
                                                uint16_t crc_cal, crc;
                                                crc = ResponseMsg->Payload[Header.PayloadLen] | (ResponseMsg->Payload[Header.PayloadLen + 1] << 8);
                                                if ((crc_cal = crc16_8005_calc(ResponseMsg->Payload, Header.PayloadLen, 0)) != crc)
                                                {
                                                    RspMsgReadyToPush = dxFALSE;
                                                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Payload CRC Error. 0x%x != 0x%x\r\n", crc,  crc_cal);
                                                    dxMemFree(ResponseMsg);
                                                }
#endif
                                            }
                                        }
                                        else
                                        {

                                            RspMsgReadyToPush = dxFALSE;
                                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Message payload (s=%d, MsgType=%d) exceeding maximum allowed size\r\n", DeviceInfoTotalSize, Header.MessageType);
                                            dxMemFree(ResponseMsg);
                                        }
                                    }

                                    if ((BridgeCommRspMsgQueue) && (RspMsgReadyToPush == dxTRUE))
                                    {

                                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d), BLERcv MsgType=%d, CmmAux=%d\r\n", __LINE__, Header.MessageType, Header.CommandAux );

                                        if (dxQueueIsFull(BridgeCommRspMsgQueue) == dxTRUE)
                                        {
                                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                                            dxMemFree(ResponseMsg);
                                        }
                                        else
                                        {
                                            if (dxQueueSend( BridgeCommRspMsgQueue, &ResponseMsg, 1000 ) != DXOS_SUCCESS)
                                            {
                                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                                                dxMemFree(ResponseMsg);
                                            }
                                        }

                                    }
                                }
                                else
                                {
                                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error unable to push received message (Out of system Memory)\r\n");
                                }

                            }
                                break;
                        }
                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BleComm receive unknown Message Type (%d)\r\n", Header.MessageType);
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error reading Header (Could be due to timeout) \r\n");
                }

            }
            else if (Preamble == UART_REPORT_ERROR_PATTERN_DMA_ERROR)
            {
                if (++dma_error_detect_counter > 3) //Couple of more continuos detection to prevent false alarm
                {
                    dma_error_detect_counter = 0;
                    BLEDevCentralCtrl_TryRecoverUART();
                }
            }

            //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "UART Read Byte Data = 0X%x (Decimal = %d)\r\n", Preamble, Preamble);
        }
        else
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error reading UART \r\n");
        }

    }

    END_OF_TASK_FUNCTION;
}


static TASK_RET_TYPE bt_transport_driver_uart_write_thread_main( TASK_PARAM_TYPE arg )
{

    BleBridgeCommMsg_t* pSendMsg;
    dxOS_RET_CODE PopResult;

    while (1)
    {
        pSendMsg = NULL;
        PopResult = dxQueueReceive( BridgeCommSendMsgQueue, &pSendMsg, DXOS_WAIT_FOREVER );

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "uart_write_thread_main MSG Popped!\r\n" );

        if (PopResult == DXOS_SUCCESS)
        {
            uint16_t TotalSizeToSend = GATEWAY_HEADER_PREAMBLE_SIZE + sizeof(BleGatewayHeader_t) + pSendMsg->Header.PayloadLen;
            if (pSendMsg->Header.PayloadLen)
            {
                TotalSizeToSend += GATEWAY_HEADER_PAYLOAD_CRC_SIZE;
            }

            uint8_t     Preamble = GATEWAY_HEADER_PREAMBLE;
            uint8_t*    BCMessageSend = dxMemAlloc( "BridgeComm Send Msg Buff", 1, TotalSizeToSend);
            uint8_t*    BCMessageSendPtr = BCMessageSend;

            if (BCMessageSend)
            {
                memcpy(BCMessageSendPtr, &Preamble, GATEWAY_HEADER_PREAMBLE_SIZE); BCMessageSendPtr += GATEWAY_HEADER_PREAMBLE_SIZE;
                memcpy(BCMessageSendPtr, &pSendMsg->Header, sizeof(BleGatewayHeader_t)); BCMessageSendPtr += sizeof(BleGatewayHeader_t);

                if (pSendMsg->Header.PayloadLen)
                {
                    memcpy(BCMessageSendPtr, pSendMsg->Payload, pSendMsg->Header.PayloadLen); BCMessageSendPtr += pSendMsg->Header.PayloadLen;

                    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d),BLESend MsgType=%d, CmmAux=%d\r\n", pSendMsg->Header.MessageType, pSendMsg->Header.CommandAux );

#if PAYLOAD_CRC_CALCULATE
                    uint16_t CalcCrcRes = crc16_8005_calc(pSendMsg->Payload, pSendMsg->Header.PayloadLen, 0);

                    memcpy(BCMessageSendPtr, &CalcCrcRes, GATEWAY_HEADER_PAYLOAD_CRC_SIZE); BCMessageSendPtr += GATEWAY_HEADER_PAYLOAD_CRC_SIZE;

                    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "CRC16 = %d\r\n", CalcCrcRes);
#endif

                }


#if 0
#pragma message ("WARNING: BLE UART RAW DATA OUT BUMP ENABLED - NOT FOR PRODUCTION!!" )

                if (pSendMsg->Header.CommandAux == SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_OUT_REQ) {
                    GSysDebugger_Dump ("PERIPHERAL_ROLE Send", (int) TotalSizeToSend, BCMessageSend);
                } else if (pSendMsg->Header.CommandAux == CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_OUT_REQ) {
                    GSysDebugger_Dump ("CONCURRENT_PERIPHERAL_DATA_OUT Send", (int) TotalSizeToSend, BCMessageSend);
                }

#endif

                if (dxUART_Send_Blocked(pdxUartObj, BCMessageSend, TotalSizeToSend) <= 0)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BridgeComm Send MSG Failed!!!\r\n" );
                }

                dxMemFree(BCMessageSend);

            }
            else
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Fail to create buffer for BCMessageSend\r\n");
            }
        }

        if (pSendMsg)
            dxMemFree(pSendMsg);

    }

    END_OF_TASK_FUNCTION;
}

/******************************************************
 *               Function Definitions
 ******************************************************/


BLEDeviceBridgeComm_result_t BLEDevBridgeComm_Init(void)
{
    BLEDeviceBridgeComm_result_t BCResult = DEV_BRIDGECOMM_SUCCESS;

    dxOS_RET_CODE result;

    BridgeCommRspMsgQueue = NULL;

    //UART Configuration
    {
        /* Initialise RX ring buffer */

        /* Configure USART comms */

        pdxUartObj = dxUART_Open(BLE_BRIDGE_UART_PORT,
                                 BLE_BRIDGE_UART_BAUD_RATE,
                                 BLE_BRIDGE_UART_PARITY_BIT,
                                 BLE_BRIDGE_UART_DATA_WIDTH,
                                 BLE_BRIDGE_UART_STOP_BITS,
                                 BLE_BRIDGE_UART_FLOW_CONTROL,
                                 NULL);
    }

    if (BridgeCommRspMsgQueue == NULL)
    {

        BridgeCommRspMsgQueue = dxQueueCreate(MAXIMUM_RSP_MESSAGE_QUEUE_DEPT_ALLOWED, sizeof(BleBridgeCommMsg_t*));
        if (BridgeCommRspMsgQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BridgeCommRspMsgQueue \r\n");
        }

    }

    if (BridgeCommSendMsgQueue == NULL)
    {

        BridgeCommSendMsgQueue = dxQueueCreate(MAXIMUM_SEND_MESSAGE_QUEUE_DEPT_ALLOWED, sizeof(BleBridgeCommMsg_t*));
        if (BridgeCommSendMsgQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BridgeCommRspMsgQueue \r\n" );
        }

    }

    result = dxTaskCreate(bt_transport_driver_uart_read_thread_main,
                          BT_UART_THREAD_NAME,
                          BT_UART_STACK_SIZE,
                          0,
                          BT_UART_THREAD_PRIORITY,
                          &uart_read_thread);
    if (result != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating MPAF UART driver thread (Error Code = %d)\r\n", result );
        BCResult = DEV_BRIDGECOMM_RTOS_ERROR;
    }
    else
    {
        GSysDebugger_RegisterForThreadDiagnostic(&uart_read_thread);
    }

    result = dxTaskCreate(bt_transport_driver_uart_write_thread_main,
                          BT_UART_WRITE_THREAD_NAME,
                          BT_UART_WRITE_STACK_SIZE,
                          0,
                          BT_UART_WRITE_THREAD_PRIORITY,
                          &uart_write_thread);
    if (result != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating MPAF UART driver write thread (Error Code = %d)\r\n", result );
        BCResult = DEV_BRIDGECOMM_RTOS_ERROR;
    }
    else
    {
        GSysDebugger_RegisterForThreadDiagnostic(&uart_write_thread);
    }


    return BCResult;

}


dxQueueHandle_t* BLEDevBridgeComm_GetResponseMsgQueueHdl(void)
{

    if (BridgeCommRspMsgQueue)
    {
        return &BridgeCommRspMsgQueue;
    }

    return NULL;
}

dxQueueHandle_t* BLEDevBridgeComm_GetSendMsgQueueHdl(void)
{

    if (BridgeCommSendMsgQueue)
    {
        return &BridgeCommSendMsgQueue;
    }

    return NULL;

}


void BLEDevBridgeComm_ResetMsgQueue (void) {
    while (dxQueueIsEmpty(BridgeCommRspMsgQueue) != dxTRUE) {
        BleBridgeCommMsg_t* message = NULL;
        dxQueueReceive( BridgeCommRspMsgQueue, &message, DXOS_WAIT_FOREVER );
        if (message) {
            dxMemFree(message);
        }
    }

    while (dxQueueIsEmpty(BridgeCommSendMsgQueue) != dxTRUE) {
        BleBridgeCommMsg_t* message = NULL;
        dxQueueReceive( BridgeCommSendMsgQueue, &message, DXOS_WAIT_FOREVER );
        if (message) {
            dxMemFree(message);
        }
    }
}


BLEDeviceBridgeComm_result_t BLEDevBridgeComm_Build128UUID(uint8_t* UUID128Out, uint16_t UUID16In)
{
    BLEDeviceBridgeComm_result_t BCResult = DEV_BRIDGECOMM_SUCCESS;

    if (UUID128Out)
    {
        memcpy(UUID128Out, DkBaseUUID, UUID_128_SIZE);
        UUID128Out[12] = LO_UINT16(UUID16In);
        UUID128Out[13] = HI_UINT16(UUID16In);

    }
    else
    {
        BCResult = DEV_BRIDGECOMM_PARAM_ERROR;
    }

    return BCResult;

}
