//============================================================================
// File: BLEDeviceInfoParser.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "BLEDeviceBridgeComm.h"
#include "dk_Peripheral.h"
#include "dxSysDebugger.h"

/******************************************************
 *                      Macros
 ******************************************************/

/** @} End DK_ADV_FLAG_MACROS */


#define BUILD_UINT16(loByte, hiByte) \
          ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))

#define BUILD_UINT32(Byte0, Byte1, Byte2, Byte3) \
          ((uint32_t)((uint32_t)((Byte0) & 0x00FF) \
          + ((uint32_t)((Byte1) & 0x00FF) << 8) \
          + ((uint32_t)((Byte2) & 0x00FF) << 16) \
          + ((uint32_t)((Byte3) & 0x00FF) << 24)))

/******************************************************
 *                    Constants
 ******************************************************/


/** @defgroup GAP_ADVERTISEMENT_TYPE_DEFINES GAP Advertiser Event Types
 * for eventType field in gapAdvertisingParams_t, gapDevRec_t and gapDeviceInfoEvent_t
 * @{
 */
#define GAP_ADTYPE_ADV_IND                0x00  //!< Connectable undirected advertisement       //Aaron - SCAN_REQ->YES CONNECT_REQ->YES
#define GAP_ADTYPE_ADV_DIRECT_IND         0x01  //!< Connectable directed advertisement         //Aaron - SCAN_REQ->NO CONNECT_REQ->YES to specific address
#define GAP_ADTYPE_ADV_DISCOVER_IND       0x02  //!< Discoverable undirected advertisement      //Aaron - SCAN_REQ->NO CONNECT_REQ->NO
#define GAP_ADTYPE_ADV_NONCONN_IND        0x03  //!< Non-Connectable undirected advertisement   //Aaron - SCAN_REQ->YES CONNECT_REQ->NO
#define GAP_ADTYPE_SCAN_RSP_IND           0x04  //!< Only used in gapDeviceInfoEvent_t
/** @} End GAP_ADVERTISEMENT_TYPE_DEFINES */

/** @defgroup GAP_ADDR_TYPE_DEFINES GAP Address Types
 * @{
 */
#define ADDRTYPE_PUBLIC               0x00  //!< Use the BD_ADDR
#define ADDRTYPE_STATIC               0x01  //!< Static address
#define ADDRTYPE_PRIVATE_NONRESOLVE   0x02  //!< Generate Non-Resolvable Private Address
#define ADDRTYPE_PRIVATE_RESOLVE      0x03  //!< Generate Resolvable Private Address
#define ADDRTYPE_DONTCARE             0xFF  //!< Don't care address type
/** @} End GAP_ADDR_TYPE_DEFINES */


/** @defgroup GAP_ADTYPE_DEFINES GAP Advertisment Data Types
 * These are the data type identifiers for the data tokens in the advertisement data field.
 * @{
 */
#define GAP_ADTYPE_FLAGS                        0x01 //!< Discovery Mode: @ref GAP_ADTYPE_FLAGS_MODES
#define GAP_ADTYPE_16BIT_MORE                   0x02 //!< Service: More 16-bit UUIDs available
#define GAP_ADTYPE_16BIT_COMPLETE               0x03 //!< Service: Complete list of 16-bit UUIDs
#define GAP_ADTYPE_32BIT_MORE                   0x04 //!< Service: More 32-bit UUIDs available
#define GAP_ADTYPE_32BIT_COMPLETE               0x05 //!< Service: Complete list of 32-bit UUIDs
#define GAP_ADTYPE_128BIT_MORE                  0x06 //!< Service: More 128-bit UUIDs available
#define GAP_ADTYPE_128BIT_COMPLETE              0x07 //!< Service: Complete list of 128-bit UUIDs
#define GAP_ADTYPE_LOCAL_NAME_SHORT             0x08 //!< Shortened local name
#define GAP_ADTYPE_LOCAL_NAME_COMPLETE          0x09 //!< Complete local name
#define GAP_ADTYPE_POWER_LEVEL                  0x0A //!< TX Power Level: 0xXX: -127 to +127 dBm
#define GAP_ADTYPE_OOB_CLASS_OF_DEVICE          0x0D //!< Simple Pairing OOB Tag: Class of device (3 octets)
#define GAP_ADTYPE_OOB_SIMPLE_PAIRING_HASHC     0x0E //!< Simple Pairing OOB Tag: Simple Pairing Hash C (16 octets)
#define GAP_ADTYPE_OOB_SIMPLE_PAIRING_RANDR     0x0F //!< Simple Pairing OOB Tag: Simple Pairing Randomizer R (16 octets)
#define GAP_ADTYPE_SM_TK                        0x10 //!< Security Manager TK Value
#define GAP_ADTYPE_SM_OOB_FLAG                  0x11 //!< Secutiry Manager OOB Flags
#define GAP_ADTYPE_SLAVE_CONN_INTERVAL_RANGE    0x12 //!< Min and Max values of the connection interval (2 octets Min, 2 octets Max) (0xFFFF indicates no conn interval min or max)
#define GAP_ADTYPE_SIGNED_DATA                  0x13 //!< Signed Data field
#define GAP_ADTYPE_SERVICES_LIST_16BIT          0x14 //!< Service Solicitation: list of 16-bit Service UUIDs
#define GAP_ADTYPE_SERVICES_LIST_128BIT         0x15 //!< Service Solicitation: list of 128-bit Service UUIDs
#define GAP_ADTYPE_SERVICE_DATA                 0x16 //!< Service Data
#define GAP_ADTYPE_APPEARANCE                   0x19 //!< Appearance
#define GAP_ADTYPE_MANUFACTURER_SPECIFIC        0xFF //!< Manufacturer Specific Data: first 2 octets contain the Company Identifier Code followed by the additional manufacturer specific data
/** @} End GAP_ADTYPE_DEFINES */


/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

//Make sure this typedef structure is 16bit (2bytes) aligned!!!
typedef struct
{
    uint8_t eventType;          //!< Advertisement Type: @ref GAP_ADVERTISEMENT_TYPE_DEFINES
    uint8_t addrType;           //!< address type: @ref GAP_ADDR_TYPE_DEFINES
    uint8_t addr[B_ADDR_LEN_8];   //!< Address of the advertisement or SCAN_RSP
    int8_t  rssi;                //!< Advertisement or SCAN_RSP RSSI
    uint8_t dataLen;            //!< Length (in bytes) of the data field (evtData)
} DeviceInfo_t;

typedef struct
{
    uint8_t     DataLen;
    uint8_t*    pData;

} DeviceDataOut_t;


typedef struct
{
    uint16_t    AddressType;    //!< address type: @ref GAP_ADDR_TYPE_DEFINES
    uint8_t     AddrLen;
    uint8_t*    pAddr;

} DeviceAddress_t;

typedef struct
{
    uint8_t     NameLen;
    uint8_t*    pName;
    int8_t      rssi;
} ScanRspInfo_t;

typedef struct
{
    uint8_t     AdvertisingType;        //!< Advertisement Type: @ref GAP_ADVERTISEMENT_TYPE_DEFINES
    uint8_t     ManufacturerDataLen;
    uint8_t*    pManufacturerData;
    int8_t      rssi;

} DeviceManufaturerData_t;


/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

extern dxBOOL BLEDevParser_ReverseAddress(uint8_t *DeviceInfoPayload);

extern DeviceAddress_t BLEDevParser_GetAddress(uint8_t *DeviceInfoPayload);

extern ScanRspInfo_t BLEDevParser_GetScanRspInfo(uint8_t *DeviceInfoPayload);

extern unsigned char BLEDevParser_IsAdvertisingDataScanRsp(uint8_t *DeviceInfoPayload);

extern unsigned char BLEDevParser_IsDeviceInOpenPairingMode(DeviceManufaturerData_t *BroadcastManufactureData);

extern unsigned char BLEDevParser_IsDeviceInStandAloneMode(DeviceManufaturerData_t *BroadcastManufactureData);

unsigned char BLEDevParser_IsDevicePaired(DeviceManufaturerData_t *BroadcastManufactureData);

extern dxBOOL BLEDevParser_IsDeviceDataEncrypted(DeviceManufaturerData_t *BroadcastManufactureData);

extern unsigned char BLEDevParser_IsDevicePayloadDataValid(DeviceManufaturerData_t *BroadcastManufactureData);

extern void BLEDevParser_GetDkAdvData(DkAdvData_t *pDkAdvData, DeviceManufaturerData_t *BroadcastManufactureData, const uint8_t* SecurityKey, uint8_t KeySize);

extern uint8_t* BLEDevParser_GetDataWithDataTypeID(uint8_t DataTypeID, DkAdvData_t *DkAdvData);

extern uint32_t BLEDevParser_GetDkUserPassword(DkAdvData_t *DkAdvData);

extern DeviceManufaturerData_t BLEDevParser_GetManufacturerData(uint8_t *DeviceInfoPayload);




