//============================================================================
// File: BLeManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "dxBSP.h"

#include "BLEManager.h"
#include "BLEDeviceCentralControl.h"
#include "BLEDeviceBridgeComm.h"
#include "Utils.h"
#include "PolarSSL_md2.h"
#include "dk_PeripheralAux.h"
#include "BLERepeater.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define AUTO_RESCAN_PROCESS_ENABLE                              1                   //Auto rescan when maximum device reported from scan iteration is reached. This is to overcome TI2541 scan capability issue.

#define DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX                  3
#define DEFAULT_ON_JOB_DISCONNECTION_RETRY_LOW                  2
#define DEFAULT_ON_JOB_DISCONNECTION_RETRY_ONCE                 1

#define DEVICE_CHAR_DISCV_QUEUE_REPORT_WAIT_TIME_OUT_MS         5000 //4000
#define DEVICE_READ_WRITE_CHAR_QUEUE_REPORT_WAIT_TIME_OUT_MS    1000 //4000
#define DEVICE_GENERIC_QUEUE_REPORT_WAIT_TIME_OUT_MS            1000
#define IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT       100

#define ADDITIANAL_JOB_TIMEOUT_DELAY_PER_CHARACTERISTIC_DISCOVERY 1500

#define CENTRAL_STATUS_CHECK_SESSION_TIME_INTERVAL              (4*SECONDS)     //(10*SECONDS)        //(2*SECONDS) //Try not to go lower than 10 seconds unless we are sure the reason.

#define MAXIMUM_DEVICE_JOB_QUEUE_DEPT_ALLOWED                       40
#define MAXIMUM_DEVICE_IN_JOB_REPORT_QUEUE_DEPT_ALLOWED             5
#define MAXIMUM_DEVICE_CHAR_DISCOVERY_REPORT_QUEUE_DEPT_ALLOWED     1
#define MAXIMUM_DEVICE_READ_WRITE_CHAR_REPORT_QUEUE_DEPT_ALLOWED    1
#define MAXIMUM_DEVICE_CENTRAL_STATE_REPORT_QUEUE_DEPT_ALLOWED      1
#define MAXIMUM_UNPAIR_DEVICE_REPORT_QUEUE_DEPT_ALLOWED             10
#define MAXIMUM_PERIPHERAL_DATA_TRANSFER_REPORT_QUEUE_DEPT_ALLOWED  20
#define MAXIMUM_CONCURRENT_PERIPHERAL_DATA_OUT_RETRY_COUNT          2
#define MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE                     10


#define MAXIMUM_DEVICE_JOB_DATA_SIZE                        256


#define BLE_MANAGER_PROCESS_THREAD_PRIORITY             DXTASK_PRIORITY_HIGH
#define BLE_MANAGER_ACCESS_PROCESS_THREAD_PRIORITY      DXTASK_PRIORITY_VERY_HIGH
#define BLE_MANAGER_PROCESS_THREAD_NAME                 "BleMan.Process"
#define BLE_MANAGER_STACK_SIZE                          1920

#define BRIDGECOMM_PROCESS_THREAD_PRIORITY  DXTASK_PRIORITY_VERY_HIGH
#define BRIDGECOMM_PROCESS_THREAD_NAME      "BridgeComm Process"
#define BRIDGECOMM_STACK_SIZE               1408

#define BLE_MANAGER_UNPAIR_DEVICE_REPORT_THREAD_PRIORITY    DX_DEFAULT_WORKER_PRIORITY
#define BLE_MANAGER_UNPAIR_DEVICE_REPORT_THREAD_NAME        "BleMan.UnPairDeviceReport"
#define BLE_MANAGER_UNPAIR_DEVICE_REPORT_STACK_SIZE         384

#define BLE_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE           1536
#define BLE_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE      (MAXIMUM_DEVICE_KEEPER_ALLOWED*2)



/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    DEV_MANAGER_NO_JOB                = 0,
    DEV_MANAGER_JOB_PAIR_DEVICE,
    DEV_MANAGER_JOB_ADD_DEVICE,
    DEV_MANAGER_JOB_UNPAIR_DEVICE,
    DEV_MANAGER_JOB_CLEANUP_ALL_DEVICE_FROMKEEPER_LIST,
    DEV_MANAGER_JOB_GET_DEVICE_KEEPER_LIST,
    DEV_MANAGER_JOB_ACCESS_WRITE_DEVICE,
    DEV_MANAGER_JOB_ACCESS_WRITE_FOLLOW_BY_ADV_READ_UPDATE_DEVICE,
    DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_AGGREGATION_UPDATE,
    DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_DATA_REPORT,
    DEV_MANAGER_JOB_PERIPHERAL_DATA_OUT_WRITE,
    DEV_MANAGER_JOB_SET_RENEW_DEVICE_SECURITY,
    DEV_MANAGER_JOB_CLEAR_DEVICE_SECURITY,
    DEV_MANAGER_JOB_FW_BLOCK_WRTE,
    DEV_MANAGER_JOB_FW_UPDATE_DONE,
    DEV_MANAGER_JOB_UPDATE_RTC,

    DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS,
    DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_DATA_OUT,

    DEV_MANAGER_JOB_SETUP_REPEATER,
    DEV_MANAGER_JOB_MONITOR_REPEATER

} BLEManager_Job_t;




/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{

    BLEManager_Job_t    JobType;
    uint16_t            JobSourceOfOrigin;
    uint64_t            JobIdentificationNumber;
    dxTime_t            JobTimeOutTime;
    uint8_t             JobDisconnectMaxRetry;
    uint8_t             JobData[MAXIMUM_DEVICE_JOB_DATA_SIZE];

} BleDeviceJobQueue_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data


typedef struct
{
    uint16_t    ErrorCode;                  //!< Non Zero if error (Srv_Discovery_Error_t)
    uint16_t    DiscoveryIdHandShake;       //!< A unique ID provided from request side, this same ID will be returned on the response message
    uint16_t    CharHandlerID;              //!< Found characteristic handler, use this handler id to send read/write BLE command. Should be NON Zero

} BleDeviceCharDiscoveryRspMsg_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data



/******************************************************
 *                 Type Definitions
 ******************************************************/


/******************************************************
 *               Function Declarations
 ******************************************************/

/*---- In Job Function -----------------------------------------------------*/

static BLEManager_GenJobProcess_t InJobStart( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobFindDevice( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobStartScanDevice( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSetCentralToIdle( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobCentralConnectToDevice( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobCentralDisconnect( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSecurityClearance( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSecurityClearanceGivenKey( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSecurityKeyClear( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSecurityKeyExchange( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobSecurityKeyExchangeAndSaveForReport( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobDataTypePayloadRead( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobGenericCharacteristicWrite( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobGenericCharacteristicRead( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobGenericCharacteristicReadAggregatedUpdate( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobFwVersionRead( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobAddPeripheralDevicesToKeeperList( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobRemovePeripheralDevicesFromKeeperList( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobRemoveALLPeripheralDevicesFromKeeperList( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobGetDeviceKeeperList( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobPeripheralTransferDataOut( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobCleanUpAsCentral (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCleanUpAsRepeater (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCleanUpAsPeripheral (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobFwBlockWrite( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobFwDoneBootToApp( instance_data_t *data );
static BLEManager_GenJobProcess_t InJobUpdateRtc( instance_data_t *data );

static BLEManager_GenJobProcess_t InJobConcurrentPeripheralCustomizeBdAddress (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobConcurrentPeripheralDataOut (instance_data_t* data);


static BLEManager_GenJobProcess_t InJobCentralConnectToRepeater (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCentralLoadRepeaterHandle (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterState (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterWatchList (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterOpenPairing (instance_data_t* data);
static BLEManager_GenJobProcess_t InJobCentralGetRepeaterAdvertiseInfo (instance_data_t* data);



/*---- Private Function ----------------------------------------------------*/

static TASK_RET_TYPE Ble_Manager_Process_thread_main( TASK_PARAM_TYPE arg );
static TASK_RET_TYPE BridgeComm_Response_Process_thread_main( TASK_PARAM_TYPE arg );
static TASK_RET_TYPE Ble_Manager_UnPairDevice_Report_thread_main( TASK_PARAM_TYPE arg );
static void CentralStatusCheckHandler(void* arg);
static void CentralControlFunctionEvent(void* arg);

static dxBOOL PushCentralStateReportToInJobQueue(BleCentralStates_t* CentralStateRspMsg);
static dxBOOL PushDiscoveryCharReportToInJobQueue(BleDeviceCharDiscoveryRspMsg_t* CharDiscvMsg);
static dxBOOL PushWriteCharReportToInJobQueue(BleDeviceWriteCharRspMsg_t* WriteMsg);
static void SendBleResetNotification (BleResetStatus status);
static void _BleManager_CheckDeviceStatus (dxTime_t* NextTime);
static void CollectConcurrentPeripheralDataInPayload (
    dxBOOL dataValid,
    BlePeripheralDataTransfer_t* dataTransfer
);


// For White List
static void IntWhiteList_ClearAll(void);
static void IntWhiteList_CheckNum(uint16_t num);



/******************************************************
 *               Variable Definitions
 ******************************************************/
char         ContinueAutoScan = 1;
dxTime_t        ConnectionStatusLastUpdatedSystemTime;

state_func_table_t StateFuncMap[] = {

        {DEV_MANAGER_ON_JOB_START, InJobStart},
        {DEV_MANAGER_ON_JOB_FIND_DEVICE, InJobFindDevice},
        {DEV_MANAGER_ON_JOB_START_SCAN, InJobStartScanDevice},
        {DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE, InJobSetCentralToIdle},
        {DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, InJobCentralConnectToDevice},
        {DEV_MANAGER_ON_JOB_CENTRAL_SET_DISCONNECT, InJobCentralDisconnect},
        {DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, InJobSecurityClearance},
        {DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE_GIVEN_KEY, InJobSecurityClearanceGivenKey},
        {DEV_MANAGER_ON_JOB_SECURITY_KEY_CLEAR, InJobSecurityKeyClear},
        {DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE, InJobSecurityKeyExchange},
        {DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE_AND_SAVE_FOR_REPORT, InJobSecurityKeyExchangeAndSaveForReport},
        {DEV_MANAGER_ON_JOB_DATATYPE_PAYLOAD_READ, InJobDataTypePayloadRead},
        {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, InJobGenericCharacteristicWrite},
        {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ, InJobGenericCharacteristicRead},
        {DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ_AGGREGATED_UPDATE, InJobGenericCharacteristicReadAggregatedUpdate},
        {DEV_MANAGER_ON_JOB_FW_VERSION_READ, InJobFwVersionRead},
        {DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST, InJobAddPeripheralDevicesToKeeperList},
        {DEV_MANAGER_ON_JOB_REMOVE_DEVICE_FROM_KEEPER_LIST, InJobRemovePeripheralDevicesFromKeeperList},
        {DEV_MANAGER_ON_JOB_REMOVE_ALLDEV_FROM_KEEPER_LIST, InJobRemoveALLPeripheralDevicesFromKeeperList},
        {DEV_MANAGER_ON_JOB_GET_DEVICE_KEEPER_LIST, InJobGetDeviceKeeperList},
        {DEV_MANAGER_ON_JOB_TRANSFER_PERIPHERAL_DATA_OUT, InJobPeripheralTransferDataOut},
        {DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, InJobCleanUpAsCentral},
        {DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER, InJobCleanUpAsRepeater},
        {DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL, InJobCleanUpAsPeripheral},
        {DEV_MANAGER_ON_JOB_FW_BLOCK_WRITE, InJobFwBlockWrite},
        {DEV_MANAGER_ON_JOB_FW_UPDATE_DONE_BOOT_TO_APP, InJobFwDoneBootToApp},
        {DEV_MANAGER_ON_JOB_RTC_UPDATE,InJobUpdateRtc},

        {DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS, InJobConcurrentPeripheralCustomizeBdAddress},
        {DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_DATA_OUT, InJobConcurrentPeripheralDataOut},

        {DEV_MANAGER_ON_JOB_CONNECT_TO_REPEATER, InJobCentralConnectToRepeater},
        {DEV_MANAGER_ON_JOB_LOAD_REPEATER_HANDLE, InJobCentralLoadRepeaterHandle},
        {DEV_MANAGER_ON_JOB_CHECK_REPEATER_STATE, InJobCentralCheckRepeaterState},
        {DEV_MANAGER_ON_JOB_CHECK_REPEATER_WATCH_LIST, InJobCentralCheckRepeaterWatchList},
        {DEV_MANAGER_ON_JOB_CHECK_REPEATER_OPEN_PAIRING, InJobCentralCheckRepeaterOpenPairing},
        {DEV_MANAGER_ON_JOB_GET_REPEATER_ADVERTISE_INFO, InJobCentralGetRepeaterAdvertiseInfo},
};

dxBOOL gDeviceScanReportAllowed = dxTRUE;
static dxBOOL deviceGoingToMissing = dxFALSE;  // This flag will be TRUE if some device is going to missing
static dxTaskHandle_t   BleManagerMainProcess_thread;
static dxTaskHandle_t   BridCommResponseProcess_thread;
static dxTaskHandle_t   BleManagerUnPairDevice_thread;
static dxQueueHandle_t  BleDeviceJobQueue;
dxQueueHandle_t  BleDeviceInJobReportQueue;
static dxQueueHandle_t  BleDeviceInJobCharDiscvReportQueue;
dxQueueHandle_t  BleDeviceInJobCentralStateReportQueue;
static dxQueueHandle_t  BleDeviceInJobWriteCharReportQueue;
static dxQueueHandle_t  BleDeviceInJobReadCharReportQueue;
static dxQueueHandle_t  BleDeviceUnPairDeviceReportQueue;
static dxQueueHandle_t  BleDevicePeripheralDataOutTransferReportQueue;
static dxQueueHandle_t  BleDevicePeripheralDataInTransferReportQueue;
static dxQueueHandle_t  BleDeviceConcurrentPeripheralDataOutTransferReportQueue;

dxWorkweTask_t   BLEManager_EventWorkerThread;
#define EventWorkerThread BLEManager_EventWorkerThread

dxEventHandler_t BLEManager_RegisteredEventFunction;
#define RegisteredEventFunction BLEManager_RegisteredEventFunction

dxEventHandler_t BleRepeater_RegisteredEventFunction;
static dxTimerHandle_t  CentralStatusSessionTimer;
static dxBOOL                   SystemInfoReceived = dxFALSE;
static SystemInfoReportObj_t    SystemInfo;
static BleDeviceJobQueue_t      CurrentJob;
static BlePeripheralDataTransfer_t PeripheralRoleCustomAdvData;
dxAesContext_t   Aes128ctx_ble_enc;
dxAesContext_t   Aes128ctx_ble_dec;
static dxBOOL bRestartBLEFlag = dxFALSE;
static BleResetStatus bleResetStatus = DEV_MANAGER_RESET_TO_UNKNOWN;
static dxMutexHandle_t m_ble_reset_mutex;
static BleConcurrentPeripheralCccd_t* peripheralCccds[MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE];



/******************************************************
 *              State LOCAL Function Definitions
 ******************************************************/

static void SetBLERoleToCentral(dxBOOL StatusUpdateRequestKickoff)
{
    BleManager_TakeBLEResetMutex();
    //First put BLE to reset
    dxGPIO_Write(DK_BLE_RESET, DXPIN_LOW);
    dxTimeDelayMS(500);

    //We clear the internal state
    BLEDevCentralCtrl_ClearConnectionStatus();

    //Set BLE mode to central and bring BLE out of reset
    dxGPIO_Write(DK_BLE_CENTRAL_MODE, DXPIN_HIGH);
    dxTimeDelayMS(100);
    dxGPIO_Write(DK_BLE_RESET, DXPIN_HIGH);
    dxTimeDelayMS(1000);
    BleManager_GiveBLEResetMutex();

    if (StatusUpdateRequestKickoff == dxTRUE)
    {
        BLEDevCentralCtrl_ConnectionStatusUpdateReq();
        BLEDevCentralCtrl_SystemInfoUpdateReq();
    }

    // Notify State Machine BLE has been reset
    SendBleResetNotification (DEV_MANAGER_RESET_TO_CENTRAL);

}


static void SetBLERoleToPeripheral(void)
{
    BleManager_TakeBLEResetMutex();
    //First put BLE to reset
    dxGPIO_Write(DK_BLE_RESET, DXPIN_LOW);
    dxTimeDelayMS(500);

    //We clear the internal state
    BLEDevCentralCtrl_ClearConnectionStatus();

    //Set BLE mode to peripheral and bring BLE out of reset
    dxGPIO_Write(DK_BLE_CENTRAL_MODE, DXPIN_LOW);
    dxTimeDelayMS(100);
    dxGPIO_Write(DK_BLE_RESET, DXPIN_HIGH);
    dxTimeDelayMS(1000);
    BleManager_GiveBLEResetMutex();

    BLEDevCentralCtrl_ConnectionStatusUpdateReq();

    // Notify State Machine BLE has been reset
    SendBleResetNotification (DEV_MANAGER_RESET_TO_PERIPHERAL);

}


void ResetBleModule (void)
{
    BleBridgeCommMsg_t*     BCMessageRsp;
    dxOS_RET_CODE           pop_result;
    dxQueueHandle_t*        BCResponseMsgQueue = BLEDevBridgeComm_GetResponseMsgQueueHdl();

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ResetBleModule! \r\n");

    BleManager_TakeBLEResetMutex();
    // First put BLE to reset
    dxGPIO_Write(DK_BLE_RESET, DXPIN_LOW);
    dxTimeDelayMS(500);

    // We clear the internal state
    BLEDevCentralCtrl_ClearConnectionStatus();

    // Reset message queue
    BLEDevBridgeComm_ResetMsgQueue ();

    // Bring BLE out of reset
    dxGPIO_Write(DK_BLE_RESET, DXPIN_HIGH);
    dxTimeDelayMS(1000);
    BleManager_GiveBLEResetMutex();

    // Notify State Machine BLE has been reset
    SendBleResetNotification (bleResetStatus);
}


static void AutoRescanProcessOnReachingIncomingMaxDevice(uint8_t* IncomingScanDeviceAddr8Byte)
{
#if AUTO_RESCAN_PROCESS_ENABLE

#pragma message ( "REMINDER: AutoRescanProcessOnReachingIncomingMaxDevice is turned ON!" )

    #define MAX_SCAN_REPORT_SLOT    10

    static uint8_t  ScanReportedAddr[B_ADDR_LEN_8][MAX_SCAN_REPORT_SLOT];

    uint8_t i = 0;
    dxBOOL  ScanReportSlotisFull = dxTRUE;

    /* For Log Only
    static dxTime_t TimeOfFirstDevOfScan = 0; //For Log Only
    if (ScanReportedAddr[0][0] == 0)
    {
        TimeOfFirstDevOfScan = dxTimeGetMS();
    }
    */

    for (i = 0; i < MAX_SCAN_REPORT_SLOT; i++)
    {
        if (ScanReportedAddr[0][i] != 0)
        {
            uint8_t j = 0;
            dxBOOL  DevAddrMatched = dxTRUE;
            for (j = 0; j < B_ADDR_LEN_8; j++)
            {
                if (ScanReportedAddr[j][i] != IncomingScanDeviceAddr8Byte[j])
                {
                    DevAddrMatched = dxFALSE;
                    break;
                }
            }

            if (DevAddrMatched == dxTRUE)
            {
                ScanReportSlotisFull = dxFALSE;
                break;
            }
        }

        if (ScanReportedAddr[0][i] == 0)
        {
            uint8_t k = 0;
            for (k = 0; k < B_ADDR_LEN_8; k++)
            {
                ScanReportedAddr[k][i] = IncomingScanDeviceAddr8Byte[k];
            }

            ScanReportSlotisFull = dxFALSE;
            break;
        }
    }

    if (ScanReportSlotisFull)
    {
        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AutoScanProcess MaxDev(%d) found in %dms\r\n", MAX_SCAN_REPORT_SLOT, (dxTimeGetMS() - TimeOfFirstDevOfScan));

        if (BleManagerCurrentState() == DEV_MANAGER_CENTRAL_STATE_SCANNING)
        {
            //NOTE: Although we should clear the gScanReportedAddr when we receive the StopScan as
            //      it can be called from other places. But for simplicity reason we will
            //      just do it here as worse case scenario it will stop scan much sooner than we expected due to
            //      old data is kept even after stop scan by other task.
            memset(ScanReportedAddr, 0, sizeof(ScanReportedAddr));
            BLEDevCentralCtrl_StopScan();
        }
    }
#endif
}


DeviceInfoReport_t* AllocAndParseDeviceInfoReportFromIndex (int16_t DevIndex)
{
    DeviceInfoReport_t* pDeviceInfoReport = NULL;
    const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

    if(DeviceInfoKp)
    {
        pDeviceInfoReport = dxMemAlloc( "Keeper Device Report Msg Buff", 1, sizeof(DeviceInfoReport_t));
    }

    if ((pDeviceInfoReport) && (DeviceInfoKp))
    {

        memcpy(pDeviceInfoReport->DevAddress.Addr, DeviceInfoKp->DevAddress.pAddr , (DeviceInfoKp->DevAddress.AddrLen > B_ADDR_LEN_8) ? B_ADDR_LEN_8:DeviceInfoKp->DevAddress.AddrLen );

        pDeviceInfoReport->DevHandlerID = DevIndex;

        pDeviceInfoReport->rssi = DeviceInfoKp->rssi;

        pDeviceInfoReport->LastUpdatedTimeMs = DeviceInfoKp->ManufacturerAdvDataLastUpdatedSystemTime;

        if (DeviceInfoKp->ManufactureAdvdataLen)
        {

            DeviceManufaturerData_t stDevManufacturerData;
            stDevManufacturerData.AdvertisingType = DeviceInfoKp->AdvertisingType;
            stDevManufacturerData.ManufacturerDataLen = DeviceInfoKp->ManufactureAdvdataLen;
            stDevManufacturerData.pManufacturerData = DeviceInfoKp->pManufactureAdvdata;
            stDevManufacturerData.rssi = DeviceInfoKp->rssi;

            SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc(DevIndex);

            BLEDevParser_GetDkAdvData( &(pDeviceInfoReport->DkAdvertisementData),
                                       &stDevManufacturerData,
                                       (SecurityKey) ? SecurityKey->value:NULL,
                                       (SecurityKey) ? SecurityKey->Size:0);

            if (SecurityKey)
            {
                dxMemFree(SecurityKey);
            }

            if (!DK_FLAG_IS_PAYLOAD_ENCRYPTED(pDeviceInfoReport->DkAdvertisementData.Header.Flag))
            {
                pDeviceInfoReport->Status.bits.Status_NoSecurity_b1 = 1;
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE: Peripheral Status_NoSecurity\r\n" );
            }
            else //Security is enabled, let's check if we find security mask at the beginning of advData
            {
                dxBOOL DataIsValid = dxFALSE;

                int16_t IndexOfSecurityMask = DkPeripheral_GetInfoContainerIndexOfDataTypeIDExt(DK_DATA_TYPE_SECURITY_MASK,
                        pDeviceInfoReport->DkAdvertisementData.AdDataLen,
                        pDeviceInfoReport->DkAdvertisementData.AdData);

                if (IndexOfSecurityMask == 0)
                {
                    uint32_t SecurityMask = 0;
                    memcpy(&SecurityMask, &pDeviceInfoReport->DkAdvertisementData.AdData[1], sizeof(SecurityMask));

                    if ((uint8_t)((SecurityMask >> 24) & 0xFF) == SECURITY_MASK_IDENTIFIER_8BIT)
                    {
                        DataIsValid = dxTRUE;
                    }
                }

                if (!DataIsValid)
                {
                    pDeviceInfoReport->Status.bits.Status_UnknownData_b2 = 1;
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE: Peripheral Status_UnknownData\r\n" );
                }
            }

        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: DeviceInfoReport_t alloc failed or DeviceInfoKp not found!\r\n" );
    }

    return pDeviceInfoReport;
}


static BLEManager_GenJobProcess_t InJobStart( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobStart (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    CleanAllInJobReportQueue();

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;
}


BLEManager_GenJobProcess_t BleGet128bitCharacteristicHandler(DeviceAddress_t* DeviceAddressIn, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t* CharHandlerIDOut)
{
    BLEManager_GenJobProcess_t TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    dxOS_RET_CODE  PopResult;

    *CharHandlerIDOut = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;

    //Need to read security access service first
    uint16_t CharHandlerID = BLEDevKeeper_GetCharacteristicHandlerID(DeviceAddressIn, ServiceUUID, CharacteristicUUID, 1);

    //If we don't have the handle id then let's first discover it
    if (CharHandlerID == CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
    {

        //Discovery the handler first
        BleCentralDiscoverChar_t CharToDiscover;
        CharToDiscover.DiscoveryIdHandShake = CharacteristicUUID;
        CharToDiscover.ServiceUUIDLen = UUID_128_SIZE;
        CharToDiscover.CharUUIDLen = UUID_128_SIZE;

        BLEDevBridgeComm_Build128UUID(CharToDiscover.ServiceUUID, ServiceUUID);
        BLEDevBridgeComm_Build128UUID(CharToDiscover.CharUUID, CharacteristicUUID);

        if (BLEDevCentralCtrl_CharacteristicDiscoveryReq(&CharToDiscover) != DEV_CENTRAL_CTRL_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Failed to send BLEDevCentralCtrl_CharacteristicDiscoveryReq\r\n");
            TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        }
        else
        {

            BleDeviceCharDiscoveryRspMsg_t* CharDiscoveryResponse = NULL;
            PopResult = dxQueueReceive( BleDeviceInJobCharDiscvReportQueue, &CharDiscoveryResponse, DEVICE_CHAR_DISCV_QUEUE_REPORT_WAIT_TIME_OUT_MS );
            if (PopResult == DXOS_SUCCESS)
            {
                if (CharDiscoveryResponse)
                {
                    if (CharDiscoveryResponse->ErrorCode == 0)
                    {
                        if (CharDiscoveryResponse->DiscoveryIdHandShake == CharacteristicUUID)
                        {

                            CharHandlerID = CharDiscoveryResponse->CharHandlerID;
                            BLEDevKeeper_SaveCharacteristicHandlerID(DeviceAddressIn, ServiceUUID, CharacteristicUUID, CharHandlerID, 1);

                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unmatch DiscoveryIdHandShake, got 0x%x but need 0x%x\r\n", CharDiscoveryResponse->DiscoveryIdHandShake, CharacteristicUUID);
                        }
                    }
                }
            }

            if (CharDiscoveryResponse)
                dxMemFree(CharDiscoveryResponse);
        }
    }

    *CharHandlerIDOut = CharHandlerID;

    return TransitionState;
}


BLEManager_GenJobProcess_t BleRead128bitCharacteristic(uint16_t CharHandlerID, uint16_t HandShakeID, BleDeviceReadCharRspMsg_t* ResponseData)
{
    BLEManager_GenJobProcess_t TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    BleCentralReadChar_t CharReadInfo;
    dxOS_RET_CODE  PopResult;

    memset(ResponseData, 0 , sizeof(BleDeviceReadCharRspMsg_t));

     //NOTE: MUST add CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET to handler to get the char "value" because CharHandlerID is the char "declaration" of the character UDID
    CharReadInfo.CharHandlerIDToRead =  CharHandlerID + CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    CharReadInfo.ReadIdHandShake = HandShakeID;

    BLEDeviceCtrl_result_t CharReadResult = BLEDevCentralCtrl_CharacteristicRead(&CharReadInfo);

    if (CharReadResult == DEV_CENTRAL_CTRL_SUCCESS)
    {
        BleDeviceReadCharRspMsg_t* ReadCharResponse = NULL;
        PopResult = dxQueueReceive(BleDeviceInJobReadCharReportQueue,
                                   &ReadCharResponse,
                                   DEVICE_READ_WRITE_CHAR_QUEUE_REPORT_WAIT_TIME_OUT_MS);
        if ((PopResult == DXOS_SUCCESS) && ReadCharResponse)
        {
            if (ReadCharResponse->ErrorCode == 0)
            {
                if (ReadCharResponse->ReadIdHandShake == HandShakeID)
                {
                    memcpy(ResponseData, ReadCharResponse, sizeof(BleDeviceReadCharRspMsg_t));
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unmatch ReadIdHandShake, got 0x%x but need 0x%x\r\n", ReadCharResponse->ReadIdHandShake, HandShakeID);
                }
            }
            else
            {
                //TODO: Handle the error code... eg, return authorization required if the characteristic needs it
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Failed to read char CharHandlerID=%d, HandShakeID=%d\r\n", CharHandlerID, HandShakeID);
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            }

        }

        if (ReadCharResponse)
            dxMemFree(ReadCharResponse);
    }
    else
    {
        //Error, either memory alloc error or pushing to queue timeout
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }

    return TransitionState;

}


BLEManager_GenJobProcess_t BleWrite128bitCharacteristic(uint16_t CharHandlerID, uint16_t HandShakeID, uint8_t* DataToWrite, uint16_t DataLen, dxBOOL* OperationSuccess )
{
    BLEManager_GenJobProcess_t TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    *OperationSuccess = dxFALSE;
    dxOS_RET_CODE  PopResult;

    if ((DataToWrite == NULL) || (DataLen == 0))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BleWrite with NULL Data or invalid Len\r\n" );
        return TransitionState;
    }

    BleCentralWriteChar_t CharWriteInfo;

    //NOTE: MUST add CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET to handler to get the char "value" because CharHandlerID is the char "declaration" of the character UDID
    CharWriteInfo.CharHandlerIDToWrite =  CharHandlerID + CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET;
    CharWriteInfo.WriteIdHandShake = HandShakeID;
    CharWriteInfo.DataWriteLen = DataLen;
    memcpy(CharWriteInfo.DataWrite, DataToWrite, DataLen);

    BLEDeviceCtrl_result_t CharWriteResult = BLEDevCentralCtrl_CharacteristicWrite(&CharWriteInfo);
    if (CharWriteResult == DEV_CENTRAL_CTRL_SUCCESS)
    {
        BleDeviceWriteCharRspMsg_t* WriteCharResponse = NULL;
        PopResult = dxQueueReceive(BleDeviceInJobWriteCharReportQueue, &WriteCharResponse, DEVICE_READ_WRITE_CHAR_QUEUE_REPORT_WAIT_TIME_OUT_MS );

        if ((PopResult == DXOS_SUCCESS) && WriteCharResponse)
        {
            if (WriteCharResponse->ErrorCode == 0)
            {
                if (WriteCharResponse->WriteIdHandShake != HandShakeID)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unmatch WriteIdHandShake, got 0x%x but need 0x%x\r\n", WriteCharResponse->WriteIdHandShake, HandShakeID);
                }
                else
                {
                    *OperationSuccess = dxTRUE;
                }
            }
            else
            {
                //TODO: Handle the error code... eg, return authorization required if the characteristic needs it
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Failed to write char CharHandlerID=%d, HandShakeID=%d\r\n", CharHandlerID, HandShakeID);
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            }

        }

        if (WriteCharResponse)
            dxMemFree(WriteCharResponse);
    }
    else
    {
        //Error, either memory alloc error or pushing to queue timeout
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }


    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobFindDevice( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobFindDevice\r\n");

    dxOS_RET_CODE  PopResult;

    if ((data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) || (data->CurrentCentralStatus->CentralState == BLE_STATE_SCANNING))
    {

        if (BLEDevKeeper_StartFindingDeviceInfoMatchingAddr(data->DeviceAddress.pAddr , ADVERTISING_TYPE_ADV_IND, &BleDeviceInJobReportQueue) == DEV_KEEPER_SUCCESS)
        {
            PopResult = dxQueueReceive(BleDeviceInJobReportQueue,
                                       &data->DeviceInfoObj,
                                       DEVICE_SEARCH_QUEUE_REPORT_WAIT_TIME_OUT_MS);
            if (PopResult == DXOS_SUCCESS)
            {

                DeviceManufaturerData_t DevManAdvData = BLEDevParser_GetManufacturerData(data->DeviceInfoObj);

                if (BLEDevParser_IsDeviceInOpenPairingMode(&DevManAdvData) || BLEDevParser_IsDeviceInStandAloneMode(&DevManAdvData))
                {
                    DeviceAddress_t DevAddress = BLEDevParser_GetAddress(data->DeviceInfoObj);
                    if (DevAddress.AddrLen)
                    {
                        data->DeviceAddress.AddressType = DevAddress.AddressType;

                        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                    else
                    {
                        //There is no address to connect... this should not happend
                        TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                    }
                }
                else
                {
                    TransitionState = DEV_MANAGER_ON_JOB_FAILED_DEVICE_ALREADY_PAIRED;
                }

            }
            else
            {
                if (data->DeviceInfoObj)
                    dxMemFree(data->DeviceInfoObj);

                data->DeviceInfoObj = NULL;

                BLEDevKeeper_StopFindingDeviceInfo();
            }
        }

    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobStartScanDevice( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobStartScanDevice\r\n");

    dxOS_RET_CODE   PopResult;

    ContinueAutoScan = 1;
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        if (BLEDevCentralCtrl_StartScan(4000) == DEV_CENTRAL_CTRL_SUCCESS)
        {
            BleCentralStates_t* CentralStateRspMsg = NULL;
            PopResult = dxQueueReceive(BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS );

            if (PopResult == DXOS_SUCCESS)
            {
                if (CentralStateRspMsg->CentralState == BLE_STATE_SCANNING)
                {
                    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                }
            }

            if (CentralStateRspMsg)
                dxMemFree(CentralStateRspMsg);

        }
        else
        {
            //Error, either memory alloc error or pushing to queue timeout
            TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        }
    }
    else if (data->CurrentCentralStatus->CentralState == BLE_STATE_SCANNING)
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobSetCentralToIdle( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobSetCentralToIdle (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    dxOS_RET_CODE   PopResult;

    ContinueAutoScan = 0; //Disable auto scan when stop is detected

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_SCANNING)
    {

        BLEDevCentralCtrl_StopScan();

        BleCentralStates_t* CentralStateRspMsg = NULL;

        PopResult = dxQueueReceive (BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS);

        if (PopResult == DXOS_SUCCESS)
        {
            if (CentralStateRspMsg->CentralState == BLE_STATE_IDLE)
            {
                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        }

        if (CentralStateRspMsg)
            dxMemFree(CentralStateRspMsg);
    }
    else if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobCentralConnectToDevice( instance_data_t *data )
{

    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobCentralConnectToDevice\r\n" );

    dxOS_RET_CODE   PopResult;

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        BLEDeviceCtrl_result_t ConnectResult = DEV_CENTRAL_CTRL_SUCCESS;

        ConnectResult = BLEDevCentralCtrl_ConnectToDevice(&data->DeviceAddress);

        if (ConnectResult == DEV_CENTRAL_CTRL_SUCCESS)
        {
            BleCentralStates_t* CentralStateRspMsg = NULL;
            PopResult = dxQueueReceive( BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS );

            if (PopResult == DXOS_SUCCESS)
            {
                if (CentralStateRspMsg->CentralState == BLE_STATE_CONNECTED)
                {
                    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobCentralConnectToDevicer central state pop TimeOut\r\n");
            }

            if (CentralStateRspMsg)
                dxMemFree(CentralStateRspMsg);

        }
        else
        {
            //Error, either memory alloc error or pushing to queue timeout
            TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        }
    }
    else if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobCentralDisconnect( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobCentralDisconnect\r\n");

    dxOS_RET_CODE   PopResult;

    if ((data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED) || (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTING))
    {
        if (BLEDevCentralCtrl_DisconnectDevice() == DEV_CENTRAL_CTRL_SUCCESS)
        {
            BleCentralStates_t* CentralStateRspMsg = NULL;
            PopResult = dxQueueReceive(BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS );

            if (PopResult == DXOS_SUCCESS)
            {
                if (CentralStateRspMsg->CentralState == BLE_STATE_DISCONNECTING)
                {
                    if (CentralStateRspMsg)
                    {
                        dxMemFree(CentralStateRspMsg);
                        CentralStateRspMsg = NULL;
                    }

                    //Wait for IDLE - Usually will arrive after BLE_STATE_DISCONNECTING
                     PopResult = dxQueueReceive( BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, DEVICE_CENTRAL_STATE_QUEUE_REPORT_WAIT_TIME_OUT_MS );

                }

                if (PopResult == DXOS_SUCCESS)
                {
                    if (CentralStateRspMsg->CentralState == BLE_STATE_IDLE)
                    {
                        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                }
            }

            if (CentralStateRspMsg)
                dxMemFree(CentralStateRspMsg);

        }
        else
        {
            //Error, either memory alloc error or pushing to queue timeout
            TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
        }
    }
    else if ((data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) || (data->CurrentCentralStatus->CentralState == BLE_STATE_SCANNING))
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;

}

static BLEManager_GenJobProcess_t JobSecurityClearance( instance_data_t *data, dxBOOL SecurityKeyInInstanceContainer )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter JobSecurityClearance (data={CentralState=%d}, SecurityKeyInInstanceContainer=%d)\r\n", data->CurrentCentralStatus->CentralState, SecurityKeyInInstanceContainer);

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {
        //Need to read security access service first
        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &CharHandlerID);

        //If we did success discovering it then we send the read security access
        unsigned char   AesDataOut[AES_ENC_DEC_BYTE];
        dxBOOL          SendChallengePhrase = dxFALSE;
        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {
            //Let's read the security access info
            BleDeviceReadCharRspMsg_t ReadCharResponse;
            TransitionState = BleRead128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &ReadCharResponse);

            if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID))
            {
                if (ReadCharResponse.DataReadLen == sizeof(SecurityAccessInfo_t))
                {
                    SecurityAccessInfo_t SecurityAccessInfo;
                    memcpy(&SecurityAccessInfo, ReadCharResponse.DataRead, sizeof(SecurityAccessInfo_t));

                    if (SecurityAccessInfo.ClearanceAccess == SECURITY_CLEARANCE_SUCCESS)
                    {
                        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                    else //We may need to challenge for clearance
                    {
                        if ((SecurityAccessInfo.SecurityLevel == SECURITY_LEVEL_PIN_CODE_HASH) &&
                            (SecurityAccessInfo.SecurityPayloadType == SECURITY_PAYLOAD_TYPE_CHALLENGE))
                        {

                            SecurityKeyCodeContainer_t* SecurityKey = NULL;

                            if (SecurityKeyInInstanceContainer ==dxTRUE)
                            {
                                SecurityKey = dxMemAlloc("SecurityKey", 1, sizeof(SecurityKeyCodeContainer_t));
                                memcpy(SecurityKey, &data->SecurityKeyCodeContainer, sizeof(SecurityKeyCodeContainer_t));
                            }
                            else
                            {
                                int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(data->DeviceAddress);

                                if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
                                {
                                    SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc(DeviceIndex);
                                }
                                else
                                {
                                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: InJobSecurityClearance DeviceIndex not found\r\n" );
                                    TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                                }
                            }

                            if (SecurityKey)
                            {

                                if (SecurityKey->Size > AES_ENC_DEC_BIT)
                                {
                                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Security Key Size found too large\r\n");
                                }

                                dxAESSW_Setkey( Aes128ctx_ble_dec, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                                dxAESSW_CryptEcb( Aes128ctx_ble_dec, DXAES_DECRYPT, SecurityAccessInfo.SecurityPayload, AesDataOut  );

                                //Get the challenge number (uint32) and add one to it, then send it back
                                uint32_t ChallengeNumber = 0;
                                memcpy(&ChallengeNumber, AesDataOut, sizeof(ChallengeNumber) );
                                ChallengeNumber++;

                                unsigned char   AesDataIn[AES_ENC_DEC_BYTE];
                                memset(AesDataIn, 0 , sizeof(AesDataIn));
                                memcpy(AesDataIn, &ChallengeNumber, sizeof(ChallengeNumber));

                                dxAESSW_Setkey( Aes128ctx_ble_enc, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_ENCRYPT );
                                dxAESSW_CryptEcb( Aes128ctx_ble_enc, DXAES_ENCRYPT, AesDataIn, AesDataOut );

                                SendChallengePhrase = dxTRUE;

                                dxMemFree(SecurityKey);
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE: Unable to comple security challenge due to missing security key\r\n" );
                                TransitionState = DEV_MANAGER_ON_JOB_MISSING_SECURITY_KEY_ERROR;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: unable to handle Securitylevel and/or SecurityPayloadType\r\n" );
                            TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                        }
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Security access read returned different data len\r\n" );
                    TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                }

            }

        }

        if (SendChallengePhrase)
        {

            dxBOOL OperationIsSuccess;
            SecurityAccessCommand_t SecurityAccessCommand;
            SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHALLENGE;
            SecurityAccessCommand.SubCommand = 0;
            memcpy(SecurityAccessCommand.Payload, AesDataOut, sizeof(SecurityAccessCommand.Payload));

            TransitionState = BleWrite128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, (uint8_t*)&SecurityAccessCommand, sizeof(SecurityAccessCommand_t), &OperationIsSuccess);

            if (OperationIsSuccess == dxTRUE)
            {
                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        }
    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave JobSecurityClearance (data={CentralState=%d}, SecurityKeyInInstanceContainer=%d), success: TransitionState=%d\r\n", data->CurrentCentralStatus->CentralState, SecurityKeyInInstanceContainer, TransitionState);

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobSecurityClearance( instance_data_t *data )
{
    return JobSecurityClearance (data, dxFALSE);
}


static BLEManager_GenJobProcess_t InJobSecurityClearanceGivenKey( instance_data_t *data )
{
    return JobSecurityClearance (data, dxTRUE);
}


static BLEManager_GenJobProcess_t InJobSecurityKeyClear( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobSecurityKeyClear\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        //Need to read security access service first
        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &CharHandlerID);

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobSecurityKeyClear 1\r\n");

        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {
            //Now we send the key exchange request providing the random value
            dxBOOL OperationIsSuccess;
            SecurityAccessCommand_t SecurityAccessCommand;
            SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHANGE_SECURITY;
            SecurityAccessCommand.SubCommand = SYSTEM_UTIL_SA_CS_SUB_SECURITY_METHOD_NONE;
            memset(SecurityAccessCommand.Payload, 0, sizeof(SecurityAccessCommand.Payload));

            TransitionState = BleWrite128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, (uint8_t*)&SecurityAccessCommand, sizeof(SecurityAccessCommand_t), &OperationIsSuccess);

            if (OperationIsSuccess == dxTRUE)
            {
                //After we send the key let's read back the confirmation of encrypted random value for verification if the provided PIN Code is accurate
                BleDeviceReadCharRspMsg_t ReadCharResponse;
                TransitionState = BleRead128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &ReadCharResponse);

                if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID))
                {
                    if (ReadCharResponse.DataReadLen == sizeof(SecurityAccessInfo_t))
                    {

                        SecurityAccessInfo_t SecurityAccessInfo;
                        memcpy(&SecurityAccessInfo, ReadCharResponse.DataRead, sizeof(SecurityAccessInfo_t));

                        if ((SecurityAccessInfo.SecurityLevel != SECURITY_LEVEL_NONE) || (SecurityAccessInfo.ClearanceAccess != SECURITY_CLEARANCE_SUCCESS))
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: InJobSecurityKeyClear failed\r\n");
                        }

                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobSecurityKeyClear Done\r\n");

                        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                }
            }
        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }


    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobUpdateRtc( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobUpdateRtc\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        //Need to read security access service first
        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_RTC_ACCESS_UUID, &CharHandlerID);

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobUpdateRtc Process Line=%d\r\n", __LINE__);

        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {

            dxBOOL OperationIsSuccess;
            RTC_t UpdateRTC;
            DKTime_t CurrentTimeRTC;
            memset(&UpdateRTC, 0, sizeof(UpdateRTC));
            if (dxSwRtcGet(&CurrentTimeRTC) == DXOS_SUCCESS)
            {
                UpdateRTC.Seconds = CurrentTimeRTC.sec;
                UpdateRTC.Minutes = CurrentTimeRTC.min;
                UpdateRTC.Hours24H = CurrentTimeRTC.hour;
                UpdateRTC.Dates = CurrentTimeRTC.day;
                UpdateRTC.Months = CurrentTimeRTC.month;
                UpdateRTC.Years = CurrentTimeRTC.year - 2000;
                UpdateRTC.DaysOfWeek = (CurrentTimeRTC.dayofweek == 7) ? 1:(CurrentTimeRTC.dayofweek+1);

                TransitionState = BleWrite128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_RTC_ACCESS_UUID, (uint8_t*)&UpdateRTC, sizeof(RTC_t), &OperationIsSuccess);

                if (OperationIsSuccess == dxTRUE)
                {
                    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobUpdateRtc Process Line=%d\r\n", __LINE__);
                }
                else
                {
                    TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobUpdateRtc Error Line=%d\r\n", __LINE__);
                }
            }
            else
            {
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobUpdateRtc Error Line=%d\r\n", __LINE__);
            }
        }
    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }


    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobSecurityKeyExchangeAndSaveForReport( instance_data_t *data )
{

    BLEManager_GenJobProcess_t TransitionState = InJobSecurityKeyExchange(data);

    if (TransitionState == DEV_MANAGER_ON_JOB_NEXT_STATE)
    {
        //Default to internal error
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;

        int16_t DevIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(data->DeviceAddress);

        if (DevIndex != DEV_KEEPER_NOT_FOUND)
        {
            SecurityKeyCodeContainer_t* SecurityKey = (data->SecurityKeyCodeContainer.Size > 0) ? &data->SecurityKeyCodeContainer:NULL;

            BLEDevKeeper_UpdateDeviceSecurityKey(DevIndex, SecurityKey);

            int16_t* pDeviceIndexData = dxMemAlloc( "Device index Buff", 1, sizeof(int16_t));

            if (pDeviceIndexData)
            {
                *pDeviceIndexData = DevIndex;

                if (data->pEventPayload)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: data->pEventPayload NOT NULL, possible mem leak\r\n");
                }

                data->pEventPayload = (uint8_t*)pDeviceIndexData;

                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        }
    }

    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobSecurityKeyExchange( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobSecurityKeyExchange\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        //Need to read security access service first
        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &CharHandlerID);

        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {

            unsigned char   SecurityKey[AES_ENC_DEC_BYTE];
            unsigned char   RandomValue[AES_ENC_DEC_BYTE];
            unsigned char   AesDataInOut[AES_ENC_DEC_BYTE];

            //TODO: We are going to create a random 16 byte value, for now let's simply take the system time and use AES to generate the bytes
            dxTime_t time = dxTimeGetMS();
            memcpy(AesDataInOut, &time, sizeof(time));
            uint8_t i = 0;
            for (i = 0; i < AES_ENC_DEC_BYTE; i++)
            {
                time++; SecurityKey[i] = ((uint8_t)time > 0) ? (uint8_t)time:1;
            }

            dxAESSW_Setkey( Aes128ctx_ble_enc, SecurityKey, AES_ENC_DEC_BIT, DXAES_ENCRYPT );
            dxAESSW_CryptEcb( Aes128ctx_ble_enc, DXAES_ENCRYPT, AesDataInOut, RandomValue );

            //Lets first calculate the key

            unsigned char* PlainTextBuffer = dxMemAlloc("PlainText For Hash", 1, AES_ENC_DEC_BYTE+SECURITY_KEY_CODE_MAX_SIZE);

            //Pad the security code to the end of random number
            memcpy(PlainTextBuffer, RandomValue, AES_ENC_DEC_BYTE);
            memcpy(&PlainTextBuffer[AES_ENC_DEC_BYTE], data->SecurityKeyCodeContainer.value, data->SecurityKeyCodeContainer.Size);

            //Generate the key
            md2_hmac(   (unsigned char*)data->SecurityKeyCodeContainer.value, data->SecurityKeyCodeContainer.Size,
                        PlainTextBuffer, AES_ENC_DEC_BYTE+data->SecurityKeyCodeContainer.Size,
                        SecurityKey );

            dxMemFree(PlainTextBuffer);

            //Now we send the key exchange request providing the random value
            dxBOOL OperationIsSuccess;
            SecurityAccessCommand_t SecurityAccessCommand;
            memset(&SecurityAccessCommand,0,sizeof(SecurityAccessCommand));
            SecurityAccessCommand.Command = SYSTEM_UTIL_SECURITY_ACCESS_CHANGE_SECURITY;
            SecurityAccessCommand.SubCommand = SYSTEM_UTIL_SA_CS_SUB_SECURITY_LEVEL_PIN_CODE_HASH;
            memcpy(SecurityAccessCommand.Payload, RandomValue, sizeof(SecurityAccessCommand.Payload));


            TransitionState = BleWrite128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, (uint8_t*)&SecurityAccessCommand, sizeof(SecurityAccessCommand_t), &OperationIsSuccess);

            if (OperationIsSuccess == dxTRUE)
            {
                //After we send the key let's read back the confirmation of encrypted random value for verification if the provided PIN Code is accurate
                BleDeviceReadCharRspMsg_t ReadCharResponse;
                TransitionState = BleRead128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID, &ReadCharResponse);

                if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID))
                {
                    if (ReadCharResponse.DataReadLen == sizeof(SecurityAccessInfo_t))
                    {
                        SecurityAccessInfo_t SecurityAccessInfo;
                        memcpy(&SecurityAccessInfo, ReadCharResponse.DataRead, sizeof(SecurityAccessInfo_t));

                        if ((SecurityAccessInfo.SecurityLevel == SECURITY_LEVEL_PIN_CODE_HASH) &&
                            (SecurityAccessInfo.SecurityPayloadType == SECURITY_PAYLOAD_TYPE_NEW_PIN_CODE_HASH_KEY_VERIFY))
                        {
                            dxBOOL KeyExchangeSuccess = dxTRUE;

                            dxAESSW_Setkey( Aes128ctx_ble_dec, SecurityKey, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                            dxAESSW_CryptEcb( Aes128ctx_ble_dec, DXAES_DECRYPT, SecurityAccessInfo.SecurityPayload, AesDataInOut );

                            //Check if the decrypted value is same as the random value we sent previously
                            uint8_t i = 0;
                            for (i = 0; i < AES_ENC_DEC_BYTE; i++)
                            {
                                if (RandomValue[i] != AesDataInOut[i])
                                {
                                    KeyExchangeSuccess = dxFALSE;
                                    break;
                                }
                            }

                            if (KeyExchangeSuccess == dxTRUE)
                            {
                                //NOTE: Since we are done using the Pin Code, we will now use the SecurityKeyCodeContainer buffer to store Security Key

                                //First clean up the container.
                                memset(&data->SecurityKeyCodeContainer, 0, sizeof(data->SecurityKeyCodeContainer));

                                //NOTE: Currently we only support Pin Code Hash based security and Key size is 16 byte. Bigger key size can be supported in the future and depends on platform (peripheral)
                                //We save the verified security key to SecurityKeyCodeContainer, this will be checked in the next stage (add peripheral to keeper list)
                                data->SecurityKeyCodeContainer.Size = AES_ENC_DEC_BYTE; //AES128 - 16 byte
                                memcpy(data->SecurityKeyCodeContainer.value, SecurityKey, AES_ENC_DEC_BYTE);

                                BleRepeater_NotifyAfterPairingDone ();

                                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                            }
                            else
                            {
                                //Since the PIN code is not valid and we already generated the key, we need to make sure we clear the security on
                                //the peripheral side s that the next key exchange with pin code can happen.
                                InJobSecurityKeyClear(data);

                                TransitionState = DEV_MANAGER_ON_JOB_INVALID_PIN_CODE_ERROR;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: unexpected Securitylevel and/or SecurityPayloadType\r\n" );
                            TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                        }

                    }
                }
            }
        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }


    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobGenericCharacteristicWrite( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        if (!data->MainCharWriteData.UUIDNeed128bitConversion)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
        }

        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, data->MainCharWriteData.ServiceUUID, data->MainCharWriteData.CharacteristicUUID, &CharHandlerID);

        //If we did success discovering it then we send the paired request
        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {

            dxBOOL OperationIsSuccess;

            if (data->MainCharWriteData.DataWriteLen > CHAR_READ_WRITE_DATA_MAX) //Larger than one transaction allowed?
            {
                MultiTransacDataFormat_t    TransactionData;
                int16_t TotalSizetoTransfer = data->MainCharWriteData.DataWriteLen;
                uint8_t  TotalPage = ((data->MainCharWriteData.DataWriteLen % sizeof(TransactionData.Payload)) == 0) ?
                                            (data->MainCharWriteData.DataWriteLen / sizeof(TransactionData.Payload)) :
                                            (data->MainCharWriteData.DataWriteLen / sizeof(TransactionData.Payload) + 1);

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLE Write TotalLen = %d (TotalPage = %d)\r\n", TotalSizetoTransfer, TotalPage);

                TransactionData.HeaderID  = MULTI_TRANSAC_HEADER_ID;
                TransactionData.PageInfo.bits.TotalPage_b0_3 = TotalPage;
                uint8_t CurrentPage = 1;
                uint8_t *pDataToTransac = data->MainCharWriteData.DataWrite;
                do
                {
                    TransactionData.PageInfo.bits.CurrentPage_b4_7 = CurrentPage;
                    TransactionData.ValidByte = (TotalSizetoTransfer > sizeof(TransactionData.Payload)) ? sizeof(TransactionData.Payload) : TotalSizetoTransfer;
                    memset(TransactionData.Payload, 0, sizeof(TransactionData.Payload) );
                    memcpy(TransactionData.Payload, pDataToTransac, TransactionData.ValidByte);

                    TransitionState = BleWrite128bitCharacteristic(CharHandlerID, data->MainCharWriteData.CharacteristicUUID, (uint8_t*)&TransactionData, sizeof(TransactionData), &OperationIsSuccess);

                    //No retry... its all or none
                    if (!OperationIsSuccess)
                        break;

                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLE Write PageNum = %d, Len = %d)\r\n", CurrentPage, TransactionData.ValidByte);

                    TotalSizetoTransfer -= TransactionData.ValidByte;
                    if (TotalSizetoTransfer > 0)
                    {
                        CurrentPage++;
                        pDataToTransac += TransactionData.ValidByte;
                    }

                } while (TotalSizetoTransfer > 0);

            }
            else
            {
                TransitionState = BleWrite128bitCharacteristic(CharHandlerID, data->MainCharWriteData.CharacteristicUUID, data->MainCharWriteData.DataWrite, data->MainCharWriteData.DataWriteLen, &OperationIsSuccess);
            }

            if (!OperationIsSuccess)
            {
                //Error, either memory alloc error or pushing to queue timeout
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
            }
            else
            {
                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }
        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {

        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }
    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobGenericCharacteristicRead( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobGenericCharacteristicRead\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        if (!data->MainCharReadData.UUIDNeed128bitConversion)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
        }

        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, data->MainCharReadData.ServiceUUID, data->MainCharReadData.CharacteristicUUID, &CharHandlerID);

        dxBOOL  MorePage = dxTRUE;
        dxBOOL  MultiPageFormat = dxFALSE;
        uint8_t MultiPageTotalReceived = 0;
        uint8_t MultiPageExpectedPage = 1; //Always start at index 1
        DataReadReportObj_t*    pReadDataPayload = NULL;
        uint8_t*                pReadDataPayloadIndex = NULL;
        while (MorePage)
        {

            BleDeviceReadCharRspMsg_t ReadCharResponse;
            TransitionState = BleRead128bitCharacteristic(CharHandlerID, data->MainCharReadData.CharacteristicUUID, &ReadCharResponse);

            if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == data->MainCharReadData.CharacteristicUUID))
            {
                if (pReadDataPayload == NULL)
                {
                    pReadDataPayload = dxMemAlloc( "DataRead Buff", 1, sizeof(DataReadReportObj_t));
                    pReadDataPayloadIndex = pReadDataPayload->Payload;
                }

                if (ReadCharResponse.DataReadLen)
                {
                    if (MultiPageFormat == dxFALSE)
                    {
                        if (ReadCharResponse.DataRead[0] == MULTI_TRANSAC_HEADER_ID)
                        {
                            MultiPageFormat = dxTRUE;
                        }
                    }

                    if (MultiPageFormat == dxTRUE)
                    {
                        MultiTransacDataFormat_t* TransactionData = (MultiTransacDataFormat_t*)ReadCharResponse.DataRead;
                        if (TransactionData->PageInfo.bits.CurrentPage_b4_7 == MultiPageExpectedPage)
                        {
                            uint8_t TotalDataLen = (TransactionData->ValidByte > sizeof(TransactionData->Payload)) ? sizeof(TransactionData->Payload):TransactionData->ValidByte;
                            pReadDataPayload->PayloadSize += TotalDataLen;

                            if (pReadDataPayload->PayloadSize > sizeof(pReadDataPayload->Payload))
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Multipage total data size exceed limitation!\r\n");
                                TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                                MorePage = dxFALSE;
                            }
                            else
                            {
                                memcpy(pReadDataPayloadIndex, TransactionData->Payload, TotalDataLen);
                                pReadDataPayloadIndex += TotalDataLen;
                                MultiPageTotalReceived++;
                                MultiPageExpectedPage++;

                                if (MultiPageTotalReceived == TransactionData->PageInfo.bits.TotalPage_b0_3)
                                {
                                    //All page received!
                                    data->pEventPayload = (uint8_t*)pReadDataPayload;
                                    MorePage = dxFALSE;
                                    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                                }
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: MultiPage expected page not matched! (%d != %d)\r\n", TransactionData->PageInfo.bits.CurrentPage_b4_7, MultiPageExpectedPage);
                            TransitionState = DEV_MANAGER_ON_JOB_FAILED_RECEIVING_MSG;
                            MorePage = dxFALSE;
                        }
                    }
                    else
                    {
                        uint16_t TotalDataLen = (ReadCharResponse.DataReadLen > sizeof(pReadDataPayload->Payload)) ? sizeof(pReadDataPayload->Payload):ReadCharResponse.DataReadLen;
                        memcpy(pReadDataPayload->Payload, ReadCharResponse.DataRead, TotalDataLen);
                        pReadDataPayload->PayloadSize = TotalDataLen;

                        data->pEventPayload = (uint8_t*)pReadDataPayload;
                        MorePage = dxFALSE;
                        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
                    }
                }

            }
            else //Failed - All or None
            {
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_RECEIVING_MSG;
                MorePage = dxFALSE;
            }

        }

        //If pEventPayload is NULL (not assigned) then we check if pReadDataPayload is allocated, if so we free it as it will not go anywhere since the access failed.
        if (data->pEventPayload == NULL)
        {
            if (pReadDataPayload)
                dxMemFree(pReadDataPayload);
        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobFwVersionRead( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobFwVersionRead\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO, &CharHandlerID);

        BleDeviceReadCharRspMsg_t ReadCharResponse;
        TransitionState = BleRead128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO, &ReadCharResponse);

        if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO))
        {

            if (ReadCharResponse.DataReadLen > 2)
            {
                uint16_t FwVersion = 0;
                memcpy(&FwVersion, ReadCharResponse.DataRead, 2);

                uint16_t MajorVer = (FwVersion & 0xFF00) >> 8;
                uint16_t MinorVer = (FwVersion & 0x00FF);

                data->DeviceFwVersion.Major = MajorVer;
                data->DeviceFwVersion.Middle = 0;
                data->DeviceFwVersion.Minor = MinorVer;

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobFwVersionRead FwVer = %d.%d.%d\r\n", data->DeviceFwVersion.Major, data->DeviceFwVersion.Middle, data->DeviceFwVersion.Minor);
            }

            TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }
    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobGenericCharacteristicReadAggregatedUpdate( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobGenericCharacteristicReadAggregatedUpdate\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        if (!data->MainCharReadData.UUIDNeed128bitConversion)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: ONLY 128 bit UUID is supported at the moment\r\n");
        }

        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, data->MainCharReadData.ServiceUUID, data->MainCharReadData.CharacteristicUUID, &CharHandlerID);

        dxBOOL       MorePage = dxTRUE;
        uint8_t      ExpectedPageToRead = 1; //Start at 1 according to spec
        uint8_t      TotalPageAlreadyRead = 0;
        uint8_t*     pReportPayload = NULL;
        uint8_t*     pOrgReportPayload = NULL;
        AggregatedReportHeader_t ReportHeader;

        memset((uint8_t *) &ReportHeader, 0 , sizeof(AggregatedReportHeader_t));

        while (MorePage)
        {

            MorePage = dxFALSE;

            BleDeviceReadCharRspMsg_t ReadCharResponse;
            TransitionState = BleRead128bitCharacteristic(CharHandlerID, data->MainCharReadData.CharacteristicUUID, &ReadCharResponse);

            if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == data->MainCharReadData.CharacteristicUUID))
            {

                //We expect exactly 20 byte of data
                if (ReadCharResponse.DataReadLen >= CHAR_READ_WRITE_DATA_MAX)
                {
                    AggregatedDataHeader_t AgHeader;
                    memcpy(&AgHeader, ReadCharResponse.DataRead, sizeof(AggregatedDataHeader_t));

                    if (AgHeader.HeaderID == AGGREGATED_HEADER_ID)
                    {
                        if ((pReportPayload == NULL) && (AgHeader.PageInfo.bits.TotalPage_b0_3))
                        {
                            //Alloc Memory
                            pReportPayload = dxMemAlloc("Aggregated Report Msg Buff", 1, sizeof(AggregatedReportHeader_t) + AgHeader.PageInfo.bits.TotalPage_b0_3 * CHAR_READ_WRITE_DATA_MAX);

                            if (pReportPayload)
                            {
                                //Save the origin pointer
                                pOrgReportPayload = pReportPayload;

                                //Copy the address
                                memcpy(ReportHeader.DevAddress.Addr , data->DeviceAddress.pAddr, data->DeviceAddress.AddrLen);

                                //Copy the header to reporter
                                memcpy(&ReportHeader.AggregatedItemHeader, &AgHeader, sizeof(AggregatedDataHeader_t));

                                //Skip the byte (according to size of AggregatedReportHeader_t) and actual aggregate data will start there
                                pReportPayload += sizeof(AggregatedReportHeader_t);
                            }
                        }

                        if (pReportPayload)
                        {
                             if (AgHeader.PageInfo.bits.CurrentPage_b4_7 == ExpectedPageToRead)
                             {
                                int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(data->DeviceAddress);

                                if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
                                {
                                    DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DeviceIndex);

                                    if (DeviceReportInfo)
                                    {
                                        unsigned char   DataOut[AES_ENC_DEC_BYTE];
                                        dxBOOL  DataOutAvailable = dxTRUE;
                                        if (DeviceReportInfo->Status.bits.Status_NoSecurity_b1 == 0)
                                        {
                                            SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc(DeviceIndex);
                                            if (SecurityKey)
                                            {
                                                dxAESSW_Setkey( Aes128ctx_ble_dec, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                                                dxAESSW_CryptEcb( Aes128ctx_ble_dec, DXAES_DECRYPT, ReadCharResponse.DataRead + sizeof(AggregatedDataHeader_t), DataOut  );
                                                dxMemFree(SecurityKey);
                                            }
                                            else
                                            {
                                                DataOutAvailable = dxFALSE;
                                            }
                                        }
                                        else
                                        {
                                            //Overwrite back the last 16 byte of unencrypted data
                                            memcpy( DataOut, ReadCharResponse.DataRead + sizeof(AggregatedDataHeader_t), sizeof(DataOut));
                                        }

                                        if (DataOutAvailable)
                                        {

                                            //According to spec the Format 0 has fixed size of 8 bytes while Format 1 has fix size of 12 bytes
                                            uint8_t FormatSize = 8; //Default Format Size is 8 (FORMAT 0)
                                            if (ReportHeader.AggregatedItemHeader.FormatInfo.bits.AfDFormat_b0_3 == 1)
                                            {
                                                FormatSize = 12;
                                            }

                                            uint8_t  i = 0;
                                            while (i < sizeof(DataOut))
                                            {
                                                if ((DataOut[i] != 0xFF) && (DataOut[i+1] != 0xFF))
                                                {

                                                    memcpy( pReportPayload, &DataOut[i], FormatSize);

#if 0
#pragma message ( "WARNING: BLE Aggregate Data Printout is ENABLED. Not for Mass production!" )
                                                    if (FormatSize == 12)
                                                    {

                                                        typedef struct
                                                        {
                                                            uint32_t        Time;
                                                            uint8_t         DataID;
                                                            uint8_t         DataValType;
                                                            uint16_t        Duration;
                                                            uint32_t        DataValue;
                                                        } AggregatedFormat1_t;

                                                        AggregatedFormat1_t* pAggFormat1 = (AggregatedFormat1_t*)pReportPayload;

                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "===========================\r\n");
                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AGG AfD1 Time=%d\r\n", pAggFormat1->Time);
                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AGG AfD1 DataID=%d\r\n", pAggFormat1->DataID);
                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AGG AfD1 DataType=%d\r\n", pAggFormat1->DataValType);
                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AGG AfD1 Duration=%d\r\n", pAggFormat1->Duration);
                                                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "AGG AfD1 DataValue=%d\r\n", pAggFormat1->DataValue);
                                                    }

#endif

                                                    pReportPayload += FormatSize;
                                                    ReportHeader.TotalAggregatedItem++;
                                                }
                                                i += FormatSize;
                                            }
                                        }

                                        dxMemFree(DeviceReportInfo);
                                    }
                                }

                                TotalPageAlreadyRead++;

                                //Continue to next page if we have more
                                if (AgHeader.PageInfo.bits.CurrentPage_b4_7 < AgHeader.PageInfo.bits.TotalPage_b0_3)
                                {
                                    MorePage = dxTRUE;
                                    ExpectedPageToRead++;
                                }

                             }
                             else
                             {
                                 G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Aggregated Data page read mismatched\r\n");
                             }
                        }

                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Aggregated data header id missing\r\n");
                    }

                }

                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }

        }

        if (pReportPayload)
        {
            //ALL OR NONE
            if (ReportHeader.AggregatedItemHeader.PageInfo.bits.TotalPage_b0_3 == TotalPageAlreadyRead)
            {
                EventReportObj_t* EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
                EventReportObj->Header.IdentificationNumber = 0;
                EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT;

                //Copy header to beginning of Payload
                memcpy(pOrgReportPayload , &ReportHeader, sizeof(AggregatedReportHeader_t));

                EventReportObj->pData = pOrgReportPayload;

                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
                if (EventResult != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Failed to send Event message back to Application\r\n" );
                    dxMemFree(pOrgReportPayload);
                    dxMemFree(EventReportObj);
                }
            }
            else
            {
                dxMemFree(pOrgReportPayload); //NOTE: pOrgReportPayload = pReportPayload
            }

        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }
    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobDataTypePayloadRead( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobDataTypePayloadRead\r\n");

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED)
    {

        uint16_t CharHandlerID = CHARACTERISTIC_HANDLER_ID_NOT_FOUND;
        TransitionState = BleGet128bitCharacteristicHandler(&data->DeviceAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_UUID, &CharHandlerID);

        //If we did success discovering it then we send the data read request
        if (CharHandlerID != CHARACTERISTIC_HANDLER_ID_NOT_FOUND)
        {

            BleDeviceReadCharRspMsg_t ReadCharResponse;
            TransitionState = BleRead128bitCharacteristic(CharHandlerID, DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_UUID, &ReadCharResponse);

            if ((TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE) &&  (ReadCharResponse.ReadIdHandShake == DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_UUID))
            {
                int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(data->DeviceAddress);

                if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
                {
                    //TODO: For now we only support "single" page adv mode read back... We will support multi page read back in near future
                    if ((ReadCharResponse.DataRead[0] == 0) && (ReadCharResponse.DataReadLen == 20))
                    {
                        DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DeviceIndex);

                        if (DeviceReportInfo)
                        {
                            //We overwrite to current time as we just got it.
                            DeviceReportInfo->LastUpdatedTimeMs = dxTimeGetMS();

                            //We overwrite the DataType payload with the latest and send back, Note that we are skipping the first byte which is header
                            DeviceReportInfo->DkAdvertisementData.AdDataLen = ReadCharResponse.DataReadLen - 1;
                            memset( DeviceReportInfo->DkAdvertisementData.AdData, 0, sizeof( DeviceReportInfo->DkAdvertisementData.AdData ) );
                            memcpy( DeviceReportInfo->DkAdvertisementData.AdData, &ReadCharResponse.DataRead[1], ReadCharResponse.DataReadLen - 1);

                            //If peripheral is encrypted ( and SecurityKey found) then it means advertisement data is encrypted, we will need to
                            //decrypt it first. Note that only the first 16 byte is decrypted as the rest is non encrypted data according to
                            //Dexatek one page advertisement specification
                            if (DeviceReportInfo->Status.bits.Status_NoSecurity_b1 == 0)
                            {
                                SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc(DeviceIndex);
                                if (SecurityKey)
                                {
                                    unsigned char   AesDataOut[AES_ENC_DEC_BYTE];

                                    dxAESSW_Setkey( Aes128ctx_ble_dec, SecurityKey->value, AES_ENC_DEC_BIT, DXAES_DECRYPT );
                                    dxAESSW_CryptEcb( Aes128ctx_ble_dec, DXAES_DECRYPT, DeviceReportInfo->DkAdvertisementData.AdData, AesDataOut  );

                                    //Overwrite back the first 16 byte with decrypted data
                                    memcpy( DeviceReportInfo->DkAdvertisementData.AdData, AesDataOut, AES_ENC_DEC_BYTE);
                                    dxMemFree(SecurityKey);
                                }
                            }

                            EventReportObj_t* EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
                            EventReportObj->Header.IdentificationNumber = 0;
                            EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT;

                            EventReportObj->pData = (uint8_t*)DeviceReportInfo;

                            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
                            if (EventResult != DXOS_SUCCESS)
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Failed to send Event message back to Application\r\n" );
                                dxMemFree(DeviceReportInfo);
                                dxMemFree(EventReportObj);
                            }
                        }

                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_DATA_REPORT Does NOT support Multi-Paging adv read in BLEManager\r\n" );
                    }

                }

                TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            }

        }

    }
    else if(data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE)
    {
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR;
    }
    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobAddPeripheralDevicesToKeeperList( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;
    SecurityKeyCodeContainer_t* SecurityKey = (data->SecurityKeyCodeContainer.Size > 0) ? &data->SecurityKeyCodeContainer:NULL;

    int16_t AddedToIndex = 0;

    BLEDeviceKeeper_result_t KeeperResult = BLEDevKeeper_AddDeviceToKeeper (data->DeviceAddress, data->DeviceType, SecurityKey, data->DeviceFwVersion, &AddedToIndex);

    if (KeeperResult == DEV_KEEPER_DEVICE_ALREADY_IN_LIST)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Adding device that is already in keeper list, will ignore.. This could happend if application is trying to re-pairing already added devices\r\n");
        data->EventResult = DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED;
    }
    else
    {
        //Report back to application when new device is added to keeper list
        if (data->DeviceInfoObj)
        {
            DeviceInfoReport_t*     DevInfoObj = dxMemAlloc( "Device Info Obj Buff", 1, sizeof(DeviceInfoReport_t));

            DevInfoObj->DevHandlerID = AddedToIndex;

            DeviceAddress_t DevParseAddress = BLEDevParser_GetAddress(data->DeviceInfoObj);

            memcpy(DevInfoObj->DevAddress.Addr, DevParseAddress.pAddr, (DevParseAddress.AddrLen > B_ADDR_LEN_8) ? B_ADDR_LEN_8:DevParseAddress.AddrLen );

            DeviceManufaturerData_t DevManufacturerData = BLEDevParser_GetManufacturerData(data->DeviceInfoObj);

            DevInfoObj->rssi = DevManufacturerData.rssi;

            if (DevManufacturerData.ManufacturerDataLen)
            {

                BLEDevParser_GetDkAdvData(  &(DevInfoObj->DkAdvertisementData),
                                            &DevManufacturerData,
                                            (SecurityKey) ? SecurityKey->value:NULL,
                                            (SecurityKey) ? SecurityKey->Size:0);
            }

            data->pEventPayload = (uint8_t*)DevInfoObj;

        }
    }

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobRemoveALLPeripheralDevicesFromKeeperList( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobRemoveALLPeripheralDevicesFromKeeperList\r\n");

    BLEDevKeeper_RemoveAllDeviceFromKeeperList();

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobGetDeviceKeeperList( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%s\r\n", __FUNCTION__);

    uint8_t* pUpdatedPeripheralData = NULL;
    //uint16_t PeripheralCountRetrieved = 0;
    uint16_t TotalPeripheral = BLEDevKeeper_GetDeviceCountFromKeeperList();

    pUpdatedPeripheralData = dxMemAlloc( "DeviceKeeperHP", 1, sizeof(DeviceKeeperListReport_t) + TotalPeripheral * sizeof(DeviceGenericInfo_t));

    if (pUpdatedPeripheralData != NULL)
    {
        DeviceKeeperListReport_t *pDeviceKeeperListReport = (DeviceKeeperListReport_t *) pUpdatedPeripheralData;
        pDeviceKeeperListReport->DeviceCount = BLEDevKeeper_GetAllDeviceGenericInfoFromKeeperList((DeviceGenericInfo_t*)pDeviceKeeperListReport->GenericInfo, TotalPeripheral);

        if (pDeviceKeeperListReport->DeviceCount != TotalPeripheral)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%s !! TotalPeripheral(%d) != PeripheralCountRetrieved(%d)\r\n", __FUNCTION__, TotalPeripheral, pDeviceKeeperListReport->DeviceCount);
        }

        EventReportObj_t* EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
        EventReportObj->Header.IdentificationNumber = 0;
        EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST;
        EventReportObj->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
        EventReportObj->Header.SourceOfOrigin = CurrentJob.JobSourceOfOrigin;
        EventReportObj->Header.IdentificationNumber = CurrentJob.JobIdentificationNumber;
        EventReportObj->pData = pUpdatedPeripheralData;

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%s send DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST to Application\r\n", __FUNCTION__);

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL Missing Device Reporting Failed to send Event message back to Application\r\n" );
            dxMemFree(pUpdatedPeripheralData);
            dxMemFree(EventReportObj);
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%s !! DeviceKeeperHP memalloc failed!\r\n", __FUNCTION__);
    }

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobRemovePeripheralDevicesFromKeeperList( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    int16_t DeviceIndexInKeeper = BLEDevKeeper_FindDeviceIndexFromGivenAddress(data->DeviceAddress);

    if (DeviceIndexInKeeper != DEV_KEEPER_NOT_FOUND)
    {

        DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DeviceIndexInKeeper);

        if (DeviceReportInfo != NULL)
        {
            data->pEventPayload = (uint8_t*)DeviceReportInfo;
        }

        BLEDevKeeper_RemoveDeviceFromKeeper(data->DeviceAddress);
    }

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobPeripheralTransferDataOut( instance_data_t *data )
{

    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    int16_t                 TotalDataToTransferPayload = 0;
    DataTransferMsgHeader_t MsgHeader;

    memcpy(&MsgHeader, data->pJobPayload, sizeof(DataTransferMsgHeader_t));

    TotalDataToTransferPayload = MsgHeader.PayloadLen + sizeof(DataTransferMsgHeader_t);

    if (TotalDataToTransferPayload < MAXIMUM_DEVICE_JOB_DATA_SIZE)
    {
        const uint8_t*  pPayloadDataToTransfer = data->pJobPayload;
        while (TotalDataToTransferPayload > 0)
        {
            uint16_t CurrentTransferSize = (TotalDataToTransferPayload > DATA_TRANSFER_SIZE_PER_CHUNK) ? DATA_TRANSFER_SIZE_PER_CHUNK:TotalDataToTransferPayload;

            //Sleep a bit before each consecutive transfer
            dxTimeDelayMS(50);

            //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobPeripheralTransferDataOut CurrentTransferSize = %d\r\n", CurrentTransferSize);
            if (BLEDevCentralCtrl_PeripheralDataTransferWrite(pPayloadDataToTransfer, CurrentTransferSize) != DEV_CENTRAL_CTRL_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BLEDevCentralCtrl_PeripheralDataTransferWrite Failed (Check parameter)\r\n");
                TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
            }
            else
            {
                dxOS_RET_CODE PopResult;
                BlePeripheralDataTransfer_t* PopDataTransferMsgResponse = NULL;

                PopResult = dxQueueReceive( BleDevicePeripheralDataOutTransferReportQueue, &PopDataTransferMsgResponse, DEVICE_GENERIC_QUEUE_REPORT_WAIT_TIME_OUT_MS );
                if (PopResult == DXOS_SUCCESS)
                {
                    uint16_t Code = 0;
                    memcpy(&Code, PopDataTransferMsgResponse->Payload, sizeof(Code));

                    if (Code)
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING-InJobPeripheralTransferDataOut Rsp error = 0X%x\r\n", Code);
                        TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - InJobPeripheralTransferDataOut Failed to receive response from ble module\r\n");
                    TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
                }

                if (PopDataTransferMsgResponse)
                    dxMemFree(PopDataTransferMsgResponse);
            }

            if (TransitionState != DEV_MANAGER_ON_JOB_NEXT_STATE)
            {
                break;
            }
            else
            {
                TotalDataToTransferPayload -= CurrentTransferSize;

                if (TotalDataToTransferPayload)
                    pPayloadDataToTransfer += CurrentTransferSize;
            }
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - Failed to send PeripheralTransferDataOut due to total size exceed MAXIMUM_DEVICE_JOB_DATA_SIZE\r\n");
        TransitionState =DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
    }

    return TransitionState;
}

static BLEManager_GenJobProcess_t JobCleanUpProcess( instance_data_t *data, uint8_t CleanUpAsCentral )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "InJobCleanUp\r\n");

    if (CleanUpAsCentral)
    {
        uint8_t CloseLoopProcessDone = 0;
        dxTime_t CleanUpSystemTimeStart = dxTimeGetMS();

        //Make sure we disconnect first
        while (!CloseLoopProcessDone)
        {

            dxTime_t CurrentSystemTime = dxTimeGetMS();

            //We are only going to allow 30 seconds trials before we reset the ble central
            if ((CurrentSystemTime - CleanUpSystemTimeStart) < (30*SECONDS))
            {
                TransitionState = InJobCentralDisconnect( data );

                if (TransitionState == DEV_MANAGER_ON_JOB_STATE_UNCHANGE)
                {
                    dxTimeDelayMS(100);
                    continue;
                }
                else if ( (TransitionState != DEV_MANAGER_ON_JOB_NEXT_STATE) && (TransitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) )
                {
                    dxTimeDelayMS(100);
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: We may NOT be able to resolve InJobCleanUp with InJobCentralDisconnect\r\n");
                    continue;
                }
            }
            else
            {
                //Let's reset the BLE central in case it's not longer responding to any command
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: JobCleanUpProcess-BleCentral NOT responding\r\n");
                SetBLERoleToCentral(dxTRUE);
            }

            //If there is NO BLE job in the queue then we make sure to automatically start the scan.
            //otherwise we skip this step and let the job follow through to speed up the process
            if (dxQueueIsEmpty (BleDeviceJobQueue) == dxTRUE)
            {
                TransitionState = InJobStartScanDevice( data );

                if ( (TransitionState != DEV_MANAGER_ON_JOB_NEXT_STATE) && (TransitionState != DEV_MANAGER_ON_JOB_STATE_UNCHANGE) )
                {
                    dxTimeDelayMS(100);
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: We may NOT be able to resolve InJobStartScanDevice with InJobCentralDisconnect\r\n");
                    continue;
                }
            }

            CloseLoopProcessDone = 1;

        }
    }

    TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobCleanUpAsCentral (instance_data_t *data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCleanUpAsCentral (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    transitionState = JobCleanUpProcess (data, 1);
    CleanAllInJobReportQueue ();

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCleanUpAsCentral (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);

    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCleanUpAsRepeater (instance_data_t *data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCleanUpAsRepeater (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    transitionState = JobCleanUpProcess (data, data->JobResult);
    CleanAllInJobReportQueue ();

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCleanUpAsRepeater (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);

    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCleanUpAsPeripheral (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCleanUpAsPeripheral (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    transitionState = JobCleanUpProcess (data, 0);
    CleanAllInJobReportQueue ();

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCleanUpAsPeripheral (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);

    return transitionState;
}


static BLEManager_GenJobProcess_t InJobFwBlockWrite( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    if (BLEDevCentralCtrl_FwBlockWriteMesageSend(data->pJobPayload[0], &data->pJobPayload[1]) != DEV_CENTRAL_CTRL_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: Central FW Write Failed. May not recover\r\n");
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }
    else
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobFwDoneBootToApp( instance_data_t *data )
{
    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    if (BLEDevCentralCtrl_SystemSBLBootToApp() != DEV_CENTRAL_CTRL_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: Central FW Write Failed. May not recover\r\n");
        TransitionState = DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG;
    }
    else
    {
        TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    return TransitionState;

}


static BLEManager_GenJobProcess_t InJobConcurrentPeripheralCustomizeBdAddress (
    instance_data_t* data
) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    BLEDeviceCtrl_result_t devResult = DEV_CENTRAL_CTRL_SUCCESS;
    BleCentralConnect_t* bleBdAddress = (BleCentralConnect_t*) data->pJobPayload;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobConcurrentPeripheralCustomizeBdAddress (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    devResult = BLEDevCentralCtrl_CustomizeConcurrentPeripheralBdAddress (
        bleBdAddress
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Fail to call BLEDevCentralCtrl_CustomizeConcurrentPeripheralBdAddress (): devResult=%d\r\n", devResult);
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
    }

fail :

    return transitionState;
}


static BLEManager_GenJobProcess_t InJobConcurrentPeripheralDataOut (
    instance_data_t* data
) {

    BLEManager_GenJobProcess_t  TransitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
    ConcurrentDataTransferHeader_t* MsgHeader;
    uint8_t RetryCount = 0;

    MsgHeader = (ConcurrentDataTransferHeader_t*) data->pJobPayload;
    if ((MsgHeader->PayloadLength + sizeof (ConcurrentDataTransferHeader_t)) >= MAXIMUM_DEVICE_JOB_DATA_SIZE) {
        G_SYS_DBG_LOG_V ("WARNING - Failed to send PeripheralTransferDataOut due to total size exceed MAXIMUM_DEVICE_JOB_DATA_SIZE\r\n");
        goto fail;
    }

    // Retry until reach retry count limit
    while (RetryCount++ < MAXIMUM_CONCURRENT_PERIPHERAL_DATA_OUT_RETRY_COUNT) {
        int16_t TotalDataToTransferPayload = MsgHeader->PayloadLength + sizeof (ConcurrentDataTransferHeader_t);
        const uint8_t* pPayloadDataToTransfer = data->pJobPayload;

        while (TotalDataToTransferPayload > 0) {

            uint16_t CurrentTransferSize = (TotalDataToTransferPayload > DATA_TRANSFER_SIZE_PER_CHUNK) ? DATA_TRANSFER_SIZE_PER_CHUNK : TotalDataToTransferPayload;

            //Sleep a bit before each consecutive transfer
            dxTimeDelayMS (50);

            if (BLEDevCentralCtrl_ConcurrentPeripheralDataOut (pPayloadDataToTransfer, CurrentTransferSize) != DEV_CENTRAL_CTRL_SUCCESS) {
                G_SYS_DBG_LOG_V ("WARNING - BLEDevCentralCtrl_ConcurrentPeripheralDataOut Failed (Check parameter)\r\n");
                break;
            } else {
                dxOS_RET_CODE PopResult;
                BlePeripheralDataTransfer_t* PopDataTransferMsgResponse = NULL;

                PopResult = dxQueueReceive (BleDeviceConcurrentPeripheralDataOutTransferReportQueue, &PopDataTransferMsgResponse, DEVICE_GENERIC_QUEUE_REPORT_WAIT_TIME_OUT_MS);
                if (PopResult == DXOS_SUCCESS) {
                    uint16_t Code = 0;

                    memcpy (&Code, PopDataTransferMsgResponse->Payload, sizeof (Code));
                    dxMemFree (PopDataTransferMsgResponse);

                    // If get error code, retry
                    if (Code) {
                        G_SYS_DBG_LOG_V ("WARNING: DataOut Rsp error = 0x%x\r\n", Code);
                        // If fatal error code, retry will not work, just stop
                        if (Code > 0x10) {
                            RetryCount = MAXIMUM_CONCURRENT_PERIPHERAL_DATA_OUT_RETRY_COUNT;
                        }
                        break;
                    }
                } else {
                    G_SYS_DBG_LOG_V ("WARNING - InJobConcurrentPeripheralDataOut Failed to receive response from ble module\r\n");
                    break;
                }
            }

            TotalDataToTransferPayload -= CurrentTransferSize;
            if (TotalDataToTransferPayload) {
                pPayloadDataToTransfer += CurrentTransferSize;
            }
        }

        // Flush
        if (BLEDevCentralCtrl_ConcurrentPeripheralFlush () != DEV_CENTRAL_CTRL_SUCCESS) {
            G_SYS_DBG_LOG_V ("WARNING - BLEDevCentralCtrl_ConcurrentPeripheralFlush Failed\r\n");
        }

        // Check if all data has been transfered
        if (TotalDataToTransferPayload == 0) {
            TransitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
            break;
        }
    }

fail :

    return TransitionState;
}


static BLEManager_GenJobProcess_t InJobCentralConnectToRepeater (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralConnectToRepeater (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_CONNECTED) {
        transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;
    }

    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = BleRepeater_Connect (&data->DeviceAddress);
    }

exit :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralConnectToRepeater (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCentralLoadRepeaterHandle (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralLoadRepeaterHandle (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    transitionState = BleRepeater_PreloadHandle ();

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralLoadRepeaterHandle (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterState (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralCheckRepeaterState (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater has been removed
    if (BLEDevKeeper_GetRepeaterIndex () == DEV_KEEPER_NOT_FOUND) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    // Check if the repeater is in active state
    if (!BleRepeater_IsActive ()) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
    }

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    transitionState = DEV_MANAGER_ON_JOB_NEXT_STATE;

exit :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralCheckRepeaterState (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterWatchList (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralCheckRepeaterWatchList (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    // Check if the register watch list is required
    transitionState  = BleRepeater_PollToRegisterWatchList ();

exit :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralCheckRepeaterWatchList (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCentralCheckRepeaterOpenPairing (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralCheckRepeaterOpenPairing (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    // Check if the open pairing mode is on
    transitionState = BleRepeater_PollToOpenPairing ();

exit :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralCheckRepeaterOpenPairing (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}


static BLEManager_GenJobProcess_t InJobCentralGetRepeaterAdvertiseInfo (instance_data_t* data) {
    BLEManager_GenJobProcess_t transitionState = DEV_MANAGER_ON_JOB_STATE_UNCHANGE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Enter InJobCentralGetRepeaterAdvertiseInfo (data={CentralState=%d})\r\n", data->CurrentCentralStatus->CentralState);

    // Check if the repeater is is disconnect --> idle means disconnectd.
    if (data->CurrentCentralStatus->CentralState == BLE_STATE_IDLE) {
        transitionState = DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR;
        goto exit;
    }

    // Update advertise info from repeater
    transitionState = BleRepeater_UpdateAdvertiseInfo (dxFALSE);

exit :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Leave InJobCentralGetRepeaterAdvertiseInfo (data={CentralState=%d}), success\r\n", data->CurrentCentralStatus->CentralState);
    return transitionState;
}



/******************************************************
 *              LOCAL Function Definitions
 ******************************************************/

//Here we check if Ble central is stuck due to state recovery issue or UART error issue that cause BLE central to stuck in same state
//for too long. We will do minimal recovery (eg, set to scanning state if its on idle)
static void CentralStatusCheckHandler(void* arg)
{

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "CentralStatusCheckHandler\r\n");

    static      uint16_t NoResponseCount = 0;
    dxTime_t    CurrentSystemTime = dxTimeGetMS();

    const BleCentralStates_t* ConnectionState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

    // Check if the repeater is in active state
    if (BleRepeater_IsActive ()) {
        return;
    }

    //If we are stuck in same state for too long then we cross check possible issue and try to recover
    if (((CurrentSystemTime - ConnectionState->UpdatedTime) >= (CENTRAL_STATUS_CHECK_SESSION_TIME_INTERVAL)) || bRestartBLEFlag)
    {
        //If Ble central job is empty and there is no current job in execution then we check why we are stuck in same state
        //otherwise we ignore since it's possible we could be in connection state for more than 30 seconds..Eg. FW update or accessing devices.
        if ((dxQueueIsEmpty (BleDeviceJobQueue) == dxTRUE) && (CurrentJob.JobType == DEV_MANAGER_NO_JOB))
        {
            if ((ConnectionState->CentralState == BLE_STATE_IDLE)
                || (ConnectionState->CentralState == BLE_STATE_SCANNING)
                || (ConnectionState->CentralState >= BLE_STATE_UPPER_LIMIT)
               )
            {
                //check if BLE already failed to response.. if so, we should reset BLE and start the process all-over
                NoResponseCount++;
                if ((NoResponseCount >= 3) || bRestartBLEFlag)
                {
                    NoResponseCount = 0; //Reset flag
                    ResetBleModule ();
                    BLEManager_WhiteList_AddFromKeeperList ();
                }

                //We do simple quick recovery here by sending start scan to BLE here again.
                BLEDevCentralCtrl_StartScan(4000);
                if(bRestartBLEFlag)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE: bRestartBLEFlag is set... Auto recovering\r\n");
                    bRestartBLEFlag = dxFALSE;
                }
                else
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE: Ble central state (%d) stuck for too long... Auto recovering\r\n", ConnectionState->CentralState);
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Reminder:BleCentralState(%d) unchanged for too long (issue?)\r\n", ConnectionState->CentralState);
            }
        }

    }
    else
    {
        NoResponseCount = 0;
    }


    //In case we missed the SystemInfo due to UART error, we are going to request it again
    if (SystemInfoReceived == dxFALSE)
    {
        BLEDevCentralCtrl_SystemInfoUpdateReq();
    }

    //Re-Start the timer again
    dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
    if (dxTimerStart(CentralStatusSessionTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unable to re-start central status session timer! Possible issue - Stability\r\n");
    }

}


static dxBOOL PushCentralStateReportToInJobQueue(BleCentralStates_t* CentralStateRspMsg)
{
    //We only need to report to InJobQueue if we are in the proccess of Job handling, otherwise we don't need and should not send it to InJobQueue
    if (CurrentJob.JobType == DEV_MANAGER_NO_JOB)
    {
        return dxFALSE;
    }

    if (BleDeviceInJobCentralStateReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues)
        while (dxQueueIsFull(BleDeviceInJobCentralStateReportQueue) == dxTRUE)
        {
            BleCentralStates_t* PopCentralStateResponse = NULL;
            dxQueueReceive( BleDeviceInJobCentralStateReportQueue, &PopCentralStateResponse, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT );

            if (PopCentralStateResponse)
                dxMemFree(PopCentralStateResponse);

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: PushCentralStateReportToInJobQueue QueuFull, we start dumping to clear queue\r\n");

        }

        if (dxQueueSend( BleDeviceInJobCentralStateReportQueue, &CentralStateRspMsg, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT ) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobCentralStateReportQueue failed to send\r\n");
            return dxFALSE;
        }
    }

    return dxTRUE;
}

static dxBOOL PushDiscoveryCharReportToInJobQueue(BleDeviceCharDiscoveryRspMsg_t* CharDiscvMsg)
{

    //We only need to report to InJobQueue if we are in the proccess of Job handling, otherwise we don't need and should not send it to InJobQueue
    if (CurrentJob.JobType == DEV_MANAGER_NO_JOB)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING-PushDiscoveryCharReportToInJobQueue not expecting\r\n" );
        return dxFALSE;
    }

    if (BleDeviceInJobCharDiscvReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).
        while (dxQueueIsFull(BleDeviceInJobCharDiscvReportQueue) == dxTRUE)
        {
            BleDeviceCharDiscoveryRspMsg_t* PopCharDiscoveryResponse = NULL;

            dxQueueReceive( BleDeviceInJobCharDiscvReportQueue, &PopCharDiscoveryResponse, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT );

            if (PopCharDiscoveryResponse)
            {
                dxMemFree(PopCharDiscoveryResponse);
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobCharDiscvReportQueue is full\r\n" );
        }

        if (dxQueueSend( BleDeviceInJobCharDiscvReportQueue, &CharDiscvMsg, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT ) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobCharDiscvReportQueue failed to send\r\n");
            return dxFALSE;
        }
    }

    return dxTRUE;
}


static dxBOOL PushWriteCharReportToInJobQueue(BleDeviceWriteCharRspMsg_t* WriteMsg)
{
    //We only need to report to InJobQueue if we are in the proccess of Job handling, otherwise we don't need and should not send it to InJobQueue
    if (CurrentJob.JobType == DEV_MANAGER_NO_JOB)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING-PushWriteCharReportToInJobQueue not expecting\r\n" );
        return dxFALSE;
    }

    if (BleDeviceInJobWriteCharReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).
        while (dxQueueIsFull(BleDeviceInJobWriteCharReportQueue) == dxTRUE)
        {
            BleDeviceWriteCharRspMsg_t* PopWriteCharResponse = NULL;

            dxQueueReceive( BleDeviceInJobWriteCharReportQueue, &PopWriteCharResponse, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT );

            if (PopWriteCharResponse)
            {
                dxMemFree(PopWriteCharResponse);
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobWriteCharReportQueue is full\r\n");
        }

        if (dxQueueSend( BleDeviceInJobWriteCharReportQueue, &WriteMsg, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT )  != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobWriteCharReportQueue failed to send\r\n");
            return dxFALSE;
        }

    }

    return dxTRUE;
}

static void PushPeripheralRoleDataTransferReportToInJobQueue(BlePeripheralDataTransfer_t* DataTransferMsg)
{

    if (BleDevicePeripheralDataOutTransferReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).
        while (dxQueueIsFull(BleDevicePeripheralDataOutTransferReportQueue) == dxTRUE)
        {
            BlePeripheralDataTransfer_t* PopDataTransferMsgResponse = NULL;

            dxQueueReceive( BleDevicePeripheralDataOutTransferReportQueue, &PopDataTransferMsgResponse, DXOS_WAIT_FOREVER );

            if (PopDataTransferMsgResponse)
            {
                dxMemFree(PopDataTransferMsgResponse);
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDevicePeripheralDataOutTransferReportQueue is full\r\n");
        }

        dxQueueSend( BleDevicePeripheralDataOutTransferReportQueue, &DataTransferMsg, DXOS_WAIT_FOREVER );

    }

}


static void PushConcurrentPeripheralRoleDataTransferReportToInJobQueue(BlePeripheralDataTransfer_t* DataTransferMsg)
{

    if (BleDeviceConcurrentPeripheralDataOutTransferReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).
        while (dxQueueIsFull(BleDeviceConcurrentPeripheralDataOutTransferReportQueue) == dxTRUE)
        {
            BlePeripheralDataTransfer_t* PopDataTransferMsgResponse = NULL;
            dxQueueReceive( BleDeviceConcurrentPeripheralDataOutTransferReportQueue, &PopDataTransferMsgResponse, DXOS_WAIT_FOREVER );

            if (PopDataTransferMsgResponse)
            {
                dxMemFree(PopDataTransferMsgResponse);
            }

            G_SYS_DBG_LOG_V("WARNING - BleDeviceConcurrentPeripheralDataOutTransferReportQueue is full\r\n");
        }

        dxQueueSend( BleDeviceConcurrentPeripheralDataOutTransferReportQueue, &DataTransferMsg, DXOS_WAIT_FOREVER );

    } else {

        dxMemFree (DataTransferMsg);

    }

}


static dxBOOL PushReadCharReportToInJobQueue(BleDeviceReadCharRspMsg_t* WriteMsg)
{
    //We only need to report to InJobQueue if we are in the proccess of Job handling, otherwise we don't need and should not send it to InJobQueue
    if (CurrentJob.JobType == DEV_MANAGER_NO_JOB)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING-PushReadCharReportToInJobQueue not expecting\r\n" );
        return dxFALSE;
    }

    if (BleDeviceInJobReadCharReportQueue)
    {
        //If Queue is full we dump the oldest one
        //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
        //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).
        while (dxQueueIsFull(BleDeviceInJobReadCharReportQueue) == dxTRUE)
        {
            BleDeviceReadCharRspMsg_t* PopReadCharResponse = NULL;

            dxQueueReceive( BleDeviceInJobReadCharReportQueue, &PopReadCharResponse, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT );

            if (PopReadCharResponse)
            {
                dxMemFree(PopReadCharResponse);
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobReadCharReportQueue is full\r\n");
        }

        if (dxQueueSend( BleDeviceInJobReadCharReportQueue, &WriteMsg, IN_JOB_STATE_REPORT_CLEAN_QUEUE_SEND_WAIT_TIMEOUT ) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - BleDeviceInJobReadCharReportQueue failed to send\r\n");
            return dxFALSE;
        }
    }

    return dxTRUE;
}


void CleanAllInJobReportQueue()
{

    //If Queue is not empty we dump all to clean it up
    //TODO: Check if it's safe to clear queue here as other thread might pop the same queue. Looks like its safe since WICED does not configure
    //      ThreadX thread as preemptive (Regardless of task priority current running Task cannot be interrupted until it's put to sleep or waiting for event/queues).

    while (dxQueueIsEmpty(BleDeviceInJobCentralStateReportQueue) != dxTRUE)
    {
        BleCentralStates_t* PopCentralStateResponse = NULL;

        dxQueueReceive( BleDeviceInJobCentralStateReportQueue, &PopCentralStateResponse, DXOS_WAIT_FOREVER );

        if (PopCentralStateResponse)
        {
            dxMemFree(PopCentralStateResponse);
        }

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE - BleDeviceInJobCentralStateReportQueue is not empty\r\n");
    }

    while (dxQueueIsEmpty(BleDeviceInJobCharDiscvReportQueue) != dxTRUE)
    {
        BleDeviceCharDiscoveryRspMsg_t* PopCharDiscoveryResponse = NULL;

        dxQueueReceive( BleDeviceInJobCharDiscvReportQueue, &PopCharDiscoveryResponse, DXOS_WAIT_FOREVER );

        if (PopCharDiscoveryResponse)
        {
            dxMemFree(PopCharDiscoveryResponse);
        }

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE - BleDeviceInJobCharDiscvReportQueue is not empty\r\n");
    }


    while (dxQueueIsEmpty(BleDeviceInJobWriteCharReportQueue) != dxTRUE)
    {
        BleDeviceWriteCharRspMsg_t* PopWriteCharResponse = NULL;

        dxQueueReceive( BleDeviceInJobWriteCharReportQueue, &PopWriteCharResponse, DXOS_WAIT_FOREVER );

        if (PopWriteCharResponse)
        {
            dxMemFree(PopWriteCharResponse);
        }

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE - BleDeviceInJobWriteCharReportQueue is not empty\r\n");
    }

    while (dxQueueIsEmpty(BleDeviceInJobReadCharReportQueue) != dxTRUE)
    {
        BleDeviceReadCharRspMsg_t* PopReadCharResponse = NULL;

        dxQueueReceive( BleDeviceInJobReadCharReportQueue, &PopReadCharResponse, DXOS_WAIT_FOREVER );

        if (PopReadCharResponse)
        {
            dxMemFree(PopReadCharResponse);
        }

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE - BleDeviceInJobReadCharReportQueue is not empty\r\n");
    }

    while (dxQueueIsEmpty(BleDevicePeripheralDataOutTransferReportQueue) != dxTRUE)
    {
        BlePeripheralDataTransfer_t* CentralPeripheralRoleDataOutRsp = NULL;

        dxQueueReceive( BleDevicePeripheralDataOutTransferReportQueue, &CentralPeripheralRoleDataOutRsp, DXOS_WAIT_FOREVER );

        if (CentralPeripheralRoleDataOutRsp)
        {
            dxMemFree(CentralPeripheralRoleDataOutRsp);
        }

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NOTE - BleDevicePeripheralDataOutTransferReportQueue is not empty\r\n");
    }

    while (dxQueueIsEmpty(BleDeviceConcurrentPeripheralDataOutTransferReportQueue) != dxTRUE)
    {
        BlePeripheralDataTransfer_t* CentralPeripheralRoleDataOutRsp = NULL;

        dxQueueReceive( BleDeviceConcurrentPeripheralDataOutTransferReportQueue, &CentralPeripheralRoleDataOutRsp, DXOS_WAIT_FOREVER );

        if (CentralPeripheralRoleDataOutRsp)
        {
            dxMemFree(CentralPeripheralRoleDataOutRsp);
        }

        //G_SYS_DBG_LOG_V("NOTE - BleDeviceConcurrentPeripheralDataOutTransferReportQueue is not empty\r\n");
    }

}


static uint16_t InJobStageIndexLookUp(BLEManager_GenJobProcess_t StateToLookFor, BLEManager_GenJobProcess_t* InJobStateSequenceLookUp, uint16_t SequenceArrayLen)
{
    uint16_t index = 0;
    uint16_t i = 0;

    for (i = 0; i < SequenceArrayLen; i++)
    {
        if (InJobStateSequenceLookUp[i] == DEV_MANAGER_ON_JOB_NULL)
            break;

        if (InJobStateSequenceLookUp[i] == StateToLookFor)
        {
            index = i;
            break;
        }

    }

    return index;
}


static void SendBleResetNotification (BleResetStatus status) {
    BleResetStatus* eventReportData = dxMemAlloc (
        "BleResetStatus",
        1,
        sizeof (BleResetStatus)
    );

    bleResetStatus = status;
    *eventReportData = bleResetStatus;

    EventReportObj_t* eventReport = dxMemAlloc (
        "EventReportObj_t",
        1,
        sizeof (EventReportObj_t)
    );

    eventReport->Header.IdentificationNumber = 0;
    eventReport->Header.ReportEvent = DEV_MANAGER_EVENT_BLE_RESET_REPORT;
    eventReport->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
    eventReport->pData = (uint8_t*) eventReportData;

    dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, eventReport);
    if (eventResult != DXOS_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL Ble Reset Notification report - Failed to send Event message back to Application\r\n");
        dxMemFree (eventReportData);
        dxMemFree (eventReport);
    }
}


static void CollectConcurrentPeripheralDataInPayload (
    dxBOOL dataValid,
    BlePeripheralDataTransfer_t* dataTransfer
) {
    static uint8_t* payloadCollectionBuffer = NULL;
    static uint8_t* payloadCollectionBufferWorkPtr = NULL;
    static uint8_t  payloadType;
    static uint8_t  payloadReserved;
    static uint16_t payloadLen;
    static uint16_t payloadLenLeft;
    uint16_t preamble;
    uint8_t* payload;
    dxBOOL   payloadCleanup = dxFALSE;

    if (!dataValid) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR, "WARNING:(Line=%d) Peripheral Role Data Transfer error, message received in progress will be erased!\r\n", __LINE__);
        payloadCleanup = dxTRUE;
        goto done;
    }

    payload = dataTransfer->Payload;
    preamble = BUILD_UINT16 (payload[0], payload[1]);

    if (preamble == DATA_TRANSFER_PREAMBLE) {
        if (payloadCollectionBuffer) {
            dxMemFree (payloadCollectionBuffer);
            payloadCollectionBuffer = NULL;
            payloadCollectionBufferWorkPtr = NULL;

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR, "WARNING:(Line=%d) Peripheral Role Message In was not fully received before new message is found!\r\n", __LINE__);
        }

        payloadType = payload[2];
        payloadReserved = payload[3];
        payloadLen = BUILD_UINT16 (payload[4], payload[5]);

        if (payloadLen) {
            if (payloadLen > (DATA_TRANSFER_SIZE_PER_CHUNK - 6)) {
                payloadCollectionBuffer = dxMemAlloc ("Peripheral Role Payload collection Buff", 1, payloadLen);

                payloadCollectionBufferWorkPtr = payloadCollectionBuffer;
                memcpy(payloadCollectionBufferWorkPtr, &payload[6], (DATA_TRANSFER_SIZE_PER_CHUNK - 6));
                payloadCollectionBufferWorkPtr += (DATA_TRANSFER_SIZE_PER_CHUNK - 6);
                payloadLenLeft = payloadLen - (DATA_TRANSFER_SIZE_PER_CHUNK - 6);
            }
            if (payloadLen > CONCURRENT_PERIPHERAL_DATA_IN_MAX) {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR, "Invalid payload length\r\n");
                payloadCleanup = dxTRUE;
                goto done;
            }
        }

        //We don't need to collect more payload, this is all for this message so we process it
        if (payloadCollectionBuffer == NULL) {
            EventReportObj_t* dataInEvent = dxMemAlloc ("Concurrent DataIn Buff", 1, sizeof (EventReportObj_t));

            dataInEvent->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD;
            dataInEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;

            BleConcurrentPeripheralDataTransfer_t* bleCcpDataTransfer = dxMemAlloc ("BleConcurrentPeripheralDataTransfer_t", 1, sizeof (BleConcurrentPeripheralDataTransfer_t));

            bleCcpDataTransfer->payloadLen = payloadLen;
            memcpy (bleCcpDataTransfer->payload, &payload[6], payloadLen);
            dataInEvent->pData = (uint8_t*) bleCcpDataTransfer;

            if (RegisteredEventFunction) {
                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, dataInEvent );
                if (EventResult != DXOS_SUCCESS) {
                    G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                    if (dataInEvent->pData) {
                        dxMemFree (dataInEvent->pData);
                    }
                    dxMemFree (dataInEvent);
                }
            }
        }
    } else if (payloadCollectionBuffer) {
        uint8_t SizeToCopy = (payloadLenLeft > DATA_TRANSFER_SIZE_PER_CHUNK) ? DATA_TRANSFER_SIZE_PER_CHUNK : payloadLenLeft;

        memcpy (
            payloadCollectionBufferWorkPtr,
            &payload[0],
            SizeToCopy
        );

        if (payloadLenLeft < SizeToCopy) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR, "WARNING:(Line=%d) Peripheral Role Data Transfer payloadLenLeft is less than SizeToCopy\r\n", __LINE__);
            payloadCleanup = dxTRUE;
            goto done;
        }

        payloadLenLeft -= SizeToCopy;
        if (payloadLenLeft) {
            payloadCollectionBufferWorkPtr += SizeToCopy;
        } else {
            EventReportObj_t* dataInEvent = dxMemAlloc ("Concurrent DataIn Buff", 1, sizeof (EventReportObj_t));

            dataInEvent->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD;
            dataInEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;

            BleConcurrentPeripheralDataTransfer_t* bleCcpDataTransfer = dxMemAlloc ("BleConcurrentPeripheralDataTransfer_t", 1, sizeof (BleConcurrentPeripheralDataTransfer_t));

            bleCcpDataTransfer->payloadLen = payloadLen;
            memcpy (bleCcpDataTransfer->payload, payloadCollectionBuffer, payloadLen);
            dataInEvent->pData = (uint8_t*) bleCcpDataTransfer;

            if (RegisteredEventFunction) {
                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, dataInEvent );
                if (EventResult != DXOS_SUCCESS) {
                    G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                    if (dataInEvent->pData) {
                        dxMemFree (dataInEvent->pData);
                    }
                    dxMemFree (dataInEvent);
                }
            }
            payloadCleanup = dxTRUE;
            goto done;
        }
    }

done :

    if (payloadCleanup == dxTRUE) {
        if (payloadCollectionBuffer) {
            dxMemFree (payloadCollectionBuffer);
        }
        payloadCollectionBuffer = NULL;
        payloadCollectionBufferWorkPtr = NULL;
        payloadType = 0;
        payloadLen = 0;
        payloadLenLeft = 0;
    }
}


static unsigned char IsTimesUp(dxTime_t SystemTimeUpTime)
{
    unsigned char TimeIsUp = 0;

    if (SystemTimeUpTime)
    {
        dxTime_t CurrentSystemTime = dxTimeGetMS();
        if (CurrentSystemTime > SystemTimeUpTime)
            TimeIsUp = 1;
    }

    return TimeIsUp;
}



BLEManager_GenJobProcess_t InJobSequence[MAXIMUM_DEVICE_IN_JOB_SEQUENCE_ALLOWED];
uint16_t CurrentInJobStageIndex;


static void _BleManager_CheckDeviceStatus (dxTime_t* NextTime) {
    dxTime_t CurrentTime;
    dxBOOL bFoundNoRespDevice = dxFALSE;
    uint16_t DevIndex = 0;

    //NOTE: We are only going to check the "presence" of devices by checking the last scanned updated payload from keepers
    //      and report to JobManager. Any other updates such as battery, payload is managed by JobManager.

    //Set the priority back to normal after the BLE access
    //dxTaskPrioritySet(BleManagerMainProcess_thread, BLE_MANAGER_PROCESS_THREAD_PRIORITY);

    CurrentTime = dxTimeGetMS ();

    if (time_before (CurrentTime, *NextTime)) {
        goto exit;
    }

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "_BleManager_CheckDeviceStatus\r\n");

    for (DevIndex = 0; DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED; DevIndex++) {
        const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

        if (DeviceInfoKp != NULL) {
            //NOTE: Before we compare we need to make sure CurrentTime is greater than DeviceInfoKp->ManufacturerAdvDataLastUpdatedSystemTime
            //      It is possible that DeviceInfoKp->ManufacturerAdvDataLastUpdatedSystemTime time is greater since the priority of this update is much
            //      higher than current!
            dxTime_t LastUpdatedTime = DeviceInfoKp->ManufacturerAdvDataLastUpdatedSystemTime;

            if ((LastUpdatedTime) && (LastUpdatedTime < CurrentTime)) {

                //Check if the device is missing by checking the last received broadcast time
                if (time_after (CurrentTime, LastUpdatedTime + DEVICE_MISSING_REPORT_TIME_MS)) {

                    DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DevIndex);

                    if (DeviceReportInfo) {
                        DeviceReportInfo->rssi = BLE_RSSI_MISSING; //Overwrite: Set RSSI to 0 which means the device is missing

                        EventReportObj_t* EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
                        EventReportObj->Header.IdentificationNumber = 0;
                        EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT;
                        EventReportObj->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                        EventReportObj->pData = (uint8_t*)DeviceReportInfo;

                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER,
                                          G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                          "Device (pid=0x%x, did=%d, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x) missing, reporting to application\r\n",
                                          DeviceReportInfo->DkAdvertisementData.Header.ProductID,
                                          DevIndex,
                                          DeviceInfoKp->DevAddress.pAddr[7],
                                          DeviceInfoKp->DevAddress.pAddr[6],
                                          DeviceInfoKp->DevAddress.pAddr[5],
                                          DeviceInfoKp->DevAddress.pAddr[4],
                                          DeviceInfoKp->DevAddress.pAddr[3],
                                          DeviceInfoKp->DevAddress.pAddr[2],
                                          DeviceInfoKp->DevAddress.pAddr[1],
                                          DeviceInfoKp->DevAddress.pAddr[0]);

                        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
                        if (EventResult != DXOS_SUCCESS) {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL Missing Device Reporting Failed to send Event message back to Application\r\n");
                            dxMemFree(EventReportObj);
                            dxMemFree(DeviceReportInfo);
                        }
                    }

                } else if (time_after (CurrentTime, LastUpdatedTime + DEVICE_STATUS_CHECK_INTERVAL)) {

                    //NOTE: this section checks whether if TI BLE central cannot receive any more adv data. If can't, reset TI BLE central
                    DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DevIndex);

                    if (DeviceReportInfo) {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER,
                                          G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                          "Device (pid=0x%x, did=%d, addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x) no response for %d ms, try to restart BLE\r\n",
                                          DeviceReportInfo->DkAdvertisementData.Header.ProductID,
                                          DevIndex,
                                          DeviceInfoKp->DevAddress.pAddr[7],
                                          DeviceInfoKp->DevAddress.pAddr[6],
                                          DeviceInfoKp->DevAddress.pAddr[5],
                                          DeviceInfoKp->DevAddress.pAddr[4],
                                          DeviceInfoKp->DevAddress.pAddr[3],
                                          DeviceInfoKp->DevAddress.pAddr[2],
                                          DeviceInfoKp->DevAddress.pAddr[1],
                                          DeviceInfoKp->DevAddress.pAddr[0],
                                          (CurrentTime - DeviceInfoKp->ManufacturerAdvDataLastUpdatedSystemTime));

                        bFoundNoRespDevice = dxTRUE;
                        dxMemFree(DeviceReportInfo);
                    }

                }
            }

        }
    }

    if (bFoundNoRespDevice) {
        bRestartBLEFlag = dxTRUE;
        deviceGoingToMissing = dxTRUE;
    } else {
        deviceGoingToMissing = dxFALSE;
    }

    *NextTime = CurrentTime + DEVICE_STATUS_CHECK_INTERVAL;

exit :

    return;
}


static TASK_RET_TYPE Ble_Manager_Process_thread_main( TASK_PARAM_TYPE arg )
{
    dxTime_t NextStatusCheckSystemTime;
    static instance_data_t InJobInstanceData;
    dxBOOL bCleanCurrentJob;
    BLEManager_GenJobProcess_t JobState;

    CurrentJob.JobType = DEV_MANAGER_NO_JOB;
    NextStatusCheckSystemTime = dxTimeGetMS () + DEVICE_STATUS_CHECK_INTERVAL;

    // Allocate memory for address in advance
    InJobInstanceData.DeviceAddress.pAddr = dxMemAlloc ("Address Name Buff", 1, B_ADDR_LEN_8); //Always allocate max len even if addr len is 6 bytes

    while (1)
    {
        bCleanCurrentJob = dxFALSE;

        if (CurrentJob.JobType == DEV_MANAGER_NO_JOB)
        {

            BleDeviceJobQueue_t *pGetJobBuff = NULL;

            if (dxQueueReceive (BleDeviceJobQueue, &pGetJobBuff, DEVICE_STATUS_CHECK_INTERVAL / 2) == DXOS_SUCCESS)
            {
                if (pGetJobBuff)
                {
                    //We are going to set BLE manager to high priority during BLE access... Purpose is to improve access failure rate (still need to verify is this helps at all)
                    //dxTaskPrioritySet(BleManagerMainProcess_thread, BLE_MANAGER_ACCESS_PROCESS_THREAD_PRIORITY);

                    InJobInstanceData.JobResult = DEV_MANAGER_TASK_SUCCESS;
                    memcpy(&CurrentJob, pGetJobBuff, sizeof(BleDeviceJobQueue_t));
                    dxMemFree(pGetJobBuff);

                    //Calculate and set the time out time of this job when it's about to start
                    dxTime_t CurrentSystemTime = dxTimeGetMS();
                    CurrentJob.JobTimeOutTime += CurrentSystemTime;

                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Ble_Manager_Process_thread_main JobType = %d\r\n", CurrentJob.JobType);

                    //READ ME - Building Job rules
                    //RULES 1 - DEV_MANAGER_ON_JOB_START must be first
                    //RULES 2 - Do not assume manager will be in scanning or any other state, If Job require scan to start with make sure to include scan start job in the sequence.
                    //RULES 3 - DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL (if current role is central) or
                    //          DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER (if current role is repeater)
                    //          DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL (if current role is peripheral) or
                    //          must be last of valid state sequence
                    //RULES 4 - ALWAYS to include DEV_MANAGER_ON_JOB_NULL at the end of the sequence
                    switch(CurrentJob.JobType) {

                        case DEV_MANAGER_JOB_ACCESS_WRITE_FOLLOW_BY_ADV_READ_UPDATE_DEVICE :
                        case DEV_MANAGER_JOB_ACCESS_WRITE_DEVICE :
                        {
                            DeviceAccess_t DevAccessData;
                            memcpy(&DevAccessData, CurrentJob.JobData, sizeof(DeviceAccess_t));

                            const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevAccessData.DevHandlerID);

                            if (DeviceInfoKp)
                            {
                                if (CurrentJob.JobType == DEV_MANAGER_JOB_ACCESS_WRITE_DEVICE)
                                {
                                    BLEManager_GenJobProcess_t DeviceWriteAndReadAccessInJobSequence[] = {  DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                                DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
                                                                                                DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL,
                                                                                                DEV_MANAGER_ON_JOB_NULL};

                                    //Push to our working in job sequencer
                                    memcpy(InJobSequence, DeviceWriteAndReadAccessInJobSequence, sizeof(DeviceWriteAndReadAccessInJobSequence));
                                }
                                else //Write flow by payload read + update
                                {
                                    BLEManager_GenJobProcess_t DeviceWriteAccessInJobSequence[] = { DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                                DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
                                                                                                DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, DEV_MANAGER_ON_JOB_DATATYPE_PAYLOAD_READ,
                                                                                                DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                                    //Push to our working in job sequencer
                                    memcpy(InJobSequence, DeviceWriteAccessInJobSequence, sizeof(DeviceWriteAccessInJobSequence));

                                }

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                InJobInstanceData.DeviceAddress.AddrLen = DeviceInfoKp->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = DeviceInfoKp->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfoKp->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                memset (InJobInstanceData.MainCharWriteData.DataWrite, 0, sizeof (InJobInstanceData.MainCharWriteData.DataWrite));
                                InJobInstanceData.MainCharWriteData.UUIDNeed128bitConversion = DevAccessData.NeedConvertTo128bit;
                                InJobInstanceData.MainCharWriteData.ServiceUUID = DevAccessData.ServiceUUID;
                                InJobInstanceData.MainCharWriteData.CharacteristicUUID = DevAccessData.CharacteristicUUID;
                                InJobInstanceData.MainCharWriteData.DataWriteLen = DevAccessData.WriteDataLen;
                                memcpy (InJobInstanceData.MainCharWriteData.DataWrite, DevAccessData.WriteData, InJobInstanceData.MainCharWriteData.DataWriteLen);
                                //work-around
                                if (DevAccessData.DevHandlerID == BLEDevKeeper_GetRepeaterIndex ()) {
                                    InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Cannot find DeviceInfoKp for DevMgr Job(%d)\r\n", CurrentJob.JobType);
                                bCleanCurrentJob = dxTRUE;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_AGGREGATION_UPDATE : {
                            DeviceAccess_t DevAccessData;
                            memcpy(&DevAccessData, CurrentJob.JobData, sizeof(DeviceAccess_t));

                            const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevAccessData.DevHandlerID);

                            if (DeviceInfoKp)
                            {
                                BLEManager_GenJobProcess_t DeviceReadAggregatedUpdateAccessInJobSequence[] = {  DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                            DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
                                                                                            DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ_AGGREGATED_UPDATE,
                                                                                            DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                                //Push to our working in job sequencer
                                memcpy(InJobSequence, DeviceReadAggregatedUpdateAccessInJobSequence, sizeof(DeviceReadAggregatedUpdateAccessInJobSequence));

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                InJobInstanceData.DeviceAddress.AddrLen = DeviceInfoKp->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = DeviceInfoKp->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfoKp->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                memset (InJobInstanceData.MainCharReadData.DataRead, 0, sizeof (InJobInstanceData.MainCharReadData.DataRead));
                                InJobInstanceData.MainCharReadData.UUIDNeed128bitConversion = DevAccessData.NeedConvertTo128bit;
                                InJobInstanceData.MainCharReadData.ServiceUUID = DevAccessData.ServiceUUID;
                                InJobInstanceData.MainCharReadData.CharacteristicUUID = DevAccessData.CharacteristicUUID;
                                InJobInstanceData.MainCharReadData.DataReadLen = 0;
                                if (DevAccessData.DevHandlerID == BLEDevKeeper_GetRepeaterIndex ()) {
                                    InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Cannot find DeviceInfoKp for DevMgr Job(%d)\r\n", CurrentJob.JobType);
                                bCleanCurrentJob = dxTRUE;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_DATA_REPORT : {
                            DeviceAccess_t DevAccessData;
                            memcpy(&DevAccessData, CurrentJob.JobData, sizeof(DeviceAccess_t));

                            const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevAccessData.DevHandlerID);

                            if (DeviceInfoKp)
                            {
                                BLEManager_GenJobProcess_t DeviceReadAggregatedUpdateAccessInJobSequence[] = {DEV_MANAGER_ON_JOB_START,
                                                                                                              DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                                              DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION,
                                                                                                              DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
                                                                                                              DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_READ,
                                                                                                              DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL,
                                                                                                              DEV_MANAGER_ON_JOB_NULL};

                                //Push to our working in job sequencer
                                memcpy(InJobSequence, DeviceReadAggregatedUpdateAccessInJobSequence, sizeof(DeviceReadAggregatedUpdateAccessInJobSequence));

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                //We would like to report this job as event DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT
                                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT;

                                InJobInstanceData.DeviceAddress.AddrLen = DeviceInfoKp->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = DeviceInfoKp->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfoKp->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                memset (InJobInstanceData.MainCharReadData.DataRead, 0, sizeof (InJobInstanceData.MainCharReadData.DataRead));
                                InJobInstanceData.MainCharReadData.UUIDNeed128bitConversion = DevAccessData.NeedConvertTo128bit;
                                InJobInstanceData.MainCharReadData.ServiceUUID = DevAccessData.ServiceUUID;
                                InJobInstanceData.MainCharReadData.CharacteristicUUID = DevAccessData.CharacteristicUUID;
                                InJobInstanceData.MainCharReadData.DataReadLen = 0;
                                if (DevAccessData.DevHandlerID == BLEDevKeeper_GetRepeaterIndex ()) {
                                    InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Cannot find DeviceInfoKp for DevMgr Job(%d)\r\n", CurrentJob.JobType);
                                bCleanCurrentJob = dxTRUE;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_CLEAR_DEVICE_SECURITY : {
                            int16_t DeviceHandlerID;
                            memcpy(&DeviceHandlerID, CurrentJob.JobData, sizeof(DeviceHandlerID));

                            const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DeviceHandlerID);

                            if (DeviceInfoKp)
                            {
                                BLEManager_GenJobProcess_t SetRenewSecurityDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE, DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION,
                                                                                        DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, DEV_MANAGER_ON_JOB_SECURITY_KEY_CLEAR,
                                                                                        DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                                //Push to our working in job sequencer
                                memcpy(InJobSequence, SetRenewSecurityDeviceInJobSequence, sizeof(SetRenewSecurityDeviceInJobSequence));

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                //Prepare the necessary info to InJob instance data
                                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED; //We would like to report this job as event DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED

                                InJobInstanceData.DeviceAddress.AddrLen = DeviceInfoKp->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = DeviceInfoKp->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfoKp->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                //We are going to prepare the event payload in advance, it will be released from application later no matter if the operation is failed or success
                                int16_t* pDeviceIndexData = dxMemAlloc ("Device index Buff", 1, sizeof(int16_t));
                                if (pDeviceIndexData) {
                                    *pDeviceIndexData = DeviceHandlerID;
                                    InJobInstanceData.pEventPayload = (uint8_t*)pDeviceIndexData;
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Cannot find DeviceInfoKp for DevMgr Job(%d)\r\n", CurrentJob.JobType);
                                bCleanCurrentJob = dxTRUE;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_SET_RENEW_DEVICE_SECURITY : {
                            stRenewSecurityInfo_t SecurityInfo;
                            memcpy(&SecurityInfo, CurrentJob.JobData, sizeof(stRenewSecurityInfo_t));


                            BLEManager_GenJobProcess_t SetRenewSecurityDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE, DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION,
                                                                                    DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE_AND_SAVE_FOR_REPORT,
                                                                                    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};


                            //Push to our working in job sequencer
                            memcpy(InJobSequence, SetRenewSecurityDeviceInJobSequence, sizeof(SetRenewSecurityDeviceInJobSequence));


                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Prepare the necessary info to InJob instance data

                            InJobInstanceData.EventReport = DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED; //We would like to report this job as event DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED

                            InJobInstanceData.DeviceAddress.AddrLen = B_ADDR_LEN_8;
                            InJobInstanceData.DeviceAddress.AddressType = 0; //Will be filled later
                            memcpy (InJobInstanceData.DeviceAddress.pAddr, SecurityInfo.Addr, InJobInstanceData.DeviceAddress.AddrLen);
                            if (SecurityInfo.ProductType == DK_PRODUCT_TYPE_BLE_REPEATER_ID) {
                                InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                            }

                            uint8_t i = 0;
                            for (i = 0; i < sizeof(SecurityInfo.SecurityCode); i++) {
                                if (SecurityInfo.SecurityCode[i] == 0x00) {
                                    break;
                                }
                            }

                            if (i > MAXIMUM_SECURITY_KEY_SIZE_ALLOWED) {
                                i = MAXIMUM_SECURITY_KEY_SIZE_ALLOWED;
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - PIN CODE longer than container can hold!!!\r\n");
                            }
                            InJobInstanceData.SecurityKeyCodeContainer.Size = i;
                            memcpy (InJobInstanceData.SecurityKeyCodeContainer.value , SecurityInfo.SecurityCode, i);
                        }
                        break;

                        case DEV_MANAGER_JOB_UPDATE_RTC : {
                            stUpdatePeripheralRtcInfo_t UpdateRTCInfo;
                            memcpy(&UpdateRTCInfo, CurrentJob.JobData, sizeof(stUpdatePeripheralRtcInfo_t));

                            BLEManager_GenJobProcess_t SetRtcUpdateDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE, DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION,
                                                                                    DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, DEV_MANAGER_ON_JOB_RTC_UPDATE,
                                                                                    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};


                            //Push to our working in job sequencer
                            memcpy(InJobSequence, SetRtcUpdateDeviceInJobSequence, sizeof(SetRtcUpdateDeviceInJobSequence));


                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Prepare the necessary info to InJob instance data

                            InJobInstanceData.EventReport = DEV_MANAGER_EVENT_RTC_UPDATED; //We would like to report this job as event DEV_MANAGER_EVENT_RTC_UPDATED

                            InJobInstanceData.DeviceAddress.AddrLen = B_ADDR_LEN_8;
                            InJobInstanceData.DeviceAddress.AddressType = 0; //Will be filled later
                            memcpy (InJobInstanceData.DeviceAddress.pAddr, UpdateRTCInfo.Addr, InJobInstanceData.DeviceAddress.AddrLen);
                        }
                        break;

                        case DEV_MANAGER_JOB_PAIR_DEVICE : {
                            stPairingInfo_t PairingInfo;
                            memcpy(&PairingInfo, CurrentJob.JobData, sizeof(stPairingInfo_t));

                            if (PairingInfo.SingleHostControl) //Security key exchange + Host Mac address lock (only this Gateway can connect with the device)
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Aaron - PairingInfo.SingleHostControl = 1\r\n");
                                //TODO: There is a small flow on Pairing, for undiscovered service/characteristic we will first discover and
                                //      save the handler id back to keeper (tied to the device address). However, since we add the device to keeper list at
                                //      last stage we won't be able to to save the handler in all service/char discoveries during this job.
/*
                                BLEManager_GenJobProcess_t PairDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_START_SCAN, DEV_MANAGER_ON_JOB_FIND_DEVICE, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                        DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE,
                                                                                        DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, DEV_MANAGER_ON_JOB_FW_VERSION_READ,
                                                                                        DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST,
                                                                                        DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};
*/
                                BLEManager_GenJobProcess_t PairDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_START_SCAN, DEV_MANAGER_ON_JOB_FIND_DEVICE, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                        DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE,
                                                                                        DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE, DEV_MANAGER_ON_JOB_FW_VERSION_READ,
                                                                                        DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST,
                                                                                        DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                                //Push to our working in job sequencer
                                memcpy(InJobSequence, PairDeviceInJobSequence, sizeof(PairDeviceInJobSequence));

                                memset( InJobInstanceData.MainCharWriteData.DataWrite, 0, sizeof( InJobInstanceData.MainCharWriteData.DataWrite ) );
                                InJobInstanceData.MainCharWriteData.UUIDNeed128bitConversion = 1;
                                InJobInstanceData.MainCharWriteData.ServiceUUID = DK_SERVICE_SYSTEM_UTILS_UUID;
                                InJobInstanceData.MainCharWriteData.CharacteristicUUID = DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_HEADER_CONFIG_UUID;
                                InJobInstanceData.MainCharWriteData.DataWriteLen = sizeof(stSystemUtilsAdvHeaderConfigCommand_t);

                                stSystemUtilsAdvHeaderConfigCommand_t SysUtilCmd;
                                SysUtilCmd.Command = SYSTEM_UTIL_DK_ADV_HEADER_SET_PAIR_BIT;
                                memset( SysUtilCmd.ParamValue, 0, sizeof( SysUtilCmd.ParamValue ) );
                                memcpy(InJobInstanceData.MainCharWriteData.DataWrite, &SysUtilCmd, sizeof(stSystemUtilsAdvHeaderConfigCommand_t));
                                InJobInstanceData.DeviceType = PairingInfo.ProductType;

                            }
                            else //Pairing with Security key exchange only
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Aaron - PairingInfo.SingleHostControl = 0\r\n");
                                //TODO: There is a small flow on Pairing, for undiscovered service/characteristic we will first discover and
                                //      save the handler id back to keeper (tied to the device address). However, since we add the device to keeper list at
                                //      last stage we won't be able to to save the handler in all service/char discoveries during this job.
                                BLEManager_GenJobProcess_t PairDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_START_SCAN, DEV_MANAGER_ON_JOB_FIND_DEVICE, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                        DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE, DEV_MANAGER_ON_JOB_SECURITY_KEY_EXCHANGE,
                                                                                        DEV_MANAGER_ON_JOB_FW_VERSION_READ, DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST,
                                                                                        DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};


                                //Push to our working in job sequencer
                                memcpy(InJobSequence, PairDeviceInJobSequence, sizeof(PairDeviceInJobSequence));
                                InJobInstanceData.DeviceType = PairingInfo.ProductType;

                            }

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Prepare the necessary info to InJob instance data

                            InJobInstanceData.EventReport = DEV_MANAGER_EVENT_NEW_DEVICE_ADDED; //We would like to report this job as event DEV_MANAGER_EVENT_NEW_DEVICE_ADDED

                            InJobInstanceData.DeviceAddress.AddrLen = B_ADDR_LEN_8;
                            InJobInstanceData.DeviceAddress.AddressType = 0; //Will be filled later
                            memcpy (InJobInstanceData.DeviceAddress.pAddr, PairingInfo.Addr, InJobInstanceData.DeviceAddress.AddrLen);
                            //work-around
                            if (PairingInfo.ProductType == DK_PRODUCT_TYPE_BLE_REPEATER_ID) {
                                InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                            }

                            uint8_t i = 0;
                            for (i = 0; i < sizeof(PairingInfo.SecurityCode); i++) {
                                if (PairingInfo.SecurityCode[i] == 0x00) {
                                    break;
                                }
                            }

                            if (i > MAXIMUM_SECURITY_KEY_SIZE_ALLOWED) {
                                i = MAXIMUM_SECURITY_KEY_SIZE_ALLOWED;
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING - PIN CODE longer than container can hold!!!\r\n");
                            }
                            InJobInstanceData.SecurityKeyCodeContainer.Size = i;
                            memcpy(InJobInstanceData.SecurityKeyCodeContainer.value , PairingInfo.SecurityCode, i);
                            //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "PIN CODE = %s\r\n", PairingInfo.SecurityCode);
                        }
                        break;

                        case DEV_MANAGER_JOB_ADD_DEVICE : {
                            BLEManager_GenJobProcess_t AddDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_ADD_DEVICE_TO_KEEPER_LIST,
                                                                                    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, AddDeviceInJobSequence, sizeof(AddDeviceInJobSequence));

                            //Prepare the necessary info to InJob instance data
                            DeviceAddInfo_t DeviceInfo;
                            memcpy (&DeviceInfo, CurrentJob.JobData, sizeof (DeviceAddInfo_t));

                            InJobInstanceData.DeviceAddress.AddrLen = B_ADDR_LEN_8;
                            InJobInstanceData.DeviceAddress.AddressType = 0; //Will be filled later
                            memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfo.DevAddress.Addr, InJobInstanceData.DeviceAddress.AddrLen);
                            InJobInstanceData.DeviceType = DeviceInfo.DeviceType;
                            if (DeviceInfo.SecurityKey.Size) {
                                InJobInstanceData.SecurityKeyCodeContainer.Size = DeviceInfo.SecurityKey.Size;
                                memcpy (InJobInstanceData.SecurityKeyCodeContainer.value , DeviceInfo.SecurityKey.value, DeviceInfo.SecurityKey.Size);
                            } else {
                                InJobInstanceData.SecurityKeyCodeContainer.Size = 0;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_UNPAIR_DEVICE : {
                            int8_t DeviceHandlerID;
                            memcpy(&DeviceHandlerID, CurrentJob.JobData, sizeof(DeviceHandlerID));

                            const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DeviceHandlerID);

                            if (DeviceInfoKp)
                            {
                                //We will remove the device from keeper list first, only then connect to ble and unpair it....
                                //The reason is because it is more important to remove the BLE from keeper list than set the BLE to unpair state
                                //Even if we remove the device from keper list (gone from system record) we should not have any issue trying to access
                                //the ble device because the only requirement from keeper list is to check the characteristic handler, if not found we will automatically
                                //re-discover again... Only draw back is it will take longer but thats acceptable
                                BLEManager_GenJobProcess_t UnPairDeviceInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_REMOVE_DEVICE_FROM_KEEPER_LIST, DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                            DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE_GIVEN_KEY,
                                                                                            DEV_MANAGER_ON_JOB_SECURITY_KEY_CLEAR, DEV_MANAGER_ON_JOB_MAIN_CHARACTERISTIC_WRITE,
                                                                                            DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                //Push to our working in job sequencer
                                memcpy(InJobSequence, UnPairDeviceInJobSequence, sizeof(UnPairDeviceInJobSequence));

                                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_DEVICE_UNPAIRED; //We would like to report this job as event DEV_MANAGER_EVENT_DEVICE_UNPAIRED

                                InJobInstanceData.DeviceAddress.AddrLen = DeviceInfoKp->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = DeviceInfoKp->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, DeviceInfoKp->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                if (DeviceHandlerID == BLEDevKeeper_GetRepeaterIndex ()) {
                                    InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                }
                                if (DeviceInfoKp->SecurityKey.Size) {
                                    InJobInstanceData.SecurityKeyCodeContainer.Size = DeviceInfoKp->SecurityKey.Size;
                                    memcpy(InJobInstanceData.SecurityKeyCodeContainer.value , DeviceInfoKp->SecurityKey.value, DeviceInfoKp->SecurityKey.Size);
                                }

                                memset( InJobInstanceData.MainCharWriteData.DataWrite, 0, sizeof( InJobInstanceData.MainCharWriteData.DataWrite ) );
                                InJobInstanceData.MainCharWriteData.UUIDNeed128bitConversion = 1;
                                InJobInstanceData.MainCharWriteData.ServiceUUID = DK_SERVICE_SYSTEM_UTILS_UUID;
                                InJobInstanceData.MainCharWriteData.CharacteristicUUID = DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_HEADER_CONFIG_UUID;
                                InJobInstanceData.MainCharWriteData.DataWriteLen = sizeof(stSystemUtilsAdvHeaderConfigCommand_t);

                                stSystemUtilsAdvHeaderConfigCommand_t SysUtilCmd;
                                SysUtilCmd.Command = SYSTEM_UTIL_DK_ADV_HEADER_CLEAR_PAIR_BIT; //Clear Pair bit
                                memset( SysUtilCmd.ParamValue, 0, sizeof( SysUtilCmd.ParamValue ) );
                                memcpy(InJobInstanceData.MainCharWriteData.DataWrite, &SysUtilCmd, sizeof(stSystemUtilsAdvHeaderConfigCommand_t));
                            }
                            else
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Cannot find DeviceInfoKp for DevMgr Job(%d)\r\n", CurrentJob.JobType);
                                bCleanCurrentJob = dxTRUE;
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_CLEANUP_ALL_DEVICE_FROMKEEPER_LIST : {
                            BLEManager_GenJobProcess_t RemoveAllDevFromKeeperListInJobSequence[] = {DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_REMOVE_ALLDEV_FROM_KEEPER_LIST,
                                                                                                    DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, RemoveAllDevFromKeeperListInJobSequence, sizeof(RemoveAllDevFromKeeperListInJobSequence));
                        }
                        break;

                        case DEV_MANAGER_JOB_GET_DEVICE_KEEPER_LIST : {
                            BLEManager_GenJobProcess_t GetDeviceKeeperListInJobSequence[] = {
                                DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_GET_DEVICE_KEEPER_LIST,
                                DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL,
                                DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, GetDeviceKeeperListInJobSequence, sizeof(GetDeviceKeeperListInJobSequence));
                        }
                        break;

                        case DEV_MANAGER_JOB_PERIPHERAL_DATA_OUT_WRITE : {
                            BLEManager_GenJobProcess_t PeripheralRoleDataOutInJobSequence[] = { DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_TRANSFER_PERIPHERAL_DATA_OUT,
                                                                                                DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL, DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, PeripheralRoleDataOutInJobSequence, sizeof(PeripheralRoleDataOutInJobSequence));

                            //Provide the data
                            InJobInstanceData.pJobPayload = CurrentJob.JobData;

                        }
                        break;

                        case DEV_MANAGER_JOB_FW_BLOCK_WRTE : {
                            //NOTE: Our last step is Job CleanUp as Peripheral although we are not in peripheral mode but we are using this
                            //      for basic clean ups
                            BLEManager_GenJobProcess_t PeripheralFwBlockWriteSequence[] = { DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_FW_BLOCK_WRITE,
                                                                                            DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL, DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, PeripheralFwBlockWriteSequence, sizeof(PeripheralFwBlockWriteSequence));

                            //Provide the data
                            InJobInstanceData.pJobPayload = CurrentJob.JobData;

                        }
                        break;

                        case DEV_MANAGER_JOB_FW_UPDATE_DONE : {
                            //NOTE: Our last step is Job CleanUp as Peripheral although we are not in peripheral mode but we are using this
                            //      for basic clean ups
                            BLEManager_GenJobProcess_t PeripheralFwUpdateDoneSequence[] = { DEV_MANAGER_ON_JOB_START, DEV_MANAGER_ON_JOB_FW_UPDATE_DONE_BOOT_TO_APP,
                                                                                            DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL, DEV_MANAGER_ON_JOB_NULL};

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy(InJobSequence, PeripheralFwUpdateDoneSequence, sizeof(PeripheralFwUpdateDoneSequence));

                        }
                        break;

                        case DEV_MANAGER_JOB_SETUP_REPEATER : {
                            int8_t repeaterIndex;
                            memcpy (&repeaterIndex, CurrentJob.JobData, sizeof (int8_t));
                            const DeviceInfoKp_t* deviceInfo = BLEDevKeeper_GetDeviceInfoFromIndex (repeaterIndex);

                            if (deviceInfo) {
                                BLEManager_GenJobProcess_t setupRepeaterInJobSequence[] = { DEV_MANAGER_ON_JOB_START,
                                                                                            DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                            DEV_MANAGER_ON_JOB_CONNECT_TO_REPEATER,
                                                                                            DEV_MANAGER_ON_JOB_LOAD_REPEATER_HANDLE,
                                                                                            DEV_MANAGER_ON_JOB_SECURITY_CLEARANCE_GIVEN_KEY,
                                                                                            DEV_MANAGER_ON_JOB_CHECK_REPEATER_WATCH_LIST,
                                                                                            DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER,
                                                                                            DEV_MANAGER_ON_JOB_NULL };

                                //Set our first job index
                                CurrentInJobStageIndex = 0;

                                //Push to our working in job sequencer
                                memcpy (InJobSequence, setupRepeaterInJobSequence, sizeof (setupRepeaterInJobSequence));

                                // Set the EventReport
                                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_BLE_REPEATER_SETUP_REPORT;

                                //Provide the data
                                InJobInstanceData.DeviceAddress.AddrLen = deviceInfo->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = deviceInfo->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, deviceInfo->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                if (deviceInfo->SecurityKey.Size) {
                                    InJobInstanceData.SecurityKeyCodeContainer.Size = deviceInfo->SecurityKey.Size;
                                    memcpy (InJobInstanceData.SecurityKeyCodeContainer.value , deviceInfo->SecurityKey.value, deviceInfo->SecurityKey.Size);
                                }
                            }
                        }
                        break;

                        case DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS : {

                            BLEManager_GenJobProcess_t ConcurrentPeripheralCustomizeBdAddressInJobSequence[] = {
                                DEV_MANAGER_ON_JOB_START,
                                DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS,
                                DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL,
                                DEV_MANAGER_ON_JOB_NULL
                            };

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy (InJobSequence, ConcurrentPeripheralCustomizeBdAddressInJobSequence, sizeof (ConcurrentPeripheralCustomizeBdAddressInJobSequence));

                            //Provide the data
                            InJobInstanceData.pJobPayload = CurrentJob.JobData;
                        }
                        break;

                        case DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_DATA_OUT : {

                            BLEManager_GenJobProcess_t ConcurrentPeripheralDataOutInJobSequence[] = {
                                DEV_MANAGER_ON_JOB_START,
                                DEV_MANAGER_ON_JOB_CONCURRENT_PERIPHERAL_DATA_OUT,
                                DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL,
                                DEV_MANAGER_ON_JOB_NULL
                            };

                            //Set our first job index
                            CurrentInJobStageIndex = 0;

                            //Push to our working in job sequencer
                            memcpy (InJobSequence, ConcurrentPeripheralDataOutInJobSequence, sizeof (ConcurrentPeripheralDataOutInJobSequence));

                            //Provide the data
                            InJobInstanceData.pJobPayload = CurrentJob.JobData;
                        }
                        break;

                        case DEV_MANAGER_JOB_MONITOR_REPEATER : {
                            int8_t repeaterIndex;
                            memcpy (&repeaterIndex, CurrentJob.JobData, sizeof (int8_t));
                            const DeviceInfoKp_t* deviceInfo = BLEDevKeeper_GetDeviceInfoFromIndex (repeaterIndex);

                            if (deviceInfo) {
                                BLEManager_GenJobProcess_t monitorRepeaterInJobSequence[] = { DEV_MANAGER_ON_JOB_START,
                                                                                              DEV_MANAGER_ON_JOB_CENTRAL_SET_IDLE,
                                                                                              DEV_MANAGER_ON_JOB_CHECK_REPEATER_STATE,
                                                                                              DEV_MANAGER_ON_JOB_CHECK_REPEATER_WATCH_LIST,
                                                                                              DEV_MANAGER_ON_JOB_CHECK_REPEATER_OPEN_PAIRING,
                                                                                              DEV_MANAGER_ON_JOB_GET_REPEATER_ADVERTISE_INFO,
                                                                                              DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER,
                                                                                              DEV_MANAGER_ON_JOB_NULL };

                                // Set our first job index
                                CurrentInJobStageIndex = 0;

                                // Push to our working in job sequencer
                                memcpy (InJobSequence, monitorRepeaterInJobSequence, sizeof (monitorRepeaterInJobSequence));

                                // Set the EventReport
                                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_BLE_REPEATER_MONITOR_REPORT;

                                //Provide the data
                                InJobInstanceData.DeviceAddress.AddrLen = deviceInfo->DevAddress.AddrLen;
                                InJobInstanceData.DeviceAddress.AddressType = deviceInfo->DevAddress.AddressType;
                                memcpy (InJobInstanceData.DeviceAddress.pAddr, deviceInfo->DevAddress.pAddr, InJobInstanceData.DeviceAddress.AddrLen);
                                InJobInstanceData.DeviceAddress.AddressType = ADDRTYPE_STATIC;
                                if (deviceInfo->SecurityKey.Size) {
                                    InJobInstanceData.SecurityKeyCodeContainer.Size = deviceInfo->SecurityKey.Size;
                                    memcpy (InJobInstanceData.SecurityKeyCodeContainer.value , deviceInfo->SecurityKey.value, deviceInfo->SecurityKey.Size);
                                }
                            }
                        }
                        break;

                        default :
                        break;
                    }

                    if (bCleanCurrentJob) {
                        CurrentJob.JobType = DEV_MANAGER_NO_JOB;
                        memset( CurrentJob.JobData, 0, sizeof( CurrentJob.JobData ) );
                        CurrentInJobStageIndex = 0;
                        InJobSequence[CurrentInJobStageIndex] = DEV_MANAGER_ON_JOB_NULL;
                    }
                }
            }

            // Assume lower priority job has greater number in BLEManager_Job_t
            // Check device status if no job or job priority is <= DEV_MANAGER_JOB_MONITOR_REPEATER
            if ((CurrentJob.JobType == DEV_MANAGER_NO_JOB) || (CurrentJob.JobType >= DEV_MANAGER_JOB_MONITOR_REPEATER)) {
                _BleManager_CheckDeviceStatus (&NextStatusCheckSystemTime);
            }

        }
        else
        {
            blemanager_state_func_t* InJobStateFunc = NULL;

            InJobStateFunc = BleRepeater_GetStateFunction (InJobSequence[CurrentInJobStageIndex]);
            if (InJobStateFunc == NULL)
            {
                uint16_t i;
                for (i = 0; i < (sizeof(StateFuncMap)/sizeof(state_func_table_t)); i++)
                {
                    state_func_table_t* FuncMap = &StateFuncMap[i];

                    if (FuncMap->State == InJobSequence[CurrentInJobStageIndex])
                    {
                        InJobStateFunc = FuncMap->StateFunction;
                        break;
                    }
                }
            }

            uint16_t  InterruptCleanUpRequired = 0;
            dxBOOL allJobDone = dxFALSE;

            if (InJobStateFunc) {
                InJobInstanceData.CurrentCentralStatus = BLEDevCentralCtrl_GetLastKnownConnectionStatus();
                JobState = InJobStateFunc(&InJobInstanceData);
            } else {
                allJobDone = dxTRUE;
            }

            if (allJobDone) {

                dxEventHandler_t eventHandler;

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Job: Done (JobResult=%d)\r\n", InJobInstanceData.JobResult);

                // Get the event handler base on job type
                if ((CurrentJob.JobType == DEV_MANAGER_JOB_SETUP_REPEATER) || (CurrentJob.JobType == DEV_MANAGER_JOB_MONITOR_REPEATER)) {
                    eventHandler = BleRepeater_RegisteredEventFunction;
                } else {
                    eventHandler = RegisteredEventFunction;
                }

                if (eventHandler) {
                    EventReportObj_t* event = dxMemAlloc ("Ble Manager Job report event Buff", 1, sizeof (EventReportObj_t));

                    //If EventReport is not explicitly specified then we use the default TASk ACCESS REPORT event
                    if (InJobInstanceData.EventReport == DEV_MANAGER_EVENT_UNKNOWN) {
                        event->Header.ReportEvent = DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT;
                    } else {
                        event->Header.ReportEvent = InJobInstanceData.EventReport;
                    }

                    //If EventResult is not explicitly specified (during inJob sequence) then we default JobResult according to access status
                    if (InJobInstanceData.EventResult == DEV_MANAGER_TASK_SUCCESS) {
                        event->Header.ResultState = InJobInstanceData.JobResult;
                    } else {
                        event->Header.ResultState = InJobInstanceData.EventResult;
                    }

                    event->Header.IdentificationNumber = CurrentJob.JobIdentificationNumber;
                    event->Header.SourceOfOrigin = CurrentJob.JobSourceOfOrigin;
                    event->pData = InJobInstanceData.pEventPayload;

                    dxOS_RET_CODE result = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, eventHandler, event);
                    if (result != DXOS_SUCCESS) {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - (L=%d)Failed to send Event message back to Application\r\n", __LINE__);
                        if (event->pData) {
                            dxMemFree (event->pData);
                        }
                        dxMemFree (event);
                    }
                } else {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: BleManager RegisteredEventFunction NOT registered");
                }

                if (InJobInstanceData.DeviceInfoObj) {
                    dxMemFree (InJobInstanceData.DeviceInfoObj);
                }

                memset(&InJobInstanceData.DeviceFwVersion, 0 , sizeof(InJobInstanceData.DeviceFwVersion));
                InJobInstanceData.DeviceInfoObj = NULL;
                InJobInstanceData.pJobPayload = NULL;
                memset(&InJobInstanceData.SecurityKeyCodeContainer, 0 , sizeof(InJobInstanceData.SecurityKeyCodeContainer));
                InJobInstanceData.pEventPayload = NULL;   /* NOTE: Event Payload Should NOT be release/free because its going to be released by application layer */
                InJobInstanceData.EventReport = DEV_MANAGER_EVENT_UNKNOWN; //Deafault -> Unspecified
                InJobInstanceData.EventResult = DEV_MANAGER_TASK_SUCCESS;   //Deafault -> Unspecified

                CurrentJob.JobType = DEV_MANAGER_NO_JOB;
                memset( CurrentJob.JobData, 0, sizeof( CurrentJob.JobData ) );

            } else {

                switch (JobState)
                {
                    case DEV_MANAGER_ON_JOB_STATE_UNCHANGE : {
                        if (IsTimesUp (CurrentJob.JobTimeOutTime))
                        {
                            //If we are already on the last stage (clean up) then we simply continue since main task is already done. We do not report timeout
                            //otherwise its a timeout
                            if ((InJobSequence[CurrentInJobStageIndex] != DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL) ||
                                (InJobSequence[CurrentInJobStageIndex] != DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER) ||
                                (InJobSequence[CurrentInJobStageIndex] != DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL))
                            {
                                InterruptCleanUpRequired = 1;
                                InJobInstanceData.JobResult = DEV_MANAGER_TASK_TIME_OUT;
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Job: TimeOut\r\n");
                            }

                            CurrentJob.JobTimeOutTime = 0; //Clear the timeup time so that we don't get here again

                        }
                        else
                        {
                            //Let's sleep for a short while and see if we can continue
                            dxTimeDelayMS(50);
                        }

                    }
                    break;

                    case DEV_MANAGER_ON_JOB_NEXT_STATE : {
                        CurrentInJobStageIndex++;

                        if (CurrentInJobStageIndex > MAXIMUM_DEVICE_IN_JOB_SEQUENCE_ALLOWED)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: CurrentInJobStageIndex exceeding MAXIMUM_DEVICE_IN_JOB_SEQUENCE_ALLOWED");
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_INTERNAL_FATAL_ERROR;
                            allJobDone = dxTRUE;
                        }
                        else if (InJobSequence[CurrentInJobStageIndex] == DEV_MANAGER_ON_JOB_NULL)
                        {
                            allJobDone = dxTRUE;
                        }
                    }
                    break;

                    case DEV_MANAGER_ON_JOB_FAILED_DISCONNECTED_WHILE_IN_PROGRESS_ERROR : {
                        //Disconnection happend quite often, its unavoidable in some cases so we do retry if we see disconnection
                        if (CurrentJob.JobDisconnectMaxRetry)
                        {
                            uint16_t index = InJobStageIndexLookUp(DEV_MANAGER_ON_JOB_CENTRAL_SET_CONNECTION, InJobSequence, sizeof(InJobSequence)/sizeof(BLEManager_GenJobProcess_t));
                            if (index)
                            {
                                //jump to the first connection job in the injob list (if found)
                                CurrentInJobStageIndex = index;
                            }

                            CurrentJob.JobDisconnectMaxRetry--;
                        }
                        else
                        {
                            InterruptCleanUpRequired = 1;
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_FAILED_CONNECTION_UNSTABLE;
                        }

                    }
                    break;

                    case DEV_MANAGER_ON_JOB_FAILED_INTERNAL_ERROR :
                    case DEV_MANAGER_ON_JOB_FAILED_SENDING_MSG :
                    case DEV_MANAGER_ON_JOB_FAILED_DEVICE_ALREADY_PAIRED :
                    case DEV_MANAGER_ON_JOB_MISSING_SECURITY_KEY_ERROR :
                    case DEV_MANAGER_ON_JOB_SECURITY_KEY_NOT_VALID_ERROR :
                    case DEV_MANAGER_ON_JOB_INVALID_PIN_CODE_ERROR :
                    {

                        InterruptCleanUpRequired = 1;

                        if (JobState == DEV_MANAGER_ON_JOB_FAILED_DEVICE_ALREADY_PAIRED)
                        {
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED;
                        }
                        else if (JobState == DEV_MANAGER_ON_JOB_MISSING_SECURITY_KEY_ERROR)
                        {
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_MISSING;
                        }
                        else if (JobState == DEV_MANAGER_ON_JOB_SECURITY_KEY_NOT_VALID_ERROR)
                        {
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_IS_NOT_VALID;
                        }
                        else if (JobState == DEV_MANAGER_ON_JOB_INVALID_PIN_CODE_ERROR)
                        {
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_DEVICE_PIN_CODE_IS_NOT_VALID;
                        }
                        else
                        {
                            InJobInstanceData.JobResult = DEV_MANAGER_TASK_INTERNAL_ERROR;
                        }

                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Job: Failed! (Code %d)\r\n", JobState);
                    }
                    break;

                    default : {
                        allJobDone = dxTRUE;
                        InJobInstanceData.JobResult = DEV_MANAGER_TASK_INTERNAL_FATAL_ERROR;
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "FATAL: wrong return value=%d\r\n", JobState);
                    }
                    break;
                }

                if (InterruptCleanUpRequired) {
                    InterruptCleanUpRequired = 0;
                    uint16_t index = 0;

                    if (!index) {
                        index = InJobStageIndexLookUp (DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_CENTRAL, InJobSequence, sizeof(InJobSequence)/sizeof(BLEManager_GenJobProcess_t));
                        CurrentInJobStageIndex = index;
                    }

                    if (!index) {
                        index = InJobStageIndexLookUp (DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_REPEATER, InJobSequence, sizeof(InJobSequence)/sizeof(BLEManager_GenJobProcess_t));
                        CurrentInJobStageIndex = index;
                    }

                    if (!index) {
                        index = InJobStageIndexLookUp (DEV_MANAGER_ON_JOB_DONE_CLEAN_UP_PERIPHERAL, InJobSequence, sizeof(InJobSequence)/sizeof(BLEManager_GenJobProcess_t));
                        CurrentInJobStageIndex = index;
                    }

                    if (!index) {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: Unable to find DEV_MANAGER_ON_JOB_DONE_CLEAN_UP on InJobSequence!\r\n");
                    }
                }
            }
        }
    }


    // Free memory for address at end
    dxMemFree (InJobInstanceData.DeviceAddress.pAddr);

    END_OF_TASK_FUNCTION;
}


static TASK_RET_TYPE BridgeComm_Response_Process_thread_main( TASK_PARAM_TYPE arg )
{
    dxOS_RET_CODE           PopResult;
    dxQueueHandle_t*        BCResponseMsgQueue = BLEDevBridgeComm_GetResponseMsgQueueHdl();
    BleBridgeCommMsg_t* BCMessageRsp;

    //We are going to request current connection status request initially so that we will conduct auto scan after receiving the status update in CentralControlFunctionEvent
    if (BLEDevCentralCtrl_ConnectionStatusUpdateReq() != DEV_CENTRAL_CTRL_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - Kickoff connection status update request failed initially\r\n" );
    }

    while (1)
    {
        BCMessageRsp = NULL;
        PopResult = dxQueueReceive( *BCResponseMsgQueue, &BCMessageRsp, DXOS_WAIT_FOREVER );
        if (PopResult == DXOS_SUCCESS)
        {

            switch(BCMessageRsp->Header.MessageType)
            {
                case MSG_TYPE_SCAN : {
                    if ((BCMessageRsp->Header.CommandAux == SCAN_AUX_TYPE_DEVICE_INFO) &&
                        (BCMessageRsp->Header.PayloadLen) &&
                        (gDeviceScanReportAllowed == dxTRUE))
                    {
                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BCMessageRsp.Header.PayloadLen = %d\r\n", BCMessageRsp->Header.PayloadLen );
                        BLEManager_HandleAdvertiseInfo ((uint8_t *)BCMessageRsp->Payload, BCMessageRsp->Header.PayloadLen);
                    }

                }
                break;

                case MSG_TYPE_CENTRAL : {
                    if ((BCMessageRsp->Header.CommandAux == CENTRAL_AUX_TYPE_STATUS_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {
                        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "MSG_TYPE_CENTRAL BCMessageRsp.Header.PayloadLen = %d\r\n", BCMessageRsp->Header.PayloadLen );
                        BLEDeviceCtrl_result_t ConnDiscResult = DEV_CENTRAL_CTRL_SUCCESS;
                        ConnDiscResult = BLEDevCentralCtrl_ConnectionStatusUpdateRspHandler(BCMessageRsp->Payload);
                        if (ConnDiscResult != DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_ConnectionStatusUpdateRspHandler Error (%d)\r\n", ConnDiscResult );
                        }
                    }
                }
                break;

                case MSG_TYPE_DISCOVERY : {
                    if ((BCMessageRsp->Header.CommandAux == DISCOVERY_AUX_TYPE_STATE_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {
                        BLEDeviceCtrl_result_t ConnDiscResult = DEV_CENTRAL_CTRL_SUCCESS;
                        ConnDiscResult = BLEDevCentralCtrl_DiscoveryStatusUpdateRspHandler(BCMessageRsp->Payload);
                        if (ConnDiscResult != DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_DiscoveryStatusUpdateRspHandler Error (%d)\r\n", ConnDiscResult );
                        }
                    }
                    else if ((BCMessageRsp->Header.CommandAux == DISCOVERY_AUX_TYPE_DISC_CHAR_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {

                        BLEDeviceCtrl_result_t ConnDiscResult = DEV_CENTRAL_CTRL_SUCCESS;
                        ConnDiscResult = BLEDevCentralCtrl_CharacteristicDiscoveryRspHandler(BCMessageRsp->Payload);
                        if (ConnDiscResult != DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_CharacteristicDiscoveryRspHandler Error (%d)\r\n", ConnDiscResult );
                        }

                    }
                }
                break;

                case MSG_TYPE_SEND : {
                    if ((BCMessageRsp->Header.CommandAux == SEND_AUX_TYPE_WRITE_CHAR_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {

                        BLEDeviceCtrl_result_t WriteRspResult = DEV_CENTRAL_CTRL_SUCCESS;
                        WriteRspResult = BLEDevCentralCtrl_CharacteristicWriteRspHandler(BCMessageRsp->Payload);
                        if (WriteRspResult != DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_CharacteristicWriteRspHandler Error (%d)\r\n", WriteRspResult);
                        }
                    }
                    else if ((BCMessageRsp->Header.CommandAux == SEND_AUX_TYPE_READ_CHAR_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {
                        BLEDeviceCtrl_result_t ReadRspResult = DEV_CENTRAL_CTRL_SUCCESS;
                        ReadRspResult = BLEDevCentralCtrl_CharacteristicReadRspHandler(BCMessageRsp->Payload);
                        if (ReadRspResult != DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_CharacteristicReadRspHandler Error (%d)\r\n", ReadRspResult );
                        }
                    }
                    else if ((BCMessageRsp->Header.CommandAux == SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_OUT_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {
                        BLEDeviceCtrl_result_t DataOutRspResult = DEV_CENTRAL_CTRL_SUCCESS;
                        DataOutRspResult = BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck(BCMessageRsp->Payload);
                        if (DataOutRspResult == DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            BlePeripheralDataTransfer_t* DataOutTransferRsp = dxMemAlloc( "Peripheral Role Data out Report Msg Buff", 1, sizeof(BlePeripheralDataTransfer_t));
                            memcpy(DataOutTransferRsp, BCMessageRsp->Payload, sizeof(BlePeripheralDataTransfer_t));
                            PushPeripheralRoleDataTransferReportToInJobQueue(DataOutTransferRsp);
                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck Failed\r\n");

                        }
                    }
                    else if ((BCMessageRsp->Header.CommandAux == SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_IN_RSP) && (BCMessageRsp->Header.PayloadLen))
                    {
                        EventReportObj_t* DevPeripheralRoleDataInEvent = dxMemAlloc( "Ble peripheral role data in event Buff", 1, sizeof(EventReportObj_t));

                        BLEDeviceCtrl_result_t DataInRspResult = DEV_CENTRAL_CTRL_SUCCESS;
                        DataInRspResult = BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck(BCMessageRsp->Payload);
                        if (DataInRspResult == DEV_CENTRAL_CTRL_SUCCESS)
                        {
                            if (RegisteredEventFunction)
                            {

                                BlePeripheralDataTransfer_t* DataInTransferRsp = dxMemAlloc( "Peripheral Role Data In Report Msg Buff", 1, sizeof(BlePeripheralDataTransfer_t));
                                memcpy(DataInTransferRsp, BCMessageRsp->Payload, sizeof(BlePeripheralDataTransfer_t));

                                DevPeripheralRoleDataInEvent->Header.ReportEvent = DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD;
                                DevPeripheralRoleDataInEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                                DevPeripheralRoleDataInEvent->pData = (uint8_t*)DataInTransferRsp;
                            }

                        }
                        else
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_PeripheralDataTransferPayloadIntegrityCheck[DataIn] Failed\r\n");
                            DevPeripheralRoleDataInEvent->Header.ReportEvent = DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD;
                            DevPeripheralRoleDataInEvent->Header.ResultState = DEV_MANAGER_TASK_INTERNAL_ERROR;
                            DevPeripheralRoleDataInEvent->pData = NULL;
                        }

                        if (RegisteredEventFunction)
                        {

                            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, DevPeripheralRoleDataInEvent );
                            if (EventResult != DXOS_SUCCESS)
                            {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - (L=%d)Failed to send Event message back to Application\r\n", __LINE__);
                                if (DevPeripheralRoleDataInEvent->pData)
                                    dxMemFree(DevPeripheralRoleDataInEvent->pData);

                                dxMemFree(DevPeripheralRoleDataInEvent);
                            }
                        }
                    }
                }
                break;

                case MSG_TYPE_SYSTEM : {

                    EventReportObj_t* SystemRspEvent = NULL;

                    if ((BCMessageRsp->Header.CommandAux == SYSTEM_AUX_GET_INFO_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        SystemInfoReceived = dxTRUE;

                        BleGatewayInfo_t GatewayInfo = BLEDevCentralCtrl_GetSystemInfoReadRsp(BCMessageRsp->Payload, BCMessageRsp->Header.PayloadLen);
                        SystemInfoReportObj_t*  SystemInfoReportObj = dxMemAlloc( "systemInfo report Buff", 1, sizeof(SystemInfoReportObj_t));

                        SystemInfoReportObj->ImageType = GatewayInfo.ImageType;
                        BleManager_SolutionID_Update(GatewayInfo.ImageType);

                        snprintf( (char*)SystemInfoReportObj->Version , sizeof(SystemInfoReportObj->Version), "%d.%d.%d", GatewayInfo.VersionMajor, GatewayInfo.VersionMiddle, GatewayInfo.VersionMinor );

                        //Make local copy
                        memcpy(&SystemInfo, SystemInfoReportObj, sizeof(SystemInfoReportObj_t));

                        SystemRspEvent = dxMemAlloc( "systemInfo event Buff", 1, sizeof(EventReportObj_t));

                        SystemRspEvent->Header.ReportEvent = DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO;
                        SystemRspEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                        SystemRspEvent->pData = (uint8_t*)SystemInfoReportObj;

                    } else if ((BCMessageRsp->Header.CommandAux == SYSTEM_AUX_WRITE_FW_BLOCK_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        uint8_t*  FwBlockVerifyReportObj = dxMemAlloc( "Fw Block report Buff", 1, BCMessageRsp->Header.PayloadLen+1);

                        if (FwBlockVerifyReportObj)
                        {
                            //First byte is the len of total payload, then follow by payload data itself
                            FwBlockVerifyReportObj[0] = BCMessageRsp->Header.PayloadLen;
                            memcpy(&FwBlockVerifyReportObj[1], BCMessageRsp->Payload, BCMessageRsp->Header.PayloadLen);

                            SystemRspEvent = dxMemAlloc( "Fw block Rsp event Buff", 1, sizeof(EventReportObj_t));

                            SystemRspEvent->Header.ReportEvent = DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY;
                            SystemRspEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            SystemRspEvent->pData = FwBlockVerifyReportObj;
                        }

                    } else if (BCMessageRsp->Header.CommandAux == SYSTEM_AUX_REBOOT_TO_APP_RSP) {

                        SystemRspEvent = dxMemAlloc( "Fw block Rsp event Buff", 1, sizeof(EventReportObj_t));

                        SystemRspEvent->Header.ReportEvent = DEV_MANAGER_EVENT_FW_UPDATE_REBOOT_TO_APP_REPORT;
                        SystemRspEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                        SystemRspEvent->pData = NULL;

                    } else if ((BCMessageRsp->Header.CommandAux == SYSTEM_AUX_CUSTOMIZE_BD_ADDR_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        SystemBdAddressResponse_t* bdAddressResponse = dxMemAlloc ("bdAddressResponse", 1, sizeof (SystemBdAddressResponse_t));
                        bdAddressResponse->result = (int16_t) ((BCMessageRsp->Payload[0]) | (BCMessageRsp->Payload[1] << 8));

                        SystemRspEvent = dxMemAlloc ("Fw block Rsp event Buff", 1, sizeof (EventReportObj_t));

                        SystemRspEvent->Header.ReportEvent = DEV_MANAGER_EVENT_DEVICE_SYSTEM_BD_ADDRESS;
                        SystemRspEvent->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                        SystemRspEvent->pData = (uint8_t*) bdAddressResponse;

                    }

                    if (RegisteredEventFunction && SystemRspEvent)
                    {
                        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, SystemRspEvent );
                        if (EventResult != DXOS_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - (L=%d)Failed to send Event message back to Application\r\n", __LINE__);
                            if (SystemRspEvent->pData)
                                dxMemFree(SystemRspEvent->pData);

                            dxMemFree(SystemRspEvent);
                        }
                    }
                    else if (SystemRspEvent)
                    {

                        if (SystemRspEvent->pData)
                            dxMemFree(SystemRspEvent->pData);

                        dxMemFree(SystemRspEvent);
                    }


                }
                break;

                case MSG_TYPE_EXT : {
                    if ((BCMessageRsp->Header.CommandAux == EXT_REGISTER_WHITE_LIST_RES) &&
                        BCMessageRsp->Header.PayloadLen)
                    {
                        IntWhiteList_CheckNum(BCMessageRsp->Payload[0] | (BCMessageRsp->Payload[1] << 8));
                    }
                }
                break;

                case MSG_TYPE_CONCURRENT_PERIPHERAL : {

                    if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_STATUS_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        BLEDeviceCtrl_result_t ConnDiscResult = DEV_CENTRAL_CTRL_SUCCESS;
                        ConnDiscResult = BLEDevCentralCtrl_ConcurrentPeripheralStatusUpdateRspHandler (BCMessageRsp->Payload);
                        if (ConnDiscResult != DEV_CENTRAL_CTRL_SUCCESS) {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BLEDevCentralCtrl_ConnectionStatusUpdateRspHandler Error (%d)\r\n", ConnDiscResult);
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_SECURITY_KEY_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        if (RegisteredEventFunction) {
                            BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse = dxMemAlloc ("securityKeyResponse", 1, sizeof (BleConcurrentPeripheralSecurityKeyResponse_t));
                            securityKeyResponse->keyLength = BCMessageRsp->Payload[0];

                            EventReportObj_t* event = dxMemAlloc ("Ble peripheral role data in event Buff", 1, sizeof (EventReportObj_t));
                            event->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SECURITY_KEY_REPORT;
                            event->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            event->pData = (uint8_t*) securityKeyResponse;

                            dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, event);
                            if (eventResult != DXOS_SUCCESS) {
                                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - Failed to send Event message back to Application\r\n");
                                if (event->pData) {
                                    dxMemFree (event->pData);
                                }
                                dxMemFree (event);
                            }
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_ADV_SETUP_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        if (RegisteredEventFunction) {
                            BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse = dxMemAlloc ("advertiseDataResponse", 1, sizeof (BleConcurrentPeripheralAdvertiseDataResponse_t));
                            advertiseDataResponse->totalBytes = (int16_t) ((BCMessageRsp->Payload[0]) | (BCMessageRsp->Payload[1] << 8));

                            EventReportObj_t* event = dxMemAlloc ("Ble peripheral role data in event Buff", 1, sizeof (EventReportObj_t));
                            event->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_ADVERTISE_DATA_REPORT;
                            event->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            event->pData = (uint8_t*) advertiseDataResponse;

                            dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, event);
                            if (eventResult != DXOS_SUCCESS) {
                                G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                                if (event->pData) {
                                    dxMemFree (event->pData);
                                }
                                dxMemFree (event);
                            }
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_SCAN_RSP_SETUP_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        if (RegisteredEventFunction) {
                            BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse = dxMemAlloc ("scanResponseResponse", 1, sizeof (BleConcurrentPeripheralScanResponseResponse_t));
                            scanResponseResponse->totalBytes = (int16_t) ((BCMessageRsp->Payload[0]) | (BCMessageRsp->Payload[1] << 8));

                            EventReportObj_t* event = dxMemAlloc ("Ble peripheral role data in event Buff", 1, sizeof (EventReportObj_t));
                            event->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SCAN_RESPONSE_REPORT;
                            event->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            event->pData = (uint8_t*) scanResponseResponse;

                            dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, event);
                            if (eventResult != DXOS_SUCCESS) {
                                G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                                if (event->pData) {
                                    dxMemFree (event->pData);
                                }
                                dxMemFree (event);
                            }
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        if (RegisteredEventFunction) {
                            BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse = dxMemAlloc ("serviceCharacteristicResponse", 1, sizeof (BleConcurrentPeripheralServiceCharacteristicResponse_t));
                            serviceCharacteristicResponse->serviceUuid = (uint16_t) ((BCMessageRsp->Payload[0]) | (BCMessageRsp->Payload[1] << 8));
                            serviceCharacteristicResponse->characteristicCount = (int8_t) BCMessageRsp->Payload[2];

                            EventReportObj_t* event = dxMemAlloc ("Ble peripheral role data in event Buff", 1, sizeof (EventReportObj_t));
                            event->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SERVICE_CHARACTERISTIC_REPORT;
                            event->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            event->pData = (uint8_t*) serviceCharacteristicResponse;

                            dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, event);
                            if (eventResult != DXOS_SUCCESS) {
                                G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                                if (event->pData) {
                                    dxMemFree (event->pData);
                                }
                                dxMemFree (event);
                            }
                        }

                    } else if (BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_SYSTEM_UTILS_ENABLE_MASK_RSP) {

                        if (RegisteredEventFunction) {
                            BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse = dxMemAlloc ("systemUtilsResponse", 1, sizeof (BleConcurrentPeripheralSystemUtilsResponse_t));
                            systemUtilsResponse->enableMask = (uint16_t) ((BCMessageRsp->Payload[0]) | (BCMessageRsp->Payload[1] << 8));

                            EventReportObj_t* event = dxMemAlloc ("Ble peripheral role data in event Buff", 1, sizeof (EventReportObj_t));
                            event->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SYSTEM_UTILS_REPORT;
                            event->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                            event->pData = (uint8_t*) systemUtilsResponse;

                            dxOS_RET_CODE eventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, event);
                            if (eventResult != DXOS_SUCCESS) {
                                G_SYS_DBG_LOG_V("FATAL - Failed to send Event message back to Application\r\n");
                                if (event->pData) {
                                    dxMemFree (event->pData);
                                }
                                dxMemFree (event);
                            }
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_OUT_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        BLEDeviceCtrl_result_t dataOutResult = DEV_CENTRAL_CTRL_SUCCESS;
                        dataOutResult = BLEDevCentralCtrl_ConcurrentPeripheralDataPayloadIntegrityCheck (BCMessageRsp->Payload);

                        if (dataOutResult == DEV_CENTRAL_CTRL_SUCCESS) {
                            BlePeripheralDataTransfer_t* dataOut = dxMemAlloc ("Concurrent peripheral data out response", 1, sizeof (BlePeripheralDataTransfer_t));
                            memcpy (dataOut, BCMessageRsp->Payload, sizeof (BlePeripheralDataTransfer_t));
                            PushConcurrentPeripheralRoleDataTransferReportToInJobQueue (dataOut);
                        } else {
                            G_SYS_DBG_LOG_V ("WARNING: BLEDevCentralCtrl_ConcurrentPeripheralDataPayloadIntegrityCheck Failed\r\n");
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_AUX_TYPE_DATA_IN_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        BLEDeviceCtrl_result_t dataInResult = DEV_CENTRAL_CTRL_SUCCESS;
                        dataInResult = BLEDevCentralCtrl_ConcurrentPeripheralDataPayloadIntegrityCheck (BCMessageRsp->Payload);

                        if (dataInResult == DEV_CENTRAL_CTRL_SUCCESS) {
                            CollectConcurrentPeripheralDataInPayload (dxTRUE, (BlePeripheralDataTransfer_t*) BCMessageRsp->Payload);
                        } else {
                            CollectConcurrentPeripheralDataInPayload (dxFALSE, (BlePeripheralDataTransfer_t*) BCMessageRsp->Payload);
                        }

                    } else if ((BCMessageRsp->Header.CommandAux == CONCURRENT_PERIPHERAL_AUX_TYPE_CCCD_SET_DONE_RSP) && (BCMessageRsp->Header.PayloadLen)) {

                        BleConcurrentPeripheralCccd_t* cccd = (BleConcurrentPeripheralCccd_t*) BCMessageRsp->Payload;
                        BleConcurrentPeripheralCccd_t* peripheralCccd;
                        uint16_t availableIndex = MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE + 1;
                        uint16_t i;

                        // Update CCCD
                        for (i = 0; i < MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE; i++) {
                            peripheralCccd = peripheralCccds[i];

                            if ((peripheralCccd == NULL) && (availableIndex == (MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE + 1))) {
                                availableIndex = i;
                            }
                            if (peripheralCccd != NULL) {
                                if ((peripheralCccd->ServiceUUID == cccd->ServiceUUID) && (peripheralCccd->CharacteristicUUID == cccd->CharacteristicUUID)) {
                                    peripheralCccd->CCCD = cccd->CCCD;
                                    availableIndex = MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE + 1;
                                    break;
                                }
                            }
                        }

                        // If not found create new CCCD
                        if (availableIndex != (MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE + 1)) {
                            peripheralCccd = dxMemAlloc ("peripheralCccd", 1, sizeof (BleConcurrentPeripheralCccd_t));
                            memcpy (peripheralCccd, cccd, sizeof (BleConcurrentPeripheralCccd_t));
                            peripheralCccds[availableIndex] = peripheralCccd;
                        }

                    }
                }
                break;

                default :
#if 1
#pragma message ( "REMINDER: Unknown BLE Event sent to application is ENABLED - Suggest to remove in production" )
                {
                    EventReportObj_t* FreeFormMsgData = dxMemAlloc( "FreeForm Message data in event Buff", 1, sizeof(EventReportObj_t));

                    uint8_t* UnknownHeaderData = dxMemAlloc( "Msg Buff", 1, sizeof(BleBridgeCommMsg_t));

                    memcpy(UnknownHeaderData, BCMessageRsp, sizeof(BleBridgeCommMsg_t));

                    FreeFormMsgData->Header.ReportEvent = DEV_MANAGER_EVENT_UNKNOWN;
                    FreeFormMsgData->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                    FreeFormMsgData->pData = UnknownHeaderData;

                    if (RegisteredEventFunction)
                    {

                        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, FreeFormMsgData );
                        if (EventResult != DXOS_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL - (L=%d)Failed to send Event message back to Application\r\n", __LINE__);
                            if (FreeFormMsgData->pData)
                                dxMemFree(FreeFormMsgData->pData);

                            dxMemFree(FreeFormMsgData);
                        }
                    }

                }
#endif
                    break;
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error popping message from BridgeComm response message queue, Result = %d\r\n", PopResult);
        }

#if 0

#pragma message ("WARNING: BLE UART RAW DATA IN BUMP ENABLED - NOT FOR PRODUCTION!!" )

        uint8_t* BLERawData = (uint8_t*)BCMessageRsp;
        if (BCMessageRsp->Header.CommandAux == SEND_PERIPHERAL_ROLE_AUX_TYPE_DATA_IN_RSP)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLE RAW IN BEGIN\r\n");
            uint16_t i = 0;
            for (i = 0; i< sizeof(BleGatewayHeader_t); i++)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%02X", *(BLERawData +i));
            }

            if (BCMessageRsp->Header.PayloadLen)
            {
                uint16_t j = 0;
                for (j = 0; j < BCMessageRsp->Header.PayloadLen + 2; j++)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%02X", *(BLERawData +i+j));
                }
            }

            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "\r\n");
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLE RAW IN END\r\n");

            uint16_t ClacCRC = 0;
            if (BCMessageRsp->Header.PayloadLen)
            {
                uint16_t k = 0;
                for (k =0; k < BCMessageRsp->Header.PayloadLen; k++)
                {
                    ClacCRC =  crc16_P8005_update(ClacCRC, BCMessageRsp->Payload[k]);
                }
                //uint16_t ClacCRC = Crc16((uint8_t*)BCMessageRsp->Payload, BCMessageRsp->Header.PayloadLen, 0);
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "PayloadCRC (len=%d) = %d\r\n", BCMessageRsp->Header.PayloadLen, ClacCRC);
            }
        }

#endif

        if (BCMessageRsp)
            dxMemFree(BCMessageRsp);

    }

    END_OF_TASK_FUNCTION;
}

static TASK_RET_TYPE Ble_Manager_UnPairDevice_Report_thread_main( TASK_PARAM_TYPE arg )
{

    while(1)
    {
        void* DevUnpairReportObj = NULL;

        dxQueueReceive( BleDeviceUnPairDeviceReportQueue, &DevUnpairReportObj, DXOS_WAIT_FOREVER );

        if (DevUnpairReportObj)
        {
            EventReportObj_t* EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
            EventReportObj->Header.IdentificationNumber = 0;
            EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_UNPAIR_DEVICE_REPORT;
            EventReportObj->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
            EventReportObj->pData = DevUnpairReportObj;

            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
            if (EventResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL Ble Man UnPai report - Failed to send Event message back to Application\r\n");
                dxMemFree(DevUnpairReportObj);
                dxMemFree(EventReportObj);
            }

            //NOTE: If event sent success We are NOT going to free DevUnpairReportObj queue here.. we will leave it to application to call BleManagerCleanEventArgumentObj
            //      when calling event function.
        }
    }

    END_OF_TASK_FUNCTION;
}



static void CentralControlFunctionEvent(void* arg)
{

    uint32_t*       EventFlag = (uint32_t*)arg;
    dxTime_t SystemTime = dxTimeGetMS();

    /* WARNING
     * -------
     * PLEASE read BLEDevCentralCtrl_RegisterForFunctionEvent for some IMPORTANT restriction on central event handling and limitation!
     *
     */

    if (*EventFlag & CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG)
    {
        *EventFlag ^= CENTRAL_CONN_STATUS_UPDATE_EVENT_FLAG;

        const BleCentralStates_t* CentralConnState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

        BleCentralStates_t* CentralStateResponse = dxMemAlloc( "Central State Resp Msg Buff", 1, sizeof(BleCentralStates_t));
        memcpy(CentralStateResponse, CentralConnState, sizeof(BleCentralStates_t));
        if (PushCentralStateReportToInJobQueue(CentralStateResponse) == dxFALSE)
        {
            dxMemFree(CentralStateResponse);
        }

        if (CentralConnState->CentralState == BLE_STATE_IDLE)
        {
            //We are going to send Start Scan again when BLE becomes IDLE.
            if (ContinueAutoScan)
            {
                if (!BleRepeater_IsActive ()) {
                    BLEDevCentralCtrl_StartScan(4000);
                }
            }

        }
        else if (CentralConnState->CentralState == BLE_PERIPHERAL_STATE_IDLE)
        {
            //If there is custom advertisement data that we need to send we make sure to send it before starting new adv cycle.
            //However, if none then we make sure NOT to overwrite the manufacture data and use the default right from the BLEBridge FW (For backward comtibility)
            uint8_t i = 0;
            for (i = 0; i < sizeof(PeripheralRoleCustomAdvData); i++)
            {
                if (PeripheralRoleCustomAdvData.Payload[i] != 0x00)
                {
                    BLEDevCentralCtrl_PeripheralAdvertisementManufacturerDataUpdate(&PeripheralRoleCustomAdvData);
                    break;
                }
            }

            //Automatically switch to advertising. Application layer will handle the top level process when ble manager role is Peripheral
            BLEDevCentralCtrl_PeripheralStartAdvertising();
        }

        ConnectionStatusLastUpdatedSystemTime = SystemTime;

        //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "CentralConnState %d, TermReason = %d\r\n", CentralConnState->CentralState, CentralConnState->TerminationReason);

    }


    if (*EventFlag & CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG)
    {
        *EventFlag ^= CENTRAL_DISCOVERY_STATUS_UPDATE_EVENT_FLAG;

        const BleCentralDiscoveryStates_t* CentralDiscoveryState = BLEDevCentralCtrl_GetLastKnownDiscoveryStatus();

        switch (CentralDiscoveryState->DiscoveryErrorReason)
        {
            case BLE_DISC_SERVICE_NOT_FOUND_ERROR:
            case BLE_DISC_SERVICE_TIME_OUT_ERROR:
            case BLE_DISC_SERVICE_ERROR:
            case BLE_DISC_SERVICE_UNABLE_TO_EXECUTE:
            case BLE_DISC_CHAR_NOT_FOUND_ERROR:
            case BLE_DISC_CHAR_TIME_OUT_ERROR:
            case BLE_DISC_CHAR_ERROR:
            {
                BleDeviceCharDiscoveryRspMsg_t* CharDiscoveryResponse = dxMemAlloc( "Char Discv Resp Msg Buff", 1, sizeof(BleDeviceCharDiscoveryRspMsg_t));

                CharDiscoveryResponse->ErrorCode = CentralDiscoveryState->DiscoveryErrorReason;
                CharDiscoveryResponse->DiscoveryIdHandShake = 0;
                CharDiscoveryResponse->CharHandlerID = 0;

                if (PushDiscoveryCharReportToInJobQueue(CharDiscoveryResponse) == dxFALSE)
                {
                    dxMemFree(CharDiscoveryResponse);
                }

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BleDisc-CentralDiscoveryState %d, DiscoveryError = %d\r\n", CentralDiscoveryState->ServiceDiscoveryState, CentralDiscoveryState->DiscoveryErrorReason );

            }
                break;


            case BLE_CHAR_WRITE_TIMEOUT:
            case BLE_CHAR_WRITE_ERROR:
            case BLE_CHAR_WRITE_UNABLE_TO_EXECUTE:
            {
                BleDeviceWriteCharRspMsg_t* WriteCharResponse = dxMemAlloc( "Char write Resp Msg Buff", 1, sizeof(BleDeviceWriteCharRspMsg_t));

                WriteCharResponse->ErrorCode = CentralDiscoveryState->DiscoveryErrorReason;
                WriteCharResponse->CharHandlerIDToWrite = 0;
                WriteCharResponse->WriteIdHandShake = 0;

                if (PushWriteCharReportToInJobQueue(WriteCharResponse) == dxFALSE)
                {
                    dxMemFree(WriteCharResponse);
                }

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BleCharW-CentralDiscoveryState %d, DiscoveryError = %d\r\n", CentralDiscoveryState->ServiceDiscoveryState, CentralDiscoveryState->DiscoveryErrorReason);

            }
                break;

            case BLE_CHAR_READ_TIMEOUT:
            case BLE_CHAR_READ_ERROR:
            case BLE_CHAR_READ_UNABLE_TO_EXECUTE:
            {

                BleDeviceReadCharRspMsg_t* ReadCharResponse = dxMemAlloc( "Char read Resp Msg Buff", 1, sizeof(BleDeviceReadCharRspMsg_t));

                ReadCharResponse->ErrorCode = CentralDiscoveryState->DiscoveryErrorReason;
                ReadCharResponse->CharHandlerIDToRead = 0;
                ReadCharResponse->ReadIdHandShake = 0;
                ReadCharResponse->DataReadLen = 0;

                if (PushReadCharReportToInJobQueue(ReadCharResponse) == dxFALSE)
                {
                    dxMemFree(ReadCharResponse);
                }

                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BleCharR-CentralDiscoveryState %d, DiscoveryError = %d\r\n", CentralDiscoveryState->ServiceDiscoveryState, CentralDiscoveryState->DiscoveryErrorReason);

            }
                break;

            default:
                break;
        }


    }

    if (*EventFlag & CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG)
    {
        *EventFlag ^= CENTRAL_CHARACTERISTIC_DISCOVERY_RSP_EVENT_FLAG;

        const BleCentralDiscoverCharResponse_t* CentralCharDiscovery = BLEDevCentralCtrl_GetLastCharacteristicDiscHandler();

        BleDeviceCharDiscoveryRspMsg_t* CharDiscoveryResponse = dxMemAlloc( "Char Discv Resp Msg Buff", 1, sizeof(BleDeviceCharDiscoveryRspMsg_t));

        CharDiscoveryResponse->ErrorCode = 0;
        CharDiscoveryResponse->DiscoveryIdHandShake = CentralCharDiscovery->DiscoveryIdHandShake;
        CharDiscoveryResponse->CharHandlerID = CentralCharDiscovery->CharHandlerID;

        if (PushDiscoveryCharReportToInJobQueue(CharDiscoveryResponse) == dxFALSE)
        {
            dxMemFree(CharDiscoveryResponse);
        }

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HandShakeID %d, CharDiscoveryHandlerID = %d\r\n", CentralCharDiscovery->DiscoveryIdHandShake, CentralCharDiscovery->CharHandlerID);

    }

    if (*EventFlag & CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG)
    {

        *EventFlag ^= CENTRAL_CHARACTERISTIC_WRITE_RSP_EVENT_FLAG;

        const BleCentralWriteCharResponse_t* CharWriteRsp = BLEDevCentralCtrl_GetLastCharacteristicWriteRsp();

        BleDeviceWriteCharRspMsg_t* WriteCharResponse = dxMemAlloc( "Char write Resp Msg Buff", 1, sizeof(BleDeviceWriteCharRspMsg_t));

        WriteCharResponse->ErrorCode = 0;
        WriteCharResponse->CharHandlerIDToWrite = CharWriteRsp->CharHandlerIDToWrite;
        WriteCharResponse->WriteIdHandShake = CharWriteRsp->WriteIdHandShake;

        if (PushWriteCharReportToInJobQueue(WriteCharResponse) == dxFALSE)
        {
            dxMemFree(WriteCharResponse);
        }

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "WriteIdHandShake %d, CharHandlerIDToWrite %d\r\n", CharWriteRsp->WriteIdHandShake, CharWriteRsp->CharHandlerIDToWrite);


    }

    if (*EventFlag & CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG)
    {

        *EventFlag ^= CENTRAL_CHARACTERISTIC_READ_RSP_EVENT_FLAG;

        const BleCentralReadCharResponse_t* CharReadRsp = BLEDevCentralCtrl_GetLastCharacteristicReadRsp();

        BleDeviceReadCharRspMsg_t* ReadCharResponse = dxMemAlloc( "Char read Resp Msg Buff", 1, sizeof(BleDeviceReadCharRspMsg_t));

        ReadCharResponse->ErrorCode = 0;
        ReadCharResponse->CharHandlerIDToRead = CharReadRsp->CharHandlerIDToRead;
        ReadCharResponse->ReadIdHandShake = CharReadRsp->ReadIdHandShake;
        ReadCharResponse->DataReadLen = CharReadRsp->DataReadLen;
        memcpy(ReadCharResponse->DataRead, CharReadRsp->DataRead, CHAR_READ_WRITE_DATA_MAX);

        if (PushReadCharReportToInJobQueue(ReadCharResponse) == dxFALSE)
        {
            dxMemFree(ReadCharResponse);
        }

        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "ReadIdHandShake %d, CharHandlerIDToRead %d\r\n", CharReadRsp->ReadIdHandShake, CharReadRsp->CharHandlerIDToRead);

        /*
        if (CharReadRsp->DataReadLen)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLE Data Read Rsp\r\n");
            uint32_t i = 0;
            for (i = 0; i < CharReadRsp->DataReadLen; i++)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "0X%x,", CharReadRsp->DataRead[i] );
                if (i == (CharReadRsp->DataReadLen - 1))
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "\r\n");
                }
            }
        }

        */
    }

    if (*EventFlag & CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG)  {

        *EventFlag ^= CENTRAL_CONCURRENT_PERIPHERAL_STATUS_UPDATE_EVENT_FLAG;

        const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
        BleConcurrentPeripheralGetStatusResponse_t* statusResponse = dxMemAlloc ("Central State Resp Msg Buff", 1, sizeof (BleConcurrentPeripheralGetStatusResponse_t));
        statusResponse->state = bleCcpState->ConcurrentPeripheralState;

        EventReportObj_t* eventReport = dxMemAlloc ("Event Report Msg Buff", 1, sizeof (EventReportObj_t));
        eventReport->Header.IdentificationNumber = 0;
        eventReport->Header.ReportEvent = DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_STATUS_REPORT;
        eventReport->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
        eventReport->pData = (uint8_t*) statusResponse;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent (&EventWorkerThread, RegisteredEventFunction, eventReport);
        if (EventResult != DXOS_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL Ble Man UnPai report - Failed to send Event message back to Application\r\n");
            dxMemFree (statusResponse);
            dxMemFree (eventReport);
        }

        /* NOTE:
         * Because we don't need to check BleCentralConcurrentPeripheralState_t at InJob function
         * so we leave PushConcurrentPeripheralStatusReportToInJobQueue() unimplemented.
         * TODO:
         * Need to implement this function once we need.
         */
    }

    return;
}


/******************************************************
 *               Function Definitions
 ******************************************************/

BLEManager_result_t BleManagerInit(dxEventHandler_t EventFunction)
{

    dxOS_RET_CODE RetResult = DXOS_SUCCESS;

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    m_ble_reset_mutex = dxMutexCreate();

    BLEDevKeeper_Init();

    BLEDevCentralCtrl_RegisterForFunctionEvent(CentralControlFunctionEvent);

    BLEDevBridgeComm_Init();

    memset( &PeripheralRoleCustomAdvData, 0, sizeof(PeripheralRoleCustomAdvData) );

    Aes128ctx_ble_enc =  dxAESSW_ContextAlloc();
    Aes128ctx_ble_dec =  dxAESSW_ContextAlloc();


    if (BleDeviceJobQueue == NULL)
    {
        BleDeviceJobQueue = dxQueueCreate (MAXIMUM_DEVICE_JOB_QUEUE_DEPT_ALLOWED, sizeof(BleDeviceJobQueue_t*));
        if (BleDeviceJobQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceJobQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }


    if (BleDeviceInJobReportQueue == NULL)
    {
        BleDeviceInJobReportQueue = dxQueueCreate(MAXIMUM_DEVICE_IN_JOB_REPORT_QUEUE_DEPT_ALLOWED, sizeof(void*));
        if (BleDeviceInJobReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceInJobReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceInJobCharDiscvReportQueue == NULL)
    {
        BleDeviceInJobCharDiscvReportQueue = dxQueueCreate(MAXIMUM_DEVICE_CHAR_DISCOVERY_REPORT_QUEUE_DEPT_ALLOWED, sizeof(BleDeviceCharDiscoveryRspMsg_t*));
        if (BleDeviceInJobCharDiscvReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceInJobCharDiscvReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceInJobWriteCharReportQueue == NULL)
    {
        BleDeviceInJobWriteCharReportQueue = dxQueueCreate(MAXIMUM_DEVICE_READ_WRITE_CHAR_REPORT_QUEUE_DEPT_ALLOWED, sizeof(BleDeviceWriteCharRspMsg_t*));
        if (BleDeviceInJobWriteCharReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceInJobWriteCharReportQueue \r\n");
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceInJobReadCharReportQueue == NULL)
    {
        BleDeviceInJobReadCharReportQueue = dxQueueCreate(MAXIMUM_DEVICE_READ_WRITE_CHAR_REPORT_QUEUE_DEPT_ALLOWED, sizeof(BleDeviceReadCharRspMsg_t*));
        if (BleDeviceInJobReadCharReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceInJobReadCharReportQueue \r\n");
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceInJobCentralStateReportQueue == NULL)
    {
        BleDeviceInJobCentralStateReportQueue = dxQueueCreate(MAXIMUM_DEVICE_CENTRAL_STATE_REPORT_QUEUE_DEPT_ALLOWED, sizeof(BleCentralStates_t*));
        if (BleDeviceInJobCentralStateReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceInJobCentralStateReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceUnPairDeviceReportQueue == NULL)
    {
        BleDeviceUnPairDeviceReportQueue = dxQueueCreate(MAXIMUM_UNPAIR_DEVICE_REPORT_QUEUE_DEPT_ALLOWED, sizeof(void*));
        if (BleDeviceUnPairDeviceReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDeviceUnPairDeviceReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDevicePeripheralDataOutTransferReportQueue == NULL)
    {
        BleDevicePeripheralDataOutTransferReportQueue = dxQueueCreate(MAXIMUM_PERIPHERAL_DATA_TRANSFER_REPORT_QUEUE_DEPT_ALLOWED, sizeof(void*));
        if (BleDevicePeripheralDataOutTransferReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDevicePeripheralDataOutTransferReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDevicePeripheralDataInTransferReportQueue == NULL)
    {
        BleDevicePeripheralDataInTransferReportQueue = dxQueueCreate(MAXIMUM_PERIPHERAL_DATA_TRANSFER_REPORT_QUEUE_DEPT_ALLOWED, sizeof(void*));
        if (BleDevicePeripheralDataInTransferReportQueue == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleDevicePeripheralDataInTransferReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    if (BleDeviceConcurrentPeripheralDataOutTransferReportQueue == NULL)
    {
        BleDeviceConcurrentPeripheralDataOutTransferReportQueue = dxQueueCreate(MAXIMUM_PERIPHERAL_DATA_TRANSFER_REPORT_QUEUE_DEPT_ALLOWED, sizeof(void*));
        if (BleDeviceConcurrentPeripheralDataOutTransferReportQueue == NULL)
        {
            G_SYS_DBG_LOG_V( "Error creating BleDeviceConcurrentPeripheralDataOutTransferReportQueue \r\n" );
            RetResult = DEV_MANAGER_RTOS_ERROR;
            goto EndInit;
        }
    }

    RetResult = dxTaskCreate(BridgeComm_Response_Process_thread_main,
                               BRIDGECOMM_PROCESS_THREAD_NAME,
                               BRIDGECOMM_STACK_SIZE,
                               0,
                               BRIDGECOMM_PROCESS_THREAD_PRIORITY,
                               &BridCommResponseProcess_thread);
    if (RetResult != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BridCommResponseProcess_thread (Error Code = %d)\r\n", RetResult );
        result = DEV_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&BridCommResponseProcess_thread);

    RetResult = dxTaskCreate(Ble_Manager_Process_thread_main,
                               BLE_MANAGER_PROCESS_THREAD_NAME,
                               BLE_MANAGER_STACK_SIZE,
                               0,
                               BLE_MANAGER_PROCESS_THREAD_PRIORITY,
                               &BleManagerMainProcess_thread);
    if ( RetResult != DXOS_SUCCESS )
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleManagerMainProcess_thread (Error Code = %d)\r\n", RetResult );
        result = DEV_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&BleManagerMainProcess_thread);

    RetResult = dxTaskCreate(Ble_Manager_UnPairDevice_Report_thread_main,
                               BLE_MANAGER_UNPAIR_DEVICE_REPORT_THREAD_NAME,
                               BLE_MANAGER_UNPAIR_DEVICE_REPORT_STACK_SIZE,
                               0,
                               BLE_MANAGER_UNPAIR_DEVICE_REPORT_THREAD_PRIORITY,
                               &BleManagerUnPairDevice_thread);
    if ( RetResult != DXOS_SUCCESS )
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error creating BleManagerUnPairDevice_thread (Error Code = %d)\r\n", RetResult );
        result = DEV_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&BleManagerUnPairDevice_thread);

    CentralStatusSessionTimer = dxTimerCreate("",
                                              CENTRAL_STATUS_CHECK_SESSION_TIME_INTERVAL,
                                              dxTRUE,
                                              &CentralStatusSessionTimer,
                                              CentralStatusCheckHandler);
    if (CentralStatusSessionTimer == NULL)
    {
        result = DEV_MANAGER_RTOS_ERROR;
        goto EndInit;
    }
    else
    {
        if (dxTimerStart(CentralStatusSessionTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unable to start central status session timer! Possible issue - Stability\r\n");
        }
    }


    if (EventFunction)
    {
        if (RegisteredEventFunction == NULL)    //First time, we create worker thread first
        {
            RegisteredEventFunction = EventFunction;

            dxOS_RET_CODE WorkerThreadResult = dxWorkerTaskCreate("BLEMGR W",
                                                                  &EventWorkerThread,
                                                                  DX_DEFAULT_WORKER_PRIORITY,
                                                                  BLE_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE,
                                                                  BLE_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE);
            if (WorkerThreadResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BleManagerInit - Failed to create worker thread\r\n");
                RegisteredEventFunction = NULL;
                result = DEV_MANAGER_RTOS_ERROR;
                goto EndInit;
            }
            else
            {
                GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "BLEManager Event Thread");
            }

        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BleManagerInit - MUST register EventFunction, system may become unstable if not registered\r\n");
        result = DEV_MANAGER_INVALID_PARAMETER;
        goto EndInit;
    }

    memset (peripheralCccds, 0, sizeof (BleConcurrentPeripheralCccd_t*) * MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE);

    //Set BLE mode to central by default and bring BLE out of reset
    gDeviceScanReportAllowed = dxTRUE;
    SetBLERoleToCentral(dxTRUE);

    BleRepeater_Init (&BleRepeater_RegisteredEventFunction);


EndInit:
    return result;

}

extern BLEManager_States_t BleManagerCurrentState()
{
    BLEManager_States_t CurrentState = DEV_MANAGER_CENTRAL_STATE_UNKNOWN;

    const BleCentralStates_t* ConnectionState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

    switch (ConnectionState->CentralState)
    {
        case BLE_STATE_IDLE:
            CurrentState = DEV_MANAGER_CENTRAL_STATE_IDLE;
            break;

        case BLE_STATE_SCANNING:
            CurrentState = DEV_MANAGER_CENTRAL_STATE_SCANNING;
            break;

        case BLE_STATE_CONNECTING:
            CurrentState = DEV_MANAGER_CENTRAL_STATE_CONNECTING;
            break;

        case BLE_STATE_DISCONNECTING:
            CurrentState = DEV_MANAGER_CENTRAL_STATE_DISCONNECTING;
            break;

        case BLE_PERIPHERAL_STATE_IDLE:
            CurrentState = DEV_MANAGER_PERIPHERAL_STATE_IDLE;
            break;

        case BLE_PERIPHERAL_STATE_ADVERTISING:
            CurrentState = DEV_MANAGER_PERIPHERAL_STATE_ADVERTISING;
            break;

        case BLE_PERIPHERAL_STATE_CONNECTED:
            CurrentState = DEV_MANAGER_PERIPHERAL_STATE_CONNECTED;
            break;

        default:
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: BleManagerCurrentState unhandled CentralState state = %d (Developer Please check!)\r\n", ConnectionState->CentralState);
            break;
    }

    return CurrentState;
}


extern BLEManager_ConcurrentPeripheralState_t BleManagerConcurrentPeripheralState ()
{
    BLEManager_ConcurrentPeripheralState_t ccpState = DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_UNKNOWN;

    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();

    switch (bleCcpState->ConcurrentPeripheralState) {

        case BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT :
            ccpState = DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT;
            break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_IDLE :
            ccpState = DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_IDLE;
            break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_ADVERTISING :
            ccpState = DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_ADVERTISING;
            break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_CONNECTED :
            ccpState = DEV_MANAGER_CONCURRENT_PERIPHERAL_STATE_CONNECTED;
            break;
        default :
            G_SYS_DBG_LOG_V1( "WARNING: BleManagerConcurrentPeripheralState unhandled CentralState state = %d (Developer Please check!)\r\n", bleCcpState->ConcurrentPeripheralState);
            break;
    }

    return ccpState;
}


extern BLEManager_result_t BleManagerSetRoleToCentral()
{
    //TODO: let's move this entire operation to task asynch?...

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    gDeviceScanReportAllowed = dxTRUE;

    SetBLERoleToCentral(dxTRUE);

    //Start central status check timer if its running
    if (dxTimerIsTimerActive( CentralStatusSessionTimer ) != dxTRUE)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Re-Starting :CentralStatusSessionTimer\r\n");

        dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
        if (dxTimerStart(CentralStatusSessionTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Unable to re-start central status session timer! Possible issue - Stability\r\n");
        }
    }

    return result;
}

extern BLEManager_result_t BleManagerSetRoleToCentralFwUpdateBoot()
{
    //TODO: let's move this entire operation to task asynch?...

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    SetBLERoleToCentral(dxFALSE);

    //Stop central status check timer if its running
    if (dxTimerIsTimerActive( CentralStatusSessionTimer ) == dxTRUE)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Stopping :CentralStatusSessionTimer\r\n");

        dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
    }

    BLEDevCentralCtrl_SystemResetToSBL();

    return result;
}


extern BLEManager_result_t BleManagerSetRoleToFwUpdate()
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    //Stop central status check timer if its running
    if (dxTimerIsTimerActive( CentralStatusSessionTimer ) == dxTRUE)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Stopping :CentralStatusSessionTimer\r\n");

        dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
    }

    return result;

}

extern BLEManager_result_t BleManagerSetRoleToPeripheral()
{

    //TODO: let's move this entire operation to task asynch?...

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if (BleRepeater_IsActive ()) {
        BleRepeater_Reset ();
        dxTimeDelayMS(1000);
    }

    const BleCentralStates_t* ConnectionState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

    if ((ConnectionState->CentralState != BLE_STATE_IDLE) && (ConnectionState->CentralState != BLE_STATE_SCANNING))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Changing to role peripheral BUT from a state that is not allowed (%d) (Developer Please check!)\r\n", ConnectionState->CentralState);
    }

    SetBLERoleToPeripheral();

    //Stop central status check timer if its running
    if (dxTimerIsTimerActive( CentralStatusSessionTimer ) == dxTRUE)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Stopping :CentralStatusSessionTimer\r\n");

        dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
    }

    return result;
}


extern BLEManager_result_t BleManagerReset()
{

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BLEDevKeeper_RemoveAllDeviceFromKeeperList();

    return result;
}

extern BLEManager_result_t BleManagerSystemInfoGet(SystemInfoReportObj_t* SystemInfoContainer)
{
    BLEManager_result_t result = DEV_MANAGER_NOT_AVAILABLE;

    if (SystemInfoReceived && SystemInfoContainer)
    {
        memcpy(SystemInfoContainer, &SystemInfo, sizeof(SystemInfoReportObj_t));
        result = DEV_MANAGER_SUCCESS;
    }

    return result;
}

extern BLEManager_result_t BleManagerSystemInfoRequest()
{

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    //Simply signal the flag as false and it will re-query again in the next CentralStatusCheckHandler check
    SystemInfoReceived = dxFALSE;

    return result;
}

extern BLEManager_result_t BleManagerSuspend()
{

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleManager_TakeBLEResetMutex();
    //First put BLE to reset
    dxGPIO_Write(DK_BLE_RESET, DXPIN_LOW);
    dxTimeDelayMS(500);
    BleManager_GiveBLEResetMutex();

    // Notify State Machine BLE has been reset
    SendBleResetNotification (DEV_MANAGER_RESET_TO_UNKNOWN);

    //We clear the internal state
    BLEDevCentralCtrl_ClearConnectionStatus();

    //Stop central status check timer if its running
    if (dxTimerIsTimerActive( CentralStatusSessionTimer ) == dxTRUE)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Stopping :CentralStatusSessionTimer\r\n");

        dxTimerStop(CentralStatusSessionTimer, DXOS_WAIT_FOREVER);
    }

    return result;

}

extern BLEManager_result_t BleManagerScanReportSuspend()
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    gDeviceScanReportAllowed = dxFALSE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Scan Report SUSPENDED BleManagerScanReportSuspend\r\n");

    return result;
}

extern BLEManager_result_t BleManagerScanReportResume()
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    gDeviceScanReportAllowed = dxTRUE;

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Scan Report RESUMED BleManagerScanReportResume\r\n");

    return result;
}

extern EventReportHeader_t BleManagerGetEventObjHeader(void* ReportedEventArgObj)
{
    EventReportHeader_t Header;
    Header.ReportEvent = DEV_MANAGER_EVENT_UNKNOWN;

    if (ReportedEventArgObj)
    {
        EventReportObj_t* EventObj = (EventReportObj_t*)ReportedEventArgObj;
        memcpy(&Header, &EventObj->Header, sizeof(EventReportHeader_t));
    }

    return Header;
}

dxBOOL BleManagerIsMaximumPeripehralSupportReached()
{
    return BLEDevKeeper_IsMaximumCapacityReached();
}

BLEManager_result_t BleManagerPairDeviceTask_Asynch(stPairingInfo_t* PairingInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_PAIR_DEVICE;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    memcpy(DevJobMsg->JobData, PairingInfo, sizeof(stPairingInfo_t));

    if (BleDeviceJobQueue)
    {

        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

    return result;

}

BLEManager_result_t BleManagerAddDeviceTask_Asynch(DeviceAddInfo_t* DeviceAddInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if (DeviceAddInfo == NULL)
        return DEV_MANAGER_INVALID_PARAMETER;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_ADD_DEVICE;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    memcpy (DevJobMsg->JobData, DeviceAddInfo, sizeof (DeviceAddInfo_t));

    if (BleDeviceJobQueue)
    {

        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {

            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }


    return result;
}

extern BLEManager_result_t BleManagerUnPairDeviceTask_Asynch(DevAddress_t DeviceAddr, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    DeviceAddress_t DevAddress;
    DevAddress.AddressType = ADDRTYPE_PUBLIC;
    DevAddress.AddrLen = B_ADDR_LEN_8;
    DevAddress.pAddr = DeviceAddr.Addr;

    int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(DevAddress);

    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {
        const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DeviceIndex);

        if (DeviceInfoKp)
        {
            BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));
            DevJobMsg->JobType = DEV_MANAGER_JOB_UNPAIR_DEVICE;
            DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
            DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
            DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
            DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

            memcpy(DevJobMsg->JobData, &DeviceIndex, sizeof(DeviceIndex));

            if (BleDeviceJobQueue)
            {

                if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                    dxMemFree(DevJobMsg);
                    result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
                }
                else
                {

                    if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                        dxMemFree(DevJobMsg);
                        result = DEV_MANAGER_RTOS_ERROR;
                    }
                }
            }

        }
        else
        {
            result = DEV_MANAGER_DEVICE_TO_ACESS_NOT_IN_KEEPERS_LIST;
        }
    }
    else
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
    }

    return result;

}


BLEManager_result_t BleManagerCleanAllDeviceFromListTask_Asynch(dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));
    DevJobMsg->JobType = DEV_MANAGER_JOB_CLEANUP_ALL_DEVICE_FROMKEEPER_LIST;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    if (BleDeviceJobQueue)
    {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }

    }

    return result;

}


BLEManager_result_t BleManagerGetDeviceKeeperList_Asynch(dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));
    DevJobMsg->JobType = DEV_MANAGER_JOB_GET_DEVICE_KEEPER_LIST;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    if (BleDeviceJobQueue)
    {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }

    }

    return result;

}


BLEManager_result_t BleManagerSetRenewSecurityTask_Asynch(stRenewSecurityInfo_t* SecurityInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_SET_RENEW_DEVICE_SECURITY;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    memcpy(DevJobMsg->JobData, SecurityInfo, sizeof(stRenewSecurityInfo_t));

    if (BleDeviceJobQueue)
    {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

    return result;

}

BLEManager_result_t BleManagerUpdateRTCTask_Asynch(stUpdatePeripheralRtcInfo_t* UpdateRtcInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_UPDATE_RTC;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_LOW; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    memcpy(DevJobMsg->JobData, UpdateRtcInfo, sizeof(stUpdatePeripheralRtcInfo_t));

    if (BleDeviceJobQueue)
    {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {

            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

    return result;

}

extern BLEManager_result_t BleManagerClearSecurityTask_Asynch(DevAddress_t DeviceAddr, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    DeviceAddress_t DevAddress;
    DevAddress.AddressType = ADDRTYPE_PUBLIC;
    DevAddress.AddrLen = B_ADDR_LEN_8;
    DevAddress.pAddr = DeviceAddr.Addr;

    int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(DevAddress);

    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {

        BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

        DevJobMsg->JobType = DEV_MANAGER_JOB_CLEAR_DEVICE_SECURITY;
        DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
        DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
        DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
        DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

        memcpy(DevJobMsg->JobData, &DeviceIndex, sizeof(DeviceIndex));

        if (BleDeviceJobQueue)
        {
            if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
            }
            else
            {
                if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                    dxMemFree(DevJobMsg);
                    result = DEV_MANAGER_RTOS_ERROR;
                }
            }
        }
    }
    else
    {
        result = DEV_MANAGER_INVALID_PARAMETER;

    }

    return result;
}

extern BLEManager_result_t BleManagerDeviceAccessWriteTask_Asynch(DeviceAccess_t DevAccessInfo, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t ReadWriteMode)
{

    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if (DevAccessInfo.DevHandlerID < 0)
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
    }
    else
    {
        const DeviceInfoKp_t* DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex(DevAccessInfo.DevHandlerID);

        if (DeviceInfoKp)
        {
            BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

            switch (ReadWriteMode)
            {
                case WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ:
                {
                    DevJobMsg->JobType = DEV_MANAGER_JOB_ACCESS_WRITE_FOLLOW_BY_ADV_READ_UPDATE_DEVICE;
                }
                break;

                case WRITE_ONLY:
                {
                    DevJobMsg->JobType = DEV_MANAGER_JOB_ACCESS_WRITE_DEVICE;
                }
                break;

                case READ_FOLLOW_BYAGGREGATIONUPDATE:
                {
                    DevJobMsg->JobType =  DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_AGGREGATION_UPDATE;
                }
                break;

                case READ_FOLLOW_BYDATA_REPORT:
                {
                    DevJobMsg->JobType = DEV_MANAGER_JOB_ACCESS_READ_FOLLOW_BY_DATA_REPORT;
                }
                break;

                default:
                    result = DEV_MANAGER_INVALID_PARAMETER;
                    goto Exit;
                    break;
            }

            DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
            DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
            DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
            DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

            memcpy(DevJobMsg->JobData, &DevAccessInfo, sizeof(DeviceAccess_t));

            if (BleDeviceJobQueue)
            {

                if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                    dxMemFree(DevJobMsg);
                    result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
                }
                else
                {

                    if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000 ) != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                        dxMemFree(DevJobMsg);
                        result = DEV_MANAGER_RTOS_ERROR;
                    }
                }
            }
        }
        else
        {
            result = DEV_MANAGER_DEVICE_TO_ACESS_NOT_IN_KEEPERS_LIST;
        }
    }

Exit:

    return result;

}

BLEManager_result_t BleManagerDeviceAccessWriteTaskExt_Asynch(uint8_t* PeripheralJobExecutionData, dxTime_t TimeOutReport, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleStandardJobExecHeader_t* JobExecHeader = (BleStandardJobExecHeader_t*)PeripheralJobExecutionData;

    DeviceAddress_t DevAddress;
    DevAddress.AddressType = ADDRTYPE_PUBLIC;
    DevAddress.AddrLen = B_ADDR_LEN_8;
    DevAddress.pAddr = JobExecHeader->PeripheralDeviceMacAddress.Addr;

    int16_t DeviceIndex = BLEDevKeeper_FindDeviceIndexFromGivenAddress(DevAddress);

    if (DeviceIndex != DEV_KEEPER_NOT_FOUND)
    {
        DeviceAccess_t DevAccessInfo;
        DevAccessInfo.DevHandlerID = DeviceIndex;
        DevAccessInfo.NeedConvertTo128bit = (JobExecHeader->CharFlag.bits.UUID_Len_b0 == JOB_EXEC_BLE_STD_UUID_128BIT) ? 1:0;
        DevAccessInfo.ServiceUUID = JobExecHeader->ServiceUUID;
        DevAccessInfo.CharacteristicUUID = JobExecHeader->CharacteristicUUID;
        DevAccessInfo.WriteDataLen = JobExecHeader->DataLen;

        if (JobExecHeader->DataLen <= CHAR_READ_WRITE_DATA_MAX_MPAGE)
        {
            memcpy(DevAccessInfo.WriteData, PeripheralJobExecutionData + sizeof(BleStandardJobExecHeader_t), JobExecHeader->DataLen);
            result = BleManagerDeviceAccessWriteTask_Asynch(DevAccessInfo, TimeOutReport, SourceOfOrigin, TaskIdentificationNumber, JobExecHeader->CharFlag.bits.ReadWriteMode_b1_3);
        }
        else
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL: DeviceAccess Write Data len is larger than CHAR_READ_WRITE_DATA_MAX_MPAGE \r\n" );
            result = DEV_MANAGER_INVALID_PARAMETER;
        }
    }
    else
    {
        result = DEV_MANAGER_DEVICE_TO_ACESS_NOT_IN_KEEPERS_LIST;
    }

    return result;
}


extern BLEManager_result_t BleManagerFwUpdateBlockWriteTask_Asynch(uint8_t* pFwBlockPayload, uint8_t len, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if ((pFwBlockPayload == NULL) || (len <= 0))
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
        return result;
    }

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_FW_BLOCK_WRTE;
    DevJobMsg->JobTimeOutTime = 1*SECONDS; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    //First byte is the len, follow by actual block data
    DevJobMsg->JobData[0] = len;
    memcpy(&DevJobMsg->JobData[1], pFwBlockPayload, len);

    if (BleDeviceJobQueue)
    {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

    return result;
}

extern BLEManager_result_t BleManagerFwUpdateDoneTask_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

    DevJobMsg->JobType = DEV_MANAGER_JOB_FW_UPDATE_DONE;
    DevJobMsg->JobTimeOutTime = 1*SECONDS; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    if (BleDeviceJobQueue)
    {

        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {

            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

    return result;
}

extern BLEManager_result_t BleManagerPeripheralRoleDataOutTransferTask_Asynch(eDataTransferMessageType MsgType, uint8_t* Payload, uint8_t len, dxTime_t TimeOutReport, uint64_t TaskIdentificationNumber)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if ((Payload == NULL) && (len))
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
    }
    else
    {

        const BleCentralStates_t* ConnectionState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

        if (ConnectionState->CentralState == BLE_PERIPHERAL_STATE_CONNECTED)
        {

            BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));

            DevJobMsg->JobType = DEV_MANAGER_JOB_PERIPHERAL_DATA_OUT_WRITE;
            DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
            DevJobMsg->JobSourceOfOrigin = 0;
            DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
            DevJobMsg->JobDisconnectMaxRetry = 0;

            DataTransferMsgHeader_t MsgHeader;
            MsgHeader.Preamble = DATA_TRANSFER_PREAMBLE;
            MsgHeader.MessageType = MsgType;
            MsgHeader.PayloadLen = len;

            memcpy(DevJobMsg->JobData, &MsgHeader, sizeof(DataTransferMsgHeader_t));

            if (Payload)
                memcpy(DevJobMsg->JobData+sizeof(DataTransferMsgHeader_t), Payload, len);

            if (BleDeviceJobQueue)
            {

                if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                    dxMemFree(DevJobMsg);
                    result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
                }
                else
                {
                    if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n" );
                        dxMemFree(DevJobMsg);
                        result = DEV_MANAGER_RTOS_ERROR;
                    }
                }
            }
        }
        else
        {
            result = DEV_MANAGER_DEVICE_PERIPHERALROLE_NOT_CONNECTED;
        }
    }

    return result;
}

extern BLEManager_result_t BleManagerPeripheralRoleCommonDataOutTransferTask_Asynch(uint8_t* Payload, uint8_t len, dxTime_t TimeOutReport, uint64_t TaskIdentificationNumber)
{
    return BleManagerPeripheralRoleDataOutTransferTask_Asynch(DATA_TRANSFER_MSG_TYPE_COMMON, Payload, len, TimeOutReport, TaskIdentificationNumber);
}


extern BLEManager_result_t BleManagerPeripheralRoleSetAdvManufactureCustomData_Asynch(uint8_t* AdvData, uint8_t AdvDataLen)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if ((AdvDataLen > DATA_TRANSFER_SIZE_PER_CHUNK) || (AdvData == NULL))
    {
        return DEV_MANAGER_INVALID_PARAMETER;
    }

    memset( &PeripheralRoleCustomAdvData, 0, sizeof(PeripheralRoleCustomAdvData) );
    memcpy(PeripheralRoleCustomAdvData.Payload, AdvData, AdvDataLen );

    const BleCentralStates_t* ConnectionState = BLEDevCentralCtrl_GetLastKnownConnectionStatus();

    if (ConnectionState->CentralState == BLE_PERIPHERAL_STATE_ADVERTISING)
    {
        //We send a stop advertisement so that our BLE manager can re-update the adv manufacturer data and re-kick off start advertisement cycle.
        BLEDevCentralCtrl_PeripheralStopAdvertising();
    }

    return result;
}


BLEManager_result_t BleManagerSetupRepeaterTask_Asynch (int16_t repeaterIndex, dxTime_t timeOutReport)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));
    const DeviceInfoKp_t* DeviceInfoKp;

    DevJobMsg->JobType = DEV_MANAGER_JOB_SETUP_REPEATER;
    DevJobMsg->JobTimeOutTime = timeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = 0;
    DevJobMsg->JobIdentificationNumber = 0;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex (repeaterIndex);
    if (DeviceInfoKp->DevAddress.AddrLen != B_ADDR_LEN_8) {
        result = DEV_MANAGER_RTOS_ERROR;
        goto fail;
    }

    memcpy(DevJobMsg->JobData, &repeaterIndex, sizeof (int16_t));

    if (BleDeviceJobQueue) {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__);
            dxMemFree (DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        } else {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000 ) != DXOS_SUCCESS) {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree (DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

fail :

    return result;
}


BLEManager_result_t BleManagerMonitorRepeaterTask_Asynch (int16_t repeaterIndex, dxTime_t timeOutReport)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    BleDeviceJobQueue_t* DevJobMsg = dxMemAlloc( "Ble Manager Job Msg Buff", 1, sizeof(BleDeviceJobQueue_t));
    const DeviceInfoKp_t* DeviceInfoKp;

    DevJobMsg->JobType = DEV_MANAGER_JOB_MONITOR_REPEATER;
    DevJobMsg->JobTimeOutTime = timeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = 0;
    DevJobMsg->JobIdentificationNumber = 0;
    DevJobMsg->JobDisconnectMaxRetry = DEFAULT_ON_JOB_DISCONNECTION_RETRY_MAX; //TODO: Maybe we should open this parameter to application to decide the number of retry?

    DeviceInfoKp = BLEDevKeeper_GetDeviceInfoFromIndex (repeaterIndex);
    if (DeviceInfoKp->DevAddress.AddrLen != B_ADDR_LEN_8) {
        result = DEV_MANAGER_RTOS_ERROR;
        goto fail;
    }

    memcpy(DevJobMsg->JobData, &repeaterIndex, sizeof (int16_t));

    if (BleDeviceJobQueue) {
        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__);
            dxMemFree (DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        } else {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000 ) != DXOS_SUCCESS) {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree (DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralGetStatusTask_Asynch (void) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    BLEDeviceCtrl_result_t devResult;

    devResult = BLEDevCentralCtrl_GetConcurrentPeripheralStatus ();
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Fail to call BLEDevCentralCtrl_GetConcurrentPeripheralStatus (): devResult=%d\r\n", devResult);
    }

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralCustomizeBdAddressTask_Asynch (
    BleCentralConnect_t* bleBdAddress,
    dxTime_t timeOutReport,
    uint64_t taskIdentificationNumber
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BleDeviceJobQueue_t* jobMessage;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    jobMessage = dxMemAlloc ("Ble Manager Job Msg Buff", 1, sizeof (BleDeviceJobQueue_t));
    jobMessage->JobType = DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS;
    jobMessage->JobTimeOutTime = timeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    jobMessage->JobSourceOfOrigin = 0;
    jobMessage->JobIdentificationNumber = taskIdentificationNumber;
    jobMessage->JobDisconnectMaxRetry = 0;

    memcpy (jobMessage->JobData, bleBdAddress, sizeof (BleCentralConnect_t));

    if (BleDeviceJobQueue) {

        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__);
            dxMemFree (jobMessage);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        } else {
            if (dxQueueSend (BleDeviceJobQueue, &jobMessage, 1000) != DXOS_SUCCESS) {
                G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree (jobMessage);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralDataOutTask_Asynch (
    eDataTransferMessageType MsgType,
    uint8_t* Payload,
    uint16_t length,
    dxTime_t TimeOutReport,
    uint64_t TaskIdentificationNumber
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BleDeviceJobQueue_t* DevJobMsg;
    ConcurrentDataTransferHeader_t MsgHeader;
    ConcurrentCharacteristicTransferHeader_t* TransferHeader;

    if ((Payload == NULL) && (length)) {
        result = DEV_MANAGER_INVALID_PARAMETER;
        G_SYS_DBG_LOG_V ("Invalid parameter, Payload=NULL\r\n");
        goto fail;
    }

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    TransferHeader = (ConcurrentCharacteristicTransferHeader_t*) Payload;

    // TODO: Reserved field is used to identify NOTIFY characteristic, perhap use AccessType if we can distinguish READ and NOTIFY
    if (TransferHeader->Reserved == DK_CHARACTERISTIC_PROPERTY_NOTIFY) {
        BleConcurrentPeripheralCccd_t* peripheralCccd;
        uint16_t availableIndex = MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE + 1;
        uint16_t i;

        // Find CCCD
        for (i = 0; i < MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE; i++) {
            peripheralCccd = peripheralCccds[i];

            if (peripheralCccd != NULL) {
                if ((peripheralCccd->ServiceUUID == TransferHeader->ServiceUuid) && (peripheralCccd->CharacteristicUUID == TransferHeader->CharacteristicUuid)) {
                    break;
                }
            }
        }

        // If not found
        if (i == MAXIMUM_CONCURRENT_PERIPHERAL_CCCD_SIZE) {
            G_SYS_DBG_LOG_V ("WARNING: CCCD not found\r\n");
            goto fail;
        }

        // If check CCCD fail
        if ((peripheralCccd->CCCD & 0x01) == 0) {
            G_SYS_DBG_LOG_V ("WARNING: CCCD check fail\r\n");
            goto fail;
        }
    }

    DevJobMsg = dxMemAlloc ("Ble Manager Job Msg Buff", 1, sizeof (BleDeviceJobQueue_t));
    DevJobMsg->JobType = DEV_MANAGER_JOB_CONCURRENT_PERIPHERAL_DATA_OUT;
    DevJobMsg->JobTimeOutTime = TimeOutReport; //NOTE: Job time out time will be calculated only when Job is about to start. Otherwise, job will always timeout if there are many job in queue
    DevJobMsg->JobSourceOfOrigin = 0;
    DevJobMsg->JobIdentificationNumber = TaskIdentificationNumber;
    DevJobMsg->JobDisconnectMaxRetry = 0;

    MsgHeader.Preamble = DATA_TRANSFER_PREAMBLE;
    MsgHeader.MessageType = MsgType;
    MsgHeader.Reserved = 0;
    MsgHeader.PayloadLength = length;

    memcpy (DevJobMsg->JobData, &MsgHeader, sizeof (ConcurrentDataTransferHeader_t));

    if (Payload) {
        memcpy (DevJobMsg->JobData + sizeof (ConcurrentDataTransferHeader_t), Payload, length);
    }

    if (BleDeviceJobQueue) {

        if (dxQueueIsFull (BleDeviceJobQueue) == dxTRUE) {
            G_SYS_DBG_LOG_V ("Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__);
            dxMemFree (DevJobMsg);
            result = DEV_MANAGER_TOO_MANY_TASK_IN_QUEUE;
        } else {
            if (dxQueueSend (BleDeviceJobQueue, &DevJobMsg, 1000) != DXOS_SUCCESS) {
                G_SYS_DBG_LOG_V ("Error pushing message to queue (Could be due to timeout) \r\n");
                dxMemFree (DevJobMsg);
                result = DEV_MANAGER_RTOS_ERROR;
            }
        }
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralSetSecurityKeyTask_Asynch (
    uint8_t* key,
    uint16_t keyLength
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;
    BleConcurrentPeripheralSecurityKey_t bleSecurityKey;

    if (keyLength != 16) {
        result = DEV_MANAGER_INVALID_PARAMETER;
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Wrong value of keyLength=%d\r\n", keyLength);
        goto fail;
    }

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = DEV_CENTRAL_CTRL_SUCCESS;

    memcpy (bleSecurityKey.securityKey, key, 16);
    devResult = BLEDevCentralCtrl_SetConcurrentPeripheralSecurityKey (
        &bleSecurityKey
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Fail to call BLEDevCentralCtrl_SetConcurrentPeripheralSecurityKey (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralSetAdvertiseDataTask_Asynch (
    BLE_ADVDATA_NAME_TYPE_t nameType,
    uint8_t                 nameLength,
    char*                   name,
    uint8_t                 flag,
    uint8_t                 uuidParam,
    uint8_t*                uuids,
    uint16_t                interval,
    uint8_t                 manufLength,
    uint8_t*                manuf
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;
    BleConcurrentPeripheralAdvertiseData_t bleAdvertiseData;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = DEV_CENTRAL_CTRL_SUCCESS;

    bleAdvertiseData.nameType       = nameType;
    bleAdvertiseData.nameLength     = nameLength;
    bleAdvertiseData.name           = name;
    bleAdvertiseData.flag           = flag;
    bleAdvertiseData.uuidParam      = uuidParam;
    bleAdvertiseData.uuids          = uuids;
    bleAdvertiseData.interval       = interval;
    bleAdvertiseData.manufLength    = manufLength;
    bleAdvertiseData.manuf          = manuf;
    devResult = BLEDevCentralCtrl_SetConcurrentPeripheralAdvertiseData (
        &bleAdvertiseData
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_SetConcurrentPeripheralAdvertiseData (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralSetScanResponseTask_Asynch (
    BLE_ADVDATA_NAME_TYPE_t nameType,
    uint8_t                 nameLength,
    char*                   name,
    uint8_t                 uuidParam,
    uint8_t*                uuids,
    uint8_t                 manufLength,
    uint8_t*                manuf
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;
    BleConcurrentPeripheralScanResponse_t bleScanResponse;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = DEV_CENTRAL_CTRL_SUCCESS;

    bleScanResponse.nameType       = nameType;
    bleScanResponse.nameLength     = nameLength;
    bleScanResponse.name           = name;
    bleScanResponse.uuidParam      = uuidParam;
    bleScanResponse.uuids          = uuids;
    bleScanResponse.manufLength    = manufLength;
    bleScanResponse.manuf          = manuf;
    devResult = BLEDevCentralCtrl_SetConcurrentPeripheralScanResponse (
        &bleScanResponse
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_SetConcurrentPeripheralScanResponse (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralAddServiceCharacteristicTask_Asynch (
    BleServiceCharacteristic_t* serviceCharacteristic
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = BLEDevCentralCtrl_AddConcurrentPeripheralServiceCharacteristic (
        serviceCharacteristic
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_AddConcurrentPeripheralServiceCharacteristic (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralEnableSystemUtilsTask_Asynch (
    uint16_t* systemUtilsMask
) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = BLEDevCentralCtrl_EnableConcurrentPeripheralSystemUtils (
        systemUtilsMask
    );
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_EnableConcurrentPeripheralSystemUtils (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralStartTask_Asynch (void) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = BLEDevCentralCtrl_StartConcurrentPeripheral ();
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_StartConcurrentPeripheral (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralStopTask_Asynch (void) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = BLEDevCentralCtrl_StopConcurrentPeripheral ();
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_StopConcurrentPeripheral (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerConcurrentPeripheralDisconnectTask_Asynch (void) {
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;
    const BleCentralConcurrentPeripheralState_t* bleCcpState = BLEDevCentralCtrl_GetLastKnownConcurrentPeripheralState ();
    BLEDeviceCtrl_result_t devResult;

    if (bleCcpState->ConcurrentPeripheralState == BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT) {
        result = DEV_MANAGER_NOT_AVAILABLE;
        G_SYS_DBG_LOG_V ("Wrong concurrent peripheral state=%d\r\n", bleCcpState->ConcurrentPeripheralState);
        goto fail;
    }

    devResult = BLEDevCentralCtrl_DisconnectConcurrentPeripheral ();
    if (devResult != DEV_CENTRAL_CTRL_SUCCESS) {
        G_SYS_DBG_LOG_V1 ("Fail to call BLEDevCentralCtrl_DisconnectConcurrentPeripheral (): devResult=%d\r\n", devResult);
    }

fail :

    return result;
}


extern BLEManager_result_t BleManagerStartReportingUnPairPeripheral()
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BLEDeviceKeeper_result_t KeeperResult = BLEDevKeeper_StartReportingUnPairDevices(&BleDeviceUnPairDeviceReportQueue);
    if (KeeperResult == DEV_KEEPER_INVALID_PARAM)
        result = DEV_MANAGER_INVALID_PARAMETER;
    IntWhiteList_ClearAll();
    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BleManagerStartReportingUnPairPeripheral()!! \r\n");

    if (BleRepeater_IsActive () && BleRepeater_StartOpenPairingScanning () != DEV_CENTRAL_CTRL_SUCCESS) {
        result = DEV_MANAGER_DEVICE_PERIPHERALROLE_NOT_CONNECTED;
    }

    return result;
}

extern int16_t BleManagerParseDeviceIndexReportEventObj(void* ReportedEventArgObj)
{
    int16_t DeviceIndex = -1;


    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if ((EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED) ||
        (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED))
    {
        if (EventReportObj->pData)
        {
            DeviceIndex = *((int8_t*)EventReportObj->pData);
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: Parse DevIndex Failed with unknown ReportEvent\r\n" );
    }


    return DeviceIndex;
}


extern BleBridgeCommMsg_t* BleManagerParseUnknownlReportEventObj(void* ReportedEventArgObj)
{
    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;
    BleBridgeCommMsg_t* UnknownEvent = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_UNKNOWN)
    {
        if (EventReportObj->pData)
        {
            UnknownEvent = (BleBridgeCommMsg_t*)EventReportObj->pData;
        }
    }

    return UnknownEvent;
}


extern DeviceInfoReport_t* BleManagerParseUnPairPeripheralReportEventObj(void* ReportedEventArgObj)
{
    static DeviceInfoReport_t DeviceReportInfo;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    memset( &DeviceReportInfo, 0, sizeof(DeviceInfoReport_t) );

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_UNPAIR_DEVICE_REPORT)
    {

        DeviceReportInfo.DevHandlerID = DEV_KEEPER_NOT_FOUND;
        DeviceAddress_t DevParseAddress = BLEDevParser_GetAddress(EventReportObj->pData);

        memcpy(DeviceReportInfo.DevAddress.Addr, DevParseAddress.pAddr, (DevParseAddress.AddrLen > B_ADDR_LEN_8) ? B_ADDR_LEN_8:DevParseAddress.AddrLen );

        DeviceManufaturerData_t DevManufacturerData = BLEDevParser_GetManufacturerData(EventReportObj->pData);

        DeviceReportInfo.rssi = DevManufacturerData.rssi;

        if (DevManufacturerData.ManufacturerDataLen)
        {

            //We do not provide security key for parsing Unpair peripheral because its either
            //1) Not yet in our keeper's list so we don't have the key
            //2) User reset the device and therefore the content is most likely not encrypted...
            BLEDevParser_GetDkAdvData(  &(DeviceReportInfo.DkAdvertisementData),
                                        &DevManufacturerData,
                                        NULL,
                                        0);
        }

        return &DeviceReportInfo;

    }
    else
    {
        return NULL;
    }


}

extern DeviceInfoReport_t* BleManagerParseKeeperPeripheralReportEventObj(void* ReportedEventArgObj)
{
    DeviceInfoReport_t* pDeviceReportInfo = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if ((EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT) ||
            (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT) )
    {
        pDeviceReportInfo = (DeviceInfoReport_t*)EventReportObj->pData;
    }

    return pDeviceReportInfo;
}

extern DeviceKeeperListReport_t* BleManagerParseDeviceKeeperListReportEventObj(void* ReportedEventArgObj)
{
    DeviceKeeperListReport_t* pDeviceDeviceKeeperList = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST)
    {
        pDeviceDeviceKeeperList = (DeviceKeeperListReport_t*)EventReportObj->pData;
    }

    return pDeviceDeviceKeeperList;
}

extern DeviceInfoReport_t* BleManagerParseUnpairedPeripheralReportEventObj(void* ReportedEventArgObj)
{
    DeviceInfoReport_t* pDeviceReportInfo = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_UNPAIRED)
    {
        pDeviceReportInfo = (DeviceInfoReport_t*)EventReportObj->pData;
    }

    return pDeviceReportInfo;

}

extern DeviceInfoReport_t* BleManagerParseNewPeripheralAddedReportEventObj(void* ReportedEventArgObj)
{
    DeviceInfoReport_t* pDeviceInfo = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_NEW_DEVICE_ADDED)
    {
        pDeviceInfo = (DeviceInfoReport_t*)EventReportObj->pData;
    }

    return pDeviceInfo;

}

extern uint8_t* BleManagerParsePeripheralRoleDataInPayloadReportEventObj(void* ReportedEventArgObj)
{
    uint8_t* Payload = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD)
    {
        BlePeripheralDataTransfer_t* PeripheralRoleDataIn = (BlePeripheralDataTransfer_t*)EventReportObj->pData;

        Payload = PeripheralRoleDataIn->Payload;
    }


    return Payload;
}


extern uint8_t* BleManagerParseConcurrentPeripheralDataInPayloadReportEventObj (
    void* ReportedEventArgObj,
    uint16_t* payloadLen
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    uint8_t* payload = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD) {
        BleConcurrentPeripheralDataTransfer_t* bleCcpDataIn = (BleConcurrentPeripheralDataTransfer_t*) EventReportObj->pData;
        *payloadLen = bleCcpDataIn->payloadLen;
        payload = (uint8_t*) bleCcpDataIn->payload;
    }

    return payload;
}


extern BleConcurrentPeripheralGetStatusResponse_t* BleManagerParseConcurrentPeripheralStatusPayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralGetStatusResponse_t* statusResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_STATUS_REPORT) {
        statusResponse = (BleConcurrentPeripheralGetStatusResponse_t*) EventReportObj->pData;
    }

    return statusResponse;
}


extern BleConcurrentPeripheralSecurityKeyResponse_t* BleManagerParseConcurrentPeripheralSecurityKeyPayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SECURITY_KEY_REPORT) {
        securityKeyResponse = (BleConcurrentPeripheralSecurityKeyResponse_t*) EventReportObj->pData;
    }

    return securityKeyResponse;
}


extern BleConcurrentPeripheralAdvertiseDataResponse_t* BleManagerParseConcurrentPeripheralAdvertiseDataPayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_ADVERTISE_DATA_REPORT) {
        advertiseDataResponse = (BleConcurrentPeripheralAdvertiseDataResponse_t*) EventReportObj->pData;
    }

    return advertiseDataResponse;
}


extern BleConcurrentPeripheralScanResponseResponse_t* BleManagerParseConcurrentPeripheralScanResponsePayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SCAN_RESPONSE_REPORT) {
        scanResponseResponse = (BleConcurrentPeripheralScanResponseResponse_t*) EventReportObj->pData;
    }

    return scanResponseResponse;
}


extern BleConcurrentPeripheralServiceCharacteristicResponse_t* BleManagerParseConcurrentPeripheralServiceCharacteristicPayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SERVICE_CHARACTERISTIC_REPORT) {
        serviceCharacteristicResponse = (BleConcurrentPeripheralServiceCharacteristicResponse_t*) EventReportObj->pData;
    }

    return serviceCharacteristicResponse;
}


extern BleConcurrentPeripheralSystemUtilsResponse_t* BleManagerParseConcurrentPeripheralSystemUtilsPayloadReportEventObj (
    void* ReportedEventArgObj
) {
    EventReportObj_t* EventReportObj = (EventReportObj_t*) ReportedEventArgObj;
    BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse = NULL;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SYSTEM_UTILS_REPORT) {
        systemUtilsResponse = (BleConcurrentPeripheralSystemUtilsResponse_t*) EventReportObj->pData;
    }

    return systemUtilsResponse;
}


extern AggregatedReportObj_t BleManagerParseAggregatedDataReportEventObj(void* ReportedEventArgObj)
{
    AggregatedReportObj_t AgReportData;

    memset(&AgReportData, 0x00, sizeof(AggregatedReportObj_t));

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT)
    {
        memcpy(&AgReportData.Header, EventReportObj->pData, sizeof(AggregatedReportHeader_t));
        AgReportData.pAggregatedItem = EventReportObj->pData + sizeof(AggregatedReportHeader_t);

    }

    return AgReportData;

}

extern DataReadReportObj_t* BleManagerParseDataReadReportEventObj(void* ReportedEventArgObj)
{
    DataReadReportObj_t* pReadData = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT)
    {
        pReadData = (DataReadReportObj_t*)EventReportObj->pData;
    }

    return pReadData;

}


extern SystemInfoReportObj_t* BleManagerParsePeripheralSystemInfoReportEventObj(void* ReportedEventArgObj)
{
    SystemInfoReportObj_t* pSystemInfo = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO)
    {
        pSystemInfo = (SystemInfoReportObj_t*)EventReportObj->pData;
    }

    return pSystemInfo;
}


extern SystemBdAddressResponse_t* BleManagerParsePeripheralSystemBdAddressReportEventObj(void* ReportedEventArgObj) {
    SystemBdAddressResponse_t* bdAddressResponse = NULL;

    EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

    if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_DEVICE_SYSTEM_BD_ADDRESS) {
        bdAddressResponse = (SystemBdAddressResponse_t*)EventReportObj->pData;
    }

    return bdAddressResponse;
}


extern uint8_t* BleManagerParseFwBlockWriteVerifyReportEventObj(void* ReportedEventArgObj, uint8_t* DataLen)
{
    uint8_t* pData = NULL;

    if (DataLen)
    {
        EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->Header.ReportEvent == DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY)
        {
            *DataLen = EventReportObj->pData[0];
            pData = &EventReportObj->pData[1];
        }
    }

    return pData;
}

extern BLEManager_result_t BleManagerStopReportingUnPairPeripheral()
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BLEDevKeeper_StopReportingUnPairDevices();
    BLEManager_WhiteList_AddFromKeeperList();
    BleRepeater_StopOpenPairingScanning ();

    return result;

}

void BleManagerForceUpdateAdvRxTimeInKeeperList(void)
{
    uint16_t DevIndex = 0;

    for (DevIndex = 0; DevIndex < MAXIMUM_DEVICE_KEEPER_ALLOWED; DevIndex++)
    {
        DeviceInfoKp_t* deviceKeeperList = BLEDevKeeper_GetDeviceInfoFromIndex (DevIndex);

        if (deviceKeeperList && deviceKeeperList->DevAddress.AddrLen)
        {
            deviceKeeperList->ManufacturerAdvDataLastUpdatedSystemTime += 1*SECONDS;
        }
    }
}


extern BLEManager_result_t BleManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    if (ReportedEventArgObj)
    {
        EventReportObj_t* EventReportObj = (EventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->pData)
            dxMemFree(EventReportObj->pData);

        dxMemFree(ReportedEventArgObj);
    }
    else
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
    }

    return result;
}



extern BLEManager_result_t BleManagerDeviceUtils_GetDataTypeValue(DeviceInfoReport_t* DeviceInfoData, uint8_t DataType, uint8_t* DataLenOut, uint8_t* DataOut )
{
    BLEManager_result_t result = DEV_MANAGER_DEVICE_UNABLE_TO_FIND_DATA_TYPE;

    if ((DataLenOut == NULL) || (DataOut == NULL) || (DeviceInfoData == NULL))
    {
        result = DEV_MANAGER_INVALID_PARAMETER;
        return result;
    }

    uint8_t DataTypeLen =  DKPeripheral_GetDataTypeLenForDataTypeID(DataType);

    if (DataTypeLen)
    {
        uint8_t*    Data = BLEDevParser_GetDataWithDataTypeID(DataType, &DeviceInfoData->DkAdvertisementData);

        if (Data)
        {
            *DataLenOut = DataTypeLen;
            memcpy(DataOut, Data, DataTypeLen );

            result = DEV_MANAGER_SUCCESS;
        }
    }

    return result;
}


extern BLEManager_result_t BleManager_FreeFormMesageSend(uint8_t MessageType, uint8_t CommandAux, uint8_t PayloadID, uint8_t PayloadLen, uint8_t* Payload)
{
    BLEManager_result_t result = DEV_MANAGER_SUCCESS;

    BLEDevCentralCtrl_FreeFormMesageSend(MessageType, CommandAux, PayloadID, PayloadLen, Payload);

    return result;
}



//////////////////////////////////////////////////
// Solution Identify
//////////////////////////////////////////////////
uint8_t m_solution_id = 0;
uint8_t m_solution_in_sbl = 0;

void BleManager_SolutionID_Update(eBleSystemInfoImageType image_type)
{
    uint8_t solution_id = SOLUTION_ID_TI_2541;
    uint8_t sbl_enable = false;

    switch(image_type)
    {
    case ACTIVE_IMAGE_TYPE_SBL_TI:
        sbl_enable = true;
        //no break
    case ACTIVE_IMAGE_TYPE_MAIN_APPLICATION_TI:
        solution_id = SOLUTION_ID_TI_2541;
        break;

    case ACTIVE_IMAGE_TYPE_SBL_NORDIC_NRF52832:
        sbl_enable = true;
        //no break
    case ACTIVE_IMAGE_TYPE_MAIN_APPLICATION_NORDIC_NRF52832:
        solution_id = SOLUTION_ID_NORDIC_52832;
        break;
    }
    m_solution_id = solution_id;
    m_solution_in_sbl = sbl_enable;
    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Solution ID:0x%x sbl: 0x%x\r\n", solution_id, sbl_enable);
}

uint8_t BleManager_SolutionID_Get(void)
{
    return m_solution_id;
}

uint8_t BleManager_SBL_Enable(void)
{
    return m_solution_in_sbl;
}


//////////////////////////////////////////////////
// White list
//////////////////////////////////////////////////

bool m_white_list_enable;
uint16_t m_expected_white_list_num = 0;
uint16_t m_response_white_list_num = 0;

static void IntWhiteList_ClearAll(void)
{
    if (BleManager_SolutionID_Get() != SOLUTION_ID_NORDIC_52832)
    {
        return;
    }

    BLEDevCentralCtrl_SendClearWhiteList();
    m_expected_white_list_num = 0;
}

void BLEManager_WhiteList_AddFromKeeperList(void)
{
    int16_t i, j;
    white_list_register_t reg;

    if (BleManager_SolutionID_Get() != SOLUTION_ID_NORDIC_52832)
    {
        return;
    }

    if (BleManager_SBL_Enable())
    {
        return;
    }

label_white_list_begin :

    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEManager_WhiteList_AddFromKeeperList() \r\n");
    IntWhiteList_ClearAll();
    dxTimeDelayMS(10);

    //set indivisual watch list
    for (i = 0; i < MAXIMUM_DEVICE_KEEPER_ALLOWED; i++)
    {
        DeviceInfoKp_t* deviceKeeperList = BLEDevKeeper_GetDeviceInfoFromIndex (i);

        if (deviceKeeperList && deviceKeeperList->DevAddress.AddrLen)
        {
            for (j = 0; j < B_ADDR_LEN_8; j++)
            {
                reg.bd_addr[j] = deviceKeeperList->DevAddress.pAddr[B_ADDR_LEN_8-1-j];
            }
            // TODO: Becasue server unable to provide the correct AddressType right now,
            // TODO: so we use ADDRTYPE_DONTCARE instead.
            reg.addr_type = (uint8_t) ADDRTYPE_DONTCARE;
            m_expected_white_list_num++;
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_BLEMANAGER,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "register white list addr=%02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
                deviceKeeperList->DevAddress.pAddr[7],
                deviceKeeperList->DevAddress.pAddr[6],
                deviceKeeperList->DevAddress.pAddr[5],
                deviceKeeperList->DevAddress.pAddr[4],
                deviceKeeperList->DevAddress.pAddr[3],
                deviceKeeperList->DevAddress.pAddr[2],
                deviceKeeperList->DevAddress.pAddr[1],
                deviceKeeperList->DevAddress.pAddr[0]
            );
            BLEDevCentralCtrl_SendRegisterWhiteList(&reg);
            dxTimeDelayMS(10);
        }
    }

    if (m_expected_white_list_num)
    {
        dxTimeDelayMS(100);
        if (m_expected_white_list_num != m_response_white_list_num)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "retry adding white list \r\n");
            goto label_white_list_begin;
        }
    }
}

static void IntWhiteList_CheckNum(uint16_t num)
{
    if (BleManager_SolutionID_Get() != SOLUTION_ID_NORDIC_52832)
    {
        return;
    }

    m_response_white_list_num = num;
}

////////////////////////////////////////////////////////////////////////////
// firmware update for Nordic NRF52
////////////////////////////////////////////////////////////////////////////

#define NRF_LOG_INFO(A)
#define JSON_BUFFER_SIZE    1024

extern dxUART_t *pdxUartObj;

//SLIP

#define SLIP_BYTE_END               0xC0
#define SLIP_BYTE_ESC               0xDB
#define SLIP_BYTE_ESC_END           0xDC
#define SLIP_BYTE_ESC_ESC           0xDD

int IntProgramFirmware_NRF52_EncodeSLIPAndSendBlocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len)
{
    int count = 0;

    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    if (ptxbuf == NULL ||
        len == 0) {
        return DXUART_ERROR;
    }

    while (count < len) {
        uint8_t ch = ptxbuf[count];
        if (ch == SLIP_BYTE_END)
        {
            if (dxUART_PutCh(pdxUart, SLIP_BYTE_ESC) == DXUART_ERROR) {
                break;
            }
            if (dxUART_PutCh(pdxUart, SLIP_BYTE_ESC_END) == DXUART_ERROR) {
                break;
            }
        }
        else if (ch == SLIP_BYTE_ESC)
        {
            if (dxUART_PutCh(pdxUart, SLIP_BYTE_ESC) == DXUART_ERROR) {
                break;
            }
            if (dxUART_PutCh(pdxUart, SLIP_BYTE_ESC_ESC) == DXUART_ERROR) {
                break;
            }
        }
        else
        {
            if (dxUART_PutCh(pdxUart, ch) == DXUART_ERROR) {
                break;
            }
        }
        count++;
    }

    dxUART_PutCh(pdxUart, SLIP_BYTE_END);

    //PRINT_BIN(ptxbuf, count, " ");

    if (count < 0) {
        return DXUART_ERROR;
    }
    else if (count == 0) {
        return DXUART_TIMEOUT;
    }

    return count;
}

int IntJson_GetObjectPosition(char *json_string, uint32_t *start_position, uint32_t *end_position)
{
    char *char_ptr;
    uint32_t offset = *start_position;

    if ((char_ptr = strchr(&json_string[offset], '{')) == NULL)
    {
        return -1;
    }
    *start_position = (uint32_t)(char_ptr - json_string);

    if ((char_ptr = strchr(char_ptr, '}')) == NULL)
    {
        return -1;
    }
    *end_position = (uint32_t)(char_ptr - json_string);
    return 0;
}

int IntJson_GetUint8Array(char *json_string, char *keyword, uint8_t *array_ptr, uint32_t *array_length_ptr)
{
    char *char_ptr, *char_end_ptr, *char_final_ptr;

    if ((char_ptr = strchr(json_string, '\"')) == NULL)
    {
        return -1;
    }
    NRF_LOG_INFO("find str \r\n");
    char_ptr++;//locate to keyword

    if ((char_end_ptr = strchr(char_ptr, '\"')) == NULL)
    {
        return -1;
    }
    *char_end_ptr = 0;

    if (strcmp(char_ptr, keyword))
    {
        *char_end_ptr = '\"';
        return -1;
    }
    *char_end_ptr++ = '\"';

    NRF_LOG_INFO("keyword match \r\n");

    if ((char_ptr = strchr(char_end_ptr, ':')) == NULL)
    {
        return -1;
    }

    if ((char_ptr = strchr(char_ptr, '[')) == NULL)
    {
        return -1;
    }
    char_ptr++;

    if ((char_final_ptr = strchr(char_ptr, ']')) == NULL)
    {
        return -1;
    }

    bool last_parse = false;
    char_end_ptr = strchr(char_ptr, ',');

    if (char_end_ptr == NULL || char_end_ptr > char_final_ptr)
    {
        last_parse = true;
        char_end_ptr = char_final_ptr;
    }

    NRF_LOG_INFO("[, match \r\n");

    *array_length_ptr = 0;
    while (true)
    {
        uint16_t value = 0;
        while (char_ptr < char_end_ptr)
        {
            if (*char_ptr >= '0' && *char_ptr <= '9')
            {
                value = value * 10 + (*char_ptr - '0');
            }
            char_ptr++;
        }
        *array_ptr++ = (uint8_t)value;
        (*array_length_ptr)++;

        if (last_parse)
        {
            break;
        }

        char_ptr = char_end_ptr + 1;
        char_end_ptr = strchr(char_ptr, ',');

        if (char_end_ptr == NULL || char_end_ptr > char_final_ptr)
        {
            last_parse = true;
            char_end_ptr = char_final_ptr;
        }
    }
    return 0;
}

typedef struct
{
    dxSubImgInfoHandle_t handle;
    uint32_t image_size;
    uint32_t image_curr_pos;
    uint32_t begin_pos;
    uint32_t end_pos;
    uint32_t pre_end_pos;
    bool image_loaded_and_valid;
    char * buffer_for_json;
    uint8_t * buffer_for_binary;
    uint8_t program_state;
    uint8_t delay_count;
} program_firmware_function_variable_t;

typedef enum
{
    PROGRAM_STATE_READ_WRITE,
    PROGRAM_STATE_DELAY
} program_state_t;

static program_firmware_function_variable_t *p_fw_update;

ProgramFlashNRF52_result_t BleManager_ProgramFirmware_NRF52_Init(dxSubImgInfoHandle_t handle)
{
    if (!p_fw_update)
    {
        p_fw_update = (program_firmware_function_variable_t *)dxMemAlloc( "DFU Fun Var", 1, sizeof(program_firmware_function_variable_t));
        if (!p_fw_update)
        {
            return PROGRAM_FLASH_NRF52_CANNOT_GET_MEMORY;
        }
    }
    memset(p_fw_update, 0, sizeof(program_firmware_function_variable_t));

    if ((p_fw_update->image_size = dxSubImage_ReadImageSize(handle)) == 0)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxSubImage_Read() FwSize is 0!\r\n");
        return PROGRAM_FLASH_NRF52_ERROR;
    }
    p_fw_update->handle = handle;
    p_fw_update->image_loaded_and_valid = true;

    p_fw_update->buffer_for_json = dxMemAlloc( "DFU JSON Buff", 1, JSON_BUFFER_SIZE + 1);

    if (!p_fw_update->buffer_for_json)
    {
        dxMemFree(p_fw_update);
        p_fw_update = NULL;
        return PROGRAM_FLASH_NRF52_CANNOT_GET_MEMORY;
    }
    memset(p_fw_update->buffer_for_json, 0, JSON_BUFFER_SIZE + 1);

    p_fw_update->buffer_for_binary = dxMemAlloc( "DFU BIN Buff", 1, 256);
    if (!p_fw_update->buffer_for_binary)
    {
        dxMemFree(p_fw_update->buffer_for_json);
        dxMemFree(p_fw_update);
        p_fw_update = NULL;
        return PROGRAM_FLASH_NRF52_CANNOT_GET_MEMORY;
    }
    p_fw_update->program_state = PROGRAM_STATE_READ_WRITE;
    dxTimeDelayMS(1000);//delay for SBL mode entering
    return PROGRAM_FLASH_NRF52_SUCESS;
}

static void IntProgramFirmware_NRF52_FreeAllMemory(void)
{
    dxMemFree(p_fw_update->buffer_for_json);
    dxMemFree(p_fw_update->buffer_for_binary);
    dxMemFree(p_fw_update);
    p_fw_update = NULL;
}

ProgramFlashNRF52_result_t BleManager_ProgramFirmware_NRF52_Execution(uint8_t *p_progress)
{
    uint32_t buff_size;

    if (!p_fw_update)
    {
        return PROGRAM_FLASH_NRF52_ERROR;
    }

    *p_progress = 0;

    switch(p_fw_update->program_state)
    {
    case PROGRAM_STATE_READ_WRITE:
    label_beginning:
        if (p_fw_update->image_curr_pos < p_fw_update->image_size)
        {
            p_fw_update->pre_end_pos = p_fw_update->end_pos;
            if (IntJson_GetObjectPosition(p_fw_update->buffer_for_json, &p_fw_update->begin_pos, &p_fw_update->end_pos))
            {
                if (p_fw_update->image_loaded_and_valid)
                {
                    p_fw_update->image_loaded_and_valid = false;
                    p_fw_update->image_curr_pos += p_fw_update->pre_end_pos;

                    dxSUBIMAGE_RET_CODE ret;
                    uint32_t target_len = p_fw_update->image_size - p_fw_update->image_curr_pos;
                    if (target_len < 10)
                    {
                        *p_progress = 50;
                        p_fw_update->program_state = PROGRAM_STATE_DELAY;
                        break;
                    }

                    if (target_len > JSON_BUFFER_SIZE)
                    {
                        target_len = JSON_BUFFER_SIZE;
                    }
                    uint32_t read_size;
                    if (DXSUBIMAGE_SUCCESS != (ret = dxSubImage_Read(p_fw_update->handle,
                                                              p_fw_update->image_curr_pos,
                                                              target_len,
                                                              p_fw_update->buffer_for_json,
                                                              &read_size)))
                    {
                        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING: dxSubImage_Read() RetCode = %d\r\n", ret);

                        IntProgramFirmware_NRF52_FreeAllMemory();
                        return PROGRAM_FLASH_NRF52_ERROR;
                    }
                    p_fw_update->begin_pos = p_fw_update->end_pos = 0;
                    p_fw_update->buffer_for_json[read_size] = NULL;
                    goto label_beginning;
                }
                else
                {
                    IntProgramFirmware_NRF52_FreeAllMemory();
                    return PROGRAM_FLASH_NRF52_ERROR;
                }
            }
            p_fw_update->image_loaded_and_valid = true;

            if (!IntJson_GetUint8Array(&p_fw_update->buffer_for_json[p_fw_update->begin_pos], "uart_tx_data",
                                   p_fw_update->buffer_for_binary, &buff_size))
            {
                IntProgramFirmware_NRF52_EncodeSLIPAndSendBlocked(pdxUartObj, p_fw_update->buffer_for_binary, buff_size);
                dxTimeDelayMS(10);
            }
            else if (!IntJson_GetUint8Array(&p_fw_update->buffer_for_json[p_fw_update->begin_pos], "uart_rx_data",
                                   p_fw_update->buffer_for_binary, &buff_size))
            {
    //            uin8_t *ptr1 = dxUART_ReceiveSLIP();
                if (p_fw_update->buffer_for_binary[0] == 0x60 && p_fw_update->buffer_for_binary[1] == 0x04)
                {
                    dxTimeDelayMS(300);
                }
            }
            p_fw_update->begin_pos = p_fw_update->end_pos + 1;
            *p_progress = (uint8_t)(50 * p_fw_update->image_curr_pos / p_fw_update->image_size);
        }
        else
        {
            *p_progress = 50;
            p_fw_update->program_state = PROGRAM_STATE_DELAY;
            break;
        }
        break;

    case PROGRAM_STATE_DELAY:
        if (p_fw_update->delay_count < 20)
        {
            dxTimeDelayMS(1000);
            *p_progress = 50 + 50 * p_fw_update->delay_count/20;
            p_fw_update->delay_count++;
        }
        else
        {
            *p_progress = 100;
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "NRF52 FWUpdate done. \r\n");
            IntProgramFirmware_NRF52_FreeAllMemory();
        }
        break;

    default:
        break;
    }
    return PROGRAM_FLASH_NRF52_SUCESS;
}


void BleManager_TakeBLEResetMutex(void)
{
    dxMutexTake(m_ble_reset_mutex, DXOS_WAIT_FOREVER);
}


void BleManager_GiveBLEResetMutex(void)
{
    dxMutexGive(m_ble_reset_mutex);
}



//////////////////////////////////////////////////////
// for repeater by John
//////////////////////////////////////////////////////

void BLEManager_HandleAdvertiseInfo (uint8_t *adv_info, uint16_t adv_info_length)
{
    int16_t DevIndex = DEV_KEEPER_NOT_FOUND;
    BLEDeviceKeeper_result_t Result = DEV_KEEPER_SUCCESS;

    //First we will need to reverse the address so that MSB is at first byte
    if (BLEDevParser_ReverseAddress(adv_info) == dxTRUE)
    {
        DeviceInfo_t* pDeviceInfo = (DeviceInfo_t*)adv_info;
        if (!BleRepeater_IsActive ()) {
            AutoRescanProcessOnReachingIncomingMaxDevice(pDeviceInfo->addr); //of no use in repeater
        }
        Result = BLEDevKeeper_DeviceInfoFeed(adv_info, adv_info_length, &DevIndex);
    }
    else
    {
        //Failed to resolve the address - can be due to its a private non-resolvable address which is usually
        //from BT cell phone or other which we do not really care about.
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,  "WARNING: Failed to BLEDevParser_ReverseAddress\r\n");
    }

    //G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "BLEManager_HandleAdvertiseInfo Result: %d, DeviceIndex: %d \r\n", Result, DevIndex);

    if (Result == DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED && DevIndex != DEV_KEEPER_NOT_FOUND && !BleRepeater_IsActive ()) {
        BleRepeater_TryActivate (adv_info, adv_info_length, DevIndex);
    }

    if (DevIndex != DEV_KEEPER_NOT_FOUND)
    {
        EventReportObj_t* EventReportObj = NULL;

        if ((Result == DEV_KEEPER_DEVICE_INFO_IS_COMPLETE) || (Result == DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED))
        {
            DeviceInfo_t* pDeviceInfo = (DeviceInfo_t*)adv_info;
            DeviceInfoReport_t* DeviceReportInfo = AllocAndParseDeviceInfoReportFromIndex(DevIndex);

            if (DeviceReportInfo)
            {
                EventReportObj = dxMemAlloc( "Event Report Msg Buff", 1, sizeof(EventReportObj_t));
                EventReportObj->Header.IdentificationNumber = 0;
                EventReportObj->Header.ReportEvent = DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT;

                if (Result == DEV_KEEPER_DEVICE_INLIST_BUT_NOT_PAIRED)
                {
                    EventReportObj->Header.ResultState = DEV_MANAGER_TASK_DEVICE_IN_KEEPER_LIST_BUT_FOUND_UNPAIRED;
                }
                else
                {
                    EventReportObj->Header.ResultState = DEV_MANAGER_TASK_SUCCESS;
                }

                EventReportObj->pData = (uint8_t*)DeviceReportInfo;

                // NOTE: Porting to dxOS
                //wiced_result_t EventResult = wiced_rtos_send_asynchronous_event( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
                //if (EventResult != WICED_SUCCESS)
                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent( &EventWorkerThread, RegisteredEventFunction, EventReportObj );
                if (EventResult != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "FATAL MSG_TYPE_SCAN Failed to send Event message back to Application\r\n" );
                    dxMemFree(DeviceReportInfo);
                    dxMemFree(EventReportObj);
                }
            }
        }
        else if (Result == DEV_KEEPER_DEVICE_INFO_CHECKSUM_FAILED)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_BLEMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: 'Unnamed' device payload checksum failed! (can happen if device first bootup)\r\n");
        }
    }
}


dxOS_RET_CODE BleManager_SendAsynchWorkerEvent(void* arg)
{
    return dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, arg);
}


dxBOOL BleManager_IsDeviceGoingToMissing (void) {
    return deviceGoingToMissing;
}


dxBOOL BleManager_IsJobQueueEmpty (void) {
    return dxQueueIsEmpty (BleDeviceJobQueue);
}
