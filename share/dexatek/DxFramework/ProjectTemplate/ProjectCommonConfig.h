//============================================================================
// File: ProjectCommonConfig.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _COMMON_CONFIG_H
#define _COMMON_CONFIG_H

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *              Project General Info
 ******************************************************/
#define RTK_WIFI_CURRENT_FW_VERSION             "3.0.0"

/******************************************************
 *              Project Setting Include
 ******************************************************/
#include "DKFrameworkConfig_Template.h"



#ifdef __cplusplus
}
#endif


#endif //_COMMON_CONFIG_H
