//============================================================================
// File: Main_Template.c
//
// Author: Sam Tsai
//
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"

#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "LedManager.h"
#include "dxSysDebugger.h"
#include "dxKeypadManager.h"
#include "CentralStateMachineAutomata.h"

//============================================================================
// Constants
//============================================================================

#define APPLICATION_MAIN_LOOP_WATCHDOG_PERMITTED_DELAY      (8*SECONDS) //RTS390X Max watchDog timeout time is 8 seconds

//============================================================================
// Structure
//============================================================================

//============================================================================
// Enumerations
//============================================================================

//============================================================================
// Global Variable
//============================================================================

static dxTaskHandle_t   Application_Start_Thread;


static dxGPIO_t TemplateGPIOConfig[] =
{
    {
        NULL,
        DK_BLE_CENTRAL_MODE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_BLE_RESET,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_BLUE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_YELLOW,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXPIN_INPUT,
        DXPIN_MODE_UNSUPPORTED
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};



dxKeypad_t TemplateKeypadConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXKEYPAD_TRIGGER_LEVEL_LOW,
        MAIN_BUTTON_REPORT_ID,
        DX_GENERAL_INTERVAL_ONE_SECOND,
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG
    }
};


//============================================================================
// Function Declaration
//============================================================================

//============================================================================
// Hybrid Peripheral Supported Functions
//============================================================================

//============================================================================
// Utils
//============================================================================

//============================================================================
// Main
//============================================================================

int main(void)
{
    dxPLATFORM_Init();

    dxOS_Init();

    GSysDebuggerInit();

    dxMainImage_Init();

    dxGPIO_Init(TemplateGPIOConfig);

    state_machine_main_event_t StMachineMainEventFunc = CentralStateMachine_Init(STATE_IDLE);

    LedManager_result_t LedManResult = LedManagerInit();
    G_SYS_DBG_LOG_V("LedManagerInit RetCode =  %d\n", LedManResult);

    BLEManager_result_t BleManRet = BleManagerInit(StMachineMainEventFunc.EventBLEManager);
    G_SYS_DBG_LOG_V("BLEManagerInit RetCode =  %d\n", BleManRet);

    WifiCommManager_result_t WifiRet = WifiCommManagerInit(StMachineMainEventFunc.EventWifiManager);
    G_SYS_DBG_LOG_V("WifiCommManagerInit RetCode =  %d\n", WifiRet);

    UdpCommManager_result_t UdpManRet = UdpCommManagerInit(StMachineMainEventFunc.EventUdpManager);
    G_SYS_DBG_LOG_V("UdpCommManagerInit RetCode =  %d\n", UdpManRet);

    dxJOB_MANAGER_RET_CODE JobManRet = JobManagerInit(StMachineMainEventFunc.EventJobManager);
    G_SYS_DBG_LOG_V("JobManagerInit RetCode =  %d\n", JobManRet);

    const WifiCommDeviceMac_t* pDeviceMac = WifiCommManagerGetDeviceMac();

    DKServer_StatusCode_t ServerManRet = DKServerManager_Init(StMachineMainEventFunc.EventServerManager,
                    (unsigned char *)pDeviceMac->MacAddr, sizeof(pDeviceMac->MacAddr),
                    (uint8_t *)DK_SERVER_DNS_NAME,
                    (uint8_t *)DK_SERVER_APP_ID,
                    (uint8_t *)DK_SERVER_API_KEY,
                    (uint8_t *)DK_SERVER_API_VERSION);
    G_SYS_DBG_LOG_V("DKServerManager_Init RetCode =  %d\n", ServerManRet);


#if DEVICE_IS_HYBRID
    HPManager_result_t HpManRet = HpManagerInit_Server(StMachineMainEventFunc.EventHpManager);
    G_SYS_DBG_LOG_V("HpManagerInit_Server RetCode =  %d\n", HpManRet);
#endif //#if DEVICE_IS_HYBRID

    //Initialize the keypad, key event will be reported to KeypadEventFunctionCallback
    dxKEYPAD_RET_CODE dxKeyPadRet = dxKeypad_Init( TemplateKeypadConfig, StMachineMainEventFunc.EventKeyPadManager);
    G_SYS_DBG_LOG_V("dxKeypad_Init RetCode =  %d\n", dxKeyPadRet);

    //Add main application to stack profiling
    GSysDebugger_RegisterForThreadDiagnostic(&Application_Start_Thread);

    /* Gateway Mode Operation Management */
    while (1)
    {
        //Process the state machine
        CentralStateMachine_Process();

        //Short sleep.
        dxTimeDelayMS(30); //SUGGEST to KEEP 30ms (due to the need of calling CentralStateMachine_Process)
    }

    dxTaskDelete(NULL);
}
