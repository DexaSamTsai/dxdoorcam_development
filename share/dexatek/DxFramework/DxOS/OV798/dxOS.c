//============================================================================
// File: dxOS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include <limits.h>

#include "dxOS.h"
#include "dxSysDebugger.h"
#include "dxBSP.h"
#include "Utils.h"


//============================================================================
// Defines and Macro
//============================================================================
#define PO10_LIMIT (ULONG_MAX / 10)
#define SDRAM_HEAP_START (uint8_t*)0x30100000       // Caluculate from (0x30200000 - DX_SDRAM_HEAP_SIZE)

# ifndef CLOCK_REALTIME
#	define CLOCK_REALTIME			0
# endif

# ifndef CLOCK_MONOTONIC
#	define CLOCK_MONOTONIC			1
# endif

//============================================================================
// Type Definitions
//============================================================================
# ifndef dbg_error
#	define dbg_error( fmt, ... )	printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# define ERTOS_SUCCESS	1
# define ERTOS_FAIL		0

#define GLOBAL_QUEUE_MAX_LIST       2
#define GLOBAL_QUEUE_NAME_MAX       64

typedef struct {
    char                QueueName[ GLOBAL_QUEUE_NAME_MAX + 4 ];
    dxQueueHandle_t     dxQueue;
} dxGlobalQueue_t;

//============================================================================
// Params
//============================================================================

/*static*/ dxSemaphoreHandle_t  dxSPI_Resource_SemaHandle = NULL;
/*static*/ dxSemaphoreHandle_t  dxRTC_Resource_SemaHandle = NULL;
/*static*/ dxSemaphoreHandle_t  dxGlobalQueueReceiveSemaphore = NULL;
/*static*/ dxSemaphoreHandle_t  dxGlobalQueueSendSemaphore = NULL;

#ifdef DK_MEMORY_DEBUG
#pragma default_variable_attributes = @ ".sdram.text"
static MemoryRecord memoryRecords[DK_MEMORY_DEBUG_RECORD_NUM];
static List_t memoryList;
static uint8_t needMoreRecords = 0;
static char fileName[256] = "";
static char functionName[256] = "";
#pragma default_variable_attributes =
#endif

static dxGlobalQueue_t dxGlobalQueueList[ GLOBAL_QUEUE_MAX_LIST ];

dxMutexHandle_t  dxSem_Resource_MutexHandle = NULL;

//============================================================================
// Time Util
//============================================================================
struct timespec
IntUtilAddOffsetToCurrentTime( uint32_t OffsetMs )
{
	struct timespec	WaitToTime;
	struct timeval	curr_time;

	gettimeofday( &curr_time, NULL );

	uint32_t usec = curr_time.tv_usec + ( OffsetMs * 1000 );

	WaitToTime.tv_sec	= curr_time.tv_sec + ( usec / 1000000 );
	WaitToTime.tv_nsec	= ( usec % 1000000 ) * 1000;

	return WaitToTime;
}

//============================================================================
// Main Initialization
//============================================================================
dxOS_RET_CODE dxOS_Init(void)
{
    dxOS_RET_CODE ret = DXOS_ERROR;
#ifdef DK_MEMORY_DEBUG
    MemoryRecord* record;
    int i;
#endif
    if (dxSPI_Resource_SemaHandle == NULL) {
        // See dxSPIaccess_ResourceTake header description for more detail why is this needed!
        dxSPI_Resource_SemaHandle = dxSemCreate(1,          // uint32_t uxMaxCount
                                                1);         // uint32_t uxInitialCount
    }

    if (dxRTC_Resource_SemaHandle == NULL) {
        // Protection for preventing RTC resource reading and writing at the same time
        dxRTC_Resource_SemaHandle = dxSemCreate(1,          // uint32_t uxMaxCount
                                                1);         // uint32_t uxInitialCount
    }

    if (dxGlobalQueueReceiveSemaphore == NULL) {
        // To make sure only one thread access dxGlobalQueueList in dxQueueReceive()
        dxGlobalQueueReceiveSemaphore = dxSemCreate(1,          // uint32_t uxMaxCount
                                                    1);         // uint32_t uxInitialCount
    }

    if (dxGlobalQueueSendSemaphore == NULL) {
        // To make sure only one thread access dxGlobalQueueList in dxQueueSendWithSize()
        dxGlobalQueueSendSemaphore = dxSemCreate(1,          // uint32_t uxMaxCount
                                                 1);         // uint32_t uxInitialCount
    }

    if ( dxSem_Resource_MutexHandle == NULL ) {
        // This mutex is used to protect semaphores that support maximum count
        // to make sure semaphore gives does not increase the values beyond the given max count
    	dxSem_Resource_MutexHandle = dxMutexCreate();
    }

    if (dxSPI_Resource_SemaHandle && dxRTC_Resource_SemaHandle) {
        ret = DXOS_SUCCESS;
    }

    memset( dxGlobalQueueList, 0, sizeof( dxGlobalQueueList ) );

    return ret;
}



//============================================================================
// Memory Management
//============================================================================

#define DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT        32  //Byte alignment


size_t dxMemGetFreeHeapSize (void)
{
    return xPortGetFreeHeapSize();
}

dxOS_RET_CODE
dxMemFree( void *memoryToFree )
{
    if ( memoryToFree ) {
		free( memoryToFree );
    }

    return DXOS_SUCCESS;
}


void *
dxMemAlloc( char* zone, size_t nelems, size_t size )
{
    uint32_t alignSize = align ( nelems * size, DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT );
    uint8_t *memory = ( uint8_t * )malloc( alignSize );

    if ( memory ) {
        memset( memory, 0, alignSize );
    }

    return ( void * )memory;
}


void *
dxMemReAlloc( char *zone, void *memoryToFree, size_t size )
{
    dxMemFree( memoryToFree );
    return dxMemAlloc( zone, 1, size );
}

//============================================================================
// Timer Management
//============================================================================
# ifndef portTICK_PERIOD_MS
#	define portTICK_PERIOD_MS		10
# endif

dxTimerHandle_t
dxTimerCreate( const char *pcTimerName,
			   const uint32_t dxTimerPeriodInMs,
			   const dxBOOL uxAutoReload,
			   const void *pvTimerID,
			   dxTimerCallbackFunction_t pxCallbackFunction )
{
    return ( dxTimerHandle_t )ertos_timer_create( ( char * )pcTimerName,
												 ( dxTimerPeriodInMs / portTICK_PERIOD_MS ),
												 ( uxAutoReload == dxTRUE )? 1: 0,
												 ( void * )pvTimerID,
												 ( t_ertos_timer_callback )pxCallbackFunction );
}

void *
dxTimerGetTimerID( dxTimerHandle_t dxTimer )
{
    return ertos_timer_get_arg( ( t_ertos_timerhandler )dxTimer );
}

dxOS_RET_CODE
dxTimerStart( dxTimerHandle_t dxTimer, const uint32_t dxMsToWait )
{
//    dbg_error( "dxMsToWait: %u", dxMsToWait );
    return ( ertos_timer_start( ( t_ertos_timerhandler )dxTimer, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxTimerStop( dxTimerHandle_t dxTimer, const uint32_t dxMsToWait )
{
    return ( ertos_timer_stop( ( t_ertos_timerhandler )dxTimer, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxTimerDelete( dxTimerHandle_t dxTimer, const uint32_t dxMsToWait )
{
    return ( ertos_timer_delete( ( t_ertos_timerhandler )dxTimer, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxBOOL
dxTimerIsTimerActive( dxTimerHandle_t dxTimer )
{
    return ( ertos_timer_is_active( ( t_ertos_timerhandler )dxTimer ) == ERTOS_SUCCESS )? dxFALSE: dxTRUE;
}

dxOS_RET_CODE
dxTimerReset( dxTimerHandle_t dxTimer, const uint32_t dxMsToWait )
{
    return ( ertos_timer_reset( ( t_ertos_timerhandler )dxTimer, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxTimerRestart( dxTimerHandle_t dxTimer,
				const uint32_t dxTimerPeriodInMs,
				const uint32_t dxMsToWait )
{
    if ( dxTimerPeriodInMs == DX_TIMER_PERIOD_NO_CHANGE ) {
        return ( ertos_timer_reset( ( t_ertos_timerhandler )dxTimer, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
    } else {
        return ( ertos_timer_change_period( ( t_ertos_timerhandler )dxTimer, dxTimerPeriodInMs, ( dxMsToWait / portTICK_PERIOD_MS ) ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
    }
}

//============================================================================
// Time Information
//============================================================================
dxOS_RET_CODE
dxTimeDelayMS( const uint32_t xMSToDelay )
{
	usleep( xMSToDelay * 1000 );

    return DXOS_SUCCESS;
}

dxTime_t
dxTimeGetMS( void )
{
    static clock_t SystemStartTime = 0;

	clock_t 		CurrentTime;
	dxTime_t        CurrentTimeInMS;

    if ( SystemStartTime == 0 ) {
        SystemStartTime	= clock() * 10;
    }

	CurrentTime	= clock() * 10;

    CurrentTimeInMS = CurrentTime - SystemStartTime;
//dbg_error( "CurrentTime: %u, SystemStartTime: %u, CurrentTimeInMS: %u", CurrentTime, SystemStartTime, CurrentTimeInMS );

	return ( CurrentTimeInMS == 0 )? 1: CurrentTimeInMS;
}

static DKTime_t                 SwRtcTime;
static dxTime_t                 SwRtcTimeSincesystemTime = 0;

#if HW_RTC_SUPPORT
#define IntToPericom(i)         (((i - (i % 10)) / 10) * 0x10 + (i % 10))
#define IntToPericom24H(i)      ((((i - (i % 10)) / 10) * 0x10 + (i % 10)) | 0x40)
#define PericomToInt(i)         (((i - (i % 0x10)) / 0x10) * 10 + (i % 0x10))
#define PericomToInt24H(i)      ((((i & 0xBF) - ((i & 0xBF) % 0x10)) / 0x10) * 10 + ((i & 0xBF) % 0x10))
#endif  //#if HW_RTC_SUPPORT

dxOS_RET_CODE dxHwRtcGet(DKTime_t* TimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    //TODO: We need an mechanism to determine if this device has HW RTC or not
    //For example, old RTK gateways does not have RTC while new gateway or other hybridperipheral device using RTK may also contain HW RTC.
    //Since dxOS is shared amoung all project as main framework we will need either determine here or use project config defines

#if HW_RTC_SUPPORT
    uint8_t         i2cdata_write[1] = {0x0};
    uint8_t         i2cdata_read[0x10];

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    memset(i2cdata_read, 0x00, 0x10);
    dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 1, 1);
    int iReadRes = dxI2C_Master_Read(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_read, 0x10, 1);
//    G_SYS_DBG_LOG_V("RTC Read: 20%02x %x/%x w%d %d:%x:%x OSF:%x res = %d\r\n", i2cdata_read[6], i2cdata_read[5], i2cdata_read[4], i2cdata_read[3], PericomToInt24H(i2cdata_read[2]), i2cdata_read[1], i2cdata_read[0], i2cdata_read[0xF], iReadRes);
    if(iReadRes == 0x10)
    {
        // Data is vallid only if OSF bit == 0x0
        if ((i2cdata_read[0xF] & 0x80) == 0x0)
        {
            result = DXOS_SUCCESS;
            TimeRTC->sec = PericomToInt(i2cdata_read[0]);
            TimeRTC->min = PericomToInt(i2cdata_read[1]);
            TimeRTC->hour = PericomToInt24H(i2cdata_read[2]);
            TimeRTC->dayofweek = i2cdata_read[3];
            TimeRTC->day = PericomToInt(i2cdata_read[4]);
            TimeRTC->month = PericomToInt(i2cdata_read[5]) & 0x1F;
            if(i2cdata_read[5] & 0x80 != 0)
            {
                TimeRTC->year = PericomToInt(i2cdata_read[6]) + 2000;
            }
            else
            {
                TimeRTC->year = PericomToInt(i2cdata_read[6]) + 1900;
            }
        }
    }
    G_SYS_DBG_LOG_V("RTC Get(%d): %d %d/%d w%d %d:%d:%d OSF:0x%x\n", iReadRes, TimeRTC->year, TimeRTC->month, TimeRTC->day, TimeRTC->dayofweek, TimeRTC->hour, TimeRTC->min, TimeRTC->sec, i2cdata_read[0xF]);
    dxSemGive(dxRTC_Resource_SemaHandle);
#endif  //#if HW_RTC_SUPPORT

    return result;
}

dxOS_RET_CODE dxHwRtcSet(DKTime_t SetTimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    //TODO: We need an mechanism to determine if this device has HW RTC or not
    //For example, old RTK gateways does not have RTC while new gateway or other hybridperipheral device using RTK may also contain HW RTC.
    //Since dxOS is shared amoung all project as main framework we will need either determine here or use project config defines

#if HW_RTC_SUPPORT
    uint8_t         i2cdata_write[8] = {0x0, 0x0, 0x43, 0x09, 0x1, 0x12, 0x03, 0x18};
    uint8_t         i2cdata_read[10];

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    G_SYS_DBG_LOG_V("RTC Set: %d %d/%d w%d %d:%d:%d\n", SetTimeRTC.year, SetTimeRTC.month, SetTimeRTC.day, SetTimeRTC.dayofweek, SetTimeRTC.hour, SetTimeRTC.min, SetTimeRTC.sec);
    i2cdata_write[0] = 0x0;
    i2cdata_write[1] = IntToPericom(SetTimeRTC.sec);
    i2cdata_write[2] = IntToPericom(SetTimeRTC.min);
    i2cdata_write[3] = IntToPericom24H(SetTimeRTC.hour);
    i2cdata_write[4] = SetTimeRTC.dayofweek;
    i2cdata_write[5] = IntToPericom(SetTimeRTC.day);
    i2cdata_write[6] = IntToPericom(SetTimeRTC.month);
    i2cdata_write[7] = IntToPericom(SetTimeRTC.year % 100);
    if(SetTimeRTC.year >= 2000)
    {
        i2cdata_write[6] | 0x80;
    }
    int iWriteRes = dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 8, 1);
//    G_SYS_DBG_LOG_V("RTC Write: 20%02x %x/%x w%d %d:%x:%x res = %d\r\n", i2cdata_write[7], i2cdata_write[6], i2cdata_write[5], i2cdata_write[4], PericomToInt24H(i2cdata_write[3]), i2cdata_write[2], i2cdata_write[1], iWriteRes);
    if(iWriteRes > 0)
    {
		// Reset OSF bit
        i2cdata_write[0] = 0xF;
        i2cdata_write[1] = 0x0;
        int iWriteRes = dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 2, 1);
        if(iWriteRes > 0)
        {
            result = DXOS_SUCCESS;
        }
    }

    dxSemGive(dxRTC_Resource_SemaHandle);
#endif  //#if HW_RTC_SUPPORT

    return result;

}

dxOS_RET_CODE dxSwRtcGet(DKTime_t* TimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;
    if (SwRtcTimeSincesystemTime && TimeRTC)
    {
        dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

        int8_t TimeZoneOrg = SwRtcTime.TimeZone;
        dxTime_t rtcTime = ConvertDKTimeToUTC(SwRtcTime);
        dxTime_t time = dxTimeGetMS();

        if (time >= SwRtcTimeSincesystemTime) {
            rtcTime += ((time - SwRtcTimeSincesystemTime) / SECONDS);
        } else {
            rtcTime += ((0xFFFFFFFF - SwRtcTimeSincesystemTime + time) / SECONDS);
        }

        DKTime_t rtcDateTime = ConvertUTCToDKTime(rtcTime);
        memcpy(TimeRTC, &rtcDateTime, sizeof(DKTime_t));
        TimeRTC->TimeZone = TimeZoneOrg;

        dxSemGive(dxRTC_Resource_SemaHandle);

        result = DXOS_SUCCESS;
    }

    return result;
}

dxOS_RET_CODE dxSwRtcSet(DKTime_t SetTimeRTC)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

    if (SwRtcTimeSincesystemTime) {
        dxTime_t localTime = ConvertDKTimeToUTC (SwRtcTime);
        dxTime_t time = dxTimeGetMS();
        if (time >= SwRtcTimeSincesystemTime) {
            localTime += ((time - SwRtcTimeSincesystemTime) / SECONDS);
        } else {
            localTime += ((0xFFFFFFFF - SwRtcTimeSincesystemTime + time) / SECONDS);
        }
    }

    SwRtcTime = SetTimeRTC;
    SwRtcTimeSincesystemTime = dxTimeGetMS();
    dxSemGive(dxRTC_Resource_SemaHandle);

    return result;
}


dxOS_RET_CODE dxGetLocalTime (
    DKTimeLocal_t* localTime
) {
    dxOS_RET_CODE result = DXOS_ERROR;

    if (SwRtcTimeSincesystemTime && localTime)
    {
        dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

        dxTime_t CurrentTimeUTC = ConvertDKTimeToUTC(SwRtcTime);
        int offset = SwRtcTime.TimeZone * 15 * 60;
        CurrentTimeUTC += ((dxTimeGetMS() - SwRtcTimeSincesystemTime) / 1000) + offset;

        DKTimeLocal_t time = ConvertUTCToDKTimeLocal (CurrentTimeUTC);
        memcpy(localTime, &time, sizeof (DKTimeLocal_t));

        dxSemGive(dxRTC_Resource_SemaHandle);

        result = DXOS_SUCCESS;
    }

    return result;
}


DKTime_t dxGetUtcTime (void) {
    DKTime_t TimeRTC;

    memset (&TimeRTC, 0, sizeof (DKTime_t));
    dxOS_RET_CODE ret = dxSwRtcGet (&TimeRTC);
    if (ret != DXOS_SUCCESS) {
        TimeRTC = ConvertUTCToDKTime (dxTimeGetMS () / SECONDS);
    }

    return TimeRTC;
}


uint32_t dxGetUtcInSec (void) {
    DKTime_t TimeRTC;
    uint32_t RTCinSec;

    TimeRTC = dxGetUtcTime ();
    RTCinSec = ConvertDKTimeToUTC (TimeRTC);

    return RTCinSec;
}

//============================================================================
// Queue Management
//============================================================================
#define MQ_NAME_STR_MAX_SIZE				128
#define MQ_NAME_NON_CROSS_PROCESS_PREFIX	"dxMQ"

typedef struct {
	char			mqName[ MQ_NAME_STR_MAX_SIZE ];
	mqd_t   		mqID;
	struct mq_attr	mqOrgAttr;
} mq_info_t;

dxQueueHandle_t
dxQueueCreate( uint32_t uxQueueLength, uint32_t uxItemSize )
{
	dxQueueHandle_t dxQueueHandler = NULL;

	mq_info_t *pmq_info = dxMemAlloc( "pmq_info", 1, sizeof( mq_info_t ) );

	if ( pmq_info ) {
		/* create the message queue */
		struct timeval curr_time;

		gettimeofday( &curr_time, NULL );

		sprintf( pmq_info->mqName, "/%s-%ld-%ld", MQ_NAME_NON_CROSS_PROCESS_PREFIX, curr_time.tv_sec, curr_time.tv_usec );

		/* initialize the queue attributes */
		pmq_info->mqOrgAttr.mq_flags	= 0;
		pmq_info->mqOrgAttr.mq_maxmsg	= uxQueueLength;
		pmq_info->mqOrgAttr.mq_msgsize	= uxItemSize;
		pmq_info->mqOrgAttr.mq_curmsgs	= 0;

		pmq_info->mqID = mq_open( pmq_info->mqName, ( O_CREAT | O_RDWR ), ( S_IRWXU | S_IRWXG | S_IRWXO ), &pmq_info->mqOrgAttr );

		if ( pmq_info->mqID ) {
			dxQueueHandler = pmq_info;
		} else {
			dbg_error( "dxQueueCreate (%s) Err=%s\n", pmq_info->mqName, strerror( errno ) );
			dxMemFree( pmq_info );
		}
	}

	return dxQueueHandler;
}

dxQueueHandle_t
dxQueueGlobalCreate( void *GlobalIdentifier, uint32_t uxQueueLength, uint32_t uxItemSize )
{
	dxQueueHandle_t dxQueueHandler = NULL;
	char* MqPipeName = ( char * )GlobalIdentifier;

	if ( MqPipeName == NULL ) {
		dbg_error( "MqPipeName == NULL" );
	} else if ( strlen( MqPipeName ) >= MQ_NAME_STR_MAX_SIZE ) {
		dbg_error( "strlen( MqPipeName ) >= MQ_NAME_STR_MAX_SIZE" );
	} else {
		uint8_t i = 0;

		for ( i = 0; i < GLOBAL_QUEUE_MAX_LIST; i ++ ) {
			if ( dxGlobalQueueList[ i ].dxQueue == NULL ) {
				mq_info_t *pmq_info = dxMemAlloc( "pmq_info", 1, sizeof( mq_info_t ) );

				if ( pmq_info ) {
					strcpy( pmq_info->mqName, MqPipeName );

					/* initialize the queue attributes */
					pmq_info->mqOrgAttr.mq_flags	= 0;
					pmq_info->mqOrgAttr.mq_maxmsg	= uxQueueLength;
					pmq_info->mqOrgAttr.mq_msgsize	= uxItemSize;
					pmq_info->mqOrgAttr.mq_curmsgs	= 0;

					/* create the message queue */
					pmq_info->mqID = mq_open( pmq_info->mqName, ( O_CREAT | O_RDWR ), ( S_IRWXU | S_IRWXG | S_IRWXO ), &pmq_info->mqOrgAttr );

					if ( pmq_info->mqID ) {
						dxGlobalQueueList[ i ].dxQueue	= pmq_info;
						strcpy( dxGlobalQueueList[ i ].QueueName, MqPipeName );

						dxQueueHandler = pmq_info;
					} else {
						dbg_error( "dxQueueCreate (%s) Err=%s\n", pmq_info->mqName, strerror( errno ) );
						dxMemFree( pmq_info );
					}
				}

				break;
			}
		}
	}

	return dxQueueHandler;
}


dxQueueHandle_t
dxQueueGlobalGet( void *GlobalIdentifier )
{
	dxQueueHandle_t dxQueueHandler = NULL;
	char *MqPipeName = ( char * )GlobalIdentifier;

	if ( MqPipeName == NULL ) {
		dbg_error( "MqPipeName == NULL" );
	} else if ( strlen( MqPipeName ) >= MQ_NAME_STR_MAX_SIZE ) {
		dbg_error( "strlen( MqPipeName ) >= MQ_NAME_STR_MAX_SIZE" );
	} else {
		uint8_t i = 0;

		for ( i = 0; i < GLOBAL_QUEUE_MAX_LIST; i ++ ) {
			if ( dxGlobalQueueList[ i ].dxQueue ) {
				if ( strcmp( MqPipeName, dxGlobalQueueList[ i ].QueueName ) == 0 ) {
					dxQueueHandler	= dxGlobalQueueList[ i ].dxQueue;
					break;
				}
			}
		}
	}

    return dxQueueHandler;
}


dxOS_RET_CODE
dxQueueReceive( dxQueueHandle_t dxQueue, void *pvBuffer, uint32_t dxMsToWait )
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if ( dxQueue && pvBuffer ) {
		ssize_t mq_sizeRcv	= -1;
		mq_info_t *pmq_info	= ( mq_info_t * )dxQueue;

		if ( dxMsToWait == DXOS_WAIT_FOREVER ) {
			mq_sizeRcv = mq_receive( pmq_info->mqID, pvBuffer, pmq_info->mqOrgAttr.mq_msgsize, NULL );
		} else {
			if ( dxMsToWait == 0 ) {
				if ( dxQueueIsEmpty( dxQueue ) == dxTRUE ) {
					return DXOS_ERROR;
				}
			}

# if 0
			dbg_error( "dxMsToWait: %u", dxMsToWait );

			struct timespec WaitToTime	= IntUtilAddOffsetToCurrentTime( dxMsToWait );

			dbg_error( "WaitToTime.tv_sec: %u, WaitToTime.tv_nsec: %u", WaitToTime.tv_sec, WaitToTime.tv_nsec );

			mq_sizeRcv = mq_timedreceive( pmq_info->mqID, pvBuffer, pmq_info->mqOrgAttr.mq_msgsize, NULL, &WaitToTime );
# else
			uint32_t start_tick = ticks;

			dxMsToWait /= 10;

			while ( ( start_tick + dxMsToWait ) > ticks ) {
				if ( dxQueueIsEmpty( dxQueue ) == dxFALSE ) {
					mq_sizeRcv = mq_receive( pmq_info->mqID, pvBuffer, pmq_info->mqOrgAttr.mq_msgsize, NULL );
					break;
				}

				dxTimeDelayMS( 50 );
			}
# endif
		}

		if ( mq_sizeRcv < 0 ) {
			retCode = DXOS_ERROR;
		}
	} else {
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE
dxQueueReceiveFromISR( dxQueueHandle_t dxQueue, void *pvBuffer, dxBOOL *HigherPriorityTaskWoken )
{
	return dxQueueReceive( dxQueue, pvBuffer, DXOS_WAIT_FOREVER );
}

dxOS_RET_CODE
dxQueueSend( dxQueueHandle_t dxQueue, const void *pvItemToQueue, uint32_t dxMsToWait )
{
	if ( dxQueue && pvItemToQueue ) {
		mq_info_t* pmq_info = ( mq_info_t * )dxQueue;

		return dxQueueSendWithSize( dxQueue, pvItemToQueue, pmq_info->mqOrgAttr.mq_msgsize, dxMsToWait );
	} else {
		return DXOS_ERROR;
	}
}

dxOS_RET_CODE
dxQueueSendWithSize( dxQueueHandle_t dxQueue,
					 const void *pvItemToQueue,
					 uint32_t MessageSizeInByte,
					 uint32_t dxMsToWait)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if ( dxQueue && pvItemToQueue ) {
		int mq_sizeSent = -1;
		mq_info_t *pmq_info = ( mq_info_t * )dxQueue;

		if ( MessageSizeInByte > pmq_info->mqOrgAttr.mq_msgsize ) {
			retCode = DXOS_INVALID;
		} else {
			if ( dxMsToWait == DXOS_WAIT_FOREVER ) {
				mq_sizeSent = mq_send( pmq_info->mqID, pvItemToQueue, MessageSizeInByte, 0 );
			} else {
				struct timespec WaitToTime	= IntUtilAddOffsetToCurrentTime( dxMsToWait );
				mq_sizeSent = mq_timedsend( pmq_info->mqID, pvItemToQueue, MessageSizeInByte, 0, &WaitToTime );
			}

			if ( mq_sizeSent < 0 ) {
				retCode = DXOS_ERROR;
			}
		}
	} else {
		retCode = DXOS_ERROR;
	}

	return retCode;
}

dxOS_RET_CODE
dxQueueSendFromISR( dxQueueHandle_t dxQueue, const void *pvItemToQueue, dxBOOL *HigherPriorityTaskWoken )
{
	return dxQueueSend( dxQueue, pvItemToQueue, DXOS_WAIT_FOREVER );
}

uint32_t
dxQueueSpacesAvailable( const dxQueueHandle_t dxQueue )
{
	uint32_t SpaceAvailable = 0;

	if ( dxQueue ) {
		mq_info_t *pmq_info = ( mq_info_t * )dxQueue;

		int Success = mq_getattr( pmq_info->mqID, &pmq_info->mqOrgAttr );
		if ( Success == 0 ) {
			SpaceAvailable = pmq_info->mqOrgAttr.mq_maxmsg - pmq_info->mqOrgAttr.mq_curmsgs;
		}
	}

	return SpaceAvailable;
}

dxOS_RET_CODE
dxQueueReset( dxQueueHandle_t dxQueue, dxQueueResetFunction_t dxResetFunction )
{
    while ( dxQueueIsEmpty( dxQueue ) != dxTRUE ) {
        void *item = NULL;

        if ( dxQueueReceive( dxQueue, &item, 0 ) ) {
            if ( item && dxResetFunction ) {
                dxResetFunction( item );
            }
        }
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxQueueDelete( dxQueueHandle_t dxQueue )
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

    if ( dxQueue ) {
    	mq_info_t* pmq_info = ( mq_info_t * )dxQueue;

    	if ( mq_close( pmq_info->mqID ) == 0 ) {
    		RetCode = DXOS_SUCCESS;
    	}

    	dxMemFree( dxQueue );
    }

    return RetCode;
}

dxBOOL
dxQueueIsEmpty( dxQueueHandle_t dxQueue )
{
	dxBOOL IsEmpty = dxFALSE;

    if ( dxQueue ) {
    	mq_info_t* pmq_info = ( mq_info_t * )dxQueue;

    	int Success = mq_getattr( pmq_info->mqID, &pmq_info->mqOrgAttr );
    	if ( Success == 0 ) {
    		IsEmpty = ( pmq_info->mqOrgAttr.mq_curmsgs == 0 )? dxTRUE: dxFALSE;
    	}
    }

    return IsEmpty;
}

dxBOOL
dxQueueIsFull( dxQueueHandle_t dxQueue )
{
	dxBOOL IsFull = dxTRUE;

    if ( dxQueue ) {
    	mq_info_t *pmq_info = ( mq_info_t * )dxQueue;

    	int Success = mq_getattr( pmq_info->mqID, &pmq_info->mqOrgAttr );
    	if ( Success == 0 ) {
    		IsFull = ( pmq_info->mqOrgAttr.mq_curmsgs == pmq_info->mqOrgAttr.mq_maxmsg )? dxTRUE: dxFALSE;
    	}
    }

    return IsFull;
}

//============================================================================
// Task Management
//============================================================================
# define USE_THREAD

typedef struct {
	pthread_t   threadId;
} pThread_info_t;

dxOS_RET_CODE
dxTaskCreate( dxTaskFunction_t pvTaskCode,
			  const char *pcName,
			  uint16_t usStackSizeInByte,
			  void *pvParameters,
			  dxTaskPriority_t uxPriority,
			  dxTaskHandle_t *pvCreatedTask )
{
	uint32_t Priority;

	switch ( uxPriority ) {
    	case DXTASK_PRIORITY_IDLE:
    		Priority	= 16;
    		break;
    	case DXTASK_PRIORITY_NORMAL:
    		Priority	= 13;
    		break;
    	case DXTASK_PRIORITY_HIGH:
    		Priority	= 10;
    		break;
    	case DXTASK_PRIORITY_VERY_HIGH:
    		Priority	= 7;
    		break;
    	case DXTASK_PRIORITY_EXTREAMLY_HIGH:
    		Priority	= 5;
    		break;
    	default:
    		Priority	= 13;
    }

# ifdef USE_THREAD
    pThread_info_t *pThreadInfo = dxMemAlloc( "THREAD", 1, sizeof( pThread_info_t ) );
    if ( pThreadInfo ) {
		// create thread
		pthread_attr_t pthread_attr;

		pthread_attr_init( &pthread_attr );
		pthread_attr.schedparam.sched_priority = Priority;

		char *pthread_stack = ( char * )dxMemAlloc( "THREAD", 1, usStackSizeInByte );

		if ( pthread_stack == NULL ) {
			dbg_error( "[ERROR]: malloc fail!\n" );
			pthread_attr_destroy( &pthread_attr );
			return DXOS_ERROR;
		}

		pthread_attr_setstack( &pthread_attr, pthread_stack, usStackSizeInByte );

		if ( pthread_create( &pThreadInfo->threadId, NULL, pvTaskCode, pvParameters ) != 0 ) {
			dxMemFree( pThreadInfo );
			pthread_attr_destroy( &pthread_attr );
			return DXOS_ERROR;
		}

		pthread_attr_destroy( &pthread_attr );

		*pvCreatedTask = pThreadInfo;
    }

    return DXOS_SUCCESS;

# else

    *pvCreatedTask	= ( dxTaskHandle_t )ertos_task_create( ( void * )pvTaskCode,
														   ( char * )pcName,
														   usStackSizeInByte, //Convert to depth
														   pvParameters,
														   Priority );

    return ( *pvCreatedTask )? DXOS_SUCCESS: DXOS_ERROR;
# endif
}

dxOS_RET_CODE
dxTaskSuspend( dxTaskHandle_t dxTaskToSuspend )
{
# ifdef USE_THREAD

# else
	ertos_task_suspend( ( t_ertos_taskhandler )dxTaskToSuspend );

# endif
	return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxTaskResume( dxTaskHandle_t dxTaskToResume )
{
# ifdef USE_THREAD

# else
	ertos_task_resume( ( t_ertos_taskhandler )dxTaskToResume );

# endif
	return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxTaskPrioritySet( dxTaskHandle_t dxTask, uint32_t uxNewPriority )
{
# ifdef USE_THREAD

# else
	ertos_task_priority_set( ( t_ertos_taskhandler )dxTask, uxNewPriority );

# endif
	return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxTaskDelete( dxTaskHandle_t dxTaskToDelete )
{
# ifdef USE_THREAD
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxTaskToDelete)
	{
		int status = -1;
		pThread_info_t* pThreadInfo = ( pThread_info_t * )dxTaskToDelete;

		status = pthread_cancel( pThreadInfo->threadId );
		if ( status !=  0 ) {
			perror( "dxTaskDelete pthread_cancel failed" );
		} else {
			void *RetVal;

			//TODO: Should think of a way to have timeout (pthread_timedjoin_np is NOT portable)
			//		otherwise we may be stuck in here forever.
			status = pthread_join( pThreadInfo->threadId, &RetVal );
			if ( status <  0 ) {
				perror("dxTaskDelete pthread_join failed");
			} else {
				RetCode = DXOS_SUCCESS;
				dxMemFree( pThreadInfo );
			}
		}

	}

    return RetCode;
# else
	ertos_task_delete( ( t_ertos_taskhandler )dxTaskToDelete );

	return DXOS_SUCCESS;
# endif
}

dxTaskHandle_t
dxTaskGetCurrentTaskHandle( void )
{
    return NULL;
}

static void
dkworker_task_main( uint32_t arg )
{
    dxWorkweTask_t *worker_task = ( dxWorkweTask_t * )arg;

    while ( 1 ) {
        dxEventMsg_t message;

        if ( dxQueueReceive( worker_task->WEventQueue, &message, 1000 ) == DXOS_SUCCESS ) {
            if ( message.function ) {
				message.function( message.arg );
            }
        }
    }
}

dxOS_RET_CODE
dxWorkerTaskCreate( const char *name, dxWorkweTask_t *pWorkerTask, dxTaskPriority_t uxPriority, uint16_t usStackDepth, uint32_t EventQueueSize )
{
	memset( pWorkerTask, 0, sizeof( dxWorkweTask_t ) );

	pWorkerTask->WEventQueue = dxQueueCreate( EventQueueSize, sizeof( dxEventMsg_t ) );

	if ( pWorkerTask->WEventQueue == NULL ) {
		return DXOS_ERROR;
	}

	if ( dxTaskCreate( ( dxTaskFunction_t )dkworker_task_main,
					   name,
					   usStackDepth,
					   ( void * )pWorkerTask,
					   uxPriority,
					   &pWorkerTask->WTask ) != DXOS_SUCCESS ) {
		dxQueueDelete( pWorkerTask->WEventQueue );
		return DXOS_ERROR;
	}

	return DXOS_SUCCESS;

}

dxOS_RET_CODE
dxWorkerTaskDelete( dxWorkweTask_t *WorkerTask )
{
	dxTaskDelete( WorkerTask->WTask );
	dxQueueDelete( WorkerTask->WEventQueue );

	return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxWorkerTaskSendAsynchEvent( dxWorkweTask_t *WorkerTask, dxEventHandler_t function, void *arg )
{
	dxEventMsg_t message;

	message.function = function;
	message.arg = arg;

	return dxQueueSend( WorkerTask->WEventQueue, &message, 0 );
}

//============================================================================
// Task Stack - TO BE VERIFIED
//============================================================================

#pragma message ( "REMINDER: MINCLUDE_uxTaskGetStackHighWaterMark must be set to 1 in FreeRTOSConfig.h to use uxTaskGetStackHighWaterMark()" )

dxOS_RET_CODE
dxTaskGetStackInfo( dxTaskHandle_t dxTask, dxTaskStack_t *pStackBuf )
{
	if ( ( dxTask == NULL ) || ( pStackBuf == NULL ) ) {
		return DXOS_ERROR;
	} else {
		memset( pStackBuf, 0x00, sizeof( dxTaskStack_t ) );

# ifdef USE_THREAD

		return DXOS_ERROR;

# else
		int len = 0;

		pStackBuf->nPriority	= ertos_task_priority( ( t_ertos_taskhandler )dxTask );
		pStackBuf->nStackSize	= 0;

		len = strlen( ertos_task_name( ( t_ertos_taskhandler )dxTask ) );
		if ( len >= sizeof( pStackBuf->szTaskName ) ) {
			len = sizeof( pStackBuf->szTaskName ) - 1;
		}

		if ( len ) {
			memcpy( pStackBuf->szTaskName, ertos_task_name( ( t_ertos_taskhandler )dxTask ), len );
		}

		return DXOS_SUCCESS;
# endif
	}
}

//============================================================================
// Semaphore Management
//============================================================================
typedef struct
{
	sem_t			SemId;
	uint32_t   		MaxCount;
} Sem_info_t;

dxSemaphoreHandle_t
dxSemCreate( uint32_t uxMaxCount, uint32_t uxInitialCount )
{
	dxSemaphoreHandle_t SemHandler = NULL;

	if (uxMaxCount)
	{
		Sem_info_t* pSemInfo = dxMemAlloc("SEM", 1, sizeof(Sem_info_t));
		if (pSemInfo)
		{
			int SeminitRet = sem_init(&pSemInfo->SemId, 0, uxInitialCount);
			if (SeminitRet == 0)
			{
				pSemInfo->MaxCount = uxMaxCount;
				SemHandler = pSemInfo;
			}
			else
			{
				printf("sem_init Err=%s\n", strerror(errno));
				dxMemFree(pSemInfo);
			}
		}
	}

    return SemHandler;
//	return ( dxSemaphoreHandle_t )ertos_sem_create();
}

dxOS_RET_CODE
dxSemGive( dxSemaphoreHandle_t dxSemaphore )
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

	int sVal = -1;
	Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

	if (sem_getvalue(&SemInfo->SemId, &sVal) != 0)
	{
		printf("sem_getvalue Err=%s\n", strerror(errno));
		goto SemGiveExit;
	}

	if (sVal >= 0)
	{
		if (sVal < SemInfo->MaxCount)
		{
			if  (sem_post(&SemInfo->SemId) != 0)
			{
				printf("sem_post Err=%s\n", strerror(errno));
				goto SemGiveExit;
			}
		}
	}

	RetCode = DXOS_SUCCESS;

SemGiveExit:

    dxMutexGive(dxSem_Resource_MutexHandle);

    return RetCode;
//	return ( ertos_sem_send( ( t_ertos_semhandler )dxSemaphore ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxSemTake( dxSemaphoreHandle_t dxSemaphore, uint32_t xBlockTimeInMS )
{
	dxOS_RET_CODE RetCode = DXOS_SUCCESS;

	if (dxSemaphore)
	{
		dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

		Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

		if (xBlockTimeInMS == DXOS_WAIT_FOREVER)
		{
			dxMutexGive(dxSem_Resource_MutexHandle);

			int SemRet = sem_wait(&SemInfo->SemId);
			if (SemRet < 0)
				RetCode = DXOS_ERROR;
		}
		else
		{

			struct timespec WaitToTime = IntUtilAddOffsetToCurrentTime( xBlockTimeInMS );

			dxMutexGive(dxSem_Resource_MutexHandle);

			int SemRet = sem_timedwait(&SemInfo->SemId, &WaitToTime);

			if (SemRet < 0)
			{
				if (errno == ETIMEDOUT)
					RetCode = DXOS_TIMEOUT;
				else
					RetCode = DXOS_ERROR;
			}
		}

	}
	else
	{
		RetCode = DXOS_ERROR;
	}

    return RetCode;
//	return ( ertos_sem_recv( ( t_ertos_semhandler )dxSemaphore, xBlockTimeInMS ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxSemDelete( dxSemaphoreHandle_t dxSemaphore )
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxSemaphore)
	{
		dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

		Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

		if (sem_destroy(&SemInfo->SemId) != 0)
		{
			printf("sem_destroy Err=%s\n", strerror(errno));
		}
		else
		{
			dxMemFree(dxSemaphore);
			RetCode = DXOS_SUCCESS;
		}

		dxMutexGive(dxSem_Resource_MutexHandle);
	}

    return RetCode;
//	ertos_sem_delete( ( t_ertos_semhandler )dxSemaphore );
//
//	return DXOS_SUCCESS;
}


//============================================================================
// Mutex Management
//============================================================================

typedef struct
{
	pthread_mutex_t   MutexId;

} pThreadMutex_info_t;

dxMutexHandle_t
dxMutexCreate(void)
{
	pThreadMutex_info_t *pThreadMutexInfo = dxMemAlloc( "THREADMUTEX", 1, sizeof( pThreadMutex_info_t ) );

    if ( pThreadMutexInfo ) {
		if ( pthread_mutex_init( &pThreadMutexInfo->MutexId, NULL ) != 0 ) {
			dxMemFree( pThreadMutexInfo );
			pThreadMutexInfo	= NULL;
		}
    }

    return ( dxMutexHandle_t )pThreadMutexInfo;
}

dxOS_RET_CODE dxMutexTake(dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS)
{

	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		int MutexRet = -1;
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;

		if (xBlockTimeInMS == DXOS_WAIT_FOREVER)
		{
			MutexRet = pthread_mutex_lock(&pThreadMutexInfo->MutexId);
		}
		else
		{
			struct timespec WaitToTime	= IntUtilAddOffsetToCurrentTime( xBlockTimeInMS );

			MutexRet = pthread_mutex_timedlock( &pThreadMutexInfo->MutexId, &WaitToTime );
		}

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
		else if (MutexRet == ETIMEDOUT)
			retCode = DXOS_TIMEOUT;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE dxMutexGive(dxMutexHandle_t dxMutex)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;
		int MutexRet = pthread_mutex_unlock(&pThreadMutexInfo->MutexId);

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE dxMutexDelete(dxMutexHandle_t dxMutex)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;
		int MutexRet = pthread_mutex_destroy(&pThreadMutexInfo->MutexId);

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
		else
			dxMemFree(dxMutex);
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}
/*
dxMutexHandle_t
dxMutexCreate( void )
{
	return ( dxMutexHandle_t )ertos_mutex_create();
}

dxOS_RET_CODE
dxMutexTake( dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS )
{
	return ( ertos_mutex_lock( ( t_ertos_mutexhandler )dxMutex, xBlockTimeInMS ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxMutexGive( dxMutexHandle_t dxMutex )
{
	return ( ertos_mutex_unlock( ( t_ertos_mutexhandler )dxMutex ) == ERTOS_SUCCESS )? DXOS_SUCCESS: DXOS_ERROR;
}

dxOS_RET_CODE
dxMutexDelete( dxMutexHandle_t dxMutex )
{
	ertos_mutex_delete( ( t_ertos_mutexhandler )dxMutex );

	return DXOS_SUCCESS;
}
*/

dxAesContext_t
dxAESSW_ContextAlloc( void )
{
	return ( dxAesContext_t )dxMemAlloc( "SwAesContext", 1, AES_ENC_DEC_BYTE );
}


dxAES_RET_CODE
dxAESSW_Setkey( dxAesContext_t ctx, const unsigned char *key, unsigned int keysize, dxAES_CRYPTO_TYPE Type )
{
	dxAES_RET_CODE ret = DXAES_SUCCESS;

	//Currently we only support 128bit SW AES
	if ( keysize != 128 ) {
		return DXAES_INVALID_PARAM;
	}

	if ( ctx && key ) {
		memcpy( ctx, key, keysize / 8 );
	} else {
		ret = DXAES_INVALID_PARAM;
	}

	return ret;
}


dxAES_RET_CODE
dxAESSW_CryptEcb( dxAesContext_t ctx, dxAES_CRYPTO_TYPE Type, const unsigned char input[ 16 ], unsigned char output[ 16 ] )
{
	dxAES_RET_CODE ret = DXAES_SUCCESS;

	if ( ctx ) {
		if ( Type == DXAES_ENCRYPT ) {
			AES128_ECB_encrypt( input, ctx, output );
		} else if ( Type == DXAES_DECRYPT ) {
			AES128_ECB_decrypt( input, ctx, output );
		} else {
			ret = DXAES_INVALID_PARAM;
		}
	} else {
		ret = DXAES_INVALID_PARAM;
	}

	return ret;
}

dxAES_RET_CODE
dxAESSW_ContextDealloc( dxAesContext_t ctx )
{
	dxAES_RET_CODE ret = DXAES_SUCCESS;

	if ( ctx ) {
		dxMemFree( ctx );
	}

	return ret;
}

uint32_t
dxRand( void )
{
	dbg_error( "Todo..." );
	//srand( ( uint32_t )ADC0_seed32() );
	return rand();
}

void  dxSPIaccess_ResourceTake(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE SemRet = dxSemTake(dxSPI_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    if ( SemRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceTake failed!!!!!\r\n");
    }
}

void  dxSPIaccess_ResourceGive(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE SemRet = dxSemGive(dxSPI_Resource_SemaHandle);
    if ( SemRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceGive failed!!!!!\r\n");
    }
}

int dxNumOfDigits (uint32_t v) {
    uint32_t po10;
    int n;

    n=1;
    po10=10;

    while (v >= po10) {
        n++;
        if (po10 > PO10_LIMIT) break;
            po10 *= 10;
    }

    return n;
}
