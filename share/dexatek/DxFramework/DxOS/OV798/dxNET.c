//============================================================================
// File: dxNET.c
//============================================================================
#include "dxOS.h"

#include "lwip/opt.h"
#include "lwip/icmp.h"
#include "lwip/inet_chksum.h"
#include "lwip/sockets.h"
#include "lwip/mem.h"
#include "lwip/inet.h"
#include "netif/etharp.h"
#include "lwip/ip.h"
#include "lwip/tcpip.h"
#include "lwip/dhcp.h"
#if LWIP_VERSION >= 0x02000000
#include "lwip/prot/dhcp.h"
#endif
#include "lwip/ip_addr.h"
#include "lwip/init.h"
#include "lwip/dns.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

//============================================================================
// Socket Option
//============================================================================

int dxSOCKET_SetTimeout(int s, uint32_t msec)
{
    int ret = 0;

    /**
     * How to use setsockopt() ?
     * (1) For lwIP 1.4.1 and before,
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&msec, sizeof(msec));
     *
     * (2) For lwIP 1.5.0 and later,
     *     struct timeval interval = { .tv_sec = msec / 1000,
     *                                 .tv_usec = (msec % 1000) * 1000 };
     *
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));
     */

#if DK_BSP_RTK_HOMEKIT_ENABLE
#pragma message ( "REMINDER: setsockopt() comply to lwIP 1.5.0!" )

    struct timeval interval = { .tv_sec = msec / 1000,
                                .tv_usec = (msec % 1000) * 1000 };

    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));
#else // DK_BSP_RTK_HOMEKIT_ENABLE
#pragma message ( "REMINDER: setsockopt() comply to lwIP 1.4.1!" )
    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char *)&msec, sizeof(msec));
#endif // DK_BSP_RTK_HOMEKIT_ENABLE

    return ret;
}


//============================================================================
// DHCP Client
//============================================================================

# define MAX_IPADDR_LEN			32
# define WLAN_MAGIC_NUM			0x57494649

typedef struct {
	uint32_t	magic_num;

	char		ip[ MAX_IPADDR_LEN ];
	char		netmask[ MAX_IPADDR_LEN ];
	char		gw[ MAX_IPADDR_LEN ];
	char		dns[ MAX_IPADDR_LEN ];
} wlan_env_t;

static dxBOOL 			netif_inited		= dxFALSE;
static struct netif		wlan_netif;
static wlan_env_t		wlan_env;
static struct dhcp		dhcp_ctrl;

extern err_t netif_wifi_init( struct netif * netif );

static void
wlan_link_callback( struct netif *netif )
{
	if ( netif ) {
		if ( netif_is_link_up( netif ) ) {
			dbg_warm( "NETIF_LINK_STATUS_ON" );
			libwifi_reconnect();
		} else {
			dbg_warm( "NETIF_LINK_STATUS_OFF" );
		}
	}

	dbg_warm();
}

static void
wlan_status_callback( struct netif * netif )
{
	if ( netif ) {
		if ( netif_is_up( netif ) ) {
			dbg_warm( "NETIF_LINK_STATUS_UP" );
		} else {
			dbg_warm( "NETIF_LINK_STATUS_DOWN" );
		}
	}

	dbg_warm();
}

static dxNET_RET_CODE
wlan_save_ip( void )
{
	struct netif	*netif = netif_find( "wl0" );
	char			*temp;

	if ( netif == NULL ) {
		return DXNET_ERROR;
	}

	memset( &wlan_env, 0x00, sizeof( wlan_env_t ) );

	wlan_env.magic_num	= WLAN_MAGIC_NUM;

	//FIXME: inet_ntoa() uses static buffer inside, don't use it continuosly
	temp = inet_ntoa( netif->ip_addr );
	if( temp == NULL ){
		return DXNET_ERROR;
	}

	sprintf( wlan_env.ip, "%s", temp );
	dbg_warm( "IP: %s", temp );

	temp = inet_ntoa( netif->netmask );
	if( temp == NULL ){
		return DXNET_ERROR;
	}
	sprintf( wlan_env.netmask, "%s", temp );
	dbg_warm( "Netmask: %s", temp );

	temp = inet_ntoa( netif->gw );
	if( temp == NULL ){
		return DXNET_ERROR;
	}
	sprintf( wlan_env.gw, "%s", temp );
	dbg_warm( "Gateway: %s", temp );

	const ip_addr_t *dns = dns_getserver( 0 );
	temp = inet_ntoa( *dns );
	if( temp  ){
		sprintf( wlan_env.dns, "%s", temp );
		dbg_warm( "DNS Server: %s", temp );
	}

	sf_write( &wlan_env, sizeof( wlan_env_t ), CONFIG_SYSTEM_ENV_OFFSET );

	return DXNET_SUCCESS;
}

static dxNET_RET_CODE
wlan_get_env( void )
{
	sf_read( &wlan_env, sizeof( wlan_env_t ), CONFIG_SYSTEM_ENV_OFFSET );

	if ( wlan_env.magic_num	!= WLAN_MAGIC_NUM ) {
		dbg_warm( "wlan_env.magic_num	!= WLAN_MAGIC_NUM" );
		return DXNET_ERROR;
	} else {
		return DXNET_SUCCESS;
	}
}

static dxNET_RET_CODE
dhcp_init()
{
	// add net interface
	ip_addr_t ipaddr, netmask, gw, dnsserver;

	if ( wlan_get_env() == DXNET_SUCCESS ) {
		inet_aton( wlan_env.ip, &ipaddr );
		inet_aton( wlan_env.netmask, &netmask );
		inet_aton( wlan_env.gw, &gw );
		ipaddr_aton( wlan_env.dns, &dnsserver );
	} else {
		ip_addr_set_zero( &ipaddr );
		ip_addr_set_zero( &gw );
		ip_addr_set_zero( &netmask );
		ip_addr_set_zero( &dnsserver );
	}

	dbg_warm( "ip: %s", wlan_env.ip );

	memset( &wlan_netif, 0, sizeof( wlan_netif ) );

	struct netif *netif_ret = netif_add( &wlan_netif, &ipaddr, &netmask, &gw, NULL, netif_wifi_init, ethernet_input );
	if ( netif_ret == NULL ) {
		dbg_warm( "[ERROR]: Netif Add Fail, Fatal Error!!" );
		return DXNET_ERROR;
	}

	netif_set_default( &wlan_netif );

	netif_set_up( &wlan_netif ); //set up netif here to fit lwip v2.0
	dns_setserver( 0, &dnsserver );

#if LWIP_NETIF_LINK_CALLBACK
	netif_set_link_callback( &wlan_netif, wlan_link_callback );
#endif

#if LWIP_NETIF_STATUS_CALLBACK
	netif_set_status_callback( &wlan_netif, wlan_status_callback );
#endif

	if ( netif_inited == dxFALSE ) {
		netif_inited = dxTRUE;
	}

	dbg_warm();

	return DXNET_SUCCESS;
}

dxNET_RET_CODE
dxDHCP_Start( void )
{
	if ( netif_inited == dxFALSE ) {
		dhcp_init();
	}

	//NOTE: This function MUST be blocking, make sure the porting satisfy this requirement!
	struct netif *netif = netif_find( "wl0" );

	if ( netif ) {
		int dhcp_starttime = ticks;

		memset( &dhcp_ctrl, 0, sizeof( struct dhcp ) );

		dhcp_set_struct( netif, &dhcp_ctrl );
		dhcp_start( netif );

#define DHCP_TIMEOUT_INTERVAL 	500	//10s
		while ( ( dhcp_ctrl.state != DHCP_BOUND ) && ( ( ticks - dhcp_starttime ) < DHCP_TIMEOUT_INTERVAL ) ) {
//		while ( ( netif->ip_addr.addr == 0 ) && ( ( ticks - dhcp_starttime ) < DHCP_TIMEOUT_INTERVAL ) ) {
			ertos_timedelay( 100 );
		}

		if ( dhcp_ctrl.state == DHCP_BOUND ) {
			wlan_save_ip();

			return DXNET_SUCCESS;
		} else {
			dhcp_stop( netif );
		}
	}

	return DXNET_ERROR;
}

dxNET_RET_CODE
dxDHCP_Stop( void )
{
	dbg_warm();
	struct netif *netif = netif_find( "wl0" );

	if ( netif ) {
		dhcp_stop( netif );
		return DXNET_SUCCESS;
	}

	return DXNET_ERROR;
}

dxNET_RET_CODE
dxDHCP_Release( void )
{
	dbg_warm();
	dxDHCP_STATE state = 0;
	struct netif *netif = netif_find( "wl0" );

	if ( netif ) {
		dhcp_release( netif );

		return DXNET_SUCCESS;
	}

	return DXNET_ERROR;
}


dxNet_Addr_t
dxDHCP_Info( void )
{
	dbg_warm();
    dxNet_Addr_t dxNetAddr;

    memset( &dxNetAddr, 0x00, sizeof( dxNetAddr ) );

	struct netif *netif = netif_find( "wl0" );

	if ( netif ) {
	    dxNetAddr.ip_addr.version = DXIP_VERSION_V4;
	    memcpy( &dxNetAddr.ip_addr.v4[ 0 ], &ip_2_ip4( netif->ip_addr ), sizeof( dxNetAddr.ip_addr.v4 ) );

	    dxNetAddr.netmask.version = DXIP_VERSION_V4;
	    memcpy( &dxNetAddr.netmask.v4[ 0 ], &ip_2_ip4( netif->netmask ), sizeof( dxNetAddr.netmask.v4 ) );

	    dxNetAddr.gw.version = DXIP_VERSION_V4;
	    memcpy( &dxNetAddr.gw.v4[ 0 ], &ip_2_ip4( netif->gw ), sizeof( dxNetAddr.gw.v4 ) );

		libwifi_get_mac( dxNetAddr.mac_addr );
	}

    return dxNetAddr;
}

//============================================================================
// Multicast
//============================================================================

#define IPBIN_TO_STR(x,y)         sprintf(x, "%d.%d.%d.%d",\
                                             y.v4[0],\
                                             y.v4[1],\
                                             y.v4[2],\
                                             y.v4[3])

typedef struct
{
    int sockId;                 // Socket ID
    dxSocketAddr_t SocketAddr;  // Multicast socket
} dxMulticastHandle_t;

void dxMCAST_Init(void)
{
    // Reserve for new Features in the futrue.
	dbg_warm( "Todo..." );
    return;
}

dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port)
{
	dbg_warm( "Todo..." );
	return NULL;

    dxMulticastHandle_t *pHandle = NULL;
    pHandle = dxMemAlloc("dxMCAST_Join", 1, sizeof(dxMulticastHandle_t));

    if (pHandle == NULL)
    {
        return NULL;
    }

    pHandle->sockId = -1;
    pHandle->SocketAddr.GroupIP.version = DXIP_VERSION_V4;
    memset(pHandle->SocketAddr.GroupIP.v4, 0x00, sizeof(pHandle->SocketAddr.GroupIP.v4));
    pHandle->SocketAddr.port = 0;

    int ret = socket(AF_INET, SOCK_DGRAM, 0);

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->sockId = ret;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, UDPGroupIp);

    // Add multicast group membership on this interface
    struct ip_mreq imr;
    memset(&imr, 0x00, sizeof(imr));
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_ADD_MEMBERSHIP, &imr, sizeof(imr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // Specify outgoing interface too
    struct in_addr intfAddr;
    memset(&intfAddr, 0x00, sizeof(intfAddr));
    intfAddr.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_MULTICAST_IF, &intfAddr, sizeof(struct in_addr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // And start listening for packets
    struct sockaddr_in bindAddr;
    memset(&bindAddr, 0x00, sizeof(bindAddr));
    bindAddr.sin_family = AF_INET;
    bindAddr.sin_port = htons(port);
    bindAddr.sin_addr.s_addr = INADDR_ANY;
    ret = bind(pHandle->sockId, (struct sockaddr *) &bindAddr, sizeof(bindAddr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->SocketAddr.GroupIP = UDPGroupIp;
    pHandle->SocketAddr.port = port;

    return (dxUDPHandle_t)pHandle;
}

dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle)
{
	dbg_warm( "Todo..." );
	return DXNET_ERROR;

    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;
    int ret = DXNET_ERROR;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, pHandle->SocketAddr.GroupIP);
    // Add multicast group membership on this interface
    struct ip_mreq imr;
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_DROP_MEMBERSHIP, &imr, sizeof(imr));
    if (ret < 0)
    {
        return DXNET_ERROR;
    }

    ret = close(pHandle->sockId);

    dxMemFree(pHandle);
    pHandle = NULL;

    return (ret == 0) ? DXNET_SUCCESS : DXNET_ERROR;
}

int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs)
{
	dbg_warm( "Todo..." );
	return DXNET_ERROR;

    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;

    uint32_t TimeOutRcv = (uint32_t)TimeOutInMs;

    int ret;

    if (dxSOCKET_SetTimeout(pHandle->sockId, TimeOutRcv) == -1) {
        dbg_warm( "dxMCAST_RecvFrom - setsockopt failed to config" );
    }

    //int ret = recvfrom(pHandle->sockId, Buffer, BuffSize, flags, pFromAddr, (socklen_t *)NULL);
    if (pFromSocket == NULL)
    {
        ret = recvfrom(pHandle->sockId, Buffer, BuffSize, 0, NULL, (socklen_t *)NULL);
    }
    else
    {
        char szIPv4[16] = {0};
        IPBIN_TO_STR(szIPv4, pFromSocket->GroupIP);

        struct sockaddr_in sour;
        memset(&sour, 0x00, sizeof(sour));
        sour.sin_family = AF_INET;
        sour.sin_port = htons(pFromSocket->port);
        sour.sin_addr.s_addr = inet_addr(szIPv4);
        ret = recvfrom(pHandle->sockId, Buffer, BuffSize, 0, &sour, (socklen_t *)NULL);
    }

    // Possible error code
    // > 0:
    // 0:
    // -1:
    // For more detail error code, please refer to 'err.h' located in
    // 'component\common\network\lwip\v1.4.1\src\include\lwip'

    if (ret > 0)
    {
        return ret;
    }
    else if (ret == 0)
    {
        return DXNET_CONN_CLOSED;
    }

    return DXNET_ERROR;
}

int
dxMCAST_SendTo( dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket )
{
	dbg_warm( "Todo..." );
	return DXNET_ERROR;

	if ( handle == NULL || dataptr == NULL ) {
		return DXNET_ERROR;
	} else {
    	dxMulticastHandle_t *pHandle = ( dxMulticastHandle_t * )handle;
	    char szIPv4[ 16 ] = { 0, };

	    struct sockaddr_in dest;
	    memset( &dest, 0x00, sizeof( struct sockaddr_in ) );
	    dest.sin_family = AF_INET;

	    if ( pToSocket == NULL ) {
	        IPBIN_TO_STR( szIPv4, pHandle->SocketAddr.GroupIP );
	        dest.sin_port = htons( pHandle->SocketAddr.port );
	    } else {
	        IPBIN_TO_STR( szIPv4, pToSocket->GroupIP );
	        dest.sin_port = htons( pToSocket->port );
	    }

	    dest.sin_addr.s_addr = inet_addr( szIPv4 );

	    //int ret = sendto(pHandle->sockId, dataptr, dataSize, flags, ToAddr, (socklen_t)NULL);
	    int ret = sendto( pHandle->sockId, dataptr, dataSize, 0, &dest, ( socklen_t )sizeof( dest ) );

	    // Possible error code
	    // > 0:
	    // -1:
	    // ERR_VAL:
	    // ERR_ARG:
	    // ERR_MEM:
	    // For more detail error code, please refer to 'err.h' located in
	    // 'component\common\network\lwip\v1.4.1\src\include\lwip'

	    return ( ret > 0 ) ? ret : DXNET_ERROR;
	}
}
