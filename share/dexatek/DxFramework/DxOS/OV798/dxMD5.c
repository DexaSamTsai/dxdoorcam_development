//============================================================================
// File: dxMD5.c
//============================================================================
#include "dxMD5.h"

static const char hex_chars[] = "0123456789abcdef";

void
dxMD5( const unsigned char *input, size_t ilen, unsigned char output[ 16 ] )
{
    MD5( input, ilen, output );
}

int
dxMD5_Starts( dxMD5_CTX *ctx )
{
	return MD5_Init( ctx );
}

int
dxMD5_Update( dxMD5_CTX *ctx, const void *data, size_t len )
{
	return MD5_Update( ctx, data, len );
}

int
dxMD5_Finish( unsigned char *md, dxMD5_CTX *ctx )
{
	return MD5_Final( md, ctx );
}

void
dxMD5_convert_hex( unsigned char *md, unsigned char *mdstr )
{
    int i;
    int j = 0;
    unsigned int c;

    for ( i = 0; i < 16; i ++ ) {
        c = ( md[ i ] >> 4 ) & 0x0f;
        mdstr[ j ++ ] = hex_chars[ c ];
        mdstr[ j ++ ] = hex_chars[ md[ i ] & 0x0f ];
    }

    mdstr[ 32 ] = '\0';
}

