//============================================================================
// File: dxWIFI.c
//============================================================================

#include "dxOS.h"
#include "dxWIFI.h"
#include "dxBSP.h"
#include "Utils.h"

//============================================================================
// Defines and Macro
//============================================================================

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

#define DX_MAX_SCAN_AP_NUMBER                       64  // The value is the same as max_ap_size that defines in wifi_conf.c

#define SCAN_RESULT_DEFAULT_WORKER_PRIORITY          DXTASK_PRIORITY_IDLE
#define SCAN_RESULT_WORKER_THREAD_STACK_SIZE         1024
#define SCAN_RESULT_WORKER_THREAD_MAX_EVENT_QUEUE    2

//==========================================================================
// Static Params
//==========================================================================
static dxBOOL wifi_inited		= dxFALSE;
static dxBOOL wifi_connected	= dxFALSE;

static t_wifi_conf conf;

# ifdef TEST_MAC
static u8	DxDoorCam_mac[ 6 ] = TEST_MAC;
# else
static u8	DxDoorCam_mac[ 6 ] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x00, };
# endif

static u8	*wlan_para_addr = 0;
static u32	wlan_para_len = 0;

static dxWiFi_Scan_Result_Handler_t	dxWiFi_Scan_Results_Handler	= NULL;
static dxWiFi_Scan_State_t			dxWiFiScanState				= DXWIFI_SCAN_STATE_NONE;
static void							*dxWiFi_Scan_UserData		= NULL;

static dxWorkweTask_t   ScanResultWorkerThread = {
	.WTask			= NULL,
	.WEventQueue	= NULL
};

static struct {
	char	*ssid;
	char	*pwd;
} wfif_info;

//==========================================================================
// Extern Functions
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
wifi_scan_result_handler( void *arg )
{
	if ( dxWiFi_Scan_Results_Handler ) {
	    int i;
	    ap_info_t		ap_list[ 30 ];
	    ap_info_num_t	cnt = { 0, };

		dxWiFi_Scan_Result_t	dxScan_Result;

	    memset( ap_list, 0, ( sizeof( ap_info_t ) * 30 ) );
	    memset( &cnt, 0, sizeof( ap_info_num_t ) );

	    cnt.max_cnt	= 30;
	    libwifi_scan( ap_list, &cnt );

	    dbg_warm( "ap num %d\n", cnt.ap_cnt );

	    for( i = 0; i < cnt.ap_cnt; i ++ ) {
	    	memset( &dxScan_Result, 0, sizeof( dxWiFi_Scan_Result_t ) );

	        dbg_warm( "[%d] %s\t %x-%x-%x-%x-%x-%x\t %d\t %d\t, %s, %s, %s, %s", i,
	                        ap_list[ i ].ssid,
	                        ap_list[ i ].bssid[ 0 ], ap_list[ i ].bssid[ 1 ], ap_list[ i ].bssid[ 2 ],
	                        ap_list[ i ].bssid[ 3 ] ,ap_list[ i ].bssid[ 4 ], ap_list[ i ].bssid[ 5 ],
	                        ap_list[ i ].channel,
	                        ap_list[ i ].rssi,
	                        BSS_TYPE_STR[ ap_list[ i ].bss_type ],
	                        SCAN_SECURITY_STR[ ap_list[ i ].scan_security ],
	                        SCAN_ENCRYPTION_STR[ ap_list[ i ].scan_encryption ],
	                        WPS_STATUS_STR[ ap_list[ i ].wps ] );

			snprintf( ( char * )dxScan_Result.ap_details.SSID.val, 32, "%s", ap_list[ i ].ssid );
			memcpy( dxScan_Result.ap_details.BSSID.octet, ap_list[ i ].bssid, 6 );

			dxScan_Result.ap_details.SSID.len			= strlen( ( char * )ap_list[ i ].ssid );
			dxScan_Result.ap_details.signal_strength	= ap_list[ i ].rssi;
			dxScan_Result.ap_details.security			= ap_list[ i ].scan_security;
			dxScan_Result.ap_details.channel			= ap_list[ i ].channel;

			dxScan_Result.ap_details.bss_type			= DXWIFI_BSS_TYPE_INFRASTRUCTURE;
			dxScan_Result.ap_details.wps_type			= DXWIFI_WPS_TYPE_DEFAULT;
			dxScan_Result.ap_details.band				= DXWIFI_802_11_BAND_2_4GHZ ;
			dxScan_Result.scan_complete					= dxFALSE;
			dxScan_Result.user_data						= dxWiFi_Scan_UserData;
			//unsigned char *p = &malloced_scan_result->ap_details.BSSID.octet[0];

			dbg_warm( "%s:\t", dxScan_Result.ap_details.SSID.val );
			dbg_warm( "%02X:%02X:%02X:%02X:%02X:%02X\t",	dxScan_Result.ap_details.BSSID.octet[ 0 ], dxScan_Result.ap_details.BSSID.octet[ 1 ], dxScan_Result.ap_details.BSSID.octet[ 2 ],
															dxScan_Result.ap_details.BSSID.octet[ 3 ], dxScan_Result.ap_details.BSSID.octet[ 4 ], dxScan_Result.ap_details.BSSID.octet[ 5 ] );

			dxWiFi_Scan_Results_Handler( &dxScan_Result );
	    }

		//Send the scan complete to upper layer
		memset( &dxScan_Result, 0x00, sizeof( dxWiFi_Scan_Result_t ) );

		dxScan_Result.scan_complete	= dxTRUE;
		dxScan_Result.user_data		= dxWiFi_Scan_UserData;

		dxWiFi_Scan_Results_Handler( &dxScan_Result );
	}

    dxWiFiScanState = DXWIFI_SCAN_STATE_DONE;
}

//==========================================================================
// APIs
//==========================================================================
dxWIFI_RET_CODE
dxWiFi_Connect( char *ssid, dxWiFi_Security_t security_type, char *password, int key_id, dxSemaphoreHandle_t *dxsemaphore )
{
	char wifi_chg = 0;

	if ( ssid == NULL ) {
		dbg_warm( "ssid == NULL!!" );
		return DXWIFI_ERROR;
	}

	if ( !( wfif_info.ssid && ( strcmp( wfif_info.ssid, ssid ) == 0 ) ) ) {
		wifi_chg	= 1;
	}

	if ( !( wfif_info.pwd && password && ( strcmp( wfif_info.pwd, password ) == 0 ) ) ) {
		wifi_chg	= 1;
	}

	wifi_connected		= dxFALSE;

	uint8_t conn_retries = 0;

	dump( DxDoorCam_mac, 6 );

	wlan_para_addr	= NULL;
	wlan_para_len	= 0;

	libwifi_para_getbuf( &wlan_para_addr, ( u32 * )&wlan_para_len );

    if ( ( wlan_para_addr == NULL ) && ( wlan_para_len > 0 ) ) {
		return DXWIFI_ERROR;
    }

	if ( wifi_chg == 0 ) {
		dbg_warm( "wifi_chg == 0" );
    	sf_read( wlan_para_addr, wlan_para_len, DK_FLASH_USERCONFIG_WIFIPARA_ADDR );
    } else {
		dbg_warm( "wifi_chg == 1" );

    	if ( wfif_info.ssid ) {
    		dxMemFree( wfif_info.ssid );
    	}

    	if ( wfif_info.pwd ) {
    		dxMemFree( wfif_info.pwd );
    	}

    	wfif_info.ssid	= NULL;
    	wfif_info.pwd	= NULL;

    	wfif_info.ssid	= dxMemAlloc( NULL, 1, strlen( ssid ) + 1 );
    	sprintf( wfif_info.ssid, "%s", ssid );

    	if ( password ) {
    		wfif_info.pwd	= dxMemAlloc( NULL, 1, strlen( password ) + 1 );
    		sprintf( wfif_info.pwd, "%s", password );
    	}

    	memset( wlan_para_addr, 0x00, wlan_para_len );
    }

    if ( libwifi_para_restore( wlan_para_addr ) ) {
        dbg_warm( "Load wifi para from SFlash\n" );
	} else {
        dbg_warm( "Init wifi para\n" );
	}

	memset( &conf, 0, sizeof( t_wifi_conf ) );

# ifdef CONFIG_WIFIMODULE_INTERFACE_SCIF_02
	if ( libwifi_init( SCIF_MODULE_02, DxDoorCam_mac, 0, &conf ) != WIFI_SUCCESS ) {
# elif CONFIG_WIFIMODULE_INTERFACE_SCIF_03
	if ( libwifi_init( SCIF_MODULE_03, DxDoorCam_mac, 0, &conf ) != WIFI_SUCCESS ) {
# else
	if ( libwifi_init( SCIF_MODULE_01, DxDoorCam_mac, 0, &conf ) != WIFI_SUCCESS ) {
# endif
		dbg_warm( "[ERROR]: Wifi Init Fail, Fatal Error!!" );
		return DXWIFI_ERROR;
	}

	memset( &conf, 0, sizeof( t_wifi_conf ) );

	int security = 3;

	conf.security = ( int * )( &security );

# define WIFI_MAX_RETRIES		3
	while ( conn_retries < WIFI_MAX_RETRIES ) {
		if ( libwifi_connect( ssid, password, ( u32 )strlen( password ), NULL, 0, &conf ) == 0 ) {
			dbg_warm( "WIFI Connected!\n" );
			break;
		} else {
			dbg_warm( "WIFI Connect %dth Retry Fail.\n", conn_retries );
			conn_retries ++;
		}
	}

	if ( conn_retries == WIFI_MAX_RETRIES ) {
		dbg_warm( "[ERROR]: Wifi Connect Fail!!\n" );
		return DXWIFI_ERROR;
	}

	if ( libwifi_para_save( wlan_para_addr ) != 0 ) {
		dbg_warm( "wlan_para[%s][%d]\n", wlan_para_addr, wlan_para_len );
	    sf_write( wlan_para_addr, wlan_para_len, DK_FLASH_USERCONFIG_WIFIPARA_ADDR );
	}

	wifi_connected		= dxTRUE;

	return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE
dxWiFi_Disconnect( void )
{
	if ( wifi_connected == dxTRUE ) {
		wifi_connected		= dxFALSE;

		if ( libwifi_stop() == WIFI_SUCCESS ) {
			return DXWIFI_SUCCESS;
		} else {
			return DXWIFI_ERROR;
		}
	}

	return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE
dxWiFi_GetMAC( dxWiFi_MAC_t *pMAC )
{
	if ( pMAC == NULL ) {
		dbg_warm( "pMAC == NULL" );
		return DXWIFI_ERROR;
	}

	if ( wifi_inited == dxFALSE ) {
		dbg_warm( "wifi_inited == dxFALSE" );
		return DXWIFI_ERROR;
	}

//	if ( wifi_connected == dxFALSE ) {
//		dbg_warm( "wifi_connected == dxFALSE" );
//		return DXWIFI_ERROR;
//	}

//	libwifi_get_mac( pMAC->octet );
	dbg_warm();
	memcpy( pMAC->octet, DxDoorCam_mac, 6 );

	return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE
dxWiFi_IsOnline( dxWiFi_Mode_t xMode )
{
	if ( wifi_connected == dxTRUE ) {
		struct netif *netif = netif_find( "wl0" );

		if ( netif ) {
			if ( netif_is_link_up( netif ) == 0 ) {
				dbg_warm( "netif_is_link_up( netif ) == 0" );
			} else if ( netif_is_up( netif ) == 0 ) {
				dbg_warm( "netif_is_up( netif ) == 0" );
			} else {
				return DXWIFI_SUCCESS;
			}
		} else {
			dbg_warm( "netif_is_link_up( netif ) == FALSE" );
		}
	}

	return DXWIFI_ERROR;
}

dxWIFI_RET_CODE
dxWiFi_Off( void )
{
	//Set filter to wifi chip
	struct netif *netif = netif_find( "wl0" );

	if ( netif != NULL ) {
		// only ip packages to the device can wake it up
		char *ip_filter = "105 0 0 12 0xFFFF00000000000000000000000000000000FFFFFFFF 0x080000000000000000000000000000000000FFFFFFFF";
		int i;
		u32 ip_addr = ntohl( *( u32 * )( &netif->ip_addr ) );

		for ( i = 0; i < 8; i ++ ) {
			if ( ( ip_addr % 16 ) < 10 ) {
				ip_filter[ 103 - i ] = '0' + ( ip_addr % 16 );
			} else {
				ip_filter[ 103 - i ] = 'A' + ( ip_addr % 16 ) - 10;
			}

			ip_addr = ip_addr / 16;
		}

		libwifi_set_filter_by_user( ip_filter, 1 );
	}

	libwifi_set_sleep( 0 );

	if ( wifi_connected == dxTRUE ) {
		dxWiFi_Disconnect();
	}

	if ( libwifi_stop() == WIFI_SUCCESS ) {
		wifi_inited = dxFALSE;
		return DXWIFI_SUCCESS;
	} else {
		return DXWIFI_ERROR;
	}
}

dxWIFI_RET_CODE
dxWiFi_On( dxWiFi_Mode_t mode )
{
    return dxWiFi_On_Ext( mode, DXWIFI_CHANNEL_PLAN_DEFAULT, DXWIFI_ADAPTIVITY_DISABLE );
}

dxWIFI_RET_CODE
dxWiFi_On_Ext( dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv )
{
	wfif_info.ssid	= NULL;
	wfif_info.pwd	= NULL;

	wlan_para_addr	= NULL;
	wlan_para_len	= 0;

	if ( ScanResultWorkerThread.WTask == NULL ) {
		if ( dxWorkerTaskCreate( "WPACTL W", &ScanResultWorkerThread, SCAN_RESULT_DEFAULT_WORKER_PRIORITY, SCAN_RESULT_WORKER_THREAD_STACK_SIZE, SCAN_RESULT_WORKER_THREAD_MAX_EVENT_QUEUE ) != DXOS_SUCCESS ) {
			G_SYS_DBG_LOG_V(  "wpa_ctrl_scan_results - Failed to create worker thread\r\n" );
			return DXWIFI_ERROR;
		}

		GSysDebugger_RegisterForThreadDiagnosticGivenName( &ScanResultWorkerThread.WTask, "wpa_ctrl_scan_results Event Thread" );
	}

	wifi_inited = dxTRUE;

	return DXWIFI_SUCCESS;
}

dxWiFi_Scan_State_t dxWiFi_GetScanState(void)
{
	return dxWiFiScanState;
}

dxWIFI_RET_CODE
dxWiFi_Scan( dxWiFi_Scan_Result_Handler_t results_handler, void *user_data )
{
	dxWiFi_Scan_Results_Handler = results_handler;

	dxWiFiScanState			= DXWIFI_SCAN_STATE_SCANNING;
	dxWiFi_Scan_UserData	= user_data;

# if 1
	wifi_scan_result_handler( NULL );

	return DXWIFI_SUCCESS;
# else
    if ( dxWorkerTaskSendAsynchEvent( &ScanResultWorkerThread, wifi_scan_result_handler, NULL ) != DXOS_SUCCESS ) {
        dbg_warm( "FATAL - wpa_ctrl_scan_results Failed to send Event message back to Application\r\n" );
		dxWiFiScanState = DXWIFI_SCAN_STATE_DONE;
		return DXWIFI_ERROR;
	} else {
		return DXWIFI_SUCCESS;
	}
# endif
}

dxWIFI_RET_CODE
dxWiFi_WaitForOnline( dxWiFi_Mode_t xMode, uint32_t xMSToWait )
{
    uint32_t value = 0;
    dxWIFI_RET_CODE Return = DXWIFI_TIMEOUT;

    while ( value < xMSToWait ) {
        dxWIFI_RET_CODE ret = dxWiFi_IsOnline( xMode );

        if ( ret == DXWIFI_SUCCESS ) {
            Return = DXWIFI_SUCCESS;
            break;
        }

        dxTimeDelayMS( 1000 );
        value += 1000;
    }

    return Return;
}

