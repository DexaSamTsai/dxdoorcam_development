//============================================================================
// File: dxNET.h
//
//============================================================================
#ifndef _DXNET_H
#define _DXNET_H

#pragma once

#include "includes.h"
#include <stdlib.h>
#include <stdint.h>

//============================================================================
// NET Client
//============================================================================

typedef struct dxIPv4 {
    uint8_t ip_addr[4];
} dxIPv4_t;

typedef enum {
    DXIP_VERSION_V4         = (1 << 0),
    DXIP_VERSION_V6         = (1 << 1),     // NOT SUPPORT. Need LwIP 1.5.0
    DXIP_VERSION_BOTH       = (DXIP_VERSION_V4 | DXIP_VERSION_V6),
} dxIP_Version_t;

typedef struct dxIP_Addr {
    dxIP_Version_t version;
    uint8_t v4[4];
    uint16_t v6[8];
} dxIP_Addr_t;

typedef struct dxNet_Addr {
    uint8_t mac_addr[6];
    dxIP_Addr_t ip_addr;
    dxIP_Addr_t netmask;
    dxIP_Addr_t gw;
} dxNet_Addr_t;

typedef enum {

    DXNET_SUCCESS = 0,
    DXNET_ERROR   = -1,
    DXNET_CONN_CLOSED = -2

} dxNET_RET_CODE;

int dxSOCKET_SetTimeout(int s, uint32_t msec);

//============================================================================
// DHCP Client
//============================================================================

typedef enum {
    DXDHCP_START            = 0,
    DXDHCP_WAIT_ADDRESS,
    DXDHCP_ADDRESS_ASSIGNED,
    DXDHCP_RELEASE_IP,
    DXDHCP_STOP,
    DXDHCP_TIMEOUT
} dxDHCP_STATE;

/**
 * @brief This is a blocking function, will try obtaining IP address or timeout is reached
 * @return dxDHCP_STATE
 */
dxNET_RET_CODE
dxDHCP_Start( void );

/**
 * @return dxDHCP_STATE
 */
dxNET_RET_CODE
dxDHCP_Stop( void );

/**
 * @return dxDHCP_STATE
 */
dxNET_RET_CODE
dxDHCP_Release( void );


/**
 * @return dxNet_Addr_t
 */
dxNet_Addr_t dxDHCP_Info(void);

//============================================================================
// Multicast UDP API
//============================================================================
typedef struct
{
    dxIP_Addr_t GroupIP;    // Multicast group IP
    uint16_t port;          // Multicast group port
} dxSocketAddr_t;


typedef void * dxUDPHandle_t;

/**
 * @brief initialization
 */
void dxMCAST_Init(void);

/**
 * @brief join specific multicast IP group and port
 * @param UDPGroupIp: multicast group IP, cant be NULL
 * @param port: specific port, cant be NULL
 * @return dxUDPHandle_t    if the socket joined multicast UDP group
 *         NULL             otherwise
 */
dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port);

/**
 * @brief the handle leave current multicast group, and close socket, at last free the handle
 * @param handle: include socket ID, UDP group IP, port
 * @return DXNET_SUCCESS    if the socket left multicast UDP group
 *         DXNET_ERROR      otherwise
 */
dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle);

/**
 * @brief receive the multicast data from specific group IP
 * @param handle: include socket ID, UDP group IP, port
 * @param Buffer: Data buffer pointer
 * @param BuffSize: Data buffer size
 * @param flags(DISABLE): message flags, default is 0
 * @param pFromAddr: Data source dxSocketAddr_t or NULL
 * @param TimeOutInMs: time out setting with millisecond
 * @return > 0                  if number of bytes received
 *         DXNET_CONN_CLOSED    if socket connection closed
 *         DXNET_ERROR          otherwise
 */
//int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, int flags, dxIP_Addr_t *pFromAddr, uint32_t TimeOutInMs);
int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs);

/**
 * @brief send the multicast data to specific group IP
 * @param handle: include socket ID, UDP group IP, port
 * @param dataptr: Data buffer pointer
 * @param dataSize: Data buffer size
 * @param flags(DISABLE): message flags, default is 0
 * @param ToAddr: Data destination dxSocketAddr_t or NULL
 * @return > 0                  if number of bytes sent
 *         DXNET_ERROR          otherwise
 */
//int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, int flags, dxIP_Addr_t ToAddr);
int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket);


#endif // _DXNET_H
