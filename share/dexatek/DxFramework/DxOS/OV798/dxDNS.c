//============================================================================
// File: dxDNS.c
//
//============================================================================
#include "dxDNS.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

//============================================================================
// DNS Client
//============================================================================
dxNET_RET_CODE
dxDNS_GetHostByName( uint8_t *hostname, dxIPv4_t *dxip )
{
# if 1
	struct addrinfo hints, *servinfo, *p;
	int status;
	dxNET_RET_CODE res = DXNET_ERROR;

	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family		= AF_INET;
	hints.ai_socktype	= SOCK_STREAM;

	if ( ( status = getaddrinfo( hostname, NULL, &hints, &servinfo ) ) != 0 ) {
		return res;
	}

	for ( p = servinfo; p != NULL; p = p->ai_next ) {
		if ( p->ai_family == AF_INET ) { // IPv4
			char ipstr[ 32 ];
			struct sockaddr_in *ipv4 = ( struct sockaddr_in * )p->ai_addr;
			inet_ntop( p->ai_family, &ipv4->sin_addr, ipstr, INET_ADDRSTRLEN );
			memcpy( dxip, &ipv4->sin_addr, 4 );
			res = DXNET_SUCCESS;
dbg_warm( "ipstr: %s", ipstr );
			break;
		}
	}

	freeaddrinfo( servinfo );

    return res;

# else
    struct hostent *ret;
    ret = gethostbyname( hostname );

dbg_warm( "%s", hostname );

	if ( ret == NULL ) {
	    dbg_warm( "GetHostByName Fail." );
        return DXNET_ERROR;
    }

    /* cast to short because of win16 winsock definition */
    if ( ( short )ret->h_addrtype != AF_INET ) {
        dbg_warm( "gethostbyname addr is not AF_INET" );
	    return DXNET_ERROR;
	}

    memcpy( dxip, ret->h_aliases, 4 );

    return DXNET_SUCCESS;
# endif
}
