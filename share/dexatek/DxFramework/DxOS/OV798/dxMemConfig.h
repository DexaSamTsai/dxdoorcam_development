//============================================================================
// File: dxMemConfig.h
//============================================================================

#ifndef _DXMEMCONFIG_H
#define _DXMEMCONFIG_H


/**
 * Please check hardware to identify the flash size.
 * Enable one of the following
 *     CONFIG_DX_FLASH_SIZE_8MBIT
 *     CONFIG_DX_FLASH_SIZE_16MBIT
 *     CONFIG_DX_FLASH_SIZE_32MBIT
 */

#define CONFIG_DX_FLASH_SIZE_8MBIT                 0
#define CONFIG_DX_FLASH_SIZE_16MBIT                1

/** RTK 8711/95 BASE ON 16Mbit Flash Size
 *
 * Flash Block Size: 4096 bytes
 *
 *   0x08000       +------------------------------------+
 *                 | BACKUP_SECTOR                      |
 *   0x09000       +------------------------------------+
 *                 | FLASH_SYSTEM_DATA_ADDR             |
 *   0x0A000       +------------------------------------+
 *                 | FLASH_CAL_DATA_BASE                |
 *   0x0B000       +------------------------------------+
 *                 | IMAGE_2 (960 KB)                   |
 *   0xFB000       +------------------------------------+ <== DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR (NewImg2Addr)
 *                 | Upgrade Image 2 (960 KB)           |
 *  0x1EB000       +------------------------------------+
 *                 | Unused space (64 KB)               |
 *  0x1FB000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR1
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *  0x1FC000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR2
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *  0x1FD000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR1
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *  0x1FE000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR2
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *  0x1FF000 + 000 +------------------------------------+ <== DX_HOMEKIT_SETTING_DATA_ADDR
 *                 | PersistentSetupcode_t (20 bytes)   |
 *           + 020 +------------------------------------+
 *                 | Reserved (236 bytes)               |
 *           + 100 +------------------------------------+ <== DX_HOMEKIT_WAC_FLASH_OFFSET
 *                 | WACPersistentConfig_t (140 bytes)  |
 *           + 190 +------------------------------------+ <== DX_HOMEKIT_HAP_KEYPAIR_OFFSET
 *                 | HAPPersistentKeypair_t (68 bytes)  |
 *           + 1E0 +------------------------------------+ <== DX_HOMEKIT_HAP_PAIRING_OFFSET
 *                 | HAPPersistentPairing_t (108 bytes) |
 *                 |                                    |
 *                 |  X 20                              |
 *                 |                                    |
 *                 |                                    |
 *           + A50 +------------------------------------+
 *                 | Reserved (1456 bytes)              |
 *  0x200000 (2MB) +------------------------------------+
 */

#define CONFIG_DX_FLASH_SIZE_32MBIT					0

#define DX_DEVINFO_FLASH_SIZE                       ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 )        // MUST align to a SECTOR of flash, typical 4KB
#define DX_SYSINFO_FLASH_SIZE                       ( CONFIG_DKFRAMEWORK_CONF_SIZE / 2 )        // MUST align to a SECTOR of flash, typical 4KB

#define DX_FLASH_SIZE_TOTAL_MBITS					16

#define DX_DEVINFO_FLASH_ADDR1						CONFIG_DKFRAMEWORK_CONF_OFFSET    // MUST be revised
#define DX_DEVINFO_FLASH_ADDR2                      CONFIG_DKFRAMEWORK_CONF_BK_OFFSET

#define DX_SYSINFO_FLASH_ADDR1                      ( DX_DEVINFO_FLASH_ADDR1 + DX_DEVINFO_FLASH_SIZE )
#define DX_SYSINFO_FLASH_ADDR2                      ( DX_DEVINFO_FLASH_ADDR2 + DX_DEVINFO_FLASH_SIZE )

#define DX_HP_STORAGE_ADDR1							CONFIG_DXDOORCAM_TEMP_OFFSET

#define DX_HP_STORAGE_SIZE							( 1024 * 512 ) // MUST align to a SECTOR of flash, typical 512KB

#define DX_HOMEKIT_SETTING_DATA_ADDR                ( CONFIG_DKFRAMEWORK_TEMP_OFFSET + DX_HP_STORAGE_SIZE )
#define DX_HOMEKIT_SETTING_DATA_SIZE                0x1000

#define DX_HOMEKIT_SETUPCODE_OFFSET                 0x000
#define DX_HOMEKIT_WAC_FLASH_OFFSET                 0x100
#define DX_HOMEKIT_HAP_KEYPAIR_OFFSET               0x190
#define DX_HOMEKIT_HAP_PAIRING_OFFSET               0x1E0

#define DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR		CONFIG_TEMP_OFFSET    // MUST be revised
#define DX_SYSINFO_FLASH_UPDATE_IMG_SIZE			( 1024 * 960 )  // MUST be revised

#define DX_SUBIMG_FLASH_ADDR1						DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR
#define DX_SUBIMG_FLASH_SIZE						( 1024 * 1024 ) // MUST align to a SECTOR of flash, typical 1MB

#endif // _DXMEMCONFIG_H
