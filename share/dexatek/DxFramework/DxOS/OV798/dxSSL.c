//============================================================================
// File: dxSSL.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include "dxSSL.h"

#include <sys/types.h>
#include <sys/socket.h>
//#include <sys/sysinfo.h>
//#include <netinet/in.h>
//#include <netdb.h>
//#include <arpa/inet.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>

#include "dxSysDebugger.h"

#define POLAR_SSL_MAX_WRITE_ATTEMPTS                3
# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

//============================================================================
// SSL/TLS Client
//============================================================================
typedef struct _dxSSLContext
{
    SSL_CTX *ctx;
    SSL     *ssl;
} dxSSLContext_t;

#define DXSSL_PRINTF                G_SYS_DBG_LOG_V

//#define DXSOCKET_CLOSE(x)           shutdown(x, SHUT_RDWR)
#define DXSOCKET_CLOSE(x)           close( x )

void
dxSSL_Lock( void )
{
}

void
dxSSL_Unlock( void )
{
}

dxSSL_RET_CODE
dxSSL_Close( dxSSLHandle_t *dxSSL )
{
    if (dxSSL == NULL)
    {
        return DXSSL_ERROR;
    }

    dxSSLContext_t *pSSL = (dxSSLContext_t *)dxSSL->ssl;
    if (pSSL)
    {
        if (pSSL->ssl)
        {
            SSL_shutdown(pSSL->ssl);
            SSL_set_connect_state(pSSL->ssl);
            DXSOCKET_CLOSE(SSL_get_fd(pSSL->ssl));

            SSL_free(pSSL->ssl);
        }

        if (pSSL->ctx)
        {
            SSL_CTX_free(pSSL->ctx);
        }

        dxMemFree(pSSL);
    }

    dxMemFree(dxSSL);

    return DXSSL_SUCCESS;
}

dxSSLHandle_t *
dxSSL_Connect( const dxIP_Addr_t* host_ip, uint16_t port, const char* cert, uint8_t endpoint, uint8_t verify )
{
	// TODO: Please refer related file in RTKAmeba

    int ret = 0;

    // These function calls initialize openssl for correct work.
    OpenSSL_add_all_algorithms();
    ERR_load_BIO_strings();
    ERR_load_crypto_strings();
    SSL_load_error_strings();

    // initialize SSL library and register algorithms
    SSL_library_init();

    // Set SSLv2 client hello, also announce SSLv3 and TLSv1
    const SSL_METHOD *method = SSLv23_client_method();

    // Try to create a new SSL context
    SSL_CTX *ctx = SSL_CTX_new(method);
    if (ctx == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_CTX_new() == NULL\r\n",
                         __LINE__);
        return NULL;
    }

#if SUPPORT_PREFER_CIPHER_LIST
    if (SSL_CTX_set_cipher_list(ctx, allowedCiphers) != 1)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_CTX_set_cipher_list() == NULL\r\n",
                         __LINE__);

        SSL_CTX_free(ctx);
        return NULL;
    }
#endif // SUPPORT_PREFER_CIPHER_LIST

    // Disabling SSLv2 will leave v3 and TSLv1 for negotiation
    SSL_CTX_set_options(ctx, SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3);

    // Create new SSL connection state object
    SSL *ssl = SSL_new(ctx);
    if (ssl == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_new() == NULL\r\n",
                         __LINE__);

        SSL_CTX_free(ctx);
        return NULL;
    }

    // Verify IP is IPv4
    if ((host_ip->version & DXIP_VERSION_V4) == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) NOT IPv4 address\r\n",
                         __LINE__);

        // We only support IPv4 currently.
        SSL_free(ssl);
        SSL_CTX_free(ctx);
        return NULL;
    }

    // create the basic TCP socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) socket() with ERROR:(%d)\r\n",
                         __LINE__,
                         errno );

        SSL_free(ssl);
        SSL_CTX_free(ctx);
        return NULL;
    }

    char szIPv4[16] = {0};
    sprintf(szIPv4, "%d.%d.%d.%d", host_ip->v4[0], host_ip->v4[1], host_ip->v4[2], host_ip->v4[3]);

    struct sockaddr_in dest_addr;
    dest_addr.sin_family = AF_INET;
    dest_addr.sin_port = htons(port);
    dest_addr.sin_addr.s_addr = inet_addr(szIPv4);

    // Zeroing the rest of the struct
    memset(&(dest_addr.sin_zero), '\0', sizeof(dest_addr.sin_zero));

    // Try to make the host connect here
    if (connect(sockfd, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr)) == -1)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) connect() to %s:%d with ERROR:(%d)\r\n",
                         __LINE__,
                         szIPv4,
                         port,
                         errno );

        SSL_free(ssl);
        SSL_CTX_free(ctx);
        return NULL;
    }

    // Attach the SSL session to the socket descriptor
    SSL_set_fd(ssl, sockfd);

    // Try to SSL-connect here, returns 1 for success
    ret = SSL_connect(ssl);
    if (ret != 1)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_connect() with ERROR:(%d)\r\n",
                         __LINE__,
                         SSL_get_error(ssl, ret));

        DXSOCKET_CLOSE(SSL_get_fd(ssl));
        SSL_free(ssl);
        SSL_CTX_free(ctx);
        return NULL;
    }

    // Get the remote certificate into the X509 structure
    X509 *c = SSL_get_peer_certificate(ssl);
    if (c == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_get_peer_certificate() == NULL\r\n",
                         __LINE__);

        SSL_shutdown(ssl);
        DXSOCKET_CLOSE(SSL_get_fd(ssl));
        SSL_free(ssl);
        SSL_CTX_free(ctx);
        return NULL;
    }

    // Verify certificate
    SSL_set_verify(ssl, verify, NULL);
    long res = SSL_get_verify_result(ssl);
    if (!(res == X509_V_OK ||
          res == X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) Unable to verify certificate(%ld), %s\r\n",
                         __LINE__,
                         res,
                         X509_verify_cert_error_string(res));

        X509_free(c);
        SSL_shutdown(ssl);
        DXSOCKET_CLOSE(SSL_get_fd(ssl));
        SSL_free(ssl);
        SSL_CTX_free(ctx);

        return NULL;
    }

#pragma message("TODO: Add codes to verify certificate !!!")

    // display cipher suite
    DXSSL_PRINTF("CipherSuite=%s, Ver=%s\n", SSL_get_cipher_name(ssl), SSL_get_cipher_version(ssl));

    // We don't need the value anymore
    X509_free(c);

    // Return DEXATEK SSL handle
    dxSSLHandle_t *pdxSSL = (dxSSLHandle_t *)dxMemAlloc("dxSSL", 1, sizeof(dxSSLHandle_t));
    if (pdxSSL == NULL)
    {
        return NULL;
    }

    pdxSSL->sfd = sockfd;
    pdxSSL->ssl = (dxSSLContext *)dxMemAlloc("dxSSL", 1, sizeof(dxSSLContext_t));
    if (pdxSSL->ssl == NULL)
    {
        dxMemFree(pdxSSL);
        pdxSSL = NULL;
        return NULL;
    }

    dxSSLContext_t *pSSL = (dxSSLContext_t *)pdxSSL->ssl;
    pSSL->ctx = ctx;
    pSSL->ssl = ssl;

    return pdxSSL;
}

int dxSSL_Receive(dxSSLHandle_t *dxSSL, void *mem, size_t len)
{
    if ( dxSSL == NULL || mem == NULL || len <= 0 ) {
        return DXSSL_ERROR;
    }

    dxSSLContext_t *pSSL = ( dxSSLContext_t * )dxSSL->ssl;
    if ( pSSL->ssl == NULL ) {
        return DXSSL_ERROR;
    }

    ERR_clear_error();

    // >0
    // The read operation was successful; the return value is the number of bytes
    // actually read from the TLS/SSL connection.
    //
    // 0
    // The read operation was not successful. The reason may either be a clean
    // shutdown due to a "close notify" alert sent by the peer (in which case the
    // SSL_RECEIVED_SHUTDOWN flag in the ssl shutdown state is set
    // (see SSL_shutdown(3), SSL_set_shutdown(3)). It is also possible, that the
    // peer simply shut down the underlying transport and the shutdown is
    // incomplete. Call SSL_get_error() with the return value ret to find out,
    // whether an error occurred or the connection was shut down cleanly
    // (SSL_ERROR_ZERO_RETURN). SSLv2 (deprecated) does not support a shutdown
    // alert protocol, so it can only be detected, whether the underlying connection
    // was closed. It cannot be checked, whether the closure was initiated by the
    // peer or by something else.
    // <0
    //
    // The read operation was not successful, because either an error occurred or
    // action must be taken by the calling process. Call SSL_get_error() with the
    // return value ret to find out the reason.
    int ret = SSL_read(pSSL->ssl, mem, len);

    if (ret <= 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_read() with ERROR:(%d)\r\n",
                         __LINE__,
                         SSL_get_error(pSSL->ssl, ret));
    }

	dbg_warm();
    return ret;
}

int dxSSL_ReceiveTimeout(dxSSLHandle_t *dxSSL, void *mem, size_t len, uint32_t msec)
{
    int ret = 0;
    fd_set              fds;
    struct timeval      to;

    if ( dxSSL == NULL || mem == NULL || len <= 0 ) {
        return DXSSL_ERROR;
    }

    dxSSLContext_t *pSSL = ( dxSSLContext_t * )dxSSL->ssl;
    if ( pSSL->ssl == NULL ) {
        return DXSSL_ERROR;
    }

    FD_ZERO( &fds );
    FD_SET( dxSSL->sfd, &fds );

    to.tv_sec   = msec / 1000;
    to.tv_usec  = msec % 1000;

    fcntl( dxSSL->sfd, F_SETFL, ( O_NONBLOCK | fcntl( dxSSL->sfd, F_GETFL, 0 ) ) );

    ret = select( ( dxSSL->sfd + 1 ), &fds, NULL, NULL, &to );
    if ( ret <= 0 ) {
	dbg_warm();
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "WARNING:(Line=%d) Timeout( %d )\r\n", __LINE__, ret );
        return DXSSL_ERROR;
    }

    ERR_clear_error();

    // >0
    // The read operation was successful; the return value is the number of bytes
    // actually read from the TLS/SSL connection.
    //
    // 0
    // The read operation was not successful. The reason may either be a clean
    // shutdown due to a "close notify" alert sent by the peer (in which case the
    // SSL_RECEIVED_SHUTDOWN flag in the ssl shutdown state is set
    // (see SSL_shutdown(3), SSL_set_shutdown(3)). It is also possible, that the
    // peer simply shut down the underlying transport and the shutdown is
    // incomplete. Call SSL_get_error() with the return value ret to find out,
    // whether an error occurred or the connection was shut down cleanly
    // (SSL_ERROR_ZERO_RETURN). SSLv2 (deprecated) does not support a shutdown
    // alert protocol, so it can only be detected, whether the underlying connection
    // was closed. It cannot be checked, whether the closure was initiated by the
    // peer or by something else.
    //
    // <0
    // The read operation was not successful, because either an error occurred or
    // action must be taken by the calling process. Call SSL_get_error() with the
    // return value ret to find out the reason.
    ret = SSL_read( pSSL->ssl, mem, len );

    if ( ret <= 0 ) {
        if ( SSL_get_error( pSSL->ssl, ret ) == SSL_ERROR_WANT_READ ) {
	        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
	                          "WARNING:(Line=%d) SSL_read() with SSL_ERROR_WANT_READ\r\n",
	                          __LINE__ );

            return 0;
        } else {
	        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
	                          "WARNING:(Line=%d) SSL_read() with ERROR:(%d)\r\n",
	                          __LINE__,
	                          SSL_get_error( pSSL->ssl, ret ) );

	        return DXSSL_ERROR;
        }
    }
dbg_warm( "%s", mem );

    return ret;
}

int dxSSL_Send(dxSSLHandle_t *dxSSL, const void *dataptr, size_t size)
{
    if (dxSSL == NULL || dataptr == NULL || size <= 0)
    {
        return DXSSL_ERROR;
    }

    dxSSLContext_t *pSSL = (dxSSLContext_t *)dxSSL->ssl;
    if (pSSL->ssl == NULL)
    {
        return DXSSL_ERROR;
    }

    ERR_clear_error();

    fcntl( dxSSL->sfd, F_SETFL, ( ~O_NONBLOCK & fcntl( dxSSL->sfd, F_GETFL, 0 ) ) );

    // >0
    // The write operation was successful, the return value is the number of bytes
    // actually written to the TLS/SSL connection.
    //
    // 0
    // The write operation was not successful. Probably the underlying connection
    // was closed. Call SSL_get_error() with the return value ret to find out,
    // whether an error occurred or the connection was shut down cleanly (
    // SSL_ERROR_ZERO_RETURN ). SSLv2 (deprecated) does not support a shutdown
    // alert protocol, so it can only be detected, whether the underlying
    // connection was closed. It cannot be checked, why the closure happened.
    //
    // <0
    // The write operation was not successful, because either an error occurred
    // or action must be taken by the calling process. Call SSL_get_error()
    // with the return value ret to find out the reason.

    int ret = SSL_write(pSSL->ssl, dataptr, size);

    if (ret <= 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SSL_read() with ERROR:(%d)\r\n",
                         __LINE__,
                         SSL_get_error(pSSL->ssl, ret));
    }
dbg_warm( "%s", dataptr );
    return ret;
}
