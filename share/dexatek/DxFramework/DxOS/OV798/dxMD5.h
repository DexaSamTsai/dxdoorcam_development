//============================================================================
// File: dxMD5.h
//============================================================================
#ifndef _DXMD5_H
#define _DXMD5_H

#pragma once

#include <openssl/md5.h>
#include "dxOS.h"

typedef MD5_CTX  dxMD5_CTX;

extern void
dxMD5( const unsigned char *input, size_t ilen, unsigned char output[ 16 ] );

extern int
dxMD5_Starts( dxMD5_CTX *ctx);

extern int
dxMD5_Update( dxMD5_CTX *ctx, const void *data, size_t len );

extern int
dxMD5_Finish( unsigned char *md, dxMD5_CTX *ctx);

extern void
dxMD5_convert_hex( unsigned char *md, unsigned char *mdstr );

#endif // _DXMD5_H
