//============================================================================
// File: dxNET.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>

#include <sys/un.h>
#include "dxWIFI.h"
#include "dxWPACtrl.h"
#include "dxNET.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

int dxSOCKET_SetTimeout(int s, uint32_t msec)
{
    int ret = 0;

    struct timeval interval = { .tv_sec = msec / 1000,
                                .tv_usec = (msec % 1000) * 1000 };

    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));

    return ret;
}

//============================================================================
// Network Interface
//============================================================================
dxNET_RET_CODE
dxNET_Info( dxNet_Addr_t *dxNetAddr )
{
	if ( dxNetAddr ) {
		struct ifreq ifr;
		int sock_fd;
		struct sockaddr_in *addr;

	    memset( dxNetAddr, 0x00, sizeof( dxNet_Addr_t ) );

		memset( &ifr, 0, sizeof( struct ifreq ) );

		sock_fd = socket( AF_INET, SOCK_STREAM, 0 );
		if ( sock_fd < 0 ) {
			dbg_warm( "sock_fd < 0" );
			return DXNET_ERROR;
		}

		strcpy( ifr.ifr_name, "wlan0" );
		if ( ioctl( sock_fd, SIOCGIFFLAGS, &ifr ) < 0 ) {
			dbg_warm( "ioctl( sock_fd, SIOCGIFFLAGS, &ifr ) < 0" );
			close( sock_fd );
			return DXNET_ERROR;
		}

		if ( ioctl( sock_fd, SIOCGIFADDR, &ifr ) == 0 ) {
			addr = ( ( struct sockaddr_in * )&ifr.ifr_addr );
			memcpy( dxNetAddr->ip_addr.v4, &addr->sin_addr, 4 );
	    	dxNetAddr->ip_addr.version = DXIP_VERSION_V4;
			dbg_warm( "IP: %d:%d:%d:%d", dxNetAddr->ip_addr.v4[ 0 ], dxNetAddr->ip_addr.v4[ 1 ], dxNetAddr->ip_addr.v4[ 2 ], dxNetAddr->ip_addr.v4[ 3 ] );
		} else {
			dbg_warm( "ioctl SIOCGIFADDR failed\n" );
		}

		if ( ioctl( sock_fd, SIOCGIFNETMASK, &ifr ) == 0 ) {
			addr = ( ( struct sockaddr_in * )&ifr.ifr_addr );
			memcpy( dxNetAddr->netmask.v4, &addr->sin_addr, 4 );
	    	dxNetAddr->netmask.version = DXIP_VERSION_V4;
			dbg_warm( "Netmask: %d:%d:%d:%d", dxNetAddr->netmask.v4[ 0 ], dxNetAddr->netmask.v4[ 1 ], dxNetAddr->netmask.v4[ 2 ], dxNetAddr->netmask.v4[ 3 ] );
		} else {
			dbg_warm( "ioctl SIOCGIFNETMASK failed\n" );
		}

		if ( ioctl( sock_fd, SIOCGIFHWADDR, &ifr ) == 0 ) {
			memcpy( dxNetAddr->mac_addr, ifr.ifr_ifru.ifru_hwaddr.sa_data, 6 );
			dbg_warm( "MAC: %02x:%02x:%02x:%02x:%02x:%02x", dxNetAddr->mac_addr[ 0 ], dxNetAddr->mac_addr[ 1 ], dxNetAddr->mac_addr[ 2 ], dxNetAddr->mac_addr[ 3 ], dxNetAddr->mac_addr[ 4 ], dxNetAddr->mac_addr[ 5 ] );
		} else {
			dbg_warm( "ioctl SIOCGIFHWADDR failed\n" );
		}

		close( sock_fd );

		return DXNET_SUCCESS;
	}

	return DXNET_ERROR;
}

//============================================================================
// DHCP Client
//============================================================================
int
dxDHCPC_Init( void )
{
	if ( access( "/usr/share/udhcpc/default.script", F_OK ) != -1 ) {
		system( "killall -9 udhcpc" );
		sleep( 1 );
		system( "/bin/udhcpc -i wlan0 -n -t 5 -T 5" ); //-t :retry 5, -T :timeout 5s, -n :failed then exit
	} else {
		printf( "Demo baord only, please check /var/conf/dhcpc.script exist!\n" );
		system( "killall -9 udhcpc" );
		sleep( 1 );
		system( "/bin/udhcpc -i wlan0 -s /var/conf/dhcpc.script" );
	}

	return 0;
}

/* perform a release */
void
dxDHCPC_Release( void )
{
	system( "killall -9 udhcpc" );
}

//============================================================================
// Multicast
//============================================================================

#define IPBIN_TO_STR(x,y)         sprintf(x, "%d.%d.%d.%d",\
                                             y.v4[0],\
                                             y.v4[1],\
                                             y.v4[2],\
                                             y.v4[3])

typedef struct
{
    int sockId;                 // Socket ID
    dxSocketAddr_t SocketAddr;  // Multicast socket
} dxMulticastHandle_t;

void dxMCAST_Init(void)
{
    // Reserve for new Features in the futrue.
dbg_warm();
    return;
}

dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port)
{
    dxMulticastHandle_t *pHandle = NULL;
    pHandle = dxMemAlloc("dxMCAST_Join", 1, sizeof(dxMulticastHandle_t));

dbg_warm();
    if (pHandle == NULL)
    {
        return NULL;
    }

    pHandle->sockId = -1;
    pHandle->SocketAddr.GroupIP.version = DXIP_VERSION_V4;
    memset(pHandle->SocketAddr.GroupIP.v4, 0x00, sizeof(pHandle->SocketAddr.GroupIP.v4));
    pHandle->SocketAddr.port = 0;

    int ret = socket(AF_INET, SOCK_DGRAM, 0);

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->sockId = ret;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, UDPGroupIp);

    // Add multicast group membership on this interface
    struct ip_mreq imr;
    memset(&imr, 0x00, sizeof(imr));
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_ADD_MEMBERSHIP, &imr, sizeof(imr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // Specify outgoing interface too
    struct in_addr intfAddr;
    memset(&intfAddr, 0x00, sizeof(intfAddr));
    intfAddr.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_MULTICAST_IF, &intfAddr, sizeof(struct in_addr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // And start listening for packets
    struct sockaddr_in bindAddr;
    memset(&bindAddr, 0x00, sizeof(bindAddr));
    bindAddr.sin_family = AF_INET;
    bindAddr.sin_port = htons(port);
    bindAddr.sin_addr.s_addr = INADDR_ANY;
    ret = bind(pHandle->sockId, (struct sockaddr *) &bindAddr, sizeof(bindAddr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->SocketAddr.GroupIP = UDPGroupIp;
    pHandle->SocketAddr.port = port;

    return (dxUDPHandle_t)pHandle;
}

dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle)
{
dbg_warm();
    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;
    int ret = DXNET_ERROR;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, pHandle->SocketAddr.GroupIP);
    // Add multicast group membership on this interface
    struct ip_mreq imr;
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = setsockopt(pHandle->sockId, IPPROTO_IP, IP_DROP_MEMBERSHIP, &imr, sizeof(imr));
    if (ret < 0)
    {
        return DXNET_ERROR;
    }

    ret = close(pHandle->sockId);

    dxMemFree(pHandle);
    pHandle = NULL;

    return (ret == 0) ? DXNET_SUCCESS : DXNET_ERROR;
}

int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs)
{
    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;

    uint32_t TimeOutRcv = (uint32_t)TimeOutInMs;

    int ret;

    if (dxSOCKET_SetTimeout(pHandle->sockId, TimeOutRcv) == -1) {
        dbg_warm( "dxMCAST_RecvFrom - setsockopt failed to config" );
    }

    //int ret = recvfrom(pHandle->sockId, Buffer, BuffSize, flags, pFromAddr, (socklen_t *)NULL);
    if (pFromSocket == NULL)
    {
        ret = recvfrom(pHandle->sockId, Buffer, BuffSize, 0, NULL, (socklen_t *)NULL);
    }
    else
    {
        char szIPv4[16] = {0};
        IPBIN_TO_STR(szIPv4, pFromSocket->GroupIP);

        struct sockaddr_in sour;
        memset(&sour, 0x00, sizeof(sour));
        sour.sin_family = AF_INET;
        sour.sin_port = htons(pFromSocket->port);
        sour.sin_addr.s_addr = inet_addr(szIPv4);
        ret = recvfrom(pHandle->sockId, Buffer, BuffSize, 0, &sour, (socklen_t *)NULL);
    }

    // Possible error code
    // > 0:
    // 0:
    // -1:
    // For more detail error code, please refer to 'err.h' located in
    // 'component\common\network\lwip\v1.4.1\src\include\lwip'

    if (ret > 0)
    {
        return ret;
    }
    else if (ret == 0)
    {
        return DXNET_CONN_CLOSED;
    }

    return DXNET_ERROR;
}

int
dxMCAST_SendTo( dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket )
{
	if ( handle == NULL || dataptr == NULL ) {
		return DXNET_ERROR;
	} else {
    	dxMulticastHandle_t *pHandle = ( dxMulticastHandle_t * )handle;
	    char szIPv4[ 16 ] = { 0, };

	    struct sockaddr_in dest;
	    memset( &dest, 0x00, sizeof( struct sockaddr_in ) );
	    dest.sin_family = AF_INET;

	    if ( pToSocket == NULL ) {
	        IPBIN_TO_STR( szIPv4, pHandle->SocketAddr.GroupIP );
	        dest.sin_port = htons( pHandle->SocketAddr.port );
	    } else {
	        IPBIN_TO_STR( szIPv4, pToSocket->GroupIP );
	        dest.sin_port = htons( pToSocket->port );
	    }

	    dest.sin_addr.s_addr = inet_addr( szIPv4 );

	    //int ret = sendto(pHandle->sockId, dataptr, dataSize, flags, ToAddr, (socklen_t)NULL);
	    int ret = sendto( pHandle->sockId, dataptr, dataSize, 0, &dest, ( socklen_t )sizeof( dest ) );

	    // Possible error code
	    // > 0:
	    // -1:
	    // ERR_VAL:
	    // ERR_ARG:
	    // ERR_MEM:
	    // For more detail error code, please refer to 'err.h' located in
	    // 'component\common\network\lwip\v1.4.1\src\include\lwip'

	    return ( ret > 0 ) ? ret : DXNET_ERROR;
	}
}
