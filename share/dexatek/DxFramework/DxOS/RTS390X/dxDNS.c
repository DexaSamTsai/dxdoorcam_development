//============================================================================
// File: dxDNS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include "dxNET.h"
#include "dxDNS.h"
#include <sys/socket.h>
#include <netdb.h>

dxNET_RET_CODE dxDNS_GetHostByName(uint8_t *hostname, dxIPv4_t *dxip)
{
    if (dxip == NULL ||
        hostname == NULL)
    {
        return DXNET_ERROR;
    }
    
    // NOTE: 
    // The following code are not thread-safe. Use getaddrinfo() instead.
    // Please also make sure thread's stack size MUST at least 21KB
    //
    //struct hostent *host = NULL;
    //
    //host = (struct hostent *)gethostbyname((const char *)hostname);
    //
    //if (host == NULL)
    //{
    //    return DXNET_ERROR;
    //}

    struct addrinfo hints;
    struct addrinfo *res = NULL;
    int status;
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;        // AF_INET or AF_INET6
    hints.ai_socktype = SOCK_STREAM;
    
    if ((status = getaddrinfo((char *)hostname, NULL, &hints, &res)) != 0) 
    {
        return DXNET_ERROR;
    }
    
    uint8_t bFound = 0;

    struct addrinfo *p = NULL;
    for(p = res;p != NULL; p = p->ai_next) 
    {
        //
        // Use the following to show IP address
        //
        // void *addr;
        // char *ipver; 
        //
        // IPv4
        // struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
        // addr = &(ipv4->sin_addr);
        // ipver = "IPv4";          
        //
        // IPv6
        // struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)p->ai_addr;
        // addr = &(ipv6->sin6_addr);
        // ipver = "IPv6";
        //
        // inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
        // printf(" %s: %s\n", ipver, ipstr);
           
        if (p->ai_family == AF_INET) 
        {
            // IPv4
            struct sockaddr_in *ipv4 = (struct sockaddr_in *)p->ai_addr;
            memcpy(&dxip->ip_addr[0], &(ipv4->sin_addr.s_addr), sizeof(dxip->ip_addr));

            bFound = 1;
        } 
        else 
        { 
            // IPv6
        }
    }

    freeaddrinfo(res); 

    return (bFound == 1) ? DXNET_SUCCESS : DXNET_ERROR;
}
