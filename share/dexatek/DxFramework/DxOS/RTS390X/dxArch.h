//============================================================================
// File: dxArch.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/04/18
//     1) Description: Initial
//============================================================================
#ifndef _DXARCH_H
#define _DXARCH_H

//============================================================================
// Hardware dependent
//
// Options (Choose One)
//============================================================================

//============================================================================
// Include files
//============================================================================

//============================================================================
// Defines
//============================================================================

//Change according to platfomrs/compiler
#define DX_PACK_STRUCT_BEGIN        struct __attribute__ ((packed))
#define DX_PACK_STRUCT_STRUCT
#define DX_PACK_STRUCT_END
#define DX_PACK_STRUCT_FIELD(x)     x
#define DX_PACK_STRUCT_USE_INCLUDES


// Change according to paltform/compiler
#define dxPTR_t                     long
// +--------+-----------+-----------+
// |        |  Windows  |   Linux   |
// |        +-----+-----+-----+-----+
// |        | x86 | x64 | x86 | x64 |
// +--------+-----+-----+-----+-----+
// | int    |   4 |   4 |   4 |   4 |
// | long   |   4 |   4 |   4 |   8 |
// | size_t |   4 |   8 |   4 |   8 |
// | time_t |   4 |   8 |   4 |   8 |
// +--------+-----+-----+-----+-----+
// The data type is use to convert between pointer and numeric data type.
// So, the size of dxPTR_t must match MCU's bus width.
// For example, in 32-bit MCU, dxPTR_t can be "int" or "long".
//              in 64-bit MCU, dxPTR_t must be "long" (in Linux)
//              in 64-bit MCU, dxPTR_t must be "int64" (in Windows 64-bit OS)

// Change according to different platform/compiler.
// Use 'TASK_RET_TYPE' and 'TASK_PARAM_TYPE' as thread function's return and parameter data type.
#define TASK_RET_TYPE               void*
#define TASK_PARAM_TYPE             void*
#define TASK_RET_GENERIC            NULL    //For Task generic return

// Calling 'END_OF_TASK_FUNCTION' before end of thread
#define END_OF_TASK_FUNCTION        pthread_exit(NULL)

#ifndef TRUE
#	define		TRUE				1
#endif

#ifndef FALSE
#	define		FALSE				0
#endif

#endif // _DXARCH_H
