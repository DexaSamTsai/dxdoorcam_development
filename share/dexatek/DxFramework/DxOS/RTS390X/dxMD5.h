//============================================================================
// File: dxMD5.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#ifndef _DXMD5_H
#define _DXMD5_H

#pragma once

#include <openssl/md5.h>
#include "dxOS.h"

typedef MD5_CTX  dxMD5_CTX;

void dxMD5(const unsigned char *input, size_t ilen, unsigned char output[16]);

int dxMD5_Starts( dxMD5_CTX *ctx);
int dxMD5_Update( dxMD5_CTX *ctx, const void *data, size_t len);
int dxMD5_Finish( unsigned char *md, dxMD5_CTX *ctx);

void dxMD5_convert_hex(unsigned char *md, unsigned char *mdstr);

#endif // _DXMD5_H
