//============================================================================
// File: dxWIFI.h
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DXWIFI_H
#define _DXWIFI_H

#pragma once

//============================================================================
// Common include files
//
// Note: Need to modify while porting to others
//============================================================================
#include "dxOS.h"
#include "dxNET.h"
#include "dk_Network.h"

//============================================================================
// WiFi Management
//============================================================================
typedef enum {
    DXWIFI_SUCCESS                         = 0,    //< Success
    DXWIFI_PENDING                         = 1,    //< Pending
    DXWIFI_TIMEOUT                         = 2,    //< Timeout
    DXWIFI_PARTIAL_RESULTS                 = 3,    //< Partial results
    DXWIFI_INVALID_KEY                     = 4,    //< Invalid key
    DXWIFI_DOES_NOT_EXIST                  = 5,    //< Does not exist
    DXWIFI_NOT_AUTHENTICATED               = 6,    //< Not authenticated
    DXWIFI_NOT_KEYED                       = 7,    //< Not keyed
    DXWIFI_IOCTL_FAIL                      = 8,    //< IOCTL fail
    DXWIFI_BUFFER_UNAVAILABLE_TEMPORARY    = 9,    //< Buffer unavailable temporarily
    DXWIFI_BUFFER_UNAVAILABLE_PERMANENT    = 10,   //< Buffer unavailable permanently
    DXWIFI_WPS_PBC_OVERLAP                 = 11,   //< WPS PBC overlap
    DXWIFI_CONNECTION_LOST                 = 12,   //< Connection lost

    DXWIFI_ERROR                           = -1,   //< Generic Error
    DXWIFI_BADARG                          = -2,   //< Bad Argument
    DXWIFI_BADOPTION                       = -3,   //< Bad option
    DXWIFI_NOTUP                           = -4,   //< Not up
    DXWIFI_NOTDOWN                         = -5,   //< Not down
    DXWIFI_NOTAP                           = -6,   //< Not AP
    DXWIFI_NOTSTA                          = -7,   //< Not STA
    DXWIFI_BADKEYIDX                       = -8,   //< BAD Key Index
    DXWIFI_RADIOOFF                        = -9,   //< Radio Off
    DXWIFI_NOTBANDLOCKED                   = -10,  //< Not  band locked
    DXWIFI_NOCLK                           = -11,  //< No Clock
    DXWIFI_BADRATESET                      = -12,  //< BAD Rate valueset
    DXWIFI_BADBAND                         = -13,  //< BAD Band
    DXWIFI_BUFTOOSHORT                     = -14,  //< Buffer too short
    DXWIFI_BUFTOOLONG                      = -15,  //< Buffer too long
    DXWIFI_BUSY                            = -16,  //< Busy
    DXWIFI_NOTASSOCIATED                   = -17,  //< Not Associated
    DXWIFI_BADSSIDLEN                      = -18,  //< Bad SSID len
    DXWIFI_OUTOFRANGECHAN                  = -19,  //< Out of Range Channel
    DXWIFI_BADCHAN                         = -20,  //< Bad Channel
    DXWIFI_BADADDR                         = -21,  //< Bad Address
    DXWIFI_NORESOURCE                      = -22,  //< Not Enough Resources
    DXWIFI_UNSUPPORTED                     = -23,  //< Unsupported
    DXWIFI_BADLEN                          = -24,  //< Bad length
    DXWIFI_NOTREADY                        = -25,  //< Not Ready
    DXWIFI_EPERM                           = -26,  //< Not Permitted
    DXWIFI_NOMEM                           = -27,  //< No Memory
    DXWIFI_ASSOCIATED                      = -28,  //< Associated
    DXWIFI_RANGE                           = -29,  //< Not In Range
    DXWIFI_NOTFOUND                        = -30,  //< Not Found
    DXWIFI_WME_NOT_ENABLED                 = -31,  //< WME Not Enabled
    DXWIFI_TSPEC_NOTFOUND                  = -32,  //< TSPEC Not Found
    DXWIFI_ACM_NOTSUPPORTED                = -33,  //< ACM Not Supported
    DXWIFI_NOT_WME_ASSOCIATION             = -34,  //< Not WME Association
    DXWIFI_SDIO_ERROR                      = -35,  //< SDIO Bus Error
    DXWIFI_WLAN_DOWN                       = -36,  //< WLAN Not Accessible
    DXWIFI_BAD_VERSION                     = -37,  //< Incorrect version
    DXWIFI_TXFAIL                          = -38,  //< TX failure
    DXWIFI_RXFAIL                          = -39,  //< RX failure
    DXWIFI_NODEVICE                        = -40,  //< Device not present
    DXWIFI_UNFINISHED                      = -41,  //< To be finished
    DXWIFI_NONRESIDENT                     = -42,  //< access to nonresident overlay
    DXWIFI_DISABLED                        = -43   //< Disabled in this build

} dxWIFI_RET_CODE;


enum {

	DX_WEP_ENABLED 	= (1<<0),
	DX_SHARED_ENABLED	= (1<<1),
	DX_WPA_SECURITY	= (1<<2),
	DX_TKIP_ENABLED	= (1<<3),
	DX_AES_ENABLED	    = (1<<4),
	DX_WPA2_SECURITY	= (1<<5),
	DX_WPS_ENABLED	    = (1<<6),
};

typedef enum {
    DXWIFI_SECURITY_OPEN           = 0,                                                      //< Open security
    DXWIFI_SECURITY_WEP_PSK        = DX_WEP_ENABLED,                                         //< WEP Security with open authentication
    DXWIFI_SECURITY_WEP_SHARED     = ( DX_WEP_ENABLED | DX_SHARED_ENABLED ),                 //< WEP Security with shared authentication
    DXWIFI_SECURITY_WPA_TKIP_PSK   = ( DX_WPA_SECURITY  | DX_TKIP_ENABLED ),                 //< WPA Security with TKIP
    DXWIFI_SECURITY_WPA_AES_PSK    = ( DX_WPA_SECURITY  | DX_AES_ENABLED ),                  //< WPA Security with AES
    DXWIFI_SECURITY_WPA2_AES_PSK   = ( DX_WPA2_SECURITY | DX_AES_ENABLED ),                  //< WPA2 Security with AES
    DXWIFI_SECURITY_WPA2_TKIP_PSK  = ( DX_WPA2_SECURITY | DX_TKIP_ENABLED ),                 //< WPA2 Security with TKIP
    DXWIFI_SECURITY_WPA2_MIXED_PSK = ( DX_WPA2_SECURITY | DX_AES_ENABLED | DX_TKIP_ENABLED ),//< WPA2 Security with AES & TKIP
    DXWIFI_SECURITY_WPA_WPA2_MIXED = ( DX_WPA_SECURITY  | DX_WPA2_SECURITY ),                //< WPA/WPA2 Security

    DXWIFI_SECURITY_WPS_OPEN       = DX_WPS_ENABLED,                                         //< WPS with open security
    DXWIFI_SECURITY_WPS_SECURE     = (DX_WPS_ENABLED | DX_AES_ENABLED),                      //< WPS with AES security

    DXWIFI_SECURITY_UNKNOWN        = -1,                                               //< May be returned by scan function if security is unknown.
                                                                                       //  Do not pass this to the join function!

    DXWIFI_SECURITY_FORCE_32_BIT   = 0x7fffffff                                        //< Exists only to force dxWiFi_Security_t type to 32 bits
} dxWiFi_Security_t;


typedef enum {
    DXWIFI_MODE_NONE = 0,
    DXWIFI_MODE_STA,
    DXWIFI_MODE_AP
} dxWiFi_Mode_t;

typedef enum {
    DXWIFI_SCAN_TYPE_ACTIVE = 0
} dxWiFi_Scan_Type_t;

typedef enum {
    DXWIFI_BSS_TYPE_INFRASTRUCTURE  = 0,        // Denotes infrastructure network
    DXWIFI_BSS_TYPE_ADHOC           = 1,        // Denotes an 802.11 ad-hoc IBSS network
    DXWIFI_BSS_TYPE_ANY             = 2,        // Denotes either infrastructure or ad-hoc network
    DXWIFI_BSS_TYPE_UNKNOWN         = -1        // May be returned by scan function if BSS type is unknown.
                                                // Do not pass this to the Join function
} dxWiFi_BSS_Type_t;

typedef struct dxWiFi_SSID {
    unsigned char len;              //< SSID length
    unsigned char val[64];          //< SSID name (AP name)
} dxWiFi_SSID_t;

typedef struct dxWiFi_MAC {
    unsigned char octet[6];         //< Unique 6-byte MAC address
} dxWiFi_MAC_t;

typedef enum {
    DXWIFI_WPS_TYPE_DEFAULT                 = 0x0000,
    DXWIFI_WPS_TYPE_USER_SPECIFIED          = 0x0001,
    DXWIFI_WPS_TYPE_MACHINE_SPECIFIED       = 0x0002,
    DXWIFI_WPS_TYPE_REKEY                   = 0x0003,
    DXWIFI_WPS_TYPE_PUSHBUTTON              = 0x0004,
    DXWIFI_WPS_TYPE_REGISTRAR_SPECIFIED     = 0x0005,
    DXWIFI_WPS_TYPE_NONE                    = 0x0006
} dxWiFi_WPS_Type_t;

typedef enum {
    DXWIFI_802_11_BAND_5GHZ   = 0,              //< Denotes 5GHz radio band
    DXWIFI_802_11_BAND_2_4GHZ = 1               //< Denotes 2.4GHz radio band
} dxWiFi_802_11_Band_t;

// bss_type and band value maybe wrong due to Realtek SDK
typedef struct dxWiFi_Scan_Details {
    dxWiFi_SSID_t           SSID;               //< Service Set Identification (i.e. Name of Access Point)
    dxWiFi_MAC_t            BSSID;              //< Basic Service Set Identification (i.e. MAC address of Access Point)
    int16_t                 signal_strength;    //< Receive Signal Strength Indication in dBm. <-90=Very poor, >-30=Excellent
    dxWiFi_BSS_Type_t       bss_type;           //< Network type
    dxWiFi_Security_t       security;           //< Security type
    dxWiFi_WPS_Type_t       wps_type;           //< WPS type
    uint32_t                channel;            //< Radio channel that the AP beacon was received on
    dxWiFi_802_11_Band_t    band;               //< Radio band
} dxWiFi_Scan_Details_t;

typedef struct dxWiFi_Scan_Result {
    dxWiFi_Scan_Details_t   ap_details;
    dxBOOL                  scan_complete;
    void*                   user_data;
} dxWiFi_Scan_Result_t;

typedef enum {
    DXWIFI_SCAN_STATE_NONE      = 0,
    DXWIFI_SCAN_STATE_SCANNING  = 1,
    DXWIFI_SCAN_STATE_DONE      = 2
} dxWiFi_Scan_State_t;

typedef enum {
    DXWIFI_ADAPTIVITY_DISABLE = 0,
    DXWIFI_ADAPTIVITY_NORMAL,           // CE
    DXWIFI_ADAPTIVITY_CARRIER_SENSE     // MKK
} dxWiFi_Adaptivity_t;

typedef enum {
    DXWIFI_CHANNEL_PLAN_DEFAULT         = 0,
    DXWIFI_CHANNEL_PLAN_KOREA           = 1,
    DXWIFI_CHANNEL_PLAN_EU              = 2,
    DXWIFI_CHANNEL_PLAN_NORTH_AMERICA   = 3,
    DXWIFI_CHANNEL_PLAN_JAPAN           = 4,
} dxWiFi_Channel_Plan_t;

typedef enum {
    RTW_SUCCESS  = 0,
    RTW_FAIL     = 1,
} rtw_result_t;

typedef dxWIFI_RET_CODE (*dxWiFi_Scan_Result_Handler_t)(dxWiFi_Scan_Result_t* malloced_scan_result);


void dxNetIf_Init(void);

// dxsemaphore MUST be NULL
dxWIFI_RET_CODE dxWiFi_Connect( char *ssid,
                                dxWiFi_Security_t security_type,
                                char *password,
                                int key_id,
                                dxSemaphoreHandle_t *dxsemaphore);

/**
 * @brief To get correct MAC address information, dxWiFi_On()
 *        must be called first.
 *
 * @return DXWIFI_SUCCESS   if MAC address returned
 *         DXWIFI_ERROR     otherwise
 */
dxWIFI_RET_CODE dxWiFi_Disconnect(void);

dxWIFI_RET_CODE dxWiFi_IsOnline(dxWiFi_Mode_t mode);

dxWIFI_RET_CODE dxWiFi_Off(void);

dxWIFI_RET_CODE dxWiFi_On(dxWiFi_Mode_t xMode);

dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv);

dxWIFI_RET_CODE dxWiFi_Scan(dxWiFi_Scan_Result_Handler_t results_handler, void* user_data);

dxWiFi_Scan_State_t dxWiFi_GetScanState(void);

dxWIFI_RET_CODE dxWiFi_WaitForOnline(dxWiFi_Mode_t xMode, uint32_t xMSToWait);

dxWIFI_RET_CODE
dxWiFi_GetRSSI( int *rssi );

#endif  // _DXWIFI_H
