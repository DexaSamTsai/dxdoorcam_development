//============================================================================
// File: dxOS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <limits.h>

#include "dxOS.h"
#include "dxWIFI.h"
#include "tiny-AES128.h"
#include "Utils.h"



//============================================================================
// Defines and Macro
//============================================================================

#define CONFIG_USE_DEXATEK_WIFI_SCAN                0   // 0 to use RTK provided wifi_scan(), 1 to use DEXATEK
#define DX_MAX_SCAN_AP_NUMBER                       64  // The value is the same as max_ap_size that defines in wifi_conf.c
#define PO10_LIMIT (ULONG_MAX / 10)



//============================================================================
// Time Util
//============================================================================

/* Macros for converting between `struct timeval' and `struct timespec'.  */
# define DX_TIMEVAL_TO_TIMESPEC(tv, ts) {                                   \
	(ts)->tv_sec = (tv)->tv_sec;                                    \
	(ts)->tv_nsec = (tv)->tv_usec * 1000;                           \
}
# define DX_TIMESPEC_TO_TIMEVAL(tv, ts) {                                   \
	(tv)->tv_sec = (ts)->tv_sec;                                    \
	(tv)->tv_usec = (ts)->tv_nsec / 1000;                           \
}

void timespec_diff(struct timespec *start, struct timespec *stop,
                   struct timespec *result)
{
    if ((stop->tv_nsec - start->tv_nsec) < 0) {
        result->tv_sec = stop->tv_sec - start->tv_sec - 1;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
    } else {
        result->tv_sec = stop->tv_sec - start->tv_sec;
        result->tv_nsec = stop->tv_nsec - start->tv_nsec;
    }

    return;
}

struct timespec IntUtilAddOffsetToCurrentTime(uint32_t OffsetMs)
{
	struct timespec CurrentTime, OffsetTime, WaitToTime;
	struct timeval CurrentTimeval, OffsetTimeval, WaitToTimeval;
	OffsetTime.tv_sec = OffsetMs/1000;
	OffsetTime.tv_nsec = (OffsetMs%1000) * 1000000;
	clock_gettime(CLOCK_REALTIME, &CurrentTime);

	DX_TIMESPEC_TO_TIMEVAL(&CurrentTimeval, &CurrentTime);
	DX_TIMESPEC_TO_TIMEVAL(&OffsetTimeval, &OffsetTime);
	timeradd(&OffsetTimeval, &CurrentTimeval, &WaitToTimeval);

	DX_TIMEVAL_TO_TIMESPEC(&WaitToTimeval, &WaitToTime);

	return WaitToTime;
}


//============================================================================
// Main Initialization
//============================================================================


dxMutexHandle_t  dxRtc_MutexHandle = NULL;
dxMutexHandle_t  dxSPI_Resource_MutexHandle = NULL;
dxMutexHandle_t  dxSem_Resource_MutexHandle = NULL;


dxOS_RET_CODE dxOS_Init(void)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    if (dxRtc_MutexHandle == NULL)
    {
        // Due to there is only one hardware encoder/decoder, there might be overwriting
        // circule buffer when multiple thread use crypto engine at the same time.
        // Use mutex to protect
    	dxRtc_MutexHandle = dxMutexCreate();
    }

    if (dxSem_Resource_MutexHandle == NULL)
    {
        // This mutex is used to protect semaphores that support maximum count
        // to make sure semaphore gives does not increase the values beyond the given max count
    	dxSem_Resource_MutexHandle = dxMutexCreate();
    }

    if (dxSPI_Resource_MutexHandle == NULL)
    {
        // See dxSPIaccess_ResourceTake header description for more detail why is this needed!
    	dxSPI_Resource_MutexHandle = dxMutexCreate();

    }

    if ((dxRtc_MutexHandle) && (dxSPI_Resource_MutexHandle) && (dxSem_Resource_MutexHandle))
    {
        return DXOS_SUCCESS;
    }

    return ret;
}


//============================================================================
// Memory Management
//============================================================================

#define DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT        32  //Byte alignment
#define DX_MEM_LEAK_LOG                         0

dxOS_RET_CODE dxMemFree(void *pv)
{
    free(pv);

#if DX_MEM_LEAK_LOG
    G_SYS_DBG_LOG_V( "%s %x\r\n", __FUNCTION__, pv);
#endif

    return DXOS_SUCCESS;
}

size_t dxMemGetFreeHeapSize(void)
{
	//TODO: Need revise if below method gives inacurate result
	size_t PhysMemPageAvailable = (size_t)sysconf( _SC_AVPHYS_PAGES);
	size_t PageSize = (size_t)sysconf( _SC_PAGESIZE);

	return PhysMemPageAvailable*PageSize;

}

void *dxMemAlloc(char* name, size_t nelems, size_t size)
{

    void* allocmem = NULL;
    uint32_t AlignSize = size;
    uint32_t LeftOver = (AlignSize%DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT);
    if (LeftOver)
    {
        AlignSize = ((AlignSize/DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT) + 1)*DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT;
    }

    //G_SYS_DBG_LOG_V1( "dxMemAlloc pack aligned size %d\r\n", AlignSize );

    allocmem = calloc( nelems, AlignSize);

    return allocmem;
}

void *dxMemReAlloc(char* name, void *ptr, size_t size)
{
    uint32_t AlignSize = size;

#if 1 //DEFAULT TO USE PACK ALIGNMENT
    uint32_t LeftOver = (AlignSize%DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT);
    if (LeftOver)
    {
        AlignSize = ((AlignSize/DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT) + 1)*DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT;
    }
#endif

    return realloc(ptr, AlignSize);
}


//============================================================================
// Timer Management
//============================================================================

typedef struct
{
	timer_t 					timerid;
	uint32_t 					TimerIntervalInMs;
	dxBOOL 						AutoReload;
	void* 						pvArg;
	dxTimerCallbackFunction_t 	pxCallbackFunction;
} Timer_info_t;


void IntTimerCallback(union sigval sv)
{
	if (sv.sival_ptr)
	{
		Timer_info_t* TimerInfoHandler = (Timer_info_t*)sv.sival_ptr;

		if (TimerInfoHandler->pxCallbackFunction)
		{
			TimerInfoHandler->pxCallbackFunction(TimerInfoHandler->pvArg);
		}
	}

}


dxTimerHandle_t dxTimerCreate(const char * pcTimerName,
                              const uint32_t dxTimerPeriodInMs,
                              const dxBOOL uxAutoReload,
                              const void * pvArg,
                              dxTimerCallbackFunction_t pxCallbackFunction)
{
    dxTimerHandle_t dxTimerHandler = NULL;
    struct sigevent sev;


    Timer_info_t* TimerInfoHandler = (Timer_info_t*)dxMemAlloc("TIMER", 1 ,sizeof(Timer_info_t));
    if (TimerInfoHandler)
    {
    	TimerInfoHandler->pvArg = (void*)pvArg;
    	TimerInfoHandler->pxCallbackFunction = pxCallbackFunction;
    	TimerInfoHandler->TimerIntervalInMs = dxTimerPeriodInMs;
    	TimerInfoHandler->AutoReload = uxAutoReload;

    	pthread_attr_t 	attr;
    	dxBOOL			bUseMinimumStackSize = dxTRUE;
    	if (pthread_attr_init(&attr) != 0)
    	{
    		perror("dxTimerCreate pthread_attr_init failed, will use default");
    		bUseMinimumStackSize = dxFALSE;
    	}
    	else if (pthread_attr_setstacksize(&attr , PTHREAD_STACK_MIN) != 0)
    	{
    		perror("dxTimerCreate pthread_attr_setstacksize failed, will use default");
    		bUseMinimumStackSize = dxFALSE;
    	}

        sev.sigev_notify = SIGEV_THREAD;
        sev.sigev_notify_function = IntTimerCallback;
        sev.sigev_value.sival_ptr = TimerInfoHandler;
        sev.sigev_notify_attributes = (bUseMinimumStackSize == dxTRUE) ? &attr : NULL;

        if (timer_create(CLOCK_REALTIME, &sev, &TimerInfoHandler->timerid) == -1)
        {
        	perror("dxTimerCreate timer_create failed");
        }
        else
        {
        	dxTimerHandler = TimerInfoHandler;
        }

        if (bUseMinimumStackSize)
        	pthread_attr_destroy(&attr); //There is no reason why this will fail, even so its not critical.

    }

    return dxTimerHandler;
}



void* dxTimerGetTimerArg( dxTimerHandle_t dxTimer )
{
	Timer_info_t* xTimer = (Timer_info_t*)dxTimer;

    return xTimer->pvArg;
}

dxOS_RET_CODE dxTimerStart(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;
	(void)dxMsToWait;

	if (dxTimer)
	{
		Timer_info_t* xTimer = (Timer_info_t*)dxTimer;
		struct itimerspec       its;

		memset(&its, 0, sizeof(its));

    	its.it_value.tv_sec = xTimer->TimerIntervalInMs/1000;
    	its.it_value.tv_nsec = (xTimer->TimerIntervalInMs%1000)*1000000;

    	if (xTimer->AutoReload)
    	{
    		its.it_interval.tv_sec = its.it_value.tv_sec;
    		its.it_interval.tv_nsec = its.it_value.tv_nsec;
    	}

    	if (timer_settime(xTimer->timerid, 0, &its, NULL) != 0)
    	{
    		perror("dxTimerStart timer_settime failed");
    	}
    	else
    	{
    		RetCode = DXOS_SUCCESS;
    	}
	}

    return RetCode;
}


dxOS_RET_CODE dxTimerStop(dxTimerHandle_t dxTimer,
                          const uint32_t dxMsToWait)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxTimer)
	{
		Timer_info_t* xTimer = (Timer_info_t*)dxTimer;
		struct itimerspec       its;

		//Set to all zero to disarm timer
		memset(&its, 0, sizeof(its));

    	if (timer_settime(xTimer->timerid, 0, &its, NULL) != 0)
    	{
    		perror("dxTimerStop timer_settime failed");
    	}
    	else
    	{
    		RetCode = DXOS_SUCCESS;
    	}
	}

    return RetCode;
}

dxOS_RET_CODE dxTimerDelete(dxTimerHandle_t dxTimer,
                         const uint32_t dxMsToWait)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxTimer)
	{
		Timer_info_t* xTimer = (Timer_info_t*)dxTimer;

    	if (timer_delete(xTimer->timerid) != 0)
    	{
    		perror("dxTimerDelete timer_delete failed");
    	}
    	else
    	{
    		dxMemFree(dxTimer);
    		dxTimer = NULL;
    		RetCode = DXOS_SUCCESS;
    	}
	}

    return RetCode;
}

dxBOOL dxTimerIsTimerActive(dxTimerHandle_t dxTimer)
{
	dxBOOL bTimerIsActive = dxFALSE;

	if (dxTimer)
	{
		Timer_info_t* xTimer = (Timer_info_t*)dxTimer;
		struct itimerspec       its;

		memset(&its, 0, sizeof(its));

    	if (timer_gettime(xTimer->timerid, &its) != 0)
    	{
    		perror("dxTimerIsTimerActive timer_gettime failed");
    	}
    	else
    	{
    		if (its.it_value.tv_sec || its.it_value.tv_nsec)
    		{
    			bTimerIsActive = dxTRUE;
    		}
    	}
	}

    return bTimerIsActive;
}

dxOS_RET_CODE dxTimerReset(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
	return dxTimerStart(dxTimer, dxMsToWait);
}

dxOS_RET_CODE dxTimerRestart(dxTimerHandle_t dxTimer,
                             const uint32_t dxTimerPeriodInMs,
                             const uint32_t dxMsToWait)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxTimer)
	{
		Timer_info_t* xTimer = (Timer_info_t*)dxTimer;

		xTimer->TimerIntervalInMs = dxTimerPeriodInMs;

		RetCode = dxTimerStart(dxTimer, dxMsToWait);
	}

    return RetCode;
}


//============================================================================
// Time Information
//============================================================================
# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif


dxOS_RET_CODE dxTimeDelayMS(const uint32_t xMSToDelay)
{

    struct timespec sleepTime;

    sleepTime.tv_sec = xMSToDelay/1000;
    sleepTime.tv_nsec = (xMSToDelay%1000)*1000000;
    nanosleep(&sleepTime,NULL);

    return DXOS_SUCCESS;
}


dxTime_t dxTimeGetMS(void)
{
    static struct timespec SystemStartTime = {.tv_sec = 0, .tv_nsec = 0};
    clockid_t clk_id = CLOCK_MONOTONIC;

	struct timespec CurrentTime;
    struct timespec DiffCurrentTime;
	dxTime_t        CurrentTimeInMS;

    if (SystemStartTime.tv_sec == 0)
    {
        clock_gettime(clk_id, &SystemStartTime);
    }

	clock_gettime(clk_id, &CurrentTime);

    timespec_diff(&SystemStartTime, &CurrentTime, &DiffCurrentTime);

    CurrentTimeInMS = (DiffCurrentTime.tv_sec * 1000) + (DiffCurrentTime.tv_nsec/1000000);

	return (CurrentTimeInMS == 0) ? 1:CurrentTimeInMS;
}

static DKTime_t SwRtcTime;
static dxTime_t SwRtcTimeSincesystemTime = 0;

dxOS_RET_CODE dxHwRtcGet( DKTime_t* TimeRTC )
{
    dxOS_RET_CODE result = DXOS_ERROR;

    if ( TimeRTC ) {
		int fd = open( "/dev/rtc0", O_RDONLY );

		if ( fd > 0 ) {
			struct rtc_time curr_time;

        	if ( ioctl( fd, RTC_RD_TIME, &curr_time ) == 0 ) {
				if ( curr_time.tm_year > 110 ) {        // Must After 2010
					TimeRTC->year		= curr_time.tm_year + 1900;
					TimeRTC->month		= curr_time.tm_mon + 1;
					TimeRTC->day		= curr_time.tm_mday;
					TimeRTC->hour		= curr_time.tm_hour;
					TimeRTC->min		= curr_time.tm_min;
					TimeRTC->sec		= curr_time.tm_sec;
					TimeRTC->dayofweek	= ( curr_time.tm_wday == 0 )? 7: curr_time.tm_wday;

		        	result = DXOS_SUCCESS;
				}
			} else {
				dbg_warm( "Do ioctl( fd, RTC_RD_TIME, &curr_time ) Failed!!!" );
			}

			close( fd );
		}
    }

    return result;
}

dxOS_RET_CODE dxHwRtcSet( DKTime_t SetTimeRTC )
{
    dxOS_RET_CODE result = DXOS_ERROR;

	int fd = open( "/dev/rtc0", O_RDONLY );

	if ( fd > 0 ) {
		struct rtc_time new_time = {
										.tm_wday	= ( ( SetTimeRTC.dayofweek == 7 )? 0: SetTimeRTC.dayofweek ),
										.tm_year	= SetTimeRTC.year - 1900,
										.tm_mon		= SetTimeRTC.month - 1,
										.tm_mday	= SetTimeRTC.day,
										.tm_hour	= SetTimeRTC.hour,
										.tm_min		= SetTimeRTC.min,
										.tm_sec		= SetTimeRTC.sec,
									};

    	if ( ioctl( fd, RTC_SET_TIME, &new_time ) == 0 ) {
    		result = DXOS_SUCCESS;
		} else {
			dbg_warm( "Do ioctl( fd, RTC_SET_TIME, &new_time ) Failed!!!" );
		}

		close( fd );
	}

    return result;
}

dxOS_RET_CODE dxSwRtcGet(DKTime_t* TimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    if (SwRtcTimeSincesystemTime && TimeRTC)
    {
		struct timeval	GetTimeVal	= { .tv_sec = 0, .tv_usec = 0 };
		struct tm		curr_time	= { 0, };

		gettimeofday( &GetTimeVal, NULL );

		localtime_r( &GetTimeVal.tv_sec, &curr_time );

		TimeRTC->year		= curr_time.tm_year + 1900;
		TimeRTC->month		= curr_time.tm_mon + 1;
		TimeRTC->day		= curr_time.tm_mday;
		TimeRTC->hour		= curr_time.tm_hour;
		TimeRTC->min		= curr_time.tm_min;
		TimeRTC->sec		= curr_time.tm_sec;
		TimeRTC->dayofweek	= ( curr_time.tm_wday == 0 )? 7: curr_time.tm_wday;
		TimeRTC->TimeZone	= SwRtcTime.TimeZone;

        result = DXOS_SUCCESS;
    }

    return result;
}

dxOS_RET_CODE dxSwRtcSet(DKTime_t SetTimeRTC)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;

	dxMutexTake( dxRtc_MutexHandle, DXOS_WAIT_FOREVER );
	SwRtcTime					= SetTimeRTC;
	SwRtcTimeSincesystemTime	= dxTimeGetMS();
	dxMutexGive( dxRtc_MutexHandle );

	//Set the settimeofval additionally in case other process will need to use it.
	struct timeval SetTimeVal = { .tv_sec = 0, .tv_usec = 0 };
	dxTime_t TimeUTC = ConvertDKTimeToUTC( SetTimeRTC );

	SetTimeVal.tv_sec = TimeUTC;

	settimeofday( &SetTimeVal, NULL );

    return result;
}

dxOS_RET_CODE dxGetLocalTime (
    DKTimeLocal_t* localTime
) {
    dxOS_RET_CODE result = DXOS_ERROR;

    if (SwRtcTimeSincesystemTime && localTime)
    {
        struct timeval  GetTimeVal  = { .tv_sec = 0, .tv_usec = 0 };
        struct tm       curr_time   = { 0, };
        int offset                  = SwRtcTime.TimeZone * 15 * 60;

        gettimeofday( &GetTimeVal, NULL );

        GetTimeVal.tv_sec += offset;
        localtime_r( &GetTimeVal.tv_sec, &curr_time );

        localTime->year      = curr_time.tm_year + 1900;
        localTime->month     = curr_time.tm_mon + 1;
        localTime->day       = curr_time.tm_mday;
        localTime->hour      = curr_time.tm_hour;
        localTime->min       = curr_time.tm_min;
        localTime->sec       = curr_time.tm_sec;
        localTime->dayofweek = ( curr_time.tm_wday == 0 )? 7: curr_time.tm_wday;

        result = DXOS_SUCCESS;
    }

    return result;
}


DKTime_t dxGetUtcTime (void) {
    DKTime_t TimeRTC;

    memset (&TimeRTC, 0, sizeof (DKTime_t));
    dxOS_RET_CODE ret = dxSwRtcGet (&TimeRTC);
    if (ret != DXOS_SUCCESS) {
        TimeRTC = ConvertUTCToDKTime (dxTimeGetMS () / SECONDS);
    }

    return TimeRTC;
}


uint32_t dxGetUtcInSec (void) {
    DKTime_t TimeRTC;
    uint32_t RTCinSec;

    TimeRTC = dxGetUtcTime ();
    RTCinSec = ConvertDKTimeToUTC (TimeRTC);

    return RTCinSec;
}



//============================================================================
// Queue Management
//============================================================================

#define MQ_NAME_STR_MAX_SIZE				128
#define MQ_NAME_NON_CROSS_PROCESS_PREFIX	"dxMQ"
typedef struct
{
	char			mqName[MQ_NAME_STR_MAX_SIZE];
	mqd_t   		mqID;
	struct mq_attr	mqOrgAttr;
} mq_info_t;

dxQueueHandle_t dxQueueCreate(uint32_t uxQueueLength,
                              uint32_t uxItemSize)
{
	dxQueueHandle_t dxQueueHandler = NULL;

	mq_info_t* pmq_info = dxMemAlloc("pmq_info", 1, sizeof(mq_info_t));

	if (pmq_info)
	{
	    struct mq_attr attr;

	    pmq_info->mqID = -1;

	    /* initialize the queue attributes */
	    attr.mq_flags = 0;
	    attr.mq_maxmsg =  uxQueueLength;
	    attr.mq_msgsize = uxItemSize;
	    attr.mq_curmsgs = 0;

	    memcpy(&pmq_info->mqOrgAttr, &attr, sizeof(struct mq_attr));

	    /* create the message queue */
        struct timespec CurrentTime;
        clock_gettime(CLOCK_REALTIME, &CurrentTime);

	    memset(pmq_info->mqName, 0 , MQ_NAME_STR_MAX_SIZE);
	    if (sprintf(pmq_info->mqName, "/%s-%ld-%ld", MQ_NAME_NON_CROSS_PROCESS_PREFIX, CurrentTime.tv_sec, CurrentTime.tv_nsec) > 0)
	    {
	    	//printf("dxQueueCreate name = %s\n", pmq_info->mqName);
	    	pmq_info->mqID = mq_open(pmq_info->mqName, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG | S_IRWXO, &attr);
	    }

	    if (pmq_info->mqID >= 0)
	    {
	    	dxQueueHandler = pmq_info;
	    	//NOTE: Here we unlink it so that the name will not be available but anyone (within the process) who obtain the handler can still send receive message
	    	mq_unlink(pmq_info->mqName);
	    }
	    else
	    {
	    	printf("dxQueueCreate (%s) Err=%s\n", pmq_info->mqName, strerror(errno));
	    	dxMemFree(pmq_info);
	    }
	}

    return dxQueueHandler;
}

dxQueueHandle_t dxQueueGlobalCreate(void*    GlobalIdentifier,
                                    uint32_t uxQueueLength,
                                    uint32_t uxItemSize)
{
	dxQueueHandle_t dxQueueHandler = NULL;
    char* MqPipeName = (char*)GlobalIdentifier;

    if (MqPipeName == NULL)
        goto MqGCreateExit;

    if (strlen(MqPipeName) >= MQ_NAME_STR_MAX_SIZE)
        goto MqGCreateExit;

	mq_info_t* pmq_info = dxMemAlloc("pmq_info", 1, sizeof(mq_info_t));

	if (pmq_info)
	{
	    struct mq_attr attr;

	    pmq_info->mqID = -1;

	    /* initialize the queue attributes */
	    attr.mq_flags = 0;
	    attr.mq_maxmsg =  uxQueueLength;
	    attr.mq_msgsize = uxItemSize;
	    attr.mq_curmsgs = 0;

	    memcpy(&pmq_info->mqOrgAttr, &attr, sizeof(struct mq_attr));

	    /* create the message queue */
	    memset(pmq_info->mqName, 0 , MQ_NAME_STR_MAX_SIZE);
        strcpy(pmq_info->mqName, MqPipeName);
    	//printf("dxQueueCreate name = %s\n", pmq_info->mqName);
    	pmq_info->mqID = mq_open(pmq_info->mqName, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG | S_IRWXO, &attr);

	    if (pmq_info->mqID >= 0)
	    {
	    	dxQueueHandler = pmq_info;
	    }
	    else
	    {
	    	printf("dxQueueCreate (%s) Err=%s\n", pmq_info->mqName, strerror(errno));
	    	dxMemFree(pmq_info);
	    }
	}

MqGCreateExit:
    return dxQueueHandler;

}

dxQueueHandle_t dxQueueGlobalGet(void*    GlobalIdentifier)
{
	dxQueueHandle_t dxQueueHandler = NULL;
    char* MqPipeName = (char*)GlobalIdentifier;

    if (MqPipeName == NULL)
        goto MqGCreateExit;

    if (strlen(MqPipeName) >= MQ_NAME_STR_MAX_SIZE)
        goto MqGCreateExit;

	mq_info_t* pmq_info = dxMemAlloc("pmq_info", 1, sizeof(mq_info_t));

	if (pmq_info)
	{
	    struct mq_attr attr;

    	pmq_info->mqID = mq_open(MqPipeName, O_RDWR, S_IRWXU | S_IRWXG | S_IRWXO, &attr);

	    if (pmq_info->mqID < 0)
	    {
	    	printf("dxQueueGlobalGet (%s) Err=%s\n", MqPipeName, strerror(errno));
	    	dxMemFree(pmq_info);
            goto MqGCreateExit;
	    }

        if (mq_getattr(pmq_info->mqID, &attr) != 0)
        {
            dxMemFree(pmq_info);
            goto MqGCreateExit;
        }

	    memcpy(&pmq_info->mqOrgAttr, &attr, sizeof(struct mq_attr));
        memcpy(pmq_info->mqName, MqPipeName, strlen(MqPipeName));

        dxQueueHandler = pmq_info;
	}

MqGCreateExit:
    return dxQueueHandler;
}

dxOS_RET_CODE dxQueueReceive(dxQueueHandle_t dxQueue,
                             void *pvBuffer,
                             uint32_t dxMsToWait)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxQueue && pvBuffer)
	{
		ssize_t mq_sizeRcv = -1;
		mq_info_t* pmq_info = (mq_info_t*)dxQueue;

		if (dxMsToWait == DXOS_WAIT_FOREVER)
		{
			mq_sizeRcv = mq_receive(pmq_info->mqID, pvBuffer, pmq_info->mqOrgAttr.mq_msgsize, NULL);
		}
		else
		{
			struct timespec CurrentTime, OffsetTime, WaitToTime;
			struct timeval CurrentTimeval, OffsetTimeval, WaitToTimeval;
			OffsetTime.tv_sec = dxMsToWait/1000;
			OffsetTime.tv_nsec = (dxMsToWait%1000) * 1000000;
			clock_gettime(CLOCK_REALTIME, &CurrentTime);

			DX_TIMESPEC_TO_TIMEVAL(&CurrentTimeval, &CurrentTime);
			DX_TIMESPEC_TO_TIMEVAL(&OffsetTimeval, &OffsetTime);
			timeradd(&OffsetTimeval, &CurrentTimeval, &WaitToTimeval);

			DX_TIMEVAL_TO_TIMESPEC(&WaitToTimeval, &WaitToTime);

			mq_sizeRcv = mq_timedreceive(pmq_info->mqID, pvBuffer, pmq_info->mqOrgAttr.mq_msgsize, NULL, &WaitToTime);

		}

		if (mq_sizeRcv < 0)
			retCode = DXOS_ERROR;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE dxQueueReceiveFromISR(dxQueueHandle_t dxQueue,
                                    void *pvBuffer,
                                    dxBOOL* HigherPriorityTaskWoken)
{
	//In Linux this is going to be equivalent to dxQueueReceive
	return dxQueueReceive(dxQueue, pvBuffer, DXOS_WAIT_FOREVER);
}

dxOS_RET_CODE dxQueueSend(dxQueueHandle_t dxQueue,
                          const void * pvItemToQueue,
                          uint32_t dxMsToWait)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxQueue && pvItemToQueue)
	{
        mq_info_t* pmq_info = (mq_info_t*)dxQueue;

        retCode = dxQueueSendWithSize(  dxQueue,
                                        pvItemToQueue,
                                        pmq_info->mqOrgAttr.mq_msgsize,
                                        dxMsToWait);
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;

}


dxOS_RET_CODE dxQueueSendWithSize(  dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    uint32_t     MessageSizeInByte,
                                    uint32_t dxMsToWait)
{

	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxQueue && pvItemToQueue)
	{
		int mq_sizeSent = -1;
		mq_info_t* pmq_info = (mq_info_t*)dxQueue;

        if (MessageSizeInByte > pmq_info->mqOrgAttr.mq_msgsize)
        {
            retCode = DXOS_INVALID;
            goto dxQSendWithSizeExit;
        }

		if (dxMsToWait == DXOS_WAIT_FOREVER)
		{
			mq_sizeSent = mq_send(pmq_info->mqID, pvItemToQueue, MessageSizeInByte, 0);
		}
		else
		{
			struct timespec CurrentTime, OffsetTime, WaitToTime;
			struct timeval CurrentTimeval, OffsetTimeval, WaitToTimeval;
			OffsetTime.tv_sec = dxMsToWait/1000;
			OffsetTime.tv_nsec = (dxMsToWait%1000) * 1000000;
			clock_gettime(CLOCK_REALTIME, &CurrentTime);

			DX_TIMESPEC_TO_TIMEVAL(&CurrentTimeval, &CurrentTime);
			DX_TIMESPEC_TO_TIMEVAL(&OffsetTimeval, &OffsetTime);
			timeradd(&OffsetTimeval, &CurrentTimeval, &WaitToTimeval);

			DX_TIMEVAL_TO_TIMESPEC(&WaitToTimeval, &WaitToTime);

			mq_sizeSent = mq_timedsend(pmq_info->mqID, pvItemToQueue, MessageSizeInByte, 0, &WaitToTime);

		}

		if (mq_sizeSent < 0)
			retCode = DXOS_ERROR;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

dxQSendWithSizeExit:

    return retCode;
}

dxOS_RET_CODE dxQueueSendFromISR(   dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    dxBOOL* HigherPriorityTaskWoken)
{
	//In Linux this is going to be equivalent to dxQueueSend
	return dxQueueSend(dxQueue, pvItemToQueue, DXOS_WAIT_FOREVER);
}

uint32_t dxQueueSpacesAvailable(const dxQueueHandle_t dxQueue)
{
	uint32_t SpaceAvailable = 0;

    if (dxQueue)
    {
    	struct mq_attr attr;
    	mq_info_t* pmq_info = (mq_info_t*)dxQueue;

    	int Success = mq_getattr(pmq_info->mqID, &attr);
    	if (Success == 0)
    	{
    		SpaceAvailable = attr.mq_maxmsg - attr.mq_curmsgs;
    	}
    }

    return SpaceAvailable;
}

dxOS_RET_CODE dxQueueReset(dxQueueHandle_t dxQueue, dxQueueResetFunction_t dxResetFunction)
{
    while (dxQueueIsEmpty (dxQueue) != dxTRUE) {
        void* item = NULL;

        if (dxQueueReceive (dxQueue, &item, 0)) {
            if (item && dxResetFunction) {
                dxResetFunction (item);
            }
        }
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxQueueDelete(dxQueueHandle_t dxQueue)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;
    if (dxQueue)
    {
    	mq_info_t* pmq_info = (mq_info_t*)dxQueue;

    	if (mq_close(pmq_info->mqID) == 0)
    	{
    		RetCode = DXOS_SUCCESS;
    	}

    	//TODO: Should we also call unlink? our intention is to have system remove the queu name when all mq is closed (check dxQueueCreate TODO)

    	dxMemFree(dxQueue);
    }

    return RetCode;
}

dxBOOL dxQueueIsEmpty(dxQueueHandle_t dxQueue)
{
	dxBOOL IsEmpty = dxFALSE;
    if (dxQueue)
    {
    	struct mq_attr attr;
    	mq_info_t* pmq_info = (mq_info_t*)dxQueue;

    	int Success = mq_getattr(pmq_info->mqID, &attr);
    	if (Success == 0)
    	{
    		IsEmpty = (attr.mq_curmsgs == 0) ? dxTRUE:dxFALSE;
    	}
    }

    return IsEmpty;
}

dxBOOL dxQueueIsFull(dxQueueHandle_t dxQueue)
{
	dxBOOL IsFull = dxTRUE;
    if (dxQueue)
    {
    	struct mq_attr attr;
    	mq_info_t* pmq_info = (mq_info_t*)dxQueue;

    	int Success = mq_getattr(pmq_info->mqID, &attr);
    	if (Success == 0)
    	{
    		IsFull = (attr.mq_curmsgs == attr.mq_maxmsg) ? dxTRUE:dxFALSE;
    	}
    }

    return IsFull;
}


//============================================================================
// Task Management
//============================================================================


dxOS_RET_CODE dxTaskCreate(dxTaskFunction_t pvTaskCode,
                           const char * pcName,
                           uint16_t usStackSizeInByte,
                           void *pvParameters,
                           dxTaskPriority_t uxPriority,
                           dxTaskHandle_t *pvCreatedTask)
{
    struct sched_param schedparam;
	pthread_attr_t  attr;
    int             schedminpriority = 0;
	size_t ThreadStackSize = (size_t) (usStackSizeInByte < PTHREAD_STACK_MIN) ? PTHREAD_STACK_MIN:usStackSizeInByte;

	long SystemPageSize = sysconf(_SC_PAGESIZE);
	if (SystemPageSize > 0)
	{
		size_t LeftOver = ThreadStackSize%SystemPageSize;
		if (LeftOver)
		{
			ThreadStackSize = ((ThreadStackSize/SystemPageSize) +1) * SystemPageSize;
		}
	}

    pThread_info_t *pThreadInfo = dxMemAlloc("THREAD", 1, sizeof(pThread_info_t));
    if (pThreadInfo)
    {
    	if (pthread_attr_init(&attr) != 0)
    	{
    		dxMemFree(pThreadInfo);
    		return DXOS_ERROR;
    	}

        if (uxPriority >= DXTASK_PRIORITY_NORMAL)
        {
            if (pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED) != 0)
            {
                dxMemFree(pThreadInfo);
                return DXOS_ERROR;
            }

            if (pthread_attr_setschedpolicy(&attr, SCHED_RR) != 0)
            {
                dxMemFree(pThreadInfo);
                return DXOS_ERROR;
            }

            schedminpriority = sched_get_priority_min(SCHED_RR);
            schedparam.sched_priority = schedminpriority + uxPriority;

            if (pthread_attr_setschedparam(&attr, &schedparam) != 0)
            {
                dxMemFree(pThreadInfo);
                return DXOS_ERROR;
            }
        }

		if (pthread_attr_setstacksize(&attr , ThreadStackSize) != 0)
		{
			dxMemFree(pThreadInfo);
			pthread_attr_destroy(&attr);
			return DXOS_ERROR;
		}

		if (pthread_create(&pThreadInfo->threadId, &attr, pvTaskCode, pvParameters) != 0)
		{
			dxMemFree(pThreadInfo);
			pthread_attr_destroy(&attr);
			return DXOS_ERROR;
		}

		pthread_attr_destroy(&attr); //There is no reason why this will fail, even so its not critical.

		*pvCreatedTask = pThreadInfo;

		printf("dxTaskCreate Success (Id = %ld, SystemPageSize = %ld, ThreadStackSize  = %d)\n", pThreadInfo->threadId, SystemPageSize, ThreadStackSize);
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxTaskSuspend(dxTaskHandle_t dxTaskToSuspend)
{

    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxTaskResume(dxTaskHandle_t dxTaskToResume)
{

    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxTaskPrioritySet(dxTaskHandle_t dxTask, uint32_t uxNewPriority)
{

    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxTaskDelete(dxTaskHandle_t dxTaskToDelete)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxTaskToDelete)
	{
		int status = -1;
		pThread_info_t* pThreadInfo = (pThread_info_t*)dxTaskToDelete;

		status = pthread_cancel(pThreadInfo->threadId);
		if ( status !=  0)
		{
			perror("dxTaskDelete pthread_cancel failed");
		}
		else
		{
			void* RetVal;

			//TODO: Should think of a way to have timeout (pthread_timedjoin_np is NOT portable)
			//		otherwise we may be stuck in here forever.
			status = pthread_join( pThreadInfo->threadId, &RetVal);
			if ( status <  0)
			{
				perror("dxTaskDelete pthread_join failed");
			}
			else
			{
				RetCode = DXOS_SUCCESS;
				dxMemFree(pThreadInfo);
			}
		}

	}

    return RetCode;
}

dxTaskHandle_t dxTaskGetCurrentTaskHandle(void)
{

    return NULL;
}

static TASK_RET_TYPE dkworker_task_main( TASK_PARAM_TYPE arg )
{
	dxWorkweTask_t* worker_task = (dxWorkweTask_t*) arg;

    while (1)
    {
        dxEventMsg_t message;

        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);

        if (dxQueueReceive(   worker_task->WEventQueue,
                              &message,
                              1000) == DXOS_SUCCESS)
        {
            if (message.function)
            {
            	message.function( message.arg );
            }
        }

        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

        dxTimeDelayMS(1); //Allow this task to handle thread cancel event (eg, this is the cancellation point for pThread)
    }

    END_OF_TASK_FUNCTION;
}

dxOS_RET_CODE dxWorkerTaskCreate (const char* name, dxWorkweTask_t *pWorkerTask, dxTaskPriority_t uxPriority, uint16_t usStackDepth, uint32_t EventQueueSize )
{

    memset( pWorkerTask, 0, sizeof( dxWorkweTask_t ) );

    pWorkerTask->WEventQueue = dxQueueCreate( EventQueueSize,
                                              sizeof(dxEventMsg_t));

    if ( pWorkerTask->WEventQueue == NULL )
    {
        return DXOS_ERROR;
    }

    if (dxTaskCreate(   dkworker_task_main,
                        name,
                        usStackDepth,
                        (void *)pWorkerTask,
                        uxPriority,
                        &pWorkerTask->WTask) != DXOS_SUCCESS)
    {
        dxQueueDelete(pWorkerTask->WEventQueue);
        return DXOS_ERROR;
    }

    return DXOS_SUCCESS;

}

dxOS_RET_CODE dxWorkerTaskDelete( dxWorkweTask_t *WorkerTask)
{
	if (WorkerTask)
	{
		dxTaskDelete(WorkerTask->WTask);
    	dxQueueDelete(WorkerTask->WEventQueue);
	}
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxWorkerTaskSendAsynchEvent( dxWorkweTask_t *WorkerTask, dxEventHandler_t function, void* arg )
{

    dxEventMsg_t message;

    message.function = function;
    message.arg = arg;

    return dxQueueSend(WorkerTask->WEventQueue,
                       &message,
                       0);

}


//============================================================================
// Task Stack
//============================================================================

dxOS_RET_CODE dxTaskGetStackInfo(dxTaskHandle_t dxTask, dxTaskStack_t *pStackBuf)
{
#if 0 //TODO
    int len = 0;

    if (dxTask == NULL ||
        pStackBuf == NULL) {

        return DXOS_ERROR;
    }

    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTask;

    memset(pStackBuf, 0x00, sizeof(dxTaskStack_t));

    pStackBuf->nTaskNumber = uxTaskGetTaskNumber(vTaskHandle);
    pStackBuf->nPriority = uxTaskPriorityGet(vTaskHandle);

    pStackBuf->nStackSize = 0;

    // INCLUDE_uxTaskGetStackHighWaterMark must be set to 1 in FreeRTOSConfig.h
    // to use uxTaskGetStackHighWaterMark()
    //NOTE: Here the returned stack size in uxTaskGetStackHighWaterMark is the 'depth' NOT the size in byte so we need to
    //      convert to byte by multiplying to the size of RTOS StackType_t
    pStackBuf->nFreeStackSpace = uxTaskGetStackHighWaterMark(vTaskHandle) * sizeof(StackType_t);

    len = strlen(pcTaskGetTaskName(vTaskHandle));
    if (len >= sizeof(pStackBuf->szTaskName)) {

        len = sizeof(pStackBuf->szTaskName) - 1;
    }
    memcpy(pStackBuf->szTaskName, pcTaskGetTaskName(vTaskHandle), len);
#endif
    return DXOS_SUCCESS;
}


//============================================================================
// Semaphore Management
//============================================================================

typedef struct
{
	sem_t			SemId;
	uint32_t   		MaxCount;
} Sem_info_t;

dxSemaphoreHandle_t dxSemCreate(uint32_t uxMaxCount,
                                uint32_t uxInitialCount)
{
	dxSemaphoreHandle_t SemHandler = NULL;

	if (uxMaxCount)
	{
		Sem_info_t* pSemInfo = dxMemAlloc("SEM", 1, sizeof(Sem_info_t));
		if (pSemInfo)
		{
			int SeminitRet = sem_init(&pSemInfo->SemId, 0, uxInitialCount);
			if (SeminitRet == 0)
			{
				pSemInfo->MaxCount = uxMaxCount;
				SemHandler = pSemInfo;
			}
			else
			{
				printf("sem_init Err=%s\n", strerror(errno));
				dxMemFree(pSemInfo);
			}
		}
	}

    return SemHandler;
}

dxOS_RET_CODE dxSemGive(dxSemaphoreHandle_t dxSemaphore)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

	int sVal = -1;
	Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

	if (sem_getvalue(&SemInfo->SemId, &sVal) != 0)
	{
		printf("sem_getvalue Err=%s\n", strerror(errno));
		goto SemGiveExit;
	}

	if (sVal >= 0)
	{
		if (sVal < SemInfo->MaxCount)
		{
			if  (sem_post(&SemInfo->SemId) != 0)
			{
				printf("sem_post Err=%s\n", strerror(errno));
				goto SemGiveExit;
			}
		}
	}

	RetCode = DXOS_SUCCESS;

SemGiveExit:

    dxMutexGive(dxSem_Resource_MutexHandle);

    return RetCode;
}

dxOS_RET_CODE dxSemTake(dxSemaphoreHandle_t dxSemaphore, uint32_t xBlockTimeInMS)
{
	dxOS_RET_CODE RetCode = DXOS_SUCCESS;

	if (dxSemaphore)
	{
		dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

		Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

		if (xBlockTimeInMS == DXOS_WAIT_FOREVER)
		{
			dxMutexGive(dxSem_Resource_MutexHandle);

			int SemRet = sem_wait(&SemInfo->SemId);
			if (SemRet < 0)
				RetCode = DXOS_ERROR;
		}
		else
		{

			struct timespec WaitToTime = IntUtilAddOffsetToCurrentTime(xBlockTimeInMS);

			dxMutexGive(dxSem_Resource_MutexHandle);

			int SemRet = sem_timedwait(&SemInfo->SemId, &WaitToTime);

			if (SemRet < 0)
			{
				if (errno == ETIMEDOUT)
					RetCode = DXOS_TIMEOUT;
				else
					RetCode = DXOS_ERROR;
			}
		}

	}
	else
	{
		RetCode = DXOS_ERROR;
	}

    return RetCode;
}

dxOS_RET_CODE dxSemDelete(dxSemaphoreHandle_t dxSemaphore)
{
	dxOS_RET_CODE RetCode = DXOS_ERROR;

	if (dxSemaphore)
	{
		dxMutexTake(dxSem_Resource_MutexHandle, DXOS_WAIT_FOREVER);

		Sem_info_t* SemInfo = (Sem_info_t*)dxSemaphore;

		if (sem_destroy(&SemInfo->SemId) != 0)
		{
			printf("sem_destroy Err=%s\n", strerror(errno));
		}
		else
		{
			dxMemFree(dxSemaphore);
			RetCode = DXOS_SUCCESS;
		}

		dxMutexGive(dxSem_Resource_MutexHandle);
	}

    return RetCode;
}


//============================================================================
// Mutex Management
//============================================================================

typedef struct
{
	pthread_mutex_t   MutexId;

} pThreadMutex_info_t;

dxMutexHandle_t dxMutexCreate(void)
{
	dxMutexHandle_t		MutexHandler = NULL;
	pthread_mutexattr_t Mutexattr;


	pThreadMutex_info_t *pThreadMutexInfo = dxMemAlloc("THREADMUTEX", 1, sizeof(pThreadMutex_info_t));
    if (pThreadMutexInfo)
    {
    	if (pthread_mutexattr_init(&Mutexattr) != 0)
    	{
    		dxMemFree(pThreadMutexInfo);
    		return NULL;
    	}

		if (pthread_mutexattr_settype(&Mutexattr , PTHREAD_MUTEX_NORMAL) != 0)
		{
			dxMemFree(pThreadMutexInfo);
			pthread_mutexattr_destroy(&Mutexattr);
			return NULL;
		}

		if (pthread_mutex_init(&pThreadMutexInfo->MutexId, &Mutexattr) != 0)
		{
			dxMemFree(pThreadMutexInfo);
			pthread_mutexattr_destroy(&Mutexattr);
			return NULL;
		}

		pthread_mutexattr_destroy(&Mutexattr); //There is no reason why this will fail, even so its not critical.

		MutexHandler = pThreadMutexInfo;

    }

    return MutexHandler;
}

dxOS_RET_CODE dxMutexTake(dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS)
{

	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		int MutexRet = -1;
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;

		if (xBlockTimeInMS == DXOS_WAIT_FOREVER)
		{
			MutexRet = pthread_mutex_lock(&pThreadMutexInfo->MutexId);
		}
		else
		{
			struct timespec CurrentTime, OffsetTime, WaitToTime;
			struct timeval CurrentTimeval, OffsetTimeval, WaitToTimeval;
			OffsetTime.tv_sec = xBlockTimeInMS/1000;
			OffsetTime.tv_nsec = (xBlockTimeInMS%1000) * 1000000;
			clock_gettime(CLOCK_REALTIME, &CurrentTime);

			DX_TIMESPEC_TO_TIMEVAL(&CurrentTimeval, &CurrentTime);
			DX_TIMESPEC_TO_TIMEVAL(&OffsetTimeval, &OffsetTime);
			timeradd(&OffsetTimeval, &CurrentTimeval, &WaitToTimeval);

			DX_TIMEVAL_TO_TIMESPEC(&WaitToTimeval, &WaitToTime);

			MutexRet = pthread_mutex_timedlock(&pThreadMutexInfo->MutexId, &WaitToTime);
		}

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
		else if (MutexRet == ETIMEDOUT)
			retCode = DXOS_TIMEOUT;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE dxMutexGive(dxMutexHandle_t dxMutex)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;
		int MutexRet = pthread_mutex_unlock(&pThreadMutexInfo->MutexId);

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}

dxOS_RET_CODE dxMutexDelete(dxMutexHandle_t dxMutex)
{
	dxOS_RET_CODE retCode = DXOS_SUCCESS;

	if (dxMutex)
	{
		pThreadMutex_info_t* pThreadMutexInfo = (pThreadMutex_info_t*)dxMutex;
		int MutexRet = pthread_mutex_destroy(&pThreadMutexInfo->MutexId);

		if (MutexRet < 0)
			retCode = DXOS_ERROR;
		else
			dxMemFree(dxMutex);
	}
	else
	{
		retCode = DXOS_ERROR;
	}

    return retCode;
}



dxAesContext_t  dxAESSW_ContextAlloc(void)
{
    dxAesContext_t pAesContext = NULL;

    pAesContext = dxMemAlloc("SwAesContext", 1, AES_ENC_DEC_BYTE);

    return pAesContext;
}



dxAES_RET_CODE  dxAESSW_Setkey( dxAesContext_t ctx, const unsigned char *key, unsigned int keysize, dxAES_CRYPTO_TYPE Type )
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    //Currently we only support 128bit SW AES
    if (keysize != 128)
        return DXAES_INVALID_PARAM;

    if (ctx && key)
    {
        memcpy(ctx, key, keysize/8);
    }
    else
    {
       ret = DXAES_INVALID_PARAM;
    }

    return ret;
}


dxAES_RET_CODE  dxAESSW_CryptEcb( dxAesContext_t ctx, dxAES_CRYPTO_TYPE Type, const unsigned char input[16], unsigned char output[16] )
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    if (ctx)
    {

        if (Type == DXAES_ENCRYPT)
        {
            AES128_ECB_encrypt((uint8_t*)input, (const uint8_t*)ctx, (uint8_t*)output);
        }
        else if (Type == DXAES_DECRYPT)
        {
            AES128_ECB_decrypt((uint8_t*)input, (const uint8_t*)ctx, (uint8_t*)output);
        }
        else
        {
            ret = DXAES_INVALID_PARAM;
        }

    }
    else
    {
       ret = DXAES_INVALID_PARAM;
    }

    return ret;
}

dxAES_RET_CODE  dxAESSW_ContextDealloc(dxAesContext_t ctx)
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    if (ctx)
    {
        dxMemFree(ctx);
    }

    return ret;
}

uint32_t
dxRand()
{
	return rand();
}

#if 0

void  dxSPIaccess_ResourceTake(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE MutexRet = dxMutexTake(dxSPI_Resource_MutexHandle, DXOS_WAIT_FOREVER);
    if ( MutexRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceTake failed!!!!!\r\n");
    }
}

void  dxSPIaccess_ResourceGive(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE MutexRet = dxMutexGive(dxSPI_Resource_MutexHandle);
    if ( MutexRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceGive failed!!!!!\r\n");
    }
}

#endif


int dxSelect (
    int nfds,
    fd_set* readfds,
    fd_set* writefds,
    fd_set* exceptfds,
    struct timeval* timeout
) {
    int ret = select (
        nfds,
        readfds,
        writefds,
        exceptfds,
        timeout
    );

    return ret;
}


int dxNumOfDigits (uint32_t v) {
    uint32_t po10;
    int n;

    n=1;
    po10=10;

    while (v >= po10) {
        n++;
        if (po10 > PO10_LIMIT) break;
            po10 *= 10;
    }

    return n;
}
