/*
 * wpa_supplicant/hostapd control interface library
 * Copyright (c) 2004-2007, Jouni Malinen <j@w1.fi>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * Alternatively, this software may be distributed under the terms of BSD
 * license.
 *
 * See README and COPYING for more details.
 */

//==========================================================================
// Include File
//==========================================================================
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <sys/un.h>

#include "dxWPACtrl.h"
#include "dxOS.h"
#include "dxSysDebugger.h"

//==========================================================================
// Type Declaration
//==========================================================================
# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

/**
 * struct wpa_ctrl - Internal structure for control interface library
 *
 * This structure is used by the wpa_supplicant/hostapd control interface
 * library to store internal data. Programs using the library should not touch
 * this data directly. They can only use the pointer to the data structure as
 * an identifier for the control interface connection and use this as one of
 * the arguments for most of the control interface library functions.
 */
struct wpa_ctrl {
	int s;
	struct sockaddr_un local;
	struct sockaddr_un dest;
};

typedef struct {
	dxEventHandler_t	ScanResultHdl;
	void				*user_data;
} wpa_scan_ctx;

#ifndef CONFIG_CTRL_IFACE_CLIENT_DIR
#define CONFIG_CTRL_IFACE_CLIENT_DIR "/var"
#endif /* CONFIG_CTRL_IFACE_CLIENT_DIR */

#ifndef CONFIG_CTRL_IFACE_CLIENT_PREFIX
#define CONFIG_CTRL_IFACE_CLIENT_PREFIX "wpa_ctrl_"
#endif /* CONFIG_CTRL_IFACE_CLIENT_PREFIX */

#define SCAN_RESULT_DEFAULT_WORKER_PRIORITY          DXTASK_PRIORITY_IDLE
#define SCAN_RESULT_WORKER_THREAD_STACK_SIZE         1024
#define SCAN_RESULT_WORKER_THREAD_MAX_EVENT_QUEUE    2

//==========================================================================
// Static Params
//==========================================================================
static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;
static struct {
    WPA_STATUS      status;
    int				mode;
    uint8_t         ip[ 4 ];
    uint8_t         mac[ 6 ];
	int             signal_level;
} wpa_info = {
    WPA_STATUS_ERROR,
    DXWIFI_MODE_NONE,
	{ 0, },
	{ 0, },
	0,
};

static dxWorkweTask_t   ScanResultWorkerThread = {
	.WTask			= NULL,
	.WEventQueue	= NULL
};

//==========================================================================
// Static Functions
//==========================================================================
static struct wpa_ctrl *
wpa_ctrl_open( const char *ctrl_path )
{
	struct wpa_ctrl *ctrl;
	static int counter = 0;
	int ret;
	size_t res;
	int tries = 0;

	ctrl = dxMemAlloc( "wpa ctrl", 1, sizeof( struct wpa_ctrl ) );
	if ( ctrl == NULL ) {
		return NULL;
	}

	memset( ctrl, 0, sizeof( struct wpa_ctrl ) );

	ctrl->s = socket( PF_UNIX, SOCK_DGRAM, 0 );
	if ( ctrl->s < 0 ) {
		dxMemFree( ctrl );
		return NULL;
	}

	ctrl->local.sun_family = AF_UNIX;
	counter ++;

try_again:
	ret = snprintf( ctrl->local.sun_path, sizeof( ctrl->local.sun_path ),
									  CONFIG_CTRL_IFACE_CLIENT_DIR "/"
									  CONFIG_CTRL_IFACE_CLIENT_PREFIX "%d-%d",
									  ( int ) getpid(), counter );
	if ( ret < 0 || (size_t)ret >= sizeof( ctrl->local.sun_path ) ) {
		close( ctrl->s );
		dxMemFree( ctrl );
		return NULL;
	}

	tries ++;
	if ( bind( ctrl->s, ( struct sockaddr * )&ctrl->local, sizeof( ctrl->local ) ) < 0 ) {
		if ( errno == EADDRINUSE && tries < 2 ) {
			/*
			 * getpid() returns unique identifier for this instance
			 * of wpa_ctrl, so the existing socket file must have
			 * been left by unclean termination of an earlier run.
			 * Remove the file and try again.
			 */
			unlink( ctrl->local.sun_path );
			goto try_again;
		}
		close( ctrl->s );
		dxMemFree( ctrl );
		return NULL;
	}

	ctrl->dest.sun_family = AF_UNIX;
	res = snprintf( ctrl->dest.sun_path, sizeof( ctrl->dest.sun_path ), "%s", ctrl_path );
	if ( res >= sizeof( ctrl->dest.sun_path ) ) {
		close( ctrl->s );
		dxMemFree( ctrl );
		return NULL;
	}

	if ( connect( ctrl->s, ( struct sockaddr * ) &ctrl->dest, sizeof( ctrl->dest ) ) < 0 ) {
		close( ctrl->s );
		unlink( ctrl->local.sun_path );
		dxMemFree( ctrl );
		return NULL;
	}

	return ctrl;
}

static void
wpa_ctrl_close( struct wpa_ctrl *ctrl )
{
	if ( ctrl == NULL ) {
		return;
	}

	unlink( ctrl->local.sun_path );
	if ( ctrl->s >= 0 ) {
		close( ctrl->s );
	}

	dxMemFree( ctrl );
}

static int
wpa_ctrl_request( struct wpa_ctrl *ctrl, const char *cmd, size_t cmd_len, char *reply, int reply_len )
{
	struct timeval tv;
	int res;
	fd_set rfds;

	if ( send( ctrl->s, cmd, cmd_len, 0 ) < 0 ) {
		return -1;
	}

	tv.tv_sec	= 10;
	tv.tv_usec	= 0;

	FD_ZERO( &rfds );
	FD_SET( ctrl->s, &rfds );

	res = select( ctrl->s + 1, &rfds, NULL, NULL, &tv );
	if ( res < 0 ) {
		return -1;
	}

	if ( FD_ISSET( ctrl->s, &rfds ) ) {
		res = recv( ctrl->s, reply, reply_len, 0 );

		if ( res < 0 ) {
			return -1;
		}

		return res;
	} else {
		return 0;
	}
}

static dxWIFI_RET_CODE
wpa_ctrl_set( struct wpa_ctrl *ctrl, char *cmd )
{
	char buf[ WPA_BUF + 1 ];
	size_t len = wpa_ctrl_request( ctrl, cmd, strlen( cmd ), buf, WPA_BUF );

	if ( len <= 0 ) {
	    dbg_warm( "'%s' command failed.\n", cmd );
	    return DXWIFI_ERROR;
	} else if ( memcmp( buf, "OK", 2 ) ) {
		buf[ len ] = '\0';
		dbg_warm( "Do %s Failed: %s", cmd, buf );
		return DXWIFI_ERROR;
	}

	return DXWIFI_SUCCESS;
}

static int
wpa_ctrl_get( struct wpa_ctrl *ctrl, char *cmd, char *buf, int buf_len )
{
	size_t len = wpa_ctrl_request( ctrl, cmd, strlen( cmd ), buf, buf_len - 1 );

	if ( len < 0 ) {
	    dbg_warm( "'%s' command failed.\n", cmd );
	    return len;
	}

	buf[ len ] = '\0';

	return len;
}

static dxWIFI_RET_CODE
wpa_ctel_setup( struct wpa_ctrl *ctrl, char *ssid, dxWiFi_Security_t security_type, char *password, int key_id )
{
	char buf[ WPA_BUF ];
	char cmd[ 256 ];

	if ( wpa_ctrl_set( ctrl, "RECONNECT" ) ) {
		return DXWIFI_ERROR;
	}

    wpa_ctrl_set( ctrl, "REMOVE_NETWORK 0" );

	if ( wpa_ctrl_get( ctrl, "ADD_NETWORK", buf, WPA_BUF ) > 0 ) {
		if ( strncmp( buf, "0", 1 ) ) {
			return DXWIFI_ERROR;
		}
	} else {
		return DXWIFI_ERROR;
	}

	snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 ssid \"%s\"", ssid );
	if ( wpa_ctrl_set( ctrl, cmd ) ) {
		return DXWIFI_BADARG;
	}

	if ( wpa_ctrl_set( ctrl, "SET_NETWORK 0 scan_ssid 1" ) ) {
		return DXWIFI_ERROR;
	}

	switch (security_type) {
		case DXWIFI_SECURITY_OPEN:
		case DXWIFI_SECURITY_WPS_OPEN:
			if ( wpa_ctrl_set( ctrl, "SET_NETWORK 0 key_mgmt NONE" ) ) {
				return DXWIFI_ERROR;
			}
			break;

		case DXWIFI_SECURITY_WEP_PSK:
		case DXWIFI_SECURITY_WEP_SHARED:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "NONE" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 wep_key0 %s", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 wep_tx_keyidx %d", key_id );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}
			break;

		case DXWIFI_SECURITY_WPA_TKIP_PSK:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "WPA" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "TKIP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}
			break;

		case DXWIFI_SECURITY_WPA_AES_PSK:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "WPA" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "CCMP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			break;

		case DXWIFI_SECURITY_WPA2_AES_PSK:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "RSN" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "CCMP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			break;

		case DXWIFI_SECURITY_WPA2_TKIP_PSK:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "RSN" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "TKIP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			break;

		case DXWIFI_SECURITY_WPA2_MIXED_PSK:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "RSN" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "CCMP TKIP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			break;

		case DXWIFI_SECURITY_WPA_WPA2_MIXED:
			if ( password == NULL ) {
				return DXWIFI_BADARG;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 key_mgmt %s", "WPA-PSK" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 proto %s", "RSN WPA" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 psk \"%s\"", password );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			snprintf( cmd, sizeof( cmd ), "SET_NETWORK 0 pairwise %s", "CCMP TKIP" );
			if ( wpa_ctrl_set( ctrl, cmd ) ) {
				return DXWIFI_ERROR;
			}

			break;

		case DXWIFI_SECURITY_WPS_SECURE:
			return DXWIFI_UNSUPPORTED;

		case DXWIFI_SECURITY_UNKNOWN:
			return DXWIFI_UNSUPPORTED;

		case DXWIFI_SECURITY_FORCE_32_BIT:
			return DXWIFI_UNSUPPORTED;

		default:
			return -5;
	}

	if ( wpa_ctrl_set( ctrl, "ENABLE_NETWORK 0" ) ) {
		return DXWIFI_ERROR;
	} else if ( wpa_ctrl_set( ctrl, "STA_AUTOCONNECT 0" ) ) {
		return DXWIFI_ERROR;
	}

	return DXWIFI_SUCCESS;
}

/*
bssid=e8:94:f6:f3:29:af
ssid=test
id=0
passphrase=8888888888
psk=2fc43de3dd287c8a6f1cf8487e8872b48c6a498ac467a208c8fc8feabb486c
mode=station
pairwise_cipher=CCMP
group_cipher=TKIP
key_mgmt=WPA2-PSK
wpa_state=COMPLETED
ip_address=192.168.10.104
address=e0:b9:4d:75:49:14
signal_level=88
*/

static dxWIFI_RET_CODE
wpa_ctrl_parser_status( char *src )
{
	char *next, *value;

	memset( &wpa_info, 0, sizeof( wpa_info ) );

	wpa_info.status     = WPA_STATUS_ERROR;

	while ( src && src[ 0 ] ) {
		next	= strchr( src, '\n' );
		if ( next ) {
			next[ 0 ] = 0;
			next ++;
		}

		value = strchr( src, '=' );
		if ( value ) {
			value[ 0 ] = 0;
			value ++;
		} else {
			return DXWIFI_ERROR;
		}

		if ( memcmp( src, "wpa_state", 9 ) == 0 ) {
			if ( memcmp( value, "COMPLETED", 9 ) == 0 ) {
				wpa_info.status = WPA_STATUS_COMPLETED;
			} else if ( memcmp( value, "SCANNING", 8 ) == 0 ) {
				wpa_info.status = WPA_STATUS_SCANNING;
			} else if ( memcmp( value, "ASSOCIATING", 11 ) == 0 ) {
				wpa_info.status = WPA_STATUS_ASSOCIATING;
			} else if ( memcmp( value, "INACTIVE", 8 ) == 0 ) {
				wpa_info.status = WPA_STATUS_INACTIVE;
			} else if ( memcmp( value, "DISCONNECTED", 12 ) == 0 ) {
				wpa_info.status = WPA_STATUS_DISCONNECTED;
			} else {
				wpa_info.status = WPA_STATUS_ERROR;
			}
		} else if ( memcmp( src, "mode", 4 ) == 0 ) {
			if ( memcmp( value, "station", 7 ) == 0 ) {
				wpa_info.mode	= DXWIFI_MODE_STA;
			} else if ( memcmp( value, "ap", 2 ) == 0 ) {
				wpa_info.mode	= DXWIFI_MODE_AP;
			} else {
				wpa_info.mode	= DXWIFI_MODE_NONE;
			}
		} else if ( memcmp( src, "address", 7 ) == 0 ) {
			sscanf( value, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", &wpa_info.mac[ 0 ], &wpa_info.mac[ 1 ], &wpa_info.mac[ 2 ],
															&wpa_info.mac[ 3 ], &wpa_info.mac[ 4 ], &wpa_info.mac[ 5 ] );
		} else if ( memcmp( src, "ip_address", 10 ) == 0 ) {
			sscanf( value, "%hhd.%hhd.%hhd.%hhd", &wpa_info.ip[ 0 ], &wpa_info.ip[ 1 ], &wpa_info.ip[ 2 ], &wpa_info.ip[ 3 ] );
		} else if ( memcmp( src, "signal_level", 12 ) == 0 ) {
			sscanf( value, "%d", &wpa_info.signal_level );
		}

		src = next;
	}

	return DXWIFI_SUCCESS;
}

static dxWIFI_RET_CODE
wpa_ctrl_get_status( void )
{
	struct wpa_ctrl *ctrl;
	char buf[ WPA_BUF ];
	int ret;

	ctrl = wpa_ctrl_open( WPA_CTRL_WLAN0 );
	if ( !ctrl ) {
		dbg_warm( "Could not get ctrl interface!\n" );
		return DXWIFI_NOTFOUND;
	}

	ret = wpa_ctrl_get( ctrl, "STATUS", buf, WPA_BUF );
	wpa_ctrl_close( ctrl );

	if ( ret <= 0 ) {
		return DXWIFI_ERROR;
	} else {
		pthread_mutex_lock( &mutex );
		ret	= wpa_ctrl_parser_status( buf );
		pthread_mutex_unlock( &mutex );

        return ret;
	}
}

static void
wpa_ctrl_scan_proc( void *arg )
{
	wpa_scan_ctx		*ctx = ( wpa_scan_ctx * )arg;

	if ( ctx && ctx->ScanResultHdl ) {
		struct wpa_ctrl		*ctrl;
		WpaScanResultObj_t	WpaScanResultObj = { 0, };

		char buf[ WPA_BUF ] = { 0, };

		WpaScanResultObj.userData			= ctx->user_data;

		ctrl = wpa_ctrl_open( WPA_CTRL_WLAN0 );
		if ( !ctrl ) {
			dbg_warm( "Could not get ctrl interface!\n" );
		} else {
			int wait;

			for ( wait = 0; wait < 20; wait ++ ) {
				if ( wpa_ctrl_get( ctrl, "STATUS", buf, WPA_BUF ) > 0 ) {
					pthread_mutex_lock( &mutex );
					wpa_ctrl_parser_status( buf );
					pthread_mutex_unlock( &mutex );

					if ( wpa_info.status == WPA_STATUS_SCANNING ) {
						break;
					}
				} else {
					break;
				}

				usleep( 100000 );
			}
		}

		if ( wpa_info.status == WPA_STATUS_SCANNING ) {
			if( wpa_ctrl_get( ctrl, "SCAN_RESULTS", buf, WPA_BUF ) > 0 ) {
				WpaScanResultObj.ScanResultSize		= WPA_BUF;
				WpaScanResultObj.pScanResultBuff	= buf;
			}
		}

		wpa_ctrl_close( ctrl );

		ctx->ScanResultHdl( &WpaScanResultObj );

		dxMemFree( ctx );
	} else {
		dbg_warm( "Something Wrong!!! arg MUST NOT BE NULL!!!" );
	}
}

//==========================================================================
// APIs
//==========================================================================
dxWIFI_RET_CODE
WPACtrl_UpdateStatus( void )
{
    return wpa_ctrl_get_status();
}

dxWIFI_RET_CODE
WPACtrl_IsReady( void )
{
    WPA_STATUS status;

	pthread_mutex_lock( &mutex );
	if ( wpa_info.status == WPA_STATUS_COMPLETED ) {
		status	= wpa_info.status;
	} else {
		status	= WPA_STATUS_ERROR;
	}
	pthread_mutex_unlock( &mutex );

	if ( status == WPA_STATUS_COMPLETED ) {
		return DXWIFI_SUCCESS;
	} else {
		return DXWIFI_ERROR;
	}
}

dxWiFi_Mode_t
WPACtrl_GetMode( void )
{
    dxWiFi_Mode_t mode;

	pthread_mutex_lock( &mutex );
	if ( wpa_info.status == WPA_STATUS_COMPLETED ) {
		mode	= wpa_info.mode;
	} else {
		mode	= DXWIFI_MODE_NONE;
	}
	pthread_mutex_unlock( &mutex );

	return mode;
}


dxWIFI_RET_CODE
WPACtrl_GetSignalLevel( int *rssi )
{
	if ( rssi == NULL ) {
		return DXWIFI_BADARG;
	} else {
		pthread_mutex_lock( &mutex );
		if ( wpa_info.status == WPA_STATUS_COMPLETED ) {
			*rssi = ( wpa_info.signal_level / 2 ) - 99;
			dbg_warm( "signal_level: %d", wpa_info.signal_level );
		} else {
			*rssi = 0;
		}
		pthread_mutex_unlock( &mutex );

		return DXWIFI_SUCCESS;
	}
}

dxWIFI_RET_CODE
WPACtrl_Scan( dxEventHandler_t ScanResultHdl, void *user_data )
{
	struct wpa_ctrl *ctrl;

	ctrl = wpa_ctrl_open( WPA_CTRL_WLAN0 );
	if ( !ctrl ) {
		dbg_warm( "Could not get ctrl interface!\n" );
		return DXWIFI_NOTFOUND;
	} else {
		int ret = wpa_ctrl_set( ctrl, "SCAN" );

		wpa_ctrl_close( ctrl );

		if ( ret == DXWIFI_SUCCESS ) {
		    wpa_scan_ctx		*ctx = dxMemAlloc( "", 1, sizeof( wpa_scan_ctx ) );

			ctx->ScanResultHdl	= ScanResultHdl;
			ctx->user_data		= user_data;

		    if ( dxWorkerTaskSendAsynchEvent( &ScanResultWorkerThread, wpa_ctrl_scan_proc, ctx ) != DXOS_SUCCESS ) {
		        dbg_warm( "FATAL - wpa_ctrl_scan_results Failed to send Event message back to Application\r\n" );
		        dxMemFree( ctx );
				return DXWIFI_ERROR;
		    }
		}

		return ret;
	}
}

dxWIFI_RET_CODE
WPACtrl_Disconnect( void )
{
	struct wpa_ctrl *ctrl;
	int ret;

	ctrl = wpa_ctrl_open( WPA_CTRL_WLAN0 );
	if ( !ctrl ) {
		dbg_warm( "Could not get ctrl interface!\n" );
		return DXWIFI_NOTFOUND;
	}

	ret = wpa_ctrl_set( ctrl, "DISCONNECT" );
	wpa_ctrl_close( ctrl );

	return ret;
}

dxWIFI_RET_CODE
WPACtrl_Connect( char *ssid, dxWiFi_Security_t security_type, char *password, int key_id )
{
	struct wpa_ctrl *ctrl;
	int ret = 0;

	if ( ssid == NULL ) {
		return DXWIFI_BADARG;
	}

	if ( wpa_info.status == WPA_STATUS_DISCONNECTED ) {
		system( "ifconfig wlan0 0.0.0.0" );
	}

	ctrl = wpa_ctrl_open( WPA_CTRL_WLAN0 );
	if ( !ctrl ) {
		dbg_warm( "Could not get ctrl interface!\n" );
		return DXWIFI_NOTFOUND;
	}

	ret = wpa_ctel_setup( ctrl, ssid, security_type, password, key_id );
	wpa_ctrl_close( ctrl );

	if ( ret != DXWIFI_SUCCESS ) {
		return ret;
	}

	return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE
WPACtrl_Start( void )
{
	system( "ifconfig wlan0 0.0.0.0 down" );
	sleep( 1 );

	system( "ifconfig wlan0 up" );
	sleep( 1 );

	system( "killall -9 wpa_supplicant" );
	sleep( 1 );

	system( "/bin/wpa_supplicant -B -Dwext -iwlan0 -c/var/run/wpa.conf" );

	if ( dxWorkerTaskCreate("WPACTL W", &ScanResultWorkerThread, SCAN_RESULT_DEFAULT_WORKER_PRIORITY, SCAN_RESULT_WORKER_THREAD_STACK_SIZE, SCAN_RESULT_WORKER_THREAD_MAX_EVENT_QUEUE ) != DXOS_SUCCESS )         {
		G_SYS_DBG_LOG_V(  "wpa_ctrl_scan_results - Failed to create worker thread\r\n" );
		return DXWIFI_ERROR;
	}

	GSysDebugger_RegisterForThreadDiagnosticGivenName( &ScanResultWorkerThread.WTask, "wpa_ctrl_scan_results Event Thread" );

	return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE
WPACtrl_GenConf( dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv )
{
	FILE *fdw;

	if ( mode != DXWIFI_MODE_STA ) {
		return DXWIFI_NOTSTA;
	}

	fdw = fopen( WPA_CFG, "w" );
	if ( fdw == NULL ) {
		return DXWIFI_ERROR;
	}

	switch ( ch ) {
		case DXWIFI_CHANNEL_PLAN_KOREA:
			fprintf( fdw,"country=%s\n", "KR" );
			break;
		case DXWIFI_CHANNEL_PLAN_EU:
			fprintf( fdw,"country=%s\n", "DE" );
			break;
		case DXWIFI_CHANNEL_PLAN_NORTH_AMERICA:
			fprintf( fdw,"country=%s\n", "US" );
			break;
		case DXWIFI_CHANNEL_PLAN_JAPAN:
			fprintf( fdw,"country=%s\n", "JP" );
			break;
		default:
			fprintf( fdw,"country=%s\n", "JP" );
			break;
	}

	fprintf( fdw,"ctrl_interface=%s\n", WPA_CTRL );
	fprintf( fdw,"update_config=1\n" );
	fclose( fdw );

	return DXWIFI_SUCCESS;
}

dxWiFi_Security_t
WPACtrl_ConvertSecurity( char *flag )
{
	dxWiFi_Security_t st = DXWIFI_SECURITY_UNKNOWN;

	if ( strstr( flag, "WEP" ) != NULL ) {
		st = DXWIFI_SECURITY_WEP_PSK;
	} else if ( strstr(flag, "WPA2-PSK-TKIP" ) != NULL ) {
		st = DXWIFI_SECURITY_WPA2_TKIP_PSK;
	} else if ( strstr( flag, "WPA-PSK-TKIP" ) != NULL ) {
        st = DXWIFI_SECURITY_WPA_TKIP_PSK;
	} else if ( strstr( flag, "WPA2-PSK-CCMP" ) != NULL ) {
        st = DXWIFI_SECURITY_WPA2_AES_PSK;
	} else if ( strstr( flag, "WPA-PSK-CCMP" ) != NULL ) {
        st = DXWIFI_SECURITY_WPA_AES_PSK;
	} else if ( strstr( flag, "WPA2-PSK-TKIP+CCMP" ) != NULL ) {
		st = DXWIFI_SECURITY_WPA2_MIXED_PSK;
		if ( strstr( flag, "WPA-PSK-TKIP+CCMP" ) != NULL ) {
			st = DXWIFI_SECURITY_WPA_WPA2_MIXED;
		}
	} else if ( ( strlen( flag ) == 0 ) ) {
		st = DXWIFI_SECURITY_OPEN;
	} else if ( ( !strncmp( flag, "[ESS]", 5 ) ) && ( strlen( flag ) == 5 ) ) {
		st = DXWIFI_SECURITY_OPEN;
	} else if ( ( !strncmp( flag, "[WPS][ESS]", 10 ) ) && ( strlen( flag ) == 10 ) ) {
		st = DXWIFI_SECURITY_WPS_OPEN;
	}

	return st;
}

/*
int main(int argc, char** argv)
{
	struct wpa_ctrl *ctrl;
	int ret;

	ctrl = wpa_ctrl_open(WPA_CTRL_WLAN0);
	if (!ctrl){
		printf("Could not get ctrl interface!\n");
		return -1;
	}

	ret = wpa_ctrl_command(ctrl,"INTERFACES");
	ret = wpa_ctrl_command(ctrl,"SCAN");
	ret = wpa_ctrl_command(ctrl,"SCAN_RESULTS");
	ret = wpa_ctrl_command(ctrl,"STATUS");

        wpa_ctrl_close(ctrl);

	return 0;
}
*/
