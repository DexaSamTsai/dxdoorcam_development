//============================================================================
// File: dxWIFI.c
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dxOS.h"
#include "dxWIFI.h"
#include "dxWPACtrl.h"
#include "Utils.h"

//============================================================================
// Defines and Macro
//============================================================================
#define CONFIG_USE_DEXATEK_WIFI_SCAN                0   // 0 to use RTK provided wifi_scan(), 1 to use DEXATEK
#define DX_MAX_SCAN_AP_NUMBER                       64  // The value is the same as max_ap_size that defines in wifi_conf.c
# ifndef dbg_y
#	define dbg_y( fmt, ... )		printf( "\033[1;33m"fmt"\033[0;39m", ##__VA_ARGS__ )
# endif

//============================================================================
// WiFi Management
//============================================================================

static dxWiFi_Scan_Result_Handler_t	dxWiFi_Scan_Results_Handler = NULL;
static dxWiFi_Scan_State_t			dxWiFiScanState = DXWIFI_SCAN_STATE_NONE;

#define SCAN_RESULT_WORKING_BUFF_SIZE   128

static void wpa_scan_result_handler( void *arg )
{
	if ( dxWiFi_Scan_Results_Handler ) {
		WpaScanResultObj_t		*pWpaScanResultHdl = ( WpaScanResultObj_t * )arg;
		dxWiFi_Scan_Result_t	dxScan_Result;

		if ( pWpaScanResultHdl && pWpaScanResultHdl->pScanResultBuff ) {
			//bssid / frequency / signal level / flags / ssid
			int frequency, signal;
			char *bssid	= ( char * )dxScan_Result.ap_details.BSSID.octet;
			char *ssid	= ( char * )dxScan_Result.ap_details.SSID.val;
			char flags[ SCAN_RESULT_WORKING_BUFF_SIZE ];
			char *p = NULL, *temp = NULL;
			int cnt = 0;

			memset( &dxScan_Result, 0x00, sizeof( dxScan_Result ) );

			dbg_y( "======================================================\n" );
			dbg_y( "Wifi AP List:\n" );
			p = strtok_r( pWpaScanResultHdl->pScanResultBuff, "\n", &temp );
			while ( ( p = strtok_r( NULL, "\n", &temp ) ) != NULL ) {
				cnt = sscanf( p, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx\t%d\t%d\t%127s\t%63[^\n]",
									&bssid[ 0 ], &bssid[ 1 ], &bssid[ 2 ], &bssid[ 3 ], &bssid[ 4 ], &bssid[ 5 ],
									&frequency, &signal, flags, ssid );

				if ( cnt != 10 ) {
					continue;
				}

				dxScan_Result.ap_details.SSID.len			= strlen( ssid );

				dxScan_Result.ap_details.signal_strength	= signal;
				dxScan_Result.ap_details.bss_type			= DXWIFI_BSS_TYPE_INFRASTRUCTURE;
				dxScan_Result.ap_details.security			= WPACtrl_ConvertSecurity( flags );
				dxScan_Result.ap_details.wps_type			= DXWIFI_WPS_TYPE_DEFAULT;
				dxScan_Result.ap_details.channel			= 0;
				dxScan_Result.ap_details.band				= DXWIFI_802_11_BAND_2_4GHZ ;
				dxScan_Result.scan_complete					= dxFALSE;
				dxScan_Result.user_data						= pWpaScanResultHdl->userData;
				//unsigned char *p = &malloced_scan_result->ap_details.BSSID.octet[0];

				dbg_y( "%s:\t", ssid );
				dbg_y( "%02X:%02X:%02X:%02X:%02X:%02X\t",	( uint8_t )bssid[ 0 ], ( uint8_t )bssid[ 1 ], ( uint8_t )bssid[ 2 ],
															( uint8_t )bssid[ 3 ], ( uint8_t )bssid[ 4 ], ( uint8_t )bssid[ 5 ] );
				dbg_y( "%s\t\n", flags );

				//printf("\r\nbss_type: %d, security: %08X\r\n", malloced_scan_result->ap_details.bss_type, malloced_scan_result->ap_details.security);

				dxWiFi_Scan_Results_Handler( &dxScan_Result );
			}
			dbg_y( "======================================================\n" );
		}

		//Send the scan complete to upper layer
		memset( &dxScan_Result, 0x00, sizeof( dxScan_Result ) );

		dxScan_Result.scan_complete	= dxTRUE;
		dxScan_Result.user_data		= pWpaScanResultHdl->userData;

		dxWiFi_Scan_Results_Handler( &dxScan_Result );
	}

    dxWiFiScanState = DXWIFI_SCAN_STATE_DONE;
}

// Forward declaraion
dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv);

dxWIFI_RET_CODE dxWiFi_Connect( char *ssid,
                                dxWiFi_Security_t security_type,
                                char *password,
                                int key_id,
                                dxSemaphoreHandle_t *dxsemaphore)
{
    return WPACtrl_Connect( ssid, security_type, password, key_id );
}

dxWIFI_RET_CODE dxWiFi_Disconnect(void)
{
    dxWIFI_RET_CODE Return = DXWIFI_SUCCESS;

    int ret =  WPACtrl_Disconnect();

    if (ret != 0)
    {
        Return = DXWIFI_ERROR;
    }

    return Return;

}

dxWIFI_RET_CODE dxWiFi_IsOnline(dxWiFi_Mode_t xMode)
{
	if ( WPACtrl_UpdateStatus() == DXWIFI_SUCCESS ) {
		if ( WPACtrl_GetMode() == xMode ) {
			return WPACtrl_IsReady();
		}
	}
    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_Off(void)
{
    if (WPACtrl_Disconnect() == 0)
        return DXWIFI_SUCCESS;
    else
        return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_On(dxWiFi_Mode_t mode)
{
    return dxWiFi_On_Ext(mode, DXWIFI_CHANNEL_PLAN_DEFAULT, DXWIFI_ADAPTIVITY_DISABLE);
}

dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv)
{
    static dxBOOL bNetIfInit = dxFALSE;

    if (bNetIfInit == dxFALSE) {
	//TODO check IF exist
        bNetIfInit = dxTRUE;
    }

    WPACtrl_GenConf(mode,ch,adv);

    if ( !WPACtrl_Start() )
        return DXWIFI_SUCCESS;
    else
        return DXWIFI_ERROR;
}

dxWiFi_Scan_State_t dxWiFi_GetScanState(void)
{
    dxWiFi_Scan_State_t ret = dxWiFiScanState;

    return ret;
}

dxWIFI_RET_CODE dxWiFi_Scan(dxWiFi_Scan_Result_Handler_t results_handler, void *user_data)
{
	dxWiFi_Scan_Results_Handler = results_handler;

	dxWiFiScanState = DXWIFI_SCAN_STATE_SCANNING;

	if ( WPACtrl_Scan( wpa_scan_result_handler, user_data ) == DXWIFI_SUCCESS ) {
		return DXWIFI_SUCCESS;
	} else {
		dxWiFiScanState = DXWIFI_SCAN_STATE_DONE;

		return DXWIFI_ERROR;
	}
}

dxWIFI_RET_CODE dxWiFi_WaitForOnline(dxWiFi_Mode_t xMode, uint32_t xMSToWait)
{
    uint32_t value = 0;
    dxWIFI_RET_CODE Return = DXWIFI_TIMEOUT;

    while (value < xMSToWait) {
        dxWIFI_RET_CODE ret = dxWiFi_IsOnline(xMode);

        if (ret == DXWIFI_SUCCESS)
        {
            Return = DXWIFI_SUCCESS;
            break;
        }

        dxTimeDelayMS(1000);
        value += 1000;
    }

    return Return;
}

dxWIFI_RET_CODE
dxWiFi_GetRSSI( int *rssi )
{
	return WPACtrl_GetSignalLevel( rssi );
}

