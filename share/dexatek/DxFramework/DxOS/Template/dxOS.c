//============================================================================
// File: dxOS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxOS.h"

//============================================================================
// Defines and Macro
//============================================================================

//============================================================================
// Type Definitions
//============================================================================

//============================================================================
// Main Initialization
//============================================================================
dxOS_RET_CODE dxOS_Init(void)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Memory Management
//============================================================================
size_t dxMemGetFreeHeapSize (void)
{
	// Todo...

    return 0;
}

dxOS_RET_CODE dxMemFree (void* memoryToFree)
{
	// Todo...

    return DXOS_ERROR;
}

void* dxMemAlloc (char* zone, size_t nelems, size_t size)
{
	// Todo...

    return NULL;
}

void* dxMemReAlloc (char* zone, void* memoryToFree, size_t size)
{
	// Todo...

    return NULL;
}


//============================================================================
// Timer Management
//============================================================================
dxTimerHandle_t dxTimerCreate(const char * pcTimerName,
                              const uint32_t dxTimerPeriodInMs,
                              const dxBOOL uxAutoReload,
                              const void * pvTimerID,
                              dxTimerCallbackFunction_t pxCallbackFunction)
{
	// Todo...

    return NULL;
}

void* dxTimerGetTimerID( dxTimerHandle_t dxTimer )
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxTimerStart(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTimerStop(dxTimerHandle_t dxTimer,
                          const uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTimerDelete(dxTimerHandle_t dxTimer,
                         const uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxBOOL dxTimerIsTimerActive(dxTimerHandle_t dxTimer)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTimerReset(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTimerRestart(dxTimerHandle_t dxTimer,
                             const uint32_t dxTimerPeriodInMs,
                             const uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Time Information
//============================================================================
dxOS_RET_CODE dxTimeDelayMS(const uint32_t xMSToDelay)
{
	// Todo...

    return DXOS_ERROR;
}

dxTime_t dxTimeGetMS(void)
{
	// Todo...

    return 0;
}

dxOS_RET_CODE dxHwRtcGet(DKTime_t* TimeRTC)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxHwRtcSet(DKTime_t SetTimeRTC)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxSwRtcGet(DKTime_t* TimeRTC)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxSwRtcSet(DKTime_t SetTimeRTC)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxGetLocalTime(DKTimeLocal_t* localTime)
{
	// Todo...

    return DXOS_ERROR;
}

DKTime_t dxGetUtcTime(void)
{
    DKTime_t TimeRTC;

    memset (&TimeRTC, 0, sizeof (DKTime_t));

	// Todo...

    return TimeRTC;
}


uint32_t dxGetUtcInSec(void)
{
	// Todo...

    return 0;
}


//============================================================================
// Queue Management
//============================================================================
dxQueueHandle_t dxQueueCreate(uint32_t uxQueueLength,
                              uint32_t uxItemSize)
{
	// Todo...

    return NULL;
}

dxQueueHandle_t dxQueueGlobalCreate(void*    GlobalIdentifier,
                                    uint32_t uxQueueLength,
                                    uint32_t uxItemSize)
{
	// Todo...

    return NULL;
}

dxQueueHandle_t dxQueueGlobalGet(void*    GlobalIdentifier)
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxQueueReceive(dxQueueHandle_t dxQueue,
                             void *pvBuffer,
                             uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxQueueReceiveFromISR(dxQueueHandle_t dxQueue,
                                    void *pvBuffer,
                                    dxBOOL* HigherPriorityTaskWoken)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxQueueSend(dxQueueHandle_t dxQueue,
                          const void * pvItemToQueue,
                          uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxQueueSendWithSize(  dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    uint32_t     MessageSizeInByte,
                                    uint32_t dxMsToWait)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxQueueSendFromISR(   dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    dxBOOL* HigherPriorityTaskWoken)
{
	// Todo...

    return DXOS_ERROR;
}

uint32_t dxQueueSpacesAvailable(const dxQueueHandle_t dxQueue)
{
	// Todo...

    return 0;
}

dxOS_RET_CODE dxQueueReset(dxQueueHandle_t dxQueue, dxQueueResetFunction_t dxResetFunction)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxQueueDelete(dxQueueHandle_t dxQueue)
{
	// Todo...

    return DXOS_ERROR;
}

dxBOOL dxQueueIsEmpty(dxQueueHandle_t dxQueue)
{
	// Todo...

    return dxFALSE;
}

dxBOOL dxQueueIsFull(dxQueueHandle_t dxQueue)
{
	// Todo...

    return dxFALSE;
}


//============================================================================
// Task Management
//============================================================================

dxOS_RET_CODE dxTaskCreate(dxTaskFunction_t pvTaskCode,
                           const char * pcName,
                           uint16_t usStackSizeInByte,
                           void *pvParameters,
                           dxTaskPriority_t uxPriority,
                           dxTaskHandle_t *pvCreatedTask)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTaskSuspend(dxTaskHandle_t dxTaskToSuspend)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTaskResume(dxTaskHandle_t dxTaskToResume)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTaskPrioritySet(dxTaskHandle_t dxTask, uint32_t uxNewPriority)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxTaskDelete(dxTaskHandle_t dxTaskToDelete)
{
	// Todo...

    return DXOS_ERROR;
}

dxTaskHandle_t dxTaskGetCurrentTaskHandle(void)
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxWorkerTaskCreate (const char* name, dxWorkweTask_t *pWorkerTask, dxTaskPriority_t uxPriority, uint16_t usStackDepth, uint32_t EventQueueSize )
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxWorkerTaskDelete( dxWorkweTask_t *WorkerTask)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxWorkerTaskSendAsynchEvent( dxWorkweTask_t *WorkerTask, dxEventHandler_t function, void* arg )
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Task Stack - TO BE VERIFIED
//============================================================================
dxOS_RET_CODE dxTaskGetStackInfo(dxTaskHandle_t dxTask, dxTaskStack_t *pStackBuf)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Semaphore Management
//============================================================================
dxSemaphoreHandle_t dxSemCreate(uint32_t uxMaxCount,
                                uint32_t uxInitialCount)
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxSemGive(dxSemaphoreHandle_t dxSemaphore)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxSemTake(dxSemaphoreHandle_t dxSemaphore, uint32_t xBlockTimeInMS)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxSemDelete(dxSemaphoreHandle_t dxSemaphore)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Mutex Management
//============================================================================
dxMutexHandle_t dxMutexCreate(void)
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxMutexTake(dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxMutexGive(dxMutexHandle_t dxMutex)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxMutexDelete(dxMutexHandle_t dxMutex)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// AES Encryption/Decryption
//============================================================================
dxAesContext_t dxAESSW_ContextAlloc(void)
{
	// Todo...

    return NULL;
}


dxAES_RET_CODE dxAESSW_Setkey(dxAesContext_t ctx, const unsigned char *key, unsigned int keysize, dxAES_CRYPTO_TYPE Type)
{
	// Todo...

    return DXOS_ERROR;
}


dxAES_RET_CODE dxAESSW_CryptEcb(dxAesContext_t ctx, dxAES_CRYPTO_TYPE Type, const unsigned char input[16], unsigned char output[16])
{
	// Todo...

    return DXOS_ERROR;
}

dxAES_RET_CODE dxAESSW_ContextDealloc(dxAesContext_t ctx)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// Rand
//============================================================================
uint32_t dxRand()
{
	return 0;
}


//============================================================================
// SPI Access Resource
// NOTE! -  Currently this is used on flash access in BSP as well as
//          IR remote (HybridPeripheral) when accessing other SPI port
//          The reason is becasue in RTK the SPI access is NOT thread safe
//          and there is no common interface for that (eg, Flash access vs Generic access)
//          We should be creating SPI resource access within our BSP so that this does not get messy
//          Need to start first by developing dxSPI interface so that we can easily
//          manage the SPI resource!!!!
//============================================================================
void dxSPIaccess_ResourceTake(void) //WORKAROUND ONLY - See header for detail
{
	// Todo...
}

void dxSPIaccess_ResourceGive(void) //WORKAROUND ONLY - See header for detail
{
	// Todo...
}
