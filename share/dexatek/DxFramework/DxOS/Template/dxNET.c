//============================================================================
// File: dxNET.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxNET.h"

//============================================================================
// Socket Option
//============================================================================
int dxSOCKET_SetTimeout(int s, uint32_t msec)
{
	// Todo...

    return -1;
}


//============================================================================
// DHCP Client
//============================================================================
dxDHCP_STATE dxDHCP_Start(void)
{
	// Todo...

    return DXDHCP_STOP;
}

dxDHCP_STATE dxDHCP_Stop(void)
{
	// Todo...

    return DXDHCP_STOP;
}

dxDHCP_STATE dxDHCP_Release(void)
{
	// Todo...

    return DXDHCP_STOP;
}

dxNet_Addr_t dxDHCP_Info(void)
{
    dxNet_Addr_t dxNetAddr;
	// Todo...

    return dxNetAddr;
}


//============================================================================
// Multicast
//============================================================================
void dxMCAST_Init(void)
{
	// Todo...

}

dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port)
{
	// Todo...

    return NULL;
}

dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle)
{
	// Todo...

    return DXOS_ERROR;
}

int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs)
{
	// Todo...

    return DXOS_ERROR;
}

int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket)
{
	// Todo...

    return DXOS_ERROR;
}
