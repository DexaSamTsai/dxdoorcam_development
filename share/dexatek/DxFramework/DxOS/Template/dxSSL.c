//============================================================================
// File: dxSSL.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxSSL.h"

//============================================================================
// SSL/TLS Client
//============================================================================
void dxSSL_Lock(void)
{
	// Todo...

}

void dxSSL_Unlock(void)
{
	// Todo...

}

dxSSL_RET_CODE dxSSL_Close(dxSSLHandle_t *dxSSL)
{
	// Todo...

    return DXSSL_ERROR;
}

dxSSLHandle_t* dxSSL_Connect(const dxIP_Addr_t* host_ip,
                             uint16_t port,
                             const char* cert,
                             uint8_t endpoint,
                             uint8_t verify)
{
	// Todo...

    return NULL;
}

int dxSSL_Receive(dxSSLHandle_t *dxSSL, void *mem, size_t len)
{
	// Todo...

    return DXSSL_ERROR;
}

int dxSSL_ReceiveTimeout(dxSSLHandle_t *dxSSL, void *mem, size_t len, uint32_t msec)
{
	// Todo...

    return DXSSL_ERROR;
}

int dxSSL_Send(dxSSLHandle_t *dxSSL, const void *dataptr, size_t size)
{
	// Todo...

    return DXSSL_ERROR;
}
