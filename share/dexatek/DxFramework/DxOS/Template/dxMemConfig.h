//============================================================================
// File: dxMemConfig.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DXMEMCONFIG_H
#define _DXMEMCONFIG_H


#define DEFAULT_FLASH_SECTOR_ERASE_SIZE				4096

#define DX_SYSINFO_FLASH_SIZE						16384        // MUST align to a SECTOR of flash, typical 16KB
#define DX_DEVINFO_FLASH_SIZE						16384        // MUST align to a SECTOR of flash, typical 16KB


#define DX_FLASH_SIZE_TOTAL_MBITS					32

#define DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR		0xFB000
#define DX_SYSINFO_FLASH_UPDATE_IMG_SIZE			( 1024 * 960 )

#define DX_DEVINFO_FLASH_ADDR1						0x3EF000

#define DX_SUBIMG_FLASH_ADDR1						( DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR +\
                                                    DX_SYSINFO_FLASH_UPDATE_IMG_SIZE )

#define DX_SUBIMG_FLASH_SIZE						( 1024 * 1024 )
#define DX_HP_STORAGE_ADDR1							( DX_SUBIMG_FLASH_ADDR1 + DX_SUBIMG_FLASH_SIZE )

#define DX_HP_STORAGE_SIZE							( 1024 * 512 )


#define DX_DEVINFO_FLASH_ADDR2						( DX_DEVINFO_FLASH_ADDR1 + DX_DEVINFO_FLASH_SIZE )

#define DX_SYSINFO_FLASH_ADDR1						( DX_DEVINFO_FLASH_ADDR2 + DX_DEVINFO_FLASH_SIZE )

#define DX_SYSINFO_FLASH_ADDR2						( DX_SYSINFO_FLASH_ADDR1 + DX_SYSINFO_FLASH_SIZE )


#endif // _DXMEMCONFIG_H
