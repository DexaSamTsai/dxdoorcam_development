//============================================================================
// File: dxDNS.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#ifndef _DXDNS_H
#define _DXDNS_H

#pragma once

#include "dxNET.h"

dxNET_RET_CODE dxDNS_GetHostByName(uint8_t *hostname, dxIPv4_t *dxip);

#endif // _DXDNS_H
