//============================================================================
// File: dxMD5.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxMD5.h"

void dxMD5(const unsigned char *input, size_t ilen, unsigned char output[16])
{
	// Todo...
}

int dxMD5_Starts( dxMD5_CTX *ctx )
{
	// Todo...

    return -1;
}

int  dxMD5_Update( dxMD5_CTX *ctx, const void *data, size_t len )
{
	// Todo...

    return 0;
}

int dxMD5_Finish( unsigned char *md, dxMD5_CTX *ctx )
{
	// Todo...

    return 0;
}

static const char hex_chars[] = "0123456789abcdef";

void dxMD5_convert_hex(unsigned char *md, unsigned char *mdstr)
{
    int i;
    int j = 0;
    unsigned int c;

    for (i = 0; i < 16; i++) {
        c = (md[i] >> 4) & 0x0f;
        mdstr[j++] = hex_chars[c];
        mdstr[j++] = hex_chars[md[i] & 0x0f];
    }
    mdstr[32] = '\0';
}

