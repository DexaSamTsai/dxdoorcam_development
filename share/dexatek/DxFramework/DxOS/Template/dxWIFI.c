//============================================================================
// File: dxWIFI.c
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxOS.h"
#include "dxWIFI.h"
#include "Utils.h"

//============================================================================
// Defines and Macro
//============================================================================


//============================================================================
// WiFi Management
//============================================================================
dxWIFI_RET_CODE dxWiFi_Connect( char *ssid,
                                dxWiFi_Security_t security_type,
                                char *password,
                                int key_id,
                                dxSemaphoreHandle_t *dxsemaphore)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_Disconnect(void)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_GetMAC(dxWiFi_MAC_t *pMAC)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_IsOnline(dxWiFi_Mode_t xMode)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_Off(void)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_On(dxWiFi_Mode_t mode)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv)
{
	// Todo...

    return DXWIFI_ERROR;
}

void dxWiFi_Scan_Result_Init(void)
{
	// Todo...

}

void dxWiFi_Scan_Result_Set(int nIndex, dxWiFi_Scan_Result_t *result)
{
	// Todo...

}

dxBOOL dxWiFi_Scan_Result_Update(dxWiFi_Scan_Result_t *result_ptr)
{
	// Todo...

    return dxFALSE;
}

dxWiFi_Scan_State_t dxWiFi_GetScanState(void)
{
    return DXWIFI_SCAN_STATE_NONE;
}

dxWIFI_RET_CODE dxWiFi_Scan(dxWiFi_Scan_Result_Handler_t results_handler, void *user_data)
{
	// Todo...

    return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_WaitForOnline(dxWiFi_Mode_t xMode, uint32_t xMSToWait)
{
	// Todo...

    return DXWIFI_ERROR;
}

