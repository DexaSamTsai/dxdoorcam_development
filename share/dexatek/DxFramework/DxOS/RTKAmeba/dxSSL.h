//============================================================================
// File: dxSSL.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#ifndef _DXSSL_H
#define _DXSSL_H

#pragma once

#include "dxOS.h"

//============================================================================
// PolarSSL include files. PolarSSL v1.3.8 or later
//
// Note: Need to modify while porting to others
//============================================================================
#include "polarssl/config.h"
#include "polarssl/net.h"
#include "polarssl/ssl.h"
#include "polarssl/error.h"
#include "polarssl/memory.h"


//============================================================================
// SSL/TLS Client
//============================================================================

#define DXSSL_CLIENT                    0
#define DXSSL_VERIFY_NONE               0

typedef ssl_context dxSSLContext;

typedef struct dxSSLHandle {
    int         	sfd;
    dxSSLContext 	ssl;
    x509_crt*       ca_crt;
} dxSSLHandle_t;

typedef enum {

    DXSSL_SUCCESS = 0,

    DXSSL_ERROR   = -1,

} dxSSL_RET_CODE;



void dxSSL_Lock(void);


void dxSSL_Unlock(void);



/**
 * @return DXSSL_SUCCESS    if SSL/TLS handle was terminated and freed sucessfully
 *         DXSSL_ERROR      otherwise
 */
dxSSL_RET_CODE dxSSL_Close(dxSSLHandle_t *dxSSL);

/**
 * @return dxSSLHandle_t *  if SSL/TLS handle was initialized successfully
 *         NULL             otherwise
 */
dxSSLHandle_t* dxSSL_Connect (
    const dxIP_Addr_t* host_ip,
    uint16_t port,
    const char* cert,
    uint8_t endpoint,
    uint8_t verify
);

/**
 * @return > 0              number of bytes received through TLS/SSL channel
 *         DXSSL_SUCCESS    if SSL/TLS handle was terminated and freed sucessfully
 *         DXSSL_ERROR      otherwise
 */
int dxSSL_Receive(dxSSLHandle_t *dxSSL, void *mem, size_t len);

int dxSSL_ReceiveTimeout(dxSSLHandle_t *dxSSL, void *mem, size_t len, uint32_t msec);

/**
 * @return > 0              number of bytes written through TLS/SSL channel
 *         DXSSL_SUCCESS    if SSL/TLS handle was terminated and freed sucessfully
 *         DXSSL_ERROR      otherwise
 */
int dxSSL_Send(dxSSLHandle_t *dxSSL, const void *dataptr, size_t size);

#endif // _DXSSL_H
