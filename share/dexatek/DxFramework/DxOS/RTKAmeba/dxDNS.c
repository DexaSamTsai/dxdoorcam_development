//============================================================================
// File: dxDNS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include "dxDNS.h"

//============================================================================
// DNS Client
//============================================================================
int dxDNS_GetHostByName(uint8_t *hostname, dxIPv4_t *dxip)
{
    int err = 0;

    ip_addr_t ipaddr;

    if (dxip == NULL) {
        return DXNET_ERROR;
    }

    memset(&ipaddr, 0x00, sizeof(ipaddr));

    err = netconn_gethostbyname(hostname, &ipaddr);
    // ERR_OK: resolving succeeded
    // ERR_MEM: memory error, try again later
    // ERR_ARG: dns client not initialized or invalid hostname
    // ERR_VAL: dns server response was invalid
    // For more detail err code, please refer to 'api_lib.c' located in
    // 'component\common\network\lwip\lwip_v1.4.1\src\api'

    if (err != ERR_OK) {
        return DXNET_ERROR;
    }

    memcpy(&dxip->ip_addr[0], &ipaddr.addr, sizeof(dxip->ip_addr));

    return DXNET_SUCCESS;

/**
 * Example code to enumerate IPv4 and IPv6
    struct addrinfo *addr_list;
    struct addrinfo *cur;
    struct addrinfo *hints;

    memset(hints, 0x00, sizeof(struct addrinfo));

    hints->ai_family = AF_UNSPEC;
    hints->ai_socktype = SOCK_STREAM;
    hints->ai_protocol = IPPROTO_TCP;
    hints->ai_flags = 0x01;

    ret = lwip_getaddrinfo((const char *)hostname, NULL, NULL, &addr_list);

    if (ret == 0) {
        cur = addr_list;
        while (cur) {
            if (cur->ai_addr->sa_family == AF_INET) {
                struct sockaddr_in *addr = (struct sockaddr_in *)cur->ai_addr;
                printf("\r\nsin_len: %d, sin_family: %d. sin_port: %d\n",
                       addr->sin_len,
                       addr->sin_family,
                       addr->sin_port);

                unsigned char *p = (unsigned char *)&addr->sin_addr.s_addr;

                printf("\r\n%d.%d.%d.%d\r\n", p[0], p[1], p[2], p[3]);

            }
            // Need ENABLE LWIP_IPV6
#if LWIP_IPV6
            else if (cur->ai_addr->sa_family == AF_INET6) {
            }
#endif
            cur = cur->ai_next;
        }
    }
 */
}
