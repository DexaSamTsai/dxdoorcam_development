//============================================================================
// File: dxNET.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#ifndef _DXNET_H
#define _DXNET_H

#pragma once
//============================================================================
// lwIP include files. lwIP v1.4.1 or later
//
// Note: Need to modify while porting to others
//============================================================================
#include "lwip_netconf.h"
#include "lwip/ip_addr.h"
#include "lwip/sockets.h"
#include "lwip/netdb.h"

//============================================================================
// NET Client
//============================================================================

typedef struct dxIPv4 {
    uint8_t ip_addr[4];
} dxIPv4_t;

typedef enum {
    DXIP_VERSION_V4         = (1 << 0),
    DXIP_VERSION_V6         = (1 << 1),     // NOT SUPPORT. Need LwIP 1.5.0
    DXIP_VERSION_BOTH       = (DXIP_VERSION_V4 | DXIP_VERSION_V6),
} dxIP_Version_t;

typedef struct dxIP_Addr {
    dxIP_Version_t version;
    uint8_t v4[4];
    uint16_t v6[8];
} dxIP_Addr_t;

typedef struct dxNet_Addr {
    uint8_t mac_addr[6];
    dxIP_Addr_t ip_addr;
    dxIP_Addr_t netmask;
    dxIP_Addr_t gw;
} dxNet_Addr_t;

typedef enum {

    DXNET_SUCCESS = 0,
    DXNET_ERROR   = -1,
    DXNET_CONN_CLOSED = -2

} dxNET_RET_CODE;

int dxSOCKET_SetTimeout(int s, uint32_t msec);

//============================================================================
// DHCP Client
//============================================================================

typedef enum {
    DXDHCP_START            = 0,
    DXDHCP_WAIT_ADDRESS,
    DXDHCP_ADDRESS_ASSIGNED,
    DXDHCP_RELEASE_IP,
    DXDHCP_STOP,
    DXDHCP_TIMEOUT
} dxDHCP_STATE;

/**
 * @brief This is a blocking function, will try obtaining IP address or timeout is reached
 * @return dxDHCP_STATE
 */
dxDHCP_STATE dxDHCP_Start(void);

/**
 * @return dxDHCP_STATE
 */
dxDHCP_STATE dxDHCP_Stop(void);

/**
 * @return dxDHCP_STATE
 */
dxDHCP_STATE dxDHCP_Release(void);


/**
 * @return dxNet_Addr_t
 */
dxNet_Addr_t dxDHCP_Info(void);


#endif // _DXNET_H
