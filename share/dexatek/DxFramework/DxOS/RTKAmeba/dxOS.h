//============================================================================
// File: dxOS.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/12/12
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/31
//     1) Description: Added two functions
//        Added: dxWiFi_IsOnline()
//               dxWiFi_WaitForOnline()
//
//     Version: 0.3
//     Date: 2016/01/19
//     1) Description: Revise all API's parameters and return code
//
//     Version: 0.4
//     Date: 2016/01/19
//     1) Description: Added a new API
//        Added:
//            - dxQueueIsEmpty()
//
//     Version: 0.5
//     Date: 2016/01/20
//     1) Description: Revise API's return code
//        Modified:
//            - All APIs list in DHCP Client, DNS Client, TCPIP Client/Server,
//              UDP Client/Server and Multicast UDP section
//
//     Version: 0.6
//     Date: 2016/01/20
//     1) Description: Added new API
//        Added:
//            - dxQueueIsFull()
//            - dxWiFi_GetMAC()
//            - dxTimerReset()
//            - dxTimerRestart()
//
//     Version: 0.7
//     Date: 2016/01/20
//     1) Description: Move dxNetIf_Init() into dxWiFi_on()
//        Deleted:
//            - dxNetIf_Init()
//        Modified:
//            - dxWiFi_on()
//
//     Version: 0.8
//     Date: 2016/01/21
//     1) Description: Declare a new data type, dxTime_t
//        Modified: Change function's return data type to dxTime_t
//            - dxTimeGetMS()
//
//     Version: 0.7
//     Date: 2016/01/26
//     1) Description: Revise function prototype
//        Modified:
//            - dxSSL_Connect()
//
//     Version: 0.8
//     Date: 2016/02/01
//     1) Description: Revise function prototype
//        Modified:
//            - dxTimerRestart()
//
//     Version: 0.9
//     Date: 2016/02/15
//     1) Description: Move dxFlash_XXXX() and dxOTA_YYYY() to dxBSP
//
//     Version: 0.10
//     Date : 2016/3/25
//     1) Description:  Rock : Add dxMCASTxxxEX() api
//
//     Version: 0.11
//     Date: 2016/06/27
//     1) Description: Rivise Multicast/UDP API and added related handle, struct
//        Modified:
//            - dxMCAST_Init()
//            - dxMCAST_Join()
//            - dxMCAST_Leave()
//            - dxMCAST_RecvFrom()
//            - dxMCAST_SendTo()
//
//     Version: 0.12
//     Date: 2016/07/06
//     1) Description: Added semaphore to protect RTK hardware crypto engine
//        Added:
//            - dxOS_Init()
//     Version: 0.13
//     Date: 2016/08/10
//     1) Description: Added SW AES
//        Added:
//            - dxAESSW_ContextAlloc()
//            - dxAESSW_Setkey()
//            - dxAESSW_CryptEcb()
//            - dxAESSW_ContextDealloc()
//
//     Version: 0.24
//     Date: 2017/03/27
//     1) Description: Added new error code to dxOS_RET_CODE
//        Added:
//            - DXOS_INVALID
//
//     Version: 0.25
//     Date: 2017/04/10
//     1) Description: Support new WiFi scan function
//        Added:
//            - dxWiFi_Scan_State_t
//            - dxWiFi_GetScanState()
//
//     Version: 0.26
//     Date: 2017/05/03
//     1) Description: Use dxBOOL instead of BOOL
//        Modified:
//            - dxWiFi_Scan_Result_t
//
//     Version: 0.27
//     Date: 2017/07/19
//     1) Description: Revise Wi-Fi API to support initialize Wi-Fi driver with
//                     channel plan and adaptivity
//        Added:
//            - dxWiFi_Adaptivity_t, dxWiFi_Channel_Plan_t
//            - dxWiFi_On_Ext()
//============================================================================
#ifndef _DXOS_H
#define _DXOS_H

#pragma once

//============================================================================
//    Include
//============================================================================

/*---- Compiler/IAR include files ------------------------------------------*/

#include <stdlib.h>
#include <stdint.h>
#include <basic_types.h>
#include "ProjectCommonConfig.h"
#include "dxArch.h"


/*---- Hardware/Platform include files -------------------------------------*/

#include <platform/platform_stdlib.h>
#include "wifi_conf.h"


/*---- FreeRTOS include files ----------------------------------------------*/

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "timers.h"
#include "event_groups.h"
#include "StackMacros.h"


/*---- Flash access include file -------------------------------------------*/

#include "flash_api.h"
#include "hal_api.h"


/*---- lwip access include file -------------------------------------------*/

#include "lwip/sockets.h"


/*---- Dexatek include file ------------------------------------------------*/

#include "dxNET.h"
#include "dxSSL.h"



//============================================================================
// Global defines
//============================================================================

#define DK_OS_WRAPPER                                               1
#define DXOS_WAIT_FOREVER                                           0xFFFFFFFF
#define DX_TIMER_PERIOD_NO_CHANGE                                   0

/**
 * NOTE: The following two definitions are use as the first parameter of
 * dxMemAlloc and dxMemReAlloc, if the parameter is not match then the default
 * memory zone (FAST) will be used.
 */
#define DK_MEM_ZONE_FAST                        "@FAST"
#define DK_MEM_ZONE_SLOW                        "@SLOW"
#define DX_SDRAM_HEAP_SIZE                      0x100000  // 1M


// Set default value
#ifdef DK_MEMORY_DEBUG
#define DK_MEM_MAGIC_NUMBER 0xefdcba98
#ifndef DK_MEMORY_DEBUG_INTERVAL
#define DK_MEMORY_DEBUG_INTERVAL                (30 * SECONDS)
#endif
#ifndef DK_MEMORY_DEBUG_RECORD_NUM
#define DK_MEMORY_DEBUG_RECORD_NUM              128
#endif
#ifndef DK_MEMORY_DEBUG_TIMEDIFF_MAX
#define DK_MEMORY_DEBUG_TIMEDIFF_MAX            (9999999U * SECONDS)
#endif
#endif



//============================================================================
// Macros
//============================================================================

#define calloc(b,c)         ((void *)RtlZmalloc(((b) * (c))))
#define assert(x)           configASSERT((x))


/**
 * Calculate the align boundary
 */
#define align(x, a)         \
    __align_mask(x, (size_t)(a) - 1)
#define __align_mask(x, mask) (((x) + (mask)) & ~(mask))


/**
 * These inlines deal with timer wrapping correctly. You are
 * strongly encouraged to use them
 * 1. Because people otherwise forget
 * 2. Because if the timer wrap changes in future you won't have to
 *    alter your driver code.
 *
 * time_after(a,b) returns true if the time a is after time b.
 *
 * Do this with "<0" and ">=0" to only test the sign of the result. A
 * good compiler would generate better code (and a really good compiler
 * wouldn't care). Gcc is currently neither.
 */
#define time_after(a,b)		\
	 ((int32_t)(b) - (int32_t)(a) < 0)
#define time_before(a,b)	time_after(b,a)

#define time_after_eq(a,b)	\
	 ((int32_t)(a) - (int32_t)(b) >= 0)
#define time_before_eq(a,b)	time_after_eq(b,a)



//============================================================================
// Type Definition
//============================================================================

typedef enum {

    DXOS_SUCCESS = 0,

    DXOS_ERROR  =       -1,
    DXOS_TIMEOUT =      -2,
    DXOS_ABORTED =      -3,
    DXOS_INVALID =      -4,

    //Resource
    DXOS_NO_MEM =        -10,
    DXOS_LIMIT_REACHED = -11,

} dxOS_RET_CODE;

typedef enum {
    dxFALSE = 0,
    dxTRUE = 1
} dxBOOL;

typedef uint32_t    dxTime_t;

typedef struct _tagDKTime
{
    uint16_t        year;
    uint8_t         month;
    uint8_t         day;
    uint8_t         hour;
    uint8_t         min;
    uint8_t         sec;
    uint8_t         dayofweek;
    int8_t          TimeZone;   //1 unit = 15min
} DKTime_t;

typedef struct _tagDKTimeLocal
{
    uint16_t        year;
    uint8_t         month;
    uint8_t         day;
    uint8_t         hour;
    uint8_t         min;
    uint8_t         sec;
    uint8_t         dayofweek;
} DKTimeLocal_t;

typedef void * dxTimerHandle_t;
typedef void (*dxTimerCallbackFunction_t)(dxTimerHandle_t dkTimer);

#ifdef DK_MEMORY_DEBUG
typedef struct {
    ListItem_t      item;          // for list
    TaskHandle_t*   current;       // the caller thread
    dxTime_t        time;          // the time when memory is allocated
    uint16_t        line;          // the line number when memory is allocated
    uint16_t        size;          // the size of memory
    uint8_t         used;          // the memory is allocate:1, reallocate:2 or free: 0
    uint8_t         overflow;      // check if time is overflow
    void*           memory;        // the pointer of memory
    char            file[64];      // the name of file when memory is allocated
    char            function[64];  // the name of function when memory is allocated
} MemoryRecord;
#endif


typedef void (*dxQueueResetFunction_t)(void *pvBuffer);



//============================================================================
// Main Initialization
//============================================================================

/**
 * @return DXOS_SUCCESS success
 *         DXOS_ERROR   fail
 */
dxOS_RET_CODE dxOS_Init(void);



//============================================================================
// Memory Management
//============================================================================

#ifndef realloc_named
#define realloc_named(n,p,s)                        dxMemReAlloc(n, p,s)
#endif // realloc_named


size_t dxMemGetFreeHeapSize(void);


#ifdef DK_MEMORY_DEBUG

/**
 * The internal structure for memory allocation:
 *
 *            +------------------------+
 *            | magic number (4 bytes) |
 *            +------------------------+
 *            |       user data        |
 * return --> +------------------------+
 *            |     size (4 bytes)     |
 * memory --> +------------------------+
 *
 */

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxMemFreeDbg (
	const char*     file,
	const char*     function,
	const int       line,
	void*           memoryToFree
);


/**
 * @note the maximum size of heap memory can be allocated will be around
 *       144 x 1024 bytes while 8177AM HomeKit started.
 */
void* dxMemAllocDbg (
	const char*     file,
	const char*     function,
	const int       line,
	char*           name,
	size_t          nelems,
	size_t          size
);


/**
 * @return NOT NULL     a pointer to the allocated space.
 *         NULL         otherwise
 */
void* dxMemReAllocDbg (
	const char*     file,
	const char*     function,
	const int       line,
	char*           name,
	void*           memoryToFree,
	size_t          size
);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxMemGetRecords (
    List_t**        list,
    uint16_t*       size,
    uint8_t*        needMore
);


#define dxMemFree(memoryToFree) \
	dxMemFreeDbg(__FILE__, __FUNCTION__, __LINE__, memoryToFree)

#define dxMemAlloc(name, nelems, size) \
	dxMemAllocDbg(__FILE__, __FUNCTION__, __LINE__, name, nelems, size)

#define dxMemReAlloc(name, memoryToFree, size) \
	dxMemReAllocDbg(__FILE__, __FUNCTION__, __LINE__, name, memoryToFree, size)

#else

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxMemFree(void* memoryToFree);


/**
 * @note the maximum size of heap memory can be allocated will be around
 *       144 x 1024 bytes while 8177AM HomeKit started.
 */
void *dxMemAlloc(char* name, size_t nelems, size_t size);


/**
 * @return NOT NULL     a pointer to the allocated space.
 *         NULL         otherwise
 */
void *dxMemReAlloc(char* name, void* memoryToFree, size_t size);

#endif



//============================================================================
// Timer Management
//============================================================================

/**
 * @return dxTimerHandle_t  a handle to the timer.
 *         NULL             otherwise
 */
dxTimerHandle_t dxTimerCreate(const char * pcTimerName,
                              const uint32_t dxTimerPeriodInMs,
                              const dxBOOL uxAutoReload,
                              const void * pvTimerID,
                              dxTimerCallbackFunction_t pxCallbackFunction);

/**
 * @return The ID assigned to the timer being queried.
 *
 */
void* dxTimerGetTimerID( dxTimerHandle_t dxTimer );

/**
 * @return DXOS_SUCCESS timer is started
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTimerStart(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait);

/**
 * @return DXOS_SUCCESS timer is stopped
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTimerStop(dxTimerHandle_t dxTimer,
                          const uint32_t dxMsToWait);

/**
 * @return DXOS_SUCCESS timer is deleted
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTimerDelete(dxTimerHandle_t dxTimer,
                            const uint32_t dxMsToWait);

/**
 * @return dxTRUE       timer is actived
 *         dxFALSE      otherwise
 */
dxBOOL dxTimerIsTimerActive(dxTimerHandle_t dxTimer);

/**
 * @return DXOS_SUCCESS timer is reset
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTimerReset(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait);

/**
 * @brief the timer will be resset before starting, use DX_TIMER_PERIOD_NO_CHANGE if timer period remains the same
 *
 * @return DXOS_SUCCESS timer is restarted
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTimerRestart(dxTimerHandle_t dxTimer,
                             const uint32_t dxTimerPeriodInMs,
                             const uint32_t dxMsToWait);

//============================================================================
// Time Information
//============================================================================

#define MILLISECONDS      (1)
#define SECONDS           (1000)
#define MINUTES           (60 * SECONDS)
#define HOURS             (60 * MINUTES)
#define DAYS              (24 * HOURS)

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxTimeDelayMS(const uint32_t xMSToDelay);

/**
 * @return milliseconds since system start
 */
dxTime_t dxTimeGetMS(void);

/** HW RTC Get - accuracy depends on how often this is being set from HW RTC value
 * @param[in\out]  TimeRTC : RTC returned if DXOS_SUCCESS
 * @return DXOS_SUCCESS or DXOS_ERROR
 */
dxOS_RET_CODE dxHwRtcGet(DKTime_t* TimeRTC);

/** HW RTC Set
 * @param[in]  TimeRTC : Set RTC
 * @return DXOS_SUCCESS or DXOS_ERROR
 */
dxOS_RET_CODE dxHwRtcSet(DKTime_t SetTimeRTC);


/** SW RTC Get - accuracy depends on how often this is being set from HW RTC value
 * @param[in\out]  TimeRTC : RTC returned if DXOS_SUCCESS
 * @return DXOS_SUCCESS or DXOS_ERROR
 */
dxOS_RET_CODE dxSwRtcGet(DKTime_t* TimeRTC);

/** SW RTC Set
 * @param[in]  TimeRTC : Set RTC
 * @return DXOS_SUCCESS or DXOS_ERROR
 */
dxOS_RET_CODE dxSwRtcSet(DKTime_t SetTimeRTC);

/** Local Time Get - accuracy depends on how often this is being set from HW RTC value
 * @param[in\out]  TimeRTC : RTC returned if DXOS_SUCCESS
 * @return DXOS_SUCCESS or DXOS_ERROR
 */
dxOS_RET_CODE dxGetLocalTime (
    DKTimeLocal_t* localTime
);


/**
 * @brief dxGetUtcTime
 */
DKTime_t dxGetUtcTime (void);


/**
 * @brief dxGetUtcInSec
 */
uint32_t dxGetUtcInSec (void);



//============================================================================
// Queue Management
//============================================================================
typedef void * dxQueueHandle_t;


/**
 * @return dxQueueHandle_t  a handle to the queue.
 *         NULL             otherwise
 */
dxQueueHandle_t dxQueueCreate(uint32_t uxQueueLength,
                              uint32_t uxItemSize);


/**
 * @brief   Open (if already exist) or Create (if does not exist) global queue that can be used
 *          cross process (eg, Unix System).
 * @param   GlobalIdentifier - global identifier (eg in Unix System is "/mqname")
 * @param   uxQueueLength - Maximum messsage queues
 * @param   uxItemSize - Maximum size of the message in byte
 * @return  dxQueueHandle_t  a handle to the queue.
 *          NULL             otherwise
 */
dxQueueHandle_t dxQueueGlobalCreate(void*    GlobalIdentifier,
                                    uint32_t uxQueueLength,
                                    uint32_t uxItemSize);


/**
 * @brief   Get the already created global message queue.
 *          cross process (eg, Unix System).
 * @param   GlobalIdentifier - global identifier (eg in Unix System is "/mqname")
 * @return  dxQueueHandle_t  a handle to the queue.
 *          NULL             otherwise
 */
dxQueueHandle_t dxQueueGlobalGet(void*    GlobalIdentifier);

/**
 * NOTE: Event the result is DXOS_ERROR, the buffer may still contains
 *       data, and need to be freed
 *
 * @return DXOS_SUCCESS if an item was successfully received from the queue
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxQueueReceive(dxQueueHandle_t dxQueue,
                             void *pvBuffer,
                             uint32_t dxMsToWait);


/**
 * @return DXOS_SUCCESS if an item was successfully received from the queue
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxQueueReceiveFromISR(dxQueueHandle_t dxQueue,
                                    void *pvBuffer,
                                    dxBOOL* HigherPriorityTaskWoken);


/**
 * @return DXOS_SUCCESS if the item was successfully posted
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxQueueSend(dxQueueHandle_t dxQueue,
                          const void * pvItemToQueue,
                          uint32_t dxMsToWait);


/**
 * @brief   Same as dxQueueSend but specifying the size of the message, this function
 *          is normally used in conjunction with dxQueueGlobalCreate to allow variable message size
 *          but not exceeding of the max message size from dxQueueGlobalCreate.
 * @param dxQueue
 * @param pvItemToQueue
 * @param MessageSizeInByte
 * @param dxMsToWait
 * @return @return  DXOS_SUCCESS if the item was successfully posted
 *                  DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxQueueSendWithSize(  dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    uint32_t     MessageSizeInByte,
                                    uint32_t dxMsToWait);


/**
 * @return DXOS_SUCCESS if the item was successfully posted
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxQueueSendFromISR(   dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    dxBOOL* HigherPriorityTaskWoken);


/**
 * @return the available length of the queue
 */
uint32_t dxQueueSpacesAvailable(const dxQueueHandle_t dxQueue);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxQueueReset(dxQueueHandle_t dxQueue, dxQueueResetFunction_t dxResetFunction);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxQueueDelete(dxQueueHandle_t dxQueue);


/**
 * @return dxTRUE       if queue is empty
 *         dxFALSE      otherwise
 */
dxBOOL dxQueueIsEmpty(dxQueueHandle_t dxQueue);


/**
 * @return dxTRUE       if queue is full
 *         dxFALSE      otherwise
 */
dxBOOL dxQueueIsFull(dxQueueHandle_t dxQueue);



//============================================================================
// Task Management
//============================================================================

#define DX_DEFAULT_WORKER_PRIORITY                  DXTASK_PRIORITY_NORMAL //SUGGEST TO REMAIN NORMAL FOR DEFAULT


typedef void * dxTaskHandle_t;
typedef TASK_RET_TYPE (*dxTaskFunction_t)(TASK_PARAM_TYPE);
typedef void (*dxEventHandler_t)(void *arg);


typedef enum {
    DXTASK_PRIORITY_IDLE = tskIDLE_PRIORITY,
    DXTASK_PRIORITY_NORMAL = tskIDLE_PRIORITY + 1,
    DXTASK_PRIORITY_HIGH = tskIDLE_PRIORITY + 2,
    DXTASK_PRIORITY_VERY_HIGH = tskIDLE_PRIORITY + 3,
    DXTASK_PRIORITY_EXTREAMLY_HIGH = tskIDLE_PRIORITY + 4,

} dxTaskPriority_t;


typedef struct
{
    dxTaskHandle_t   WTask;
    dxQueueHandle_t  WEventQueue;

} dxWorkweTask_t;


typedef struct
{
    dxEventHandler_t    function;
    void*               arg;

} dxEventMsg_t;


/**
 * @return DXOS_SUCCESS a task is created
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTaskCreate(dxTaskFunction_t pvTaskCode,
                           const char * pcName,
                           uint16_t usStackSizeInByte,
                           void *pvParameters,
                           dxTaskPriority_t uxPriority,
                           dxTaskHandle_t *pvCreatedTask);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxTaskSuspend(dxTaskHandle_t dxTaskToSuspend);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxTaskResume(dxTaskHandle_t dxTaskToResume);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxTaskPrioritySet(dxTaskHandle_t dxTask, uint32_t uxNewPriority);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxTaskDelete(dxTaskHandle_t dxTaskToDelete);


/**
 * @return dxTaskHandle_t   a handle to the task.
 *         NULL             otherwise
 */
dxTaskHandle_t dxTaskGetCurrentTaskHandle(void);


/**
 * @return DXOS_SUCCESS a worker task released created
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxWorkerTaskCreate (const char* name, dxWorkweTask_t *pWorkerTask, dxTaskPriority_t uxPriority, uint16_t usStackDepth, uint32_t EventQueueSize);


/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxWorkerTaskDelete(dxWorkweTask_t *WorkerTask);


/**
 * @return DXOS_SUCCESS an event was sent
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxWorkerTaskSendAsynchEvent(dxWorkweTask_t *WorkerTask, dxEventHandler_t function, void* arg);



//============================================================================
// Task Stack - TO BE VERIFIED
//============================================================================
#define DXMAX_TASK_NAME_LEN                 12

typedef struct dxTaskStack {
    uint32_t    nTaskNumber;
    uint16_t    nPriority;
    uint16_t    nFreeStackSpace;
    uint16_t    nStackSize;
    uint8_t     szTaskName[DXMAX_TASK_NAME_LEN];
} dxTaskStack_t;

/**
 * @brief Retrieve task stack information
 * @param[IN] dxTask            A task handle created by dxTaskCreate()
 * @param[IN,OUT] pStackBuf     A pre-allocated buffer to store task stack information
 * @return DXOS_SUCCESS if task stack information are stored in pStackBuf
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxTaskGetStackInfo(dxTaskHandle_t dxTask, dxTaskStack_t *pStackBuf);

//============================================================================
// Semaphore Management
//============================================================================
typedef void * dxSemaphoreHandle_t;

/**
 * @return dxSemaphoreHandle_t  a handle to the semaphore.
 *         NULL                 otherwise
 */
dxSemaphoreHandle_t dxSemCreate(uint32_t uxMaxCount,
                                uint32_t uxInitialCount);

/**
 * @return DXOS_SUCCESS if the semaphore was released
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxSemGive(dxSemaphoreHandle_t dxSemaphore);

/**
 * @return DXOS_SUCCESS if the semaphore was obtained
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxSemTake(dxSemaphoreHandle_t dxSemaphore, uint32_t xBlockTimeInMS);

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxSemDelete(dxSemaphoreHandle_t dxSemaphore);

//============================================================================
// Mutex Management
//============================================================================
typedef void * dxMutexHandle_t;

/**
 * @return dxMutexHandle_t  a handle to the mutex.
 *         NULL             otherwise
 */
dxMutexHandle_t dxMutexCreate(void);

/**
 * @return DXOS_SUCCESS if the mutex was obtained
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxMutexTake(dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS);

/**
 * @return DXOS_SUCCESS if the mutex was released
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxMutexGive(dxMutexHandle_t dxMutex);

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxMutexDelete(dxMutexHandle_t dxMutex);

//============================================================================
// EventGroup Management
//============================================================================
typedef void * dxEventGroupHandle_t;

#define BIT_0                   ( 1 << 0 )
#define BIT_1                   ( 1 << 1 )
#define BIT_2                   ( 1 << 2 )
#define BIT_3                   ( 1 << 3 )
#define BIT_4                   ( 1 << 4 )
#define BIT_5                   ( 1 << 5 )
#define BIT_6                   ( 1 << 6 )
#define BIT_7                   ( 1 << 7 )
#define BIT_8                   ( 1 << 8 )
#define BIT_9                   ( 1 << 9 )
#define BIT_10                  ( 1 << 10 )
#define BIT_11                  ( 1 << 11 )
#define BIT_12                  ( 1 << 12 )
#define BIT_13                  ( 1 << 13 )
#define BIT_14                  ( 1 << 14 )
#define BIT_15                  ( 1 << 15 )
#if configUSE_16_BIT_TICKS == 0
#define BIT_16                  ( 1 << 16 )
#define BIT_17                  ( 1 << 17 )
#define BIT_18                  ( 1 << 18 )
#define BIT_19                  ( 1 << 19 )
#define BIT_20                  ( 1 << 20 )
#define BIT_21                  ( 1 << 21 )
#define BIT_22                  ( 1 << 22 )
#define BIT_23                  ( 1 << 23 )
#endif // configUSE_16_BIT_TICKS == 0

#define LOW16_EVENT_BITS        ( BIT_0 | \
                                  BIT_1 | \
                                  BIT_2 | \
                                  BIT_3 | \
                                  BIT_4 | \
                                  BIT_5 | \
                                  BIT_6 | \
                                  BIT_7 | \
                                  BIT_8 | \
                                  BIT_9 | \
                                  BIT_10 | \
                                  BIT_11 | \
                                  BIT_12 | \
                                  BIT_13 | \
                                  BIT_14 | \
                                  BIT_15)

#if configUSE_16_BIT_TICKS == 0
#define HIGH8_EVENT_BITS        ( BIT_16 | \
                                  BIT_17 | \
                                  BIT_18 | \
                                  BIT_19 | \
                                  BIT_20 | \
                                  BIT_21 | \
                                  BIT_22 | \
                                  BIT_23 )
#else
#define HIGH8_EVENT_BITS        0
#endif // configUSE_16_BIT_TICKS == 0

#define ALL_EVENT_BITS          ( HIGH8_EVENT_BITS | LOW16_EVENT_BITS)

/**
 * @return dxEventGroupHandle_t a handle to the event group.
 *         NULL                 otherwise
 */
dxEventGroupHandle_t dxEventGroupCreate(void);

/**
 * @return uint32_t     the value of the event group
 */
uint32_t dxEventGroupSet(dxEventGroupHandle_t dxEventGroup,
                         const uint32_t uxBitsToSet);

/**
 * @return uint32_t     the value of the event group before the specified bits
 *                      were cleared
 */
uint32_t dxEventGroupClear(dxEventGroupHandle_t dxEventGroup,
                           const uint32_t uxBitsToClear);

/**
 * @return uint32_t     The value of the event group at the time either the
 *                      bits being waited for became set, or the block time
 *                      expired
 */
uint32_t dxEventGroupWait(dxEventGroupHandle_t dxEventGroup,
                          const uint32_t uxBitsToWaitFor,
                          const dxBOOL xClearOnExit,
                          const dxBOOL xWaitForAllBits,
                          const uint32_t xMsToWait);

/**
 * @return DXOS_SUCCESS
 */
dxOS_RET_CODE dxEventGroupDelete(dxEventGroupHandle_t dxEventGroup);

//============================================================================
// Multicast UDP API
//============================================================================

typedef struct
{
    dxIP_Addr_t GroupIP;    // Multicast group IP
    uint16_t port;          // Multicast group port
} dxSocketAddr_t;


typedef void * dxUDPHandle_t;

/**
 * @brief initialization
 */
void dxMCAST_Init(void);

/**
 * @brief join specific multicast IP group and port
 * @param UDPGroupIp: multicast group IP, cant be NULL
 * @param port: specific port, cant be NULL
 * @return dxUDPHandle_t    if the socket joined multicast UDP group
 *         NULL             otherwise
 */
dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port);

/**
 * @brief the handle leave current multicast group, and close socket, at last free the handle
 * @param handle: include socket ID, UDP group IP, port
 * @return DXNET_SUCCESS    if the socket left multicast UDP group
 *         DXNET_ERROR      otherwise
 */
dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle);

/**
 * @brief receive the multicast data from specific group IP
 * @param handle: include socket ID, UDP group IP, port
 * @param Buffer: Data buffer pointer
 * @param BuffSize: Data buffer size
 * @param flags(DISABLE): message flags, default is 0
 * @param pFromAddr: Data source dxSocketAddr_t or NULL
 * @param TimeOutInMs: time out setting with millisecond
 * @return > 0                  if number of bytes received
 *         DXNET_CONN_CLOSED    if socket connection closed
 *         DXNET_ERROR          otherwise
 */
//int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, int flags, dxIP_Addr_t *pFromAddr, uint32_t TimeOutInMs);
int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs);

/**
 * @brief send the multicast data to specific group IP
 * @param handle: include socket ID, UDP group IP, port
 * @param dataptr: Data buffer pointer
 * @param dataSize: Data buffer size
 * @param flags(DISABLE): message flags, default is 0
 * @param ToAddr: Data destination dxSocketAddr_t or NULL
 * @return > 0                  if number of bytes sent
 *         DXNET_ERROR          otherwise
 */
//int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, int flags, dxIP_Addr_t ToAddr);
int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket);



//============================================================================
// AES Encryption/Decryption
//============================================================================

typedef void * dxAesContext_t;


typedef enum {

    DXAES_SUCCESS = 0,

    DXAES_ERROR             = -1,
    DXAES_INVALID_PARAM     = -2,


} dxAES_RET_CODE;


typedef enum {

    DXAES_ENCRYPT   = 1,

    DXAES_DECRYPT   = 2,

} dxAES_CRYPTO_TYPE;


/**
 * @brief Allocate the context for SW AES (Thread Safe!)
 * @return : Context is non null if success
 */
dxAesContext_t  dxAESSW_ContextAlloc(void);


/**
 * @brief Set the SW AES Key  (Thread Safe!)
 * @param ctx - The allocated context
 * @param key - The Key
 * @param keysize - Key size
 * @param Type  - Type (see dxAES_CRYPTO_TYPE)
 * @return : Retun code
 */
dxAES_RET_CODE  dxAESSW_Setkey( dxAesContext_t ctx, const unsigned char *key, unsigned int keysize, dxAES_CRYPTO_TYPE Type );


/**
 * @brief Encrypt/Decrypt SW AES (Thread Safe!)
 * @param ctx - The allocated context
 * @param Type  - Type (see dxAES_CRYPTO_TYPE)
 * @param input - Source
 * @param output  - destination
 * @return : Retun code
 */
dxAES_RET_CODE  dxAESSW_CryptEcb( dxAesContext_t ctx, dxAES_CRYPTO_TYPE Type, const unsigned char input[16], unsigned char output[16] );


/**
 * @brief Deallocate the context for SW AES (Thread Safe!)
 * @return : return code
 */
dxAES_RET_CODE  dxAESSW_ContextDealloc(dxAesContext_t ctx);


extern uint32_t dxRand();


//============================================================================
// SPI Access Resource
// NOTE! -  Currently this is used on flash access in BSP as well as
//          IR remote (HybridPeripheral) when accessing other SPI port
//          The reason is becasue in RTK the SPI access is NOT thread safe
//          and there is no common interface for that (eg, Flash access vs Generic access)
//          We should be creating SPI resource access within our BSP so that this does not get messy
//          Need to start first by developing dxSPI interface so that we can easily
//          manage the SPI resource!!!!
//============================================================================

/**
 * @brief Take SPI single Resource
 * @return : return none
 */
void  dxSPIaccess_ResourceTake(void); //WORKAROUND ONLY - See detail above!!
void  dxSPIaccess_ResourceGive(void); //WORKAROUND ONLY - See detail above!!

int dxSelect (
    int nfds,
    fd_set* readfds,
    fd_set* writefds,
    fd_set* exceptfds,
    struct timeval* timeout
);


int dxNumOfDigits (uint32_t v);

#endif // _DXOS_H
