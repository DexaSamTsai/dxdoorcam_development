//============================================================================
// File: dxWIFI.c
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dxOS.h"
#include "dxWIFI.h"
#include "Utils.h"

//============================================================================
// Defines and Macro
//============================================================================
#define CONFIG_USE_DEXATEK_WIFI_SCAN                0   // 0 to use RTK provided wifi_scan(), 1 to use DEXATEK
#define DX_MAX_SCAN_AP_NUMBER                       64  // The value is the same as max_ap_size that defines in wifi_conf.c

//============================================================================
// WiFi Management
//============================================================================

// Forward declaraion
extern int wifi_on_ext(rtw_mode_t mode, rtw_country_code_t ch, rtw_adaptivity_mode_t adv);

dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv);

// External variables
extern struct netif xnetif[];

//void dxNetIf_Init(void)
//{
//    LwIP_Init();
//}

dxWIFI_RET_CODE dxWiFi_Connect( char *ssid,
                                dxWiFi_Security_t security_type,
                                char *password,
                                int key_id,
                                dxSemaphoreHandle_t *dxsemaphore)
{
    dxWIFI_RET_CODE Return = DXWIFI_SUCCESS;

    int ret = wifi_connect(ssid,
                       (rtw_security_t)security_type,
                       password,
                       strlen(ssid),
                       strlen(password),
                       key_id,
                       NULL);

    G_SYS_DBG_LOG_V1("\r WifiConnect ret = %d       \n\n", ret);
    if (ret != 0)
    {
        Return = DXWIFI_ERROR;
    }

    return Return;
}

dxWIFI_RET_CODE dxWiFi_Disconnect(void)
{
    dxWIFI_RET_CODE Return = DXWIFI_SUCCESS;

    int ret =  wifi_disconnect();

    if (ret != 0)
    {
        Return = DXWIFI_ERROR;
    }

    return Return;

}

dxWIFI_RET_CODE dxWiFi_GetMAC(dxWiFi_MAC_t *pMAC)
{
    if (pMAC == NULL) {
        return DXWIFI_ERROR;
    }

    memcpy(pMAC->octet, LwIP_GetMAC(&xnetif[0]), sizeof(pMAC->octet));

    return DXWIFI_SUCCESS;
}

dxWIFI_RET_CODE dxWiFi_IsOnline(dxWiFi_Mode_t xMode)
{
    rtw_interface_t rmode;

    if (xMode == DXWIFI_MODE_STA) {
        rmode = RTW_STA_INTERFACE;
    }
    else {
        rmode = RTW_AP_INTERFACE;
    }

    if (wifi_is_ready_to_transceive(rmode) == 0)
        return DXWIFI_SUCCESS;
    else
        return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_Off(void)
{
    if (wifi_off() == 0)
        return DXWIFI_SUCCESS;
    else
        return DXWIFI_ERROR;
}

dxWIFI_RET_CODE dxWiFi_On(dxWiFi_Mode_t mode)
{
    return dxWiFi_On_Ext(mode, DXWIFI_CHANNEL_PLAN_DEFAULT, DXWIFI_ADAPTIVITY_DISABLE);
}

dxWIFI_RET_CODE dxWiFi_On_Ext(dxWiFi_Mode_t mode, dxWiFi_Channel_Plan_t ch, dxWiFi_Adaptivity_t adv)
{
    static dxBOOL bNetIfInit = dxFALSE;

    if (bNetIfInit == dxFALSE) {
        LwIP_Init();
        bNetIfInit = dxTRUE;
    }

    rtw_mode_t rmode = RTW_MODE_NONE;

    if (mode == DXWIFI_MODE_STA) {
        rmode = RTW_MODE_STA;
    }
    else if (mode == DXWIFI_MODE_AP) {
        rmode = RTW_MODE_AP;
    }

    rtw_country_code_t cc;

    switch (ch)
    {
    case DXWIFI_CHANNEL_PLAN_KOREA:         // 1
        cc = RTW_COUNTRY_ETSI1;                 // 1
        break;
    case DXWIFI_CHANNEL_PLAN_EU:            // 2
        cc = RTW_COUNTRY_ETSI1;                 // 1
        break;
    case DXWIFI_CHANNEL_PLAN_NORTH_AMERICA: // 3
        cc = RTW_COUNTRY_FCC1;                  // 2
        break;
    case DXWIFI_CHANNEL_PLAN_JAPAN:         // 4
        cc = RTW_COUNTRY_MKK1;                  // 3
        break;
    default:
        cc = RTW_COUNTRY_WORLD1;                // 0
        break;
    }

    dxWiFi_Adaptivity_t adaptivity;

    switch (adv)
    {
    case DXWIFI_ADAPTIVITY_NORMAL:          // 1
        adaptivity = RTW_ADAPTIVITY_NORMAL;         // 1
        break;
    case DXWIFI_ADAPTIVITY_CARRIER_SENSE:   // 2
        adaptivity = RTW_ADAPTIVITY_CARRIER_SENSE;  // 2
        break;
    default:                                // 0
        adaptivity = RTW_ADAPTIVITY_DISABLE;        // 0
        break;
    }

    if (wifi_on_ext(rmode, cc, adaptivity) >= 0)
        return DXWIFI_SUCCESS;
    else
        return DXWIFI_ERROR;

}

static dxWiFi_Scan_Result_Handler_t dxWiFi_Scan_Results_Handler = NULL;
#if CONFIG_USE_DEXATEK_WIFI_SCAN
#else // CONFIG_USE_DEXATEK_WIFI_SCAN

static rtw_result_t app_scan_result_handler(rtw_scan_handler_result_t* malloced_scan_result )
{
    dxWiFi_Scan_Result_t dxScan_Result;
    memset(&dxScan_Result, 0x00, sizeof(dxScan_Result));

    dxScan_Result.ap_details.SSID.len = malloced_scan_result->ap_details.SSID.len;
    memcpy(&dxScan_Result.ap_details.SSID.val, &malloced_scan_result->ap_details.SSID.val, dxScan_Result.ap_details.SSID.len);

    memcpy(dxScan_Result.ap_details.BSSID.octet, malloced_scan_result->ap_details.BSSID.octet, sizeof(dxScan_Result.ap_details.BSSID.octet));

    //unsigned char *p = &malloced_scan_result->ap_details.BSSID.octet[0];
    //printf("\r\nBSSID: %02X:%02X:%02X:%02X:%02X:%02X\r\n", p[0], p[1], p[2], p[3], p[4], p[5]);

    //printf("\r\nbss_type: %d, security: %08X\r\n", malloced_scan_result->ap_details.bss_type, malloced_scan_result->ap_details.security);

     dxScan_Result.ap_details.signal_strength = malloced_scan_result->ap_details.signal_strength;
     dxScan_Result.ap_details.bss_type = (dxWiFi_BSS_Type_t)malloced_scan_result->ap_details.bss_type;
     dxScan_Result.ap_details.security = (dxWiFi_Security_t)malloced_scan_result->ap_details.security;
     dxScan_Result.ap_details.wps_type = (dxWiFi_WPS_Type_t)malloced_scan_result->ap_details.wps_type;
     dxScan_Result.ap_details.channel = (uint32_t)malloced_scan_result->ap_details.channel;
     dxScan_Result.ap_details.band = (dxWiFi_802_11_Band_t)malloced_scan_result->ap_details.band;

     dxScan_Result.scan_complete = malloced_scan_result->scan_complete;

     dxScan_Result.user_data = malloced_scan_result->user_data;

    if (dxWiFi_Scan_Results_Handler) {
        (dxWiFi_Scan_Results_Handler)(&dxScan_Result);
    }

    return RTW_SUCCESS;
}
#endif // CONFIG_USE_DEXATEK_WIFI_SCAN

static dxWiFi_Scan_State_t dxWiFiScanState = DXWIFI_SCAN_STATE_NONE;

#if CONFIG_USE_DEXATEK_WIFI_SCAN
#pragma default_variable_attributes = @ ".sdram.text"
static uint32_t             dxWiFi_Scan_Result_Count = 0;
static dxWiFi_Scan_Result_t dxWiFi_Scan_Result_List[DX_MAX_SCAN_AP_NUMBER]; // Around 4608 bytes in SDRAM
static void                 *dxWiFi_Scan_UserData = NULL;
#pragma default_variable_attributes =

void dxWiFi_Scan_Result_Init(void)
{
    dxWiFi_Scan_Result_Count = 0;
    memset(&dxWiFi_Scan_Result_List[0], 0x00, sizeof(dxWiFi_Scan_Result_List));
}

void dxWiFi_Scan_Result_Set(int nIndex, dxWiFi_Scan_Result_t *result)
{
    if (nIndex >= 0 &&
        nIndex < DX_MAX_SCAN_AP_NUMBER &&
        result)
    {
        dxWiFi_Scan_Result_t *p = &dxWiFi_Scan_Result_List[nIndex];

        memset(p, 0x00, sizeof(dxWiFi_Scan_Result_t));

        memcpy(p->ap_details.SSID.val,    result->ap_details.SSID.val,    result->ap_details.SSID.len);
        memcpy(p->ap_details.BSSID.octet, result->ap_details.BSSID.octet, sizeof(p->ap_details.BSSID.octet));
        p->ap_details.SSID.len          = result->ap_details.SSID.len;
        p->ap_details.signal_strength   = result->ap_details.signal_strength;
        p->ap_details.bss_type          = result->ap_details.bss_type;
        p->ap_details.security          = result->ap_details.security;
        p->ap_details.wps_type          = result->ap_details.wps_type;
        p->ap_details.channel           = result->ap_details.channel;
        p->ap_details.band              = result->ap_details.band;
        p->scan_complete                = result->scan_complete;
        p->user_data                    = result->user_data;
    }
}

dxBOOL dxWiFi_Scan_Result_Update(dxWiFi_Scan_Result_t *result_ptr)
{
    dxBOOL bFound = dxFALSE;
    int i = 0;
    int nIndex = dxWiFi_Scan_Result_Count;

    // Replace existing
    for (i = 0; i < dxWiFi_Scan_Result_Count; i++)
    {
        dxWiFi_Scan_Result_t *p = &dxWiFi_Scan_Result_List[i];
        if (CMP_MAC(p->ap_details.BSSID.octet, result_ptr->ap_details.BSSID.octet))
        {
            dxWiFi_Scan_Result_Set(i, result_ptr);
            bFound = dxTRUE;
        }
    }

    if (bFound == dxTRUE)
    {
        return dxTRUE;
    }

    dxWiFi_Scan_Result_Set(nIndex, result_ptr);

    dxWiFi_Scan_Result_Count++;

    return dxFALSE;
}

static void dxWiFi_Scan_Each_Report_HDL(char* buf, int buf_len, int flags, void* userdata)
{
    rtw_scan_result_t **result_ptr = (rtw_scan_result_t **)buf;
    rtw_scan_result_t *q = (*result_ptr);
    rtw_scan_result_t *x = (rtw_scan_result_t *)buf;
    rtw_scan_result_t r;
    memcpy(&r, (*result_ptr), sizeof(r));

    dxWiFi_Scan_Result_t current_result;
    memset(&current_result, 0x00, sizeof(dxWiFi_Scan_Result_t));

    dxWiFi_Scan_Result_t *p = &current_result;

    memcpy(p->ap_details.SSID.val,    q->SSID.val,    q->SSID.len);
    memcpy(p->ap_details.BSSID.octet, q->BSSID.octet, sizeof(p->ap_details.BSSID.octet));
    p->ap_details.SSID.len          = q->SSID.len;
    p->ap_details.signal_strength   = q->signal_strength;
    p->ap_details.bss_type          = (dxWiFi_BSS_Type_t)q->bss_type;
    p->ap_details.security          = (dxWiFi_Security_t)q->security;
    p->ap_details.wps_type          = (dxWiFi_WPS_Type_t)q->wps_type;
    p->ap_details.channel           = (uint32_t)q->channel;
    p->ap_details.band              = (dxWiFi_802_11_Band_t)q->band;
    p->scan_complete                = dxFALSE;
    p->user_data                    = dxWiFi_Scan_UserData;

    if (dxWiFi_Scan_Result_Update(p) == dxFALSE)
    {
        // Found new Wi-Fi AP
        if (dxWiFi_Scan_Results_Handler)
        {
            (dxWiFi_Scan_Results_Handler)(p);
        }
    }

    memset(*result_ptr, 0, sizeof(rtw_scan_result_t));
}
#endif // CONFIG_USE_DEXATEK_WIFI_SCAN

static void dxWiFi_Scan_Done_HDL(char *buf, int buf_len, int flags, void *userdata)
{
#if CONFIG_USE_DEXATEK_WIFI_SCAN
    wifi_unreg_event_handler(WIFI_EVENT_SCAN_RESULT_REPORT, dxWiFi_Scan_Each_Report_HDL);
#endif // CONFIG_USE_DEXATEK_WIFI_SCAN
    wifi_unreg_event_handler(WIFI_EVENT_SCAN_DONE, dxWiFi_Scan_Done_HDL);

#if CONFIG_USE_DEXATEK_WIFI_SCAN
    dxWiFi_Scan_Result_t *p = NULL;
    if (dxWiFi_Scan_Result_Count > 0)
    {
        p = &dxWiFi_Scan_Result_List[dxWiFi_Scan_Result_Count - 1];
        p->scan_complete = dxTRUE;
    }

#if 0
    // Dump WiFi scan result
    G_SYS_DBG_LOG_NV("=====================================================================================\r\n");

    for (int i = 0; i < dxWiFi_Scan_Result_Count; i++)
    {
        dxWiFi_Scan_Result_t *p = &dxWiFi_Scan_Result_List[i];
        G_SYS_DBG_LOG_NV("[%20s %02X:%02X:%02X:%02X:%02X:%02X %3d %12d %8X %2d %2d %12d]\r\n",
                         p->ap_details.SSID.val,
                         p->ap_details.BSSID.octet[0],
                         p->ap_details.BSSID.octet[1],
                         p->ap_details.BSSID.octet[2],
                         p->ap_details.BSSID.octet[3],
                         p->ap_details.BSSID.octet[4],
                         p->ap_details.BSSID.octet[5],
                         p->ap_details.signal_strength,
                         p->ap_details.bss_type,
                         p->ap_details.security,
                         p->ap_details.wps_type,
                         p->ap_details.channel,
                         p->ap_details.band);
    }

    G_SYS_DBG_LOG_NV("=====================================================================================\r\n");
#endif

    if (p && dxWiFi_Scan_Results_Handler)
    {
        (dxWiFi_Scan_Results_Handler)(p);
    }

    dxWiFi_Scan_Results_Handler = NULL;
#endif // CONFIG_USE_DEXATEK_WIFI_SCAN

    dxWiFiScanState = DXWIFI_SCAN_STATE_DONE;
}

dxWiFi_Scan_State_t dxWiFi_GetScanState(void)
{
    dxWiFi_Scan_State_t ret = dxWiFiScanState;

    return ret;
}

dxWIFI_RET_CODE dxWiFi_Scan(dxWiFi_Scan_Result_Handler_t results_handler, void *user_data)
{
    dxWiFi_Scan_Results_Handler = results_handler;

    dxWiFiScanState = DXWIFI_SCAN_STATE_SCANNING;

    wifi_reg_event_handler(WIFI_EVENT_SCAN_DONE, dxWiFi_Scan_Done_HDL, NULL);

#if CONFIG_USE_DEXATEK_WIFI_SCAN
    wifi_reg_event_handler(WIFI_EVENT_SCAN_RESULT_REPORT, dxWiFi_Scan_Each_Report_HDL, NULL);

    dxWiFi_Scan_Result_Init();

    dxWiFi_Scan_UserData = user_data;

    uint16_t nScanType = (RTW_SCAN_COMMAMD << 4) | RTW_SCAN_TYPE_ACTIVE;
    uint16_t nBSSType = RTW_BSS_TYPE_ANY;
    uint16_t nFlag = nScanType | (nBSSType << 8);

    int r = wext_set_scan(WLAN0_NAME, NULL, 0, nFlag);

    return (r == 0) ? DXWIFI_SUCCESS : DXWIFI_ERROR;
#else // CONFIG_USE_DEXATEK_WIFI_SCAN
    dxWIFI_RET_CODE ret = DXWIFI_ERROR;
    if (wifi_scan_networks(app_scan_result_handler, user_data) == 0)
    {
        ret = DXWIFI_SUCCESS;
    }
    return ret;
#endif // CONFIG_USE_DEXATEK_WIFI_SCAN
}

dxWIFI_RET_CODE dxWiFi_WaitForOnline(dxWiFi_Mode_t xMode, uint32_t xMSToWait)
{
    uint32_t value = 0;
    dxWIFI_RET_CODE Return = DXWIFI_TIMEOUT;

    while (value < xMSToWait) {
        dxWIFI_RET_CODE ret = dxWiFi_IsOnline(xMode);

        if (ret == DXWIFI_SUCCESS)
        {
            Return = DXWIFI_SUCCESS;
            break;
        }

        dxTimeDelayMS(1000);
        value += 1000;
    }

    return Return;
}

