//============================================================================
// File: dxMemConfig.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/02/16
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/04/25
//     1) Description: Reserve DEVINFO_FLASH (4KB) for user device information
//
//     Version: 0.3
//     Date: 2016/05/05
//     1) Description: Reserve DX_SUBIMG_FLASH (1MB) for subsystem FW update
//
//     Version: 0.4
//     Date: 2016/07/05
//     1) Description: Added DX_FLASH_SIZE_TOTAL_MBITS
//
//     Version: 0.5
//     Date: 2016/08/16
//     1) Description: Reserve DX_HP_STORAGE_SIZE (512KB) for hyper peripheral storage
//     2) Description: Revise DX_DEVINFO_FLASH_SIZE and DX_SYSINFO_FLASH_SIZE to 16KB
//============================================================================

#ifndef _DXMEMCONFIG_H
#define _DXMEMCONFIG_H


/**
 * Please check hardware to identify the flash size.
 * Enable one of the following
 *     CONFIG_DX_FLASH_SIZE_8MBIT
 *     CONFIG_DX_FLASH_SIZE_16MBIT
 *     CONFIG_DX_FLASH_SIZE_32MBIT
 */

#define CONFIG_DX_FLASH_SIZE_8MBIT                 0

/** RTK 8711/95 BASE ON 8Mbit Flash Size
 *
 * Flash Block Size: 4096 bytes
 *
 *   0x08000       +------------------------------------+
 *                 | BACKUP_SECTOR                      |
 *   0x09000       +------------------------------------+
 *                 | FLASH_SYSTEM_DATA_ADDR             |
 *   0x0A000       +------------------------------------+
 *                 | FLASH_CAL_DATA_BASE                |
 *   0x0B000       +------------------------------------+
 *                 | IMAGE_2 (468 KB)                   |
 *   0x80000       +------------------------------------+ <== DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR (NewImg2Addr)
 *                 | Upgrade Image 2 (468 KB)           |
 *   0xF5000       +------------------------------------+
 *                 | Unused space (24 KB)               |
 *   0xFB000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR1
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *   0xFC000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR2
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *   0xFD000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR1
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *   0xFE000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR2
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *   0xFF000 + 000 +------------------------------------+ <== DX_HOMEKIT_SETTING_DATA_ADDR
 *                 | PersistentSetupcode_t (20 bytes)   |
 *           + 020 +------------------------------------+
 *                 | Reserved (236 bytes)               |
 *           + 100 +------------------------------------+ <== DX_HOMEKIT_WAC_FLASH_OFFSET
 *                 | WACPersistentConfig_t (140 bytes)  |
 *           + 190 +------------------------------------+ <== DX_HOMEKIT_HAP_KEYPAIR_OFFSET
 *                 | HAPPersistentKeypair_t (68 bytes)  |
 *           + 1E0 +------------------------------------+ <== DX_HOMEKIT_HAP_PAIRING_OFFSET
 *                 | HAPPersistentPairing_t (108 bytes) |
 *                 |                                    |
 *                 |  X 20                              |
 *                 |                                    |
 *                 |                                    |
 *           + A50 +------------------------------------+
 *                 | Reserved (1456 bytes)              |
 *  0x100000 + 000 +------------------------------------+
 */

#define CONFIG_DX_FLASH_SIZE_16MBIT                0

/** RTK 8711/95 BASE ON 16Mbit Flash Size
 *
 * Flash Block Size: 4096 bytes
 *
 *   0x08000       +------------------------------------+
 *                 | BACKUP_SECTOR                      |
 *   0x09000       +------------------------------------+
 *                 | FLASH_SYSTEM_DATA_ADDR             |
 *   0x0A000       +------------------------------------+
 *                 | FLASH_CAL_DATA_BASE                |
 *   0x0B000       +------------------------------------+
 *                 | IMAGE_2 (960 KB)                   |
 *   0xFB000       +------------------------------------+ <== DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR (NewImg2Addr)
 *                 | Upgrade Image 2 (960 KB)           |
 *  0x1EB000       +------------------------------------+
 *                 | Unused space (64 KB)               |
 *  0x1FB000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR1
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *  0x1FC000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR2
 *                 | DX_DEVINFO_FLASH_SIZE (4KB)        |
 *  0x1FD000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR1
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *  0x1FE000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR2
 *                 | DX_SYSINFO_FLASH_SIZE (4KB)        |
 *  0x1FF000 + 000 +------------------------------------+ <== DX_HOMEKIT_SETTING_DATA_ADDR
 *                 | PersistentSetupcode_t (20 bytes)   |
 *           + 020 +------------------------------------+
 *                 | Reserved (236 bytes)               |
 *           + 100 +------------------------------------+ <== DX_HOMEKIT_WAC_FLASH_OFFSET
 *                 | WACPersistentConfig_t (140 bytes)  |
 *           + 190 +------------------------------------+ <== DX_HOMEKIT_HAP_KEYPAIR_OFFSET
 *                 | HAPPersistentKeypair_t (68 bytes)  |
 *           + 1E0 +------------------------------------+ <== DX_HOMEKIT_HAP_PAIRING_OFFSET
 *                 | HAPPersistentPairing_t (108 bytes) |
 *                 |                                    |
 *                 |  X 20                              |
 *                 |                                    |
 *                 |                                    |
 *           + A50 +------------------------------------+
 *                 | Reserved (1456 bytes)              |
 *  0x200000 (2MB) +------------------------------------+
 */

#define CONFIG_DX_FLASH_SIZE_32MBIT                1

/** RTK 8711/95 BASE ON 32Mbit Flash Size
 *
 * Flash Block Size: 4096 bytes
 *
 *   0x08000       +------------------------------------+
 *                 | BACKUP_SECTOR                      |
 *   0x09000       +------------------------------------+
 *                 | FLASH_SYSTEM_DATA_ADDR             |
 *   0x0A000       +------------------------------------+
 *                 | FLASH_CAL_DATA_BASE                |
 *   0x0B000       +------------------------------------+
 *                 | IMAGE_2 (960 KB)                   |
 *   0xFB000       +------------------------------------+ <== DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR (NewImg2Addr)
 *                 | Upgrade Image 2 (960 KB)           |
 *  0x1EB000       +------------------------------------+ <== DX_SUBIMG_FLASH_ADDR1
 *                 | DX_SUBIMG_FLASH_SIZE (1MB)         |
 *  0x2EB000       +------------------------------------+ <== DX_HP_STORAGE_ADDR1
 *                 | DX_HP_STORAGE_SIZE (512KB)         |
 *  0x36B000       +------------------------------------+
 *                 | Unused space (528 KB)              |
 *  0x3EF000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR1
 *                 | DX_DEVINFO_FLASH_SIZE (16KB)       |
 *  0x3F3000       +------------------------------------+ <== DX_DEVINFO_FLASH_ADDR2
 *                 | DX_DEVINFO_FLASH_SIZE (16KB)       |
 *  0x3F7000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR1
 *                 | DX_SYSINFO_FLASH_SIZE (16KB)       |
 *  0x3FB000       +------------------------------------+ <== DX_SYSINFO_FLASH_ADDR2
 *                 | DX_SYSINFO_FLASH_SIZE (16KB)       |
 *  0x3FF000 + 000 +------------------------------------+ <== DX_HOMEKIT_SETTING_DATA_ADDR
 *                 | PersistentSetupcode_t (20 bytes)   |
 *           + 020 +------------------------------------+
 *                 | Reserved (236 bytes)               |
 *           + 100 +------------------------------------+ <== DX_HOMEKIT_WAC_FLASH_OFFSET
 *                 | WACPersistentConfig_t (140 bytes)  |
 *           + 190 +------------------------------------+ <== DX_HOMEKIT_HAP_KEYPAIR_OFFSET
 *                 | HAPPersistentKeypair_t (68 bytes)  |
 *           + 1E0 +------------------------------------+ <== DX_HOMEKIT_HAP_PAIRING_OFFSET
 *                 | HAPPersistentPairing_t (108 bytes) |
 *                 |                                    |
 *                 |  X 20                              |
 *                 |                                    |
 *                 |                                    |
 *           + A50 +------------------------------------+
 *                 | Reserved (1456 bytes)              |
 *  0x400000 (2MB) +------------------------------------+
 */
#define DEFAULT_FLASH_SECTOR_ERASE_SIZE      4096

#if CONFIG_DX_FLASH_SIZE_32MBIT

#define DX_SYSINFO_FLASH_SIZE                       16384        // MUST align to a SECTOR of flash, typical 16KB

#define DX_DEVINFO_FLASH_SIZE                       16384        // MUST align to a SECTOR of flash, typical 16KB

#else

#define DX_SYSINFO_FLASH_SIZE                       4096        // MUST align to a SECTOR of flash, typical 4KB

#define DX_DEVINFO_FLASH_SIZE                       4096        // MUST align to a SECTOR of flash, typical 4KB

#endif // CONFIG_DX_FLASH_SIZE_32MBIT


#if CONFIG_DX_FLASH_SIZE_16MBIT

#pragma message ( "REMINDER: CONFIG_DX_FLASH_SIZE_16MBIT (2MB SIZE) SELECTED!" )

  #define DX_FLASH_SIZE_TOTAL_MBITS                 16

  #define DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR    0xFB000     // MUST be revised
  #define DX_SYSINFO_FLASH_UPDATE_IMG_SIZE          (1024*960)  // MUST be revised

  #define DX_DEVINFO_FLASH_ADDR1                    0x1FB000    // MUST be revised

#elif CONFIG_DX_FLASH_SIZE_32MBIT

#pragma message ( "REMINDER: CONFIG_DX_FLASH_SIZE_32MBIT  (4MB SIZE) SELECTED!" )

  #define DX_FLASH_SIZE_TOTAL_MBITS                 32

  #define DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR    0xFB000     // MUST be revised
  #define DX_SYSINFO_FLASH_UPDATE_IMG_SIZE          (1024*960)  // MUST be revised

  #define DX_DEVINFO_FLASH_ADDR1                    0x3EF000//0x3FB000    // MUST be revised

  #define DX_SUBIMG_FLASH_ADDR1                     (DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR +\
                                                    DX_SYSINFO_FLASH_UPDATE_IMG_SIZE)//0x1EB000    // MUST be revised

  #define DX_SUBIMG_FLASH_SIZE                      (1024*1024) // MUST align to a SECTOR of flash, typical 1MB

  #define DX_HP_STORAGE_ADDR1                       (DX_SUBIMG_FLASH_ADDR1 + DX_SUBIMG_FLASH_SIZE) // MUST be revised

  #define DX_HP_STORAGE_SIZE                        (1024*512) // MUST align to a SECTOR of flash, typical 512KB

#else // CONFIG_DX_FLASH_SIZE_8MBIT

#pragma message ( "REMINDER: CONFIG_DX_FLASH_SIZE_8MBIT  (1MB SIZE) SELECTED!" )

  #define DX_FLASH_SIZE_TOTAL_MBITS                 8

  #define DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR    0x80000     // MUST be revised
  #define DX_SYSINFO_FLASH_UPDATE_IMG_SIZE          (1024*468)  // MUST be revised

  #define DX_DEVINFO_FLASH_ADDR1                    0xFB000     // MUST be revised

#endif // CONFIG_DX_FLASH_SIZE_8MBIT

#define DX_DEVINFO_FLASH_ADDR2                      (DX_DEVINFO_FLASH_ADDR1 + DX_DEVINFO_FLASH_SIZE)

#define DX_SYSINFO_FLASH_ADDR1                      (DX_DEVINFO_FLASH_ADDR2 + DX_DEVINFO_FLASH_SIZE)

#define DX_SYSINFO_FLASH_ADDR2                      (DX_SYSINFO_FLASH_ADDR1 + DX_SYSINFO_FLASH_SIZE)

#define DX_HOMEKIT_SETTING_DATA_ADDR                (DX_SYSINFO_FLASH_ADDR2 + DX_SYSINFO_FLASH_SIZE)

#define DX_HOMEKIT_SETUPCODE_OFFSET                 0x000
#define DX_HOMEKIT_WAC_FLASH_OFFSET                 0x100
#define DX_HOMEKIT_HAP_KEYPAIR_OFFSET               0x190
#define DX_HOMEKIT_HAP_PAIRING_OFFSET               0x1E0

#endif // _DXMEMCONFIG_H
