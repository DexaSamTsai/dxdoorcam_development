//============================================================================
// File: dxNET.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include "dxNET.h"


//============================================================================
// Socket Option
//============================================================================

int dxSOCKET_SetTimeout(int s, uint32_t msec)
{
    int ret = 0;

    /**
     * How to use setsockopt() ?
     * (1) For lwIP 1.4.1 and before,
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&msec, sizeof(msec));
     *
     * (2) For lwIP 1.5.0 and later,
     *     struct timeval interval = { .tv_sec = msec / 1000,
     *                                 .tv_usec = (msec % 1000) * 1000 };
     *
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));
     */

#if DK_BSP_RTK_HOMEKIT_ENABLE
#pragma message ( "REMINDER: setsockopt() comply to lwIP 1.5.0!" )

    struct timeval interval = { .tv_sec = msec / 1000,
                                .tv_usec = (msec % 1000) * 1000 };

    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));
#else // DK_BSP_RTK_HOMEKIT_ENABLE
#pragma message ( "REMINDER: setsockopt() comply to lwIP 1.4.1!" )
    ret = setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (const char *)&msec, sizeof(msec));
#endif // DK_BSP_RTK_HOMEKIT_ENABLE

    return ret;
}


//============================================================================
// DHCP Client
//============================================================================

extern struct netif xnetif[];

dxDHCP_STATE dxDHCP_Start(void)
{
    //NOTE: This function MUST be blocking, make sure the porting satisfy this requirement!

    dxDHCP_STATE state = 0;
    DHCP_State_TypeDef lwstate = LwIP_DHCP(0, DHCP_START);

    switch (lwstate) {
    case DHCP_START:
        state = DXDHCP_START;
        break;
    case DHCP_WAIT_ADDRESS:
        state = DXDHCP_WAIT_ADDRESS;
        break;
    case DHCP_ADDRESS_ASSIGNED:
        state = DXDHCP_ADDRESS_ASSIGNED;
        break;
    case DHCP_RELEASE_IP:
        state = DXDHCP_RELEASE_IP;
        break;
    case DHCP_STOP:
        state = DXDHCP_STOP;
        break;
    case DHCP_TIMEOUT:
        state = DXDHCP_TIMEOUT;
        break;
    }

    return state;
}

dxDHCP_STATE dxDHCP_Stop(void)
{
    dxDHCP_STATE state = 0;
    DHCP_State_TypeDef lwstate = LwIP_DHCP(0, DHCP_STOP);

    switch (lwstate) {
    case DHCP_START:
        state = DXDHCP_START;
        break;
    case DHCP_WAIT_ADDRESS:
        state = DXDHCP_WAIT_ADDRESS;
        break;
    case DHCP_ADDRESS_ASSIGNED:
        state = DXDHCP_ADDRESS_ASSIGNED;
        break;
    case DHCP_RELEASE_IP:
        state = DXDHCP_RELEASE_IP;
        break;
    case DHCP_STOP:
        state = DXDHCP_STOP;
        break;
    case DHCP_TIMEOUT:
        state = DXDHCP_TIMEOUT;
        break;
    }

    return state;
}

dxDHCP_STATE dxDHCP_Release(void)
{
    dxDHCP_STATE state = 0;
    DHCP_State_TypeDef lwstate = LwIP_DHCP(0, DHCP_RELEASE_IP);

    switch (lwstate) {
    case DHCP_START:
        state = DXDHCP_START;
        break;
    case DHCP_WAIT_ADDRESS:
        state = DXDHCP_WAIT_ADDRESS;
        break;
    case DHCP_ADDRESS_ASSIGNED:
        state = DXDHCP_ADDRESS_ASSIGNED;
        break;
    case DHCP_RELEASE_IP:
        state = DXDHCP_RELEASE_IP;
        break;
    case DHCP_STOP:
        state = DXDHCP_STOP;
        break;
    case DHCP_TIMEOUT:
        state = DXDHCP_TIMEOUT;
        break;
    }

    return state;
}


dxNet_Addr_t dxDHCP_Info(void)
{
    dxNet_Addr_t dxNetAddr;
    memset(&dxNetAddr, 0x00, sizeof(dxNetAddr));

    memcpy(&dxNetAddr.mac_addr[0], LwIP_GetMAC(&xnetif[0]), sizeof(dxNetAddr.mac_addr));

    dxNetAddr.ip_addr.version = DXIP_VERSION_V4;
    memcpy(&dxNetAddr.ip_addr.v4[0], LwIP_GetIP(&xnetif[0]), sizeof(dxNetAddr.ip_addr.v4));

    dxNetAddr.netmask.version = DXIP_VERSION_V4;
    memcpy(&dxNetAddr.ip_addr.v4[0], LwIP_GetIP(&xnetif[0]), sizeof(dxNetAddr.ip_addr.v4));

    memcpy(&dxNetAddr.netmask.v4[0], LwIP_GetMASK(&xnetif[0]), sizeof(dxNetAddr.netmask.v4));

    dxNetAddr.gw.version = DXIP_VERSION_V4;
    memcpy(&dxNetAddr.gw.v4[0], LwIP_GetGW(&xnetif[0]), sizeof(dxNetAddr.gw.v4));

    return dxNetAddr;
}
