//============================================================================
// File: dxSSL.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/08
//     1) Description: Initial
//
//============================================================================
#include "dxSSL.h"
#include "dxSysDebugger.h"

#define POLAR_SSL_MAX_WRITE_ATTEMPTS                3

//============================================================================
// SSL/TLS Client
//============================================================================
#define DEBUG_LEVEL             0
#define SDRAM_BUFFER_ALLOC_THRESHOLD    (16 * 1024)


static dxSemaphoreHandle_t  dxSSL_Resource_SemaHandle = NULL;
static size_t min_heap_size = 0;

static void dxDebug(void *ctx, int level, const char *str)
{
    if (level <= DEBUG_LEVEL) {
        printf("\r\n%s", str);
    }
}

static unsigned int arc4random(void)
{
    unsigned int res = xTaskGetTickCount();
    static unsigned int seed = 0xDEADB00B;

    seed = ((seed & 0x007F00FF) << 7) ^
           ((seed & 0x0F80FF00) >> 8) ^ // be sure to stir those low bits
           (res << 13) ^ (res >> 9);    // using the clock too!

    return seed;
}

static void get_random_bytes(void *buf, size_t len)
{
    unsigned int ranbuf;
    unsigned int *lp;
    int i, count;
    count = len / sizeof(unsigned int);
    lp = (unsigned int *) buf;

    for (i = 0; i < count; i ++) {
        lp[i] = arc4random();
        len -= sizeof(unsigned int);
    }

    if (len > 0) {
        ranbuf = arc4random();
        memcpy(&lp[i], &ranbuf, len);
    }
}

static int dxRandom(void *p_rng, unsigned char *output, size_t output_len)
{
    get_random_bytes(output, output_len);

    return 0;
}

void *dxMalloc(size_t size) {

    void *ptr = NULL;

    if (size >= SDRAM_BUFFER_ALLOC_THRESHOLD) {
        ptr = dxMemAlloc (DK_MEM_ZONE_SLOW, 1, size);
        G_SYS_DBG_LOG_NV ("[DBG] allocate buffer %X in SDRAM\n", ptr);
    } else {
        ptr = dxMemAlloc (DK_MEM_ZONE_FAST, 1, size);
        size_t current_heap_size = xPortGetFreeHeapSize ();

        if ((current_heap_size < min_heap_size) || (min_heap_size == 0)) {
            min_heap_size = current_heap_size;
        }
    }

    return ptr;
}

void dxFree(void *pv) {
    if (pv) {
        dxMemFree(pv);
    }
}

void dxSSL_Lock(void)
{
    if (dxSSL_Resource_SemaHandle)
    {
        // To protect SIMULTANEOUS ACCESS of hardware crypto engine
        dxSemTake(dxSSL_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    }
}

void dxSSL_Unlock(void)
{
    if (dxSSL_Resource_SemaHandle)
    {
        // Release control
        dxSemGive(dxSSL_Resource_SemaHandle);
    }
}

dxSSL_RET_CODE dxSSL_Close (dxSSLHandle_t *dxSSL)
{
    dxSSL_RET_CODE ret = DXSSL_ERROR;
    int err = 0;

    if (dxSSL == NULL || /*dxSSL.ssl == NULL || */dxSSL->sfd < 0) {
        return ret;
    }

    err = ssl_close_notify(&dxSSL->ssl);

    if (err == 0) {
        ret = DXSSL_SUCCESS;
    }
    else {
        // For more detail error code, please refer to ssl.h located in
        // 'component\common\network\ssl\polarssl-1.3.8\include\polarssl'
        ret = DXSSL_ERROR;
    }

    if (dxSSL->ca_crt != NULL) {
        x509_crt_free (dxSSL->ca_crt);
        polarssl_free (dxSSL->ca_crt);
        dxSSL->ca_crt = NULL;
    }
    net_close(dxSSL->sfd);
    ssl_free(&dxSSL->ssl);

    dxFree(dxSSL);

    return ret;
}

dxSSLHandle_t* dxSSL_Connect (
    const dxIP_Addr_t* host_ip,
    uint16_t port,
    const char* cert,
    uint8_t endpoint,
    uint8_t verify
) {
    dxSSLHandle_t* dxSSL;
    x509_crt* ca_crt = NULL;
    int ret = DXSSL_ERROR;
    int sfd = 0;
    char szIPv4[16] = {0};

    if (host_ip == NULL) {
        goto fail_alloc_ssl;
    }

    dxSSL = dxMalloc (sizeof (dxSSLHandle_t));
    if (dxSSL == NULL) {
        goto fail_alloc_ssl;
    }

    if (dxSSL_Resource_SemaHandle == NULL) {
        // See dxSPIaccess_ResourceTake header description for more detail why is this needed!
        dxSSL_Resource_SemaHandle = dxSemCreate (
            1,       // uint32_t uxMaxCount
            1        // uint32_t uxInitialCount
        );
    }

    if ((host_ip->version & DXIP_VERSION_V4) == 0) {
        G_SYS_DBG_LOG_V ("Host is NOT a IPv4 address\n");
        goto fail_connet_ssl;
    };

    sprintf (
        szIPv4,
        "%d.%d.%d.%d",
        host_ip->v4[0],
        host_ip->v4[1],
        host_ip->v4[2],
        host_ip->v4[3]
    );

    ret = net_connect (&sfd, &szIPv4[0], port);
    if (ret < 0) {
        G_SYS_DBG_LOG_V ("net_connect(): %d\n", ret);
        goto fail_connet_ssl;
    }

    dxSSL->sfd = sfd;

    ret = ssl_init (&dxSSL->ssl);
    if (ret < 0) {
        G_SYS_DBG_LOG_V ("ssl_init(): %d\n", ret);
        goto fail_alloc_cert;
    }

    ssl_set_endpoint (&dxSSL->ssl, endpoint);
    ssl_set_authmode (&dxSSL->ssl, verify);
    ssl_set_rng (&dxSSL->ssl, dxRandom, NULL);

    ssl_set_dbg (&dxSSL->ssl, dxDebug, NULL);
    ssl_set_bio (&dxSSL->ssl, net_recv, &dxSSL->sfd, net_send, &dxSSL->sfd);

    if (cert != NULL) {
        ca_crt = (x509_crt*) polarssl_malloc (sizeof (x509_crt));
        if (ca_crt == NULL) {
            G_SYS_DBG_LOG_V ("Fail to alloc x509_crt\n");
            goto fail_alloc_cert;
        }

        x509_crt_init (ca_crt);
        if (x509_crt_parse (ca_crt, cert, strlen (cert)) != 0) {
            G_SYS_DBG_LOG_V ("Fail to parse x509_crt\n");
            goto fail_parse_cert;
        }
    }

    dxSSL->ca_crt = ca_crt;

    while ((ret = ssl_handshake(&dxSSL->ssl)) != 0) {
        if (ret != POLARSSL_ERR_NET_WANT_READ && ret != POLARSSL_ERR_NET_WANT_WRITE) {
            G_SYS_DBG_LOG_V("ssl_handshake() failed, returned -0x%x\n", -ret);
            break;
        }
    }

    if (ret < 0) {
        goto fail_parse_cert;
    }

    G_SYS_DBG_LOG_V("Use ciphersuite %s (0x%X)\n", ssl_get_ciphersuite(&dxSSL->ssl), dxSSL->ssl.session->ciphersuite);

    return dxSSL;

fail_parse_cert :

    if (ca_crt != NULL) {
        x509_crt_free (ca_crt);
        polarssl_free (ca_crt);
        ca_crt = NULL;
    }

fail_alloc_cert :

    if (dxSSL->sfd != 0) {
        net_close (dxSSL->sfd);
        dxSSL->sfd = 0;
    }
    ssl_free (&dxSSL->ssl);

fail_connet_ssl :

    /**
     * WARNING:
     * If we use the following 3 lines to free memory we allocated,
     * System will crash here.
     * In this case, we see
     * 1) xPortGetFreeHeapSize() increase from 52272 to 65120 bytes
     * 2) A message will be shown in UART console,
     *    "RTL8195A[Driver]: sta recv deauth reason code(7) sta:5c:f9:38:96:51:aa"
     */
    if (dxSSL != NULL) {
        dxFree (dxSSL);
        dxSSL = NULL;
    }
    // For more detail error code, please refer to net.h located in
    // 'component\common\network\ssl\polarssl-1.3.8\include\polarssl'

fail_alloc_ssl :

    return NULL;
}

int dxSSL_Receive(dxSSLHandle_t *dxSSL, void *mem, size_t len)
{
    int ret = DXSSL_ERROR;
    int err = 0;

    if (dxSSL == NULL || mem == NULL || len <= 0) {
        return ret;
    }

    // To protect SIMULTANEOUS ACCESS of hardware crypto engine
    dxSemTake(dxSSL_Resource_SemaHandle, DXOS_WAIT_FOREVER);

    err = ssl_read(&dxSSL->ssl, mem, len);

    // Release control
    dxSemGive(dxSSL_Resource_SemaHandle);

    if (err < 0) {
        // For more detail error code, please refer to ssl.h located in
        // 'component\common\network\ssl\polarssl-1.3.8\include\polarssl'
        ret = DXSSL_ERROR;
    }
    else if (err == 0) {
        ret = DXSSL_SUCCESS;
    }
    else {
        ret = err;
    }

    return ret;
}

int dxSSL_ReceiveTimeout(dxSSLHandle_t *dxSSL, void *mem, size_t len, uint32_t msec)
{
    int ret = DXSSL_ERROR;
    int err = 0;

    if (dxSSL == NULL || mem == NULL || len <= 0) {
        return ret;
    }

    /**
     * How to use setsockopt() ?
     * (1) For lwIP 1.4.1 and before,
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (const char *)&msec, sizeof(msec));
     *
     * (2) For lwIP 1.5.0 and later,
     *     struct timeval interval = { .tv_sec = msec / 1000,
     *                                 .tv_usec = (msec % 1000) * 1000 };
     *
     *     setsockopt(dxSSL->sfd, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&interval, sizeof(struct timeval));
     */

    dxSOCKET_SetTimeout(dxSSL->sfd, msec);

    // To protect SIMULTANEOUS ACCESS of hardware crypto engine
    dxSemTake(dxSSL_Resource_SemaHandle, DXOS_WAIT_FOREVER);

    err = ssl_read(&dxSSL->ssl, mem, len);

    // Release control
    dxSemGive(dxSSL_Resource_SemaHandle);

    if (err < 0) {
        // For more detail error code, please refer to ssl.h located in
        // 'component\common\network\ssl\polarssl-1.3.8\include\polarssl'
        G_SYS_DBG_LOG_V("dxSSL_ReceiveTimeout err %d \r\n", err);
        ret = DXSSL_ERROR;
    }
    else if (err == 0) {
        ret = DXSSL_SUCCESS;
    }
    else {
        ret = err;
    }

    return ret;
}

int dxSSL_Send(dxSSLHandle_t *dxSSL, const void *dataptr, size_t size)
{
    int ret = DXSSL_ERROR;
    int err = 0;
    int writeAttempts;

    if (dxSSL == NULL || dataptr == NULL || size <= 0) {
        return ret;
    }

    // To protect SIMULTANEOUS ACCESS of hardware crypto engine
    dxSemTake(dxSSL_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    writeAttempts = 0;

    while ((err = ssl_write(&dxSSL->ssl, dataptr, size)) <= 0) {
        writeAttempts++;
        if(writeAttempts >= POLAR_SSL_MAX_WRITE_ATTEMPTS)
        {
            G_SYS_DBG_LOG_V("ssl_write() failed for %d attempts, returned %d\n", writeAttempts, err);
            break;
        }
    }

    // Release control
    dxSemGive(dxSSL_Resource_SemaHandle);

    if (err < 0) {
        // For more detail error code, please refer to ssl.h located in
        // 'component\common\network\ssl\polarssl-1.3.8\include\polarssl'
        ret = DXSSL_ERROR;
    }
    else if (err == 0) {
        ret = DXSSL_SUCCESS;
    }
    else {
        ret = err;
    }


    return ret;
}
