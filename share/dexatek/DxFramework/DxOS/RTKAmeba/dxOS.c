//============================================================================
// File: dxOS.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2015/12/12
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2015/12/31
//     1) Description: Added two functions
//        Added: dxWiFi_IsOnline()
//               dxWiFi_WaitForOnline()
//
//     Version: 0.3
//     Date: 2016/01/19
//     1) Description: Revise all API's parameters and return code
//
//     Version: 0.4
//     Date: 2016/01/19
//     1) Description: Added a new API
//        Added:
//            - dxQueueIsEmpty()
//
//     Version: 0.5
//     Date: 2016/01/20
//     1) Description: Revise API's return code
//        Modified:
//            - All APIs list in DHCP Client, DNS Client, TCPIP Client/Server,
//              UDP Client/Server and Multicast UDP section
//
//     Version: 0.6
//     Date: 2016/01/20
//     1) Description: Added new API
//        Added:
//            - dxQueueIsFull()
//            - dxWiFi_GetMAC()
//            - dxTimerReset()
//            - dxTimerRestart()
//
//     Version: 0.7
//     Date: 2016/01/20
//     1) Description: Change the stack size from 'number of variables' to 'number of bytes'
//        Modified:
//            - dxTaskCreate()
//
//     Version: 0.8
//     Date: 2016/01/20
//     1) Description: Move dxNetIf_Init() into dxWiFi_on()
//        Deleted:
//            - dxNetIf_Init()
//        Modified:
//            - dxWiFi_on()
//
//     Version: 0.9
//     Date: 2016/01/21
//     1) Description: Declare a new data type, dxTime_t
//        Modified: Change function's return data type to dxTime_t
//            - dxTimeGetMS()
//
//     Version: 0.10
//     Date: 2016/01/26
//     1) Description: Fix issue with worker thread event
//        Modified:
//            - dkworker_task_main()
//     2) Description: Revise function prototype
//        Modified:
//            - dxSSL_Connect()
//
//     Version: 0.11
//     Date: 2016/02/01
//     1) Description: Revise function
//        Modified:
//            - dxTimerRestart()
//
//     Version: 0.12
//     Date: 2016/02/15
//     1) Description: Move dxFlash_XXXX() and dxOTA_YYYY() to dxBSP
//
//     Version: 0.13
//     Date: 2016/02/18
//     1) Description: Make sure allocated memory are clear
//        Modified:
//            - dxMemAlloc()
//            - dxMemReAlloc()
//
//     Version: 0.14
//     Date: 2016/03/25
//     1) Description:  Rock : Add dxMCASTxxxEX() api
//
//     Version: 0.15
//     Date: 2016/05/06
//     1) Description: Fixed issue with socket read timeout
//            - dxSSL_ReceiveTimeout()
//
//     Version: 0.16
//     Date: 2016/05/11
//     1) Description: Fixed issue with setsockopt()
//        Modified:
//            - dxSSL_ReceiveTimeout()
//
//     Version: 0.17
//     Date: 2016/05/13
//     1) Description: Added socket timeout function
//        Modified:
//            - dxSOCKET_SetTimeout()
//
//     Version: 0.18
//     Date: 2016/06/27
//     1) Description: Rivise Multicast/UDP API and added related handle, struct
//        Modified:
//            - dxMCAST_Init()
//            - dxMCAST_Join()
//            - dxMCAST_Leave()
//            - dxMCAST_RecvFrom()
//            - dxMCAST_SendTo()
//
//     Version: 0.19
//     Date: 2016/07/06
//     1) Description: Added semaphore to protect RTK hardware crypto engine
//        Added:
//            - dxOS_Init()
//        Modified:
//            - dxSSL_Receive()
//            - dxSSL_ReceiveTimeout()
//            - dxSSL_Send()
//     Version: 0.20
//     Date: 2016/08/10
//     1) Description: Added SW AES
//        Added:
//            - dxAESSW_ContextAlloc()
//            - dxAESSW_Setkey()
//            - dxAESSW_CryptEcb()
//            - dxAESSW_ContextDealloc()
//
//     Version: 0.21
//     Date: 2017/04/10
//     1) Description: Support new WiFi scan function
//        Added:
//            - dxWiFi_Scan_Done_HDL()
//            - dxWiFi_GetScanState()
//        Modified:
//            - dxWiFi_Scan()
//
//     Version: 0.22
//     Date: 2017/07/19
//     1) Description: Revise Wi-Fi API to support initialize Wi-Fi driver with
//                     channel plan and adaptivity
//        Added:
//            - dxWiFi_On_Ext()
//        Modified:
//            - dxWiFi_On()
//
//     Version: 0.23
//     Date: 2017/09/28
//     1) Description: Revise semaphore used for RTC read and write
//        Modified:
//            - dxOS_Init()
//            - dxSwRtcGet()
//            - dxSwRtcSet()
//     2) Description: Revise ssl_write infinite loop possibility
//        Modified:
//            - dxSSL_Send()
//
//     Version: 0.24
//     Date: 2018/03/16
//     1) Description: Implement HW RTC features
//        Modified:
//            - dxHwRtcGet()
//            - dxHwRtcSet()
//============================================================================

#include <limits.h>

#include "dxOS.h"
#include "dxSysDebugger.h"
#include "dxBSP.h"
#include "tiny-AES128.h"
#include "Utils.h"



//============================================================================
// Defines and Macro
//============================================================================

#define PO10_LIMIT (ULONG_MAX / 10)
#define SDRAM_HEAP_START (uint8_t*)0x30100000       // Caluculate from (0x30200000 - DX_SDRAM_HEAP_SIZE)



//============================================================================
// Type Definitions
//============================================================================



//============================================================================
// Main Initialization
//============================================================================

/*static*/ dxSemaphoreHandle_t  dxSPI_Resource_SemaHandle = NULL;
/*static*/ dxSemaphoreHandle_t  dxRTC_Resource_SemaHandle = NULL;
/*static*/ dxSemaphoreHandle_t  dxGlobalQueueReceiveSemaphore = NULL;
/*static*/ dxSemaphoreHandle_t  dxGlobalQueueSendSemaphore = NULL;

#ifdef DK_MEMORY_DEBUG
#pragma default_variable_attributes = @ ".sdram.text"
static MemoryRecord memoryRecords[DK_MEMORY_DEBUG_RECORD_NUM];
static List_t memoryList;
static uint8_t needMoreRecords = 0;
static char fileName[256] = "";
static char functionName[256] = "";
#pragma default_variable_attributes =
#endif



dxOS_RET_CODE dxOS_Init(void)
{
    dxOS_RET_CODE ret = DXOS_ERROR;
#ifdef DK_MEMORY_DEBUG
    MemoryRecord* record;
    int i;
#endif

    if (dxSPI_Resource_SemaHandle == NULL) {
        // See dxSPIaccess_ResourceTake header description for more detail why is this needed!
        dxSPI_Resource_SemaHandle = dxSemCreate(1,          // uint32_t uxMaxCount
                                                1);         // uint32_t uxInitialCount
    }

    if (dxRTC_Resource_SemaHandle == NULL) {
        // Protection for preventing RTC resource reading and writing at the same time
        dxRTC_Resource_SemaHandle = dxSemCreate(1,          // uint32_t uxMaxCount
                                                1);         // uint32_t uxInitialCount
    }

    if (dxGlobalQueueReceiveSemaphore == NULL) {
        // To make sure only one thread access dxGlobalQueueList in dxQueueReceive()
        dxGlobalQueueReceiveSemaphore = dxSemCreate(1,          // uint32_t uxMaxCount
                                                    1);         // uint32_t uxInitialCount
    }

    if (dxGlobalQueueSendSemaphore == NULL) {
        // To make sure only one thread access dxGlobalQueueList in dxQueueSendWithSize()
        dxGlobalQueueSendSemaphore = dxSemCreate(1,          // uint32_t uxMaxCount
                                                 1);         // uint32_t uxInitialCount
    }

    FirstFit_Init (SDRAM_HEAP_START, DX_SDRAM_HEAP_SIZE);

#ifdef DK_MEMORY_DEBUG
    vListInitialise (&memoryList);
    memset (memoryRecords, 0, sizeof (MemoryRecord) * DK_MEMORY_DEBUG_RECORD_NUM);
    for (i = 0; i < DK_MEMORY_DEBUG_RECORD_NUM; i++) {
        record = &memoryRecords[i];
        vListInitialiseItem (&record->item);
        listSET_LIST_ITEM_OWNER (&record->item, record);
    }
#endif

    if (dxSPI_Resource_SemaHandle && dxRTC_Resource_SemaHandle) {
        ret = DXOS_SUCCESS;
    }

    return ret;
}



//============================================================================
// Memory Management
//============================================================================

#define DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT        32  //Byte alignment


size_t dxMemGetFreeHeapSize (void)
{
    return xPortGetFreeHeapSize();
}


#ifdef DK_MEMORY_DEBUG

#pragma optimize=speed
dxOS_RET_CODE dxMemFreeDbg (
    const char*     file,
    const char*     function,
    const int       line,
    void*           memoryToFree
) {
    MemoryRecord* record = NULL;
    ListItem_t const* endItem;
    ListItem_t* item;
    uint8_t* memory;
    uint32_t alignSize;

    vTaskSuspendAll ();

    if (!memoryToFree) {
        goto fail;
    }

    memory = (uint8_t*) memoryToFree - 4;
    alignSize = *((uint32_t*) memory);

    endItem = listGET_END_MARKER (&memoryList);
    item = listGET_HEAD_ENTRY (&memoryList);
    while (item != endItem) {
        record = (MemoryRecord*) listGET_LIST_ITEM_OWNER (item);
        if ((record->used != 0) && (record->memory == memory)) {
            record->used = 0; // NOTE: for free
            uxListRemove (item);
            break;
        }
        item = item->pxNext;
    }

    if (record != NULL) {
        /* Check if the magic number is corrupted */
        if ((record->size != alignSize) || (*((uint32_t*) (memory + 4 + alignSize)) != DK_MEM_MAGIC_NUMBER)) {
            TaskHandle_t* current;
            dxTaskStack_t stackBuf;
            dxTime_t time = dxTimeGetMS ();
            dxTime_t diff;
            char* p = NULL;

            /* Check if the time is overflow */
            if (record->overflow) {
                // time overflow
                diff = DK_MEMORY_DEBUG_TIMEDIFF_MAX;
            } else {
                if (time_after (time, record->time)) {
                    if (time >= record->time) {
                        diff = time - record->time;
                    } else {
                        diff = 0xFFFFFFFF - record->time + time;
                    }
                } else {
                    diff = 0;
                }

                // time overflow
                if (diff >= DK_MEMORY_DEBUG_TIMEDIFF_MAX) {
                    diff = DK_MEMORY_DEBUG_TIMEDIFF_MAX;
                    record->overflow = 1;
                }
            }

            printf ("\r\n=========================MEMORY FREE ERROR=============================\r\n");

            if (record->current != NULL) {
                memset (&stackBuf, 0, sizeof (stackBuf));
                dxTaskGetStackInfo (record->current, &stackBuf);

                printf (
                    "[X] at T%u(%us ago) %s(%u) allocate 0x%x(%u bytes) from %s(%s, %d)\r\n",
                    record->time,
                    diff / SECONDS,
                    stackBuf.szTaskName,
                    record->current,
                    record->memory,
                    record->size,
                    record->function,
                    record->file,
                    record->line
                );
            } else {
                printf (
                    "[X] at T%u(%us ago) task(NA) allocate 0x%x(%u bytes) from %s(%s, %d)\r\n",
                    record->time,
                    diff / SECONDS,
                    record->memory,
                    record->size,
                    record->function,
                    record->file,
                    record->line
                );
            }

#ifdef __unix__
            p = strrchr (file, '/');
#elif __linux__
            p = strrchr (file, '/');
#elif __APPLE__
            p = strrchr (file, '/');
#else
            p = strrchr (file, '\\');
#endif
            if (p) {
                p = p + 1;
            } else {
                p = (char*) file;
            }

            current = (TaskHandle_t*) vTaskGetCurrentTCB ();
            if (current != NULL) {
                memset (&stackBuf, 0, sizeof (stackBuf));
                dxTaskGetStackInfo (current, &stackBuf);

                printf (
                    "--> at T%u(%us ago) %s(%u) free 0x%x(%u bytes) magic 0x%x from %s(%s, %d)\r\n",
                    time,
                    0,
                    stackBuf.szTaskName,
                    current,
                    record->memory,
                    alignSize,
                    (record->size != alignSize) ? 0 : *((uint32_t*) (memory + 4 + alignSize)),
                    function,
                    p,
                    line
                );
            } else {
                printf (
                    "--> at T%u(%us ago) task(NA) free 0x%x(%u bytes) magic 0x%x from %s(%s, %d)\r\n",
                    time,
                    0,
                    record->memory,
                    alignSize,
                    (record->size != alignSize) ? 0 : *((uint32_t*) (memory + 4 + alignSize)),
                    function,
                    p,
                    line
                );
            }

            printf ("===============================END=====================================\r\n\r\n");

            while (1) dxTimeDelayMS (500);
        }
    }

    // Check if the memory is allocated from SDRAM_HEAP_START
    if ((memory > SDRAM_HEAP_START) && (memory < SDRAM_HEAP_START + DX_SDRAM_HEAP_SIZE)) {
        FirstFit_Free (memory);
    } else {
        free (memory);
    }

fail :

    (void) xTaskResumeAll ();

    return DXOS_SUCCESS;
}


void* dxMemAllocDbg (
    const char*     file,
    const char*     function,
    const int       line,
    char*           zone,
    size_t          nelems,
    size_t          size
) {
    MemoryRecord* record;
    TaskHandle_t* current = NULL;
    uint32_t alignSize = align (nelems * size, DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT);
    size_t length;
    int i;
    uint8_t* memory = NULL;
    char* p = NULL;

    vTaskSuspendAll ();

    if (alignSize == 0) {
        goto fail;
    }

    // Allocate memory by memory zone
    if ((strlen (zone) >= 4) && (strncmp (zone, DK_MEM_ZONE_SLOW, 4) == 0)) {
        memory = (uint8_t*) FirstFit_Malloc (alignSize + 8); // Add 8 bytes for size and magic number
        assert ((memory > SDRAM_HEAP_START) && (memory < SDRAM_HEAP_START + DX_SDRAM_HEAP_SIZE));
        if (memory) {
            memset (memory, 0, alignSize + 8);
        }
    } else {
        memory = (uint8_t*) calloc (1, alignSize + 8); // Add 8 bytes for size and magic number
    }
    if (memory == NULL) {
        goto fail;
    }

    *((uint32_t*) memory) = alignSize; // Remember size
    *((uint32_t*) (memory + 4 + alignSize)) = DK_MEM_MAGIC_NUMBER; // Write magic number after

    record = NULL;
    for (i = 0; i < DK_MEMORY_DEBUG_RECORD_NUM; i++) {
        if (memoryRecords[i].used == 0) {
            record = &memoryRecords[i];
            break;
        }
    }

    if (record == NULL) {
        needMoreRecords = 1;
        goto fail;
    }

#ifdef __unix__
    p = strrchr (file, '/');
#elif __linux__
    p = strrchr (file, '/');
#elif __APPLE__
    p = strrchr (file, '/');
#else
    p = strrchr (file, '\\');
#endif
    if (p) {
        p = p + 1;
    } else {
        p = (char*) file;
    }
    record->current = (TaskHandle_t*) vTaskGetCurrentTCB ();
    record->time = dxTimeGetMS ();
    record->line = line;
    record->overflow = 0;
    record->used = 1;  // NOTE: for allocate
    length = strlen (p) > 63 ? 63 : strlen (p);
    memcpy (record->file, p, length);
    record->file[length] = '\0';
    length = strlen (function) > 63 ? 63 : strlen (function);
    memcpy (record->function, function, length);
    record->function[length] = '\0';
    record->size = alignSize;
    record->memory = memory;
    memory += 4; // Bypass the size (4 bytes)

    vListInsertEnd (&memoryList, &record->item);

fail :

    (void) xTaskResumeAll ();

    return memory;
}


void* dxMemReAllocDbg (
    const char*     file,
    const char*     function,
    const int       line,
    char*           zone,
    void*           memoryToFree,
    size_t          size
) {
    dxMemFreeDbg (file, function, line, memoryToFree);
    return dxMemAllocDbg (file, function, line, zone, 1, size);
}


dxOS_RET_CODE dxMemGetRecords (
    List_t**        list,
    uint16_t*       size,
    uint8_t*        needMore
) {
    *list = &memoryList;
    *size = DK_MEMORY_DEBUG_RECORD_NUM;
    *needMore = needMoreRecords;

    return DXOS_SUCCESS;
}
#pragma optimize=

#else

dxOS_RET_CODE dxMemFree (void* memoryToFree) {

    if (!memoryToFree) {
        goto fail;
    }

    // Check if the memory is allocated from SDRAM_HEAP_START
    if ((memoryToFree > SDRAM_HEAP_START) && (memoryToFree < SDRAM_HEAP_START + DX_SDRAM_HEAP_SIZE)) {
        FirstFit_Free (memoryToFree);
    } else {
        free (memoryToFree);
    }

fail :

    return DXOS_SUCCESS;
}


void* dxMemAlloc (char* zone, size_t nelems, size_t size) {
    uint32_t alignSize = align (nelems * size, DX_MEM_ALLOC_SIZE_PACK_ALIGNMENT);
    size_t len;
    uint8_t* memory = NULL;

    // Allocate memory by memory zone
    if ((strlen (zone) >= 4) && (strncmp (zone, DK_MEM_ZONE_SLOW, 4) == 0)) {
        memory = (uint8_t*) FirstFit_Malloc (alignSize);
        assert ((memory > SDRAM_HEAP_START) && (memory < SDRAM_HEAP_START + DX_SDRAM_HEAP_SIZE));
        if (memory) {
            memset (memory, 0, alignSize);
        }
    } else {
        memory = (uint8_t*) calloc (1, alignSize);
    }

    return (void*) memory;
}


void* dxMemReAlloc (char* zone, void* memoryToFree, size_t size) {
    dxMemFree (memoryToFree);
    return dxMemAlloc (zone, 1, size);
}

#endif


//============================================================================
// Timer Management
//============================================================================
dxTimerHandle_t dxTimerCreate(const char * pcTimerName,
                              const uint32_t dxTimerPeriodInMs,
                              const dxBOOL uxAutoReload,
                              const void * pvTimerID,
                              dxTimerCallbackFunction_t pxCallbackFunction)
{
    dxTimerHandle_t dxTimerHandler;
    TimerCallbackFunction_t pxTimerCallback = (TimerCallbackFunction_t)pxCallbackFunction;
    UBaseType_t autoreload = (uxAutoReload == dxTRUE) ? pdTRUE : pdFALSE;

    dxTimerHandler = (dxTimerHandle_t)xTimerCreate(pcTimerName,
                                                   (dxTimerPeriodInMs/portTICK_PERIOD_MS),
                                                   autoreload,
                                                   pvTimerID,
                                                   pxTimerCallback);
    return dxTimerHandler;
}

void* dxTimerGetTimerID( dxTimerHandle_t dxTimer )
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    return pvTimerGetTimerID( xTimer );
}

dxOS_RET_CODE dxTimerStart(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    BaseType_t ret = xTimerStart(xTimer, dxMsToWait/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxTimerStop(dxTimerHandle_t dxTimer,
                          const uint32_t dxMsToWait)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    BaseType_t ret = xTimerStop(xTimer, dxMsToWait/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxTimerDelete(dxTimerHandle_t dxTimer,
                         const uint32_t dxMsToWait)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    BaseType_t ret = xTimerDelete(xTimer, dxMsToWait/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxBOOL dxTimerIsTimerActive(dxTimerHandle_t dxTimer)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    BaseType_t ret = xTimerIsTimerActive(xTimer);

    return (ret == pdFALSE) ? dxFALSE : dxTRUE;
}

dxOS_RET_CODE dxTimerReset(dxTimerHandle_t dxTimer,
                           const uint32_t dxMsToWait)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;

    BaseType_t ret = xTimerReset(xTimer, dxMsToWait/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxTimerRestart(dxTimerHandle_t dxTimer,
                             const uint32_t dxTimerPeriodInMs,
                             const uint32_t dxMsToWait)
{
    TimerHandle_t xTimer = (TimerHandle_t)dxTimer;
    BaseType_t ret = pdFAIL;

    if (dxTimerPeriodInMs == DX_TIMER_PERIOD_NO_CHANGE)
    {
        ret = xTimerReset(xTimer, dxMsToWait/portTICK_PERIOD_MS);
    }
    else
    {
        ret = xTimerChangePeriod( xTimer, dxTimerPeriodInMs/portTICK_PERIOD_MS, dxMsToWait/portTICK_PERIOD_MS );
    }

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

//============================================================================
// Time Information
//============================================================================
dxOS_RET_CODE dxTimeDelayMS(const uint32_t xMSToDelay)
{
    vTaskDelay(xMSToDelay / portTICK_PERIOD_MS);

    return DXOS_SUCCESS;
}

dxTime_t dxTimeGetMS(void)
{
    uint32_t value = xTaskGetTickCount() / portTICK_PERIOD_MS;
    return (dxTime_t)value;
}

static DKTime_t                 SwRtcTime;
static dxTime_t                 SwRtcTimeSincesystemTime = 0;

#if HW_RTC_SUPPORT
#define IntToPericom(i)         (((i - (i % 10)) / 10) * 0x10 + (i % 10))
#define IntToPericom24H(i)      ((((i - (i % 10)) / 10) * 0x10 + (i % 10)) | 0x40)
#define PericomToInt(i)         (((i - (i % 0x10)) / 0x10) * 10 + (i % 0x10))
#define PericomToInt24H(i)      ((((i & 0xBF) - ((i & 0xBF) % 0x10)) / 0x10) * 10 + ((i & 0xBF) % 0x10))
#endif  //#if HW_RTC_SUPPORT

dxOS_RET_CODE dxHwRtcGet(DKTime_t* TimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    //TODO: We need an mechanism to determine if this device has HW RTC or not
    //For example, old RTK gateways does not have RTC while new gateway or other hybridperipheral device using RTK may also contain HW RTC.
    //Since dxOS is shared amoung all project as main framework we will need either determine here or use project config defines

#if HW_RTC_SUPPORT
    uint8_t         i2cdata_write[1] = {0x0};
    uint8_t         i2cdata_read[0x10];

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    memset(i2cdata_read, 0x00, 0x10);
    dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 1, 1);
    int iReadRes = dxI2C_Master_Read(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_read, 0x10, 1);
    if(iReadRes == 0x10)
    {
        // Data is vallid only if OSF bit == 0x0
        if ((i2cdata_read[0xF] & 0x80) == 0x0)
        {
            result = DXOS_SUCCESS;
            TimeRTC->sec = PericomToInt(i2cdata_read[0]);
            TimeRTC->min = PericomToInt(i2cdata_read[1]);
            TimeRTC->hour = PericomToInt24H(i2cdata_read[2]);
            TimeRTC->dayofweek = i2cdata_read[3];
            TimeRTC->day = PericomToInt(i2cdata_read[4]);
            TimeRTC->month = PericomToInt(i2cdata_read[5]) & 0x1F;

            // Century has problem, so force to start from 2000
#if 0
            if (i2cdata_read[5] & 0x80 != 0) {
                TimeRTC->year = PericomToInt(i2cdata_read[6]) + 2000;
            } else {
                TimeRTC->year = PericomToInt(i2cdata_read[6]) + 1900;
            }
#else
            TimeRTC->year = PericomToInt(i2cdata_read[6]) + 2000;
#endif

        }
    }
    G_SYS_DBG_LOG_V("RTC Get(%d): %d %d/%d w%d %d:%d:%d OSF:0x%x\n", iReadRes, TimeRTC->year, TimeRTC->month, TimeRTC->day, TimeRTC->dayofweek, TimeRTC->hour, TimeRTC->min, TimeRTC->sec, i2cdata_read[0xF]);
    dxSemGive(dxRTC_Resource_SemaHandle);
#endif  //#if HW_RTC_SUPPORT

    return result;
}

dxOS_RET_CODE dxHwRtcSet(DKTime_t SetTimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    //TODO: We need an mechanism to determine if this device has HW RTC or not
    //For example, old RTK gateways does not have RTC while new gateway or other hybridperipheral device using RTK may also contain HW RTC.
    //Since dxOS is shared amoung all project as main framework we will need either determine here or use project config defines

#if HW_RTC_SUPPORT
    uint8_t         i2cdata_write[8] = {0x0, 0x0, 0x43, 0x09, 0x1, 0x12, 0x03, 0x18};
    uint8_t         i2cdata_read[10];

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    G_SYS_DBG_LOG_V("RTC Set: %d %d/%d w%d %d:%d:%d\n", SetTimeRTC.year, SetTimeRTC.month, SetTimeRTC.day, SetTimeRTC.dayofweek, SetTimeRTC.hour, SetTimeRTC.min, SetTimeRTC.sec);
    i2cdata_write[0] = 0x0;
    i2cdata_write[1] = IntToPericom(SetTimeRTC.sec);
    i2cdata_write[2] = IntToPericom(SetTimeRTC.min);
    i2cdata_write[3] = IntToPericom24H(SetTimeRTC.hour);
    i2cdata_write[4] = SetTimeRTC.dayofweek;
    i2cdata_write[5] = IntToPericom(SetTimeRTC.day);
    if (SetTimeRTC.year >= 2000) {
        i2cdata_write[6] = IntToPericom(SetTimeRTC.month) | 0x80;
    } else {
        i2cdata_write[6] = IntToPericom(SetTimeRTC.month);
    }
    i2cdata_write[7] = IntToPericom(SetTimeRTC.year % 100);

    int iWriteRes = dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 8, 1);
    if(iWriteRes > 0)
    {
        // Reset OSF bit
        i2cdata_write[0] = 0xF;
        i2cdata_write[1] = 0x0;
        iWriteRes = dxI2C_Master_Write(HW_RTC_I2C_SDA, HW_RTC_I2C_SLAVE_ADDR, i2cdata_write, 2, 1);
        if (iWriteRes > 0) {
            result = DXOS_SUCCESS;
        }
    }

    dxSemGive(dxRTC_Resource_SemaHandle);
#endif  //#if HW_RTC_SUPPORT

    DKTime_t aTimeRTC;
    dxHwRtcGet (&aTimeRTC);
    return result;

}

dxOS_RET_CODE dxSwRtcGet(DKTime_t* TimeRTC)
{
    dxOS_RET_CODE result = DXOS_ERROR;
    if (SwRtcTimeSincesystemTime && TimeRTC)
    {
        dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

        int8_t TimeZoneOrg = SwRtcTime.TimeZone;
        dxTime_t rtcTime = ConvertDKTimeToUTC(SwRtcTime);
        dxTime_t time = dxTimeGetMS();

        if (time >= SwRtcTimeSincesystemTime) {
            rtcTime += ((time - SwRtcTimeSincesystemTime) / SECONDS);
        } else {
            rtcTime += ((0xFFFFFFFF - SwRtcTimeSincesystemTime + time) / SECONDS);
        }

        DKTime_t rtcDateTime = ConvertUTCToDKTime(rtcTime);
        memcpy(TimeRTC, &rtcDateTime, sizeof(DKTime_t));
        TimeRTC->TimeZone = TimeZoneOrg;

        dxSemGive(dxRTC_Resource_SemaHandle);

        result = DXOS_SUCCESS;
    }

    return result;
}

dxOS_RET_CODE dxSwRtcSet(DKTime_t SetTimeRTC)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;

    dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

    if (SwRtcTimeSincesystemTime) {
        dxTime_t serverTime = ConvertDKTimeToUTC (SetTimeRTC);
        dxTime_t localTime = ConvertDKTimeToUTC (SwRtcTime);
        dxTime_t time = dxTimeGetMS();
        if (time >= SwRtcTimeSincesystemTime) {
            localTime += ((time - SwRtcTimeSincesystemTime) / SECONDS);
        } else {
            localTime += ((0xFFFFFFFF - SwRtcTimeSincesystemTime + time) / SECONDS);
        }
    }

    SwRtcTime = SetTimeRTC;
    SwRtcTimeSincesystemTime = dxTimeGetMS();
    dxSemGive(dxRTC_Resource_SemaHandle);

    return result;
}


dxOS_RET_CODE dxGetLocalTime (
    DKTimeLocal_t* localTime
) {
    dxOS_RET_CODE result = DXOS_ERROR;

    if (SwRtcTimeSincesystemTime && localTime)
    {
        dxSemTake(dxRTC_Resource_SemaHandle, DXOS_WAIT_FOREVER);

        dxTime_t CurrentTimeUTC = ConvertDKTimeToUTC(SwRtcTime);
        int offset = SwRtcTime.TimeZone * 15 * 60;
        CurrentTimeUTC += ((dxTimeGetMS() - SwRtcTimeSincesystemTime) / 1000) + offset;

        DKTimeLocal_t time = ConvertUTCToDKTimeLocal (CurrentTimeUTC);
        memcpy(localTime, &time, sizeof (DKTimeLocal_t));

        dxSemGive(dxRTC_Resource_SemaHandle);

        result = DXOS_SUCCESS;
    }

    return result;
}


DKTime_t dxGetUtcTime (void) {
    DKTime_t TimeRTC;

    memset (&TimeRTC, 0, sizeof (DKTime_t));
    dxOS_RET_CODE ret = dxSwRtcGet (&TimeRTC);
    if (ret != DXOS_SUCCESS) {
        TimeRTC = ConvertUTCToDKTime (dxTimeGetMS () / SECONDS);
    }

    return TimeRTC;
}


uint32_t dxGetUtcInSec (void) {
    DKTime_t TimeRTC;
    uint32_t RTCinSec;

    TimeRTC = dxGetUtcTime ();
    RTCinSec = ConvertDKTimeToUTC (TimeRTC);

    return RTCinSec;
}



//============================================================================
// Queue Management
//============================================================================

#define GLOBAL_QUEUE_MAX_LIST       2
#define GLOBAL_QUEUE_NAME_MAX       64

dxBOOL  bGlobalQueueFirstTimeCreate = dxTRUE;

typedef struct{

    uint32_t    ID;
    uint32_t    MemBuffSize;
    void*       pMemBuffData;

} dxQueueMemBuff_t;

typedef struct{

    char                QueueName[GLOBAL_QUEUE_NAME_MAX];
    uint32_t            QueueDepth;
    dxQueueMemBuff_t*   pQueueMemBuffList;
    dxQueueHandle_t     dxQueue;

} dxGlobalQueue_t;

dxGlobalQueue_t dxGlobalQueueList[GLOBAL_QUEUE_MAX_LIST];

dxQueueHandle_t dxQueueCreate(uint32_t uxQueueLength,
                              uint32_t uxItemSize)
{
    dxQueueHandle_t dxQueueHandler;
    UBaseType_t length = (UBaseType_t)uxQueueLength;
    UBaseType_t size = (UBaseType_t)uxItemSize;

    dxQueueHandler = (dxQueueHandle_t)xQueueCreate(length, size);

    return dxQueueHandler;
}

dxQueueHandle_t dxQueueGlobalCreate(void*    GlobalIdentifier,
                                    uint32_t uxQueueLength,
                                    uint32_t uxItemSize)
{
    dxQueueHandle_t dxQueueHandler = NULL;
    uint8_t i = 0;
    uint8_t FoundAvailableIndex = GLOBAL_QUEUE_MAX_LIST; //Set to max as default meaning available index is NOT found
    char* GlobalQueueName = (char*)GlobalIdentifier;

    if ((GlobalIdentifier) && (strlen(GlobalIdentifier) >= GLOBAL_QUEUE_NAME_MAX))
    {
        G_SYS_DBG_LOG_V( "WARNING: dxQueueGlobalCreate invalid parameter\r\n");
        return NULL;
    }

    if (bGlobalQueueFirstTimeCreate == dxTRUE)
    {
        //CleanUp
        for (i = 0; i < GLOBAL_QUEUE_MAX_LIST; i++)
        {
            memset(dxGlobalQueueList[i].QueueName, 0, GLOBAL_QUEUE_NAME_MAX);
            dxGlobalQueueList[i].dxQueue = NULL;
            dxGlobalQueueList[i].pQueueMemBuffList = NULL;
        }

        bGlobalQueueFirstTimeCreate = dxFALSE;
    }

    for (i = 0; i < GLOBAL_QUEUE_MAX_LIST; i++)
    {
        if (dxGlobalQueueList[i].dxQueue == NULL)
        {
            FoundAvailableIndex = i;
            break;
        }
    }

    if (FoundAvailableIndex != GLOBAL_QUEUE_MAX_LIST)
    {
        memcpy(dxGlobalQueueList[i].QueueName, GlobalQueueName, strlen(GlobalQueueName));
        dxGlobalQueueList[i].QueueDepth = uxQueueLength;
        dxGlobalQueueList[i].pQueueMemBuffList = dxMemAlloc("", dxGlobalQueueList[i].QueueDepth, sizeof(dxQueueMemBuff_t));
        UBaseType_t length = (UBaseType_t)uxQueueLength;
        UBaseType_t size = sizeof(void*);

        dxQueueHandler = (dxQueueHandle_t)xQueueCreate(length, size);
        dxGlobalQueueList[i].dxQueue = dxQueueHandler;
    }
    else
    {
        G_SYS_DBG_LOG_V( "WARNING: dxQueueGlobalCreate fail with maximum global queue list reached\r\n");
    }

    return dxQueueHandler;
}


dxQueueHandle_t dxQueueGlobalGet(void*    GlobalIdentifier)
{
    dxQueueHandle_t dxQueueHandler = NULL;
    uint8_t i = 0;

    for (i = 0; i < GLOBAL_QUEUE_MAX_LIST; i++)
    {
        if (dxGlobalQueueList[i].dxQueue != NULL)
        {
            if (strcmp((char*)GlobalIdentifier, dxGlobalQueueList[i].QueueName) == 0)
            {
                dxQueueHandler = dxGlobalQueueList[i].dxQueue;
                break;
            }
        }
    }

    if (dxQueueHandler == NULL)
    {
        G_SYS_DBG_LOG_V( "WARNING: dxQueueGlobalGet NOT found!\r\n");
    }

    return dxQueueHandler;
}


dxOS_RET_CODE dxQueueReceive(dxQueueHandle_t dxQueue,
                             void *pvBuffer,
                             uint32_t dxMsToWait)
{
    QueueHandle_t xQueue = (QueueHandle_t)dxQueue;
    BaseType_t ret = pdFAIL;
    //Check if this is from global queue
    uint8_t i = 0;
    uint8_t FoundAvailableIndex = GLOBAL_QUEUE_MAX_LIST; //Set to max as default meaning available index is NOT found

    for (i = 0; i < GLOBAL_QUEUE_MAX_LIST; i++)
    {
        if (dxGlobalQueueList[i].dxQueue == dxQueue)
        {
            //G_SYS_DBG_LOG_V( "dxQueueReceive FoundAvailableIndex (%s)\r\n", dxGlobalQueueList[i].QueueName);
            FoundAvailableIndex = i;
            break;
        }
    }

    if (FoundAvailableIndex != GLOBAL_QUEUE_MAX_LIST)
    {
        dxBOOL  GlobalQueueDataIdFound = dxFALSE;
        dxQueueMemBuff_t* pGlobalQueueData;
        ret = xQueueReceive(xQueue, &pGlobalQueueData, dxMsToWait/portTICK_PERIOD_MS);
        if (ret == pdPASS)
        {

            uint8_t j;

            dxSemTake(dxGlobalQueueReceiveSemaphore, DXOS_WAIT_FOREVER);

            for (j = 0; j < dxGlobalQueueList[FoundAvailableIndex].QueueDepth; j++)
            {
                if (pGlobalQueueData->ID == dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].ID)
                {
                    memcpy(pvBuffer, dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].pMemBuffData, dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].MemBuffSize);
                    dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].ID = 0;
                    dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].MemBuffSize = 0;
                    dxMemFree(dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].pMemBuffData);
                    dxGlobalQueueList[FoundAvailableIndex].pQueueMemBuffList[j].pMemBuffData = NULL;
                    GlobalQueueDataIdFound = dxTRUE;
                    break;
                }
            }

            dxSemGive(dxGlobalQueueReceiveSemaphore);

            if (GlobalQueueDataIdFound == dxFALSE)
            {
                G_SYS_DBG_LOG_V( "WARNING:dxQueueReceive global queue data ID not found!?(MUST CHECK THIS ISSUE)\r\n");
                ret = pdFAIL;
            }
        }
    }
    else
    {
        ret = xQueueReceive(xQueue, pvBuffer, dxMsToWait/portTICK_PERIOD_MS);
    }

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxQueueReceiveFromISR(dxQueueHandle_t dxQueue,
                                    void *pvBuffer,
                                    dxBOOL* HigherPriorityTaskWoken)
{
    BaseType_t      xHigherPriorityTaskWoken = pdFALSE;
    QueueHandle_t   xQueue = (QueueHandle_t)dxQueue;

    BaseType_t ret = xQueueReceiveFromISR(xQueue, pvBuffer, &xHigherPriorityTaskWoken);
    if (xHigherPriorityTaskWoken == pdTRUE)
    {
        *HigherPriorityTaskWoken = dxTRUE;
    }

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxQueueSend(dxQueueHandle_t dxQueue,
                          const void * pvItemToQueue,
                          uint32_t dxMsToWait)
{
    QueueHandle_t xQueue = (QueueHandle_t)dxQueue;

    BaseType_t ret = xQueueSend(xQueue, pvItemToQueue, dxMsToWait/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxQueueSendWithSize(  dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    uint32_t     MessageSizeInByte,
                                    uint32_t dxMsToWait)
{
    dxOS_RET_CODE ret = DXOS_ERROR;
    uint8_t i = 0;
    static uint32_t QueueMemId = 1; //Start with 1

    if (dxQueue == NULL)
    {
        G_SYS_DBG_LOG_V( "WARNING: dxQueueSendWithSize given queue is NULL!\r\n");
        return ret;
    }

    if (dxQueueSpacesAvailable(dxQueue) <= 2)
    {
        G_SYS_DBG_LOG_V( "WARNING: dxQueueSendWithSize queue full!\r\n");
        return ret;
    }

    for (i = 0; i < GLOBAL_QUEUE_MAX_LIST; i++)
    {
        if ((dxGlobalQueueList[i].dxQueue == dxQueue) && dxGlobalQueueList[i].pQueueMemBuffList)
        {

            uint8_t j;

            dxSemTake(dxGlobalQueueSendSemaphore, DXOS_WAIT_FOREVER);

            for (j = 0; j < dxGlobalQueueList[i].QueueDepth; j++)
            {
                if (dxGlobalQueueList[i].pQueueMemBuffList[j].pMemBuffData == NULL)
                {
                    dxGlobalQueueList[i].pQueueMemBuffList[j].MemBuffSize = MessageSizeInByte;
                    dxGlobalQueueList[i].pQueueMemBuffList[j].pMemBuffData = dxMemAlloc("",1, MessageSizeInByte);
                    memcpy(dxGlobalQueueList[i].pQueueMemBuffList[j].pMemBuffData, pvItemToQueue, MessageSizeInByte);
                    dxGlobalQueueList[i].pQueueMemBuffList[j].ID = QueueMemId;
                    QueueMemId++;

                    dxQueueMemBuff_t* pToBuffList = &dxGlobalQueueList[i].pQueueMemBuffList[j];
                    ret = dxQueueSend(dxQueue, &pToBuffList, dxMsToWait);

                    break;
                }
            }

            dxSemGive(dxGlobalQueueSendSemaphore);

            break;
        }
    }

    if (ret == DXOS_ERROR)
    {
        //Check if pMemBuffData is not available, note that pMemBuffData should be available if we have normal queue send/received.
        G_SYS_DBG_LOG_V( "WARNING: dxQueueSendWithSize failed! (Check if pMemBuffData is not available)\r\n");
    }

    return ret;
}

dxOS_RET_CODE dxQueueSendFromISR(   dxQueueHandle_t dxQueue,
                                    const void * pvItemToQueue,
                                    dxBOOL* HigherPriorityTaskWoken)
{
    BaseType_t      xHigherPriorityTaskWoken = pdFALSE;
    QueueHandle_t   xQueue = (QueueHandle_t)dxQueue;

    BaseType_t ret = xQueueSendFromISR(xQueue, pvItemToQueue, &xHigherPriorityTaskWoken);

    if (xHigherPriorityTaskWoken == pdTRUE)
    {
        *HigherPriorityTaskWoken = dxTRUE;
    }

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

uint32_t dxQueueSpacesAvailable(const dxQueueHandle_t dxQueue)
{
    const QueueHandle_t xQueue = (QueueHandle_t)dxQueue;

    BaseType_t ret = uxQueueSpacesAvailable(xQueue);

    return (uint32_t)ret;
}

dxOS_RET_CODE dxQueueReset(dxQueueHandle_t dxQueue, dxQueueResetFunction_t dxResetFunction)
{
    void* item = NULL;

    while (dxQueueIsEmpty (dxQueue) != dxTRUE) {
        item = NULL;
        if (dxQueueReceive (dxQueue, &item, 0)) {
            if (item && dxResetFunction) {
                dxResetFunction (item);
            }
        }
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxQueueDelete(dxQueueHandle_t dxQueue)
{
    QueueHandle_t xQueue = (QueueHandle_t)dxQueue;

    vQueueDelete(xQueue);

    return DXOS_SUCCESS;
}

dxBOOL dxQueueIsEmpty(dxQueueHandle_t dxQueue)
{
    QueueHandle_t xQueue = (QueueHandle_t)dxQueue;
    BaseType_t ret;

    taskENTER_CRITICAL();
    ret = xQueueIsQueueEmptyFromISR(xQueue);
    taskEXIT_CRITICAL();

    return (ret == pdTRUE) ? dxTRUE : dxFALSE;
}

dxBOOL dxQueueIsFull(dxQueueHandle_t dxQueue)
{
    QueueHandle_t xQueue = (QueueHandle_t)dxQueue;
    BaseType_t ret;

    taskENTER_CRITICAL();
    ret = xQueueIsQueueFullFromISR(xQueue);
    taskEXIT_CRITICAL();

    return (ret == pdTRUE) ? dxTRUE : dxFALSE;
}

//============================================================================
// Task Management
//============================================================================

dxOS_RET_CODE dxTaskCreate(dxTaskFunction_t pvTaskCode,
                           const char * pcName,
                           uint16_t usStackSizeInByte,
                           void *pvParameters,
                           dxTaskPriority_t uxPriority,
                           dxTaskHandle_t *pvCreatedTask)
{
    TaskFunction_t pvTaskFunction = (TaskFunction_t)pvTaskCode;
    TaskHandle_t *pvTaskHandle = (TaskHandle_t *)pvCreatedTask;

    BaseType_t ret = xTaskCreate(pvTaskFunction,
                                 pcName,
                                 usStackSizeInByte / sizeof(StackType_t), //Convert to depth
                                 pvParameters,
                                 uxPriority,
                                 pvTaskHandle);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxTaskSuspend(dxTaskHandle_t dxTaskToSuspend)
{
    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTaskToSuspend;

    vTaskSuspend(vTaskHandle);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxTaskResume(dxTaskHandle_t dxTaskToResume)
{
    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTaskToResume;

    vTaskResume(vTaskHandle);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxTaskPrioritySet(dxTaskHandle_t dxTask, uint32_t uxNewPriority)
{
    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTask;
    BaseType_t newpriority = (BaseType_t)uxNewPriority;

    vTaskPrioritySet(vTaskHandle, newpriority);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxTaskDelete(dxTaskHandle_t dxTaskToDelete)
{
    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTaskToDelete;

    vTaskDelete(vTaskHandle);

    return DXOS_SUCCESS;
}

dxTaskHandle_t dxTaskGetCurrentTaskHandle(void)
{
    TaskHandle_t xTaskHandle = xTaskGetCurrentTaskHandle();

    dxTaskHandle_t dxTaskHandle = (dxTaskHandle_t)xTaskHandle;

    return dxTaskHandle;
}

static void dkworker_task_main( uint32_t arg )
{
    dxWorkweTask_t* worker_task = (dxWorkweTask_t*) arg;

    while ( 1 )
    {
        dxEventMsg_t message;

        if (dxQueueReceive(   worker_task->WEventQueue,
                              &message,
                              portMAX_DELAY) == DXOS_SUCCESS)
        {
            if (message.function)
            {
              message.function( message.arg );
            }
        }
    }
}

dxOS_RET_CODE dxWorkerTaskCreate (const char* name, dxWorkweTask_t *pWorkerTask, dxTaskPriority_t uxPriority, uint16_t usStackDepth, uint32_t EventQueueSize )
{
    memset( pWorkerTask, 0, sizeof( dxWorkweTask_t ) );

    pWorkerTask->WEventQueue = dxQueueCreate((UBaseType_t) EventQueueSize,
                                              sizeof(dxEventMsg_t));

    if ( pWorkerTask->WEventQueue == NULL )
    {
        return DXOS_ERROR;
    }

    if (dxTaskCreate(   dkworker_task_main,
                        name,
                        usStackDepth,
                        (void *)pWorkerTask,
                        uxPriority,
                        &pWorkerTask->WTask) != DXOS_SUCCESS)
    {
        dxQueueDelete(pWorkerTask->WEventQueue);
        return DXOS_ERROR;
    }

    return DXOS_SUCCESS;

}

dxOS_RET_CODE dxWorkerTaskDelete( dxWorkweTask_t *WorkerTask)
{

    dxTaskDelete(WorkerTask->WTask);
    dxQueueDelete(WorkerTask->WEventQueue);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxWorkerTaskSendAsynchEvent( dxWorkweTask_t *WorkerTask, dxEventHandler_t function, void* arg )
{
    dxEventMsg_t message;

    message.function = function;
    message.arg = arg;

    return dxQueueSend(WorkerTask->WEventQueue,
                       &message,
                       0);
}

//============================================================================
// Task Stack - TO BE VERIFIED
//============================================================================

#pragma message ( "REMINDER: MINCLUDE_uxTaskGetStackHighWaterMark must be set to 1 in FreeRTOSConfig.h to use uxTaskGetStackHighWaterMark()" )

dxOS_RET_CODE dxTaskGetStackInfo(dxTaskHandle_t dxTask, dxTaskStack_t *pStackBuf)
{
    int len = 0;

    if (dxTask == NULL ||
        pStackBuf == NULL) {

        return DXOS_ERROR;
    }

    TaskHandle_t vTaskHandle = (TaskHandle_t)dxTask;

    memset(pStackBuf, 0x00, sizeof(dxTaskStack_t));

    pStackBuf->nTaskNumber = uxTaskGetTaskNumber(vTaskHandle);
    pStackBuf->nPriority = uxTaskPriorityGet(vTaskHandle);

    pStackBuf->nStackSize = 0;

    // INCLUDE_uxTaskGetStackHighWaterMark must be set to 1 in FreeRTOSConfig.h
    // to use uxTaskGetStackHighWaterMark()
    //NOTE: Here the returned stack size in uxTaskGetStackHighWaterMark is the 'depth' NOT the size in byte so we need to
    //      convert to byte by multiplying to the size of RTOS StackType_t
    pStackBuf->nFreeStackSpace = uxTaskGetStackHighWaterMark(vTaskHandle) * sizeof(StackType_t);

    len = strlen(pcTaskGetTaskName(vTaskHandle));
    if (len >= sizeof(pStackBuf->szTaskName)) {

        len = sizeof(pStackBuf->szTaskName) - 1;
    }
    memcpy(pStackBuf->szTaskName, pcTaskGetTaskName(vTaskHandle), len);

    return DXOS_SUCCESS;
}

//============================================================================
// Semaphore Management
//============================================================================
dxSemaphoreHandle_t dxSemCreate(uint32_t uxMaxCount,
                                uint32_t uxInitialCount)
{
    dxSemaphoreHandle_t dxSemaphoreHandler;
    BaseType_t count = (BaseType_t)uxMaxCount;
    BaseType_t init = (BaseType_t)uxInitialCount;

    dxSemaphoreHandler = (dxSemaphoreHandle_t)
                         xSemaphoreCreateCounting(count, init);

    return dxSemaphoreHandler;
}

dxOS_RET_CODE dxSemGive(dxSemaphoreHandle_t dxSemaphore)
{
    SemaphoreHandle_t xSemaphore = (SemaphoreHandle_t)dxSemaphore;

    BaseType_t ret = xSemaphoreGive(xSemaphore);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxSemTake(dxSemaphoreHandle_t dxSemaphore, uint32_t xBlockTimeInMS)
{
    SemaphoreHandle_t xSemaphore = (SemaphoreHandle_t)dxSemaphore;

    BaseType_t ret = xSemaphoreTake(xSemaphore, xBlockTimeInMS/portTICK_PERIOD_MS);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxSemDelete(dxSemaphoreHandle_t dxSemaphore)
{
    SemaphoreHandle_t xSemaphore = (SemaphoreHandle_t)dxSemaphore;

    vSemaphoreDelete(xSemaphore);

    return DXOS_SUCCESS;
}

//============================================================================
// Mutex Management
//============================================================================
dxMutexHandle_t dxMutexCreate(void)
{
    dxMutexHandle_t dxMutexHandle;

    QueueHandle_t xQueueHandler = xQueueCreateMutex(queueQUEUE_TYPE_MUTEX);

    dxMutexHandle = (dxMutexHandle_t)xQueueHandler;

    return dxMutexHandle;
}

dxOS_RET_CODE dxMutexTake(dxMutexHandle_t dxMutex, uint32_t xBlockTimeInMS)
{
    QueueHandle_t xQueueHandle = dxMutex;

    BaseType_t ret = xQueueGenericReceive(xQueueHandle, NULL, (xBlockTimeInMS/portTICK_PERIOD_MS), pdFALSE);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxMutexGive(dxMutexHandle_t dxMutex)
{
    QueueHandle_t xQueueHandle = dxMutex;

    BaseType_t ret = xQueueGenericSend(xQueueHandle, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return (ret == pdPASS) ? DXOS_SUCCESS : DXOS_ERROR;
}

dxOS_RET_CODE dxMutexDelete(dxMutexHandle_t dxMutex)
{
    QueueHandle_t xQueueHandle = dxMutex;

    vQueueDelete(xQueueHandle);

    return DXOS_SUCCESS;
}

//============================================================================
// EventGroup Management
//============================================================================
dxEventGroupHandle_t dxEventGroupCreate(void)
{
    EventGroupHandle_t xEventGroupHandle = xEventGroupCreate();

    dxEventGroupHandle_t dxEventGroupHandle = (dxEventGroupHandle_t)xEventGroupHandle;

    return dxEventGroupHandle;
}

uint32_t dxEventGroupSet(dxEventGroupHandle_t dxEventGroup,
                         const uint32_t uxBitsToSet)
{
    EventGroupHandle_t xEventGroupHandle = (EventGroupHandle_t)dxEventGroup;
    BaseType_t bitstoset = (BaseType_t)uxBitsToSet;

    EventBits_t evt = xEventGroupSetBits(xEventGroupHandle, bitstoset);

    return (uint32_t)evt;
}

uint32_t dxEventGroupClear(dxEventGroupHandle_t dxEventGroup,
                           const uint32_t uxBitsToClear)
{
    EventGroupHandle_t xEventGroupHandle = (EventGroupHandle_t)dxEventGroup;
    BaseType_t bitstoclear = (BaseType_t)uxBitsToClear;

    EventBits_t evt = xEventGroupClearBits(xEventGroupHandle, bitstoclear);

    return (uint32_t)evt;
}

uint32_t dxEventGroupWait(dxEventGroupHandle_t dxEventGroup,
                             const uint32_t uxBitsToWaitFor,
                             const dxBOOL xClearOnExit,
                             const dxBOOL xWaitForAllBits,
                             const uint32_t xMsToWait)
{
    EventGroupHandle_t xEventGroupHandle = (EventGroupHandle_t)dxEventGroup;
    EventBits_t bitstowait = (EventBits_t)uxBitsToWaitFor;
    BaseType_t clearonexit = (xClearOnExit == dxTRUE) ? pdTRUE : pdFALSE;
    BaseType_t waitforallbits = (xWaitForAllBits == dxTRUE) ? pdTRUE : pdFALSE;

    EventBits_t evt = xEventGroupWaitBits(xEventGroupHandle,
                                          bitstowait,
                                          clearonexit,
                                          waitforallbits,
                                          xMsToWait/portTICK_PERIOD_MS);

    return (uint32_t)evt;
}

dxOS_RET_CODE dxEventGroupDelete(dxEventGroupHandle_t dxEventGroup)
{
    EventGroupHandle_t xEventGroupHandle = (EventGroupHandle_t)dxEventGroup;

    vEventGroupDelete(xEventGroupHandle);

    return DXOS_SUCCESS;
}

//============================================================================
// Multicast
//============================================================================

// External variables
extern struct netif xnetif[];

#define IPBIN_TO_STR(x,y)         sprintf(x, "%d.%d.%d.%d",\
                                             y.v4[0],\
                                             y.v4[1],\
                                             y.v4[2],\
                                             y.v4[3])

typedef struct
{
    int sockId;                 // Socket ID
    dxSocketAddr_t SocketAddr;  // Multicast socket
} dxMulticastHandle_t;

void dxMCAST_Init(void)
{
    // Reserve for new Features in the futrue.

    return;
}

dxUDPHandle_t dxMCAST_Join(dxIP_Addr_t UDPGroupIp, uint16_t port)
{
    dxMulticastHandle_t *pHandle = NULL;
    pHandle = dxMemAlloc("dxMCAST_Join", 1, sizeof(dxMulticastHandle_t));

    if (pHandle == NULL)
    {
        return NULL;
    }

    pHandle->sockId = -1;
    pHandle->SocketAddr.GroupIP.version = DXIP_VERSION_V4;
    memset(pHandle->SocketAddr.GroupIP.v4, 0x00, sizeof(pHandle->SocketAddr.GroupIP.v4));
    pHandle->SocketAddr.port = 0;

    xnetif[0].flags |= NETIF_FLAG_IGMP;
    int ret = lwip_socket(AF_INET, SOCK_DGRAM, 0);

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->sockId = ret;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, UDPGroupIp);

    // Add multicast group membership on this interface
    struct ip_mreq imr;
    memset(&imr, 0x00, sizeof(imr));
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = lwip_setsockopt(pHandle->sockId, IPPROTO_IP, IP_ADD_MEMBERSHIP, &imr, sizeof(imr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // Specify outgoing interface too
    struct in_addr intfAddr;
    memset(&intfAddr, 0x00, sizeof(intfAddr));
    intfAddr.s_addr = INADDR_ANY;
    ret = lwip_setsockopt(pHandle->sockId, IPPROTO_IP, IP_MULTICAST_IF, &intfAddr, sizeof(struct in_addr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    // And start listening for packets
    struct sockaddr_in bindAddr;
    memset(&bindAddr, 0x00, sizeof(bindAddr));
    bindAddr.sin_family = AF_INET;
    bindAddr.sin_port = htons(port);
    bindAddr.sin_addr.s_addr = INADDR_ANY;
    ret = lwip_bind(pHandle->sockId, (struct sockaddr *) &bindAddr, sizeof(bindAddr));

    if (ret < 0)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->SocketAddr.GroupIP = UDPGroupIp;
    pHandle->SocketAddr.port = port;

    return (dxUDPHandle_t)pHandle;
}

dxNET_RET_CODE dxMCAST_Leave(dxUDPHandle_t handle)
{
    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;
    int ret = DXNET_ERROR;

    char szIPv4[16] = {0};
    IPBIN_TO_STR(szIPv4, pHandle->SocketAddr.GroupIP);
    // Add multicast group membership on this interface
    struct ip_mreq imr;
    imr.imr_multiaddr.s_addr = inet_addr(szIPv4);
    imr.imr_interface.s_addr = INADDR_ANY;
    ret = lwip_setsockopt(pHandle->sockId, IPPROTO_IP, IP_DROP_MEMBERSHIP, &imr, sizeof(imr));
    if (ret < 0)
    {
        return DXNET_ERROR;
    }

    ret = lwip_close(pHandle->sockId);

    dxMemFree(pHandle);
    pHandle = NULL;

    return (ret == 0) ? DXNET_SUCCESS : DXNET_ERROR;
}

int dxMCAST_RecvFrom(dxUDPHandle_t handle, void *Buffer, uint32_t BuffSize, dxSocketAddr_t *pFromSocket, uint32_t TimeOutInMs)
{
    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;

    uint32_t TimeOutRcv = (uint32_t)TimeOutInMs;

    int ret;

    if (dxSOCKET_SetTimeout(pHandle->sockId,
                            TimeOutRcv) == -1)
    {
        G_SYS_DBG_LOG_V("dxMCAST_RecvFrom - lwip_setsockopt failed to config\r\n");
    }

    //int ret = lwip_recvfrom(pHandle->sockId, Buffer, BuffSize, flags, pFromAddr, (socklen_t *)NULL);
    if (pFromSocket == NULL)
    {
        ret = lwip_recvfrom(pHandle->sockId, Buffer, BuffSize, 0, NULL, (socklen_t *)NULL);
    }
    else
    {
        char szIPv4[16] = {0};
        IPBIN_TO_STR(szIPv4, pFromSocket->GroupIP);

        struct sockaddr_in sour;
        memset(&sour, 0x00, sizeof(sour));
        sour.sin_family = AF_INET;
        sour.sin_port = htons(pFromSocket->port);
        sour.sin_addr.s_addr = inet_addr(szIPv4);
        ret = lwip_recvfrom(pHandle->sockId, Buffer, BuffSize, 0, &sour, (socklen_t *)NULL);
    }

    // Possible error code
    // > 0:
    // 0:
    // -1:
    // For more detail error code, please refer to 'err.h' located in
    // 'component\common\network\lwip\lwip_v1.4.1\src\include\lwip'

    if (ret > 0)
    {
        return ret;
    }
    else if (ret == 0)
    {
        return DXNET_CONN_CLOSED;
    }

    return DXNET_ERROR;
}

int dxMCAST_SendTo(dxUDPHandle_t handle, const void *dataptr, uint32_t dataSize, dxSocketAddr_t *pToSocket)
{
    dxMulticastHandle_t *pHandle = (dxMulticastHandle_t *)handle;

    char szIPv4[16] = {0};

    struct sockaddr_in dest;
    memset(&dest, 0x00, sizeof(dest));
    dest.sin_family = AF_INET;

    if (pToSocket == NULL)
    {
        IPBIN_TO_STR(szIPv4, pHandle->SocketAddr.GroupIP);
        dest.sin_port = htons(pHandle->SocketAddr.port);
    }
    else
    {
        IPBIN_TO_STR(szIPv4, pToSocket->GroupIP);
        dest.sin_port = htons(pToSocket->port);
    }
    dest.sin_addr.s_addr = inet_addr(szIPv4);

    //int ret = lwip_sendto(pHandle->sockId, dataptr, dataSize, flags, ToAddr, (socklen_t)NULL);
    int ret = lwip_sendto(pHandle->sockId, dataptr, dataSize, 0, &dest, (socklen_t)sizeof(dest));

    // Possible error code
    // > 0:
    // -1:
    // ERR_VAL:
    // ERR_ARG:
    // ERR_MEM:
    // For more detail error code, please refer to 'err.h' located in
    // 'component\common\network\lwip\lwip_v1.4.1\src\include\lwip'

    return (ret > 0) ? ret : DXNET_ERROR;
}


dxAesContext_t  dxAESSW_ContextAlloc(void)
{
    dxAesContext_t pAesContext = NULL;

    pAesContext = dxMemAlloc("SwAesContext", 1, AES_ENC_DEC_BYTE);

    return pAesContext;
}


dxAES_RET_CODE  dxAESSW_Setkey( dxAesContext_t ctx, const unsigned char *key, unsigned int keysize, dxAES_CRYPTO_TYPE Type )
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    //Currently we only support 128bit SW AES
    if (keysize != 128)
        return DXAES_INVALID_PARAM;

    if (ctx && key)
    {
        memcpy(ctx, key, keysize/8);
    }
    else
    {
       ret = DXAES_INVALID_PARAM;
    }

    return ret;
}


dxAES_RET_CODE  dxAESSW_CryptEcb( dxAesContext_t ctx, dxAES_CRYPTO_TYPE Type, const unsigned char input[16], unsigned char output[16] )
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    if (ctx)
    {

        if (Type == DXAES_ENCRYPT)
        {
            AES128_ECB_encrypt(input, ctx, output);
        }
        else if (Type == DXAES_DECRYPT)
        {
            AES128_ECB_decrypt(input, ctx, output);
        }
        else
        {
            ret = DXAES_INVALID_PARAM;
        }

    }
    else
    {
       ret = DXAES_INVALID_PARAM;
    }

    return ret;
}

dxAES_RET_CODE  dxAESSW_ContextDealloc(dxAesContext_t ctx)
{
    dxAES_RET_CODE ret = DXAES_SUCCESS;

    if (ctx)
    {
        dxMemFree(ctx);
    }

    return ret;
}

uint32_t
dxRand()
{
	srand( ( uint32_t )ADC0_seed32() );
	return rand();
}

void  dxSPIaccess_ResourceTake(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE SemRet = dxSemTake(dxSPI_Resource_SemaHandle, DXOS_WAIT_FOREVER);
    if ( SemRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceTake failed!!!!!\r\n");
    }
}

void  dxSPIaccess_ResourceGive(void) //WORKAROUND ONLY - See header for detail
{
    dxOS_RET_CODE SemRet = dxSemGive(dxSPI_Resource_SemaHandle);
    if ( SemRet != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V("Warning - dxSPIaccess_ResourceGive failed!!!!!\r\n");
    }
}


int dxSelect (
    int nfds,
    fd_set* readfds,
    fd_set* writefds,
    fd_set* exceptfds,
    struct timeval* timeout
) {
    int ret = select (
        nfds,
        readfds,
        writefds,
        exceptfds,
        timeout
    );

    return ret;
}


int dxNumOfDigits (uint32_t v) {
    uint32_t po10;
    int n;

    n=1;
    po10=10;

    while (v >= po10) {
        n++;
        if (po10 > PO10_LIMIT) break;
            po10 *= 10;
    }

    return n;
}
