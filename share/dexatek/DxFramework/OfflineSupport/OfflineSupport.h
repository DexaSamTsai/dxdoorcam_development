//============================================================================
// File: OfflineSupport.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/02
//     1) Description: Initial
//
//============================================================================
#ifndef _OFFLINE_SUPPORT_H
#define _OFFLINE_SUPPORT_H

#include "BLEDeviceKeeper.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

typedef enum
{
    OFFLINE_FLAG_ALL           = 0,
    OFFLINE_FLAG_ADV_JOB_INFO_UPDATED,
    OFFLINE_FLAG_SCH_JOB_INFO_UPDATED,
    OFFLINE_FLAG_PERIPHERAL_INFO_UPDATED,
    OFFLINE_FLAG_ADV_JOB_INFO_DOWNLOADED,
    OFFLINE_FLAG_SCH_JOB_INFO_DOWNLOADED,
    OFFLINE_FLAG_PERIPHERAL_INFO_DOWNLOADED,

} eOfflineFlags;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Function
//============================================================================

dxBOOL  isCentralOfflineSupported(void);

void CentralOfflineSupportDeviceInfoRestore(void);

void CentralOfflineSupportDeviceInfoBackUp(void);

void CentralOfflineSupportDeviceEraseAll(void);

void CentralOfflineSupportFlagSet(eOfflineFlags FlagToSet);

void CentralOfflineSupportFlagClear(eOfflineFlags FlagToClear);

DeviceGenericInfo_t* CentralOfflineSupportFindDeviceInfoByMac (uint8_t *macaddr, int macaddrlen);

#endif // _OFFLINE_SUPPORT_H
