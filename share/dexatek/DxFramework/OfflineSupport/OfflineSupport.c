//============================================================================
// File: OfflineSupport.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/02
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "WifiCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxDeviceInfoManager.h"
#include "ObjectDictionary.h"
#include "OfflineSupport.h"

#if CENTRAL_OFFLINE_SUPPORT
#pragma message ( "REMINDER: CENTRAL_OFFLINE_SUPPORT is ENABLED!" )
#else
#pragma message ( "REMINDER: CENTRAL_OFFLINE_SUPPORT is DISABLED!" )
#endif

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    uint16_t            Preamble;
    uint16_t            PeripheralGenInfoSize;
    uint16_t            ScheduledJobSize;
    uint16_t            AdvanceJobSize;
    uint16_t            PeripheralGenInfoCRC;
    uint16_t            ScheduleJobCRC;
    uint16_t            AdvanceJobCRC;

} OfflineSupportBackupHeader_t;

typedef struct
{
    OfflineSupportBackupHeader_t    Header;
    uint8_t                         Payload[0];

} OfflineSupportBackup_t;


struct CENTRAL_OFFLINE_FLAG_BIT_INFO {
    uint8_t Reserve_b0_1     			  :2;   // bit 0 through 1 (2 bit total)
    uint8_t AdvanceJobInfoUpdated       :1;   // bit 2 (1 bit total)
    uint8_t ScheduleJobInfoUpdated      :1;   // bit 3 (1 bit total)
    uint8_t PeripheralInfoUpdated       :1;   // bit 4 (1 bit total)
    uint8_t AdvanceJobInfoDownloaded    :1;   // bit 5 (1 bit total)
    uint8_t ScheduleJobInfoDownloaded   :1;   // bit 6 (1 bit total)
    uint8_t PeripheralInfoDownloaded    :1;   // bit 7 (1 bit total)
};

union CentralOfflineSupportFlag_u {
    uint8_t  						        flag;
    struct CENTRAL_OFFLINE_FLAG_BIT_INFO  bits;
};

typedef struct
{
    union CentralOfflineSupportFlag_u	 Value;

} CentralOfflineSupportFlag_t;

//============================================================================
// Global Var
//============================================================================
#if CENTRAL_OFFLINE_SUPPORT

dxAppDataHandle_t BackupDataAccess = NULL;

CentralOfflineSupportFlag_t OfflineContentChanges = { .Value.flag = 0 };

#endif

//============================================================================
// Internal Function
//============================================================================

//============================================================================
// Util Function
//============================================================================

#if CENTRAL_OFFLINE_SUPPORT

void UtlCentralOfflineSupportFlagChange(eOfflineFlags FlagToSet, uint8_t SetOrClear)
{
    switch (FlagToSet)
    {
        case OFFLINE_FLAG_ALL:
            OfflineContentChanges.Value.flag = (SetOrClear) ? 0xFF:0x00;
            break;
        case OFFLINE_FLAG_ADV_JOB_INFO_UPDATED:
            OfflineContentChanges.Value.bits.AdvanceJobInfoUpdated = SetOrClear;
            break;
        case OFFLINE_FLAG_SCH_JOB_INFO_UPDATED:
            OfflineContentChanges.Value.bits.ScheduleJobInfoUpdated = SetOrClear;
            break;
        case OFFLINE_FLAG_PERIPHERAL_INFO_UPDATED:
            OfflineContentChanges.Value.bits.PeripheralInfoUpdated = SetOrClear;
            break;
        case OFFLINE_FLAG_ADV_JOB_INFO_DOWNLOADED:
            OfflineContentChanges.Value.bits.AdvanceJobInfoDownloaded = SetOrClear;
            break;
        case OFFLINE_FLAG_SCH_JOB_INFO_DOWNLOADED:
            OfflineContentChanges.Value.bits.ScheduleJobInfoDownloaded = SetOrClear;
            break;
        case OFFLINE_FLAG_PERIPHERAL_INFO_DOWNLOADED:
            OfflineContentChanges.Value.bits.PeripheralInfoDownloaded = SetOrClear;
            break;

            default:
            break;
    }
}

#endif

//============================================================================
// Function
//============================================================================

dxBOOL  isCentralOfflineSupported(void)
{
#if CENTRAL_OFFLINE_SUPPORT
    return dxTRUE;
#else
    return dxFALSE;
#endif
}


void CentralOfflineSupportDeviceInfoRestore(void)
{

#if CENTRAL_OFFLINE_SUPPORT
    uint16_t    DeviceAccessKeyBuffSize = 128;
    char*       pDeviceAccessKeyBuff = dxMemAlloc( "DevAccessKeyBuff", 1, DeviceAccessKeyBuffSize);
    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, pDeviceAccessKeyBuff, &DeviceAccessKeyBuffSize, dxFALSE);
    if (DevInfoRet == DXDEV_INFO_SUCCESS)
    {
        if (strlen(pDeviceAccessKeyBuff))
        {
            dxAPPDATA_RET_CODE ret;

            if (BackupDataAccess == NULL)
            {
                BackupDataAccess = dxAppData_Init(DXBLOCKID_DEVINFO);
            }

            if (BackupDataAccess)
            {
                OfflineSupportBackup_t* pBackupDataToRestore = NULL;
                uint32_t RestoreDataSize = dxAppData_ReadDataSize(BackupDataAccess);

                if (RestoreDataSize)
                {
                    pBackupDataToRestore = dxMemAlloc( "RestoreData", 1, RestoreDataSize);
                    ret = dxAppData_Read(BackupDataAccess, (void*)pBackupDataToRestore, &RestoreDataSize);
                    if (ret == DXAPPDATA_SUCCESS)
                    {
                        if (pBackupDataToRestore->Header.PeripheralGenInfoSize)
                        {
                            uint16_t remains = pBackupDataToRestore->Header.PeripheralGenInfoSize % sizeof (DeviceGenericInfo_t);
                            DeviceGenericInfo_t* lastInfo = (DeviceGenericInfo_t*) (pBackupDataToRestore->Payload + pBackupDataToRestore->Header.PeripheralGenInfoSize - sizeof (DeviceGenericInfo_t));
                            const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac ();
                            dxBOOL  bHybridDeviceDetected = dxFALSE;

                            if (remains || ((lastInfo->DevAddress.Addr[3] != 0xff) && (lastInfo->DevAddress.Addr[4] != 0xff))) {
                                uint16_t TotalElement = pBackupDataToRestore->Header.PeripheralGenInfoSize /sizeof(OldDeviceGenericInfo_t);
                                uint16_t i = 0;
                                OldDeviceGenericInfo_t* DeviceGenericInfo = (OldDeviceGenericInfo_t*)pBackupDataToRestore->Payload;

                                G_SYS_DBG_LOG_V("Restoring Peripheral-Total Element = %d\r\n", TotalElement);
                                G_SYS_DBG_LOG_V("Restoring Peripheral-CRC = %x\r\n", pBackupDataToRestore->Header.PeripheralGenInfoCRC);

                                for (i = 0; i < TotalElement; i++)
                                {
                                    OldDeviceGenericInfo_t* WorkingElement = DeviceGenericInfo + i;
                                    bHybridDeviceDetected = dxFALSE;

                                    uint8_t k = 0;
                                    for (k = 0; k < B_ADDR_LEN_8; k++) {
                                        if (WorkingElement->DevAddress.Addr[k] == pMainDeviceMacAddr->MacAddr[k]) {
                                            if ((k + 1) == B_ADDR_LEN_8) {
                                                bHybridDeviceDetected = dxTRUE;
                                            }
                                            continue;
                                        } else {
                                            break;
                                        }
                                    }

                                    if (bHybridDeviceDetected == dxFALSE) {
                                        DeviceAddInfo_t DeviceAddInfo;
                                        DeviceAddInfo.SecurityKey.Size = 0;     //First make sure the key is initialize to 0
                                        DeviceAddInfo.SingleHostControl = 0;    //By default we disable the singlehost control.. in the future we may need to update/get this value from server

                                        memcpy(DeviceAddInfo.DevAddress.Addr, WorkingElement->DevAddress.Addr, sizeof(DeviceAddInfo.DevAddress.Addr));
                                        memcpy(DeviceAddInfo.SecurityKey.value, WorkingElement->SecurityKey.value, sizeof(DeviceAddInfo.SecurityKey.value));
                                        DeviceAddInfo.SecurityKey.Size = WorkingElement->SecurityKey.Size;
                                        DeviceAddInfo.DeviceType = 0;

                                        BLEManager_result_t BLEManRet = BleManagerAddDeviceTask_Asynch(&DeviceAddInfo, 1000, SOURCE_OF_ORIGIN_DEFAULT, 0);
                                        if (BLEManRet != DEV_MANAGER_SUCCESS) {
                                            G_SYS_DBG_LOG_V("WARNING: Restoring some device failed ret=%d\r\n", BLEManRet);
                                        }
                                    }
                                }
                            } else {
                                uint16_t TotalElement = pBackupDataToRestore->Header.PeripheralGenInfoSize /sizeof(DeviceGenericInfo_t);
                                uint16_t i = 0;
                                DeviceGenericInfo_t* DeviceGenericInfo = (DeviceGenericInfo_t*)pBackupDataToRestore->Payload;

                                G_SYS_DBG_LOG_V("Restoring Peripheral-Total Element = %d\r\n", TotalElement);
                                G_SYS_DBG_LOG_V("Restoring Peripheral-CRC = %x\r\n", pBackupDataToRestore->Header.PeripheralGenInfoCRC);

                                for (i = 0; i < TotalElement; i++)
                                {
                                    DeviceGenericInfo_t* WorkingElement = DeviceGenericInfo + i;
                                    bHybridDeviceDetected = dxFALSE;

                                    uint8_t k = 0;
                                    for (k = 0; k < B_ADDR_LEN_8; k++) {
                                        if (WorkingElement->DevAddress.Addr[k] == pMainDeviceMacAddr->MacAddr[k]) {
                                            if ((k + 1) == B_ADDR_LEN_8) {
                                                bHybridDeviceDetected = dxTRUE;
                                            }
                                            continue;
                                        } else {
                                            break;
                                        }
                                    }

                                    if (bHybridDeviceDetected == dxFALSE) {
                                        DeviceAddInfo_t DeviceAddInfo;
                                        DeviceAddInfo.SecurityKey.Size = 0;     //First make sure the key is initialize to 0
                                        DeviceAddInfo.SingleHostControl = 0;    //By default we disable the singlehost control.. in the future we may need to update/get this value from server

                                        memcpy(DeviceAddInfo.DevAddress.Addr, WorkingElement->DevAddress.Addr, sizeof(DeviceAddInfo.DevAddress.Addr));
                                        memcpy(DeviceAddInfo.SecurityKey.value, WorkingElement->SecurityKey.value, sizeof(DeviceAddInfo.SecurityKey.value));
                                        DeviceAddInfo.SecurityKey.Size = WorkingElement->SecurityKey.Size;
                                        DeviceAddInfo.DeviceType = WorkingElement->DeviceType;

                                        BLEManager_result_t BLEManRet = BleManagerAddDeviceTask_Asynch(&DeviceAddInfo, 1000, SOURCE_OF_ORIGIN_DEFAULT, 0);
                                        if (BLEManRet != DEV_MANAGER_SUCCESS) {
                                            G_SYS_DBG_LOG_V("WARNING: Restoring some device failed ret=%d\r\n", BLEManRet);
                                        }
                                    }
                                }
                            }
                        }

                        if (pBackupDataToRestore->Header.ScheduledJobSize)
                        {

                            uint8_t* pScheduleJobRestore = pBackupDataToRestore->Payload + pBackupDataToRestore->Header.PeripheralGenInfoSize;
                            uint16_t RestoredScheduleJob = JobManagerRestoreDataOfAllScheduleJob(pScheduleJobRestore, pBackupDataToRestore->Header.ScheduledJobSize);

                            G_SYS_DBG_LOG_V("Restoring Shcedule Job-Total Job Restored = %d\r\n", RestoredScheduleJob);
                            G_SYS_DBG_LOG_V("Restoring Shcedule-CRC = %x\r\n", pBackupDataToRestore->Header.ScheduleJobCRC);

                        }

                        if (pBackupDataToRestore->Header.AdvanceJobSize)
                        {
                            G_SYS_DBG_LOG_V("Restoring Adv Job Size = %d\r\n", pBackupDataToRestore->Header.AdvanceJobSize);

                            uint8_t* pAdvJobRestore =   pBackupDataToRestore->Payload +
                                                        pBackupDataToRestore->Header.PeripheralGenInfoSize +
                                                        pBackupDataToRestore->Header.ScheduledJobSize;

                            uint16_t RestoredAdvJob = JobManagerRestoreDataOfAllAdvanceJob(pAdvJobRestore, pBackupDataToRestore->Header.AdvanceJobSize);
                            G_SYS_DBG_LOG_V("Restoring Adv Job-Total Job Restored = %d\r\n", RestoredAdvJob);
                            G_SYS_DBG_LOG_V("Restoring Adv-CRC = %x\r\n", pBackupDataToRestore->Header.AdvanceJobCRC);

                        }

                    }
                    else
                    {
                        G_SYS_DBG_LOG_V("WARNING: Failed to retrive data to restore\r\n");
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V("NOTE: No data to restore\r\n");
                }

                if (pBackupDataToRestore)
                {
                    dxMemFree(pBackupDataToRestore);
                    pBackupDataToRestore = NULL;
                }

            }
        }
    }

    dxMemFree(pDeviceAccessKeyBuff);

#endif //CENTRAL_OFFLINE_SUPPORT

   return;
}


void CentralOfflineSupportDeviceInfoBackUp(void)
{

#if CENTRAL_OFFLINE_SUPPORT

    dxAPPDATA_RET_CODE ret;

    //If there is nothing to backup then we simply return.
    if (OfflineContentChanges.Value.flag == 0)
        return;

    if (BackupDataAccess == NULL)
    {
        BackupDataAccess = dxAppData_Init(DXBLOCKID_DEVINFO);
    }

    if (BackupDataAccess)
    {
        OfflineSupportBackupHeader_t NewBackupDataHeader;
        OfflineSupportBackup_t* pCurrentBackupData = NULL;
        uint8_t* pNewBackupData = NULL;
        uint8_t* pUpdatedPeripheralData = NULL;
        uint8_t* pUpdatedScheduledJobData = NULL;
        uint8_t* pUpdatedAdvanceJobData = NULL;

        memset(&NewBackupDataHeader, 0, sizeof(OfflineSupportBackupHeader_t));

        uint32_t BackupDataSize = dxAppData_ReadDataSize(BackupDataAccess);

        if (BackupDataSize)
        {
            G_SYS_DBG_LOG_V("BackupDataSize = %d\r\n", BackupDataSize);


            pCurrentBackupData = dxMemAlloc( "BackupData", 1, BackupDataSize);
            ret = dxAppData_Read(BackupDataAccess, (void*)pCurrentBackupData, &BackupDataSize);
            if (ret != DXAPPDATA_SUCCESS)
            {
                if (pCurrentBackupData)
                {
                    dxMemFree(pCurrentBackupData);
                    pCurrentBackupData = NULL;
                }
            }
            else
            {
                //Fillup the new header first from the previously backup data.
                memcpy(&NewBackupDataHeader, (void*)pCurrentBackupData, sizeof(OfflineSupportBackupHeader_t));
            }

        }


        if ((OfflineContentChanges.Value.bits.PeripheralInfoUpdated) || (OfflineContentChanges.Value.bits.PeripheralInfoDownloaded))
        {
#if DEVICE_IS_HYBRID
            uint16_t TotalPeripheral = BLEDevKeeper_GetDeviceCountFromKeeperList() + 1;
#else
            uint16_t TotalPeripheral = BLEDevKeeper_GetDeviceCountFromKeeperList();
#endif
            if (TotalPeripheral)
            {
                pUpdatedPeripheralData = dxMemAlloc( "PeripheralData", 1, TotalPeripheral * sizeof(DeviceGenericInfo_t));

                if (pUpdatedPeripheralData)
                {
                    DeviceGenericInfo_t* updateDeviceInfo = (DeviceGenericInfo_t*) pUpdatedPeripheralData;

#if DEVICE_IS_HYBRID
                    const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                    DeviceGenericInfo_t* deviceInfo = BLEDevKeeper_FindDeviceInfoByMac (pMainDeviceMacAddr->MacAddr, B_ADDR_LEN_8);

                    if (deviceInfo) {
                        memcpy (updateDeviceInfo, deviceInfo, sizeof (DeviceGenericInfo_t));
                        dxMemFree (deviceInfo);
                        updateDeviceInfo++;
                    }
#endif
                    uint16_t PeripheralCountRetrieved = BLEDevKeeper_GetAllDeviceGenericInfoFromKeeperList(updateDeviceInfo, TotalPeripheral);
                    uint16_t Crc16 = crc16_ccitt_kermit(pUpdatedPeripheralData, TotalPeripheral * sizeof(DeviceGenericInfo_t));
                    if (Crc16 != NewBackupDataHeader.PeripheralGenInfoCRC)
                    {
                        G_SYS_DBG_LOG_V("PeripheralData changed, Backup Required\r\n");
                        NewBackupDataHeader.PeripheralGenInfoCRC = Crc16;
                        NewBackupDataHeader.PeripheralGenInfoSize = TotalPeripheral * sizeof(DeviceGenericInfo_t);
                    }
                    else
                    {
                        //Peripheral Data the same... free the mem and no update required.
                        dxMemFree(pUpdatedPeripheralData);
                        pUpdatedPeripheralData = NULL;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V("WARNING: PeripheralData memalloc failed!\r\n");
                }
            }
            else
            {
                NewBackupDataHeader.PeripheralGenInfoCRC = 0;
                NewBackupDataHeader.PeripheralGenInfoSize = 0;
                G_SYS_DBG_LOG_V("WARNING: Offline Support backup with no peripheral found!\r\n");
            }
        }


        if ((OfflineContentChanges.Value.bits.ScheduleJobInfoUpdated) || (OfflineContentChanges.Value.bits.ScheduleJobInfoDownloaded))
        {
            uint32_t ScheduleJobBackupSize = JobManagerGetTotalScheduleJobDataBackupSize();

            if (ScheduleJobBackupSize)
            {
                pUpdatedScheduledJobData = dxMemAlloc( "ScheduleData", 1, ScheduleJobBackupSize);
                if (pUpdatedScheduledJobData)
                {
                    uint32_t ScheduleJobSizeRetrieved = JobManagerGetBackupDataOfAllScheduleJob(pUpdatedScheduledJobData, ScheduleJobBackupSize);

                    if (ScheduleJobSizeRetrieved != ScheduleJobBackupSize)
                    {
                        G_SYS_DBG_LOG_V("WARNING: ScheduleJobSizeRetrieved != ScheduleJobBackupSize\r\n");
                    }

                    uint16_t Crc16 = crc16_ccitt_kermit(pUpdatedScheduledJobData, ScheduleJobSizeRetrieved);

                    if (Crc16 != NewBackupDataHeader.ScheduleJobCRC)
                    {
                        G_SYS_DBG_LOG_V("ScheduleData changed, Backup Required\r\n");
                        NewBackupDataHeader.ScheduleJobCRC = Crc16;
                        NewBackupDataHeader.ScheduledJobSize = ScheduleJobSizeRetrieved;
                    }
                    else
                    {
                        //Peripheral Data the same... free the mem and no update required.
                        dxMemFree(pUpdatedScheduledJobData);
                        pUpdatedScheduledJobData = NULL;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V("WARNING: ScheduleJobData memalloc failed!\r\n");
                }

            }
            else
            {
                NewBackupDataHeader.ScheduleJobCRC = 0;
                NewBackupDataHeader.ScheduledJobSize = 0;
                G_SYS_DBG_LOG_V("WARNING: Offline Support backup with no scheduleJob found!\r\n");
            }


        }

        if ((OfflineContentChanges.Value.bits.AdvanceJobInfoUpdated) || (OfflineContentChanges.Value.bits.AdvanceJobInfoDownloaded))
        {
            uint32_t AdvanceJobBackupSize = JobManagerGetTotalAdvanceJobDataBackupSize(dxTRUE);

            if (AdvanceJobBackupSize)
            {
                pUpdatedAdvanceJobData = dxMemAlloc( "AdvanceData", 1, AdvanceJobBackupSize);
                if (pUpdatedAdvanceJobData)
                {
                    uint32_t AdvJobSizeRetrieved = JobManagerGetBackupDataOfAllAdvanceJob(pUpdatedAdvanceJobData, AdvanceJobBackupSize, dxTRUE);

                    if (AdvJobSizeRetrieved != AdvanceJobBackupSize)
                    {
                        G_SYS_DBG_LOG_V("WARNING: AdvJobSizeRetrieved != AdvanceJobBackupSize\r\n");
                    }

                    uint16_t Crc16 = crc16_ccitt_kermit(pUpdatedAdvanceJobData, AdvJobSizeRetrieved);

                    if (Crc16 != NewBackupDataHeader.AdvanceJobCRC)
                    {
                        G_SYS_DBG_LOG_V("AdvData changed, Backup Required\r\n");
                        NewBackupDataHeader.AdvanceJobCRC = Crc16;
                        NewBackupDataHeader.AdvanceJobSize = AdvJobSizeRetrieved;
                    }
                    else
                    {
                        //Peripheral Data the same... free the mem and no update required.
                        dxMemFree(pUpdatedAdvanceJobData);
                        pUpdatedAdvanceJobData = NULL;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V("WARNING: AdvJobData memalloc failed!\r\n");
                }

            }
            else
            {
                NewBackupDataHeader.AdvanceJobCRC = 0;
                NewBackupDataHeader.AdvanceJobSize = 0;
                G_SYS_DBG_LOG_V("WARNING: Offline Support backup with no AdvJob found!\r\n");
            }

        }


        if (pCurrentBackupData)
        {
            if (memcmp(pCurrentBackupData, &NewBackupDataHeader, sizeof(OfflineSupportBackupHeader_t)) == 0)
            {
                //Looks like the data is exactly the same, no need to overwrite to flash..
                goto ExitCleanUp;
            }
        }


        //Should be NULL but just in case if its not we do not alloc again (code revise required if this is the case).. Data will not be written to flash!
        if (pNewBackupData == NULL)
        {
            uint32_t TotalNewBackupSize =   NewBackupDataHeader.PeripheralGenInfoSize + NewBackupDataHeader.ScheduledJobSize +
                                            NewBackupDataHeader.AdvanceJobSize + sizeof(OfflineSupportBackupHeader_t);

            G_SYS_DBG_LOG_V("TotalNewBackupSize = %d\r\n", TotalNewBackupSize);

            pNewBackupData = dxMemAlloc( "NewBackupbuff", 1, TotalNewBackupSize);

            if (pNewBackupData)
            {
                //Copy the header
                memcpy(pNewBackupData, &NewBackupDataHeader, sizeof(OfflineSupportBackupHeader_t));

                //Copy the PeripheralData
                if (NewBackupDataHeader.PeripheralGenInfoSize)
                {
                    uint8_t* SourceData = NULL;
                    if (pUpdatedPeripheralData)
                    {
                        SourceData = pUpdatedPeripheralData;
                    }
                    else
                    {
                        SourceData =    (uint8_t*)pCurrentBackupData +
                                        sizeof(OfflineSupportBackupHeader_t);
                    }

                    memcpy(pNewBackupData + sizeof(OfflineSupportBackupHeader_t), SourceData, NewBackupDataHeader.PeripheralGenInfoSize);
                }

                //Copy the Schedule Job
                if (NewBackupDataHeader.ScheduledJobSize)
                {
                    uint8_t* SourceData = NULL;
                    if (pUpdatedScheduledJobData)
                    {
                        SourceData = pUpdatedScheduledJobData;
                    }
                    else
                    {
                        SourceData =    (uint8_t*)pCurrentBackupData +
                                        sizeof(OfflineSupportBackupHeader_t) +
                                        pCurrentBackupData->Header.PeripheralGenInfoSize;
                    }

                    memcpy( pNewBackupData + sizeof(OfflineSupportBackupHeader_t) + NewBackupDataHeader.PeripheralGenInfoSize,
                            SourceData, NewBackupDataHeader.ScheduledJobSize);
                }

                //Copy the Adv Job
                if (NewBackupDataHeader.AdvanceJobSize)
                {
                    uint8_t* SourceData = NULL;
                    if (pUpdatedAdvanceJobData)
                    {
                        SourceData = pUpdatedAdvanceJobData;
                    }
                    else
                    {
                        SourceData =    (uint8_t*)pCurrentBackupData +
                                        sizeof(OfflineSupportBackupHeader_t) +
                                        pCurrentBackupData->Header.PeripheralGenInfoSize +
                                        pCurrentBackupData->Header.ScheduledJobSize;
                    }

                    memcpy( pNewBackupData +
                            sizeof(OfflineSupportBackupHeader_t) +
                            NewBackupDataHeader.PeripheralGenInfoSize +
                            NewBackupDataHeader.ScheduledJobSize, SourceData, NewBackupDataHeader.AdvanceJobSize);
                }

                G_SYS_DBG_LOG_V("BackingUp PCRC= %x, SCRC = %x, ACRC = %x\r\n",
                                    NewBackupDataHeader.PeripheralGenInfoCRC,
                                    NewBackupDataHeader.ScheduleJobCRC,
                                    NewBackupDataHeader.AdvanceJobCRC);

                ret = dxAppData_Write(BackupDataAccess, pNewBackupData, TotalNewBackupSize);
                if (ret != DXAPPDATA_SUCCESS)
                {
                    G_SYS_DBG_LOG_V("WARNING: BackupData Failed ret = %d\r\n", ret);
                }

            }
            else
            {
                G_SYS_DBG_LOG_V("WARNING: new backup buff memalloc failed!\r\n");
            }
        }

ExitCleanUp:
        //Free all allocated memories
        if (pCurrentBackupData)
        {
            dxMemFree(pCurrentBackupData);
        }
        if (pNewBackupData)
        {
            dxMemFree(pNewBackupData);
        }
        if (pUpdatedPeripheralData)
        {
            dxMemFree(pUpdatedPeripheralData);
        }
        if (pUpdatedScheduledJobData)
        {
            dxMemFree(pUpdatedScheduledJobData);
        }
        if (pUpdatedAdvanceJobData)
        {
            dxMemFree(pUpdatedAdvanceJobData);
        }

    }

#endif //CENTRAL_OFFLINE_SUPPORT

    return;
}

void CentralOfflineSupportDeviceEraseAll(void)
{
#if CENTRAL_OFFLINE_SUPPORT

    if (BackupDataAccess == NULL)
    {
        BackupDataAccess = dxAppData_Init(DXBLOCKID_DEVINFO);
    }

    if (BackupDataAccess)
    {
        dxAPPDATA_RET_CODE  ret = dxAppData_Clean(BackupDataAccess);
        if (ret != DXAPPDATA_SUCCESS)
        {
            G_SYS_DBG_LOG_V("WARNING: Failed to CentralOfflineSupportDeviceEraseAll ret=%d\r\n", ret);
        }
    }
#endif //CENTRAL_OFFLINE_SUPPORT

    return;

}


void CentralOfflineSupportFlagSet(eOfflineFlags FlagToSet)
{
#if CENTRAL_OFFLINE_SUPPORT
    UtlCentralOfflineSupportFlagChange(FlagToSet, 1);
#endif //CENTRAL_OFFLINE_SUPPORT

    return;
}

void CentralOfflineSupportFlagClear(eOfflineFlags FlagToClear)
{
#if CENTRAL_OFFLINE_SUPPORT
    UtlCentralOfflineSupportFlagChange(FlagToClear, 0);
#endif //CENTRAL_OFFLINE_SUPPORT

    return;
}


DeviceGenericInfo_t* CentralOfflineSupportFindDeviceInfoByMac (uint8_t *macaddr, int macaddrlen) {

    DeviceGenericInfo_t* deviceInfo = NULL;

#if CENTRAL_OFFLINE_SUPPORT
    OfflineSupportBackup_t* pBackupDataToRestore = NULL;
    DeviceGenericInfo_t* lastInfo = NULL;
    dxDEV_INFO_RET_CODE DevInfoRet = DXDEV_INFO_SUCCESS;
    dxAPPDATA_RET_CODE ret = DXAPPDATA_SUCCESS;
    uint32_t RestoreDataSize = 0;
    uint16_t DeviceAccessKeyBuffSize = 128;
    uint16_t remains = 0;
    char* pDeviceAccessKeyBuff = NULL;

    pDeviceAccessKeyBuff = dxMemAlloc ("DevAccessKeyBuff", 1, DeviceAccessKeyBuffSize);
    if (!pDeviceAccessKeyBuff) {
        goto fail_alloc_key;
    }

    DevInfoRet = dxDevInfo_GetData (DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, pDeviceAccessKeyBuff, &DeviceAccessKeyBuffSize, dxFALSE);
    if (DevInfoRet) {
        goto fail_alloc_data;
    }

    if (!strlen (pDeviceAccessKeyBuff)) {
        goto fail_alloc_data;
    }

    if (BackupDataAccess == NULL) {
        BackupDataAccess = dxAppData_Init (DXBLOCKID_DEVINFO);
    }

    if (!BackupDataAccess) {
        goto fail_alloc_data;
    }

    RestoreDataSize = dxAppData_ReadDataSize (BackupDataAccess);
    if (!RestoreDataSize) {
        goto fail_alloc_data;
    }

    pBackupDataToRestore = dxMemAlloc ("RestoreData", 1, RestoreDataSize);
    if (!pBackupDataToRestore) {
        goto fail_alloc_data;
    }

    ret = dxAppData_Read (BackupDataAccess, (void*) pBackupDataToRestore, &RestoreDataSize);
    if (ret) {
        goto fail;
    }

    if (!pBackupDataToRestore->Header.PeripheralGenInfoSize) {
        goto fail;
    }

    remains = pBackupDataToRestore->Header.PeripheralGenInfoSize % sizeof (DeviceGenericInfo_t);
    lastInfo = (DeviceGenericInfo_t*) (pBackupDataToRestore->Payload + pBackupDataToRestore->Header.PeripheralGenInfoSize - sizeof (DeviceGenericInfo_t));

    if (remains || ((lastInfo->DevAddress.Addr[3] != 0xff) && (lastInfo->DevAddress.Addr[4] != 0xff))) {
        uint16_t TotalElement = pBackupDataToRestore->Header.PeripheralGenInfoSize /sizeof(OldDeviceGenericInfo_t);
        uint16_t i = 0;
        OldDeviceGenericInfo_t* DeviceGenericInfo = (OldDeviceGenericInfo_t*)pBackupDataToRestore->Payload;

        for (i = 0; i < TotalElement; i++)
        {
            OldDeviceGenericInfo_t* WorkingElement = DeviceGenericInfo + i;

            if (memcmp (WorkingElement->DevAddress.Addr, macaddr, macaddrlen) == 0) {
                deviceInfo = (DeviceGenericInfo_t*) dxMemAlloc ("deviceInfo", 1, sizeof (DeviceGenericInfo_t));
                memcpy(deviceInfo->DevAddress.Addr, WorkingElement->DevAddress.Addr, B_ADDR_LEN_8);
                memcpy(deviceInfo->SecurityKey.value, WorkingElement->SecurityKey.value, MAXIMUM_SECURITY_KEY_SIZE_ALLOWED);
                deviceInfo->SecurityKey.Size = WorkingElement->SecurityKey.Size;
                deviceInfo->DeviceType = 0;
                break;
            }
        }
    } else {
        uint16_t TotalElement = pBackupDataToRestore->Header.PeripheralGenInfoSize /sizeof(DeviceGenericInfo_t);
        uint16_t i = 0;
        DeviceGenericInfo_t* DeviceGenericInfo = (DeviceGenericInfo_t*)pBackupDataToRestore->Payload;

        for (i = 0; i < TotalElement; i++)
        {
            DeviceGenericInfo_t* WorkingElement = DeviceGenericInfo + i;

            if (memcmp (WorkingElement->DevAddress.Addr, macaddr, macaddrlen) == 0) {
                deviceInfo = (DeviceGenericInfo_t*) dxMemAlloc ("deviceInfo", 1, sizeof (DeviceGenericInfo_t));
                memcpy(deviceInfo->DevAddress.Addr, WorkingElement->DevAddress.Addr, B_ADDR_LEN_8);
                memcpy(deviceInfo->SecurityKey.value, WorkingElement->SecurityKey.value, MAXIMUM_SECURITY_KEY_SIZE_ALLOWED);
                deviceInfo->SecurityKey.Size = WorkingElement->SecurityKey.Size;
                deviceInfo->DeviceType = WorkingElement->DeviceType;
                break;
            }
        }
    }

fail :

    if (pBackupDataToRestore) {
        dxMemFree (pBackupDataToRestore);
        pBackupDataToRestore = NULL;
    }

fail_alloc_data :

    dxMemFree(pDeviceAccessKeyBuff);

fail_alloc_key :

#endif //CENTRAL_OFFLINE_SUPPORT

    return deviceInfo;
}
