//============================================================================
// File: HpJobBridge.h
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/17
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/11/28
//     1) Description: Move to new FW
//
//============================================================================
#ifndef _HP_JOB_BRIDGE_H
#define _HP_JOB_BRIDGE_H

//============================================================================
// INCLUDES
//============================================================================
//#include "HybridPeripheralMainApp.h"
#include "dk_Peripheral.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    uint32_t        AdvJobObjID;
    uint32_t        DataLen;
    uint64_t        IdentificationNumber;    
    uint8_t         pDataPayload[0];    //Holding of Payload ADDRESS
} HpJobBridgeJobPayload_t;

typedef struct
{
    int16_t         StatusReport;           //Check HpJobBridgeAdvJobAdd       
    uint16_t        SourceOfOrigin;
    uint64_t        IdentificationNumber;
} HpJobBridgeExeRes_t;

//============================================================================
// Constants
//============================================================================

//============================================================================
// Var
//============================================================================

//============================================================================
// Function declearancd
//============================================================================

//============================================================================
// Function definition
//============================================================================

//HP -> FW
dxBOOL HP_CMD_JobBridge_AdvJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority);
dxBOOL HP_CMD_JobBridge_SingleGwAdvJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority);
dxBOOL HP_CMD_JobBridge_OneTimeJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority, uint64_t IdentificationNumber);
dxBOOL HP_CMD_JobBridge_ScheduleJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint32_t Priority);

//FW -> HP
dxBOOL HP_CMD_JobBridge_AdvJobBridgeExeRes(uint32_t MessageID, HpJobBridgeExeRes_t *pJobBridgeExeRes, uint32_t Priority);

#endif // _HP_JOB_BRIDGE_H