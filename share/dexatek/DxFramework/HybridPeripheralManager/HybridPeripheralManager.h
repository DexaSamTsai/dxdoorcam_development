//============================================================================
// File: HybridPeripheralManager.h
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DXHPMANAGER_H
#define _DXHPMANAGER_H

#include "dxOS.h"
#include "dk_Peripheral.h"



//============================================================================
// Defines
//============================================================================

#define STORAGE_ACCESS_TEST_CODE            0  //0 - Dissable, 1 - Enable
#define CONTROL_ACCESS_TEST_CODE            0  //0 - Dissable, 1 - Enable
#define DATA_EXCHANGE_TEST_CODE             0  //0 - Dissable, 1 - Enable

#define MAX_STORAGE_ACCESS_READ_SIZE        4096

//============================================================================
// Enumeration
//============================================================================

typedef enum
{
    /*---- DATA EXCHANGE COMMAND MESSAGE ------------------------------------*/

    //DataExchange Direction = HP to Framework
    HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE                      = 1,    //Payload given as HpDataExcStatDataUpdate_t
    HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ,                             //Payload given as HpDataExcContainerDataRequest_t
    HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ,                 //Payload given as HpDataExcCreateFullContainerData_t
    HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ,               //Payload given as HpDataExcInsertContainerDetailData_t
    HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ,               //Payload given as HpDataExcUpdateContainerDetailData_t

    HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ,               //Payload given as HpDataExcDataTypeDetailData_t - This update Peripheral data status only in data type format
    HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ,            //Payload given as HpDataExcHistoryEventDetailData_t - This update both Peripheral data status & history event in data type format
    HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_REQ,                     //Payload given as HpDataAggregatedDataDetailData_t
    HP_CMD_DATAEXCHANGE_GET_DEVICE_GENERIC_INFO_REQ,                    //Payload given as HpGetDeviceGenericInfoRequest_t

    //DataExchange Direction = Framework to HP
    HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE_RES                  = 100,  //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT,                          //Payload given as HpDataExcContainerDataReport_t
                                                                        //This message will automatically be reported to HP during
                                                                        //initial boot, as well as it will be reported as response from HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ
                                                                        //NOTE: The message ID will follow the provided one when requested from HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ
    HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_RES,                 //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_RES,               //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_RES,               //Payload given as generic HpDataExcRes_t

    HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_RES,               //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_RES,            //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_RES,                     //Payload given as generic HpDataExcRes_t
    HP_CMD_DATAEXCHANGE_GET_DEVICE_GENERIC_INFO_RES,                    //Payload given as generic HpDeviceGenericInfo_t


    /*---- STORAGE ACCESS COMMAND MESSAGE -----------------------------------*/

    //Storage Access Direction = HP to Framework
    HP_CMD_STORAGE_ACCESS_STORAGE_INFO_REQUEST                  = 200,
    HP_CMD_STORAGE_ACCESS_STORAGE_WRITE_REQUEST,
    HP_CMD_STORAGE_ACCESS_STORAGE_READ_REQUEST,
    HP_CMD_STORAGE_ACCESS_STORAGE_ERASE_REQUEST,

    //Storage Access Direction = Framework to HP
    HP_CMD_STORAGE_ACCESS_STORAGE_INFO_RES                      = 300,
    HP_CMD_STORAGE_ACCESS_STORAGE_WRITE_RES,
    HP_CMD_STORAGE_ACCESS_STORAGE_READ_RES,
    HP_CMD_STORAGE_ACCESS_STORAGE_ERASE_RES,


    /*---- CONTROL MODULE & PROFILE COMMAND MESSAGE -------------------------*/

    //Control Access Direction = Framework to HP
    HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST = 400,  //Right after access it is required to update status data (SValue)
    HP_CMD_CONTROL_ACCESS_WRITE_REQUEST,
    HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST,         //TODO: Implementation to support such is not yet finalized.

    //Control Access Direction = HP to Framework
    HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_RES     = 500,  //HpControlAccessRes_t - Must provide status report right after both the "actual" write access & sending the status data update request via DataExchange (HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ)
    HP_CMD_CONTROL_ACCESS_WRITE_RES,                                    //HpControlAccessRes_t - Must provide status report right after the "actual" write access.
    HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_RES,             //HpControlAccessRes_t - Must provide status report right after both the "actual" write access & sending the status data update request via DataExchange (HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_REQ).


    /*---- Video & Image Command MESSAGE ------------------------------------*/

    //Video&Image Access Direction = HP to Framework
    HP_CMD_STREAMING_URL_REQUEST                                = 600,  //No Payload
    HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST,                   //Payload given as HpVideoImageEvtUrlReq_t
    HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST,                             //Payload given as HpEvtNotificationReq_t
    HP_CMD_NOTIFIY_EVENT_DONE_SEND_REQUEST,                             //Payload given as HpNotifyEvtDoneReq_t

    //Video&Image Access Direction = Framework to HP
    HP_CMD_STREAMING_URL_RES                                    = 650,  //Payload given as HpStreamingUrlInfo_t
    HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES,                       //Payload given as HpVideoImageEvtUrlInfo_t
    HP_CMD_EVENT_NOTIFICATION_SEND_RES,                                 //Payload given as HpEvtGenericRes_t
    HP_CMD_NOTIFY_EVENT_DONE_SEND_RES,                                  //Payload given as HpEvtGenericRes_t

    /*---- FrameWork State & Status Report MESSAGE --------------------------*/

    //Status Report Direction = Framework to HP
    //WARNING: State EXITING should always follow right after ENTERING (eg if ENTERING is 600 then same state EXITING should be 601)
    HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING                   = 800,  //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_IDLE_EXITING,                           //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING,                      //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_EXITING,                       //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING,                       //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_EXITING,                        //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING,                     //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_EXITING,                      //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING,                         //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_SETUP_EXITING,                          //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_ENTERING,                      //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_EXITING,                       //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_FATAL_ENTERING,                         //No Payload
    HP_CMD_FRW_STATUS_REPORT_ST_FATAL_EXITING,                          //No Payload

    HP_CMD_FRW_STATUS_REPORT_SERVER_READY,                              //No Payload
    HP_CMD_FRW_STATUS_REPORT_ACTIVE_USER_DETECTED,                      //No Payload - this message will be received periodically when end user is operating the app
    HP_CMD_FRW_STATUS_REPORT_NO_ACTIVE_USER,                            //No Payload - this message will be received periodically when end user is not longer operating the app
    HP_CMD_FRW_STATUS_REPORT_BLE_RESET,                                 //Payload given as eHpBleResetGenericStatus


    /*---- System Report MESSAGE --------------------------------------------*/

    //System Report Direction = Framework to HP
    HP_CMD_ENTER_FW_UPDATE_REPORT                               = 900,  //Payload given as HpFwUpdateStatusRes_t
    HP_CMD_SYSTEM_NOT_REGISTERED,                                       //No Payload, will be sent when framewrok is in IDLE state and system is not registered
    HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_REQ,                      //Payload given as HpProductionSelfTestReportReq_t
    HP_CMD_BLE_DEVICE_INFO_UPDATE,                                      //Update the AdvData of BLE peripheral advertised, use DeviceInfoReport_t as data
    HP_CMD_BLE_DEVICE_KEEPER_LIST_UPDATE,                               //Update the device keeper list of BLE, device keeper list is an array of DeviceGenericInfo_t
    HP_CMD_CHECK_FW_UPDATE_AVAILABE_RES,                                //Response FW availability, data refer HpFwUpdateAvailableRes_t
    HP_CMD_SYS_REPORT_GET_LOCAL_TIME_RES,                               //Payload given as HpLocalTimeGenericRes_t
    HP_CMD_SYS_GET_SECURITY_KEY_RES,                                    //Payload given as HpGetSecurityKeyRes_t

    //System Report Direction = HP to Framework
    HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_RES               = 950,  //Payload given as HpProductionSelfTestReportRes_t
    HP_CMD_ENTER_SYSTEM_PAIRING_MODE_REQUEST,                           //Requst system enter System paring mode for a period, data refer HpSystemPairing_t
    HP_CMD_CONFIG_WRITE_REQUEST,                                        //Write config to system, the config need to updated when central state cheange, config type refer HpConfigWrite_t
    HP_CMD_CHECK_FW_UPDATE_AVAILABE_REQUEST,                            //Reqeust information about FW update availability, no data payload
    HP_CMD_ENTER_FW_UPDATE_REQUEST,                                     //Request execute FW update, no data payload
    HP_CMD_ENTER_FACTORY_RESET_REQUEST,                                 //Request factory reset, no data payload
    HP_CMD_SYS_REPORT_GET_LOCAL_TIME_REQ,                               //No Payload
    HP_CMD_SYS_GET_SECURITY_KEY_REQ,                                    //No Payload

    /* HP Internal Report MESSAGE = From HP Client/Server Thread
     * WARNING: Highly recommended to hanlde this internal command report, upon receiving this, it is suggested to initialize the HP again.
    */
    HP_INTERNAL_REPORT_CLIENT_ERROR_TERMINATED                  = 1000, //No Payload
    HP_INTERNAL_REPORT_SERVER_ERROR_TERMINATED,


    /*---- Job Bridging -----------------------------------------------------*/

    //Job bridging Direction = HP to Framework
    HP_CMD_JOBBRIDGE_ADD_ADV_JOB_REQUEST                        = 1100, //Transfer adv job payload to job manager, could use reserved Adv Job ID for identify
    HP_CMD_JOBBRIDGE_ADD_SINGLE_GW_ADV_JOB_REQUEST,                     //Transfer Single GW execution Adv job payload to job manager, could use reserved Adv Job ID for identify
    HP_CMD_JOBBRIDGE_ONE_TIME_EXECUTE_JOB_REQUEST,                      //Transfer one time execution to job manager
    HP_CMD_JOBBRIDGE_ADD_SCHEDULE_JOB_REQUEST,                          //Transfer job payload to job manager, use SCHEDULE_SEQNO to identify different jobs
    HP_CMD_JOBBRIDGE_JOB_SUPERIOR_ONLY_CONFIG,                          //Set only Job Superior jobs can be execute or not, refer HpJobSuperiorOnly_t

    //Job bridging Direction = Framework to HP
    HP_CMD_JOBBRIDGE_EXE_RES                                    = 1200, //Return status of Job Bridge execution status



    /*---- Storage Access ---------------------------------------------------*/

    //Container Storage Access Direction = HP to Framework
    HP_CMD_CONT_STORAGE_INIT_REQUEST                           = 1500,
    HP_CMD_CONT_STORAGE_UNINIT_REQUEST,
    HP_CMD_CONT_STORAGE_READ_ALL_REQUEST,
    HP_CMD_CONT_STORAGE_FIND_REQUEST,
    HP_CMD_CONT_STORAGE_WRITE_REQUEST,
    HP_CMD_CONT_STORAGE_FORMAT_REQUEST,

    //Container Storage Access Direction = HP to Framework
    HP_CMD_CONT_STORAGE_INIT_RES                               = 1600,
    HP_CMD_CONT_STORAGE_UNINIT_RES,
    HP_CMD_CONT_STORAGE_READ_ALL_RES,
    HP_CMD_CONT_STORAGE_FIND_RES,
    HP_CMD_CONT_STORAGE_WRITE_RES,
    HP_CMD_CONT_STORAGE_FORMAT_RES,
    HP_CMD_CONT_STORAGE_DATA_RES,
    HP_CMD_CONT_STORAGE_WRITE_COMPLETE_RES,
    HP_CMD_CONT_STORAGE_REINIT_REQ,


    /*---- Concurrent Peripheral --------------------------------------------*/

    // Framework to HP
    HP_CMD_CONCURRENT_PERIPHERAL_GET_STATUS_RES = 1700,                 //Payload given as eHpBleConcurrentPeripheralStatus
    HP_CMD_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS_RES,              //Payload given as HpConcurrentPeripheralBdAddressResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_SET_SECURITY_KEY_RES,                  //Payload given as HpConcurrentPeripheralSecurityKeyResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_SET_ADVERTISE_DATA_RES,                //Payload given as HpConcurrentPeripheralAdvertiseDataResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_SET_SCAN_RESPONSE_RES,                 //Payload given as HpConcurrentPeripheralScanResponseResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_RES,                 //Payload given as HpConcurrentPeripheralServiceCharacteristicResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_ENABLE_SYSTEM_UTILS_RES,               //Payload given as HpConcurrentPeripheralSystemUtilsResponse_t
    HP_CMD_CONCURRENT_PERIPHERAL_START_RES,                             //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_STOP_RES,                              //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_DISCONNECT_RES,                        //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_DATA_IN_UPDATE,                        //Payload given as HpConcurrentPeripheralData_t

    // HP to Framework
    HP_CMD_CONCURRENT_PERIPHERAL_GET_STATUS_REQ = 1800,                 //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS_REQ,              //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_SET_SECURITY_KEY_REQ,                  //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_SET_ADVERTISE_DATA_REQ,                //Payload given as DkAdvData_t
    HP_CMD_CONCURRENT_PERIPHERAL_SET_SCAN_RESPONSE_REQ,                 //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_REQ,                 //Payload given as HpConcurrentPeripheralServiceCharacteristicRequest_t
    HP_CMD_CONCURRENT_PERIPHERAL_ENABLE_SYSTEM_UTILS_REQ,               //Payload given as HpConcurrentPeripheralSystemUtilsReq_t
    HP_CMD_CONCURRENT_PERIPHERAL_START_REQ,                             //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_STOP_REQ,                              //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_DISCONNECT_REQ,                        //No Payload
    HP_CMD_CONCURRENT_PERIPHERAL_DATA_OUT_REQ,                          //Payload given as HpConcurrentPeripheralData_t


} eHpCmdMessage;


typedef enum
{
    HP_EVENT_TYPE_VIDEO = 0,
    HP_EVENT_TYPE_IMAGE,

} eHpEvtType_t;


typedef enum
{
    HP_FILE_TYPE_MP4 = 0,
    HP_FILE_TYPE_ZIP,
    HP_FILE_TYPE_TAR_GZ,

} eHpFileFormat_t;


typedef enum
{
    HP_TEMPLATE_STD = 0,

}eHpTemplateType_t;


typedef enum
{
    HP_MSG_EVENT_GET_EVENT = 1,
    HP_MSG_EVENT_GET_OFFLINE,

} eHpMsgEvtType_t;


enum eHPControlAccessStatus
{
    HP_CONTROL_ACCESS_SUCCESS = 0,

    HP_CONTROL_ACCESS_GENERIC_ERROR = -1,
    HP_CONTROL_ACCESS_INVALID_PARAM = -2,
    HP_CONTROL_ACCESS_NOT_SUPPORTED = -3,
    HP_CONTROL_ACCESS_TIME_OUT      = -4,
};

enum eHPDataExchangeStatus
{
    HP_DATA_EXCHANGE_SUCCESS = 0,

    HP_DATA_EXCHANGE_GENERIC_ERROR = -1,
    HP_DATA_EXCHANGE_INVALID_PARAM = -2,
    HP_DATA_EXCHANGE_NOT_SUPPORTED = -3,
    HP_DATA_EXCHANGE_TIME_OUT      = -4,
};

enum eHPFwUpdateAvailable{
    FW_UPDATE_AVAILABLE_NONE            = 0,
    FW_UPDATE_AVAILABLE_MAIN_FW,
    FW_UPDATE_AVAILABLE_BLE_FW,
    FW_UPDATE_AVAILABLE_AUTO_UPDATE,

    FW_UPDATE_AVAILABLE_GENERIC_ERROR   = -1,
};

enum eHPFwUpdateStatus{
    FW_UPDATE_STATUS_IDLE               = 0,
    FW_UPDATE_STATUS_MAIN_UPDATING,
    FW_UPDATE_STATUS_BLE_UPDATING,
    FW_UPDATE_STATUS_BLE_DOWNLOADING,

    FW_UPDATE_STATUS_GENERIC_ERROR      = -1,
};

enum eHPEvtGenRspStatus
{
    HP_EVENT_ACCESS_SUCCESS             = 0,

    HP_EVENT_ACCESS_ERROR               = -1,
    HP_EVENT_ACCESS_INVALID_PARAM       = -2,
};

enum eHPProductionSelfTestGeneralRspStatus
 {
    HP_PRODUCTION_SELF_TEST_SUCCESS     = 0,

    HP_PRODUCTION_SELF_TEST_FAILED      = -1,
};


enum eHpLocalTimeGenericStatus
 {
    HP_LOCAL_TIME_GET_SUCCESS     = 0,

    HP_LOCAL_TIME_GET_FAILED      = -1,
};


enum eHpGetSecurityKeyStatus {
    HP_GET_SECURITY_KEY_SUCCESS   = 0,

    HP_GET_SECURITY_KEY_FAILED      = -1,
};


typedef enum _eConfigId
{
    CONFIG_ID_NULL = 0,
    CONFIG_ID_BLE_PERIPHERAL_INFO_UPDATE,
    CONFIG_ID_BLE_DEVICE_KEEPER_LIST_UPDATE,

} eConfigId;


typedef enum _eHpBleResetGenericStatus {
    HP_BLE_RESET_TO_CENTRAL = 0,
    HP_BLE_RESET_TO_PERIPHERAL,
    HP_BLE_RESET_TO_UNKNOWN,

    HP_BLE_RESET_NUM,
} eHpBleResetGenericStatus;


typedef enum _eHpBleConcurrentPeripheralStatus {
    HP_BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT,
    HP_BLE_CONCURRENT_PERIPHERAL_STATE_IDLE,
    HP_BLE_CONCURRENT_PERIPHERAL_STATE_ADVERTISING,
    HP_BLE_CONCURRENT_PERIPHERAL_STATE_CONNECTED,

    HP_BLE_CONCURRENT_PERIPHERAL_STATE_NUM
} eHpBleConcurrentPeripheralStatus;



//============================================================================
// Structure
//============================================================================

typedef struct
{
    eHpCmdMessage   CommandMessage;
    uint32_t        MessageID;          //Returning ID on command message response for identification purposes.
    uint32_t        Priority;           //0 - Normal, 1 - High
    dxTime_t        TimeStamp;          //This is the time in UTC+0 when it was originally created
    uint32_t        PayloadSize;
    uint8_t*        PayloadData;

} HpCmdMessageQueue_t;


typedef struct
{
    uint64_t        ContMID;
    uint64_t        ContDID;
    uint32_t        ContMType;
    uint32_t        ContDType;
    uint16_t        SValueLen;
    uint8_t         SValue[0]; //Holding of SValue ADDRESS
} HpDataExcStatDataUpdate_t;

typedef struct
{
    uint64_t        ContMID;
    uint32_t        ContMType;
    uint64_t        ContDID;
    uint32_t        ContDType;
    uint32_t        DateTime;
    uint16_t        SValueLen;
    uint16_t        LValueLen;
    uint16_t        ContLVCRC;  //LValue CRC

    //Holding of SValue & LValue ADDRESS
    //NOTE: All value is concatenated into the same pointer buffer in such order SValue->LValue.
    //For example, SValue starts as the first index if SValueLen > 0, then follow by LValue... etc...
    uint8_t         CombinedValue[0];

} HpDataExcContainerDataReport_t;


typedef struct
{
    uint64_t        ContMID;
    uint64_t        ContDID;

} HpDataExcContainerDataRequest_t;

typedef struct
{
    uint32_t        ContMType;
    uint32_t        ContDType;
    uint16_t        ContMNameLen;
    uint16_t        ContDNameLen;
    uint16_t        SValueLen;
    uint16_t        LValueLen;
    uint16_t        ContLVCRC;  //LValue CRC

    //Holding of ContMName & ContDName & SValue & LValue ADDRESS
    //NOTE: All value is concatenated into the same pointer buffer in such order ContMName->ContDName->SValue->LValue.
    //For example, ContMName starts as the first index if ContMNameLen > 0, then follow by ContDName... etc...
    uint8_t         CombinedValue[0];

} HpDataExcCreateFullContainerData_t;


typedef struct
{
    uint64_t        ContMID;
    uint32_t        ContDType;
    uint16_t        ContDNameLen;
    uint16_t        SValueLen;
    uint16_t        LValueLen;
    uint16_t        ContLVCRC;  //LValue CRC

    //Holding of ContDName & SValue & LValue ADDRESS
    //NOTE: All value is concatenated into the same pointer buffer in such order ContDName->SValue->LValue.
    //For example, ContDName starts as the first index if ContDNameLen > 0, then follow by SValue... etc...
    uint8_t         CombinedValue[0];

} HpDataExcInsertContainerDetailData_t;


typedef struct
{
    uint64_t        ContMID;
    uint64_t        ContDID;
    uint32_t        ContDType;
    uint16_t        ContDNameLen;
    uint16_t        SValueLen;
    uint16_t        LValueLen;
    uint16_t        ContLVCRC;  //LValue CRC

    //Holding of ContDName & SValue & LValue ADDRESS
    //NOTE: All value is concatenated into the same pointer buffer in such order ContDName->SValue->LValue.
    //For example, ContDName starts as the first index if ContDNameLen > 0, then follow by SValue... etc...
    uint8_t         CombinedValue[0];

} HpDataExcUpdateContainerDetailData_t;

typedef struct
{
    uint32_t    PayloadDataSize;

    //Holding of sequence of DataType follow by its Data
    //[1 Byte DataType][N Byte Data][1 Byte DataType][N Byte Data] .........
    uint8_t     PayloadData[0];        //Holding of Payload ADDRESS

} HpDataExcDataTypeDetailData_t;

typedef struct
{
    uint32_t    PayloadDataSize;
    //Holding of sequence of DataType follow by its Data
    //[1 Byte DataType][N Byte Data][1 Byte DataType][N Byte Data] .........
    uint8_t     PayloadData[0];        //Holding of Payload ADDRESS

} HpDataExcHistoryEventDetailData_t;

typedef struct
{
    AggregatedDataHeader_t  Header;
    uint32_t                TotalAggregatedItems;
    uint32_t                PayloadDataSize;        //AggregatedData Size
    //The PayloadData contain bundle array of AggregateItem_t (if AGD format is set to 0) or AggregateItemAgD1_t (if AGD format is set to 1)
    uint8_t                 PayloadData[0];         //Holding of Payload ADDRESS

} HpDataAggregatedDataDetailData_t;


typedef struct
{
    DevAddress_t                    DevAddress;

} HpGetDeviceGenericInfoRequest_t;


typedef struct
{
    int32_t     Status;                 //Check eHPDataExchangeStatus
} HpDataExcRes_t;

typedef struct
{
    uint32_t        TotalSize;
    uint32_t        SectorSize;
} HpStorageInfo_t;

typedef struct
{
    uint32_t        StartPosition;
    uint32_t        DataLen;
    uint8_t         pDataPayload[0]; //Holding of Payload ADDRESS

} HpStorageWriteReq_t;

typedef struct
{
    int32_t         Status;          //The Message response

} HpStorageWriteRes_t;

typedef struct
{
    uint32_t        StartPosition;
    uint32_t        DataLenToRead;   //At the moment we limit it to MAX_STORAGE_ACCESS_READ_SIZE so that we do not allocate to much of memory

} HpStorageReadReq_t;

typedef struct
{
    int32_t         Status;          //The Message response
    uint32_t        DataLen;
    uint8_t         pDataPayload[0]; //Holding of Payload ADDRESS

} HpStorageReadRes_t;

typedef struct
{
    uint32_t        StartPosition;      //Must be start of sector aligned
    uint16_t        SectorSize;         //TotalSector to erase

} HpStorageEraseReq_t;

typedef struct
{
    int32_t         Status;          //The Message response

} HpStorageEraseRes_t;

typedef struct
{
    uint32_t    SourceOfOrigin;         //This must be returned as is when responding to the access status (HpControlAccessRes_t)
    uint64_t	 IdentificationNumber;   //This must be returned as is when responding to the access status (HpControlAccessRes_t)
    uint8_t     ServiceUUID[16];
    uint8_t     CharacteristicUUID[16];
    uint32_t    DataLen;
    uint8_t     pDataPayload[0];        //Holding of Payload ADDRESS

} HpControlAccessReq_t;

typedef struct
{
    int32_t     Status;                 //Check eHPControlAccessStatus
    uint32_t    SourceOfOrigin;
    uint64_t    IdentificationNumber;
    uint16_t    DataLen;                //Data lens of returned data which is returned to the source of origin
    uint8_t     pDataPayload[0];        //Holding of Payload ADDRESS
} HpControlAccessRes_t;

typedef struct
{
    int32_t    FwUpdateAvailabilityStatus;       //eHPFwUpdateAvailable
} HpFwUpdateAvailableRes_t;

typedef struct
{
    int32_t     FwUpdateDownloadType;   //eHPFwUpdateStatus
    uint8_t     Progress;               //0 to 100 in percent
} HpFwUpdateStatusRes_t;

typedef struct
{
    uint32_t        Timeout;    //timeout in MS
} HpSystemPairing_t;

typedef struct
{
    uint32_t        ConfigId;   //eConfigId
    uint16_t        DataLen;
    uint8_t         pData[0];
} HpConfigWrite_t;

typedef struct
{
    dxBOOL          bJobSuperiorOnly;    //enable Superior only period
} HpJobSuperiorOnly_t;

typedef struct
{
    uint32_t    UrlLen;
    uint8_t     pUrl[0];        //Holding of Url ADDRESS

} HpStreamingUrlInfo_t;


typedef struct
{
    eHpEvtType_t    EventType;
    eHpFileFormat_t FileFormat;
    uint32_t        DateTime;   //Optional, set to 0 or the datetime (UTC+0) in seconds of the event.

} HpVideoImageEvtUrlReq_t;


typedef struct
{
    uint32_t    UploadUrlLen;
    uint32_t    DownloadUrlLen;

    //Holding of UploadUrlString & DownloadUrlString ADDRESS
    //NOTE: All value is concatenated into the same pointer buffer in such order UploadUrlString->DownloadUrlString.
    //For example, UploadUrlString starts as the first index if UploadUrlLen > 0, then follow by DownloadUrlString (if DownloadUrlLen > 0)...
    uint8_t     CombineUrl[0];        //Holding of combined Url ADDRESS

} HpVideoImageEvtUrlInfo_t;

typedef struct
{
    eHpEvtType_t        MessageCategoryType;
    eHpTemplateType_t   MessageCategoryTemplate;
    eHpFileFormat_t     MessageCategoryFileType;
    uint32_t            MessageResourceLen;

    //Holding of MessageResourceString
    uint8_t             CombinePayload[0];        //Holding of resource payload, a string and including null terminated

}HpEvtNotificationReq_t;

typedef struct
{
    eHpMsgEvtType_t     MessageCategoryType;
	uint32_t            MessageResourceLen;

    //Holding of MessageResourceString
    uint8_t             CombinePayload[0];        //Holding of resource payload, a string and including null terminated

}HpNotifyEvtDoneReq_t;

typedef struct
{
    int32_t             Status;                   //eHPEvtGenRspStatus

} HpEvtGenericRes_t;


typedef struct
{
    uint32_t        DataLen;

    //NOTE: The production self test report data payload content (either request or response) is individually defined by HybridPeripheral main implementation
    uint8_t         pDataPayload[0]; //Holding of Payload ADDRESS

} HpProductionSelfTestReportReq_t;


typedef struct
{
    int32_t         Status;     //eHPProductionSelfTestGeneralRspStatus
    uint32_t        DataLen;

    //NOTE: The production self test report data payload content (either request or response) is individually defined by HybridPeripheral main implementation
    uint8_t         pDataPayload[0]; //Holding of Payload ADDRESS

} HpProductionSelfTestReportRes_t;


typedef struct
{
    int32_t         Status;          //eHpLocalTimeGenericStatus
    DKTimeLocal_t   Time;

} HpLocalTimeGenericRes_t;


typedef struct {
    int32_t         status;          //eHpGetSecurityKeyStatus
    uint8_t         securityKey[AES_ENC_DEC_BYTE];
} HpGetSecurityKeyRes_t;


typedef struct {
    int16_t     result;
} HpConcurrentPeripheralBdAddressResponse_t;


typedef struct {
    uint8_t    keyLength;
} HpConcurrentPeripheralSecurityKeyResponse_t;


typedef struct {
    int16_t     totalBytes;
} HpConcurrentPeripheralAdvertiseDataResponse_t;


typedef struct {
    int16_t     totalBytes;
} HpConcurrentPeripheralScanResponseResponse_t;


typedef DX_PACK_STRUCT_BEGIN {
    uint16_t    uuid;
    uint8_t     property;
    uint16_t    reserveSpace;
} HpConcurrentPeripheralServiceCharacteristic_t;


typedef struct {
    uint16_t                                        serviceUuid;
    uint8_t                                         characteristicCount;
    HpConcurrentPeripheralServiceCharacteristic_t   characteristics[8];
} HpConcurrentPeripheralServiceCharacteristicRequest_t;


typedef struct {
    uint16_t    serviceUuid;
    int8_t      characteristicCount;
} HpConcurrentPeripheralServiceCharacteristicResponse_t;


typedef struct {
    uint16_t    mask;
} HpConcurrentPeripheralSystemUtilsReq_t;


typedef struct {
    uint16_t    enableMask;
} HpConcurrentPeripheralSystemUtilsResponse_t;


typedef struct {
    uint16_t        dataLength;
    uint8_t         data[240];
} HpConcurrentPeripheralData_t;


typedef struct {
    DevAddress_t                    DevAddress;
    uint16_t                        DeviceType;
    SecurityKeyCodeContainer_t      SecurityKey;
} HpDeviceGenericInfo_t;



//============================================================================
// Type Definitions
//============================================================================

typedef enum
{
    HP_MANAGER_SUCCESS                              = 0,    /**< Success */
    HP_MANAGER_INTERNAL_ERROR                       = -21,
    HP_MANAGER_TOO_MANY_TASK_IN_QUEUE               = -22,
    HP_MANAGER_INVALID_PARAMETER                    = -23,
} HPManager_result_t;

typedef void (*HpEventHandler_t)(void *arg);



//============================================================================
// Function Declarations
//============================================================================

#if DEVICE_IS_HYBRID

/**
 * @brief Initialize HybridPeripheral Server Manager, all task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  NOTE1: The arg pointer is a pointer to structure type HpCmdMessageQueue_t
 *  NOTE2: According to the message command cast the payload to appropriate structure type to obtain the data if needed. See eHpCmdMessage for detail
 *  NOTE3: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE4: At the end of EventFunction call HpManagerClearEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[IN] EventFunction - The EventFunction that will be called when reporting activities
 * @return HPManager_result_t
 */
HPManager_result_t HpManagerInit_Server(HpEventHandler_t EventFunction);


/**
 * @brief Initialize HybridPeripheral Client Manager, all task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  NOTE1: The arg pointer is a pointer to structure type HpCmdMessageQueue_t
 *  NOTE2: According to the message command cast the payload to appropriate structure type to obtain the data if needed. See eHpCmdMessage for detail
 *  NOTE3: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE4: At the end of EventFunction call HpManagerClearEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[IN] EventFunction - The EventFunction that will be called when reporting activities
 * @return HPManager_result_t
 */
HPManager_result_t HpManagerInit_Client(HpEventHandler_t EventFunction);


/**
 * @brief Send HpCmdMessage to Client
 *
 * @param[IN] HpCmdMessageQueue_t - The HybridPeripheral Command Message data
 * @return HPManager_result_t
 */
HPManager_result_t HpManagerSendTo_Client(HpCmdMessageQueue_t *pHpCmdMessage);

/**
 * @brief Send HpCmdMessage to HP Server
 *
 * @param[IN] HpCmdMessageQueue_t - The HybridPeripheral Command Message data
 * @return HPManager_result_t
 */
HPManager_result_t HpManagerSendTo_Server(HpCmdMessageQueue_t *pHpCmdMessage);


/**
 * @brief Clean reported event obj arg
 *
 * @param[IN] ReportedEventArgObj - The reported obj argument to obtain the header info
 * @return HPManager_result_t
 */
HPManager_result_t HpManagerCleanEventArgumentObj(void *ReportedEventArgObj);


/**
 * @brief Convert the result of HP to JobDone report format that HP server needs
 *
 * @param[IN] HybridPeripheralControlAccessResultToConvert - The result to convert to.
 * @return int16_t - the converted result (eHPControlAccessStatus)
 */
int16_t ConvertHybridPeripheralControlAccessResultStateToJobDoneReportStatus(int32_t HybridPeripheralControlAccessResultToConvert); //eHPControlAccessStatus

#endif //#if DEVICE_IS_HYBRID

#endif // _DXHPMANAGER_H
