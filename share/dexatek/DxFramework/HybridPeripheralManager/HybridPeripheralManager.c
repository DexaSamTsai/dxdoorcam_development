//============================================================================
// File: HybridPeripheralManager.c
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dxSysDebugger.h"
#include "HybridPeripheralManager.h"

#if DEVICE_IS_HYBRID

//============================================================================
// Constants
//============================================================================

#define HP_CMD_SERVER_THREAD_NAME                        "Hp SRV"
#define HP_CMD_CLIENT_THREAD_NAME                        "Hp CLN"
#define HP_CMD_PROCESS_THREAD_PRIORITY                    DXTASK_PRIORITY_NORMAL
#define HP_CMD_SERVER_STACK_SIZE                          1536
#define HP_CMD_CLIENT_STACK_SIZE                          1280

#define HP_CMD_SERVER_GLOBAL_MESSAGE_QUEUE_NAME            "/dxHpCmdSrvGlobalMQ"
#define HP_CMD_CLIENT_GLOBAL_MESSAGE_QUEUE_NAME            "/dxHpCmdClientGlobalMQ"

#define MAXIMUM_HP_CMD_MESSAGE_QUEUE_DEPT_ALLOWED           20
#define MAXIMUM_HP_CMD_MESSAGE_SIZE                         (sizeof(HpCmdMessageQueue_t) + (1024*4))

#define SERVER_CLIENT_GLOBAL_MQ_WAIT_COUNT_MAX              10

//============================================================================
// Type Definitions
//============================================================================

//============================================================================
// Global Variable
//============================================================================

static dxQueueHandle_t         HpCmdMessageQueue_Server = NULL;
static dxQueueHandle_t         HpCmdMessageQueue_Client = NULL;

static dxTaskHandle_t   HpCmdServerMainProcess_thread;
static dxTaskHandle_t   HpCmdClientMainProcess_thread;

static HpEventHandler_t RegisteredEventFunction_Server;
static HpEventHandler_t RegisteredEventFunction_Client;

#pragma default_variable_attributes = @ ".sdram.text"

uint8_t pHpCmdMessageQueueBuffServer[MAXIMUM_HP_CMD_MESSAGE_SIZE];
uint8_t pHpCmdMessageQueueBuffClient[MAXIMUM_HP_CMD_MESSAGE_SIZE];

#pragma default_variable_attributes =

//============================================================================
// Function Declarations
//============================================================================

static TASK_RET_TYPE HpCmdServer_Process_thread_main( TASK_PARAM_TYPE arg );
static TASK_RET_TYPE HpCmdClient_Process_thread_main( TASK_PARAM_TYPE arg );

//============================================================================
// Local Function
//============================================================================

void HyperPeripheralMessageRelease(HpCmdMessageQueue_t* pHpCmdMessage)
{
    if (pHpCmdMessage)
    {
        if (pHpCmdMessage->PayloadData)
            dxMemFree(pHpCmdMessage->PayloadData);

        dxMemFree(pHpCmdMessage);
        pHpCmdMessage = NULL;
    }
}

//============================================================================
// Hybrid Peripheral Server/client process handler
//============================================================================

static TASK_RET_TYPE HpCmdServer_Process_thread_main( TASK_PARAM_TYPE arg )
{
    dxQueueHandle_t ClientHpCmdMessageQueue = NULL;
    uint16_t        WaitCount = 0;

    while(1)
    {
        if (ClientHpCmdMessageQueue == NULL)
        {
            if ((ClientHpCmdMessageQueue = dxQueueGlobalGet(HP_CMD_SERVER_GLOBAL_MESSAGE_QUEUE_NAME)) == NULL)
            {
                if (WaitCount > SERVER_CLIENT_GLOBAL_MQ_WAIT_COUNT_MAX)
                {
                    G_SYS_DBG_LOG_V( "WARNING: HpCmdServer_Process_thread_main unable to obtain global MQ\n");

                    HpCmdMessageQueue_t* pHpCmdMessage = dxMemAlloc("", 1, sizeof(HpCmdMessageQueue_t));
                    memset(pHpCmdMessage, 0, sizeof(HpCmdMessageQueue_t));
                    pHpCmdMessage->CommandMessage = HP_INTERNAL_REPORT_SERVER_ERROR_TERMINATED;
                    if (RegisteredEventFunction_Server)
                    {
                        HpEventHandler_t RegisteredEventFunction_ServerTemp = RegisteredEventFunction_Server;
                        RegisteredEventFunction_Server = NULL;
                        RegisteredEventFunction_ServerTemp((void*)pHpCmdMessage);
                    }
                    else
                    {
                        HyperPeripheralMessageRelease(pHpCmdMessage);
                    }

                    break;
                }

                dxTimeDelayMS(1000);
                WaitCount++;
                continue;
            }
        }
        else
        {
            if (dxQueueReceive( ClientHpCmdMessageQueue, pHpCmdMessageQueueBuffServer, DXOS_WAIT_FOREVER ) == DXOS_SUCCESS)
            {
                HpCmdMessageQueue_t* pHpCmdMessage = dxMemAlloc("", 1, sizeof(HpCmdMessageQueue_t));
                uint16_t             MessageHeaderSize = sizeof(HpCmdMessageQueue_t) - sizeof(((HpCmdMessageQueue_t*)0)->PayloadData);

                memcpy((uint8_t*)pHpCmdMessage, pHpCmdMessageQueueBuffServer, MessageHeaderSize);

                if (pHpCmdMessage->PayloadSize)
                {
                    uint8_t* pMessagePayloadBuff = dxMemAlloc("pMessagePayloadBuff", 1, pHpCmdMessage->PayloadSize);
                    memcpy(pMessagePayloadBuff, pHpCmdMessageQueueBuffServer + MessageHeaderSize, pHpCmdMessage->PayloadSize);
                    pHpCmdMessage->PayloadData = pMessagePayloadBuff;
                }
                else
                {
                    pHpCmdMessage->PayloadData = NULL;
                }

                if (RegisteredEventFunction_Server)
                    RegisteredEventFunction_Server((void*)pHpCmdMessage);
                else
                    HyperPeripheralMessageRelease(pHpCmdMessage);
            }
        }
    }

    END_OF_TASK_FUNCTION;
}

static TASK_RET_TYPE HpCmdClient_Process_thread_main( TASK_PARAM_TYPE arg )
{
    dxQueueHandle_t ServerHpCmdMessageQueue = NULL;
    uint16_t        WaitCount = 0;

    while(1)
    {
        if (ServerHpCmdMessageQueue == NULL)
        {
            if ((ServerHpCmdMessageQueue = dxQueueGlobalGet(HP_CMD_CLIENT_GLOBAL_MESSAGE_QUEUE_NAME)) == NULL)
            {
                if (WaitCount > SERVER_CLIENT_GLOBAL_MQ_WAIT_COUNT_MAX)
                {
                    G_SYS_DBG_LOG_V( "WARNING: HpCmdClient_Process_thread_main unable to obtain global MQ\n");

                    HpCmdMessageQueue_t* pHpCmdMessage = dxMemAlloc("", 1, sizeof(HpCmdMessageQueue_t));
                    memset(pHpCmdMessage, 0, sizeof(HpCmdMessageQueue_t));
                    pHpCmdMessage->CommandMessage = HP_INTERNAL_REPORT_CLIENT_ERROR_TERMINATED;
                    if (RegisteredEventFunction_Client)
                    {
                        printf("HpCmdClient_Process_thread_main calling funcCallback");
                        HpEventHandler_t RegisteredEventFunction_ClientTemp = RegisteredEventFunction_Client;
                        RegisteredEventFunction_Client = NULL;
                        RegisteredEventFunction_ClientTemp((void*)pHpCmdMessage);
                    }
                    else
                    {
                        HyperPeripheralMessageRelease(pHpCmdMessage);
                    }

                    break;
                }

                dxTimeDelayMS(1000);
                WaitCount++;
                continue;
            }
        }
        else
        {
            if (dxQueueReceive( ServerHpCmdMessageQueue, pHpCmdMessageQueueBuffClient, DXOS_WAIT_FOREVER ) == DXOS_SUCCESS)
            {

                HpCmdMessageQueue_t* pHpCmdMessage = dxMemAlloc("", 1, sizeof(HpCmdMessageQueue_t));
                uint16_t             MessageHeaderSize = sizeof(HpCmdMessageQueue_t) - sizeof(((HpCmdMessageQueue_t*)0)->PayloadData);

                memcpy((uint8_t*)pHpCmdMessage, pHpCmdMessageQueueBuffClient, MessageHeaderSize);

                if (pHpCmdMessage->PayloadSize)
                {
                    uint8_t* pMessagePayloadBuff = dxMemAlloc("pMessagePayloadBuff", 1, pHpCmdMessage->PayloadSize);
                    memcpy(pMessagePayloadBuff, pHpCmdMessageQueueBuffClient + MessageHeaderSize, pHpCmdMessage->PayloadSize);
                    pHpCmdMessage->PayloadData = pMessagePayloadBuff;
                }
                else
                {
                    pHpCmdMessage->PayloadData = NULL;
                }

                if (RegisteredEventFunction_Client)
                    RegisteredEventFunction_Client((void*)pHpCmdMessage);
                else
                    HyperPeripheralMessageRelease(pHpCmdMessage);
            }
        }
    }

    END_OF_TASK_FUNCTION;
}


//============================================================================
// Hybrid Peripheral Supported Functions
//============================================================================

HPManager_result_t HpManagerInit_Server(HpEventHandler_t EventFunction)
{
    G_SYS_DBG_LOG_V("%s\r\n", __FUNCTION__);

    HPManager_result_t  result;

    result = HP_MANAGER_SUCCESS;

    if (EventFunction)
    {
        if (RegisteredEventFunction_Server == NULL)    //First time, we create worker thread first
        {

            dxOS_RET_CODE dxOSResult = dxTaskCreate(HpCmdServer_Process_thread_main,
                                                    HP_CMD_SERVER_THREAD_NAME,
                                                    HP_CMD_SERVER_STACK_SIZE,
                                                    NULL,
                                                    HP_CMD_PROCESS_THREAD_PRIORITY,
                                                    &HpCmdServerMainProcess_thread);
            if (dxOSResult != DXOS_SUCCESS)
            {
                result = HP_MANAGER_INTERNAL_ERROR;
                goto EndInit;
            }

            GSysDebugger_RegisterForThreadDiagnostic(&HpCmdServerMainProcess_thread);

            RegisteredEventFunction_Server = EventFunction;


            HpCmdMessageQueue_Server = dxQueueGlobalCreate( HP_CMD_SERVER_GLOBAL_MESSAGE_QUEUE_NAME,
                                                            MAXIMUM_HP_CMD_MESSAGE_QUEUE_DEPT_ALLOWED,
                                                            MAXIMUM_HP_CMD_MESSAGE_SIZE);


            HpCmdMessageQueue_Client = dxQueueGlobalCreate( HP_CMD_CLIENT_GLOBAL_MESSAGE_QUEUE_NAME,
                                                            MAXIMUM_HP_CMD_MESSAGE_QUEUE_DEPT_ALLOWED,
                                                            MAXIMUM_HP_CMD_MESSAGE_SIZE);

            if ((HpCmdMessageQueue_Server == NULL) || (HpCmdMessageQueue_Client == NULL))
            {
                RegisteredEventFunction_Server = NULL;
                dxTaskDelete(HpCmdServerMainProcess_thread);
                result = HP_MANAGER_INTERNAL_ERROR;
                G_SYS_DBG_LOG_V(  "WARNING: Error creating HpCmdMessageQueue_Server \r\n" );
                goto EndInit;
            }
        }
    }
    else
    {
        G_SYS_DBG_LOG_V( "WARNING: %s - MUST register EventFunction, system may become unstable if not registered\r\n", __FUNCTION__);
        result = HP_MANAGER_INVALID_PARAMETER;
        goto EndInit;
    }

EndInit:

    return result;
}


HPManager_result_t HpManagerInit_Client(HpEventHandler_t EventFunction)
{
    G_SYS_DBG_LOG_V("%s\r\n", __FUNCTION__);

    HPManager_result_t  result;

    result = HP_MANAGER_SUCCESS;

    if (EventFunction)
    {
        if (RegisteredEventFunction_Client == NULL)    //First time, we create worker thread first
        {

            dxOS_RET_CODE dxOSResult = dxTaskCreate(HpCmdClient_Process_thread_main,
                                                    HP_CMD_CLIENT_THREAD_NAME,
                                                    HP_CMD_CLIENT_STACK_SIZE,
                                                    NULL,
                                                    HP_CMD_PROCESS_THREAD_PRIORITY,
                                                    &HpCmdClientMainProcess_thread);
            if (dxOSResult != DXOS_SUCCESS)
            {
                result = HP_MANAGER_INTERNAL_ERROR;
                goto EndInit;
            }

            GSysDebugger_RegisterForThreadDiagnostic(&HpCmdClientMainProcess_thread);

            RegisteredEventFunction_Client = EventFunction;
        }
    }
    else
    {
        G_SYS_DBG_LOG_V( "WARNING: %s - MUST register EventFunction, system may become unstable if not registered\r\n", __FUNCTION__);
        result = HP_MANAGER_INVALID_PARAMETER;
        goto EndInit;
    }

EndInit:

    return result;
}



HPManager_result_t HpManagerSendTo_Client(HpCmdMessageQueue_t *pHpCmdMessage)
{
    HPManager_result_t result = HP_MANAGER_INTERNAL_ERROR;

    if (HpCmdMessageQueue_Client == NULL)
        HpCmdMessageQueue_Client = dxQueueGlobalGet(HP_CMD_CLIENT_GLOBAL_MESSAGE_QUEUE_NAME);

    if (HpCmdMessageQueue_Client)
    {
        uint32_t    MessageSize = pHpCmdMessage->PayloadSize + sizeof(HpCmdMessageQueue_t);
        uint8_t*    pMessageBuff = dxMemAlloc("pMessageBuff", 1, MessageSize);
        uint16_t    MessageHeader = sizeof(HpCmdMessageQueue_t) - sizeof(((HpCmdMessageQueue_t*)0)->PayloadData);
        memset(pMessageBuff, 0, MessageSize);

        memcpy(pMessageBuff, pHpCmdMessage, MessageHeader);
        if (pHpCmdMessage->PayloadSize)
            memcpy(pMessageBuff + MessageHeader, pHpCmdMessage->PayloadData, pHpCmdMessage->PayloadSize);

        if (dxQueueSendWithSize( HpCmdMessageQueue_Client, pMessageBuff, MessageSize, 1000 ) != DXOS_SUCCESS)
        {
            result = HP_MANAGER_INTERNAL_ERROR;
        }
        else
        {
            result = HP_MANAGER_SUCCESS;
        }

        dxMemFree(pMessageBuff);
    }

    return result;
}

HPManager_result_t HpManagerSendTo_Server(HpCmdMessageQueue_t *pHpCmdMessage)
{
    HPManager_result_t result = HP_MANAGER_INTERNAL_ERROR;

    if (HpCmdMessageQueue_Server == NULL)
        HpCmdMessageQueue_Server = dxQueueGlobalGet(HP_CMD_SERVER_GLOBAL_MESSAGE_QUEUE_NAME);

    if (HpCmdMessageQueue_Server)
    {
        uint32_t    MessageSize = pHpCmdMessage->PayloadSize + sizeof(HpCmdMessageQueue_t);
        uint8_t*    pMessageBuff = dxMemAlloc("pMessageBuff", 1, MessageSize);
        uint16_t    MessageHeader = sizeof(HpCmdMessageQueue_t) - sizeof(((HpCmdMessageQueue_t*)0)->PayloadData);
        memset(pMessageBuff, 0, MessageSize);

        memcpy(pMessageBuff, pHpCmdMessage, MessageHeader);
        if (pHpCmdMessage->PayloadSize)
            memcpy(pMessageBuff + MessageHeader, pHpCmdMessage->PayloadData, pHpCmdMessage->PayloadSize);

        if (dxQueueSendWithSize( HpCmdMessageQueue_Server, pMessageBuff, MessageSize, 1000 ) != DXOS_SUCCESS)
        {
            result = HP_MANAGER_INTERNAL_ERROR;
        }
        else
        {
            result = HP_MANAGER_SUCCESS;
        }

        dxMemFree(pMessageBuff);
    }

    return result;
}


HPManager_result_t HpManagerCleanEventArgumentObj(void *ReportedEventArgObj)
{
    if (ReportedEventArgObj == NULL)
    {
        return HP_MANAGER_INVALID_PARAMETER;
    }

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) ReportedEventArgObj;
    HyperPeripheralMessageRelease(pHpCmdMessage);
    pHpCmdMessage = NULL;

    return HP_MANAGER_SUCCESS;
}

#endif //#if DEVICE_IS_HYBRID
