//============================================================================
// File: Data_Exchange.h
//
// Author: Chris Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/10/12
//     1) Description: Initial
//
//
//============================================================================

#ifndef _HP_DataExchange_H
#define _HP_DataExchange_H

#include "dxContainer.h"

/*  Container Util API request to HP Server. These APIs will send request to HP server which upon completion
 *  will send the response back to HP client
 */
extern void HP_CMD_DataExchange_CreateFullContainerData(uint32_t MessageID, uint32_t ContMType, uint32_t ContDType, uint16_t ContMNameLen, uint8_t *ContMName, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority);
extern void HP_CMD_DataExchange_InsertContainerDetailData(uint32_t MessageID, uint64_t ContMID, uint32_t ContDType, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority);
extern void HP_CMD_DataExchange_UpdateContainerDetailData(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t ContDType, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority);
extern void HP_CMD_DataExchange_ContainerDataRequest(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t Priority);
extern void HP_CMD_DataExchange_StatusDataUpdate(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t ContMType, uint32_t ContDType, uint16_t SValueLen, uint8_t *SValue, uint32_t Priority);



/*  Container Util API used in HP server to pack the container request from client into a dxContainer format
 *  before sending it to Dkcloud.
 */
dxContainer_t* HP_RequestContainerData_Pack(HpDataExcContainerDataRequest_t* pDataRequest, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont);
dxContainer_t* HP_CreateContainerMData_Pack(HpDataExcCreateFullContainerData_t* pFullContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont);
dxContainer_t* HP_InsertContainerDData_Pack(HpDataExcInsertContainerDetailData_t* pInsertContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont);
dxContainer_t* HP_UpdateContainerDData_Pack(HpDataExcUpdateContainerDetailData_t* pUpdateContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont);
dxContainer_t* HP_UpdateContainerDDataStd_Pack(HpDataExcStatDataUpdate_t* pUpdateContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont);



#endif // _HP_DataExchange_H
