//============================================================================
// File: HpJobBridge.c
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/17
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/11/28
//     1) Description: Move to new FW
//
//============================================================================

//============================================================================
// INCLUDES
//============================================================================
#include "dxSysDebugger.h"
#include "HybridPeripheralManager.h"
#include "HpJobBridge.h"
#include "dk_Job.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Var
//============================================================================

//============================================================================
// Function declearancd
//============================================================================

//============================================================================
// Function definition
//============================================================================

// HP -> FW

dxBOOL HP_CMD_JobBridge_AdvJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    
    uint16_t PayloadSize = sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen;
    HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = dxMemAlloc("HpAdvJobAddReq", 1, sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen);
    if(JobBridgeAdvJobAddReq == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_JOBBRIDGE_ADD_ADV_JOB_REQUEST;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority; 
    
    JobBridgeAdvJobAddReq->AdvJobObjID = AdvJobObjID;
    JobBridgeAdvJobAddReq->DataLen = JobPayloadLen;
    memcpy(((uint8_t*) JobBridgeAdvJobAddReq) + sizeof(HpJobBridgeJobPayload_t), pJobPayload, JobPayloadLen);
    
    pCmdMessage->PayloadSize = PayloadSize;
    pCmdMessage->PayloadData = (uint8_t *) JobBridgeAdvJobAddReq;        
    HpManagerSendTo_Server(pCmdMessage);
    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);
    
    HpManagerCleanEventArgumentObj(pCmdMessage);
    
    return dxTRUE;
}

dxBOOL HP_CMD_JobBridge_SingleGwAdvJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    
    uint16_t PayloadSize = sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen;
    HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = dxMemAlloc("HpSGWJobAddReq", 1, sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen);
    if(JobBridgeAdvJobAddReq == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_JOBBRIDGE_ADD_SINGLE_GW_ADV_JOB_REQUEST;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority; 
    
    JobBridgeAdvJobAddReq->AdvJobObjID = AdvJobObjID;
    JobBridgeAdvJobAddReq->DataLen = JobPayloadLen;
    memcpy(((uint8_t*) JobBridgeAdvJobAddReq) + sizeof(HpJobBridgeJobPayload_t), pJobPayload, JobPayloadLen);
    
    pCmdMessage->PayloadSize = PayloadSize;
    pCmdMessage->PayloadData = (uint8_t *) JobBridgeAdvJobAddReq;        
    HpManagerSendTo_Server(pCmdMessage);
    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);
    
    HpManagerCleanEventArgumentObj(pCmdMessage);
    
    return dxTRUE;
}

dxBOOL HP_CMD_JobBridge_OneTimeJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint16_t AdvJobObjID, uint32_t Priority, uint64_t IdentificationNumber)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    
    uint16_t PayloadSize = sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen;
    HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = dxMemAlloc("HpOnceJobAddReq", 1, sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen);
    if(JobBridgeAdvJobAddReq == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_JOBBRIDGE_ONE_TIME_EXECUTE_JOB_REQUEST;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority; 
    
    JobBridgeAdvJobAddReq->AdvJobObjID = AdvJobObjID;
    JobBridgeAdvJobAddReq->DataLen = JobPayloadLen;
    memcpy(((uint8_t*) JobBridgeAdvJobAddReq) + sizeof(HpJobBridgeJobPayload_t), pJobPayload, JobPayloadLen);
    JobBridgeAdvJobAddReq->IdentificationNumber = IdentificationNumber;
    
    pCmdMessage->PayloadSize = PayloadSize;
    pCmdMessage->PayloadData = (uint8_t *) JobBridgeAdvJobAddReq;
    
    HpManagerSendTo_Server(pCmdMessage);
    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);
    
    HpManagerCleanEventArgumentObj(pCmdMessage);
    
    return dxTRUE;
}

dxBOOL HP_CMD_JobBridge_ScheduleJobBridgeAddReq(uint32_t MessageID, uint8_t *pJobPayload, uint16_t JobPayloadLen, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    
    uint16_t PayloadSize = sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen;
    HpJobBridgeJobPayload_t* JobBridgeJobAddReq = dxMemAlloc("HpJobAddReq", 1, sizeof(HpJobBridgeJobPayload_t) + JobPayloadLen);
    if(JobBridgeJobAddReq == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_JOBBRIDGE_ADD_SCHEDULE_JOB_REQUEST;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority; 
    
    JobBridgeJobAddReq->AdvJobObjID = 0;
    JobBridgeJobAddReq->DataLen = JobPayloadLen;
    memcpy(((uint8_t*) JobBridgeJobAddReq) + sizeof(HpJobBridgeJobPayload_t), pJobPayload, JobPayloadLen);
    
    pCmdMessage->PayloadSize = PayloadSize;
    pCmdMessage->PayloadData = (uint8_t *) JobBridgeJobAddReq;        
    HpManagerSendTo_Server(pCmdMessage);
    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);
    
    HpManagerCleanEventArgumentObj(pCmdMessage);
    
    return dxTRUE;
}


//FW -> HP

dxBOOL HP_CMD_JobBridge_AdvJobBridgeExeRes(uint32_t MessageID, HpJobBridgeExeRes_t *pJobBridgeExeRes, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_JOBBRIDGE_EXE_RES;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority; //Normal

    HpJobBridgeExeRes_t *pJobBridgeExeResBuf = (HpJobBridgeExeRes_t *) dxMemAlloc("HpJobExeRes", 1, sizeof(HpJobBridgeExeRes_t));
    if(pJobBridgeExeResBuf == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }
    
    memcpy(pJobBridgeExeResBuf, pJobBridgeExeRes, sizeof(HpJobBridgeExeRes_t));
    
    pCmdMessage->PayloadSize = sizeof(HpJobBridgeExeRes_t);
    pCmdMessage->PayloadData = (uint8_t*) pJobBridgeExeResBuf;                             

    HpManagerSendTo_Client(pCmdMessage);
//    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    
    return dxTRUE;
}
