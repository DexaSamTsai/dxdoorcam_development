//============================================================================
// File: Data_Exchange.c
//
// Author: Chris Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/10/12
//     1) Description: Initial
//
//============================================================================

//============================================================================
// INCLUDES
//============================================================================
#include "dxSysDebugger.h"
#include "HybridPeripheralManager.h"
#include "Utils.h"
#include "HpData_Exchange.h"
//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================
#define Show_Debug_Message  1
//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Var
//============================================================================
//extern dxQueueHandle_t HpDevCmdMessageQueue_Out;
//============================================================================
// Function declearancd
//============================================================================

//============================================================================
// Function definition
//============================================================================
void HP_CMD_DataExchange_CreateFullContainerData(uint32_t MessageID, uint32_t ContMType, uint32_t ContDType, uint16_t ContMNameLen, uint8_t *ContMName, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority;

    uint16_t HpCreateFullDataSize = sizeof(HpDataExcCreateFullContainerData_t) + ContMNameLen + ContDNameLen + SValueLen + LValueLen;

    HpDataExcCreateFullContainerData_t* pHpCreateFullData = dxMemAlloc("HpCreateFullData", 1, HpCreateFullDataSize);
    pHpCreateFullData->ContMType = ContMType;
    pHpCreateFullData->ContDType = ContDType;
    pHpCreateFullData->ContMNameLen = ContMNameLen;
    pHpCreateFullData->ContDNameLen = ContDNameLen;
    pHpCreateFullData->SValueLen = SValueLen;
    pHpCreateFullData->LValueLen = LValueLen;
    uint8_t* pPayload = pHpCreateFullData->CombinedValue;

    if (pHpCreateFullData->ContMNameLen)
    {
        memcpy(pPayload, ContMName, pHpCreateFullData->ContMNameLen);
        pPayload += pHpCreateFullData->ContMNameLen;
    }

    if (pHpCreateFullData->ContDNameLen)
    {
        memcpy(pPayload, ContDName, pHpCreateFullData->ContDNameLen);
        pPayload += pHpCreateFullData->ContDNameLen;
    }

    if (pHpCreateFullData->SValueLen)
    {
        memcpy(pPayload, SValue, pHpCreateFullData->SValueLen);
        pPayload += pHpCreateFullData->SValueLen;
    }

    if (pHpCreateFullData->LValueLen)
    {
        memcpy(pPayload, LValue, pHpCreateFullData->LValueLen);
        pHpCreateFullData->ContLVCRC = crc16_ccitt_kermit(LValue, pHpCreateFullData->LValueLen);
    }

    pCmdMessage->PayloadSize = HpCreateFullDataSize;
    pCmdMessage->PayloadData = (uint8_t*)pHpCreateFullData;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
}

void HP_CMD_DataExchange_InsertContainerDetailData(uint32_t MessageID, uint64_t ContMID, uint32_t ContDType, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority;

    uint16_t HpInsertDetailDataSize = sizeof(HpDataExcInsertContainerDetailData_t) + ContDNameLen + SValueLen + LValueLen;

    HpDataExcInsertContainerDetailData_t* pHpInsertDetailData = dxMemAlloc("HpInsertDetailData", 1, HpInsertDetailDataSize);

    pHpInsertDetailData->ContMID = ContMID;
    pHpInsertDetailData->ContDType = ContDType;
    pHpInsertDetailData->ContDNameLen = ContDNameLen;
    pHpInsertDetailData->SValueLen = SValueLen;
    pHpInsertDetailData->LValueLen = LValueLen;
    uint8_t* pPayload = pHpInsertDetailData->CombinedValue;

    if (pHpInsertDetailData->ContDNameLen)
    {
        memcpy(pPayload, ContDName, pHpInsertDetailData->ContDNameLen);
        pPayload += pHpInsertDetailData->ContDNameLen;
    }

    if (pHpInsertDetailData->SValueLen)
    {
        memcpy(pPayload, SValue, pHpInsertDetailData->SValueLen);
        pPayload += pHpInsertDetailData->SValueLen;
    }

    if (pHpInsertDetailData->LValueLen)
    {
        memcpy(pPayload, LValue, pHpInsertDetailData->LValueLen);
        pHpInsertDetailData->ContLVCRC = crc16_ccitt_kermit(LValue, pHpInsertDetailData->LValueLen);
    }

    pCmdMessage->PayloadSize = HpInsertDetailDataSize;
    pCmdMessage->PayloadData = (uint8_t*)pHpInsertDetailData;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
}

void HP_CMD_DataExchange_UpdateContainerDetailData(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t ContDType, uint16_t ContDNameLen, uint8_t *ContDName, uint16_t SValueLen, uint8_t *SValue, uint16_t LValueLen, uint8_t *LValue, uint32_t Priority)
{
    //G_SYS_DBG_LOG_V("%s %s SValueLen = %d, LValueLen = %d\r\n", __FUNCTION__, ContDName, SValueLen, LValueLen);

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority;

    uint16_t HpUpdateDetailDataSize = sizeof(HpDataExcUpdateContainerDetailData_t) + ContDNameLen + SValueLen + LValueLen; ;

    HpDataExcUpdateContainerDetailData_t* pHpUpdateDetailData = dxMemAlloc("HpUpdateDetailData", 1, HpUpdateDetailDataSize);
    pHpUpdateDetailData->ContMID = ContMID;
    pHpUpdateDetailData->ContDID = ContDID;
    pHpUpdateDetailData->ContDType = ContDType;
    pHpUpdateDetailData->ContDNameLen = ContDNameLen;
    pHpUpdateDetailData->SValueLen = SValueLen;
    pHpUpdateDetailData->LValueLen = LValueLen;
    uint8_t* pPayload = pHpUpdateDetailData->CombinedValue;

    if (pHpUpdateDetailData->ContDNameLen)
    {
        memcpy(pPayload, ContDName, pHpUpdateDetailData->ContDNameLen);
        pPayload += pHpUpdateDetailData->ContDNameLen;
    }

    if (pHpUpdateDetailData->SValueLen)
    {
        memcpy(pPayload, SValue, pHpUpdateDetailData->SValueLen);
        pPayload += pHpUpdateDetailData->SValueLen;
    }

    if (pHpUpdateDetailData->LValueLen)
    {
        memcpy(pPayload, LValue, pHpUpdateDetailData->LValueLen);
        pHpUpdateDetailData->ContLVCRC = crc16_ccitt_kermit(LValue, pHpUpdateDetailData->LValueLen);
    }

    pCmdMessage->PayloadSize = HpUpdateDetailDataSize;
    pCmdMessage->PayloadData = (uint8_t*)pHpUpdateDetailData;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
}

void HP_CMD_DataExchange_ContainerDataRequest(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority;

    HpDataExcContainerDataRequest_t* pContainerDataRequest = dxMemAlloc("ContDataReq", 1, sizeof(HpDataExcContainerDataRequest_t));
    pContainerDataRequest->ContMID = ContMID;
    pContainerDataRequest->ContDID = ContDID;

    pCmdMessage->PayloadSize = sizeof(HpDataExcContainerDataRequest_t);
    pCmdMessage->PayloadData = (uint8_t*)pContainerDataRequest;
    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
}

extern void HP_CMD_DataExchange_StatusDataUpdate(uint32_t MessageID, uint64_t ContMID, uint64_t ContDID, uint32_t ContMType, uint32_t ContDType, uint16_t SValueLen, uint8_t *SValue, uint32_t Priority)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE;
    pCmdMessage->MessageID = MessageID;
    pCmdMessage->Priority = Priority;

    uint16_t HpStatusUpdateDataSize = sizeof(HpDataExcStatDataUpdate_t) + SValueLen;

    HpDataExcStatDataUpdate_t* pStatusDataUpdate = dxMemAlloc("StatDataUpdate", 1, HpStatusUpdateDataSize);
    pStatusDataUpdate->ContMID = ContMID;
    pStatusDataUpdate->ContDID = ContDID;
    pStatusDataUpdate->ContMType = ContMType;
    pStatusDataUpdate->ContDType = ContDType;
    pStatusDataUpdate->SValueLen = SValueLen;

    if (pStatusDataUpdate->SValueLen)
    {
        memcpy(pStatusDataUpdate->SValue, SValue, pStatusDataUpdate ->SValueLen);
    }

    pCmdMessage->PayloadSize = sizeof(HpDataExcStatDataUpdate_t);
    pCmdMessage->PayloadData = (uint8_t*)pStatusDataUpdate;
    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
}



dxContainer_t* HP_UpdateContainerDDataStd_Pack(HpDataExcStatDataUpdate_t* pUpdateContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont)
{
    dxContainer_t *pContWork = NULL;

    if (pCont == NULL)
    {
        pContWork = dxContainer_Create();
    }
    else
    {
        pContWork = pCont;
    }

    dxContainerM_t *pContM = NULL;
    dxContainerD_t *pContD = NULL;

    if (pContWork)
    {

        uint16_t SValueIndex = 0;
        uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) MacAddr, MacAddrLen);

        pContM = dxContainerM_Create(   pUpdateContainerData->ContMID,
                                        BLEObjectID, pUpdateContainerData->ContMType,
                                        NULL);
        if (pContM)
        {
            VarLenType_t *pSV = NULL;

            if (pUpdateContainerData->SValueLen)
            {
                pSV = dxContainer_VarLenType_Init(&pUpdateContainerData->SValue[SValueIndex], pUpdateContainerData->SValueLen);
            }

            pContM->DirtyFlag = 1;

            dxContainer_Append(pContWork, pContM);

            pContD = dxContainerD_Create(   pUpdateContainerData->ContDID,
                                            pUpdateContainerData->ContMID,
                                            NULL,
                                            pUpdateContainerData->ContDType,
                                            0,
                                            pSV,
                                            NULL,
                                            0);

            if (pContD)
            {
                dxContainerM_Append(pContM, pContD);
            }

            if (pSV)
            {
                dxContainer_VarLenType_Free(pSV);
            }
        }
    }

    return pContWork;
}

dxContainer_t* HP_RequestContainerData_Pack(HpDataExcContainerDataRequest_t* pDataRequest, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont)
{
    dxContainer_t *pContWork = NULL;

    if (pCont == NULL)
    {
        pContWork = dxContainer_Create();
    }
    else
    {
        pContWork = pCont;
    }

    dxContainerM_t *pContM = NULL;
    dxContainerD_t *pContD = NULL;

    if (pContWork)
    {
        uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) MacAddr, MacAddrLen);

        pContM = dxContainerM_Create(pDataRequest->ContMID , BLEObjectID, 0, NULL);
        if (pContM)
        {
            dxContainer_Append(pContWork, pContM);

            pContD = dxContainerD_Create(   pDataRequest->ContDID,
                                            0,
                                            NULL,
                                            0,
                                            0,
                                            NULL,
                                            NULL,
                                            0);

            if (pContD)
            {
                dxContainerM_Append(pContM, pContD);
            }
        }
    }

    return pContWork;
}


dxContainer_t* HP_CreateContainerMData_Pack(HpDataExcCreateFullContainerData_t* pFullContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont)
{
    dxContainer_t *pContWork = NULL;

    if (pCont == NULL)
    {
        pContWork = dxContainer_Create();
    }
    else
    {
        pContWork = pCont;
    }

    dxContainerM_t *pContM = NULL;
    dxContainerD_t *pContD = NULL;

    if (pContWork)
    {
        //Holding of ContMName & ContDName & SValue & LValue ADDRESS
        uint16_t ContMNameIndex = 0;
        uint16_t ContDNameIndex = 0;
        uint16_t SValueIndex = 0;
        uint16_t LValueIndex = 0;
        uint16_t CombineValueIndex = 0;

        if (pFullContainerData->ContMNameLen)
        {
            ContMNameIndex = CombineValueIndex;
            CombineValueIndex += pFullContainerData->ContMNameLen;
        }
        if (pFullContainerData->ContDNameLen)
        {
            ContDNameIndex = CombineValueIndex;
            CombineValueIndex += pFullContainerData->ContDNameLen;
        }
        if (pFullContainerData->SValueLen)
        {
            SValueIndex = CombineValueIndex;
            CombineValueIndex += pFullContainerData->SValueLen;
        }
        if (pFullContainerData->LValueLen)
        {
            LValueIndex = CombineValueIndex;
            CombineValueIndex += pFullContainerData->LValueLen;
        }

        uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) MacAddr, MacAddrLen);

        pContM = dxContainerM_Create(0, BLEObjectID, pFullContainerData->ContMType,
                                    (pFullContainerData->ContMNameLen) ? (&pFullContainerData->CombinedValue[ContMNameIndex]):(uint8_t*)"N/A");
        if (pContM)
        {
            dxContainer_Append(pContWork, pContM);

            VarLenType_t *pLV = NULL;
            VarLenType_t *pSV = NULL;

            if (pFullContainerData->LValueLen)
            {
                pLV = dxContainer_VarLenType_Init(&pFullContainerData->CombinedValue[LValueIndex], pFullContainerData->LValueLen);
            }

            if (pFullContainerData->SValueLen)
            {
                pSV = dxContainer_VarLenType_Init(&pFullContainerData->CombinedValue[SValueIndex], pFullContainerData->SValueLen);
            }

            pContD = dxContainerD_Create(   0,
                                            0,
                                            (pFullContainerData->ContDNameLen) ? (&pFullContainerData->CombinedValue[ContDNameIndex]):(uint8_t*)"N/A",
                                            pFullContainerData->ContDType,
                                            0,
                                            pSV,
                                            pLV,
                                            pFullContainerData->ContLVCRC);

            if (pContD)
            {
                dxContainerM_Append(pContM, pContD);
            }

            if (pLV)
            {
                dxContainer_VarLenType_Free(pLV);
            }

            if (pSV)
            {
                dxContainer_VarLenType_Free(pSV);
            }
        }
    }

    return pContWork;
}


dxContainer_t* HP_InsertContainerDData_Pack(HpDataExcInsertContainerDetailData_t* pInsertContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont)
{

    dxContainer_t *pContWork = NULL;

    if (pCont == NULL)
    {
        pContWork = dxContainer_Create();
    }
    else
    {
        pContWork = pCont;
    }

    dxContainerM_t *pContM = NULL;
    dxContainerD_t *pContD = NULL;

    if (pContWork)
    {

        uint16_t ContDNameIndex = 0;
        uint16_t SValueIndex = 0;
        uint16_t LValueIndex = 0;
        uint16_t CombineValueIndex = 0;

        if (pInsertContainerData->ContDNameLen)
        {
            ContDNameIndex = CombineValueIndex;
            CombineValueIndex += pInsertContainerData->ContDNameLen;
        }
        if (pInsertContainerData->SValueLen)
        {
            SValueIndex = CombineValueIndex;
            CombineValueIndex += pInsertContainerData->SValueLen;
        }
        if (pInsertContainerData->LValueLen)
        {
            LValueIndex = CombineValueIndex;
            CombineValueIndex += pInsertContainerData->LValueLen;
        }

        uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) MacAddr, MacAddrLen);

        pContM = dxContainerM_Create(pInsertContainerData->ContMID, BLEObjectID, 0, NULL);
        if (pContM)
        {
            dxContainer_Append(pContWork, pContM);

            VarLenType_t *pLV = NULL;
            VarLenType_t *pSV = NULL;

            if (pInsertContainerData->LValueLen)
            {
                pLV = dxContainer_VarLenType_Init(&pInsertContainerData->CombinedValue[LValueIndex], pInsertContainerData->LValueLen);
            }

            if (pInsertContainerData->SValueLen)
            {
                pSV = dxContainer_VarLenType_Init(&pInsertContainerData->CombinedValue[SValueIndex], pInsertContainerData->SValueLen);
            }

            pContD = dxContainerD_Create(   0,
                                            0,
                                            (pInsertContainerData->ContDNameLen) ? (&pInsertContainerData->CombinedValue[ContDNameIndex]):(uint8_t*)"N/A",
                                            pInsertContainerData->ContDType,
                                            0,
                                            pSV,
                                            pLV,
                                            pInsertContainerData->ContLVCRC);

            if (pContD)
            {
                dxContainerM_Append(pContM, pContD);
            }

            if (pLV)
            {
                dxContainer_VarLenType_Free(pLV);
            }

            if (pSV)
            {
                dxContainer_VarLenType_Free(pSV);
            }
        }
    }

    return pContWork;
}


dxContainer_t* HP_UpdateContainerDData_Pack(HpDataExcUpdateContainerDetailData_t* pUpdateContainerData, uint8_t* MacAddr, uint8_t MacAddrLen, dxContainer_t* pCont)
{

    dxContainer_t *pContWork = NULL;

    if (pCont == NULL)
    {
        pContWork = dxContainer_Create();
    }
    else
    {
        pContWork = pCont;
    }

    dxContainerM_t *pContM = NULL;
    dxContainerD_t *pContD = NULL;

    if (pContWork)
    {

        uint16_t ContDNameIndex = 0;
        uint16_t SValueIndex = 0;
        uint16_t LValueIndex = 0;
        uint16_t CombineValueIndex = 0;

        if (pUpdateContainerData->ContDNameLen)
        {
            ContDNameIndex = CombineValueIndex;
            CombineValueIndex += pUpdateContainerData->ContDNameLen;
        }
        if (pUpdateContainerData->SValueLen)
        {
            SValueIndex = CombineValueIndex;
            CombineValueIndex += pUpdateContainerData->SValueLen;
        }
        if (pUpdateContainerData->LValueLen)
        {
            LValueIndex = CombineValueIndex;
            CombineValueIndex += pUpdateContainerData->LValueLen;
        }

        uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) MacAddr, MacAddrLen);

        pContM = dxContainerM_Create(pUpdateContainerData->ContMID, BLEObjectID, 0, NULL);
        if (pContM)
        {
            pContM->DirtyFlag = 1;

            dxContainer_Append(pContWork, pContM);

            VarLenType_t *pLV = NULL;
            VarLenType_t *pSV = NULL;

            if (pUpdateContainerData->SValueLen)
            {
                pSV = dxContainer_VarLenType_Init(&pUpdateContainerData->CombinedValue[SValueIndex], pUpdateContainerData->SValueLen);
            }

            if (pUpdateContainerData->LValueLen)
            {
                pLV = dxContainer_VarLenType_Init(&pUpdateContainerData->CombinedValue[LValueIndex], pUpdateContainerData->LValueLen);
            }

            pContD = dxContainerD_Create(   pUpdateContainerData->ContDID,
                                            0,
                                            (pUpdateContainerData->ContDNameLen) ? (&pUpdateContainerData->CombinedValue[ContDNameIndex]):(uint8_t*)"N/A",
                                            pUpdateContainerData->ContDType,
                                            0,
                                            pSV,
                                            pLV,
                                            pUpdateContainerData->ContLVCRC);

            if (pContD)
            {
                dxContainerM_Append(pContM, pContD);
            }

            if (pLV)
            {
                dxContainer_VarLenType_Free(pLV);
            }

            if (pSV)
            {
                dxContainer_VarLenType_Free(pSV);
            }
        }
    }

    return pContWork;
}
