//============================================================================
// File: HpContStore.h
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/28
//     1) Description: Initial
//
// Modified History:
//     Version: 0.2
//     Date: 2017/11/28
//     1) Description: Move to new FW
//
//============================================================================

#ifndef _HP_CONT_STORE_H
#define _HP_CONT_STORE_H

#include "dxOS.h"
#include "HybridPeripheralManager.h"
#include "dxContStore.h"
//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    int32_t         Status;          //The Message response       
} ContStorageRes_t;

typedef struct
{
    dxContStoreAccessStatus_t   EventStatus;
    dxContStoreData_t           *pStoreData; 
    uint32_t                    NumberOfStoreData;
} ContStorageDataRes_t;

typedef struct
{
    dxContStoreAccessStatus_t   EventStatus;
    dxContStoreErrorCode_t      nErrorCode;
} ContStorageWriteCompleteRes_t;

typedef struct
{
    int32_t                     nErrorCode;
} ContStorageErrorRes_t;

typedef struct
{
    uint32_t        ContMType;
    uint32_t        ContDType;
    uint32_t        DataLen;
    uint8_t         pDataPayload[0]; //Holding of Payload ADDRESS
} ContStorageWriteReq_t;

typedef struct
{
    uint32_t        ContMType;
    uint32_t        ContDType;
} ContStorageFindReq_t;

//============================================================================
// Function
//============================================================================


/*
 * FW -> ContStorage APIs
 */
dxContStoreErrorCode_t Main_ContStore_Format(uint16_t ClientId);
dxContStoreErrorCode_t Main_ContStore_Init(uint16_t ClientId);
dxContStoreErrorCode_t Main_ContStore_Uninit(uint16_t ClientId);
dxContStoreErrorCode_t Main_ContStore_ReadAll(uint16_t ClientId);
dxContStoreErrorCode_t Main_ContStore_Write(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType, uint8_t *pPayload, uint32_t PayloadLen);
dxContStoreErrorCode_t Main_ContStore_Find(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType);

/*
 * HP -> FW
 */
dxOS_RET_CODE HP_ContStore_Format(uint16_t ClientId);
dxOS_RET_CODE HP_ContStore_Init(uint16_t ClientId);
dxOS_RET_CODE HP_ContStore_Uninit(uint16_t ClientId);
dxOS_RET_CODE HP_ContStore_ReadAll(uint16_t ClientId);
dxOS_RET_CODE HP_ContStore_Write(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType, uint8_t *pPayload, uint32_t PayloadLen);
dxOS_RET_CODE HP_ContStore_Find(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType);

void HEX_Dump_Data(uint8_t *pData, int len);

#endif // _HP_CONT_STORE_H