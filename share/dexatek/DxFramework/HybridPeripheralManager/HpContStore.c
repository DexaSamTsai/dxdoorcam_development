//============================================================================
// File: HpContStore.c
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/28
//     1) Description: Initial
//
// Modified History:
//     Version: 0.2
//     Date: 2017/11/28
//     1) Description: Move to new FW
//
//============================================================================

//============================================================================
// INCLUDES
//============================================================================
//#include <platform/platform_stdlib.h>
#include "dxOS.h"
#include "dxContStore.h"
#include "dxSysDebugger.h"
#include "dxContStoreQ.h"
//#include "HybridPeripheralMainApp.h"
#include "HybridPeripheralManager.h"
#include "HpContStore.h"

//============================================================================
// Macros
//============================================================================
#define dxDIRECT_DEBUG_PRINT    0
#if dxDIRECT_DEBUG_PRINT
    #define DX_PRINT            printf
#else // dxDIRECT_DEBUG_PRINT
    #define DX_PRINT            G_SYS_DBG_LOG_NV
#endif // dxDIRECT_DEBUG_PRINT

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Constants
//============================================================================

#define HP_CONT_STORE_SECTOR_SIZE   64  //256KB

//============================================================================
// Var
//============================================================================
static dxCSHandle_t hCS;

//============================================================================
// Function declearancd
//============================================================================

//============================================================================
// Function definition
//============================================================================

void HEX_Dump_Data(uint8_t *pData, int len)
{
    char szPrintBuff[64];
    int i = 0;
    memset(szPrintBuff, 0x00, sizeof(szPrintBuff));

    DX_PRINT("len = %d\n", len);
    while (i < len)
    {
        sprintf(szPrintBuff, "%s%02X", szPrintBuff, (pData[i] & 0x0FF));
        i++;
        if (((i % 16) == 0) ||
            (i == len))
        {
            strcat(szPrintBuff, "\n");

//            dxTimeDelayMS(50);

            DX_PRINT(szPrintBuff);
            memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        }
        else if((i % 4) == 0)
        {
            strcat(szPrintBuff, " - ");
        }
        else
        {
            strcat(szPrintBuff, " ");
//            DX_PRINT(szPrintBuff);
//            DX_PRINT("\n");
        }
    }
}

//============================================================================
// dxContStore callback function
//============================================================================
void CONTSTORE_CompletionCallback(dxContStoreAccessStatus_t EventStatus, uint16_t ClientId, dxContStoreData_t *pStoreData, uint32_t NumberOfStoreData, dxContStoreErrorCode_t nErrorCode)
{
    DX_PRINT("[%s] EventStatus: %d, nErrorCode: %d, ClientId: %d, NumberOfStoreData: %d\n", __FUNCTION__, EventStatus, nErrorCode, ClientId, NumberOfStoreData);

    if ((nErrorCode == CONTSTORE_ERROR_WRITE_READ_VERIFY_FAIL) &&
        (nErrorCode == CONTSTORE_ERROR_NOT_ENOUGH_FLASH_SPACE))
    {
        DX_PRINT("[%s] !! Found invalid ContStorage access error code: %d, intend to re-init storage\r\n", __FUNCTION__, nErrorCode);

        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
        if(pCmdMessage == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
            return;
        }
        pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_REINIT_REQ;
        pCmdMessage->MessageID = ClientId;
        pCmdMessage->Priority = 0; //Normal

        ContStorageErrorRes_t* pErrorRes = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageErrorRes_t));
        if(pErrorRes == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pErrorRes\r\n", __FUNCTION__);
            dxMemFree(pCmdMessage);
            return;
        }

        pErrorRes->nErrorCode = nErrorCode;

        pCmdMessage->PayloadSize = sizeof(ContStorageErrorRes_t);
        pCmdMessage->PayloadData = (uint8_t *)pErrorRes;

        HpManagerSendTo_Client(pCmdMessage);

        HpManagerCleanEventArgumentObj(pCmdMessage);

        return;
    }

#if 0
    if (pStoreData)
    {
        for (uint32_t i = 0; i < NumberOfStoreData; i++)
        {
            DX_PRINT("MID:%-8X DID:%-8X SEQ:%-8X LEN:%-4X PYLD:\n",
                     pStoreData[i].Header.MID,
                     pStoreData[i].Header.DID,
                     pStoreData[i].Header.SeqNo,
                     pStoreData[i].Header.PayloadLen);
            HEX_Dump_Data(pStoreData[i].pPayload, pStoreData[i].Header.PayloadLen);
            DX_PRINT("\n");
        }
    }
#endif

    if (pStoreData)
    {
        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
        if(pCmdMessage == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
            return;
        }
        pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_DATA_RES;
        pCmdMessage->MessageID = ClientId;
        pCmdMessage->Priority = 0; //Normal

        ContStorageDataRes_t* pDataRes = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageDataRes_t));
        if(pDataRes == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pDataRes\r\n", __FUNCTION__);
            dxMemFree(pCmdMessage);
            return;
        }

        dxContStoreData_t *pStoreDataBuf = (dxContStoreData_t *) dxMemAlloc("StoreDataBuf", 1, sizeof(dxContStoreData_t) * NumberOfStoreData);
        DX_PRINT("[%s] pStoreDataBuf = 0x%x\r\n", __FUNCTION__, pStoreDataBuf);

        for (uint32_t i = 0; i < NumberOfStoreData; i++)
        {
            memcpy((uint8_t *)(&pStoreDataBuf[i]), (uint8_t *)(&pStoreData[i]), sizeof(dxContStoreData_t));
            pStoreDataBuf[i].pPayload = dxMemAlloc("ContRestoreRaw", 1, pStoreData[i].Header.PayloadLen);
            memcpy(pStoreDataBuf[i].pPayload, pStoreData[i].pPayload, pStoreData[i].Header.PayloadLen);
        }

        pDataRes->EventStatus = EventStatus;
        pDataRes->pStoreData = pStoreDataBuf;
        pDataRes->NumberOfStoreData = NumberOfStoreData;

        pCmdMessage->PayloadSize = sizeof(ContStorageDataRes_t);
        pCmdMessage->PayloadData = (uint8_t *)pDataRes;

        HpManagerSendTo_Client(pCmdMessage);

        HpManagerCleanEventArgumentObj(pCmdMessage);
    }
    else if (EventStatus == CONTSTORE_ACCESS_WRITE)
    {
        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
        if(pCmdMessage == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
            return;
        }
        pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_WRITE_COMPLETE_RES;
        pCmdMessage->MessageID = ClientId;
        pCmdMessage->Priority = 0; //Normal

        ContStorageWriteCompleteRes_t* pWriteCompleteRes = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageWriteCompleteRes_t));
        if(pWriteCompleteRes == NULL)
        {
            DX_PRINT("[%s] !! DXOS_NO_MEM for pWriteCompleteRes\r\n", __FUNCTION__);
            dxMemFree(pCmdMessage);
            return;
        }

        pWriteCompleteRes->EventStatus = EventStatus;
        pWriteCompleteRes->nErrorCode = nErrorCode;

        pCmdMessage->PayloadSize = sizeof(ContStorageWriteCompleteRes_t);
        pCmdMessage->PayloadData = (uint8_t *)pWriteCompleteRes;

        HpManagerSendTo_Client(pCmdMessage);

        HpManagerCleanEventArgumentObj(pCmdMessage);
    }
}

dxContStoreErrorCode_t Main_ContStore_Format(uint16_t ClientId)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    ret = dxContStore_Erase(DX_HP_STORAGE_ADDR1,
                            DEFAULT_FLASH_SECTOR_ERASE_SIZE * HP_CONT_STORE_SECTOR_SIZE);
    DX_PRINT("[%s] dxContStore_Erase(): %d\n", __FUNCTION__, ret);

    ret = dxContStore_Init(CONTSTORE_SECTOR_ONE,
                           DX_HP_STORAGE_ADDR1,
                           DEFAULT_FLASH_SECTOR_ERASE_SIZE * HP_CONT_STORE_SECTOR_SIZE,
                           CONTSTORE_CompletionCallback,
                           &hCS);
    DX_PRINT("[%s] dxContStore_Init(hCS:0x%X): %d\n", __FUNCTION__, hCS, ret);

    ret = dxContStore_FormatHeader(CONTSTORE_SECTOR_ONE, &hCS);
    DX_PRINT("[%s] dxContStore_FormatHeader(hCS:0x%X): %d\n", __FUNCTION__, hCS, ret);

    return ret;
}

dxContStoreErrorCode_t Main_ContStore_Init(uint16_t ClientId)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;
    ret = dxContStore_Init(CONTSTORE_SECTOR_ONE,
                           DX_HP_STORAGE_ADDR1,
                           DEFAULT_FLASH_SECTOR_ERASE_SIZE * HP_CONT_STORE_SECTOR_SIZE,
                           CONTSTORE_CompletionCallback,
                           &hCS);
    DX_PRINT("[%s] dxContStore_Init(hCS:0x%X): %d\n", __FUNCTION__, hCS, ret);
    if (ret == CONTSTORE_ERROR_UNKNOWN_FORMAT)
    {
        ret = dxContStore_FormatHeader(CONTSTORE_SECTOR_ONE, &hCS);
        DX_PRINT("[%s] dxContStore_FormatHeader(hCS:0x%X): %d\n", __FUNCTION__, hCS, ret);
    }

    return ret;
}

dxContStoreErrorCode_t Main_ContStore_Uninit(uint16_t ClientId)
{
    return dxContStore_Uninit(hCS);
}

dxContStoreErrorCode_t Main_ContStore_ReadAll(uint16_t ClientId)
{
    return dxContStore_ReadAll(hCS, 0, 0);
}

dxContStoreErrorCode_t Main_ContStore_Write(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType, uint8_t *pPayload, uint32_t PayloadLen)
{
    return dxContStore_Write(hCS, 0, ContMType, ContDType, pPayload, PayloadLen);
}

dxContStoreErrorCode_t Main_ContStore_Find(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType)
{
    return dxContStore_Find(hCS, 0, ContMType, ContDType);
}

dxOS_RET_CODE HP_ContStore_Format(uint16_t ClientId)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        );

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_FORMAT_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal
    pCmdMessage->PayloadSize = 0;
    pCmdMessage->PayloadData = NULL;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}

dxOS_RET_CODE HP_ContStore_Init(uint16_t ClientId)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        );

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_INIT_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal
    pCmdMessage->PayloadSize = 0;
    pCmdMessage->PayloadData = NULL;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}

dxOS_RET_CODE HP_ContStore_Uninit(uint16_t ClientId)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        );

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_UNINIT_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal
    pCmdMessage->PayloadSize = 0;
    pCmdMessage->PayloadData = NULL;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}

dxOS_RET_CODE HP_ContStore_ReadAll(uint16_t ClientId)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        );

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_READ_ALL_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal
    pCmdMessage->PayloadSize = 0;
    pCmdMessage->PayloadData = NULL;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}

dxOS_RET_CODE HP_ContStore_Write(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType, uint8_t *pPayload, uint32_t PayloadLen)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        , ContMType
        , ContDType
        , PayloadLen
        );

    //HEX_Dump_Data(pPayload, PayloadLen);

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_WRITE_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal

    ContStorageWriteReq_t* pWriteReq = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageWriteReq_t) + PayloadLen);
    if(pWriteReq == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pWriteReq\r\n", __FUNCTION__);
        dxMemFree(pCmdMessage);
        return DXOS_NO_MEM;
    }

    pWriteReq->ContMType = ContMType;
    pWriteReq->ContDType = ContDType;
    pWriteReq->DataLen = PayloadLen;
    memcpy(pWriteReq->pDataPayload, pPayload, PayloadLen);

    pCmdMessage->PayloadSize = sizeof(ContStorageWriteReq_t) + PayloadLen;
    pCmdMessage->PayloadData = (uint8_t *)pWriteReq;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}

dxOS_RET_CODE HP_ContStore_Find(uint16_t ClientId, uint32_t ContMType, uint32_t ContDType)
{
    DX_PRINT("[%s] ClientId = %d, ContMType = 0x%x, ContDType = 0x%x, Datalen = %d\r\n", __FUNCTION__
        , ClientId
        , ContMType
        , ContDType
        );

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pCmdMessage\r\n", __FUNCTION__);
        return DXOS_NO_MEM;
    }
    pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_FIND_REQUEST;
    pCmdMessage->MessageID = ClientId;
    pCmdMessage->Priority = 0; //Normal

    ContStorageFindReq_t* pFindReq = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageFindReq_t));
    if(pFindReq == NULL)
    {
        DX_PRINT("[%s] !! DXOS_NO_MEM for pFindReq\r\n", __FUNCTION__);
        dxMemFree(pCmdMessage);
        return DXOS_NO_MEM;
    }

    pFindReq->ContMType = ContMType;
    pFindReq->ContDType = ContDType;

    pCmdMessage->PayloadSize = sizeof(ContStorageFindReq_t);
    pCmdMessage->PayloadData = (uint8_t *)pFindReq;

    HpManagerSendTo_Server(pCmdMessage);

    HpManagerCleanEventArgumentObj(pCmdMessage);
    return DXOS_SUCCESS;
}
