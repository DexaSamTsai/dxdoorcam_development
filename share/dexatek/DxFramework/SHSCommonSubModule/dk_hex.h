//============================================================================
// File: dk_hex.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#ifndef _DK_HEX_H
#define _DK_HEX_H

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#ifndef __IAR_SYSTEMS_ICC__
#include <ctype.h>              // NOT include in IAR
#endif // __IAR_SYSTEMS_ICC__


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
/**
 * @fn DKHex_HexToASCII
 *
 * @param hex: Source character array to be convert
 * @param hexlen: Number of character in hex array
 * @param ascii: A output buffer to store the data.
 *               The size of ascii buffer which must be larger then (hexlen x 2).
 *               The minimum size is (hexlen x 2 + 1) including a NULL-terminator character.
 * @param asclen: The size of character array ascii
 *
 * @retval Number of character converted.
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < asclen, len characters are stored in ascii buffer, a null-terminator character is appended, and len is returned.
 *         If len = asclen, len characters are stored in ascii buffer, NO null-terminator character is appended, and len is returned.
 *         If len > asclen, no character is stored in ascii buffer, and 0 is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKHex_HexToASCII(char *hex, int hexlen, char *ascii, int asclen);

/**
 * @fn DKHex_ASCToHex
 *
 * @param ascii: Source character array to be convert
 * @param asclen: Number of character in ascii array. The value must be an even number.
 * @param hex: A output buffer to store the data.
 *               The size of hex must larger then (asclen / 2). The minimum size is (asclen / 2 + 1) including a NULL character.
 * @param hexlen: The size of character array ascii
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < hexlen, len characters are stored in hex buffer, a null-terminator character is appended, and len is returned.
 *         If len = hexlen, len characters are stored in hex buffer, NO null-terminator character is appended, and len is returned.
 *         If len > hexlen, no character is stored in hex buffer, and 0 is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKHex_ASCToHex(char *ascii, int asclen, char *hex, int hexlen);

#ifdef __cplusplus
}
#endif

#endif // _DK_HEX_H
