//============================================================================
// File: dk_url.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include <stdio.h>
#include <string.h>
#include "dk_hex.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
static char url_decode_char(char *buffer, int *skip);

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
static char url_decode_char(char *buffer, int *skip)
{
    char ret = 0;
    char found = 0;
    char szOut[3] = {0};

    if (buffer[0] == '%')
    {
    	if (DKHex_ASCToHex(&buffer[1], 2, szOut, sizeof(szOut)) > 0)
    	{
    		ret = szOut[0];
            *skip = 3;
            found = 1;
    	}
    }

    if (found == 0)
    {
    	// Due to newline normalization which replaces spaces with "+" instead of "%20".
    	// We have to change back to space if the character is "+"
    	if (buffer[0] == '+')
    	{
    		ret = 0x20;
    	}
    	else
    	{
    		ret = buffer[0];
    	}

        *skip = 1;
    }

    return ret;
}

static int url_encode_char(char ch, char *dataencoded)
{
	int ret = 0;

	if ((ch >= '0' && ch <= '9') ||
		(ch >= 'a' && ch <= 'z') ||
		(ch >= 'A' && ch <= 'Z') ||
		ch == '-' || ch == '_' || ch == '.')	// || ch == '~')	// Due to linux, "~" is HOME directory, must be encoded in our system.
	{
		ret = 1;
		if (dataencoded)
		{
			dataencoded[0] = ch;
		}
	}
	else
	{
		// 0x20 encode to "%20" instead of "+"
		ret = 3;
		if (dataencoded)
		{
			sprintf(dataencoded, "%%%02X", (unsigned char)ch);
		}
	}

	return ret;
}

/**
 * @fn DKUrl_decode
 *
 * @param buffer	 [in, out]: Source character array to be decode
 * @param buffersize [in]     :  The size of buffer
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < buffersize, len characters are stored in buffer, a null-terminator character is appended, and len is returned.
 *         If len = buffersize, len characters are stored in buffer, NO null-terminator character is appended, and len is returned.
 *         If len > buffersize, buffersize characters are stored in buffer, NO null-terminator character is appended, and buffersize is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKUrl_decode(char *buffer, int buffersize)
{
    char ch = 0;
    int skip = 0;
    int i = 0;
    int j = 0;

    int len = strlen(buffer);

    if (len <= 0) return 0;

    while (i < len)
    {
        ch = url_decode_char( &buffer[i], &skip);

        if (j + 1 > buffersize)
        {
        	break;
        }

        buffer[j++] = ch;
        i += skip;
    }

    if (j < buffersize)
    {
    	buffer[j] = 0x00;
    }
    return j;
}

/**
 * @fn DKUrl_encode
 *
 * @param buffer     [out]: The buffer to store characters after encoding.
 * @param buffersize [in]:  The size of buffer
 * @param data       [in] : The source character array to be encode
 * @param datalen    [in] : The length of data
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < buffersize, len characters are stored in buffer, a null-terminator character is appended, and len is returned.
 *         If len = buffersize, len characters are stored in buffer, NO null-terminator character is appended, and len is returned.
 *         If len > buffersize, buffersize characters are stored in buffer, NO null-terminator character is appended, and buffersize is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKUrl_encode(char *buffer, int buffersize, unsigned char *data, int datalen)
{
	int i = 0;
	int len = 0;
	int totallen = 0;

	if (datalen <= 0) return 0;

	for (i = 0; i < datalen; i++)
	{
		len = url_encode_char(data[i], &buffer[totallen]);
		if (len <= 0)
		{
			break;
		}

		if (buffer && totallen + len > buffersize)
		{
			break;
		}
		totallen += len;
	}

	if (totallen < buffersize)
	{
		buffer[totallen] = 0x00;
	}

	return totallen;
}
