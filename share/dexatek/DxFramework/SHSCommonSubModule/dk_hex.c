//============================================================================
// File: dk_hex.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
static const char szA[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
/**
 * @brief Check the input character. If the character is in '0' ~ '9', 'A' ~ 'F', and 'a' ~ 'f', 
 *        return their hexadecimal value, 0x00 to 0x0F
 *
 * @fn _IsHexDigit
 *
 * @retval Hex value of 0x00 ~ 0x0F
 *         - SUCCESS: 0 to 15
 *         - ERROR: -1
 */
int _IsHexDigit(char ch)
{
	if (isdigit((int)ch))
	{
		return (ch - '0');
	}

	if (ch >= 0x41 && ch <= 0x46)
	{
		return (ch - 0x41) + 10;
	}

	if (ch >= 0x61 && ch <= 0x66)
	{
		return (ch - 0x61) + 10;
	}

	return -1;
}
/**
 * @fn DKHex_HexToASCII
 *
 * @param hex: Source character array to be convert
 * @param hexlen: Number of character in hex array
 * @param ascii: A output buffer to store the data.
 *               The size of ascii buffer which must be larger then (hexlen x 2). 
 *               The minimum size is (hexlen x 2 + 1) including a NULL-terminator character.
 * @param asclen: The size of character array ascii
 *
 * @retval Number of character converted.
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < asclen, len characters are stored in ascii buffer, a null-terminator character is appended, and len is returned.
 *         If len = asclen, len characters are stored in ascii buffer, NO null-terminator character is appended, and len is returned.
 *         If len > asclen, no character is stored in ascii buffer, and 0 is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKHex_HexToASCII(char *hex, int hexlen, char *ascii, int asclen)
{
	int ret = 0;
	int i = 0;
	char ch = 0;

	if (hexlen * 2 > asclen || hex == NULL || ascii == NULL)
	{
		return 0;
	}

    // expand 0x41 to "41"
    for (i = 0; i < hexlen; i++)
    {
    	ch = hex[hexlen - i - 1];
    	ascii[2 * (hexlen - i - 1)] = szA[(ch >> 4) & 0x0F];
    	ascii[2 * (hexlen - i - 1) + 1] = szA[ch & 0x0F];
    	ret += 2;
    }

    if (ret < asclen)
    {
    	ascii[ret] = 0x00;		// NULL terminate
    }

    return ret;
}

/**
 * @fn DKHex_ASCToHex
 *
 * @param ascii: Source character array to be convert
 * @param asclen: Number of character in ascii array. The value must be an even number.
 * @param hex: A output buffer to store the data.
 *               The size of hex must larger then (asclen / 2). The minimum size is (asclen / 2 + 1) including a NULL character.
 * @param hexlen: The size of character array ascii
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < hexlen, len characters are stored in hex buffer, a null-terminator character is appended, and len is returned.
 *         If len = hexlen, len characters are stored in hex buffer, NO null-terminator character is appended, and len is returned.
 *         If len > hexlen, no character is stored in hex buffer, and 0 is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKHex_ASCToHex(char *ascii, int asclen, char *hex, int hexlen)
{
	int ret = 0;
	int i = 0;
	int n = asclen / 2;
	int x = 0;
	int y = 0;

	if (hexlen * 2 < asclen ||
		hex == NULL ||
		ascii == NULL ||
		(asclen % 2) != 0)
	{
		return 0;
	}

    // compress "41" to 0x41
	while (i < n)
    {
    	x = _IsHexDigit(ascii[2 * i]);
    	y = _IsHexDigit(ascii[2 * i + 1]);
    	if (x < 0 || y < 0)
    	{
    		ret = 0;
    		break;
    	}

    	hex[i] = ((x << 4) | y);

    	i++;
    	ret++;
    }

    if (ret > 0 && ret < hexlen)
    {
    	hex[ret] = 0x00;		// NULL terminate
    }

    return ret;
}
