//============================================================================
// File: dk_url.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#ifndef _DK_URL_H
#define _DK_URL_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
/**
 * @fn DKUrl_decode
 *
 * @param buffer	 [in, out]: Source character array to be decode
 * @param buffersize [in]     :  The size of buffer
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < buffersize, len characters are stored in buffer, a null-terminator character is appended, and len is returned.
 *         If len = buffersize, len characters are stored in buffer, NO null-terminator character is appended, and len is returned.
 *         If len > buffersize, buffersize characters are stored in buffer, NO null-terminator character is appended, and buffersize is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKUrl_decode(char *buffer, int buffersize);

/**
 * @fn DKUrl_encode
 *
 * @param buffer     [out]: The buffer to store characters after encoding.
 * @param buffersize [in]:  The size of buffer
 * @param data       [in] : The source character array to be encode
 * @param datalen    [in] : The length of data
 *
 * @retval Number of character converted
 *         Let len be the length of the data converted, not including the terminating null.
 *         If len < buffersize, len characters are stored in buffer, a null-terminator character is appended, and len is returned.
 *         If len = buffersize, len characters are stored in buffer, NO null-terminator character is appended, and len is returned.
 *         If len > buffersize, buffersize characters are stored in buffer, NO null-terminator character is appended, and buffersize is returned.
 *         - SUCCESS: > 0
 *         - ERROR: <= 0
 */
int DKUrl_encode(char *buffer, int buffersize, unsigned char *data, int datalen);

#ifdef __cplusplus
}
#endif

#endif // _DK_URL_H
