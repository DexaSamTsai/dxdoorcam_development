//============================================================================
// File: dk_CharConv.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/05/09
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/05/26
//     1) Description: Added new function to convert string from \uNNNN to UTF-8.
//                     More information, refer to RFC5137, https://tools.ietf.org/pdf/rfc5137.pdf
//        Added:
//            - CHARC_UNICODEtoUTF8Char()
//            - CHARC_UTF8CharLength()
//            - CHARC_EscapedFormtoUTF8String()
//============================================================================
#ifndef _DXCHARCONV_H
#define _DXCHARCONV_H

#pragma once

#include "dxOS.h"

#ifdef __cplusplus
extern "C" {
#endif

//============================================================================
// Defines and Macro
//============================================================================

//============================================================================
// Structure and Enumerate
//============================================================================

//============================================================================
// Functions
//============================================================================

/**
  @code{.c}
    uint8_t u8_ch[5] = {0, 0, 0, 0, 0};
    uint32_t u8_ch_len = sizeof(u8_ch);
    uint32_t u_ch = 0x10437;
    dxBOOL r;
    r = CHARC_UNICODEtoUTF8Char(u8_ch, &u8_ch_len, u_ch);
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_UNICODEtoUTF8Char():%d, %02X %02X %02X %02X\n",
                    __FUNCTION__, __LINE__,
                    r, u8_ch[0], u8_ch[1], u8_ch[2], u8_ch[3]);

    // Output to console
    // [SYSDBG][T17][UN][GI][Application_Main] Line 145, CHARC_UNICODEtoUTF8Char():1, F0 90 90 B7
  @endcode
 */
/**
 * @brief Convert a UNICODE character to UTF-8 characters (multiple bytes)
 * @param pDst[out]         Pointer to a buffer that receives the converted string.
 *                          The buffer should be large enough to store additional NULL-terminated character.
 * @param pcbDstLen[in,out] Size, in bytes, of the buffer indicated by pDst.
 *                          After converting, the value will be the length of pDst.
 *                          Possible value is 1, 2, or 4.
 * @param u_ch              A UNICODE character. Only support U+0000 to U+10FFFF
 * @retval dxTRUE Success
 * @retval dxFALSE Fail
 */
dxBOOL CHARC_UNICODEtoUTF8Char(uint8_t *pDst, uint32_t *pcbDstLen, uint32_t u_ch);

/**
  @code{.c}
    uint8_t szLCDStr[32] = {0};
    uint32_t nLCDStrLen = 0;
    uint8_t *pSrc = "Pr羹fen..";
    uint32_t nSrcLen = strlen(pSrc);

    dxBOOL ret = CHARC_UTF8toLCDString(szLCDStr, &nLCDStrLen, pSrc, nSrcLen);

    DX_PRINT("[%s] Line %d, CHARC_UTF8toLCDString():%d, Length of pSrc:%d, Length of output:%d\r\n",
             __FUNCTION__,
             __LINE__,
             ret,
             nSrcLen,
             nLCDStrLen);
  @endcode
 */
/**
 * @brief Convert UTF-8 character string to VITECH VO1626-KBW-O6 LCD pattern
 * @param pDst[out]         Pointer to a buffer that receives the converted string.
 *                          The buffer should be large enough to store additional NULL-terminated character.
 * @param pcbDstLen[in,out] Size, in bytes, of the buffer indicated by pDst.
 *                          After converting, the value will be the length of pDst.
 * @param pSrc[in]          A NULL-terminated UTF-8 string
 * @param cbSrcLen[in]      Length of pSrc, in bytes
 * @retval dxTRUE Success
 * @retval dxFALSE Fail
 */

dxBOOL CHARC_UTF8toLCDString(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen);

/**
  @code{.c}
    uint8_t u8_ch1[] = {0x41, 0x41, 0x00};
    uint8_t u8_ch2[] = {0xC3, 0x96, 0x41, 0x42, 0x00};
    uint8_t u8_ch3[] = {0xE7, 0x89, 0x87, 0x41, 0x00};
    uint8_t u8_ch4[] = {0xF0, 0x90, 0x90, 0xB7, 0x00};

    uint32_t nBytes = 0;
    uint32_t u_ch = 0;
    nBytes = CHARC_UTF8CharLength(u8_ch1);
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_UTF8CharLength():%d\n",
                    __FUNCTION__, __LINE__,
                    nBytes);
    nBytes = CHARC_UTF8CharLength(u8_ch2);
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_UTF8CharLength():%d\n",
                    __FUNCTION__, __LINE__,
                    nBytes);
    nBytes = CHARC_UTF8CharLength(u8_ch3);
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_UTF8CharLength():%d\n",
                    __FUNCTION__, __LINE__,
                    nBytes);
    nBytes = CHARC_UTF8CharLength(u8_ch4);
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_UTF8CharLength():%d\n",
                    __FUNCTION__, __LINE__,
                    nBytes);

    // Output to console
    // [SYSDBG][T17][UN][GI][Application_Main] Line 161, CHARC_UTF8CharLength():1
    // [SYSDBG][T17][UN][GI][Application_Main] Line 165, CHARC_UTF8CharLength():2
    // [SYSDBG][T17][UN][GI][Application_Main] Line 169, CHARC_UTF8CharLength():3
    // [SYSDBG][T18][UN][GI][Application_Main] Line 173, CHARC_UTF8CharLength():4
  @endcode
 */
/**
 * @brief Calculate UTF-8 character length
 * @param pUTF8Char[in]     Point to a UTF-8 character
 * @retval > 0 Length of UTF-8 character. Possible value is 1, 2 or 4
 * @retval = 0 Fail
 */
uint32_t CHARC_UTF8CharLength(uint8_t *pUTF8Char);

/**
  @code{.c}
    uint8_t *pSrc = "\\u00f6A\\u7247a\\Ud801\\uDC37b";
    uint32_t nLen = 0;
    dxBOOL r = CHARC_EscapedFormtoUTF8String(NULL, &nLen, pSrc, strlen(pSrc));
    G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_EscapedFormtoUTF8String():%d, nLen:%d\n",
                    __FUNCTION__, __LINE__,
                    r, nLen);

    if (r == dxTRUE)
    {
        uint8_t *p = dxMemAlloc("", 1, nLen + 1);
        nLen ++;

        r = CHARC_EscapedFormtoUTF8String(p, &nLen, pSrc, strlen(pSrc));

        G_SYS_DBG_LOG_V("[%s] Line %d, CHARC_EscapedFormtoUTF8String():%d, nLen:%d\n",
                        __FUNCTION__, __LINE__,
                        r, nLen);

        DK_PRINT_HEX(p, nLen);

        if (p)
        {
            dxMemFree(p);
            p = NULL;
        }
    }

    // Output to console
    // [SYSDBG][T18][UN][GI][Application_Main] Line 184, CHARC_EscapedFormtoUTF8String():1, nLen:12
    // [SYSDBG][T19][UN][GI][Application_Main] Line 195, CHARC_EscapedFormtoUTF8String():1, nLen:12
    // C3 B6 41 E7 89 87 61 F0 90 90 B7 62

  @endcode
 */
/**
 * @brief Convert Backslash-U form character string to UTF-8 form
 * @param pDst[out]         Pointer to a buffer that receives the converted string.
 *                          The buffer should be large enough to store additional NULL-terminated character.
 * @param pcbDstLen[in,out] Size, in bytes, of the buffer indicated by pDst.
 *                          After converting, the value will be the length of pDst.
 * @param pSrc[in]          A NULL-terminated Backslash-U form string
 * @param cbSrcLen[in]      Length of pSrc, in bytes
 * @retval dxTRUE Success
 * @retval dxFALSE Fail
 */
dxBOOL CHARC_EscapedFormtoUTF8String(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen);

#ifdef __cplusplus
}
#endif

#endif // _DXCHARCONV_H
