//============================================================================
// File: dk_peripheralTaiSEIA.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

//============================================================================
// Includes
//============================================================================

#include "dk_PeripheralTaiSEIA_Dehumidifier.h"
#include "Utils.h"



//============================================================================
// Implementations
//============================================================================

TaiSEIADehumidifierSupportedPowerControl_t TaiSEIADehumidifier_GetSupportedPowerControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedPowerControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedPowerControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedModeControl_t TaiSEIADehumidifier_GetSupportedModeControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedModeControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedModeControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.AutoDehumidify       = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.ProgramedDehumidify  = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.ContinuousDehumidify = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.ClothesDry           = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.AirPurify            = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.MoldPrevent          = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.AirSupply            = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.HumanComfort         = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.LowHumidity          = ((serviceInfo.DataHighByte >> 0) & 1);
    control.EconomizeComfortable = ((serviceInfo.DataHighByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedOperationTimeSetting_t TaiSEIADehumidifier_GetSupportedOperationTimeSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedOperationTimeSetting_t setting;

    memset (&setting, 0, sizeof (TaiSEIADehumidifierSupportedOperationTimeSetting_t));
    setting.Writable = serviceInfo.ServiceIsWritable;
    setting.Minimum = (uint8_t) serviceInfo.DataHighByte;
    setting.Maximum = (uint8_t) serviceInfo.DataLowByte;

    return setting;
}


TaiSEIADehumidifierSupportedRelativeHumiditySetting_t TaiSEIADehumidifier_GetSupportedRelativeHumiditySetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedRelativeHumiditySetting_t setting;

    memset (&setting, 0, sizeof (TaiSEIADehumidifierSupportedRelativeHumiditySetting_t));
    setting.Writable = serviceInfo.ServiceIsWritable;
    setting.Minimum = (uint8_t) serviceInfo.DataHighByte;
    setting.Maximum = (uint8_t) serviceInfo.DataLowByte;

    return setting;
}


TaiSEIADehumidifierSupportedHumidifierLevelControl_t TaiSEIADehumidifier_GetSupportedHumidifierLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedHumidifierLevelControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedHumidifierLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0   = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return control;
}


TaiSEIADehumidifierSupportedClothesDryerLevelControl_t TaiSEIADehumidifier_GetSupportedClothesDryerLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedClothesDryerLevelControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedClothesDryerLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0   = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return control;
}


TaiSEIADehumidifierSupportedIndoorTemperature_t TaiSEIADehumidifier_GetSupportedIndoorTemperature (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedIndoorTemperature_t temperature;

    memset (&temperature, 0, sizeof (TaiSEIADehumidifierSupportedIndoorTemperature_t));
    temperature.Writable = serviceInfo.ServiceIsWritable;
    temperature.Minimum = (int8_t) serviceInfo.DataHighByte;
    temperature.Maximum = (int8_t) serviceInfo.DataLowByte;

    return temperature;
}


TaiSEIADehumidifierSupportedIndoorHumidity_t TaiSEIADehumidifier_GetSupportedIndoorHumidity (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedIndoorHumidity_t humidity;

    memset (&humidity, 0, sizeof (TaiSEIADehumidifierSupportedIndoorHumidity_t));
    humidity.Writable = serviceInfo.ServiceIsWritable;
    humidity.Minimum = (uint8_t) serviceInfo.DataHighByte;
    humidity.Maximum = (uint8_t) serviceInfo.DataLowByte;

    return humidity;
}


TaiSEIADehumidifierSupportedWindSwingableControl_t TaiSEIADehumidifier_GetSupportedWindSwingableControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedWindSwingableControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedWindSwingableControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedWindDirectionLevelControl_t TaiSEIADehumidifier_GetSupportedWindDirectionLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedWindDirectionLevelControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedWindDirectionLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0   = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return control;
}


TaiSEIADehumidifierSupportedWaterFullWarning_t TaiSEIADehumidifier_GetSupportedWaterFullWarning (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedWaterFullWarning_t warning;

    memset (&warning, 0, sizeof (TaiSEIADehumidifierSupportedWaterFullWarning_t));
    warning.Writable = serviceInfo.ServiceIsWritable;
    warning.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    warning.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return warning;
}


TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t TaiSEIADehumidifier_GetSupportedCleanFilterNotify (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedAtmosphereLightControl_t TaiSEIADehumidifier_GetSupportedAtmosphereLightControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedAtmosphereLightControl_t atmosphereLightControl;

    memset (&atmosphereLightControl, 0, sizeof (TaiSEIADehumidifierSupportedAtmosphereLightControl_t));
    atmosphereLightControl.Writable = serviceInfo.ServiceIsWritable;
    atmosphereLightControl.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    atmosphereLightControl.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return atmosphereLightControl;
}


TaiSEIADehumidifierSupportedAirPurifyLevelControl_t TaiSEIADehumidifier_GetSupportedAirPurifyLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedAirPurifyLevelControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedAirPurifyLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Disable_b0  = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return control;
}


TaiSEIADehumidifierSupportedWindSpeedLevelControl_t TaiSEIADehumidifier_GetSupportedWindSpeedLevelControl (
    TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedWindSpeedLevelControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedWindSpeedLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Auto_b0     = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return control;
}


TaiSEIADehumidifierSupportedSideVent_t TaiSEIADehumidifier_GetSupportedSideVent (
    TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedSideVent_t sideVent;

    memset (&sideVent, 0, sizeof (TaiSEIADehumidifierSupportedSideVent_t));
    sideVent.Writable = serviceInfo.ServiceIsWritable;
    sideVent.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    sideVent.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return sideVent;
}


TaiSEIADehumidifierSupportedSoundControl_t TaiSEIADehumidifier_GetSupportedSoundControl (
    TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedSoundControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedSoundControl_t));
    control.Writable           = serviceInfo.ServiceIsWritable;
    control.Silent             = ((serviceInfo.DataLowByte >> 0) & 1);
    control.Button             = ((serviceInfo.DataLowByte >> 1) & 1);
    control.ButtonAndWaterFull = ((serviceInfo.DataLowByte >> 2) & 1);

    return control;
}


TaiSEIADehumidifierSupportedDefrost_t TaiSEIADehumidifier_GetSupportedDefrost (
    TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedDefrost_t defrost;

    memset (&defrost, 0, sizeof (TaiSEIADehumidifierSupportedDefrost_t));
    defrost.Writable = serviceInfo.ServiceIsWritable;
    defrost.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    defrost.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return defrost;
}


TaiSEIADehumidifierSupportedErrorCode_t TaiSEIADehumidifier_GetSupportedErrorCode (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedErrorCode_t errorCode;

    memset (&errorCode, 0, sizeof (TaiSEIADehumidifierSupportedErrorCode_t));
    errorCode.Writable = serviceInfo.ServiceIsWritable;
    errorCode.BitInfo.bits.General_b0  = ((serviceInfo.DataLowByte  >> 0) & 1);
    errorCode.BitInfo.bits.Error1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    errorCode.BitInfo.bits.Error2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    errorCode.BitInfo.bits.Error3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    errorCode.BitInfo.bits.Error4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    errorCode.BitInfo.bits.Error5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    errorCode.BitInfo.bits.Error6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    errorCode.BitInfo.bits.Error7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    errorCode.BitInfo.bits.Error8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    errorCode.BitInfo.bits.Error9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    errorCode.BitInfo.bits.Error10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    errorCode.BitInfo.bits.Error11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    errorCode.BitInfo.bits.Error12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    errorCode.BitInfo.bits.Error13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    errorCode.BitInfo.bits.Error14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    errorCode.BitInfo.bits.Error15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);

    return errorCode;
}


TaiSEIADehumidifierSupportedMoldPreventControl_t TaiSEIADehumidifier_GetSupportedMoldPreventControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedMoldPreventControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedMoldPreventControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t TaiSEIADehumidifier_GetSupportedHighHumidifyNofityControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedHighHumidityValueSetting_t TaiSEIADehumidifier_GetSupportedHighHumidifyValueSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedHighHumidityValueSetting_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedHighHumidityValueSetting_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Minimum = (uint8_t) serviceInfo.DataHighByte;
    control.Maximum = (uint8_t) serviceInfo.DataLowByte;

    return control;
}


TaiSEIADehumidifierSupportedKeyLockControl_t TaiSEIADehumidifier_GetSupportedKeyLockControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedKeyLockControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedKeyLockControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Off = ((serviceInfo.DataLowByte >> 0) & 1);
    control.On  = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedControllerProhibitedControl_t TaiSEIADehumidifier_GetSupportedControllerProhibitedControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedControllerProhibitedControl_t control;
    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedControllerProhibitedControl_t));

    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.PowerControl_b0         = ((serviceInfo.DataLowByte >> 0) & 1);
    control.BitInfo.bits.OperationControl_b1     = ((serviceInfo.DataLowByte >> 1) & 1);
    control.BitInfo.bits.HumiditySetting_b2      = ((serviceInfo.DataLowByte >> 2) & 1);
    control.BitInfo.bits.WindSpeedSetting_b3     = ((serviceInfo.DataLowByte >> 3) & 1);
    control.BitInfo.bits.WindDirectionSetting_b4 = ((serviceInfo.DataLowByte >> 4) & 1);
    control.BitInfo.bits.StopButtonEnable_b5     = ((serviceInfo.DataLowByte >> 5) & 1);

    return control;
}

TaiSEIADehumidifierSupportedSaaVoiceControl_t TaiSEIADehumidifier_GetSupportedSaaVoiceControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedSaaVoiceControl_t control;

    memset (&control, 0, sizeof (TaiSEIADehumidifierSupportedSaaVoiceControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.Enable  = ((serviceInfo.DataLowByte >> 0) & 1);
    control.Disable = ((serviceInfo.DataLowByte >> 1) & 1);

    return control;
}


TaiSEIADehumidifierSupportedOperatingCurrent_t TaiSEIADehumidifier_GetSupportedOperatingCurrent (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedOperatingCurrent_t current;

    memset (&current, 0, sizeof (TaiSEIADehumidifierSupportedOperatingCurrent_t));
    current.Writable = serviceInfo.ServiceIsWritable;
    current.Maximum = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return current;
}


TaiSEIADehumidifierSupportedOperatingVoltage_t TaiSEIADehumidifier_GetSupportedOperatingVoltage (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedOperatingVoltage_t voltage;

    memset (&voltage, 0, sizeof (TaiSEIADehumidifierSupportedOperatingVoltage_t));
    voltage.Writable = serviceInfo.ServiceIsWritable;
    voltage.Maximum = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return voltage;
}


TaiSEIADehumidifierSupportedOperatingPowerFactor_t TaiSEIADehumidifier_GetSupportedOperatingPowerFactor (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedOperatingPowerFactor_t powerFactor;

    memset (&powerFactor, 0, sizeof (TaiSEIADehumidifierSupportedOperatingPowerFactor_t));
    powerFactor.Writable = serviceInfo.ServiceIsWritable;
    powerFactor.Maximum = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return powerFactor;
}


TaiSEIADehumidifierSupportedInstantaneousPower_t TaiSEIADehumidifier_GetSupportedInstantaneousPower (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedInstantaneousPower_t power;

    memset (&power, 0, sizeof (TaiSEIADehumidifierSupportedInstantaneousPower_t));
    power.Writable = serviceInfo.ServiceIsWritable;
    power.Maximum = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return power;
}


TaiSEIADehumidifierSupportedCumulativePowerSetting_t TaiSEIADehumidifier_GetSupportedCumulativePowerSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedCumulativePowerSetting_t setting;

    memset (&setting, 0, sizeof (TaiSEIADehumidifierSupportedCumulativePowerSetting_t));
    setting.Writable = serviceInfo.ServiceIsWritable;
    setting.Minimum = serviceInfo.DataHighByte;

    return setting;
}


eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_GetCurrentPowerControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierPowerControl control = TAISEIA_DEHUMIDIFIER_POWER_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0 : control =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_OFF; break;
        case 1 : control =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierModeControl TaiSEIADehumidifier_GetCurrentModeControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierModeControl control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AUTO_DEHUMIDIFY;       break;
        case 1 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_PROGRAMED_DEHUMIDIFY;  break;
        case 2 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CONTINUOUS_DEHUMIDIFY; break;
        case 3 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CLOTHES_DRY;           break;
        case 4 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_PURIFY;            break;
        case 5 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_MOLD_PREVENT;          break;
        case 6 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_SUPPLY;            break;
        case 7 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_HUMAN_CONFORT;         break;
        case 8 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_LOW_HUMIDITY;          break;
        case 9 : control = TAISEIA_DEHUMIDIFIER_MODE_CONTROL_ECONOMIZE_COMFORTABLE; break;
        default : break;
    }

    return control;
}


uint8_t TaiSEIADehumidifier_GetCurrentOperationTimeSetting (
    TaiSEIAServiceInfo_t serviceInfo
) {
    return (uint8_t) serviceInfo.DataLowByte;
}


uint8_t TaiSEIADehumidifier_GetCurrentRelativeHumiditySetting (
    TaiSEIAServiceInfo_t serviceInfo
) {
    return (uint8_t) serviceInfo.DataLowByte;
}


eTAISEIADehumidifierDehumidifyLevelControl TaiSEIADehumidifier_GetCurrentDehumidifyLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierDehumidifyLevelControl control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_0;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_1;  break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_2;  break;
        case 3  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_3;  break;
        case 4  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_4;  break;
        case 5  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_5;  break;
        case 6  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_6;  break;
        case 7  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_7;  break;
        case 8  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_8;  break;
        case 9  : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_9;  break;
        case 10 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_10; break;
        case 11 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_11; break;
        case 12 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_12; break;
        case 13 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_13; break;
        case 14 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_14; break;
        case 15 : control = TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_15; break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierClothesDryLevelControl TaiSEIADehumidifier_GetCurrentClothesDryLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierClothesDryLevelControl control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_0;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_1;  break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_2;  break;
        case 3  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_3;  break;
        case 4  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_4;  break;
        case 5  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_5;  break;
        case 6  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_6;  break;
        case 7  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_7;  break;
        case 8  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_8;  break;
        case 9  : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_9;  break;
        case 10 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_10; break;
        case 11 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_11; break;
        case 12 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_12; break;
        case 13 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_13; break;
        case 14 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_14; break;
        case 15 : control = TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_15; break;
        default : break;
    }

    return control;
}


int8_t TaiSEIADehumidifier_GetCurrentIndoorTemperature (
    TaiSEIAServiceInfo_t serviceInfo
) {
    return (int8_t) serviceInfo.DataLowByte;
}


uint8_t TaiSEIADehumidifier_GetCurrentIndoorHumidity (
    TaiSEIAServiceInfo_t serviceInfo
) {
    return (uint8_t) serviceInfo.DataLowByte;
}


eTAISEIADehumidifierWindSwingableControl TaiSEIADehumidifier_GetCurrentWindSwingableControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierWindSwingableControl control = TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control =  TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_OFF; break;
        case 1  : control =  TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierWindDirectionLevelControl TaiSEIADehumidifier_GetCurrentWindDirectionLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierWindDirectionLevelControl control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_0;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_1;  break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_2;  break;
        case 3  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_3;  break;
        case 4  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_4;  break;
        case 5  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_5;  break;
        case 6  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_6;  break;
        case 7  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_7;  break;
        case 8  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_8;  break;
        case 9  : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_9;  break;
        case 10 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_10; break;
        case 11 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_11; break;
        case 12 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_12; break;
        case 13 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_13; break;
        case 14 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_14; break;
        case 15 : control = TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_15; break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierWaterFullWarning TaiSEIADehumidifier_GetCurrentWaterFullWarning (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierWaterFullWarning warning = TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : warning =  TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_OFF; break;
        case 1  : warning =  TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_ON;  break;
        default : break;
    }

    return warning;
}


eTAISEIADehumidifierCleanFilterNotifyControl TaiSEIADehumidifier_GetCurrentCleanFilterNotifyControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierCleanFilterNotifyControl control = TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control =  TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_OFF; break;
        case 1  : control =  TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierAtmosphereLightControl TaiSEIADehumidifier_GetCurrentAtmosphereLightControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierAtmosphereLightControl control = TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control =  TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_OFF; break;
        case 1  : control =  TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierAirPurifyLevelControl TaiSEIADehumidifier_GetCurrentAirPurifyLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierAirPurifyLevelControl control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_0;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_1;  break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_2;  break;
        case 3  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_3;  break;
        case 4  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_4;  break;
        case 5  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_5;  break;
        case 6  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_6;  break;
        case 7  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_7;  break;
        case 8  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_8;  break;
        case 9  : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_9;  break;
        case 10 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_10; break;
        case 11 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_11; break;
        case 12 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_12; break;
        case 13 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_13; break;
        case 14 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_14; break;
        case 15 : control = TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_15; break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierWindSpeedControl TaiSEIADehumidifier_GetCurrentWindSpeedControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierWindSpeedControl control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_AUTO;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_1;  break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_2;  break;
        case 3  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_3;  break;
        case 4  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_4;  break;
        case 5  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_5;  break;
        case 6  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_6;  break;
        case 7  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_7;  break;
        case 8  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_8;  break;
        case 9  : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_9;  break;
        case 10 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_10; break;
        case 11 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_11; break;
        case 12 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_12; break;
        case 13 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_13; break;
        case 14 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_14; break;
        case 15 : control = TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_15; break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierSideVent TaiSEIADehumidifier_GetCurrentSideVent (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierSideVent sideVent = TAISEIA_DEHUMIDIFIER_SIDE_VENT_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : sideVent = TAISEIA_DEHUMIDIFIER_SIDE_VENT_OFF; break;
        case 1  : sideVent = TAISEIA_DEHUMIDIFIER_SIDE_VENT_ON;  break;
        default : break;
    }

    return sideVent;
}


eTAISEIADehumidifierSoundControl TaiSEIADehumidifier_GetCurrentSoundControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierSoundControl control = TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_SILENT;               break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON;               break;
        case 2  : control = TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON_AND_WATERFULL; break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierDefrost TaiSEIADehumidifier_GetCurrentDefrost (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierDefrost defrost = TAISEIA_DEHUMIDIFIER_DEFROST_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : defrost = TAISEIA_DEHUMIDIFIER_DEFROST_OFF; break;
        case 1  : defrost = TAISEIA_DEHUMIDIFIER_DEFROST_ON;  break;
        default : break;
    }

    return defrost;
}


TaiSEIADehumidifierSupportedErrorCodeBitInfo_u TaiSEIADehumidifier_GetCurrentErrorCode (
    TaiSEIAServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedErrorCodeBitInfo_u bitInfo;

    bitInfo.flag = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return bitInfo;
}


eTAISEIADehumidifierMoldPreventControl TaiSEIADehumidifier_GetCurrentMoldPreventControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierMoldPreventControl control = TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_OFF; break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


eTAISEIADehumidifierHighHumidityNotifyControl TaiSEIADehumidifier_GetCurrentHighHumidityNotifyControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierHighHumidityNotifyControl control = TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_OFF; break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


uint8_t TaiSEIADehumidifier_GetCurrentHighHumidityValueSetting (
    TaiSEIAServiceInfo_t serviceInfo
) {
    return (uint8_t) serviceInfo.DataLowByte;
}


eTAISEIADehumidifierKeyLockControl TaiSEIADehumidifier_GetCurrentKeyLockControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierKeyLockControl control = TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_OFF; break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_ON;  break;
        default : break;
    }

    return control;
}


TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u TaiSEIADehumidifier_GetCurrentControllerProhibitedControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u bitInfo;

    bitInfo.flag = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return bitInfo;
}


eTAISEIADehumidifierSaaVoiceControl TaiSEIADehumidifier_GetCurrentSaaVoiceControl (
    TaiSEIAServiceInfo_t serviceInfo
) {
    eTAISEIADehumidifierSaaVoiceControl control = TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_UNKNOWN;

    switch (serviceInfo.DataLowByte) {
        case 0  : control = TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_ENABLE;  break;
        case 1  : control = TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_DISABLE; break;
        default : break;
    }

    return control;
}


uint16_t TaiSEIADehumidifier_GetCurrentOperatingCurrent (
    TaiSEIAServiceInfo_t serviceInfo
) {
    uint16_t current = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return current;
}


uint16_t TaiSEIADehumidifier_GetCurrentOperatingVoltage (
    TaiSEIAServiceInfo_t serviceInfo
) {
    uint16_t voltage = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return voltage;
}


uint16_t TaiSEIADehumidifier_GetCurrentOperatingPowerFactor (
    TaiSEIAServiceInfo_t serviceInfo
) {
    uint16_t powerFactor = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return powerFactor;
}


uint16_t TaiSEIADehumidifier_GetCurrentInstantaneousPower (
    TaiSEIAServiceInfo_t serviceInfo
) {
    uint16_t power = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return power;
}


float TaiSEIADehumidifier_GetCurrentCumulativePower (
    TaiSEIAServiceInfo_t serviceInfo
) {
    uint16_t power = (((uint16_t) serviceInfo.DataHighByte) << 8) + ((uint16_t) serviceInfo.DataLowByte);

    return ((float) power / 10.0f);
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertPowerControlPacketPayload (
    eTAISEIADehumidifierPowerControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_POWER_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_POWER_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertModeControlPacketPayload (
    eTAISEIADehumidifierModeControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;
    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));

    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AUTO_DEHUMIDIFY       : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_PROGRAMED_DEHUMIDIFY  : serviceAccess.DataLowByte = 1; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CONTINUOUS_DEHUMIDIFY : serviceAccess.DataLowByte = 2; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CLOTHES_DRY           : serviceAccess.DataLowByte = 3; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_PURIFY            : serviceAccess.DataLowByte = 4; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_MOLD_PREVENT          : serviceAccess.DataLowByte = 5; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_SUPPLY            : serviceAccess.DataLowByte = 6; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_HUMAN_CONFORT         : serviceAccess.DataLowByte = 7; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_LOW_HUMIDITY          : serviceAccess.DataLowByte = 8; break;
        case TAISEIA_DEHUMIDIFIER_MODE_CONTROL_ECONOMIZE_COMFORTABLE : serviceAccess.DataLowByte = 9; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertOperationTimeSettingPacketPayload (
    uint8_t value
) {
    TaiSEIAServiceAccess_t serviceAccess;
    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));

    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING;
    serviceAccess.DataHighByte = 0x00;
    serviceAccess.DataLowByte = value;

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertRelativeHumiditySettingPacketPayload (
    uint8_t value
) {
    TaiSEIAServiceAccess_t serviceAccess;
    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));

    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING;
    serviceAccess.DataHighByte = 0x00;
    serviceAccess.DataLowByte = value;

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHumidityLevelControlPacketPayload (
    eTAISEIADehumidifierDehumidifyLevelControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;
    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));

    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_0  : serviceAccess.DataLowByte =  0; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_1  : serviceAccess.DataLowByte =  1; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_2  : serviceAccess.DataLowByte =  2; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_3  : serviceAccess.DataLowByte =  3; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_4  : serviceAccess.DataLowByte =  4; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_5  : serviceAccess.DataLowByte =  5; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_6  : serviceAccess.DataLowByte =  6; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_7  : serviceAccess.DataLowByte =  7; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_8  : serviceAccess.DataLowByte =  8; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_9  : serviceAccess.DataLowByte =  9; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_10 : serviceAccess.DataLowByte = 10; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_11 : serviceAccess.DataLowByte = 11; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_12 : serviceAccess.DataLowByte = 12; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_13 : serviceAccess.DataLowByte = 13; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_14 : serviceAccess.DataLowByte = 14; break;
        case TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_15 : serviceAccess.DataLowByte = 15; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertClothesDryLevelControlPacketPayload (
    eTAISEIADehumidifierClothesDryLevelControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;
    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));

    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_0  : serviceAccess.DataLowByte =  0; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_1  : serviceAccess.DataLowByte =  1; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_2  : serviceAccess.DataLowByte =  2; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_3  : serviceAccess.DataLowByte =  3; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_4  : serviceAccess.DataLowByte =  4; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_5  : serviceAccess.DataLowByte =  5; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_6  : serviceAccess.DataLowByte =  6; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_7  : serviceAccess.DataLowByte =  7; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_8  : serviceAccess.DataLowByte =  8; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_9  : serviceAccess.DataLowByte =  9; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_10 : serviceAccess.DataLowByte = 10; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_11 : serviceAccess.DataLowByte = 11; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_12 : serviceAccess.DataLowByte = 12; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_13 : serviceAccess.DataLowByte = 13; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_14 : serviceAccess.DataLowByte = 14; break;
        case TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_15 : serviceAccess.DataLowByte = 15; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindSwingableControlPacketPayload (
    eTAISEIADehumidifierWindSwingableControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindDirectionLevelControlPacketPayload (
    eTAISEIADehumidifierWindDirectionLevelControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_0  : serviceAccess.DataLowByte = 0;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_1  : serviceAccess.DataLowByte = 1;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_2  : serviceAccess.DataLowByte = 2;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_3  : serviceAccess.DataLowByte = 3;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_4  : serviceAccess.DataLowByte = 4;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_5  : serviceAccess.DataLowByte = 5;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_6  : serviceAccess.DataLowByte = 6;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_7  : serviceAccess.DataLowByte = 7;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_8  : serviceAccess.DataLowByte = 8;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_9  : serviceAccess.DataLowByte = 9;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_10 : serviceAccess.DataLowByte = 10; break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_11 : serviceAccess.DataLowByte = 11; break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_12 : serviceAccess.DataLowByte = 12; break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_13 : serviceAccess.DataLowByte = 13; break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_14 : serviceAccess.DataLowByte = 14; break;
        case TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_15 : serviceAccess.DataLowByte = 15; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertCleanFilterNotifyControlPacketPayload (
    eTAISEIADehumidifierCleanFilterNotifyControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertAtmosphereLightControlPacketPayload (
    eTAISEIADehumidifierAtmosphereLightControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertAirPurifyLevelControlPacketPayload (
    eTAISEIADehumidifierAirPurifyLevelControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_0  : serviceAccess.DataLowByte = 0;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_1  : serviceAccess.DataLowByte = 1;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_2  : serviceAccess.DataLowByte = 2;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_3  : serviceAccess.DataLowByte = 3;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_4  : serviceAccess.DataLowByte = 4;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_5  : serviceAccess.DataLowByte = 5;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_6  : serviceAccess.DataLowByte = 6;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_7  : serviceAccess.DataLowByte = 7;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_8  : serviceAccess.DataLowByte = 8;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_9  : serviceAccess.DataLowByte = 9;  break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_10 : serviceAccess.DataLowByte = 10; break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_11 : serviceAccess.DataLowByte = 11; break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_12 : serviceAccess.DataLowByte = 12; break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_13 : serviceAccess.DataLowByte = 13; break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_14 : serviceAccess.DataLowByte = 14; break;
        case TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_15 : serviceAccess.DataLowByte = 15; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindSpeedControlPacketPayload (
    eTAISEIADehumidifierWindSpeedControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_AUTO : serviceAccess.DataLowByte = 0;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_1    : serviceAccess.DataLowByte = 1;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_2    : serviceAccess.DataLowByte = 2;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_3    : serviceAccess.DataLowByte = 3;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_4    : serviceAccess.DataLowByte = 4;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_5    : serviceAccess.DataLowByte = 5;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_6    : serviceAccess.DataLowByte = 6;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_7    : serviceAccess.DataLowByte = 7;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_8    : serviceAccess.DataLowByte = 8;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_9    : serviceAccess.DataLowByte = 9;  break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_10   : serviceAccess.DataLowByte = 10; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_11   : serviceAccess.DataLowByte = 11; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_12   : serviceAccess.DataLowByte = 12; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_13   : serviceAccess.DataLowByte = 13; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_14   : serviceAccess.DataLowByte = 14; break;
        case TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_15   : serviceAccess.DataLowByte = 15; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertSoundControlPacketPayload (
    eTAISEIADehumidifierSoundControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_SILENT               : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON               : serviceAccess.DataLowByte = 1; break;
        case TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON_AND_WATERFULL : serviceAccess.DataLowByte = 2; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertMoldPreventControlPacketPayload (
    eTAISEIADehumidifierMoldPreventControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHighHumidityNotifyControlPacketPayload (
    eTAISEIADehumidifierHighHumidityNotifyControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHighHumidityValueSettingPacketPayload (
    uint8_t value
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING;
    serviceAccess.DataHighByte = 0x00;
    serviceAccess.DataLowByte = (uint8_t) value;

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertKeyLockControlPacketPayload (
    eTAISEIADehumidifierKeyLockControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_OFF : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_ON  : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertControllerProhibitControlPacketPayload (
    TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL;
    serviceAccess.DataHighByte = (uint8_t) ((control.flag & 0xFF00) >> 8);
    serviceAccess.DataLowByte = (uint8_t) (control.flag & 0x00FF);

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertSaaVoiceControlPacketPayload (
    eTAISEIADehumidifierSaaVoiceControl control
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL;
    serviceAccess.DataHighByte = 0x00;

    switch (control) {
        case TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_ENABLE  : serviceAccess.DataLowByte = 0; break;
        case TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_DISABLE : serviceAccess.DataLowByte = 1; break;
        default : break;
    }

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}


TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertCumulativePowerControlPacketPayload (
    uint16_t value
) {
    TaiSEIAServiceAccess_t serviceAccess;

    memset (&serviceAccess, 0, sizeof (TaiSEIAServiceAccess_t));
    serviceAccess.AccessWrite = 1;
    serviceAccess.SATypeID = TAISEIA_SA_TYPE_ID_DEHUMIDIFIER;
    serviceAccess.ServiceID = TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING;
    serviceAccess.DataHighByte = (uint8_t) ((value & 0xFF00) >> 8);
    serviceAccess.DataLowByte = (uint8_t) (value & 0x00FF);

    TaiSEIAPackedPayload_t payload = TaiSEIA_ConvertAccessWritePacketPayload (serviceAccess);

    return payload;
}

/********************************************************************
 *   TaiSEIA Dehumidifier PackedPayload Parse Function Declarations
 ********************************************************************/

eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_ParsePowerControlPacketPayload (
    TaiSEIAPackedPayload_t PacketPayload
) {
    TaiSEIAServiceAccess_t ServiceAccess = TaiSEIA_ParseAccessWritePacketPayload(&PacketPayload);
    
    eTAISEIADehumidifierPowerControl PowerControl = TAISEIA_DEHUMIDIFIER_POWER_CONTROL_UNKNOWN;
    
    switch (ServiceAccess.DataLowByte) {
        case 0x00:
        {
            PowerControl =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_OFF;
        }
            break;
        case 0x01:
        {
            PowerControl =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_ON;
        }
            break;
        default:
            break;
    }
    
    return PowerControl;
}

/******************************************************************
 *   TaiSEIA Dehumidifier WriteAccess Parse Function Declarations
 ******************************************************************/

eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_ParsePowerControlServiceAccess (
    TaiSEIAServiceAccess_t ServiceAccess
) {
    eTAISEIADehumidifierPowerControl PowerControl = TAISEIA_DEHUMIDIFIER_POWER_CONTROL_UNKNOWN;
    
    switch (ServiceAccess.DataLowByte) {
        case 0x00:
        {
            PowerControl =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_OFF;
        }
            break;
        case 0x01:
        {
            PowerControl =  TAISEIA_DEHUMIDIFIER_POWER_CONTROL_ON;
        }
            break;
        default:
            break;
    }
    
    return PowerControl;
}
