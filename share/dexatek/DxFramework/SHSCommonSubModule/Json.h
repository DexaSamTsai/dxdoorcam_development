/*!
 *  @file       Json.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef _JSON_H
# define _JSON_H

# ifdef __cplusplus
# include <string.h>
extern "C"
{
# endif
//==========================================================================
// Include File
//==========================================================================
# include <stdlib.h>
# include <inttypes.h>

//==========================================================================
// Type Define
//==========================================================================
typedef enum {
	JSON_NONE,
	JSON_OBJECT,
	JSON_ARRAY,
	JSON_NUMBER,
	JSON_STRING,
	JSON_BOOLEAN,
	JSON_NULL
} json_type;

typedef struct _json_value		json_value;

struct _json_value {
	json_value		*parent;

	char			*name;
	unsigned int	name_length;
	json_type		type;

	union {
		int			boolean;
		double		number;
		char		*string;
		json_value	*child;
	} v;

	unsigned int	data_length;
	json_value		*end;
	json_value		*next;
};

//==========================================================================
// APIs
//==========================================================================
extern int
Json_GetNumber( json_value *json, double *value );

extern int
Json_GetString( json_value *json, char **string );

extern json_value *
Json_GetChild( json_value *json );

extern json_value *
Json_FindChild( json_value *json, const char *name );

extern int
Json_FindChildNumber( json_value *json, const char *name, double *value );

extern int
Json_FindChildString( json_value *json, const char *name, char **string );

extern json_value *
Json_Parse( const char * json, size_t length, const void *buff, uint32_t max_cnt );

extern void
Json_Free( void *value );

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif


