//============================================================================
// File: dk_peripheralAux.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dk_PeripheralAux.h"
#include "Utils.h"
#include "Json.h"

/******************************************************
 *                      Macros
 ******************************************************/
#define Gc_Add_Config(s, t)  \
    {\
        GenericConfigHeader_t   *GenericConfigHeader;\
        GenericConfigHeader = (GenericConfigHeader_t *) pWriteData;\
        GenericConfigHeader->GenericConfigType = t;\
        GenericConfigHeader->ConfigLen = sizeof(s);\
        pWriteData += sizeof(GenericConfigHeader_t);\
        memcpy(pWriteData, ((uint8_t *) &(s)), sizeof(s));\
        pWriteData += sizeof(s);\
    }

#define GcConfigCaseMacro(c, t) \
            case t:\
            {\
                if (sizeof(c) == pGenericConfigHeader->ConfigLen)\
                {\
                    memcpy((uint8_t *) &(c), pParseData + Ind + sizeof(GenericConfigHeader_t), pGenericConfigHeader->ConfigLen);\
                }\
            }\
            break;

#define GcCheckCaseMacro(c, t) \
            case t:\
            {\
                if (sizeof(c) != pGenericConfigHeader->ConfigLen)\
                {\
                    bAllMatch = false;\
                }\
            }\
            break;

#define GcCheckIfConfigEnabled(c) \
    {\
        if(c == OPERATION_ENABLE)\
        {\
            return true;\
        }\
    }


/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Local Function Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

ThermoPackedPayload_t ThermostatCB_ConvertScheduleToPackedPayload(CBSwitchPointConfig_t ScheduleSetup)
{
    ThermoPackedPayload_t PackedInfo;
    uint8_t i = 0;

    PackedInfo.PackedPayloadLen = 56;

    //Monday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Monday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Tuesday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Tuesday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Wednesday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Wednesday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Thursday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Thursday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Friday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Friday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Saturday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Saturday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    //Sunday
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.FirstONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.FirstOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.SecondONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.SecondOFFTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.ThirdONTime;
    PackedInfo.PackedPayload[i++] = ScheduleSetup.Sunday.ThirdOFFTime;
    PackedInfo.PackedPayload[i++] = 0xFF;
    PackedInfo.PackedPayload[i++] = 0xFF;

    return PackedInfo;
}

CBSwitchPointConfig_t ThermostatCB_ParseSchedulePackedPayload(void* SchedulePayloadPacket, uint16_t PayloadPacketSize)
{
    CBSwitchPointConfig_t   ScheduleSetup;
    uint8_t*                pSchedulePacketPayload = (uint8_t*)SchedulePayloadPacket;
    uint8_t                 i = 0;

    memset(&ScheduleSetup, 0xFF, sizeof(ScheduleSetup));

    if ((PayloadPacketSize >= 56) && (pSchedulePacketPayload))
    {
        //Monday
        ScheduleSetup.Monday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Monday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Monday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Monday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Monday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Monday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Tuesday
        ScheduleSetup.Tuesday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Tuesday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Tuesday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Tuesday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Tuesday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Tuesday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Wednesday
        ScheduleSetup.Wednesday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Wednesday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Wednesday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Wednesday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Wednesday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Wednesday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Thursday
        ScheduleSetup.Thursday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Thursday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Thursday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Thursday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Thursday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Thursday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Friday
        ScheduleSetup.Friday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Friday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Friday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Friday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Friday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Friday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Saturday
        ScheduleSetup.Saturday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Saturday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Saturday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Saturday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Saturday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Saturday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

        //Sunday
        ScheduleSetup.Sunday.FirstONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Sunday.FirstOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Sunday.SecondONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Sunday.SecondOFFTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Sunday.ThirdONTime = pSchedulePacketPayload[i++];
        ScheduleSetup.Sunday.ThirdOFFTime = pSchedulePacketPayload[i++];
        i += 2;

    }

    return ScheduleSetup;
}

ThermoPackedPayload_t ThermostatCB_ConvertGeneralConfigToPackedPayload(eThermostatGenConfigMode_t Command, uint8_t* ParameterBaseOnCommand)
{
    ThermoPackedPayload_t PacketPayload;
    PacketPayload.PackedPayloadLen = 0;

    switch (Command)
    {
        case COMMAND_CHANGE_TO_MANUAL_MODE:
        case COMMAND_CHANGE_TO_AUTOMATIC_MODE:
        case COMMAND_SET_VALVE_MOVE_TO_MOUNTING_POS:
        case COMMAND_SET_VALVE_START_READAPTATION:
        case COMMAND_SHUDOWN_MODE:
        {
            PacketPayload.PackedPayloadLen = 2;
            uint16_t CommandType = Command;
            memcpy(PacketPayload.PackedPayload, &CommandType, sizeof(CommandType));

        }
        break;

        case COMMAND_SET_REALTIME_CLOCK:
        {
            if (ParameterBaseOnCommand)
            {
                uint16_t CommandType = Command;
                PacketPayload.PackedPayloadLen = sizeof(CommandType) + sizeof(ThermoRealTimeClock_t);
                memcpy(PacketPayload.PackedPayload, &CommandType, sizeof(CommandType));
                memcpy(&PacketPayload.PackedPayload[sizeof(CommandType)], ParameterBaseOnCommand, sizeof(ThermoRealTimeClock_t));

            }
        }
        break;

        case COMMAND_CHANGE_TO_MANUAL_WITH_GIVEN_TEMP:
        {
            if (ParameterBaseOnCommand)
            {
                uint16_t CommandType = Command;
                PacketPayload.PackedPayloadLen = sizeof(CommandType) + sizeof(ThermoTemp_t);
                memcpy(PacketPayload.PackedPayload, &CommandType, sizeof(CommandType));
                memcpy(&PacketPayload.PackedPayload[sizeof(CommandType)], ParameterBaseOnCommand, sizeof(ThermoTemp_t));
            }
        }
        break;

        case COMMAND_CHANGE_OPEN_WINDOW_DETECT_THRESHOLD:
        {
            if (ParameterBaseOnCommand)
            {
                uint16_t CommandType = Command;
                PacketPayload.PackedPayloadLen = sizeof(CommandType) + sizeof(ThermoWindowOpenDect_t);
                memcpy(PacketPayload.PackedPayload, &CommandType, sizeof(CommandType));
                memcpy(&PacketPayload.PackedPayload[sizeof(CommandType)], ParameterBaseOnCommand, sizeof(ThermoWindowOpenDect_t));
            }
        }
        break;

        default:
        break;
    }

    return PacketPayload;
}

CBGeneralConfig_t ThermostatCB_ParseGeneralConfigPackedPayload(void* GeneralConfigPayloadPacket, uint16_t PayloadPacketSize)
{
    CBGeneralConfig_t GenConfig;

    if ((PayloadPacketSize >= 2) && GeneralConfigPayloadPacket)
    {
        uint16_t Command;
        memcpy(&Command, GeneralConfigPayloadPacket, sizeof(Command));

        switch (Command)
        {
            case COMMAND_CHANGE_TO_MANUAL_MODE:
            case COMMAND_CHANGE_TO_AUTOMATIC_MODE:
            case COMMAND_SET_VALVE_MOVE_TO_MOUNTING_POS:
            case COMMAND_SET_VALVE_START_READAPTATION:
            case COMMAND_SHUDOWN_MODE:
            {
                GenConfig.Command = Command;
                GenConfig.PayloadSize = 0;
            }
            break;

            case COMMAND_SET_REALTIME_CLOCK:
            {
                if (PayloadPacketSize >= (sizeof(ThermoRealTimeClock_t) + 2))
                {
                    GenConfig.Command = Command;
                    GenConfig.PayloadSize = sizeof(ThermoRealTimeClock_t);
                    memcpy(GenConfig.Payload, ((uint8_t*)GeneralConfigPayloadPacket + sizeof(Command)), sizeof(ThermoRealTimeClock_t));
                }
            }
            break;

            case COMMAND_CHANGE_TO_MANUAL_WITH_GIVEN_TEMP:
            {
                if (PayloadPacketSize >= (sizeof(ThermoTemp_t) + 2))
                {
                    GenConfig.Command = Command;
                    GenConfig.PayloadSize = sizeof(ThermoTemp_t);
                    memcpy(GenConfig.Payload, ((uint8_t*)GeneralConfigPayloadPacket + sizeof(Command)), sizeof(ThermoTemp_t));
                }
            }
            break;

            case COMMAND_CHANGE_OPEN_WINDOW_DETECT_THRESHOLD:
            {
                if (PayloadPacketSize >= (sizeof(ThermoWindowOpenDect_t) + 2))
                {
                    GenConfig.Command = Command;
                    GenConfig.PayloadSize = sizeof(ThermoWindowOpenDect_t);
                    memcpy(GenConfig.Payload, ((uint8_t*)GeneralConfigPayloadPacket + sizeof(Command)), sizeof(ThermoWindowOpenDect_t));
                }
            }
            break;

            default:
            break;
        }

    }

    return GenConfig;
}

ThermoPackedPayload_t ThermostatCB_ConvertSetTemperaturesToPackedPayload(CBSetTemperatures_t TemperaturesSetting)
{
    ThermoPackedPayload_t PacketPayload;
    PacketPayload.PackedPayloadLen = 7;

    //We skip the first byte ([0]) and start on the second byte.
    PacketPayload.PackedPayload[1] = TemperaturesSetting.SetCurrentTemperature;
    PacketPayload.PackedPayload[2] = TemperaturesSetting.SetEnergySavingTemp;
    PacketPayload.PackedPayload[3] = TemperaturesSetting.SetHeatingTemperature;
    PacketPayload.PackedPayload[4] = TemperaturesSetting.OffsetTemperatureValue;
    PacketPayload.PackedPayload[5] = TemperaturesSetting.WindowDetectionThreshold;
    PacketPayload.PackedPayload[6] = TemperaturesSetting.WindowOpenDetectionTimer;

    return PacketPayload;
}


CBSetTemperatures_t ThermostatCB_ParseSetTemperaturesToPackedPayload(void* SetTemperaturesPayloadPacket, uint16_t PayloadPacketSize)
{
    CBSetTemperatures_t setTemperatures;
    memset(&setTemperatures, 0, sizeof(setTemperatures));

    uint8_t* pSetTemperaturesPacketPayload = (uint8_t*)SetTemperaturesPayloadPacket;

    if ((PayloadPacketSize >= 7) && SetTemperaturesPayloadPacket)
    {
        setTemperatures.SetCurrentTemperature       = pSetTemperaturesPacketPayload[1];
        setTemperatures.SetEnergySavingTemp         = pSetTemperaturesPacketPayload[2];
        setTemperatures.SetHeatingTemperature       = pSetTemperaturesPacketPayload[3];
        setTemperatures.OffsetTemperatureValue      = pSetTemperaturesPacketPayload[4];
        setTemperatures.WindowDetectionThreshold    = pSetTemperaturesPacketPayload[5];
        setTemperatures.WindowOpenDetectionTimer    = pSetTemperaturesPacketPayload[6];
    }

    return setTemperatures;
}


IRRemotePackedPayload_t IRRemote_ConvertNewRemoteLearningPackedPayload( uint32_t RemoteMType, uint32_t RemoteDType, uint8_t* RemoteMNameUnicode8, uint8_t* RemoteDNameUnicode8)
{
    IRRemotePackedPayload_t     PacketPayload;
    IRRemoteNewLearningHeader_t NewLearningHeader;
    NewLearningHeader.RemoteType = RemoteMType;
    NewLearningHeader.RemoteKeyType = RemoteDType;
    NewLearningHeader.RemoteNameSize = RemoteMNameUnicode8 ? (strlen((char*)RemoteMNameUnicode8) + 1):0;
    NewLearningHeader.RemoteKeyNameSize = RemoteDNameUnicode8 ? (strlen((char*)RemoteDNameUnicode8) + 1):0;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));
    PacketPayload.PackedPayloadLen = sizeof(IRRemoteNewLearningHeader_t)+NewLearningHeader.RemoteNameSize+NewLearningHeader.RemoteKeyNameSize;

    if (PacketPayload.PackedPayloadLen < sizeof(PacketPayload.PackedPayload))
    {
        uint8_t* pWriteData = PacketPayload.PackedPayload;
        memcpy(pWriteData,&NewLearningHeader, sizeof(NewLearningHeader)); pWriteData += sizeof(NewLearningHeader);
        if (NewLearningHeader.RemoteNameSize)
        {
            memcpy(pWriteData, RemoteMNameUnicode8, NewLearningHeader.RemoteNameSize - 1); pWriteData += NewLearningHeader.RemoteNameSize;
        }
        if (NewLearningHeader.RemoteKeyNameSize)
        {
            memcpy(pWriteData, RemoteDNameUnicode8, NewLearningHeader.RemoteKeyNameSize - 1); pWriteData += NewLearningHeader.RemoteKeyNameSize;
        }

    }
    else //Buffer size limit reached
    {
        //WARNING: Buffer size exceed limitation... increasing array buffer of IRRemotePackedPayload_t can solve this issue
        PacketPayload.PackedPayloadLen = 0;

    }

    return PacketPayload;
 }

IRRemotePackedPayload_t IRRemote_ConvertInsertKeyRemoteLearningPackedPayload( uint64_t RemoteMID, uint32_t RemoteDType, uint8_t* RemoteDNameUnicode8)
{
    IRRemotePackedPayload_t         PacketPayload;
    IRRemoteInsertLearningHeader_t  InsertLearningHeader;
    InsertLearningHeader.RemoteID       = RemoteMID;
    InsertLearningHeader.RemoteKeyType  = RemoteDType;
    InsertLearningHeader.RemoteKeyNameSize = RemoteDNameUnicode8 ? (strlen((char*)RemoteDNameUnicode8) + 1) : 0;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));
    PacketPayload.PackedPayloadLen = sizeof(IRRemoteInsertLearningHeader_t)+InsertLearningHeader.RemoteKeyNameSize;

    if (PacketPayload.PackedPayloadLen < sizeof(PacketPayload.PackedPayload))
    {
        uint8_t* pWriteData = PacketPayload.PackedPayload;
        memcpy(pWriteData,&InsertLearningHeader, sizeof(InsertLearningHeader)); pWriteData += sizeof(InsertLearningHeader);

        if (InsertLearningHeader.RemoteKeyNameSize)
        {
            memcpy(pWriteData, RemoteDNameUnicode8, InsertLearningHeader.RemoteKeyNameSize - 1); pWriteData += InsertLearningHeader.RemoteKeyNameSize;
        }

    }
    else //Buffer size limit reached
    {
        //WARNING: Buffer size exceed limitation... increasing array buffer of IRRemotePackedPayload_t can solve this issue
        PacketPayload.PackedPayloadLen = 0;

    }

    return PacketPayload;
}

IRRemotePackedPayload_t IRRemote_ConvertUpdateKeyRemoteLearningPackedPayload( uint64_t RemoteMID, uint64_t RemoteDID, uint32_t RemoteDType, uint8_t* RemoteDNameUnicode8)
{
    IRRemotePackedPayload_t         PacketPayload;
    IRRemoteUpdateLearningHeader_t  UpdateLearningHeader;
    UpdateLearningHeader.RemoteID       = RemoteMID;
    UpdateLearningHeader.RemoteKeyID    = RemoteDID;
    UpdateLearningHeader.RemoteKeyType  = RemoteDType;
    UpdateLearningHeader.RemoteKeyNameSize = RemoteDNameUnicode8 ? (strlen((char*)RemoteDNameUnicode8) + 1) : 0;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));
    PacketPayload.PackedPayloadLen = sizeof(IRRemoteUpdateLearningHeader_t)+UpdateLearningHeader.RemoteKeyNameSize;

    if (PacketPayload.PackedPayloadLen < sizeof(PacketPayload.PackedPayload))
    {
        uint8_t* pWriteData = PacketPayload.PackedPayload;
        memcpy(pWriteData,&UpdateLearningHeader, sizeof(UpdateLearningHeader)); pWriteData += sizeof(UpdateLearningHeader);

        if (UpdateLearningHeader.RemoteKeyNameSize)
        {
            memcpy(pWriteData, RemoteDNameUnicode8, UpdateLearningHeader.RemoteKeyNameSize - 1); pWriteData += UpdateLearningHeader.RemoteKeyNameSize;
        }

    }
    else //Buffer size limit reached
    {
        //WARNING: Buffer size exceed limitation... increasing array buffer of IRRemotePackedPayload_t can solve this issue
        PacketPayload.PackedPayloadLen = 0;

    }

    return PacketPayload;
}

IRRemotePackedPayload_t IRRemote_ConvertDeleteRemotePackedPayload( uint64_t RemoteMID)
{
    IRRemotePackedPayload_t         PacketPayload;
    IRRemoteDeleteHeader_t          IRDeleteHeader;
    IRDeleteHeader.RemoteID       = RemoteMID;

    PacketPayload.PackedPayloadLen = sizeof(IRRemoteDeleteHeader_t);

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData,&IRDeleteHeader, sizeof(IRDeleteHeader));

    return PacketPayload;
}

IRRemotePackedPayload_t IRRemote_ConvertDeleteKeyRemotePackedPayload( uint64_t RemoteMID, uint64_t RemoteDID)
{
    IRRemotePackedPayload_t         PacketPayload;
    IRRemoteKeyDeleteHeader_t       IRKeyDeleteHeader;
    IRKeyDeleteHeader.RemoteID       = RemoteMID;
    IRKeyDeleteHeader.RemoteKeyID    = RemoteDID;

    PacketPayload.PackedPayloadLen = sizeof(IRRemoteKeyDeleteHeader_t);

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData,&IRKeyDeleteHeader, sizeof(IRKeyDeleteHeader));

    return PacketPayload;
}

IRRemotePackedPayload_t IRRemote_ConvertSendRemotePackedPayload( uint64_t RemoteMID, uint64_t RemoteDID)
{
    IRRemotePackedPayload_t         PacketPayload;
    IRRemoteSendHeader_t            IRSendHeader;
    IRSendHeader.RemoteID       = RemoteMID;
    IRSendHeader.RemoteKeyID    = RemoteDID;

    PacketPayload.PackedPayloadLen = sizeof(IRRemoteSendHeader_t);

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData,&IRSendHeader, sizeof(IRSendHeader));

    return PacketPayload;
}


extern IRRemoteSendHeader_t IRRemote_ParseSendRemotePackedPayload( void* IRSendPayloadPacket, uint16_t IRSendPayloadPacketSize )
{
    IRRemoteSendHeader_t RemoteSendHeader;

    RemoteSendHeader.RemoteID = 0;
    RemoteSendHeader.RemoteKeyID = 0;

    if (IRSendPayloadPacket && (sizeof(IRRemoteSendHeader_t) == IRSendPayloadPacketSize))
    {
        memcpy(&RemoteSendHeader, IRSendPayloadPacket, IRSendPayloadPacketSize);
    }

    return RemoteSendHeader;
}

extern AlarmPackedPayload_t Alarm_ConvertChangeAlarmArmingModePackedPayload(uint8_t ArmingMode)
{
    AlarmPackedPayload_t                PacketPayload;
    AlarmChangeAlarmArmingModeHeader_t  ChangeAlarmArmingModeHeader;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));

    ChangeAlarmArmingModeHeader.ArmingMode = ArmingMode;

    PacketPayload.PackedPayloadLen = sizeof(AlarmChangeAlarmArmingModeHeader_t);

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData, &ChangeAlarmArmingModeHeader, sizeof(AlarmChangeAlarmArmingModeHeader_t));
    pWriteData += sizeof(AlarmChangeAlarmArmingModeHeader_t);

    return PacketPayload;
}

extern AlarmPackedPayload_t Alarm_ConvertSetPinCodePackedPayload(uint8_t *PinCode)
{
    AlarmPackedPayload_t                PacketPayload;
    AlarmSetPinCodeHeader_t             SetPinCodeHeader;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));

    SetPinCodeHeader.LengthOfPinCode = strlen((char*)PinCode);

    PacketPayload.PackedPayloadLen = sizeof(AlarmSetPinCodeHeader_t) + SetPinCodeHeader.LengthOfPinCode;

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData, &SetPinCodeHeader, sizeof(AlarmSetPinCodeHeader_t));
    pWriteData += sizeof(AlarmSetPinCodeHeader_t);
    memcpy(pWriteData, PinCode, SetPinCodeHeader.LengthOfPinCode);
    pWriteData += SetPinCodeHeader.LengthOfPinCode;

    return PacketPayload;
}

extern AlarmPackedPayload_t Alarm_ConvertSetPinCodePackedPayload_EXT(uint8_t PinCodeMode, uint8_t *PinCode)
{
    AlarmPackedPayload_t                PacketPayload;
    AlarmSetPinCodeHeaderExt_t          SetPinCodeHeader;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));

    SetPinCodeHeader.PinCodeMode = PinCodeMode;
    SetPinCodeHeader.LengthOfPinCode = strlen((char*)PinCode);

    PacketPayload.PackedPayloadLen = sizeof(AlarmSetPinCodeHeaderExt_t) + SetPinCodeHeader.LengthOfPinCode;

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData, &SetPinCodeHeader, sizeof(AlarmSetPinCodeHeaderExt_t));
    pWriteData += sizeof(AlarmSetPinCodeHeaderExt_t);
    memcpy(pWriteData, PinCode, SetPinCodeHeader.LengthOfPinCode);
    pWriteData += SetPinCodeHeader.LengthOfPinCode;

    return PacketPayload;

}

extern bool Alarm_ArmingDelayCreateWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList)
{
    if(pWhiteList == NULL)
    {
        return false;
    }
    pWhiteList->AmountOfWhiteList = 0;
    pWhiteList->Items = NULL;

    return true;
}

extern bool Alarm_ArmingDelayFreeWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList)
{
    if(pWhiteList == NULL)
    {
        return false;
    }

    pWhiteList->AmountOfWhiteList = 0;
    if(pWhiteList->Items)
    {
        DK_FREE(pWhiteList->Items);
    }
    pWhiteList->Items= NULL;

    return true;
}

extern bool Alarm_ArmingDelayAddWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList, AlarmArmingDelayWhiteListItem_t *WhiteListItem)
{
    if((pWhiteList == NULL) || (WhiteListItem == NULL))
    {
        return false;
    }
    AlarmArmingDelayWhiteListItem_t *NewItems;

    NewItems = DK_ALLOC("WhiteListBuf", 1, sizeof(AlarmArmingDelayWhiteListItem_t) * (pWhiteList->AmountOfWhiteList + 1));
    if(NewItems == NULL)
    {
        return false;
    }

    if(pWhiteList->AmountOfWhiteList > 0)
    {
        memcpy(((uint8_t *) NewItems),  ((uint8_t *) (pWhiteList->Items)), sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList);
        DK_FREE(pWhiteList->Items);
    }
    memcpy(((uint8_t *) NewItems) + (sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList), ((uint8_t *) WhiteListItem), sizeof(AlarmArmingDelayWhiteListItem_t));

    pWhiteList->Items = NewItems;
    pWhiteList->AmountOfWhiteList ++;

    return true;
}

/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertSetArmingDelayPackedPayloadWithWhitelist_ALLOC(AlarmSetArmingDelayHeader_t *pArmingDelays, AlarmArmingDelayWhiteListInstance_t *pWhiteList)
{
    //AlarmPackedPayload_t                PacketPayload;
    AlarmPackedPayloadAlloc_t           PacketPayload;

    PacketPayload.PackedPayloadLen = sizeof(AlarmSetArmingDelayHeader_t) + sizeof(pWhiteList->AmountOfWhiteList) + (sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList);
    PacketPayload.pPackedPayload = (uint8_t *) DK_ALLOC("WhiteListBuf", 1, PacketPayload.PackedPayloadLen);
    memset(PacketPayload.pPackedPayload, 0 , PacketPayload.PackedPayloadLen);

    uint8_t* pWriteData = PacketPayload.pPackedPayload;
    uint16_t Ind = 0;

    memcpy(pWriteData + Ind, &(pArmingDelays->ArmingDelayDisarm),  sizeof(pArmingDelays->ArmingDelayDisarm));
    Ind += sizeof(pArmingDelays->ArmingDelayDisarm);
    memcpy(pWriteData + Ind, &(pArmingDelays->ArmingDelayHome),  sizeof(pArmingDelays->ArmingDelayHome));
    Ind += sizeof(pArmingDelays->ArmingDelayHome);
    memcpy(pWriteData + Ind, &(pArmingDelays->ArmingDelayArm),  sizeof(pArmingDelays->ArmingDelayArm));
    Ind += sizeof(pArmingDelays->ArmingDelayArm);

    memcpy(pWriteData + Ind, &(pWhiteList->AmountOfWhiteList),  sizeof(pWhiteList->AmountOfWhiteList));
    Ind += sizeof(pWhiteList->AmountOfWhiteList);

    memcpy(pWriteData + Ind, pWhiteList->Items, sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList);
    Ind += (sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList);

    PacketPayload.PackedPayloadLen = Ind;
    return PacketPayload;
}

extern AlarmPackedPayload_t Alarm_ConvertSetArmingDelayPackedPayload(AlarmSetArmingDelayHeader_t ArmingDelays)
{
    AlarmPackedPayload_t                PacketPayload;
//    AlarmSetArmingDelayHeader_t         SetArmingDelayHeader;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));

    PacketPayload.PackedPayloadLen = 0;

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    uint16_t Ind = 0;

    memcpy(pWriteData + Ind, &(ArmingDelays.ArmingDelayDisarm),  sizeof(ArmingDelays.ArmingDelayDisarm));
    Ind += sizeof(ArmingDelays.ArmingDelayDisarm);
    memcpy(pWriteData + Ind, &(ArmingDelays.ArmingDelayHome),  sizeof(ArmingDelays.ArmingDelayHome));
    Ind += sizeof(ArmingDelays.ArmingDelayHome);
    memcpy(pWriteData + Ind, &(ArmingDelays.ArmingDelayArm),  sizeof(ArmingDelays.ArmingDelayArm));
    Ind += sizeof(ArmingDelays.ArmingDelayArm);

    PacketPayload.PackedPayloadLen = Ind;
    return PacketPayload;
}

extern AlarmPackedPayload_t Alarm_ConvertSensorDetectPackedPayload(uint8_t Zone)
{
    AlarmPackedPayload_t                PacketPayload;
    AlarmSensorDetectHeader_t           SetArmingDelayHeader;

    memset(&PacketPayload, 0 , sizeof(PacketPayload));

    SetArmingDelayHeader.Zone = Zone;

    PacketPayload.PackedPayloadLen = sizeof(AlarmSensorDetectHeader_t);

    uint8_t* pWriteData = PacketPayload.PackedPayload;
    memcpy(pWriteData, &SetArmingDelayHeader, sizeof(AlarmSensorDetectHeader_t));
    pWriteData += sizeof(AlarmSensorDetectHeader_t);

    return PacketPayload;
}


/* please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertSetEmergencyCallPhoneNumbersWithNamesPackedPayload(AlarmPhoneNumberWithName_t* pPhoneNumberArray)
{
    AlarmPackedPayloadAlloc_t                   PacketPayload;
    AlarmSetEmergencyCallPhoneNumbersHeader_t   SetEmergencyCallPhoneNumbersHeader;
    uint16_t                                    TotalPhoneNumbersDataLength = 0;
    uint16_t                                    i = 0;

    for (i = 0; i < pPhoneNumberArray->AmountOfPhones; i++)
    {
        uint8_t    PhoneNumberLen;
        PhoneNumberLen = strlen((char*)pPhoneNumberArray->PhoneNumber[i]);
        TotalPhoneNumbersDataLength += (3 + PhoneNumberLen + pPhoneNumberArray->NameByteCount[i]);
    }

    PacketPayload.pPackedPayload = (uint8_t *) DK_ALLOC("AlarmPhoneBuf", 1, sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t) + TotalPhoneNumbersDataLength);

    PacketPayload.PackedPayloadLen = sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t) + TotalPhoneNumbersDataLength;
    memset(PacketPayload.pPackedPayload, 0, PacketPayload.PackedPayloadLen);

    SetEmergencyCallPhoneNumbersHeader.AmountofPhoneNumbers = pPhoneNumberArray->AmountOfPhones;
    SetEmergencyCallPhoneNumbersHeader.TotalPhoneNumbersDataLength = TotalPhoneNumbersDataLength;

    uint8_t *pWriteData = PacketPayload.pPackedPayload;
    memcpy(pWriteData, &SetEmergencyCallPhoneNumbersHeader, sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t));
    pWriteData += sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t);

    for (i = 0; i < pPhoneNumberArray->AmountOfPhones; i++)
    {
        uint8_t    PhoneNumberLen;
        PhoneNumberLen = strlen((char*)pPhoneNumberArray->PhoneNumber[i]);

        *pWriteData = pPhoneNumberArray->ReferenceID[i];
        pWriteData += 1;
        *pWriteData = PhoneNumberLen;
        pWriteData += 1;
        memcpy(pWriteData, pPhoneNumberArray->PhoneNumber[i], PhoneNumberLen);
        pWriteData += PhoneNumberLen;
        *pWriteData = pPhoneNumberArray->NameByteCount[i];
        pWriteData += 1;
        memcpy(pWriteData, pPhoneNumberArray->Name[i], pPhoneNumberArray->NameByteCount[i]);
        pWriteData += pPhoneNumberArray->NameByteCount[i];
    }

    return PacketPayload;
}


extern AlarmPackedPayloadAlloc_t Alarm_ConvertJobConditionUpdateForSpecificZonePackedPayload_ALLOC(uint8_t Zone, uint16_t JobLength, void* JobPayloadIn)
{
    //AlarmPackedPayload_t                            PacketPayload;
    AlarmPackedPayloadAlloc_t                       PacketPayload;
    AlarmJobConditionUpdateForSpecificZoneHeader_t  JobConditionUpdateForSpecificZoneHeader;

    PacketPayload.PackedPayloadLen = sizeof(AlarmJobConditionUpdateForSpecificZoneHeader_t) + JobLength;
    PacketPayload.pPackedPayload = (uint8_t *) DK_ALLOC("ConditionHpBuf", 1, PacketPayload.PackedPayloadLen);
    memset(PacketPayload.pPackedPayload, 0 , PacketPayload.PackedPayloadLen);

    //memset(&PacketPayload, 0 , sizeof(PacketPayload));

    JobConditionUpdateForSpecificZoneHeader.Zone = Zone;
    JobConditionUpdateForSpecificZoneHeader.JobLength = JobLength;

    //PacketPayload.PackedPayloadLen = sizeof(AlarmJobConditionUpdateForSpecificZoneHeader_t) + JobLength;

    uint8_t *pWriteData = PacketPayload.pPackedPayload;
    memcpy(pWriteData, (uint8_t *) &JobConditionUpdateForSpecificZoneHeader, sizeof(AlarmJobConditionUpdateForSpecificZoneHeader_t));
    pWriteData += sizeof(AlarmJobConditionUpdateForSpecificZoneHeader_t);

    if (JobPayloadIn && JobLength > 0)
    {
        memcpy(pWriteData, JobPayloadIn, JobLength);
        pWriteData += JobLength;
    }
    return PacketPayload;
}

extern GeneralSizePackedPayload_t Siren_ConvertAlertControlCountModePacketPayload(uint16_t AlertCount)
{
    GeneralSizePackedPayload_t  PacketPayload;
    SirenControl_t              SirenControlPayload;

    SirenControlPayload.Control = 0x03; //Alarm with Count
    SirenControlPayload.Count = AlertCount;

    PacketPayload.PackedPayloadLen = sizeof(SirenControl_t);
    memcpy(PacketPayload.PackedPayload, &SirenControlPayload, sizeof(SirenControl_t));

    return PacketPayload;
}

extern GeneralSizePackedPayload_t Siren_ConvertAlertControlOnOffPacketPayload(uint8_t OnOff)
{
    GeneralSizePackedPayload_t  PacketPayload;
    SirenControl_t              SirenControlPayload;

    SirenControlPayload.Control = (OnOff) ? 0x02:0x01; //0x02 - Alarm ON (Non Stop) or 0x01 - Normal OFF

    PacketPayload.PackedPayloadLen = sizeof(SirenControl_t);
    memcpy(PacketPayload.PackedPayload, &SirenControlPayload, sizeof(SirenControl_t));

    return PacketPayload;
}


extern GeneralSizePackedPayload_t RemoteControl_ConvertSetRTCPacketPayload(RTC_t RTCtoSet)
{
    GeneralSizePackedPayload_t PacketPayload;

    PacketPayload.PackedPayloadLen = sizeof(RTC_t);
    memcpy(PacketPayload.PackedPayload, &RTCtoSet, sizeof(RTC_t));

    return PacketPayload;
}

extern GeneralSizePackedPayload_t RGBLightBulb_ConvertColorModelPacketPayload(ColorModel_t Model)
{
    GeneralSizePackedPayload_t PacketPayload;

    size_t size = sizeof(ColorModel_t);

    PacketPayload.PackedPayloadLen = size;
    memcpy(PacketPayload.PackedPayload, &Model, size);

    return PacketPayload;
}

extern GeneralSizePackedPayload_t LightStrip_ConvertLightStripModelPacketPayload(LightStripModel_t Model)
{
    GeneralSizePackedPayload_t PacketPayload;
    
    size_t size = sizeof(LightStripModel_t);
    
    PacketPayload.PackedPayloadLen = size;
    memcpy(PacketPayload.PackedPayload, &Model, size);
    
    return PacketPayload;
}

extern AlarmPackedPayloadAlloc_t Alarm_ConvertJobExecutionUpdateAndTimeoutPackedPayload_ALLOC(uint16_t ExecutionTimeout, uint16_t JobEnableLength, void* JobEnablePayloadIn, uint16_t JobDisableLength, void* JobDisablePayloadIn)
{
    //AlarmPackedPayload_t                            PacketPayload;
    AlarmPackedPayloadAlloc_t                       PacketPayload;
    AlarmJobExecutionUpdateAndTimeoutHeader_t       JobExecutionUpdateAndTimeoutHeader;

    PacketPayload.PackedPayloadLen = sizeof(AlarmJobExecutionUpdateAndTimeoutHeader_t) + JobEnableLength + JobDisableLength;
    PacketPayload.pPackedPayload = (uint8_t *) DK_ALLOC("ExecutionHpBuf", 1, PacketPayload.PackedPayloadLen);
    memset(PacketPayload.pPackedPayload, 0 , PacketPayload.PackedPayloadLen);

    JobExecutionUpdateAndTimeoutHeader.ExecutionTimeout = ExecutionTimeout;
    JobExecutionUpdateAndTimeoutHeader.JobEnableLength = JobEnableLength;
    JobExecutionUpdateAndTimeoutHeader.JobDisableLength = JobDisableLength;

    uint8_t *pWriteData = PacketPayload.pPackedPayload;
    memcpy(pWriteData, (uint8_t *) &JobExecutionUpdateAndTimeoutHeader, sizeof(AlarmJobExecutionUpdateAndTimeoutHeader_t));
    pWriteData += sizeof(AlarmJobExecutionUpdateAndTimeoutHeader_t);

    if (JobEnableLength > 0)
    {
        memcpy(pWriteData, JobEnablePayloadIn, JobEnableLength);
        pWriteData += JobEnableLength;
    }

    if (JobDisableLength > 0)
    {
        memcpy(pWriteData, JobDisablePayloadIn, JobDisableLength);
        pWriteData += JobDisableLength;
    }

    return PacketPayload;
}

extern AlarmPackedPayloadAlloc_t Alarm_ConvertConfigsPackedPayload(AlarmConfigs_t *pAlarmConfigs)
{
    AlarmPackedPayloadAlloc_t   PacketPayload;

    PacketPayload.PackedPayloadLen = 0;

    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Alert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Sos);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Sabotage);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Powersource);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Wifistate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Gsmstate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Silentalert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Appnotification_Disarmsabtg);

    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Alert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Sos);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Sabotage);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Powersource);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Wifistate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Gsmstate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Silentalert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularsms_Disarmsabtg);

    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Alert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Sos);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Sabotage);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Powersource);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Wifistate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Gsmstate);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Silentalert);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Cellularcall_Disarmsabtg);

    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Incoming_Call_Reject_Delay);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Display_Off_Timing);
    PacketPayload.PackedPayloadLen +=  sizeof(GenericConfigHeader_t) + sizeof(pAlarmConfigs->Gc_Alarm_Outgoing_Call_Hangup_Delay);

    PacketPayload.pPackedPayload = (uint8_t *) DK_ALLOC("AlarmCfgBuf", 1, PacketPayload.PackedPayloadLen);
    memset(PacketPayload.pPackedPayload, 0 , PacketPayload.PackedPayloadLen);

    uint8_t *pWriteData = PacketPayload.pPackedPayload;

    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Alert,        GC_ALARM_APPNOTIFICATION_ALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Sos,          GC_ALARM_APPNOTIFICATION_SOS);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Sabotage,     GC_ALARM_APPNOTIFICATION_SABOTAGE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Powersource,  GC_ALARM_APPNOTIFICATION_POWERSOURCE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Wifistate,    GC_ALARM_APPNOTIFICATION_WIFISTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Gsmstate,     GC_ALARM_APPNOTIFICATION_GSMSTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Silentalert,  GC_ALARM_APPNOTIFICATION_SILENTALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Appnotification_Disarmsabtg,  GC_ALARM_APPNOTIFICATION_DISARMSABTG);

    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Alert,            GC_ALARM_CELLULARSMS_ALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Sos,              GC_ALARM_CELLULARSMS_SOS);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Sabotage,         GC_ALARM_CELLULARSMS_SABOTAGE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Powersource,      GC_ALARM_CELLULARSMS_POWERSOURCE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Wifistate,        GC_ALARM_CELLULARSMS_WIFISTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Gsmstate,         GC_ALARM_CELLULARSMS_GSMSTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Silentalert,      GC_ALARM_CELLULARSMS_SILENTALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularsms_Disarmsabtg,      GC_ALARM_CELLULARSMS_DISARMSABTG);

    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Alert,           GC_ALARM_CELLULARCALL_ALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Sos,             GC_ALARM_CELLULARCALL_SOS);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Sabotage,        GC_ALARM_CELLULARCALL_SABOTAGE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Powersource,     GC_ALARM_CELLULARCALL_POWERSOURCE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Wifistate,       GC_ALARM_CELLULARCALL_WIFISTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Gsmstate,        GC_ALARM_CELLULARCALL_GSMSTATE);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Silentalert,     GC_ALARM_CELLULARCALL_SILENTALERT);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Cellularcall_Disarmsabtg,     GC_ALARM_CELLULARCALL_DISARMSABTG);

    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Incoming_Call_Reject_Delay,   GC_ALARM_INCOMING_CALL_REJECT_DELAY);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Display_Off_Timing,           GC_ALARM_DISPLAY_OFF_TIMING);
    Gc_Add_Config(pAlarmConfigs->Gc_Alarm_Outgoing_Call_Hangup_Delay,   GC_ALARM_OUTGOING_CALL_HANGUP_DELAY);


    return PacketPayload;
}

extern int b64_pton(char const *src, unsigned char *target, size_t targsize);
extern int b64_ntop(uint8_t const *src, size_t srclength, char *target, size_t targsize);

static const uint16_t map_fps[ 4 ] = { 12, 15, 24, 30, };
static const uint16_t map_bitrate[ 7 ] = { 256, 512, 768, 1024, 2048, 3072, 4096, };

extern GenericPackedPayloadAlloc_t IPCam_ConvertSetStreamingConfigPackedPayload( IPCamStreamingConfigHeader_t StreamingConfig )
{
	GenericPackedPayloadAlloc_t PacketPayload;

	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "StreamingConfig", 1, 128 );

	if ( PacketPayload.pPackedPayload ) {
		memset( PacketPayload.pPackedPayload, 0, 128 );

		PacketPayload.PackedPayloadLen	= snprintf( ( char * )PacketPayload.pPackedPayload, 128,
													"{\"StreamingConfig\": {"
														"\"Resolution\": %u,"
														"\"FrameRate\": %u,"
														"\"BitRate\": %u"
													"}}",
													( StreamingConfig.Resolution == IPCAM_RESOLUTION_1080P )? 1080: 720,
													map_fps[ StreamingConfig.FPS ],
													map_bitrate[ StreamingConfig.Bitrate ] );

		PacketPayload.PackedPayloadLen ++;
	} else {
		PacketPayload.PackedPayloadLen	= 0;
	}

	return PacketPayload;
}

extern GenericPackedPayloadAlloc_t IPCam_ConvertSetEventRequestPackedPayload( IPCamEventRequestHeader_t EventRequest )
{
	GenericPackedPayloadAlloc_t PacketPayload = { 0, };
	uint8_t *dst;

	dst =
	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "EventRequest", 1, 128 );

	if ( PacketPayload.pPackedPayload ) {
		memset( PacketPayload.pPackedPayload, 0, 128 );

	dst += snprintf( ( char * )dst, 128, "{" );

	if ( EventRequest.VideoEvt.Avail ) {
		dst += snprintf( ( char * )dst, ( 128 - ( dst - PacketPayload.pPackedPayload ) ),
						"\"VideoEvt\": {"
							"\"DurationB\": %d,"
							"\"DurationA\": %d,"
							"\"Rule\": %d }%s",
							EventRequest.VideoEvt.PreRecordDuration,
							EventRequest.VideoEvt.PostRecordDuration,
							EventRequest.VideoEvt.Role,
							( EventRequest.JPEGEvt.Avail )? ",": "" );
	}

	if ( EventRequest.JPEGEvt.Avail ) {
		dst += snprintf( ( char * )dst, ( 128 - ( dst - PacketPayload.pPackedPayload ) ),
						"\"JPEGEvt\": {"
							"\"DurationB\": %d,"
							"\"DurationA\": %d,"
							"\"Rule\": %d }",
							EventRequest.JPEGEvt.PreRecordDuration,
							EventRequest.JPEGEvt.PostRecordDuration,
							EventRequest.JPEGEvt.Role );
	}

	dst += snprintf( ( char * )dst, 128, "}" );

		PacketPayload.PackedPayloadLen	= dst - PacketPayload.pPackedPayload + 1;
	} else {
		PacketPayload.PackedPayloadLen	= 0;
	}

	return PacketPayload;
}

extern GenericPackedPayloadAlloc_t IPCam_ConvertSetMotionConfigPackedPayload( IPCamMotionConfigHeader_t MotionConfig )
{
	GenericPackedPayloadAlloc_t PacketPayload;
	uint8_t *dst;

	dst =
	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "MotionConfig", 1, 2048 );

	if ( PacketPayload.pPackedPayload ) {
		memset( PacketPayload.pPackedPayload, 0, 2048 );

		dst += snprintf( ( char * )dst, 2048,
						"{\"MotionConfig\": {"
							"\"Mode\": %d",
							MotionConfig.Mode );

		if ( MotionConfig.ActiveWindowLen ) {
			int i;

			dst += snprintf( ( char * )dst, ( 2048 - ( dst - PacketPayload.pPackedPayload ) ), ",\"ActiveWindow\": [" );

			for ( i = 0; i < MotionConfig.ActiveWindowLen && i < IPCAM_MOTION_ACTIVE_WINDOW_MAX_COUNT; i ++ ) {
				if ( i ) {
					dst += snprintf( ( char * )dst, ( 2048 - ( dst - PacketPayload.pPackedPayload ) ), "," );
				}

				dst += snprintf( ( char * )dst, ( 2048 - ( dst - PacketPayload.pPackedPayload ) ),
								"{"
									"\"WinStartX\": %.02f,"
									"\"WinStartY\": %.02f,"
									"\"WinEndX\": %.02f,"
									"\"WinEndY\": %.02f,"
									"\"Sensitivity\": %d"
								"}",
								MotionConfig.ActiveWindow[ i ].WinStartX,
								MotionConfig.ActiveWindow[ i ].WinStartY,
								MotionConfig.ActiveWindow[ i ].WinEndX,
								MotionConfig.ActiveWindow[ i ].WinEndY,
								MotionConfig.ActiveWindow[ i ].Sensitivity );
			}

			dst += snprintf( ( char * )dst, ( 2048 - ( dst - PacketPayload.pPackedPayload ) ), "]" );
		}

		dst += snprintf( ( char * )dst, ( 2048 - ( dst - PacketPayload.pPackedPayload ) ), "}}" );

		PacketPayload.PackedPayloadLen	= dst - PacketPayload.pPackedPayload + 1;
	} else {
		PacketPayload.PackedPayloadLen	= 0;
	}

	return PacketPayload;
}

extern GenericPackedPayloadAlloc_t IPCam_ConvertSetVideoConfigPackedPayload( IPCamVideoConfigHeader_t VideoConfig )
{
	GenericPackedPayloadAlloc_t PacketPayload;

	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "VideoConfig", 1, 128 );

	if ( PacketPayload.pPackedPayload ) {
		memset( PacketPayload.pPackedPayload, 0, 128 );

		PacketPayload.PackedPayloadLen	= snprintf( ( char * )PacketPayload.pPackedPayload, 128,
													"{\"VideoConfig\": {"
														"\"UpSideDown\": %d"
													"}}",
													( VideoConfig.UpSideDown == IPCAM_UP_SIDE_DOWN_ON )? 1: 0 );

		PacketPayload.PackedPayloadLen ++;
	} else {
		PacketPayload.PackedPayloadLen	= 0;
	}

	return PacketPayload;
}

extern GenericPackedPayloadAlloc_t IPCam_ConvertCaptureImagePackedPayload( IPCamCaptureImage_t CaptureImage )
{
	GenericPackedPayloadAlloc_t PacketPayload;

	uint32_t	b64_image_size	= 0;

	if ( CaptureImage.Image && CaptureImage.ImageSize ) {
		b64_image_size = 1 + ( ( ( CaptureImage.ImageSize + 3 ) * 4 ) / 3 );
	}

	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "CaptureImage", 1, ( 128 + b64_image_size ) );

    if ( PacketPayload.pPackedPayload ) {
    	uint8_t *dst = PacketPayload.pPackedPayload;

    	memset( PacketPayload.pPackedPayload, 0, ( 128 + b64_image_size ) );

		dst	+= snprintf( ( char * )dst, 128,
						"{\"CaptureImage\": {"
							"\"Resolution\": %d,"
							"\"Format\": \"%s\"",
						( CaptureImage.Resolution == IPCAM_RESOLUTION_1080P )?	1080:
						( CaptureImage.Resolution == IPCAM_RESOLUTION_720P )?	720: 480,
						"JPEG" );

		if ( b64_image_size ) {
			int n;

			dst	+= sprintf( ( char * )dst, ",\"Image\": \"" );

			n = b64_ntop( CaptureImage.Image, CaptureImage.ImageSize, ( char * )dst, b64_image_size );
			if ( n > 0 ) {
				dst += n;
			}

			dst	+= sprintf( ( char * )dst, "\"" );
		}

		dst	+= sprintf( ( char * )dst, "}}" );

		PacketPayload.PackedPayloadLen	= dst - PacketPayload.pPackedPayload + 1;
    } else {
    	PacketPayload.PackedPayloadLen	= 0;
    }

	return PacketPayload;
}

extern GenericPackedPayloadAlloc_t IPCam_ConvertDownStreamingPackedPayload( IPCamDownStreaming_t DownStreaming )
{
	GenericPackedPayloadAlloc_t PacketPayload;

	uint32_t data_size	= 128;

	if ( DownStreaming.Url && DownStreaming.UrlLength ) {
		data_size	= 128 + DownStreaming.UrlLength;
	}

	PacketPayload.pPackedPayload	= ( uint8_t * )DK_ALLOC( "DownStreaming", 1, data_size );

	if ( PacketPayload.pPackedPayload ) {
		memset( PacketPayload.pPackedPayload, 0, data_size );

		PacketPayload.PackedPayloadLen	= snprintf( ( char * )PacketPayload.pPackedPayload, data_size,
													"{\"DownStreaming\": {"
														"\"Action\": \"%s\","
														"\"Url\": \"%s\""
													"}}",
													( DownStreaming.Action == IPCAM_DOWN_STREAMING_ACTION_PLAY )? "Play": "Stop",
													( DownStreaming.Url && DownStreaming.UrlLength )? DownStreaming.Url: "" );

		PacketPayload.PackedPayloadLen ++;
	} else {
		PacketPayload.PackedPayloadLen	= 0;
	}

	return PacketPayload;
}

extern GeneralSizePackedPayload_t HomeKit_ConvertPowerStatusPacketPayload(ePowerStatus_t PowerStatus)
{
    GeneralSizePackedPayload_t PacketPayload;

    size_t size = sizeof(PowerStatus);

    PacketPayload.PackedPayloadLen = size;
    memcpy(PacketPayload.PackedPayload, &PowerStatus, size);

    return PacketPayload;
}

extern GeneralSizePackedPayload_t HomeKit_ConvertLightDimmerPacketPayload(DimmerValueInPercent_t Percentage)
{
    GeneralSizePackedPayload_t PacketPayload;

    size_t size = sizeof(Percentage);

    PacketPayload.PackedPayloadLen = size;
    memcpy(PacketPayload.PackedPayload, &Percentage, size);

    return PacketPayload;
}

extern AlarmSetArmingDelayHeader_t Alarm_ParseArmingDelayPackedPayload(uint16_t ArmingDelayPacketLen, void* ArmingDelayPayloadPacket)
{
    AlarmSetArmingDelayHeader_t ArmingDelays;

    memset((uint8_t *) &ArmingDelays, 0 , sizeof(AlarmSetArmingDelayHeader_t));

    uint8_t* pReadData = ArmingDelayPayloadPacket;
    uint16_t Ind = 0;

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        ArmingDelays.ArmingDelayDisarm = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        ArmingDelays.ArmingDelayHome = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        ArmingDelays.ArmingDelayArm = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    return ArmingDelays;
}

extern AlarmSetArmingDelayInfo_t Alarm_ParseArmingDelayPackedPayloadWithWhiteList(uint16_t ArmingDelayPacketLen, void* ArmingDelayPayloadPacket)
{
    AlarmSetArmingDelayInfo_t           ArmingDelayInfo;
    AlarmSetArmingDelayHeader_t         *pArmingDelays;
    AlarmArmingDelayWhiteListInstance_t *pWhiteList;

    memset((uint8_t *) &ArmingDelayInfo, 0 , sizeof(AlarmSetArmingDelayInfo_t));
    pArmingDelays = &ArmingDelayInfo.Header;
    pWhiteList = &ArmingDelayInfo.WhiteList;

    uint8_t* pReadData = ArmingDelayPayloadPacket;
    uint16_t Ind = 0;

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        pArmingDelays->ArmingDelayDisarm = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        pArmingDelays->ArmingDelayHome = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint16_t)))
    {
        pArmingDelays->ArmingDelayArm = *((uint16_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint16_t);

    if(ArmingDelayPacketLen >= (Ind + sizeof(uint8_t)))
    {
        pWhiteList->AmountOfWhiteList = *((uint8_t *) (pReadData + Ind));
    }
    Ind += sizeof(uint8_t);

    if(ArmingDelayPacketLen >= (Ind + (sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList)))
    {
        pWhiteList->Items = (AlarmArmingDelayWhiteListItem_t *) (pReadData + Ind);
    }
    Ind += (sizeof(AlarmArmingDelayWhiteListItem_t) * pWhiteList->AmountOfWhiteList);
    return ArmingDelayInfo;
}

extern AlarmArmingDelayWhiteListItem_t *Alarm_GetWhiteListItem(AlarmArmingDelayWhiteListInstance_t *pWhiteList, uint8_t index)
{
    if((pWhiteList->AmountOfWhiteList == 0) || (index >= pWhiteList->AmountOfWhiteList))
    {
        return NULL;
    }
    return &(pWhiteList->Items[index]);
}

extern void Alarm_ParsePhoneNumbersWithNamesPackedPayload(uint16_t PhoneNumbersPacketLen, void* PhoneNumbersPayloadPacket, AlarmPhoneNumberWithName_t *pPhoneNumbers)
{
    AlarmSetEmergencyCallPhoneNumbersHeader_t   SetEmergencyCallPhoneNumbersHeader;

    memset((uint8_t *) &SetEmergencyCallPhoneNumbersHeader, 0 , sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t));
    memset((uint8_t *) pPhoneNumbers, 0 , sizeof(AlarmPhoneNumberWithName_t));

    if (PhoneNumbersPayloadPacket && (sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t) <= PhoneNumbersPacketLen))
    {
        memcpy(&SetEmergencyCallPhoneNumbersHeader, PhoneNumbersPayloadPacket, sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t));
    }

    uint8_t     AmountofPhoneNumbers = SetEmergencyCallPhoneNumbersHeader.AmountofPhoneNumbers;
    uint16_t    TotalPhoneNumbersDataLength = SetEmergencyCallPhoneNumbersHeader.TotalPhoneNumbersDataLength;
    uint8_t     *pParseData = ((uint8_t *) PhoneNumbersPayloadPacket) + sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t);

    if ((sizeof(AlarmSetEmergencyCallPhoneNumbersHeader_t) + TotalPhoneNumbersDataLength) == PhoneNumbersPacketLen)
    {
        uint16_t i =0;
        pPhoneNumbers->AmountOfPhones = AmountofPhoneNumbers;
        for (i = 0; i < AmountofPhoneNumbers; i++)
        {
            uint8_t    PhoneNumberLen;

            pPhoneNumbers->ReferenceID[i] = *pParseData;
            pParseData += 1;
            PhoneNumberLen = *pParseData;
            pParseData += 1;
            memcpy(pPhoneNumbers->PhoneNumber[i], pParseData, PhoneNumberLen);
            pParseData += PhoneNumberLen;
            pPhoneNumbers->NameByteCount[i] = *pParseData;
            pParseData += 1;
            memcpy(pPhoneNumbers->Name[i], pParseData, pPhoneNumbers->NameByteCount[i]);
            pParseData += pPhoneNumbers->NameByteCount[i];
        }
    }

    return;
}


extern AlarmJobExecutionPtr_t Alarm_ParseJobExecutionPackedPayloadPtr(uint16_t JobExecutionPacketLen, void* JobExecutionPayloadPacket)
{
    AlarmJobExecutionUpdateHeader_t                 JobExecutionUpdateHeader;
    AlarmJobExecutionPtr_t                          JobExecution;

    memset((uint8_t *) &JobExecutionUpdateHeader, 0 , sizeof(AlarmJobExecutionUpdateHeader_t));
    memset((uint8_t *) &JobExecution, 0 , sizeof(AlarmJobExecutionPtr_t));

    if (JobExecutionPayloadPacket && (sizeof(AlarmJobExecutionUpdateHeader_t) <= JobExecutionPacketLen))
    {
        memcpy(&JobExecutionUpdateHeader, JobExecutionPayloadPacket, sizeof(AlarmJobExecutionUpdateHeader_t));
    }

    JobExecution.JobEnableLength = JobExecutionUpdateHeader.JobEnableLength;
    JobExecution.JobDisableLength = JobExecutionUpdateHeader.JobDisableLength;

    uint8_t     *pParseData = ((uint8_t *) JobExecutionPayloadPacket) + sizeof(AlarmJobExecutionUpdateHeader_t);

    if ((sizeof(AlarmJobExecutionUpdateHeader_t) + JobExecution.JobEnableLength + JobExecution.JobDisableLength) == JobExecutionPacketLen)
    {
        JobExecution.pJobEnable = pParseData;
        pParseData += JobExecution.JobEnableLength;
        JobExecution.pJobDisable = pParseData;
        pParseData += JobExecution.JobDisableLength;
    }

    return JobExecution;
}

extern AlarmPowerStateInfo_t Alarm_ParsePowerStatePackedPayload(uint16_t PowerStatePacketLen, void* PowerStatePayloadPacket)
{
    AlarmPowerStateInfo_t   PowerStateInfo;
    uint8_t                 *pParseData;

    memset((uint8_t *) &PowerStateInfo, 0 , sizeof(AlarmPowerStateInfo_t));

    pParseData = (uint8_t *) PowerStatePayloadPacket;

    if(pParseData && PowerStatePacketLen >= 3)
    {
        PowerStateInfo.Source = *(pParseData + 0);
        PowerStateInfo.BatteryState = *(pParseData + 1);
        PowerStateInfo.BatteryLevel = *(pParseData + 2);
    }

    return PowerStateInfo;
}

//returns eSimCardState_t
extern uint8_t Alarm_ParseSimState(uint16_t PowerStatePacketLen, void* PowerStatePayloadPacket)
{
    AlarmPowerStateInfo_t   PowerStateInfo;
    uint8_t                 *pParseData;
    uint16_t                Ind;
    uint8_t                 ret;

    memset((uint8_t *) &PowerStateInfo, 0 , sizeof(AlarmPowerStateInfo_t));

    pParseData = (uint8_t *) PowerStatePayloadPacket;

    Ind = sizeof(AlarmPowerStateInfo_t);
    ret = SIM_CARD_STATE_NODATA;
    while(pParseData && (PowerStatePacketLen > Ind))
    {
        GenericInfoHeader_t *pGenericInfoHeader;

        pGenericInfoHeader = (GenericInfoHeader_t *) (pParseData + Ind);
        if(pGenericInfoHeader->GenericInfoType == GI_ALARM_SIM_CARD_STATE)
        {
            ret = *(pParseData + Ind + sizeof(GenericInfoHeader_t));
        }
        Ind += (sizeof(GenericInfoHeader_t) + pGenericInfoHeader->InfoLen);
    }

    return ret;
}

extern AlarmConfigs_t Alarm_ParseConfigPackedPayload(uint16_t ConfigPacketLen, void* pConfigPayloadPacket)
{
    AlarmConfigs_t  AlarmConfigs;
    uint8_t         *pParseData;
    uint16_t        Ind;

    memset((uint8_t *) &AlarmConfigs, 0 , sizeof(AlarmConfigs_t));


    pParseData = (uint8_t *) pConfigPayloadPacket;
    Ind = 0;

    while (Ind < ConfigPacketLen)
    {
        GenericConfigHeader_t   *pGenericConfigHeader;
        pGenericConfigHeader = (GenericConfigHeader_t *) (pParseData + Ind);

        switch(pGenericConfigHeader->GenericConfigType)
        {
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Alert,          GC_ALARM_APPNOTIFICATION_ALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Sos,            GC_ALARM_APPNOTIFICATION_SOS)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Sabotage,       GC_ALARM_APPNOTIFICATION_SABOTAGE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Powersource,    GC_ALARM_APPNOTIFICATION_POWERSOURCE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Wifistate,      GC_ALARM_APPNOTIFICATION_WIFISTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Gsmstate,       GC_ALARM_APPNOTIFICATION_GSMSTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Silentalert,    GC_ALARM_APPNOTIFICATION_SILENTALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Disarmsabtg,    GC_ALARM_APPNOTIFICATION_DISARMSABTG)

            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Alert,              GC_ALARM_CELLULARSMS_ALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Sos,                GC_ALARM_CELLULARSMS_SOS)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Sabotage,           GC_ALARM_CELLULARSMS_SABOTAGE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Powersource,        GC_ALARM_CELLULARSMS_POWERSOURCE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Wifistate,          GC_ALARM_CELLULARSMS_WIFISTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Gsmstate,           GC_ALARM_CELLULARSMS_GSMSTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Silentalert,        GC_ALARM_CELLULARSMS_SILENTALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Disarmsabtg,        GC_ALARM_CELLULARSMS_DISARMSABTG)

            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Alert,             GC_ALARM_CELLULARCALL_ALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Sos,               GC_ALARM_CELLULARCALL_SOS)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Sabotage,          GC_ALARM_CELLULARCALL_SABOTAGE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Powersource,       GC_ALARM_CELLULARCALL_POWERSOURCE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Wifistate,         GC_ALARM_CELLULARCALL_WIFISTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Gsmstate,          GC_ALARM_CELLULARCALL_GSMSTATE)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Silentalert,       GC_ALARM_CELLULARCALL_SILENTALERT)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Disarmsabtg,       GC_ALARM_CELLULARCALL_DISARMSABTG)

            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Incoming_Call_Reject_Delay,     GC_ALARM_INCOMING_CALL_REJECT_DELAY)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Display_Off_Timing,             GC_ALARM_DISPLAY_OFF_TIMING)
            GcConfigCaseMacro(AlarmConfigs.Gc_Alarm_Outgoing_Call_Hangup_Delay,     GC_ALARM_OUTGOING_CALL_HANGUP_DELAY)
        }
        Ind += (sizeof(GenericConfigHeader_t) + pGenericConfigHeader->ConfigLen);
    }

    return AlarmConfigs;
}

extern IPCamStreamingConfigHeader_t IPCam_ParseStreamingConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamStreamingConfigHeader_t StreamingConfig = { 0, };

	StreamingConfig.Resolution	= IPCAM_RESOLUTION_1080P;
	StreamingConfig.FPS			= IPCAM_FPS_30;
	StreamingConfig.Bitrate		= IPCAM_BITRATE_1024KBPS;

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 32 );

		if ( root ) {
			json_value *items	= Json_FindChild( root, "StreamingConfig" );

			if ( items ) {
				double		value	= 0;

				if ( Json_FindChildNumber( items, "Resolution", &value ) == 0 ) {
					if ( value == 720 ) {
						StreamingConfig.Resolution	= IPCAM_RESOLUTION_720P;
					} else {
						StreamingConfig.Resolution	= IPCAM_RESOLUTION_1080P;
					}
				}

				if ( Json_FindChildNumber( items, "FrameRate", &value ) == 0 ) {
					if ( value == 12 ) {
						StreamingConfig.FPS	= IPCAM_FPS_12;
					} else if ( value == 15 ) {
						StreamingConfig.FPS	= IPCAM_FPS_15;
					} else if ( value == 24 ) {
						StreamingConfig.FPS	= IPCAM_FPS_24;
					} else {
						StreamingConfig.FPS	= IPCAM_FPS_30;
					}
				}

				if ( Json_FindChildNumber( items, "BitRate", &value ) == 0 ) {
					if ( value == 256 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_256KBPS;
					} else if ( value == 512 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_512KBPS;
					} else if ( value == 768 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_768KBPS;
					} else if ( value == 2048 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_768KBPS;
					} else if ( value == 3076 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_768KBPS;
					} else if ( value == 4096 ) {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_768KBPS;
					} else {
						StreamingConfig.Bitrate	= IPCAM_BITRATE_1024KBPS;
					}
				}
			}

			Json_Free( root );
		}
	}

	return StreamingConfig;
}

static void
ipcam_parser_event_request( json_value *items, IPCamEventRequestSettings_t *eq )
{
	double		value	= 0;

	eq->Avail	= 1;

	if ( Json_FindChildNumber( items, "DurationB", &value ) == 0 ) {
		eq->PreRecordDuration	= value;
		if ( eq->PreRecordDuration > 30 ) {
			eq->PreRecordDuration	= 30;
		}
	}

	if ( Json_FindChildNumber( items, "DurationA", &value ) == 0 ) {
		eq->PostRecordDuration	= value;
		if ( eq->PostRecordDuration > 30 ) {
			eq->PostRecordDuration	= 30;
		}
	}

	if ( Json_FindChildNumber( items, "Rule", &value ) == 0 ) {
		eq->Role	= value;
		if ( eq->Role > IPCAM_RECORDING_RULE_FLEXIBLE_BOTH ) {
			eq->Role	= IPCAM_RECORDING_RULE_FLEXIBLE_BOTH;
		}
	}
}

extern IPCamEventRequestHeader_t IPCam_ParseEventRequestPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamEventRequestHeader_t EventRequest;

    memset( &EventRequest, 0 , sizeof( IPCamEventRequestHeader_t ) );

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 32 );

		if ( root ) {
			json_value *items;

			items = Json_FindChild( root, "VideoEvt" );
			if ( items ) {
				ipcam_parser_event_request( items, &EventRequest.VideoEvt );
			}

			items = Json_FindChild( root, "JPEGEvt" );
			if ( items ) {
				ipcam_parser_event_request( items, &EventRequest.JPEGEvt );
			}

			Json_Free( root );
		}
	}

	return EventRequest;
}

static void
ipcam_parser_motion_active_window( json_value *array, IPCamMotionConfigHeader_t *MotionConfig )
{
	json_value *items	= array->v.child;
	double		value	= 0;

	MotionConfig->ActiveWindowLen	= 0;

	while ( items ) {
		MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinStartX	= 0;
		if ( Json_FindChildNumber( items, "WinStartX", &value ) == 0 ) {
			if ( ( value >= 0 ) && ( value <= 100 ) ) {
				MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinStartX	= value;
			}
		}

		MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinStartY	= 0;
		if ( Json_FindChildNumber( items, "WinStartY", &value ) == 0 ) {
			if ( ( value >= 0 ) && ( value <= 100 ) ) {
				MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinStartY	= value;
			}
		}

		MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinEndX	= 100;
		if ( Json_FindChildNumber( items, "WinEndX", &value ) == 0 ) {
			if ( ( value >= 0 ) && ( value <= 100 ) ) {
				MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinEndX	= value;
			}
		}

		MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinEndY	= 100;
		if ( Json_FindChildNumber( items, "WinEndY", &value ) == 0 ) {
			if ( ( value >= 0 ) && ( value <= 100 ) ) {
				MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].WinEndY	= value;
			}
		}

		MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].Sensitivity	= IPCAM_MOTION_SENSITIVITY_MIDDLE;
		if ( Json_FindChildNumber( items, "Sensitivity", &value ) == 0 ) {
			if ( value <= IPCAM_MOTION_SENSITIVITY_HIGH ) {
				MotionConfig->ActiveWindow[ MotionConfig->ActiveWindowLen ].Sensitivity	= value;
			}
		}

		items	= items->next;
		MotionConfig->ActiveWindowLen ++;

		if ( MotionConfig->ActiveWindowLen == IPCAM_MOTION_ACTIVE_WINDOW_MAX_COUNT ) {
			break;
		}
	}
}

extern IPCamMotionConfigHeader_t IPCam_ParseMotionConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamMotionConfigHeader_t MotionConfig = { 0, };

    memset( &MotionConfig, 0 , sizeof( IPCamMotionConfigHeader_t ) );

	MotionConfig.Mode				= IPCAM_MOTION_MODE_OFF;

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 64 );

		if ( root ) {
			json_value *items	= Json_FindChild( root, "MotionConfig" );

			if ( items ) {
				json_value *item;
				double		value	= 0;

				MotionConfig.Mode	= IPCAM_MOTION_MODE_OFF;
				if ( Json_FindChildNumber( items, "Mode", &value ) == 0 ) {
					if ( value <= IPCAM_MOTION_MODE_ON ) {
						MotionConfig.Mode	= value;
					}
				}

				item = Json_FindChild( items, "ActiveWindow" );
				if ( item ) {
					ipcam_parser_motion_active_window( item, &MotionConfig );
				} else {
					MotionConfig.ActiveWindowLen	= 0;
				}
			}

			Json_Free( root );
		}
	}

	return MotionConfig;
}

extern IPCamVideoConfigHeader_t IPCam_ParseVideoConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamVideoConfigHeader_t VideoConfig = { 0, };

	VideoConfig.UpSideDown	= IPCAM_UP_SIDE_DOWN_OFF;

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 32 );

		if ( root ) {
			json_value *items	= Json_FindChild( root, "VideoConfig" );

			if ( items ) {
				double		value	= 0;

				if ( Json_FindChildNumber( items, "UpSideDown", &value ) == 0 ) {
					if ( value == 1 ) {
						VideoConfig.UpSideDown	= IPCAM_UP_SIDE_DOWN_ON;
					} else {
						VideoConfig.UpSideDown	= IPCAM_UP_SIDE_DOWN_OFF;
					}
				}
			}

			Json_Free( root );
		}
	}

	return VideoConfig;
}

extern IPCamCaptureImage_t IPCam_ParseCaptureImagePackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamCaptureImage_t CaptureImage = { 0, };

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 32 );

		if ( root ) {
			json_value *items	= Json_FindChild( root, "CaptureImage" );

			if ( items ) {
				double		value	= 0;
				char		*string = NULL;

				if ( Json_FindChildNumber( items, "Resolution", &value ) == 0 ) {
					if ( value == 480 ) {
						CaptureImage.Resolution	= IPCAM_RESOLUTION_480;
					} else if ( value == 720 ) {
						CaptureImage.Resolution	= IPCAM_RESOLUTION_720P;
					} else {
						CaptureImage.Resolution	= IPCAM_RESOLUTION_1080P;
					}
				}

				if ( ( Json_FindChildString( items, "Format", &string ) == 0 ) && string ) {
					if ( strcmp( string, "JPEG" ) == 0 ) {
						CaptureImage.Format	= IPCAM_CAPTURE_IMAGE_FORMAT_JPEG;
					} else {
						CaptureImage.Format	= IPCAM_CAPTURE_IMAGE_FORMAT_JPEG;
					}
				}

				if ( ( Json_FindChildString( items, "Image", &string ) == 0 ) && string ) {
					uint32_t size = strlen( string );

					if ( size ) {
						CaptureImage.Image	= ( uint8_t * )DK_ALLOC( "", 1, size );

						if ( CaptureImage.Image ) {
							int n = b64_pton( string, CaptureImage.Image, size );

							if ( n > 0 ) {
								CaptureImage.ImageSize	= n;
							} else {
								DK_FREE( CaptureImage.Image );
								CaptureImage.Image = NULL;
							}
						}
					}
				}
			}

			Json_Free( root );
		}
	}

	return CaptureImage;
}

extern IPCamDownStreaming_t IPCam_ParseDownStreamingPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
	IPCamDownStreaming_t DownStreaming = { 0, };

	if ( PackedPayload ) {
		json_value *root	= Json_Parse( PackedPayload, PackedPayloadLen, NULL, 32 );

		if ( root ) {
			json_value *items	= Json_FindChild( root, "DownStreaming" );

			if ( items ) {
				char		*string = NULL;

				if ( ( Json_FindChildString( items, "Action", &string ) == 0 ) && string ) {
					if ( strcmp( string, "Play" ) == 0 ) {
						DownStreaming.Action	= IPCAM_DOWN_STREAMING_ACTION_PLAY;
					} else if ( strcmp( string, "Stop" ) == 0 ) {
						DownStreaming.Action	= IPCAM_DOWN_STREAMING_ACTION_STOP;
					} else {
						DownStreaming.Action	= IPCAM_DOWN_STREAMING_ACTION_STOP;
					}
				}

				if ( ( Json_FindChildString( items, "Url", &string ) == 0 ) && string ) {
					uint32_t size = strlen( string );

					if ( size ) {
						DownStreaming.Url	= ( char * )DK_ALLOC( "", 1, size + 1 );

						if ( DownStreaming.Url ) {
							DownStreaming.UrlLength	= sprintf( DownStreaming.Url, "%s", string );

							if ( DownStreaming.UrlLength == 0 ) {
								DK_FREE( DownStreaming.Url );
								DownStreaming.Url = NULL;
							}
						}
					}
				}
			}

			Json_Free( root );
		}
	}

	return DownStreaming;
}

/* HomeKit */
extern ePowerStatus_t HomeKit_ParsePowerStatusPacketPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
    ePowerStatus_t PowerStatus = POWER_STATUS_UNKNOWN;
    ePowerStatus_t *pParseData;

    pParseData = (ePowerStatus_t *) PackedPayload;

    if (pParseData && PackedPayloadLen >= sizeof(PowerStatus))
    {
        PowerStatus = *(pParseData + 0);
    }

    return PowerStatus;
}

extern DimmerValueInPercent_t HomeKit_ParseLightDimmerPacketPayload( uint16_t PackedPayloadLen, void *PackedPayload )
{
    DimmerValueInPercent_t Dimmer;
    DimmerValueInPercent_t *pParseData;

    pParseData = (DimmerValueInPercent_t *) PackedPayload;

    if (pParseData && PackedPayloadLen >= sizeof(Dimmer))
    {
        Dimmer = *(pParseData + 0);
    }

    return Dimmer;
}

extern bool Alarm_CheckConfigPackedPayload(uint16_t ConfigPacketLen, void* pConfigPayloadPacket)
{
    AlarmConfigs_t  AlarmConfigs;
    uint8_t         *pParseData;
    uint16_t        Ind;
    bool            bAllMatch;

    memset((uint8_t *) &AlarmConfigs, 0 , sizeof(AlarmConfigs_t));

    pParseData = (uint8_t *) pConfigPayloadPacket;
    Ind = 0;
    bAllMatch = true;

    while (Ind < ConfigPacketLen)
    {
        GenericConfigHeader_t   *pGenericConfigHeader;
        pGenericConfigHeader = (GenericConfigHeader_t *) (pParseData + Ind);

        switch(pGenericConfigHeader->GenericConfigType)
        {
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Alert,           GC_ALARM_APPNOTIFICATION_ALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Sos,             GC_ALARM_APPNOTIFICATION_SOS)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Sabotage,        GC_ALARM_APPNOTIFICATION_SABOTAGE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Powersource,     GC_ALARM_APPNOTIFICATION_POWERSOURCE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Wifistate,       GC_ALARM_APPNOTIFICATION_WIFISTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Gsmstate,        GC_ALARM_APPNOTIFICATION_GSMSTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Silentalert,     GC_ALARM_APPNOTIFICATION_SILENTALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Appnotification_Disarmsabtg,     GC_ALARM_APPNOTIFICATION_DISARMSABTG)

            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Alert,               GC_ALARM_CELLULARSMS_ALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Sos,                 GC_ALARM_CELLULARSMS_SOS)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Sabotage,            GC_ALARM_CELLULARSMS_SABOTAGE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Powersource,         GC_ALARM_CELLULARSMS_POWERSOURCE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Wifistate,           GC_ALARM_CELLULARSMS_WIFISTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Gsmstate,            GC_ALARM_CELLULARSMS_GSMSTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Silentalert,         GC_ALARM_CELLULARSMS_SILENTALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularsms_Disarmsabtg,         GC_ALARM_CELLULARSMS_DISARMSABTG)

            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Alert,              GC_ALARM_CELLULARCALL_ALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Sos,                GC_ALARM_CELLULARCALL_SOS)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Sabotage,           GC_ALARM_CELLULARCALL_SABOTAGE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Powersource,        GC_ALARM_CELLULARCALL_POWERSOURCE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Wifistate,          GC_ALARM_CELLULARCALL_WIFISTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Gsmstate,           GC_ALARM_CELLULARCALL_GSMSTATE)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Silentalert,        GC_ALARM_CELLULARCALL_SILENTALERT)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Cellularcall_Disarmsabtg,        GC_ALARM_CELLULARCALL_DISARMSABTG)

            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Incoming_Call_Reject_Delay,      GC_ALARM_INCOMING_CALL_REJECT_DELAY)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Display_Off_Timing,              GC_ALARM_DISPLAY_OFF_TIMING)
            GcCheckCaseMacro(AlarmConfigs.Gc_Alarm_Outgoing_Call_Hangup_Delay,      GC_ALARM_OUTGOING_CALL_HANGUP_DELAY)
        }
        if(bAllMatch == false)
        {
            break;
        }
        Ind += (sizeof(GenericConfigHeader_t) + pGenericConfigHeader->ConfigLen);
    }

    return bAllMatch;
}

extern bool Alarm_CheckIsGsmRequired(AlarmConfigs_t *AlarmConfigs)
{
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Alert);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Sos);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Sabotage);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Powersource);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Wifistate);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Gsmstate);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Silentalert);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularsms_Disarmsabtg);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Alert);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Sos);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Sabotage);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Powersource);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Wifistate);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Gsmstate);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Silentalert);
    GcCheckIfConfigEnabled(AlarmConfigs->Gc_Alarm_Cellularcall_Disarmsabtg);

    return false;
}

extern void Alarm_FreePackedPayload(AlarmPackedPayloadAlloc_t AlarmPackedPayloadAlloc)
{
    if (AlarmPackedPayloadAlloc.pPackedPayload)
    {
        DK_FREE(AlarmPackedPayloadAlloc.pPackedPayload);
		AlarmPackedPayloadAlloc.pPackedPayload = NULL;
    }
}

extern void Generic_FreePackedPayload( GenericPackedPayloadAlloc_t PackedPayload )
{
    if ( PackedPayload.pPackedPayload ) {
        DK_FREE( PackedPayload.pPackedPayload );
    }
}

