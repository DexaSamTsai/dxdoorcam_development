/**
 * Copyright (C) 2018 Dexatek Technology Ltd.
 *
 * dk_PeripheralTaiSEIA_HeatExchanger.c
 *
 * This is proprietary information of Dexatek Technology Ltd. All Rights
 * Reserved. Reproduction of this documentation or the accompanying programs in
 * any manner whatsoever without the written permission of Dexatek Technology
 * Ltd. is strictly forbidden.
 */

#include "dk_PeripheralTaiSEIA_HeatExchanger.h"
#include "Utils.h"

#define TO_U16(high, low)           (((uint16_t)(high) << 8) | (uint16_t)(low));
#define BIT_0                       (1 << 0)
#define BIT_1                       (1 << 1)
#define BIT_2                       (1 << 2)
#define BIT_3                       (1 << 3)
#define BIT_4                       (1 << 4)
#define BIT_5                       (1 << 5)
#define BIT_6                       (1 << 6)
#define BIT_7                       (1 << 7)
#define BIT_8                       (1 << 8)
#define BIT_9                       (1 << 9)
#define BIT_10                      (1 << 10)
#define BIT_11                      (1 << 11)
#define BIT_12                      (1 << 12)
#define BIT_13                      (1 << 13)
#define BIT_14                      (1 << 14)
#define BIT_15                      (1 << 15)


/**
 * API for available services
 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_POWER_CONTROL, H'00 */

TaiseiaHeatExchangerSupportedPowerControl_t TaiseiaHeatExchangerGetSupportedPowerControl(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedPowerControl_t control = { 0 };

    control.IsWritable = service.ServiceIsWritable;
    control.PowerOn = service.DataLowByte & BIT_0;
    control.PowerOff = service.DataLowByte & BIT_1;

    return control;
}

eTaiseiaHeatExchangerPowerControl TaiseiaHeatExchangerGetConvertedPowerControl(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerPowerControl powerControl = TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_UNKNOWN;
    uint16_t data = TO_U16(service.DataHighByte, service.DataLowByte);

    switch (data) {
    case 0:
        powerControl = TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_OFF;
        break;
    case 1:
        powerControl = TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_ON;
        break;
    default:
        break;
    }

    return powerControl;
}

eTaiseiaHeatExchangerPowerControl TaiseiaHeatExchangerParsePowerControlServiceAccess(TaiSEIAServiceAccess_t ServiceAccess)
{
    eTaiseiaHeatExchangerPowerControl PowerControl = TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_UNKNOWN;
    
    switch (ServiceAccess.DataLowByte) {
        case 0x00:
        {
            PowerControl =  TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_OFF;
        }
            break;
        case 0x01:
        {
            PowerControl =  TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_ON;
        }
            break;
        default:
            break;
    }
    
    return PowerControl;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreatePowerControlPayload(eTaiseiaHeatExchangerPowerControl state)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_POWER_CONTROL;
    service.DataHighByte = 0x00;

    switch (state) {
    case TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_OFF:
        service.DataLowByte = 0x00;
        break;
    case TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_ON:
        service.DataLowByte = 0x01;
        break;
    default:
        break;
    }

    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);

    return payload;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_MODE, H'01 */

TaiseiaHeatExchangerSupportedOperationMode_t TaiseiaHeatExchangerGetSupportedOperationMode(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedOperationMode_t mode = { 0 };

    mode.IsWritable = service.ServiceIsWritable;
    mode.AirCondition = service.DataLowByte & BIT_0;
    mode.Dehumidification = service.DataLowByte & BIT_1;
    mode.AirSupply = service.DataLowByte & BIT_2;
    mode.Auto = service.DataLowByte & BIT_3;
    mode.Heater = service.DataLowByte & BIT_4;

    return mode;
}

eTaiseiaHeatExchangerOperationMode TaiseiaHeatExchangerGetConvertedOperationMode(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerOperationMode mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_UNKNOWN;
    uint16_t data = TO_U16(service.DataHighByte, service.DataLowByte);

    switch (data) {
    case 0:
        mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_CONDITION;
        break;
    case 1:
        mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_DEHUMIDIFICATION;
        break;
    case 2:
        mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_SUPPLY;
        break;
    case 3:
        mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_AUTO;
        break;
    case 4:
        mode = TAISEIA_HEAT_EXCHANGER_OP_MODE_HEATER;
        break;
    default:
        break;
    }

    return mode;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateOperationModePayload(eTaiseiaHeatExchangerOperationMode mode)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_MODE;
    service.DataHighByte = 0x00;

    switch (mode) {
    case TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_CONDITION:
        service.DataLowByte = 0x00;
        break;
    case TAISEIA_HEAT_EXCHANGER_OP_MODE_DEHUMIDIFICATION:
        service.DataLowByte = 0x01;
        break;
    case TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_SUPPLY:
        service.DataLowByte = 0x02;
        break;
    case TAISEIA_HEAT_EXCHANGER_OP_MODE_AUTO:
        service.DataLowByte = 0x03;
        break;
    case TAISEIA_HEAT_EXCHANGER_OP_MODE_HEATER:
        service.DataLowByte = 0x04;
        break;
    default:
        break;
    }

    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);

    return payload;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_WIND_SPEED_SETTING, H'02 */

TaiseiaHeatExchangerSupportedWindSpeed_t TaiseiaHeatExchangerGetSupportedWindSpeed(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedWindSpeed_t windSpeed = { 0 };
    uint16_t data = TO_U16(service.DataHighByte, service.DataLowByte);

    windSpeed.IsWritable = service.ServiceIsWritable;
    windSpeed.Speed.bits.Level0_b0 = (data & BIT_0) >> 0;
    windSpeed.Speed.bits.Level1_b1 = (data & BIT_1) >> 1;
    windSpeed.Speed.bits.Level2_b2 = (data & BIT_2) >> 2;
    windSpeed.Speed.bits.Level3_b3 = (data & BIT_3) >> 3;
    windSpeed.Speed.bits.Level4_b4 = (data & BIT_4) >> 4;
    windSpeed.Speed.bits.Level5_b5 = (data & BIT_5) >> 5;
    windSpeed.Speed.bits.Level6_b6 = (data & BIT_6) >> 6;
    windSpeed.Speed.bits.Level7_b7 = (data & BIT_7) >> 7;
    windSpeed.Speed.bits.Level8_b8 = (data & BIT_8) >> 8;
    windSpeed.Speed.bits.Level9_b9 = (data & BIT_9) >> 9;
    windSpeed.Speed.bits.Level10_b10 = (data & BIT_10) >> 10;
    windSpeed.Speed.bits.Level11_b11 = (data & BIT_11) >> 11;
    windSpeed.Speed.bits.Level12_b12 = (data & BIT_12) >> 12;
    windSpeed.Speed.bits.Level13_b13 = (data & BIT_13) >> 13;
    windSpeed.Speed.bits.Level14_b14 = (data & BIT_14) >> 14;
    windSpeed.Speed.bits.Level15_b15 = (data & BIT_15) >> 15;

    return windSpeed;
}

eTaiseiaHeatExchangerWindSpeed TaiseiaHeatExchangerGetConvertedWindSpeed(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerWindSpeed speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_UNKNOWN;
    uint16_t data = TO_U16(service.DataHighByte, service.DataLowByte);

    switch (data) {
    case 0:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_AUTO;
        break;
    case 1:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_1;
        break;
    case 2:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_2;
        break;
    case 3:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_3;
        break;
    case 4:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_4;
        break;
    case 5:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_5;
        break;
    case 6:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_6;
        break;
    case 7:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_7;
        break;
    case 8:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_8;
        break;
    case 9:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_9;
        break;
    case 10:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_10;
        break;
    case 11:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_11;
        break;
    case 12:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_12;
        break;
    case 13:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_13;
        break;
    case 14:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_14;
        break;
    case 15:
        speed = TAISEIA_HEAT_EXCHANGER_WIND_SPEED_15;
        break;
    default:
        break;
    }

    return speed;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateWindSpeedPayload(eTaiseiaHeatExchangerWindSpeed speed)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_WIND_SPEED_SETTING;
    service.DataHighByte = 0x00;

    switch (speed) {
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_AUTO:
        service.DataLowByte = 0;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_1:
        service.DataLowByte = 1;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_2:
        service.DataLowByte = 2;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_3:
        service.DataLowByte = 3;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_4:
        service.DataLowByte = 4;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_5:
        service.DataLowByte = 5;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_6:
        service.DataLowByte = 6;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_7:
        service.DataLowByte = 7;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_8:
        service.DataLowByte = 8;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_9:
        service.DataLowByte = 9;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_10:
        service.DataLowByte = 10;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_11:
        service.DataLowByte = 11;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_12:
        service.DataLowByte = 12;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_13:
        service.DataLowByte = 13;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_14:
        service.DataLowByte = 14;
        break;
    case TAISEIA_HEAT_EXCHANGER_WIND_SPEED_15:
        service.DataLowByte = 15;
        break;
    default:
        break;
    }

    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);

    return payload;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TEMPERATURE_SETTING, H'03 */

TaiseiaHeatExchangerSupportedTemperatureSetting_t TaiseiaHeatExchangerSupportedTemperatureSetting(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedTemperatureSetting_t setting = { 0 };

    setting.IsWritable = service.ServiceIsWritable;
    setting.Minimum = service.DataHighByte;
    setting.Maximum = service.DataLowByte;

    return setting;
}

int8_t TaiseiaHeatExchangerGetConvertedTemperatureSetting(TaiSEIAServiceInfo_t service)
{
    return TaiSEIA_GetInt8Value(service.DataHighByte, service.DataLowByte);
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateTemperatureSettingPayload(int8_t setting)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TEMPERATURE_SETTING;
    service.DataHighByte = 0x00;
    service.DataLowByte = (uint8_t)setting;

    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);

    return payload;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_RETURN_AIR_TEMPERATURE, H'04 */

TaiseiaHeatExchangerSupportedIndoorTemperature_t TaiseiaHeatExchangerSupportedIndoorTemperature(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedIndoorTemperature_t temperature = { 0 };

    temperature.IsWritable = service.ServiceIsWritable;
    temperature.Minimum = service.DataHighByte;
    temperature.Maximum = service.DataLowByte;

    return temperature;
}

int8_t TaiseiaHeatExchangerGetConvertedIndoorTemperature(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetInt8Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OUTSIDE_AIR_TEMPERATURE, H'05 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TIMER_SETTING, H'06 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_SETTINGS_CHECKING, H'07 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_INDOOR_MODEL, H'08 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_MESSAGE, H'09 */

TaiseiaHeatExchangerSupportedErrorMessage_t TaiseiaHeatExchangerSupportedErrorMessage(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedErrorMessage_t msg = { 0 };

    msg.IsWritable = service.ServiceIsWritable;
    msg.Code.bits.Normal_b0 = (service.DataLowByte >> 0);
    msg.Code.bits.Error1_b1 = (service.DataLowByte >> 1);
    msg.Code.bits.Error2_b2 = (service.DataLowByte >> 2);
    msg.Code.bits.Error3_b3 = (service.DataLowByte >> 3);
    msg.Code.bits.Error4_b4 = (service.DataLowByte >> 4);
    msg.Code.bits.Error5_b5 = (service.DataLowByte >> 5);
    msg.Code.bits.Error6_b6 = (service.DataLowByte >> 6);
    msg.Code.bits.Error7_b7 = (service.DataLowByte >> 7);
    msg.Code.bits.Error8_b8 = (service.DataHighByte >> 0);
    msg.Code.bits.Error9_b9 = (service.DataHighByte >> 1);
    msg.Code.bits.Error10_b10 = (service.DataHighByte >> 2);
    msg.Code.bits.Error11_b11 = (service.DataHighByte >> 3);
    msg.Code.bits.Error12_b12 = (service.DataHighByte >> 4);
    msg.Code.bits.Error13_b13 = (service.DataHighByte >> 5);
    msg.Code.bits.Error14_b14 = (service.DataHighByte >> 6);
    msg.Code.bits.Error15_b15 = (service.DataHighByte >> 7);

    return msg;
}

TaiseiaHeatExchangerErrorMessage_u TaiseiaHeatExchangerGetConvertedErrorMessage(TaiSEIAServiceInfo_t service)
{
    TaiseiaHeatExchangerErrorMessage_u msg = { 0 };

    msg.flag = (((uint16_t)service.DataHighByte) << 8) + (uint16_t)service.DataLowByte;

    return msg;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_CURRENT, H'0A */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_VOLTAGE, H'0B */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_POWER_FACTOR, H'0C */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_CURRENT_POWER, H'0D */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_AGGREGATE_POWER_CONSUMPTION, H'0E */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_1, H'0F */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_2, H'10 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_3, H'11 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_4, H'12 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_5, H'13 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_FILTER_CLEANING_NOTIFICATION, H'14 */
TaiseiaHeatExchangerSupportedFilterCleaningNotification_t TaiseiaHeatExchangerGetSupportedFilterCleaningNotification(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedFilterCleaningNotification_t notification = { 0 };

    notification.IsWritable = service.ServiceIsWritable;
    notification.Off = ((service.DataLowByte >> 0) & 1);
    notification.On = ((service.DataLowByte >> 1) & 1);

    return notification;
}

eTaiseiaHeatExchangerFilterCleaningNotification TaiseiaHeatExchangerGetConvertedFilterCleaningNotification(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerFilterCleaningNotification notification = TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_UNKNOWN;

    switch (service.DataLowByte) {
    case 0:
        notification = TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_OFF;
        break;
    case 1:
        notification = TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_ON;
        break;
    default:
        break;
    }

    return notification;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateFilterCleaningNotificationPayload(eTaiseiaHeatExchangerFilterCleaningNotification notification)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_FILTER_CLEANING_NOTIFICATION;
    service.DataHighByte = 0x00;
    
    switch (notification) {
    case TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_OFF:
        service.DataLowByte = 0x00;
        break;
    case TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_ON:
        service.DataLowByte = 0x01;
        break;
    default:
        break;
    }
    
    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);
    
    return payload;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateUsedHourResetAfterCleanFilterPacketPayload(void)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    
    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    ServiceAccess.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_FILTER_CLEANING_NOTIFICATION;
    ServiceAccess.DataHighByte = 0x00;
    ServiceAccess.DataLowByte = 0x00;
    
    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);
    
    return Payload;
}


/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_VENTILATION_MODE, H'15 */

TaiseiaHeatExchangerSupportedVentilationMode_t TaiseiaHeatExchangerGetSupportedVentilationMode(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedVentilationMode_t mode = { 0 };

    mode.IsWritable = service.ServiceIsWritable;
    mode.Auto = ((service.DataLowByte >> 0) & 1);
    mode.EnergyRecovery = ((service.DataLowByte >> 1) & 1);
    mode.Normal = ((service.DataLowByte >> 2) & 1);

    return mode;
}

eTaiseiaHeatExchangerVentilationMode TaiseiaHeatExchangerGetConvertedVentilationMode(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerVentilationMode mode = TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_UNKNOWN;

    switch (service.DataLowByte) {
        case 0:
            mode = TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_AUTO;
            break;
        case 1:
            mode = TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_ENERGY_RECOVERY;
            break;
        case 2:
            mode = TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_NORMAL;
            break;
        default:
            break;
    }

    return mode;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateVentilationModePayload(eTaiseiaHeatExchangerVentilationMode mode)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_VENTILATION_MODE;
    service.DataHighByte = 0x00;

    switch (mode) {
    case TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_AUTO:
        service.DataLowByte = 0x00;
        break;
    case TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_ENERGY_RECOVERY:
        service.DataLowByte = 0x01;
        break;
    case TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_NORMAL:
        service.DataLowByte = 0x02;
        break;
    default:
        break;
    }
    
    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);
    
    return payload;
}

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_PREHEAT_PRECOOL, H'16 */

TaiseiaHeatExchangerSupportedPreheatPrecool_t TaiseiaHeatExchangerGetSupportedPreheatPrecool(TaiSEIASupportedServiceInfo_t service)
{
    TaiseiaHeatExchangerSupportedPreheatPrecool_t setting = { 0 };

    setting.IsWritable = service.ServiceIsWritable;
    setting.NoDelay = ((service.DataLowByte >> 0) & 1);
    setting.Delay30Min = ((service.DataLowByte >> 1) & 1);
    setting.Delay60Min = ((service.DataLowByte >> 2) & 1);

    return setting;
}

eTaiseiaHeatExchangerPreheatPrecool TaiseiaHeatExchangerGetConvertedPreheatPrecool(TaiSEIAServiceInfo_t service)
{
    eTaiseiaHeatExchangerPreheatPrecool setting = TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_UNKNOWN;

    switch (service.DataLowByte) {
        case 0:
            setting = TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_NO_DELAY;
            break;
        case 1:
            setting = TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_30_MIN;
            break;
        case 2:
            setting = TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_60_MIN;
            break;
        default:
            break;
    }

    return setting;
}

TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreatePreheatPrecoolPayload(eTaiseiaHeatExchangerPreheatPrecool setting)
{
    TaiSEIAServiceAccess_t service = { 0 };
    TaiSEIAPackedPayload_t payload = { 0 };

    service.AccessWrite = 1;
    service.SATypeID = TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER;
    service.ServiceID = TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_PREHEAT_PRECOOL;
    service.DataHighByte = 0x00;

    switch (setting) {
    case TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_NO_DELAY:
        service.DataLowByte = 0x00;
        break;
    case TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_30_MIN:
        service.DataLowByte = 0x01;
        break;
    case TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_60_MIN:
        service.DataLowByte = 0x02;
        break;
    default:
        break;
    }
    
    payload = TaiSEIA_ConvertAccessWritePacketPayload(service);
    
    return payload;
}
