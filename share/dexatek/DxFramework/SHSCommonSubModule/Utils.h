//============================================================================
// File: Utils.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _UTILS_H
#define _UTILS_H

#pragma once

#ifdef DK_GATEWAY
#include "dxOS.h"
#include "dxWIFI.h"
#include "dxSysDebugger.h"
#include "dk_Peripheral.h"
#endif

#include "dk_Job.h"
#include "dk_hex.h"

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************
 *                      Defines
 ******************************************************/

#define GENERAL_MEM_ALLOC_SIZE_PACK_ALIGNMENT			32   //Byte alignment

#define NULL_STRING			'\0'

/******************************************************
 *                      Macros
 ******************************************************/

#define BUILD_UINT16(loByte, hiByte) \
          ((uint16_t)(((loByte) & 0x00FF) + (((hiByte) & 0x00FF) << 8)))

#ifdef DK_GATEWAY
#define DK_ALLOC(name, nelems, elemsize) mem_calloc_AlignmentPack(name,nelems,elemsize);
#else
#define DK_ALLOC(name, nelems, elemsize) calloc(nelems,elemsize);
#endif
#define DK_FREE(buf) free(buf);


/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

//NOTE: DKTime_t is already defined in dxOS.h but in case it is needed
//      from other places (non gateway).
#ifndef DK_GATEWAY

typedef struct _tagDKTime
{
    uint16_t        year;       //Actual year, eg. 2017
    uint8_t         month;
    uint8_t         day;
    uint8_t         hour;       //24Hours
    uint8_t         min;
    uint8_t         sec;
    uint8_t         dayofweek;  //1 - Monday, 2 - Tuesday......
} DKTime_t;

typedef struct _tagDKTimeLocal
{
    uint16_t        year;
    uint8_t         month;
    uint8_t         day;
    uint8_t         hour;
    uint8_t         min;
    uint8_t         sec;
    uint8_t         dayofweek;
} DKTimeLocal_t;

#endif

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/*******************************************************************************
 * crc16 KERMIT polynomial 8408
 * ****************************************************************************
 */
extern uint16_t crc16_ccitt_kermit(uint8_t *pData, uint16_t len);


/*******************************************************************************
 * crc16
 *
 * Computes a crc16 over the input data using the polynomial:
 *
 *       x^16 + x^12 +x^5 + 1
 *
 * The caller provides the initial value (either CRC16_INIT_VALUE
 * or the previous returned value) to allow for processing of
 * discontiguous blocks of data.  When generating the CRC the
 * caller is responsible for complementing the final return value
 * and inserting it into the byte stream.  When checking, a final
 * return value of CRC16_GOOD_VALUE indicates a valid CRC.
 *
 * Reference: Dallas Semiconductor Application Note 27
 *   Williams, Ross N., "A Painless Guide to CRC Error Detection Algorithms",
 *     ver 3, Aug 1993, ross@guest.adelaide.edu.au, Rocksoft Pty Ltd.,
 *     ftp://ftp.rocksoft.com/clients/rocksoft/papers/crc_v3.txt
 *
 * ****************************************************************************
 */

extern uint16_t Crc16(
	uint8_t *pdata,  	/* pointer to array of data to process */
	int16_t nbytes, 	/* number of input data bytes to process */
	uint16_t crc     	/* either CRC16_INIT_VALUE or previous return value */
);


/*******************************************************************************
 * Update crc16 polynomial 8005
 * ****************************************************************************
 */
extern uint16_t crc16_P8005_update(uint16_t crc, uint8_t a);


/*******************************************************************************
 * Check for valid frame, computes from pkt[0] to pkt[len-3] inclusive
 * ****************************************************************************
 */
extern uint8_t crc16_P8005_check(uint8_t* pkt, uint8_t len, uint16_t Seed);

/*******************************************************************************
 * Appends a CRC at the end of the frame, at pkt[len - 1] and pkt[len - 2]
 * ****************************************************************************
 */
extern void crc16_P8005_append(uint8_t* pkt, uint8_t len, uint16_t Seed);

/*******************************************************************************
 * Generate Access Key by Mac Address.
 * ****************************************************************************
 */
extern int
UtilsConvertMacToGMac( uint8_t mac[ 6 ], uint8_t gmac[ 8 ] );

/*******************************************************************************
 * Get the size of variant value type (eConditionValueType)
 * ****************************************************************************
 */
extern uint8_t UtilsGetValueTypeSize(uint8_t ValueType);

/*******************************************************************************
 * Challenge 2 variant value given operator (eConditionOperator)
 * ****************************************************************************
 */
extern int8_t UtilsValueTypeChallengeWithOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp, uint8_t Operator);

/*******************************************************************************
 * Conver Mac address to decimal string
 * ****************************************************************************
 */
extern int _MACAddrToDecimalStr(char *buffer, int cbSize, uint8_t *macaddr, int macaddrlen);

/*******************************************************************************
 * Decimal string to mac address
 * ****************************************************************************
 */
extern int _DecimalStrToMACAddr(uint8_t *macaddr, int macaddrlen, char *data);

/*******************************************************************************
 * Uint64 to decimal string
 * ****************************************************************************
 */
extern int _UINT64ToDecimalStr(char *buffer, int cbSize, uint64_t nU64Value);

/*******************************************************************************
 * Compare UUID (128bit) between string and array
 * Return 1 = Match exactly
 * Return 0 = Does not match
 * ****************************************************************************
 */
extern int UtilsUUIDStrArrayCompare(uint8_t UUID128[16], uint8_t* StrUUID128);

/*******************************************************************************
 * Available for Gateway compilation only
 * ****************************************************************************
 */
#ifdef DK_GATEWAY

extern int32_t APSecurityTypeTranslateFromDxToApp(int32_t dxAPSecurityType);

extern int32_t APSecurityTypeTranslateFromAppToDx(int32_t dxAPSecurityType);

/* NOTE!! This function is intended for use within SHSCommonSubModule's file ONLY
 * Please use dxMemAlloc instead for pack alignment if needed outside of SHSCommonSubModule's file.
 * Pack aligment base on the value defined by GENERAL_MEM_ALLOC_SIZE_PACK_ALIGNMENT */
extern void*    mem_calloc_AlignmentPack(const char* name, size_t nelems, size_t elemsize);


#endif //DK_GATEWAY

/*******************************************************************************
 * Convert from UTC (in seconds) to our DKTime_t
 * ****************************************************************************
 */
extern DKTime_t ConvertUTCToDKTime(uint32_t utcsec);

/*******************************************************************************
 * Convert from UTC (in seconds) to our DKTimeLocal_t
 * ****************************************************************************
 */
extern DKTimeLocal_t ConvertUTCToDKTimeLocal (uint32_t utcsec);

/*******************************************************************************
 * Convert from DkTime_t to UTC in Seconds (uint32_t)
 * ****************************************************************************
 */
extern uint32_t ConvertDKTimeToUTC(DKTime_t dktime);

/*******************************************************************************
 * Convert from DkTimeLocal_t to UTC in Seconds (uint32_t)
 * ****************************************************************************
 */
extern uint32_t ConvertDKTimeLocalToUTC(DKTimeLocal_t localTime);

#ifdef __cplusplus
}
#endif


#endif //_UTILS_H
