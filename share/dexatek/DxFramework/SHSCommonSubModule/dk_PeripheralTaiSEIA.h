//============================================================================
// File: dk_peripheralTaiSEIA.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef BLE_PERIPHERAL_TAISEIA_INFO_H
#define BLE_PERIPHERAL_TAISEIA_INFO_H

#pragma once

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "dk_Peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Defines
 ******************************************************/

// Used to denote input parameter
#ifndef IN
#define IN
#endif

// Used to denote output parameter
#ifndef OUT
#define OUT
#endif

#define TAISEIA_SA_TYPE_ID_GENERIC              0x00
#define TAISEIA_SA_TYPE_ID_AIR_CON              0x01
#define TAISEIA_SA_TYPE_ID_DEHUMIDIFIER         0x04
#define TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER       0x0E

#define TAISEIA_GENERIC_SERVICE_ID_REGISTRATION         0x00    //This include feedback of all info such as brand name, supported feature list, etc..
#define TAISEIA_GENERIC_SERVICE_ID_SUPPORTED_FEATURE    0x07    //NOT used as we get Registration to obtain feature list
#define TAISEIA_GENERIC_SERVICE_ID_ALL_CURRENT_STATUS   0x08

/* Generic Engineering mode Service ID */
#define TAISEIA_ENGINEERING_ID_PAIRING_SETTING                          0x52

/* Air Condition Service ID */
#define TAISEIA_AIRCON_SERVICE_ID_POWER_CONTROL                         0x00
#define TAISEIA_AIRCON_SERVICE_ID_MODE_CONTROL                          0x01
#define TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL                    0x02
#define TAISEIA_AIRCON_SERVICE_ID_TEMPERATURE_SETTING                   0x03
#define TAISEIA_AIRCON_SERVICE_ID_INDOOR_TEMPERATURE                    0x04
#define TAISEIA_AIRCON_SERVICE_ID_SLEEP_MODE_TIMER_SETTING              0x06
#define TAISEIA_AIRCON_SERVICE_ID_BOOT_TIMER_SETTING                    0x0B
#define TAISEIA_AIRCON_SERVICE_ID_SHUTDOWN_TIMER_SETTING                0x0C
#define TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_SWINGABLE_CONTROL       0x0E
#define TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL 0x0F
#define TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL     0x11
#define TAISEIA_AIRCON_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL           0x12
#define TAISEIA_AIRCON_SERVICE_ID_INDOOR_HUMIDITY                       0x14
#define TAISEIA_AIRCON_SERVICE_ID_MOLD_PREVENT_SETTING                  0x17
#define TAISEIA_AIRCON_SERVICE_ID_FAST_OPERATION_SETTING                0x1A
#define TAISEIA_AIRCON_SERVICE_ID_ENERGY_SAVING_SETTING                 0x1B
#define TAISEIA_AIRCON_SERVICE_ID_CONTROLLER_PROHIBITED_SETTING         0x1D
#define TAISEIA_AIRCON_SERVICE_ID_SAA_VOICE_CONTROL                     0x1E
#define TAISEIA_AIRCON_SERVICE_ID_HOST_DISPLAY_BRIGHTNESS_SETTING       0x1F
#define TAISEIA_AIRCON_SERVICE_ID_HUMIDIFIER_SETTING                    0x20
#define TAISEIA_AIRCON_SERVICE_ID_OUTDOOR_TEMPERATURE                   0x21
#define TAISEIA_AIRCON_SERVICE_ID_OUTDOOR_HOST_CURRENTS                 0x24
#define TAISEIA_AIRCON_SERVICE_ID_OUTDOOR_HOST_ACCUMULATION_KWH         0x28
#define TAISEIA_AIRCON_SERVICE_ID_ERROR_CODE                            0x29
#define TAISEIA_AIRCON_SERVICE_ID_HOURS_USED_AFTER_MAINTAINCE           0x2F
#define TAISEIA_AIRCON_SERVICE_ID_HOURS_USED_AFTER_CLEAN_FILTER         0x30

/* Air Condition Engineering mode Service ID */
#define TAISEIA_AIRCON_SERVICE_ID_AC_LOWERST_TEMPERATURE_SETTING        0x50
#define TAISEIA_AIRCON_SERVICE_ID_HEATER_HIGHERST_TEMPERATURE_SETTING   0x51

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    TAISEIA_AC_POWER_STATE_UNKNOWN          = -1,
    TAISEIA_AC_POWER_STATE_OFF,
    TAISEIA_AC_POWER_STATE_ON,

} eTAISEIAAirConPowerState;

typedef enum
{
    TAISEIA_AC_MODE_UNKNOWN                 = -1,
    TAISEIA_AC_MODE_AIR_CONDITION,
    TAISEIA_AC_MODE_DEHUMIDIFICATION,
    TAISEIA_AC_MODE_AIR_SUPPLY,
    TAISEIA_AC_MODE_AUTO,
    TAISEIA_AC_MODE_HEATER,

} eTAISEIAAirConOperationMode;

/**
 * @description eTAISEIAAirConWindSpeedLevel will be obsolete, please use eTaiSEIAAirConditionWindSpeedControl
 */
typedef enum
{
    TAISEIA_AC_WS_UNKNOWN                   = -1,
    TAISEIA_AC_WS_AUTO,
    TAISEIA_AC_WS_SILENT,
    TAISEIA_AC_WS_BREEZE,
    TAISEIA_AC_WS_WEAK,
    TAISEIA_AC_WS_STRONG,

} eTAISEIAAirConWindSpeedLevel;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL
 */
typedef enum
{
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_UNKNOWN = -1,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_AUTO,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_1,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_2,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_3,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_4,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_5,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_6,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_7,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_8,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_9,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_10,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_11,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_12,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_13,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_14,
    TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_15,
    
} eTaiSEIAAirConditionWindSpeedControl;

typedef enum
{
    TAISEIA_AC_VERTICAL_SWINGABLE_UNKNOWN   = -1,
    TAISEIA_AC_VERTICAL_SWINGABLE_OFF,
    TAISEIA_AC_VERTICAL_SWINGABLE_ON,

} eTAISEIAAirConVerticalWindSwingableState;

/**
 * @see TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL
 */
typedef enum
{
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN = -1,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_0,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_1,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_2,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_3,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_4,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_5,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_6,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_7,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_8,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_9,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_10,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_11,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_12,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_13,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_14,
    TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_15,
    
} eTaiSEIAAirConditionVerticalWindDirectionLevelControl;

/**
 * @description eTAISEIAAirConHorizontalWindDirection will be obsolete, please use eTaiSEIAAirConditionHorizontalWindDirectionLevelControl
 */
typedef enum
{
    TAISEIA_AC_HORIZONTAL_WD_UNKNOWN        = -1,
    TAISEIA_AC_HORIZONTAL_WD_AUTO,
    TAISEIA_AC_HORIZONTAL_WD_LEFTMOST,
    TAISEIA_AC_HORIZONTAL_WD_MIDDLELEFT,
    TAISEIA_AC_HORIZONTAL_WD_CENTRAL,
    TAISEIA_AC_HORIZONTAL_WD_MIDDLERIGHT,
    TAISEIA_AC_HORIZONTAL_WD_RIGHTMOST,

} eTAISEIAAirConHorizontalWindDirection;

/**
 * @see TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL
 */
typedef enum
{
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN = -1,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_0,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_1,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_2,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_3,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_4,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_5,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_6,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_7,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_8,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_9,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_10,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_11,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_12,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_13,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_14,
    TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_15,
    
} eTaiSEIAAirConditionHorizontalWindDirectionLevelControl;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
typedef enum {
    TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_UNKNOWN        = -1,
    TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_OFF,
    TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_ON,
} eTaiSEIAAirConditionCleanFilterNotifyControl;
    
typedef enum
{
    TAISEIA_AC_MOLD_PREVENT_UNKNOWN         = -1,
    TAISEIA_AC_MOLD_PREVENT_OFF,
    TAISEIA_AC_MOLD_PREVENT_ON,

} eTAISEIAAirConMoldPreventState;

typedef enum
{
    TAISEIA_AC_FAST_OPERATION_UNKNOWN        = -1,
    TAISEIA_AC_FAST_OPERATION_OFF,
    TAISEIA_AC_FAST_OPERATION_ON,

} eTAISEIAAirConFastOperationState;

typedef enum
{
    TAISEIA_AC_ENERGY_SAVING_UNKNOWN        = -1,
    TAISEIA_AC_ENERGY_SAVING_OFF,
    TAISEIA_AC_ENERGY_SAVING_ON,

} eTAISEIAAirConEnergySavingState;

typedef enum
{
    TAISEIA_AC_SAA_STATE_UNKNOWN           = -1,
    TAISEIA_AC_SAA_ENABLE,
    TAISEIA_AC_SAA_DISABLE,

} eTAISEIAAirConSAAVoiceState;

typedef enum
{
    TAISEIA_AC_DISPLAY_BRIGHTNESS_UNKNOWN   = -1,
    TAISEIA_AC_DISPLAY_BRIGHTNESS_BRIGHT,
    TAISEIA_AC_DISPLAY_BRIGHTNESS_DARK,
    TAISEIA_AC_DISPLAY_BRIGHTNESS_OFF,
    TAISEIA_AC_DISPLAY_BRIGHTNESS_ALL_OFF,

} eTAISEIAAirConDisplayBrightnessLevel;

typedef enum
{
    TAISEIA_AC_HUMIDIFIER_UNKNOWN           = -1,
    TAISEIA_AC_HUMIDIFIER_DISABLE,
    TAISEIA_AC_HUMIDIFIER_LOW,
    TAISEIA_AC_HUMIDIFIER_HIGH,

} eTAISEIAAirConHumidifierLevel;


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
    uint8_t     PacketLength;
    uint8_t     SATypeID;
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;
    uint8_t     CheckSum;

} TaiSEIAPacket_t;

typedef struct
{
    uint8_t     PacketLength;
    uint8_t     Reserved1;
    uint8_t     ClassID;
    uint8_t     SAANetVerMajor;
    uint8_t     SAANetVerMinor;
    uint8_t     Reserved2;
    uint8_t     SATypeID;
    uint8_t     BrandName[20];  //NULL Terminated
    uint8_t     ModelName[20];  //NULL Terminated
    uint8_t     TotalServiceAvailable;
    uint8_t     ServiceStartIndex;  //Internal Use for quick indexing

} TaiSEIARegistrationHeader_t;


typedef struct
{
    uint8_t     TotalServiceAvailable;

} TaiSEIAServiceStatusListHeader_t;

typedef struct
{
    uint8_t     ServiceIsWritable; //1 - Write, 0 - Read
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;

} TaiSEIASupportedServiceInfo_t;

typedef struct
{
    uint8_t     ServiceIsWritable; //1 - Write, 0 - Read
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;

} TaiSEIAServiceInfo_t;

typedef struct
{
    uint8_t     AccessWrite; //1 - Write Access, 0 - Read Access
    uint8_t     SATypeID;
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;

} TaiSEIAServiceAccess_t;

typedef struct
{
    uint16_t    PackedPayloadLen;
    uint8_t     PackedPayload[sizeof(TaiSEIAPacket_t)];
} TaiSEIAPackedPayload_t;

typedef struct
{
    bool        Writable;
    bool        PowerOn;
    bool        PowerOff;

} TaiSEIASupportedPowerState_t;

typedef struct
{
    bool        Writable;
    bool        AirCondition;
    bool        Dehumidification;
    bool        AirSupply;
    bool        Auto;
    bool        Heater;

} TaiSEIASupportedOperationMode_t;

/**
 * @description  TaiSEIASupportedWindSpeedLevel_t will be obsolete, please use TaiSEIAAirConditionSupportedWindSpeedLevelControl_t
 */
typedef struct
{
    bool        Writable;
    bool        Auto;
    bool        Silent;
    bool        Breeze;
    bool        Weak;
    bool        Strong;

} TaiSEIASupportedWindSpeedLevel_t;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIAAirConditionSupportedWindSpeedLevelBitInfo_t;
typedef union {
    uint16_t                                                flag;
    TaiSEIAAirConditionSupportedWindSpeedLevelBitInfo_t     bits;
} TaiSEIAAirConditionSupportedWindSpeedLevelBitInfo_u;
typedef struct {
    bool                                                    Writable;
    TaiSEIAAirConditionSupportedWindSpeedLevelBitInfo_u     BitInfo;
} TaiSEIAAirConditionSupportedWindSpeedLevelControl_t;

typedef struct
{
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;

} TaiSEIASupportedTemperatureSetting_t;

typedef struct
{
    bool        Writable;
    int8_t      Minimum;
    int8_t      Maximum;

} TaiSEIASupportedIndoorTemperature_t;

typedef struct
{
    bool        Writable;
    uint16_t    MaxMinute;

} TaiSEIASupportedSleepModeTimer_t;

typedef struct
{
    bool        Writable;
    uint16_t    MaxMinute;

} TaiSEIASupportedBootTimer_t;

typedef struct
{
    bool        Writable;
    uint16_t    MaxMinute;

} TaiSEIASupportedShutdownTimer_t;

typedef struct
{
    bool        Writable;
    bool        On;
    bool        Off;

} TaiSEIASupportedVerticalWindSwingable_t;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIAAirConditionSupportedVerticalWindDirectionLevelBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIAAirConditionSupportedVerticalWindDirectionLevelBitInfo_t     bits;
} TaiSEIAAirConditionSupportedVerticalWindDirectionLevelBitInfo_u;
typedef struct {
    bool                                                                Writable;
    TaiSEIAAirConditionSupportedVerticalWindDirectionLevelBitInfo_u     BitInfo;
} TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t;

/**
 * @description TaiSEIASupportedHorizontalWindDirection_t will be obsolete, please use TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t
 */
typedef struct
{
    bool        Writable;
    bool        Auto;
    bool        LeftMost;
    bool        MiddleLeft;
    bool        Central;
    bool        MiddleRight;
    bool        RightMost;

} TaiSEIASupportedHorizontalWindDirection_t;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelBitInfo_t   bits;
} TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelBitInfo_u;
typedef struct {
    bool                                                                Writable;
    TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelBitInfo_u   BitInfo;
} TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
typedef struct
{
    bool        Writable;
    bool        Off;
    bool        On;
    
} TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t;

typedef struct
{
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;

} TaiSEIASupportedIndoorHumidity_t;

typedef struct
{
    bool        Writable;
    bool        Off;
    bool        On;

} TaiSEIASupportedMoldPreventState_t;

typedef struct
{
    bool        Writable;
    bool        Off;
    bool        On;

} TaiSEIASupportedFastOperationState_t;

typedef struct
{
    bool        Writable;
    bool        Off;
    bool        On;

} TaiSEIASupportedEnergySavingState_t;

/**
 * @description  TaiSEIASupportedControllerProhibited_t will be obsolete, please use TaiSEIAAirConditionSupportedControllerProhibitedControl_t
 */
struct TaiSEIAControllerProhibitedBitInfo_t
{
    uint16_t        PowerControl_b0             :1;
    uint16_t        OperationControl_b1         :1;
    uint16_t        TemperatureSetting_b2       :1;
    uint16_t        WindSpeedSetting_b3         :1;
    uint16_t        WindDirectionSetting_b4     :1;
    uint16_t        StopButtonEnable_b5         :1;
    uint16_t        Reserve_b6_15               :10;
};
union TaiSEIAControllerProhibitedBitInfo_u
{
    uint16_t                                                flag;
    struct TaiSEIAControllerProhibitedBitInfo_t             bits;

};
typedef struct
{
    bool                                                    Writable;
    union TaiSEIAControllerProhibitedBitInfo_u              BitInfo;

} TaiSEIASupportedControllerProhibited_t;
    
/**
 * @see TAISEIA_AIRCON_SERVICE_ID_CONTROLLER_PROHIBITED_SETTING
 */
typedef struct {
    uint16_t        PowerControl_b0             :1;
    uint16_t        OperationControl_b1         :1;
    uint16_t        TemperatureSetting_b2       :1;
    uint16_t        WindSpeedSetting_b3         :1;
    uint16_t        WindDirectionSetting_b4     :1;
    uint16_t        StopButtonEnable_b5         :1;
    uint16_t        Reserve_b6_15               :10;
} TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_t;
typedef union {
    uint16_t                                                        flag;
    TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_t       bits;
} TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u;
typedef struct {
    bool                                                            Writable;
    TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u       BitInfo;
} TaiSEIAAirConditionSupportedControllerProhibitedControl_t;

typedef struct
{
    bool        Writable;
    bool        Enable;
    bool        Disable;

} TaiSEIASupportedSAAVoiceState_t;

typedef struct
{
    bool        Writable;
    bool        Bright;
    bool        Dark;
    bool        Off;
    bool        AllOff;

} TaiSEIASupportedDisplayBrightnessLevel_t;

typedef struct
{
    bool        Writable;
    bool        Disable;
    bool        Low;
    bool        High;

} TaiSEIASupportedHumidifierLevel_t;

typedef struct
{
    bool        Writable;
    int8_t      Minimum;
    int8_t      Maximum;

} TaiSEIASupportedOutdoorTemperature_t;

typedef struct
{
    bool        Writable;
    float       Maximum;

} TaiSEIASupportedOutdoorHostCurrents_t;

typedef struct
{
    bool        Writable;
    float       Maximum;

} TaiSEIASupportedOutdoorHostAccumulationkWh_t;

/**
 * @description  TaiSEIASupportedErrorCode_t will be obsolete, please use TaiSEIAAirConditionSupportedErrorCode_t
 */
struct TaiSEIAErrorCodeBitInfo_t
{
    uint16_t        General_b0      :1;
    uint16_t        Error1_b1       :1;
    uint16_t        Error2_b2       :1;
    uint16_t        Error3_b3       :1;
    uint16_t        Error4_b4       :1;
    uint16_t        Error5_b5       :1;
    uint16_t        Error6_b6       :1;
    uint16_t        Error7_b7       :1;
    uint16_t        Error8_b8       :1;
    uint16_t        Error9_b9       :1;
    uint16_t        Error10_b10     :1;
    uint16_t        Error11_b11     :1;
    uint16_t        Error12_b12     :1;
    uint16_t        Error13_b13     :1;
    uint16_t        Error14_b14     :1;
    uint16_t        Error15_b15     :1;

};
union TaiSEIAErrorCodeBitInfo_u
{
    uint16_t                                    flag;
    struct TaiSEIAErrorCodeBitInfo_t            bits;

};
typedef struct
{
    bool                                        Writable;
    union TaiSEIAErrorCodeBitInfo_u             BitInfo;

} TaiSEIASupportedErrorCode_t;

/**
 * @see TAISEIA_AIRCON_SERVICE_ID_ERROR_CODE
 */
typedef struct {
    uint16_t        General_b0      :1;
    uint16_t        Error1_b1       :1;
    uint16_t        Error2_b2       :1;
    uint16_t        Error3_b3       :1;
    uint16_t        Error4_b4       :1;
    uint16_t        Error5_b5       :1;
    uint16_t        Error6_b6       :1;
    uint16_t        Error7_b7       :1;
    uint16_t        Error8_b8       :1;
    uint16_t        Error9_b9       :1;
    uint16_t        Error10_b10     :1;
    uint16_t        Error11_b11     :1;
    uint16_t        Error12_b12     :1;
    uint16_t        Error13_b13     :1;
    uint16_t        Error14_b14     :1;
    uint16_t        Error15_b15     :1;
} TaiSEIAAirConditionSupportedErrorCodeBitInfo_t;
typedef union {
    uint16_t                                            flag;
    TaiSEIAAirConditionSupportedErrorCodeBitInfo_t      bits;
} TaiSEIAAirConditionSupportedErrorCodeBitInfo_u;
typedef struct {
    bool                                                Writable;
    TaiSEIAAirConditionSupportedErrorCodeBitInfo_u      BitInfo;
} TaiSEIAAirConditionSupportedErrorCode_t;
    
typedef struct
{
    bool        Writable;

} TaiSEIASupportedHoursUsedAfterMaintance_t;

typedef struct
{
    bool        Writable;

} TaiSEIASupportedHoursUsedAfterCleanFilter_t;

typedef struct
{
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;

} TaiSEIASupportedAirConLowerstTemperatureSetting_t;

typedef struct
{
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;

} TaiSEIASupportedHeaterHigherstTemperatureSetting_t;

typedef struct
{
    uint8_t             *BrandName;
    eDeviceBrand        Index;
} TaiSEIABrandNameMap_t;

/******************************************************
 *               Parser Function Declarations
 ******************************************************/

/** Parse Taiseia registration base info (See TaiSEIARegistrationHeader_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaRegistrationPayload 	 : 	The payload of Taiseia returned registration data
 *
 * @return @ref TaiSEIARegistrationHeader_t, NOTE: if PacketLength is 0 then TaiseiaRegistrationPayload is NOT valid!
 */
TaiSEIARegistrationHeader_t TaiSEIA_GetRegistrationBaseInfo(uint8_t* TaiseiaRegistrationPayload);

eDeviceInfo TaiSEIA_GetModelInfo(uint8_t* TaiseiaRegistrationPayload, uint16_t SaType);

/** Parse Taiseia registration support service info by index (See TaiSEIASupportedServiceInfo_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaRegistrationPayload 	 : 	The payload of Taiseia returned registration data
 * @param[in]  ServiceIndex 	 : 	The index of service to get (NOTE: index start from 0)
 * @param[in]  knownHeader 	 : 	Make sure the header is obtained from same TaiseiaRegistrationPayload
 *
 * @return @ref TaiSEIASupportedServiceInfo_t, NOTE: if ServiceID is 0xFF then either ServiceIndex or TaiseiaRegistrationPayload is invalid.
 */
TaiSEIASupportedServiceInfo_t TaiSEIA_GetRegistrationSupportServiceByIndex(uint8_t* TaiseiaRegistrationPayload, uint8_t ServiceIndex, TaiSEIARegistrationHeader_t* knownHeader);


/** Parse Taiseia current service status info base info (See TaiSEIAServiceStatusListHeader_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaServiceDetailPayload 	 : 	The payload of Taiseia returned detail service data
 *
 * @return @ref TaiSEIAServiceStatusListHeader_t
 */
TaiSEIAServiceStatusListHeader_t TaiSEIA_GetServiceStatusListBaseInfo(uint8_t* TaiseiaServiceDetailPayload);

/** Parse Taiseia current service status info by index (See TaiSEIAServiceInfo_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaServiceDetailPayload 	 : 	The payload of Taiseia returned detail service data
 * @param[in]  ServiceIndex 	 : 	The index of service to get (NOTE: index start from 0)
 *
 * @return @ref TaiSEIAServiceInfo_t, NOTE: if ServiceID is 0xFF then either ServiceIndex or TaiseiaServiceDetailPayload is invalid.
 */
TaiSEIAServiceInfo_t TaiSEIA_GetServiceStatusByIndex(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceIndex);

/** Parse Taiseia current service status info by ServiceID (See TaiSEIAServiceInfo_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaServiceDetailPayload 	 : 	The payload of Taiseia returned detail service data
 * @param[in]  ServiceID    : 	The service ID to search for
 *
 * @return @ref TaiSEIAServiceInfo_t, NOTE: if ServiceID is 0xFF then either ServiceID or TaiseiaServiceDetailPayload is NotFound/invalid.
 */
TaiSEIAServiceInfo_t TaiSEIA_GetServiceStatusByServiceID(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceID);


/** Set Taiseia current service status info by ServiceID (See TaiSEIAServiceInfo_t for detail of what can be obtained)
 *
 * @param[in]  TaiseiaServiceDetailPayload 	 : 	The payload of Taiseia returned detail service data
 * @param[in]  ServiceID    : 	The service ID to search for
 * @param[in]  ServiceInfoToUpdate  : 	The value to be updated
 *
 * @return @ref TaiSEIAServiceInfo_t, NOTE: if ServiceID is 0xFF then either ServiceID or TaiseiaServiceDetailPayload is NotFound/invalid.
 */
int TaiSEIA_SetServiceStatusByServiceID(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceID, TaiSEIAServiceInfo_t* ServiceInfoToUpdate);


/******************************************************
 *               Access Function Declarations
 ******************************************************/


/** Convert Service Access to Payload
 *
 * @param[in]  ServiceAccess 	 : 	Service Access Data
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertAccessWritePacketPayload(TaiSEIAServiceAccess_t ServiceAccess);

/** Convert Get all current status info to Payload. NOTE: this does not require any input.
 *
 * @return @ref TaiSEIAPackedPayload_t
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertGetAllCurrentStatusInfoPacketPayload(void);


/** Convert Get registration info to Payload. NOTE: this does not require any input.
 *
 * @return @ref TaiSEIAPackedPayload_t
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertGetRegistrationInfoPacketPayload(void);

/** Parse Service Access to Payload
 *
 * @param[in]  pTaiSEIAPacket 	 : 	The TaiSEIAPacket to parse
 *
 * @return @ref TaiSEIAServiceAccess_t, NOTE: if ServiceID = 0xFF then given pTaiSEIAPacket is invalid (NULL)!
 */
TaiSEIAServiceAccess_t TaiSEIA_ParseAccessWritePacketPayload(TaiSEIAPackedPayload_t* pTaiSEIAPacket);

/******************************************************
 *               Other Utils
 ******************************************************/

/** Calculate CCR Check Sum, TotalLen should NOT include the CheckSum field of TaiSEIA packet
 *
 * @return @ref uint8_t CRC CheckSum
 */
uint8_t TaiSEIA_CalcTaiSEIACheckSum(uint8_t TotalLen, uint8_t* pPayload);

/******************************************************
 *                 Assistant Function
 ******************************************************/

int8_t TaiSEIA_GetInt8Value(uint8_t DataHighByte, uint8_t DataLowByte);
uint8_t TaiSEIA_GetUInt8Value(uint8_t DataHighByte, uint8_t DataLowByte);
uint16_t TaiSEIA_GetUInt16Value(uint8_t DataHighByte, uint8_t DataLowByte);

/******************************************************
 *    TaiSEIA Parse Supported Function Declarations
 ******************************************************/

/** Get Taiseia supported Power State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedPowerState_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedPowerState_t TaiSEIA_GetSupportedPowerState(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Operation Mode by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedOperationMode_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedOperationMode_t TaiSEIA_GetSupportedOperationMode(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Wind Speed Level by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedWindSpeedLevel_t, in structure, true means support otherwise means not.
 * @description will be obsolete, please use TaiSEIAAirConditionSupportedWindSpeedLevelControl_t TaiSEIAAirCondition_GetSupportedWindSpeedLevelControl(TaiSEIASupportedServiceInfo_t serviceInfo);
 */
TaiSEIASupportedWindSpeedLevel_t TaiSEIA_GetSupportedWindSpeedLevel(TaiSEIASupportedServiceInfo_t ServiceInfo);
    
/**
 * Get TaiSEIA AirCondition Supported Wind Speed Level Control
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedWindSpeedLevelControl_t, in structure, true means support otherwise means not.
 *
 * @see TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL
 */
TaiSEIAAirConditionSupportedWindSpeedLevelControl_t TaiSEIAAirCondition_GetSupportedWindSpeedLevelControl(TaiSEIASupportedServiceInfo_t serviceInfo);

/** Get Taiseia supported Temperature Setting by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedTemperatureSetting_t, if Writable flag is true means writable otherwise means not, Minimum and Minimum Parameter indicates the Lowerst and Higherst temperature range.
 */
TaiSEIASupportedTemperatureSetting_t TaiSEIA_GetSupportedTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Indoor Temperature by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedIndoorTemperature_t, if Writable flag is true means writable otherwise means not, Minimum and Minimum Parameter indicates the Lowerst and Higherst temperature range.
 */
TaiSEIASupportedIndoorTemperature_t TaiSEIA_GetSupportedIndoorTemperature(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Sleep Mode Timer Setting Limit by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedSleepModeTimer_t, if Writable flag is true means writable otherwise means not, MaxMinute is maximum count down timer of sleep mode.
 */
TaiSEIASupportedSleepModeTimer_t TaiSEIA_GetSupportedSleepModeTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Auto Boot Timer Setting Limit by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedBootTimer_t, if Writable flag is true means writable otherwise means not, MaxMinute is maximum count down timer of auto boot.
 */
TaiSEIASupportedBootTimer_t TaiSEIA_GetSupportedBootTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Auto Shutdown Timer Setting Limit by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedShutdownTimer_t, if Writable flag is true means writable otherwise means not, MaxMinute is maximum count down timer of auto shutdown.
 */
TaiSEIASupportedShutdownTimer_t TaiSEIA_GetSupportedShutdownTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Vertical Wind Swingable State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedVerticalWindSwingable_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedVerticalWindSwingable_t TaiSEIA_GetSupportedVerticalWindSwingableState(TaiSEIASupportedServiceInfo_t ServiceInfo);
    
/**
 * Get TaiSEIA AirCondition Supported Vertical Wind Direction Level Control
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t, in structure, true means support otherwise means not.
 *
 * @see TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL
 */
TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t TaiSEIAAirCondition_GetSupportedVerticalWindDirectionLevelControl(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Horizontal Wind Direction by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedHorizontalWindDirectionLevel_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedHorizontalWindDirection_t TaiSEIA_GetSupportedHorizontalWindDirection(TaiSEIASupportedServiceInfo_t ServiceInfo);
    
/**
 * Get TaiSEIA AirCondition Supported Horizontal Wind Direction Level Control
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t, in structure, true means support otherwise means not.
 *
 * @see TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL
 */
TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t TaiSEIAAirCondition_GetSupportedHorizontalWindDirectionLevelControl(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get TaiSEIA AirCondition supported Clean Filter Notify
 *
 * @param[in]  ServiceInfo      :     TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t, in structure, true means support otherwise means not.
 */
TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t TaiSEIAAirCondition_GetSupportedCleanFilterNotify(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Indoor Humidity by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedIndoorHumidity_t, if Writable flag is true means writable otherwise means not, Minimum and Maximum Parameter indicates the minimum and maximum value of humidity.
 */
TaiSEIASupportedIndoorHumidity_t TaiSEIA_GetSupportedIndoorHumidity(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Mold Prevent State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedMoldPreventState_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedMoldPreventState_t TaiSEIA_GetSupportedMoldPreventState(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Fast Operation State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedFastOperationState_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedFastOperationState_t TaiSEIA_GetSupportedFastOperationState(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Energy Saving State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedEnergySavingState_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedEnergySavingState_t TaiSEIA_GetSupportedEnergySavingMode(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Controller Prohibited Info by TaiSEIASupportedServiceInfo_t
  *
  * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
  *
  * @return TaiSEIASupportedControllerProhibited_t, if Writable flag is true means writable otherwise means not.
  */
TaiSEIASupportedControllerProhibited_t TaiSEIA_GetSupportedControllerProhibited(TaiSEIASupportedServiceInfo_t ServiceInfo);
    
/** Get TaiSEIA AirCondition Supported Prohibited Control
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedControllerProhibitedControl_t, if Writable flag is true means writable otherwise means not.
 */
TaiSEIAAirConditionSupportedControllerProhibitedControl_t TaiSEIAAirCondition_GetSupportedControllerProhibitedControl(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported SAA Voice State by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedSAAVoiceState_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedSAAVoiceState_t TaiSEIA_GetSupportedSAAVoiceState(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Display Brightness Level by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedDisplayBrightnessLevel_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedDisplayBrightnessLevel_t TaiSEIA_GetSupportedDisplayBrightnessLevel(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Humidifier Level by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedHumidifierLevel_t, in structure, true means support otherwise means not.
 */
TaiSEIASupportedHumidifierLevel_t TaiSEIA_GetSupportedHumidifierLevel(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Outdoor Temperature by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedOutdoorTemperature_t, if Writable flag is true means writable otherwise means not, Minimum and Minimum Parameter indicates the Lowerst and Higherst temperature range.
 */
TaiSEIASupportedOutdoorTemperature_t TaiSEIA_GetSupportedOutdoorTemperature(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Outdoor Currents by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedOutdoorHostCurrents_t, if Writable flag is true means writable otherwise means not, Maximum Parameter indicates the maximum value of currents.
 */
TaiSEIASupportedOutdoorHostCurrents_t TaiSEIA_GetSupportedOutdoorHostCurrents(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Outdoor Accumulation kWh by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedOutdoorHostAccumulationkWh_t, if Writable flag is true means writable otherwise means not, Maximum Parameter indicates the maximum value of accumulation kWh.
 */
TaiSEIASupportedOutdoorHostAccumulationkWh_t TaiSEIA_GetSupportedOutdoorHostAccumulationkWh(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Error Code by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedErrorCode_t, if Writable flag is true means writable otherwise means not.
 * @description will be obsolete, please use TaiSEIAAirConditionSupportedErrorCode_t TaiSEIAAirCondition_GetSupportedErrorCode(TaiSEIASupportedServiceInfo_t ServiceInfo);
 */
TaiSEIASupportedErrorCode_t TaiSEIA_GetSupportedErrorCode(TaiSEIASupportedServiceInfo_t ServiceInfo);
    
/** Get Taiseia AirCondition supported Error Code
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedErrorCode_t, if Writable flag is true means writable otherwise means not.
 */
TaiSEIAAirConditionSupportedErrorCode_t TaiSEIAAirCondition_GetSupportedErrorCode(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Hours Used Value After Maintance by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedHoursUsedAfterMaintance_t, if Writable flag is true means writable otherwise means not.
 */
TaiSEIASupportedHoursUsedAfterMaintance_t TaiSEIA_GetSupportedHoursUsedAfterMaintance(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Hours Used Value After Clean Filter by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedHoursUsedAfterCleanFilter_t, if Writable flag is true means writable otherwise means not.
 */
TaiSEIASupportedHoursUsedAfterCleanFilter_t TaiSEIA_GetSupportedHoursUsedAfterCleanFilter(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported AirCon Lowerst Temperature Setting by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedAirConLowerstTemperatureSetting_t, if Writable flag is true means writable otherwise means not, Minimum and Maximum Parameter indicates the allowed range of temperature setting.
 */
TaiSEIASupportedAirConLowerstTemperatureSetting_t TaiSEIA_GetSupportedAirConLowerstTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/** Get Taiseia supported Heater Higherst Temperature Setting by TaiSEIASupportedServiceInfo_t
 *
 * @param[in]  ServiceInfo      : 	TaiSEIASupportedServiceInfo_t
 *
 * @return TaiSEIASupportedHeaterHigherstTemperatureSetting_t, if Writable flag is true means writable otherwise means not, Minimum and Maximum Parameter indicates the allowed range of temperature setting.
 */
TaiSEIASupportedHeaterHigherstTemperatureSetting_t TaiSEIA_GetSupportedHeaterHigherstTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo);

/******************************************************
 *    TaiSEIA State Converter Function Declarations
 ******************************************************/

/** Get Taiseia Current Power State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConPowerState that indicate current Power State.
 */
eTAISEIAAirConPowerState TaiSEIA_GetCurrentPowerState(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Operation Mode by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConOperationMode that indicate current Operation Mode.
 */
eTAISEIAAirConOperationMode TaiSEIA_GetCurrentOperationMode(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Wind Speed Level by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConWindSpeedLevel that indicate current Wind Speed Level.
 *
 * @ description will obsolete, please use eTaiSEIAAirConditionWindSpeedControl TaiSEIAAirCondition_GetCurrentWindSpeedControl(TaiSEIAServiceInfo_t serviceInfo);
 */
eTAISEIAAirConWindSpeedLevel TaiSEIA_GetCurrentWindSpeedLevel(TaiSEIAServiceInfo_t ServiceInfo);
    
/**
 * Get TaiSEIA AirCondition Current Wind Speed Control
 *
 * @param serviceInfo the input parameter
 *
 * @return an instance of eTAISEIAAirConditionWindSpeedControl that indicate current Wind Speed Control.
 *
 * @see TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL
 */
eTaiSEIAAirConditionWindSpeedControl TaiSEIAAirCondition_GetCurrentWindSpeedControl(TaiSEIAServiceInfo_t serviceInfo);

/** Get Taiseia Target Temperature setting value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates the target tempearture.
 */
uint8_t TaiSEIA_GetCurrentTargetTemperature(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Indoor Temperature by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates current indoor temperature.
 */
int8_t TaiSEIA_GetCurrentIndoorTemperature(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia left timer to end sleep mode by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint16_t value that indicates the left minutes of sleep mode.
 */
uint16_t TaiSEIA_GetCurrentSleepModeLeftTimer(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia left timer to boot host by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint16_t value that indicates the count down minutes of boot host.
 */
uint16_t TaiSEIA_GetCurrentBootLeftTimer(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia left timer to shutdown host by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint16_t value that indicates the count down minutes of shutdown host.
 */
uint16_t TaiSEIA_GetCurrentShutdownLeftTimer(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Vertical Wind Swingable State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConVerticalWindSwingableState that indicate current Vertical Wind Swingable State.
 */
eTAISEIAAirConVerticalWindSwingableState TaiSEIA_GetCurrentVerticalWindDirectionState(TaiSEIAServiceInfo_t ServiceInfo);
    
/** Get Taiseia Current Vertical Wind Direction Level
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTaiSEIAAirConditionVerticalWindDirectionLevelControl that indicate current Vertical Wind Direction Level.
 */
eTaiSEIAAirConditionVerticalWindDirectionLevelControl TaiSEIAAirCondition_GetCurrentVerticalWindDirectionLevelControl(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Horizontal Wind Direction by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConHorizontalWindDirection that indicate current Horizontal Wind Direction.
 *
 * @description will be obsolete, please use eTaiSEIAAirConditionHorizontalWindDirectionLevelControl TaiSEIAAirCondition_GetCurrentHorizontalWindDirectionLevelControl(TaiSEIAServiceInfo_t ServiceInfo);
 */
eTAISEIAAirConHorizontalWindDirection TaiSEIA_GetCurrentHorizontalWindDirection(TaiSEIAServiceInfo_t ServiceInfo);
    
/** Get Taiseia Current Horizontal Wind Direction by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTaiSEIAAirConditionHorizontalWindDirectionLevelControl that indicate current Horizontal Wind Direction.
 */
eTaiSEIAAirConditionHorizontalWindDirectionLevelControl TaiSEIAAirCondition_GetCurrentHorizontalWindDirectionLevelControl(TaiSEIAServiceInfo_t ServiceInfo);
    
/** Get TaiSEIA AirCondition Current Clean Filter Notify Control
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTaiSEIAAirConditionCleanFilterNotifyControl that indicates current notify clean filter state.
 */
eTaiSEIAAirConditionCleanFilterNotifyControl TaiSEIAAirCondition_GetCurrentCleanFilterNotifyControl(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Indoor Humidity value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates the current Indoor Humidity.
 */
uint8_t TaiSEIA_GetCurrentIndoorHumidity(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Mold Prevent State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConMoldPreventState that indicate current Mold Prevent State.
 */
eTAISEIAAirConMoldPreventState TaiSEIA_GetCurrentPreventMoldState(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Fast Operation State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConFastOperationState that indicate current Fast Operation State.
 */
eTAISEIAAirConFastOperationState TaiSEIA_GetCurrentFastOperationState(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Energy Saving State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConEnergySavingState that indicate current Mold Prevent State.
 */
eTAISEIAAirConEnergySavingState TaiSEIA_GetCurrentEnergySavingState(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Controller Prohibited by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return TaiSEIAControllerProhibitedBitInfo_u that indicate Controller Prohibited bits.
 * @description will be obsolete, please use TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u TaiSEIAAirCondition_GetCurrentControllerProhibitedControl(TaiSEIAServiceInfo_t ServiceInfo);
 */
union TaiSEIAControllerProhibitedBitInfo_u TaiSEIA_GetCurrentControllerProhibited(TaiSEIAServiceInfo_t ServiceInfo);
    
/** Get TaiSEIA AirCondition Controller Prohibited
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u that indicate Current Controller Prohibited Setting.
 */
TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u TaiSEIAAirCondition_GetCurrentControllerProhibitedControl(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current SAA Voice State by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConSAAVoiceState that indicate current SAA Voice Status.
 */
eTAISEIAAirConSAAVoiceState TaiSEIA_GetCurrentSAAVoiceState(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Display Brightness Level by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConDisplayBrightnessLevel that indicate current Display Brightness Level.
 */
eTAISEIAAirConDisplayBrightnessLevel TaiSEIA_GetCurrentDisplayBrightnessLevel(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Humidifier Level by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return eTAISEIAAirConHumidifierLevel that indicate current Humidifier Level.
 */
eTAISEIAAirConHumidifierLevel TaiSEIA_GetCurrentHumidifierLevel(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Outdoor Temperature by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates current outdoor temperature.
 */
int8_t TaiSEIA_GetCurrentOutdoorTemperature(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Outdoor Host Currents value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return float value that indicates outdoor host cuurents with "Amp" unit.
 */
float TaiSEIA_GetCurrentOutdoorHostCurrents(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Current Outdoor Host Accumulation kWh value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return float value that indicates outdoor host accumulation with "kWh" unit.
 */
float TaiSEIA_GetCurrentOutdoorHostAccumulationkWh(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Error Code by TaiSEIAServiceInfo_t
*
* @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
*
* @return TaiSEIAErrorCodeBitInfo_u that indicate Error Code bits;
* @ description will obsolete, please use TaiSEIAAirConditionSupportedErrorCodeBitInfo_u TaiSEIAAirCondition_GetCurrentErrorCode(TaiSEIAServiceInfo_t ServiceInfo);
*/
union TaiSEIAErrorCodeBitInfo_u TaiSEIA_GetCurrentErrorCode(TaiSEIAServiceInfo_t ServiceInfo);

/** Get TaiSEIA AirCondition Current Error Code
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return TaiSEIAAirConditionSupportedErrorCodeBitInfo_u that indicate current Error Code.
 */
TaiSEIAAirConditionSupportedErrorCodeBitInfo_u TaiSEIAAirCondition_GetCurrentErrorCode(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Hours Used after Maintance by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint16_t value that indicates the used hour after maintance.
 */
uint16_t TaiSEIA_GetCurrentHoursUsedAfterMaintance(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Hours Used after Clean Filter by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint16_t value that indicates the used hour after clean filter.
 */
uint16_t TaiSEIA_GetCurrentHoursUsedAfterCleanFilter(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia AirCon Lowerst Target Temperature value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates the lowerst target temperature of air condition.
 */
uint8_t TaiSEIA_GetCurrentAirConLowerstTargetTemperatureValue(TaiSEIAServiceInfo_t ServiceInfo);

/** Get Taiseia Heater Highester Target Temperature value by TaiSEIAServiceInfo_t
 *
 * @param[in]  ServiceInfo      :   TaiSEIAServiceInfo_t
 *
 * @return uint8_t value that indicates the higherst target temperature of heater.
 */
uint8_t TaiSEIA_GetCurrentHeaterHigherstTargetTemperatureValue(TaiSEIAServiceInfo_t ServiceInfo);

/******************************************************
 * TaiSEIA WriteAccess Converter Function Declarations
 ******************************************************/

/** Convert Power Control State to Payload
 *
 * @param[in]  State 	 : 	A valid state of eTAISEIAAirConPowerState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertPowerStatePacketPayload(eTAISEIAAirConPowerState State);

/** Convert Operation Mode to Payload
 *
 * @param[in]  Mode 	 : 	A valid state of eTAISEIAAirConOperationMode
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertOperationPacketPayload(eTAISEIAAirConOperationMode Mode);

/** Convert Wind Speed Level to Payload
 *
 * @param[in]  Level 	 : 	A valid state of eTAISEIAAirConWindSpeedLevel
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 *
 * @description will be obsolete, please use TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertWindSpeedControlPacketPayload (eTaiSEIAAirConditionWindSpeedControl control);
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertWindSpeedLevelPacketPayload(eTAISEIAAirConWindSpeedLevel Level);

/** Convert Wind Speed Control to Payload
 *
 * @param[in]  Control : A valid value of eTaiSEIAAirConditionWindSpeedControl
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertWindSpeedControlPacketPayload(eTaiSEIAAirConditionWindSpeedControl Control);
    
/** Convert Air Condition Target Temperature to Payload
 *
 * @param[in]  TargetTemperature    :   Target Temperature, temperature limit please reference to TaiSEIASupportedTemperatureSetting_t
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertTargetTemperaturePacketPayload(uint8_t TargetTemperature);

/** Convert Sleep Mode Count Down Timer to Payload
 *
 * @param[in]  Minutes    :   Minutes to count down to end sleep mode, time limit please reference to TaiSEIASupportedSleepModeTimer_t
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertSleepModeCountDownTimerPacketPayload(uint16_t Minutes);

/** Convert Boot Host Count Down Timer to Payload
 *
 * @param[in]  Minutes    :   Minutes to count down to boot host, time limit please reference to TaiSEIASupportedBootTimer_t
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertBootHostCountdownTimerPacketPayload(uint16_t Minutes);

/** Convert Shutdown Host Count Down Timer to Payload
 *
 * @param[in]  Minutes    :   Minutes to count down to shutdown host, time limit please reference to TaiSEIASupportedShutdownTimer_t
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertShutdownHostCountdownTimerPacketPayload(uint16_t Minutes);

/** Convert Vertical Swingable State to Payload
 *
 * @param[in]  State    :  A valid state of  eTAISEIAAirConVerticalWindSwingableState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertVerticalSwingablePacketPayload(eTAISEIAAirConVerticalWindSwingableState State);
    
/** Convert Vertical Swingable State to Payload
 *
 * @param[in]  Control    :  A valid value of  eTaiSEIAAirConditionVerticalWindDirectionLevelControl
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertVerticalWindDirectionLevelControlPacketPayload(eTaiSEIAAirConditionVerticalWindDirectionLevelControl Control);

/** Convert Horizontal Wind Direction to Payload
 *
 * @param[in]  SwingState    :  A valid state of  eTAISEIAAirConHorizontalWindDirection
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertHorizontalWindDirectionPacketPayload(eTAISEIAAirConHorizontalWindDirection Direction);
    
/** Convert Horizontal Wind Direction to Payload
 *
 * @param[in]  SwingState    :  A valid state of eTaiSEIAAirConditionHorizontalWindDirectionLevelControl
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertHorizontalWindDirectionLevelControlPacketPayload(eTaiSEIAAirConditionHorizontalWindDirectionLevelControl Control);
    
/** Convert Clean Filter Notify to Payload
 *
 * @param[in]  Control   :  A valid value of eTaiSEIAAirConditionCleanFilterNotifyControl
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertCleanFilterNotifyPacketPayload(eTaiSEIAAirConditionCleanFilterNotifyControl Control);

/** Convert Mold Prevent State to Payload
 *
 * @param[in]  PreventState    :  A valid state of  eTAISEIAAirConMoldPreventState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertMoldPreventStatePacketPayload(eTAISEIAAirConMoldPreventState State);

/** Convert Fast Operation State to Payload
 *
 * @param[in]  State    :  A valid state of  eTAISEIAAirConFastOperationState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertFastOperationStatePacketPayload(eTAISEIAAirConFastOperationState State);

/** Convert Energy Saving State to Payload
 *
 * @param[in]  State    :  A valid state of  eTAISEIAAirConEnergySavingState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertEnergySavingStatePacketPayload(eTAISEIAAirConEnergySavingState State);

/** Convert SAA Voice State to Payload
 *
 * @param[in]  State    :  A valid state of  eTAISEIAAirConSAAVoiceState
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertSAAVoiceStatePacketPayload(eTAISEIAAirConSAAVoiceState State);

/** Convert Display Brightness Level to Payload
 *
 * @param[in]  State    :  A valid state of eTAISEIAAirConDisplayBrightnessLevel
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertDisplayBrightnessLevelPacketPayload(eTAISEIAAirConDisplayBrightnessLevel Level);

/** Convert Display Brightness Level to Payload
 *
 * @param[in]  State    :  A valid state of eTAISEIAAirConHumidifierLevel
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertHumidifierLevelPacketPayload(eTAISEIAAirConHumidifierLevel Level);

/** Convert Host Accumulation kWh value to Payload
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertAccumulationkWhResetAfterMaintancePacketPayload(void);

/** Convert Used Hour Reset After Maintance to Payload
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertUsedHourResetAfterMaintancePacketPayload(void);

/** Convert Used Hour Reset After Clean Filter to Payload
 *
 * @return @ref TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given ServiceAccess is invalid!
 */
TaiSEIAPackedPayload_t TaiSEIA_ConvertUsedHourResetAfterCleanFilterPacketPayload(void);

/******************************************************
 *   TaiSEIA WriteAccess Parse Function Declarations
 ******************************************************/

/** Parse Power Control State from TaiSEIAServiceAccess_t
 *
 * @param[in]  ServiceAccess 	 : 	TaiSEIAServiceAccess_t
 *
 * @return eTAISEIAAirConPowerState that indicate Power State.
 */
eTAISEIAAirConPowerState TaiSEIA_ParsePowerStatePacketPayload(TaiSEIAServiceAccess_t ServiceAccess);

#ifdef __cplusplus
} /*extern "C" */
#endif

#endif //BLE_PERIPHERAL_TAISEIA_INFO_H
