//============================================================================
// File: dk_peripheralTaiSEIA.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dk_PeripheralTaiSEIA.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define TAISEIA_REGISTRATION_PACKET_LENGTH_INDEX             0
#define TAISEIA_REGISTRATION_PACKET_CLASS_ID_INDEX           2
#define TAISEIA_REGISTRATION_PACKET_VERSION_MAJOR_INDEX      3
#define TAISEIA_REGISTRATION_PACKET_VERSION_MINOR_INDEX      4
#define TAISEIA_REGISTRATION_PACKET_SA_TYPEID_INDEX          6
#define TAISEIA_REGISTRATION_PACKET_BRAND_NAME_INDEX         8

#define TAISEIA_SERVICE_PACKET_LENGTH_INDEX                 0
#define TAISEIA_SERVICE_PACKET_TYPE_ID_INDEX                1
#define TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX             2

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;

} TaiSEIAGenServiceInfo_t;

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/
static TaiSEIABrandNameMap_t BrandNameMap[] = {
    { "HITACHI", DEVICE_BRAND_HITACHI }
};

const uint8_t *HitachAcCommercialModel[] = {
    "RPI", "RCI", "RPCI", "RPC", "RPF", "RPK", "RPV"
};

/******************************************************
 *               Local Function Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/


TaiSEIARegistrationHeader_t TaiSEIA_GetRegistrationBaseInfo(uint8_t* TaiseiaRegistrationPayload)
{
    TaiSEIARegistrationHeader_t Header;
    memset(&Header, 0, sizeof(Header));

    if (TaiseiaRegistrationPayload)
    {
        uint8_t* pRegistrationPayload = TaiseiaRegistrationPayload;
        uint8_t  PacketLength = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_LENGTH_INDEX];
        //Packet length should be greater than 9, otherwise its consider invalid!
        if (PacketLength > 9)
        {
            Header.PacketLength = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_LENGTH_INDEX];
            Header.ClassID = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_CLASS_ID_INDEX];
            Header.SAANetVerMajor = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_VERSION_MAJOR_INDEX];
            Header.SAANetVerMinor = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_VERSION_MINOR_INDEX];
            Header.SATypeID = pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_SA_TYPEID_INDEX];
            uint8_t BrandNameLen = strlen(&pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_BRAND_NAME_INDEX]);
            if (BrandNameLen && (BrandNameLen < 20))
                memcpy(Header.BrandName, &pRegistrationPayload[TAISEIA_REGISTRATION_PACKET_BRAND_NAME_INDEX], BrandNameLen);

            uint8_t ModelNameStartIndex = TAISEIA_REGISTRATION_PACKET_BRAND_NAME_INDEX + BrandNameLen + 1;
            uint8_t ModelNameLen = strlen(&pRegistrationPayload[ModelNameStartIndex]);
            if (ModelNameLen && (ModelNameLen < 20))
                memcpy(Header.ModelName, &pRegistrationPayload[ModelNameStartIndex], ModelNameLen);

            uint8_t ServiceIndex = ModelNameStartIndex + ModelNameLen + 1;

            //To Continue we make sure there is at least 1 service
            if (PacketLength > (ServiceIndex + sizeof(TaiSEIAGenServiceInfo_t)))
            {
                Header.ServiceStartIndex = ServiceIndex;

                while ((PacketLength - 1) > ServiceIndex)
                {
                    Header.TotalServiceAvailable++;
                    ServiceIndex += sizeof(TaiSEIAGenServiceInfo_t);
                }
            }
        }
    }

    return Header;
}

eDeviceBrand getBrandInfo(uint8_t* brandName)
{
    int32_t i = 0;

    if (*brandName == '\0') {
        return DEVICE_BRAND_UNKNOWN;
    }

    for (i = 0; i < sizeof(BrandNameMap) / sizeof(BrandNameMap[0]); i++) {
        if (!strncmp(BrandNameMap[i].BrandName, brandName, strlen(BrandNameMap[i].BrandName))) {
            return BrandNameMap[i].Index;
        }
    }

    return DEVICE_BRAND_GENERIC;
}

eDeviceInfo getHitachModelInfo(uint8_t* modelName)
{
    int32_t i = 0;

    for (i = 0; i < sizeof(HitachAcCommercialModel) / sizeof(HitachAcCommercialModel[0]); i++) {
        if (!strncmp(HitachAcCommercialModel[i], modelName, strlen(HitachAcCommercialModel[i]))) {
            return DEVICE_INFO_HITACHI_AIRCON_COMMERCIAL;
        }
    }

    return DEVICE_INFO_HITACHI_AIRCON_NON_COMMERCIAL;
}

eDeviceInfo TaiSEIA_GetModelInfo(uint8_t* TaiseiaRegistrationPayload, uint16_t SaType)
{
    TaiSEIARegistrationHeader_t Header = TaiSEIA_GetRegistrationBaseInfo(TaiseiaRegistrationPayload);
    int32_t brand = DEVICE_BRAND_UNKNOWN;
    uint16_t model = DEVICE_INFO_UNDEFINED;

    if (Header.PacketLength == 0) {
        return DEVICE_INFO_UNDEFINED;
    }

    switch (SaType) {
    case TAISEIA_SA_TYPE_ID_AIR_CON:
        brand = getBrandInfo(Header.BrandName);
        if (brand == DEVICE_BRAND_HITACHI) {
            model = getHitachModelInfo(Header.ModelName);
        } else {
            model = DEVICE_INFO_GENERIC_AIRCON;
        }
        break;
    case TAISEIA_SA_TYPE_ID_DEHUMIDIFIER:
        if (brand == DEVICE_BRAND_HITACHI) {
            model = DEVICE_INFO_HITACHI_DEHUMIDIFIER;
        } else {
            model = DEVICE_INFO_GENERIC_DEHUMIDIFIER;
        }
        break;
    case TAISEIA_SA_TYPE_ID_HEAT_EXCHANGER:
        if (brand == DEVICE_BRAND_HITACHI) {
            model = DEVICE_INFO_HITACHI_HEAT_EXCHANGER;
        } else {
            model = DEVICE_INFO_GENERIC_HEAT_EXCHANGER;
        }
        break;
    default:
        model = DEVICE_INFO_UNDEFINED;
        break;
    }

    return model;
}

TaiSEIASupportedServiceInfo_t TaiSEIA_GetRegistrationSupportServiceByIndex(uint8_t* TaiseiaRegistrationPayload, uint8_t ServiceIndex, TaiSEIARegistrationHeader_t* knownHeader)
{
    TaiSEIASupportedServiceInfo_t ServiceInfo;
    memset(&ServiceInfo, 0, sizeof(ServiceInfo));
    ServiceInfo.ServiceID = 0xFF;

    if (TaiseiaRegistrationPayload && knownHeader)
    {
        if (knownHeader->PacketLength == TaiseiaRegistrationPayload[TAISEIA_REGISTRATION_PACKET_LENGTH_INDEX])
        {
            if ((ServiceIndex < knownHeader->TotalServiceAvailable) && knownHeader->ServiceStartIndex)
            {
                uint8_t TargetedServiceIndex = knownHeader->ServiceStartIndex + (ServiceIndex*sizeof(TaiSEIAGenServiceInfo_t));
                TaiSEIAGenServiceInfo_t TargetedServiceInfo;
                memcpy(&TargetedServiceInfo, &TaiseiaRegistrationPayload[TargetedServiceIndex], sizeof(TaiSEIAGenServiceInfo_t));

                ServiceInfo.ServiceIsWritable = (TargetedServiceInfo.ServiceID & 0x80) ? 1:0;
                ServiceInfo.ServiceID = (TargetedServiceInfo.ServiceID & 0x7F);
                ServiceInfo.DataHighByte = TargetedServiceInfo.DataHighByte;
                ServiceInfo.DataLowByte = TargetedServiceInfo.DataLowByte;
            }
        }
    }

    return ServiceInfo;
}


TaiSEIAServiceStatusListHeader_t TaiSEIA_GetServiceStatusListBaseInfo(uint8_t* TaiseiaServiceDetailPayload)
{
    TaiSEIAServiceStatusListHeader_t Header;
    memset(&Header, 0, sizeof(TaiSEIAServiceStatusListHeader_t));

    if (TaiseiaServiceDetailPayload)
    {
        uint8_t  PacketLength = TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_LENGTH_INDEX];

        //Make sure at least there is 1 service status info
        if (PacketLength > 6)
        {
            //make sure we are parsing all service status payload
            if ((TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_TYPE_ID_INDEX] == 0) &&
                (TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX] == TAISEIA_GENERIC_SERVICE_ID_ALL_CURRENT_STATUS))
            {
                uint8_t Index = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1;
                while ((PacketLength -1) > Index)
                {
                    Header.TotalServiceAvailable++;
                    Index += sizeof(TaiSEIAGenServiceInfo_t);
                }
            }
        }
    }

    return Header;
}

TaiSEIAServiceInfo_t TaiSEIA_GetServiceStatusByServiceID(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceID)
{

    TaiSEIAServiceInfo_t ServiceInfo;

    memset(&ServiceInfo, 0, sizeof(TaiSEIAServiceInfo_t));
    ServiceInfo.ServiceID = 0xFF;

    ServiceID &= 0x7F;
    if (TaiseiaServiceDetailPayload)
    {
        uint8_t PacketLength = TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_LENGTH_INDEX];
        uint8_t i = 0;
        uint8_t TargetedServiceIndex = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1 + (i * sizeof(TaiSEIAGenServiceInfo_t));

        while ((PacketLength - 1) >= (TargetedServiceIndex + sizeof(TaiSEIAGenServiceInfo_t)))
        {
            TaiSEIAGenServiceInfo_t TargetedServiceInfo;
            memcpy(&TargetedServiceInfo, &TaiseiaServiceDetailPayload[TargetedServiceIndex], sizeof(TaiSEIAGenServiceInfo_t));

            if ((TargetedServiceInfo.ServiceID & 0x7F) == ServiceID) {
                ServiceInfo.ServiceIsWritable = (TargetedServiceInfo.ServiceID & 0x80) ? 1:0;
                ServiceInfo.ServiceID = (TargetedServiceInfo.ServiceID & 0x7F);
                ServiceInfo.DataHighByte = TargetedServiceInfo.DataHighByte;
                ServiceInfo.DataLowByte = TargetedServiceInfo.DataLowByte;
                break;
            }

            //Not found, we clean up first
            memset(&ServiceInfo, 0, sizeof(TaiSEIAServiceInfo_t));
            ServiceInfo.ServiceID = 0xFF;

            i++;
            TargetedServiceIndex = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1 + (i * sizeof(TaiSEIAGenServiceInfo_t));
        }
    }

    return ServiceInfo;
}

int TaiSEIA_SetServiceStatusByServiceID(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceID, TaiSEIAServiceInfo_t* ServiceInfoToUpdate)
{
    int result = -1;

    ServiceID &= 0x7F;
    if (TaiseiaServiceDetailPayload)
    {
        uint8_t PacketLength = TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_LENGTH_INDEX];
        uint8_t i = 0;
        uint8_t TargetedServiceIndex = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1 + (i * sizeof(TaiSEIAGenServiceInfo_t));

        while ((PacketLength - 1) >= (TargetedServiceIndex + sizeof(TaiSEIAGenServiceInfo_t)))
        {
            TaiSEIAGenServiceInfo_t TargetedServiceInfo;
            memcpy(&TargetedServiceInfo, &TaiseiaServiceDetailPayload[TargetedServiceIndex], sizeof(TaiSEIAGenServiceInfo_t));

            if ((TargetedServiceInfo.ServiceID & 0x7F) == ServiceID) {
                TargetedServiceInfo.DataHighByte = ServiceInfoToUpdate->DataHighByte;
                TargetedServiceInfo.DataLowByte = ServiceInfoToUpdate->DataLowByte;
                memcpy (&TaiseiaServiceDetailPayload[TargetedServiceIndex], &TargetedServiceInfo, sizeof (TaiSEIAGenServiceInfo_t));
                result = 0;
                break;
            }

            i++;
            TargetedServiceIndex = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1 + (i * sizeof(TaiSEIAGenServiceInfo_t));
        }
    }

    return result;
}

TaiSEIAServiceInfo_t TaiSEIA_GetServiceStatusByIndex(uint8_t* TaiseiaServiceDetailPayload, uint8_t ServiceIndex)
{
    TaiSEIAServiceInfo_t ServiceInfo;
    memset(&ServiceInfo, 0, sizeof(TaiSEIAServiceInfo_t));
    ServiceInfo.ServiceID = 0xFF;

    if (TaiseiaServiceDetailPayload)
    {
        uint8_t  PacketLength = TaiseiaServiceDetailPayload[TAISEIA_SERVICE_PACKET_LENGTH_INDEX];

        uint8_t  TargetedServiceIndex = TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX + 1 + (ServiceIndex * sizeof(TaiSEIAGenServiceInfo_t));
        if ((PacketLength - 1) >= (TargetedServiceIndex + sizeof(TaiSEIAGenServiceInfo_t)))
        {
            TaiSEIAGenServiceInfo_t TargetedServiceInfo;
            memcpy(&TargetedServiceInfo, &TaiseiaServiceDetailPayload[TargetedServiceIndex], sizeof(TaiSEIAGenServiceInfo_t));

            ServiceInfo.ServiceIsWritable = (TargetedServiceInfo.ServiceID & 0x80) ? 1:0;
            ServiceInfo.ServiceID = (TargetedServiceInfo.ServiceID & 0x7F);
            ServiceInfo.DataHighByte = TargetedServiceInfo.DataHighByte;
            ServiceInfo.DataLowByte = TargetedServiceInfo.DataLowByte;
        }
    }

    return ServiceInfo;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertAccessWritePacketPayload(TaiSEIAServiceAccess_t ServiceAccess)
{
    TaiSEIAPackedPayload_t Payload;
    memset(&Payload, 0, sizeof(TaiSEIAPackedPayload_t));

    Payload.PackedPayloadLen = sizeof(TaiSEIAPacket_t);
    TaiSEIAPacket_t Packet;
    Packet.PacketLength = 6;
    Packet.SATypeID = ServiceAccess.SATypeID;
    Packet.ServiceID = (ServiceAccess.AccessWrite > 0) ? (0x80 | ServiceAccess.ServiceID) : (0x7F & ServiceAccess.ServiceID);
    Packet.DataHighByte = ServiceAccess.DataHighByte;
    Packet.DataLowByte = ServiceAccess.DataLowByte;
    Packet.CheckSum = TaiSEIA_CalcTaiSEIACheckSum(Packet.PacketLength - 1, &Packet);

    memcpy(Payload.PackedPayload, &Packet, sizeof(Packet));

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertGetAllCurrentStatusInfoPacketPayload(void)
{
    TaiSEIAPackedPayload_t Payload;
    memset(&Payload, 0, sizeof(TaiSEIAPackedPayload_t));

    Payload.PackedPayloadLen = sizeof(TaiSEIAPacket_t);
    TaiSEIAPacket_t Packet;
    Packet.PacketLength = 6;
    Packet.SATypeID = TAISEIA_SA_TYPE_ID_GENERIC;
    Packet.ServiceID = TAISEIA_GENERIC_SERVICE_ID_ALL_CURRENT_STATUS;
    Packet.DataHighByte = 0x00;
    Packet.DataLowByte = 0x00;
    Packet.CheckSum = TaiSEIA_CalcTaiSEIACheckSum(Packet.PacketLength - 1, &Packet);

    memcpy(Payload.PackedPayload, &Packet, sizeof(Packet));

    return Payload;

}

TaiSEIAPackedPayload_t TaiSEIA_ConvertGetRegistrationInfoPacketPayload(void)
{
    TaiSEIAPackedPayload_t Payload;
    memset(&Payload, 0, sizeof(TaiSEIAPackedPayload_t));

    Payload.PackedPayloadLen = sizeof(TaiSEIAPacket_t);
    TaiSEIAPacket_t Packet;
    Packet.PacketLength = 6;
    Packet.SATypeID = TAISEIA_SA_TYPE_ID_GENERIC;
    Packet.ServiceID = TAISEIA_GENERIC_SERVICE_ID_REGISTRATION;
    Packet.DataHighByte = 0x00;
    Packet.DataLowByte = 0x00;
    Packet.CheckSum = TaiSEIA_CalcTaiSEIACheckSum(Packet.PacketLength - 1, &Packet);

    memcpy(Payload.PackedPayload, &Packet, sizeof(Packet));

    return Payload;
}

TaiSEIAServiceAccess_t TaiSEIA_ParseAccessWritePacketPayload(TaiSEIAPackedPayload_t* pTaiSEIAPacket)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    ServiceAccess.ServiceID = 0xFF;

    if (pTaiSEIAPacket)
    {
        TaiSEIAPacket_t pPacket;
        memcpy(&pPacket, pTaiSEIAPacket->PackedPayload, sizeof(TaiSEIAPacket_t));

        ServiceAccess.SATypeID = pPacket.SATypeID;
        ServiceAccess.DataHighByte = pPacket.DataHighByte;
        ServiceAccess.DataLowByte = pPacket.DataLowByte;
        ServiceAccess.AccessWrite = (pPacket.ServiceID & 0x80) ? 1:0;
        ServiceAccess.ServiceID = (pPacket.ServiceID & 0x7F);
    }

    return ServiceAccess;
}

uint8_t TaiSEIA_CalcTaiSEIACheckSum(uint8_t TotalLen, uint8_t* pPayload)
{
    uint16_t i = 0;
    uint8_t     CheckSum = 0x00;

    if (TotalLen && pPayload)
    {
        for (i = 0; i < TotalLen; i++)
        {
            CheckSum ^= pPayload[i];
        }
    }

    return CheckSum;
}

/******************************************************
 *                 Assistant Function
 ******************************************************/

int8_t TaiSEIA_GetInt8Value(uint8_t DataHighByte, uint8_t DataLowByte)
{
    int8_t value = (int8_t)DataLowByte;

    return value;
}

uint8_t TaiSEIA_GetUInt8Value(uint8_t DataHighByte, uint8_t DataLowByte)
{
    return DataLowByte;
}

uint16_t TaiSEIA_GetUInt16Value(uint8_t DataHighByte, uint8_t DataLowByte)
{
    uint16_t value = 0;

    uint16_t UINT16HighByte = DataHighByte;
    uint16_t UINT16LowByte = DataLowByte;
    value = (((UINT16HighByte & 0x0000FFFF) << 8) + (UINT16LowByte & 0x000000FF));

    return value;
}

/******************************************************
 *          TaiSEIA Parse Supported Functions
 ******************************************************/

TaiSEIASupportedPowerState_t TaiSEIA_GetSupportedPowerState(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedPowerState_t PowerState;
    memset(&PowerState, 0, sizeof(TaiSEIASupportedPowerState_t));

    PowerState.Writable = ServiceInfo.ServiceIsWritable;
    PowerState.PowerOff = (ServiceInfo.DataLowByte & (1 << 0));
    PowerState.PowerOn = (ServiceInfo.DataLowByte & (1 << 1));

    return PowerState;
}

TaiSEIASupportedOperationMode_t TaiSEIA_GetSupportedOperationMode(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedOperationMode_t OperationMode;
    memset(&OperationMode, 0, sizeof(TaiSEIASupportedOperationMode_t));

    OperationMode.Writable = ServiceInfo.ServiceIsWritable;
    OperationMode.AirCondition = (ServiceInfo.DataLowByte & (1 << 0));
    OperationMode.Dehumidification = (ServiceInfo.DataLowByte & (1 << 1));
    OperationMode.AirSupply = (ServiceInfo.DataLowByte & (1 << 2));
    OperationMode.Auto = (ServiceInfo.DataLowByte & (1 << 3));
    OperationMode.Heater = (ServiceInfo.DataLowByte & (1 << 4));

    return OperationMode;
}

TaiSEIASupportedWindSpeedLevel_t TaiSEIA_GetSupportedWindSpeedLevel(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedWindSpeedLevel_t WindSpeedLevel;
    memset(&WindSpeedLevel, 0, sizeof(TaiSEIASupportedWindSpeedLevel_t));

    WindSpeedLevel.Writable = ServiceInfo.ServiceIsWritable;
    WindSpeedLevel.Auto = (ServiceInfo.DataLowByte & (1 << 0));
    WindSpeedLevel.Silent = (ServiceInfo.DataLowByte & (1 << 1));
    WindSpeedLevel.Breeze = (ServiceInfo.DataLowByte & (1 << 2));
    WindSpeedLevel.Weak = (ServiceInfo.DataLowByte & (1 << 3));
    WindSpeedLevel.Strong = (ServiceInfo.DataLowByte & (1 << 4));

    return WindSpeedLevel;
}

TaiSEIAAirConditionSupportedWindSpeedLevelControl_t TaiSEIAAirCondition_GetSupportedWindSpeedLevelControl(TaiSEIASupportedServiceInfo_t serviceInfo)
{
    TaiSEIAAirConditionSupportedWindSpeedLevelControl_t control;
    
    memset (&control, 0, sizeof (TaiSEIAAirConditionSupportedWindSpeedLevelControl_t));
    control.Writable = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0   = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1   = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2   = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3   = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4   = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5   = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6   = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7   = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8   = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9   = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10 = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11 = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12 = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13 = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14 = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15 = ((serviceInfo.DataHighByte >> 7) & 1);
    
    return control;
}

TaiSEIASupportedTemperatureSetting_t TaiSEIA_GetSupportedTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedTemperatureSetting_t TemperatureSetting;
    memset(&TemperatureSetting, 0, sizeof(TaiSEIASupportedTemperatureSetting_t));

    TemperatureSetting.Writable = ServiceInfo.ServiceIsWritable;
    TemperatureSetting.Minimum = ServiceInfo.DataHighByte;
    TemperatureSetting.Maximum = ServiceInfo.DataLowByte;

    return TemperatureSetting;
}

TaiSEIASupportedIndoorTemperature_t TaiSEIA_GetSupportedIndoorTemperature(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedIndoorTemperature_t IndoorTemperature;
    memset(&IndoorTemperature, 0, sizeof(TaiSEIASupportedIndoorTemperature_t));

    IndoorTemperature.Writable = ServiceInfo.ServiceIsWritable;
    IndoorTemperature.Minimum = ServiceInfo.DataHighByte;
    IndoorTemperature.Maximum = ServiceInfo.DataLowByte;

    return IndoorTemperature;
}

TaiSEIASupportedSleepModeTimer_t TaiSEIA_GetSupportedSleepModeTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedSleepModeTimer_t SleepModeTimer;
    memset(&SleepModeTimer, 0, sizeof(TaiSEIASupportedSleepModeTimer_t));

    SleepModeTimer.Writable = ServiceInfo.ServiceIsWritable;
    SleepModeTimer.MaxMinute = TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);

    return SleepModeTimer;
}

TaiSEIASupportedBootTimer_t TaiSEIA_GetSupportedBootTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedBootTimer_t BootTimer;
    memset(&BootTimer, 0, sizeof(TaiSEIASupportedBootTimer_t));

    BootTimer.Writable = ServiceInfo.ServiceIsWritable;
    BootTimer.MaxMinute = TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);

    return BootTimer;
}

TaiSEIASupportedShutdownTimer_t TaiSEIA_GetSupportedShutdownTimerSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedShutdownTimer_t ShutdownTimer;
    memset(&ShutdownTimer, 0, sizeof(TaiSEIASupportedShutdownTimer_t));

    ShutdownTimer.Writable = ServiceInfo.ServiceIsWritable;
    ShutdownTimer.MaxMinute = TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);

    return ShutdownTimer;
}

TaiSEIASupportedVerticalWindSwingable_t TaiSEIA_GetSupportedVerticalWindSwingableState(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedVerticalWindSwingable_t VerticalSwingable;
    memset(&VerticalSwingable, 0, sizeof(TaiSEIASupportedVerticalWindSwingable_t));

    VerticalSwingable.Writable = ServiceInfo.ServiceIsWritable;
    VerticalSwingable.Off = (ServiceInfo.DataLowByte & (1 << 0));
    VerticalSwingable.On = (ServiceInfo.DataLowByte & (1 << 1));

    return VerticalSwingable;
}

TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t TaiSEIAAirCondition_GetSupportedVerticalWindDirectionLevelControl(TaiSEIASupportedServiceInfo_t serviceInfo)
{
    TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t control;
    memset (&control, 0, sizeof (TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t));
    
    control.Writable                    = serviceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0      = ((serviceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1      = ((serviceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2      = ((serviceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3      = ((serviceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4      = ((serviceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5      = ((serviceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6      = ((serviceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7      = ((serviceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8      = ((serviceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9      = ((serviceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10    = ((serviceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11    = ((serviceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12    = ((serviceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13    = ((serviceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14    = ((serviceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15    = ((serviceInfo.DataHighByte >> 7) & 1);
    
    return control;
}

TaiSEIASupportedHorizontalWindDirection_t TaiSEIA_GetSupportedHorizontalWindDirection(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedHorizontalWindDirection_t HorizontalDirection;
    memset(&HorizontalDirection, 0, sizeof(TaiSEIASupportedHorizontalWindDirection_t));

    HorizontalDirection.Writable = ServiceInfo.ServiceIsWritable;
    HorizontalDirection.Auto = (ServiceInfo.DataLowByte & (1 << 0));
    HorizontalDirection.LeftMost = (ServiceInfo.DataLowByte & (1 << 5));
    HorizontalDirection.MiddleLeft = (ServiceInfo.DataLowByte & (1 << 4));
    HorizontalDirection.Central = (ServiceInfo.DataLowByte & (1 << 3));
    HorizontalDirection.MiddleRight = (ServiceInfo.DataLowByte & (1 << 2));
    HorizontalDirection.RightMost = (ServiceInfo.DataLowByte & (1 << 1));

    return HorizontalDirection;
}

TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t TaiSEIAAirCondition_GetSupportedHorizontalWindDirectionLevelControl(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedHorizontalWindDirectionLevelControl_t control;
    memset (&control, 0, sizeof (TaiSEIAAirConditionSupportedVerticalWindDirectionLevelControl_t));
    
    control.Writable                    = ServiceInfo.ServiceIsWritable;
    control.BitInfo.bits.Level0_b0      = ((ServiceInfo.DataLowByte  >> 0) & 1);
    control.BitInfo.bits.Level1_b1      = ((ServiceInfo.DataLowByte  >> 1) & 1);
    control.BitInfo.bits.Level2_b2      = ((ServiceInfo.DataLowByte  >> 2) & 1);
    control.BitInfo.bits.Level3_b3      = ((ServiceInfo.DataLowByte  >> 3) & 1);
    control.BitInfo.bits.Level4_b4      = ((ServiceInfo.DataLowByte  >> 4) & 1);
    control.BitInfo.bits.Level5_b5      = ((ServiceInfo.DataLowByte  >> 5) & 1);
    control.BitInfo.bits.Level6_b6      = ((ServiceInfo.DataLowByte  >> 6) & 1);
    control.BitInfo.bits.Level7_b7      = ((ServiceInfo.DataLowByte  >> 7) & 1);
    control.BitInfo.bits.Level8_b8      = ((ServiceInfo.DataHighByte >> 0) & 1);
    control.BitInfo.bits.Level9_b9      = ((ServiceInfo.DataHighByte >> 1) & 1);
    control.BitInfo.bits.Level10_b10    = ((ServiceInfo.DataHighByte >> 2) & 1);
    control.BitInfo.bits.Level11_b11    = ((ServiceInfo.DataHighByte >> 3) & 1);
    control.BitInfo.bits.Level12_b12    = ((ServiceInfo.DataHighByte >> 4) & 1);
    control.BitInfo.bits.Level13_b13    = ((ServiceInfo.DataHighByte >> 5) & 1);
    control.BitInfo.bits.Level14_b14    = ((ServiceInfo.DataHighByte >> 6) & 1);
    control.BitInfo.bits.Level15_b15    = ((ServiceInfo.DataHighByte >> 7) & 1);
    
    return control;
}

TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t TaiSEIAAirCondition_GetSupportedCleanFilterNotify(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t control;
    memset (&control, 0, sizeof (TaiSEIAAirConditionSupportedCleanFilterNotifyControl_t));
    
    control.Writable = ServiceInfo.ServiceIsWritable;
    control.Off      = ((ServiceInfo.DataLowByte >> 0) & 1);
    control.On       = ((ServiceInfo.DataLowByte >> 1) & 1);
    
    return control;
}

TaiSEIASupportedIndoorHumidity_t TaiSEIA_GetSupportedIndoorHumidity(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedIndoorHumidity_t Humidity;
    memset(&Humidity, 0, sizeof(TaiSEIASupportedIndoorHumidity_t));

    Humidity.Writable = ServiceInfo.ServiceIsWritable;
    Humidity.Minimum = ServiceInfo.DataHighByte;
    Humidity.Maximum = ServiceInfo.DataLowByte;

    return Humidity;
}

TaiSEIASupportedMoldPreventState_t TaiSEIA_GetSupportedMoldPreventState(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedMoldPreventState_t MoldPrevent;
    memset(&MoldPrevent, 0, sizeof(TaiSEIASupportedMoldPreventState_t));

    MoldPrevent.Writable = ServiceInfo.ServiceIsWritable;
    MoldPrevent.Off = (ServiceInfo.DataLowByte & (1 << 0));
    MoldPrevent.On = (ServiceInfo.DataLowByte & (1 << 1));

    return MoldPrevent;
}

TaiSEIASupportedFastOperationState_t TaiSEIA_GetSupportedFastOperationState(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedFastOperationState_t FastOperation;
    memset(&FastOperation, 0, sizeof(TaiSEIASupportedFastOperationState_t));

    FastOperation.Writable = ServiceInfo.ServiceIsWritable;
    FastOperation.Off = (ServiceInfo.DataLowByte & (1 << 0));
    FastOperation.On = (ServiceInfo.DataLowByte & (1 << 1));

    return FastOperation;
}

TaiSEIASupportedEnergySavingState_t TaiSEIA_GetSupportedEnergySavingMode(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedEnergySavingState_t EnergySaving;
    memset(&EnergySaving, 0, sizeof(TaiSEIASupportedEnergySavingState_t));

    EnergySaving.Writable = ServiceInfo.ServiceIsWritable;
    EnergySaving.Off = (ServiceInfo.DataLowByte & (1 << 0));
    EnergySaving.On = (ServiceInfo.DataLowByte & (1 << 1));

    return EnergySaving;
}

TaiSEIASupportedControllerProhibited_t TaiSEIA_GetSupportedControllerProhibited(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedControllerProhibited_t Prohibited;
    memset(&Prohibited, 0, sizeof(TaiSEIASupportedControllerProhibited_t));

    Prohibited.Writable = ServiceInfo.ServiceIsWritable;
    Prohibited.BitInfo.bits.PowerControl_b0 = (ServiceInfo.DataLowByte >> 0);
    Prohibited.BitInfo.bits.OperationControl_b1 = (ServiceInfo.DataLowByte >> 1);
    Prohibited.BitInfo.bits.TemperatureSetting_b2 = (ServiceInfo.DataLowByte >> 2);
    Prohibited.BitInfo.bits.WindSpeedSetting_b3 = (ServiceInfo.DataLowByte >> 3);
    Prohibited.BitInfo.bits.WindDirectionSetting_b4 = (ServiceInfo.DataLowByte >> 4);
    Prohibited.BitInfo.bits.StopButtonEnable_b5 = (ServiceInfo.DataLowByte >> 5);

    return Prohibited;
}

TaiSEIAAirConditionSupportedControllerProhibitedControl_t TaiSEIAAirCondition_GetSupportedControllerProhibitedControl(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedControllerProhibitedControl_t control;
    memset(&control, 0, sizeof(TaiSEIAAirConditionSupportedControllerProhibitedControl_t));
    
    control.Writable = ServiceInfo.ServiceIsWritable;
    control.BitInfo.bits.PowerControl_b0            = ((ServiceInfo.DataLowByte >> 0) & 1);
    control.BitInfo.bits.OperationControl_b1        = ((ServiceInfo.DataLowByte >> 1) & 1);
    control.BitInfo.bits.TemperatureSetting_b2      = ((ServiceInfo.DataLowByte >> 2) & 1);
    control.BitInfo.bits.WindSpeedSetting_b3        = ((ServiceInfo.DataLowByte >> 3) & 1);
    control.BitInfo.bits.WindDirectionSetting_b4    = ((ServiceInfo.DataLowByte >> 4) & 1);
    control.BitInfo.bits.StopButtonEnable_b5        = ((ServiceInfo.DataLowByte >> 5) & 1);
    
    return control;
}

TaiSEIASupportedSAAVoiceState_t TaiSEIA_GetSupportedSAAVoiceState(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedSAAVoiceState_t SAAVoice;
    memset(&SAAVoice, 0, sizeof(TaiSEIASupportedSAAVoiceState_t));

    SAAVoice.Writable = ServiceInfo.ServiceIsWritable;
    SAAVoice.Enable = (ServiceInfo.DataLowByte & (1 << 0));
    SAAVoice.Disable = (ServiceInfo.DataLowByte & (1 << 1));

    return SAAVoice;
}

TaiSEIASupportedDisplayBrightnessLevel_t TaiSEIA_GetSupportedDisplayBrightnessLevel(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedDisplayBrightnessLevel_t Level;
    memset(&Level, 0, sizeof(TaiSEIASupportedDisplayBrightnessLevel_t));

    Level.Writable = ServiceInfo.ServiceIsWritable;
    Level.Bright = (ServiceInfo.DataLowByte & (1 << 0));
    Level.Dark = (ServiceInfo.DataLowByte & (1 << 1));
    Level.Off = (ServiceInfo.DataLowByte & (1 << 2));
    Level.AllOff = (ServiceInfo.DataLowByte & (1 << 3));

    return Level;
}

TaiSEIASupportedHumidifierLevel_t TaiSEIA_GetSupportedHumidifierLevel(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedHumidifierLevel_t Level;
    memset(&Level, 0, sizeof(TaiSEIASupportedHumidifierLevel_t));

    Level.Writable = ServiceInfo.ServiceIsWritable;
    Level.Disable = (ServiceInfo.DataLowByte & (1 << 0));
    Level.Low = (ServiceInfo.DataLowByte & (1 << 1));
    Level.High = (ServiceInfo.DataLowByte & (1 << 2));

    return Level;
}

TaiSEIASupportedOutdoorTemperature_t TaiSEIA_GetSupportedOutdoorTemperature(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedOutdoorTemperature_t OutdoorTemperature;
    memset(&OutdoorTemperature, 0, sizeof(TaiSEIASupportedOutdoorTemperature_t));

    OutdoorTemperature.Writable = ServiceInfo.ServiceIsWritable;
    OutdoorTemperature.Minimum = ServiceInfo.DataHighByte;
    OutdoorTemperature.Maximum = ServiceInfo.DataLowByte;

    return OutdoorTemperature;
}

TaiSEIASupportedOutdoorHostCurrents_t TaiSEIA_GetSupportedOutdoorHostCurrents(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedOutdoorHostCurrents_t Currents;
    memset(&Currents, 0, sizeof(TaiSEIASupportedOutdoorHostCurrents_t));

    Currents.Writable = ServiceInfo.ServiceIsWritable;
    Currents.Maximum = TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte) / 10.0f;

    return Currents;
}

TaiSEIASupportedOutdoorHostAccumulationkWh_t TaiSEIA_GetSupportedOutdoorHostAccumulationkWh(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedOutdoorHostAccumulationkWh_t Accumulation;
    memset(&Accumulation, 0, sizeof(TaiSEIASupportedOutdoorHostAccumulationkWh_t));

    Accumulation.Writable = ServiceInfo.ServiceIsWritable;
    Accumulation.Maximum = TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte) / 10.0f;

    return Accumulation;
}

TaiSEIASupportedErrorCode_t TaiSEIA_GetSupportedErrorCode(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedErrorCode_t ErrorCode;
    memset(&ErrorCode, 0, sizeof(TaiSEIASupportedErrorCode_t));

    ErrorCode.Writable = ServiceInfo.ServiceIsWritable;
    ErrorCode.BitInfo.bits.General_b0 = (ServiceInfo.DataLowByte >> 0);
    ErrorCode.BitInfo.bits.Error1_b1 = (ServiceInfo.DataLowByte >> 1);
    ErrorCode.BitInfo.bits.Error2_b2 = (ServiceInfo.DataLowByte >> 2);
    ErrorCode.BitInfo.bits.Error3_b3 = (ServiceInfo.DataLowByte >> 3);
    ErrorCode.BitInfo.bits.Error4_b4 = (ServiceInfo.DataLowByte >> 4);
    ErrorCode.BitInfo.bits.Error5_b5 = (ServiceInfo.DataLowByte >> 5);
    ErrorCode.BitInfo.bits.Error6_b6 = (ServiceInfo.DataLowByte >> 6);
    ErrorCode.BitInfo.bits.Error7_b7 = (ServiceInfo.DataLowByte >> 7);
    ErrorCode.BitInfo.bits.Error8_b8 = (ServiceInfo.DataHighByte >> 0);
    ErrorCode.BitInfo.bits.Error9_b9 = (ServiceInfo.DataHighByte >> 1);
    ErrorCode.BitInfo.bits.Error10_b10 = (ServiceInfo.DataHighByte >> 2);
    ErrorCode.BitInfo.bits.Error11_b11 = (ServiceInfo.DataHighByte >> 3);
    ErrorCode.BitInfo.bits.Error12_b12 = (ServiceInfo.DataHighByte >> 4);
    ErrorCode.BitInfo.bits.Error13_b13 = (ServiceInfo.DataHighByte >> 5);
    ErrorCode.BitInfo.bits.Error14_b14 = (ServiceInfo.DataHighByte >> 6);
    ErrorCode.BitInfo.bits.Error15_b15 = (ServiceInfo.DataHighByte >> 7);

    return ErrorCode;
}

TaiSEIAAirConditionSupportedErrorCode_t TaiSEIAAirCondition_GetSupportedErrorCode(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedErrorCode_t errorCode;
    memset (&errorCode, 0, sizeof (TaiSEIAAirConditionSupportedErrorCode_t));
    
    errorCode.Writable = ServiceInfo.ServiceIsWritable;
    errorCode.BitInfo.bits.General_b0  = ((ServiceInfo.DataLowByte  >> 0) & 1);
    errorCode.BitInfo.bits.Error1_b1   = ((ServiceInfo.DataLowByte  >> 1) & 1);
    errorCode.BitInfo.bits.Error2_b2   = ((ServiceInfo.DataLowByte  >> 2) & 1);
    errorCode.BitInfo.bits.Error3_b3   = ((ServiceInfo.DataLowByte  >> 3) & 1);
    errorCode.BitInfo.bits.Error4_b4   = ((ServiceInfo.DataLowByte  >> 4) & 1);
    errorCode.BitInfo.bits.Error5_b5   = ((ServiceInfo.DataLowByte  >> 5) & 1);
    errorCode.BitInfo.bits.Error6_b6   = ((ServiceInfo.DataLowByte  >> 6) & 1);
    errorCode.BitInfo.bits.Error7_b7   = ((ServiceInfo.DataLowByte  >> 7) & 1);
    errorCode.BitInfo.bits.Error8_b8   = ((ServiceInfo.DataHighByte >> 0) & 1);
    errorCode.BitInfo.bits.Error9_b9   = ((ServiceInfo.DataHighByte >> 1) & 1);
    errorCode.BitInfo.bits.Error10_b10 = ((ServiceInfo.DataHighByte >> 2) & 1);
    errorCode.BitInfo.bits.Error11_b11 = ((ServiceInfo.DataHighByte >> 3) & 1);
    errorCode.BitInfo.bits.Error12_b12 = ((ServiceInfo.DataHighByte >> 4) & 1);
    errorCode.BitInfo.bits.Error13_b13 = ((ServiceInfo.DataHighByte >> 5) & 1);
    errorCode.BitInfo.bits.Error14_b14 = ((ServiceInfo.DataHighByte >> 6) & 1);
    errorCode.BitInfo.bits.Error15_b15 = ((ServiceInfo.DataHighByte >> 7) & 1);
    
    return errorCode;
}

TaiSEIASupportedHoursUsedAfterMaintance_t TaiSEIA_GetSupportedHoursUsedAfterMaintance(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedHoursUsedAfterMaintance_t Support;
    memset(&Support, 0, sizeof(TaiSEIASupportedHoursUsedAfterMaintance_t));

    Support.Writable = ServiceInfo.ServiceIsWritable;

    return Support;
}

TaiSEIASupportedHoursUsedAfterCleanFilter_t TaiSEIA_GetSupportedHoursUsedAfterCleanFilter(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedHoursUsedAfterCleanFilter_t Support;
    memset(&Support, 0, sizeof(TaiSEIASupportedHoursUsedAfterCleanFilter_t));

    Support.Writable = ServiceInfo.ServiceIsWritable;

    return Support;
}

TaiSEIASupportedAirConLowerstTemperatureSetting_t TaiSEIA_GetSupportedAirConLowerstTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedAirConLowerstTemperatureSetting_t AirConLowerst;
    memset(&AirConLowerst, 0, sizeof(TaiSEIASupportedAirConLowerstTemperatureSetting_t));

    AirConLowerst.Writable = ServiceInfo.ServiceIsWritable;
    AirConLowerst.Minimum = ServiceInfo.DataHighByte;
    AirConLowerst.Maximum = ServiceInfo.DataLowByte;

    return AirConLowerst;
}

TaiSEIASupportedHeaterHigherstTemperatureSetting_t TaiSEIA_GetSupportedHeaterHigherstTemperatureSetting(TaiSEIASupportedServiceInfo_t ServiceInfo)
{
    TaiSEIASupportedHeaterHigherstTemperatureSetting_t HeaterHigherst;
    memset(&HeaterHigherst, 0, sizeof(TaiSEIASupportedHeaterHigherstTemperatureSetting_t));

    HeaterHigherst.Writable = ServiceInfo.ServiceIsWritable;
    HeaterHigherst.Minimum = ServiceInfo.DataHighByte;
    HeaterHigherst.Maximum = ServiceInfo.DataLowByte;

    return HeaterHigherst;
}

/******************************************************
 *    TaiSEIA State Converter Function Declarations
 ******************************************************/

eTAISEIAAirConPowerState TaiSEIA_GetCurrentPowerState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConPowerState PowerState = TAISEIA_AC_POWER_STATE_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            PowerState =  TAISEIA_AC_POWER_STATE_OFF;
        }
            break;
        case 1:
        {
            PowerState =  TAISEIA_AC_POWER_STATE_ON;
        }
            break;
        default:
            break;
    }

    return PowerState;
}

eTAISEIAAirConOperationMode TaiSEIA_GetCurrentOperationMode(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConOperationMode Mode = TAISEIA_AC_MODE_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Mode =  TAISEIA_AC_MODE_AIR_CONDITION;
        }
            break;
        case 1:
        {
            Mode =  TAISEIA_AC_MODE_DEHUMIDIFICATION;
        }
            break;
        case 2:
        {
            Mode =  TAISEIA_AC_MODE_AIR_SUPPLY;
        }
            break;
        case 3:
        {
            Mode =  TAISEIA_AC_MODE_AUTO;
        }
            break;
        case 4:
        {
            Mode =  TAISEIA_AC_MODE_HEATER;
        }
            break;
        default:
            break;
    }

    return Mode;
}

eTAISEIAAirConWindSpeedLevel TaiSEIA_GetCurrentWindSpeedLevel(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConWindSpeedLevel Level = TAISEIA_AC_WS_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Level =  TAISEIA_AC_WS_AUTO;
        }
            break;
        case 1:
        {
            Level =  TAISEIA_AC_WS_SILENT;
        }
            break;
        case 2:
        {
            Level =  TAISEIA_AC_WS_BREEZE;
        }
            break;
        case 3:
        {
            Level =  TAISEIA_AC_WS_WEAK;
        }
            break;
        case 4:
        {
            Level =  TAISEIA_AC_WS_STRONG;
        }
            break;
        default:
            break;
    }

    return Level;
}

eTaiSEIAAirConditionWindSpeedControl TaiSEIAAirCondition_GetCurrentWindSpeedControl(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTaiSEIAAirConditionWindSpeedControl control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_UNKNOWN;
    
    switch (ServiceInfo.DataLowByte) {
        case 0  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_AUTO;  break;
        case 1  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_1;  break;
        case 2  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_2;  break;
        case 3  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_3;  break;
        case 4  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_4;  break;
        case 5  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_5;  break;
        case 6  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_6;  break;
        case 7  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_7;  break;
        case 8  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_8;  break;
        case 9  : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_9;  break;
        case 10 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_10; break;
        case 11 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_11; break;
        case 12 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_12; break;
        case 13 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_13; break;
        case 14 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_14; break;
        case 15 : control = TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_15; break;
        default : break;
    }
    
    return control;
}

uint8_t TaiSEIA_GetCurrentTargetTemperature(TaiSEIAServiceInfo_t ServiceInfo)
{
    return ServiceInfo.DataLowByte;
}

int8_t TaiSEIA_GetCurrentIndoorTemperature(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetInt8Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

uint16_t TaiSEIA_GetCurrentSleepModeLeftTimer(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

uint16_t TaiSEIA_GetCurrentBootLeftTimer(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

uint16_t TaiSEIA_GetCurrentShutdownLeftTimer(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

eTAISEIAAirConVerticalWindSwingableState TaiSEIA_GetCurrentVerticalWindDirectionState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConVerticalWindSwingableState Swingable = TAISEIA_AC_VERTICAL_SWINGABLE_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Swingable =  TAISEIA_AC_VERTICAL_SWINGABLE_OFF;
        }
            break;
        case 1:
        {
            Swingable =  TAISEIA_AC_VERTICAL_SWINGABLE_ON;
        }
            break;
        default:
            break;
    }

    return Swingable;
}

eTaiSEIAAirConditionVerticalWindDirectionLevelControl TaiSEIAAirCondition_GetCurrentVerticalWindDirectionLevelControl(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTaiSEIAAirConditionVerticalWindDirectionLevelControl control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN;
    
    switch (ServiceInfo.DataLowByte) {
        case 0  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_0;  break;
        case 1  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_1;  break;
        case 2  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_2;  break;
        case 3  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_3;  break;
        case 4  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_4;  break;
        case 5  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_5;  break;
        case 6  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_6;  break;
        case 7  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_7;  break;
        case 8  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_8;  break;
        case 9  : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_9;  break;
        case 10 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_10; break;
        case 11 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_11; break;
        case 12 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_12; break;
        case 13 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_13; break;
        case 14 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_14; break;
        case 15 : control = TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_15; break;
        default : break;
    }
    
    return control;
}

eTAISEIAAirConHorizontalWindDirection TaiSEIA_GetCurrentHorizontalWindDirection(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConHorizontalWindDirection Direction = TAISEIA_AC_HORIZONTAL_WD_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_AUTO;
        }
            break;
        case 1:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_RIGHTMOST;
        }
            break;
        case 2:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_MIDDLERIGHT;
        }
            break;
        case 3:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_CENTRAL;
        }
            break;
        case 4:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_MIDDLELEFT;
        }
            break;
        case 5:
        {
            Direction =  TAISEIA_AC_HORIZONTAL_WD_LEFTMOST;
        }
            break;
        default:
            break;
    }

    return Direction;
}

eTaiSEIAAirConditionHorizontalWindDirectionLevelControl TaiSEIAAirCondition_GetCurrentHorizontalWindDirectionLevelControl(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTaiSEIAAirConditionHorizontalWindDirectionLevelControl Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN;
    
    switch (ServiceInfo.DataLowByte) {
        case 0  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_0;  break;
        case 1  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_1;  break;
        case 2  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_2;  break;
        case 3  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_3;  break;
        case 4  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_4;  break;
        case 5  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_5;  break;
        case 6  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_6;  break;
        case 7  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_7;  break;
        case 8  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_8;  break;
        case 9  : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_9;  break;
        case 10 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_10; break;
        case 11 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_11; break;
        case 12 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_12; break;
        case 13 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_13; break;
        case 14 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_14; break;
        case 15 : Control = TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_15; break;
        default : break;
    }
    
    return Control;
}

eTaiSEIAAirConditionCleanFilterNotifyControl TaiSEIAAirCondition_GetCurrentCleanFilterNotifyControl(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTaiSEIAAirConditionCleanFilterNotifyControl control = TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_UNKNOWN;
    
    switch (ServiceInfo.DataLowByte) {
        case 0  : control =  TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_OFF; break;
        case 1  : control =  TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_ON;  break;
        default : break;
    }
    
    return control;
}

uint8_t TaiSEIA_GetCurrentIndoorHumidity(TaiSEIAServiceInfo_t ServiceInfo)
{
    return ServiceInfo.DataLowByte;
}

eTAISEIAAirConMoldPreventState TaiSEIA_GetCurrentPreventMoldState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConMoldPreventState MoldPrevent = TAISEIA_AC_MOLD_PREVENT_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            MoldPrevent =  TAISEIA_AC_MOLD_PREVENT_OFF;
        }
            break;
        case 1:
        {
            MoldPrevent =  TAISEIA_AC_MOLD_PREVENT_ON;
        }
            break;
        default:
            break;
    }

    return MoldPrevent;
}

eTAISEIAAirConFastOperationState TaiSEIA_GetCurrentFastOperationState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConFastOperationState FastOperation = TAISEIA_AC_FAST_OPERATION_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            FastOperation =  TAISEIA_AC_FAST_OPERATION_OFF;
        }
            break;
        case 1:
        {
            FastOperation =  TAISEIA_AC_FAST_OPERATION_ON;
        }
            break;
        default:
            break;
    }

    return FastOperation;
}

eTAISEIAAirConEnergySavingState TaiSEIA_GetCurrentEnergySavingState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConEnergySavingState EnergySaving = TAISEIA_AC_ENERGY_SAVING_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            EnergySaving =  TAISEIA_AC_ENERGY_SAVING_OFF;
        }
            break;
        case 1:
        {
            EnergySaving =  TAISEIA_AC_ENERGY_SAVING_ON;
        }
            break;
        default:
            break;
    }

    return EnergySaving;
}

union TaiSEIAControllerProhibitedBitInfo_u TaiSEIA_GetCurrentControllerProhibited(TaiSEIAServiceInfo_t ServiceInfo)
{
    union TaiSEIAControllerProhibitedBitInfo_u Prohibited;
    memset(&Prohibited, 0, sizeof(Prohibited));

    Prohibited.bits.PowerControl_b0 = (ServiceInfo.DataLowByte >> 0);
    Prohibited.bits.OperationControl_b1 = (ServiceInfo.DataLowByte >> 1);
    Prohibited.bits.TemperatureSetting_b2 = (ServiceInfo.DataLowByte >> 2);
    Prohibited.bits.WindSpeedSetting_b3 = (ServiceInfo.DataLowByte >> 3);
    Prohibited.bits.WindDirectionSetting_b4 = (ServiceInfo.DataLowByte >> 4);
    Prohibited.bits.StopButtonEnable_b5 = (ServiceInfo.DataLowByte >> 5);

    return Prohibited;
}

TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u TaiSEIAAirCondition_GetCurrentControllerProhibitedControl(TaiSEIAServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedControllerProhibitedBitInfo_u bitInfo;
    
    bitInfo.flag = (((uint16_t) ServiceInfo.DataHighByte) << 8) + ((uint16_t) ServiceInfo.DataLowByte);
    
    return bitInfo;
}

eTAISEIAAirConSAAVoiceState TaiSEIA_GetCurrentSAAVoiceState(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConSAAVoiceState State = TAISEIA_AC_SAA_STATE_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            State =  TAISEIA_AC_SAA_ENABLE;
        }
            break;
        case 1:
        {
            State =  TAISEIA_AC_SAA_DISABLE;
        }
            break;
        default:
            break;
    }

    return State;
}

eTAISEIAAirConDisplayBrightnessLevel TaiSEIA_GetCurrentDisplayBrightnessLevel(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConDisplayBrightnessLevel Level = TAISEIA_AC_DISPLAY_BRIGHTNESS_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Level =  TAISEIA_AC_DISPLAY_BRIGHTNESS_BRIGHT;
        }
            break;
        case 1:
        {
            Level =  TAISEIA_AC_DISPLAY_BRIGHTNESS_DARK;
        }
            break;
        case 2:
        {
            Level =  TAISEIA_AC_DISPLAY_BRIGHTNESS_OFF;
        }
            break;
        case 3:
        {
            Level =  TAISEIA_AC_DISPLAY_BRIGHTNESS_ALL_OFF;
        }
            break;
        default:
            break;
    }

    return Level;
}

eTAISEIAAirConHumidifierLevel TaiSEIA_GetCurrentHumidifierLevel(TaiSEIAServiceInfo_t ServiceInfo)
{
    eTAISEIAAirConHumidifierLevel Level = TAISEIA_AC_HUMIDIFIER_UNKNOWN;

    switch (ServiceInfo.DataLowByte) {
        case 0:
        {
            Level =  TAISEIA_AC_HUMIDIFIER_DISABLE;
        }
            break;
        case 1:
        {
            Level =  TAISEIA_AC_HUMIDIFIER_LOW;
        }
            break;
        case 2:
        {
            Level =  TAISEIA_AC_HUMIDIFIER_HIGH;
        }
            break;
        default:
            break;
    }

    return Level;
}

int8_t TaiSEIA_GetCurrentOutdoorTemperature(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetInt8Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

float TaiSEIA_GetCurrentOutdoorHostCurrents(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetInt8Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte) / 10.0f;
}

float TaiSEIA_GetCurrentOutdoorHostAccumulationkWh(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetInt8Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte) / 10.0f;
}

union TaiSEIAErrorCodeBitInfo_u TaiSEIA_GetCurrentErrorCode(TaiSEIAServiceInfo_t ServiceInfo)
{
    union TaiSEIAErrorCodeBitInfo_u ErrorCode;
    memset(&ErrorCode, 0, sizeof(ErrorCode));

    ErrorCode.bits.General_b0 = (ServiceInfo.DataLowByte >> 0);
    ErrorCode.bits.Error1_b1 = (ServiceInfo.DataLowByte >> 1);
    ErrorCode.bits.Error2_b2 = (ServiceInfo.DataLowByte >> 2);
    ErrorCode.bits.Error3_b3 = (ServiceInfo.DataLowByte >> 3);
    ErrorCode.bits.Error4_b4 = (ServiceInfo.DataLowByte >> 4);
    ErrorCode.bits.Error5_b5 = (ServiceInfo.DataLowByte >> 5);
    ErrorCode.bits.Error6_b6 = (ServiceInfo.DataLowByte >> 6);
    ErrorCode.bits.Error7_b7 = (ServiceInfo.DataLowByte >> 7);
    ErrorCode.bits.Error8_b8 = (ServiceInfo.DataHighByte >> 0);
    ErrorCode.bits.Error9_b9 = (ServiceInfo.DataHighByte >> 1);
    ErrorCode.bits.Error10_b10 = (ServiceInfo.DataHighByte >> 2);
    ErrorCode.bits.Error11_b11 = (ServiceInfo.DataHighByte >> 3);
    ErrorCode.bits.Error12_b12 = (ServiceInfo.DataHighByte >> 4);
    ErrorCode.bits.Error13_b13 = (ServiceInfo.DataHighByte >> 5);
    ErrorCode.bits.Error14_b14 = (ServiceInfo.DataHighByte >> 6);
    ErrorCode.bits.Error15_b15 = (ServiceInfo.DataHighByte >> 7);

    return ErrorCode;
}

TaiSEIAAirConditionSupportedErrorCodeBitInfo_u TaiSEIAAirCondition_GetCurrentErrorCode(TaiSEIAServiceInfo_t ServiceInfo)
{
    TaiSEIAAirConditionSupportedErrorCodeBitInfo_u bitInfo;
    
    bitInfo.flag = (((uint16_t) ServiceInfo.DataHighByte) << 8) + ((uint16_t) ServiceInfo.DataLowByte);
    
    return bitInfo;
}

uint16_t TaiSEIA_GetCurrentHoursUsedAfterMaintance(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

uint16_t TaiSEIA_GetCurrentHoursUsedAfterCleanFilter(TaiSEIAServiceInfo_t ServiceInfo)
{
    return TaiSEIA_GetUInt16Value(ServiceInfo.DataHighByte, ServiceInfo.DataLowByte);
}

uint8_t TaiSEIA_GetCurrentAirConLowerstTargetTemperatureValue(TaiSEIAServiceInfo_t ServiceInfo)
{
    return ServiceInfo.DataLowByte;
}

uint8_t TaiSEIA_GetCurrentHeaterHigherstTargetTemperatureValue(TaiSEIAServiceInfo_t ServiceInfo)
{
    return ServiceInfo.DataLowByte;
}

/******************************************************
 * TaiSEIA WriteAccess Converter Function Declarations
 ******************************************************/

TaiSEIAPackedPayload_t TaiSEIA_ConvertPowerStatePacketPayload(eTAISEIAAirConPowerState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_POWER_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_POWER_STATE_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_POWER_STATE_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertOperationPacketPayload(eTAISEIAAirConOperationMode Mode)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_MODE_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (Mode) {
        case TAISEIA_AC_MODE_AIR_CONDITION:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_MODE_DEHUMIDIFICATION:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        case TAISEIA_AC_MODE_AIR_SUPPLY:
        {
            ServiceAccess.DataLowByte = 0x02;
        }
            break;
        case TAISEIA_AC_MODE_AUTO:
        {
            ServiceAccess.DataLowByte = 0x03;
        }
            break;
        case TAISEIA_AC_MODE_HEATER:
        {
            ServiceAccess.DataLowByte = 0x04;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertWindSpeedLevelPacketPayload(eTAISEIAAirConWindSpeedLevel Level)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (Level) {
        case TAISEIA_AC_WS_AUTO:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_WS_SILENT:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        case TAISEIA_AC_WS_BREEZE:
        {
            ServiceAccess.DataLowByte = 0x02;
        }
            break;
        case TAISEIA_AC_WS_WEAK:
        {
            ServiceAccess.DataLowByte = 0x03;
        }
            break;
        case TAISEIA_AC_WS_STRONG:
        {
            ServiceAccess.DataLowByte = 0x04;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertWindSpeedControlPacketPayload(eTaiSEIAAirConditionWindSpeedControl Control)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    
    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_WIND_SPEED_CONTROL;
    ServiceAccess.DataHighByte = 0x00;
    
    switch (Control) {
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_AUTO : ServiceAccess.DataLowByte = 0;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_1    : ServiceAccess.DataLowByte = 1;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_2    : ServiceAccess.DataLowByte = 2;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_3    : ServiceAccess.DataLowByte = 3;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_4    : ServiceAccess.DataLowByte = 4;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_5    : ServiceAccess.DataLowByte = 5;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_6    : ServiceAccess.DataLowByte = 6;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_7    : ServiceAccess.DataLowByte = 7;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_8    : ServiceAccess.DataLowByte = 8;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_9    : ServiceAccess.DataLowByte = 9;  break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_10   : ServiceAccess.DataLowByte = 10; break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_11   : ServiceAccess.DataLowByte = 11; break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_12   : ServiceAccess.DataLowByte = 12; break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_13   : ServiceAccess.DataLowByte = 13; break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_14   : ServiceAccess.DataLowByte = 14; break;
        case TAISEIA_AIRCONDITION_WIND_SPEED_CONTROL_15   : ServiceAccess.DataLowByte = 15; break;
        default : break;
    }
    
    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);
    
    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertTargetTemperaturePacketPayload(uint8_t TargetTemperature)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_TEMPERATURE_SETTING;
    ServiceAccess.DataHighByte = 0x00;
    ServiceAccess.DataLowByte = TargetTemperature;

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertSleepModeCountDownTimerPacketPayload(uint16_t Minutes)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_SLEEP_MODE_TIMER_SETTING;
    ServiceAccess.DataHighByte = (uint8_t)(Minutes >> 8);
    ServiceAccess.DataLowByte = (uint8_t)(Minutes & 0xFF);

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertBootHostCountdownTimerPacketPayload(uint16_t Minutes)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_BOOT_TIMER_SETTING;
    ServiceAccess.DataHighByte = (uint8_t)(Minutes >> 8);
    ServiceAccess.DataLowByte = (uint8_t)(Minutes & 0xFF);

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertShutdownHostCountdownTimerPacketPayload(uint16_t Minutes)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_SHUTDOWN_TIMER_SETTING;
    ServiceAccess.DataHighByte = (uint8_t)(Minutes >> 8);
    ServiceAccess.DataLowByte = (uint8_t)(Minutes & 0xFF);

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertVerticalSwingablePacketPayload(eTAISEIAAirConVerticalWindSwingableState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_SWINGABLE_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_VERTICAL_SWINGABLE_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_VERTICAL_SWINGABLE_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertVerticalWindDirectionLevelControlPacketPayload(eTaiSEIAAirConditionVerticalWindDirectionLevelControl Control)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    
    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL;
    ServiceAccess.DataHighByte = 0x00;
    
    switch (Control) {
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_0   : ServiceAccess.DataLowByte = 0;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_1   : ServiceAccess.DataLowByte = 1;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_2   : ServiceAccess.DataLowByte = 2;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_3   : ServiceAccess.DataLowByte = 3;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_4   : ServiceAccess.DataLowByte = 4;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_5   : ServiceAccess.DataLowByte = 5;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_6   : ServiceAccess.DataLowByte = 6;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_7   : ServiceAccess.DataLowByte = 7;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_8   : ServiceAccess.DataLowByte = 8;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_9   : ServiceAccess.DataLowByte = 9;  break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_10  : ServiceAccess.DataLowByte = 10; break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_11  : ServiceAccess.DataLowByte = 11; break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_12  : ServiceAccess.DataLowByte = 12; break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_13  : ServiceAccess.DataLowByte = 13; break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_14  : ServiceAccess.DataLowByte = 14; break;
        case TAISEIA_AIRCONDITION_VERTICAL_WIND_DIRECTION_LEVEL_CONTROL_15  : ServiceAccess.DataLowByte = 15; break;
        default : break;
    }
    
    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);
    
    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertHorizontalWindDirectionPacketPayload(eTAISEIAAirConHorizontalWindDirection Direction)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (Direction) {
        case TAISEIA_AC_HORIZONTAL_WD_AUTO:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_HORIZONTAL_WD_LEFTMOST:
        {
            ServiceAccess.DataLowByte = 0x05;
        }
            break;
        case TAISEIA_AC_HORIZONTAL_WD_MIDDLELEFT:
        {
            ServiceAccess.DataLowByte = 0x04;
        }
            break;
        case TAISEIA_AC_HORIZONTAL_WD_CENTRAL:
        {
            ServiceAccess.DataLowByte = 0x03;
        }
            break;
        case TAISEIA_AC_HORIZONTAL_WD_MIDDLERIGHT:
        {
            ServiceAccess.DataLowByte = 0x02;
        }
            break;
        case TAISEIA_AC_HORIZONTAL_WD_RIGHTMOST:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertHorizontalWindDirectionLevelControlPacketPayload(eTaiSEIAAirConditionHorizontalWindDirectionLevelControl Control)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    
    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HORIZONTAL_WIND_DIRECTION_CONTROL;
    ServiceAccess.DataHighByte = 0x00;
    
    switch (Control) {
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_0   : ServiceAccess.DataLowByte = 0;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_1   : ServiceAccess.DataLowByte = 1;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_2   : ServiceAccess.DataLowByte = 2;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_3   : ServiceAccess.DataLowByte = 3;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_4   : ServiceAccess.DataLowByte = 4;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_5   : ServiceAccess.DataLowByte = 5;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_6   : ServiceAccess.DataLowByte = 6;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_7   : ServiceAccess.DataLowByte = 7;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_8   : ServiceAccess.DataLowByte = 8;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_9   : ServiceAccess.DataLowByte = 9;  break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_10  : ServiceAccess.DataLowByte = 10; break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_11  : ServiceAccess.DataLowByte = 11; break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_12  : ServiceAccess.DataLowByte = 12; break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_13  : ServiceAccess.DataLowByte = 13; break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_14  : ServiceAccess.DataLowByte = 14; break;
        case TAISEIA_AIRCONDITION_HORIZONTAL_WIND_DIRECTION_LEVEL_CONTROL_15  : ServiceAccess.DataLowByte = 15; break;
        default : break;
    }
    
    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);
    
    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIAAirCondition_ConvertCleanFilterNotifyPacketPayload(eTaiSEIAAirConditionCleanFilterNotifyControl Control)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));
    
    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL;
    ServiceAccess.DataHighByte = 0x00;
    
    switch (Control) {
        case TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AIRCONDITION_CLEAN_FILTER_NOTIFY_CONTROL_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }
    
    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);
    
    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertMoldPreventStatePacketPayload(eTAISEIAAirConMoldPreventState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_MOLD_PREVENT_SETTING;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_MOLD_PREVENT_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_MOLD_PREVENT_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertFastOperationStatePacketPayload(eTAISEIAAirConFastOperationState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_FAST_OPERATION_SETTING;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_FAST_OPERATION_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_FAST_OPERATION_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertEnergySavingStatePacketPayload(eTAISEIAAirConEnergySavingState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_ENERGY_SAVING_SETTING;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_ENERGY_SAVING_OFF:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_ENERGY_SAVING_ON:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertSAAVoiceStatePacketPayload(eTAISEIAAirConSAAVoiceState State)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_SAA_VOICE_CONTROL;
    ServiceAccess.DataHighByte = 0x00;

    switch (State) {
        case TAISEIA_AC_SAA_ENABLE:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_SAA_DISABLE:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertDisplayBrightnessLevelPacketPayload(eTAISEIAAirConDisplayBrightnessLevel Level)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HOST_DISPLAY_BRIGHTNESS_SETTING;
    ServiceAccess.DataHighByte = 0x00;

    switch (Level) {
        case TAISEIA_AC_DISPLAY_BRIGHTNESS_BRIGHT:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_DISPLAY_BRIGHTNESS_DARK:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        case TAISEIA_AC_DISPLAY_BRIGHTNESS_OFF:
        {
            ServiceAccess.DataLowByte = 0x02;
        }
            break;
        case TAISEIA_AC_DISPLAY_BRIGHTNESS_ALL_OFF:
        {
            ServiceAccess.DataLowByte = 0x03;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertHumidifierLevelPacketPayload(eTAISEIAAirConHumidifierLevel Level)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HUMIDIFIER_SETTING;
    ServiceAccess.DataHighByte = 0x00;

    switch (Level) {
        case TAISEIA_AC_HUMIDIFIER_DISABLE:
        {
            ServiceAccess.DataLowByte = 0x00;
        }
            break;
        case TAISEIA_AC_HUMIDIFIER_LOW:
        {
            ServiceAccess.DataLowByte = 0x01;
        }
            break;
        case TAISEIA_AC_HUMIDIFIER_HIGH:
        {
            ServiceAccess.DataLowByte = 0x02;
        }
            break;
        default:
            break;
    }

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertAccumulationkWhResetAfterMaintancePacketPayload(void)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_OUTDOOR_HOST_ACCUMULATION_KWH;
    ServiceAccess.DataHighByte = 0x00;
    ServiceAccess.DataLowByte = 0x00;

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertUsedHourResetAfterMaintancePacketPayload(void)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HOURS_USED_AFTER_MAINTAINCE;
    ServiceAccess.DataHighByte = 0x00;
    ServiceAccess.DataLowByte = 0x00;

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

TaiSEIAPackedPayload_t TaiSEIA_ConvertUsedHourResetAfterCleanFilterPacketPayload(void)
{
    TaiSEIAServiceAccess_t ServiceAccess;
    memset(&ServiceAccess, 0, sizeof(TaiSEIAServiceAccess_t));

    ServiceAccess.AccessWrite = 1;
    ServiceAccess.SATypeID = TAISEIA_SA_TYPE_ID_AIR_CON;
    ServiceAccess.ServiceID = TAISEIA_AIRCON_SERVICE_ID_HOURS_USED_AFTER_CLEAN_FILTER;
    ServiceAccess.DataHighByte = 0x00;
    ServiceAccess.DataLowByte = 0x00;

    TaiSEIAPackedPayload_t Payload = TaiSEIA_ConvertAccessWritePacketPayload(ServiceAccess);

    return Payload;
}

/******************************************************
 *    TaiSEIA WriteAccess Parse Function Declarations
 ******************************************************/

eTAISEIAAirConPowerState TaiSEIA_ParsePowerStatePacketPayload(TaiSEIAServiceAccess_t ServiceAccess)
{
    eTAISEIAAirConPowerState PowerState = TAISEIA_AC_POWER_STATE_UNKNOWN;

    if (ServiceAccess.ServiceID != TAISEIA_AIRCON_SERVICE_ID_POWER_CONTROL) {
        return PowerState;
    }

    switch (ServiceAccess.DataLowByte) {
        case 0x00:
        {
            PowerState =  TAISEIA_AC_POWER_STATE_OFF;
        }
            break;
        case 0x01:
        {
            PowerState =  TAISEIA_AC_POWER_STATE_ON;
        }
            break;
        default:
            break;
    }

    return PowerState;
}
