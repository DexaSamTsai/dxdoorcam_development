//============================================================================
// File: dk_peripheralTaiSEIA.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef __DK_PERIPHERAL_TAISEIA_DEHUMIDIFIER_H__
#define __DK_PERIPHERAL_TAISEIA_DEHUMIDIFIER_H__

#pragma once

//============================================================================
// Includes
//============================================================================

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "dk_PeripheralTaiSEIA.h"



//============================================================================
// Definations
//============================================================================

/* Dehumidifier Service ID */
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL                       0x00  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL                        0x01  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING              0x02  // RW: UNIT8
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING           0x03  // RW: UNIT8
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL            0x04  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL           0x05  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_TEMPERATURE                  0x06  // R : INT8
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_HUMIDITY                     0x07  // R : UINT8
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL              0x08  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL        0x09  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_WATER_FULL_WARNING                  0x0A  // R : ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL         0x0B  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL            0x0C  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL            0x0D  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL                  0x0E  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_SIDE_VENT                           0x0F  // R : ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL                       0x10  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEFROST                             0x11  // R : ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_ERROR_CODE                          0x12  // R : ENUM16_Bit
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL                0x13  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL        0x14  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING         0x15  // RW: UINT8
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL                    0x16  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL       0x17  // RW: ENUM16_Bit
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL                   0x18  // RW: ENUM16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_CURRENT                   0x19  // R : UINT16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_VOLTAGE                   0x1A  // R : UINT16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_POWER_FACTOR              0x1B  // R : UINT16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_INSTANTANEOUS_POWER                 0x1C  // R : UINT16
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING            0x1D  // RW: UINT16

/* Dehumidifier Engineering mode Service ID */
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_AC_LOWERST_TEMPERATURE_SETTING      0x50
#define TAISEIA_DEHUMIDIFIER_SERVICE_ID_HEATER_HIGHERST_TEMPERATURE_SETTING 0x51



//============================================================================
// Enumerations
//============================================================================

/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_POWER_CONTROL_UNKNOWN          = -1,
    TAISEIA_DEHUMIDIFIER_POWER_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_POWER_CONTROL_ON,
} eTAISEIADehumidifierPowerControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AUTO_DEHUMIDIFY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_PROGRAMED_DEHUMIDIFY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CONTINUOUS_DEHUMIDIFY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_CLOTHES_DRY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_PURIFY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_MOLD_PREVENT,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_AIR_SUPPLY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_HUMAN_CONFORT,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_LOW_HUMIDITY,
    TAISEIA_DEHUMIDIFIER_MODE_CONTROL_ECONOMIZE_COMFORTABLE,
} eTAISEIADehumidifierModeControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_0,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_1,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_2,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_3,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_4,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_5,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_6,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_7,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_8,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_9,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_10,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_11,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_12,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_13,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_14,
    TAISEIA_DEHUMIDIFIER_DEHUMIDIFY_LEVEL_CONTROL_15,
} eTAISEIADehumidifierDehumidifyLevelControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_0,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_1,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_2,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_3,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_4,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_5,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_6,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_7,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_8,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_9,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_10,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_11,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_12,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_13,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_14,
    TAISEIA_DEHUMIDIFIER_CLOTHES_DRY_LEVEL_CONTROL_15,
} eTAISEIADehumidifierClothesDryLevelControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_WIND_SWINGABLE_CONTROL_ON,
} eTAISEIADehumidifierWindSwingableControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_0,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_1,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_2,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_3,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_4,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_5,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_6,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_7,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_8,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_9,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_10,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_11,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_12,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_13,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_14,
    TAISEIA_DEHUMIDIFIER_WIND_DIRECTION_LEVEL_CONTROL_15,
} eTAISEIADehumidifierWindDirectionLevelControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WATER_FULL_WARNING
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_OFF,
    TAISEIA_DEHUMIDIFIER_WATER_FULL_WARNING_ON,
} eTAISEIADehumidifierWaterFullWarning;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_CLEAN_FILTER_NOTIFY_CONTROL_ON,
} eTAISEIADehumidifierCleanFilterNotifyControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_ATMOSPHERE_LIGHT_CONTROL_ON,
} eTAISEIADehumidifierAtmosphereLightControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_0,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_1,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_2,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_3,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_4,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_5,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_6,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_7,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_8,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_9,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_10,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_11,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_12,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_13,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_14,
    TAISEIA_DEHUMIDIFIER_AIR_PURIFY_LEVEL_CONTROL_15,
} eTAISEIADehumidifierAirPurifyLevelControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_AUTO,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_1,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_2,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_3,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_4,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_5,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_6,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_7,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_8,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_9,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_10,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_11,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_12,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_13,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_14,
    TAISEIA_DEHUMIDIFIER_WIND_SPEED_CONTROL_15,
} eTAISEIADehumidifierWindSpeedControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SIDE_VENT
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_SIDE_VENT_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_SIDE_VENT_OFF,
    TAISEIA_DEHUMIDIFIER_SIDE_VENT_ON,
} eTAISEIADehumidifierSideVent;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_SILENT,
    TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON,
    TAISEIA_DEHUMIDIFIER_SOUND_CONTROL_BUTTON_AND_WATERFULL,
} eTAISEIADehumidifierSoundControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEFROST
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_DEFROST_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_DEFROST_OFF,
    TAISEIA_DEHUMIDIFIER_DEFROST_ON,
} eTAISEIADehumidifierDefrost;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_MOLD_PREVENT_CONTROL_ON,
} eTAISEIADehumidifierMoldPreventControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_HIGH_HUMIDITY_NOTIFY_CONTROL_ON,
} eTAISEIADehumidifierHighHumidityNotifyControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_OFF,
    TAISEIA_DEHUMIDIFIER_KEY_LOCK_CONTROL_ON,
} eTAISEIADehumidifierKeyLockControl;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL
 */
typedef enum {
    TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_UNKNOWN           = -1,
    TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_ENABLE,
    TAISEIA_DEHUMIDIFIER_SAA_VOICE_CONTROL_DISABLE,
} eTAISEIADehumidifierSaaVoiceControl;



//============================================================================
// Structure
//============================================================================

/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedPowerControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        AutoDehumidify;
    bool        ProgramedDehumidify;
    bool        ContinuousDehumidify;
    bool        ClothesDry;
    bool        AirPurify;
    bool        MoldPrevent;
    bool        AirSupply;
    bool        HumanComfort;
    bool        LowHumidity;
    bool        EconomizeComfortable;
} TaiSEIADehumidifierSupportedModeControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING
 */
typedef struct {
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;
} TaiSEIADehumidifierSupportedOperationTimeSetting_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING
 */
typedef struct {
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;
} TaiSEIADehumidifierSupportedRelativeHumiditySetting_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIADehumidifierSupportedHumidifierLevelBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIADehumidifierSupportedHumidifierLevelBitInfo_t    bits;
} TaiSEIADehumidifierSupportedHumidifierLevelBitInfo_u;
typedef struct {
    bool        Writable;
    TaiSEIADehumidifierSupportedHumidifierLevelBitInfo_u     BitInfo;
} TaiSEIADehumidifierSupportedHumidifierLevelControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIADehumidifierSupportedClothesDryerLevelBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIADehumidifierSupportedClothesDryerLevelBitInfo_t    bits;
} TaiSEIADehumidifierSupportedClothesDryerLevelBitInfo_u;
typedef struct {
    bool        Writable;
    TaiSEIADehumidifierSupportedClothesDryerLevelBitInfo_u     BitInfo;
} TaiSEIADehumidifierSupportedClothesDryerLevelControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_TEMPERATURE
 */
typedef struct {
    bool        Writable;
    int8_t      Minimum;
    int8_t      Maximum;
} TaiSEIADehumidifierSupportedIndoorTemperature_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_HUMIDITY
 */
typedef struct {
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;
} TaiSEIADehumidifierSupportedIndoorHumidity_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedWindSwingableControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL
 */
typedef struct {
    uint16_t        Level0_b0       :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIADehumidifierSupportedWindDirectionLevelBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIADehumidifierSupportedWindDirectionLevelBitInfo_t    bits;
} TaiSEIADehumidifierSupportedWindDirectionLevelBitInfo_u;
typedef struct {
    bool        Writable;
    TaiSEIADehumidifierSupportedWindDirectionLevelBitInfo_u     BitInfo;
} TaiSEIADehumidifierSupportedWindDirectionLevelControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WATER_FULL_WARNING
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedWaterFullWarning_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedAtmosphereLightControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL
 */
typedef struct {
    uint16_t        Disable_b0      :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIADehumidifierSupportedAirPurifyLevelBitInfo_t;
typedef union {
    uint16_t                                                flag;
    TaiSEIADehumidifierSupportedAirPurifyLevelBitInfo_t   bits;
} TaiSEIADehumidifierSupportedAirPurifyLevelBitInfo_u;
typedef struct {
    bool                                                    Writable;
    TaiSEIADehumidifierSupportedAirPurifyLevelBitInfo_u    BitInfo;
} TaiSEIADehumidifierSupportedAirPurifyLevelControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL
 */
typedef struct {
    uint16_t        Auto_b0         :1;
    uint16_t        Level1_b1       :1;
    uint16_t        Level2_b2       :1;
    uint16_t        Level3_b3       :1;
    uint16_t        Level4_b4       :1;
    uint16_t        Level5_b5       :1;
    uint16_t        Level6_b6       :1;
    uint16_t        Level7_b7       :1;
    uint16_t        Level8_b8       :1;
    uint16_t        Level9_b9       :1;
    uint16_t        Level10_b10     :1;
    uint16_t        Level11_b11     :1;
    uint16_t        Level12_b12     :1;
    uint16_t        Level13_b13     :1;
    uint16_t        Level14_b14     :1;
    uint16_t        Level15_b15     :1;
} TaiSEIADehumidifierSupportedWindSpeedLevelBitInfo_t;
typedef union {
    uint16_t                                                flag;
    TaiSEIADehumidifierSupportedWindSpeedLevelBitInfo_t   bits;
} TaiSEIADehumidifierSupportedWindSpeedLevelBitInfo_u;
typedef struct {
    bool                                                    Writable;
    TaiSEIADehumidifierSupportedWindSpeedLevelBitInfo_u    BitInfo;
} TaiSEIADehumidifierSupportedWindSpeedLevelControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SIDE_VENT
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedSideVent_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Silent;
    bool        Button;
    bool        ButtonAndWaterFull;
} TaiSEIADehumidifierSupportedSoundControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEFROST
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedDefrost_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ERROR_CODE
 */
typedef struct {
    uint16_t        General_b0      :1;
    uint16_t        Error1_b1       :1;
    uint16_t        Error2_b2       :1;
    uint16_t        Error3_b3       :1;
    uint16_t        Error4_b4       :1;
    uint16_t        Error5_b5       :1;
    uint16_t        Error6_b6       :1;
    uint16_t        Error7_b7       :1;
    uint16_t        Error8_b8       :1;
    uint16_t        Error9_b9       :1;
    uint16_t        Error10_b10     :1;
    uint16_t        Error11_b11     :1;
    uint16_t        Error12_b12     :1;
    uint16_t        Error13_b13     :1;
    uint16_t        Error14_b14     :1;
    uint16_t        Error15_b15     :1;
} TaiSEIADehumidifierSupportedErrorCodeBitInfo_t;
typedef union {
    uint16_t                                                flag;
    TaiSEIADehumidifierSupportedErrorCodeBitInfo_t   bits;
} TaiSEIADehumidifierSupportedErrorCodeBitInfo_u;
typedef struct {
    bool                                                    Writable;
    TaiSEIADehumidifierSupportedErrorCodeBitInfo_u    BitInfo;
} TaiSEIADehumidifierSupportedErrorCode_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedMoldPreventControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING
 */
typedef struct {
    bool        Writable;
    uint8_t     Minimum;
    uint8_t     Maximum;
} TaiSEIADehumidifierSupportedHighHumidityValueSetting_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Off;
    bool        On;
} TaiSEIADehumidifierSupportedKeyLockControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL
 */
typedef struct {
    uint16_t        PowerControl_b0             :1;
    uint16_t        OperationControl_b1         :1;
    uint16_t        HumiditySetting_b2          :1;
    uint16_t        WindSpeedSetting_b3         :1;
    uint16_t        WindDirectionSetting_b4     :1;
    uint16_t        StopButtonEnable_b5         :1;
    uint16_t        Reserve_b6_15               :9;
} TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_t;
typedef union {
    uint16_t                                                            flag;
    TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_t    bits;
} TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u;
typedef struct {
    bool                                                                Writable;
    TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u     BitInfo;
} TaiSEIADehumidifierSupportedControllerProhibitedControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL
 */
typedef struct {
    bool        Writable;
    bool        Enable;
    bool        Disable;
} TaiSEIADehumidifierSupportedSaaVoiceControl_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_CURRENT
 */
typedef struct {
    bool        Writable;
    uint16_t    Maximum;
} TaiSEIADehumidifierSupportedOperatingCurrent_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_VOLTAGE
 */
typedef struct {
    bool        Writable;
    uint16_t    Maximum;
} TaiSEIADehumidifierSupportedOperatingVoltage_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_POWER_FACTOR
 */
typedef struct {
    bool        Writable;
    uint16_t    Maximum;
} TaiSEIADehumidifierSupportedOperatingPowerFactor_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INSTANTANEOUS_POWER
 */
typedef struct {
    bool        Writable;
    uint16_t    Maximum;
} TaiSEIADehumidifierSupportedInstantaneousPower_t;


/**
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING
 */
typedef struct {
    bool        Writable;
    uint8_t     Minimum;
} TaiSEIADehumidifierSupportedCumulativePowerSetting_t;



#ifdef __cplusplus
extern "C" {
#endif


//============================================================================
// TaiSEAI Dehumidifier Specific Functions
//============================================================================

/**
 * Get Taiseia Dehumidifier Supported Power Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedPowerControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL
 */
TaiSEIADehumidifierSupportedPowerControl_t TaiSEIADehumidifier_GetSupportedPowerControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Mode Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedModeControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL
 */
TaiSEIADehumidifierSupportedModeControl_t TaiSEIADehumidifier_GetSupportedModeControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Operation Mode
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedOperationTime_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING
 */
TaiSEIADehumidifierSupportedOperationTimeSetting_t TaiSEIADehumidifier_GetSupportedOperationTimeSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Indoor Humidity
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedRelativeHumiditySetting_t, if Writable flag is
 *         true means writable otherwise means not, Minimum and Maximum
 *         Parameter indicates the minimum and maximum value of humidity.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING
 */
TaiSEIADehumidifierSupportedRelativeHumiditySetting_t TaiSEIADehumidifier_GetSupportedRelativeHumiditySetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Humidifier Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedHumidifierLevelControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL
 */
TaiSEIADehumidifierSupportedHumidifierLevelControl_t TaiSEIADehumidifier_GetSupportedHumidifierLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Clothes Dryer Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedClothesDryerLevelControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL
 */
TaiSEIADehumidifierSupportedClothesDryerLevelControl_t TaiSEIADehumidifier_GetSupportedClothesDryerLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Indoor Temperature
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedIndoorTemperature_t, if Writable
 *         flag is true means writable otherwise means not, Minimum and Minimum
 *         Parameter indicates the Lowerst and Higherst temperature range.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_TEMPERATURE
 */
TaiSEIADehumidifierSupportedIndoorTemperature_t TaiSEIADehumidifier_GetSupportedIndoorTemperature (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Indoor Humidity
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedIndoorHumidity_t, if Writable
 *         flag is true means writable otherwise means not, Minimum and Minimum
 *         Parameter indicates the Lowerst and Higherst temperature range.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_HUMIDITY
 */
TaiSEIADehumidifierSupportedIndoorHumidity_t TaiSEIADehumidifier_GetSupportedIndoorHumidity (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Wind Swingable Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedWindSwingableControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL
 */
TaiSEIADehumidifierSupportedWindSwingableControl_t TaiSEIADehumidifier_GetSupportedWindSwingableControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Wind Direction Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedWindDirectionLevelControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL
 */
TaiSEIADehumidifierSupportedWindDirectionLevelControl_t TaiSEIADehumidifier_GetSupportedWindDirectionLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Water Full Warning
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedWaterFullWarning_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WATER_FULL_WARNING
 */
TaiSEIADehumidifierSupportedWaterFullWarning_t TaiSEIADehumidifier_GetSupportedWaterFullWarning (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Clean Filter Notify
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
TaiSEIADehumidifierSupportedCleanFilterNotifyControl_t TaiSEIADehumidifier_GetSupportedCleanFilterNotify (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Atmosphere Light Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedAtmosphereLightControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL
 */
TaiSEIADehumidifierSupportedAtmosphereLightControl_t TaiSEIADehumidifier_GetSupportedAtmosphereLightControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Air Purifier Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedAirPurifyLevelControl_t
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL
 */
TaiSEIADehumidifierSupportedAirPurifyLevelControl_t TaiSEIADehumidifier_GetSupportedAirPurifyLevelControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Wind Speed Level
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 * @return TaiSEIADehumidifierSupportedWindSpeedLevelControl_t, in structure, true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL
 */
TaiSEIADehumidifierSupportedWindSpeedLevelControl_t TaiSEIADehumidifier_GetSupportedWindSpeedLevelControl (
    TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Side Vent
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 * @return TaiSEIADehumidifierSupportedSideVent_t, in structure, true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SIDE_VENT
 */
TaiSEIADehumidifierSupportedSideVent_t TaiSEIADehumidifier_GetSupportedSideVent (
    TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Sound Control
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 * @return TaiSEIADehumidifierSupportedSoundControl_t, in structure, true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL
 */
TaiSEIADehumidifierSupportedSoundControl_t TaiSEIADehumidifier_GetSupportedSoundControl (
    TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Defrost
 *
 * @param ServiceInfo TaiSEIASupportedServiceInfo_t
 * @return TaiSEIADehumidifierSupportedDefrost_t, in structure, true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEFROST
 */
TaiSEIADehumidifierSupportedDefrost_t TaiSEIADehumidifier_GetSupportedDefrost (
    TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Error Code
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedErrorCode_t, if Writable flag is true
 *         means writable otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ERROR_CODE
 */
TaiSEIADehumidifierSupportedErrorCode_t TaiSEIADehumidifier_GetSupportedErrorCode (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Mold Prevent State
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedMoldPreventControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL
 */
TaiSEIADehumidifierSupportedMoldPreventControl_t TaiSEIADehumidifier_GetSupportedMoldPreventControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported High Humidity Notify Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL
 */
TaiSEIADehumidifierSupportedHighHumidityNotifyControl_t TaiSEIADehumidifier_GetSupportedHighHumidifyNofityControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported High Humidity Value Setting
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedHighHumidityValueSetting_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING
 */
TaiSEIADehumidifierSupportedHighHumidityValueSetting_t TaiSEIADehumidifier_GetSupportedHighHumidifyValueSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Key Lock Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedKeyLockControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL
 */
TaiSEIADehumidifierSupportedKeyLockControl_t TaiSEIADehumidifier_GetSupportedKeyLockControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Controller Prohibited Info
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedControllerProhibitedControl_t, if Writable
 *         flag is true means writable otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL
 */
TaiSEIADehumidifierSupportedControllerProhibitedControl_t TaiSEIADehumidifier_GetSupportedControllerProhibitedControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported SAA Voice State
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedSaaVoiceControl_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL
 */
TaiSEIADehumidifierSupportedSaaVoiceControl_t TaiSEIADehumidifier_GetSupportedSaaVoiceControl (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Operating Current
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedOperatingCurrent_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_CURRENT
 */
TaiSEIADehumidifierSupportedOperatingCurrent_t TaiSEIADehumidifier_GetSupportedOperatingCurrent (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Operating Voltage
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedOperatingVoltage_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_VOLTAGE
 */
TaiSEIADehumidifierSupportedOperatingVoltage_t TaiSEIADehumidifier_GetSupportedOperatingVoltage (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Operating Power Factor
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedOperatingPowerFactor_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_POWER_FACTOR
 */
TaiSEIADehumidifierSupportedOperatingPowerFactor_t TaiSEIADehumidifier_GetSupportedOperatingPowerFactor (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Instantaneous Power
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedInstantaneousPower_t, in structure,
 *         true means support otherwise means not.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INSTANTANEOUS_POWER
 */
TaiSEIADehumidifierSupportedInstantaneousPower_t TaiSEIADehumidifier_GetSupportedInstantaneousPower (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Supported Outdoor Cumulative Power in kWh
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedCumulativePowerSetting_t, if
 *         Writable flag is true means writable otherwise means not, Maximum
 *         Parameter indicates the maximum value of accumulation kWh.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING
 */
TaiSEIADehumidifierSupportedCumulativePowerSetting_t TaiSEIADehumidifier_GetSupportedCumulativePowerSetting (
    IN  TaiSEIASupportedServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Power Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierPowerControl that indicate current Power State.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL
 */
eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_GetCurrentPowerControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Mode Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierModeControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL
 */
eTAISEIADehumidifierModeControl TaiSEIADehumidifier_GetCurrentModeControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Operation Time Setting
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING
 */
uint8_t TaiSEIADehumidifier_GetCurrentOperationTimeSetting (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Relative Humidity Setting
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING
 */
uint8_t TaiSEIADehumidifier_GetCurrentRelativeHumiditySetting (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Dehumidify Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierDehumidifyLevelControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL
 */
eTAISEIADehumidifierDehumidifyLevelControl TaiSEIADehumidifier_GetCurrentDehumidifyLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Clothes Dry Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierDehumidifyLevelControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL
 */
eTAISEIADehumidifierClothesDryLevelControl TaiSEIADehumidifier_GetCurrentClothesDryLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Indoor Temperature
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_TEMPERATURE
 */
int8_t TaiSEIADehumidifier_GetCurrentIndoorTemperature (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Indoor Humidity
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INDOOR_HUMIDITY
 */
uint8_t TaiSEIADehumidifier_GetCurrentIndoorHumidity (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Wind Swingable Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierWindSwingableControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL
 */
eTAISEIADehumidifierWindSwingableControl TaiSEIADehumidifier_GetCurrentWindSwingableControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Wind Direction Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierWindDirectionLevelControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL
 */
eTAISEIADehumidifierWindDirectionLevelControl TaiSEIADehumidifier_GetCurrentWindDirectionLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Water Full Warning
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierWaterFullWarning that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WATER_FULL_WARNING
 */
eTAISEIADehumidifierWaterFullWarning TaiSEIADehumidifier_GetCurrentWaterFullWarning (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Clean Filter Notify Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierCleanFilterNotifyControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
eTAISEIADehumidifierCleanFilterNotifyControl TaiSEIADehumidifier_GetCurrentCleanFilterNotifyControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Atmosphere Light Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierAtmosphereLightControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL
 */
eTAISEIADehumidifierAtmosphereLightControl TaiSEIADehumidifier_GetCurrentAtmosphereLightControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Air Purify Level Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierAirPurifyLevelControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL
 */
eTAISEIADehumidifierAirPurifyLevelControl TaiSEIADehumidifier_GetCurrentAirPurifyLevelControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Wind Speed Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierWindSpeedControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL
 */
eTAISEIADehumidifierWindSpeedControl TaiSEIADehumidifier_GetCurrentWindSpeedControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Side Vent
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierSideVent that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SIDE_VENT
 */
eTAISEIADehumidifierSideVent TaiSEIADehumidifier_GetCurrentSideVent (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Sound Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierSoundControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL
 */
eTAISEIADehumidifierSoundControl TaiSEIADehumidifier_GetCurrentSoundControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Defrost
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierDefrost that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEFROST
 */
eTAISEIADehumidifierDefrost TaiSEIADehumidifier_GetCurrentDefrost (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Error Code
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedErrorCodeBitInfo_u that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ERROR_CODE
 */
TaiSEIADehumidifierSupportedErrorCodeBitInfo_u TaiSEIADehumidifier_GetCurrentErrorCode (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Mold Prevent Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierMoldPreventControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL
 */
eTAISEIADehumidifierMoldPreventControl TaiSEIADehumidifier_GetCurrentMoldPreventControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current High Humidity Notify Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierHighHumidityNotifyControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL
 */
eTAISEIADehumidifierHighHumidityNotifyControl TaiSEIADehumidifier_GetCurrentHighHumidityNotifyControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current High Humidity Value Setting
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING
 */
uint8_t TaiSEIADehumidifier_GetCurrentHighHumidityValueSetting (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Key Lock Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierKeyLockControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL
 */
eTAISEIADehumidifierKeyLockControl TaiSEIADehumidifier_GetCurrentKeyLockControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Controller Prohibited Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL
 */
TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u TaiSEIADehumidifier_GetCurrentControllerProhibitedControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current SAA Voice Control
 *
 * @param serviceInfo the input parameter
 * @return an instance of eTAISEIADehumidifierSaaVoiceControl that indicate current
 *         Operation Mode.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL
 */
eTAISEIADehumidifierSaaVoiceControl TaiSEIADehumidifier_GetCurrentSaaVoiceControl (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Operating Current
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_CURRENT
 */
uint16_t TaiSEIADehumidifier_GetCurrentOperatingCurrent (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Operating Voltage
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_VOLTAGE
 */
uint16_t TaiSEIADehumidifier_GetCurrentOperatingVoltage (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Operating Power Factor
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATING_POWER_FACTOR
 */
uint16_t TaiSEIADehumidifier_GetCurrentOperatingPowerFactor (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Instantaneous Power
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_INSTANTANEOUS_POWER
 */
uint16_t TaiSEIADehumidifier_GetCurrentInstantaneousPower (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Get Taiseia Dehumidifier Current Cumulative Power
 *
 * @param serviceInfo the input parameter
 * @return value that indicates the target setting.
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING
 */
float TaiSEIADehumidifier_GetCurrentCumulativePower (
    TaiSEIAServiceInfo_t serviceInfo
);


/**
 * Convert Taiseia Dehumidifier Power Control State to Payload
 *
 * @param State A valid state of eTAISEIADehumidifierPowerControl
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_POWER_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertPowerControlPacketPayload (
    eTAISEIADehumidifierPowerControl control
);


/**
 * Convert Taiseia Dehumidifier Mode Control to Payload
 *
 * @param Mode A valid state of eTAISEIAAirConOperationMode
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MODE_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertModeControlPacketPayload (
    eTAISEIADehumidifierModeControl control
);


/**
 * Convert Taiseia Dehumidifier Operation Time Setting to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_OPERATION_TIME_SETTING
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertOperationTimeSettingPacketPayload (
    uint8_t value
);


/**
 * Convert Taiseia Dehumidifier Relative Humidity Setting to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_RELATIVE_HUMIDITY_SETTING
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertRelativeHumiditySettingPacketPayload (
    uint8_t value
);


/**
 * Convert Taiseia Dehumidifier Humidity Level Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_DEHUMIDIFY_LEVEL_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHumidityLevelControlPacketPayload (
    eTAISEIADehumidifierDehumidifyLevelControl control
);


/**
 * Convert Taiseia Dehumidifier Clothes Dry Level Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLOTHES_DRY_LEVEL_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertClothesDryLevelControlPacketPayload (
    eTAISEIADehumidifierClothesDryLevelControl control
);


/**
 * Convert Taiseia Dehumidifier Wind Swingable Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SWINGABLE_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindSwingableControlPacketPayload (
    eTAISEIADehumidifierWindSwingableControl control
);


/**
 * Convert Taiseia Dehumidifier Wind Direction Level Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_DIRECTION_LEVEL_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindDirectionLevelControlPacketPayload (
    eTAISEIADehumidifierWindDirectionLevelControl control
);


/**
 * Convert Taiseia Dehumidifier Clean Filter Notify Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CLEAN_FILTER_NOTIFY_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertCleanFilterNotifyControlPacketPayload (
    eTAISEIADehumidifierCleanFilterNotifyControl control
);


/**
 * Convert Taiseia Dehumidifier Atmosphere Light Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_ATMOSPHERE_LIGHT_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertAtmosphereLightControlPacketPayload (
    eTAISEIADehumidifierAtmosphereLightControl control
);


/**
 * Convert Taiseia Dehumidifier Air Purify Level Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_AIR_PURIFY_LEVEL_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertAirPurifyLevelControlPacketPayload (
    eTAISEIADehumidifierAirPurifyLevelControl control
);


/**
 * Convert Taiseia Dehumidifier Wind Speed Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_WIND_SPEED_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertWindSpeedControlPacketPayload (
    eTAISEIADehumidifierWindSpeedControl control
);


/**
 * Convert Taiseia Dehumidifier Sound Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SOUND_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertSoundControlPacketPayload (
    eTAISEIADehumidifierSoundControl control
);


/**
 * Convert Taiseia Dehumidifier Mold Prevent Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_MOLD_PREVENT_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertMoldPreventControlPacketPayload (
    eTAISEIADehumidifierMoldPreventControl control
);


/**
 * Convert Taiseia Dehumidifier High Humidity Notify Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_NOTIFY_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHighHumidityNotifyControlPacketPayload (
    eTAISEIADehumidifierHighHumidityNotifyControl control
);


/**
 * Convert Taiseia Dehumidifier High Humidity Value Setting to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_HIGH_HUMIDITY_VALUE_SETTING
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertHighHumidityValueSettingPacketPayload (
    uint8_t value
);


/**
 * Convert Taiseia Dehumidifier Key Lock Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_KEY_LOCK_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertKeyLockControlPacketPayload (
    eTAISEIADehumidifierKeyLockControl control
);


/**
 * Convert Taiseia Dehumidifier Controller Prohibited Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CONTROLLER_PROHIBITED_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertControllerProhibitControlPacketPayload (
    TaiSEIADehumidifierSupportedControllerProhibitedBitInfo_u control
);


/**
 * Convert Taiseia Dehumidifier SAA Voice Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_SAA_VOICE_CONTROL
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertSaaVoiceControlPacketPayload (
    eTAISEIADehumidifierSaaVoiceControl control
);


/**
 * Convert Taiseia Dehumidifier Cumulative Power Control to Payload
 *
 * @param value target value
 * @see TaiSEIAPackedPayload_t, NOTE: if PackedPayloadLen = 0 then given
 *      ServiceAccess is invalid!
 * @see TAISEIA_DEHUMIDIFIER_SERVICE_ID_CUMULATIVE_POWER_SETTING
 */
TaiSEIAPackedPayload_t TaiSEIADehumidifier_ConvertCumulativePowerControlPacketPayload (
    uint16_t value
);
    
/********************************************************************
 *   TaiSEIA Dehumidifier PackedPayload Parse Function Declarations
 ********************************************************************/

/** Parse Power Control State from TaiSEIAPackedPayload_t
 *
 * @param[in]  PacketPayload 	 : 	TaiSEIAPackedPayload_t
 *
 * @return eTAISEIADehumidifierPowerControl that indicate Power State.
 */
    
eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_ParsePowerControlPacketPayload (
    TaiSEIAPackedPayload_t PacketPayload
);

/******************************************************************
 *   TaiSEIA Dehumidifier WriteAccess Parse Function Declarations
 ******************************************************************/

/** Parse Power Control State from TaiSEIAServiceAccess_t
 *
 * @param[in]  ServiceAccess 	 : 	TaiSEIAServiceAccess_t
 *
 * @return eTAISEIADehumidifierPowerControl that indicate Power State.
 */

eTAISEIADehumidifierPowerControl TaiSEIADehumidifier_ParsePowerControlServiceAccess (
    TaiSEIAServiceAccess_t ServiceAccess
);

#ifdef __cplusplus
} /*extern "C" */
#endif

#endif //BLE_PERIPHERAL_TAISEIA_INFO_H
