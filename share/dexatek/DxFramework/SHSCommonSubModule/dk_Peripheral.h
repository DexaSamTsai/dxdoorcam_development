//============================================================================
// File: dk_peripheral.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef BLE_PERIPHERAL_INFO_H
#define BLE_PERIPHERAL_INFO_H

#pragma once

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Defines
 ******************************************************/

/** @defgroup 128 BIT DEXATEK_SERVICE_CHARACTERISTIC_TABLE
 * @{
 */
#define DK_SERVICE_SYSTEM_UTILS_UUID                                                0xFA01
#define     DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_IDENTIFICATION_UUID                  0xFB14
#define     DK_CHARACTERISTIC_SYSTEM_UTILS_PASSCODE_CONFIG_UUID                     0xFB20
#define     DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO                                 0xFB21
#define     DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_HEADER_CONFIG_UUID            0xFB22
#define     DK_CHARACTERISTIC_SYSTEM_UTIL_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_UUID  0xFB23
#define     DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID                      0xFB24
#define     DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID  0xFB25
#define     DK_CHARACTERISTIC_SYSTEM_UTIL_RTC_ACCESS_UUID                           0xFB26
#define DK_SERVICE_POWER_PLUG_UUID                                                  0xFA70
#define     DK_CHARACTERISTIC_POWER_CALIBRATION_UUID                                0xFB70  // Power Plug Power calibration request
#define     DK_CHARACTERISTIC_POWER_NOTIFICATION_UUID                               0xFB71  // Power Plug Power notification
#define     DK_CHARACTERISTIC_POWER_CONFIG_UUID                                     0xFB72  // Power Plug Power configuration (see eConfiguration)
#define DK_SERVICE_LIGHT_BULB_UUID                                                  0xFA80
#define     DK_CHARACTERISTIC_LIGHT_BULB_DIMMER_UUID                                0xFB80
#define DK_SERVICE_SMOKE_DETECTOR_UUID                                              0xFAA0
#define     DK_CHARACTERISTIC_ALART_CONTROL_UUID                                    0xFBA0
#define DK_SERVICE_SHOCK_SENSOR_UUID                                                0xFAB0
#define     DK_CHARACTERISTIC_SHOCK_THRESHOLD_CONFIG_UUID                           0xFBB0
#define DK_SERVICE_INLET_SWITCH_UUID                                                0xFAC0
#define     DK_CHARACTERISTIC_INLETSWITCH_CONTROL_UUID                              0xFBC0  //See eInletSwitchControl
#define DK_SERVICE_MOTION_SENSOR_UUID                                               0xFAD0
#define     DK_CHARACTERISTIC_MOTION_SENSITIVITY_CONTROL_UUID                       0xFBD0
#define     DK_CHARACTERISTIC_MOTION_DELAY_CONTROL_UUID                             0xFBD1
#define DK_SERVICE_THERMOSTAT_CB_UUID                                               0xFAE0
#define     DK_CHARACTERISTIC_THERMOSTAT_CB_SET_TEMPERATURE_UUID                    0xFBE0
#define     DK_CHARACTERISTIC_THERMOSTAT_CB_SET_SCHEDULE_UUID                       0xFBE1
#define     DK_CHARACTERISTIC_THERMOSTAT_CB_SET_HOLIDAY_UUID                        0xFBE2
#define     DK_CHARACTERISTIC_THERMOSTAT_CB_GENERAL_CONFIGE_UUID                    0xFBE3
#define DK_SERVICE_POWER_SOCKET_UUID                                                0xFAF0
#define     DK_CHARACTERISTIC_POWER_SOCKET_NOTIFICATION_UUID                        0xFBF0
#define     DK_CHARACTERISTIC_POWER_SOCKET_CONFIG_UUID                              0xFBF1  //See ePowerSocketConfig
#define DK_SERVICE_DOOR_LOCK_UUID                                                   0x1A10
#define     DK_CHARACTERISTIC_DOOR_LOCK_CONTROL_UUID                                0x1B10
#define DK_SERVICE_SIREN_UUID                                                       0x1A20
#define     DK_CHARACTERISTIC_SIREN_CONTROL_UUID                                    0x1B20
#define     DK_CHARACTERISTIC_SIREN_AUTO_SHUTDOWN_CTRL_UUID                         0x1B21
#define     DK_CHARACTERISTIC_SIREN_BATTERY_REPLACE_GRACE_TIME_CONFIG_UUID          0x1B22
#define DK_SERVICE_REMOTE_CONTROL_UUID                                              0x1A30
#define     DK_CHARACTERISTIC_REMOTE_CONTROL_RTC_CONFIG_UUID                        0x1B30
#define DK_SERVICE_LED_ADAPTOR_CONTROL_UUID                                         0x1A40
#define     DK_CHARACTERISTIC_LED_ADAPTOR_CONTROL_UUID                              0x1B40
#define DK_SERVICE_RGB_LIGHT_CONTROL_UUID                                           0x1A50
#define     DK_CHARACTERISTIC_RGB_MAIN_SWITCH_CONTROL_UUID                          0x1B50
#define     DK_CHARACTERISTIC_RGB_MAIN_DIMMER_CONTROL_UUID                          0x1B51
#define     DK_CHARACTERISTIC_RGB_MAIN_RGB_CONTROL_UUID                             0x1B52
#define DK_SERVICE_LIGHT_STRIP_CONTROL_UUID                                         0x1A60
#define     DK_CHARACTERISTIC_LIGHT_STRIP_SWITCH_CONTROL_UUID                       0x1B60
#define     DK_CHARACTERISTIC_LIGHT_STRIP_DIMMER_CONTROL_UUID                       0x1B61
#define     DK_CHARACTERISTIC_LIGHT_STRIP_HUE_CONTROL_UUID                          0x1B62
#define     DK_CHARACTERISTIC_LIGHT_STRIP_SATURATION_CONTROL_UUID                   0x1B63
#define     DK_CHARACTERISTIC_LIGHT_STRIP_HSV_SWITCH_CONTROL_UUID                   0x1B64
#define DK_SERVICE_TAISEIA_CONTROL_UUID                                             0xEC00
#define     DK_CHARACTERISTIC_TAISEIA_GET_REGISTRATION_INFO_UUID                    0xED00
#define     DK_CHARACTERISTIC_TAISEIA_WRITE_PACKET_UUID                             0xED01
#define     DK_CHARACTERISTIC_TAISEIA_GET_CURRENT_STATUS_UUID                       0xED03
#define     DK_CHARACTERISTIC_TAISEIA_NOTIFICATION_UUID                             0xED04

#define DK_SERVICE_IR_REMOTE_UUID                                                   "0D278010F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_NEW_REMOTE_LEARNING_UUID                              "0D278011F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_INSERT_REMOTE_KEY_LEARNING_UUID                       "0D278012F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_UPDATE_REMOTE_KEY_LEARNING_UUID                       "0D278013F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_DELETE_REMOTE_UUID                                    "0D278014F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_DELETE_KEY_REMOTE_UUID                                "0D278015F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SEND_KEY_REMOTE_UUID                                  "0D278016F0D4469DAFD3605A6EBBDB13"
#define DK_SERVICE_ALARM_CENTRAL_UUID                                               "0D278020F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_CHANGE_ALARM_ARMING_MODE_UUID                         "0D278021F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_JOB_CONDITION_UPDATE_UUID                             "0D278022F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_JOB_EXECUTION_UPDATE_UUID                             "0D278023F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_ZONE_MAPPING_MODE_UUID                                "0D278024F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SET_EMERGENCY_PHONE_NUMBERS_UUID                      "0D278025F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SET_PIN_CODE_UUID                                     "0D278026F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SET_ARMING_DELAY_UUID                                 "0D278027F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SENSOR_DETECT_UUID                                    "0D278028F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_REMOTE_CONTROL_MODE_CHANGE_UUID                       "0D278029F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SET_EMERGENCY_PHONE_NUMBERS_WITH_NAMES_UUID           "0D27802AF0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_SET_PIN_CODE_EXT_UUID                                 "0D27802BF0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_JOB_EXECUTION_AND_TIMEOUT_UPDATE_UUID                 "0D27802CF0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_GENERIC_CONFIGS_UUID                                  "0D27802DF0D4469DAFD3605A6EBBDB13"
#define DK_SERVICE_IPCAM_UUID                                                       "0D278030F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_STREAMING_CONFIG_UUID                                 "0D278031F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_EVENT_REQUEST_UUID                                    "0D278032F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_WIN_MOTION_DET_CONFIG_UUID                            "0D278033F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_VIDEO_CONFIG_UUID                                     "0D278034F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_CAPTURE_IMAGE_UUID                                    "0D278035F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_DOWN_STREAMING_UUID                                   "0D278036F0D4469DAFD3605A6EBBDB13"
#define DK_SERVICE_ERGO_DESK_UUID                                                   "0D278040F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_ERGO_DESK_HEIGHT_STATUS_UUID                          "0D278041F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_ERGO_DESK_REMINDER_UUID                               "0D278042F0D4469DAFD3605A6EBBDB13"
#define DK_SERVICE_TAISEIA_UUID                                                     "0D278050F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_REGISTRATION_FULL_INFO_UUID                           "0D278051F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_PACKET_WRITE_INFO_UUID                                "0D278052F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_PACKET_READ_INFO_UUID                                 "0D278053F0D4469DAFD3605A6EBBDB13"
#define     DK_CHARACTERISTIC_CURRENT_STATUS_LIST_UUID                              "0D278054F0D4469DAFD3605A6EBBDB13"

// For HomeKit access
//       Services
#define APPLE_SERVICE_SWITHES_UUID                                                  "000000490000100080000026BB765291"
#define APPLE_SERVICE_LIGHTING_UUID                                                 "000000430000100080000026BB765291"

//          Characteristics
#define     APPLE_CHARACTERISTIC_ON_UUID                                            "000000250000100080000026BB765291"
#define     APPLE_CHARACTERISTIC_BRIGHTNESS_UUID                                    "000000080000100080000026BB765291"


/** @} End 128 BIT DEXATEK_SERVICE_CHARACTERISTIC_TABLE */


//System Utils Passcode Config Command
#define SYSTEM_UTIL_PASSCODE_CONFIG_UNLOCK              1
#define SYSTEM_UTIL_PASSCODE_CONFIG_CHANGE_PASSCODE     2

//System Utils DK Adv. header config Command
#define SYSTEM_UTIL_DK_ADV_HEADER_SET_PAIR_BIT    		1
#define SYSTEM_UTIL_DK_ADV_HEADER_CLEAR_PAIR_BIT  		2
#define SYSTEM_UTIL_DK_ADV_HEADER_SET_PAIR_BIT_PROVIDED_MAC_ADDRESS  		3

//System Utils Security Command / Sub-Command
#define SYSTEM_UTIL_SECURITY_ACCESS_CHALLENGE                   100     //Challenge the security
#define SYSTEM_UTIL_SECURITY_ACCESS_CHANGE_SECURITY             200     //Change the security - sub command/parameter required for securit type change
#define   SYSTEM_UTIL_SA_CS_SUB_SECURITY_METHOD_NONE            210     //Clear all security
#define   SYSTEM_UTIL_SA_CS_SUB_SECURITY_LEVEL_PIN_CODE_HASH    220     //The last 16 byte of data is a ramdon generated value that is going to be using for
                                                                        //pin code hash MD2(16byte+Pincode) = AES Key.
#define SYSTEM_UTILS_SECURITY_PIN_CODE_CHANGE                   300     //PIN Code Change, this is available only if Pin Code is not yet written in NVRAM
                                                                        //(eg, fresh program, usually factory production build). The last 16 byte of data
                                                                        //contain PIN Code in alpha numberic ASCII character with first byte as the Lenth of character


//System Utils DK Security info defines
#define SECURITY_LEVEL_NONE                 1
#define SECURITY_LEVEL_PIN_CODE_HASH        2
#define SECURITY_PAYLOAD_NONE                                   0
#define SECURITY_PAYLOAD_TYPE_CHALLENGE                         1
#define SECURITY_PAYLOAD_TYPE_NEW_PIN_CODE_HASH_KEY_VERIFY      2
#define SECURITY_CLEARANCE_SUCCESS                              1
#define SECURITY_CLEARANCE_REQUIRED                             2

/** @defgroup DK_COMPANY_IDS
 * @{
 */
#define DK_COMPANY_DEXATEK                      0xD0D1
/** @} End DK_COMPANY_IDS */


/** @defgroup DK_PRODUCT_TYPE_IDS
 * WARNING: Make sure to also add the handling in BLEDevParser_GetDkAdvData from BLEDeviceInfoParser
 * 			whenever new device type is added here
 *
 * @{
 */

#define DK_PRODUCT_TYPE_NONE_ID                     0x00
#define DK_PRODUCT_TYPE_PHOTOKEY_ID                 0x01
#define DK_PRODUCT_TYPE_WEATHER_ID                  0x02
#define DK_PRODUCT_TYPE_IBEACON_ID                  0x03
#define DK_PRODUCT_TYPE_HOME_DOOR_ID                0x04
#define DK_PRODUCT_TYPE_POWERPLUG_ID                0x05
#define DK_PRODUCT_TYPE_LIGHTBULB_ID                0x06
#define DK_PRODUCT_TYPE_SMOKE_DETECTOR              0x07
#define DK_PRODUCT_TYPE_SHOCK_SENSE_DETECTOR        0x08
#define DK_PRODUCT_TYPE_INLET_SWITCH_ID             0x09
#define DK_PRODUCT_TYPE_MOTION_SENSOR_ID            0x0A
#define DK_PRODUCT_TYPE_THERMOSTAT_CTRL_ID          0x0B
#define DK_PRODUCT_TYPE_POWER_SOCKET_ID             0x0C
#define DK_PRODUCT_TYPE_DOOR_LOCK_ID                0x0D
#define DK_PRODUCT_TYPE_SIREN_ID                    0x0E
#define DK_PRODUCT_TYPE_ROMOTE_CONTROL_ALARM_ID     0x0F
#define DK_PRODUCT_TYPE_LED_ADAPTOR_ID              0x10
#define DK_PRODUCT_TYPE_RGB_LIGHT_ID                0x11
#define DK_PRODUCT_TYPE_RGB_LIGHT_STRIP_ID          0x12
#define DK_PRODUCT_TYPE_GATEWAY_PERIPHERAL_ID       0x20
#define DK_PRODUCT_TYPE_GATEWAY_BROADCASTER_ID      0x21
#define DK_PRODUCT_TYPE_BLE_REPEATER_ID             0x30
#define DK_PRODUCT_TYPE_PM25_COMBO_ID               0x8C
#define DK_PRODUCT_TYPE_IR_REMOTE_ID                0x8D
#define DK_PRODUCT_TYPE_ALARM_SYSTEM_ID             0x8E
#define DK_PRODUCT_TYPE_IPCAM_SYSTEM_ID             0x8F
#define DK_PRODUCT_TYPE_TAISEIA_AIR_CON_ID          0x90
#define DK_PRODUCT_TYPE_FARGLORY_PM25_COMBO_ID      0x91 //This is custom device (not compatible with SigmaCasa family)
#define DK_PRODUCT_TYPE_TAISEIA_DEHUMIDIFIER_ID     0x92
#define DK_PRODUCT_TYPE_ALARM_LITE_ID               0x93
#define DK_PRODUCT_TYPE_TAISEIA_HEAT_EXCHANGER_ID   0x94
#define DK_PRODUCT_TYPE_SMART_DESK_ID               0x95
#define DK_PRODUCT_TYPE_DOORCAM_SYSTEM_ID       0x96

// PRODUCT_TYPE for HomeKit enable accessory
// HomeKit Category: Switches
#define DK_PRODUCT_TYPE_HOMEKIT_SWITCH_ID           0xC0    // ON/OFF control only
#define DK_PRODUCT_TYPE_HOMEKIT_PLUGIN_ID           0xC1    // ON/OFF control only

// HomeKit Category: Lighting
#define DK_PRODUCT_TYPE_HOMEKIT_SWITCH_DIMMER_ID    0xC2    // ON/OFF and brightness control
#define DK_PRODUCT_TYPE_HOMEKIT_PLUGIN_DIMMER_ID    0xC3    // ON/OFF and brightness control

/** @} End DK_PRODUCT_TYPE_IDS */


/** @defgroup DK_MASTER_DATA_TYPE_IDS
 * @{
 */

#define DK_MASTER_DATA_TYPE_GENERIC_ID              0x0010
#define DK_MASTER_DATA_TYPE_AIRCON_IR_REMOTE_ID     0x1000
#define DK_MASTER_DATA_TYPE_GENERAL_IR_REMOTE_ID    0x1001
#define DK_MASTER_DATA_TYPE_ALARM_CENTRAL_ID        0x1010
#define DK_MASTER_DATA_TYPE_IPCAM_ID                0x1020
#define DK_MASTER_DATA_TYPE_TAISEIA_ID              0x1030
#define DK_MASTER_DATA_TYPE_ERGO_DESK_ID            0x1040


/** @} End DK_MASTER_DATA_TYPE_IDS */


/** @defgroup DK_DATA_TYPE_IDS
 * WARNING: Make sure to also add the len in DKPeripheral_GetDataTypeLenForDataTypeID in this file
 * 			whenever new data type is added here
 * @{
 */
#define DK_DATA_TYPE_TEMPERATURE				0x01
#define DK_DATA_TYPE_ATMOSPHERIC_PRESSURE		0x02
#define DK_DATA_TYPE_RELATIVE_HUMILITY			0x03
#define DK_DATA_TYPE_USERPASSCODE				0x04
#define DK_DATA_TYPE_FIRMWARE_VERSION			0x05
#define DK_DATA_TYPE_SHORT_RELATIVE_HUMILITY	0x06
#define DK_DATA_TYPE_AIR_QUALITY_VOC			0x07
#define DK_DATA_TYPE_REGISTERED_ID				0x08
#define DK_DATA_TYPE_DOOR_STATUS				0x09
#define DK_DATA_TYPE_POWER_PLUG_STATE			0x0A
#define DK_DATA_TYPE_VOLTAGE_RMS				0x0B
#define DK_DATA_TYPE_POWER_IN_WATT				0x0C
#define DK_DATA_TYPE_AC_SOURCE_FREQUENCY		0x0D
#define DK_DATA_TYPE_LIGHT_BULB_DIMMER          0x0E
#define DK_DATA_TYPE_LIGHT_BULB_COLOUR          0x0F
#define DK_DATA_TYPE_SECURITY_MASK              0x10
#define DK_DATA_TYPE_SMOKE_DETECTOR_STATUS      0x11
#define DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_STATUS      0x12
#define DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_ZFORCE      0x13 //OBSOLETE
#define DK_DATA_TYPE_SHOCK_SENSE_ZFORCE_THRESHOLD     0x14
#define DK_DATA_TYPE_GENERIC_SWITCH_STATUS            0x15
#define DK_DATA_TYPE_INLET_SWITCH_STATUS              DK_DATA_TYPE_GENERIC_SWITCH_STATUS  //Backward compatibility
#define DK_DATA_TYPE_MOTION_SENSE_STATUS              0x16
#define DK_DATA_TYPE_MOTION_SENSE_SENSITIVITY         0x17
#define DK_DATA_TYPE_MOTION_SENSE_TRIGGER_DELAY_CTRL  0x18
#define DK_DATA_TYPE_THERMOSTAT_CB_STATUS             0x19
#define DK_DATA_TYPE_THERMOSTAT_CB_CURRENT_AMB_TEMP   0x1A
#define DK_DATA_TYPE_THERMOSTAT_CB_SETPOINT_TEMP      0x1B
#define DK_DATA_TYPE_THERMOSTAT_CB_HEATING_TEMP       0x1C
#define DK_DATA_TYPE_THERMOSTAT_CB_ENERGY_SAV_TEMP    0x1D
#define DK_DATA_TYPE_THERMOSTAT_CB_DATA_SYNCH_ID      0x1E
#define DK_DATA_TYPE_PM25_VALUE                       0x1F
#define DK_DATA_TYPE_NOISE_DB                         0x20
#define DK_DATA_TYPE_LIGHT_LUX                        0x21
#define DK_DATA_TYPE_POWER_SOCKET_A_IN_USE            0x22
#define DK_DATA_TYPE_POWER_SOCKET_B_IN_USE            0x23
#define DK_DATA_TYPE_POWER_SOCKET_A_STATUS            0x24
#define DK_DATA_TYPE_POWER_SOCKET_B_STATUS            0x25
#define DK_DATA_TYPE_POWER_SOCKET_SAFETY_MODE         0x26
#define DK_DATA_TYPE_CO2_CONCENTRATION                0x27
#define DK_DATA_TYPE_DOOR_LOCK_STATUS                 0x28
#define DK_DATA_TYPE_SIREN_STATUS                     0x29
#define DK_DATA_TYPE_REMOTE_CONTROL_STATUS            0x2A
#define DK_DATA_TYPE_RTC_STATUS                       0x2B
#define DK_DATA_TYPE_RTC_TIME_UTC0                    0x2C
#define DK_DATA_TYPE_RECORDING_EVENT                  0x2D
#define DK_DATA_TYPE_ALARM_SYSTEM_STATE               0x2E
#define DK_DATA_TYPE_POWER_ADAPTER_STATUS             0x2F
#define DK_DATA_TYPE_IMPLICIT_DEV                     0x30
#define DK_DATA_TYPE_REPEATER_NUM_CONDITIONS_HOLD     0x31
#define DK_DATA_TYPE_REPEATER_NUM_TOTAL_CONDITIONS    0x32
#define DK_DATA_TYPE_FILTER_CLEANING_TIME_ACCUMULATION  0x33
#define DK_DATA_TYPE_CURRENT_STATUS_INFO_CRC16          0x34
#define DK_DATA_TYPE_INTANK_LEVEL                       0x35
#define DK_DATA_TYPE_PAIRING_KEY_STATUS                 0x36
#define DK_DATA_TYPE_DOS_ATTACK_STATUS                  0x37
#define DK_DATA_TYPE_FILTER_CLEANING_NOTIFICATION       0x38
#define DK_DATA_TYPE_DEVICE_INFO                        0x39
#define DK_DATA_TYPE_LIGHT_HUE_VALUE                    0x3A
#define DK_DATA_TYPE_LIGHT_SATURATION_VALUE             0x3B

#define DK_DATA_TYPE_GENERIC_ALERT_COUNT                0x0001

// Alarm system specific detail containers
#define DK_DATA_TYPE_ARMING_MODE                        0x40
//#define DK_DATA_TYPE_ZONES_FOR_MODE_DISARM            0x41
//#define DK_DATA_TYPE_ZONES_FOR_MODE_HOME              0x42
#define DK_DATA_TYPE_CONDITIONS_SABOTAGE                0x43
#define DK_DATA_TYPE_CONDITIONS_NORMAL                  0x44
#define DK_DATA_TYPE_CONDITIONS_HOME                    0x45
#define DK_DATA_TYPE_CONDITIONS_24HR                    0x46
#define DK_DATA_TYPE_JOB_ALARM_EXECUTION                0x47
#define DK_DATA_TYPE_PIN_CODE                           0x48
#define DK_DATA_TYPE_ARMING_DELAY                       0x49
//#define DK_DATA_TYPE_EMERGENCY_PHONE_NUMBERS            0x4C  //Not longer available, can be re-used
#define DK_DATA_TYPE_EMERGENCY_PHONE_NUMBERS_WITH_NAMES 0x4D
#define DK_DATA_TYPE_POWER_STATE                        0x4E
#define DK_DATA_TYPE_PIN_CODE_SILENTALERT               0x4F
#define DK_DATA_TYPE_PIN_CODE_ADMIN                     0x50
#define DK_DATA_TYPE_GENERIC_CONFIG                     0x51
#define DK_DATA_TYPE_ERGO_DESK_STATUS                   0x52
#define DK_DATA_TYPE_ERGO_DESK_AGGREGATED_TIME          0x53
#define DK_DATA_TYPE_ERGO_DESK_UPDATE_TIME              0x54
#define DK_DATA_TYPE_VOC_CONCENTRATION                  0x55
#define DK_DATA_TYPE_NON_UNIQUE_START_INDEX             0x0FFF  //This is just an index of starting non unique data type

#define DK_DATA_TYPE_REMOTE_IR_LEARNING_GROUP         0x1000
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_0      0x1001
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_1      0x1002
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_2      0x1003
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_3      0x1004
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_4      0x1005
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_5      0x1006
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_6      0x1007
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_7      0x1008
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_8      0x1009
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_9      0x100A
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_VOL_UP       0x100B
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_VOL_DOWN     0x100C
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_CH_UP        0x100D
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_CH_DOWN      0x100E
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_POWER        0x100F
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_RETURN       0x1010
#define DK_DATA_TYPE_REMOTE_IR_LEARNING_KEYPAD_ENTER        0x1011

#define DK_DATA_TYPE_IPCAMERA_STREAMING_CONFIG              0x0001
#define DK_DATA_TYPE_IPCAMERA_MOTION_DETECTION_CONFIG       0x0002
#define DK_DATA_TYPE_IPCAMERA_VIDEO_CONFIG       			0x0003

#define DK_DATA_TYPE_TAISEIA_REGISTRATION_OF_COMPLETE_INFO  0x0001
#define DK_DATA_TYPE_TAISEIA_ALL_CURRENT_SERVICE_STATUS     0x0002

#define DK_DATA_TYPE_ERGO_DESK_HEIGHT                       0x0001
#define DK_DATA_TYPE_ERGO_DESK_REMINDER                     0x0002

/** @} End DK_DATA_TYPE_IDS */

// Solution ID

#define DK_SOLUTION_ID_TI                                   0x00
#define DK_SOLUTION_ID_NORDIC                               0x01

//Other
#define LOW_BATTERY_LESS_THAN_WARNING_VALUE             20
#define CHARACTERISTIC_HANDLER_READ_WRITE_OFFSET        1
#define CHARACTERISTIC_HANDLER_NOTIFICATION_OFFSET      2
#define UUID_128_SIZE                                   16  // Size of 128-bit UUID
#define UUID_16_SIZE                                    2   // Size of 16-bit Bluetooth UUID

#define SECURITY_KEY_CODE_MAX_SIZE          16
#define SECURITY_MASK_IDENTIFIER_8BIT       0xC1

#define AGGREGATED_HEADER_ID                0xC2
#define MULTI_TRANSAC_HEADER_ID             0xC2

#define AGGREGATED_DATA_HEADER_FORMAT_AGD0      0
#define AGGREGATED_DATA_HEADER_FORMAT_AGD1      1
#define AGGREGATED_DATA_HEADER_FORMAT_TIME0     0
#define AGGREGATED_DATA_HEADER_FORMAT_TIME1     1

#define AGGREGATED_DATA_POWER_IN_JOULES_DATA_ID             1
#define AGGREGATED_DATA_TEMPERATURE_CELSIUS_DATA_ID         10
#define AGGREGATED_DATA_HUMIDITY_PERCENT_DATA_ID            11
#define AGGREGATED_DATA_ATM_PSI_DATA_ID                     12
#define AGGREGATED_DATA_MOTION_FREQUENCY_DATA_ID            20
#define AGGREGATED_DATA_MOTION_ACTIVITY_DURATION_DATA_ID    21
#define AGGREGATED_DATA_DESK_STATUS_OUT_DATA_ID             30
#define AGGREGATED_DATA_DESK_STATUS_SIT_DATA_ID             31
#define AGGREGATED_DATA_DESK_STATUS_STAND_DATA_ID           32


#define MULTI_TRANSAC_DATA_LEN              16

#define B_ADDR_LEN_8                        8

#define MAXIMUM_SECURITY_KEY_SIZE_ALLOWED   16

#define AES_ENC_DEC_BIT                     128
#define AES_ENC_DEC_BYTE                    (AES_ENC_DEC_BIT/8)

#define DATA_TRANSFER_PREAMBLE              0xDADE
#define DATA_TRANSFER_SIZE_PER_CHUNK        20      //WARNING: This value must match with Gateway Peripheral bridge code

#define PERIPHERAL_DATA_TYPE_PLAYLOAD_SIZE  64

#define MAX_RTC_OFFSET_IN_SEC               90 //30 is preferred but 90 should be good enough
#define RTC_UPDATE_THRESHOLD                (MAX_RTC_OFFSET_IN_SEC/2)



/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/
typedef enum
{
    DEVICE_BRAND_UNKNOWN = -1,
    DEVICE_BRAND_GENERIC,
    DEVICE_BRAND_HITACHI
} eDeviceBrand;

typedef enum
{
    DEVICE_INFO_RESERVED = 0,
    DEVICE_INFO_HITACHI_AIRCON_COMMERCIAL = 1,
    DEVICE_INFO_HITACHI_AIRCON_NON_COMMERCIAL,
    DEVICE_INFO_HITACHI_AIRCON_UNKNOWN,

    DEVICE_INFO_HITACHI_DEHUMIDIFIER = 10,
    DEVICE_INFO_HITACHI_DEHUMIDIFIER_UNKNOWN = 19,

    DEVICE_INFO_HITACHI_HEAT_EXCHANGER = 20,
    DEVICE_INFO_HITACHI_HEAT_EXCHANGER_UNKNOWN = 29,

    DEVICE_INFO_GAP_HITACHI = 100,
    /* put model definitions of other brands after the gap */

    DEVICE_INFO_GENERIC_AIRCON = 65001,
    DEVICE_INFO_GENERIC_DEHUMIDIFIER = 65002,
    DEVICE_INFO_GENERIC_HEAT_EXCHANGER = 65003,

    //limited to uint16_t (2-byte long)
    DEVICE_INFO_MAX = 65535,
    DEVICE_INFO_UNDEFINED = DEVICE_INFO_MAX
} eDeviceInfo;

typedef enum
{
    DATA_TRANSFER_MSG_TYPE_UNKNOWN = 0,
    /* Event direction from Mobile Device to Gateway */
    DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_REPORT_REQ,			//No Payload, Response with DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_XXXXXX
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE,				//Payload - WifiConfigSimple_t, Response with DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_XXXXX
    DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_TURN_DOWN_REQ,	//No Payload, Response with DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_XXXXX
    DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REQ,             //No Payload, Response with DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_XXXXX
    DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG,				//Payload, - GatewayInfoConfig_t, Response with DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG_XXXXXXXX
    DATA_TRANSFER_MSG_TYPE_USER_INFO_CONFIG,				//Payload, - UserInfoConfig_t, Response with DATA_TRANSFER_MSG_TYPE_USER_INFO_CONFIG_XXXXXXXX
    DATA_TRANSFER_MSG_TYPE_GATEWAY_EXIT_SETUP_MODE,			//No Payload

    /* Event direction from Gateway to Mobile Device
     * WARNING: Adding new enum in here will also require to add the translation in iOS GP_DataTransferMessageTypeToEventMsgConvert and Android project
     */
    DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_NOT_POSSIBLE,		//No Payload, make sure wifi connection is turned off (disconnected) before requesting for Ap scan
    DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_REPORT,				//Payload - ScanApInfo_t
    DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_REPORT_DONE,		//No Payload

    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SUCCESS,				//No Payload - Wifi Connection is up when this event is received
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_INVALID_PARAM,		//No Payload
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SSID_NOT_FOUND,		//No Payload
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_FAILED,				//No Payload - Generic fail, wrong password, wrong encryption type, others...
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_NOT_POSSIBLE_WHILE_CONNECTION_IS_UP,		//No Payload

    DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN,			//No Payload
    DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN_NOT_POSSIBLE_AT_THIS_MOMENT,	//No Payload

    DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REPORT,          //Payload - WifiCommDeviceMac_t

    DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG_SUCCESS,		//Payload - GatewayInfoConfig_t, this is the returned value of previously sent info (should cross-check in case of transmission eror)
    DATA_TRANSFER_MSG_TYPE_USER_INFO_CONFIG_SUCCESS,		//Payload - UserInfoConfig_t, this is the returned value of previously sent info (should cross-check in case of transmission eror)
    DATA_TRANSFER_MSG_TYPE_USER_INFO_CONFIG_FAILED_INVALID_PARAM,				//No Payload

    /* Event direction that is common to both Mobile device and gateway
     * WARNING: Adding new enum in here will also require to add the translation in iOS GP_DataTransferMessageTypeToEventMsgConvert and Android project
     */
    DATA_TRANSFER_MSG_TYPE_COMMON,
    DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE_EXT,          //Payload - WifiConfigSimpleExt_t, Response with DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_XXXXX


    /* Extension Message Type (Starting at enum 100)
     *
     */
    DATA_TRANSFER_MSG_TYPE_EXTENSION_START				= 	100,
    DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_REQ		= 	110,		//No Payload, Response with DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI
    DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_NOT_POSSIBLE,			//No Payload, make sure wifi connection is up (connected) before requesting for RSSI
    DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI,							//Payload - int32_t
    DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_REQ		=	120,		//PRODUCTION USE ONLY - No Payload
    DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_SUCCESS,					//No Payload
    DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_DENIED,					//No Payload
    DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REQ   =   130,        //Payload - WifiCommDeviceMac_t
    DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REPORT,               //Payload - WifiCommDeviceMac_t

    /**
     * For custom device (not compatible with SigmaCasa family)
     */
    DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_NETWORK_REQ   = 140,          //Payload - dxIPConfig_t
    DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_NETWORK_SUCCESS,              //No Payload
    DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_NETWORK_DENIED,               //No Payload

    /**
     * For custom device (not compatible with SigmaCasa family)
     */
    DATA_TRANSFER_MSG_TYPE_CONCURRENT_PERIPHERAL        = 150,          //Payload - ConcurrentDataTransferHeader_t

    /**
     * Production Test Related Messages
     */
     DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_REQ = 240,    //Payload - HpProductionSelfTestReportReq_t but payload content is device specific
     DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_SUCCESS,  //Payload - HpProductionSelfTestReportRes_t but payload content is device specific
     DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_FAILURE,  //Payload - HpProductionSelfTestReportRes_t but payload content is device specific

} eDataTransferMessageType;

/* WARNING: This enum MUST be the same as in BLE Central FW Project source code */
typedef enum
{

  ACTIVE_IMAGE_TYPE_SBL_TI = 0,
  ACTIVE_IMAGE_TYPE_MAIN_APPLICATION_TI,
  ACTIVE_IMAGE_TYPE_SBL_NORDIC_NRF52832,
  ACTIVE_IMAGE_TYPE_MAIN_APPLICATION_NORDIC_NRF52832

} eBleSystemInfoImageType;

typedef enum
{
  POWER_CONFIG_NONE = 0,
  POWER_CONFIG_RESET_SCHEDULING,    //Reset Scheduling and allocate necessary memory for upcoming scheduling
  POWER_CONFIG_CURRENT_TIME,
  POWER_CONFIG_SCHEDULE_START,      //Contain current task index and the time in minutes for next task from current time.
  POWER_CONFIG_SCHEDULE_TASK_ADD,
  POWER_CONFIG_SWITCH_ON,
  POWER_CONFIG_SWITCH_OFF,

} eConfiguration;

typedef enum
{
    POWER_SOCKET_CONFIG_NONE = 0,
    POWER_SOCKET_CONFIG_RESET_SCHEDULING,    //Reset Scheduling and allocate necessary memory for upcoming scheduling
    POWER_SOCKET_CONFIG_CURRENT_TIME,
    POWER_SOCKET_CONFIG_SCHEDULE_START,      //Contain current task index and the time in minutes for next task from current time.
    POWER_SOCKET_CONFIG_SCHEDULE_TASK_ADD,
    POWER_SOCKET_CONFIG_SWITCH_ON_SOCKET_A,
    POWER_SOCKET_CONFIG_SWITCH_OFF_SOCKET_A,
    POWER_SOCKET_CONFIG_SWITCH_ON_SOCKET_B,
    POWER_SOCKET_CONFIG_SWITCH_OFF_SOCKET_B,

    POWER_SOCKET_CONFIG_TURN_ON_SAFETY_MODE = 20,
    POWER_SOCKET_CONFIG_TURN_OFF_SAFETY_MODE = 21,

} ePowerSocketConfig;

typedef enum
{
    INLET_SWITCH_CONTROL_UNKNOWN = 0,
    INLET_SWITCH_CONTROL_ON,
    INLET_SWITCH_CONTROL_OFF,

} eInletSwitchControl;

typedef enum
{
    GENERIC_SWITCH_CONTROL_UNKNOWN = 0,
    GENERIC_SWITCH_CONTROL_ON,
    GENERIC_SWITCH_CONTROL_OFF,

} eGenericSwitchControl;

typedef enum
{
  SIGNAL_STRENGTH_UNKNOWN = -1, //For internal initialization use only, application should never expect to see such value
  SIGNAL_STRENGTH_NONE = 0,
  SIGNAL_STRENGTH_VERY_WEAK,
  SIGNAL_STRENGTH_WEAK,
  SIGNAL_STRENGTH_NORMAL,
  SIGNAL_STRENGTH_STRONG,
  SIGNAL_STRENGTH_VERY_STRONG,

} ePeripheralSignalStrength;


/**
 * Enumeration of Wi-Fi security modes
 * WARNING: DO NOT INSERT definitions in the middle, new additional definition must be added at the end
 *          for backward compatibility.
 */
typedef enum
{
    AP_SECURITY_OPEN           	= 0,          	/**< Open security                           */
    AP_SECURITY_WEP_PSK,                        /**< WEP Security with open authentication   */
    AP_SECURITY_WEP_SHARED,       	          	/**< WEP Security with shared authentication */
    AP_SECURITY_WPA_TKIP_PSK,                 	/**< WPA Security with TKIP                  */
    AP_SECURITY_WPA_AES_PSK,                  	/**< WPA Security with AES                   */
    AP_SECURITY_WPA2_AES_PSK,                  	/**< WPA2 Security with AES                  */
    AP_SECURITY_WPA2_TKIP_PSK,                 	/**< WPA2 Security with TKIP                 */
    AP_SECURITY_WPA2_MIXED_PSK,                   /**< WPA2 Security with AES & TKIP          */

    AP_SECURITY_WPS_OPEN,                       /**< WPS with open security                  */
    AP_SECURITY_WPS_SECURE,                     /**< WPS with AES security                   */

    AP_SECURITY_UNKNOWN,                        /**< May be returned by scan function if security is unknown. Do not pass this to the join function! */

    AP_SECURITY_FORCE_32_BIT,                   /**Use this define to indicate non hidden SSID, which will result in scan-connect method.*/

    AP_SECURITY_WPA_WPA2_MIXED,                 /**< WPA & WPA2 Mixed Security          */

} Ap_security_t;


/**
 * Enumeration of 802.11 radio bands
 * WARNING: The enum MUST be the same as wiced_802_11_band_t in wwd_constants.h
 */
typedef enum
{
    AP_802_11_BAND_5GHZ   = 0, /**< Denotes 5GHz radio band   */
    AP_802_11_BAND_2_4GHZ = 1  /**< Denotes 2.4GHz radio band */

} Ap_802_11_band_t;

/******************************************************
 *                      Macros
 ******************************************************/

#define DK_CHARACTERISTIC_PROPERTY_READ                     (0x01 << 0)
#define DK_CHARACTERISTIC_PROPERTY_WRITE                    (0x01 << 1)
#define DK_CHARACTERISTIC_PROPERTY_NOTIFY                   (0x01 << 2)
#define DK_CHARACTERISTIC_PROPERTY_INDICATE                 (0x01 << 3)

#define DK_CHARACTERISTIC_SYSTEM_UTILS_ASK_SERVER_TO_DISCONNECT_MASK            (0x0001 << 0)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_ADVERTISE_OFF_TIME_MASK                  (0x0001 << 1)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_CHANGE_DEV_NAME_MASK                     (0x0001 << 2)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_IDENTIFICATION_MASK                  (0x0001 << 3)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_DEV_INFO_MASK                            (0x0001 << 6)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_ADVERTISING_HEADER_CONFIG_MASK           (0x0001 << 7)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_ADVERTISING_DATATYPE_PAYLOAD_ACCESS_MASK (0x0001 << 8)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_SECURITY_ACCESS_MASK                     (0x0001 << 9)
#define DK_CHARACTERISTIC_SYSTEM_UTILS_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_MASK (0x0001 << 10)

#define DK_FLAG_IMAGE_TYPE_OAD                              (0x01 << 0)
#define DK_FLAG_PAYLOAD_ENCRYPTED                           (0x01 << 1)
#define DK_FLAG_CONTAIN_PAGING_INFO                         (0x01 << 2)
#define DK_FLAG_CONTAIN_BATTERY_INFO                        (0x01 << 3)
#define DK_FLAG_UPDATE_PAYLOAD_TO_PERIPHERAL_STATUS_ONLY    (0x01 << 7)
#define DK_FLAG_FACTORY_PRODUCTION_BUILD                    (0x01 << 14)
#define DK_FLAG_CONTAIN_EXT_FLAG                            (0x01 << 15)


/** @defgroup DK_ADV_FLAG_MACROS Dk Flag defines
 * @{
 */
#define DK_FLAG_IS_IMAGE_TYPE_OAD(flag)                             (flag & (1 << 0))
#define DK_FLAG_IS_PAYLOAD_ENCRYPTED(flag)                          (flag & (1 << 1))
#define DK_FLAG_IS_CONTAIN_PAGING_INFO(flag)                        (flag & (1 << 2))
#define DK_FLAG_IS_CONTAIN_BATTERY_INFO(flag)                       (flag & (1 << 3))
#define DK_FLAG_IS_UPDATE_PAYLOAD_TO_PERIPHERAL_STATUS_ONLY(flag)   (flag & (1 << 7))
#define DK_FLAG_IS_DEVICE_PAIRED(flag)                              (flag & (1 << 11))
#define DK_FLAG_IS_DEVICE_IN_GATEWAY_OPEN_PAIRING_MODE(flag)        (flag & (2 << 11))
#define DK_FLAG_IS_DEVICE_IN_STAND_ALONE_MODE(flag)                 !(flag & (3 << 11))
#define DK_FLAG_IS_FACTORY_PRODUCTION_BUILD(flag)                   (flag & (1 << 14))
#define DK_FLAG_IS_CONTAIN_EXT_FLAG(flag)                           (flag & (1 << 15))
#define DK_FLAG_SOLUTION_ID(flag)                                   ((flag >> 4) & 7)


#define CONVERT_SIGNAL_STREGTH(InRSSI,OutPSignalStr)	if ((InRSSI > -50) && (InRSSI < 0)) \
															OutPSignalStr = SIGNAL_STRENGTH_VERY_STRONG; \
														else if ((InRSSI > -65) && (InRSSI < 0)) \
															OutPSignalStr = SIGNAL_STRENGTH_STRONG; \
														else if ((InRSSI > -75) && (InRSSI < 0)) \
															OutPSignalStr = SIGNAL_STRENGTH_NORMAL; \
														else if ((InRSSI > -90) && (InRSSI < 0)) \
															OutPSignalStr = SIGNAL_STRENGTH_WEAK; \
														else if (InRSSI < 0) \
															OutPSignalStr = SIGNAL_STRENGTH_VERY_WEAK; \
														else \
															OutPSignalStr = SIGNAL_STRENGTH_NONE; \

#define	DKABS(a)            (((a) < 0) ? -(a) : (a))

#define CONVERT_SIGNAL_STREGTH_ABS(InRSSI,OutPSignalStr){ \
                                                            uint8_t RSSIAbs = DKABS(InRSSI); \
                                                            if ((RSSIAbs < 50) && (RSSIAbs > 0)) \
                                                                OutPSignalStr = SIGNAL_STRENGTH_VERY_STRONG; \
                                                            else if ((RSSIAbs < 65) && (RSSIAbs > 0)) \
                                                                OutPSignalStr = SIGNAL_STRENGTH_STRONG; \
                                                            else if ((RSSIAbs < 75) && (RSSIAbs > 0)) \
                                                                OutPSignalStr = SIGNAL_STRENGTH_NORMAL; \
                                                            else if ((RSSIAbs < 90) && (RSSIAbs > 0)) \
                                                                OutPSignalStr = SIGNAL_STRENGTH_WEAK; \
                                                            else if (RSSIAbs > 0) \
                                                                OutPSignalStr = SIGNAL_STRENGTH_VERY_WEAK; \
                                                            else \
                                                                OutPSignalStr = SIGNAL_STRENGTH_NONE; \
                                                        } \


/******************************************************
 *                 Type Definitions
 ******************************************************/

struct PERIPHERAL_STATUS_FLAG_BIT_INFO {
	//REMARK: Peripheral security is not enabled, it can happen if it's been reset before. Need to send set security command providing PinCode to set the security
    uint16_t Status_NoSecurity_b1     		:1;     // bit 0 single bit

    //REMARK: This also means security key for the decryption is wrong causing invalid data!.. In order to fix this issue it is require to reset the peripheral and set the security again with provided PinCode
    uint16_t Status_UnknownData_b2   		:1;     // bit 1 single bit

    uint16_t Status_FwUpdating_b3   		:1;     // bit 2 single bit

    uint16_t Status_Reserve_b4_15   		:13;    // bit 4 through 15

};

union PeripheralStatusFlag_u {
    uint16_t  								flag;
    struct PERIPHERAL_STATUS_FLAG_BIT_INFO 	bits;
};

struct MULTI_TRANSAC_PAGE_BIT_INFO {

    uint8_t 	TotalPage_b0_3     		:4;     // bit 0 through 3 (4 bit total)
    uint8_t 	CurrentPage_b4_7   		:4;     // bit 4 through 7 (4 bit total)

};

union MultiTransacDataPageInfo_u {
    uint8_t  								value;
    struct MULTI_TRANSAC_PAGE_BIT_INFO    bits;
};

struct AGGREGATED_PAGE_BIT_INFO {

    uint8_t 	TotalPage_b0_3     		:4;     // bit 0 through 3 (4 bit total)
    uint8_t 	CurrentPage_b4_7   		:4;     // bit 4 through 7 (4 bit total)

};

union AggregatedHeaderPageInfo_u {
    uint8_t  								value;
    struct AGGREGATED_PAGE_BIT_INFO 		bits;
};

struct AGGREGATED_FORMAT_BIT_INFO {

    uint8_t 	AfDFormat_b0_3     		:4;     // bit 0 through 3 (4 bit total)
    uint8_t 	Reserve_b4_6	   		:3;     // bit 4 through 6 (3 bit total)
    uint8_t 	TimeFormat_b7	   		:1;     // bit 7 (1 bit total)

};

union AggregatedHeaderFormatInfo_u {
    uint8_t  								value;
    struct AGGREGATED_FORMAT_BIT_INFO 		bits;
};


typedef struct {
	uint16_t  Preamble;
	uint8_t   MessageType;
	uint8_t   PayloadLen;
} DataTransferMsgHeader_t;


typedef struct {
	uint16_t  Preamble;
	uint8_t   MessageType;
	uint8_t   Reserved;
	uint16_t  PayloadLength;
} ConcurrentDataTransferHeader_t;


typedef struct {
	uint16_t  ServiceUuid;
	uint16_t  CharacteristicUuid;
	uint8_t   AccessType;   // Bit0: read/notify/indicate eanble, Bit1: write enable
	uint8_t   Reserved;
	uint16_t  DataLen;
} ConcurrentCharacteristicTransferHeader_t;


typedef struct {
    char                SSID[32];
    char                Password[32];
    int32_t             SecyrityType; /** < See Ap_security_t, Set to AP_SECURITY_FORCE_32_BIT for auto lookup BUT must specify if SSID is "hidden" */
} WifiConfigSimple_t;

typedef struct {
    char                SSID[32];       // Section 7.3.2.1 of the 802.11-2007 specification
    char                Password[128];  // Section 7.3.2.1 of the 802.11-2007 specification
    int32_t             SecyrityType;   /** < See Ap_security_t, Set to AP_SECURITY_FORCE_32_BIT for auto lookup BUT must specify if SSID is "hidden" */
} WifiConfigSimpleExt_t;

typedef struct {

    char   				GatewayDeviceName[64];	//[Optional] If NULL string terminated, it won't be updated
    char   				GatewayAccessKey[64];	//[Optional] If NULL string terminated, it won't be updated
    double				latitude;				//[Optional] If 0, it won't be updated
    double				longitude;				//[Optional] If 0, it won't be updated

} GatewayInfoConfig_t;


typedef struct {
    char   				Username[64];
    char   				Password[32];
} UserInfoConfig_t;


typedef struct {
    char   					SSID[32];			/**< Service Set Identification (i.e. Name of Access Point)                    */
    int16_t					signal_strength; 	/**< Receive Signal Strength Indication in dBm. <-90=Very poor, >-30=Excellent */
    int32_t					security;			/**< Security type, see Ap_security_t                                          */
    uint32_t		     	band;             	/**< Radio band, see Ap_802_11_band_t                                          */
} ScanApInfo_t;


typedef struct
{
    uint8_t     Addr[B_ADDR_LEN_8];

} DevAddress_t;


typedef struct
{
    uint16_t    Size;
    uint8_t     value[MAXIMUM_SECURITY_KEY_SIZE_ALLOWED];

} SecurityKeyCodeContainer_t;


typedef struct
{
	uint16_t 	nDataID;
	uint16_t 	nDuration;
	uint32_t 	nDataValue;

} AggregateItem_t;

typedef struct
{
	uint32_t 	nTime;
	uint8_t 	nDataID;
	uint8_t 	nValueType;
	uint16_t 	nDuration;
	uint32_t 	nDataValue;

} AggregateItemAgD1_t;

typedef struct
{
	uint32_t 	nTime;
	uint8_t 	nDataID;
	uint16_t 	nDataValue;

} AggregateItemAgD2_t;

typedef struct
{
	uint16_t 	CompanyID;
	uint8_t 	ProductID;
	uint8_t 	XorPayloadChecksum;
	uint16_t  	Flag;

} DeviceDkPeripheralAdvHeader_t;


typedef struct
{
	DeviceDkPeripheralAdvHeader_t 	Header;
	uint8_t 	ExtDkFlag;
	uint8_t 	BatteryInfo;
	uint16_t  	PagingInfo;
	uint16_t	AdDataLen;			/**<  If 0 should ignore AdData*/
	uint8_t		AdData[PERIPHERAL_DATA_TYPE_PLAYLOAD_SIZE];

} DkAdvData_t;


typedef struct
{
	DevAddress_t 					DevAddress;
	int8_t							DevHandlerID;
	int8_t  						rssi;
	union PeripheralStatusFlag_u	Status;
	uint32_t						LastUpdatedTimeMs;		/**<  Time of Data that was last updated (System time)*/
	DkAdvData_t     				DkAdvertisementData;

} DeviceInfoReport_t; ////MUST be 4 byte (32-bit) alignment

typedef struct
{
    uint16_t                        DeviceCount;
    uint8_t                         GenericInfo[0];
} DeviceKeeperListReport_t;

typedef struct
{
    DevAddress_t        DevAddress;
    DkAdvData_t         DkAdvertisementData;

} DeviceInfoJobCheck_t; ////MUST be 4 byte (32-bit) alignment

typedef struct
{

    uint8_t           SecurityLevel;
    uint8_t           SecurityPayloadType;
    uint8_t           ClearanceAccess;
    uint8_t           Reserve2;
    uint8_t           SecurityPayload[16];

} SecurityAccessInfo_t;


typedef struct
{

    uint16_t          Command;
    uint16_t          SubCommand;
    uint8_t           Payload[16];

} SecurityAccessCommand_t;


typedef struct
{
  uint32_t		 	  Command;
  uint32_t            UserPassCode;     //Number only - 4 to 8 digit

} stSystemUtilsPasscodeCommand_t; //OBSOLETE - Will not longer be available


typedef struct
{
  uint32_t		 	  Command;				//See System Utils DK Adv. header config Command
  uint8_t             ParamValue[8];     	//Depend on the command

} stSystemUtilsAdvHeaderConfigCommand_t;


typedef struct
{
    uint8_t 	Addr[B_ADDR_LEN_8];
    uint8_t     ProductType;
    uint8_t		SingleHostControl;
    uint8_t		SecurityCode[SECURITY_KEY_CODE_MAX_SIZE]; //WARNING: Security Code must terminate with NULL (0x00) and cannot contain NULL in between

} stPairingInfo_t;

typedef struct
{
    uint8_t 	Addr[B_ADDR_LEN_8];
    uint8_t     ProductType;
    uint8_t		Reserve;
    uint8_t		SecurityCode[SECURITY_KEY_CODE_MAX_SIZE]; //WARNING: Security Code must terminate with NULL (0x00) and cannot contain NULL in between

} stRenewSecurityInfo_t;


typedef struct
{
    uint8_t 	Addr[B_ADDR_LEN_8];
    uint8_t    ProductType;
    uint8_t		Reserve;

} stUpdatePeripheralRtcInfo_t;

typedef struct
{
    uint8_t 	Addr[B_ADDR_LEN_8];
    uint8_t     ProductType;

} DevAddressAndType_t;


typedef struct
{
    uint8_t 								HeaderID; //Preamble
    union AggregatedHeaderPageInfo_u     	PageInfo;
    union AggregatedHeaderFormatInfo_u		FormatInfo;
    uint8_t									Reserver;

} AggregatedDataHeader_t;

typedef struct
{

    uint8_t                             HeaderID;   //Preamble
    union MultiTransacDataPageInfo_u    PageInfo;
    uint8_t                             ValidByte; //Total valid byte in Payload
    uint8_t	                            Reserver;
    uint8_t                             Payload[MULTI_TRANSAC_DATA_LEN];

} MultiTransacDataFormat_t;

typedef struct
{
    uint32_t    PeripheralObjID;
    uint8_t     PeripheralName[48];

} PeripheralNameUpdate_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

static const uint8_t DkBaseUUID[UUID_128_SIZE] =
{
	/*																  		| Change |			*/
	0x13, 0xDB, 0xBB, 0x6E, 0x5A, 0x60, 0xD3, 0xAF, 0x9D, 0x46, 0xD4, 0xF0, 0xFF, 0xFF, 0x27, 0x0D
};

/******************************************************
 *               Function Declarations
 ******************************************************/

extern uint8_t DKPeripheral_GetDataTypeLenForDataTypeID(uint8_t DataTypeID);

extern uint8_t* DkPeripheral_GetDataWithDataTypeIDExt(uint8_t DataTypeID, uint16_t AdDataTypeInfoLen, uint8_t* AdvDataTypeInfoContainer);

extern int16_t DkPeripheral_GetInfoContainerIndexOfDataTypeIDExt(uint8_t DataTypeID, uint16_t AdDataTypeInfoLen, uint8_t* AdvDataTypeInfoContainer);

extern uint8_t DkPeripheral_IsUuidMatch(uint8_t* uuid_string, uint8_t* uuid_bin);

#ifdef __cplusplus
} /*extern "C" */
#endif

#endif //BLE_PERIPHERAL_INFO_H
