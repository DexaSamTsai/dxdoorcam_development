//============================================================================
// File: dk_peripheralAux.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef BLE_PERIPHERAL_AUX_INFO_H
#define BLE_PERIPHERAL_AUX_INFO_H

#pragma once

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Defines
 ******************************************************/
#define GEN_CONFIG_PAYLOAD_MAX_SIZE     12

#define B_ADDR_LEN_8                    8

#define ALARM_ARMINGMODE_NONE           0xFF
#define ALARM_ARMINGMODE_DISARM         0x00
#define ALARM_ARMINGMODE_HOME           0x01
#define ALARM_ARMINGMODE_ARM            0x02
#define ALARM_ARMINGMODE_ALERT          0x10
#define ALARM_ARMINGMODE_SYS_SABOTAGED  0x20

#define ALARM_ARMINGMODE_DISARMING      0x80
#define ALARM_ARMINGMODE_HOMING         0x81
#define ALARM_ARMINGMODE_ARMING         0x82
#define ALARM_ARMINGMODE_SENSOR_DETECT  0x90

#define ALARM_ZONE_NONE                 0x00
#define ALARM_ZONE_ALL                  0xFF
#define ALARM_ZONE_NORMAL               0x01
#define ALARM_ZONE_HOME                 0x02
#define ALARM_ZONE_24HR                 0x04
#define ALARM_ZONE_SABOTAGE             0x08
#define ALARM_ZONE_DOS_ATTACK           0x10

#define ALARM_EMERGENCY_PHONE_AMOUNT    8

# define IPCAM_MOTION_ACTIVE_WINDOW_MAX_COUNT		3

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    COMMAND_UNKNOWN = 0,
    COMMAND_CHANGE_TO_MANUAL_MODE               = 1,    //NO Param
    COMMAND_CHANGE_TO_AUTOMATIC_MODE            = 2,    //NO Param
    COMMAND_SET_VALVE_MOVE_TO_MOUNTING_POS      = 3,    //No Param
    COMMAND_SET_VALVE_START_READAPTATION        = 4,    //No Param
    COMMAND_SET_REALTIME_CLOCK                  = 5,    //ThermoRealTimeClock_t
    COMMAND_SHUDOWN_MODE                        = 6,    //No Param
    COMMAND_CHANGE_TO_MANUAL_WITH_GIVEN_TEMP    = 7,    //ThermoTemp_t
    COMMAND_CHANGE_OPEN_WINDOW_DETECT_THRESHOLD = 8,    //ThermoWindowOpenDect_t

} eThermostatGenConfigMode_t;


typedef enum
{
    ALL_KEY_RELEASED        = 1,
    KEY_UP_PRESSED          = 2,
    KEY_DOWN_PRESSED        = 3,
    KEY_LEFT_PRESSED        = 4,
    KEY_RIGHT_PRESSED       = 5,
    KEY_AUX1_PRESSED        = 6,

} eRemoteControlKeyEvent_t;

typedef enum
{
    ALARM_STATE_DISARM      = 0,
    ALARM_STATE_ARM         = 1,
    ALARM_STATE_HOME        = 2,
    ALARM_STATE_ALARM       = 3,
    ALARM_STATE_SOS         = 4,
    ALARM_STATE_SABOTAGE    = 5,
    ALARM_STATE_DOS_ATTACK  = 6,
} eAlarmStateEvent_t;

typedef enum
{
    POWER_SOURCE_ADAPTOR       = 0,
    POWER_SOURCE_BATTERY,
} eAlarmPowerSource_t;

typedef enum
{
    BATTERY_FULL       = 0,
    BATTERY_CHARGING,
    BATTERY_DISCHARGING,
} eAlarmBatteryState_t;


typedef enum
{
    GI_NONE = 0,

    //Alarm Generic Infos
    GI_ALARM_SIM_CARD_STATE = 100,  //uint8_t, eSimCardState_t
} eGenericInfoType_t;

typedef enum
{
    GC_NONE = 0,

    //Alarm Generic Configs
    GC_ALARM_APPNOTIFICATION_ALERT = 100,       //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_SOS,               //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_SABOTAGE,          //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_POWERSOURCE,       //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_WIFISTATE,         //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_GSMSTATE,          //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_SILENTALERT,       //uint8_t, eConfigTypeOps_t
    GC_ALARM_APPNOTIFICATION_DISARMSABTG,       //uint8_t, eConfigTypeOps_t

    GC_ALARM_CELLULARSMS_ALERT = 200,           //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_SOS,                   //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_SABOTAGE,              //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_POWERSOURCE,           //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_WIFISTATE,             //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_GSMSTATE,              //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_SILENTALERT,           //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARSMS_DISARMSABTG,           //uint8_t, eConfigTypeOps_t

    GC_ALARM_CELLULARCALL_ALERT = 300,          //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_SOS,                  //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_SABOTAGE,             //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_POWERSOURCE,          //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_WIFISTATE,            //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_GSMSTATE,             //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_SILENTALERT,          //uint8_t, eConfigTypeOps_t
    GC_ALARM_CELLULARCALL_DISARMSABTG,          //uint8_t, eConfigTypeOps_t

    GC_ALARM_INCOMING_CALL_REJECT_DELAY = 1000, //uint8_t, delay in seconds, this option currently is only for Alarm FW internal use.
    GC_ALARM_DISPLAY_OFF_TIMING,                //uint16_t, 1 unit is 100ms, this option currently is only for Alarm FW internal use.
    GC_ALARM_OUTGOING_CALL_HANGUP_DELAY,        //uint8_t, delay in seconds, this option currently is only for Alarm FW internal use.
} eGenericConfigType_t;

typedef enum
{
    OPERATION_NOT_SUPPORTED = 0,
    OPERATION_ENABLE,
    OPERATION_DISABLE,
} eConfigTypeOps_t;

typedef enum
{
    SIM_CARD_STATE_NODATA = 0,
    SIM_CARD_STATE_NOT_DETECTED,
    SIM_CARD_STATE_PINCODE_LOCKED,
    SIM_CARD_STATE_PINCODE_UNLOCKED,
    SIM_CARD_STATE_PINCODE_PUK,
} eSimCardState_t;

typedef enum
{
    PINCODE_DISARM       = 0,
    PINCODE_ADMIN,
    PINCODE_SILENTALERT,
} ePinCodeMode_t;

typedef enum
{
    EVENT_NONE          = 0x00,
    EVENT_VIDEO_RECORD  = 0x01,
    EVENT_IMAGE_RECORD  = 0x02,
    EVENT_RECORD_BOTH   = 0x04,

} eRecordingEvent_t;

typedef enum
{
    POWER_STATUS_UNKNOWN = -1,
    POWER_IS_OFF         = 0,
    POWER_IS_ON          = 1,
} ePowerStatus_t;

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
#define DimmerValueInPercent_t              uint8_t

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
    uint8_t     PackedPayloadLen;
    uint8_t     PackedPayload[63];

} GeneralSizePackedPayload_t;


typedef struct
{
    uint8_t   Minutes;    //0 to 59
    uint8_t   Hour;       //0 to 23
    uint8_t   Day;        //0 to 31
    uint8_t   Month;      //0 to 12
    uint8_t   Year;       //0 to 99 - For example, 16 is 2016

} ThermoRealTimeClock_t;

typedef struct
{
    //0x0A = 5.0 C, 0x0B = 5.5 C ..... 0x3C = 30 C
    //0x09 = OFF, 0x3D = ON
    uint8_t   Heat;

} ThermoTemp_t;

typedef struct
{
    //0x01 = 1/12 C, 0x02 = 2/12 C ..... 0x12 = 12/12 C
    //0xFF = Deactivate WindowOpen Detection
    uint8_t   Threshold;

} ThermoWindowOpenDect_t;

typedef struct
{
    //0x00 to 0x8F where 0x00 = 0:00, 0x01 = 0:10... 0x8F = 23:50
    //0xFF = OFF
    uint8_t     FirstONTime;
    uint8_t     FirstOFFTime;
    uint8_t     SecondONTime;
    uint8_t     SecondOFFTime;
    uint8_t     ThirdONTime;
    uint8_t     ThirdOFFTime;

} SwitchPointDay_t;


typedef struct
{
    SwitchPointDay_t    Monday;
    SwitchPointDay_t    Tuesday;
    SwitchPointDay_t    Wednesday;
    SwitchPointDay_t    Thursday;
    SwitchPointDay_t    Friday;
    SwitchPointDay_t    Saturday;
    SwitchPointDay_t    Sunday;

} CBSwitchPointConfig_t;


typedef struct
{
    //0x09 = OFF; 0x0A = 5.0C, 0x0B = 5.5C.... 0x3C = 30C; 0x3D = ON
    //Set to 0x80 = NO CHANGE to the valve setting.
    uint8_t     SetCurrentTemperature;
    uint8_t     SetEnergySavingTemp;
    uint8_t     SetHeatingTemperature;

    //0xFA = -3.0°C; 0xFB = -2.5°C; 0xFC = -2.0°C … 0xFF =-0.5°C; 0x00 = 0°C; 0x01 = 0.5°C; 0x02 = 1.0°C … 0x06 = 3.0°C
    //Set to 0x80 = NO CHANGES to the valve setting.
    uint8_t     OffsetTemperatureValue;

    //0x01 = 1/12°C; 0x02 = 2/12°C … 0x12 = 12/12°C
    //0xFF will deactivate this function.
    //Set to 0x80 = NO CHANGES to the valve setting.
    uint8_t     WindowDetectionThreshold;

    //0x01 = 1 Minute;
    //0x02 = 2 Minutes;
    //..
    //0x1E = 30 Minutes;
    //Set to 0x80 = NO CHANGES to the valve setting.
    uint8_t     WindowOpenDetectionTimer;

} CBSetTemperatures_t;

typedef struct
{
    eThermostatGenConfigMode_t      Command;

    //Depending on Command the payload will contain the data of corresponding data structure.
    //For example if command is COMMAND_CHANGE_TO_MANUAL_WITH_GIVEN_TEMP then it will contain data of data structure ThermoTemp_t
    uint8_t                         PayloadSize;
    uint8_t                         Payload[GEN_CONFIG_PAYLOAD_MAX_SIZE];
} CBGeneralConfig_t;

typedef struct
{
    uint8_t     PackedPayloadLen;
    uint8_t     PackedPayload[63];

} ThermoPackedPayload_t;


typedef struct
{
    uint16_t    PackedPayloadLen;
    uint8_t     PackedPayload[256];

} IRRemotePackedPayload_t;


typedef struct
{
    uint16_t    PackedPayloadLen;
    uint8_t     PackedPayload[256];
} AlarmPackedPayload_t;


typedef struct
{
    uint16_t    PackedPayloadLen;
    uint8_t     *pPackedPayload;
} AlarmPackedPayloadAlloc_t;


#pragma pack(2)

typedef struct
{
    uint32_t        RemoteType;
    uint32_t        RemoteKeyType;
    uint16_t        RemoteNameSize;
    uint16_t        RemoteKeyNameSize;
} IRRemoteNewLearningHeader_t;

typedef struct
{
    uint64_t        RemoteID;
    uint32_t        RemoteKeyType;
    uint16_t        RemoteKeyNameSize;
} IRRemoteInsertLearningHeader_t;

typedef struct
{
    uint64_t        RemoteID;
    uint64_t        RemoteKeyID;
    uint32_t        RemoteKeyType;
    uint16_t        RemoteKeyNameSize;
} IRRemoteUpdateLearningHeader_t;

typedef struct
{
    uint64_t        RemoteID;
} IRRemoteDeleteHeader_t;

typedef struct
{
    uint64_t        RemoteID;
    uint64_t        RemoteKeyID;
} IRRemoteKeyDeleteHeader_t;

typedef struct
{
    uint64_t        RemoteID;
    uint64_t        RemoteKeyID;
} IRRemoteSendHeader_t;

#pragma pack()

#pragma pack(2)
typedef struct
{
    uint8_t     ArmingMode;
} AlarmChangeAlarmArmingModeHeader_t;

typedef struct
{
    uint8_t     Zone;
    uint16_t    JobLength;
} AlarmJobConditionUpdateForSpecificZoneHeader_t;

typedef struct
{
    uint16_t    JobEnableLength;
    uint16_t    JobDisableLength;
} AlarmJobExecutionUpdateHeader_t;

typedef struct
{
    uint16_t    ExecutionTimeout;
    uint16_t    JobEnableLength;
    uint16_t    JobDisableLength;
} AlarmJobExecutionUpdateAndTimeoutHeader_t;

typedef struct
{
    uint16_t    JobEnableLength;    //Job executiion length for enable alarm
    uint8_t     *pJobEnable;        //Job executiion for enable alarm
    uint16_t    JobDisableLength;   //Job executiion length for disable alarm
    uint8_t     *pJobDisable;       //Job executiion for disable alarm
} AlarmJobExecutionPtr_t;

typedef struct
{
    uint8_t     LengthOfPinCode;
} AlarmSetPinCodeHeader_t;

typedef struct
{
    uint8_t     PinCodeMode; //See ePinCodeMode_t
    uint8_t     LengthOfPinCode;
} AlarmSetPinCodeHeaderExt_t;

typedef struct
{
    uint8_t     AmountofPhoneNumbers;
    uint16_t    TotalPhoneNumbersDataLength;
} AlarmSetEmergencyCallPhoneNumbersHeader_t;

typedef struct
{
    uint8_t         AmountOfPhones;                                 //Total amount of phone records
    uint8_t         ReferenceID[ALARM_EMERGENCY_PHONE_AMOUNT];      //RFU for quick dialing or etc.
    uint8_t         PhoneNumber[ALARM_EMERGENCY_PHONE_AMOUNT][20];  //Phone numbers in 0-ended ASCII
    uint8_t         NameByteCount[ALARM_EMERGENCY_PHONE_AMOUNT];    //Phone names in UTF-8 length count in bytes
    uint8_t         Name[ALARM_EMERGENCY_PHONE_AMOUNT][60];         //Phone names in UTF-8
} AlarmPhoneNumberWithName_t;

typedef struct
{
    uint8_t     DelayMode;
    //#define ALARM_ARMINGMODE_DISARM         0x00
    //#define ALARM_ARMINGMODE_HOME           0x01
    //#define ALARM_ARMINGMODE_ARM            0x02
    uint8_t     DevAddr[B_ADDR_LEN_8];
} AlarmArmingDelayWhiteListItem_t;

typedef struct
{
    uint8_t                             AmountOfWhiteList;
    AlarmArmingDelayWhiteListItem_t     *Items;
} AlarmArmingDelayWhiteListInstance_t;

typedef struct
{
    uint16_t                            ArmingDelayDisarm; //Arming delay for mode Disarm
    uint16_t                            ArmingDelayHome;   //Arming delay for mode Home
    uint16_t                            ArmingDelayArm;    //Arming delay for mode Arm
} AlarmSetArmingDelayHeader_t;

typedef struct
{
    AlarmSetArmingDelayHeader_t         Header;
    AlarmArmingDelayWhiteListInstance_t WhiteList;
} AlarmSetArmingDelayInfo_t;

typedef struct
{
    uint8_t     Zone;
} AlarmSensorDetectHeader_t;

typedef struct
{
    uint8_t                 Source;         //eAlarmPowerSource_t
    uint8_t                 BatteryState;   //eAlarmBatteryState_t
    uint8_t                 BatteryLevel;   //Unit in percent
} AlarmPowerStateInfo_t;

typedef struct
{
    uint16_t                GenericConfigType;  //eGenericConfigType_t
    uint16_t                ConfigLen;          //Length in byte
} GenericConfigHeader_t;

typedef struct
{
    uint16_t                GenericInfoType;    //eGenericInfoType_t
    uint16_t                InfoLen;            //Length in byte
} GenericInfoHeader_t;

typedef struct
{
    uint8_t Gc_Alarm_Appnotification_Alert;         //GC_ALARM_APPNOTIFICATION_ALERT = 100, , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Sos;           //GC_ALARM_APPNOTIFICATION_SOS,         , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Sabotage;      //GC_ALARM_APPNOTIFICATION_SABOTAGE,    , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Powersource;   //GC_ALARM_APPNOTIFICATION_POWERSOURCE, , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Wifistate;     //GC_ALARM_APPNOTIFICATION_WIFISTATE,   , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Gsmstate;      //GC_ALARM_APPNOTIFICATION_GSMSTATE,    , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Silentalert;   //GC_ALARM_APPNOTIFICATION_SILENTALERT, , eConfigTypeOps_t
    uint8_t Gc_Alarm_Appnotification_Disarmsabtg;   //GC_ALARM_APPNOTIFICATION_DISARMSABTG, , eConfigTypeOps_t

    uint8_t Gc_Alarm_Cellularsms_Alert;             //GC_ALARM_CELLULARSMS_ALERT = 200,     , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Sos;               //GC_ALARM_CELLULARSMS_SOS,             , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Sabotage;          //GC_ALARM_CELLULARSMS_SABOTAGE,        , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Powersource;       //GC_ALARM_CELLULARSMS_POWERSOURCE,     , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Wifistate;         //GC_ALARM_CELLULARSMS_WIFISTATE,       , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Gsmstate;          //GC_ALARM_CELLULARSMS_GSMSTATE,        , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Silentalert;       //GC_ALARM_CELLULARSMS_SILENTALERT,     , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularsms_Disarmsabtg;       //GC_ALARM_CELLULARSMS_DISARMSABTG,     , eConfigTypeOps_t

    uint8_t Gc_Alarm_Cellularcall_Alert;            //GC_ALARM_CELLULARCALL_ALERT = 300,    , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Sos;              //GC_ALARM_CELLULARCALL_SOS,            , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Sabotage;         //GC_ALARM_CELLULARCALL_SABOTAGE,       , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Powersource;      //GC_ALARM_CELLULARCALL_POWERSOURCE,    , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Wifistate;        //GC_ALARM_CELLULARCALL_WIFISTATE,      , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Gsmstate;         //GC_ALARM_CELLULARCALL_GSMSTATE,       , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Silentalert;      //GC_ALARM_CELLULARCALL_SILENTALERT,    , eConfigTypeOps_t
    uint8_t Gc_Alarm_Cellularcall_Disarmsabtg;      //GC_ALARM_CELLULARCALL_DISARMSABTG,    , eConfigTypeOps_t

    uint8_t Gc_Alarm_Incoming_Call_Reject_Delay;    //GC_ALARM_INCOMING_CALL_REJECT_DELAY,  , uint8_t, this option currently is only for Alarm FW internal use.
    uint16_t Gc_Alarm_Display_Off_Timing;           //GC_ALARM_DISPLAY_OFF_TIMING,          , uint16_t, 1 unit is 100ms, this option currently is only for Alarm FW internal use.
    uint8_t Gc_Alarm_Outgoing_Call_Hangup_Delay;    //GC_ALARM_OUTGOING_CALL_HANGUP_DELAY,  , uint8_t, this option currently is only for Alarm FW internal use.
} AlarmConfigs_t;

typedef struct
{
    uint16_t     Control;
    uint16_t     Count;

} SirenControl_t;

typedef struct
{
    uint8_t     Seconds;
    uint8_t     Minutes;
    uint8_t     Hours24H;
    uint8_t     DaysOfWeek; //01 = SUNDAY, 02 = MONDAY, etc.....
    uint8_t     Dates;
    uint8_t     Months;
    uint8_t     Years;      //Since 2000, for example, for Year 2017 set year to 17.

} RTC_t;

typedef struct
{
    uint8_t     Red;
    uint8_t     Green;
    uint8_t     Blue;
    uint8_t     Reserve;

} ColorModel_t;
    
typedef struct
{
    uint16_t    Hue;
    uint8_t     Saturation;
    uint8_t     Dimmer;
    uint8_t     OnOff;
    uint8_t     Reserve;
    
} LightStripModel_t;

#pragma pack()

/* IPCam */
typedef struct {
    uint16_t    PackedPayloadLen;
    uint8_t     *pPackedPayload;
} GenericPackedPayloadAlloc_t;

typedef enum {
	IPCAM_RESOLUTION_1080P	= 0,
	IPCAM_RESOLUTION_720P,
	IPCAM_RESOLUTION_480,						// Capture Image Only
} IPCAM_RESOLUTION;

typedef enum {
	IPCAM_BITRATE_256KBPS	= 0,
	IPCAM_BITRATE_512KBPS,
	IPCAM_BITRATE_768KBPS,
	IPCAM_BITRATE_1024KBPS,
	IPCAM_BITRATE_2048KBPS,
	IPCAM_BITRATE_3072KBPS,
	IPCAM_BITRATE_4096KBPS,
} IPCAM_BITRATE;

typedef enum {
	IPCAM_FPS_12	= 0,
	IPCAM_FPS_15,
	IPCAM_FPS_24,
	IPCAM_FPS_30,
} IPCAM_FPS;

typedef struct {
    IPCAM_RESOLUTION				Resolution;
    IPCAM_BITRATE					Bitrate;
    IPCAM_FPS						FPS;
} IPCamStreamingConfigHeader_t;

typedef enum {
	IPCAM_RECORDING_RULE_FLEXIBLE_PRERECORD		= 0,
	IPCAM_RECORDING_RULE_FLEXIBLE_POSTRECORD,
	IPCAM_RECORDING_RULE_FLEXIBLE_BOTH,
} IPCAM_RECORDING_RULE;

typedef struct {
	uint8_t							Avail;
    uint16_t						PreRecordDuration;
    uint16_t						PostRecordDuration;
    IPCAM_RECORDING_RULE			Role;
} IPCamEventRequestSettings_t;

typedef struct {
    IPCamEventRequestSettings_t			VideoEvt;
    IPCamEventRequestSettings_t			JPEGEvt;
} IPCamEventRequestHeader_t;

typedef enum {
	IPCAM_MOTION_MODE_OFF				= 0,
	IPCAM_MOTION_MODE_ON,
} IPCAM_MOTION_MODE;

typedef enum {
	IPCAM_MOTION_SENSITIVITY_LOW				= 0,
	IPCAM_MOTION_SENSITIVITY_MIDDLE,
	IPCAM_MOTION_SENSITIVITY_HIGH,
} IPCAM_MOTION_SENSITIVITY;

typedef struct {
    float							WinStartX;
    float							WinStartY;
    float							WinEndX;
    float							WinEndY;
    IPCAM_MOTION_SENSITIVITY		Sensitivity;
} IPCamActiveWindow_t;

typedef struct {
	IPCAM_MOTION_MODE				Mode;
	uint16_t						ActiveWindowLen;
	IPCamActiveWindow_t				ActiveWindow[ IPCAM_MOTION_ACTIVE_WINDOW_MAX_COUNT ];
} IPCamMotionConfigHeader_t;

typedef enum {
	IPCAM_UP_SIDE_DOWN_OFF				= 0,
	IPCAM_UP_SIDE_DOWN_ON,
} IPCAM_UP_SIDE_DOWN;

typedef struct {
    IPCAM_UP_SIDE_DOWN				UpSideDown;
} IPCamVideoConfigHeader_t;

typedef enum {
	IPCAM_CAPTURE_IMAGE_FORMAT_JPEG		= 0,
} IPCAM_CAPTURE_IMAGE_FORMAT;

typedef struct {
	IPCAM_RESOLUTION				Resolution;
	IPCAM_CAPTURE_IMAGE_FORMAT		Format;
    uint8_t							*Image;
    uint32_t						ImageSize;
} IPCamCaptureImage_t;

typedef enum {
	IPCAM_DOWN_STREAMING_ACTION_STOP	= 0,
	IPCAM_DOWN_STREAMING_ACTION_PLAY,
} IPCAM_DOWN_STREAMING_ACTION;

typedef struct {
	IPCAM_DOWN_STREAMING_ACTION		Action;
    char							*Url;
    uint32_t						UrlLength;
} IPCamDownStreaming_t;

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

//Convert

/* Thermostat */
extern ThermoPackedPayload_t ThermostatCB_ConvertScheduleToPackedPayload(CBSwitchPointConfig_t ScheduleSetup);
extern ThermoPackedPayload_t ThermostatCB_ConvertGeneralConfigToPackedPayload(eThermostatGenConfigMode_t Command, uint8_t* ParameterBaseOnCommand); //Check required Parameter from eThermostatGenConfigMode
extern ThermoPackedPayload_t ThermostatCB_ConvertSetTemperaturesToPackedPayload(CBSetTemperatures_t TemperaturesSetting);

/* IR Remote */
extern IRRemotePackedPayload_t IRRemote_ConvertNewRemoteLearningPackedPayload( uint32_t RemoteMType, uint32_t RemoteDType, uint8_t* RemoteMNameUnicode8, uint8_t* RemoteDNameUnicode8);
extern IRRemotePackedPayload_t IRRemote_ConvertInsertKeyRemoteLearningPackedPayload( uint64_t RemoteMID, uint32_t RemoteDType, uint8_t* RemoteDNameUnicode8);
extern IRRemotePackedPayload_t IRRemote_ConvertUpdateKeyRemoteLearningPackedPayload( uint64_t RemoteMID, uint64_t RemoteDID, uint32_t RemoteDType, uint8_t* RemoteDNameUnicode8);
extern IRRemotePackedPayload_t IRRemote_ConvertDeleteRemotePackedPayload( uint64_t RemoteMID);
extern IRRemotePackedPayload_t IRRemote_ConvertDeleteKeyRemotePackedPayload( uint64_t RemoteMID, uint64_t RemoteDID);
extern IRRemotePackedPayload_t IRRemote_ConvertSendRemotePackedPayload( uint64_t RemoteMID, uint64_t RemoteDID);

/* Alarm System */
extern AlarmPackedPayload_t Alarm_ConvertChangeAlarmArmingModePackedPayload(uint8_t ArmingMode);
/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertJobConditionUpdateForSpecificZonePackedPayload_ALLOC(uint8_t Zone, uint16_t JobLength, void* JobPayloadIn);
/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertJobExecutionUpdateAndTimeoutPackedPayload_ALLOC(uint16_t ExecutionTimeout, uint16_t JobEnableLength, void* JobEnablePayloadIn, uint16_t JobDisableLength, void* JobDisablePayloadIn);
/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertSetEmergencyCallPhoneNumbersWithNamesPackedPayload(AlarmPhoneNumberWithName_t* pPhoneNumberArray);
extern AlarmPackedPayload_t Alarm_ConvertSetPinCodePackedPayload(uint8_t *PinCode); //WILL BE OBSOLETE!
extern AlarmPackedPayload_t Alarm_ConvertSetPinCodePackedPayload_EXT(uint8_t PinCodeMode, uint8_t *PinCode);
extern bool Alarm_ArmingDelayCreateWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList);
extern bool Alarm_ArmingDelayFreeWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList);
extern bool Alarm_ArmingDelayAddWhiteList(AlarmArmingDelayWhiteListInstance_t *pWhiteList, AlarmArmingDelayWhiteListItem_t *WhiteListItem);
extern AlarmPackedPayload_t Alarm_ConvertSetArmingDelayPackedPayloadWithWhitelist(AlarmSetArmingDelayHeader_t *pArmingDelays, AlarmArmingDelayWhiteListInstance_t *pWhiteList);
/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertSetArmingDelayPackedPayloadWithWhitelist_ALLOC(AlarmSetArmingDelayHeader_t *pArmingDelays, AlarmArmingDelayWhiteListInstance_t *pWhiteList);
extern AlarmPackedPayload_t Alarm_ConvertSetArmingDelayPackedPayload(AlarmSetArmingDelayHeader_t ArmingDelays);
extern AlarmPackedPayload_t Alarm_ConvertSensorDetectPackedPayload(uint8_t Zone);
/* !!! Please make sure Alarm_FreePackedPayload(AlarmPackedPayload_t) to free memory allocated by this function */
extern AlarmPackedPayloadAlloc_t Alarm_ConvertConfigsPackedPayload(AlarmConfigs_t *pAlarmConfigs);

/* Siren */
extern GeneralSizePackedPayload_t Siren_ConvertAlertControlCountModePacketPayload(uint16_t AlertCount);
extern GeneralSizePackedPayload_t Siren_ConvertAlertControlOnOffPacketPayload(uint8_t OnOff); //0 - OFF, 1 - ON

/* Remote Control */
extern GeneralSizePackedPayload_t RemoteControl_ConvertSetRTCPacketPayload(RTC_t RTCtoSet);

/* RGB LightBulb */
extern GeneralSizePackedPayload_t RGBLightBulb_ConvertColorModelPacketPayload(ColorModel_t Model);
    
/* Light Strip */
extern GeneralSizePackedPayload_t LightStrip_ConvertLightStripModelPacketPayload(LightStripModel_t Model);

/* IPCam */
extern GenericPackedPayloadAlloc_t IPCam_ConvertSetStreamingConfigPackedPayload( IPCamStreamingConfigHeader_t StreamingConfig );
extern GenericPackedPayloadAlloc_t IPCam_ConvertSetEventRequestPackedPayload( IPCamEventRequestHeader_t EventRequest );
extern GenericPackedPayloadAlloc_t IPCam_ConvertSetMotionConfigPackedPayload( IPCamMotionConfigHeader_t MotionConfig );
extern GenericPackedPayloadAlloc_t IPCam_ConvertSetVideoConfigPackedPayload( IPCamVideoConfigHeader_t VideoConfig );
extern GenericPackedPayloadAlloc_t IPCam_ConvertCaptureImagePackedPayload( IPCamCaptureImage_t CaptureImage );
extern GenericPackedPayloadAlloc_t IPCam_ConvertDownStreamingPackedPayload( IPCamDownStreaming_t DownStreaming );

/* HomeKit */
extern GeneralSizePackedPayload_t HomeKit_ConvertPowerStatusPacketPayload(ePowerStatus_t PowerStatus);
extern GeneralSizePackedPayload_t HomeKit_ConvertLightDimmerPacketPayload(DimmerValueInPercent_t Percentage);

//Parser

/* Thermostat */
extern CBSwitchPointConfig_t ThermostatCB_ParseSchedulePackedPayload(void* SchedulePayloadPacket, uint16_t PayloadPacketSize);
extern CBGeneralConfig_t ThermostatCB_ParseGeneralConfigPackedPayload(void* GeneralConfigPayloadPacket, uint16_t PayloadPacketSize);
extern CBSetTemperatures_t ThermostatCB_ParseSetTemperaturesToPackedPayload(void* SetTemperaturesPayloadPacket, uint16_t PayloadPacketSize);

/* IR Remote */
extern IRRemoteSendHeader_t IRRemote_ParseSendRemotePackedPayload( void* IRSendPayloadPacket, uint16_t IRSendPayloadPacketSize );

/* Alarm System */
extern AlarmSetArmingDelayHeader_t Alarm_ParseArmingDelayPackedPayload(uint16_t ArmingDelayPacketLen, void* ArmingDelayPayloadPacket);
//We'll use ArmingDelayPayloadPacket buffer be part of the returned data,
//so don't free ArmingDelayPayloadPacket until all your data  is extracted
extern AlarmSetArmingDelayInfo_t Alarm_ParseArmingDelayPackedPayloadWithWhiteList(uint16_t ArmingDelayPacketLen, void* ArmingDelayPayloadPacket);
extern AlarmArmingDelayWhiteListItem_t *Alarm_GetWhiteListItem(AlarmArmingDelayWhiteListInstance_t *pWhiteList, uint8_t index);
//extern AlarmPhoneNumber_t Alarm_ParsePhoneNumbersPackedPayload(uint16_t PhoneNumbersPacketLen, void* PhoneNumbersPayloadPacket);
//Please allocate the buffer for pPhoneNumbers, size is sizeof(AlarmPhoneNumberWithName_t)
extern void Alarm_ParsePhoneNumbersWithNamesPackedPayload(uint16_t PhoneNumbersPacketLen, void* PhoneNumbersPayloadPacket, AlarmPhoneNumberWithName_t *pPhoneNumbers);
//extern AlarmJobExecution_t Alarm_ParseJobExecutionPackedPayload(uint16_t JobExecutionPacketLen, void* JobExecutionPayloadPacket);
extern AlarmJobExecutionPtr_t Alarm_ParseJobExecutionPackedPayloadPtr(uint16_t JobExecutionPacketLen, void* JobExecutionPayloadPacket);
extern AlarmPowerStateInfo_t Alarm_ParsePowerStatePackedPayload(uint16_t PowerStatePacketLen, void* PowerStatePayloadPacket);
extern uint8_t Alarm_ParseSimState(uint16_t PowerStatePacketLen, void* PowerStatePayloadPacket); //returns eSimCardState_t
extern AlarmConfigs_t Alarm_ParseConfigPackedPayload(uint16_t ConfigPacketLen, void* pConfigPayloadPacket);

/* IPCam */
extern IPCamStreamingConfigHeader_t IPCam_ParseStreamingConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern IPCamEventRequestHeader_t IPCam_ParseEventRequestPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern IPCamMotionConfigHeader_t IPCam_ParseMotionConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern IPCamVideoConfigHeader_t IPCam_ParseVideoConfigPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern IPCamCaptureImage_t IPCam_ParseCaptureImagePackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern IPCamDownStreaming_t IPCam_ParseDownStreamingPackedPayload( uint16_t PackedPayloadLen, void *PackedPayload );

/* HomeKit */
extern ePowerStatus_t HomeKit_ParsePowerStatusPacketPayload( uint16_t PackedPayloadLen, void *PackedPayload );
extern DimmerValueInPercent_t HomeKit_ParseLightDimmerPacketPayload( uint16_t PackedPayloadLen, void *PackedPayload );

//Util
extern bool Alarm_CheckConfigPackedPayload(uint16_t ConfigPacketLen, void* pConfigPayloadPacket);
extern bool Alarm_CheckIsGsmRequired(AlarmConfigs_t *AlarmConfigs);

/* Alarm System */
extern void Alarm_FreePackedPayload(AlarmPackedPayloadAlloc_t AlarmPackedPayloadAlloc);
extern void Generic_FreePackedPayload( GenericPackedPayloadAlloc_t PackedPayload );

#ifdef __cplusplus
} /*extern "C" */
#endif

#endif //BLE_PERIPHERAL_AUX_INFO_H
