//============================================================================
// File: dk_Job.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include "dk_Job.h"
#include "Utils.h"
#include "dk_hex.h"

/******************************************************
 *                      Macros
 ******************************************************/

#ifdef DK_GATEWAY

#define DK_JOB_ALLOC(name, nelems, elemsize) mem_calloc_AlignmentPack(name,nelems,elemsize);

#else

#define DK_JOB_ALLOC(name, nelems, elemsize) calloc(nelems,elemsize);


#endif

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Local Function Definitions
 ******************************************************/

void LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance_t* JobInstance, BleStandardJobExecHeader_t* StdExecHeader, uint8_t* pStdExecData)
{
    uint8_t*	pExecutionItem = NULL;
    
    if (JobInstance->pExecution)
    {
        //Alloc large enought to fit previously added execution and current new added one
        pExecutionItem =  DK_JOB_ALLOC("ReAllocJobExecItem", 1, JobInstance->ExecutionLen +sizeof(BleStandardJobExecHeader_t)+StdExecHeader->DataLen);
        
        
        //Copy the previously added execution to the newly allocated buffer
        memcpy(pExecutionItem, JobInstance->pExecution, JobInstance->ExecutionLen);
        
        //Free previous one
        free(JobInstance->pExecution);
        JobInstance->pExecution = NULL;
    }
    else
    {
        pExecutionItem = DK_JOB_ALLOC("JobExecItem", 1, sizeof(BleStandardJobExecHeader_t)+StdExecHeader->DataLen);
    }
    
    memcpy(pExecutionItem + JobInstance->ExecutionLen, StdExecHeader, sizeof(BleStandardJobExecHeader_t));
    JobInstance->ExecutionLen += sizeof(BleStandardJobExecHeader_t);
    if (StdExecHeader->DataLen)
    {
        memcpy(pExecutionItem + JobInstance->ExecutionLen, pStdExecData, StdExecHeader->DataLen);
        JobInstance->ExecutionLen += StdExecHeader->DataLen;
    }
    
    JobInstance->pExecution = pExecutionItem;
    
    JobInstance->Header.TotalJobExecution++;
}


void LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance_t* JobInstance, HybridPeripheralJobExecHeader_t* HybridExecHeader, uint8_t* pHybridExecData)
{
    uint8_t*	pExecutionItem = NULL;
    
    if (JobInstance->pExecution)
    {
        //Alloc large enought to fit previously added execution and current new added one
        pExecutionItem =  DK_JOB_ALLOC("ReAllocJobExecItem", 1, JobInstance->ExecutionLen +sizeof(HybridPeripheralJobExecHeader_t) + HybridExecHeader->DataLen);
        
        
        //Copy the previously added execution to the newly allocated buffer
        memcpy(pExecutionItem, JobInstance->pExecution, JobInstance->ExecutionLen);
        
        //Free previous one
        free(JobInstance->pExecution);
        JobInstance->pExecution = NULL;
    }
    else
    {
        pExecutionItem = DK_JOB_ALLOC("JobExecItem", 1, sizeof(HybridPeripheralJobExecHeader_t) + HybridExecHeader->DataLen);
    }
    
    memcpy(pExecutionItem + JobInstance->ExecutionLen, HybridExecHeader, sizeof(HybridPeripheralJobExecHeader_t));
    JobInstance->ExecutionLen += sizeof(HybridPeripheralJobExecHeader_t);
    if (HybridExecHeader->DataLen)
    {
        memcpy(pExecutionItem + JobInstance->ExecutionLen, pHybridExecData, HybridExecHeader->DataLen);
        JobInstance->ExecutionLen += HybridExecHeader->DataLen;
    }
    
    JobInstance->pExecution = pExecutionItem;
    
    JobInstance->Header.TotalJobExecution++;
}


void LcJobPackageConditionAdd_CommonSetup(JobInstance_t* JobInstance, JobConditionHeader_t* pHeader, JobConditionPayloadHeader_t* pPayloadHeader, uint8_t* pPayloadData)
{
    
    uint8_t*	pConditionItem = NULL;
    
    if (JobInstance->pCondition)
    {
        //Alloc large enought to fit previously added execution and current new added one
        pConditionItem =  DK_JOB_ALLOC("ReAllocJobConditionItem", 1, JobInstance->ConditionLen + sizeof(JobConditionHeader_t) + pHeader->PayloadLen );
        
        
        //Copy the previously added execution to the newly allocated buffer
        memcpy(pConditionItem, JobInstance->pCondition, JobInstance->ConditionLen);
        
        //Free previous one
        free(JobInstance->pCondition);
        JobInstance->pCondition = NULL;
    }
    else
    {
        pConditionItem = DK_JOB_ALLOC("JobConditionItem", 1, sizeof(JobConditionHeader_t) + pHeader->PayloadLen);
    }
    
    memcpy(pConditionItem + JobInstance->ConditionLen, pHeader, sizeof(JobConditionHeader_t));
    JobInstance->ConditionLen += sizeof(JobConditionHeader_t);
    
    memcpy(pConditionItem + JobInstance->ConditionLen, pPayloadHeader, sizeof(JobConditionPayloadHeader_t));
    JobInstance->ConditionLen += sizeof(JobConditionPayloadHeader_t);
    
    if (pPayloadData && UtilsGetValueTypeSize(pPayloadHeader->ValueType))
    {
        
        memcpy(pConditionItem + JobInstance->ConditionLen, pPayloadData, UtilsGetValueTypeSize(pPayloadHeader->ValueType));
        JobInstance->ConditionLen += UtilsGetValueTypeSize(pPayloadHeader->ValueType);
    }
    
    JobInstance->pCondition = pConditionItem;
    
    JobInstance->Header.TotalJobCondition++;
    
}

void LcJobPackageConditionAdd_EnhanceSetup(JobInstance_t* JobInstance, JobConditionEnhancedHeader_t* pEnhanceHeader, uint8_t* pEnhanceData, JobConditionPayloadHeader_t* pPayloadHeader, uint8_t* pPayloadData)
{
    uint8_t*	pConditionItem = NULL;
    
    if (JobInstance->pCondition)
    {
        //Alloc large enought to fit previously added execution and current new added one
        pConditionItem =  DK_JOB_ALLOC("ReAllocJobConditionItem", 1, JobInstance->ConditionLen +
                                       sizeof(JobConditionEnhancedHeader_t) + pEnhanceHeader->EnhancedCommandDataLen +
                                       pEnhanceHeader->CommandConditionPayloadLen);
        
        
        //Copy the previously added execution to the newly allocated buffer
        memcpy(pConditionItem, JobInstance->pCondition, JobInstance->ConditionLen);
        
        //Free previous one
        free(JobInstance->pCondition);
        JobInstance->pCondition = NULL;
    }
    else
    {
        pConditionItem = DK_JOB_ALLOC("JobConditionItem", 1, sizeof(JobConditionEnhancedHeader_t) + pEnhanceHeader->EnhancedCommandDataLen +
                                      pEnhanceHeader->CommandConditionPayloadLen);
    }
    
    //Copy the Enhance command header
    memcpy(pConditionItem + JobInstance->ConditionLen, pEnhanceHeader, sizeof(JobConditionEnhancedHeader_t));
    JobInstance->ConditionLen += sizeof(JobConditionEnhancedHeader_t);

    //Copy the Enhance command data
    memcpy(pConditionItem + JobInstance->ConditionLen, pEnhanceData, pEnhanceHeader->EnhancedCommandDataLen);
    JobInstance->ConditionLen += pEnhanceHeader->EnhancedCommandDataLen;
    
    //Copy the Enhance command condition header
    memcpy(pConditionItem + JobInstance->ConditionLen, pPayloadHeader, sizeof(JobConditionPayloadHeader_t));
    JobInstance->ConditionLen += sizeof(JobConditionPayloadHeader_t);

    //Copy the Enhance command condition value
    if (pPayloadData && UtilsGetValueTypeSize(pPayloadHeader->ValueType))
    {
        
        memcpy(pConditionItem + JobInstance->ConditionLen, pPayloadData, UtilsGetValueTypeSize(pPayloadHeader->ValueType));
        JobInstance->ConditionLen += UtilsGetValueTypeSize(pPayloadHeader->ValueType);
    }
    
    JobInstance->pCondition = pConditionItem;
    
    JobInstance->Header.TotalJobCondition++;

}


void LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance_t* JobInstance, DeviceCommandJobHeader_t* StdExecHeader, uint8_t* pStdExecData)
{
    uint8_t*	pExecutionItem = NULL;
    
    if (JobInstance->pExecution)
    {
        //Alloc large enought to fit previously added execution and current new added one
        pExecutionItem =  DK_JOB_ALLOC("ReAllocJobExecItem", 1, JobInstance->ExecutionLen +sizeof(DeviceCommandJobHeader_t)+StdExecHeader->PayloadLen);
        
        
        //Copy the previously added execution to the newly allocated buffer
        memcpy(pExecutionItem, JobInstance->pExecution, JobInstance->ExecutionLen);
        
        //Free previous one
        free(JobInstance->pExecution);
        JobInstance->pExecution = NULL;
    }
    else
    {
        pExecutionItem = DK_JOB_ALLOC("JobExecItem", 1, sizeof(DeviceCommandJobHeader_t)+StdExecHeader->PayloadLen);
    }
    
    memcpy(pExecutionItem + JobInstance->ExecutionLen, StdExecHeader, sizeof(DeviceCommandJobHeader_t));
    JobInstance->ExecutionLen += sizeof(DeviceCommandJobHeader_t);
    if (StdExecHeader->PayloadLen)
    {
        memcpy(pExecutionItem + JobInstance->ExecutionLen, pStdExecData, StdExecHeader->PayloadLen);
        JobInstance->ExecutionLen += StdExecHeader->PayloadLen;
    }
    
    JobInstance->pExecution = pExecutionItem;
    
    JobInstance->Header.TotalJobExecution++;
}



bool LcAdvJobPackageExecutionAdd_PeripheralAccessWriteJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ReadWriteMode, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData)
{
	bool Success = false;

    if (JobInstance && DevAddr)
    {
    	//Make sure there is NO execution since in advance job package we restrict to create either condition or execution but not mixed.
    	//Also we do a simple check if this execution package is set to scheduled ONCE (other not allowed for advance job execution)
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
        	(JobInstance->ConditionLen == 0) &&
        	(JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0))
        {
            BleStandardJobExecHeader_t StdExecHeader;
            memcpy(StdExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
            StdExecHeader.CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
            StdExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = ReadWriteMode;
            StdExecHeader.ServiceUUID = ServiceUUID;
            StdExecHeader.CharacteristicUUID = CharacteristicUUID;
            StdExecHeader.DataLen = (pData) ? DataLen:0;

            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_STANDARD_BLE;

            LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance, &StdExecHeader, pData);

            Success = true;
        }
    }

    return Success;
}

JobExecutionHybrid_t LcParseHybridJobExecutionPackageByExecutionIndex(uint8_t ExecutionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen)
{
    JobExecutionHybrid_t ParsedJobExecutionHybrid;
    
    memset(&ParsedJobExecutionHybrid, 0, sizeof(JobExecutionHybrid_t));
    ParsedJobExecutionHybrid.ExecutionType = 0xFF; //Set to an invalid one
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((ExecutionIndex < JobHeader->TotalJobExecution) && (JobPayloadDataLen > sizeof(JobHeader_t)))
            {
                if (JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_HYBRIDPERIPHERAL)
                {
                    ParsedJobExecutionHybrid.ExecutionType = JOB_TYPE_HYBRIDPERIPHERAL;
                    
                    uint8_t*  pJobExecutionHybrid = GetJobExecutionStartPointer(JobPayloadIn);
                    
                    //Offset to the requested one
                    uint8_t i = 0;
                    for (i = 0; i < ExecutionIndex; i++)
                    {
                        HybridPeripheralJobExecHeader_t* JobExecutionHybridHeader = (HybridPeripheralJobExecHeader_t*)pJobExecutionHybrid;
                        pJobExecutionHybrid += (sizeof(HybridPeripheralJobExecHeader_t) + JobExecutionHybridHeader->DataLen);
                    }
                    
                    HybridPeripheralJobExecHeader_t* JobExecutionHybridHeader = (HybridPeripheralJobExecHeader_t*)pJobExecutionHybrid;
                    
                    //Parse the MacAddress
                    memcpy((uint8_t*)&ParsedJobExecutionHybrid.PeripheralDeviceMacAddress, (uint8_t*)&JobExecutionHybridHeader->PeripheralDeviceMacAddress, sizeof(DevAddress_t));
                    
                    memcpy((uint8_t*)ParsedJobExecutionHybrid.ServiceUUID, (uint8_t*)JobExecutionHybridHeader->ServiceUUID, sizeof(JobExecutionHybridHeader->ServiceUUID));
                    
                    memcpy((uint8_t*)ParsedJobExecutionHybrid.CharacteristicUUID, (uint8_t*)JobExecutionHybridHeader->CharacteristicUUID, sizeof(JobExecutionHybridHeader->CharacteristicUUID));
                    
                    ParsedJobExecutionHybrid.DataLen = JobExecutionHybridHeader->DataLen;
                    
                    uint8_t* pData = (pJobExecutionHybrid + sizeof(HybridPeripheralJobExecHeader_t));
                    
                    ParsedJobExecutionHybrid.pData = pData;
                }
            }
        }
    }
    
    return ParsedJobExecutionHybrid;
}

/******************************************************
 *               Function Definitions
 ******************************************************/

extern bool CreateAdvanceJobPackage(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, uint8_t ThresholdReExecutionUnit)
{
	bool Success = false;
    if (JobInstance && GatewayMAC)
    {
        memset((void*)JobInstance, 0 , sizeof(JobInstance_t));

        JobInstance->InstanceID = JOB_INSTANCE_VALID_ID;
        JobInstance->Header.Preamble = JOB_HEADER_PREAMBLE;
        JobInstance->Header.Reserve1 = 0;
        memcpy(&JobInstance->Header.DeviceMacAddress, GatewayMAC, sizeof(WifiCommDeviceMac_t));
        JobInstance->Header.ReExecutionThreshold = ThresholdReExecutionUnit;

        JobInstance->ConditionLen = 0;
        JobInstance->pCondition = NULL;
        JobInstance->ExecutionLen = 0;
        JobInstance->pExecution = NULL;

        JobInstance->Header.JobType.bits.JobConditionOr_b7 = CONDITION_TYPE_AND;
        JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_NONE;        
        JobInstance->Header.JobType.bits.JobConditionFirstRun_b5 = CONDITION_FIRST_SKIP;
        JobInstance->Header.JobType.bits.JobBleWriteOnce_b4 = BLE_WRITE_RETRY_10S;
        Success = true;
    }

    return Success;
}

extern bool CreateAdvanceJobPackageOrCondition(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, uint8_t ThresholdReExecutionUnit, uint8_t Flags)
{
	bool Success = false;
    if (JobInstance && GatewayMAC)
    {
        memset((void*)JobInstance, 0 , sizeof(JobInstance_t));

        JobInstance->InstanceID = JOB_INSTANCE_VALID_ID;
        JobInstance->Header.Preamble = JOB_HEADER_PREAMBLE;
        JobInstance->Header.Reserve1 = 0;
        memcpy(&JobInstance->Header.DeviceMacAddress, GatewayMAC, sizeof(WifiCommDeviceMac_t));
        JobInstance->Header.ReExecutionThreshold = ThresholdReExecutionUnit;

        JobInstance->ConditionLen = 0;
        JobInstance->pCondition = NULL;
        JobInstance->ExecutionLen = 0;
        JobInstance->pExecution = NULL;
        
        JobInstance->Header.JobType.bits.JobConditionOr_b7 = CONDITION_TYPE_OR;
        if(Flags & 0x02)
        {
            JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_ALL;
        }
        else
        {
            JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_NONE;
        }
        JobInstance->Header.JobType.bits.JobConditionFirstRun_b5 = CONDITION_FIRST_SKIP;
        Success = true;
    }

    return Success;
}

extern bool FreeAdvanceJobPackage(JobInstance_t* JobInstance)
{
	bool Success = false;
    if (JobInstance)
    {
        JobInstance->InstanceID = 0;
        memset(&JobInstance->Header.DeviceMacAddress, 0, sizeof(WifiCommDeviceMac_t));

        JobInstance->ConditionLen = 0;
        if (JobInstance->pCondition)
        {
            free(JobInstance->pCondition);
            JobInstance->pCondition = NULL;
        }

        JobInstance->ExecutionLen = 0;
        if (JobInstance->pExecution)
        {
            free(JobInstance->pExecution);
            JobInstance->pExecution = NULL;
        }

        if (JobInstance->pPayloadPack)
        {
            free(JobInstance->pPayloadPack);
            JobInstance->pPayloadPack = NULL;
        }

    	Success = true;
    }

    return Success;
}


extern uint8_t* AdvanceJobPackageGeneratePayloadPack(JobInstance_t* JobInstance, uint16_t* ReturnedPayloadLen)
{
    uint8_t* pPayload = NULL;
    uint8_t* pPayloadIndex = NULL;

    if (JobInstance && ReturnedPayloadLen)
    {
        *ReturnedPayloadLen = 0;
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Free First - Should be NULL but we can still continue.
            if (JobInstance->pPayloadPack)
            {
                free(JobInstance->pPayloadPack);
                JobInstance->pPayloadPack=NULL;
            }

            *ReturnedPayloadLen += sizeof(JobHeader_t) + JobInstance->ConditionLen + JobInstance->ExecutionLen;
            JobInstance->pPayloadPack = DK_JOB_ALLOC("JobPayloadPack", 1, *ReturnedPayloadLen);
            pPayloadIndex = JobInstance->pPayloadPack;

            memcpy(pPayloadIndex, &JobInstance->Header, sizeof(JobHeader_t)); pPayloadIndex += sizeof(JobHeader_t);
            if (JobInstance->ConditionLen)
            {
                memcpy(pPayloadIndex, JobInstance->pCondition, JobInstance->ConditionLen); pPayloadIndex += JobInstance->ConditionLen;
            }
            if (JobInstance->ExecutionLen)
            {
                memcpy(pPayloadIndex, JobInstance->pExecution, JobInstance->ExecutionLen); pPayloadIndex += JobInstance->ExecutionLen;
            }

            pPayload = JobInstance->pPayloadPack;

        }
    }

    return pPayload;
}


extern bool AdvJobPackageHeader_RangedTimeAndDayRepeatableJobSetup(JobInstance_t* JobInstance, DaysOfWeek_t DaysSelection, SchTime_t InUTC0RangeStartTime, SchTime_t InUTC0RangeEndTime, float TimeZone)
{
	bool Success = false;
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Set the day schedule
            JobInstance->Header.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
            JobInstance->Header.ScheduleFlag.bits.ScheduleStartTimeOption = 1; //Time range
            JobInstance->Header.ScheduleFlag.bits.ShceduleMon_b1 = DaysSelection.Monday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleTue_b2 = DaysSelection.Tuesday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleWed_b3 = DaysSelection.Wednesday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleThu_b4 = DaysSelection.Thursday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleFri_b5 = DaysSelection.Friday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleSat_b6 = DaysSelection.Saturday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleSun_b7 = DaysSelection.Sunday;

            //Set the Execution time
            JobInstance->Header.ScheduleStartTime.bits.SchTimeRangeStartHour_b24_31 = InUTC0RangeStartTime.Hour24;
            JobInstance->Header.ScheduleStartTime.bits.SchTimeRangeStartMinute_b16_23 = InUTC0RangeStartTime.Minute;
            JobInstance->Header.ScheduleStartTime.bits.SchTimeRangeEndHour_b8_15 = InUTC0RangeEndTime.Hour24;
            JobInstance->Header.ScheduleStartTime.bits.SchTimeRangeEndMinute_b0_7 = InUTC0RangeEndTime.Minute;

            if (TimeZone)
            {
            	float Integerpart = 0;


            	if (TimeZone < 0.0f)
            	{
            		JobInstance->Header.TimeZone.bits.Sign_b7 = 1;
            	}

                float FractionalPart = modff(fabsf(TimeZone), &Integerpart);

            	JobInstance->Header.TimeZone.bits.Integral_b0_3 = 0x0F & (uint8_t)Integerpart;

            	if (FractionalPart == 0.50)
            	{
            		JobInstance->Header.TimeZone.bits.FractionalMode_b4_5 = TIMEZONE_FRAC_30MIN;
            	}
            	else if (FractionalPart == 0.75)
            	{
            		JobInstance->Header.TimeZone.bits.FractionalMode_b4_5 = TIMEZONE_FRAC_45MIN;
            	}
            }

            Success = true;
        }
    }

    return Success;
}



extern bool AdvJobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue)
{
	bool Success = false;

    if (JobInstance && pValue)
    {
    	//Make sure there is NO execution since in advance job package we restrict to create either condition or execution but not mixed.
    	//Also we do a simple check if this execution package is NOT set to scheduled ONCE (Should be repeatable with range but we are not going to double check that here again)
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
        	(JobInstance->ExecutionLen == 0) &&
        	(!JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0))
        {

            //Protection as we don't allow 2 different types (BLE & Enhanced) of condition on same conditional job
            if ((JobInstance->Header.TotalJobCondition)  && (JobInstance->Header.JobType.bits.JobConditionType_b8_11 != JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL))
            {
                return false;
            }
            
            //Mark it as BLE Peripheral Adv condition TYPE
            JobInstance->Header.JobType.bits.JobConditionType_b8_11 = JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL;
            
            /* Condition */
            JobConditionHeader_t ConditionHeader;
            memcpy(ConditionHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddr->Addr));
            ConditionHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + UtilsGetValueTypeSize(TypeValue);

            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.bits.ConditionField_b0_7 = ConditionField;
            ConditionPayloadHeader.ConditionType.bits.ConditionReached_b8 = 0; //Must be set to 0
            ConditionPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = STANDARD_CHECK_STDBLE_COND_TYPE_FLAG;
            ConditionPayloadHeader.Operator = Operator;
            ConditionPayloadHeader.ValueType = TypeValue;

            LcJobPackageConditionAdd_CommonSetup(JobInstance, &ConditionHeader, &ConditionPayloadHeader, pValue);

            Success = true;
        }
    }

    return Success;
}


bool AdvJobPackageConditionAddEnh_GeofencingConditionJobSetup(JobInstance_t* JobInstance, GeoLocation_t CenterLoc, enum eConditionOperator Operator, uint32_t RadiusInMeters)
{
   	bool Success = false;

    if (JobInstance)
    {
        //Make sure there is NO execution since in advance job package we restrict to create either condition or execution but not mixed.
        //Also we do a simple check if this execution package is NOT set to scheduled ONCE (Should be repeatable with range but we are not going to double check that here again)
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            (JobInstance->ExecutionLen == 0) &&
            (!JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0))
        {

            //Protection as we don't allow 2 different types (BLE & Enhanced) of condition on same conditional job
            if ((JobInstance->Header.TotalJobCondition)  && (JobInstance->Header.JobType.bits.JobConditionType_b8_11 != JOB_CONDITION_TYPE_ENHANCED_COMMAND))
            {
                return false;
            }
            
            //Mark it as Enhanced Command Type
            JobInstance->Header.JobType.bits.JobConditionType_b8_11 = JOB_CONDITION_TYPE_ENHANCED_COMMAND;
            
            JobConditionEnhancedHeader_t EnhanceCommandHeader;
            EnhanceCommandHeader.EnhancedCommandID = CONDITION_ENHANCE_CMD_ID_GEOFENCING;
            EnhanceCommandHeader.EnhancedCommandDataLen = 8; //Geofencing is 8 (Latt & Long with 4 byte each)
            EnhanceCommandHeader.CommandConditionPayloadLen = 8; //Usually 8 if it contain data, otherwise its 0
            
            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.Type = 0; //Reserve for Enhance condition payload command
            ConditionPayloadHeader.Operator = Operator;
            ConditionPayloadHeader.ValueType = VALUE_TYPE_UINT32; //Geofencing radius in meter is 4 byte uint32
            
            
            LcJobPackageConditionAdd_EnhanceSetup(JobInstance, &EnhanceCommandHeader, (uint8_t*)&CenterLoc, &ConditionPayloadHeader, (uint8_t*)&RadiusInMeters);
            
            Success = true;
            

        }
    }
    
    return Success;
}


extern bool AdvJobPackageHeader_OneTimeJobSetup(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
        	(JobInstance->ConditionLen == 0))
        {
            JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0 = 1;

            Success = true;
        }
    }

    return Success;

}

extern bool AdvJobPackageHeader_SetOrCondition(JobInstance_t* JobInstance, uint16_t Reserve)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionOr_b7 = CONDITION_TYPE_OR;
        Success = true;
    }
    return Success;
}

extern bool AdvJobPackageHeader_SetResetAllCondition(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_ALL;
        Success = true;
    }
    return Success;
}

extern bool AdvJobPackageHeader_SetFirstRunCondition(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionFirstRun_b5 = CONDITION_FIRST_RUN;
        Success = true;
    }
    return Success;
}

extern bool AdvJobPackageHeader_SetBLEWriteOnce(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobBleWriteOnce_b4 = BLE_WRITE_RETRY_SHORT;
        Success = true;
    }
    return Success;
}

extern bool AdvJobPackageHeader_SetJobSuperior(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobSuperior_b3 = JOB_SUPERIOR_ENABLE;
        Success = true;
    }
    return Success;
}

extern bool AdvJobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData)
{
    return LcAdvJobPackageExecutionAdd_PeripheralAccessWriteJobSetup(JobInstance, DevAddr, WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ, ServiceUUID, CharacteristicUUID, DataLen, pData);
}

extern bool AdvJobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData)
{
    return LcAdvJobPackageExecutionAdd_PeripheralAccessWriteJobSetup(JobInstance, DevAddr, WRITE_ONLY, ServiceUUID, CharacteristicUUID, DataLen, pData);
}

extern bool AdvJobPackageExecutionAdd_HybridPeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData)
{
	bool Success = false;

    if (JobInstance && DevAddr)
    {
    	//Make sure there is NO execution since in advance job package we restrict to create either condition or execution but not mixed.
    	//Also we do a simple check if this execution package is set to scheduled ONCE (other not allowed for advance job execution)
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
        	(JobInstance->ConditionLen == 0) &&
        	(JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0))
        {
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {            
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = (pData) ? DataLen:0;
                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;

                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, pData);                        

                Success = true;
            }
        }
    }

    return Success;
}


extern bool AdvJobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData)
{
	bool Success = false;

    if (JobInstance && DevAddr)
    {
    	//Make sure there is NO execution since in advance job package we restrict to create either condition or execution but not mixed.
    	//Also we do a simple check if this execution package is set to scheduled ONCE (other not allowed for advance job execution)
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
        	(JobInstance->ConditionLen == 0) &&
        	(JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0))
        {
            
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {                        
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));                
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_ONLY;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = (pData) ? DataLen:0;
                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;

                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, pData);                        

                Success = true;
            }
        }
    }

    return Success;
}


extern bool	ParseAdvJobPackage_JobSuperior(uint8_t* JobPayloadIn)
{
    bool bSuperior = false;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->JobType.bits.JobSuperior_b3 == JOB_SUPERIOR_ENABLE)
        {
            bSuperior = true;
        }
    }
    
    return bSuperior;
}


extern JobConditionTimeRange_t	ParseAdvJobPackage_ConditionTimeRange(uint8_t* JobPayloadIn)
{
    JobConditionTimeRange_t TimeRange;
    
    TimeRange.IsValid = false;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((JobHeader->ScheduleFlag.bits.ShceduleRepeatable_b15) &&
                (JobHeader->ScheduleFlag.bits.ScheduleStartTimeOption == 1))
            {
                TimeRange.IsValid = true;
                TimeRange.DaysSelection.Monday = JobHeader->ScheduleFlag.bits.ShceduleMon_b1;
                TimeRange.DaysSelection.Tuesday = JobHeader->ScheduleFlag.bits.ShceduleTue_b2;
                TimeRange.DaysSelection.Wednesday = JobHeader->ScheduleFlag.bits.ShceduleWed_b3;
                TimeRange.DaysSelection.Thursday = JobHeader->ScheduleFlag.bits.ShceduleThu_b4;
                TimeRange.DaysSelection.Friday = JobHeader->ScheduleFlag.bits.ShceduleFri_b5;
                TimeRange.DaysSelection.Saturday = JobHeader->ScheduleFlag.bits.ShceduleSat_b6;
                TimeRange.DaysSelection.Sunday = JobHeader->ScheduleFlag.bits.ShceduleSun_b7;
                
                TimeRange.UTC0RangeStartTime.Hour24 = JobHeader->ScheduleStartTime.bits.SchTimeRangeStartHour_b24_31;
                TimeRange.UTC0RangeStartTime.Minute = JobHeader->ScheduleStartTime.bits.SchTimeRangeStartMinute_b16_23;
                
                TimeRange.UTC0RangeEndTime.Hour24 = JobHeader->ScheduleStartTime.bits.SchTimeRangeEndHour_b8_15;
                TimeRange.UTC0RangeEndTime.Minute = JobHeader->ScheduleStartTime.bits.SchTimeRangeEndMinute_b0_7;

                if (JobHeader->TimeZone.value)
                {
                	TimeRange.TimeZone = (float)JobHeader->TimeZone.bits.Integral_b0_3;
                	if (JobHeader->TimeZone.bits.FractionalMode_b4_5 == TIMEZONE_FRAC_30MIN)
                	{
                		TimeRange.TimeZone += 0.50;
                	}
                	else if (JobHeader->TimeZone.bits.FractionalMode_b4_5 == TIMEZONE_FRAC_45MIN)
                	{
                		TimeRange.TimeZone += 0.75;
                	}

                	if (JobHeader->TimeZone.bits.Sign_b7)
                	{
                		TimeRange.TimeZone = -TimeRange.TimeZone;
                	}
                }
            }
        }
    }
    
    return TimeRange;

}


extern uint8_t	ParseAdvJobPackage_TotalAvailableCondition(uint8_t* JobConditionPayloadIn)
{
    uint8_t TotalCondition = 0;
    
    if (JobConditionPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobConditionPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            TotalCondition = JobHeader->TotalJobCondition;
        }
    }
    
    return TotalCondition;
}


extern JobCondition_t	ParseAdvJobPackage_ConditionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ConditionIndex)
{
    JobCondition_t ParsedJobCondition;
    
    memset(&ParsedJobCondition, 0, sizeof(JobCondition_t));
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((ConditionIndex < JobHeader->TotalJobCondition) && (JobPayloadDataLen > sizeof(JobHeader_t)))
            {
                
                if (JobHeader->JobType.bits.JobConditionType_b8_11 == JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL)
                {
                    ParsedJobCondition.ConditionType = JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL;
                    ParsedJobCondition.TypeAdvData.Valid = true;
                    ParsedJobCondition.TypeEnhanceData.Valid = false;
                    
                    uint8_t*  pJobCondition =  GetJobConditionStartPointer(JobPayloadIn);
                    
                    //Offset to the requested one
                    uint8_t i = 0;
                    for (i = 0; i < ConditionIndex; i++)
                    {
                        JobConditionHeader_t* pJobConditionHeader = (JobConditionHeader_t*)pJobCondition;
                        
                        pJobCondition += (sizeof(JobConditionHeader_t) + pJobConditionHeader->PayloadLen);
                    }
                    
                    JobConditionHeader_t* pJobConditionHeader = (JobConditionHeader_t*)pJobCondition;
                    
                    //Parse the MacAddress
                    memcpy((uint8_t*)&ParsedJobCondition.TypeAdvData.PeripheralDeviceMacAddress, (uint8_t*)&pJobConditionHeader->PeripheralDeviceMacAddress, sizeof(DevAddress_t));
                    
                    JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionHeader_t));
                    
                    if (pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 == STANDARD_CHECK_STDBLE_COND_TYPE_FLAG)
                    {

                        ParsedJobCondition.TypeAdvData.DataType = pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7;
                        ParsedJobCondition.TypeAdvData.Operator = pJobConditionPayloadHeader->Operator;
                        ParsedJobCondition.TypeAdvData.ValueType = pJobConditionPayloadHeader->ValueType;
                        
                        //Get the value of the DataType
                        uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7);
                        uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);
                        if ((ValueSizeInByte) && (ValueSizeInByte == DataLen))
                        {
                            uint8_t* pConditionValue = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                            
                            memcpy(&ParsedJobCondition.TypeAdvData.Value, pConditionValue, ValueSizeInByte);
                        }
        
                    }
                }
                else if (JobHeader->JobType.bits.JobConditionType_b8_11 == JOB_CONDITION_TYPE_ENHANCED_COMMAND)
                {
                    ParsedJobCondition.ConditionType = JOB_CONDITION_TYPE_ENHANCED_COMMAND;
                    ParsedJobCondition.TypeAdvData.Valid = false;
                    ParsedJobCondition.TypeEnhanceData.Valid = true;
                    
                    uint8_t*  pJobCondition =  GetJobConditionStartPointer(JobPayloadIn);
                    
                    //Offset to the requested one
                    uint8_t i = 0;
                    for (i = 0; i < ConditionIndex; i++)
                    {
                        JobConditionEnhancedHeader_t* pJobConditionEnhancedHeader = (JobConditionEnhancedHeader_t*)pJobCondition;
                        
                        pJobCondition += (sizeof(JobConditionEnhancedHeader_t) +
                                          pJobConditionEnhancedHeader->EnhancedCommandDataLen +
                                          pJobConditionEnhancedHeader->CommandConditionPayloadLen);
                    }
                    
                    JobConditionEnhancedHeader_t* pJobConditionEnhancedHeader = (JobConditionEnhancedHeader_t*)pJobCondition;                    
                    
                    ParsedJobCondition.TypeEnhanceData.EnhanceCommandID = pJobConditionEnhancedHeader->EnhancedCommandID;
                    ParsedJobCondition.TypeEnhanceData.EnhanceCommandDataLen = pJobConditionEnhancedHeader->EnhancedCommandDataLen;
                    ParsedJobCondition.TypeEnhanceData.pEnhanceCommData = pJobCondition + sizeof(JobConditionEnhancedHeader_t);
                    
                    if (pJobConditionEnhancedHeader->CommandConditionPayloadLen > sizeof(JobConditionPayloadHeader_t))
                    {
                        JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionEnhancedHeader_t) + pJobConditionEnhancedHeader->EnhancedCommandDataLen);
                        
                        ParsedJobCondition.TypeEnhanceData.Operator = pJobConditionPayloadHeader->Operator;
                        ParsedJobCondition.TypeEnhanceData.ValueType = pJobConditionPayloadHeader->ValueType;
                        //Get the value of the DataType
                        uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);
                        if (ValueSizeInByte)
                        {
                            uint8_t* pConditionValue =  pJobCondition + sizeof(JobConditionEnhancedHeader_t) +
                                                        pJobConditionEnhancedHeader->EnhancedCommandDataLen + sizeof(JobConditionPayloadHeader_t);
                            
                            memcpy(&ParsedJobCondition.TypeEnhanceData.Value, pConditionValue, ValueSizeInByte);
                        }

                    }
                    
                }
                
                // AND or OR condition
                if(JobHeader->JobType.bits.JobConditionOr_b7 == 1)
                {
                    ParsedJobCondition.ConditionTypeFlags.FlagOperate= CONDITION_TYPE_OR;
                }
                else
                {
                    ParsedJobCondition.ConditionTypeFlags.FlagOperate= CONDITION_TYPE_AND;
                }
                
                // OR trigger need reset all condition or any new event will re-trigger
                if(JobHeader->JobType.bits.JobConditionRst_b6 == 1)
                {
                    ParsedJobCondition.ConditionTypeFlags.FlagOperate= CONDITION_RST_ALL;
                }
                else
                {
                    ParsedJobCondition.ConditionTypeFlags.FlagOperate= CONDITION_RST_NONE;
                }
                
            }
        }
    }

    
    return ParsedJobCondition;
}

extern bool  AuxParseAdvJob_EnhanceComm_GeoFencingLoc(JobCondition_t* pInJobConditionData, GeoLocation_t* pOutGeoLocation)
{
    bool Success = false;
    
    if (pInJobConditionData && pOutGeoLocation)
    {
        if (pInJobConditionData->ConditionType == JOB_CONDITION_TYPE_ENHANCED_COMMAND)
        {
            if ((pInJobConditionData->TypeEnhanceData.EnhanceCommandID == CONDITION_ENHANCE_CMD_ID_GEOFENCING) &&
                (pInJobConditionData->TypeEnhanceData.EnhanceCommandDataLen == sizeof(GeoLocation_t)))
            {
                memcpy(pOutGeoLocation, pInJobConditionData->TypeEnhanceData.pEnhanceCommData, sizeof(GeoLocation_t));
                Success = true;
            }
        }
    }
    
    return Success;
}

extern uint8_t	ParseAdvJobPackage_TotalAvailableExecution(uint8_t* JobExecutionPayloadIn)
{
    uint8_t TotalExecution = 0;
    
    if (JobExecutionPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobExecutionPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            TotalExecution = JobHeader->TotalJobExecution;
        }
    }

    return TotalExecution;
    
}

extern JobType_t    ParseAdvJobPackage_GetExecutionjobType(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex)
{
    JobType_t JobType = JOB_TYPE_INVALID;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((ExecutionIndex < JobHeader->TotalJobExecution) && (JobPayloadDataLen > sizeof(JobHeader_t)))
            {
                JobType = JobHeader->JobType.bits.JobExecutionType_b12_15;
            }            
        }
    }
    
    return JobType;
}

extern JobExecution_t	ParseAdvJobPackage_ExecutionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex)
{
    JobExecution_t ParsedJobExecution;
    
    memset(&ParsedJobExecution, 0, sizeof(JobExecution_t));
    ParsedJobExecution.ExecutionType = 0xFF; //Set to an invalid one

    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((ExecutionIndex < JobHeader->TotalJobExecution) && (JobPayloadDataLen > sizeof(JobHeader_t)))
            {
                if (JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_STANDARD_BLE)
                {
                    ParsedJobExecution.ExecutionType = JOB_TYPE_STANDARD_BLE;
                    
                    uint8_t*  pJobExecution = GetJobExecutionStartPointer(JobPayloadIn);
                    
                    //Offset to the requested one
                    uint8_t i = 0;
                    for (i = 0; i < ExecutionIndex; i++)
                    {
                        BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;
                        pJobExecution += (sizeof(BleStandardJobExecHeader_t) + JobExecutionHeader->DataLen);
                    }
                    
                    BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;
                    
                    //Parse the MacAddress
                    memcpy((uint8_t*)&ParsedJobExecution.PeripheralDeviceMacAddress, (uint8_t*)&JobExecutionHeader->PeripheralDeviceMacAddress, sizeof(DevAddress_t));
                    
                    ParsedJobExecution.ServiceUUID = JobExecutionHeader->ServiceUUID;
                    ParsedJobExecution.CharacteristicUUID = JobExecutionHeader->CharacteristicUUID;
                    ParsedJobExecution.DataLen = (JobExecutionHeader->DataLen > sizeof(ParsedJobExecution.Data)) ? sizeof(ParsedJobExecution.Data):JobExecutionHeader->DataLen;
                    
                    uint8_t* pData = (pJobExecution + sizeof(BleStandardJobExecHeader_t));
                    memcpy((uint8_t*)ParsedJobExecution.Data, pData, ParsedJobExecution.DataLen);
                }
            }
        }
    }

    return ParsedJobExecution;
}


extern JobExecutionHybrid_t ParseAdvJobPackage_HybridExecutionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex)
{
    JobExecutionHybrid_t ParsedJobExecutionHybrid = LcParseHybridJobExecutionPackageByExecutionIndex(ExecutionIndex, JobPayloadIn, JobPayloadDataLen);

    return ParsedJobExecutionHybrid;
}





extern void CreateJobPackage(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, struct ScheduleSequenceNumber_s* ScheduleSEQNo, uint8_t ThresholdReExecutionUnit)
{
    if (JobInstance && GatewayMAC && ScheduleSEQNo)
    {
        memset((void*)JobInstance, 0 , sizeof(JobInstance_t));
        
        JobInstance->InstanceID = JOB_INSTANCE_VALID_ID;
        JobInstance->Header.Preamble = JOB_HEADER_PREAMBLE;
        JobInstance->Header.Reserve1 = 0;
        memcpy(&JobInstance->Header.DeviceMacAddress, GatewayMAC, sizeof(WifiCommDeviceMac_t));
        memcpy(&JobInstance->Header.ScheduleSequenceNumber.Component, ScheduleSEQNo, sizeof(struct ScheduleSequenceNumber_s));
        JobInstance->Header.ReExecutionThreshold = ThresholdReExecutionUnit;

        JobInstance->Header.JobType.bits.JobConditionOr_b7 = CONDITION_TYPE_AND;
        JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_NONE;        
        JobInstance->Header.JobType.bits.JobConditionFirstRun_b5 = CONDITION_FIRST_SKIP;
        JobInstance->Header.JobType.bits.JobBleWriteOnce_b4 = BLE_WRITE_RETRY_10S;
        
        JobInstance->ConditionLen = 0;
        JobInstance->pCondition = NULL;
        JobInstance->ExecutionLen = 0;
        JobInstance->pExecution = NULL;
    }
}

extern void FreeJobPackage(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        JobInstance->InstanceID = 0;
        memset(&JobInstance->Header.DeviceMacAddress, 0, sizeof(WifiCommDeviceMac_t));
        
        JobInstance->ConditionLen = 0;
        if (JobInstance->pCondition)
        {
            free(JobInstance->pCondition);
            JobInstance->pCondition = NULL;
        }
        
        JobInstance->ExecutionLen = 0;
        if (JobInstance->pExecution)
        {
            free(JobInstance->pExecution);
            JobInstance->pExecution = NULL;
        }
        
        if (JobInstance->pPayloadPack)
        {
            free(JobInstance->pPayloadPack);
            JobInstance->pPayloadPack = NULL;
        }
    }
}

extern void JobPackageHeader_OneTimeJobSetup(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            JobInstance->Header.ScheduleFlag.bits.ScheduleOnce_b0 = 1;
        }
    }
    
}

extern void JobPackageHeader_ScheduleIntervalJobSetup(JobInstance_t* JobInstance, uint16_t intervalInSeconds, uint8_t Repeatable)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            if (Repeatable)
                JobInstance->Header.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
            
            JobInstance->Header.ScheduleFlag.bits.ShceduleInterval_b10 = 1;
            JobInstance->Header.ScheduleTime.Time = intervalInSeconds;
        }
    }
    
}

extern void JobPackageHeader_ScheduleRepeatableJobSetup(JobInstance_t* JobInstance, uint32_t ScheduleStartTime)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            JobInstance->Header.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
            
            JobInstance->Header.ScheduleTime.Time = JOB_SCHEDULE_TIME_NONE;
            JobInstance->Header.ScheduleStartTime.Time = ScheduleStartTime;
            
        }
    }
    
}

extern void JobPackageHeader_ScheduleTimeAndDayRepeatableJobSetup(JobInstance_t* JobInstance, uint32_t ScheduleStartTime, DaysOfWeek_t DaysSelection, SchTime_t InUTC0ExecutionTime)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Set the day schedule
            JobInstance->Header.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
            JobInstance->Header.ScheduleFlag.bits.ShceduleMon_b1 = DaysSelection.Monday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleTue_b2 = DaysSelection.Tuesday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleWed_b3 = DaysSelection.Wednesday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleThu_b4 = DaysSelection.Thursday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleFri_b5 = DaysSelection.Friday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleSat_b6 = DaysSelection.Saturday;
            JobInstance->Header.ScheduleFlag.bits.ShceduleSun_b7 = DaysSelection.Sunday;
            
            //Set the Execution time
            JobInstance->Header.ScheduleTime.bits.ShceduleTimeHour_b8_15 = InUTC0ExecutionTime.Hour24;
            JobInstance->Header.ScheduleTime.bits.ScheduleTimeMinute_b0_7 = InUTC0ExecutionTime.Minute;
            
            //Set the start time
            JobInstance->Header.ScheduleStartTime.Time = ScheduleStartTime;
            
        }
    }
    
}

extern bool JobPackageHeader_SetOrCondition(JobInstance_t* JobInstance, uint16_t Reserve)
{
    bool Success = false;
    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionOr_b7 = CONDITION_TYPE_OR;
        Success = true;
    }
    return Success;
}

extern bool JobPackageHeader_SetResetAllCondition(JobInstance_t* JobInstance)
{
    bool Success = false;
    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionRst_b6 = CONDITION_RST_ALL;
        Success = true;
    }
    return Success;
}

extern bool JobPackageHeader_SetFirstRunCondition(JobInstance_t* JobInstance)
{
    bool Success = false;
    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobConditionFirstRun_b5 = CONDITION_FIRST_RUN;
        Success = true;
    }
    return Success;
}

extern bool JobPackageHeader_SetBLEWriteOnce(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobBleWriteOnce_b4 = BLE_WRITE_RETRY_SHORT;
        Success = true;
    }
    return Success;
}

extern bool JobPackageHeader_SetJobSuperior(JobInstance_t* JobInstance)
{
	bool Success = false;

    if (JobInstance)
    {
        JobInstance->Header.JobType.bits.JobSuperior_b3 = JOB_SUPERIOR_ENABLE;
        Success = true;
    }
    return Success;
}

extern void JobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue)
{
    if (JobInstance && pValue)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            
            /* Condition */
            JobConditionHeader_t ConditionHeader;
            memcpy(ConditionHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddr->Addr));
            ConditionHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + UtilsGetValueTypeSize(TypeValue);
            
            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.bits.ConditionField_b0_7 = ConditionField;
            ConditionPayloadHeader.ConditionType.bits.ConditionReached_b8 = 0; //Must be set to 0
            ConditionPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = STANDARD_CHECK_STDBLE_COND_TYPE_FLAG;
            ConditionPayloadHeader.Operator = Operator;
            ConditionPayloadHeader.ValueType = TypeValue;
            
            LcJobPackageConditionAdd_CommonSetup(JobInstance, &ConditionHeader, &ConditionPayloadHeader, pValue);
            
        }
    }
    
    
}

extern void JobPackageConditionAdd_AdvertisementProtectedDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue)
{
    if (JobInstance && pValue)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            
            /* Condition */
            JobConditionHeader_t ConditionHeader;
            memcpy(ConditionHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddr->Addr));
            ConditionHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + UtilsGetValueTypeSize(TypeValue);
            
            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.bits.ConditionField_b0_7 = ConditionField;
            ConditionPayloadHeader.ConditionType.bits.ConditionReached_b8 = 0; //Must be set to 0
            ConditionPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG;
            ConditionPayloadHeader.Operator = Operator;
            ConditionPayloadHeader.ValueType = TypeValue;
            
            LcJobPackageConditionAdd_CommonSetup(JobInstance, &ConditionHeader, &ConditionPayloadHeader, pValue);
            
        }
    }    
}

extern void JobPackageConditionAdd_AdvertisementRTCSynchConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, enum eConditionOperator Operator, uint32_t RtcSynchThresholdMs)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            
            /* Condition */
            JobConditionHeader_t ConditionHeader;
            memcpy(ConditionHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddr->Addr));
            ConditionHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + sizeof(RtcSynchThresholdMs);
            
            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.bits.ConditionField_b0_7 = 0;
            ConditionPayloadHeader.ConditionType.bits.ConditionReached_b8 = 0; //Must be set to 0
            ConditionPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = RTC_CHECK_STDBLE_COND_TYPE_FLAG;
            ConditionPayloadHeader.Operator = Operator;
            ConditionPayloadHeader.ValueType = VALUE_TYPE_UINT32;
            
            LcJobPackageConditionAdd_CommonSetup(JobInstance, &ConditionHeader, &ConditionPayloadHeader, &RtcSynchThresholdMs);
            
        }
    }    
    
}

extern void JobPackageConditionAdd_AdvertisementNoEncKeyConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr)
{
    if (JobInstance)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            uint8_t DummyData = 0;
            
            /* Condition */
            JobConditionHeader_t ConditionHeader;
            memcpy(ConditionHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddr->Addr));
            ConditionHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + sizeof(DummyData);
            
            JobConditionPayloadHeader_t ConditionPayloadHeader;
            ConditionPayloadHeader.ConditionType.bits.ConditionField_b0_7 = 0;
            ConditionPayloadHeader.ConditionType.bits.ConditionReached_b8 = 0; //Must be set to 0
            ConditionPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG;
            ConditionPayloadHeader.Operator = OPERATOR_EQUAL;       //Don't care, any is fine
            ConditionPayloadHeader.ValueType = VALUE_TYPE_UINT8;    //Use UINT8 but actual value is not relevant to this condition.
                        
            LcJobPackageConditionAdd_CommonSetup(JobInstance, &ConditionHeader, &ConditionPayloadHeader, &DummyData);
            
        }
    }        
}


extern void JobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData)
{
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            BleStandardJobExecHeader_t StdExecHeader;
            memcpy(StdExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
            StdExecHeader.CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
            StdExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_ONLY;
            StdExecHeader.ServiceUUID = ServiceUUID;
            StdExecHeader.CharacteristicUUID = CharacteristicUUID;
            StdExecHeader.DataLen = (pData) ? DataLen:0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_STANDARD_BLE;
            
            LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance, &StdExecHeader, pData);
        }
    }
}

extern void JobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData)
{
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            BleStandardJobExecHeader_t StdExecHeader;
            memcpy(StdExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
            StdExecHeader.CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
            StdExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ;
            StdExecHeader.ServiceUUID = ServiceUUID;
            StdExecHeader.CharacteristicUUID = CharacteristicUUID;
            StdExecHeader.DataLen = (pData) ? DataLen:0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_STANDARD_BLE;
            
            LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance, &StdExecHeader, pData);
        }
    }
}

extern void JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID)
{
    
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            BleStandardJobExecHeader_t StdExecHeader;
            memcpy(StdExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
            StdExecHeader.CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
            StdExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = READ_FOLLOW_BYAGGREGATIONUPDATE;
            StdExecHeader.ServiceUUID = ServiceUUID;
            StdExecHeader.CharacteristicUUID = CharacteristicUUID;
            StdExecHeader.DataLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_STANDARD_BLE;
            
            LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance, &StdExecHeader, NULL);
        }
    }
}

extern void JobPackageExecutionAdd_PeripheralAccessReadFollowByDataReportJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID)
{
    
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            BleStandardJobExecHeader_t StdExecHeader;
            memcpy(StdExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
            StdExecHeader.CharFlag.bits.UUID_Len_b0 = JOB_EXEC_BLE_STD_UUID_128BIT;
            StdExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = READ_FOLLOW_BYDATA_REPORT;
            StdExecHeader.ServiceUUID = ServiceUUID;
            StdExecHeader.CharacteristicUUID = CharacteristicUUID;
            StdExecHeader.DataLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_STANDARD_BLE;
            
            LcJobPackageExecutionAdd_PeripheralAccessCommonSetup(JobInstance, &StdExecHeader, NULL);
        }
    }
}




extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData)
{
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {                
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));                
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_ONLY;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = (pData) ? DataLen:0;                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;
                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, pData);  
            
            }                      
        }
    }
}


extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData)
{
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {
            
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));                
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = (pData) ? DataLen:0;
                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;

                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, pData);    
            }                    
        }
    }
}


extern void JobPackageExecutionAdd_HybridPeripheralAccessReadFollowByAggregationUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128)
{
    
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {            
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));                
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = READ_FOLLOW_BYAGGREGATIONUPDATE;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = 0;
                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;

                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, NULL);  
            }                      
        }
    }
}

extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyDataAutoFillJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128)
{
    if (JobInstance && DevAddr)
    {
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Make sure its exactly 32 byte character
            if (   (strlen((char*)ServiceStrUUID128) == 32)
                && (strlen((char*)CharacteristicStrUUID128) == 32))
            {            
                HybridPeripheralJobExecHeader_t HybridExecHeader;
                memset(&HybridExecHeader, 0, sizeof(HybridExecHeader));                
                memcpy(HybridExecHeader.PeripheralDeviceMacAddress.Addr, DevAddr->Addr, sizeof(DevAddress_t));
                HybridExecHeader.CharFlag.bits.ReadWriteMode_b1_3 = WRITE_ONLY;
                HybridExecHeader.CharFlag.bits.DataMode_b4_5 = CHAR_DATA_AUTO_FILLED;
                
                DKHex_ASCToHex((char*)ServiceStrUUID128, strlen((char*)ServiceStrUUID128), (char*)HybridExecHeader.ServiceUUID, sizeof(HybridExecHeader.ServiceUUID));
                DKHex_ASCToHex((char*)CharacteristicStrUUID128, strlen((char*)CharacteristicStrUUID128), (char*)HybridExecHeader.CharacteristicUUID, sizeof(HybridExecHeader.CharacteristicUUID));
                
                HybridExecHeader.DataLen = 0;
                
                JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_HYBRIDPERIPHERAL;

                LcJobPackageExecutionAdd_HybridPeripheralAccessCommonSetup(JobInstance, &HybridExecHeader, NULL);  
            }                      
        }
    }    
}

extern void JobPackageExecutionAdd_GatewayJob_IdentifySetup(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_IDENTIFY;
            GatewayCommandHeader.PayloadLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, NULL);
            
        }
    }
    
}

extern void JobPackageExecutionAdd_GatewayJob_PeripheralUnpairSetup(JobInstance_t* JobInstance, DevAddressAndType_t* DevToUnpair)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_UNPAIR_PERIPHERAL;
            GatewayCommandHeader.PayloadLen = sizeof(DevAddressAndType_t);
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)DevToUnpair);
            
        }
    }
    
}

extern void JobPackageExecutionAdd_GatewayJob_SoftResetSetup(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_SOFT_RESTART;
            GatewayCommandHeader.PayloadLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, NULL);
            
        }
    }
    
}

extern void JobPackageExecutionAdd_GatewayJob_ScanUnpairPeripheralSetup(JobInstance_t* JobInstance, uint16_t ScanReportTimeInSeconds)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_SCAN_UNPAIR_PERIPHERAL;
            GatewayCommandHeader.PayloadLen = sizeof(ScanReportTimeInSeconds);
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)&ScanReportTimeInSeconds);
            
        }
    }
    
}

extern void JobPackageExecutionAdd_GatewayJob_PeripheralPairSetup(JobInstance_t* JobInstance, stPairingInfo_t* DevToPair)
{
    
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_PAIR_PERIPHERAL;
            GatewayCommandHeader.PayloadLen = sizeof(stPairingInfo_t);
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)DevToPair);
            
        }
    }
    
}

extern void JobPackageExecutionAdd_GatewayJob_PeripheralSetNewSecuritySetup(JobInstance_t* JobInstance, stRenewSecurityInfo_t* DevToReNewSecurity)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_SET_RENEW_SECURITY_FOR_PERIPHERAL;
            GatewayCommandHeader.PayloadLen = sizeof(stRenewSecurityInfo_t);
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)DevToReNewSecurity);
        }
    }
    
}


extern void JobPackageExecutionAdd_GatewayJob_NotificationSetup(JobInstance_t* JobInstance, const char* NotificationKey, const char* ReplacementParam1)
{
    if (JobInstance && NotificationKey)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_SEND_NOTIFICATION;
            if(ReplacementParam1)
            {
                GatewayCommandHeader.PayloadLen = strlen(NotificationKey) + strlen(ReplacementParam1) + 3; // The +3 at the end is the byte len for KeyLen, TotalParam, and ParamLen in Payload (See spec for detail)
            }
            else
            {
                GatewayCommandHeader.PayloadLen = strlen(NotificationKey)  + 2; // The +2 at the end is the byte len for KeyLen, and TotalParam in Payload (See spec for detail)
            }
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            uint8_t* GatewayCommandPayload =  DK_JOB_ALLOC("GatewayCommandPayload", 1, GatewayCommandHeader.PayloadLen);
            uint8_t* pGatewayCommandPayloadIndex = GatewayCommandPayload;
            
            uint8_t PayloadTotalParam;
            uint8_t PayloadParamLen;
            uint8_t PayloadKeyLen = strlen(NotificationKey);
            if(ReplacementParam1)
            {
                PayloadTotalParam = 1;
                PayloadParamLen = strlen(ReplacementParam1);
            }
            else
            {
                PayloadTotalParam = 0;
            }
            
            memcpy(pGatewayCommandPayloadIndex, &PayloadKeyLen, sizeof(PayloadKeyLen)); pGatewayCommandPayloadIndex += sizeof(PayloadKeyLen);
            memcpy(pGatewayCommandPayloadIndex, NotificationKey, strlen(NotificationKey)); pGatewayCommandPayloadIndex += strlen(NotificationKey);
            memcpy(pGatewayCommandPayloadIndex, &PayloadTotalParam, sizeof(PayloadTotalParam)); pGatewayCommandPayloadIndex += sizeof(PayloadTotalParam);
            if(ReplacementParam1)
            {
                memcpy(pGatewayCommandPayloadIndex, &PayloadParamLen, sizeof(PayloadParamLen)); pGatewayCommandPayloadIndex += sizeof(PayloadParamLen);
                memcpy(pGatewayCommandPayloadIndex, ReplacementParam1, strlen(ReplacementParam1)); pGatewayCommandPayloadIndex += strlen(ReplacementParam1);
            }
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, GatewayCommandPayload);
            
            free(GatewayCommandPayload);
        }
    }
    
}


extern void JobPackageExecutionAdd_GatewayJob_NotificationSetupExt(JobInstance_t* JobInstance, const char* NotificationKey, const char* ReplacementParam1, const char* ReplacementParam2)
{
    if (JobInstance && NotificationKey && ReplacementParam1 && ReplacementParam2)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_GATEWAY_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_SEND_NOTIFICATION;
            GatewayCommandHeader.PayloadLen = strlen(NotificationKey) + strlen(ReplacementParam1) + strlen(ReplacementParam2) + 4; //4 extra byte for the notification payload (KeyLen, TotalParam, Param1Len, and Param2Len) (See spec for detail)

            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_GATEWAY_COMMAND;

            uint8_t* GatewayCommandPayload =  DK_JOB_ALLOC("GatewayCommandPayload", 1, GatewayCommandHeader.PayloadLen);

            uint8_t* pGatewayCommandPayloadIndex = GatewayCommandPayload;

            uint8_t PayloadKeyLen = strlen(NotificationKey);
            uint8_t PayloadTotalParam = 2;
            uint8_t PayloadParam1Len = strlen(ReplacementParam1);
            uint8_t PayloadParam2Len = strlen(ReplacementParam2);
            
            memcpy(pGatewayCommandPayloadIndex, &PayloadKeyLen, sizeof(PayloadKeyLen)); pGatewayCommandPayloadIndex += sizeof(PayloadKeyLen);
            memcpy(pGatewayCommandPayloadIndex, NotificationKey, strlen(NotificationKey)); pGatewayCommandPayloadIndex += strlen(NotificationKey);
            memcpy(pGatewayCommandPayloadIndex, &PayloadTotalParam, sizeof(PayloadTotalParam)); pGatewayCommandPayloadIndex += sizeof(PayloadTotalParam);
            memcpy(pGatewayCommandPayloadIndex, &PayloadParam1Len, sizeof(PayloadParam1Len)); pGatewayCommandPayloadIndex += sizeof(PayloadParam1Len);
            memcpy(pGatewayCommandPayloadIndex, ReplacementParam1, strlen(ReplacementParam1)); pGatewayCommandPayloadIndex += strlen(ReplacementParam1);
            memcpy(pGatewayCommandPayloadIndex, &PayloadParam2Len, sizeof(PayloadParam2Len)); pGatewayCommandPayloadIndex += sizeof(PayloadParam2Len);
            memcpy(pGatewayCommandPayloadIndex, ReplacementParam2, strlen(ReplacementParam2)); pGatewayCommandPayloadIndex += strlen(ReplacementParam2);

            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, GatewayCommandPayload);
            free(GatewayCommandPayload);
        }
    }
}

extern void JobPackageExecutionAdd_GatewayJob_DeleteScheduleLongTermJobSetup(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_DELETE_SCHEDULE_LONGTERM_JOB;
            GatewayCommandHeader.PayloadLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, NULL);
            
        }
    }
}

extern void JobPackageExecutionAdd_GatewayJob_DeleteAdvenceJobSetup(JobInstance_t* JobInstance, uint32_t AdvanceJobObjectID)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_DELETE_ADVANCE_JOB;
            GatewayCommandHeader.PayloadLen = sizeof(AdvanceJobObjectID);

            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)&AdvanceJobObjectID);

        }
    }

}

extern void JobPackageExecutionAdd_GatewayJob_HistoricEventUpdateSetup(JobInstance_t* JobInstance)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_HISTORIC_EVENT_UPDATE;
            GatewayCommandHeader.PayloadLen = 0;
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, NULL);
            
        }
    }
}

extern void JobPackageExecutionAdd_GatewayJob_UpdateLocalPeripheralName(JobInstance_t* JobInstance, uint32_t PeripheralObjectID, uint8_t *pDeviceName)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_UPDATE_LOCAL_PERIPHERAL_NAME;
            GatewayCommandHeader.PayloadLen = sizeof(PeripheralObjectID) + strlen((char*)pDeviceName) + 1;         // Including NULL-terminated character

            uint8_t* GatewayCommandPayload =  DK_JOB_ALLOC("GatewayCommandPayload", 1, GatewayCommandHeader.PayloadLen);

            uint8_t* pGatewayCommandPayloadIndex = GatewayCommandPayload;

            memcpy(pGatewayCommandPayloadIndex, &PeripheralObjectID, sizeof(PeripheralObjectID)); pGatewayCommandPayloadIndex += sizeof(PeripheralObjectID);
            memcpy(pGatewayCommandPayloadIndex, pDeviceName, strlen((char*)pDeviceName)); pGatewayCommandPayloadIndex += strlen((char*)pDeviceName);

            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, GatewayCommandPayload);
            free(GatewayCommandPayload);

            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
        }
    }
}


extern void JobPackageExecutionAdd_GatewayJob_UpdatePeripheralRTC(JobInstance_t* JobInstance, stUpdatePeripheralRtcInfo_t* DevToUpdateRTC)
{
    if (JobInstance)
    {
        if ((JobInstance->InstanceID == JOB_INSTANCE_VALID_ID) &&
            ((JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_DEVICE_COMMAND) || (JobInstance->Header.JobType.bits.JobExecutionType_b12_15 == 0)))
        {
            DeviceCommandJobHeader_t GatewayCommandHeader;
            GatewayCommandHeader.CommandId = DEVICE_COMMAND_ID_UPDATE_PERIPHERAL_RTC;
            GatewayCommandHeader.PayloadLen = sizeof(stUpdatePeripheralRtcInfo_t);
            
            JobInstance->Header.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
            
            LcJobPackageExecutionAdd_GatewayCommandCommonSetup(JobInstance, &GatewayCommandHeader, (uint8_t*)DevToUpdateRTC);
            
        }
    }
    
}


extern uint8_t* JobPackageGeneratePayloadPack(JobInstance_t* JobInstance, uint16_t* ReturnedPayloadLen)
{
    uint8_t* pPayload = NULL;
    uint8_t* pPayloadIndex = NULL;
    
    if (JobInstance && ReturnedPayloadLen)
    {
        *ReturnedPayloadLen = 0;
        if (JobInstance->InstanceID == JOB_INSTANCE_VALID_ID)
        {
            //Free First - Should be NULL but we can still continue.
            if (JobInstance->pPayloadPack)
            {
                free(JobInstance->pPayloadPack);
                JobInstance->pPayloadPack=NULL;
            }
            
            *ReturnedPayloadLen += sizeof(JobHeader_t) + JobInstance->ConditionLen + JobInstance->ExecutionLen;
            JobInstance->pPayloadPack = DK_JOB_ALLOC("JobPayloadPack", 1, *ReturnedPayloadLen);
            pPayloadIndex = JobInstance->pPayloadPack;
            
            memcpy(pPayloadIndex, &JobInstance->Header, sizeof(JobHeader_t)); pPayloadIndex += sizeof(JobHeader_t);
            if (JobInstance->ConditionLen)
            {
                memcpy(pPayloadIndex, JobInstance->pCondition, JobInstance->ConditionLen); pPayloadIndex += JobInstance->ConditionLen;
            }
            if (JobInstance->ExecutionLen)
            {
                memcpy(pPayloadIndex, JobInstance->pExecution, JobInstance->ExecutionLen); pPayloadIndex += JobInstance->ExecutionLen;
            }
            
            pPayload = JobInstance->pPayloadPack;
        }
    }
    
    return pPayload;
}



extern bool	ParseJobPackage_JobSuperior(uint8_t* JobPayloadIn)
{
    bool bSuperior = false;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->JobType.bits.JobSuperior_b3 == JOB_SUPERIOR_ENABLE)
        {
            bSuperior = true;
        }
    }
    
    return bSuperior;
}


extern bool	ParseJobPackage_ThresholdReExecutionUnit(uint8_t* JobPayloadIn, uint8_t* ThresholdUnitOf15Sec)
{
    bool Valid = false;
    
    if (JobPayloadIn && ThresholdUnitOf15Sec )
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            *ThresholdUnitOf15Sec = JobHeader->ReExecutionThreshold;
            Valid = true;
        }
    }
    
    return Valid;
}


extern bool	ParseJobPackage_ScheduleTimeAndDay(uint8_t* JobPayloadIn, uint32_t* OutScheduleStartTime, DaysOfWeek_t* OutDaysSelection, SchTime_t* OutUTC0ExecutionTime)
{
    bool Valid = false;
    
    if (JobPayloadIn && OutScheduleStartTime && OutDaysSelection && OutUTC0ExecutionTime)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if (JobHeader->ScheduleFlag.bits.ShceduleRepeatable_b15)
            {
                *OutScheduleStartTime = JobHeader->ScheduleStartTime.Time;
                
                OutDaysSelection->Monday = JobHeader->ScheduleFlag.bits.ShceduleMon_b1;
                OutDaysSelection->Tuesday = JobHeader->ScheduleFlag.bits.ShceduleTue_b2;
                OutDaysSelection->Wednesday = JobHeader->ScheduleFlag.bits.ShceduleWed_b3;
                OutDaysSelection->Thursday = JobHeader->ScheduleFlag.bits.ShceduleThu_b4;
                OutDaysSelection->Friday = JobHeader->ScheduleFlag.bits.ShceduleFri_b5;
                OutDaysSelection->Saturday = JobHeader->ScheduleFlag.bits.ShceduleSat_b6;
                OutDaysSelection->Sunday = JobHeader->ScheduleFlag.bits.ShceduleSun_b7;
                
                OutUTC0ExecutionTime->Hour24 = JobHeader->ScheduleTime.bits.ShceduleTimeHour_b8_15;
                OutUTC0ExecutionTime->Minute = JobHeader->ScheduleTime.bits.ScheduleTimeMinute_b0_7;
                
                Valid = true;
            }
        }
    }
    
    return Valid;
}


extern const uint8_t*	ParseJobPackage_FirstPeripheralAccessInfo(uint8_t* JobPayloadIn, uint16_t JobPayloadLen, DevAddress_t* OutDevAddr, uint16_t* OutServiceUUID, uint16_t* OutCharacteristicUUID, uint16_t* OutDataLen)
{
    uint8_t* OutData = NULL;
    
    if (JobPayloadIn && OutDevAddr && OutServiceUUID && OutCharacteristicUUID && OutDataLen)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if ((JobHeader->Preamble == JOB_HEADER_PREAMBLE) && (JobPayloadLen > sizeof(JobHeader_t)))
        {
            if ((JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_STANDARD_BLE) && (JobHeader->TotalJobExecution))
            {
                uint8_t*  pJobExecution = GetJobExecutionStartPointer(JobPayloadIn);
                BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;
                memcpy((uint8_t*)OutDevAddr, (uint8_t*)&JobExecutionHeader->PeripheralDeviceMacAddress, sizeof(DevAddress_t));
                *OutServiceUUID = JobExecutionHeader->ServiceUUID;
                *OutCharacteristicUUID = JobExecutionHeader->CharacteristicUUID;
                *OutDataLen = JobExecutionHeader->DataLen;
                OutData = (pJobExecution + sizeof(BleStandardJobExecHeader_t));
                
            }
        }
    }
    
    return OutData;
    
}


extern const uint8_t*   ParseJobPackage_FirstAdvertisementDataConditionInfo(uint8_t* JobPayloadIn, uint16_t JobPayloadLen, uint8_t* OutConditionField, enum eConditionOperator *OutOperator, enum eConditionValueType *OutTypeValue)
{
    uint8_t* OutData = NULL;

    if (JobPayloadIn && OutConditionField && OutOperator && OutTypeValue)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if ((JobHeader->Preamble == JOB_HEADER_PREAMBLE) && (JobPayloadLen > sizeof(JobHeader_t)))
        {
            if ((JobHeader->JobType.bits.JobConditionType_b8_11 == JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL) && (JobHeader->TotalJobCondition))
            {
                uint8_t*  pJobCondition = GetJobConditionStartPointer(JobPayloadIn);
                JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionHeader_t));

                if (pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 == STANDARD_CHECK_STDBLE_COND_TYPE_FLAG)
                {
                    *OutConditionField = pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7;
                    *OutOperator = pJobConditionPayloadHeader->Operator;
                    *OutTypeValue = pJobConditionPayloadHeader->ValueType;
                    OutData = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                }
            }
        }
    }

    return OutData;
    
}


extern uint8_t	ParseJobPackage_TotalAvailableAdvertisementDataCondition(uint8_t* JobPayloadIn)
{
    uint8_t TotalJobCondition = 0;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((JobHeader->JobType.bits.JobConditionType_b8_11 == JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL) && (JobHeader->TotalJobCondition))
            {
                TotalJobCondition = JobHeader->TotalJobCondition;
            }
        }
    }
    
    return TotalJobCondition;
}

extern uint8_t	ParseJobPackage_TotalAvailablePeripheralAccessExecution(uint8_t* JobPayloadIn)
{
    uint8_t TotalJobExecution = 0;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_STANDARD_BLE) && (JobHeader->TotalJobExecution))
            {
                TotalJobExecution = JobHeader->TotalJobExecution;
            }
        }
    }
    
    return TotalJobExecution;
    
}

extern uint8_t	ParseJobPackage_TotalAvailableHybridPeripheralAccessExecution(uint8_t* JobPayloadIn)
{
    uint8_t TotalJobExecution = 0;
    
    if (JobPayloadIn)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
        {
            if ((JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_HYBRIDPERIPHERAL) && (JobHeader->TotalJobExecution))
            {
                TotalJobExecution = JobHeader->TotalJobExecution;
            }
        }
    }
    
    return TotalJobExecution;
    
    
}

extern const uint8_t*	ParseJobPackage_PeripheralAccessInfoByIndex(uint8_t ExecutionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen, DevAddress_t* OutDevAddr, uint16_t* OutServiceUUID, uint16_t* OutCharacteristicUUID, uint16_t* OutDataLen)
{
    uint8_t* OutData = NULL;
    
    if (JobPayloadIn && OutDevAddr && OutServiceUUID && OutCharacteristicUUID && OutDataLen)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if ((JobHeader->Preamble == JOB_HEADER_PREAMBLE) && (JobPayloadLen > sizeof(JobHeader_t)))
        {
            if ((JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_STANDARD_BLE)
                && (JobHeader->TotalJobExecution)
                && (ExecutionIndex < JobHeader->TotalJobExecution))
            {
                uint8_t*  pJobExecution = GetJobExecutionStartPointer(JobPayloadIn);
                //Offset to the requested one
                uint8_t i = 0;
                for (i = 0; i < ExecutionIndex; i++)
                {
                    BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;
                    pJobExecution += (sizeof(BleStandardJobExecHeader_t) + JobExecutionHeader->DataLen);
                }
                
                BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;
                memcpy((uint8_t*)OutDevAddr, (uint8_t*)&JobExecutionHeader->PeripheralDeviceMacAddress, sizeof(DevAddress_t));
                *OutServiceUUID = JobExecutionHeader->ServiceUUID;
                *OutCharacteristicUUID = JobExecutionHeader->CharacteristicUUID;
                *OutDataLen = JobExecutionHeader->DataLen;
                OutData = (pJobExecution + sizeof(BleStandardJobExecHeader_t));
                
            }
        }
    }
    
    return OutData;
    
}

extern JobExecutionHybrid_t ParseJobPackage_HybridPeripheralAccessInfoByIndex(uint8_t ExecutionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen)
{
    JobExecutionHybrid_t ParsedJobExecutionHybrid = LcParseHybridJobExecutionPackageByExecutionIndex(ExecutionIndex, JobPayloadIn, JobPayloadLen);
    
    return ParsedJobExecutionHybrid;
}


extern const uint8_t*   ParseJobPackage_AdvertisementDataConditionInfoByIndex(uint8_t ConditionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen, uint8_t* OutConditionField, enum eConditionOperator *OutOperator, enum eConditionValueType *OutTypeValue)
{
    uint8_t* OutData = NULL;
    
    if (JobPayloadIn && OutConditionField && OutOperator && OutTypeValue)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobPayloadIn;
        if ((JobHeader->Preamble == JOB_HEADER_PREAMBLE) && (JobPayloadLen > sizeof(JobHeader_t)))
        {
            if ((JobHeader->JobType.bits.JobConditionType_b8_11 == JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL)
                && (JobHeader->TotalJobCondition)
                && (ConditionIndex < JobHeader->TotalJobCondition))
            {
                uint8_t*  pJobCondition = GetJobConditionStartPointer(JobPayloadIn);
                //Offset to the requested one
                uint8_t i = 0;
                for (i = 0; i < ConditionIndex; i++)
                {
                    JobConditionHeader_t* pJobConditionHeader = (JobConditionHeader_t*)pJobCondition;
                    pJobCondition += (sizeof(JobConditionHeader_t) + pJobConditionHeader->PayloadLen);
                }
                
                JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionHeader_t));
                if (pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 == STANDARD_CHECK_STDBLE_COND_TYPE_FLAG)
                {
                    *OutConditionField = pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7;
                    *OutOperator = pJobConditionPayloadHeader->Operator;
                    *OutTypeValue = pJobConditionPayloadHeader->ValueType;
                    OutData = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                }
            }
        }
    }
    
    return OutData;
}

extern uint64_t _ScheduleSequnceStructToUINT64(struct ScheduleSequenceNumber_s* ScheduleSEQNo)
{
    uint64_t ScheduleSequenceNumber = 0;
    uint64_t PeripheralUniqueID = ScheduleSEQNo->PeripheralUniqueID;
    uint64_t UniqueSequenceID = ScheduleSEQNo->UniqueSequenceID;
    ScheduleSequenceNumber = (	((PeripheralUniqueID & 0x00000000FFFFFFFF) << 16) +
                               (UniqueSequenceID & 0x000000000000FFFF));
    
    //ScheduleSequenceNumber = (((ScheduleSEQNo->PeripheralUniqueID << 16)&0xFFFFFFFFFFFFFFFF) | (ScheduleSEQNo->UniqueSequenceID));
    
    return ScheduleSequenceNumber;
}

extern struct ScheduleSequenceNumber_s _UINT64ScheduleSeqNoToStruct(uint64_t ScheduleSEQNo)
{
    struct ScheduleSequenceNumber_s ScheduleStructSeqNo;
    
    memset(&ScheduleStructSeqNo, 0, sizeof(struct ScheduleSequenceNumber_s));
    
    ScheduleStructSeqNo.PeripheralUniqueID = (uint32_t)((ScheduleSEQNo >> 16) & (0x00000000FFFFFFFF));
    ScheduleStructSeqNo.UniqueSequenceID = (uint16_t)((ScheduleSEQNo) & (0x000000000000FFFF));
    
    return ScheduleStructSeqNo;
}


extern uint8_t* GetJobConditionStartPointer(uint8_t* JobData)
{
    
    uint8_t* pWorkingJobData = NULL;
    
    if (JobData)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobData;
        
        if (JobHeader->TotalJobCondition)
            pWorkingJobData = JobData + sizeof(JobHeader_t);
    }
    
    return pWorkingJobData;
}


extern uint8_t* GetJobExecutionStartPointer(uint8_t* JobData)
{
    uint8_t* pWorkingJobData = NULL;
    
    if (JobData)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobData;
        
        if (JobHeader->TotalJobExecution)
        {
            pWorkingJobData = JobData + sizeof(JobHeader_t);
            
            uint8_t CurrentJobCondition;
            for (CurrentJobCondition = 0; CurrentJobCondition < JobHeader->TotalJobCondition; CurrentJobCondition++)
            {
                JobConditionHeader_t* pCurrentJobCondition = (JobConditionHeader_t*)pWorkingJobData;
                pWorkingJobData += (sizeof(JobConditionHeader_t) + pCurrentJobCondition->PayloadLen);
            }
        }
    }
    
    return pWorkingJobData;
}
