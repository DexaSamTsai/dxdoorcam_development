/*!
 *  @file       json.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
#include "includes.h"
# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <ctype.h>

# include "Json.h"

//==========================================================================
// Type Define
//==========================================================================
# ifndef dbg_line
#	define dbg_line( fmt, ... )		printf( "[%s line: %d]"fmt"\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

typedef struct {
	json_value		*values;
	uint32_t		value_len;
	uint32_t		value_idx;
} json_settings;

typedef struct {
	json_settings	settings;

	const char		*ptr;
	const char		*end;
} json_state;

//==========================================================================
// Static Functions
//==========================================================================
# ifndef atof
double
atof( const char *str )
{
	if ( str ) {
		double val;
		sscanf( str, "%lf", &val );
		return val;
	}
	return 0;
}
# endif

static json_value *
json_new( json_state * state, unsigned int cnt, json_type type )
{
	if ( state->settings.values ) {
		if ( state->settings.value_idx + cnt >= state->settings.value_len ) {
			return NULL;
		} else {
			json_value *value	= &state->settings.values[ state->settings.value_idx ];
			state->settings.value_idx += cnt;
			value->type		= type;
			return value;
		}
	} else {
		return NULL;
	}
}

# define whitespace \
	case '\n': \
	case ' ': case '\t': case '\r'

static int
json_string_len( json_state *state )
{
	uint32_t	str_len	= 0;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			case '\0':
			case '\n':
				return -1;

			case '"':
				return str_len;
		}
		str_len ++;
	}
	return -1;
}

static int
json_get_string( json_state *state, char **string, uint32_t *len )
{
	char	*name	= ( char * )state->ptr;
	int		str_len	= json_string_len( state );

	if ( str_len < 0 ) {
		return -1;
	} else if ( str_len ) {
		name[ str_len ]	= '\0';
	} else {
		name = NULL;
	}

	*string = name;
	*len	= str_len;

	return 0;
}

static json_value *
json_get_num( json_state *state )
{
	char *start = ( char * )state->ptr;
	json_value *value = NULL;
	char is_e_num = 0;
	char is_float = 0;

	state->ptr ++;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			case 'e':
			case 'E':
				if ( is_e_num ) {
					return NULL;
				} else {
					state->ptr ++;
					if ( ( *state->ptr == '+' ) || ( *state->ptr == '-' ) ) {
						is_e_num	= 1;
					} else {
						return NULL;
					}
				}
				continue;

			case '.':
				if ( is_e_num || is_float ) {
					return NULL;
				} else {
					is_float = 1;
				}
				continue;

			default:
				if ( isdigit( *state->ptr ) ) {
					continue;
				} else {
					state->ptr --;
					goto generate_num;
				}
		}
	}

generate_num:
	value = json_new( state, 1, JSON_NUMBER );

	if ( value ) {
		value->v.number	= atof( start );
	}

	return value;
}

static json_value *
json_find_start( json_state *state )
{
	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case '{':
				return json_new( state, 1, JSON_OBJECT );

			case '[':
				return json_new( state, 1, JSON_ARRAY );

			default:
				break;
		}
	}

	return NULL;
}

static json_value *
json_get_value( json_state *state )
{
	json_value *value;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case '"': {
				char *str;
				uint32_t str_len;

				state->ptr ++;
				if ( json_get_string( state, &str, &str_len ) < 0 ) {
					dbg_line( "Do json_get_string Failed!!!" );
					return NULL;
				}

				value	= json_new( state, 1, JSON_STRING );
				value->v.string		= str;
				value->data_length	= str_len;
				return value;
			}

			case '{':
				return json_new( state, 1, JSON_OBJECT );

			case '[':
				return json_new( state, 1, JSON_ARRAY );

			case 't':
				if ( strncmp( state->ptr, "true", 4 ) ) {
					return NULL;
				}

				state->ptr += 3;

				value = json_new( state, 1, JSON_BOOLEAN );
				if ( value ) {
					value->v.boolean	= 1;
				}

				return value;

			case 'f':
				if ( strncmp( state->ptr, "false", 5 ) ) {
					return NULL;
				}

				state->ptr += 4;

				value = json_new( state, 1, JSON_BOOLEAN );
				if ( value ) {
					value->v.boolean	= 0;
				}

				return value;

			case 'n':
				if ( strncmp( state->ptr, "null", 4 ) ) {
					return NULL;
				}

				state->ptr += 3;

				return json_new( state, 1, JSON_NULL );

			default:
				if ( isdigit( *state->ptr ) || ( *state->ptr == '-' ) ) {
					return json_get_num( state );
				} else {
					dbg_line( "(%c)!!!", *state->ptr );
					return NULL;
				}
		}
	}

	dbg_line( "(%c)!!!", *state->ptr );
	return NULL;
}

static int
json_find_node( json_state *state, json_value **value )
{
	*value = json_get_value( state );

	if ( *value == NULL ) {
		if ( state->ptr > state->end ) {
			return -1;
		} else if ( *state->ptr != ']' ) {
			return -1;
		}
	}

	return 0;
}

static json_value *
json_find_value( json_state *state )
{
	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case ':':
				state->ptr ++;
				return json_get_value( state );

			default:
				dbg_line();
				return NULL;
		}
	}
	dbg_line();
	return NULL;
}

static int
json_find_object( json_state *state, json_value **value )
{
	char *name;
	uint32_t str_len = 0;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case '"':
				state->ptr ++;
				if ( json_get_string( state, &name, &str_len ) < 0 ) {
					dbg_line( "Do json_get_string Failed!!!" );
					return -1;
				}

				state->ptr ++;
				*value = json_find_value( state );
				if ( *value ) {
					( *value )->name		= name;
					( *value )->name_length	= str_len;
					return 0;
				} else {
					dbg_line( "Do json_find_value Failed!!!" );
					return -1;
				}

			case '}':
				dbg_line( "json_find_object end!!!" );
				*value = NULL;
				return 0;

			default:
				dbg_line( "json_find_object (%c)!!!", *state->ptr );
				return -1;
		}
	}
	return -1;
}

static int
json_find_next( json_state *state, char end )
{
	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case ',':
				return 1;

			default:
				if ( *state->ptr == end ) {
					return 0;
				}
				return -1;
		}
	}
	return -1;
}

static int
json_chk_finished( json_state *state, char end )
{
	int finished = 0;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		switch ( *state->ptr ) {
			whitespace:
				continue;

			case '\0':
				if ( finished ) {
					return finished;
				} else {
					return -1;
				}
				break;

			default:
				if ( ( *state->ptr == end ) && ( finished == 0 ) ) {
					finished = 1;
					continue;
				}
				dbg_line( "json_chk_finished (%c)!!!", *state->ptr );
				return -1;
		}
	}

	if ( finished ) {
		return 0;
	} else {
		return -1;
	}
}

static json_value *
json_start_parse( json_state *state, const char * json, size_t length )
{
	int res;
	json_value *value = NULL, *root = NULL, *parent = NULL;

   /* Skip UTF-8 BOM */
	if ( length >= 3 && ( ( unsigned char )json[ 0 ] ) == 0xEF
					 && ( ( unsigned char )json[ 1 ] ) == 0xBB
					 && ( ( unsigned char )json[ 2 ] ) == 0xBF ) {
		json += 3;
		length -= 3;
	}

	state->end = ( json + length );
	state->ptr = json;

	value = json_find_start( state );
	if ( value == NULL ) {
		dbg_line( "Do json_find_start Failed!!!" );
		goto e_failed;
	}

	parent	=
	root	= value;

	state->ptr ++;

	for ( ; state->ptr <= state->end; state->ptr ++ ) {
		value = NULL;

		if ( parent->type == JSON_ARRAY ) {
			if ( json_find_node( state, &value ) < 0 ) {
				dbg_line( "Do json_find_node Failed!!!" );
				goto e_failed;
			}

			if ( parent->data_length == 0 ) {
				parent->v.child	= value;
			}
		} else if ( parent->type == JSON_OBJECT ) {
			if ( json_find_object( state, &value ) < 0 ) {
				dbg_line( "Do json_find_object Failed!!!" );
				goto e_failed;
			}

			if ( parent->data_length == 0 ) {
				parent->v.child	= value;
			}
		} else {
			dbg_line( "parent->type( %d ) error!!!", parent->type );
			goto e_failed;
		}

		if ( value == NULL ) {
			if ( parent->data_length ) {
				dbg_line( "parent->data_length = %d", parent->data_length );
				goto e_failed;
			}
			parent	= parent->parent;
		} else {
			if ( parent == NULL ) {
				dbg_line( "parent == NULL" );
				goto e_failed;
			}

			if ( parent->end ) {
				parent->end->next	= value;
			}

			parent->data_length ++;
			parent->end		= value;
			value->parent	= parent;

			if ( ( value->type == JSON_ARRAY ) ||
				 ( value->type == JSON_OBJECT ) ) {
				parent = value;
				continue;
			}
		}

		while ( parent ) {
			state->ptr ++;

			if ( parent->type == JSON_ARRAY  ) {
				res = json_find_next( state, ']' );
			} else if ( parent->type == JSON_OBJECT ) {
				res = json_find_next( state, '}' );
			} else {
				dbg_line( "parent->type( %d ) error!!!", parent->type );
				goto e_failed;
			}

			if ( res < 0 ) {
				goto e_failed;
			} else if ( res == 0 ) {
				parent	= parent->parent;
			} else {
				break;
			}
		}

		if ( parent == NULL ) {
			if ( root->type == JSON_ARRAY  ) {
				res = json_chk_finished( state, ']' );
			} else if ( root->type == JSON_OBJECT ) {
				res = json_chk_finished( state, '}' );
			} else {
				res = -1;
			}

			if ( res < 0 ) {
				dbg_line( "Do json_chk_finished Failed!!!" );
				goto e_failed;
			} else if ( res > 0 ) {
				break;
			}
		}
	}

	return root;

e_failed:
	Json_Free( state->settings.values );

	return NULL;
}

static json_value *
json_find_child( json_value *json, const char *name )
{
	json_value *item	= json->v.child;

	while ( item ) {
		if ( item->name && ( strcmp( item->name, name ) == 0 ) ) {
			return item;
		}
		item = item->next;
	}

	return NULL;
}

//==========================================================================
// APIs
//==========================================================================
int
Json_GetNumber( json_value *json, double *value )
{
	if ( json && value && ( json->type == JSON_NUMBER ) ) {
		*value = json->v.number;
		return 0;
	}

	return -1;
}

int
Json_GetString( json_value *json, char **string )
{
	if ( json && string && ( json->type == JSON_STRING ) ) {
		*string = json->v.string;
		return 0;
	}

	return -1;
}

json_value *
Json_GetChild( json_value *json )
{
	if ( json ) {
		if ( ( json->type == JSON_ARRAY ) || ( json->type == JSON_OBJECT )) {
			return json->v.child;
		}
		return NULL;
	}

	return NULL;
}

json_value *
Json_FindChild( json_value *json, const char *name )
{
	if ( json && name && ( json->type == JSON_OBJECT ) ) {
		return json_find_child( json, name );
	}

	return NULL;
}

int
Json_FindChildNumber( json_value *json, const char *name, double *value )
{
	if ( json && name && value ) {
		json_value *item = json_find_child( json, name );
		*value	= 0;

		if ( item ) {
			return Json_GetNumber( item, value );
		}
	}

	return -1;
}

int
Json_FindChildString( json_value *json, const char *name, char **string )
{
	if ( json && name && string ) {
		json_value *item = json_find_child( json, name );
		*string	= NULL;

		if ( item ) {
			return Json_GetString( item, string );
		}
	}

	return -1;
}

json_value *
Json_Parse( const char *json, size_t length, const void *buff, uint32_t max_cnt )
{
	json_state state;

	memset( &state, 0, sizeof( json_state ) );

	state.settings.value_len	= max_cnt;
	if ( buff == NULL ) {
		state.settings.values		= ( json_value * )malloc( sizeof( json_value ) * state.settings.value_len );
	} else {
		state.settings.values		= ( json_value * )buff;
	}

	memset( state.settings.values, 0, ( sizeof( json_value ) * state.settings.value_len ) );

	return json_start_parse( &state, json, length );
}

void
Json_Free( void *value )
{
	if ( value ) {
		free( value );
	}
}

