//============================================================================
// File: Utils.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.175.0
//     Date: 2015/09/07
//     1) Description: Fix an issue while converting between DKTime and UTC.
//                     It's important to fix this issue if we have to support motion sensor
//        Modified: ConvertDKTimeToUTC()
//                  ConvertUTCToDKTime()
//
//     Version: 0.175.1
//     Date: 2016/03/02
//     1) Description: Fix an issue while converting "2016-03-01 08:59:59" to UTC.
//        Modified: ConvertDKTimeToUTC()
//============================================================================
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define CRC_KERMIT_POLYNOM  0x8408;
#define CRC_KERMIT_PRESET   0

#define POLYNOMIAL 0x8005


static const uint16_t crc16_table[256] = {
	0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
	0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
	0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
	0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
	0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
	0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
	0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
	0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
	0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
	0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
	0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
	0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
	0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
	0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
	0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
	0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
	0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
	0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
	0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
	0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
	0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
	0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
	0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
	0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
	0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
	0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
	0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
	0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
	0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
	0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
	0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
	0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78
};

/* 0x8005 Table but formula is wrong... need to get proper formula
static const uint16_t crc16_table[256] = {
	0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
	0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
	0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
	0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
	0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
	0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
	0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
	0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
	0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
	0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
	0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
	0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
	0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
	0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
	0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
	0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
	0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
	0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
	0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
	0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
	0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
	0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
	0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
	0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
	0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
	0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
	0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
	0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
	0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
	0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
	0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
	0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
};
*/

#define CRC_INNER_LOOP(n, c, x) \
	(c) = ((c) >> 8) ^ crc##n##_table[((c) ^ (x)) & 0xff]


/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Local Function Definitions
 ******************************************************/


int8_t uValueTypeChallenge_GreaterThanOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp)
{
	int8_t ChallengeResult = -1;

	switch (LeftValueOp->ValueType)
	{
		case VALUE_TYPE_INT8:
			ChallengeResult = (LeftValueOp->Value.TypeINT8 > RightValueOp->Value.TypeINT8) ? 1:0;
		break;

		case VALUE_TYPE_UINT8:
			ChallengeResult = (LeftValueOp->Value.TypeUINT8 > RightValueOp->Value.TypeUINT8) ? 1:0;
			break;

		case VALUE_TYPE_INT16:
			ChallengeResult = (LeftValueOp->Value.TypeINT16 > RightValueOp->Value.TypeINT16) ? 1:0;
			break;

		case VALUE_TYPE_UINT16:
			ChallengeResult = (LeftValueOp->Value.TypeUINT16 > RightValueOp->Value.TypeUINT16) ? 1:0;
			break;

		case VALUE_TYPE_INT32:
			ChallengeResult = (LeftValueOp->Value.TypeINT32 > RightValueOp->Value.TypeINT32) ? 1:0;
			break;

		case VALUE_TYPE_UINT32:
			ChallengeResult = (LeftValueOp->Value.TypeUINT32 > RightValueOp->Value.TypeUINT32) ? 1:0;
			break;

		case VALUE_TYPE_FLOAT:
			ChallengeResult = (LeftValueOp->Value.TypeFLOAT > RightValueOp->Value.TypeFLOAT) ? 1:0;
			break;

	}

	return ChallengeResult;
}


int8_t uValueTypeChallenge_GreaterThanEqualOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp)
{
	int8_t ChallengeResult = -1;

	switch (LeftValueOp->ValueType)
	{
		case VALUE_TYPE_INT8:
			ChallengeResult = (LeftValueOp->Value.TypeINT8 >= RightValueOp->Value.TypeINT8) ? 1:0;
		break;

		case VALUE_TYPE_UINT8:
			ChallengeResult = (LeftValueOp->Value.TypeUINT8 >= RightValueOp->Value.TypeUINT8) ? 1:0;
			break;

		case VALUE_TYPE_INT16:
			ChallengeResult = (LeftValueOp->Value.TypeINT16 >= RightValueOp->Value.TypeINT16) ? 1:0;
			break;

		case VALUE_TYPE_UINT16:
			ChallengeResult = (LeftValueOp->Value.TypeUINT16 >= RightValueOp->Value.TypeUINT16) ? 1:0;
			break;

		case VALUE_TYPE_INT32:
			ChallengeResult = (LeftValueOp->Value.TypeINT32 >= RightValueOp->Value.TypeINT32) ? 1:0;
			break;

		case VALUE_TYPE_UINT32:
			ChallengeResult = (LeftValueOp->Value.TypeUINT32 >= RightValueOp->Value.TypeUINT32) ? 1:0;
			break;

		case VALUE_TYPE_FLOAT:
			ChallengeResult = (LeftValueOp->Value.TypeFLOAT >= RightValueOp->Value.TypeFLOAT) ? 1:0;
			break;

	}

	return ChallengeResult;
}



int8_t uValueTypeChallenge_LessThanOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp)
{
	int8_t ChallengeResult = -1;

	switch (LeftValueOp->ValueType)
	{
		case VALUE_TYPE_INT8:
			ChallengeResult = (LeftValueOp->Value.TypeINT8 < RightValueOp->Value.TypeINT8) ? 1:0;
		break;

		case VALUE_TYPE_UINT8:
			ChallengeResult = (LeftValueOp->Value.TypeUINT8 < RightValueOp->Value.TypeUINT8) ? 1:0;
			break;

		case VALUE_TYPE_INT16:
			ChallengeResult = (LeftValueOp->Value.TypeINT16 < RightValueOp->Value.TypeINT16) ? 1:0;
			break;

		case VALUE_TYPE_UINT16:
			ChallengeResult = (LeftValueOp->Value.TypeUINT16 < RightValueOp->Value.TypeUINT16) ? 1:0;
			break;

		case VALUE_TYPE_INT32:
			ChallengeResult = (LeftValueOp->Value.TypeINT32 < RightValueOp->Value.TypeINT32) ? 1:0;
			break;

		case VALUE_TYPE_UINT32:
			ChallengeResult = (LeftValueOp->Value.TypeUINT32 < RightValueOp->Value.TypeUINT32) ? 1:0;
			break;

		case VALUE_TYPE_FLOAT:
			ChallengeResult = (LeftValueOp->Value.TypeFLOAT < RightValueOp->Value.TypeFLOAT) ? 1:0;
			break;

	}

	return ChallengeResult;
}



int8_t uValueTypeChallenge_LessThanEqualOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp)
{
	int8_t ChallengeResult = -1;

	switch (LeftValueOp->ValueType)
	{
		case VALUE_TYPE_INT8:
			ChallengeResult = (LeftValueOp->Value.TypeINT8 <= RightValueOp->Value.TypeINT8) ? 1:0;
		break;

		case VALUE_TYPE_UINT8:
			ChallengeResult = (LeftValueOp->Value.TypeUINT8 <= RightValueOp->Value.TypeUINT8) ? 1:0;
			break;

		case VALUE_TYPE_INT16:
			ChallengeResult = (LeftValueOp->Value.TypeINT16 <= RightValueOp->Value.TypeINT16) ? 1:0;
			break;

		case VALUE_TYPE_UINT16:
			ChallengeResult = (LeftValueOp->Value.TypeUINT16 <= RightValueOp->Value.TypeUINT16) ? 1:0;
			break;

		case VALUE_TYPE_INT32:
			ChallengeResult = (LeftValueOp->Value.TypeINT32 <= RightValueOp->Value.TypeINT32) ? 1:0;
			break;

		case VALUE_TYPE_UINT32:
			ChallengeResult = (LeftValueOp->Value.TypeUINT32 <= RightValueOp->Value.TypeUINT32) ? 1:0;
			break;

		case VALUE_TYPE_FLOAT:
			ChallengeResult = (LeftValueOp->Value.TypeFLOAT <= RightValueOp->Value.TypeFLOAT) ? 1:0;
			break;

	}

	return ChallengeResult;
}



int8_t uValueTypeChallenge_EqualOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp)
{
	int8_t ChallengeResult = -1;

	switch (LeftValueOp->ValueType)
	{
		case VALUE_TYPE_INT8:
			ChallengeResult = (LeftValueOp->Value.TypeINT8 == RightValueOp->Value.TypeINT8) ? 1:0;
		break;

		case VALUE_TYPE_UINT8:
			ChallengeResult = (LeftValueOp->Value.TypeUINT8 == RightValueOp->Value.TypeUINT8) ? 1:0;
			break;

		case VALUE_TYPE_INT16:
			ChallengeResult = (LeftValueOp->Value.TypeINT16 == RightValueOp->Value.TypeINT16) ? 1:0;
			break;

		case VALUE_TYPE_UINT16:
			ChallengeResult = (LeftValueOp->Value.TypeUINT16 == RightValueOp->Value.TypeUINT16) ? 1:0;
			break;

		case VALUE_TYPE_INT32:
			ChallengeResult = (LeftValueOp->Value.TypeINT32 == RightValueOp->Value.TypeINT32) ? 1:0;
			break;

		case VALUE_TYPE_UINT32:
			ChallengeResult = (LeftValueOp->Value.TypeUINT32 == RightValueOp->Value.TypeUINT32) ? 1:0;
			break;

		case VALUE_TYPE_FLOAT:
			ChallengeResult = (LeftValueOp->Value.TypeFLOAT == RightValueOp->Value.TypeFLOAT) ? 1:0;
			break;

	}

	return ChallengeResult;
}


/******************************************************
 *               Function Definitions
 ******************************************************/

uint16_t crc16_ccitt_kermit(uint8_t *pData, uint16_t len)
{
    uint16_t i = 0;
    uint16_t j = 0;
    uint16_t crc = CRC_KERMIT_PRESET;
    for (i = 0; i < len; i++) /* cnt = number of protocol bytes without CRC */
    {
        crc ^= pData[i];
        for (j = 0; j < 8; j++)
        {
            if (crc & 0x0001)
            {
                crc = (crc >> 1) ^ CRC_KERMIT_POLYNOM;
            }
            else
            {
                crc = (crc >> 1);
            }
        }
    }

    return crc;
}


uint16_t Crc16(uint8_t *pdata, int16_t nbytes, uint16_t crc)
{
	while (nbytes-- > 0)
	{
		CRC_INNER_LOOP(16, crc, *pdata++);
	}
	return crc;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
// Update CRC
uint16_t crc16_P8005_update(uint16_t crc, uint8_t a)
{
        // polynomial 0xA001 is the same as 0x8005
        // in reverse implementation
        uint8_t i;

        crc ^= (a << 8);
        for (i = 0; i < 8; i++)
        {
                if (crc & 0x8000)
                        crc = (crc << 1) ^ POLYNOMIAL;
                else
                        crc = (crc << 1);
        }

        return crc;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Check for valid frame, computes from pkt[0] to pkt[len-3] inclusive
uint8_t crc16_P8005_check(uint8_t* pkt, uint8_t len, uint16_t Seed)
{
        uint16_t  crc = Seed;
        uint16_t* end = (uint16_t*) &pkt[len - 2];
        uint8_t   i;

        for (i = 0; i < len - 2; i++)
                crc = crc16_P8005_update(crc, pkt[i]);

        return (*end == crc) ? 1 : 0;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Appends a CRC at the end of the frame, at pkt[len - 1] and pkt[len - 2]
void crc16_P8005_append(uint8_t* pkt, uint8_t len, uint16_t Seed)
{
        uint16_t  crc = Seed;
        uint16_t* end = (uint16_t*) &pkt[len - 2];
        uint8_t   i;

        for (i = 0; i < len - 2; i++)
                crc = crc16_P8005_update(crc, pkt[i]);

        *end = crc;
}

int
UtilsConvertMacToGMac( uint8_t mac[ 6 ], uint8_t gmac[ 8 ] )
{
	if ( mac && gmac ) {
		gmac[ 0 ] = mac[ 0 ];
		gmac[ 1 ] = mac[ 1 ];
		gmac[ 2 ] = mac[ 2 ];
		gmac[ 3 ] = 0xFF;
		gmac[ 4 ] = 0xFF;
		gmac[ 5 ] = mac[ 3 ];
		gmac[ 6 ] = mac[ 4 ];
		gmac[ 7 ] = mac[ 5 ];

		return 0;
	} else {
		return -1;
	}
}


extern uint8_t UtilsGetValueTypeSize(uint8_t ValueType)
{
	uint8_t ValueSizeInByte = 0;

	switch (ValueType)
	{
		case VALUE_TYPE_INT8:
			ValueSizeInByte = sizeof(int8_t);
			break;

		case VALUE_TYPE_UINT8:
			ValueSizeInByte = sizeof(uint8_t);
			break;

		case VALUE_TYPE_INT16:
			ValueSizeInByte = sizeof(int16_t);
			break;

		case VALUE_TYPE_UINT16:
			ValueSizeInByte = sizeof(uint16_t);
			break;

		case VALUE_TYPE_INT32:
			ValueSizeInByte = sizeof(int32_t);
			break;

		case VALUE_TYPE_UINT32:
			ValueSizeInByte = sizeof(uint32_t);
			break;

		case VALUE_TYPE_FLOAT:
			ValueSizeInByte = sizeof(float);
			break;

	}

	return ValueSizeInByte;
}



extern int8_t UtilsValueTypeChallengeWithOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp, uint8_t Operator)
{
	int8_t ChallengeResult = -1;

	if (LeftValueOp->ValueType == RightValueOp->ValueType)
	{
		switch (Operator)
		{
			case OPERATOR_GREATER_THAN:

				ChallengeResult = uValueTypeChallenge_GreaterThanOperator(LeftValueOp, RightValueOp);

				break;

			case OPERATOR_GREATER_THAN_EQUAL:

				ChallengeResult = uValueTypeChallenge_GreaterThanEqualOperator(LeftValueOp, RightValueOp);

				break;

			case OPERATOR_LESS_THAN:

				ChallengeResult = uValueTypeChallenge_LessThanOperator(LeftValueOp, RightValueOp);

				break;

			case OPERATOR_LESS_THAN_EQUAL:

				ChallengeResult = uValueTypeChallenge_LessThanEqualOperator(LeftValueOp, RightValueOp);

				break;

			case OPERATOR_EQUAL:

				ChallengeResult = uValueTypeChallenge_EqualOperator(LeftValueOp, RightValueOp);

				break;

		}

	}

	return ChallengeResult;
}


extern int _MACAddrToDecimalStr(char *buffer, int cbSize, uint8_t *macaddr, int macaddrlen)
{
    char szASCII[] = "0123456789";
    char szTempBuff[21];
    int i, j, r;
    unsigned long long mac = 0;

    if (buffer == NULL || macaddr == NULL || macaddrlen <= 0) //macaddrlen != 8)
    {
        return 0;
    }

    for (i = 0; i < macaddrlen; i++)
    {
        mac <<= 8;
        mac |= macaddr[i];
    }

    memset(szTempBuff, 0x00, sizeof(szTempBuff));

    i = sizeof(szTempBuff) - 1;
    j = 0;

    do
    {
        r = mac % 10;
        szTempBuff[i - 1] = szASCII[r];
        i--;
        mac /= 10L;
        j++;
    } while (mac);

    if (j > cbSize) return 0;

    memcpy(buffer, &szTempBuff[i], j);
    if (j < cbSize)
    {
        buffer[j] = 0x00;
    }

    return j;
}

extern int _DecimalStrToMACAddr(uint8_t *macaddr, int macaddrlen, char *data)
{
    unsigned long long mac = 0;
    int len = 0;                    // length of data
    int i = 0;
    int ret = 0;
    uint8_t ch = 0;

    if (macaddr == NULL ||
        macaddrlen < 8 ||
        data == NULL ||
        strlen(data) > 20)          // The maximum length of unsigned long long data is 20 bytes
    {
        return 0;
    }

    len = strlen(data);

    for (i = 0; i < len; i++)
    {
        ch = data[i];
        if (ch < '0' || ch > '9')
        {
            ret = 0;
            break;
        }

        mac = mac * 10;
        mac += (ch - '0');          //mac |= (ch - '0');    // Use '|=' operation will get the wrong value
        ret++;
    }

    if (ret == 0) return 0;         // there is a non numeric character in data buffer

    for (i = 0; i < 8; i++)
    {
        ch = mac % 256;
        macaddr[7 - i] = ch;
        mac /= 256;
    }

    return ret;
}

extern int _UINT64ToDecimalStr(char *buffer, int cbSize, uint64_t nU64Value)
{
    char szASCII[] = "0123456789";
    char szTempBuff[21];
    int i, j, r;

    if (buffer == NULL)
    {
        return 0;
    }

    memset(szTempBuff, 0x00, sizeof(szTempBuff));

    i = sizeof(szTempBuff) - 1;
    j = 0;

    do
    {
        r = nU64Value % 10;
        szTempBuff[i - 1] = szASCII[r];
        i--;
        nU64Value /= 10L;
        j++;
    } while (nU64Value);

    if (j > cbSize) return 0;

    memcpy(buffer, &szTempBuff[i], j);
    if (j < cbSize)
    {
        buffer[j] = 0x00;
    }

    return j;
}

extern int UtilsUUIDStrArrayCompare(uint8_t UUID128[16], uint8_t* StrUUID128)
{
    int Match = 1;
    uint8_t i = 0;
    uint8_t UUID128StrConvert[16];

    DKHex_ASCToHex((char *)StrUUID128, strlen((char *)StrUUID128), (char *)UUID128StrConvert, sizeof(UUID128StrConvert));

    for (i = 0; i < 16; i++)
    {
        if (UUID128[i] != UUID128StrConvert[i])
        {
            Match = 0;
            break;
        }
    }

    return Match;
}


#ifdef DK_GATEWAY

extern void*    mem_calloc_AlignmentPack(const char* name, size_t nelems, size_t elemsize)
{
    void* allocmem = NULL;
    uint16_t AlignSize = elemsize;
    uint16_t LeftOver = (AlignSize%GENERAL_MEM_ALLOC_SIZE_PACK_ALIGNMENT);
    if (LeftOver)
    {
        AlignSize = ((AlignSize/GENERAL_MEM_ALLOC_SIZE_PACK_ALIGNMENT) + 1)*GENERAL_MEM_ALLOC_SIZE_PACK_ALIGNMENT;
    }

    //G_SYS_DBG_LOG_V1( "mem_alloc_AlignmentPack pack aligned size %d\r\n", AlignSize );

    allocmem = dxMemAlloc( (char*)name, nelems, AlignSize);
    if (allocmem == NULL)
    {
        G_SYS_DBG_LOG_V1( "FATAL: mem_alloc_AlignmentPack pack aligned failed to allocate size %d\r\n", AlignSize );
    }
    else
    {
        memset(allocmem, 0, AlignSize*nelems);
    }

    return allocmem;
}

extern int32_t APSecurityTypeTranslateFromDxToApp(int32_t dxAPSecurityType)
{
    int32_t DkApSecurityType = AP_SECURITY_UNKNOWN;

    //Translate the access point security
    switch (dxAPSecurityType)
    {
        case DXWIFI_SECURITY_OPEN:
            DkApSecurityType = AP_SECURITY_OPEN;
        break;

        case DXWIFI_SECURITY_WEP_PSK:
            DkApSecurityType = AP_SECURITY_WEP_PSK;
        break;

        case DXWIFI_SECURITY_WEP_SHARED:
            DkApSecurityType = AP_SECURITY_WEP_SHARED;
        break;

        case DXWIFI_SECURITY_WPA_TKIP_PSK:
            DkApSecurityType = AP_SECURITY_WPA_TKIP_PSK;
        break;

        case DXWIFI_SECURITY_WPA_AES_PSK:
            DkApSecurityType = AP_SECURITY_WPA_AES_PSK;
        break;

        case DXWIFI_SECURITY_WPA2_AES_PSK:
            DkApSecurityType = AP_SECURITY_WPA2_AES_PSK;
        break;

        case DXWIFI_SECURITY_WPA2_TKIP_PSK:
            DkApSecurityType = AP_SECURITY_WPA2_TKIP_PSK;
        break;

        case DXWIFI_SECURITY_WPA2_MIXED_PSK:
            DkApSecurityType = AP_SECURITY_WPA2_MIXED_PSK;
        break;

        case DXWIFI_SECURITY_WPS_OPEN:
            DkApSecurityType = AP_SECURITY_WPS_OPEN;
        break;

        case DXWIFI_SECURITY_WPS_SECURE:
            DkApSecurityType = AP_SECURITY_WPS_SECURE;
        break;

        case DXWIFI_SECURITY_WPA_WPA2_MIXED:
            DkApSecurityType = AP_SECURITY_WPA_WPA2_MIXED;
        break;

        case DXWIFI_SECURITY_FORCE_32_BIT:
            DkApSecurityType = AP_SECURITY_FORCE_32_BIT;
        break;

        default:
            G_SYS_DBG_LOG_V1( "WARNING: APSecurityTypeTranslateFromDxToApp with unknown dxAPSecurityType (%d)\r\n", dxAPSecurityType );
        break;
    }

	return DkApSecurityType;

}

extern int32_t APSecurityTypeTranslateFromAppToDx(int32_t dxAPSecurityType)
{

    int32_t DkApSecurityType = DXWIFI_SECURITY_UNKNOWN;

    //Translate the access point security
    switch (dxAPSecurityType)
    {
        case AP_SECURITY_OPEN :
            DkApSecurityType = DXWIFI_SECURITY_OPEN;
        break;

        case AP_SECURITY_WEP_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WEP_PSK;
        break;

        case AP_SECURITY_WEP_SHARED :
            DkApSecurityType = DXWIFI_SECURITY_WEP_SHARED;
        break;

        case AP_SECURITY_WPA_TKIP_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WPA_TKIP_PSK;
        break;

        case AP_SECURITY_WPA_AES_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WPA_AES_PSK;
        break;

        case AP_SECURITY_WPA2_AES_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WPA2_AES_PSK;
        break;

        case AP_SECURITY_WPA2_TKIP_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WPA2_TKIP_PSK;
        break;

        case AP_SECURITY_WPA2_MIXED_PSK :
            DkApSecurityType = DXWIFI_SECURITY_WPA2_MIXED_PSK;
        break;

        case AP_SECURITY_WPA_WPA2_MIXED :
            DkApSecurityType = DXWIFI_SECURITY_WPA_WPA2_MIXED;
        break;

        case AP_SECURITY_WPS_OPEN :
            DkApSecurityType = DXWIFI_SECURITY_WPS_OPEN;
        break;

        case AP_SECURITY_WPS_SECURE :
            DkApSecurityType = DXWIFI_SECURITY_WPS_SECURE;
        break;

        case AP_SECURITY_FORCE_32_BIT :
            DkApSecurityType = DXWIFI_SECURITY_FORCE_32_BIT;
        break;

        default:
            G_SYS_DBG_LOG_V1( "WARNING: APSecurityTypeTranslateFromAppToDx with unknown DkAPSecurityType (%d)\r\n", DkApSecurityType );
        break;
	}

	return DkApSecurityType;
}



#endif //DK_GATEWAY

uint8_t IsLeapYear(uint32_t year)
{
    if (year % 4)
    {
        return 0;
    }

    if ((year % 100) == 0 && (year % 400) != 0)
    {
        return 0;
    }

    return 1;
}

extern DKTime_t ConvertUTCToDKTime(uint32_t utcsec)
{
    uint8_t ndayofmonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    DKTime_t dktime = { 0 };
    dktime.sec = utcsec % 60;

    utcsec -= dktime.sec;
    utcsec /= 60;                           // unit of utcsec is minute
    dktime.min = utcsec % 60;

    utcsec -= dktime.min;
    utcsec /= 60;                           // unit of utcsec is hour
    dktime.hour = utcsec % 24;

    utcsec -= dktime.hour;
    utcsec /= 24;                           // unit of utcsec is day

    dktime.year = 1970;
    dktime.dayofweek = 1 + (utcsec + 3) % 7;// 1970-01-01 is Thursday. Also, Monday(1), Tuesday(2), ... Saturday(6), Sunday(7)

    dktime.month = 1;
    dktime.day = 1;

    while (utcsec)
    {
        uint16_t ndayofyear = 365;
        if (IsLeapYear(dktime.year) == 1)
        {
            ndayofmonth[1] = 29;
            ndayofyear++;
        }
        else
        {
            ndayofmonth[1] = 28;
        }

        if (utcsec >= ndayofyear)
        {
            dktime.year++;
            utcsec -= ndayofyear;
        }
        else
        {
            uint16_t i = 0;
            for (i = 0; i < sizeof(ndayofmonth) / sizeof(ndayofmonth[0]); i++)
            {
                dktime.month = i + 1;
                if (utcsec <= ndayofmonth[i])
                {
                    dktime.day += utcsec;
                    if (dktime.day > ndayofmonth[i])
                    {
                        dktime.month++;
                        dktime.day = 1;
                    }
                    utcsec = 0;
                    break;
                }
                else
                {
                    utcsec -= ndayofmonth[i];
                }
            }
        }
    }

    return dktime;
}

extern DKTimeLocal_t ConvertUTCToDKTimeLocal (uint32_t utcsec)
{
    uint8_t ndayofmonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    DKTimeLocal_t localTime = { 0 };
    localTime.sec = utcsec % 60;

    utcsec -= localTime.sec;
    utcsec /= 60;                           // unit of utcsec is minute
    localTime.min = utcsec % 60;

    utcsec -= localTime.min;
    utcsec /= 60;                           // unit of utcsec is hour
    localTime.hour = utcsec % 24;

    utcsec -= localTime.hour;
    utcsec /= 24;                           // unit of utcsec is day

    localTime.year = 1970;
    localTime.dayofweek = 1 + (utcsec + 3) % 7;// 1970-01-01 is Thursday. Also, Monday(1), Tuesday(2), ... Saturday(6), Sunday(7)

    localTime.month = 1;
    localTime.day = 1;

    while (utcsec)
    {
        uint16_t ndayofyear = 365;
        if (IsLeapYear(localTime.year) == 1)
        {
            ndayofmonth[1] = 29;
            ndayofyear++;
        }
        else
        {
            ndayofmonth[1] = 28;
        }

        if (utcsec >= ndayofyear)
        {
            localTime.year++;
            utcsec -= ndayofyear;
        }
        else
        {
            uint16_t i = 0;
            for (i = 0; i < sizeof(ndayofmonth) / sizeof(ndayofmonth[0]); i++)
            {
                localTime.month = i + 1;
                if (utcsec <= ndayofmonth[i])
                {
                    localTime.day += utcsec;
                    if (localTime.day > ndayofmonth[i])
                    {
                        localTime.month++;
                        localTime.day = 1;
                    }
                    utcsec = 0;
                    break;
                }
                else
                {
                    utcsec -= ndayofmonth[i];
                }
            }
        }
    }

    return localTime;
}

extern uint32_t ConvertDKTimeToUTC(DKTime_t dktime)
{
    uint8_t ndayofmonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    uint32_t utc = 0;
    uint16_t i = 0;

    for (i = 1970; i < dktime.year; i++)
    {
        uint16_t ndayofyear = 365;
        if (IsLeapYear(i) == 1)
        {
            ndayofmonth[1] = 29;
            ndayofyear++;
        }
        else
        {
            ndayofmonth[1] = 28;
        }

        utc += ndayofyear;
    }

    if (IsLeapYear(dktime.year) == 1)
    {
        ndayofmonth[1] = 29;
    }
    else
    {
        ndayofmonth[1] = 28;
    }

    for (i = 0; i < sizeof(ndayofmonth) / sizeof(ndayofmonth[0]); i++)
    {
        if (i + 1 < dktime.month)
        {
            utc += ndayofmonth[i];
        }
        else
        {
            utc += dktime.day - 1;
            break;
        }
    }

    utc *= 24;              // Convert to hours
    utc += dktime.hour;

    utc *= 60;              // Conver to minutes
    utc += dktime.min;

    utc *= 60;              // Convert to seconds
    utc += dktime.sec;

    return utc;
}

extern uint32_t ConvertDKTimeLocalToUTC(DKTimeLocal_t localTime)
{
    uint8_t ndayofmonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    uint32_t utc = 0;
    uint16_t i = 0;

    for (i = 1970; i < localTime.year; i++)
    {
        uint16_t ndayofyear = 365;
        if (IsLeapYear(i) == 1)
        {
            ndayofmonth[1] = 29;
            ndayofyear++;
        }
        else
        {
            ndayofmonth[1] = 28;
        }

        utc += ndayofyear;
    }

    if (IsLeapYear(localTime.year) == 1)
    {
        ndayofmonth[1] = 29;
    }
    else
    {
        ndayofmonth[1] = 28;
    }

    for (i = 0; i < sizeof(ndayofmonth) / sizeof(ndayofmonth[0]); i++)
    {
        if (i + 1 < localTime.month)
        {
            utc += ndayofmonth[i];
        }
        else
        {
            utc += localTime.day - 1;
            break;
        }
    }

    utc *= 24;              // Convert to hours
    utc += localTime.hour;

    utc *= 60;              // Conver to minutes
    utc += localTime.min;

    utc *= 60;              // Convert to seconds
    utc += localTime.sec;

    return utc;
}
