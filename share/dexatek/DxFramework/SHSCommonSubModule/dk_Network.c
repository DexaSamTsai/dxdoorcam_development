//============================================================================
// File: dk_network.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#ifdef __IAR_SYSTEMS_ICC__
#include <platform/platform_stdlib.h>
#else // __IAR_SYSTEMS_ICC__
#include <stdlib.h>
#endif // __IAR_SYSTEMS_ICC__
#include "dk_Network.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Local Function Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

DKJSONTokenTable *_ConvertTokenString(int n)
{
    int i = 0;
    int nCount = sizeof(DKJSONKeyTable) / sizeof(DKJSONKeyTable[0]);
    DKJSONTokenTable *p = NULL;
    char bFound = 0;

    for (i = 0; i < nCount; i++)
    {
        p = (DKJSONTokenTable *)&DKJSONKeyTable[i];

        if (p == NULL) break;

        if (p->id == n)
        {
            bFound = 1;
            break;
        }
    }

    if (bFound == 0) return NULL;

    return p;
}


