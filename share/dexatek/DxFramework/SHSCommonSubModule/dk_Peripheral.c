//============================================================================
// File: dk_peripheral.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "dk_Peripheral.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Local Function Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

uint8_t DKPeripheral_GetDataTypeLenForDataTypeID(uint8_t DataTypeID)
{
    uint8_t DataTypeLen = 0;

    //TODO: We should revise this, maybe it's a good idea to obtain this data from server instead of hardcoding
    switch (DataTypeID)
    {
        case DK_DATA_TYPE_FIRMWARE_VERSION:
        case DK_DATA_TYPE_SHORT_RELATIVE_HUMILITY:
        case DK_DATA_TYPE_REGISTERED_ID:
        case DK_DATA_TYPE_LIGHT_BULB_DIMMER:
        case DK_DATA_TYPE_DOOR_STATUS:
        case DK_DATA_TYPE_POWER_PLUG_STATE:
        case DK_DATA_TYPE_AC_SOURCE_FREQUENCY:
        case DK_DATA_TYPE_SMOKE_DETECTOR_STATUS:
        case DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_STATUS:
        case DK_DATA_TYPE_INLET_SWITCH_STATUS:
        case DK_DATA_TYPE_MOTION_SENSE_STATUS:
        case DK_DATA_TYPE_MOTION_SENSE_SENSITIVITY:
        case DK_DATA_TYPE_MOTION_SENSE_TRIGGER_DELAY_CTRL:
        case DK_DATA_TYPE_THERMOSTAT_CB_CURRENT_AMB_TEMP:
        case DK_DATA_TYPE_THERMOSTAT_CB_STATUS:
        case DK_DATA_TYPE_THERMOSTAT_CB_DATA_SYNCH_ID:
        case DK_DATA_TYPE_THERMOSTAT_CB_ENERGY_SAV_TEMP:
        case DK_DATA_TYPE_THERMOSTAT_CB_HEATING_TEMP:
        case DK_DATA_TYPE_THERMOSTAT_CB_SETPOINT_TEMP:
        case DK_DATA_TYPE_POWER_SOCKET_A_IN_USE:
        case DK_DATA_TYPE_POWER_SOCKET_B_IN_USE:
        case DK_DATA_TYPE_POWER_SOCKET_A_STATUS:
        case DK_DATA_TYPE_POWER_SOCKET_B_STATUS:
        case DK_DATA_TYPE_POWER_SOCKET_SAFETY_MODE:
        case DK_DATA_TYPE_DOOR_LOCK_STATUS:
        case DK_DATA_TYPE_SIREN_STATUS:
        case DK_DATA_TYPE_REMOTE_CONTROL_STATUS:
        case DK_DATA_TYPE_RTC_STATUS:
        case DK_DATA_TYPE_RECORDING_EVENT:
        case DK_DATA_TYPE_ALARM_SYSTEM_STATE:
        case DK_DATA_TYPE_POWER_ADAPTER_STATUS:
        case DK_DATA_TYPE_REPEATER_NUM_CONDITIONS_HOLD:
        case DK_DATA_TYPE_REPEATER_NUM_TOTAL_CONDITIONS:
        case DK_DATA_TYPE_IMPLICIT_DEV:
        case DK_DATA_TYPE_INTANK_LEVEL:
        case DK_DATA_TYPE_PAIRING_KEY_STATUS:
        case DK_DATA_TYPE_DOS_ATTACK_STATUS:
        case DK_DATA_TYPE_FILTER_CLEANING_NOTIFICATION:
        case DK_DATA_TYPE_LIGHT_SATURATION_VALUE:
            DataTypeLen = 1;
            break;


        case DK_DATA_TYPE_TEMPERATURE:
        case DK_DATA_TYPE_ATMOSPHERIC_PRESSURE:
        case DK_DATA_TYPE_RELATIVE_HUMILITY:
        case DK_DATA_TYPE_AIR_QUALITY_VOC:
        case DK_DATA_TYPE_VOLTAGE_RMS:
        case DK_DATA_TYPE_SHOCK_SENSE_ZFORCE_THRESHOLD:
        case DK_DATA_TYPE_PM25_VALUE:
        case DK_DATA_TYPE_NOISE_DB:
        case DK_DATA_TYPE_LIGHT_LUX:
        case DK_DATA_TYPE_CO2_CONCENTRATION:
        case DK_DATA_TYPE_VOC_CONCENTRATION:
        case DK_DATA_TYPE_FILTER_CLEANING_TIME_ACCUMULATION:
        case DK_DATA_TYPE_CURRENT_STATUS_INFO_CRC16:
        case DK_DATA_TYPE_DEVICE_INFO:
        case DK_DATA_TYPE_LIGHT_HUE_VALUE:
        case DK_DATA_TYPE_ERGO_DESK_STATUS:
            DataTypeLen = 2;
            break;

        case DK_DATA_TYPE_USERPASSCODE:
        case DK_DATA_TYPE_POWER_IN_WATT:
        case DK_DATA_TYPE_SECURITY_MASK:
        case DK_DATA_TYPE_SHOCK_SENSE_DETECTOR_ZFORCE:
        case DK_DATA_TYPE_RTC_TIME_UTC0:
        case DK_DATA_TYPE_LIGHT_BULB_COLOUR:
        case DK_DATA_TYPE_ERGO_DESK_AGGREGATED_TIME:
        case DK_DATA_TYPE_ERGO_DESK_UPDATE_TIME:
            DataTypeLen = 4;
            break;

        default:
#ifdef DK_GATEWAY
            //G_SYS_DBG_LOG_V1(  "WARNING: Unknown DataType found (0X%x)\r\n", DataTypeID );
#endif
            break;
    }


    return DataTypeLen;

}


uint8_t* DkPeripheral_GetDataWithDataTypeIDExt(uint8_t DataTypeID, uint16_t AdDataTypeInfoLen, uint8_t* AdvDataTypeInfoContainer)
{
	uint8_t* pDataTypeData = NULL;

	int16_t Index = DkPeripheral_GetInfoContainerIndexOfDataTypeIDExt(DataTypeID, AdDataTypeInfoLen, AdvDataTypeInfoContainer);

	if (Index >= 0)
	{
		pDataTypeData = &AdvDataTypeInfoContainer[Index+1];
	}

	return pDataTypeData;
}


int16_t DkPeripheral_GetInfoContainerIndexOfDataTypeIDExt(uint8_t DataTypeID, uint16_t AdDataTypeInfoLen, uint8_t* AdvDataTypeInfoContainer)
{
	uint8_t Found = 0;
	int16_t DataTypeDataIndex = 0;
	for (DataTypeDataIndex = 0; DataTypeDataIndex < AdDataTypeInfoLen; DataTypeDataIndex++)
	{
		if (AdvDataTypeInfoContainer[DataTypeDataIndex] == DataTypeID)
		{
			Found = 1;
			break;
		}
		else
		{
			if (AdvDataTypeInfoContainer[DataTypeDataIndex] == 0)
				continue;

			//Check the dataType and Skip to the next DataType
			DataTypeDataIndex += DKPeripheral_GetDataTypeLenForDataTypeID(AdvDataTypeInfoContainer[DataTypeDataIndex]);
		}
	}

	if (!Found)
	{
		DataTypeDataIndex = -1;
#ifdef DK_GATEWAY
		//G_SYS_DBG_LOG_V1(  "WARNING: DataType(0X%x) NOT Found!\r\n", DataTypeID );
#endif
	}

	return DataTypeDataIndex;

}

uint8_t IntUUIDIsEqual(uint8_t *pASC, uint8_t *pByte, int cbByteLen)
{
    if (pASC == NULL ||
        pByte == NULL ||
        cbByteLen <= 0) {
        return 0;
    }

    if (cbByteLen * 2 != strlen(pASC)) {
        return 0;
    }

    uint8_t i = 0;
    uint8_t ch = 0;
    uint8_t d = 0;

    while (i < cbByteLen) {

        ch = (pByte[i] >> 4) & 0x0F;
        ch += 0x30;

        if (ch > '9') {
            ch += 7;
        }

        d = pASC[i * 2];

        if (d >= 'a' && d <= 'z') {
            d = d - 'a' + 'A';
        }

        if (ch != d) {
        	return 0;
        }

        ch = (pByte[i]) & 0x0F;
        ch += 0x30;

        if (ch > '9') {
            ch += 7;
        }

        d = pASC[i * 2 + 1];

        if (d >= 'a' && d <= 'z') {
            d = d - 'a' + 'A';
        }

        if (ch != d) {
        	return 0;
        }

        i++;
    }

    return 1;
}

uint8_t DkPeripheral_IsUuidMatch(uint8_t* uuid_string, uint8_t* uuid_bin)
{
    return IntUUIDIsEqual(uuid_string, uuid_bin, 16);
}
