//============================================================================
// File: dk_peripheralDesk.c
//
// Author: Alin Su
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dk_PeripheralDesk.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define TAISEIA_REGISTRATION_PACKET_LENGTH_INDEX            0
#define TAISEIA_REGISTRATION_PACKET_CLASS_ID_INDEX          2
#define TAISEIA_REGISTRATION_PACKET_VERSION_MAJOR_INDEX     3
#define TAISEIA_REGISTRATION_PACKET_VERSION_MINOR_INDEX     4
#define TAISEIA_REGISTRATION_PACKET_SA_TYPEID_INDEX         6
#define TAISEIA_REGISTRATION_PACKET_BRAND_NAME_INDEX        8

#define TAISEIA_SERVICE_PACKET_LENGTH_INDEX                 0
#define TAISEIA_SERVICE_PACKET_TYPE_ID_INDEX                1
#define TAISEIA_SERVICE_PACKET_SERVICE_ID_INDEX             2

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{
    uint8_t     ServiceID;
    uint8_t     DataHighByte;
    uint8_t     DataLowByte;

} TaiSEIAGenServiceInfo_t;



/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *            Local Function Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/

/******************************************************
 *              Parse Supported Functions
 ******************************************************/
ErgoHeightPacket_t Desk_ParseHeightPackedPayload(uint16_t PacketLen, void* PayloadPacket)
{
    ErgoHeightPacket_t HeightInfo;
    uint8_t *pParseData;

    memset((uint8_t *) &HeightInfo, 0 , sizeof(ErgoHeightPacket_t));

    pParseData = (uint8_t *) PayloadPacket;

    if(pParseData && PacketLen >= sizeof(ErgoHeightPacket_t))
    {
        HeightInfo.SitHeight = *(pParseData + 0);
        HeightInfo.StandHeight = *(pParseData + 1);
    }

    return HeightInfo;
}

ErgoReminderPacket_t Desk_ParseReminderPackedPayload(uint16_t PacketLen, void* PayloadPacket)
{
    ErgoReminderPacket_t ReminderInfo;
    uint8_t *pParseData;

    memset((uint8_t *) &ReminderInfo, 0 , sizeof(ErgoReminderPacket_t));

    pParseData = (uint8_t *) PayloadPacket;

    if(pParseData && PacketLen >= sizeof(ErgoHeightPacket_t))
    {
        ReminderInfo.Sit = *(pParseData + 0);
        ReminderInfo.Stand = *(pParseData + 1);
        ReminderInfo.Target = *((uint16_t *) (pParseData + 2));
        ReminderInfo.Tolerance = *(pParseData + 4);
    }

    return ReminderInfo;
}

/******************************************************
 *     WriteAccess Converter Function Declarations
 ******************************************************/
ErgoDeskPayload_t Desk_ConvertHeightPacketPayload(ErgoHeightStatusPacket_t HeightStatus)
{
    ErgoDeskPayload_t Payload;

    memset(&Payload, 0, sizeof(ErgoDeskPayload_t));

    Payload.PackedPayloadLen = sizeof(ErgoHeightStatusPacket_t);

    uint8_t* pWriteData = Payload.PackedPayload;
    memcpy(pWriteData, &HeightStatus, sizeof(ErgoHeightStatusPacket_t));

    return Payload;
}

ErgoDeskPayload_t Desk_ConvertReminderPacketPayload(ErgoReminderPacket_t Reminder)
{
    ErgoDeskPayload_t Payload;

    memset(&Payload, 0, sizeof(ErgoDeskPayload_t));

    Payload.PackedPayloadLen = sizeof(ErgoReminderPacket_t);

    uint8_t* pWriteData = Payload.PackedPayload;
    memcpy(pWriteData, &Reminder, sizeof(ErgoReminderPacket_t));

    return Payload;
}