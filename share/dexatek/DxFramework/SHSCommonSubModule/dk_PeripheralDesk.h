//============================================================================
// File: dk_peripheralDesk.h
//
// Author: Alin Su
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef BLE_PERIPHERAL_DESK_INFO_H
#define BLE_PERIPHERAL_DESK_INFO_H

#pragma once

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "dk_Peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Defines
 ******************************************************/
#define DESK_DEFAULT_HEIGHT_SIT             70
#define DESK_DEFAULT_HEIGHT_STAND           100

// (stand - sit) most larger than DESK_HEIGHT_DIFF
#define DESK_HEIGHT_DIFF                    10

#define DESK_REMINDER_SIT_TIME_MIN          30
#define DESK_REMINDER_STAND_TIME_MIN        10
#define DESK_REMINDER_TARGET_DAY_TIME_MIN   60
#define DESK_REMINDER_TOLERANCE_TIME_MIN    5
/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/
enum
{
    HEIGHT_NOT_CHANGE       = 0,    //Keep container current config
    HEIGHT_UPDATE,                  //Device gets current height then update to container
    HEIGHT_RESET                    //Reset to default
};

enum
{
    OCCUPY_OUT              = 0,    //Out of range
    OCCUPY_SIT,                     //In the range and sit
    OCCUPY_STAND                    //In the range and stand
};

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
typedef struct
{
    uint16_t BaseElapsedTime : 14;
    uint16_t Occupy : 2;
}ErgoADVStatus_t;

typedef struct
{
    uint16_t BaseAggStandTime;
    uint16_t BaseAggSitTime;
}ErgoADVBaseAggTime_t;

typedef struct
{
    uint8_t     Sit;
    uint8_t     Stand;
}
ErgoHeightStatusPacket_t;

typedef struct
{
    uint8_t     SitHeight;
    uint8_t     StandHeight;
}
ErgoHeightPacket_t;

typedef struct
{
    uint8_t     Sit;
    uint8_t     Stand;
    uint16_t    Target; //Stand target in one day
    uint8_t     Tolerance;
}
ErgoReminderPacket_t;

typedef struct
{
    uint16_t    PackedPayloadLen;
    uint8_t     PackedPayload[6];
} ErgoDeskPayload_t;

/******************************************************
 *              Parse Supported Functions
 ******************************************************/
ErgoHeightPacket_t Desk_ParseHeightPackedPayload(uint16_t PacketLen, void* PayloadPacket);

ErgoReminderPacket_t Desk_ParseReminderPackedPayload(uint16_t PacketLen, void* PayloadPacket);

/******************************************************
 *     WriteAccess Converter Function Declarations
 ******************************************************/
ErgoDeskPayload_t Desk_ConvertHeightPacketPayload(ErgoHeightStatusPacket_t HeightStatus);

ErgoDeskPayload_t Desk_ConvertReminderPacketPayload(ErgoReminderPacket_t Reminder);

#endif //BLE_PERIPHERAL_DESK_INFO_H