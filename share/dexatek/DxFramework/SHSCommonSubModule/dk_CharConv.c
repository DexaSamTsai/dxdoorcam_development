//============================================================================
// File: dk_ContStore.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/05/09
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/05/26
//     1) Description: Revise to fix a potential crash issue
//        Modified:
//            - CHARC_UTF8toLCDString()
//
//     2) Description: Added new function to convert string from \uNNNN to UTF-8.
//                     More information, refer to RFC5137, https://tools.ietf.org/pdf/rfc5137.pdf
//        Added:
//            - CHARC_EscapedFormChartoUNCODE()
//            - CHARC_UNICODEtoUTF8Char()
//            - CHARC_UTF8CharLength()
//            - CHARC_EscapedFormtoUTF8String()
//============================================================================
#include "dk_CharConv.h"

//============================================================================
// Defines and Macro
//============================================================================

#define CHARCONV_DEFAULT_CHAR           0x5F    // '_'

//============================================================================
// Static Variables
//============================================================================

#if STORE_VARIABLE_IN_SDRAM
#pragma default_variable_attributes = @ ".sdram.text"
#endif // STORE_VARIABLE_IN_SDRAM
static uint8_t m_nUTF16toLCD[] =
{
 // UTF-16
    // VITECH VO1626-KBW LCD character
 // 0x0020, 0x0021, 0x0022, 0x0023, 0x0024, 0x0025, 0x0026, 0x0027, 0x0028, 0x0029, 0x002A, 0x002B, 0x002C, 0x002D, 0x002E, 0x002F,
    0x20,   0x21,   0x22,   0x23,   0x24,   0x25,   0x26,   0x27,   0x28,   0x29,   0x2A,   0x2B,   0x2C,   0x2D,   0x2E,   0x2F,
 // 0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038, 0x0039, 0x003A, 0x003B, 0x003C, 0x003D, 0x003E, 0x003F,
    0x30,   0x31,   0x32,   0x33,   0x34,   0x35,   0x36,   0x37,   0x38,   0x39,   0x3A,   0x3B,   0x3C,   0x3D,   0x3E,   0x3F,
 // 0x0040, 0x0041, 0x0042, 0x0043, 0x0044, 0x0045, 0x0046, 0x0047, 0x0048, 0x0049, 0x004A, 0x004B, 0x004C, 0x004D, 0x004E, 0x004F,
    0x40,   0x41,   0x42,   0x43,   0x44,   0x45,   0x46,   0x47,   0x48,   0x49,   0x4A,   0x4B,   0x4C,   0x4D,   0x4E,   0x4F,
 // 0x0050, 0x0051, 0x0052, 0x0053, 0x0054, 0x0055, 0x0056, 0x0057, 0x0058, 0x0059, 0x005A, 0x005B, 0x005C, 0x005D, 0x005E, 0x005F,
    0x50,   0x51,   0x52,   0x53,   0x54,   0x55,   0x56,   0x57,   0x58,   0x59,   0x5A,   0x5B,   0x5C,   0x5D,   0x00,   0x5F,
 // 0x0060, 0x0061, 0x0062, 0x0063, 0x0064, 0x0065, 0x0066, 0x0067, 0x0068, 0x0069, 0x006A, 0x006B, 0x006C, 0x006D, 0x006E, 0x006F,
    0x00,   0x61,   0x62,   0x63,   0x64,   0x65,   0x66,   0x67,   0x68,   0x69,   0x6A,   0x6B,   0x6C,   0x6D,   0x6E,   0x6F,
 // 0x0070, 0x0071, 0x0072, 0x0073, 0x0074, 0x0075, 0x0076, 0x0077, 0x0078, 0x0079, 0x007A, 0x007B, 0x007C, 0x007D, 0x007E, 0x007F,
    0x70,   0x71,   0x72,   0x73,   0x74,   0x75,   0x76,   0x77,   0x78,   0x79,   0x7A,   0x7B,   0x7C,   0x7D,   0x00,   0x00,

 // 0x00A0, 0x00A1, 0x00A2, 0x00A3, 0x00A4, 0x00A5, 0x00A6, 0x00A7, 0x00A8, 0x00A9, 0x00AA, 0x00AB, 0x00AC, 0x00AD, 0x00AE, 0x00AF,
    0x00,   0xE9,   0xE4,   0xE5,   0x00,   0xE6,   0x00,   0x12,   0xF1,   0x00,   0x9D,   0xFB,   0x00,   0x00,   0x00,   0xFF,
 // 0x00B0, 0x00B1, 0x00B2, 0x00B3, 0x00B4, 0x00B5, 0x00B6, 0x00B7, 0x00B8, 0x00B9, 0x00BA, 0x00BB, 0x00BC, 0x00BD, 0x00BE, 0x00BF,
    0xF2,   0x00,   0x00,   0x00,   0xF4,   0x00,   0x13,   0x0D,   0x00,   0x00,   0x9E,   0xFC,   0xF6,   0xF5,   0x00,   0x9F,
 // 0x00C0, 0x00C1, 0x00C2, 0x00C3, 0x00C4, 0x00C5, 0x00C6, 0x00C7, 0x00C8, 0x00C9, 0x00CA, 0x00CB, 0x00CC, 0x00CD, 0x00CE, 0x00CF,
    0x00,   0x00,   0x00,   0xEA,   0x8E,   0x8F,   0x92,   0x80,   0x00,   0x90,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
 // 0x00D0, 0x00D1, 0x00D2, 0x00D3, 0x00D4, 0x00D5, 0x00D6, 0x00D7, 0x00D8, 0x00D9, 0x00DA, 0x00DB, 0x00DC, 0x00DD, 0x00DE, 0x00DF,
    0x00,   0x00,   0x00,   0x00,   0x00,   0xEC,   0x99,   0xF7,   0xEE,   0x00,   0x00,   0x00,   0x9A,   0x00,   0x00,   0x00,
 // 0x00E0, 0x00E1, 0x00E2, 0x00E3, 0x00E4, 0x00E5, 0x00E6, 0x00E7, 0x00E8, 0x00E9, 0x00EA, 0x00EB, 0x00EC, 0x00ED, 0x00EE, 0x00EF,
    0x85,   0xE0,   0x83,   0xEB,   0x84,   0x00,   0x91,   0x87,   0x8A,   0x82,   0x88,   0x89,   0x8D,   0xE1,   0x8C,   0x8B,
 // 0x00F0, 0x00F1, 0x00F2, 0x00F3, 0x00F4, 0x00F5, 0x00F6, 0x00F7, 0x00F8, 0x00F9, 0x00FA, 0x00FB, 0x00FC, 0x00FD, 0x00FE, 0x00FF,
    0x00,   0x9B,   0x95,   0xE2,   0x93,   0xED,   0x94,   0xF8,   0xEF,   0x97,   0xE3,   0x96,   0x81,   0x00,   0x00,   0x00,
};
#if STORE_VARIABLE_IN_SDRAM
#pragma default_variable_attributes =
#endif // STORE_VARIABLE_IN_SDRAM

//============================================================================
// External Functions
//============================================================================
extern int _IsHexDigit(char ch);

//============================================================================
// Functions
//============================================================================
/**
 * @fn uint32_t CHARC_EscapedFormChartoUNCODE(uint32_t *u_ch, uint8_t *u8_ch, uint32_t u8_ch_len)
 * @author David Tang
 * @date 2017/05/26
 */
uint32_t CHARC_EscapedFormChartoUNCODE(uint32_t *u_ch, uint8_t *u8_ch, uint32_t u8_ch_len)
{
    uint32_t nBytes = 0;

    if (u8_ch == NULL ||
        u_ch == NULL ||
        u8_ch_len < 6)
    {
        return 0;
    }

    if ((u8_ch[0] == '\\') &&
        (u8_ch[1] == 'U' || u8_ch[1] == 'u'))
    {
        int ch1 = _IsHexDigit(u8_ch[2]);
        int ch2 = _IsHexDigit(u8_ch[3]);
        int ch3 = _IsHexDigit(u8_ch[4]);
        int ch4 = _IsHexDigit(u8_ch[5]);

        if (ch1 >= 0 && ch1 <= 0x0F &&
            ch2 >= 0 && ch2 <= 0x0F &&
            ch3 >= 0 && ch3 <= 0x0F &&
            ch4 >= 0 && ch4 <= 0x0F)
        {
            uint32_t hi = (ch1 << 12) | (ch2 << 8) | (ch3 << 4) | (ch4);

            if (hi >= 0x0D800 && hi <= 0x0DBFF)
            {
                if (u8_ch[6] == '\\' &&
                    (u8_ch[7] == 'U' || u8_ch[7] == 'u') &&
                    u8_ch_len >= 12)
                {
                    int ch5 = _IsHexDigit(u8_ch[8]);
                    int ch6 = _IsHexDigit(u8_ch[9]);
                    int ch7 = _IsHexDigit(u8_ch[10]);
                    int ch8 = _IsHexDigit(u8_ch[11]);

                    if (ch5 >= 0 && ch5 <= 0x0F &&
                        ch6 >= 0 && ch6 <= 0x0F &&
                        ch7 >= 0 && ch7 <= 0x0F &&
                        ch8 >= 0 && ch8 <= 0x0F)
                    {
                        uint32_t lo = (ch5 << 12) | (ch6 << 8) | (ch7 << 4) | (ch8);
                        if (lo >= 0x0DC00 && lo <= 0x0DFFF)
                        {
                            *u_ch = 0x10000 + (((hi - 0xD800) << 10) | (lo & 0x3FF));
                            nBytes = 12;
                        }
                        else
                        {
                            nBytes = 0;
                        }
                    }
                    else
                    {
                        nBytes = 0;
                    }
                }
                else
                {
                    nBytes = 0;
                }
            }
            else
            {
                nBytes = 6;
                *u_ch = hi;
            }
        }
    }

    return nBytes;
}

/**
 * @fn uint8_t CHARC_UTF16toLCDChar(uint16_t nCode)
 * @author David Tang
 * @date 2017/05/08
 */
uint8_t CHARC_UTF16toLCDChar(uint16_t nCode)
{
    uint8_t nRC = 0;
    uint16_t nIndexOffset = 0;

    if (nCode < 0x20) return CHARCONV_DEFAULT_CHAR;

    // 0x20 - 0x7F
    nIndexOffset = nCode - 0x20;

    if (nCode <= 0x7F)
    {
        nRC = m_nUTF16toLCD[nIndexOffset];

        if (nRC == 0x00)
        {
            nRC = CHARCONV_DEFAULT_CHAR;
        }

        return nRC;
    }

    // 0xA0 - 0xFF
    nIndexOffset -= 0x20;

    if (nCode < 0x100)
    {
        nRC = m_nUTF16toLCD[nIndexOffset];

        if (nRC == 0x00)
        {
            nRC = CHARCONV_DEFAULT_CHAR;
        }

        return nRC;
    }

    nRC = CHARCONV_DEFAULT_CHAR;

    return nRC;
}

/**
 * @fn uint8_t CHARC_UTF8toLCDChar(uint8_t *pUTF8Char, uint8_t *pcbLen)
 * @author David Tang
 * @date 2017/05/26
 */
uint8_t CHARC_UTF8toLCDChar(uint8_t *pUTF8Char, uint8_t *pcbLen)
{
    uint32_t nUNICODE = 0;

    if (pUTF8Char == NULL)
    {
        return 0;
    }

    *pcbLen = 1;

    if (pUTF8Char[0] <= 0x7F)
    {
        //U+0000 to U+007F
        nUNICODE = pUTF8Char[0];

    }
    else if ((pUTF8Char[0] & 0xF8) == 0xF0)
    {
        //U+10000 to U+10FFFF
        if ((pUTF8Char[1] & 0xC0) == 0x80 && (pUTF8Char[2] & 0xC0) == 0x80 && (pUTF8Char[3] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0x8) << 2) | ((pUTF8Char[1] & 0x30) >> 4);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[1] & 0xF) << 4) | ((pUTF8Char[2] & 0x3C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[2] & 0x3) << 6) | (pUTF8Char[3] & 0x3F);

            *pcbLen = 4;
        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }
    else if ((pUTF8Char[0] & 0xF0) == 0xE0)
    {
        //U+0800 to U+FFFF
        if ((pUTF8Char[1] & 0xC0) == 0x80 && (pUTF8Char[2] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0xF) << 4) | ((pUTF8Char[1] & 0x3C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[1] & 0x3) << 6) | (pUTF8Char[2] & 0x3F);

            *pcbLen = 3;

        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }
    else if ((pUTF8Char[0] & 0xE0) == 0xC0)
    {
        //U+0080 to U+07FF
        if ((pUTF8Char[1] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0x1C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[0] & 0x3) << 6) | (pUTF8Char[1] & 0x3F);

            *pcbLen = 2;

        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }

    if (nUNICODE > 0xFFFF)
    {
        nUNICODE = CHARCONV_DEFAULT_CHAR;
    }

    return CHARC_UTF16toLCDChar((uint16_t)(nUNICODE & 0x0FFFF));
}

/**
 * \fn dxBOOL CHARC_UNICODEtoUTF8Char(uint8_t *pDst, uint32_t *pcbDstLen, uint32_t u_ch)
 * \author David Tang
 * \date 2017/05/26
 */
dxBOOL CHARC_UNICODEtoUTF8Char(uint8_t *pDst, uint32_t *pcbDstLen, uint32_t u_ch)
{
    uint32_t i = 0;
    uint8_t ch = 0;
    dxBOOL bRC = dxFALSE;

    if (pDst == NULL ||
        pcbDstLen == NULL)
    {
        return dxFALSE;
    }

    if (u_ch < 0x0080)
    {
        if (*pcbDstLen > 1)
        {
            pDst[i++] = (uint8_t)(u_ch & 0x0FF);

            *pcbDstLen = 1;
            bRC = dxTRUE;
        }
    }
    else if (u_ch >= 0x0080 && u_ch < 0x0800)
    {
        if (*pcbDstLen > 2)
        {
            ch = (uint8_t)((u_ch >> 6) & 0x03F);
            pDst[i++] = ch | 0xC0;
            ch = (uint8_t)(u_ch & 0x03F);
            pDst[i++] = ch | 0x80;

            *pcbDstLen = 2;
            bRC = dxTRUE;
        }
    }
    else if (u_ch >= 0x0800 && u_ch < 0x0FFFF)
    {
        if (*pcbDstLen > 3)
        {
            ch = (uint8_t)((u_ch >> 12) & 0x03F);
            pDst[i++] = ch | 0xE0;
            ch = (uint8_t)((u_ch >> 6) & 0x03F);
            pDst[i++] = ch | 0x80;
            ch = (uint8_t)(u_ch & 0x03F);
            pDst[i++] = ch | 0x80;

            *pcbDstLen = 3;
            bRC = dxTRUE;
        }
    }
    else if (u_ch > 0x10000 && u_ch <= 0x10FFFF)
    {
        if (*pcbDstLen > 4)
        {
            ch = (uint8_t)((u_ch >> 18) & 0x03F);
            pDst[i++] = ch | 0xF0;
            ch = (uint8_t)((u_ch >> 12) & 0x03F);
            pDst[i++] = ch | 0x80;
            ch = (uint8_t)((u_ch >> 6) & 0x03F);
            pDst[i++] = ch | 0x80;
            ch = (uint8_t)(u_ch & 0x03F);
            pDst[i++] = ch | 0x80;

            *pcbDstLen = 4;
            bRC = dxTRUE;
        }
    }

    if (bRC == dxTRUE)
    {
        pDst[i] = 0x00;
    }

    return bRC;
}

/**
 * @fn dxBOOL CHARC_UTF8toLCDString(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen)
 * @author David Tang
 * @date 2017/05/26
 */
dxBOOL CHARC_UTF8toLCDString(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen)
{
    uint16_t i = 0;
    uint16_t j = 0;
    uint8_t cbChar = 0;

    if (cbSrcLen == 0 ||
        pSrc == NULL ||
        pDst == NULL ||
        pcbDstLen == NULL)
    {
        return dxFALSE;
    }

    while (i < cbSrcLen)
    {
        pDst[j++] = CHARC_UTF8toLCDChar(&pSrc[i], &cbChar);
        i += cbChar;
    }

    pDst[j] = 0x00;

    *pcbDstLen = j;

    return dxTRUE;
}

/**
 * @fn uint32_t CHARC_UTF8CharLength(uint8_t *pUTF8Char)
 * @author David Tang
 * @date 2017/05/26
 */
uint32_t CHARC_UTF8CharLength(uint8_t *pUTF8Char)
{
    uint32_t nLen = 0;
    uint32_t nUNICODE = 0;

    if (pUTF8Char == NULL)
    {
        return 0;
    }

    nLen = 1;

    if (pUTF8Char[0] <= 0x7F)
    {
        //U+0000 to U+007F
        nUNICODE = pUTF8Char[0];
    }
    else if ((pUTF8Char[0] & 0xF8) == 0xF0)
    {
        //U+10000 to U+10FFFF
        if ((pUTF8Char[1] & 0xC0) == 0x80 && (pUTF8Char[2] & 0xC0) == 0x80 && (pUTF8Char[3] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0x8) << 2) | ((pUTF8Char[1] & 0x30) >> 4);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[1] & 0xF) << 4) | ((pUTF8Char[2] & 0x3C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[2] & 0x3) << 6) | (pUTF8Char[3] & 0x3F);

            nLen = 4;
        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }
    else if ((pUTF8Char[0] & 0xF0) == 0xE0)
    {
        //U+0800 to U+FFFF
        if ((pUTF8Char[1] & 0xC0) == 0x80 && (pUTF8Char[2] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0xF) << 4) | ((pUTF8Char[1] & 0x3C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[1] & 0x3) << 6) | (pUTF8Char[2] & 0x3F);

            nLen = 3;

        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }
    else if ((pUTF8Char[0] & 0xE0) == 0xC0)
    {
        //U+0080 to U+07FF
        if ((pUTF8Char[1] & 0xC0) == 0x80)
        {
            nUNICODE = ((pUTF8Char[0] & 0x1C) >> 2);
            nUNICODE <<= 8;
            nUNICODE |= ((pUTF8Char[0] & 0x3) << 6) | (pUTF8Char[1] & 0x3F);

            nLen = 2;

        }
        else
        {
            nUNICODE = CHARCONV_DEFAULT_CHAR;
        }
    }

    if (nUNICODE > 0xFFFF)
    {
        nUNICODE = CHARCONV_DEFAULT_CHAR;
    }

    return nLen;
}

/**
 * @fn dxBOOL CHARC_EscapedFormtoUTF8String(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen)
 * @author David Tang
 * @date 2017/05/26
 */
dxBOOL CHARC_EscapedFormtoUTF8String(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen)
{
    uint16_t i = 0;
    uint16_t j = 0;
    dxBOOL bCalculateLengthOnly = dxFALSE;

    if (cbSrcLen == 0 ||
        pSrc == NULL ||
        pcbDstLen == NULL)
    {
        return dxFALSE;
    }

    if (pDst == NULL)
    {
        bCalculateLengthOnly = dxTRUE;
    }

    while (i < cbSrcLen)
    {
        uint32_t nBytes = 0;

        if (pSrc[i] == '\\' &&
            i + 6 <= cbSrcLen)
        {
            uint32_t u_ch = 0;
            nBytes = CHARC_EscapedFormChartoUNCODE(&u_ch, &pSrc[i], strlen((char *)&pSrc[i]));

            if (nBytes > 0 && i + nBytes <= cbSrcLen)
            {
                uint8_t u8_str[5] = {0x00, 0x00, 0x00, 0x00, 0x00};
                uint32_t u8_str_len = sizeof(u8_str);

                dxBOOL r = CHARC_UNICODEtoUTF8Char(u8_str, &u8_str_len, u_ch);
                if (r == dxTRUE)
                {
                    if (bCalculateLengthOnly == dxFALSE)
                    {
                        memcpy(&pDst[j], u8_str, u8_str_len);
                    }
                    i += nBytes;
                    j += u8_str_len;
                }
            }
        }

        if (nBytes == 0)
        {
            if (bCalculateLengthOnly == dxFALSE)
            {
                pDst[j] = pSrc[i];
            }
            i++;
            j++;
        }
    }

    *pcbDstLen = j;

    if (bCalculateLengthOnly == dxFALSE)
    {
        pDst[j] = 0x00;
    }

    return (j > 0) ? dxTRUE : dxFALSE;
}
