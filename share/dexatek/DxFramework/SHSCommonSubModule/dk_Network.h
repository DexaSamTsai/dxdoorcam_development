//============================================================================
// File: dk_network.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DK_NETWORK_H
#define _DK_NETWORK_H

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


/******************************************************
 *                      Defines
 ******************************************************/

#define UDP_TARGET_PORT                     50009

#define UDP_MULTICAST_ADDRESS_FIRST         224 //239 //224
#define UDP_MULTICAST_ADDRESS_SECOND        0   //255 //0
#define UDP_MULTICAST_ADDRESS_THIRD         1   //0   //1
#define UDP_MULTICAST_ADDRESS_FOURTH        1   //169 //1

#define AES_ENC_DEC_BIT                     128
#define AES_ENC_DEC_BYTE                    (AES_ENC_DEC_BIT/8)

#define UDP_PACKET_PREAMBLE                 0xC9E4
#define UDP_COMM_ACK_WAIT_TIME_MAX_MS       500
#define UDP_RECEIVED_VALID_TIME_IN_SEC      (5)
#define UDP_COMMAND_RETRY_MAX               (5)

/******************************************************
 *                      Macros
 ******************************************************/

// Must follow according to server programming guide Gateway State table
#define GATEWAY_STATE_READY                 (1)
#define GATEWAY_STATE_FW_UPDATETING         (2)


/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/


typedef enum _enumJSONToken
{
    DKJSN_TOKEN_UNKNOWN = 0,

    DKJSN_TOKEN_STATUS,                 // status
    DKJSN_TOKEN_CODE,                   // code
    DKJSN_TOKEN_ERROR,                  // error
    DKJSN_TOKEN_UPDATETIME,             // updatetime
    DKJSN_TOKEN_RESULTS,                // results

    DKJSN_TOKEN_SESSIONTOKEN,           // sessionToken
    DKJSN_TOKEN_PERIPHERALS,            // Peripherals
    DKJSN_TOKEN_BLEPERIPHERALSTATUS,    // BLEPeripheralStatus

    // Users Table
    DKJSN_TOKEN_OBJECTID,               // ObjectID
    DKJSN_TOKEN_PARENTOBJECTID,         // ParentObjectID
    DKJSN_TOKEN_USERNAME,               // Username
    DKJSN_TOKEN_PASSWORD,               // Password
    DKJSN_TOKEN_PERSONALKEY,            // PersonalKey
    DKJSN_TOKEN_EMAIL,                  // Email
    DKJSN_TOKEN_PREFERLANG,             // PreferLang

    // DeviceAccessControl Table
    DKJSN_TOKEN_USEROBJECTID,           // UserObjectID
    DKJSN_TOKEN_GATEWAYOBJECTID,        // GatewayObjectID
    DKJSCN_TOKEN_PERMISSION,            // Permission

    // BLEGateway Table
    DKJSN_TOKEN_GMACADDRESS,            // GMACAddress
    DKJSN_TOKEN_DEVICENAME,             // DeviceName
    DKJSN_TOKEN_LONGITUDE,              // Longitude
    DKJSN_TOKEN_LATITUDE,               // Latitude
    DKJSN_TOKEN_TOTALPERIPHERALS,       // TotalPeripherals
    DKJSN_TOKEN_WIFIVERSION,            // WiFiVersion
    DKJSN_TOKEN_BLEVERSION,             // BLEVersion

    // BLEPeripheral Table
    DKJSN_TOKEN_PMACADDRESS,            // PMACAddress
    DKJSN_TOKEN_GATEWAYID,              // GatewayID
    DKJSN_TOKEN_DEVICETYPE,             // DeviceType
    DKJSN_TOKEN_DEVICE_SIGNAL_STR,      // PSignalStr
    DKJSN_TOKEN_DEVICE_BATTERY,         // PBattery
    DKJSN_TOKEN_FIRMWAREVERSION,        // FirmwareVersion
    DKJSN_TOKEN_SECURITYKEY,            // SecurityKey

    // BLEHistory Table
    DKJSN_TOKEN_BLEOBJECTID,            // BLEObjectID
    DKJSN_TOKEN_DATETIME,               // DateTime

    // BLEPeripheralStatus Table
    DKJSN_TOKEN_BLEDATAPAYLOAD,         // BLEDataPayload

    // BLESchedule Table
    DKJSN_TOKEN_JOBINFORMATION,         // JobInformation
    DKJSN_TOKEN_SCHEDULESTATE,          // ScheduleState

    // BLEJobQueue
    DKJSN_TOKEN_JOBSTATE,               // JobState
    DKJSN_TOKEN_DEVICEID,               // DeviceID
    DKJSN_TOKEN_TASKID,                 // TaskID
    DKJSN_TOKEN_JOBHEADER,              // JobHeader
    DKJSN_TOKEN_CONDITIONS,             // Conditions
    DKJSN_TOKEN_JOBCONDITION,           // JobCondition
    DKJSN_TOKEN_EXECUTIONS,             // Executions
    DKJSN_TOKEN_JOBEXECUTION,           // JobExecution
    DKJSN_TOKEN_JOBTYPE,                // JobType
    DKJSN_TOKEN_CREATEDBYUSEROBJID,     // CreatedByUserObjID

    // ScheduleJob
    DKJSN_TOKEN_TOTALSCHJOBS,           // TotalSchJobs
    DKJSN_TOKEN_SCHJOBS,                // SchJobs
    DKJSN_TOKEN_SCHEDULESEQNO,          // ScheduleSEQNO

    // AdvancedJob
    DKJSN_TOKEN_ADVOBJID,               // AdvObjID
    DKJSN_TOKEN_TOTALADVJOBS,           // TotalAdvJobs
    DKJSN_TOKEN_ADVJOBS,                // AdvJobs

    // IPCam
    DKJSN_TOKEN_LIVEURL,                // liveURL
    DKJSN_TOKEN_TYPE,                   // Type
    DKJSN_TOKEN_FILEFORMAT,             // FileFormat
    DKJSN_TOKEN_ADVJOB,                 // AdvJob
    DKJSN_TOKEN_UPLOADURL,              // UploadURL
    DKJSN_TOKEN_RECORDSTARTTIME,        // RecordStartTime
    DKJSN_TOKEN_DOWNLOADURL,            // DownloadURL
    DKJSN_TOKEN_MESSAGEDATA,            // MessageData

    // Container
    DKJSN_TOKEN_DATACONTAINER,          // DataContainer
    DKJSN_TOKEN_CONTMID,                // ContMID
    DKJSN_TOKEN_CONTMNAME,              // ContMName
    DKJSN_TOKEN_CONTMTYPE,              // ContMType
    DKJSN_TOKEN_CONTDETAILS,            // ContDetails
    DKJSN_TOKEN_CONTDID,                // ContDID
    DKJSN_TOKEN_CONTDNAME,              // ContDName
    DKJSN_TOKEN_CONTDTYPE,              // ContDType
    DKJSN_TOKEN_SVALUE,                 // SValue
    DKJSN_TOKEN_LVALUE,                 // LValue
    DKJSN_TOKEN_CONTLVCRC,              // ContLVCRC

    // Others
    DKJSN_TOKEN_IP,                     // IP
    DKJSN_TOKEN_STATUSCODE,             // statuscode
    DKJSN_TOKEN_SERVERTIME,             // ServerTime
    DKJSN_TOKEN_NUMBEROFACTIVEUSERS,    // NumberOfActiveUsers
    DKJSN_TOKEN_CLIENTTIMEZONE,         // ClientTimeZone
    DKJSN_TOKEN_TIMESTAMP,              // TimeStamp
    DKJSN_TOKEN_DKAUTH,                 // DKAuth
    DKJSN_TOKEN_ACCESSTOKEN,            // AccessToken
    DKJSN_TOKEN_ACCTOKENTIME,           // AccTokenTime

    // Firmware
    DKJSN_TOKEN_GATEWAYFIRMWARE,        // GatewayFirmware
    DKJSN_TOKEN_GWWIFIFW,               // GWWifiFw
    DKJSN_TOKEN_GWBLEFW,                // GWBLEFw
    DKJSN_TOKEN_FULLPATHFILENAME,       // FullPathFileName
    DKJSN_TOKEN_VERSION,                // Version
    DKJSN_TOKEN_MD5,                    // MD5

    // DKEvent
    DKJSN_TOKEN_EVENT_FUNC,             // func
    DKJSN_TOKEN_EVENT_TIME,             // time
    DKJSN_TOKEN_EVENT_ARGS,             // args
    DKJSN_TOKEN_EVENT_NAME,             // Name
    DKJSN_TOKEN_EVENT_STATUS,           // Status
    DKJSN_TOKEN_EVENT_VALUE,            // Value

} JSONToken;


typedef enum
{

    UDP_COMM_EVENT_UNKNOWN                = 0,
    UDP_COMM_EVENT_NEW_JOB,                                         /**< From Mobile Device to gateway - Gateway Application to Handle*/
    UDP_COMM_EVENT_DEVICE_INFO,                                     /**< From Gateway (Application) to Mobile Device - Mobile SDK to Handle*/
    UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_START,                /**< From Mobile Device to gateway - Gateway INTERNAL UdpCommManager to Handle*/
    UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_START_RESPONSE,       /**< From Mobile Device to gateway - Gateway INTERNAL UdpCommManager to Handle*/
    UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_WILL_EXPIRE,          /**< From Gateway (Library) to Mobile Device - Mobile SDK to Handle*/
    UDP_COMM_EVENT_JOB_EXECUTION_RESPONSE,
    UDP_COMM_EVENT_UNPAIR_DEVICE_INFO,                              /**< From Gateway (Application) to Mobile Device - Mobile SDK to Handle*/

} UdpCommManager_EventType_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct _tagDKJSONTOkenTable
{
    JSONToken   id;
    char        *pszKey;
} DKJSONTokenTable;


typedef struct
{
    uint8_t MacAddr[8];

} WifiCommDeviceMac_t;


/** The Udp packet transferred between mobile device and Gateway is UdpPayloadHeader_t+PayloadData were Payload data is appended to the end of
 *  UdpPayloadHeader_t. All transferred data via Udp is AES128 encrypted.
 */

typedef struct
{

    uint16_t                DeviceIdentificationNumber; /**< Device unique ID - Usually Wifi MAC or Mobile Specific UUID convert to CRC16 */
    uint16_t                PacketType;                 /**< See UdpCommManager_EventType_t */
    uint32_t                TimeToken;
    WifiCommDeviceMac_t     GatewayMacAddr;             /**< The gateway MAC, if sent from mobile device then this Udp package will only be received by this mac address */
    uint16_t                PayLoadIdentificationNumber;
    uint16_t                PayloadLen;

} UdpPayloadHeader_t; //MUST be 4 byte (32-bit) alignment

/** Internal Use
 */
typedef struct
{
    uint16_t                Preamble;
    uint8_t                 Reserver[4];
    uint16_t                DeviceId;
    uint16_t                PacketId;
    uint16_t                PayloadCRC;
    uint16_t                PayloadLen;

} UdpPacketHeader_t;

/******************************************************
 *               Variable Definitions
 ******************************************************/

static const unsigned char UdpPacketAesKey[16] = {'d', 'k', 'u', 'd', 'p', 'p', 'a', 'c', 'k', 'e', 't', 'a', 'e', 's', 'i', 'd'};

static const DKJSONTokenTable DKJSONKeyTable[] =
{
    { DKJSN_TOKEN_STATUS,               "status"                },
    { DKJSN_TOKEN_CODE,                 "code"                  },
    { DKJSN_TOKEN_ERROR,                "error"                 },
    { DKJSN_TOKEN_UPDATETIME,           "updatetime"            },
    { DKJSN_TOKEN_RESULTS,              "results"               },

    { DKJSN_TOKEN_SESSIONTOKEN,         "sessionToken"          },
    { DKJSN_TOKEN_PERIPHERALS,          "Peripherals"           },
    { DKJSN_TOKEN_BLEPERIPHERALSTATUS,  "BLEPeripheralStatus"   },

    // Users Table
    { DKJSN_TOKEN_OBJECTID,             "ObjectID"              },
    { DKJSN_TOKEN_PARENTOBJECTID,       "ParentObjectID"        },
    { DKJSN_TOKEN_USERNAME,             "Username"              },
    { DKJSN_TOKEN_PASSWORD,             "Password"              },
    { DKJSN_TOKEN_PERSONALKEY,          "PersonalKey"           },
    { DKJSN_TOKEN_EMAIL,                "Email"                 },
    { DKJSN_TOKEN_PREFERLANG,           "PreferLang"            },

    // DeviceAccessControl Table
    { DKJSN_TOKEN_USEROBJECTID,         "UserObjectID"          },
    { DKJSN_TOKEN_GATEWAYOBJECTID,      "GatewayObjectID"       },
    { DKJSCN_TOKEN_PERMISSION,          "Permission"            },

    // BLEGateway Table
    { DKJSN_TOKEN_GMACADDRESS,          "GMACAddress"           },
    { DKJSN_TOKEN_DEVICENAME,           "DeviceName"            },
    { DKJSN_TOKEN_LONGITUDE,            "Longitude"             },
    { DKJSN_TOKEN_LATITUDE,             "Latitude"              },
    { DKJSN_TOKEN_TOTALPERIPHERALS,     "TotalPeripherals"      },    
    { DKJSN_TOKEN_WIFIVERSION,          "WiFiVersion"           },
    { DKJSN_TOKEN_BLEVERSION,           "BLEVersion"            },

    // BLEPeripheral Table
    { DKJSN_TOKEN_PMACADDRESS,          "PMACAddress"           },
    { DKJSN_TOKEN_GATEWAYID,            "GatewayID"             },
    { DKJSN_TOKEN_DEVICETYPE,           "DeviceType"            },
    { DKJSN_TOKEN_DEVICE_SIGNAL_STR,    "PSignalStr"            },  //TODO: Aaron add 2014/05/07 - Make sure to update related document, Signal strength to be subdivided into 4 category (0-No signal,1-Weak,2-Normal,3-Strong)
    { DKJSN_TOKEN_DEVICE_BATTERY,       "PBattery"              },  //TODO: Aaron add 2014/05/07 - Make sure to update related document, Battery level from 0 to 100, Battery level of 0 means the device is NOT battery powered device.
    { DKJSN_TOKEN_FIRMWAREVERSION,      "FirmwareVersion"       },
    { DKJSN_TOKEN_SECURITYKEY,          "SecurityKey"           },

    // BLEHistory Table
    { DKJSN_TOKEN_BLEOBJECTID,          "BLEObjectID"           },
    { DKJSN_TOKEN_DATETIME,             "DateTime"              },

    // BLEPeripheralStatus Table
    { DKJSN_TOKEN_BLEDATAPAYLOAD,       "BLEDataPayload"        },

    // BLESchedule Table
    { DKJSN_TOKEN_JOBINFORMATION,       "JobInformation"        },
    { DKJSN_TOKEN_SCHEDULESTATE,        "ScheduleState"         },

    // BLEJobQueue
    { DKJSN_TOKEN_JOBSTATE,             "JobState"              },
    { DKJSN_TOKEN_DEVICEID,             "DeviceID"              },
    { DKJSN_TOKEN_TASKID,               "TaskID"                },
    { DKJSN_TOKEN_JOBHEADER,            "JobHeader"             },
    { DKJSN_TOKEN_CONDITIONS,           "Conditions"            },
    { DKJSN_TOKEN_JOBCONDITION,         "JobCondition"          },
    { DKJSN_TOKEN_EXECUTIONS,           "Executions"            },
    { DKJSN_TOKEN_JOBEXECUTION,         "JobExecution"          },
    { DKJSN_TOKEN_JOBTYPE,              "JobType"               },
    { DKJSN_TOKEN_CREATEDBYUSEROBJID,   "CreatedByUserObjID"    },

    // ScheduleJob
    { DKJSN_TOKEN_TOTALSCHJOBS,         "TotalSchJobs"          },
    { DKJSN_TOKEN_SCHJOBS,              "SchJobs"               },
    { DKJSN_TOKEN_SCHEDULESEQNO,        "ScheduleSEQNO"         },

    // AdvancedJob
    { DKJSN_TOKEN_ADVOBJID,             "AdvObjID"              },
    { DKJSN_TOKEN_TOTALADVJOBS,         "TotalAdvJobs"          },
    { DKJSN_TOKEN_ADVJOBS,              "AdvJobs"               },

    // IPCam
    { DKJSN_TOKEN_LIVEURL,              "liveURL"               },
    { DKJSN_TOKEN_TYPE,                 "Type"                  },
    { DKJSN_TOKEN_FILEFORMAT,           "FileFormat"            },
    { DKJSN_TOKEN_ADVJOB,               "AdvJob"                },
    { DKJSN_TOKEN_UPLOADURL,            "UploadURL"             },
    { DKJSN_TOKEN_RECORDSTARTTIME,      "RecordStartTime"       },
    { DKJSN_TOKEN_DOWNLOADURL,          "DownloadURL"           },
    { DKJSN_TOKEN_MESSAGEDATA,          "MessageData"           },

    // Container
    { DKJSN_TOKEN_DATACONTAINER,        "DataContainer"         },
    { DKJSN_TOKEN_CONTMID,              "ContMID"               },
    { DKJSN_TOKEN_CONTMNAME,            "ContMName"             },
    { DKJSN_TOKEN_CONTMTYPE,            "ContMType"             },
    { DKJSN_TOKEN_CONTDETAILS,          "ContDetails"           },
    { DKJSN_TOKEN_CONTDID,              "ContDID"               },
    { DKJSN_TOKEN_CONTDNAME,            "ContDName"             },
    { DKJSN_TOKEN_CONTDTYPE,            "ContDType"             },
    { DKJSN_TOKEN_SVALUE,               "SValue"                },
    { DKJSN_TOKEN_LVALUE,               "LValue"                },
    { DKJSN_TOKEN_CONTLVCRC,            "ContLVCRC"             },

    // Others
    { DKJSN_TOKEN_IP,                   "IP"                    },
    { DKJSN_TOKEN_STATUSCODE,           "statuscode"            },
    { DKJSN_TOKEN_SERVERTIME,           "ServerTime"            },
    { DKJSN_TOKEN_NUMBEROFACTIVEUSERS,  "NumberOfActiveUsers"   },
    { DKJSN_TOKEN_CLIENTTIMEZONE,       "ClientTimeZone"        },
    { DKJSN_TOKEN_TIMESTAMP,            "TimeStamp"             },
    { DKJSN_TOKEN_DKAUTH,               "DKAuth"                },
    { DKJSN_TOKEN_ACCESSTOKEN,          "AccessToken"           },
    { DKJSN_TOKEN_ACCTOKENTIME,         "AccTokenTime"          },

    // Firmware
    { DKJSN_TOKEN_GATEWAYFIRMWARE,      "GatewayFirmware"       },
    { DKJSN_TOKEN_GWWIFIFW,             "GWWifiFw"              },
    { DKJSN_TOKEN_GWBLEFW,              "GWBLEFw"               },
    { DKJSN_TOKEN_FULLPATHFILENAME,     "FullPathFileName"      },
    { DKJSN_TOKEN_VERSION,              "Version"               },
    { DKJSN_TOKEN_MD5,                  "MD5"                   },

    // DKEvent
    { DKJSN_TOKEN_EVENT_FUNC,           "func"                  },
    { DKJSN_TOKEN_EVENT_TIME,           "time"                  },
    { DKJSN_TOKEN_EVENT_ARGS,           "args"                  },
    { DKJSN_TOKEN_EVENT_NAME,           "Name"                  },
    { DKJSN_TOKEN_EVENT_STATUS,         "Status"                },
    { DKJSN_TOKEN_EVENT_VALUE,          "Value"                 },

 };

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/**
 * @fn _ConvertTokenString
 *
 * @param n
 *
 * @return
 */
DKJSONTokenTable *_ConvertTokenString(int n);


#ifdef __cplusplus
}
#endif


#endif // _DK_NETWORK_H
