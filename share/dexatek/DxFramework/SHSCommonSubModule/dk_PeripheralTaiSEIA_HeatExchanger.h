/**
 * Copyright (C) 2018 Dexatek Technology Ltd.
 *
 * dk_PeripheralTaiSEIA_HeatExchanger.h
 *
 * This is proprietary information of Dexatek Technology Ltd. All Rights
 * Reserved. Reproduction of this documentation or the accompanying programs in
 * any manner whatsoever without the written permission of Dexatek Technology
 * Ltd. is strictly forbidden.
 */

#ifndef DK_PERIPHERAL_TAISEIA_HEAT_EXCHANGER_H
#define DK_PERIPHERAL_TAISEIA_HEAT_EXCHANGER_H

#pragma once

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "dk_Peripheral.h"
#include "dk_PeripheralTaiSEIA.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Heat Exchanger Service Code */
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_POWER_CONTROL                   0x00
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_MODE                  0x01
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_WIND_SPEED_SETTING              0x02
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TEMPERATURE_SETTING             0x03
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_RETURN_AIR_TEMPERATURE          0x04
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OUTSIDE_AIR_TEMPERATURE         0x05
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TIMER_SETTING                   0x06
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_SETTINGS_CHECKING               0x07
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_INDOOR_MODEL                    0x08
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_MESSAGE                   0x09
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_CURRENT               0x0A
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_VOLTAGE               0x0B
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_POWER_FACTOR          0x0C
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_CURRENT_POWER                   0x0D
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_AGGREGATE_POWER_CONSUMPTION     0x0E
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_1                     0x0F
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_2                     0x10
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_3                     0x11
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_4                     0x12
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_5                     0x13
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_FILTER_CLEANING_NOTIFICATION    0x14
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_VENTILATION_MODE                0x15
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_PREHEAT_PRECOOL                 0x16

/* Engineering mode Service Code */
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ENGINEERING_MODE_1              0x50
#define TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ENGINEERING_MODE_N              0x7E

/**
 * API for available services
 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_POWER_CONTROL, H'00 */
typedef enum
{
    TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_OFF,
    TAISEIA_HEAT_EXCHANGER_POWER_CONTROL_ON
} eTaiseiaHeatExchangerPowerControl;

typedef struct {
    bool IsWritable;
    bool PowerOn;
    bool PowerOff;
} TaiseiaHeatExchangerSupportedPowerControl_t;

TaiseiaHeatExchangerSupportedPowerControl_t TaiseiaHeatExchangerGetSupportedPowerControl(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerPowerControl TaiseiaHeatExchangerGetConvertedPowerControl(TaiSEIAServiceInfo_t service);
eTaiseiaHeatExchangerPowerControl TaiseiaHeatExchangerParsePowerControlServiceAccess(TaiSEIAServiceAccess_t ServiceAccess);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreatePowerControlPayload(eTaiseiaHeatExchangerPowerControl state);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_MODE, H'01 */

typedef enum {
    TAISEIA_HEAT_EXCHANGER_OP_MODE_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_CONDITION,
    TAISEIA_HEAT_EXCHANGER_OP_MODE_DEHUMIDIFICATION,
    TAISEIA_HEAT_EXCHANGER_OP_MODE_AIR_SUPPLY,
    TAISEIA_HEAT_EXCHANGER_OP_MODE_AUTO,
    TAISEIA_HEAT_EXCHANGER_OP_MODE_HEATER
} eTaiseiaHeatExchangerOperationMode;

typedef struct {
    bool IsWritable;
    bool AirCondition;
    bool Dehumidification;
    bool AirSupply;
    bool Auto;
    bool Heater;
} TaiseiaHeatExchangerSupportedOperationMode_t;

TaiseiaHeatExchangerSupportedOperationMode_t TaiseiaHeatExchangerGetSupportedOperationMode(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerOperationMode TaiseiaHeatExchangerGetConvertedOperationMode(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateOperationModePayload(eTaiseiaHeatExchangerOperationMode mode);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_WIND_SPEED_SETTING, H'02 */

typedef enum {
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_AUTO,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_1,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_2,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_3,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_4,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_5,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_6,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_7,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_8,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_9,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_10,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_11,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_12,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_13,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_14,
    TAISEIA_HEAT_EXCHANGER_WIND_SPEED_15
} eTaiseiaHeatExchangerWindSpeed;

typedef struct {
    uint16_t Level0_b0 :1;
    uint16_t Level1_b1 :1;
    uint16_t Level2_b2 :1;
    uint16_t Level3_b3 :1;
    uint16_t Level4_b4 :1;
    uint16_t Level5_b5 :1;
    uint16_t Level6_b6 :1;
    uint16_t Level7_b7 :1;
    uint16_t Level8_b8 :1;
    uint16_t Level9_b9 :1;
    uint16_t Level10_b10 :1;
    uint16_t Level11_b11 :1;
    uint16_t Level12_b12 :1;
    uint16_t Level13_b13 :1;
    uint16_t Level14_b14 :1;
    uint16_t Level15_b15 :1;
} TaiseiaHeatExchangerWindSpeedBitmap_t;

typedef union {
    uint16_t flag;
    TaiseiaHeatExchangerWindSpeedBitmap_t bits;
} TaiseiaHeatExchangerWindSpeed_u;

typedef struct {
    bool IsWritable;
    TaiseiaHeatExchangerWindSpeed_u Speed;
} TaiseiaHeatExchangerSupportedWindSpeed_t;

TaiseiaHeatExchangerSupportedWindSpeed_t TaiseiaHeatExchangerGetSupportedWindSpeed(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerWindSpeed TaiseiaHeatExchangerGetConvertedWindSpeed(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateWindSpeedPayload(eTaiseiaHeatExchangerWindSpeed speed);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TEMPERATURE_SETTING, H'03 */

typedef struct {
    bool IsWritable;
    uint8_t Minimum;
    uint8_t Maximum;
} TaiseiaHeatExchangerSupportedTemperatureSetting_t;

TaiseiaHeatExchangerSupportedTemperatureSetting_t TaiseiaHeatExchangerSupportedTemperatureSetting(TaiSEIASupportedServiceInfo_t service);
int8_t TaiseiaHeatExchangerGetConvertedTemperatureSetting(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateTemperatureSettingPayload(int8_t setting);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_RETURN_AIR_TEMPERATURE, H'04 */

typedef struct {
    bool IsWritable;
    int8_t Minimum;
    int8_t Maximum;
} TaiseiaHeatExchangerSupportedIndoorTemperature_t;

TaiseiaHeatExchangerSupportedIndoorTemperature_t TaiseiaHeatExchangerSupportedIndoorTemperature(TaiSEIASupportedServiceInfo_t service);
int8_t TaiseiaHeatExchangerGetConvertedIndoorTemperature(TaiSEIAServiceInfo_t ServiceInfo);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OUTSIDE_AIR_TEMPERATURE, H'05 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_TIMER_SETTING, H'06 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_SETTINGS_CHECKING, H'07 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_INDOOR_MODEL, H'08 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_MESSAGE, H'09 */

typedef struct {
    uint16_t Normal_b0 :1;
    uint16_t Error1_b1 :1;
    uint16_t Error2_b2 :1;
    uint16_t Error3_b3 :1;
    uint16_t Error4_b4 :1;
    uint16_t Error5_b5 :1;
    uint16_t Error6_b6 :1;
    uint16_t Error7_b7 :1;
    uint16_t Error8_b8 :1;
    uint16_t Error9_b9 :1;
    uint16_t Error10_b10 :1;
    uint16_t Error11_b11 :1;
    uint16_t Error12_b12 :1;
    uint16_t Error13_b13 :1;
    uint16_t Error14_b14 :1;
    uint16_t Error15_b15 :1;
} TaiseiaHeatExchangerErrorMessageBitmap_t;

typedef union {
    uint16_t flag;
    TaiseiaHeatExchangerErrorMessageBitmap_t bits;
} TaiseiaHeatExchangerErrorMessage_u;

typedef struct {
    bool IsWritable;
    TaiseiaHeatExchangerErrorMessage_u Code;
} TaiseiaHeatExchangerSupportedErrorMessage_t;

TaiseiaHeatExchangerSupportedErrorMessage_t TaiseiaHeatExchangerSupportedErrorMessage(TaiSEIASupportedServiceInfo_t service);
TaiseiaHeatExchangerErrorMessage_u TaiseiaHeatExchangerGetConvertedErrorMessage(TaiSEIAServiceInfo_t service);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_CURRENT, H'0A */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_VOLTAGE, H'0B */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_OPERATION_POWER_FACTOR, H'0C */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_CURRENT_POWER, H'0D */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_AGGREGATE_POWER_CONSUMPTION, H'0E */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_1, H'0F */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_2, H'10 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_3, H'11 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_4, H'12 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_ERROR_LOG_5, H'13 */

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_FILTER_CLEANING_NOTIFICATION, H'14 */

typedef enum {
    TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_OFF,
    TAISEIA_HEAT_EXCHANGER_FILTER_CLEANING_NOTIFICATION_ON
} eTaiseiaHeatExchangerFilterCleaningNotification;

typedef struct {
    bool IsWritable;
    bool Off;
    bool On;    
} TaiseiaHeatExchangerSupportedFilterCleaningNotification_t;

TaiseiaHeatExchangerSupportedFilterCleaningNotification_t TaiseiaHeatExchangerGetSupportedFilterCleaningNotification(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerFilterCleaningNotification TaiseiaHeatExchangerGetConvertedFilterCleaningNotification(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateFilterCleaningNotificationPayload(eTaiseiaHeatExchangerFilterCleaningNotification notification);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateUsedHourResetAfterCleanFilterPacketPayload(void);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_VENTILATION_MODE, H'15 */

typedef enum {
    TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_AUTO,
    TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_ENERGY_RECOVERY,
    TAISEIA_HEAT_EXCHANGER_VENTILATION_MODE_NORMAL
} eTaiseiaHeatExchangerVentilationMode;

typedef struct {
    bool IsWritable;
    bool Auto;
    bool EnergyRecovery;
    bool Normal;
} TaiseiaHeatExchangerSupportedVentilationMode_t;

TaiseiaHeatExchangerSupportedVentilationMode_t TaiseiaHeatExchangerGetSupportedVentilationMode(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerVentilationMode TaiseiaHeatExchangerGetConvertedVentilationMode(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreateVentilationModePayload(eTaiseiaHeatExchangerVentilationMode mode);

/* API for TAISEIA_HEAT_EXCHANGER_SERVICE_CODE_PREHEAT_PRECOOL, H'16 */

typedef enum {
    TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_UNKNOWN = -1,
    TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_NO_DELAY,
    TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_30_MIN,
    TAISEIA_HEAT_EXCHANGER_PREHEAT_PRECOOL_60_MIN
} eTaiseiaHeatExchangerPreheatPrecool;

typedef struct {
    bool IsWritable;
    bool NoDelay;
    bool Delay30Min;
    bool Delay60Min;
} TaiseiaHeatExchangerSupportedPreheatPrecool_t;

TaiseiaHeatExchangerSupportedPreheatPrecool_t TaiseiaHeatExchangerGetSupportedPreheatPrecool(TaiSEIASupportedServiceInfo_t service);
eTaiseiaHeatExchangerPreheatPrecool TaiseiaHeatExchangerGetConvertedPreheatPrecool(TaiSEIAServiceInfo_t service);
TaiSEIAPackedPayload_t TaiseiaHeatExchangerCreatePreheatPrecoolPayload(eTaiseiaHeatExchangerPreheatPrecool setting);

#endif //DK_PERIPHERAL_TAISEIA_HEAT_EXCHANGER_H
