//============================================================================
// File: dk_Job.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef DK_JOB_H
#define DK_JOB_H

#pragma once

#include <stdbool.h>
#include "dk_Network.h"
#include "dk_Peripheral.h"

#ifdef __cplusplus
extern "C" {
#endif

    /******************************************************
     *                      Defines
     ******************************************************/

#define JOB_SCHEDULE_START_TIME_NOW                 0

#define JOB_SCHEDULE_TIME_NONE                      0xFFFF

#define JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL       0
#define JOB_CONDITION_TYPE_ENHANCED_COMMAND         1

#define JOB_EXEC_BLE_STD_UUID_128BIT				    1
#define JOB_EXEC_BLE_STD_UUID_16BIT					0

#define THRESHOLD_UNIT_PER_15_SECONDS				    1

#define JOB_HEADER_PREAMBLE							0xD1D0
#define JOB_INSTANCE_VALID_ID						    0xEAAE

#define TIMEZONE_FRAC_30MIN							1
#define TIMEZONE_FRAC_45MIN							2

#define MAX_ADV_JOB_RESERVED_ID                     0x0FFF
#define MIN_SCHEDULE_JOB_RESERVED_UNIQUE_ID         65000

/** @defgroup JOB NOTIFICATION NAMED & NUM KEYS
 *
 * @{
 */

#define JOB_NOTIFICATION_DOOR_OPEN_KEY                      "DOOROPEN"
#define JOB_NOTIFICATION_DOOR_CLOSE_KEY                     "DOORCLOSE"
#define JOB_NOTIFICATION_SMOKE_DETECTOR_ALARM_KEY           "SMOKEDETALARM"
#define JOB_NOTIFICATION_SMOKE_DETECTOR_WARNING_KEY         "SMOKEDETWARNING"
#define JOB_NOTIFICATION_SHOCK_SENSE_DETECTOR_ALERT_KEY     "SHOCKDETALARM"
#define JOB_NOTIFICATION_MOTION_SENSE_DETECTED_ALERT_KEY    "MOTIONDETECTEDALARM"
#define JOB_NOTIFICATION_MOTION_SENSE_TEMPERED_ALERT_KEY    "MOTIONTEMPEREDALARM"
#define JOB_NOTIFICATION_WEATHER_TEMP_GREATER_THAN_KEY      "WHTEMPGREATERNOTIFY"
#define JOB_NOTIFICATION_WEATHER_TEMP_LESS_THAN_KEY         "WHTEMPLESSNOTIFY"
#define JOB_NOTIFICATION_WEATHER_HUM_GREATER_THAN_KEY       "WHHUMGREATERNOTIFY"
#define JOB_NOTIFICATION_WEATHER_HUM_LESS_THAN_KEY          "WHHUMLESSNOTIFY"
#define JOB_NOTIFICATION_DOOR_LOCK_UNLOCK                   "DOORLOCKUNLOCK"
#define JOB_NOTIFICATION_ALARM_SYSTEM_ALERT                 "AS_ALERT"
#define JOB_NOTIFICATION_ALARM_SYSTEM_POWER_OFF             "AS_POWEROFF"
#define JOB_NOTIFICATION_ALARM_SYSTEM_BATTERY_LEVEL         "AS_BATTLEVEL"
#define JOB_NOTIFICATION_ALARM_SYSTEM_MAIN_SABOTAGE         "AS_SABOTAGE"
#define JOB_NOTIFICATION_ALARM_SYSTEM_DEV_SABOTAGE          "AS_DEV_SBTG"
#define JOB_NOTIFICATION_ALARM_SYSTEM_DOS_ATTACK            "AS_DOS_ATTACK"
#define JOB_NOTIFICATION_ALARM_SYSTEM_SILENT_ALARM          "AS_SILENTALARM"
#define JOB_NOTIFICATION_ALARM_SYSTEM_ALERT_OFFLINE         "AS_ALRTOFFLINE"
#define JOB_NOTIFICATION_ALARM_SYSTEM_GENERAL_ALERT         "AS_ALARM_GEN"
#define JOB_NOTIFICATION_ALARM_SYSTEM_DISARM_MAIN_SBTG      "AS_DSM_SBTG"
#define JOB_NOTIFICATION_ALARM_SYSTEM_DISARM_DEV_SBTG       "AS_DSM_DEV_SBTG"
#define JOB_NOTIFICATION_AIRCONDITION_CLEAN_FILTER          "AIRCON_CLEANFILTER"
#define JOB_NOTIFICATION_DEHUMIDIFIER_WATER_FULL            "DEHUMIDIFIER_WATER_FULL"
#define JOB_NOTIFICATION_DESK_STANDUP                       "DSK_STANDUP"
#define JOB_NOTIFICATION_DESK_SITDOWN                       "DSK_SITDOWN"

#define JOB_NOTIFICATION_IPCAM_VIDEO_EVT_NUM_KEY            50700
#define JOB_NOTIFICATION_IPCAM_IMAGE_EVT_NUM_KEY            50710
#define JOB_NOTIFICATION_DESK_STANDUP_KEY                   51100
#define JOB_NOTIFICATION_DESK_SITDOWN_KEY                   51110

/** @} End JOB NOTIFICATION NAMED & NUM KEYS */

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{
    JOB_TYPE_STANDARD_BLE       = 0,
    JOB_TYPE_GATEWAY_COMMAND    = 1, //OBSOLETE (NAMING PURPOSE) - Please use JOB_TYPE_DEVICE_COMMAND instead (its exactly the same)
    JOB_TYPE_DEVICE_COMMAND     = 1,
    JOB_TYPE_HYBRIDPERIPHERAL   = 2,

    JOB_TYPE_INVALID            = 0xFFFF,
} JobType_t;

typedef enum
{
    SOURCE_OF_ORIGIN_DEFAULT = 0,
    SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL,
    SOURCE_OF_ORIGIN_UDP_JOB,
    SOURCE_OF_ORIGIN_DK_SERVER_JOB,
    SOURCE_OF_ORIGIN_HP,  //Job passed from HP Job Bridging

} SourceOfOrigin_t;

/* WARNING: Command enum MUST follow according to Job Spec..
 * DO NOT Re-order the command enums!
 */
typedef enum
{
    DEVICE_COMMAND_ID_SCAN_UNPAIR_PERIPHERAL = 1,
    DEVICE_COMMAND_ID_PAIR_PERIPHERAL,
    DEVICE_COMMAND_ID_UNPAIR_PERIPHERAL,
    DEVICE_COMMAND_ID_IDENTIFY,
    DEVICE_COMMAND_ID_SOFT_RESTART,
    DEVICE_COMMAND_ID_SEND_NOTIFICATION,
    DEVICE_COMMAND_ID_SET_RENEW_SECURITY_FOR_PERIPHERAL,
    DEVICE_COMMAND_ID_DELETE_SCHEDULE_LONGTERM_JOB,
    DEVICE_COMMAND_ID_HISTORIC_EVENT_UPDATE,
    DEVICE_COMMAND_ID_DELETE_ADVANCE_JOB,
    DEVICE_COMMAND_ID_UPDATE_LOCAL_PERIPHERAL_NAME,
    DEVICE_COMMAND_ID_UPDATE_PERIPHERAL_RTC,

} DeviceCommandID_t;

/* WARNING: Command enum MUST follow according to Job Spec..
 * DO NOT Re-order the command enums!
 */
typedef enum
{
    CONDITION_ENHANCE_CMD_ID_GEOFENCING = 1,

} CondEnahncedCommandID_t;

/* WARNING: Command enum MUST follow according to Job Spec..
 * DO NOT Re-order the command enums!
 */
typedef enum
{
    UNIQUE_ID_POWER_PLUG_AGGREGATED_INTERVAL_UPDATE = 	100,
    UNIQUE_ID_POWER_PLUG_ON_HISTORIC_EVENT_UPDATE =       101,    //PowerPlug ON - asking gateway to send to Historic Event
    UNIQUE_ID_POWER_PLUG_OFF_HISTORIC_EVENT_UPDATE =      102,    //PowerPlug OFF - asking gateway to send to Historic Event

    UNIQUE_ID_HOME_DOOR_NOTIFICATION = 					200,
    UNIQUE_ID_WEATHER_CUBE_SAMPLE_INTERVAL_UPDATE = 		300,
    UNIQUE_ID_WEATHER_CUBE_TEMP_GREATER_THAN_NOTIFICATION = 	301,
    UNIQUE_ID_WEATHER_CUBE_TEMP_LESS_THAN_NOTIFICATION = 		302,
    UNIQUE_ID_WEATHER_CUBE_HUM_GREATER_THAN_NOTIFICATION = 		303,
    UNIQUE_ID_WEATHER_CUBE_HUM_LESS_THAN_NOTIFICATION = 		304,


    UNIQUE_ID_SMOKE_DETECTOR_ALERT_NOTIFICATION =			400,	//Smoke/Fire detected mobile notification
    UNIQUE_ID_SMOKE_DETECTOR_WARNING_NOTIFICATION =		401,	//Malfunction/Tempered detected mobile notification
    UNIQUE_ID_SHOCK_SENSE_DETECTOR_ALERT_NOTIFICATION =   500,    //Shock Detected Alert mobile notification
    UNIQUE_ID_SHOCK_SENSE_DETECTOR_ALERT_HISTORIC_EVENT_UPDATE =   501,    //Shock Detected by asking gateway to send to Historic Event
    UNIQUE_ID_MOTION_SENSE_DETECT_ALERT_NOTIFICATION_HISTORIC_EVENT_UPDATE = 600, //Motion Detected by asking gateway to send notification & Historic Event
    UNIQUE_ID_MOTION_SENSE_TEMPER_ALERT_NOTIFICATION_HISTORIC_EVENT_UPDATE = 601, //Motion Tempered by asking gateway to send notification & Historic Event
    UNIQUE_ID_MOTION_SENSE_AGGREGATE_INTERVAL_UPDATE = 602, //Motion Aggregate Data Interval Update
    UNIQUE_ID_POWER_SOCKET_A_ON_HISTORIC_EVENT_UPDATE =         700,    //PowerSocket A ON - asking gateway to send to Historic Event
    UNIQUE_ID_POWER_SOCKET_A_OFF_HISTORIC_EVENT_UPDATE =        701,    //PowerSocket A OFF - asking gateway to send to Historic Event
    UNIQUE_ID_POWER_SOCKET_B_ON_HISTORIC_EVENT_UPDATE =         702,    //PowerSocket B ON - asking gateway to send to Historic Event
    UNIQUE_ID_POWER_SOCKET_B_OFF_HISTORIC_EVENT_UPDATE =        703,    //PowerSocket B OFF - asking gateway to send to Historic Event
    UNIQUE_ID_DOOR_LOOK_UNLOCK_NOTIFICATION = 800,
    UNIQUE_ID_DOOR_LOOK_UNLOCK_HISTORIC_EVENT_UPDATE = 802,
    UNIQUE_ID_THERMOSTAT_SHUTDOWN_HISTORIC_EVENT_UPDATE = 900,
    UNIQUE_ID_THERMOSTAT_MANUAL_HISTORIC_EVENT_UPDATE = 901,
    UNIQUE_ID_THERMOSTAT_AUTO_HISTORIC_EVENT_UPDATE = 902,

    UNIQUE_ID_RGB_LIGHT_ON_HISTORIC_EVENT_UPDATE        = 1000,
    UNIQUE_ID_RGB_LIGHT_OFF_HISTORIC_EVENT_UPDATE       = 1001,

    UNIQUE_ID_AIRCONDITION_ON_HISTORIC_EVENT_UPDATE     = 1100,
    UNIQUE_ID_AIRCONDITION_OFF_HISTORIC_EVENT_UPDATE    = 1101,
    UNIQUE_ID_AIRCONDITION_FILTER_ACCUMULATED_CLEANING_NOTIFICATION = 1102,
    UNIQUE_ID_AIRCONDITION_FILTER_CLEANING_NOTIFICATION = 1103,

    UNIQUE_ID_RGB_LIGHTSTRIP_ON_HISTORIC_EVENT_UPDATE   = 1200,
    UNIQUE_ID_RGB_LIGHTSTRIP_OFF_HISTORIC_EVENT_UPDATE  = 1201,

    UNIQUE_ID_DEHUMIDIFIER_ON_HISTORIC_EVENT_UPDATE     = 1300,
    UNIQUE_ID_DEHUMIDIFIER_OFF_HISTORIC_EVENT_UPDATE    = 1301,
    UNIQUE_ID_DEHUMIDIFIER_INTANK_WATER_FULL_LEVEL      = 1302,

    /**
     * 1400: LED Adapter ON Historic Event Update
     * 1401: LED Adapter OFF Historic Event Update
     */

    UNIQUE_ID_AIRCON_RPI_ON_HISTORIC_EVENT_UPDATE       = 1500,
    UNIQUE_ID_AIRCON_RPI_OFF_HISTORIC_EVENT_UPDATE      = 1501,
    UNIQUE_ID_AIRCON_RPI_FILTER_CLEANING_NOTIFICATION   = 1502,

    UNIQUE_ID_POWER_PLUG_SCHEDULE_RANGE_MIN = 			6000,
    UNIQUE_ID_POWER_PLUG_SCHEDULE_RANGE_MAX = 			6099,

    UNIQUE_ID_LIGHT_SCHEDULE_RANGE_MIN = 					7000,
    UNIQUE_ID_LIGHT_PLUG_SCHEDULE_RANGE_MAX = 			7099,

    UNIQUE_ID_INLETSWITCH_SCHEDULE_RANGE_MIN = 		    8000,
    UNIQUE_ID_INLETSWITCH_SCHEDULE_RANGE_MAX = 			8099,

    UNIQUE_ID_POWERSOCKET_SCHEDULE_RANGE_MIN = 			9000,
    UNIQUE_ID_POWERSOCKET_SCHEDULE_RANGE_MAX = 			9099,

    UNIQUE_ID_REMOTE_CONTROL_KEYPRESS_EVENT_HYBRID_REPORT =     10000,	//Remote control keypress event to hybrid report
    UNIQUE_ID_REMOTE_CONTROL_RTC_UPDATE =                       10001,	//Remote control RTC Update
    UNIQUE_ID_REMOTE_CONTROL_AUTO_SECURITY_RENEW =              10002,	//Remote control Auto security renew

    UNIQUE_ID_LED_ADAPTOR_SCHEDULE_RANGE_MIN = 			11000,
    UNIQUE_ID_LED_ADAPTOR_SCHEDULE_RANGE_MAX = 			11099,

    UNIQUE_ID_RGB_LIGHT_SCHEDULE_RANGE_MIN =            12000,
    UNIQUE_ID_RGB_LIGHT_SCHEDULE_RANGE_MAX = 			12099,

    UNIQUE_ID_AIRCONDITION_SCHEDULE_RANGE_MIN =         13000,
    UNIQUE_ID_AIRCONDITION_SCHEDULE_RANGE_MAX =         13099,

    UNIQUE_ID_RGB_LIGHT_STRIP_SCHEDULE_RANGE_MIN =      14000,
    UNIQUE_ID_RGB_LIGHT_STRIP_SCHEDULE_RANGE_MAX =      14099,

    UNIQUE_ID_TAISEIA_DEHUMIDIFIER_SCHEDULE_RANGE_MIN = 15000,
    UNIQUE_ID_TAISEIA_DEHUMIDIFIER_SCHEDULE_RANGE_MAX = 15099,

    UNIQUE_ID_TAISEIA_HEAT_EXCHANGER_SCHEDULE_RANGE_MIN = 16000,
    UNIQUE_ID_TAISEIA_HEAT_EXCHANGER_SCHEDULE_RANGE_MAX = 16099,
} DefaultSpecificJobUniqueId_t;


typedef enum
{

    JOB_SUCCESS = 0,
    JOB_UNCOMPLETED_PERIPHERAL_ALREADY_PAIRED,

    JOB_FAILED_GENERIC				= -1,
    JOB_FAILED_TIME_OUT				= -2,
    JOB_FAILED_CONNECTION_UNSTABLE	= -3,
    JOB_FAILED_INTERNAL				= -4,
    JOB_FAILED_MISSING_SECURITY_KEY	= -5,			 //SecurityKey cannot be found from server, repairing with given Pin Code is required (Most likely an internal error)
    JOB_FAILED_SECURITY_KEY_NOT_LONGER_VALID 	= -6,    //The key is not longer valid, possibly cause by device reset and repaired by other mobile devices (although this is not likely to happen)
    JOB_FAILED_PIN_CODE_IS_NOT_VALID 			= -7,    //The security key exchange failed because pin code is not valid
    JOB_FAILED_SERVER_ERROR					= -8,
    JOB_FAILED_MAXIMUM_CAPACITY_REACHED 		= -9,	 //Example of receiving this is when asking gateway to scan for peripheral and gateway report failure as it can no longer support more peripherals
    JOB_FAILED_INVALID_PARAM                  = -10,
    JOB_FAILED_NOT_SUPPORTED                  = -11,

} JobReportStatus_t;

/* Standard BLE Job BLE condition flag enum */
enum eStdBleJobCondTypeFlag
{
    STANDARD_CHECK_STDBLE_COND_TYPE_FLAG        = 0,
    PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG       = 1,
    RTC_CHECK_STDBLE_COND_TYPE_FLAG             = 2,
    KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG    = 3,

};


/* Must folow Ble Gateway Job data format spec */
enum eStdBleJobReadWriteMode
{
    WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ = 0,
    WRITE_ONLY = 1,
    READ_FOLLOW_BYAGGREGATIONUPDATE = 2,
    READ_FOLLOW_BYDATA_REPORT = 3,

};

/* Must folow Ble Gateway Job data format spec */
enum eDataMode
{
    CHAR_DATA_SELF_FILLED = 0,
    CHAR_DATA_AUTO_FILLED = 1,
};


/* Must follow according to OPERATOR in Job format's specification */
enum eConditionOperator
{
    OPERATOR_GREATER_THAN = 1,
    OPERATOR_GREATER_THAN_EQUAL,
    OPERATOR_LESS_THAN_EQUAL,
    OPERATOR_LESS_THAN,
    OPERATOR_EQUAL,

};


/* Must follow according to VALUE_TYPE in Job format's specification */
enum eConditionValueType
{
    VALUE_TYPE_UNKNOWN = 0,
    VALUE_TYPE_INT8 = 1,
    VALUE_TYPE_UINT8,
    VALUE_TYPE_INT16,
    VALUE_TYPE_UINT16,
    VALUE_TYPE_INT32,
    VALUE_TYPE_UINT32,
    VALUE_TYPE_FLOAT,

};

/* Job condition OR flag enum */
enum eConditionTypeOrFlag
{
    CONDITION_TYPE_AND = 0,
    CONDITION_TYPE_OR = 1,
};

/* Job condition reset flag enum */
enum eConditionTypeRstFlag
{
    CONDITION_RST_NONE = 0,
    CONDITION_RST_ALL = 1,
};

/* Job condition first run flag enum */
enum eConditionTypeFirstRunFlag
{
    CONDITION_FIRST_SKIP = 0,
    CONDITION_FIRST_RUN = 1,
};

/* Job BLE execution retry type flag enum */
enum eBleWriteRetryTypeFlag
{
    BLE_WRITE_RETRY_10S = 0,
    BLE_WRITE_RETRY_SHORT = 1,
};

/* Job superior flag enum */
enum eJobSuperiorFlag
{
    JOB_SUPERIOR_DISABLE = 0,
    JOB_SUPERIOR_ENABLE = 1,
};

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

struct SCHEDULE_FLAG_BIT_INFO {
    uint16_t ScheduleOnce_b0     	:1;     // bit 0 single bit
    uint16_t ShceduleMon_b1 	    :1;     // bit 1 single bit
    uint16_t ShceduleTue_b2   		:1;     // bit 2 single bit
    uint16_t ShceduleWed_b3   		:1;     // bit 3 single bit
    uint16_t ShceduleThu_b4   		:1;     // bit 4 single bit
    uint16_t ShceduleFri_b5   		:1;     // bit 5 single bit
    uint16_t ShceduleSat_b6   		:1;     // bit 6 single bit
    uint16_t ShceduleSun_b7   		:1;     // bit 7 single bit
    uint16_t ShceduleDayOfMonth_b8 	:1;     // bit 8 single bit
    uint16_t ShceduleYear_b9   		:1;     // bit 9 single bit
    uint16_t ShceduleInterval_b10	:1;     // bit 10 single bit
    uint16_t ScheduleStartTimeOption :1;	// bit 11 single bit
    uint16_t Reserve_b12_14			:3;		// bit 12 through 14 (3 bit total)
    uint16_t ShceduleRepeatable_b15	:1;		// bit 15 single bit

};

struct SCHEDULE_TIME_BIT_INFO {
    uint16_t ScheduleTimeMinute_b0_7     	:8;     // bit 0 through 7 (8 bit total)
    uint16_t ShceduleTimeHour_b8_15 	    :8;     // bit 8 through 15 (8 bit total)
};

struct SCHEDULE_START_TIME_BIT_INFO {

    uint32_t SchTimeRangeEndMinute_b0_7     	:8;     // bit 0 through 7 (8 bit total)
    uint32_t SchTimeRangeEndHour_b8_15 	    	:8;     // bit 8 through 15 (8 bit total)
    uint32_t SchTimeRangeStartMinute_b16_23     :8;     // bit 16 through 23 (8 bit total)
    uint32_t SchTimeRangeStartHour_b24_31 	    :8;     // bit 24 through 31 (8 bit total)

};

struct JOB_FLAG_BIT_INFO {
    uint16_t Reserve_b0_2               :3;     // bit 0 through 2 (3 bit total)
    uint16_t JobSuperior_b3             :1;     // bit 3 single bit (1 bit total)
    uint16_t JobBleWriteOnce_b4         :1;     // bit 4 single bit (1 bit total)
    uint16_t JobConditionFirstRun_b5    :1;     // bit 5 single bit (1 bit total)
    uint16_t JobConditionRst_b6         :1;     // bit 6 single bit (1 bit total)
    uint16_t JobConditionOr_b7          :1;     // bit 7 single bit (1 bit total)
    uint16_t JobConditionType_b8_11     :4;     // bit 8 through 11 (4 bit total)
    uint16_t JobExecutionType_b12_15    :4;     // bit 12 through 15 (4 bit total)
};

struct JOB_BLE_STD_CHAR_FLAH_BIT_INFO {
    uint16_t UUID_Len_b0     	:1;     	// bit 0 single bit
    uint16_t ReadWriteMode_b1_3	:3;			// bit 1 through 3 (3 bit total)
    uint16_t Reserve_b4_15 	    :12;     	// bit 4 through 15 (12 bit total)
};

struct JOB_HYBRID_STD_CHAR_FLAH_BIT_INFO {
    uint16_t Reserve_b0         :1;     	// bit 0 single bit
    uint16_t ReadWriteMode_b1_3	:3;			// bit 1 through 3 (3 bit total)
    uint16_t DataMode_b4_5	    :2;			// bit 4 through 5 (2 bit total)
    uint16_t Reserve_b6_15 	    :10;     	// bit 6 through 15 (10 bit total)
};

struct JOB_CONDITION_TYPE {
    uint16_t ConditionField_b0_7     	:8;     	// bit 0 through 7 (8 bit total)
    uint16_t ConditionReached_b8		:1;			// bit 8 single bit
    uint16_t Reserve_b9_11     			:3;     	// bit 9 through 11 (3 bit total)
    uint16_t ConditionTypeFlag_b12_15  	:4;     	// bit 12 through 15 (4 bit total)
};

struct TIME_ZONE {
    uint8_t Integral_b0_3     	:4;     	// bit 0 through 3 (4 bit total)
    uint8_t FractionalMode_b4_5	:2;			// bit 4 through 5 (2 bit total)
    uint8_t Reserve_b6 			:1;     	// bit 6 (1 bit total)
    uint8_t Sign_b7  			:1;     	// bit 7 (1 bit total)
};

#ifdef __IAR_SYSTEMS_ICC__
#pragma pack(push, 1)
#endif // __IAR_SYSTEMS_ICC__
struct ScheduleSequenceNumber_s
{
    uint32_t				PeripheralUniqueID;
    uint16_t				UniqueSequenceID;

#ifdef __IAR_SYSTEMS_ICC__
};
#pragma pack(pop)
#else // __IAR_SYSTEMS_ICC__
} __attribute__((packed));
#endif // __IAR_SYSTEMS_ICC__

union JobScheduleFlag_u {
    uint16_t  						flag;
    struct SCHEDULE_FLAG_BIT_INFO 	bits;
};

union JobScheduleTime_u {
    uint16_t  						Time;
    struct SCHEDULE_TIME_BIT_INFO 	bits;
};

union JobType_u {
    uint16_t  						Type;
    struct JOB_FLAG_BIT_INFO	 	bits;
};

union JobBleStdCharFlag_u {
    uint16_t  								CharFlag;
    struct JOB_BLE_STD_CHAR_FLAH_BIT_INFO	 bits;
};

union JobHybridStdCharFlag_u {
    uint16_t                                    CharFlag;
    struct JOB_HYBRID_STD_CHAR_FLAH_BIT_INFO    bits;
};

union JobConditionType_u {
    uint16_t  						Type;
    struct JOB_CONDITION_TYPE 		bits;
};

union ValueType_u {
    int8_t  						TypeINT8;
    uint8_t  						TypeUINT8;
    int16_t  						TypeINT16;
    uint16_t  						TypeUINT16;
    int32_t  						TypeINT32;
    uint32_t  						TypeUINT32;
    float  							TypeFLOAT;
};

union ScheduleSequenceNumber_u {
    uint8_t  							byte[6];
    struct ScheduleSequenceNumber_s		Component;
};

union JobScheduleStartTime_u {
    uint32_t  								Time;
    struct SCHEDULE_START_TIME_BIT_INFO 	bits;
};

union TimeZone_u {
    uint8_t  						value;
    struct TIME_ZONE 				bits;
};


typedef struct
{
    uint16_t                        Preamble;
    uint8_t                         Reserve1;                   //Padding
    union TimeZone_u                TimeZone;
    WifiCommDeviceMac_t             DeviceMacAddress;
    union ScheduleSequenceNumber_u  ScheduleSequenceNumber;     //This is packed to archive 6 byte alignment
    union JobScheduleFlag_u         ScheduleFlag;
    union JobScheduleStartTime_u    ScheduleStartTime;          //Check Job Data Format Spec for detail
    union JobScheduleTime_u         ScheduleTime;
    union JobType_u                 JobType;
    uint8_t                         TotalJobCondition;
    uint8_t                         TotalJobExecution;
    uint8_t                         ReExecutionThreshold;       //0 - No Threshold, otherwise each unit is 15 Seconds
    uint8_t                         Reserve2;                   //Padding

} JobHeader_t; //MUST be 4 byte (32-bit) alignment (according to spec)

typedef struct
{
    uint16_t            InstanceID;
    JobHeader_t         Header;
    uint16_t            ConditionLen;
    uint8_t*            pCondition;
    uint16_t            ExecutionLen;
    uint8_t*            pExecution;
    uint8_t*            pPayloadPack;

} JobInstance_t;


typedef struct
{
    bool						Monday;
    bool						Tuesday;
    bool						Wednesday;
    bool						Thursday;
    bool						Friday;
    bool						Saturday;
    bool						Sunday;

} DaysOfWeek_t;


typedef struct
{
    uint8_t						Hour24; //0 to 23
    uint8_t						Minute; //0 to 59

} SchTime_t;



typedef struct
{

    DevAddress_t				PeripheralDeviceMacAddress;
    uint16_t					PayloadLen;

} JobConditionHeader_t;


typedef struct
{

    uint8_t                 EnhancedCommandID;
    uint8_t                 EnhancedCommandDataLen;
    uint8_t                 CommandConditionPayloadLen;

} JobConditionEnhancedHeader_t;


typedef struct
{

    DevAddress_t                PeripheralDeviceMacAddress;
    union JobBleStdCharFlag_u   CharFlag;
    uint16_t                    ServiceUUID;
    uint16_t                    CharacteristicUUID;
    uint16_t                    DataLen;

} BleStandardJobExecHeader_t;

typedef struct
{

    DevAddress_t                    PeripheralDeviceMacAddress;
    union JobHybridStdCharFlag_u    CharFlag;
    uint8_t                         ServiceUUID[16];
    uint8_t                         CharacteristicUUID[16];
    uint16_t                        DataLen;

} HybridPeripheralJobExecHeader_t;

typedef struct
{
    uint8_t         CommandId;
    uint8_t         PayloadLen;

} DeviceCommandJobHeader_t;


typedef struct
{

    union JobConditionType_u	ConditionType;
    uint8_t						Operator;			//See eConditionOperator
    uint8_t						ValueType;			//See eConditionValueType

} JobConditionPayloadHeader_t;

typedef struct
{
    uint8_t						ValueType;			//See eConditionValueType
    union ValueType_u			Value;

} TypeValue_t;

typedef struct
{
    bool            IsValid;
    DaysOfWeek_t    DaysSelection;
    SchTime_t       UTC0RangeStartTime;
    SchTime_t       UTC0RangeEndTime;
    float			TimeZone;

} JobConditionTimeRange_t;


typedef struct
{
    //Data is valid ONLY if Valid is set to true
    bool                        Valid;

    DevAddress_t				PeripheralDeviceMacAddress;
    uint8_t                     DataType;
    enum eConditionOperator     Operator;
    enum eConditionValueType    ValueType;
    union ValueType_u			Value;

} JobConditionTypeAdvData_t;

typedef struct
{
    //Data is valid ONLY if Valid is set to true
    bool                        Valid;

    uint8_t                     EnhanceCommandID;
    uint8_t                     EnhanceCommandDataLen;
    uint8_t*                    pEnhanceCommData;

    enum eConditionOperator     Operator;
    enum eConditionValueType    ValueType;
    union ValueType_u			Value;

} JobConditionTypeEnhData_t;


typedef struct
{
    uint8_t                     FlagOperate;    //Condition is OR(1) or AND(0)
    uint8_t                     FlagRst;        //Condition need to all reset before next execution (1) or trigger by any new condition reach (0)
} JobConditionTypeFlag_t;

typedef struct
{
    uint8_t                     ConditionType;  //JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL or JOB_CONDITION_TYPE_ENHANCED_COMMAND
    JobConditionTypeAdvData_t   TypeAdvData;    //Data will be valid (check the Valid member) if Condition type is JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL
    JobConditionTypeEnhData_t   TypeEnhanceData;//Data will be valid (check the Valid member) if Condition type is JOB_CONDITION_TYPE_ENHANCED_COMMAND
    JobConditionTypeFlag_t      ConditionTypeFlags; //Condition is OR
} JobCondition_t;


typedef struct
{
    uint8_t                     ExecutionType; //Below info is valid ONLY if this flag is equal to JOB_TYPE_STANDARD_BLE
    DevAddress_t				PeripheralDeviceMacAddress;
    uint16_t					ServiceUUID;
    uint16_t					CharacteristicUUID;
    uint16_t                    DataLen;
    uint8_t                     Data[20]; //We preallocate just enough, we avoid use of pointers for simplicity

} JobExecution_t;

typedef struct
{
    uint8_t                     ExecutionType;  //Below info is valid ONLY if this flag is equal to JOB_TYPE_HYBRIDPERIPHERAL
    DevAddress_t				PeripheralDeviceMacAddress;
    uint8_t                     ServiceUUID[16];
    uint8_t                     CharacteristicUUID[16];
    uint16_t                    DataLen;
    uint8_t*                    pData;          //WARNING: This pointer is a reference pointed from the given payload, if payload is release then this pointer will not longer have valid data

} JobExecutionHybrid_t;

typedef struct
{
    float						Latitude;
    float                       longitude;

} GeoLocation_t;


typedef struct _tagAdvJobSatusSrv
{
    uint32_t        TimeStamp;
    uint32_t        AdvObjID;
    uint8_t         JobStatus;
} AdvJobStatusSrv_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                 Global Variables
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

//Job Package Usage Instruction/Sample (Below sample is for creating LongTerm Schedule Job)
//
//    JobInstance_t JobInstance;
//    struct ScheduleSequenceNumber_s ScheduleSEQNo;
//    WifiCommDeviceMac_t    GatewayMac;
//    DevAddress_t            PeripheralMac;
//
//    ScheduleSEQNo.PeripheralUniqueID = PeripheralObjectID;
//    ScheduleSEQNo.UniqueSequenceID = PeripheralUniqueID;
//
//    _DecimalStrToMACAddr(GatewayMac.MacAddr, sizeof(GatewayMac.MacAddr), GatewayMacDecimalStr);
//    _DecimalStrToMACAddr(PeripheralMac.Addr, sizeof(PeripheralMac.Addr), PeripheralMacDecimalStr);
//
//    CreateJobPackage(&JobInstance, &GatewayMac, &ScheduleSEQNo, 0);
//
//    JobPackageHeader_ScheduleIntervalJobSetup(&JobInstance, 30, 1);
//    JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(&JobInstance, &PeripheralMac,
//                                                                                 DK_SERVICE_SYSTEM_UTILS_UUID,
//                                                                                 DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID);
//
//    uint16_t ReturnedPayloadLen = 0;
//    uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);
//
//    (... Save/Convert the pJobPayload (WILL BE INVALID after calling FreeJobPackage) that will needed to send to server...)
//
//    FreeJobPackage(&JobInstance);
//
//    uint64_t ScheduleSEQNO = _ScheduleSequnceStructToUINT64(&ScheduleSEQNo);
//
//    (... Send the copied JobPayload and ScheduleSEQNO to server)

//Job Package Main Init/Release/Create (WARNING: ALWAYS FreeJobPackage after calling JobPackageGeneratePayloadPack)
//NOTE: For default specific job please use DefaultSpecificJobUniqueId_t to fill ScheduleSEQNo->UniqueSequenceID
extern void CreateJobPackage(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, struct ScheduleSequenceNumber_s* ScheduleSEQNo, uint8_t ThresholdReExecutionUnit);
extern void FreeJobPackage(JobInstance_t* JobInstance);
extern uint8_t* JobPackageGeneratePayloadPack(JobInstance_t* JobInstance, uint16_t* ReturnedPayloadLen);

//Job Package Header Setup (WARNING: Some Header overwrite the others)
extern void JobPackageHeader_OneTimeJobSetup(JobInstance_t* JobInstance);
extern void JobPackageHeader_ScheduleIntervalJobSetup(JobInstance_t* JobInstance, uint16_t intervalInSeconds, uint8_t Repeatable);
extern void JobPackageHeader_ScheduleRepeatableJobSetup(JobInstance_t* JobInstance, uint32_t ScheduleStartTime);
extern void JobPackageHeader_ScheduleTimeAndDayRepeatableJobSetup(JobInstance_t* JobInstance, uint32_t ScheduleStartTime, DaysOfWeek_t DaysSelection, SchTime_t InUTC0ExecutionTime);
extern bool JobPackageHeader_SetOrCondition(JobInstance_t* JobInstance, uint16_t Reserve);
extern bool JobPackageHeader_SetResetAllCondition(JobInstance_t* JobInstance);
extern bool JobPackageHeader_SetFirstRunCondition(JobInstance_t* JobInstance);
extern bool JobPackageHeader_SetBLEWriteOnce(JobInstance_t* JobInstance);
extern bool JobPackageHeader_SetJobSuperior(JobInstance_t* JobInstance);

//Job Package Condition create, can add multiple
extern void JobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue);
extern void JobPackageConditionAdd_AdvertisementProtectedDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue);
extern void JobPackageConditionAdd_AdvertisementRTCSynchConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, enum eConditionOperator Operator, uint32_t RtcSynchThresholdMs);
extern void JobPackageConditionAdd_AdvertisementNoEncKeyConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr);


//Job Package Execution create, can add multiple (WARNING: CANNOT Mix PeripheralAccess, HybridPeripheralAccess and GatewayJob.. all 3 are treated as different kind)
extern void JobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData);
extern void JobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData);
extern void JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID);
extern void JobPackageExecutionAdd_PeripheralAccessReadFollowByDataReportJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID);

extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData);
extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData);
extern void JobPackageExecutionAdd_HybridPeripheralAccessReadFollowByAggregationUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128);
extern void JobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyDataAutoFillJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128);

extern void JobPackageExecutionAdd_GatewayJob_IdentifySetup(JobInstance_t* JobInstance);
extern void JobPackageExecutionAdd_GatewayJob_PeripheralUnpairSetup(JobInstance_t* JobInstance, DevAddressAndType_t* DevToUnpair);
extern void JobPackageExecutionAdd_GatewayJob_SoftResetSetup(JobInstance_t* JobInstance);
extern void JobPackageExecutionAdd_GatewayJob_ScanUnpairPeripheralSetup(JobInstance_t* JobInstance, uint16_t ScanReportTimeInSeconds);
extern void JobPackageExecutionAdd_GatewayJob_PeripheralPairSetup(JobInstance_t* JobInstance, stPairingInfo_t* DevToPair);
extern void JobPackageExecutionAdd_GatewayJob_PeripheralSetNewSecuritySetup(JobInstance_t* JobInstance, stRenewSecurityInfo_t* DevToReNewSecurity);
extern void JobPackageExecutionAdd_GatewayJob_NotificationSetup(JobInstance_t* JobInstance, const char* NotificationKey, const char* ReplacementParam1);
extern void JobPackageExecutionAdd_GatewayJob_NotificationSetupExt(JobInstance_t* JobInstance, const char* NotificationKey, const char* ReplacementParam1, const char* ReplacementParam2);
extern void JobPackageExecutionAdd_GatewayJob_DeleteScheduleLongTermJobSetup(JobInstance_t* JobInstance);
extern void JobPackageExecutionAdd_GatewayJob_DeleteAdvenceJobSetup(JobInstance_t* JobInstance, uint32_t AdvanceJobObjectID);
extern void JobPackageExecutionAdd_GatewayJob_HistoricEventUpdateSetup(JobInstance_t* JobInstance);
extern void JobPackageExecutionAdd_GatewayJob_UpdateLocalPeripheralName(JobInstance_t* JobInstance, uint32_t PeripheralObjectID, uint8_t *pDeviceName);
extern void JobPackageExecutionAdd_GatewayJob_UpdatePeripheralRTC(JobInstance_t* JobInstance, stUpdatePeripheralRtcInfo_t* DevToUpdateRTC);

/* JobPayload Parser
 * NOTE: JobPayload is the payload from server or the result from JobPackageGeneratePayloadPack
 * 		 DO NOT pass in JobInstance_t* JobInstance to JobPayloadIn*/
extern bool     ParseJobPackage_JobSuperior(uint8_t* JobPayloadIn);
extern bool     ParseJobPackage_ThresholdReExecutionUnit(uint8_t* JobPayloadIn, uint8_t* ThresholdUnitOf15Sec);
extern bool     ParseJobPackage_ScheduleTimeAndDay(uint8_t* JobPayloadIn, uint32_t* OutScheduleStartTime, DaysOfWeek_t* OutDaysSelection, SchTime_t* OutUTC0ExecutionTime);
extern const uint8_t*   ParseJobPackage_FirstPeripheralAccessInfo(uint8_t* JobPayloadIn, uint16_t JobPayloadLen, DevAddress_t* OutDevAddr, uint16_t* OutServiceUUID, uint16_t* OutCharacteristicUUID, uint16_t* OutDataLen);
extern const uint8_t*   ParseJobPackage_FirstAdvertisementDataConditionInfo(uint8_t* JobPayloadIn, uint16_t JobPayloadLen, uint8_t* OutConditionField, enum eConditionOperator *OutOperator, enum eConditionValueType *OutTypeValue);

//NOTE: ExecutionIndex and ConditionIndex starts with 0 (eg, 0 as the first item)
extern uint8_t	ParseJobPackage_TotalAvailableAdvertisementDataCondition(uint8_t* JobPayloadIn);
extern uint8_t	ParseJobPackage_TotalAvailablePeripheralAccessExecution(uint8_t* JobPayloadIn);
extern uint8_t	ParseJobPackage_TotalAvailableHybridPeripheralAccessExecution(uint8_t* JobPayloadIn);
extern const uint8_t*	ParseJobPackage_PeripheralAccessInfoByIndex(uint8_t ExecutionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen, DevAddress_t* OutDevAddr, uint16_t* OutServiceUUID, uint16_t* OutCharacteristicUUID, uint16_t* OutDataLen);
extern JobExecutionHybrid_t ParseJobPackage_HybridPeripheralAccessInfoByIndex(uint8_t ExecutionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen);
extern const uint8_t*   ParseJobPackage_AdvertisementDataConditionInfoByIndex(uint8_t ConditionIndex, uint8_t* JobPayloadIn, uint16_t JobPayloadLen, uint8_t* OutConditionField, enum eConditionOperator *OutOperator, enum eConditionValueType *OutTypeValue);



/* 	Advance Job Package APIs
 *
 *	INSTRUCTION
 *	Advance Job Package is designed according to Server REST API usage behaviour, In Advance Job you CREATE a package that
 *	is EITHER Condition package or Execution package, you DO NOT create a Mix of both Condition and Execution.
 *	Check the REST API fist for detail and you should get the idea.
 *
 *	EXAMPLE 1 (Create multiple condition package for a Gateway)
 *
 *	CreateAdvanceJobPackage (JobIns, GatewayA, 15)
 *	AdvJobPackageHeader_RangedTimeAndDayRepeatableJobSetup(.........) //Call ONLY once
 *	AdvJobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobIns, PeripheralA, .......)
 *	AdvJobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobIns, PeripheralB, .......)
 *	AdvJobPackage = AdvanceJobPackageGeneratePayloadPack()
 *	SaveOrCopyAdvJobPackage(AdvJobPackage) //Here you save it or copy it to REST API data preparation buffer
 *	FreeAdvanceJobPackage(JobIns) //FREE, NOTE that AdvJobPackage at this point forward will NOT longer be valid!
 *
 *
 *	EXAMPLE 2 (Create multiple execution package for a Gateway)
 *
 *	CreateAdvanceJobPackage (JobIns, GatewayA, 15)
 *	AdvJobPackageHeader_OneTimeJobSetup(.........) //Call ONLY once
 *
 *	AdvJobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobIns, PeripheralA, .......)
 *	AdvJobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobIns, PeripheralB, .......)
 *	AdvJobPackage = AdvanceJobPackageGeneratePayloadPack()
 *	SaveOrCopyAdvJobPackage(AdvJobPackage) //Here you save it or copy it to REST API data preparation buffer
 *	FreeAdvanceJobPackage(JobIns) //FREE, NOTE that AdvJobPackage at this point forward will NOT longer be valid!
 *
 */

/*
 * Advance Job Package Main Init/Release/Create (WARNING: ALWAYS FreeAdvanceJobPackage after calling AdvanceJobPackageGeneratePayloadPack)
 */
extern bool CreateAdvanceJobPackage(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, uint8_t ThresholdReExecutionUnit);  // AND Condition AdvJob
extern bool CreateAdvanceJobPackageOrCondition(JobInstance_t* JobInstance, const WifiCommDeviceMac_t* GatewayMAC, uint8_t ThresholdReExecutionUnit, uint8_t Flags);  // OR Condition AdvJob
extern bool FreeAdvanceJobPackage(JobInstance_t* JobInstance);
extern uint8_t* AdvanceJobPackageGeneratePayloadPack(JobInstance_t* JobInstance, uint16_t* ReturnedPayloadLen);

/*
 * Advance Job Package Condition Setup
 * (WARNING:Header should only be called once while Condition of the same "TYPE" can be added/called multiple times)
 */
//Header
extern bool AdvJobPackageHeader_RangedTimeAndDayRepeatableJobSetup(JobInstance_t* JobInstance, DaysOfWeek_t DaysSelection, SchTime_t InUTC0RangeStartTime, SchTime_t InUTC0RangeEndTime, float TimeZone);

//Condition TYPE of BLE Peripheral
extern bool AdvJobPackageConditionAdd_AdvertisementDataConditionJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t ConditionField, enum eConditionOperator Operator, enum eConditionValueType TypeValue, uint8_t* pValue);

//Condition TYPE of Enhanced command
extern bool AdvJobPackageConditionAddEnh_GeofencingConditionJobSetup(JobInstance_t* JobInstance, GeoLocation_t CenterLoc, enum eConditionOperator Operator, uint32_t RadiusInMeters);


/*
 * Advance Job Package Execution Setup
 * (WARNING:Header should only be called once while execution add can be called multiple times)
 * (WARNING:CANNOT Mix PeripheralAccess, HybridPeripheralAccess.. all 2 are treated as different kind)
 */
extern bool AdvJobPackageHeader_OneTimeJobSetup(JobInstance_t* JobInstance);
extern bool AdvJobPackageHeader_SetOrCondition(JobInstance_t* JobInstance, uint16_t Reserve);
extern bool AdvJobPackageHeader_SetResetAllCondition(JobInstance_t* JobInstance);
extern bool AdvJobPackageHeader_SetFirstRunCondition(JobInstance_t* JobInstance);
extern bool AdvJobPackageHeader_SetBLEWriteOnce(JobInstance_t* JobInstance);
extern bool AdvJobPackageHeader_SetJobSuperior(JobInstance_t* JobInstance);

extern bool AdvJobPackageExecutionAdd_PeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData);
extern bool AdvJobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint16_t ServiceUUID, uint16_t CharacteristicUUID, uint16_t DataLen, uint8_t* pData);

extern bool AdvJobPackageExecutionAdd_HybridPeripheralAccessWriteFollowByAdvReadUpdateJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData);
extern bool AdvJobPackageExecutionAdd_HybridPeripheralAccessWriteOnlyJobSetup(JobInstance_t* JobInstance, DevAddress_t* DevAddr, uint8_t* ServiceStrUUID128, uint8_t* CharacteristicStrUUID128, uint16_t DataLen, uint8_t* pData);


/* Advance Job Payload Parser
 * NOTE: JobPayload is the payload from server or the result from JobPackageGeneratePayloadPack
 * 		 DO NOT pass in JobInstance_t* JobInstance to JobPayloadIn */

//Advance Job Header
extern bool     ParseAdvJobPackage_JobSuperior(uint8_t* JobPayloadIn);

//Advance Job Condition Time Range
extern JobConditionTimeRange_t	ParseAdvJobPackage_ConditionTimeRange(uint8_t* JobPayloadIn);

//Advance Job Condition
extern uint8_t	ParseAdvJobPackage_TotalAvailableCondition(uint8_t* JobConditionPayloadIn);
extern JobCondition_t	ParseAdvJobPackage_ConditionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ConditionIndex); //Condition Index starts at 0 (First condition)

//Auxiliar parser for Advance Job Condition
extern bool     AuxParseAdvJob_EnhanceComm_GeoFencingLoc(JobCondition_t* pInJobConditionData, GeoLocation_t* pOutGeoLocation);


//Advance Job Execution
extern uint8_t          ParseAdvJobPackage_TotalAvailableExecution(uint8_t* JobExecutionPayloadIn);
extern JobType_t        ParseAdvJobPackage_GetExecutionjobType(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex);
extern JobExecution_t	ParseAdvJobPackage_ExecutionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex); //Execution Index starts at 0 (First execution)
extern JobExecutionHybrid_t ParseAdvJobPackage_HybridExecutionGivenIndex(uint8_t* JobPayloadIn, uint16_t JobPayloadDataLen, uint8_t ExecutionIndex); //Execution Index starts at 0 (First execution)

/*Other utils
 *
 *
 * */
extern uint64_t _ScheduleSequnceStructToUINT64(struct ScheduleSequenceNumber_s* ScheduleSEQNo);
extern struct ScheduleSequenceNumber_s _UINT64ScheduleSeqNoToStruct(uint64_t ScheduleSEQNo);
extern uint8_t* GetJobConditionStartPointer(uint8_t* JobData);
extern uint8_t* GetJobExecutionStartPointer(uint8_t* JobData);

#if 0 //Moved to Utils.h (due to strange linking error when called in JobManager while accessing JobScheduleList as pointer)
extern uint8_t GetValueTypeSize(uint8_t ValueType);


/** Challenge 2 variant type value
 *
 * @param[in]  LeftValueOp 	 : 	The Left value operant for challenge
 * @param[in]  RightValueOp  : 	The Rigth value operant for challenge
 * @param[in]  Operator 	 : 	The challenge operator (see eConditionOperator)
 *
 * @return @ref int8_t Invalid value = -1, Challenge failed = 0, challenge success = 1
 */
extern int8_t ValueTypeChallengeWithOperator(TypeValue_t* LeftValueOp, TypeValue_t* RightValueOp, uint8_t Operator);
#endif


#ifdef __cplusplus
} /*extern "C" */
#endif

#endif //DK_JOB_H
