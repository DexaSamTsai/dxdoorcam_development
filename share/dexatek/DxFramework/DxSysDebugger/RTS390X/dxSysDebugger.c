//============================================================================
// File: dxSysdebugger.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#ifndef __IAR_SYSTEMS_ICC__
#include <malloc.h>
#endif // __IAR_SYSTEMS_ICC__
#include <stdarg.h>
#include <sys/sysinfo.h>
#include "Utils.h"
#include "dxSysDebugger.h"
#include "dxSysDebugger_Utils.h"


#if ENABLE_SYSTEM_DEBUGGER
#pragma message ( "REMINDER: DK System Diagnostic is ENABLED!" )
#elif ENABLE_SYSTEM_LOG
#pragma message ( "WARNING: LOG Debugger Enabled while System Diagnostic is disabled. System Diagnostic is required for Log" )
#endif

void WPRINT_APP_INFO_UART(const char *fmt, ...);

/******************************************************
 *                      Macros
 ******************************************************/
#define SYS_DEBUGGER_PROCESS_THREAD_PRIORITY                    DXTASK_PRIORITY_IDLE
#define SYS_DEBUGGER_PROCESS_THREAD_NAME                        "Sys Debugger Process"
#define SYS_DEBUGGER_STACK_SIZE                                 (30*1024) //1024  //Since we are using PURE print and limited, we can reduce this to 2KB or even less depending on actual result base on RTOS and platform

#define MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED             80 //20  //Usually for Log Queue

#define SYS_DEBUGGER_GLOBAL_MESSAGE_QUEUE_NAME                  "/dxSysDbgGlobalMQ"

/******************************************************
 *                    Constants
 ******************************************************/

#define SYSTEM_DIAGNOSTIC_TIMER_TICK                    (1*SECONDS)

#if SYS_DBG_LOG_OUTPUT_TO_UART

#define WPRINT_APP_INFO(x)                              do {WPRINT_APP_INFO_UART x;} while(0==1)

#else

#define WPRINT_APP_INFO(x)                              do {printf x;} while(0==1)

#endif

/******************************************************
 *                   Enumerations
 ******************************************************/


typedef enum
{

    SYS_DBG_NO_JOB                = 0,
    SYS_DBG_LOG_OUTPUT,
    SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING,
    SYS_DBG_THREAD_STACK_CHECK,
    SYS_DBG_MEM_HEAP_CHECK,
    SYS_DBG_CPU_USAGE_CHECK,
    SYS_DBG_LOG_ARRAY_FULL_WARNING,
    SYS_DBG_LOG_ARRAY_QUEUE_FULL_WARNING,

} SysDebugg_Job_t;


/******************************************************
 *                 Type Definitions
 ******************************************************/

#define G_SYS_DBG_CATEGORY_BITS                         12  /**
                                                               12 indicate that bit15 ~ bit4 is for data type GSysDebugger_category_t;
                                                               the others, bit3 ~ bit0, is for G_SYS_DBG_LEVEL_NNNNNNNN
                                                               If G_SYS_DBG_CATEGORY_MAX_VALUE exceeds 16, the value of G_SYS_DBG_CATEGORY_BITS must be increased.
                                                             */

typedef struct
{
    SysDebugg_Job_t     JobType;
    dxTime_t            JobCreatedSysTime;
    uint8_t             JobData[LOG_MAX_CHAR_BUFFER];
    uint8_t/*dxBOOL*/   DebuggIncludeHeader;                // dxBOOL will occupied 4 bytes space in this case
    uint8_t             SlotNum;
    uint16_t            Type;   //
                                // +---+---+---+---+---+---+---+---+---+---+
                                // | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
                                // +---+---+---+---+---+---+---+---+---+---+
                                //   ^   ^   ^   ^   ^   ^       ^   ^   ^
                                //   |   |   |   |   |   |       |   |   |
                                //   |   |   |   |   |   |       +---+---+---- G_SYS_DBG_LEVEL_NNNNNNNN
                                //   +---+---+---+---+---+-------------------- GSysDebugger_category_t, in case of G_SYS_DBG_CATEGORY_BITS equals to 4
                                //

} SysDbgJobQueue_t; //This MUST be sent as pointer instead of whole data


typedef struct
{
    dxTaskHandle_t*         pSysDbgThreadObj;
    char*                   pThreadName;

} SysDbgThreadDiagnose_t;


typedef struct _tagSysDbgIdNameTable {
    uint8_t     id;
    char        *pszName;
} SysDbgIdNameTable;



/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

static TASK_RET_TYPE Sys_Debugger_Process_thread_main( TASK_PARAM_TYPE arg );

static void SystemDiagnosticTimeTickHandler(void* arg);

static void print_System_memory( void );

#if (ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC || ENABLE_SYSTEM_MEMORY_DIASGNOSTIC)

static void SystemJobMessageGenericSend(SysDbgJobQueue_t* SystemDbgJobMsg);

#endif

static dxOS_RET_CODE rtos_check_stack( dxTaskHandle_t* thread,char* GivenName);

#endif

/******************************************************
 *               Variable Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

static dxUART_t*                    SysDbgUartHandler = NULL;

static SysDbgThreadDiagnose_t*      SysDbgThreadCheckList;

static dxTaskHandle_t               SysDebuggerMainProcess_thread;

static dxQueueHandle_t              SysDebuggerJobQueue;

static dxTimerHandle_t              SystemDiagnosticTimer;

static dxMutexHandle_t              LogBufferMutexProtect = NULL;

static char                         LogStrBuffer[LOG_MAX_CHAR_BUFFER];

static dxBOOL                      SysDebuggerServerEnabled = dxFALSE;

static uint8_t SysDebuggerLogLevel = G_SYS_DBG_LEVEL_FATAL_ERROR | G_SYS_DBG_LEVEL_GENERAL_INFORMATION;
static uint8_t SysDebuggerCategory[G_SYS_DBG_CATEGORY_MAX_VALUE];

static const SysDbgIdNameTable CategoryName[] =
{
    { G_SYS_DBG_CATEGORY_UNKNOWN,                           "UN" },
    { G_SYS_DBG_CATEGORY_BLEMANAGER,                        "BM" },
    { G_SYS_DBG_CATEGORY_JOBMANAGER,                        "JM" },
    { G_SYS_DBG_CATEGORY_LEDMANAGER,                        "LM" },
    { G_SYS_DBG_CATEGORY_SERVERMANAGER,                     "SM" },
    { G_SYS_DBG_CATEGORY_HTTPMANAGER,                       "HM" },
    { G_SYS_DBG_CATEGORY_SFLASHMANAGER,                     "FM" },
    { G_SYS_DBG_CATEGORY_UDPCOMMMANAGER,                    "UM" },
    { G_SYS_DBG_CATEGORY_WIFIMANAGER,                       "WM" },
    { G_SYS_DBG_CATEGORY_BLEMANAGER_EVENTREPORTHANDLER,     "BE" },
    { G_SYS_DBG_CATEGORY_JOBMANAGER_EVENTREPORTHANDLER,     "JE" },
    { G_SYS_DBG_CATEGORY_WIFIMANAGER_EVENTREPORTHANDLER,    "WE" },
    { G_SYS_DBG_CATEGORY_UDPCOMMMANAGER_EVENTREPORTHANDLER, "UE" },
    { G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER,  "SE" },
    { G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER,    "HE" },
    { G_SYS_DBG_CATEGORY_STATE_MANAGER,                     "ST" },
    { G_SYS_DBG_CATEGORY_FW_UPDATE,                         "FW" },
    { G_SYS_DBG_CATEGORY_OFFLINE,                           "OF" },
    { G_SYS_DBG_CATEGORY_ONLINE,                            "OL" },
    { G_SYS_DBG_CATEGORY_SETUP,                             "SU" },
    { G_SYS_DBG_CATEGORY_IDLE,                              "IL" },
    { G_SYS_DBG_CATEGORY_REGISTER,                          "RG" },
    { G_SYS_DBG_CATEGORY_FATAL,                             "FT" },
    { G_SYS_DBG_CATEGORY_MAIN_APP,                          "MN" },
};

static const SysDbgIdNameTable LevelName[] =
{
    { G_SYS_DBG_LEVEL_FATAL_ERROR,                          "FE" },
    { G_SYS_DBG_LEVEL_GENERAL_INFORMATION,                  "GI" },
    { G_SYS_DBG_LEVEL_DETAIL_INFORMATION,                   "DI" }
};

char* GetSysDbgCategoryName (GSysDebugger_category_t category)
{
    char *ret = NULL;
    int i = 0;
    for (i = 0; i < sizeof(CategoryName) / sizeof(CategoryName[0]); i++)
    {
        if (CategoryName[i].id == (uint8_t)category)
        {
            ret = CategoryName[i].pszName;
            break;
        }
    }
    return ret;
}

char* GetSysDbgLevelName (uint8_t level)
{
    char *ret = NULL;
    int i = 0;
    for (i = 0; i < sizeof(LevelName) / sizeof(LevelName[0]); i++)
    {
        if (LevelName[i].id == (uint8_t)level)
        {
            ret = LevelName[i].pszName;
            break;
        }
    }
    return ret;
}

#endif

/******************************************************
 *               Local Function Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

#if SYS_DBG_LOG_OUTPUT_TO_UART

void WPRINT_APP_INFO_UART(const char *fmt, ...)
{
    if (SysDbgUartHandler)
    {
        va_list ap;
        char UartLogbuf[LOG_MAX_CHAR_BUFFER*2];
        memset(UartLogbuf, 0, sizeof(UartLogbuf));
        va_start(ap, fmt);
        _vsnprintf(UartLogbuf, LOG_MAX_CHAR_BUFFER*2, fmt, ap);
        va_end(ap);
        dxUART_Send_Blocked(SysDbgUartHandler, (uint8_t*)UartLogbuf, strlen(UartLogbuf));
    }
}

#endif //SYS_DBG_LOG_OUTPUT_TO_UART

#if (ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC || ENABLE_SYSTEM_MEMORY_DIASGNOSTIC)

void SystemJobMessageGenericSend(SysDbgJobQueue_t* SystemDbgJobMsg)
{
    if (SystemDbgJobMsg)
    {
        if (SysDebuggerJobQueue != NULL)
        {
            if (dxQueueIsFull(SysDebuggerJobQueue) == dxTRUE)
            {
                //Queue is FULL, NO PRINT ALLOWED IN THIS FUNCTION.. We simply skip
                SystemDbgJobMsg->JobData[0] = NULL_STRING;      // useless data
            }
            else if (dxQueueSend( SysDebuggerJobQueue, SystemDbgJobMsg, 1000 ) != DXOS_SUCCESS)
            {
                //Error, NO PRINT ALLOWED IN THIS FUNCTION.. We simply skip
                SystemDbgJobMsg->JobData[0] = NULL_STRING;      // useless data
            }

        }
    }
}

#endif

void SystemDiagnosticTimeTickHandler(void* arg)
{
    static dxTime_t LastMemDiagnosedTime = 0;
    static dxTime_t LastThreadStackDiagnosedTime = 0;

    dxTime_t CurrentSystemTime = dxTimeGetMS();

    //First time initialisation
    if ((LastMemDiagnosedTime == 0) || (LastThreadStackDiagnosedTime == 0))
    {
        LastMemDiagnosedTime = CurrentSystemTime;
        LastThreadStackDiagnosedTime = CurrentSystemTime;
    }

#if ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC

    if ((CurrentSystemTime - LastThreadStackDiagnosedTime) > THREAD_STACK_DIAGNOSTIC_INTERVAL)
    {
        SysDbgJobQueue_t    JobMessage;
        JobMessage.JobType = SYS_DBG_THREAD_STACK_CHECK;
        JobMessage.JobCreatedSysTime = CurrentSystemTime;
        JobMessage.JobData[0] = NULL_STRING;      // useless data
        JobMessage.SlotNum = 0; //NOT Longer Used in this port
        JobMessage.Type = 0;

        SystemJobMessageGenericSend(&JobMessage);

        LastThreadStackDiagnosedTime = CurrentSystemTime;
    }

#endif


#if ENABLE_SYSTEM_MEMORY_DIASGNOSTIC

    if ((CurrentSystemTime - LastMemDiagnosedTime) > MEMORY_DIAGNOSTIC_INTERVAL)
    {
        SysDbgJobQueue_t    JobMessage;
        JobMessage.JobType = SYS_DBG_MEM_HEAP_CHECK;
        JobMessage.JobCreatedSysTime = CurrentSystemTime;
        JobMessage.JobData[0] = NULL_STRING;      // useless data
        JobMessage.SlotNum = 0; //NOT Longer Used in this port
        JobMessage.Type = 0;

        SystemJobMessageGenericSend(&JobMessage);

        LastMemDiagnosedTime = CurrentSystemTime;
    }

#endif

    if (dxTimerIsTimerActive(SystemDiagnosticTimer) == dxTRUE)
    {
        dxTimerStop(SystemDiagnosticTimer, DXOS_WAIT_FOREVER);
    }

    if (dxTimerStart(SystemDiagnosticTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
    {
        WPRINT_APP_INFO( ( "WARNING - Unable to re-start SystemDiagnosticTimer\r\n") );
    }
}

static TASK_RET_TYPE Sys_Debugger_Process_thread_main( TASK_PARAM_TYPE arg )
{
    SystemDiagnosticTimer = dxTimerCreate("",
                                          SYSTEM_DIAGNOSTIC_TIMER_TICK,
                                          dxTRUE,
                                          &SystemDiagnosticTimer,
                                          SystemDiagnosticTimeTickHandler);
    if (SystemDiagnosticTimer != NULL)
    {
        if (dxTimerStart(SystemDiagnosticTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
            WPRINT_APP_INFO( ( "WARNING - Unable to re-start SystemDiagnosticTimer\r\n") );
    }

    else
    {
        WPRINT_APP_INFO( ( "WARNING - System Debugger unable to create RTOS timer\r\n") );
    }

    while (1)
    {

        SysDbgJobQueue_t *pGetJobBuff = dxMemAlloc("", 1, sizeof(SysDbgJobQueue_t));

        if (dxQueueReceive( SysDebuggerJobQueue, pGetJobBuff, DXOS_WAIT_FOREVER ) == DXOS_SUCCESS)
        {
            if (pGetJobBuff)
            {
                int category = pGetJobBuff->Type >> (sizeof(pGetJobBuff->Type) * 8 - G_SYS_DBG_CATEGORY_BITS);
                int level = pGetJobBuff->Type & G_SYS_DBG_LEVEL_ALL;
                switch(pGetJobBuff->JobType)
                {
                    case SYS_DBG_LOG_OUTPUT:
                    {
                        if(pGetJobBuff->JobData[0] != NULL_STRING)
                        {
                            GSysDebugger_MulticastSend(pGetJobBuff->JobData, strlen((char*)pGetJobBuff->JobData));

                            if (pGetJobBuff->DebuggIncludeHeader == dxTRUE)
                            {
                                WPRINT_APP_INFO( ( "[SYSDBG][T%d][%s][%s]%s", pGetJobBuff->JobCreatedSysTime,
                                                                               GetSysDbgCategoryName(category),
                                                                               GetSysDbgLevelName(level),
                                                                               pGetJobBuff->JobData) );
                            }
                            else
                            {
                                WPRINT_APP_INFO( ( "%s", pGetJobBuff->JobData) );
                            }
                        }
                    }
                        break;

                    case SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Queue Full!\r\n", (unsigned int)pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    case SYS_DBG_THREAD_STACK_CHECK:
                    {
                        WPRINT_APP_INFO( ( "------------------System Dbg Thread Stack Diagnostic-------------------\r\n") );

                        uint16_t i = 0;
                        for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
                        {
                            if (SysDbgThreadCheckList[i].pSysDbgThreadObj != NULL)
                            {
                                rtos_check_stack( SysDbgThreadCheckList[i].pSysDbgThreadObj, SysDbgThreadCheckList[i].pThreadName);
                            }
                        }

                        WPRINT_APP_INFO( ( "-------------------------------END-------------------------------------\r\n") );

                    }
                        break;

                    case SYS_DBG_MEM_HEAP_CHECK:
                    {
                        print_System_memory();
                    }
                        break;

                    case SYS_DBG_LOG_ARRAY_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Arrays Full!\r\n", pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    case SYS_DBG_LOG_ARRAY_QUEUE_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Arrays Full!\r\n", pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    default:
                        break;
                }

                pGetJobBuff->JobData[0] = NULL_STRING;
                dxMemFree(pGetJobBuff);
                pGetJobBuff = NULL;
            }
        }
    }

    END_OF_TASK_FUNCTION;
}


static void print_System_memory( void )
{

    WPRINT_APP_INFO( ("\r\n-----------------SYS DBG GLOBAL MEM HEAP DIAGNOSTIC-----------------\r\n") );


    /*
    struct sysinfo {
      long uptime;             // Seconds since boot
      unsigned long loads[3];  // 1, 5, and 15 minute load averages
      unsigned long totalram;  // Total usable main memory size
      unsigned long freeram;   // Available memory size
      unsigned long sharedram; // Amount of shared memory
      unsigned long bufferram; // Memory used by buffers
      unsigned long totalswap; // Total swap space size
      unsigned long freeswap;  // swap space still available
      unsigned short procs;    // Number of current processes
      char _f[22];             // Pads structure to 64 bytes
    };
    */

    struct sysinfo info;

    if ( sysinfo( &info ) == 0 ) {
        WPRINT_APP_INFO( ("up time:    %l\n", info.uptime) );
        WPRINT_APP_INFO( ("loads:      %l %l %l\n", info.loads[0], info.loads[1], info.loads[2]) );
        WPRINT_APP_INFO( ("total ram:  %l\n", info.totalram) );
        WPRINT_APP_INFO( ("free ram:   %l\n", info.freeram) );
        WPRINT_APP_INFO( ("shared ram: %l\n", info.sharedram) );
        WPRINT_APP_INFO( ("buffer ram: %l\n", info.bufferram) );
        WPRINT_APP_INFO( ("total swap: %l\n", info.totalswap) );
        WPRINT_APP_INFO( ("free swap:  %l\n", info.freeswap) );
        WPRINT_APP_INFO( ("process #:  %u\n", info.procs) );
    }

    // VmPeak: Peak virtual memory usage
    // VmSize: Current virtual memory usage
    // VmLck: Current mlocked memory
    // VmHWM: Peak resident set size
    // VmRSS: Resident set size
    // VmData: Size of "data" segment
    // VmStk: Size of stack
    // VmExe: Size of "text" segment
    // VmLib: Shared library usage
    // VmPTE: Pagetable entries size
    // VmSwap: Swap space used

	FILE *fp;
	char buf[64];

	fp = fopen("/proc/self/status","r");
	if (fp)
	{
        WPRINT_APP_INFO( ("\r\n-------------SYS DBG CURRENT PROCESS MEM HEAP DIAGNOSTIC------------\r\n") );

    	while (1)
    	{
    		memset(buf, 0x00, sizeof(buf));
    		if (fgets(buf, sizeof(buf), fp) == NULL)
    		{
    			break;
            }
    		if (strncmp(buf, "Vm", 2) == 0)
    		{
    			WPRINT_APP_INFO( ("%s", buf) );
    		}
    	}

	    fclose(fp);
	}


    WPRINT_APP_INFO( ("\r\n------------------------------END-----------------------------------\r\n") );
}


dxOS_RET_CODE rtos_check_stack( dxTaskHandle_t* thread, char* GivenName)
{

    if (thread == NULL)
    {
        return DXOS_ERROR;
    }

#if 0 //TODO

    dxTaskStack_t StackBuf;

    memset(&StackBuf, 0x00, sizeof(StackBuf));

    if (dxTaskGetStackInfo(*thread, &StackBuf) != DXOS_SUCCESS)
    {
        return DXOS_ERROR;
    }

    if (GivenName)
    {
        WPRINT_APP_INFO( ( "[SYSDBG][STACK][%s] ", GivenName) );
    }
    else
    {
        WPRINT_APP_INFO( ( "[SYSDBG][STACK][%s] ", StackBuf.szTaskName) );
    }

    if (StackBuf.nStackSize == 0)
    {
        WPRINT_APP_INFO( ( "Total Stack Size is NA (Available %ld) bytes\r\n", StackBuf.nFreeStackSpace) );
    }
    else
    {
        WPRINT_APP_INFO( ( "Total Stack Size is %ld (Available %ld) bytes\r\n", StackBuf.nStackSize, StackBuf.nFreeStackSpace) );
    }

#endif

    return DXOS_SUCCESS;
}

#endif

/******************************************************
 *               Function Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

GSysDebugger_result_t GSysDebuggerInit(void)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;
    dxOS_RET_CODE dxOSResult = DXOS_SUCCESS;

    SysDbgThreadCheckList = (SysDbgThreadDiagnose_t *)dxMemAlloc( "", MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC, sizeof(SysDbgThreadDiagnose_t));
    memset(SysDbgThreadCheckList, 0, sizeof(SysDbgThreadDiagnose_t) * MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC);

    memset(LogStrBuffer, 0, LOG_MAX_CHAR_BUFFER);

    if (SysDebuggerJobQueue == NULL)
    {

        SysDebuggerJobQueue = dxQueueGlobalCreate(  SYS_DEBUGGER_GLOBAL_MESSAGE_QUEUE_NAME,
                                                    MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED,
                                                    sizeof(SysDbgJobQueue_t));

        if (SysDebuggerJobQueue == NULL)
        {
            result = G_SYS_DBG_DX_RTOS_ERROR;

            goto EndInit;
        }
    }

#if SYS_DBG_LOG_OUTPUT_TO_UART
    SysDbgUartHandler = dxUART_Open(SYS_DBG_LOG_UART_PORT, 57600, DXUART_PARITY_NONE, 8, 1, DXUART_FLOWCONTROL_NONE, NULL);
    if (SysDbgUartHandler == NULL)
    {
        dxQueueDelete(SysDebuggerJobQueue);

        result = G_SYS_DBG_DX_GENERIC_ERROR;

        goto EndInit;
    }
#endif

    dxOSResult = dxTaskCreate(Sys_Debugger_Process_thread_main,
                              SYS_DEBUGGER_PROCESS_THREAD_NAME,
                              SYS_DEBUGGER_STACK_SIZE,
                              NULL,
                              SYS_DEBUGGER_PROCESS_THREAD_PRIORITY,
                              &SysDebuggerMainProcess_thread);

    if (dxOSResult != DXOS_SUCCESS)
    {
        dxQueueDelete(SysDebuggerJobQueue);

        result = G_SYS_DBG_DX_RTOS_ERROR;

        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&SysDebuggerMainProcess_thread);

    SysDebuggerServerEnabled = dxTRUE;


    // Open all category by default
    memset (SysDebuggerCategory, 1, G_SYS_DBG_CATEGORY_MAX_VALUE);

EndInit:
    return result;


}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */

    return GSysDebugger_RegisterForThreadDiagnosticGivenName(thread, NULL);

}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName)
{
    //We only support one server (eg. the process who first call ssy debug init and can only be one or there will be resource fight)
    //TODO: If thread stack disgnostic is required for other process that is using sysdebugger then we will need to think of some
    //      other method.
    if (SysDebuggerServerEnabled == dxFALSE)
        return G_SYS_DBG_DX_GENERIC_ERROR;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    uint16_t i = 0;
    for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
    {
        if (SysDbgThreadCheckList[i].pSysDbgThreadObj == NULL)
        {
            SysDbgThreadCheckList[i].pSysDbgThreadObj = thread;

            if (ThreadName)
            {
                int16_t LogStrLen = strlen(ThreadName);
                if (LogStrLen > 0)
                {
                    //Limit the Thread name to 31
                    if (LogStrLen > 31)
                        LogStrLen = 31;

                    char* pGivenThreadNameBuff = dxMemAlloc( "SysDbg Thread Name Buff", 1, LogStrLen + 1);
                    memset(pGivenThreadNameBuff, 0, LogStrLen + 1);

                    memcpy(pGivenThreadNameBuff, ThreadName, LogStrLen);
                    pGivenThreadNameBuff[LogStrLen] = NULL_STRING;

                    SysDbgThreadCheckList[i].pThreadName = pGivenThreadNameBuff;

                }
            }

            return result;
        }
    }

    return G_SYS_DBG_MAX_THREAD_REGISTRATION_REACHED;

}

GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    uint16_t i = 0;
    for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
    {
        if (SysDbgThreadCheckList[i].pSysDbgThreadObj == thread)
        {
            SysDbgThreadCheckList[i].pSysDbgThreadObj = NULL;

            if (SysDbgThreadCheckList[i].pThreadName)
            {
                dxMemFree(SysDbgThreadCheckList[i].pThreadName);
                SysDbgThreadCheckList[i].pThreadName = NULL;
            }

            return result;
        }
    }

    return G_SYS_DBG_MAX_THREAD_REGISTRATION_REACHED;

}


GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    if ((SysDebuggerLogLevel & level) == 0) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    if ((category >= G_SYS_DBG_CATEGORY_MAX_VALUE) || !SysDebuggerCategory[category]) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }
    {
        GSysDebugger_RetainBuffPermissionAccess();

        va_list ap;
        char *buf = GSysDebugger_GetBuffForLog();
        /* See http://www.ijs.si/software/snprintf/ for portable
         * implementation of snprintf.
         */
        va_start(ap, fmt);
        _vsnprintf(buf, LOG_MAX_CHAR_BUFFER, fmt, ap);
        va_end(ap);
        if (LOG_MAX_CHAR_BUFFER > 0)
        {
            buf[LOG_MAX_CHAR_BUFFER - 1] = '\0';
        }

        GSysDebugger_ReleaseBuffPermissionAccess();
        GSysDebugger_Log(buf, dxTRUE, category,level);
    }

    return G_SYS_DBG_SUCCESS;
}


GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    if ((SysDebuggerLogLevel & level) == 0) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    if ((category >= G_SYS_DBG_CATEGORY_MAX_VALUE) || !SysDebuggerCategory[category]) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    GSysDebugger_RetainBuffPermissionAccess();

    va_list ap;
    char *buf = GSysDebugger_GetBuffForLog();
    /* See http://www.ijs.si/software/snprintf/ for portable
     * implementation of snprintf.
     */
    va_start(ap, fmt);
    _vsnprintf(buf, LOG_MAX_CHAR_BUFFER, fmt, ap);
    va_end(ap);
    if (LOG_MAX_CHAR_BUFFER > 0)
    {
        buf[LOG_MAX_CHAR_BUFFER - 1] = '\0';
    }

    GSysDebugger_ReleaseBuffPermissionAccess();
    GSysDebugger_Log(buf, dxFALSE, category,level);

    return G_SYS_DBG_SUCCESS;
}

GSysDebugger_result_t GSysDebugger_Log(char* pLogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    //Get the global MQ (check if it exist)
    if (SysDebuggerJobQueue == NULL)
    {
        SysDebuggerJobQueue = dxQueueGlobalGet(SYS_DEBUGGER_GLOBAL_MESSAGE_QUEUE_NAME);
        //Unable to obtain global MQ?
        if (SysDebuggerJobQueue == NULL)
        {
            result = G_SYS_DBG_DISABLED;
            goto GdbLogExit;
        }
    }

    if ((SysDebuggerLogLevel & level) == 0 ||
        category >= G_SYS_DBG_CATEGORY_MAX_VALUE)
    {
        result = G_SYS_DBG_INVALID_PARAMETER;
        goto GdbLogExit;
    }

    int16_t LogStrLen = strlen(pLogStrBuffer);

    if (LogStrLen > 0)
    {
        if (LogStrLen > LOG_MAX_CHAR_BUFFER)
            LogStrLen = LOG_MAX_CHAR_BUFFER;

        dxTime_t CurrentSystemTime = dxTimeGetMS();

        SysDbgJobQueue_t    JobMessage;
        JobMessage.JobType = SYS_DBG_LOG_OUTPUT;
        JobMessage.JobCreatedSysTime = CurrentSystemTime;
        JobMessage.DebuggIncludeHeader = IncludeHeader;
        JobMessage.SlotNum = 0; //NOT Longer Used in this port
        JobMessage.Type = (category <<  (sizeof(JobMessage.Type) * 8 - G_SYS_DBG_CATEGORY_BITS)) |
                                        (level & G_SYS_DBG_LEVEL_ALL);

        memcpy( JobMessage.JobData, pLogStrBuffer, LogStrLen);
        JobMessage.JobData[LogStrLen] = NULL_STRING;

        if (SysDebuggerJobQueue != NULL)
        {
            if (dxQueueSpacesAvailable(SysDebuggerJobQueue) == 1)
            {
                JobMessage.JobType = SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING;
                JobMessage.JobData[0] = NULL_STRING;
            }

            if (dxQueueSend( SysDebuggerJobQueue, &JobMessage, 1000 ) != DXOS_SUCCESS)
            {
                result = G_SYS_DBG_DX_RTOS_ERROR;
            }
        }
    }

GdbLogExit:
    return result;
}


char* GSysDebugger_GetBuffForLog()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */

    return LogStrBuffer;
}

GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogBufferMutexProtect == NULL)
    {
    	LogBufferMutexProtect = dxMutexCreate();
    }

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    dxMutexTake(LogBufferMutexProtect, DXOS_WAIT_FOREVER);

    return result;

}

GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogBufferMutexProtect == NULL)
    {
    	LogBufferMutexProtect = dxMutexCreate();
    }

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    dxMutexGive(LogBufferMutexProtect);

    return result;

}

GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level)
{

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    if ((level & G_SYS_DBG_LEVEL_ALL) == 0)
    {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    SysDebuggerLogLevel = level;

    return result;
}

uint8_t GSysDebugger_ReadLogLevel(void)
{
    return SysDebuggerLogLevel;
}

#if MULTICAST_UDP_DEBUG_OUTPUT
#pragma message ( "REMINDER: Multicast UDP debug NEED to disable while production!" )
// TODO: NOT well TEST.
static int sfd = -1;
static const char *group_ip = "239.239.239.239";
static const uint16_t port = 23456;
#endif // MULTICAST_UDP_DEBUG_OUTPUT

int GSysDebugger_MulticastInit(void)
{
    int ret = 0;

#if MULTICAST_UDP_DEBUG_OUTPUT
    // To TEST, run another UDP multicast client before testing
    //
    // 32. Create socket
    sfd = dxMCAST_Create();

    printf("\r\ndxMCAST_Create(): %d\r\n", sfd);

    // 33. Join to multicast group
    ret = dxMCAST_Join(sfd, group_ip, port);

    printf("\r\ndxMCAST_AddMembership(): %d\r\n", ret);
#endif // MULTICAST_UDP_DEBUG_OUTPUT

    return ret;
}

int GSysDebugger_MulticastSend(uint8_t *pData, int len)
{
    int ret = 0;
#if MULTICAST_UDP_DEBUG_OUTPUT
    struct sockaddr_in dest;

    if (sfd < 0) return -1;

    // 34. Send to group
    memset(&dest, 0x00, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    dest.sin_addr.s_addr = inet_addr(group_ip);

    ret = dxMCAST_SendTo(sfd, pData, len, 0, &dest, sizeof(dest));
#endif // MULTICAST_UDP_DEBUG_OUTPUT
    return ret;
}


uint8_t* GSysDebugger_GetDebuggerLevel (void) {
    return &SysDebuggerLogLevel;
}


uint8_t* GSysDebugger_GetDebuggerCategory (void) {
    return SysDebuggerCategory;
}


GSysDebugger_result_t GSysDebugger_Dump (
    const char* title,
    int len,
    uint8_t* data
) {
    // TODO: to implement this function please refer relative "Ameba" file.
    return G_SYS_DBG_SUCCESS;
}


GSysDebugger_result_t GSysDebugger_DumpString (
    int len,
    uint8_t* buffer
) {
    // TODO: to implement this function please refer relative "Ameba" file.
    return G_SYS_DBG_SUCCESS;
}

#else

GSysDebugger_result_t GSysDebuggerInit(void)
{
    return G_SYS_DBG_DISABLED;
}

extern GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_Log(char* LogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level)
{
    return G_SYS_DBG_DISABLED;
}

char* GSysDebugger_GetBuffForLog()
{
    return NULL;

}

GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess()
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess()
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level)
{
    return G_SYS_DBG_DISABLED;
}

uint8_t GSysDebugger_ReadLogLevel(void)
{
    return 0;
}

int GSysDebugger_MulticastInit(void)
{
    return G_SYS_DBG_DISABLED;
}

int GSysDebugger_MulticastSend(uint8_t *pData, int len)
{
    return G_SYS_DBG_DISABLED;
}


uint8_t* GSysDebugger_GetDebuggerLevel (void) {
    return NULL;
}


uint8_t* GSysDebugger_GetDebuggerCategory (void) {
    return NULL;
}


GSysDebugger_result_t GSysDebugger_Dump (
    const char* title,
    int len,
    uint8_t* data
) {
    return G_SYS_DBG_DISABLED;
}


GSysDebugger_result_t GSysDebugger_DumpString (
    int len,
    uint8_t* buffer
) {
    return G_SYS_DBG_DISABLED;
}

#endif
