2016-01-14
 - Checkout V1.0.26 source code from Bitbucket.
 - Porting to IAR/Realtek environment
   + Define DK_OS_WRAPPER in dxOS.h
   + Define DK_GATEWAY in [Defined symbols] section of IAR compiler
 - Some files are changed for IAR/Realtek environment
   + bsd-base64.c
   + GatewaySysDebugger.h
   + GatewaySysDebugger.c
   + dk_Network.c
