//============================================================================
// File: dxSysdebugger.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>
#ifndef __IAR_SYSTEMS_ICC__
#include <malloc.h>
#endif // __IAR_SYSTEMS_ICC__
#include <stdarg.h>
// NOTE: Porting to dxOS
//#include "wiced.h"
#include "dxOS.h"
#include "Utils.h"
#include "dxSysDebugger.h"

#if ENABLE_SYSTEM_DEBUGGER
#pragma message ( "REMINDER: DK System Diagnostic is ENABLED!" )
#elif ENABLE_SYSTEM_LOG
#pragma message ( "WARNING: LOG Debugger Enabled while System Diagnostic is disabled. System Diagnostic is required for Log" )
#endif



/******************************************************
 *                      Macros
 ******************************************************/

#define SYS_DEBUGGER_PROCESS_THREAD_PRIORITY                    DXTASK_PRIORITY_IDLE
#define SYS_DEBUGGER_PROCESS_THREAD_NAME                        "Sys Debugger Process"
#define SYS_DEBUGGER_STACK_SIZE                                 896  //Since we are using PURE print and limited, we can reduce this to 2KB or even less depending on actual result base on RTOS and platform

#define MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED             80 //20  //Usually for Log Queue
#define SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE                        16
#define SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE                  96



/******************************************************
 *                    Constants
 ******************************************************/

#define SYSTEM_DIAGNOSTIC_TIMER_TICK                    (1*SECONDS)

#define WPRINT_APP_INFO(x)                              do {printf x;} while(0==1)



/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

    SYS_DBG_NO_JOB                = 0,
    SYS_DBG_LOG_OUTPUT,
    SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING,
    SYS_DBG_THREAD_STACK_CHECK,
    SYS_DBG_MEM_HEAP_CHECK,
    SYS_DBG_CPU_USAGE_CHECK,
    SYS_DBG_LOG_ARRAY_FULL_WARNING,
    SYS_DBG_LOG_ARRAY_QUEUE_FULL_WARNING,

} SysDebugg_Job_t;



/******************************************************
 *                 Type Definitions
 ******************************************************/

#define G_SYS_DBG_CATEGORY_BITS                         12  /**
                                                               12 indicate that bit15 ~ bit4 is for data type GSysDebugger_category_t;
                                                               the others, bit3 ~ bit0, is for G_SYS_DBG_LEVEL_NNNNNNNN
                                                               If G_SYS_DBG_CATEGORY_MAX_VALUE exceeds 16, the value of G_SYS_DBG_CATEGORY_BITS must be increased.
                                                             */

typedef struct
{
    SysDebugg_Job_t     JobType;
    dxTime_t            JobCreatedSysTime;
    uint8_t             JobData[LOG_MAX_CHAR_BUFFER];
    uint8_t/*dxBOOL*/   DebuggIncludeHeader;                // dxBOOL will occupied 4 bytes space in this case
    uint8_t             SlotNum;
    uint16_t            Type;   //
                                // +---+---+---+---+---+---+---+---+---+---+
                                // | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
                                // +---+---+---+---+---+---+---+---+---+---+
                                //   ^   ^   ^   ^   ^   ^       ^   ^   ^
                                //   |   |   |   |   |   |       |   |   |
                                //   |   |   |   |   |   |       +---+---+---- G_SYS_DBG_LEVEL_NNNNNNNN
                                //   +---+---+---+---+---+-------------------- GSysDebugger_category_t, in case of G_SYS_DBG_CATEGORY_BITS equals to 4
                                //

} SysDbgJobQueue_t; //This MUST be sent as pointer instead of whole data


typedef struct
{
    dxBOOL              SlotAvailable;
    SysDbgJobQueue_t    PoolQueue;

} SysDbgJobPool_t;


typedef struct
{
    dxTaskHandle_t*         pSysDbgThreadObj;
    char*                   pThreadName;

} SysDbgThreadDiagnose_t;


typedef struct _tagSysDbgIdNameTable {
    uint8_t     id;
    char        *pszName;
} SysDbgIdNameTable;



/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

static void Sys_Debugger_Process_thread_main( uint32_t arg );

static void SystemDiagnosticTimeTickHandler(void* arg);

static GSysDebugger_result_t _sendLogEvent(char* LogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level);

static void _printSystemMemory(void);

static void _printCpuUsage (void);

#if (ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC || ENABLE_SYSTEM_MEMORY_DIASGNOSTIC)
static void SystemJobMessageGenericSend(SysDbgJobQueue_t* SystemDbgJobMsg);
#endif

static dxOS_RET_CODE rtos_check_stack( dxTaskHandle_t* thread,char* GivenName);

#endif

/******************************************************
 *               Variable Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

static dxBOOL                       LogPinDisabled = dxTRUE;
static SysDbgThreadDiagnose_t*      SysDbgThreadCheckList;
static dxTaskHandle_t               SysDebuggerMainProcess_thread;
static dxQueueHandle_t              SysDebuggerJobQueue;
static dxTimerHandle_t              SystemDiagnosticTimer;
static dxSemaphoreHandle_t          LogBufferSemaphore;
static dxSemaphoreHandle_t          cpuUsageBufferSemaphore;
static char*                        LogStrBuffer;

#pragma default_variable_attributes = @ ".sdram.text"
static SysDbgJobPool_t* SysDbgQueuePool;
static char* cpuUsageBuffer;
#pragma default_variable_attributes =

#ifdef ENABLE_DEBUG_MESSAGE_QUEUE
static dxTaskHandle_t           Test_thread;
#endif

static uint8_t SysDebuggerLogLevel = G_SYS_DBG_LEVEL_FATAL_ERROR | G_SYS_DBG_LEVEL_GENERAL_INFORMATION;
static uint8_t SysDebuggerCategory[G_SYS_DBG_CATEGORY_MAX_VALUE];

static const SysDbgIdNameTable CategoryName[] =
{
    { G_SYS_DBG_CATEGORY_UNKNOWN,                           "UN" },
    { G_SYS_DBG_CATEGORY_BLEMANAGER,                        "BM" },
    { G_SYS_DBG_CATEGORY_JOBMANAGER,                        "JM" },
    { G_SYS_DBG_CATEGORY_LEDMANAGER,                        "LM" },
    { G_SYS_DBG_CATEGORY_SERVERMANAGER,                     "SM" },
    { G_SYS_DBG_CATEGORY_MQTTMANAGER,                       "MQ" },
    { G_SYS_DBG_CATEGORY_HTTPMANAGER,                       "HM" },
    { G_SYS_DBG_CATEGORY_SFLASHMANAGER,                     "FM" },
    { G_SYS_DBG_CATEGORY_UDPCOMMMANAGER,                    "UM" },
    { G_SYS_DBG_CATEGORY_WIFIMANAGER,                       "WM" },
    { G_SYS_DBG_CATEGORY_BLEMANAGER_EVENTREPORTHANDLER,     "BE" },
    { G_SYS_DBG_CATEGORY_JOBMANAGER_EVENTREPORTHANDLER,     "JE" },
    { G_SYS_DBG_CATEGORY_WIFIMANAGER_EVENTREPORTHANDLER,    "WE" },
    { G_SYS_DBG_CATEGORY_UDPCOMMMANAGER_EVENTREPORTHANDLER, "UE" },
    { G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER,  "SE" },
    { G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER,    "HE" },
    { G_SYS_DBG_CATEGORY_MQTTMANAGER_EVENTREPORTHANDLER,    "ME" },
    { G_SYS_DBG_CATEGORY_STATE_MANAGER,                     "ST" },
    { G_SYS_DBG_CATEGORY_FW_UPDATE,                         "FW" },
    { G_SYS_DBG_CATEGORY_OFFLINE,                           "OF" },
    { G_SYS_DBG_CATEGORY_ONLINE,                            "OL" },
    { G_SYS_DBG_CATEGORY_SETUP,                             "SU" },
    { G_SYS_DBG_CATEGORY_IDLE,                              "IL" },
    { G_SYS_DBG_CATEGORY_REGISTER,                          "RG" },
    { G_SYS_DBG_CATEGORY_FATAL,                             "FT" },
    { G_SYS_DBG_CATEGORY_MAIN_APP,                          "MN" },
};

static const SysDbgIdNameTable LevelName[] =
{
    { G_SYS_DBG_LEVEL_FATAL_ERROR,                          "FE" },
    { G_SYS_DBG_LEVEL_GENERAL_INFORMATION,                  "GI" },
    { G_SYS_DBG_LEVEL_DETAIL_INFORMATION,                   "DI" }
};

char* GetSysDbgCategoryName (GSysDebugger_category_t category)
{
    char *ret = NULL;
    int i = 0;
    for (i = 0; i < sizeof(CategoryName) / sizeof(CategoryName[0]); i++)
    {
        if (CategoryName[i].id == (uint8_t)category)
        {
            ret = CategoryName[i].pszName;
            break;
        }
    }
    return ret;
}

char* GetSysDbgLevelName (uint8_t level)
{
    char *ret = NULL;
    int i = 0;
    for (i = 0; i < sizeof(LevelName) / sizeof(LevelName[0]); i++)
    {
        if (LevelName[i].id == (uint8_t)level)
        {
            ret = LevelName[i].pszName;
            break;
        }
    }
    return ret;
}

#endif

/******************************************************
 *               Local Function Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

/**

%desc

    Obtain available queue slot from arrays

%param

      0 ~ ( (MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED+2) - 1 )

%return

      Available: dxTRUE
    Unavailable: dxFALSE
*/
static dxBOOL GSysDebugger_GetAvailablePoolQueueSlot(uint8_t* SlotNum)
{
    uint8_t i = 0;

    for ( i = 0 ; i < (MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED+2); i++ )
    {
        if ( SysDbgQueuePool[i].SlotAvailable == dxTRUE )
        {
            *SlotNum = i;
            return dxTRUE;
        }
    }

    return dxFALSE;
}


#if (ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC || ENABLE_SYSTEM_MEMORY_DIASGNOSTIC)

void SystemJobMessageGenericSend(SysDbgJobQueue_t* SystemDbgJobMsg)
{
    if (SystemDbgJobMsg)
    {
#if DK_OS_WRAPPER
        if (SysDebuggerJobQueue != NULL)
#else // DK_OS_WRAPPER
        if (SysDebuggerJobQueue.buffer)
#endif // DK_OS_WRAPPER
        {
            if (dxQueueIsFull(SysDebuggerJobQueue) == dxTRUE)
            {
                //Queue is FULL, NO PRINT ALLOWED IN THIS FUNCTION.. We simply skip
                SystemDbgJobMsg->JobData[0] = NULL_STRING;      // useless data

                SysDbgQueuePool[SystemDbgJobMsg->SlotNum].SlotAvailable = dxTRUE;
            }

            else if (dxQueueSend( SysDebuggerJobQueue, &SystemDbgJobMsg, 1000 ) != DXOS_SUCCESS)
            {
                //Error, NO PRINT ALLOWED IN THIS FUNCTION.. We simply skip
                SystemDbgJobMsg->JobData[0] = NULL_STRING;      // useless data

                SysDbgQueuePool[SystemDbgJobMsg->SlotNum].SlotAvailable = dxTRUE;
            }

        }
    }
}

#endif

void SystemDiagnosticTimeTickHandler(void* arg)
{
    static dxTime_t NextThreadStackDiagnosedTime = 0;
    static dxTime_t NextMemDiagnosedTime = 0;
    static dxTime_t NextCpuDiagnosedTime = 0;

    dxTime_t CurrentSystemTime = dxTimeGetMS();

    //First time initialisation
    if (NextThreadStackDiagnosedTime == 0) {
        NextThreadStackDiagnosedTime = CurrentSystemTime + THREAD_STACK_DIAGNOSTIC_INTERVAL;
    }
    if (NextMemDiagnosedTime == 0) {
        NextMemDiagnosedTime = CurrentSystemTime + MEMORY_DIAGNOSTIC_INTERVAL;
    }
    if (NextCpuDiagnosedTime == 0) {
        NextCpuDiagnosedTime = CurrentSystemTime + CPU_DIAGNOSTIC_INTERVAL;
    }

#if ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC

    if (time_after (CurrentSystemTime, NextThreadStackDiagnosedTime)) {
        uint8_t i;
        dxBOOL SlotAvailable;

        SlotAvailable = GSysDebugger_GetAvailablePoolQueueSlot( &i );

        if (SlotAvailable == dxTRUE)
        {
            SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_THREAD_STACK_CHECK;
            SysDbgQueuePool[i].PoolQueue.JobCreatedSysTime = SYS_DBG_THREAD_STACK_CHECK;
            SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;      // useless data
            SysDbgQueuePool[i].PoolQueue.SlotNum = i;
            SysDbgQueuePool[i].PoolQueue.Type = 0;
            SysDbgQueuePool[i].SlotAvailable = dxFALSE;

            SystemJobMessageGenericSend(&SysDbgQueuePool[i].PoolQueue);
        }

        NextThreadStackDiagnosedTime = CurrentSystemTime + THREAD_STACK_DIAGNOSTIC_INTERVAL;
    }

#endif


#if ENABLE_SYSTEM_MEMORY_DIASGNOSTIC

    if (time_after (CurrentSystemTime, NextMemDiagnosedTime)) {
        uint8_t i;
        dxBOOL SlotAvailable;

        SlotAvailable = GSysDebugger_GetAvailablePoolQueueSlot( &i );

        if (SlotAvailable == dxTRUE)
        {
            SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_MEM_HEAP_CHECK;
            SysDbgQueuePool[i].PoolQueue.JobCreatedSysTime = CurrentSystemTime;
            SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;      // useless data
            SysDbgQueuePool[i].PoolQueue.SlotNum = i;
            SysDbgQueuePool[i].PoolQueue.Type = 0;
            SysDbgQueuePool[i].SlotAvailable = dxFALSE;

            SystemJobMessageGenericSend(&SysDbgQueuePool[i].PoolQueue);
        }

        NextMemDiagnosedTime = CurrentSystemTime + MEMORY_DIAGNOSTIC_INTERVAL;
    }

#endif

#if ENABLE_SYSTEM_CPU_DIASGNOSTIC && configGENERATE_RUN_TIME_STATS

    if (time_after (CurrentSystemTime, NextCpuDiagnosedTime)) {
        uint8_t i;
        dxBOOL SlotAvailable;

        SlotAvailable = GSysDebugger_GetAvailablePoolQueueSlot( &i );

        if (SlotAvailable == dxTRUE)
        {
            SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_CPU_USAGE_CHECK;
            SysDbgQueuePool[i].PoolQueue.JobCreatedSysTime = CurrentSystemTime;
            SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;      // useless data
            SysDbgQueuePool[i].PoolQueue.SlotNum = i;
            SysDbgQueuePool[i].PoolQueue.Type = 0;
            SysDbgQueuePool[i].SlotAvailable = dxFALSE;

            SystemJobMessageGenericSend(&SysDbgQueuePool[i].PoolQueue);
        }

        NextCpuDiagnosedTime = CurrentSystemTime + CPU_DIAGNOSTIC_INTERVAL;
    }

#endif

    if (dxTimerIsTimerActive(SystemDiagnosticTimer) == dxTRUE)
    {
        dxTimerStop(SystemDiagnosticTimer, DXOS_WAIT_FOREVER);
    }

    if (dxTimerStart(SystemDiagnosticTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
    {
        WPRINT_APP_INFO( ( "WARNING - Unable to re-start SystemDiagnosticTimer\r\n") );
    }
}

static void Sys_Debugger_Process_thread_main( uint32_t arg )
{
    SystemDiagnosticTimer = dxTimerCreate("",
                                          SYSTEM_DIAGNOSTIC_TIMER_TICK,
                                          dxTRUE,
                                          &SystemDiagnosticTimer,
                                          SystemDiagnosticTimeTickHandler);
    if (SystemDiagnosticTimer != NULL)
    {
        if (dxTimerStart(SystemDiagnosticTimer, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
            WPRINT_APP_INFO( ( "WARNING - Unable to re-start SystemDiagnosticTimer\r\n") );
    }

    else
    {
        WPRINT_APP_INFO( ( "WARNING - System Debugger unable to create RTOS timer\r\n") );
    }

    while (1)
    {

        SysDbgJobQueue_t *pGetJobBuff = NULL;

        if (dxQueueReceive( SysDebuggerJobQueue, &pGetJobBuff, DXOS_WAIT_FOREVER ) == DXOS_SUCCESS)
        {
            if (pGetJobBuff)
            {
                int category = pGetJobBuff->Type >> (sizeof(pGetJobBuff->Type) * 8 - G_SYS_DBG_CATEGORY_BITS);
                int level = pGetJobBuff->Type & G_SYS_DBG_LEVEL_ALL;
                switch(pGetJobBuff->JobType)
                {
                    case SYS_DBG_LOG_OUTPUT:
                    {
                        if(pGetJobBuff->JobData[0] != NULL_STRING)
                        {
                            GSysDebugger_MulticastSend(pGetJobBuff->JobData, strlen(pGetJobBuff->JobData));

                            if (pGetJobBuff->DebuggIncludeHeader == dxTRUE)
                            {
                                WPRINT_APP_INFO( ( "[SYSDBG][T%ld][%s][%s]%s", pGetJobBuff->JobCreatedSysTime,
                                                                               GetSysDbgCategoryName(category),
                                                                               GetSysDbgLevelName(level),
                                                                               pGetJobBuff->JobData) );
                            }
                            else
                            {
                                WPRINT_APP_INFO( ( "%s", pGetJobBuff->JobData) );
                            }
                        }
                    }
                        break;

                    case SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Queue Full!\r\n", (unsigned int)pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    case SYS_DBG_THREAD_STACK_CHECK:
                    {
                        WPRINT_APP_INFO( ( "\r\n==================System Dbg Thread Stack Diagnostic===================\r\n") );

                        uint16_t i = 0;
                        for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
                        {
                            if (SysDbgThreadCheckList[i].pSysDbgThreadObj != NULL)
                            {
                                rtos_check_stack( SysDbgThreadCheckList[i].pSysDbgThreadObj, SysDbgThreadCheckList[i].pThreadName);
                            }
                        }

                        WPRINT_APP_INFO( ( "===============================END=====================================\r\n") );

                    }
                        break;

                    case SYS_DBG_MEM_HEAP_CHECK:
                    {
                        _printSystemMemory ();
                    }
                        break;

                    case SYS_DBG_CPU_USAGE_CHECK : {
                        _printCpuUsage ();
                    }
                    break;

                    case SYS_DBG_LOG_ARRAY_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Arrays Full!\r\n", pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    case SYS_DBG_LOG_ARRAY_QUEUE_FULL_WARNING:
                    {
                        WPRINT_APP_INFO( ( "[SYSDBG][T%ld][WARNING] Log Message Arrays Full!\r\n", pGetJobBuff->JobCreatedSysTime) );
                    }
                        break;

                    default:
                        break;
                }

                pGetJobBuff->JobData[0] = NULL_STRING;

                SysDbgQueuePool[pGetJobBuff->SlotNum].SlotAvailable = dxTRUE;

            }
        }
#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
        {
            static dxTime_t lastTick = 0;
            dxTime_t CurrTick = 0;

            CurrTick = dxTimeGetMS();
            if((CurrTick - lastTick) > (1 * SECONDS))
            {
                GSysSemDbg_Dump();
                lastTick = CurrTick;
            }
        }
#endif  //#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
    }

}

#ifdef ENABLE_DEBUG_MESSAGE_QUEUE
typedef enum
{
    GATEWAY_OPERATION_MODE_INIT                     = 0,
    GATEWAY_OPERATION_MODE_SWITCHING_TO_NORMAL,
    GATEWAY_OPERATION_MODE_NORMAL,
    GATEWAY_OPERATION_MODE_SWITCHING_TO_SETUP,
    GATEWAY_OPERATION_MODE_SETUP,

} Gateway_Operation_Mode_t;
typedef enum
{
    GATEWAY_CONDITION_NONE                          = 0,
    GATEWAY_CONDITION_WIFI_CONNECTING,
    GATEWAY_CONDITION_WIFI_CONNECTION_ERROR,
    GATEWAY_CONDITION_CONNECTING_WITH_DK_SERVER,
    GATEWAY_CONDITION_DK_SERVER_OPERATION_FAILED,
    GATEWAY_CONDITION_WWW_NOT_REACHABLE,
    GATEWAY_CONDITION_BUSY_PAIRING_WITH_PERIPHERAL,
    GATEWAY_CONDITION_BUSY_ACCESSING_PERIPHERAL,
} Gateway_Condition_t;

extern Gateway_Operation_Mode_t     gGatewayOpMode;
extern Gateway_Condition_t          gGatewayCondition;

static void Test_thread_main( uint32_t arg )
{
    static uint32_t count = 0;

    while (1)
    {
        dxTimeDelayMS(5);

        //if ( gGatewayOpMode == 2 && gGatewayCondition == 0 )
        {
            count++;

            G_SYS_DBG_LOG_X1( "Test_thread_main = [%12ld] \r\n", count );

            if ( count >= 0xffffffff )
            {
                G_SYS_DBG_LOG_X1( "Exit .......... = [%12ld] \r\n", count );
                break;
            }
        }
    }
}
#endif

static void _printSystemMemory (void) {

#ifdef DK_MEMORY_DEBUG
    MemoryRecord* record;
    ListItem_t const* endItem;
    ListItem_t* item;
    List_t* list;
    dxTaskStack_t stackBuf;
    dxTime_t time;
    dxTime_t diff;
    uint32_t alignSize;
    uint16_t size;
    uint16_t count;
    uint16_t i;
    uint8_t* memory;
    uint8_t needMore;
    char mark;
#endif

    WPRINT_APP_INFO( ("\r\n====================SYS DBG MEM HEAP DIAGNOSTIC========================\r\n") );

#ifdef DK_MEMORY_DEBUG
    count = 0;
    time = dxTimeGetMS ();
    dxMemGetRecords (&list, &size, &needMore);

    endItem = listGET_END_MARKER (list);
    item = listGET_HEAD_ENTRY (list);
    while (item != endItem) {
        record = (MemoryRecord*) listGET_LIST_ITEM_OWNER (item);
        if (record->used != 0) {

            /* Check if the time is overflow */
            if (record->overflow) {
                // time overflow
                diff = DK_MEMORY_DEBUG_TIMEDIFF_MAX;
            } else {
                if (time_after (time, record->time)) {
                    if (time >= record->time) {
                        diff = time - record->time;
                    } else {
                        diff = 0xFFFFFFFF - record->time + time;
                    }
                } else {
                    diff = 0;
                }

                // time overflow
                if (diff >= DK_MEMORY_DEBUG_TIMEDIFF_MAX) {
                    diff = DK_MEMORY_DEBUG_TIMEDIFF_MAX;
                    record->overflow = 1;
                }
            }

            if (diff > DK_MEMORY_DEBUG_INTERVAL) {

                /* Check if the magic number is corrupted */
                memory = record->memory;
                alignSize = *((uint32_t*) memory);
                if ((record->size != alignSize) || (*((uint32_t*) (memory + 4 + alignSize)) != DK_MEM_MAGIC_NUMBER)) {
                    mark = 'X';
                } else {
                    mark = 'O';
                }

                if (record->current != 0) {
                    memset (&stackBuf, 0, sizeof (stackBuf));
                    dxTaskGetStackInfo (record->current, &stackBuf);
                    WPRINT_APP_INFO ((
                        "[%c] at T%u(%us ago) %s(%u) %s 0x%x(%u bytes) from %s(%s, %d)\r\n",
                        mark,
                        record->time,
                        diff / SECONDS,
                        stackBuf.szTaskName,
                        record->current,
                        (record->used == 1) ? "allocate" : "reallocate",
                        record->memory,
                        record->size,
                        record->function,
                        record->file,
                        record->line
                    ));
                } else {
                    WPRINT_APP_INFO ((
                        "[%c] at T%u(%us ago) task(NA) %s 0x%x(%u bytes) from %s(%s, %d)\r\n",
                        mark,
                        record->time,
                        diff / SECONDS,
                        (record->used == 1) ? "allocate" : "reallocate",
                        record->memory,
                        record->size,
                        record->function,
                        record->file,
                        record->line
                    ));
                }
                count++;
            }
        }
        item = item->pxNext;
    }

    if (count) {
        WPRINT_APP_INFO( ("-----------------------------------------------------------------------\r\n") );
    }
    WPRINT_APP_INFO ((
        "SUMMARY: free heap %u bytes, %d records occupied exceed %d seconds%s.\r\n"
        "NOTE: [X] means data boundary has been altered. [O] otherwise\r\n",
        xPortGetFreeHeapSize (),
        count,
        DK_MEMORY_DEBUG_INTERVAL / SECONDS,
        needMore ? ".\r\nWARNING: increase DK_MEMORY_DEBUG_RECORD_NUM for more memory debug records" : ""
    ));
#else
    WPRINT_APP_INFO((
        "SUMMARY: free heap %u bytes.\r\n",
        xPortGetFreeHeapSize ()
    ));
#endif

    WPRINT_APP_INFO( ("===============================END=====================================\r\n\r\n") );
}


static void _printCpuUsage (void) {

#if ENABLE_SYSTEM_CPU_DIASGNOSTIC && configGENERATE_RUN_TIME_STATS
    WPRINT_APP_INFO( ("\r\n=====================SYS CPU USAGE DIAGNOSTIC==========================\r\n") );
    char* buffer = GSysDebugger_TakeCpuUsageBuffer ();
    WPRINT_APP_INFO( ("%s", buffer) );
    GSysDebugger_GiveCpuUsageBuffer ();
    WPRINT_APP_INFO( ("===============================END=====================================\r\n\r\n") );
#endif
}


dxOS_RET_CODE rtos_check_stack( dxTaskHandle_t* thread, char* GivenName)
{
    dxTaskStack_t StackBuf;

    if (thread == NULL)
    {
        return DXOS_ERROR;
    }

    memset(&StackBuf, 0x00, sizeof(StackBuf));

    if (dxTaskGetStackInfo(*thread, &StackBuf) != DXOS_SUCCESS)
    {
        return DXOS_ERROR;
    }

    if (GivenName)
    {
        WPRINT_APP_INFO( ( "[SYSDBG][STACK][%s] ", GivenName) );
    }
    else
    {
        WPRINT_APP_INFO( ( "[SYSDBG][STACK][%s] ", StackBuf.szTaskName) );
    }

    if (StackBuf.nStackSize == 0)
    {
        WPRINT_APP_INFO( ( "Total Stack Size is NA (Available %ld) bytes\r\n", StackBuf.nFreeStackSpace) );
    }
    else
    {
        WPRINT_APP_INFO( ( "Total Stack Size is %ld (Available %ld) bytes\r\n", StackBuf.nStackSize, StackBuf.nFreeStackSpace) );
    }

    return DXOS_SUCCESS;
}

#endif

/******************************************************
 *               Function Definitions
 ******************************************************/

#if ENABLE_SYSTEM_DEBUGGER

GSysDebugger_result_t GSysDebuggerInit(void)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    // NOTE: Porting to dxOS
    //LogPinDisabled = WICED_FALSE;
    LogPinDisabled = dxFALSE;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;
    // NOTE: Porting to dxOS
    //wiced_result_t WicedResult = WICED_SUCCESS;
    dxOS_RET_CODE dxOSResult = DXOS_SUCCESS;
    uint8_t i = 0;

    SysDbgQueuePool = (SysDbgJobPool_t*) dxMemAlloc (DK_MEM_ZONE_SLOW, MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED + 2, sizeof (SysDbgJobPool_t));
    cpuUsageBuffer = (char*) dxMemAlloc (DK_MEM_ZONE_SLOW, 4096, sizeof (char));

    SysDbgThreadCheckList = (SysDbgThreadDiagnose_t*) dxMemAlloc ("SysDbgThreadCheckList", MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC, sizeof (SysDbgThreadDiagnose_t));
    LogStrBuffer = dxMemAlloc ("LogStrBuffer", 1, LOG_MAX_CHAR_BUFFER);

    LogBufferSemaphore = dxSemCreate(0x7fffffff, 0);
    if (LogBufferSemaphore == NULL)
    {
        result = G_SYS_DBG_DX_RTOS_ERROR;
        goto EndInit;
    }
    else
    {
        //Initial semaphore count is 0 (see wiced_rtos_init_semaphore). So we initialise it to 1 by set/put a semaphore to get us started
        dxSemGive(LogBufferSemaphore);
    }

    cpuUsageBufferSemaphore = dxSemCreate (1, 1);
    if (cpuUsageBufferSemaphore == NULL) {
        result = G_SYS_DBG_DX_RTOS_ERROR;
        goto EndInit;
    }

#if DK_OS_WRAPPER
    if (SysDebuggerJobQueue == NULL)
#else // DK_OS_WRAPPER
    if (SysDebuggerJobQueue.buffer == NULL)
#endif // DK_OS_WRAPPER
    {
        SysDebuggerJobQueue = dxQueueCreate(MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED,
                                            sizeof(SysDbgJobQueue_t*));

        if (SysDebuggerJobQueue == NULL)
        {
            dxSemDelete(LogBufferSemaphore);

            result = G_SYS_DBG_DX_RTOS_ERROR;

            goto EndInit;
        }
    }

    dxOSResult = dxTaskCreate(Sys_Debugger_Process_thread_main,
                              SYS_DEBUGGER_PROCESS_THREAD_NAME,
                              SYS_DEBUGGER_STACK_SIZE,
                              NULL,
                              SYS_DEBUGGER_PROCESS_THREAD_PRIORITY,
                              &SysDebuggerMainProcess_thread);

    if (dxOSResult != DXOS_SUCCESS)
    {
        dxSemDelete(LogBufferSemaphore);
        dxQueueDelete(SysDebuggerJobQueue);

        result = G_SYS_DBG_DX_RTOS_ERROR;

        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&SysDebuggerMainProcess_thread);

#ifdef ENABLE_DEBUG_MESSAGE_QUEUE
    dxOSResult = dxTaskCreate(Test_thread_main,
                              "Test Process",
                              SYS_DEBUGGER_STACK_SIZE,
                              NULL,
                              0,
                              &Test_thread);

    if (dxOSResult != DXOS_SUCCESS)
    {
        dxSemDelete(LogBufferSemaphore);
        dxQueueDelete(SysDebuggerJobQueue);

        result = G_SYS_DBG_DX_RTOS_ERROR;

        goto EndInit;
    }
#endif

    for (i = 0; i < (MAXIMUM_SYS_DEBUGGER_JOB_QUEUE_DEPT_ALLOWED+2) ; i++)
    {
        SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;
        SysDbgQueuePool[i].SlotAvailable = dxTRUE;
    }

    // Open all category by default
    memset (SysDebuggerCategory, 1, G_SYS_DBG_CATEGORY_MAX_VALUE);

EndInit:
    return result;


}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    return GSysDebugger_RegisterForThreadDiagnosticGivenName(thread, NULL);

}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName)
{
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    uint16_t i = 0;
    for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
    {
        if (SysDbgThreadCheckList[i].pSysDbgThreadObj == NULL)
        {
            SysDbgThreadCheckList[i].pSysDbgThreadObj = thread;

            if (ThreadName)
            {
                int16_t LogStrLen = strlen(ThreadName);
                if (LogStrLen > 0)
                {
                    //Limit the Thread name to 31
                    if (LogStrLen > 31)
                        LogStrLen = 31;

                    char* pGivenThreadNameBuff = dxMemAlloc ("SysDbg Thread Name Buff", 1, LogStrLen + 1);
                    memcpy(pGivenThreadNameBuff, ThreadName, LogStrLen);
                    pGivenThreadNameBuff[LogStrLen] = NULL_STRING;

                    SysDbgThreadCheckList[i].pThreadName = pGivenThreadNameBuff;

                }
            }

            return result;
        }
    }

    return G_SYS_DBG_MAX_THREAD_REGISTRATION_REACHED;

}

GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    uint16_t i = 0;
    for (i = 0; i < MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC; i++)
    {
        if (SysDbgThreadCheckList[i].pSysDbgThreadObj == thread)
        {
            SysDbgThreadCheckList[i].pSysDbgThreadObj = NULL;

            if (SysDbgThreadCheckList[i].pThreadName)
            {
                dxMemFree(SysDbgThreadCheckList[i].pThreadName);
                SysDbgThreadCheckList[i].pThreadName = NULL;
            }

            return result;
        }
    }

    return G_SYS_DBG_MAX_THREAD_REGISTRATION_REACHED;

}


GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    if (LogPinDisabled == dxTRUE) {
        return G_SYS_DBG_DISABLED;
    }

    if ((SysDebuggerLogLevel & level) == 0) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    if ((category >= G_SYS_DBG_CATEGORY_MAX_VALUE) || !SysDebuggerCategory[category]) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    GSysDebugger_RetainBuffPermissionAccess();

    va_list ap;
    char *buf = GSysDebugger_GetBuffForLog();
    /* See http://www.ijs.si/software/snprintf/ for portable
     * implementation of snprintf.
     */
    va_start(ap, fmt);
#if DK_OS_WRAPPER
    _vsnprintf(buf, LOG_MAX_CHAR_BUFFER, fmt, ap);
#else // DK_OS_WRAPPER
    vsnprintf(buf, LOG_MAX_CHAR_BUFFER, fmt, ap);
#endif // DK_OS_WRAPPER
    va_end(ap);
    if (LOG_MAX_CHAR_BUFFER > 0)
    {
        buf[LOG_MAX_CHAR_BUFFER - 1] = '\0';
    }

    GSysDebugger_ReleaseBuffPermissionAccess();
    _sendLogEvent(buf, dxTRUE, category,level);

    return G_SYS_DBG_SUCCESS;
}


GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    if (LogPinDisabled == dxTRUE) {
        return G_SYS_DBG_DISABLED;
    }

    if ((SysDebuggerLogLevel & level) == 0) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    if ((category >= G_SYS_DBG_CATEGORY_MAX_VALUE) || !SysDebuggerCategory[category]) {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    GSysDebugger_RetainBuffPermissionAccess();

    va_list ap;
    char *buf = GSysDebugger_GetBuffForLog();
    /* See http://www.ijs.si/software/snprintf/ for portable
     * implementation of snprintf.
     */
    va_start(ap, fmt);
    _vsnprintf(buf, LOG_MAX_CHAR_BUFFER, fmt, ap);
    va_end(ap);
    if (LOG_MAX_CHAR_BUFFER > 0)
    {
        buf[LOG_MAX_CHAR_BUFFER - 1] = '\0';
    }

    GSysDebugger_ReleaseBuffPermissionAccess();
    _sendLogEvent(buf, dxFALSE, category,level);

    return G_SYS_DBG_SUCCESS;
}

GSysDebugger_result_t _sendLogEvent(char* LogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level)
{
    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    int16_t LogStrLen = strlen(LogStrBuffer);

    if (LogStrLen > 0)
    {
        uint8_t i = 0;
        dxBOOL SlotAvailable;

        if (LogStrLen > LOG_MAX_CHAR_BUFFER)
            LogStrLen = LOG_MAX_CHAR_BUFFER;

        dxTime_t CurrentSystemTime = dxTimeGetMS();

        SlotAvailable = GSysDebugger_GetAvailablePoolQueueSlot( &i );

        SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_LOG_OUTPUT;
        SysDbgQueuePool[i].PoolQueue.JobCreatedSysTime = CurrentSystemTime;
        SysDbgQueuePool[i].PoolQueue.DebuggIncludeHeader = IncludeHeader;
        SysDbgQueuePool[i].PoolQueue.SlotNum = i;
        SysDbgQueuePool[i].PoolQueue.Type = (category << (sizeof(SysDbgQueuePool[i].PoolQueue.Type) * 8 - G_SYS_DBG_CATEGORY_BITS)) |
                                            (level & G_SYS_DBG_LEVEL_ALL);

        if ( SlotAvailable == dxTRUE )
        {
            memcpy( SysDbgQueuePool[i].PoolQueue.JobData, LogStrBuffer, LogStrLen);
            SysDbgQueuePool[i].PoolQueue.JobData[LogStrLen] = NULL_STRING;
        }
        else
        {
            SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_LOG_ARRAY_FULL_WARNING;
            SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;
        }

#if DK_OS_WRAPPER
        if (SysDebuggerJobQueue != NULL)
#else // DK_OS_WRAPPER
        if (SysDebuggerJobQueue.buffer)
#endif // DK_OS_WRAPPER
        {
            SysDbgJobQueue_t*   SysDbgJobMsg;

            SysDbgJobMsg = &SysDbgQueuePool[i].PoolQueue;
            SysDbgQueuePool[i].SlotAvailable = dxFALSE;

#if DK_OS_WRAPPER
            if (dxQueueSpacesAvailable(SysDebuggerJobQueue) == 1)
#else // DK_OS_WRAPPER
            if (SysDebuggerJobQueue.handle.tx_queue_available_storage == 1)
#endif // DK_OS_WRAPPER
            {
                if ( SlotAvailable == dxTRUE )
                {
                    SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_LOG_OUTPUT_QUEUE_FULL_WARNING;
                }
                else
                {
                    SysDbgQueuePool[i].PoolQueue.JobType = SYS_DBG_LOG_ARRAY_QUEUE_FULL_WARNING;
                }

                SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;
            }

            if (dxQueueSend( SysDebuggerJobQueue, &SysDbgJobMsg, 1000 ) != DXOS_SUCCESS)
            {
                SysDbgQueuePool[i].PoolQueue.JobData[0] = NULL_STRING;
                SysDbgQueuePool[i].SlotAvailable = dxTRUE;
                result = G_SYS_DBG_DX_RTOS_ERROR;
            }
        }
    }

    return result;
}


char* GSysDebugger_GetBuffForLog()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */

    return LogStrBuffer;
}


char* GSysDebugger_TakeCpuUsageBuffer () {
    dxSemTake (cpuUsageBufferSemaphore, DXOS_WAIT_FOREVER);
	vTaskGetRunTimeStats ((char*) cpuUsageBuffer);

    return cpuUsageBuffer;
}

void GSysDebugger_GiveCpuUsageBuffer () {
    dxSemGive (cpuUsageBufferSemaphore);
}


GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    dxSemTake(LogBufferSemaphore, DXOS_WAIT_FOREVER);

    return result;

}

GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess()
{
    /*
     * WARNING: NO PRINT IN THIS FUNCTION
     */
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    dxSemGive(LogBufferSemaphore);

    return result;

}

GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level)
{
    if (LogPinDisabled == dxTRUE)
        return G_SYS_DBG_DISABLED;

    GSysDebugger_result_t result = G_SYS_DBG_SUCCESS;

    if ((level & G_SYS_DBG_LEVEL_ALL) == 0)
    {
        return G_SYS_DBG_INVALID_PARAMETER;
    }

    SysDebuggerLogLevel = level;

    return result;
}

uint8_t GSysDebugger_ReadLogLevel(void)
{
    return SysDebuggerLogLevel;
}

#if MULTICAST_UDP_DEBUG_OUTPUT
#pragma message ( "REMINDER: Multicast UDP debug NEED to disable while production!" )
// TODO: NOT well TEST.
static int sfd = -1;
static const char *group_ip = "239.239.239.239";
static const uint16_t port = 23456;
#endif // MULTICAST_UDP_DEBUG_OUTPUT

int GSysDebugger_MulticastInit(void)
{
    int ret = 0;

#if MULTICAST_UDP_DEBUG_OUTPUT
    // To TEST, run another UDP multicast client before testing
    //
    // 32. Create socket
    sfd = dxMCAST_Create();

    printf("\r\ndxMCAST_Create(): %d\r\n", sfd);

    // 33. Join to multicast group
    ret = dxMCAST_Join(sfd, group_ip, port);

    printf("\r\ndxMCAST_AddMembership(): %d\r\n", ret);
#endif // MULTICAST_UDP_DEBUG_OUTPUT

    return ret;
}

int GSysDebugger_MulticastSend(uint8_t *pData, int len)
{
    int ret = 0;
#if MULTICAST_UDP_DEBUG_OUTPUT
    struct sockaddr_in dest;

    if (sfd < 0) return -1;

    // 34. Send to group
    memset(&dest, 0x00, sizeof(dest));
    dest.sin_family = AF_INET;
    dest.sin_port = htons(port);
    dest.sin_addr.s_addr = inet_addr(group_ip);

    ret = dxMCAST_SendTo(sfd, pData, len, 0, &dest, sizeof(dest));
#endif // MULTICAST_UDP_DEBUG_OUTPUT
    return ret;
}


uint8_t* GSysDebugger_GetDebuggerLevel (void)
{
    return &SysDebuggerLogLevel;
}


uint8_t* GSysDebugger_GetDebuggerCategory (void)
{
    return SysDebuggerCategory;
}


GSysDebugger_result_t GSysDebugger_Dump (
    const char* title,
    int len,
    uint8_t* data
) {
    GSysDebugger_result_t ret = G_SYS_DBG_SUCCESS;
    int segment;
    int remain;
    int i;
    int j;
    char* buf;

    if (data == NULL) {
        ret = G_SYS_DBG_DX_GENERIC_ERROR;
        goto fail;
    }

    if (LogPinDisabled == dxTRUE) {
        ret = G_SYS_DBG_DX_GENERIC_ERROR;
        goto fail;
    }

    GSysDebugger_RetainBuffPermissionAccess();

    dxTimeDelayMS (100);                 // Waiting for log queue be clear

    buf = GSysDebugger_GetBuffForLog ();
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "\r\n\r\n++++++++++++++++++++++++++++++++ begin ++++++++++++++++++++++++++++++++\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    buf = GSysDebugger_GetBuffForLog ();
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "%s packet = {\r\n", title);
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    segment = len / SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE;
    remain = len % SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE;

    if (!remain) {
      segment--;
      remain = SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE;
    }

    for (i = 0; i < segment; i++) {
        memset (buf, 0x00, LOG_MAX_CHAR_BUFFER);
        snprintf (buf, LOG_MAX_CHAR_BUFFER, "    ");
        for (j = 0; j < SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE; j++) {
            snprintf (&buf[strlen (buf)], LOG_MAX_CHAR_BUFFER, "%02X:", data[i * SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE + j] & 0xff);
        }
        snprintf (&buf[strlen (buf)], LOG_MAX_CHAR_BUFFER, "\r\n");
        _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);
    }

    // last segment
    memset (buf, 0x00, LOG_MAX_CHAR_BUFFER);
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "    ");
    for (j = 0; j < remain - 1; j++) {
        snprintf (&buf[strlen (buf)], LOG_MAX_CHAR_BUFFER, "%02X:", data[i * SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE + j] & 0xff);
    }

    // last byte
    snprintf (&buf[strlen (buf)], LOG_MAX_CHAR_BUFFER, "%02X", data[i * SYS_DEBUGGER_MAX_DUMP_BATCH_SIZE + j] & 0xff);
    snprintf (&buf[strlen (buf)], LOG_MAX_CHAR_BUFFER, "\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    memset (buf, 0x00, LOG_MAX_CHAR_BUFFER);
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "}\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    buf = GSysDebugger_GetBuffForLog ();
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "\r\n+++++++++++++++++++++++++++++++++ end +++++++++++++++++++++++++++++++++\r\n\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    GSysDebugger_ReleaseBuffPermissionAccess ();

fail :

    return ret;
}


GSysDebugger_result_t GSysDebugger_DumpString (
    int len,
    uint8_t* buffer
) {
    GSysDebugger_result_t ret = G_SYS_DBG_SUCCESS;
    int segment;
    int remain;
    int i;
    char* buf;

    if (len <= 0) goto fail;

    GSysDebugger_RetainBuffPermissionAccess();

    dxTimeDelayMS (100);                 // Waiting for log queue be clear

    buf = GSysDebugger_GetBuffForLog ();
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "\r\n\r\n++++++++++++++++++++++++++++++++ begin ++++++++++++++++++++++++++++++++\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    segment = len / SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE;
    remain = len % SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE;

    for (i = 0; i < segment; i++) {
        buf = GSysDebugger_GetBuffForLog ();
        memset (buf, 0x00, LOG_MAX_CHAR_BUFFER);
        memcpy(buf, &buffer[SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE * i], SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE);
        _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);
    }

    // last segment
    buf = GSysDebugger_GetBuffForLog ();
    memset (buf, 0x00, LOG_MAX_CHAR_BUFFER);
    memcpy(buf, &buffer[SYS_DEBUGGER_MAX_DUMPSTRING_BATCH_SIZE * i], remain);
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    buf = GSysDebugger_GetBuffForLog ();
    snprintf (buf, LOG_MAX_CHAR_BUFFER, "\r\n+++++++++++++++++++++++++++++++++ end +++++++++++++++++++++++++++++++++\r\n\r\n");
    _sendLogEvent (buf, dxFALSE, DEFAULT_LOG_CATEGORY, G_SYS_DBG_LEVEL_FATAL_ERROR);

    GSysDebugger_ReleaseBuffPermissionAccess ();

fail :

    return ret;
}

#else

GSysDebugger_result_t GSysDebuggerInit(void)
{
    return G_SYS_DBG_DISABLED;
}

extern GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t _sendLogEvent(char* LogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level)
{
    return G_SYS_DBG_DISABLED;
}

char* GSysDebugger_GetBuffForLog()
{
    return NULL;

}


char* GSysDebugger_GetBuffCpuUsage () {
    return NULL;
}

GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess()
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess()
{
    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level)
{
    return G_SYS_DBG_DISABLED;
}

uint8_t GSysDebugger_ReadLogLevel(void)
{
    return 0;
}

int GSysDebugger_MulticastInit(void)
{
    return G_SYS_DBG_DISABLED;
}

int GSysDebugger_MulticastSend(uint8_t *pData, int len)
{
    return G_SYS_DBG_DISABLED;
}


uint8_t* GSysDebugger_GetDebuggerLevel (void)
{
    return NULL;
}


uint8_t* GSysDebugger_GetDebuggerCategory (void)
{
    return NULL;
}


GSysDebugger_result_t GSysDebugger_Dump (
    const char* title,
    int len,
    uint8_t* data
) {
    return G_SYS_DBG_DISABLED;
}


GSysDebugger_result_t GSysDebugger_DumpString (
    int len,
    uint8_t* buffer
) {
    return G_SYS_DBG_DISABLED;
}

#endif


#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
#define G_SYS_DBG_SEM_TIMEOUT   (10 * SECONDS)


#define cpstr(a,b,e,l) \
{\
    if(a != NULL)\
    {\
        dxMemFree(a);\
        a = NULL;\
    }\
    uint16_t len;\
    uint8_t *tmp;\
    len = strlen(b) + 16;\
    tmp = (uint8_t *) dxMemAlloc("GSysSemDbg_BufT", 1, len);\
    memset(tmp, 0, len);\
    if(e != -1)\
    {\
        sprintf(tmp, "%s-%d(L:%d)", b, e, l);\
    }\
    else\
    {\
        sprintf(tmp, "%s(L:%d)", b, l);\
    }\
    len = strlen(tmp) + 1;\
    a = (uint8_t *) dxMemAlloc("GSysSemDbg_Buf", 1, len);\
    memset(a, 0, len);\
    strcpy(a, tmp);\
    dxMemFree(tmp);\
}

#define frstr(a) \
{\
    if(a != NULL)\
    {\
        dxMemFree(a);\
        a = NULL;\
    }\
}

dxSemaphoreHandle_t SysSemDbgProtect = NULL;
aGSys_SemDbg_List_t *GSys_SemDbg_List = NULL;

aGSys_SemDbg_List_t *GSysSemDbg_FindList(dxSemaphoreHandle_t SempHandle)
{
    aGSys_SemDbg_List_t *pSem;

    pSem = GSys_SemDbg_List;
    while(pSem)
    {
        if(pSem->SempHandle == SempHandle)
        {
            return pSem;
        }
        pSem = pSem->pNext;
    }
    return NULL;
}

extern void GSysSemDbg_Register(uint8_t *strCallFrom, uint32_t Line, dxSemaphoreHandle_t SempHandle, int32_t Event)
{
    if(SysSemDbgProtect == NULL)
    {
        SysSemDbgProtect = dxSemCreate( 1,          // uint32_t uxMaxCount
                                        1);         // uint32_t uxInitialCount
    }

//    G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x IN\r\n", __FUNCTION__, strCallFrom, Line, SempHandle);
    if(SempHandle == NULL)
    {
        G_SYS_DBG_LOG_V("%s %s-%d(%d) 0x%08x !! ERR(NULL)\r\n", __FUNCTION__, strCallFrom, Event, Line, SempHandle);
        return;
    }

    dxSemTake(SysSemDbgProtect, DXOS_WAIT_FOREVER);
    aGSys_SemDbg_List_t *pSem;
    pSem = GSysSemDbg_FindList(SempHandle);
    if (pSem == NULL)
    {
        pSem = dxMemAlloc("GSysSemDbg_Buf", 1, sizeof(aGSys_SemDbg_List_t));
        pSem->pNext = GSys_SemDbg_List;
        GSys_SemDbg_List = pSem;
    }
    pSem->pInitFunc = NULL;
    pSem->pTakeFunc = NULL;
    cpstr(pSem->pInitFunc, strCallFrom, Event, Line);
    pSem->SempHandle = SempHandle;
    pSem->TakeTick = 0;
    dxSemGive(SysSemDbgProtect);
//    G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x OUT\r\n", __FUNCTION__, strCallFrom, Line, SempHandle);
}

extern void GSysSemDbg_Take(uint8_t *strCallFrom, uint32_t Line, dxSemaphoreHandle_t SempHandle)
{
//    G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x IN\r\n", __FUNCTION__, strCallFrom, Line, SempHandle);
    int16_t SemDbg_Ind = 0;

    dxSemTake(SysSemDbgProtect, DXOS_WAIT_FOREVER);
    aGSys_SemDbg_List_t *pSem;
    pSem = GSysSemDbg_FindList(SempHandle);
    if(pSem == NULL)
    {
        G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x !! not registered\r\n", __FUNCTION__, strCallFrom, Line, SempHandle);
        dxSemGive(SysSemDbgProtect);
        return;
    }
    cpstr(pSem->pTakeFunc, strCallFrom, -1, Line);
    pSem->TakeTick = dxTimeGetMS();
    dxSemGive(SysSemDbgProtect);
//    G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x OUT\r\n", __FUNCTION__, strCallFrom, Line, SempHandle);
}

extern void GSysSemDbg_Give(dxSemaphoreHandle_t SempHandle)
{
//    G_SYS_DBG_LOG_V("%s 0x%08x IN\r\n", __FUNCTION__, SempHandle);
    int16_t SemDbg_Ind = 0;

    dxSemTake(SysSemDbgProtect, DXOS_WAIT_FOREVER);
    aGSys_SemDbg_List_t *pSem;
    pSem = GSysSemDbg_FindList(SempHandle);
    if(pSem == NULL)
    {
        G_SYS_DBG_LOG_V("%s %s(%d) 0x%08x !! not registered\r\n", __FUNCTION__, SempHandle);
        dxSemGive(SysSemDbgProtect);
        return;
    }
    frstr(pSem->pTakeFunc);
    pSem->TakeTick = 0;
    dxSemGive(SysSemDbgProtect);
//    G_SYS_DBG_LOG_V("%s 0x%08x OUT\r\n", __FUNCTION__, SempHandle);
}

extern void GSysSemDbg_Dump(void)
{
//    WPRINT_APP_INFO(("%s IN\r\n", __FUNCTION__));

    if(GSys_SemDbg_List == NULL)
    {
        return;
    }

    dxTime_t CurrTick = 0;

    dxSemTake(SysSemDbgProtect, DXOS_WAIT_FOREVER);
    CurrTick = dxTimeGetMS();
    aGSys_SemDbg_List_t *pSem;
    pSem = GSys_SemDbg_List;
    while(pSem)
    {
        if(((CurrTick - pSem->TakeTick) > G_SYS_DBG_SEM_TIMEOUT)
           && (pSem->TakeTick > 0)
        )
        {
            WPRINT_APP_INFO(("!! SEM T/O C:%s T:%s From:%d\r\n"
                ,pSem->pInitFunc
                ,pSem->pTakeFunc
                ,pSem->TakeTick
            ));
        }
        pSem = pSem->pNext;
    }
    dxSemGive(SysSemDbgProtect);
//    WPRINT_APP_INFO(("%s OUT\r\n", __FUNCTION__));
}
#endif  //#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
