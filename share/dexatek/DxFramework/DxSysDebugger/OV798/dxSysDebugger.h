//============================================================================
// File: dxSysdebugger.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include <stdio.h>

#include "dxOS.h"

//#define ENABLE_DEBUG_MESSAGE_QUEUE

#define ENABLE_SYSTEM_DEBUGGER                          1
#define     ENABLE_SYSTEM_LOG                           1
#define     ENABLE_SYSTEM_THREAD_STACK_DIAGNOSTIC       1
#define     ENABLE_SYSTEM_MEMORY_DIASGNOSTIC            1
#define     ENABLE_SYSTEM_CPU_DIASGNOSTIC               0   // WARNING: Please make sure configGENERATE_RUN_TIME_STATS set to 1 in FreeRTOSConfig.h
#define     ENABLE_DEBUG_SEMAPHORE_DEADLOCK             0

#define MULTICAST_UDP_DEBUG_OUTPUT                      0

#define MEMORY_DIAGNOSTIC_INTERVAL                  (30*SECONDS)
#define CPU_DIAGNOSTIC_INTERVAL                     (10*SECONDS)
#define THREAD_STACK_DIAGNOSTIC_INTERVAL            (30*SECONDS)

#define MAXIMUM_SYS_DEBUGGER_THREAD_REGISTER_SIZE_FOR_DIAGNOSTIC    30
#define LOG_MAX_CHAR_BUFFER                                         128


/*********************************************************
 *             Color printing definitions
 *
 * If you want to print RED:
 * G_SYS_DBG_LOG_V(RED("- RED Color -\r\n"));
 * G_SYS_DBG_LOG_V1(RED("- RED Color - %d\r\n"), color);
 *********************************************************/
#define ESC_COLOR_NORMAL    "\033[m"
#define ESC_COLOR_BLACK     "\033[30m"
#define ESC_COLOR_RED       "\033[31m"
#define ESC_COLOR_GREEN     "\033[32m"
#define ESC_COLOR_YELLOW    "\033[33m"
#define ESC_COLOR_BLUE      "\033[34m"
#define ESC_COLOR_MAGENTA   "\033[35m"
#define ESC_COLOR_CYAN      "\033[36m"
#define ESC_COLOR_WHITE     "\033[37m"

#define NORMAL( x )         ESC_COLOR_NORMAL x
#define BLACK( x )          ESC_COLOR_BLACK  x ESC_COLOR_NORMAL
#define RED( x )            ESC_COLOR_RED    x ESC_COLOR_NORMAL
#define GREEN( x )          ESC_COLOR_GREEN  x ESC_COLOR_NORMAL
#define BLUE( x )           ESC_COLOR_BLUE   x ESC_COLOR_NORMAL
#define YELLOW( x )         ESC_COLOR_YELLOW   x ESC_COLOR_NORMAL


/******************************************************
 *                      Defines
 ******************************************************/
typedef enum
{

    G_SYS_DBG_SUCCESS                           = 0,    /**< Success */

    G_SYS_DBG_DISABLED                          = -10,
    G_SYS_DBG_MAX_THREAD_REGISTRATION_REACHED   = -11,

    G_SYS_DBG_DX_GENERIC_ERROR                  = -20,
    G_SYS_DBG_DX_RTOS_ERROR                     = -21,
    G_SYS_DBG_TOO_MANY_TASK_IN_QUEUE            = -22,
    G_SYS_DBG_INVALID_PARAMETER                 = -23,
    G_SYS_DBG_DX_API_ERROR                      = -24,

} GSysDebugger_result_t;

typedef enum
{
    G_SYS_DBG_CATEGORY_UNKNOWN                  = 0,
    G_SYS_DBG_CATEGORY_BLEMANAGER,
    G_SYS_DBG_CATEGORY_JOBMANAGER,
    G_SYS_DBG_CATEGORY_LEDMANAGER,
    G_SYS_DBG_CATEGORY_SERVERMANAGER,
    G_SYS_DBG_CATEGORY_MQTTMANAGER,
    G_SYS_DBG_CATEGORY_HTTPMANAGER,
    G_SYS_DBG_CATEGORY_SFLASHMANAGER,
    G_SYS_DBG_CATEGORY_UDPCOMMMANAGER,
    G_SYS_DBG_CATEGORY_WIFIMANAGER,
    G_SYS_DBG_CATEGORY_BLEMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_JOBMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_WIFIMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_UDPCOMMMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_MQTTMANAGER_EVENTREPORTHANDLER,
    G_SYS_DBG_CATEGORY_STATE_MANAGER,
    G_SYS_DBG_CATEGORY_FW_UPDATE,
    G_SYS_DBG_CATEGORY_OFFLINE,
    G_SYS_DBG_CATEGORY_ONLINE,
    G_SYS_DBG_CATEGORY_SETUP,
    G_SYS_DBG_CATEGORY_IDLE,
    G_SYS_DBG_CATEGORY_REGISTER,
    G_SYS_DBG_CATEGORY_FATAL,
    G_SYS_DBG_CATEGORY_MAIN_APP,

    G_SYS_DBG_CATEGORY_MAX_VALUE        // Last Item. Can't exceed 4095
} GSysDebugger_category_t;


/**
 +---+---+---+---+---+---+---+---+
 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |
 +---+---+---+---+---+---+---+---+
                       ^   ^   ^
                       |   |   |
                       |   |   +---- G_SYS_DBG_LEVEL_GENERAL_INFORMATION
                       |   +-------- G_SYS_DBG_LEVEL_FATAL_ERROR
                       +------------ G_SYS_DBG_LEVEL_DETAIL_INFORMATION
*/
#define G_SYS_DBG_LEVEL_FATAL_ERROR                     (0x01 << 0)
#define G_SYS_DBG_LEVEL_GENERAL_INFORMATION             (0x01 << 1)
#define G_SYS_DBG_LEVEL_DETAIL_INFORMATION              (0x01 << 2)
#define G_SYS_DBG_LEVEL_NUMER                           3

#define G_SYS_DBG_LEVEL_ALL                             (G_SYS_DBG_LEVEL_GENERAL_INFORMATION | \
                                                         G_SYS_DBG_LEVEL_FATAL_ERROR |         \
                                                         G_SYS_DBG_LEVEL_DETAIL_INFORMATION)

#define G_SYS_DBG_LEVEL_NORMAL                          (G_SYS_DBG_LEVEL_GENERAL_INFORMATION | \
                                                         G_SYS_DBG_LEVEL_FATAL_ERROR)

#define DEFAULT_LOG_CATEGORY                            G_SYS_DBG_CATEGORY_UNKNOWN
#define DEFAULT_LOG_LEVEL                               G_SYS_DBG_LEVEL_GENERAL_INFORMATION

/******************************************************
 *                      Macros
 ******************************************************/

/** Example of using G_SYS_DBG_LOG_V#
 *
 * Pure Text
 * G_SYS_DBG_LOG_V("Pure Text\r\n")
 *
 * With 1 variable
 * G_SYS_DBG_LOG_V1("Long Value = %ld\r\n", LongValueVar)
 *
 * With 2 variable
 * G_SYS_DBG_LOG_V2("Long Value = %ld and String = %s\r\n", LongValueVar, String)
 *
 * @return @ref GSysDebugger_result_t
 */

#if (ENABLE_SYSTEM_LOG && ENABLE_SYSTEM_DEBUGGER)

/* For Test Purpose ONLY - Comparison of regular printf vs sys debug and check the stack
#define G_SYS_DBG_LOG_V(arg) do {printf(arg);} while(0==1)
#define G_SYS_DBG_LOG_V2(arg,arg1,arg2) do {printf(arg,arg1,arg2);} while(0==1)
*/

#ifndef ENABLE_DEBUG_MESSAGE_QUEUE
/* WITH HEADER */
#define G_SYS_DBG_LOG_V(...)                    GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, __VA_ARGS__);

#define G_SYS_DBG_LOG_V1(arg,arg1)              GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1); \

#define G_SYS_DBG_LOG_V2(arg,arg1,arg2)         GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2); \

#define G_SYS_DBG_LOG_V3(arg,arg1,arg2,arg3)    GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3); \

#define G_SYS_DBG_LOG_V4(arg,arg1,arg2,arg3,arg4)   GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4); \

#define G_SYS_DBG_LOG_V5(arg,arg1,arg2,arg3,arg4,arg5)          GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4, arg5); \

#define G_SYS_DBG_LOG_V6(arg,arg1,arg2,arg3,arg4,arg5,arg6)     GSysDebugger_LogIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4, arg5, arg6); \

/* WITHOUT HEADER */
#define G_SYS_DBG_LOG_NV(...)                    GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, __VA_ARGS__);

#define G_SYS_DBG_LOG_NV1(arg,arg1)              GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1); \

#define G_SYS_DBG_LOG_NV2(arg,arg1,arg2)         GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2); \

#define G_SYS_DBG_LOG_NV3(arg,arg1,arg2,arg3)    GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3); \

#define G_SYS_DBG_LOG_NV4(arg,arg1,arg2,arg3,arg4)   GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4); \

#define G_SYS_DBG_LOG_NV5(arg,arg1,arg2,arg3,arg4,arg5)          GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4, arg5); \

#define G_SYS_DBG_LOG_NV6(arg,arg1,arg2,arg3,arg4,arg5,arg6)     GSysDebugger_LogNIV(DEFAULT_LOG_CATEGORY, DEFAULT_LOG_LEVEL, arg, arg1, arg2, arg3, arg4, arg5, arg6); \


/* WITH HEADER */
#define G_SYS_DBG_LOG_IV(c,l,...)                                   GSysDebugger_LogIV(c, l, __VA_ARGS__);

#define G_SYS_DBG_LOG_IV1(c,l,arg,arg1)                             GSysDebugger_LogIV(c, l, arg, arg1); \

#define G_SYS_DBG_LOG_IV2(c,l,arg,arg1,arg2)                        GSysDebugger_LogIV(c, l, arg, arg1, arg2); \

#define G_SYS_DBG_LOG_IV3(c,l,arg,arg1,arg2,arg3)                   GSysDebugger_LogIV(c, l, arg, arg1, arg2, arg3); \

#define G_SYS_DBG_LOG_IV4(c,l,arg,arg1,arg2,arg3,arg4)              GSysDebugger_LogIV(c, l, arg, arg1, arg2, arg3, arg4); \

#define G_SYS_DBG_LOG_IV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)         GSysDebugger_LogIV(c, l, arg, arg1, arg2, arg3, arg4, arg5); \

#define G_SYS_DBG_LOG_IV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)    GSysDebugger_LogIV(c, l, arg, arg1, arg2, arg3, arg4, arg5, arg6); \

/* WITHOUT HEADER */
#define G_SYS_DBG_LOG_INV(c,l,...)                                  GSysDebugger_LogNIV(c, l, __VA_ARGS__);

#define G_SYS_DBG_LOG_INV1(c,l,arg,arg1)                            GSysDebugger_LogNIV(c, l, arg, arg1); \

#define G_SYS_DBG_LOG_INV2(c,l,arg,arg1,arg2)                       GSysDebugger_LogNIV(c, l, arg, arg1, arg2); \

#define G_SYS_DBG_LOG_INV3(c,l,arg,arg1,arg2,arg3)                  GSysDebugger_LogNIV(c, l, arg, arg1, arg2, arg3); \

#define G_SYS_DBG_LOG_INV4(c,l,arg,arg1,arg2,arg3,arg4)             GSysDebugger_LogNIV(c, l, arg, arg1, arg2, arg3, arg4); \

#define G_SYS_DBG_LOG_INV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)        GSysDebugger_LogNIV(c, l, arg, arg1, arg2, arg3, arg4, arg5); \

#define G_SYS_DBG_LOG_INV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)   GSysDebugger_LogNIV(c, l, arg, arg1, arg2, arg3, arg4, arg5, arg6); \

#define G_SYS_DBG_LOG_LEVEL(x)                  GSysDebugger_ChangeLogLevel(x)


#else

#define G_SYS_DBG_LOG_V(...)
#define G_SYS_DBG_LOG_V1(arg,arg1)
#define G_SYS_DBG_LOG_V2(arg,arg1,arg2)
#define G_SYS_DBG_LOG_V3(arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_V4(arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_V5(arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_V6(arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_NV(...)
#define G_SYS_DBG_LOG_NV1(arg,arg1)
#define G_SYS_DBG_LOG_NV2(arg,arg1,arg2)
#define G_SYS_DBG_LOG_NV3(arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_NV4(arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_NV5(arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_NV6(arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_IV(c,l,...)
#define G_SYS_DBG_LOG_IV1(c,l,arg,arg1)
#define G_SYS_DBG_LOG_IV2(c,l,arg,arg1,arg2)
#define G_SYS_DBG_LOG_IV3(c,l,arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_IV4(c,l,arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_IV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_IV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_INV(c,l,...)
#define G_SYS_DBG_LOG_INV1(c,l,arg,arg1)
#define G_SYS_DBG_LOG_INV2(c,l,arg,arg1,arg2)
#define G_SYS_DBG_LOG_INV3(c,l,arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_INV4(c,l,arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_INV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_INV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_LEVEL(x)
#endif

#else

#define G_SYS_DBG_LOG_V(...)
#define G_SYS_DBG_LOG_V1(arg,arg1)
#define G_SYS_DBG_LOG_V2(arg,arg1,arg2)
#define G_SYS_DBG_LOG_V3(arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_V4(arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_V5(arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_V6(arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_NV(...)
#define G_SYS_DBG_LOG_NV1(arg,arg1)
#define G_SYS_DBG_LOG_NV2(arg,arg1,arg2)
#define G_SYS_DBG_LOG_NV3(arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_NV4(arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_NV5(arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_NV6(arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_IV(c,l,...)
#define G_SYS_DBG_LOG_IV1(c,l,arg,arg1)
#define G_SYS_DBG_LOG_IV2(c,l,arg,arg1,arg2)
#define G_SYS_DBG_LOG_IV3(c,l,arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_IV4(c,l,arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_IV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_IV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_INV(c,l,...)
#define G_SYS_DBG_LOG_INV1(c,l,arg,arg1)
#define G_SYS_DBG_LOG_INV2(c,l,arg,arg1,arg2)
#define G_SYS_DBG_LOG_INV3(c,l,arg,arg1,arg2,arg3)
#define G_SYS_DBG_LOG_INV4(c,l,arg,arg1,arg2,arg3,arg4)
#define G_SYS_DBG_LOG_INV5(c,l,arg,arg1,arg2,arg3,arg4,arg5)
#define G_SYS_DBG_LOG_INV6(c,l,arg,arg1,arg2,arg3,arg4,arg5,arg6)

#define G_SYS_DBG_LOG_LEVEL(x)
#endif

//debug semaphore
#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK && ENABLE_SYSTEM_LOG && ENABLE_SYSTEM_DEBUGGER

#define alSemRegister(a,b) \
    {\
        GSysSemDbg_Register(__FUNCTION__, __LINE__, a, b);\
    }

#define alSemTake(a,b)  \
    {\
        dxSemTake(a,b);\
        GSysSemDbg_Take(__FUNCTION__, __LINE__, a);\
    }
#define alSemGive(a)  \
    {\
        dxSemGive(a);\
        GSysSemDbg_Give(a);\
    }
#else

#define alSemRegister(a,b) \
    ({\
    })
#define alSemTake(a,b)  \
    {\
        dxSemTake(a,b);\
    }
#define alSemGive(a)  \
    {\
        dxSemGive(a);\
    }

#endif

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/
#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
typedef struct tagGSys_SemDbg_List_t
{
    struct tagGSys_SemDbg_List_t    *pNext;
    dxSemaphoreHandle_t             SempHandle;
    uint8_t                         *pInitFunc;
    uint8_t                         *pTakeFunc;
    dxTime_t                        TakeTick;
} aGSys_SemDbg_List_t;
#endif //#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/** Initialize System Debugger
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebuggerInit(void);

/** Register Thread for diagnostics
 *
 * @param[in]  thread    :  The thread to monitor for stack diagnostic
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread);

/** Register Thread for diagnostics with given name
 *
 * @param[in]  thread    :  The thread to monitor for stack diagnostic
 * @param[in]  ThreadName    :  Given Name, if NULL then system debugg will use the default name from thread (if given upon creation)
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName);


/** UnRegister Thread -remove from thread diagnostic
 *
 * @param[in]  thread    :  The thread to unregister
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread);

/** Send Log to SysDebugger with header info
 *  NOTE: You can use MACRO instead
 *
 * @param[in]  category          :	Category of the log base on GSysDebugger_category_t
 * @param[in]  level             :  Level of the log base on G_SYS_DBG_LEVEL_GENERAL_INFORMATION, G_SYS_DBG_LEVEL_FATAL_ERROR, or G_SYS_DBG_LEVEL_DETAIL_INFORMATION
 * @param[in]  fmt      		 :  Log and its argument to print
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...);

/** Send Log to SysDebugger without header info
 *  NOTE: You can use MACRO instead
 *
 * @param[in]  category          :	Category of the log base on GSysDebugger_category_t
 * @param[in]  level             :  Level of the log base on G_SYS_DBG_LEVEL_GENERAL_INFORMATION, G_SYS_DBG_LEVEL_FATAL_ERROR, or G_SYS_DBG_LEVEL_DETAIL_INFORMATION
 * @param[in]  fmt      		 :  Log and its argument to print
 *
 * @return @ref GSysDebugger_result_t
 */
extern GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...);


/** Get the buffer for log process if needed the size of buffer is LOG_MAX_CHAR_BUFFER
 *
 *  WARNING:    Not recommended to use this API directly, but if needed make sure to use GSysDebugger_RetainBuffPermissionAccess before calling this API
 *              and  GSysDebugger_ReleaseBuffPermissionAccess after work is done with the buffer (eg after calling GSysDebugger_Log)
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
extern char* GSysDebugger_GetBuffForLog(void);

/** Take CPU Usage buffer
 *
 *  WARNING:    Not recommended to use this API directly, but if needed make sure to use GSysDebugger_RetainBuffPermissionAccess before calling this API
 *              and  GSysDebugger_ReleaseBuffPermissionAccess after work is done with the buffer (eg after calling GSysDebugger_Log)
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
char* GSysDebugger_TakeCpuUsageBuffer(void);

/** Give CPU Usage buffer
 *
 *  WARNING:    Not recommended to use this API directly, but if needed make sure to use GSysDebugger_RetainBuffPermissionAccess before calling this API
 *              and  GSysDebugger_ReleaseBuffPermissionAccess after work is done with the buffer (eg after calling GSysDebugger_Log)
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
void GSysDebugger_GiveCpuUsageBuffer(void);

/** Obtain the access permission of buffer return by GSysDebugger_GetBuffForLog
 *
 *  WARNING: This is a semaphore locking mechanism
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
extern GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess(void);


/** Release the access permission of buffer return by GSysDebugger_GetBuffForLog
 *
 *  WARNING: This is a semaphore locking mechanism
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
extern GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess(void);

/** Change the log level.
 *
 *  WARNING: This is a semaphore locking mechanism
 *
 *  NOTE: This is generally use by macro G_SYS_DBG_LOG_V
 *
 * @return @ref char*
 */
extern GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level);

/** Read the log level.
 *
 * @return @ref uint8_t log level, please check G_SYS_DBG_LEVEL_XXX level definitions
 */
extern uint8_t GSysDebugger_ReadLogLevel(void);

extern int GSysDebugger_MulticastInit(void);
extern int GSysDebugger_MulticastSend(uint8_t *pData, int len);

char* GetSysDbgCategoryName (GSysDebugger_category_t category);
char* GetSysDbgLevelName (uint8_t level);
uint8_t* GSysDebugger_GetDebuggerLevel (void);
uint8_t* GSysDebugger_GetDebuggerCategory (void);


/**
 * @brief GSysDebugger_Dump
 */
extern GSysDebugger_result_t GSysDebugger_Dump (
    const char* title,
    int len,
    uint8_t* data
);


/**
 * @brief GSysDebugger_DumpString
 */
extern GSysDebugger_result_t GSysDebugger_DumpString (
    int len,
    uint8_t* buffer
);


#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
extern void GSysSemDbg_Register(uint8_t *strCallFrom, uint32_t Line, dxSemaphoreHandle_t SempHandle, int32_t event);
extern void GSysSemDbg_Take(uint8_t *strCallFrom, uint32_t Line, dxSemaphoreHandle_t SempHandle);
extern void GSysSemDbg_Give(dxSemaphoreHandle_t SempHandle);
extern void GSysSemDbg_Dump(void);
#endif //#if ENABLE_DEBUG_SEMAPHORE_DEADLOCK
