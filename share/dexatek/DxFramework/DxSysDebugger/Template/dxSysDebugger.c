//============================================================================
// File: dxSysdebugger.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxSysDebugger.h"

/******************************************************
 *                      Macros
 ******************************************************/


/******************************************************
 *                    Constants
 ******************************************************/


/******************************************************
 *                   Enumerations
 ******************************************************/


/******************************************************
 *                 Type Definitions
 ******************************************************/


/******************************************************
 *                    Structures
 ******************************************************/


/******************************************************
 *               Function Declarations
 ******************************************************/


/******************************************************
 *               Local Function Definitions
 ******************************************************/


/******************************************************
 *               Function Definitions
 ******************************************************/
GSysDebugger_result_t GSysDebuggerInit(void)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_RegisterForThreadDiagnosticGivenName(dxTaskHandle_t* thread, char* ThreadName)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_UnRegisterForThreadDiagnostic(dxTaskHandle_t* thread)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}


GSysDebugger_result_t GSysDebugger_LogIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}


GSysDebugger_result_t GSysDebugger_LogNIV(GSysDebugger_category_t category, uint8_t level, const char *fmt, ...)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t _sendLogEvent(char* LogStrBuffer, dxBOOL IncludeHeader, GSysDebugger_category_t category, uint8_t level)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

char* GSysDebugger_GetBuffForLog()
{
	// Todo...

    return NULL;
}

char* GSysDebugger_TakeCpuUsageBuffer ()
{
	// Todo...

    return NULL;
}

void GSysDebugger_GiveCpuUsageBuffer ()
{
	// Todo...

}

GSysDebugger_result_t GSysDebugger_RetainBuffPermissionAccess()
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ReleaseBuffPermissionAccess()
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_ChangeLogLevel(uint8_t level)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

uint8_t GSysDebugger_ReadLogLevel(void)
{
    return 0;
}

int GSysDebugger_MulticastInit(void)
{
	// Todo...

    return -1;
}

int GSysDebugger_MulticastSend(uint8_t *pData, int len)
{
	// Todo...

    return -1;
}

uint8_t* GSysDebugger_GetDebuggerLevel (void)
{
	// Todo...

    return NULL;
}

uint8_t* GSysDebugger_GetDebuggerCategory (void)
{
	// Todo...

    return NULL;
}

GSysDebugger_result_t GSysDebugger_Dump(const char* title, int len,uint8_t* data)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}

GSysDebugger_result_t GSysDebugger_DumpString(int len, uint8_t* buffer)
{
	// Todo...

    return G_SYS_DBG_DISABLED;
}


