//============================================================================
// File: dxdxKeypadManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/08
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "dxKeypadManager.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

#define DEBOUNCE_TIME_MS                                    (100)
#define KEYPAD_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE        (384)
#define KEYPAD_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE   (5)

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    uint32_t                    PinName;
    dxKEYPAD_TRIGGER_LEVEL      ConfiguredLevelTrigger;
    int                         CurrentLevelState;
    dxBOOL                      KeyHeldPressed;
    dxTime_t                    KeyPressedNextReportTime;
    dxBOOL                      KeyHeldEventSent;
} dxKeypadIntObj_t;


//============================================================================
// Global Var
//============================================================================
dxTime_t            CurrentTime;
dxTimerHandle_t     KeyTouchMonitorTimer;
dxWorkweTask_t      EventWorkerThread;
dxEventHandler_t    RegisteredEventFunction;
dxKeypad_t*         CurrentConfigTable;

dxKeypad_t dxKeypadConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXKEYPAD_TRIGGER_LEVEL_LOW,
        MAIN_BUTTON_REPORT_ID,
        DX_GENERAL_INTERVAL_ONE_SECOND,
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG
    }
};

//============================================================================
// Internal Function
//============================================================================

void KeyPadGpioHandler (dxKeypadIntObj_t* KeypadHdl)
{
    if ( KeypadHdl != NULL )
    {
        dxBOOL bChangeOfStateDetected = dxFALSE;
        int CurrentGPIOLevel = dxGPIO_Read(KeypadHdl->PinName);
        if  (KeypadHdl->CurrentLevelState != CurrentGPIOLevel)
        {
            KeypadHdl->CurrentLevelState = CurrentGPIOLevel;
            bChangeOfStateDetected = dxTRUE;
        }

        if (bChangeOfStateDetected)
        {
            int ConfiguredTriggerLevelTranslation = DXPIN_LOW;
            if (KeypadHdl->ConfiguredLevelTrigger == DXKEYPAD_TRIGGER_LEVEL_HIGH)
                ConfiguredTriggerLevelTranslation = DXPIN_HIGH;

            if (KeypadHdl->CurrentLevelState == ConfiguredTriggerLevelTranslation)
            {
                KeypadHdl->KeyPressedNextReportTime = dxTimeGetMS() + DEBOUNCE_TIME_MS;
                KeypadHdl->KeyHeldPressed = dxTRUE;
            }
            else
            {
                KeypadHdl->KeyHeldPressed = dxFALSE;
            }
        }
    }
}


static void check_state_timer_handler( void* arg )
{
    CurrentTime = dxTimeGetMS();
    dxKeypad_t* pKeyPad = NULL;

    for (pKeyPad = CurrentConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
    {
        if (pKeyPad->pObject != NULL)
        {
            dxKeypadIntObj_t* IntKeypadObj = (dxKeypadIntObj_t*)pKeyPad->pObject;

            //Check the GPIO and update its state to it object handler
            KeyPadGpioHandler (IntKeypadObj);

            if (IntKeypadObj->KeyPressedNextReportTime != 0)
            {
                if (IntKeypadObj->KeyHeldPressed == dxTRUE)
                {
                    if (CurrentTime > IntKeypadObj->KeyPressedNextReportTime)
                    {
                        IntKeypadObj->KeyPressedNextReportTime = CurrentTime + pKeyPad->KeyHeldReportIntervalMs;


                        dxKeypadReport_t* pKeyReportObj = (dxKeypadReport_t*)dxMemAlloc( "KeyPad Event Report Msg Buff", 1, sizeof(dxKeypadReport_t));

                        pKeyReportObj->ReportIdentifier = pKeyPad->ReportIdentifier;

                        if (IntKeypadObj->KeyHeldEventSent != dxTRUE)
                        {
                            //First Time we send as PRESSED
                            pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_PRESSED;
                        }
                        else
                        {
                            pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_HELD;
                        }

                        IntKeypadObj->KeyHeldEventSent = dxTRUE;

                        dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, (void*)pKeyReportObj);

                    }
                }
                else //Key released
                {
                    if (IntKeypadObj->KeyHeldEventSent == dxTRUE)
                    {
                        dxKeypadReport_t* pKeyReportObj = (dxKeypadReport_t*)dxMemAlloc( "KeyPad Event Report Msg Buff", 1, sizeof(dxKeypadReport_t));

                        pKeyReportObj->ReportIdentifier = pKeyPad->ReportIdentifier;
                        pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_RELEASED;
                        dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, (void*)pKeyReportObj);

                    }

                    //Cleanup the flags
                    IntKeypadObj->KeyPressedNextReportTime = 0;
                    IntKeypadObj->KeyHeldEventSent = dxFALSE;

                }
            }
        }
    }

}

//============================================================================
// Function
//============================================================================

dxKEYPAD_RET_CODE dxKeypad_Init( dxKeypad_t* ConfigTable, dxKeypadCallbackFunction_t EventFunction)
{
    dxKEYPAD_RET_CODE RetCode = DXKEYPAD_SUCCESS;

    if ((ConfigTable == NULL) || (EventFunction == NULL))
        return DXKEYPAD_INVALID_PARAMETER;

    if (CurrentConfigTable)
        return DXKEYPAD_ALREADY_INITIALISED;

    KeyTouchMonitorTimer =dxTimerCreate("KeypadTimer",
                                        100,
                                        dxTRUE,
                                        &KeyTouchMonitorTimer,
                                        check_state_timer_handler);

    if (KeyTouchMonitorTimer == NULL)
    {
        G_SYS_DBG_LOG_V("WARNING: dxKeypad_Init failed unable to create timer!\n");
        return DXKEYPAD_INTERNAL_ERROR;
    }

    dxOS_RET_CODE WorkerThreadResult = dxWorkerTaskCreate("KEYMGR W",
                                                          &EventWorkerThread,
                                                          DX_DEFAULT_WORKER_PRIORITY,
                                                          KEYPAD_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE,
                                                          KEYPAD_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE);

    if (WorkerThreadResult != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V( "dxKeypad_Init - Failed to create worker thread\r\n");
        dxTimerDelete(KeyTouchMonitorTimer, DXOS_WAIT_FOREVER);
        return DXKEYPAD_INTERNAL_ERROR;
    }
    else
    {
        RegisteredEventFunction = EventFunction;
        GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "KeyPadManager Event Thread");
    }

    uint8_t index = 0;
    dxKeypad_t *pKeyPad = NULL;
    for (pKeyPad = ConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
    {

        index++; //Add ahead
        if (pKeyPad->GpioForKey == DX_KEYPAD_NOT_ASSIGNED)
            continue;

        pKeyPad->pObject = calloc(1, sizeof(dxKeypadIntObj_t));

        if (pKeyPad->pObject != NULL)
        {
            dxKeypadIntObj_t* IntKeypadObj = (dxKeypadIntObj_t*)pKeyPad->pObject;
            IntKeypadObj->ConfiguredLevelTrigger = pKeyPad->LevelTrigger;
            IntKeypadObj->CurrentLevelState = (IntKeypadObj->ConfiguredLevelTrigger == DXKEYPAD_TRIGGER_LEVEL_LOW) ? DXPIN_LOW:DXPIN_HIGH; //Make the same for initial state
            IntKeypadObj->KeyPressedNextReportTime = 0;
            IntKeypadObj->PinName = pKeyPad->GpioForKey;
        }
        else
        {
            G_SYS_DBG_LOG_V1("WARNING: dxKeypad_Init failed at index %d (start with 1)\n", index);
        }

    }

    if (RetCode == DXKEYPAD_SUCCESS)
    {
        CurrentConfigTable = ConfigTable;
        dxTimerStart(KeyTouchMonitorTimer,DXOS_WAIT_FOREVER);
    }
    else // Failed so we clean up
    {
        dxTimerDelete(KeyTouchMonitorTimer, DXOS_WAIT_FOREVER);
        dxWorkerTaskDelete(&EventWorkerThread);
    }

    return RetCode;
}


dxKEYPAD_RET_CODE dxKeypad_UninitCurrentTable()
{
    dxKEYPAD_RET_CODE Success = DXKEYPAD_SUCCESS;

    if (CurrentConfigTable)
    {
        dxKeypad_t *pKeyPad = NULL;
        for (pKeyPad = CurrentConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
        {
            if (pKeyPad->pObject != NULL)
            {
                dxMemFree(pKeyPad->pObject);
            }
        }

        CurrentConfigTable = NULL;
    }

    return Success;
}

dxKEYPAD_RET_CODE dxKeypad_CleanEventArgumentObj(void* arg)
{
    if (arg)
        dxMemFree(arg);

    return DXKEYPAD_SUCCESS;
}

