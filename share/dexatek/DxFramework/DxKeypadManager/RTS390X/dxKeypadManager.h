//============================================================================
// File: dxKeypadManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DXKEYPADMANAGER_H
#define _DXKEYPADMANAGER_H

#if defined(configUSE_LOCAL_GPIO_SETTING) && configUSE_LOCAL_GPIO_SETTING == 1
#include "dxGPIO.h"
#else // configUSE_LOCAL_GPIO_SETTING
#pragma message ("NOTE: Project specific GPIO setting disabled, will use default dxGPIOConfig.h!!!!")
#include "dxGPIOConfig.h"
#endif // configUSE_LOCAL_GPIO_SETTING

//============================================================================
// Defines
//============================================================================

#define DX_ENDOF_KEYPAD_CONFIG            0xFFFFFFFF
#define DX_KEYPAD_NOT_ASSIGNED            0xFFFFFFFE

#define DX_GENERAL_INTERVAL_ONE_SECOND    1000


//============================================================================
// Enumeration
//============================================================================

typedef enum {

    DXKEYPAD_SUCCESS                      = 0,
    DXKEYPAD_ALREADY_INITIALISED          = 1,

    DXKEYPAD_NOT_INITIALIZED              = -1,
    DXKEYPAD_INVALID_PARAMETER            = -2,
    DXKEYPAD_MEM_ALLOC_ERROR              = -3,
    DXKEYPAD_INTERNAL_ERROR               = -4,
  
} dxKEYPAD_RET_CODE;

typedef enum {

    DXKEYPAD_TRIGGER_LEVEL_LOW                      = 1,
    DXKEYPAD_TRIGGER_LEVEL_HIGH                     = 2,
    
} dxKEYPAD_TRIGGER_LEVEL;

typedef enum {

    DXKEYPAD_KEY_PRESSED                    = 1,
    DXKEYPAD_KEY_HELD                       = 2,
    DXKEYPAD_KEY_RELEASED                   = 3,
    
} dxKEYPAD_EVENT;

typedef struct
{
    void*                       pObject;                    //Internal use - Should initialized to NULL
    uint32_t                    GpioForKey;
    dxKEYPAD_TRIGGER_LEVEL      LevelTrigger;
    uint32_t                    ReportIdentifier;           //User defined for identification purposes when reporting to appication of key event
    uint32_t                    KeyHeldReportIntervalMs;    //NOTE: Use in step of 100ms
    
} dxKeypad_t;

typedef struct
{
    uint32_t                    ReportIdentifier;
    dxKEYPAD_EVENT              KeyStateEvent;    
} dxKeypadReport_t;

//============================================================================
// Type Defines
//============================================================================
typedef void (*dxKeypadCallbackFunction_t)(void* arg);


//============================================================================
// Keypad Config Table
//============================================================================

//#define MAIN_BUTTON_GPIO                        PD_4
#define MAIN_BUTTON_REPORT_ID                   1

//#define SUB_BUTTON_GPIO                         PE_5    //Secondary button for Test Purpose
#define SUB_BUTTON_REPORT_ID                    2       //Secondary button for Test Purpose

//README FIRST!!!
//In RTS390X platform ALL GPIO must be initialized from dxGPIO_Init in dxBSP. There should be one global table that defined all GPIO
//configurations. dxKeypadManager will not initialize the GPIO..

/*static dxKeypad_t dxKeypadConfig[] =
{
    {
        NULL,
        MAIN_BUTTON_GPIO,
        DXKEYPAD_TRIGGER_LEVEL_LOW,
        MAIN_BUTTON_REPORT_ID,
        DX_GENERAL_INTERVAL_ONE_SECOND,
    },

    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG,
        DX_ENDOF_KEYPAD_CONFIG
    }
};*/

extern dxKeypad_t dxKeypadConfig[];

//============================================================================
// Function
//============================================================================

/**
 * @brief Initialized KeyPad Manager all Key event or activity will be reported by calling EventFunction passing the arg as dxKeypadReport_t
 *          NOTE1: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data from dxKeypadReport_t
 *          NOTE2:  At the end of EventFunction call dxKeypad_CleanEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 * 
 * @param[IN] ConfigTable - The table containing gpio assigment and setup for the keypad
 * @param[IN] EventFunction - The callback function when key event occurs
 * @return dxKEYPAD_RET_CODE
 */
dxKEYPAD_RET_CODE dxKeypad_Init( dxKeypad_t* ConfigTable, dxKeypadCallbackFunction_t EventFunction);

/**
 * @brief       Deinitialize the KeypadManager
 * @param[IN]   ConfigTable - The table to uninitialise
 * @return      dxKEYPAD_RET_CODE
 */
dxKEYPAD_RET_CODE dxKeypad_UninitCurrentTable(void);

/**
 * @brief   Clears the callback event arguments
 * @param   arg - the argument from the eventfunction.
 * @return  dxKEYPAD_RET_CODE
 */
dxKEYPAD_RET_CODE dxKeypad_CleanEventArgumentObj(void* arg);


#endif // _DXKEYPADMANAGER_H