//============================================================================
// File: dxdxKeypadManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/08
//     1) Description: Initial
//============================================================================
#include "device.h"
#include "gpio_irq_api.h"   // mbed
#include "gpio_irq_ex_api.h"

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "dxKeypadManager.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

#define DEBOUNCE_TIME_MS                                    (100)
#define KEYPAD_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE        (384)
#define KEYPAD_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE   (5)

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    gpio_irq_t*                 pKeyPadObject;
    dxKEYPAD_TRIGGER_LEVEL      ConfiguredLevelTrigger;
    dxKEYPAD_TRIGGER_LEVEL      CurrentLevelTriggerState;
    dxBOOL                      KeyHeldPressed;
    dxTime_t                    KeyPressedNextReportTime;
    dxBOOL                      KeyHeldEventSent;
} dxKeypadIntObj_t;


//============================================================================
// Global Var
//============================================================================
dxTime_t            CurrentTime;
dxTimerHandle_t     KeyTouchMonitorTimer;
dxWorkweTask_t      EventWorkerThread;
dxEventHandler_t    RegisteredEventFunction;
dxKeypad_t*         CurrentConfigTable;

//============================================================================
// Internal Function
//============================================================================

void gpio_level_irq_handler (uint32_t id, gpio_irq_event event)
{
    //G_SYS_DBG_LOG_V1("gpio_level_irq_handler event = %d\n", event);

    dxKeypadIntObj_t* pIntKeypadObj = (dxKeypadIntObj_t*)id;

    if ( pIntKeypadObj != NULL )
    {
        // Disable level irq because the irq will keep triggered when it keeps in same level.
        gpio_irq_disable(pIntKeypadObj->pKeyPadObject);


        if ( pIntKeypadObj->CurrentLevelTriggerState == pIntKeypadObj->ConfiguredLevelTrigger )
        {
            pIntKeypadObj->KeyPressedNextReportTime = dxTimeGetMS() + DEBOUNCE_TIME_MS;
            pIntKeypadObj->KeyHeldPressed = dxTRUE;
        }
        else if (pIntKeypadObj->CurrentLevelTriggerState != pIntKeypadObj->ConfiguredLevelTrigger)
        {
            pIntKeypadObj->KeyHeldPressed = dxFALSE;
        }

        //Make sure we swap the state of irq level trigger so that we will get here whenever pin level changes.
        if (pIntKeypadObj->CurrentLevelTriggerState == IRQ_LOW)
        {
            pIntKeypadObj->CurrentLevelTriggerState = IRQ_HIGH;
            gpio_irq_set(pIntKeypadObj->pKeyPadObject, IRQ_HIGH, 1);
            gpio_irq_enable(pIntKeypadObj->pKeyPadObject);
        }
        else if (pIntKeypadObj->CurrentLevelTriggerState == IRQ_HIGH)
        {
             pIntKeypadObj->CurrentLevelTriggerState = IRQ_LOW;
            gpio_irq_set(pIntKeypadObj->pKeyPadObject, IRQ_LOW, 1);
            gpio_irq_enable(pIntKeypadObj->pKeyPadObject);
        }

    }
}


static void check_state_timer_handler( void* arg )
{
    CurrentTime = dxTimeGetMS();
    for (dxKeypad_t *pKeyPad = CurrentConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
    {
        if (pKeyPad->pObject != NULL)
        {
            dxKeypadIntObj_t* IntKeypadObj = (dxKeypadIntObj_t*)pKeyPad->pObject;

            if (IntKeypadObj->KeyPressedNextReportTime != 0)
            {
                if (IntKeypadObj->KeyHeldPressed == dxTRUE)
                {
                    if (CurrentTime > IntKeypadObj->KeyPressedNextReportTime)
                    {
                        IntKeypadObj->KeyPressedNextReportTime = CurrentTime + pKeyPad->KeyHeldReportIntervalMs;


                        dxKeypadReport_t* pKeyReportObj = (dxKeypadReport_t*)dxMemAlloc( "KeyPad Event Report Msg Buff", 1, sizeof(dxKeypadReport_t));

                        pKeyReportObj->ReportIdentifier = pKeyPad->ReportIdentifier;

                        if (IntKeypadObj->KeyHeldEventSent != dxTRUE)
                        {
                            //First Time we send as PRESSED
                            pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_PRESSED;
                        }
                        else
                        {
                            pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_HELD;
                        }

                        IntKeypadObj->KeyHeldEventSent = dxTRUE;

                        dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, (void*)pKeyReportObj);

                    }
                }
                else //Key released
                {
                    if (IntKeypadObj->KeyHeldEventSent == dxTRUE)
                    {
                        dxKeypadReport_t* pKeyReportObj = (dxKeypadReport_t*)dxMemAlloc( "KeyPad Event Report Msg Buff", 1, sizeof(dxKeypadReport_t));

                        pKeyReportObj->ReportIdentifier = pKeyPad->ReportIdentifier;
                        pKeyReportObj->KeyStateEvent = DXKEYPAD_KEY_RELEASED;
                        dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, (void*)pKeyReportObj);

                    }

                    //Cleanup the flags
                    IntKeypadObj->KeyPressedNextReportTime = 0;
                    IntKeypadObj->KeyHeldEventSent = dxFALSE;

                }
            }
        }
    }

}

//============================================================================
// Function
//============================================================================

dxKEYPAD_RET_CODE dxKeypad_Init( dxKeypad_t* ConfigTable, dxKeypadCallbackFunction_t EventFunction)
{
    dxKEYPAD_RET_CODE RetCode = DXKEYPAD_SUCCESS;

    if ((ConfigTable == NULL) || (EventFunction == NULL))
        return DXKEYPAD_INVALID_PARAMETER;

    if (CurrentConfigTable)
        return DXKEYPAD_ALREADY_INITIALISED;

    KeyTouchMonitorTimer =dxTimerCreate("KeypadTimer",
                                        100,
                                        dxTRUE,
                                        &KeyTouchMonitorTimer,
                                        check_state_timer_handler);

    if (KeyTouchMonitorTimer == NULL)
    {
        G_SYS_DBG_LOG_V("WARNING: dxKeypad_Init failed unable to create timer!\n");
        return DXKEYPAD_INTERNAL_ERROR;
    }

    dxOS_RET_CODE WorkerThreadResult = dxWorkerTaskCreate("KEYMGR W",
                                                          &EventWorkerThread,
                                                          DX_DEFAULT_WORKER_PRIORITY,
                                                          KEYPAD_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE,
                                                          KEYPAD_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE);

    if (WorkerThreadResult != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V( "dxKeypad_Init - Failed to create worker thread\r\n");
        dxTimerDelete(KeyTouchMonitorTimer, DXOS_WAIT_FOREVER);
        return DXKEYPAD_INTERNAL_ERROR;
    }
    else
    {
        RegisteredEventFunction = EventFunction;
        GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "KeyPadManager Event Thread");
    }

    uint8_t index = 0;
    for (dxKeypad_t *pKeyPad = ConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
    {

        index++; //Add ahead
        if (pKeyPad->GpioForKey == DX_KEYPAD_NOT_ASSIGNED)
            continue;

        pKeyPad->pObject = calloc(1, sizeof(dxKeypadIntObj_t));

        if (pKeyPad->pObject != NULL)
        {
            dxKeypadIntObj_t* IntKeypadObj = (dxKeypadIntObj_t*)pKeyPad->pObject;
            IntKeypadObj->ConfiguredLevelTrigger = (pKeyPad->LevelTrigger == DXKEYPAD_TRIGGER_LEVEL_LOW) ? IRQ_LOW : IRQ_HIGH;
            IntKeypadObj->CurrentLevelTriggerState = IntKeypadObj->ConfiguredLevelTrigger; //Make the same for initial state
            IntKeypadObj->KeyPressedNextReportTime = 0;
            IntKeypadObj->pKeyPadObject = calloc(1, sizeof(gpio_irq_t));

            if (IntKeypadObj->pKeyPadObject == NULL)
            {
                dxKeypad_UninitCurrentTable();
                G_SYS_DBG_LOG_V("WARNING: dxKeypad_Init failed due to mem alloc issue\n");
                RetCode = DXKEYPAD_MEM_ALLOC_ERROR;
            }
            else
            {
                // configure level trigger handler
                gpio_irq_init((gpio_irq_t*)IntKeypadObj->pKeyPadObject, pKeyPad->GpioForKey, gpio_level_irq_handler, (uint32_t)(pKeyPad->pObject));
                gpio_irq_set((gpio_irq_t*)IntKeypadObj->pKeyPadObject, IntKeypadObj->ConfiguredLevelTrigger, 1);
                gpio_irq_enable((gpio_irq_t*)IntKeypadObj->pKeyPadObject);
            }
        }
        else
        {
            G_SYS_DBG_LOG_V1("WARNING: dxKeypad_Init failed at index %d (start with 1)\n", index);
        }

    }

    if (RetCode == DXKEYPAD_SUCCESS)
    {
        CurrentConfigTable = ConfigTable;
        dxTimerStart(KeyTouchMonitorTimer,DXOS_WAIT_FOREVER);
    }
    else // Failed so we clean up
    {
        dxTimerDelete(KeyTouchMonitorTimer, DXOS_WAIT_FOREVER);
        dxWorkerTaskDelete(&EventWorkerThread);
    }

    return RetCode;
}


dxKEYPAD_RET_CODE dxKeypad_UninitCurrentTable()
{
    dxKEYPAD_RET_CODE Success = DXKEYPAD_SUCCESS;

    if (CurrentConfigTable)
    {
        for (dxKeypad_t *pKeyPad = CurrentConfigTable; pKeyPad->GpioForKey != DX_ENDOF_KEYPAD_CONFIG; pKeyPad++)
        {
            if (pKeyPad->pObject != NULL)
            {
                dxKeypadIntObj_t* IntKeypadObj = (dxKeypadIntObj_t*)pKeyPad->pObject;
                if (IntKeypadObj->pKeyPadObject != NULL)
                    dxMemFree(IntKeypadObj->pKeyPadObject);

                IntKeypadObj->pKeyPadObject = NULL;

                dxMemFree(pKeyPad->pObject);
            }
        }

        CurrentConfigTable = NULL;
    }

    return Success;
}

dxKEYPAD_RET_CODE dxKeypad_CleanEventArgumentObj(void* arg)
{
    if (arg)
        dxMemFree(arg);

    return DXKEYPAD_SUCCESS;
}

