//============================================================================
// File: dxdxKeypadManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxKeypadManager.h"

//============================================================================
// Macros
//============================================================================


//============================================================================
// Constants
//============================================================================


//============================================================================
// Enum
//============================================================================


//============================================================================
// Structure
//============================================================================


//============================================================================
// Global Var
//============================================================================


//============================================================================
// Function
//============================================================================
dxKEYPAD_RET_CODE dxKeypad_Init( dxKeypad_t* ConfigTable, dxKeypadCallbackFunction_t EventFunction)
{
	// Todo...

    return DXKEYPAD_INTERNAL_ERROR;
}

dxKEYPAD_RET_CODE dxKeypad_UninitCurrentTable()
{
	// Todo...

    return DXKEYPAD_INTERNAL_ERROR;
}

dxKEYPAD_RET_CODE dxKeypad_CleanEventArgumentObj(void* arg)
{
	// Todo...

    return DXKEYPAD_INTERNAL_ERROR;
}

