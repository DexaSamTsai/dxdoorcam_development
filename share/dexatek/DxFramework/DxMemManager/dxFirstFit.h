//============================================================================
// File: dxFirstFit.h
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef __DX_FIRST_FIT_H__
#define __DX_FIRST_FIT_H__

//============================================================================
//    Include
//============================================================================

#include "dxOS.h"



//============================================================================
//    Define
//============================================================================



//============================================================================
//    Enumerations
//============================================================================



//============================================================================
//    Structures
//============================================================================



//============================================================================
//    Function Declarations
//============================================================================

#ifdef __cplusplus
extern "C" {
#endif

/*---- Public Function -----------------------------------------------------*/

/**
 * @breif FirstFit_Init
 */
void FirstFit_Init (uint8_t* memory, size_t memorySize);


/**
 * @breif FirstFit_Malloc
 */
void* FirstFit_Malloc (size_t xWantedSize);


/**
 * @breif FirstFit_ReAlloc
 */
void* FirstFit_ReAlloc (void* pv,  size_t wantedSize);


/**
 * @breif FirstFit_Free
 */
void FirstFit_Free (void *pv);


/**
 * @breif FirstFit_GetFreeHeapSize
 */
size_t FirstFit_GetFreeHeapSize (void);


/**
 * @breif FirstFit_GetMinimumEverFreeHeapSize
 */
size_t FirstFit_GetMinimumEverFreeHeapSize (void);


#ifdef __cplusplus
}
#endif

#endif
