//============================================================================
// File: dxFirstFit.c
//
// A sample implementation of FirstFit_Malloc () and FirstFit_Free () that combines
// (coalescences) adjacent memory blocks as they are freed, and in so doing
// limits memory fragmentation.
// See heap_1.c, heap_2.c and heap_3.c for alternative implementations, and the
// memory management pages of http://www.FreeRTOS.org for more information.
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

//============================================================================
//    Include
//============================================================================

#include <stdlib.h>
#include "dxFirstFit.h"



//============================================================================
//    Define
//============================================================================

#define FIRSTFIT_BYTE_ALIGNMENT          (8)
#define FIRSTFIT_BYTE_ALIGNMENT_MASK     (0x0007U)

/* Block sizes must not get too small. */
#define FIRSTFIT_MINIMUM_BLOCK_SIZE      ((size_t) (heapStructSize * 2))

/* Assumes 8bit bytes! */
#define FIRSTFIT_BITS_PER_BYTE           ((size_t) 8)



//============================================================================
//    Structures
//============================================================================

/* Define the linked list structure.  This is used to link free blocks in order
of their memory address. */
typedef struct _BlockLink_t {
    struct _BlockLink_t* nextFreeBlock;   /*<< The next free block in the list. */
    size_t blockSize;                     /*<< The size of the free block. */
} BlockLink_t;



//============================================================================
//    Function Declarations
//============================================================================

/*---- Private Function ----------------------------------------------------*/

/*
 * Inserts a block of memory that is being freed into the correct position in
 * the list of free memory blocks.  The block being freed will be merged with
 * the block in front it and/or the block behind it if the memory blocks are
 * adjacent to each other.
 */
static void _FirstFit_InsertBlockIntoFreeList (BlockLink_t *pxBlockToInsert);

/*
 * Called automatically to setup the required heap structures the first time
 * FirstFit_Malloc () is called.
 */
static void _FirstFit_HeapInit (void);



//============================================================================
//    Global Variable
//============================================================================

/* The begin address for the heap. */
static uint8_t* heap = NULL;

/* The total size for the heap. */
static size_t heapSize = 0;

/* The mutex to protect heap. */
static dxMutexHandle_t heapMutex = NULL;

/* The size of the structure placed at the beginning of each allocated memory
block must by correctly byte aligned. */
static const size_t heapStructSize = ((sizeof (BlockLink_t) + (FIRSTFIT_BYTE_ALIGNMENT - 1)) & ~FIRSTFIT_BYTE_ALIGNMENT_MASK);

/* Create a couple of list links to mark the start and end of the list. */
static BlockLink_t blockLinkStart;
static BlockLink_t* blockLinkEnd = NULL;

/* Keeps track of the number of free bytes remaining, but says nothing about
fragmentation. */
static size_t freeBytesRemaining = 0U;
static size_t minimumEverFreeBytesRemaining = 0U;

/* Gets set to the top bit of an size_t type. When this bit in the blockSize
member of an BlockLink_t structure is set then the block belongs to the
application.  When the bit is free the block is still part of the free heap
space. */
static size_t blockAllocatedBit = 0;



//============================================================================
//    Implementation
//============================================================================

/*---- Public Function -----------------------------------------------------*/

void FirstFit_Init (uint8_t* memory, size_t memorySize) {
    heap = memory;
    heapSize = memorySize;
    if (!heapMutex) {
        heapMutex = dxMutexCreate ();
    }
}


void* FirstFit_Malloc (size_t wantedSize) {
    BlockLink_t* block;
    BlockLink_t* previousBlock;
    BlockLink_t* newBlock;
    void* memory = NULL;

    dxMutexTake (heapMutex, DXOS_WAIT_FOREVER);
    {
        /* If this is the first call to malloc then the heap will require
        initialisation to setup the list of free blocks. */
        if (blockLinkEnd == NULL) {
            _FirstFit_HeapInit ();
        }

        /* Check the requested block size is not so large that the top bit is
        set.  The top bit of the block size member of the BlockLink_t structure
        is used to determine who owns the block - the application or the
        kernel, so it must be free. */
        if ((wantedSize & blockAllocatedBit) == 0) {
            /* The wanted size is increased so it can contain a BlockLink_t
            structure in addition to the requested amount of bytes. */
            if (wantedSize > 0) {
                wantedSize += heapStructSize;

                /* Ensure that blocks are always aligned to the required number
                of bytes. */
                if ((wantedSize & FIRSTFIT_BYTE_ALIGNMENT_MASK) != 0x00) {
                    /* Byte alignment required. */
                    wantedSize += (FIRSTFIT_BYTE_ALIGNMENT - (wantedSize & FIRSTFIT_BYTE_ALIGNMENT_MASK));
                    assert ((wantedSize & FIRSTFIT_BYTE_ALIGNMENT_MASK) == 0);
                }
            }

            if ((wantedSize > 0) && (wantedSize <= freeBytesRemaining)) {
                /* Traverse the list from the start (lowest address) block until
                one of adequate size is found. */
                previousBlock = &blockLinkStart;
                block = blockLinkStart.nextFreeBlock;
                while ((block->blockSize < wantedSize) && (block->nextFreeBlock != NULL)) {
                    previousBlock = block;
                    block = block->nextFreeBlock;
                }

                /* If the end marker was reached then a block of adequate size
                was not found. */
                if (block != blockLinkEnd) {
                    /* Return the memory space pointed to - jumping over the
                    BlockLink_t structure at its start. */
                    memory = (void*) (((uint8_t*) previousBlock->nextFreeBlock) + heapStructSize);

                    /* This block is being returned for use so must be taken out
                    of the list of free blocks. */
                    previousBlock->nextFreeBlock = block->nextFreeBlock;

                    /* If the block is larger than required it can be split into
                    two. */
                    if ((block->blockSize - wantedSize) > FIRSTFIT_MINIMUM_BLOCK_SIZE) {
                        /* This block is to be split into two.  Create a new
                        block following the number of bytes requested. The void
                        cast is used to prevent byte alignment warnings from the
                        compiler. */
                        newBlock = (void*) (((uint8_t*) block ) + wantedSize);
                        assert ((((uint32_t) newBlock) & FIRSTFIT_BYTE_ALIGNMENT_MASK) == 0);

                        /* Calculate the sizes of two blocks split from the
                        single block. */
                        newBlock->blockSize = block->blockSize - wantedSize;
                        block->blockSize = wantedSize;

                        /* Insert the new block into the list of free blocks. */
                        _FirstFit_InsertBlockIntoFreeList ((newBlock));
                    }

                    freeBytesRemaining -= block->blockSize;

                    if (freeBytesRemaining < minimumEverFreeBytesRemaining) {
                        minimumEverFreeBytesRemaining = freeBytesRemaining;
                    }

                    /* The block is being returned - it is allocated and owned
                    by the application and has no "next" block. */
                    block->blockSize |= blockAllocatedBit;
                    block->nextFreeBlock = NULL;
                }
            }
        }
    }
    dxMutexGive (heapMutex);

    assert ((((uint32_t) memory) & FIRSTFIT_BYTE_ALIGNMENT_MASK) == 0);
    return memory;
}


void* FirstFit_ReAlloc (void* memoryToFree,  size_t wantedSize) {

    uint8_t* p = (uint8_t*) memoryToFree;
    BlockLink_t* block;

    if (memoryToFree) {
        if (!wantedSize) {
            FirstFit_Free (memoryToFree);
            return NULL;
        }

        void* memory = FirstFit_Malloc (wantedSize);
        if (memory) {
            /* The memory being freed will have an BlockLink_t structure immediately
                before it. */
            p -= heapStructSize;

            /* This casting is to keep the compiler from issuing warnings. */
            block = (void*) p;

            int oldSize =  (block->blockSize & ~blockAllocatedBit) - heapStructSize;
            int copySize = (oldSize < wantedSize) ? oldSize : wantedSize;
            memcpy (memory, memoryToFree, copySize);

            dxMutexTake (heapMutex, DXOS_WAIT_FOREVER);
            {
                /* Add this block to the list of free blocks. */
                block->blockSize &= ~blockAllocatedBit;
                freeBytesRemaining += block->blockSize;
                _FirstFit_InsertBlockIntoFreeList (((BlockLink_t*) block));
            }
            dxMutexGive (heapMutex);
            return memory;
        }
    }
    else if (wantedSize)
        return FirstFit_Malloc (wantedSize);
    else
        return NULL;

    return NULL;
}


void FirstFit_Free (void* memoryToFree) {

    uint8_t* p = (uint8_t*) memoryToFree;
    BlockLink_t* block;

    if (memoryToFree != NULL) {
        /* The memory being freed will have an BlockLink_t structure immediately
        before it. */
        p -= heapStructSize;

        /* This casting is to keep the compiler from issuing warnings. */
        block = (void*) p;

        /* Check the block is actually allocated. */
        assert ((block->blockSize & blockAllocatedBit) != 0);
        assert (block->nextFreeBlock == NULL);

        if ((block->blockSize & blockAllocatedBit) != 0) {
            if (block->nextFreeBlock == NULL) {
                /* The block is being returned to the heap - it is no longer
                allocated. */
                block->blockSize &= ~blockAllocatedBit;

                dxMutexTake (heapMutex, DXOS_WAIT_FOREVER);
                {
                    /* Add this block to the list of free blocks. */
                    freeBytesRemaining += block->blockSize;
                    _FirstFit_InsertBlockIntoFreeList (((BlockLink_t*) block));
                }
                dxMutexGive (heapMutex);
            }
        }
    }
}


size_t FirstFit_GetFreeHeapSize (void) {
    return freeBytesRemaining;
}


size_t FirstFit_GetMinimumEverFreeHeapSize (void) {
    return minimumEverFreeBytesRemaining;
}


/*---- Private Function ----------------------------------------------------*/

static void _FirstFit_HeapInit (void) {

    BlockLink_t* firstFreeBlock;
    uint8_t* alignedHeap;
    uint32_t address;
    size_t totalHeapSize = heapSize;

    /* Ensure the heap starts on a correctly aligned boundary. */
    address = (uint32_t) heap;

    if ((address & FIRSTFIT_BYTE_ALIGNMENT_MASK) != 0) {
        address += (FIRSTFIT_BYTE_ALIGNMENT - 1);
        address &= ~FIRSTFIT_BYTE_ALIGNMENT_MASK;
        totalHeapSize -= address - (uint32_t) heap;
    }

    alignedHeap = (uint8_t*) address;

    /* blockLinkStart is used to hold a pointer to the first item in the list of free
    blocks.  The void cast is used to prevent compiler warnings. */
    blockLinkStart.nextFreeBlock = (void*) alignedHeap;
    blockLinkStart.blockSize = (size_t) 0;

    /* blockLinkEnd is used to mark the end of the list of free blocks and is inserted
    at the end of the heap space. */
    address = ((uint32_t) alignedHeap) + totalHeapSize;
    address -= heapStructSize;
    address &= ~FIRSTFIT_BYTE_ALIGNMENT_MASK;
    blockLinkEnd = (void*) address;
    blockLinkEnd->blockSize = 0;
    blockLinkEnd->nextFreeBlock = NULL;

    /* To start with there is a single free block that is sized to take up the
    entire heap space, minus the space taken by blockLinkEnd. */
    firstFreeBlock = (void*) alignedHeap;
    firstFreeBlock->blockSize = address - (uint32_t) firstFreeBlock;
    firstFreeBlock->nextFreeBlock = blockLinkEnd;

    /* Only one block exists - and it covers the entire usable heap space. */
    minimumEverFreeBytesRemaining = firstFreeBlock->blockSize;
    freeBytesRemaining = firstFreeBlock->blockSize;

    /* Work out the position of the top bit in a size_t variable. */
    blockAllocatedBit = ((size_t) 1) << ((sizeof (size_t) * FIRSTFIT_BITS_PER_BYTE) - 1);
}


static void _FirstFit_InsertBlockIntoFreeList (BlockLink_t* blockToInsert) {

    BlockLink_t* iterator;
    uint8_t* p;

    /* Iterate through the list until a block is found that has a higher address
    than the block being inserted. */
    for (iterator = &blockLinkStart; iterator->nextFreeBlock < blockToInsert; iterator = iterator->nextFreeBlock) {
        /* Nothing to do here, just iterate to the right position. */
    }

    /* Do the block being inserted, and the block it is being inserted after
    make a contiguous block of memory? */
    p = (uint8_t*) iterator;
    if ((p + iterator->blockSize) == (uint8_t*) blockToInsert ) {
        iterator->blockSize += blockToInsert->blockSize;
        blockToInsert = iterator;
    }

    /* Do the block being inserted, and the block it is being inserted before
    make a contiguous block of memory? */
    p = (uint8_t*) blockToInsert;
    if ((p + blockToInsert->blockSize ) == (uint8_t*) iterator->nextFreeBlock) {
        if (iterator->nextFreeBlock != blockLinkEnd) {
            /* Form one big block from the two blocks. */
            blockToInsert->blockSize += iterator->nextFreeBlock->blockSize;
            blockToInsert->nextFreeBlock = iterator->nextFreeBlock->nextFreeBlock;
        } else {
            blockToInsert->nextFreeBlock = blockLinkEnd;
        }
    } else {
        blockToInsert->nextFreeBlock = iterator->nextFreeBlock;
    }

    /* If the block being inserted plugged a gab, so was merged with the block
    before and the block after, then it's nextFreeBlock pointer will have
    already been set, and should not be set here as that would make it point
    to itself. */
    if (iterator != blockToInsert) {
        iterator->nextFreeBlock = blockToInsert;
    }
}
