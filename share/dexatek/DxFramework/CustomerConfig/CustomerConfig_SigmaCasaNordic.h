//============================================================================
// File: CustomerConfig_SigmaCasa.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _CUSTOMER_CONFIG_H
#define _CUSTOMER_CONFIG_H

#pragma once

#include "ProjectCommonConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma message ( "!!!!! SigmaCasa - Server Configuration Selected !!!!!" )

/******************************************************
 *              Wi-Fi channel/adaptivity
 ******************************************************/
#define CONFIG_USE_WIFI_EXTENSION_API           1

/******************************************************
 *              Central/Hybrid System Config
 ******************************************************/

#define DEVICE_IS_HYBRID                        0   // Define 1 if project config is for pure central or Hybrid
#define MQTT_IS_ENABLED                         0   // Define 1 if MQTT is enabled
#define CENTRAL_OFFLINE_SUPPORT                 1
#define WIFI_FAIL_COUNT_BEFORE_REBOOT           10  // Define 0 if don't want to reboot system
#define FW_AUTO_UPDATE_UPON_AVAILABLE           1   //1 - Auto update, 0 - Manual Update via panel
#define FW_AUTO_UPDATE_AFTER_REGISTER           0   // Sec

#define MAXIMUM_DEVICE_KEEPER_ALLOWED           21

/******************************************************
 *              Server Configuration
 ******************************************************/

/* Dexatek DNS server access info and keys */
#define DK_SERVER_DNS_NAME          "sigmacasa.dexatekwebservice.net"
#define DK_SERVER_API_VERSION       "/3.6"

#define DK_QA_SERVER_DNS_NAME       "sigmacasaqaserver.dexatekwebservice.net"
#define DK_QA_SERVER_API_VERSION    "/testing" //"/SHSVR-236"

/******************************************************
 *              FW Download
 ******************************************************/
#define FW_DOWNLOAD_SIZE_PER_CHUNK                      (1024*3)

/******************************************************
 *              Dk Server Manager Config
 ******************************************************/
#define SRVM_THREAD_PRIORITY                            (DXTASK_PRIORITY_IDLE)
#define SRVM_THREAD_NAME                                "DKSM"
#define SRVM_THREAD_STACK_SIZE                          (2*1024)

/******************************************************
 *              Peripheral Manager Config
 ******************************************************/
#define DEVICE_MISSING_REPORT_TIME_MS                           (120*SECONDS)
#define DEVICE_STATUS_CHECK_INTERVAL                            (60*SECONDS)    //Don't go below 1 min. unless we are sure the reason.

/******************************************************
 *              BLE Bridge Co-Processor Config
 ******************************************************/
#define BLE_BRIDGE_UART_PORT            DXUART_PORT0
#define BLE_BRIDGE_UART_BAUD_RATE       115200
#define BLE_BRIDGE_UART_DATA_WIDTH      8
#define BLE_BRIDGE_UART_PARITY_BIT      DXUART_PARITY_NONE
#define BLE_BRIDGE_UART_FLOW_CONTROL    DXUART_FLOWCONTROL_NONE
#define BLE_BRIDGE_UART_STOP_BITS       1

/******************************************************
*              HW Support
******************************************************/
#define HW_RTC_SUPPORT                  1  //Default Support RTC is PERICOM 4337

/******************************************************
 *              Project Configuration
 ******************************************************/

#define UART_LOG_BAUD               38400

#if DEVICE_IS_HYBRID

#pragma message ( "NOTE!!!: Device is Set to HYBRID Mode, MAKE SURE to make necessary changes for the Hybrid peripheral setting" )

#include "dk_Peripheral.h"

//HYBRID Peripheral Device Settings
#define HYBRID_PERIPHERAL_PRODUCT_ID                DK_PRODUCT_TYPE_IR_REMOTE_ID
#define DK_SERVER_APP_ID                            "ODM-SIGMACASA-IRREMOTE-06c2de8d1d67"
#define DK_SERVER_API_KEY                           "67fa03338b4eb143167ee6964fde215d"
#define SECURITY_KEY_RANDOMIZE                      1  // Generate Security Key using Random Number
#define HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE        0   /* BLE device keeper list sends to HP if device amount changed */
#define HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE   0   /* BLE device PeripheralInfo sends to HP if BLE data or peripheral status changed */

#else

#pragma message ( "NOTE!!!: Device is Set to Gateway Mode Only" )

#define DK_SERVER_APP_ID                        "ODM-SIGMACASA-HUBRTK-NORDIC-713a4ffbda30b"
#define DK_SERVER_API_KEY                       "0bf7b7a4a53ab8af446325d5024aa6b1"

#endif //DEVICE_IS_HYBRID

#ifdef __cplusplus
}
#endif


#endif //_CUSTOMER_CONFIG_H
