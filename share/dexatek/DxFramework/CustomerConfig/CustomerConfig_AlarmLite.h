//============================================================================
// File: CustomerConfig_Alarm.h
//
// Author: Vance Yeh
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _CUSTOMER_CONFIG_H
#define _CUSTOMER_CONFIG_H

#pragma once

#include "ProjectCommonConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma message ( "!!!!! Alarm System - Server Configuration Selected !!!!!" )

/******************************************************
 *              Wi-Fi channel/adaptivity
 ******************************************************/
#define CONFIG_USE_WIFI_EXTENSION_API           1

/******************************************************
 *              Central/Hybrid System Config
 ******************************************************/

#define DEVICE_IS_HYBRID                        1   // Define 1 if project config is for pure central or Hybrid
#define MQTT_IS_ENABLED                         0   // Define 1 if MQTT is enabled
#define CENTRAL_OFFLINE_SUPPORT                 1
#define WIFI_FAIL_COUNT_BEFORE_REBOOT           10  // Define 0 if don't want to reboot system
#define FW_AUTO_UPDATE_UPON_AVAILABLE           0   //1 - Auto update, 0 - Manual Update via panel
#define FW_AUTO_UPDATE_AFTER_REGISTER           0   // Sec

#define MAXIMUM_DEVICE_KEEPER_ALLOWED           32

/******************************************************
 *              Server Configuration
 ******************************************************/

/* Dexatek DNS server access info and keys */
#define DK_SERVER_DNS_NAME          "sigmacasa.dexatekwebservice.net"
#define DK_SERVER_API_VERSION       "/3.6"

#define DK_QA_SERVER_DNS_NAME       "sigmacasaqaserver.dexatekwebservice.net"
#define DK_QA_SERVER_API_VERSION    "/testing" //"/SHSVR-236"

/******************************************************
 *              FW Download
 ******************************************************/
#define FW_DOWNLOAD_SIZE_PER_CHUNK                      (1024*3)

/******************************************************
 *              Dk Server Manager Config
 ******************************************************/
#define SRVM_THREAD_PRIORITY                            (DXTASK_PRIORITY_IDLE)
#define SRVM_THREAD_NAME                                "DKSM"
#define SRVM_THREAD_STACK_SIZE                          (2*1024)

/******************************************************
 *              Peripheral Manager Config
 ******************************************************/
#define DEVICE_MISSING_REPORT_TIME_MS                           (300*SECONDS)
#define DEVICE_STATUS_CHECK_INTERVAL                            (60*SECONDS)    //Don't go below 1 min. unless we are sure the reason.

/******************************************************
 *              BLE Bridge Co-Processor Config
 ******************************************************/
#define BLE_BRIDGE_UART_PORT            DXUART_PORT0
#define BLE_BRIDGE_UART_BAUD_RATE       115200
#define BLE_BRIDGE_UART_DATA_WIDTH      8
#define BLE_BRIDGE_UART_PARITY_BIT      DXUART_PARITY_NONE
#define BLE_BRIDGE_UART_FLOW_CONTROL    DXUART_FLOWCONTROL_NONE
#define BLE_BRIDGE_UART_STOP_BITS       1

/******************************************************
*              HW Support
******************************************************/
#define HW_RTC_SUPPORT                  1  //Default Support RTC is PERICOM 4337

/******************************************************
 *              Project Configuration
 ******************************************************/

#define UART_LOG_BAUD               38400

#if DEVICE_IS_HYBRID

#pragma message ( "NOTE!!!: Device is Set to HYBRID Mode, MAKE SURE to make necessary changes for the Hybrid peripheral setting" )

#include "dk_Peripheral.h"

//HYBRID Peripheral Device Settings
#define HYBRID_PERIPHERAL_PRODUCT_ID            DK_PRODUCT_TYPE_ALARM_LITE_ID
#define DK_SERVER_APP_ID                        "ODM-SIGMACASA-ALARMSYS-LITE-d03b3dd818c5b"
#define DK_SERVER_API_KEY                       "e8b68a693dd76b6d3ab477197e5bdb96"
#define HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE        1   /* BLE device keeper list sends to HP if device amount changed */
#define HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE   1   /* BLE device PeripheralInfo sends to HP if BLE data or peripheral status changed */

//#define GENERATE_SECURITY_KEY
#ifdef GENERATE_SECURITY_KEY
#define SECURITY_KEY_RANDOMIZE                      1  // Generate Security Key using Random Number
#endif

#else

#pragma message ( "NOTE!!!: Device is Set to Gateway Mode Only" )

#define DK_SERVER_APP_ID                        "ODM-SIGMACASA-HUBRTK-0ba4f32555214"
#define DK_SERVER_API_KEY                       "ce8f2bd73b478e816a49fe68979089d"

#endif //DEVICE_IS_HYBRID

#ifdef __cplusplus
}
#endif

#define DATETIME_FORMAT_EU   1	//Set to 1 if required to execute when initial condition reached

#if DATETIME_FORMAT_EU
#pragma message ( "NOTE: System DATETIME uses EU foramt: DD-MM-YYYY HH:MM" )
#else
#pragma message ( "NOTE: System DATETIME uses default foramt: MM-DD-YYYY HH:MM" )
#endif

#endif //_CUSTOMER_CONFIG_H
