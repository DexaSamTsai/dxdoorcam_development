//============================================================================
// File: DKServerManager.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.175.0
//     Date: 2015/09/07
//     1) Description: Support update motion sensor aggregation data
//        Added:
//            - A static variable UTCLocalTimeTable
//            - _IsUTCTimeReady()
//            - DKServerManager_GetUTCLocalTimeTable()
//        Modified:
//            - _GetReponseJSONPtr()
//            - DKServerManager_UpdatePeripheralAggregationData_Asynch()
//
//     Version: 0.176
//     Date: 2015/09/10
//     1) Description: Support AddSmartHomeCentralLog API
//        Added:
//            - DKServerManager_AddSmartHomeCentralLog_Asynch()
//
//     Version: 0.177
//     Date: 2015/11/23
//     1) Description: Change the Get Job Interval dynamically according to needs.
//        Added:
//            - DKServerManager_ConfigGetJobInterval()
//        Modified:
//            - IsTimeToEnterLongPoll()
//
//     Version: 0.178
//     Date: 2016/01/11
//     1) Description: Support AddStandalonePeripheral API
//        Added:
//            - DKServerManager_AddStandalonePeripheral_Asynch()
//
//     Version: 0.178.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.178.2
//     Date: 2016/02/18
//     1) Description: Remove unused function
//        Removed:
//            - DKServerManager_MD5Start()
//            - DKServerManager_MD5Update()
//            - DKServerManager_MD5Finish()
//            - DKServerManager_MD5Equal()
//            - DKServerManager_MD5Clear()
//
//     Version: 0.178.3
//     Date: 2016/02/24
//     1) Description: Revise and enhance DKServerManager state
//        Modified:
//            - DKSMThreadParam_t
//            - _BuildAndSendCommon()
//            - _BuildAndSendLongPoll()
//            - HTTPThreadEventReportHandler()
//            - DKServerThread_Loop()
//            - DKServerManager_State_Suspend()
//            - DKServerManager_State_Resume()
//        Added:
//            - _BuildAndSendRenewSession()
//            - DKServerManager_State()
//
//     Version: 0.178.4
//     Date: 2016/02/24
//     1) Description: Move structure from DKServerManager.h
//        Moved:
//            - DKServerEventReportObj_t
//
//     Version: 0.178.5
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - _BuildAndSendRenewSession()
//            - _PushGatewayLoginToQ()
//            - DKServerManager_GatewayLogin_Asynch()
//            - DKServerManager_AddGateway_Asynch()
//            - DKServerManager_ChangeGatewayStatus_Asynch()
//            - DKServerManager_UpdateGatewayFirmwareVersion_Asynch()
//            - DKServerManager_GetPeripheralByMAC_Asynch()
//            - DKServerManager_AddPeripheral_Asynch()
//            - DKServerManager_AddStandalonePeripheral_Asynch()
//            - DKServerManager_UpdatePeripheralFirmware_Asynch()
//            - DKServerManager_SetPeripheralQueryCollection_Asynch()
//            - DKServerManager_ClearPeripheralSecurityKey_Asynch()
//            - DKServerManager_SetPeripheralSecurityKey_Asynch()
//            - DKServerManager_UpdatePeripheralDataPayload_Asynch()
//            - DKServerManager_UpdatePeripheralStatusInfo_Asynch()
//            - DKServerManager_UpdatePeripheralDataPayloadToHistory_Asynch()
//            - DKServerManager_UpdatePeripheralAggregationData_Asynch()
//            - _BuildAndSendLongPoll()
//            - DKServerManager_SendEventToWorkerThread()
//            - DKServerManager_GetJobs_Asynch()
//            - DKServerManager_JobDoneReport_Asynch()
//            - DKServerManager_UpdateJobStatusExt_Asynch()
//            - DKServerManager_UpdateAdvJobStatus_Asynch()
//            - DKServerManager_GetAdvJobCondition_Asynch()
//            - DKServerManager_GetScheduleJob_Asynch()
//            - DKServerManager_DeleteScheduleJob_Asynch()
//            - DKServerManager_RenewSessionToken_Asynch()
//            - DKServerManager_SendNotificationMessage_Asynch()
//            - DKServerManager_SendMessageWithKey_Asynch()
//            - DKServerManager_GetLastFirmareInfo_Asynch()
//            - DKServerManager_FirmwareDownload_Asynch()
//            - HTTPThreadEventReportHandler()
//            - DKServerManager_GetServerInfo_Asynch()
//            - DKServerManager_GetServerTime_Asynch()
//            - DKServerThread_Loop()
//            - IsTimeToEnterLongPoll()
//            - _BuildAndSendCommon()
//            - DKServerManager_AddSmartHomeCentralLog_Asynch()
//            - _HandleHostRedirect()
//            - DKServerManager_GetEventObjHeader()
//            - DKServerManager_HostRedirect_Asynch()
//            - DKServerManager_State_Suspend()
//            - DKServerManager_State_Resume()
//            - _DKSMPushMessageToQueue()
//            - DKServerManager_Init()
//
//     Version: 0.178.6
//     Date: 2016/03/07
//     1) Description: Send DKSERVER_SYSTEM_ERROR_DNS_LOOKUP_FAILED if DNS lookup failed
//        Modified:
//            - _HandleHostRedirect()
//            - DKServerThread_Loop()
//     2) Description: Support disable Get Job request
//        Modified:
//            - DKServerManager_ConfigGetJobInterval()
//            - IsTimeToEnterLongPoll()
//
//     Version: 0.178.7
//     Date: 2016/03/14
//     1) Description: Remove "NOTE: Porting to dxOS" comment
//     2) Description: Remove Long Poll support
//        Modified:
//            - Changed IsTimeToEnterLongPoll() to IsTimeToGetJob()
//            - DKSMThreadParam_t
//            - _BuildAndSendCommon()
//            - Changed _BuildAndSendLongPoll() to _BuildAndSendGetJob()
//            - HTTPThreadEventReportHandler()
//            - DKServerThread_Loop()
//            - DKServerManager_SendEventToWorkerThread()
//        Removed:
//            - DKServerManager_SetLongPollFlag()
//     3) Description: Fixed issue while sending multiple request
//        Modified:
//            - DKServerThread_Loop()
//
//     Version: 0.178.8
//     Date: 2016/04/01
//     1) Description: Unblock a thread while HTTPS is offline
//        Modified:
//            - DKServerThread_Loop()
//
//     Version: 0.178.9
//     Date: 2016/04/06
//     1) Description: Fixed issue with state changing notification
//        Modified:
//            - DKServerManager_State()
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.178.10
//     Date: 2016/04/07
//     1) Description: Fixed network recovery issue
//        Modified:
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.178.11
//     Date: 2016/05/04
//     1) Description: Clear SessionToken if receiving INVALID_SESSION_TOKEN
//        Modified:
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.178.12
//     Date: 2016/05/06
//     1) Description: Show more information while downloading firmware
//        Modified:
//            - HTTPThreadEventReportHandler
//
//     Version: 0.178.13
//     Date: 2016/07/04
//     1) Description: Fixed memory leak issue
//        Modified:
//            - _PushGatewayLoginToQ()
//
//     Version: 0.178.14
//     Date: 2016/09/06
//     1) Description: To support MQTT, report UserObjectID and GatewayID when server response success
//        Modified:
//            - DKServerManager_SendEventToWorkerThread()
//
//     Version: 0.224
//     Date: 2016/09/07
//     1) Description: Revise JobDoneReport API to support "ReportedData"
//        Modified:
//            - DKServerManager_JobDoneReport_Asynch
//
//     Version: 0.228
//     Date: 2016/10/03
//     1) Description: Support Container
//        Modifed:
//            - DKServerManager_SendEventToWorkerThread()
//        Added:
//            - DKServerManager_GetDataContainerByID_Asynch()
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Revise dxContainerM_t structure
//        Modified:
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//
//     Version: 0.235
//     Date: 2016/11/01
//     1) Description: Added return value
//        Modified:
//            _BuildAndSendCommon()
//            DKServerThread_Loop()
//
//     Version: 0.235.1
//     Date: 2016/11/09
//     1) Description: Fixed issue SHFW-44 (DKServerManager report DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL while preparing request JSON message)
//        Modified:
//            - Change value of THREAD_BUFFER_SIZE to 4096
//            - Force DKThreadParam to put at SDRAM
//
//     Version: 0.235.2
//     Date: 2016/11/10
//     1) Description: Fixed issue SHFW-45 (Memory leak when using Container Object)
//        Modified:
//            - DKServerManager_GetDataContainerByID_Asynch()
//            - _BuildAndSendCommon()
//        Added:
//            - _ContainerData_Free()
//
//     Version: 0.235.3
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Added:
//            - DKServerManager_GetPeripheralNameByIndex()
//
//     Version: 0.235.4
//     Date: 2017/03/02
//     1) Description: Change peripheral's DeviceName
//        Added:
//            - DKServerManager_ChangePeripheralDeviceName()
//
//     Version: 0.235.5
//     Date: 2017/03/27
//     1) Description: Revise Error Code
//        Modified:
//            - DKServerManager_FirmwareVersionCompare()
//
//     Version: 0.235.6
//     Date: 2017/03/27
//     1) Description: Revise Error Code
//        Added:
//            - _HandleCOMMONErrorCode()
//        Modified:
//            - _BuildAndSendCommon()
//            - _HandleHostRedirect()
//            - _DKSMPushMessageToQueue()
//            - DKServerManager_State()
//            - HTTPThreadEventReportHandler()
//            - DKServerThread_Loop()
//            - DKServerManager_Init()
//            - DKServerManager_State_Suspend()
//            - DKServerManager_State_Resume()
//            - DKServerManager_GatewayLogin_Asynch()
//            - DKServerManager_GetPeripheralByMAC_Asynch()
//            - DKServerManager_UpdatePeripheralDataPayload_Asynch()
//            - DKServerManager_UpdatePeripheralStatusInfo_Asynch()
//            - DKServerManager_GetJobs_Asynch()
//            - DKServerManager_AddGateway_Asynch()
//            - DKServerManager_AddPeripheral_Asynch()
//            - DKServerManager_SendNotificationMessage_Asynch()
//            - DKServerManager_RenewSessionToken_Asynch()
//            - DKServerManager_JobDoneReport_Asynch()
//            - DKServerManager_UpdateJobStatusExt_Asynch()
//            - DKServerManager_GetServerInfo_Asynch()
//            - DKServerManager_UpdateGatewayFirmwareVersion_Asynch()
//            - DKServerManager_ChangeGatewayStatus_Asynch()
//            - DKServerManager_UpdatePeripheralFirmware_Asynch()
//            - DKServerManager_SetPeripheralQueryCollection_Asynch()
//            - DKServerManager_GetServerTime_Asynch()
//            - DKServerManager_UpdatePeripheralDataPayloadToHistory_Asynch()
//            - DKServerManager_SendMessageWithKey_Asynch()
//            - DKServerManager_GetLastFirmareInfo_Asynch()
//            - DKServerManager_FirmwareDownload_Asynch()
//            - DKServerManager_GetScheduleJob_Asynch()
//            - DKServerManager_DeleteScheduleJob_Asynch()
//            - DKServerManager_ClearPeripheralSecurityKey_Asynch()
//            - DKServerManager_SetPeripheralSecurityKey_Asynch()
//            - DKServerManager_UpdatePeripheralAggregationData_Asynch()
//            - DKServerManager_UpdateAdvJobStatus_Asynch()
//            - DKServerManager_GetAdvJobCondition_Asynch()
//            - DKServerManager_HostRedirect_Asynch()
//            - DKServerManager_AddSmartHomeCentralLog_Asynch()
//            - DKServerManager_AddStandalonePeripheral_Asynch()
//            - DKServerManager_GetDataContainerByID_Asynch()
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//            - DKServerManager_GetPeripheralNameByIndex()
//            - DKServerManager_GetPeripheralSecurityKeyByIndex()
//            - DKServerManager_FirmwareVersionCompare()
//            - DKServerManager_GetGatewayFirmwareVersion()
//            - DKServerManager_GetPeripheralFirmwareVersionByMAC()
//
//     Version: 0.235.7
//     Date: 2017/03/31
//     1) Description: Fixed ServerManager always stay in SUSPENDING after network "net_connect(): -68" error occurred
//        Modified:
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.235.8
//     Date: 2017/04/06
//     1) Description: Implement a function to forace State Machine to SUSPENDED
//        Modified:
//            - DKServerManager_State()
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.242
//     Date: 2017/04/19
//     1) Description: Support GetCurrentUser API
//        Added:
//            - New function DKServerManager_GetCurrentUser_Asynch()
//        Modified:
//            - DKServerManager_SendEventToWorkerThread()
//
//     Version: 0.242.1
//     Date: 2017/04/19
//     1) Description: Fixed issue with HTTPManager stop working.
//        Modified:
//            - HTTPThreadEventReportHandler()
//            - DKServerManager_State_Resume()
//            - DKServerThread_Loop()
//
//     Version: 0.242.2
//     Date: 2017/05/08
//     1) Description: Support DeleteGateway API
//        Added:
//            - Support new function DKServerManager_DeleteGateway_Asynch().
//              After deleting, DKMSG_DELETEGATEWAY message will be notified without PAYLOAD
//
//     Version: 0.242.3
//     Date: 2017/06/29
//     1) Description: Increase HTTP timeout to 30 seconds
//        Modified:
//            - DKTHREAD_HTTP_TIMEOUT_TIME, TCP_CONNECT_TIMEOUT
//            - _BuildAndSendCommon()
//            - _BuildAndSendRenewSession()
//        Delete:
//            - DKTHREAD_HTTP_DEFAULT_TIMEOUT_TIME
//
//     Version: 0.242.4
//     Date: 2017/06/29
//     1) Description: Added debug log information when fatal issue occurred
//
//     Version: 0.242.5
//     Date: 2017/08/10
//     1) Description: Porting to Embedded Linux.
//                     Revise to use dxQueueReceive() instead of dxQueuePeek(),
//                     due to we will not support this API in Linux.
//        Modified:
//            - DKServerThread_Loop()
//
//     Version: 0.242.6
//     Date: 2017/08/15
//     1) Description: Support IPCam API
//        Added:
//            - New structure IPCamEventFile_t
//            - _IsValidMAC()
//            - DKServerManager_GetPushRTMPLiveURL_Asynch()
//            - DKServerManager_GetEventMediaFileUploadPath_Asynch()
//        Modified:
//            - DKServerManager_SendEventToWorkerThread()
//
//     Version: 0.253.0
//     Date: 2017/08/30
//     1) Description: Support "TimeStamp" in UpdateAdvJobStatus API
//        Modified:
//            - DKServerManager_UpdateAdvJobStatus_Asynch()
//     2) Description: Revise IPCam API
//        Modified:
//            - DKServerManager_GetEventMediaFileUploadPath_Asynch()
//        Added:
//            - DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData()
//            - DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData()
//            - DKServerManager_GetEventMediaFileUploadPath_MessageData_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_MessageData_GetData()
//
//     Version: 0.255
//     Date: 2017/09/08
//     1) Description: Revise SendMessageWithKey API
//        Modified:
//            - DKServerManager_SendMessageWithKey_Asynch()
//
//     Version: 0.255.1
//     Date: 2017/09/11
//     1) Description: Support reading Central's
//            - DKServerManager_GetCentralName()
//
//     Version: 0.255.2
//     Date: 2017/09/12
//     1) Description: Support a new API, SendMessageWithKeyNum and keep SendMessageWithKey for backward compatible.
//        Modified:
//            - DKServerManager_SendMessageWithKey_Asynch()
//            - DKServerManager_SendMessageWithKeyNum_Asynch()
//
//     Version: 0.255.2
//     Date: 2017/09/25
//     1) Description: Fixed "Segmentation fault" issue on embeddex Linux
//        Modified:
//            - HTTPThreadEventReportHandler()
//
//     Version: 0.255.3
//     Date: 2017/10/26
//     1) Description: Free memory if calling dxWorkerTaskSendAsynchEvent() failed
//        Modified:
//            - DKServerManager_SendEventToWorkerThread()
//
//     Version: 0.255.4
//     Date: 2017/12/19
//     1) Description: Add new functions for Job Polling control
//        Modified:
//            - DKServerManager_IsJobPollingEnabled()
//            - DKServerManager_SetJobPolling()
//============================================================================
#include <stdlib.h>
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#include "DKMSG_Common.h"
#include "DKMSG_Handler.h"
#include "HTTPManager.h"
#include "DKServerManagerConfig.h"
#include "Utils.h"
#include "dk_http.h"


/******************************************************
 *                    Constants
 ******************************************************/
#define DKSM_USE_WORKAROUND                         1

#define CHECKDKSERVER_MAX_RETRY                     3

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagEventReportObj           // Message for RTOS Queue MUST be 4 bytes aligned and LESS than 64 bytes
{
    DKEventHeader_t  Header;
    uint8_t          *pData;
    uint8_t          Reserved[4];           // Alignment byte
} DKServerEventReportObj_t;                 // sizeof(DKServerEventReportObj_t): 32

typedef struct _tagDKSMThreadParam
{
    dxTaskHandle_t                      Handle;
    dxQueueHandle_t                     Queue;
    HTWorker_t                          Worker;
    uint32_t                            TimeoutInSec;
    uint8_t                             szBuffer[SRVM_THREAD_BUFFER_SIZE];

    dxMutexHandle_t                     mutex;
    uint32_t                            GetJobInterval;
    dxBOOL                              JobPolling;

    dxTime_t                            LastRequestTime;
    dxTime_t                            StartRecvTime;
    DKMSG_Command_t                     APIID;
} DKSMThreadParam_t;

typedef struct _tagDKOptionHTTPHeader
{
    DKTime_t            DKTime;
    DKContentRange_t    ContentRange;   // Use for DKMSG_FIRMWAREDOWNLOAD
} DKOptionHTTPHeader_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern dxOS_RET_CODE DKServerManager_SendEventToWorkerThread(uint16_t src, uint64_t taskid, DKMSG_Command_t msg, DKJobType_t type, uint16_t httpcode, uint16_t dkcode, void *pData, int DataLen);
extern dxOS_RET_CODE DKServerManager_SendEventIDToWorkerThread(uint16_t src, uint64_t taskid, DKMSG_Command_t msg, uint16_t httpcode, uint16_t dkcode);

uint8_t _IsUTCTimeReady(void);
uint8_t *_GetReponseJSONPtr(uint8_t *pData, DKOptionHTTPHeader_t *pOptionHeader);
void _SendEventDKTimeUpdate(DKOptionHTTPHeader_t *pOptionHeader);
dxBOOL _BuildAndSendCommon(DKSMThreadParam_t *pThreadParam);
void _BuildAndSendGetJob(DKSMThreadParam_t *pThreadParam);
void _HandleHostRedirect(DKRESTCommMsg_t *pCommand);
DKServer_StatusCode_t _DKSMPushMessageToQueue(DKRESTCommMsg_t **msg);
void _CleanDKRESTCommandQueueObj(DKRESTCommMsg_t *obj);
int IsTimeToGetJob(void);

static void HTTPThreadEventReportHandler(void *arg);
static TASK_RET_TYPE DKServerThread_Loop(TASK_PARAM_TYPE arg);
static DKServerManager_StateMachine_State_t DKSManagerState = DKSERVERMANAGER_STATEMACHINE_STATE_NONE;

/******************************************************
 *               Variable Definitions
 ******************************************************/
#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes = @ ".sdram.text"
#endif // CONFIG_SDRAM_BUFFER_ALLOC
static DKSMThreadParam_t DKThreadParam =
{
    NULL,                                   // Handle
    NULL,                                   // Queue
    {                                       // HTWorker_t
        NULL,                               //      Handle
        NULL                                //      RegisterFunc
    },
    3,                                      // TimeoutInSec
    { 0x00 },                               // szBuffer
    NULL,                                   // mutex
    SRVM_THREAD_REQUEST_JOB_TIME_INTERVAL_DEFAULT,
    dxTRUE,                                 // JobPolling
    0,                                      // LastRequestTime
    0,                                      // StartRecvTime
    DKMSG_UNKNOWN                           // APIID
};
#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes =
#endif // CONFIG_SDRAM_BUFFER_ALLOC

static UTCLocalTimeTable_t  UTCLocalTimeTable = {0, 0};     // For UpdatePeripheralAggregationData


/******************************************************
 *               Function Implementation - Local
 ******************************************************/
dxBOOL _IsValidMAC(uint8_t *macaddr, int macaddrlen)
{
    if (macaddr == NULL ||
        macaddrlen != 8)
    {
        return dxFALSE;
    }

    if (macaddr[0] == 0 &&
        macaddr[1] == 0 &&
        macaddr[2] == 0 &&
        macaddr[5] == 0 &&
        macaddr[6] == 0 &&
        macaddr[7] == 0)
    {
        return dxFALSE;
    }

    return dxTRUE;
}
// 1 if Gateway received Server's UTC time. otherwise, 0
uint8_t _IsUTCTimeReady(void)
{
    if (UTCLocalTimeTable.UTCTime > 0 &&
        UTCLocalTimeTable.LocalTime > 0)
    {
        return 1;
    }

    return 0;
}

uint8_t *_GetReponseJSONPtr(uint8_t *pData, DKOptionHTTPHeader_t *pOptionHeader)
{
    DKHTTPResponse DKHttpResponse = {0, 0, 0, 0, 0};
    int nErrorCode = 0;
    uint8_t *p = NULL;

    if (pData == NULL) return NULL;

    p = (uint8_t *)DK_GetHTTPResponseMessageBody((char *)pData, &DKHttpResponse, &nErrorCode);

    if (nErrorCode != 0)
    {
        DK_FreeHTTPResponse(&DKHttpResponse);
        return NULL;
    }

    if (DKHttpResponse.nStatusCode != HTTP_OK)
    {
        DK_FreeHTTPResponse(&DKHttpResponse);
        return NULL;
    }

    if (pOptionHeader)
    {
        if (DKMSG_ConvertToDKTime((uint8_t *)DKHttpResponse.szDate, &pOptionHeader->DKTime) == 1)
        {
            // Success
            dxTime_t lt = dxTimeGetMS();
            {
                UTCLocalTimeTable.LocalTime = lt / 1000;
                UTCLocalTimeTable.UTCTime = ConvertDKTimeToUTC(pOptionHeader->DKTime);

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "Synchronising System Tick: %u and Server(UTC + 0): %u\n", (unsigned int)UTCLocalTimeTable.LocalTime, (unsigned int)UTCLocalTimeTable.UTCTime);
            }
        }

        if (DKHttpResponse.nFileSize > 0)
        {
            pOptionHeader->ContentRange.nFirstBytePosition = DKHttpResponse.nFirstBytePosition;
            pOptionHeader->ContentRange.nLastBytePosition = DKHttpResponse.nLastBytePosition;
            pOptionHeader->ContentRange.nFileSize = DKHttpResponse.nFileSize;
        }
    }

    DK_FreeHTTPResponse(&DKHttpResponse);

    return p;
}

void _SendEventDKTimeUpdate(DKOptionHTTPHeader_t *pOptionHeader)
{
    if (pOptionHeader->DKTime.year >= 1970 &&
        pOptionHeader->DKTime.month > 0 &&
        pOptionHeader->DKTime.day > 0 &&
        pOptionHeader->DKTime.hour >= 0 &&
        pOptionHeader->DKTime.min >= 0 &&
        pOptionHeader->DKTime.sec >= 0 &&
        pOptionHeader->DKTime.dayofweek > 0)
    {
        DKServerManager_SendEventToWorkerThread(0,
                                                0,
                                                DKMSG_DATETIME_UPDATE,
                                                DKJOBTYPE_NONE,
                                                HTTP_OK,
                                                DKSERVER_STATUS_SUCCESS,
                                                (void *)&pOptionHeader->DKTime,
                                                sizeof(pOptionHeader->DKTime));

    }
}

void _ContainerData_Free(DKRESTCommMsg_t *pCommand)
{
    if (pCommand && pCommand->pData)
    {
        uint8_t  msg = pCommand->Header.MessageType;
        dxContainer_t   *pCont = NULL;

        // The _CleanDKRESTCommandQueueObj() will free pData itself only.
        // In following case, we store an address of container object here.
        // The object will keep in memory until we call dxContainer_Free()
        switch (msg)
        {
        case DKMSG_GETDATACONTAINERBYID:
        /*
         *     Header.DataLen           = 8 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         *                                | format          | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_CREATECONTAINERMDATA:
        /*
         *     Header.DataLen           = 4 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_UPDATECONTAINERMDATA:
        /*
         *     Header.DataLen           = 4 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_INSERTCONTAINERDDATA:
        /*
         *     Header.DataLen           = 4 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_UPDATECONTAINERDDATA:
        /*
         *     Header.DataLen           = 10 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         *                                | updateTo        | 2    |
         *                                +-----------------+------+
         *                                | advObjID        | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_DELETECONTAINERDDATA:
        /*
         *     Header.DataLen           = 10 Bytes
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         */
        case DKMSG_DELETECONTAINERMDATA:
        /*
         *     pData                    = +-----------------+------+
         *                                | dxContainer_t * | 4    |
         *                                +-----------------+------+
         */
            //Copy the address
            memcpy(&pCont, (dxContainer_t *)pCommand->pData, sizeof(pCont));
            break;
        }

        dxContainer_Free(pCont);
    }
}

void _HandleCOMMONErrorCode(int16_t nErrorCode)
{
    DKServer_SystemErrorCode_t ret = DKSERVER_SYSTEM_ERROR_SUCCESS;

    switch (nErrorCode)
    {
    case DXOS_SUCCESS:
        ret = DKSERVER_SYSTEM_ERROR_SUCCESS;
        break;
    case DXOS_ERROR:
        ret = DKSERVER_SYSTEM_ERROR_DX_RTOS;
        break;
    case DXOS_TIMEOUT:
        ret = DKSERVER_SYSTEM_ERROR_NETWORK_TIMEOUT;
        break;
    case DXOS_NO_MEM:
        ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
        break;
    case DXOS_LIMIT_REACHED:
        ret = DKSERVER_STATUS_ERROR_QUEUE_IS_FULL;
        break;
    default:
        ret = DKSERVER_SYSTEM_ERROR_UNDEFINED;
    }

    if (ret != DKSERVER_SYSTEM_ERROR_SUCCESS)
    {
        DKServerManager_SendEventIDToWorkerThread(0,
                                                  0,
                                                  DKMSG_SYSTEM_ERROR,
                                                  0,
                                                  ret);
    }
}

dxBOOL _BuildAndSendCommon(DKSMThreadParam_t *pThreadParam)
{
    DKRESTCommMsg_t *pCommand = NULL;
    int ret = DKSERVER_STATUS_SUCCESS;
    int len = 0;
    uint8_t *pBuffer = NULL;
    int cbSize = 0;
    uint32_t timeout = SRVM_THREAD_HTTP_TIMEOUT_TIME;

    if (pThreadParam)
    {
        pCommand = (DKRESTCommMsg_t *)DKQ_PeekMessage();

        if (pCommand)
        {
            if (DKQ_PeekMessageType() == DKMSG_HOST_REDIRECT)
            {
                _HandleHostRedirect(pCommand);

                return dxTRUE;
            }

            pBuffer = pThreadParam->szBuffer;
            cbSize = sizeof(pThreadParam->szBuffer);
            len = sizeof(DKRESTCommMsg_t) + pCommand->Header.DataLen;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, Creating Message: %d\n",
                             __FUNCTION__,
                             __LINE__,
                             pCommand->Header.MessageType);

            ret = DKMSG_CreateRequestMessage(pBuffer, cbSize, pCommand, len);

            // We need to free the memory that container object occupied
            _ContainerData_Free(pCommand);

            if (ret == DKSERVER_STATUS_SUCCESS)
            {
                cbSize = strlen((char *)pBuffer);
                ret = HTTPManager_Send_Asynch(pBuffer, cbSize, timeout);
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, HTTPManager_Send_Asynch(): %d, Length: %d\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 (unsigned int)ret, cbSize);

                _HandleCOMMONErrorCode(ret);

                return (ret == DXOS_SUCCESS) ? dxTRUE : dxFALSE;
            }
            else
            {
            }
        }
    }

    return dxFALSE;
}

void _BuildAndSendGetJob(DKSMThreadParam_t *pThreadParam)
{
    int ret = DKSERVER_STATUS_SUCCESS;
    int len = 0;
    uint8_t *pBuffer = NULL;
    int cbSize = 0;
    DKRESTCommMsg_t *pCommand = NULL;

    if (pThreadParam)
    {
        pCommand = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

        if (pCommand)
        {
            // Send DKMSG_NEWJOBS
            pCommand->Header.MessageType = DKMSG_NEWJOBS;

            pBuffer = pThreadParam->szBuffer;
            cbSize = sizeof(pThreadParam->szBuffer);
            len = sizeof(DKRESTCommMsg_t);

            ret = DKMSG_CreateRequestMessage(pBuffer, cbSize, pCommand, len);

            if (ret == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, DKQ_IsEmpty(): %d\n", __FUNCTION__, __LINE__, DKQ_IsEmpty());

                LOCK(&pThreadParam->mutex);

                if (DKQ_IsEmpty() == 0)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, DKQ_Empty(), MType: %d\n", __FUNCTION__, __LINE__, DKQ_PeekMessageType());
                    DKQ_Empty();
                }

                DKQ_Push(pCommand);
                UNLOCK(&pThreadParam->mutex);

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, DKQ_IsEmpty(): %d\n", __FUNCTION__, __LINE__, DKQ_IsEmpty());

                cbSize = strlen((char *)pBuffer);
                ret = HTTPManager_Send_Asynch(pBuffer, cbSize, SRVM_THREAD_HTTP_TIMEOUT_TIME);
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, HTTPManager_Send_Asynch(): %d, Length: %d\n",
                                 __FUNCTION__,
                                 __LINE__, (unsigned int)ret,
                                 cbSize);

                if (ret != DXOS_SUCCESS)
                {
                    // If fail, remove the command from DKQ
                    LOCK(&pThreadParam->mutex);
                    DKQ_Pop(NULL);
                    UNLOCK(&pThreadParam->mutex);
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, DKMSG_CreateRequestMessage()=%d\n", __FUNCTION__, __LINE__, ret);

            }

            _CleanDKRESTCommandQueueObj(pCommand);
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                             __FUNCTION__,
                             __LINE__,
                             (int)sizeof(DKRESTCommMsg_t));
        }
    }
}

void _BuildAndSendRenewSession(DKSMThreadParam_t *pThreadParam)
{
    int ret = DKSERVER_STATUS_SUCCESS;
    int len = 0;
    uint8_t *pBuffer = NULL;
    int cbSize = 0;
    DKRESTCommMsg_t *pCommand = NULL;

    if (pThreadParam)
    {
        pCommand = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

        if (pCommand)
        {
            // Send DKMSG_RENEWSESSIONTOKEN
            pCommand->Header.MessageType = DKMSG_RENEWSESSIONTOKEN;

            pBuffer = pThreadParam->szBuffer;
            cbSize = sizeof(pThreadParam->szBuffer);
            len = sizeof(DKRESTCommMsg_t);

            ret = DKMSG_CreateRequestMessage(pBuffer, cbSize, pCommand, len);

            if (ret == DKSERVER_STATUS_SUCCESS)
            {
                cbSize = strlen((char *)pBuffer);
                ret = HTTPManager_Send_Asynch(pBuffer, cbSize, SRVM_THREAD_HTTP_TIMEOUT_TIME);
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, HTTPManager_Send_Asynch(): %d, Length: %d\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 (unsigned int)ret,
                                 cbSize);

                if (ret == DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, DKQ_IsEmpty(): %d\n", __FUNCTION__, __LINE__, DKQ_IsEmpty());

                    LOCK(&pThreadParam->mutex);

                    if (DKQ_IsEmpty() == 0)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d, DKQ_Empty(), MType: %d\n", __FUNCTION__, __LINE__, DKQ_PeekMessageType());
                        DKQ_Empty();
                    }

                    DKQ_Push(pCommand);
                    UNLOCK(&pThreadParam->mutex);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, DKQ_IsEmpty(): %d\n", __FUNCTION__, __LINE__, DKQ_IsEmpty());
                }
            }

            _CleanDKRESTCommandQueueObj(pCommand);
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                             __FUNCTION__,
                             __LINE__,
                             (int)sizeof(DKRESTCommMsg_t));
        }
    }
}

void _HandleHostRedirect(DKRESTCommMsg_t *pCommand)
{
    uint16_t ret = 0;
    uint8_t *pHostName = NULL;
    uint8_t *pAPIVer = NULL;

    if (pCommand && pCommand->Header.MessageType == DKMSG_HOST_REDIRECT)
    {
        pHostName = pCommand->pData;
        pAPIVer = &pCommand->pData[strlen((char *)pHostName) + 1];

        if (pAPIVer)
        {
            ret = HTTPManager_Suspend_Asynch();

            _HandleCOMMONErrorCode(ret);

            ObjectDictionary_SetHostname ((char *)pHostName);
            ObjectDictionary_SetApiVersion((char *) pAPIVer);

            // Query new ip address of pHostName
            if (DK_DNS_Lookup() != DXOS_SUCCESS)
            {
                DKServerManager_SendEventIDToWorkerThread(0,
                                                          0,
                                                          DKMSG_SYSTEM_ERROR,
                                                          0,
                                                          DKSERVER_SYSTEM_ERROR_DNS_LOOKUP_FAILED);
            }

            dxIP_Addr_t *ipaddr = ObjectDictionary_GetHostIp ();

            ret = HTTPManager_Resume_Asynch(ipaddr, DKSM_USE_PORT, TCP_CONNECT_TIMEOUT);

            _HandleCOMMONErrorCode(ret);
        }
    }
}

DKServer_StatusCode_t _DKSMPushMessageToQueue(DKRESTCommMsg_t **msg)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    if (pThreadParam->Queue)
    {
        if (dxQueueIsFull(pThreadParam->Queue) == dxTRUE)
        {
            dxMemFree((*msg)->pData);
            (*msg)->pData = NULL;
            dxMemFree((*msg));
            (*msg) = NULL;
            result = DKSERVER_STATUS_ERROR_QUEUE_IS_FULL;
        }
        else
        {
            if (dxQueueSend(pThreadParam->Queue, msg, 1000) != DXOS_SUCCESS)
            {
                dxMemFree((*msg)->pData);
                (*msg)->pData = NULL;
                dxMemFree((*msg));
                (*msg) = NULL;

                result = DKSERVER_STATUS_ERROR_DX_RTOS;
            }
        }
    }

    return result;
}

void _CleanDKRESTCommandQueueObj(DKRESTCommMsg_t *obj)
{
    if (obj)
    {
        if (obj->pData)
        {
            dxMemFree(obj->pData);
            obj->pData = NULL;
        }

        dxMemFree(obj);
        obj = NULL;                 // CANNOT use memset(obj, 0x00, sizeof(DKRESTCommMsg_t));
    }
}

DKRESTCommMsg_t *_DuplicateDKRESTCommandQueueObj(DKRESTCommMsg_t *pSrcObj)
{
    DKRESTCommMsg_t *pDstObj = NULL;
    dxBOOL ret = dxFALSE;

    if (pSrcObj)
    {
        pDstObj = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));
        if (pDstObj)
        {
            memcpy(pDstObj, pSrcObj, sizeof(pSrcObj->Header));
            if (pSrcObj->Header.DataLen > 0 && pSrcObj->pData)
            {
                pDstObj->pData = dxMemAlloc(SRVM_THREAD_NAME, 1, pSrcObj->Header.DataLen);
                if (pDstObj->pData)
                {
                    memcpy(pDstObj->pData, pSrcObj->pData, pSrcObj->Header.DataLen);
                }
            }
            ret = dxTRUE;
        }
    }

    if (ret == dxFALSE)
    {
        _CleanDKRESTCommandQueueObj(pDstObj);
        pDstObj = NULL;
    }

    return pDstObj;
}

int IsTimeToGetJob(void)
{
    int ret = 0;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    dxTime_t CurrentTime = 0;
    dxTime_t LastRequestTime = 0;
    uint32_t GetJobInterval = 0;
    dxBOOL JobPolling = dxTRUE;

    ObjectDictionary_Lock ();
    char *pSessionToken = ObjectDictionary_GetSessionToken ();
    DK_BLEGateway_t* pGateway = ObjectDictionary_GetGateway ();
    ObjectDictionary_Unlock ();


    if (*pSessionToken == 0x00)
    {
        return ret;
    }

    if (pGateway == NULL) {
        return ret;
    }

    if (pThreadParam == NULL)
    {
        return ret;
    }

    LOCK(&pThreadParam->mutex);

    GetJobInterval = pThreadParam->GetJobInterval;
    JobPolling = pThreadParam->JobPolling;

    if (pThreadParam->LastRequestTime == 0)
    {
        pThreadParam->LastRequestTime = dxTimeGetMS();
    }

    LastRequestTime = pThreadParam->LastRequestTime;
    UNLOCK(&pThreadParam->mutex);

    if (JobPolling) {
        CurrentTime = dxTimeGetMS();                                                    // Get system time in milliseconds
        if ((CurrentTime - LastRequestTime) >= GetJobInterval &&
            GetJobInterval > 0)                                                         // Thread is idle for a few seconds
        {
            if (pSessionToken && strlen(pSessionToken) > 0)
            {
                pThreadParam->LastRequestTime = CurrentTime;
                ret = 1;
            }
        }
    }

    return ret;
}

/******************************************************
 *               Function Implementation - Static
 ******************************************************/
 static DKServerManager_StateMachine_State_t DKServerManager_State(DKServerManager_StateMachine_Command_t command)
{
    DKServerManager_StateMachine_State_t state = DKSERVERMANAGER_STATEMACHINE_STATE_NONE;
    //dxOS_RET_CODE result = DXOS_SUCCESS;
    //static int retry_count = 0;

    //if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER)
    //{
    //    retry_count = 0;
    //}

    switch (DKSManagerState)
    {
    case DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDING:
        if (command == DKSERVERMANAGER_COMMAND_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
        }
        else if (command == DKSERVERMANAGER_COMMAND_FORCE_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
        }
        break;
    case DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED:
        if (command == DKSERVERMANAGER_COMMAND_RESUME)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_READY;
        }
        else if (command == DKSERVERMANAGER_COMMAND_FAIL)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER;
        }
        else if (command == DKSERVERMANAGER_COMMAND_FORCE_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
        }
        break;
    case DKSERVERMANAGER_STATEMACHINE_STATE_READY:
        if (command == DKSERVERMANAGER_COMMAND_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDING;
        }
        else if (command == DKSERVERMANAGER_COMMAND_FAIL)
        {
            //state = DKSERVERMANAGER_STATEMACHINE_STATE_SESSIONTIMEOUT;
            state = DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER;
        }
        //else if (command == DKSERVERMANAGER_COMMAND_RECONNECT)
        //{
        //    state = DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER;
        //}
        else if (command == DKSERVERMANAGER_COMMAND_FORCE_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
        }
        break;
    case DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER:
        if (command == DKSERVERMANAGER_COMMAND_SUSPEND)
        {
            //retry_count = 0;
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDING;
        }
        else if (command == DKSERVERMANAGER_COMMAND_SUCCESS)
        {
            //retry_count = 0;
            state = DKSERVERMANAGER_STATEMACHINE_STATE_READY;
        }
        else if (command == DKSERVERMANAGER_COMMAND_FAIL)
        {
            //retry_count++;
            state = DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER;

            //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
            //                 "[%s] retry_count: %d\n", __FUNCTION__, retry_count);

        }
        else if (command == DKSERVERMANAGER_COMMAND_FORCE_SUSPEND)
        {
            state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
        }
        break;
    //case DKSERVERMANAGER_STATEMACHINE_STATE_SESSIONTIMEOUT:
    //    if (command == DKSERVERMANAGER_COMMAND_SUSPEND)
    //    {
    //        state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDING;
    //    }
    //    else if (command == DKSERVERMANAGER_COMMAND_SUCCESS)
    //    {
    //        state = DKSERVERMANAGER_STATEMACHINE_STATE_READY;
    //    }
    //    else if (command == DKSERVERMANAGER_COMMAND_FAIL)
    //    {
    //        state = DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER;
    //    }
    //    else if (command == DKSERVERMANAGER_COMMAND_FORCE_SUSPEND)
    //    {
    //        state = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;
    //    }
    //    break;
    default:
        break;
    }

    if (state != DKSERVERMANAGER_STATEMACHINE_STATE_NONE)
    {
        /*if (DKSManagerState == state &&
            state == DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER &&
            retry_count == CHECKDKSERVER_MAX_RETRY)
        {
            DKServerManager_SendEventIDToWorkerThread(0,
                                                      0,
                                                      DKMSG_SYSTEM_ERROR,
                                                      0,
                                                      DKSERVER_SYSTEM_ERROR_NETWORK_TIMEOUT);
        }
        else */if (DKSManagerState != state)
        {
            DKSManagerState = state;

            DKServerManager_SendEventIDToWorkerThread(0,
                                                      0,
                                                      DKMSG_STATE_CHANGED,
                                                      0,
                                                      state);
        }
    }

    return state;
}

static void HTTPThreadEventReportHandler(void *arg)
{
    HTQHeader_t Header = HTTPManager_GetEventObjHeader(arg);
    int16_t nErrorCode = HTTPManager_GetEventObjErrorCode(arg);
    uint16_t nHTTPStatusCode = HTTPManager_GetEventObjHTTPStatusCode(arg);
    uint16_t ret = 0;
    uint8_t *pHeader = HTTPManager_GetEventObjPayload(arg);
    uint8_t *pData = NULL;

    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    dxOS_RET_CODE result = DXOS_SUCCESS;

    (void)ret;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Line %d, HMType: %d, Current State: %d, nErrorCode: %d\n",
                     __FUNCTION__,
                     __LINE__,
                     Header.MessageType,
                     DKSManagerState,
                     nErrorCode);

    switch(Header.MessageType)
    {
    case HTTPMANAGER_COMMAND_CONNECT:
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "[%s] Line %d, DKQ_Count(): %d, MType: %d\n",
                         __FUNCTION__,
                         __LINE__,
                         DKQ_Count(),
                         DKQ_PeekMessageType());

        if (nErrorCode == DXOS_SUCCESS)
        {
            DKServerManager_State(DKSERVERMANAGER_COMMAND_RESUME);

            if (DKQ_PeekMessageType() > DKMSG_UNKNOWN)
            {
                _BuildAndSendCommon(pThreadParam);
            }

            DKServerManager_State(DKSERVERMANAGER_COMMAND_SUCCESS);
        }
        else
        {
            //DKServerManager_SendEventIDToWorkerThread(0,
            //                                          0,
            //                                          DKMSG_SYSTEM_ERROR,
            //                                          0,
            //                                          nErrorCode);

            _HandleCOMMONErrorCode(nErrorCode);

            // Sometimes, we will see "net_connect(): -68" while connecting to our Cloud Server
            // In this case, DKServerManager state will be in SUSPENDING if we call DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND) once.
            // To fix the issue, we need to call twice.

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Line %d, Call DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND) to force to DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED\n",
                             __LINE__);

            //DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
            //DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
            DKServerManager_State_ForceToSuspended();

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Line %d, DKServerManager_GetCurrentState():%d\n",
                             __LINE__,
                             DKServerManager_GetCurrentState());
        }

        break;

    case HTTPMANAGER_COMMAND_RESUME:
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Line %d, nErrorCode: %d, DKQ_Count(): %d, MType: %d\n",
                         __LINE__,
                         nErrorCode,
                         DKQ_Count(),
                         DKQ_PeekMessageType());

        if (nErrorCode == DXOS_SUCCESS)
        {
            if (DKQ_PeekMessageType() > DKMSG_UNKNOWN)
            {
                _BuildAndSendCommon(pThreadParam);
            }

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "Line %d, DKSManagerState: %d\n",
                             __LINE__,
                             DKSManagerState);

            DKServerManager_State(DKSERVERMANAGER_COMMAND_RESUME);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "Line %d, DKSManagerState: %d\n",
                             __LINE__,
                             DKSManagerState);
        }
        else
        {
            //DKServerManager_SendEventIDToWorkerThread(0,
            //                                          0,
            //                                          DKMSG_SYSTEM_ERROR,
            //                                          0,
            //                                          nErrorCode);

            _HandleCOMMONErrorCode(nErrorCode);

            //DKServerManager_State(DKSERVERMANAGER_COMMAND_RECONNECT);
        }

        break;

    case HTTPMANAGER_COMMAND_SEND:
        if (nErrorCode == DXOS_SUCCESS)
        {
            result = HTTPManager_Receive_Asynch(Header.TCPWaitTimeInSec);

            _HandleCOMMONErrorCode(result);

            pThreadParam->StartRecvTime = dxTimeGetMS();

            if (result == DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, HTTPManager_Receive_Asynch()\n",
                                 __FUNCTION__,
                                 __LINE__);
            }
        }
        else
        {
            //DKServerManager_SendEventIDToWorkerThread(0,
            //                                          0,
            //                                          DKMSG_SYSTEM_ERROR,
            //                                          0,
            //                                          nErrorCode);

            _HandleCOMMONErrorCode(nErrorCode);

            //DKServerManager_State(DKSERVERMANAGER_COMMAND_FAIL);
            DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
            HTTPManager_Suspend_Asynch();
        }
        break;

    case HTTPMANAGER_COMMAND_RECEIVE:

        pThreadParam->StartRecvTime  = 0;

        // Reset Timer. To make sure IsTimeToGetJob
        pThreadParam->LastRequestTime = dxTimeGetMS();

        if (nErrorCode == DXOS_SUCCESS)
        {
            DKServerManager_State(DKSERVERMANAGER_COMMAND_SUCCESS);

            DKOptionHTTPHeader_t OptionHeader;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "Line %d, HTTP Status-Code: %d, len of HTTP response = %d\n",
                             __LINE__,
                             nHTTPStatusCode,
                             Header.DataLen);

            memset(&OptionHeader, 0x00, sizeof(OptionHeader));
            pData = _GetReponseJSONPtr(pHeader, &OptionHeader);

            if (DKQ_PeekMessageType() == DKMSG_FIRMWAREDOWNLOAD)
            {
                DK_PRINTLN((char *)pHeader, (int)(pData - pHeader));
            }
            else
            {
                //GSysDebugger_DumpString (strlen (pHeader), pHeader);
            }

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, DKQ_Count(): %d, DKQ_IsEmpty(): %d\n",
                             __FUNCTION__,
                             __LINE__,
                             DKQ_Count(),
                             DKQ_IsEmpty());

            DKRESTCommMsg_t *pCommand = NULL;
            LOCK(&pThreadParam->mutex);
            DKQ_Pop(&pCommand);
            UNLOCK(&pThreadParam->mutex);

			if ( pCommand == NULL ) {
	            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
	                             "[%s] Line %d, DKQ_Count(): %d\n",
	                             __FUNCTION__,
	                             __LINE__,
	                             DKQ_Count());
			} else {
	            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
	                             "[%s] Line %d, DKQ_Count(): %d, MType: %d\n",
	                             __FUNCTION__,
	                             __LINE__,
	                             DKQ_Count(),
	                             pCommand->Header.MessageType);
			}

            if (nHTTPStatusCode == HTTP_OK)
            {
                _SendEventDKTimeUpdate(&OptionHeader);
            }
            else
            {
                if (pCommand)
                {
                    DKServerManager_SendEventIDToWorkerThread(pCommand->Header.SourceOfOrigin,
                                                              pCommand->Header.TaskIdentificationNumber,
                                                              pCommand->Header.MessageType,
                                                              nHTTPStatusCode,
                                                              0);
                }

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, pCommand: %s\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 pCommand == NULL ? "NULL" : "NOT NULL");

                _CleanDKRESTCommandQueueObj(pCommand);
                break;
            }

            if (nHTTPStatusCode == HTTP_OK && pData)
            {
                if (pCommand)
                {
                    G_SYS_DBG_LOG_V("HTTPMANAGER_COMMAND_RECEIVE MessageType = %d, DataLen = %d\n", pCommand->Header.MessageType, Header.DataLen);
                    if (pCommand->Header.MessageType == DKMSG_FIRMWAREDOWNLOAD)
                    {
                        result = (OptionHeader.ContentRange.nFileSize > 0) ? DKSERVER_STATUS_SUCCESS : DKSERVER_SYSTEM_ERROR_UNDEFINED;

                        // pData pointer to binary data
                        // PHeader pointer to HTTP response header
                        // Header.DataLen == strlen(pHeader), if there is no 0x00 in binary data
                        //
                        // Dump binary data in HEX
                        //DK_PRINT_HEX(pData, Header.DataLen - (pData - pHeader));

                        // Dump the first 32 bytes of binary data in HEX
                        //DK_PRINT_HEX(pData, (Header.DataLen - (pData - pHeader)) > 32 ? 32 : Header.DataLen - (pData - pHeader));

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d, MType: %d, Binary Data Length: %d\n",
                                         __FUNCTION__,
                                         __LINE__,
                                         pCommand->Header.MessageType,
                                         (int)(Header.DataLen - (pData - pHeader)));
                    }
                    else
                    {
                        result = DKMSG_VerifyDKServerResponse(pData, strlen((char *)pData));
                    }

                    if (result == (int)DKSERVER_STATUS_SUCCESS)
                    {
                        uint8_t *pOption = NULL;
                        int cbOption = 0;
                        int len = sizeof(DKRESTCommMsg_t) + pCommand->Header.DataLen;

                        if (OptionHeader.ContentRange.nFileSize > 0)
                        {
                            pOption = (uint8_t *)&OptionHeader.ContentRange;
                            cbOption = sizeof(OptionHeader.ContentRange);
                        }

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d\n",
                                         __FUNCTION__,
                                         __LINE__);

                        result = DKMSG_ParseResponseMessage(pData,
                                                            (int)(Header.DataLen - (pData - pHeader)),
                                                            pCommand,
                                                            len,
                                                            pOption,
                                                            cbOption);
                    }
                    else if (result == (int)DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d\n",
                                         __FUNCTION__,
                                         __LINE__);

                        // Clear SessionToken if SessionToken is invalid
                        ObjectDictionary_Lock ();
                        ObjectDictionary_SetSessionToken (NULL);
                        ObjectDictionary_Unlock ();

                        // TODO...
                        //DKServerManager_State(DKSERVERMANAGER_COMMAND_FAIL);

                        // TODO: TNeed to send renew DKMSG_RENEWSESSIONTOKEN
                        _BuildAndSendRenewSession(pThreadParam);
                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d, result: %d\n",
                                         __FUNCTION__,
                                         __LINE__,
                                         result);
                    }

                    if (result != (int)DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT)
                    {
                        DKServerManager_SendEventIDToWorkerThread(pCommand->Header.SourceOfOrigin,
                                                                  pCommand->Header.TaskIdentificationNumber,
                                                                  pCommand->Header.MessageType,
                                                                  HTTP_OK,
                                                                  result);
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, pCommand: %s\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     pCommand == NULL ? "NULL" : "NOT NULL");
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, nHTTPStatusCode: %d, pData: %s\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 nHTTPStatusCode,
                                 pData == NULL ? "NULL" : "NOT NULL");
            }

            _CleanDKRESTCommandQueueObj(pCommand);

        }
        else
        {
            //DKServerManager_State(DKSERVERMANAGER_COMMAND_RECONNECT);
            DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
            HTTPManager_Suspend_Asynch();

            // Timeout
            //DKServerManager_SendEventIDToWorkerThread(0,
            //                                          0,
            //                                          DKMSG_SYSTEM_ERROR,
            //                                          0,
            //                                          nErrorCode);

            _HandleCOMMONErrorCode(nErrorCode);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d\n",
                             __FUNCTION__,
                             __LINE__);

            // Socket error, pop and free memory
            LOCK(&pThreadParam->mutex);
            DKQ_Pop(NULL);
            UNLOCK(&pThreadParam->mutex);
        }

        // Reset Timer. To make sure IsTimeToGetJob
        pThreadParam->LastRequestTime = dxTimeGetMS();

        break;

    case HTTPMANAGER_COMMAND_DISCONNECT:
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Line %d, HTTPMANAGER_COMMAND_DISCONNECT\n",
                         __LINE__);

        ObjectDictionary_Lock ();
        ObjectDictionary_SetSessionToken (NULL);
        ObjectDictionary_Unlock ();

        //if (DKQ_PeekMessageType() == DKMSG_HOST_REDIRECT)
        {
            DKServerManager_State(DKSERVERMANAGER_COMMAND_FORCE_SUSPEND);
        }

        break;

    case HTTPMANAGER_COMMAND_SUSPEND:
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Line %d, HTTPMANAGER_COMMAND_SUSPEND\n",
                         __LINE__);

        LOCK(&pThreadParam->mutex);
        DKQ_Empty();
        UNLOCK(&pThreadParam->mutex);

        if (nErrorCode == DXOS_SUCCESS)
        {
            //DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
        }
        else
        {
            //DKServerManager_SendEventIDToWorkerThread(0,
            //                                          0,
            //                                          DKMSG_SYSTEM_ERROR,
            //                                          0,
            //                                          nErrorCode);

            _HandleCOMMONErrorCode(nErrorCode);
        }

        DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
        break;
    default:
        break;
    }

    if (DKSManagerState == DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER)
    {

        dxIP_Addr_t *ipaddr = ObjectDictionary_GetHostIp ();

        // Try to reconnect
        result = HTTPManager_Connect_Asynch(ipaddr, DKSM_USE_PORT, TCP_CONNECT_TIMEOUT);

        _HandleCOMMONErrorCode(result);

        if (result == DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, HTTPManager_Connect_Asynch()\n",
                             __FUNCTION__,
                             __LINE__);
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Line %d, Current State: %d\n",
                     __FUNCTION__,
                     __LINE__,
                     DKSManagerState);

    HTTPManager_CleanEventArgumentObj(arg);

    return ;
}

static TASK_RET_TYPE DKServerThread_Loop(TASK_PARAM_TYPE arg)
{
    uint16_t ret = 0;
    dxOS_RET_CODE result = DXOS_SUCCESS;
    dxBOOL bExit = dxFALSE;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)arg;

    DKSManagerState = DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] Started!\n", __FUNCTION__);

    DKRESTCommMsg_t *pLastReceivedCommand = NULL;

    while (bExit == dxFALSE)
    {
        DKRESTCommMsg_t *pReceivedCommand = NULL;

        {
            result = DXOS_ERROR;

            if (pLastReceivedCommand)
            {
                // Restore the message
                pReceivedCommand = pLastReceivedCommand;
                pLastReceivedCommand = NULL;
                result = DXOS_SUCCESS;

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "[%s] Line %d, Restore MType: %d\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 pReceivedCommand->Header.MessageType);
            }
            else if (pLastReceivedCommand == NULL)
            {
                result = dxQueueReceive(pThreadParam->Queue,
                                        (void *)&pReceivedCommand,
                                        SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME);
            }

            if (result == DXOS_ERROR)
            {
                if (IsTimeToGetJob() == 1 &&
                    HTTPManager_GetCurrentState() == HTTPMANAGER_STATEMACHINE_STATE_CONNECTED)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     ">>>> Line %d, DKQ_Count(): %d, DKQ_PeekMessageType(): %d\n", __LINE__, DKQ_Count(), DKQ_PeekMessageType());
                    _BuildAndSendGetJob(pThreadParam);
                }
                else
                {
                    //Short delay before we check again
                    dxTimeDelayMS(100);
                }
            }
            else if (result == DXOS_SUCCESS)
            {
                if (pReceivedCommand && pReceivedCommand->Header.MessageType == DKMSG_STATEMACHINE_SUSPEND)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, MType: %d\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     pReceivedCommand->Header.MessageType);

                    ret = HTTPManager_Suspend_Asynch();

                    _HandleCOMMONErrorCode(ret);

                    //result = dxQueueReceive(pThreadParam->Queue,
                    //                        (void *)&pReceivedCommand,
                    //                        SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME);

                    _CleanDKRESTCommandQueueObj(pReceivedCommand);
                    pReceivedCommand = NULL;
                }
                else if (pReceivedCommand && pReceivedCommand->Header.MessageType == DKMSG_STATEMACHINE_RESUME)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "[%s] Line %d, MType: %d\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     pReceivedCommand->Header.MessageType);

                    if (DK_DNS_Lookup() != DXOS_SUCCESS)
                    {
                        DKServerManager_SendEventIDToWorkerThread(0,
                                                                  0,
                                                                  DKMSG_SYSTEM_ERROR,
                                                                  0,
                                                                  DKSERVER_SYSTEM_ERROR_DNS_LOOKUP_FAILED);
                    }

                    dxIP_Addr_t *ipaddr = ObjectDictionary_GetHostIp ();

                    ret = HTTPManager_Resume_Asynch(ipaddr, DKSM_USE_PORT, TCP_CONNECT_TIMEOUT);

                    _HandleCOMMONErrorCode(ret);

                    //result = dxQueueReceive(pThreadParam->Queue,
                    //                        (void *)&pReceivedCommand,
                    //                        SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME);

                    _CleanDKRESTCommandQueueObj(pReceivedCommand);
                    pReceivedCommand = NULL;
                }
                else if (HTTPManager_GetCurrentState() == HTTPMANAGER_STATEMACHINE_STATE_CONNECTED)
                {
                    if (DKQ_IsEmpty())
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "[%s] Line %d, MType: %d\n",
                                         __FUNCTION__,
                                         __LINE__,
                                         pReceivedCommand->Header.MessageType);

                        DKQ_Push(pReceivedCommand);

                        if (_BuildAndSendCommon(pThreadParam) == dxTRUE)
                        {
                            //result = dxQueueReceive(pThreadParam->Queue,
                            //                        (void *)&pReceivedCommand,
                            //                        SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME);
                        }
                        else
                        {
                            // Pop up message from queue and drop it.
                            //dxQueueReceive(pThreadParam->Queue,
                            //               (void *)&pReceivedCommand,
                            //               SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME);

                            DKQ_Pop(NULL);
                        }

                        _CleanDKRESTCommandQueueObj(pReceivedCommand);
                        pReceivedCommand = NULL;
                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "Line %d, HTTP LAT: %u\n",
                                         __LINE__,
                                         HTTPManager_GetLastAccessTime());

                        // Backup the message
                        //pLastReceivedCommand = pReceivedCommand;
                        pLastReceivedCommand = _DuplicateDKRESTCommandQueueObj(pReceivedCommand);

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "Line %d, Backup %p MTYPE: %u\n",
                                         __LINE__,
                                         (pLastReceivedCommand == NULL) ? 0 : pLastReceivedCommand,
                                         (pLastReceivedCommand == NULL) ? 0 : pLastReceivedCommand->Header.MessageType);

                        _CleanDKRESTCommandQueueObj(pReceivedCommand);
                        pReceivedCommand = NULL;

#if DKSM_USE_WORKAROUND
                        dxTime_t LastAccessTime = HTTPManager_GetLastAccessTime();

                        if (dxTimeGetMS() - LastAccessTime > 2 * SRVM_THREAD_HTTP_TIMEOUT_TIME)
                        {
                            /**
                             * WORKAROUND: Use DKQ_Empty()
                             */
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                             "Line %d, Start ClearQ Workaround!!! HTTP LAT: %u\n",
                                             __LINE__,
                                             LastAccessTime);

                            DKQ_Empty();

                            /**
                             * WORKAROUND: Reboot. Prefer use DKQ_Empty()
                             */
                            /***
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                             "Line %d, Start reboot Workaround!!! HTTP LAT: %u\n",
                                             __LINE__,
                                             LastAccessTime);

                            DKServerManager_SendEventIDToWorkerThread(0,
                                                                      0,
                                                                      DKMSG_SYSTEM_ERROR,
                                                                      0,
                                                                      DKSERVER_SYSTEM_ERROR_UNDEFINED);
                            */
                        }
#endif // DKSM_USE_WORKAROUND
                    }
                }
                else
                {
                    // Message is not DKMSG_STATEMACHINE_SUSPEND or DKMSG_STATEMACHINE_RESUME,
                    // as well as HTTPManager is not in HTTPMANAGER_STATEMACHINE_STATE_CONNECTED state

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "Line %d, MType: %d, HTTPManager_GetCurrentState(): %d\n",
                                     __LINE__,
                                     (pReceivedCommand == NULL) ? 0 : pReceivedCommand->Header.MessageType,
                                     HTTPManager_GetCurrentState());

                    if (DKSManagerState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Line %d, MType: %d, Clear Queue due to SUSPENDED\n",
                                         __LINE__,
                                         (pReceivedCommand == NULL) ? 0 : pReceivedCommand->Header.MessageType);
                    }

                    if (HTTPManager_GetCurrentState() == HTTPMANAGER_STATEMACHINE_STATE_SENDING ||
                        HTTPManager_GetCurrentState() == HTTPMANAGER_STATEMACHINE_STATE_RECEIVING)
                    {
                        // Backup the message
                        //pLastReceivedCommand = pReceivedCommand;
                        pLastReceivedCommand = _DuplicateDKRESTCommandQueueObj(pReceivedCommand);

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                         "Line %d, Backup MTYPE: %u\n",
                                         __LINE__,
                                         (pLastReceivedCommand == NULL) ? 0 : pLastReceivedCommand->Header.MessageType);
                    }
                    //else
                    {
                        // To give up the message
                        _CleanDKRESTCommandQueueObj(pReceivedCommand);
                        pReceivedCommand = NULL;
                    }

                    // To give up all the message in Queue
                    result = DXOS_SUCCESS;
                    while (result == DXOS_SUCCESS &&
                           DKSManagerState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
                    {
                        result = dxQueueReceive(pThreadParam->Queue,
                                                (void *)&pReceivedCommand,
                                                0);
                        if (result == DXOS_SUCCESS)
                        {
                            _CleanDKRESTCommandQueueObj(pReceivedCommand);
                            pReceivedCommand = NULL;
                        }
                    }

                    // Take a sleep to wait for online
                    dxTimeDelayMS(500);
                }
            }
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Done!\n", __FUNCTION__);

    END_OF_TASK_FUNCTION;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
DKEventHeader_t DKServerManager_GetEventObjHeader(void *ReportedEventArgObj)
{
    DKEventHeader_t Header;
    Header.MessageType = DKMSG_UNKNOWN;

    if (ReportedEventArgObj)
    {
        DKServerEventReportObj_t *EventObj = (DKServerEventReportObj_t *)ReportedEventArgObj;
        memcpy(&Header, &EventObj->Header, sizeof(DKEventHeader_t));
    }

    return Header;
}

uint8_t *DKServerManager_GetEventObjPayload(void *ReportedEventArgObj)
{
    uint8_t *pData = NULL;

    if (ReportedEventArgObj)
    {
        DKServerEventReportObj_t *EventObj = (DKServerEventReportObj_t *)ReportedEventArgObj;
        if (EventObj->Header.DataLen > 0)
        {
            pData = EventObj->pData;
        }
    }

    return pData;
}

uint16_t DKServerManager_GetEventObjHTTPStatusCode(void *ReportedEventArgObj)
{
    uint16_t nCode = 0xFFFF;

    if (ReportedEventArgObj)
    {
        DKServerEventReportObj_t *EventObj = (DKServerEventReportObj_t *)ReportedEventArgObj;
        nCode = EventObj->Header.HTTPStatusCode;
    }

    return nCode;
}

DKServer_StatusCode_t DKServerManager_CleanEventArgumentObj(void *ReportedEventArgObj)
{
    if (ReportedEventArgObj == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    DKServerEventReportObj_t *EventObj = (DKServerEventReportObj_t *)ReportedEventArgObj;

    if (EventObj->pData)
    {
        dxMemFree(EventObj->pData);
        EventObj->pData = NULL;
    }

    dxMemFree(EventObj);
    EventObj = NULL;

    return DKSERVER_STATUS_SUCCESS;
}

dxOS_RET_CODE DKServerManager_SendEventToWorkerThread(uint16_t src,
                                                      uint64_t taskid,
                                                      DKMSG_Command_t msg,
                                                      DKJobType_t type,
                                                      uint16_t httpcode,
                                                      uint16_t dkcode,
                                                      void *pData,
                                                      int DataLen)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    DKServerEventReportObj_t *obj = (DKServerEventReportObj_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKServerEventReportObj_t));

    if (obj)
    {
        obj->Header.MessageType = msg;
        obj->Header.SubType = type;
        obj->Header.HTTPStatusCode = httpcode;
        obj->Header.s.ReportStatusCode = dkcode;
        obj->Header.SourceOfOrigin = src;
        obj->Header.TaskIdentificationNumber = taskid;

        if (pData != NULL || DataLen > 0)
        {
            if (msg == DKMSG_NEWJOBS)
            {
                if (type == DKJOBTYPE_STANDARD ||
                    type == DKJOBTYPE_SCHEDULE ||
                    type == DKJOBTYPE_ADVANCED_CONDITION ||
                    type == DKJOBTYPE_ADVANCED_EXECUTION ||
                    type == DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY ||
                    type == DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY)
                {
                    obj->Header.DataLen = (DataLen & 0x0FFFF);
                    obj->pData = dxMemAlloc(SRVM_THREAD_NAME, 1, DataLen + 1);

                    if (obj->pData)
                    {
                        memcpy(obj->pData, pData, DataLen);
                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                                         __FUNCTION__,
                                         __LINE__,
                                         DataLen + 1);
                    }
                }
            }
            else if (msg == DKMSG_GETSERVERINFO ||
                    msg == DKMSG_GETSERVERTIME ||
                    msg == DKMSG_GETLASTFIRMWAREINFO ||
                    msg == DKMSG_DATETIME_UPDATE ||
                    msg == DKMSG_FIRMWAREDOWNLOAD ||
                    msg == DKMSG_GETSCHEDULEJOB ||
                    msg == DKMSG_NUMOFACTIVEUSERS ||
                    msg == DKMSG_GETADVJOBBYGATEWAY ||
                    msg == DKMSG_GETPERIPHERALBYGMACADDRESS ||
                    msg == DKMSG_GETDATACONTAINERBYID ||
                    msg == DKMSG_CREATECONTAINERMDATA ||
                    msg == DKMSG_UPDATECONTAINERMDATA ||
                    msg == DKMSG_INSERTCONTAINERDDATA ||
                    msg == DKMSG_UPDATECONTAINERDDATA ||
                    msg == DKMSG_DELETECONTAINERDDATA ||
                    msg == DKMSG_DELETECONTAINERMDATA ||
                    msg == DKMSG_GETCURRENTUSER ||
                    msg == DKMSG_GETPUSHRTMPLIVEURL ||
                    msg == DKMSG_GETEVENTMEDIAFILEUPLOADPATH ||
                    msg == DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH)
            {
                obj->Header.DataLen = (DataLen & 0x0FFFF);
                obj->pData = dxMemAlloc(SRVM_THREAD_NAME, 1, DataLen + 1);

                if (obj->pData)
                {
                    memcpy(obj->pData, pData, DataLen);
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     DataLen + 1);
                }
            }
        }

        result = dxWorkerTaskSendAsynchEvent(pThreadParam->Worker.Handle,
                                             pThreadParam->Worker.RegisterFunc,
                                             obj);

        if (result != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "[%s] Line %d, FATAL: dxWorkerTaskSendAsynchEvent() failed, RetCode = %d\n",
                             __FUNCTION__,
                             __LINE__,
                             result);

            dxMemFree(obj);
            obj = NULL;
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKServerEventReportObj_t));

        result = DXOS_NO_MEM;
    }

    return result;
}

dxOS_RET_CODE DKServerManager_SendEventIDToWorkerThread(uint16_t src,
                                                        uint64_t taskid,
                                                        DKMSG_Command_t msg,
                                                        uint16_t httpcode,
                                                        uint16_t dkcode)
{
    return DKServerManager_SendEventToWorkerThread(src,
                                                   taskid,
                                                   msg,
                                                   DKJOBTYPE_NONE,
                                                   httpcode,
                                                   dkcode,
                                                   NULL,
                                                   0);
}

DKServer_StatusCode_t DKServerManager_Init(dxEventHandler_t EventFunction, uint8_t *macaddr, int macaddrlen, uint8_t *dkServerName, uint8_t *appID, uint8_t *apiKey, uint8_t *apiVer)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;

    DKCommon_Init ();

    ObjectDictionary_Init (macaddr, macaddrlen, dkServerName, appID, apiKey, apiVer);


    DKThreadParam.mutex = dxMutexCreate();
    if (DKThreadParam.mutex == NULL)
    {
        return DKSERVER_STATUS_ERROR_DX_RTOS;
    }

    // Initialize Queue
    if (DKThreadParam.Queue == NULL)
    {

        DKThreadParam.Queue = dxQueueCreate(SRVM_THREAD_QUEUE_MAX_ALLOWED,
                                            sizeof(DKRESTCommMsg_t *));

        if (DKThreadParam.Queue == NULL)
        {
            return DKSERVER_STATUS_ERROR_DX_RTOS;
        }
    }

    // Initialize
    DKThreadParam.Handle = NULL;

    result = dxTaskCreate(DKServerThread_Loop,
                          SRVM_THREAD_NAME,
                          SRVM_THREAD_STACK_SIZE,
                          (void *)&DKThreadParam,
                          SRVM_THREAD_PRIORITY,
                          &DKThreadParam.Handle);

    if (DKThreadParam.Handle == NULL)
    {
        return DKSERVER_STATUS_ERROR_DX_RTOS;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&DKThreadParam.Handle);

    dxWorkweTask_t *worker = dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(dxWorkweTask_t));

    if (DKThreadParam.Worker.RegisterFunc == NULL)
    {
        result = dxWorkerTaskCreate("SRVMGR W",
                                    worker,
                                    DX_DEFAULT_WORKER_PRIORITY,
                                    SRVM_THREAD_WORKER_THREAD_STACK_SIZE,
                                    SRVM_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE);

        if (result != DXOS_SUCCESS)
        {
            dxTaskDelete(DKThreadParam.Handle);
            DKThreadParam.Handle = NULL;

            return DKSERVER_STATUS_ERROR_DX_RTOS;
        }

        DKThreadParam.Worker.Handle = worker;

        GSysDebugger_RegisterForThreadDiagnosticGivenName(&DKThreadParam.Worker.Handle->WTask, "DKSMEvent");

        DKThreadParam.Worker.RegisterFunc = EventFunction;
    }

    // Create HTTP Thread
    HTTPManager_Init(HTTPThreadEventReportHandler);

    return DKSERVER_STATUS_SUCCESS;
}

DKServer_StatusCode_t DKServerManager_State_Suspend(void)
{
    DKRESTCommMsg_t *msg = NULL;

    msg = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_STATEMACHINE_SUSPEND;

    DKServer_StatusCode_t ret = _DKSMPushMessageToQueue(&msg);

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        DKServerManager_State(DKSERVERMANAGER_COMMAND_SUSPEND);
    }

    return ret;
}

DKServer_StatusCode_t DKServerManager_State_Resume(void)
{
    DKRESTCommMsg_t *msg = NULL;

    msg = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_STATEMACHINE_RESUME;

    DKServer_StatusCode_t ret = _DKSMPushMessageToQueue(&msg);

    //if (ret == DKSERVER_STATUS_SUCCESS)
    //{
    //    DKServerManager_State(DKSERVERMANAGER_COMMAND_RESUME);
    //}

    return ret;
}

DKServer_StatusCode_t DKServerManager_State_ForceToSuspended(void)
{
    LOCK(&DKThreadParam.mutex);
    DKServerManager_State(DKSERVERMANAGER_COMMAND_FORCE_SUSPEND);
    UNLOCK(&DKThreadParam.mutex);

    return DKSERVER_STATUS_SUCCESS;
}

DKServer_StatusCode_t DKServerManager_GatewayLogin_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *gatewaykey)
{
    DKRESTCommMsg_t *msg = NULL;
    UserLogin_t *pUser = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (gatewaykey == NULL || strlen((char *)gatewaykey) > sizeof(pUser->Username))
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_GATEWAYLOGIN;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

/**
 *     Header.DataLen           = sizeof(UserLogin_t)
 *     pData                    = +----------------+-------------+-----------------------+
 *                                | Username       | ( 64 Byte ) |                       |
 *                                +----------------+-------------+                       |
 *                                | Password       | ( 32 Byte ) | UserLogin_t           |
 *                                +----------------+-------------+                       |
 *                                | DK_LoginType_t | ( 4 Byte )  |                       |
 *                                +----------------+-------------+-----------------------+
 */

    msg->Header.DataLen = sizeof(UserLogin_t);
    msg->pData = dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(UserLogin_t));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(UserLogin_t));

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pUser = (UserLogin_t *)msg->pData;
    memcpy(pUser->Username, (char *)gatewaykey, strlen((char *)gatewaykey));
    pUser->LoginType = DK_LOGINTYPE_GATEWAY;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_AnonymousLogin_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *accessToken, uint32_t accTokenTimeout)
{
    DKRESTCommMsg_t *msg = NULL;
    UserLogin_t *pUser = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (accessToken == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_ANONYMOUSLOGIN;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

/**
 *     Header.DataLen           = sizeof(UserLogin_t)
 *     pData                    = +----------------+-------------+----------------------------------+
 *                                | Username       | ( 64 Byte ) |                                  |
 *                                +----------------+-------------+                                  |
 *                                | Password       | ( 32 Byte ) | UserLogin_t                      |
 *                                +----------------+-------------+                                  |
 *                                | DK_LoginType_t | ( 4 Byte )  |                                  |
 *                                +----------------+-------------+----------------------------------+
 *                                | AccTokenTime   | ( 4 Byte )  |                                  |
 *                                +----------------+-------------+----------------------------------+
 *                                | AccessToken    | ( n Byte )  | Should be NULL terminated string |
 *                                +----------------+-------------+----------------------------------+
 */

    int16_t nLen = sizeof(UserLogin_t) + sizeof(accTokenTimeout) + strlen((char *)accessToken) + 1; // Including 0x00 NULL terminated string
    msg->Header.DataLen = nLen;
    msg->pData = dxMemAlloc(SRVM_THREAD_NAME, 1, nLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(UserLogin_t));

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pUser = (UserLogin_t *)msg->pData;
    pUser->LoginType = DK_LOGINTYPE_ANONYMOUS;

    int index = sizeof(UserLogin_t);

    // AccTokenTime
    memcpy(&msg->pData[index], &accTokenTimeout, sizeof(accTokenTimeout));
    index += sizeof(accTokenTimeout);

    // AccessToken
    memcpy(&msg->pData[index], accessToken, strlen((char *)accessToken));
    index += strlen((char *)accessToken);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetPeripheralByMAC_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int nStartFrom, int nMaxToReceive)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (nStartFrom <= 0 ||
        nMaxToReceive <= 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(SRVM_THREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_GETPERIPHERALBYGMACADDRESS;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /**
     *     Header.DataLen           = sizeof(int) * 2
     *     pData                    = +----------------+-------------+
     *                                | nStartFrom     | ( 4 Byte )  |
     *                                +----------------+-------------+
     *                                | nMaxToReceive  | ( 4 Byte )  |
     *                                +----------------+-------------+
     */

    len = sizeof(int) * 2;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc (CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                          __FUNCTION__,
                          __LINE__,
                          len + 1);

        dxMemFree (msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy to msg->pData
    memcpy(&msg->pData[idx], &nStartFrom, sizeof (int));
    idx += sizeof(int);
    memcpy(&msg->pData[idx], &nMaxToReceive, sizeof (int));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdatePeripheralDataPayload_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint8_t *datapayload, uint16_t payloadlen, DKServer_Update_DataPayload_Target_Info updateTo, uint32_t advObjID)
{
    DKRESTCommMsg_t *msg = NULL;
    UpdatePeripheralStatusData_t *pPeripheralStatus = NULL;
    uint16_t len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (datapayload == NULL ||
        payloadlen == 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEPERIPHERALDATAPAYLOAD;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(UpdatePeripheralStatusData_t) + payloadlen;
    msg->Header.DataLen = (uint16_t)len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pPeripheralStatus = (UpdatePeripheralStatusData_t *)msg->pData;
    pPeripheralStatus->UpdateTo = updateTo;
    pPeripheralStatus->PayloadLen = payloadlen;
    pPeripheralStatus->AdvObjID = advObjID;

    memcpy(&msg->pData[offsetof(UpdatePeripheralStatusData_t, ObjectID)], ObjectID, sizeof (uint32_t));
    memcpy(&msg->pData[offsetof(UpdatePeripheralStatusData_t, pPayload)], datapayload, payloadlen);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdatePeripheralStatusInfo_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint16_t signal, uint16_t battery, uint16_t status)
{
    DKRESTCommMsg_t *msg = NULL;
    UpdatePeripheralStatusData_t *pPeripheralStatus = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEPERIPHERALSTATUSINFO;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(UpdatePeripheralStatusData_t);
    msg->Header.DataLen = len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, len + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pPeripheralStatus = (UpdatePeripheralStatusData_t *)msg->pData;
    pPeripheralStatus->PSignalStr = signal;
    pPeripheralStatus->PBattery = battery;
    pPeripheralStatus->RunningStatus = status;
    pPeripheralStatus->PayloadLen = 0;

    memcpy(&msg->pData[offsetof(UpdatePeripheralStatusData_t, ObjectID)], ObjectID, sizeof (uint32_t));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetJobs_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_NEWJOBS;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = 0;
    msg->pData = NULL;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_AddGateway_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, char *deviceName, double latitude, double longitude, char *wifiv, char *blev)
{
    DKRESTCommMsg_t *msg = NULL;
    AddGateway_t *pGateway = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (deviceName == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_ADDGATEWAY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(AddGateway_t);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (sizeof(AddGateway_t) + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(AddGateway_t) + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pGateway = (AddGateway_t *)msg->pData;

    // DeviceName
    len = strlen(deviceName);
    if (len >= sizeof(pGateway->szDeviceName))
    {
        len = sizeof(pGateway->szDeviceName) - 1;
    }
    memcpy(pGateway->szDeviceName, deviceName, len);

    // WiFiVersion
    if (wifiv)
    {
        len = strlen(wifiv);
        if (len >= sizeof(pGateway->WiFiVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pGateway->WiFiVersion, wifiv, len);
    }

    // BLEVersion
    if (blev)
    {
        len = strlen(blev);
        if (len >= sizeof(pGateway->BLEVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pGateway->BLEVersion, blev, len);
    }

    pGateway->Latitude = latitude;
    pGateway->Longitude = longitude;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_AddPeripheral_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *macaddr, int macaddrlen, char *deviceName, int deviceType, uint16_t signal, uint16_t battery, char *fwv, uint8_t *seckey, uint8_t seckeylen)
{
    DKRESTCommMsg_t *msg = NULL;
    AddPeripheral_t *pPeripheral = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (deviceName == NULL ||
        macaddr == NULL ||
        macaddrlen != 8)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    if (ObjectDictionary_DoesPeripheralExist (macaddr, macaddrlen) == 1)
    {
        ObjectDictionary_Unlock ();

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "Peripheral's MAC address exist in database table. Do nothing\n");
        return DKSERVER_STATUS_SUCCESS;
    }

    ObjectDictionary_Unlock ();

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_ADDPERIPHERAL;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(AddPeripheral_t);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (sizeof(AddPeripheral_t) + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(AddPeripheral_t) + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pPeripheral = (AddPeripheral_t *)msg->pData;

    // DeviceName
    len = strlen(deviceName);
    if (len >= sizeof(pPeripheral->szDeviceName))
    {
        len = sizeof(pPeripheral->szDeviceName) - 1;
    }
    memcpy(pPeripheral->szDeviceName, deviceName, len);

    // FWVersion
    if (fwv)
    {
        len = strlen(fwv);
        if (len >= sizeof(pPeripheral->FWVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pPeripheral->FWVersion, fwv, len);
    }

    // SecurityKey
    if (seckey)
    {
        if (seckeylen > sizeof(pPeripheral->szSecurityKey))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        pPeripheral->nSecurityKeyLen = seckeylen;
        memcpy(pPeripheral->szSecurityKey, seckey, seckeylen);
    }

    // MAC
    memcpy(&msg->pData[offsetof(AddPeripheral_t, MAC)], macaddr, macaddrlen);

    // Others Numeric Data Type
    pPeripheral->deviceType = deviceType;
    pPeripheral->PSignalStr = signal;
    pPeripheral->PBattery = battery;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_SendNotificationMessage_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *message)
{
    DKRESTCommMsg_t *msg = NULL;
    NotificationMessage_t *pNotifyMessage = NULL;
    int len = 0;
    int msglen = strlen((char *)message);

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (message == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_SENDNOTIFICATIONMESSAGE;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(pNotifyMessage->MessageLen) + msglen;
    msg->Header.DataLen = len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pNotifyMessage = (NotificationMessage_t *)msg->pData;
    pNotifyMessage->MessageLen = msglen;
    memcpy(&msg->pData[offsetof(NotificationMessage_t, pMessageData)],  message,        msglen);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_RenewSessionToken_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_RENEWSESSIONTOKEN;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_JobDoneReport_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, JobDoneReport_t *job, int NumOfJob)
{
    DKRESTCommMsg_t *msg = NULL;
    JobDoneReport_t *pJobReport = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (job == NULL || NumOfJob < 1)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    /**
     * +-----+       +-----------------------------+-------------+
     * | Job +------>| ObjectID                    | ( 4 Bytes ) |
     * +-----+       +-----------------------------+-------------+
     *               | DeviceIdentificationNumber  | ( 2 Bytes ) |
     *               +-----------------------------+-------------+
     *               | PayLoadIdentificationNumber | ( 2 Bytes ) |
     *               +-----------------------------+-------------+
     *               | JobStatus                   | ( 4 Bytes ) |
     *               +-----------------------------+-------------+
     *               | Reserved                    | ( 2 Bytes ) |
     *               +-----------------------------+-------------+
     *               | JobPayloadLen               | ( 2 Bytes ) |
     *               +-----------------------------+-------------+         +------------------------------------------+
     *               | pJobPayload                 | ( 4 Bytes ) + ------> | Binary data with length of JobPayloadLen |
     *               +-----------------------------+-------------+         +------------------------------------------+
     *
     * +------------+       +-----------------------------+-------------------------+------+
     * | pJobReport +------>| ObjectID                    | ( 4 Bytes )             |      |
     * +------------+       +-----------------------------+-------------------------+      |
     *                      | DeviceIdentificationNumber  | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | PayLoadIdentificationNumber | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | JobStatus                   | ( 4 Bytes )             | # 0  |
     *                      +-----------------------------+-------------------------+      |
     *                      | Reserved                    | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | JobPayloadLen               | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | pJobPayload                 | ( 4 Bytes, 0x00000000 ) |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | Binary data                 | JobPayloadLen           |      |
     *                      +-----------------------------+-------------------------+------+
     *                      | ObjectID                    | ( 4 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | DeviceIdentificationNumber  | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | PayLoadIdentificationNumber | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | JobStatus                   | ( 4 Bytes )             | # 1  |
     *                      +-----------------------------+-------------------------+      |
     *                      | Reserved                    | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | JobPayloadLen               | ( 2 Bytes )             |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | pJobPayload                 | ( 4 Bytes, 0x00000000 ) |      |
     *                      +-----------------------------+-------------------------+      |
     *                      | Binary data                 | JobPayloadLen           |      |
     *                      +-----------------------------+-------------------------+------+
     */

    int TotalJobLen = 0;
    int i = 0;
    for (i = 0; i < NumOfJob; i++)
    {
        JobDoneReport_t *p = (JobDoneReport_t *)&job[i];
        TotalJobLen += sizeof(JobDoneReport_t) + p->JobPayloadLen;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_JOBDONEREPORT;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;
    msg->Header.DataLen = TotalJobLen;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, TotalJobLen);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         TotalJobLen);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;
    for (i = 0; i < NumOfJob; i++)
    {
        JobDoneReport_t *p = (JobDoneReport_t *)&job[i];
        pJobReport = (JobDoneReport_t *)&msg->pData[index];

        pJobReport->ObjectID = p->ObjectID;
        pJobReport->DeviceIdentificationNumber = p->DeviceIdentificationNumber;
        pJobReport->PayLoadIdentificationNumber =  p->PayLoadIdentificationNumber;
        pJobReport->JobStatus = p->JobStatus;
        pJobReport->Reserved = 0;

        pJobReport->JobPayloadLen = p->JobPayloadLen;

        if (p->pJobPayload && p->JobPayloadLen > 0)
        {
            memcpy(&msg->pData[index + sizeof(JobDoneReport_t)], p->pJobPayload, p->JobPayloadLen);
        }

        index += sizeof(JobDoneReport_t) + p->JobPayloadLen;
    }

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetServerInfo_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_GETSERVERINFO;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdateGatewayFirmwareVersion_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, char *wifiv, char *blev)
{
    DKRESTCommMsg_t *msg = NULL;
    AddGateway_t *pGateway = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (wifiv == NULL && blev == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEGATEWAYFIRMWAREVERSION;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(AddGateway_t);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (sizeof(AddGateway_t) + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(AddGateway_t) + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pGateway = (AddGateway_t *)msg->pData;

    // WiFiVersion
    if (wifiv)
    {
        len = strlen(wifiv);
        if (len >= sizeof(pGateway->WiFiVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pGateway->WiFiVersion, wifiv, len);
    }

    // BLEVersion
    if (blev)
    {
        len = strlen(blev);
        if (len >= sizeof(pGateway->BLEVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pGateway->BLEVersion, blev, len);
    }

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_ChangeGatewayStatus_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint16_t status)
{
    DKRESTCommMsg_t *msg = NULL;
    AddGateway_t *pGateway = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_CHANGEGATEWAYSTATUS;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(AddGateway_t);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (sizeof(AddGateway_t) + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(AddGateway_t) + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pGateway = (AddGateway_t *)msg->pData;

    // Status
    pGateway->Status = status;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdatePeripheralFirmware_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, char *fwv)
{
    DKRESTCommMsg_t* msg = NULL;
    PeripheralFirmware_t* pPeripheral = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (fwv == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEPERIPHERALFIRMARE;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(PeripheralFirmware_t);
    msg->Header.DataLen = len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pPeripheral = (PeripheralFirmware_t *)msg->pData;

    // MAC
    memcpy(&msg->pData[offsetof(PeripheralFirmware_t, ObjectID)], ObjectID, sizeof (uint32_t));

    // FWVersion
    if (fwv)
    {
        len = strlen(fwv);
        if (len >= sizeof(pPeripheral->FWVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pPeripheral->FWVersion, fwv, len);
    }

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_SetPeripheralQueryCollection_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, PeripheralQuery_t *peripheral)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (peripheral == NULL) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_SETPERIPHERALQUERYCOLLECTION;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(PeripheralQuery_t);
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memcpy(&msg->pData[offsetof(PeripheralQuery_t, PMAC)],  peripheral->PMAC,   sizeof(peripheral->PMAC));

    PeripheralQuery_t *p = (PeripheralQuery_t *)msg->pData;
    p->DeviceType = peripheral->DeviceType;
    p->PSignalStr = peripheral->PSignalStr;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetServerTime_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(msg, 0x00, sizeof(DKRESTCommMsg_t));

    msg->Header.MessageType = DKMSG_GETSERVERTIME;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdatePeripheralDataPayloadToHistory_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, PeripheralDataPayloadInfo_t *payload, int count)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int i = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (payload == NULL || count <= 0) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEDATAPAYLOADHISTORY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(PeripheralDataPayloadInfo_t) * count;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    for (i = 0; i < count; i++)
    {
        PeripheralDataPayloadInfo_t *p = (PeripheralDataPayloadInfo_t *)&msg->pData[sizeof(PeripheralDataPayloadInfo_t) * i];
        memcpy(p->AdData, payload->AdData, payload->AdDataLen);
        p->ObjectID = payload->ObjectID;
        p->AdDataLen = payload->AdDataLen;
        p->TimeStamp = payload->TimeStamp;
        /**
         * According to document v0.142 specification, added two variables, AdvObjID and CreatedByUserobjID
         */
        p->AdvObjID           = payload->AdvObjID;
        p->CreatedByUserobjID = payload->CreatedByUserobjID;
        payload++;
    }

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_SendMessageWithKey_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *key, uint8_t *pmszParams, int cbSize)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (key == NULL ||
        cbSize < 0) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_SENDMESSAGEWITHKEY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = cbSize + strlen((char *)key) + 1;         // 1 => 0x00
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, len + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy key to msg->pData
    idx = 0;
    memcpy(&msg->pData[idx], key, strlen((char *)key));
    idx += strlen((char *)key) + 1;

    // Copy to msg->pData
    memcpy(&msg->pData[idx], pmszParams, cbSize);

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_SendMessageWithKeyNum_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t key, uint8_t *category, uint8_t *resource, uint8_t *pmszParams, int cbSize)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (cbSize < 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_SENDMESSAGEWITHKEYNUM;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /**
     * +-----+-+----------+-+----------+-+-----+-+-----+-+----+-----+-+-+
     * | key |0| category |0| resource |0| $1$ |0| $2$ |0|... | $n$ |0|0|
     * +-----+-+----------+-+----------+-+-----+-+-----+-+----+-----+-+-+
     */

    // 1 => 0x00
    len = cbSize + sizeof(key) + 1;

    // MessageCategory (Optional)
    if (category)
    {
        len +=  strlen((char *)category);
    }
    len++;

    // MessageResource (Optional)
    if (resource)
    {
        len += + strlen((char *)resource);
    }
    len++;

    msg->Header.DataLen = len + 1;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, len + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy key to msg->pData
    idx = 0;

    memcpy(&msg->pData[idx], &key, sizeof(key));

    idx += sizeof(key) + 1;     // Always including a 0x00 NULL terminated char

    // MessageCategory (Optional)
    if (category)
    {
        // Copy category to msg->pData
        len = strlen((char *)category);

        if (len > 0)
        {
            memcpy(&msg->pData[idx], category, len);
            idx += len;
        }
    }

    idx += 1;       // Always including a 0x00 NULL terminated char

    // MessageResource (Optional)
    if (resource)
    {
        // Copy resource to msg->pData
        len = strlen((char *)resource);
        if (len > 0)
        {
            memcpy(&msg->pData[idx], resource, len);
            idx += len;
        }
    }

    idx += 1;       // Always including a 0x00 NULL terminated char

    // Copy to msg->pData
    memcpy(&msg->pData[idx], pmszParams, cbSize);

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_NotifyEventDone( uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int Type, uint8_t *resource )
{
	DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    // Allocate memory
    msg = ( DKRESTCommMsg_t * ) dxMemAlloc( CALLOC_NAME, 1, sizeof( DKRESTCommMsg_t ) );

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                          __FUNCTION__,
                          __LINE__,
                          ( int ) sizeof( DKRESTCommMsg_t ) );

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_NOTIFYEVENTDONE;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /**
     * +------+-+----------+-+-+
     * | Type |0| resource |0|0|
     * +------+-+----------+-+-+
     */

    // 1 => 0x00
    len = sizeof(Type) + 1;

    // Resource
    if ( resource )
    {
        len += strlen( ( char * ) resource );
    }
    len++;

    msg->Header.DataLen = len + 1;
    msg->pData = dxMemAlloc( CALLOC_NAME, 1, len + 1 );

    if ( msg->pData == NULL )
    {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                          __FUNCTION__,
                          __LINE__,
                          len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy Type to msg->pData
    idx = 0;

    memcpy( &msg->pData[idx], &Type, sizeof( Type ) );

    idx += sizeof(Type) + 1;     // Always including a 0x00 NULL terminated char

    // Resource
    if ( resource )
    {
        // Copy resource to msg->pData
        len = strlen( ( char * ) resource );
        if ( len > 0 )
        {
            memcpy( &msg->pData[ idx ], resource, len );
            idx += len;
        }
    }

    idx += 1;       // Always including a 0x00 NULL terminated char

    // Push to queue
    return _DKSMPushMessageToQueue( &msg );
}

DKServer_StatusCode_t DKServerManager_GetLastFirmareInfo_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETLASTFIRMWAREINFO;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = 0;

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_FirmwareDownload_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *pszurl, int byteposition, int bytestoread)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pszurl == NULL ||
        byteposition < 0 ||
        bytestoread <= 0) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    // Allocate memory
    msg = (DKRESTCommMsg_t *)(DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg->Header.MessageType = DKMSG_FIRMWAREDOWNLOAD;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(int) * 2 + strlen((char *)pszurl);
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy to msg->pData
    memcpy(&msg->pData[idx], &byteposition, sizeof(int));
    idx += sizeof(int);
    memcpy(&msg->pData[idx], &bytestoread, sizeof(int));
    idx += sizeof(int);
    memcpy(&msg->pData[idx], pszurl, strlen((char *)pszurl));

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetScheduleJob_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int nStartFrom, int nMaxToReceive)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (nStartFrom <= 0 ||
        nMaxToReceive <= 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETSCHEDULEJOB;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /**
     *     Header.DataLen           = sizeof(int) * 2
     *     pData                    = +----------------+-------------+
     *                                | nStartFrom     | ( 4 Byte )  |
     *                                +----------------+-------------+
     *                                | nMaxToReceive  | ( 4 Byte )  |
     *                                +----------------+-------------+
     */

    len = sizeof(int) * 2;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy to msg->pData
    memcpy(&msg->pData[idx], &nStartFrom, sizeof(int));
    idx += sizeof(int);
    memcpy(&msg->pData[idx], &nMaxToReceive, sizeof(int));

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_DeleteScheduleJob_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint64_t nScheduleSEQNO)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (nScheduleSEQNO == 0) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_DELETESCHEDULEJOB;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(nScheduleSEQNO);
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy to msg->pData
    memcpy(&msg->pData[idx], &nScheduleSEQNO, sizeof(nScheduleSEQNO));

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_ClearPeripheralSecurityKey_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID)
{
    DKRESTCommMsg_t *msg = NULL;

    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_CLEARPERIPHERALSECURITYKEY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(PeripheralSecurityKey_t);
    msg->Header.DataLen = len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // MAC
    memcpy(&msg->pData[offsetof(PeripheralSecurityKey_t, ObjectID)], ObjectID, sizeof (uint32_t));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_SetPeripheralSecurityKey_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint8_t *seckey, uint8_t seckeylen)
{
    DKRESTCommMsg_t *msg = NULL;
    PeripheralSecurityKey_t *pPeripheral = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (seckey == NULL ||
        seckeylen == 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_SETPERIPHERALSECURITYKEY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(PeripheralSecurityKey_t);
    msg->Header.DataLen = len;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    pPeripheral = (PeripheralSecurityKey_t *)msg->pData;
    memcpy(&msg->pData[offsetof(PeripheralSecurityKey_t, ObjectID)], ObjectID, sizeof (uint32_t));

    // SecurityKey
    if (seckey)
    {
        if (seckeylen > sizeof(pPeripheral->szSecurityKey))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        pPeripheral->nSecurityKeyLen = seckeylen;
        memcpy(pPeripheral->szSecurityKey, seckey, seckeylen);
    }

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdatePeripheralAggregationData_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, AggregateDataHeader_t *pAgHeader, AggregateItem_t *pAgItem, int nNumOfAgItem)
{
    DKRESTCommMsg_t *msg = NULL;
    AggregateHeaderFormat_t *p = NULL;
    uint32_t AgItemSize = 0;
    int len = 0;
    int i = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pAgHeader == NULL ||
        pAgItem == NULL ||
        nNumOfAgItem <= 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    p = (AggregateHeaderFormat_t *)&pAgHeader->DataHeader;
    if (p->AgDFormat == 1 && _IsUTCTimeReady() == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "Server's UTC time is not ready. Abort. Line %d\n", __LINE__);

        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATEPERIPHERALAGGREGATIONDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

/**
 *     Header.DataLen           = sizeof(AggregateDataHeader_t) + sizeof(AggregateItem_t) * nNumOfAgItem
 *     pData                    = +-----------+-------------+-----------------------+
 *                                | MAC       | ( 8 Bytes ) |                       |
 *                                +-----------+-------------+ AggregateDataHeader_t |
 *                                | HEADER    | ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *                                | DATA_ID   | ( 2 Byte )  |                       |
 *                                +-----------+-------------+                       |
 *                                | DURATION  | ( 2 Byte )  | AggregateItem_t       |
 *                                +-----------+-------------+                       |
 *                                | DATA_VALUE| ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *                                |               ......                            |
 *                                +-----------+-------------+-----------------------+
 *                                | DATA_ID   | ( 2 Byte )  |                       |
 *                                +-----------+-------------+                       |
 *                                | DURATION  | ( 2 Byte )  | AggregateItem_t       |
 *                                +-----------+-------------+                       |
 *                                | DATA_VALUE| ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *
 */
    if (p->AgDFormat == 1)
    {
        AgItemSize = sizeof(AggregateItemAgD1_t);
    }
    else
    {
        AgItemSize = sizeof(AggregateItem_t);
    }

    len = sizeof(AggregateDataHeader_t) + AgItemSize * nNumOfAgItem;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // AggregateDataHeader_t
    memcpy(&msg->pData[i], pAgHeader, sizeof(AggregateDataHeader_t));
    i += sizeof(AggregateDataHeader_t);

    // AggregateItem_t
    memcpy(&msg->pData[i], pAgItem, AgItemSize * nNumOfAgItem);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdateAdvJobStatus_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, AdvJobStatusSrv_t *pAdvJobStatus, int nNumOfAdvJobItem)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int i = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pAdvJobStatus == NULL ||
        nNumOfAdvJobItem <= 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

/**
 *     Header.DataLen           = sizeof(AdvJobStatusSrv_t) * nNumOfAdvJobItem
 *     pData                    = +------------+-------------+-----------------------+
 *                                | nTimeStamp | ( 4 Byte )  |                       |
 *                                +------------+-------------+                       |
 *                                | AdvObjID   | ( 4 Byte )  | AdvJobStatusSrv_t     |
 *                                +------------+-------------+                       |
 *                                | JobStatus  | ( 1 Byte )  |                       |
 *                                +------------+-------------+-----------------------+
 *                                |                ......                            |
 *                                +------------+-------------+-----------------------+
 *                                | nTimeStamp | ( 4 Byte )  |                       |
 *                                +------------+-------------+                       |
 *                                | AdvObjID   | ( 4 Byte )  | AdvJobStatusSrv_t     |
 *                                +------------+-------------+                       |
 *                                | JobStatus  | ( 1 Byte )  |                       |
 *                                +------------+-------------+-----------------------+
 */

    msg->Header.MessageType = DKMSG_UPDATEADVJOBSTATUS;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    len = sizeof(AdvJobStatusSrv_t) * nNumOfAdvJobItem;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // AdvJobStatusSrv_t
    memcpy(&msg->pData[i], pAdvJobStatus, sizeof(AdvJobStatusSrv_t) * nNumOfAdvJobItem);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetAdvJobCondition_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint16_t nStartIndex, uint8_t nMaxRows)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int i = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETADVJOBBYGATEWAY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /**
     *     Header.DataLen           = sizeof (uint16_t) + sizeof (uint8_t);
     *     pData                    = +----------------+-------------+
     *                                | nStartIndex    | ( 2 Byte )  |
     *                                +----------------+-------------+
     *                                | nMaxRows       | ( 1 Byte )  |
     *                                +----------------+-------------+
     */

    len = sizeof(nStartIndex) + sizeof(nMaxRows);
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // nStartIndex
    memcpy(&msg->pData[i], &nStartIndex, sizeof(nStartIndex));
    i += sizeof(nStartIndex);

    // nMaxRows
    memcpy(&msg->pData[i], &nMaxRows, sizeof(nMaxRows));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_HostRedirect_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *dkServerName, uint8_t *apiVer)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;
    int idx = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (dkServerName == NULL ||
        strlen((char *)dkServerName) > 64 ||
        apiVer == NULL ||
        strlen((char *)apiVer) > 16)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_HOST_REDIRECT;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

/*
 *     Header.DataLen           = Variable length of data
 *     pData                    = +--------------+---+--------+---+---+
 *                                | dkServerName | 0 | apiVer | 0 | 0 | where 0 indicated a null (0x00) character
 *                                +--------------+---+--------+---+---+
 *                                Maximum length of dkServerName is 64
 *                                Maximum length of apiVer is 16
 */
    len = strlen((char *)dkServerName) + 1 + strlen((char *)apiVer) + 1;    // 1 => 0x00 character
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy dkServerName to msg->pData
    idx = 0;
    memcpy(&msg->pData[idx], (char *)dkServerName, strlen((char *)dkServerName));
    idx += strlen((char *)dkServerName) + 1;

    // Copy to msg->pData
    memcpy(&msg->pData[idx], (char *)apiVer, strlen((char *)apiVer));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_AddSmartHomeCentralLog_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *pLogData, int cbLogDataLen)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pLogData == NULL ||
        cbLogDataLen <= 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_ADDSMARTHOMECENTRALLOG;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = cbLogDataLen, <Depends on pData>
     *     pData                    = +--------------+
     *                                | pLogData     |
     *                                +--------------+
     */
    len = cbLogDataLen;
    msg->Header.DataLen = len;
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, (len + 1) * sizeof(char));

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Copy pLogData to msg->pData
    memcpy(&msg->pData[0], (char *)pLogData, cbLogDataLen);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_AddStandalonePeripheral_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *macaddr, int macaddrlen, char *deviceName, double latitude, double longitude, int deviceType, uint16_t signal, uint16_t battery, char *fwv, uint8_t *seckey, uint8_t seckeylen)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    AddPeripheral_t *pPeripheral = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (deviceName == NULL ||
        macaddr == NULL ||
        macaddrlen != 8)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    if (ObjectDictionary_DoesPeripheralExist (macaddr, macaddrlen) == 1)
    {
        ObjectDictionary_Unlock ();

        // Peripheral's MAC address exist in database table. Do nothing.
        return DKSERVER_STATUS_SUCCESS;
    }

    ObjectDictionary_Unlock ();

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_ADDSTANDALONEPERIPHERAL;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(AddPeripheral_t) + sizeof(double) * 2;

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // Latitude
    int index = 0;
    double *p = (double *)&msg->pData[index];
    *p = latitude;
    index += sizeof(double);

    // longitude
    p = (double *)&msg->pData[index];
    *p = longitude;
    index += sizeof(double);

    pPeripheral = (AddPeripheral_t *)&msg->pData[index];

    // DeviceName
    len = strlen(deviceName);
    if (len >= sizeof(pPeripheral->szDeviceName))
    {
        len = sizeof(pPeripheral->szDeviceName) - 1;
    }
    memcpy(pPeripheral->szDeviceName, deviceName, len);

    // FWVersion
    if (fwv)
    {
        len = strlen(fwv);
        if (len >= sizeof(pPeripheral->FWVersion))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        memcpy(pPeripheral->FWVersion, fwv, len);
    }

    // SecurityKey
    if (seckey)
    {
        if (seckeylen > sizeof(pPeripheral->szSecurityKey))
        {
            dxMemFree(msg->pData);
            msg->pData = NULL;
            dxMemFree(msg);
            msg = NULL;
            return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
        }
        pPeripheral->nSecurityKeyLen = seckeylen;
        memcpy(pPeripheral->szSecurityKey, seckey, seckeylen);
    }

    // MAC
    memcpy(&msg->pData[index + offsetof(AddPeripheral_t, MAC)], macaddr, macaddrlen);

    // Others Numeric Data Type
    pPeripheral->deviceType = deviceType;
    pPeripheral->PSignalStr = signal;
    pPeripheral->PBattery = battery;

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetDataContainerByID_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont,
                                                                  dxContFormat_t format)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETDATACONTAINERBYID;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 8 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     *                                | format          | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(uint32_t) + sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_ALL);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    // format
    dxContFormat_t *pFormat = (dxContFormat_t *)&msg->pData[index];
    *pFormat = format;
    index += sizeof(uint32_t);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_CreateContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_CREATECONTAINERMDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_ALL);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdateContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATECONTAINERMDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_DIRTYFLAG);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_InsertContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_INSERTCONTAINERDDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_ALL);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_UpdateContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont,
                                                                  uint32_t advObjID,
                                                                  DKServer_Update_DataPayload_Target_Info updateTo)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_UPDATECONTAINERDDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 10 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     *                                | updateTo        | 2    |
     *                                +-----------------+------+
     *                                | advObjID        | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(uint16_t) + sizeof(uint32_t) + sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_DIRTYFLAG);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    // updateTo
    uint16_t n = (uint16_t)updateTo;
    memcpy(&msg->pData[index], &n, sizeof(n));
    index += sizeof(n);

    // advObjID
    memcpy(&msg->pData[index], &advObjID, sizeof(advObjID));
    index += sizeof(advObjID);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_DeleteContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_DELETECONTAINERDDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_ALL);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_DeleteContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont)
{
    //DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKRESTCommMsg_t *msg = NULL;
    //int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pCont == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_DELETECONTAINERMDATA;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +-----------------+------+
     *                                | dxContainer_t * | 4    |
     *                                +-----------------+------+
     */
    msg->Header.DataLen = sizeof(pCont);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    int index = 0;

    // dxContainer_t
    dxContainer_t *p = dxContainer_Duplicate(pCont, DXCONT_COPY_OPTION_ALL);
    memcpy(&msg->pData[index], &p, sizeof(p));      // NOTE: "&p" (We need to copy the address of p, not content of p)
    index += sizeof(pCont);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetCurrentUser_Asynch(uint16_t SourceOfOrigin,
                                                            uint64_t TaskIdentificationNumber)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETCURRENTUSER;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = 0;

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_DeleteGateway_Asynch(uint16_t SourceOfOrigin,
                                                           uint64_t TaskIdentificationNumber,
                                                           uint8_t EraseAllHistoryData,
                                                           uint8_t RemoveRegionIfEmpty)
{
    DKRESTCommMsg_t *msg = NULL;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    // Allocate memory
    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_DELETEGATEWAY;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    msg->Header.DataLen = sizeof(EraseAllHistoryData) + sizeof(RemoveRegionIfEmpty);

    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    // EraseAllHistoryData
    int index = 0;

    memcpy(&msg->pData[index], &EraseAllHistoryData, sizeof(EraseAllHistoryData));
    index += sizeof(EraseAllHistoryData);

    // RemoveRegionIfEmpty
    memcpy(&msg->pData[index], &RemoveRegionIfEmpty, sizeof(RemoveRegionIfEmpty));
    index += sizeof(RemoveRegionIfEmpty);

    // Push to queue
    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetPushRTMPLiveURL_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID)
{
    DKRESTCommMsg_t *msg = NULL;
    PeripheralObject_t* pPeripheral;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    /*
     *     Header.DataLen           = 4 Bytes
     *     pData                    = +--------------+------+
     *                                | ObjectID     | 4    |
     *                                +--------------+------+
     */

    msg->Header.MessageType = DKMSG_GETPUSHRTMPLIVEURL;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;
    msg->Header.DataLen = sizeof(PeripheralObject_t);
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, sizeof (PeripheralObject_t));
    pPeripheral = (PeripheralObject_t *)msg->pData;
    memcpy(&msg->pData[offsetof(PeripheralObject_t, ObjectID)], ObjectID, sizeof (uint32_t));

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetEventMediaFileUploadPath_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, IPCamEventFileREQ_t *pIPCamEventData)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pIPCamEventData == NULL ||
        pIPCamEventData->FileFormat == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETEVENTMEDIAFILEUPLOADPATH;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 32 Bytes
     *     pData                    = +-----------------+------+
     *                                | ObjectID        | 4    |
     *                                +-----------------+------+
     *                                | Type            | 1    |
     *                                +-----------------+------+
     *                                | Reserved        | 3    |
     *                                +-----------------+------+
     *                                | FileFormat      | 16   |
     *                                +-----------------+------+
     *                                | DateTime        | 4    |
     *                                +-----------------+------+
     *                                | AdvObjID        | 4    |
     *                                +-----------------+------+
     */

    msg->Header.DataLen = sizeof(IPCamEventFileREQ_t);
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memcpy(&msg->pData[0], pIPCamEventData, msg->Header.DataLen);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetOfflineMediaFileUploadPath_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, IPCamOfflineFile_t *pIPCamEventData)
{
    DKRESTCommMsg_t *msg = NULL;
    int len = 0;

    if (DKSManagerState != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        return DKSERVER_STATUS_NOT_READY;
    }

    if (pIPCamEventData == NULL ||
        pIPCamEventData->FileFormat == NULL ||
        pIPCamEventData->RecordStartTime == 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    msg = (DKRESTCommMsg_t *)dxMemAlloc(CALLOC_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));

        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    msg->Header.MessageType = DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH;
    msg->Header.SourceOfOrigin = SourceOfOrigin;
    msg->Header.TaskIdentificationNumber = TaskIdentificationNumber;

    /*
     *     Header.DataLen           = 24 Bytes
     *     pData                    = +-----------------+------+
     *                                | ObjectID        | 4    |
     *                                +-----------------+------+
     *                                | FileFormat      | 16   |
     *                                +-----------------+------+
     *                                | RecordStartTime | 4    |
     *                                +-----------------+------+
     */

    msg->Header.DataLen = sizeof(IPCamOfflineFile_t);
    msg->pData = dxMemAlloc(CALLOC_NAME, 1, msg->Header.DataLen + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         len + 1);

        dxMemFree(msg);
        msg = NULL;
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memcpy(&msg->pData[0], pIPCamEventData, msg->Header.DataLen);

    return _DKSMPushMessageToQueue(&msg);
}

DKServer_StatusCode_t DKServerManager_GetCentralName(uint8_t *DeviceName, int DeviceNameSize)
{
    DKServer_StatusCode_t ret = DKSERVER_STATUS_ERROR_UNDEFINED;
    DK_BLEGateway_t *pGateway = NULL;

    ObjectDictionary_Lock ();

    pGateway = ObjectDictionary_GetGateway ();

    if (pGateway == NULL)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ret = DKSERVER_STATUS_SUCCESS;
    if (pGateway->pDeviceName)
    {
        if (strlen((char *)pGateway->pDeviceName) >= DeviceNameSize)
        {
            ret = DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
        }
        else
        {
            memset(DeviceName, 0x00, DeviceNameSize);
            memcpy(DeviceName, pGateway->pDeviceName, strlen((char *)pGateway->pDeviceName));
        }
    }

    ObjectDictionary_Unlock ();

    return ret;
}

DKServer_StatusCode_t DKServerManager_GetTotalPeripheralNumbers(int *count)
{
    if (count == NULL) return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    ObjectDictionary_Lock ();
    *count = ObjectDictionary_GetPeripheralCount ();
    ObjectDictionary_Unlock ();

    return DKSERVER_STATUS_SUCCESS;
}

DKServer_StatusCode_t DKServerManager_GetPeripheralByIndex(int index, uint8_t *macaddr, int macaddrlen, uint16_t* deviceType)
{
    DK_BLEPeripheral_t *pPeripheral = NULL;

    ObjectDictionary_Lock ();
    int count = ObjectDictionary_GetPeripheralCount ();
    ObjectDictionary_Unlock ();

    if (macaddr == NULL || macaddrlen != 8 || index >= count)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    pPeripheral = ObjectDictionary_GetPeripheralByIndex (index);

    if (pPeripheral == NULL)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    memcpy(macaddr, pPeripheral->MACAddress, macaddrlen);
    if (deviceType) {
        *deviceType = pPeripheral->DeviceType;
    }

    ObjectDictionary_Unlock ();

    return DKSERVER_STATUS_SUCCESS;
}

DKServer_StatusCode_t DKServerManager_GetPeripheralNameByIndex(int index, uint8_t *DeviceName, int DeviceNameSize)
{
    DKServer_StatusCode_t ret = DKSERVER_STATUS_ERROR_UNDEFINED;
    DK_BLEPeripheral_t *pPeripheral = NULL;

    ObjectDictionary_Lock ();
    int count = ObjectDictionary_GetPeripheralCount ();
    ObjectDictionary_Unlock();

    if (DeviceName == NULL ||
        DeviceNameSize <= 0 ||
        index >= count)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;;
    }

    ObjectDictionary_Lock ();

    pPeripheral = ObjectDictionary_GetPeripheralByIndex (index);

    if (pPeripheral == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ret = DKSERVER_STATUS_SUCCESS;
    if (pPeripheral->pDeviceName)
    {
        if (strlen((char *)pPeripheral->pDeviceName) >= DeviceNameSize)
        {
            ret = DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
        }
        else
        {
            memset(DeviceName, 0x00, DeviceNameSize);
            memcpy(DeviceName, pPeripheral->pDeviceName, strlen((char *)pPeripheral->pDeviceName));
        }
    }

    ObjectDictionary_Unlock ();

    return ret;
}

DKServer_StatusCode_t DKServerManager_ChangePeripheralDeviceName(uint32_t objectID, uint8_t *pDeviceName)
{
    if (objectID == 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    int ret = ObjectDictionary_ChangePeripheralDeviceName (objectID, pDeviceName);
    if (ret == 0)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Unlock ();

    return DKSERVER_STATUS_SUCCESS;
}

uint16_t DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL)
    {
        return 0;
    }

    return pEventFileREP->UploadURL_Length;
}

uint8_t *DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL ||
        pEventFileREP->UploadURL_Length == 0)
    {
        return 0;
    }

    return &pEventFileREP->pPayload[pEventFileREP->UploadURL_Offset];
}

uint16_t DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL)
    {
        return 0;
    }

    return pEventFileREP->DownloadURL_Length;
}

uint8_t *DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL ||
        pEventFileREP->DownloadURL_Length == 0)
    {
        return 0;
    }

    return &pEventFileREP->pPayload[pEventFileREP->DownloadURL_Offset];
}

uint16_t DKServerManager_GetEventMediaFileUploadPath_MessageData_Length(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL)
    {
        return 0;
    }

    return pEventFileREP->MessageData_Length;
}

uint8_t *DKServerManager_GetEventMediaFileUploadPath_MessageData_GetData(IPCamEventFileREP_t *pEventFileREP)
{
    if (pEventFileREP == NULL ||
        pEventFileREP->MessageData_Length == 0)
    {
        return 0;
    }

    return &pEventFileREP->pPayload[pEventFileREP->MessageData_Offset];
}

DKServer_StatusCode_t DKServerManager_GetPeripheralSecurityKeyByIndex(int index, uint8_t *seckey, uint8_t *seckeylen)
{
    DK_BLEPeripheral_t *pPeripheral = NULL;

    ObjectDictionary_Lock ();

    int count = ObjectDictionary_GetPeripheralCount ();

    if (seckey == NULL || seckeylen == 0 || index >= count)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    pPeripheral = ObjectDictionary_GetPeripheralByIndex (index);
    if (pPeripheral == NULL)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    if (*seckeylen < pPeripheral->nSecurityKeyLen)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(seckey, 0x00, *seckeylen);
    memcpy(seckey, pPeripheral->szSecurityKey, pPeripheral->nSecurityKeyLen);
    *seckeylen = pPeripheral->nSecurityKeyLen;

    ObjectDictionary_Unlock ();

    return DKSERVER_STATUS_SUCCESS;
}

DKServerManager_StateMachine_State_t DKServerManager_GetCurrentState(void)
{
    return DKSManagerState;
}

uint32_t DKServerManager_GetAvailableTaskQueue(void)
{
    uint32_t ret = 0;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    if (pThreadParam->Queue)
    {
        ret = dxQueueSpacesAvailable(pThreadParam->Queue);
    }

    return ret;
}

uint8_t *DKServerManager_GetFirmwareItem(DKFirmare_Info_ID_t nID, uint8_t *pData)
{
    int i = 0;
    int nTotalLen = 0;
    uint8_t *p = NULL;


    if (pData == NULL) return NULL;

    nTotalLen = (pData[0] << 8) | pData[1];

    while (i < nTotalLen)
    {
        if (pData[i + 2] == nID)
        {
            p = &pData[i + 4];          // pData[i + 4] => DATA1
            break;
        }

        i += 2 + pData[i + 3];          //  pData[i + 3] => ITEM_LEN1
    }

    return p;
}

DKServer_StatusCode_t DKServerManager_FirmwareVersionSplit(uint8_t *pszVersion, uint32_t *pnMajor, uint32_t *pnMinor, uint32_t *pnBuild)
{
    DKServer_StatusCode_t ret = DKSERVER_STATUS_SUCCESS;
    int i = 0;
    int len = 0;
    int nPart = 0;
    uint8_t ch = 0;
    uint32_t value = 0;

    if (pszVersion == NULL ||
        pnMajor == NULL ||
        pnMinor == NULL ||
        pnBuild == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    len = strlen((char *)pszVersion);
    for (i = 0; i < len; i++)
    {
        ch = pszVersion[i];
        if (isdigit((int)ch))
        {
            value = value * 10 + (ch - '0');
        }
        else if (ch == '.')
        {
            if (nPart == 0)
            {
                *pnMajor = value;
            }
            else if (nPart == 1)
            {
                *pnMinor = value;
            }
            else
            {
                ret = DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
                break;
            }

            value = 0;
            nPart++;
        }
        else
        {
            ret = DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
            break;
        }
    }

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        *pnBuild = value;
    }
    else
    {
        *pnMajor = 0;
        *pnMinor = 0;
        *pnBuild = 0;
    }

    return ret;
}

DKServer_StatusCode_t DKServerManager_FirmwareVersionCompare(uint8_t *pszVerRuning, uint8_t *pszVerDownload)
{
    DKServer_StatusCode_t ret = DKSERVER_STATUS_SUCCESS;
    uint32_t verrunMajor = 0;
    uint32_t verrunMinor = 0;
    uint32_t verrunBuild = 0;
    uint32_t verDownloadMajor = 0;
    uint32_t verDownloadMinor = 0;
    uint32_t verDownloadBuild = 0;

    if (pszVerRuning == NULL || pszVerDownload == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ret = DKServerManager_FirmwareVersionSplit(pszVerRuning, &verrunMajor, &verrunMinor, &verrunBuild);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    ret = DKServerManager_FirmwareVersionSplit(pszVerDownload, &verDownloadMajor, &verDownloadMinor, &verDownloadBuild);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    if (verDownloadMajor < verrunMajor)
    {
        return DKSERVER_STATUS_FIRMWARE_VERSION_ERROR;
    }
    else if (verDownloadMajor > verrunMajor)
    {
        return DKSERVER_STATUS_SUCCESS;
    }

    if (verDownloadMinor < verrunMinor)
    {
        return DKSERVER_STATUS_FIRMWARE_VERSION_ERROR;
    }
    else if (verDownloadMinor > verrunMinor)
    {
        return DKSERVER_STATUS_SUCCESS;
    }

    if (verDownloadBuild < verrunBuild)
    {
        return DKSERVER_STATUS_FIRMWARE_VERSION_ERROR;
    }
    else if (verDownloadBuild > verrunBuild)
    {
        return DKSERVER_STATUS_SUCCESS;
    }

    return DKSERVER_STATUS_FIRMWARE_VERSION_EQUAL;
}

uint8_t DKServerManager_IsValidPeripheralObjectID(uint32_t nObjectID)
{
    ObjectDictionary_Lock ();
    DK_BLEPeripheral_t *pPeripheral = ObjectDictionary_GetPeripheral (nObjectID);
    ObjectDictionary_Unlock ();

    return (pPeripheral == NULL) ? 0 : 1;
}

DKServer_StatusCode_t DKServerManager_GetGatewayFirmwareVersion(uint8_t *wifiversion, int wifiversionlen, uint8_t *bleversion, int blelen)
{
    DK_BLEGateway_t *pGateway = NULL;

    if (wifiversion == NULL ||
        bleversion == NULL)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    pGateway = ObjectDictionary_GetGateway ();

    if (pGateway == NULL)
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pGateway->WiFiVersion) > wifiversionlen ||
        strlen((char *)pGateway->BLEVersion) > blelen )
    {
        ObjectDictionary_Unlock ();
        return DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
    }

    memset(wifiversion, 0x00, wifiversionlen);
    memset(bleversion, 0x00, blelen);

    memcpy((char *)wifiversion, (char *)pGateway->WiFiVersion, strlen((char *)pGateway->WiFiVersion));
    memcpy((char *)bleversion, (char *)pGateway->BLEVersion, strlen((char *)pGateway->BLEVersion));

    ObjectDictionary_Unlock ();

    return DKSERVER_STATUS_SUCCESS;
}

DKServer_StatusCode_t DKServerManager_GetPeripheralFirmwareVersionByMAC(uint8_t *phversion, int phlen, uint8_t *macaddr, int macaddrlen)
{
    DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DK_BLEPeripheral_t *pPeripheral = NULL;
    int nPeripheralCount = 0;
    int i = 0;
    int bFound = 0;
    int fwlen = 0;

    if (phversion == NULL ||
        macaddr == NULL ||
        macaddrlen != 8)
    {
        return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();

    nPeripheralCount = ObjectDictionary_GetPeripheralCount ();

    for (i = 0; i < nPeripheralCount; i++)
    {
        pPeripheral = ObjectDictionary_GetPeripheralByIndex (i);
        if (pPeripheral == NULL)
        {
            continue;
        }

        if (memcmp(pPeripheral->MACAddress, macaddr, macaddrlen) == 0)
        {
            fwlen = strlen(pPeripheral->FWVersion);
            if (phlen < fwlen)
            {
                result = DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL;
            }
            else
            {
                memset(phversion, 0x00, phlen);
                strncpy((char *)phversion, pPeripheral->FWVersion, fwlen);
            }
            bFound = 1;
            break;
        }
    }

    ObjectDictionary_Unlock ();

    if (bFound == 0)
    {
        result = DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    }

    return result;
}

DKServer_StatusCode_t DKServerManager_ConfigGetJobInterval(uint32_t NewGetJobIntervalms)
{
    DKServer_StatusCode_t result = DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t *)&DKThreadParam;

    if (pThreadParam == NULL) {
        goto fail;
    }

    if (pThreadParam->GetJobInterval == NewGetJobIntervalms) {
        goto fail;
    }

    LOCK(&pThreadParam->mutex);

    if (NewGetJobIntervalms >= 1000 ||
        NewGetJobIntervalms == 0)
    {
        pThreadParam->GetJobInterval = NewGetJobIntervalms;
        result = DKSERVER_STATUS_SUCCESS;
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Change time interval to %d\r\n",
                          NewGetJobIntervalms);
    }
    else //If invalid parameter we set back to default for safety precausion.
    {
        pThreadParam->GetJobInterval = SRVM_THREAD_REQUEST_JOB_TIME_INTERVAL_DEFAULT;
    }

    UNLOCK(&pThreadParam->mutex);

fail :

    return result;
}

UTCLocalTimeTable_t *DKServerManager_GetUTCLocalTimeTable(void)
{
    return &UTCLocalTimeTable;
}


DKServer_StatusCode_t DKServerManager_GetHostname( char **pHostname )
{
    DKServer_StatusCode_t result = DKSERVER_STATUS_INVALID_LOCAL_PARAMETER;

    if ( pHostname ) {
        char *hostname;

        ObjectDictionary_Lock ();

        hostname = ObjectDictionary_GetHostname ();

        if ( hostname ) {
            *pHostname = ( char * )dxMemAlloc( "", 1, ( strlen( hostname ) + 1 ) );

            if ( *pHostname ) {
                memcpy( *pHostname, hostname, strlen( hostname ) );
                result = DKSERVER_STATUS_SUCCESS;
            } else {
                result = DKSERVER_STATUS_ERROR_ALLOC_MEMORY;
            }
        } else {
            result = DKSERVER_STATUS_NOT_READY;
        }

        ObjectDictionary_Unlock ();
    }

    return result;
}


dxBOOL DKServerManager_IsJobPollingEnabled (void) {
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t*) &DKThreadParam;

    return pThreadParam->JobPolling;
}


DKServer_StatusCode_t DKServerManager_SetJobPolling (
    dxBOOL jobPolling
) {
    DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    DKSMThreadParam_t *pThreadParam = (DKSMThreadParam_t*) &DKThreadParam;

    LOCK (&pThreadParam->mutex);

    pThreadParam->JobPolling = jobPolling;

    UNLOCK (&pThreadParam->mutex);

    return result;
}
