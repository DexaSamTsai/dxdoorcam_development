 //============================================================================
// File: DKMSG_Handler.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.142.3
//     Date: 2015/05/28
//     1) Description: Support GetAdvJobCondition API
//        Modified:
//            - Added entry to DKServerAPITable[]
//
//     Version: 0.171.0
//     Date: 2015/07/21
//     1) Description: Support advanced job single gateway pack
//        Modified:
//            - Changed filename to DKAPI_GetAdvJobByGateway.h
//            - Modified DKServerAPITable[]
//
//     Version: 0.176
//     Date: 2015/09/10
//     1) Description: Support AddSmartHomeCentralLog API
//        Added:
//            - A new entry into DKServerAPITable[]
//
//     Version: 0.177
//     Date: 2016/01/11
//     1) Description: Support AddStandalonePeripheral API
//        Modified:
//            - Added new entry to DKServerAPITable[]
//
//     Version: 0.177.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.177.2
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - DKServerAPITable[]
//            - DKMSG_CreateRequestMessage()
//            - DKMSG_ParseResponseMessage()
//
//     Version: 0.177.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - DKMSG_VerifyDKServerResponse()
//
//     Version: 0.177.4
//     Date: 2016/03/04
//     1) Description: Fix a firmware download issue if "Content-Length" is less then 160 bytes
//                     The response header length is around 162 bytes
//        Modified:
//            - DKMSG_ParseResponseMessage()
//
//     Version: 0.177.5
//     Date: 2016/06/17
//     1) Description: Added including "dxBSP.h" to get the definition of DK_BSP_RTK_HOMEKIT_ENABLE
//
//     Version: 0.242
//     Date: 2017/04/19
//     1) Description: Support GetCurrentUser API
//        Added:
//            - define SUPPORT_DKAPI_GETCURRENTUSER
//        Modified:
//            - DKServerAPITable[]
//
//     Version: 0.242.1
//     Date: 2017/05/08
//     1) Description: Support DeleteGateway API
//        Added:
//            - define SUPPORT_DKAPI_DELETEGATEWAY
//        Modified:
//            - DKServerAPITable[]
//
//     Version: 0.242.2
//     Date: 2017/08/15
//     1) Description: Support the following IPCam API
//                     PushRTMPLive
//                     GetEventMediaFileUploadPath
//                     GetOfflineMediaFileUploadPath
//
//     Version: 0.255.2
//     Date: 2017/09/12
//     1) Description: Support a new API, SendMessageWithKeyNum and keep SendMessageWithKey for backward compatible.
//        Modified:
//            - DKServerManager_SendMessageWithKey_Asynch()
//            - DKServerManager_SendMessageWithKeyNum_Asynch()
//
//     Version: 0.255.3
//     Date: 2017/09/27
//     1) Description: Support login with "DKAuth"
//        Modified:
//            - DKServerAPITable[]
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "dk_hex.h"
#include "Utils.h"
#include "DKMSG_Handler.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dkjsn.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "dk_Network.h"
#include "dk_url.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"


#if DK_BSP_RTK_HOMEKIT_ENABLE
  #define SUPPORT_DKAPI_ADDGATEWAY                                  0
  #define SUPPORT_DKAPI_ADDPERIPHERAL                               0
  #define SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID                     0
  #define SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY                  0
  #define SUPPORT_DKAPI_DELETESCHEDULEJOB                           0
  #define SUPPORT_DKAPI_DOWNLOADFILE                                1
  #define SUPPORT_DKAPI_GATEWAYLOGIN                                1
  #define SUPPORT_DKMSG_ANONYMOUSLOGIN                              1
  #define SUPPORT_DKAPI_GETLASTFIRMWAREINFO                         1
  #define SUPPORT_DKAPI_GETNEWJOBS                                  1
  #define SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS                  1
  #define SUPPORT_DKAPI_GETSCHEDULEJOB                              0
  #define SUPPORT_DKAPI_GETSERVERINFO                               0
  #define SUPPORT_DKAPI_GETSERVERTIME                               0
  #define SUPPORT_DKAPI_JOBDONEREPORT                               1
  #define SUPPORT_DKAPI_RENEWSESSIONTOKEN                           1
  #define SUPPORT_DKAPI_SENDMESSAGE                                 0
  #define SUPPORT_DKAPI_SENDMESSAGEWITHKEY                          0
  #define SUPPORT_DKAPI_NOTIFYEVENTDONE                             0
  #define SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION                0
  #define SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY                    0
  #define SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID            0
  #define SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA             0
  #define SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD                 0
  #define SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY        0
  #define SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE                    0
  #define SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO                  0
  #define SUPPORT_DKAPI_UPDATEADVJOBSTATUS                          0
  #define SUPPORT_DKAPI_GETADVJOBBYGATEWAY                          0
  #define SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG                      0
  #define SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL                     0
  #define SUPPORT_DKAPI_GETDATACONTAINERBYID                        0
  #define SUPPORT_DKAPI_CREATECONTAINERMDATA                        0
  #define SUPPORT_DKAPI_UPDATECONTAINERMDATA                        0
  #define SUPPORT_DKAPI_INSERTCONTAINERDDATA                        0
  #define SUPPORT_DKMSG_UPDATECONTAINERDDATA                        0
  #define SUPPORT_DKAPI_DELETECONTAINERDDATA                        0
  #define SUPPORT_DKAPI_DELETECONTAINERMDATA                        0
  #define SUPPORT_DKAPI_GETCURRENTUSER                              0
  #define SUPPORT_DKAPI_DELETEGATEWAY                               0
  #define SUPPORT_DKMSG_GETPUSHRTMPLIVEURL                          0
  #define SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH                 0
  #define SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH               0
#else // DK_BSP_RTK_HOMEKIT_ENABLE
  #define SUPPORT_DKAPI_ADDGATEWAY                                  1
  #define SUPPORT_DKAPI_ADDPERIPHERAL                               1
  #define SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID                     1
  #define SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY                  1
  #define SUPPORT_DKAPI_DELETESCHEDULEJOB                           1
  #define SUPPORT_DKAPI_DOWNLOADFILE                                1
  #define SUPPORT_DKAPI_GATEWAYLOGIN                                1
  #define SUPPORT_DKMSG_ANONYMOUSLOGIN                              1
  #define SUPPORT_DKAPI_GETLASTFIRMWAREINFO                         1
  #define SUPPORT_DKAPI_GETNEWJOBS                                  1
  #define SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS                  1
  #define SUPPORT_DKAPI_GETSCHEDULEJOB                              1
  #define SUPPORT_DKAPI_GETSERVERINFO                               1
  #define SUPPORT_DKAPI_GETSERVERTIME                               1
  #define SUPPORT_DKAPI_JOBDONEREPORT                               1
  #define SUPPORT_DKAPI_RENEWSESSIONTOKEN                           1
  #define SUPPORT_DKAPI_SENDMESSAGE                                 1
  #define SUPPORT_DKAPI_SENDMESSAGEWITHKEY                          1
  #define SUPPORT_DKAPI_NOTIFYEVENTDONE                             1
  #define SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION                1
  #define SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY                    1
  #define SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID            1
  #define SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA             1
  #define SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD                 1
  #define SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY        1
  #define SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE                    1
  #define SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO                  1
  #define SUPPORT_DKAPI_UPDATEADVJOBSTATUS                          1
  #define SUPPORT_DKAPI_GETADVJOBBYGATEWAY                          1
  #define SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG                      1
  #define SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL                     1
  #define SUPPORT_DKAPI_GETDATACONTAINERBYID                        1
  #define SUPPORT_DKAPI_CREATECONTAINERMDATA                        1
  #define SUPPORT_DKAPI_UPDATECONTAINERMDATA                        1
  #define SUPPORT_DKAPI_INSERTCONTAINERDDATA                        1
  #define SUPPORT_DKMSG_UPDATECONTAINERDDATA                        1
  #define SUPPORT_DKAPI_DELETECONTAINERDDATA                        1
  #define SUPPORT_DKAPI_DELETECONTAINERMDATA                        1
  #define SUPPORT_DKAPI_GETCURRENTUSER                              1
  #define SUPPORT_DKAPI_DELETEGATEWAY                               1
  #define SUPPORT_DKMSG_GETPUSHRTMPLIVEURL                          1
  #define SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH                 1
  #define SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH               1
#endif // DK_BSP_RTK_HOMEKIT_ENABLE

#if SUPPORT_DKAPI_ADDGATEWAY
#include "DKAPI_AddGateway.h"
#endif // SUPPORT_DKAPI_ADDGATEWAY

#if SUPPORT_DKAPI_ADDPERIPHERAL
#include "DKAPI_AddPeripheral.h"
#endif // SUPPORT_DKAPI_ADDPERIPHERAL

#if SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID
#include "DKAPI_ChangeGatewayStatusByID.h"
#endif // SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID

#if SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY
#include "DKAPI_ClearPeripheralSecurityKey.h"
#endif // SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY

#if SUPPORT_DKAPI_DELETESCHEDULEJOB
#include "DKAPI_DeleteScheduleJob.h"
#endif // SUPPORT_DKAPI_DELETESCHEDULEJOB

#if SUPPORT_DKAPI_DOWNLOADFILE
#include "DKAPI_DownloadFile.h"
#endif // SUPPORT_DKAPI_DOWNLOADFILE

#if SUPPORT_DKAPI_GATEWAYLOGIN || SUPPORT_DKMSG_ANONYMOUSLOGIN
#include "DKAPI_GatewayLogin.h"
#endif // SUPPORT_DKAPI_GATEWAYLOGIN || SUPPORT_DKMSG_ANONYMOUSLOGIN

#if SUPPORT_DKAPI_GETLASTFIRMWAREINFO
#include "DKAPI_GetLastFirmwareInfo.h"
#endif // SUPPORT_DKAPI_GETLASTFIRMWAREINFO

#if SUPPORT_DKAPI_GETNEWJOBS
#include "DKAPI_GetNewJobs.h"
#endif // SUPPORT_DKAPI_GETNEWJOBS

#if SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS
#include "DKAPI_GetPeripheralByGMACAddress.h"
#endif // SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS

#if SUPPORT_DKAPI_GETSCHEDULEJOB
#include "DKAPI_GetScheduleJob.h"
#endif // SUPPORT_DKAPI_GETSCHEDULEJOB

#if SUPPORT_DKAPI_GETSERVERINFO
#include "DKAPI_GetServerInfo.h"
#endif // SUPPORT_DKAPI_GETSERVERINFO

#if SUPPORT_DKAPI_GETSERVERTIME
#include "DKAPI_GetServerTime.h"
#endif // SUPPORT_DKAPI_GETSERVERTIME

#if SUPPORT_DKAPI_JOBDONEREPORT
#include "DKAPI_JobDoneReport.h"
#endif // SUPPORT_DKAPI_JOBDONEREPORT

#if SUPPORT_DKAPI_RENEWSESSIONTOKEN
#include "DKAPI_RenewSessionToken.h"
#endif // SUPPORT_DKAPI_RENEWSESSIONTOKEN

#if SUPPORT_DKAPI_SENDMESSAGE
#include "DKAPI_SendMessage.h"
#endif // SUPPORT_DKAPI_SENDMESSAGE

#if SUPPORT_DKAPI_SENDMESSAGEWITHKEY
#include "DKAPI_SendMessageWithKey.h"
#include "DKAPI_SendMessageWithKeyNum.h"
#endif // SUPPORT_DKAPI_SENDMESSAGEWITHKEY

#if SUPPORT_DKAPI_NOTIFYEVENTDONE
#include "DKAPI_NotifyEventDone.h"
#endif // SUPPORT_DKAPI_NOTIFYEVENTDONE

#if SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION
#include "DKAPI_SetPeripheralQueryCollection.h"
#endif // SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION

#if SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY
#include "DKAPI_SetPeripheralSecurityKey.h"
#endif // SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY

#if SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID
#include "DKAPI_UpdateGatewayFirmwareVersionByID.h"
#endif // SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID

#if SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA
#include "DKAPI_UpdatePeripheralAggregationData.h"
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA

#if SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD
#include "DKAPI_UpdatePeripheralDataPayload.h"
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD

#if SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY
#include "DKAPI_UpdatePeripheralDataPayloadToHistory.h"
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY

#if SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE
#include "DKAPI_UpdatePeripheralFirmware.h"
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE

#if SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO
#include "DKAPI_UpdatePeripheralStatusInfo.h"
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO

#if SUPPORT_DKAPI_UPDATEADVJOBSTATUS
#include "DKAPI_UpdateAdvJobStatus.h"
#endif // SUPPORT_DKAPI_UPDATEADVJOBSTATUS

#if SUPPORT_DKAPI_GETADVJOBBYGATEWAY
#include "DKAPI_GetAdvJobByGateway.h"
#endif // SUPPORT_DKAPI_GETADVJOBBYGATEWAY

#if SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG
#include "DKAPI_AddSmartHomeCentralLog.h"
#endif // SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG

#if SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL
#include "DKAPI_AddStandalonePeripheral.h"
#endif // SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL

#if SUPPORT_DKAPI_GETDATACONTAINERBYID
#include "DKAPI_GetDataContainerByID.h"
#endif // SUPPORT_DKAPI_GETDATACONTAINERBYID

#if SUPPORT_DKAPI_CREATECONTAINERMDATA
#include "DKAPI_CreateContainerMData.h"
#endif // SUPPORT_DKAPI_CREATECONTAINERMDATA

#if SUPPORT_DKAPI_UPDATECONTAINERMDATA
#include "DKAPI_UpdateContainerMData.h"
#endif // SUPPORT_DKAPI_UPDATECONTAINERMDATA

#if SUPPORT_DKAPI_INSERTCONTAINERDDATA
#include "DKAPI_InsertContainerDData.h"
#endif // SUPPORT_DKAPI_INSERTCONTAINERDDATA

#if SUPPORT_DKMSG_UPDATECONTAINERDDATA
#include "DKAPI_UpdateContainerDData.h"
#endif // SUPPORT_DKAPI_UPDATECONTAINERDATA

#if SUPPORT_DKAPI_DELETECONTAINERDDATA
#include "DKAPI_DeleteContainerDData.h"
#endif // SUPPORT_DKAPI_DELETECONTAINERDDATA

#if SUPPORT_DKAPI_DELETECONTAINERMDATA
#include "DKAPI_DeleteContainerMData.h"
#endif // SUPPORT_DKAPI_DELETECONTAINERMDATA

#if SUPPORT_DKAPI_GETCURRENTUSER
#include "DKAPI_GetCurrentUser.h"
#endif // SUPPORT_DKAPI_GETCURRENTUSER

#if SUPPORT_DKAPI_DELETEGATEWAY
#include "DKAPI_DeleteGateway.h"
#endif // SUPPORT_DKAPI_DELETEGATEWAY

#if SUPPORT_DKMSG_GETPUSHRTMPLIVEURL
#include "DKAPI_GetPushRTMPLiveURL.h"
#endif // SUPPORT_DKMSG_GETPUSHRTMPLIVEURL

#if SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH
#include "DKAPI_GetEventMediaFileUploadPath.h"
#endif // SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH

#if SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH
#include "DKAPI_GetOfflineMediaFileUploadPath.h"
#endif // SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern dxOS_RET_CODE DKServerManager_SendEventIDToWorkerThread(uint16_t src, uint64_t taskid, DKMSG_Command_t msg, uint16_t httpcode, uint16_t dkcode);

/******************************************************
 *               Variable Definitions
 ******************************************************/
static DKServerAPI_t    DKServerAPITable[] =
{
#if  SUPPORT_DKAPI_ADDGATEWAY
    {
        DKMSG_ADDGATEWAY,
        Build_AddGateway_Data,
        Parse_AddGateway_Response
    },
#endif // SUPPORT_DKAPI_ADDGATEWAY

#if SUPPORT_DKAPI_ADDPERIPHERAL
    {
        DKMSG_ADDPERIPHERAL,
        Build_AddPeripheral_Data,
        Parse_AddPeripheral_Response
    },
#endif // SUPPORT_DKAPI_ADDPERIPHERAL

#if SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID
    {
        DKMSG_CHANGEGATEWAYSTATUS,
        Build_ChangeGatewayStatus_Data,
        Parse_ChangeGatewayStatus_Response
    },
#endif // SUPPORT_DKAPI_CHANGEGATEWAYSTATUSBYID

#if SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY
    {
        DKMSG_CLEARPERIPHERALSECURITYKEY,
        Build_ClearPeripheralSecurityKey_Data,
        Parse_ClearPeripheralSecurityKey_Response
    },
#endif // SUPPORT_DKAPI_CLEARPERIPHERALSECURITYKEY

#if SUPPORT_DKAPI_DELETESCHEDULEJOB
    {
        DKMSG_DELETESCHEDULEJOB,
        Build_DeleteScheduleJob_Data,
        Parse_DeleteScheduleJob_Response
    },
#endif // SUPPORT_DKAPI_DELETESCHEDULEJOB

#if SUPPORT_DKAPI_DOWNLOADFILE
    {
        DKMSG_FIRMWAREDOWNLOAD,
        Build_FirmwareDownload_Data,
        Parse_FirmwareDownload_Response
    },
#endif // SUPPORT_DKAPI_DOWNLOADFILE

#if SUPPORT_DKAPI_GATEWAYLOGIN
    {
        DKMSG_GATEWAYLOGIN,
        Build_GatewayLogin_Data,
        Parse_GatewayLogin_Response
    },
#endif // SUPPORT_DKAPI_GATEWAYLOGIN

#if SUPPORT_DKMSG_ANONYMOUSLOGIN
    {
        DKMSG_ANONYMOUSLOGIN,
        Build_GatewayLogin_Data,
        Parse_GatewayLogin_Response
    },
#endif // SUPPORT_DKMSG_ANONYMOUSLOGIN

#if SUPPORT_DKAPI_GETLASTFIRMWAREINFO
    {
        DKMSG_GETLASTFIRMWAREINFO,
        Build_GetLastFirmwareInfo_Data,
        Parse_GetLastFirmareInfo_Response
    },
#endif // SUPPORT_DKAPI_GETLASTFIRMWAREINFO

#if SUPPORT_DKAPI_GETNEWJOBS
    {
        DKMSG_NEWJOBS,
        Build_GetNewJobs_Data,
        Parse_GetNewJobs_Response
    },
#endif // SUPPORT_DKAPI_GETNEWJOBS

#if SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS
    {
        DKMSG_GETPERIPHERALBYGMACADDRESS,
        Build_GetPeripheralByGMAC_Data,
        Parse_GetPeripheralByGMAC_Response
    },
#endif // SUPPORT_DKAPI_GETPERIPHERALBYGMACADDRESS

#if SUPPORT_DKAPI_GETSCHEDULEJOB
    {
        DKMSG_GETSCHEDULEJOB,
        Build_GetScheduleJob_Data,
        Parse_GetScheduleJob_Response
    },
#endif // SUPPORT_DKAPI_GETSCHEDULEJOB

#if SUPPORT_DKAPI_GETSERVERINFO
    {
        DKMSG_GETSERVERINFO,
        Build_GetServerInfo_Data,
        Parse_GetServerInfo_Response
    },
#endif // SUPPORT_DKAPI_GETSERVERINFO

#if SUPPORT_DKAPI_GETSERVERTIME
    {
        DKMSG_GETSERVERTIME,
        Build_GetServerTime_Data,
        Parse_GetServerTime_Response
    },
#endif // SUPPORT_DKAPI_GETSERVERTIME

#if SUPPORT_DKAPI_JOBDONEREPORT
    {
        DKMSG_JOBDONEREPORT,
        Build_JobDoneReport_Data,
        Parse_JobDoneReport_Response
    },
#endif // SUPPORT_DKAPI_JOBDONEREPORT

#if SUPPORT_DKAPI_RENEWSESSIONTOKEN
    {
        DKMSG_RENEWSESSIONTOKEN,
        Build_RenewSessionToken_Data,
        Parse_RenewSessionToken_Response
    },
#endif // SUPPORT_DKAPI_RENEWSESSIONTOKEN

#if SUPPORT_DKAPI_SENDMESSAGE
    {
        DKMSG_SENDNOTIFICATIONMESSAGE,
        Build_SendNotificationMessage_Data,
        Parse_SendNotificationMessage_Response
    },
#endif // SUPPORT_DKAPI_SENDMESSAGE

#if SUPPORT_DKAPI_SENDMESSAGEWITHKEY
    {
        DKMSG_SENDMESSAGEWITHKEY,
        Build_SendMessageWithKey_Data,
        Parse_SendMessageWithKey_Response
    },
    {
        DKMSG_SENDMESSAGEWITHKEYNUM,
        Build_SendMessageWithKeyNum_Data,
        Parse_SendMessageWithKeyNum_Response
    },
#endif // SUPPORT_DKAPI_SENDMESSAGEWITHKEY

#if SUPPORT_DKAPI_NOTIFYEVENTDONE
    {
        DKMSG_NOTIFYEVENTDONE,
        Build_NotifyEventDone_Data,
        Parse_NotifyEventDone_Response
    },
#endif // SUPPORT_DKAPI_NOTIFYEVENTDONE

#if SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION
    {
        DKMSG_SETPERIPHERALQUERYCOLLECTION,
        Build_SetPeripheralQueryCollection_Data,
        Parse_SetPeripheralQueryCollection_Response
    },
#endif // SUPPORT_DKAPI_SETPERIPHERALQUERYCOLLECTION

#if SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY
    {
        DKMSG_SETPERIPHERALSECURITYKEY,
        Build_SetPeripheralSecurityKey_Data,
        Parse_SetPeripheralSecurityKey_Response
    },
#endif // SUPPORT_DKAPI_SETPERIPHERALSECURITYKEY

#if SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID
    {
        DKMSG_UPDATEGATEWAYFIRMWAREVERSION,
        Build_UpdateGatewayFirmwareVersion_Data,
        Parse_UpdateGatewayFirmwareVersion_Response
    },
#endif // SUPPORT_DKAPI_UPDATEGATEWAYFIRMWAREVERSIONBYID

#if SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA
    {
        DKMSG_UPDATEPERIPHERALAGGREGATIONDATA,
        Build_UpdatePeripheralAggregationData_Data,
        Parse_UpdatePeripheralAggregationData_Response
    },
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALAGGREGATIONDATA

#if SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD
    {
        DKMSG_UPDATEPERIPHERALDATAPAYLOAD,
        Build_UpdatePeripheralDataPayload_Data,
        Parse_UpdatePeripheralDataPayload_Response
    },
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOAD

#if SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY
    {
        DKMSG_UPDATEDATAPAYLOADHISTORY,
        Build_UpdatePeripheralDataPayloadToHistory_Data,
        Parse_UpdatePeripheralDataPayloadToHistory_Response
    },
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALDATAPAYLOADTOHISTORY

#if SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE
    {
        DKMSG_UPDATEPERIPHERALFIRMARE,
        Build_UpdatePeripheralFirmware_Data,
        Parse_UpdatePeripheralFirmware_Response
    },
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALFIRMWARE

#if SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO
    {
        DKMSG_UPDATEPERIPHERALSTATUSINFO,
        Build_UpdatePeripheralStatusInfo_Data,
        Parse_UpdatePeripheralStatusInfo_Response
    },
#endif // SUPPORT_DKAPI_UPDATEPERIPHERALSTATUSINFO

#if SUPPORT_DKAPI_UPDATEADVJOBSTATUS
    {
        DKMSG_UPDATEADVJOBSTATUS,
        Build_UpdateAdvJobStatus_Data,
        Parse_UpdateAdvJobStatus_Response
    },
#endif // SUPPORT_DKAPI_UPDATEADVJOBSTATUS

#if SUPPORT_DKAPI_GETADVJOBBYGATEWAY
    {
        DKMSG_GETADVJOBBYGATEWAY,
        Build_GetAdvJobByGateway_Data,
        Parse_GetAdvJobByGateway_Response
    },
#endif // SUPPORT_DKAPI_GETADVJOBBYGATEWAY

#if SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG
    {
        DKMSG_ADDSMARTHOMECENTRALLOG,
        Build_AddSmartHomeCentralLog_Data,
        Parse_AddSmartHomeCentralLog_Response
    },
#endif // SUPPORT_DKAPI_ADDSMARTHOMECENTRALLOG

#if SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL
    {
        DKMSG_ADDSTANDALONEPERIPHERAL,
        Build_AddStandalonePeripheral_Data,
        Parse_AddStandalonePeripheral_Response
    },
#endif // SUPPORT_DKAPI_ADDSTANDALONEPERIPHERAL

#if SUPPORT_DKAPI_GETDATACONTAINERBYID
    {
        DKMSG_GETDATACONTAINERBYID,
        Build_GetDataContainerByID_Data,
        Parse_GetDataContainerByID_Response
    },
#endif // SUPPORT_DKAPI_GETDATACONTAINERBYID

#if SUPPORT_DKAPI_CREATECONTAINERMDATA
    {
        DKMSG_CREATECONTAINERMDATA,
        Build_CreateContainerMData_Data,
        Parse_CreateContainerMData_Response
    },
#endif // SUPPORT_DKAPI_CREATECONTAINERMDATA

#if SUPPORT_DKAPI_UPDATECONTAINERMDATA
    {
        DKMSG_UPDATECONTAINERMDATA,
        Build_UpdateContainerMData_Data,
        Parse_UpdateContainerMData_Response
    },
#endif // SUPPORT_DKAPI_UPDATECONTAINERMDATA

#if SUPPORT_DKAPI_INSERTCONTAINERDDATA
    {
        DKMSG_INSERTCONTAINERDDATA,
        Build_InsertContainerDData_Data,
        Parse_InsertContainerDData_Response
    },
#endif // SUPPORT_DKAPI_INSERTCONTAINERDDATA

#if SUPPORT_DKMSG_UPDATECONTAINERDDATA
    {
        DKMSG_UPDATECONTAINERDDATA,
        Build_UpdateContainerDData_Data,
        Parse_UpdateContainerDData_Response
    },
#endif // SUPPORT_DKAPI_UPDATECONTAINERDATA

#if SUPPORT_DKAPI_DELETECONTAINERDDATA
    {
        DKMSG_DELETECONTAINERDDATA,
        Build_DeleteContainerDData_Data,
        Parse_DeleteContainerDData_Response
    },
#endif // SUPPORT_DKAPI_DELETECONTAINERDDATA

#if SUPPORT_DKAPI_DELETECONTAINERMDATA
    {
        DKMSG_DELETECONTAINERMDATA,
        Build_DeleteContainerMData_Data,
        Parse_DeleteContainerMData_Response
    },
#endif // SUPPORT_DKAPI_DELETECONTAINERMDATA

#if SUPPORT_DKAPI_GETCURRENTUSER
    {
        DKMSG_GETCURRENTUSER,
        Build_GetCurrentUser_Data,
        Parse_GetCurrentUser_Response
    },
#endif // SUPPORT_DKAPI_GETCURRENTUSER

#if SUPPORT_DKAPI_DELETEGATEWAY
    {
        DKMSG_DELETEGATEWAY,
        Build_DeleteGateway_Data,
        Parse_DeleteGateway_Response
    },
#endif // SUPPORT_DKAPI_DELETEGATEWAY

#if SUPPORT_DKMSG_GETPUSHRTMPLIVEURL
    {
        DKMSG_GETPUSHRTMPLIVEURL,
        Build_GetPushRTMPLiveURL_Data,
        Parse_GetPushRTMPLiveURL_Response
    },
#endif // SUPPORT_DKMSG_GETPUSHRTMPLIVEURL

#if SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH
    {
        DKMSG_GETEVENTMEDIAFILEUPLOADPATH,
        Build_GetEventMediaFileUploadPath_Data,
        Parse_GetEventMediaFileUploadPath_Response
    },
#endif // SUPPORT_DKMSG_GETEVENTMEDIAFILEUPLOADPATH

#if SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH
    {
        DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH,
        Build_GetOfflineMediaFileUploadPath_Data,
        Parse_GetOfflineMediaFileUploadPath_Response
    },
#endif // SUPPORT_DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH
};

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
/**
 * \return >= 0 index of DKServerAPITable[]
 *         -1 not exist
 */
int DKMSG_GetFunctionIndex(int nAPIID)
{
    int i = 0;
    int nItems = sizeof(DKServerAPITable) / sizeof(DKServerAPITable[0]);
    DKServerAPI_t *pAPI = NULL;
    int found = -1;

    for (i = 0; i < nItems; i++)
    {
        pAPI = &DKServerAPITable[i];
        if (pAPI->nAPIID == nAPIID)
        {
            found = i;
            break;
        }
    }

    return found;
}

int DKMSG_CreateRequestMessage(uint8_t *pBuffer, int cbSize, DKRESTCommMsg_t *pCommandData, int len)
{
    int ret = DKSERVER_STATUS_SUCCESS;
    DKServerAPI_t *pAPI = NULL;
    DKRESTHeader_t *pHeader = NULL;
    int index = 0;

    if (pBuffer == NULL ||
        cbSize <= len ||
        pCommandData == NULL ||
        len < sizeof(DKRESTCommMsg_t))
    {
        return DKSERVER_SYSTEM_ERROR_UNDEFINED;
    }

    pHeader = &(pCommandData->Header);

    if (pHeader == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_UNDEFINED;
    }

    index = DKMSG_GetFunctionIndex(pHeader->MessageType);

    if (index < 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pAPI = &DKServerAPITable[index];

    if (pAPI && pAPI->CreateRequest)
    {
        ret = pAPI->CreateRequest(pBuffer, cbSize, (uint8_t *)pCommandData, len);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            DKServerManager_SendEventIDToWorkerThread(pHeader->SourceOfOrigin,
                                                      pHeader->TaskIdentificationNumber,
                                                      pHeader->MessageType,
                                                      HTTP_OK,
                                                      ret);
            return ret;
        }
    }

    return ret;
}

int DKMSG_VerifyDKServerResponse(uint8_t *pMsg, int cbMsgSize)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;
    int rootnode = 0;                   // rootnode, must be 0
    int statuscode = 0;                 // DKServer HTTP status code

    int findList1[] = {DKJSN_TOKEN_STATUS, DKJSN_TOKEN_CODE, 0};

    if (strlen((char *)pMsg) <= 0 || cbMsgSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Status code
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList1, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        // Status code not found
        DKJSN_Uninit(handle);
        return ret;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "\"status\":\"code\": %s\n", szBuffer);

    statuscode = atol(szBuffer);

    /*if (statuscode == DKSERVER_STATUS_INVALID_SESSION_TOKEN)
    {
        ret = DKSERVER_STATUS_SESSION_TIME_OUT;
    }
    else if (statuscode == DKSERVER_STATUS_GATEWAY_NOT_FOUND)
    {
        ret = DKSERVER_STATUS_MAC_ADDRESS_NOT_FOUND;
    }
    else */if (statuscode == DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS)
    {
        ret = DKSERVER_STATUS_SUCCESS;
    }
    else if (statuscode > 0)
    {
        ret = statuscode;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------
    DKJSN_Uninit(handle);

    return ret;
}

int DKMSG_ParseResponseMessage(uint8_t *pBuffer, int cbSize, DKRESTCommMsg_t *pCommandData, int len, uint8_t *optionData, int optionDataLen)
{
    int ret = DKSERVER_STATUS_SUCCESS;
    DKServerAPI_t *pAPI = NULL;
    DKRESTHeader_t *pHeader = NULL;
    int index = 0;

    if (pBuffer == NULL ||
        cbSize <= 0 ||// <= len ||
        pCommandData == NULL ||
        len < sizeof(DKRESTCommMsg_t))
    {
        return DKSERVER_SYSTEM_ERROR_UNDEFINED;
    }

    pHeader = &(pCommandData->Header);

    if (pHeader == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_UNDEFINED;
    }

    index = DKMSG_GetFunctionIndex(pHeader->MessageType);

    if (index < 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pAPI = &DKServerAPITable[index];

    if (pAPI && pAPI->ParseResponse)
    {
        ret = pAPI->ParseResponse(pBuffer, cbSize, (uint8_t *)pCommandData, len, optionData, optionDataLen);
        /*if (ret != DKSERVER_STATUS_SUCCESS)
        {
            DKServerManager_SendEventIDToWorkerThread(pHeader->SourceOfOrigin,
                                                      pHeader->TaskIdentificationNumber,
                                                      pHeader->MessageType,
                                                      HTTP_OK,
                                                      ret);
            return ret;
        }*/
    }

    return ret;
}
