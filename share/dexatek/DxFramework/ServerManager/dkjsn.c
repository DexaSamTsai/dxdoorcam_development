//============================================================================
// File: dkjsn.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/26
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/10/19
//     1) Description: Force variable to be placed at SDRAM
//        Modified:
//            - Declaration of variable JSMNTokenTable
//
//     Version: 0.4
//     Date: 2017/06/29
//     1) Description: Use dxMemAlloc()
//        Modified:
//            - DKJSN_Init()
//
//     Version: 0.5
//     Date: 2017/08/11
//     1) Description: Under cross platform consideration, revise data type of
//                     "handle" to "long" instead of "int"
//                     +--------+-----------+-----------+
//                     |        |  Windows  |   Linux   |
//                     |        +-----+-----+-----+-----+
//                     |        | x86 | x64 | x86 | x64 |
//                     +--------+-----+-----+-----+-----+
//                     | int    |   4 |   4 |   4 |   4 |
//                     | long   |   4 |   4 |   4 |   8 |
//                     | size_t |   4 |   8 |   4 |   8 |
//                     | time_t |   4 |   8 |   4 |   8 |
//                     +--------+-----+-----+-----+-----+
//        Modified:
//            - DKJSN_Init()
//            - DKJSN_GetObjectType()
//            - DKJSN_GetObjectIndexForKey()
//            - DKJSN_GetObjectCount()
//            - DKJSN_SelectObjectByIndex()
//            - DKJSN_GetObjectStringLength()
//            - DKJSN_GetObjectString()
//            - DKJSN_GetObjectStringLengthByList()
//            - DKJSN_GetObjectStringByList()
//            - DKJSN_SelectObjectByList()
//            - DKJSN_Uninit()
//            - _FindTargetIndex()
//============================================================================
#include <stdio.h>
#include <string.h>
#include "dk_Network.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"


/******************************************************
 *                      Macros
 ******************************************************/
#pragma message ( "WARNING: We increase the size to 36KB to support large container and peripherals. This is temporarily only as our data retrive mechanism need revise. " )

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagDKJSNHandle
{
    char        bInit;              // 1 if DKJSN_Init() success.
    jsmn_parser jsmnParser;         // Allocate in this file and need to free before closing.
    jsmntok_t   *pjsmnTokens;       // Allocate in this file and need to free before closing.
    int         nNumOfTokens;       // Number of tokens which m_pjsmnTokens occupied.
    char        *pszJSONData;       // JSON data is allocated in other file and CAN NOT free memory here.
} DKJSNHANDE;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
/**
 * @fn _FindTargetIndex
 *
 * @param handle
 * @param findList
 * @param node
 *
 * @return
 */
int _FindTargetIndex(dxPTR_t handle, int findList[], int node)
{
    int i = 0;
    DKJSONTokenTable *p = NULL;
    int r = node;
    char bFound = 1;

    while (findList[i] > 0)
    {
        p = _ConvertTokenString(findList[i]);
        if (p == NULL || p->id == 0 || p->pszKey == NULL)
        {
            bFound = 0;
            break;
        }

        r = DKJSN_GetObjectIndexForKey(handle, r, p->pszKey);

        if (r == -1)
        {
            bFound = 0;
            break;
        }

        // Why need (findList[i + 1] > 0) ?
        // If the last node is a directory, we must return the whole array to caller
        //

        if (DKJSN_GetObjectType(handle, r) == JSMN_ARRAY &&
            DKJSN_GetObjectCount(handle, r) > 0 &&
            findList[i + 1] > 0 )
        {
            // Always get first child
            r = DKJSN_SelectObjectByIndex(handle, r, 0);
            if (r == -1)
            {
                bFound = 0;
                break;
            }
        }

        i++;
    }

    if (bFound == 0) return -1;

    return r;
}

/**
 * @fn DKJSN_Init
 *
 * @param pszJSON a json format string
 *
 * @retval 0 Error
 * @retval >0 Number of tokens
 */
dxPTR_t DKJSN_Init(const char *pszJSON, jsmntok_t* tokenTable, int tokenTableSize)
{
    DKJSNHANDE *handle = NULL;
    int num = 0;
    int ret = 0;

    // David Changed. Use dxMemAlloc() instead of calloc() or malloc() + memset()
    //handle = malloc(sizeof(DKJSNHANDE));
    //handle = (DKJSNHANDE *)calloc(1, sizeof(DKJSNHANDE));
    handle = (DKJSNHANDE *)dxMemAlloc("DKJSN", 1, sizeof(DKJSNHANDE));
    if (handle == NULL) {
        goto fail_alloc_handle;
    }

    //memset(handle, 0x00, sizeof(DKJSNHANDE));

    handle->bInit = 0;
    jsmn_init(&handle->jsmnParser);

    num = jsmn_parse(&handle->jsmnParser, pszJSON, strlen(pszJSON), NULL, 0);
    if (num == 0 || num > tokenTableSize)
    {
        goto fail;
    }

    {
        memset(&tokenTable[0], 0x00, tokenTableSize * sizeof (jsmntok_t));
        handle->pjsmnTokens = tokenTable;

        jsmn_init(&handle->jsmnParser);

        ret = jsmn_parse(&handle->jsmnParser, pszJSON, strlen(pszJSON), handle->pjsmnTokens, MAX_NUM_JSON_ELEMENT_SUPPORT);
        if (ret > 0)
        {
            handle->nNumOfTokens = ret;
            handle->pszJSONData = (char *)pszJSON;
            handle->bInit = 1;
        }

#if 0   // Dump table. for debug.
        {
            int i = 0;
            jsmntok_t *p = NULL;
            for (i = 0; i < handle->nNumOfTokens; i++)
            {
                p = (jsmntok_t *)&handle->pjsmnTokens[i];
                WPRINT_APP_INFO( ("#%-4d Type:%-4d Start:%-4d End:%-4d Size:%-4d Parent:%-4d\n", i, p->type, p->start, p->end, p->size, p->parent) );
            }
        }
#endif
    }

    return (dxPTR_t)handle;

fail :

    dxMemFree (handle);

fail_alloc_handle :

    return 0;
}

/**
 * @fn DKJSN_GetObjectType
 *
 * @param handle
 * @param node index value of json data node
 *
 * @retval 0 fail
 * @retval >0 success
 */
int DKJSN_GetObjectType(dxPTR_t handle, int node)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;

    if (h == NULL || h->bInit == 0 || h->pjsmnTokens == NULL) return 0;

    if (node < 0 || node >= h->nNumOfTokens)
    {
        return 0;
    }

    jsmntok_t *p = (jsmntok_t *)&h->pjsmnTokens[node];

    if (p == NULL) return 0;

    return (int)p->type;
}

/**
 * @fn DKJSN_GetObjectIndexForKey
 *
 * @param handle
 * @param node
 * @param pszKey
 *
 * @retval -1 failure
 * @retval >=0 node index
 */
int DKJSN_GetObjectIndexForKey(dxPTR_t handle, int node, const char *pszKey)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    jsmntok_t *p = NULL;
    int i = 0;
    int keyNode = -1;

    if (h == NULL || node < 0 || node >= h->nNumOfTokens)
    {
        return -1;
    }

    for (i = 0; i < h->nNumOfTokens; i++)
    {
        p = (jsmntok_t *)&h->pjsmnTokens[i];

        if (p &&
            p->parent == node &&
            strlen(pszKey) == (p->end - p->start) &&
            memcmp(pszKey, &h->pszJSONData[p->start], p->end - p->start) == 0)
        {
            keyNode = i;
            break;
        }
    }

    if (keyNode >= 0 && (keyNode + 1) < h->nNumOfTokens)
    {
        keyNode++;
    }

    return keyNode;
}

/**
 * @fn DKJSN_GetObjectCount
 *
 * @param handle
 * @param node
 *
 * @return number of child nodes
 */
int DKJSN_GetObjectCount(dxPTR_t handle, int node)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    jsmntok_t *p = NULL;
    int i = 0;
    int count = 0;

    if (h == NULL || node < 0 || node >= h->nNumOfTokens)
    {
        return -1;
    }

    for (i = 0; i < h->nNumOfTokens; i++)
    {
        p = (jsmntok_t *)&h->pjsmnTokens[i];

        if (p && p->parent == node)
        {
            count++;
        }
    }

    return count;
}

/**
 * @fn DKJSN_SelectObjectByIndex
 *
 * @param handle
 * @param node
 * @param index
 *
 * @retval -1 failure
 * @retval >=0 node index
 */
int DKJSN_SelectObjectByIndex(dxPTR_t handle, int node, int index)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    jsmntok_t *p = NULL;
    int i = 0;
    int ret = -1;
    int count = 0;

    if (h == NULL || node < 0 || node >= h->nNumOfTokens)
    {
        return -1;
    }

    for (i = 0; i < h->nNumOfTokens; i++)
    {
        p = (jsmntok_t *)&h->pjsmnTokens[i];

        if (p && p->parent == node)
        {
            count++;
            if (index + 1 == count)
            {
                ret = i;
                break;
            }
        }
    }

    return ret;
}

/**
 * @fn DKJSN_GetObjectStringLength
 *
 * @param handle
 * @param node
 *
 * @return
 */
int DKJSN_GetObjectStringLength(dxPTR_t handle, int node)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    jsmntok_t *p = NULL;
    int len = 0;

    if (h == NULL || node < 0 || node >= h->nNumOfTokens)
    {
        return 0;
    }

    p = (jsmntok_t *)&h->pjsmnTokens[node];

    if (p == NULL) return 0;

    len = (p->end - p->start);

    return len;
}

/**
 * @fn DKJSN_GetObjectString
 *
 * @param handle
 * @param index
 *
 * @retval NULL failure
 * @retval Others success;
 */
char *DKJSN_GetObjectString(dxPTR_t handle, int node)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    jsmntok_t *p = NULL;

    if (h == NULL || node < 0 || node >= h->nNumOfTokens)
    {
        return NULL;
    }

    p = (jsmntok_t *)&h->pjsmnTokens[node];

    if (p == NULL) return NULL;

    if ((unsigned int)p->start >= strlen(h->pszJSONData)) return NULL;

    return &h->pszJSONData[p->start];
}

/**
 * @fn DKJSN_GetObjectStringLengthByList
 *
 * @param handle
 * @param findList
 * @param node
 *
 * @return
 */
int DKJSN_GetObjectStringLengthByList(dxPTR_t handle, int findList[], int node)
{
    int r = _FindTargetIndex(handle, findList, node);

    r = DKJSN_GetObjectStringLength(handle, r);

    return r;
}

/**
 * @fn DKJSN_GetObjectStringByList
 *
 * @param handle
 * @param findList
 * @param node
 *
 * @return
 */
char *DKJSN_GetObjectStringByList(dxPTR_t handle, int findList[], int node)
{
    int r = _FindTargetIndex(handle, findList, node);

    return DKJSN_GetObjectString(handle, r);
}

/**
 * @fn DKJSN_SelectObjectByList
 *
 * @param handle
 * @param findList
 * @param node
 *
 * @return
 */
int DKJSN_SelectObjectByList(dxPTR_t handle, int findList[], int node)
{
    return _FindTargetIndex(handle, findList, node);
}

/**
 * @fn DKJSN_Uninit
 *
 * @param handle
 */
void DKJSN_Uninit(dxPTR_t handle)
{
    DKJSNHANDE *h = (DKJSNHANDE *)handle;
    if (h)
    {
        h->bInit = 0;
        h->pjsmnTokens = NULL;
        h->nNumOfTokens = 0;

        dxMemFree (h);
        h = NULL;
    }
}
