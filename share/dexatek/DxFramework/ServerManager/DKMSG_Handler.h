//============================================================================
// File: DKMSG_Handler.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/02/24
//     1) Description: Move structure from DKServerManager.h
//
//============================================================================
#ifndef _DKMSG_HANDLER_H
#define _DKMSG_HANDLER_H

#pragma once

#include "dxOS.h"

#include "DKMSG_Common.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int DKMSG_GetFunctionIndex(int nAPIID);
int DKMSG_CreateRequestMessage(uint8_t *pBuffer, int cbSize, DKRESTCommMsg_t *pCommandData, int len);
int DKMSG_VerifyDKServerResponse(uint8_t *pMsg, int cbMsgSize);
int DKMSG_ParseResponseMessage(uint8_t *pBuffer, int cbSize, DKRESTCommMsg_t *pCommandData, int len, uint8_t *optionData, int optionDataLen);

#ifdef __cplusplus
}
#endif

#endif // _DKMSG_HANDLER_H
