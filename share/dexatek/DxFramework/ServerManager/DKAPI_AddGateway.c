//============================================================================
// File: DKAPI_AddGateway.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_AddGateway_Response()
//            - Build_AddGateway_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_AddGateway_Data()
//
//     Version: 0.5
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//            - Parse_AddGateway_Response()
//============================================================================
#include <stdlib.h>

#include "dxOS.h"
#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_AddGateway.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_ADDGATEWAY_API                                                                   \
                                                    "%s "                                               \
                                                    "%s/AddGateway.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_AddGateway_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    int fNewDataAppend = 0;

    char szData[21] = {0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (AddGateway_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Gateway information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    ObjectDictionary_Lock ();
    int maclen = _MACAddrToDecimalStr(szData, sizeof(szData), (uint8_t *) ObjectDictionary_GetMacAddress (), 8);
    ObjectDictionary_Unlock ();

    if (maclen <= 0)
    {
        ObjectDictionary_Lock ();
        DKHex_HexToASCII (ObjectDictionary_GetMacAddress (), 8, &szData[0], sizeof(szData));
        ObjectDictionary_Unlock();

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, MAC address %s convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__,
                         szData);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    AddGateway_t *pGateway = (AddGateway_t *)pCommMsg->pData;


    sprintf((char *)pBuffer, DKMSG_TEMPLATE_ADDGATEWAY_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    strcat((char *)pBuffer, "{");

    // "GMACAddress
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\"GMACAddress\":\"%s\",\"DeviceName\":\"%s\",\"Latitude\":%9.6f,\"Longitude\":%10.6f",
            szData, pGateway->szDeviceName, pGateway->Latitude, pGateway->Longitude);

    // "GatewayFirmware"
    strcat((char *)pBuffer, ",\"GatewayFirmware\":{");

    if (*pGateway->WiFiVersion)
    {
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"WiFiVersion\":\"%s\"", pGateway->WiFiVersion);
        fNewDataAppend = 1;
    }

    if (*pGateway->BLEVersion)
    {
        if (fNewDataAppend == 1)
        {
            strcat((char *)pBuffer, ",");
        }

        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"BLEVersion\":\"%s\"", pGateway->BLEVersion);
        fNewDataAppend = 1;
    }

    strcat((char *)pBuffer, "}}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_AddGateway_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;

    int findList2[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_OBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (AddGateway_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Gateway information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    AddGateway_t *pGateway = (AddGateway_t *)pCommMsg->pData;

    uint32_t objectID = 0;

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0 || pGateway == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {

        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ObjectID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Gateway ObjectID: %s\n", szBuffer);

    objectID = strtoul(szBuffer, NULL, 10);
    if (objectID == 0)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Delete current gateway information

    ObjectDictionary_Lock();

    ObjectDictionary_DeleteGateway ();

    // Copy local variable to Global
    ObjectDictionary_AddGateway (objectID, (uint8_t*) ObjectDictionary_GetMacAddress (), 8, pGateway->WiFiVersion, pGateway->BLEVersion, pGateway->szDeviceName);

    ObjectDictionary_Unlock ();

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
