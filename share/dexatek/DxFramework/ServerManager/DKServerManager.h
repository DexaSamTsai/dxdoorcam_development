//============================================================================
// File: DKServerManager.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.175.0
//     Date: 2015/09/07
//     1) Description: Support update motion sensor aggregation data
//        Added:
//            - DKServerManager_GetUTCLocalTimeTable()
//
//     Version: 0.176
//     Date: 2015/09/10
//     1) Description: Support AddSmartHomeCentralLog API
//        Added:
//            - A new entry into DKHTTPCommand_t, DKHTTP_ADDSMARTHOMECENTRALLOG
//            - DKServerManager_AddSmartHomeCentralLog_Asynch()
//
//     Version: 0.177
//     Date: 2015/11/23
//     1) Description: Change the Get Job Interval dynamically according to needs.
//        Added:
//            - DKServerManager_ConfigGetJobInterval()
//
//     Version: 0.178
//     Date: 2016/01/11
//     1) Description: Support AddStandalonePeripheral API
//        Added:
//            - DKServerManager_AddStandalonePeripheral_Asynch()
//        Modified:
//            - Add a new value, DKHTTP_ADDSTANDALONEPERIPHERAL, into DKHTTPCommand_t
//
//     Version: 0.179
//     Date: 2016/01/15
//     1) Description: Previous architecture does not need the
//        Added:
//            - DKServerManager_GetUTCLocalTimeTable()
//
//     Version: 0.179.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.179.2
//     Date: 2016/02/18
//     1) Description: Remove unused function
//        Removed:
//            - DKServerManager_MD5Start()
//            - DKServerManager_MD5Update()
//            - DKServerManager_MD5Finish()
//            - DKServerManager_MD5Equal()
//            - DKServerManager_MD5Clear()
//
//     Version: 0.179.3
//     Date: 2016/02/24
//     1) Description: Revise and enhance DKServerManager state
//        Modified:
//            - DKServerManager_StateMachine_State_t
//            - DKServerManager_State_Suspend()
//            - DKServerManager_State_Resume()
//        Added:
//            - DKServerManager_StateMachine_Command_t
//            - DKServerManager_GetCurrentState()
//
//
//     Version: 0.179.4
//     Date: 2016/02/24
//     1) Description: Move structure to DKMSG_Common.h and DKServerManager.c
//        Moved:
//            - DKRESTHeader_t
//            - DKRESTCommMsg_t
//            - DKServerEventReportObj_t
//
//     Version: 0.179.5
//     Date: 2016/03/01
//     1) Description: Revise DKServer_ReportStatusCode_t, DKServer_StatusCode_t, DKServer_SystemErrorCode_t and others
//        Moved:
//            - DKServer_ReportStatusCode_t
//            - DKServer_StatusCode_t
//            - DKServer_SystemErrorCode_t
//            - Rename DKHTTPCommand_t to DKMSG_Command_t
//        Added:
//            - DKMSG_StateMachine_Command_t
//
//
//     Version: 0.179.6
//     Date: 2016/03/07
//     1) Description: DKSERVER_REPORT_STATUS_GATEWAY_NOT_FOUND to DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
//        Modified:
//            - DKServer_ReportStatusCode_t
//
//     2) Description: Add message to handle DNS lookup failed
//        Modified:
//            - Remove DKMSG_DNS_LOOKUP from DKMSG_Command_t
//        Added:
//            - Added a new enum value DKSERVER_SYSTEM_ERROR_DNS_LOOKUP_FAILED into DKServer_SystemErrorCode_t;
//
//     Version: 0.179.7
//     Date: 2016/03/14
//     1) Description: Remove "NOTE: Porting to dxOS" comment
//     2) Description: Remove Long Poll support
//
//     Version: 0.179.8
//     Date: 2016/09/06
//     1) Description: To support MQTT, report UserObjectID and GatewayID when server response success
//        Modified:
//            - DKServerManager_GetPeripheralByMAC_Asynch()
//
//     Version: 0.224
//     Date: 2016/09/07
//     1) Description: Revise JobDoneReport API to support "ReportedData"
//        Modified:
//            - JobDoneReport_t
//            - DKServerManager_JobDoneReport_Asynch()
//
//     Version: 0.228
//     Date: 2016/10/03
//     1) Description: Support Container
//        Modified:
//            - DKMSG_Command_t
//            - DKJobType_t
//        Added:
//            - DKSubType_t
//            - DKServerManager_GetDataContainerByID_Asynch()
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Revise dxContainerM_t structure
//        Modified:
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//
//     Version: 0.234.1
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Added:
//            - DKServerManager_GetPeripheralNameByIndex()
//
//     Version: 0.234.2
//     Date: 2017/03/02
//     1) Description: Change peripheral's DeviceName
//        Added:
//            - DKServerManager_ChangePeripheralDeviceName()
//
//     Version: 0.234.3
//     Date: 2017/03/27
//     1) Description: Revise Error Code
//        Added:
//            - DKSERVER_STATUS_FIRMWARE_VERSION_ERROR
//        Modified:
//            - DKServer_SystemErrorCode_t and DKServer_StatusCode_t
//
//     Version: 0.234.4
//     Date: 2017/04/06
//     1) Description: Implement a function to forace State Machine to SUSPENDED
//        Added:
//            - DKSERVERMANAGER_COMMAND_FORCE_SUSPEND
//            - DKServerManager_State_ForceToSuspended()
//
//     Version: 0.242
//     Date: 2017/04/14
//     1) Description: Support "ClientTimeZone"
//        Added:
//            - New structure ClientServerTime_t
//
//     Version: 0.242.1
//     Date: 2017/04/19
//     1) Description: Support GetCurrentUser API
//        Added:
//            - New structure CurrentUserInfo_t
//            - Added DKMSG_GETCURRENTUSER to DKMSG_Command_t
//            - New function DKServerManager_GetCurrentUser_Asynch()
//
//     Version: 0.242.2
//     Date: 2017/05/08
//     1) Description: Support DeleteGateway API
//        Added:
//            - Added DKMSG_DELETEGATEWAY to DKMSG_Command_t
//            - Support new function DKServerManager_DeleteGateway_Asynch().
//              After deleting, DKMSG_DELETEGATEWAY message will be notified without PAYLOAD
//
//     Version: 0.242.3
//     Date: 2017/08/08
//     1) Description: Porting to Embedded Linux
//
//     Version: 0.242.4
//     Date: 2017/08/15
//     1) Description: Support IPCam API
//        Added:
//            - New structure IPCamEventFile_t
//                            IPCamOfflineFile_t
//            - DKServerManager_GetPushRTMPLiveURL_Asynch()
//            - DKServerManager_GetEventMediaFileUploadPath_Asynch()
//            - DKServerManager_GetOfflineMediaFileUploadPath_Asynch()
//        Modified:
//            - DKMSG_Command_t
//
//     Version: 0.253.0
//     Date: 2017/08/30
//     1) Description: Revise IPCam API
//        Modified:
//            - Rename structure to IPCamEventFileREQ_t
//        Added:
//            - New structure, IPCamEventFileREP_t, to store response message of GetEventMediaFileUploadPath
//            - DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData()
//            - DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData()
//            - DKServerManager_GetEventMediaFileUploadPath_MessageData_Length()
//            - DKServerManager_GetEventMediaFileUploadPath_MessageData_GetData()
//
//     Version: 0.255
//     Date: 2017/09/08
//     1) Description: Revise SendMessageWithKey API
//        Modified:
//            - DKServerManager_SendMessageWithKey_Asynch()
//
//     Version: 0.255.1
//     Date: 2017/09/11
//     1) Description: Support reading Central's device name
//            - DKServerManager_GetCentralName()
//
//     Version: 0.255.2
//     Date: 2017/09/12
//     1) Description: Support a new API, SendMessageWithKeyNu
//        Modified:
//            - DKServerManager_SendMessageWithKey_Asynch()
//            - DKServerManager_SendMessageWithKeyNum_Asynch()
//
//     Version: 0.255.3
//     Date: 2017/09/27
//     1) Description: Support login with "DKAuth"
//        Modified:
//            - Added DKMSG_ANONYMOUSLOGIN to DKMSG_Command_t
//        Added:
//            - DKServerManager_AnonymousLogin_Asynch()
//
//      Version: 0.255.4
//      Date: 2018/05/28
//      1) Description: Support
//         Added:
//            - DKServerManager_NotifyEventDone()
//============================================================================
#ifndef _DKSERVERMANAGER_H
#define _DKSERVERMANAGER_H

#pragma once

#include "dxOS.h"
#include "dxContainer.h"
#include "dk_Peripheral.h"
#include "dk_Job.h"

#include "ObjectDictionary.h"



#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/
#define CALLOC_NAME                                 "DKSM"

/******************************************************
 *                      Macros
 ******************************************************/

#define DK_SERVER_HTTP_OK                           200

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 * NOTE: Both value of DKMSG_Command_t and DKMSG_StateMachine_Command_t are
 *       stored in MessageType of DKEventHeader_t structure. So, please check
 *       the correct range value listed below before adding a new enum value.
 *
 * +-----------+------------------------------+
 * |   0 ~ 200 | DKMSG_Command_t              |
 * +-----------+------------------------------+
 * | 201 ~ 255 | DKMSG_StateMachine_Command_t |
 * +-----------+------------------------------+
 *
 ******************************************************/
typedef enum _enumDKMSG_Command
{
    DKMSG_UNKNOWN                               = 0,

    // Login
    DKMSG_USERLOGIN                             = 1,
    DKMSG_RENEWSESSIONTOKEN                     = 2,
    DKMSG_GATEWAYLOGIN                          = 3,

    // Gateway
    DKMSG_ADDGATEWAY                            = 4,
    DKMSG_CHANGEGATEWAYSTATUS                   = 5,
    DKMSG_UPDATEGATEWAYFIRMWAREVERSION          = 6,

    // Peripheral
    DKMSG_GETPERIPHERALBYGMACADDRESS            = 7,
    DKMSG_ADDPERIPHERAL                         = 8,
    DKMSG_ADDSTANDALONEPERIPHERAL               = 9,
    DKMSG_UPDATEPERIPHERALFIRMARE               = 10,
    DKMSG_SETPERIPHERALQUERYCOLLECTION          = 11,
    DKMSG_CLEARPERIPHERALSECURITYKEY            = 12,
    DKMSG_SETPERIPHERALSECURITYKEY              = 13,

    DKMSG_UPDATEPERIPHERALDATAPAYLOAD           = 14,
    DKMSG_UPDATEPERIPHERALSTATUSINFO            = 15,
    DKMSG_UPDATEDATAPAYLOADHISTORY              = 16,
    DKMSG_UPDATEPERIPHERALAGGREGATIONDATA       = 17,

    // Job
    DKMSG_NEWJOBS                               = 18,
    DKMSG_JOBDONEREPORT                         = 19,

    DKMSG_UPDATEADVJOBSTATUS                    = 21,
    DKMSG_GETADVJOBBYGATEWAY                    = 22,

    DKMSG_GETSCHEDULEJOB                        = 23,
    DKMSG_DELETESCHEDULEJOB                     = 24,

    // Amazon SNS
    DKMSG_SENDNOTIFICATIONMESSAGE               = 25,
    DKMSG_SENDMESSAGEWITHKEY                    = 26,

    // Firmware Download
    DKMSG_GETLASTFIRMWAREINFO                   = 27,
    DKMSG_FIRMWAREDOWNLOAD                      = 28,

    // Others
    DKMSG_GETSERVERINFO                         = 29,
    DKMSG_GETSERVERTIME                         = 30,
    DKMSG_NUMOFACTIVEUSERS                      = 31,
    DKMSG_GETCURRENTUSER                        = 32,
    DKMSG_DEVICESTATUS                          = 33,

    // Log
    DKMSG_ADDSMARTHOMECENTRALLOG                = 34,

    // Container
    DKMSG_GETDATACONTAINERBYID                  = 35,
    DKMSG_CREATECONTAINERMDATA                  = 36,
    DKMSG_UPDATECONTAINERMDATA                  = 37,
    DKMSG_INSERTCONTAINERDDATA                  = 38,
    DKMSG_UPDATECONTAINERDDATA                  = 39,
    DKMSG_DELETECONTAINERDDATA                  = 40,
    DKMSG_DELETECONTAINERMDATA                  = 41,

    // Gateway
    DKMSG_DELETEGATEWAY                         = 42,

    // IPCam
    DKMSG_GETPUSHRTMPLIVEURL                    = 43,
    DKMSG_GETEVENTMEDIAFILEUPLOADPATH           = 44,
    DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH         = 45,

    // Amazon SNS
    DKMSG_SENDMESSAGEWITHKEYNUM                 = 46,

    // Anonymous Login
    DKMSG_ANONYMOUSLOGIN                        = 47,

    // Amazon SNS
    DKMSG_NOTIFYEVENTDONE                       = 48,
    
    // DKServerManager use only
    DKMSG_HOST_REDIRECT                         = 50,

    // Notify
    DKMSG_SYSTEM_ERROR                          = 60,
    DKMSG_DATETIME_UPDATE                       = 61,

    // StateMachine state changed notification
    DKMSG_STATE_CHANGED                         = 63,

    // NOTE: The value of DKMSG_Command_t can't exceed 200

} DKMSG_Command_t;

typedef enum _enumDKMSG_StateMachine_Command
{
    DKMSG_STATEMACHINE_SUSPEND                  = 200,
    DKMSG_STATEMACHINE_RESUME                   = 201,

    // NOTE: The value of DKMSG_StateMachine_Command_t can't exceed 255

} DKMSG_StateMachine_Command_t;

typedef enum _enumDKServerReportStatusCode
{
    DKSERVER_REPORT_STATUS_SUCCESS                          = 0,

    // Error code return from DKServer.
    // Error codes smaller than 1024 are defined in document "DEXATEK's Server Programming Guide"
    DKSERVER_REPORT_STATUS_INVALID_PARAMS                   = 1,
    DKSERVER_REPORT_STATUS_MISSING_PARAMS                   = 2,
    DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS                  = 3,
    DKSERVER_REPORT_STATUS_INVALID_OBJECTID                 = 4,
    DKSERVER_REPORT_STATUS_DUPLICATE_MAC                    = 5,
    DKSERVER_REPORT_STATUS_UNAUTHORIZE_LOGIN                = 6,
    DKSERVER_REPORT_STATUS_USER_ALREADY_REGISTERED          = 7,
    DKSERVER_REPORT_STATUS_ALREADY_SUBUSER                  = 8,
    DKSERVER_REPORT_STATUS_SUBUSER_NOT_FOUND                = 9,
    DKSERVER_REPORT_STATUS_SUPPORT_ARRAY_SIZE_OF_ONE_ONLY   = 10,
    DKSERVER_REPORT_STATUS_INVALID_MAC                      = 11,
    DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN            = 12,
    DKSERVER_REPORT_STATUS_INVALID_API_KEY                  = 13,
    DKSERVER_REPORT_STATUS_INVALID_RANGE                    = 14,
    DKSERVER_REPORT_STATUS_SAME_DATA                        = 15,
    DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND                 = 16,       // Change from DKSERVER_REPORT_STATUS_GATEWAY_NOT_FOUND
    DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST                = 17,
    DKSERVER_REPORT_STATUS_UNAUTHORIZE_ACCESS               = 18,
    DKSERVER_REPORT_STATUS_MAX_NUMBER_OF_PERIPHERALS_EXCEEDED    = 19,
    DKSERVER_REPORT_STATUS_GATEWAY_ALREADY_IN_OTHER_REGION  = 20,
    DKSERVER_REPORT_STATUS_DUPLICATE_UUID                   = 21,
    DKSERVER_REPORT_STATUS_PERIPHERAL_TYPE_ERROR            = 22,
    DKSERVER_REPORT_STATUS_ONE_OR_MORE_PERIPHERAL_IN_GROUP  = 23,
    DKSERVER_REPORT_STATUS_DISABLED_BY_USER_SETTING         = 24,
    DKSERVER_REPORT_STATUS_DUPLICATE_JOBCONDITION_GATEWAYID = 25,
    DKSERVER_REPORT_STATUS_DUPLICATE_JOBEXECUTION_GATEWAYID = 26,
    DKSERVER_REPORT_STATUS_ACCESS_DENIED                    = 27,       // Change from DKSERVER_STATUS_DKSERVER_NEED_REDIRECT to match document
    DKSERVER_REPORT_STATUS_INVALID_ACCESS_CODE              = 28,
    DKSERVER_REPORT_STATUS_SETTING_NO_EXIST                 = 29,
    DKSERVER_REPORT_STATUS_INVALID_REGION                   = 30,
    DKSERVER_REPORT_STATUS_APP_EXECUTION_LIMIT_EXCEEDED     = 31,
    DKSERVER_REPORT_STATUS_INVALID_GROUP                    = 32,
    DKSERVER_REPORT_STATUS_SUBUSER_TARGET_HAVE_DEVICE       = 33,
    DKSERVER_REPORT_STATUS_SERVER_VERIFY_FAIL               = 34,
    DKSERVER_REPORT_STATUS_MAXIMUM_SUBUSERS_REACHED         = 35,
    DKSERVER_REPORT_STATUS_SERVER_IS_BUSY                   = 36,

    // Code range start from 1024 to 4999 will be reserved for DKServer & MySQL

    // Special error code reserved for internal use
    DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT                 = 6000

} DKServer_ReportStatusCode_t;

typedef enum _enumDKServerStatusCode
{
    // COMMON Error Code
    DKSERVER_STATUS_SUCCESS                         = 0,
    DKSERVER_STATUS_ERROR_UNDEFINED                 = 101,
    DKSERVER_STATUS_ERROR_DX_RTOS                   = 102,
    DKSERVER_STATUS_ERROR_ALLOC_MEMORY              = 103,
    DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL          = 104,
    DKSERVER_STATUS_INVALID_LOCAL_PARAMETER         = 105,
    DKSERVER_STATUS_ERROR_NETWORK_TIMEOUT           = 106,

    // Special Error Code
    DKSERVER_STATUS_ERROR_QUEUE_IS_FULL             = 201,
    DKSERVER_STATUS_FIRMWARE_VERSION_EQUAL,
    DKSERVER_STATUS_FIRMWARE_VERSION_ERROR,
    DKSERVER_STATUS_NOT_READY,

} DKServer_StatusCode_t;

typedef enum _enumDKServerSystemErrorCode
{
    // COMMON Error Code
    DKSERVER_SYSTEM_ERROR_SUCCESS                   = 0,
    DKSERVER_SYSTEM_ERROR_UNDEFINED                 = 101,
    DKSERVER_SYSTEM_ERROR_DX_RTOS                   = 102,
    DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY              = 103,
    DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL          = 104,
    DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER   = 105,
    DKSERVER_SYSTEM_ERROR_NETWORK_TIMEOUT           = 106,

    // Special Error Code
    DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND        = 301,
    DKSERVER_SYSTEM_ERROR_JSON_INIT,
    DKSERVER_SYSTEM_ERROR_BASE64_URL_ENCODE_DECODE,
    DKSERVER_SYSTEM_ERROR_DNS_LOOKUP_FAILED

} DKServer_SystemErrorCode_t;

typedef enum _enumDKJobType
{
    DKJOBTYPE_NONE                              = 0,

    DKJOBTYPE_STANDARD                          = 1,    // --+
    DKJOBTYPE_SCHEDULE                          = 2,    //   |
    DKJOBTYPE_ADVANCED_CONDITION                = 3,    //   |
    DKJOBTYPE_ADVANCED_EXECUTION                = 4,    //   +--> Work with DKMSG_NEWJOBS
    DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY = 5,    //   |
    DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY = 6,    // --+

    DKJOBTYPE_GATEWAY_INFO                      = 10,    // --+
    DKJOBTYPE_PERIPHERAL_INFO                   = 11,    // --+--> Work with DKMSG_GETPERIPHERALBYGMACADDRESS
    DKJOBTYPE_CONTAINER_INFO                    = 12,    // --+
} DKJobType_t;

/******************************************************
 *
 *                 +-------------------+-------------------+-------------------+-------------------+
 *                 |    SUSPENDING     |     SUSPENDED     |       READY       |   CHECKDKSERVER   | DKServerManager_StateMachine_State_t
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 * |  SUSPEND      |     SUSPENDED     |                   |    SUSPENDING     |    SUSPENDING     |
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 * |   RESUME      |                   |       READY       |                   |                   |
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 * |  SUCCESS      |                   |                   |                   |       READY       |
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 * |     FAIL      |                   |   CHECKDKSERVER   |   CHECKDKSERVER   |   CHECKDKSERVER   |
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 * | FORCE_SUSPEND |     SUSPENDED     |     SUSPENDED     |     SUSPENDED     |     SUSPENDED     |
 * +---------------+-------------------+-------------------+-------------------+-------------------+
 *
 ******************************************************/

typedef enum
{
    DKSERVERMANAGER_STATEMACHINE_STATE_NONE         = 0,
    DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDING,
    DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED,
    DKSERVERMANAGER_STATEMACHINE_STATE_READY,
    DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER,
} DKServerManager_StateMachine_State_t;

typedef enum
{
    DKSERVERMANAGER_COMMAND_SUSPEND                 = 0,
    DKSERVERMANAGER_COMMAND_RESUME,
    DKSERVERMANAGER_COMMAND_SUCCESS,
    DKSERVERMANAGER_COMMAND_FAIL,
    DKSERVERMANAGER_COMMAND_FORCE_SUSPEND,
} DKServerManager_StateMachine_Command_t;

typedef enum
{
    DKFIRMWARE_INFO_ID_UNKNOWN                      = 0,
    DKFIRMWARE_INFO_ID_WIFI_FILENAME,
    DKFIRMWARE_INFO_ID_WIFI_VERSION,
    DKFIRMWARE_INFO_ID_WIFI_MD5,
    DKFIRMWARE_INFO_ID_BLE_FILENAME,
    DKFIRMWARE_INFO_ID_BLE_VERSION,
    DKFIRMWARE_INFO_ID_BLE_MD5
} DKFirmare_Info_ID_t;

typedef enum
{
    DKSERVER_UPDATE_DATAPAYLOAD_TARGET_ALL                      = 1,
    DKSERVER_UPDATE_DATAPAYLOAD_TARGET_PERIPHERAL_TABLE_ONLY    = 2,
    DKSERVER_UPDATE_DATAPAYLOAD_TARGET_HISTORY_TABLE_ONLY       = 3
} DKServer_Update_DataPayload_Target_Info;

typedef struct _tagDKEventHeader
{
    // Considering data alignment, add "Reserved" to structure.
    uint64_t TaskIdentificationNumber;      // The identification number that will be reported back to registered EventFunction when Task is done.
    uint16_t SourceOfOrigin;                // The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
    uint8_t  MessageType;                   // See DKMSG_Command_t
    uint8_t  SubType;                       // See DKJobType_t
    uint16_t HTTPStatusCode;                // HTTP Status-Code. Range from 100 - 505 (already defined), 505 - 999 (extension-code). Refer RFC 2616 for details.
    union
    {
        uint16_t ReportStatusCode;          // See DKServer_ReportStatusCode_t if MessageType is neither DKSMANAGER_STATE_CHANGED nor DKMSG_SYSTEM_ERROR
        uint16_t StateMachineState;         // See DKServerManager_StateMachine_State_t if MessageType is DKSMANAGER_STATE_CHANGED
        uint16_t SystemErrorCode;           // See DKServer_SystemErrorCode_t if MessageType is DKMSG_SYSTEM_ERROR
    } s;
    int16_t  DataLen;
    uint8_t  Reserved[6];                   // Alignment byte
} DKEventHeader_t;                          // sizeof(DKEventHeader_t): 24

typedef struct _tagDKContentRange
{
    uint32_t        nFirstBytePosition;
    uint32_t        nLastBytePosition;
    uint32_t        nFileSize;
} DKContentRange_t;

typedef struct _tagDKFirmwareInfo
{
    DKContentRange_t range;
    uint8_t          pData[0];
} DKFirmwareInfo_t;

typedef struct _tagDKStandardJob
{
    uint32_t        ObjectID;
    uint32_t        GatewayID;
    uint32_t        UserObjectID;                   // Currently never use in JobManager, Now we will use to store the value of "CreatedByUserObjID"
    uint32_t        TimeStamp;
    uint16_t        DeviceIdentificationNumber;
    uint16_t        PayLoadIdentificationNumber;
    uint8_t         pPayload[0];
} DKStandardJob_t;

// For UpdatePeripheralStatus
typedef struct _tagUpdatePeripheralStatus
{
    uint32_t        ObjectID;
    uint32_t        AdvObjID;
    uint16_t        PSignalStr;
    uint16_t        PBattery;
    uint16_t        RunningStatus;
    uint16_t        PayloadLen;
    uint8_t         UpdateTo;
    uint8_t         Reserved[7];
    uint8_t         pPayload[0];
} UpdatePeripheralStatusData_t;                     // sizeof(UpdatePeripheralStatusData_t) == 28

// For AddGateway
typedef struct _tagAddGateway
{
    uint8_t         szDeviceName[32];
    char            WiFiVersion[16];
    char            BLEVersion[16];
    double          Latitude;
    double          Longitude;
    uint16_t        Status;
} AddGateway_t;

// For AddPeripheral
typedef struct _tagAddPeripheral
{
    uint8_t         MAC[8];
    uint8_t         szDeviceName[32];
    char            FWVersion[16];
    int             deviceType;
    uint16_t        PSignalStr;
    uint16_t        PBattery;
    uint8_t         szSecurityKey[16];
    uint8_t         nSecurityKeyLen;
} AddPeripheral_t;

// For UpdatePeripheralFirmware
typedef struct _PeripheralFirmware_t
{
    uint32_t        ObjectID;
    char            FWVersion[16];
} PeripheralFirmware_t;

// For ClearPeripheralSecurityKey, SetPeripheralSecurityKey
typedef struct _PeripheralSecurityKey_t
{
    uint32_t        ObjectID;
    uint8_t         szSecurityKey[16];
    uint8_t         nSecurityKeyLen;
} PeripheralSecurityKey_t;

// For GetPushRTMPLiveURL
typedef struct _PeripheralObject_t
{
    uint32_t        ObjectID;
} PeripheralObject_t;

// For Temp Notification Service
typedef struct _tagNotificationMessage
{
    int             MessageLen;
    uint8_t         pMessageData[0];
} NotificationMessage_t;

// For Job Done Report
typedef struct _tagJobInfo
{
    uint32_t        ObjectID;
    uint16_t        DeviceIdentificationNumber;
    uint16_t        PayLoadIdentificationNumber;
    int             JobStatus;
    uint16_t        Reserved;
    uint16_t        JobPayloadLen;
    uint8_t         *pJobPayload;
} JobDoneReport_t;

// For SetPeripheralQueryCollection
typedef struct _tagPeripheralQuery
{
    uint8_t         PMAC[8];
    uint16_t        DeviceType;
    uint16_t        PSignalStr;
} PeripheralQuery_t;

// For UpdatePeripheralDataPayloadToHistory
typedef struct _tagPeripheralDataPayloadInfo
{
    uint32_t        ObjectID;
    uint32_t        TimeStamp;
    /**
     * According to document v0.142 specification, added two variables, AdvObjID and CreatedByUserobjID
     *
     * AdvObjID: If the event is triggered by a advanced job, assign advanced job's ObjectID to AdvObjID. Otherwise, specific 0
     * CreatedByUserobjID: If the event is triggered by a user, assign user's ObjectID to CreatedByUserobjID. Otherwise, specific 0
     */
    uint32_t        AdvObjID;
    uint32_t        CreatedByUserobjID;
    uint8_t         AdData[PERIPHERAL_DATA_TYPE_PLAYLOAD_SIZE];
    uint16_t        AdDataLen;
} PeripheralDataPayloadInfo_t;


// For GetScheduleJob
typedef struct _ScheduleJobStatus
{
    uint64_t        updatetime;                         // The last modified/updated time of advance Jobs from requested gateway/central
    uint32_t        totalSchedules;
} ScheduleJobStatus_t;

typedef struct _tagScheduleJobInfo
{
    uint16_t        nIndex;                 // Start from 1, according to chapter 2.7.2 of document, DEXATEK's Server Programming Guide.
    uint64_t        nScheduleSEQNO;
    uint8_t         pPayload[0];
} ScheduleJobInfo_t;


typedef struct _tagAggregateHeaderFormat
{
    uint8_t                     HeaderID;
    uint8_t                     TotalPage:4;    // NOT checked in DKServerManger
    uint8_t                     CurrentPage:4;  // NOT checked in DKServerManger
    uint8_t                     AgDFormat:4;
    uint8_t                     Reserved1:3;
    uint8_t                     TimeFormat:1;
    uint8_t                     Reserved2;
} AggregateHeaderFormat_t;

typedef struct _tagAggregateDataHeader
{
    uint8_t         MAC[8];
    uint32_t        DataHeader;                 // AggregateHeaderFormat_t
} AggregateDataHeader_t;


// For GetAdvJobCondition
typedef struct _AdvanceJobStatus
{
    uint64_t        updatetime;                         // The last modified/updated time of advance Jobs from requested gateway/central
    uint32_t        totalAdvances;
} AdvanceJobStatus_t;

typedef struct _tagAdvJobInfo
{
    uint16_t        nIndex;                 // Start from 1, according to chapter 2.8.4 of document, DEXATEK's Server Programming Guide.
    uint32_t        nAdvJobID;
    uint8_t         pPayload[0];
} AdvJobInfo_t;


// For GetPeripheralByGMAC
// For GetPeripheralByGMAC
typedef struct _GatewayInfo
{
    uint32_t                    objectId;                           // int          4
    uint8_t                     macAddress[8];                      // char         8
    char                        wifiVersion[16];                    // char         16
    char                        bleVersion[16];                     // char         16
    uint8_t                     name[32];
    uint64_t                    updatetime;                         // The last modified/updated time of advance Jobs from requested gateway/central
    uint32_t                    totalPeripherals;      // The total number of peripherals
} GatewayInfo_t;

typedef struct _PeripheralInfo
{
    uint16_t                    index;                 // Start from 1, according to chapter 2.8.4 of document, DEXATEK's Server Programming Guide.
    uint8_t                     name[32];
    uint8_t                     numberOfStatus;
    DK_BLEPeripheral_t          peripheral;
    DK_BLEPeripheralStatus_t    peripheralStatus;
} PeripheralInfo_t;


// For UpdatePeripheralAggregationData
typedef struct _tagUTCLocalTimeTable
{
    uint32_t        UTCTime;
    uint32_t        LocalTime;
} UTCLocalTimeTable_t;

// For GetServerTime API
typedef struct _tagClientServerTime
{
    uint32_t        nServerTimeStamp;
    int8_t          nClientTimeZone;
    uint8_t         Reserved[3];
} ClientServerTime_t;

// For GetCurrentUser API
typedef struct _tagCurrentUserInfo
{
    uint8_t         PreferLang[16 + 1];     // PreferLang should be 16 bytes (defined in MySQL database). + 1 to include NULL character
} CurrentUserInfo_t;

// For IPCam API
typedef struct _tagIPCamEventFileREQ
{
    uint32_t        ObjectID;
    uint8_t         Type;
    uint8_t         Reserved[3];
    uint8_t         FileFormat[16];
    uint32_t        DateTime;
    uint32_t        AdvObjID;
} IPCamEventFileREQ_t;

typedef struct _tagIPCamEventFileREP
{
    uint16_t UploadURL_Offset;
    uint16_t UploadURL_Length;
    uint16_t DownloadURL_Offset;
    uint16_t DownloadURL_Length;
    uint16_t MessageData_Offset;
    uint16_t MessageData_Length;
    uint8_t  pPayload[0];
} IPCamEventFileREP_t;

typedef struct _tagIPCamOfflineFile
{
    uint32_t        ObjectID;
    uint8_t         FileFormat[16];
    uint32_t        RecordStartTime;
} IPCamOfflineFile_t;

/******************************************************
 *               Function Definitions
 ******************************************************/
DKEventHeader_t DKServerManager_GetEventObjHeader(void *ReportedEventArgObj);
uint8_t *DKServerManager_GetEventObjPayload(void *ReportedEventArgObj);
uint16_t DKServerManager_GetEventObjHTTPStatusCode(void *ReportedEventArgObj);
DKServer_StatusCode_t DKServerManager_CleanEventArgumentObj(void *ReportedEventArgObj);

DKServer_StatusCode_t DKServerManager_Init(dxEventHandler_t EventFunction, uint8_t *macaddr, int macaddrlen, uint8_t *dkServerName, uint8_t *appID, uint8_t *apiKey, uint8_t *apiVer);

/**
 * \param flag[in]  0 to diable; 1 to enable
 *
 * \retval DKSERVER_STATUS_SUCCESS
 */
DKServer_StatusCode_t DKServerManager_SetLongPoolFlag(uint8_t flag);

DKServer_StatusCode_t DKServerManager_State_Suspend(void);
DKServer_StatusCode_t DKServerManager_State_Resume(void);

/**
 * \brief In general, State Machine will change from SUSPENDING --> SUSPENDED.
 *        In some case of net_connect() = -68, State Machine will stay in SUSPENDING

 *
 * \param flag[in]  0 to diable; 1 to enable
 *
 * \retval DKSERVER_STATUS_SUCCESS
 */
DKServer_StatusCode_t DKServerManager_State_ForceToSuspended(void);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GATEWAYLOGIN
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_UNAUTHORIZE_LOGIN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GatewayLogin_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *gatewaykey);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_ANONYMOUSLOGIN
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_UNAUTHORIZE_LOGIN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_AnonymousLogin_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *accessToken, uint32_t accTokenTimeout);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_GETPERIPHERALBYGMACADDRESS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 *     Header (Send exactly once)
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETPERIPHERALBYGMACADDRESS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = <Length of pData>
 *     pData                    = +-----------------------------+---------------------+
 *                                | UserObjectID                | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *                                | GatewayID                   | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 *    Client ---------_ Server Manager ---------------------------------------- Cloud
 *      |                     |                                                   |
 *      + ------------------> + ---> DKMSG_GETPERIPHERALBYGMACADDRESS request --> |
 *      |                     |                                                   |
 *      | <--------(A)------- + <-- DKMSG_GETPERIPHERALBYGMACADDRESS response <-- +
 *      |          (B)        |
 *      |           :         |
 *      |           :         |
 *      | <--------(A)------- +
 *      |          (B)        |
 *      | <--------(C)------- +
 *
 *   (A) => DKMSG_GETPERIPHERALBYGMACADDRESS message with DKJOBTYPE_PERIPHERAL_INFO
 *   (B) => DKMSG_GETPERIPHERALBYGMACADDRESS message with DKJOBTYPE_CONTAINER_INFO (optional)
 *   (C) => DKMSG_GETPERIPHERALBYGMACADDRESS message with DKJOBTYPE_NONE
 */
DKServer_StatusCode_t DKServerManager_GetPeripheralByMAC_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int nStartFrom, int nMaxToReceive);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      updateTo   = DKSERVER_UPDATE_DATAPAYLOAD_TARGET_ALL (Request to update both BLEPeripheralStatus and BLEHistory tables)
 *                            DKSERVER_UPDATE_DATAPAYLOAD_TARGET_PERIPHERAL_TABLE_ONLY (Request to update BLEPeripheralStatus table only)
 *                            DKSERVER_UPDATE_DATAPAYLOAD_TARGET_HISTORY_TABLE_ONLY (Request to update BLEHistory table only)
 *     [in]      advObjID   = Advanced Job ObjectID. Must provide this value if updateTo is equal to DKSERVER_UPDATE_DATAPAYLOAD_TARGET_ALL or DKSERVER_UPDATE_DATAPAYLOAD_TARGET_HISTORY_TABLE_ONLY
 *                            0 will be omitted while sending to DKServer
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEPERIPHERALDATAPAYLOAD
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *     Header.DataLen           = 0
 */
DKServer_StatusCode_t DKServerManager_UpdatePeripheralDataPayload_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint8_t *datapayload, uint16_t payloadlen, DKServer_Update_DataPayload_Target_Info updateTo, uint32_t advObjID);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEPERIPHERALSTATUSINFO
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_SUPPORT_ARRAY_SIZE_OF_ONE_ONLY
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdatePeripheralStatusInfo_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint16_t signal, uint16_t battery, uint16_t status);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_STANDARD
 *                                DKJOBTYPE_ADVANCED_CONDITION
 *                                DKJOBTYPE_ADVANCED_EXECUTION
 *         .MessageType         = DKMSG_NEWJOBS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = <Length of pData>
 *     pData                    = +-----------------------------+---------------------+-----------------+
 *                                | ObjectID                    | ( 4 Bytes )         | DKStandardJob_t |
 *                                +-----------------------------+---------------------+                 |
 *                                | GatewayID                   | ( 4 Bytes )         |                 |
 *                                +-----------------------------+---------------------+                 |
 *                                | UserObjectID                | ( 4 Bytes )         |                 |
 *                                +-----------------------------+---------------------+                 |
 *                                | DeviceIdentificationNumber  | ( 2 Bytes )         |                 |
 *                                +-----------------------------+---------------------+                 |
 *                                | PayLoadIdentificationNumber | ( 2 Bytes )         |                 |
 *                                +-----------------------------+---------------------+                 |
 *                                | pPayload                    | ( Variable Length ) |                 |
 *                                +-----------------------------+---------------------+-----------------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_DATETIME_UPDATE
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = 8
 *     pData                    = +-----------+-------------+----------+
 *                                | year      | ( 2 Bytes ) | DKTime_t |
 *                                +-----------+-------------+          |
 *                                | month     | ( 1 Byte )  |          |
 *                                +-----------+-------------+          |
 *                                | day       | ( 1 Byte )  |          |
 *                                +-----------+-------------+          |
 *                                | hour      | ( 1 Byte )  |          |
 *                                +-----------+-------------+          |
 *                                | min       | ( 1 Byte )  |          |
 *                                +-----------+-------------+          |
 *                                | sec       | ( 1 Byte )  |          |
 *                                +-----------+-------------+          |
 *                                | dayofweek | ( 1 Byte )  |          |
 *                                +-----------+-------------+----------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_NEWJOBS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *                                DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetJobs_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_ADDGATEWAY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_AddGateway_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, char *deviceName, double latitude, double longitude, char *wifiv, char *blev);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_ADDPERIPHERAL
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_SUPPORT_ARRAY_SIZE_OF_ONE_ONLY
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen         = 0
 */
DKServer_StatusCode_t DKServerManager_AddPeripheral_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *macaddr, int macaddrlen, char *deviceName, int deviceType, uint16_t signal, uint16_t battery, char *fwv, uint8_t *seckey, uint8_t seckeylen);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_SENDNOTIFICATIONMESSAGE
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_SendNotificationMessage_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *message);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType            = DKJOBTYPE_NONE
 *         .MessageType        = DKMSG_RENEWSESSIONTOKEN
 *         .s.ReportStatusCode = DKSERVER_REPORT_STATUS_SUCCESS
 *                               DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                               DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                               DKSERVER_REPORT_STATUS_UNAUTHORIZE_LOGIN
 *                               DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *         .s.SystemErrorCode  = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                               DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                               DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                               DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_RenewSessionToken_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_JOBDONEREPORT
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_SUPPORT_ARRAY_SIZE_OF_ONE_ONLY
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_JobDoneReport_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, JobDoneReport_t *job, int NumOfJob);

#if 0 // Obsolete
/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEJOBSTATUSEXT
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdateJobStatusExt_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, JobDoneReport_t *job);
#endif // Obsolete

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSERVERINFO
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = <Depends on pData>
 *     pData                    = <An IP address string><0x00>
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSERVERINFO
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetServerInfo_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEGATEWAYFIRMWAREVERSION
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdateGatewayFirmwareVersion_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, char *wifiv, char *blev);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_CHANGEGATEWAYSTATUS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_ChangeGatewayStatus_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint16_t status);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEPERIPHERALFIRMARE
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdatePeripheralFirmware_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, char *fwv);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_SETPERIPHERALQUERYCOLLECTION
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_SetPeripheralQueryCollection_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, PeripheralQuery_t *peripheral);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSERVERTIME
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = 8
 *     pData                    = +----------------+ <== ClientServerTime_t
 *                                | ServerTime     | ( 4 Byte, uint32_t )
 *                                +----------------+
 *                                | ClientTimeZone | ( 1 Byte )
 *                                +----------------+
 *                                | Reserved       | ( 3 Byte )
 *                                +----------------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSERVERTIME
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetServerTime_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEDATAPAYLOADHISTORY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdatePeripheralDataPayloadToHistory_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, PeripheralDataPayloadInfo_t *payload, int count);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      key            = null terminated string
 *     [in]      pmszParams     = +----+-+----+-+----+----+-+-+
 *                                |$1$ |0|$2$ |0|... |$n$ |0|0|      where 0 indicated a null (0x00) character
 *                                +----+-+----+-+----+----+-+-+
 *     [in]      cbSize         = Character count from first byte to last 0x00
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_SENDMESSAGEWITHKEY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_SendMessageWithKey_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *key, uint8_t *pmszParams, int cbSize);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      key            = uint32_t
 *     [in]      category       = null terminated string
 *     [in]      resource       = null terminated string
 *     [in]      pmszParams     = +----+-+----+-+----+----+-+-+
 *                                |$1$ |0|$2$ |0|... |$n$ |0|0|      where 0 indicated a null (0x00) character
 *                                +----+-+----+-+----+----+-+-+
 *     [in]      cbSize         = Character count from first byte to last 0x00
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_SENDMESSAGEWITHKEYNUM
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_SendMessageWithKeyNum_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t key, uint8_t *category, uint8_t *resource, uint8_t *pmszParams, int cbSize);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      Type           = int
 *     [in]      resource       = null terminated string
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_NOTIFYEVENTDONE
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *                                DKSERVER_REPORT_STATUS_SERVER_IS_BUSY
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_NotifyEventDone( uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int Type, uint8_t *resource );

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETLASTFIRMWAREINFO
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = <Depends on pData>
 *     pData                    = +-----------+
 *                                | TOTAL_LEN | ( 2 Byte, length from 1st field ID1 to last DATA6 )
 *                                +-----------+
 *                                | ID1       | ( 1 Byte, reference DKFirmare_Info_ID_t )
 *                                +-----------+
 *                                | ITEM_LEN1 | ( 1 Byte, length of DATA1 )
 *                                +-----------+
 *                                | DATA1     | ( Variable length, 0x00 character included )
 *                                +-----------+
 *                                | ID2       | ( 1 Byte, reference DKFirmare_Info_ID_t )
 *                                +-----------+
 *                                | ITEM_LEN2 | ( 1 Byte, length of DATA2 )
 *                                +-----------+
 *                                | DATA2     | ( Variable length, 0x00 character included )
 *                                +-----------+
 *                                | ......    |
 *                                +-----------+
 *                                | ID6       | ( 1 Byte, reference DKFirmare_Info_ID_t )
 *                                +-----------+
 *                                | ITEM_LEN6 | ( 1 Byte, length of DATA6 )
 *                                +-----------+
 *                                | DATA6     | ( Variable length, 0x00 character included )
 *                                +-----------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETLASTFIRMWAREINFO
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *                                DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetLastFirmareInfo_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_FIRMWAREDOWNLOAD
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = <Length of pData>
 *     pData                    = +---+--------------------+-------------+------------------+
 *                                | r | nFirstBytePosition | ( 4 Bytes ) | structure of     |
 *                                + a +--------------------+-------------+ DKContentRange_t |
 *                                | n | nLastBytePosition  | ( 4 Bytes ) |                  |
 *                                + g +--------------------+-------------+                  |
 *                                | e | nFileSize          | ( 4 Byte )  |                  |
 *                                +---+--------------------+-------------+------------------+
 *                                | pData                  | ( Length is equal to the value specified with "Content-Length" in HTTP header)
 *                                +------------------------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_FIRMWAREDOWNLOAD
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_FirmwareDownload_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *pszurl, int byteposition, int bytestoread);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSCHEDULEJOB
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *         .DataLen             = <Depends on pData>
 *     pData                    = +-----------------+-------------+-------------------+
 *                                | nTotalSchedules | ( 4 Bytes ) | structure of      |
 *                                +-----------------+-------------+ ScheduleJobInfo_t |
 *                                | nIndex          | ( 2 Bytes ) |                   |
 *                                +-----------------+-------------+                   |
 *                                | nScheduleSEQNO  | ( 2 Bytes ) |                   |
 *                                +-----------------+-------------+-------------------+
 *                                | pPayload        | ( Variable length )
 *                                +-----------------+
 *
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETSCHEDULEJOB
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *                                DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetScheduleJob_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, int nStartFrom, int nMaxToReceive);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_DELETESCHEDULEJOB
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_DeleteScheduleJob_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint64_t nScheduleSEQNO);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_CLEARPERIPHERALSECURITYKEY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_ClearPeripheralSecurityKey_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_SETPERIPHERALSECURITYKEY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_SetPeripheralSecurityKey_Asynch (uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID, uint8_t *seckey, uint8_t seckeylen);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      pAgHeader      = +-----------+-------------+-----------------------+
 *                                | MAC       | ( 8 Bytes ) |                       |
 *                                +-----------+-------------+ AggregateDataHeader_t |
 *                                | HEADER    | ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *     [in]      pAgItem        = +-----------+-------------+-----------------------+
 *                                | DATA_ID   | ( 2 Byte )  |                       |
 *                                +-----------+-------------+                       |
 *                                | DURATION  | ( 2 Byte )  | AggregateItem_t       |
 *                                +-----------+-------------+                       |
 *                                | DATA_VALUE| ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *                                |               ......                            |
 *                                +-----------+-------------+-----------------------+
 *                                | DATA_ID   | ( 2 Byte )  |                       |
 *                                +-----------+-------------+                       |
 *                                | DURATION  | ( 2 Byte )  | AggregateItem_t       |
 *                                +-----------+-------------+                       |
 *                                | DATA_VALUE| ( 4 Byte )  |                       |
 *                                +-----------+-------------+-----------------------+
 *     [in]      nNumOfAgItem   = Number of AggregateItem_t item in pAgItem
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEPERIPHERALAGGREGATIONDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_SUPPORT_ARRAY_SIZE_OF_ONE_ONLY
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0

 */
DKServer_StatusCode_t DKServerManager_UpdatePeripheralAggregationData_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, AggregateDataHeader_t *pAgHeader, AggregateItem_t *pAgItem, int nNumOfAgItem);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      pAdvJobStatus  = +------------+-------------+-----------------------+
 *                                | nTimeStamp | ( 4 Byte )  |                       |
 *                                +------------+-------------+                       |
 *                                | AdvObjID   | ( 4 Byte )  | AdvJobStatusSrv_t     |
 *                                +------------+-------------+                       |
 *                                | JobStatus  | ( 1 Byte )  |                       |
 *                                +------------+-------------+-----------------------+
 *                                |                ......                            |
 *                                +------------+-------------+-----------------------+
 *                                | nTimeStamp | ( 4 Byte )  |                       |
 *                                +------------+-------------+                       |
 *                                | AdvObjID   | ( 4 Byte )  | AdvJobStatusSrv_t     |
 *                                +------------+-------------+                       |
 *                                | JobStatus  | ( 1 Byte )  |                       |
 *                                +------------+-------------+-----------------------+
 *
 *     [in]      nNumOfAdvJobItem = Number of AdvJobStatusSrv_t item in pAdvJobStatus
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATEADVJOBSTATUS
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_PARTIAL_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdateAdvJobStatus_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, AdvJobStatusSrv_t *pAdvJobStatus, int nNumOfAdvJobItem);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      nStartIndex    =
 *     [in]      nMaxRows       = The maximum number of AdvJobInfo_t to receive
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETADVJOBCONDITION
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = <Depends on pData>
 *     pData                    = +-----------------+-------------+-------------------+
 *                                | nTotalAdvJobs   | ( 4 Bytes ) | structure of      |
 *                                +-----------------+-------------+ AdvJobInfo_t      |
 *                                | nIndex          | ( 2 Bytes ) |                   |
 *                                +-----------------+-------------+                   |
 *                                | nAdvJobID       | ( 2 Bytes ) |                   |
 *                                +-----------------+-------------+-------------------+
 *                                | pPayload        | ( Variable length )
 *                                +-----------------+
 */
DKServer_StatusCode_t DKServerManager_GetAdvJobCondition_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint16_t nStartIndex, uint8_t nMaxRows);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      dkServerName   = API server URL string with null (0x00) terminated character.
 *                                Maximum length of dkServerName is 64
 *     [in]      apiVer         = API version. Null terminated string.
 *                                Maximum length of apiVer is 16
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_HOST_REDIRECT
 *         .s.ReportStatusCode  =
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_HostRedirect_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *dkServerName, uint8_t *apiVer);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in]      pLogData       = Log data with binary format.
 *                                Will be encoded with BASE64 before sending.
 *     [in]      cbLogDataLen   = Number of bytes of pLogData
 *
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_ADDSMARTHOMECENTRALLOG
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_AddSmartHomeCentralLog_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *pLogData, int cbLogDataLen);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_ADDSTANDALONEPERIPHERAL
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_AddStandalonePeripheral_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint8_t *macaddr, int macaddrlen, char *deviceName, double latitude, double longitude, int deviceType, uint16_t signal, uint16_t battery, char *fwv, uint8_t *seckey, uint8_t seckeylen);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_GETDATACONTAINERBYID
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETDATACONTAINERBYID
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_GetDataContainerByID_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont,
                                                                  dxContFormat_t format);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_CREATECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_CREATECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_CreateContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_UPDATECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdateContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_INSERTCONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_INSERTCONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_InsertContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_UPDATECONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_UPDATECONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_UpdateContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont,
                                                                  uint32_t advObjID,
                                                                  DKServer_Update_DataPayload_Target_Info updateTo);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_DELETECONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_DELETECONTAINERDDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_DeleteContainerDData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Header (Send 0 or multiple times)
 *         .SubType             = DKJOBTYPE_CONTAINER_INFO
 *         .MessageType         = DKMSG_DELETECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 4
 *     pData                    = +-----------------------------+---------------------+
 *                                | dxContainer_t *             | ( 4 Bytes )         |
 *                                +-----------------------------+---------------------+
 *
 * DKServerManager Event Report Message
 *     Header
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_DELETECONTAINERMDATA
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_DUPLICATE_MAC
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_INVALID_RANGE
 *                                DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 */
DKServer_StatusCode_t DKServerManager_DeleteContainerMData_Asynch(uint16_t SourceOfOrigin,
                                                                  uint64_t TaskIdentificationNumber,
                                                                  dxContainer_t *pCont);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * DKServerManager Event Report Message
 *     Heade
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETCURRENTUSER
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 16
 *     pData                    = +-----------------------------+---------------------+
 *                                | CurrentUserInfo_t *         | ( 16 Bytes )        |
 *                                +-----------------------------+---------------------+
 *
 */
DKServer_StatusCode_t DKServerManager_GetCurrentUser_Asynch(uint16_t SourceOfOrigin,
                                                            uint64_t TaskIdentificationNumber);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *     [in] EraseAllHistoryData     0 is default; 1 to remove all history data in CloudServer when deleting Gateway
 *     [in] RemoveRegionIfEmpty     0 is default; 1 to remove empty region
 *
 * DKServerManager Event Report Message
 *     Heade
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_DELETEGATEWAY
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_MISSING_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 *     pData                    = NULL
 *
 */
DKServer_StatusCode_t DKServerManager_DeleteGateway_Asynch(uint16_t SourceOfOrigin,
                                                           uint64_t TaskIdentificationNumber,
                                                           uint8_t EraseAllHistoryData,
                                                           uint8_t RemoveRegionIfEmpty);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *
 * DKServerManager Event Report Message
 *     Heade
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETPUSHRTMPLIVEURL
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = Length of pData
 *     pData                    = point to a URL string
 *
 */
/**
 * Example of parsing DKServerManager event report message:
<pre>
    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    if (EventHeader.MessageType == DKMSG_GETPUSHRTMPLIVEURL &&
        EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK &&
    	EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
    {
        uint8_t *pURL = DKServerManager_GetEventObjPayload(arg);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "liveURL:(%d)[%s]\n", EventHeader.DataLen, pURL);
    }
</pre>
 */
DKServer_StatusCode_t DKServerManager_GetPushRTMPLiveURL_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, uint32_t* ObjectID);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *
 * DKServerManager Event Report Message
 *     Heade
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETEVENTMEDIAFILEUPLOADPATH
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = Length of pData
 *     pData                    = +--------------------+---------------------+
 *                                | UploadURL_Offset   | 2 bytes             |-----+
 *                                +--------------------+---------------------+     |
 *                                | UploadURL_Length   | 2 bytes             |     |
 *                                +--------------------+---------------------+     |
 *                                | DownloadURL_Offset | 2 bytes             |----------------------------+
 *                                +--------------------+---------------------+     |                      |
 *                                | DownloadURL_Length | 2 bytes             |     |                      |
 *                                +--------------------+---------------------+     |                      |
 *                                | MessageData_Offset | 2 bytes             |---------------------------------------------------+
 *                                +--------------------+---------------------+     |                      |                      |
 *                                | MessageData_Length | 2 bytes             |     V                      V                      V
 *                                +--------------------+---------------------+     +----------------------+----------------------+----------------------+
 *                                | pPayload           | ( Variable Length ) | ==> |                      |                      |                      |
 *                                +--------------------+---------------------+     +----------------------+----------------------+----------------------+
 *                                                                                 ^                      ^                      ^                      ^
 *                                                                                 |                      |                      |                      |
 *                                                                                 +-- UploadURL_Length --+- DownloadURL_Length -+- MessageData_Length -+
 */
/**
 * Example of sending request message:
<pre>
    char *pFormat = "ts";
    IPCamEventFileREQ_t IpCamFile;
    memset(&IpCamFile, 0x00, sizeof(IpCamFile));

    IpCamFile.Type = 1;
    memcpy(IpCamFile.FileFormat, pFormat, strlen(pFormat));
    IpCamFile.DateTime = 234567;
    IpCamFile.AdvObjID = 3000;

    DKServerManager_GetEventMediaFileUploadPath_Asynch(GetSourceOfOrigin(),
                                                       GetTaskIdNumber(),
                                                       &IpCamFile);
</pre>
 */
/**
 * Example of parsing DKServerManager event report message:
<pre>
    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    if (EventHeader.MessageType == DKMSG_GETEVENTMEDIAFILEUPLOADPATH &&
        EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK &&
    	EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
    {
        IPCamEventFileREQ_t *p = DKServerManager_GetEventObjPayload(arg);
        if (p)
        {
            uint16_t nUploadURL_Length = DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length(p);
            uint16_t nDownloadURL_Length = DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length(p);
            uint16_t nMessageData_Length = DKServerManager_GetEventMediaFileUploadPath_MessageData_Length(p);
            uint8_t *pUploadURL = DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData(p);
            uint8_t *pDownloadURL = DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData(p);
            uint8_t *pMessageData = DKServerManager_GetEventMediaFileUploadPath_MessageData_GetData(p);

            if (pUploadURL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "UpLoadURL:(%d)[%s]\n", nUploadURL_Length, pUploadURL);
            }

            if (pDownloadURL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "DownloadURL:(%d)[%s]\n", nDownloadURL_Length, pDownloadURL);
            }

            if (pMessageData)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "MessageData:(%d)[%s]\n", nMessageData_Length, pMessageData);
            }
        }
    }
</pre>
 */
DKServer_StatusCode_t DKServerManager_GetEventMediaFileUploadPath_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, IPCamEventFileREQ_t *pIPCamEventData);

/**
 * DKServer_StatusCode_t        = DKSERVER_STATUS_SUCCESS
 *                                DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_STATUS_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_STATUS_ERROR_DX_RTOS
 *                                DKSERVER_STATUS_ERROR_QUEUE_IS_FULL
 *
 * Parameters
 *
 * DKServerManager Event Report Message
 *     Heade
 *         .SubType             = DKJOBTYPE_NONE
 *         .MessageType         = DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH
 *         .s.ReportStatusCode  = DKSERVER_REPORT_STATUS_SUCCESS
 *                                DKSERVER_REPORT_STATUS_INVALID_PARAMS
 *                                DKSERVER_REPORT_STATUS_INVALID_OBJECTID
 *                                DKSERVER_REPORT_STATUS_INVALID_SESSION_TOKEN
 *                                DKSERVER_REPORT_STATUS_INVALID_API_KEY
 *                                DKSERVER_REPORT_STATUS_ACCESS_DENIED
 *         .s.SystemErrorCode   = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
 *                                DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL
 *                                DKSERVER_SYSTEM_ERROR_JSON_INIT
 *                                DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND
 *         .DataLen             = 0
 *     pData                    = NULL
 *
 */
/**
 * Example of sending request message:
<pre>
    char *pFormat = "ts";
    IPCamOfflineFile_t IpCamFile;
    memset(&IpCamFile, 0x00, sizeof(IpCamFile));

    memcpy(IpCamFile.FileFormat, pFormat, strlen(pFormat));
    IpCamFile.RecordStartTime = 234567;

    DKServerManager_GetOfflineMediaFileUploadPath_Asynch(GetSourceOfOrigin(),
                                                         GetTaskIdNumber(),
                                                         &IpCamFile);
</pre>
 */
/**
 * Example of parsing DKServerManager event report message:
<pre>
    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    if (EventHeader.MessageType == DKMSG_GETOFFLINEMEDIAFILEUPLOADPATH &&
        EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK &&
    	EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
    {
        uint8_t *pURL = DKServerManager_GetEventObjPayload(arg);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "UpLoadURL:(%d)[%s]\n", EventHeader.DataLen, pURL);
    }
</pre>
 */
DKServer_StatusCode_t DKServerManager_GetOfflineMediaFileUploadPath_Asynch(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, IPCamOfflineFile_t *pIPCamEventData);

/**
 * @brief Get central's name
 */
DKServer_StatusCode_t DKServerManager_GetCentralName(uint8_t *DeviceName, int DeviceNameSize);

DKServer_StatusCode_t DKServerManager_GetTotalPeripheralNumbers(int *count);

DKServer_StatusCode_t DKServerManager_GetPeripheralByIndex(int index, uint8_t *macaddr, int macaddrlen, uint16_t* deviceType);
DKServer_StatusCode_t DKServerManager_GetPeripheralNameByIndex(int index, uint8_t *DeviceName, int DeviceNameSize);
DKServer_StatusCode_t DKServerManager_ChangePeripheralDeviceName(uint32_t objectID, uint8_t *pDeviceName);

/**
 * @brief return the length of "UploadURL"
 */
uint16_t DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length(IPCamEventFileREP_t *pEventFileREP);

/**
 * @brief return the address of "UploadURL"
 */
uint8_t *DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData(IPCamEventFileREP_t *pEventFileREP);

/**
 * @brief return the length of "DownloadURL"
 */
uint16_t DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length(IPCamEventFileREP_t *pEventFileREP);

/**
 * @brief return the address of "DownloadURL"
 */
uint8_t *DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData(IPCamEventFileREP_t *pEventFileREP);

/**
 * @brief return the length of "MessageData"
 */
uint16_t DKServerManager_GetEventMediaFileUploadPath_MessageData_Length(IPCamEventFileREP_t *pEventFileREP);

/**
 * @brief return the address of "MessageData"
 */
uint8_t *DKServerManager_GetEventMediaFileUploadPath_MessageData_GetData(IPCamEventFileREP_t *pEventFileREP);

/**
 * @method DKServerManager_GetPeripheralSecurityKeyByIndex
 * @param[in]      index        Index of Peripheral
 * @param[out]     seckey       Buffer to store Peripheral's SecurityKey
 * @param[in, out] seckeylen    The size of seckey. If function executing success, seckeylen will be the length of seckey
 */
DKServer_StatusCode_t DKServerManager_GetPeripheralSecurityKeyByIndex(int index, uint8_t *seckey, uint8_t *seckeylen);
DKServerManager_StateMachine_State_t DKServerManager_GetCurrentState(void);
uint32_t DKServerManager_GetAvailableTaskQueue(void);

/**
 * return NULL terminated string if success; otherwise NULL.
 */
uint8_t *DKServerManager_GetFirmwareItem(DKFirmare_Info_ID_t nID, uint8_t *pData);

/**
 * return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER   if any error occurs.
 *        DKSERVER_STATUS_SUCCESS
 */
DKServer_StatusCode_t DKServerManager_FirmwareVersionSplit(uint8_t *pszVersion, uint32_t *pnMajor, uint32_t *pnMinor, uint32_t *pnBuild);

/**
 * return DKSERVER_STATUS_INVALID_LOCAL_PARAMETER   if the data format of pszVerRuning or pszVerDownload are not correct
 *        DKSERVER_STATUS_FIRMWARE_VERSION_ERROR    if pszVerRuning is larger than pszVerDownload
 *        DKSERVER_STATUS_SUCCESS                   if pszVerRuning is smaller than pszVerDownload
 *        DKSERVER_STATUS_FIRMWARE_VERSION_EQUAL    if pszVerRuning and pszVerDownload are identical.
 */
DKServer_StatusCode_t DKServerManager_FirmwareVersionCompare(uint8_t *pszVerRuning, uint8_t *pszVerDownload);

/**
 * @brief The function use mutex to protect memory access.
 *        To prevent slowing the whole system performance, keep the access time as short as possible.
 *
 * @param nObjectID[in] Peripheral's ObjectID

 * @retval 0    Invalid
 * @retval 1    Valid
 */
uint8_t DKServerManager_IsValidPeripheralObjectID(uint32_t nObjectID);

/**
 * @brief The function use mutex to protect memory access.
 *        To prevent slowing the whole system performance, keep the access time as short as possible.
 *
 * @param wifiversion[out]      WiFi's firmware information
 * @param wifiversionlen[in]    Buffer length of wifiversion
 * @param bleversion[out]       BLE's firmware information
 * @param blelen[in]            Buffer length of bleversion
 *
 * @retval DKSERVER_STATUS_SUCCESS     If success
 */
DKServer_StatusCode_t DKServerManager_GetGatewayFirmwareVersion(uint8_t *wifiversion, int wifiversionlen, uint8_t *bleversion, int blelen);

/**
 * @brief The function use mutex to protect memory access.
 *        To prevent slowing the whole system performance, keep the access time as short as possible.
 *
 * @param phversion[out]        Peripheral's firmware information
 * @param phlen[in]             Buffer length of phversion
 *
 * @retval DKSERVER_STATUS_SUCCESS                  If success
 * @retval DKSERVER_STATUS_INVALID_LOCAL_PARAMETER  If peripheral not found.
 * @retval DKSERVER_STATUS_ERROR_BUFFER_TOO_SMALL   If the size of phversion is not enough to store the firmware information.
 */
DKServer_StatusCode_t DKServerManager_GetPeripheralFirmwareVersionByMAC(uint8_t *phversion, int phlen, uint8_t *macaddr, int macaddrlen);

/**
 * @brief Change the Get Job Interval dynamically according to needs..
 *        NewGetJobIntervalms must be 0 or value >= 1000
 *
 * @param NewGetJobInterval[in]         The new get job interval in milli-seconds.
 *                                      0 to disable get job request
 *
 * @retval DKSERVER_SUCCESS             If success
 * @retval DKSERVER_INVALID_PARAMETER   If new job interval not supported
 */
DKServer_StatusCode_t DKServerManager_ConfigGetJobInterval(uint32_t NewGetJobIntervalms);

/**
 * @brief Due to there is no RTC clock, we use server's UTC time and local system tick to calculate local time
 *
 * @param none
 *
 * @retval UTCLocalTimeTable_t *
 */
UTCLocalTimeTable_t *DKServerManager_GetUTCLocalTimeTable(void);

/**
 * @brief Get Server Hostname
 *
 * @param pHostname[out]                Server Hostname
 *
 * @retval DKSERVER_STATUS_SUCCESS              If success
 * @retval DKSERVER_STATUS_ERROR_ALLOC_MEMORY   If return Null pointer
 * @retval DKSERVER_STATUS_NOT_READY            If get hostname fail
 * @retval DKSERVER_INVALID_PARAMETER           If new job interval not supported
 */
DKServer_StatusCode_t DKServerManager_GetHostname( char **pHostname );

/**
 * @brief Send event to DKServerManager's worker thread
 */
dxOS_RET_CODE DKServerManager_SendEventToWorkerThread(uint16_t src, uint64_t taskid, DKMSG_Command_t msg, DKJobType_t type, uint16_t httpcode, uint16_t dkcode, void *pData, int DataLen);

/**
 * @brief Send event to DKServerManager's worker thread
 */
dxOS_RET_CODE DKServerManager_SendEventIDToWorkerThread(uint16_t src, uint64_t taskid, DKMSG_Command_t msg, uint16_t httpcode, uint16_t dkcode);

/**
 * @brief Check if Job Polling is enabled
 */
dxBOOL DKServerManager_IsJobPollingEnabled (void);

/**
 * @brief Enable/Disable Job Polling
 */
DKServer_StatusCode_t DKServerManager_SetJobPolling (
    dxBOOL jobPolling
);


#ifdef __cplusplus
}
#endif

#endif // _DKSERVERMANAGER_H
