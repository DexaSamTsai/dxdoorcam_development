//============================================================================
// File: DKAPI_GetPushRTMPLiveURL.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/15
//     1) Description: Initial
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetPushRTMPLiveURL.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETPUSHRTMPLIVEURL_BODY                                                          \
                                                    "{\"BLEObjectID\":%u}"
#define DKMSG_TEMPLATE_GETPUSHRTMPLIVEURL           "%s "                                               \
                                                    "%s/PushRTMPLive.php HTTP/1.1\r\n"                  \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GETPUSHRTMPLIVEURL_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetPushRTMPLiveURL_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    PeripheralObject_t* pPeripheral = NULL;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (PeripheralObject_t))) // sizeof(HTTPBuffer))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    pPeripheral = (PeripheralObject_t*) pCommMsg->pData;

    if (pPeripheral->ObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         pPeripheral->ObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETPUSHRTMPLIVEURL,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETPUSHRTMPLIVEURL_BODY, (unsigned int)pPeripheral->ObjectID),
            ObjectDictionary_GetSessionToken (),
            (unsigned int)pPeripheral->ObjectID);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetPushRTMPLiveURL_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data

    int resultnode = 0;                 // index of result

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    uint32_t objid = 0;                 // An variable to store the ObjectID which convert from strtoul()
    (void)objid;                        // Avoid compilation warning when ENABLE_SYSTEM_DEBUGGER is disabled
#endif

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_LIVEURL, 0};

    if (pParameters == NULL ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (PeripheralObject_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    resultnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (resultnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get liveURL

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList3[0]);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "liveURL: %s\n", szBuffer);
#endif

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Remove JSON escape character
    szBuffer[len] = 0x00;
    len = DKMSG_JSONRemoveEscapeChar((uint8_t *)szBuffer, STRING_BUFFER_SIZE, (uint8_t *)szBuffer);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETPUSHRTMPLIVEURL,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)&szBuffer[0],
                                            len);

    return DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
}
