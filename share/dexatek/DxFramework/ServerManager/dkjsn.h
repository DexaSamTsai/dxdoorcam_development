//============================================================================
// File: dkjsn.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2017/08/11
//     1) Description: Under cross platform consideration, revise data type of
//                     "handle" to "long" instead of "int"
//                     +--------+-----------+-----------+
//                     |        |  Windows  |   Linux   |
//                     |        +-----+-----+-----+-----+
//                     |        | x86 | x64 | x86 | x64 |
//                     +--------+-----+-----+-----+-----+
//                     | int    |   4 |   4 |   4 |   4 |
//                     | long   |   4 |   4 |   4 |   8 |
//                     | size_t |   4 |   8 |   4 |   8 |
//                     | time_t |   4 |   8 |   4 |   8 |
//                     +--------+-----+-----+-----+-----+
//                     The "handle" variable is a pointer not just a numeric variable.
//                     So, the size of "handle" must match MCU's bus width.
//                     For example, in 32-bit MCU, "handle" can be "int" or "long".
//                                  in 64-bit MCU, "handle" must be "long" (in Linux)
//        Modified:
//            - DKJSN_Init()
//            - DKJSN_GetObjectType()
//            - DKJSN_GetObjectIndexForKey()
//            - DKJSN_GetObjectCount()
//            - DKJSN_SelectObjectByIndex()
//            - DKJSN_GetObjectStringLength()
//            - DKJSN_GetObjectString()
//            - DKJSN_GetObjectStringLengthByList()
//            - DKJSN_GetObjectStringByList()
//            - DKJSN_SelectObjectByList()
//            - DKJSN_Uninit()
//============================================================================
#ifndef _DKJSN_H
#define _DKJSN_H
#include "jsmn.h"
#include "dxArch.h"

#define MAX_NUM_JSON_ELEMENT_SUPPORT                2500 //400 - 16 device, 300 - 12 device

#pragma once

#include "dxOS.h"

#ifdef __cplusplus
extern "C" {
#endif

dxPTR_t DKJSN_Init(const char *pszJSON, jsmntok_t* tokenTable, int tokenTableSize);
int DKJSN_GetObjectType(dxPTR_t handle, int node);
int DKJSN_GetObjectIndexForKey(dxPTR_t handle, int node, const char *pszKey);
int DKJSN_GetObjectCount(dxPTR_t handle, int node);
int DKJSN_SelectObjectByIndex(dxPTR_t handle, int node, int index);
int DKJSN_GetObjectStringLength(dxPTR_t handle, int node);
char *DKJSN_GetObjectString(dxPTR_t handle, int node);

int DKJSN_GetObjectStringLengthByList(dxPTR_t handle, int findList[], int node);
char *DKJSN_GetObjectStringByList(dxPTR_t handle, int findList[], int node);
int DKJSN_SelectObjectByList(dxPTR_t handle, int findList[], int node);

void DKJSN_Uninit(dxPTR_t handle);


#ifdef __cplusplus
}
#endif

#endif // _DKJSN_H
