//============================================================================
// File: HTTPManager.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/02/19
//     1) Description: Fix a potential issue with printing garbage information.
//                     Especially, GetLastFirmwareInfo.php
//        Modified:
//            - HTTPManager_Send_Asynch()
//
//     Version: 0.4
//     Date: 2016/02/24
//     1) Description: Fix an issue with ssl_read() will never return while network is down
//        Modified:
//            - _Receive()
//     2) Description: Fix an issue with NO NULL-terminated character.
//        Modified:
//            - HTTPManager_Send_Asynch()
//     3) Description: Add a new function
//        Modified:
//        Added:
//            - HTTPManager_Disconnect()
//
//     Version: 0.5
//     Date: 2016/03/14
//     1) Description: Remove "NOTE: Porting to dxOS" comment
//
//     Version: 0.6
//     Date: 2016/04/06
//     1) Description: Fixed issue with state changing notification
//        Modified:
//            - _Receive()
//            - HttpThread_Loop()
//
//     Version: 0.7
//     Date: 2016/04/07
//     1) Description: Fixed network recovery issue
//        Modified:
//            - _Connect()
//            - HttpThread_Loop()
//
//     Version: 0.8
//     Date: 2016/05/04
//     1) Description: Clear Receive buffer before receiving data
//        Modified:
//            - _Receive()
//
//     Version: 0.9
//     Date: 2016/05/06
//     1) Description: Fixed issue when receving multiple part of HTTP content
//        Modified:
//            - _Receive()
//
//     Version: 0.10
//     Date: 2016/05/27
//     1) Description: Added LastAccessTick to HTTPThreadParam_t
//        Added:
//            - HTTPManager_GetLastAccessTime()
//        Modified:
//            - _Connect()
//            - _Send()
//            - _Receive()
//            - _Disconnect()
//
//     Version: 0.11
//     Date: 2016/10/19
//     1) Description: Force variable to be placed at SDRAM
//        Modified:
//            - Declaration of variable HTTPThreadParam
//
//     Version: 0.12
//     Date: 2017/03/28
//     1) Description: Revise Error Code
//        Modified:
//            - _Connect()
//            - _Send()
//            - _Receive()
//            - HttpThread_Loop()
//            - HTTPManager_GetEventObjErrorCode()
//            - HTTPManager_CleanEventArgumentObj()
//            - HTTPManager_SendEventToWorkerThread()
//            - HTTPManager_SendEventIDToWorkerThread()
//            - HTTPManager_Init()
//            - HTTPManager_Send_Asynch()
//
//     Version: 0.13
//     Date: 2017/06/29
//     1) Description: Fixed typo issue
//        Modified:
//            - HTTPManager_Connect_Asynch()
//            - HTTPManager_Resume_Asynch()
//     2) Description: Added debug log information when fatal issue occurred
//        Modified:
//            - _HTMPushMessageToQueue()
//            - _Receive()
//            - HTTPManager_SendEventToWorkerThread()
//            - HTTPManager_Receive_Asynch()
//            - HTTPManager_Connect_Asynch()
//            - HTTPManager_Send_Asynch()
//            - HTTPManager_Disconnect_Asynch()
//            - HTTPManager_Suspend_Asynch()
//            - HTTPManager_Resume_Asynch()
//        Added:
//            - _HTMEmptyQueue()
//
//     Version: 0.14
//     Date: 2017/08/10
//     1) Description: Use message queue instead of event group for compatible with Linux.
//        Modified:
//            - HTTPThreadParam_t
//            - HTTPThreadParam
//            - _Receive()
//            - HTTPManager_Init()
//            - HTTPManager_Receive_Stop()
//============================================================================
//#include <platform/platform_stdlib.h>
#include "dxArch.h"
#include "Utils.h"
#include "dk_hex.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKServerManagerConfig.h"
#include "dxNET.h"
#include "dxSSL.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define SP                                          0x20
#define HT                                          0x09
#define CR                                          0x0D
#define LF                                          0x0A
#define HTTP_CONTENT_LENGTH_STRING                  "Content-Length:"
#define ENABLE_SEEK_HTTP_CONTENT_LENGTH             1

#define HTTP_EVENT_STOP_RECEIVING                   (1 << 0)
#define HTTP_ALL_EVENTS                             (HTTP_EVENT_STOP_RECEIVING)

//#define FORCE_DISCONNECT_RECONNECT_AFTER_ERROR

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagHTTPThreadParam
{
    dxTaskHandle_t                      Handle;
    dxQueueHandle_t                     Queue;
    HTWorker_t                          Worker;
    uint32_t                            TimeoutInSec;
    dxTime_t                            LastAccessTick;
    uint8_t                             szBuffer[HTTP_BUFFER_SIZE];

    dxMutexHandle_t                     mutex;
    dxQueueHandle_t                     EventQueue;
    HTTPManager_StateMachine_State_t    state;

    dxSSLHandle_t                       *socket;
} HTTPThreadParam_t;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern int _IsCRLF(char *pszText);

static TASK_RET_TYPE HttpThread_Loop(TASK_PARAM_TYPE arg);

dxOS_RET_CODE _Connect(HTTPThreadParam_t *pThreadParam, dxIP_Addr_t *ip, uint16_t port, uint32_t timeout);
dxOS_RET_CODE _Send(HTTPThreadParam_t *pThreadParam, uint8_t *pData, uint16_t cbData);
dxOS_RET_CODE _Disconnect(HTTPThreadParam_t *pThreadParam);

/******************************************************
 *               Variable Definitions
 ******************************************************/
#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes = @ ".sdram.text"
#endif // CONFIG_SDRAM_BUFFER_ALLOC
static HTTPThreadParam_t HTTPThreadParam =
{
    NULL,                                   // Handle
    NULL,                                   // Queue
    {                                       // HTWorker_t
        NULL,                               //      Handle
        NULL                                //      RegisterFunc
    },
    100,                                    // TimeoutInSec
    0,                                      // LastAccessTick
    { 0x00 },                               // szBuffer
    NULL,                                   // mutex
    NULL,                                   // EventQueue
    HTTPMANAGER_STATEMACHINE_STATE_UNKNOWN, // state
    NULL                                    // socket
};
#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes =
#endif // CONFIG_SDRAM_BUFFER_ALLOC

/******************************************************
 *               Function Implementation - Local
 ******************************************************/
void _FreeHTQCommandObj(HTQCommMsg_t *obj)
{
    if (obj)
    {
        if (obj->pData)
        {
            dxMemFree(obj->pData);
            obj->pData = NULL;
        }

        dxMemFree(obj);
        obj = NULL;                 // CANNOT use memset(obj, 0x00, sizeof(DKRESTCommMsg_t));
    }
}

void _HTMEmptyQueue(void)
{
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;
    dxOS_RET_CODE result = DXOS_SUCCESS;

    if (pThreadParam->Queue)
    {
        while (result == DXOS_SUCCESS)
        {
            HTQCommMsg_t *pBuffer = NULL;
            result = dxQueueReceive(pThreadParam->Queue,
                                    (void *)&pBuffer,
                                    0);
            _FreeHTQCommandObj(pBuffer);
        }
    }
}

dxOS_RET_CODE _HTMPushMessageToQueue(HTQCommMsg_t **msg)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;

    if (pThreadParam->Queue)
    {
        if (dxQueueIsFull(pThreadParam->Queue) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, dxQueueIsFull() == dxTRUE\n",
                             __FUNCTION__,
                             __LINE__);

            dxMemFree((*msg)->pData);
            (*msg)->pData = NULL;
            dxMemFree((*msg));
            (*msg) = NULL;

            result = DXOS_LIMIT_REACHED;
        }
        else
        {
            if (dxQueueSend(pThreadParam->Queue, msg, 1000) != DXOS_SUCCESS)
            {
                dxMemFree((*msg)->pData);
                (*msg)->pData = NULL;
                dxMemFree((*msg));
                (*msg) = NULL;

                result = DXOS_ERROR;
            }
        }
    }

    return result;
}

dxOS_RET_CODE _Connect(HTTPThreadParam_t *pThreadParam, dxIP_Addr_t *ip, uint16_t port, uint32_t timeout)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;

    if (pThreadParam->state != HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED)
    {
        return DXOS_INVALID;
    }

    LOCK(&pThreadParam->mutex);

    pThreadParam->socket = dxSSL_Connect (ip, port, NULL, DXSSL_CLIENT, DXSSL_VERIFY_NONE);

    UNLOCK(&pThreadParam->mutex);

    if (pThreadParam->socket == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] dxSSL_Connect() failed\n", __FUNCTION__);

        dxTimeDelayMS(100);

        return DXOS_ERROR;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] port: %d\n", __FUNCTION__, port);
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] timeout: %d\n", __FUNCTION__, (int)timeout);
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] ip: %d.%d.%d.%d\n",
                     __FUNCTION__,
                     ip->v4[0],
                     ip->v4[1],
                     ip->v4[2],
                     ip->v4[3]);

    if (pThreadParam->socket)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "[%s] _Connect() SUCCESS\n", __FUNCTION__);
    }

    LOCK(&pThreadParam->mutex);
    pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_CONNECTED;
    pThreadParam->LastAccessTick = dxTimeGetMS();
    UNLOCK(&pThreadParam->mutex);

    return result;
}

dxOS_RET_CODE _Send(HTTPThreadParam_t *pThreadParam, uint8_t *pData, uint16_t cbData)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    int ret = 0;

    if (pThreadParam->state != HTTPMANAGER_STATEMACHINE_STATE_CONNECTED)
    {
        return DXOS_INVALID;
    }

    LOCK(&pThreadParam->mutex);
    pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_SENDING;
    UNLOCK(&pThreadParam->mutex);

    ret = dxSSL_Send(pThreadParam->socket, pData, cbData);

    result = (ret > 0) ? DXOS_SUCCESS : DXOS_ERROR;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] dxSSL_Send(): %d\n", __FUNCTION__, ret);

    //GSysDebugger_DumpString (strlen (pData), pData);

    LOCK(&pThreadParam->mutex);
    pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_CONNECTED;
    pThreadParam->LastAccessTick = dxTimeGetMS();
    UNLOCK(&pThreadParam->mutex);

    if (result != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "[%s] dxSSL_Send(): 0x%X\n", __FUNCTION__, result);
    }

    return result;
}

dxOS_RET_CODE _Receive(HTTPThreadParam_t *pThreadParam, uint8_t *pData, uint32_t *cbData, uint32_t timeout)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    dxOS_RET_CODE event_result = DXOS_SUCCESS;

    uint32_t buffer_size = *cbData;
    uint32_t total_data_length  = 0;
#if ENABLE_SEEK_HTTP_CONTENT_LENGTH
    uint8_t *pBufferHead = pData;
    uint8_t *pContentLength = NULL;
    int nContentLength = 0;
    int nHeaderLength = 0;
    int i = 0;
    int n = 0;
    char ch = 0;
#endif // ENABLE_SEEK_HTTP_CONTENT_LENGTH

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] RECV timeout: %d\n", __FUNCTION__, timeout);

    LOCK(&pThreadParam->mutex);
    pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_RECEIVING;

    if (pData && *cbData > 0)
    {
        memset(pData, 0x00, *cbData);
    }

    UNLOCK(&pThreadParam->mutex);

    int rx_timeout = timeout;
    int ret = 0;

    do
    {
        uint32_t value = 0;

        event_result = dxQueueReceive(pThreadParam->EventQueue,
                                      (void *)&value,
                                      0);

        if (event_result == DXOS_SUCCESS && (value & HTTP_EVENT_STOP_RECEIVING))
        {
            // Event occurred.

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] HTTP_EVENT_STOP_RECEIVING received\n", __FUNCTION__);

            result = DXOS_ABORTED;
            break;
        }
        else
        {
            // No event occurred
        }

        ret = dxSSL_ReceiveTimeout(pThreadParam->socket,
                                   &pData[total_data_length],
                                   buffer_size - total_data_length,
                                   rx_timeout);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "[%s] dxSSL_ReceiveTimeout(): %d\n", __FUNCTION__, ret);

        if (ret < 0) {

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, dxSSL_ReceiveTimeout() TIMEOUT occurred. total_data_length: %d\n",
                             __FUNCTION__,
                             __LINE__,
                             total_data_length);

            result = DXOS_TIMEOUT;
            break;
        }
        else if (ret == 0) {

            continue;
        }
        else {

            total_data_length += ret;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] Line %d, total_data_length: %d\n",
                             __FUNCTION__,
                             __LINE__,
                             total_data_length);

#if ENABLE_SEEK_HTTP_CONTENT_LENGTH
            // We just receive data from HOST, parse the HTTP Header Message and get the Content-Length
            pContentLength = (uint8_t *)strstr((const char *)pBufferHead, HTTP_CONTENT_LENGTH_STRING);

            if (pContentLength != NULL && pContentLength > pBufferHead) {

                if (pContentLength != NULL && pContentLength > pBufferHead) {

                    ch = *(pContentLength - 1);
                    if (ch == CR || ch == LF) {

                        nContentLength = (int)atol((char *)&pContentLength[strlen(HTTP_CONTENT_LENGTH_STRING)]);
                        i = 0;

                        while (i < total_data_length) {

                            n = _IsCRLF((char *)pBufferHead + i);

                            if (n == 0) {

                                i++;
                                continue;
                            }
                            else {

                                i += n;
                            }

                            if (n > 0 && i < total_data_length) {

                                n = _IsCRLF((char *)pBufferHead + i);

                                if (n > 0) {

                                    nHeaderLength = i + n;
                                    break;
                                }
                                else {

                                    i++;
                                }
                            }
                        }
                    }
                }
            }

            if (nHeaderLength > 0 && nContentLength > 0) {

                if (total_data_length >= nHeaderLength + nContentLength) {

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "Line %d, Header Length: %d, Content-Length: %d, Received Length: %d\n",
                                     __LINE__,
                                     nHeaderLength,
                                     nContentLength,
                                     (int)total_data_length);

                    result = DXOS_SUCCESS;
                    break;
                }
            }
#endif // ENABLE_SEEK_HTTP_CONTENT_LENGTH

            // Use the new timeout value to make sure there is no more data from server.
        }
    } while (result == DXOS_SUCCESS);

    LOCK(&pThreadParam->mutex);

    if (result == DXOS_SUCCESS)
    {
        *cbData = total_data_length;
        pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_CONNECTED;
    }
    else
    {
        // Socket error
        *cbData = 0;

        if (dxSSL_Close(pThreadParam->socket) == DXSSL_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] dxSSL_Close()\n", __FUNCTION__);
        }

        pThreadParam->socket = NULL;

        pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED;
    }

    pThreadParam->LastAccessTick = dxTimeGetMS();

    pData[total_data_length] = 0x00;

    UNLOCK(&pThreadParam->mutex);

    return result;
}

dxOS_RET_CODE _Disconnect(HTTPThreadParam_t *pThreadParam)
{

    if (pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_CONNECTED ||
        pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_SENDING ||
        pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_RECEIVING)
    {
        LOCK(&pThreadParam->mutex);
        pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_DISCONNECTING;

        if (dxSSL_Close(pThreadParam->socket) == DXSSL_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] dxSSL_Close()\n", __FUNCTION__);
        }

        pThreadParam->socket = NULL;

        pThreadParam->state = HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED;
        pThreadParam->LastAccessTick = dxTimeGetMS();
        UNLOCK(&pThreadParam->mutex);

        dxTimeDelayMS(500);
    }

    return DXOS_SUCCESS;
}

/******************************************************
 *               Function Implementation - Static
 ******************************************************/
static TASK_RET_TYPE HttpThread_Loop(TASK_PARAM_TYPE arg)
{
    static dxIP_Addr_t ip = {0};
    static uint16_t port = 0;
    static uint32_t timeout = {0};
    dxBOOL bExit = dxFALSE;

    dxOS_RET_CODE result = DXOS_SUCCESS;
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)arg;
    uint8_t *HTTPBuffer = (uint8_t *)pThreadParam->szBuffer;
    uint32_t HTTPBuffer_Size = sizeof(pThreadParam->szBuffer);

    int f = 0;
    int i = 0;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Started!, sizeof(szBuffer): %d bytes\n", __FUNCTION__, (int)sizeof(pThreadParam->szBuffer));

    while (bExit == dxFALSE)    // Thread loop
    {
        if (pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_SENDING ||
            pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_RECEIVING ||
            pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_DISCONNECTING)
        {
            dxTimeDelayMS(0);
            continue;
        }

        HTQCommMsg_t *pBuffer = NULL;

        result = dxQueueReceive(pThreadParam->Queue,
                                (void *)&pBuffer,
                                THREAD_POP_QUEUE_TIMEOUT_TIME);

        if (result == DXOS_SUCCESS)
        {
            f = pBuffer->Header.MessageType;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "[%s] HMType: %d, State: %d\n", __FUNCTION__, f, pThreadParam->state);

            switch(pThreadParam->state)
            {
            case HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED:
                if (f == HTTPMANAGER_COMMAND_CONNECT ||
                    f == HTTPMANAGER_COMMAND_RESUME)
                {
                    if (pBuffer->Header.DataLen > 0 && pBuffer->pData)
                    {
                        i = 0;
                        memcpy(&ip, &pBuffer->pData[i], sizeof(ip));
                        i += sizeof(ip);

                        memcpy(&port, &pBuffer->pData[i], sizeof(port));
                        i += sizeof(port);

                        memcpy(&timeout, &pBuffer->pData[i], sizeof(timeout));
                        i += sizeof(timeout);

                        result = _Connect(pThreadParam, &ip, port, timeout);

                        HTTPManager_SendEventIDToWorkerThread(f, 0, result);
                    }
                }
                else if (f == HTTPMANAGER_COMMAND_DISCONNECT ||
                         f == HTTPMANAGER_COMMAND_SUSPEND)
                {
                    result = _Disconnect(pThreadParam);

                    // Due to HTTPS disconnected, need to notify DKServerManager
                    HTTPManager_SendEventIDToWorkerThread(HTTPMANAGER_COMMAND_DISCONNECT, 0, DXOS_SUCCESS);

                    dxTimeDelayMS(100);

                    HTTPManager_SendEventIDToWorkerThread(f, 0, result);
                }
                break;
            case HTTPMANAGER_STATEMACHINE_STATE_CONNECTED:
                if (f == HTTPMANAGER_COMMAND_SEND)
                {
                    timeout = pBuffer->Header.TCPWaitTimeInSec;     // The value will be used in HTTPMANAGER_COMMAND_RECEIVE but must pass from HTTPMANAGER_COMMAND_SEND

                    if (pBuffer->Header.DataLen > 0 && pBuffer->pData)
                    {
                        result = _Send(pThreadParam, pBuffer->pData, pBuffer->Header.DataLen);

                        HTTPManager_SendEventIDToWorkerThread(f, timeout, result);
                    }
                }
                else if (f == HTTPMANAGER_COMMAND_RECEIVE)
                {
                    HTTPBuffer_Size = sizeof(pThreadParam->szBuffer);
                    result = _Receive(pThreadParam, HTTPBuffer, &HTTPBuffer_Size, pBuffer->Header.TCPWaitTimeInSec);

#if defined(FORCE_DISCONNECT_RECONNECT_AFTER_ERROR)
                    if (result == DXOS_TIMEOUT)
                    {
                        // Due to HTTPS disconnected, need to notify DKServerManager
                        HTTPManager_SendEventIDToWorkerThread(HTTPMANAGER_COMMAND_DISCONNECT, 0, DXOS_SUCCESS);

                        dxTimeDelayMS(100);
                    }
#endif // FORCE_DISCONNECT_RECONNECT_AFTER_ERROR

                    HTTPManager_SendEventToWorkerThread(f, 0, result, HTTPBuffer, HTTPBuffer_Size);
                }
                else if (f == HTTPMANAGER_COMMAND_DISCONNECT ||
                         f == HTTPMANAGER_COMMAND_SUSPEND)
                {
                    result = _Disconnect(pThreadParam);

                    // Due to HTTPS disconnected, need to notify DKServerManager
                    HTTPManager_SendEventIDToWorkerThread(HTTPMANAGER_COMMAND_DISCONNECT, 0, DXOS_SUCCESS);

                    dxTimeDelayMS(100);

                    HTTPManager_SendEventIDToWorkerThread(f, 0, result);
                }
                else if (f == HTTPMANAGER_COMMAND_RESUME)
                {
                    HTTPManager_SendEventIDToWorkerThread(f, 0, DXOS_SUCCESS);
                }
                break;
            case HTTPMANAGER_STATEMACHINE_STATE_SENDING:
            case HTTPMANAGER_STATEMACHINE_STATE_RECEIVING:
                // Do nothing
                break;
            default:
                break;
            }
        }

        _FreeHTQCommandObj(pBuffer);
    }

    END_OF_TASK_FUNCTION;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
HTQHeader_t HTTPManager_GetEventObjHeader(void *ReportedEventArgObj)
{
    HTQHeader_t Header;
    Header.MessageType = HTTPMANAGER_COMMAND_UNKNOWN;

    if (ReportedEventArgObj)
    {
        HTEventReport_t *EventObj = (HTEventReport_t *)ReportedEventArgObj;
        memcpy(&Header, &EventObj->Header, sizeof(HTQHeader_t));
    }

    return Header;
}

uint8_t *HTTPManager_GetEventObjPayload(void *ReportedEventArgObj)
{
    uint8_t *pData = NULL;

    if (ReportedEventArgObj)
    {
        HTEventReport_t *EventObj = (HTEventReport_t *)ReportedEventArgObj;
        if (EventObj->Header.DataLen > 0)
        {
            pData = EventObj->pData;
        }
    }

    return pData;
}

int16_t HTTPManager_GetEventObjErrorCode(void *ReportedEventArgObj)
{
    int16_t nCode = DXOS_INVALID;

    if (ReportedEventArgObj)
    {
        HTEventReport_t *EventObj = (HTEventReport_t *)ReportedEventArgObj;
        nCode = EventObj->ErrorCode;
    }

    return nCode;
}

uint16_t HTTPManager_GetEventObjHTTPStatusCode(void *ReportedEventArgObj)
{
    uint16_t nCode = 0xFFFF;

    if (ReportedEventArgObj)
    {
        HTEventReport_t *EventObj = (HTEventReport_t *)ReportedEventArgObj;
        nCode = EventObj->HTTPStatusCode;
    }

    return nCode;
}

dxOS_RET_CODE HTTPManager_CleanEventArgumentObj(void *ReportedEventArgObj)
{
    if (ReportedEventArgObj == NULL)
    {
        return DXOS_INVALID;
    }

    HTEventReport_t *EventObj = (HTEventReport_t *)ReportedEventArgObj;

    if (EventObj->pData)
    {
        dxMemFree(EventObj->pData);
        EventObj->pData = NULL;
    }

    dxMemFree(EventObj);
    EventObj = NULL;

    return DXOS_SUCCESS;
}

dxOS_RET_CODE HTTPManager_SendEventToWorkerThread(uint8_t nMessageType, uint32_t timeout, int16_t/*uint8_t*/ nErrorCode, void *pData, int DataLen)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;

    HTEventReport_t *obj = (HTEventReport_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTEventReport_t));

    if (obj)
    {
        obj->Header.MessageType = nMessageType;
        obj->Header.TCPWaitTimeInSec = timeout;
        obj->ErrorCode = nErrorCode;

        if (pData != NULL && DataLen > 0)
        {
            int n = 0;
            int ret = 0;
            DKHTTPStatus DKHSCode = {{0}, 0, {0}};
            ret = DK_GetHTTPResponseStatus(pData, &DKHSCode, &n);
            if (ret > 0 && n == 0)
            {
                obj->HTTPStatusCode = DKHSCode.nStatusCode;
            }
            DK_FreeStatus(&DKHSCode);

            obj->Header.DataLen = (DataLen & 0x0FFFF);
            obj->pData = dxMemAlloc(HTTP_THREAD_NAME, 1, DataLen + 1);

            if (obj->pData)
            {
                memcpy(obj->pData, pData, DataLen);
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 DataLen + 1);
            }
        }

        result = dxWorkerTaskSendAsynchEvent(pThreadParam->Worker.Handle,
                                             pThreadParam->Worker.RegisterFunc,
                                             obj);
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                          __FUNCTION__,
                          __LINE__,
                          (int)sizeof(HTEventReport_t));
    }


    return result;
}

dxOS_RET_CODE HTTPManager_SendEventIDToWorkerThread(uint8_t nMessageType, uint32_t timeout, int16_t/*uint8_t*/ nErrorCode)
{
    return HTTPManager_SendEventToWorkerThread(nMessageType, timeout, nErrorCode, NULL, 0);
}

uint8_t HTTPManager_GetCurrentState(void)
{
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;
    uint8_t state = (uint8_t)HTTPMANAGER_STATEMACHINE_STATE_UNKNOWN;
    LOCK(&pThreadParam->mutex);
    state = pThreadParam->state;
    UNLOCK(&pThreadParam->mutex);

    return state;
}

dxOS_RET_CODE HTTPManager_Init(dxEventHandler_t EventFunction)
{
    dxOS_RET_CODE result = DXOS_ERROR;

    if (EventFunction == NULL)
    {
        return DXOS_INVALID;//DXOS_ERROR;
    }

    // Initialize mutex
    HTTPThreadParam.mutex = dxMutexCreate();

    // Initialize Queue
    if (HTTPThreadParam.Queue == NULL)
    {
        HTTPThreadParam.Queue = dxQueueCreate(HTTP_THREAD_QUEUE_MAX_ALLOWED,
                                              sizeof(HTQCommMsg_t *));

        if (HTTPThreadParam.Queue == NULL)
        {
            return DXOS_ERROR;
        }
    }

    // Initialize thread
    HTTPThreadParam.Handle = NULL;

# if 1
static u32				*task_handler_stk;
static int				task_handler_stksize = 10240;
static t_ertos_task		task_handler_tcb = {
		.flags	= 0,
	};

	task_handler_stk	= ( u32 * )malloc( ( sizeof( u32 ) * task_handler_stksize ) );

	ertos_task_create_internal( HttpThread_Loop, HTTP_THREAD_NAME, task_handler_stksize, ( void * )&HTTPThreadParam, 10, NULL, &task_handler_stk[ 0 ], &task_handler_tcb );

	HTTPThreadParam.Handle = &task_handler_tcb;
# else
    // Create thread
    result = dxTaskCreate(HttpThread_Loop,
                          HTTP_THREAD_NAME,
                          HTTP_THREAD_STACK_SIZE,
                          (void *)&HTTPThreadParam,
                          HTTP_THREAD_PRIORITY,
                          &HTTPThreadParam.Handle);

    if (result != DXOS_SUCCESS ||
        HTTPThreadParam.Handle == NULL)
    {
        return DXOS_ERROR;
    }
# endif

    GSysDebugger_RegisterForThreadDiagnostic(&HTTPThreadParam.Handle);

    dxWorkweTask_t *worker = dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(dxWorkweTask_t));

    if (HTTPThreadParam.Worker.RegisterFunc == NULL)
    {
        result = dxWorkerTaskCreate("HTTPMGR W",
                                    worker,
                                    DX_DEFAULT_WORKER_PRIORITY,
                                    HTTP_THREAD_WORKER_THREAD_STACK_SIZE,
                                    HTTP_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE);

        if (result != DXOS_SUCCESS)
        {
            dxTaskDelete(HTTPThreadParam.Handle);
            HTTPThreadParam.Handle = NULL;
            return result;
        }

        HTTPThreadParam.Worker.Handle = worker;

        GSysDebugger_RegisterForThreadDiagnosticGivenName(&HTTPThreadParam.Worker.Handle->WTask, "HTEvent");

        HTTPThreadParam.Worker.RegisterFunc = EventFunction;
    }

    // Initialize event queue
    HTTPThreadParam.EventQueue = dxQueueCreate(HTTP_THREAD_QUEUE_MAX_ALLOWED, sizeof(uint32_t));

    if (HTTPThreadParam.EventQueue == NULL)
    {
        dxWorkerTaskDelete(HTTPThreadParam.Worker.Handle);
        dxTaskDelete(HTTPThreadParam.Handle);
        HTTPThreadParam.Handle = NULL;
    }


    HTTPThreadParam.state = HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED;

    return DXOS_SUCCESS;
}

dxOS_RET_CODE HTTPManager_Receive_Stop(void)
{
    dxOS_RET_CODE result = DXOS_SUCCESS;
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;

    if (pThreadParam->EventQueue == NULL)
    {
        return DXOS_INVALID;
    }

    if (dxQueueIsFull(pThreadParam->EventQueue) == dxTRUE)
    {
        return DXOS_LIMIT_REACHED;
    }

    uint32_t event = HTTP_EVENT_STOP_RECEIVING;
    result = dxQueueSend(pThreadParam->EventQueue, &event, 1000);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Line %d, dxEventGroupSet(%d) = %d\n", __LINE__, HTTP_EVENT_STOP_RECEIVING, result);

    return result;
}

dxOS_RET_CODE HTTPManager_Receive_Asynch(uint32_t timeout)
{
    HTQCommMsg_t *msg = NULL;

    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    msg->Header.MessageType = HTTPMANAGER_COMMAND_RECEIVE;
    msg->Header.TCPWaitTimeInSec = timeout;
    msg->Header.DataLen = 0;

    return _HTMPushMessageToQueue(&msg);
}

dxOS_RET_CODE HTTPManager_Connect_Asynch(dxIP_Addr_t *ip, uint16_t port, uint32_t timeout)
{
    HTQCommMsg_t *msg = NULL;
    uint32_t i = 0;

    _HTMEmptyQueue();

    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    msg->Header.MessageType = HTTPMANAGER_COMMAND_CONNECT;
    msg->Header.DataLen = sizeof(dxIP_Addr_t) +
                          sizeof(port) +
                          sizeof(timeout);

    msg->pData = (uint8_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, msg->Header.DataLen);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen);

        dxMemFree(msg);

        return DXOS_NO_MEM;
    }

    /**
     *               +---------+
     * msg->pData => | ip      | sizeof(dxIP_Addr_t)
     *               +---------+
     *               | port    | uint16_t
     *               +---------+
     *               | timeout | uint32_t
     *               +---------+
     */
    memcpy(&msg->pData[i], ip, sizeof(dxIP_Addr_t));
    i += sizeof(dxIP_Addr_t);

    memcpy(&msg->pData[i], &port, sizeof(port));
    i += sizeof(port);

    memcpy(&msg->pData[i], &timeout, sizeof(timeout));
    i += sizeof(timeout);

    return _HTMPushMessageToQueue(&msg);
}

dxOS_RET_CODE HTTPManager_Send_Asynch(uint8_t *pData, uint32_t cbData, uint32_t timeout)
{
    HTQCommMsg_t *msg = NULL;

    if (pData == NULL ||
        cbData == 0)
    {
        return DXOS_INVALID;
    }

    // Header
    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    msg->Header.MessageType = HTTPMANAGER_COMMAND_SEND;
    msg->Header.TCPWaitTimeInSec = timeout;
    msg->Header.DataLen = cbData;

    // pData
    // Reserved a NULL character space to prevent output garbage information
    msg->pData = (uint8_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, cbData + 1);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         cbData + 1);

        dxMemFree(msg);

        return DXOS_NO_MEM;
    }

    memcpy(msg->pData, pData, msg->Header.DataLen);

    return _HTMPushMessageToQueue(&msg);
}

dxOS_RET_CODE HTTPManager_Disconnect_Asynch(void)
{
    HTQCommMsg_t *msg = NULL;

    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    memset(msg, 0x00, sizeof(HTQCommMsg_t));

    msg->Header.MessageType = HTTPMANAGER_COMMAND_DISCONNECT;
    msg->Header.DataLen = 0;

    return _HTMPushMessageToQueue(&msg);
}

dxOS_RET_CODE HTTPManager_Suspend_Asynch(void)
{
    HTQCommMsg_t *msg = NULL;
    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;

    if (pThreadParam->state == HTTPMANAGER_STATEMACHINE_STATE_RECEIVING)
    {
        HTTPManager_Receive_Stop();
    }

    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    msg->Header.MessageType = HTTPMANAGER_COMMAND_SUSPEND;
    msg->Header.DataLen = 0;

    return _HTMPushMessageToQueue(&msg);
}

dxOS_RET_CODE HTTPManager_Resume_Asynch(dxIP_Addr_t *ip, uint16_t port, uint32_t timeout)
{
    HTQCommMsg_t *msg = NULL;
    uint32_t i = 0;

    _HTMEmptyQueue();

    msg = (HTQCommMsg_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, sizeof(HTQCommMsg_t));

    if (msg == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(HTQCommMsg_t));

        return DXOS_NO_MEM;
    }

    msg->Header.MessageType = HTTPMANAGER_COMMAND_RESUME;
    msg->Header.DataLen = sizeof(dxIP_Addr_t) +
                          sizeof(port) +
                          sizeof(timeout);

    msg->pData = (uint8_t *)dxMemAlloc(HTTP_THREAD_NAME, 1, msg->Header.DataLen);

    if (msg->pData == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_HTTPMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         msg->Header.DataLen);

        dxMemFree(msg);

        return DXOS_NO_MEM;
    }

    /**
     *               +---------+
     * msg->pData => | ip      | sizeof(dxIP_Addr_t)
     *               +---------+
     *               | port    | uint16_t
     *               +---------+
     *               | timeout | uint32_t
     *               +---------+
     */
    memcpy(&msg->pData[i], ip, sizeof(dxIP_Addr_t));
    i += sizeof(dxIP_Addr_t);

    memcpy(&msg->pData[i], &port, sizeof(port));
    i += sizeof(port);

    memcpy(&msg->pData[i], &timeout, sizeof(timeout));
    i += sizeof(timeout);

    return _HTMPushMessageToQueue(&msg);
}

dxTime_t HTTPManager_GetLastAccessTime(void)
{
    dxTime_t ret = 0;

    HTTPThreadParam_t *pThreadParam = (HTTPThreadParam_t *)&HTTPThreadParam;

    if (pThreadParam == NULL ||
        pThreadParam->mutex == NULL)
    {
        return 0;
    }

    LOCK(&pThreadParam->mutex);

    ret = pThreadParam->LastAccessTick;

    UNLOCK(&pThreadParam->mutex);

    return ret;
}
