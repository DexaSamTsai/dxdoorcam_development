//============================================================================
// File: DKAPI_GetDataContainerByID.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//
//============================================================================
#ifndef _DKAPI_GETDATACONTAINERBYID_H
#define _DKAPI_GETDATACONTAINERBYID_H

#pragma once

#include "dxOS.h"
#include "ObjectDictionary.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/


/******************************************************
 *               Function Definitions
 ******************************************************/
int Build_GetDataContainerByID_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen);
int Parse_GetDataContainerByID_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen);

#ifdef __cplusplus
}
#endif

#endif // _DKAPI_GETDATACONTAINERBYID_H
