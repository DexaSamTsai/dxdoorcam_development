//============================================================================
// File: DKAPI_GetAdvJobByGateway.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.161
//     Date: 2015/06/10
//     1) Description: Modify GetAdvJobCondition API
//        Modified:
//            - Parse_GetAdvJobCondition_Response()
//
//     Version: 0.161.1
//     Date: 2015/06/25
//     1) Description: Modify GetAdvJobCondition API
//        Modified:
//            - Parse_GetAdvJobCondition_Response()
//
//     Version: 0.171.0
//     Date: 2015/07/21
//     1) Description: Support advanced job single gateway pack
//        Added:
//            - Added a new function _SendAdvJobToMain()
//        Modified:
//            - Changed filename to DKAPI_GetAdvJobByGateway.c
//            - Changed function name to Build_GetAdvJobByGateway_Data()
//            - Changed function name to Parse_GetAdvJobByGateway_Response()
//
//     Version: 0.171.2
//     Date: 2015/07/27
//     1) Description: Fix an issue with wrong "TotalAdvJobs" value
//        Modified:
//            - Parse_GetAdvJobByGateway_Response()
//
//     Version: 0.171.3
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.171.4
//     Date: 2016/03/01
//     1) Description: Added external function declaration
//
//     Version: 0.171.5
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - _SendAdvJobToMain()
//
//     Version: 0.171.6
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetAdvJobByGateway_Response()
//            - _SendAdvJobToMain()
//
//     Version: 0.171.7
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Build_GetAdvJobByGateway_Data()
//
//     Version: 0.171.8
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GetAdvJobByGateway_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetAdvJobByGateway.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETADVJOBBYGATEWAY_BODY      "{\"GatewayID\":%u,\"StartFrom\":%u,\"MaxNumOfAdvJobToRetrieve\":%u}"
#define DKMSG_TEMPLATE_GETADVJOBBYGATEWAY           "%s "                                               \
                                                    "%s/GetAdvJobByGateway.php HTTP/1.1\r\n"            \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GETADVJOBBYGATEWAY_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/
/******************************************************
 *               Function Implementation - Local
 ******************************************************/
int _SendAdvJobToMain(DKRESTCommMsg_t *pCommMsg, uint32_t advobjid, uint16_t index, int type, uint8_t *payload, int payloadlen)
{
    AdvJobInfo_t *pAdvJob = NULL;       // Advanced job information
    int joblen = 0;                     // length of pJobInfo

    if (advobjid == 0 ||
        payload == NULL ||
        payloadlen <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    joblen = sizeof(AdvJobInfo_t) + payloadlen;

    pAdvJob = (AdvJobInfo_t *)DKMSG_AllocMemory(joblen);
    if (pAdvJob == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    // Preparing JobCondition
    memcpy(pAdvJob->pPayload, payload, payloadlen);

    // Preparing nAdvJobID
    pAdvJob->nAdvJobID = advobjid;

    // Preparing nIndex
    pAdvJob->nIndex = index;

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
     G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                      "AdvJob, AdvJobID: %u, Payload len: %u\n",
                      (unsigned int)pAdvJob->nAdvJobID, (unsigned int)payloadlen);
#endif

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETADVJOBBYGATEWAY,
                                            type,
                                            HTTP_OK,
                                            DKSERVER_STATUS_SUCCESS,
                                            (void *)pAdvJob,
                                            joblen);

    DKMSG_FreeMemory((uint8_t *)pAdvJob);
    pAdvJob = NULL;

    return DKSERVER_STATUS_SUCCESS;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetAdvJobByGateway_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    uint16_t nStartIndex = 0;
    uint8_t nMaxRows = 0;
    int idx = 0;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (uint16_t) + sizeof (uint8_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid parameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    memcpy(&nStartIndex, &pCommMsg->pData[idx], sizeof(uint16_t));
    idx += sizeof(uint16_t);
    memcpy(&nMaxRows, &pCommMsg->pData[idx], sizeof(uint8_t));

    ObjectDictionary_Lock ();

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETADVJOBBYGATEWAY,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETADVJOBBYGATEWAY_BODY, (unsigned int) ObjectDictionary_GetGateway ()->ObjectID, (unsigned int)nStartIndex, (unsigned int)nMaxRows),
            ObjectDictionary_GetSessionToken (),
            (unsigned int) ObjectDictionary_GetGateway ()->ObjectID,
            (unsigned int) nStartIndex,
            (unsigned int) nMaxRows);

    ObjectDictionary_Unlock ();

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetAdvJobByGateway_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    AdvanceJobStatus_t* advanceStatus = NULL;
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t advobjid = 0;              // A variable to store the AdvObjID which convert from strtoul()
    uint32_t gatewayid = 0;             // A variable to store the GatewayID which convert from strtoul()
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;
    int j = 0;
    int type = 0;                       // value of enum DKJobType_t

    int totaladvjobs = 0;               // value of DKJSN_TOKEN_TOTALADVJOBS
    int resultnode = 0;                 // index of DKJSN_TOKEN_RESULTS
    int advjobnode = 0;                 // index of DKJSN_TOKEN_ADVJOBS
    int advjobcount;                    // total number of DKJSN_TOKEN_ADVJOBS
    int childnode = 0;                  // child of DKJSN_TOKEN_ADVJOBS
    int jobnode = 0;                    // index of DKJSN_TOKEN_CONDITIONS
    int condscount = 0;                 // total number of DKJSN_CONDITIONS
    int jobexecfound = 0;               // 1 if DKJSN_TOKEN_JOBEXECUTION found

    int nStartFrom = 0;                 // Start index of nTotalAdvJobs
    uint8_t  jobtype = 0;               // JobType. 2 for Advanced Job; 3 for Advanced Job Single Gateway Pack

    int findList1[] = {DKJSN_TOKEN_STATUS, DKJSN_TOKEN_UPDATETIME, 0};

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_TOTALADVJOBS, 0};
    int findList4[] = {DKJSN_TOKEN_ADVJOBS, 0};
    int findList5[] = {DKJSN_TOKEN_ADVOBJID, 0};
    int findList6[] = {DKJSN_TOKEN_GATEWAYID, 0};
    int findList7[] = {DKJSN_TOKEN_CONDITIONS, 0};
    int findList8[] = {DKJSN_TOKEN_JOBCONDITION, 0};
    int findList9[] = {DKJSN_TOKEN_EXECUTIONS, 0};
    int findList10[] = {DKJSN_TOKEN_JOBEXECUTION, 0};
    int findList11[] = {DKJSN_TOKEN_JOBTYPE, 0};

    advanceStatus = (AdvanceJobStatus_t*) dxMemAlloc ("advanceStatus", 1, sizeof (AdvanceJobStatus_t));
    if (advanceStatus == NULL) {
        ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
        goto alloc_fail;
    }

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (uint16_t) + sizeof (uint8_t) + sizeof (DKRESTCommMsg_t)) ||
        paramlen == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    memcpy(&nStartFrom, &pCommMsg->pData[0], sizeof(uint16_t));

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        ret = DKSERVER_SYSTEM_ERROR_JSON_INIT;
        goto fail;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Status updatetime of advanced jobs
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList1, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d:%d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList1[0],
                         findList1[1]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }
    advanceStatus->updatetime = strtoull(szBuffer, NULL, 10);

    // Get Results
    resultnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (resultnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get TotalAdvJobs
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList3[0]);
        DKJSN_Uninit(handle);
        goto fail;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "TotalAdvJobs: %s\n", szBuffer);
#endif

    advanceStatus->totalAdvances = strtoul (szBuffer, NULL, 10);
    if (advanceStatus->totalAdvances == 0)
    {
        DKJSN_Uninit(handle);
        ret = DKSERVER_STATUS_SUCCESS;
        goto fail;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get AdvJobs
    advjobnode = DKJSN_SelectObjectByList(handle, findList4, resultnode);
    if (advjobnode < 0)
    {
        // "AdvJobs" not found
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    advjobcount = DKJSN_GetObjectCount(handle, advjobnode);

    if (advanceStatus->totalAdvances != advjobcount)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, TotalAdvJobs(%d) not equal to AdvJobs(%d)\n",
                         __FUNCTION__,
                         __LINE__,
                         advanceStatus->totalAdvances,
                         advjobcount);
    }

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETADVJOBBYGATEWAY,
                                            DKJOBTYPE_STANDARD,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            advanceStatus,
                                            sizeof (AdvanceJobStatus_t));

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < advjobcount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, advjobnode, i);
        if (childnode < 0)
        {
            // Can't select the result node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get AdvObjID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList5[0]);
            DKJSN_Uninit(handle);
            goto fail;
        }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d AdvObjID: %s\n", i, szBuffer);
#endif
        advobjid = strtoul(szBuffer, NULL, 10);
        if (advobjid == 0)
        {
            continue;
        }

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "#%d AdvObjID: %u\n", i, (unsigned int)advobjid);
#endif

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get JobType

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList11, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList11[0]);
            DKJSN_Uninit(handle);
            goto fail;
        }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d AdvObjID: %s\n", i, szBuffer);
#endif
        jobtype = strtoul(szBuffer, NULL, 10);
        if (jobtype != 2 &&         // 2 => General advanced job
            jobtype != 3)           // 3 => Advanced job single gateway pack
        {
            continue;
        }

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "#%d JobType: %u\n", i, (unsigned int)jobtype);
#endif

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get Executions - Optional (Check Only)

        jobnode = DKJSN_SelectObjectByList(handle, findList9, childnode);

        jobexecfound = 0;

        if (jobtype == 3 && jobnode > 0)
        {
            condscount = DKJSN_GetObjectCount(handle, jobnode);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Advanced job Executions found: %d\n", condscount);

            // NOTE: condscount SHOULD be 1
            for (j = 0; j < condscount; j++)
            {
                jobnode = DKJSN_SelectObjectByIndex(handle, jobnode, j);
                if (jobnode < 0)
                {
                    break;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get GatewayID

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, jobnode, 0);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    break;
                }

                gatewayid = strtoul(szBuffer, NULL, 10);

                ObjectDictionary_Lock ();

                if (gatewayid == 0 ||
                    gatewayid != ObjectDictionary_GetGateway ()->ObjectID)
                {
                    ObjectDictionary_Unlock ();

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, Invalid GatewayID = %u\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     (unsigned int)gatewayid);

                    continue;
                }

                ObjectDictionary_Unlock ();

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get JobExecution

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList10, jobnode, 1);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    break;
                }

                jobexecfound = 1;
            }
        }

        if (jobtype == 3 &&
            jobexecfound == 0)
        {
            // JobCondition and JobExecution must exist at the same time when JobType value is equal to 3
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get Conditions - MUST

        jobnode = DKJSN_SelectObjectByList(handle, findList7, childnode);

        if (jobnode > 0)
        {
            condscount = DKJSN_GetObjectCount(handle, jobnode);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Advanced job Conditions found: %d\n", condscount);

            // NOTE: condscount SHOULD be 1
            for (j = 0; j < condscount; j++)
            {
                jobnode = DKJSN_SelectObjectByIndex(handle, jobnode, j);
                if (jobnode < 0)
                {
                    // Skip to next object in Conditions array
                    continue;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get GatewayID

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, jobnode, 0);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, JSON key %d not found\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     findList6[0]);
                    continue;
                }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "#%d GatewayID: %s\n", i, szBuffer);
#endif
                gatewayid = strtoul(szBuffer, NULL, 10);

                ObjectDictionary_Lock ();

                if (gatewayid == 0 ||
                    gatewayid != ObjectDictionary_GetGateway ()->ObjectID)
                {
                    ObjectDictionary_Unlock ();

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, Invalid GatewayID = %u\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     (unsigned int)gatewayid);

                    continue;
                }

                ObjectDictionary_Unlock ();

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "#%d GatewayID: %u\n", i, (unsigned int)gatewayid);
#endif

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get JobCondition

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList8, jobnode, 1);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    continue;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Preparing JobInformation

                if (jobtype == 3)
                {
                    type = DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY;
                }
                else
                {
                    type = DKJOBTYPE_ADVANCED_CONDITION;
                }

                ret = _SendAdvJobToMain(pCommMsg, advobjid, i + nStartFrom, type, (uint8_t *)szBuffer, len);

                if (ret == DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY)
                {
                    // Stop processing JSON only when memory allocation failed.
                    // In other case, just skip this.
                    break;
                }
            }
        }
        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get Executions - Optional

        jobnode = DKJSN_SelectObjectByList(handle, findList9, childnode);

        if (jobnode > 0)
        {
            condscount = DKJSN_GetObjectCount(handle, jobnode);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Advanced job Executions found: %d\n", condscount);

            // NOTE: condscount SHOULD be 1
            for (j = 0; j < condscount; j++)
            {
                jobnode = DKJSN_SelectObjectByIndex(handle, jobnode, j);
                if (jobnode < 0)
                {

                    // Skip to next object in Conditions array
                    continue;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get GatewayID

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, jobnode, 0);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, JSON key %d not found\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     findList6[0]);
                    // Skip to next object in Conditions array
                    continue;
                }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                 "#%d GatewayID: %s\n", i, szBuffer);
#endif
                gatewayid = strtoul(szBuffer, NULL, 10);

                ObjectDictionary_Lock ();

                if (gatewayid == 0 ||
                    gatewayid != ObjectDictionary_GetGateway ()->ObjectID)
                {
                    ObjectDictionary_Unlock ();

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "[%s] Line %d, Invalid GatewayID = %u\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     (unsigned int)gatewayid);

                    continue;
                }

                ObjectDictionary_Unlock ();

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "#%d GatewayID: %u\n", i, (unsigned int)gatewayid);
#endif


                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Get JobExecution

                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList10, jobnode, 1);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    continue;
                }

                //-----------------------------------------------------------------
                //-----------------------------------------------------------------

                // Preparing JobInformation

                if (jobtype == 3)
                {
                    type = DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY;
                }
                else
                {
                    type = DKJOBTYPE_ADVANCED_EXECUTION;
                }

                ret = _SendAdvJobToMain(pCommMsg, advobjid, i + nStartFrom, type, (uint8_t *)szBuffer, len);

                if (ret == DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY)
                {
                    // Stop processing JSON only when memory allocation failed.
                    // In other case, just skip this.
                    break;
                }
            }
        }
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    if (ret == DKSERVER_REPORT_STATUS_SUCCESS)
    {
        // Send JobInformation data to worker thread
        DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                pCommMsg->Header.TaskIdentificationNumber,
                                                DKMSG_GETADVJOBBYGATEWAY,
                                                DKJOBTYPE_NONE,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                NULL,
                                                0);
        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

fail :

    DKMSG_FreeMemory ((uint8_t*) advanceStatus);
    advanceStatus = NULL;

alloc_fail :

    return ret;

}
