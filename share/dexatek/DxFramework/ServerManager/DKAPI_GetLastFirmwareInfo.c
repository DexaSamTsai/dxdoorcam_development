//============================================================================
// File: DKAPI_GetLastFirmwareInfo.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Added external function declaration
//
//     2) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Parse_GetLastFirmareInfo_Response()
//
//     3) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetLastFirmareInfo_Response()
//            - Build_GetLastFirmwareInfo_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GetLastFirmwareInfo_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetLastFirmwareInfo.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETLASTFIRMWAREINFO          "%s "                                               \
                                                    "%s/GetLastFirmwareInfo.php HTTP/1.1\r\n"           \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetLastFirmwareInfo_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETLASTFIRMWAREINFO,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            0,                              // Content-Length
            ObjectDictionary_GetSessionToken ());

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetLastFirmareInfo_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int datacount = 0;                  // Total data length, not including TOTAL_LEN
    int i = 0;
    int idx = 0;

    int findList[][3] =
    {
        // NOTE: The order must the same as DKFirmare_Info_ID_t
        {DKJSN_TOKEN_GWWIFIFW,  DKJSN_TOKEN_FULLPATHFILENAME,   0},
        {DKJSN_TOKEN_GWWIFIFW,  DKJSN_TOKEN_VERSION,            0},
        {DKJSN_TOKEN_GWWIFIFW,  DKJSN_TOKEN_MD5,                0},
        {DKJSN_TOKEN_GWBLEFW,   DKJSN_TOKEN_FULLPATHFILENAME,   0},
        {DKJSN_TOKEN_GWBLEFW,   DKJSN_TOKEN_VERSION,            0},
        {DKJSN_TOKEN_GWBLEFW,   DKJSN_TOKEN_MD5,                0}
    };

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    /*
     * +-----------+
     * | TOTAL_LEN | ( 2 Byte, length from 1st field ID1 to last DATA(n) )
     * +-----------+
     * | ID1       | ( 1 Byte, reference DKFirmare_Info_ID_t )
     * +-----------+
     * | ITEM_LEN1 | ( 1 Byte, length of DATA )
     * +-----------+
     * | DATA1     | ( Variable length )
     * +-----------+
     * | ID2       | ( 1 Byte, reference DKFirmare_Info_ID_t )
     * +-----------+
     * | ITEM_LEN2 | ( 1 Byte, length of DATA )
     * +-----------+
     * | DATA2     | ( Variable length )
     * +-----------+
     * | ......    |
     * +-----------+
     */

    datacount = 0;

    // Calculating all the data length
    for (i = 0; i < sizeof(findList) / sizeof(findList[0]); i++)
    {
        // Get JSON data

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList[i], rootnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            break;
        }

        datacount += len + 3;       // 2 => ID + ITEM_LEN + 0x00 in DATA field
    }

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------
    uint8_t *pFirmwareData = DKMSG_AllocMemory((datacount + 3) * sizeof(uint8_t));       // 3 => two for TOTAL_LEN; one for 0x00
    if (pFirmwareData == NULL)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Total data length: %d\n", datacount + 1);

    idx = 2;    // Field length of TOTAL_LEN is 2

    for (i = 0; i < sizeof(findList) / sizeof(findList[0]); i++)
    {
        pFirmwareData[idx++] = (i + 1);

        // Get JSON data

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList[i], rootnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            break;
        }

        // Copy to pFirmwareData
        pFirmwareData[idx++] = len + 1;         // 1 => 0x00, append a null character for easy access

        if (len > 0)
        {
            memcpy(&pFirmwareData[idx], szBuffer, len);
        }

        idx += len + 1;                         // 1 => 0x00, append a null character for easy access
    }

    pFirmwareData[0] = ((idx - 2) >> 8) & 0x0FF;        // Field length of TOTAL_LEN is 2 and not included.
    pFirmwareData[1] = (idx - 2) & 0x0FF;               // Field length of TOTAL_LEN is 2 and not included.

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKMSG_FreeMemory(pFirmwareData);
        pFirmwareData = NULL;

        DKJSN_Uninit(handle);
        return ret;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETLASTFIRMWAREINFO,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)pFirmwareData, idx);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKMSG_FreeMemory(pFirmwareData);
    pFirmwareData = NULL;

    DKJSN_Uninit(handle);

    return DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
}
