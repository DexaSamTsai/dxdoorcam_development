//============================================================================
// File: DKAPI_GetPeripheralByGMACAddress.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     2) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//            - Build_GetPeripheralByGMAC_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GetPeripheralByGMAC_Data()
//
//     Version: 0.5
//     Date: 2016/09/06
//     1) Description: To support MQTT, report UserObjectID and GatewayID when server response success
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     Version: 0.6
//     Date: 2016/09/21
//     1) Description: Fixed a portential issue with NO SecurityKey as well as BLEPeripheralStatus
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     Version: 0.228
//     Date: 2016/10/03
//     1) Description: Support Container
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Revise dxContainerM_t structure
//        Modified:
//            - DKMSG_ParseDataContainer()
//
//     Version: 0.234.1
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     Version: 0.234.2
//     Date: 2017/06/14
//     1) Description: Convert Backslash-U form character string to UTF-8 form
//        Modified:
//            - Parse_GetPeripheralByGMAC_Response()
//
//     Version: 0.234.3
//     Date: 2017/06/29
//     1) Description: Added debug log information when fatal issue occurred
//        Modified:
//            - DKMSG_ParseDataContainer()
//            - Parse_GetPeripheralByGMAC_Response()
//============================================================================
#include <stdlib.h>
#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetPeripheralByGMACAddress.h"
#include "dxContainer.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETPERIPHERAL_BY_GMAC_BODY   "{\"Data\":[{\"GMACAddress\":\"%s\",\"StartFrom\":%d,\"MaxNumOfPeripheralToRetrieve\":%d}]}"
#define DKMSG_TEMPLATE_GETPERIPHERAL_BY_GMAC        "%s "                                               \
                                                    "%s/GetPeripheralByGMACAddress.php HTTP/1.1\r\n"    \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GETPERIPHERAL_BY_GMAC_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/
extern dxBOOL CHARC_EscapedFormtoUTF8String(uint8_t *pDst, uint32_t *pcbDstLen, uint8_t *pSrc, uint32_t cbSrcLen);

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/

int DKMSG_ParsePeripheral (dxPTR_t handle, int node, int peripheralIndex, uint32_t gatewayId, DKRESTHeader_t *pCommMsgHeader)
{
    PeripheralInfo_t* peripheralInfo = NULL;        // Peripheral information
    unsigned int objid = 0;             // An variable to store the value which convert from strtoul()
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int len;
    int j;
    int peristatnode = 0;               // index of BLEPeripheralStatus node
    int statuschildnode = 0;            // child of BLEPeripheralStatus
    int peripheralstatusCount = 0;      // Number of peripheralstatus in peripherals

    int findList7[] = {DKJSN_TOKEN_OBJECTID, 0};
    int findList8[] = {DKJSN_TOKEN_PMACADDRESS, 0};
    int findList10[] = {DKJSN_TOKEN_DEVICETYPE, 0};
    int findList11[] = {DKJSN_TOKEN_BLEPERIPHERALSTATUS, 0};
    int findList12[] = {DKJSN_TOKEN_BLEOBJECTID, 0};
    int findList15[] = {DKJSN_TOKEN_FIRMWAREVERSION, 0};
    int findList16[] = {DKJSN_TOKEN_SECURITYKEY, 0};
    int findList17[] = {DKJSN_TOKEN_DEVICENAME, 0};


    peripheralInfo = (PeripheralInfo_t*) dxMemAlloc("peripheralInfo", 1, sizeof (PeripheralInfo_t));
    if (peripheralInfo == NULL) {
        ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
        goto fail;
    }

    // Get ObjectID
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData (szBuffer, &len, handle, findList7, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        goto exit;
    }

    objid = strtoul (szBuffer, NULL, 10);
    if (objid == 0)
    {
        goto exit;
    }

    peripheralInfo->peripheral.ObjectID = objid;

    // Get PMACAddress
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList8, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        goto exit;
    }

    len = DKJSN_GetObjectStringLengthByList(handle, findList8, node);
    if (len == 0)
    {
        // PMACAddress not found
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto exit;
    }

    // Store PMACAddress
    DKMSG_CopyToMemory(szBuffer, STRING_BUFFER_SIZE, DKJSN_GetObjectStringByList(handle, findList8, node), len);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Peripheral #%d, PMACAddress: %s\n", peripheralIndex, szBuffer);

    len = _DecimalStrToMACAddr(peripheralInfo->peripheral.MACAddress, sizeof(peripheralInfo->peripheral.MACAddress), szBuffer);
    if (len == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, _DecimalStrToMACAddr(\"%s\") = 0\n",
                         __FUNCTION__,
                         __LINE__,
                         szBuffer);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_UNDEFINED;
        goto exit;
    }

    // Get GatewayID
    peripheralInfo->peripheral.GatewayID = gatewayId;

    // Get DeviceType
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList10, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS) {
        goto exit;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Peripheral #%d, DeviceType: %s\n", peripheralIndex, szBuffer);

    peripheralInfo->peripheral.DeviceType = atol (szBuffer);

    // Get FirmwareVersion
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList15, node, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "Peripheral #%d, FirmwareVersion: %s\n", peripheralIndex, szBuffer);

        memcpy(peripheralInfo->peripheral.FWVersion, szBuffer, strlen((char *)szBuffer));
    }

    // Get SecurityKey
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList16, node, 1);

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        if (len <= sizeof(peripheralInfo->peripheral.szSecurityKey))
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Peripheral #%d, SecurityKey found (%d)\n", peripheralIndex, len);

            peripheralInfo->peripheral.nSecurityKeyLen = len;
            memcpy(peripheralInfo->peripheral.szSecurityKey, szBuffer, len);
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Peripheral #%d, SecurityKey too long (%d)\n",
                             __FUNCTION__,
                             __LINE__,
                             peripheralIndex,
                             len);
        }
    }
    else
    {
        // NO SecurityKey
        ret = DKSERVER_STATUS_SUCCESS;
    }

    // Get DeviceName
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList17, node, 0);

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        // If there is no DeviceName, clear buffer to avoid adding wrong DeviceName when calling DKObj_AddGateway()
        memset (peripheralInfo->name, 0x00, 32);
    }
    else
    {
        // Convert Escape-Form UNICODE character to UTF8
        uint32_t nUTF8StrLen = STRING_BUFFER_SIZE + 1;
        dxBOOL r = CHARC_EscapedFormtoUTF8String(szBuffer2, &nUTF8StrLen, (uint8_t *)szBuffer, strlen(szBuffer));
        if (r == dxTRUE && nUTF8StrLen > 0)
        {
            memset (peripheralInfo->name, 0x00, 32);
            memcpy (peripheralInfo->name, szBuffer2, nUTF8StrLen > 31 ? 31 : nUTF8StrLen);
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Peripheral #%d, DeviceName: %s\n", peripheralIndex, ((uint8_t *)&peripheralInfo->name[0] != NULL) ? peripheralInfo->name : "");

    // Get BLEPeripheralStatus (Optional)
    peristatnode = DKJSN_SelectObjectByList(handle, findList11, node);
    if (peristatnode >= 0)
    {
        peripheralstatusCount = DKJSN_GetObjectCount(handle, peristatnode);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "BLEPeripheralStatus found: %d item(s)\n", peripheralstatusCount);

        // Why peripheralCount? Because each peripheral has only one BLEPeripheralStatus item.
        // If planning to use peripheralstatusCount, we must implement memory increase methodology.

        for (j = 0; j < peripheralstatusCount; j++)
        {
            statuschildnode = DKJSN_SelectObjectByIndex(handle, peristatnode, j);
            if (statuschildnode < 0)
            {
                // Can't select the peripheral node
                ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
                break;
            }

            // Get ObjectID of BLEPeripheralStatus

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList7, statuschildnode, 0);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                break;
            }

            objid = strtoul(szBuffer, NULL, 10);
            if (objid == 0)
            {
                continue;
            }

            peripheralInfo->peripheralStatus.ObjectID = objid;

            // Get BLEObjectID of BLEPeripheralStatus

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList12, statuschildnode, 0);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                break;
            }

            objid = strtoul(szBuffer, NULL, 10);
            if (objid == 0)
            {
                continue;
            }

            peripheralInfo->peripheralStatus.BLEObjectID = objid;
        }
    }
    else
    {
        // "Results":"Peripherals":"BLEPeripheralStatus" not found.
        // Continue to find "DataContainer"
    }

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsgHeader->SourceOfOrigin,
                                            pCommMsgHeader->TaskIdentificationNumber,
                                            pCommMsgHeader->MessageType,
                                            DKJOBTYPE_PERIPHERAL_INFO,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void*) peripheralInfo,
                                            sizeof (PeripheralInfo_t));

exit :

    DKMSG_FreeMemory ((uint8_t*) peripheralInfo);
    peripheralInfo = NULL;

fail :

    return ret;
}


int DKMSG_ParseDataContainer (dxPTR_t handle, int node, DKRESTHeader_t *pCommMsgHeader)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail

    int contnode = 0;                   // index of DataContainer node
    int contmnode = 0;                  // index of DataContainer's child node
    int contdetalsnode = 0;             // index of ContDetails node
    int contdnode = 0;                  // index of ContDetails' child node

    int containerCount = 0;             // Number of DataContainer
    int contdetailsCount = 0;           // Number of DataContainer

    int len = 0;
    int i = 0;
    int j = 0;

    int findList1[] = {DKJSN_TOKEN_DATACONTAINER, 0};
    int findList2[] = {DKJSN_TOKEN_CONTMID, 0};
    int findList3[] = {DKJSN_TOKEN_CONTMNAME, 0};
    int findList4[] = {DKJSN_TOKEN_CONTMTYPE, 0};
    int findList5[] = {DKJSN_TOKEN_CONTDETAILS, 0};
    int findList6[] = {DKJSN_TOKEN_CONTDID, 0};
    int findList7[] = {DKJSN_TOKEN_CONTDNAME, 0};
    int findList8[] = {DKJSN_TOKEN_CONTDTYPE, 0};
    int findList9[] = {DKJSN_TOKEN_DATETIME, 0};
    int findList10[] = {DKJSN_TOKEN_SVALUE, 0};
    int findList11[] = {DKJSN_TOKEN_LVALUE, 0};
    int findList12[] = {DKJSN_TOKEN_CONTLVCRC, 0};
    int findList13[] = {DKJSN_TOKEN_BLEOBJECTID, 0};


    if (handle == 0 ||
        pCommMsgHeader == NULL)
    {
        return DKSERVER_STATUS_SUCCESS;
    }

    // Get DataContainer
    contnode = DKJSN_SelectObjectByList(handle, findList1, node);
    if (contnode < 0)
    {
        // "DataContainer" not found
        return DKSERVER_STATUS_SUCCESS;
    }

    containerCount = DKJSN_GetObjectCount(handle, contnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "\"DataContainer\" found: %d\n", containerCount);

    dxContainer_t *pCont = NULL;

    for (i = 0; i < containerCount; i++)
    {
        contmnode = DKJSN_SelectObjectByIndex(handle, contnode, i);
        if (contmnode < 0)
        {
            // Can't select the "DataContainer" node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        uint64_t ContMID = 0;
        uint32_t BLEObjectID = 0;
        uint8_t *ContMName = NULL;
        uint32_t ContMType = 0;

        // Get ContMID
        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, contmnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            // Skip this node
            continue;
        }

        ContMID = strtoull(szBuffer, NULL, 10);

        // Get BLEObjectID (Optional)
        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList13, contmnode, 0);
        if (ret == DKSERVER_STATUS_SUCCESS)
        {
            BLEObjectID = strtoul(szBuffer, NULL, 10);
        }

        // Get ContMName (Optional)
        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, contmnode, 0);
        if (ret == DKSERVER_STATUS_SUCCESS)
        {
            ContMName = dxContainer_CString_Init((uint8_t *)szBuffer, len);
        }

        // Get ContMType (Optional)
        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, contmnode, 0);
        if (ret == DKSERVER_STATUS_SUCCESS)
        {
            ContMType = strtoul(szBuffer, NULL, 10);
        }

        // Create dxContainerM. DON'T forget to free VarLenType_t variable if create fail
        if (pCont == NULL)
        {
            pCont = dxContainer_Create();
        }

        if (pCont == NULL)
        {
            dxContainer_CString_Free(ContMName);

            ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
            break;
        }

        dxContainerM_t *pContM = dxContainerM_Create(ContMID,
                                                     BLEObjectID,
                                                     ContMType,
                                                     ContMName);

        if (pContM == NULL)
        {
            dxContainer_CString_Free(ContMName);

            dxContainer_Free(pCont);

            ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
            break;
        }

        dxContainer_Append(pCont, pContM);

        // We will make a copy of these values before sending notification. So, they need to be free
        dxContainer_CString_Free(ContMName);

        // Get ContDetails
        contdetalsnode = DKJSN_SelectObjectByList(handle, findList5, contmnode);
        if (contdetalsnode >= 0)
        {
            contdetailsCount = DKJSN_GetObjectCount(handle, contdetalsnode);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "\"ContDetails\" found: %d\n", contdetailsCount);

            for (j = 0; j < contdetailsCount; j++)
            {
                contdnode = DKJSN_SelectObjectByIndex(handle, contdetalsnode, j);
                if (contdnode < 0)
                {
                    // Can't select the "ContDetails" node
                    ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
                    break;
                }

                uint64_t ContDID = 0;
                uint8_t *ContDName = NULL;
                uint32_t ContDType = 0;
                uint32_t DateTime = 0;
                VarLenType_t *SValue = NULL;
                VarLenType_t *LValue = NULL;
                uint16_t ContLVCRC = 0;

                // Get ContDID
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, contdnode, 0);
                if (ret != DKSERVER_STATUS_SUCCESS)
                {
                    // Skip this node
                    continue;
                }

                ContDID = strtoull(szBuffer, NULL, 10);

                // Get ContDName (Optional)
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList7, contdnode, 0);
                if (ret == DKSERVER_STATUS_SUCCESS)
                {
                    ContDName = dxContainer_CString_Init((uint8_t *)szBuffer, len);
                }

                // Get ContDType (Optional)
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList8, contdnode, 0);
                if (ret == DKSERVER_STATUS_SUCCESS)
                {
                    ContDType = strtoul(szBuffer, NULL, 10);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                     "ContDType: %s\n", szBuffer);
                }

                // Get DateTime (Optional)
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList9, contdnode, 0);
                if (ret == DKSERVER_STATUS_SUCCESS)
                {
                    DateTime = strtoul(szBuffer, NULL, 10);
                }

                // Get SValue (Optional)
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList10, contdnode, 0);
                if (ret == DKSERVER_STATUS_SUCCESS)
                {
                    SValue = dxContainer_VarLenType_Init((uint8_t *)szBuffer, len);
                }

                // Get LValue (Optional)
                // NOTE: The length of LValue may be very large, so we can't use static szBuffer (256 bytes)
                //       Allocate a dynamic buffer to retrieve the value
                len = DKJSN_GetObjectStringLengthByList(handle, findList11, contdnode);
                if (len > 0)
                {
                    memset (szBuffer2, 0, STRING_BUFFER_SIZE + 1);
                    ret = DKMSG_GetJSONData((char *)szBuffer2, &len, handle, findList11, contdnode, 1);
                    if (ret == DKSERVER_STATUS_SUCCESS)
                    {
                        LValue = dxContainer_VarLenType_Init(szBuffer2, len);
                    }
                }

                // Get ContLVCRC (Optional)
                len = STRING_BUFFER_SIZE;
                ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList12, contdnode, 0);
                if (ret == DKSERVER_STATUS_SUCCESS)
                {
                    ContLVCRC = (uint16_t)strtoul(szBuffer, NULL, 10);
                }

                // Create dxContainerD. DON'T forget to free VarLenType_t variable if create fail
                dxContainerD_t *pContD = dxContainerD_Create(ContDID,
                                                             ContMID,
                                                             ContDName,
                                                             ContDType,
                                                             DateTime,
                                                             SValue,
                                                             LValue,
                                                             ContLVCRC);

                if (pContD == NULL)
                {
                    dxContainer_VarLenType_Free(LValue);
                    dxContainer_VarLenType_Free(SValue);
                    dxContainer_CString_Free(ContDName);

                     // Skip this node
                    continue;
                }

                dxContainerM_Append(pContM, pContD);

                // We will make a copy of these values before sending notification. So, they need to be free
                dxContainer_VarLenType_Free(LValue);
                dxContainer_VarLenType_Free(SValue);
                dxContainer_CString_Free(ContDName);
            }
        }
        else
        {
            // "ContDetails" not found
            ret = DKSERVER_STATUS_SUCCESS;
        }
    }

    if (pCont)
    {
        // Send JobInformation data to worker thread
        DKServerManager_SendEventToWorkerThread(pCommMsgHeader->SourceOfOrigin,
                                                pCommMsgHeader->TaskIdentificationNumber,
                                                pCommMsgHeader->MessageType,
                                                DKJOBTYPE_CONTAINER_INFO,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                (void *)&pCont,
                                                sizeof(pCont));

        ret = DKSERVER_STATUS_SUCCESS;
    }

    return ret;
}


int Build_GetPeripheralByGMAC_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t* pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int nStartFrom = 0;
    int nMaxNumToRetrieve = 0;
    int idx = 0;
    char szData[21] = {0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (int) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, ScheduleJob information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    ObjectDictionary_Lock ();

    int maclen = _MACAddrToDecimalStr (szData, sizeof(szData), (uint8_t*) ObjectDictionary_GetMacAddress (), 8);
    if (maclen <= 0)
    {
        ObjectDictionary_Unlock ();
        DKHex_HexToASCII (ObjectDictionary_GetMacAddress (), 8, &szData[0], sizeof(szData));
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, MAC address %s convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__,
                         szData);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Unlock ();

    memcpy(&nStartFrom, &pCommMsg->pData[idx], sizeof (int));
    idx += sizeof(int);
    memcpy(&nMaxNumToRetrieve, &pCommMsg->pData[idx], sizeof (int));

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETPERIPHERAL_BY_GMAC,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETPERIPHERAL_BY_GMAC_BODY, szData, nStartFrom, nMaxNumToRetrieve),
            ObjectDictionary_GetSessionToken (),
            szData,
            nStartFrom,
            nMaxNumToRetrieve);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
    }

    return ret;
}

int Parse_GetPeripheralByGMAC_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    GatewayInfo_t* gatewayInfo = NULL;
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    unsigned int objid = 0;             // An variable to store the value which convert from strtoul()
    int rootnode = 0;                   // rootnode, must be 0

    int resultsnode = 0;                // index of results node
    int gatewaynode = 0;                // child of results node
    int perinode = 0;                   // index of Peripherals node
    int childnode = 0;                  // child of Peripherals node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;

    int gatewayCount = 0;               // Number of gateways

    int peripheralCount = 0;            // Number of peripherals
    int i = 0;
    int j = 0;

    int findList1[] = {DKJSN_TOKEN_STATUS, DKJSN_TOKEN_UPDATETIME, 0};

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_OBJECTID, 0};
    int findList4[] = {DKJSN_TOKEN_GMACADDRESS, 0};
    int findList5[] = {DKJSN_TOKEN_TOTALPERIPHERALS, 0};

    int findList6[] = {DKJSN_TOKEN_PERIPHERALS, 0};

    int findList13[] = {DKJSN_TOKEN_GATEWAYFIRMWARE, DKJSN_TOKEN_WIFIVERSION, 0};
    int findList14[] = {DKJSN_TOKEN_GATEWAYFIRMWARE, DKJSN_TOKEN_BLEVERSION, 0};
    int findList17[] = {DKJSN_TOKEN_DEVICENAME, 0};


    gatewayInfo = (GatewayInfo_t*) dxMemAlloc ("gatewayInfo", 1, sizeof (GatewayInfo_t));
    if (gatewayInfo == NULL) {
        ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
        goto alloc_fail;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0 ||
        pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (int) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        ret = DKSERVER_SYSTEM_ERROR_JSON_INIT;
        goto fail;
    }

    // Get updatetime
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData (szBuffer, &len, handle, findList1, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, JSON key %d:%d not found\n",
                          __FUNCTION__,
                          __LINE__,
                          findList1[0],
                          findList1[1]);
        DKJSN_Uninit (handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    gatewayInfo->updatetime = strtoull (szBuffer, NULL, 10);

    // Get results
    resultsnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (resultsnode < 0)
    {
        // "Results" not found
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList2[0]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    gatewayCount = DKJSN_GetObjectCount(handle, resultsnode);
    if (gatewayCount <= 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Value of JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList2[0]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    // Always get the 1st Gateway information
    gatewaynode = DKJSN_SelectObjectByIndex(handle, resultsnode, 0);
    if (gatewaynode < 0)
    {
        // "Results" not found
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList2[0]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    // Get ObjectID of the Gateway
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, gatewaynode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        goto fail;
    }

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    gatewayInfo->objectId = objid;

    // Get MACAddress of the gateway
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, gatewaynode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        goto fail;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "GMACAddress: %s\n", szBuffer);

    len = _DecimalStrToMACAddr(gatewayInfo->macAddress, sizeof (gatewayInfo->macAddress), szBuffer);
    if (len == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, _DecimalStrToMACAddr(\"%s\") = 0\n",
                         __FUNCTION__,
                         __LINE__,
                         szBuffer);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_UNDEFINED;
        goto fail;
    }

    // Get TotalPeripherals of the gateway
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, gatewaynode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        goto fail;
    }
    gatewayInfo->totalPeripherals = strtoul (szBuffer, NULL, 10);

    // Get WiFiVersion
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList13, gatewaynode, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        memcpy (gatewayInfo->wifiVersion, szBuffer, strlen (szBuffer));
    }

    // Get BLEVersion
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList14, gatewaynode, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        memcpy(gatewayInfo->bleVersion, szBuffer, strlen (szBuffer));
    }

    // Get DeviceName

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList17, gatewaynode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        memset (gatewayInfo->name, 0, 32);
    }
    else
    {
        uint32_t nUTF8StrLen = STRING_BUFFER_SIZE + 1;

        // Convert Escape-Form UNICODE character to UTF8
        memset (szBuffer2, 0, STRING_BUFFER_SIZE + 1);
        dxBOOL r = CHARC_EscapedFormtoUTF8String(szBuffer2, &nUTF8StrLen, (uint8_t *)szBuffer, strlen(szBuffer));
        if (r == dxTRUE && nUTF8StrLen > 0)
        {
            memset (gatewayInfo->name, 0, 32);
            memcpy (gatewayInfo->name, szBuffer2, nUTF8StrLen > 31 ? 31 : nUTF8StrLen);
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "DeviceName: %s\n", ((uint8_t *)&gatewayInfo->name[0] != NULL) ? gatewayInfo->name : "");

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread (pCommMsg->Header.SourceOfOrigin,
                                             pCommMsg->Header.TaskIdentificationNumber,
                                             DKMSG_GETPERIPHERALBYGMACADDRESS,
                                             DKJOBTYPE_GATEWAY_INFO,
                                             HTTP_OK,
                                             DKSERVER_REPORT_STATUS_SUCCESS,
                                             (void*) gatewayInfo,
                                             sizeof (GatewayInfo_t));

    // Get Peripherals
    perinode = DKJSN_SelectObjectByList(handle, findList6, gatewaynode);
    if (perinode < 0)
    {
        // "Results":"Peripherals" not found
        DKJSN_Uninit(handle);

        uint32_t ObjectID[2] = {0};
        ObjectID[0] = ObjectDictionary_GetUser ()->ObjectID;
        ObjectID[1] = gatewayInfo->objectId;

        // Send JobInformation data to worker thread
        DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                pCommMsg->Header.TaskIdentificationNumber,
                                                DKMSG_GETPERIPHERALBYGMACADDRESS,
                                                DKJOBTYPE_NONE,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                (void *)&ObjectID[0],
                                                sizeof(ObjectID));

        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
        goto fail;
    }

    peripheralCount = DKJSN_GetObjectCount(handle, perinode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Peripheral found: %d device(s)\n", peripheralCount);

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < peripheralCount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, perinode, i);
        if (childnode < 0)
        {
            // Can't select the peripheral node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        DKMSG_ParsePeripheral (
            handle,
            childnode,
            i,
            gatewayInfo->objectId,
            &pCommMsg->Header
        );

        // GetDataContainer (Optional)
        DKMSG_ParseDataContainer (handle,
                                 childnode,
                                 &pCommMsg->Header);
    }

    DKJSN_Uninit(handle);

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        uint32_t ObjectID[2] = {0};
        ObjectID[0] = ObjectDictionary_GetUser ()->ObjectID;
        ObjectID[1] = gatewayInfo->objectId;

        // Send JobInformation data to worker thread
        DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                pCommMsg->Header.TaskIdentificationNumber,
                                                DKMSG_GETPERIPHERALBYGMACADDRESS,
                                                DKJOBTYPE_NONE,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                (void *)&ObjectID[0],
                                                sizeof(ObjectID));

        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

fail :

    DKMSG_FreeMemory ((uint8_t*) gatewayInfo);
    gatewayInfo = NULL;

alloc_fail :

    return ret;
}
