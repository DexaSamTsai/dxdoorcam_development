//============================================================================
// File: DKAPI_CreateContainerMData.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Revise dxContainerM_t structure
//        Modified:
//            - Build_CreateContainerMData_Data()
//            - Parse_CreateContainerMData_Response()
//
//     Version: 0.234.1
//     Date: 2017/03/27
//     1) Description: Revise Error Code
//        Modified:
//            - Build_CreateContainerMData_Data()
//============================================================================
#include <stdlib.h>
#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_CreateContainerMData.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_CREATECONTAINERMDATA_API                                                         \
                                                    "%s "                                               \
                                                    "%s/CreateContainerMData.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern int DKMSG_ParseDataContainer(dxPTR_t handle, int node, DKRESTHeader_t *pCommMsgHeader);

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_CreateContainerMData_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;

    /*
     *  pCommMsg->Header.DataLen  = 4 Bytes
     *  pCommMsg->pData           = +-----------------+
     *                              | dxContainer_t * |
     *                              +-----------------+
     */

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (dxContainer_t *)) ||
        paramlen == 0 ||
        paramlen > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    sprintf((char *)pBuffer, DKMSG_TEMPLATE_CREATECONTAINERMDATA_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    dxContainer_t *pCont = NULL;

    // NOTE: "&pCont" (We need to read back the address of dxContainer object)
    memcpy(&pCont, &pCommMsg->pData[i], sizeof(pCont));

    if (pCont == NULL || pCont->pContMList == NULL)
    {
        // No data
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    else
    {
        dxContainerM_t *p = pCont->pContMList;
        dxContainerM_t *pLastContM = dxContainerM_GetLast(p);

        // Start creating JSON
        strcat((char *)pBuffer, "{");

        // Append "BLEObjectID" to JSON
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"BLEObjectID\":%u", (unsigned int) p->BLEObjectID);
        i += sizeof (uint32_t);

        strcat((char *)pBuffer, ",\"DataContainer\":[");

        while (p)
        {
            // Append "ContMName", "ContMType" to JSON
            if (p->ContMName &&
                p->ContMName->pData &&
                p->ContMName->Length > 0 &&
                p->ContMType > 0)
            {
                strcat((char *)pBuffer, "{");

                n = strlen((char *)pBuffer);
                sprintf((char *)&pBuffer[n], "\"ContMName\":\"%s\"", p->ContMName->pData);

                n = strlen((char *)pBuffer);
                sprintf((char *)&pBuffer[n], ",\"ContMType\":%u", p->ContMType);

                dxContainerD_t *q = p->pContDList;
                dxContainerD_t *pLastContD = dxContainerD_GetLast(q);

                if (q)
                {
                    strcat((char *)pBuffer, ",\"ContDetails\":[");

                    while (q)
                    {
                        if (q->ContDName &&
                            q->ContDName->pData &&
                            q->ContDName->Length > 0 &&
                            q->ContDType > 0)
                        {
                            strcat((char *)pBuffer, "{");

                            n = strlen((char *)pBuffer);
                            sprintf((char *)&pBuffer[n], "\"ContDName\":\"%s\"", q->ContDName->pData);

                            n = strlen((char *)pBuffer);
                            sprintf((char *)&pBuffer[n], ",\"ContDType\":%u", q->ContDType);

                            // SValue (Optional)
                            if (q->SValue &&
                                q->SValue->pData &&
                                q->SValue->Length > 0)
                            {
                                n = strlen((char *)pBuffer);
                                sprintf((char *)&pBuffer[n], ",\"SValue\":\"%s\"", q->SValue->pData);
                            }

                            // LValue and ContLVCRC (Optional)
                            if (q->LValue &&
                                q->LValue->pData &&
                                q->LValue->Length > 0)
                            {
                                strcat((char *)pBuffer, ",\"LValue\":\"");

                                n = strlen((char *)pBuffer);
                                n = b64_ntop(q->LValue->pData, q->LValue->Length, (char *)&pBuffer[n], cbSize - n);

                                uint16_t crc16 = Crc16(q->LValue->pData, q->LValue->Length, 0);
                                n = strlen((char *)pBuffer);
                                sprintf((char *)&pBuffer[n], "\",\"ContLVCRC\":%u", crc16);
                            }
                            strcat((char *)pBuffer, "}");

                            if (q != pLastContD)
                            {
                                strcat((char *)pBuffer, ",");
                            }
                        }

                        q = q->pNextContD;
                    }

                    strcat((char *)pBuffer, "]");
                }

                strcat((char *)pBuffer, "}");

                if (p != pLastContM)
                {
                    strcat((char *)pBuffer, ",");
                }
            }

            p = p->pNextContM;
        }

        strcat((char *)pBuffer, "]");
        strcat((char *)pBuffer, "}");
    }

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
    }

    return ret;
}

int Parse_CreateContainerMData_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0

    int resultsnode = 0;                // index of results node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    //int len = 0;

    int findList1[] = {DKJSN_TOKEN_RESULTS, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (dxContainer_t *)) ||
        paramlen == 0 ||
        paramlen > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get "results"
    resultsnode = DKJSN_SelectObjectByList(handle, findList1, rootnode);
    if (resultsnode < 0)
    {
        // "results" not found
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList1[0]);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    ret = DKMSG_ParseDataContainer(handle,
                                   resultsnode,
                                   &pCommMsg->Header);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
