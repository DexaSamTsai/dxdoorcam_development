//============================================================================
// File: DKAPI_UpdatePeripheralDataPayload.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.171.0
//     Date: 2015/07/21
//     1) Description: Support advanced job single gateway pack
//        Modified:
//            - Build_UpdatePeripheralDataPayload_Data()
//
//     Version: 0.171.3
//     Date: 2015/08/12
//     1) Description: Fix an issue with wrong JSON data format when AdvObjID > 0
//        Modified:
//            - Build_UpdatePeripheralDataPayload_Data()
//
//     Version: 0.171.4
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.171.5
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_UpdatePeripheralDataPayload_Response()
//            - Build_UpdatePeripheralDataPayload_Data()
//
//     Version: 0.171.6
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_UpdatePeripheralDataPayload_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_UpdatePeripheralDataPayload.h"
//Build_UpdatePeripheralDataPayload_Data

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_UPDATEPERIPHERALDATAPAYLOAD_API                                                  \
                                                    "%s "                                               \
                                                    "%s/UpdatePeripheralDataPayload.php HTTP/1.1\r\n"


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/
int _CheckUpdateStatusCommandLength(uint32_t objectid, uint16_t len)
{
    int overheadlen = 46;                   // 34 => {"ObjectID":,"UpdateTo":,"BLEDataPayload":""},
    overheadlen += ((len + 2) / 3 * 4);     // Base64 encode overhead
    overheadlen += snprintf(NULL, 0, "%u", (unsigned int)objectid);

    return overheadlen;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_UpdatePeripheralDataPayload_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    uint8_t nNumOfPayloadAppend = 0;        // Number of {"ObjectID":<ObjectID>,"UpdateTo":<UpdateTo>,"BLEDataPayload":<BLEDataPayload>} appended to buffer

    UpdatePeripheralStatusData_t *pPeripheralStatus = NULL;

    if (pParameters == NULL ||
        paramlen < sizeof (DKRESTCommMsg_t))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Total data length: %d\n", paramlen);
#endif
    sprintf((char *)pBuffer, DKMSG_TEMPLATE_UPDATEPERIPHERALDATAPAYLOAD_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    strcat((char *)pBuffer, "{\"Data\":[");

    while (i < paramlen && ret == DKSERVER_STATUS_SUCCESS)
    {
        i += sizeof(DKRESTCommMsg_t);

#if defined(ENABLE_COMPRESS_UPDATE_DATAPAYLOAD)
        pPeripheralStatus = (UpdatePeripheralStatusData_t *)&pParameters[i];
#else
        pPeripheralStatus = (UpdatePeripheralStatusData_t *)((DKRESTCommMsg_t *)pParameters)->pData;
#endif

        if (pPeripheralStatus == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid pParameters\n",
                             __FUNCTION__,
                             __LINE__);
            ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
            continue;
        }

        if (pPeripheralStatus->ObjectID == 0)
        {
            // Invalid object ID. Will skip it.
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                             __FUNCTION__,
                             __LINE__,
                             pPeripheralStatus->ObjectID);

            // Move to next data which providing by DKServerManager_UpdatePeripheralDataPayload_Asynch()
            i += sizeof(UpdatePeripheralStatusData_t) + pPeripheralStatus->PayloadLen;

            continue;
        }

        // Check remaining buffer
        n = _CheckUpdateStatusCommandLength(pPeripheralStatus->ObjectID, pPeripheralStatus->PayloadLen); // Current PeripheralStatus JSON format data length
        n += 2;                                                                          // Additional "]}" characters
        n += strlen((char *)pBuffer);                                                    // JSON data which already in buffer
        if (n >= cbSize)
        {
            // pBuffer is not enough, so give up the rest data
            break;
        }

        if (nNumOfPayloadAppend > 0)
        {
            strcat((char *)pBuffer, ",");
        }

        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "{\"ObjectID\":%u,\"UpdateTo\":%u,\"BLEDataPayload\":\"", (unsigned int)pPeripheralStatus->ObjectID, (unsigned int)pPeripheralStatus->UpdateTo);
        n = strlen((char *)pBuffer);

        uint8_t *p = (uint8_t *)pPeripheralStatus;
        n = b64_ntop(&p[offsetof(UpdatePeripheralStatusData_t, pPayload)], pPeripheralStatus->PayloadLen, (char *)&pBuffer[n], cbSize - n);

        strcat((char *)pBuffer, "\"");

        if (pPeripheralStatus->AdvObjID > 0)        // Skip this data when the value is 0
        {
            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n], ",\"AdvObjID\":%u", (unsigned int)pPeripheralStatus->AdvObjID);
        }

        strcat((char *)pBuffer, "}");

        i += sizeof(UpdatePeripheralStatusData_t) + pPeripheralStatus->PayloadLen;

        nNumOfPayloadAppend++;
    }

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    if (nNumOfPayloadAppend == 0)
    {
        // No data, return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    strcat((char *)pBuffer, "]}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_UpdatePeripheralDataPayload_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t objid = 0;                 // An variable to store the ObjectID which convert from strtoul()
    uint32_t bleid = 0;                 // An variable to store the BLEObjectID which convert from strtoul()
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;
    int Idx = 0;                        // Pointer to pParameters buffer

    int periphpayloadnode = 0;          // index of peripheral data payload result
    int periphpayloadcount;
    int childnode = 0;                  // child of peripheralnode

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList4[] = {DKJSN_TOKEN_BLEOBJECTID, 0};

    if (pParameters == NULL ||
        paramlen < sizeof (DKRESTCommMsg_t))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    periphpayloadnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (periphpayloadnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    periphpayloadcount = DKJSN_GetObjectCount(handle, periphpayloadnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of results found: %d\n", periphpayloadcount);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < periphpayloadcount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, periphpayloadnode, i);
        if (childnode < 0)
        {
            // Can't select the job node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get BLEObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList4[0]);
            DKJSN_Uninit(handle);
            return ret;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d BLEObjectID: %s\n", i, szBuffer);
#endif
        bleid = strtoul(szBuffer, NULL, 10);
        if (bleid == 0)
        {
            continue;
        }

        // According to specification v0.99, "ObjectID" was removed from DKServer response JSON data.
        // We still need a UNIQUE PeripheralStatus's ObjectID to fit our data structure, DK_BLEPeripheralStatus_t
        // Before we make any changing to DK_BLEPeripheralStatus_t, we will use BLEObjectID as PeripheralStatus's ObjectID
        objid = bleid;

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "#%d ObjectID: %u BLEObjectID: %u\n", i, (unsigned int)objid, (unsigned int)bleid);
#endif

        ObjectDictionary_Lock ();
        ObjectDictionary_AddPeripheralStatus (objid, bleid);
        ObjectDictionary_Unlock ();

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        if (Idx < paramlen)
        {
            // NOTE:
            //
            //     DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)&pParameters[Idx];
            //     DKServerManager_SendEventIDToWorkerThread(pCommMsg->Header.SourceOfOrigin,
            //                                               pCommMsg->Header.TaskIdentificationNumber,
            //                                               pCommMsg->Header.MessageType,
            //                                               ret);
            //     Idx += (sizeof(DKRESTCommMsg_t) + pCommMsg->Header.DataLen);
            //
            // After running the code above over three times, system will crash.
            //

            DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)&pParameters[Idx];

            DKServerManager_SendEventIDToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                      pCommMsg->Header.TaskIdentificationNumber,
                                                      pCommMsg->Header.MessageType,
                                                      HTTP_OK,
                                                      ret);

            Idx += ((int)sizeof(DKRESTCommMsg_t) + pCommMsg->Header.DataLen);
        }
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

    return ret;
}
