//============================================================================
// File: dxContainer.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/10/03
//     1) Description: Initial
//
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Support copy whole container data or just those data with DirtyFlag = DXCONT_COPY_OPTION_DIRTYFLAG
//        Modified:
//            - DKServerManager_GetDataContainerByID_Asynch()
//            - DKServerManager_CreateContainerMData_Asynch()
//            - DKServerManager_UpdateContainerMData_Asynch()
//            - DKServerManager_InsertContainerDData_Asynch()
//            - DKServerManager_UpdateContainerDData_Asynch()
//            - DKServerManager_DeleteContainerDData_Asynch()
//            - DKServerManager_DeleteContainerMData_Asynch()
//     2) Description: Revise dxContainerM_t structure
//        Modified:
//            - dxContainerM_Create()
//            - dxContainer_Duplicate()
//============================================================================
#include "dxContainer.h"
#include "dxOS.h"
#include "dxSysDebugger.h"

//============================================================================
// Global defines
//============================================================================
#define DXCONT_NAME                                 "dxCONT"

extern int _UINT64ToDecimalStr(char *buffer, int cbSize, uint64_t nU64Value);

//============================================================================
// Convert uint64_t value to a string for printing
//============================================================================
uint8_t *dxContainer_UINT64_ToStr(uint64_t v)
{
    // Max value of uint64_t is 18446744073709551615 which is a 20 characters long value
    int len = 21;

    uint8_t *p = (uint8_t *)dxMemAlloc(DXCONT_NAME, 1, len);

    if (p == NULL || _UINT64ToDecimalStr((char *)p, len, v) == 0)
    {
        return NULL;
    }

    return p;
}

//============================================================================
// dxContainer Memory Allocate/Free Function
//============================================================================
uint8_t *dxContainer_CString_Init(uint8_t *pData, uint32_t length)
{
    if (pData == NULL ||
        length == 0)
    {
        return NULL;
    }

    uint8_t *p = (uint8_t *)dxMemAlloc(DXCONT_NAME, 1, length + 1);
    if (p)
    {
        memset(p, 0 , length + 1);
        memcpy(p, pData, length);
    }

    return p;
}

void dxContainer_CString_Free(uint8_t *pData)
{
    if (pData)
    {
        dxMemFree(pData);
        pData = NULL;
    }
}

VarLenType_t *dxContainer_VarLenType_Init(uint8_t *pData, uint32_t length)
{
    if (pData == NULL ||
        length == 0)
    {
        return NULL;
    }

    VarLenType_t *p = (VarLenType_t *)dxMemAlloc(DXCONT_NAME, 1, sizeof(VarLenType_t));
    if (p)
    {
        p->pData = dxContainer_CString_Init(pData, length);
        p->Length = length;
    }

    return p;
}

void dxContainer_VarLenType_Free(VarLenType_t *p)
{
    if (p)
    {
        if (p->pData && p->Length)
        {
            dxMemFree(p->pData);
        }

        p->Length = 0;

        dxMemFree(p);
        p = NULL;
    }
}

//============================================================================
// dxContainer Access Function
//============================================================================
dxContainerD_t *dxContainerD_Create(uint64_t ContDID,
                                    uint64_t ContMID,
                                    uint8_t *ContDName,
                                    uint32_t ContDType,
                                    uint32_t DateTime,
                                    VarLenType_t *SValue,
                                    VarLenType_t *LValue,
                                    uint16_t ContLVCRC)
{
    dxContainerD_t *p = (dxContainerD_t *)dxMemAlloc(DXCONT_NAME, 1, sizeof(dxContainerD_t));
    if (p == NULL)
    {
        return NULL;
    }

    p->ContDID   = ContDID;
    p->ContMID   = ContMID;
    if (ContDName)
    {
        p->ContDName = dxContainer_VarLenType_Init(ContDName, strlen((char *)ContDName));
    }
    p->ContDType = ContDType;
    p->DateTime  = DateTime;
    p->SValue    = (SValue) ? dxContainer_VarLenType_Init(SValue->pData, SValue->Length) : NULL;
    p->LValue    = (LValue) ? dxContainer_VarLenType_Init(LValue->pData, LValue->Length) : NULL;
    p->ContLVCRC = ContLVCRC;

    return p;
}

void dxContainerD_Free(dxContainerD_t *pContD)
{
    if (pContD)
    {
        if (pContD->ContDName)
        {
            dxContainer_VarLenType_Free(pContD->ContDName);
        }

        if (pContD->SValue)
        {
            dxContainer_VarLenType_Free(pContD->SValue);
        }

        if (pContD->LValue)
        {
            dxContainer_VarLenType_Free(pContD->LValue);
        }

        dxMemFree(pContD);
    }

    pContD = NULL;
}

dxContainerM_t *dxContainerM_Create(uint64_t ContMID,
                                    uint32_t BLEObjectID,
                                    uint32_t ContMType,
                                    uint8_t *ContMName)
{
    dxContainerM_t *p = (dxContainerM_t *)dxMemAlloc(DXCONT_NAME, 1, sizeof(dxContainerM_t));
    if (p == NULL)
    {
        return NULL;
    }

    p->ContMID      = ContMID;
    p->BLEObjectID  = BLEObjectID;
    p->ContMType    = ContMType;
    if (ContMName)
    {
        p->ContMName    = dxContainer_VarLenType_Init(ContMName, strlen((char *)ContMName));
    }

    return p;
}

void dxContainerM_Free(dxContainerM_t *pContM)
{
    if (pContM)
    {
        if (pContM->ContMName)
        {
            dxContainer_VarLenType_Free(pContM->ContMName);
        }

        dxMemFree(pContM);
    }

    pContM = NULL;
}

dxCONT_RET_CODE dxContainerM_Append(dxContainerM_t *pContM, dxContainerD_t *pContD)
{
    if (pContM == NULL || pContD == NULL)
    {
        return DXCONT_RET_ERROR;
    }

    dxContainerD_t *pFirstD = pContM->pContDList;

    dxContainerD_t *pLastD = dxContainerD_GetLast(pFirstD);

    if (pLastD == NULL)
    {
        pContM->pContDList = pContD;
        pContD->pNextContD = NULL;
    }
    else
    {
        pLastD->pNextContD = pContD;
        pContD->pNextContD = NULL;
    }

    return DXCONT_RET_SUCCESS;
}

dxContainer_t *dxContainer_Create(void)
{
    dxContainer_t *p = (dxContainer_t *)dxMemAlloc(DXCONT_NAME, 1, sizeof(dxContainer_t));

    return p;
}

dxContainer_t *dxContainer_Duplicate(dxContainer_t *pCont, dxContCopyOption_t copyOption)
{
    if (pCont == NULL)
    {
        return NULL;
    }

    dxContainer_t *pNewCont = dxContainer_Create();
    if (pNewCont == NULL)
    {
        return NULL;
    }

    // Evaluate all data
    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        if (copyOption == DXCONT_COPY_OPTION_DIRTYFLAG && p->DirtyFlag == 0)
        {
            p = p->pNextContM;
            continue;
        }

        dxContainerM_t *pNewContM = dxContainerM_Create(p->ContMID,
                                                        p->BLEObjectID,
                                                        p->ContMType,
                                                        (p->ContMName) ? p->ContMName->pData : NULL);

        if (pNewContM == NULL)
        {
            dxContainer_Free(pNewCont);
            pNewCont = NULL;
            break;
        }

        dxContainer_Append(pNewCont, pNewContM);

        dxContainerD_t *q = p->pContDList;
        while (q)
        {
            dxContainerD_t *pNewContD = dxContainerD_Create(q->ContDID,
                                                            q->ContMID,
                                                            (q->ContDName) ? q->ContDName->pData : NULL,
                                                            q->ContDType,
                                                            q->DateTime,
                                                            q->SValue,
                                                            q->LValue,
                                                            q->ContLVCRC);

            if (pNewContD == NULL)
            {
                dxContainer_Free(pNewCont);
                pNewCont = NULL;
                break;
            }

            dxContainerM_Append(pNewContM, pNewContD);

            q = q->pNextContD;
        }

        p = p->pNextContM;
    }

    return pNewCont;
}

void dxContainer_Free(dxContainer_t *pCont)
{
    if (pCont == NULL)
    {
        return;
    }

    while (1)
    {
        dxContainerM_t *p = pCont->pContMList;

        if (p == NULL)
        {
            break;
        }

        while (1)
        {
            dxContainerD_t *q = p->pContDList;

            if (q == NULL)
            {
                break;
            }

            p->pContDList = q->pNextContD;
            dxContainerD_Free(q);
        }

        pCont->pContMList = p->pNextContM;
        dxContainerM_Free(p);
    }

    dxMemFree(pCont);
}

dxCONT_RET_CODE dxContainer_Append(dxContainer_t *pCont, dxContainerM_t *pContM)
{
    if (pCont == NULL || pContM == NULL)
    {
        return DXCONT_RET_ERROR;
    }

    dxContainerM_t *pFirstM = pCont->pContMList;

    dxContainerM_t *pLastM = dxContainerM_GetLast(pFirstM);

    if (pLastM == NULL)
    {
        pCont->pContMList = pContM;
        pContM->pNextContM = NULL;
    }
    else
    {
        pLastM->pNextContM = pContM;
        pContM->pNextContM = NULL;
    }

    return DXCONT_RET_SUCCESS;
}

uint32_t dxContainer_GetMDataCount(dxContainer_t *pCont)
{
    uint32_t count = 0;

    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        count++;

        p = p->pNextContM;
    }

    return count;
}

dxContainerM_t *dxContainer_GetMDataByIndex(dxContainer_t *pCont, uint32_t index)
{
    uint32_t count = 0;
    uint8_t found = 0;

    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        if (count >= index)
        {
            found = 1;
            break;
        }

        count++;

        p = p->pNextContM;
    }

    if (found == 0)
    {
        p = NULL;
    }

    return p;
}

uint32_t dxContainer_GetDDataCount(dxContainerM_t *pContM)
{
    uint32_t count = 0;

    dxContainerD_t *q = pContM->pContDList;
    while (q)
    {
        count++;

        q = q->pNextContD;
    }

    return count;
}

dxContainerD_t *dxContainer_GetDDataByIndex(dxContainerM_t *pContM, uint32_t index)
{
    uint32_t count = 0;
    uint8_t found = 0;

    dxContainerD_t *q = pContM->pContDList;
    while (q)
    {
        if (count >= index)
        {
            found = 1;
            break;
        }

        count++;

        q = q->pNextContD;
    }

    if (found == 0)
    {
        q = NULL;
    }

    return q;
}

//============================================================================
// dxContainer Search/Find Function
//============================================================================
dxContainerM_t *dxContainer_FindMDataByType(dxContainer_t *pCont, dxContainerM_t *pStartFrom, uint32_t MType)
{
    uint8_t foundStartObject = 0;
    uint8_t found = 0;

    if (pStartFrom == NULL)
    {
        foundStartObject = 1;
    }

    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        if (foundStartObject == 1 &&
            p->ContMType == MType)
        {
            found = 1;
            break;
        }

        if (p == pStartFrom)
        {
            foundStartObject = 1;
        }

        p = p->pNextContM;
    }

    if (found == 0)
    {
        p = NULL;
    }

    return p;
}

dxContainerD_t *dxContainer_FindDDataByType(dxContainer_t *pCont, dxContainerD_t *pStartFrom, uint32_t DType)
{
    uint8_t foundStartObject = 0;
    uint8_t found = 0;

    if (pStartFrom == NULL)
    {
        foundStartObject = 1;
    }

    dxContainerM_t *p = pCont->pContMList;
    dxContainerD_t *q = NULL;

    while (p)
    {
        q = p->pContDList;
        while (q)
        {
            if (foundStartObject == 1 &&
                q->ContDType == DType)
            {
                found = 1;
                break;
            }

            if (q == pStartFrom)
            {
                foundStartObject = 1;
            }

            q = q->pNextContD;
        }

        if (found == 1)
        {
            break;
        }

        p = p->pNextContM;
    }

    if (found == 0)
    {
        q = NULL;
    }

    return q;
}

dxContainerM_t *dxContainer_FindMDataByID(dxContainer_t *pCont, uint64_t ContMID)
{
    uint8_t found = 0;

    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        if (p->ContMID == ContMID)
        {
            found = 1;
            break;
        }

        p = p->pNextContM;
    }

    if (found == 0)
    {
        p = NULL;
    }

    return p;
}

dxContainerD_t *dxContainer_FindDDataByID(dxContainer_t *pCont, uint64_t ContDID)
{
    uint8_t found = 0;

    dxContainerM_t *p = pCont->pContMList;
    dxContainerD_t *q = NULL;

    while (p)
    {
        q = p->pContDList;
        while (q)
        {
            if (q->ContDID == ContDID)
            {
                found = 1;
                break;
            }

            q = q->pNextContD;
        }

        if (found == 1)
        {
            break;
        }

        p = p->pNextContM;
    }

    if (found == 0)
    {
        q = NULL;
    }

    return q;
}

dxContainerM_t *dxContainerM_GetLast(dxContainerM_t *pContM)
{
    if (pContM == NULL)
    {
        return NULL;
    }

    dxContainerM_t *p = pContM;

    while (p->pNextContM)
    {
        p = p->pNextContM;
    }

    return p;
}

dxContainerD_t *dxContainerD_GetLast(dxContainerD_t *pContD)
{
    if (pContD == NULL)
    {
        return NULL;
    }

    dxContainerD_t *p = pContD;

    while (p->pNextContD)
    {
        p = p->pNextContD;
    }

    return p;
}


void PRINT_Container(dxContainer_t *pCont)
{
    // Evaluate all data
    dxContainerM_t *p = pCont->pContMList;
    const char *pEmptyString = "";

    while (p)
    {
        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);
        uint8_t *pContMName = (uint8_t *)pEmptyString;
        if (p->ContMName)
        {
            pContMName = p->ContMName->pData;
        }

        G_SYS_DBG_LOG_NV("Line %d,\n"
                         "\tContMID: %s\n"
                         "\tBLEObjectID: %d\n"
                         "\tContMType: %u\n"
                         "\tContMName: [%d]%s\n",
                         __LINE__,
                         pContMID,
                         p->BLEObjectID,
                         p->ContMType,
                         (p->ContMName) ? p->ContMName->Length : 0, pContMName);

        dxContainer_UINT64_Free(pContMID);

        dxContainerD_t *q = p->pContDList;
        while (q)
        {
            uint8_t *pContDID = dxContainer_UINT64_ToStr(q->ContDID);
            uint8_t *pContMID = dxContainer_UINT64_ToStr(q->ContMID);
            uint8_t *pContDName = (uint8_t *)pEmptyString;

            if (q->ContDName)
            {
                pContDName = q->ContDName->pData;
            }

            uint8_t *pSValue = (uint8_t *)pEmptyString;
            if (q->SValue)
            {
                pSValue = q->SValue->pData;
            }

            uint8_t *pLValue = (uint8_t *)pEmptyString;

            if (q->LValue)
            {
                pLValue = q->LValue->pData;
            }

            G_SYS_DBG_LOG_NV("Line %d,\n"
                             "\tContDID: %s\n"
                             "\tContMID: %s\n"
                             "\tContDName: [%d]%s\n"
                             "\tContDType: %d\n"
                             "\tDateTime: %d\n"
                             "\tSValue: [%d]%s\n",
                             __LINE__,
                             pContDID,
                             pContMID,
                             (q->ContDName) ? q->ContDName->Length : 0, pContDName,
                             q->ContDType,
                             q->DateTime,
                             (q->SValue) ? q->SValue->Length : 0, pSValue);

            G_SYS_DBG_LOG_NV("\tLValue: [%d]%s\n"
                             "\tContLVCRC: %d\n\n",
                             (q->LValue) ? q->LValue->Length : 0, pLValue,
                             q->ContLVCRC);

            dxContainer_UINT64_Free(pContMID);
            dxContainer_UINT64_Free(pContDID);

            q = q->pNextContD;
        }

        G_SYS_DBG_LOG_NV("\n");
        p = p->pNextContM;
    }
}
