//============================================================================
// File: DKAPI_UpdatePeripheralStatusInfo.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_UpdatePeripheralStatusInfo_Response()
//            - Build_UpdatePeripheralStatusInfo_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_UpdatePeripheralStatusInfo_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_UpdatePeripheralStatusInfo.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_UPDATEPERIPHERALSTATUSINFO_BODY                                                  \
                                                    "{\"Data\":[{\"ObjectID\":%u,\"PSignalStr\":%u,\"PBattery\":%u,\"Status\":%d}]}"
#define DKMSG_TEMPLATE_UPDATEPERIPHERALSTATUSINFO   "%s "                                               \
                                                    "%s/UpdatePeripheralStatusInfo.php HTTP/1.1\r\n"    \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_UPDATEPERIPHERALSTATUSINFO_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_UpdatePeripheralStatusInfo_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (UpdatePeripheralStatusData_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    UpdatePeripheralStatusData_t *pPeripheralStatus = (UpdatePeripheralStatusData_t *)pCommMsg->pData;

    if (pPeripheralStatus->ObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         pPeripheralStatus->ObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_UPDATEPERIPHERALSTATUSINFO,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_UPDATEPERIPHERALSTATUSINFO_BODY, (unsigned int)pPeripheralStatus->ObjectID, (unsigned int)pPeripheralStatus->PSignalStr, (unsigned int)pPeripheralStatus->PBattery, pPeripheralStatus->RunningStatus),
			
            ObjectDictionary_GetSessionToken (),
            (unsigned int)pPeripheralStatus->ObjectID,
            (unsigned int)pPeripheralStatus->PSignalStr,
            (unsigned int)pPeripheralStatus->PBattery,
            pPeripheralStatus->RunningStatus);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_UpdatePeripheralStatusInfo_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;
    int Idx = 0;                        // Pointer to pParameters

    int periphpayloadnode = 0;          // index of peripheral data payload result
    int periphpayloadcount;
    int childnode = 0;                  // child of peripheralnode

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    uint32_t objid = 0;                 // An variable to store the ObjectID which convert from strtoul()
    (void)objid;                        // Avoid compilation warning when ENABLE_SYSTEM_DEBUGGER is disabled
#endif

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_OBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (UpdatePeripheralStatusData_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    periphpayloadnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (periphpayloadnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    periphpayloadcount = DKJSN_GetObjectCount(handle, periphpayloadnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of results found: %d\n", periphpayloadcount);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < periphpayloadcount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, periphpayloadnode, i);
        if (childnode < 0)
        {
            // Can't select the job node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get ObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList3[0]);
            DKJSN_Uninit(handle);
            return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d ObjectID: %s\n", i, szBuffer);
#endif

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        objid = strtoul(szBuffer, NULL, 10);
#endif

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------
        if (Idx < paramlen)
        {
            pCommMsg = (DKRESTCommMsg_t *)&pParameters[Idx];

            // Send data to worker thread
            DKServerManager_SendEventIDToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                      pCommMsg->Header.TaskIdentificationNumber,
                                                      pCommMsg->Header.MessageType,
                                                      HTTP_OK,
                                                      ret);

            Idx += (sizeof(DKRESTCommMsg_t) + pCommMsg->Header.DataLen);
        }
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

    return ret;
}
