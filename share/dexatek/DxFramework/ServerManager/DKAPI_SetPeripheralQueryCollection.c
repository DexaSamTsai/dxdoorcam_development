//============================================================================
// File: DKAPI_SetPeripheralQueryCollection.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Build_SetPeripheralQueryCollection_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_SetPeripheralQueryCollection_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_SetPeripheralQueryCollection.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_SETPERIPHERALCOLLECTION_BODY "{\"Data\":{\"GMACAddress\":\"%s\",\"Peripherals\":[{\"PMACAddress\":\"%s\",\"DeviceType\":%u,\"PSignalStr\":%u}]}}"
#define DKMSG_TEMPLATE_SETPERIPHERALCOLLECTION      "%s "                                               \
                                                    "%s/SetPeripheralQueryCollection.php HTTP/1.1\r\n"  \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_SETPERIPHERALCOLLECTION_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_SetPeripheralQueryCollection_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    PeripheralQuery_t *pPeriQuery = NULL;
    int maclen = 0;

    char szGMACData[21] = {0};              // To stored GMACAddress
    char szPMACData[21] = {0};              // To stored PMACAddress

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (PeripheralQuery_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, DKMSG_SETPERIPHERALQUERYCOLLECTION parameters are invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    pPeriQuery = (PeripheralQuery_t *)pCommMsg->pData;

    ObjectDictionary_Lock ();

    maclen = _MACAddrToDecimalStr (szGMACData, sizeof(szGMACData), (uint8_t*) ObjectDictionary_GetMacAddress (), 8);
    if (maclen <= 0)
    {
        DKHex_HexToASCII (ObjectDictionary_GetMacAddress (), 8, &szGMACData[0], sizeof(szGMACData));

        ObjectDictionary_Unlock ();

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, GMAC address %s convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__,
                         szGMACData);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Unlock ();

    maclen = _MACAddrToDecimalStr(szPMACData, sizeof(szPMACData), (uint8_t *)pPeriQuery->PMAC, sizeof(pPeriQuery->PMAC));
    if (maclen <= 0)
    {
        DKHex_HexToASCII((char *)pPeriQuery->PMAC, sizeof(pPeriQuery->PMAC), &szPMACData[0], sizeof(szPMACData));
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, PMAC address %s convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__,
                         szPMACData);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_SETPERIPHERALCOLLECTION,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_SETPERIPHERALCOLLECTION_BODY, szGMACData, szPMACData, pPeriQuery->DeviceType, pPeriQuery->PSignalStr),
            ObjectDictionary_GetSessionToken (),
            szGMACData,
            szPMACData,
            pPeriQuery->DeviceType,
            pPeriQuery->PSignalStr);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_SetPeripheralQueryCollection_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    return DKSERVER_STATUS_SUCCESS;
}
