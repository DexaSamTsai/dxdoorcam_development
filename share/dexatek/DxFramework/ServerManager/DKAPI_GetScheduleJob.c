//============================================================================
// File: DKAPI_GetScheduleJob.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Added external function declaration
//
//     Version: 0.4
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Parse_GetScheduleJob_Response()
//
//     2) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetScheduleJob_Response()
//            - Build_GetScheduleJob_Data()
//
//     Version: 0.5
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GetScheduleJob_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetScheduleJob.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETSCHEDULEJOB_BODY          "{\"GatewayID\":%u,\"StartFrom\":%d,\"MaxNumOfSchJobToRetrieve\":%d}"
#define DKMSG_TEMPLATE_GETSCHEDULEJOB               "%s "                                               \
                                                    "%s/GetScheduleJob.php HTTP/1.1\r\n"                \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GETSCHEDULEJOB_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetScheduleJob_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int nStartFrom = 0;
    int nMaxNumToRetrieve = 0;
    int idx = 0;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (int) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, ScheduleJob information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    ObjectDictionary_Lock ();
    DK_BLEGateway_t *pThisGateway = (DK_BLEGateway_t*) ObjectDictionary_GetGateway ();
    ObjectDictionary_Unlock ();

    if (pThisGateway == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memcpy(&nStartFrom, &pCommMsg->pData[idx], sizeof(int));
    idx += sizeof(int);
    memcpy(&nMaxNumToRetrieve, &pCommMsg->pData[idx], sizeof(int));

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETSCHEDULEJOB,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETSCHEDULEJOB_BODY, (unsigned int)pThisGateway->ObjectID, nStartFrom, nMaxNumToRetrieve),
            ObjectDictionary_GetSessionToken (),
            (unsigned int)pThisGateway->ObjectID,
            nStartFrom,
            nMaxNumToRetrieve);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetScheduleJob_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    ScheduleJobStatus_t* scheduleStatus = NULL;
    DK_BLEGateway_t* gateway = NULL;
    DKRESTCommMsg_t* pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0

    int schjobsnode = 0;                // index of SchJobs node
    int childnode = 0;                  // child of SchJobs node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;

    int schjobCount = 0;                // Number of SchJobs

    uint32_t objid = 0;                 // ObjectID
    uint32_t gatewayid = 0;             // GatewayID
    uint64_t schseqno = 0;              // ScheduleSEQNO

    int nStartFrom = 0;                 // Start index of TotalSchJobs
    int joblen = 0;                     // Length of JobInformation, after base64 deccoding
    uint8_t *pJobInfo = NULL;           // Job information
    ScheduleJobInfo_t *pSchJob = NULL;  // Schedule job information

    int i = 0;

    int findList1[] = {DKJSN_TOKEN_STATUS, DKJSN_TOKEN_UPDATETIME, 0};

    int findList2[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_TOTALSCHJOBS, 0};
    int findList3[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_SCHJOBS, 0};
    int findList4[] = {DKJSN_TOKEN_OBJECTID, 0};
    int findList5[] = {DKJSN_TOKEN_GATEWAYID, 0};
    int findList6[] = {DKJSN_TOKEN_SCHEDULESEQNO, 0};
    int findList7[] = {DKJSN_TOKEN_JOBINFORMATION, 0};

    scheduleStatus = (ScheduleJobStatus_t*) dxMemAlloc ("scheduleStatus", 1, sizeof (ScheduleJobStatus_t));
    if (scheduleStatus == NULL) {
        ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
        goto alloc_fail;
    }

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (int) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, ScheduleJob information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    ObjectDictionary_Lock ();
    gateway = (DK_BLEGateway_t*) ObjectDictionary_GetGateway ();
    ObjectDictionary_Unlock ();

    if (gateway == NULL)
    {
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    memcpy(&nStartFrom, &pCommMsg->pData[0], sizeof(int));

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        goto fail;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        ret = DKSERVER_SYSTEM_ERROR_JSON_INIT;
        goto fail;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Status updatetime of schedule jobs
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList1, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d:%d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList1[0],
                         findList1[1]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    scheduleStatus->updatetime = strtoull (szBuffer, NULL, 10);

    // Get TotalSchJobs

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d:%d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList2[0],
                         findList2[1]);
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "TotalSchJobs: %s\n", szBuffer);
#endif

    scheduleStatus->totalSchedules = strtoul(szBuffer, NULL, 10);
    if (scheduleStatus->totalSchedules == 0) {
        DKJSN_Uninit(handle);
        ret = DKSERVER_STATUS_SUCCESS;
        goto fail;
    }

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETSCHEDULEJOB,
                                            DKJOBTYPE_STANDARD,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            scheduleStatus,
                                            sizeof (ScheduleJobStatus_t));

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get SchJobs

    schjobsnode = DKJSN_SelectObjectByList(handle, findList3, rootnode);
    if (schjobsnode < 0)
    {
        // "Results":"SchJobs" not found
        DKJSN_Uninit(handle);
        ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
        goto fail;
    }

    schjobCount = DKJSN_GetObjectCount(handle, schjobsnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of SchJobs found: %d\n", schjobCount);

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < schjobCount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, schjobsnode, i);
        if (childnode < 0)
        {
            // Can't select the job node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }


        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get ObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList4[0]);
            DKJSN_Uninit(handle);
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            goto fail;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d ObjectID: %s\n", i, szBuffer)
#endif
        objid = strtoul(szBuffer, NULL, 10);
        if (objid == 0)
        {
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get GatewayID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList5[0]);
            DKJSN_Uninit(handle);
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            goto fail;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d GatewayID: %s\n", i, szBuffer);
#endif
        gatewayid = strtoul(szBuffer, NULL, 10);
        if (gatewayid == 0 || gateway->ObjectID != gatewayid)
        {
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get ScheduleSEQNO

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList6[0]);
            DKJSN_Uninit(handle);
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            goto fail;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d ScheduleSEQNO: %s\n", i, szBuffer);
#endif

        schseqno = strtoull(szBuffer, NULL, 10);
        if (schseqno == 0)
        {
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get JobInformation

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList7, childnode, 1);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList7[0]);
            DKJSN_Uninit(handle);
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            goto fail;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Preparing JobInformation

        joblen = sizeof(ScheduleJobInfo_t) + len;

        //pJobInfo = dxMemAlloc(CALLOC_NAME, 1, joblen);
        pJobInfo = DKMSG_AllocMemory(joblen);
        if (pJobInfo == NULL)
        {
            ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
            goto fail;
        }

        pSchJob = (ScheduleJobInfo_t *)pJobInfo;
        memcpy(pSchJob->pPayload, szBuffer, len);

        // Preparing nScheduleSEQNO

        pSchJob->nScheduleSEQNO = schseqno;

        // Preparing nIndex

        pSchJob->nIndex = i + nStartFrom;

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                pCommMsg->Header.TaskIdentificationNumber,
                                                DKMSG_GETSCHEDULEJOB,
                                                DKJOBTYPE_SCHEDULE,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                (void *)pSchJob,
                                                joblen);

        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;
    }

    DKJSN_Uninit(handle);

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        // Send JobInformation data to worker thread
        DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                pCommMsg->Header.TaskIdentificationNumber,
                                                DKMSG_GETSCHEDULEJOB,
                                                DKJOBTYPE_NONE,
                                                HTTP_OK,
                                                DKSERVER_REPORT_STATUS_SUCCESS,
                                                NULL,
                                                0);

        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

fail :

    DKMSG_FreeMemory ((uint8_t*) scheduleStatus);
    scheduleStatus = NULL;

alloc_fail :

    return ret;
}
