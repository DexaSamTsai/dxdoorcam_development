//============================================================================
// File: DKAPI_GetPushRTMPLiveURL.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/08/15
//     1) Description: Initial
//
//     Version: 0.253.0
//     Date: 2017/08/30
//     1) Description: Revise IPCam API
//        Modified:
//            - Build_GetEventMediaFileUploadPath_Data()
//            - Parse_GetEventMediaFileUploadPath_Response()
//        Added:
//            - Memory_IPCamEventFileREP_free()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetEventMediaFileUploadPath.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETEVENTMEDIAFILEUPLOADPATH_API                                                  \
                                                    "%s "                                               \
                                                    "%s/GetEventMediaFileUploadPath HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetEventMediaFileUploadPath_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;

    uint32_t GatewayID = 0;                 // ObjectID of Gateway
    uint8_t nNumOfAdvJobAppend = 0;         // Number of {"AdvObjID":<ObjectID>,"GatewayID":<GatewayID>,"UserObjectID":<UserObjectID>} appended to buffer

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (IPCamEventFileREQ_t)) ||
        paramlen > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    sprintf((char *)pBuffer, DKMSG_TEMPLATE_GETEVENTMEDIAFILEUPLOADPATH_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    IPCamEventFileREQ_t *pIPCamE = (IPCamEventFileREQ_t *)&pCommMsg->pData[i];

    if (pIPCamE == NULL)
    {
        // No data
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pIPCamE->ObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         pIPCamE->ObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Start creating JSON
    strcat((char *)pBuffer, "{");

    // Append "BLEObjectID" to JSON
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\"BLEObjectID\":%u", (unsigned int)pIPCamE->ObjectID);

    // Append "Type" to JSON
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], ",\"Type\":%u", (unsigned int)pIPCamE->Type);

    // Append "FileFormat" to JSON
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], ",\"FileFormat\":\"%s\"", pIPCamE->FileFormat);

    if (pIPCamE->DateTime > 0)
    {
        // Append "DateTime" to JSON
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], ",\"DateTime\":%u", (unsigned int)pIPCamE->DateTime);
    }

    // Create "AdvJob" to JSON
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], ",\"AdvJob\":{");

    if (pIPCamE->AdvObjID > 0)
    {
        // Append "AdvObjID" to JSON
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"AdvObjID\":%u", (unsigned int)pIPCamE->AdvObjID);
        nNumOfAdvJobAppend++;
    }

    ObjectDictionary_Lock ();

    if (ObjectDictionary_GetGateway () == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, ObjectDictionary_GetGateway () == NULL\n",
                         __FUNCTION__,
                         __LINE__);
        ObjectDictionary_Unlock ();
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    GatewayID = ObjectDictionary_GetGateway ()->ObjectID;

    ObjectDictionary_Unlock ();

    if (GatewayID > 0)
    {
        if (nNumOfAdvJobAppend > 0)
        {
            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n], ",");
        }

        // Append "GatewayID" to JSON
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"GatewayID\":%u", (unsigned int)GatewayID);
    }

    // Finish "AdvJob"
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "}");

    // Finish creating JSON
    strcat((char *)pBuffer, "}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }

    return ret;
}

int Parse_GetEventMediaFileUploadPath_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;                          // index

    int resultnode = 0;                 // index of result

    IPCamEventFileREP_t *pIPCamFileREP = NULL;
    uint16_t UploadURL_Length = 0;      // length of "UploadURL"
    uint16_t DownloadURL_Length = 0;    // length of "DownloadURL"
//    uint16_t MessageData_Length = 0;    // length of "MessageData"

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    uint32_t objid = 0;                 // An variable to store the ObjectID which convert from strtoul()
    (void)objid;                        // Avoid compilation warning when ENABLE_SYSTEM_DEBUGGER is disabled
#endif

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_UPLOADURL, 0};
    int findList4[] = {DKJSN_TOKEN_DOWNLOADURL, 0};
//    int findList5[] = {DKJSN_TOKEN_MESSAGEDATA, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (DKRESTCommMsg_t) + sizeof (IPCamEventFileREQ_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    resultnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (resultnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get data length of UploadURL

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList3[0]);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    UploadURL_Length = len;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] Line %d, Length of \"UploadURL\" = %d\n",
                     __FUNCTION__,
                     __LINE__,
                     UploadURL_Length);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get data length of DownloadURL

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList4[0]);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    DownloadURL_Length = len;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "[%s] Line %d, Length of \"DownloadURL\" = %d\n",
                     __FUNCTION__,
                     __LINE__,
                     DownloadURL_Length);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get data length of MessageData

    //len = STRING_BUFFER_SIZE;
    //ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, resultnode, 0);
    //if (ret != DKSERVER_STATUS_SUCCESS)
    //{
    //    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
    //                     "[%s] Line %d, JSON key %d not found\n",
    //                     __FUNCTION__,
    //                     __LINE__,
    //                     findList5[0]);
    //    DKJSN_Uninit(handle);
    //    return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    //}
    //
    //MessageData_Length = len;
    //
    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
    //                 "[%s] Line %d, Length of \"MessageData\" = %d\n",
    //                 __FUNCTION__,
    //                 __LINE__,
    //                 MessageData_Length);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Allocate a large enough memory

    uint8_t *pMsgREP = DKMSG_AllocMemory(sizeof(IPCamEventFileREP_t) + (UploadURL_Length + 1) + (DownloadURL_Length + 1)/* + (MessageData_Length + 1)*/);       // 1 => for 0x00

    if (pMsgREP == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc() ERROR\n",
                         __FUNCTION__,
                         __LINE__);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    pIPCamFileREP = (IPCamEventFileREP_t *)pMsgREP;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get UploadURL

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList3[0]);

        DKMSG_FreeMemory(pMsgREP);
        pMsgREP = NULL;
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "UploadURL: %s\n", szBuffer);
#endif

    // Remove JSON escape character
    szBuffer[len] = 0x00;
    len = DKMSG_JSONRemoveEscapeChar((uint8_t *)szBuffer, STRING_BUFFER_SIZE, (uint8_t *)szBuffer);

    pIPCamFileREP->UploadURL_Offset = i;
    pIPCamFileREP->UploadURL_Length = len + 1;

    memcpy(&pIPCamFileREP->pPayload[i], szBuffer, len);

    i += pIPCamFileREP->UploadURL_Length;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get DownloadURL

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, resultnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList4[0]);

        DKMSG_FreeMemory(pMsgREP);
        pMsgREP = NULL;
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "DownloadURL: %s\n", szBuffer);
#endif

    // Remove JSON escape character
    szBuffer[len] = 0x00;
    len = DKMSG_JSONRemoveEscapeChar((uint8_t *)szBuffer, STRING_BUFFER_SIZE, (uint8_t *)szBuffer);

    pIPCamFileREP->DownloadURL_Offset = i;
    pIPCamFileREP->DownloadURL_Length = len + 1;

    memcpy(&pIPCamFileREP->pPayload[i], szBuffer, len);

    i += pIPCamFileREP->DownloadURL_Length;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get MessageData

//    len = STRING_BUFFER_SIZE;
//    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, resultnode, 0);
//    if (ret != DKSERVER_STATUS_SUCCESS)
//    {
//        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
//                         "[%s] Line %d, JSON key %d not found\n",
//                         __FUNCTION__,
//                         __LINE__,
//                         findList5[0]);
//
//        DKMSG_FreeMemory(pMsgREP);
//        pMsgREP = NULL;
//        DKJSN_Uninit(handle);
//        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
//    }
//
//#if defined(ENABLE_LOG_INFORMATION_DETAILS)
//    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
//                     "MessageData: %s\n", szBuffer);
//#endif
//
//    // Remove JSON escape character
//    szBuffer[len] = 0x00;
//    len = DKMSG_JSONRemoveEscapeChar((uint8_t *)szBuffer, STRING_BUFFER_SIZE, (uint8_t *)szBuffer);
//
//    pIPCamFileREP->MessageData_Offset = i;
//    pIPCamFileREP->MessageData_Length = MessageData_Length + 1;
//
//    memcpy(&pIPCamFileREP->pPayload[i], szBuffer, len);
//
//    i += pIPCamFileREP->MessageData_Length;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETEVENTMEDIAFILEUPLOADPATH,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)&pMsgREP[0],
                                            sizeof(IPCamEventFileREP_t) + i);

    DKMSG_FreeMemory(pMsgREP);
    pMsgREP = NULL;

    return DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
}
