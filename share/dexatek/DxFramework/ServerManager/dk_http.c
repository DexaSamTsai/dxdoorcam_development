//============================================================================
// File: dk_http.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/07
//     1) Description: Use default server (54.154.211.190) if dxDNS_GetHostByName() failed
//        Modified:
//            - DK_DNS_Lookup()
//
//     Version: 0.4
//     Date: 2016/03/08
//     1) Description: Use system default strcasecmp()/strncasecmp, instead of stricmp()/strnicmp
//        Modified:
//            - DK_GetHTTPResponseMessageBody()
//============================================================================
#include <string.h>

#include "dxOS.h"
#include "dk_http.h"
#include "Utils.h"
#include "ObjectDictionary.h"
#include "dxSysDebugger.h"
#include "dxDNS.h"

/******************************************************
 *                      Macros
 ******************************************************/
#define SP                                  0x20
#define HT                                  0x09
#define CR                                  0x0D
#define LF                                  0x0A

#define CALLOC_NAME                         "DKSM"
#define STATUS_LINE_HTTP_VERSION_PREFIX     "HTTP/"
#define HTTP_CONTENT_LENGTH_STRING          "Content-Length:"
#define ENABLE_SEEK_HTTP_CONTENT_LENGTH

//#define ENABLE_DUMP_HTTP_MESSAGE_BODY_BUFFER
//#define ENABLE_DUMP_ALL_HTTP_BUFFER

/******************************************************
 *                    Constants
 ******************************************************/
#define DNS_LOOKUP_TIMEOUT                          10000   // 10 seconds
#define TCP_CONNECT_TIMEOUT                         10000   // 10 seconds
#define TCP_CONNECT_RETRY                           1       // 3
#define HTTPS_RECEIVE_TIMEOUT                       10000   // 5 seconds
#define DEFAULT_HOST_IP_ADDR_A                      54
#define DEFAULT_HOST_IP_ADDR_B                      154
#define DEFAULT_HOST_IP_ADDR_C                      211
#define DEFAULT_HOST_IP_ADDR_D                      190

#define USE_KEEP_ALIVE

/******************************************************
 *                       Macros
 ******************************************************/
#define stricmp                                     strcasecmp
#define strnicmp                                    strncasecmp

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
static int _IsLWS(char *pszText);
/*static */int _IsCRLF(char *pszText);
static int _doesEndofHeader(char *pszText);
static int _doesEndofHeaderField(char *pszText);
static int _AllocDKSTR(PDKSTR pDKStr, char *pData, int cbSize);
static void _FreeDKSTR(PDKSTR pDKStr);
static void _AllocAndCopyTo(char **pDest, char *pSrc, int cbSize);
static char _IsCharInStr(char *pData, int cbSize, char ch);
static char _IsaSeparator(char ch);
static int _SkipSeparators(char *pszText, int cbSize);
static char *_GetNextToken(char *pszText, int cbSize, int *pLen);

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
/**
 * @fn _IsLWS
 *
 * @param pszText
 *
 * @return Number of LWS characters.
 *         Return 0 if the first character of pMessage is not LWS character.
 */
static int _IsLWS(char *pszText)
{
    // LWS = [CRLF] *(SP | HT)
    int i = 0;
    int len = strlen(pszText);
    int nCRLF = 0;

    char ch = 0;

    while (i < len)
    {
        ch = pszText[i];

        if (ch != 0x0D && ch != 0x0A && ch != SP && ch != HT)
        {
            // None LWS
            break;
        }

        nCRLF = _IsCRLF(&pszText[i]);
        if (i == 0 && nCRLF > 0)
        {
            i += nCRLF;
            continue;
        }

        if (_IsCRLF(&pszText[i]) > 0)
        {
            // CRLF => At most one occurrence.
            break;
        }

        if (ch == SP || ch == HT)
        {
            i++;
        }
    }

    return i;
}

/**
 * @fn _IsCRLF
 *
 * @param pszText
 *
 * @return Number of CR/LF characters
 */
/*static */int _IsCRLF(char *pszText)
{
    // CRLF = (CR | LF | CRLF)
    int i = 0;
    int ret = 0;
    int len = strlen(pszText);
    char ch = 0;

    while (i < len)
    {
        ch = pszText[i];

        if (ch == 0x0A)
        {
            ret++;
            break;
        }

        if (ch == 0x0D)             // There is a potential issue here.
        {                           // {0x0D 0x0D} will be 2 CRLF and will be end of HTTP header.
            ret++;
            i++;
            continue;
        }

        break;
    }

    return ret;
}

/**
 * @ _doesEndofHeader
 *
 * @param pszText
 *
 * @return Number of 2*(CRLF) characters
 */
static int _doesEndofHeader(char *pszText)
{
    int i = 0;
    int n = 0;
    int index = 0;

    for (i = 0; i < 2; i++)
    {
        n = _IsCRLF(&pszText[index]);
        if (n == 0)
        {
            index = 0;
            break;
        }
        index += n;
    }

    return index;
}

/**
 * @fn _doesEndofHeaderField
 *
 * @param pszText
 *
 * @return Number of characters if ending of (field-name ":" field-value)
 */
static int _doesEndofHeaderField(char *pszText)
{
    int index = 0;
    int n = _IsCRLF(&pszText[index]);

    if (n == 0)
    {
        return 0;
    }

    index += n;

    if (pszText[index] == SP ||
        pszText[index] == HT)
    {
        return 0;
    }

    return index;
}

/**
 * @fn _AllocDKSTR
 *
 * @param pDKStr
 * @param pData
 * @param cbSize
 *
 * @return Number of characters allocated.
 */
static int _AllocDKSTR(PDKSTR pDKStr, char *pData, int cbSize)
{
    if (pDKStr == NULL || pData == NULL || cbSize == 0) return 0;

    /**
     * NOTE:
     *           dxMemAlloc("NAME", num, sizeof(uint8_t)) is not the same as
     *           dxMemAlloc("NAME", 1, sizeof(uint8_t) * num))
     *           Because sizeof(uint8_t) will be alignment to 32 bytes
     *           which cause the total allocating memory of dxMemAlloc("NAME", num, sizeof(uint8_t))
     *           is num x 32 bytes.
     */
    pDKStr->pszValue = (char *)dxMemAlloc(CALLOC_NAME, 1, cbSize + 1);
    if (pDKStr->pszValue == NULL) return 0;

    memset(pDKStr->pszValue, 0x00, cbSize + 1);

    pDKStr->cbSize = cbSize + 1;

    //memset(pDKStr->pszValue, 0x00, cbSize + 1);
    memcpy(pDKStr->pszValue, pData, cbSize);

    return cbSize;
}

/**
 * @fn _FreeDKSTR
 *
 * @param pDKStr
 */
static void _FreeDKSTR(PDKSTR pDKStr)
{
    if (pDKStr)
    {
        if (pDKStr->cbSize > 0)
        {
            pDKStr->cbSize = 0;
            if (pDKStr->pszValue)
            {
                dxMemFree(pDKStr->pszValue);
            }
            pDKStr->pszValue = NULL;
        }
    }
}

/**
 * @fn _AllocAndCopyTo
 *
 * @param pDest
 * @param pSrc
 *
 * @param cbSize
 */
static void _AllocAndCopyTo(char **pDest, char *pSrc, int cbSize)
{
    if (pSrc == NULL || cbSize <= 0)
    {
        return ;
    }

    /**
     * NOTE:
     *           dxMemAlloc("NAME", num, sizeof(uint8_t)) is not the same as
     *           dxMemAlloc("NAME", 1, sizeof(uint8_t) * num))
     *           Because sizeof(uint8_t) will be alignment to 32 bytes
     *           which cause the total allocating memory of dxMemAlloc("NAME", num, sizeof(uint8_t))
     *           is num x 32 bytes.
     */
    *pDest = (char *)dxMemAlloc(CALLOC_NAME, 1, cbSize + 1);

    if (*pDest == NULL)
    {
        return ;
    }

    memset(*pDest, 0x00, cbSize + 1);
    memcpy(*pDest, pSrc, cbSize);
}

/**
 * @fn _IsCharInStr
 *
 * @param pszText
 * @param cbSize
 * @param ch
 *
 * @return
 */
static char _IsCharInStr(char *pszText, int cbSize, char ch)
{
    int i = 0;
    char bFound = 0;

    if (pszText == NULL || cbSize <= 0)
    {
        return 0;   // False
    }

    for (i = 0; i < cbSize; i++)
    {
        if (pszText[i] == ch && i > 0 && (i + 1) < cbSize)
        {
            bFound = 1; // True;
            break;
        }
    }

    return bFound;
}

/**
 * @fn _IsaSeparator
 *
 * @param ch
 *
 * @return
 */
static char _IsaSeparator(char ch)
{
    // 0x22 => "
    // 0x5C =>
    // 0x3F => ?
    if (ch == '('  || ch == ')'  || ch == '<'  || ch == '>'  || ch == '@'  ||
        ch == ','  || ch == ';'  || ch == ':'  || ch == 0x5C || ch == 0x22 ||
        ch == '/'  || ch == '['  || ch == ']'  || ch == 0x3F || ch == '='  ||
        ch == '{'  || ch == '}'  || ch == SP   || ch == HT)
    {
        return 1;   // True
    }

    return 0;       // False
}

/**
 * @fn _SkipSeparators
 *
 * @param pszText
 * @param cbSize
 *
 * @return
 */
static int _SkipSeparators(char *pszText, int cbSize)
{
    int i = 0;

    if (pszText == NULL || cbSize == 0)
    {
        return 0;
    }

    while (_IsaSeparator(pszText[i]) == 1 || pszText[i] == CR || pszText[i] == LF)
    {
        i++;
    }

    return i;
}

/**
 * @fn _GetNextToken
 *
 * @param pszText
 * @param cbSize
 * @param pLen
 *
 * @return
 */
static char *_GetNextToken(char *pszText, int cbSize, int *pLen)
{
    int i = 0;
    char *p = NULL;

    int n = _SkipSeparators(&pszText[0], cbSize);            // Skip SP/HT/CR/LF/.... characters

    if (pszText == NULL || cbSize <= 0 || pLen == NULL)
    {
        return NULL;
    }

    p = &pszText[n];

    i = 0;
    while (i < cbSize)
    {
        if (_IsaSeparator(pszText[n + i]) == 1)
        {
            break;
        }
        i++;
    }

    *pLen = i;
    return p;
}

#if 0
/**
 * @fn _TrimLastCRLF
 *
 * @param pData
 */
void _TrimLastCRLF(char *pData)
{
    int len = 0;

    if (pData == NULL) return ;

    len = strlen(pData);
    if (len <= 0) return ;

    if (pData[len - 2] == CR && pData[len - 1] == LF)
    {
        pData[len - 2] = 0x00;
    }
    else if ((pData[len - 1] == LF) || pData[len - 1] == CR)
    {
        pData[len - 1] = 0x00;
    }
}
#endif

/**
 * @fn DK_GetHTTPResponseStatus
 *
 * @param pszResponse
 * @param pHttpStatus
 * @param pErrorCode
 *
 * @return
 */
int DK_GetHTTPResponseStatus(char *pszResponse, DKHTTPStatus *pHttpStatus, int *pErrorCode)
{
    // Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
    // The first digit of the Status-Code defines the class of response. The last two digits do not have any categorization role.
    // There are 5 values for the first digit:
    // - 1xx: Informational - Request received, continuing process
    // - 2xx: Success - The action was successfully received, understood, and accepted
    // - 3xx: Redirection - Further action must be taken in order to complete the request
    // - 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
    // - 5xx: Server Error - The server failed to fulfill an apparently valid request
    // extension-code = 3DIGIT
    //
    // HTTP-Version = "HTTP" "/" 1*DIGIT "." 1*DIGIT
    //
    // Reason-Phrase  = *<TEXT, excluding CR, LF>
    //
    // In the interest of robustness, servers SHOULD ignore any empty line(s) received where a Request-Line is expected.
    // In other words, if the server is reading the protocol stream at the beginning of a message and receives a CRLF first,
    // it should ignore the CRLF. Certain buggy HTTP/1.0 client implementations generate extra CRLF's after a POST request.
    // To restate what is explicitly forbidden by the BNF, an HTTP/1.1 client MUST NOT preface or follow a request with an extra CRLF.
    //

    char ch = 0;
    int nCRLF = 0;
    int i = 0;
    int j = 0;
    int dotcount = 0;
    int nTokenIndex = 0;
    int nTokenLen = 0;
    int len = strlen(pszResponse);
    char EOL = 0;
    char szBuffer[128] = {0};           // Use for string conversion, for example sprintf,...

    // 'HTTP/1.1 200 OK[LF]' (At least 16 bytes)
    // David changed. To make sure the length of pszResponse is long enough to store 'HTTP/1.1 200 OK'
    if (pHttpStatus == NULL ||
        pErrorCode == NULL ||
        len < 16)
    {
        return 0;
    }

    *pErrorCode = 0;                    // SUCCESS

    while (i < len && EOL == 0)
    {
        ch = pszResponse[i];
        nCRLF = _IsCRLF(&pszResponse[i]);

        if (nCRLF > 0 || ch == 0x20)
        {
            switch(nTokenIndex)
            {
            case 0:
                if (nTokenLen > 0)
                {
                    _AllocDKSTR(&pHttpStatus->Version, &pszResponse[i - nTokenLen], nTokenLen);

                    nTokenLen = 0;
                    nTokenIndex++;
                }
                break;
            case 1:
                if (nTokenLen < sizeof(szBuffer))
                {
                    // Check Status-Code format
                    memcpy(szBuffer, &pszResponse[i - nTokenLen], nTokenLen);

                    j = 0;
                    while (j < strlen(szBuffer))
                    {
                        if (isdigit((int)szBuffer[j]) == 0)
                        {
                            *pErrorCode = -2;
                            break;
                        }

                        j++;
                    }

                    if (j != 3)
                    {
                        *pErrorCode = -2;
                    }

                    pHttpStatus->nStatusCode = atoi(szBuffer);
                    nTokenLen = 0;
                    nTokenIndex++;
                }
                break;
            case 2:
                if (nTokenLen > 0)
                {
                    _AllocDKSTR(&pHttpStatus->ReasonPhrase, &pszResponse[i - nTokenLen], nTokenLen);

                    nTokenLen = 0;
                    nTokenIndex++;
                }
                break;
            }

            if (nCRLF > 0)
            {
                i += nCRLF;
                EOL = 1;
            }
            else
            {
                i++;
            }
        }
        else
        {
            nTokenLen++;
            i++;
        }
    }

    // Check Format
    // Must start with "HTTP/"
    if (strncmp(pHttpStatus->Version.pszValue, STATUS_LINE_HTTP_VERSION_PREFIX, strlen(STATUS_LINE_HTTP_VERSION_PREFIX)) != 0)
    {
        *pErrorCode = -1;
    }

    // 1*DIGIT "." 1*DIGIT
    for (j = strlen(STATUS_LINE_HTTP_VERSION_PREFIX); j < strlen(pHttpStatus->Version.pszValue); j++)
    {
        if (isdigit((int)pHttpStatus->Version.pszValue[j]) == 0)
        {
            if ((j == strlen(STATUS_LINE_HTTP_VERSION_PREFIX) ||
                (j == strlen(pHttpStatus->Version.pszValue) - 1)))
            {
                *pErrorCode = -1;
                break;
            }

            if (pHttpStatus->Version.pszValue[j] != '.')
            {
                *pErrorCode = -1;
                break;
            }

            dotcount++;

            if (dotcount > 1)
            {
                *pErrorCode = -1;
                break;
            }
        }
    }

    if (pErrorCode && *pErrorCode < 0)
    {
        i = 0;
    }

    return i;
}

/**
 * @fn DK_FreeStatus
 *
 * @param pHttpStatus
 */
void DK_FreeStatus(DKHTTPStatus *pHttpStatus)
{
    if (pHttpStatus)
    {
        _FreeDKSTR(&pHttpStatus->Version);

        _FreeDKSTR(&pHttpStatus->ReasonPhrase);
    }
}

/**
 * @fn DK_GetHTTPResponseHeader
 *
 * @param pszResponse
 * @param pHttpHeader
 * @param pErrorCode
 *
 * @return
 */
int DK_GetHTTPResponseHeader(char *pszResponse, DKHTTPHeader *pHttpHeader, int *pErrorCode)
{
    //
    // message-header = field-name ":" [ field-value ]
    // field-name = token
    // field-value = *( field-content | LWS )
    // field-content = <the OCTETs making up the field-value and consisting of either *TEXT or combinations
    //                 of token, separators, and quoted-string>
    //
    // The field value MA Y be preceded by any amount of LWS, though a single SP is preferred.
    // Header fields can be extended over multiple lines by preceding each extra line with at least one SP or HT
    //
    // Both the followings are legal
    // #1 => "Content-Type: application/json; charset=utf-8\n"
    // #2 => "Content-Type: application/json; \n"
    //       " charset=utf-8\n"
    // #3 => "Content-Type: application/json; \n"
    //       "      charset=utf-8\n"

    int i = 0;
    int nLWS = 0;
    int nTokenIndex = 0;
    int nTokenLen = 0;
    char ch = 0;
    int len = strlen(pszResponse);

    while (i < len)
    {
        // Check end of Message-Header
        nLWS = _doesEndofHeader(&pszResponse[i]);
        if (nLWS > 0)
        {
            // In case of "Access-Control-Allow-Origin: *\n" and followed by
            //     "\n"
            if (nTokenIndex == 1 && nTokenLen > 0)
            {
                _AllocDKSTR(&pHttpHeader->FieldValue, &pszResponse[i - nTokenLen], nTokenLen);
                nTokenLen = 0;
                nTokenIndex++;
            }

            // DO NOT i += nLWS.
            // We have to stop reading before the second CRLF characters
            i += _doesEndofHeaderField(&pszResponse[i]);
            break;
        }

        // Check end of (field-name ":" field-value) pair.
        nLWS = _doesEndofHeaderField(&pszResponse[i]);
        if (nLWS > 0)
        {
            // In case of "Access-Control-Allow-Origin: *\n" and followed by
            //     "Access-Control-Request-Method: *\n"
            if (nTokenIndex == 1 && nTokenLen > 0)
            {
                _AllocDKSTR(&pHttpHeader->FieldValue, &pszResponse[i - nTokenLen], nTokenLen);
                nTokenLen = 0;
                nTokenIndex++;
            }

            // Field-Name
            if (nTokenIndex == 0 && nTokenLen > 0)
            {
                // In case of "Access-Control-Allow-Origin" and followed by
                //     "\r\n: *\n"
                // nTokenLen > 0 and CRLF/SP/HT found ==> illegal

                if (pErrorCode)
                {
                    *pErrorCode = -1;
                }
            }

            i += nLWS;
            break;
        }

        ch = pszResponse[i];
        nLWS = _IsLWS(&pszResponse[i]);

        if (nLWS == 0 && ch != ':')
        {
            nTokenLen++;
            i++;
            continue;
        }

        if (ch == ':')
        {
            if (nTokenIndex == 0)
            {
                // Field-Value will start from next byte.
                if (nTokenLen > 0)
                {
                    _AllocDKSTR(&pHttpHeader->FieldName, &pszResponse[i - nTokenLen], nTokenLen);
                    nTokenLen = 0;
                    nTokenIndex++;
                    i++;

                    // Skip any LWS characters between ':' and Field-Value.
                    // In this case, LWS characters ARE NOT data.
                    nLWS = _IsLWS(&pszResponse[i]);
                    i += nLWS;

                    continue;
                }
            }

            nTokenLen++;
            i++;
            continue;
        }

        // LWS characters found.
        // The following lines are to handle LWS characters

        if (nTokenIndex == 0 && nTokenLen == 0)
        {
            // In case of
            //     "\r\nAccess-Control-Allow-Origin: *\n" or
            //     " \tAccess-Control-Allow-Origin: *\n"
            // Any LWS characters before Field-Name are illegal
            i += nLWS;
            break;
        }

        // Field-Name
        if (nTokenIndex == 0)
        {
            // In case of "Access-Control-Allow-Origin" and followed by
            //     " : *\n"     or
            //     "\t: *\n"    or
            // nTokenLen > 0 and CRLF/SP/HT found ==> illegal
            i += nLWS;

            if (pErrorCode)
            {
                *pErrorCode = -1;
            }
            break;
        }

        // Field-Value
        if (nTokenIndex == 1)
        {
            // In case of "Content-Type: application/json;\n" and followed by
            //     " charset=utf-8\n"
            // In ths case, LWS between "json;" and "charset" ARE data.

            if (nLWS > 0)
            {
                nTokenLen += nLWS;
            }
            else
            {
                nTokenLen++;
            }

            i += nLWS;
        }
    }

    if (pErrorCode && *pErrorCode < 0)
    {
        i = 0;
    }

    return i;
}

/**
 * @fn DK_GetHTTPHeaderContentType
 *
 * @param pszResponse
 * @param pStart
 * @param pEnd
 *
 * @return Number of characters scanned. 0 if error occurred.
 */
int DK_GetHTTPHeaderContentType(char *pszResponse, int *pStart, int *pEnd)
{
    // HTTP uses Internet Media Types [17] in the Content-Type (section 14.17) and Accept (section 14.1) header
    // fields in order to provide open and extensible data typing and type negotiation.
    //       media-type     = type "/" subtype *( ";" parameter )
    //       type           = token
    //       subtype        = token

    int i = 0;
    int nLWS = 0;
    int nTokenLen = 0;
    int len = strlen(pszResponse);

    if (pStart == NULL || pEnd == NULL)
    {
        return 0;
    }

    while (i < len)
    {
        nLWS = _IsLWS(&pszResponse[i]);
        if (nTokenLen == 0 && nLWS > 0)                     // To skip first LWS characters
        {
            i += nLWS;
            continue;
        }

        if (nTokenLen == 0 && pszResponse[i] == ';')        // To skip first ';' character
        {
            i++;
            continue;
        }

        if (pszResponse[i] == ';')
        {
            *pStart = i - nTokenLen;
            *pEnd = i;                                      // To skip ';' character
            break;
        }

        i++;
        nTokenLen++;
    }

    if (nTokenLen == 0)
    {
        i = 0;
    }

    if (i >= len)
    {
        *pStart = i - nTokenLen;
        *pEnd = i;
    }

    return i;
}

/**
 * @fn DK_FreeHeader
 *
 * @param pHttpHeader
 */
void DK_FreeHeader(DKHTTPHeader *pHttpHeader)
{
    if (pHttpHeader)
    {
        _FreeDKSTR(&pHttpHeader->FieldName);
        _FreeDKSTR(&pHttpHeader->FieldValue);
    }
}

/**
 * @fn DK_GetHTTPResponseMessageBody
 *
 * @param pszResponse
 * @param pHttpResponse
 * @param pErrorCode
 *
 * @return
 */
char *DK_GetHTTPResponseMessageBody(char *pszResponse, DKHTTPResponse *pHttpResponse, int *pErrorCode)
{
    int index = 0;
    int ret = 0;
    int nErrorCode = 0;
    char *pszMBody = NULL;
    char *p = 0;
    DKHTTPStatus StatusCode = {{0}, 0, {0}};
    DKHTTPHeader HttpHeader = {{0}, {0}};
    int nFoundMessageBody = 0;

    ret = DK_GetHTTPResponseStatus(pszResponse, &StatusCode, &nErrorCode);
    index += ret;

    // Dump StatusCode
    //WPRINT_APP_INFO ( ("length: %d\n", strlen(pszResponse)) );
    //WPRINT_APP_INFO ( ("DK_GetHTTPResponseStatus() = %d, ErrorCode: %d\n", ret, nErrorCode) );
    //WPRINT_APP_INFO ( ("\tCode: %d\r\n\tReason-Phrase: %s\r\n\tVersion: %s\r\n\n", StatusCode.nStatusCode, StatusCode.ReasonPhrase.pszValue, StatusCode.Version.pszValue) );

    if (pHttpResponse)
    {
        pHttpResponse->nStatusCode = StatusCode.nStatusCode;
        if (StatusCode.Version.cbSize)
        {
            //WPRINT_APP_INFO( ("Line %d, pHttpResponse->szHTTPVersion = 0x%X\n", __LINE__, (int)pHttpResponse->szHTTPVersion) );
            _AllocAndCopyTo(&pHttpResponse->szHTTPVersion, StatusCode.Version.pszValue, StatusCode.Version.cbSize);
            //WPRINT_APP_INFO( ("Line %d, pHttpResponse->szHTTPVersion = 0x%X\n", __LINE__, (int)pHttpResponse->szHTTPVersion) );
        }
    }

    DK_FreeStatus(&StatusCode);

    if (nErrorCode < 0 && ret == 0)
    {
        // Status-Line not found, try to find Resposne-Header
        index = 0;
    }

    nErrorCode = 0;

    while (nFoundMessageBody == 0 && nErrorCode == 0)
    {
        ret = DK_GetHTTPResponseHeader(&pszResponse[index], &HttpHeader, &nErrorCode);

        if (nErrorCode == 0 && HttpHeader.FieldName.cbSize == 0)
        {
            //WPRINT_APP_INFO(("Line %d\n", __LINE__));

            // Message-Body found
            nFoundMessageBody = 1;
        }

        //WPRINT_APP_INFO( ("nErrorCode = %d, index = %d ret = %d\n", nErrorCode, index, ret) )

        /*
         * In case of "\r\n\t...",         (ret = 3)
         *            "\r\n ....",         (ret = 3)
         *            "\r\n\n...",         (ret = 3)
         *            "\r\n\r...",         (ret = 3)
         *            "\r\n\r\n..."        (ret = 4)
         *
         *     The first "\r\n" indicated the ending of HTTP Message Header.
         *     All the other are Message-BODY, CANNOT be skip.
         */
        if (nFoundMessageBody == 1)
        {
            index += _IsCRLF(&pszResponse[index]);
        }
        else
        {
            index += ret;
        }

        if (nFoundMessageBody == 0)
        {
            // Dump for Debug
            //WPRINT_APP_INFO ( ("\tField-Name: %s\n", HttpHeader.FieldName.pszValue) );
            //WPRINT_APP_INFO ( ("\tField-Value: %s\n",  HttpHeader.FieldValue.pszValue) );

            // Content-Type
            if (stricmp(HttpHeader.FieldName.pszValue, "Content-Type") == 0)
            {
                int w = 0;
                int x = 0;
                int y = 0;
                int z = 1;

                while (z > 0)
                {
                    z = DK_GetHTTPHeaderContentType(&HttpHeader.FieldValue.pszValue[w], &x, &y);

                    if (z > 0)
                    {
                        // Field-Content start from &HttpHeader.FieldValue.pszValue[w + x], with length = y - x
                        // If you have allocated memory buffer, you can copy to it. For example,
                        // memcpy(szBuffer, &HttpHeader.FieldValue.pszValue[w + x], y - x);

                        if (pHttpResponse)
                        {
                            if (_IsCharInStr(&HttpHeader.FieldValue.pszValue[w + x], y - x, '/') == 1)
                            {
                                // Media-Type
                                _AllocAndCopyTo(&pHttpResponse->szContentType, &HttpHeader.FieldValue.pszValue[w + x], y - x);
                            }
                            else if (_IsCharInStr(&HttpHeader.FieldValue.pszValue[w + x], y - x, '=') == 1)
                            {
                                p = _GetNextToken(&HttpHeader.FieldValue.pszValue[w + x], y - x, &ret);

                                if (strnicmp(p, "charset", ret) == 0)
                                {
                                    // Charset
                                    p += ret;
                                    p = _GetNextToken(p, strlen(p), &ret);
                                    if (p)
                                    {
                                        _AllocAndCopyTo(&pHttpResponse->szCharset, p, ret);
                                    }
                                }
                            }
                        }

                        w += y;
                        //WPRINT_APP_INFO ( ("\t             %s\n",  szBuffer) );
                    }
                }
            }
            else if (stricmp(HttpHeader.FieldName.pszValue, "Content-Length") == 0)
            {
                if (pHttpResponse)
                {
                    pHttpResponse->nContentLength = atoi(HttpHeader.FieldValue.pszValue);
                }
            }
            else if (stricmp(HttpHeader.FieldName.pszValue, "Date") == 0)
            {
                if (pHttpResponse)
                {
                    _AllocAndCopyTo(&pHttpResponse->szDate, HttpHeader.FieldValue.pszValue, strlen((char *)HttpHeader.FieldValue.pszValue));
                }
            }
            else if (stricmp(HttpHeader.FieldName.pszValue, "SessionTTL") == 0)
            {
                if (pHttpResponse)
                {
                    pHttpResponse->nSessionTTL = strtoul(HttpHeader.FieldValue.pszValue, NULL, 10);
                }
            }
            else if (stricmp(HttpHeader.FieldName.pszValue, "Content-Range") == 0)
            {
                if (pHttpResponse)
                {
                    pHttpResponse->nFirstBytePosition = 0;
                    pHttpResponse->nLastBytePosition = 0;
                    pHttpResponse->nFileSize = 0;

                    int x = 0;
                    int y = strlen((char *)HttpHeader.FieldValue.pszValue);
                    p = _GetNextToken(&HttpHeader.FieldValue.pszValue[x], y - x, &ret);

                    if (strnicmp(p, "bytes", ret) == 0)
                    {
                        p += ret;
                        if (sscanf(p, "%d-%d/%d", &pHttpResponse->nFirstBytePosition, &pHttpResponse->nLastBytePosition, &pHttpResponse->nFileSize) == 3)
                        {
                            // Parse "Content-Range" success, show the result
                            //G_SYS_DBG_LOG_V("Start byte: %d, Last byte: %d, FileSize: %d\n", pHttpResponse->nFirstBytePosition, pHttpResponse->nLastBytePosition, pHttpResponse->nFileSize);
                        }
                    }
                }
            }
        }

        DK_FreeHeader(&HttpHeader);
    }

    //WPRINT_APP_INFO( ("Line %d, nErrorCode = %d, nFoundMessageBody = %d\n", __LINE__, nErrorCode, nFoundMessageBody) );
    if (nFoundMessageBody == 1)
    {
        pszMBody = &pszResponse[index];
        //_TrimLastCRLF(pszMBody);          // CANNOT do it. The last characters may be CRLF when HTTP Message-Body is in binary mode.
    }
    else
    {
        if (pErrorCode)
        {
            *pErrorCode = nErrorCode;
        }
    }

//    G_SYS_DBG_LOG_V("Message-Body first 6 bytes: %02X %02X %02X %02X %02X %02X\n",
//                        (pszMBody[0] & 0x0FF), (pszMBody[1] & 0x0FF), (pszMBody[2] & 0x0FF),
//                        (pszMBody[3] & 0x0FF), (pszMBody[4] & 0x0FF), (pszMBody[5] & 0x0FF));

    return pszMBody;
}

/**
 * @fn DK_FreeHTTPResponse
 *
 * @param pHttpResponse
 */
void DK_FreeHTTPResponse(DKHTTPResponse *pHttpResponse)
{
    if (pHttpResponse)
    {
        if (pHttpResponse->szHTTPVersion)
        {
            dxMemFree(pHttpResponse->szHTTPVersion);
            pHttpResponse->szHTTPVersion = NULL;
        }

        if (pHttpResponse->szContentType)
        {
            dxMemFree(pHttpResponse->szContentType);
            pHttpResponse->szContentType = NULL;
        }

        if (pHttpResponse->szCharset)
        {
            dxMemFree(pHttpResponse->szCharset);
            pHttpResponse->szCharset = NULL;
        }

        if (pHttpResponse->szDate)
        {
            dxMemFree(pHttpResponse->szDate);
            pHttpResponse->szDate = NULL;
        }

        memset(pHttpResponse, 0x00, sizeof(DKHTTPResponse));
    }
}

/**
 * @fn DK_DNS_Lookup
 */
dxOS_RET_CODE DK_DNS_Lookup()
{
    dxNET_RET_CODE result = DXOS_SUCCESS;
    dxIP_Addr_t *hostIP = ObjectDictionary_GetHostIp ();
    dxIP_Addr_t ip_address;
    uint32_t n = 0;

    if (hostIP != NULL)
    {
        memcpy(&n, hostIP->v4, sizeof(n));
    }

    if (hostIP->version == DXIP_VERSION_V4 &&
        n > 0x00000000)
    {
        // Use hostIP
        memcpy(&ip_address, hostIP, sizeof(ip_address));
    }
    else
    {
        dxIPv4_t dxip;
        uint8_t *pHostName = (uint8_t*) ObjectDictionary_GetHostname ();

        result = dxDNS_GetHostByName(pHostName, &dxip);

        if (result == DXNET_SUCCESS)
        {
            ip_address.version = DXIP_VERSION_V4;
            /*ip_address.ip.v4[0] = dxip.ip_addr[0];
            ip_address.ip.v4[1] = dxip.ip_addr[1];
            ip_address.ip.v4[2] = dxip.ip_addr[2];
            ip_address.ip.v4[3] = dxip.ip_addr[3];*/

            memcpy(&ip_address.v4[0], &dxip.ip_addr[0], sizeof(ip_address.v4));

            ObjectDictionary_SetHostIp (ip_address);

            G_SYS_DBG_LOG_V("Server is at %u.%u.%u.%u\n", (uint8_t)ip_address.v4[0],
                                                          (uint8_t)ip_address.v4[1],
                                                          (uint8_t)ip_address.v4[2],
                                                          (uint8_t)ip_address.v4[3]);
        }
        else {
            return DXNET_ERROR;
        }
    }

    return result;
}
