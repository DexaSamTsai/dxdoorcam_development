//============================================================================
// File: DKAPI_UpdatePeripheralDataPayloadToHistory.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_UpdatePeripheralDataPayloadToHistory_Response()
//            - Build_UpdatePeripheralDataPayloadToHistory_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_UpdatePeripheralDataPayloadToHistory_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_UpdatePeripheralDataPayloadToHistory.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_UPDATEPERIPHERALDATAPAYLOADHISTORY_API                                           \
                                                    "%s "                                               \
                                                    "%s/UpdatePeripheralDataPayloadToHistory.php HTTP/1.1\r\n"


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/
int _CheckUpdateHistoryDataPayloadCommandLength(uint32_t objectid, uint16_t len, uint32_t time)
{
    int overheadlen = 47;                   // 47 => {"ObjectID":,"BLEDataPayload":"","TimeStamp":},
    overheadlen += ((len + 2) / 3 * 4);     // Base64 encode overhead
    overheadlen += snprintf(NULL, 0, "%u%u", (unsigned int)objectid, (unsigned int)time);

    return overheadlen;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_UpdatePeripheralDataPayloadToHistory_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    int index = 0;
    PeripheralDataPayloadInfo_t *pPeripheralData = NULL;
    uint8_t nNumOfPayloadAppend = 0;        // Number of {"ObjectID":<ObjectID>,"UpdateTo":<UpdateTo>,"BLEDataPayload":<BLEDataPayload>,"CreatedByUserObjID":<UserObjectID>,"AdvObjID":<AdvObjID>} appended to buffer


    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen < (sizeof (PeripheralDataPayloadInfo_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Total data length: %d\n", paramlen);
#endif
    sprintf((char *)pBuffer, DKMSG_TEMPLATE_UPDATEPERIPHERALDATAPAYLOADHISTORY_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    strcat((char *)pBuffer, "{\"Data\":[");

    while (i + sizeof(DKRESTCommMsg_t) < paramlen &&
           ret == DKSERVER_STATUS_SUCCESS)
    {
        pPeripheralData = (PeripheralDataPayloadInfo_t *)&pCommMsg->pData[i];

        if (pPeripheralData == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid pParameters\n", __FUNCTION__, __LINE__);
            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        if (pPeripheralData->ObjectID == 0)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                             __FUNCTION__,
                             __LINE__,
                             pPeripheralData->ObjectID);

            // Move to next data which providing by DKServerManager_UpdatePeripheralDataPayloadToHistory_Asynch()
            i += sizeof(PeripheralDataPayloadInfo_t);

            continue;
        }

        // Check remaining buffer
        n = _CheckUpdateHistoryDataPayloadCommandLength(pPeripheralData->ObjectID,
                                                        pPeripheralData->AdDataLen,
                                                        pPeripheralData->TimeStamp);    // Current PeripheralData JSON format data length
        n += 2;                                                                         // Additional "]}" characters
        n += strlen((char *)pBuffer);                                                   // JSON data which already in buffer
        if (n >= cbSize)
        {
            // pBuffer is not enough, so give up the rest data
            break;
        }

        if (nNumOfPayloadAppend > 0)
        {
            strcat((char *)pBuffer, ",");
        }

        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n],
                "{\"ObjectID\":%u,\"TimeStamp\":%u,\"BLEDataPayload\":\"",
                (unsigned int)pPeripheralData->ObjectID,
                (unsigned int)pPeripheralData->TimeStamp);

        n = strlen((char *)pBuffer);

//#if defined(ENABLE_BASE64)
        n = b64_ntop(pPeripheralData->AdData, pPeripheralData->AdDataLen, (char *)&pBuffer[n], cbSize - n);
//#endif

        strcat((char *)pBuffer, "\"");

        /**
         * According to document v0.142 specification, added two variables, AdvObjID and CreatedByUserobjID
         */
        if (pPeripheralData->CreatedByUserobjID > 0)
        {
            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n],
                    ",\"CreatedByUserObjID\":%u",
                    (unsigned int)pPeripheralData->CreatedByUserobjID);
        }

        if (pPeripheralData->AdvObjID > 0)
        {
            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n],
                    ",\"AdvObjID\":%u",
                    (unsigned int)pPeripheralData->AdvObjID);
        }

        strcat((char *)pBuffer, "}");

        i += sizeof(PeripheralDataPayloadInfo_t);
        index++;

        nNumOfPayloadAppend++;
    }

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    if (nNumOfPayloadAppend == 0)
    {
        // No data, return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    strcat((char *)pBuffer, "]}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_UpdatePeripheralDataPayloadToHistory_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t bleid = 0;                 // An variable to store the BLEObjectID which convert from strtoul()
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;

    int periphpayloadnode = 0;          // index of peripheral data payload result
    int periphpayloadcount;
    int childnode = 0;                  // child of peripheralnode

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList4[] = {DKJSN_TOKEN_BLEOBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen < (sizeof (PeripheralDataPayloadInfo_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    periphpayloadnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (periphpayloadnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    periphpayloadcount = DKJSN_GetObjectCount(handle, periphpayloadnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of results found: %d\n", periphpayloadcount);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < periphpayloadcount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, periphpayloadnode, i);
        if (childnode < 0)
        {
            // Can't select the result node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get BLEObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList4[0]);
            DKJSN_Uninit(handle);
            return ret;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d BLEObjectID: %s\n", i, szBuffer);
#endif
        bleid = strtoul(szBuffer, NULL, 10);
        if (bleid == 0)
        {
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d BLEObjectID: %u\n", i, (unsigned int)bleid);
#endif
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
