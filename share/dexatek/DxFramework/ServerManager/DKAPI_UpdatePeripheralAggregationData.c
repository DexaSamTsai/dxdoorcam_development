//============================================================================
// File: DKAPI_UpdatePeripheralAggregationData.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.175.0
//     Date: 2015/09/07
//     1) Description: Support update motion sensor aggregation data
//        Modified:
//            - Build_UpdatePeripheralAggregationData_Data()
//
//     Version: 0.175.1
//     Date: 2015/09/15
//     1) Description: Fixed an issue with incorrect time calculating
//        Modified:
//            - Function Build_UpdatePeripheralAggregationData_Data()
//
//     Version: 0.175.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.175.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_UpdatePeripheralAggregationData_Response()
//            - Build_UpdatePeripheralAggregationData_Data()
//
//     Version: 0.175.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_UpdatePeripheralAggregationData_Data()
//
//     Version: 0.175.5
//     Date: 2017/01/04
//     1) Description: Incorrect output value while using sprintf() to format floating value 0 (SHFW-51)
//        Modified:
//            - Build_UpdatePeripheralAggregationData_Data()
//============================================================================

#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_UpdatePeripheralAggregationData.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_UPDATEPERIPHERALAGGREGATIONDATA_API                                              \
                                                    "%s "                                               \
                                                    "%s/UpdatePeripheralAggregationData.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_UpdatePeripheralAggregationData_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    uint32_t BLEObjectID = 0;               // Peripheral's ObjectID stored in global variable
    int i = 0;
    uint32_t TimeValue = 0;                 // To store a UNIX time value

    AggregateDataHeader_t *pAgHeader = NULL;
    AggregateHeaderFormat_t *pFormat = NULL;// DATA_HEADER
    uint32_t AgItemSize = 0;                // Size of AggregateItem_t or AggregateItemAgD1_t

    uint8_t nNumOfPayloadAppend = 0;        // Number of {"DataID":<DataID>,"DataValue":<DataValue>,"Duration":<Duration>} appended to buffer

    UTCLocalTimeTable_t *UTCLocalTime = DKServerManager_GetUTCLocalTimeTable();     // The sturcture stored server's UTC time and corresponding local system tick

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen < (sizeof (AggregateDataHeader_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    pAgHeader = (AggregateDataHeader_t *)pCommMsg->pData;

    pFormat = (AggregateHeaderFormat_t *)&pAgHeader->DataHeader;
    if (pFormat->AgDFormat == 1)
    {
        AgItemSize = sizeof(AggregateItemAgD1_t);
    }
    else
    {
        AgItemSize = sizeof(AggregateItem_t);
    }

    if (paramlen < sizeof(AggregateDataHeader_t) + AgItemSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Illegal aggregation data format\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Converting MAC address to decimal string format
    i = _MACAddrToDecimalStr(szBuffer, STRING_BUFFER_SIZE, (uint8_t *)pAgHeader->MAC, sizeof(pAgHeader->MAC));
    if (i <= 0)
    {
        DKHex_HexToASCII((char *)pAgHeader->MAC, sizeof(pAgHeader->MAC), szBuffer, STRING_BUFFER_SIZE);
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, PMAC address %s convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__,
                         szBuffer);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Lock ();
    BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) pAgHeader->MAC, sizeof(pAgHeader->MAC));
    ObjectDictionary_Unlock ();

    if (BLEObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         BLEObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Show Server Time (UTC + 00:00) and Local System Time
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Last Server time: %u, corresponding local: %u\n", (unsigned int)UTCLocalTime->UTCTime, (unsigned int)UTCLocalTime->LocalTime);

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Total data length: %d\n", paramlen);
#endif
    sprintf((char *)pBuffer, DKMSG_TEMPLATE_UPDATEPERIPHERALAGGREGATIONDATA_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

   strcat((char *)pBuffer, "{\"Peripherals\":[");

    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "{\"PMACAddress\":\"%s\",\"Data\":[", szBuffer);

    i = sizeof(AggregateDataHeader_t);

    while (i + sizeof(DKRESTCommMsg_t) < paramlen &&
           ret == DKSERVER_STATUS_SUCCESS)
    {
        AggregateItem_t *pAgData0 = (AggregateItem_t *)&pCommMsg->pData[i];
        AggregateItemAgD1_t *pAgData1 = (AggregateItemAgD1_t *)&pCommMsg->pData[i];

        if (pAgData0 == NULL ||
            pAgData1 == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid pParameters\n",
                             __FUNCTION__,
                             __LINE__);
            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        if (pFormat->AgDFormat == 1)
        {
            if (pFormat->TimeFormat == 1)
            {
                TimeValue = pAgData1->nTime;
            }
            else if (pFormat->TimeFormat == 0)
            {
                dxTime_t lt = dxTimeGetMS();

                // Convert pAgData1->nTime to UNIX timestamp by using UTCLocalTimeTable to calibrate
                TimeValue = UTCLocalTime->UTCTime + (lt / 1000 - UTCLocalTime->LocalTime) - pAgData1->nTime;

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "Current Tick: %u, pAgData1->nTime: %u, TimeValue: %u\n", lt / 1000, pAgData1->nTime, TimeValue);
            }
            else
            {
                // Incorrect time value. skip the record
                i += sizeof(AggregateItemAgD1_t);
                continue;
            }

            if (nNumOfPayloadAppend > 0)
            {
                strcat((char *)pBuffer, ",");
            }

            strcat((char *)pBuffer, "{");

            if (pAgData1->nTime > 0)
            {
                n = strlen((char *)pBuffer);
                sprintf((char *)&pBuffer[n], "\"Time\":%u,", (unsigned int)TimeValue);
            }

            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n], "\"DataID\":%u,\"DataValue\":", (unsigned int)pAgData1->nDataID);

            n = strlen((char *)pBuffer);
            switch(pAgData1->nValueType)
            {
            case 1: // INT8
            {
                int8_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (int)nDataValue);
                break;
            }
            case 2: // UINT8
            {
                uint8_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (unsigned int)nDataValue);
                break;
            }
            case 3: // INT16
            {
                int16_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (int)nDataValue);
                break;
            }
            case 4: // UINT16
            {
                uint16_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (unsigned int)nDataValue);
                break;
            }
            case 5: // INT32
            {
                int32_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (int)nDataValue);
                break;
            }
            case 6: // UINT32
            {
                uint32_t nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                sprintf((char *)&pBuffer[n], "%d", (unsigned int)nDataValue);
                break;
            }
            case 7: // FLOAT
            {
                float nDataValue = 0;
                memcpy(&nDataValue, &pAgData1->nDataValue, sizeof(nDataValue));
                if (nDataValue == 0)        // Fixed issue of SHFW-51
                {
                    sprintf((char *)&pBuffer[n], "%d", (int)nDataValue);
                }
                else
                {
                    sprintf((char *)&pBuffer[n], "%f", nDataValue);
                }
                break;
            }
            default:
                // Incorrect time value. skip the record
                i += sizeof(AggregateItemAgD1_t);
                continue;
                break;
            }

            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n], ",\"Duration\":%u}", (unsigned int)pAgData1->nDuration);

            i += sizeof(AggregateItemAgD1_t);
        }
        else
        {
            float nDataValue = 0;

            memcpy(&nDataValue, &pAgData0->nDataValue, sizeof(nDataValue));

            if (nNumOfPayloadAppend > 0)
            {
                strcat((char *)pBuffer, ",");
            }

            n = strlen((char *)pBuffer);

            if (nDataValue == 0)            // Fixed issue of SHFW-51
            {
                sprintf((char *)&pBuffer[n], "{\"DataID\":%u,\"DataValue\":%d,\"Duration\":%u}", (unsigned int)pAgData0->nDataID, (int)nDataValue, (unsigned int)pAgData0->nDuration);
            }
            else
            {
                sprintf((char *)&pBuffer[n], "{\"DataID\":%u,\"DataValue\":%f,\"Duration\":%u}", (unsigned int)pAgData0->nDataID, nDataValue, (unsigned int)pAgData0->nDuration);
            }

            i += sizeof(AggregateItem_t);
        }

        nNumOfPayloadAppend++;
    }

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    if (nNumOfPayloadAppend == 0)
    {
        // No data, return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    strcat((char *)pBuffer, "]}]}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_UpdatePeripheralAggregationData_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t bleid = 0;                 // An variable to store the BLEObjectID which convert from strtoul()
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int rootnode = 0;                   // rootnode, must be 0
    int len = 0;                        // length of data
    int i = 0;

    int periphpayloadnode = 0;          // index of peripheral data payload result
    int periphpayloadcount;
    int childnode = 0;                  // child of peripheralnode

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList4[] = {DKJSN_TOKEN_BLEOBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen < (sizeof (AggregateDataHeader_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Results
    periphpayloadnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (periphpayloadnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    periphpayloadcount = DKJSN_GetObjectCount(handle, periphpayloadnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of results found: %d\n", periphpayloadcount);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < periphpayloadcount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, periphpayloadnode, i);
        if (childnode < 0)
        {
            // Can't select the result node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get BLEObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, JSON key %d not found\n",
                             __FUNCTION__,
                             __LINE__,
                             findList4[0]);
            DKJSN_Uninit(handle);
            return ret;
        }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d BLEObjectID: %s\n", i, szBuffer);
#endif
        bleid = strtoul(szBuffer, NULL, 10);
        if (bleid == 0)
        {
            continue;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "#%d BLEObjectID: %u\n", i, (unsigned int)bleid);
#endif
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
