//============================================================================
// File: dk_http.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DK_HTTP_H
#define _DK_HTTP_H

#pragma once

#include "dxOS.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef struct _tagDKSTR
{
    int     cbSize;                     // size of pszValue including NULL terminate character.
    char    *pszValue;
} DKSTR, *PDKSTR;

typedef struct _tagDKHTTPStatus         // HTTP Response Status
{
    DKSTR   Version;
    int     nStatusCode;
    DKSTR   ReasonPhrase;
} DKHTTPStatus;

typedef struct _tagDKHTTPHeader
{
    DKSTR FieldName;
    DKSTR FieldValue;
} DKHTTPHeader;

typedef struct _tagDKHTTPResponse
{
    char *szHTTPVersion;
    int nStatusCode;
    char *szContentType;
    char *szCharset;
    char *szDate;
    int nContentLength;
    int nFirstBytePosition;
    int nLastBytePosition;
    int nFileSize;
    unsigned int nSessionTTL;
} DKHTTPResponse;

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Definitions
 ******************************************************/
int DK_GetHTTPResponseStatus(char *pszResponse, DKHTTPStatus *pHttpStatus, int *pErrorCode);
void DK_FreeStatus(DKHTTPStatus *pHttpStatus);

int DK_GetHTTPResponseHeader(char *pszResponse, DKHTTPHeader *pHttpHeader, int *pErrorCode);
int DK_GetHTTPHeaderContentType(char *pszResponse, int *pStart, int *pEnd);
void DK_FreeHeader(DKHTTPHeader *pHttpHeader);

char *DK_GetHTTPResponseMessageBody(char *pszResponse, DKHTTPResponse *pHttpResponse, int *pErrorCode);
void DK_FreeHTTPResponse(DKHTTPResponse *pHttpResponse);

dxOS_RET_CODE DK_DNS_Lookup();

#ifdef __cplusplus
}
#endif

#endif // _DK_HTTP_H
