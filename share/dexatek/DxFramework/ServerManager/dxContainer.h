//============================================================================
// File: dxContainer.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/10/03
//     1) Description: Initial
//
//     Version: 0.234
//     Date: 2016/10/14
//     1) Description: Support copy whole container data or just those data with DirtyFlag = DXCONT_COPY_OPTION_DIRTYFLAG
//        Added:
//            - dxContCopyOption_t
//        Modified:
//            - dxContainer_Duplicate()
//     2) Description: Revise dxContainerM_t structure
//        Modified:
//            - dxContainerM_t
//            - dxContainerM_Create()
//============================================================================
#ifndef _DXCONTAINER_H
#define _DXCONTAINER_H

#pragma once

#include "dxOS.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {

    DXCONT_RET_SUCCESS = 0,
    DXCONT_RET_ERROR   = -1,
} dxCONT_RET_CODE;

typedef enum {
    DXCONTFORMAT_FULLSET = 0,
    DXCONTFORMAT_MINISET = 1
} dxContFormat_t;

typedef enum {
    DXCONT_COPY_OPTION_ALL = 0,
    DXCONT_COPY_OPTION_DIRTYFLAG = 1
} dxContCopyOption_t;

typedef struct VarLenType
{
    uint16_t    Length;
    uint8_t     *pData;
} VarLenType_t;

typedef struct dxContainerD
{
    uint64_t        ContDID;
    uint64_t        ContMID;
    VarLenType_t    *ContDName;
    uint32_t        ContDType;
    uint32_t        DateTime;
    VarLenType_t    *SValue;
    VarLenType_t    *LValue;
    uint16_t        ContLVCRC;

    struct dxContainerD *pNextContD;
} dxContainerD_t;

typedef struct dxContainerM
{
    uint64_t        ContMID;
    uint32_t        BLEObjectID;
    uint32_t        ContMType;
    VarLenType_t*   ContMName;
    uint8_t         UpdateTo;
    uint8_t         DirtyFlag;
    uint8_t         Reserved[2];

    dxContainerD_t      *pContDList;
    struct dxContainerM *pNextContM;
} dxContainerM_t;

typedef struct dxContainer
{
    dxContainerM_t  *pContMList;
} dxContainer_t;

/**
 * Example #1 (How to use dxContainer_VarLenType_Init()/dxContainer_VarLenType_Free(), dxContainer_UINT64_ToStr()/dxContainer_UINT64_Free())
   <pre>
    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());

    printf("\nLine %d, #1 Basic memory allocate/free verify\n", __LINE__);

    VarLenType_t *pLValue = dxContainer_VarLenType_Init("ABCDEFGHIJK", 11);

    uint32_t BLEObjectID = 123456;
    uint64_t ContDID = 18446744073709551613;
    uint64_t ContMID = 18446744073709551614;
    dxContainerD_t *pContD = dxContainerD_Create(ContDID,           // ContDID
                                                 ContMID,           // ContMID
                                                 "ContDName",       // ContDName
                                                 99999,             // ContDType
                                                 1475144087,        // DateTime
                                                 "SValue",          // SValue
                                                 pLValue,           // LValue
                                                 8284);             // ContLVCRC

    dxContainer_VarLenType_Free(pLValue);

    uint8_t *pContDID = dxContainer_UINT64_ToStr(pContD->ContDID);
    uint8_t *pContMID = dxContainer_UINT64_ToStr(pContD->ContMID);

    printf("Line %d, \n"
           "\tContDID: %s \n"
           "\tContMID: %s \n"
           "\tContDName: [%d]%s \n"
           "\tContDType: %d \n"
           "\tDateTime: %d \n"
           "\tSValue: [%d]%s \n"
           "\tLValue: [%d]%s \n"
           "\tContLVCRC: %d\n",
           __LINE__,
           pContDID,
           pContMID,
           (pContD->ContDName) ? pContD->ContDName->Length : 0, (pContD->ContDName) ? pContD->ContDName->pData : "",
           pContD->ContDType,
           pContD->DateTime,
           (pContD->SValue) ? pContD->SValue->Length : 0, (pContD->SValue) ? pContD->SValue->pData : "",
           (pContD->LValue) ? pContD->LValue->Length : 0, (pContD->LValue) ? pContD->LValue->pData : "",
           pContD->ContLVCRC);

    dxContainer_UINT64_Free(pContMID);
    dxContainer_UINT64_Free(pContDID);

    dxContainerD_Free(pContD);

    dxContainerM_t *pContM = dxContainerM_Create(ContMID,           // ContMID
                                                 BLEObjectID,       // BLEObjectID
                                                 4294967294,        // ContMType
                                                 "ContMName");      // ContMName

    pContMID = dxContainer_UINT64_ToStr(pContM->ContMID);

    printf("Line %d, \n"
           "\tContMID: %s \n"
           "\tBLEObjectID: %u \n"
           "\tContMType: %u \n"
           "\tContMName: [%d]%s\n",
           __LINE__,
           pContMID,
           pContM->BLEObjectID,
           pContM->ContMType,
           (pContM->ContMName) ? pContM->ContMName->Length : 0, (pContM->ContMName) ? pContM->ContMName->pData : "");

    dxContainer_UINT64_Free(pContMID);

    dxContainerM_Free(pContM);

    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());
   </pre>

 * Example #2 (How to use dxContainerD_Create()/dxContainerD_Free(), dxContainerM_Create()/dxContainerM_Free()/dxContainerM_Append(), dxContainer_Create()/dxContainer_Free()/dxContainer_Append())
   <pre>
    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());

    uint32_t BLEObjectID = 123456;

    // pContM1
    dxContainerM_t *pContM1 = dxContainerM_Create(11,               // ContMID
                                                  BLEObjectID,      // BLEObjectID
                                                  13,               // ContMType
                                                  "ContMName");     // ContMName

    // pContD1 of pContM1
    VarLenType_t *pLV1 = dxContainer_VarLenType_Init("ABCDEFGHIJK", 11);

    dxContainerD_t *pContD1 = dxContainerD_Create(101,              // ContDID
                                                  11,               // ContMID
                                                  "ContDName1",     // ContDName
                                                  99999,            // ContDType
                                                  1475144087,       // DateTime
                                                  "SValue1",        // SValue
                                                  pLV1,             // LValue
                                                  8284);            // ContLVCRC

    dxContainer_VarLenType_Free(pLV1);

    dxContainerM_Append(pContM1, pContD1);

    // pContD2 of pContM1
    VarLenType_t *pLV2 = dxContainer_VarLenType_Init("abcdefghijkl", 12);

    dxContainerD_t *pContD2 = dxContainerD_Create(102,              // ContDID
                                                  12,               // ContMID
                                                  "ContDName2",     // ContDName
                                                  8888,             // ContDType
                                                  1475144000,       // DateTime
                                                  "SValue2",        // SValue
                                                  pLV2,             // LValue
                                                  15564);           // ContLVCRC

    dxContainer_VarLenType_Free(pLV2);

    dxContainerM_Append(pContM1, pContD2);

    // pCont
    dxContainer_t *pCont = dxContainer_Create();

    dxContainer_Append(pCont, pContM1);

    // pContM2
    dxContainerM_t *pContM2 = dxContainerM_Create(21,               // ContMID
                                                  BLEObjectID,      // BLEObjectID
                                                  23,               // ContMType
                                                  "ContMName2");    // ContMName

    // pContD3 of pContM2
    VarLenType_t *pLV3 = dxContainer_VarLenType_Init("1234567890", 10);

    dxContainerD_t *pContD3 = dxContainerD_Create(103,              // ContDID
                                                  13,               // ContMID
                                                  "ContDName3",     // ContDName
                                                  99999,            // ContDType
                                                  1475144003,       // DateTime
                                                  "SValue3",        // SValue
                                                  pLV3,             // LValue
                                                  10347);           // ContLVCRC

    dxContainer_VarLenType_Free(pLV3);

    dxContainerM_Append(pContM2, pContD3);

    dxContainer_Append(pCont, pContM2);

    // pContM3
    dxContainerM_t *pContM3 = dxContainerM_Create(11,               // ContMID
                                                  BLEObjectID,      // BLEObjectID
                                                  33,               // ContMType
                                                  "ContMName3");    // ContMName

    // pContD4 of pContM3
    VarLenType_t *pLV4 = dxContainer_VarLenType_Init("!@#$%^&*()(*&^%$#@!", 19);

    dxContainerD_t *pContD4 = dxContainerD_Create(104,              // ContDID
                                                  14,               // ContMID
                                                  "ContDName4",     // ContDName
                                                  99999,            // ContDType
                                                  1475144004,       // DateTime
                                                  "SValue4",        // SValue
                                                  pLV4,             // LValue
                                                  30959);           // ContLVCRC

    dxContainer_VarLenType_Free(pLV4);

    dxContainerM_Append(pContM3, pContD4);

    // pContD5 of pContM3
    VarLenType_t *pLV5 = dxContainer_VarLenType_Init("QWERTYUIOPpoiuytrewq", 20);

    dxContainerD_t *pContD5 = dxContainerD_Create(105,              // ContDID
                                                  15,               // ContMID
                                                  "ContDName5",     // ContDName
                                                  8888,             // ContDType
                                                  1475144005,       // DateTime
                                                  "SValue5",        // SValue
                                                  pLV5,             // LValue
                                                  60335);           // ContLVCRC

    dxContainer_VarLenType_Free(pLV5);

    dxContainerM_Append(pContM3, pContD5);

    dxContainer_Append(pCont, pContM3);

    // pContD6 of pContM1
    VarLenType_t *pLV6 = dxContainer_VarLenType_Init("ZSXDCFtugijleoieiiaiaa", 22);

    dxContainerD_t *pContD6 = dxContainerD_Create(106,              // ContDID
                                                  16,               // ContMID
                                                  "ContDName6",     // ContDName
                                                  8888,             // ContDType
                                                  1475144006,       // DateTime
                                                  "SValue6",        // SValue
                                                  pLV6,             // LValue
                                                  31851);           // ContLVCRC

    dxContainer_VarLenType_Free(pLV6);

    dxContainerM_Append(pContM1, pContD6);

    // pContD7 of pContM1
    VarLenType_t *pLV7 = dxContainer_VarLenType_Init("plikjyhgtfrfgnmkuyq52781hj2j31", 30);

    dxContainerD_t *pContD7 = dxContainerD_Create(107,              // ContDID
                                                  17,               // ContMID
                                                  "ContDName7",     // ContDName
                                                  8888,             // ContDType
                                                  1475144007,       // DateTime
                                                  "SValue7",        // SValue
                                                  pLV7,             // LValue
                                                  1811);            // ContLVCRC

    dxContainer_VarLenType_Free(pLV7);

    dxContainerM_Append(pContM1, pContD7);

    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());
   </pre>

 * Example #3 (Container Object Traversal. Need Example #2)
   <pre>
    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());

    // Evaluate all data
    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

        printf("Line %d, \n"
               "\tContMID: %s \n"
               "\tBLEObjectID: %d\n"
               "\tContMType: %u \n"
               "\tContMName: [%d]%s\n",
               __LINE__,
               pContMID,
               p->BLEObjectID,
               p->ContMType,
               (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? p->ContMName->pData : "");

        dxContainer_UINT64_Free(pContMID);

        dxContainerD_t *q = p->pContDList;
        while (q)
        {
            uint8_t *pContDID = dxContainer_UINT64_ToStr(q->ContDID);
            uint8_t *pContMID = dxContainer_UINT64_ToStr(q->ContMID);

            printf("Line %d, \n"
                   "\tContDID: %s \n"
                   "\tContMID: %s \n"
                   "\tContDName: [%d]%s \n"
                   "\tContDType: %d \n"
                   "\tDateTime: %d \n"
                   "\tSValue: [%d]%s \n"
                   "\tLValue: [%d]%s \n"
                   "\tContLVCRC: %d\n",
                   __LINE__,
                   pContDID,
                   pContMID,
                   (q->ContDName) ? q->ContDName->Length : 0, (q->ContDName) ? q->ContDName->pData : "",
                   q->ContDType,
                   q->DateTime,
                   (q->SValue) ? q->SValue->Length : 0, (q->SValue) ? q->SValue->pData : "",
                   (q->LValue) ? q->LValue->Length : 0, (q->LValue) ? q->LValue->pData : "",
                   q->ContLVCRC);

            dxContainer_UINT64_Free(pContMID);
            dxContainer_UINT64_Free(pContDID);

            q = q->pNextContD;
        }

        printf("\n");
        p = p->pNextContM;
    }

    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());
   </pre>

 * Example #4 (Free Container Object. Need Example #2)
   <pre>
    dxContainer_Free(pCont);

    printf("Line %d, xPortGetFreeHeapSize(): %u\n", __LINE__, xPortGetFreeHeapSize());
   </pre>

 * Example #5 (How to get number of dxContainerM_t object? Need Example #2)
   <pre>
    uint32_t countOfContainerM = dxContainer_GetMDataCount(pCont);

    printf("Line %d, dxContainer_GetMDataCount(): %u\n", __LINE__, countOfContainerM);
   </pre>

 * Example #6 (How to get number of dxContainerD_t object? Need Example #2)
   <pre>
    uint32_t countOfContainerD1 = dxContainer_GetDDataCount(pContM1);
    uint32_t countOfContainerD2 = dxContainer_GetDDataCount(pContM2);
    uint32_t countOfContainerD3 = dxContainer_GetDDataCount(pContM3);

    printf("Line %d, dxContainer_GetDDataCount(): %u %u %u\n",
           __LINE__,
           countOfContainerD1,
           countOfContainerD2,
           countOfContainerD3);
   </pre>

 * Example #7 (How to use for..loop to retrieve container object? Need Example #2)
   <pre>
    uint32_t numOfContM = dxContainer_GetMDataCount(pCont);
    if (numOfContM > 0)
    {
        uint32_t i = 0;
        uint32_t j = 0;

        for (i = 0; i < numOfContM; i++)
        {
            dxContainerM_t *p = dxContainer_GetMDataByIndex(pCont, i);

            uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

            printf("Line %d, \n"
                   "\tContMID: %s \n"
                   "\tBLEObjectID: %u \n"
                   "\tContMType: %u \n"
                   "\tContMName: [%d]%s\n",
                   __LINE__,
                   pContMID,
                   p->BLEObjectID,
                   p->ContMType,
                   (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? p->ContMName->pData : "");

            dxContainer_UINT64_Free(pContMID);

            uint32_t numOfContD = dxContainer_GetDDataCount(p);

            for (j = 0; j < numOfContD; j++)
            {
                dxContainerD_t *q = dxContainer_GetDDataByIndex(p, j);

                uint8_t *pContDID = dxContainer_UINT64_ToStr(q->ContDID);
                uint8_t *pContMID = dxContainer_UINT64_ToStr(q->ContMID);

                printf("Line %d, \n"
                       "\tContDID: %s \n"
                       "\tContMID: %s \n"
                       "\tContDName: [%d]%s \n"
                       "\tContDType: %d \n"
                       "\tDateTime: %d \n"
                       "\tSValue: [%d]%s \n"
                       "\tLValue: [%d]%s \n"
                       "\tContLVCRC: %d\n",
                       __LINE__,
                       pContDID,
                       pContMID,
                       (q->ContDName) ? q->ContDName->Length : 0, (q->ContDName) ? q->ContDName->pData : "",
                       q->ContDType,
                       q->DateTime,
                       (q->SValue) ? q->SValue->Length : 0, (q->SValue) ? q->SValue->pData : "",
                       (q->LValue) ? q->LValue->Length : 0, (q->LValue) ? q->LValue->pData : "",
                       q->ContLVCRC);

                dxContainer_UINT64_Free(pContMID);
                dxContainer_UINT64_Free(pContDID);
            }

            printf("\n");
        }
    }
   </pre>

 * Example #8 (How to use dxContainer_FindMDataByType() to search a special ContMType of dxContainerM_t object? Need Example #2)
   <pre>
    dxContainerM_t *p = NULL;

    while (1)
    {
        p = dxContainer_FindMDataByType(pCont, p, 23);

        if (p == NULL)
        {
            break;
        }

        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

        printf("Line %d, \n"
               "\tContMID: %s \n"
               "\tBLEObjectID: %u \n"
               "\tContMType: %u \n"
               "\tContMName: [%d]%s\n",
               __LINE__,
               pContMID,
               p->BLEObjectID,
               p->ContMType,
               (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? p->ContMName->pData : "");

        dxContainer_UINT64_Free(pContMID);
    }
   </pre>

 * Example #9 (How to use dxContainer_FindDDataByType() to search a special ContDType of dxContainerD_t object? Need Example #2)
   <pre>
    // Evaluate all data
    p = pCont->pContMList;

    while (p)
    {
        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

        printf("Line %d, \n"
               "\tContMID: %s \n"
               "\tBLEObjectID: %u \n"
               "\tContMType: %u \n"
               "\tContMName: [%d]%s\n",
               __LINE__,
               pContMID,
               p->BLEObjectID,
               p->ContMType,
               (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? p->ContMName->pData : "");

        dxContainer_UINT64_Free(pContMID);

        dxContainerD_t *q = p->pContDList;
        while (q)
        {
            uint8_t *pContDID = dxContainer_UINT64_ToStr(q->ContDID);
            uint8_t *pContMID = dxContainer_UINT64_ToStr(q->ContMID);

            printf("Line %d, \n"
                   "\tContDID: %s \n"
                   "\tContMID: %s \n"
                   "\tContDName: [%d]%s \n"
                   "\tContDType: %d \n"
                   "\tDateTime: %d \n"
                   "\tSValue: [%d]%s \n"
                   "\tLValue: [%d]%s \n"
                   "\tContLVCRC: %d\n",
                   __LINE__,
                   pContDID,
                   pContMID,
                   (q->ContDName) ? q->ContDName->Length : 0, (q->ContDName) ? q->ContDName->pData : "",
                   q->ContDType,
                   q->DateTime,
                   (q->SValue) ? q->SValue->Length : 0, (q->SValue) ? q->SValue->pData : "",
                   (q->LValue) ? q->LValue->Length : 0, (q->LValue) ? q->LValue->pData : "",
                   q->ContLVCRC);

            dxContainer_UINT64_Free(pContMID);
            dxContainer_UINT64_Free(pContDID);

            q = q->pNextContD;
        }

        printf("\n");
        p = p->pNextContM;
    }
   </pre>

 * Example #10 (How to find a specific ContMID dxContainerM_t object? Need Example #2)
   <pre>
    p = dxContainer_FindMDataByID(pCont, 31);
    if (p)
    {
        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

        printf("Line %d, \n"
               "\tContMID: %s \n"
               "\tBLEObjectID: %u \n"
               "\tContMType: %u \n"
               "\tContMName: [%d]%s\n",
               __LINE__,
               pContMID,
               p->BLEObjectID,
               p->ContMType,
               (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? p->ContMName->pData : "");

        dxContainer_UINT64_Free(pContMID);
    }
   </pre>

 * Example #11 (How to find a specific ContDID dxContainerD_t object? Need Example #2)
   <pre>
    q = dxContainer_FindDDataByID(pCont, 105);
    if (q)
    {
        uint8_t *pContDID = dxContainer_UINT64_ToStr(q->ContDID);
        uint8_t *pContMID = dxContainer_UINT64_ToStr(q->ContMID);

        printf("Line %d, \n"
               "\tContDID: %s \n"
               "\tContMID: %s \n"
               "\tContDName: [%d]%s \n"
               "\tContDType: %d \n"
               "\tDateTime: %d \n"
               "\tSValue: [%d]%s \n"
               "\tLValue: [%d]%s \n"
               "\tContLVCRC: %d\n",
               __LINE__,
               pContDID,
               pContMID,
               (q->ContDName) ? q->ContDName->Length : 0, (q->ContDName) ? q->ContDName->pData : "",
               q->ContDType,
               q->DateTime,
               (q->SValue) ? q->SValue->Length : 0, (q->SValue) ? q->SValue->pData : "",
               (q->LValue) ? q->LValue->Length : 0, (q->LValue) ? q->LValue->pData : "",
               q->ContLVCRC);

        dxContainer_UINT64_Free(pContMID);
        dxContainer_UINT64_Free(pContDID);
    }
   </pre>

 * Example #12 (How to make a copy of dxContainer object? Need Examaple #2)
   <pre>
    dxContainer_t *pNewCont = dxContainer_Duplicate(pCont);

    // Do something...

    dxContainer_Free(pNewCont);
   </pre>
 */

//============================================================================
// Convert uint64_t value to a string for printing
//============================================================================
/**
 * @brief Convert a uint64_t type of value to a string for printing. It's due to
 *        printf() doesn't support printing uint64_t value.
 * @param[IN] v     A uint64_t type of value
 * @return A pointer to point to a string value of v, or NULL if error occurred.
 *         To free the allocated memory, use dxContainer_UINT64_Free(),
 *         dxContainer_CString_Free, or dxMemFree()
 */
uint8_t *dxContainer_UINT64_ToStr(uint64_t v);

/**
 * @brief Free a memory block. The function is the same as dxContainer_CString_Free()
 * @param[IN] pData     Previously allocated memory block to be freed
 */
#define dxContainer_UINT64_Free                 dxContainer_CString_Free

//============================================================================
// dxContainer Memory Allocate/Free Function
//============================================================================
/**
 * @brief Allocate a memory block and copy data into it.
 * @param[IN] pData     Data to be copied to
 * @param[IN] length    Length of pData
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         A null terminating character will be appened.
 *         To free the allocated memory, use dxContainer_CString_Free() or dxMemFree()
 */
uint8_t *dxContainer_CString_Init(uint8_t *pData, uint32_t length);

/**
 * @brief Free a memory block
 * @param[IN] pData     Previously allocated memory block to be freed
 */
void dxContainer_CString_Free(uint8_t *pData);

/**
 * @brief Allocate a VarLenType_t memory block and copy data into it.
 * @param[IN] pData     Data to be copied to
 * @param[IN] length    Length of pData
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         A null terminating character will be appened.
 *         To free the allocated memory, use dxContainer_VarLenType_Free()
 */
VarLenType_t *dxContainer_VarLenType_Init(uint8_t *pData, uint32_t length);

/**
 * @brief Free a memory block of VarLenType_t type
 * @param[IN] pData     Previously allocated memory block to be freed
 */
void dxContainer_VarLenType_Free(VarLenType_t *p);

//============================================================================
// dxContainer Access Function
//============================================================================
/**
 * @brief Allocate memory blocks to store dxContainerD_t data. For ContDName, SValue and LValue
 *        variables, it's caller's responsibility to free the memory.
 * @param[IN] ContDID   ContDID value of dxContainerD_t
 * @param[IN] ContMID   ContMID value of dxContainerD_t
 * @param[IN] ContDName ContDName value of dxContainerD_t
 * @param[IN] ContDType ContDType value of dxContainerD_t
 * @param[IN] DateTime  Time in seconds in Unix Epoch Time format.
 * @param[IN] SValue    Small size of data
 * @param[IN] LValue    Large size of data.
 * @param[IN] ContLVCRC CRC16-CCITT (Kermit) value of data stored in LValue
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         To free the allocated memory, use dxContainerD_Free()
 */
dxContainerD_t *dxContainerD_Create(uint64_t ContDID,
                                    uint64_t ContMID,
                                    uint8_t *ContDName,
                                    uint32_t ContDType,
                                    uint32_t DateTime,
                                    VarLenType_t *SValue,
                                    VarLenType_t *LValue,
                                    uint16_t ContLVCRC);

/**
 * @brief Free previously allocated memory blocks that using dxContainerD_Create()
 * @param[IN] pContD    Previously allocated memory blocks to be freed
 */
void dxContainerD_Free(dxContainerD_t *pContD);

/**
 * @brief Allocate memory blocks to store dxContainerM_t data. For ContMName variable, it's
 *        caller's responsibility to free the memory.
 * @param[IN] ContMID       ContMID value of dxContainerM_t
 * @param[IN] BLEObjectID   BLE ObjectID.
 * @param[IN] ContMType     ContMType value of dxContainerM_t
 * @param[IN] ContMName     ContMName value of dxContainerM_t
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         To free the allocated memory, use dxContainerM_Free()
 */
dxContainerM_t *dxContainerM_Create(uint64_t ContMID,
                                    uint32_t BLEObjectID,
                                    uint32_t ContMType,
                                    uint8_t *ContMName);

/**
 * @brief Free previously allocated memory blocks that using dxContainerM_Create()
 * @param[IN] pContD    Previously allocated memory blocks to be freed
 */
void dxContainerM_Free(dxContainerM_t *pContM);

/**
 * @brief Appened a dxContainerD_t object to dxContainerM_t
 *        caller's responsibility to free the memory.
 * @param[IN, OUT] pContM   A dxContainerM_t object
 * @param[IN] pContD        A dxContainerD_t object
 * @return DXCONT_RET_SUCCESS
 *         DXCONT_RET_ERROR
 */
dxCONT_RET_CODE dxContainerM_Append(dxContainerM_t *pContM, dxContainerD_t *pContD);

/**
 * @brief Allocate memory blocks to store dxContainer_t data.
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         To free the allocated memory, use dxContainer_Free()
 */
dxContainer_t *dxContainer_Create(void);

/**
 * @brief Make a deep copy of pCont
 * @param[IN] pCont         A dxContainer_t object to be copied
 * @param[IN] copyOption    If DXCONT_COPY_OPTION_ALL, whole container data will be copied.
 *                          otherwise, just container data with DirtyFlag equal to 1 will be copied
 * @return A pointer to point to the allocated memory block, or NULL if there is insufficient memory available.
 *         To free the allocated memory, use dxContainer_Free()
 */
dxContainer_t *dxContainer_Duplicate(dxContainer_t *pCont, dxContCopyOption_t copyOption);

/**
 * @brief Free previously allocated memory blocks that using dxContainer_Create()
 * @param[IN] pContD    Previously allocated memory blocks to be freed
 */
void dxContainer_Free(dxContainer_t *pCont);

/**
 * @brief Appened a dxContainerM_t object to dxContainer_t
 *        caller's responsibility to free the memory.
 * @param[IN, OUT] pCont    A dxContainer_t object
 * @param[IN] pContD        A dxContainerM_t object
 * @return DXCONT_RET_SUCCESS
 *         DXCONT_RET_ERROR
 */
dxCONT_RET_CODE dxContainer_Append(dxContainer_t *pCont, dxContainerM_t *pContM);

/**
 * @brief Calculate how many dxContainerM_t object exists in pCont
 * @param[IN] pCont     A dxContainer_t object
 * @return >= 0
 */
uint32_t dxContainer_GetMDataCount(dxContainer_t *pCont);

/**
 * @brief Get the index(th) dxContainerM_t object from pCont
 * @param[IN] pCont     A dxContainer_t object
 * @param[IN] index     An index
 * @return A pointer to a dxContainerM_t object we found
 *         NULL, otherwise
 */
dxContainerM_t *dxContainer_GetMDataByIndex(dxContainer_t *pCont, uint32_t index);

/**
 * @brief Calculate how many dxContainerD_t object exists in pContM
 * @param[IN] pContM    A dxContainerM_t object
 * @return >= 0
 */
uint32_t dxContainer_GetDDataCount(dxContainerM_t *pContM);

/**
 * @brief Get the index(th) dxContainerD_t object from pContM
 * @param[IN] pContM    A dxContainerM_t object
 * @param[IN] index     An index
 * @return A pointer to a dxContainerD_t object we found
 *         NULL, otherwise
 */
dxContainerD_t *dxContainer_GetDDataByIndex(dxContainerM_t *pContM, uint32_t index);

//============================================================================
// dxContainer Search/Find Function
//============================================================================
/**
 * @brief Find a specific ContMType dxContainerM_t object that next to pStartFrom.
 *        If pStartFrom is NULL, we will start a new search from head of pCont.
 * @param[IN] pCont         A dxContainer_t object
 * @param[IN] pStartFrom    A dxContainerM_t object.
 * @param[IN] index         An index
 * @return A pointer to a dxContainerM_t object we found
 *         NULL, otherwise
 */
dxContainerM_t *dxContainer_FindMDataByType(dxContainer_t *pCont, dxContainerM_t *pStartFrom, uint32_t MType);

/**
 * @brief Find a specific ContDType dxContainerD_t object that next to pStartFrom.
 *        If pStartFrom is NULL, we will start a new search from head of pCont.
 * @param[IN] pCont         A dxContainer_t object
 * @param[IN] pStartFrom    A dxContainerD_t object.
 * @param[IN] index         An index
 * @return A pointer to a dxContainerD_t object we found
 *         NULL, otherwise
 */
dxContainerD_t *dxContainer_FindDDataByType(dxContainer_t *pCont, dxContainerD_t *pStartFrom, uint32_t DType);

/**
 * @brief Find a specific ContMID of dxContainerM_t object.
 * @param[IN] pCont         A dxContainer_t object
 * @param[IN] ContMID       A ContMID value to search
 * @return A pointer to a dxContainerM_t object we found
 *         NULL, otherwise
 */
dxContainerM_t *dxContainer_FindMDataByID(dxContainer_t *pCont, uint64_t ContMID);

/**
 * @brief Find a specific ContDID of dxContainerD_t object.
 * @param[IN] pCont         A dxContainer_t object
 * @param[IN] ContDID       A ContDID value to search
 * @return A pointer to a dxContainerD_t object we found
 *         NULL, otherwise
 */
dxContainerD_t *dxContainer_FindDDataByID(dxContainer_t *pCont, uint64_t ContDID);

/**
 * @brief Find the last dxContainerM_t object.
 * @param[IN] pContM    A dxContainerM_t object
 * @return A pointer to a dxContainerM_t object we found
 *         NULL, otherwise
 */
dxContainerM_t *dxContainerM_GetLast(dxContainerM_t *pContM);

/**
 * @brief Find the last dxContainerD_t object.
 * @param[IN] pContM    A dxContainerD_t object
 * @return A pointer to a dxContainerD_t object we found
 *         NULL, otherwise
 */
dxContainerD_t *dxContainerD_GetLast(dxContainerD_t *pContD);

/**
 * @brief Print out the container.
 * @param[IN] pCont    Container object to print
 * @return NONE
 */
void PRINT_Container(dxContainer_t *pCont);

#ifdef __cplusplus
}
#endif

#endif // _DXCONTAINER_H
