//============================================================================
// File: DKMSG_Common.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.142.4
//     Date: 2016/01/08
//     1) Description: Fixed issue "Log Message Queue Full"
//        Modified:
//            - Change name of DK_PRINTLN_128() to DK_PRINTLN_ALL()
//        Added:
//            - DK_PRINTLN()
//
//     Version: 0.142.5
//     Date: 2016/01/11
//     1) Description: Fixed issue "Log Message Queue Full"
//        Modified:
//            - Change name of DK_HEX_DUMP() to DK_PRINT_HEX()
//            - DK_PRINT_HEX()
//
//     Version: 0.142.6
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.142.7
//     Date: 2016/02/24
//     1) Description: Move structure from DKServerManager.h
//        Added:
//            - DKRESTHeader_t
//            - DKRESTCommMsg_t
//
//     Version: 0.142.8
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Added:
//            - CONFIG_HEADER_X_DK_STATE
//            - DKMSG_GetSeqNo
//        Modified:
//            - DKMSG_TEMPLATE_HEADER_1
//
//     Version: 0.142.9
//     Date: 2017/03/27
//     1) Description: Fixed large JobInformation issue
//        Modified:
//            - STRING_BUFFER_SIZE
//
//     Version: 0.142.10
//     Date: 2017/09/11
//     1) Description: Support URL encode
//        Added:
//            - DKMSG_URLEncode()
//============================================================================
#ifndef _DKMSG_COMMON_H
#define _DKMSG_COMMON_H

#pragma once

#include "dxArch.h"
#include "dxOS.h"
#include "dkjsn.h"
#include "DKServerManager.h"
#include "Utils.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/
//512 //Aaron (20161004) - Temp fix to expand the buffer from 256 to 512. David will make this dynamic allocation for permanent fix
#define STRING_BUFFER_SIZE                          2048        // Move szBuffer to SDRAM

#define CONFIG_HEADER_X_DK_STATE                    0

#define DKMSG_TEMPLATE_HEADER_0                     "Host: %s\r\n"                                      \
                                                    "X-DK-Application-Id: %s\r\n"                       \
                                                    "X-DK-API-Key: %s\r\n"

#if CONFIG_HEADER_X_DK_STATE
#define DKMSG_TEMPLATE_HEADER_1                      DKMSG_TEMPLATE_HEADER_0                            \
                                                    "X-Dk-State: %d\r\n"                                \
                                                    "Content-Type: application/json\r\n"                \
                                                    "Content-Length: %d\r\n"
#else // CONFIG_HEADER_X_DK_STATE
#define DKMSG_TEMPLATE_HEADER_1                      DKMSG_TEMPLATE_HEADER_0                            \
                                                    "Content-Type: application/json\r\n"                \
                                                    "Content-Length: %d\r\n"
#endif // CONFIG_HEADER_X_DK_STATE


#define DKMSG_TEMPLATE_HEADER_2                     DKMSG_TEMPLATE_HEADER_1                             \
                                                    "X-DK-Session-Token: %s\r\n"

#define DKMSG_TEMPLATE_HEADER_RANGE                 "Range: bytes=%d-%d\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/
// For DKThread_Queue access
typedef struct _tagDKRESTHeader
{
    // Considering data alignment, add "Reserved" to structure.
    uint64_t TaskIdentificationNumber;      // The identification number that will be reported back to registered EventFunction when Task is done.
    uint16_t SourceOfOrigin;                // The original source/location that sent this task (see SourceOfOrigin_t in dk_job.h)
    int16_t  DataLen;
    uint8_t  MessageType;                   // See DKHTTPCommand_t
    uint8_t  Reserved[3];                   // Alignment byte
} DKRESTHeader_t;                           // sizeof(DKRESTHeader_t): 16

typedef struct _tagDKRESTCommMsg            // Message for RTOS Queue MUST be 4 bytes aligned and LESS than 64 bytes
{
    DKRESTHeader_t  Header;
    uint8_t         *pData;
    uint8_t         Reserved[4];            // Alignment byte
} DKRESTCommMsg_t;                          // sizeof(DKRESTHeader_t): 24

typedef int (*BUILD_DKSERVER_REQUEST_FUNC)(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen);
typedef int (*PARSE_DKSERVER_RESPONSE_FUNC)(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen);

typedef struct _tagDKServerAPI
{
    int                             nAPIID;
    BUILD_DKSERVER_REQUEST_FUNC     CreateRequest;
    PARSE_DKSERVER_RESPONSE_FUNC    ParseResponse;
} DKServerAPI_t;

/**
 * Use for DKRESTCommMsg_t Queue
 */
#ifndef __IAR_SYSTEMS_ICC__
typedef struct _tagDKAPIQueueNode DKAPIQueueNode_t;

typedef struct _tagDKAPIQueueNode
{
    DKAPIQueueNode_t *Next;
    DKRESTCommMsg_t *pRawCommand;
} DKAPIQueueNode_t;
#else // __IAR_SYSTEMS_ICC__
typedef struct _tagDKAPIQueueNode *PDKAPIQueueNode_t;

typedef struct _tagDKAPIQueueNode
{
    PDKAPIQueueNode_t Next;
    DKRESTCommMsg_t *pRawCommand;
} DKAPIQueueNode_t;
#endif // __IAR_SYSTEMS_ICC__

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *                External Variables
 ******************************************************/
extern char* szBuffer;
extern char* szBuffer2;
extern jsmntok_t* commonTokenTable;


/******************************************************
 *               Function Definitions
 ******************************************************/

extern int DKCommon_Init (void);

extern int b64_pton(char const *src, unsigned char *target, size_t targsize);
extern int b64_ntop(uint8_t const *src, size_t srclength, char *target, size_t targsize);

void DK_PRINTLN_ALL(char *p);
void DK_PRINTLN(char *p, int len);
void DK_PRINT_HEX(uint8_t *pData, int len);
void DKMSG_CopyToMemory(char *pD, int cbOutSize, char *pS, int cbInSize);
int DKMSG_GetJSONData(char *buffer, int *size, dxPTR_t handle, int *jsonpath, int node, int decode);
int DKMSG_JSONRemoveEscapeChar(uint8_t *pBuffer, int cbBuffSize, uint8_t *pszEscape);
int DKMSG_URLEncode(uint8_t *pDstBuff, int cbDstBuffSize, uint8_t *pSrcBuff);
uint8_t *DKMSG_AllocMemory(size_t size);
void DKMSG_FreeMemory(uint8_t *pMemBuffer);
int DKMSG_ConvertToDKTime(uint8_t *pszDateTime, DKTime_t *dkTime);

/**
 * Use for DKRESTCommMsg_t Queue
 */
void DKQ_Empty(void);
uint8_t DKQ_IsEmpty(void);
void DKQ_Push(DKRESTCommMsg_t *pCommand);
uint8_t DKQ_Pop(DKRESTCommMsg_t **pCommand);
uint8_t DKQ_PeekMessageType(void);
DKRESTCommMsg_t *DKQ_PeekMessage(void);
uint8_t DKQ_Count(void);

#if CONFIG_HEADER_X_DK_STATE
/**
 * Use for X-Dk-State
 */
uint32_t DKMSG_GetSeqNo(void);
#endif // CONFIG_HEADER_X_DK_STATE

#ifdef __cplusplus
}
#endif

#endif // _DKMSG_COMMON_H
