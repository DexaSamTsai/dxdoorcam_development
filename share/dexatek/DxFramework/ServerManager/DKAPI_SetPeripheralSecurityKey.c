//============================================================================
// File: DKAPI_SetPeripheralSecurityKey.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_SetPeripheralSecurityKey_Response()
//            - Build_SetPeripheralSecurityKey_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_SetPeripheralSecurityKey_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_SetPeripheralSecurityKey.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_SETPERIPHERALSECURITYKEY_API                                                     \
                                                    "%s "                                               \
                                                    "%s/SetPeripheralSecurityKey.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_SetPeripheralSecurityKey_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (PeripheralSecurityKey_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Peripheral information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    PeripheralSecurityKey_t *pPeripheral = (PeripheralSecurityKey_t *)pCommMsg->pData;

    if (pPeripheral->ObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         pPeripheral->ObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Total data length: %d\n", paramlen);
#endif

    sprintf((char *)pBuffer, DKMSG_TEMPLATE_SETPERIPHERALSECURITYKEY_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "{\"ObjectID\":%u,\"SecurityKey\":\"", (unsigned int)pPeripheral->ObjectID);
    n = strlen((char *)pBuffer);

    n = b64_ntop(pPeripheral->szSecurityKey, pPeripheral->nSecurityKeyLen, (char *)&pBuffer[n], cbSize - n);

    strcat((char *)pBuffer, "\"}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_SetPeripheralSecurityKey_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0
    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;
    uint32_t objectID = 0;              // Peripheral's ObjectID providing by DKServer
    int findList2[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_OBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (PeripheralSecurityKey_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Peripheral information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 ||
        cbSize <= 0 )
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    PeripheralSecurityKey_t *pPeripheral = (PeripheralSecurityKey_t *)pCommMsg->pData;

    if (pPeripheral->ObjectID == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid Peripheral Object ID %d!\n",
                         __FUNCTION__,
                         __LINE__,
                         pPeripheral->ObjectID);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {

        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ObjectID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "ObjectID: %s\n", szBuffer);

    objectID = strtoul(szBuffer, NULL, 10);
    if (objectID == 0 ||
        objectID != pPeripheral->ObjectID)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Replace global variable with the new value

    ObjectDictionary_Lock ();

    DK_BLEPeripheral_t *pStoredDev = ObjectDictionary_GetPeripheral (objectID);

    if (pStoredDev && pPeripheral->nSecurityKeyLen <= sizeof(pStoredDev->szSecurityKey))
    {
        memcpy(pStoredDev->szSecurityKey, pPeripheral->szSecurityKey, pPeripheral->nSecurityKeyLen);
        pStoredDev->nSecurityKeyLen = pPeripheral->nSecurityKeyLen;
    }

    ObjectDictionary_Unlock ();

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
