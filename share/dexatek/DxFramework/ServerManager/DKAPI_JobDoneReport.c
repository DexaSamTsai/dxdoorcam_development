//============================================================================
// File: DKAPI_JobDoneReport.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Build_JobDoneReport_Data()
//
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_JobDoneReport_Response()
//            - Build_JobDoneReport_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_JobDoneReport_Data()
//
//     Version: 0.224
//     Date: 2016/09/07
//     1) Description: Revise JobDoneReport API to support "ReportedData"
//        Modified:
//            - Build_JobDoneReport_Data
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_JobDoneReport.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_JOBDONEREPORT_API            "%s "                                               \
                                                    "%s/JobDoneReport.php HTTP/1.1\r\n"


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_JobDoneReport_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    uint8_t nNumOfPayloadAppend = 0;        // Number of {"ObjectID":<ObjectID>,"DeviceID":<DeviceID>,"TaskID":<TaskID>,"JobStatus":<JobStatus>} appended to buffer

    JobDoneReport_t *pJob = NULL;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen < (sizeof (JobDoneReport_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, DKMSG_JOBDONEREPORT parameters are invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    sprintf((char *)pBuffer, DKMSG_TEMPLATE_JOBDONEREPORT_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    strcat((char *)pBuffer, "{\"Data\":[");

    while (i + sizeof(DKRESTCommMsg_t) < paramlen &&
           ret == DKSERVER_STATUS_SUCCESS)
    {
        pJob = (JobDoneReport_t *)&pCommMsg->pData[i];

        if (pJob == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Invalid pParameters\n",
                             __FUNCTION__,
                             __LINE__);
            ret = DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
            continue;
        }

        if (nNumOfPayloadAppend > 0)
        {
            strcat((char *)pBuffer, ",");
        }

        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n],
                "{\"ObjectID\":%u,\"DeviceID\":%u,\"TaskID\":%u,\"JobStatus\":%d",
                (unsigned int)pJob->ObjectID,
                (unsigned int)pJob->DeviceIdentificationNumber,
                (unsigned int)pJob->PayLoadIdentificationNumber,
                (unsigned int)pJob->JobStatus);

        if (pJob->JobPayloadLen)
        {
            strcat((char *)pBuffer, ",\"ReportedData\":\"");

            n = strlen((char *)pBuffer);
            uint8_t *p = (uint8_t *)pJob;
            n = b64_ntop(&p[sizeof(JobDoneReport_t)], pJob->JobPayloadLen, (char *)&pBuffer[n], cbSize - n);

            strcat((char *)pBuffer, "\"");
        }

        strcat((char *)pBuffer, "}");

        i += sizeof(JobDoneReport_t) + pJob->JobPayloadLen;

        nNumOfPayloadAppend++;
    }

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }

    if (nNumOfPayloadAppend == 0)
    {
        // No data, return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    strcat((char *)pBuffer, "]}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    // Dump the result
    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_JobDoneReport_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail

    int rootnode = 0;                   // rootnode, must be 0
    int childnode = 0;                  // child of peripherals node

    int resultsnode = 0;                // index of results node
    int resultscount = 0;               // Number of results node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;
    int i = 0;

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList3[] = {DKJSN_TOKEN_OBJECTID, 0};

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0 ||
        pParameters == NULL ||
        paramlen < (sizeof (JobDoneReport_t) + sizeof (DKRESTCommMsg_t)))
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get result
    resultsnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (resultsnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    resultscount = DKJSN_GetObjectCount(handle, resultsnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Number of results: %d\n", resultscount);

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < resultscount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, resultsnode, i);
        if (childnode < 0)
        {
            // Can't select the job node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get ObjectID

        len = STRING_BUFFER_SIZE;
        ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, childnode, 0);
        if (ret != DKSERVER_STATUS_SUCCESS)
        {
            DKJSN_Uninit(handle);
            return ret;
        }

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "Results #%d, ObjectID: %s\n", i, szBuffer);

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Confirm job is done
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
