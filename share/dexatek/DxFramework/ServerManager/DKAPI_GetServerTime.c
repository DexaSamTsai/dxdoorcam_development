//============================================================================
// File: DKAPI_GetServerTime.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Added external function declaration
//
//     Version: 0.4
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Parse_GetServerTime_Response()
//
//     2) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetServerTime_Response()
//            - Build_GetServerTime_Data()
//
//     Version: 0.242
//     Date: 2017/04/14
//     1) Description: Support "ClientTimeZone"
//        Added:
//            - Parse_GetServerTime_Response()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetServerTime.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETSERVERTIME                "%s "                                               \
                                                    "%s/GetServerTime.php HTTP/1.1\r\n"                 \
                                                    "Host: %s\r\n\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetServerTime_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETSERVERTIME, "GET", ObjectDictionary_GetApiVersion (), ObjectDictionary_GetHostname ());

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetServerTime_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail

    int childnode = 0;                  // child of peripherals node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;
    ClientServerTime_t TimeInfo;        // ServerTime and ClientTimeZone

    int findList2[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_SERVERTIME, 0};
    int findList3[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_CLIENTTIMEZONE, 0};

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    memset(&TimeInfo, 0x00, sizeof(TimeInfo));

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ServerTime

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, childnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    TimeInfo.nServerTimeStamp = strtoul(szBuffer, NULL, 10);
    if (TimeInfo.nServerTimeStamp == 0)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "ServerTime: %u\n", (unsigned int)TimeInfo.nServerTimeStamp);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ClientTimeZone

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, childnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    TimeInfo.nClientTimeZone = strtol(szBuffer, NULL, 10);
    if ((TimeInfo.nClientTimeZone < -48) || (TimeInfo.nClientTimeZone > 48)) {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "ClientTimeZone: %u\n", (unsigned int)TimeInfo.nClientTimeZone);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Send TimeInfo (ServerTime,ClientTimeZone) to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETSERVERTIME,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)&TimeInfo,
                                            sizeof(TimeInfo));

    DKJSN_Uninit(handle);

    return DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
}
