//============================================================================
// File: HTTPManager.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/02/24
//     1) Description: Add a new function
//        Added:
//            - HTTPManager_Disconnect()
//
//     Version: 0.4
//     Date: 2016/03/14
//     1) Description: Remove "NOTE: Porting to dxOS" comment
//
//     Version: 0.5
//     Date: 2016/05/27
//     1) Description: Added a new function
//        Added:
//            - HTTPManager_GetLastAccessTime()
//
//     Version: 0.6
//     Date: 2017/03/28
//     1) Description: Revise Error Code
//        Modified:
//            - HTTPManager_SendEventToWorkerThread()
//            - HTTPManager_SendEventIDToWorkerThread()
//============================================================================
#ifndef _HTTPMANAGER_H
#define _HTTPMANAGER_H

#pragma once

#include "dxOS.h"

#include "ObjectDictionary.h"
#include "DKServerManager.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/
#define HTTP_OK                                     200

#define HTTP_PORT                                   80
#define HTTPS_PORT                                  443

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/
/******************************************************
 *
 *              +-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 *              |      UNKNOWN      |    INITIALIZED    |     CONNECTED     |      SENDING      |     RECEIVING     |   DISCONNECTING   | HTTPManager_StateMachine_State_t
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * | INITIALIZE |    INITIALIZED    |                   |                   |                   |                   |                   |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * |    CONNECT |                   |     CONNECTED     |                   |                   |                   |                   |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * |       SEND |                   |                   |      SENDING      |                   |                   |                   |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * |    RECEIVE |                   |                   |     RECEIVING     |                   |                   |                   |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * | DISCONNECT |                   |                   |   DISCONNECTING   |                   |                   |                   |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 * |            |                   |                   |                   |     CONNECTED     |     CONNECTED     |    INITIALIZED    |
 * +------------+-------------------+-------------------+-------------------+-------------------+-------------------+-------------------+
 *
 ******************************************************/

typedef enum
{
    HTTPMANAGER_STATEMACHINE_STATE_UNKNOWN      = 0,
    HTTPMANAGER_STATEMACHINE_STATE_INITIALIZED,
    HTTPMANAGER_STATEMACHINE_STATE_CONNECTED,
    HTTPMANAGER_STATEMACHINE_STATE_SENDING,
    HTTPMANAGER_STATEMACHINE_STATE_RECEIVING,
    HTTPMANAGER_STATEMACHINE_STATE_DISCONNECTING
} HTTPManager_StateMachine_State_t;

typedef enum
{
    HTTPMANAGER_COMMAND_UNKNOWN                 = 0,
    HTTPMANAGER_COMMAND_INITIALIZE              = 1,
    HTTPMANAGER_COMMAND_CONNECT                 = 2,
    HTTPMANAGER_COMMAND_SEND                    = 3,
    HTTPMANAGER_COMMAND_RECEIVE                 = 4,
    HTTPMANAGER_COMMAND_DISCONNECT              = 5,
    HTTPMANAGER_COMMAND_SUSPEND                 = 6,
    HTTPMANAGER_COMMAND_RESUME                  = 7
} HTTPManager_StateMachine_Command_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/
typedef void (*THREAD_MAIN_FUNC)(uint32_t arg);

typedef struct _tagHTWorker
{
    dxWorkweTask_t                      *Handle;
    dxEventHandler_t                    RegisterFunc;
} HTWorker_t;

/******************************************************
 *                    Structures
 ******************************************************/
// For queue access
typedef struct _tagHTQHeader
{
    uint32_t    TCPWaitTimeInSec;
    uint8_t     MessageType;
    int16_t     DataLen;
} HTQHeader_t;

typedef struct _tagHTQCommMsg
{
    HTQHeader_t     Header;
    uint8_t         *pData;
} HTQCommMsg_t;

typedef struct _tagHTEventReport
{
    HTQHeader_t Header;
    uint16_t    HTTPStatusCode;
    int16_t     ErrorCode;
    uint8_t     *pData;
} HTEventReport_t;

/******************************************************
 *               Function Definitions
 ******************************************************/
HTQHeader_t HTTPManager_GetEventObjHeader(void *ReportedEventArgObj);
uint8_t *HTTPManager_GetEventObjPayload(void *ReportedEventArgObj);

dxOS_RET_CODE HTTPManager_CleanEventArgumentObj(void *ReportedEventArgObj);
int16_t HTTPManager_GetEventObjErrorCode(void *ReportedEventArgObj);
uint16_t HTTPManager_GetEventObjHTTPStatusCode(void *ReportedEventArgObj);

dxOS_RET_CODE HTTPManager_SendEventToWorkerThread(uint8_t nMessageType, uint32_t timeout, int16_t/*uint8_t*/ nErrorCode, void *pData, int DataLen);
dxOS_RET_CODE HTTPManager_SendEventIDToWorkerThread(uint8_t nMessageType, uint32_t timeout, int16_t/*uint8_t*/ nErrorCode);
uint8_t HTTPManager_GetCurrentState(void);

dxOS_RET_CODE HTTPManager_Init(dxEventHandler_t EventFunction);
dxOS_RET_CODE HTTPManager_Receive_Stop(void);

dxOS_RET_CODE HTTPManager_Connect_Asynch(dxIP_Addr_t *ip, uint16_t port, uint32_t timeout);
dxOS_RET_CODE HTTPManager_Send_Asynch(uint8_t *pData, uint32_t cbData, uint32_t timeout);
dxOS_RET_CODE HTTPManager_Receive_Asynch(uint32_t timeout);
dxOS_RET_CODE HTTPManager_Disconnect_Asynch(void);

dxOS_RET_CODE HTTPManager_Suspend_Asynch(void);
dxOS_RET_CODE HTTPManager_Resume_Asynch(dxIP_Addr_t *ip, uint16_t port, uint32_t timeout);

dxTime_t HTTPManager_GetLastAccessTime(void);

#ifdef __cplusplus
}
#endif

#endif // _HTTPMANAGER_H
