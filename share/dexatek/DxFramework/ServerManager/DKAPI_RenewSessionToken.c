//============================================================================
// File: DKAPI_RenewSessionToken.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_RenewSessionToken_Response()
//            - Build_RenewSessionToken_Data()
//
//     Version: 0.4
//     Date: 2016/05/04
//     1) Description: Added "GMACAddress" to request message
//        Modified:
//            - DKMSG_TEMPLATE_RENEWSESSIONTOKEN3_BODY
//            - Build_RenewSessionToken_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_RenewSessionToken_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_RenewSessionToken.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_RENEWSESSIONTOKEN1_BODY      "{\"ServerLogin\":{\"Email\":\"%s\",\"HashCode\":\"%s\"}}"
#define DKMSG_TEMPLATE_RENEWSESSIONTOKEN1           "%s "                                               \
                                                    "%s/RenewSessionToken.php HTTP/1.1\r\n"             \
                                                    DKMSG_TEMPLATE_HEADER_1                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_RENEWSESSIONTOKEN1_BODY

#define DKMSG_TEMPLATE_RENEWSESSIONTOKEN3_BODY      "{\"GatewayLogin\":{\"GatewayKey\":\"%s\",\"GMACAddress\":\"%s\"}}"
#define DKMSG_TEMPLATE_RENEWSESSIONTOKEN3           "%s "                                               \
                                                    "%s/RenewSessionToken.php HTTP/1.1\r\n"             \
                                                    DKMSG_TEMPLATE_HEADER_1                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_RENEWSESSIONTOKEN3_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_RenewSessionToken_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    DK_Users_t *pUser = NULL;

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    ObjectDictionary_Lock ();
    pUser = ObjectDictionary_GetUser ();
    ObjectDictionary_Unlock ();

    if (pUser == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, User login information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(szBuffer, 0x00, STRING_BUFFER_SIZE);
    if (pUser->UserInfo.LoginType == DK_LOGINTYPE_SERVER)
    {
        n = ObjectDictionary_CreateHashCode ((char *)pUser->UserInfo.Username, (char *)pUser->UserInfo.Password, szBuffer, STRING_BUFFER_SIZE);
        if (n <= 0)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Failed to create UserLogin HashCode.\n",
                             __FUNCTION__,
                             __LINE__);
            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_RENEWSESSIONTOKEN1,
                "POST",
                ObjectDictionary_GetApiVersion (),
                ObjectDictionary_GetHostname (),
                ObjectDictionary_GetApplicationId (),
                ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
                DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
                snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_RENEWSESSIONTOKEN1_BODY, pUser->UserInfo.Username, szBuffer),
                pUser->UserInfo.Username,
                szBuffer);
    }
    else if (pUser->UserInfo.LoginType == DK_LOGINTYPE_GATEWAY)
    {
        char szData[21] = {0};

        ObjectDictionary_Lock ();

        int maclen = _MACAddrToDecimalStr (szData, sizeof(szData), (uint8_t*) ObjectDictionary_GetMacAddress (), 8);
        if (maclen <= 0)
        {
            ObjectDictionary_Unlock ();
            DKHex_HexToASCII (ObjectDictionary_GetMacAddress (), 8, &szData[0], sizeof(szData));
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, MAC address %s convert to decimal string failed!\n",
                             __FUNCTION__,
                             __LINE__,
                             szData);
            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        ObjectDictionary_Unlock ();

        n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_RENEWSESSIONTOKEN3,
                "POST",
                ObjectDictionary_GetApiVersion (),
                ObjectDictionary_GetHostname (),
                ObjectDictionary_GetApplicationId (),
                ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
                DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
                snprintf(NULL, 0, DKMSG_TEMPLATE_RENEWSESSIONTOKEN3_BODY, pUser->UserInfo.Username, szData),
                pUser->UserInfo.Username,
                szData);
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, User's LoginType (%d) is invalid\n",
                         __FUNCTION__,
                         __LINE__,
                         pUser->UserInfo.LoginType);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_RenewSessionToken_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int rootnode = 0;
    dxPTR_t handle = 0;
    int len = 0;

    int findList3[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_SESSIONTOKEN, 0};

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get sessionToken

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    // Added HashCode to dk_objects.c
    ObjectDictionary_SetSessionToken (szBuffer);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "SessionToken: %s\n", ObjectDictionary_GetSessionToken ());

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
