//============================================================================
// File: DKAPI_DownloadFile.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Added external function declaration
//
//     Version: 0.4
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - Parse_FirmwareDownload_Response()
//            - Build_FirmwareDownload_Data()
//
//     Version: 0.5
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_FirmwareDownload_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_DownloadFile.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_FIRMWAREDOWNLOAD_BODY        "{\"FullPathFileName\":\"%s\"}"
#define DKMSG_TEMPLATE_FIRMWAREDOWNLOAD             "%s "                                               \
                                                    "%s/DownloadFile.php HTTP/1.1\r\n"                  \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    DKMSG_TEMPLATE_HEADER_RANGE                         \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_FIRMWAREDOWNLOAD_BODY


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_FirmwareDownload_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int nstart = 0;                         // first-byte-position
    int nlast = 0;                          // last-byte-position
    char *p = NULL;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen == 0 ||
        paramlen >= cbSize || //sizeof(HTTPBuffer) ||
        paramlen < 10)              // Cannot less than 10 characters
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    p = (char *)pCommMsg->pData;

    /*
     * pCommMsg->pData format =>
     * +----------------+
     * | first-byte-pos | ( 4 Byte )
     * +----------------+
     * | last-byte-pos  | ( 4 Byte )
     * +----------------+
     * | filename       | ( n Byte, null terminated string )
     * +----------------+
     */

    n = 0;
    memcpy(&nstart, &p[n], sizeof(n));
    n += sizeof(n);
    memcpy(&nlast, &p[n], sizeof(n));
    n += sizeof(n);
    nlast += nstart - 1;

    memset(pBuffer, 0x00, cbSize);

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_FIRMWAREDOWNLOAD,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_FIRMWAREDOWNLOAD_BODY, &p[n]),
            ObjectDictionary_GetSessionToken (),
            nstart,
            nlast,
            &p[n]);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_FirmwareDownload_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    DKContentRange_t *range = NULL;
    uint8_t *pFirmware = NULL;

    if (pMsg == NULL ||
        optionData == NULL ||
        optionDataLen != sizeof(DKContentRange_t) ||
        cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    range = (DKContentRange_t *)optionData;
    if (range->nLastBytePosition < range->nFirstBytePosition ||
        range->nFileSize == 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pFirmware = DKMSG_AllocMemory((cbSize + optionDataLen) * sizeof(uint8_t));
    if (pFirmware == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    DKFirmwareInfo_t *p = (DKFirmwareInfo_t *)pFirmware;
    p->range.nFirstBytePosition = range->nFirstBytePosition;
    p->range.nLastBytePosition = range->nLastBytePosition;
    p->range.nFileSize = range->nFileSize;

    memcpy(&pFirmware[offsetof(DKFirmwareInfo_t, pData)], pMsg, cbSize);

    // Send Information data to worker thread
    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_FIRMWAREDOWNLOAD,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_STATUS_SUCCESS,
                                            (void *)pFirmware,
                                            cbSize + optionDataLen);

    DKMSG_FreeMemory(pFirmware);
    pFirmware = NULL;

    return DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
}
