//============================================================================
// File: DKAPI_AddStandalonePeripheral.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.177
//     Date: 2016/01/11
//     1) Description: Support AddStandalonePeripheral API
//        Added:
//            - Define DKMSG_TEMPLATE_ADDSTANDALONEPERIPHERAL_API
//            - Build_AddStandalonePeripheral_Data()
//            - Parse_AddStandalonePeripheral_Response()
//
//     Version: 0.177.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.177.2
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_AddStandalonePeripheral_Response()
//            - Build_AddStandalonePeripheral_Data()
//
//     Version: 0.177.3
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_AddStandalonePeripheral_Data()
//
//     Version: 0.177.4
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Modified:
//            - Parse_AddStandalonePeripheral_Response()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_AddSmartHomeCentralLog.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_ADDSTANDALONEPERIPHERAL_API                                                      \
                                                    "%s "                                               \
                                                    "%s/AddStandalonePeripheral.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_AddStandalonePeripheral_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;

    char szData[21] = {0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (AddPeripheral_t) + sizeof (double) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Peripheral information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    double *latitude = (double *)&pCommMsg->pData[i];
    i += sizeof(double);

    double *longtitude = (double *)&pCommMsg->pData[i];
    i += sizeof(double);

    AddPeripheral_t *pPeripheral = (AddPeripheral_t *)&pCommMsg->pData[i];

    int maclen = _MACAddrToDecimalStr(szData, sizeof(szData), pPeripheral->MAC, 8);
    if (maclen <= 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, MAC address convert to decimal string failed!\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    sprintf((char *)pBuffer, DKMSG_TEMPLATE_ADDSTANDALONEPERIPHERAL_API, "POST", ObjectDictionary_GetApiVersion ());
    strcat((char *)pBuffer, "Host: ");
    strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
    strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
    strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
    strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
    strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
    strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
    strcat((char *)pBuffer, "\r\nContent-Length: ");
    ContentLengthPosition = strlen((char *)pBuffer);
    strcat((char *)pBuffer, "0000\r\n\r\n");
    JSONDataPosition = strlen((char *)pBuffer);

    strcat((char *)pBuffer, "{");

    // "GMACAddress
    n = strlen((char *)pBuffer);
    sprintf((char *)&pBuffer[n], "\"PMACAddress\":\"%s\",\"DeviceName\":\"%s\",\"Latitude\":%9.6f,\"Longitude\":%10.6f,\"DeviceType\":%d,\"PSignalStr\":%d,\"PBattery\":%d",
            szData, pPeripheral->szDeviceName, *latitude, *longtitude, pPeripheral->deviceType, pPeripheral->PSignalStr, pPeripheral->PBattery);

    // "FirmwareVersion"
    if (*pPeripheral->FWVersion)
    {
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], ",\"FirmwareVersion\":\"%s\"", pPeripheral->FWVersion);
    }

    // "SecurityKey"
    if (*pPeripheral->szSecurityKey && pPeripheral->nSecurityKeyLen > 0)
    {
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], ",\"SecurityKey\":\"");

        n = strlen((char *)pBuffer);
#if defined(ENABLE_BASE64)
        n = b64_ntop(pPeripheral->szSecurityKey, pPeripheral->nSecurityKeyLen, (char *)&pBuffer[n], cbSize - n);
#endif

        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"");
    }

    strcat((char *)pBuffer, "}");

    // Overwrite Content-Length
    n = strlen((char *)pBuffer);
    if (n > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen((char *)&pBuffer[JSONDataPosition]);

    for (i = 4; i > 0; i--)
    {
        pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
        n /= 10;
    }

    n = strlen((char *)pBuffer);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_AddStandalonePeripheral_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail

    int rootnode = 0;                   // rootnode, must be 0

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;
    int i = 0;

    int findList2[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_OBJECTID, 0};

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen != (sizeof (AddPeripheral_t) + sizeof (double) * 2 + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Peripheral information is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // First 8 bytes are latitude
    // Next 8 bytes are Longitude
    // The other bytes are structure of AddPeripheral_t
    AddPeripheral_t *pPeripheral = (AddPeripheral_t *)&pCommMsg->pData[sizeof(double) * 2];

    uint32_t objectID = 0;

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ObjectID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Peripheral, ObjectID: %s\n", szBuffer);

    objectID = strtoul(szBuffer, NULL, 10);
    if (objectID == 0)
    {
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Copy local variable to global

    ObjectDictionary_Lock ();
    ObjectDictionary_AddPeripheral (objectID, pPeripheral->MAC, sizeof(pPeripheral->MAC), 0, pPeripheral->deviceType, pPeripheral->FWVersion, pPeripheral->szSecurityKey, pPeripheral->nSecurityKeyLen, pPeripheral->szDeviceName);
    ObjectDictionary_Unlock ();

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    ObjectDictionary_Lock();

    // Dump all peripheral
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "[%s] Line %d, ObjectDictionary_GetPeripheralCount () = %d\n", __FUNCTION__, __LINE__, ObjectDictionary_GetPeripheralCount ());

    for (i = 0; i < ObjectDictionary_GetPeripheralCount (); i++)
    {
        DK_BLEPeripheral_t *pPeripheral = ObjectDictionary_GetPeripheralByIndex (i);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "#%d =>\n\tObjectID: %u\n\tGatewayID: %u\n\tDeviceType: %d\n",
                         i, (unsigned int)pPeripheral->ObjectID, (unsigned int)pPeripheral->GatewayID, pPeripheral->DeviceType);
        G_SYS_DBG_LOG_INV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                          "\tMAC Address: %02X-%02X-%02X-%02X",
                          ObjectDictionary_GetGateway ()->MACAddress[0], ObjectDictionary_GetGateway ()->MACAddress[1], ObjectDictionary_GetGateway ()->MACAddress[2],
                          ObjectDictionary_GetGateway ()->MACAddress[3]);
        G_SYS_DBG_LOG_INV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                          "-%02X-%02X-%02X-%02X\n",
                          ObjectDictionary_GetGateway ()->MACAddress[4], ObjectDictionary_GetGateway ()->MACAddress[5], ObjectDictionary_GetGateway ()->MACAddress[6],
                          ObjectDictionary_GetGateway ()->MACAddress[7]);
    }

    ObjectDictionary_Unlock();

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
