//============================================================================
// File: DKAPI_GatewayLogin.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GatewayLogin_Response()
//            - Build_GatewayLogin_Data()
//
//     Version: 0.4
//     Date: 2016/04/08
//     1) Description: Support changing SessionTTL value
//        Added:
//            - CONFIG_CLIENT_PROVIDED_SESSIONTTL
//        Modified:
//            - DKMSG_TEMPLATE_GATEWAYLOGIN
//
//     Version: 0.5
//     Date: 2016/05/04
//     1) Description: Added "GMACAddress" to request message
//        Modified:
//            - DKMSG_TEMPLATE_GATEWAYLOGIN_BODY
//            - Build_GatewayLogin_Data()
//
//     Version: 0.6
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GatewayLogin_Data()
//
//     Version: 0.223.0
//     Date: 2017/09/27
//     1) Description: Support login with "DKAuth".
//        Modified:
//            - Build_GatewayLogin_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GatewayLogin.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GATEWAYLOGIN_BODY            "{\"GatewayLogin\":{\"GatewayKey\":\"%s\",\"GMACAddress\":\"%s\"}}"
#define DKMSG_TEMPLATE_ANONYMOUSLOGIN_API                                                               \
                                                    "%s "                                               \
                                                    "%s/UserLogin.php HTTP/1.1\r\n"

#define CONFIG_CLIENT_PROVIDED_SESSIONTTL           0

#if CONFIG_CLIENT_PROVIDED_SESSIONTTL
#define USE_CLIENT_PROVIDED_SESSIONTTL              "300"   // In seconds
#pragma message ( "WARNING: USE_CLIENT_PROVIDED_SESSIONTTL is set to " USE_CLIENT_PROVIDED_SESSIONTTL )

#define DKMSG_TEMPLATE_GATEWAYLOGIN                 "%s "                                               \
                                                    "%s/UserLogin.php HTTP/1.1\r\n"                     \
                                                    DKMSG_TEMPLATE_HEADER_1                             \
                                                    "SessionTTL: " USE_CLIENT_PROVIDED_SESSIONTTL "\r\n"                               \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GATEWAYLOGIN_BODY
#else // CONFIG_CLIENT_PROVIDED_SESSIONTTL
#define DKMSG_TEMPLATE_GATEWAYLOGIN                 "%s "                                               \
                                                    "%s/UserLogin.php HTTP/1.1\r\n"                     \
                                                    DKMSG_TEMPLATE_HEADER_1                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GATEWAYLOGIN_BODY
#endif // CONFIG_CLIENT_PROVIDED_SESSIONTTL

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GatewayLogin_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    UserLogin_t *pUser = NULL;
    uint32_t AccTokenTime = 0;
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    char *p = NULL;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen < (sizeof (UserLogin_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, GatewayKey is invalid\n", __FUNCTION__, __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

/**
 *     Header.DataLen           = sizeof(UserLogin_t)
 *     pData                    = +----------------+-------------+----------------------------------+
 *                                | Username       | ( 64 Byte ) |                                  |
 *                                +----------------+-------------+                                  |
 *                                | Password       | ( 32 Byte ) | UserLogin_t                      |
 *                                +----------------+-------------+                                  |
 *                                | DK_LoginType_t | ( 4 Byte )  |                                  |
 *                                +----------------+-------------+----------------------------------+
 *                                | AccTokenTime   | ( 4 Byte )  |                                  |
 *                                +----------------+-------------+----------------------------------+
 *                                | AccessToken    | ( n Byte )  | Should be NULL terminated string |
 *                                +----------------+-------------+----------------------------------+
 */

    pUser = (UserLogin_t *)pCommMsg->pData;

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    memset(pBuffer, 0x00, cbSize);

    if (pUser->LoginType == DK_LOGINTYPE_GATEWAY)
    {
        char szData[21] = {0};

        ObjectDictionary_Lock ();

        int maclen = _MACAddrToDecimalStr (szData, sizeof(szData), (uint8_t*) ObjectDictionary_GetMacAddress (), 8);
        if (maclen <= 0)
        {
            ObjectDictionary_Unlock ();
            DKHex_HexToASCII (ObjectDictionary_GetMacAddress (), 8, &szData[0], sizeof(szData));
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, MAC address %s convert to decimal string failed!\n",
                             __FUNCTION__,
                             __LINE__,
                             szData);
            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        ObjectDictionary_Unlock ();

        n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GATEWAYLOGIN,
                "POST",
                ObjectDictionary_GetApiVersion (),
                ObjectDictionary_GetHostname (),
                ObjectDictionary_GetApplicationId (),
                ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
                DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
                snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GATEWAYLOGIN_BODY, pUser->Username, szData),
                pUser->Username,
                szData);
    }
    else if (pUser->LoginType == DK_LOGINTYPE_ANONYMOUS)
    {
        p = (char *)&pCommMsg->pData[sizeof(UserLogin_t)];

        i = 0;
        memcpy(&AccTokenTime, &p[i], sizeof(AccTokenTime));
        i += sizeof(AccTokenTime);

        sprintf((char *)pBuffer, DKMSG_TEMPLATE_ANONYMOUSLOGIN_API, "POST", ObjectDictionary_GetApiVersion ());
        strcat((char *)pBuffer, "Host: ");
        strcat((char *)pBuffer, ObjectDictionary_GetHostname ());
        strcat((char *)pBuffer, "\r\nX-DK-Application-Id: ");
        strcat((char *)pBuffer, ObjectDictionary_GetApplicationId ());
        strcat((char *)pBuffer, "\r\nX-DK-API-Key: ");
        strcat((char *)pBuffer, ObjectDictionary_GetApiKey ());
#if CONFIG_HEADER_X_DK_STATE
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo());
#endif // CONFIG_HEADER_X_DK_STATE
        strcat((char *)pBuffer, "\r\nX-DK-Session-Token: ");
        strcat((char *)pBuffer, ObjectDictionary_GetSessionToken ());
        strcat((char *)pBuffer, "\r\nContent-Length: ");
        ContentLengthPosition = strlen((char *)pBuffer);
        strcat((char *)pBuffer, "0000\r\n\r\n");
        JSONDataPosition = strlen((char *)pBuffer);


        strcat((char *)pBuffer, "{\"DKAuth\":{");

        // AccessToken
        n = strlen((char *)pBuffer);
        sprintf((char *)&pBuffer[n], "\"AccessToken\":\"%s\"", &p[i]);
        i += strlen((char *)&p[i]) + 1;         // // Move p to next field. Including 0x00 NULL terminated string

        // AccTokenTime
        if (AccTokenTime > 0)
        {
            n = strlen((char *)pBuffer);
            sprintf((char *)&pBuffer[n], ",\"AccTokenTime\":%u", AccTokenTime);
        }

        strcat((char *)pBuffer, "}");

        strcat((char *)pBuffer, "}");

        // Overwrite Content-Length
        n = strlen((char *)pBuffer);
        if (n > cbSize)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                             __FUNCTION__,
                             __LINE__,
                             n,
                             cbSize);
            return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        }

        n = strlen((char *)&pBuffer[JSONDataPosition]);

        for (i = 4; i > 0; i--)
        {
            pBuffer[ContentLengthPosition + i - 1] = '0' + (n % 10);
            n /= 10;
        }

        n = strlen((char *)pBuffer);
    }
    else
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GatewayLogin_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int rootnode = 0;
    dxPTR_t handle = 0;
    int len = 0;
    uint32_t objectID = 0;
    UserLogin_t *pUser = NULL;

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen < (sizeof (UserLogin_t) + sizeof (DKRESTCommMsg_t)))
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, GatewayKey is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pUser = (UserLogin_t *)pCommMsg->pData;

    int findList3[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_SESSIONTOKEN, 0};
    int findList4[] = {DKJSN_TOKEN_RESULTS, DKJSN_TOKEN_OBJECTID, 0};

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get sessionToken

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    // Added HashCode to dk_objects.c
    ObjectDictionary_SetSessionToken (szBuffer);
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "SessionToken: %s\n", ObjectDictionary_GetSessionToken ());
#endif
    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ObjectID of a user

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList4, rootnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        DKJSN_Uninit(handle);
        return ret;
    }

    objectID = strtoul(szBuffer, NULL, 10);

    ObjectDictionary_Lock ();

    if (objectID == 0 || ObjectDictionary_AddUser (objectID, objectID, pUser) == 0)
    {
        ObjectDictionary_Unlock ();
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "ObjectID: %u\n", (unsigned int) ObjectDictionary_GetUser ()->ObjectID);

    ObjectDictionary_Unlock ();

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    DKJSN_Uninit(handle);

    return ret;
}
