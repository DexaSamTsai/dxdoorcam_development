//============================================================================
// File: DKAPI_GetNewJobs.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//
//     Version: 0.142.2
//     Date: 2015/05/26
//     1) Description: Shorten output log message
//        Modified:
//            - _ParseStandardJobJSON()
//            - _ParseAdvancedJobJSON()
//
//     Version: 0.171.0
//     Date: 2015/07/21
//     1) Description: Support advanced job single gateway pack
//        Modified:
//            - _ParseAdvancedJobJSON()
//            - Parse_GetNewJobs_Response()
//
//     Version: 0.171.1
//     Date: 2015/07/21
//     1) Description: Fix a potenital issue with JobType
//        Modified:
//            - Parse_GetNewJobs_Response()
//
//     Version: 0.171.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.171.3
//     Date: 2016/02/24
//     1) Description: Fix an issue while parsing response
//        Modified:
//            - Parse_GetNewJobs_Response()
//
//     Version: 0.171.4
//     Date: 2016/03/01
//     1) Description: Rename DKHTTPCommand_t to DKMSG_Command_t
//        Modified:
//            - _ParseStandardJobJSON()
//            - _ParseAdvancedJobJSON()
//
//     Version: 0.171.4
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Parse_GetNewJobs_Response()
//            - _ParseStandardJobJSON()
//            - _ParseAdvancedJobJSON()
//            - Build_GetNewJobs_Data()
//
//     Version: 0.171.5
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_GetNewJobs_Data()
//
//     Version: 0.171.6
//     Date: 2017/04/14
//     1) Description: To avoid a JSON parsing error due to no "JobType" in JSON data.
//        Modified:
//            - Parse_GetNewJobs_Response()
//
//     Version: 0.253.0
//     Date: 2017/08/30
//     1) Description: Support "TimeStamp" in GetNewJobs API
//        Modified:
//            - _ParseStandardJobJSON()
//            - _ParseAdvancedJobJSON()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetNewJobs.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETNEWJOBS_BODY              "{\"GatewayID\":%u,\"Max\":%d}"
#define DKMSG_TEMPLATE_GETNEWJOBS                   "%s "                                               \
                                                    "%s/GetNewJobs.php HTTP/1.1\r\n"                    \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_GETNEWJOBS_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/
int _ParseStandardJobJSON(dxPTR_t handle, int node, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t objid = 0;                 // An variable to store the value which convert from strtoul()

    int len;
    int joblen = 0;                     // length of pJobInfo

    uint8_t *pJobInfo = NULL;           // Job information, ObjectID/GatewayID/DeviceID/TaskID...
    DKStandardJob_t *pStdJob = NULL;    // Standard job information

    int findList2[] = {DKJSN_TOKEN_OBJECTID, 0};
    int findList3[] = {DKJSN_TOKEN_GATEWAYID, 0};
    //int findList4[] = {DKJSN_TOKEN_USEROBJECTID, 0};        // Never use in JobManager, will be removed
    int findList5[] = {DKJSN_TOKEN_DEVICEID, 0};
    int findList6[] = {DKJSN_TOKEN_TASKID, 0};
    int findList7[] = {DKJSN_TOKEN_JOBINFORMATION, 0};
    int findList8[] = {DKJSN_TOKEN_CREATEDBYUSEROBJID, 0};
    int findList9[] = {DKJSN_TOKEN_TIMESTAMP, 0};

    if (handle == 0)
    {
        return 0;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get JobInformation

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList7, node, 1);

    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        //DKJSN_Uninit(handle);
        return ret;
    }

    joblen = sizeof(DKStandardJob_t) + len;

    //pJobInfo = dxMemAlloc(CALLOC_NAME, 1, joblen);
    pJobInfo = DKMSG_AllocMemory(joblen);
    if (pJobInfo == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    pStdJob = (DKStandardJob_t *)pJobInfo;
    memcpy(pStdJob->pPayload, szBuffer, len);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get ObjectID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return ret;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Job, ObjectID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pStdJob->ObjectID = objid;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get GatewayID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return ret;
    }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Job, GatewayID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pStdJob->GatewayID = objid;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get CreatedByUserObjID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList8, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return ret;
    }
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Job, CreatedByUserObjID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        //dxMemFree(pJobInfo);
        DKMSG_FreeMemory(pJobInfo);
        pJobInfo = NULL;

        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    pStdJob->UserObjectID = objid;


    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get TimeStamp (Optional)

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList9, node, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Job, TimeStamp: %s\n", szBuffer);
#endif

        pStdJob->TimeStamp = strtoul(szBuffer, NULL, 10);;
    }
    else
    {
        pStdJob->TimeStamp = 0;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get DeviceID (Optional)

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, node, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Job, DeviceID: %s\n", szBuffer);
#endif

        objid = strtoul(szBuffer, NULL, 10);
        if (objid == 0)
        {
            //dxMemFree(pJobInfo);
            DKMSG_FreeMemory(pJobInfo);
            pJobInfo = NULL;

            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        pStdJob->DeviceIdentificationNumber = objid;
    }
    else
    {
        pStdJob->DeviceIdentificationNumber = 0;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get TaskID (Optional)

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, node, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Job, TaskID: %s\n", szBuffer);
#endif

        objid = strtoul(szBuffer, NULL, 10);
        if (objid == 0)
        {
            //dxMemFree(pJobInfo);
            DKMSG_FreeMemory(pJobInfo);
            pJobInfo = NULL;

            return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
        }

        pStdJob->PayLoadIdentificationNumber = objid;
    }
    else
    {
        pStdJob->PayLoadIdentificationNumber = 0;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Job, ObjectID: %u, GatewayID: %u, UserObjID: %u\n",
                     (unsigned int)pStdJob->ObjectID, (unsigned int)pStdJob->GatewayID, (unsigned int)pStdJob->UserObjectID);
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Job, DeviceID: %u, TaskID: %u\n",
                     (unsigned int)pStdJob->DeviceIdentificationNumber, (unsigned int)pStdJob->PayLoadIdentificationNumber);
#endif

    // Send JobInformation data to worker thread
    DKServerManager_SendEventToWorkerThread(SourceOfOrigin,
                                            TaskIdentificationNumber,
                                            DKMSG_NEWJOBS,
                                            DKJOBTYPE_STANDARD,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)pJobInfo,
                                            joblen);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    //dxMemFree(pJobInfo);
    DKMSG_FreeMemory(pJobInfo);
    pJobInfo = NULL;

    return ret;
}

int _ParseAdvancedJobJSON(dxPTR_t handle, int node, uint8_t jobtype, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    uint32_t objid = 0;                 // An variable to store the value which convert from strtoul()

    int childnode = 0;                  // index of Conditions node or Executions node
    int childcount = 0;                 // number of Conditions node or Executions node

    int jobnode = 0;                    // index of JobCondition or JobExecution node

    int i = 0;
    int len;
    int joblen = 0;                     // length of pJobInfo
    int type = 0;                       // value of enum DKJobType_t
    int jobcondfound = 0;               // 1 if DKJSN_TOKEN_JOBCONDITION found
    int jobexecfound = 0;               // 1 if DKJSN_TOKEN_JOBEXECUTION found

    DKStandardJob_t JobCommon;          // Job common information
    DKStandardJob_t *pAdvJob = NULL;    // Advanced job information

    int findList2[] = {DKJSN_TOKEN_OBJECTID, 0};
    int findList3[] = {DKJSN_TOKEN_GATEWAYID, 0};

    int findList8[] = {DKJSN_TOKEN_CONDITIONS, 0};
    int findList9[] = {DKJSN_TOKEN_JOBCONDITION, 0};
    int findList10[] = {DKJSN_TOKEN_EXECUTIONS, 0};
    int findList11[] = {DKJSN_TOKEN_JOBEXECUTION, 0};
    int findList12[] = {DKJSN_TOKEN_CREATEDBYUSEROBJID, 0};
    int findList13[] = {DKJSN_TOKEN_TIMESTAMP, 0};

    if (handle == 0)
    {
        return 0;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    memset(&JobCommon, 0x00, sizeof(JobCommon));

    // Get ObjectID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }
#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "AdvJob, ObjectID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    JobCommon.ObjectID = objid;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get GatewayID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }
#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "AdvJob, GatewayID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    JobCommon.GatewayID = objid;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get CreatedByUserObjID

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList12, node, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        return ret;
    }
#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "AdvJob, CreatedByUserObjID: %s\n", szBuffer);
#endif

    objid = strtoul(szBuffer, NULL, 10);
    if (objid == 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    JobCommon.UserObjectID = objid;

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get TimeStamp (Optional)

    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList13, node, 0);
    if (ret == DKSERVER_STATUS_SUCCESS)
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "AdvJob, TimeStamp: %s\n", szBuffer);
#endif

        JobCommon.TimeStamp = strtoul(szBuffer, NULL, 10);;
    }
    else
    {
        JobCommon.TimeStamp = 0;
    }

    // Only JobCondition & JobExecution need to send to JobManager

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Executions - Optional (Check Only)

    jobexecfound = 0;

    childnode = DKJSN_SelectObjectByList(handle, findList10, node);

    if (childnode > 0)
    {
        childcount = DKJSN_GetObjectCount(handle, childnode);

        // NOTE: childcount SHOULD be 1
        for (i = 0; i < childcount; i++)
        {
            jobnode = DKJSN_SelectObjectByIndex(handle, childnode, i);
            if (jobnode < 0)
            {
                break;
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            // Get JobExecution

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList11, jobnode, 1);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                break;
            }

            jobexecfound = 1;
        }
    }

    if (jobtype == 3 &&
        jobexecfound == 0)
    {
        // JobCondition and JobExecution must exist at the same time when JobType value is equal to 3
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Conditions

    jobcondfound = 0;

    childnode = DKJSN_SelectObjectByList(handle, findList8, node);
    if (childnode > 0)
    {
        childcount = DKJSN_GetObjectCount(handle, childnode);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "Advanced job Conditions found: %d\n", childcount);

        for (i = 0; i < childcount; i++)
        {
            jobnode = DKJSN_SelectObjectByIndex(handle, childnode, i);
            if (jobnode < 0)
            {
                // Skip to next object in Conditions array
                continue;
            }

            jobcondfound = 1;

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            // Get JobCondition

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList9, jobnode, 1);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                continue;
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            if (jobtype == 3)
            {
                type = DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY;
            }
            else
            {
                type = DKJOBTYPE_ADVANCED_CONDITION;
            }

            joblen = sizeof(DKStandardJob_t) + len;

            //pAdvJob = (DKStandardJob_t *)dxMemAlloc(CALLOC_NAME, 1, joblen);
            pAdvJob = (DKStandardJob_t *)DKMSG_AllocMemory(joblen);
            if (pAdvJob == NULL)
            {
                ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
                break;
            }

            memcpy(pAdvJob->pPayload, szBuffer, len);

            pAdvJob->GatewayID = JobCommon.GatewayID;
            pAdvJob->ObjectID = JobCommon.ObjectID;
            pAdvJob->UserObjectID = JobCommon.UserObjectID;
            pAdvJob->TimeStamp = JobCommon.TimeStamp;

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "AdvJob, ObjectID: %u, GatewayID: %u, UserObjID: %u\n",
                             (unsigned int)pAdvJob->ObjectID, (unsigned int)pAdvJob->GatewayID, (unsigned int)pAdvJob->UserObjectID);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "AdvJob, Condition len: %u\n",
                             (unsigned int)len);
#endif

            // Send JobInformation data to worker thread
            DKServerManager_SendEventToWorkerThread(SourceOfOrigin,
                                                    TaskIdentificationNumber,
                                                    DKMSG_NEWJOBS,
                                                    type,
                                                    HTTP_OK,
                                                    DKSERVER_REPORT_STATUS_SUCCESS,
                                                    (void *)pAdvJob,
                                                    joblen);

            //dxMemFree(pAdvJob);
            DKMSG_FreeMemory((uint8_t *)pAdvJob);
            pAdvJob = NULL;
        }
    }

    if (jobtype == 3 &&
        jobcondfound == 0)
    {
        // JobCondition and JobExecution must exist at the same time when JobType value is equal to 3
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Executions
    childnode = DKJSN_SelectObjectByList(handle, findList10, node);

    if (childnode > 0)
    {
        childcount = DKJSN_GetObjectCount(handle, childnode);

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "Advanced job Executions found: %d\n", childcount);

        for (i = 0; i < childcount; i++)
        {
            jobnode = DKJSN_SelectObjectByIndex(handle, childnode, i);
            if (jobnode < 0)
            {
                // Skip to next object in Conditions array
                continue;
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            // Get JobExecution

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList11, jobnode, 1);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                continue;
            }

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            if (jobtype == 3)
            {
                type = DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY;
            }
            else
            {
                type = DKJOBTYPE_ADVANCED_EXECUTION;
            }

            joblen = sizeof(DKStandardJob_t) + len;

            pAdvJob = (DKStandardJob_t *)DKMSG_AllocMemory(joblen);
            if (pAdvJob == NULL)
            {
                ret = DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
                break;
            }

            memcpy(pAdvJob->pPayload, szBuffer, len);

            pAdvJob->GatewayID = JobCommon.GatewayID;
            pAdvJob->ObjectID = JobCommon.ObjectID;
            pAdvJob->UserObjectID = JobCommon.UserObjectID;
            pAdvJob->TimeStamp = JobCommon.TimeStamp;

#if !defined(ENABLE_LOG_INFORMATION_DETAILS)
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "AdvJob, ObjectID: %u, GatewayID: %u, UserObjID: %u\n",
                             (unsigned int)pAdvJob->ObjectID, (unsigned int)pAdvJob->GatewayID, (unsigned int)pAdvJob->UserObjectID);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "AdvJob, Execution len: %u\n",
                             (unsigned int)len);
#endif

            // Send JobInformation data to worker thread
            DKServerManager_SendEventToWorkerThread(SourceOfOrigin,
                                                    TaskIdentificationNumber,
                                                    DKMSG_NEWJOBS,
                                                    type,
                                                    HTTP_OK,
                                                    DKSERVER_REPORT_STATUS_SUCCESS,
                                                    (void *)pAdvJob,
                                                    joblen);

            //dxMemFree(pAdvJob);
            DKMSG_FreeMemory((uint8_t *)pAdvJob);
            pAdvJob = NULL;
        }
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    return ret;
}

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetNewJobs_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    ObjectDictionary_Lock ();

    if (ObjectDictionary_GetGateway () == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, ObjectDictionary_GetGateway () == NULL\n",
                         __FUNCTION__,
                         __LINE__);
        ObjectDictionary_Unlock ();
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    ObjectDictionary_Unlock ();

    if (pBuffer == NULL || cbSize <= 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, pBuffer == NULL\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    ObjectDictionary_Lock ();

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETNEWJOBS,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETNEWJOBS_BODY, (unsigned int) ObjectDictionary_GetGateway ()->ObjectID, (int)0),
            ObjectDictionary_GetSessionToken (),
            (unsigned int) ObjectDictionary_GetGateway ()->ObjectID,
            (int)0);

    ObjectDictionary_Unlock ();

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetNewJobs_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0
    uint32_t gatewayid = 0;             // A variable to store the GatewayID which convert from strtoul()


    int jobsnode = 0;                   // index of jobs node
    int childnode = 0;                  // child of peripherals node

    long handle = 0;                    // Handle of the DKJSN object
    int len = 0;

    int jobCount = 0;                   // Number of peripherals

    uint32_t nNumOfActiveUsers = 0;     // Number of active users
    uint8_t jobtype = 0;                // JobType. 1 for Standard Job; 2 for Advanced Job

    int i = 0;

    int findList2[] = {DKJSN_TOKEN_RESULTS, 0};

    int findList3[] = {DKJSN_TOKEN_GATEWAYID, 0};
    int findList5[] = {DKJSN_TOKEN_NUMBEROFACTIVEUSERS, 0};
    int findList6[] = {DKJSN_TOKEN_JOBTYPE, 0};

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get Jobs
    jobsnode = DKJSN_SelectObjectByList(handle, findList2, rootnode);
    if (jobsnode < 0)
    {
        // "Results" not found
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    jobCount = DKJSN_GetObjectCount(handle, jobsnode);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Job found: %d\n", jobCount);

    ret = DKSERVER_STATUS_SUCCESS;

    for (i = 0; i < jobCount; i++)
    {
        childnode = DKJSN_SelectObjectByIndex(handle, jobsnode, i);
        if (childnode < 0)
        {
            // Can't select the job node
            ret = DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
            break;
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get NumOfActiveUsers
        len = DKJSN_GetObjectStringLengthByList(handle, findList5, childnode);

        if (len > 0)
        {
            // Found NumOfActiveUsers
            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList5, childnode, 0);
            if (ret == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "NumOfActiveUsers: %s\n", szBuffer);

                nNumOfActiveUsers = strtoul(szBuffer, NULL, 10);

                // Send data to worker thread
                DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                                        pCommMsg->Header.TaskIdentificationNumber,
                                                        DKMSG_NUMOFACTIVEUSERS,
                                                        DKJOBTYPE_NONE,
                                                        HTTP_OK,
                                                        DKSERVER_REPORT_STATUS_SUCCESS,
                                                        (void *)&nNumOfActiveUsers,
                                                        sizeof(nNumOfActiveUsers));
            }
        }

        //-----------------------------------------------------------------
        //-----------------------------------------------------------------

        // Get JobType

        jobtype = 0;

        len = DKJSN_GetObjectStringLengthByList(handle, findList6, childnode);

        if (len > 0)
        {
            // Found JobType
            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList6, childnode, 0);
            if (ret == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "JobType: %s\n", szBuffer);

                jobtype = strtoul(szBuffer, NULL, 10);
            }
        }

        if (jobtype == 2 ||
            jobtype == 3)
        {
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            // Get GatewayID

            len = STRING_BUFFER_SIZE;
            ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList3, childnode, 0);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "[%s] Line %d, JSON key %d not found\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 findList3[0]);
                // Skip to next object in Conditions array
                continue;
            }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "GatewayID: %s\n", szBuffer);
#endif
            gatewayid = strtoul(szBuffer, NULL, 10);

            ObjectDictionary_Lock ();

            if (gatewayid == 0 ||
                gatewayid != ObjectDictionary_GetGateway ()->ObjectID)
            {
                ObjectDictionary_Unlock ();

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "[%s] Line %d, Invalid GatewayID = %u\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 (unsigned int)gatewayid);

                continue;
            }

            ObjectDictionary_Unlock ();

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------

            // Advanced Job body
            ret = _ParseAdvancedJobJSON(handle, childnode, jobtype, pCommMsg->Header.SourceOfOrigin, pCommMsg->Header.TaskIdentificationNumber);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                // Error
            }

            continue;       // Skip to next record
        }
        else if (jobtype == 1)
        {
            // For backward compatible
            // According to v0.141 and before, there is no "JobType" element in JSON data.
            // We must use "JobInformation" to identify it is a Standard Job or not.

            ret = _ParseStandardJobJSON(handle, childnode, pCommMsg->Header.SourceOfOrigin, pCommMsg->Header.TaskIdentificationNumber);
            if (ret != DKSERVER_STATUS_SUCCESS)
            {
                // Error
            }

            ret = DKSERVER_STATUS_SUCCESS;

            continue;       // Skip to next record
        }
        else
        {
            // There is no need to handle backward compatible format due to CloudServer already send "JobType" data
            // By David Tang 2017/04/14
            //
        }
    }

    DKJSN_Uninit(handle);

    if (ret == DKSERVER_STATUS_SUCCESS)
    {
        ret = DKSERVER_REPORT_STATUS_SUCCESS_NO_EVENT;
    }

    return ret;
}
