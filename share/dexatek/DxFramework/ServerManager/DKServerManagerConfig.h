//============================================================================
// File: DKServerManagerConfig.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/09/26
//     1) Description: Initial
//============================================================================
#ifndef _DKSERVERMANAGERCONFIG_H
#define _DKSERVERMANAGERCONFIG_H

#pragma once

#include "dxOS.h"
#include "ProjectCommonConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

#define SRVM_THREAD_QUEUE_NAME                          "DKSMQ"
#define SRVM_THREAD_QUEUE_MAX_ALLOWED                   20

#define SRVM_THREAD_WORKER_THREAD_STACK_SIZE            (2 * 640)
#define SRVM_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE       MAXIMUM_DEVICE_KEEPER_ALLOWED

#define SRVM_THREAD_POP_QUEUE_TIMEOUT_TIME              1000
#define SRVM_THREAD_REQUEST_JOB_TIME_INTERVAL_DEFAULT   3000
#define SRVM_THREAD_REQUEST_LONG_POOL_INTERVAL          5000

#define SRVM_THREAD_HTTP_TIMEOUT_TIME                   30000   // 30 seconds
#define TCP_CONNECT_TIMEOUT                             30      // 30 seconds
#define SRVM_THREAD_BUFFER_SIZE                         4096
#define DKSM_USE_PORT                                   HTTPS_PORT


#define HTTP_THREAD_PRIORITY                        (DXTASK_PRIORITY_HIGH)
#define HTTP_THREAD_NAME                            "HT"
#define HTTP_THREAD_STACK_SIZE                      (4 * 1024)

#define HTTP_THREAD_QUEUE_NAME                      "HTQ"
#define HTTP_THREAD_QUEUE_MAX_ALLOWED               2

#define HTTP_THREAD_WORKER_THREAD_STACK_SIZE        1280 //2 * 1024
#define HTTP_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE   3

#define THREAD_POP_QUEUE_TIMEOUT_TIME               1000
//#define TCP_RECEIVE_TIMEOUT                         3000    // 3 seconds

#pragma message ( "WARNING: We increase the size to 36KB to support large container and peripherals. This is temporarily only as our data retrive mechanism need revise. " )
#define HTTP_BUFFER_SIZE                            36 * 1024 //4 * 1024

#ifdef __cplusplus
}
#endif

#endif // _DKSERVERMANAGERCONFIG_H

