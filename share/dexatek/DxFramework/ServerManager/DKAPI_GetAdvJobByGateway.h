//============================================================================
// File: DKAPI_GetAdvJobByGateway.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.171.0
//     Date: 2015/07/21
//     1) Description: Support advanced job single gateway pack
//        Added:
//            - Added a new function _SendAdvJobToMain()
//        Modified:
//            - Changed filename to DKAPI_GetAdvJobByGateway.c
//            - Changed function name to Build_GetAdvJobByGateway_Data()
//            - Changed function name to Parse_GetAdvJobByGateway_Response()
//
//     Version: 0.171.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DKAPI_GETADVJOBBYGATEWAY_H
#define _DKAPI_GETADVJOBBYGATEWAY_H

#pragma once

#include "dxOS.h"

#include "ObjectDictionary.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/


/******************************************************
 *               Function Definitions
 ******************************************************/
int Build_GetAdvJobByGateway_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen);
int Parse_GetAdvJobByGateway_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen);

#ifdef __cplusplus
}
#endif

#endif // _DKAPI_GETADVJOBBYGATEWAY_H
