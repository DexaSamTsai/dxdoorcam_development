//============================================================================
// File: DKAPI_GetCurrentUser.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.242
//     Date: 2017/04/19
//     1) Description: Support GetCurrentUser API
//
//============================================================================
#include <stdlib.h>
#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_GetCurrentUser.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_GETCURRENTUSER               "%s "                                               \
                                                    "%s/GetCurrentUser.php HTTP/1.1\r\n"                \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/
extern int DKMSG_ParseDataContainer(dxPTR_t handle, int node, DKRESTHeader_t *pCommMsgHeader);

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_GetCurrentUser_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    if (pParameters == NULL ||
        paramlen != sizeof (DKRESTCommMsg_t) ||
        paramlen == 0 ||
        paramlen > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_GETCURRENTUSER,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            0,                              // Content-Length
            ObjectDictionary_GetSessionToken ());

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_GetCurrentUser_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    int rootnode = 0;                   // rootnode, must be 0

    int resultsnode = 0;                // index of results node

    dxPTR_t handle = 0;                 // Handle of the DKJSN object
    int len = 0;

    CurrentUserInfo_t *pUserInfo = NULL;// Current user information

    int findList1[] = {DKJSN_TOKEN_RESULTS, 0};
    int findList2[] = {DKJSN_TOKEN_PREFERLANG, 0};

    if (pParameters == NULL ||
        paramlen != sizeof (DKRESTCommMsg_t) ||
        paramlen == 0 ||
        paramlen > cbSize)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Invalid pParameters\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (strlen((char *)pMsg) <= 0 || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    handle = DKJSN_Init((char *)pMsg, commonTokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0)
    {
        return DKSERVER_SYSTEM_ERROR_JSON_INIT;
    }

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get "results"
    resultsnode = DKJSN_SelectObjectByList(handle, findList1, rootnode);
    if (resultsnode < 0)
    {
        // "results" not found
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList1[0]);
        DKJSN_Uninit(handle);
        return DKSERVER_SYSTEM_ERROR_JSON_KEY_NOT_FOUND;
    }

    ret = DKMSG_ParseDataContainer(handle,
                                   resultsnode,
                                   &pCommMsg->Header);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Get PreferLang
    len = STRING_BUFFER_SIZE;
    ret = DKMSG_GetJSONData(szBuffer, &len, handle, findList2, resultsnode, 0);
    if (ret != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, JSON key %d not found\n",
                         __FUNCTION__,
                         __LINE__,
                         findList2[0]);
        DKJSN_Uninit(handle);
        return ret;
    }

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "PreferLang: %s\n", szBuffer);
#endif

    pUserInfo = (CurrentUserInfo_t *)DKMSG_AllocMemory(sizeof(CurrentUserInfo_t));
    if (pUserInfo == NULL)
    {
        return DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY;
    }

    if (len >= sizeof(pUserInfo->PreferLang))
    {
        DKMSG_FreeMemory((uint8_t *)pUserInfo);
        pUserInfo = NULL;
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    memcpy(pUserInfo->PreferLang, szBuffer, len);

    //-----------------------------------------------------------------
    //-----------------------------------------------------------------

    // Send CurrentUserInfo_t to worker thread

    DKServerManager_SendEventToWorkerThread(pCommMsg->Header.SourceOfOrigin,
                                            pCommMsg->Header.TaskIdentificationNumber,
                                            DKMSG_GETCURRENTUSER,
                                            DKJOBTYPE_NONE,
                                            HTTP_OK,
                                            DKSERVER_REPORT_STATUS_SUCCESS,
                                            (void *)pUserInfo,
                                            len);
    DKMSG_FreeMemory((uint8_t *)pUserInfo);
    pUserInfo = NULL;

    DKJSN_Uninit(handle);

    return ret;
}
