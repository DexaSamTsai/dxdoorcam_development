//============================================================================
// File: DKAPI_SendMessage.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.2
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.3
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - Build_SendNotificationMessage_Data()
//
//     Version: 0.4
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Modified:
//            - Build_SendNotificationMessage_Data()
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_SendMessage.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_SENDNOTIFYMESSAGE_BODY       "{\"SendToSpecificUser\":%d,\"MSG\":\"%s\"}"
#define DKMSG_TEMPLATE_SENDNOTIFYMESSAGE            "%s "                                               \
                                                    "%s/SendMessage.php HTTP/1.1\r\n"                   \
                                                    DKMSG_TEMPLATE_HEADER_2                             \
                                                    "\r\n"                                              \
                                                    DKMSG_TEMPLATE_SENDNOTIFYMESSAGE_BODY

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_SendNotificationMessage_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = (DKRESTCommMsg_t *)pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail

    if (pParameters == NULL ||
        pCommMsg->pData == NULL ||
        paramlen < (sizeof (int) + sizeof (DKRESTCommMsg_t)))             // int => MessageLen data member of structure NotificationMessage_t
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Notification information is incorrect\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    if (pBuffer == NULL || cbSize <= 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset(pBuffer, 0x00, cbSize);

    NotificationMessage_t *pNotifyMessage = (NotificationMessage_t *)pCommMsg->pData;

    n = snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_SENDNOTIFYMESSAGE,
            "POST",
            ObjectDictionary_GetApiVersion (),
            ObjectDictionary_GetHostname (),
            ObjectDictionary_GetApplicationId (),
            ObjectDictionary_GetApiKey (),
#if CONFIG_HEADER_X_DK_STATE
            DKMSG_GetSeqNo(),
#endif // CONFIG_HEADER_X_DK_STATE
            snprintf((char *)pBuffer, cbSize, DKMSG_TEMPLATE_SENDNOTIFYMESSAGE_BODY, 0, (char *)pNotifyMessage->pMessageData),
            ObjectDictionary_GetSessionToken (),
            0,
            (char *)pNotifyMessage->pMessageData);

    if (n <= 0 || n >= cbSize)
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                         __FUNCTION__,
                         __LINE__,
                         n,
                         cbSize);
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                         "Length: %d\n", strlen((char *)pBuffer));
#endif
    }

    return ret;
}

int Parse_SendNotificationMessage_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    int ret = DKSERVER_STATUS_SUCCESS;  // 0: Success; Otherwise, Fail
    return ret;
}
