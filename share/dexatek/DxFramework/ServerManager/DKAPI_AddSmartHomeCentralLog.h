//============================================================================
// File: DKAPI_AddSmartHomeCentralLog.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.176
//     Date: 2015/09/10
//     1) Description: Support AddSmartHomeCentralLog API
//        Added: Build_AddSmartHomeCentralLog_Data()
//               Parse_AddSmartHomeCentralLog_Response()
//
//     Version: 0.176.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//============================================================================
#ifndef _DKAPI_ADDSMARTHOMECENTRALLOG_H
#define _DKAPI_ADDSMARTHOMECENTRALLOG_H

#pragma once

#include "dxOS.h"

#include "ObjectDictionary.h"

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/


/******************************************************
 *               Function Definitions
 ******************************************************/
int Build_AddSmartHomeCentralLog_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen);
int Parse_AddSmartHomeCentralLog_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen);

#ifdef __cplusplus
}
#endif

#endif // _DKAPI_ADDSMARTHOMECENTRALLOG_H
