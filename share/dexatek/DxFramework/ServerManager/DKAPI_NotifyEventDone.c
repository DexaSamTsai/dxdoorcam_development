//============================================================================
// File: DKAPI_NotifyEventDone.c
//
// Author: Allen Liao
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include <stdlib.h>

#include "Utils.h"
#include "dk_hex.h"
#include "DKServerManager.h"
#include "HTTPManager.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dk_url.h"
#include "dkjsn.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"
#include "DKAPI_NotifyEventDone.h"

/******************************************************
 *                    Constants
 ******************************************************/
#define DKMSG_TEMPLATE_NOTIFYEVENTDONE_API                                                           \
                                                    "%s "                                               \
                                                    "%s/NotifyEventDone.php HTTP/1.1\r\n"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

/******************************************************
 *               Function Implementation - Local
 ******************************************************/

/******************************************************
 *               Function Implementation - Public
 ******************************************************/
int Build_NotifyEventDone_Data(uint8_t *pBuffer, int cbSize, uint8_t *pParameters, int paramlen)
{
    DKRESTCommMsg_t *pCommMsg = ( DKRESTCommMsg_t * ) pParameters;
    int n = 0;
    int ret = DKSERVER_STATUS_SUCCESS;      // 0: Success; Otherwise, Fail
    int ContentLengthPosition = 0;          // Pointer to 0000 of "Content-Length: 0000\n"
    int JSONDataPosition = 0;               // Pointer to "{\"Data\":..."
    int i = 0;
    char *p = NULL;

    if ( pParameters == NULL ||
         pCommMsg->pData == NULL ||
         paramlen == 0 ||
         paramlen >= cbSize ||
         paramlen < sizeof( DKRESTCommMsg_t ) )
    {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, Invalid pParameters\n",
                          __FUNCTION__,
                          __LINE__ );
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    /**
     * +------+-+----------+-+-+
     * | Type |0| resource |0|0|
     * +------+-+----------+-+-+
     */

    if ( pBuffer == NULL || cbSize <= 0 )
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }
    memset( pBuffer, 0x00, cbSize );

    p = ( char * ) pCommMsg->pData;

#if defined(ENABLE_LOG_INFORMATION_DETAILS)
    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                      "Total data length: %d\n", paramlen );
#endif
    sprintf( ( char * ) pBuffer, DKMSG_TEMPLATE_NOTIFYEVENTDONE_API, "POST", ObjectDictionary_GetApiVersion() );
    strcat( ( char * ) pBuffer, "Host: ");
    strcat( ( char * ) pBuffer, ObjectDictionary_GetHostname() );
    strcat( ( char * ) pBuffer, "\r\nX-DK-Application-Id: ");
    strcat( ( char * ) pBuffer, ObjectDictionary_GetApplicationId() );
    strcat( ( char * ) pBuffer, "\r\nX-DK-API-Key: ");
    strcat( ( char * ) pBuffer, ObjectDictionary_GetApiKey() );
#if CONFIG_HEADER_X_DK_STATE
    n = strlen( ( char * ) pBuffer);
    sprintf( ( char * ) &pBuffer[ n ], "\r\nX-Dk-State: %d", DKMSG_GetSeqNo() );
#endif // CONFIG_HEADER_X_DK_STATE
    strcat( ( char * ) pBuffer, "\r\nX-DK-Session-Token: " );
    strcat( ( char * ) pBuffer, ObjectDictionary_GetSessionToken() );
    strcat( ( char * ) pBuffer, "\r\nContent-Length: " );
    ContentLengthPosition = strlen( ( char * ) pBuffer );
    strcat( ( char * ) pBuffer, "0000\r\n\r\n" );
    JSONDataPosition = strlen( ( char * ) pBuffer );

	strcat( ( char * ) pBuffer, "{" );

    i = 0;

    // #1: Type
    int Type = 0;
    memcpy( &Type, &p[ i ], sizeof( Type ) );

    n = strlen( ( char * ) pBuffer );
    sprintf( ( char * ) &pBuffer[ n ], "\"Type\":%d", Type );
    i = i + sizeof( Type ) + 1;                     // Move p to next field

    // #2: Resource
    if ( p[ i ] > 0 )
    {
        int ResourceLen = strlen( &p[ i ] );
        if ( ResourceLen <= 0 )
        {
            return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        }

        n = strlen( ( char * ) pBuffer );

        if ( n + ResourceLen >= cbSize )
        {
            return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        }
		
		sprintf( ( char * ) &pBuffer[ n ], ",\"Resource\":\"%s\"", &p[ i ] );

        i += strlen( &p[ i ] );                     // Move p to next field
    }
    i++;                                            // Must skip 0x00 NULL terminated char

	strcat( ( char * ) pBuffer, "}" );

	// Overwrite Content-Length
    n = strlen( ( char * ) pBuffer );
    if ( n > cbSize )
    {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, Require %d bytes, but the buffer is %d bytes\n",
                          __FUNCTION__,
                          __LINE__,
                          n,
                          cbSize );
        return DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
    }

    n = strlen( ( char * ) &pBuffer[ JSONDataPosition ] );

    for ( i = 4; i > 0; i-- )
    {
        pBuffer[ ContentLengthPosition + i - 1 ] = '0' + ( n % 10 );
        n /= 10;
    }

    // Dump the result
    n = strlen( ( char * ) pBuffer );

    if ( n <= 0 || n >= cbSize )
    {
        ret = DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL;
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "[%s] Line %d, Buffer not enough! require %d bytes, but current %d bytes\n",
                          __FUNCTION__,
                          __LINE__,
                          n,
                          cbSize );
    }
    else
    {
#if defined(ENABLE_LOG_INFORMATION_DETAILS)
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                          "Length: %d\n", strlen( ( char * ) pBuffer ) );
#endif
    }

    return ret;
}

int Parse_NotifyEventDone_Response(uint8_t *pMsg, int cbSize, uint8_t *pParameters, int paramlen, uint8_t *optionData, int optionDataLen)
{
    return DKSERVER_STATUS_SUCCESS;
}
