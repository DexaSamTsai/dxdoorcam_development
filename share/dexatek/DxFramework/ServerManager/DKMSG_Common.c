//============================================================================
// File: DKMSG_Common.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.142.3
//     Date: 2015/05/28
//     1) Description: Fixed issue "Log Message Queue Full"
//        Modified:
//            - DK_PRINTLN_128()
//
//     Version: 0.142.4
//     Date: 2016/01/08
//     1) Description: Fixed issue "Log Message Queue Full"
//        Modified:
//            - Change name DK_PRINTLN_128() to DK_PRINTLN_ALL()
//        Added:
//            - DK_PRINTLN()
//
//     Version: 0.142.5
//     Date: 2016/01/11
//     1) Description: Fixed issue "Log Message Queue Full"
//        Modified:
//            - Change name of DK_HEX_DUMP() to DK_PRINTLN_HEX()
//            - DK_PRINTLN_HEX()
//
//     Version: 0.142.6
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.142.7
//     Date: 2016/02/19
//     1) Description: Revise function for better understanding
//        Modified:
//            - DK_PRINTLN()
//
//     Version: 0.142.8
//     Date: 2016/03/01
//     1) Description: Revise DKServer_StatusCode_t to DKServer_SystemErrorCode_t
//        Modified:
//            - DKMSG_GetJSONData()
//
//     Version: 0.142.9
//     Date: 2016/03/07
//     1) Description: Change function name from DK_PRINTLN_HEX to DK_PRINT_HEX
//        Modified:
//            - DK_PRINT_HEX()
//
//     Version: 0.142.10
//     Date: 2016/05/05
//     1) Description: Added optional X-Dk-State information in HTTP Header
//        Added:
//            - DKMSG_GetSeqNo()
//        Modified:
//            - DKMSG_TEMPLATE_HEADER_1
//
//     Version: 0.142.11
//     Date: 2016/07/04
//     1) Description: Fixed a potential infinite loop issue
//        Modified:
//            - DKQ_Count()
//     2) Description: Fixed memory leak issue
//        Modified:
//            - DKQ_Pop()
//
//     Version: 0.142.12
//     Date: 2017/03/27
//     1) Description: Fixed large JobInformation issue
//        Modified:
//            - szBuffer[]
//
//     Version: 0.142.13
//     Date: 2017/06/29
//     1) Description: Added debug log information when fatal issue occurred
//        Modified:
//            - DKMSG_AllocMemory()
//            - DKQ_Push()
//
//     Version: 0.142.14
//     Date: 2017/09/11
//     1) Description: Support URL encode
//        Added:
//            - DKMSG_URLEncode()
//
//     Version: 0.142.15
//     Date: 2018/01/16
//     1) Description: Fixed issues while DownloadFile due to incorrect HTTP request body. (Garbage characters was appeneded to HTTP request body)
//        Modified:
//            - DKQ_Push()
//============================================================================
#include <stdlib.h>
#include <inttypes.h>
#include "dk_hex.h"
#include "Utils.h"
#include "ObjectDictionary.h"
#include "dk_http.h"
#include "dkjsn.h"
#include "DKServerManager.h"
#include "dk_Network.h"
#include "dk_url.h"
#include "dxSysDebugger.h"
#include "DKMSG_Common.h"


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/
#define DKTHREAD_NAME                               "DKCM"
//#define DKMSG_ENABLE_DUMP

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/******************************************************
 *               Variable Definitions
 ******************************************************/

DKAPIQueueNode_t        *pDKQHead = NULL;                               // Use for DKRESTCommMsg_t Queue
DKAPIQueueNode_t        *pDKQTail = NULL;                               // Use for DKRESTCommMsg_t Queue
char* szBuffer = NULL;                   // Use for 1) string conversion
char* szBuffer2 = NULL;              // Use for 1) string conversion
jsmntok_t* commonTokenTable = NULL;


/******************************************************
 *               Function Definitions
 ******************************************************/

int DKCommon_Init (void) {
    szBuffer = dxMemAlloc (DK_MEM_ZONE_SLOW, STRING_BUFFER_SIZE, sizeof (char));
    if (szBuffer == NULL) {
        goto fail_create_buffer;
    }

    szBuffer2 = dxMemAlloc (DK_MEM_ZONE_SLOW, STRING_BUFFER_SIZE + 1, sizeof (char));
    if (szBuffer2 == NULL) {
        goto fail_create_buffer2;
    }

    commonTokenTable = (jsmntok_t*) dxMemAlloc (DK_MEM_ZONE_SLOW, MAX_NUM_JSON_ELEMENT_SUPPORT, sizeof (jsmntok_t));
    if (commonTokenTable == NULL) {
        goto fail_create_token;
    }

    // Empty DKServer command Queue
    DKQ_Empty();

    return 0;

fail_create_token :

    dxMemFree (szBuffer2);

fail_create_buffer2 :

    dxMemFree (szBuffer);

fail_create_buffer :

    return -1;
}


void DK_PRINTLN(char *p, int len)
{
    char szPrintBuff[64];
    int i = 0;
    int n = 0;
    int buffersize = (sizeof(szPrintBuff) < LOG_MAX_CHAR_BUFFER) ? sizeof(szPrintBuff) : LOG_MAX_CHAR_BUFFER;
    int datalen = buffersize - 1;               // Reserved 1 byte for NULL character

    if (len <= 0) return ;

    n = (len + datalen - 1) / datalen;

    dxTimeDelayMS(100);                         // Waiting for log queue be clear

    for (i = 0; i < n; i++)
    {
        if ((i % 10) == 0)
        {
            dxTimeDelayMS(100);                 // Waiting for log queue be clear
        }

        int j = datalen * i;

        memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        memcpy(szPrintBuff,
               &p[j],
               ((len - j) < datalen) ? (len - j) : datalen);

        G_SYS_DBG_LOG_INV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "%s", szPrintBuff);
    }
    G_SYS_DBG_LOG_INV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR,
                      "\n");
}

void DK_PRINTLN_ALL(char *p)
{
    DK_PRINTLN(p, strlen(p));
}

void DK_PRINT_HEX(uint8_t *pData, int len)
{
#ifdef DKMSG_ENABLE_DUMP
    char szPrintBuff[64];
    int i = 0;
    memset(szPrintBuff, 0x00, sizeof(szPrintBuff));

    while (i < len)
    {
        sprintf(&szPrintBuff[strlen(szPrintBuff)], "%02X", (pData[i] & 0x0FF));
        i++;
        if (((i % 16) == 0) ||
            (i == len))
        {
            strcat(szPrintBuff, "\n");

            dxTimeDelayMS(20);

            G_SYS_DBG_LOG_INV(G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                              "%s", szPrintBuff);
            memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        }
        else
        {
            strcat(szPrintBuff, " ");
        }
    }
#endif
}

void DKMSG_CopyToMemory(char *pD, int cbOutSize, char *pS, int cbInSize)
{
    memset(pD, 0x00, cbOutSize);

    if (pD == NULL ||
        pS == NULL ||
        cbInSize <= 0 ||
        cbOutSize == 0 ||
        cbOutSize < cbInSize)
    {
        return ;
    }

    memcpy(pD, pS, cbInSize);

    // If there is an another space, put a NULL character into it.
    if (cbInSize < cbOutSize)
    {
        pD[cbInSize] = 0x00;
    }

}

int DKMSG_GetJSONData(char *buffer, int *size, dxPTR_t handle, int *jsonpath, int node, int decode)
{
    int len = 0;                        // length of node

    if (buffer == NULL ||
        size == NULL ||
        *size  == 0 ||
        handle == 0 ||
        jsonpath == NULL ||
        node < 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER - parameter itself is invalid\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Get JSON value
    len = DKJSN_GetObjectStringLengthByList(handle, jsonpath, node);
    if (len == 0)
    {
        return DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER;
    }

    // Get data from JSON
    memset(buffer, 0x00, *size);
    DKMSG_CopyToMemory(buffer, *size, DKJSN_GetObjectStringByList(handle, jsonpath, node), len);

#if defined(ENABLE_SHOW_OBJECTS1)
    G_SYS_DBG_LOG_V("JSON data: %s\n", buffer);
#endif

    if (decode == 1)
    {
        // Remove JSON escape character
        buffer[len] = 0x00;
        len = DKMSG_JSONRemoveEscapeChar((uint8_t *)buffer, *size, (uint8_t *)buffer);

        // base64 decode
        len = b64_pton(buffer, (uint8_t *)buffer, len);
        if (len <= 0)
        {
            G_SYS_DBG_LOG_V("%s b64_pton(): %d\n", __FUNCTION__, len);
        }
    }

    if (len <= 0)
    {
        *size = 0;
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, DKSERVER_SYSTEM_ERROR_BASE64_URL_ENCODE_DECODE\n",
                         __FUNCTION__,
                         __LINE__);
        return DKSERVER_SYSTEM_ERROR_BASE64_URL_ENCODE_DECODE;
    }

    // If buffer is enough for a additional null character, added it.
    if (len < *size)
    {
        buffer[len] = 0x00;
    }

    *size = len;                        // return the length of data

    return DKSERVER_STATUS_SUCCESS;
}

int DKMSG_JSONRemoveEscapeChar(uint8_t *pBuffer, int cbBuffSize, uint8_t *pszEscape)
{
    int i = 0;                      // index of pszEscape
    int j = 0;                      // index of pBuffer
    int k = 0;
    int srclen = 0;                 // length of pszEscape
    uint8_t ch = 0;
    int value = 0;

    if (pBuffer == NULL ||
        pszEscape == NULL ||
        cbBuffSize <= 0) return 0;

    srclen = strlen((char *)pszEscape);

    i = 0;
    j = 0;
    while (i < srclen)
    {
        ch = pszEscape[i];

        if (ch == '\\')
        {
            switch(pszEscape[i + 1])
            {
                case '\"':
                case '\\':
                case '/':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = pszEscape[i + 1];
                    i += 2;
                    break;
                case 'b':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = 0x08;
                    i += 2;
                    break;
                case 'f':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = 0x0c;
                    i += 2;
                    break;
                case 'n':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = 0x0a;
                    i += 2;
                    break;
                case 'r':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = 0x0d;
                    i += 2;
                    break;
                case 't':
                    if (i + 1 >= srclen) break;
                    pBuffer[j++] = 0x09;
                    i += 2;
                    break;
                case 'u':
                    if (i + 1 + 4 >= srclen) break;         //\u0000
                    value = 0;
                    for (k = 0; k < 4; k++)
                    {
                        ch = pszEscape[i + 1 + k];

                        if (ch >= '0' && ch <= '9')
                        {
                            ch -= '0';
                        }
                        else if (ch >= 'A' && ch <= 'F')
                        {
                            ch -= 'A' + 10;
                        }
                        else if (ch >= 'a' && ch <= 'f')
                        {
                            ch -= 'a' + 10;
                        }
                        else
                        {
                            break;
                        }

                        value = (value << 4) | ch;
                    }

                    if (value <= 255)
                    {
                        // unicode code start from 256 are not support
                        pBuffer[j++] = (value & 0x0FF);
                    }
                    i += (2 + k);
                    j++;
                    break;
                default:
                    // character not in specification, skip it.
                    i++;
                    break;
            }
        }
        else
        {
            pBuffer[j] = ch;
            j++;
            i++;
        };
    }

    if (j + 1 <= cbBuffSize)
    {
        pBuffer[j] = 0x00;          // Append 0x00
    }
    return j;
}

/**
 * @return 0 ERROR
 *         > 0 Data length in pDstBuff
 */
int DKMSG_URLEncode(uint8_t *pDstBuff, int cbDstBuffSize, uint8_t *pSrcBuff)
{
    if (pSrcBuff == NULL)
    {
        return 0;
    }

    int i = 0;
    int j = 0;
    int cbSrcBuffDataLen = strlen((char *)pSrcBuff);

    for (i = 0; i < cbSrcBuffDataLen; i++)
    {
        char ch = pSrcBuff[i];

        if (isalnum(ch) > 0 ||
            ch == '-' ||
            ch == '_' ||
            ch == '.' ||
            ch == '~')
        {
            if (pDstBuff &&                 // if pDstBuff is NULL, j will be the length of url-encoding string
                (j + 1) >= cbDstBuffSize)
            {
                j = 0;
                break;
            }

            if (pDstBuff)
            {
                pDstBuff[j] = ch;
            }
            j++;
        }
        else
        {
            // Why 3 ? due to the character should be encoded as Percent-encoding format
            if (pDstBuff &&                 // if pDstBuff is NULL, j will be the length of url-encoding string
                (j + 3) >= cbDstBuffSize)
            {
                j = 0;
                break;
            }

            char c1 = ((ch >> 4) & 0x0F);
            if (c1 > 9)
            {
                c1 += 7;
            }

            char c2 = (ch & 0x0F);
            if (c2 > 9)
            {
                c2 += 7;
            }

            if (pDstBuff)
            {
                pDstBuff[j] = '%';
                pDstBuff[j + 1] = c1 + '0';
                pDstBuff[j + 2] = c2 + '0';
            }
            j += 3;
        }
    }

    // Append 0x00 character to buffer if there is enough space
    if (pDstBuff && j > 0 && j < cbDstBuffSize)
    {
        pDstBuff[j + 1] = 0x00;
    }

    return j;
}

uint8_t *DKMSG_AllocMemory(size_t size)
{
    uint8_t *pMemBuffer = NULL;

    pMemBuffer = (uint8_t *)dxMemAlloc(CALLOC_NAME, 1, size * sizeof(uint8_t));

    if (pMemBuffer == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)size);
    }

#if defined(ENABLE_MALLOC_FREE_DIAGNOSTIC)
    G_SYS_DBG_LOG_V(">>>> Line %d, dxMemAlloc(%d): 0x%X\n", __LINE__, (int)size, (unsigned int)pMemBuffer);
#endif // ENABLE_MALLOC_FREE_DIAGNOSTIC

    return pMemBuffer;
}

void DKMSG_FreeMemory(uint8_t *pMemBuffer)
{
#if defined(ENABLE_MALLOC_FREE_DIAGNOSTIC)
    G_SYS_DBG_LOG_V(">>>> Line %d, free(0x%X)\n", __LINE__, (unsigned int)pMemBuffer);
#endif // ENABLE_MALLOC_FREE_DIAGNOSTIC

    dxMemFree(pMemBuffer);
}

int DKMSG_ConvertToDKTime(uint8_t *pszDateTime, DKTime_t *dkTime)
{
    int year = 0;
    int month = 0;
    int day = 0;
    int dayofweek = 0;
    int hour = 0;
    int min = 0;
    int sec = 0;

    int ret = 0;

    if (pszDateTime == NULL || dkTime == NULL)
    {
        return 0;
    }

    if (strlen((char *)pszDateTime) != 22)  // DateTime format => "2, 30 09 2014 06:21:55"
    {
        return 0;
    }

    if (isdigit(pszDateTime[0]) == 0 ||
        pszDateTime[1] != ',' ||
        pszDateTime[2] != ' ')
    {
        return 0;
    }

    if (sscanf((char *)pszDateTime, "%d,%d%d%d%d:%d:%d", &dayofweek, &day, &month, &year, &hour, &min, &sec) != 7)
    {
        return 0;
    }

    if ((dayofweek > 0 && dayofweek < 8) &&
        (year >= 1970) &&
        (month > 0 && month < 13) &&
        (day > 0 && day < 32) &&
        (hour >= 0 && hour < 24) &&
        (min >= 0 && min < 60) &&
        (sec >= 0 && sec < 60))
    {
        dkTime->year = year & 0x0FFFF;
        dkTime->month = month & 0x0FF;
        dkTime->day = day & 0x0FF;
        dkTime->hour = hour & 0x0FF;
        dkTime->min = min & 0x0FF;
        dkTime->sec = sec & 0x0FF;
        dkTime->dayofweek = dayofweek & 0x0FF;
        ret = 1;
    }

    return ret;
}

/**
 * Use for DKRESTCommMsg_t Queue
 */
void DKQ_Empty(void)
{
    DKAPIQueueNode_t *p = NULL;

    p = pDKQHead;
    while (p)
    {
        DKRESTCommMsg_t *q = p->pRawCommand;

        if (q)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "Line %d, [%s] MType: %d\n",
                             __LINE__,
                             __FUNCTION__,
                             q->Header.MessageType);

            if (q->pData)
            {
                dxMemFree(q->pData);
                q->pData = NULL;
            }

            dxMemFree(q);
            q = NULL;
        }

        //p = p->Next;
        pDKQHead = p->Next;
        dxMemFree(p);
        p = pDKQHead;
    }

    pDKQHead = NULL;
    pDKQTail = NULL;
}

uint8_t DKQ_IsEmpty(void)
{
    uint8_t ret = 1;

    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);
    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQTail: %X\n", __LINE__, (unsigned int)pDKQTail);

    if (pDKQHead && pDKQTail)
    {
        ret = 0;
    }

    return ret;
}

void DKQ_Push(DKRESTCommMsg_t *pCommand)
{
    DKRESTCommMsg_t *q = (DKRESTCommMsg_t *)dxMemAlloc(DKTHREAD_NAME, 1, sizeof(DKRESTCommMsg_t));

    if (q)
    {
        memcpy(&q->Header, &pCommand->Header, sizeof(pCommand->Header));

        if (pCommand->pData && pCommand->Header.DataLen > 0)
        {
            // NOTE: Appened a null-terminated character to avoid issues.
            //
            //       In some case, we may use sprintf() as well as pCommand->pData to build HTTP request.
            //       If value of pCommand->Header.DataLen is multiple of 32, no 0x00 character will be appended.
            //       That will be a problem.
            q->pData = (uint8_t *)dxMemAlloc(DKTHREAD_NAME, 1, pCommand->Header.DataLen + 1);
            if (q->pData == NULL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 pCommand->Header.DataLen);

                dxMemFree(q);
                q = NULL;
                return ;
            }

            memcpy(q->pData, pCommand->pData, pCommand->Header.DataLen);
        }

        DKAPIQueueNode_t *pNode = (DKAPIQueueNode_t *)dxMemAlloc(DKTHREAD_NAME, 1, sizeof(DKAPIQueueNode_t));
        if (pNode == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                             __FUNCTION__,
                             __LINE__,
                             (int)sizeof(DKAPIQueueNode_t));

            dxMemFree(q);
            q = NULL;
            return ;
        }

        pNode->pRawCommand = q;

        if (pDKQTail == NULL)
        {
            pDKQTail = pNode;
            pDKQTail->Next = NULL;
            pDKQHead = pNode;
            pDKQHead->Next = NULL;
        }
        else
        {
            pDKQTail->Next = pNode;
            pDKQTail = pNode;
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, dxMemAlloc(%d) == NULL\n",
                         __FUNCTION__,
                         __LINE__,
                         (int)sizeof(DKRESTCommMsg_t));
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "Line %d, [%s] MType: %d\n",
                     __LINE__,
                     __FUNCTION__,
                     pCommand->Header.MessageType);

    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);
    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);
    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQTail: %X\n", __LINE__, (unsigned int)pDKQTail);
}

uint8_t DKQ_Pop(DKRESTCommMsg_t **pCommand)
{
    uint8_t ret = 0;
    DKAPIQueueNode_t *pNode = NULL;

    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);

    if (pDKQHead)
    {
        if (pDKQHead == pDKQTail)
        {
            pNode = pDKQHead;
            pDKQHead = NULL;
            pDKQTail = NULL;
            pNode->Next = NULL;
        }
        else
        {
            pNode = pDKQHead;
            pDKQHead = pDKQHead->Next;
            pNode->Next = NULL;
        }

        if (pCommand == NULL)
        {
            //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);
            if (pNode->pRawCommand)
            {
                if (pNode->pRawCommand->pData)
                {
                    dxMemFree(pNode->pRawCommand->pData);
                    pNode->pRawCommand->pData = NULL;
                }

                dxMemFree(pNode->pRawCommand);
                pNode->pRawCommand = NULL;
            }

            dxMemFree(pNode);
            pNode = NULL;

            //G_SYS_DBG_LOG_V(">>>> Line %d, DKQ_Count(): %d\n", __LINE__, DKQ_Count());
        }
        else
        {
            *pCommand = pNode->pRawCommand;
            pNode->pRawCommand = NULL;
            dxMemFree(pNode);
            pNode = NULL;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "Line %d, [%s] MType: %d\n",
                             __LINE__,
                             __FUNCTION__,
                             (*pCommand)->Header.MessageType);
        }

        /*if (pNode)
        {
            dxMemFree(pNode);
            pNode = NULL;
        }*/

        ret = 1;
    }

    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQHead: %X\n", __LINE__, (unsigned int)pDKQHead);
    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, "Line %d, pDKQTail: %X\n", __LINE__, (unsigned int)pDKQTail);

    return ret;
}

uint8_t DKQ_PeekMessageType(void)
{
    uint8_t ret = 0;

    if (pDKQHead)
    {
        ret = pDKQHead->pRawCommand->Header.MessageType;
    }

    return ret;
}

DKRESTCommMsg_t *DKQ_PeekMessage(void)
{
    DKRESTCommMsg_t *pCommand = NULL;

    if (pDKQHead)
    {
        pCommand = pDKQHead->pRawCommand;
    }

    return pCommand;
}

uint8_t DKQ_Count(void)
{
    uint8_t ret = 0;

    if (pDKQHead == NULL || pDKQTail == NULL)
    {
        return 0;
    }

    DKAPIQueueNode_t *pNode = pDKQHead;
    while (pNode)
    {
        ret++;

        //pNode = pDKQHead->Next;
        pNode = pNode->Next;
    }

    return ret;
}

#if CONFIG_HEADER_X_DK_STATE
/**
 * Use for X-Dk-State
 */
uint32_t DKMSG_GetSeqNo(void)
{
    static uint32_t Value = 0;

    Value++;

    if (Value == 0xFFFFFFFF)
    {
        Value = 0;
    }

    return Value;

}
#endif // CONFIG_HEADER_X_DK_STATE
