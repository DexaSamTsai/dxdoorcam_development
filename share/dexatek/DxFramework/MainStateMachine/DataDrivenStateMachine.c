//============================================================================
// File: DataDrivenStateMachine.c
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

//============================================================================
//    Includes
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "DataDrivenStateMachine.h"



//============================================================================
//    Public methods
//============================================================================

/**
 * @brief DataDrivenStateMachine_process
 */
DataDrivenStateMachineResult DataDrivenStateMachine_process (
    DataDrivenStateMachine* stateMachine,
    void* data
);


/**
 * @brief DataDrivenStateMachine_processOnce
 */
DataDrivenStateMachineResult DataDrivenStateMachine_processOnce (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToExecute,
    void* data
);


/**
 * @brief DataDrivenStateMachine_changeState
 */
DataDrivenStateMachineResult DataDrivenStateMachine_changeState (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToChange
);


/**
 * @brief DataDrivenStateMachine_getCurrentState
 */
uint16_t DataDrivenStateMachine_getCurrentState (
    DataDrivenStateMachine* stateMachine
);


/**
 * @brief DataDrivenStateMachine_stateNotChangingWhileEqualTo
 */
dxBOOL DataDrivenStateMachine_stateNotChangingWhileEqualTo (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToCompare
);



//============================================================================
//    Private methods
//============================================================================

/**
 * @brief _DataDrivenStateMachine_searchEntry
 */
DataDrivenStateMachineTable* _DataDrivenStateMachine_searchTable (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToChange
);


/**
 * @brief _DataDrivenStateMachine_execute
 */
DataDrivenStateMachineResult _DataDrivenStateMachine_execute (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToExecute,
    void* data,
    dxBOOL isFirstCall
);



//============================================================================
//    Implementations
//============================================================================

DataDrivenStateMachine* DataDrivenStateMachine_create (
    DataDrivenStateMachineTable* sourceTables
) {
    DataDrivenStateMachine* stateMachine = NULL;
    DataDrivenStateMachineTable* table = NULL;
    dxBOOL notDone = dxTRUE;
    uint16_t totalState = 0;
    uint16_t i = 0;

    if (!sourceTables) {
        goto fail_alloc_machine;
    }

    // Get total state from sourceTables
    table = sourceTables;
    while (notDone) {
        if (table->code == DATA_DRIVEN_STATE_INVALID) {
            notDone = dxFALSE; //exit
        } else {
            table++;
        }
        totalState++;
    }
    if (totalState == 0) {
        goto fail_alloc_machine;
    }

    // Allocate state machine
    stateMachine = (DataDrivenStateMachine*) dxMemAlloc ("stateMachine", 1, sizeof (DataDrivenStateMachine));
    if (!stateMachine) {
        goto fail_alloc_machine;
    }

    // Allocate table
    stateMachine->tables = dxMemAlloc ("tables", 1, sizeof (DataDrivenStateMachineTable) * totalState);
    if (!stateMachine) {
        goto fail_alloc_table;
    }

    //Build the table
    for (i = 0; i < totalState; i++)
    {
        DataDrivenStateMachineTable* sourceTable = sourceTables + i;
        DataDrivenStateMachineTable* destinationTable = stateMachine->tables + i;
        destinationTable->code = sourceTable->code;
        destinationTable->function = sourceTable->function;
    }

    stateMachine->nextState = DATA_DRIVEN_STATE_INVALID;
    stateMachine->currentState = DATA_DRIVEN_STATE_INVALID;

    // Assign methods
    stateMachine->process = DataDrivenStateMachine_process;
    stateMachine->processOnce = DataDrivenStateMachine_processOnce;
    stateMachine->changeState = DataDrivenStateMachine_changeState;
    stateMachine->getCurrentState = DataDrivenStateMachine_getCurrentState;
    stateMachine->stateNotChangingWhileEqualTo = DataDrivenStateMachine_stateNotChangingWhileEqualTo;

    return stateMachine;

fail_alloc_table :

    if (stateMachine) {
        free (stateMachine);
        stateMachine = NULL;
    }

fail_alloc_machine :

    return NULL;
}


void DataDrivenStateMachine_destroy (
    DataDrivenStateMachine* stateMachine
) {
    if (stateMachine) {
        if (stateMachine->tables) {
            dxMemFree (stateMachine->tables);
            stateMachine->tables = NULL;
        }
        dxMemFree (stateMachine);
        stateMachine = NULL;
    }
}


DataDrivenStateMachineResult DataDrivenStateMachine_process (
    DataDrivenStateMachine* stateMachine,
    void* data
) {
    DataDrivenStateMachineResult result = DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;

    if (!stateMachine) {
        result = DATA_DRIVEN_STATE_MACHINE_RET_ERROR;
        goto fail;
    }

    if ((stateMachine->nextState != stateMachine->currentState) &&
        (stateMachine->nextState != DATA_DRIVEN_STATE_INVALID)) {

        stateMachine->currentState = stateMachine->nextState;
        stateMachine->nextState = DATA_DRIVEN_STATE_INVALID;  //Need to clear the nextState first

        result = _DataDrivenStateMachine_execute (stateMachine, stateMachine->currentState, data, dxTRUE);

    } else {
        result = _DataDrivenStateMachine_execute (stateMachine, stateMachine->currentState, data, dxFALSE);
    }

fail :

    return result;
}


DataDrivenStateMachineResult DataDrivenStateMachine_processOnce (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToExecute,
    void* data
) {
    DataDrivenStateMachineResult result = DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;

    if (!stateMachine) {
        result = DATA_DRIVEN_STATE_MACHINE_RET_ERROR;
        goto fail;
    }

    if ((stateToExecute != stateMachine->currentState) &&
        (stateToExecute != DATA_DRIVEN_STATE_INVALID)) {

        result = _DataDrivenStateMachine_execute (stateMachine, stateToExecute, data, dxTRUE);
    }

fail :

    return result;
}


DataDrivenStateMachineResult DataDrivenStateMachine_changeState (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToChange
) {
    DataDrivenStateMachineResult result = DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;

    if (!stateMachine) {
        result = DATA_DRIVEN_STATE_MACHINE_RET_ERROR;
        goto fail;
    }

    //TODO: Should we add resource protection here? if so make sure it is
    //      ONLY limited to here otherwise we might create deadlock if this function is called while
    //      in data driven state function called (which we need to allow it).
    if (_DataDrivenStateMachine_searchTable (stateMachine, stateToChange) != NULL) {
        if (stateMachine->currentState != stateToChange) {
            stateMachine->nextState = stateToChange;
        }
    } else {
        result = DATA_DRIVEN_STATE_MACHINE_RET_STATE_CODE_NOT_VALID;
    }

fail :

    return result;
}


uint16_t DataDrivenStateMachine_getCurrentState (
    DataDrivenStateMachine* stateMachine
) {
    uint16_t currentState = DATA_DRIVEN_STATE_INVALID;

    if (!stateMachine) {
        goto fail;
    }

    currentState = stateMachine->currentState;

fail :

    return currentState;
}


dxBOOL DataDrivenStateMachine_stateNotChangingWhileEqualTo (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToCompare
) {
    dxBOOL pass = dxFALSE;

    if (!stateMachine) {
        goto fail;
    }

    if (stateMachine->nextState == DATA_DRIVEN_STATE_INVALID) {
        if (stateMachine->currentState == stateToCompare) {
            pass = dxTRUE;
        }
    }

fail :

    return pass;
}


DataDrivenStateMachineTable* _DataDrivenStateMachine_searchTable (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToChange
) {
    DataDrivenStateMachineTable* table = NULL;
    dxBOOL notFound = dxTRUE;

    if (!stateMachine) {
        goto fail;
    }

    table = stateMachine->tables;

    while (notFound) {
        if (table->code == stateToChange) {
            notFound = dxFALSE; //exit
        } else if (table->code == DATA_DRIVEN_STATE_INVALID) { //Reach end of table list
            table = NULL;
            notFound = dxFALSE; //exit
        } else {
            table++;
        }
    }

fail :

    return table;
}


DataDrivenStateMachineResult _DataDrivenStateMachine_execute (
    DataDrivenStateMachine* stateMachine,
    uint16_t stateToExecute,
    void* data,
    dxBOOL isFirstCall
) {
    DataDrivenStateMachineResult result = DATA_DRIVEN_STATE_MACHINE_RET_ERROR;
    DataDrivenStateMachineTable* table = NULL;

    if (!stateMachine) {
        goto fail;
    }

    table = _DataDrivenStateMachine_searchTable (stateMachine, stateToExecute);
    if (table) {
        //Execute the state function
        if (table->function) {
            table->function (data, isFirstCall);
        }

        result = DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
    }

fail :

    return result;
}
