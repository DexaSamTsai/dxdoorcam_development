//============================================================================
// File: ST_CommonUtils.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/02
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_COMMON_UTILS_H
#define _ST_COMMON_UTILS_H

#include "ProjectCommonConfig.h"
#include "SimpleTimedStateMachine.h"
#include "BLEManager.h"
#include "dk_Peripheral.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif

//============================================================================
// Defines
//============================================================================

#define FW_VERSION_DUMMY                                "0.0.0"

#define SERVER_LOG_IN_TIMEOUT_MIN                       (5 * MINUTES)
#define SERVER_LOG_IN_TIMEOUT_MAX                       (25 * MINUTES)
#define SERVER_LOG_IN_TIMEOUT_ADD                       (5 * MINUTES)
#define WIFI_SCAN_TIMEOUT_MAX_MS                        (60*SECONDS)

//SubState
#define CUTL_SUBSTATE_TIMEOUT_MAX                       (15*SECONDS)
#define CUTL_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX           (60*SECONDS)

//Temporary storage for Task info that require handling that is not in the same run loop
#define MAX_TASK_INFO_STORAGE_SLOT                      10
#define TASK_INFO_MAX_STORAGE_TIME_IN_SEC               (60*SECONDS)

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    dxTime_t        Time;
    int16_t         DevHandlerID;
    uint16_t        Reserve;
    uint64_t        IdentificationNumber;

} TaskInfo_t;

typedef enum
{
    FR_FW_UPDATE_AVAILABLE_NONE           = 0,
    FR_FW_UPDATE_AVAILABLE_MAIN_FW,
    FR_FW_UPDATE_AVAILABLE_BLE_FW,
    FR_FW_UPDATE_AVAILABLE_AUTO_UPDATE,

} FwNewUpdateAvailStatus_t;

typedef enum
{
    REGISTER_STATE_NOT_EXIST               = 0,
    REGISTER_STATE_NONE,
    REGISTER_STATE_REGISTERING,
    REGISTER_STATE_FINISH,

} RegisterState_t;

typedef struct
{
    uint16_t            SourceOfOrigin;
    uint64_t            IdentificationNumber;
    DeviceInfoReport_t  DeviceInfoReport;

} HpEventDataPayloadPackage_t;

//============================================================================
// Function
//============================================================================

/**
 * @brief cUtlJobDoneReportSendWithPayloadHandler
 */
void cUtlJobDoneReportSendWithPayloadHandler(uint16_t SourceOfOrigin, uint64_t IdentificationNumber, int16_t StatusReport, uint16_t PayloadSize, uint8_t* Payload );


/**
 * @brief cUtlJobDoneReportSendHandler
 */
void cUtlJobDoneReportSendHandler(uint16_t SourceOfOrigin, uint64_t IdentificationNumber, int16_t StatusReport);


/**
 * @brief cUtlConvertBleDevManagerResultStateToJobDoneReportStatus
 */
int16_t cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(BLEManager_TaskResult_t BleDevManagerResultToConvert);


/**
 * @brief cUtlLogInToServer
 */
void cUtlLogInToServer(void);


/**
 * @brief cUtlSubState_Reset
 */
SimpleStateMachine_result_t cUtlSubState_Reset(void* data, dxBOOL isFirstCall);


/**
 * @brief cUtlIsSysTimeGreaterThan
 */
dxBOOL cUtlIsSysTimeGreaterThan(dxTime_t TimeToCompare, uint32_t ValueGreaterThan);


/**
 * @brief cUtlHexDump
 */
void cUtlHexDump(uint8_t *pData, int len);


/**
 * @brief cUtlParseContainer
 */
void cUtlParseContainer(dxContainer_t *pCont);

RegisterState_t cUtlCheckRegisterStateGet( void );

void cUtlConnectToWifi(void);

dxBOOL cUtlIsBleExecutionOfflineSupported( uint8_t* PeripheralJobExec );

void cUtlParseAndDispatchContainerDataReport( dxContainer_t* pCont, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, dxBOOL OnlyWithLValueInContainer );

#if DEVICE_IS_HYBRID
void cUtlUpdateHyBridPeripheralStatus( void );

void cUtlAddHybridPeripheral( void );
#endif

void cUtlAddGateway( void );



//============================================================================
// Server Connect Procedure Gating Functions
//============================================================================

/**
 * @brief cUtlServerConnectGating_AllInfoSynched
 */
void cUtlServerConnectGating_AllInfoSynched(void);


/**
 * @brief cUtlServerConnectGating_ConnectAttemptStart
 */
void cUtlServerConnectGating_ConnectAttemptStart(void);


/**
 * @brief cUtlServerConnectGating_AllowStartConnect
 */
dxBOOL cUtlServerConnectGating_AllowStartConnect(void);



//============================================================================
// HybribPeripheral Function
//============================================================================
#if DEVICE_IS_HYBRID
/**
 * Get security key from local storage, if no security found then generate a new one.
 */
dxBOOL cUtlGetSecurityKey (uint8_t* key, uint16_t* keyLength);

HpEventDataPayloadPackage_t cUtlHybridPeripheralDataTypeDataPayloadUpdateOnlyPackage(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t* pAdDataPayload, uint32_t AdDataPayloadSize);
HpEventDataPayloadPackage_t cUtlHybridPeripheralHistoryEventDataPayloadPackage(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t* pAdDataPayload, uint32_t AdDataPayloadSize);

dxBOOL cUtlHybridPeripheralGetDeviceGenericInfo (uint32_t messageId, uint32_t payloadLength, uint8_t* payload);
dxBOOL cUtlHybridPeripheralDeviceInfoUpdate(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, DeviceInfoReport_t* DeviceKeeperInfoReport);
dxBOOL cUtlHybridPeripheralDeviceKeeperListUpdate(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t *pUpdatedPeripheralData, uint16_t PeripheralDataLen);
dxBOOL cUtlHybridPeripheralGetConcurrentPeripheralStatus (void);
dxBOOL cUtlHybridPeripheralCustomizeConcurrentPeripheralBdAddress (void);
dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralSecurityKey (void);
dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralAdvertiseData (uint32_t payloadLength, uint8_t* payload);
dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralScanResponse (void);
dxBOOL cUtlHybridPeripheralAddConcurrentPeripheralServiceCharacteristic (uint32_t payloadLength, uint8_t* payload);
dxBOOL cUtlHybridPeripheralEnableConcurrentPeripheralSystemUtils (uint32_t payloadLength, uint8_t* payload);
dxBOOL cUtlHybridPeripheralStartConcurrentPeripheral (void);
dxBOOL cUtlHybridPeripheralStopConcurrentPeripheral (void);
dxBOOL cUtlHybridPeripheralDisconnectConcurrentPeripheral (void);
dxBOOL cUtlHybridPeripheralTransmitConcurrentPeripheralData (uint32_t payloadLength, uint8_t* payload);
dxBOOL cUtlHybridPeripheralReceiveConcurrentPeripheralData (uint16_t payloadLength, uint8_t* payload);

dxBOOL cUtlHybridPeripheralConcurrentPeripheralStatusResponse (BleConcurrentPeripheralGetStatusResponse_t* statusResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralBdAddressResponse (SystemBdAddressResponse_t* bdAddressResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralSecurityKeyResponse (BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralAdvertiseDataResponse (BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralScanResponseResponse (BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralServiceCharacteristicResponse (BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse);
dxBOOL cUtlHybridPeripheralConcurrentPeripheralSystemUtilsResponse (BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse);

#endif  //#if DEVICE_IS_HYBRID

#endif // _ST_COMMON_UTILS_H
