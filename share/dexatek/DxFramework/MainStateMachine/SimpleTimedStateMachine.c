//============================================================================
// File: SimpleStateMachine.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/01
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "SimpleTimedStateMachine.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

typedef struct
{
    simple_st_machine_table_t   StateTable;
    dxTime_t                    StateTimeoutTime;

} simple_st_machine_t;


typedef struct
{
    simple_st_machine_t*        pStateTableList;
    uint16_t                    CurrentState;
    uint16_t                    NextState;

} simple_st_machine_handler_t;

//============================================================================
// Global Var
//============================================================================

//============================================================================
// Utils Function
//============================================================================

simple_st_machine_t* UtilSearchForStateInfo(simple_st_machine_handler_t* SmpStateHandler, uint16_t StateCodeToChange)
{
    simple_st_machine_t* pSearchedStateInfo = NULL;

    if (SmpStateHandler)
    {
        dxBOOL  NotFound = dxTRUE;

        pSearchedStateInfo = SmpStateHandler->pStateTableList;

        while(NotFound)
        {
            if (pSearchedStateInfo->StateTable.StateCode == StateCodeToChange)
            {
                NotFound = dxFALSE; //exit
            }
            else if (pSearchedStateInfo->StateTable.StateCode == SIMPLE_STATE_INVALID) //Reach end of table list
            {
                pSearchedStateInfo = NULL;
                NotFound = dxFALSE; //exit
            }
            else
            {
                pSearchedStateInfo++;
            }
        }
    }

    return pSearchedStateInfo;
}

simple_st_machine_t* UtilSearchForCurrentStateInfo(simple_st_machine_handler_t* SmpStateHandler)
{
    return  UtilSearchForStateInfo(SmpStateHandler, SmpStateHandler->CurrentState);
}


SimpleStateMachine_result_t UtlExecuteState(SimpleStateHandler handler, uint16_t StateCodeToExecute, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_ERROR;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;

        simple_st_machine_t* pCurrentStateInfo = UtilSearchForStateInfo(SmpHandler, StateCodeToExecute);

        if (pCurrentStateInfo)
        {
            SmpHandler->CurrentState = StateCodeToExecute;

            //Execute the state function
            if (pCurrentStateInfo->StateTable.StateFunc)
            {
                pCurrentStateInfo->StateTable.StateFunc(NULL, isFirstCall);
            }

            //Refresh the next time out time
            if (pCurrentStateInfo->StateTable.StateMaxTimeout)
            {
                dxTime_t    CurrentTime;
                CurrentTime = dxTimeGetMS();
                pCurrentStateInfo->StateTimeoutTime = CurrentTime + pCurrentStateInfo->StateTable.StateMaxTimeout;
            }

            result = SMP_STATE_MACHINE_RET_SUCCESS;
        }

    }

    return result;
}

//============================================================================
// Function
//============================================================================

SimpleStateHandler SimpleStateMachine_Init(simple_st_machine_table_t* pSimpleStateTableTable)
{
    simple_st_machine_handler_t* SmpHandler = NULL;

    if (pSimpleStateTableTable)
    {
        dxBOOL      NotDone = dxTRUE;
        uint16_t    TotalState = 0;
        simple_st_machine_table_t* pSimpleStatetableIndex = pSimpleStateTableTable;

        //Get total state from pSimpleStateTableTable
        while (NotDone)
        {
            if (pSimpleStatetableIndex->StateCode == SIMPLE_STATE_INVALID)
            {
                NotDone = dxFALSE; //exit
            }
            else
            {
                pSimpleStatetableIndex++;
            }

            TotalState++;
        }

        //Allocate handler
        SmpHandler = (simple_st_machine_handler_t*)dxMemAlloc("SmpHandler", 1, sizeof(simple_st_machine_handler_t));
        if (SmpHandler)
        {
            SmpHandler->pStateTableList = dxMemAlloc("StTableList", 1, sizeof(simple_st_machine_t) * TotalState);
        }

        //Build the table
        uint16_t i = 0;
        for (i = 0; i < TotalState; i++)
        {
            simple_st_machine_table_t*  pSourceSimpleStatetable = pSimpleStateTableTable + i;
            simple_st_machine_t*        pDstSimpleStateTable = SmpHandler->pStateTableList + i;
            pDstSimpleStateTable->StateTable.StateCode = pSourceSimpleStatetable->StateCode;
            pDstSimpleStateTable->StateTable.StateFunc = pSourceSimpleStatetable->StateFunc;
            pDstSimpleStateTable->StateTable.StateMaxTimeout = pSourceSimpleStatetable->StateMaxTimeout;
            pDstSimpleStateTable->StateTimeoutTime = 0;
        }

        SmpHandler->NextState = SIMPLE_STATE_INVALID;
        SmpHandler->CurrentState = SIMPLE_STATE_INVALID;

    }

    return (SimpleStateHandler)SmpHandler;
}

SimpleStateMachine_result_t SimpleStateMachine_Process(SimpleStateHandler handler)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;

        if ((SmpHandler->NextState != SmpHandler->CurrentState) && (SmpHandler->NextState != SIMPLE_STATE_INVALID))
        {
            uint16_t NextExecuteState = SmpHandler->NextState;
            SmpHandler->NextState = SIMPLE_STATE_INVALID;  //Need to clear the nextState first

            result = UtlExecuteState( handler, NextExecuteState, dxTRUE);
        }

        simple_st_machine_t* pCurrentStateInfo = UtilSearchForCurrentStateInfo(SmpHandler);

        if (pCurrentStateInfo->StateTimeoutTime != 0)
        {
            dxTime_t    CurrentTime;
            CurrentTime = dxTimeGetMS();

            //Time is Up? if so we re-execute the state function again
            if (CurrentTime > pCurrentStateInfo->StateTimeoutTime)
            {
                result = UtlExecuteState( handler, SmpHandler->CurrentState, dxFALSE);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SimpleStateMachine_ExecuteState(SimpleStateHandler handler, uint16_t StateCodeToChange)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (handler)
    {
        //TODO: Should we add resource protection here? if so make sure it is
        //      ONLY limited to here otherwise we might create deadlock if this function is called while
        //      in simple state function called (which we need to allow it).
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;

        if (UtilSearchForStateInfo(SmpHandler, StateCodeToChange) != NULL)
        {
            if (SmpHandler->CurrentState != StateCodeToChange)
            {
                if (SmpHandler->NextState == SIMPLE_STATE_INVALID)
                {
                    SmpHandler->NextState = StateCodeToChange;
                }
                else
                {
                    result = SMP_STATE_MACHINE_RET_NOT_ALLOWED_ST_CHANGE_INPROCESS;
                }
            }
        }
        else
        {
            result = SMP_STATE_MACHINE_RET_STATE_CODE_NOT_VALID;
        }
    }
    else
    {
        result = SMP_STATE_MACHINE_RET_ERROR;
    }

    return result;
}


uint16_t SimpleStateMachine_GetCurrentState(SimpleStateHandler handler)
{
    uint16_t CurrentState = SIMPLE_STATE_INVALID;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;
        CurrentState = SmpHandler->CurrentState;
    }

    return CurrentState;
}

uint16_t SimpleStateMachine_GetNextState(SimpleStateHandler handler)
{
    uint16_t NextState = SIMPLE_STATE_INVALID;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;
        NextState = SmpHandler->NextState;
    }

    return NextState;
}

dxBOOL SimpleStateMachine_StateNotChangingWhileEqualTo(SimpleStateHandler handler, uint16_t EqualToStateCode)
{
    dxBOOL Pass = dxFALSE;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;

        if (SmpHandler->NextState == SIMPLE_STATE_INVALID)
        {
            if (SmpHandler->CurrentState == EqualToStateCode)
            {
                Pass = dxTRUE;
            }
        }
    }

    return Pass;
}

SimpleStateMachine_result_t SimpleStateMachine_Uninit(SimpleStateHandler handler)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (handler)
    {
        simple_st_machine_handler_t* SmpHandler = (simple_st_machine_handler_t*)handler;
        if (SmpHandler->pStateTableList)
        {
            dxMemFree(SmpHandler->pStateTableList);
        }

        dxMemFree(SmpHandler);
    }

    return result;
}




