//============================================================================
// File: CentralStateMachineAutomata.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _CENTRALSTATEMACHINE_H
#define _CENTRALSTATEMACHINE_H

#include "dxOS.h"
#include "ProjectCommonConfig.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

typedef enum
{
    STATE_UNKNOWN           = 0,
    STATE_IDLE,
    STATE_SETUP,
    STATE_FW_UPDATE,
    STATE_SERVER_ONLINE,
    STATE_OFFLINE,
    STATE_REGISTER,
    STATE_FATAL,

    STATE_UPPER_LIMIT,
} State_Code_t;

typedef enum
{
    STATE_EVENT_UNKNOWN           = 0,
    STATE_EVENT_BLE,
    STATE_EVENT_SERVER,
    STATE_EVENT_MQTT,
#if DEVICE_IS_HYBRID
    STATE_EVENT_HP,
#endif //#if DEVICE_IS_HYBRID
    STATE_EVENT_WIFI,
    STATE_EVENT_UDP,
    STATE_EVENT_JOB,
    STATE_EVENT_KEYPAD,
    STATE_EVENT_LED,

} State_Event_t;


typedef enum
{

    STATE_MACHINE_RET_SUCCESS                           = 0,
    STATE_MACHINE_RET_NOT_ALLOWED_ST_CHANGE_INPROCESS,
    STATE_MACHINE_RET_NOT_SUPPORTED,

    STATE_MACHINE_RET_ERROR                             = -1,
    STATE_MACHINE_RET_ERROR_INALID_STATE                = -2,
    
} StateMachine_result_t;

typedef enum
{

    STATE_MACHINE_EVENT_NORMAL                  = 0,
    STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE,

    STATE_MACHINE_EVENT_ERROR                   = -1,

} StateMachineEvent_State_t;

/******************************************************
 *                    Structures
 ******************************************************/

typedef StateMachine_result_t (*state_func_t)( void* data );
typedef StateMachineEvent_State_t (*state_event_func_t)( void* data );
typedef void (*state_main_event_func_t)( void* data );

typedef struct
{
    State_Code_t           State;
    state_func_t           StateInit;
    state_func_t           StateMainProcess;
    state_func_t           StateEnd;

} state_machine_table_t;

typedef struct
{
    state_main_event_func_t     EventBLEManager;
    state_main_event_func_t     EventServerManager;
    state_main_event_func_t     EventMqttManager;
#if DEVICE_IS_HYBRID
    state_main_event_func_t     EventHpManager;
#endif //#if DEVICE_IS_HYBRID
    state_main_event_func_t     EventWifiManager;
    state_main_event_func_t     EventUdpManager;
    state_main_event_func_t     EventJobManager;
    state_main_event_func_t     EventKeyPadManager;
    state_main_event_func_t     EventLEDManager;

} state_machine_main_event_t;

typedef struct
{
    State_Code_t           EventForState;
    State_Event_t          Event;
    state_event_func_t     pFuncEvent;
    dxSemaphoreHandle_t    EventResourceProtection;

} state_machine_event_table_t;


//============================================================================
// Type Defines
//============================================================================

//============================================================================
// Function
//============================================================================

/** Initialization of central state machine, and execute the initial state.
 *  Be sure to register the returned event function to each specific manager before calling
 *  CentralStateMachine_Process.
 *
 * @param[in]  InitialState 	 : 	The initial state to execute
 *
 * @return state_machine_main_event_t  : The returned event of each manager to register to.
 */
state_machine_main_event_t CentralStateMachine_Init(State_Code_t InitialState);


/** This function should be called from the main loop thread once a while (eg Suggest every 30ms, or 100ms max)
 *  to allow state handlings.
 *
 * @return StateMachine_result_t
 */
StateMachine_result_t CentralStateMachine_Process(void);


/** Although state machine should handle the state change within the event function call,
 *  it is still possible to change state from the main loop but most likely not needed.
 *
 * @param[in]  StateChange 	 : 	    The state to change to
 * @param[in]  pStateInitData 	 : 	    The pointer of state (Init) data when the initial function of changed state is executed
 *                                    Note: The data is copied to a buffer therefore the source data can be freed right after this call
 * @param[in]  StateInitDataSize 	 : 	The size of init data
 *
 * @return StateMachine_result_t
 */
StateMachine_result_t CentralStateMachine_ChangeState(State_Code_t StateChange, void* pStateInitData, uint32_t StateInitDataSize);


/** Get current state of stae machine
 *
 *
 * @return State_Code_t
 */
State_Code_t CentralStateMachine_GetCurrentState(void);


/** Uninitialize the state machine
 *
 *
 * @return StateMachine_result_t
 */
StateMachine_result_t CentralStateMachine_Uninit(void);

#endif // _CENTRALSTATEMACHINE_H
