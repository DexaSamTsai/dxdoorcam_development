//============================================================================
// File: ST_RegisterMain.h
//
// Author: Allen Liao
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _ST_REGISTER_MAIN_H
#define _ST_REGISTER_MAIN_H

#include "dxOS.h"


//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t Register_Init(void* data );
StateMachine_result_t Register_MainProcess(void* data );
StateMachine_result_t Register_End(void* data );
StateMachineEvent_State_t Register_EventBLEManager(void* data );
StateMachineEvent_State_t Register_EventServerManager(void* data );
StateMachineEvent_State_t Register_EventMqttManager(void* data );
#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Register_EventHpManager(void* data );
#endif //#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Register_EventWifiManager(void* data );
StateMachineEvent_State_t Register_EventUdpManager(void* data );
StateMachineEvent_State_t Register_EventJobManager(void* data );
StateMachineEvent_State_t Register_EventKeyPadManager(void* data );
StateMachineEvent_State_t Register_EventLEDManager(void* data );


#endif // _ST_REGISTER_MAIN_H