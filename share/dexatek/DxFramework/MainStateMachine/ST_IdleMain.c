//============================================================================
// File: ST_IdleMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#include "LedManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "dxDeviceInfoManager.h"
#include "OfflineSupport.h"
#include "CentralStateMachineAutomata.h"
#include "SimpleTimedStateMachine.h"
#include "ST_CommonUtils.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================
#define REGISTEING_DELAY_TIME                                   (30*SECONDS)


//============================================================================
// Enum
//============================================================================

//============================================================================
// Typedef
//============================================================================

typedef enum
{
    IDLE_CHECK_DEVICE_REGISTERED = 0,

    IDLE_CHECK_DONE,
} LEDModeInitializationTimedState_t;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Function Definition
//============================================================================

SimpleStateMachine_result_t IdleCheck_DeviceIsRegistered(void* data, dxBOOL isFirstCall);

//============================================================================
// Global Var
//============================================================================

SimpleStateHandler      IdleCheckTimedStHld = NULL;

static simple_st_machine_table_t IdleCheck_SmpStateMachineProcessFuncTable[] =
{

    {IDLE_CHECK_DEVICE_REGISTERED, IdleCheck_DeviceIsRegistered, 1 * SECONDS},
    {IDLE_CHECK_DONE, NULL, 0},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};

unsigned int RegisteringCounter = 0;
dxTime_t IdleInitTime = 0;

//============================================================================
// Internal Function
//============================================================================

//============================================================================
// Util Function
//============================================================================

dxBOOL UtlDevInfoDeviceIsRegitered(void)
{
    //Here we are going to check if Access Key and Wifi SSID is available. If so we conclude this device is registered and is capable of
    //connecting to server.

    dxBOOL IsRegistered = dxFALSE;

    uint16_t    SSIDBuffSize = 128;
    uint16_t    DeviceAccessKeyBuffSize = 128;
    void* pTempSSIDBuff = dxMemAlloc("TempSSIDBuff", 1, SSIDBuffSize);
    void* pDeviceAccessKeyBuff = dxMemAlloc( "DevAccessKeyBuff", 1, DeviceAccessKeyBuffSize);

    if (pTempSSIDBuff && pDeviceAccessKeyBuff)
    {
        //We are not going to load directly from Disk, simply load from the memory as it should already be there due to we have already
        //initialize the devinfo.
        dxDEV_INFO_RET_CODE DevInfoAccessKeyRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, pDeviceAccessKeyBuff, &DeviceAccessKeyBuffSize, dxFALSE);
        dxDEV_INFO_RET_CODE DevInfoWifiSSIDRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID, pTempSSIDBuff, &SSIDBuffSize, dxFALSE);

        if ((DevInfoWifiSSIDRet == DXDEV_INFO_SUCCESS) && (DevInfoAccessKeyRet == DXDEV_INFO_SUCCESS))
        {
            IsRegistered = dxTRUE;
        }
        else
        {
            /*
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Unable to find ApSSID Or Key from DevInfo (%d / %d)\r\n",
                             __LINE__,
                             DevInfoWifiSSIDRet,
                             DevInfoAccessKeyRet);
            */
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) [%s]\r\n",
                         __LINE__,
                         __FUNCTION__);

        CentralStateMachine_ChangeState(STATE_FATAL, NULL, 0);
    }


    if (pTempSSIDBuff)
    {
        dxMemFree(pTempSSIDBuff);
    }

    if (pDeviceAccessKeyBuff)
    {
        dxMemFree(pDeviceAccessKeyBuff);
    }

    return IsRegistered;
}

//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t Idle_EventBLEManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Idle_EventJobManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Idle_EventServerManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    switch (EventHeader.MessageType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t Idle_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Idle_EventHpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }
        }
        break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Idle_EventWifiManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Idle_EventUdpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Idle_EventKeyPadManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE;

    //NOTE: We are returning STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE so that it is handled by the default state machine

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Idle_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}

//============================================================================
// SubState Function - Idle Check
//============================================================================

SimpleStateMachine_result_t IdleCheck_DeviceIsRegistered(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    RegisterState_t rs_ret = REGISTER_STATE_NOT_EXIST;

    if ( UtlDevInfoDeviceIsRegitered() == dxTRUE ) {
        rs_ret = cUtlCheckRegisterStateGet();
        if ( rs_ret == REGISTER_STATE_NOT_EXIST ) {
            int32_t RegisterState = REGISTER_STATE_REGISTERING;
            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
            if (DevInfoRet != DXDEV_INFO_SUCCESS)
            {
                G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                             __LINE__,
                                             DevInfoRet);
            }

            CentralStateMachine_ChangeState( STATE_REGISTER, NULL, 0);
        } else if ( rs_ret == REGISTER_STATE_NONE ) {
            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
            if (DevInfoRet != DXDEV_INFO_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                 __LINE__, DevInfoRet);
            }
        } else if ( rs_ret == REGISTER_STATE_FINISH ) {
            //Device is registered, we change to OFFLINE State to continue!
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                        "%s Device is regitered!\r\n", __FUNCTION__);

            CentralStateMachine_ChangeState( STATE_OFFLINE, NULL, 0);
        } else {
            //REGISTER_STATE_REGISTERING
            RegisteringCounter++;
            if ( RegisteringCounter == 0xFFFFFFFF ) RegisteringCounter = 1;

            CentralStateMachine_ChangeState( STATE_REGISTER, NULL, 0);
        }
    } else {
#if DEVICE_IS_HYBRID
        G_SYS_DBG_LOG_V("%s inform system is not registered \r\n", __FUNCTION__);

        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
        if(pCmdMessage == NULL)
        {
            result = SMP_STATE_MACHINE_RET_ERROR;
            G_SYS_DBG_LOG_V("%s no buffer for HpCmdMsg, size = %d\r\n", __FUNCTION__, sizeof(HpCmdMessageQueue_t));
            return result;
        }
        pCmdMessage->CommandMessage = HP_CMD_SYSTEM_NOT_REGISTERED;
        pCmdMessage->MessageID = 0;
        pCmdMessage->Priority = 0; //Normal

        pCmdMessage->PayloadSize = 0;
        pCmdMessage->PayloadData = NULL;

        HpManagerSendTo_Client(pCmdMessage);
        HpManagerCleanEventArgumentObj(pCmdMessage);

#endif // DEVICE_IS_HYBRID
        CentralOfflineSupportDeviceEraseAll();
    }
    return result;
}

//============================================================================
// Function
//============================================================================

StateMachine_result_t Idle_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    IdleInitTime = dxTimeGetMS();

    dxDEV_INFO_RET_CODE devInfoRet = dxDevInfo_Init();
    if (devInfoRet != DXDEV_INFO_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "FATAL:(Line=%d) dxDevInfo_Init() = %d\r\n",
                         __LINE__,
                         devInfoRet);
        CentralStateMachine_ChangeState(STATE_FATAL, NULL, 0);
    }

    IdleCheckTimedStHld =  SimpleStateMachine_Init(IdleCheck_SmpStateMachineProcessFuncTable);
    if (IdleCheckTimedStHld == NULL)
    {
        G_SYS_DBG_LOG_V("FATAL: IdleCheckTimedStHld init Failed\r\n");
    }
    else
    {
        SimpleStateMachine_result_t IdleCheckStResult = SimpleStateMachine_ExecuteState(IdleCheckTimedStHld, IDLE_CHECK_DEVICE_REGISTERED);
        if (IdleCheckStResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_V("FATAL: IdleCheckStResult (%d) IDLE_CHECK_DEVICE_REGISTERED failed\r\n", IdleCheckStResult);
        }
    }

    LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_INIT);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedMode (ret error %d)\r\n",
                         __LINE__, LedManagerRetCode);
    }

    return result;
}

StateMachine_result_t Idle_MainProcess(void* data )
{
    if ( RegisteringCounter > 0) {
        if ( dxTimeGetMS() < ( IdleInitTime + REGISTEING_DELAY_TIME ) ) {
            return STATE_MACHINE_RET_SUCCESS;
        }
    }

    SimpleStateMachine_result_t IdleCheckStResult = SimpleStateMachine_Process(IdleCheckTimedStHld);
    if (IdleCheckStResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_V("FATAL: SimpleStateMachine_Process failed(%d)\r\n", IdleCheckStResult);
    }

    return STATE_MACHINE_RET_SUCCESS;
}

StateMachine_result_t Idle_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (IdleCheckTimedStHld) {
        SimpleStateMachine_Uninit(IdleCheckTimedStHld);
        IdleCheckTimedStHld = NULL;
    }

	DKTime_t CurrentDkTime;

	memset( &CurrentDkTime, 0, sizeof( DKTime_t ) );

    if ( dxHwRtcGet( &CurrentDkTime ) == DXOS_SUCCESS ) {
    	dxSwRtcSet( CurrentDkTime );
	}

    return result;
}


