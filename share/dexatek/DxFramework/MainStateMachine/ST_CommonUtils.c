//============================================================================
// File: ST_CommonUtils.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/02
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dk_Job.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#include "WifiCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxDeviceInfoManager.h"
#include "LedManager.h"
#include "ST_CommonUtils.h"
#include "OfflineSupport.h"
#include "UdpCommManager.h"
#include "CentralStateMachineAutomata.h"


//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

static uint8_t* securityKey = NULL;



//============================================================================
// Internal Function
//============================================================================

//============================================================================
// Util Function
//============================================================================


//============================================================================
// Function
//============================================================================

void cUtlJobDoneReportSendWithPayloadHandler(uint16_t SourceOfOrigin, uint64_t IdentificationNumber, int16_t StatusReport, uint16_t PayloadSize, uint8_t* Payload )
{
    uint32_t    ObjectID = (uint32_t)((IdentificationNumber >> 32) & 0x00000000FFFFFFFF);
    uint16_t    DeviceId = (uint16_t)((IdentificationNumber >> 16) & 0x000000000000FFFF);
    uint16_t    JobId = (uint16_t)(IdentificationNumber & 0x000000000000FFFF);

    if (SourceOfOrigin == SOURCE_OF_ORIGIN_UDP_JOB)
    {
        //TODO: We should add the ability to have Udp report the payload if any
        UdpCommManagerSendJobExecutionResponse(DeviceId, JobId, StatusReport);
    }
    else if (SourceOfOrigin == SOURCE_OF_ORIGIN_DK_SERVER_JOB)
    {
        G_SYS_DBG_LOG_V("SOURCE_OF_ORIGIN_DK_SERVER_JOB Done (PayloadSize=%d)\r\n", PayloadSize);

        JobDoneReport_t JobReport;
        JobReport.ObjectID = ObjectID;
        JobReport.DeviceIdentificationNumber = DeviceId;
        JobReport.PayLoadIdentificationNumber = JobId;
        JobReport.JobStatus = StatusReport;
        JobReport.JobPayloadLen = PayloadSize;
        JobReport.pJobPayload = Payload;
        DKServer_StatusCode_t DkServerResult = DKServerManager_JobDoneReport_Asynch(SourceOfOrigin, IdentificationNumber, &JobReport, 1);
        if (DkServerResult != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_V1(  "DKServerManager_JobDoneReport_Asynch failed (%d)\r\n", DkServerResult );
        }
    }
}

void cUtlJobDoneReportSendHandler(uint16_t SourceOfOrigin, uint64_t IdentificationNumber, int16_t StatusReport)
{
    cUtlJobDoneReportSendWithPayloadHandler(SourceOfOrigin, IdentificationNumber, StatusReport, 0, NULL );
}


int16_t cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(BLEManager_TaskResult_t BleDevManagerResultToConvert)
{
    int16_t StatusReport = JOB_SUCCESS;

    switch (BleDevManagerResultToConvert)
    {
        case DEV_MANAGER_TASK_SUCCESS:
            StatusReport = JOB_SUCCESS;
            break;

        case DEV_MANAGER_TASK_TIME_OUT:
            StatusReport = JOB_FAILED_TIME_OUT;
            break;

        case DEV_MANAGER_TASK_FAILED_CONNECTION_UNSTABLE:
            StatusReport = JOB_FAILED_CONNECTION_UNSTABLE;
            break;

        case DEV_MANAGER_TASK_DEVICE_ALREADY_PAIRED:
            StatusReport = JOB_UNCOMPLETED_PERIPHERAL_ALREADY_PAIRED;
            break;

        case DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_MISSING:
            StatusReport = JOB_FAILED_MISSING_SECURITY_KEY;
            break;

        case DEV_MANAGER_TASK_DEVICE_SECURITY_KEY_IS_NOT_VALID:
            StatusReport = JOB_FAILED_SECURITY_KEY_NOT_LONGER_VALID;
            break;

        case DEV_MANAGER_TASK_DEVICE_PIN_CODE_IS_NOT_VALID:
            StatusReport = JOB_FAILED_PIN_CODE_IS_NOT_VALID;
            break;

        default:
        {
            G_SYS_DBG_LOG_V1(  "WARNING: BleDevManagerResultToConvert %d not implemented \r\n", BleDevManagerResultToConvert );
            StatusReport = JOB_FAILED_GENERIC;
        }
        break;
    }

    return StatusReport;
}


void cUtlLogInToServer(void)
{
    uint16_t    DeviceAccessKeyBuffSize = 128;
    char*       pDeviceAccessKeyBuff = dxMemAlloc( "DevAccessKeyBuff", 1, DeviceAccessKeyBuffSize);
    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, pDeviceAccessKeyBuff, &DeviceAccessKeyBuffSize, dxFALSE);
    if (DevInfoRet == DXDEV_INFO_SUCCESS)
    {
        CentralOfflineSupportFlagClear(OFFLINE_FLAG_ALL);

        DKServer_StatusCode_t ServerManagerResult = DKServerManager_GatewayLogin_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, (unsigned char *)pDeviceAccessKeyBuff);

        G_SYS_DBG_LOG_V("[%s]Line %d, DKServerManager_GatewayLogin_Asynch():%d\r\n", __FUNCTION__, __LINE__, ServerManagerResult);

        LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_CONNECTING_WITH_DK_SERVER);
        if (LedManagerRetCode != LED_MANAGER_SUCCESS)
        {
            G_SYS_DBG_LOG_V1( "WARNING: LedManager Failed to SetLedCondition (ret error %d)\r\n", LedManagerRetCode );
        }
    }
    else
    {
        if (DevInfoRet == DXDEV_INFO_TYPE_NOT_FOUND)
        {
            G_SYS_DBG_LOG_V( "WARNING: Missing DeviceAccessKey for log-in\r\n" );
        }
        else
        {
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_DK_SERVER_OPERATION_ERROR);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_V1( "WARNING: LedManager Failed to SetLedCondition (ret error %d)\r\n", LedManagerRetCode );
            }
        }
    }

    if (pDeviceAccessKeyBuff)
        dxMemFree(pDeviceAccessKeyBuff);
}

SimpleStateMachine_result_t cUtlSubState_Reset(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    //Take the resource to make sure flash will not be accessed when we reset the system!
    dxFlash_ResourceTake(NULL);

    dxPLATFORM_Reset();

    return result;
}

dxBOOL cUtlIsSysTimeGreaterThan(dxTime_t TimeToCompare, uint32_t ValueGreaterThan)

{
    dxBOOL      Result = dxFALSE;
    dxTime_t    CurrentTime;
    CurrentTime = dxTimeGetMS();

    //We calculate only if CurrentTime >= TimeToCompare, if not then its not greater than
    //It is possible that CurrentTime >= TimeToCompare in case of preemtive multi-thread enviroment.
    if (CurrentTime >= TimeToCompare)
    {
        if ((CurrentTime - TimeToCompare) > ValueGreaterThan)
        {
            Result = dxTRUE;
        }
    }

    return Result;

}

void cUtlHexDump(uint8_t *pData, int len)
{
    char szPrintBuff[128];
    int i = 0;
    memset(szPrintBuff, 0x00, sizeof(szPrintBuff));

//    G_SYS_DBG_LOG_NV("len = %d\n", len);
    while (i < len)
    {
        sprintf(&szPrintBuff[strlen(szPrintBuff)], "%02X", (pData[i] & 0x0FF));
        i++;
        if (((i % 16) == 0) ||
            (i == len))
        {
            strcat(szPrintBuff, "\n");

            G_SYS_DBG_LOG_NV("%s", szPrintBuff);
            memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        }
        else if((i % 4) == 0)
        {
            strcat(szPrintBuff, " - ");
        }
        else
        {
            strcat(szPrintBuff, " ");
        }
    }
    G_SYS_DBG_LOG_NV("\n");
}



void cUtlParseContainer(dxContainer_t *pCont)
{
    // Evaluate all data
    dxContainerM_t *p = pCont->pContMList;

    while (p)
    {
        uint8_t *pContMID = dxContainer_UINT64_ToStr(p->ContMID);

        G_SYS_DBG_LOG_NV("Line %d,\n"
                         "\tContMID: %s\n"
                         "\tBLEObjectID: %d\n"
                         "\tContMType: %u\n"
                         "\tContMName: [%d]%s\n",
                         __LINE__,
                         pContMID,
                         p->BLEObjectID,
                         p->ContMType,
                         (p->ContMName) ? p->ContMName->Length : 0, (p->ContMName) ? (char*)p->ContMName->pData : "");

        dxContainer_UINT64_Free(pContMID);

        dxContainerD_t *q = p->pContDList;
        while (q)
        {
            G_SYS_DBG_LOG_NV("Function %s,\n"
                             "\tContDID: 0x%08x%08x\n"
                             "\tContMID: 0x%08x%08x\n"
                             "\tContDName: [%d]%s\n"
                             "\tContDType: 0x%x\n",
                             __FUNCTION__,
                             (uint32_t) (q->ContDID >> 32), (uint32_t) (q->ContDID & 0xffffffff),
                             (uint32_t) (q->ContMID >> 32), (uint32_t) (q->ContMID & 0xffffffff),
                             (q->ContDName) ? q->ContDName->Length : 0, (q->ContDName) ? (char*)q->ContDName->pData : " ",
                             q->ContDType
                             );

            G_SYS_DBG_LOG_NV("\tDateTime: %d\n"
                             "\tSValue: [%d]%s\n"
                             "\tContLVCRC: %d\n"
                             "\tLValue: [%d]\n",
                             q->DateTime,
                             (q->SValue) ? q->SValue->Length : 0, (q->SValue) ? (char*)q->SValue->pData : " ",
                             q->ContLVCRC,
                             (q->LValue) ? q->LValue->Length : 0
                            );
#if ENABLE_SYSTEM_DEBUGGER
            if(GSysDebugger_ReadLogLevel() & G_SYS_DBG_LEVEL_DETAIL_INFORMATION)
            {
                if(q->LValue)
                {
                    if(q->LValue->Length > 0)
                    {
                        cUtlHexDump(q->LValue->pData, q->LValue->Length);
                    }
                }
            }
#endif
            q = q->pNextContD;
        }

        G_SYS_DBG_LOG_NV("\n");
        p = p->pNextContM;
    }
}

RegisterState_t cUtlCheckRegisterStateGet( void )
{
    int32_t  RegisterState = REGISTER_STATE_NOT_EXIST;
    uint16_t RegisterStateSize = sizeof(RegisterState);

    dxDEV_INFO_RET_CODE DevInfoRegisterStateRet = dxDevInfo_GetData( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, &RegisterStateSize, dxFALSE );
    if (DevInfoRegisterStateRet == DXDEV_INFO_SUCCESS)
    {
        return RegisterState;
    }
    else
    {
        return REGISTER_STATE_NOT_EXIST;
    }
}

void cUtlConnectToWifi( void )
{
    dxDevInfo_IntegrityCheck(dxTRUE);

    uint16_t SSIDBuffSize = 128;
    uint16_t PasswordBuffSize = 128;
    int32_t  SecurityType = DXWIFI_SECURITY_FORCE_32_BIT;
    uint16_t SecurityTypeLen = sizeof(SecurityType);

    void*    pTempSSIDBuff = dxMemAlloc("TempSSIDBuff", 1, SSIDBuffSize);
    void*    pTempPasswordBuff = dxMemAlloc("TempPasswordBuff", 1, PasswordBuffSize);

    if (pTempSSIDBuff && pTempPasswordBuff)
    {

        //First we get SSID from Disk directly
        dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID, pTempSSIDBuff, &SSIDBuffSize, dxTRUE);
        if (DevInfoRet == DXDEV_INFO_SUCCESS)
        {
            //Found so let's update first
            DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID, pTempSSIDBuff, SSIDBuffSize, dxFALSE);
            if (DevInfoRet != DXDEV_INFO_SUCCESS)
            {
            	G_SYS_DBG_LOG_V2("WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID failed (%d)\r\n",
                                 __LINE__,
                                 DevInfoRet);
            }

            //Now we get Password from disk directly
            DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD, pTempPasswordBuff, &PasswordBuffSize, dxTRUE);
            if (DevInfoRet != DXDEV_INFO_SUCCESS) //Did not found password from disk
            {
                PasswordBuffSize = 0;
                ((char*)pTempPasswordBuff)[0] = '\0';
            }
            else    //We got the password from disk so now we update it
            {
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD, pTempPasswordBuff, PasswordBuffSize, dxFALSE);
                if (DevInfoRet != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_V2("WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD failed (%d)\r\n",
                                     __LINE__,
                                     DevInfoRet);
                }
            }

            //Now we get SecurityType from disk directly
            DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE, &SecurityType, &SecurityTypeLen, dxTRUE);
            if (DevInfoRet == DXDEV_INFO_SUCCESS)
            {
                //Now we update it.
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE, &SecurityType, SecurityTypeLen, dxFALSE);
                if (DevInfoRet != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_V2("WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE failed (%d)\r\n",
                                     __LINE__,
                                     DevInfoRet);
                }
            }

            uint16_t SSIDLen = strlen(pTempSSIDBuff);
            SSIDLen = (SSIDLen > SSIDBuffSize) ? SSIDBuffSize:SSIDLen;
            uint16_t PasswordLen = strlen(pTempPasswordBuff);
            PasswordLen = (PasswordLen > PasswordBuffSize) ? PasswordBuffSize:PasswordLen;

            if (SecurityType == DXWIFI_SECURITY_FORCE_32_BIT)
            {
                G_SYS_DBG_LOG_V2("Scan Connect To: '%s' (Pass '%s')\r\n", (char *)pTempSSIDBuff, (char *)pTempPasswordBuff);
                //We don't know the security type so we use scan to connect method
                WifiCommManagerConnectToSSIDTask_Asynch(pTempSSIDBuff, SSIDLen, pTempPasswordBuff, PasswordLen, SOURCE_OF_ORIGIN_DEFAULT, 0);
            }
            else
            {
                G_SYS_DBG_LOG_V2("Direct Connect To: '%s' (Pass '%s')\r\n", (char *)pTempSSIDBuff, (char *)pTempPasswordBuff);
                //We know the security type so we use direct connect method
                //This is hidden SSID so we need to use direct method
                WifiCommManagerConnectToSSIDDirectTask_Asynch(pTempSSIDBuff, SSIDLen, pTempPasswordBuff, PasswordLen, SecurityType, SOURCE_OF_ORIGIN_DEFAULT, 0);
            }

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_WIFI_CONNECTING);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_V2("WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        else
        {
            G_SYS_DBG_LOG_V2("WARNING:(Line=%d) Unable to find ApSSID from DevInfo (%d)\r\n",
                             __LINE__,
                             DevInfoRet);
        }
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING:(Line=%d) Unable to allocate necessary memory in ConnectToWifi\r\n",
                         __LINE__);
    }

    if (pTempSSIDBuff)
    {
        dxMemFree(pTempSSIDBuff);
    }

    if (pTempPasswordBuff)
    {
        dxMemFree(pTempPasswordBuff);
    }

}

dxBOOL cUtlIsBleExecutionOfflineSupported( uint8_t* PeripheralJobExec )
{
    dxBOOL OfflineSupported = dxTRUE;

    BleStandardJobExecHeader_t* JobExecHeader = (BleStandardJobExecHeader_t*)PeripheralJobExec;

    if (JobExecHeader->ServiceUUID == DK_SERVICE_SYSTEM_UTILS_UUID)
    {
        switch (JobExecHeader->CharacteristicUUID)
        {
            case DK_CHARACTERISTIC_SYSTEM_UTIL_SECURITY_ACCESS_UUID:
            case DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID:
                OfflineSupported = dxFALSE;
            break;
        }
    }

    return OfflineSupported;
}

void cUtlParseAndDispatchContainerDataReport( dxContainer_t* pCont, uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber, dxBOOL OnlyWithLValueInContainer )
{
    dxBOOL          IsHybrid = dxTRUE;
    const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
    uint32_t BLEObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) pMainDeviceMacAddr->MacAddr, 8);

    if (BLEObjectID != pCont->pContMList->BLEObjectID)
    {
        IsHybrid = dxFALSE;
    }

    if (IsHybrid == dxTRUE)
    {
#if DEVICE_IS_HYBRID

        // Evaluate all data
        dxContainerM_t *p = pCont->pContMList;

        while (p)
        {
            dxContainerD_t *q = p->pContDList;
            while (q)
            {
                uint16_t LValueLen = (q->LValue) ? q->LValue->Length:0;
                if ((OnlyWithLValueInContainer == dxTRUE) && (LValueLen == 0))
                {
                    //We were ask to expect container with LValue but we did not found it.. report a warning
                    //maybe in some case this is normal but worth checking by reporting it.
                    G_SYS_DBG_LOG_V("WARNING:LcParseAndDispatchContainerDataReport found no LValue\r\n");
                }
                else
                {
                    uint16_t index = 0;
                    uint16_t HpDataReportSize = sizeof(HpDataExcContainerDataReport_t);
                    HpDataReportSize += (q->SValue) ? q->SValue->Length:0;
                    HpDataReportSize += (q->LValue) ? q->LValue->Length:0;

                    HpDataExcContainerDataReport_t* pHpDataReport = dxMemAlloc("HpDataReport", 1, HpDataReportSize);

                    pHpDataReport->ContMID = p->ContMID;
                    pHpDataReport->ContMType = p->ContMType;
                    pHpDataReport->ContDID = q->ContDID;
                    pHpDataReport->ContDType = q->ContDType;
                    pHpDataReport->DateTime = q->DateTime;
                    pHpDataReport->SValueLen = (q->SValue) ? q->SValue->Length:0;
                    pHpDataReport->LValueLen = (q->LValue) ? q->LValue->Length:0;
                    pHpDataReport->ContLVCRC = q->ContLVCRC;
                    if (pHpDataReport->SValueLen)
                    {
                        memcpy(pHpDataReport->CombinedValue + index, q->SValue->pData, q->SValue->Length);
                        index += q->SValue->Length;
                    }
                    if (pHpDataReport->LValueLen)
                    {
                        memcpy(pHpDataReport->CombinedValue + index, q->LValue->pData, q->LValue->Length);
                        index += q->LValue->Length;
                    }

                    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                    pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT;
                    pCmdMessage->MessageID = (uint32_t)TaskIdentificationNumber;
                    pCmdMessage->Priority = 0; //Normal
                    pCmdMessage->PayloadSize = HpDataReportSize;
                    pCmdMessage->PayloadData = (uint8_t*)pHpDataReport;

                    G_SYS_DBG_LOG_V2("HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT SO=%d, TI=%d\r\n", SourceOfOrigin, pCmdMessage->MessageID);

                    HpManagerSendTo_Client(pCmdMessage);
                    HpManagerCleanEventArgumentObj(pCmdMessage);
                }

                q = q->pNextContD;
            }

            p = p->pNextContM;
        }

#endif
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING:Non-Hybrid Peri. Container Handling NOT YET Implemented\r\n");
    }
}

#if DEVICE_IS_HYBRID
void cUtlUpdateHyBridPeripheralStatus( void )
{
    uint16_t Battery =  0;  //0 - Not a battery powered deviced
    uint16_t Status =   0;  //Normal
    uint16_t CurrentSignalStrength = SIGNAL_STRENGTH_VERY_WEAK;

    const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();

    int16_t rssi;
    rssi = WifiCommManagerGetWifiRSSI();
    G_SYS_DBG_LOG_V1("WIFI RSSI = %d\r\n", rssi );
    CONVERT_SIGNAL_STREGTH_ABS(rssi,CurrentSignalStrength)
    //We should not provide NONE becuase it should have some value as we are already connected to wifi network.
    if (CurrentSignalStrength == SIGNAL_STRENGTH_NONE)
    {
        G_SYS_DBG_LOG_V1("WARNING:(Line=%d) WIFI RSSI Strength NONE detected.. Something is not right\r\n", __LINE__);
        CurrentSignalStrength = SIGNAL_STRENGTH_VERY_WEAK;
    }

    ObjectDictionary_Lock ();
    uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) pMainDeviceMacAddr->MacAddr, sizeof (pMainDeviceMacAddr->MacAddr));
    ObjectDictionary_Unlock ();

    DKServerManager_UpdatePeripheralStatusInfo_Asynch (SOURCE_OF_ORIGIN_DEFAULT,
                                                       0,
                                                       &ObjectID,
                                                       CurrentSignalStrength,
                                                       Battery,
                                                       Status);
}

void cUtlAddHybridPeripheral( void )
{
	int16_t rssi;
    rssi = WifiCommManagerGetWifiRSSI();
    uint16_t CurrentSignalStrength = SIGNAL_STRENGTH_VERY_WEAK;

    CONVERT_SIGNAL_STREGTH_ABS(rssi,CurrentSignalStrength)

    const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();

    char        FwVersinData[16]; //16 byte should be more than enough for format "xx.xx.xx"
    uint16_t    FwVersinDataSize = 0;
    dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, FwVersinData, &FwVersinDataSize, dxFALSE);

    char HybridPeripheralNameInMacAddrDecStr[23] = {"HY#"};
    _MACAddrToDecimalStr(&HybridPeripheralNameInMacAddrDecStr[3], sizeof(HybridPeripheralNameInMacAddrDecStr) - 3, (uint8_t*)pMainDeviceMacAddr->MacAddr, sizeof(pMainDeviceMacAddr->MacAddr));

    uint16_t keyLength;
    uint8_t* key = dxMemAlloc ("uint8_t", 1, AES_ENC_DEC_BYTE * sizeof (uint8_t));
    dxBOOL utlResult = cUtlGetSecurityKey (
        key,
        &keyLength
    );
    if (utlResult != dxTRUE) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR, "Fail to call cUtlGetSecurityKey ()\r\n");
    }

    if (keyLength > 0) {
        if (
            DKServerManager_AddPeripheral_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                (uint8_t*)pMainDeviceMacAddr->MacAddr,
                sizeof(pMainDeviceMacAddr->MacAddr),
                HybridPeripheralNameInMacAddrDecStr,
                HYBRID_PERIPHERAL_PRODUCT_ID,
                CurrentSignalStrength,
                0, //0 for non battery device
                (FwVersinDataSize) ? FwVersinData:"1.0.0",
                key,
                keyLength
            ) != DKSERVER_STATUS_SUCCESS
        ) {
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_FATAL_ERROR,
                "WARNING:(Line=%d) DKServerManager_AddPeripheral_Asynch(Hybrid) FAILED\r\n",
                __LINE__
            );
        }
    } else {
        if (
            DKServerManager_AddPeripheral_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                (uint8_t*)pMainDeviceMacAddr->MacAddr,
                sizeof(pMainDeviceMacAddr->MacAddr),
                HybridPeripheralNameInMacAddrDecStr,
                HYBRID_PERIPHERAL_PRODUCT_ID,
                CurrentSignalStrength,
                0, //0 for non battery device
                (FwVersinDataSize) ? FwVersinData:"1.0.0",
                NULL,
                0
            ) != DKSERVER_STATUS_SUCCESS
        ) {
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_FATAL_ERROR,
                "WARNING:(Line=%d) DKServerManager_AddPeripheral_Asynch(Hybrid) FAILED\r\n",
                __LINE__
            );
        }
    }

    dxMemFree (key);

}
#endif

void cUtlAddGateway( void )
{
    float       Latitude = 0.0f;
    float       Longitude = 0.0f;
    uint16_t    DeviceNameSize = 128;
    uint16_t    FwVersionSize = 32;
    uint16_t    FwVersionCpBleSize = 32;
    uint16_t    GeoLocSize = sizeof(float);
    char* pTempDevNameBuff = dxMemAlloc("TempDevNameBuff", 1, DeviceNameSize);
    char* pTempFwVersionBuff = dxMemAlloc("TempFwVersionBuff", 1, FwVersionSize);
    char* pTempFwVersionCpBleBuff = dxMemAlloc("TempFwVersionCpBleBuff", 1, FwVersionCpBleSize);

    if (pTempDevNameBuff && pTempFwVersionBuff && pTempFwVersionCpBleBuff)
    {
        dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_NAME, pTempDevNameBuff, &DeviceNameSize, dxFALSE);
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            DeviceNameSize = 0; //Make sure we set the size to 0 t identify as no content is retrieved.
            G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_NAME\r\n",
                             __LINE__);
        }

        DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, pTempFwVersionBuff, &FwVersionSize, dxFALSE);
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            FwVersionSize = 0; //Make sure we set the size to 0 t identify as no content is retrieved.
            G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_FW_VERSION\r\n",
                             __LINE__);
        }

        DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, pTempFwVersionCpBleBuff, &FwVersionCpBleSize, dxFALSE);
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            FwVersionCpBleSize = 0; //Make sure we set the size to 0 t identify as no content is retrieved.
            G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION\r\n",
                             __LINE__);
        }

        DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LATITUDE, &Latitude, &GeoLocSize, dxFALSE);
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LATITUDE\r\n",
                             __LINE__);
        }

        DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LONGITUDE, &Longitude, &GeoLocSize, dxFALSE);
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LONGITUDE\r\n",
                             __LINE__);
        }

        G_SYS_DBG_LOG_V3("Adding Device with Name = %s, latitude = %f, longitude = %f\r\n", pTempDevNameBuff, Latitude, Longitude);

        G_SYS_DBG_LOG_V2("Adding Device GV=%s, BV=%s\r\n", pTempFwVersionBuff, pTempFwVersionCpBleBuff);

        DKServer_StatusCode_t ServerManagerRet = DKServerManager_AddGateway_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, pTempDevNameBuff, Latitude, Longitude, pTempFwVersionBuff, pTempFwVersionCpBleBuff );

        if (ServerManagerRet != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_V1("DKServerManager_AddGateway_Asynch failed with errorcode (%d)\r\n", ServerManagerRet);
        }
    }
    else
    {
        G_SYS_DBG_LOG_V1("WARNING:(Line=%d) Unable to alloc memory for DKServerManager_AddGateway_Asynch\r\n",
                         __LINE__);
    }

    if (pTempDevNameBuff)
        dxMemFree(pTempDevNameBuff);

    if (pTempFwVersionBuff)
        dxMemFree(pTempFwVersionBuff);

    if (pTempFwVersionCpBleBuff)
        dxMemFree(pTempFwVersionCpBleBuff);

}

//============================================================================
// Server Connect Procedure Gating Functions
//============================================================================
#define SERVER_CONNECT_PROCEDURE_GATING_DELAY_MIN       (0 * SECONDS)
#define SERVER_CONNECT_PROCEDURE_GATING_DELAY_MAX       (1 * MINUTES)
#define SERVER_CONNECT_PROCEDURE_GATING_DELAY_ADD       (10 * SECONDS)
#define SERVER_CONNECT_PROCEDURE_GATING_DELAY_RESET_THR (5 * MINUTES)

dxTime_t    ServerConnectAllInfoSynchedTime = 0;
dxTime_t    ServerConnectAttemptTime = 0;
dxTime_t    ServerConnectGatingDelay = SERVER_CONNECT_PROCEDURE_GATING_DELAY_MIN;

void cUtlServerConnectGating_AllInfoSynched (void) {
    ServerConnectAllInfoSynchedTime = dxTimeGetMS();
}


void cUtlServerConnectGating_ConnectAttemptStart (void) {

    dxTime_t CurrentTick = dxTimeGetMS();

    if (ServerConnectAttemptTime == 0) {

        // Initial ServerConnectGatingDelay when first run
        ServerConnectGatingDelay = SERVER_CONNECT_PROCEDURE_GATING_DELAY_MIN;

    } else if (ServerConnectAttemptTime > ServerConnectAllInfoSynchedTime) {

        // Did not reach all info sync, add delay to ServerConnectGatingDelay
        ServerConnectGatingDelay += SERVER_CONNECT_PROCEDURE_GATING_DELAY_ADD;
        if (ServerConnectGatingDelay > SERVER_CONNECT_PROCEDURE_GATING_DELAY_MAX) {
            ServerConnectGatingDelay = SERVER_CONNECT_PROCEDURE_GATING_DELAY_MAX;
        }
        G_SYS_DBG_LOG_V ("WARNING: All Info synced not reached or not last long, D:%ds\r\n", (ServerConnectGatingDelay / SECONDS));

    } else {

        // Reach all info sync, reset ServerConnectGatingDelay
        ServerConnectGatingDelay = SERVER_CONNECT_PROCEDURE_GATING_DELAY_MIN;
        G_SYS_DBG_LOG_V ("NOTE: All Info synced last long enough, D:%ds\r\n", (ServerConnectGatingDelay / SECONDS));

    }

    ServerConnectAttemptTime = CurrentTick + ServerConnectGatingDelay;

}


dxBOOL cUtlServerConnectGating_AllowStartConnect(void) {

    if (time_after (dxTimeGetMS (), ServerConnectAttemptTime)) {
        return dxTRUE;
    }

    return dxFALSE;

}


void cUtlServerConnectGating_ResetGatingDelay (void) {
    ServerConnectAttemptTime = 0;
}



//============================================================================
// HybribPeripheral Function
//============================================================================

#if DEVICE_IS_HYBRID

dxBOOL cUtlGetSecurityKey (
    uint8_t* key,
    uint16_t* keyLength
) {
    dxBOOL result = dxTRUE;
    uint16_t length;

#ifdef GENERATE_SECURITY_KEY

    if (securityKey == NULL) {

        securityKey = dxMemAlloc ("uint8_t", 1, AES_ENC_DEC_BYTE);

        dxDEV_INFO_RET_CODE devRet = dxDevInfo_GetData (DX_DEVICE_INFO_TYPE_CONCURRENT_PERIPHERAL_SECURITY_KEY,
                                                        securityKey,
                                                        &length,
                                                        dxTRUE);
        if ((devRet != DXDEV_INFO_SUCCESS) || (length != AES_ENC_DEC_BYTE)) {

            uint8_t* md2Key;
            uint16_t md2KeyLen;
            uint8_t* textBuffer;
            uint16_t textBufferLen;
            uint8_t  i;

#if SECURITY_KEY_RANDOMIZE
            dxAesContext_t aes128;
            dxTime_t time;
            const WifiCommDeviceMac_t* deviceMac = WifiCommManagerGetDeviceMac ();
            uint8_t* macaddr        = (unsigned char*) deviceMac->MacAddr;
            uint16_t macaddrLen     = sizeof (deviceMac->MacAddr);
            uint8_t* randomValue    = dxMemAlloc ("randomValue", 1, AES_ENC_DEC_BYTE);
            uint8_t* aesDataInOut   = dxMemAlloc ("aesDataInOut", 1, AES_ENC_DEC_BYTE);

            md2KeyLen = macaddrLen;
            md2Key = macaddr;
            textBufferLen = AES_ENC_DEC_BYTE + macaddrLen;
            textBuffer = dxMemAlloc ("textBuffer", 1, textBufferLen);
            aes128 =  dxAESSW_ContextAlloc ();
            time = dxTimeGetMS ();

            memcpy (aesDataInOut, &time, sizeof (time));
            for (i = 0; i < AES_ENC_DEC_BYTE; i++) {
                time++;
                securityKey[i] = ((uint8_t) time > 0) ? (uint8_t) time : 1;
            }

            dxAESSW_Setkey (aes128, securityKey, AES_ENC_DEC_BIT, DXAES_ENCRYPT);
            dxAESSW_CryptEcb (aes128, DXAES_ENCRYPT, aesDataInOut, randomValue);
            dxAESSW_ContextDealloc (aes128);

            // Pad the security code to the end of random number
            memcpy (textBuffer, randomValue, AES_ENC_DEC_BYTE);
            memcpy (textBuffer + AES_ENC_DEC_BYTE, macaddr, macaddrLen);

            dxMemFree (aesDataInOut);
            dxMemFree (randomValue);
#else
            uint16_t accessKeySize = 0;

            md2Key          = "80503525";
            md2KeyLen       = strlen( md2Key );

            textBufferLen   = 129;											// Type( 1 Byte ) + Access Key( 128 Bytes )
            textBuffer      = dxMemAlloc( "textBuffer", 1, textBufferLen );
            textBuffer[ 0 ] = HYBRID_PERIPHERAL_PRODUCT_ID;

            devRet = dxDevInfo_GetData( DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY,
                                        textBuffer + 1,
                                        &accessKeySize,
                                        dxFALSE );
            if ( devRet == DXDEV_INFO_SUCCESS ) {
                // Pad the security code to the end of random number
                textBufferLen = 1 + accessKeySize;
            }
#endif

            // Generate the key
            md2_hmac ((const unsigned char*) md2Key,
                      (size_t) md2KeyLen,
                      (const unsigned char*) textBuffer,
                      (size_t) textBufferLen,
                      securityKey);

            dxMemFree (textBuffer);

            devRet = dxDevInfo_Update (DX_DEVICE_INFO_TYPE_CONCURRENT_PERIPHERAL_SECURITY_KEY,
                                       securityKey,
                                       AES_ENC_DEC_BYTE,
                                       dxTRUE);
            if (devRet != DXDEV_INFO_SUCCESS) {
                G_SYS_DBG_LOG_V ("WARNING: Fail to update DX_DEVICE_INFO_TYPE_CONCURRENT_PERIPHERAL_SECURITY_KEY\r\n");
            }
        }
    }

    *keyLength = AES_ENC_DEC_BYTE;
    memcpy (key, securityKey, AES_ENC_DEC_BYTE);

#else

    *keyLength = 0;

#endif

    return result;
}


HpEventDataPayloadPackage_t IntHybridPeripheralDevInfoReportPayloadPackage(uint8_t* pAdDataPayload, uint32_t AdDataPayloadSize, uint16_t AdvHeaderFlag)
{
    HpEventDataPayloadPackage_t DevInfoReportPackage;
    memset(&DevInfoReportPackage, 0, sizeof(HpEventDataPayloadPackage_t));

    const WifiCommDeviceMac_t* pDeviceMac = WifiCommManagerGetDeviceMac();
    memcpy(DevInfoReportPackage.DeviceInfoReport.DevAddress.Addr, pDeviceMac->MacAddr, sizeof(DevAddress_t));
    DevInfoReportPackage.DeviceInfoReport.DevHandlerID = 0;
    DevInfoReportPackage.DeviceInfoReport.LastUpdatedTimeMs = 0;
    DevInfoReportPackage.DeviceInfoReport.rssi = BLE_RSSI_NORMAL;
    DevInfoReportPackage.DeviceInfoReport.Status.flag = 0x0000;

    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdDataLen = 5 + AdDataPayloadSize;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.Header.CompanyID = DK_COMPANY_DEXATEK;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.Header.ProductID = HYBRID_PERIPHERAL_PRODUCT_ID;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.Header.Flag = AdvHeaderFlag;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.Header.XorPayloadChecksum = 0;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.ExtDkFlag = 0;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.BatteryInfo = 0;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.PagingInfo = 0x0000;

    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[0] = DK_DATA_TYPE_SECURITY_MASK;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[1] = 0x00;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[2] = 0x00;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[3] = 0x00;
    DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[4] = 0x00;
    memcpy(&DevInfoReportPackage.DeviceInfoReport.DkAdvertisementData.AdData[5], pAdDataPayload, AdDataPayloadSize);

    return DevInfoReportPackage;
}


HpEventDataPayloadPackage_t cUtlHybridPeripheralDataTypeDataPayloadUpdateOnlyPackage(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t* pAdDataPayload, uint32_t AdDataPayloadSize)
{
    HpEventDataPayloadPackage_t DataTypeDataPayloadUpdate;
    DataTypeDataPayloadUpdate = IntHybridPeripheralDevInfoReportPayloadPackage(pAdDataPayload, AdDataPayloadSize, 0x0080);

    DataTypeDataPayloadUpdate.IdentificationNumber = IdentificationNumber;
    DataTypeDataPayloadUpdate.SourceOfOrigin = SourceOfOrigin;

    return DataTypeDataPayloadUpdate;
}


HpEventDataPayloadPackage_t cUtlHybridPeripheralHistoryEventDataPayloadPackage(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t* pAdDataPayload, uint32_t AdDataPayloadSize)
{
    HpEventDataPayloadPackage_t HistoryEventData;

    HistoryEventData = IntHybridPeripheralDevInfoReportPayloadPackage(pAdDataPayload, AdDataPayloadSize, 0x0000);

    HistoryEventData.IdentificationNumber = IdentificationNumber;
    HistoryEventData.SourceOfOrigin = SourceOfOrigin;

    return HistoryEventData;
}


dxBOOL cUtlHybridPeripheralGetDeviceGenericInfo (uint32_t messageId, uint32_t payloadLength, uint8_t* payload) {

    if (payloadLength && payload) {

        HpCmdMessageQueue_t* message = dxMemAlloc ("HpCmdMsg", 1, sizeof (HpCmdMessageQueue_t));
        message->CommandMessage = HP_CMD_DATAEXCHANGE_GET_DEVICE_GENERIC_INFO_RES;
        message->MessageID = messageId;
        message->Priority = 0; //Normal

        HpGetDeviceGenericInfoRequest_t* request = (HpGetDeviceGenericInfoRequest_t*) payload;
        HpDeviceGenericInfo_t* hpDeviceInfo = NULL;
        DeviceGenericInfo_t* deviceInfo = NULL;
        uint32_t hpDeviceInfoSize = 0;

#if CENTRAL_OFFLINE_SUPPORT
        deviceInfo = CentralOfflineSupportFindDeviceInfoByMac (request->DevAddress.Addr, B_ADDR_LEN_8);
#else
        deviceInfo = BLEDevKeeper_FindDeviceInfoByMac (request->DevAddress.Addr, B_ADDR_LEN_8);
#endif
        if (deviceInfo) {
            hpDeviceInfo = dxMemAlloc ("hpDeviceInfo", 1, sizeof (HpDeviceGenericInfo_t));
            if (hpDeviceInfo) {
                memcpy (&hpDeviceInfo->DevAddress, &deviceInfo->DevAddress, sizeof (DevAddress_t));
                hpDeviceInfo->DeviceType = deviceInfo->DeviceType;
                memcpy (&hpDeviceInfo->SecurityKey, &deviceInfo->SecurityKey, sizeof (SecurityKeyCodeContainer_t));
                hpDeviceInfoSize = sizeof (HpDeviceGenericInfo_t);
            }
            dxMemFree (deviceInfo);
        }

        message->PayloadSize = hpDeviceInfoSize;
        message->PayloadData = (uint8_t*) hpDeviceInfo;

        HpManagerSendTo_Client (message);
        HpManagerCleanEventArgumentObj (message);
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralDeviceInfoUpdate(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, DeviceInfoReport_t* DeviceKeeperInfoReport)
{
    if (DeviceKeeperInfoReport == NULL) {
        return dxFALSE;
    }

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_BLE_DEVICE_INFO_UPDATE;
    //pCmdMessage->MessageID = MessageID;
    pCmdMessage->MessageID = 0;
    //pCmdMessage->Priority = Priority; //Normal
    pCmdMessage->Priority = 0; //Normal

    DeviceInfoReport_t *pDeviceInfoBuf = dxMemAlloc("HpDevInfo", 1, sizeof(DeviceInfoReport_t));
    if(pDeviceInfoBuf == 0)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        dxMemFree(pCmdMessage);
        return dxFALSE;
    }

    memcpy(pDeviceInfoBuf, DeviceKeeperInfoReport, sizeof(DeviceInfoReport_t));

    pCmdMessage->PayloadSize = sizeof(DeviceInfoReport_t);
    pCmdMessage->PayloadData = (uint8_t*) pDeviceInfoBuf;

    HpManagerSendTo_Client(pCmdMessage);
//    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);

    HpManagerCleanEventArgumentObj(pCmdMessage);

    return dxTRUE;
}

dxBOOL cUtlHybridPeripheralDeviceKeeperListUpdate(uint32_t SourceOfOrigin, uint64_t IdentificationNumber, uint8_t *pUpdatedPeripheralData, uint16_t PeripheralDataLen)
{
    if (pUpdatedPeripheralData == NULL) {
        return dxFALSE;
    }

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    if(pCmdMessage == NULL)
    {
        G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
        return dxFALSE;
    }
    pCmdMessage->CommandMessage = HP_CMD_BLE_DEVICE_KEEPER_LIST_UPDATE;
    //pCmdMessage->MessageID = MessageID;
    pCmdMessage->MessageID = 0;
    //pCmdMessage->Priority = Priority; //Normal
    pCmdMessage->Priority = 0; //Normal

    if(PeripheralDataLen > 0)
    {
        uint8_t *pDeviceKeeperListBuf = dxMemAlloc("HpDevKpList", 1, PeripheralDataLen);
        if(pDeviceKeeperListBuf == 0)
        {
            G_SYS_DBG_LOG_V("%s out of buffer Line:%d\r\n", __FUNCTION__, __LINE__);
            dxMemFree(pCmdMessage);
            return dxFALSE;
        }

        memcpy(pDeviceKeeperListBuf, pUpdatedPeripheralData, PeripheralDataLen);
        pCmdMessage->PayloadData = (uint8_t*) pDeviceKeeperListBuf;
    }
    else
    {
        pCmdMessage->PayloadData = NULL;
    }
    pCmdMessage->PayloadSize = PeripheralDataLen;

    HpManagerSendTo_Client(pCmdMessage);
//    G_SYS_DBG_LOG_V("%s sent Line:%d\r\n", __FUNCTION__, __LINE__);

    HpManagerCleanEventArgumentObj(pCmdMessage);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralGetConcurrentPeripheralStatus (void) {
    HPManager_result_t hpResult;
    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralGetStatusTask_Asynch ();
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Fail to call BleManagerConcurrentPeripheralGetStatusTask_Asynch ()\r\n");
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralCustomizeConcurrentPeripheralBdAddress (void) {
    const WifiCommDeviceMac_t* deviceMac = WifiCommManagerGetDeviceMac ();
    BleCentralConnect_t* bleBdAddress = dxMemAlloc ("bleBdAddress", 1, B_ADDR_LEN_8 * sizeof (BleCentralConnect_t));

    memcpy (bleBdAddress->ConnectAddr, deviceMac->MacAddr, B_ADDR_LEN_8);
    bleBdAddress->AddressType = ADDRTYPE_PUBLIC;

    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralCustomizeBdAddressTask_Asynch (bleBdAddress,           /* bleBdAddress */
                                                                                                 1000,                   /* timeOutReport */
                                                                                                 0                       /* taskIdentificationNumber */ );
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (
            G_SYS_DBG_CATEGORY_UNKNOWN,
            G_SYS_DBG_LEVEL_FATAL_ERROR,
            "Fail to call BleManagerConcurrentPeripheralSetSecurityKeyTask_Asynch ()\r\n"
        );
    }

    dxMemFree (bleBdAddress);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralSecurityKey (void) {
    uint16_t keyLength;
    uint8_t* key = dxMemAlloc ("uint8_t", 1, AES_ENC_DEC_BYTE * sizeof (uint8_t));

    dxBOOL utlResult = cUtlGetSecurityKey (
        key,
        &keyLength
    );
    if (utlResult != dxTRUE) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR, "Fail to call cUtlGetSecurityKey ()\r\n");
    }

    if (keyLength > 0) {
        BLEManager_result_t bleResult = BleManagerConcurrentPeripheralSetSecurityKeyTask_Asynch (
            key,
            keyLength
        );
        if (bleResult != DEV_MANAGER_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN, G_SYS_DBG_LEVEL_FATAL_ERROR, "Fail to call BleManagerConcurrentPeripheralSetSecurityKeyTask_Asynch ()\r\n");
        }
    }

    dxMemFree (key);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralAdvertiseData (uint32_t payloadLength, uint8_t* payload) {

    if (payloadLength && payload) {

        DkAdvData_t* advertiseData = (DkAdvData_t*) payload;
        uint16_t keyLength;
        uint16_t manufectureDataLength = 0;
        uint8_t* key = dxMemAlloc ("uint8_t", 1, AES_ENC_DEC_BYTE * sizeof (uint8_t));
        uint8_t* workingData;
        uint8_t* manufectureData;
        uint8_t dataOut[AES_ENC_DEC_BYTE];
        uint8_t checksum;
        uint8_t i;

        // Encrypt payload if needed make sure AdDataLen >= 16
        if (DK_FLAG_IS_PAYLOAD_ENCRYPTED (advertiseData->Header.Flag)) {
            if (advertiseData->AdDataLen < 16) {
                // Add 0 padding
                for (i = advertiseData->AdDataLen; i < 16; i++) {
                    advertiseData->AdData[i] = 0;
                }
                advertiseData->AdDataLen = 16;
            }
        }

        // Calculate checksum
        workingData = advertiseData->AdData;
        checksum = *workingData;
        workingData++;
        for (i = 0; i < advertiseData->AdDataLen; i++) {
            checksum ^= *workingData;
            workingData++;
        }
        advertiseData->Header.XorPayloadChecksum = checksum;

        // Prepare manufacture data
        manufectureData = dxMemAlloc ("uint8_t", 1, sizeof (DkAdvData_t));
        workingData = manufectureData;
        memcpy (workingData, &advertiseData->Header, sizeof (DeviceDkPeripheralAdvHeader_t));
        workingData += sizeof (DeviceDkPeripheralAdvHeader_t);

        if (DK_FLAG_IS_CONTAIN_EXT_FLAG (advertiseData->Header.Flag)) {
            *workingData = advertiseData->ExtDkFlag;
            workingData++;
        }

        if (DK_FLAG_IS_CONTAIN_BATTERY_INFO (advertiseData->Header.Flag)) {
            *workingData = advertiseData->BatteryInfo;
            workingData++;
        }

        if (DK_FLAG_IS_CONTAIN_PAGING_INFO (advertiseData->Header.Flag)) {
            *workingData = LO_UINT16 (advertiseData->PagingInfo);
            workingData++;
            *workingData = HI_UINT16 (advertiseData->PagingInfo);
            workingData++;
        }
        memcpy (workingData, advertiseData->AdData, advertiseData->AdDataLen * sizeof (uint8_t));
        workingData += advertiseData->AdDataLen;
        manufectureDataLength = (uint16_t) (workingData - manufectureData);

        BLEManager_result_t bleResult = BleManagerConcurrentPeripheralSetAdvertiseDataTask_Asynch (BLE_ADVDATA_NO_NAME,    /* nameType */
                                                                                                   0,                      /* nameLength */
                                                                                                   NULL,                   /* name */
                                                                                                   0x06,                   /* flag */
                                                                                                   0,                      /* uuidParam */
                                                                                                   NULL,                   /* uuids */
                                                                                                   320,                    /* interval */
                                                                                                   manufectureDataLength,  /* manufLength */
                                                                                                   manufectureData         /* manuf */ );
        if (bleResult != DEV_MANAGER_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                              G_SYS_DBG_LEVEL_FATAL_ERROR,
                              "Fail to call BleManagerConcurrentPeripheralSetAdvertiseDataTask_Asynch ()\r\n");
        }

        dxMemFree (manufectureData);
        dxMemFree (key);
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralSetConcurrentPeripheralScanResponse (void) {
    const WifiCommDeviceMac_t* deviceMac;
    uint8_t scanResponse[16];
    uint8_t* pointer = NULL;

    deviceMac = WifiCommManagerGetDeviceMac ();

    memset (scanResponse, 0, 16);
    scanResponse[0] = 0x13;     // The 16th byte of UUID base
    scanResponse[1] = 0xDB;     // The 15th byte of UUID base
    scanResponse[2] = 0xBB;     // The 14th byte of UUID base
    scanResponse[3] = 0x6E;     // The 13th byte of UUID base
    scanResponse[4] = 0x5A;     // The 12th byte of UUID base
    scanResponse[5] = 0x60;     // The 11th byte of UUID base
    scanResponse[6] = 0xD3;     // The 10th byte of UUID base
    scanResponse[7] = 0xAF;     // The 9th byte of UUID base

    // The 1th to 8th byte of UUID base is used as MAC address
    pointer = &scanResponse[8];
    *pointer = deviceMac->MacAddr[7]; pointer++;
    *pointer = deviceMac->MacAddr[6]; pointer++;
    *pointer = deviceMac->MacAddr[5]; pointer++;
    *pointer = deviceMac->MacAddr[4]; pointer++;
    *pointer = deviceMac->MacAddr[3]; pointer++;
    *pointer = deviceMac->MacAddr[2]; pointer++;
    *pointer = deviceMac->MacAddr[1]; pointer++;
    *pointer = deviceMac->MacAddr[0]; pointer++;

    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralSetScanResponseTask_Asynch (BLE_ADVDATA_NO_NAME,         /* nameType */
                                                                                              0,                           /* nameLength */
                                                                                              NULL,                        /* name */
                                                                                              0x41,                        /* uuidParam */
                                                                                              scanResponse,                /* uuids */
                                                                                              0,                           /* manufLength */
                                                                                              NULL                         /* manuf */ );
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Fail to call BleManagerConcurrentPeripheralSetScanResponseTask_Asynch ()\r\n");
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralAddConcurrentPeripheralServiceCharacteristic (uint32_t payloadLength, uint8_t* payload) {
    if (payloadLength && payload) {
        HPManager_result_t hpResult;
        BleServiceCharacteristic_t serviceCharacteristic;
        HpConcurrentPeripheralServiceCharacteristicRequest_t* hpServiceCharacteristic = (HpConcurrentPeripheralServiceCharacteristicRequest_t*) payload;
        uint8_t i;

        serviceCharacteristic.serviceUuid = hpServiceCharacteristic->serviceUuid;
        serviceCharacteristic.characteristicCount = hpServiceCharacteristic->characteristicCount;
        for (i = 0; i < serviceCharacteristic.characteristicCount; i++) {
            serviceCharacteristic.characteristics[i].uuid = hpServiceCharacteristic->characteristics[i].uuid;
            serviceCharacteristic.characteristics[i].property = hpServiceCharacteristic->characteristics[i].property;
            serviceCharacteristic.characteristics[i].reserveSpace = hpServiceCharacteristic->characteristics[i].reserveSpace;
        }

        BLEManager_result_t bleResult = BleManagerConcurrentPeripheralAddServiceCharacteristicTask_Asynch (&serviceCharacteristic);
        if (bleResult != DEV_MANAGER_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                              G_SYS_DBG_LEVEL_FATAL_ERROR,
                              "Fail to call BleManagerConcurrentPeripheralAddServiceCharacteristicTask_Asynch ()\r\n");
        }
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralEnableConcurrentPeripheralSystemUtils (uint32_t payloadLength, uint8_t* payload) {
    if (payloadLength && payload) {
        HPManager_result_t hpResult;
        HpConcurrentPeripheralSystemUtilsReq_t* systemUtilReq = (HpConcurrentPeripheralSystemUtilsReq_t*) payload;
        BLEManager_result_t bleResult = BleManagerConcurrentPeripheralEnableSystemUtilsTask_Asynch (&systemUtilReq->mask);
        if (bleResult != DEV_MANAGER_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                              G_SYS_DBG_LEVEL_FATAL_ERROR,
                              "Fail to call BleManagerConcurrentPeripheralEnableSystemUtilsTask_Asynch ()\r\n");
        }
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralStartConcurrentPeripheral (void) {
    HPManager_result_t hpResult;
    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralStartTask_Asynch ();
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Fail to call BleManagerConcurrentPeripheralStartTask_Asynch ()\r\n");
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralStopConcurrentPeripheral (void) {
    HPManager_result_t hpResult;
    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralStopTask_Asynch ();
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Fail to call BleManagerConcurrentPeripheralStopTask_Asynch ()\r\n");
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralDisconnectConcurrentPeripheral (void) {
    HPManager_result_t hpResult;
    BLEManager_result_t bleResult = BleManagerConcurrentPeripheralDisconnectTask_Asynch ();
    if (bleResult != DEV_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                          G_SYS_DBG_LEVEL_FATAL_ERROR,
                          "Fail to call BleManagerConcurrentPeripheralDisconnectTask_Asynch ()\r\n");
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralTransmitConcurrentPeripheralData (uint32_t payloadLength, uint8_t* payload) {
    if (payloadLength && payload) {
        HPManager_result_t hpResult;
        HpConcurrentPeripheralData_t* hpData = (HpConcurrentPeripheralData_t*) payload;
        BLEManager_result_t bleResult = BleManagerConcurrentPeripheralDataOutTask_Asynch (DATA_TRANSFER_MSG_TYPE_CONCURRENT_PERIPHERAL,                            /* MsgType */
                                                                                          hpData->data,                 /* Payload */
                                                                                          hpData->dataLength,           /* length */
                                                                                          1000,                         /* TimeOutReport */
                                                                                          0                             /* TaskIdentificationNumber */ );
        if (bleResult != DEV_MANAGER_SUCCESS) {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_UNKNOWN,
                              G_SYS_DBG_LEVEL_FATAL_ERROR,
                              "Fail to call BleManagerConcurrentPeripheralStopTask_Asynch ()\r\n");
        }
    }

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralReceiveConcurrentPeripheralData (uint16_t payloadLength, uint8_t* payload) {

    if (payload == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralData_t* hpData = dxMemAlloc ("hpData", 1, sizeof (HpConcurrentPeripheralData_t));
    hpData->dataLength = payloadLength;
    memcpy (hpData->data, payload, payloadLength);

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_DATA_IN_UPDATE;
    message->PayloadData = (uint8_t*) hpData;
    message->PayloadSize = sizeof (HpConcurrentPeripheralData_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralStatusResponse (BleConcurrentPeripheralGetStatusResponse_t* statusResponse) {

    if (statusResponse == NULL) {
        return dxFALSE;
    }

    eHpBleConcurrentPeripheralStatus* hpStatus = dxMemAlloc ("hpStatus", 1, sizeof (eHpBleConcurrentPeripheralStatus));

    switch (statusResponse->state) {
        case BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT : *hpStatus = HP_BLE_CONCURRENT_PERIPHERAL_STATE_NOT_SUPPORT; break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_IDLE        : *hpStatus = HP_BLE_CONCURRENT_PERIPHERAL_STATE_IDLE; break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_ADVERTISING : *hpStatus = HP_BLE_CONCURRENT_PERIPHERAL_STATE_ADVERTISING; break;
        case BLE_CONCURRENT_PERIPHERAL_STATE_CONNECTED   : *hpStatus = HP_BLE_CONCURRENT_PERIPHERAL_STATE_CONNECTED; break;
        default : *hpStatus = HP_BLE_CONCURRENT_PERIPHERAL_STATE_NUM; break;
    }

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_GET_STATUS_RES;
    message->PayloadData = (uint8_t*) hpStatus;
    message->PayloadSize = sizeof (eHpBleConcurrentPeripheralStatus);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralBdAddressResponse (SystemBdAddressResponse_t* bdAddressResponse) {

    if (bdAddressResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralBdAddressResponse_t* hpBdAddressResponse = dxMemAlloc ("hpBdAddressResponse", 1, sizeof (HpConcurrentPeripheralBdAddressResponse_t));

    hpBdAddressResponse->result = bdAddressResponse->result;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS_RES;
    message->PayloadData = (uint8_t*) hpBdAddressResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralBdAddressResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralSecurityKeyResponse (BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse) {

    if (securityKeyResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralSecurityKeyResponse_t* hpSecurityKeyResponse = dxMemAlloc ("hpSecurityKeyResponse", 1, sizeof (HpConcurrentPeripheralSecurityKeyResponse_t));

    hpSecurityKeyResponse->keyLength = securityKeyResponse->keyLength;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_SET_SECURITY_KEY_RES;
    message->PayloadData = (uint8_t*) hpSecurityKeyResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralSecurityKeyResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralAdvertiseDataResponse (BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse) {

    if (advertiseDataResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralAdvertiseDataResponse_t* hpAdvertiseDataResponse = dxMemAlloc ("hpAdvertiseDataResponse", 1, sizeof (HpConcurrentPeripheralAdvertiseDataResponse_t));

    hpAdvertiseDataResponse->totalBytes = advertiseDataResponse->totalBytes;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_SET_ADVERTISE_DATA_RES;
    message->PayloadData = (uint8_t*) hpAdvertiseDataResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralAdvertiseDataResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralScanResponseResponse (BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse) {

    if (scanResponseResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralScanResponseResponse_t* hpScanResponseResponse = dxMemAlloc ("hpScanResponseResponse", 1, sizeof (HpConcurrentPeripheralScanResponseResponse_t));

    hpScanResponseResponse->totalBytes = scanResponseResponse->totalBytes;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_SET_SCAN_RESPONSE_RES;
    message->PayloadData = (uint8_t*) hpScanResponseResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralScanResponseResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralServiceCharacteristicResponse (BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse) {

    if (serviceCharacteristicResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralServiceCharacteristicResponse_t* hpServiceCharacteristicResponse = dxMemAlloc ("hpServiceCharacteristicResponse", 1, sizeof (HpConcurrentPeripheralServiceCharacteristicResponse_t));

    hpServiceCharacteristicResponse->serviceUuid = serviceCharacteristicResponse->serviceUuid;
    hpServiceCharacteristicResponse->characteristicCount = serviceCharacteristicResponse->characteristicCount;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_RES;
    message->PayloadData = (uint8_t*) hpServiceCharacteristicResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralServiceCharacteristicResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}


dxBOOL cUtlHybridPeripheralConcurrentPeripheralSystemUtilsResponse (BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse) {

    if (systemUtilsResponse == NULL) {
        return dxFALSE;
    }

    HpConcurrentPeripheralSystemUtilsResponse_t* hpSystemUtilsResponse = dxMemAlloc ("hpSystemUtilsResponse", 1, sizeof (HpConcurrentPeripheralSystemUtilsResponse_t));

    hpSystemUtilsResponse->enableMask = systemUtilsResponse->enableMask;

    HpCmdMessageQueue_t* message = dxMemAlloc ("message", 1, sizeof (HpCmdMessageQueue_t));
    message->Priority = 0; //Normal
    message->MessageID = 0;
    message->CommandMessage = HP_CMD_CONCURRENT_PERIPHERAL_ENABLE_SYSTEM_UTILS_RES;
    message->PayloadData = (uint8_t*) hpSystemUtilsResponse;
    message->PayloadSize = sizeof (HpConcurrentPeripheralSystemUtilsResponse_t);

    //Send the message to HybridPeripheral
    HpManagerSendTo_Client (message);
    HpManagerCleanEventArgumentObj (message);

    return dxTRUE;
}

#endif //DEVICE_IS_HYBRID
