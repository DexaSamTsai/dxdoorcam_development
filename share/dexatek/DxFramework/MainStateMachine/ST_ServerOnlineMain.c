//============================================================================
// File: ST_ServerOnlineMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"

#if MQTT_IS_ENABLED
#include "MqttManager.h"
#endif

#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#include "HpData_Exchange.h"
#include "HpJobBridge.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "BLERepeater.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "dxDeviceInfoManager.h"
#include "CentralStateMachineAutomata.h"
#include "SimpleTimedStateMachine.h"
#include "DataDrivenStateMachine.h"
#include "OfflineSupport.h"
#include "dxContainer.h"
#include "ST_CommonUtils.h"



//============================================================================
// Function Definition
//============================================================================

DataDrivenStateMachineResult GetPeripheral_CheckGateway (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetPeripheral_CheckPeripheral (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetPeripheral_CheckContainer (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetPeripheral_CheckIteration (void* data, dxBOOL isFirstCall);


DataDrivenStateMachineResult GetScheduleJob_CheckStatus (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetScheduleJob_CheckJob (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetScheduleJob_CheckIteration (void* data, dxBOOL isFirstCall);


DataDrivenStateMachineResult GetAdvanceJob_CheckStatus (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetAdvanceJob_CheckJob (void* data, dxBOOL isFirstCall);
DataDrivenStateMachineResult GetAdvanceJob_CheckIteration (void* data, dxBOOL isFirstCall);


SimpleStateMachine_result_t SubState_OnlineReLogIn(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineHostRedirect(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineGetFwVersion(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineGetUserInfo(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineGetPeripheral(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineGetScheduleJob(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineGetAdvJob(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OnlineAllInfoSynched(void* data, dxBOOL isFirstCall);


SimpleStateMachine_result_t SubState_RoutineCheck_UnpairPeripheral(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RoutineCheck_UpgradeFirmware(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RoutineCheck_CycleCompleted(void* data, dxBOOL isFirstCall);


#if DEVICE_IS_HYBRID
void HPClientFrwStatusReportServerReady(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber);
#endif



//============================================================================
// Constants
//============================================================================

//Get Peripheral Request
#define GET_PERIPHERAL_STARTING_INDEX                   1   //NOTE: First item starts at 1 (NOT 0)
#define GET_PERIPHERAL_MAX_PER_REQUEST                  8

//Scheduled Job Request
#define GETSCHEDULE_JOB_STARTING_INDEX                  1   //NOTE: First item starts at 1 (NOT 0)
#define GETSCHEDULE_JOB_MAX_PER_REQUEST                 6

//Advance Job Request
#define GETADVANCE_JOB_STARTING_INDEX           1   //NOTE: First item starts at 1 (NOT 0)
#define GETADVANCE_JOB_MAX_PER_REQUEST          6

//Get Job Interval
#define GET_NEW_JOB_INTERVAL_DURING_ACTIVE_USER          (2*SECONDS)
#define GET_NEW_JOB_INTERVAL_WHEN_NO_ACTIVE_USER         (4*SECONDS)

//Other interval definition
#define SERVER_TIME_SYNCH_INTERVALIN_MS         (60*60*SECONDS) //Synch every 1 hours



//============================================================================
// Enum
//============================================================================

typedef enum
{
    GET_PERIPHERAL_IDLE           = 0,

    GET_PERIPHERAL_CHECK_GATEWAY,
    GET_PERIPHERAL_CHECK_PERIPHERAL,
    GET_PERIPHERAL_CHECK_CONTAINER,
    GET_PERIPHERAL_CHECK_ITERATION,

} GetPeripheralStateCode;

typedef enum
{
    GET_SCHEDULEJOB_IDLE           = 0,

    GET_SCHEDULEJOB_CHECK_STATUS,
    GET_SCHEDULEJOB_CHECK_JOB,
    GET_SCHEDULEJOB_CHECK_ITERATION,

} GetScheduleJobStateCode;

typedef enum
{
    GET_ADVANCEJOB_IDLE           = 0,

    GET_ADVANCEJOB_CHECK_STATUS,
    GET_ADVANCEJOB_CHECK_JOB,
    GET_ADVANCEJOB_CHECK_ITERATION,

} GetAdvanceJobStateCode;

typedef enum
{
    ONLINE_SUBSTATE_IDLE           = 0,
    ONLINE_SUBSTATE_RELOGIN,
    ONLINE_SUBSTATE_HOST_REDIRECT,
    ONLINE_SUBSTATE_GET_PERIPHERAL,
    ONLINE_SUBSTATE_GET_SCHEDULE_JOB,
    ONLINE_SUBSTATE_GET_ADVANCE_JOB,
    ONLINE_SUBSTATE_GET_FW_VERSION,
    ONLINE_SUBSTATE_GET_USER_INFO,
    ONLINE_SUBSTATE_ALL_INFO_SYNCHED,

    ONLINE_SUBSTATE_RESET,

} Online_SubState_Code_t;

typedef enum
{
    ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL  = 0,
    ONLINE_SUBSTATE_ROUTINE_CHECK_UPGRADEFIRMWARE,
    ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED,

} Online_SubState_Routine_Check_t;

typedef enum {
    ONLINE_SERVER_LOCATION_MAIN  = 0,
    ONLINE_SERVER_LOCATION_QA,
} Online_Server_Location_t;



//============================================================================
// Structure
//============================================================================

typedef struct
{
    uint16_t        Taken;
    uint64_t        ScheduleSEQNO;

} DeleteScheduleJobInfo_t;

typedef struct
{
    uint16_t        TotalJobSlot;
    DeleteScheduleJobInfo_t*    pJobInfo;

} ScheduleJobDeleteList_t;



//============================================================================
// Global Var
//============================================================================

FwNewUpdateAvailStatus_t    FwNewUpdateAvailabilityStatus = FR_FW_UPDATE_AVAILABLE_NONE;
dxBOOL                      mDeviceFwVersionUpdateCheck = dxFALSE;

SimpleStateHandler      OnlineSubStateHandler = NULL;
SimpleStateHandler      OnlineSubStateRoutineCheckHandler = NULL;

static DataDrivenStateMachineTable getPeripheralStateMachineTable[] =
{
    {GET_PERIPHERAL_IDLE, NULL},
    {GET_PERIPHERAL_CHECK_GATEWAY, GetPeripheral_CheckGateway},
    {GET_PERIPHERAL_CHECK_PERIPHERAL, GetPeripheral_CheckPeripheral},
    {GET_PERIPHERAL_CHECK_CONTAINER, GetPeripheral_CheckContainer},     // Temp state only process once
    {GET_PERIPHERAL_CHECK_ITERATION, GetPeripheral_CheckIteration},

    //MUST BE IN THE LAST STATE ITEM
    {DATA_DRIVEN_STATE_INVALID, NULL}                                   //END of state Table - This is a MUST
};

static DataDrivenStateMachineTable getScheduleJobStateMachineTable[] =
{
    {GET_SCHEDULEJOB_IDLE, NULL},
    {GET_SCHEDULEJOB_CHECK_STATUS, GetScheduleJob_CheckStatus},
    {GET_SCHEDULEJOB_CHECK_JOB, GetScheduleJob_CheckJob},
    {GET_SCHEDULEJOB_CHECK_ITERATION, GetScheduleJob_CheckIteration},

    //MUST BE IN THE LAST STATE ITEM
    {DATA_DRIVEN_STATE_INVALID, NULL}                                   //END of state Table - This is a MUST
};

static DataDrivenStateMachineTable getAdvanceJobStateMachineTable[] =
{
    {GET_ADVANCEJOB_IDLE, NULL},
    {GET_ADVANCEJOB_CHECK_STATUS, GetAdvanceJob_CheckStatus},
    {GET_ADVANCEJOB_CHECK_JOB, GetAdvanceJob_CheckJob},
    {GET_ADVANCEJOB_CHECK_ITERATION, GetAdvanceJob_CheckIteration},

    //MUST BE IN THE LAST STATE ITEM
    {DATA_DRIVEN_STATE_INVALID, NULL}                                   //END of state Table - This is a MUST
};

static simple_st_machine_table_t OnlineSubStateProcessFuncTable[] =
{
    {ONLINE_SUBSTATE_IDLE, NULL, 0},
    {ONLINE_SUBSTATE_RELOGIN, SubState_OnlineReLogIn, 15*SECONDS},
    {ONLINE_SUBSTATE_HOST_REDIRECT, SubState_OnlineHostRedirect, 15*SECONDS},
    {ONLINE_SUBSTATE_GET_PERIPHERAL, SubState_OnlineGetPeripheral, 5*SECONDS},
    {ONLINE_SUBSTATE_GET_SCHEDULE_JOB, SubState_OnlineGetScheduleJob, 5*SECONDS},
    {ONLINE_SUBSTATE_GET_ADVANCE_JOB, SubState_OnlineGetAdvJob, 5*SECONDS},
    {ONLINE_SUBSTATE_GET_FW_VERSION, SubState_OnlineGetFwVersion, 5*SECONDS},
    {ONLINE_SUBSTATE_GET_USER_INFO, SubState_OnlineGetUserInfo, 5*SECONDS},
    {ONLINE_SUBSTATE_ALL_INFO_SYNCHED, SubState_OnlineAllInfoSynched, 15*SECONDS},

    {ONLINE_SUBSTATE_RESET, cUtlSubState_Reset, 0},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};


static simple_st_machine_table_t OnlineSubStateRoutineCheckProcessFuncTable[] =
{
    {ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL, SubState_RoutineCheck_UnpairPeripheral, 0},
    {ONLINE_SUBSTATE_ROUTINE_CHECK_UPGRADEFIRMWARE, SubState_RoutineCheck_UpgradeFirmware, 0},
    {ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED, SubState_RoutineCheck_CycleCompleted, 1*SECONDS},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};

//Get Peripheral Request
DataDrivenStateMachine*     peripheralStateMachine = NULL;
uint16_t                    peripheralExpectedItemCountForCurrentRequest = 0;
uint16_t                    peripheralExpectedTotal = 0;
uint16_t                    peripheralNextItemIndex = 1;    //First item is 1 (NOT 0)
uint64_t                    peripheralUpdateTime = 0;

//Schedule Job Request
DataDrivenStateMachine*     scheduleJobStateMachine = NULL;
uint16_t                    scheduleJobExpectedItemCountForCurrentRequest = 0;
uint16_t                    scheduleJobExpectedTotal = 0;
uint16_t                    scheduleJobNextItemIndex = 1;       //First item is 1 (NOT 0)
uint64_t                    scheduleJobUpdateTime = 0;
ScheduleJobDeleteList_t     scheduleJobDeleteList;

//Advance Job Request
DataDrivenStateMachine*     advanceJobStateMachine = NULL;
uint16_t                    advanceJobExpectedItemCountForCurrentRequest = 0;
uint16_t                    advanceJobExpectedTotal = 0;
uint16_t                    advanceJobNextItemIndex = 1;    //First item is 1 (NOT 0)
uint64_t                    advanceJobUpdateTime = 0;
dxBOOL                      advanceJobIncomplete = dxFALSE;

//Unpair Peripheral Scan
dxTime_t                    UnpairScanStartedTime = 0;
uint16_t                    UnpairScanDiscoveryTimeMaxInSecond = 0;

//Temporary storage for Task info that require handling that is not in the same run loop
TaskInfo_t                  OnlineTaskInfoStorage[MAX_TASK_INFO_STORAGE_SLOT];

// MqttManager
dxBOOL activeUserDetected = dxFALSE;

#if DEVICE_IS_HYBRID
//Server Ready HP message sent flag
dxBOOL                      bHpClientFrwServerReadyReported = dxFALSE;
#endif //#if DEVICE_IS_HYBRID

Online_Server_Location_t	Online_Server_Location	= ONLINE_SERVER_LOCATION_MAIN;

//============================================================================
// Internal Function
//============================================================================

dxBOOL  IntServerStateReadyCheck(void)
{
    dxBOOL ServerStateReady = dxTRUE;

    if (DKServerManager_GetCurrentState() != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        ServerStateReady = dxFALSE;

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Wrong server state=%d\r\n", DKServerManager_GetCurrentState());

        //Change to Offline
        CentralStateMachine_ChangeState(STATE_OFFLINE, NULL, 0);
    }

    return ServerStateReady;
}

dxBOOL IntCentralFwUpdateRequired (uint8_t* pCentralFirmwareVersion)
{
    dxBOOL UpdateIsRequired = dxFALSE;

    if (pCentralFirmwareVersion)
    {
        DKServer_StatusCode_t FwVersionCompareResult = DKServerManager_FirmwareVersionCompare((uint8_t*)RTK_WIFI_CURRENT_FW_VERSION, pCentralFirmwareVersion);
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "LocalVer = %s Server Ver = %s\r\n", RTK_WIFI_CURRENT_FW_VERSION ,pCentralFirmwareVersion);
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "FwVer Compare Result = %d\r\n", FwVersionCompareResult);

        //If the version from server is higher.
        if (FwVersionCompareResult == DKSERVER_STATUS_SUCCESS)
        {
            UpdateIsRequired = dxTRUE;
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) Latest FW Version from Server/local is incomplete\r\n",
                         __LINE__);
    }

    return   UpdateIsRequired;
}


dxBOOL IntBleFwUpdateRequired (uint8_t* pBleFirmwareVersion)
{
    dxBOOL UpdateIsRequired = dxFALSE;
    SystemInfoReportObj_t SystemInfoContainer;

    BLEManager_result_t bleresult = BleManagerSystemInfoGet(&SystemInfoContainer);

    if (bleresult == DEV_MANAGER_SUCCESS)
    {
        if (pBleFirmwareVersion && !BleManager_SBL_Enable())
        {
            DKServer_StatusCode_t FwVersionCompareResult = DKServerManager_FirmwareVersionCompare((uint8_t*)SystemInfoContainer.Version, pBleFirmwareVersion);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Ble LocalVer = %s Server Ver = %s\r\n", SystemInfoContainer.Version ,pBleFirmwareVersion);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "FwVer Compare Result = %d\r\n", FwVersionCompareResult);

            //If the version from server is higher.
            if (FwVersionCompareResult == DKSERVER_STATUS_SUCCESS)
            {
                UpdateIsRequired = dxTRUE;
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Latest BLE FW Version from Server/local is incomplete\r\n",
                             __LINE__);
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) IntBleFwUpdateRequired cannot obtain ble fw ver.\r\n",
                         __LINE__);
    }

    return   UpdateIsRequired;
}

#if DEVICE_IS_HYBRID
void HPClientFrwStatusReportServerReady(uint16_t SourceOfOrigin, uint64_t TaskIdentificationNumber)
{
    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_FRW_STATUS_REPORT_SERVER_READY;
    pCmdMessage->MessageID = (uint32_t)TaskIdentificationNumber;
    pCmdMessage->Priority = 0; //Normal
    pCmdMessage->PayloadSize = 0;
    pCmdMessage->PayloadData = 0;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "HP_CMD_DATAEXCHANGE_SERVER_READY SO=%d, TI=%d\r\n", SourceOfOrigin, pCmdMessage->MessageID);

    HpManagerSendTo_Client(pCmdMessage);
    HpManagerCleanEventArgumentObj(pCmdMessage);
}
#endif

dxBOOL IntIterateDeleteScheduleJobListExecution()
{
    dxBOOL Result = dxTRUE;
    dxBOOL DeleteItemFound = dxFALSE;
    uint16_t i = 0;
    for (i = 0; i < scheduleJobDeleteList.TotalJobSlot; i++)
    {
        //Is the slot taken?
        if (scheduleJobDeleteList.pJobInfo[i].Taken)
        {
            //NOTE: We are not going to check the result because we will always have the chance to reexecute when we login again during token session expire.
            //      also once this get executed we will call this function (IterateDeleteScheduleJobListExecution) again when we receive DKMSG_DELETESCHEDULEJOB
            //      until we are done with it.
            /*
            struct ScheduleSequenceNumber_s ScheduleSequenceNumber = _UINT64ScheduleSeqNoToStruct(scheduleJobDeleteList.pJobInfo[i].ScheduleSEQNO);
            G_SYS_DBG_LOG_V1("Delete ScheduleSEQNO-PeripheralUniqueID: %ld, ", ScheduleSequenceNumber.PeripheralUniqueID);
            G_SYS_DBG_LOG_NV1("Delete ScheduleSEQNO-UniqueSequenceID: %d\n", ScheduleSequenceNumber.UniqueSequenceID);
            */
            DKServerManager_DeleteScheduleJob_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, scheduleJobDeleteList.pJobInfo[i].ScheduleSEQNO);
            scheduleJobDeleteList.pJobInfo[i].Taken = 0; //mark as clean
            DeleteItemFound = dxTRUE;
            break;
        }
    }

    //If we didn't find any item left then all is being executed so we free the memory
    if (DeleteItemFound == dxFALSE)
    {
        if (scheduleJobDeleteList.pJobInfo)
        {
            dxMemFree(scheduleJobDeleteList.pJobInfo);
            scheduleJobDeleteList.pJobInfo = NULL;
        }
        scheduleJobDeleteList.TotalJobSlot = 0;
    }

    return Result;
}


void IntTaskInfoSave(int16_t DevIndex, uint64_t IndentificationNumber)
{
    dxBOOL      SaveSuccess = dxFALSE;
    dxTime_t    OldestTime = 0;
    int16_t     SlotIndexOfOldestTime = -1;
    uint8_t i = 0;
    for (i = 0; i < MAX_TASK_INFO_STORAGE_SLOT; i++)
    {
        if (OnlineTaskInfoStorage[i].Time == 0)
        {
            OldestTime = dxTimeGetMS();
            OnlineTaskInfoStorage[i].DevHandlerID = DevIndex;
            OnlineTaskInfoStorage[i].IdentificationNumber = IndentificationNumber;
            SaveSuccess = dxTRUE;

            //Clean/reset the oldest time slot info
            OldestTime = 0;
            SlotIndexOfOldestTime = -1;

            break;
        }

        if ((OldestTime == 0) || (OnlineTaskInfoStorage[i].Time < OldestTime))
        {
            OldestTime = OnlineTaskInfoStorage[i].Time;
            SlotIndexOfOldestTime = i;
        }
    }

    if ((SlotIndexOfOldestTime != -1) && (OldestTime != 0))
    {
        if (cUtlIsSysTimeGreaterThan(OldestTime, TASK_INFO_MAX_STORAGE_TIME_IN_SEC) == dxTRUE)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "TaskInfo storage index max time reached and replaced\r\n");
            OnlineTaskInfoStorage[SlotIndexOfOldestTime].DevHandlerID = DevIndex;
            OnlineTaskInfoStorage[SlotIndexOfOldestTime].IdentificationNumber = IndentificationNumber;
            OnlineTaskInfoStorage[SlotIndexOfOldestTime].Time = dxTimeGetMS();

            SaveSuccess = dxTRUE;
        }
    }

    if (!SaveSuccess)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) unable to SaveTaskInfo due to insufficient sapce\r\n", __LINE__);
    }
}


int16_t IntOnlineTaskInfoDevIndexGetAndClear(uint64_t IndentificationNumber)
{

    int16_t     DevIndex = -1;
    uint8_t     i = 0;
    for (i = 0; i < MAX_TASK_INFO_STORAGE_SLOT; i++)
    {
        if (OnlineTaskInfoStorage[i].IdentificationNumber == IndentificationNumber)
        {
            DevIndex = OnlineTaskInfoStorage[i].DevHandlerID;

            memset(&OnlineTaskInfoStorage[i], 0, sizeof(TaskInfo_t));
            break;
        }
    }

    return DevIndex;
}

//============================================================================
// SubState Function
//============================================================================

DataDrivenStateMachineResult GetPeripheral_CheckGateway (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetPeripheral_CheckGateway, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_ITERATION);
        return peripheralStateMachine->process (peripheralStateMachine, data);
    }

    // Check DKJOBTYPE_GATEWAY_INFO
    if (eventHeader.SubType == DKJOBTYPE_GATEWAY_INFO) {
        if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {
            GatewayInfo_t* gatewayInfo = (GatewayInfo_t*) DKServerManager_GetEventObjPayload (data);
            uint64_t updatetime = ObjectDictionary_GetPeripheralUpdatetime ();

            if ((gatewayInfo->updatetime != 0) && (gatewayInfo->updatetime == updatetime)) {

                peripheralUpdateTime = 0;

            } else {

                // Delete gateway at first time
                if (peripheralExpectedTotal == 0) {
                    // Copy local variable to Global
                    ObjectDictionary_Lock ();
                    ObjectDictionary_DeleteGateway ();
                    ObjectDictionary_AddGateway (
                        gatewayInfo->objectId,
                        gatewayInfo->macAddress,
                        sizeof (gatewayInfo->macAddress),
                        gatewayInfo->wifiVersion,
                        gatewayInfo->bleVersion,
                        (uint8_t*) gatewayInfo->name
                    );
                    ObjectDictionary_Unlock ();
                }

                // If first time we update
                if (peripheralExpectedTotal == 0) {
                    peripheralExpectedTotal = gatewayInfo->totalPeripherals;
                }

                peripheralUpdateTime = gatewayInfo->updatetime;
            }

            peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_PERIPHERAL);
            goto exit;
        }
    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetPeripheral_CheckPeripheral (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetPeripheral_CheckPeripheral, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_ITERATION);
        return peripheralStateMachine->process (peripheralStateMachine, data);
    }

    // Bypass DKJOBTYPE_CONTAINER_INFO
    if (eventHeader.SubType == DKJOBTYPE_CONTAINER_INFO) {
        return peripheralStateMachine->processOnce (peripheralStateMachine, GET_PERIPHERAL_CHECK_CONTAINER, data);
    }

    // Check DKJOBTYPE_PERIPHERAL_INFO
    if (eventHeader.SubType == DKJOBTYPE_PERIPHERAL_INFO) {

        if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {
            PeripheralInfo_t* peripheralInfo = (PeripheralInfo_t*) DKServerManager_GetEventObjPayload (data);

            if (peripheralInfo) {

                if (peripheralUpdateTime) {
                    // Add/Replace to list as advance condition
                    ObjectDictionary_Lock ();
                    ObjectDictionary_AddPeripheral (
                        peripheralInfo->peripheral.ObjectID,
                        peripheralInfo->peripheral.MACAddress,
                        sizeof (peripheralInfo->peripheral.MACAddress),
                        peripheralInfo->peripheral.GatewayID,
                        peripheralInfo->peripheral.DeviceType,
                        peripheralInfo->peripheral.FWVersion,
                        peripheralInfo->peripheral.szSecurityKey,
                        peripheralInfo->peripheral.nSecurityKeyLen,
                        (uint8_t*) peripheralInfo->name
                    );
                    ObjectDictionary_AddPeripheralStatus (
                        peripheralInfo->peripheralStatus.ObjectID,
                        peripheralInfo->peripheralStatus.BLEObjectID
                    );
                    ObjectDictionary_Unlock ();
                }

                // Update all index/variable
                peripheralExpectedItemCountForCurrentRequest--;
                peripheralExpectedTotal--;
                peripheralNextItemIndex++;

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "peripheralExpectedItemCountForCurrentRequest=%d, peripheralExpectedTotal=%d, peripheralNextItemIndex=%d\r\n",
                    peripheralExpectedItemCountForCurrentRequest,
                    peripheralExpectedTotal,
                    peripheralNextItemIndex
                );

                // Check if this iteration is end
                if ((peripheralExpectedTotal == 0) || (peripheralExpectedItemCountForCurrentRequest == 0)) {
                    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_ITERATION);
                }
            }
        }
        goto exit;

    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetPeripheral_CheckContainer (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetPeripheral_CheckContainer, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Check DKJOBTYPE_CONTAINER_INFO
    if (eventHeader.SubType == DKJOBTYPE_CONTAINER_INFO) {
        if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {
            dxContainer_t* pCont = NULL;

            //Copy the address
            memcpy (&pCont, (dxContainer_t*) DKServerManager_GetEventObjPayload (data), sizeof (dxContainer_t*));
            if (peripheralUpdateTime) {
                //PRINT_Container(pCont);
                cUtlParseAndDispatchContainerDataReport (pCont, eventHeader.SourceOfOrigin, eventHeader.TaskIdentificationNumber, dxFALSE);
            }
            dxContainer_Free (pCont);
        }
        goto exit;
    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetPeripheral_CheckIteration (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetPeripheral_CheckIteration, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_CONTAINER_INFO
    if (eventHeader.SubType == DKJOBTYPE_CONTAINER_INFO) {
        return peripheralStateMachine->processOnce (peripheralStateMachine, GET_PERIPHERAL_CHECK_CONTAINER, data);
    }

    // Check DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {

        //There is more
        if (peripheralUpdateTime && (peripheralExpectedTotal != 0)) {

            //We got all item from previous request? if so then we will need to get remaining
            if (peripheralExpectedItemCountForCurrentRequest == 0) {

                peripheralExpectedItemCountForCurrentRequest = (peripheralExpectedTotal > GET_PERIPHERAL_MAX_PER_REQUEST) ? GET_PERIPHERAL_MAX_PER_REQUEST : peripheralExpectedTotal;

                DKServer_StatusCode_t serverResult = DKServerManager_GetPeripheralByMAC_Asynch (
                    SOURCE_OF_ORIGIN_DEFAULT,
                    0,
                    peripheralNextItemIndex,
                    peripheralExpectedItemCountForCurrentRequest
                );
                if (serverResult != DKSERVER_STATUS_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "WARNING:(Line=%d) Failed to Call DKServerManager_GetAdvanceJob_Asynch\r\n",
                        __LINE__
                    );
                } else {
                    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_GATEWAY);
                    goto exit;
                }
            }
        } else {

            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "GetPeripheral Finished\r\n"
            );
            peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);
            if (peripheralUpdateTime) {
                ObjectDictionary_SetPeripheralUpdatetime (peripheralUpdateTime);
            }

            //Just make sure we are still in the same state and its not about to change
            if (SimpleStateMachine_StateNotChangingWhileEqualTo (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_PERIPHERAL)) {
                if (eventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_SUCCESS) {
                    // Refresh ble device list if any peripherals has been changed.
                    if (peripheralUpdateTime) {
                        if (isCentralOfflineSupported ()) {
                            //Make sure we clear all the device and jobs Asynchronously (direct access to device keeper is prohibited!)..
                            //this prevent in case there is device in the backup and no device from server
                            //it will always get the device because it was restored previously before login to server.
                            if (BleManagerCleanAllDeviceFromListTask_Asynch (1000, SOURCE_OF_ORIGIN_DEFAULT, 0) != DEV_MANAGER_SUCCESS) {
                                G_SYS_DBG_LOG_IV (
                                    G_SYS_DBG_CATEGORY_ONLINE,
                                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "WARNING: FAILED to send BleManagerCleanAllDeviceFromListTask_Asynch\r\n"
                                );
                            }
                        }

                        int nDeviceCount = 0;
                        DKServer_StatusCode_t ServerManagerRet = DKServerManager_GetTotalPeripheralNumbers (&nDeviceCount);

                        if (ServerManagerRet == DKSERVER_STATUS_SUCCESS) {
                            dxBOOL  bHybridDeviceDetected = dxFALSE;
                            int j = 0;
                            DeviceAddInfo_t DeviceAddInfo;
                            DeviceAddInfo.SecurityKey.Size = 0;     //First make sure the key is initialize to 0
                            DeviceAddInfo.SingleHostControl = 0;    //By default we disable the singlehost control.. in the future we may need to update/get this value from server

                            if (nDeviceCount > 0) {
                                // Dump peripheral MAC address
                                 G_SYS_DBG_LOG_IV (
                                    G_SYS_DBG_CATEGORY_ONLINE,
                                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "Total Peripherals: %d\r\n",
                                    nDeviceCount
                                );

                                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac ();

                                for (j = 0; j < nDeviceCount; j++) {
                                    bHybridDeviceDetected = dxFALSE;
                                    ServerManagerRet = DKServerManager_GetPeripheralByIndex (j, DeviceAddInfo.DevAddress.Addr, sizeof(DeviceAddInfo.DevAddress.Addr), &DeviceAddInfo.DeviceType);
                                    if (ServerManagerRet == DKSERVER_STATUS_SUCCESS) {
                                        uint8_t k = 0;
                                        for (k = 0; k < sizeof(DeviceAddInfo.DevAddress.Addr); k++) {
                                            if (DeviceAddInfo.DevAddress.Addr[k] == pMainDeviceMacAddr->MacAddr[k]) {
                                                if ((k + 1) == sizeof (DeviceAddInfo.DevAddress.Addr)) {
                                                    bHybridDeviceDetected = dxTRUE;
                                                }
                                                continue;
                                            } else {
                                                break;
                                            }
                                        }

                                        if (bHybridDeviceDetected == dxFALSE) {
                                            uint8_t SecretKeyLen = sizeof (DeviceAddInfo.SecurityKey.value);
                                            ServerManagerRet = DKServerManager_GetPeripheralSecurityKeyByIndex (j, DeviceAddInfo.SecurityKey.value, &SecretKeyLen);

                                            if (ServerManagerRet == DKSERVER_STATUS_SUCCESS) {
                                                DeviceAddInfo.SecurityKey.Size = SecretKeyLen;
                                            } else {
                                                G_SYS_DBG_LOG_IV (
                                                    G_SYS_DBG_CATEGORY_ONLINE,
                                                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                    "WARNING:(Line=%d) Get DkServerSecurityKey failed (%d)\r\n",
                                                    __LINE__,
                                                    ServerManagerRet
                                                );
                                            }

                                            G_SYS_DBG_LOG_IV (
                                                G_SYS_DBG_CATEGORY_ONLINE,
                                                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                                "DeviceAddInfo.SecurityKey.Size = %d\r\n",
                                                DeviceAddInfo.SecurityKey.Size
                                            );

                                            //NOTE: We add the Peripheral to BLE Manager (DIRECTLY to Keeper list)
                                            //      There is no need to go over the pairing, simply add it to the keeper list, BLEManager will
                                            //      automatically report back if the device is not paired and pairing will be done automatically from application handling
                                            if (BleManagerAddDeviceTask_Asynch (&DeviceAddInfo, 1000, SOURCE_OF_ORIGIN_DEFAULT, 0) != DEV_MANAGER_SUCCESS) {
                                                G_SYS_DBG_LOG_IV (
                                                    G_SYS_DBG_CATEGORY_ONLINE,
                                                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                                    "FAILED to send BleManagerAddDeviceTask_Asynch\r\n"
                                                );
                                            }
                                        } else {
#if DEVICE_IS_HYBRID
                                            cUtlUpdateHyBridPeripheralStatus ();
#else
                                            G_SYS_DBG_LOG_IV (
                                                G_SYS_DBG_CATEGORY_ONLINE,
                                                G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                "WARNING! - Hybrid device detected BUT framework is NOT configured for Hybrid!\r\n"
                                            );
#endif
                                        }
                                    }
                                }
                            } else {
                                G_SYS_DBG_LOG_IV (
                                    G_SYS_DBG_CATEGORY_ONLINE,
                                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "Dk Server No Peripheral paired with this Gateway\r\n"
                                );
                            }

#if DEVICE_IS_HYBRID
                            //Since this is a Hybrid Device we expect to find it after retrieving the peripheral list if not it could due to following reason
                            //1) This device/gateway is newly added and for the first time there should be no peripheral.
                            //2) For some reason the hybrid peripheral is being removed
                            if (bHybridDeviceDetected == dxFALSE) {
                                //Alway add the device back
                                cUtlAddHybridPeripheral ();
                            }
#endif
                        }

                        CentralOfflineSupportFlagSet (OFFLINE_FLAG_PERIPHERAL_INFO_DOWNLOADED);
                    }

                    LedManager_result_t LedManagerRetCode = SetLedModeCondition (LED_OPERATION_MODE_NORMAL, LED_CONDITION_NONE);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS) {
                        G_SYS_DBG_LOG_IV (
                            G_SYS_DBG_CATEGORY_ONLINE,
                            G_SYS_DBG_LEVEL_FATAL_ERROR,
                            "WARNING:(Line=%d) LedManager Failed SetLedModeCondition (%d)\r\n",
                            __LINE__,
                            LedManagerRetCode
                        );
                    }

#if MQTT_IS_ENABLED
                    // Connect to MQTT
                    MqttResult mqttResult = MqttManager_Connect_Async ();
                    if (mqttResult != MqttResult_SUCCESS) {
                        G_SYS_DBG_LOG_IV (
                            G_SYS_DBG_CATEGORY_ONLINE,
                            G_SYS_DBG_LEVEL_FATAL_ERROR,
                            "WARNING: Fail to call MqttManager_Connect_Async (%d)\r\n",
                            mqttResult
                        );
                    }
#endif

                    //Always tell server that Device ready
                    DKServerManager_ChangeGatewayStatus_Asynch (SOURCE_OF_ORIGIN_DEFAULT, 0, GATEWAY_STATE_READY);

                    //Now we proceed to the next sub state
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_SCHEDULE_JOB);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                        G_SYS_DBG_LOG_IV (
                            G_SYS_DBG_CATEGORY_ONLINE,
                            G_SYS_DBG_LEVEL_FATAL_ERROR,
                            "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_SCHEDULE_JOB (%d)\r\n",
                            __LINE__,
                            SmpResult
                        );
                    }

                    DKServerManager_ConfigGetJobInterval (GET_NEW_JOB_INTERVAL_WHEN_NO_ACTIVE_USER);

                } else {

                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
                    if (DevInfoRet != DXDEV_INFO_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                         __LINE__, DevInfoRet);
                    }

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DKSERVER_REPORT_STATUS_DEVICE_NOT_FOUND\r\n",
                                     __LINE__ );

                     LedManager_result_t LedManagerRetCode = SetLedCondition (LED_CONDITION_DK_SERVER_OPERATION_ERROR);
                     if (LedManagerRetCode != LED_MANAGER_SUCCESS) {
                         G_SYS_DBG_LOG_IV (
                             G_SYS_DBG_CATEGORY_ONLINE,
                             G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                             __LINE__,
                             LedManagerRetCode
                         );
                     }

                    // TODO: What's the next step while AddGateway fail?
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RESET);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING: SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n", SmpResult);
                    }

                }

                BleManagerScanReportResume ();
            }
        }
        goto exit;

    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetScheduleJob_CheckStatus (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetScheduleJob_CheckStatus, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_ITERATION);
        return scheduleJobStateMachine->process (scheduleJobStateMachine, data);
    }

    if (eventHeader.SubType == DKJOBTYPE_STANDARD) {

        ScheduleJobStatus_t* scheduleStatus = (ScheduleJobStatus_t*) DKServerManager_GetEventObjPayload (data);
        uint64_t updatetime = ObjectDictionary_GetScheduleJobUpdatetime ();

        if (!scheduleStatus) {
            goto exit;
        }

        G_SYS_DBG_LOG_V ("scheduleStatus->updatetime=%x, updatetime=%x\r\n", scheduleStatus->updatetime, updatetime);
        if (scheduleStatus->updatetime == updatetime) {
            scheduleJobUpdateTime = 0;
        } else {
            // If first time we update
            if (scheduleJobExpectedTotal == 0) {
                scheduleJobExpectedTotal = scheduleStatus->totalSchedules;
                if (isCentralOfflineSupported ()) {
                    JobManagerRemoveAllScheduleJobExceptReserved ();
                }
            }
            scheduleJobUpdateTime = scheduleStatus->updatetime;
        }

        scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_JOB);
        goto exit;

    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetScheduleJob_CheckJob (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetScheduleJob_CheckJob, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_ITERATION);
        return scheduleJobStateMachine->process (scheduleJobStateMachine, data);
    }

    if (eventHeader.SubType == DKJOBTYPE_SCHEDULE) {
        if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {

            ScheduleJobInfo_t* scheduleJob = (ScheduleJobInfo_t*) DKServerManager_GetEventObjPayload (data);

            if (!scheduleJob) {
                goto exit;
            }

            if (scheduleJobUpdateTime) {

                struct ScheduleSequenceNumber_s ScheduleSequenceNumber = _UINT64ScheduleSeqNoToStruct (scheduleJob->nScheduleSEQNO);
                //G_SYS_DBG_LOG_V1 ("ScheduleSEQNO-PeripheralUniqueID: %ld\n", ScheduleSequenceNumber.PeripheralUniqueID);
                //G_SYS_DBG_LOG_V1 ("ScheduleSEQNO-UniqueSequenceID: %d\n", ScheduleSequenceNumber.UniqueSequenceID);

                if ((ScheduleSequenceNumber.PeripheralUniqueID != 0xFFFFFFFF) && (DKServerManager_IsValidPeripheralObjectID (ScheduleSequenceNumber.PeripheralUniqueID))) {

                    //Update to JobManager
                    if (
                        JobManagerNewJob_Task (
                            scheduleJob->pPayload,
                            eventHeader.DataLen - sizeof(ScheduleJobInfo_t),
                            SOURCE_OF_ORIGIN_DEFAULT,
                            0
                        ) != DXJOBMANAGER_SUCCESS
                    ) {
                        G_SYS_DBG_LOG_IV (
                            G_SYS_DBG_CATEGORY_ONLINE,
                            G_SYS_DBG_LEVEL_FATAL_ERROR,
                            "WARNING:(Line=%d) Failed to Call JobManagerNewJob_Task\r\n",
                            __LINE__
                        );
                    }

                } else {

                    //We are going to SAVE the list first and only then execute the required deletion after we have downloaded all the schedules.
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                        "NOTE: ScheduleJob SeqNo invalid will be removed automatically\r\n"
                    );

                    if (scheduleJobDeleteList.pJobInfo == NULL) {
                        //Alloc more than what we required since we don't really know how many schedule job is invalid
                        scheduleJobDeleteList.TotalJobSlot = 0;
                        scheduleJobDeleteList.pJobInfo = (DeleteScheduleJobInfo_t*) dxMemAlloc ("DeleteScheduleJob", scheduleJobExpectedTotal, sizeof (DeleteScheduleJobInfo_t));
                        if (scheduleJobDeleteList.pJobInfo) {
                            scheduleJobDeleteList.TotalJobSlot = scheduleJobExpectedTotal;
                        }
                    }

                    uint16_t i = 0;
                    for (i = 0; i < scheduleJobDeleteList.TotalJobSlot; i++) {
                        //Is the slot taken?
                        if (scheduleJobDeleteList.pJobInfo[i].Taken == 0) {
                            scheduleJobDeleteList.pJobInfo[i].ScheduleSEQNO = scheduleJob->nScheduleSEQNO;
                            scheduleJobDeleteList.pJobInfo[i].Taken = 1; //mark as taken
                            break;
                        }
                    }

                }

                //Update all index/variable
                scheduleJobExpectedItemCountForCurrentRequest--;
                scheduleJobExpectedTotal--;
                scheduleJobNextItemIndex++;

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "scheduleJobExpectedItemCountForCurrentRequest=%d, scheduleJobExpectedTotal=%d, scheduleJobNextItemIndex=%d\r\n",
                    scheduleJobExpectedItemCountForCurrentRequest,
                    scheduleJobExpectedTotal,
                    scheduleJobNextItemIndex
                );

                // Check if this iteration is end
                if ((scheduleJobExpectedTotal == 0) || (scheduleJobExpectedItemCountForCurrentRequest == 0)) {
                    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_ITERATION);
                }
            }

        } else {

            //Let the sub state handles it
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "WARNING:(Line=%d) GetScheduleJob_CheckJob failed\r\n",
                __LINE__
            );

        }
        goto exit;
    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetScheduleJob_CheckIteration (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetScheduleJob_CheckIteration, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Check DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {

        //There is more
        if (scheduleJobUpdateTime && scheduleJobExpectedTotal != 0) {

            //We got all item from previous request? if so then we will need to get remaining
            if (scheduleJobExpectedItemCountForCurrentRequest == 0) {

                scheduleJobExpectedItemCountForCurrentRequest = (scheduleJobExpectedTotal > GETSCHEDULE_JOB_MAX_PER_REQUEST) ? GETSCHEDULE_JOB_MAX_PER_REQUEST : scheduleJobExpectedTotal;

                DKServer_StatusCode_t serverResult = DKServerManager_GetScheduleJob_Asynch (
                    SOURCE_OF_ORIGIN_DEFAULT,
                    0,
                    scheduleJobNextItemIndex,
                    scheduleJobExpectedItemCountForCurrentRequest
                );
                if (serverResult != DKSERVER_STATUS_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "WARNING:(Line=%d) Failed to Call DKServerManager_GetScheduleJob_Asynch\r\n",
                        __LINE__
                    );

                    //Related CleanUp
                    if (scheduleJobDeleteList.pJobInfo) {
                        dxMemFree (scheduleJobDeleteList.pJobInfo);
                        scheduleJobDeleteList.pJobInfo = NULL;
                    }
                    scheduleJobDeleteList.TotalJobSlot = 0;
                } else {
                    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_STATUS);
                    goto exit;
                }
            }

        } else { //We done downloading all the list

            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "GetScheduleJob Finished\r\n"
            );

            if (scheduleJobUpdateTime) {
                ObjectDictionary_SetScheduleJobUpdatetime (scheduleJobUpdateTime);
            }

            IntIterateDeleteScheduleJobListExecution ();
            CentralOfflineSupportFlagSet (OFFLINE_FLAG_SCH_JOB_INFO_DOWNLOADED);

            if (SimpleStateMachine_StateNotChangingWhileEqualTo (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_SCHEDULE_JOB)) {
                //Switch to next sub state get adv job
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_ADVANCE_JOB);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_ADVANCE_JOB (%d)\r\n",
                        __LINE__,
                        SmpResult
                    );
                }
            }

        }

    }

    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetAdvanceJob_CheckStatus (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetAdvanceJob_CheckStatus, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_ITERATION);
        return advanceJobStateMachine->process (advanceJobStateMachine, data);
    }

    if (eventHeader.SubType == DKJOBTYPE_STANDARD) {

        AdvanceJobStatus_t* advanceStatus = (AdvanceJobStatus_t*) DKServerManager_GetEventObjPayload (data);
        uint64_t updatetime = ObjectDictionary_GetAdvanceJobUpdatetime ();

        if (!advanceStatus) {
            goto exit;
        }

        G_SYS_DBG_LOG_V ("advanceStatus->updatetime=%x, updatetime=%x\r\n", advanceStatus->updatetime, updatetime);
        if (advanceStatus->updatetime == updatetime) {
            advanceJobUpdateTime = 0;
        } else {
            // If first time we update
            if (advanceJobExpectedTotal == 0) {
                advanceJobExpectedTotal = advanceStatus->totalAdvances;
                if (isCentralOfflineSupported ()) {
                    JobManagerRemoveAllAdvanceJobExceptReserved ();
                }
            }
            advanceJobUpdateTime = advanceStatus->updatetime;
        }

        advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_JOB);
        goto exit;

    }

    G_SYS_DBG_LOG_IV (
        G_SYS_DBG_CATEGORY_ONLINE,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "WARNNIG: wrong data provided"
    );
    advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetAdvanceJob_CheckJob (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetAdvanceJob_CheckJob, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Bypass DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {
        advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_ITERATION);
        return advanceJobStateMachine->process (advanceJobStateMachine, data);
    }

    AdvJobInfo_t* advanceJob = (AdvJobInfo_t*) DKServerManager_GetEventObjPayload (data);

    if (!advanceJob) {
        goto exit;
    }

    switch (eventHeader.SubType) {

        case DKJOBTYPE_ADVANCED_CONDITION : {
            if (advanceJobIncomplete) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) FATAL: Adv Job single pack incomplete detected!!\n",
                    __LINE__
                );
            }

            //Update all index/variable
            advanceJobExpectedItemCountForCurrentRequest--;
            advanceJobExpectedTotal--;
            advanceJobNextItemIndex++;

            //Add/Replace to list as advance condition
            JobManagerNewAdvanceJob_Task (
                advanceJob->pPayload,
                0,
                eventHeader.DataLen - sizeof(AdvJobInfo_t),
                advanceJob->nAdvJobID,
                SOURCE_OF_ORIGIN_DEFAULT,
                0
            );
        }
        break;

        case DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY : {
            if (advanceJobIncomplete) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) FATAL: Adv Job single pack incomplete detected!!\n",
                    __LINE__
                );
            }

            //Add/Replace to list as advance condition
            JobManagerNewAdvanceJob_Task (
                advanceJob->pPayload,
                0,
                eventHeader.DataLen - sizeof(AdvJobInfo_t),
                advanceJob->nAdvJobID,
                SOURCE_OF_ORIGIN_DEFAULT,
                0
            );

            advanceJobIncomplete = dxTRUE; //Set flag as we expect adv execution job in the next event
        }
        break;

        case DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY : {
            //Update all index/variable
            advanceJobExpectedItemCountForCurrentRequest--;
            advanceJobExpectedTotal--;
            advanceJobNextItemIndex++;

            //Add/Replace to list as single pack execution
            JobManagerNewAdvanceJob_Task (
                advanceJob->pPayload,
                1,
                eventHeader.DataLen - sizeof(AdvJobInfo_t),
                advanceJob->nAdvJobID,
                SOURCE_OF_ORIGIN_DEFAULT,
                0
            );

            advanceJobIncomplete = dxFALSE; //Reset the flag
        }
        break;

        default : {
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_FATAL_ERROR,
                "WARNNIG: wrong data provided"
            );
            advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_IDLE);
        }
        break;

    }

    // Check if this iteration is end
    if ((advanceJobExpectedTotal == 0) || (advanceJobExpectedItemCountForCurrentRequest == 0)) {
        advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_ITERATION);
    }

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


DataDrivenStateMachineResult GetAdvanceJob_CheckIteration (void* data, dxBOOL isFirstCall) {
    DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (data);

    G_SYS_DBG_LOG_V ("GetAdvanceJob_CheckIteration, isFirstCall=%d, type=%d, status=%d\r\n", isFirstCall, eventHeader.SubType, eventHeader.s.ReportStatusCode);

    // Check DKJOBTYPE_NONE
    if (eventHeader.SubType == DKJOBTYPE_NONE) {

        //There is more
        if (advanceJobUpdateTime && advanceJobExpectedTotal != 0) {

            //We got all item from previous request? if so then we will need to get remaining
            if (advanceJobExpectedItemCountForCurrentRequest == 0) {

                advanceJobExpectedItemCountForCurrentRequest = (advanceJobExpectedTotal > GETADVANCE_JOB_MAX_PER_REQUEST) ? GETADVANCE_JOB_MAX_PER_REQUEST : advanceJobExpectedTotal;

                DKServer_StatusCode_t serverResult = DKServerManager_GetAdvJobCondition_Asynch (
                    SOURCE_OF_ORIGIN_DEFAULT,
                    0,
                    advanceJobNextItemIndex,
                    advanceJobExpectedItemCountForCurrentRequest
                );
                if (serverResult != DKSERVER_STATUS_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "WARNING:(Line=%d) Failed to Call DKServerManager_GetAdvJobCondition_Asynch\r\n",
                        __LINE__
                    );
                } else {
                    advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_STATUS);
                    goto exit;
                }
            }

        } else { //We done downloading all the list

            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_ONLINE,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "GetAdvanceJob Finished\r\n"
            );

            if (advanceJobUpdateTime) {
                ObjectDictionary_SetAdvanceJobUpdatetime (advanceJobUpdateTime);
            }

            CentralOfflineSupportFlagSet (OFFLINE_FLAG_ADV_JOB_INFO_DOWNLOADED);

            if (SimpleStateMachine_StateNotChangingWhileEqualTo (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_ADVANCE_JOB)) {
                //Switch to next sub state get adv job
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState (OnlineSubStateHandler, ONLINE_SUBSTATE_GET_FW_VERSION);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_ONLINE,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_FW_VERSION (%d)\r\n",
                        __LINE__,
                        SmpResult
                    );
                }
            }

        }

    }

    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_ADVANCEJOB_IDLE);

exit :

    return DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS;
}


SimpleStateMachine_result_t SubState_OnlineReLogIn(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            cUtlLogInToServer();
        }
        else
        {
            //Change to Offline
            CentralStateMachine_ChangeState(STATE_OFFLINE, NULL, 0);
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineHostRedirect(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {

            //Host Redirecting will automatically switch to suspended and back to ready state... our state process will automatically handle the login process and
            //other related task as usual.
            DKServerManager_HostRedirect_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, (uint8_t *)DK_QA_SERVER_DNS_NAME, (uint8_t *)DK_QA_SERVER_API_VERSION);
        }
        else
        {
            //Change to Offline
            CentralStateMachine_ChangeState(STATE_OFFLINE, NULL, 0);
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineGetPeripheral(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Synch time with server when needed
            DKTime_t TimeRTC;

            if ( dxSwRtcGet( &TimeRTC ) == DXOS_ERROR ) {
                DKServer_StatusCode_t DkServerResult = DKServerManager_GetServerTime_Asynch( SOURCE_OF_ORIGIN_DEFAULT, 0 );
                if ( DkServerResult != DKSERVER_STATUS_SUCCESS ) {
                    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                        "WARNNIG: Failed to send DKServerManager_GetServerTime_Asynch (Error = %d):", DkServerResult );
                }
            }

            peripheralExpectedItemCountForCurrentRequest = GET_PERIPHERAL_MAX_PER_REQUEST;
            peripheralExpectedTotal = 0;
            peripheralNextItemIndex = GET_PERIPHERAL_STARTING_INDEX;

            DKServer_StatusCode_t serverResult = DKServerManager_GetPeripheralByMAC_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                GET_PERIPHERAL_STARTING_INDEX,
                GET_PERIPHERAL_MAX_PER_REQUEST
            );
            if (serverResult != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) DKServerManager_GetPeripheralByMAC_Asynch failed (ret error %d)\r\n",
                    __LINE__,
                    serverResult
                );
            } else {
                peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_GATEWAY);
            }

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Re-Login to Server!\r\n");

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RELOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_RELOGIN (%d)\r\n", __LINE__, SmpResult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineGetScheduleJob(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get Scheduled Job (initial Kick-off)
            scheduleJobExpectedItemCountForCurrentRequest = GETSCHEDULE_JOB_MAX_PER_REQUEST;
            scheduleJobExpectedTotal = 0;
            scheduleJobNextItemIndex = GETSCHEDULE_JOB_STARTING_INDEX;

            DKServer_StatusCode_t serverResult = DKServerManager_GetScheduleJob_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                GETSCHEDULE_JOB_STARTING_INDEX,
                GETSCHEDULE_JOB_MAX_PER_REQUEST
            );
            if (serverResult != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) Failed to Call DKServerManager_GetScheduleJob_Asynch\r\n",
                    __LINE__
                );
            } else {
                scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_CHECK_STATUS);
            }

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RELOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_RELOGIN (%d)\r\n", __LINE__, SmpResult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineGetAdvJob(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
           //Get Advance Job (initial Kick-off)
            advanceJobExpectedItemCountForCurrentRequest = GETADVANCE_JOB_MAX_PER_REQUEST;
            advanceJobExpectedTotal = 0;
            advanceJobNextItemIndex = GETADVANCE_JOB_STARTING_INDEX;
            DKServer_StatusCode_t serverResult = DKServerManager_GetAdvJobCondition_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                advanceJobNextItemIndex,
                advanceJobExpectedItemCountForCurrentRequest
            );
            if (serverResult != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "WARNING:(Line=%d) Failed to Call DKServerManager_GetAdvJobCondition_Asynch\r\n",
                    __LINE__
                );
            } else {
                advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_CHECK_STATUS);
            }

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RELOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_RELOGIN (%d)\r\n", __LINE__, SmpResult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineAllInfoSynched(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    static dxTime_t LastSynchedTime = 0;
    if (isFirstCall)
    {
        LastSynchedTime = 0;

#if DEVICE_IS_HYBRID
        if (bHpClientFrwServerReadyReported == dxFALSE)
        {
            HPClientFrwStatusReportServerReady(0, 0);
            bHpClientFrwServerReadyReported = dxTRUE;

#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
            }
#endif
        }
#endif
        BLEManager_WhiteList_AddFromKeeperList();
        BleRepeater_UpdateWatchList ();
        if (OnlineSubStateRoutineCheckHandler == NULL) {
            OnlineSubStateRoutineCheckHandler = SimpleStateMachine_Init(OnlineSubStateRoutineCheckProcessFuncTable);
        }

        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateRoutineCheckHandler, ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL (%d)\r\n", __LINE__, SmpResult);
        }

        JobManagerNotifyServerReadyState(dxTRUE);

        cUtlServerConnectGating_AllInfoSynched();
    }

    //Regardless of if its first time call or timed call we are going to backup the data
    //We are safe in doing this because this sub state is only called after all
    //information is downloaded which mean we will be backing up entired downloaded info
    //After the first call we will just check once a while to see if there is any data update
    //that needs backup. Even though if its missed we will still be able to back it up on the next session time out login
    CentralOfflineSupportDeviceInfoBackUp();            //Backup if any changes
    CentralOfflineSupportFlagClear(OFFLINE_FLAG_ALL);   //Clear the flag after we done back up.

    if (mDeviceFwVersionUpdateCheck == dxTRUE)
    {
        dxBOOL      FwVersionUpdateToServer = dxFALSE;
        char        FwVersinData[16]; //16 byte should be more than enough for format "xx.xx.xx"
        uint16_t    FwVersinDataSize = sizeof(FwVersinData);

        //Check if BLE FW version exist or if its a dummy, if so that means we havent update it to server yet
        dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, FwVersinData, &FwVersinDataSize, dxFALSE);
        if (DevInfoRet == DXDEV_INFO_SUCCESS)
        {
            if (DKServerManager_FirmwareVersionCompare((uint8_t*)FW_VERSION_DUMMY, (uint8_t*)FwVersinData) == DKSERVER_STATUS_FIRMWARE_VERSION_EQUAL)
            {
                FwVersionUpdateToServer = dxTRUE;
            }
        }
        else
        {
            FwVersionUpdateToServer = dxTRUE;
        }

        //Check if Central FW version exist or if its a dummy, if so that means we havent update it to server yet
        DevInfoRet = dxDevInfo_GetData(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, FwVersinData, &FwVersinDataSize, dxFALSE);
        if (DevInfoRet == DXDEV_INFO_SUCCESS)
        {
            if (DKServerManager_FirmwareVersionCompare((uint8_t*)FW_VERSION_DUMMY, (uint8_t*)FwVersinData) == DKSERVER_STATUS_FIRMWARE_VERSION_EQUAL)
            {
                FwVersionUpdateToServer = dxTRUE;
            }
        }
        else
        {
            FwVersionUpdateToServer = dxTRUE;
        }

        if (FwVersionUpdateToServer)
        {
            SystemInfoReportObj_t SystemInfoContainer;
            BLEManager_result_t bleresult = BleManagerSystemInfoGet(&SystemInfoContainer);

            if ((bleresult == DEV_MANAGER_SUCCESS) && !BleManager_SBL_Enable())
            {
                if (DKServerManager_UpdateGatewayFirmwareVersion_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, RTK_WIFI_CURRENT_FW_VERSION, (char*)SystemInfoContainer.Version) == DKSERVER_STATUS_SUCCESS)
                {
                    mDeviceFwVersionUpdateCheck = dxFALSE;
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Unable to retrieve Ble SystemInfoContainer(%d)\r\n",
                                 __LINE__, bleresult);
            }
        }
        else
        {
           mDeviceFwVersionUpdateCheck = dxFALSE;
        }
    }

    //Synch time with server when needed
    DKTime_t TimeRTC;
    dxBOOL   bSynchTime = dxFALSE;
    if (dxSwRtcGet(&TimeRTC) == DXOS_ERROR)
    {
        bSynchTime = dxTRUE;
    }
    else if ((LastSynchedTime == 0) || ((dxTimeGetMS() - LastSynchedTime) > SERVER_TIME_SYNCH_INTERVALIN_MS))
    {
        LastSynchedTime = dxTimeGetMS();
        bSynchTime = dxTRUE;
    }

    if (bSynchTime)
    {
        DKServer_StatusCode_t DkServerResult = DKServerManager_GetServerTime_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);
        if (DkServerResult != DKSERVER_STATUS_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNNIG: Failed to send DKServerManager_GetServerTime_Asynch (Error = %d):", DkServerResult);
        }
    }

    return result;
}


SimpleStateMachine_result_t SubState_OnlineGetFwVersion(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get Fw version from server's copy
            DKServerManager_GetLastFirmareInfo_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);
            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RELOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_RELOGIN (%d)\r\n", __LINE__, SmpResult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OnlineGetUserInfo(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get User Info from server's copy
            DKServerManager_GetCurrentUser_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);
            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RELOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_RELOGIN (%d)\r\n", __LINE__, SmpResult);
            }
        }
    }

    return result;
}

//============================================================================
// SubState Routine Check Function
//============================================================================

SimpleStateMachine_result_t SubState_RoutineCheck_UnpairPeripheral(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SubState_RoutineCheck_UnpairPeripheral\r\n");

    if (UnpairScanStartedTime)
    {
        dxTime_t CurrentTime = dxTimeGetMS();

        if ((CurrentTime - UnpairScanStartedTime) > (UnpairScanDiscoveryTimeMaxInSecond*SECONDS) + 10*SECONDS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Unpair Periheral Report timeout, will now stop reporting\r\n");

            BleManagerStopReportingUnPairPeripheral();

            UnpairScanStartedTime = 0;

            //NOTE: We are going to set condition to none ONLY if current condition is still in scanning unpair peripheral, otherwise we will leave it
            //		up to whoever changed the condition
            if (GetLedCondition() == LED_CONDITION_SCANNING_UNPAIR_PERIPHERALS)
            {
                LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode);
                }
            }
        }
        else
        {
            // NOTE: Extend the time to check missing device in each keeper list record.
            // TODO: We should remove this function and do the same job automatically in BLEManager.
            BleManagerForceUpdateAdvRxTimeInKeeperList();
        }
    }

    if ( FW_AUTO_UPDATE_UPON_AVAILABLE ) {
        //NOTE: At the moment we only have one routine check so we go to cycle completed, if we add more routine check state then we should execute the next state instead
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState( OnlineSubStateRoutineCheckHandler, ONLINE_SUBSTATE_ROUTINE_CHECK_UPGRADEFIRMWARE );
        if ( SmpResult != SMP_STATE_MACHINE_RET_SUCCESS ) {
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ROUTINE_CHECK_UPGRADEFIRMWARE (%d)\r\n", __LINE__, SmpResult);
        }
    } else {
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState( OnlineSubStateRoutineCheckHandler, ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED );
        if ( SmpResult != SMP_STATE_MACHINE_RET_SUCCESS ) {
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED (%d)\r\n", __LINE__, SmpResult );
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RoutineCheck_UpgradeFirmware(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    if ( FwNewUpdateAvailabilityStatus == FR_FW_UPDATE_AVAILABLE_AUTO_UPDATE ) {
        DKTime_t CurrentDkTime;

        if ( dxSwRtcGet( &CurrentDkTime ) == DXOS_SUCCESS ) {
            uint32_t CurrentUTCTime = ConvertDKTimeToUTC( CurrentDkTime );
            uint32_t RegisterUTCTime;
            uint16_t RegisterUTCTimeSize = sizeof( uint32_t );

            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_GetData( DX_DEVICE_INFO_TYPE_DEV_REGISTER_TIME, &RegisterUTCTime, &RegisterUTCTimeSize, dxFALSE );

            if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
                G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                    "WARNING:(Line=%d) Unable to retrieve DX_DEVICE_INFO_TYPE_DEV_REGISTER_TIME\r\n",
                                    __LINE__ );
                CentralStateMachine_ChangeState( STATE_FW_UPDATE, NULL, 0 );
            } else if ( CurrentUTCTime >= RegisterUTCTime ) {
                CentralStateMachine_ChangeState( STATE_FW_UPDATE, NULL, 0 );
            }
        }
    }

    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState( OnlineSubStateRoutineCheckHandler, ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED );
    if ( SmpResult != SMP_STATE_MACHINE_RET_SUCCESS ) {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                            "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ROUTINE_CHECK_CYCLE_COMPLETED (%d)\r\n", __LINE__, SmpResult );
    }

    return result;
}

SimpleStateMachine_result_t SubState_RoutineCheck_CycleCompleted(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;


    //After the seconds call (a delay) we cycle back to the beginning for routine check
    if (isFirstCall == dxFALSE)
    {
        //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SubState_RoutineCheck_CycleCompleted\r\n");

        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateRoutineCheckHandler, ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ROUTINE_CHECK_UNPAIRPERIPHERAL (%d)\r\n", __LINE__, SmpResult);
        }
    }

    return result;
}


//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t SrvOnline_EventBLEManager(void* arg)
{

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s Event=%d\r\n", __FUNCTION__, EventHeader.ReportEvent);

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_UNPAIR_DEVICE_REPORT:
        {
            static dxTime_t LastServerReport = 0;

            DeviceInfoReport_t* UnpairDevInfo = NULL;
            UnpairDevInfo = BleManagerParseUnPairPeripheralReportEventObj(arg);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "UnPair Device Address Found : " );
            uint16_t i = 0;
            for (i = 0; i < B_ADDR_LEN_8; i++)
            {
                G_SYS_DBG_LOG_NV(  "%x:", UnpairDevInfo->DevAddress.Addr[i] );
            }

            G_SYS_DBG_LOG_NV(  "   ProductID = %d, BattLevel = %d, AdvLen = %d, RSSI = %d\r\n",UnpairDevInfo->DkAdvertisementData.Header.ProductID,
                                                                                               UnpairDevInfo->DkAdvertisementData.BatteryInfo,
                                                                                               UnpairDevInfo->DkAdvertisementData.AdDataLen,
                                                                                               UnpairDevInfo->rssi );

            //Send only after N second since previous send to avoid excess traffic.
            dxTime_t CurrentTime = dxTimeGetMS();
            if ((CurrentTime - LastServerReport) > (4*SECONDS))
            {
                LastServerReport = CurrentTime;

                PeripheralQuery_t UnpairPeripheral;
                UnpairPeripheral.DeviceType = UnpairDevInfo->DkAdvertisementData.Header.ProductID;
                CONVERT_SIGNAL_STREGTH(UnpairDevInfo->rssi,UnpairPeripheral.PSignalStr);
                memcpy(UnpairPeripheral.PMAC, UnpairDevInfo->DevAddress.Addr, sizeof(UnpairDevInfo->DevAddress.Addr));

                //TODO: Since unpair reporting is on a different ble manager thread we won't know who (Udp or Server) is requesting,
                //      So we are sending both server and Udp at the same time, we may need to have global flag to resolve this issue if we want to be smart about this.
                DKServer_StatusCode_t DkServerResult = DKServerManager_SetPeripheralQueryCollection_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, &UnpairPeripheral);
                if (DkServerResult != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNNIG: Failed to send DKServerManager_SetPeripheralQueryCollection_Asynch (Error = %d):", DkServerResult);
                }

                UdpCommManager_result_t UdpManagerResult = UdpCommManagerSendUnpairDeviceInfo((uint8_t*)UnpairDevInfo, sizeof(DeviceInfoReport_t));
                if (UdpManagerResult != UDP_COMM_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "Failed to send unpair device info Code = %d\r\n", UdpManagerResult);
                }
            }
        }
        break;

        case DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);
            JobManagerPeripheralDataUpdate_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber);
        }
        break;

        case DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST:
        {
#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                DeviceKeeperListReport_t* pDeviceKeeperList = BleManagerParseDeviceKeeperListReportEventObj(arg);
                cUtlHybridPeripheralDeviceKeeperListUpdate(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, (uint8_t *) pDeviceKeeperList->GenericInfo, sizeof(DeviceGenericInfo_t) * pDeviceKeeperList->DeviceCount);
            }
            else
#endif
#endif // DEVICE_IS_HYBRID
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "INFORMATION:(Line=%d) Receive DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST with SourceOfOrigin(%d)\r\n",
                                 __LINE__,
                                 EventHeader.SourceOfOrigin);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);

            //FIRST: We TAG JobID if found.
            char MacAddrDecStr[21] = {0};
            if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), DeviceKeeperInfoReport->DevAddress.Addr, 8))
            {
                uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                if (MacAddrDec)
                {
                    if (JobManagerTagJobIdInfoByMacAddrDec(MacAddrDec) == dxTRUE)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "JobID Tagged\r\n");
                    }
                }
            }

            //SECOND: We report to Job Manager
            //We change this to FALSE (No force update) 2015/08/31. The reason is because force update will cause issue (eg Light)
            //with repeat historic event update. However, this will be under QA test to check if there is a side effect with this change.
            //NOTE: This access with data update force was develop mainly for "Group" access where we need to update
            //the payload as we change its state... But looks like even without force update we can still manage it
            JobManagerPeripheralDataUpdateExt_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, dxFALSE);

        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT:
        {
            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);

#if DEVICE_IS_HYBRID
            if(EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                HpJobBridgeExeRes_t JobBridgeExeRes;
                JobBridgeExeRes.StatusReport = StatusReport;
                JobBridgeExeRes.SourceOfOrigin = EventHeader.SourceOfOrigin;
                JobBridgeExeRes.IdentificationNumber = EventHeader.IdentificationNumber;
                HP_CMD_JobBridge_AdvJobBridgeExeRes(0, &JobBridgeExeRes, 0);
            }
#endif  //#if DEVICE_IS_HYBRID

            cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode );
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT:
        {
            AggregatedReportObj_t AggData = BleManagerParseAggregatedDataReportEventObj(arg);

            AggregateDataHeader_t AggHeader;
            memcpy(AggHeader.MAC, AggData.Header.DevAddress.Addr, sizeof(AggHeader.MAC));
            memcpy(&AggHeader.DataHeader , &AggData.Header.AggregatedItemHeader, sizeof(AggHeader.DataHeader));

            if (DKServerManager_UpdatePeripheralAggregationData_Asynch( SOURCE_OF_ORIGIN_DEFAULT,
                                                                        0,
                                                                        &AggHeader,
                                                                        (void*)AggData.pAggregatedItem,
                                                                        (int)AggData.Header.TotalAggregatedItem) != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Failed to send DKServerManager_UpdatePeripheralAggregationData_Asynch\r\n",
                                 __LINE__);
            }


        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT:
        {
            DataReadReportObj_t* pReadData = BleManagerParseDataReadReportEventObj(arg);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "TODO: NEED TO SEND DATA TO SERVER VIA JOB REPORTING\r\n"  );

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }


            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);
            cUtlJobDoneReportSendWithPayloadHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport, pReadData->PayloadSize, pReadData->Payload);


        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_SECURITY_DISABLED:
        {
            int16_t DevIndex = BleManagerParseDeviceIndexReportEventObj(arg);

            if (DevIndex >= 0)
            {
                const DeviceInfoKp_t* DevInfoKeeper = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

                if (DevInfoKeeper)
                {
                    ObjectDictionary_Lock ();
                    uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) DevInfoKeeper->DevAddress.pAddr, DevInfoKeeper->DevAddress.AddrLen);
                    ObjectDictionary_Unlock ();

                    DKServerManager_ClearPeripheralSecurityKey_Asynch (EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, &ObjectID);
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Dev man Event invalid DevIndex (%d)\r\n", DevIndex);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED:
        {
            if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
            {
                int16_t DevIndex = BleManagerParseDeviceIndexReportEventObj(arg);

                if (DevIndex >= 0)
                {
                    const DeviceInfoKp_t* DevInfoKeeper = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

                    if (DevInfoKeeper)
                    {

                        ObjectDictionary_Lock ();
                        uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) DevInfoKeeper->DevAddress.pAddr, DevInfoKeeper->DevAddress.AddrLen);
                        ObjectDictionary_Unlock ();

                        if (DKServerManager_SetPeripheralSecurityKey_Asynch (EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber,
                                                                             &ObjectID,
                                                                             (uint8_t*)DevInfoKeeper->SecurityKey.value,
                                                                             DevInfoKeeper->SecurityKey.Size) != DKSERVER_STATUS_SUCCESS)
                        {
                            int16_t StatusReport = JOB_FAILED_INTERNAL;
                            cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

                            DevAddress_t DevAddress;
                            memcpy(DevAddress.Addr, DevInfoKeeper->DevAddress.pAddr, DevInfoKeeper->DevAddress.AddrLen);
                            BleManagerClearSecurityTask_Asynch(DevAddress , 10000, SOURCE_OF_ORIGIN_DEFAULT, 0);
                        }
                        else
                        {
                            //Save the task info.
                            IntTaskInfoSave(DevIndex, EventHeader.IdentificationNumber);
                        }
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING: Dev man Event invalid DevIndex (%d)\r\n", DevIndex);
                }
            }
            else
            {
                int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);
                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);
            }

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case DEV_MANAGER_EVENT_RTC_UPDATED:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "DEV_MANAGER_EVENT_RTC_UPDATED\r\n");

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

        }
        break;

        case DEV_MANAGER_EVENT_NEW_DEVICE_ADDED:
        {
            if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
            {
                DeviceInfoReport_t* PeripheralInfo = BleManagerParseNewPeripheralAddedReportEventObj(arg);
                if (PeripheralInfo)
                {
                    //Get the security key
                    SecurityKeyCodeContainer_t* SecurityKey = BLEDevKeeper_GetSecurityKeyFromIndexAlloc(PeripheralInfo->DevHandlerID);

                    //Get the FwVersion
                    const FwVersion_t* DevFwVersion = BLEDevKeeper_GetFwVersionFromIndex(PeripheralInfo->DevHandlerID);
                    char  DeviceVersionStr[16];
                    if (DevFwVersion)
                    {
                        memset( &DeviceVersionStr, 0, sizeof(DeviceVersionStr) );
                        snprintf( DeviceVersionStr , sizeof(DeviceVersionStr), "%d.%d.%d", DevFwVersion->Major, DevFwVersion->Middle, DevFwVersion->Minor );
                    }

                    unsigned short SignalStrength;
                    CONVERT_SIGNAL_STREGTH(PeripheralInfo->rssi, SignalStrength);

                    if ( DKServerManager_AddPeripheral_Asynch(          EventHeader.SourceOfOrigin,
                                                                        EventHeader.IdentificationNumber,
                                                                        PeripheralInfo->DevAddress.Addr,
                                                                        sizeof(PeripheralInfo->DevAddress.Addr),
                                                                        "MyDevice", //Default Name
                                                                        PeripheralInfo->DkAdvertisementData.Header.ProductID,
                                                                        SignalStrength,
                                                                        PeripheralInfo->DkAdvertisementData.BatteryInfo,
                                                                        (DevFwVersion) ? DeviceVersionStr:"1.0",
                                                                        (SecurityKey) ? (uint8_t*)SecurityKey->value:NULL,
                                                                        (SecurityKey) ? SecurityKey->Size:0) != DKSERVER_STATUS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) DKServerManager_AddPeripheral_Asynch FAILED\r\n",
                                         __LINE__);

                        int16_t StatusReport = JOB_FAILED_INTERNAL;
                        cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

                        BleManagerUnPairDeviceTask_Asynch(  PeripheralInfo->DevAddress,
                                                            (10*SECONDS),
                                                            SOURCE_OF_ORIGIN_DEFAULT,
                                                            0);
                    }
                    else
                    {
                        CentralOfflineSupportFlagSet(OFFLINE_FLAG_PERIPHERAL_INFO_UPDATED);
                        BLEManager_WhiteList_AddFromKeeperList();
                        BleRepeater_UpdateWatchList ();
#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
                        if(bHpClientFrwServerReadyReported)
                        {
                            if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
                            {
                                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
                            }
                        }
#endif
#endif  //#if DEVICE_IS_HYBRID

                        //Save the task info.
                        IntTaskInfoSave(PeripheralInfo->DevHandlerID, EventHeader.IdentificationNumber);
                    }

                    if (SecurityKey)
                    {
                        dxMemFree(SecurityKey);
                    }
                }
            }
            else
            {
                int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);

                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);
            }

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_UNPAIRED:
        {

            DeviceInfoReport_t* UnpairedPeripheralInfo = BleManagerParseUnpairedPeripheralReportEventObj(arg);

            if (UnpairedPeripheralInfo)
            {

                CentralOfflineSupportFlagSet(OFFLINE_FLAG_PERIPHERAL_INFO_UPDATED);
                BLEManager_WhiteList_AddFromKeeperList();
                BleRepeater_UpdateWatchList ();

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
                if(bHpClientFrwServerReadyReported)
                {
                    if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
                    }
                }
#endif
#endif //#if DEVICE_IS_HYBRID

                JobManagerPeripheralUnpaired(UnpairedPeripheralInfo);

                ObjectDictionary_Lock ();
                uint32_t PeripheralObjID = ObjectDictionary_FindPeripheralObjectIdByMac((const uint8_t*) UnpairedPeripheralInfo->DevAddress.Addr, B_ADDR_LEN_8);
                ObjectDictionary_Unlock ();

                if (PeripheralObjID)
                {
                    JobManagerRemovePeripheralSpecificJobs_Task(PeripheralObjID, SOURCE_OF_ORIGIN_DEFAULT, 0);
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING: Peripheral ObjID not found\r\n");
                }
            }

            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);

            cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

            //TODO: Calling this is a problem due it is protected by sub state machine! Need to change the sub state but is it really
            //      necessary to go over all the server synch again??
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Calling DKServerManager_GetPeripheralByMAC_Asynch, SubState=%d\r\n", SimpleStateMachine_GetCurrentState(OnlineSubStateHandler));

            peripheralExpectedItemCountForCurrentRequest = GET_PERIPHERAL_MAX_PER_REQUEST;
            peripheralExpectedTotal = 0;
            peripheralNextItemIndex = GET_PERIPHERAL_STARTING_INDEX;

            DKServer_StatusCode_t serverResult = DKServerManager_GetPeripheralByMAC_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                GET_PERIPHERAL_STARTING_INDEX,
                GET_PERIPHERAL_MAX_PER_REQUEST
            );
            if (serverResult != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) DKServerManager_GetPeripheralByMAC_Asynch failed (ret error %d)\r\n",
                    __LINE__,
                    serverResult
                );
            } else {
                peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_CHECK_GATEWAY);
            }

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO:
        {
            SystemInfoReportObj_t* pSystemInfo = BleManagerParsePeripheralSystemInfoReportEventObj(arg);

            //TODO: We can avoid handling this in this state if we add sub state in offline state to make sure
            //      we received the ble device system info first then follow by wifi connect, etc... but this may delay
            //      the process as ble manager may take more than 10 seconds in case of communication issue/recovery
            if (pSystemInfo)
            {
                if (BleManager_SBL_Enable())
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "BLE FW Image is ACTIVE_IMAGE_TYPE_SBL Changing state to BLE FW Download\r\n");
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "NOTE: We are in server online state and we should not get here\r\n");

                    //We are not suppose to get here as offline is responsible to handle this before it gets to
                    //server online state.
                    CentralStateMachine_ChangeState(STATE_FW_UPDATE, NULL, 0);
                }
            }
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD : {
#if DEVICE_IS_HYBRID
            uint16_t dataLen;
            uint8_t* data = BleManagerParseConcurrentPeripheralDataInPayloadReportEventObj (arg, &dataLen);

            cUtlHybridPeripheralReceiveConcurrentPeripheralData (dataLen, data);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_STATUS_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralGetStatusResponse_t* statusResponse = BleManagerParseConcurrentPeripheralStatusPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralStatusResponse (statusResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_BD_ADDRESS : {
#if DEVICE_IS_HYBRID
            SystemBdAddressResponse_t* bdAddressResponse = BleManagerParsePeripheralSystemBdAddressReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralBdAddressResponse (bdAddressResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SECURITY_KEY_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse = BleManagerParseConcurrentPeripheralSecurityKeyPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralSecurityKeyResponse (securityKeyResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_ADVERTISE_DATA_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse = BleManagerParseConcurrentPeripheralAdvertiseDataPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralAdvertiseDataResponse (advertiseDataResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SCAN_RESPONSE_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse = BleManagerParseConcurrentPeripheralScanResponsePayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralScanResponseResponse (scanResponseResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SERVICE_CHARACTERISTIC_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse = BleManagerParseConcurrentPeripheralServiceCharacteristicPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralServiceCharacteristicResponse (serviceCharacteristicResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SYSTEM_UTILS_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse = BleManagerParseConcurrentPeripheralSystemUtilsPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralSystemUtilsResponse (systemUtilsResponse);
#endif
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t SrvOnline_EventJobManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);


    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    if (EventHeader && EventHeader->EventType != JOB_MANAGER_EVENT_PERIPHERAL_DATA_KEEP_ALIVE)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "%s EvtType = %d\r\n", __FUNCTION__, EventHeader->EventType);
    }

    switch (EventHeader->EventType)
    {
        case JOB_MANAGER_EVENT_PERIPHERAL_ACCESS:
        {

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "JOB_MANAGER_EVENT_PERIPHERAL_ACCESS\r\n");

            uint8_t* PeripheralJobExec = JobManagerGetEventObjPayloadData(arg);

            BLEManager_result_t PeripheralAccessResult = BleManagerDeviceAccessWriteTaskExt_Asynch(PeripheralJobExec, EventHeader->Timeout, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

            if (PeripheralAccessResult != DEV_MANAGER_SUCCESS)
            {
                 G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                  "BleManagerDeviceAccessWriteTaskExt_Asynch Failed with ResultCode = %d\r\n",PeripheralAccessResult  );
            }
            else
            {
                LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode );
                }
            }

        }
        break;

        case JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS:
        {

#if DEVICE_IS_HYBRID
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS\r\n");

            HybridPeripheralJobExecHeader_t* PeripheralJobExec = (HybridPeripheralJobExecHeader_t*)JobManagerGetEventObjPayloadData(arg);
            HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
            pCmdMessage->Priority = 0; //Normal
            pCmdMessage->MessageID = 0;
            pCmdMessage->TimeStamp = EventHeader->TimeStamp;
            switch (PeripheralJobExec->CharFlag.bits.ReadWriteMode_b1_3)
            {
                case WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ:
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST;
                    break;
                case READ_FOLLOW_BYAGGREGATIONUPDATE:
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST;
                    break;
                default: //Default make it a write access only
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_WRITE_REQUEST;
                    break;
            }

            HpControlAccessReq_t* pControlAccessInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(HpControlAccessReq_t) + PeripheralJobExec->DataLen);

            pControlAccessInfo->SourceOfOrigin = EventHeader->SourceOfOrigin;
            pControlAccessInfo->IdentificationNumber = EventHeader->IdentificationNumber;
            pControlAccessInfo->DataLen = PeripheralJobExec->DataLen;
            memcpy(pControlAccessInfo->ServiceUUID, PeripheralJobExec->ServiceUUID, sizeof(pControlAccessInfo->ServiceUUID));
            memcpy(pControlAccessInfo->CharacteristicUUID, PeripheralJobExec->CharacteristicUUID, sizeof(pControlAccessInfo->CharacteristicUUID));
            memcpy(pControlAccessInfo->pDataPayload, ((uint8_t*)PeripheralJobExec) + sizeof(HybridPeripheralJobExecHeader_t), PeripheralJobExec->DataLen);

            pCmdMessage->PayloadData = (uint8_t*)pControlAccessInfo;
            pCmdMessage->PayloadSize = sizeof(HpControlAccessReq_t) + PeripheralJobExec->DataLen;

            //Send the message to HybridPeripheral
            HpManagerSendTo_Client(pCmdMessage);
            HpManagerCleanEventArgumentObj(pCmdMessage);

#else
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS not supported in pure central mode\r\n");

#endif
        }
        break;


        case JOB_MANAGER_EVENT_PERIPHERAL_STATUS_NEEDS_UPDATE:
        {
            //TODO: Should provide identification number and save all necessary info, in case of failure we should reset the status from Job manager so that
            //      it will update again for such peripheral

            DeviceInfoReport_t* PeripheralDevInfoReport = (DeviceInfoReport_t*)JobManagerGetEventObjPayloadData(arg);

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE
            cUtlHybridPeripheralDeviceInfoUpdate(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, PeripheralDevInfoReport);
#endif  //#if DEVICE_IS_HYBRID
#endif  //#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE

            unsigned short CurrentSignalStrength = SIGNAL_STRENGTH_NONE;

            CONVERT_SIGNAL_STREGTH_ABS(PeripheralDevInfoReport->rssi,CurrentSignalStrength)

            //G_SYS_DBG_LOG_V3(  "***Job Event (%d), RSSI:%d SG:%d\r\n", EventHeader->EventType, PeripheralDevInfoReport->rssi, CurrentSignalStrength);

            ObjectDictionary_Lock ();
            uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) PeripheralDevInfoReport->DevAddress.Addr, sizeof (PeripheralDevInfoReport->DevAddress.Addr));
            ObjectDictionary_Unlock ();

            DKServerManager_UpdatePeripheralStatusInfo_Asynch (SOURCE_OF_ORIGIN_DEFAULT,
                                                               0,
                                                               &ObjectID,
                                                               CurrentSignalStrength,
                                                               PeripheralDevInfoReport->DkAdvertisementData.BatteryInfo,
                                                               PeripheralDevInfoReport->Status.flag);

        }
        break;

        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_KEEP_ALIVE:
        {
            DeviceInfoReport_t* PeripheralDevInfoReport = (DeviceInfoReport_t*)JobManagerGetEventObjPayloadData(arg);

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE
            cUtlHybridPeripheralDeviceInfoUpdate(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, PeripheralDevInfoReport);
#endif  //#if DEVICE_IS_HYBRID
#endif  //#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE

            /*  Send the device info to Udp Manager.. It's up to UDP manager if the information is going to be sent to local network or not
                We send the data either its a keep alive data or data integrity changed*/

            UdpCommManager_result_t UdpManagerResult = UdpCommManagerSendDeviceInfo((uint8_t*)PeripheralDevInfoReport, sizeof(DeviceInfoReport_t));

            if ((UdpManagerResult != UDP_COMM_MANAGER_SUCCESS) && (UdpManagerResult != UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING))
            {
                 G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                  "Failed to send device info via UDP, Code = %d\r\n", UdpManagerResult  );
            }

            /* STEP 2 */
            /*  Send the device info to Dk Server manager.. It's up to Dk Server manager if the information is going to be sent and what to send
                NOTE: For Dk server update we should ignore KEEEP_ALIVE event unless there is a reason to update the same data integrity again*/

            if ((EventHeader->EventType == JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE) ||
                (EventHeader->EventType == JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY) ||
                (EventHeader->EventType == JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY))
            {
                //TODO: We should try to handle failure by giving identification number and save some information in the event of failure.
                uint32_t AdvJobID = 0;
                DKServer_Update_DataPayload_Target_Info Target = 0;
                uint8_t CheckForAdvJobId = 0;
                if (EventHeader->EventType == JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY)
                {
                    Target = DKSERVER_UPDATE_DATAPAYLOAD_TARGET_PERIPHERAL_TABLE_ONLY;
                }
                else if (EventHeader->EventType == JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY)
                {
                    Target = DKSERVER_UPDATE_DATAPAYLOAD_TARGET_HISTORY_TABLE_ONLY;
                    CheckForAdvJobId = 1;
                }
                else
                {
                    Target = DKSERVER_UPDATE_DATAPAYLOAD_TARGET_ALL;
                    CheckForAdvJobId = 1;
                }

                if (CheckForAdvJobId)
                {
                    char MacAddrDecStr[21] = {0};
                    if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), PeripheralDevInfoReport->DevAddress.Addr, 8))
                    {
                        uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                        if (MacAddrDec)
                        {
                            JobIdInfo_t JobIdInfo = JobManagerCheckJobIdInfoByMacAddrDec(MacAddrDec, dxTRUE);
                            if (JobIdInfo.AdvObjID)
                            {
                                AdvJobID = JobIdInfo.AdvObjID;
                            }
                        }
                    }
                }

                G_SYS_DBG_LOG_V2( "JobManager Evt Peripheral Data Update(AdvJobID = %d, Evt=%d)\r\n",  AdvJobID, EventHeader->EventType );

                ObjectDictionary_Lock ();
                uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) PeripheralDevInfoReport->DevAddress.Addr, sizeof (PeripheralDevInfoReport->DevAddress.Addr));
                ObjectDictionary_Unlock ();

                DKServer_StatusCode_t DkServerResult = DKServerManager_UpdatePeripheralDataPayload_Asynch (SOURCE_OF_ORIGIN_DEFAULT,
                                                                                                           0,
                                                                                                           &ObjectID,
                                                                                                           PeripheralDevInfoReport->DkAdvertisementData.AdData,
                                                                                                           PeripheralDevInfoReport->DkAdvertisementData.AdDataLen,
                                                                                                           Target,
                                                                                                           AdvJobID);

                if (DkServerResult != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "Failed to send DKServerManager_SavePeripheralStatus_Asynch Code = %d\r\n", DkServerResult  );
                }
            }
        }
        break;

        case JOB_MANAGER_EVENT_ADV_JOB_STATUS_UPDATE:
        {
            AdvJobStatusSrv_t* pAdvJobStatus = (AdvJobStatusSrv_t*)JobManagerGetEventObjPayloadData(arg);

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Sending AdvJob Status  ID = %d, Status = %d\r\n",
                             pAdvJobStatus->AdvObjID, pAdvJobStatus->JobStatus);

            DKServer_StatusCode_t DkServerResult = DKServerManager_UpdateAdvJobStatus_Asynch(EventHeader->SourceOfOrigin,
                                                                                             EventHeader->IdentificationNumber,
                                                                                             pAdvJobStatus,
                                                                                             1);
            if (DkServerResult != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Failed to send DKServerManager_UpdateAdvJobStatus_Asynch Code = %d\r\n",
                                 __LINE__, DkServerResult);
            }
        }
        break;

        case JOB_MANAGER_EVENT_GATEWAY_IDENTIFY:
        {
            int16_t StatusReport = JOB_SUCCESS;
            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_ONE_TIME_IDENTIFY);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case JOB_MANAGER_EVENT_SOFT_RESTART:
        {
            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_SUCCESS);

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RESET);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n", SmpResult);
            }
        }
        break;

        case JOB_MANAGER_EVENT_SCAN_FOR_PERIPHERAL:
        {
            //We are not going to allow scan for peripheral before all info is synched
            if (SimpleStateMachine_StateNotChangingWhileEqualTo(OnlineSubStateHandler, ONLINE_SUBSTATE_ALL_INFO_SYNCHED))
            {
                if (BleManagerIsMaximumPeripehralSupportReached() == dxTRUE)
                {
                    cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_MAXIMUM_CAPACITY_REACHED);
                }
                else
                {
                    uint16_t* ScanDiscoveryTime = (uint16_t*)JobManagerGetEventObjPayloadData(arg);

                    memcpy(&UnpairScanDiscoveryTimeMaxInSecond, ScanDiscoveryTime, sizeof(UnpairScanDiscoveryTimeMaxInSecond));

                    cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_SUCCESS);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "Unpair Periheral start reporting\r\n");

                    if (BleManagerStartReportingUnPairPeripheral() != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "Error BleManagerStartReportingUnPairPeripheral\r\n" );
                    }
                    else
                    {
                        UnpairScanStartedTime = dxTimeGetMS();
                        LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_SCANNING_UNPAIR_PERIPHERALS);
                        if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                             __LINE__,
                                             LedManagerRetCode);
                        }
                    }
                }
            }
            else
            {
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_GENERIC);
            }
        }
        break;

        case JOB_MANAGER_EVENT_UPDATE_PERIPHERAL_RTC:
        {
            stUpdatePeripheralRtcInfo_t* PeripheralUpdateRTCInfo = (stUpdatePeripheralRtcInfo_t*)JobManagerGetEventObjPayloadData(arg);

            BLEManager_result_t ManagerTaskResult = BleManagerUpdateRTCTask_Asynch(PeripheralUpdateRTCInfo, 3000, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

            if (ManagerTaskResult != DEV_MANAGER_SUCCESS)
            {
                int16_t StatusReport = JOB_FAILED_INTERNAL;
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Error sending BleManagerUpdateRTCTask_Asynch to ble manager");
            }
            else
            {
                LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode);
                }
            }
        }
        break;

        case JOB_MANAGER_EVENT_SECURITY_SET_RENEW_PERIPHERAL:
        {
            //We are not going to allow renew peripheral key before all info is synched
            if (SimpleStateMachine_StateNotChangingWhileEqualTo(OnlineSubStateHandler, ONLINE_SUBSTATE_ALL_INFO_SYNCHED))
            {
                stRenewSecurityInfo_t* PeripheralSetRenewSecurityInfo = (stRenewSecurityInfo_t*)JobManagerGetEventObjPayloadData(arg);

                BLEManager_result_t ManagerTaskResult = BleManagerSetRenewSecurityTask_Asynch(PeripheralSetRenewSecurityInfo, 10000, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

                if (ManagerTaskResult != DEV_MANAGER_SUCCESS)
                {
                    int16_t StatusReport = JOB_FAILED_INTERNAL;
                    cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING: Error sending BleManagerSetRenewSecurityTask_Asynch to ble manager");
                }
                else
                {
                    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                         __LINE__,
                                         LedManagerRetCode);
                    }
                }
            }
            else
            {
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_GENERIC);
            }
        }
        break;

        case JOB_MANAGER_EVENT_PAIR_PERIPHERAL:
        {
            //We are not going to allow pair peripheral before all info is synched
            if (SimpleStateMachine_StateNotChangingWhileEqualTo(OnlineSubStateHandler, ONLINE_SUBSTATE_ALL_INFO_SYNCHED))
            {
                stPairingInfo_t* PeripheralPairingInfo = (stPairingInfo_t*)JobManagerGetEventObjPayloadData(arg);

                BLEManager_result_t ManagerTaskResult = BleManagerPairDeviceTask_Asynch(PeripheralPairingInfo, 10000, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

                if (ManagerTaskResult != DEV_MANAGER_SUCCESS)
                {
                    int16_t StatusReport = JOB_FAILED_INTERNAL;
                    cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) Error sending BleManagerPairDeviceTask_Asynch to ble manager",
                                     __LINE__);
                }
                else
                {
                    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_PAIRING_WITH_PERIPHERAL);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                         __LINE__,
                                         LedManagerRetCode);
                    }
                }
            }
            else
            {
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_GENERIC);
            }
        }
        break;

        case JOB_MANAGER_EVENT_UNPAIR_PERIPHERAL:
        {
            //We are not going to allow unpair peripheral before all info is synched
            if (SimpleStateMachine_StateNotChangingWhileEqualTo(OnlineSubStateHandler, ONLINE_SUBSTATE_ALL_INFO_SYNCHED))
            {
                DevAddress_t* PeripheralDevAddressToUnpair = (DevAddress_t*)JobManagerGetEventObjPayloadData(arg);

                BLEManager_result_t BleManagerResult = BleManagerUnPairDeviceTask_Asynch(   *PeripheralDevAddressToUnpair,
                                                                                            (10*SECONDS),
                                                                                            EventHeader->SourceOfOrigin,
                                                                                            EventHeader->IdentificationNumber);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) Failed to send BleManagerUnPairDeviceTask_Asynch Code = %d\r\n",
                                     __LINE__, BleManagerResult);
                }
                else
                {
                    //TODO: We should add another event on BLE manager to notify application that peripheral access is about to start and move below condition to there
                    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                         __LINE__,
                                         LedManagerRetCode);
                    }
                }
            }
            else
            {
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_GENERIC);
            }

        }
        break;

        case JOB_MANAGER_EVENT_NOTIFICATION:
        {
            int16_t StatusReport = JOB_FAILED_GENERIC;
            uint8_t* NotificationPayload = (uint8_t*)JobManagerGetEventObjPayloadData(arg);

            //Notification Payload format is according to Job format specification under notification command id's "payload".
            if (NotificationPayload)
            {

                int16_t     TotalParamSize = 0;
                uint8_t*    pWorkingNotificationPayload = NotificationPayload;
                uint8_t     NotificationKeyLen = (uint8_t)*pWorkingNotificationPayload; pWorkingNotificationPayload++;

                uint8_t*    pKeyAlloc = dxMemAlloc( "Key Buffer", 1, NotificationKeyLen + 1); //+1 for NULL terminate
                memcpy(pKeyAlloc, pWorkingNotificationPayload, NotificationKeyLen); pWorkingNotificationPayload += NotificationKeyLen;

                //First we need to pre-calculate the params size
                uint8_t     TotalParameter = (uint8_t)*pWorkingNotificationPayload; pWorkingNotificationPayload++;
                uint8_t     i = 0;
                uint8_t*    pTempWorkingNotPayload = pWorkingNotificationPayload;
                for (i = 0; i < TotalParameter; i++)
                {
                    uint8_t ParameterLen = (uint8_t)*pTempWorkingNotPayload;
                    pTempWorkingNotPayload++;
                    pTempWorkingNotPayload += ParameterLen;
                    TotalParamSize += (ParameterLen + 1); //+1 for the Zero separation
                }

                //Pack the params into one string buffer with NULL in between the strings
                uint8_t* pParamsAlloc = NULL;
                if (TotalParamSize)
                {
                    pParamsAlloc = dxMemAlloc( "Params Buffer", 1, TotalParamSize + 1); //+1 for NULL terminate

                    if (pParamsAlloc)
                    {
                        uint8_t*    pWriteParamAlloc = pParamsAlloc;
                        uint8_t j = 0;
                        for (j = 0; j < TotalParameter; j++)
                        {
                            uint8_t ParameterLen = (uint8_t)*pWorkingNotificationPayload; pWorkingNotificationPayload++;

                            memcpy(pWriteParamAlloc, pWorkingNotificationPayload, ParameterLen);
                            pWorkingNotificationPayload += ParameterLen;
                            pWriteParamAlloc += (ParameterLen + 1); //+1 to add for ZERO separation
                        }
                    }
                }

                //TODO: We should try to hanlde the failure case by providing identification number and save some info for the handling
                DKServer_StatusCode_t DkServerManagerResult = DKServerManager_SendMessageWithKey_Asynch(SOURCE_OF_ORIGIN_DEFAULT ,0 ,pKeyAlloc, pParamsAlloc, TotalParamSize);
                if (DkServerManagerResult != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DKServerManager_SendMessageWithKey_Asynch Failed (%d)\r\n",
                                     __LINE__, DkServerManagerResult);
                }

                if (pKeyAlloc)
                {
                    dxMemFree(pKeyAlloc);
                }

                if (pParamsAlloc)
                {
                    dxMemFree(pParamsAlloc);
                }

                StatusReport = JOB_SUCCESS;
            }

            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);
        }
        break;

        case JOB_MANAGER_EVENT_UPDATE_LOCAL_PERIPHERAL_NAME:
        {
            int16_t StatusReport = JOB_SUCCESS;
            PeripheralNameUpdate_t* PeripheralNameUpdateInfo = (PeripheralNameUpdate_t*)JobManagerGetEventObjPayloadData(arg);

            DKServer_StatusCode_t DkServerResult = DKServerManager_ChangePeripheralDeviceName(  PeripheralNameUpdateInfo->PeripheralObjID,
                                                                                                PeripheralNameUpdateInfo->PeripheralName);

            if (DkServerResult != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "WARNING:(Line=%d) Failed to send DKServerManager_ChangePeripheralDeviceName Code = %d\r\n",
                                 __LINE__, DkServerResult);

                StatusReport = JOB_FAILED_INVALID_PARAM;
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "JOB_MANAGER_EVENT_UPDATE_LOCAL_PERIPHERAL_NAME (Line=%d) Success\r\n",  __LINE__);
            }

            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);
        }
        break;

        case JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_SUCCESS:
        case JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_FAILED:
        {
            int16_t StatusReport = JOB_FAILED_GENERIC;

            if (EventHeader->EventType == JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_SUCCESS)
            {
                CentralOfflineSupportFlagSet(OFFLINE_FLAG_SCH_JOB_INFO_UPDATED);
                StatusReport = JOB_SUCCESS;
            }

            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);
        }
        break;

        case JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_SUCCESS:
        case JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_FAILED:
        {
            if (EventHeader->EventType == JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_SUCCESS)
            {
                CentralOfflineSupportFlagSet(OFFLINE_FLAG_ADV_JOB_INFO_UPDATED);
            }

            //NOTE: We don't need to send job reporting (cUtlJobDoneReportSendHandler) because advance job is usually
            //      delivered condition and execution separately so its not a direct response to client end (eg App).

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "AdvJob delete success/failure (Evt Type = %d)\r\n", EventHeader->EventType);
        }
        break;

        case JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_UPDATED:
        {
            CentralOfflineSupportFlagSet(OFFLINE_FLAG_SCH_JOB_INFO_UPDATED);
        }
        break;

        case JOB_MANAGER_EVENT_ADVANCE_JOB_UPDATED:
        {
            CentralOfflineSupportFlagSet(OFFLINE_FLAG_ADV_JOB_INFO_UPDATED);
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }

        }
        break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t SrvOnline_EventServerManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    //For any other message type except DKMSG_SYSTEM_ERROR & DKMSG_STATE_CHANGED, we check if server re-direct is required as
    //this account could be configured for QA Test Server. We simply re-login to the QA server and proceed from there as usual.
    if (EventHeader.MessageType != DKMSG_SYSTEM_ERROR &&
        EventHeader.MessageType != DKMSG_STATE_CHANGED)
    {
        //If Access denied we may need to check if we need to re-direct to QA server
        if (EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_ACCESS_DENIED)
        {

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Re-Directing to QA Server!\r\n");

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_HOST_REDIRECT);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_HOST_REDIRECT (%d)\r\n",
                                 __LINE__, SmpResult);
            }
            return STATE_MACHINE_EVENT_NORMAL;
        }
    }

     G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                  "%s EventHeader.MessageType (%d)\r\n", __FUNCTION__, EventHeader.MessageType);

    switch (EventHeader.MessageType)
    {
        case DKMSG_STATE_CHANGED:
        {
            if (EventHeader.s.StateMachineState != DKSERVERMANAGER_STATEMACHINE_STATE_READY) {
                LedManager_result_t LedManagerRetCode = SetLedCondition (LED_CONDITION_DISCONNECTED_BY_DK_SERVER);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode);
                }
            }
             G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                              "DKMSG_STATE_CHANGED (%d)\r\n", EventHeader.s.StateMachineState);
        }
        break;

        case DKMSG_SYSTEM_ERROR:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) DKMSG_SYSTEM_ERROR with ErrorCode: %d\r\n",
                             __LINE__, EventHeader.s.SystemErrorCode);

            //TODO: NEED to check value of EventHeader.s.SystemErrorCode
            //      DKSERVER_SYSTEM_ERROR_DX_RTOS           Return the value when fail to call the following functions
            //                                                  - dxTaskCreate()
            //                                                  - dxEventGroupCreate()
            //                                                  - dxQueueCreate()
            //                                                  - dxQueueSend()
            //                                                  - others OS level API
            //      DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY      Return the value when dxMemAlloc() return NULL
            //      DKSERVER_STATUS_ERROR_NETWORK_TIMEOUT   Timeout occurred while receiving data through socket

            //TODO: For now when we see any error we change the state back to Offline and let it recover.
            //      However, current system error involves with any unsuccessful API return coe from server
            //      so we need to check with David how we can improve the error messaging system instead of switching to offline mode
            //      regardless of what the error is.

            CentralStateMachine_ChangeState(STATE_OFFLINE, NULL, 0);
        }
        break;

        case DKMSG_GATEWAYLOGIN:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                //Relogin success, Now we proceed to the next sub state
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_GET_PERIPHERAL);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_PERIPHERAL (%d)\r\n",
                                     __LINE__, SmpResult);
                }
            }
        }
        break;

        case DKMSG_GETPERIPHERALBYGMACADDRESS:
        {
            //Process get peripheral state machine
            DataDrivenStateMachineResult stateMachineResult = peripheralStateMachine->process (peripheralStateMachine, arg);
            if (stateMachineResult != DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) peripheralStateMachine->process failed (%d)\r\n",
                    __LINE__,
                    stateMachineResult
                );
            }
        }
        break;

        case DKMSG_GETSCHEDULEJOB:
        {
            //Process get schedule job state machine
            DataDrivenStateMachineResult stateMachineResult = scheduleJobStateMachine->process (scheduleJobStateMachine, arg);
            if (stateMachineResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) scheduleJobStateMachine->process failed (%d)\r\n",
                    __LINE__,
                    stateMachineResult
                );
            }
        }
        break;

        case DKMSG_GETADVJOBBYGATEWAY:
        {
            //Process get advance job state machine
            DataDrivenStateMachineResult stateMachineResult = advanceJobStateMachine->process (advanceJobStateMachine, arg);
            if (stateMachineResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) advanceJobStateMachine->process failed (%d)\r\n",
                    __LINE__,
                    stateMachineResult
                );
            }
        }
        break;

        case DKMSG_GETLASTFIRMWAREINFO:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                //Just make sure we are still in the same state and its not about to change
                if (SimpleStateMachine_StateNotChangingWhileEqualTo(OnlineSubStateHandler, ONLINE_SUBSTATE_GET_FW_VERSION))
                {
                    dxBOOL  NewFwAvailable = dxFALSE;
                    uint8_t *pData = (uint8_t *)DKServerManager_GetEventObjPayload(arg);

                    DKFirmare_Info_ID_t fwVersionID = DKFIRMWARE_INFO_ID_WIFI_VERSION;
                    uint8_t *pFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

                    fwVersionID = DKFIRMWARE_INFO_ID_BLE_VERSION;
                    uint8_t *pBleCentralFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

                    //We always check first if we need to update Wifi FW first, if Wifi FW update is not needed then we
                    //move on to check BLE FW update check
                    if (IntCentralFwUpdateRequired(pFirmwareVersion) == dxTRUE)
                    {
                        FwNewUpdateAvailabilityStatus = FR_FW_UPDATE_AVAILABLE_MAIN_FW;
                        NewFwAvailable = dxTRUE;
                    }
                    else if (IntBleFwUpdateRequired(pBleCentralFirmwareVersion) == dxTRUE)
                    {
                        if (dxSubImage_Init(DXSUBIMGBLOCK_ONE) == NULL)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) dxSubImage_Init() RetCode = NULL, BLE firmware update STOPPED\r\n",
                                             __LINE__);
                        }
                        else
                        {
                            FwNewUpdateAvailabilityStatus = FR_FW_UPDATE_AVAILABLE_BLE_FW;
                            NewFwAvailable = dxTRUE;
                        }
                    }

                    //Switch sub state to All info ready
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_GET_USER_INFO);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_USER_INFO (%d)\r\n",
                                         __LINE__, SmpResult);
                    }

                    if ( NewFwAvailable == dxTRUE ) {
                        if ( FW_AUTO_UPDATE_UPON_AVAILABLE ) {
                            FwNewUpdateAvailabilityStatus = FR_FW_UPDATE_AVAILABLE_AUTO_UPDATE;
                        }
                    }
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "WARNING:(Line=%d) DKServerManager_GetLastFirmwareInfo_Asynch failed\r\n", __LINE__);
            }
        }
        break;

        case DKMSG_GETCURRENTUSER:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                CurrentUserInfo_t *pUserInfo = (CurrentUserInfo_t *)DKServerManager_GetEventObjPayload(arg);
                if (pUserInfo)
                {
                    G_SYS_DBG_LOG_V("DKMSG_GETCURRENTUSER Language = %s\n", pUserInfo->PreferLang);

                    /*dxDEV_INFO_RET_CODE DevInfoRet = */dxDevInfo_IntegrityCheck(dxTRUE);
                    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "dxDevInfo_IntegrityCheck = %d\r\n", DevInfoRet);

                    /*DevInfoRet = */dxDevInfo_Update(DX_DEVICE_INFO_TYPE_USER_INFO_PREFER_LANG, pUserInfo, sizeof(CurrentUserInfo_t) + 1, dxTRUE);
                    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "dxDevInfo_Update DX_DEVICE_INFO_TYPE_USER_INFO_PREFER_LANG = %d\r\n", DevInfoRet);

                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_ALL_INFO_SYNCHED);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_ALL_INFO_SYNCHED (%d)\r\n",
                                         __LINE__, SmpResult);
                    }

                }
            }
        }
        break;

        case DKMSG_NUMOFACTIVEUSERS:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                uint32_t* pNumOfActiveUsers = (uint32_t*) DKServerManager_GetEventObjPayload (arg);

                if (pNumOfActiveUsers && *pNumOfActiveUsers) {
                    activeUserDetected = dxTRUE;
                } else {
                    activeUserDetected = dxFALSE;
                }

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "SERVER activeUserDetected=%d\r\n",
                    activeUserDetected
                );

                if (activeUserDetected) {
                    //We have active user, let's extend the session alive time for 30 seconds (This is NOT accumulative so its ok to call this frequently)
                    JobManagerMobileClientSessionTimeExtend (30);
                }

#if DEVICE_IS_HYBRID
                    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                    pCmdMessage->Priority = 0; //Normal
                    pCmdMessage->MessageID = 0;
                    pCmdMessage->CommandMessage = (activeUserDetected == dxTRUE) ? HP_CMD_FRW_STATUS_REPORT_ACTIVE_USER_DETECTED:HP_CMD_FRW_STATUS_REPORT_NO_ACTIVE_USER;
                    pCmdMessage->PayloadData = NULL;
                    pCmdMessage->PayloadSize = 0;

                    //Send the message to HybridPeripheral
                    HpManagerSendTo_Client(pCmdMessage);
                    HpManagerCleanEventArgumentObj(pCmdMessage);
#endif
            }
        }
        break;

        case DKMSG_GETSERVERTIME:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                ClientServerTime_t *pTimeInfo = (ClientServerTime_t *)DKServerManager_GetEventObjPayload(arg);
                if (pTimeInfo)
                {
                    G_SYS_DBG_LOG_NV("Line %d, ServerTimeStamp: %u, ClientTimeZone: %d\n",
                                     __LINE__, pTimeInfo->nServerTimeStamp, pTimeInfo->nClientTimeZone);

                    DKTime_t CurrentDkTime;

                    CurrentDkTime = ConvertUTCToDKTime(pTimeInfo->nServerTimeStamp);
                    CurrentDkTime.TimeZone = pTimeInfo->nClientTimeZone;
                    dxSwRtcSet(CurrentDkTime);
                    dxHwRtcSet( CurrentDkTime );
                }
            }
        }
        break;
        /*
        case DKMSG_DATETIME_UPDATE:
        {
             G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                              "DKMSG_DATETIME_UPDATE (%d)\r\n", EventHeader.s.ReportStatusCode);

            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                DKTime_t *dkTime = (DKTime_t *)DKServerManager_GetEventObjPayload(arg);
                if (dkTime && EventHeader.DataLen == sizeof(DKTime_t))
                {
                    dxSwRtcSet(*dkTime);
                }
            }
        }
        break;
        */
        case DKMSG_GETDATACONTAINERBYID:
        case DKMSG_CREATECONTAINERMDATA:
        case DKMSG_UPDATECONTAINERMDATA:
        case DKMSG_UPDATECONTAINERDDATA:
        case DKMSG_INSERTCONTAINERDDATA:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                if (EventHeader.SubType == DKJOBTYPE_CONTAINER_INFO)
                {
                    dxContainer_t *pCont = NULL;
                    memcpy(&pCont, (dxContainer_t *)DKServerManager_GetEventObjPayload(arg), sizeof(pCont));

                    if (EventHeader.MessageType == DKMSG_GETDATACONTAINERBYID)
                    {
                        //We only dispatch container to HybridPeripheral if the container contain LValue
                        cUtlParseAndDispatchContainerDataReport(pCont, EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, dxTRUE);
                    }
                    else
                    {
                        //NOTE: For any create, insert, update that we send from this device we are going to report
                        //back to hybrid peripheral or other device managers. The dk server manager response's cntainer
                        //comes will full information except LValue. It is up to HybridPeripheral or the device manager
                        //to request LValue specifically if needed.
                        cUtlParseAndDispatchContainerDataReport(pCont, EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, dxFALSE);
                    }
                    cUtlParseContainer(pCont);
                    dxContainer_Free(pCont);
                }
            }
        }
        break;

        case DKMSG_DELETECONTAINERDDATA:
        case DKMSG_DELETECONTAINERMDATA:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                if (EventHeader.SubType == DKJOBTYPE_CONTAINER_INFO)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DKMSG_DELETECONTAINER_XX (Implementation NOT Complete)\r\n");
                    dxContainer_t *pCont = NULL;
                    memcpy(&pCont, (dxContainer_t *)DKServerManager_GetEventObjPayload(arg), sizeof(pCont));
                    cUtlParseContainer(pCont);   //TODO, check why container is requried to be passed here
                    dxContainer_Free(pCont);
                }
            }
        }
        break;

        case DKMSG_NEWJOBS:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DKMSG_NEWJOBS Found\r\n");

                DKStandardJob_t* JobPayload = (DKStandardJob_t*)DKServerManager_GetEventObjPayload(arg);

                if (JobPayload)
                {
                    switch (EventHeader.SubType)
                    {
                        case DKJOBTYPE_STANDARD:
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                             "DKMSG_NEWJOBS JobPayload = DevID = 0x%d, PayloadID = 0x%d\r\n", JobPayload->DeviceIdentificationNumber, JobPayload->PayLoadIdentificationNumber);

                            uint64_t IdentificationNumber = (((uint64_t)JobPayload->ObjectID << 32) | ((uint64_t)JobPayload->DeviceIdentificationNumber << 16) | ((uint64_t)JobPayload->PayLoadIdentificationNumber));

                            JobManagerNewJobWithTime_Task(JobPayload->pPayload, (dxTime_t)JobPayload->TimeStamp, EventHeader.DataLen, SOURCE_OF_ORIGIN_DK_SERVER_JOB, IdentificationNumber);

                        }
                        break;

                        case DKJOBTYPE_ADVANCED_CONDITION:
                        case DKJOBTYPE_ADVANCED_EXECUTION:
                        case DKJOBTYPE_ADVANCED_CONDITION_SINGLE_GATEWAY:
                        {
                            //Process ad non single pack execution.
                            JobManagerNewAdvanceJobWithTime_Task(JobPayload->pPayload, (dxTime_t)JobPayload->TimeStamp, 0, EventHeader.DataLen - sizeof(DKStandardJob_t), JobPayload->ObjectID , SOURCE_OF_ORIGIN_DK_SERVER_JOB, 0);
                        }
                        break;

                        case DKJOBTYPE_ADVANCED_EXECUTION_SINGLE_GATEWAY:
                        {
                            //Process ad single pack execution
                            JobManagerNewAdvanceJobWithTime_Task(JobPayload->pPayload, (dxTime_t)JobPayload->TimeStamp, 1, EventHeader.DataLen - sizeof(DKStandardJob_t), JobPayload->ObjectID , SOURCE_OF_ORIGIN_DK_SERVER_JOB, 0);
                        }
                        break;

                        default:
                             G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                              "JobType (%d) Not yet supported, implementation required\r\n", EventHeader.SubType );
                        break;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d)  No Job Payload found from DKMSG_NEWJOBS\r\n",
                                     __LINE__);
                }
            }
        }
        break;

        case DKMSG_ADDPERIPHERAL:
        {
            int16_t DevIndex = IntOnlineTaskInfoDevIndexGetAndClear(EventHeader.TaskIdentificationNumber);

            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DKMSG_ADDPERIPHERAL SUCCESS\r\n");
                int16_t StatusReport = JOB_SUCCESS;
                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, StatusReport);
            }
            else
            {
                 G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                  "WARNING:(Line=%d) DKMSG_ADDPERIPHERAL FAILED with code = %d\r\n",
                                  __LINE__, EventHeader.s.ReportStatusCode);
                int16_t StatusReport = JOB_FAILED_SERVER_ERROR;
                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, StatusReport);

                //We recover by unpair/removing the device from Gateway as adding the device to server failed!
                if (DevIndex >= 0)
                {
                    const DeviceInfoKp_t* DevInfoKeeper = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

                    if (DevInfoKeeper)
                    {
                        DevAddress_t DevAddress;
                        memcpy(DevAddress.Addr, DevInfoKeeper->DevAddress.pAddr, DevInfoKeeper->DevAddress.AddrLen);
                        BleManagerUnPairDeviceTask_Asynch(  DevAddress,
                                                            (10*SECONDS),
                                                            SOURCE_OF_ORIGIN_DEFAULT,
                                                            0);
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DKMSG_ADDPERIPHERAL with invalid DevIndex (%d)\r\n",
                                     __LINE__, DevIndex);
                }
            }
        }
        break;

       case DKMSG_SETPERIPHERALSECURITYKEY:
        {
            int16_t DevIndex = IntOnlineTaskInfoDevIndexGetAndClear(EventHeader.TaskIdentificationNumber);

            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "DKMSG_SETPERIPHERALSECURITYKEY SUCCESS\r\n");
                int16_t StatusReport = JOB_SUCCESS;
                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, StatusReport);
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) DKMSG_SETPERIPHERALSECURITYKEY FAILED\r\n",
                                 __LINE__);

                int16_t StatusReport = JOB_FAILED_SERVER_ERROR;
                cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.TaskIdentificationNumber, StatusReport);

                if (DevIndex >= 0)
                {
                    const DeviceInfoKp_t* DevInfoKeeper = BLEDevKeeper_GetDeviceInfoFromIndex(DevIndex);

                    if (DevInfoKeeper)
                    {
                        //Server failed to save the security key, let's clear the security so that it can be renew again.
                        DevAddress_t DevAddress;
                        memcpy(DevAddress.Addr, DevInfoKeeper->DevAddress.pAddr, DevInfoKeeper->DevAddress.AddrLen);
                        BleManagerClearSecurityTask_Asynch(DevAddress , 10000, SOURCE_OF_ORIGIN_DEFAULT, 0);
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DKHTTP...SECURITYKEY with invalid DevIndex (%d)\r\n",
                                     __LINE__, DevIndex);
                }
            }
        }
        break;

        case DKMSG_CLEARPERIPHERALSECURITYKEY:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                 G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                  "DKMSG_CLEARPERIPHERALSECURITYKEY SUCCESS\r\n");
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) DKMSG_CLEARPERIPHERALSECURITYKEY FAILED\r\n",
                                 __LINE__);
            }
        }
        break;

        case DKMSG_UPDATEGATEWAYFIRMWAREVERSION:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                dxBOOL                  bSaveInfoAllUpdated = dxTRUE;
                SystemInfoReportObj_t   SystemInfoContainer;
                char                    MainFwVersinData[16]; //16 byte should be more than enough for format "xx.xx.xx"

                BLEManager_result_t BleResult = BleManagerSystemInfoGet(&SystemInfoContainer);
                if (BleResult == DEV_MANAGER_SUCCESS)
                {
                    //Update the FW version to flash
                    dxDevInfo_IntegrityCheck(dxTRUE);

                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, SystemInfoContainer.Version, sizeof(SystemInfoContainer.Version), dxFALSE);
                    if (DevInfoRet != DXDEV_INFO_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION Update failed (%d)\r\n",
                                         __LINE__, DevInfoRet);
                        bSaveInfoAllUpdated = dxFALSE;
                    }

                    memset(MainFwVersinData,0,sizeof(MainFwVersinData));
                    memcpy(MainFwVersinData, RTK_WIFI_CURRENT_FW_VERSION, strlen(RTK_WIFI_CURRENT_FW_VERSION));
                    DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, MainFwVersinData, sizeof(MainFwVersinData), dxFALSE);
                    if (DevInfoRet != DXDEV_INFO_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) DX_DEVICE_INFO_TYPE_DEV_FW_VERSION Update failed (%d)\r\n",
                                         __LINE__, DevInfoRet);
                        bSaveInfoAllUpdated = dxFALSE;
                    }

                    if (bSaveInfoAllUpdated)
                    {
                        DevInfoRet =  dxDevInfo_SaveToDisk();
                        if (DevInfoRet != DXDEV_INFO_SUCCESS)
                        {
                             G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                              "WARNING:(Line=%d) DKMSG_UPDATEGATEWAYFIRMWAREVERSION dxDevInfo_SaveToDisk failed (%d)\r\n",
                                              __LINE__, DevInfoRet);
                        }
                    }
                }
            }
            else
            {
                //Failed to update, let's retry it again
                mDeviceFwVersionUpdateCheck = dxTRUE;
            }
        }
        break;

        case DKMSG_DELETESCHEDULEJOB:
        {
             G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                              "DKMSG_DELETESCHEDULEJOB - StatusCode: %d\r\n", EventHeader.s.ReportStatusCode);
            IntIterateDeleteScheduleJobListExecution();
        }
        break;

        case DKMSG_DELETEGATEWAY:
        {
            //Gateways is deleted by system itself (eg, from user operation such as Alarm system, etc..)
            //So we are going to clean all the devinfo and then reset for a complete clean up
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
                if (DevInfoRet != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                     __LINE__, DevInfoRet);
                }

                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_RESET);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING: SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n", SmpResult);
                }
            }
        }

        case DKMSG_GETPUSHRTMPLIVEURL:
        {
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
#if DEVICE_IS_HYBRID
                HpStreamingUrlInfo_t* pStreamingUrlInfo = NULL;
                uint32_t              StreamingUrlInfoSize = 0;
                if (EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK &&	EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
                {
                    uint8_t *pURL = DKServerManager_GetEventObjPayload(arg);
                    int16_t UrlDataLen = EventHeader.DataLen;

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "liveURL:(%d)[%s]\n", UrlDataLen, pURL);

                    StreamingUrlInfoSize = sizeof(HpStreamingUrlInfo_t) + UrlDataLen;
                    pStreamingUrlInfo = (HpStreamingUrlInfo_t*)dxMemAlloc("HpCmdMsgpayload", 1, StreamingUrlInfoSize);
                    memset(pStreamingUrlInfo, 0, StreamingUrlInfoSize);
                    pStreamingUrlInfo->UrlLen = UrlDataLen;
                    memcpy(pStreamingUrlInfo->pUrl, pURL, UrlDataLen);

                }
                else
                {
                    StreamingUrlInfoSize = sizeof(HpStreamingUrlInfo_t);
                    pStreamingUrlInfo = (HpStreamingUrlInfo_t*)dxMemAlloc("HpCmdMsgpayload", 1, StreamingUrlInfoSize);
                    memset(pStreamingUrlInfo, 0, StreamingUrlInfoSize);

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                    "WARNING: Failed to receive DKMSG_GETPUSHRTMPLIVEURL:(%d)(%d)\n", EventHeader.HTTPStatusCode, EventHeader.s.ReportStatusCode);
                }

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = (uint32_t)EventHeader.TaskIdentificationNumber;
                pCmdMessage->CommandMessage = HP_CMD_STREAMING_URL_RES;
                pCmdMessage->PayloadData = (uint8_t*)pStreamingUrlInfo;
                pCmdMessage->PayloadSize = StreamingUrlInfoSize;

                //Send the message to HybridPeripheral
                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);
#endif
            }
        }
        break;

        case DKMSG_GETEVENTMEDIAFILEUPLOADPATH:
        {
            if (EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK && EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
                {
#if DEVICE_IS_HYBRID
                    IPCamEventFileREP_t *p = (IPCamEventFileREP_t*)DKServerManager_GetEventObjPayload(arg);
                    if (p)
                    {
                        uint16_t nUploadURL_Length = DKServerManager_GetEventMediaFileUploadPath_UploadURL_Length(p);
                        uint16_t nDownloadURL_Length = DKServerManager_GetEventMediaFileUploadPath_DownloadURL_Length(p);
                        uint8_t *pUploadURL = DKServerManager_GetEventMediaFileUploadPath_UploadURL_GetData(p);
                        uint8_t *pDownloadURL = DKServerManager_GetEventMediaFileUploadPath_DownloadURL_GetData(p);

                        if (pUploadURL)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                             "UpLoadURL:(%d)[%s]\n", nUploadURL_Length, pUploadURL);
                        }

                        if (pDownloadURL)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                             "DownloadURL:(%d)[%s]\n", nDownloadURL_Length, pDownloadURL);
                        }

                        uint32_t EventSize = sizeof(HpVideoImageEvtUrlInfo_t) + nUploadURL_Length + nDownloadURL_Length + 2;
                        HpVideoImageEvtUrlInfo_t* pEvtInfo = (HpVideoImageEvtUrlInfo_t*)dxMemAlloc("",1, EventSize);
                        pEvtInfo->UploadUrlLen = nUploadURL_Length+1;       //Includding NULL
                        pEvtInfo->DownloadUrlLen = nDownloadURL_Length+1;   //Includding NULL
                        uint8_t*    pCombinedUrl = pEvtInfo->CombineUrl;
                        if (nUploadURL_Length && pUploadURL)
                        {
                            memcpy(pCombinedUrl, pUploadURL, nUploadURL_Length);
                            pCombinedUrl += nUploadURL_Length+1; //Includding NULL
                        }

                        if (nDownloadURL_Length && pDownloadURL)
                        {
                            memcpy(pCombinedUrl, pDownloadURL, nDownloadURL_Length);
                            pCombinedUrl += nDownloadURL_Length+1; //Includding NULL
                        }

                        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                        pCmdMessage->Priority = 0; //Normal
                        pCmdMessage->MessageID = (uint32_t)EventHeader.TaskIdentificationNumber;
                        pCmdMessage->CommandMessage = HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES;
                        pCmdMessage->PayloadData = (uint8_t*)pEvtInfo;
                        pCmdMessage->PayloadSize = EventSize;

                        //Send the message to HybridPeripheral
                        HpManagerSendTo_Client(pCmdMessage);
                        HpManagerCleanEventArgumentObj(pCmdMessage);

                    }
#endif
                }
            }
        }
        break;

        case DKMSG_SENDMESSAGEWITHKEYNUM:
        {
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
#if DEVICE_IS_HYBRID
                HpEvtGenericRes_t* pEventNotificationRes = dxMemAlloc( "pEventNotificationRes", 1, sizeof( HpEvtGenericRes_t ) );
                if (EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK && EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
                {
                    pEventNotificationRes->Status = HP_EVENT_ACCESS_SUCCESS;
                }
                else
                {
                    pEventNotificationRes->Status = HP_EVENT_ACCESS_ERROR;
                }

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = (uint32_t)EventHeader.TaskIdentificationNumber;
                pCmdMessage->CommandMessage = HP_CMD_EVENT_NOTIFICATION_SEND_RES;
                pCmdMessage->PayloadData = (uint8_t*)pEventNotificationRes;
                pCmdMessage->PayloadSize = sizeof( HpEvtGenericRes_t );

                //Send the message to HybridPeripheral
                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);
#endif
            }
        }
        break;

        case DKMSG_NOTIFYEVENTDONE:
        {
            if ( EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP )
            {
#if DEVICE_IS_HYBRID
                HpEvtGenericRes_t* pNotifyEventDoneRes = dxMemAlloc( "pNotifyEventDoneRes", 1, sizeof( HpEvtGenericRes_t ) );
                if ( EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK && EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS )
                {
                    pNotifyEventDoneRes->Status = HP_EVENT_ACCESS_SUCCESS;
                }
                else
                {
                    pNotifyEventDoneRes->Status = HP_EVENT_ACCESS_ERROR;
                }

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc( "HpCmdMsg", 1, sizeof( HpCmdMessageQueue_t ) );
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = ( uint32_t ) EventHeader.TaskIdentificationNumber;
                pCmdMessage->CommandMessage = HP_CMD_NOTIFY_EVENT_DONE_SEND_RES;
                pCmdMessage->PayloadData = ( uint8_t* ) pNotifyEventDoneRes;
                pCmdMessage->PayloadSize = sizeof( HpEvtGenericRes_t );

                //Send the message to HybridPeripheral
                HpManagerSendTo_Client( pCmdMessage );
                HpManagerCleanEventArgumentObj( pCmdMessage );
#endif
            }
        }
        break;

        case DKMSG_UPDATEPERIPHERALAGGREGATIONDATA:
        {
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
#if DEVICE_IS_HYBRID
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "%s DKMSG_UPDATEPERIPHERALAGGREGATIONDATA Received\r\n", __FUNCTION__);

                int32_t ResStatus = HP_DATA_EXCHANGE_GENERIC_ERROR;
                if (EventHeader.HTTPStatusCode == DK_SERVER_HTTP_OK && EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
                {
                    ResStatus = HP_DATA_EXCHANGE_SUCCESS;
                }

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_RES;
                pCmdMessage->MessageID = (uint32_t)EventHeader.TaskIdentificationNumber;
                pCmdMessage->Priority = 0; //Normal

                HpDataExcRes_t* pHpDataExcRes = dxMemAlloc("HpDataExcRes", 1, sizeof(HpDataExcRes_t));
                pHpDataExcRes->Status = ResStatus;

                pCmdMessage->PayloadSize = sizeof(HpDataExcRes_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataExcRes;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);
#endif
            }
        }
        break;

        case DKMSG_RENEWSESSIONTOKEN:
        {
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "%s DKMSG_RENEWSESSIONTOKEN Received\r\n", __FUNCTION__ );

            // We do not need to relogin because we have already got the new session token.
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState( OnlineSubStateHandler, ONLINE_SUBSTATE_GET_PERIPHERAL );
            if ( SmpResult != SMP_STATE_MACHINE_RET_SUCCESS ) {
                G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                    "WARNING:(Line=%d) SMP unable to switch to ONLINE_SUBSTATE_GET_PERIPHERAL (%d)\r\n", __LINE__, SmpResult );
            }

            break;
        }

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t SrvOnline_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t state = STATE_MACHINE_EVENT_NORMAL;
    DKServer_StatusCode_t result = DKSERVER_STATUS_SUCCESS;
    MqttEvent* event = NULL;
    MqttHeader header = MqttManager_GetEventReportHeader (arg);
    uint8_t* payload = MqttManager_GetEventReportPayload (arg);


    if (header.messageType == MqttMessageType_NORMAL) {

        if (payload != NULL) {
            event = (MqttEvent*) payload;

            if (event->eventId == DKMSG_NEWJOBS) {

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "MQTT Message arrived. NewJobs, (ST_ServerOnlineMain.c, %d)\r\n",
                    __LINE__
                );

                DKServerManager_GetJobs_Asynch (0, 0);

            } else if (event->eventId == DKMSG_NUMOFACTIVEUSERS) {

                uint32_t* activeUser = (uint32_t*) event->data;

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "MQTT Message arrived. ActiveUser=%d, (ST_ServerOnlineMain.c, %d)\r\n",
                    *activeUser,
                    __LINE__
                );

                if (*activeUser > 0) {
                    activeUserDetected = dxTRUE;
                } else {
                    activeUserDetected = dxFALSE;
                }

                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "MQTT activeUserDetected=%d\r\n",
                    activeUserDetected
                );
            }

            MqttManager_CleanEvent (event);

            // Just call JobManagerMobileClientSessionTimeExtend () if
            // active user is detected.
            if (activeUserDetected) {
                JobManagerMobileClientSessionTimeExtend (30);
            }

            if (DKServerManager_IsJobPollingEnabled () == dxTRUE) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    "MQTT enable. call DKServerManager_SetJobPolling (dxFALSE), (ST_ServerOnlineMain.c, %d)\r\n",
                    __LINE__
                );
                result = DKServerManager_SetJobPolling (dxFALSE);
                if (result != DKSERVER_STATUS_SUCCESS) {
                    G_SYS_DBG_LOG_IV (
                        G_SYS_DBG_CATEGORY_MQTTMANAGER,
                        G_SYS_DBG_LEVEL_FATAL_ERROR,
                        "Fail to call DKServerManager_SetJobPolling (dxFALSE), result=%d, (ST_ServerOnlineMain.c, %d)\r\n",
                        result,
                        __LINE__
                    );
                }
            }
        }

    } else {

        // Fallback to polling mechanism, config get job interval
        G_SYS_DBG_LOG_IV (
            G_SYS_DBG_CATEGORY_MQTTMANAGER,
            G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
            "MQTT Fallback, (ST_ServerOnlineMain.c, %d)\r\n",
            __LINE__
        );

        if (DKServerManager_IsJobPollingEnabled () == dxFALSE) {
            G_SYS_DBG_LOG_IV (
                G_SYS_DBG_CATEGORY_MQTTMANAGER,
                G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                "MQTT disable. call DKServerManager_SetJobPolling (dxTRUE), (ST_ServerOnlineMain.c, %d)\r\n",
                __LINE__
            );
            result = DKServerManager_SetJobPolling (dxTRUE);
            if (result != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_MQTTMANAGER,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "Fail to call DKServerManager_SetJobPolling (dxTRUE), result=%d, (ST_ServerOnlineMain.c, %d)\r\n",
                    result,
                    __LINE__
                );
            }
        }
    }

    return state;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t SrvOnline_EventHpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "%s pHpCmdMessage->CommandMessage = %d\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);

    switch (pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_CONFIG_WRITE_REQUEST:
        {

            G_SYS_DBG_LOG_V("Online HP_CMD_CONFIG_WRITE_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpConfigWrite_t *pConfigWrite = (HpConfigWrite_t *) pHpCmdMessage->PayloadData;

                switch(pConfigWrite->ConfigId)
                {
                    default:
                        G_SYS_DBG_LOG_V("Unknown 0x%02x Config ID received! \r\n", pConfigWrite->ConfigId);
                    break;
                }
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE received! \r\n" );
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcStatDataUpdate_t* pDataUpdate = (HpDataExcStatDataUpdate_t*)pHpCmdMessage->PayloadData;

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMType = %d\r\n", pDataUpdate->ContMType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDType = %d\r\n", pDataUpdate->ContDType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValueLen = %d\r\n", pDataUpdate->SValueLen );
                if (pDataUpdate->SValueLen)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValue = %s\r\n", pDataUpdate->SValue );
                }


                int32_t ResStatus = HP_DATA_EXCHANGE_SUCCESS;

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                HpDataExcRes_t* pHpDataExcRes = dxMemAlloc("HpDataExcRes", 1, sizeof(HpDataExcRes_t));
                pHpDataExcRes->Status = ResStatus;

                pCmdMessage->PayloadSize = sizeof(HpDataExcRes_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataExcRes;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "TODO: - Implementation not yet fully completed!\r\n" );
                //NOTE! Below we are explicitly sending it to server by retrieving AdvObjID (if any)..
                //However, the correct method should send the pCOnt to JobManager and let it
                //check for any logn term job (eg, ssend to historic event), advance job condition, etc....

                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                dxContainer_t* pCont = HP_UpdateContainerDDataStd_Pack( pDataUpdate, (uint8_t*)pMainDeviceMacAddr->MacAddr,
                                                                        sizeof(pMainDeviceMacAddr->MacAddr), NULL);

                uint32_t AdvJobID = 0;
                char MacAddrDecStr[21] = {0};
                if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), (uint8_t*)pMainDeviceMacAddr->MacAddr, 8))
                {
                    uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                    if (MacAddrDec)
                    {
                        JobIdInfo_t JobIdInfo = JobManagerCheckJobIdInfoByMacAddrDec(MacAddrDec, dxTRUE);
                        if (JobIdInfo.AdvObjID)
                        {
                            AdvJobID = JobIdInfo.AdvObjID;
                        }
                    }
                }

                //PARSE_Container(pCont);
                cUtlParseContainer(pCont);
                DKServer_StatusCode_t DkServerManRet =  DKServerManager_UpdateContainerDData_Asynch(SOURCE_OF_ORIGIN_DEFAULT,
                                                            pHpCmdMessage->MessageID,
                                                            pCont,
                                                            AdvJobID,
                                                            DKSERVER_UPDATE_DATAPAYLOAD_TARGET_HISTORY_TABLE_ONLY);

                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE DkServerFail = %d\r\n", DkServerManRet );
                }

                dxContainer_Free(pCont);
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE no payload!\r\n",
                                 __LINE__);
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ:
        {

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ received! \r\n" );
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcContainerDataRequest_t* pDataRequet = (HpDataExcContainerDataRequest_t*)pHpCmdMessage->PayloadData;

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMID = %llu\r\n", (unsigned long long)pDataRequet->ContMID );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDID = %llu\r\n", (unsigned long long)pDataRequet->ContDID );

                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                dxContainer_t* pCont = HP_RequestContainerData_Pack(pDataRequet, (uint8_t*)pMainDeviceMacAddr->MacAddr,
                                                                    sizeof(pMainDeviceMacAddr->MacAddr), NULL);

                DKServer_StatusCode_t DkServerManRet = DKServerManager_GetDataContainerByID_Asynch(SOURCE_OF_ORIGIN_DEFAULT,
                                                                        pHpCmdMessage->MessageID,
                                                                        pCont,
                                                                        DXCONTFORMAT_FULLSET);

                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ DkServerFail = %d\r\n", DkServerManRet );
                }

                dxContainer_Free(pCont);

        #if DATA_EXCHANGE_TEST_CODE //Simulation that return back simulated datas
                {
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                //Add 256 on top so that is enough to fit our simulated data.. This is just simulation so we don't really care if we allocate more than enough
                uint16_t HpDataReportSize = sizeof(HpDataExcContainerDataReport_t) + 256;

                HpDataExcContainerDataReport_t* pHpDataReport = dxMemAlloc("HpDataReport", 1, HpDataReportSize);
                pHpDataReport->ContMID = pDataRequet->ContMID;
                pHpDataReport->ContDID = pDataRequet->ContDID;
                pHpDataReport->ContMType = DK_MASTER_DATA_TYPE_AIRCON_IR_REMOTE_ID;
                pHpDataReport->ContDType = DK_DATA_TYPE_REMOTE_IR_LEARNING_GROUP;
                pHpDataReport->DateTime =  1474527081;
                pHpDataReport->SValueLen = sizeof("This is SValue\0");
                pHpDataReport->LValueLen = sizeof("This is LValue\0");
                uint8_t* pPayload = pHpDataReport->CombinedValue;
                memcpy(pPayload, "This is SValue\0", sizeof("This is SValue\0")); pPayload += sizeof("This is SValue\0");
                memcpy(pPayload, "This is LValue\0", sizeof("This is LValue\0"));
                pHpDataReport->ContLVCRC = crc16_ccitt_kermit("This is LValue\0", sizeof("This is LValue\0"));

                pCmdMessage->PayloadSize = sizeof(HpDataExcContainerDataReport_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataReport;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);
                }
        #endif
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ no payload!\r\n",
                                 __LINE__);
            }

        }
        break;

        case HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcCreateFullContainerData_t* pCreateFullDataRequet = (HpDataExcCreateFullContainerData_t*)pHpCmdMessage->PayloadData;

                //TODO: Once DKServerManager is ready we need to handle the data and send it back to server here!!!!
                int32_t ResStatus = HP_DATA_EXCHANGE_SUCCESS;

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                HpDataExcRes_t* pHpDataExcRes = dxMemAlloc("HpDataExcRes", 1, sizeof(HpDataExcRes_t));
                pHpDataExcRes->Status = ResStatus;

                pCmdMessage->PayloadSize = sizeof(HpDataExcRes_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataExcRes;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);

                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                dxContainer_t* pCont = HP_CreateContainerMData_Pack(pCreateFullDataRequet, (uint8_t*)pMainDeviceMacAddr->MacAddr,
                                                                    sizeof(pMainDeviceMacAddr->MacAddr), NULL);

                //PARSE_Container(pCont);
                cUtlParseContainer(pCont);

                DKServer_StatusCode_t DkServerManRet = DKServerManager_CreateContainerMData_Asynch( SOURCE_OF_ORIGIN_DEFAULT,
                                                                                                    pHpCmdMessage->MessageID,
                                                                                                    pCont);
                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ DkServerFail = %d\r\n", DkServerManRet );
                }

                dxContainer_Free(pCont);

        #if DATA_EXCHANGE_TEST_CODE
                {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMType = %d\r\n", pCreateFullDataRequet->ContMType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDType = %d\r\n", pCreateFullDataRequet->ContDType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMNameLen = %d\r\n", pCreateFullDataRequet->ContMNameLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDNameLen = %d\r\n", pCreateFullDataRequet->ContDNameLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValueLen = %d\r\n", pCreateFullDataRequet->SValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValueLen = %d\r\n", pCreateFullDataRequet->LValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContLVCRC = %d\r\n", pCreateFullDataRequet->ContLVCRC );
                uint8_t* pContentPayload = pCreateFullDataRequet->CombinedValue;

                if (pCreateFullDataRequet->ContMNameLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMName = %s\r\n", pContentPayload );
                    pContentPayload += pCreateFullDataRequet->ContMNameLen;
                }

                if (pCreateFullDataRequet->ContDNameLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDName = %s\r\n", pContentPayload );
                    pContentPayload += pCreateFullDataRequet->ContDNameLen;
                }

                if (pCreateFullDataRequet->SValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValue = %s\r\n", pContentPayload );
                    pContentPayload += pCreateFullDataRequet->SValueLen;
                }

                if (pCreateFullDataRequet->LValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValue = %s\r\n", pContentPayload );
                }
                }
        #endif

            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ no payload!\r\n",
                                 __LINE__);
            }

        }
        break;

        case HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcInsertContainerDetailData_t* pInsertDetailDataRequet = (HpDataExcInsertContainerDetailData_t*)pHpCmdMessage->PayloadData;

                //TODO: Once DKServerManager is ready we need to handle the data and send it back to server here!!!!
                int32_t ResStatus = HP_DATA_EXCHANGE_SUCCESS;

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                HpDataExcRes_t* pHpDataExcRes = dxMemAlloc("HpDataExcRes", 1, sizeof(HpDataExcRes_t));
                pHpDataExcRes->Status = ResStatus;

                pCmdMessage->PayloadSize = sizeof(HpDataExcRes_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataExcRes;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);

                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                dxContainer_t* pCont = HP_InsertContainerDData_Pack(pInsertDetailDataRequet, (uint8_t*)pMainDeviceMacAddr->MacAddr,
                                                            sizeof(pMainDeviceMacAddr->MacAddr), NULL);

                //PARSE_Container(pCont);
                cUtlParseContainer(pCont);
                DKServer_StatusCode_t DkServerManRet =  DKServerManager_InsertContainerDData_Asynch(SOURCE_OF_ORIGIN_DEFAULT,
                                                            pHpCmdMessage->MessageID,
                                                            pCont);

                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ DkServerFail = %d\r\n", DkServerManRet );
                }

                dxContainer_Free(pCont);

        #if DATA_EXCHANGE_TEST_CODE
                {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMID = %ld\r\n", pInsertDetailDataRequet->ContMID );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDType = %d\r\n", pInsertDetailDataRequet->ContDType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDNameLen = %d\r\n", pInsertDetailDataRequet->ContDNameLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValueLen = %d\r\n", pInsertDetailDataRequet->SValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValueLen = %d\r\n", pInsertDetailDataRequet->LValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContLVCRC = %d\r\n", pInsertDetailDataRequet->ContLVCRC );

                uint8_t* pContentPayload = pInsertDetailDataRequet->CombinedValue;
                if (pInsertDetailDataRequet->ContDNameLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDName = %s\r\n", pContentPayload );
                    pContentPayload += pInsertDetailDataRequet->ContDNameLen;
                }

                if (pInsertDetailDataRequet->SValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValue = %s\r\n", pContentPayload );
                    pContentPayload += pInsertDetailDataRequet->SValueLen;
                }

                if (pInsertDetailDataRequet->LValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValue = %s\r\n", pContentPayload );
                }
                }
        #endif

            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ no payload!\r\n",
                                 __LINE__);
            }

        }
        break;

        case HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcUpdateContainerDetailData_t* pUpdateDetailDataRequet = (HpDataExcUpdateContainerDetailData_t*)pHpCmdMessage->PayloadData;

                //TODO: Once DKServerManager is ready we need to handle the data and send it back to server here!!!!
                int32_t ResStatus = HP_DATA_EXCHANGE_SUCCESS;

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                HpDataExcRes_t* pHpDataExcRes = dxMemAlloc("HpDataExcRes", 1, sizeof(HpDataExcRes_t));
                pHpDataExcRes->Status = ResStatus;

                pCmdMessage->PayloadSize = sizeof(HpDataExcRes_t);
                pCmdMessage->PayloadData = (uint8_t*)pHpDataExcRes;

                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);

                const WifiCommDeviceMac_t* pMainDeviceMacAddr = WifiCommManagerGetDeviceMac();
                dxContainer_t* pCont = HP_UpdateContainerDData_Pack(pUpdateDetailDataRequet, (uint8_t*)pMainDeviceMacAddr->MacAddr,
                                                            sizeof(pMainDeviceMacAddr->MacAddr), NULL);

                //PARSE_Container(pCont);
                cUtlParseContainer(pCont);
                DKServer_StatusCode_t DkServerManRet =  DKServerManager_UpdateContainerDData_Asynch(SOURCE_OF_ORIGIN_DEFAULT,
                                                            pHpCmdMessage->MessageID,
                                                            pCont,
                                                            0, //advObjID
                                                            DKSERVER_UPDATE_DATAPAYLOAD_TARGET_PERIPHERAL_TABLE_ONLY);

                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ DkServerFail = %d\r\n", DkServerManRet );
                }

                dxContainer_Free(pCont);
        #if DATA_EXCHANGE_TEST_CODE
                {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContMID = %ld\r\n", pUpdateDetailDataRequet->ContMID );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDID = %ld\r\n", pUpdateDetailDataRequet->ContDID );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDType = %d\r\n", pUpdateDetailDataRequet->ContDType );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDNameLen = %d\r\n", pUpdateDetailDataRequet->ContDNameLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValueLen = %d\r\n", pUpdateDetailDataRequet->SValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValueLen = %d\r\n", pUpdateDetailDataRequet->LValueLen );
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContLVCRC = %d\r\n", pUpdateDetailDataRequet->ContLVCRC );

                uint8_t* pContentPayload = pUpdateDetailDataRequet->CombinedValue;
                if (pUpdateDetailDataRequet->ContDNameLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "ContDName = %s\r\n", pContentPayload );
                    pContentPayload += pUpdateDetailDataRequet->ContDNameLen;
                }

                if (pUpdateDetailDataRequet->SValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SValue = %s\r\n", pContentPayload );
                    pContentPayload += pUpdateDetailDataRequet->SValueLen;
                }

                if (pUpdateDetailDataRequet->LValueLen)
                {
                    //WARNING: THIS IS JUST EXAMPLE BECAUSE WE KNOW ITS A STRING SO WE CAN PRINT IT AS STRING
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "LValue = %s\r\n", pContentPayload );
                }
                }
        #endif

            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ no payload!\r\n", __LINE__);
            }
        }
        break;

        case HP_CMD_STREAMING_URL_REQUEST:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_STREAMING_URL_REQUEST received! \r\n" );

            ObjectDictionary_Lock ();
            uint32_t ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) ObjectDictionary_GetMacAddress (), B_ADDR_LEN_8);
            ObjectDictionary_Unlock ();

            DKServer_StatusCode_t DkServerManRet = DKServerManager_GetPushRTMPLiveURL_Asynch(SOURCE_OF_ORIGIN_HP, pHpCmdMessage->MessageID, &ObjectID);
            if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "HP_CMD_STREAMING_URL_REQUEST DkServerFail = %d\r\n", DkServerManRet );
            }

        }
        break;

        case HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {

                HpVideoImageEvtUrlReq_t* pEvtInfo = (HpVideoImageEvtUrlReq_t*)pHpCmdMessage->PayloadData;

                IPCamEventFileREQ_t IpCamFile;
                memset(&IpCamFile, 0, sizeof(IpCamFile));

                ObjectDictionary_Lock ();
                IpCamFile.ObjectID = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) ObjectDictionary_GetMacAddress (), B_ADDR_LEN_8);
                ObjectDictionary_Unlock ();

                switch (pEvtInfo->FileFormat)
                {
                    case HP_FILE_TYPE_MP4:
                        strcpy((char*)IpCamFile.FileFormat, "MP4");
                        break;
                    case HP_FILE_TYPE_ZIP:
                        strcpy((char*)IpCamFile.FileFormat, "ZIP");
                        break;
                    case HP_FILE_TYPE_TAR_GZ:
                        strcpy((char*)IpCamFile.FileFormat, "tar.gz");
                        break;
                    default:
                    break;
                }

                switch (pEvtInfo->EventType)
                {
                    case HP_EVENT_TYPE_VIDEO:
                        IpCamFile.Type = 1;
                        break;
                    case HP_EVENT_TYPE_IMAGE:
                        IpCamFile.Type = 0;
                        break;
                    default:
                    break;
                }

                if (pEvtInfo->DateTime)
                {
                    IpCamFile.DateTime = pEvtInfo->DateTime;
                }
                else
                {
                    DKTime_t TimeRTC;
                    if (dxSwRtcGet(&TimeRTC) == DXOS_SUCCESS)
                    {
                        IpCamFile.DateTime = ConvertDKTimeToUTC(TimeRTC);
                    }
                }

                //TODO: Consider providing this when needed.
                IpCamFile.AdvObjID = 0;

                DKServer_StatusCode_t DkServerManRet = DKServerManager_GetEventMediaFileUploadPath_Asynch(  SOURCE_OF_ORIGIN_HP,
                                                                                                            pHpCmdMessage->MessageID,
                                                                                                            &IpCamFile);
                if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                    "HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST DkServerFail = %d\r\n", DkServerManRet );
                }
            }
        }
        break;

        case HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST received! \r\n" );

            int32_t     RspStatus = HP_EVENT_ACCESS_INVALID_PARAM;
            uint32_t    NotificationNumKey = 0;
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpEvtNotificationReq_t* pEvtNotificationInfo = ( HpEvtNotificationReq_t* ) pHpCmdMessage->PayloadData;
                char    Category[32]; memset(Category, 0, sizeof(Category));
                switch (pEvtNotificationInfo->MessageCategoryType)
                {
                    case HP_EVENT_TYPE_VIDEO:
                        strcat(Category, "video");
                        NotificationNumKey = JOB_NOTIFICATION_IPCAM_VIDEO_EVT_NUM_KEY;
                        break;
                    case HP_EVENT_TYPE_IMAGE:
                        strcat(Category, "image");
                        NotificationNumKey = JOB_NOTIFICATION_IPCAM_IMAGE_EVT_NUM_KEY;
                        break;
                    default:
                        strcat(Category, "video");
                        break;
                }

                switch (pEvtNotificationInfo->MessageCategoryTemplate)
                {
                    case HP_TEMPLATE_STD:
                        strcat(Category, "_std");
                        break;
                    default:
                        strcat(Category, "_std");
                        break;
                }

                switch (pEvtNotificationInfo->MessageCategoryFileType)
                {
                    case HP_FILE_TYPE_MP4:
                        strcat(Category, "_mp4");
                        break;
                    case HP_FILE_TYPE_ZIP:
                        strcat(Category, "_zip");
                        break;
                    case HP_FILE_TYPE_TAR_GZ:
                        strcat(Category, "_tar.gz");
                        break;
                    default:
                        strcat(Category, "_mp4");
                        break;
                }

                if (pEvtNotificationInfo->MessageResourceLen && NotificationNumKey)
                {
                    uint8_t*    pResource = pEvtNotificationInfo->CombinePayload;
                    uint8_t*    pParamBuff = dxMemAlloc("pParamBuff", 1, 128);
                    size_t      ParamSize = 0;
                    DKServer_StatusCode_t DkServerRet = DKServerManager_GetCentralName(pParamBuff, 128);
                    if (DkServerRet != DKSERVER_STATUS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                            "HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST unable to obtain CentralName\r\n");

                        strcat((char*)pParamBuff, "Device");
                    }

                    ParamSize = strlen((char*)pParamBuff);
                    DKServer_StatusCode_t DkServerRes = DKServerManager_SendMessageWithKeyNum_Asynch(   SOURCE_OF_ORIGIN_HP,
                                                                                                        pHpCmdMessage->MessageID,
                                                                                                        NotificationNumKey,
                                                                                                        (uint8_t*)Category,
                                                                                                        pResource,
                                                                                                        pParamBuff,
                                                                                                        (int)ParamSize);
                    if (DkServerRes != DKSERVER_STATUS_SUCCESS)
                    {
                        RspStatus = HP_EVENT_ACCESS_ERROR;
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                        "HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST DkServerFail = %d\r\n", DkServerRes );
                    }
                    else
                    {
                        RspStatus = HP_EVENT_ACCESS_SUCCESS;
                    }

                    dxMemFree(pParamBuff);
                }
            }

            //We only feedback the response right away when sending the notification failed
            if (RspStatus != HP_EVENT_ACCESS_SUCCESS)
            {
                HpEvtGenericRes_t* pEventNotificationRes = dxMemAlloc("pEventNotificationRes",1, sizeof(HpEvtGenericRes_t));
                pEventNotificationRes->Status = RspStatus;
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->CommandMessage = HP_CMD_EVENT_NOTIFICATION_SEND_RES;
                pCmdMessage->PayloadData = (uint8_t*)pEventNotificationRes;
                pCmdMessage->PayloadSize = sizeof(HpEvtGenericRes_t);

                //Send the message to HybridPeripheral
                HpManagerSendTo_Client(pCmdMessage);
                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_NOTIFIY_EVENT_DONE_SEND_REQUEST:
        {
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "HP_CMD_NOTIFIY_EVENT_DONE_SEND_REQUEST received! \r\n" );

            int32_t     RspStatus = HP_EVENT_ACCESS_INVALID_PARAM;
            if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData )
            {
                HpNotifyEvtDoneReq_t* pNotifyEvtDoneInfo = ( HpNotifyEvtDoneReq_t* ) pHpCmdMessage->PayloadData;
                int Type = ( int ) pNotifyEvtDoneInfo->MessageCategoryType;

                if ( pNotifyEvtDoneInfo->MessageResourceLen )
                {
                    uint8_t*    pResource = pNotifyEvtDoneInfo->CombinePayload;
                    DKServer_StatusCode_t DkServerRes = DKServerManager_NotifyEventDone(	SOURCE_OF_ORIGIN_HP,
																							pHpCmdMessage->MessageID,
																							Type,
																							pResource);
                    if (DkServerRes != DKSERVER_STATUS_SUCCESS)
                    {
                        RspStatus = HP_EVENT_ACCESS_ERROR;
                        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "HP_CMD_NOTIFIY_EVENT_DONE_SEND_REQUEST DkServerFail = %d\r\n", DkServerRes );
                    }
                    else
                    {
                        RspStatus = HP_EVENT_ACCESS_SUCCESS;
                    }
                }
            }

            //We only feedback the response right away when sending the notification failed
            if ( RspStatus != HP_EVENT_ACCESS_SUCCESS )
            {
                HpEvtGenericRes_t* pNotifyEventDoneRes = dxMemAlloc( "pNotifyEventDoneRes", 1, sizeof( HpEvtGenericRes_t ) );
                pNotifyEventDoneRes->Status = RspStatus;
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc( "HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t ) );
                pCmdMessage->Priority = 0; //Normal
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->CommandMessage = HP_CMD_NOTIFY_EVENT_DONE_SEND_RES;
                pCmdMessage->PayloadData = ( uint8_t* ) pNotifyEventDoneRes;
                pCmdMessage->PayloadSize = sizeof( HpEvtGenericRes_t );

                //Send the message to HybridPeripheral
                HpManagerSendTo_Client( pCmdMessage );
                HpManagerCleanEventArgumentObj( pCmdMessage );
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ:
        case HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ:
        {
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpEventDataPayloadPackage_t DataPayloadPackage;

                if (pHpCmdMessage->CommandMessage == HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ)
                {
                    G_SYS_DBG_LOG_V(  "Online HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ received! \r\n" );

                    HpDataExcHistoryEventDetailData_t* pHpAdvDataReport = (HpDataExcHistoryEventDetailData_t*)pHpCmdMessage->PayloadData;
                    DataPayloadPackage = cUtlHybridPeripheralHistoryEventDataPayloadPackage(JOB_TYPE_HYBRIDPERIPHERAL,
                                                                                                                        (uint64_t)pHpCmdMessage->MessageID,
                                                                                                                        pHpAdvDataReport->PayloadData,
                                                                                                                        pHpAdvDataReport->PayloadDataSize);
                }
                else //Must be HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ
                {
                    G_SYS_DBG_LOG_V(  "Online HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ received! \r\n" );

                    HpDataExcDataTypeDetailData_t* pHpAdvDataReport = (HpDataExcDataTypeDetailData_t*)pHpCmdMessage->PayloadData;
                    DataPayloadPackage = cUtlHybridPeripheralDataTypeDataPayloadUpdateOnlyPackage(JOB_TYPE_HYBRIDPERIPHERAL,
                                                                                                                        (uint64_t)pHpCmdMessage->MessageID,
                                                                                                                        pHpAdvDataReport->PayloadData,
                                                                                                                        pHpAdvDataReport->PayloadDataSize);
                }

                //We TAG JobID if found.
                char MacAddrDecStr[21] = {0};
                const WifiCommDeviceMac_t* pDeviceMac = WifiCommManagerGetDeviceMac();
                if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), (uint8_t*) pDeviceMac->MacAddr, sizeof(DevAddress_t)))
                {
                    uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                    if (MacAddrDec)
                    {
                        if (JobManagerTagJobIdInfoByMacAddrDec(MacAddrDec) == dxTRUE)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "JobID Tagged\r\n");
                        }
                    }
                }

                JobManagerHybridPeripheralDataUpdate_Task(  &DataPayloadPackage.DeviceInfoReport,
                                                            DataPayloadPackage.SourceOfOrigin,
                                                            DataPayloadPackage.IdentificationNumber);
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_REQ:
        {
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                G_SYS_DBG_LOG_V(  "Online HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_REQ received! \r\n" );
                HpDataAggregatedDataDetailData_t* pAggregateDataPayloadDetail = (HpDataAggregatedDataDetailData_t*)pHpCmdMessage->PayloadData;

                const WifiCommDeviceMac_t* pDeviceMac = WifiCommManagerGetDeviceMac();
                AggregateDataHeader_t AggHeader;
                memcpy(AggHeader.MAC, (uint8_t*)pDeviceMac->MacAddr, sizeof(pDeviceMac->MacAddr));
                memcpy(&AggHeader.DataHeader , &pAggregateDataPayloadDetail->Header, sizeof(pAggregateDataPayloadDetail->Header));

                if (DKServerManager_UpdatePeripheralAggregationData_Asynch( SOURCE_OF_ORIGIN_HP,
                                                                            (uint64_t)pHpCmdMessage->MessageID,
                                                                            &AggHeader,
                                                                            (void*)pAggregateDataPayloadDetail->PayloadData,
                                                                            (int)pAggregateDataPayloadDetail->TotalAggregatedItems) != DKSERVER_STATUS_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "WARNING:(Line=%d) Failed to send DKServerManager_UpdatePeripheralAggregationData_Asynch\r\n",
                                     __LINE__);
                }
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_GET_DEVICE_GENERIC_INFO_REQ : {
            cUtlHybridPeripheralGetDeviceGenericInfo (pHpCmdMessage->MessageID, pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_JOBBRIDGE_JOB_SUPERIOR_ONLY_CONFIG:
        {
            G_SYS_DBG_LOG_V(  "Online HP_CMD_JOBBRIDGE_JOB_SUPERIOR_ONLY_CONFIG received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobSuperiorOnly_t* pSuperiodOnly = (HpJobSuperiorOnly_t*)pHpCmdMessage->PayloadData;

                JobManagerSetJobSuperiorOnly(pSuperiodOnly->bJobSuperiorOnly);
            }
        }
        break;

        case HP_CMD_ENTER_FACTORY_RESET_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "Online HP_CMD_ENTER_FACTORY_RESET_REQUEST received! \r\n" );

            DKServer_StatusCode_t DkServerManRet = DKServerManager_DeleteGateway_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, 1, 1);

            if (DkServerManRet != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "HP_CMD_ENTER_FACTORY_RESET_REQUEST DkServerFail = %d\r\n", DkServerManRet );
            }
        }
        break;

        case HP_CMD_CHECK_FW_UPDATE_AVAILABE_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "Online HP_CMD_CHECK_FW_UPDATE_AVAILABE_REQUEST received! \r\n" );

            HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
            pCmdMessage->CommandMessage = HP_CMD_CHECK_FW_UPDATE_AVAILABE_RES;
            pCmdMessage->MessageID = pHpCmdMessage->MessageID;
            pCmdMessage->Priority = 0; //Normal

            HpFwUpdateAvailableRes_t* pHpFwAvailabilityRes = dxMemAlloc("pHpFwAvailabilityRes", 1, sizeof(HpFwUpdateAvailableRes_t));
            pHpFwAvailabilityRes->FwUpdateAvailabilityStatus = FW_UPDATE_AVAILABLE_NONE;
            switch (FwNewUpdateAvailabilityStatus)
            {
                case FR_FW_UPDATE_AVAILABLE_MAIN_FW:
                    pHpFwAvailabilityRes->FwUpdateAvailabilityStatus = FW_UPDATE_AVAILABLE_MAIN_FW;
                    break;
                case FR_FW_UPDATE_AVAILABLE_BLE_FW:
                    pHpFwAvailabilityRes->FwUpdateAvailabilityStatus = FW_UPDATE_AVAILABLE_BLE_FW;
                    break;
                case FR_FW_UPDATE_AVAILABLE_AUTO_UPDATE:
                    pHpFwAvailabilityRes->FwUpdateAvailabilityStatus = FW_UPDATE_AVAILABLE_AUTO_UPDATE;
                    break;
                default:
                    break;
            }

            pCmdMessage->PayloadSize = sizeof(HpFwUpdateAvailableRes_t);
            pCmdMessage->PayloadData = (uint8_t*)pHpFwAvailabilityRes;

            HpManagerSendTo_Client(pCmdMessage);

            HpManagerCleanEventArgumentObj(pCmdMessage);
        }
        break;

        case HP_CMD_ENTER_FW_UPDATE_REQUEST:
        {
            if (FwNewUpdateAvailabilityStatus != FR_FW_UPDATE_AVAILABLE_NONE)
            {
                CentralStateMachine_ChangeState(STATE_FW_UPDATE, NULL, 0);
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "WARNING: HP_CMD_ENTER_FW_UPDATE_REQUEST State not allowed(%d)\r\n", FwNewUpdateAvailabilityStatus);
            }
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_GET_STATUS_REQ : {
            cUtlHybridPeripheralGetConcurrentPeripheralStatus ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS_REQ : {
            cUtlHybridPeripheralCustomizeConcurrentPeripheralBdAddress ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_SECURITY_KEY_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralSecurityKey ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_ADVERTISE_DATA_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralAdvertiseData (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_SCAN_RESPONSE_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralScanResponse ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_REQ : {
            cUtlHybridPeripheralAddConcurrentPeripheralServiceCharacteristic (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_ENABLE_SYSTEM_UTILS_REQ : {
            cUtlHybridPeripheralEnableConcurrentPeripheralSystemUtils (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_START_REQ : {
            cUtlHybridPeripheralStartConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_STOP_REQ : {
            cUtlHybridPeripheralStopConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_DISCONNECT_REQ : {
            cUtlHybridPeripheralDisconnectConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_DATA_OUT_REQ : {
            cUtlHybridPeripheralTransmitConcurrentPeripheralData (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t SrvOnline_EventWifiManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t SrvOnline_EventUdpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE;

    //NOTE: We are returning STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE so that it is handled by the default state machine

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        case UDP_COMM_EVENT_NEW_JOB:
        {
            char* JobPayload = UdpCommManagerGetEventObjPayloadData(arg);

            if (JobPayload)
            {
                uint64_t IdentificationNumber = (((uint64_t)EventHeader->DeviceIdentificationNumber << 16) | ((uint64_t)EventHeader->PayLoadIdentificationNumber));
                JobManagerNewJob_Task(JobPayload, EventHeader->PayloadLen, SOURCE_OF_ORIGIN_UDP_JOB, IdentificationNumber);
                EventStateRet = STATE_MACHINE_EVENT_NORMAL;
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) No Job Payload found from UDP_COMM_EVENT_NEW_JOB\r\n", __LINE__);
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}




StateMachineEvent_State_t SrvOnline_EventKeyPadManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE;

    dxKeypadReport_t* KeyPadEvent = (dxKeypadReport_t*)arg;

    switch (KeyPadEvent->ReportIdentifier)
    {
        // EventStateRet should set to STATE_MACHINE_EVENT_NORMAL after handling KeyPad event
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, KeyPadEvent->ReportIdentifier);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t SrvOnline_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

StateMachine_result_t ServerOnline_Init(void* data )
{
    char *pHostname = NULL;

    DKServer_StatusCode_t ServerManagerRet = DKServerManager_GetHostname( &pHostname );

    Online_Server_Location = ONLINE_SERVER_LOCATION_MAIN;

    if ( ( ServerManagerRet == DKSERVER_STATUS_SUCCESS ) &&
         ( strcmp( pHostname, DK_QA_SERVER_DNS_NAME ) == 0 ) ) {
        Online_Server_Location = ONLINE_SERVER_LOCATION_QA;
    }

#if MQTT_IS_ENABLED
    if (Online_Server_Location == ONLINE_SERVER_LOCATION_MAIN) {
        MqttManager_SetActiveIndex (0);
    } else {
        MqttManager_SetActiveIndex (1);
    }
#endif

    if ( pHostname ) {
        dxMemFree( pHostname );
    }

    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    FwNewUpdateAvailabilityStatus = FR_FW_UPDATE_AVAILABLE_NONE;

    mDeviceFwVersionUpdateCheck = dxTRUE;

    CentralOfflineSupportFlagClear(OFFLINE_FLAG_ALL);

    //We shut down BLE manager as we are about to get peripheral from server
    //which we will update the device to BLE manager, we don't want BLE manager
    //scanning peripheral while updating the device list.
    BleManagerScanReportSuspend();
    DKServerManager_ConfigGetJobInterval(0);

    // Variable/Member clean up
    scheduleJobDeleteList.TotalJobSlot = 0;
    scheduleJobDeleteList.pJobInfo = NULL;

    //Variable/Member clean up
    uint16_t i = 0;
    for (i = 0; i < MAX_TASK_INFO_STORAGE_SLOT; i++)
    {
        memset(&OnlineTaskInfoStorage[i], 0, sizeof(TaskInfo_t));
    }

    DKServerManager_ConfigGetJobInterval (GET_NEW_JOB_INTERVAL_WHEN_NO_ACTIVE_USER);

    // Create state machine for GetPeripheralByMAC
    peripheralStateMachine = DataDrivenStateMachine_create (
        getPeripheralStateMachineTable
    );
    peripheralStateMachine->changeState (peripheralStateMachine, GET_PERIPHERAL_IDLE);

    // Create state machine for GetScheduleJob
    scheduleJobStateMachine = DataDrivenStateMachine_create (
        getScheduleJobStateMachineTable
    );
    scheduleJobStateMachine->changeState (scheduleJobStateMachine, GET_SCHEDULEJOB_IDLE);

    // Create state machine for GetAdvJob
    advanceJobStateMachine = DataDrivenStateMachine_create (
        getAdvanceJobStateMachineTable
    );
    advanceJobStateMachine->changeState (advanceJobStateMachine, GET_ADVANCEJOB_IDLE);

    OnlineSubStateHandler = SimpleStateMachine_Init (OnlineSubStateProcessFuncTable);
    SimpleStateMachine_ExecuteState (OnlineSubStateHandler, ONLINE_SUBSTATE_IDLE);

    return result;
}

StateMachine_result_t ServerOnline_MainProcess(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    //Kick start the sub state if its IDLE
    if (SimpleStateMachine_GetCurrentState(OnlineSubStateHandler) == ONLINE_SUBSTATE_IDLE)
    {
        //Get Periheral first, because we migh in the process of add gateway so let's get that done as soon as possible
        SimpleStateMachine_ExecuteState(OnlineSubStateHandler, ONLINE_SUBSTATE_GET_PERIPHERAL);
    }

    //Process the sub state machine
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_Process(OnlineSubStateHandler);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SimpleStateMachine_Process failed (%d)\r\n",
                         __LINE__, SmpResult);
    }

    if (OnlineSubStateRoutineCheckHandler)
    {
        SimpleStateMachine_result_t SmpRoutineCheckResult = SimpleStateMachine_Process(OnlineSubStateRoutineCheckHandler);
        if (SmpRoutineCheckResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SimpleStateMachine_Process failed (%d)\r\n",
                             __LINE__, SmpRoutineCheckResult);
        }
    }

    return result;
}

StateMachine_result_t ServerOnline_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;


    JobManagerNotifyServerReadyState(dxFALSE);

    CentralOfflineSupportFlagClear(OFFLINE_FLAG_ALL);

#if DEVICE_IS_HYBRID
    bHpClientFrwServerReadyReported = dxFALSE;
#endif

    // Destroy state machine for GetAdvJob
    if (advanceJobStateMachine) {
        DataDrivenStateMachine_destroy (advanceJobStateMachine);
        advanceJobStateMachine = NULL;
    }

    // Destroy state machine for GetScheduleJob
    if (scheduleJobStateMachine) {
        DataDrivenStateMachine_destroy (scheduleJobStateMachine);
        scheduleJobStateMachine = NULL;
    }

    // Destroy state machine for GetPeripheralByMAC
    if (peripheralStateMachine) {
        DataDrivenStateMachine_destroy (peripheralStateMachine);
        peripheralStateMachine = NULL;
    }

    if (OnlineSubStateHandler) {
        SimpleStateMachine_Uninit(OnlineSubStateHandler);
        OnlineSubStateHandler = NULL;
    }

    if (OnlineSubStateRoutineCheckHandler) {
        SimpleStateMachine_Uninit(OnlineSubStateRoutineCheckHandler);
        OnlineSubStateRoutineCheckHandler = NULL;
    }

    //We need to make sure we stop the unpair peripheral reporting in case it is still enabled (eg, we are about to exit online state while
    //unpair peripheral is enabled).
    BleManagerStopReportingUnPairPeripheral();

    return result;
}


