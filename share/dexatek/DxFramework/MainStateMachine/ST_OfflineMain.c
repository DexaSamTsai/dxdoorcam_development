//============================================================================
// File: ST_OfflineMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#include "HpJobBridge.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "dxDeviceInfoManager.h"
#include "ST_CommonUtils.h"
#include "CentralStateMachineAutomata.h"
#include "SimpleTimedStateMachine.h"
#include "OfflineSupport.h"

//============================================================================
// Function Definition
//============================================================================

SimpleStateMachine_result_t SubState_OfflineWifiConnecting(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OfflineWifiScanDone(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OfflineWifiIsConnected(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OfflineLogInToServer(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_OfflineServerLoggedIn(void* data, dxBOOL isFirstCall);

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

typedef enum
{
    OFFLINE_SUBSTATE_IDLE           = 0,
    OFFLINE_SUBSTATE_WIFI_CONNECTING,
    OFFLINE_SUBSTATE_WIFI_SCAN_DONE,
    OFFLINE_SUBSTATE_WIFI_IS_CONNECTED,
    OFFLINE_SUBSTATE_SERVER_LOGIN,
    OFFLINE_SUBSTATE_SERVER_CONNECTED,
    OFFLINE_SUBSTATE_RESET,

} Offline_SubState_Code_t;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

static simple_st_machine_table_t OfflineSubStateProcessFuncTable[] =
{
    {OFFLINE_SUBSTATE_IDLE, NULL, 0},
    {OFFLINE_SUBSTATE_WIFI_CONNECTING, SubState_OfflineWifiConnecting, 10*SECONDS},    //Call every 10 seconds if remain in the same state
    {OFFLINE_SUBSTATE_WIFI_SCAN_DONE, SubState_OfflineWifiScanDone, 10*SECONDS},       //Call every 10 seconds if remain in the same state
    {OFFLINE_SUBSTATE_WIFI_IS_CONNECTED, SubState_OfflineWifiIsConnected, 0},
    {OFFLINE_SUBSTATE_SERVER_LOGIN, SubState_OfflineLogInToServer, 5*SECONDS},        //Call every 10 seconds if remain in the same state
    {OFFLINE_SUBSTATE_SERVER_CONNECTED, SubState_OfflineServerLoggedIn, 0},
    {OFFLINE_SUBSTATE_RESET, cUtlSubState_Reset, 0},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};

SimpleStateHandler      OfflineSubStateHandler = NULL;

uint8_t     gOfflineTotalWifiConnectionDisconnectionCycle = 0;
dxTime_t    gOfflineServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MIN;

#if DEVICE_IS_HYBRID
#endif //#if DEVICE_IS_HYBRID

//============================================================================
// Utils Function
//============================================================================

//============================================================================
// Internal Function
//============================================================================


SimpleStateMachine_result_t SubState_OfflineWifiConnecting(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    dxTime_t CurrentTick = dxTimeGetMS();

    if (isFirstCall)
    {
        cUtlServerConnectGating_ConnectAttemptStart();
    }


    //NOTE: There is no reason why we prevent first call to process wifi below, unless there is specific case issue which
    //      a delay is required (eg, only after second call to this state).
    //if (isFirstCall == dxFALSE)

    if(cUtlServerConnectGating_AllowStartConnect())
    {
        if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN)
        {
            // Stop UdpCommManager when Wi-Fi connection is down
            UdpCommManagerStop();

#if WIFI_FAIL_COUNT_BEFORE_REBOOT
            if (gOfflineTotalWifiConnectionDisconnectionCycle > WIFI_FAIL_COUNT_BEFORE_REBOOT)
            {
                gOfflineTotalWifiConnectionDisconnectionCycle = 0;

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "WARNING: System Auto-Reset due to maximum wifi connection/disconnection cycle reached!!!\r\n");

                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_RESET);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
            else
#endif
            {
                cUtlConnectToWifi();

                //Switch sub state to wifi connected
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_SCAN_DONE);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_SCAN_DONE (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
        }
        else if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_UP)
        {
            //Switch sub state to wifi connected
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_IS_CONNECTED);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }

    }
    return result;
}

SimpleStateMachine_result_t SubState_OfflineWifiScanDone(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;

    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (isFirstCall)
    {
        TimeAtFirstTry = dxTimeGetMS();
    }

    if (dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_DONE ||
        dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_NONE)
    {
        if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_UP)
        {
            //Switch sub state to wifi connected
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_IS_CONNECTED);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        else if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN)
        {
            // We can't find WiFi AP which we expect to connect to
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
    }
    else if (dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_SCANNING)
    {
        if (dxTimeGetMS() > TimeAtFirstTry + WIFI_SCAN_TIMEOUT_MAX_MS)
        {
            // We doesn't receive WIFI_EVENT_SCAN_DONE (This should not happened)
            // Scan again

            WifiCommManager_result_t wifiresult = WifiCommManagerDisconnectCurrentConnectionTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);

            if (wifiresult == WIFI_COMM_MANAGER_SUCCESS)
            {
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SubState_OfflineWifiConnecting unable to disconnect wifi (%d)\r\n",
                                 __LINE__,
                                 wifiresult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OfflineWifiIsConnected(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    // Restart UdpCommManager when Wi-Fi connection is Up
    UdpCommManagerStart();

    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_CONNECTING_WITH_DK_SERVER);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                         __LINE__,
                         LedManagerRetCode);
    }

    //Simply switch to server log in and let it ahndles it.
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_SERVER_LOGIN);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_SERVER_LOGIN (%d)\r\n",
                         __LINE__,
                         SmpResult);
    }

    gOfflineTotalWifiConnectionDisconnectionCycle++;

    return result;
}


SimpleStateMachine_result_t SubState_OfflineLogInToServer(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    static dxTime_t TimeoutReal = SERVER_LOG_IN_TIMEOUT_MIN;

    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    //TODO: IMPORTANT
    //      We should handle when server state is NOT ready since log-in relies on this
    //      If server state is not ready we should rekick the retry process if this is not automatic in server manager

    if (isFirstCall)
    {
        TimeAtFirstTry = dxTimeGetMS();
        gOfflineServerLoginTimeoutMax = gOfflineServerLoginTimeoutMax + SERVER_LOG_IN_TIMEOUT_ADD;
        if(gOfflineServerLoginTimeoutMax > SERVER_LOG_IN_TIMEOUT_MAX)
        {
            gOfflineServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MAX;
        }

        srand((uint32_t)dxTimeGetMS());
        uint32_t index = (uint32_t)rand();
        TimeoutReal = SERVER_LOG_IN_TIMEOUT_MIN + (index % (gOfflineServerLoginTimeoutMax - SERVER_LOG_IN_TIMEOUT_MIN));

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "%s DKServerManager login timeout is %d\r\n", __FUNCTION__, TimeoutReal);

    }

    if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN)
    {
        //Connection when down, go back to wifi connect
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                             __LINE__,
                             SmpResult);
        }
    }
    else if ((dxTimeGetMS() - TimeAtFirstTry) > TimeoutReal)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) Unable to resume DKServerManager. Disconnect Wi-Fi and try again!!!\r\n",
                         __LINE__);

        TimeAtFirstTry = dxTimeGetMS();

        //Its taking too long for server to log in, we are going to disconnect wifi and start over.
        WifiCommManager_result_t wifiresult = WifiCommManagerDisconnectCurrentConnectionTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);
        if (wifiresult == WIFI_COMM_MANAGER_SUCCESS)
        {
            //Switch sub state to reconnect to wifi
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SubState_OfflineWifiConnecting unable to disconnect wifi (%d)\r\n",
                             __LINE__,
                             wifiresult);
        }
    }
    else if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_UP)
    {
        DKServerManager_StateMachine_State_t ServerState = DKServerManager_GetCurrentState();

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "%s DKServerManager current state is %d\r\n", __FUNCTION__, ServerState);

        if (ServerState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
        {
            DKServerManager_State_Resume();
        }
        else if (ServerState == DKSERVERMANAGER_STATEMACHINE_STATE_READY)
        {
            // NOTE: If call cUtlLogInToServer() here, we will see UserLogin twice in case of hardware reboot.
            //       So, I removed calling cUtlLogInToServer() when receiving DKMSG_STATE_CHANGED
            cUtlLogInToServer();
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_OfflineServerLoggedIn(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    gOfflineServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MIN;

    //Change the main state to Server Online
    CentralStateMachine_ChangeState(STATE_SERVER_ONLINE, NULL, 0);

    return result;
}


//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t Offline_EventBLEManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);
            JobManagerPeripheralDataUpdate_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber);
        }
        break;

        case DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST:
        {
#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                DeviceKeeperListReport_t* pDeviceKeeperList = BleManagerParseDeviceKeeperListReportEventObj(arg);
                cUtlHybridPeripheralDeviceKeeperListUpdate(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, (uint8_t *) pDeviceKeeperList->GenericInfo, sizeof(DeviceGenericInfo_t) * pDeviceKeeperList->DeviceCount);
            }
            else
#endif
#endif //DEVICE_IS_HYBRID
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "INFORMATION:(Line=%d) Receive DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST with SourceOfOrigin(%d)\r\n",
                                 __LINE__,
                                 EventHeader.SourceOfOrigin);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);

            //We change this to FALSE (No force update) 2015/08/31. The reason is because force update will cause issue (eg Light)
            //with repeat historic event update.
            //NOTE: This access with data update force was develop mainly for "Group" access where we need to update
            //the payload as we change its state... But looks like even without force update we can still manage it
            JobManagerPeripheralDataUpdateExt_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, dxFALSE);

        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT:
        {
            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);

#if DEVICE_IS_HYBRID
            if(EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                HpJobBridgeExeRes_t JobBridgeExeRes;
                JobBridgeExeRes.StatusReport = StatusReport;
                JobBridgeExeRes.SourceOfOrigin = EventHeader.SourceOfOrigin;
                JobBridgeExeRes.IdentificationNumber = EventHeader.IdentificationNumber;
                HP_CMD_JobBridge_AdvJobBridgeExeRes(0, &JobBridgeExeRes, 0);
            }
#endif  //#if DEVICE_IS_HYBRID

            cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT:
        {
            DataReadReportObj_t* pReadData = BleManagerParseDataReadReportEventObj(arg);

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);
            cUtlJobDoneReportSendWithPayloadHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport, pReadData->PayloadSize, pReadData->Payload);
        }
        break;


        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO:
        {
            SystemInfoReportObj_t* pSystemInfo = BleManagerParsePeripheralSystemInfoReportEventObj(arg);

            if (pSystemInfo)
            {
                if (BleManager_SBL_Enable())
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "BLE FW Image is ACTIVE_IMAGE_TYPE_SBL Changing state to BLE FW Download\r\n");

                    //We change to FW update state, although we are in offline mode we can still update BLE FW
                    //since the FW is more likely already in Flash.. Reaching here might be because of system
                    //crash during BLE FW update process.
                    CentralStateMachine_ChangeState(STATE_FW_UPDATE, NULL, 0);
                }
            }
        }
        break;

        case DEV_MANAGER_EVENT_RTC_UPDATED:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "DEV_MANAGER_EVENT_RTC_UPDATED\r\n");

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

        }
        break;

        case DEV_MANAGER_EVENT_NEW_DEVICE_ADDED:
        case DEV_MANAGER_EVENT_DEVICE_UNPAIRED:
            BLEManager_WhiteList_AddFromKeeperList();
            BleRepeater_UpdateWatchList ();

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
            }
#endif
#endif  //#if DEVICE_IS_HYBRID
            //no break to enter the following procedure
        case DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT:
        case DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED:
        {

            //We should not get here since this kind of access (that require server) is prevented
            //from JobManager event.
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "NOTE:Why are we still getting this ble event (%d)?\r\n",
                             EventHeader.ReportEvent);

            //Set the LED to condition none so that it does not remain in the access condition
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_DATA_IN_PAYLOAD : {
#if DEVICE_IS_HYBRID
            uint16_t dataLen;
            uint8_t* data = BleManagerParseConcurrentPeripheralDataInPayloadReportEventObj (arg, &dataLen);

            cUtlHybridPeripheralReceiveConcurrentPeripheralData (dataLen, data);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_STATUS_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralGetStatusResponse_t* statusResponse = BleManagerParseConcurrentPeripheralStatusPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralStatusResponse (statusResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_BD_ADDRESS : {
#if DEVICE_IS_HYBRID
            SystemBdAddressResponse_t* bdAddressResponse = BleManagerParsePeripheralSystemBdAddressReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralBdAddressResponse (bdAddressResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SECURITY_KEY_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralSecurityKeyResponse_t* securityKeyResponse = BleManagerParseConcurrentPeripheralSecurityKeyPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralSecurityKeyResponse (securityKeyResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_ADVERTISE_DATA_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralAdvertiseDataResponse_t* advertiseDataResponse = BleManagerParseConcurrentPeripheralAdvertiseDataPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralAdvertiseDataResponse (advertiseDataResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SCAN_RESPONSE_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralScanResponseResponse_t* scanResponseResponse = BleManagerParseConcurrentPeripheralScanResponsePayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralScanResponseResponse (scanResponseResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SERVICE_CHARACTERISTIC_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralServiceCharacteristicResponse_t* serviceCharacteristicResponse = BleManagerParseConcurrentPeripheralServiceCharacteristicPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralServiceCharacteristicResponse (serviceCharacteristicResponse);
#endif
        }
        break;

        case DEV_MANAGER_EVENT_CONCURRENT_PERIPHERAL_SYSTEM_UTILS_REPORT : {
#if DEVICE_IS_HYBRID
            BleConcurrentPeripheralSystemUtilsResponse_t* systemUtilsResponse = BleManagerParseConcurrentPeripheralSystemUtilsPayloadReportEventObj (arg);
            cUtlHybridPeripheralConcurrentPeripheralSystemUtilsResponse (systemUtilsResponse);
#endif
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Offline_EventJobManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        case JOB_MANAGER_EVENT_PERIPHERAL_ACCESS:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "JOB_MANAGER_EVENT_PERIPHERAL_ACCESS\r\n");

            uint8_t* PeripheralJobExec = JobManagerGetEventObjPayloadData(arg);

            //Check if the execution is supported by offline, we will be able to only block some execution and not all
            if (cUtlIsBleExecutionOfflineSupported(PeripheralJobExec) == dxTRUE)
            {
                BLEManager_result_t PeripheralAccessResult = BleManagerDeviceAccessWriteTaskExt_Asynch(PeripheralJobExec, EventHeader->Timeout, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

                if (PeripheralAccessResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) BleManagerDeviceAccessWriteTaskExt_Asynch Failed with ResultCode = %d\r\n",
                                     __LINE__,
                                     PeripheralAccessResult);
                }
                else
                {
                    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                         __LINE__,
                                         LedManagerRetCode);
                    }
                }
            }
        }
        break;

        case JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS:
        {
#if DEVICE_IS_HYBRID
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS\r\n");

            HybridPeripheralJobExecHeader_t* PeripheralJobExec = (HybridPeripheralJobExecHeader_t*)JobManagerGetEventObjPayloadData(arg);
            HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
            pCmdMessage->Priority = 0; //Normal
            pCmdMessage->MessageID = 0;
            switch (PeripheralJobExec->CharFlag.bits.ReadWriteMode_b1_3)
            {
                case WRITE_FOLLOW_BYADVERTISEMENT_PAYLOAD_READ:
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST;
                    break;
                case READ_FOLLOW_BYAGGREGATIONUPDATE:
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST;
                    break;
                default: //Default make it a write access only
                    pCmdMessage->CommandMessage = HP_CMD_CONTROL_ACCESS_WRITE_REQUEST;
                    break;
            }

            HpControlAccessReq_t* pControlAccessInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(HpControlAccessReq_t) + PeripheralJobExec->DataLen);

            pControlAccessInfo->SourceOfOrigin = EventHeader->SourceOfOrigin;
            pControlAccessInfo->IdentificationNumber = EventHeader->IdentificationNumber;
            pControlAccessInfo->DataLen = PeripheralJobExec->DataLen;
            memcpy(pControlAccessInfo->ServiceUUID, PeripheralJobExec->ServiceUUID, sizeof(pControlAccessInfo->ServiceUUID));
            memcpy(pControlAccessInfo->CharacteristicUUID, PeripheralJobExec->CharacteristicUUID, sizeof(pControlAccessInfo->CharacteristicUUID));
            memcpy(pControlAccessInfo->pDataPayload, ((uint8_t*)PeripheralJobExec) + sizeof(HybridPeripheralJobExecHeader_t), PeripheralJobExec->DataLen);

            pCmdMessage->PayloadData = (uint8_t*)pControlAccessInfo;
            pCmdMessage->PayloadSize = sizeof(HpControlAccessReq_t) + PeripheralJobExec->DataLen;

            //Send the message to HybridPeripheral
            HpManagerSendTo_Client(pCmdMessage);
            HpManagerCleanEventArgumentObj(pCmdMessage);

#else
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS not supported in pure central mode\r\n",
                             __LINE__);

#endif
        }
        break;

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE
        case JOB_MANAGER_EVENT_PERIPHERAL_STATUS_NEEDS_UPDATE:
        {
            DeviceInfoReport_t* PeripheralDevInfoReport = (DeviceInfoReport_t*)JobManagerGetEventObjPayloadData(arg);
            cUtlHybridPeripheralDeviceInfoUpdate(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, PeripheralDevInfoReport);
        }
        break;
#endif  //#if DEVICE_IS_HYBRID
#endif  //#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE

        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY:
        case JOB_MANAGER_EVENT_PERIPHERAL_DATA_KEEP_ALIVE:
        {

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE
            {
                DeviceInfoReport_t* PeripheralDevInfoReport = (DeviceInfoReport_t*)JobManagerGetEventObjPayloadData(arg);
                cUtlHybridPeripheralDeviceInfoUpdate(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, PeripheralDevInfoReport);
            }
#endif  //#if DEVICE_IS_HYBRID
#endif  //#if HP_BLE_DEVICE_PERIPHERAL_INFO_AUTO_UPDATE

            /*  Send the device info to Udp Manager.. It's up to UDP manager if the information is going to be sent to local network or not
                We send the data either its a keep alive data or data integrity changed*/

            /*
            DeviceInfoReport_t* PeripheralDevInfoReport = (DeviceInfoReport_t*)JobManagerGetEventObjPayloadData(arg);

            // We are in OFFLINE state that means we have network issue. Sending information via Udp may not work.
            UdpCommManager_result_t UdpManagerResult = UdpCommManagerSendDeviceInfo((uint8_t*)PeripheralDevInfoReport, sizeof(DeviceInfoReport_t));

            if ((UdpManagerResult != UDP_COMM_MANAGER_SUCCESS) && (UdpManagerResult != UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING))
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Failed to send device info Code = %d\r\n",
                                 __LINE__,
                                 UdpManagerResult);
            }*/

        }
        break;

        case JOB_MANAGER_EVENT_GATEWAY_IDENTIFY:
        {
            int16_t StatusReport = JOB_SUCCESS;
            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_ONE_TIME_IDENTIFY);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case JOB_MANAGER_EVENT_SOFT_RESTART:
        {
            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_SUCCESS);

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_RESET);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        break;

        case JOB_MANAGER_EVENT_UPDATE_PERIPHERAL_RTC:
        {
            stUpdatePeripheralRtcInfo_t* PeripheralUpdateRTCInfo = (stUpdatePeripheralRtcInfo_t*)JobManagerGetEventObjPayloadData(arg);

            BLEManager_result_t ManagerTaskResult = BleManagerUpdateRTCTask_Asynch(PeripheralUpdateRTCInfo, 3000, EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber);

            if (ManagerTaskResult != DEV_MANAGER_SUCCESS)
            {
                int16_t StatusReport = JOB_FAILED_INTERNAL;
                cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, StatusReport);

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Error sending BleManagerUpdateRTCTask_Asynch to ble manager");
            }
            else
            {
                LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_BUSY_ACCESSING_PERIPHERAL);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode);
                }
            }
        }
        break;

        case JOB_MANAGER_EVENT_SCAN_FOR_PERIPHERAL:
        case JOB_MANAGER_EVENT_SECURITY_SET_RENEW_PERIPHERAL:
        case JOB_MANAGER_EVENT_PAIR_PERIPHERAL:
        case JOB_MANAGER_EVENT_UNPAIR_PERIPHERAL:
        case JOB_MANAGER_EVENT_NOTIFICATION:
        {
            cUtlJobDoneReportSendHandler(EventHeader->SourceOfOrigin, EventHeader->IdentificationNumber, JOB_FAILED_NOT_SUPPORTED);
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Offline_EventServerManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    switch (EventHeader.MessageType)
    {
        case DKMSG_GATEWAYLOGIN:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                //Switch sub state machine
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_SERVER_CONNECTED);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_SERVER_CONNECTED (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
            else
            {
                //TODO: Depending on the error code, we may need to handle it
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) unable to login to server (Report=%d)\r\n",
                                 __LINE__,
                                 EventHeader.s.ReportStatusCode);
            }
        }
        break;

        case DKMSG_STATE_CHANGED:
        {

            //NOTE: DKSERVERMANAGER_STATEMACHINE_STATE_READY to login will be handled by the timed state SubState_OfflineLogInToServer
            if (EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER/* ||
                     EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_SESSIONTIMEOUT*/)
            {
                // Suspend & Resume DKServerManager
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) switch to OFFLINE_SUBSTATE_SERVER_LOGIN\r\n",
                                 __LINE__);

                DKServerManager_State_Suspend();

                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_SERVER_LOGIN);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_SERVER_LOGIN (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }

            if (EventHeader.s.StateMachineState != DKSERVERMANAGER_STATEMACHINE_STATE_READY) {
                LedManager_result_t LedManagerRetCode = SetLedCondition (LED_CONDITION_DISCONNECTED_BY_DK_SERVER);
                if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                     __LINE__,
                                     LedManagerRetCode);
                }
            }

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "DKMSG_STATE_CHANGED (%d)\r\n", EventHeader.s.StateMachineState);
        }
        break;

        case DKMSG_SYSTEM_ERROR:
        {
            uint16_t SystemErrorCode = EventHeader.s.SystemErrorCode;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "DKMSG_SYSTEM_ERROR (%d)\r\n", SystemErrorCode);

            switch (SystemErrorCode)
            {
                case DKSERVER_SYSTEM_ERROR_DX_RTOS:                 // 102
                    // An ssl_init() == -32512 (-0x7F00) will receive the error code
                    //
                    // Retry can't fix issue. Should REBOOT hardware
                    break;
                case DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY:            // 103
                case DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL:        // 104
                case DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER: // 105
                case DKSERVER_SYSTEM_ERROR_NETWORK_TIMEOUT:         // 106
                    break;
                default:
                    break;
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t Offline_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Offline_EventHpManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_CONFIG_WRITE_REQUEST:
        {
            G_SYS_DBG_LOG_V("Offline HP_CMD_CONFIG_WRITE_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                dxDEV_INFO_RET_CODE DevInfoRet;
                HpConfigWrite_t* pConfigWrite = (HpConfigWrite_t*)pHpCmdMessage->PayloadData;

                switch(pConfigWrite->ConfigId)
                {
                    default:
                        G_SYS_DBG_LOG_V("Unknown 0x%02x Config ID received! \r\n", pConfigWrite->ConfigId);
                    break;
                }
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ:
        {
            G_SYS_DBG_LOG_V(  "Offline HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpDataExcHistoryEventDetailData_t* pHpAdvDataReport = (HpDataExcHistoryEventDetailData_t*)pHpCmdMessage->PayloadData;
                HpEventDataPayloadPackage_t DataPayloadPackage = cUtlHybridPeripheralHistoryEventDataPayloadPackage(JOB_TYPE_HYBRIDPERIPHERAL,
                                                                                                                    (uint64_t)pHpCmdMessage->MessageID,
                                                                                                                    pHpAdvDataReport->PayloadData,
                                                                                                                    pHpAdvDataReport->PayloadDataSize);

                JobManagerHybridPeripheralDataUpdate_Task(  &DataPayloadPackage.DeviceInfoReport,
                                                            DataPayloadPackage.SourceOfOrigin,
                                                            DataPayloadPackage.IdentificationNumber);
            }
        }
        break;

        case HP_CMD_DATAEXCHANGE_GET_DEVICE_GENERIC_INFO_REQ : {
            cUtlHybridPeripheralGetDeviceGenericInfo (pHpCmdMessage->MessageID, pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_JOBBRIDGE_JOB_SUPERIOR_ONLY_CONFIG:
        {
            G_SYS_DBG_LOG_V(  "Offline HP_CMD_JOBBRIDGE_JOB_SUPERIOR_ONLY_CONFIG received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobSuperiorOnly_t* pSuperiodOnly = (HpJobSuperiorOnly_t*)pHpCmdMessage->PayloadData;

                JobManagerSetJobSuperiorOnly(pSuperiodOnly->bJobSuperiorOnly);
            }
        }
        break;

        case HP_CMD_ENTER_FACTORY_RESET_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "Offline HP_CMD_ENTER_FACTORY_RESET_REQUEST received! \r\n" );

            //We are in offline mode, normally we should delete the device from cloud first but due to
            //certain circumstances such as unable to connect to wifi or system is stuck in certain operation
            //and recovery is not possible then the only option is to delete all info and re-pair again
            //provided that the system is already removed from the cloud list!
            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
            if (DevInfoRet != DXDEV_INFO_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                 __LINE__, DevInfoRet);
            }

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_RESET);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_RESET (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_GET_STATUS_REQ : {
            cUtlHybridPeripheralGetConcurrentPeripheralStatus ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_CUSTOMIZE_BD_ADDRESS_REQ : {
            cUtlHybridPeripheralCustomizeConcurrentPeripheralBdAddress ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_SECURITY_KEY_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralSecurityKey ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_ADVERTISE_DATA_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralAdvertiseData (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_SET_SCAN_RESPONSE_REQ : {
            cUtlHybridPeripheralSetConcurrentPeripheralScanResponse ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_ADD_SERVICE_CHARS_REQ : {
            cUtlHybridPeripheralAddConcurrentPeripheralServiceCharacteristic (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_ENABLE_SYSTEM_UTILS_REQ : {
            cUtlHybridPeripheralEnableConcurrentPeripheralSystemUtils (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_START_REQ : {
            cUtlHybridPeripheralStartConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_STOP_REQ : {
            cUtlHybridPeripheralStopConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_DISCONNECT_REQ : {
            cUtlHybridPeripheralDisconnectConcurrentPeripheral ();
        }
        break;

        case HP_CMD_CONCURRENT_PERIPHERAL_DATA_OUT_REQ : {
            cUtlHybridPeripheralTransmitConcurrentPeripheralData (pHpCmdMessage->PayloadSize, pHpCmdMessage->PayloadData);
        }

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }
        }
        break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Offline_EventWifiManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case WIFI_COMM_EVENT_COMMUNICATION:
        {
            switch (EventHeader.ResultState)
            {
                case WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID:
                case WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED:
                case WIFI_COMM_MANAGER_WRONG_PASSWORD:
                case WIFI_COMM_MANAGER_PASSWORD_REQUIRED:
                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED_BUT_UNABLE_TO_REACH_WWW:
                {
                    //We dont do anything here, let the sub state handles it. Getting here is most likely
                    //user made some setting changes in their AP since last wifi setup.
                    //TODO: maybe we can have some mechanism to provide data feedback to the sub state machine for other handling
                    //      or maybe its not needed and we handles it here.
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) NOTE: Unable to connect to Wifi (ResultState = %d)\r\n",
                                     __LINE__,
                                     EventHeader.ResultState);

                    // Switch sub state to reconnect Wifi!
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                         __LINE__,
                                         SmpResult);
                    }
                }
                break;

                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED:
                {
                    dxIP_Addr_t ConnectedIpAddress;

                    if (WifiCommManagerGetIpAddress(&ConnectedIpAddress) == WIFI_COMM_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Connected Local Addr is at %u.%u.%u.%u\r\n",
                                         (uint8_t)ConnectedIpAddress.v4[0],
                                         (uint8_t)ConnectedIpAddress.v4[1],
                                         (uint8_t)ConnectedIpAddress.v4[2],
                                         (uint8_t)ConnectedIpAddress.v4[3]);

                        //UdpCommManagerStart();        // Move to SubState_OfflineWifiIsConnected

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "DKServerManager_GetCurrentState():%d\r\n", DKServerManager_GetCurrentState());

                        if (DKServerManager_GetCurrentState() != DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "FATAL:(Line=%d) DKServerManager_GetCurrentState():%d (SHOULD be %d)\r\n",
                                             __LINE__,
                                             DKServerManager_GetCurrentState(),
                                             DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED);

                            DKServerManager_State_ForceToSuspended();

                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "FATAL:(Line=%d) DKServerManager_GetCurrentState():%d (SHOULD be %d)\r\n",
                                             __LINE__,
                                             DKServerManager_GetCurrentState(),
                                             DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED);
                        }

                        DKServerManager_State_Resume();

                        //Switch sub state to Wifi Connected!
                        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_IS_CONNECTED);
                        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) SMP unable to switch to OFFLINE_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                                             __LINE__,
                                             SmpResult);
                        }

                    }
                    else
                    {
                        //Unable to obtain IP address, do nothing and let the sub state machine handles it automatically
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) Wifi connected but unable to obtain IP\r\n",
                                         __LINE__);
                    }
                }
                break;

                case WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER:
                case WIFI_COMM_MANAGER_CONNECTION_IS_DOWN:
                {
                    //Stop and suspend managers, let the sub state machine handles it automatically
                    UdpCommManagerStop();
                    DKServerManager_State_Suspend();

                }
                break;

                default:
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) Unhandled Wifi ResultState = %d\r\n",
                                     __LINE__,
                                     EventHeader.ResultState);
                }
                break;
            }

        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Offline_EventUdpManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    if (EventHeader)
    {
        switch (EventHeader->PacketType)
        {
            case UDP_COMM_EVENT_NEW_JOB:
            {
                char* JobPayload = UdpCommManagerGetEventObjPayloadData(arg);

                if (JobPayload)
                {
                    uint64_t IdentificationNumber = (((uint64_t)EventHeader->DeviceIdentificationNumber << 16) | ((uint64_t)EventHeader->PayLoadIdentificationNumber));
                    JobManagerNewJob_Task(JobPayload, EventHeader->PayloadLen, SOURCE_OF_ORIGIN_UDP_JOB, IdentificationNumber);
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) No Job Payload found from UDP_COMM_EVENT_NEW_JOB\r\n",
                                     __LINE__);
                }
            }
            break;

            default:
            {
                static dxBOOL bReported = dxFALSE;
                if (bReported == dxFALSE)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                    "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType);
                    bReported = dxTRUE;
                }
            }
            break;
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Offline_EventKeyPadManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE;

    //NOTE: We are returning STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE so that it is handled by the default state machine

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Offline_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

static dxBOOL bInitialized;

StateMachine_result_t Offline_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    bInitialized = dxFALSE;

    //We shut down BLE manager as we are about to restore peripheral
    //which we will update the device to BLE manager, we don't want BLE manager
    //scanning peripheral while updating the device list.
    BleManagerScanReportSuspend();

    //Restore all device information for offline mode
    //TODO: IMPORTANT
    //      1)Check if each time we call this function whenever we switch from server online
    //        to offline will cause any issues (eg, schedule and adv job will be reset/replaced which
    //        can cause re-triggering).
    //      2)See if we can simply call this when we come back from Idle or Setup, but make sure we cover all cases
    CentralOfflineSupportDeviceInfoRestore();

    LedManager_result_t LedManagerRetCode = SetLedModeCondition (LED_OPERATION_MODE_NORMAL, LED_CONDITION_DISCONNECTED_BY_DK_SERVER);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedMode (ret error %d)\r\n",
                         __LINE__, LedManagerRetCode);
    }

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
    if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
    }
#endif
#endif  //#if DEVICE_IS_HYBRID

    //RESUME BLEManager SCAN report by switching BLE Manager back to central role (we need to switch to central role just in case it was from previous role
    //before)
    BleManagerSetRoleToCentral();

    OfflineSubStateHandler = SimpleStateMachine_Init(OfflineSubStateProcessFuncTable);
    SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_IDLE);

    return result;
}

StateMachine_result_t Offline_MainProcess(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (bInitialized == dxFALSE)
    {
        BLEManager_WhiteList_AddFromKeeperList();
        BleRepeater_UpdateWatchList ();
        BleManagerScanReportResume ();
        bInitialized = dxTRUE;
    }

    //Kick start the sub state if its IDLE
    if (SimpleStateMachine_GetCurrentState(OfflineSubStateHandler) == OFFLINE_SUBSTATE_IDLE)
    {
        SimpleStateMachine_ExecuteState(OfflineSubStateHandler, OFFLINE_SUBSTATE_WIFI_CONNECTING);
    }

    //Process the sub state machine
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_Process(OfflineSubStateHandler);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_OFFLINE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SimpleStateMachine_Process failed (%d)\r\n",
                         __LINE__,
                         SmpResult);
    }

    return result;
}

StateMachine_result_t Offline_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (OfflineSubStateHandler) {
        SimpleStateMachine_Uninit(OfflineSubStateHandler);
        OfflineSubStateHandler = NULL;
    }

    return result;
}


