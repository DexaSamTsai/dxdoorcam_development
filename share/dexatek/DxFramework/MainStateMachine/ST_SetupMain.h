//============================================================================
// File: ST_SetupMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_SETUP_MAIN_H
#define _ST_SETUP_MAIN_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t Setup_Init(void* data );
StateMachine_result_t Setup_MainProcess(void* data );
StateMachine_result_t Setup_End(void* data );
StateMachineEvent_State_t Setup_EventBLEManager(void* data );
StateMachineEvent_State_t Setup_EventServerManager(void* data );
StateMachineEvent_State_t Setup_EventMqttManager(void* data );
#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Setup_EventHpManager(void* arg);
#endif //#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Setup_EventWifiManager(void* data );
StateMachineEvent_State_t Setup_EventUdpManager(void* data );
StateMachineEvent_State_t Setup_EventJobManager(void* data );
StateMachineEvent_State_t Setup_EventKeyPadManager(void* data );
StateMachineEvent_State_t Setup_EventLEDManager(void* data );


#endif // _ST_SETUP_MAIN_H
