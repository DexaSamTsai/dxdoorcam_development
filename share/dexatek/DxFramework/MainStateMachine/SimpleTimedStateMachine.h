//============================================================================
// File: SimpleTimedStateMachine.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/02/01
//     1) Description: Initial
//
//============================================================================
#ifndef _SIMPLESTATEMACHINE_H
#define _SIMPLESTATEMACHINE_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

#define SIMPLE_STATE_INVALID        0xFFFF

//============================================================================
// Enumeration
//============================================================================

typedef enum
{

    SMP_STATE_MACHINE_RET_SUCCESS                           = 0,
    SMP_STATE_MACHINE_RET_NOT_ALLOWED_ST_CHANGE_INPROCESS,
    SMP_STATE_MACHINE_RET_NOT_SUPPORTED,

    SMP_STATE_MACHINE_RET_ERROR                             = -1,
    SMP_STATE_MACHINE_RET_STATE_CODE_NOT_VALID              = -2,

} SimpleStateMachine_result_t;

/******************************************************
 *                    Structures
 ******************************************************/

typedef SimpleStateMachine_result_t (*simple_state_func_t)( void* data, dxBOOL isFirstCall);

typedef struct
{
    uint16_t                StateCode;
    simple_state_func_t     StateFunc;
    dxTime_t                StateMaxTimeout;

} simple_st_machine_table_t;

//============================================================================
// SAMPLE State Machine Config Table
//============================================================================

#if SAMPLE_CONFIG_TABLE

typedef enum
{
    STATE_UNKNOWN           = 0,
    STATE_IDLE,
    STATE_WIFI_CONNECTING,
    STATE_WIFI_CONNECTED,
    STATE_SERVER_CONNECTING,
    STATE_SERVER_CONNECTED,

} Sample_State_Code_t;

static simple_st_machine_table_t Sample_SmpStateMachineProcessFuncTable[] =
{
    {STATE_IDLE, SimpleIdleFunc, 0},
    {STATE_WIFI_CONNECTING, SimpleWifiConnecting, 5*SECONDS},       //Call SimpleWifiConnecting every 5 seconds if remain in the same state
    {STATE_WIFI_CONNECTED, SimpleWifiConnected, 0},
    {STATE_SERVER_CONNECTING, SimpleServerConnecting, 5*SECONDS},   //Call SimpleWifiConnecting every 5 seconds if remain in the same state
    {STATE_SERVER_CONNECTED, SimpleServerConnected, 0},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};

#endif

//============================================================================
// Type Defines
//============================================================================

typedef void* SimpleStateHandler;

//============================================================================
// Function
//============================================================================

/** Initialization of simple state machine given the table, please check Sample_SmpStateMachineProcessFuncTable as a sample creating the table
 *
 * @param[in]  SimpleStateTableTable 	 : 	The table that contain the state information
 *
 * @return SimpleStateHandler  : The handler of simple state machine.
 */
SimpleStateHandler SimpleStateMachine_Init(simple_st_machine_table_t* pSimpleStateTableTable);


/** This function should be called from the loop thread once a while (eg every 100 millisecond or so)
 *  to allow state handlings.
 *
 * @return SimpleStateMachine_result_t
 */
SimpleStateMachine_result_t SimpleStateMachine_Process(SimpleStateHandler handler);


/** Change/Execute state machine, NOTE: If StateCodeToChange is same as current state the state function will not execute again
 *
 * @param[in]  StateCodeToChange 	 : 	  The state to change to, make sure the state code is at least a valid value from the given table
 *                                      during the initialization.
 *
 * @return SimpleStateMachine_result_t
 */
SimpleStateMachine_result_t SimpleStateMachine_ExecuteState(SimpleStateHandler handler, uint16_t StateCodeToChange);


/** Get current state of state machine
 *
 *
 * @return uint16_t The state code of current state
 */
uint16_t SimpleStateMachine_GetCurrentState(SimpleStateHandler handler);


/** Get next state of state machine
 *
 *
 * @return uint16_t The state code of current state
 */
uint16_t SimpleStateMachine_GetNextState(SimpleStateHandler handler);


/** Check if state machine is not about to change while it is equal to state
 *
 * @param[in]  EqualToStateCode 	 : 	  The state code to check if its equal to current state
 *
 * @return uint16_t The state code of current state
 */
dxBOOL SimpleStateMachine_StateNotChangingWhileEqualTo(SimpleStateHandler handler, uint16_t EqualToStateCode);

/** Uninitialize the state machine
 *
 *
 * @return SimpleStateMachine_result_t
 */
SimpleStateMachine_result_t SimpleStateMachine_Uninit(SimpleStateHandler handler);

#endif // _SIMPLESTATEMACHINE_H
