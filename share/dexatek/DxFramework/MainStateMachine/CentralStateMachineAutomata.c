//============================================================================
// File: CentralStateMachineAutomata.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#include "HpJobBridge.h"
#include "HpContStore.h"
#endif  //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "ST_CommonUtils.h"
#include "CentralStateMachineAutomata.h"


//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//Key button
#define MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS 3

//============================================================================
// Enum
//============================================================================

typedef enum
{
    STATE_COND_UNKNOWN           = 0,
    STATE_COND_INIT,
    STATE_COND_MAIN_PROCESS,
    STATE_COND_EXIT,

} ST_Condition_t;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

State_Code_t        CurrentState = STATE_UNKNOWN;
State_Code_t        NextState = STATE_UNKNOWN;
void*               pNextStateInitData = NULL;

extern uint32_t SystemPairingTimout;


//============================================================================
// State Machine Config Table
//============================================================================

#include "ST_SetupMain.h"
#include "ST_IdleMain.h"
#include "ST_SetupMain.h"
#include "ST_FwUpdateMain.h"
#include "ST_ServerOnlineMain.h"
#include "ST_OfflineMain.h"
#include "ST_FatalMain.h"
#include "ST_RegisterMain.h"

static state_machine_table_t StateMachineProcessFuncTable[] =
{
    {STATE_IDLE, Idle_Init, Idle_MainProcess, Idle_End},
    {STATE_SETUP, Setup_Init, Setup_MainProcess, Setup_End},
    {STATE_FW_UPDATE, FwUpdate_Init, FwUpdate_MainProcess, FwUpdate_End},
    {STATE_SERVER_ONLINE, ServerOnline_Init, ServerOnline_MainProcess, ServerOnline_End},
    {STATE_OFFLINE, Offline_Init, Offline_MainProcess, Offline_End},
    {STATE_REGISTER, Register_Init, Register_MainProcess, Register_End},
    {STATE_FATAL, Fatal_Init, Fatal_MainProcess, Fatal_End}
};

static state_machine_event_table_t StateMachineEventFuncTable[] =
{
    {STATE_IDLE, STATE_EVENT_BLE, Idle_EventBLEManager, NULL},
    {STATE_IDLE, STATE_EVENT_SERVER, Idle_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_IDLE, STATE_EVENT_MQTT, Idle_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_IDLE, STATE_EVENT_HP, Idle_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_IDLE, STATE_EVENT_WIFI, Idle_EventWifiManager, NULL},
    {STATE_IDLE, STATE_EVENT_UDP, Idle_EventUdpManager, NULL},
    {STATE_IDLE, STATE_EVENT_JOB, Idle_EventJobManager, NULL},
    {STATE_IDLE, STATE_EVENT_KEYPAD, Idle_EventKeyPadManager, NULL},
    {STATE_IDLE, STATE_EVENT_LED, Idle_EventLEDManager, NULL},

    {STATE_SETUP, STATE_EVENT_BLE, Setup_EventBLEManager, NULL},
    {STATE_SETUP, STATE_EVENT_SERVER, Setup_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_SETUP, STATE_EVENT_MQTT, Setup_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_SETUP, STATE_EVENT_HP, Setup_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_SETUP, STATE_EVENT_WIFI, Setup_EventWifiManager, NULL},
    {STATE_SETUP, STATE_EVENT_UDP, Setup_EventUdpManager, NULL},
    {STATE_SETUP, STATE_EVENT_JOB, Setup_EventJobManager, NULL},
    {STATE_SETUP, STATE_EVENT_KEYPAD, Setup_EventKeyPadManager, NULL},
    {STATE_SETUP, STATE_EVENT_LED, Setup_EventLEDManager, NULL},

    {STATE_FW_UPDATE, STATE_EVENT_BLE, FwUpdate_EventBLEManager, NULL},
    {STATE_FW_UPDATE, STATE_EVENT_SERVER, FwUpdate_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_FW_UPDATE, STATE_EVENT_MQTT, FwUpdate_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_FW_UPDATE, STATE_EVENT_HP, FwUpdate_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_FW_UPDATE, STATE_EVENT_WIFI, FwUpdate_EventWifiManager, NULL},
    {STATE_FW_UPDATE, STATE_EVENT_UDP, FwUpdate_EventUdpManager, NULL},
    {STATE_FW_UPDATE, STATE_EVENT_JOB, FwUpdate_EventJobManager, NULL},
    {STATE_FW_UPDATE, STATE_EVENT_KEYPAD, FwUpdate_EventKeyPadManager, NULL},
    {STATE_FW_UPDATE, STATE_EVENT_LED, FwUpdate_EventLEDManager, NULL},

    {STATE_SERVER_ONLINE, STATE_EVENT_BLE, SrvOnline_EventBLEManager, NULL},
    {STATE_SERVER_ONLINE, STATE_EVENT_SERVER, SrvOnline_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_SERVER_ONLINE, STATE_EVENT_MQTT, SrvOnline_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_SERVER_ONLINE, STATE_EVENT_HP, SrvOnline_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_SERVER_ONLINE, STATE_EVENT_WIFI, SrvOnline_EventWifiManager, NULL},
    {STATE_SERVER_ONLINE, STATE_EVENT_UDP, SrvOnline_EventUdpManager, NULL},
    {STATE_SERVER_ONLINE, STATE_EVENT_JOB, SrvOnline_EventJobManager, NULL},
    {STATE_SERVER_ONLINE, STATE_EVENT_KEYPAD, SrvOnline_EventKeyPadManager, NULL},
    {STATE_SERVER_ONLINE, STATE_EVENT_LED, SrvOnline_EventLEDManager, NULL},

    {STATE_OFFLINE, STATE_EVENT_BLE, Offline_EventBLEManager, NULL},
    {STATE_OFFLINE, STATE_EVENT_SERVER, Offline_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_OFFLINE, STATE_EVENT_MQTT, Offline_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_OFFLINE, STATE_EVENT_HP, Offline_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_OFFLINE, STATE_EVENT_WIFI, Offline_EventWifiManager, NULL},
    {STATE_OFFLINE, STATE_EVENT_UDP, Offline_EventUdpManager, NULL},
    {STATE_OFFLINE, STATE_EVENT_JOB, Offline_EventJobManager, NULL},
    {STATE_OFFLINE, STATE_EVENT_KEYPAD, Offline_EventKeyPadManager, NULL},
    {STATE_OFFLINE, STATE_EVENT_LED, Offline_EventLEDManager, NULL},

	{STATE_REGISTER, STATE_EVENT_BLE, Register_EventBLEManager, NULL},
    {STATE_REGISTER, STATE_EVENT_SERVER, Register_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_REGISTER, STATE_EVENT_MQTT, Register_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_REGISTER, STATE_EVENT_HP, Register_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_REGISTER, STATE_EVENT_WIFI, Register_EventWifiManager, NULL},
    {STATE_REGISTER, STATE_EVENT_UDP, Register_EventUdpManager, NULL},
    {STATE_REGISTER, STATE_EVENT_JOB, Register_EventJobManager, NULL},
    {STATE_REGISTER, STATE_EVENT_KEYPAD, Register_EventKeyPadManager, NULL},
    {STATE_REGISTER, STATE_EVENT_LED, Register_EventLEDManager, NULL},

    {STATE_FATAL, STATE_EVENT_BLE, Fatal_EventBLEManager, NULL},
    {STATE_FATAL, STATE_EVENT_SERVER, Fatal_EventServerManager, NULL},
#if MQTT_IS_ENABLED
    {STATE_FATAL, STATE_EVENT_MQTT, Fatal_EventMqttManager, NULL},
#endif
#if DEVICE_IS_HYBRID
    {STATE_FATAL, STATE_EVENT_HP, Fatal_EventHpManager, NULL},
#endif //#if DEVICE_IS_HYBRID
    {STATE_FATAL, STATE_EVENT_WIFI, Fatal_EventWifiManager, NULL},
    {STATE_FATAL, STATE_EVENT_UDP, Fatal_EventUdpManager, NULL},
    {STATE_FATAL, STATE_EVENT_JOB, Fatal_EventJobManager, NULL},
    {STATE_FATAL, STATE_EVENT_KEYPAD, Fatal_EventKeyPadManager, NULL},
    {STATE_FATAL, STATE_EVENT_LED, Fatal_EventLEDManager, NULL}

};

//============================================================================
// Internal Function
//============================================================================

#if DEVICE_IS_HYBRID
int16_t IntConvertHybridPeripheralControlAccessResultStateToJobDoneReportStatus(int32_t HybridPeripheralControlAccessResultToConvert) //eHPControlAccessStatus
{
    int16_t StatusReport = JOB_SUCCESS;

    switch (HybridPeripheralControlAccessResultToConvert)
    {
        case HP_CONTROL_ACCESS_SUCCESS:
            StatusReport = JOB_SUCCESS;
            break;

        case HP_CONTROL_ACCESS_GENERIC_ERROR:
            StatusReport = JOB_FAILED_GENERIC;
            break;

        case HP_CONTROL_ACCESS_INVALID_PARAM:
            StatusReport = JOB_FAILED_INVALID_PARAM;
            break;

        case HP_CONTROL_ACCESS_NOT_SUPPORTED:
            StatusReport = JOB_FAILED_NOT_SUPPORTED;
            break;

        case HP_CONTROL_ACCESS_TIME_OUT:
            StatusReport = JOB_FAILED_TIME_OUT;
            break;

        default:
            StatusReport = JOB_FAILED_GENERIC;
            break;
    }

    return StatusReport;
}


uint32_t IntStStateToHpStatusReportTranslation(State_Code_t StateToProcess, ST_Condition_t CondProcess)
{
    uint32_t HpFrwStatusReport = 0;
    if ((CondProcess == STATE_COND_INIT) || (CondProcess == STATE_COND_EXIT))
    {
        switch (StateToProcess)
        {
            case STATE_IDLE:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING;
                break;
            case STATE_SETUP:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING;
                break;
            case STATE_FW_UPDATE:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING;
                break;
            case STATE_SERVER_ONLINE:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING;
                break;
            case STATE_OFFLINE:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING;
                break;
    		case STATE_REGISTER:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_ENTERING;
                break;
            case STATE_FATAL:
                HpFrwStatusReport = HP_CMD_FRW_STATUS_REPORT_ST_FATAL_ENTERING;
                break;

            default:
                break;
        }

        if ((HpFrwStatusReport) && (CondProcess == STATE_COND_EXIT))
            HpFrwStatusReport++;
    }

    return HpFrwStatusReport;
}

#endif

StateMachine_result_t IntProcessStateFunc(State_Code_t StateToProcess, ST_Condition_t CondProcess, void* pData)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    static ST_Condition_t KnownCond = STATE_COND_UNKNOWN;

    if (StateToProcess != STATE_UNKNOWN)
    {
        uint16_t i = 0;
        for (i = 0; i < (sizeof(StateMachineProcessFuncTable)/sizeof(state_machine_table_t)); i++)
        {
            if (StateMachineProcessFuncTable[i].State == StateToProcess)
            {
                state_func_t funcToProcess = NULL;

                switch (CondProcess)
                {
                    case STATE_COND_INIT:
                    {
                        if (KnownCond != CondProcess) //Just Print Once
                        {
                            G_SYS_DBG_LOG_V("Execute StateProcess Init\r\n");
                            KnownCond = CondProcess;
                        }
                        funcToProcess = StateMachineProcessFuncTable[i].StateInit;
                    }
                    break;

                    case STATE_COND_MAIN_PROCESS:
                    {
                        if (KnownCond != CondProcess) //Just Print Once
                        {
                            G_SYS_DBG_LOG_V("Execute StateProcess Main\r\n");
                            KnownCond = CondProcess;
                        }
                        funcToProcess = StateMachineProcessFuncTable[i].StateMainProcess;
                    }
                    break;

                    case STATE_COND_EXIT:
                    {
                        if (KnownCond != CondProcess) //Just Print Once
                        {
                            G_SYS_DBG_LOG_V("Execute StateProcess Exit\r\n");
                            KnownCond = CondProcess;
                        }
                        funcToProcess = StateMachineProcessFuncTable[i].StateEnd;
                    }
                    break;

                    default:
                    {
                        G_SYS_DBG_LOG_V("WARNING: CondProcess not supported\r\n");
                    }
                        break;
                }

                if (funcToProcess)
                {

#if DEVICE_IS_HYBRID
                    uint32_t HpFrwStatusReport = IntStStateToHpStatusReportTranslation(StateToProcess, CondProcess);
                    if (HpFrwStatusReport)
                    {
                        HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                        memset(pCmdMessage, 0, sizeof(HpCmdMessageQueue_t));
                        pCmdMessage->CommandMessage = HpFrwStatusReport;
                        HpManagerSendTo_Client(pCmdMessage);
                        HpManagerCleanEventArgumentObj(pCmdMessage);
                    }
#endif // DEVICE_IS_HYBRID

                    result = funcToProcess(pData);
                }

                break;
            }
        }
    }

    return result;
}

void IntCentralProtectReleaseAllResourceOfState(State_Code_t StateResourceToProtect, dxBOOL isResourceProtect)
{
    if (CurrentState != STATE_UNKNOWN)
    {
        uint16_t i = 0;
        for (i = 0; i < (sizeof(StateMachineEventFuncTable)/sizeof(state_machine_event_table_t)); i++)
        {
            if (StateMachineEventFuncTable[i].EventForState == StateResourceToProtect)
            {
                if (StateMachineEventFuncTable[i].EventResourceProtection)
                {
                    if (isResourceProtect) //Get Resource
                    {
                        dxSemTake(StateMachineEventFuncTable[i].EventResourceProtection, DXOS_WAIT_FOREVER);
                    }
                    else //Put Resource
                    {
                        dxSemGive(StateMachineEventFuncTable[i].EventResourceProtection);
                    }
                }
            }
        }
    }
}

void IntCentralProtectAllResourceOfState(State_Code_t StateResourceToProtect)
{
    IntCentralProtectReleaseAllResourceOfState(StateResourceToProtect, dxTRUE);
}

void IntCentralReleaseAllResourceOfState(State_Code_t StateResourceToRelease)
{
    IntCentralProtectReleaseAllResourceOfState(StateResourceToRelease, dxFALSE);
}

state_event_func_t IntCentralGetEvenFuncTableOFCurrentMode(State_Event_t EvenFuncToGet)
{
    state_event_func_t EventFunc = NULL;

    if (CurrentState != STATE_UNKNOWN)
    {
        uint16_t i = 0;
        for (i = 0; i < (sizeof(StateMachineEventFuncTable)/sizeof(state_machine_event_table_t)); i++)
        {
            if ((StateMachineEventFuncTable[i].EventForState == CurrentState) &&
                (StateMachineEventFuncTable[i].Event == EvenFuncToGet))
            {
                EventFunc = StateMachineEventFuncTable[i].pFuncEvent;
                break;
            }
        }
    }

    return EventFunc;
}

void IntCentralGetOrPutEventResourceOfGivenMode(State_Code_t StateMode, State_Event_t EvenResourceToGet, dxBOOL isResourceGet)
{

    if (StateMode != STATE_UNKNOWN)
    {
        uint16_t i = 0;
        for (i = 0; i < (sizeof(StateMachineEventFuncTable)/sizeof(state_machine_event_table_t)); i++)
        {
            if ((StateMachineEventFuncTable[i].EventForState == StateMode) &&
                (StateMachineEventFuncTable[i].Event == EvenResourceToGet))
            {
                if (StateMachineEventFuncTable[i].EventResourceProtection)
                {
                    if (isResourceGet) //Get Resource
                    {
                        dxSemTake(StateMachineEventFuncTable[i].EventResourceProtection, DXOS_WAIT_FOREVER);
                    }
                    else //Put Resource
                    {
                        dxSemGive(StateMachineEventFuncTable[i].EventResourceProtection);
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V("WARNING:Resource Protection for event (S=%d,E=%d) not available\r\n", StateMode, EvenResourceToGet);
                }

                break;
            }
        }
    }

    return;
}

void IntCentralGet_EventResourceOfGivenMode(State_Code_t StateMode, State_Event_t EvenResourceToGet)
{
    IntCentralGetOrPutEventResourceOfGivenMode(StateMode, EvenResourceToGet, dxTRUE);
}

void IntCentralPut_EventResourceOfGivenMode(State_Code_t StateMode, State_Event_t EvenResourceToPut)
{
    IntCentralGetOrPutEventResourceOfGivenMode(StateMode, EvenResourceToPut, dxFALSE);
}

//============================================================================
// Event Function
//============================================================================

void MainEvent_BleManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_BLE);

    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_BLEMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,"%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);
    EventReportObj_t* EventReportObj = (EventReportObj_t*) arg;
    dxBOOL alreadyhandledByDefaultEvent = dxTRUE;

    switch (EventHeader.ReportEvent) {

        case DEV_MANAGER_EVENT_BLE_RESET_REPORT : {
#if DEVICE_IS_HYBRID
            eHpBleResetGenericStatus* resetGenericStatus = dxMemAlloc (
                "BleResetStatus",
                1,
                sizeof (eHpBleResetGenericStatus)
            );
            BleResetStatus* resetStatus = (BleResetStatus*) EventReportObj->pData;

            switch (*resetStatus) {
                case DEV_MANAGER_RESET_TO_CENTRAL    : *resetGenericStatus = HP_BLE_RESET_TO_CENTRAL;    break;
                case DEV_MANAGER_RESET_TO_PERIPHERAL : *resetGenericStatus = HP_BLE_RESET_TO_PERIPHERAL; break;
                case DEV_MANAGER_RESET_TO_UNKNOWN    : *resetGenericStatus = HP_BLE_RESET_TO_UNKNOWN;    break;
                default : *resetGenericStatus = HP_BLE_RESET_NUM; break;
            }

            HpCmdMessageQueue_t* message = dxMemAlloc (
                "HpCmdMessageQueue_t",
                1,
                sizeof (HpCmdMessageQueue_t)
            );

            message->Priority = 0; //Normal
            message->MessageID = 0;
            message->CommandMessage = HP_CMD_FRW_STATUS_REPORT_BLE_RESET;
            message->PayloadData = (uint8_t*) resetGenericStatus;
            message->PayloadSize = sizeof (eHpBleResetGenericStatus);

            //Send the message to HybridPeripheral
            HpManagerSendTo_Client (message);
            HpManagerCleanEventArgumentObj (message);
#endif
        }
        break;

        default : {
            alreadyhandledByDefaultEvent = dxFALSE;
        }
        break;
    }

    if (alreadyhandledByDefaultEvent == dxFALSE) {

        state_event_func_t BleEventFunc = IntCentralGetEvenFuncTableOFCurrentMode (STATE_EVENT_BLE);

        if (BleEventFunc) {
            EventStateRet = BleEventFunc (arg);
        } else {
            G_SYS_DBG_LOG_V("WARNING: BleEventFunc not implemented\r\n");
        }

        if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE) {
            switch (EventHeader.ReportEvent) {
                default : {
                    G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_BleManagerEventReportHandler (%d)\r\n", EventHeader.ReportEvent );
                }
                break;
            }
        }
    }

    //Always clean the arg object from event
    BleManagerCleanEventArgumentObj (arg);

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode (OrgCurrentState, STATE_EVENT_BLE);

}


void MainEvent_JobManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_JOB);

    //G_SYS_DBG_LOG_V("MainEvent_JobManagerEventReportHandler IN\r\n");

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader = JobManagerGetEventObjHeader(arg);

    state_event_func_t JobEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_JOB);

    if (JobEventFunc)
    {
        EventStateRet = JobEventFunc(arg);
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: JobEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
    {
        switch (EventHeader->EventType)
        {
            default:
            {
                G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_JobManagerEventReportHandler (%d)\r\n", EventHeader->EventType );
            }
                break;
        }
    }

    //Always clean the arg object from event
    JobManagerCleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_JobManagerEventReportHandler OUT\r\n");

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_JOB);

}

void MainEvent_ServerManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_SERVER);

    //G_SYS_DBG_LOG_V("MainEvent_ServerManagerEventReportHandler IN\r\n");

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    state_event_func_t ServerEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_SERVER);

    if (ServerEventFunc)
    {
        EventStateRet = ServerEventFunc(arg);
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: ServerEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
    {
        switch (EventHeader.MessageType)
        {
            default:
            {
                G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_ServerManagerEventReportHandler (%d)\r\n", EventHeader.MessageType );
            }
                break;
        }
    }

    //Always clean the arg object from event
    DKServerManager_CleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_ServerManagerEventReportHandler OUT\r\n");

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_SERVER);

}


#if MQTT_IS_ENABLED
void MainEvent_MqttManagerEventReportHandler (
    void* arg
) {
    State_Code_t OrgCurrentState = CurrentState;
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    DKEventHeader_t EventHeader;
    state_event_func_t ServerEventFunc;

    IntCentralGet_EventResourceOfGivenMode (OrgCurrentState, STATE_EVENT_MQTT);

    EventHeader = DKServerManager_GetEventObjHeader (arg);
    ServerEventFunc = IntCentralGetEvenFuncTableOFCurrentMode (STATE_EVENT_MQTT);

    if (ServerEventFunc) {
        EventStateRet = ServerEventFunc (arg);
    } else {
        G_SYS_DBG_LOG_V ("WARNING: ServerEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE) {
        switch (EventHeader.MessageType) {
            default : {
                G_SYS_DBG_LOG_V (
                    "WARNING: Not supported MainEvent_ServerManagerEventReportHandler (%d)\r\n",
                    EventHeader.MessageType
                );
            }
            break;
        }
    }

    // Always clean the arg object from event
    MqttManager_CleanEventReport (arg);

    // Release the resource
    IntCentralPut_EventResourceOfGivenMode (OrgCurrentState, STATE_EVENT_MQTT);

}
#endif


#if DEVICE_IS_HYBRID

void MainEvent_HpManagerEventReportHandler(void* arg)
{
    //G_SYS_DBG_LOG_V("%s IN\r\n", __FUNCTION__);

    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_HP);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    dxBOOL bAlreadyhandledByDefaultEvent = dxTRUE;
    switch(pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_CONT_STORAGE_FORMAT_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_FORMAT_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            dxContStoreErrorCode_t ContStoreRet = Main_ContStore_Format(pHpCmdMessage->MessageID);

            {
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_FORMAT_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONT_STORAGE_INIT_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_INIT_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            dxContStoreErrorCode_t ContStoreRet = Main_ContStore_Init(pHpCmdMessage->MessageID);

            {
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_INIT_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONT_STORAGE_UNINIT_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_UNINIT_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            dxContStoreErrorCode_t ContStoreRet = Main_ContStore_Uninit(pHpCmdMessage->MessageID);

            {
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_UNINIT_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONT_STORAGE_READ_ALL_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_READ_ALL_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            dxContStoreErrorCode_t ContStoreRet = Main_ContStore_ReadAll(pHpCmdMessage->MessageID);

            {
                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_READ_ALL_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONT_STORAGE_WRITE_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_WRITE_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                ContStorageWriteReq_t* pWriteReq = (ContStorageWriteReq_t*)pHpCmdMessage->PayloadData;

                dxContStoreErrorCode_t ContStoreRet = Main_ContStore_Write(pHpCmdMessage->MessageID, pWriteReq->ContMType, pWriteReq->ContDType, pWriteReq->pDataPayload,pWriteReq->DataLen);

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_WRITE_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONT_STORAGE_FIND_REQUEST:
        {
            G_SYS_DBG_LOG_V("HP_CMD_CONT_STORAGE_FIND_REQUEST received! ID = %d\r\n", pHpCmdMessage->MessageID);
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                ContStorageFindReq_t* pFindReq = (ContStorageFindReq_t*)pHpCmdMessage->PayloadData;

                dxContStoreErrorCode_t ContStoreRet = Main_ContStore_Find(pHpCmdMessage->MessageID, pFindReq->ContMType, pFindReq->ContDType);

                HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
                pCmdMessage->CommandMessage = HP_CMD_CONT_STORAGE_FIND_RES;
                pCmdMessage->MessageID = pHpCmdMessage->MessageID;
                pCmdMessage->Priority = 0; //Normal

                pCmdMessage->PayloadSize = sizeof(ContStorageRes_t);
                ContStorageRes_t* pRetInfo = dxMemAlloc("HpCmdMsgpayload", 1, sizeof(ContStorageRes_t));
                pRetInfo->Status = ContStoreRet;
                pCmdMessage->PayloadData = (uint8_t*)pRetInfo;

                HpManagerSendTo_Client (pCmdMessage);

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
        }
        break;

        case HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_RES:
        case HP_CMD_CONTROL_ACCESS_WRITE_RES:
        case HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_RES:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_CONTROL_ACCESS RESPONSE received! \r\n" );
            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpControlAccessRes_t* pControlAccessResponse = (HpControlAccessRes_t*)pHpCmdMessage->PayloadData;

                int16_t StatusReport = IntConvertHybridPeripheralControlAccessResultStateToJobDoneReportStatus(pControlAccessResponse->Status);

                G_SYS_DBG_LOG_V(  "SourceOfOrigin = %d,  IdentificationNumber = %llu\r\n", pControlAccessResponse->SourceOfOrigin, (unsigned long long)pControlAccessResponse->IdentificationNumber);
                G_SYS_DBG_LOG_V(  "StatusReport = %d\r\n", StatusReport);

                if (pControlAccessResponse->DataLen)
                {
                    cUtlJobDoneReportSendWithPayloadHandler((uint16_t)pControlAccessResponse->SourceOfOrigin,
                                                            pControlAccessResponse->IdentificationNumber,
                                                            StatusReport,
                                                            pControlAccessResponse->DataLen,
                                                            pControlAccessResponse->pDataPayload);
                }
                else
                {
                    cUtlJobDoneReportSendHandler((uint16_t)pControlAccessResponse->SourceOfOrigin, pControlAccessResponse->IdentificationNumber, StatusReport);
                }

            }
        }
        break;

        case HP_CMD_JOBBRIDGE_ADD_SCHEDULE_JOB_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_SCHEDULE_JOB_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobBridgeJobPayload_t* JobBridgeJobAddReq = (HpJobBridgeJobPayload_t*)pHpCmdMessage->PayloadData;
                dxJOB_MANAGER_RET_CODE ret = JobManagerNewJob_Task(JobBridgeJobAddReq->pDataPayload, JobBridgeJobAddReq->DataLen, SOURCE_OF_ORIGIN_HP, 0);
                if(ret != DXJOBMANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_SCHEDULE_JOB_REQUEST, return code = %d\r\n", ret);
                }
            }
        }
        break;

        case HP_CMD_JOBBRIDGE_ADD_ADV_JOB_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_ADV_JOB_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = (HpJobBridgeJobPayload_t*)pHpCmdMessage->PayloadData;
                dxJOB_MANAGER_RET_CODE ret = JobManagerNewAdvanceJob_Task(JobBridgeAdvJobAddReq->pDataPayload, 0, JobBridgeAdvJobAddReq->DataLen, JobBridgeAdvJobAddReq->AdvJobObjID , SOURCE_OF_ORIGIN_HP/*_JOBBRIDGE_JOB*/,  0);
                if(ret != DXJOBMANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_ADV_JOB_REQUEST, return code = %d\r\n", ret);
                }
            }
        }
        break;

        case HP_CMD_JOBBRIDGE_ADD_SINGLE_GW_ADV_JOB_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_SINGLE_GW_ADV_JOB_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = (HpJobBridgeJobPayload_t*)pHpCmdMessage->PayloadData;
                dxJOB_MANAGER_RET_CODE ret = JobManagerNewAdvanceJob_Task(JobBridgeAdvJobAddReq->pDataPayload, 1, JobBridgeAdvJobAddReq->DataLen, JobBridgeAdvJobAddReq->AdvJobObjID , SOURCE_OF_ORIGIN_HP/*_JOBBRIDGE_JOB*/,  0);
                if(ret != DXJOBMANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ADD_SINGLE_GW_ADV_JOB_REQUEST, return code = %d\r\n", ret);
                }
            }
        }
        break;

        case HP_CMD_JOBBRIDGE_ONE_TIME_EXECUTE_JOB_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ONE_TIME_EXECUTE_JOB_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpJobBridgeJobPayload_t* JobBridgeAdvJobAddReq = (HpJobBridgeJobPayload_t*)pHpCmdMessage->PayloadData;
                dxJOB_MANAGER_RET_CODE ret = JobManagerNewJob_Task(JobBridgeAdvJobAddReq->pDataPayload, JobBridgeAdvJobAddReq->DataLen, SOURCE_OF_ORIGIN_HP/*_JOBBRIDGE_JOB*/,  JobBridgeAdvJobAddReq->IdentificationNumber);
                if(ret != DXJOBMANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "HP_CMD_JOBBRIDGE_ONE_TIME_EXECUTE_JOB_REQUEST, return code = %d\r\n", ret);
                }
            }
        }
        break;

        case HP_CMD_ENTER_SYSTEM_PAIRING_MODE_REQUEST:
        {
            G_SYS_DBG_LOG_V(  "HP_CMD_ENTER_SYSTEM_PAIRING_MODE_REQUEST received! \r\n" );

            if (pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData)
            {
                HpSystemPairing_t* SystemPairing = (HpSystemPairing_t*)pHpCmdMessage->PayloadData;
                G_SYS_DBG_LOG_V(  "HP_CMD_ENTER_SYSTEM_PAIRING_MODE_REQUEST for %d seconds \r\n", SystemPairing->Timeout);
                SystemPairingTimout = SystemPairing->Timeout;
                //StateManager_result_t StateManagerRetCode = SetStateMode(STATE_OPERATION_MODE_SWITCHING_TO_SETUP);
                G_SYS_DBG_LOG_V("[NewSt]%s Enter STATE_SETUP\r\n", HP_CMD_ENTER_SYSTEM_PAIRING_MODE_REQUEST);
                CentralStateMachine_ChangeState(STATE_SETUP, NULL, 0);
            }
        }
        break;

        case HP_CMD_SYS_REPORT_GET_LOCAL_TIME_REQ :
        {

            dxOS_RET_CODE ret;
            DKTimeLocal_t localTime;

            HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc ("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
            pCmdMessage->CommandMessage = HP_CMD_SYS_REPORT_GET_LOCAL_TIME_RES;
            pCmdMessage->MessageID = pHpCmdMessage->MessageID;
            pCmdMessage->Priority = 0; //Normal
            HpLocalTimeGenericRes_t* LocalTimeRes = dxMemAlloc ("PayloadData", 1, sizeof (HpLocalTimeGenericRes_t));
            pCmdMessage->PayloadSize = sizeof (HpLocalTimeGenericRes_t);
            pCmdMessage->PayloadData = (uint8_t*) LocalTimeRes;

            ret = dxGetLocalTime (&localTime);
            if (ret == DXOS_SUCCESS) {
                LocalTimeRes->Status = HP_LOCAL_TIME_GET_SUCCESS;
                memcpy (&LocalTimeRes->Time, &localTime, sizeof (DKTimeLocal_t));
            } else {
                LocalTimeRes->Status = HP_LOCAL_TIME_GET_FAILED;
            }

            HpManagerSendTo_Client (pCmdMessage);
            HpManagerCleanEventArgumentObj (pCmdMessage);
        }
        break;

        case HP_CMD_SYS_GET_SECURITY_KEY_REQ : {

            HpGetSecurityKeyRes_t* securityKeyRes = dxMemAlloc ("securityKeyRes", 1, sizeof (HpGetSecurityKeyRes_t));
            uint16_t keyLength;

            dxBOOL utlResult = cUtlGetSecurityKey (
                securityKeyRes->securityKey,
                &keyLength
            );
            if (utlResult != dxTRUE) {
                securityKeyRes->status = HP_GET_SECURITY_KEY_FAILED;
            } else {
                securityKeyRes->status = HP_GET_SECURITY_KEY_SUCCESS;
            }

            G_SYS_DBG_LOG_V("HP_CMD_SYS_GET_SECURITY_KEY_REQ, securityKeyRes->status=%d\r\n", securityKeyRes->status);

            HpCmdMessageQueue_t* hpMessage = dxMemAlloc ("hpMessage", 1, sizeof(HpCmdMessageQueue_t));
            hpMessage->CommandMessage = HP_CMD_SYS_GET_SECURITY_KEY_RES;
            hpMessage->MessageID = pHpCmdMessage->MessageID;
            hpMessage->Priority = 0; //Normal
            hpMessage->PayloadSize = sizeof (HpGetSecurityKeyRes_t);
            hpMessage->PayloadData = (uint8_t*) securityKeyRes;

            HpManagerSendTo_Client (hpMessage);
            HpManagerCleanEventArgumentObj (hpMessage);
        }
        break;

        default:
            bAlreadyhandledByDefaultEvent = dxFALSE;
        break;
    }

    if(bAlreadyhandledByDefaultEvent == dxFALSE)
    {
        state_event_func_t HpEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_HP);

        if (HpEventFunc)
        {
            EventStateRet = HpEventFunc(arg);
        }
        else
        {
            G_SYS_DBG_LOG_V("WARNING: HpEventFunc not implemented\r\n");
        }

        if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
        {
            switch (pHpCmdMessage->CommandMessage)
            {
                default:
                {
                    G_SYS_DBG_LOG_V("WARNING: Not supported %s (%d)\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage );
                }
                break;
            }
        }
    }

    //Always clean the arg object from event
    HpManagerCleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("%s OUT\r\n", __FUNCTION__);

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_HP);
}

#endif //#if DEVICE_IS_HYBRID

void MainEvent_WifiManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_WIFI);

    //G_SYS_DBG_LOG_V("MainEvent_WifiManagerEventReportHandler IN\r\n");

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    state_event_func_t WifiEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_WIFI);

    if (WifiEventFunc)
    {
        EventStateRet = WifiEventFunc(arg);
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: WifiEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
    {
        switch (EventHeader.ReportEvent)
        {
            default:
            {
                G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_WifiManagerEventReportHandler (%d)\r\n", EventHeader.ReportEvent );
            }
                break;
        }
    }

    //Always clean the arg object from event
    WifiCommManagerCleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_WifiManagerEventReportHandler OUT\r\n");

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_WIFI);

}

void MainEvent_UdpManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_UDP);

    //G_SYS_DBG_LOG_V("MainEvent_UdpManagerEventReportHandler IN\r\n");

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    state_event_func_t UdpEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_UDP);

    if (UdpEventFunc)
    {
        EventStateRet = UdpEventFunc(arg);
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: UdpEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
    {
        switch (EventHeader->PacketType)
        {
            default:
            {
                G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_UdpManagerEventReportHandler (%d)\r\n", EventHeader->PacketType );
            }
                break;
        }
    }

    //Always clean the arg object from event
    UdpCommManagerCleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_UdpManagerEventReportHandler OUT\r\n");

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_UDP);

}

void MainEvent_KeypadManagerEventReportHandler(void* arg)
{
    //Get the resource first
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_KEYPAD);

    //G_SYS_DBG_LOG_V("MainEvent_KeypadManagerEventReportHandler IN\r\n");

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    dxKeypadReport_t* KeyPadEvent = (dxKeypadReport_t*)arg;

    state_event_func_t KeyEventFunc = IntCentralGetEvenFuncTableOFCurrentMode(STATE_EVENT_KEYPAD);

    if (KeyEventFunc)
    {
        EventStateRet = KeyEventFunc(arg);
    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: KeyEventFunc not implemented\r\n");
    }

    if (EventStateRet == STATE_MACHINE_EVENT_NEED_PROCESS_BY_STATE)
    {
        static uint8_t  KeyHoldTimeInSec = 0;
        static dxBOOL   MainButtonPressed = dxFALSE;

        switch (KeyPadEvent->KeyStateEvent)
        {
            case DXKEYPAD_KEY_PRESSED:
            {
                if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID)
                {
                    MainButtonPressed = dxTRUE;
                }
                KeyHoldTimeInSec = 0;
            }
            break;

            case DXKEYPAD_KEY_RELEASED:
            {
                if (KeyHoldTimeInSec == 0)
                {
                    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_ONE_TIME_ACCESS_DENIED);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_V1( "WARNING: LedManager Failed to SetLedCondition (ret error %d)\r\n", LedManagerRetCode );
                    }
                }

                if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID)
                {
                    MainButtonPressed = dxFALSE;
                }
                KeyHoldTimeInSec = 0;
            }
            break;

            case DXKEYPAD_KEY_HELD:
            {
                if (KeyPadEvent->ReportIdentifier == MAIN_BUTTON_REPORT_ID && MainButtonPressed == dxTRUE)
                {
                    KeyHoldTimeInSec++;
                    if (KeyHoldTimeInSec == MAIN_BUTTON_OPERATION_CHANGE_ON_KEY_HOLD_IN_SECONDS)
                    {
                        BLEManager_States_t BleManagerState = BleManagerCurrentState();
                        if ((BleManagerState == DEV_MANAGER_CENTRAL_STATE_IDLE) || (BleManagerState == DEV_MANAGER_CENTRAL_STATE_SCANNING))
                        {
                            //Jump to Setup Mode
                            StateMachine_result_t ChangeStateResult = CentralStateMachine_ChangeState(STATE_SETUP, NULL, 0);

                            if (ChangeStateResult != STATE_MACHINE_RET_SUCCESS)
                            {
                                G_SYS_DBG_LOG_V1( "WARNING: CentralStateMachine_ChangeState Failed (ret error %d)\r\n", ChangeStateResult );
                            }
                        }
                        else
                        {
                            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_ONE_TIME_ACCESS_DENIED);
                            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                            {
                                G_SYS_DBG_LOG_V1( "WARNING: LedManager Failed to SetLedCondition (ret error %d)\r\n", LedManagerRetCode );
                            }
                        }
                    }
                }
            }
            break;

            default:
            {
                G_SYS_DBG_LOG_V("WARNING: Not supported MainEvent_KeypadManagerEventReportHandler (%d)\r\n", KeyPadEvent->ReportIdentifier );
            }
            break;
        }
    }

    //Always clean the arg object from event
    dxKeypad_CleanEventArgumentObj(arg);

    //G_SYS_DBG_LOG_V("MainEvent_KeypadManagerEventReportHandler OUT\r\n");

    //Release the resource
    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_KEYPAD);

}

void MainEvent_CentralLedManagerEventReportHandler(void* arg)
{
    //TODO:
    State_Code_t OrgCurrentState = CurrentState;
    IntCentralGet_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_LED);

    IntCentralPut_EventResourceOfGivenMode(OrgCurrentState, STATE_EVENT_LED);
}


//============================================================================
// Function
//============================================================================

state_machine_main_event_t CentralStateMachine_Init(State_Code_t InitialState)
{
    state_machine_main_event_t MainEventFunctionCall;

    memset(&MainEventFunctionCall, 0 , sizeof(state_machine_main_event_t));

    if (InitialState == STATE_UNKNOWN)
    {
        G_SYS_DBG_LOG_V("WARNING:CentralStateMachine_Init InitialState not supported\r\n" );
        goto ExitInit;
    }

    if (CurrentState == STATE_UNKNOWN)
    {

        uint16_t i = 0;
        for (i = 0; i < (sizeof(StateMachineEventFuncTable)/sizeof(state_machine_event_table_t)); i++)
        {
            if (StateMachineEventFuncTable[i].EventResourceProtection == NULL)
            {
                StateMachineEventFuncTable[i].EventResourceProtection = dxSemCreate( 1,          // uint32_t uxMaxCount
                                                                                    1);         // uint32_t uxInitialCount

                if (StateMachineEventFuncTable[i].EventResourceProtection == NULL)
                {
                    G_SYS_DBG_LOG_V("WARNING: Unable to create semaphore in CentralStateMachine_Init\r\n" );
                    goto ExitInit;
                }
            }
            else
            {
                G_SYS_DBG_LOG_V("WARNING: CentralStateMachine_Init event semaphore expected NULL\r\n" );
            }
        }

        //Set the initial state
        NextState = InitialState;

        MainEventFunctionCall.EventBLEManager = MainEvent_BleManagerEventReportHandler;
        MainEventFunctionCall.EventJobManager = MainEvent_JobManagerEventReportHandler;
        MainEventFunctionCall.EventServerManager = MainEvent_ServerManagerEventReportHandler;
#if MQTT_IS_ENABLED
        MainEventFunctionCall.EventMqttManager = MainEvent_MqttManagerEventReportHandler;
#endif
#if DEVICE_IS_HYBRID
        MainEventFunctionCall.EventHpManager = MainEvent_HpManagerEventReportHandler;
#endif //#if DEVICE_IS_HYBRID
        MainEventFunctionCall.EventWifiManager = MainEvent_WifiManagerEventReportHandler;
        MainEventFunctionCall.EventUdpManager = MainEvent_UdpManagerEventReportHandler;
        MainEventFunctionCall.EventKeyPadManager = MainEvent_KeypadManagerEventReportHandler;
        MainEventFunctionCall.EventLEDManager = MainEvent_CentralLedManagerEventReportHandler;

    }

ExitInit:
    return MainEventFunctionCall;
}


StateMachine_result_t CentralStateMachine_Process(void)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    //Change of state?
    if ((NextState >= STATE_UPPER_LIMIT) || (NextState < STATE_UNKNOWN))
    {
        G_SYS_DBG_LOG_V("%s ERROR: State Change has skipped %d -> %d\r\n", __FUNCTION__, CurrentState, NextState);
        NextState = CurrentState;
        result = STATE_MACHINE_RET_ERROR_INALID_STATE;
    }
    else if (CurrentState != NextState)
    {
        G_SYS_DBG_LOG_V("%s State Change In Progress %d -> %d\r\n", __FUNCTION__, CurrentState, NextState);

        State_Code_t OrgCurrentState = CurrentState;
        dxBOOL ResourceIsProtected = dxFALSE;
        if (CurrentState != STATE_UNKNOWN)
        {
            //Protect All rsources
            G_SYS_DBG_LOG_V("Protecting All Resources\r\n");
            ResourceIsProtected = dxTRUE;
            IntCentralProtectAllResourceOfState(OrgCurrentState);
            IntCentralProtectAllResourceOfState(NextState);
        }

        //We exit current state first
        result = IntProcessStateFunc(CurrentState, STATE_COND_EXIT, NULL);
        if (result != STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_V("WARNING: state exit IntProcessStateFunc error = %d\r\n", result );
        }

        //Now we initialize the new state
        result = IntProcessStateFunc(NextState, STATE_COND_INIT, pNextStateInitData);
        if (result != STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_V("WARNING: new state init IntProcessStateFunc error = %d\r\n", result );
        }

        CurrentState = NextState;

        //Release all resources
        if (ResourceIsProtected)
        {
            G_SYS_DBG_LOG_V("Releasing All Resources\r\n");
            IntCentralReleaseAllResourceOfState(OrgCurrentState);
            IntCentralReleaseAllResourceOfState(NextState);
        }
    }

    if (result != STATE_MACHINE_RET_ERROR_INALID_STATE)
    {
        if ((CurrentState >= STATE_UPPER_LIMIT) || (CurrentState < STATE_UNKNOWN))
        {
            G_SYS_DBG_LOG_V("%s ERROR: Invalid CurrentState = %d\r\n", __FUNCTION__, CurrentState);
            result = STATE_MACHINE_RET_ERROR_INALID_STATE;
        }
        else if (CurrentState != STATE_UNKNOWN)
        {
            //We exit current state first
            result = IntProcessStateFunc(CurrentState, STATE_COND_MAIN_PROCESS, NULL);
            if (result != STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_V("WARNING: state mainprocess IntProcessStateFunc error = %d\r\n", result );
            }
        }
        else
        {
            result = STATE_MACHINE_RET_NOT_SUPPORTED;
        }
    }

    return result;
}

StateMachine_result_t CentralStateMachine_ChangeState(State_Code_t StateChange, void* pStateInitData, uint32_t StateInitDataSize)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if ((StateChange >= STATE_UPPER_LIMIT) || (StateChange < STATE_UNKNOWN))
    {
        G_SYS_DBG_LOG_V("%s ERROR: State Change has skipped %d -> %d\r\n", __FUNCTION__, CurrentState, StateChange);
        result = STATE_MACHINE_RET_ERROR_INALID_STATE;
    }
    else if (StateChange != STATE_UNKNOWN)
    {
        //Make sure the state change is differet than current
        if (CurrentState != StateChange)
        {
            //Just make sure we are not in state transition
            if (CurrentState != NextState)
            {
                result = STATE_MACHINE_RET_NOT_ALLOWED_ST_CHANGE_INPROCESS;
            }
            else
            {

                //Signal to switch to next state at next process cycle.
                NextState = StateChange;

                if (pNextStateInitData)
                {
                    dxMemFree(pNextStateInitData);
                    pNextStateInitData = NULL;
                }

                if (pStateInitData && StateInitDataSize)
                {
                    pNextStateInitData = dxMemAlloc("NextStateInitData", 1, StateInitDataSize);
                    memcpy(pNextStateInitData, pStateInitData, StateInitDataSize);
                }

                G_SYS_DBG_LOG_V("Request to change State from %d to %d\r\n", CurrentState,  NextState);
            }

        }
        else
        {
            result = STATE_MACHINE_RET_NOT_SUPPORTED;
            G_SYS_DBG_LOG_V("CentralState same state change not supported (%d)\r\n", CurrentState);
        }
    }
    else
    {
        result = STATE_MACHINE_RET_NOT_SUPPORTED;
    }

    return result;
}

State_Code_t CentralStateMachine_GetCurrentState(void)
{
    return CurrentState;
}

StateMachine_result_t CentralStateMachine_Uninit(void)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    //TODO: This function is most likely not going to be used but if it comes the time of needing it, do all necessary clean up here
    G_SYS_DBG_LOG_V("WARNING: CentralStateMachine_Uninit NOT Implemented\r\n");

    return result;

}
