//============================================================================
// File: ST_ServerOnlineMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_SERVER_ONLINE_MAIN_H
#define _ST_SERVER_ONLINE_MAIN_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t ServerOnline_Init(void* data );
StateMachine_result_t ServerOnline_MainProcess(void* data );
StateMachine_result_t ServerOnline_End(void* data );
StateMachineEvent_State_t SrvOnline_EventBLEManager(void* data );
StateMachineEvent_State_t SrvOnline_EventServerManager(void* data );
StateMachineEvent_State_t SrvOnline_EventMqttManager(void* data );
#if DEVICE_IS_HYBRID
StateMachineEvent_State_t SrvOnline_EventHpManager(void* data);
#endif //#if DEVICE_IS_HYBRID
StateMachineEvent_State_t SrvOnline_EventWifiManager(void* data );
StateMachineEvent_State_t SrvOnline_EventUdpManager(void* data );
StateMachineEvent_State_t SrvOnline_EventJobManager(void* data );
StateMachineEvent_State_t SrvOnline_EventKeyPadManager(void* data );
StateMachineEvent_State_t SrvOnline_EventLEDManager(void* data );


#endif // _ST_SERVER_ONLINE_MAIN_H
