//============================================================================
// File: ST_FwUpdateMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_FW_UPDATE_MAIN_H
#define _ST_FW_UPDATE_MAIN_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t FwUpdate_Init(void* data );
StateMachine_result_t FwUpdate_MainProcess(void* data );
StateMachine_result_t FwUpdate_End(void* data );
StateMachineEvent_State_t FwUpdate_EventBLEManager(void* data );
StateMachineEvent_State_t FwUpdate_EventServerManager(void* data );
StateMachineEvent_State_t FwUpdate_EventMqttManager(void* data );
#if DEVICE_IS_HYBRID
StateMachineEvent_State_t FwUpdate_EventHpManager(void* data);
#endif //#if DEVICE_IS_HYBRID
StateMachineEvent_State_t FwUpdate_EventWifiManager(void* data );
StateMachineEvent_State_t FwUpdate_EventUdpManager(void* data );
StateMachineEvent_State_t FwUpdate_EventJobManager(void* data );
StateMachineEvent_State_t FwUpdate_EventKeyPadManager(void* data );
StateMachineEvent_State_t FwUpdate_EventLEDManager(void* data );


#endif // _ST_FW_UPDATE_MAIN_H
