//============================================================================
// File: ST_RegisterMain.c
//
// Author: Allen Liao
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#include "HpJobBridge.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "dxDeviceInfoManager.h"
#include "ST_CommonUtils.h"
#include "CentralStateMachineAutomata.h"
#include "SimpleTimedStateMachine.h"
#include "ST_RegisterMain.h"
#include "DataDrivenStateMachine.h"
#include "OfflineSupport.h"
#include "BLERepeater.h"

//============================================================================
// Function Definition
//============================================================================

SimpleStateMachine_result_t SubState_RegisterWifiConnecting(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterWifiScanDone(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterWifiIsConnected(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterLogInToServer(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterServerLoggedIn(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterGetPeripheral(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterAddGateway(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterHostRedirect(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterFinish(void* data, dxBOOL isFirstCall);
SimpleStateMachine_result_t SubState_RegisterError(void* data, dxBOOL isFirstCall);

//============================================================================
// Constants
//============================================================================

//Get Peripheral Request
#define RegisterGET_PERIPHERAL_STARTING_INDEX                   1   //NOTE: First item starts at 1 (NOT 0)
#define RegisterGET_PERIPHERAL_MAX_PER_REQUEST                  1

//============================================================================
// Enum
//============================================================================

typedef enum
{
    GET_PERIPHERAL_IDLE           = 0,

    GET_PERIPHERAL_CHECK_GATEWAY,
    GET_PERIPHERAL_CHECK_PERIPHERAL,
    GET_PERIPHERAL_CHECK_CONTAINER,
    GET_PERIPHERAL_CHECK_ITERATION,

} GetPeripheralStateCode;

typedef enum
{
    REGISTER_SUBSTATE_IDLE           = 0,
    REGISTER_SUBSTATE_WIFI_CONNECTING,
    REGISTER_SUBSTATE_WIFI_SCAN_DONE,
    REGISTER_SUBSTATE_WIFI_IS_CONNECTED,
    REGISTER_SUBSTATE_SERVER_LOGIN,
    REGISTER_SUBSTATE_SERVER_CONNECTED,
    REGISTER_SUBSTATE_RESET,
    REGISTER_SUBSTATE_GET_PERIPHERAL,
    REGISTER_SUBSTATE_ADD_GATEWAY,
    REGISTER_SUBSTATE_HOST_REDIRECT,
    REGISTER_SUBSTATE_FINISH,
    REGISTER_SUBSTATE_ERROR,

} Register_SubState_Code_t;

typedef enum {
    REGISTER_SERVER_LOCATION_MAIN  = 0,
    REGISTER_SERVER_LOCATION_QA,
} Register_Server_Location_t;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

static simple_st_machine_table_t OfflineSubStateProcessFuncTable[] =
{
    {REGISTER_SUBSTATE_IDLE, NULL, 0},
    {REGISTER_SUBSTATE_WIFI_CONNECTING, SubState_RegisterWifiConnecting, 0},    //Call every 10 seconds if remain in the same state
    {REGISTER_SUBSTATE_WIFI_SCAN_DONE, SubState_RegisterWifiScanDone, 10*SECONDS},       //Call every 10 seconds if remain in the same state
    {REGISTER_SUBSTATE_WIFI_IS_CONNECTED, SubState_RegisterWifiIsConnected, 0},
    {REGISTER_SUBSTATE_SERVER_LOGIN, SubState_RegisterLogInToServer, 5*SECONDS},        //Call every 10 seconds if remain in the same state
    {REGISTER_SUBSTATE_SERVER_CONNECTED, SubState_RegisterServerLoggedIn, 0},
    {REGISTER_SUBSTATE_GET_PERIPHERAL, SubState_RegisterGetPeripheral, 5*SECONDS},
    {REGISTER_SUBSTATE_ADD_GATEWAY, SubState_RegisterAddGateway, 5*SECONDS},
    {REGISTER_SUBSTATE_HOST_REDIRECT, SubState_RegisterHostRedirect, 15*SECONDS},
    {REGISTER_SUBSTATE_FINISH, SubState_RegisterFinish, 15*SECONDS},
    {REGISTER_SUBSTATE_ERROR, SubState_RegisterError, 15*SECONDS},
    {REGISTER_SUBSTATE_RESET, cUtlSubState_Reset, 0},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};

SimpleStateHandler      RegisterSubStateHandler = NULL;

dxTime_t    gRegisterServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MIN;

Register_Server_Location_t	Register_Server_Location	= REGISTER_SERVER_LOCATION_MAIN;

//Temporary storage for Task info that require handling that is not in the same run loop
TaskInfo_t  RegisterTaskInfoStorage[MAX_TASK_INFO_STORAGE_SLOT];

dxBOOL      WiFiReConnect = dxFALSE;
dxBOOL      GatewayExist = dxFALSE;
dxBOOL      PeripheralExist = dxFALSE;

//============================================================================
// Utils Function
//============================================================================

//============================================================================
// Internal Function
//============================================================================

dxBOOL IntRegisterServerStateReadyCheck( void )
{
    dxBOOL ServerStateReady = dxTRUE;

    if (DKServerManager_GetCurrentState() != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        ServerStateReady = dxFALSE;

        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Wrong server state=%d\r\n", DKServerManager_GetCurrentState() );

        //Change to Offline
        CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
    }

    return ServerStateReady;
}

SimpleStateMachine_result_t SubState_RegisterError(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "error, clean disk and go idle mode\r\n" );

    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
    if (DevInfoRet != DXDEV_INFO_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_IDLE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                     __LINE__, DevInfoRet);
    }

    LedManager_result_t LedManagerRetCode = SetLedCondition (LED_CONDITION_DK_SERVER_OPERATION_ERROR);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS) {
        G_SYS_DBG_LOG_IV (
            G_SYS_DBG_CATEGORY_ONLINE,
            G_SYS_DBG_LEVEL_FATAL_ERROR,
            "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
            __LINE__,
            LedManagerRetCode
        );
    }

    CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );

    return result;
}

SimpleStateMachine_result_t SubState_RegisterFinish(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    int32_t RegisterState = REGISTER_STATE_FINISH;
    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
    if (DevInfoRet != DXDEV_INFO_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                     __LINE__,
                                     DevInfoRet);
    }

    DevInfoRet =  dxDevInfo_SaveToDisk();
    if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SubState_RegisterFinish dxDevInfo_SaveToDisk failed (%d)\r\n",
                                     __LINE__, DevInfoRet );
    }

    CentralStateMachine_ChangeState( STATE_SERVER_ONLINE, NULL, 0 );

    return result;
}

SimpleStateMachine_result_t SubState_RegisterHostRedirect(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntRegisterServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Host Redirecting will automatically switch to suspended and back to ready state... our state process will automatically handle the login process and
            //other related task as usual.
            DKServerManager_HostRedirect_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, (uint8_t *)DK_QA_SERVER_DNS_NAME, (uint8_t *)DK_QA_SERVER_API_VERSION);
        }
        else
        {
            int32_t RegisterState = REGISTER_STATE_NONE;
            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
            if (DevInfoRet != DXDEV_INFO_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                             __LINE__,
                                             DevInfoRet);
            }

            DevInfoRet = dxDevInfo_SaveToDisk();
            if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
                G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                    "WARNING:(Line=%d) DKMSG_ADDGATEWAY dxDevInfo_SaveToDisk failed (%d)\r\n",
                                    __LINE__, DevInfoRet );
            }

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_SERVER_LOGIN);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_SERVER_LOGIN (%d)\r\n",
                                 __LINE__, SmpResult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterWifiConnecting(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    RegisterState_t rs_ret = REGISTER_STATE_NOT_EXIST;
    WifiCommManager_ConnectionStatus_t wifi_st = WIFI_COMM_CONNECTION_UP_BUT_BUSY;

    wifi_st = WifiCommManagerGetWifiConnectionStatus();
    if ( wifi_st == WIFI_COMM_CONNECTION_UP)
    {
        //Switch sub state to wifi connected
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_IS_CONNECTED);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                             __LINE__,
                             SmpResult);
        }
    } else {
        rs_ret = cUtlCheckRegisterStateGet();
        if ( rs_ret == REGISTER_STATE_NONE ) {

            // Stop UdpCommManager when Wi-Fi connection is down
            UdpCommManagerStop();

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "WARNING: wifi disconnection, go to idle mode!!!\r\n");

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_ERROR);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_ERROR (%d)\r\n",
                                 __LINE__, SmpResult);
            }
        } else {	//REGISTER_STATE_REGISTERING
            if ( WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN ) {
                if ( WiFiReConnect == dxTRUE ) {
                    WiFiReConnect = dxFALSE;
                    cUtlConnectToWifi();

                    //Switch sub state to wifi connected
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_SCAN_DONE);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_SCAN_DONE (%d)\r\n",
                                         __LINE__,
                                         SmpResult);
                    }
                } else {
                    CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
                }
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterWifiScanDone(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;

    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (isFirstCall)
    {
        TimeAtFirstTry = dxTimeGetMS();
    }

    if (dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_DONE ||
        dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_NONE)
    {
        if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_UP)
        {
            //Switch sub state to wifi connected
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_IS_CONNECTED);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        else if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN)
        {
            // We can't find WiFi AP which we expect to connect to
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
    }
    else if (dxWiFi_GetScanState() == DXWIFI_SCAN_STATE_SCANNING)
    {
        if (dxTimeGetMS() > TimeAtFirstTry + WIFI_SCAN_TIMEOUT_MAX_MS)
        {
            // We doesn't receive WIFI_EVENT_SCAN_DONE (This should not happened)
            // Scan again

            WifiCommManager_result_t wifiresult = WifiCommManagerDisconnectCurrentConnectionTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);

            if (wifiresult == WIFI_COMM_MANAGER_SUCCESS)
            {
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SubState_RegisterWifiConnecting unable to disconnect wifi (%d)\r\n",
                                 __LINE__,
                                 wifiresult);
            }
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterWifiIsConnected(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    // Restart UdpCommManager when Wi-Fi connection is Up
    UdpCommManagerStart();

    LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_CONNECTING_WITH_DK_SERVER);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                         __LINE__,
                         LedManagerRetCode);
    }

    //Simply switch to server log in and let it ahndles it.
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_SERVER_LOGIN);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_SERVER_LOGIN (%d)\r\n",
                         __LINE__,
                         SmpResult);
    }

    return result;
}


SimpleStateMachine_result_t SubState_RegisterLogInToServer(void* data, dxBOOL isFirstCall)
{

    static dxTime_t TimeAtFirstTry = 0;
    static dxTime_t TimeoutReal = SERVER_LOG_IN_TIMEOUT_MIN;

    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    //TODO: IMPORTANT
    //      We should handle when server state is NOT ready since log-in relies on this
    //      If server state is not ready we should rekick the retry process if this is not automatic in server manager

    if (isFirstCall)
    {
        TimeAtFirstTry = dxTimeGetMS();
        gRegisterServerLoginTimeoutMax = gRegisterServerLoginTimeoutMax + SERVER_LOG_IN_TIMEOUT_ADD;
        if(gRegisterServerLoginTimeoutMax > SERVER_LOG_IN_TIMEOUT_MAX)
        {
            gRegisterServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MAX;
        }

        srand((uint32_t)dxTimeGetMS());
        uint32_t index = (uint32_t)rand();
        TimeoutReal = SERVER_LOG_IN_TIMEOUT_MIN + (index % (gRegisterServerLoginTimeoutMax - SERVER_LOG_IN_TIMEOUT_MIN));

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "%s DKServerManager login timeout is %d\r\n", __FUNCTION__, TimeoutReal);

    }

    if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_DOWN)
    {
        //Connection when down, go back to wifi connect
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                             __LINE__,
                             SmpResult);
        }
    }
    else if ((dxTimeGetMS() - TimeAtFirstTry) > TimeoutReal)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) Unable to resume DKServerManager. Disconnect Wi-Fi and try again!!!\r\n",
                         __LINE__);

        TimeAtFirstTry = dxTimeGetMS();

        //Its taking too long for server to log in, we are going to disconnect wifi and start over.
        WifiCommManager_result_t wifiresult = WifiCommManagerDisconnectCurrentConnectionTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);
        if (wifiresult == WIFI_COMM_MANAGER_SUCCESS)
        {
            //Switch sub state to reconnect to wifi
            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                 __LINE__,
                                 SmpResult);
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SubState_RegisterWifiConnecting unable to disconnect wifi (%d)\r\n",
                             __LINE__,
                             wifiresult);
        }
    }
    else if (WifiCommManagerGetWifiConnectionStatus() == WIFI_COMM_CONNECTION_UP)
    {
        DKServerManager_StateMachine_State_t ServerState = DKServerManager_GetCurrentState();

        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "%s DKServerManager current state is %d\r\n", __FUNCTION__, ServerState);

        if (ServerState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
        {
            DKServerManager_State_Resume();
        }
        else if (ServerState == DKSERVERMANAGER_STATEMACHINE_STATE_READY)
        {
            // NOTE: If call cUtlLogInToServer() here, we will see UserLogin twice in case of hardware reboot.
            //       So, I removed calling cUtlLogInToServer() when receiving DKMSG_STATE_CHANGED
            cUtlLogInToServer();
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterServerLoggedIn(void* data, dxBOOL isFirstCall)
{

    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    RegisterState_t rs_ret = REGISTER_STATE_NOT_EXIST;

    gRegisterServerLoginTimeoutMax = SERVER_LOG_IN_TIMEOUT_MIN;

    rs_ret = cUtlCheckRegisterStateGet();
    if ( rs_ret == REGISTER_STATE_NONE ) {
        int32_t RegisterState = REGISTER_STATE_REGISTERING;
        dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
        if (DevInfoRet != DXDEV_INFO_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                         __LINE__,
                                         DevInfoRet);
        }

        DevInfoRet = dxDevInfo_SaveToDisk();
        if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "WARNING:(Line=%d) DKMSG_ADDGATEWAY dxDevInfo_SaveToDisk failed (%d)\r\n",
                                __LINE__, DevInfoRet );
        }

        //TODO: Add Gateway
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_ADD_GATEWAY);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_ADD_GATEWAY (%d)\r\n",
                             __LINE__, SmpResult);
        }
    } else {
        //TODO: GetPeripheralByMAC
        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_GET_PERIPHERAL);
        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_GET_PERIPHERAL (%d)\r\n",
                             __LINE__, SmpResult);
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterGetPeripheral(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntRegisterServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Synch time with server when needed
            DKTime_t TimeRTC;

            if ( dxSwRtcGet( &TimeRTC ) == DXOS_ERROR ) {
                DKServer_StatusCode_t DkServerResult = DKServerManager_GetServerTime_Asynch( SOURCE_OF_ORIGIN_DEFAULT, 0 );
                if ( DkServerResult != DKSERVER_STATUS_SUCCESS ) {
                    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                        "WARNNIG: Failed to send DKServerManager_GetServerTime_Asynch (Error = %d):", DkServerResult );
                }
            }

            GatewayExist = dxFALSE;
            PeripheralExist = dxFALSE;
            DKServer_StatusCode_t serverResult = DKServerManager_GetPeripheralByMAC_Asynch (
                SOURCE_OF_ORIGIN_DEFAULT,
                0,
                RegisterGET_PERIPHERAL_STARTING_INDEX,
                RegisterGET_PERIPHERAL_MAX_PER_REQUEST
            );
            if (serverResult != DKSERVER_STATUS_SUCCESS) {
                G_SYS_DBG_LOG_IV (
                    G_SYS_DBG_CATEGORY_ONLINE,
                    G_SYS_DBG_LEVEL_FATAL_ERROR,
                    "WARNING:(Line=%d) DKServerManager_GetPeripheralByMAC_Asynch failed (ret error %d)\r\n",
                    __LINE__,
                    serverResult
                );
            }

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "SubState_RegisterGetPeripheral change to Idle mode!\r\n");

            CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
        }
    }

    return result;
}

SimpleStateMachine_result_t SubState_RegisterAddGateway(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntRegisterServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Add gateway
            cUtlAddGateway();

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > CUTL_SUBSTATE_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
        }
    }

    return result;
}

int16_t IntRegisterTaskInfoDevIndexGetAndClear(uint64_t IndentificationNumber)
{

    int16_t     DevIndex = -1;
    uint8_t     i = 0;
    for (i = 0; i < MAX_TASK_INFO_STORAGE_SLOT; i++)
    {
        if (RegisterTaskInfoStorage[i].IdentificationNumber == IndentificationNumber)
        {
            DevIndex = RegisterTaskInfoStorage[i].DevHandlerID;

            memset(&RegisterTaskInfoStorage[i], 0, sizeof(TaskInfo_t));
            break;
        }
    }

    return DevIndex;
}

//============================================================================
// Event Function
//============================================================================

StateMachineEvent_State_t Register_EventBLEManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_KEEPERS_DEVICE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);
            JobManagerPeripheralDataUpdate_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber);
        }
        break;

        case DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST:
        {
#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                DeviceKeeperListReport_t* pDeviceKeeperList = BleManagerParseDeviceKeeperListReportEventObj(arg);
                cUtlHybridPeripheralDeviceKeeperListUpdate(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, (uint8_t *) pDeviceKeeperList->GenericInfo, sizeof(DeviceGenericInfo_t) * pDeviceKeeperList->DeviceCount);
            }
            else
#endif
#endif //DEVICE_IS_HYBRID
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "INFORMATION:(Line=%d) Receive DEV_MANAGER_EVENT_GET_DEVICE_KEEPER_LIST with SourceOfOrigin(%d)\r\n",
                                 __LINE__,
                                 EventHeader.SourceOfOrigin);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DAVICE_TASK_ACCESS_WITH_UPDATE_DATA_REPORT:
        {
            DeviceInfoReport_t* DeviceKeeperInfoReport = BleManagerParseKeeperPeripheralReportEventObj(arg);

            //We change this to FALSE (No force update) 2015/08/31. The reason is because force update will cause issue (eg Light)
            //with repeat historic event update.
            //NOTE: This access with data update force was develop mainly for "Group" access where we need to update
            //the payload as we change its state... But looks like even without force update we can still manage it
            JobManagerPeripheralDataUpdateExt_Task(DeviceKeeperInfoReport, EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, dxFALSE);

        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_REPORT:
        {
            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);

#if DEVICE_IS_HYBRID
            if(EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_HP)
            {
                HpJobBridgeExeRes_t JobBridgeExeRes;
                JobBridgeExeRes.StatusReport = StatusReport;
                JobBridgeExeRes.SourceOfOrigin = EventHeader.SourceOfOrigin;
                JobBridgeExeRes.IdentificationNumber = EventHeader.IdentificationNumber;
                HP_CMD_JobBridge_AdvJobBridgeExeRes(0, &JobBridgeExeRes, 0);
            }
#endif  //#if DEVICE_IS_HYBRID

            cUtlJobDoneReportSendHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport);

            //For now we ignore if the task was success or not... simply set condition to none
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }
        }
        break;

        case DEV_MANAGER_EVENT_DEVICE_TASK_ACCESS_READ_DATA_REPORT:
        {
            DataReadReportObj_t* pReadData = BleManagerParseDataReadReportEventObj(arg);

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (ret error %d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

            int16_t StatusReport = cUtlConvertBleDevManagerResultStateToJobDoneReportStatus(EventHeader.ResultState);
            cUtlJobDoneReportSendWithPayloadHandler(EventHeader.SourceOfOrigin, EventHeader.IdentificationNumber, StatusReport, pReadData->PayloadSize, pReadData->Payload);
        }
        break;


        case DEV_MANAGER_EVENT_DEVICE_SYSTEM_INFO:
        {
            SystemInfoReportObj_t* pSystemInfo = BleManagerParsePeripheralSystemInfoReportEventObj(arg);

            if (pSystemInfo)
            {
                if (BleManager_SBL_Enable())
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "BLE FW Image is ACTIVE_IMAGE_TYPE_SBL Changing state to BLE FW Download\r\n");

                    //We change to FW update state, although we are in offline mode we can still update BLE FW
                    //since the FW is more likely already in Flash.. Reaching here might be because of system
                    //crash during BLE FW update process.
                    CentralStateMachine_ChangeState( STATE_FW_UPDATE, NULL, 0 );
                }
            }
        }
        break;

        case DEV_MANAGER_EVENT_RTC_UPDATED:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "DEV_MANAGER_EVENT_RTC_UPDATED\r\n");

            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

        }
        break;

        case DEV_MANAGER_EVENT_NEW_DEVICE_ADDED:
        case DEV_MANAGER_EVENT_DEVICE_UNPAIRED:
            BLEManager_WhiteList_AddFromKeeperList();
            BleRepeater_UpdateWatchList ();

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
            if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
            }
#endif
#endif  //#if DEVICE_IS_HYBRID
            //no break to enter the following procedure
        case DEV_MANAGER_EVENT_DEVICE_TASK_READ_AGGREGATED_DATA_REPORT:
        case DEV_MANAGER_EVENT_DEVICE_NEW_SECURITY_EXCHANGED:
        {
            //We should not get here since this kind of access (that require server) is prevented
            //from JobManager event.
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "NOTE:Why are we still getting this ble event (%d)?\r\n",
                             EventHeader.ReportEvent);

            //Set the LED to condition none so that it does not remain in the access condition
            LedManager_result_t LedManagerRetCode = SetLedCondition(LED_CONDITION_NONE);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedCondition (%d)\r\n",
                                 __LINE__,
                                 LedManagerRetCode);
            }

        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Register_EventJobManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;

}


StateMachineEvent_State_t Register_EventServerManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    //For any other message type except DKMSG_SYSTEM_ERROR & DKMSG_STATE_CHANGED, we check if server re-direct is required as
    //this account could be configured for QA Test Server. We simply re-login to the QA server and proceed from there as usual.
    if (EventHeader.MessageType != DKMSG_SYSTEM_ERROR &&
        EventHeader.MessageType != DKMSG_STATE_CHANGED)
    {
        //If Access denied we may need to check if we need to re-direct to QA server
        if (EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_ACCESS_DENIED)
        {

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "Re-Directing to QA Server!\r\n");

            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_HOST_REDIRECT);
            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_HOST_REDIRECT (%d)\r\n",
                                 __LINE__, SmpResult);
            }

            return STATE_MACHINE_EVENT_NORMAL;
        }
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
              "%s EventHeader.MessageType (%d)\r\n", __FUNCTION__, EventHeader.MessageType);

    switch (EventHeader.MessageType)
    {
        case DKMSG_GATEWAYLOGIN:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                //Switch sub state machine
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_SERVER_CONNECTED);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_SERVER_CONNECTED (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }
            else
            {
                RegisterState_t rs_ret = REGISTER_STATE_NOT_EXIST;

                //TODO: Depending on the error code, we may need to handle it
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) unable to login to server (Report=%d)\r\n",
                                 __LINE__,
                                 EventHeader.s.ReportStatusCode);

                rs_ret = cUtlCheckRegisterStateGet();
                if ( rs_ret == REGISTER_STATE_NONE ) {
                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
                    if (DevInfoRet != DXDEV_INFO_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                         __LINE__, DevInfoRet);
                    }
                }

                CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
            }
        }
        break;

        case DKMSG_STATE_CHANGED:
        {

            //NOTE: DKSERVERMANAGER_STATEMACHINE_STATE_READY to login will be handled by the timed state SubState_RegisterLogInToServer
            if (EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_CHECKDKSERVER/* ||
                     EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_SESSIONTIMEOUT*/)
            {
                // Suspend & Resume DKServerManager
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) switch to REGISTER_SUBSTATE_SERVER_LOGIN\r\n",
                                 __LINE__);

                DKServerManager_State_Suspend();

                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_SERVER_LOGIN);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_SERVER_LOGIN (%d)\r\n",
                                     __LINE__,
                                     SmpResult);
                }
            }

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "DKMSG_STATE_CHANGED (%d)\r\n", EventHeader.s.StateMachineState);
        }
        break;

        case DKMSG_SYSTEM_ERROR:
        {
            uint16_t SystemErrorCode = EventHeader.s.SystemErrorCode;

            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "DKMSG_SYSTEM_ERROR (%d)\r\n", SystemErrorCode);

            switch (SystemErrorCode)
            {
                case DKSERVER_SYSTEM_ERROR_DX_RTOS:                 // 102
                    // An ssl_init() == -32512 (-0x7F00) will receive the error code
                    //
                    // Retry can't fix issue. Should REBOOT hardware
                    break;
                case DKSERVER_SYSTEM_ERROR_ALLOC_MEMORY:            // 103
                case DKSERVER_SYSTEM_ERROR_BUFFER_TOO_SMALL:        // 104
                case DKSERVER_SYSTEM_ERROR_INVALID_LOCAL_PARAMETER: // 105
                case DKSERVER_SYSTEM_ERROR_NETWORK_TIMEOUT:         // 106
                    break;
                default:
                    break;
            }
        }
        break;

        case DKMSG_ADDGATEWAY:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                DKTime_t CurrentDkTime;

                if ( dxSwRtcGet( &CurrentDkTime ) == DXOS_SUCCESS ) {
                    uint32_t RegisterUTCTime = ConvertDKTimeToUTC( CurrentDkTime );

                    char *pHostname = NULL;

                    DKServer_StatusCode_t ServerManagerRet = DKServerManager_GetHostname( &pHostname );

                    Register_Server_Location = REGISTER_SERVER_LOCATION_MAIN;

                    if ( ( ServerManagerRet == DKSERVER_STATUS_SUCCESS ) &&
                         ( strcmp( pHostname, DK_QA_SERVER_DNS_NAME ) == 0 ) ) {
                        Register_Server_Location = REGISTER_SERVER_LOCATION_QA;
                    }

                    if ( pHostname ) {
                        dxMemFree( pHostname );
                    }

                    if ( FW_AUTO_UPDATE_AFTER_REGISTER ) {
                        if ( Register_Server_Location == REGISTER_SERVER_LOCATION_MAIN ) {
                            RegisterUTCTime += FW_AUTO_UPDATE_AFTER_REGISTER;
                        } else {
                            RegisterUTCTime += 300;
                        }
                    }

                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_DEV_REGISTER_TIME, &RegisterUTCTime, sizeof( uint32_t ), dxFALSE );
                    if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
                        G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                            "WARNING:(Line=%d) DKMSG_ADDGATEWAY Update failed (%d)\r\n",
                                            __LINE__, DevInfoRet );
                    } else {
                        DevInfoRet =  dxDevInfo_SaveToDisk();
                        if ( DevInfoRet != DXDEV_INFO_SUCCESS ) {
                            G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                "WARNING:(Line=%d) DKMSG_ADDGATEWAY dxDevInfo_SaveToDisk failed (%d)\r\n",
                                                __LINE__, DevInfoRet );
                        }
                    }
                }

            #if DEVICE_IS_HYBRID
                cUtlAddHybridPeripheral();
            #else
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_FINISH);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_FINISH (%d)\r\n",
                                     __LINE__, SmpResult);
                }
            #endif
            }
            else
            {
                if (EventHeader.s.ReportStatusCode == DKSERVER_REPORT_STATUS_NOT_IN_WHITE_LIST)
                {
                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_CleanDisk();
                    if (DevInfoRet != DXDEV_INFO_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) dxDevInfo_CleanDisk failed (%d)\r\n",
                                         __LINE__, DevInfoRet);
                    }
                }

                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) DKMSG_ADDGATEWAY failed (%d)\r\n",
                                 __LINE__, EventHeader.s.ReportStatusCode);

                CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
            }
        }
        break;

        case DKMSG_ADDPERIPHERAL:
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_FINISH);
                if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_FINISH (%d)\r\n",
                                     __LINE__, SmpResult);
                }
            }
            else
            {
                CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
            }

        }
        break;

        case DKMSG_GETPERIPHERALBYGMACADDRESS:
        {
            DKEventHeader_t eventHeader = DKServerManager_GetEventObjHeader (arg);

            if (eventHeader.SubType == DKJOBTYPE_GATEWAY_INFO) {

                if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {
                    GatewayInfo_t* gatewayInfo = (GatewayInfo_t*) DKServerManager_GetEventObjPayload (arg);

                    ObjectDictionary_Lock ();
                    ObjectDictionary_DeleteGateway ();
                    ObjectDictionary_AddGateway (
                        gatewayInfo->objectId,
                        gatewayInfo->macAddress,
                        sizeof (gatewayInfo->macAddress),
                        gatewayInfo->wifiVersion,
                        gatewayInfo->bleVersion,
                        (uint8_t*) gatewayInfo->name
                    );
                    ObjectDictionary_Unlock ();
                    GatewayExist = dxTRUE;
                }

            } else if (eventHeader.SubType == DKJOBTYPE_PERIPHERAL_INFO) {

                if (eventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS) {
                    PeripheralInfo_t* peripheralInfo = (PeripheralInfo_t*) DKServerManager_GetEventObjPayload (arg);
                    const WifiCommDeviceMac_t* deviceMacAddr = WifiCommManagerGetDeviceMac ();
                    uint8_t k = 0;
                    for (k = 0; k < sizeof (peripheralInfo->peripheral.MACAddress); k++) {
                        if (peripheralInfo->peripheral.MACAddress[k] == deviceMacAddr->MacAddr[k]) {
                            if ((k + 1) == sizeof (peripheralInfo->peripheral.MACAddress)) {
                                PeripheralExist = dxTRUE;
                            }
                        } else {
                            break;
                        }
                    }
                }

            } else if (eventHeader.SubType == DKJOBTYPE_NONE) {

                //Just make sure we are still in the same state and its not about to change
                if (SimpleStateMachine_StateNotChangingWhileEqualTo (RegisterSubStateHandler, REGISTER_SUBSTATE_GET_PERIPHERAL)) {
                    if (GatewayExist) {

                        LedManager_result_t LedManagerRetCode = SetLedModeCondition (LED_OPERATION_MODE_NORMAL, LED_CONDITION_NONE);
                        if (LedManagerRetCode != LED_MANAGER_SUCCESS) {
                            G_SYS_DBG_LOG_IV (
                                G_SYS_DBG_CATEGORY_ONLINE,
                                G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "WARNING:(Line=%d) LedManager Failed SetLedModeCondition (%d)\r\n",
                                __LINE__,
                                LedManagerRetCode
                            );
                        }
#if DEVICE_IS_HYBRID
                        if (!PeripheralExist) {
                            cUtlAddHybridPeripheral ();
                        } else {
#endif
                            SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_FINISH);
                            if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                                G_SYS_DBG_LOG_IV (
                                    G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                    "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_FINISH (%d)\r\n",
                                    __LINE__,
                                    SmpResult
                                );
                            }
#if DEVICE_IS_HYBRID
                        }
#endif
                    } else {

                        //Gateway not exist
                        // Todo Enter ERROR SubState
                        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_ERROR);
                        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS) {
                            G_SYS_DBG_LOG_IV (
                                G_SYS_DBG_CATEGORY_REGISTER,
                                G_SYS_DBG_LEVEL_FATAL_ERROR,
                                "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_ERROR (%d)\r\n",
                                __LINE__,
                                SmpResult
                            );
                        }

                    }
                }
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#if MQTT_IS_ENABLED
StateMachineEvent_State_t Register_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Register_EventHpManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_RES:
        {
            if (pHpCmdMessage->PayloadData && pHpCmdMessage->PayloadSize)
            {
                if (pHpCmdMessage->MessageID == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                {
                    HpProductionSelfTestReportRes_t* pProductionSelfTestReport = (HpProductionSelfTestReportRes_t*)pHpCmdMessage->PayloadData;
                    uint8_t DataTransferReportMsg = DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_FAILURE;
                    if (pProductionSelfTestReport->Status == HP_PRODUCTION_SELF_TEST_SUCCESS)
                    {
                        DataTransferReportMsg = DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_SUCCESS;
                    }

                    BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DataTransferReportMsg,
                                                                                                                (uint8_t*)pHpCmdMessage->PayloadData, pHpCmdMessage->PayloadSize,
                                                                                                                1000,
                                                                                                                0);

                    if (BleManagerResult != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_RES failed to send with errorcode = %d\r\n",
                                         __LINE__,
                                         BleManagerResult);
                    }
                }
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }

        }
        break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Register_EventWifiManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case WIFI_COMM_EVENT_COMMUNICATION:
        {
            switch (EventHeader.ResultState)
            {
                case WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID:
                case WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED:
                case WIFI_COMM_MANAGER_WRONG_PASSWORD:
                case WIFI_COMM_MANAGER_PASSWORD_REQUIRED:
                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED_BUT_UNABLE_TO_REACH_WWW:
                {
                    //We dont do anything here, let the sub state handles it. Getting here is most likely
                    //user made some setting changes in their AP since last wifi setup.
                    //TODO: maybe we can have some mechanism to provide data feedback to the sub state machine for other handling
                    //      or maybe its not needed and we handles it here.
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) NOTE: Unable to connect to Wifi (ResultState = %d)\r\n",
                                     __LINE__,
                                     EventHeader.ResultState);

                    // Switch sub state to reconnect Wifi!
                    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
                    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_CONNECTING (%d)\r\n",
                                         __LINE__,
                                         SmpResult);
                    }
                }
                break;

                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED:
                {
                    dxIP_Addr_t ConnectedIpAddress;

                    if (WifiCommManagerGetIpAddress(&ConnectedIpAddress) == WIFI_COMM_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Connected Local Addr is at %u.%u.%u.%u\r\n",
                                         (uint8_t)ConnectedIpAddress.v4[0],
                                         (uint8_t)ConnectedIpAddress.v4[1],
                                         (uint8_t)ConnectedIpAddress.v4[2],
                                         (uint8_t)ConnectedIpAddress.v4[3]);

                        //UdpCommManagerStart();        // Move to SubState_RegisterWifiIsConnected

                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "DKServerManager_GetCurrentState():%d\r\n", DKServerManager_GetCurrentState());

                        if (DKServerManager_GetCurrentState() != DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "FATAL:(Line=%d) DKServerManager_GetCurrentState():%d (SHOULD be %d)\r\n",
                                             __LINE__,
                                             DKServerManager_GetCurrentState(),
                                             DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED);

                            DKServerManager_State_ForceToSuspended();

                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "FATAL:(Line=%d) DKServerManager_GetCurrentState():%d (SHOULD be %d)\r\n",
                                             __LINE__,
                                             DKServerManager_GetCurrentState(),
                                             DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED);
                        }

                        DKServerManager_State_Resume();

                        //Switch sub state to Wifi Connected!
                        SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_IS_CONNECTED);
                        if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) SMP unable to switch to REGISTER_SUBSTATE_WIFI_IS_CONNECTED (%d)\r\n",
                                             __LINE__,
                                             SmpResult);
                        }

                    }
                    else
                    {
                        //Unable to obtain IP address, do nothing and let the sub state machine handles it automatically
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) Wifi connected but unable to obtain IP\r\n",
                                         __LINE__);
                    }
                }
                break;

                case WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER:
                case WIFI_COMM_MANAGER_CONNECTION_IS_DOWN:
                {
                    //Stop and suspend managers, let the sub state machine handles it automatically
                    UdpCommManagerStop();
                    DKServerManager_State_Suspend();

                }
                break;

                default:
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) Unhandled Wifi ResultState = %d\r\n",
                                     __LINE__,
                                     EventHeader.ResultState);
                }
                break;
            }

        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Register_EventUdpManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Register_EventKeyPadManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    //We will not be handling any key event here.

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Register_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}

//============================================================================
// Function
//============================================================================

static dxBOOL bInitialized;

StateMachine_result_t Register_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    bInitialized = dxFALSE;
    WiFiReConnect = dxTRUE;

    //We shut down BLE manager as we are about to restore peripheral
    //which we will update the device to BLE manager, we don't want BLE manager
    //scanning peripheral while updating the device list.
    BleManagerScanReportSuspend();

    //Restore all device information for offline mode
    //TODO: IMPORTANT
    //      1)Check if each time we call this function whenever we switch from server online
    //        to offline will cause any issues (eg, schedule and adv job will be reset/replaced which
    //        can cause re-triggering).
    //      2)See if we can simply call this when we come back from Idle or Setup, but make sure we cover all cases
    CentralOfflineSupportDeviceInfoRestore();

    LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_NORMAL);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedMode (ret error %d)\r\n",
                         __LINE__, LedManagerRetCode);
    }

#if DEVICE_IS_HYBRID
#if HP_BLE_DEVICE_KEEPERLIST_AUTO_UPDATE
    if (BleManagerGetDeviceKeeperList_Asynch(1000, SOURCE_OF_ORIGIN_HP, 0) != DEV_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "WARNING: FAILED to send BleManagerGetDeviceKeeperList_Asynch\r\n");
    }
#endif
#endif  //#if DEVICE_IS_HYBRID

    //RESUME BLEManager SCAN report by switching BLE Manager back to central role (we need to switch to central role just in case it was from previous role
    //before)
    BleManagerSetRoleToCentral();

    //Variable/Member clean up
    uint16_t i = 0;
    for (i = 0; i < MAX_TASK_INFO_STORAGE_SLOT; i++)
    {
        memset(&RegisterTaskInfoStorage[i], 0, sizeof(TaskInfo_t));
    }

    RegisterSubStateHandler = SimpleStateMachine_Init(OfflineSubStateProcessFuncTable);
    SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_IDLE);

    return result;
}

StateMachine_result_t Register_MainProcess(void* data )
{

    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (bInitialized == dxFALSE)
    {
        BLEManager_WhiteList_AddFromKeeperList();
        BleRepeater_UpdateWatchList ();
        BleManagerScanReportResume ();
        bInitialized = dxTRUE;
    }

    //Kick start the sub state if its IDLE
    if (SimpleStateMachine_GetCurrentState(RegisterSubStateHandler) == REGISTER_SUBSTATE_IDLE)
    {
        SimpleStateMachine_ExecuteState(RegisterSubStateHandler, REGISTER_SUBSTATE_WIFI_CONNECTING);
    }

    //Process the sub state machine
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_Process(RegisterSubStateHandler);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_REGISTER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SimpleStateMachine_Process failed (%d)\r\n",
                         __LINE__,
                         SmpResult);
    }

    return result;
}

StateMachine_result_t Register_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (RegisterSubStateHandler) {
        SimpleStateMachine_Uninit(RegisterSubStateHandler);
        RegisterSubStateHandler = NULL;
    }

    return result;
}

