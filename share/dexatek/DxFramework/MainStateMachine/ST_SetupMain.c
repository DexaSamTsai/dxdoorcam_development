//============================================================================
// File: ST_SetupMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "dxDeviceInfoManager.h"
#include "ST_CommonUtils.h"
#include "CentralStateMachineAutomata.h"

//============================================================================
// Macros
//============================================================================

#define PERIPHERAL_TO_CENTRAL_AUTO_SWITCH_TIME          (10*SECONDS)

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

dxTime_t                StateTimer = 0;
uint32_t                SystemPairingTimout = PERIPHERAL_TO_CENTRAL_AUTO_SWITCH_TIME;
static dxBOOL           WaitingForRegister;

//============================================================================
// Internal Function
//============================================================================
void IntSaveDefaultFirmwareInfo(void)
{
    SystemInfoReportObj_t SystemInfoContainer;

    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, RTK_WIFI_CURRENT_FW_VERSION, sizeof(RTK_WIFI_CURRENT_FW_VERSION), dxFALSE);
    if (DevInfoRet == DXDEV_INFO_SUCCESS)
    {
        BLEManager_result_t BleRet = BleManagerSystemInfoGet(&SystemInfoContainer);

        if (BleRet == DEV_MANAGER_SUCCESS)
        {
            if (BleManager_SBL_Enable())
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Ble Central in SBL mode (Ble Central should be in MAIN APPLICATION by default\r\n",
                                 __LINE__);
            }
            else
            {
                dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, SystemInfoContainer.Version, sizeof(SystemInfoContainer.Version), dxFALSE);

                if (DevInfoRet == DXDEV_INFO_SUCCESS)
                {

                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION Update failed (%d)\r\n",
                                     __LINE__,
                                     DevInfoRet);
                }
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) DX_DEVICE_INFO_TYPE_DEV_FW_VERSION Update failed (%d)\r\n",
                             __LINE__,
                             BleRet);
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) BleManagerSystemInfoGet() failed (%d)\r\n",
                         __LINE__,
                         DevInfoRet);
    }
}

static void IntBLEHostRemoteMessageProcess(uint8_t MessageType, uint8_t MessagePayloadLen, uint8_t* DataInTransferPayload)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Host Remove Message Arrived\r\n");
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "---------------------------\r\n");
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "Message type = 0x%x, PayloadLen = %d\r\n",
                     MessageType, MessagePayloadLen);

    switch (MessageType)
    {
        case DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_REPORT_REQ:
        {
            if (WifiCommManagerGetWifiConnectionStatus() != WIFI_COMM_CONNECTION_DOWN)
            {
                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_NOT_POSSIBLE,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) WIFI_COMM_MANAGER_SCAN_AP_REPORT failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);                }
            }
            else
            {
                WifiCommManager_result_t WifiCommManResult = WifiCommManagerScanSSIDTask_Asynch(SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL, 0);
                if (WifiCommManResult != WIFI_COMM_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) WifiCommManagerScanSSIDTask_Asynch failed to with errorcode = %d)\r\n",
                                     __LINE__,
                                     WifiCommManResult);
                }
            }

        }
        break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG:
        {
            //TODO: We need to change the DataTransferPayload content to ALLOW more flexibility so that
            //      AccessKey, Device Name, SSID and Password is NOT limited by current string len size!

            GatewayInfoConfig_t* DeviceInfoConfig = (GatewayInfoConfig_t*)DataInTransferPayload;

#if 0 //Enable if debugging is required
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Received GatewayDeviceName = %s\r\n",
                             DeviceInfoConfig->GatewayDeviceName);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Received GatewayAccessKey = %s\r\n",
                             DeviceInfoConfig->GatewayAccessKey);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Received latitude = %f\r\n",
                             (float)DeviceInfoConfig->latitude);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Received longitude = %f\r\n",
                             (float)DeviceInfoConfig->longitude);
#endif
            uint16_t DeviceDeviceNameLen = strlen(DeviceInfoConfig->GatewayDeviceName);
            uint16_t DeviceAccessKeyLen = strlen(DeviceInfoConfig->GatewayAccessKey);

            //Check the Integrity
            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_IntegrityCheck(dxTRUE);
            //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,"dxDevInfo_IntegrityCheck = %d\r\n",DevInfoRet);

            //Make sure the gateway name is limited to the allowed size
            DeviceDeviceNameLen = (DeviceDeviceNameLen >= sizeof(DeviceInfoConfig->GatewayDeviceName)) ? (sizeof(DeviceInfoConfig->GatewayDeviceName) - 1) : DeviceDeviceNameLen;

            if (DeviceDeviceNameLen)
            {
                DeviceInfoConfig->GatewayDeviceName[DeviceDeviceNameLen] = '\0';
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_NAME, DeviceInfoConfig->GatewayDeviceName, DeviceDeviceNameLen + 1, dxFALSE);
                //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,"dxDevInfo_Update DX_DEVICE_INFO_TYPE_DEV_NAME = %d\r\n",DevInfoRet);
            }

            if (DeviceAccessKeyLen)
            {
                DeviceInfoConfig->GatewayAccessKey[DeviceAccessKeyLen] = '\0';
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY, DeviceInfoConfig->GatewayAccessKey, DeviceAccessKeyLen + 1, dxFALSE);
                //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,"dxDevInfo_Update DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY = %d\r\n",DevInfoRet);
            }

            if (DeviceInfoConfig->latitude)
            {
                float latitude = (float)DeviceInfoConfig->latitude;
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LATITUDE, &latitude, sizeof(float), dxFALSE);
                //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, "dxDevInfo_Update DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LATITUDE = %d\r\n", DevInfoRet);

                DeviceInfoConfig->latitude = latitude; //Send back the actual latitude after conversion to float
             }

            if (DeviceInfoConfig->longitude)
            {
                float longitude = (float)DeviceInfoConfig->longitude;
                DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LONGITUDE, &longitude, sizeof(float), dxFALSE);
                //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,"dxDevInfo_Update DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LONGITUDE = %d\r\n",DevInfoRet);

                DeviceInfoConfig->longitude = longitude; //Send back the actual longitude after conversion to float
            }

            RegisterState_t rs_ret = cUtlCheckRegisterStateGet();
            if ( rs_ret == REGISTER_STATE_NOT_EXIST ) {
                //normal flow
                int32_t RegisterState = REGISTER_STATE_NONE;
                dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
                if (DevInfoRet != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                 "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                                 __LINE__,
                                                 DevInfoRet);
                }
            } else {
                //abnormal flow: set RegisterState to REGISTER_STATE_REGISTERING
                int32_t RegisterState = REGISTER_STATE_REGISTERING;
                dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update( DX_DEVICE_INFO_TYPE_REGISTER_STATE, &RegisterState, sizeof(RegisterState), dxFALSE );
                if (DevInfoRet != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV( G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                 "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_REGISTER_STATE failed (%d)\r\n",
                                                 __LINE__,
                                                 DevInfoRet);
                }
            }

            //Save default firmware information to DevInfo
            IntSaveDefaultFirmwareInfo();

            DevInfoRet = dxDevInfo_SaveToDisk();
            //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,"dxDevInfo_SaveToDisk = %d\r\n",DevInfoRet);

            //Send back the user config info with success event
            BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG_SUCCESS,
                                                                                                        (uint8_t*)DeviceInfoConfig, sizeof(GatewayInfoConfig_t),
                                                                                                        1000,
                                                                                                        0);

            if (BleManagerResult != DEV_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_GATEWAY_INFO_CONFIG failed to send with errorcode = %d\r\n",
                                 __LINE__,
                                 BleManagerResult);
            }

            WaitingForRegister = dxTRUE;
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE:
        case DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE_EXT:
        {

            WifiConfigSimpleExt_t WifiConfig;

            memset (&WifiConfig, 0, sizeof (WifiConfigSimpleExt_t));
            if (MessageType == DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SIMPLE) {
                WifiConfigSimple_t* config = (WifiConfigSimple_t*)DataInTransferPayload;
                memcpy (WifiConfig.SSID, config->SSID, sizeof (char) * 32);
                memcpy (WifiConfig.Password, config->Password, sizeof (char) * 32);
                memcpy (&WifiConfig.SecyrityType, &config->SecyrityType, sizeof (int32_t));
            } else {
                WifiConfigSimpleExt_t* config = (WifiConfigSimpleExt_t*)DataInTransferPayload;
                memcpy (WifiConfig.SSID, config->SSID, sizeof (char) * 32);
                memcpy (WifiConfig.Password, config->Password, sizeof (char) * 128);
                memcpy (&WifiConfig.SecyrityType, &config->SecyrityType, sizeof (int32_t));
            }

            if (WifiCommManagerGetWifiConnectionStatus() != WIFI_COMM_CONNECTION_DOWN)
            {
                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_NOT_POSSIBLE_WHILE_CONNECTION_IS_UP,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) WIFI_COMM_MANAGER_SCAN_AP_REPORT failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
            else
            {
                char* SSID =  (WifiConfig.SSID[0] != NULL_STRING) ? WifiConfig.SSID:NULL;

                if (SSID)
                {
                    uint16_t SSIDLen =  (strlen(WifiConfig.SSID) >= sizeof(WifiConfig.SSID)) ? (sizeof(WifiConfig.SSID) - 1) : strlen(WifiConfig.SSID);
                    char* Password = (WifiConfig.Password[0] != NULL_STRING) ? WifiConfig.Password:NULL;

                    //Check the Integrity
                    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_IntegrityCheck(dxTRUE);
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "INFORMATION:(Line=%d) dxDevInfo_IntegrityCheck = %d\r\n",
                                     __LINE__,
                                     DevInfoRet);

                    int32_t SecurityType = APSecurityTypeTranslateFromAppToDx(WifiConfig.SecyrityType);
                    DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE, &SecurityType, sizeof(int32_t), dxFALSE);
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "INFORMATION:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE = %d\r\n",
                                     __LINE__,
                                     DevInfoRet);

                    WifiConfig.SSID[SSIDLen] = '\0';
                    DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID, WifiConfig.SSID, SSIDLen + 1, dxFALSE);
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "INFORMATION:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID = %d\r\n",
                                     __LINE__,
                                     DevInfoRet);

                    uint16_t PasswordLen = 0;
                    if (Password)
                    {
                        PasswordLen = (strlen(WifiConfig.Password) >= sizeof(WifiConfig.Password)) ? (sizeof(WifiConfig.Password) - 1) : strlen(WifiConfig.Password);

                        WifiConfig.Password[PasswordLen] = '\0';
                    }
                    else
                    {
                        PasswordLen = 0; //Should be 0 as we will always pad additional byte for null string terminator
                        WifiConfig.Password[0] = '\0';
                    }

                    DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD, WifiConfig.Password, PasswordLen + 1, dxFALSE);
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "INFORMATION:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD = %d\r\n",
                                     __LINE__,
                                     DevInfoRet);

                    //Check the Integrity
                    DevInfoRet = dxDevInfo_IntegrityCheck(dxTRUE);
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "dxDevInfo_IntegrityCheck = %d\r\n",
                                     DevInfoRet);

                    if (SecurityType == DXWIFI_SECURITY_FORCE_32_BIT)
                    {
                        WifiCommManagerConnectToSSIDTask_Asynch(SSID, SSIDLen, WifiConfig.Password, strlen(WifiConfig.Password), SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL, 0);
                    }
                    else
                    {
                        //This is hidden SSID so we need to use direct method
                        WifiCommManagerConnectToSSIDDirectTask_Asynch(SSID, SSIDLen, WifiConfig.Password, strlen(WifiConfig.Password), SecurityType, SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL, 0);
                    }
                }
                else
                {
                    BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_INVALID_PARAM,
                                                                                                                NULL, 0,
                                                                                                                1000,
                                                                                                                0);

                    if (BleManagerResult != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG failed to send with errorcode = %d\r\n",
                                         __LINE__,
                                         BleManagerResult);
                    }
                }
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_TURN_DOWN_REQ:
        {

            if (WifiCommManagerGetWifiConnectionStatus() != WIFI_COMM_CONNECTION_DOWN)
            {

                DKServerManager_StateMachine_State_t DkServerState = DKServerManager_GetCurrentState();

                if (DkServerState == DKSERVERMANAGER_STATEMACHINE_STATE_SUSPENDED)
                {
                    //NOTE: DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN will be sent after connection is turned down, so we don't need to send it here
                    WifiCommManagerDisconnectCurrentConnectionTask_Asynch(SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL, 0);
                }
                else
                {
                    BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN_NOT_POSSIBLE_AT_THIS_MOMENT,
                                                                                                                NULL, 0,
                                                                                                                1000,
                                                                                                                0);

                    if (BleManagerResult != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN_NOT_POSSIBLE_AT_THIS_MOMENT failed to send with errorcode = %d\r\n",
                                         __LINE__,
                                         BleManagerResult);
                    }
                }
            }
            else //Connection is already down
            {
                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN failed to send with errorcode = %d\\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REQ:
        {

            const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

            if (DeviceMac == NULL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) FATAL: WifiCommManagerGetDeviceMac MAC is NULL\r\n",
                                 __LINE__);
            }

            BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REPORT,
                                                                                                        (uint8_t*)DeviceMac, sizeof(WifiCommDeviceMac_t),
                                                                                                        1000,
                                                                                                        0);

            if (BleManagerResult != DEV_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_GATEWAY_ADDRESS_REQ failed to send with errorcode = %d\r\n",
                                 __LINE__,
                                 BleManagerResult);
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REQ:
        {
            WifiCommDeviceMac_t *NewDeviceMac = (WifiCommDeviceMac_t *)DataInTransferPayload;

            if (NewDeviceMac == NULL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) FATAL: DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REQ MAC is NULL\r\n",
                                 __LINE__);
            }
            else
            {
                WifiCommDeviceMac_t IncorrectMac = { {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF} };
                dxBSP_RET_CODE ret = dxPLATFORM_SetMAC((DeviceMac_t *)NewDeviceMac, (DeviceMac_t *)NewDeviceMac);

                if (ret != DXBSP_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) FATAL: dxPLATFORM_SetMAC() failed with errorcode = %d\r\n",
                                     __LINE__,
                                     ret);

                    NewDeviceMac = &IncorrectMac;
                }
                else
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     ">>> dxPLATFORM_SetMAC() <<<\r\n");
                }


                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REPORT,
                                                                                                            (uint8_t*)NewDeviceMac, sizeof(WifiCommDeviceMac_t),
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_GATEWAY_CONFIG_ADDRESS_REQ failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_REQ:
        {
#if DEVICE_IS_HYBRID
            HpProductionSelfTestReportReq_t *pSelfTestReqPayload = (HpProductionSelfTestReportReq_t *)DataInTransferPayload;

            if (pSelfTestReqPayload == NULL)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) FATAL: DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_REQ PAYLOAD is NULL\r\n",
                                 __LINE__);
            }
            else
            {
                uint32_t SelfTestMessageSize = sizeof(HpProductionSelfTestReportReq_t) + pSelfTestReqPayload->DataLen;
                HpProductionSelfTestReportReq_t *pSelfTestMessage = (HpProductionSelfTestReportReq_t *)dxMemAlloc("SelfTestMsg", 1, SelfTestMessageSize);
                HpCmdMessageQueue_t* pCmdMessage = (HpCmdMessageQueue_t *)dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));

                if (pSelfTestMessage && pCmdMessage)
                {
                    // Duplicate pSelfTestReqPayload
                    memcpy((void *)pSelfTestMessage, pSelfTestReqPayload, SelfTestMessageSize);

                    pCmdMessage->CommandMessage = HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_REQ;
                    pCmdMessage->MessageID = SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL;
                    pCmdMessage->Priority = 0; //Normal
                    pCmdMessage->PayloadSize = SelfTestMessageSize;
                    pCmdMessage->PayloadData = (uint8_t *)pSelfTestMessage;

                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                     "HP_CMD_DATAEXCHANGE_SERVER_READY SO=%d, TI=%d\r\n", 0, pCmdMessage->MessageID);

                    HpManagerSendTo_Client(pCmdMessage);
                }
                else
                {
                    if (pSelfTestMessage)
                    {
                        dxMemFree(pSelfTestMessage);
                        pSelfTestMessage = NULL;
                    }
                }

                HpManagerCleanEventArgumentObj(pCmdMessage);
            }
#endif // DEVICE_IS_HYBRID
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_GATEWAY_EXIT_SETUP_MODE:
        {
            if ( WaitingForRegister == dxTRUE ) {
                CentralStateMachine_ChangeState( STATE_REGISTER, NULL, 0 );
            } else {
                CentralStateMachine_ChangeState( STATE_IDLE, NULL, 0 );
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_REQ:
        {
            if (WifiCommManagerGetWifiConnectionStatus() != WIFI_COMM_CONNECTION_UP)
            {
                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_NOT_POSSIBLE,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_NOT_POSSIBLE failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
            else
            {
                int32_t rssi;
                rssi = (int32_t)WifiCommManagerGetWifiRSSI();

                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI,
                                                                                                            (uint8_t*)&rssi, sizeof(int32_t),
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_CONNECTED_WIFI_RSSI_NOT_POSSIBLE failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
        }
        break;

        case DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_REQ:
        {
            //Just a precausion check in case this is called by mistake
            //WARNING!!! - DO NOT Remove this as its NECESSARY for gateway Production tool (will need to clear DCT after production test)
            uint32_t* pPassPhrase = (uint32_t*)DataInTransferPayload;
            if (*pPassPhrase == 80503525)
            {
                dxDevInfo_CleanDisk();

                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_SUCCESS,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_SUCCESS failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
            else
            {
                BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_DENIED,
                                                                                                            NULL, 0,
                                                                                                            1000,
                                                                                                            0);

                if (BleManagerResult != DEV_MANAGER_SUCCESS)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING:(Line=%d) DATA_TRANSFER_MSG_TYPE_CLEAR_GATEWAY_INFO_SUCCESS failed to send with errorcode = %d\r\n",
                                     __LINE__,
                                     BleManagerResult);
                }
            }
        }
        break;

        default:
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) IntBLEHostRemoteMessageProcess unknown messagetype\r\n",
                             __LINE__);
        break;
    }
}

//============================================================================
// Util Function
//============================================================================

void UtilSetPeripheralRoleCustomAdvertisementData(void)
{

#if DEVICE_IS_HYBRID

    //DO NOT CHANGE - Static Information
    {
        uint8_t CustomAdvData[8];
        CustomAdvData[0] = 0xD1; //CompanyID LOW BYTE
        CustomAdvData[1] = 0xD0; //CompanyID HIGH BYTE
        CustomAdvData[2] = DK_PRODUCT_TYPE_GATEWAY_PERIPHERAL_ID; //Product ID - 0x20 is Generic Wifi Setup device with BLE Bridge
        CustomAdvData[3] = 0x00; //Payload Check Sum
        CustomAdvData[4] = 0x00; //FLAG Low Byte
        CustomAdvData[5] = 0x00; //FLAG High Byte
        CustomAdvData[6] = DK_DATA_TYPE_IMPLICIT_DEV; //Data Type - Implicit Device Type
        CustomAdvData[7] = HYBRID_PERIPHERAL_PRODUCT_ID;

        uint8_t         XorSum = CustomAdvData[6];
        XorSum ^= CustomAdvData[7];
        CustomAdvData[3] = XorSum;

        BleManagerPeripheralRoleSetAdvManufactureCustomData_Asynch(CustomAdvData, sizeof(CustomAdvData));
    }

#endif

}


//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t Setup_EventBLEManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_PERIPHERAL_ROLE_DATA_IN_TRANSFER_PAYLOAD:
        {
            static uint8_t* MessagePayloadCollectionBuffer = NULL;
            static uint8_t* pMessagePayloadCollectionBufferWorkPtr = NULL;
            static uint8_t  MessageType;
            static uint8_t  MessagePayloadLen;
            static uint8_t  MessagePayloadLenLeft;
            dxBOOL          MessageCleanup = dxFALSE;

            if (EventHeader.ResultState == DEV_MANAGER_TASK_SUCCESS)
            {
                uint8_t* DataInTransferPayload = BleManagerParsePeripheralRoleDataInPayloadReportEventObj(arg);

                uint16_t PayloadPreamble = BUILD_UINT16(DataInTransferPayload[0], DataInTransferPayload[1]);

                if (PayloadPreamble == DATA_TRANSFER_PREAMBLE)
                {
                    if (MessagePayloadCollectionBuffer)
                    {
                        dxMemFree(MessagePayloadCollectionBuffer);
                        MessagePayloadCollectionBuffer = NULL;
                        pMessagePayloadCollectionBufferWorkPtr = NULL;
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) Peripheral Role Message In was not fully received before new message is found!\r\n",
                                         __LINE__);
                    }

                    MessageType = DataInTransferPayload[2];
                    MessagePayloadLen = DataInTransferPayload[3];

                    if (MessagePayloadLen)
                    {
                        if (MessagePayloadLen > (DATA_TRANSFER_SIZE_PER_CHUNK - 4))
                        {
                            MessagePayloadCollectionBuffer = dxMemAlloc( "Peripheral Role Payload collection Buff", 1, MessagePayloadLen);

                            pMessagePayloadCollectionBufferWorkPtr = MessagePayloadCollectionBuffer;
                            memcpy(pMessagePayloadCollectionBufferWorkPtr, &DataInTransferPayload[4], (DATA_TRANSFER_SIZE_PER_CHUNK - 4));
                            pMessagePayloadCollectionBufferWorkPtr += (DATA_TRANSFER_SIZE_PER_CHUNK - 4);
                            MessagePayloadLenLeft = MessagePayloadLen - (DATA_TRANSFER_SIZE_PER_CHUNK - 4);
                        }
                    }

                    //We don't need to collect more payload, this is all for this message so we process it
                    if (MessagePayloadCollectionBuffer == NULL)
                    {
                        IntBLEHostRemoteMessageProcess(MessageType, MessagePayloadLen, &DataInTransferPayload[4]);
                    }
                }
                else if (MessagePayloadCollectionBuffer)
                {
                    uint8_t SizeToCopy = (MessagePayloadLenLeft > DATA_TRANSFER_SIZE_PER_CHUNK) ? DATA_TRANSFER_SIZE_PER_CHUNK:MessagePayloadLenLeft;

                    memcpy( pMessagePayloadCollectionBufferWorkPtr,
                            &DataInTransferPayload[0],
                            SizeToCopy);

                    if (MessagePayloadLenLeft >= SizeToCopy)
                    {
                        MessagePayloadLenLeft -= SizeToCopy;
                        if (MessagePayloadLenLeft)
                        {
                            pMessagePayloadCollectionBufferWorkPtr += SizeToCopy;
                        }
                        else
                        {
                            IntBLEHostRemoteMessageProcess(MessageType, MessagePayloadLen, MessagePayloadCollectionBuffer);
                            MessageCleanup = dxTRUE;
                        }
                    }
                    else
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) Peripheral Role Data Transfer MessagePayloadLenLeft is less than SizeToCopy\r\n",
                                         __LINE__);
                        MessageCleanup = dxTRUE;
                    }
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) Peripheral Role Data Transfer error, message received in progress will be erased!\r\n",
                                 __LINE__);
                MessageCleanup = dxTRUE;
            }

            if (MessageCleanup == dxTRUE)
            {
                if (MessagePayloadCollectionBuffer)
                    dxMemFree(MessagePayloadCollectionBuffer);

                MessagePayloadCollectionBuffer = NULL;
                pMessagePayloadCollectionBufferWorkPtr = NULL;
                MessageType = 0;
                MessagePayloadLen = 0;
                MessagePayloadLenLeft = 0;
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Setup_EventJobManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Setup_EventServerManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    switch (EventHeader.MessageType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t Setup_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Setup_EventHpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        case HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_RES:
        {
            if (pHpCmdMessage->PayloadData && pHpCmdMessage->PayloadSize)
            {
                if (pHpCmdMessage->MessageID == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                {
                    HpProductionSelfTestReportRes_t* pProductionSelfTestReport = (HpProductionSelfTestReportRes_t*)pHpCmdMessage->PayloadData;
                    uint8_t DataTransferReportMsg = DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_FAILURE;
                    if (pProductionSelfTestReport->Status == HP_PRODUCTION_SELF_TEST_SUCCESS)
                    {
                        DataTransferReportMsg = DATA_TRANSFER_MSG_TYPE_GATEWAY_HYBRIDPERIPHERAL_PRODUCTION_SELF_TEST_RES_SUCCESS;
                    }

                    BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DataTransferReportMsg,
                                                                                                                (uint8_t*)pHpCmdMessage->PayloadData, pHpCmdMessage->PayloadSize,
                                                                                                                1000,
                                                                                                                0);

                    if (BleManagerResult != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) HP_CMD_SYSTEM_PRODUCTION_SELF_TEST_REPORT_RES failed to send with errorcode = %d\r\n",
                                         __LINE__,
                                         BleManagerResult);
                    }
                }
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }

        }
        break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Setup_EventWifiManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case WIFI_COMM_EVENT_COMMUNICATION:
        {
            switch (EventHeader.ResultState)
            {
                case WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID:
                {
                    if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                    {
                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SSID_NOT_FOUND,
                                                                                                                    NULL, 0,
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             BleManagerResult);
                        }
                    }
                }
                break;

                case WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED:
                case WIFI_COMM_MANAGER_WRONG_PASSWORD:
                case WIFI_COMM_MANAGER_PASSWORD_REQUIRED:
                {
                    if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                    {
                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_FAILED,
                                                                                                                    NULL, 0,
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER Encryption/Password (%d) failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             EventHeader.ResultState,
                                             BleManagerResult);
                        }
                    }
                }
                break;

                case WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED:
                {

                    if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                    {
                        dxWiFi_Security_t* pWifiSecurityType = WifiCommManagerParseCommEstablishEventObj(arg);

                        if (pWifiSecurityType)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                             "INFO:(Line=%d) WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED with SecurityType (0x%X)\r\n",
                                             __LINE__,
                                             *pWifiSecurityType);

                            //Update it if we do get known security type
                            dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE, pWifiSecurityType, sizeof(dxWiFi_Security_t), dxFALSE);
                            if (DevInfoRet != DXDEV_INFO_SUCCESS)
                            {
                                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                                 "WARNING:(Line=%d) dxDevInfo_Update DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE failed (%d)\r\n",
                                                 __LINE__,
                                                 DevInfoRet);
                            }
                        }

                        //Success - Let's save the info back to disk
                        dxDevInfo_SaveToDisk();

                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_SUCCESS,
                                                                                                                    NULL, 0,
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             BleManagerResult);
                        }
                    }

                }
                break;

                case WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER:
                case WIFI_COMM_MANAGER_CONNECTION_IS_DOWN:
                {
                    if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                    {
                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONNECTION_DOWN,
                                                                                                                    NULL, 0,
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER_CONNECTION_IS_DOWN failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             BleManagerResult);
                        }

                    }
                }
                break;

                default:
                {
                    if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
                    {

                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_CONFIG_FAILED,
                                                                                                                    NULL, 0,
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER unhandled event (%d) failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             EventHeader.ResultState,
                                             BleManagerResult);
                        }

                        if (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_SSID_TIME_OUT)
                        {
                            //NOTE: If WIFI_COMM_MANAGER_SCAN_SSID_TIME_OUT, we should consider self system reset
                            //      Although this seems to be brute force BUT if we leave it as is then we have NO way of recovering it.
                            //      Also we could have this handled from the main loop BUT since this is mainly done while user's phone is connected to Gateway via
                            //      BLE so if user tries to do re-scan it will always fails.. So by reseting the system the user will simply see the scan view
                            //      disapear (because BLE connection is lost due to system reset), and can be recover easily by trying it again.
                            //      Here we switch to FATAL state and let it handles it.
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_STATE_MANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "[%s] @%d\r\n", __FUNCTION__, __LINE__ );
                            CentralStateMachine_ChangeState(STATE_FATAL, NULL, 0);
                        }

                    }

                }
                break;
            }

        }
        break;

        case WIFI_COMM_EVENT_SCAN_AP:
        {
            if (EventHeader.SourceOfOrigin == SOURCE_OF_ORIGIN_PERIPHERAL_CONTROL)
            {
                if (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_REPORT)
                {
                    dxWiFi_Scan_Details_t* ApInfo = WifiCommManagerParseScanApReportEventObj(arg);
                    if ((ApInfo) && (ApInfo->SSID.val[0] != NULL_STRING))
                    {
                        ScanApInfo_t TranslatedApInfo;
                        memset(&TranslatedApInfo, 0, sizeof(ScanApInfo_t));
                        memcpy(TranslatedApInfo.SSID, ApInfo->SSID.val, ApInfo->SSID.len);

                        TranslatedApInfo.signal_strength = ApInfo->signal_strength;
                        TranslatedApInfo.band = ApInfo->band;

                        //Translate the access point security FROM Wiced TO Dk
                        TranslatedApInfo.security = APSecurityTypeTranslateFromDxToApp(ApInfo->security);

                        BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_REPORT,
                                                                                                                    (uint8_t*)&TranslatedApInfo, sizeof(TranslatedApInfo),
                                                                                                                    1000,
                                                                                                                    0);

                        if (BleManagerResult != DEV_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING:(Line=%d) WIFI_COMM_MANAGER_SCAN_AP_REPORT failed to send with errorcode = %d\r\n",
                                             __LINE__,
                                             BleManagerResult);
                        }
                    }

                }
                else if (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_REPORT_ENDED)
                {
                    BLEManager_result_t BleManagerResult = BleManagerPeripheralRoleDataOutTransferTask_Asynch(  DATA_TRANSFER_MSG_TYPE_WIFI_SCAN_AP_REPORT_DONE,
                                                                                                                NULL, 0,
                                                                                                                1000,
                                                                                                                0);

                    if (BleManagerResult != DEV_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) WIFI_COMM_MANAGER_SCAN_AP_REPORT_ENDED failed to send with errorcode = %d\r\n",
                                         __LINE__,
                                         BleManagerResult);
                    }

                }
                else if ((EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_TIME_OUT) || (EventHeader.ResultState == WIFI_COMM_MANAGER_SCAN_AP_INTERNAL_ERROR))
                {
                    //NOTE: If WIFI_COMM_MANAGER_SCAN_SSID_TIME_OUT, we should consider self system reset
                    //      Although this seems to be brute force BUT if we leave it as is then we have NO way of recovering it.
                    //      Also we could have this handled from the main loop BUT since this is mainly done while user's phone is connected to Gateway via
                    //      BLE so if user tries to do re-scan it will always fails.. So by reseting the system the user will simply see the scan view
                    //      disapear (because BLE connection is lost due to system reset), and can be recover easily by trying it again.
                    //      Here we switch to FATAL state and let it handles it.
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_STATE_MANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "[%s] @%d\r\n", __FUNCTION__, __LINE__ );
                    CentralStateMachine_ChangeState(STATE_FATAL, NULL, 0);
                }
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Setup_EventUdpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Setup_EventKeyPadManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    //We will not be handling any key event here.

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Setup_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

StateMachine_result_t Setup_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_SETUP);
    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) LedManager Failed to SetLedMode (ret error %d)\r\n",
                         __LINE__, LedManagerRetCode);
    }

    WaitingForRegister	= dxFALSE;

    DKServerManager_State_Suspend();

    UdpCommManagerStop();

    BleManagerSetRoleToPeripheral();

    UtilSetPeripheralRoleCustomAdvertisementData();

    StateTimer = dxTimeGetMS();

    return result;
}

StateMachine_result_t Setup_MainProcess(void* data )
{
    StateMachine_result_t   result = STATE_MACHINE_RET_SUCCESS;

    BLEManager_States_t BleManagerState = BleManagerCurrentState();

    if ((BleManagerState == DEV_MANAGER_PERIPHERAL_STATE_IDLE) || (BleManagerState == DEV_MANAGER_PERIPHERAL_STATE_ADVERTISING))
    {
        if (cUtlIsSysTimeGreaterThan(StateTimer, SystemPairingTimout) == dxTRUE)
        {
            LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_INIT);
            if (LedManagerRetCode != LED_MANAGER_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_SETUP, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING:(Line=%d) LedManager Failed to SetLedMode (ret error %d)\r\n",
                                 __LINE__, LedManagerRetCode);
            }

            CentralStateMachine_ChangeState(STATE_IDLE, NULL, 0);
        }

    }
    else if (BleManagerState == DEV_MANAGER_PERIPHERAL_STATE_CONNECTED)
    {
        //Update PeripheralStateTimer to current.
        StateTimer = dxTimeGetMS();
    }

    return result;
}

StateMachine_result_t Setup_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    //We break the loop if wifi manager is still in the process of searching SSID
    WifiCommManagerBreakSearchInProgressTask_Asynch();

    //Suspend BLE Manager and let the state handles it.
    BleManagerSuspend();

    return result;
}


