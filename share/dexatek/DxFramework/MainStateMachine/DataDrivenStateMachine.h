//============================================================================
// File: DataDrivenStateMachine.h
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef __DATA_DRIVEN_STATE_MACHINE_H__
#define __DATA_DRIVEN_STATE_MACHINE_H__

//============================================================================
//    Includes
//============================================================================

#include "dxOS.h"



//============================================================================
//    Definations
//============================================================================

#define DATA_DRIVEN_STATE_INVALID        0xFFFF



//============================================================================
//    Types
//============================================================================

//--- Enumerations -----------------------------------------------------------

typedef enum {

    DATA_DRIVEN_STATE_MACHINE_RET_SUCCESS                           = 0,
    DATA_DRIVEN_STATE_MACHINE_RET_NOT_ALLOWED_ST_CHANGE_INPROCESS,
    DATA_DRIVEN_STATE_MACHINE_RET_NOT_SUPPORTED,

    DATA_DRIVEN_STATE_MACHINE_RET_ERROR                             = -1,
    DATA_DRIVEN_STATE_MACHINE_RET_STATE_CODE_NOT_VALID              = -2,

} DataDrivenStateMachineResult;


//--- Structures -------------------------------------------------------------

typedef DataDrivenStateMachineResult (*DataDrivenStateFunction)( void* data, dxBOOL isFirstCall);


typedef struct {
    uint16_t                    code;
    DataDrivenStateFunction     function;
} DataDrivenStateMachineTable;


typedef void* DataDrivenStateHandler;


//--- Classes ----------------------------------------------------------------

/**
 * The Circle Class, inherit Shape Class
 */
typedef struct _DataDrivenStateMachine {

    /**
     * This function should be called from the data thread at data arrival
     *nextExecuteState
     * @return DataDrivenStateMachineResult
     */
    DataDrivenStateMachineResult (*process) (
        struct _DataDrivenStateMachine* stateMachine,
        void* data
    );


    /**
     * This function should be called from the data thread at data arrival
     * but this will be execute once and return back to old state
     *
     * @return DataDrivenStateMachineResult
     */
    DataDrivenStateMachineResult (*processOnce) (
        struct _DataDrivenStateMachine* stateMachine,
        uint16_t stateToExecute,
        void* data
    );


    /**
     * Change/Execute state machine, NOTE: If StateCodeToChange is same as current state the state function will not execute again
     *
     * @param[in]  StateCodeToChange     :    The state to change to, make sure the state code is at least a valid value from the given table
     *                                      during the initialization.
     *
     * @return DataDrivenStateMachineResult
     */
    DataDrivenStateMachineResult (*changeState) (
        struct _DataDrivenStateMachine* stateMachine,
        uint16_t stateToChange
    );


    /**
     * Get current state of state machine
     *
     * @return uint16_t The state code of current state
     */
    uint16_t (*getCurrentState) (
        struct _DataDrivenStateMachine* stateMachine
    );


    /**
     * Check if state machine is not about to change while it is equal to state
     *
     * @param[in]  EqualToStateCode      :    The state code to check if its equal to current state
     * @return uint16_t The state code of current state
     */
    dxBOOL (*stateNotChangingWhileEqualTo) (
        struct _DataDrivenStateMachine* stateMachine,
        uint16_t stateToCompare
    );


    DataDrivenStateMachineTable*    tables;
    uint16_t                        currentState;
    uint16_t                        nextState;

} DataDrivenStateMachine;



//============================================================================
//    Public methods
//============================================================================

/**
 * Data driven state machine constructor
 */
DataDrivenStateMachine* DataDrivenStateMachine_create (
    DataDrivenStateMachineTable* sourceTables
);


/**
 * Data driven state machine destructor
 */
void DataDrivenStateMachine_destroy (
    DataDrivenStateMachine* stateMachine
);

#endif
