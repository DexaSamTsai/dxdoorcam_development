//============================================================================
// File: ST_IdleMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_IDLE_MAIN_H
#define _ST_IDLE_MAIN_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t Idle_Init(void* data );
StateMachine_result_t Idle_MainProcess(void* data );
StateMachine_result_t Idle_End(void* data );
StateMachineEvent_State_t Idle_EventBLEManager(void* data );
StateMachineEvent_State_t Idle_EventServerManager(void* data );
StateMachineEvent_State_t Idle_EventMqttManager(void* data );
#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Idle_EventHpManager(void* data );
#endif //#if DEVICE_IS_HYBRID
StateMachineEvent_State_t Idle_EventWifiManager(void* data );
StateMachineEvent_State_t Idle_EventUdpManager(void* data );
StateMachineEvent_State_t Idle_EventJobManager(void* data );
StateMachineEvent_State_t Idle_EventKeyPadManager(void* data );
StateMachineEvent_State_t Idle_EventLEDManager(void* data );


#endif // _ST_IDLE_MAIN_H
