//============================================================================
// File: ST_FatalMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "CentralStateMachineAutomata.h"
#include "ST_CommonUtils.h"
#include "SimpleTimedStateMachine.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

#define FATAL_DELAY_SECONDS_TO_REBOOT                         (10*SECONDS)

//============================================================================
// Enum
//============================================================================
typedef enum
{
    FATAL_SUBSTATE_IDLE             = 0,
    FATAL_SUBSTATE_RESET,

} Fatal_SubState_Code_t;

//============================================================================
// Structure
//============================================================================

//============================================================================
// Function Definition
//============================================================================

static SimpleStateMachine_result_t SubState_FatalReset(void* data, dxBOOL isFirstCall);

//============================================================================
// Global Var
//============================================================================
SimpleStateHandler      FatalSubStateHandler = NULL;

static simple_st_machine_table_t FatalSubStateProcessFuncTable[] =
{
    {FATAL_SUBSTATE_IDLE,       NULL,                   0},
    {FATAL_SUBSTATE_RESET,      SubState_FatalReset,    1*SECONDS},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID,      NULL,                   0}          //END of state Table - This is a MUST
};

//============================================================================
// Internal Function
//============================================================================


//============================================================================
// SubState Function
//============================================================================

static SimpleStateMachine_result_t SubState_FatalReset(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    //if (IntServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            G_SYS_DBG_LOG_V("%s reboot system in %d seconds\r\n", __FUNCTION__, FATAL_DELAY_SECONDS_TO_REBOOT);
            //Get Fw Information (initial Kick-off)
            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > FATAL_DELAY_SECONDS_TO_REBOOT)
        {
            G_SYS_DBG_LOG_V("%s reboot system now\r\n", __FUNCTION__);
            cUtlSubState_Reset(data, isFirstCall);
        }
    }

    return result;
}

//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t Fatal_EventBLEManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             EventHeader.ReportEvent);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Fatal_EventJobManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             EventHeader->EventType);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Fatal_EventServerManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    switch (EventHeader.MessageType)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             EventHeader.MessageType);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t Fatal_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Fatal_EventHpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             pHpCmdMessage->CommandMessage);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t Fatal_EventWifiManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             EventHeader.ReportEvent);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Fatal_EventUdpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        default:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING:(Line=%d) Not handled by %s (%d)\r\n",
                             __LINE__,
                             __FUNCTION__,
                             EventHeader->PacketType);
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t Fatal_EventKeyPadManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    //We will not be handling any key event here.

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t Fatal_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

StateMachine_result_t Fatal_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    FatalSubStateHandler = SimpleStateMachine_Init(FatalSubStateProcessFuncTable);

    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(FatalSubStateHandler, FATAL_SUBSTATE_RESET);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SMP unable to switch to (%d) (%d)\r\n", __LINE__, SmpResult, FATAL_SUBSTATE_RESET);
    }

    return result;
}

StateMachine_result_t Fatal_MainProcess(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    //Process the sub state machine
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_Process(FatalSubStateHandler);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FATAL, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: SimpleStateMachine_Process failed (%d)\r\n", SmpResult);
    }

    return result;
}

StateMachine_result_t Fatal_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    if (FatalSubStateHandler) {
        SimpleStateMachine_Uninit(FatalSubStateHandler);
        FatalSubStateHandler = NULL;
    }

    return result;
}


