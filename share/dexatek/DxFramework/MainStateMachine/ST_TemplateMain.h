//============================================================================
// File: ST_TemplateMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//
//============================================================================
#ifndef _ST_TEMPLATE_MAIN_H
#define _ST_TEMPLATE_MAIN_H

#include "dxOS.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//============================================================================
// Function
//============================================================================

StateMachine_result_t Template_Init(void* data );
StateMachine_result_t Template_MainProcess(void* data );
StateMachine_result_t Template_End(void* data );
StateMachineEvent_State_t Template_EventBLEManager(void* data );
StateMachineEvent_State_t Template_EventServerManager(void* data );
StateMachineEvent_State_t Template_EventWifiManager(void* data );
StateMachineEvent_State_t Template_EventUdpManager(void* data );
StateMachineEvent_State_t Template_EventJobManager(void* data );
StateMachineEvent_State_t Template_EventKeyPadManager(void* data );
StateMachineEvent_State_t Template_EventLEDManager(void* data );


#endif // _ST_TEMPLATE_MAIN_H
