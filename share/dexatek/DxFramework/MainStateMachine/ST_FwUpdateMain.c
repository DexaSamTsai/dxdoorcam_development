//============================================================================
// File: ST_FwUpdateMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "ProjectCommonConfig.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#if DEVICE_IS_HYBRID
#include "HybridPeripheralManager.h"
#endif //#if DEVICE_IS_HYBRID
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "LedManager.h"
#include "dxDeviceInfoManager.h"
#include "CentralStateMachineAutomata.h"
#include "ST_CommonUtils.h"
#include "SimpleTimedStateMachine.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================
#define DX_FWUPDATE_ALLOC_NAME                          "FWUPD"

#define CONFIG_USE_MUTEX                                0

//SubState
#define FWUPDATE_SUBSTATE_TIMEOUT_MAX                   (15*SECONDS)
#define FWUPDATE_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX       (60*SECONDS)

// Set to 1 to debug a potential memory crash issue
#define MEMORY_CRASH_ISSUE_WHILE_BOOTING                0

#define SERVER_FW_DOWNLOAD_WATCH_DOG_TIME               (60*SECONDS)

#define DELAY_SECONDS_TO_REBOOT                         (10*SECONDS)

//BLE CoProcessor FW update
#define BLE_FW_UPDATE_WRITE_BLOCK_HEADER_SIZE           2
#define BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE             32
#define BLE_FW_UPDATE_WRITE_BLOCK_CRC_SIZE              1
#define BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE            (BLE_FW_UPDATE_WRITE_BLOCK_HEADER_SIZE + BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE + BLE_FW_UPDATE_WRITE_BLOCK_CRC_SIZE)
#define BLE_FW_UPDATE_WRITE_BLOCK_HEADER_INDEX          0
#define BLE_FW_UPDATE_WRITE_BLOCK_DATA_INDEX            2
#define BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX             34
#define BLE_FW_UPDATE_WATCH_DOG_TIME                    (1*SECONDS)
#define BLE_FW_UPDATE_RETRY_MAX                         (10)

//============================================================================
// Enum
//============================================================================
typedef enum
{
    FWUPDATE_SUBSTATE_IDLE           = 0,
    FWUPDATE_SUBSTATE_RELOGIN,
    FWUPDATE_SUBSTATE_GET_FW_VERSION,
    FWUPDATE_SUBSTATE_GET_MAIN_FW_DATA,
    FWUPDATE_SUBSTATE_GET_BLE_FW_DATA,
    FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_TI,
    FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_NRF52,

    FWUPDATE_SUBSTATE_RESET,

} FwUpdate_SubState_Code_t;

typedef enum
{
    DOWNLOAD_STATE_ERROR = -1,
    DOWNLOAD_STATE_DOWNLOADING = 0,
    DOWNLOAD_STATE_COMPLETE = 1,
} Download_State_Code_t;

//============================================================================
// Structure
//============================================================================
typedef struct FwDownloadInfo
{
#if CONFIG_USE_MUTEX
    dxMutexHandle_t         mutex;
#endif // CONFIG_USE_MUTEX
    dxSubImgInfoHandle_t    BleSubImageHandle;
    uint32_t                nServerFwTotalReceiveBytes;
    uint32_t                nServerFwDownloadBytes;

    uint8_t*                pServerFirwareFileUrl;
    uint8_t*                pServerFirwareVersion;
    uint8_t*                pServerFirwareMD5Sum;
    dxTime_t                WatchDogTimer;
} FwDownloadInfo_t;

typedef struct
{
    uint32_t        FwSize;
    uint32_t        CurrentReadPosition;
    uint32_t        RetryCount;
    uint8_t         FwBuffer[64];

} CoProcessorFwUpdateInfo_t;

//============================================================================
// Function Definition
//============================================================================

dxBOOL IntSetBleFwToDUMMY(void);
dxBOOL IntSetMainFwToDUMMY(void);

static SimpleStateMachine_result_t SubState_ReLogIn(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_GetFwVersion(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_GetMainFwData(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_GetBleFwData(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_BleCentralFwUpdate_TI(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_BleCentralFwUpdate_NRF52(void* data, dxBOOL isFirstCall);
static SimpleStateMachine_result_t SubState_Reset(void* data, dxBOOL isFirstCall);

//For NRF52

//============================================================================
// Global Var
//============================================================================
#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes = @ ".sdram.text"
#endif // CONFIG_SDRAM_BUFFER_ALLOC
SimpleStateHandler      FwUpdateSubStateHandler = NULL;

static simple_st_machine_table_t FwUpdateSubStateProcessFuncTable[] =
{
    {FWUPDATE_SUBSTATE_IDLE,                        NULL,                               0},
    {FWUPDATE_SUBSTATE_RELOGIN,                     SubState_ReLogIn,                   15*SECONDS},
    {FWUPDATE_SUBSTATE_GET_FW_VERSION,              SubState_GetFwVersion,              15*SECONDS},
    {FWUPDATE_SUBSTATE_GET_MAIN_FW_DATA,            SubState_GetMainFwData,             30*SECONDS},
    {FWUPDATE_SUBSTATE_GET_BLE_FW_DATA,             SubState_GetBleFwData,              30*SECONDS},
    {FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_TI,    SubState_BleCentralFwUpdate_TI,     1*SECONDS},
    {FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_NRF52, SubState_BleCentralFwUpdate_NRF52,  10},
    {FWUPDATE_SUBSTATE_RESET,                       SubState_Reset,                     1*SECONDS},

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID,              NULL,                   0}          //END of state Table - This is a MUST
};


static FwDownloadInfo_t FwDownload =
{
#if CONFIG_USE_MUTEX
    NULL,                       // mutex
#endif // CONFIG_USE_MUTEX
    NULL,                       // BleSubImageHandle
    0,                          // nServerFwTotalReceiveBytes
    FW_DOWNLOAD_SIZE_PER_CHUNK, // nServerFwDownloadBytes
    NULL,                       // pServerFirwareFileUrl
    NULL,                       // pServerFirwareVersion
    NULL,                       // pServerFirwareMD5Sum
    0xFFFF                      // WatchDogTimer
};

CoProcessorFwUpdateInfo_t   CoProcessorFwUpdateInfo;

#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes =
#endif // CONFIG_SDRAM_BUFFER_ALLOC

//============================================================================
// Internal Function
//============================================================================

#if DEVICE_IS_HYBRID

void IntSendFwUpdateInfoProgressToHp(int32_t UpdateType, uint8_t PercentageCompletion)
{

    HpCmdMessageQueue_t* pCmdMessage = dxMemAlloc("HpCmdMsg", 1, sizeof(HpCmdMessageQueue_t));
    pCmdMessage->CommandMessage = HP_CMD_ENTER_FW_UPDATE_REPORT;
    pCmdMessage->MessageID = 0;
    pCmdMessage->Priority = 0; //Normal

    HpFwUpdateStatusRes_t* pFwUpdateInfoRes = dxMemAlloc("pFwUpdateInfoRes", 1, sizeof(HpFwUpdateStatusRes_t));
    pFwUpdateInfoRes->FwUpdateDownloadType = UpdateType;
    pFwUpdateInfoRes->Progress = PercentageCompletion;

    pCmdMessage->PayloadSize = sizeof(HpFwUpdateStatusRes_t);
    pCmdMessage->PayloadData = (uint8_t*)pFwUpdateInfoRes;

    HpManagerSendTo_Client(pCmdMessage);
    HpManagerCleanEventArgumentObj(pCmdMessage);
}

#endif

dxBOOL  IntFwUpdateServerStateReadyCheck(void)
{
    dxBOOL ServerStateReady = dxTRUE;

    if (DKServerManager_GetCurrentState() != DKSERVERMANAGER_STATEMACHINE_STATE_READY)
    {
        ServerStateReady = dxFALSE;

        //Change to IDLE
        CentralStateMachine_ChangeState(STATE_IDLE, NULL, 0);
    }

    return ServerStateReady;
}

dxBOOL IntChangeSubStateTo(FwUpdate_SubState_Code_t SubState)
{
    dxBOOL bRC = dxTRUE;

    //Something is wrong, let's get firmware information again, if it still does not succeed then it will change to relogin state
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_ExecuteState(FwUpdateSubStateHandler, SubState);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING:(Line=%d) SMP unable to switch to (%d) (%d)\r\n", __LINE__, SmpResult, SubState);
        bRC = dxFALSE;
    }

    return bRC;
}

void IntFwDownload_Init(void)
{
#if CONFIG_USE_MUTEX
    FwDownload.mutex = dxMutexCreate();
#endif // CONFIG_USE_MUTEX
}

void IntFwDownload_Free(void)
{
    FwDownload.nServerFwTotalReceiveBytes = 0;

#if CONFIG_USE_MUTEX
    if (FwDownload.mutex)
    {
        dxMutexDelete(FwDownload.mutex);
        FwDownload.mutex = NULL;
    }
#endif // CONFIG_USE_MUTEX

    if (FwDownload.pServerFirwareFileUrl)
    {
        dxMemFree(FwDownload.pServerFirwareFileUrl);
        FwDownload.pServerFirwareFileUrl = NULL;
    }

    if (FwDownload.pServerFirwareVersion)
    {
        dxMemFree(FwDownload.pServerFirwareVersion);
        FwDownload.pServerFirwareVersion = NULL;
    }

    if (FwDownload.pServerFirwareMD5Sum)
    {
        dxMemFree(FwDownload.pServerFirwareMD5Sum);
        FwDownload.pServerFirwareMD5Sum = NULL;
    }
}

void IntFwDownload_Resource_Take(void)
{
#if CONFIG_USE_MUTEX
    if (FwDownload.mutex)
    {
        dxMutexTake(FwDownload.mutex, DXOS_WAIT_FOREVER);
    }
#endif // CONFIG_USE_MUTEX
}

void IntFwDownload_Resource_Release(void)
{
#if CONFIG_USE_MUTEX
    if (FwDownload.mutex)
    {
        dxMutexGive(FwDownload.mutex);
    }
#endif // CONFIG_USE_MUTEX
}

dxBOOL IntBleCentralInSBL(void)
{
    dxBOOL bRC = dxFALSE;

    SystemInfoReportObj_t SystemInfoContainer;
    BLEManager_result_t bleresult = BleManagerSystemInfoGet(&SystemInfoContainer);

    if (bleresult == DEV_MANAGER_SUCCESS)
    {
        if (BleManager_SBL_Enable())
        {
            bRC = dxTRUE;
        }
    }

    return bRC;
}

dxBOOL IntMainFwUpdateCheck(uint8_t *pData)
{
    dxBOOL UpdateIsRequired = dxFALSE;

    if (pData)
    {
        DKFirmare_Info_ID_t fwVersionID = DKFIRMWARE_INFO_ID_WIFI_VERSION;
        uint8_t *pMainFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

        if (pMainFirmwareVersion)
        {
            DKServer_StatusCode_t FwVersionCompareResult = DKServerManager_FirmwareVersionCompare((uint8_t*)RTK_WIFI_CURRENT_FW_VERSION, pMainFirmwareVersion);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "LocalVer = %s Server Ver = %s\r\n", RTK_WIFI_CURRENT_FW_VERSION, pMainFirmwareVersion);
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "FwVer Compare Result = %d\r\n", FwVersionCompareResult);

            //If the version from server is higher.
            if (FwVersionCompareResult == DKSERVER_STATUS_SUCCESS)
            {
                UpdateIsRequired = dxTRUE;
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: Latest FW Version from Server/local is incomplete\r\n");
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: Latest FW Version from Server/local is incomplete\r\n");
    }

    return UpdateIsRequired;
}

static FwUpdate_SubState_Code_t get_firmware_update_sub_state_code(void)
{
    if (BleManager_SolutionID_Get() == SOLUTION_ID_NORDIC_52832)
    {
        return FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_NRF52;
    }
    else
    {
        return FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_TI;
    }
}


dxBOOL IntBleFwUpdateCheck(uint8_t *pData)
{
    dxBOOL UpdateIsRequired = dxFALSE;
    SystemInfoReportObj_t SystemInfoContainer;
    DKServer_StatusCode_t FwVersionCompareResult;

    if (pData)
    {
        DKFirmare_Info_ID_t fwVersionID = DKFIRMWARE_INFO_ID_BLE_VERSION;
        uint8_t *pBleFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

        BLEManager_result_t bleresult = BleManagerSystemInfoGet(&SystemInfoContainer);

        if (bleresult == DEV_MANAGER_SUCCESS)
        {
            if (BleManager_SBL_Enable())
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "Ble Central in SBL mode %d\r\n", SystemInfoContainer.ImageType);

                if (IntChangeSubStateTo(get_firmware_update_sub_state_code()) == dxTRUE)
                {
                    LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_RESTART);
                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerRetCode);
                    }
                }
            }
            else if (pBleFirmwareVersion)
            {
                FwVersionCompareResult = DKServerManager_FirmwareVersionCompare((uint8_t*)SystemInfoContainer.Version, pBleFirmwareVersion);
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "Ble LocalVer = %s Server Ver = %s\r\n", SystemInfoContainer.Version ,pBleFirmwareVersion);
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "FwVer Compare Result = %d\r\n", FwVersionCompareResult);

                //If the version from server is higher.
                if (FwVersionCompareResult == DKSERVER_STATUS_SUCCESS)
                {
                    FwDownload.BleSubImageHandle = dxSubImage_Init(DXSUBIMGBLOCK_ONE);

                    if (FwDownload.BleSubImageHandle == NULL)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING:(Line=%d) dxSubImage_Init() RetCode = NULL, BLE firmware update STOPPED\r\n",
                                         __LINE__);
                    }
                    else
                    {
                        UpdateIsRequired = dxTRUE;
                    }
                }
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Latest BLE FW Version from Server/local is incomplete\r\n");
            }
        }
        else
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: IntBleFwUpdateRequired cannot obtain ble fw ver.\r\n");
        }
    }
    else
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: IntBleFwUpdateRequired pData is NULL.\r\n");
    }

    return UpdateIsRequired;
}

dxBOOL IntParseMainFwMessage_GetLastFirmwareInfo(uint8_t *pData)
{
    dxBOOL StartToUpdate = dxFALSE;
    LedManager_result_t LedManagerResult;

    if (pData == NULL)
    {
        return dxFALSE;
    }

    // DKFIRMWARE_INFO_ID_WIFI_FILENAME => Get WiFi firmware filename.
    // DKFIRMWARE_INFO_ID_WIFI_VERSION  => Get WiFi firmware version.
    // DKFIRMWARE_INFO_ID_WIFI_MD5      => Get WiFi firmware's MD5 checksum value
    // DKFIRMWARE_INFO_ID_BLE_FILENAME  => Get BLE firmware filename.
    // DKFIRMWARE_INFO_ID_BLE_VERSION   => Get BLE firmware version.
    // DKFIRMWARE_INFO_ID_BLE_MD5       => Get BLE firmware's MD5 checksum value

    DKFirmare_Info_ID_t fwNameID = DKFIRMWARE_INFO_ID_WIFI_FILENAME;
    uint8_t *pFirmwareURL = DKServerManager_GetFirmwareItem(fwNameID, pData);
    DKFirmare_Info_ID_t fwMD5ID = DKFIRMWARE_INFO_ID_WIFI_MD5;
    uint8_t *pFirmwareMD5 = DKServerManager_GetFirmwareItem(fwMD5ID, pData);
    DKFirmare_Info_ID_t fwVersionID = DKFIRMWARE_INFO_ID_WIFI_VERSION;
    uint8_t *pFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "pFirmwareURL %s, pFirmwareMD5 %s, pFirmwareVersion %s\r\n",
                     pFirmwareURL,
                     pFirmwareMD5,
                     pFirmwareVersion);

    IntFwDownload_Free();

    FwDownload.pServerFirwareFileUrl = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                  sizeof(uint8_t),
                                                  strlen((char *)pFirmwareURL) + 1);

    FwDownload.pServerFirwareVersion = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                  sizeof(uint8_t),
                                                  strlen((char *)pFirmwareVersion) + 1);

    FwDownload.pServerFirwareMD5Sum = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                 sizeof(uint8_t),
                                                 strlen((char *)pFirmwareMD5) + 1);

    if (FwDownload.pServerFirwareFileUrl == NULL ||
        FwDownload.pServerFirwareVersion == NULL ||
        FwDownload.pServerFirwareMD5Sum == NULL)
    {
        goto CleanUpExit;
    }

    // Save the firmware filename
    memcpy(FwDownload.pServerFirwareFileUrl, pFirmwareURL, strlen((char *)pFirmwareURL));

    // Save the firmware version
    memcpy(FwDownload.pServerFirwareVersion, pFirmwareVersion, strlen((char *)pFirmwareVersion));

    // Save the firmware MD5 checksum
    memcpy(FwDownload.pServerFirwareMD5Sum, pFirmwareMD5, strlen((char *)pFirmwareMD5));

    //Tell server gateway state is FW updateting
    DKServerManager_ChangeGatewayStatus_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, GATEWAY_STATE_FW_UPDATETING);

    //Switch to next sub state to get firmware data
    if (IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_MAIN_FW_DATA) == dxFALSE)
    {
        goto CleanUpExit;
    }

    //TODO: Here we are going to change the mode rigth away, BUT we should instead schedule for FW update (say local time mid-nigth) if
    //      the FW update is not urgent.
    LedManagerResult = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_FW_DOWNLOAD);
    if (LedManagerResult != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerResult);
        goto CleanUpExit;
    }

    StartToUpdate = dxTRUE;
    goto SafeExit;

CleanUpExit:
    IntFwDownload_Free();

SafeExit:
    return StartToUpdate;
}

dxBOOL IntParseBleFwMessage_GetLastFirmwareInfo(uint8_t *pData)
{
    dxBOOL StartToUpdate = dxFALSE;
    LedManager_result_t LedManagerResult;

    if (pData == NULL)
    {
        return dxFALSE;
    }

    // DKFIRMWARE_INFO_ID_WIFI_FILENAME => Get WiFi firmware filename.
    // DKFIRMWARE_INFO_ID_WIFI_VERSION  => Get WiFi firmware version.
    // DKFIRMWARE_INFO_ID_WIFI_MD5      => Get WiFi firmware's MD5 checksum value
    // DKFIRMWARE_INFO_ID_BLE_FILENAME  => Get BLE firmware filename.
    // DKFIRMWARE_INFO_ID_BLE_VERSION   => Get BLE firmware version.
    // DKFIRMWARE_INFO_ID_BLE_MD5       => Get BLE firmware's MD5 checksum value

    DKFirmare_Info_ID_t fwNameID = DKFIRMWARE_INFO_ID_BLE_FILENAME;
    uint8_t *pFirmwareURL = DKServerManager_GetFirmwareItem(fwNameID, pData);
    DKFirmare_Info_ID_t fwMD5ID = DKFIRMWARE_INFO_ID_BLE_MD5;
    uint8_t *pFirmwareMD5 = DKServerManager_GetFirmwareItem(fwMD5ID, pData);
    DKFirmare_Info_ID_t fwVersionID = DKFIRMWARE_INFO_ID_BLE_VERSION;
    uint8_t *pFirmwareVersion = DKServerManager_GetFirmwareItem(fwVersionID, pData);

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "pFirmwareURL %s, pFirmwareMD5 %s, pFirmwareVersion %s\r\n",
                     pFirmwareURL,
                     pFirmwareMD5,
                     pFirmwareVersion);

    IntFwDownload_Free();

    FwDownload.pServerFirwareFileUrl = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                  sizeof(uint8_t),
                                                  strlen((char *)pFirmwareURL) + 1);

    FwDownload.pServerFirwareVersion = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                  sizeof(uint8_t),
                                                  strlen((char *)pFirmwareVersion) + 1);

    FwDownload.pServerFirwareMD5Sum = dxMemAlloc(DX_FWUPDATE_ALLOC_NAME,
                                                 sizeof(uint8_t),
                                                 strlen((char *)pFirmwareMD5) + 1);

    if (FwDownload.pServerFirwareFileUrl == NULL ||
        FwDownload.pServerFirwareVersion == NULL ||
        FwDownload.pServerFirwareMD5Sum == NULL)
    {
        goto CleanUpExit;
    }

    // Save the firmware filename
    memcpy(FwDownload.pServerFirwareFileUrl, pFirmwareURL, strlen((char *)pFirmwareURL));

    // Save the firmware version
    memcpy(FwDownload.pServerFirwareVersion, pFirmwareVersion, strlen((char *)pFirmwareVersion));

    // Save the firmware MD5 checksum
    memcpy(FwDownload.pServerFirwareMD5Sum, pFirmwareMD5, strlen((char *)pFirmwareMD5));

    //Tell server gateway state is FW updateting
    DKServerManager_ChangeGatewayStatus_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0, GATEWAY_STATE_FW_UPDATETING);

    //Switch to next sub state to get firmware data
    if (IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_BLE_FW_DATA) == dxFALSE)
    {
        goto CleanUpExit;
    }

    //TODO: Here we are going to change the mode rigth away, BUT we should instead schedule for FW update (say local time mid-nigth) if
    //      the FW update is not urgent.
    LedManagerResult = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_BLE_CENTRAL_FW_UPDATE);
    if (LedManagerResult != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerResult);
        goto CleanUpExit;
    }

    StartToUpdate = dxTRUE;
    goto SafeExit;

CleanUpExit:
    IntFwDownload_Free();

SafeExit:
    return StartToUpdate;
}

dxBOOL IntPrepareDownloadFirmware(void)
{
    LedManager_result_t LedManagerResult;
    dxBOOL ReadyToDownload = dxTRUE;

    UdpCommManagerStop();

    //We need to disable GetNewJob before we start fw update process
    if (DKServerManager_ConfigGetJobInterval(0) != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: DKServerManager_SetGetNewJobFlag failed\r\n");
    }

    //Suspend BLE Manager and then reset it to release all data to cleanup memory
    BleManagerSuspend();
    BleManagerReset();

    //Remove all schedule job to release all data to cleanup memory
    JobManagerRemoveAllJob(SOURCE_OF_ORIGIN_DEFAULT,  0);

    LedManagerResult = SetLedMode(LED_OPERATION_MODE_FW_DOWNLOAD);
    if (LedManagerResult != LED_MANAGER_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerResult);
    }

    return ReadyToDownload;
}

dxBOOL IntDownloadFirmwareRequest(void)
{
    DKServer_StatusCode_t Result = DKServerManager_FirmwareDownload_Asynch(SOURCE_OF_ORIGIN_DEFAULT,
                                                                           0,
                                                                           FwDownload.pServerFirwareFileUrl,
                                                                           FwDownload.nServerFwTotalReceiveBytes,
                                                                           FwDownload.nServerFwDownloadBytes);
    if (Result != DKSERVER_STATUS_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: Failed to call FwDownload (error %d)\r\n", Result);

        return dxFALSE;
    }

    FwDownload.WatchDogTimer = dxTimeGetMS();

    return dxTRUE;
}

Download_State_Code_t IntParseMainFwMessage_DownloadFirmware(DKFirmwareInfo_t *pDownloadInfo)
{
    Download_State_Code_t DownloadState = DOWNLOAD_STATE_ERROR;

    dxMAINIMAGE_RET_CODE MainImageUpRet = DXMAINIMAGE_ERROR;

    if (pDownloadInfo == NULL)
    {
        return DOWNLOAD_STATE_ERROR;
    }

    int nCurrentReceivedBytes = (pDownloadInfo->range.nLastBytePosition + 1 - pDownloadInfo->range.nFirstBytePosition);

    if (pDownloadInfo->range.nFileSize <= 0)
    {
        return DOWNLOAD_STATE_ERROR;
    }

    // Only first time need to call dxMainImage_Start() to prepare empty flash memory
    // and start a MD5 checksum context
    if (FwDownload.nServerFwTotalReceiveBytes == 0)
    {
        MainImageUpRet = dxMainImage_Start(FwDownload.pServerFirwareMD5Sum, pDownloadInfo->range.nFileSize);
        if (MainImageUpRet < DXMAINIMAGE_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: dxMainImage_Start failed (%d)\r\n", MainImageUpRet);
            return DOWNLOAD_STATE_ERROR;
        }
    }

    // Write all receiving binary data to flash
    MainImageUpRet = dxMainImage_Write(pDownloadInfo->pData, nCurrentReceivedBytes);
    if (MainImageUpRet < DXMAINIMAGE_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxMainImage_Write failed (%d)\r\n", MainImageUpRet);
        return DOWNLOAD_STATE_ERROR;
    }

    FwDownload.nServerFwTotalReceiveBytes += nCurrentReceivedBytes;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "FileSize %d, Current total %d, From %d to %d\r\n",
                     (int)pDownloadInfo->range.nFileSize,
                     FwDownload.nServerFwTotalReceiveBytes,
                     (int)pDownloadInfo->range.nFirstBytePosition,
                     (int)pDownloadInfo->range.nLastBytePosition);

    if (FwDownload.nServerFwTotalReceiveBytes < pDownloadInfo->range.nFileSize)
    {
        DownloadState = DOWNLOAD_STATE_DOWNLOADING;
    }
    else if (MainImageUpRet == DXMAINIMAGE_COMPLETE)
    {
        // Finish download. Free allocated memory
        IntFwDownload_Free();
        DownloadState = DOWNLOAD_STATE_COMPLETE;
    }

    return DownloadState;
}

Download_State_Code_t IntParseBleFwMessage_DownloadFirmware(DKFirmwareInfo_t *pDownloadInfo)
{
    Download_State_Code_t DownloadState = DOWNLOAD_STATE_ERROR;

    dxSUBIMAGE_RET_CODE BleImageUpRet = DXMAINIMAGE_ERROR;

    if (pDownloadInfo == NULL ||
        FwDownload.BleSubImageHandle == NULL)
    {
        return DOWNLOAD_STATE_ERROR;
    }

    int nCurrentReceivedBytes = (pDownloadInfo->range.nLastBytePosition + 1 - pDownloadInfo->range.nFirstBytePosition);

    if (pDownloadInfo->range.nFileSize <= 0)
    {
        return DOWNLOAD_STATE_ERROR;
    }

    // Only first time need to call dxMainImage_Start() to prepare empty flash memory
    // and start a MD5 checksum context
    if (FwDownload.nServerFwTotalReceiveBytes == 0)
    {
        BleImageUpRet = dxSubImage_Start(FwDownload.BleSubImageHandle, FwDownload.pServerFirwareMD5Sum, pDownloadInfo->range.nFileSize);
        if (BleImageUpRet < DXSUBIMAGE_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: dxSubImage_Start failed (%d)\r\n", BleImageUpRet);
            return DOWNLOAD_STATE_ERROR;
        }
    }

    // Write all receiving binary data to flash
    BleImageUpRet = dxSubImage_Write(FwDownload.BleSubImageHandle, pDownloadInfo->pData, nCurrentReceivedBytes);
    if (BleImageUpRet < DXSUBIMAGE_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxSubImage_Write failed (%d)\r\n", BleImageUpRet);
        return DOWNLOAD_STATE_ERROR;
    }

    FwDownload.nServerFwTotalReceiveBytes += nCurrentReceivedBytes;

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "FileSize %d, Current total %d, From %d to %d\r\n",
                     (int)pDownloadInfo->range.nFileSize,
                     FwDownload.nServerFwTotalReceiveBytes,
                     (int)pDownloadInfo->range.nFirstBytePosition,
                     (int)pDownloadInfo->range.nLastBytePosition);

    if (FwDownload.nServerFwTotalReceiveBytes < pDownloadInfo->range.nFileSize)
    {
        DownloadState = DOWNLOAD_STATE_DOWNLOADING;
    }
    else if (BleImageUpRet == DXSUBIMAGE_COMPLETE)
    {
        // Finish download. Free allocated memory
        IntFwDownload_Free();
        DownloadState = DOWNLOAD_STATE_COMPLETE;
    }

    return DownloadState;
}

dxBOOL IntBleFwUpdateBlockWrite(void)
{
    uint32_t ReadSize;
    dxSubImgInfoHandle_t handle = dxSubImage_Init(DXSUBIMGBLOCK_ONE);

    if (handle == NULL)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxSubImage_Init() FAILED!\r\n");
        return dxFALSE;
    }

    CoProcessorFwUpdateInfo.FwSize = dxSubImage_ReadImageSize(handle);
    if (CoProcessorFwUpdateInfo.FwSize == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxSubImage_Read() FwSize is 0!\r\n");
        return dxFALSE;
    }

    dxSUBIMAGE_RET_CODE ret = dxSubImage_Read(handle,
                                              CoProcessorFwUpdateInfo.CurrentReadPosition,
                                              BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE,
                                              &CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_DATA_INDEX],
                                              &ReadSize);

    if (ret != DXSUBIMAGE_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: dxSubImage_Read() RetCode = %d\r\n", ret);
        return dxFALSE;
    }

    uint32_t CurrentReadPositionForFlash = CoProcessorFwUpdateInfo.CurrentReadPosition / 4;
    CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_HEADER_INDEX+1] = (CurrentReadPositionForFlash / 256);
    CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_HEADER_INDEX] = (CurrentReadPositionForFlash % 256);
    CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX] = 0;
    uint8_t j = 0;
    for (j = 0; j < (BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE-1); j++)
    {
        CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX] ^= CoProcessorFwUpdateInfo.FwBuffer[j];
    }

    BleManagerFwUpdateBlockWriteTask_Asynch(CoProcessorFwUpdateInfo.FwBuffer, BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE, SOURCE_OF_ORIGIN_DEFAULT, 0);

    return dxTRUE;
}

dxBOOL IntPrepareBleCentralFwUpdate(void)
{
    DKServerManager_State_Suspend();

    UdpCommManagerStop();

    BleManagerSetRoleToFwUpdate();

    memset(&CoProcessorFwUpdateInfo, 0, sizeof(CoProcessorFwUpdateInfo_t));

    IntBleFwUpdateBlockWrite();

    return dxTRUE;
}

dxBOOL IntPrepareBleCentralFwUpdate_NRF52(void)
{
    DKServerManager_State_Suspend();

    UdpCommManagerStop();

    BleManagerSetRoleToFwUpdate();

    return dxTRUE;
}

Download_State_Code_t IntBleCentralFwUpdate(void)
{
    Download_State_Code_t DownloadState = DOWNLOAD_STATE_ERROR;

    CoProcessorFwUpdateInfo.RetryCount++;

    //We are going to retry base on the situation of progress
    if (CoProcessorFwUpdateInfo.RetryCount < BLE_FW_UPDATE_RETRY_MAX)
    {
        if (CoProcessorFwUpdateInfo.CurrentReadPosition < CoProcessorFwUpdateInfo.FwSize)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "BLE FW Updating...\r\n" );

            //Since we failed we will need to roll back to 2K alignment (each block is 2048).
            uint32_t LeftOver2KAlignment = CoProcessorFwUpdateInfo.CurrentReadPosition%2048;
            CoProcessorFwUpdateInfo.CurrentReadPosition = (LeftOver2KAlignment > CoProcessorFwUpdateInfo.CurrentReadPosition) ?
                                                           0 : (CoProcessorFwUpdateInfo.CurrentReadPosition - LeftOver2KAlignment);
            IntBleFwUpdateBlockWrite();

            DownloadState = DOWNLOAD_STATE_DOWNLOADING;
        }
        else //We already finished the update, simply re-send the FwUpdate done comand.
        {
#if DEVICE_IS_HYBRID
            IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_BLE_UPDATING, 100);
#endif // DEVICE_IS_HYBRID

            DownloadState = DOWNLOAD_STATE_COMPLETE;

            BleManagerFwUpdateDoneTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);

            //Change to STATE_SERVER_ONLINE
            //CentralStateMachine_ChangeState(STATE_SERVER_ONLINE, NULL, 0);

            // Reboot
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "Ble Fw update finished. REBOOT later!!!\r\n");

            // Set Ble Fw version to DUMMY that will force update firmware to Cloud Server after rebooting
            IntSetBleFwToDUMMY();

            IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);
        }
    }

    return DownloadState;
}

dxBOOL IntBleFwBlockWriteResponseVerify(void* arg)
{
    dxBOOL bRC = dxTRUE;
    uint8_t  DataLen = 0;
    uint8_t* pData = BleManagerParseFwBlockWriteVerifyReportEventObj(arg, &DataLen);

    if (pData == NULL || DataLen == 0)
    {
        return dxFALSE;
    }

    if ((CoProcessorFwUpdateInfo.CurrentReadPosition % 8192) == 0)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                         "FIRMWARE UPDATE PROGRESS - %d / %d\r\n", CoProcessorFwUpdateInfo.CurrentReadPosition, CoProcessorFwUpdateInfo.FwSize);
    }

    //We expect total of BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE byte response data
    if (DataLen == BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE)
    {
        uint16_t j;
        uint8_t VerifyChecksumCalc = 0;
        for (j = 0; j < (BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE-1); j++)
        {
            if (pData[j] == CoProcessorFwUpdateInfo.FwBuffer[j])
            {
                VerifyChecksumCalc ^= pData[j];
            }
            else
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Ble fw byte verify failed\r\n");
                bRC = dxFALSE;
                break;
            }
        }

        if ((VerifyChecksumCalc != CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX]) || (j < (BLE_FW_UPDATE_WRITE_BLOCK_TOTAL_SIZE -1)))
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: Ble fw block verify failed! (%x,%x)\r\n",
                             VerifyChecksumCalc,
                             CoProcessorFwUpdateInfo.FwBuffer[BLE_FW_UPDATE_WRITE_BLOCK_CRC_INDEX]);
        }
        else
        {
            CoProcessorFwUpdateInfo.RetryCount = 0; //Reset the retry count whenever we successfully received data verify
            CoProcessorFwUpdateInfo.CurrentReadPosition += BLE_FW_UPDATE_WRITE_BLOCK_DATA_SIZE;
            if (CoProcessorFwUpdateInfo.CurrentReadPosition < CoProcessorFwUpdateInfo.FwSize)
            {
                if (IntBleFwUpdateBlockWrite() == dxTRUE)
                {
                    static uint8_t  LastPercentageCompletion = 100;
                    //Update the time
                    FwDownload.WatchDogTimer = dxTimeGetMS();

                    uint8_t PercentageCompletion = (uint8_t)((((float)CoProcessorFwUpdateInfo.CurrentReadPosition)/CoProcessorFwUpdateInfo.FwSize)*100);
                    if(LastPercentageCompletion != PercentageCompletion)
                    {
#if DEVICE_IS_HYBRID
                        IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_BLE_UPDATING, PercentageCompletion);
#endif  //#if DEVICE_IS_HYBRID
                        LastPercentageCompletion = PercentageCompletion;
                    }
                }
            }
            else
            {
                BleManagerFwUpdateDoneTask_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0);

                //Change to STATE_SERVER_ONLINE
                //CentralStateMachine_ChangeState(STATE_SERVER_ONLINE, NULL, 0);

                // Reboot
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                 "Ble Fw update finished. REBOOT later!!!\r\n");

                // Set Ble Fw version to DUMMY that will force update firmware to Cloud Server after rebooting
                IntSetBleFwToDUMMY();

                IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);
            }
        }
    }

    return bRC;
}

dxBOOL IntSetMainFwToDUMMY(void)
{
    dxBOOL bRC = dxFALSE;

    uint16_t FwDummyLen = strlen(FW_VERSION_DUMMY);
    uint8_t FwDummyBuffer[16];
    memset(FwDummyBuffer, 0x00, sizeof(FwDummyBuffer));
    memcpy(FwDummyBuffer, FW_VERSION_DUMMY, FwDummyLen);

    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_FW_VERSION, FwDummyBuffer, FwDummyLen + 1, dxTRUE);
    if (DevInfoRet == DXDEV_INFO_SUCCESS)
    {
        bRC = dxTRUE;
    }

    return bRC;
}

dxBOOL IntSetBleFwToDUMMY(void)
{
    dxBOOL bRC = dxFALSE;

    uint16_t FwDummyLen = strlen(FW_VERSION_DUMMY);
    uint8_t FwDummyBuffer[16];
    memset(FwDummyBuffer, 0x00, sizeof(FwDummyBuffer));
    memcpy(FwDummyBuffer, FW_VERSION_DUMMY, FwDummyLen);

    dxDEV_INFO_RET_CODE DevInfoRet = dxDevInfo_Update(DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION, FwDummyBuffer, FwDummyLen + 1, dxTRUE);
    if (DevInfoRet == DXDEV_INFO_SUCCESS)
    {
        bRC = dxTRUE;
    }

    return bRC;
}

//============================================================================
// SubState Function
//============================================================================

static SimpleStateMachine_result_t SubState_ReLogIn(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntFwUpdateServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            cUtlLogInToServer();
        }
        else
        {
            //Change to ILDE
            CentralStateMachine_ChangeState(STATE_IDLE, NULL, 0);
        }
    }

    return result;
}

static SimpleStateMachine_result_t SubState_GetFwVersion(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntFwUpdateServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get Fw Information (initial Kick-off)
            if (DKServerManager_GetLastFirmareInfo_Asynch(SOURCE_OF_ORIGIN_DEFAULT, 0) != DKSERVER_STATUS_SUCCESS)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Failed to Call DKServerManager_GetLastFirmareInfo_Asynch\r\n");
            }

            TimeAtFirstTry = dxTimeGetMS();
        }
        else if ((dxTimeGetMS() - TimeAtFirstTry) > FWUPDATE_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            TimeAtFirstTry = dxTimeGetMS();

            //Something is wrong, let's relogin again, if it still does not succeed then it will change to offline state
            IntChangeSubStateTo(FWUPDATE_SUBSTATE_RELOGIN);
        }
    }

    return result;
}

static SimpleStateMachine_result_t SubState_GetMainFwData(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntFwUpdateServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get Main firmware data (initial Kick-off)
            IntFwDownload_Resource_Take();
            if (IntDownloadFirmwareRequest() == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Failed to Call IntDownloadFirmwareRequest()\r\n");
            }
            IntFwDownload_Resource_Release();
        }
        else
        {
            uint32_t n = 0;
            IntFwDownload_Resource_Take();
            n = dxTimeGetMS() - FwDownload.WatchDogTimer;
            IntFwDownload_Resource_Release();

            if (n > SERVER_FW_DOWNLOAD_WATCH_DOG_TIME)
            {
                //Something is wrong, let's get firmware information again, if it still does not succeed then it will change to relogin state
                IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_FW_VERSION);
            }

            IntFwDownload_Resource_Take();
            FwDownload.WatchDogTimer = dxTimeGetMS();
            IntFwDownload_Resource_Release();
        }
    }

    return result;
}

static SimpleStateMachine_result_t SubState_GetBleFwData(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (IntFwUpdateServerStateReadyCheck() == dxTRUE)
    {
        if (isFirstCall)
        {
            //Get Ble Central firmware data (initial Kick-off)
            IntFwDownload_Resource_Take();
            if (IntDownloadFirmwareRequest() == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                 "WARNING: Failed to Call IntDownloadFirmwareRequest()\r\n");
            }
            IntFwDownload_Resource_Release();
        }
        else //if ((dxTimeGetMS() - TimeAtFirstTry) > FWUPDATE_SUBSTATE_SEQ_EXTEDED_TIMEOUT_MAX)
        {
            uint32_t n = 0;
            IntFwDownload_Resource_Take();
            n = dxTimeGetMS() - FwDownload.WatchDogTimer;
            IntFwDownload_Resource_Release();

            if (n > SERVER_FW_DOWNLOAD_WATCH_DOG_TIME)
            {
                //Something is wrong, let's get firmware information again, if it still does not succeed then it will change to relogin state
                IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_FW_VERSION);

                IntFwDownload_Resource_Take();
                FwDownload.WatchDogTimer = dxTimeGetMS();
                IntFwDownload_Resource_Release();
            }
        }
    }

    return result;
}

static SimpleStateMachine_result_t SubState_BleCentralFwUpdate_TI(void* data, dxBOOL isFirstCall)
{
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (isFirstCall)
    {
        IntPrepareBleCentralFwUpdate();

        LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_BLE_CENTRAL_FW_UPDATE);
        if (LedManagerRetCode != LED_MANAGER_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n",
                             LedManagerRetCode);
        }
    }
    else
    {
        uint32_t n = 0;
        dxBOOL      ErrorResetSystem = dxFALSE;
        IntFwDownload_Resource_Take();
        n = dxTimeGetMS() - FwDownload.WatchDogTimer;
        IntFwDownload_Resource_Release();

        if (n > BLE_FW_UPDATE_WATCH_DOG_TIME)
        {
            Download_State_Code_t DownloadState = IntBleCentralFwUpdate();

            if (DownloadState == DOWNLOAD_STATE_ERROR)
            {
                ErrorResetSystem = dxTRUE;
            }
        }

        if (ErrorResetSystem == dxTRUE)
        {
            IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);
        }
    }

    return result;
}

static SimpleStateMachine_result_t SubState_Reset(void* data, dxBOOL isFirstCall)
{
    static dxTime_t TimeAtFirstTry = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (isFirstCall)
    {
        //Get Fw Information (initial Kick-off)
        TimeAtFirstTry = dxTimeGetMS();
    }
    else if ((dxTimeGetMS() - TimeAtFirstTry) > DELAY_SECONDS_TO_REBOOT)
    {
        cUtlSubState_Reset(data, isFirstCall);
    }

    return result;
}

//============================================================================
// Event Function
//============================================================================

StateMachineEvent_State_t FwUpdate_EventBLEManager(void* arg)
{
    dxBOOL bRC = dxFALSE;
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        case DEV_MANAGER_EVENT_FW_BLOCK_RESPONSE_VERIFY:
        {
            IntFwDownload_Resource_Take();
            bRC = IntBleFwBlockWriteResponseVerify(arg);
            IntFwDownload_Resource_Release();
            if (bRC == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                             "WARNING: IntBleFwBlockWriteResponseVerify failed\r\n" );
            }
        }
            break;
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n",
                     __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t FwUpdate_EventJobManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);

    switch (EventHeader->EventType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n",
                     __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t FwUpdate_EventServerManager(void* arg)
{
    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);

    switch (EventHeader.MessageType)
    {
        case DKMSG_CHANGEGATEWAYSTATUS:     // 5
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                if (IntPrepareDownloadFirmware() == dxFALSE)
                {
                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                     "WARNING: IntPrepareDownloadFirmware() failed!!!\r\n");
                }
            }
        }
            break;

        case DKMSG_GETLASTFIRMWAREINFO:     // 27
        {
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                //Just make sure we are still in the same state and its not about to change
                if (SimpleStateMachine_StateNotChangingWhileEqualTo(FwUpdateSubStateHandler, FWUPDATE_SUBSTATE_GET_FW_VERSION))
                {
                    uint8_t *pData = (uint8_t *)DKServerManager_GetEventObjPayload(arg);

                    IntFwDownload_Resource_Take();

                    //We always check first if we need to update Wifi FW first, if Wifi FW update is not needed then we
                    //move on to check BLE FW update check
                    if (IntMainFwUpdateCheck(pData) == dxTRUE)
                    {
                        if (IntParseMainFwMessage_GetLastFirmwareInfo(pData) == dxFALSE)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING: IntParseMainFwMessage_GetLastFirmwareInfo() failed!!!\r\n");
                        }
                    }
                    else if (IntBleFwUpdateCheck(pData) == dxTRUE)
                    {
                        if (IntParseBleFwMessage_GetLastFirmwareInfo(pData) == dxFALSE)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING: IntParseBleFwMessage_GetLastFirmwareInfo() failed!!!\r\n");
                        }
                    }
                    else
                    {
                        //Change to STATE_SERVER_ONLINE
                        CentralStateMachine_ChangeState(STATE_SERVER_ONLINE, NULL, 0);
                    }

                    IntFwDownload_Resource_Release();
                }
            }
        }
            break;

        case DKMSG_FIRMWAREDOWNLOAD:        // 28
        {
            dxBOOL DownloadProcessRestart = dxFALSE;
            if (EventHeader.s.ReportStatusCode == DKSERVER_STATUS_SUCCESS)
            {
                Download_State_Code_t DownloadState;

                IntFwDownload_Resource_Take();
                DKFirmwareInfo_t *pDownloadInfo = (DKFirmwareInfo_t *)DKServerManager_GetEventObjPayload(arg);
                IntFwDownload_Resource_Release();

                if (SimpleStateMachine_StateNotChangingWhileEqualTo(FwUpdateSubStateHandler, FWUPDATE_SUBSTATE_GET_MAIN_FW_DATA))
                {
                    IntFwDownload_Resource_Take();
                    DownloadState = IntParseMainFwMessage_DownloadFirmware(pDownloadInfo);
                    IntFwDownload_Resource_Release();

                    if (DownloadState == DOWNLOAD_STATE_DOWNLOADING)
                    {
                        IntFwDownload_Resource_Take();
                        dxBOOL bRC = IntDownloadFirmwareRequest();
                        IntFwDownload_Resource_Release();

                        if (bRC == dxFALSE)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING: Failed to Call IntDownloadFirmwareRequest()\r\n");
                            DownloadProcessRestart = dxTRUE;
                        }
                        else
                        {
                            if (pDownloadInfo->range.nFileSize)
                            {
                                uint8_t PercentageCompletion = (uint8_t)((((float)FwDownload.nServerFwTotalReceiveBytes)/pDownloadInfo->range.nFileSize)*100);

#if DEVICE_IS_HYBRID
                                IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_MAIN_UPDATING, PercentageCompletion);
#endif // DEVICE_IS_HYBRID
                            }
                        }
                    }
                    else if (DownloadState == DOWNLOAD_STATE_COMPLETE)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Download Main Fw completed!!! REBOOT later!!!\r\n");

#if DEVICE_IS_HYBRID
                        IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_MAIN_UPDATING, 100);
#endif // DEVICE_IS_HYBRID

                        //Mark reboot but do not reboot the system - we are going to reboot in safe mode (eg, turning off wifi, server manager, ect....)
                        dxMainImage_UpdateReboot(dxFALSE);

                        // Set Main Fw version to DUMMY that will force update firmware to Cloud Server after rebooting
                        IntSetMainFwToDUMMY();

                        IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);

                        LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_RESTART);
                        if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerRetCode);
                        }
                    }
                    else
                    {
                        DownloadProcessRestart = dxTRUE;
                    }

                }
                else if (SimpleStateMachine_StateNotChangingWhileEqualTo(FwUpdateSubStateHandler, FWUPDATE_SUBSTATE_GET_BLE_FW_DATA))
                {
                    IntFwDownload_Resource_Take();
                    DownloadState = IntParseBleFwMessage_DownloadFirmware(pDownloadInfo);
                    IntFwDownload_Resource_Release();

                    if (DownloadState == DOWNLOAD_STATE_DOWNLOADING)
                    {
                        IntFwDownload_Resource_Take();
                        dxBOOL bRC = IntDownloadFirmwareRequest();
                        IntFwDownload_Resource_Release();

                        if (bRC == dxFALSE)
                        {
                            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                             "WARNING: Failed to Call IntDownloadFirmwareRequest()\r\n");
                            DownloadProcessRestart = dxTRUE;
                        }
                        else
                        {
                            if (pDownloadInfo->range.nFileSize)
                            {
                                uint8_t PercentageCompletion = (uint8_t)((((float)FwDownload.nServerFwTotalReceiveBytes)/pDownloadInfo->range.nFileSize)*100);

#if DEVICE_IS_HYBRID
                                IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_BLE_DOWNLOADING, PercentageCompletion);
#endif // DEVICE_IS_HYBRID
                            }
                        }

                    }
                    else if (DownloadState == DOWNLOAD_STATE_COMPLETE)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Download Ble Fw completed!!!\r\n");

#if DEVICE_IS_HYBRID
                        IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_BLE_DOWNLOADING, 100);
#endif // DEVICE_IS_HYBRID

                        //Set Central to boot to SBL
                        BleManagerSetRoleToCentralFwUpdateBoot();

                        // Reboot
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                         "Ble Fw update finished.!!!\r\n");

                        IntChangeSubStateTo(get_firmware_update_sub_state_code());
                    }
                    else
                    {
                        DownloadProcessRestart = dxTRUE;
                    }
                }

                if (DownloadProcessRestart)
                {
                    LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_SWITCHING_TO_RESTART);

                    if (LedManagerRetCode != LED_MANAGER_SUCCESS)
                    {
                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                                         "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n", LedManagerRetCode);
                    }

                    //We try again the process starting from get fw version
                    IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_FW_VERSION);
                }
            }
        }
            break;

        case DKMSG_STATE_CHANGED:
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                             "FW Update DKMSG_STATE_CHANGED (%d)\r\n", EventHeader.s.StateMachineState);

            if (EventHeader.s.StateMachineState == DKSERVERMANAGER_STATEMACHINE_STATE_READY)
            {
            }
            else
            {
                // Suspend DKServerManager
                DKServerManager_State_ForceToSuspended();

                // Change to IDLE
                //NOTE: We cannot change back to IDLE if we are about to update NRF52
                //TODO: IMPORTANT we need to revise the BLE FW update flow!!
                if (SimpleStateMachine_GetCurrentState(FwUpdateSubStateHandler) != FWUPDATE_SUBSTATE_BLE_CENTRAL_FW_UPDATE_NRF52)
                {
                    CentralStateMachine_ChangeState(STATE_IDLE, NULL, 0);
                }
            }
        }
        break;

        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType);

                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n",
                     __FUNCTION__);

    return EventStateRet;
}


#if MQTT_IS_ENABLED
StateMachineEvent_State_t FwUpdate_EventMqttManager (
    void* arg
) {
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    return EventStateRet;
}
#endif


#if DEVICE_IS_HYBRID

StateMachineEvent_State_t FwUpdate_EventHpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n", __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    HpCmdMessageQueue_t *pHpCmdMessage = (HpCmdMessageQueue_t *) arg;

    switch (pHpCmdMessage->CommandMessage)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, pHpCmdMessage->CommandMessage);
                bReported = dxTRUE;
            }

        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}

#endif //#if DEVICE_IS_HYBRID

StateMachineEvent_State_t FwUpdate_EventWifiManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);

    switch (EventHeader.ReportEvent)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t FwUpdate_EventUdpManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);

    switch (EventHeader->PacketType)
    {
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                                "WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType);
                bReported = dxTRUE;
            }
        }
            break;
    }

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n",
                     __FUNCTION__);

    return EventStateRet;
}

StateMachineEvent_State_t FwUpdate_EventKeyPadManager(void* arg)
{

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s IN\r\n",
                     __FUNCTION__);

    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    //We will not be handling any key event here. It is not allow any action during FW Update

    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_DETAIL_INFORMATION,
                     "%s OUT\r\n", __FUNCTION__);

    return EventStateRet;
}


StateMachineEvent_State_t FwUpdate_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;

    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

StateMachine_result_t FwUpdate_Init(void* data)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    G_SYS_DBG_LOG_V("Update solution id 0x%x \r\n", BleManager_SolutionID_Get());

    FwUpdateSubStateHandler = SimpleStateMachine_Init(FwUpdateSubStateProcessFuncTable);

    IntFwDownload_Init();

    FwUpdate_SubState_Code_t NextSubState = FWUPDATE_SUBSTATE_IDLE;

    if (IntBleCentralInSBL() == dxTRUE)
    {
        // Ble Central is in SBL. Update Ble Fw
        NextSubState = get_firmware_update_sub_state_code();
    }
    else
    {
        //Switch BLE Manager back to central role
        BleManagerSetRoleToCentral();
    }

    IntChangeSubStateTo(NextSubState);

    return result;
}

StateMachine_result_t FwUpdate_MainProcess(void* data)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    // 1. If BLE in SBL, do BLE firmware update first.
    // 2. Download RTK firmware from cloud if server online
    // 3. Boot to new firmware
    // 4. Download BLE firmware from cloud if server online
    // 5. Update BLE firmware
    //
    // NOTE: If server/wifi connection issues, switch to offline

    //Kick start the sub state if its IDLE
    if (SimpleStateMachine_GetCurrentState(FwUpdateSubStateHandler) == FWUPDATE_SUBSTATE_IDLE)
    {
        //Get Fw Information first
        IntChangeSubStateTo(FWUPDATE_SUBSTATE_GET_FW_VERSION);
    }

    //Process the sub state machine
    SimpleStateMachine_result_t SmpResult = SimpleStateMachine_Process(FwUpdateSubStateHandler);
    if (SmpResult != SMP_STATE_MACHINE_RET_SUCCESS)
    {
        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "WARNING: SimpleStateMachine_Process failed (%d)\r\n", SmpResult);
    }

    return result;
}

StateMachine_result_t FwUpdate_End(void* data)
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;

    IntFwDownload_Resource_Take();
    IntFwDownload_Free();
    IntFwDownload_Resource_Release();
    dxMainImage_Stop();

    if (FwUpdateSubStateHandler) {
        SimpleStateMachine_Uninit(FwUpdateSubStateHandler);
        FwUpdateSubStateHandler = NULL;
    }

    return result;
}


////////////////////////////////////////////////////////////////////////////
// firmware update for Nordic NRF52
////////////////////////////////////////////////////////////////////////////

// Main substate for firmware update
static SimpleStateMachine_result_t SubState_BleCentralFwUpdate_NRF52(void* data, dxBOOL isFirstCall)
{
    uint8_t progress;
    static uint8_t pre_progress = 0;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (isFirstCall)
    {
        //G_SYS_DBG_LOG_V("NRF52 update 001 \r\n");
        IntPrepareBleCentralFwUpdate_NRF52();

        LedManager_result_t LedManagerRetCode = SetLedMode(LED_OPERATION_MODE_BLE_CENTRAL_FW_UPDATE);
        if (LedManagerRetCode != LED_MANAGER_SUCCESS)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: LedManager Failed to SetLedMode (ret error %d)\r\n",
                             LedManagerRetCode);
        }

        dxSubImgInfoHandle_t handle = dxSubImage_Init(DXSUBIMGBLOCK_ONE);

        if (handle == NULL)
        {
            G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_FW_UPDATE, G_SYS_DBG_LEVEL_FATAL_ERROR,
                             "WARNING: dxSubImage_Init() FAILED!\r\n");
            return SMP_STATE_MACHINE_RET_ERROR;
        }

        if (BleManager_ProgramFirmware_NRF52_Init(handle) != PROGRAM_FLASH_NRF52_SUCESS)
        {
            return SMP_STATE_MACHINE_RET_ERROR;
        }
        pre_progress = 0;
    }
    else
    {
        if (BleManager_ProgramFirmware_NRF52_Execution(&progress) != PROGRAM_FLASH_NRF52_SUCESS)
        {
            G_SYS_DBG_LOG_V("NRF52 update NG \r\n");
            IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);
        }
        else
        {

            if (pre_progress != progress)
            {
                pre_progress = progress;
#if DEVICE_IS_HYBRID
                IntSendFwUpdateInfoProgressToHp(FW_UPDATE_STATUS_BLE_UPDATING, progress);
#endif // DEVICE_IS_HYBRID
                G_SYS_DBG_LOG_V("NRF52 update progress %d \r\n", pre_progress);
            }

            if (progress == 100)
            {
                G_SYS_DBG_LOG_V("NRF52 update ok \r\n");
                IntChangeSubStateTo(FWUPDATE_SUBSTATE_RESET);
            }
        }
    }
    return result;
}





