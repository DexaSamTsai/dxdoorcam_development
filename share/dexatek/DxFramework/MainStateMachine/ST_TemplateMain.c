//============================================================================
// File: ST_TemplateMain.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/01/23
//     1) Description: Initial
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "DKServerManager.h"
#include "WifiCommManager.h"
#include "UdpCommManager.h"
#include "BLEManager.h"
#include "JobManager.h"
#include "dxKeypadManager.h"
#include "CentralStateMachineAutomata.h"

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enum
//============================================================================

//============================================================================
// Structure
//============================================================================

//============================================================================
// Global Var
//============================================================================

//============================================================================
// Internal Function
//============================================================================

//============================================================================
// Event Function
//============================================================================


StateMachineEvent_State_t Template_EventBLEManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventBLEManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    EventReportHeader_t EventHeader =  BleManagerGetEventObjHeader(arg);
    
    switch (EventHeader.ReportEvent)
    {            
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent );          
                bReported = dxTRUE;
            }            
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventBLEManager OUT\r\n");

    return EventStateRet;
}

StateMachineEvent_State_t Template_EventJobManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventJobManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    JobEventObjHeader_t* EventHeader =  JobManagerGetEventObjHeader(arg);
    
    switch (EventHeader->EventType)
    {            
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->EventType );          
                bReported = dxTRUE;
            }             
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventJobManager OUT\r\n");

    return EventStateRet;
}


StateMachineEvent_State_t Template_EventServerManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventServerManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    DKEventHeader_t EventHeader = DKServerManager_GetEventObjHeader(arg);
    
    switch (EventHeader.MessageType)
    {            
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.MessageType );          
                bReported = dxTRUE;
            }             
            
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventServerManager OUT\r\n");

    return EventStateRet;
}

StateMachineEvent_State_t Template_EventWifiManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventWifiManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    WifiCommEventReportHeader_t EventHeader = WifiCommManagerGetEventObjHeader(arg);
    
    switch (EventHeader.ReportEvent)
    {            
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader.ReportEvent );          
                bReported = dxTRUE;
            }             
            
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventWifiManager OUT\r\n");

    return EventStateRet;
}


StateMachineEvent_State_t Template_EventUdpManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventUdpManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    UdpPayloadHeader_t* EventHeader =  UdpCommManagerGetEventObjHeader(arg);
    
    switch (EventHeader->PacketType)
    {            
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, EventHeader->PacketType );          
                bReported = dxTRUE;
            }             
            
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventUdpManager OUT\r\n");

    return EventStateRet;
}

StateMachineEvent_State_t Template_EventKeyPadManager(void* arg)
{
    
    G_SYS_DBG_LOG_V("Template_EventKeyPadManager IN\r\n");
    
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    dxKeypadReport_t* KeyPadEvent = (dxKeypadReport_t*)arg;
      
    switch (KeyPadEvent->ReportIdentifier)
    {     
        case MAIN_BUTTON_REPORT_ID:
        {
            if (KeyPadEvent->KeyStateEvent == DXKEYPAD_KEY_PRESSED)
            {
                G_SYS_DBG_LOG_V("DXKEYPAD_KEY_PRESSED\r\n");
            }
            else if (KeyPadEvent->KeyStateEvent == DXKEYPAD_KEY_RELEASED)
            {
                G_SYS_DBG_LOG_V("DXKEYPAD_KEY_RELEASED\r\n");
                CentralStateMachine_ChangeState(STATE_OFFLINE, NULL, 0);                  
            }
            else if (KeyPadEvent->KeyStateEvent == DXKEYPAD_KEY_HELD)
            {
                G_SYS_DBG_LOG_V("DXKEYPAD_KEY_HELD\r\n");                
            }

        }
        break;
      
        default:
        {
            static dxBOOL bReported = dxFALSE;
            if (bReported == dxFALSE)
            {
                G_SYS_DBG_LOG_V("WARNING: Not handled by %s (%d), will report once ONLY\r\n", __FUNCTION__, KeyPadEvent->ReportIdentifier );          
                bReported = dxTRUE;
            }                         
        }
            break;
    }    
    
    G_SYS_DBG_LOG_V("Template_EventKeyPadManager OUT\r\n");

    return EventStateRet;
}


StateMachineEvent_State_t Template_EventLEDManager(void* arg)
{
    StateMachineEvent_State_t  EventStateRet = STATE_MACHINE_EVENT_NORMAL;
    
    return EventStateRet;
}


//============================================================================
// Function
//============================================================================

StateMachine_result_t Template_Init(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    
    return result;
}

StateMachine_result_t Template_MainProcess(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    
    return result;    
}

StateMachine_result_t Template_End(void* data )
{
    StateMachine_result_t result = STATE_MACHINE_RET_SUCCESS;
    
    return result;    
}


