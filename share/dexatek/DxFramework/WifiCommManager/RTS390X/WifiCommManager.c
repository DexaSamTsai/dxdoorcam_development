//============================================================================
// File: WifiCommManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <stdlib.h>

#include "dxOS.h"
#include "dxWIFI.h"
#include "dxWPACtrl.h"
#include "WifiCommManager.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

#define INTERNET_CONNECTION_CHECK_INTERVAL                  (15*SECONDS)

#define MAXIMUM_WIFI_COMM_JOB_WAIT_TIME                     (1*SECONDS)
#define MAXIMUM_WIFI_COMM_JOB_DATA_SIZE                     80
#define MAXIMUM_WIFI_COMM_JOB_QUEUE_DEPT_ALLOWED            2//1

#define IP_CLASS_A                                          10
#define IP_CLASS_B                                          172
#define IP_CLASS_C                                          192

#define WIFI_COMM_MANAGER_PROCESS_THREAD_PRIORITY           DXTASK_PRIORITY_IDLE
#define WIFI_COMM_MANAGER_PROCESS_THREAD_NAME               "WifiComm Manager Process"
#define WIFI_COMM_MANAGER_STACK_SIZE                        (1024*60)

#define WIFI_DEFAULT_WORKER_PRIORITY                            DXTASK_PRIORITY_IDLE
#define WIFI_COMM_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE         1024
#define WIFI_COMM_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE    30

#define WIFI_SCAN_AP_INFO_POINTER_CONTAINER_SIZE                30

/******************************************************
 *                   Enumerations
 ******************************************************/

typedef enum
{

    WIFI_COMM_MANAGER_CONNECTION_DOWN                = 0,
    WIFI_COMM_MANAGER_SCANNING_AP,
    WIFI_COMM_MANAGER_SCANNING_AP_NEW_AVAILABLE_IN_LIST,
    WIFI_COMM_MANAGER_SCANNING_AP_ENDED,
    WIFI_COMM_MANAGER_SEARCHING_FOR_SSID,
    WIFI_COMM_MANAGER_SEARCH_SSID_FOUND_AND_SAVED,
    WIFI_COMM_MANAGER_SEARCH_ENDED_SSID_NOT_FOUND,
    WIFI_COMM_MANAGER_SEARCH_SSID_ENCRYPTION_TYPE_UNSUPPORTED,
    WIFI_COMM_MANAGER_SEARCH_SSID_PASSWORD_REQUIRED,
    WIFI_COMM_MANAGER_SEARCH_INTERNAL_ERROR,
    WIFI_COMM_MANAGER_CONNECTION_UP,
    WIFI_COMM_MANAGER_CONNECTION_UP_BUT_UNABLE_TO_REACH_TO_WWW,

} WifiCommManager_Status_t;


typedef enum
{

    WIFI_COMM_MANAGER_NO_JOB                = 0,
    WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID,
    WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID_DIRECT,
    WIFI_COMM_MANAGER_JOB_DISCONNECT,
    WIFI_COMM_MANAGER_JOB_SCAN_AP,

} WifiCommManager_Job_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

/******************************************************
 *                    Structures
 ******************************************************/

typedef struct
{

    WifiCommManager_Job_t   JobType;
    uint16_t                JobSourceOfOrigin;
    uint32_t                JobIdentificationNumber;
    dxTime_t                JobTimeOutTime;
    uint8_t                 JobData[MAXIMUM_WIFI_COMM_JOB_DATA_SIZE];

} WifiCommJobQueue_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data


typedef struct
{
    uint8_t             SSID[SSID_MAX_NAME_LEN+1];
    uint8_t             Password[PASSWORD_MAX_NAME_LEN+1];
    dxWiFi_Security_t   SecurityType;

} WifiConnectInfo_t;


/******************************************************
 *               Function Declarations
 ******************************************************/


static TASK_RET_TYPE WifiComm_Manager_Process_thread_main( TASK_PARAM_TYPE );

static dxWIFI_RET_CODE scan_result_handler( dxWiFi_Scan_Result_t* malloced_scan_result );

/******************************************************
 *               Variable Definitions
 ******************************************************/

static dxTaskHandle_t           WifiCommMainProcess_thread;

static dxWorkweTask_t           EventWorkerThread;

static dxQueueHandle_t          WifiCommJobQueue;

static dxSemaphoreHandle_t      ScanConnectResultSemaphore;

static dxEventHandler_t         RegisteredEventFunction;

static WifiCommManager_Status_t gCurrentWifiStatus;

static WifiCommDeviceMac_t      DeviceMacAddr = { { 0, 0, 0, 0, 0, 0, 0, 0 } };

static dxSemaphoreHandle_t      NetworkThreadTaskAccessSemaphore;

static dxWiFi_Scan_Details_t*   pScanResultList[WIFI_SCAN_AP_INFO_POINTER_CONTAINER_SIZE];

static WifiConnectInfo_t        gConnectApRecord;

static dxBOOL                   gbBreakWifiCommSearch;

/******************************************************
 *               Local Function Definitions
 ******************************************************/
dxBOOL SaveWifiConnectionInfo(WifiConnectInfo_t* wConnectInfo)
{
	memset(&gConnectApRecord, 0, sizeof(WifiConnectInfo_t) );
	memcpy(&gConnectApRecord, wConnectInfo, sizeof(WifiConnectInfo_t));
	return dxTRUE;
}

/*
 * Callback function to handle scan results
 */
dxWIFI_RET_CODE scan_result_handler( dxWiFi_Scan_Result_t* malloced_scan_result )
{
    WifiCommJobQueue_t	*JobInfo		= (WifiCommJobQueue_t*)malloced_scan_result->user_data;
    WifiConnectInfo_t	*ConnectInfo	= (WifiConnectInfo_t*)JobInfo->JobData;

    if ( !malloced_scan_result->scan_complete ) {
        dxWiFi_Scan_Details_t *AP_Record = &malloced_scan_result->ap_details;

		if ( JobInfo->JobType == WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID ) {
			if ( ( gCurrentWifiStatus == WIFI_COMM_MANAGER_SEARCHING_FOR_SSID ) &&
				 ( !strcmp( ( char * )ConnectInfo->SSID, ( char * )AP_Record->SSID.val ) ) ) {
				// Print other network characteristics
				G_SYS_DBG_LOG_V1(  "\tSSID Found    : %s\r\n", ( char * )AP_Record->SSID.val );
				G_SYS_DBG_LOG_V6(  "\tBSSID         : %02X:%02X:%02X:%02X:%02X:%02X\r\n",	AP_Record->BSSID.octet[0], AP_Record->BSSID.octet[1], AP_Record->BSSID.octet[2],
																							AP_Record->BSSID.octet[3], AP_Record->BSSID.octet[4], AP_Record->BSSID.octet[5] );
				G_SYS_DBG_LOG_V1(  "\tRSSI          : %ddBm\r\n", AP_Record->signal_strength  );
				G_SYS_DBG_LOG_V1(  "\tNetwork Type  : %s\r\n",	( AP_Record->bss_type == DXWIFI_BSS_TYPE_INFRASTRUCTURE )	? "Infrastructure" :
																( AP_Record->bss_type == DXWIFI_BSS_TYPE_ADHOC ) ? "Ad hoc" : "Unknown"  );
				G_SYS_DBG_LOG_V1(  "\tSecurity      : %s\r\n",	( AP_Record->security == DXWIFI_SECURITY_OPEN )				? "Open" :
																( AP_Record->security == DXWIFI_SECURITY_WEP_PSK )			? "WEP" :
																( AP_Record->security == DXWIFI_SECURITY_WPA_TKIP_PSK )		? "WPA TKIP" :
																( AP_Record->security == DXWIFI_SECURITY_WPA_AES_PSK )		? "WPA AES" :
																( AP_Record->security == DXWIFI_SECURITY_WPA2_AES_PSK )		? "WPA2 AES" :
																( AP_Record->security == DXWIFI_SECURITY_WPA2_TKIP_PSK )	? "WPA2 TKIP" :
																( AP_Record->security == DXWIFI_SECURITY_WPA2_MIXED_PSK )	? "WPA2 Mixed" :
																( AP_Record->security == DXWIFI_SECURITY_WPA_WPA2_MIXED )	? "WPA/WPA2 Mixed":
																"Unknown"  );
				G_SYS_DBG_LOG_V1(  "\tRadio Band    : %s\r\n", ( AP_Record->band == DXWIFI_802_11_BAND_5GHZ ) ? "5GHz" : "2.4GHz"  );
				G_SYS_DBG_LOG_V1(  "\tChannel       : %d\r\n", AP_Record->channel  );

				if ( AP_Record->security == DXWIFI_SECURITY_UNKNOWN ) {
					gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCH_SSID_ENCRYPTION_TYPE_UNSUPPORTED;
				} else {
					uint8_t PasswordLen = 0;

					ConnectInfo->SecurityType = AP_Record->security;
					G_SYS_DBG_LOG_V1(  "Security Type RAW = 0x%x\r\n", AP_Record->security );

					if ( ConnectInfo->Password[ 0 ] != '\0' ) {
						PasswordLen = strlen( ( char * )ConnectInfo->Password );
					}

					if ( ( AP_Record->security != DXWIFI_SECURITY_OPEN ) && ( PasswordLen == 0 ) ) {
						gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCH_SSID_PASSWORD_REQUIRED;
					} else {
						gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCH_SSID_FOUND_AND_SAVED;
						if ( !SaveWifiConnectionInfo( ConnectInfo ) ) {
							gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCH_INTERNAL_ERROR;
						}
					}
				}
			}
		} else if ( JobInfo->JobType == WIFI_COMM_MANAGER_JOB_SCAN_AP ) {
			//Find available slot
			uint8_t i = 0;

			for ( i = 0; i < WIFI_SCAN_AP_INFO_POINTER_CONTAINER_SIZE; i ++ ) {
				if ( pScanResultList[ i ] == NULL ) {
					dxWiFi_Scan_Details_t *ScanResultSlot	= dxMemAlloc( "Scan Ap info Buff", 1, sizeof( dxWiFi_Scan_Details_t ) );

					if ( ScanResultSlot ) {
						memcpy( ScanResultSlot, &malloced_scan_result->ap_details, sizeof( dxWiFi_Scan_Details_t ) );
					} else {
						G_SYS_DBG_LOG_V(  "FATAL - dxMemAlloc return NULL\r\n" );
					}

					pScanResultList[ i ] = ScanResultSlot;
					break;
				}
			}
		}
    } else {	//End of scan
        if ( JobInfo->JobType == WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID ) {
            if ( gCurrentWifiStatus == WIFI_COMM_MANAGER_SEARCHING_FOR_SSID ) {
                gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCH_ENDED_SSID_NOT_FOUND;
            }
        } else if ( JobInfo->JobType == WIFI_COMM_MANAGER_JOB_SCAN_AP ) {
            gCurrentWifiStatus = WIFI_COMM_MANAGER_SCANNING_AP_ENDED;
        }

        //NOTE: Always send the outcome at the end of scan because if we send it earlier and establish connection before scan ends, memory leak will occur!
        //However, with an exception from above WIFI_COMM_MANAGER_JOB_SCAN_AP which purely reports scanned AP.
        if ( dxSemGive( ScanConnectResultSemaphore ) != DXOS_SUCCESS ) {
            G_SYS_DBG_LOG_V(  "FATAL - Scan AP unable to set ScanConnectResultSemaphore\r\n" );
        }
    }

    return DXWIFI_SUCCESS;
}

WifiCommManager_result_t StartNetworkAndCheckIPAddress( void )
{
    //char ip_addr[4]; /* We only support IPV4 for now */
    uint8_t IpClass;
    dxNet_Addr_t dxNet_Addr;

    WifiCommManager_result_t result;

    dxDHCPC_Init();

	if ( dxNET_Info( &dxNet_Addr ) == DXNET_SUCCESS ) {
        IpClass = ( uint8_t )dxNet_Addr.ip_addr.v4[ 0 ];

        if ( ( IpClass == IP_CLASS_A ) ||
			 ( IpClass == IP_CLASS_B ) ||
			 ( IpClass == IP_CLASS_C ) ) {
            result = WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED;
        } else if ( ( dxNet_Addr.ip_addr.v4[ 0 ] == 0 ) &&
					( dxNet_Addr.ip_addr.v4[ 1 ] == 0 ) &&
					( dxNet_Addr.ip_addr.v4[ 2 ] == 0 ) &&
					( dxNet_Addr.ip_addr.v4[ 3 ] == 0 ) ) {
        	result = WIFI_COMM_MANAGER_WATING_FOR_IP_ASSIGNMENT;
        } else {
        	result = WIFI_COMM_MANAGER_REQUEST_IP_FAILED;
		}
    } else {
        result = WIFI_COMM_MANAGER_WATING_FOR_IP_ASSIGNMENT;
    }

    return result;
}

void SendWifiCommEventReportObj(WifiCommEventReportObj_t* Obj)
{
    if (RegisteredEventFunction && Obj)
    {
        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, Obj);

        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "FATAL - WifiComm Failed to send Event message back to Application\r\n" );

            if (Obj->pData)
                dxMemFree(Obj->pData);

            dxMemFree(Obj);
        }
    }
}

static WifiCommManager_result_t
wifi_connect( char *ssid, dxWiFi_Security_t security_type, char *password, int key_id, dxSemaphoreHandle_t *dxsemaphore )
{
	WifiCommManager_result_t result = WIFI_COMM_MANAGER_WRONG_PASSWORD;

	dxWIFI_RET_CODE wifiresult = dxWiFi_Connect( ( char * )gConnectApRecord.SSID,
					                                gConnectApRecord.SecurityType,
					                                ( char * )gConnectApRecord.Password,
					                                0,
					                                NULL);

	if ( wifiresult == DXWIFI_SUCCESS ) {
		int counter = 0;

		while ( counter < 10 ) {
			usleep( 500000 );

			if ( counter & 0x01 ) {
				if ( dxWiFi_IsOnline( DXWIFI_MODE_STA ) == DXWIFI_SUCCESS ) {
					result = StartNetworkAndCheckIPAddress();
					if ( result != WIFI_COMM_MANAGER_WATING_FOR_IP_ASSIGNMENT ) {
						break;
					}
				}
			}

			counter ++;
		}

		if ( result != WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED ) {
			dxWiFi_Disconnect();
		}
	}

	return result;
}


static TASK_RET_TYPE WifiComm_Manager_Process_thread_main( void* arg )
{
    static WifiCommJobQueue_t 	CurrentJob;
    CurrentJob.JobType = WIFI_COMM_MANAGER_NO_JOB;

    while (1)
    {
        WifiCommJobQueue_t *pGetJobBuff = NULL;
        if (dxQueueReceive(  WifiCommJobQueue,
                             &pGetJobBuff,
                             MAXIMUM_WIFI_COMM_JOB_WAIT_TIME) == DXOS_SUCCESS)
        {
            if (pGetJobBuff)
            {
                memcpy(&CurrentJob, pGetJobBuff, sizeof(WifiCommJobQueue_t));
                dxMemFree(pGetJobBuff);

                switch(CurrentJob.JobType)
                {
                    case WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID_DIRECT:
                    {
                        dxWiFi_Security_t   NextSecurityTry = DXWIFI_SECURITY_UNKNOWN;
                        dxBOOL              bAllSecurityAutoDiscovery = dxFALSE;
                        dxWiFi_Security_t   ConfirmedSecurity = DXWIFI_SECURITY_UNKNOWN;

                        gbBreakWifiCommSearch = dxFALSE;
                        gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCHING_FOR_SSID;

                        //NOTE: Since this is a "Direct" connection (eg, Hidden SSID) we won't actually know if it's either SSID not found
                        //or incorrect password.. So for now we simply feedback SSID not found.
                        WifiCommManager_result_t FeedbackStatus = WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID;

                        WifiConnectInfo_t* pConnectInfo = (WifiConnectInfo_t*)CurrentJob.JobData;

                        switch (pConnectInfo->SecurityType)
                        {
                            case DXWIFI_SECURITY_UNKNOWN:
                            {
                                //We start from very beginning and set the auto discovery flag
                                NextSecurityTry = DXWIFI_SECURITY_OPEN;
                                bAllSecurityAutoDiscovery = dxTRUE;
                            }
                            break;

                            case DXWIFI_SECURITY_OPEN:
                                NextSecurityTry = DXWIFI_SECURITY_OPEN;
                            break;

                            case DXWIFI_SECURITY_WEP_PSK:
                            case DXWIFI_SECURITY_WEP_SHARED:
                                G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WEP_PSK\r\n");
                                NextSecurityTry = DXWIFI_SECURITY_WEP_PSK; //Just select the first as we are going to try all WEP
                            break;

                            case DXWIFI_SECURITY_WPA_TKIP_PSK:
                            case DXWIFI_SECURITY_WPA_AES_PSK:
                                G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA_TKIP_PSK\r\n");
                                NextSecurityTry = DXWIFI_SECURITY_WPA_TKIP_PSK; //Just select the first as we are going to try all WPA
                            break;

                            case DXWIFI_SECURITY_WPA2_AES_PSK:
                            case DXWIFI_SECURITY_WPA2_TKIP_PSK:
                            case DXWIFI_SECURITY_WPA2_MIXED_PSK:
                                G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA2_AES_PSK\r\n");
                                NextSecurityTry = DXWIFI_SECURITY_WPA2_AES_PSK; //Just select the first as we are going to try all WPA2
                            break;

                            case DXWIFI_SECURITY_WPA_WPA2_MIXED:
                                G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA_WPA2_MIXED\r\n");
                                NextSecurityTry = DXWIFI_SECURITY_WPA_WPA2_MIXED;
                            break;

                            default:

                            break;

                        }

                        //G_SYS_DBG_LOG_V2( "Connecting to SSID = %s Len = %d\r\n", SsidToConnect.value, SsidToConnect.length);
                        //G_SYS_DBG_LOG_V2( "Connecting to Password = %s Len = %d\r\n", pConnectInfo->Password, strlen((char*)pConnectInfo->Password));
                        while (NextSecurityTry != DXWIFI_SECURITY_UNKNOWN)
                        {
                            dxWIFI_RET_CODE wifiresult = dxWiFi_Connect((char*)pConnectInfo->SSID,
                                                                    NextSecurityTry,
                                                                    (char*)pConnectInfo->Password,
                                                                    0,
                                                                    NULL);

                            if (wifiresult == DXWIFI_SUCCESS)
                            {
		                        WifiCommManager_result_t result = WIFI_COMM_MANAGER_SCAN_AP_TIME_OUT;
								int counter = 0;

								while ( ( gbBreakWifiCommSearch == dxFALSE ) && ( counter < 10 ) ) {
									usleep( 500000 );

									if ( counter & 0x01 ) {
										if ( dxWiFi_IsOnline( DXWIFI_MODE_STA ) == DXWIFI_SUCCESS ) {
			                                result = StartNetworkAndCheckIPAddress();
											if ( result != WIFI_COMM_MANAGER_WATING_FOR_IP_ASSIGNMENT ) {
												break;
											}
										}
									}

                                    counter ++;
								}

                                if (result == WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED)
                                {
                                    FeedbackStatus = WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED;
                                    gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_UP;
                                    ConfirmedSecurity = NextSecurityTry; //Record the found connected security
                                    NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //We done so lets set the NextSecurityTry to UNKNOWN
                                }
                                else if (gbBreakWifiCommSearch == dxTRUE)
                                {
                                    gbBreakWifiCommSearch = dxFALSE;
                                    dxWiFi_Disconnect();
                                    break;
                                }
                                else
                                {
                                    dxWiFi_Disconnect();
                                }
                            }

                            if (FeedbackStatus != WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED)
                            {
                                switch (NextSecurityTry)
                                {
                                    case DXWIFI_SECURITY_OPEN:
                                    {
                                        if(bAllSecurityAutoDiscovery)
                                        {
                                            NextSecurityTry = DXWIFI_SECURITY_WPA_WPA2_MIXED;
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V( "Trying NONE\r\n");
                                            NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //No more tries Exit the loop
                                        }
                                    }
                                    break;

                                    case DXWIFI_SECURITY_WPA_WPA2_MIXED:
                                    {
                                        if(bAllSecurityAutoDiscovery)
                                        {
                                            NextSecurityTry = DXWIFI_SECURITY_WEP_PSK;
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V( "Trying NONE\r\n");
                                            NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //No more tries Exit the loop
                                        }
                                    }
                                    break;

                                    //WEP Tries
                                    case DXWIFI_SECURITY_WEP_PSK:
                                        G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WEP_SHARED\r\n");
                                        NextSecurityTry = DXWIFI_SECURITY_WEP_SHARED; //Next try DXWIFI_SECURITY_WEP_SHARED
                                    break;

                                    case DXWIFI_SECURITY_WEP_SHARED:
                                    {
                                        if(bAllSecurityAutoDiscovery)
                                        {
                                            NextSecurityTry = DXWIFI_SECURITY_WPA_TKIP_PSK;
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V( "Trying NONE\r\n");
                                            NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //No more tries Exit the loop
                                        }
                                    }
                                    break;

                                    //WPA Tries
                                    case DXWIFI_SECURITY_WPA_TKIP_PSK:
                                        G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA_AES_PSK\r\n");
                                        NextSecurityTry = DXWIFI_SECURITY_WPA_AES_PSK; //Next try DXWIFI_SECURITY_WPA_AES_PSK
                                    break;

                                    case DXWIFI_SECURITY_WPA_AES_PSK:
                                    {
                                        if(bAllSecurityAutoDiscovery)
                                        {
                                            NextSecurityTry = DXWIFI_SECURITY_WPA2_AES_PSK;
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V( "Trying NONE\r\n");
                                            NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //No more tries Exit the loop
                                        }
                                    }
                                    break;

                                    //WPA2 Tries
                                    case DXWIFI_SECURITY_WPA2_AES_PSK:
                                        G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA2_TKIP_PSK\r\n");
                                        NextSecurityTry = DXWIFI_SECURITY_WPA2_TKIP_PSK; //Next try DXWIFI_SECURITY_WPA2_TKIP_PSK
                                    break;

                                    case DXWIFI_SECURITY_WPA2_TKIP_PSK:
                                        G_SYS_DBG_LOG_V( "Trying DXWIFI_SECURITY_WPA2_MIXED_PSK\r\n");
                                        NextSecurityTry = DXWIFI_SECURITY_WPA2_MIXED_PSK; //Next try DXWIFI_SECURITY_WPA2_MIXED_PSK
                                    break;

                                    case DXWIFI_SECURITY_WPA2_MIXED_PSK:
                                    {
                                        //Regardless of bAllSecurityAutoDiscovery or not, we end here to prevent infinite loop
                                        G_SYS_DBG_LOG_V( "Trying NONE\r\n");
                                        NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //No more tries Exit the loop
                                    }
                                    break;

                                    default:
                                        NextSecurityTry = DXWIFI_SECURITY_UNKNOWN; //Exit the loop
                                    break;
                                }
                            }
                        }

                        //If Wifi status connection is not up by the time we get here we'll need to set it to connection down status
                        if (gCurrentWifiStatus != WIFI_COMM_MANAGER_CONNECTION_UP)
                            gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_DOWN;

                        //Report feedback
                        WifiCommEventReportObj_t* WifiCommReportEvent = dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));
                        WifiCommReportEvent->Header.ReportEvent = WIFI_COMM_EVENT_COMMUNICATION;
                        WifiCommReportEvent->Header.ResultState = FeedbackStatus;
                        WifiCommReportEvent->Header.SourceOfOrigin = CurrentJob.JobSourceOfOrigin;
                        WifiCommReportEvent->Header.IdentificationNumber = CurrentJob.JobIdentificationNumber;

                        dxWiFi_Security_t* pWifiSecurity = dxMemAlloc( "WifiSecurityType", 1, sizeof(dxWiFi_Security_t));
                        memcpy(pWifiSecurity, &ConfirmedSecurity, sizeof(dxWiFi_Security_t));
                        WifiCommReportEvent->pData = (uint8_t*)pWifiSecurity;

                        SendWifiCommEventReportObj(WifiCommReportEvent);

                    }
                    break;

                    case WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID:
                    {
						WifiCommManager_result_t	FeedbackStatus = WIFI_COMM_INTERNAL_GENERIC_ERROR;
						dxWiFi_Security_t			*pWifiSecurity = NULL;

						int retry;

						for ( retry = 0; retry < 2; retry ++ ) {
							//Let's do retry (max of 2) before reprorting issue back to application
							dxWIFI_RET_CODE wifiresult = dxWiFi_Scan( scan_result_handler, &CurrentJob );

							if ( wifiresult == DXWIFI_SUCCESS ) {
								gCurrentWifiStatus = WIFI_COMM_MANAGER_SEARCHING_FOR_SSID;

								if ( dxSemTake( ScanConnectResultSemaphore, 60 * SECONDS ) == DXOS_SUCCESS ) {
									if ( gCurrentWifiStatus == WIFI_COMM_MANAGER_SEARCH_ENDED_SSID_NOT_FOUND ) {
										FeedbackStatus = WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID;
										sleep( 2 );
										continue;
									}

									switch ( gCurrentWifiStatus ) {
										case WIFI_COMM_MANAGER_SEARCH_SSID_FOUND_AND_SAVED:
											FeedbackStatus = wifi_connect( ( char * )gConnectApRecord.SSID,
																			gConnectApRecord.SecurityType,
																			( char * )gConnectApRecord.Password,
																			0,
																			NULL);

											if ( FeedbackStatus == WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED ) {
												gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_UP;

												pWifiSecurity = dxMemAlloc( "WifiSecurityType", 1, sizeof( dxWiFi_Security_t ) );
												memcpy( pWifiSecurity, &gConnectApRecord.SecurityType, sizeof( dxWiFi_Security_t ) );
											}
											break;

										case WIFI_COMM_MANAGER_SEARCH_SSID_ENCRYPTION_TYPE_UNSUPPORTED:
											FeedbackStatus = WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED;
											break;

										case WIFI_COMM_MANAGER_SEARCH_SSID_PASSWORD_REQUIRED:
											FeedbackStatus = WIFI_COMM_MANAGER_PASSWORD_REQUIRED;
											break;

										case WIFI_COMM_MANAGER_SEARCH_INTERNAL_ERROR:
											FeedbackStatus = WIFI_COMM_INTERNAL_API_ERROR;
											break;

										default:
											break;
									}
								} else {	//Scan timeout!
									G_SYS_DBG_LOG_V( "FATAL - Scan TimeOut!... This should not happen, check reason\r\n" );
									FeedbackStatus = WIFI_COMM_MANAGER_SCAN_SSID_TIME_OUT;
								}
							} else {
								FeedbackStatus = WIFI_COMM_INTERNAL_GENERIC_ERROR;
							}

							break;
						}

						//If Wifi status connection is not up by the time we get here we'll need to set it to connection down status
						if ( gCurrentWifiStatus != WIFI_COMM_MANAGER_CONNECTION_UP ) {
							gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_DOWN;
						}

						//Report feedback
						WifiCommEventReportObj_t* WifiCommReportEvent = dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));
						WifiCommReportEvent->Header.ReportEvent = WIFI_COMM_EVENT_COMMUNICATION;
						WifiCommReportEvent->Header.ResultState = FeedbackStatus;
						WifiCommReportEvent->Header.SourceOfOrigin = CurrentJob.JobSourceOfOrigin;
						WifiCommReportEvent->Header.IdentificationNumber = CurrentJob.JobIdentificationNumber;
						WifiCommReportEvent->pData = (uint8_t *)pWifiSecurity;

						SendWifiCommEventReportObj(WifiCommReportEvent);
					}

					break;

                    case WIFI_COMM_MANAGER_JOB_DISCONNECT:
                    {
                        dxDHCPC_Release();
                        dxWiFi_Disconnect();

                        WifiCommEventReportObj_t* WifiCommReportEvent = dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));

                        WifiCommReportEvent->Header.ReportEvent = WIFI_COMM_EVENT_COMMUNICATION;
                        WifiCommReportEvent->Header.ResultState = WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER;
                        WifiCommReportEvent->Header.SourceOfOrigin = CurrentJob.JobSourceOfOrigin;
                        WifiCommReportEvent->Header.IdentificationNumber = CurrentJob.JobIdentificationNumber;
                        WifiCommReportEvent->pData = NULL;

                        //Update our state
                        gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_DOWN;

                        SendWifiCommEventReportObj(WifiCommReportEvent);

                    }
                    break;

					case WIFI_COMM_MANAGER_JOB_SCAN_AP:
					{
						uint8_t     ApInfoReportIndex = 0;
						WifiCommManager_result_t    FeedbackStatus = WIFI_COMM_INTERNAL_GENERIC_ERROR;

						//Clean all allocated scan AP info
						for ( ApInfoReportIndex = 0; ApInfoReportIndex < WIFI_SCAN_AP_INFO_POINTER_CONTAINER_SIZE; ApInfoReportIndex ++ ) {
							if ( pScanResultList[ ApInfoReportIndex ] != NULL ) {
                                G_SYS_DBG_LOG_V( "FATAL - pScanResultList[ %d ] is not NULL, This should not happen\r\n", ApInfoReportIndex );
								dxMemFree( pScanResultList[ ApInfoReportIndex ] );
								pScanResultList[ ApInfoReportIndex ] = NULL;
							}
						}

						int retry;

						for ( retry = 0; retry < 2; retry ++ ) {
							//Let's do retry (max of 2) before reprorting issue back to application
							dxWIFI_RET_CODE wifiresult = dxWiFi_Scan( scan_result_handler, &CurrentJob );

							if ( wifiresult == DXWIFI_SUCCESS ) {
								gCurrentWifiStatus = WIFI_COMM_MANAGER_SCANNING_AP;

								if ( dxSemTake( ScanConnectResultSemaphore, 10 * SECONDS ) == DXOS_SUCCESS ) {
									if ( pScanResultList[ 0 ] == NULL ) {
										if ( retry == 0 ) {
											sleep( 2 );
											continue;
										}
									} else {
										for ( ApInfoReportIndex = 0; ApInfoReportIndex < WIFI_SCAN_AP_INFO_POINTER_CONTAINER_SIZE; ApInfoReportIndex ++ ) {
											if ( pScanResultList[ ApInfoReportIndex ] ) {
												WifiCommEventReportObj_t* WifiCommReportEvent		= dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));

												WifiCommReportEvent->Header.ReportEvent				= WIFI_COMM_EVENT_SCAN_AP;
												WifiCommReportEvent->Header.ResultState				= WIFI_COMM_MANAGER_SCAN_AP_REPORT;
												WifiCommReportEvent->Header.SourceOfOrigin			= CurrentJob.JobSourceOfOrigin;
												WifiCommReportEvent->Header.IdentificationNumber	= CurrentJob.JobIdentificationNumber;
												WifiCommReportEvent->pData							= ( uint8_t * )pScanResultList[ ApInfoReportIndex ];

												SendWifiCommEventReportObj( WifiCommReportEvent );

												pScanResultList[ ApInfoReportIndex ] = NULL;
											}
										}
									}

									FeedbackStatus = WIFI_COMM_MANAGER_SCAN_AP_REPORT_ENDED;
								} else {
									G_SYS_DBG_LOG_V( "FATAL - Scan AP TimeOut!... This should not happen, check reason\r\n" );

									FeedbackStatus = WIFI_COMM_MANAGER_SCAN_AP_TIME_OUT;
								}

								//NOTE: After scanning we are going to set the status to connection down because this is the original state as
								//		scan is not possible while connection is up.
								gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_DOWN;
							} else {
								G_SYS_DBG_LOG_V1( "WARNING - dxWiFi_Scan failed with result = %d\r\n", wifiresult );

								FeedbackStatus	= WIFI_COMM_MANAGER_SCAN_AP_INTERNAL_ERROR;
							}

							break;
						}

						//Report feedback
						WifiCommEventReportObj_t* WifiCommReportEvent		= dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));

						WifiCommReportEvent->Header.ReportEvent				= WIFI_COMM_EVENT_SCAN_AP;
						WifiCommReportEvent->Header.ResultState				= FeedbackStatus;
						WifiCommReportEvent->Header.SourceOfOrigin			= CurrentJob.JobSourceOfOrigin;
						WifiCommReportEvent->Header.IdentificationNumber	= CurrentJob.JobIdentificationNumber;
						WifiCommReportEvent->pData							= NULL;

						SendWifiCommEventReportObj( WifiCommReportEvent );

						break;
					}

                    default:
                        break;
                }

            }
        }
        else //If there is no JOB, Let's check if connection is up or down
        {
#if 1   //TODO - enable back when dxwifi_on is fixed (eg, if dxwifi_on fails calling dxWiFi_IsOnline will crash)

            dxWIFI_RET_CODE NetworkIsUp = dxWiFi_IsOnline(DXWIFI_MODE_STA);

            if ((NetworkIsUp != DXWIFI_SUCCESS) && (gCurrentWifiStatus == WIFI_COMM_MANAGER_CONNECTION_UP))
            {
                dxDHCPC_Release();
                //dxWiFi_Disconnect(); //NOTE: RTK Ameba will crash if we call diconnect in here when we detect connection is not longer valid

                gCurrentWifiStatus = WIFI_COMM_MANAGER_CONNECTION_DOWN;

                if (RegisteredEventFunction)
                {
                    WifiCommEventReportObj_t* WifiCommReportEvent = dxMemAlloc( "WifiComm report event Buff", 1, sizeof(WifiCommEventReportObj_t));

                    WifiCommReportEvent->Header.ReportEvent = WIFI_COMM_EVENT_COMMUNICATION;
                    WifiCommReportEvent->Header.ResultState = WIFI_COMM_MANAGER_CONNECTION_IS_DOWN;
                    WifiCommReportEvent->Header.IdentificationNumber = 0;
                    WifiCommReportEvent->pData = NULL;

                    dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, WifiCommReportEvent);

                    if (EventResult != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_V( "FATAL - WifiComm Failed to send Event message back to Application\r\n" );
                        dxMemFree(WifiCommReportEvent);
                    }

                }
            }
#endif
        }
    }

    return TASK_RET_GENERIC;
}

/******************************************************
 *               Function Definitions
 ******************************************************/

const WifiCommDeviceMac_t* WifiCommManagerGetDeviceMac(void)
{
    WifiCommDeviceMac_t* MacAddress = NULL;

    if(DeviceMacAddr.MacAddr[0] == 0)
    {
		dxNet_Addr_t dxNetAddr;

		if ( dxNET_Info( &dxNetAddr ) == DXNET_SUCCESS ) {
            DeviceMacAddr.MacAddr[0] = dxNetAddr.mac_addr[0];
            DeviceMacAddr.MacAddr[1] = dxNetAddr.mac_addr[1];
            DeviceMacAddr.MacAddr[2] = dxNetAddr.mac_addr[2];
            DeviceMacAddr.MacAddr[3] = 0xFF;
            DeviceMacAddr.MacAddr[4] = 0xFF;
            DeviceMacAddr.MacAddr[5] = dxNetAddr.mac_addr[3];
            DeviceMacAddr.MacAddr[6] = dxNetAddr.mac_addr[4];
            DeviceMacAddr.MacAddr[7] = dxNetAddr.mac_addr[5];

            MacAddress = &DeviceMacAddr;
        }
    }
    else
    {
        MacAddress = &DeviceMacAddr;
    }

    return MacAddress;
}


WifiCommManager_result_t WifiCommManagerInit(event_handler_t EventFunction)
{

    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;

    ScanConnectResultSemaphore = dxSemCreate(1, 0);
    if (!ScanConnectResultSemaphore)
    {
        G_SYS_DBG_LOG_V(  "Error creating ScanConnectResultSemaphore \r\n" );
        result = WIFI_COMM_INTERNAL_RTOS_ERROR;
        goto EndInit;
    }

    NetworkThreadTaskAccessSemaphore = dxSemCreate(1, 0);
    if (!NetworkThreadTaskAccessSemaphore)
    {
        G_SYS_DBG_LOG_V(  "Error creating NetworkThreadTaskAccessSemaphore \r\n" );
        result = WIFI_COMM_INTERNAL_RTOS_ERROR;
        goto EndInit;
    }

    WifiCommJobQueue = dxQueueCreate(MAXIMUM_WIFI_COMM_JOB_QUEUE_DEPT_ALLOWED, sizeof(WifiCommJobQueue_t*));
    if (!WifiCommJobQueue)
    {
        G_SYS_DBG_LOG_V(  "Error creating WifiCommJobQueue \r\n" );
        result = WIFI_COMM_INTERNAL_RTOS_ERROR;
        goto EndInit;
    }


    if  (dxTaskCreate(WifiComm_Manager_Process_thread_main,
                           WIFI_COMM_MANAGER_PROCESS_THREAD_NAME,
                           WIFI_COMM_MANAGER_STACK_SIZE,
                           NULL,
                           WIFI_COMM_MANAGER_PROCESS_THREAD_PRIORITY,
                           &WifiCommMainProcess_thread) != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V( "Error creating WifiCommMainProcess_thread\r\n");
        result = WIFI_COMM_INTERNAL_RTOS_ERROR;
        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&WifiCommMainProcess_thread);

    if (EventFunction)
    {
        if (RegisteredEventFunction == NULL)	//First time, we create worker thread first
        {
            RegisteredEventFunction = EventFunction;

            if (dxWorkerTaskCreate("WIFICOMM W", &EventWorkerThread, WIFI_DEFAULT_WORKER_PRIORITY, WIFI_COMM_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE, WIFI_COMM_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V(  "WifiCommManagerInit - Failed to create worker thread\r\n" );
                RegisteredEventFunction = NULL;
                result = WIFI_COMM_INTERNAL_RTOS_ERROR;
                goto EndInit;
            }
            else
            {
                GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "WifiCommManager Event Thread");
            }
        }
        else
        {
            result = WIFI_COMM_INVALID_PARAMETER;
            goto EndInit;
        }
    }

    //Init and TurnOn Wifi Interface
#if CONFIG_USE_WIFI_EXTENSION_API
#pragma message ("IMPOTENT: SET Wi-Fi channel plan and adaptivity for Europe!!!")
    dxWiFi_On_Ext(DXWIFI_MODE_STA, DXWIFI_CHANNEL_PLAN_EU, DXWIFI_ADAPTIVITY_NORMAL);
#else
#pragma message ("IMPOTENT: SET Wi-Fi channel plan and adaptivity to hardware DEFAULT!!!")
    dxWiFi_On(DXWIFI_MODE_STA);
#endif // CONFIG_USE_WIFI_EXTENSION_API

EndInit:
    return result;
}

WifiCommEventReportHeader_t WifiCommManagerGetEventObjHeader(void* ReportedEventArgObj)
{
    WifiCommEventReportHeader_t Header;
    Header.ReportEvent = WIFI_COMM_EVENT_UNKNOWN;

    if (ReportedEventArgObj)
    {
        WifiCommEventReportObj_t* EventObj = (WifiCommEventReportObj_t*)ReportedEventArgObj;
        memcpy(&Header, &EventObj->Header, sizeof(WifiCommEventReportHeader_t));
    }

    return Header;

}


dxWiFi_Scan_Details_t* WifiCommManagerParseScanApReportEventObj(void* ReportedEventArgObj)
{
    dxWiFi_Scan_Details_t* result = NULL;

    if (ReportedEventArgObj)
    {
        WifiCommEventReportObj_t* EventReportObj = (WifiCommEventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->Header.ReportEvent == WIFI_COMM_EVENT_SCAN_AP)
        {
            result = (dxWiFi_Scan_Details_t*)EventReportObj->pData;
        }
    }

    return result;

}


dxWiFi_Security_t* WifiCommManagerParseCommEstablishEventObj(void* ReportedEventArgObj)
{
    dxWiFi_Security_t* result = NULL;

    if (ReportedEventArgObj)
    {
        WifiCommEventReportObj_t* EventReportObj = (WifiCommEventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->Header.ResultState == WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED)
        {
            result = (dxWiFi_Security_t*)EventReportObj->pData;
        }
    }

    return result;
}


WifiCommManager_result_t WifiCommManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;

    if (ReportedEventArgObj)
    {
        WifiCommEventReportObj_t* EventReportObj = (WifiCommEventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->pData)
            dxMemFree(EventReportObj->pData);

        dxMemFree(ReportedEventArgObj);
    }
    else
    {
        result = WIFI_COMM_INVALID_PARAMETER;
    }

    return result;

}


WifiCommManager_result_t WifiCommManagerGetIpAddress(dxIP_Addr_t* IpAddress)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;
    dxNet_Addr_t CurrentIPAddrInfo;

    if (gCurrentWifiStatus != WIFI_COMM_MANAGER_CONNECTION_UP)
    {
        result = WIFI_COMM_MANAGER_CONNECTION_IS_DOWN;
        goto Exit;
    }

    //We only support IPV4 for now
    /* Get our local IP address */
	if ( dxNET_Info( &CurrentIPAddrInfo ) == DXNET_SUCCESS ) {
	    //We only support IPV4 for now
	    if (CurrentIPAddrInfo.ip_addr.version == DXIP_VERSION_V4)
	    {
	        /* Get our local IP address */
	        IpAddress->version = DXIP_VERSION_V4;
	        memcpy(IpAddress->v4, CurrentIPAddrInfo.ip_addr.v4, sizeof(CurrentIPAddrInfo.ip_addr.v4));
	    }
	    else if (CurrentIPAddrInfo.ip_addr.version == DXIP_VERSION_V6)
	    {
	        /* Get our local IP address */
	        IpAddress->version = DXIP_VERSION_V6;
	        memcpy(IpAddress->v6, CurrentIPAddrInfo.ip_addr.v6, sizeof(CurrentIPAddrInfo.ip_addr.v6));
	    }
	    else if (CurrentIPAddrInfo.ip_addr.version == DXIP_VERSION_BOTH)
	    {
	        IpAddress->version = DXIP_VERSION_BOTH;
	        memcpy(IpAddress->v4, CurrentIPAddrInfo.ip_addr.v4, sizeof(CurrentIPAddrInfo.ip_addr.v4));
	        memcpy(IpAddress->v6, CurrentIPAddrInfo.ip_addr.v6, sizeof(CurrentIPAddrInfo.ip_addr.v6));
	    }
	    else
	    {
	        result = WIFI_COMM_INVALID_PARAMETER;
	    }
	} else {
        result = WIFI_COMM_INVALID_PARAMETER;
	}

Exit:
    return result;
}

WifiCommManager_ConnectionStatus_t WifiCommManagerGetWifiConnectionStatus(void)
{
    WifiCommManager_ConnectionStatus_t ConnectionStatus = WIFI_COMM_CONNECTION_UP_BUT_BUSY;

    if (gCurrentWifiStatus == WIFI_COMM_MANAGER_CONNECTION_UP)
    {
        ConnectionStatus = WIFI_COMM_CONNECTION_UP;
    }
    else if (gCurrentWifiStatus == WIFI_COMM_MANAGER_CONNECTION_DOWN)
    {
        ConnectionStatus = WIFI_COMM_CONNECTION_DOWN;
    }

    return ConnectionStatus;
}

int16_t WifiCommManagerGetWifiRSSI(void)
{
    int RSSI = 0;

    if (dxWiFi_GetRSSI(&RSSI) != DXWIFI_SUCCESS) {
        RSSI = 0;
	}

    return (int16_t)RSSI;
}

WifiCommManager_result_t WifiCommManagerScanSSIDTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;

    if (gCurrentWifiStatus == WIFI_COMM_MANAGER_CONNECTION_DOWN)
    {
        WifiCommJobQueue_t* WifiCommJobMsg = NULL;
        WifiCommJobMsg = dxMemAlloc( "WifiComm Job Msg Buff", 1, sizeof(WifiCommJobQueue_t));
        WifiCommJobMsg->JobType = WIFI_COMM_MANAGER_JOB_SCAN_AP;
        WifiCommJobMsg->JobTimeOutTime = 0;
        WifiCommJobMsg->JobSourceOfOrigin = SourceOfOrigin;
        WifiCommJobMsg->JobIdentificationNumber = TaskIdentificationNumber;

        if (WifiCommJobQueue)
        {
            if (dxQueueSpacesAvailable(WifiCommJobQueue) == 0)
            {
                G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                dxMemFree(WifiCommJobMsg);
                result = WIFI_COMM_TOO_MANY_TASK_IN_QUEUE;

            }
            else
            {
                if  (dxQueueSend( WifiCommJobQueue,
                                &WifiCommJobMsg,
                                1000) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "Error pushing message to queue (Could be due to timeout) \r\n" );
                    dxMemFree(WifiCommJobMsg);
                    result = WIFI_COMM_INTERNAL_RTOS_ERROR;
                }

            }
        }
    }
    else
    {
        result = WIFI_COMM_MANAGER_OPERATION_NOT_POSSIBLE_DUE_TO_INVALID_STATE;
    }

    return result;
}


WifiCommManager_result_t WifiCommManagerConnectToSSIDTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;
    WifiCommJobQueue_t* WifiCommJobMsg = NULL;
    if (SSID == NULL)
    {
        result = WIFI_COMM_INVALID_PARAMETER;
        goto Exit;
    }

    WifiCommJobMsg = dxMemAlloc( "WifiComm Job Msg Buff", 1, sizeof(WifiCommJobQueue_t));
    WifiCommJobMsg->JobType = WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID;
    WifiCommJobMsg->JobTimeOutTime = 0;
    WifiCommJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    WifiCommJobMsg->JobIdentificationNumber = TaskIdentificationNumber;

    WifiConnectInfo_t ConnectInfo;
    memcpy(ConnectInfo.SSID, SSID, SSIDLen);
    ConnectInfo.SSID[SSIDLen] = '\0';

    if (PasswordLen)
    {
        memcpy(ConnectInfo.Password, Password, PasswordLen);
        ConnectInfo.Password[PasswordLen] = '\0';
    }
    else
    {
        ConnectInfo.Password[0] = '\0';
    }

    //This is a scan so let's set the security to DXWIFI_SECURITY_FORCE_32_BIT which is what we use for internal filter
    ConnectInfo.SecurityType = DXWIFI_SECURITY_FORCE_32_BIT;

    memcpy(WifiCommJobMsg->JobData, &ConnectInfo, sizeof(WifiConnectInfo_t));

    if (WifiCommJobQueue)
    {
        if (dxQueueSpacesAvailable(WifiCommJobQueue) == 0)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(WifiCommJobMsg);
            result = WIFI_COMM_TOO_MANY_TASK_IN_QUEUE;

        }
        else
        {
            if  (dxQueueSend( WifiCommJobQueue,
                            &WifiCommJobMsg,
                            1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V(  "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(WifiCommJobMsg);
                result = WIFI_COMM_INTERNAL_RTOS_ERROR;
            }

        }
    }

Exit:

    return result;
}


WifiCommManager_result_t WifiCommManagerConnectToSSIDDirectTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, int32_t ApSecurityType, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;
    WifiCommJobQueue_t* WifiCommJobMsg = NULL;
    if (SSID == NULL)
    {
        result = WIFI_COMM_INVALID_PARAMETER;
        goto Exit;
    }

    WifiCommJobMsg = dxMemAlloc( "WifiComm Job Msg Buff", 1, sizeof(WifiCommJobQueue_t));
    WifiCommJobMsg->JobType = WIFI_COMM_MANAGER_JOB_CONNECT_TO_SSID_DIRECT;
    WifiCommJobMsg->JobTimeOutTime = 0;
    WifiCommJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    WifiCommJobMsg->JobIdentificationNumber = TaskIdentificationNumber;

    WifiConnectInfo_t ConnectInfo;
    memcpy(ConnectInfo.SSID, SSID, SSIDLen);
    ConnectInfo.SSID[SSIDLen] = '\0';

    if (PasswordLen)
    {
        memcpy(ConnectInfo.Password, Password, PasswordLen);
        ConnectInfo.Password[PasswordLen] = '\0';
    }
    else
    {
        ConnectInfo.Password[0] = '\0';
    }

    ConnectInfo.SecurityType = ApSecurityType;

    memcpy(WifiCommJobMsg->JobData, &ConnectInfo, sizeof(WifiConnectInfo_t));

    if (WifiCommJobQueue)
    {
        if (dxQueueSpacesAvailable(WifiCommJobQueue) == 0)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(WifiCommJobMsg);
            result = WIFI_COMM_TOO_MANY_TASK_IN_QUEUE;

        }
        else
        {
            if  (dxQueueSend( WifiCommJobQueue,
                              &WifiCommJobMsg,
                              1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V(  "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(WifiCommJobMsg);
                result = WIFI_COMM_INTERNAL_RTOS_ERROR;
            }

        }
    }

Exit:

    return result;
}


WifiCommManager_result_t WifiCommManagerBreakSearchInProgressTask_Asynch(void)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;

    if (gCurrentWifiStatus == WIFI_COMM_MANAGER_SEARCHING_FOR_SSID)
    {
        gbBreakWifiCommSearch = dxTRUE;
    }

    return result;
}


WifiCommManager_result_t WifiCommManagerDisconnectCurrentConnectionTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
    WifiCommManager_result_t result = WIFI_COMM_MANAGER_SUCCESS;

    WifiCommJobQueue_t* WifiCommJobMsg = dxMemAlloc( "WifiComm Job Msg Buff", 1, sizeof(WifiCommJobQueue_t));
    WifiCommJobMsg->JobType = WIFI_COMM_MANAGER_JOB_DISCONNECT;
    WifiCommJobMsg->JobTimeOutTime = 0;
    WifiCommJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    WifiCommJobMsg->JobIdentificationNumber = TaskIdentificationNumber;

    if (WifiCommJobQueue)
    {
        if (dxQueueSpacesAvailable(WifiCommJobQueue) == 0)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(WifiCommJobMsg);
            result = WIFI_COMM_TOO_MANY_TASK_IN_QUEUE;

        }
        else
        {
            if  (dxQueueSend( WifiCommJobQueue,
                              &WifiCommJobMsg,
                              1000) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V(  "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(WifiCommJobMsg);
                result = WIFI_COMM_INTERNAL_RTOS_ERROR;
            }

        }
    }

    return result;
}

