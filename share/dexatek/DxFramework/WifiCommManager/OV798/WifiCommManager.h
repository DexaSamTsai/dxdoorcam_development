//============================================================================
// File: WifiCommManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef __WIFI_COMM_MANAGER_H__
#define __WIFI_COMM_MANAGER_H__

#pragma once

#include "dk_Network.h"
#include "dxOS.h"
#include "dxSysDebugger.h"
#include "dxWIFI.h"

/******************************************************
 *                      Defines
 ******************************************************/

//NOTE: If len limit adjustment is required, Make sure (SSID_MAX_NAME_LEN + PASSWORD_MAX_NAME_LEN) MUST be equal to 62
#define SSID_MAX_NAME_LEN                   31
#define PASSWORD_MAX_NAME_LEN               31


/******************************************************
 *                      Macros
 ******************************************************/

/******************************************************
 *                    Constants
 ******************************************************/

/******************************************************
 *                   Enumerations
 ******************************************************/


typedef enum
{

    WIFI_COMM_MANAGER_SUCCESS                       = 0,    /**< Success */
    WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED,
    WIFI_COMM_MANAGER_COMMUNICATION_ESTABLISHED_BUT_UNABLE_TO_REACH_WWW,
    WIFI_COMM_MANAGER_SCAN_AP_REPORT,
    WIFI_COMM_MANAGER_SCAN_AP_REPORT_ENDED,

    WIFI_COMM_MANAGER_UNABLE_TO_FIND_SSID               = -1,
    WIFI_COMM_MANAGER_WRONG_PASSWORD                    = -2,
    WIFI_COMM_MANAGER_PASSWORD_REQUIRED                 = -3,
    WIFI_COMM_MANAGER_ALREADY_CONNECTED_TO_ANOTHER_SSID = -4,
    WIFI_COMM_MANAGER_CONNECTION_IS_DOWN                = -5,
    WIFI_COMM_MANAGER_SCAN_SSID_TIME_OUT                = -6,
    WIFI_COMM_MANAGER_SSID_AP_ENCRYPTION_NOT_SUPPORTED  = -7,
    WIFI_COMM_MANAGER_SCAN_AP_TIME_OUT                  = -8,
    WIFI_COMM_MANAGER_OPERATION_NOT_POSSIBLE_DUE_TO_INVALID_STATE = -9,
    WIFI_COMM_MANAGER_CONNECTION_SHUTDOWN_BY_USER       = -10,
    WIFI_COMM_MANAGER_SCAN_AP_INTERNAL_ERROR            = -11,

    WIFI_COMM_INTERNAL_GENERIC_ERROR                    = -20,
    WIFI_COMM_INTERNAL_RTOS_ERROR                       = -21,
    WIFI_COMM_TOO_MANY_TASK_IN_QUEUE                    = -22,
    WIFI_COMM_INVALID_PARAMETER                         = -23,
    WIFI_COMM_INTERNAL_API_ERROR                        = -24,

} WifiCommManager_result_t;

typedef enum
{

    WIFI_COMM_EVENT_UNKNOWN                = 0,
    WIFI_COMM_EVENT_COMMUNICATION,
    WIFI_COMM_EVENT_SCAN_AP,

} WifiCommManager_EventType_t;

typedef enum
{

    WIFI_COMM_CONNECTION_DOWN                = 0,
    WIFI_COMM_CONNECTION_UP,
    WIFI_COMM_CONNECTION_UP_BUT_BUSY,

} WifiCommManager_ConnectionStatus_t;

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    WifiCommManager_EventType_t     ReportEvent;
    WifiCommManager_result_t        ResultState;
    uint16_t                        SourceOfOrigin;
    uint32_t                        IdentificationNumber;

} WifiCommEventReportHeader_t;

typedef struct
{
    WifiCommEventReportHeader_t     Header;
    uint8_t*                        pData;

} WifiCommEventReportObj_t;

typedef void (*event_handler_t)( void* arg );

/******************************************************
 *                    Structures
 ******************************************************/

/******************************************************
 *               Function Declarations
 ******************************************************/

/** Get the MAC address of Gateway (8 byte)
 *
 * @return @ref WifiCommDeviceMac_t
 */
extern const WifiCommDeviceMac_t* WifiCommManagerGetDeviceMac(void);


/** Initialize Wifi Communication Manager, all task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  NOTE1: Use WifiCommManagerGetEventObjHeader(arg) to obtain the WifiCommEventReportObj_t.
 *  NOTE2: According to ReportEvent handle necessary action (see WifiCommManager_EventType_t)
 *  NOTE3: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE4: At the end of EventFunction call WifiCommManagerClearEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[in]  EventFunction 	 : 	The EventFunction that will be called when reporting activities
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerInit(event_handler_t EventFunction);


/** Get the event header from reported event obj arg
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported obj argument to obtain the header info
 *
 * @return @ref WifiCommEventReportHeader_t
 */
extern WifiCommEventReportHeader_t WifiCommManagerGetEventObjHeader(void* ReportedEventArgObj);


/** Get the Ap info payload data from reported scan Ap report event obj arg
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported obj argument to obtain the Ap info payload data
 *
 * @return @ref wiced_scan_result_t*
 */
extern dxWiFi_Scan_Details_t* WifiCommManagerParseScanApReportEventObj(void* ReportedEventArgObj);

/** Get the related wifi communication establish info from reported Comm establish report event obj arg
 *
 * @param[in]  ReportedEventArgObj  : The reported obj argument to obtain the Comm estblish info
 *
 * @return @ref dxWiFi_Security_t* .. Note that it might be NULL
 */
extern dxWiFi_Security_t* WifiCommManagerParseCommEstablishEventObj(void* ReportedEventArgObj);


/** All information of Object given by EventFunction will be cleaned
 *
 * @param[in]  ReportedEventArgObj 	 : 	The reported obj argument to clean up
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerCleanEventArgumentObj(void* ReportedEventArgObj);


/** Get local Ip address of current connection, will return error if current connection is down, in this case Ipv4Address is not valid
 *
 * @param[out]  IpAddress 	 : 	The reported ip address, only valid if the function return result is SUCCESS
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerGetIpAddress(dxIP_Addr_t* IpAddress);


/** Check current wifi connection status
 *
 * @return @ref WifiCommManager_ConnectionStatus_t
 */
extern WifiCommManager_ConnectionStatus_t WifiCommManagerGetWifiConnectionStatus(void);

/** Check current wifi connection RSSI
 *
 * @return @ref Returned RSSI, if 0 it means invalid (eg Wifi is not connected or due to other reason)
 */
extern int16_t WifiCommManagerGetWifiRSSI(void);

/** Scan for SSID
 *
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * NOTE1: WIFI_COMM_EVENT_SCAP_AP will be reported with ResultState (WifiCommManager_result_t)
 * NOTE2: if ResultState is WIFI_COMM_MANAGER_SCAN_AP_REPORT then use WifiCommManagerParseScanApReportEventObj to obtain the Ap info
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerScanSSIDTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber);


/** Connect to SSID (WifiManager will scan first and if found it will try to coneect)
 *
 * @param[in]  SSID 	 : 	The name of the Access Point to connect to
 * @param[in]  SSIDLen 	 : 	The length of the name of the Access Point to connect to. MAXIMUM is SSID_MAX_NAME_LEN character
 * @param[in]  Password 	 : 	The Password of the Access Point, NULL if no password.
 * @param[in]  PasswordLen 	 : 	The length of the Password of the Access Point, 0 if no password. MAXIMUM is PASSWORD_MAX_NAME_LEN character
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * NOTE1: WIFI_COMM_EVENT_COMMUNICATION will be reported with ResultState (WifiCommManager_result_t)
 * NOTE2: It may take up to 30 seconds for a feedback on current state
 * NOTE3: Once connection has been establish WifiCommManager will notify if connection is lost or has been re-established. WifiCommManager will always try to re-connect back when possible.
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerConnectToSSIDTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber);


/** Connect to SSID Direct Mode (this can be used for connecting hidden SSID)
 *
 * @param[in]  SSID 	 : 	The name of the Access Point to connect to
 * @param[in]  SSIDLen 	 : 	The length of the name of the Access Point to connect to. MAXIMUM is SSID_MAX_NAME_LEN character
 * @param[in]  Password 	 : 	The Password of the Access Point, NULL if no password.
 * @param[in]  PasswordLen 	 : 	The length of the Password of the Access Point, 0 if no password. MAXIMUM is PASSWORD_MAX_NAME_LEN character
 * @param[in]  ApSecurityType 	 : The Security type, set to DXWIFI_SECURITY_UNKNOWN to auto try all known type
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * NOTE1: WIFI_COMM_EVENT_COMMUNICATION will be reported with ResultState (WifiCommManager_result_t)
 * NOTE2: It may take up to 30 seconds for a feedback on current state
 * NOTE3: Once connection has been establish WifiCommManager will notify if connection is lost or has been re-established. WifiCommManager will always try to re-connect back when possible.
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerConnectToSSIDDirectTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, int32_t ApSecurityType, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber);

/** If current wifi manager is still searching for SSID then break the loop
 *
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerBreakSearchInProgressTask_Asynch(void);


/** Disconnect current connection
 *
 * @param[in]  SourceOfOrigin    :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  TaskIdentificationNumber 	 : 	The identification number that will be reported back to registered EventFunction when Task is done.
 *
 * NOTE1: WIFI_COMM_EVENT_COMMUNICATION will be reported with ResultState (WifiCommManager_result_t)
 * NOTE2: Once disconnected WifiCommManager will NOT try to re-connect to previously connect SSID.
 *
 * @return @ref WifiCommManager_result_t
 */
extern WifiCommManager_result_t WifiCommManagerDisconnectCurrentConnectionTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber);

#endif
