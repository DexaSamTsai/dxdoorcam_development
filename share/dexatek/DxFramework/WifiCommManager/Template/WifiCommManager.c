//============================================================================
// File: WifiCommManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "WifiCommManager.h"
#include <string.h>

/******************************************************
 *                      Macros
 ******************************************************/


/******************************************************
 *                    Constants
 ******************************************************/


/******************************************************
 *               Function Definitions
 ******************************************************/
const WifiCommDeviceMac_t* WifiCommManagerGetDeviceMac(void)
{
	// Todo...

    return NULL;
}

WifiCommManager_result_t WifiCommManagerInit(event_handler_t EventFunction)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommEventReportHeader_t WifiCommManagerGetEventObjHeader(void* ReportedEventArgObj)
{
	// Todo...
    WifiCommEventReportHeader_t Header;

	memset( &Header, 0x00, sizeof( WifiCommEventReportHeader_t ) );

    return Header;
}

dxWiFi_Scan_Details_t* WifiCommManagerParseScanApReportEventObj(void* ReportedEventArgObj)
{
	// Todo...

    return NULL;
}

dxWiFi_Security_t* WifiCommManagerParseCommEstablishEventObj(void* ReportedEventArgObj)
{
	// Todo...

    return NULL;
}

WifiCommManager_result_t WifiCommManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}


WifiCommManager_result_t WifiCommManagerGetIpAddress(dxIP_Addr_t* IpAddress)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommManager_ConnectionStatus_t WifiCommManagerGetWifiConnectionStatus(void)
{
	// Todo...

    return WIFI_COMM_CONNECTION_DOWN;
}

int16_t WifiCommManagerGetWifiRSSI(void)
{
	// Todo...

    return 0;
}

WifiCommManager_result_t WifiCommManagerScanSSIDTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommManager_result_t WifiCommManagerConnectToSSIDTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommManager_result_t WifiCommManagerConnectToSSIDDirectTask_Asynch(char* SSID, uint8_t SSIDLen, char* Password, uint8_t PasswordLen, int32_t ApSecurityType, uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommManager_result_t WifiCommManagerBreakSearchInProgressTask_Asynch(void)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

WifiCommManager_result_t WifiCommManagerDisconnectCurrentConnectionTask_Asynch(uint16_t SourceOfOrigin, uint32_t TaskIdentificationNumber)
{
	// Todo...

    return WIFI_COMM_INTERNAL_RTOS_ERROR;
}

