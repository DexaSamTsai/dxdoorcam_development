//============================================================================
// File: dxGPIOConfig.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/06/15
//     1) Description: Initial version
//============================================================================
#ifndef _DXGPIOCONFIG_H
#define _DXGPIOCONFIG_H

#define DX_NOT_ASSIGNED                 0xFFFFFFFE

//README FIRST!!!
/**
 * The following GPIOs are default.
 * If your project use different configuration, you should
 *
 * 1) Make a copy of dxGPIOConfig.h and rename to dxGPIO.h
 * 2) Move dxGPIO.h to your project's directory
 * 3) Open your project. [IAR Embedded Workbench IDE] application will be started.
 * 4) Move mouse to [SDRAM] in [IAR Embedded Workbench IDE] [Workspace], right click and select [Options...] from popup menu.
 * 5) Click [C/C++ Compiler] -> [Preprocessor], add configUSE_LOCAL_GPIO_SETTING=1 to [Defined symbols: (one per line)]
 * 6) Save your project and rebuild.
 */
#define DK_BLE_CENTRAL_MODE             DX_NOT_ASSIGNED
#define DK_BLE_RESET                    DX_NOT_ASSIGNED

#define DK_LED_BLUE                     DX_NOT_ASSIGNED
#define DK_LED_YELLOW                   DX_NOT_ASSIGNED
#define DK_LED_RED                      DX_NOT_ASSIGNED

#define DK_UART0_TX                     DX_NOT_ASSIGNED
#define DK_UART0_RX                     DX_NOT_ASSIGNED

#define DK_UART1_TX                     DX_NOT_ASSIGNED
#define DK_UART1_RX                     DX_NOT_ASSIGNED

#define DK_UART2_TX                     DX_NOT_ASSIGNED
#define DK_UART2_RX                     DX_NOT_ASSIGNED

#define MAIN_BUTTON_GPIO                DX_NOT_ASSIGNED
#define SUB_BUTTON_GPIO                 DX_NOT_ASSIGNED

#endif // _DXGPIOCONFIG_H