//============================================================================
// File: dxBSP.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#ifndef _DXBSP_H
#define _DXBSP_H

#include "dxMemConfig.h"
#include "dxGPIOConfig.h"

//============================================================================
// Hardware dependent
//
// Options (Choose One)
//============================================================================

//============================================================================
// Hardware dependent
//
// Set 1 if hardware support HomeKit
//============================================================================

//============================================================================
// RTK 8XXX include files
//============================================================================
#include "dxOS.h"

//============================================================================
// Defines and Macro
//============================================================================

//============================================================================
// UART Wrapper
//============================================================================
typedef struct dxUART {
	// Todo...
} dxUART_t;

typedef enum
{
    DXUART_PORT0 = 0,
    DXUART_PORT1 = 1,
    DXUART_PORT2 = 2
} dxUART_Port_t;

typedef enum
{
    DXUART_FLOWCONTROL_NONE,
    DXUART_FLOWCONTROL_RTS,
    DXUART_FLOWCONTROL_CTS,
    DXUART_FLOWCONTROL_RTSCTS
} dxUART_FlowControl_t;

typedef enum
{
    DXUART_PARITY_NONE = 0,
    DXUART_PARITY_ODD = 1,
    DXUART_PARITY_EVEN = 2,
} dxUART_Pairty_t;

typedef enum {

    DXUART_SUCCESS = 0,

    DXUART_ERROR   = -1,

    DXUART_TIMEOUT = -2,

} dxUART_RET_CODE;

/**
 * \return DXUART_SUCCESS   if UART closed
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_Close(dxUART_t *pdxUart);

/**
 * \return DXUART_SUCCESS   if a character received
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_GetCh(dxUART_t *pdxUart, uint8_t *ch);

/**
 * \return dxUART_t         a handle to control UART.
 *         NULL             otherwise
 */
dxUART_t *dxUART_Open(dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option);

/**
 * \return DXUART_SUCCESS   if a character sent
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_PutCh(dxUART_t *pdxUart, uint8_t ch);

/**
 * \return > 0              if number of bytes received
 *         DXUART_TIMEOUT   if timeout
 *         DXUART_ERROR     otherwise
 */
int dxUART_Receive_Blocked(dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms);

/**
 * \return > 0              if number of bytes sent
 *         DXUART_TIMEOUT   if timeout
 *         DXUART_ERROR     otherwise
 */
int dxUART_Send_Blocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len);

//============================================================================
// BSP
//============================================================================
typedef enum {

    DXBSP_SUCCESS = 0,

    DXBSP_ERROR   = -1,

} dxBSP_RET_CODE;

/**
 * \return DXBSP_SUCCESS    if BSP initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxBSP_Init(void);

//============================================================================
// GPIO Access Function
//============================================================================
typedef enum {

    DXPIN_PULLNONE  = 0,
    DXPIN_PULLUP    = 1,
    DXPIN_PULLDOWN  = 2,
    DXPIN_OPENDRAIN = 3,
    DXPIN_PULLDEFAULT = DXPIN_PULLNONE,
    DXPIN_MODE_UNSUPPORTED = 255,

} dxPIN_Mode;

typedef enum {

    DXPIN_INPUT  = 0,
    DXPIN_OUTPUT = 1

} dxPIN_Direction;

typedef enum {

    DXPIN_LOW = 0,
    DXPIN_HIGH = 1

} dxPIN_Level;

typedef struct {

	// Todo...
    void			*Object;
    uint32_t		Name;
    dxPIN_Direction	Direction;
    uint32_t		Mode;			// dxPIN_Mode

} dxGPIO_t;

#define DX_ENDOF_GPIO_CONFIG			0xFFFFFFFF

#define DK_GPIO_CONTROL_LEVEL_ON		DXPIN_HIGH
#define DK_GPIO_CONTROL_LEVEL_OFF		DXPIN_LOW

#define DK_LED_CONTROL_LEVEL_ON			DXPIN_LOW
#define DK_LED_CONTROL_LEVEL_OFF		DXPIN_HIGH

/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Init(dxGPIO_t* ConfigTable);

/**
 * \return DXPIN_LOW        if GPIO level is low
 *         DXPIN_HIGH       if GPIO level is high
 *         DXBSP_ERROR      otherwise
 */
int dxGPIO_Read(uint32_t PinName);

/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Write(uint32_t PinName, dxPIN_Level level);

/**
 * \return DXBSP_SUCCESS    if GPIO change direction success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Set_Direction(uint32_t PinName, dxPIN_Direction Dir);

/**
 * \return dxPIN_Direction  if get GPIO direction success
 *         DXBSP_ERROR      otherwise
 */
int dxGPIO_Get_Direction(uint32_t PinName);

/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Uninit(dxGPIO_t* ConfigTable);

//============================================================================
// PWM Access Function
//============================================================================
typedef struct
{
	// Todo...
} dxPWM_t;

/**
 * \return DXBSP_SUCCESS    if PWM initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPWM_Init(dxPWM_t* ConfigTable);

/**
 * \return DXBSP_SUCCESS    if PWM uninitial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPWM_Uninit(dxPWM_t* ConfigTable);

/**
 * \return DXBSP_SUCCESS    if PWM write success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPWM_Write(uint32_t PinName, float PwmPeriod);


//============================================================================
// I2C Access Function
//============================================================================
typedef enum
{
    DXI2C_WRITE_ADDRESSED = 3,
    DXI2C_WRITE_GENERAL   = 2,
    DXI2C_READ_ADDRESSED  = 1,
    DXI2C_OK              = 0,
    DXI2C_BUSY            = -1,
    DXI2C_TIMEOUT         = -2,
    DXI2C_ERR_PARA        = -3,
    DXI2C_ERR_UNKNOWN     = -4,
    DXI2C_NO_DATA         = -5,
} dxI2C_RET_CODE;

typedef struct
{
	// Todo...
} dxI2C_t;

/**
 * \return DXBSP_SUCCESS    if I2C initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxI2C_Init(dxI2C_t* ConfigTable);

/**
 * \return DXBSP_SUCCESS    if I2C uninitial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxI2C_Uninit(dxI2C_t* ConfigTable);


/**
 * \return int              written data length
 */
int dxI2C_Master_Write(uint32_t Pin_I2C_SDA,    // i2c SDA pin
                       int address,             // i2c slave address
                       const char *data,        // i2c write in data
                       int length,              // i2c write in data length
                       dxBOOL stop);            // combined mode or not, true: normal mode, false: combined mode

/**
 * \return int              read data length
 */
int dxI2C_Master_Read(uint32_t Pin_I2C_SDA, // i2c SDA pin
                      int address,          // i2c slave address
                      char *data,           // i2c read data buffer
                      int length,           // i2c read data buffer length
                      dxBOOL stop);         // combined mode or not, true: normal mode, false: combined mode

/**
 * \return none
 */
void dxI2C_Reset(dxI2C_t *pHandle);      // i2c handle

/**
 * \return uint8_t          read 1 byte data
 */
uint8_t dxI2C_Master_U8_Read(uint32_t Pin_I2C_SDA,  // i2c handle
                             int address,           // i2c slave address
                             dxBOOL last);          // combined mode or not, true: normal mode, false: combined mode

/**
 * \return int              written data length
 */
int dxI2C_Master_U8_Write(uint32_t Pin_I2C_SDA, // i2c handle
                          int address,          // i2c slave address
                          uint8_t data);        // i2c write in data

/**
 * \return none
 */
void dxI2C_Slave_Mode(uint32_t Pin_I2C_SDA, // i2c handle
                      dxBOOL enable_slave); // enable slave function, 0 disable, 1 enable

/**
 * \return dxI2C_RET_CODE
 */
dxI2C_RET_CODE dxI2C_Slave_ReceiveStatus(uint32_t Pin_I2C_SDA);  // i2c handle

/**
 * \return int              read data length
 */
int dxI2C_Slave_Read(uint32_t Pin_I2C_SDA,  // i2c handle
                     char *data,            // point to the buffer to hold the received data
                     int length);           // the length of data that to be received

/**
 * \return int              written data length
 */
int dxI2C_Slave_Write(uint32_t Pin_I2C_SDA,  // i2c handle
                      const char *data,      // point to the data to be sent
                      int length);           // the length of data that to be sent

/**
  * \brief  Set i2c slave address.
  * \return none
  */
//void dxI2C_Slave_Address(dxI2CHandle_t handle,  // i2c handle
void dxI2C_Slave_Address(uint32_t Pin_I2C_SDA,  // i2c handle
                         int idx,               // i2c index, 0 = I2C0 Device, 1 = I2C1 Device
                         uint32_t address,      // slave address
                         uint32_t mask);        // the mask of address


//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void);

/**
 * \brief the function should be called once while system startup.
 */
void dxPLATFORM_Init(void);

// DeviceMac_t structure is almost the same as WifiCommDeviceMac_t in dk_Network.h
// and can be direct case to WifiCommDeviceMac_t.
// We define a new structure here is to avoid cross reference due to dxBSP.h and dxOS.h
// are lowest level of .h files.
typedef struct
{
    uint8_t MacAddr[8];

} DeviceMac_t;

/**
 * \brief the funciton will change system's MAC address. NEED to reboot to take effect.
 *
 * \return DXBSP_SUCCESS    if MAC address wrote success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac);

/**
 * \brief Initialize platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut);

/**
 * \brief Start platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void);

/**
 * \brief Refresh platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void);

/**
 * \brief Close platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void);

//============================================================================
// FLASH API
//============================================================================
typedef void * dxFlashHandle_t;

/**
 * \return dxFlashHandle_t  a handle to the flash access control.
 *         NULL             otherwise
 */
dxFlashHandle_t dxFlash_Init(void);

/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash);


/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash);


/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address);

/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size);

/**
 * \return DXOS_SUCCESS if the data was read successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Read(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data);

/**
 * \return DXOS_SUCCESS if the data was read successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Read_Word(dxFlashHandle_t flash, uint32_t address, uint32_t *data);

/**
 * \return DXOS_SUCCESS if the data was written successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Write(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data);

/**
 * \return DXOS_SUCCESS if the data was written successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Write_Word(dxFlashHandle_t flash, uint32_t address, uint32_t data);

/**
 * \return DXOS_SUCCESS
 */
dxOS_RET_CODE dxFlash_Uninit(dxFlashHandle_t flash);

//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================

typedef enum {

    DXMAINIMAGE_COMPLETE              = 1,

    DXMAINIMAGE_SUCCESS               = 0,

    DXMAINIMAGE_ERROR                 = -1,

    DXMAINIMAGE_SECTION_LIMIT_REACHED = -2,

    DXMAINIMAGE_SIZE_LIMIT_REACHED    = -3,

    DXMAINIMAGE_VERIFY_ERROR          = -4,

} dxMAINIMAGE_RET_CODE;

/**
 * \return DXMAINIMAGE_SUCCESS if success
 *         DXMAINIMAGE_ERROR   otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Init(void);

/**
 * \brief The function will clear all data and reset flash access handle. Any calling dxMainImage_Write() that
 *        is next to dxMainImage_Start() will cause current data in flash to be overwrite, instead of appending.
 *        MD5 checksum value will be reset too.
 *
 * \param[in] pszMD5Sum Pointer to a NULL terminated string. It should be 32 bytes long. For example,
 *                      "6aea856851667a4238597607eb4c1f6e" (character " is not incuded)
 *
 * \return DXMAINIMAGE_SUCCESS            if success
 *         DXMAINIMAGE_SIZE_LIMIT_REACHED if nImageSize is too large.
 *                                        nImageSize > DX_SYSINFO_FLASH_ADDR1 - DX_SYSINFO_FLASH_UPDATE_IMG_ADDR
 *         DXMAINIMAGE_ERROR              otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Start(uint8_t *pszMD5Sum, uint32_t nImageSize);

/**
 * \return DXMAINIMAGE_SUCCESS            if flash write success.
 *         DXMAINIMAGE_COMPLETE           if flash write success and MD5 checksum value is correct.
 *                                        System is ready to reboot.
 *         DXMAINIMAGE_SIZE_LIMIT_REACHED if the free spaces for update image is not enough to store the data
 *         DXMAINIMAGE_ERROR              otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Write(uint8_t *pImage, uint32_t nImageLen);

/**
 * \return DXMAINIMAGE_SUCCESS  if success.
 *         DXMAINIMAGE_ERROR    otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_UpdateReboot(dxBOOL WithSystemSelfReboot);

/**
 * \brief  Free resource
 * \return DXMAINIMAGE_SUCCESS  if success.
 *         DXMAINIMAGE_ERROR    otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Stop(void);

//============================================================================
// SUB IMAGE UPDATE API
//============================================================================

typedef enum {

    DXSUBIMAGE_COMPLETE                 = 1,

    DXSUBIMAGE_SUCCESS                  = 0,

    DXSUBIMAGE_ERROR                    = -1,

    DXSUBIMAGE_SECTION_LIMIT_REACHED    = -2,

    DXSUBIMAGE_SIZE_LIMIT_REACHED       = -3,

    DXSUBIMAGE_VERIFY_ERROR             = -4,

    DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR  = -5,

} dxSUBIMAGE_RET_CODE;

typedef enum
{
    DXSUBIMGBLOCK_ONE    = 0,
} dxSUBIMGBLOCKID_t;

/**
 * \brief This pointer definition refer dxSubImgInfoHandle_t (check dxBSP.c).
 */
typedef void * dxSubImgInfoHandle_t;

/**
 * \return dxSubImgInfoHandle_t                 if success
 *         NULL                                 otherwise
 */
dxSubImgInfoHandle_t dxSubImage_Init(dxSUBIMGBLOCKID_t block_id);

/**
 * \brief The function will clear all data and reset flash access handle. Any calling dxSubImage_Write() that
 *        is next to dxSubImage_Start() will cause current data in flash to be overwrite, instead of appending.
 *        MD5 checksum value will be reset too.
 *
 * \param[in] pszMD5Sum Pointer to a NULL terminated string. It should be 32 bytes long. For example,
 *                      "6aea856851667a4238597607eb4c1f6e" (character " is not incuded)
 *
 * \return DXSUBIMAGE_SUCCESS                   if success
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if nImageSize is too large.
 *                                              nImageSize > DX_SUBIMG_FLASH_ADDR1 - DX_SYSINFO_FLASH_UPDATE_IMG_ADDR
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Start(dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize);

/**
 * \return DXSUBIMAGE_SUCCESS                   if flash write success.
 *         DXSUBIMAGE_COMPLETE                  if flash write success and MD5 checksum value is correct.
 *                                              System is ready to reboot.
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if the free spaces for update image is not enough to store the data
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Write(dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen);

/**
 * \brief  Read subsystem image data from flash StartPosition to pBDataBuffer, assign image size and handle (for MD5 checksum) by caller.
 *
 * \return DXSUBIMAGE_SUCCESS                   if success.
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if the free spaces for update image is not enough to store the data
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Read(dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead,
                                    uint8_t *pBDataBuffer, uint32_t *FwSizeOut);

/**
 * \return DXSUBIMAGE_SUCCESS                   if success
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Uninit(dxSubImgInfoHandle_t handle);

/**
 * \return > 0 (Full Sub Image size)    if success
 *         = 0                          otherwise
 */
uint32_t dxSubImage_ReadImageSize(dxSubImgInfoHandle_t handle);

//============================================================================
// Hyper Peripheral Storage Access API
//============================================================================
typedef enum {

    DXHPSTORAGE_SUCCESS             = 0,

    DXHPSTORAGE_ERROR               = -1,

    DXHPSTORAGE_SIZE_LIMIT_REACHED  = -2,

    DXHPSTORAGE_NOT_INITIALIZED     = -3,

    DXHPSTORAGE_INVALID_PARAMETER   = -4,

    DXHPSTORAGE_NOT_AVAILABLE	     = -5,

} dxHPSTORAGE_RET_CODE;

typedef enum
{
    DXHPSTORAGESECTOR_ONE    = 0,
} dxHPSTORAGESECTORID_t;

/**
 * \brief This pointer definition refer dxHpStorageInfoAccessCtrl_t (check dxBSP.c).
 */
typedef void * dxHpStorageInfoHandle_t;

/**
 * \return dxHpStorageInfoHandle_t               if success
 *         NULL                                  otherwise
 */
dxHpStorageInfoHandle_t dxHpStorage_Init(dxHPSTORAGESECTORID_t sector_id);

/**
 * \return DXHPSTORAGE_SUCCESS                   if flash write success.
 *         DXHPSTORAGE_SIZE_LIMIT_REACHED        if the free spaces is not enough to store the data
 *         DXHPSTORAGE_NOT_INITIALIZED           not initialised
 *         DXHPSTORAGE_ERROR                     otherwise
 */
dxHPSTORAGE_RET_CODE dxHpStorage_Write(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen);

/**
 * \return DXHPSTORAGE_SUCCESS                   if success.
 *         DXHPSTORAGE_SIZE_LIMIT_REACHED        Read range exceed the allowed range
 *         DXHPSTORAGE_NOT_INITIALIZED           not initialised
 *         DXHPSTORAGE_ERROR                     otherwise
 */
dxHPSTORAGE_RET_CODE dxHpStorage_Read(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen);

/**
 * \return DXHPSTORAGE_SUCCESS                   if success
 *         DXHPSTORAGE_NOT_INITIALIZED           not initialised
 *         DXHPSTORAGE_ERROR                     otherwise
 */
dxHPSTORAGE_RET_CODE dxHpStorage_EraseNumOfSector(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint16_t NumOfSector);

/**
 * \return DXHPSTORAGE_SUCCESS                   if success
 *         DXHPSTORAGE_INVALID_PARAMETER         Invalid Parameter
 *         DXHPSTORAGE_ERROR                     otherwise
 */
dxHPSTORAGE_RET_CODE dxHpStorage_Uninit(dxHpStorageInfoHandle_t handle);

/**
 * \return > 0 (The size of available storage for use in byte)  if success
 *         = 0                                                  otherwise
 */
uint32_t dxHpStorage_ReadTotalSize(dxHpStorageInfoHandle_t handle);

/**
 * \return > 0 (The size of block in byte)  if success
 *         = 0                              otherwise
 */
uint32_t dxHpStorage_ReadSectorSize(dxHpStorageInfoHandle_t handle);


//============================================================================
// System Information
//============================================================================

typedef enum {

    DXAPPDATA_SUCCESS            = 0,

    DXAPPDATA_ERROR              = -1,

    DXAPPDATA_INVALID_HANDLE     = -2,

    DXAPPDATA_SIZE_LIMIT_REACHED = -3,

} dxAPPDATA_RET_CODE;

typedef enum
{
    DXBLOCKID_DEVINFO           = 0,

    DXBLOCKID_SYSINFO           = 1,

} dxBLOCKID_t;

/**
 * \brief This pointer definition refer dxAppDataAccessCtrl_t (check dxBSP.c).
 */
typedef void * dxAppDataHandle_t;

/**
 * \brief the function suggest called once while system startup to initialize DEVINFO or SYSINFO.
 *        0: DXBLOCKID_DEVINFO, 1: DXBLOCKID_SYSINFO.
 */
dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t block_id);

/**
 * \param[in,out] pnSizeOfAPInfo    Provide the size of the buffer to store data and return the number of bytes
 *                                  return to the buffer.
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_SIZE_LIMIT_REACHED if buffer size is not enough to store data
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo);

/**
 * \brief Read the last written data size. Before using dxAppData_Read(), the function should be call to obtain
 *        the last written data size and allocate enough memory for dxAppData_Read()
 *
 * \return > 0      if success
 *         == 0     otherwize
 */
uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle);

/**
 * \brief Maximum 4076 bytes data can be written due to limitation of structure dxAppInfo_t (uint16_t Size)
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_INVALID_HANDLE     if system information handle is incorrect
 *         DXAPPDATA_SIZE_LIMIT_REACHED if nSizeOfAPInfo is 0 or larger than 4076
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo);

/**
 * \brief The function will clear both DX_DEVINFO_FLASH_ADDR1 and DX_DEVINFO_FLASH_ADDR2
 *        or DX_SYSINFO_FLASH_ADDR1 and DX_SYSINFO_FLASH_ADDR2 sectors, determined by handle.
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_INVALID_HANDLE     if system information handle is incorrect
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle);

/**
 * \return DXAPPDATA_SUCCESS        if success
 *         DXAPPDATA_INVALID_HANDLE if system information handle is incorrect
 */
dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle);


#endif // _DXBSP_H
