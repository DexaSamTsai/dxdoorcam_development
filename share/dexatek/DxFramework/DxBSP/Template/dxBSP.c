//============================================================================
// File: dxBSP.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "dxBSP.h"

//============================================================================
// UART Wrapper
//============================================================================
dxUART_RET_CODE dxUART_Close(dxUART_t *pdxUart)
{
	// Todo...

    return DXUART_ERROR;
}

dxUART_RET_CODE dxUART_GetCh(dxUART_t *pdxUart, uint8_t *ch)
{
	// Todo...

    return DXUART_ERROR;
}

dxUART_t *dxUART_Open(dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option)
{
	// Todo...

    return NULL;
}

dxUART_RET_CODE dxUART_PutCh(dxUART_t *pdxUart, uint8_t ch)
{
	// Todo...

    return DXUART_ERROR;
}

int dxUART_Receive_Blocked(dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms)
{
	// Todo...

    return DXUART_ERROR;
}

int dxUART_Send_Blocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len)
{
	// Todo...

    return DXUART_ERROR;
}


//============================================================================
// BSP
//============================================================================
dxBSP_RET_CODE dxBSP_Init(void)
{
    return DXBSP_ERROR;
}


//============================================================================
// GPIO
//============================================================================
static dxGPIO_t *dxGPIO_GetObject(uint32_t PinName)
{
	// Todo...

    return NULL;
}

dxBSP_RET_CODE dxGPIO_Init(dxGPIO_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxGPIO_Read(uint32_t PinName)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxGPIO_Write(uint32_t PinName, dxPIN_Level level)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxGPIO_Set_Direction(uint32_t PinName, dxPIN_Direction Dir)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxGPIO_Get_Direction(uint32_t PinName)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxGPIO_Uninit(dxGPIO_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}


//============================================================================
// PWM
//============================================================================
dxBSP_RET_CODE dxPWM_Init(dxPWM_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPWM_Uninit(dxPWM_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPWM_Write(uint32_t PinName, float PwmPeriod)
{
	// Todo...

    return DXBSP_ERROR;
}


//============================================================================
// I2C
//============================================================================
dxBSP_RET_CODE dxI2C_Init(dxI2C_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxI2C_Uninit(dxI2C_t* ConfigTable)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxI2C_Master_Write(uint32_t Pin_I2C_SDA,
                       int address,
                       const char *data,
                       int length,
                       dxBOOL stop)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxI2C_Master_Read(uint32_t Pin_I2C_SDA,
                      int address,
                      char *data,
                      int length,
                      dxBOOL stop)
{
	// Todo...

    return DXBSP_ERROR;
}

void dxI2C_Reset(dxI2C_t *pHandle)
{
	// Todo...
}

uint8_t dxI2C_Master_U8_Read(uint32_t Pin_I2C_SDA,
                             int address,
                             dxBOOL last)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxI2C_Master_U8_Write(uint32_t Pin_I2C_SDA,
                          int address,
                          uint8_t data)
{
	// Todo...

    return DXBSP_ERROR;
}

void dxI2C_Slave_Mode(uint32_t Pin_I2C_SDA,
                      dxBOOL enable_slave)
{
	// Todo...
}

dxI2C_RET_CODE dxI2C_Slave_ReceiveStatus(uint32_t Pin_I2C_SDA)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxI2C_Slave_Read(uint32_t Pin_I2C_SDA,
                     char *data,
                     int length)
{
	// Todo...

    return DXBSP_ERROR;
}

int dxI2C_Slave_Write(uint32_t Pin_I2C_SDA,
                      const char *data,
                      int length)
{
	// Todo...

    return DXBSP_ERROR;
}

void dxI2C_Slave_Address(uint32_t Pin_I2C_SDA,
                         int idx,
                         uint32_t address,
                         uint32_t mask)
{
	// Todo...
}


//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void)
{
	// Todo...
}

void dxPLATFORM_Init(void)
{
	// Todo...
}

dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac)
{
	// Todo...

    return DXBSP_ERROR;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void)
{
	// Todo...

    return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void)
{
	// Todo...

    return DXBSP_ERROR;
}


//============================================================================
// FLASH API
//============================================================================
dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash)
{
	// Todo...

    return DXOS_ERROR;
}

dxFlashHandle_t dxFlash_Init(void)
{
	// Todo...

    return NULL;
}

dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Read(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Read_Word(dxFlashHandle_t flash, uint32_t address, uint32_t *data)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Write(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Write_Word(dxFlashHandle_t flash, uint32_t address, uint32_t data)
{
	// Todo...

    return DXOS_ERROR;
}

dxOS_RET_CODE dxFlash_Uninit(dxFlashHandle_t flash)
{
	// Todo...

    return DXOS_ERROR;
}


//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================
dxMAINIMAGE_RET_CODE dxMainImage_Init(void)
{
	// Todo...

    return DXMAINIMAGE_ERROR;
}

dxMAINIMAGE_RET_CODE dxMainImage_Start(uint8_t *pszMD5Sum, uint32_t nImageSize)
{
	// Todo...

    return DXMAINIMAGE_ERROR;
}

dxMAINIMAGE_RET_CODE dxMainImage_Write(uint8_t *pImage, uint32_t nImageLen)
{
	// Todo...

    return DXMAINIMAGE_ERROR;
}

dxMAINIMAGE_RET_CODE dxMainImage_UpdateReboot(dxBOOL WithSystemSelfReboot)
{
	// Todo...

    return DXMAINIMAGE_ERROR;
}

dxMAINIMAGE_RET_CODE dxMainImage_Stop(void)
{
	// Todo...

    return DXMAINIMAGE_ERROR;
}


//============================================================================
// SUB IMAGE UPDATE API
//============================================================================
static dxSUBIMAGE_RET_CODE dxSubImage_WriteVerify(dxFlashHandle_t dxFlash, uint32_t address, uint32_t length, uint8_t *pData)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}

dxSubImgInfoHandle_t dxSubImage_Init(dxSUBIMGBLOCKID_t blockId)
{
	// Todo...

    return NULL;
}

dxSUBIMAGE_RET_CODE dxSubImage_Start(dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}

dxSUBIMAGE_RET_CODE dxSubImage_Write(dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}

dxSUBIMAGE_RET_CODE dxSubImage_Read(dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead,
                                    uint8_t *pBDataBuffer, uint32_t *FwSizeOut)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}

dxSUBIMAGE_RET_CODE dxSubImage_Uninit(dxSubImgInfoHandle_t handle)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}

uint32_t dxSubImage_ReadImageSize(dxSubImgInfoHandle_t handle)
{
	// Todo...

    return DXSUBIMAGE_ERROR;
}


//============================================================================
// Hyper Perioheral Storage Access API
//============================================================================
dxHpStorageInfoHandle_t dxHpStorage_Init(dxHPSTORAGESECTORID_t sector_id)
{
	// Todo...

    return NULL;
}

dxHPSTORAGE_RET_CODE dxHpStorage_Write(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
	// Todo...

    return DXHPSTORAGE_ERROR;
}

dxHPSTORAGE_RET_CODE dxHpStorage_Read(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
	// Todo...

    return DXHPSTORAGE_ERROR;
}

dxHPSTORAGE_RET_CODE dxHpStorage_EraseNumOfSector(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint16_t NumOfSector)
{
	// Todo...

    return DXHPSTORAGE_ERROR;
}


dxHPSTORAGE_RET_CODE dxHpStorage_Uninit(dxHpStorageInfoHandle_t handle)
{
	// Todo...

    return DXHPSTORAGE_ERROR;
}

uint32_t dxHpStorage_ReadTotalSize(dxHpStorageInfoHandle_t handle)
{
	// Todo...

    return 0;
}

uint32_t dxHpStorage_ReadSectorSize(dxHpStorageInfoHandle_t handle)
{
	// Todo...

    return 0;
}


//============================================================================
// System Information
//============================================================================
dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t blockId)
{
	// Todo...

    return NULL;
}

dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo)
{
	// Todo...

    return DXAPPDATA_SUCCESS;
}

uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle)
{
	// Todo...

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo)
{
	// Todo...

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle)
{
	// Todo...

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle)
{
	// Todo...

    return DXAPPDATA_SUCCESS;
}

