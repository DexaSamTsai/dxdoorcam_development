//============================================================================
// File: dxGPIOConfig.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#ifndef _DXGPIOCONFIG_H
#define _DXGPIOCONFIG_H

#define DX_NOT_ASSIGNED                 0xFFFFFFFE

//README FIRST!!!
/**
 * The following GPIOs are default.
 * If your project use different configuration, you should
 *
 * 1) Make a copy of dxGPIOConfig.h and rename to dxGPIO.h
 * 2) Move dxGPIO.h to your project's directory
 * 3) Make sure configUSE_LOCAL_GPIO_SETTING is defined euqal to 1
 */

#define GPIO_GROUP_1                    0       //RTS SYSTEM GPIO Group Offset
#define GPIO_GROUP_2                    300     //RTS CIS GPIO Group Offset


//For RTS390X GPIO definition please check linux-3.10\drivers\pinctrl\pinctrl-rts.c
#define DK_BLE_CENTRAL_MODE             (GPIO_GROUP_1 + 9)
#define DK_BLE_RESET                    (GPIO_GROUP_1 + 3)

#define DK_LED_BLUE                     DX_NOT_ASSIGNED
#define DK_LED_YELLOW                   DX_NOT_ASSIGNED
#define DK_LED_RED                      DX_NOT_ASSIGNED     //TBD

#define DK_UART0_TX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X
#define DK_UART0_RX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X

#define DK_UART1_TX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X
#define DK_UART1_RX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X

#define DK_UART2_TX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X
#define DK_UART2_RX                     DX_NOT_ASSIGNED     //NOT USE IN RTS390X

#define MAIN_BUTTON_GPIO                (GPIO_GROUP_1 + 1)
#define SUB_BUTTON_GPIO                 DX_NOT_ASSIGNED     //Secondary button for Test Purpose

#endif // _DXGPIOCONFIG_H
