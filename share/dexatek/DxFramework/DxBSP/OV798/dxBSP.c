//============================================================================
// File: dxBSP.c
//
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dxMD5.h"
#include "dxSysDebugger.h"

extern uint16_t Crc16(uint8_t *pdata, int16_t nbytes, uint16_t crc);

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif


//============================================================================
// UART Wrapper
//============================================================================
// UART0 ===> Console
// UART1 ===> MCU
# define OV_UART_RX_BUFFER_MAX		8192
typedef struct {
	u16		r;
	u16		w;
	u8		data[ OV_UART_RX_BUFFER_MAX ];
} Uart_RX_Buffer;

typedef struct {
	dxBOOL			open;
	dxUART_Port_t	port;
	Uart_RX_Buffer	*rx;

	void			( *set_config )	( uint32_t, uint32_t );
	uint8_t			( *read )		( void );
	int				( *write )		( uint8_t );
	void			( *close )		( void );
} OV_Uart;

static OV_Uart uart0 = {
	.open		= dxFALSE,
	.port		= DXUART_PORT0,
	.set_config	= uart_config,
	.read		= uart_getc,
	.write		= uart_putc,
};

static OV_Uart uart1 = {
	.open		= dxFALSE,
	.port		= DXUART_PORT1,
	.set_config	= uart1_config,
	.read		= uarts_getc,
	.write		= uarts_putc,
};


static int
uart0_irq_handler( void *arg )
{
	// Todo
	dbg_warm( "Todo..." );

	return 0;
}

static int
uart1_irq_handler( void *arg )
{
    u8 ch;

	while ( uarts_getc_nob( &ch ) == 0 ) {
		uart1.rx->data[ uart1.rx->w ++ ]	= ch;

		if ( uart1.rx->w >= OV_UART_RX_BUFFER_MAX ) {
			uart1.rx->w = 0;
		}
	}

	return 0;
}

static int
uart_get_ch( OV_Uart *uart, uint8_t *ch, uint32_t timeout )
{
	dxTime_t curr_time	= dxTimeGetMS();

	while ( uart->rx->w == uart->rx->r ) {
		if ( timeout != DXOS_WAIT_FOREVER ) {
			if ( curr_time + timeout < dxTimeGetMS() ) {
				dbg_warm( "DXUART_ERROR timeout: %u", timeout );
	    		return DXUART_ERROR;
			}
		}

		dxTimeDelayMS( 10 );
	}

	*ch	= uart->rx->data[ uart->rx->r ++ ];

	if ( uart->rx->r >= OV_UART_RX_BUFFER_MAX ) {
		uart->rx->r = 0;
	}

	return DXUART_SUCCESS;
}

dxUART_RET_CODE
dxUART_Close( dxUART_t *pdxUart )
{
    if ( pdxUart == NULL ) {
        return DXUART_ERROR;
    } else {
		OV_Uart *uart	= ( OV_Uart * )pdxUart;

		uart->close();

	    return DXUART_SUCCESS;
    }
}

dxUART_t *
dxUART_Open( dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option )
{
	OV_Uart *uart	= NULL;

	switch ( port ) {
		case DXUART_PORT0:
			uart	= &uart0;
			break;

		case DXUART_PORT1:
			uart	= &uart1;
			break;

		case DXUART_PORT2:
			return NULL;
	}

	if ( uart == NULL ) {
		return NULL;
	} else if ( uart->open == dxTRUE ) {
		return NULL;
	}

	uart->rx = dxMemAlloc( NULL, 1, sizeof( Uart_RX_Buffer ) );

	uart ->set_config( baudrate, UART_IER_RDI );

	if ( port == DXUART_PORT0 ) {
		lowlevel_set( 0 );
		irq_request( IRQ_BIT_UART0, uart0_irq_handler, "UART0", NULL );
	} else {
		lowlevel_set( 1 );
		irq_request( IRQ_BIT_UART1, uart1_irq_handler, "UART1", NULL );
	}

	uart->open = dxTRUE;

	return ( dxUART_t * )uart;
}

dxUART_RET_CODE
dxUART_GetCh( dxUART_t *pdxUart, uint8_t *ch )
{
    if ( pdxUart == NULL ) {
        return DXUART_ERROR;
    } else {
		return uart_get_ch( ( OV_Uart * )pdxUart, ch, 300 );
    }
}

dxUART_RET_CODE
dxUART_PutCh( dxUART_t *pdxUart, uint8_t ch )
{
    if ( pdxUart == NULL ) {
        return DXUART_ERROR;
    } else {
		OV_Uart *uart	= ( OV_Uart * )pdxUart;

		uart->write( ch );

	    return DXUART_SUCCESS;
    }
}

int
dxUART_Receive_Blocked( dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms )
{
	if ( pdxUart == NULL ) {
		return DXUART_ERROR;
	} else {
		OV_Uart *uart	= ( OV_Uart * )pdxUart;

		if ( uart->open == dxFALSE ) {
			return DXUART_ERROR;
		}

		int count;

		for ( count = 0; count < len; count ++ ) {
			if ( uart_get_ch( ( OV_Uart * )pdxUart, &prxbuf[ count ], timeout_ms ) == DXUART_ERROR ) {
				return count;
			}
		}

		return count;
	}
}

int
dxUART_Send_Blocked( dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len )
{
    if ( pdxUart == NULL ) {
        return DXUART_ERROR;
    } else {
		OV_Uart *uart	= ( OV_Uart * )pdxUart;

        if ( uart->open == dxFALSE ) {
            return DXUART_ERROR;
        }

        int count;

        for( count = 0; count < len; count ++ ) {
            uart->write( ptxbuf[ count ] );
        }

	    return count;
    }
}

//============================================================================
// BSP
//============================================================================
//README FIRST!!!
//Make sure the assigned GPIO for general purpose DOES NOT conflict
//with the dxKeypadConfig from dxKeypadManager.
static dxGPIO_t dxGPIOConfig[] =
{
    {
        NULL,
        DK_BLE_CENTRAL_MODE,
        DXPIN_OUTPUT,
        DXPIN_PULLNONE
    },
    {
        NULL,
        DK_BLE_RESET,
        DXPIN_OUTPUT,
        DXPIN_PULLNONE
    },
    {
        NULL,
        DK_LED_BLUE,
        DXPIN_OUTPUT,
        DXPIN_PULLDOWN
    },
    {
        NULL,
        DK_LED_YELLOW,
        DXPIN_OUTPUT,
        DXPIN_PULLDOWN
    },
    {
        NULL,
        DK_LED_RED,
        DXPIN_OUTPUT,
        DXPIN_PULLDOWN
    },


    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        ( int )DX_ENDOF_GPIO_CONFIG,
        ( int )DX_ENDOF_GPIO_CONFIG,
        ( int )DX_ENDOF_GPIO_CONFIG
    }
};

/**
 * \return DXBSP_SUCCESS    if BSP initialize success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxBSP_Init(void)
{
    dxBSP_RET_CODE result = DXBSP_ERROR;

    dxGPIO_Init(dxGPIOConfig);

    return result;
}

//============================================================================
// GPIO
//============================================================================
static dxGPIO_t *dxGPIOTable = NULL;

static dxGPIO_t *
dxGPIO_GetObject( uint32_t PinName )
{
    if ( dxGPIOTable == NULL ) {
        return NULL;
    }

    dxGPIO_t *p = NULL;

    for ( p = dxGPIOTable; p->Name != DX_ENDOF_GPIO_CONFIG; p ++ ) {
        if ( p != NULL && p->Name == PinName ) {
            return p;
        }
    }

    return NULL;
}

dxBSP_RET_CODE
dxGPIO_Init( dxGPIO_t *ConfigTable )
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    if (dxGPIOTable != NULL) {
        ret = dxGPIO_Uninit(dxGPIOTable);

        if (ret != DXBSP_SUCCESS) {
            return ret;
        }
    }

    dxGPIOTable = ConfigTable;

    dxGPIO_t *pGPIO;

    for ( pGPIO = dxGPIOTable; pGPIO->Name != DX_ENDOF_GPIO_CONFIG; pGPIO ++ ) {
        if ( pGPIO->Name != DX_NOT_ASSIGNED ) {
            if ( pGPIO->Direction == DXPIN_INPUT ) {
            	libgpio_config( pGPIO->Name, PIN_DIR_INPUT );
            } else if ( pGPIO->Direction == DXPIN_OUTPUT ) {
            	libgpio_config( pGPIO->Name, PIN_DIR_OUTPUT );
            }
	    }
    }

    return DXBSP_SUCCESS;
}

int
dxGPIO_Read( uint32_t PinName )
{
	if ( PinName != DX_NOT_ASSIGNED ) {
		dxGPIO_t *pGPIO = dxGPIO_GetObject( PinName );

		if ( pGPIO == NULL ) {
			return DXBSP_ERROR;
		} else if ( pGPIO->Direction != DXPIN_INPUT ) {
			return DXBSP_ERROR;
		}

		return libgpio_read( PinName );
	} else {
		return DXBSP_ERROR;
	}
}

dxBSP_RET_CODE
dxGPIO_Write( uint32_t PinName, dxPIN_Level level )
{
	if ( PinName != DX_NOT_ASSIGNED ) {
		dxGPIO_t *pGPIO = dxGPIO_GetObject( PinName );

		if ( pGPIO == NULL ) {
			return DXBSP_ERROR;
		} else if ( pGPIO->Direction != DXPIN_OUTPUT ) {
			return DXBSP_ERROR;
		}

		libgpio_write( PinName, level );

		return DXBSP_SUCCESS;
	} else {
		return DXBSP_ERROR;
	}
}

dxBSP_RET_CODE
dxGPIO_Set_Direction( uint32_t PinName, dxPIN_Direction Dir )
{
	if ( PinName != DX_NOT_ASSIGNED ) {
		dxGPIO_t *pGPIO = dxGPIO_GetObject( PinName );

		if ( pGPIO == NULL ) {
			return DXBSP_ERROR;
		}

		// Change Direction
		if ( ( Dir == DXPIN_INPUT ) || ( Dir == DXPIN_OUTPUT ) ) {
			pGPIO->Direction	= Dir;

			if ( pGPIO->Direction == DXPIN_INPUT ) {
				libgpio_config( pGPIO->Name, PIN_DIR_INPUT );
			} else if ( pGPIO->Direction == DXPIN_OUTPUT ) {
				libgpio_config( pGPIO->Name, PIN_DIR_OUTPUT );
			}
		} else {
			return DXBSP_ERROR;
		}

		return DXBSP_SUCCESS;
	} else {
		return DXBSP_ERROR;
	}
}

int
dxGPIO_Get_Direction( uint32_t PinName )
{
	if ( PinName != DX_NOT_ASSIGNED ) {
		dxGPIO_t *pGPIO = dxGPIO_GetObject( PinName );

		if ( pGPIO == NULL ) {
			return DXBSP_ERROR;
		}

		return pGPIO->Direction;
	} else {
		return DXBSP_ERROR;
	}
}

dxBSP_RET_CODE
dxGPIO_Uninit( dxGPIO_t *ConfigTable )
{
	if ( ConfigTable ) {
		dxGPIOTable	= NULL;

		return DXBSP_SUCCESS;
	} else {
		return DXBSP_ERROR;
	}
}

//============================================================================
// PWM
//============================================================================
static dxPWM_t* dxPWMTable = NULL;

static dxPWM_t *dxPWM_GetObject(uint32_t PinName)
{
	return NULL;
}


dxBSP_RET_CODE dxPWM_Init(dxPWM_t* ConfigTable)
{
	return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPWM_Uninit(dxPWM_t* ConfigTable)
{
	return DXBSP_ERROR;
}

dxBSP_RET_CODE dxPWM_Write(uint32_t PinName, float PwmPeriod)
{
	return DXBSP_ERROR;
}

//============================================================================
// I2C
//============================================================================
static dxI2C_t* dxI2CTable = NULL;

dxBSP_RET_CODE dxI2C_Init(dxI2C_t* ConfigTable)
{
	return DXBSP_ERROR;
}

dxBSP_RET_CODE dxI2C_Uninit(dxI2C_t* ConfigTable)
{
	return DXBSP_ERROR;
}

int dxI2C_Master_Write(uint32_t Pin_I2C_SDA,
                       int address,
                       const char *data,
                       int length,
                       dxBOOL stop)
{
	return 0;
}

int dxI2C_Master_Read(uint32_t Pin_I2C_SDA,
                      int address,
                      char *data,
                      int length,
                      dxBOOL stop)
{
	return 0;
}

void dxI2C_Reset(dxI2C_t *pHandle)
{
}

uint8_t dxI2C_Master_U8_Read(uint32_t Pin_I2C_SDA,
                             int address,
                             dxBOOL last)
{
	return 0;
}

int dxI2C_Master_U8_Write(uint32_t Pin_I2C_SDA,
                          int address,
                          uint8_t data)
{
	return 0;
}

void dxI2C_Slave_Mode(uint32_t Pin_I2C_SDA,
                      dxBOOL enable_slave)
{
}

dxI2C_RET_CODE dxI2C_Slave_ReceiveStatus(uint32_t Pin_I2C_SDA)
{
	return DXBSP_ERROR;
}

int dxI2C_Slave_Read(uint32_t Pin_I2C_SDA,
                     char *data,
                     int length)
{
	return 0;
}

int dxI2C_Slave_Write(uint32_t Pin_I2C_SDA,
                      const char *data,
                      int length)
{
	return 0;
}

void dxI2C_Slave_Address(uint32_t Pin_I2C_SDA,
                         int idx,
                         uint32_t address,
                         uint32_t mask)
{
}

//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void)
{
	dbg_warm( "Todo..." );
}

extern void *dxMalloc(size_t size);
extern void dxFree(void *pv);

void dxPLATFORM_Init(void)
{
	dbg_warm( "Todo..." );
}

dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac)
{
    uint8_t mac[ 6 ];

    if ( pmac == NULL ) {
        return DXBSP_ERROR;
    }

	mac[ 0 ] = pmac->MacAddr[ 0 ];
	mac[ 1 ] = pmac->MacAddr[ 1 ];
	mac[ 2 ] = pmac->MacAddr[ 2 ];
	mac[ 3 ] = pmac->MacAddr[ 5 ];
	mac[ 4 ] = pmac->MacAddr[ 6 ];
	mac[ 5 ] = pmac->MacAddr[ 7 ];

	wlan_set_mac( mac );

    if ( prmac ) {
		wlan_get_mac( mac );

		prmac->MacAddr[ 0 ] = mac[ 0 ];
		prmac->MacAddr[ 1 ] = mac[ 1 ];
		prmac->MacAddr[ 2 ] = mac[ 2 ];
        prmac->MacAddr[ 3 ] = 0x0FF;
        prmac->MacAddr[ 4 ] = 0x0FF;
		prmac->MacAddr[ 5 ] = mac[ 3 ];
		prmac->MacAddr[ 6 ] = mac[ 4 ];
		prmac->MacAddr[ 7 ] = mac[ 5 ];
    }

    return DXBSP_SUCCESS;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;

    if ( TimeOut ) {
        ertos_task_set_watchdog( NULL, ( TimeOut * 100 ) );
    } else {
        ertos_task_set_watchdog( NULL, ( 10 * 100 ) );
    }

    return ret;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;

    ertos_watchdog_start();

    return ret;
}



dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;

    ertos_watchdog_update();

    return ret;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;

    ertos_watchdog_stop();

    return ret;
}

//============================================================================
// FLASH API
//============================================================================
# define OV_FLASH_SECTER_SIZE		0x1000
static dxFlashHandle_t *dxFlashHandle = NULL;

typedef struct {
	t_sf_hdr	ov_header;
} ov_flash_info;

int dxFlash_GetFlashInfo(dxFLASH_FLASHINFO_TYPE type)
{
	t_sflash * flash = sf_info();
	if(flash == NULL){
		return DXBSP_ERROR;
	}

	if(type==DXFLASH_FLASHINFO_BLOCKSIZE)
    {
        return flash->block_size;
    }
    else if(type==DXFLASH_FLASHINFO_SECTORSIZE)
    {
        return flash->sector_size;
    }
    else if(type==DXFLASH_FLASHINFO_PAGESIZE)
    {
        return flash->page_size;
    }
    else
    {
        return DXBSP_ERROR;
    }
}

int dxFlash_GetPartitionSize(dxFLASH_PARTITIONINFO_TYPE type)
{

     if(type==DXFLASH_PARTITIONINFO_HWHEADER)
     {
         return 0x1000;
     }

    t_sf_hdr sf_hdr;
    if (dxFlashHandle != NULL)
    {
        memcpy(&sf_hdr, dxFlashHandle, sizeof(t_sf_hdr));
    }
    else
    {
	    if(partition_get_hwhead(&sf_hdr) != 0)
	    {
		    return DXBSP_ERROR;
	    }
	}

    if(type==DXFLASH_PARTITIONINFO_BL1)
    {
        return sf_hdr.bl_len;
    }
    else if(type==DXFLASH_PARTITIONINFO_BL2)
    {
        return sf_hdr.bl2_len;
    }
    else if(type==DXFLASH_PARTITIONINFO_FW1)
    {
        return sf_hdr.fw_len;
    }
    else if(type==DXFLASH_PARTITIONINFO_FW2)
    {
        return sf_hdr.fw2_len;
    }
    else if(type==DXFLASH_PARTITIONINFO_CAIL)
    {
        return sf_hdr.cali_len;
    }
    else if(type==DXFLASH_PARTITIONINFO_USERCONFIG)
    {
        return sf_hdr.user_config_len;
    }
    else
    {
        return DXBSP_ERROR;
    }
}

int dxFlash_CheckPartition(uint32_t address, uint32_t len)
{
    if((address >= 0x0) && (address < DK_FLASH_PARTIONS_BL1_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_BL1_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_BL1_ADDR) && (address < DK_FLASH_PARTIONS_BL2_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_BL2_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_BL2_ADDR) && (address < DK_FLASH_PARTIONS_FW1_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_FW1_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_FW1_ADDR) && (address < DK_FLASH_PARTIONS_FW2_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_FW2_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_FW2_ADDR) && (address < DK_FLASH_PARTIONS_CAIL_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_CAIL_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_CAIL_ADDR) && (address < DK_FLASH_PARTIONS_USERCONFIG_ADDR))
    {
        return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_USERCONFIG_ADDR);
    }
    else if ((address >= DK_FLASH_PARTIONS_USERCONFIG_ADDR) && (address < DK_FLASH_PARTIONS_MAX_ADDR))
    {
        //Check which bolck in USER CONFIG Partition
        if ((address >= DK_FLASH_USERCONFIG_BASICE_ADDR) && (address < DK_FLASH_USERCONFIG_HW_ADDR))
        {
            return dxFlash_CheckOverPartition(address+len,DK_FLASH_USERCONFIG_HW_ADDR);
        }
        else if ((address >= DK_FLASH_USERCONFIG_HW_ADDR) && (address < DK_FLASH_USERCONFIG_FRAMEWORK_ADDR))
        {
            return dxFlash_CheckOverPartition(address+len,DK_FLASH_USERCONFIG_FRAMEWORK_ADDR);
        }
        else if ((address >= DK_FLASH_USERCONFIG_FRAMEWORK_ADDR) && (address < DK_FLASH_USERCONFIG_CAMERA_ADDR))
        {
            return dxFlash_CheckOverPartition(address+len,DK_FLASH_USERCONFIG_CAMERA_ADDR);
        }
        else if ((address >= DK_FLASH_USERCONFIG_CAMERA_ADDR) && (address < DK_FLASH_PARTIONS_MAX_ADDR))
        {
            return dxFlash_CheckOverPartition(address+len,DK_FLASH_PARTIONS_MAX_ADDR);
        }
        else
        {
            return DXOS_ERROR;
        }
    }
    else
    {
        return DXOS_ERROR;
    }
}

int dxFlash_CheckOverPartition(uint32_t endaddr, uint32_t partitionedge)
{
    if(endaddr < partitionedge)
    {
        return DXOS_SUCCESS;
    }
    else
    {
        return DXOS_ERROR;
    }
}

dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash)
{
    return DXOS_UNAVAILABLE;

//    dxSPIaccess_ResourceTake(); //WORKAROUND ONLY - See header for detail
//    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash)
{
    return DXOS_UNAVAILABLE;
//    dxSPIaccess_ResourceGive(); //WORKAROUND ONLY - See header for detail
//    return DXOS_SUCCESS;
}

dxFlashHandle_t
dxFlash_Init( void )
{
    if ( dxFlashHandle == NULL ) {
    	ov_flash_info *ov_flash	= dxMemAlloc( NULL, 1, sizeof( ov_flash_info ) );

        if ( ov_flash ) {
			if ( partition_get_hwhead( ( t_sf_hdr * )&ov_flash->ov_header ) == DXOS_ERROR ) {
				dxMemFree( ov_flash );
				ov_flash = NULL;
			}
        }

        dxFlashHandle	= ( dxFlashHandle_t )ov_flash;
    }

    return dxFlashHandle;
}

dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address)
{
	dbg_warm();
    if ( flash == NULL ) {
        return DXOS_ERROR;
    }

    address = ( address / DEFAULT_FLASH_SECTOR_ERASE_SIZE ) * DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    if(dxFlash_CheckPartition(address,DEFAULT_FLASH_SECTOR_ERASE_SIZE)==DXOS_ERROR)
    {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }


    dxFlash_ResourceTake( flash );
    sf_erase( address, ERASE_SECTOR );
    dxFlash_ResourceGive( flash );

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size)
{
    uint8_t i = 0;
    uint32_t sizeforsector=0;

	dbg_warm();
    if (flash == NULL) {
        return DXOS_ERROR;
    }

    address = (address / DEFAULT_FLASH_SECTOR_ERASE_SIZE) * DEFAULT_FLASH_SECTOR_ERASE_SIZE;
    sizeforsector = (size / DEFAULT_FLASH_SECTOR_ERASE_SIZE) * DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    if(dxFlash_CheckPartition(address,sizeforsector)==DXOS_ERROR)
    {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }

    for(i = (size / DEFAULT_FLASH_SECTOR_ERASE_SIZE); i > 0; i--)
    {
        dxFlash_ResourceTake(flash);
		sf_erase( address, ERASE_SECTOR );
        dxFlash_ResourceGive(flash);

        address += DEFAULT_FLASH_SECTOR_ERASE_SIZE;
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxFlash_Read( dxFlashHandle_t flash, uint32_t address, uint32_t len, void *data )
{
    if ( flash == NULL ) {
        return DXOS_ERROR;
    }

    if ( dxFlash_CheckPartition( address, len ) == DXOS_ERROR ) {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }

    dxFlash_ResourceTake( flash );
    sf_read( data, len, address );
    dxFlash_ResourceGive( flash );

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxFlash_Read_Word( dxFlashHandle_t flash, uint32_t address, void *data )
{
    if ( flash == NULL ) {
        return DXOS_ERROR;
    }

    if ( dxFlash_CheckPartition( address, 4 ) == DXOS_ERROR ) {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }

    dxFlash_ResourceTake( flash );
    sf_read( data, 4, address );
    dxFlash_ResourceGive( flash );

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxFlash_Write( dxFlashHandle_t flash, uint32_t address, uint32_t len, void *data )
{
    if ( flash == NULL ) {
        return DXOS_ERROR;
    }

    //Check address is correct partition
    if ( dxFlash_CheckPartition( address, len ) == DXOS_ERROR ) {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }

	dxFlash_ResourceTake( flash );
	sf_write( data, len, address );
	dxFlash_ResourceGive( flash );

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxFlash_Write_Word( dxFlashHandle_t flash, uint32_t address, uint32_t data )
{
    if ( flash == NULL ) {
        return DXOS_ERROR;
    }

    if ( dxFlash_CheckPartition( address, 4 ) == DXOS_ERROR ) {
        dbg_warm( "Write data to Flash OVER to other partition!!!!!!" );
        return DXOS_ERROR;
    }

    dxFlash_ResourceTake( flash );
	sf_write( ( uint8_t * )&data, 4, address );
    dxFlash_ResourceGive( flash );

    return DXOS_SUCCESS;
}

dxOS_RET_CODE
dxFlash_Uninit( dxFlashHandle_t flash )
{
    if ( flash ) {
        free(flash);
        flash = NULL;
    }

    return DXOS_SUCCESS;
}

//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================
#define OFFSET_DATA                     0
#define BACKUP_SECTOR                   ( 0 - 0x1000 )

#define DX_MAINIMAGE_SIG0               0x35393138
#define DX_MAINIMAGE_SIG1               0x31313738

/**
 * \brief Structure to store MD5 checksum result
 */
typedef struct {
    uint32_t		nTotalReceiveBytes;		// File size in bytes
    uint32_t		nDownloadBytes;			// Current received bytes
    uint8_t         *pszFileMD5Sum;         // The MD5 checksum result of file, in hexadecimal string format.
                                            // For example, 0x41 => "41" (character " is not included)

    dxMD5_CTX       *pmd5cx;                //No used by OV798// MD5 checksum context
    uint8_t         *pmd5sum;               //No used by OV798// Current MD5 checksum data

	uint8_t			*ImageBuffer;
    uint32_t        ImageBufferBytes;

    uint8_t         BlockID;         // Reserved for expandability
} dxImgInfoAccessCtrl_t;

static dxImgInfoAccessCtrl_t dxMainImgInfoAccessCtrl = { 0, };

static dxBOOL
dxImage_MD5IsEqual( dxImgInfoAccessCtrl_t *pHandle )
{
    uint32_t len = 16;//strlen(pHandle->pmd5sum);
    if ( ( len * 2 ) != strlen( ( char * )pHandle->pszFileMD5Sum ) ) {
        return dxFALSE;
    }

    uint8_t i   = 0;
    uint8_t ch  = 0;
    uint8_t d   = 0;

    while ( i < len ) {
        ch = ( pHandle->pmd5sum[ i ] >> 4 ) & 0x0F;
        ch += 0x30;

        if ( ch > '9' ) {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[ i * 2 ];

        if ( d >= 'a' && d <= 'z' ) {
            d = d - 'a' + 'A';
        }

        if ( ch != d ) {
			return dxFALSE;
        }

        ch = ( pHandle->pmd5sum[ i ] ) & 0x0F;
        ch += 0x30;

        if ( ch > '9' ) {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[ ( i * 2 ) + 1 ];

        if ( d >= 'a' && d <= 'z' ) {
            d = d - 'a' + 'A';
        }

        if ( ch != d ) {
			return dxFALSE;
        }

        i ++;
    }

    return dxTRUE;
}

static void
dxImage_EraseArea( dxFlashHandle_t dxFlash, uint32_t address, uint32_t size )
{
    //EraseArea include upgreade function
	return ;
}

static void
dxImage_Free( dxImgInfoAccessCtrl_t *pHandle )
{
    if ( pHandle->pszFileMD5Sum ) {
        dxMemFree( pHandle->pszFileMD5Sum );
        pHandle->pszFileMD5Sum = NULL;
    }

    if ( pHandle->pmd5cx ) {
        dxMemFree( pHandle->pmd5cx );
        pHandle->pmd5cx = NULL;
    }

    if ( pHandle->pmd5sum ) {
        dxMemFree( pHandle->pmd5sum );
        pHandle->pmd5sum = NULL;
    }

    pHandle->nTotalReceiveBytes	= 0;
	pHandle->ImageBuffer		= NULL;
	pHandle->ImageBufferBytes	= 0;
}

static dxBOOL
dxImage_Init( dxImgInfoAccessCtrl_t *pHandle, uint8_t *pszMD5Sum, uint32_t nImageSize, uint32_t ImageBufferSize )
{
    dxImage_Free( pHandle );

    pHandle->nTotalReceiveBytes	= nImageSize;
    pHandle->nDownloadBytes		= 0;

    pHandle->pszFileMD5Sum = ( uint8_t * )dxMemAlloc( NULL, 1, MD5_DIGEST_LENGTH * 2 + 1 ); // Additional NULL character is needed.

    if ( pHandle->pszFileMD5Sum  == NULL ) {
        return dxFALSE;
    }

    memcpy( pHandle->pszFileMD5Sum, pszMD5Sum, strlen( ( char * )pszMD5Sum ) );

    pHandle->pmd5cx = ( dxMD5_CTX * )dxMemAlloc( NULL, 1, sizeof( dxMD5_CTX ) );

    if ( pHandle->pmd5cx  == NULL ) {
		dbg_warm( "pHandle->pmd5cx == NULL" );
        return dxFALSE;
    }

    /**
     * sizeof(dxMD5Info.pmd5cx->state) should be 16 bytes,
     * due to declare of "uint32_t state[4]"
     */
    pHandle->pmd5sum = ( uint8_t * )dxMemAlloc( NULL, 1, MD5_DIGEST_LENGTH + 1 );

    if ( pHandle->pmd5sum == NULL ) {
		dbg_warm( "pHandle->pmd5sum == NULL" );
        return dxFALSE;
    }

    dxMD5_Starts( pHandle->pmd5cx );

    /*
     * Initial Image Buffer in memory, when read full image, wrtie to mtd
     */
    pHandle->ImageBufferBytes   = ImageBufferSize;
    pHandle->ImageBuffer		= ( uint8_t * )CONFIG_FREE_MEM_5_START;
    if ( ( CONFIG_FREE_MEM_5_END - CONFIG_FREE_MEM_5_START ) < ImageBufferSize ) {
		dbg_warm( "( CONFIG_FREE_MEM_5_END( 0x%08x ) - CONFIG_FREE_MEM_5_START( 0x%08x ) ) < ImageBufferSize( %d )", CONFIG_FREE_MEM_5_END, CONFIG_FREE_MEM_5_START, ImageBufferSize );
        return dxFALSE;
    }

    memset( pHandle->ImageBuffer, 0xFF, pHandle->ImageBufferBytes );

    return dxTRUE;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Init(void)
{
    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Stop(void)
{
    dxImage_Free( &dxMainImgInfoAccessCtrl );

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Start( uint8_t *pszMD5Sum, uint32_t nImageSize )
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    if ( ( pszMD5Sum == NULL ) ||
		 ( strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 ) ) ||
		 ( nImageSize == 0 ) ) {
        return DXMAINIMAGE_ERROR;
    } else if ( nImageSize > DX_SYSINFO_FLASH_UPDATE_IMG_SIZE ) {
        return DXMAINIMAGE_SIZE_LIMIT_REACHED;
    } else {
    	dxMAINIMAGE_RET_CODE ret = DXMAINIMAGE_ERROR;

    	if ( dxImage_Init( &dxMainImgInfoAccessCtrl, pszMD5Sum, nImageSize, nImageSize ) == dxTRUE ) {
			ret = DXMAINIMAGE_SUCCESS;
		}

		return ret;
	}
}

dxMAINIMAGE_RET_CODE
dxMainImage_Write( uint8_t *pImage, uint32_t nImageLen )
{
	if ( ( pImage == NULL ) || ( nImageLen == 0 ) ) {
		dbg_warm( "( pImage == NULL ) || ( nImageLen == 0 )" );
		return DXMAINIMAGE_ERROR;
	} else if ( ( dxMainImgInfoAccessCtrl.pmd5cx == NULL ) ||
				 ( dxMainImgInfoAccessCtrl.pmd5sum == NULL ) ||
				 ( dxMainImgInfoAccessCtrl.ImageBuffer == NULL ) ||
				 ( dxMainImgInfoAccessCtrl.nTotalReceiveBytes == 0 ) ) {
		dbg_warm( "dxMainImgInfoAccessCtrl.pmd5cx: %p", dxMainImgInfoAccessCtrl.pmd5cx );
		dbg_warm( "dxMainImgInfoAccessCtrl.pmd5sum: %p", dxMainImgInfoAccessCtrl.pmd5sum );
		dbg_warm( "dxMainImgInfoAccessCtrl.ImageBuffer: %p", dxMainImgInfoAccessCtrl.ImageBuffer );
		dbg_warm( "dxMainImgInfoAccessCtrl.nTotalReceiveBytes: %d", dxMainImgInfoAccessCtrl.nTotalReceiveBytes );
		dxMainImage_Stop();
		return DXMAINIMAGE_ERROR;
    } else if ( ( dxMainImgInfoAccessCtrl.nDownloadBytes + nImageLen ) > dxMainImgInfoAccessCtrl.ImageBufferBytes ) {
		dbg_warm( "( dxMainImgInfoAccessCtrl.nDownloadBytes + nImageLen ) > dxMainImgInfoAccessCtrl.ImageBufferBytes" );
		dxMainImage_Stop();
		return DXMAINIMAGE_ERROR;
	}

    memcpy( dxMainImgInfoAccessCtrl.ImageBuffer + dxMainImgInfoAccessCtrl.nDownloadBytes, pImage, nImageLen ); // write image

    dxMD5_Update( dxMainImgInfoAccessCtrl.pmd5cx, ( unsigned char * )pImage , nImageLen );

    dxMainImgInfoAccessCtrl.nDownloadBytes += nImageLen;

    if ( dxMainImgInfoAccessCtrl.nDownloadBytes >= dxMainImgInfoAccessCtrl.nTotalReceiveBytes ) {
        // MD5 checksum verify
        dxMD5_Finish( dxMainImgInfoAccessCtrl.pmd5sum, dxMainImgInfoAccessCtrl.pmd5cx );

        if ( dxImage_MD5IsEqual( &dxMainImgInfoAccessCtrl ) == dxTRUE ) {
			if ( partition_upgrade_sf( dxMainImgInfoAccessCtrl.ImageBuffer ) == 0 ) {
				return DXMAINIMAGE_COMPLETE;
			} else {
				dbg_warm( "partition_upgrade_sf( dxMainImgInfoAccessCtrl.ImageBuffer ) != 0" );
            	dxMainImage_Stop();
				return DXMAINIMAGE_ERROR;
			}
        } else {
			dbg_warm( "dxImage_MD5IsEqual" );
            dxMainImage_Stop();
            return DXMAINIMAGE_ERROR;
        }
    }

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_UpdateReboot( dxBOOL WithSystemSelfReboot )
{
    dxMainImage_Stop();

	if ( WithSystemSelfReboot == dxTRUE ) {
        dxPLATFORM_Reset();
    }

    return DXMAINIMAGE_SUCCESS;
}

#if DX_SUBIMG_FLASH_SIZE                // SubImage only support on flash at least 4MB

//============================================================================
// SUB IMAGE UPDATE API
//============================================================================
#define DX_SUBIMAGE_SIG0               0x35393138
#define DX_SUBIMAGE_SIG1               0x31313738
#define DX_SUBIMAGE_MARK               0xF1E2D3C4B5A69788
#define DX_SUBIMAGE_MAX_DATA_SIZE      (DX_SUBIMG_FLASH_SIZE - sizeof(dxSubImageHeaderAccessCtrl_t) - sizeof(dxSubImgData_t))

//============================================================================
//    Header
//+-----------------------------+---------------------+--------------------+
//| Tag                         | ( 8 Bytes )         | dxSubImageHeader_t |
//+-----------------------------+---------------------+                    |
//| DeviceType                  | ( 2 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderVersion               | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderSize                  | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| ImageSize                   | ( 4 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| SubImageVersion             | ( 16 Bytes )        |                    |
//+-----------------------------+---------------------+--------------------+
//============================================================================
typedef struct
{
    uint64_t        Tag;                    // Must be DX_SUBIMAGE_MARK (check dxBSP.c)
    uint16_t        DeviceType;             // Distinguish this sub image for which device
    uint8_t         HeaderVer;              // For header extensions, start form 1
    uint8_t         HeaderSize;             // For simplify code, should be sizeof(dxSubImageHeaderAccessCtrl_t)
    uint32_t        ImageSize;              // SubImg from server must >0, should be less than DX_SUBIMG_FLASH_SIZE (except header)
    uint8_t         SubImgVer[16];          // Sub image version from supplier, use ASCII coding
} dxSubImageHeaderAccessCtrl_t;             // sizeof(dxSubImageHeaderAccessCtrl_t) should be 32 bytes

static dxImgInfoAccessCtrl_t *dxSubImgInfoAccessCtrl[ 1 ] = { NULL, };

static dxOS_RET_CODE
dxSubImage_WriteHeader( dxFlashHandle_t hFlash, uint32_t addr, uint32_t nImageLen )
{
    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( hFlash == NULL ) {
        return DXOS_ERROR;
    } else {
	    dxSubImageHeaderAccessCtrl_t dxHeader;

	    memset( &dxHeader, 0x00, sizeof( dxHeader ) );

	    dxHeader.Tag		= DX_SUBIMAGE_MARK;
	    dxHeader.DeviceType	= 0x00;
	    dxHeader.HeaderVer	= 0x01;
	    dxHeader.HeaderSize	= sizeof( dxSubImageHeaderAccessCtrl_t );
	    dxHeader.ImageSize	= nImageLen;

	    return dxFlash_Write( hFlash, addr, sizeof( dxHeader ), &dxHeader );
	}
}

static void
dxSubImage_EraseArea( dxFlashHandle_t dxFlash, uint32_t address, uint32_t size )
{
    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return;
    } else if ( ( dxFlash == NULL ) ||
				( address == 0 ) ||
				( size == 0 ) ) {
        return;
    } else {
	    int i, sectors = (size + DEFAULT_FLASH_SECTOR_ERASE_SIZE - 1) / DEFAULT_FLASH_SECTOR_ERASE_SIZE;

	    for ( i = 0; i < sectors; i ++ ) {
	        dxFlash_Erase_Sector( dxFlash, address + i * DEFAULT_FLASH_SECTOR_ERASE_SIZE );
	    }
	}
}

static dxSUBIMAGE_RET_CODE
dxSubImage_WriteVerify( dxFlashHandle_t dxFlash, uint32_t address, uint32_t length, uint8_t *pData )
{
    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( dxFlash == NULL ) ||
				( length == 0 ) ||
				( pData == NULL ) ) {
        return DXSUBIMAGE_ERROR;
    } else {
	    dxSUBIMAGE_RET_CODE ret = DXSUBIMAGE_VERIFY_ERROR;
	    uint8_t *pReadBuffer = dxMemAlloc( NULL, 1, length );

	    if ( pReadBuffer == NULL ) {
	        return DXSUBIMAGE_ERROR;
	    }

	    memset( pReadBuffer, 0x00, length );

	    if ( dxFlash_Read( dxFlash, address, length, pReadBuffer ) == DXOS_SUCCESS ) {
	    	if ( memcmp( pReadBuffer, pData, length ) == 0 ) {
	        	ret	= DXSUBIMAGE_SUCCESS;
	    	}
	    }

	    dxMemFree( pReadBuffer );

	    return ret;
	}
}

dxSubImgInfoHandle_t
dxSubImage_Init( dxSUBIMGBLOCKID_t blockId )
{
    dxImgInfoAccessCtrl_t *pHandle = NULL;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return NULL;
    } else if ( blockId != DXSUBIMGBLOCK_ONE ) {
    	return NULL;
	} else if ( dxSubImgInfoAccessCtrl[ blockId ] ) {
        return ( dxSubImgInfoHandle_t )dxSubImgInfoAccessCtrl[ blockId ];
    }

    pHandle = dxMemAlloc( NULL, 1, sizeof( dxImgInfoAccessCtrl_t ) );
	if ( pHandle == NULL ) {
		return NULL;
	}

    pHandle->BlockID = blockId;

    dxSubImgInfoAccessCtrl[ blockId ] = pHandle;

    return pHandle;
}

dxSUBIMAGE_RET_CODE
dxSubImage_Start( dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize )
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
		dbg_warm( "DX_FLASH_SIZE_TOTAL_MBITS < 32" );
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) || ( pszMD5Sum == NULL ) || ( nImageSize == 0 ) ) {
		dbg_warm( "( handle == NULL ) || ( pszMD5Sum == NULL ) || ( nImageSize == 0 )" );
		return DXSUBIMAGE_ERROR;
    } else if ( pHandle->BlockID != DXSUBIMGBLOCK_ONE ) {
		dbg_warm( "pHandle->BlockID != DXSUBIMGBLOCK_ONE" );
		return DXSUBIMAGE_ERROR;
	} else if ( nImageSize > DX_SUBIMG_FLASH_SIZE ) {
		dbg_warm( "nImageSize > DX_SUBIMG_FLASH_SIZE" );
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    } else if (  strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 ) ) {
		dbg_warm( "strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 )" );
		return DXSUBIMAGE_ERROR;
    } else {
    	dxMAINIMAGE_RET_CODE ret = DXSUBIMAGE_ERROR;

    	if ( dxImage_Init( pHandle, pszMD5Sum, nImageSize, ( nImageSize + sizeof( dxSubImageHeaderAccessCtrl_t ) ) ) == dxTRUE ) {
			ret = DXSUBIMAGE_SUCCESS;
		}

		return ret;
	}
}

dxSUBIMAGE_RET_CODE
dxSubImage_Write( dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen )
{
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    dxSUBIMAGE_RET_CODE ret = DXSUBIMAGE_ERROR;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) || ( pImage == NULL ) || ( nImageLen == 0 ) ) {
        return DXSUBIMAGE_ERROR;
	} else if ( pHandle->BlockID != DXSUBIMGBLOCK_ONE ) {
        return DXSUBIMAGE_ERROR;
	} else if ( ( pHandle->pmd5cx == NULL ) ||
				( pHandle->pmd5sum == NULL ) ||
				( pHandle->nTotalReceiveBytes <= 0 ) ||
				( pHandle->nDownloadBytes < 0 ) ) {
		return DXSUBIMAGE_ERROR;
    } else if ( ( pHandle->nDownloadBytes + nImageLen ) > DX_SUBIMG_FLASH_SIZE ) {
		return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    }

    uint32_t addr = DX_SUBIMG_FLASH_ADDR1 + sizeof( dxSubImageHeaderAccessCtrl_t ) + pHandle->nDownloadBytes;

//    if ( dxFlash_Write( pHandle->hFlash, addr, nImageLen, pImage ) != DXOS_SUCCESS ) {
//    	goto end;
//    }
//
//    ret = dxSubImage_WriteVerify( pHandle->hFlash, addr, nImageLen, pImage );
//
//    if ( ret != DXSUBIMAGE_SUCCESS ) {
//    	goto end;
//    }

    dxMD5_Update( pHandle->pmd5cx, pImage, nImageLen );

    pHandle->nDownloadBytes += nImageLen;

    ret	= DXSUBIMAGE_SUCCESS;

    if ( pHandle->nDownloadBytes >= pHandle->nTotalReceiveBytes ) {
        // MD5 checksum verify
        dxMD5_Finish( pHandle->pmd5sum, pHandle->pmd5cx );

        if ( dxImage_MD5IsEqual( pHandle ) == dxTRUE ) {
//            if ( dxSubImage_WriteHeader( pHandle->hFlash, DX_SUBIMG_FLASH_ADDR1, pHandle->nTotalReceiveBytes ) != DXOS_SUCCESS ) {
//                ret	= DXSUBIMAGE_ERROR;
//            }

            ret	= DXSUBIMAGE_COMPLETE;
        } else {
            ret	= DXSUBIMAGE_ERROR;
        }
    }

    return ret;
}

dxSUBIMAGE_RET_CODE
dxSubImage_Read( dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead, uint8_t *pBDataBuffer, uint32_t *FwSizeOut)
{
    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) ||
				( TotalbyteToRead == 0 ) ||
				( pBDataBuffer == NULL ) ||
				( FwSizeOut == NULL ) ) {
        return DXSUBIMAGE_ERROR;
    } else if ( TotalbyteToRead > DX_SUBIMG_FLASH_SIZE ) {
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    } else {
    	dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;
	    uint32_t address = DX_SUBIMG_FLASH_ADDR1 + sizeof( dxSubImageHeaderAccessCtrl_t ) + StartPosition;

//	    dxOS_RET_CODE ret = dxFlash_Read( pHandle->hFlash, address, TotalbyteToRead, pBDataBuffer );

//	    if ( ret != DXOS_SUCCESS ) {
//	        *FwSizeOut = 0;
//	        return DXAPPDATA_ERROR;
//	    }

	    *FwSizeOut = TotalbyteToRead;

	    return DXAPPDATA_SUCCESS;
	}
}

dxSUBIMAGE_RET_CODE
dxSubImage_Uninit( dxSubImgInfoHandle_t handle )
{
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( pHandle == NULL ) {
        return DXSUBIMAGE_ERROR;
    }

//    dxFlash_Uninit( pHandle->hFlash );

    dxSubImgInfoAccessCtrl[ pHandle->BlockID ] = NULL;

    dxMemFree( pHandle );

    return DXSUBIMAGE_SUCCESS;
}

uint32_t
dxSubImage_ReadImageSize( dxSubImgInfoHandle_t handle )
{
    uint32_t ImageSize = 0;

	if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return ImageSize;
    } else if ( handle == NULL ) {
        return ImageSize;
    } else {
	    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

	    dxSubImageHeaderAccessCtrl_t dxHeader;

	    memset( &dxHeader, 0x00, sizeof( dxHeader ) );

//	    if ( ( dxFlash_Read( pHandle->hFlash, DX_SUBIMG_FLASH_ADDR1, sizeof( dxHeader ), &dxHeader ) == DXOS_SUCCESS ) &&
//	         ( dxHeader.Tag == DX_SUBIMAGE_MARK ) &&
//	         ( dxHeader.DeviceType == 0x00 ) &&
//	         ( dxHeader.HeaderVer == 0x01 ) &&
//	         ( dxHeader.HeaderSize == sizeof( dxSubImageHeaderAccessCtrl_t ) ) ) {
//	         ImageSize = dxHeader.ImageSize;
//	    }

	    return ImageSize;
	}
}

#endif // DX_SUBIMG_FLASH_SIZE

//============================================================================
// Hyper Perioheral Storage Access API
//============================================================================

typedef struct
{
    uint32_t        nTotalSize;         // File size in bytes
    uint32_t        nSectorSize;        // Current block size in bytes
    dxBOOL          bInitialized;       // Initialization status
    uint8_t         nHPStorageSectorID; // Reserved for expandability

    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
} dxHpStorageInfoAccessCtrl_t;

static dxHpStorageInfoAccessCtrl_t *dxHpStorageInfoAccessCtrl[ 1 ] = { NULL, };

dxHpStorageInfoHandle_t dxHpStorage_Init(dxHPSTORAGESECTORID_t sector_id)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = NULL;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return NULL;
    }

    if (sector_id == DXHPSTORAGESECTOR_ONE && dxHpStorageInfoAccessCtrl[sector_id])
    {
        return (dxHpStorageInfoHandle_t)dxHpStorageInfoAccessCtrl[sector_id];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxHpStorageInfoAccessCtrl_t));

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->hFlash = dxFlash_Init();

    if (pHandle->hFlash == NULL)
    {
        dxMutexDelete(pHandle->hMutex);
        pHandle->hMutex = NULL;
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    if (sector_id == DXHPSTORAGESECTOR_ONE)
    {
        pHandle->nTotalSize = DX_HP_STORAGE_SIZE;
        pHandle->nSectorSize = DEFAULT_FLASH_SECTOR_ERASE_SIZE;
        pHandle->nHPStorageSectorID = sector_id;
        pHandle->bInitialized = dxTRUE;

        dxHpStorageInfoAccessCtrl[sector_id] = pHandle;
        return (dxHpStorageInfoHandle_t)dxHpStorageInfoAccessCtrl[sector_id];
    }

    return NULL;
}

#if DX_HP_STORAGE_SIZE != 0
dxHPSTORAGE_RET_CODE dxHpStorage_Write(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (handle && pHandle->nHPStorageSectorID == DXHPSTORAGESECTOR_ONE)
    {
        if (pHandle->hFlash == NULL ||
            pHandle->hMutex == NULL ||
            pData == NULL ||
            nDataLen <= 0)
        {
            return DXHPSTORAGE_ERROR;
        }
    }
    else
    {
        return DXHPSTORAGE_ERROR;
    }

    if (((nDataLen + StartPosition) > DX_HP_STORAGE_SIZE) ||
        (StartPosition < 0))
    {
        return DXHPSTORAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    uint32_t addr = DX_HP_STORAGE_ADDR1 + StartPosition;

    ret = dxFlash_Write(pHandle->hFlash,
                        addr,
                        nDataLen,
                        pData);

    if (ret != DXOS_SUCCESS)
    {
        dxMutexGive(pHandle->hMutex);
        return DXOS_ERROR;
    }

    dxHPSTORAGE_RET_CODE r = DXHPSTORAGE_SUCCESS;

    dxMutexGive(pHandle->hMutex);

    return r;
}

dxHPSTORAGE_RET_CODE dxHpStorage_Read(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (handle && pHandle->nHPStorageSectorID == DXHPSTORAGESECTOR_ONE)
    {
        if (pHandle->hFlash == NULL ||
            pHandle->hMutex == NULL ||
            pData == NULL ||
            nDataLen <= 0)
        {
            return DXHPSTORAGE_ERROR;
        }
    }
    else
    {
        return DXHPSTORAGE_ERROR;
    }

    if (((nDataLen + StartPosition) > DX_HP_STORAGE_SIZE) ||
        (StartPosition < 0))
    {
        return DXHPSTORAGE_SIZE_LIMIT_REACHED;
    }

    uint32_t addr = DX_HP_STORAGE_ADDR1 + StartPosition;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxOS_RET_CODE r = dxFlash_Read(pHandle->hFlash,
                                   addr,
                                   nDataLen,
                                   pData);

    dxMutexGive(pHandle->hMutex);

    if (r != DXOS_SUCCESS)
    {
        return DXHPSTORAGE_ERROR;
    }

    return DXHPSTORAGE_SUCCESS;
}

dxHPSTORAGE_RET_CODE dxHpStorage_EraseNumOfSector(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint16_t NumOfSector)
{
    int i = 0;
    dxOS_RET_CODE ret = DXOS_ERROR;

    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        StartPosition < 0)
    {
        return DXHPSTORAGE_ERROR;
    }

    StartPosition += DX_HP_STORAGE_ADDR1;

    G_SYS_DBG_LOG_V("Erase StartPosition 0x%X, sectors: %d (%d bytes)\n",
                    StartPosition,
                    NumOfSector,
                    (NumOfSector * DEFAULT_FLASH_SECTOR_ERASE_SIZE));

    for (i = 0; i < NumOfSector; i++)
    {
        ret = dxFlash_Erase_Sector(pHandle->hFlash, StartPosition + (i * DEFAULT_FLASH_SECTOR_ERASE_SIZE));

        if (ret != DXOS_SUCCESS)
        {
            return DXHPSTORAGE_ERROR;
        }
    }

    return DXHPSTORAGE_SUCCESS;
}


dxHPSTORAGE_RET_CODE dxHpStorage_Uninit(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->hFlash == NULL)
    {
        return DXHPSTORAGE_ERROR;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxFlash_Uninit(pHandle->hFlash);
    pHandle->hFlash = NULL;

    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXSUBIMAGE_SUCCESS;
}

uint32_t dxHpStorage_ReadTotalSize(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return 0; //DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nTotalSize <= 0)
    {
        return 0; //DXHPSTORAGE_ERROR;
    }

    return pHandle->nTotalSize;
}

uint32_t dxHpStorage_ReadSectorSize(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return 0; //DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nSectorSize<= 0 ||
        pHandle->nHPStorageSectorID != DXHPSTORAGESECTOR_ONE)
    {
        return 0; //DXHPSTORAGE_ERROR;
    }

    return pHandle->nSectorSize;
}
#endif //DX_HP_STORAGE_SIZE != 0

//============================================================================
// System Information
//============================================================================
#define DX_APPDATA_MARK                 0xF1E2D3C4B5A69788
#define DX_APPDATA_VER                  0x01

#define DX_APPDATA_BLOCK_IS_VALID       1
#define DX_APPDATA_BLOCK_IS_INVALID     0

#define DX_APPDATA_OFFSET_INVALID       0x0FFFFFFFF
#define DX_APPDATA_MAX_APPDATA_SIZE     (DX_SYSINFO_FLASH_SIZE - sizeof(dxAppDataHeaderInfo_t) - sizeof(dxAppInfo_t))

typedef struct
{
    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
    uint8_t         nPage;                  // 0 => DX_DEVINFO_FLASH_ADDR1 or
                                            //      DX_SYSINFO_FLASH_ADDR1 (check dxMemConfig.h)
                                            // 1 => DX_DEVINFO_FLASH_ADDR2 or
                                            //      DX_SYSINFO_FLASH_ADDR2 (check dxMemConfig.h)
    uint8_t         nBlockID;               // 0 => dxBLOCKID_t is DXBLOCKID_DEVINFO
                                            // 1 => dxBLOCKID_t is DXBLOCKID_SYSINFO
    uint8_t         Reserved[2];            // Must be 0x0000
    uint32_t        nOffset;                // always point to the last data of structure dxAppInfo_t
                                            // 0xFFFFFFFF means there is no data behind the header
                                            // 0          means there is only one record behind the header
                                            // 24         means the last data is start from
                                            //            DX_xxxINFO_FLASH_ADDRn + sizeof(dxAppDataHeaderInfo_t) + 24
} dxAppDataAccessCtrl_t;

typedef struct
{
    uint64_t        Tag;                    // Must be DX_APPDATA_MARK (check dxBSP.c)
    uint8_t         Version;                // Must not 0x00
    uint8_t         SeqNo;                  // Must be 0x01 ~ 0xFE. 0xFF: Just erased.
    uint8_t         Reserved[4];            // Reserved. Must be 0x00000000
    uint16_t        Size;                   // sizeof(dxAppDataHeaderInfo_t). Size will be padded to 4 bytes alignment.
} dxAppDataHeaderInfo_t;                    // sizeof(dxAppDataHeaderInfo_t) should be 16 bytes

/**
 * Example usage:
   <pre>
typedef struct
{
    uint32_t        Column1;
    uint16_t        Column2;
    uint8_t         Column3;
    uint8_t         Column4;
} MyUserData_t;

uint16_t len = sizeof(dxAppInfo_t) + sizeof(MyUserData_t);                                 // Real data size
uint16_t size4align = (len + sizeof(uint32_t) - 1) / sizeof(uint32_t)) * sizeof(uint32_t); // Align to uint32_t
dxAppInfo_t *pAppInfo = dxMemAlloc(NULL, 1, size4align);
pAppInfo->Size = size4align;
MyUserData_t *pMyUserData = (MyUserData_t *)&pAppInfo[offsetof(dxAppInfo_t, pData)];
pMyUserData->Column1 = 10;
pMyUserData->Column2 = 20;
pMyUserData->Column3 = 30;
pMyUserData->Column4 = 40;
   </pre>
 */

 /**
 * Example structure dxAppInfo_t:
   <pre>
typedef struct
{
    uint16_t        Size;
    uint16_t        CRC16;

    // Example user data. Declare your data here
    uint32_t        MyLocalVar;     // Should be put BEFORE pData[0]
    uint8_t         pData[0];
} dxAppInfo_t;
   </pre>
 *
 *        00   01   02   03
 *      +----+----+----+----+
 *   00 |   Size  |  CRC16  |
 *      +----+----+----+----+
 *   04 |     MyLocalVar    |
 *      +----+----+----+----+
 *   08 |                   |
 *      +----+----+----+----+
 *        ^
 *        |
 *        +-------- pData[0]
 */
typedef struct
{
    uint16_t        Size;                   // Size including sizeof(dxAppInfo_t) + all data behind pData[0]
    uint16_t        CRC16;                  // Using CRC16 with polynomial 0x8005 to calculate data from pData[0]

    // Example user data. Declare your data here.
    uint8_t         pData[0];               // NOTE: If you have dynamic length data, you can put your data to pData[0].
                                            //       pData[0] should be the last element of structure dxAppInfo_t. Or
                                            //       a compiler error will occur.
                                            //
                                            //       Use offsetof(dxAppInfo_t, pData) to get the address of pData[0].
                                            //       The value of offsetof(dxAppInfo_t, pData) should be 4
} dxAppInfo_t;                              // sizeof(dxAppInfo_t) should be 4 bytes

static dxAppDataAccessCtrl_t *dxAppDataAccessCtrl[ 2 ] = { NULL, NULL, };

static dxOS_RET_CODE dxAppData_WriteHeader(dxFlashHandle_t hFlash, uint32_t addr, uint8_t seqno)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    dxAppDataHeaderInfo_t dxHeader;

    if (hFlash == NULL) {

        return DXOS_ERROR;
    }

    memset(&dxHeader, 0x00, sizeof(dxHeader));

    dxHeader.Tag = DX_APPDATA_MARK;
    dxHeader.Version = DX_APPDATA_VER;
    dxHeader.SeqNo = seqno;
    dxHeader.Size = ((sizeof(dxHeader) + sizeof(uint32_t) - 1) / sizeof(uint32_t)) * sizeof(uint32_t);

    ret = dxFlash_Write(hFlash, addr, sizeof(dxHeader), &dxHeader);

    return ret;
}

static uint32_t dxAppData_GetNextAvailableAddress(dxFlashHandle_t hFlash, uint8_t *pNewSeqNo, uint8_t *pBlockIsValid, uint8_t blockId)
{
    uint32_t addr[2];
    if (blockId == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    else
    {
        return 0;
    }

    dxAppDataHeaderInfo_t dxHeader[sizeof(addr) / sizeof(addr[0])];
    uint8_t Reserved[4] = { 0x00, 0x00, 0x00, 0x00 };
    int SeqNo[sizeof(addr) / sizeof(addr[0])];
    uint8_t BlockIsValid[sizeof(addr) / sizeof(addr[0])];
    dxOS_RET_CODE ret = DXOS_ERROR;
    int i = 0;

    if (hFlash == NULL ||
        pNewSeqNo == NULL) {

        return 0;
    }

    for (i = 0; i < sizeof(addr) / sizeof(addr[0]); i++) {

         ret = dxFlash_Read(hFlash, addr[i], sizeof(dxAppDataHeaderInfo_t), &dxHeader[i]);

         if (ret != DXOS_SUCCESS)
         {
            break;
         }

        if (dxHeader[i].Tag == DX_APPDATA_MARK &&
            dxHeader[i].Version == DX_APPDATA_VER &&
            memcmp(dxHeader[i].Reserved, Reserved, sizeof(dxHeader[i].Reserved)) == 0)
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_VALID;
            SeqNo[i] = dxHeader[i].SeqNo;
        }
        else
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_INVALID;
            SeqNo[i] = 0;
        }
    }

    if (ret != DXOS_SUCCESS)
    {
        return 0;
    }

    if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
        BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = 0x01;

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[0];
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = SeqNo[1] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[0];
    }
    else if (BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = SeqNo[0] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[1];
    }

    // Both Blocks are valid
    if (SeqNo[0] == 0xFF &&
        SeqNo[1] == 0xFF)
    {
        *pNewSeqNo = 0x01;

        return addr[0];
    }

    if (SeqNo[0] > SeqNo[1] &&
        SeqNo[0] == SeqNo[1] + 1)
    {
        // Change to BLOCK2
        *pNewSeqNo = SeqNo[0] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }
        return addr[1];

    }
    else if (SeqNo[1] > SeqNo[0] &&
             SeqNo[1] == SeqNo[0] + 1)
    {
        // Change to BLOCK1
        *pNewSeqNo = SeqNo[1] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        return addr[0];
    }
    else if ((SeqNo[0] == 0xFF) ||
             (SeqNo[0] == 0xFE && SeqNo[1] == 0x01))
    {
        // Change to BLOCK1
        *pNewSeqNo = SeqNo[1] + 1;

        return addr[0];
    }
    else if ((SeqNo[1] == 0xFF) ||
             (SeqNo[1] == 0xFE && SeqNo[0] == 0x01))
    {
        // Change to BLOCK2
        *pNewSeqNo = SeqNo[0] + 1;

        return addr[1];
    }

    return 0;
}

static uint32_t dxAppData_GetActiveAddress(dxFlashHandle_t hFlash, uint8_t blockId)
{
    uint32_t addr[2];
    if (blockId == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    else
    {
        return 0;
    }

    dxAppDataHeaderInfo_t dxHeader[sizeof(addr) / sizeof(addr[0])];
    uint8_t Reserved[4] = { 0x00, 0x00, 0x00, 0x00 };
    int SeqNo[sizeof(addr) / sizeof(addr[0])];
    uint8_t BlockIsValid[sizeof(addr) / sizeof(addr[0])];
    dxOS_RET_CODE ret = DXOS_ERROR;
    int i = 0;

    if (hFlash == 0)
    {
        return 0;
    }

    for (i = 0; i < sizeof(addr) / sizeof(addr[0]); i++)
    {
        ret = dxFlash_Read(hFlash, addr[i], sizeof(dxAppDataHeaderInfo_t), &dxHeader[i]);

        if (ret != DXOS_SUCCESS)
        {
            break;
        }

        if (dxHeader[i].Tag == DX_APPDATA_MARK &&
            dxHeader[i].Version == DX_APPDATA_VER &&
            memcmp(dxHeader[i].Reserved, Reserved, sizeof(dxHeader[i].Reserved)) == 0)
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_VALID;
            SeqNo[i] = dxHeader[i].SeqNo;
        }
        else
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_INVALID;
            SeqNo[i] = 0;
        }
    }

    if (ret != DXOS_SUCCESS)
    {
        return 0;
    }

    if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
        BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        return 0;
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_VALID &&
             BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        return addr[0];
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
             BlockIsValid[1] == DX_APPDATA_BLOCK_IS_VALID)
    {
        return addr[1];
    }

    // Both two Block are empty
    if (SeqNo[0] == 0xFF &&
        SeqNo[1] == 0xFF)
    {
        return 0;
    }

    if (SeqNo[0] > SeqNo[1] &&
        SeqNo[0] == SeqNo[1] + 1)
    {
        return addr[0];
    }
    else if (SeqNo[1] > SeqNo[0] &&
             SeqNo[1] == SeqNo[0] + 1)
    {
        return addr[1];
    }
    else if ((SeqNo[0] == 0xFF) ||
             (SeqNo[0] == 0xFE && SeqNo[1] == 0x01))
    {
        return addr[1];
    }
    else if ((SeqNo[1] == 0xFF) ||
             (SeqNo[1] == 0xFE && SeqNo[0] == 0x01))
    {
        return addr[0];
    }

    return 0;
}

static dxAPPDATA_RET_CODE dxAppData_ReadToLastData(dxAppDataAccessCtrl_t *handle)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    if (handle == NULL ||
        handle->hFlash == NULL ||
        handle->hMutex == NULL ||
        handle->nPage  >= 2 ||
        handle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    uint32_t addr[2];
    if (handle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (handle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    uint32_t address = addr[handle->nPage];

    dxAppInfo_t dxAppData;

    uint32_t i = 0;
    uint32_t prev = DX_APPDATA_OFFSET_INVALID;
    uint32_t nHeaderSize = sizeof(dxAppDataHeaderInfo_t);
    uint16_t crc16 = 0;

    while (nHeaderSize + i < DX_SYSINFO_FLASH_SIZE) {

        memset(&dxAppData, 0x00, sizeof(dxAppData));

        ret = dxFlash_Read(handle->hFlash,
                           address + nHeaderSize + i,
                           sizeof(dxAppData),
                           &dxAppData);

        if (ret != DXOS_SUCCESS) {
            break;
        }

        if (dxAppData.Size == 0x0FFFF) {
            break;
        }

        if (dxAppData.Size > DX_APPDATA_MAX_APPDATA_SIZE) {

            // Data overwrite
            ret = DXAPPDATA_SIZE_LIMIT_REACHED;
            break;
        }

        prev = i;

        i += sizeof(dxAppData);

        uint8_t *pData = dxMemAlloc(NULL, 1, dxAppData.Size);

        if (pData == NULL) {

            ret = DXAPPDATA_ERROR;
            break;
        }

        if (dxAppData.Size > 0) {

            dxOS_RET_CODE r = dxFlash_Read(handle->hFlash,
                                           address + nHeaderSize + i,
                                           dxAppData.Size,
                                           pData);
            if (r != DXOS_SUCCESS) {

                dxMemFree(pData);
                pData = NULL;

                ret = DXAPPDATA_ERROR;
                break;
            }

            crc16 = Crc16(pData, dxAppData.Size, 0);

            if (crc16 != dxAppData.CRC16) {

                // CRC16 mismatch
                dxMemFree(pData);
                pData = NULL;

                ret = DXAPPDATA_ERROR;
                break;
            }
        }

        dxMemFree(pData);
        pData = NULL;

        i += dxAppData.Size;
    }

    if (ret != DXOS_SUCCESS) {

        return DXAPPDATA_ERROR;
    }

    if (nHeaderSize + i > DX_SYSINFO_FLASH_SIZE) {

        return DXAPPDATA_ERROR;
    }

    handle->nOffset =  prev;

    return DXAPPDATA_SUCCESS;
}

dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t blockId)
{
    dxAppDataAccessCtrl_t *pHandle = NULL;
    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr = 0;

    if (blockId == DXBLOCKID_DEVINFO && dxAppDataAccessCtrl[0])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }
    else if (blockId == DXBLOCKID_SYSINFO && dxAppDataAccessCtrl[1])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxAppDataAccessCtrl_t));

    if (pHandle == NULL) {
        return NULL;
    }

    pHandle->nPage = 0;
    memset(&pHandle->Reserved[0], 0x00, sizeof(pHandle->Reserved));

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL) {
        dxMemFree(pHandle);
        pHandle = NULL;

        return NULL;
    }

    pHandle->hFlash = dxFlash_Init();

    if (pHandle->hFlash == NULL) {
        dxMutexDelete(pHandle->hMutex);
        pHandle->hMutex = NULL;

        dxMemFree(pHandle);
        pHandle = NULL;

        return NULL;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    addr = dxAppData_GetActiveAddress(pHandle->hFlash, blockId);

    if (addr != DX_DEVINFO_FLASH_ADDR1 &&
        addr != DX_DEVINFO_FLASH_ADDR2 &&
        addr != DX_SYSINFO_FLASH_ADDR1 &&
        addr != DX_SYSINFO_FLASH_ADDR2)
    {

        // There is no valid data in these sectors.
        // Write data into sector 1
        if (blockId == DXBLOCKID_DEVINFO)
        {
            addr = DX_DEVINFO_FLASH_ADDR1;
        }
        else if (blockId == DXBLOCKID_SYSINFO)
        {
            addr = DX_SYSINFO_FLASH_ADDR1;
        }

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, addr, DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            dxFlash_Uninit(pHandle->hFlash);
            pHandle->hFlash = NULL;

            dxMutexGive(pHandle->hMutex);
            dxMutexDelete(pHandle->hMutex);
            pHandle->hMutex = NULL;

            dxMemFree(pHandle);
            pHandle = NULL;

            return NULL;
        }

        ret = dxAppData_WriteHeader(pHandle->hFlash, addr, 0x0FF);

        if (ret != DXOS_SUCCESS) {

            dxFlash_Uninit(pHandle->hFlash);
            pHandle->hFlash = NULL;

            dxMutexGive(pHandle->hMutex);
            dxMutexDelete(pHandle->hMutex);
            pHandle->hMutex = NULL;

            dxMemFree(pHandle);
            pHandle = NULL;

            return NULL;
        }
    }

    switch (addr) {
    case DX_DEVINFO_FLASH_ADDR1:
        pHandle->nPage      = 0;
        pHandle->nBlockID   = DXBLOCKID_DEVINFO;
        break;
    case DX_DEVINFO_FLASH_ADDR2:
        pHandle->nPage      = 1;
        pHandle->nBlockID   = DXBLOCKID_DEVINFO;
        break;
    case DX_SYSINFO_FLASH_ADDR1:
        pHandle->nPage      = 0;
        pHandle->nBlockID   = DXBLOCKID_SYSINFO;
        break;
    case DX_SYSINFO_FLASH_ADDR2:
        pHandle->nPage      = 1;
        pHandle->nBlockID   = DXBLOCKID_SYSINFO;
        break;
    default:
        pHandle->nPage      = 0xFF;
        pHandle->nBlockID   = 0xFF;
    }

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    dxAPPDATA_RET_CODE r = dxAppData_ReadToLastData(pHandle);

    dxMutexGive(pHandle->hMutex);

    if (r != DXAPPDATA_SUCCESS) {

        return NULL;
    }

    if (blockId == DXBLOCKID_DEVINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[0];
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[1];
    }

    return NULL;
}

dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2 ||
        pnSizeOfAPInfo == NULL ||
        pAPInf == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (*pnSizeOfAPInfo == 0) {
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {
        return DXAPPDATA_SUCCESS;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;

    dxAppInfo_t dxAppData;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    ret = dxFlash_Read(pHandle->hFlash,
                       address,
                       sizeof(dxAppData),
                       &dxAppData);

    if (ret != DXOS_SUCCESS) {
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    if (dxAppData.Size > DX_APPDATA_MAX_APPDATA_SIZE) {

        // Incorrect value of .Size
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    if (*pnSizeOfAPInfo < dxAppData.Size) {

        // Not enough space
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    dxOS_RET_CODE r = dxFlash_Read(pHandle->hFlash,
                                   address + sizeof(dxAppData),
                                   dxAppData.Size,
                                   pAPInf);

    dxMutexGive(pHandle->hMutex);

    if (r != DXOS_SUCCESS) {
        return DXAPPDATA_ERROR;
    }

    uint16_t crc16 = Crc16(pAPInf, dxAppData.Size, 0);

    if (crc16 != dxAppData.CRC16) {
        // CRC16 mismatch
        return DXAPPDATA_ERROR;
    }

    *pnSizeOfAPInfo = dxAppData.Size;

    return DXAPPDATA_SUCCESS;
}

uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {

        return 0;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;

    dxAppInfo_t dxAppData;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    ret = dxFlash_Read(pHandle->hFlash,
                       address,
                       sizeof(dxAppData),
                       &dxAppData);

    dxMutexGive(pHandle->hMutex);

    if (ret != DXOS_SUCCESS || dxAppData.Size == 0x0FFFF) {
        return 0;
    }

    return dxAppData.Size;
}

dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];
    uint32_t prevoffset = 0;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nBlockID >= 2 ||
        pHandle->nPage >= 2 ||
        nSizeOfAPInfo == 0 ||
        pAPInf == NULL) {
dbg_warm( "DXAPPDATA_INVALID_HANDLE" );
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (nSizeOfAPInfo == 0 ||
        nSizeOfAPInfo > DX_APPDATA_MAX_APPDATA_SIZE) {

dbg_warm( "DXAPPDATA_SIZE_LIMIT_REACHED" );
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t);

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {

        // Both sectors are empty.
        prevoffset = 0;

        ret = dxAppData_WriteHeader(pHandle->hFlash, addr[pHandle->nPage], 0x01);

        if (ret != DXOS_SUCCESS) {
            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }
    } else {
        address += pHandle->nOffset;
        prevoffset += pHandle->nOffset;

        // handle->nOffset point to the last record, NOT an empty space
        dxAppInfo_t dxAppData;

        ret = dxFlash_Read(pHandle->hFlash,
                           address,
                           sizeof(dxAppData),
                           &dxAppData);

        if (ret != DXOS_SUCCESS) {
            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        if (dxAppData.Size <= DX_APPDATA_MAX_APPDATA_SIZE) {
            uint8_t *pData = dxMemAlloc(NULL, 1, dxAppData.Size);

            if (pData == NULL) {
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            ret = dxFlash_Read(pHandle->hFlash,
                               address + sizeof(dxAppData),
                               dxAppData.Size,
                               pData);

            if (ret != DXOS_SUCCESS) {

                dxMemFree(pData);
                pData = NULL;
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            uint16_t crc16 = Crc16(pData, dxAppData.Size, 0);

            if (crc16 != dxAppData.CRC16) {

                // CRC16 mismatch
                dxMemFree(pData);
                pData = NULL;
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            dxMemFree(pData);
            pData = NULL;

            address += sizeof(dxAppData) + dxAppData.Size;
            prevoffset += sizeof(dxAppData) + dxAppData.Size;
        }
    }

    // Check available space
    if ((address + sizeof(dxAppInfo_t) + nSizeOfAPInfo) >= (addr[pHandle->nPage] + DX_SYSINFO_FLASH_SIZE)) {
        // There is no enough space to write in current sector. Write to next sector
        uint8_t newseqno = 0;
        uint8_t blockvalid = 0;

        address = dxAppData_GetNextAvailableAddress(pHandle->hFlash, &newseqno, &blockvalid, pHandle->nBlockID);

        if (address != DX_DEVINFO_FLASH_ADDR1 &&
            address != DX_DEVINFO_FLASH_ADDR2 &&
            address != DX_SYSINFO_FLASH_ADDR1 &&
            address != DX_SYSINFO_FLASH_ADDR2) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, address, DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        ret = dxAppData_WriteHeader(pHandle->hFlash, address, newseqno);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);

            return DXAPPDATA_ERROR;
        }

        switch (address) {
        case DX_DEVINFO_FLASH_ADDR1:
            pHandle->nPage = 0;
            break;
        case DX_DEVINFO_FLASH_ADDR2:
            pHandle->nPage = 1;
            break;
        case DX_SYSINFO_FLASH_ADDR1:
            pHandle->nPage = 0;
            break;
        case DX_SYSINFO_FLASH_ADDR2:
            pHandle->nPage = 1;
            break;
        }

        pHandle->nOffset = 0;
        prevoffset = 0;

        address += sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;
    }

    // Append data
    uint32_t len = sizeof(dxAppInfo_t) + nSizeOfAPInfo;
    uint8_t *p = dxMemAlloc(NULL, 1, len);

    if (p == NULL) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    dxAppInfo_t *pAppInfo = (dxAppInfo_t *)p;

    pAppInfo->Size = nSizeOfAPInfo;
    pAppInfo->CRC16 = Crc16(pAPInf, nSizeOfAPInfo, 0);
    memcpy(&p[offsetof(dxAppInfo_t, pData)], pAPInf, nSizeOfAPInfo);

    ret = dxFlash_Write(pHandle->hFlash,
                        address,
                        len,
                        p);

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        dxMemFree(p);
        p = NULL;

        return DXAPPDATA_ERROR;
    }

    pHandle->nOffset = prevoffset;

    dxMutexGive(pHandle->hMutex);
    dxMemFree(p);
    p = NULL;

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

	int i;
    for ( i = 0; i < sizeof(addr) / sizeof(addr[0]); i++) {

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, addr[i], DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            break;
        }
    }

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    pHandle->nPage = 0;

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        ret = dxAppData_WriteHeader(dxAppDataAccessCtrl[pHandle->nBlockID]->hFlash, addr[pHandle->nPage], 0x0FF);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        ret = dxAppData_WriteHeader(dxAppDataAccessCtrl[pHandle->nBlockID]->hFlash, addr[pHandle->nPage], 0x0FF);
    }

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    dxMutexGive(pHandle->hMutex);

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxAppDataAccessCtrl[pHandle->nBlockID] = NULL;

    dxFlash_Uninit(pHandle->hFlash);
    pHandle->hFlash = NULL;

    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXAPPDATA_SUCCESS;
}

#if defined(configUSE_LOCAL_SYSINFO_DEBUG) && configUSE_LOCAL_SYSINFO_DEBUG == 1
dxFlashHandle_t dxAppData_GetFlash(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->hFlash;
    }

    return NULL;
}

dxBLOCKID_t dxAppData_GetBlockID(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nBlockID;
    }

    return DXAPPDATA_ERROR;
}

uint8_t dxAppData_GetPage(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nPage;
    }

    return DXAPPDATA_ERROR;
}

uint32_t dxAppData_GetOffset(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nOffset;
    }

    return DXAPPDATA_ERROR;
}

#endif //configUSE_LOCAL_SYSINFO_DEBUG
