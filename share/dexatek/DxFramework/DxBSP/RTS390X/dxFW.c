//============================================================================
// File: dxFW.c
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include "dxSysDebugger.h"

#include "dxOS.h"
#include "dxBSP.h"
#include "dxMD5.h"
#include "dxFW.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# define DX_FW_STORE_MTD_ERASE_SLEEP_MSEC    100

static int
dxFWStoreMTD_GetMTDID( dxFWStorage_t *dxFWStorage )
{
	int mtd_id = 0;

    if ( sscanf( dxFWStorage->Location, "/dev/mtd%d", &mtd_id ) != 1 ) {
		return -1;
	} else {
		return mtd_id;
	}
}

static int
dxFWStoreMTD_Init( dxFWStorage_t *dxFWStorage )
{
	int mtd_id = dxFWStoreMTD_GetMTDID( dxFWStorage );

	if ( ( mtd_id < 3 ) || ( mtd_id > 4 ) ) {
        return -1;
    } else {
        struct rts_hconf_entry * hconf_dxboot = dxMemAlloc( NULL, 1, sizeof ( struct rts_hconf_entry ) + sizeof ( uint8_t ) );

        hconf_dxboot->id	= DX_BOOT_ID;
        hconf_dxboot->type	= 0;
        hconf_dxboot->len	= 1;

        rts_hconf_read_entry( hconf_dxboot );

        printf( "dxboot=%c\n", hconf_dxboot->data[ 0 ] );
        printf( "location=%s\n", dxFWStorage->Location );

        /* Check hconf dxboot value, if value="3", mtd should add 2, 3->5 , 4->6*/
        if ( hconf_dxboot->data[ 0 ] == 0x33 ) {
            mtd_id += 2;
            sprintf( dxFWStorage->Location, "/dev/mtd%d", mtd_id );
            printf( "re-location=%s\n", dxFWStorage->Location );
        }

        dxMemFree( hconf_dxboot );

        return 0;
    }
}

static int
dxFWStoreMTD_Erase( dxFWStorage_t *dxFWStorage )
{
    int fd = open( dxFWStorage->Location, O_RDWR );

	if ( fd > 0 ) {
	    mtd_info_t		mtd_info;           // the MTD structure
	    erase_info_t	ei;               // the erase block structure

        ioctl( fd, MEMGETINFO, &mtd_info );   // get the device info

        // dump it for a sanity check, should match what's in /proc/mtd
        printf( "mtd dev=%s\n", dxFWStorage->Location );
        printf( "MTD Type: 0x%08x\nMTD total size: 0x%08x bytes\nMTD erase size: 0x%08x bytes\n",
				mtd_info.type, mtd_info.size, mtd_info.erasesize );

        ei.length = mtd_info.erasesize;   //set the erase block size

        for ( ei.start = 0; ei.start < mtd_info.size; ei.start += ei.length ) {
            ioctl( fd, MEMUNLOCK, &ei );
            ioctl( fd, MEMERASE, &ei );
            dxTimeDelayMS( DX_FW_STORE_MTD_ERASE_SLEEP_MSEC );
        }

        close( fd );

        return 0;
	}

	return -1;
}

static int
dxFWStoreMTD_Write( dxFWStorage_t *dxFWStorage )
{
    printf( "[dxFWStoreMTD_Write] Write Image to %s\n", dxFWStorage->Location );

    int fd = open( dxFWStorage->Location, O_RDWR );

	if ( fd > 0 ) {
		int ret;

        lseek( fd, 0, SEEK_SET );        // go back to first block's start
        ret = write( fd, dxFWStorage->Data, dxFWStorage->Size );
        close( fd );

        if ( ret == dxFWStorage->Size ) {
        	printf( "[dxFWStoreMTD_Write] Write Image End.\n" );
    		return 0;
        } else {
     	   return -1;
		}
	}

    return -1;
}

static int
dxFWStoreMTD_MD5( dxFWStorage_t *dxFWStorage )
{
    printf( "[dxFWStoreMTD_MD5] Check MD5\n" );

    int fd = open( dxFWStorage->Location, O_RDONLY );

	if ( fd > 0 ) {
		int ret;
        uint8_t *buffer = dxMemAlloc( NULL, 1, dxFWStorage->Size );

		if ( buffer == NULL ) {
        	close( fd );
			return -1;
		}

        lseek( fd, 0, SEEK_SET );        // go back to first block's start
        ret = read( fd, buffer, dxFWStorage->Size );
        close( fd );

        if ( ret == dxFWStorage->Size ) {
            unsigned char md5[ 16 ];
            unsigned char md5str[ 32 + 1 ];

            dxMD5( buffer, dxFWStorage->Size, md5 );
            dxMD5_convert_hex( md5, md5str );

            printf( "File_MD5=%s Size=%zu\n", md5str, dxFWStorage->Size );

            if ( memcmp ( dxFWStorage->MD5, md5, 16 ) == 0 ) {
                ret = 0;
			} else {
                ret = -1;
			}
		} else {
            ret = -1;
        }

        dxMemFree( buffer );

		return ret;
	}

	return -1;
}

int
dxFWParserWrite( uint8_t * dxFWBuf )
{
    unsigned char md5str[ 32 + 1 ];
    unsigned char md5[ 16 ];

    int p;

    dxFW_t *dxFW = ( dxFW_t * )dxFWBuf;

    dxFWPackage_t *dxFWPkg		= NULL ;
    dxFWStorage_t *dxFWStorData = NULL;

    uint32_t dxFWOffset = sizeof( dxFW_t );

    printf( "FW: Tag=%llx, Size=%zu, Count=%d\n", dxFW->Tag, dxFW->nTotoalSize, dxFW->nPackageCount );

    for ( p = 0; p < dxFW->nPackageCount; p ++ ) {
        dxFWPkg = ( dxFWPackage_t * )( dxFWBuf + dxFWOffset );
        printf( "PKG: No=%d, Type=%d\n", dxFWPkg->No, dxFWPkg->Type );

        dxFWOffset += sizeof(dxFWPackage_t) - sizeof( dxFWStorage_t * );

        dxFWStorData = ( dxFWStorage_t * )( dxFWBuf + dxFWOffset );
        printf( "STORE: Type=%d, Location=%s, Size=%zu\n", dxFWStorData->Type, dxFWStorData->Location, dxFWStorData->Size );

        dxMD5_convert_hex( dxFWStorData->MD5, md5str );
        printf( "STORE: MD5=%s\n", md5str );

        dxMD5( dxFWStorData->Data, dxFWStorData->Size, md5 );
        dxMD5_convert_hex( md5, md5str );
        printf( "DATA: MD5=%s\n", md5str );

		if ( memcmp ( dxFWStorData->MD5, md5, 16 ) ) {
			dbg_warm( "memcmp ( dxFWStorData->MD5, md5, 16 ) Error" );
			return -1;
		} else if ( dxFWStorData->Type != DXSTORAGETYPE_DXMTD ) {
			dbg_warm( "dxFWStor->Type( %d ) != DXSTORAGETYPE_DXMTD( %d )", dxFWStorData->Type, DXSTORAGETYPE_DXMTD );
			return -1;
		} else {
            if ( !( ( dxFWStoreMTD_Init( dxFWStorData ) == 0 ) &&
	                ( dxFWStoreMTD_Erase( dxFWStorData ) == 0 ) &&
	                ( dxFWStoreMTD_Write( dxFWStorData ) == 0 ) &&
	                ( dxFWStoreMTD_MD5( dxFWStorData ) == 0 ) ) ) {
				dbg_warm( "dxFWStorData->Type( %d ) != DXSTORAGETYPE_DXMTD( %d )", dxFWStorData->Type, DXSTORAGETYPE_DXMTD );
				return -1;
			}
		}

        dxFWOffset += sizeof( dxFWStorage_t ) + dxFWStorData->Size;
    }

    return 0;
}

/*
 *  For rtk IPCAM only
 *  Switch boot form mtd3 or mtd5
 *  in uboot & hconf is dxboot="3" or dxboot="5"
 */
dxBOOL
dxFWIpcamBootSwitch(void)
{
    dxBOOL ret = dxFALSE;

    struct rts_hconf_entry * hconf_dxboot = dxMemAlloc( NULL, 1, sizeof ( struct rts_hconf_entry ) + sizeof ( uint8_t ) );

    hconf_dxboot->id	= DX_BOOT_ID;
    hconf_dxboot->type	= 0;
    hconf_dxboot->len	= 1;

    if ( rts_hconf_read_entry( hconf_dxboot ) ) {
        ret = dxFALSE;
	} else {
        printf( "Current dxboot=%c\n", hconf_dxboot->data[ 0 ] );

        if ( hconf_dxboot->data[ 0 ] == 0x33 ) {
            hconf_dxboot->data[ 0 ]	= 0x35;
        } else {
            hconf_dxboot->data[ 0 ]	= 0x33; //Default is "3"
        }

        printf( "Switch to dxboot=%c\n", hconf_dxboot->data[ 0 ] );
        rts_hconf_write_entry( hconf_dxboot );

        if ( !rts_hconf_sync() ) {
           ret = dxTRUE;
		}
    }

	dxMemFree( hconf_dxboot );

    return ret;
}

