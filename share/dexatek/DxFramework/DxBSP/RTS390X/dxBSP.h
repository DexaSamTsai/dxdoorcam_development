//============================================================================
// File: dxBSP.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================

#ifndef _DXBSP_H
#define _DXBSP_H

#include "dxMemConfig.h"
#if defined(configUSE_LOCAL_GPIO_SETTING) && configUSE_LOCAL_GPIO_SETTING == 1
#include "dxGPIO.h"
#else // configUSE_LOCAL_GPIO_SETTING
#pragma message ("NOTE: Project specific GPIO setting disabled, will use default dxGPIOConfig.h!!!!")
#include "dxGPIOConfig.h"
#endif // configUSE_LOCAL_GPIO_SETTING

//============================================================================
// Hardware dependent Includes
//============================================================================
#include <mtd/mtd-user.h>
#include "rts_io_gpio.h"
#include "rts_hconf.h"

//============================================================================
// Defines and Macro
//============================================================================


//============================================================================
// UART Wrapper
//============================================================================
typedef struct dxUART {
    int                 Uartfd;

} dxUART_t;

typedef enum
{
    DXUART_PORT0 = 0,
    DXUART_PORT1 = 1,
    DXUART_PORT2 = 2
} dxUART_Port_t;

typedef enum
{
    DXUART_FLOWCONTROL_NONE,
    DXUART_FLOWCONTROL_RTSCTS
} dxUART_FlowControl_t;

typedef enum
{
    DXUART_PARITY_NONE = 0,
    DXUART_PARITY_ODD = 1,
    DXUART_PARITY_EVEN = 2,
} dxUART_Pairty_t;

typedef enum {

    DXUART_SUCCESS = 0,

    DXUART_ERROR   = -1,

    DXUART_TIMEOUT = -2,
    
    DXUART_PARAM_ERR = -3,

} dxUART_RET_CODE;


/**
 * \return dxUART_t         a handle to control UART.
 *         NULL             otherwise
 */
dxUART_t *dxUART_Open(dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option);

/**
 * \return DXUART_SUCCESS   if UART closed
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_Close(dxUART_t *pdxUart);

/**
 * \return DXUART_SUCCESS   if a character received
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_GetCh(dxUART_t *pdxUart, uint8_t *ch);

/**
 * \return DXUART_SUCCESS   if a character sent
 *         DXUART_ERROR     otherwise
 */
dxUART_RET_CODE dxUART_PutCh(dxUART_t *pdxUart, uint8_t ch);

/**
 * \return > 0              if number of bytes received
 *         DXUART_TIMEOUT   if timeout
 *         DXUART_ERROR     otherwise
 */
int dxUART_Receive_Blocked(dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms);

/**
 * \return > 0              if number of bytes sent
 *         DXUART_TIMEOUT   if timeout
 *         DXUART_ERROR     otherwise
 */
int dxUART_Send_Blocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len);


//============================================================================
// BSP
//============================================================================
typedef enum {

    DXBSP_SUCCESS = 0,

    DXBSP_ERROR   = -1,

} dxBSP_RET_CODE;

/**
 * \return DXBSP_SUCCESS    if BSP initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxBSP_Init(void);

//============================================================================
// GPIO
//============================================================================
#define DX_ENDOF_GPIO_CONFIG            0xFFFFFFFF

#define PORT_NUM(pin)                   (((uint32_t)(pin) >> 4) & 0xF)
#define PIN_NUM(pin)                    ((uint32_t)(pin) & 0xF)

typedef enum
{
    DXPIN_PULLNONE  = 0,
    DXPIN_PULLUP    = 1,
    DXPIN_PULLDOWN  = 2,
    DXPIN_OPENDRAIN = 3,
    DXPIN_PULLDEFAULT = DXPIN_PULLNONE,
    DXPIN_MODE_UNSUPPORTED = 255,
} dxPIN_Mode;

typedef enum
{
    DXPIN_INPUT  = 0,
    DXPIN_OUTPUT = 1
} dxPIN_Direction;

typedef enum
{
    DXPIN_LOW = 0,
    DXPIN_HIGH = 1
} dxPIN_Level;

typedef struct
{
    struct rts_gpio *   Object;
    uint32_t            Name;
    dxPIN_Direction     Direction;
    dxPIN_Mode          Mode;
} dxGPIO_t;

//============================================================================
// RTS 390X GPIO
//
// MUST modify to match your platform
//============================================================================

#define DK_GPIO_CONTROL_LEVEL_ON         DXPIN_HIGH
#define DK_GPIO_CONTROL_LEVEL_OFF        DXPIN_LOW

#define DK_LED_CONTROL_LEVEL_ON          DXPIN_LOW
#define DK_LED_CONTROL_LEVEL_OFF         DXPIN_HIGH

//README FIRST!!!
//Make sure the assigned GPIO for general purpose DOES NOT conflict
//with the dxKeypadConfig from dxKeypadManager.
#if 0 //SAMPLE
static dxGPIO_t dxGPIOConfig[] =
{
    {
        NULL,
        DK_BLE_CENTRAL_MODE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_BLE_RESET,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_BLUE,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_YELLOW,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },
    {
        NULL,
        DK_LED_RED,
        DXPIN_OUTPUT,
        DXPIN_MODE_UNSUPPORTED
    },


    // DO NOT REMOVE - MUST BE PLACED IN LAST ITEM
    {
        NULL,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG,
        DX_ENDOF_GPIO_CONFIG
    }
};
#endif

//============================================================================
// GPIO Access Function
//============================================================================
/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Init(dxGPIO_t* ConfigTable);

/**
 * \return DXPIN_LOW        if GPIO level is low
 *         DXPIN_HIGH       if GPIO level is high
 *         DXBSP_ERROR      otherwise
 */
int dxGPIO_Read(uint32_t PinName);

/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Write(uint32_t PinName, dxPIN_Level level);

/**
 * \return DXBSP_SUCCESS    if GPIO change direction success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Set_Direction(uint32_t PinName, dxPIN_Direction Dir);

/**
 * \return dxPIN_Direction  if get GPIO direction success
 *         DXBSP_ERROR      otherwise
 */
int dxGPIO_Get_Direction(uint32_t PinName);

/**
 * \return DXBSP_SUCCESS    if GPIO initial success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxGPIO_Uninit(dxGPIO_t* ConfigTable);

//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void);

/**
 * \brief the function should be called once while system startup.
 */
void dxPLATFORM_Init(void);

// DeviceMac_t structure is almost the same as WifiCommDeviceMac_t in dk_Network.h
// and can be direct case to WifiCommDeviceMac_t.
// We define a new structure here is to avoid cross reference due to dxBSP.h and dxOS.h
// are lowest level of .h files.
typedef struct
{
    uint8_t MacAddr[8];

} DeviceMac_t;

/**
 * \brief the funciton will change system's MAC address. NEED to reboot to take effect.
 *
 * \return DXBSP_SUCCESS    if MAC address wrote success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac);


/**
 * \brief Initialize platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut);

/**
 * \brief Start platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void);

/**
 * \brief Refresh platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void);

/**
 * \brief Close platform watch dog timer
 *
 * \return DXBSP_SUCCESS    success.
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void);

//============================================================================
// FLASH API
//============================================================================
typedef void * dxFlashHandle_t;
typedef void * flash_t;

#define DX_SYSINFO_FLASH_SIZE		16384        // MUST align to a SECTOR of flash, typical 16KB
#define DX_DEVINFO_FLASH_SIZE		16384        // MUST align to a SECTOR of flash, typical 16KB
#define DX_DEVINFO_FLASH_ADDR1		0x3EF000//0x3FB000    // MUST be revised
#define DX_DEVINFO_FLASH_ADDR2		(DX_DEVINFO_FLASH_ADDR1 + DX_DEVINFO_FLASH_SIZE)
#define DX_SYSINFO_FLASH_ADDR1		(DX_DEVINFO_FLASH_ADDR2 + DX_DEVINFO_FLASH_SIZE)
#define DX_SYSINFO_FLASH_ADDR2		(DX_SYSINFO_FLASH_ADDR1 + DX_SYSINFO_FLASH_SIZE)

/**
 * \return dxFlashHandle_t  a handle to the flash access control.
 *         NULL             otherwise
 */
dxFlashHandle_t dxFlash_Init(void);

/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash);


/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash);


/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address);

/**
 * \return DXOS_SUCCESS if the sector was erased successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size);

/**
 * \return DXOS_SUCCESS if the data was read successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Read(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data);

/**
 * \return DXOS_SUCCESS if the data was read successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Read_Word(dxFlashHandle_t flash, uint32_t address, uint32_t *data);

/**
 * \return DXOS_SUCCESS if the data was written successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Write(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data);

/**
 * \return DXOS_SUCCESS if the data was written successfully
 *         DXOS_ERROR   otherwise
 */
dxOS_RET_CODE dxFlash_Write_Word(dxFlashHandle_t flash, uint32_t address, uint32_t data);

/**
 * \return DXOS_SUCCESS
 */
dxOS_RET_CODE dxFlash_Uninit(dxFlashHandle_t flash);


//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================

#define DX_BOOT_ID      0x5000002

#define DX_BOOT_MTD3                    3
#define DX_BOOT_MTD5                    5

typedef enum
{
    DX_SUBIMAGE_BLE = 0,
} dxSubImage_t;

typedef struct
{
    dxSubImage_t type;
    char file_name[64];
} dxFILEImage_t;

typedef enum
{
    DX_MAINIMAGE_IPCAM = 0,
} dxMainImage_t;

typedef struct
{
    dxMainImage_t type;
} dxDEVICEImage_t;

typedef enum {

    DXMAINIMAGE_COMPLETE              = 1,

    DXMAINIMAGE_SUCCESS               = 0,

    DXMAINIMAGE_ERROR                 = -1,

    DXMAINIMAGE_SECTION_LIMIT_REACHED = -2,

    DXMAINIMAGE_SIZE_LIMIT_REACHED    = -3,

    DXMAINIMAGE_VERIFY_ERROR          = -4,

} dxMAINIMAGE_RET_CODE;

/**
 * \return DXMAINIMAGE_SUCCESS if success
 *         DXMAINIMAGE_ERROR   otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Init(void);

/**
 * \brief The function will clear all data and reset flash access handle. Any calling dxMainImage_Write() that
 *        is next to dxMainImage_Start() will cause current data in flash to be overwrite, instead of appending.
 *        MD5 checksum value will be reset too.
 *
 * \param[in] pszMD5Sum Pointer to a NULL terminated string. It should be 32 bytes long. For example,
 *                      "6aea856851667a4238597607eb4c1f6e" (character " is not incuded)
 *
 * \return DXMAINIMAGE_SUCCESS            if success
 *         DXMAINIMAGE_SIZE_LIMIT_REACHED if nImageSize is too large.
 *                                        nImageSize > DX_SYSINFO_FLASH_ADDR1 - DX_SYSINFO_FLASH_UPDATE_IMG_ADDR
 *         DXMAINIMAGE_ERROR              otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Start(uint8_t *pszMD5Sum, uint32_t nImageSize);

/**
 * \return DXMAINIMAGE_SUCCESS            if flash write success.
 *         DXMAINIMAGE_COMPLETE           if flash write success and MD5 checksum value is correct.
 *                                        System is ready to reboot.
 *         DXMAINIMAGE_SIZE_LIMIT_REACHED if the free spaces for update image is not enough to store the data
 *         DXMAINIMAGE_ERROR              otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_Write(uint8_t *pImage, uint32_t nImageLen);

/**
 * \return DXMAINIMAGE_SUCCESS  if success.
 *         DXMAINIMAGE_ERROR    otherwise
 */
dxMAINIMAGE_RET_CODE dxMainImage_UpdateReboot(dxBOOL WithSystemSelfReboot);

/**
 * \Free ImageBuffer
 * When call dxMainImage_Start, will malloc ImageBuffer for download image buffer
 * If network disconnect, need call dxMainImage_Stop to free.
 */
dxMAINIMAGE_RET_CODE dxMainImage_Stop(void);

//============================================================================
// SUB IMAGE UPDATE API
//============================================================================

typedef enum {

    DXSUBIMAGE_COMPLETE                 = 1,

    DXSUBIMAGE_SUCCESS                  = 0,

    DXSUBIMAGE_ERROR                    = -1,

    DXSUBIMAGE_SECTION_LIMIT_REACHED    = -2,

    DXSUBIMAGE_SIZE_LIMIT_REACHED       = -3,

    DXSUBIMAGE_VERIFY_ERROR             = -4,

    DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR  = -5,

} dxSUBIMAGE_RET_CODE;

typedef enum
{
    DXSUBIMGBLOCK_ONE    = 0,
} dxSUBIMGBLOCKID_t;

/**
 * \brief This pointer definition refer dxSubImgInfoHandle_t (check dxBSP.c).
 */
typedef void * dxSubImgInfoHandle_t;

/**
 * \return dxSubImgInfoHandle_t                 if success
 *         NULL                                 otherwise
 */
dxSubImgInfoHandle_t dxSubImage_Init(dxSUBIMGBLOCKID_t block_id);

#if DX_SUBIMG_FLASH_SIZE
/**
 * \brief The function will clear all data and reset flash access handle. Any calling dxSubImage_Write() that
 *        is next to dxSubImage_Start() will cause current data in flash to be overwrite, instead of appending.
 *        MD5 checksum value will be reset too.
 *
 * \param[in] pszMD5Sum Pointer to a NULL terminated string. It should be 32 bytes long. For example,
 *                      "6aea856851667a4238597607eb4c1f6e" (character " is not incuded)
 *
 * \return DXSUBIMAGE_SUCCESS                   if success
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if nImageSize is too large.
 *                                              nImageSize > DX_SUBIMG_FLASH_ADDR1 - DX_SYSINFO_FLASH_UPDATE_IMG_ADDR
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Start(dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize);

/**
 * \return DXSUBIMAGE_SUCCESS                   if flash write success.
 *         DXSUBIMAGE_COMPLETE                  if flash write success and MD5 checksum value is correct.
 *                                              System is ready to reboot.
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if the free spaces for update image is not enough to store the data
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Write(dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen);

/**
 * \brief  Read subsystem image data from flash StartPosition to pBDataBuffer, assign image size and handle (for MD5 checksum) by caller.
 *
 * \return DXSUBIMAGE_SUCCESS                   if success.
 *         DXSUBIMAGE_SIZE_LIMIT_REACHED        if the free spaces for update image is not enough to store the data
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Read(dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead,
                                    uint8_t *pBDataBuffer, uint32_t *FwSizeOut);

/**
 * \return DXSUBIMAGE_SUCCESS                   if success
 *         DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR   if flash size select fail
 *         DXSUBIMAGE_ERROR                     otherwise
 */
dxSUBIMAGE_RET_CODE dxSubImage_Uninit(dxSubImgInfoHandle_t handle);

/**
 * \return > 0 (Full Sub Image size)    if success
 *         = 0                          otherwise
 */
uint32_t dxSubImage_ReadImageSize(dxSubImgInfoHandle_t handle);

#endif // DX_SUBIMG_FLASH_SIZE

//============================================================================
// Hyper Peripheral Storage Access API
//============================================================================
#ifndef DX_HP_STORAGE_SIZE
#define DX_HP_STORAGE_SIZE 0
#endif //DX_HP_STORAGE_SIZE

#ifndef DX_HP_STORAGE_ADDR1
#define DX_HP_STORAGE_ADDR1 0x00
#endif //DX_HP_STORAGE_ADDR1

//============================================================================
// System Information
//============================================================================

#define DX_PATH_DKSTORAGE     "/var/DkStorage"
#define DX_PATH_FRAMEWORKINFO "/var/DkStorage/FrameWorkInfo"
#define DX_PATH_FWUPDATEMAIN  "/var/DkStorage/FwUpdateMain"
#define DX_PATH_FWUPDATESUB   "/var/DkStorage/FwUpdateSub"

#define DX_CONF_SYS "DxSYSINFO"
#define DX_CONF_DEV "DxDEVINFO"
#define DX_FW_SUB_IMAGE "DxFWSUB"

typedef enum {
    DXAPPDATA_BAK_SUCCESS            = 1,

    DXAPPDATA_SUCCESS                = 0,

    DXAPPDATA_ERROR                  = -1,

    DXAPPDATA_INVALID_HANDLE         = -2,

    DXAPPDATA_SIZE_LIMIT_REACHED     = -3,

    DXAPPDATA_BAK_ERROR              = -4,

    DXAPPDATA_BAK_SIZE_LIMIT_REACHED = -5,

} dxAPPDATA_RET_CODE;

typedef enum
{
    DXBLOCKID_DEVINFO           = 0,

    DXBLOCKID_SYSINFO           = 1,

} dxBLOCKID_t;

/**
 * \brief This pointer definition refer dxAppDataAccessCtrl_t (check dxBSP.c).
 */
typedef void * dxAppDataHandle_t;

/**
 * \brief the function suggest called once while system startup to initialize DEVINFO or SYSINFO.
 *        0: DXBLOCKID_DEVINFO, 1: DXBLOCKID_SYSINFO.
 */
dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t block_id);

/**
 * \param[in,out] pnSizeOfAPInfo    Provide the size of the buffer to store data and return the number of bytes
 *                                  return to the buffer.
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_SIZE_LIMIT_REACHED if buffer size is not enough to store data
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo);

/**
 * \brief Read the last written data size. Before using dxAppData_Read(), the function should be call to obtain
 *        the last written data size and allocate enough memory for dxAppData_Read()
 *
 * \return > 0      if success
 *         == 0     otherwize
 */
uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle);

/**
 * \brief Maximum 4076 bytes data can be written due to limitation of structure dxAppInfo_t (uint16_t Size)
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_INVALID_HANDLE     if system information handle is incorrect
 *         DXAPPDATA_SIZE_LIMIT_REACHED if nSizeOfAPInfo is 0 or larger than 4076
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo);

/**
 * \brief The function will clear both DX_DEVINFO_FLASH_ADDR1 and DX_DEVINFO_FLASH_ADDR2
 *        or DX_SYSINFO_FLASH_ADDR1 and DX_SYSINFO_FLASH_ADDR2 sectors, determined by handle.
 *
 * \return DXAPPDATA_SUCCESS            if success
 *         DXAPPDATA_INVALID_HANDLE     if system information handle is incorrect
 *         DXAPPDATA_ERROR              otherwise
 */
dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle);

/**
 * \return DXAPPDATA_SUCCESS        if success
 *         DXAPPDATA_INVALID_HANDLE if system information handle is incorrect
 */
dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle);


#if defined(configUSE_LOCAL_SYSINFO_DEBUG) && configUSE_LOCAL_SYSINFO_DEBUG == 1
/**
 * \return dxFlashHandle_t          if success
 *         NULL                     if system information handle is incorrect
 */
dxFlashHandle_t dxAppData_GetFlash(dxAppDataHandle_t handle);

/**
 * \return dxBLOCKID_t              if success
 *         DXAPPDATA_ERROR          if system information handle is incorrect
 */
dxBLOCKID_t dxAppData_GetBlockID(dxAppDataHandle_t handle);

/**
 * \return nPage                    if success
 *         DXAPPDATA_ERROR          if system information handle is incorrect
 */
uint8_t dxAppData_GetPage(dxAppDataHandle_t handle);

/**
 * \return nOffset                  if success
 *         DXAPPDATA_ERROR          if system information handle is incorrect
 */
uint32_t dxAppData_GetOffset(dxAppDataHandle_t handle);
#endif //configUSE_LOCAL_SYSINFO_DEBUG



#endif // _DXBSP_H
