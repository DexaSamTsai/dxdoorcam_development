//============================================================================
// File: dxBSP.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#include <termios.h>
#include <sys/ioctl.h>
#include <sys/reboot.h>
#include <linux/watchdog.h>
#include <unistd.h>

#include "dxSysDebugger.h"

#include "dxOS.h"
#include "dxBSP.h"
#include "dxMD5.h"
#include "dxFW.h"

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

extern uint16_t Crc16(uint8_t *pdata, int16_t nbytes, uint16_t crc);

#define CRC16_ENABLE 1

//============================================================================
// UART Wrapper
//============================================================================

dxUART_t *dxUART_Open(dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option)
{
    dxUART_t    *pdxUart = dxMemAlloc(NULL, 1, sizeof(dxUART_t));

    if (pdxUart == NULL) {
        return NULL;
    }

    switch (port)
    {
        case DXUART_PORT0:
            pdxUart->Uartfd = open( "/dev/ttyS0", O_WRONLY);
            break;
        case DXUART_PORT1:
            pdxUart->Uartfd = open( "/dev/ttyS1", O_RDWR | O_NOCTTY |O_NONBLOCK);
            break;
        case DXUART_PORT2:
            pdxUart->Uartfd = open( "/dev/ttyS2", O_RDWR | O_NOCTTY |O_NONBLOCK);
            break;
    }

    if (pdxUart->Uartfd < 0) {
        perror("UART open Failed");
        dxMemFree(pdxUart);
        return NULL;
    }

    struct termios  config;

    if ( tcgetattr( pdxUart->Uartfd, &config ) != 0 ) {
        perror("dxUART_Open tcgetattr:");
        goto ErrorExitResourceFree;
    }

    speed_t BSpeed = B0;
    switch (baudrate)
    {
        case 4800:
            BSpeed = B4800;
            break;

        case 9600:
            BSpeed = B9600;
            break;

        case 19200:
            BSpeed = B19200;
            break;

        case 38400:
            BSpeed = B38400;
            break;

        case 57600:
            BSpeed = B57600;
            break;

        case 115200:
            BSpeed = B115200;
            break;

    }

    if (BSpeed == B0)
    {
        printf("dxUART_Open cfsetispeed: unsupported baudrate");
        goto ErrorExitResourceFree;
    }

    if (cfsetispeed(&config, BSpeed) != 0)
    {
        perror("dxUART_Open cfsetispeed:");
        goto ErrorExitResourceFree;
    }

    if (cfsetospeed(&config, BSpeed) != 0)
    {
        perror("dxUART_Open cfsetospeed:");
        goto ErrorExitResourceFree;
    }

    if (port != DXUART_PORT0)
    {
    //RAW MODE
    config.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    config.c_oflag &= ~OPOST;
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;
    }
    if (stop_bits == 1)
        config.c_cflag &= ~CSTOPB;
    else if (stop_bits == 2)
        config.c_cflag |= CSTOPB;

    switch (data_bits)
    {
        case 5:
            config.c_cflag &= ~(CSIZE);
            config.c_cflag |= CS5;
            break;

        case 6:
            config.c_cflag &= ~(CSIZE);
            config.c_cflag |= CS6;
            break;

        case 7:
            config.c_cflag &= ~(CSIZE);
            config.c_cflag |= CS7;
            break;

        default:
            config.c_cflag &= ~(CSIZE);
            config.c_cflag |= CS8;
            break;
    }

    if (parity == DXUART_PARITY_ODD) {
        config.c_cflag |= (PARENB | PARODD);
    }
    else if (parity == DXUART_PARITY_EVEN) {
        config.c_cflag |= PARENB;
        config.c_cflag &= ~PARODD;
    }

    if (flowctrl == DXUART_FLOWCONTROL_RTSCTS) {
        config.c_cflag |= CRTSCTS;
    }
    else
    {
        config.c_cflag &= ~CRTSCTS;
    }

    if ( tcsetattr( pdxUart->Uartfd, TCSANOW, &config ) != 0 )
    {
        perror("dxUART_Open tcsetattr:");
        goto ErrorExitResourceFree;
    }

    return pdxUart;

ErrorExitResourceFree:

    perror("dxUART_Open Error:\n");
    close( pdxUart->Uartfd );
    dxMemFree(pdxUart);
    return NULL;
}


dxUART_RET_CODE dxUART_Close(dxUART_t *pdxUart)
{
    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    if (close(pdxUart->Uartfd) != 0)
    {
        perror("Failed closing UART");
        return DXUART_ERROR;
    }

    dxMemFree(pdxUart);

    return DXUART_SUCCESS;
}


dxUART_RET_CODE dxUART_GetCh(dxUART_t *pdxUart, uint8_t *ch)
{
    dxUART_RET_CODE RetCode = DXUART_ERROR;

    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    uint16_t TryCount = 2;

    while (TryCount)
    {
        TryCount--;
        ssize_t SizeRead = read(pdxUart->Uartfd, (void*)ch, 1);
        if (SizeRead < 0)
        {
            perror("dxUART_GetCh Error");
            return DXUART_ERROR;
        }
        else if (SizeRead == 0)
        {
            if (TryCount)
            {
                fd_set set;
                struct timeval timeout;
                FD_ZERO(&set); /* clear the set */
                FD_SET(pdxUart->Uartfd, &set); /* add our file descriptor to the set */
                timeout.tv_sec = DXOS_WAIT_FOREVER;
                timeout.tv_usec = 0;
                if (select(pdxUart->Uartfd + 1, &set, NULL, NULL, &timeout) <= 0)
                {
                    break;
                }
            }
        }
        else
        {
            RetCode = DXUART_SUCCESS;
            break;
        }

    }


    return RetCode;
}

dxUART_RET_CODE dxUART_PutCh(dxUART_t *pdxUart, uint8_t ch)
{
    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    if (write(pdxUart->Uartfd, &ch, 1) <= 0)
        return DXUART_ERROR;

    return DXUART_SUCCESS;
}

int dxUART_Receive_Blocked(dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms)
{

    int LenRead = DXUART_ERROR;

    if ((pdxUart == NULL) || (prxbuf == NULL)) {
        return DXUART_ERROR;
    }

    if ((len > 256) || (len == 0)) {
        return DXUART_PARAM_ERR;
    }

    char        UartRcvBuff[256];
    uint32_t    TotalByteLeftToRead = len;
    uint8_t*    pHostRcvBuff = prxbuf;
    uint32_t    WaitTimeOutLeft = timeout_ms;
    while (TotalByteLeftToRead)
    {
        memset(UartRcvBuff, 0, sizeof(UartRcvBuff));
        ssize_t SizeRead = read(pdxUart->Uartfd, UartRcvBuff, TotalByteLeftToRead);
        if (SizeRead < 0)
        {
            if ((errno != EAGAIN) && (errno != EWOULDBLOCK))
            {
                perror("dxUART_Receive_Blocked Error1");
                break;

            }
        }
        else if (SizeRead > 0)
        {
            //printf("dxUART_Receive_Blocked number byte read = %d (%s) (TotalByteWasLeft=%d)\n", SizeRead, UartRcvBuff, TotalByteLeftToRead);
            TotalByteLeftToRead = (SizeRead > TotalByteLeftToRead) ? 0:(TotalByteLeftToRead - SizeRead);

            memcpy(pHostRcvBuff, UartRcvBuff, SizeRead);
            pHostRcvBuff += SizeRead;
        }

        if (TotalByteLeftToRead)
        {
            if (WaitTimeOutLeft)
            {

                fd_set set;
                struct timeval timeout;
                FD_ZERO(&set);
                FD_SET(pdxUart->Uartfd, &set);
                timeout.tv_sec = WaitTimeOutLeft/1000;
                timeout.tv_usec = (WaitTimeOutLeft%1000)*1000;
                dxTime_t BeforeWait = dxTimeGetMS();
                int selret = select(pdxUart->Uartfd + 1, &set, NULL, NULL, &timeout);

                if (selret < 0)
                {
                    perror("dxUART_Receive_Blocked Error2");
                    break;
                }
                else if (selret > 0)
                {
                    dxTime_t TimeWaited = dxTimeGetMS() - BeforeWait;

                    //printf("dxUART_Receive_Blocked wait byte interrupted, data available (T=%d, T1=%d, T2=%d)\n", TimeWaited, WaitTimeOutLeft, (WaitTimeOutLeft-TimeWaited));

                    WaitTimeOutLeft = (TimeWaited >= WaitTimeOutLeft) ? 0:(WaitTimeOutLeft-TimeWaited);

                }
                else //TimeOut
                {
                    LenRead = DXUART_TIMEOUT;
                    //printf("dxUART_Receive_Blocked TimeOut\n");
                    break;
                }

            }
        }
        else
        {
            LenRead = len;
        }
    }

    return LenRead;
}

int dxUART_Send_Blocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len)
{
    int count = 0;

    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    uint32_t ByteToWrite = len;
    uint8_t* pWriteBuff = ptxbuf;

    while (ByteToWrite)
    {
        ssize_t byteWrote = write(pdxUart->Uartfd, (const void*) pWriteBuff, ByteToWrite);
        if (byteWrote < 0)
        {
            count = DXUART_ERROR;
            perror("dxUART_Send_Blocked Error");
            break;
        }

        ByteToWrite = (byteWrote >= ByteToWrite) ? 0:(ByteToWrite - byteWrote);
    }

    if (count >= 0) {
        count = len - ByteToWrite;
    }

    return count;
}



//============================================================================
// BSP
//============================================================================

/**
 * \return DXBSP_SUCCESS    if BSP initialize success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxBSP_Init(void)
{
    dxBSP_RET_CODE result = DXBSP_SUCCESS;

    return result;
}

//============================================================================
// GPIO
//============================================================================
static dxGPIO_t* dxGPIOTable = NULL;

static dxGPIO_t *dxGPIO_GetObject(uint32_t PinName)
{
    if (dxGPIOTable == NULL) {
        return NULL;
    }
    dxGPIO_t *p = NULL;
    dxGPIO_t *pObject = NULL;
    for (p = dxGPIOTable; p->Name != DX_ENDOF_GPIO_CONFIG; p++) {
        if (p != NULL && p->Name == PinName) {
            pObject = p;

            //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
            //                 __FUNCTION__,
            //                 p->Name,
            //                 p->Direction,
            //                 p->Mode);
            break;
        }
    }

    return pObject;
}

dxBSP_RET_CODE dxGPIO_Init(dxGPIO_t* ConfigTable)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    if (ConfigTable != NULL) {
        ret = dxGPIO_Uninit(ConfigTable);

        if (ret != DXBSP_SUCCESS) {
            return ret;
        }
    }

    dxGPIOTable = ConfigTable;

    dxGPIO_t *pGPIO = NULL;
    for (pGPIO = dxGPIOTable; pGPIO->Name != DX_ENDOF_GPIO_CONFIG; pGPIO++) {

        if (pGPIO->Name == DX_NOT_ASSIGNED)
            continue;

        if (pGPIO->Object == NULL) {

            int GpioDomain = SYSTEM_GPIO;
            int GpioNumber = pGPIO->Name;
            if (pGPIO->Name > GPIO_GROUP_2)
            {
                GpioDomain = MCU_GPIO;
                GpioNumber = pGPIO->Name - GPIO_GROUP_2;
            }

            pGPIO->Object = rts_io_gpio_request(GpioDomain, GpioNumber);
            if (pGPIO->Object == NULL)
            {
                printf("WARNING: GPIO%d Initialize FAILED\n", pGPIO->Name);
                continue;
            }

            int ret = rts_io_gpio_set_direction(pGPIO->Object, pGPIO->Direction);
            if (ret)
                printf("WARNING: GPIO%d set direction FAILED\n", pGPIO->Name);

        }

    }

    return DXBSP_SUCCESS;
}

int dxGPIO_Read(uint32_t PinName)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL) {
        return DXBSP_ERROR;
    }

    if (pGPIO->Direction != DXPIN_INPUT) {

        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL) {
        return DXBSP_ERROR;
    }

    int ret = rts_io_gpio_get_value(pGPIO->Object);

    return ret;
}

dxBSP_RET_CODE dxGPIO_Write(uint32_t PinName, dxPIN_Level level)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL) {
        return DXBSP_ERROR;
    }

    if (pGPIO->Direction != DXPIN_OUTPUT) {

        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL) {
        return DXBSP_ERROR;
    }

    if (rts_io_gpio_set_value(pGPIO->Object, level) != 0)
        return DXBSP_ERROR;

    return DXBSP_SUCCESS;
}

dxBSP_RET_CODE dxGPIO_Set_Direction(uint32_t PinName, dxPIN_Direction Dir)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL)
    {
        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL)
    {
        return DXBSP_ERROR;
    }

    // Change Direction
    if ((Dir == DXPIN_INPUT) || (Dir == DXPIN_OUTPUT))
    {
        if (rts_io_gpio_set_direction(pGPIO->Object, Dir) == 0)
        {
            pGPIO->Direction = Dir;
        }
        else
        {
            return DXBSP_ERROR;
        }
    }
    else
    {
        return DXBSP_ERROR;
    }

    return DXBSP_SUCCESS;
}

int dxGPIO_Get_Direction(uint32_t PinName)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL ||
        pGPIO->Object == NULL)
    {
        return DXBSP_ERROR;
    }

    return rts_io_gpio_get_direction(pGPIO->Object);
}

dxBSP_RET_CODE dxGPIO_Uninit(dxGPIO_t* ConfigTable)
{
    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    char GpioUnexportCmd[64];
    dxGPIO_t *p = NULL;
    printf("dxGPIO_Uninit all GPIO regardless if it was initialized before\n");
    for (p = ConfigTable; p->Name != DX_ENDOF_GPIO_CONFIG; p++) {
        if (p != NULL) {
            if (p->Name != DX_NOT_ASSIGNED)
            {
                if (p->Object != NULL)
                {
                    rts_io_gpio_free(p->Object);
                    p->Object = NULL;
                }

                memset(GpioUnexportCmd, 0, sizeof(GpioUnexportCmd));
                sprintf(GpioUnexportCmd, "echo %d > /sys/class/gpio/unexport", p->Name);
                system(GpioUnexportCmd);
                //G_SYS_DBG_LOG_NV("[%s]Line %d\n", __FUNCTION__, __LINE__);
            }
        }
    }

    return DXBSP_SUCCESS;
}


//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void)
{
    sync();

	G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_ONLINE, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                     "WARNING:(Line=%d) System will reboot after 5 seconds\r\n",
                     __LINE__);

    dxTimeDelayMS(5000);
    reboot(RB_AUTOBOOT);
}

void dxPLATFORM_Init(void)
{
    //TODO: If needed
}

#include <linux/wireless.h>	/* for "struct iwreq" et al */
#include <linux/sockios.h>	/* SIOCDEVPRIVATE */
static int
wlan_ioctl_mp( char *pBuffer, uint16_t BufferSize )
{
	int sock_fd, err = 0;
	struct ifreq ifr;
	union iwreq_data u;

	sock_fd = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sock_fd < 0 ) {
		dbg_warm( "sock_fd < 0" );
		return -1;
	}

	memset( &u, 0, sizeof( union iwreq_data ) );
	u.data.pointer	= pBuffer;
	u.data.length	= BufferSize;

	memset( &ifr, 0, sizeof( struct ifreq ) );
	strcpy( ifr.ifr_ifrn.ifrn_name, "wlan0" );
	ifr.ifr_ifru.ifru_data = &u;

	err = ioctl( sock_fd, SIOCDEVPRIVATE, &ifr );

	if ( u.data.length == 0 ) {
		*pBuffer = 0;
	}

	close( sock_fd );

	return err;
}

static int
wlan_old_ioctl_mp( char *pBuffer, uint16_t BufferSize )
{
	int sock_fd, err = 0;
	struct iwreq iwr;

	sock_fd = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sock_fd < 0 ) {
		dbg_warm( "sock_fd < 0" );
		return -1;
	}

	memset( &iwr, 0, sizeof( struct iwreq ) );
	strcpy( iwr.ifr_ifrn.ifrn_name, "wlan0" );

	iwr.u.data.pointer	= pBuffer;
	iwr.u.data.length	= BufferSize;

	err = ioctl( sock_fd, SIOCDEVPRIVATE, &iwr );

	if ( iwr.u.data.length == 0 ) {
		*pBuffer = 0;
	}

	close( sock_fd );

	return err;
}

static int
wlan_get_mac( uint8_t *pmac )
{
	struct ifreq ifr;
	int sock_fd;

	memset( &ifr, 0, sizeof( struct ifreq ) );

	sock_fd = socket( AF_INET, SOCK_STREAM, 0 );
	if ( sock_fd < 0 ) {
		dbg_warm( "sock_fd < 0" );
		return DXNET_ERROR;
	}

	strcpy( ifr.ifr_name, "wlan0" );
	if ( ioctl( sock_fd, SIOCGIFFLAGS, &ifr ) < 0 ) {
		dbg_warm( "ioctl( sock_fd, SIOCGIFFLAGS, &ifr ) < 0" );
		close( sock_fd );
		return DXNET_ERROR;
	}

	if ( ioctl( sock_fd, SIOCGIFHWADDR, &ifr ) == 0 ) {
        uint8_t mac[ 6 ];
		memcpy( mac, ifr.ifr_ifru.ifru_hwaddr.sa_data, 6 );
		dbg_warm( "MAC: %02x:%02x:%02x:%02x:%02x:%02x", mac[ 0 ], mac[ 1 ], mac[ 2 ], mac[ 3 ], mac[ 4 ], mac[ 5 ] );

		pmac[ 0 ]   = mac[ 0 ];
		pmac[ 1 ]   = mac[ 1 ];
		pmac[ 2 ]   = mac[ 2 ];
		pmac[ 3 ]   = 0xFF;
		pmac[ 4 ]   = 0xFF;
		pmac[ 5 ]   = mac[ 3 ];
		pmac[ 6 ]   = mac[ 4 ];
		pmac[ 7 ]   = mac[ 5 ];
	} else {
		dbg_warm( "ioctl SIOCGIFHWADDR failed\n" );
	}

	close( sock_fd );

	return 0;
}

dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac)
{
	int size, err;
	char input[ 64 ];

	size = sprintf( input, "efuse_set mac,%02x%02x%02x%02x%02x%02x", pmac->MacAddr[ 0 ], pmac->MacAddr[ 1 ], pmac->MacAddr[ 2 ], pmac->MacAddr[ 5 ], pmac->MacAddr[ 6 ], pmac->MacAddr[ 7 ] );

	dbg_warm( "MAC: %s", input );
 	err = wlan_old_ioctl_mp( input, size + 1 );
	/* Close the socket. */

	if ( err < 0 ) {
		err = wlan_ioctl_mp( input, size + 1 );
	}

	if ( err < 0 ) {
		return DXBSP_ERROR;
	} else {
		sleep( 1 );
		system( "rmmod /usr/lib/modules/8188eu.ko" );

		sleep( 1 );
		system( "insmod /usr/lib/modules/8188eu.ko" );

		sleep( 1 );
		system( "ifconfig wlan0 up" );

		if ( wlan_get_mac( prmac->MacAddr ) < 0 ) {
			return DXBSP_ERROR;
		} else {
			return DXBSP_SUCCESS;
		}
	}
}

int         PlatformWatchDogfd = -1;
uint32_t    PlatformWatchDogTimeOutTime = 10;
dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;

    PlatformWatchDogfd = open("/dev/watchdog", O_WRONLY);
    if (PlatformWatchDogfd < 0)
    {
        printf("WARNING: dxPLATFORM_WatchDogInit failed!");
        ret = DXBSP_ERROR;
    }
    else
    {
        if (TimeOut)
            PlatformWatchDogTimeOutTime = TimeOut;

        ioctl(PlatformWatchDogfd, WDIOC_SETTIMEOUT, (int*)&PlatformWatchDogTimeOutTime);
    }

    return ret;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (PlatformWatchDogfd >= 0)
    {
        int flags = WDIOS_ENABLECARD;
        ioctl(PlatformWatchDogfd, WDIOC_SETOPTIONS, &flags);
        ret = DXBSP_SUCCESS;
    }

    return ret;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (PlatformWatchDogfd >= 0)
    {
        ioctl(PlatformWatchDogfd, WDIOC_KEEPALIVE, NULL);
        ret = DXBSP_SUCCESS;
    }

    return ret;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (PlatformWatchDogfd >= 0)
    {
        close(PlatformWatchDogfd);
        ret = DXBSP_SUCCESS;
    }

    return ret;
}

//============================================================================
// FLASH API
//============================================================================
dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash)
{
    return DXOS_UNAVAILABLE;
}

dxFlashHandle_t dxFlash_Init(void)
{
    return NULL;
}

dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Read(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Read_Word(dxFlashHandle_t flash, uint32_t address, uint32_t *data)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Write(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Write_Word(dxFlashHandle_t flash, uint32_t address, uint32_t data)
{
    return DXOS_UNAVAILABLE;
}

dxOS_RET_CODE dxFlash_Uninit(dxFlashHandle_t flash)
{
    return DXOS_UNAVAILABLE;
}

//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================

/**
 * \brief Structure to store MD5 checksum result
 */
typedef struct {
    uint32_t         nTotalReceiveBytes;    // File size in bytes
    uint32_t         nDownloadBytes;        // Current received bytes
    uint8_t         *pszFileMD5Sum;         // The MD5 checksum result of file, in hexadecimal string format.
                                            // For example, 0x41 => "41" (character " is not included)

    dxMD5_CTX       *pmd5cx;                // MD5 checksum context
    uint8_t         *pmd5sum;               // Current MD5 checksum data

	uint8_t			*ImageBuffer;
    uint32_t        ImageBufferBytes;

    uint8_t         BlockID;         // Reserved for expandability
} dxImgInfoAccessCtrl_t;

static dxImgInfoAccessCtrl_t dxMainImgInfoAccessCtrl = { 0, };

static dxBOOL
dxImage_MD5IsEqual( dxImgInfoAccessCtrl_t *pHandle )
{
    uint32_t len = 16;//strlen(pHandle->pmd5sum);
    if ( ( len * 2 ) != strlen( ( char * )pHandle->pszFileMD5Sum ) ) {
        return dxFALSE;
    }

    uint8_t i   = 0;
    uint8_t ch  = 0;
    uint8_t d   = 0;

    while ( i < len ) {
        ch = ( pHandle->pmd5sum[ i ] >> 4 ) & 0x0F;
        ch += 0x30;

        if ( ch > '9' ) {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[ i * 2 ];

        if ( d >= 'a' && d <= 'z' ) {
            d = d - 'a' + 'A';
        }

        if ( ch != d ) {
			return dxFALSE;
        }

        ch = ( pHandle->pmd5sum[ i ] ) & 0x0F;
        ch += 0x30;

        if ( ch > '9' ) {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[ ( i * 2 ) + 1 ];

        if ( d >= 'a' && d <= 'z' ) {
            d = d - 'a' + 'A';
        }

        if ( ch != d ) {
			return dxFALSE;
        }

        i ++;
    }

    return dxTRUE;
}

static void
dxImage_Free( dxImgInfoAccessCtrl_t *pHandle )
{
    if ( pHandle->pszFileMD5Sum ) {
        dxMemFree( pHandle->pszFileMD5Sum );
        pHandle->pszFileMD5Sum = NULL;
    }

    if ( pHandle->pmd5cx ) {
        dxMemFree( pHandle->pmd5cx );
        pHandle->pmd5cx = NULL;
    }

    if ( pHandle->pmd5sum ) {
        dxMemFree( pHandle->pmd5sum );
        pHandle->pmd5sum = NULL;
    }

	if ( pHandle->ImageBuffer ) {
		dxMemFree( pHandle->ImageBuffer );
		pHandle->ImageBuffer		= NULL;
		pHandle->ImageBufferBytes	= 0;
	}
}

static dxBOOL
dxImage_Init( dxImgInfoAccessCtrl_t *pHandle, uint8_t *pszMD5Sum, uint32_t nImageSize, uint32_t ImageBufferSize )
{
    pHandle->nTotalReceiveBytes	= nImageSize;
    pHandle->nDownloadBytes		= 0;

    dxImage_Free( pHandle );

    pHandle->pszFileMD5Sum = ( uint8_t * )dxMemAlloc( NULL, 1, MD5_DIGEST_LENGTH * 2 + 1 ); // Additional NULL character is needed.

    if ( pHandle->pszFileMD5Sum  == NULL ) {
        return dxFALSE;
    }

    memcpy( pHandle->pszFileMD5Sum, pszMD5Sum, strlen( ( char * )pszMD5Sum ) );

    pHandle->pmd5cx = ( dxMD5_CTX * )dxMemAlloc( NULL, 1, sizeof( dxMD5_CTX ) );

    if ( pHandle->pmd5cx  == NULL ) {
		dbg_warm( "pHandle->pmd5cx == NULL" );
        return dxFALSE;
    }

    /**
     * sizeof(dxMD5Info.pmd5cx->state) should be 16 bytes,
     * due to declare of "uint32_t state[4]"
     */
    pHandle->pmd5sum = ( uint8_t * )dxMemAlloc( NULL, 1, MD5_DIGEST_LENGTH + 1 );

    if ( pHandle->pmd5sum == NULL ) {
		dbg_warm( "pHandle->pmd5sum == NULL" );
        return dxFALSE;
    }

    dxMD5_Starts( pHandle->pmd5cx );

    /*
     * Initial Image Buffer in memory, when read full image, wrtie to mtd
     */
    pHandle->ImageBufferBytes   = ImageBufferSize;
    pHandle->ImageBuffer		= ( uint8_t * )dxMemAlloc( NULL, 1, pHandle->ImageBufferBytes );
    if ( pHandle->ImageBuffer == NULL ) {
		dbg_warm( "pHandle->ImageBuffer == NULL" );
        return dxFALSE;
    }
    memset( pHandle->ImageBuffer, 0xFF, pHandle->ImageBufferBytes );

    return dxTRUE;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Init(void)
{
    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Stop(void)
{
    dxImage_Free( &dxMainImgInfoAccessCtrl );

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_Start( uint8_t *pszMD5Sum, uint32_t nImageSize )
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    if ( ( pszMD5Sum == NULL ) ||
		 ( strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 ) ) ||
		 ( nImageSize == 0 ) ) {
        return DXMAINIMAGE_ERROR;
    } else if ( dxImage_Init( &dxMainImgInfoAccessCtrl, pszMD5Sum, nImageSize, nImageSize ) == dxFALSE ) {
        return DXMAINIMAGE_ERROR;
	} else {
		return DXMAINIMAGE_SUCCESS;
	}
}

dxMAINIMAGE_RET_CODE
dxMainImage_Write( uint8_t *pImage, uint32_t nImageLen )
{
	if ( ( pImage == NULL ) || ( nImageLen == 0 ) ) {
		return DXMAINIMAGE_ERROR;
	} else if ( ( dxMainImgInfoAccessCtrl.pmd5cx == NULL ) ||
		 ( dxMainImgInfoAccessCtrl.pmd5sum == NULL ) ||
		 ( dxMainImgInfoAccessCtrl.ImageBuffer == NULL ) ||
		 ( dxMainImgInfoAccessCtrl.nTotalReceiveBytes <= 0 ) ||
		 ( dxMainImgInfoAccessCtrl.nDownloadBytes < 0 ) ||
		 ( pImage == NULL ) ) {
		dxMainImage_Stop();
		return DXMAINIMAGE_ERROR;
    } else if ( ( dxMainImgInfoAccessCtrl.nDownloadBytes + nImageLen ) > dxMainImgInfoAccessCtrl.ImageBufferBytes ) {
		dxMainImage_Stop();
		return DXMAINIMAGE_ERROR;
	}

    memcpy( dxMainImgInfoAccessCtrl.ImageBuffer + dxMainImgInfoAccessCtrl.nDownloadBytes, pImage, nImageLen ); // write image

    dxMD5_Update( dxMainImgInfoAccessCtrl.pmd5cx, ( unsigned char * )pImage , nImageLen );

    dxMainImgInfoAccessCtrl.nDownloadBytes += nImageLen;

    if ( dxMainImgInfoAccessCtrl.nDownloadBytes >= dxMainImgInfoAccessCtrl.nTotalReceiveBytes ) {
        // MD5 checksum verify
        dxMD5_Finish( dxMainImgInfoAccessCtrl.pmd5sum, dxMainImgInfoAccessCtrl.pmd5cx );

        if ( dxImage_MD5IsEqual( &dxMainImgInfoAccessCtrl ) == dxTRUE ) {
            if ( dxFWParserWrite( dxMainImgInfoAccessCtrl.ImageBuffer ) == 0 ) {
                return DXMAINIMAGE_COMPLETE;
            } else {
            	dxMainImage_Stop();
                return DXMAINIMAGE_ERROR;
            }
        } else {
            dxMainImage_Stop();
            return DXMAINIMAGE_ERROR;
        }
    }

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE
dxMainImage_UpdateReboot( dxBOOL WithSystemSelfReboot )
{
    dxMainImage_Stop();

    if ( dxFWIpcamBootSwitch() == dxFALSE ) {
        return DXMAINIMAGE_ERROR;
    } else if ( WithSystemSelfReboot == dxTRUE ) {
        dxPLATFORM_Reset();
    }

    return DXMAINIMAGE_SUCCESS;
}

#if DX_SUBIMG_FLASH_SIZE                // SubImage only support on flash at least 4MB

//============================================================================
// SUB IMAGE UPDATE API
//============================================================================

//============================================================================
//    Header
//+-----------------------------+---------------------+--------------------+
//| Tag                         | ( 8 Bytes )         | dxSubImageHeader_t |
//+-----------------------------+---------------------+                    |
//| DeviceType                  | ( 2 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderVersion               | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderSize                  | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| ImageSize                   | ( 4 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| SubImageVersion             | ( 16 Bytes )        |                    |
//+-----------------------------+---------------------+--------------------+
//============================================================================
# define DX_SUBIMAGE_MARK			0xF1E2D3C4B5A69788
# define DX_FW_SUB_IMAGE_PATH		DX_PATH_FWUPDATESUB"/"DX_FW_SUB_IMAGE

typedef struct __attribute__ ( ( packed ) ) {
    uint64_t        Tag;                    // Must be DX_SUBIMAGE_MARK (check dxBSP.c)
    uint16_t        DeviceType;             // Distinguish this sub image for which device
    uint8_t         HeaderVer;              // For header extensions, start form 1
    uint8_t         HeaderSize;             // For simplify code, should be sizeof(dxSubImageHeaderAccessCtrl_t)
    uint32_t        ImageSize;              // SubImg from server must >0, should be less than DX_SUBIMG_FLASH_SIZE (except header)
    uint8_t         SubImgVer[ 16 ];        // Sub image version from supplier, use ASCII coding
} dxSubImageHeaderAccessCtrl_t;             // sizeof(dxSubImageHeaderAccessCtrl_t) should be 32 bytes

static dxImgInfoAccessCtrl_t *dxSubImgInfoAccessCtrl[ 1 ] = {};

static dxOS_RET_CODE
dxWriteFILE( uint8_t *pImageBuffer, int32_t nImageSize )
{
    struct stat st = {0};

    if ( stat( DX_PATH_FWUPDATESUB, &st ) < 0 ) {
        mkdir( DX_PATH_FWUPDATESUB, 0700 );
    }

    dxOS_RET_CODE ret = DXSUBIMAGE_ERROR;

    FILE *fp = fopen( DX_FW_SUB_IMAGE_PATH, "wb" );
    if ( fp != NULL ) {
        if ( fwrite( pImageBuffer, 1, nImageSize, fp ) != nImageSize ) {
            ret = DXSUBIMAGE_ERROR;
        } else {
            ret = DXSUBIMAGE_SUCCESS;
		}

        fclose( fp );
    }

    return ret;
}

static dxOS_RET_CODE
dxSubImage_WriteHeader( uint8_t *pImageBuffer, uint32_t nImageLen )
{
	dxSubImageHeaderAccessCtrl_t *dxHeader = ( dxSubImageHeaderAccessCtrl_t * )pImageBuffer;

	memset( dxHeader, 0x00, sizeof( dxSubImageHeaderAccessCtrl_t ) );

	dxHeader->Tag			= DX_SUBIMAGE_MARK;
	dxHeader->DeviceType	= 0x00;
	dxHeader->HeaderVer		= 0x01;
	dxHeader->HeaderSize	= sizeof( dxSubImageHeaderAccessCtrl_t );
	dxHeader->ImageSize		= nImageLen;

	return DXSUBIMAGE_SUCCESS;
}

dxSubImgInfoHandle_t
dxSubImage_Init( dxSUBIMGBLOCKID_t blockId )
{
    dxImgInfoAccessCtrl_t *pHandle = NULL;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return NULL;
    } else if ( blockId != DXSUBIMGBLOCK_ONE ) {
    	return NULL;
	} else if ( dxSubImgInfoAccessCtrl[ blockId ] ) {
        return ( dxSubImgInfoHandle_t )dxSubImgInfoAccessCtrl[ blockId ];
    }

    pHandle = dxMemAlloc( NULL, 1, sizeof( dxImgInfoAccessCtrl_t ) );
	if ( pHandle == NULL ) {
		return NULL;
	}

    pHandle->BlockID = blockId;

    dxSubImgInfoAccessCtrl[ blockId ] = pHandle;

    return pHandle;
}

dxSUBIMAGE_RET_CODE
dxSubImage_Start( dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize )
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
		dbg_warm( "DX_FLASH_SIZE_TOTAL_MBITS < 32" );
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) || ( pszMD5Sum == NULL ) || ( nImageSize == 0 ) ) {
		dbg_warm( "( handle == NULL ) || ( pszMD5Sum == NULL ) || ( nImageSize == 0 )" );
		return DXSUBIMAGE_ERROR;
    } else if ( pHandle->BlockID != DXSUBIMGBLOCK_ONE ) {
		dbg_warm( "pHandle->BlockID != DXSUBIMGBLOCK_ONE" );
		return DXSUBIMAGE_ERROR;
	} else if ( nImageSize > DX_SUBIMG_FLASH_SIZE ) {
		dbg_warm( "nImageSize > DX_SUBIMG_FLASH_SIZE" );
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    } else if (  strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 ) ) {
		dbg_warm( "strlen( ( char * )pszMD5Sum ) != ( MD5_DIGEST_LENGTH * 2 )" );
		return DXSUBIMAGE_ERROR;
    } else if ( dxImage_Init( pHandle, pszMD5Sum, nImageSize, ( nImageSize + sizeof( dxSubImageHeaderAccessCtrl_t ) ) ) == dxFALSE ) {
        return DXSUBIMAGE_ERROR;
	} else {
		unlink( DX_FW_SUB_IMAGE_PATH );
    	return DXSUBIMAGE_SUCCESS;
	}
}

dxSUBIMAGE_RET_CODE
dxSubImage_Write( dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen )
{
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint8_t *dst;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) || ( pImage == NULL ) || ( nImageLen == 0 ) ) {
        return DXSUBIMAGE_ERROR;
	} else if ( pHandle->BlockID != DXSUBIMGBLOCK_ONE ) {
        return DXSUBIMAGE_ERROR;
	} else if ( ( pHandle->pmd5cx == NULL ) ||
				( pHandle->pmd5sum == NULL ) ||
				( pHandle->ImageBuffer == NULL ) ||
				( pHandle->nTotalReceiveBytes <= 0 ) ||
				( pHandle->nDownloadBytes < 0 ) ) {
		return DXSUBIMAGE_ERROR;
    } else if ( ( pHandle->nDownloadBytes + nImageLen ) > pHandle->ImageBufferBytes ) {
		return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    }

	dst = pHandle->ImageBuffer + sizeof( dxSubImageHeaderAccessCtrl_t ) + pHandle->nDownloadBytes;
    memcpy( dst, pImage, nImageLen ); // write image

    dxMD5_Update( pHandle->pmd5cx, pImage, nImageLen );

    pHandle->nDownloadBytes += nImageLen;

    if ( pHandle->nDownloadBytes >= pHandle->nTotalReceiveBytes ) {
        // MD5 checksum verify
        dxMD5_Finish( pHandle->pmd5sum, pHandle->pmd5cx );

        if ( dxImage_MD5IsEqual( pHandle ) == dxTRUE ) {
            ret = dxSubImage_WriteHeader( pHandle->ImageBuffer, pHandle->nTotalReceiveBytes );
            if ( ret != DXOS_SUCCESS ) {
                return DXSUBIMAGE_ERROR;
            }

            dxWriteFILE( pHandle->ImageBuffer, sizeof( dxSubImageHeaderAccessCtrl_t ) + pHandle->nTotalReceiveBytes );

            return DXSUBIMAGE_COMPLETE;
        } else {
            return DXSUBIMAGE_ERROR;
        }
    }

    return DXSUBIMAGE_SUCCESS;
}

dxSUBIMAGE_RET_CODE
dxSubImage_Read( dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead, uint8_t *pBDataBuffer, uint32_t *FwSizeOut )
{
    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( ( handle == NULL ) ||
				( TotalbyteToRead == 0 ) ||
				( pBDataBuffer == NULL ) ||
				( FwSizeOut == NULL ) ) {
        return DXSUBIMAGE_ERROR;
    } else if ( TotalbyteToRead > DX_SUBIMG_FLASH_SIZE ) {
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    } else {
	    FILE *fp = fopen( DX_FW_SUB_IMAGE_PATH, "rb" );
	    if ( fp == NULL ) {
	        *FwSizeOut = 0;
	        return DXAPPDATA_ERROR;
	    }

	    uint32_t offset = sizeof( dxSubImageHeaderAccessCtrl_t ) + StartPosition;

	    fseek( fp, offset, SEEK_SET );

	    *FwSizeOut = fread( pBDataBuffer, 1, TotalbyteToRead, fp );

	    fclose( fp );

	    return DXAPPDATA_SUCCESS;
	}
}

dxSUBIMAGE_RET_CODE
dxSubImage_Uninit( dxSubImgInfoHandle_t handle )
{
    dxImgInfoAccessCtrl_t *pHandle = ( dxImgInfoAccessCtrl_t * )handle;

    if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    } else if ( pHandle == NULL ) {
        return DXSUBIMAGE_ERROR;
    }

    dxSubImgInfoAccessCtrl[ pHandle->BlockID ] = NULL;

    dxMemFree( pHandle );

    return DXSUBIMAGE_SUCCESS;
}

uint32_t
dxSubImage_ReadImageSize( dxSubImgInfoHandle_t handle )
{
    uint32_t ImageSize = 0;

	if ( DX_FLASH_SIZE_TOTAL_MBITS < 32 ) {
        return ImageSize;
    } else if ( handle == NULL ) {
        return ImageSize;
    } else {
		int		ret;
		FILE	*fp;

	    dxSubImageHeaderAccessCtrl_t	dxHeader;

	    fp = fopen( DX_FW_SUB_IMAGE_PATH, "rb" );
		if ( fp == NULL ) {
			dbg_warm( "%s NOT Exist!!!", DX_FW_SUB_IMAGE_PATH );
			return ImageSize;
		}

	    ret = fread( &dxHeader, 1, sizeof( dxSubImageHeaderAccessCtrl_t ), fp );
		fclose( fp );

		if ( ( ret == sizeof( dxSubImageHeaderAccessCtrl_t ) ) &&
			( dxHeader.Tag == DX_SUBIMAGE_MARK ) &&
			( dxHeader.DeviceType == 0x00 ) &&
			( dxHeader.HeaderVer == 0x01 ) &&
			( dxHeader.HeaderSize == sizeof( dxSubImageHeaderAccessCtrl_t ) ) ) {
	        ImageSize = dxHeader.ImageSize;
		}

	    return ImageSize;
	}
}

#endif // DX_SUBIMG_FLASH_SIZE

//============================================================================
// System Information
//============================================================================
#define DX_APPDATA_MARK                 0xF1E2D3C4B5A69788
#define DX_APPDATA_VER                  0x01

#define DX_APPDATA_BLOCK_IS_VALID       1
#define DX_APPDATA_BLOCK_IS_INVALID     0

#define DX_APPDATA_OFFSET_INVALID       0x0FFFFFFFF
#define DX_APPDATA_MAX_APPDATA_SIZE     (DX_SYSINFO_FLASH_SIZE - sizeof(dxAppDataHeaderInfo_t) - sizeof(dxAppInfo_t))

typedef struct
{
    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
    uint8_t         nPage;                  // 0 => DX_DEVINFO_FLASH_ADDR1 or
                                            //      DX_SYSINFO_FLASH_ADDR1 (check dxMemConfig.h)
                                            // 1 => DX_DEVINFO_FLASH_ADDR2 or
                                            //      DX_SYSINFO_FLASH_ADDR2 (check dxMemConfig.h)
    uint8_t         nBlockID;               // 0 => dxBLOCKID_t is DXBLOCKID_DEVINFO
                                            // 1 => dxBLOCKID_t is DXBLOCKID_SYSINFO
    uint8_t         Reserved[2];            // Must be 0x0000
    uint32_t        nOffset;                // always point to the last data of structure dxAppInfo_t
                                            // 0xFFFFFFFF means there is no data behind the header
                                            // 0          means there is only one record behind the header
                                            // 24         means the last data is start from
                                            //            DX_xxxINFO_FLASH_ADDRn + sizeof(dxAppDataHeaderInfo_t) + 24
} dxAppDataAccessCtrl_t;

typedef struct
{
    uint64_t        Tag;                    // Must be DX_APPDATA_MARK (check dxBSP.c)
    uint8_t         Version;                // Must not 0x00
    uint8_t         SeqNo;                  // Must be 0x01 ~ 0xFE. 0xFF: Just erased.
    uint8_t         Reserved[4];            // Reserved. Must be 0x00000000
    uint16_t        Size;                   // sizeof(dxAppDataHeaderInfo_t). Size will be padded to 4 bytes alignment.
} dxAppDataHeaderInfo_t;                    // sizeof(dxAppDataHeaderInfo_t) should be 16 bytes

typedef struct
{
    uint16_t        Size;                   // Size including sizeof(dxAppInfo_t) + all data behind pData[0]
    uint16_t        CRC16;                  // Using CRC16 with polynomial 0x8005 to calculate data from pData[0]

    // Example user data. Declare your data here.
    uint8_t         pData[0];               // NOTE: If you have dynamic length data, you can put your data to pData[0].
                                            //       pData[0] should be the last element of structure dxAppInfo_t. Or
                                            //       a compiler error will occur.
                                            //
                                            //       Use offsetof(dxAppInfo_t, pData) to get the address of pData[0].
                                            //       The value of offsetof(dxAppInfo_t, pData) should be 4
} dxAppInfo_t;                              // sizeof(dxAppInfo_t) should be 4 bytes

static dxAppDataAccessCtrl_t * dxAppDataAccessCtrl[2]={NULL,NULL};

dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t blockId)
{
    dxAppDataAccessCtrl_t *pHandle = NULL;
    struct stat st = {0};

    if (stat(DX_PATH_DKSTORAGE, &st) == -1) {
        printf("mkdir %s\n",DX_PATH_DKSTORAGE);
        mkdir(DX_PATH_DKSTORAGE, 0700);
    }
    if (stat(DX_PATH_FRAMEWORKINFO, &st) == -1) {
        mkdir(DX_PATH_FRAMEWORKINFO, 0700);
    }

    if (blockId == DXBLOCKID_DEVINFO && dxAppDataAccessCtrl[0])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }
    else if (blockId == DXBLOCKID_SYSINFO && dxAppDataAccessCtrl[1])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxAppDataAccessCtrl_t));

    if (pHandle == NULL) {
        return NULL;
    }

    pHandle->nBlockID = blockId;

    pHandle->nPage = 0;
    memset(&pHandle->Reserved[0], 0x00, sizeof(pHandle->Reserved));

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL) {
        dxMemFree(pHandle);
        pHandle = NULL;

        return NULL;
    }

    if (blockId == DXBLOCKID_DEVINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[0];
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[1];
    }

    return NULL;
}

dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo)
{
    FILE *fd;
    char cfg_file[64], bak_file[64];
    uint16_t crc16;

    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxAPPDATA_RET_CODE ret = DXAPPDATA_ERROR;

    if (pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nBlockID >= 2 ||
        pnSizeOfAPInfo == NULL ||
        pAPInf == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
    }

    if (*pnSizeOfAPInfo == 0) {
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    dxAppInfo_t *pAppInfo = dxMemAlloc("pAppInfo", 1, (*pnSizeOfAPInfo + sizeof(dxAppInfo_t)));

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    fd = fopen(cfg_file,"rb");
    if(fd != NULL){
        ret=fread(pAppInfo,1,sizeof(dxAppInfo_t)+*pnSizeOfAPInfo,fd);
        fclose(fd);
        if (ret != (sizeof(dxAppInfo_t)+*pnSizeOfAPInfo)){
            ret = DXAPPDATA_ERROR;
        }else {
            ret = DXAPPDATA_SUCCESS;
            if ((pAppInfo->Size > DX_APPDATA_MAX_APPDATA_SIZE) || (*pnSizeOfAPInfo < pAppInfo->Size)) {
                ret = DXAPPDATA_SIZE_LIMIT_REACHED;
#if CRC16_ENABLE
            } else {
                crc16 = Crc16(pAppInfo->pData, pAppInfo->Size, 0);
                if (crc16 != pAppInfo->CRC16) {
                    // CRC16 mismatch
                    ret = DXAPPDATA_ERROR;
                }
#endif
            }
        }
    } else {
        ret = DXAPPDATA_ERROR;
    }

    if (ret != DXAPPDATA_SUCCESS) {
        fd = fopen(bak_file,"rb");
        if(fd != NULL){
            ret=fread(pAppInfo,1,sizeof(dxAppInfo_t)+*pnSizeOfAPInfo,fd);
            fclose(fd);
            if (ret != (sizeof(dxAppInfo_t)+*pnSizeOfAPInfo)){
                ret = DXAPPDATA_BAK_ERROR;
            } else {
                ret = DXAPPDATA_BAK_SUCCESS;
                if ((pAppInfo->Size > DX_APPDATA_MAX_APPDATA_SIZE) || (*pnSizeOfAPInfo < pAppInfo->Size)) {
                    ret = DXAPPDATA_BAK_SIZE_LIMIT_REACHED;
                }
#if CRC16_ENABLE
                crc16 = Crc16(pAppInfo->pData, pAppInfo->Size, 0);
                if (crc16 != pAppInfo->CRC16) {
                    // CRC16 mismatch
                    ret = DXAPPDATA_BAK_ERROR;
                }
#endif
            }
	} else {
            ret = DXAPPDATA_BAK_ERROR;
        }
    }

    if (ret == DXAPPDATA_BAK_SUCCESS){
        // Copy bak to cfg file
        char buffer[64];
        size_t bytes;

        FILE *fdr,*fdw;
        fdr = fopen(bak_file,"rb");
        if(fdr != NULL){
            fdw = fopen(cfg_file,"wb");
            if (fdw != NULL) {
                while ((bytes = fread(buffer, 1, 64, fdr)) != 0) {
#if DBG_LOG
                    if(fwrite(buffer, 1, bytes, fdw) != bytes) {
                        G_SYS_DBG_LOG_V("Copy file %s to %s failed!\n", bak_file, cfg_file);
                    }
#else
                    fwrite(buffer, 1, bytes, fdw);
#endif
                }
                fclose(fdw);
            }
            fclose(fdr);
        }
        ret = DXAPPDATA_SUCCESS;
    }

    dxMutexGive(pHandle->hMutex);

    if ((ret == DXAPPDATA_BAK_ERROR) || (ret == DXAPPDATA_BAK_SIZE_LIMIT_REACHED)){
        dxAppData_Clean(pHandle);
        ret = DXAPPDATA_ERROR;
    }

    if (ret == DXAPPDATA_SUCCESS)
    {
        memcpy(pAPInf, pAppInfo->pData, *pnSizeOfAPInfo);
        *pnSizeOfAPInfo = pAppInfo->Size;
        dxMemFree(pAppInfo);
    }

    return ret;
}

uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle)
{
    FILE *fd;
    char cfg_file[64], bak_file[64];

    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
    }

    dxAppInfo_t pAppInfo;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    fd = fopen(cfg_file,"rb");
    if(fd != NULL){
        ret=fread(&pAppInfo,1,sizeof(dxAppInfo_t),fd);
        fclose(fd);
    }
    if (ret != sizeof(dxAppInfo_t)){
        fd = fopen(bak_file,"rb");
        if(fd != NULL){
            ret=fread(&pAppInfo,1,sizeof(dxAppInfo_t),fd);
            fclose(fd);
        }
    }
    if (ret != sizeof(dxAppInfo_t)){
        pAppInfo.Size = 0;
    }

    dxMutexGive(pHandle->hMutex);

    return pAppInfo.Size;
}

dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxAPPDATA_RET_CODE ret = DXAPPDATA_ERROR;
    char cfg_file[64],bak_file[64];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nBlockID >= 2 ||
        nSizeOfAPInfo == 0 ||
        pAPInf == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }
    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
    }

    if (nSizeOfAPInfo == 0 ||
        nSizeOfAPInfo > DX_APPDATA_MAX_APPDATA_SIZE) {

        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    // Copy cfg to bak file
    char buffer[64];
    size_t bytes;

    FILE *fdr,*fdw;
    fdr = fopen(cfg_file,"rb");
    if(fdr != NULL){
        fdw = fopen(bak_file,"wb");
        if (fdw != NULL) {
            while ((bytes = fread(buffer, 1, 64, fdr)) != 0) {
#if DBG_LOG
                if(fwrite(buffer, 1, bytes, fdw) != bytes) {
                    G_SYS_DBG_LOG_V("Copy file %s to %s failed!\n", cfg_file, bak_file);
                }
#else
                fwrite(buffer, 1, bytes, fdw);
#endif
            }
            fclose(fdw);
         }
         fclose(fdr);
    }

    // Append data
    uint32_t len = sizeof(dxAppInfo_t) + nSizeOfAPInfo;
    uint8_t *p = dxMemAlloc(NULL, 1, len);

    dxAppInfo_t *pAppInfo = (dxAppInfo_t *)p;

    pAppInfo->Size = nSizeOfAPInfo;
#if CRC16_ENABLE
    pAppInfo->CRC16 = Crc16(pAPInf, nSizeOfAPInfo, 0);
#endif
    memcpy(&p[offsetof(dxAppInfo_t, pData)], pAPInf, nSizeOfAPInfo);

    fdw = fopen(cfg_file,"wb");
    if(fdw != NULL){

        if (fwrite(p,len,1,fdw) != 1)
            ret = DXAPPDATA_ERROR;
        else
            ret = DXAPPDATA_SUCCESS;
        fclose(fdw);
    } else {
          ret = DXAPPDATA_ERROR;
    }

    if (ret != DXAPPDATA_SUCCESS) {
        dxMutexGive(pHandle->hMutex);
        dxMemFree(p);
        p = NULL;
        return DXAPPDATA_ERROR;
    }

    dxMutexGive(pHandle->hMutex);
    dxMemFree(p);
    p = NULL;

    return ret;
}

dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    char cfg_file[64], bak_file[64];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_DEV);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        sprintf(cfg_file,"%s/%s.dxcfg",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
        sprintf(bak_file,"%s/%s.dxbak",DX_PATH_FRAMEWORKINFO,DX_CONF_SYS);
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    ret = unlink(cfg_file);
    if (ret != DXOS_SUCCESS) {
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    } else {
        ret = unlink(bak_file);
        if (ret != DXOS_SUCCESS) {
            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_BAK_ERROR;
        }
    }

    dxMutexGive(pHandle->hMutex);

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxAppDataAccessCtrl[pHandle->nBlockID] = NULL;

    pHandle->hFlash = NULL;

    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXAPPDATA_SUCCESS;
}

#if defined(configUSE_LOCAL_SYSINFO_DEBUG) && configUSE_LOCAL_SYSINFO_DEBUG == 1
dxFlashHandle_t dxAppData_GetFlash(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->hFlash;
    }

    return NULL;
}

dxBLOCKID_t dxAppData_GetBlockID(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nBlockID;
    }

    return DXAPPDATA_ERROR;
}

uint8_t dxAppData_GetPage(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nPage;
    }

    return DXAPPDATA_ERROR;
}

uint32_t dxAppData_GetOffset(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nOffset;
    }

    return DXAPPDATA_ERROR;
}

#endif //configUSE_LOCAL_SYSINFO_DEBUG

