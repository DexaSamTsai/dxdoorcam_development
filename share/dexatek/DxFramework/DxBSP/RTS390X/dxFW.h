//============================================================================
// File: dxFW.h
//
// Author: Joey Wang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================

#ifndef _DXFW_H
#define _DXFW_H

typedef enum
{
    DXFWTYPE_UPGRADE        = 0,
    DXFWTYPE_IMAGE          = 1,
    DXFWTYPE_HTTP           = 2,
    DXFWTYPE_HTTPS          = 3,
} dxFWType_t;

typedef enum
{
    DXSTORAGETYPE_DXFLASH        = 0,
    DXSTORAGETYPE_DXMTD          = 1,
    DXSTORAGETYPE_DXFILE         = 2,
} dxFWStorageType_t;

typedef struct __attribute__ ( ( packed ) ) dxFWStorage {
    dxFWStorageType_t   Type;
    char                Location[64];
			/* DXFLASH => Flash Address Location[0]-Location[3]
			 * DXMTD   => /dev/mtd5
                         * DXFILE  => /var/DkStorage/FwUpdateSub/DxFWSUB
                         */
    unsigned char	MD5[16];      //MD5 of FWImage
    uint32_t            Size;         //Size of FWImage
    uint8_t             Data[0];      //FWImage
} dxFWStorage_t;

typedef struct __attribute__ ( ( packed ) ) dxFWPackage {
    uint8_t             No;            //0,1,2,3,...
    dxFWType_t          Type;          //Image or URL
    dxFWStorage_t       *Storage;
} dxFWPackage_t;

typedef struct __attribute__ ( ( packed ) ) dxFW {
    uint64_t            Tag;                    //DX_FW_MARK
    uint32_t            nTotoalSize;            //Total Image Size
    uint8_t             nPackageCount;          //Total Package count
    dxFWPackage_t       dxFWImagePackage[0];
} dxFW_t;

#define DX_FWIMAGE_MARK 0xF1E2D3C4B5A69788

extern dxBOOL
dxFWIpcamBootSwitch(void);

extern int
dxFWParserWrite(uint8_t * dxFWBuf);

#endif //DXFW_H
