//============================================================================
// File: dxBSP.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/01/14
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2016/01/20
//     1) Description: Revise API's return code
//        Modified:
//            - All APIs list in this section
//
//     Version: 0.3
//     Date: 2016/01/27
//     1) Description: Add BSP, GPIO, UART API section
//        Modified:
//            - dxBSP_Init()
//
//     Version: 0.4
//     Date: 2016/01/29
//     1) Description: Revise GPIO section
//
//     Version: 0.5
//     Date: 2016/02/01
//     1) Description: Revise GPIO section (dxGPIO_Init())
//
//     Version: 0.6
//     Date: 2016/02/15
//     1) Description: Move dxFlash_XXXX() and dxSysInfo_XXXX() from dxOS
//     2) Description: Support system information access
//        Added:
//            - dxSysInfo_Init()
//            - dxSysInfo_Read()
//            - dxSysInfo_Write()
//            - dxSysInfo_Uninit()
//
//     Version: 0.7
//     Date: 2016/04/26
//     1) Description: Modify dxDevInfo_XXXX() to dxAppDataInfo_XXXX() for new device information memory space
//
//     Version: 0.8
//     Date: 2016/05/05
//     1) Description: Move dxAppData's handle and header structure definition to c file, for access control
//
//     Version: 0.9
//     Date: 2016/05/12
//     1) Description: Add dxSubImage_XXX() for subsystem FW update (e.g. BLE)
//
//     Version: 0.10
//     Date: 2016/05/20
//     1) Description: Modified dxSubImage_XXXX() function
//        Modified:
//            - dxSubImage_Init()
//            - dxSubImage_Start()
//            - dxSubImage_Write()
//            - dxSubImage_Read()
//            - dxSubImage_Uninit()
//            - Data type of nTotalReceiveBytes and nDownloadBytes in structure dxSubImgInfoAccessCtrl_t
//
//     Version: 0.11
//     Date: 2016/05/27
//     1) Description: If flash memory size is incorrect, do nothing to prevent system error.
//        Modified:
//            - All dxSubImage_XXXX() function
//
//     Version: 0.12
//     Date: 2016/05/27
//     1) Description: Added GPIO Direction function for change GPIO direction before read or write
//        Added:
//            - dxGPIO_Set_Direction()
//            - dxGPIO_Get_Direction()
//
//     Version: 0.13
//     Date: 2016/06/15
//     1) Description: Use (DK_UART0_TX, DK_UART0_RX), instead of direct GPIO pin name
//        Modified:
//            - dxUART_Open()
//
//     Version: 0.14
//     Date: 2016/07/05
//     1) Description: Added dxSubImage_ReadImageSize() function for get full Sub Image size
//     2) Description: Revise dxSubImage_Write() function. Witer SubImage header after MD5 check
//        Added:
//            - dxSubImage_ReadImageSize()
//        Modified:
//            - dxSubImage_Write()
//
//     Version: 0.15
//     Date: 2016/08/16
//     1) Description: Add dxHpStorage_XXX() for hyper peripheral storage
//     2) Description: Revise DX_DEVINFO_FLASH_SIZE and DX_SYSINFO_FLASH_SIZE to 16KB
//        Added:
//            - dxHpStorage_XXX()
//            - dxFlash_Erase_SectorWithSize()
//
//     Version: 0.16
//     Date: 2016/08/25
//     1) Description: Added dxPLATFORM_SetMAC() function for update MAC address
//        Added:
//            - dxPLATFORM_SetMAC()
//
//     Version: 0.17
//     Date: 2017/03/20
//     1) Description: Fixed MD5 comparison issue if there is 0x00 in MD5 checksum value
//        Modified:
//            - dxMainImage_MD5IsEqual()
//            - dxSubImage_MD5IsEqual()
//
//     Version: 0.18
//     Date: 2017/03/21
//     1) Description: Turn off Wi-Fi before rebooting
//        Modified:
//            - dxPLATFORM_Reset()
//
//     Version: 0.19
//     Date: 2017/05/03
//     1) Description: Use WLAN0_NAME instead of "wlan0"
//        Modified:
//            - dxPLATFORM_SetMAC()
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "sys_api.h"
#include "wdt_api.h"

extern uint16_t Crc16(uint8_t *pdata, int16_t nbytes, uint16_t crc);

//============================================================================
// UART Wrapper
//============================================================================
void UART_RX_Done_Handler(uint32_t id)
{
    dxUART_t *pdxUart = (dxUART_t *)id;
    signed portBASE_TYPE xHigherPriorityTaskWoken;

    if (pdxUart) {
        serial_t *sobj = (void*)pdxUart->Object;
        pdxUart->RXBytes = sobj->rx_len;

        xSemaphoreGiveFromISR(pdxUart->RXSema, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

dxUART_RET_CODE dxUART_Close(dxUART_t *pdxUart)
{
    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    serial_t *obj = (serial_t *)pdxUart->Object;

    if (obj == NULL) {
        return DXUART_ERROR;
    }

    serial_free(obj);

    dxMemFree(pdxUart->Object);
    dxMemFree(pdxUart);

    return DXUART_SUCCESS;
}

dxUART_RET_CODE dxUART_GetCh(dxUART_t *pdxUart, uint8_t *ch)
{
    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    serial_t *obj = (serial_t *)pdxUart->Object;

    if (obj == NULL) {
        return DXUART_ERROR;
    }

    uint8_t c = serial_getc(obj);

    *ch = c;

    return DXUART_SUCCESS;
}

dxUART_t *dxUART_Open(dxUART_Port_t port, int baudrate, dxUART_Pairty_t parity, int data_bits, int stop_bits, dxUART_FlowControl_t flowctrl, void *option)
{
    dxUART_t *pdxUart = dxMemAlloc(NULL, 1, sizeof(dxUART_t));

    if (pdxUart == NULL) {
        return NULL;
    }

    pdxUart->Object = dxMemAlloc(NULL, 1, sizeof(serial_t));

    if (pdxUart->Object == NULL) {
        return NULL;
    }

    serial_t *obj = pdxUart->Object;

    SerialParity serialparity = ParityNone;
    FlowControl flowcontrol = FlowControlNone;

    PinName tx;
    PinName rx;

    switch (port)
    {
    case DXUART_PORT0:
        tx = DK_UART0_TX;
        rx = DK_UART0_RX;
        break;
    case DXUART_PORT1:
        tx = DK_UART1_TX;
        rx = DK_UART1_RX;
        break;
    case DXUART_PORT2:
        tx = DK_UART2_TX;
        rx = DK_UART2_RX;
        break;
    }

    serial_init(obj, tx, rx);

    serial_baud(obj, baudrate);

    if (parity == DXUART_PARITY_ODD) {
        serialparity = ParityOdd;
    }
    else if (parity == DXUART_PARITY_EVEN) {
        serialparity = ParityEven;
    }

    serial_format(obj, data_bits, serialparity, stop_bits);

    if (flowctrl == DXUART_FLOWCONTROL_RTS) {
        flowcontrol = FlowControlRTS;
    }
    else if (flowctrl == DXUART_FLOWCONTROL_CTS) {
        flowcontrol = FlowControlCTS;
    }
    else if (flowctrl == DXUART_FLOWCONTROL_RTSCTS) {
        flowcontrol = FlowControlRTSCTS;
    }

    serial_set_flow_control(obj, flowcontrol, tx, rx);

    serial_recv_comp_handler(obj, (void*)UART_RX_Done_Handler, (uint32_t)pdxUart);

    pdxUart->RXSema = xSemaphoreCreateBinary();

    return pdxUart;
}

dxUART_RET_CODE dxUART_PutCh(dxUART_t *pdxUart, uint8_t ch)
{
    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    serial_t *obj = (serial_t *)pdxUart->Object;

    if (obj == NULL) {
        return DXUART_ERROR;
    }

    serial_putc(obj, (int)ch);

    return DXUART_SUCCESS;
}

int dxUART_Receive_Blocked(dxUART_t *pdxUart, uint8_t *prxbuf, uint32_t len, uint32_t timeout_ms)
{
    int count = 0;

    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    serial_t *obj = (serial_t *)pdxUart->Object;

    if (obj == NULL ||
        prxbuf == NULL ||
        len == 0) {
        return DXUART_ERROR;
    }

    pdxUart->RXBytes = 0;

    int ret = serial_recv_stream(obj, prxbuf, len);

    if (xSemaphoreTake(pdxUart->RXSema,
                       (TickType_t)timeout_ms/portTICK_RATE_MS) != pdTRUE) {
        pdxUart->RXBytes = serial_recv_stream_abort(obj);
    }

    if (pdxUart->RXBytes > 0) {
        prxbuf[pdxUart->RXBytes] = 0x00;

        //PRINT_BIN(prxbuf, count, " ");
    }

    //G_SYS_DBG_LOG_NV("[%s]Line %d, %s\n", __FUNCTION__, __LINE__, prxbuf);

    if (pdxUart->RXBytes < 0) {
        return DXUART_ERROR;
    }
    else if (pdxUart->RXBytes == 0) {
        return DXUART_TIMEOUT;
    }

    return pdxUart->RXBytes;
}

int dxUART_Send_Blocked(dxUART_t *pdxUart, uint8_t *ptxbuf, uint32_t len)
{
    int count = 0;

    if (pdxUart == NULL) {
        return DXUART_ERROR;
    }

    serial_t *obj = (serial_t *)pdxUart->Object;

    if (obj == NULL ||
        ptxbuf == NULL ||
        len == 0) {
        return DXUART_ERROR;
    }

    while (count < len) {
        uint8_t ch = ptxbuf[count];
        if (dxUART_PutCh(pdxUart, ch) == DXUART_ERROR) {
            break;
        }
        count++;
    }

    //PRINT_BIN(ptxbuf, count, " ");

    if (count < 0) {
        return DXUART_ERROR;
    }
    else if (count == 0) {
        return DXUART_TIMEOUT;
    }

    return count;
}

//============================================================================
// BSP
//============================================================================

/**
 * \return DXBSP_SUCCESS    if BSP initialize success
 *         DXBSP_ERROR      otherwise
 */
dxBSP_RET_CODE dxBSP_Init(void)
{
    dxBSP_RET_CODE result = DXBSP_ERROR;

    dxGPIO_Init(dxGPIOConfig);

    return result;
}

//============================================================================
// GPIO
//============================================================================
static dxGPIO_t* dxGPIOTable = NULL;

static dxGPIO_t *dxGPIO_GetObject(uint32_t PinName)
{
    if (dxGPIOTable == NULL) {
        return NULL;
    }

    dxGPIO_t *pObject = NULL;
    for (dxGPIO_t *p = dxGPIOTable; p->Name != DX_ENDOF_GPIO_CONFIG; p++) {
        if (p != NULL && p->Name == PinName) {
            pObject = p;

            //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
            //                 __FUNCTION__,
            //                 p->Name,
            //                 p->Direction,
            //                 p->Mode);
            break;
        }
    }

    return pObject;
}

dxBSP_RET_CODE dxGPIO_Init(dxGPIO_t* ConfigTable)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    if (dxGPIOTable != NULL) {
        ret = dxGPIO_Uninit(dxGPIOTable);

        if (ret != DXBSP_SUCCESS) {
            return ret;
        }
    }

    dxGPIOTable = ConfigTable;

    PinDirection direction;
    PinMode mode;

    for (dxGPIO_t *pGPIO = dxGPIOTable; pGPIO->Name != DX_ENDOF_GPIO_CONFIG; pGPIO++) {

        if (pGPIO->Name == DX_NOT_ASSIGNED)
            continue;

         pGPIO->Object = dxMemAlloc("BspGpio", 1, sizeof(gpio_t));

        if (pGPIO->Object != NULL) {
            gpio_init(pGPIO->Object, pGPIO->Name);

            if (pGPIO->Direction == DXPIN_INPUT) {
                direction = PIN_INPUT;
            }
            else if (pGPIO->Direction == DXPIN_OUTPUT) {
                direction = PIN_OUTPUT;
            }

            switch (pGPIO->Mode) {
            case DXPIN_PULLNONE:
                mode = PullNone;
                break;
            case DXPIN_PULLUP:
                mode = PullUp;
                break;
            case DXPIN_PULLDOWN:
                mode = PullDown;
                break;
            case DXPIN_OPENDRAIN:
                mode = OpenDrain;
                break;
            default:
                mode = PullDefault;
            }

            //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
            //                 __FUNCTION__,
            //                 pGPIO->Name,
            //                 pGPIO->Direction,
            //                 pGPIO->Mode);

            gpio_dir(pGPIO->Object, direction);
            gpio_mode(pGPIO->Object, mode);
        }

    }

    return DXBSP_SUCCESS;
}

int dxGPIO_Read(uint32_t PinName)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL) {
        return DXBSP_ERROR;
    }

    if (pGPIO->Direction != DXPIN_INPUT) {

        // Change Direction to INPUT
        //pGPIO->Direction = DXPIN_INPUT;
        //gpio_dir(pGPIO->Object, DXPIN_INPUT);

        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL) {
        return DXBSP_ERROR;
    }

    //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
    //                 __FUNCTION__,
    //                 pGPIO->Name,
    //                 pGPIO->Direction,
    //                 pGPIO->Mode);

    int ret = gpio_read(pGPIO->Object);

    return ret;
}

dxBSP_RET_CODE dxGPIO_Write(uint32_t PinName, dxPIN_Level level)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL) {
        return DXBSP_ERROR;
    }

    if (pGPIO->Direction != DXPIN_OUTPUT) {

        // Change Direction to output
        //pGPIO->Direction = DXPIN_OUTPUT;
        //gpio_dir(pGPIO->Object, PIN_OUTPUT);

        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL) {
        return DXBSP_ERROR;
    }

    //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
    //                 __FUNCTION__,
    //                 pGPIO->Name,
    //                 pGPIO->Direction,
    //                 pGPIO->Mode);

    gpio_write(pGPIO->Object, level);

    return DXBSP_SUCCESS;
}

dxBSP_RET_CODE dxGPIO_Set_Direction(uint32_t PinName, dxPIN_Direction Dir)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL)
    {
        return DXBSP_ERROR;
    }

    if (pGPIO->Object == NULL)
    {
        return DXBSP_ERROR;
    }

    //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
    //                 __FUNCTION__,
    //                 pGPIO->Name,
    //                 pGPIO->Direction,
    //                 pGPIO->Mode);

    // Change Direction
    if ((Dir == DXPIN_INPUT) || (Dir == DXPIN_OUTPUT))
    {
        pGPIO->Direction = Dir;
        gpio_dir(pGPIO->Object, Dir);
    }
    else
    {
        return DXBSP_ERROR;
    }

    return DXBSP_SUCCESS;
}

int dxGPIO_Get_Direction(uint32_t PinName)
{
    dxGPIO_t *pGPIO = dxGPIO_GetObject(PinName);

    if (pGPIO == NULL ||
        pGPIO->Object == NULL)
    {
        return DXBSP_ERROR;
    }
    
    //G_SYS_DBG_LOG_NV("[%s]GPIO Name: %X, Dir: %d, Mode: %d\n",
    //                 __FUNCTION__,
    //                 pGPIO->Name,
    //                 pGPIO->Direction,
    //                 pGPIO->Mode);

    return pGPIO->Direction;    
}

dxBSP_RET_CODE dxGPIO_Uninit(dxGPIO_t* ConfigTable)
{
    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    for (dxGPIO_t *p = ConfigTable; p->Name != DX_ENDOF_GPIO_CONFIG; p++) {
        if (p != NULL && p->Object != NULL) {
            dxMemFree(p->Object);
            p->Object = NULL;

            //G_SYS_DBG_LOG_NV("[%s]Line %d\n", __FUNCTION__, __LINE__);
        }
    }

    return DXBSP_SUCCESS;
}

//============================================================================
// PWM
//============================================================================
static dxPWM_t* dxPWMTable = NULL;

static dxPWM_t *dxPWM_GetObject(uint32_t PinName)
{
    if (dxPWMTable == NULL) {
        return NULL;
    }

    dxPWM_t *pObject = NULL;
    for (dxPWM_t *p = dxPWMTable; p->Name != DX_ENDOF_PWM_CONFIG; p++) {
        if (p != NULL && p->Name == PinName) {
            pObject = p;

            //G_SYS_DBG_LOG_NV("[%s]PWM Name: %X\n",
            //                 __FUNCTION__,
            //                 p->Name);
            break;
        }
    }

    return pObject;
}


dxBSP_RET_CODE dxPWM_Init(dxPWM_t* ConfigTable)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    if (dxPWMTable != NULL) {
        ret = dxPWM_Uninit(dxPWMTable);

        if (ret != DXBSP_SUCCESS) {
            return ret;
        }
    }

    dxPWMTable = ConfigTable;

    for (dxPWM_t *pPWM = dxPWMTable; pPWM->Name != DX_ENDOF_PWM_CONFIG; pPWM++) {

        if (pPWM->Name == DX_NOT_ASSIGNED)
            continue;

        pPWM->Object = dxMemAlloc("BspPwm", 1, sizeof(pwmout_t));

        if (pPWM->Object != NULL) {
            pwmout_init(pPWM->Object, pPWM->Name);
        }

    }

    return DXBSP_SUCCESS;
}

dxBSP_RET_CODE dxPWM_Uninit(dxPWM_t* ConfigTable)
{
    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    for (dxPWM_t *p = ConfigTable; p->Name != DX_ENDOF_PWM_CONFIG; p++) {
        if (p != NULL && p->Object != NULL) {
            dxMemFree(p->Object);
            p->Object = NULL;

            //G_SYS_DBG_LOG_NV("[%s]Line %d\n", __FUNCTION__, __LINE__);
        }
    }

    return DXBSP_SUCCESS;
}

dxBSP_RET_CODE dxPWM_Write(uint32_t PinName, float PwmPeriod)
{
    dxPWM_t *pPWM = dxPWM_GetObject(PinName);

    if (pPWM == NULL) {
        return DXBSP_ERROR;
    }

    if (pPWM->Object == NULL) {
        return DXBSP_ERROR;
    }

    //G_SYS_DBG_LOG_NV("[%s]PWM Name: %X, Dir: %d, Mode: %d\n",
    //                 __FUNCTION__,
    //                 pPWM->Name);

    if(PwmPeriod != DX_PWM_NOTE_NONE)
    {
        pwmout_period(pPWM->Object, PwmPeriod);            
        pwmout_pulsewidth(pPWM->Object, (PwmPeriod / 2));
    }
    else
    {
        pwmout_period(pPWM->Object, 1);            
        pwmout_pulsewidth(pPWM->Object, 0);
    }


    return DXBSP_SUCCESS;
}

//============================================================================
// I2C
//============================================================================
static dxI2C_t* dxI2CTable = NULL;

static dxI2C_t *dxI2C_GetObject(uint32_t Pin_I2C_SDA)
{
    if (dxI2CTable == NULL) {
        return NULL;
    }

    dxI2C_t *pObject = NULL;
    for (dxI2C_t *p = dxI2CTable; p->Pin_I2C_SDA != DX_ENDOF_I2C_CONFIG; p++) {
        if (p != NULL && p->Pin_I2C_SDA == Pin_I2C_SDA) {
            pObject = p;

            break;
        }
    }

    return pObject;
}


dxBSP_RET_CODE dxI2C_Init(dxI2C_t* ConfigTable)
{
    dxBSP_RET_CODE ret = DXBSP_ERROR;

    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    if (dxI2CTable != NULL) {
        ret = dxI2C_Uninit(dxI2CTable);

        if (ret != DXBSP_SUCCESS) {
            return ret;
        }
    }

    dxI2CTable = ConfigTable;

    for (dxI2C_t *pI2C = dxI2CTable; pI2C->Pin_I2C_SDA != DX_ENDOF_I2C_CONFIG; pI2C++) {

        if (pI2C->Pin_I2C_SDA == DX_NOT_ASSIGNED)
            continue;

        pI2C->Object = dxMemAlloc("BspI2C", 1, sizeof(i2c_t));

        if (pI2C->Object != NULL) {
            pI2C->hMutex = dxMutexCreate();
        
            if (pI2C->hMutex == NULL)
            {
                dxMemFree(pI2C);
                pI2C = NULL;
                continue;
            }
            else
            {
                i2c_init(pI2C->Object, pI2C->Pin_I2C_SDA, pI2C->Pin_I2C_SCL);
                i2c_frequency(pI2C->Object, pI2C->I2C_BUS_CLK);
        
                if (pI2C->restart_enable)
                {
                    i2c_restart_enable(pI2C->Object);
                }
            }
        }
    }

    return DXBSP_SUCCESS;
}

dxBSP_RET_CODE dxI2C_Uninit(dxI2C_t* ConfigTable)
{
    if (ConfigTable == NULL) {
        return DXBSP_ERROR;
    }

    for (dxI2C_t *p = ConfigTable; p->Pin_I2C_SDA != DX_ENDOF_I2C_CONFIG; p++) {
        if (p != NULL && p->Object != NULL) {
            dxI2C_Reset(p);
        }
    }
    
    dxI2CTable = NULL;

    return DXBSP_SUCCESS;
}

int dxI2C_Master_Write(uint32_t Pin_I2C_SDA,
                       int address,
                       const char *data,
                       int length,
                       dxBOOL stop)
{
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);
    
    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        address <= 0 ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL ||
        data == NULL||
        length <= 0)
    {
        return 0;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_write(pHandle->Object, address, data, length, stop);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

int dxI2C_Master_Read(uint32_t Pin_I2C_SDA,
                      int address,
                      char *data,
                      int length,
                      dxBOOL stop)
{
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        address <= 0 ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL ||
        data == NULL||
        length <= 0)
    {
        return 0;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_read(pHandle->Object, address, data, length, stop);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

void dxI2C_Reset(dxI2C_t *pHandle)
{
    if (pHandle == NULL ||
        &(pHandle->Object) == NULL  ||
        pHandle->hMutex == NULL)
    {
        return;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    i2c_reset(pHandle->Object);
    dxMemFree(pHandle->Object);
    pHandle->Object = NULL;

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);
    //dxMutexDelete(pHandle->hMutex);
    vQueueDelete(pHandle->hMutex);

    return;
}

uint8_t dxI2C_Master_U8_Read(uint32_t Pin_I2C_SDA,
                             int address,
                             dxBOOL last)
{
//    dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);
    
    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        address <= 0 ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL)
    {
        return 0;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    uint8_t I2C_RET = i2c_byte_read(pHandle->Object, last);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

int dxI2C_Master_U8_Write(uint32_t Pin_I2C_SDA,
                          int address,
                          uint8_t data)
{
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        address <= 0 ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL ||
        data <= 0)
    {
        return DXBSP_ERROR;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_byte_write(pHandle->Object, data);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

void dxI2C_Slave_Mode(uint32_t Pin_I2C_SDA,
                      dxBOOL enable_slave)
{
    //dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL)
    {
        return;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    i2c_slave_mode(pHandle->Object, enable_slave);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);
}

dxI2C_RET_CODE dxI2C_Slave_ReceiveStatus(uint32_t Pin_I2C_SDA)
{
    //dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL)
    {
        return DXBSP_ERROR;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_slave_receive(pHandle->Object);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);
    
    if (I2C_RET == 0)
    {
        return DXI2C_NO_DATA;
    }
    return (dxI2C_RET_CODE)I2C_RET;
}

int dxI2C_Slave_Read(uint32_t Pin_I2C_SDA,
                     char *data,
                     int length)
{
    //dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL ||
        data == NULL||
        length <= 0)
    {
        return 0;
    }
    
    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_slave_read(pHandle->Object, data, length);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

int dxI2C_Slave_Write(uint32_t Pin_I2C_SDA,
                      const char *data,
                      int length)
{
    //dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL ||
        data == NULL||
        length <= 0)
    {
        return 0;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    int I2C_RET = i2c_slave_write(pHandle->Object, data, length);
    wait_us(10);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);

    return I2C_RET;
}

void dxI2C_Slave_Address(uint32_t Pin_I2C_SDA,
                         int idx,
                         uint32_t address,
                         uint32_t mask)
{
    //dxI2CAccessHandle_t *pHandle = (dxI2CAccessHandle_t *)handle;
    dxI2C_t *pHandle = dxI2C_GetObject(Pin_I2C_SDA);

    if (pHandle == NULL ||
        pHandle->Object == NULL  ||
        address <= 0 ||
        pHandle->Pin_I2C_SDA <= 0 ||
        pHandle->Pin_I2C_SCL <= 0 ||
        pHandle->I2C_BUS_CLK <= 0 ||
        pHandle->hMutex == NULL)
    {
        return;
    }

    //dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);
    xQueueGenericReceive(pHandle->hMutex, NULL, (DXOS_WAIT_FOREVER/portTICK_PERIOD_MS), pdFALSE);

    i2c_slave_address(pHandle->Object, idx, address, mask);

    //dxMutexGive(pHandle->hMutex);
    xQueueGenericSend(pHandle->hMutex, NULL, semGIVE_BLOCK_TIME, queueSEND_TO_BACK);
}


//============================================================================
// ADC
//============================================================================
uint16_t dxADC_Read(PinName AdcPin)
{
    uint16_t adcData = 0;
    analogin_t adc;
    analogin_init(&adc, AdcPin);
    
    {
        adcData = analogin_read_u16(&adc);
        
//        G_SYS_DBG_LOG_V("[%s]ADC read data = 0x%X\n", __FUNCTION__, adcData);
    }
    analogin_deinit(&adc);
    
    return adcData;
}

#define OFFSET      0x43B
#define GAIN_DIV    0x211
#define AD2MV(ad,offset,gain)   (((ad/16)-offset)*1000/gain)

int32_t dxADC_ReadMvValue(PinName AdcPin)
{
    uint16_t adcData = 0;
    uint16_t readCount = 0;
    
    while ((adcData == 0) && (readCount < 10))
    {
        adcData = dxADC_Read(AdcPin);
        readCount++;
    }
    
    return AD2MV(adcData, OFFSET, GAIN_DIV);
}


//============================================================================
// Platform System level
//============================================================================
void dxPLATFORM_Reset(void)
{
    sys_reset();    // Use sys_reset() instead of ota_platform_reset()
}

extern void *dxMalloc(size_t size);
extern void dxFree(void *pv);

void dxPLATFORM_Init(void)
{
    // Should be called if SSL/AES are required
    memory_set_own(dxMalloc, dxFree);
}

dxBSP_RET_CODE dxPLATFORM_SetMAC(DeviceMac_t *pmac, DeviceMac_t *prmac)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;
    int n = 0;
    struct iwreq iwr;

    if (pmac == NULL)
    {
        return DXBSP_ERROR;
    }

    // Maximum command length. Can be 2600 or 1024 (Check wifi_util.c)
    // We only need a small buffer here to handle MAC address read/write
    size_t nBufferSize = 64;
    uint8_t *pBuffer = dxMemAlloc(NULL, 1, nBufferSize);

    if (pBuffer == NULL)
    {
        return DXBSP_ERROR;
    }

    // Write MAC
    sprintf(pBuffer,
            "write_mac %02x%02x%02x%02x%02x%02x",
            pmac->MacAddr[0],
            pmac->MacAddr[1],
            pmac->MacAddr[2],
            pmac->MacAddr[5],
            pmac->MacAddr[6],
            pmac->MacAddr[7]);

	memset(&iwr, 0, sizeof(iwr));
	iwr.u.data.pointer = pBuffer;
	iwr.u.data.length = nBufferSize;
	iwr.u.data.flags = 0;

    ret = iw_ioctl(WLAN0_NAME/*"wlan0"*/, SIOCDEVPRIVATE, &iwr);
    if (ret < 0)
    {
        dxMemFree(pBuffer);
        return DXBSP_ERROR;
    }

    if (prmac)
    {
        // Read MAC
        sprintf(pBuffer, "read_mac");

    	memset(&iwr, 0, sizeof(iwr));
    	iwr.u.data.pointer = pBuffer;
    	iwr.u.data.length = nBufferSize;
    	iwr.u.data.flags = 0;

        ret = iw_ioctl(WLAN0_NAME/*"wlan0"*/, SIOCDEVPRIVATE, &iwr);
        if (ret < 0)
        {
            dxMemFree(pBuffer);
            return DXBSP_ERROR;
        }

        n = sscanf(pBuffer,
                   "%x:%x:%x:%x:%x:%x",
                   (int *)prmac->MacAddr[0],
                   (int *)prmac->MacAddr[1],
                   (int *)prmac->MacAddr[2],
                   (int *)prmac->MacAddr[5],
                   (int *)prmac->MacAddr[6],
                   (int *)prmac->MacAddr[7]);

        if (n != 6)
        {
            dxMemFree(pBuffer);
            return DXBSP_ERROR;
        }

        prmac->MacAddr[3] = 0x0FF;
        prmac->MacAddr[4] = 0x0FF;

        // We don't need to verify here.
        // Just return the MAC address and let App to check
        /**
        if (memcmp(prmac->MacAddr, pmac->MacAddr, sizeof(prmac->MacAddr)) != 0)
        {
            dxMemFree(pBuffer);
            return DXBSP_ERROR;
        }*/
    }

    dxMemFree(pBuffer);

    return DXBSP_SUCCESS;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogInit(uint32_t TimeOut)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;
    
    if (TimeOut)
    {
        watchdog_init(TimeOut);
    }
    else
    {
        watchdog_init(10000);
    }
    
    return ret;
}

dxBSP_RET_CODE dxPLATFORM_WatchDogStart(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;
    
    watchdog_start();
    
    return ret;
}



dxBSP_RET_CODE dxPLATFORM_WatchDogRefresh(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;
    
    watchdog_refresh();
    
    return ret;
}


dxBSP_RET_CODE dxPLATFORM_WatchDogClose(void)
{
    dxBSP_RET_CODE ret = DXBSP_SUCCESS;
    
    return ret;
}

//============================================================================
// FLASH API
//============================================================================

static dxFlashHandle_t *dxFlashHandle = NULL;

extern SPIC_INIT_PARA SpicInitParaAllClk[3][CPU_CLK_TYPE_NO];

static void flash_init(flash_t *obj)
{
    SPI_FLASH_PIN_FCTRL(ON);

    if (!SpicFlashInitRtl8195A(SpicOneBitMode)) {
        G_SYS_DBG_LOG_V("SPI Init Fail!!!!!!\n");
        HAL_WRITE32(SYSTEM_CTRL_BASE, REG_SYS_DSTBY_INFO3, HAL_READ32(SYSTEM_CTRL_BASE, REG_SYS_DSTBY_INFO3)|0xf);
    }
    else {
    }
    obj->SpicInitPara.flashtype = SpicInitParaAllClk[SpicOneBitMode][0].flashtype;
}


dxOS_RET_CODE dxFlash_ResourceTake(dxFlashHandle_t flash)
{
    dxSPIaccess_ResourceTake(); //WORKAROUND ONLY - See header for detail
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_ResourceGive(dxFlashHandle_t flash)
{
    dxSPIaccess_ResourceGive(); //WORKAROUND ONLY - See header for detail
    return DXOS_SUCCESS;
}

dxFlashHandle_t dxFlash_Init(void)
{
    if (dxFlashHandle == NULL) {

        dxFlashHandle = dxMemAlloc(NULL, 1, sizeof(flash_t));

        if (dxFlashHandle == NULL) {
            return NULL;
        }

        dxFlash_ResourceTake(dxFlashHandle);
        flash_init((flash_t *)dxFlashHandle);
        dxFlash_ResourceGive(dxFlashHandle);
        
    }

    return dxFlashHandle;

}

dxOS_RET_CODE dxFlash_Erase_Sector(dxFlashHandle_t flash, uint32_t address)
{

    flash_t *rtkflash = (flash_t *)flash;

    if (flash == NULL) {
        return DXOS_ERROR;
    }

    address = (address / DEFAULT_FLASH_SECTOR_ERASE_SIZE) * DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    dxFlash_ResourceTake(flash);
    flash_init(rtkflash);
    SpicSectorEraseFlashRtl8195A(SPI_FLASH_BASE + address);
    SpicDisableRtl8195A();
    dxFlash_ResourceGive(flash);

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Erase_SectorWithSize(dxFlashHandle_t flash, uint32_t address, uint32_t size)
{
    uint8_t i = 0;

    flash_t *rtkflash = (flash_t *)flash;

    if (flash == NULL) {
        return DXOS_ERROR;
    }

    address = (address / DEFAULT_FLASH_SECTOR_ERASE_SIZE) * DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    for(i = (size / DEFAULT_FLASH_SECTOR_ERASE_SIZE); i > 0; i--)
    {
        dxFlash_ResourceTake(flash);        
        flash_init(rtkflash);
        SpicSectorEraseFlashRtl8195A(SPI_FLASH_BASE + address);
        SpicDisableRtl8195A();
        dxFlash_ResourceGive(flash);

        address += DEFAULT_FLASH_SECTOR_ERASE_SIZE;
    }

    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Read(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
    flash_t *rtkflash = (flash_t *)flash;

    int i = 0;

    if (flash == NULL) {
        return DXOS_ERROR;
    }
    
    dxFlash_ResourceTake(flash);
    
    flash_init(rtkflash);

    SpicWaitWipDoneRefinedRtl8195A(rtkflash->SpicInitPara);

    for (int i = 0; i < len; i++) {
        data[i] = HAL_READ8(SPI_FLASH_BASE, address + i);
    }

    SpicDisableRtl8195A();
    dxFlash_ResourceGive(flash);
    
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Read_Word(dxFlashHandle_t flash, uint32_t address, uint32_t *data)
{
    flash_t *rtkflash = (flash_t *)flash;

    if (flash == NULL) {
        return DXOS_ERROR;
    }

    dxFlash_ResourceTake(flash);
    // Read Word
    flash_init(rtkflash);

    // Wait flash busy done (wip=0)
    SpicWaitWipDoneRefinedRtl8195A(rtkflash->SpicInitPara);

    *data = HAL_READ32(SPI_FLASH_BASE, address);

    SpicDisableRtl8195A();

    dxFlash_ResourceGive(flash);
    
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Write(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
    flash_t *rtkflash = (flash_t *)flash;

    int i = 0;

    if (flash == NULL) {
        return DXOS_ERROR;
    }

       
    dxFlash_ResourceTake(flash);
    flash_init(rtkflash);

    for (int i = 0; i < len; i++) {
        HAL_WRITE8(SPI_FLASH_BASE, address + i, data[i]);

        SpicWaitBusyDoneRtl8195A();
        // Wait flash busy done (wip=0)
        if (rtkflash->SpicInitPara.flashtype == FLASH_MICRON) {
            SpicWaitOperationDoneRtl8195A(rtkflash->SpicInitPara);
        }
        else {
            SpicWaitWipDoneRefinedRtl8195A(rtkflash->SpicInitPara);
        }
    }

    SpicDisableRtl8195A();
    dxFlash_ResourceGive(flash);
    
    
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Write_Word(dxFlashHandle_t flash, uint32_t address, uint32_t data)
{
    flash_t *rtkflash = (flash_t *)flash;

    if (flash == NULL) {
        return DXOS_ERROR;
    }

    dxFlash_ResourceTake(flash);
    
    // Disable write protection
    u8 flashtype = 0;

    flash_init(rtkflash);

    flashtype = rtkflash->SpicInitPara.flashtype;

    //Write word
    HAL_WRITE32(SPI_FLASH_BASE, address, data);

    // Wait spic busy done
    SpicWaitBusyDoneRtl8195A();

    // Wait flash busy done (wip=0)
    if (flashtype == FLASH_MICRON) {
        SpicWaitOperationDoneRtl8195A(rtkflash->SpicInitPara);
    }
    else {
        SpicWaitWipDoneRefinedRtl8195A(rtkflash->SpicInitPara);
    }

    SpicDisableRtl8195A();
    // Enable write protection

    dxFlash_ResourceGive(flash);
    
    
    return DXOS_SUCCESS;
}

dxOS_RET_CODE dxFlash_Uninit(dxFlashHandle_t flash)
{
    if (flash) {
        dxMemFree(flash);
        flash = NULL;
    }

    return DXOS_SUCCESS;
}

//============================================================================
// MAIN IMAGE UPDATE API
//============================================================================
#define OFFSET_DATA                     FLASH_SYSTEM_DATA_ADDR
#define BACKUP_SECTOR                   (FLASH_SYSTEM_DATA_ADDR - 0x1000)

#define DX_MAINIMAGE_SIG0               0x35393138
#define DX_MAINIMAGE_SIG1               0x31313738

/**
 * \brief Structure to store MD5 checksum result
 */
typedef struct
{
    int             nTotalReceiveBytes;     // File size in bytes
    int             nDownloadBytes;         // Current received bytes
    uint8_t         *pszFileMD5Sum;         // The MD5 checksum result of file, in hexadecimal string format.
                                            // For example, 0x41 => "41" (character " is not included)

    md5_context     *pmd5cx;                // MD5 checksum context
    uint8_t         *pmd5sum;               // Current MD5 checksum data

    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
} dxMD5Info_t;

static dxMD5Info_t dxMD5Info = NULL;

static dxMAINIMAGE_RET_CODE dxMainImage_WriteBootAddr(dxFlashHandle_t flash, uint32_t address)
{
    flash_t *rtkflash = (flash_t *)flash;
    int i = 0;
    uint32_t data = 0;

    if (flash == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    // Erase backup sector
    dxFlash_Erase_Sector(rtkflash, BACKUP_SECTOR);

    // Backup system data to backup sector
    for (i = 0; i < 0x1000; i+= 4) {

        dxFlash_Read_Word(rtkflash, OFFSET_DATA + i, &data);

        if (i == 0) {

            data = address;
        }

        dxFlash_Write_Word(rtkflash, BACKUP_SECTOR + i, data);
    }

    // Erase system data
    dxFlash_Erase_Sector(rtkflash, OFFSET_DATA);

    // Write data back to system data
    for (i = 0; i < 0x1000; i+= 4) {

        dxFlash_Read_Word(rtkflash, BACKUP_SECTOR + i, &data);
        dxFlash_Write_Word(rtkflash, OFFSET_DATA + i, data);
    }

    // Erase backup sector
    dxFlash_Erase_Sector(rtkflash, BACKUP_SECTOR);

    return DXMAINIMAGE_SUCCESS;
}

static dxMAINIMAGE_RET_CODE dxMainImage_WriteBootMark(dxFlashHandle_t flash,
                                                      uint32_t address,
                                                      uint32_t sign0,
                                                      uint32_t sign1)
{
    uint32_t data = 0;
    int i = 0;
    dxMAINIMAGE_RET_CODE ret = DXMAINIMAGE_ERROR;

    if (flash == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    ret = dxMainImage_WriteBootAddr(flash, address);

    if (ret != DXMAINIMAGE_SUCCESS) {
        return ret;
    }

    // Set signature in New Image 2 addr + 8 and + 12
    uint32_t sig_readback0 = 0;
    uint32_t sig_readback1 = 0;

    dxFlash_Write_Word(flash, address +  8, sign0);
    dxFlash_Write_Word(flash, address + 12, sign1);
    dxFlash_Read_Word(flash,  address +  8, &sig_readback0);
    dxFlash_Read_Word(flash,  address + 12, &sig_readback1);

    G_SYS_DBG_LOG_V("Firmware Signature %08X, %08X\n", sig_readback0, sig_readback1);

    if ((sign0 == sig_readback0) &&
        (sign1 == sig_readback1)) {
        ret = DXMAINIMAGE_SUCCESS;
    }
    else {
        ret = DXMAINIMAGE_ERROR;
    }

    return ret;
}

static void dxMainImage_EraseArea(dxFlashHandle_t dxFlash, uint32_t address, uint32_t size)
{
    int i = 0;

    if (dxFlash == NULL ||
        address == 0 ||             // Should not erase address 0
        size == 0) {
        return ;
    }

    int sectors = (size + DEFAULT_FLASH_SECTOR_ERASE_SIZE - 1) / DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    G_SYS_DBG_LOG_V("Erase address 0x%X, len: %d bytes, total sectors: %d\n", address, size, sectors);

    for (i = 0; i < sectors; i++) {
        dxFlash_Erase_Sector(dxFlash, address + i * DEFAULT_FLASH_SECTOR_ERASE_SIZE);
    }
}

static dxMAINIMAGE_RET_CODE dxMainImage_WriteVerify(dxFlashHandle_t dxFlash, uint32_t address, uint32_t length, uint8_t *pData)
{
    if (dxFlash == NULL ||
        length == 0 ||
        pData == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    uint8_t *pReadBuffer = dxMemAlloc(NULL, 1, length);

    if (pReadBuffer == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    memset(pReadBuffer, 0x00, length);

    dxOS_RET_CODE ret = dxFlash_Read(dxFlash, address, length, pReadBuffer);

    if (ret != DXOS_SUCCESS) {

        dxMemFree(pReadBuffer);
        return DXMAINIMAGE_VERIFY_ERROR;
    }

    int r = memcmp((void *)pReadBuffer, pData, length);

    dxMemFree(pReadBuffer);

    if (r != 0) {
        return DXMAINIMAGE_VERIFY_ERROR;
    }

    return DXMAINIMAGE_SUCCESS;
}

static dxMAINIMAGE_RET_CODE dxMainImage_MD5IsEqual(void)
{
    if (dxMD5Info.pmd5sum == NULL ||
        dxMD5Info.pszFileMD5Sum == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    uint32_t len = 16; //strlen(dxMD5Info.pmd5sum); //MD5 is fixed at length 16, shall not use strlen to find its length
    if (len * 2 != strlen(dxMD5Info.pszFileMD5Sum)) {
        return DXMAINIMAGE_ERROR;
    }

    dxMAINIMAGE_RET_CODE r = DXMAINIMAGE_SUCCESS;

    uint8_t i = 0;
    uint8_t ch = 0;
    uint8_t d = 0;

    while (i < len) {

        ch = (dxMD5Info.pmd5sum[i] >> 4) & 0x0F;
        ch += 0x30;

        if (ch > '9') {
            ch += 7;
        }

        d = dxMD5Info.pszFileMD5Sum[i * 2];

        if (d >= 'a' && d <= 'z') {
            d = d - 'a' + 'A';
        }

        if (ch != d) {
            r = DXMAINIMAGE_ERROR;
            break;
        }

        ch = (dxMD5Info.pmd5sum[i]) & 0x0F;
        ch += 0x30;

        if (ch > '9') {
            ch += 7;
        }

        d = dxMD5Info.pszFileMD5Sum[i * 2 + 1];

        if (d >= 'a' && d <= 'z') {
            d = d - 'a' + 'A';
        }

        if (ch != d) {
            r = DXMAINIMAGE_ERROR;
            break;
        }

        i++;
    }

    return r;
}

dxMAINIMAGE_RET_CODE dxMainImage_Init(void)
{
    dxMD5Info.hMutex = dxMutexCreate();

    if (dxMD5Info.hMutex == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    dxMD5Info.hFlash = dxFlash_Init();

    if (dxMD5Info.hFlash == NULL) {

        dxMutexDelete(dxMD5Info.hMutex);
        dxMD5Info.hMutex = NULL;

        return DXMAINIMAGE_ERROR;
    }

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE dxMainImage_Start(uint8_t *pszMD5Sum, uint32_t nImageSize)
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    if (dxMD5Info.hMutex == NULL ||
        dxMD5Info.hFlash == NULL ||
        pszMD5Sum == NULL ||
        strlen(pszMD5Sum) != sizeof(dxMD5Info.pmd5cx->state) * 2 ||
        nImageSize == 0) {
        return DXMAINIMAGE_ERROR;
    }

    if (nImageSize > DX_SYSINFO_FLASH_UPDATE_IMG_SIZE) {
        return DXMAINIMAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(dxMD5Info.hMutex, DXOS_WAIT_FOREVER);

    dxMD5Info.nTotalReceiveBytes = nImageSize;
    dxMD5Info.nDownloadBytes = 0;

    if (dxMD5Info.pszFileMD5Sum) {

        dxMemFree(dxMD5Info.pszFileMD5Sum);
        dxMD5Info.pszFileMD5Sum = NULL;
    }

    if (dxMD5Info.pmd5cx) {

        dxMemFree(dxMD5Info.pmd5cx);
        dxMD5Info.pmd5cx = NULL;
    }

    if (dxMD5Info.pmd5sum) {

        dxMemFree(dxMD5Info.pmd5sum);
        dxMD5Info.pmd5sum = NULL;
    }

    dxMD5Info.pszFileMD5Sum = (uint8_t *)dxMemAlloc(NULL, 1, sizeof(dxMD5Info.pmd5cx->state) * 2 + 1); // Additional NULL character is needed.

    if (dxMD5Info.pszFileMD5Sum  == NULL) {

        dxMutexGive(dxMD5Info.hMutex);
        return DXMAINIMAGE_ERROR;
    }

    memcpy(dxMD5Info.pszFileMD5Sum, pszMD5Sum, strlen(pszMD5Sum));

    dxMD5Info.pmd5cx = (md5_context *)dxMemAlloc(NULL, 1, sizeof(md5_context));

    if (dxMD5Info.pmd5cx  == NULL) {

        dxMutexGive(dxMD5Info.hMutex);
        return DXMAINIMAGE_ERROR;
    }

    /**
     * sizeof(dxMD5Info.pmd5cx->state) should be 16 bytes,
     * due to declare of "uint32_t state[4]"
     */
    dxMD5Info.pmd5sum = (uint8_t *)dxMemAlloc(NULL, 1, sizeof(dxMD5Info.pmd5cx->state) + 1);

    if (dxMD5Info.pmd5sum == NULL) {

        dxMutexGive(dxMD5Info.hMutex);
        return DXMAINIMAGE_ERROR;
    }

    md5_starts(dxMD5Info.pmd5cx);

    dxMainImage_EraseArea(dxMD5Info.hFlash,
                          DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR,
                          nImageSize);

    dxMutexGive(dxMD5Info.hMutex);

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE dxMainImage_Write(uint8_t *pImage, uint32_t nImageLen)
{
    if (dxMD5Info.hFlash == NULL ||
        dxMD5Info.hMutex == NULL ||
        dxMD5Info.pmd5cx == NULL ||
        dxMD5Info.pmd5sum == NULL ||
        dxMD5Info.nTotalReceiveBytes <= 0 ||
        dxMD5Info.nDownloadBytes < 0 ||
        pImage == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    if (dxMD5Info.nDownloadBytes + nImageLen > DX_SYSINFO_FLASH_UPDATE_IMG_SIZE) {
        return DXMAINIMAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(dxMD5Info.hMutex, DXOS_WAIT_FOREVER);

    uint32_t addr = DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR + dxMD5Info.nDownloadBytes;

    dxOS_RET_CODE ret = dxFlash_Write(dxMD5Info.hFlash,
                                      addr,
                                      nImageLen,
                                      pImage);

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(dxMD5Info.hMutex);
        return DXMAINIMAGE_ERROR;
    }

    dxMAINIMAGE_RET_CODE r = dxMainImage_WriteVerify(dxMD5Info.hFlash,
                                                     addr,
                                                     nImageLen,
                                                     pImage);

    if (r != DXMAINIMAGE_SUCCESS) {

        dxMutexGive(dxMD5Info.hMutex);
        return r;
    }

    md5_update(dxMD5Info.pmd5cx, pImage, nImageLen);

    dxMD5Info.nDownloadBytes += nImageLen;

    r = DXMAINIMAGE_SUCCESS;

    if (dxMD5Info.nDownloadBytes >= dxMD5Info.nTotalReceiveBytes) {

        // MD5 checksum verify
        md5_finish(dxMD5Info.pmd5cx, dxMD5Info.pmd5sum);

        if (dxMainImage_MD5IsEqual() == DXMAINIMAGE_SUCCESS) {

            r = DXMAINIMAGE_COMPLETE;
        }
        else {
            r = DXMAINIMAGE_ERROR;
        }
    }

    dxMutexGive(dxMD5Info.hMutex);

    return r;
}

dxMAINIMAGE_RET_CODE dxMainImage_UpdateReboot(dxBOOL WithSystemSelfReboot)
{
    if (dxMD5Info.hFlash == NULL ||
        dxMD5Info.hMutex == NULL) {
        return DXMAINIMAGE_ERROR;
    }

    dxMutexTake(dxMD5Info.hMutex, DXOS_WAIT_FOREVER);

    dxMAINIMAGE_RET_CODE ret = dxMainImage_WriteBootMark(dxMD5Info.hFlash,
                                                         DX_SYSINFO_FLASH_UPDATE_IMG_START_ADDR,
                                                         DX_MAINIMAGE_SIG0,
                                                         DX_MAINIMAGE_SIG1);

    dxMutexGive(dxMD5Info.hMutex);

    if (ret != DXMAINIMAGE_SUCCESS) {
        return ret;
    }

    if (WithSystemSelfReboot == dxTRUE)
    {
        dxPLATFORM_Reset();    
    }

    return DXMAINIMAGE_SUCCESS;
}

dxMAINIMAGE_RET_CODE dxMainImage_Stop(void)
{
    //Nothing to free

    return DXMAINIMAGE_SUCCESS;
}
//============================================================================
// SUB IMAGE UPDATE API
//============================================================================
#define DX_SUBIMAGE_SIG0               0x35393138
#define DX_SUBIMAGE_SIG1               0x31313738
#define DX_SUBIMAGE_MARK               0xF1E2D3C4B5A69788
#define DX_SUBIMAGE_MAX_DATA_SIZE      (DX_SUBIMG_FLASH_SIZE - sizeof(dxSubImageHeaderAccessCtrl_t) - sizeof(dxSubImgData_t))

//============================================================================
//    Header
//+-----------------------------+---------------------+--------------------+
//| Tag                         | ( 8 Bytes )         | dxSubImageHeader_t |
//+-----------------------------+---------------------+                    |
//| DeviceType                  | ( 2 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderVersion               | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| HeaderSize                  | ( 1 Byte  )         |                    |
//+-----------------------------+---------------------+                    |
//| ImageSize                   | ( 4 Bytes )         |                    |
//+-----------------------------+---------------------+                    |
//| SubImageVersion             | ( 16 Bytes )        |                    |
//+-----------------------------+---------------------+--------------------+
//============================================================================
typedef struct
{
    uint64_t        Tag;                    // Must be DX_SUBIMAGE_MARK (check dxBSP.c)
    uint16_t        DeviceType;             // Distinguish this sub image for which device
    uint8_t         HeaderVer;              // For header extensions, start form 1
    uint8_t         HeaderSize;             // For simplify code, should be sizeof(dxSubImageHeaderAccessCtrl_t)
    uint32_t        ImageSize;              // SubImg from server must >0, should be less than DX_SUBIMG_FLASH_SIZE (except header)
    uint8_t         SubImgVer[16];          // Sub image version from supplier, use ASCII coding
} dxSubImageHeaderAccessCtrl_t;             // sizeof(dxSubImageHeaderAccessCtrl_t) should be 32 bytes

typedef struct
{
    uint32_t         nTotalReceiveBytes;    // File size in bytes
    uint32_t         nDownloadBytes;        // Current received bytes
    uint8_t         *pszFileMD5Sum;         // The MD5 checksum result of file, in hexadecimal string format.
                                            // For example, 0x41 => "41" (character " is not included)

    md5_context     *pmd5cx;                // MD5 checksum context
    uint8_t         *pmd5sum;               // Current MD5 checksum data

    uint8_t         nSubImgBlockID;         // Reserved for expandability

    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
} dxSubImgInfoAccessCtrl_t;

static dxSubImgInfoAccessCtrl_t *dxSubImgInfoAccessCtrl[1] = NULL;

#if DX_SUBIMG_FLASH_SIZE                // SubImage only support on flash at least 4MB

static dxOS_RET_CODE dxSubImage_WriteHeader(dxFlashHandle_t hFlash, uint32_t addr, uint32_t nImageLen)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    dxSubImageHeaderAccessCtrl_t dxHeader;

    if (hFlash == NULL)
    {
        return DXOS_ERROR;
    }

    memset(&dxHeader, 0x00, sizeof(dxHeader));

    dxHeader.Tag = DX_SUBIMAGE_MARK;
    dxHeader.DeviceType = 0x00;
    dxHeader.HeaderVer = 0x01;
    dxHeader.HeaderSize = sizeof(dxSubImageHeaderAccessCtrl_t);
    dxHeader.ImageSize = nImageLen;
    memset(dxHeader.SubImgVer, 0x00, sizeof(dxHeader.SubImgVer));

    ret = dxFlash_Write(hFlash,
                        addr,
                        sizeof(dxHeader),
                        &dxHeader);

    return ret;
}

static void dxSubImage_EraseArea(dxFlashHandle_t dxFlash, uint32_t address, uint32_t size)
{
    int i = 0;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return ;
    }

    if (dxFlash == NULL ||
        address == 0 ||     // Should not erase address 0
        size == 0)
    {
        return ;
    }

    int sectors = (size + DEFAULT_FLASH_SECTOR_ERASE_SIZE - 1) / DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    G_SYS_DBG_LOG_V("Erase address 0x%X, len: %d bytes, total sectors: %d\n", address, size, sectors);

    for (i = 0; i < sectors; i++)
    {
        dxFlash_Erase_Sector(dxFlash, address + i * DEFAULT_FLASH_SECTOR_ERASE_SIZE);
    }
}

static dxSUBIMAGE_RET_CODE dxSubImage_WriteVerify(dxFlashHandle_t dxFlash, uint32_t address, uint32_t length, uint8_t *pData)
{
    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (dxFlash == NULL ||
        length == 0 ||
        pData == NULL)
    {
        return DXSUBIMAGE_ERROR;
    }

    uint8_t *pReadBuffer = dxMemAlloc(NULL, 1, length);

    if (pReadBuffer == NULL)
    {
        return DXSUBIMAGE_ERROR;
    }

    memset(pReadBuffer, 0x00, length);

    dxOS_RET_CODE ret = dxFlash_Read(dxFlash, address, length, pReadBuffer);

    if (ret != DXOS_SUCCESS)
    {
        dxMemFree(pReadBuffer);
        return DXSUBIMAGE_VERIFY_ERROR;
    }

    int r = memcmp((void *)pReadBuffer, pData, length);

    dxMemFree(pReadBuffer);

    if (r != 0)
    {
        return DXSUBIMAGE_VERIFY_ERROR;
    }

    return DXSUBIMAGE_SUCCESS;
}

static dxSUBIMAGE_RET_CODE dxSubImage_MD5IsEqual(dxSubImgInfoAccessCtrl_t *pHandle)
{
    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (pHandle == NULL ||
        pHandle->pmd5sum == NULL ||
        pHandle->pszFileMD5Sum == NULL)
    {
        return DXSUBIMAGE_ERROR;
    }

    uint32_t len = 16;//strlen(pHandle->pmd5sum);
    if (len * 2 != strlen(pHandle->pszFileMD5Sum))
    {
        return DXSUBIMAGE_ERROR;
    }

    dxSUBIMAGE_RET_CODE r = DXSUBIMAGE_SUCCESS;

    uint8_t i   = 0;
    uint8_t ch  = 0;
    uint8_t d   = 0;

    while (i < len)
    {
        ch = (pHandle->pmd5sum[i] >> 4) & 0x0F;
        ch += 0x30;

        if (ch > '9')
        {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[i * 2];

        if (d >= 'a' && d <= 'z')
        {
            d = d - 'a' + 'A';
        }

        if (ch != d)
        {
            r = DXSUBIMAGE_ERROR;
            break;
        }

        ch = (pHandle->pmd5sum[i]) & 0x0F;
        ch += 0x30;

        if (ch > '9')
        {
            ch += 7;
        }

        d = pHandle->pszFileMD5Sum[i * 2 + 1];

        if (d >= 'a' && d <= 'z')
        {
            d = d - 'a' + 'A';
        }

        if (ch != d)
        {
            r = DXSUBIMAGE_ERROR;
            break;
        }

        i++;
    }

    return r;
}

#endif // DX_SUBIMG_FLASH_SIZE

dxSubImgInfoHandle_t dxSubImage_Init(dxSUBIMGBLOCKID_t blockId)
{
    dxSubImgInfoAccessCtrl_t *pHandle = NULL;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return NULL;
    }

    if (blockId == DXSUBIMGBLOCK_ONE && dxSubImgInfoAccessCtrl[blockId])
    {
        return (dxSubImgInfoHandle_t)dxSubImgInfoAccessCtrl[blockId];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxSubImgInfoAccessCtrl_t));

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->hFlash = dxFlash_Init();

    if (pHandle->hFlash == NULL)
    {
        dxMutexDelete(pHandle->hMutex);
        pHandle->hMutex = NULL;
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    if (blockId == DXSUBIMGBLOCK_ONE)
    {
        pHandle->nSubImgBlockID = blockId;
        dxSubImgInfoAccessCtrl[blockId] = pHandle;
        return (dxSubImgInfoHandle_t)dxSubImgInfoAccessCtrl[blockId];
    }

    return NULL;
}

#if DX_SUBIMG_FLASH_SIZE

dxSUBIMAGE_RET_CODE dxSubImage_Start(dxSubImgInfoHandle_t handle, uint8_t *pszMD5Sum, uint32_t nImageSize)
{
    /**
     * strlen(pszMD5Sum) should be a NULL terminated string and length is 32 bytes
     */
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (handle && pHandle->nSubImgBlockID == DXSUBIMGBLOCK_ONE)
    {
        if (pHandle->hMutex == NULL ||
            pHandle->hFlash == NULL ||
            pszMD5Sum == NULL ||
            strlen(pszMD5Sum) != sizeof(pHandle->pmd5cx->state) * 2 ||
            nImageSize == 0)
        {
            return DXSUBIMAGE_ERROR;
        }
    }
    else
    {
        return DXSUBIMAGE_ERROR;
    }

    if (nImageSize > DX_SUBIMG_FLASH_SIZE)
    {
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    pHandle->nTotalReceiveBytes = nImageSize;
    pHandle->nDownloadBytes = 0;

    if (pHandle->pszFileMD5Sum)
    {
        dxMemFree(pHandle->pszFileMD5Sum);
        pHandle->pszFileMD5Sum = NULL;
    }

    if (pHandle->pmd5cx)
    {
        dxMemFree(pHandle->pmd5cx);
        pHandle->pmd5cx = NULL;
    }

    if (pHandle->pmd5sum)
    {
        dxMemFree(pHandle->pmd5sum);
        pHandle->pmd5sum = NULL;
    }

    pHandle->pszFileMD5Sum = (uint8_t *)dxMemAlloc(NULL, 1, sizeof(pHandle->pmd5cx->state) * 2 + 1); // Additional NULL character is needed.

    if (pHandle->pszFileMD5Sum  == NULL)
    {
        dxMutexGive(pHandle->hMutex);
        return DXSUBIMAGE_ERROR;
    }

    memcpy(pHandle->pszFileMD5Sum, pszMD5Sum, strlen(pszMD5Sum));

    pHandle->pmd5cx = (md5_context *)dxMemAlloc(NULL, 1, sizeof(md5_context));

    if (pHandle->pmd5cx  == NULL)
    {
        dxMutexGive(pHandle->hMutex);
        return DXSUBIMAGE_ERROR;
    }

    /**
     * sizeof(dxSubImgInfoAccessCtrl_t.pmd5cx->state) should be 16 bytes,
     * due to declare of "uint32_t state[4]"
     */
    pHandle->pmd5sum = (uint8_t *)dxMemAlloc(NULL, 1, sizeof(pHandle->pmd5cx->state) + 1);

    if (pHandle->pmd5sum == NULL)
    {
        dxMutexGive(pHandle->hMutex);
        return DXSUBIMAGE_ERROR;
    }

    md5_starts(pHandle->pmd5cx);

    dxSubImage_EraseArea(pHandle->hFlash,
                         DX_SUBIMG_FLASH_ADDR1,
                         nImageSize);

    dxMutexGive(pHandle->hMutex);

    return DXSUBIMAGE_SUCCESS;
}

dxSUBIMAGE_RET_CODE dxSubImage_Write(dxSubImgInfoHandle_t handle, uint8_t *pImage, uint32_t nImageLen)
{
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (handle && pHandle->nSubImgBlockID == DXSUBIMGBLOCK_ONE)
    {
        if (pHandle->hFlash == NULL ||
            pHandle->hMutex == NULL ||
            pHandle->pmd5cx == NULL ||
            pHandle->pmd5sum == NULL ||
            pHandle->nTotalReceiveBytes <= 0 ||
            pHandle->nDownloadBytes < 0 ||
            pImage == NULL)
        {
            return DXSUBIMAGE_ERROR;
        }
    }
    else
    {
        return DXSUBIMAGE_ERROR;
    }

    if (pHandle->nDownloadBytes + nImageLen > DX_SUBIMG_FLASH_SIZE)
    {
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    uint32_t addr = DX_SUBIMG_FLASH_ADDR1 + sizeof(dxSubImageHeaderAccessCtrl_t) + pHandle->nDownloadBytes;

    ret = dxFlash_Write(pHandle->hFlash,
                        addr,
                        nImageLen,
                        pImage);

    if (ret != DXOS_SUCCESS)
    {
        dxMutexGive(pHandle->hMutex);
        return DXOS_ERROR;
    }

    dxSUBIMAGE_RET_CODE r = dxSubImage_WriteVerify(pHandle->hFlash,
                                                   addr,
                                                   nImageLen,
                                                   pImage);

    if (r != DXSUBIMAGE_SUCCESS)
    {
        dxMutexGive(pHandle->hMutex);
        return r;
    }

    md5_update(pHandle->pmd5cx, pImage, nImageLen);

    pHandle->nDownloadBytes += nImageLen;

    r = DXSUBIMAGE_SUCCESS;

    if (pHandle->nDownloadBytes >= pHandle->nTotalReceiveBytes)
    {
        // MD5 checksum verify
        md5_finish(pHandle->pmd5cx, pHandle->pmd5sum);

        if (dxSubImage_MD5IsEqual(pHandle) == DXSUBIMAGE_SUCCESS)
        {
            ret = dxSubImage_WriteHeader(pHandle->hFlash, DX_SUBIMG_FLASH_ADDR1, pHandle->nTotalReceiveBytes);
            
            if (ret != DXOS_SUCCESS)
            {
                dxMutexGive(pHandle->hMutex);
                return DXOS_ERROR;
            }
        
            r = DXSUBIMAGE_COMPLETE;
        }
        else
        {
            r = DXSUBIMAGE_ERROR;
        }
    }

    dxMutexGive(pHandle->hMutex);

    return r;
}

dxSUBIMAGE_RET_CODE dxSubImage_Read(dxSubImgInfoHandle_t handle, uint32_t StartPosition, uint32_t TotalbyteToRead,
                                    uint8_t *pBDataBuffer, uint32_t *FwSizeOut)
{
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr = 0;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (handle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        TotalbyteToRead == 0 ||
        pBDataBuffer == NULL ||
        FwSizeOut == NULL)
    {
        return DXSUBIMAGE_ERROR;
    }

    if (pHandle->nSubImgBlockID == DXSUBIMGBLOCK_ONE)
    {
        addr = DX_SUBIMG_FLASH_ADDR1;
    }
    else
    {
        return DXSUBIMAGE_ERROR;
    }

    if (TotalbyteToRead > DX_SUBIMG_FLASH_SIZE)
    {
        return DXSUBIMAGE_SIZE_LIMIT_REACHED;
    }

    uint32_t address = addr + sizeof(dxSubImageHeaderAccessCtrl_t) + StartPosition;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxOS_RET_CODE r = dxFlash_Read(pHandle->hFlash,
                                   address,
                                   TotalbyteToRead,
                                   pBDataBuffer);

    dxMutexGive(pHandle->hMutex);

    if (r != DXOS_SUCCESS)
    {
        *FwSizeOut = 0;
        return DXAPPDATA_ERROR;
    }

    *FwSizeOut = TotalbyteToRead;

    return DXAPPDATA_SUCCESS;
}

dxSUBIMAGE_RET_CODE dxSubImage_Uninit(dxSubImgInfoHandle_t handle)
{
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXSUBIMAGE_VERIFY_FLASH_SIZE_ERROR;
    }

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL)
    {
        return DXSUBIMAGE_ERROR;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxFlash_Uninit(pHandle->hFlash);
    pHandle->hFlash = NULL;

    dxSubImgInfoAccessCtrl[pHandle->nSubImgBlockID] = NULL;
        
    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXSUBIMAGE_SUCCESS;
}

#if defined(configUSE_LOCAL_SYSINFO_DEBUG) && configUSE_LOCAL_SYSINFO_DEBUG == 1
dxSUBIMAGE_RET_CODE dxSubImg_SetMD5Sum(dxSubImgInfoHandle_t handle, uint8_t *testMD5Sum)
{
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    pHandle->pmd5sum = testMD5Sum;

    if (pHandle != NULL)
    {
        return DXSUBIMAGE_SUCCESS;
    }

    return DXSUBIMAGE_ERROR;
}
#endif //configUSE_LOCAL_SYSINFO_DEBUG

uint32_t dxSubImage_ReadImageSize(dxSubImgInfoHandle_t handle)
{
    dxSubImgInfoAccessCtrl_t *pHandle = (dxSubImgInfoAccessCtrl_t *)handle;

    uint32_t ImageSize = 0;
    uint32_t addr = 0;
    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return ImageSize;
    }

    if (handle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL)
    {
        return ImageSize;
    }

    if (pHandle->nSubImgBlockID == DXSUBIMGBLOCK_ONE)
    {
        addr = DX_SUBIMG_FLASH_ADDR1;
    }
    else
    {
        return ImageSize;
    }

    dxSubImageHeaderAccessCtrl_t dxHeader;

    memset(&dxHeader, 0x00, sizeof(dxHeader));
    
    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    memset(dxHeader.SubImgVer, 0x00, sizeof(dxHeader.SubImgVer));
                                   
    ret = dxFlash_Read(pHandle->hFlash,
                       addr,
                       sizeof(dxHeader),
                       &dxHeader);
                        
    dxMutexGive(pHandle->hMutex);

    if (ret == DXOS_SUCCESS &&
        dxHeader.Tag == DX_SUBIMAGE_MARK &&
        dxHeader.DeviceType == 0x00 &&
        dxHeader.HeaderVer == 0x01 &&
        dxHeader.HeaderSize == sizeof(dxSubImageHeaderAccessCtrl_t))
    {
        ImageSize = dxHeader.ImageSize;
    }
    
    return ImageSize;
}

#endif // DX_SUBIMG_FLASH_SIZE

//============================================================================
// Hyper Perioheral Storage Access API
//============================================================================

typedef struct
{
    uint32_t        nTotalSize;         // File size in bytes
    uint32_t        nSectorSize;        // Current block size in bytes
    dxBOOL          bInitialized;       // Initialization status
    uint8_t         nHPStorageSectorID; // Reserved for expandability

    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
} dxHpStorageInfoAccessCtrl_t;

static dxHpStorageInfoAccessCtrl_t *dxHpStorageInfoAccessCtrl[1] = NULL;

dxHpStorageInfoHandle_t dxHpStorage_Init(dxHPSTORAGESECTORID_t sector_id)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = NULL;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return NULL;
    }

    if (sector_id == DXHPSTORAGESECTOR_ONE && dxHpStorageInfoAccessCtrl[sector_id])
    {
        return (dxHpStorageInfoHandle_t)dxHpStorageInfoAccessCtrl[sector_id];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxHpStorageInfoAccessCtrl_t));

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL)
    {
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    pHandle->hFlash = dxFlash_Init();

    if (pHandle->hFlash == NULL)
    {
        dxMutexDelete(pHandle->hMutex);
        pHandle->hMutex = NULL;
        dxMemFree(pHandle);
        pHandle = NULL;
        return NULL;
    }

    if (sector_id == DXHPSTORAGESECTOR_ONE)
    {
        pHandle->nTotalSize = DX_HP_STORAGE_SIZE;
        pHandle->nSectorSize = DEFAULT_FLASH_SECTOR_ERASE_SIZE;
        pHandle->nHPStorageSectorID = sector_id;
        pHandle->bInitialized = dxTRUE;

        dxHpStorageInfoAccessCtrl[sector_id] = pHandle;
        return (dxHpStorageInfoHandle_t)dxHpStorageInfoAccessCtrl[sector_id];
    }

    return NULL;
}

#if DX_HP_STORAGE_SIZE != 0
dxHPSTORAGE_RET_CODE dxHpStorage_Write(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (handle && pHandle->nHPStorageSectorID == DXHPSTORAGESECTOR_ONE)
    {
        if (pHandle->hFlash == NULL ||
            pHandle->hMutex == NULL ||
            pData == NULL ||
            nDataLen <= 0)
        {
            return DXHPSTORAGE_ERROR;
        }
    }
    else
    {
        return DXHPSTORAGE_ERROR;
    }

    if (((nDataLen + StartPosition) > DX_HP_STORAGE_SIZE) ||
        (StartPosition < 0))
    {
        return DXHPSTORAGE_SIZE_LIMIT_REACHED;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    uint32_t addr = DX_HP_STORAGE_ADDR1 + StartPosition;

    ret = dxFlash_Write(pHandle->hFlash,
                        addr,
                        nDataLen,
                        pData);

    if (ret != DXOS_SUCCESS)
    {
        dxMutexGive(pHandle->hMutex);
        return DXOS_ERROR;
    }

    dxHPSTORAGE_RET_CODE r = DXHPSTORAGE_SUCCESS;

    dxMutexGive(pHandle->hMutex);

    return r;
}

dxHPSTORAGE_RET_CODE dxHpStorage_Read(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint8_t *pData, uint32_t nDataLen)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (handle && pHandle->nHPStorageSectorID == DXHPSTORAGESECTOR_ONE)
    {
        if (pHandle->hFlash == NULL ||
            pHandle->hMutex == NULL ||
            pData == NULL ||
            nDataLen <= 0)
        {
            return DXHPSTORAGE_ERROR;
        }
    }
    else
    {
        return DXHPSTORAGE_ERROR;
    }

    if (((nDataLen + StartPosition) > DX_HP_STORAGE_SIZE) ||
        (StartPosition < 0))
    {
        return DXHPSTORAGE_SIZE_LIMIT_REACHED;
    }

    uint32_t addr = DX_HP_STORAGE_ADDR1 + StartPosition;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxOS_RET_CODE r = dxFlash_Read(pHandle->hFlash,
                                   addr,
                                   nDataLen,
                                   pData);

    dxMutexGive(pHandle->hMutex);

    if (r != DXOS_SUCCESS)
    {
        return DXHPSTORAGE_ERROR;
    }

    return DXHPSTORAGE_SUCCESS;
}

dxHPSTORAGE_RET_CODE dxHpStorage_EraseNumOfSector(dxHpStorageInfoHandle_t handle, uint32_t StartPosition, uint16_t NumOfSector)
{
    int i = 0;
    dxOS_RET_CODE ret = DXOS_ERROR;
    
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        StartPosition < 0)
    {
        return DXHPSTORAGE_ERROR;
    }

    StartPosition += DX_HP_STORAGE_ADDR1;

    G_SYS_DBG_LOG_V("Erase StartPosition 0x%X, sectors: %d (%d bytes)\n",
                    StartPosition,
                    NumOfSector,
                    (NumOfSector * DEFAULT_FLASH_SECTOR_ERASE_SIZE));

    for (i = 0; i < NumOfSector; i++)
    {
        ret = dxFlash_Erase_Sector(pHandle->hFlash, StartPosition + (i * DEFAULT_FLASH_SECTOR_ERASE_SIZE));

        if (ret != DXOS_SUCCESS)
        {
            return DXHPSTORAGE_ERROR;
        }
    }
    
    return DXHPSTORAGE_SUCCESS;
}


dxHPSTORAGE_RET_CODE dxHpStorage_Uninit(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->hFlash == NULL)
    {
        return DXHPSTORAGE_ERROR;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxFlash_Uninit(pHandle->hFlash);
    pHandle->hFlash = NULL;

    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXSUBIMAGE_SUCCESS;
}

uint32_t dxHpStorage_ReadTotalSize(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;
    
    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return 0; //DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nTotalSize <= 0)
    {
        return 0; //DXHPSTORAGE_ERROR;
    }

    return pHandle->nTotalSize;
}

uint32_t dxHpStorage_ReadSectorSize(dxHpStorageInfoHandle_t handle)
{
    dxHpStorageInfoAccessCtrl_t *pHandle = (dxHpStorageInfoAccessCtrl_t *)handle;
    
    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return 0; //DXHPSTORAGE_INVALID_PARAMETER;
    }

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nSectorSize<= 0 ||
        pHandle->nHPStorageSectorID != DXHPSTORAGESECTOR_ONE)
    {
        return 0; //DXHPSTORAGE_ERROR;
    }

    return pHandle->nSectorSize;
}
#endif //DX_HP_STORAGE_SIZE != 0

//============================================================================
// System Information
//============================================================================
#define DX_APPDATA_MARK                 0xF1E2D3C4B5A69788
#define DX_APPDATA_VER                  0x01

#define DX_APPDATA_BLOCK_IS_VALID       1
#define DX_APPDATA_BLOCK_IS_INVALID     0

#define DX_APPDATA_OFFSET_INVALID       0x0FFFFFFFF
#define DX_APPDATA_MAX_APPDATA_SIZE     (DX_SYSINFO_FLASH_SIZE - sizeof(dxAppDataHeaderInfo_t) - sizeof(dxAppInfo_t))

typedef struct
{
    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
    uint8_t         nPage;                  // 0 => DX_DEVINFO_FLASH_ADDR1 or
                                            //      DX_SYSINFO_FLASH_ADDR1 (check dxMemConfig.h)
                                            // 1 => DX_DEVINFO_FLASH_ADDR2 or
                                            //      DX_SYSINFO_FLASH_ADDR2 (check dxMemConfig.h)
    uint8_t         nBlockID;               // 0 => dxBLOCKID_t is DXBLOCKID_DEVINFO
                                            // 1 => dxBLOCKID_t is DXBLOCKID_SYSINFO
    uint8_t         Reserved[2];            // Must be 0x0000
    uint32_t        nOffset;                // always point to the last data of structure dxAppInfo_t
                                            // 0xFFFFFFFF means there is no data behind the header
                                            // 0          means there is only one record behind the header
                                            // 24         means the last data is start from
                                            //            DX_xxxINFO_FLASH_ADDRn + sizeof(dxAppDataHeaderInfo_t) + 24
} dxAppDataAccessCtrl_t;

typedef struct
{
    uint64_t        Tag;                    // Must be DX_APPDATA_MARK (check dxBSP.c)
    uint8_t         Version;                // Must not 0x00
    uint8_t         SeqNo;                  // Must be 0x01 ~ 0xFE. 0xFF: Just erased.
    uint8_t         Reserved[4];            // Reserved. Must be 0x00000000
    uint16_t        Size;                   // sizeof(dxAppDataHeaderInfo_t). Size will be padded to 4 bytes alignment.
} dxAppDataHeaderInfo_t;                    // sizeof(dxAppDataHeaderInfo_t) should be 16 bytes

/**
 * Example usage:
   <pre>
typedef struct
{
    uint32_t        Column1;
    uint16_t        Column2;
    uint8_t         Column3;
    uint8_t         Column4;
} MyUserData_t;

uint16_t len = sizeof(dxAppInfo_t) + sizeof(MyUserData_t);                                 // Real data size
uint16_t size4align = (len + sizeof(uint32_t) - 1) / sizeof(uint32_t)) * sizeof(uint32_t); // Align to uint32_t
dxAppInfo_t *pAppInfo = dxMemAlloc(NULL, 1, size4align);
pAppInfo->Size = size4align;
MyUserData_t *pMyUserData = (MyUserData_t *)&pAppInfo[offsetof(dxAppInfo_t, pData)];
pMyUserData->Column1 = 10;
pMyUserData->Column2 = 20;
pMyUserData->Column3 = 30;
pMyUserData->Column4 = 40;
   </pre>
 */

 /**
 * Example structure dxAppInfo_t:
   <pre>
typedef struct
{
    uint16_t        Size;
    uint16_t        CRC16;

    // Example user data. Declare your data here
    uint32_t        MyLocalVar;     // Should be put BEFORE pData[0]
    uint8_t         pData[0];
} dxAppInfo_t;
   </pre>
 *
 *        00   01   02   03
 *      +----+----+----+----+
 *   00 |   Size  |  CRC16  |
 *      +----+----+----+----+
 *   04 |     MyLocalVar    |
 *      +----+----+----+----+
 *   08 |                   |
 *      +----+----+----+----+
 *        ^
 *        |
 *        +-------- pData[0]
 */
typedef struct
{
    uint16_t        Size;                   // Size including sizeof(dxAppInfo_t) + all data behind pData[0]
    uint16_t        CRC16;                  // Using CRC16 with polynomial 0x8005 to calculate data from pData[0]

    // Example user data. Declare your data here.
    uint8_t         pData[0];               // NOTE: If you have dynamic length data, you can put your data to pData[0].
                                            //       pData[0] should be the last element of structure dxAppInfo_t. Or
                                            //       a compiler error will occur.
                                            //
                                            //       Use offsetof(dxAppInfo_t, pData) to get the address of pData[0].
                                            //       The value of offsetof(dxAppInfo_t, pData) should be 4
} dxAppInfo_t;                              // sizeof(dxAppInfo_t) should be 4 bytes

static dxAppDataAccessCtrl_t *dxAppDataAccessCtrl[2] = NULL;

static dxOS_RET_CODE dxAppData_WriteHeader(dxFlashHandle_t hFlash, uint32_t addr, uint8_t seqno)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    dxAppDataHeaderInfo_t dxHeader;

    if (hFlash == NULL) {

        return DXOS_ERROR;
    }

    memset(&dxHeader, 0x00, sizeof(dxHeader));

    dxHeader.Tag = DX_APPDATA_MARK;
    dxHeader.Version = DX_APPDATA_VER;
    dxHeader.SeqNo = seqno;
    dxHeader.Size = ((sizeof(dxHeader) + sizeof(uint32_t) - 1) / sizeof(uint32_t)) * sizeof(uint32_t);

    ret = dxFlash_Write(hFlash, addr, sizeof(dxHeader), &dxHeader);

    return ret;
}

static uint32_t dxAppData_GetNextAvailableAddress(dxFlashHandle_t hFlash, uint8_t *pNewSeqNo, uint8_t *pBlockIsValid, uint8_t blockId)
{
    uint32_t addr[2];
    if (blockId == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    else
    {
        return 0;
    }

    dxAppDataHeaderInfo_t dxHeader[sizeof(addr) / sizeof(addr[0])];
    uint8_t Reserved[4] = { 0x00, 0x00, 0x00, 0x00 };
    int SeqNo[sizeof(addr) / sizeof(addr[0])];
    uint8_t BlockIsValid[sizeof(addr) / sizeof(addr[0])];
    dxOS_RET_CODE ret = DXOS_ERROR;
    int i = 0;

    if (hFlash == NULL ||
        pNewSeqNo == NULL) {

        return 0;
    }

    for (i = 0; i < sizeof(addr) / sizeof(addr[0]); i++) {

         ret = dxFlash_Read(hFlash, addr[i], sizeof(dxAppDataHeaderInfo_t), &dxHeader[i]);

         if (ret != DXOS_SUCCESS)
         {
            break;
         }

        if (dxHeader[i].Tag == DX_APPDATA_MARK &&
            dxHeader[i].Version == DX_APPDATA_VER &&
            memcmp(dxHeader[i].Reserved, Reserved, sizeof(dxHeader[i].Reserved)) == 0)
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_VALID;
            SeqNo[i] = dxHeader[i].SeqNo;
        }
        else
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_INVALID;
            SeqNo[i] = 0;
        }
    }

    if (ret != DXOS_SUCCESS)
    {
        return 0;
    }

    if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
        BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = 0x01;

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[0];
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = SeqNo[1] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[0];
    }
    else if (BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        *pNewSeqNo = SeqNo[0] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        *pBlockIsValid = DX_APPDATA_BLOCK_IS_INVALID;

        return addr[1];
    }

    // Both Blocks are valid
    if (SeqNo[0] == 0xFF &&
        SeqNo[1] == 0xFF)
    {
        *pNewSeqNo = 0x01;

        return addr[0];
    }

    if (SeqNo[0] > SeqNo[1] &&
        SeqNo[0] == SeqNo[1] + 1)
    {
        // Change to BLOCK2
        *pNewSeqNo = SeqNo[0] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }
        return addr[1];

    }
    else if (SeqNo[1] > SeqNo[0] &&
             SeqNo[1] == SeqNo[0] + 1)
    {
        // Change to BLOCK1
        *pNewSeqNo = SeqNo[1] + 1;

        if (*pNewSeqNo > 0xFE)
        {
            *pNewSeqNo = 0x01;
        }

        return addr[0];
    }
    else if ((SeqNo[0] == 0xFF) ||
             (SeqNo[0] == 0xFE && SeqNo[1] == 0x01))
    {
        // Change to BLOCK1
        *pNewSeqNo = SeqNo[1] + 1;

        return addr[0];
    }
    else if ((SeqNo[1] == 0xFF) ||
             (SeqNo[1] == 0xFE && SeqNo[0] == 0x01))
    {
        // Change to BLOCK2
        *pNewSeqNo = SeqNo[0] + 1;

        return addr[1];
    }

    return 0;
}

static uint32_t dxAppData_GetActiveAddress(dxFlashHandle_t hFlash, uint8_t blockId)
{
    uint32_t addr[2];
    if (blockId == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    else
    {
        return 0;
    }

    dxAppDataHeaderInfo_t dxHeader[sizeof(addr) / sizeof(addr[0])];
    uint8_t Reserved[4] = { 0x00, 0x00, 0x00, 0x00 };
    int SeqNo[sizeof(addr) / sizeof(addr[0])];
    uint8_t BlockIsValid[sizeof(addr) / sizeof(addr[0])];
    dxOS_RET_CODE ret = DXOS_ERROR;
    int i = 0;

    if (hFlash == 0)
    {
        return 0;
    }

    for (i = 0; i < sizeof(addr) / sizeof(addr[0]); i++)
    {
        ret = dxFlash_Read(hFlash, addr[i], sizeof(dxAppDataHeaderInfo_t), &dxHeader[i]);

        if (ret != DXOS_SUCCESS)
        {
            break;
        }

        if (dxHeader[i].Tag == DX_APPDATA_MARK &&
            dxHeader[i].Version == DX_APPDATA_VER &&
            memcmp(dxHeader[i].Reserved, Reserved, sizeof(dxHeader[i].Reserved)) == 0)
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_VALID;
            SeqNo[i] = dxHeader[i].SeqNo;
        }
        else
        {
            BlockIsValid[i] = DX_APPDATA_BLOCK_IS_INVALID;
            SeqNo[i] = 0;
        }
    }

    if (ret != DXOS_SUCCESS)
    {
        return 0;
    }

    if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
        BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        return 0;
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_VALID &&
             BlockIsValid[1] == DX_APPDATA_BLOCK_IS_INVALID)
    {
        return addr[0];
    }
    else if (BlockIsValid[0] == DX_APPDATA_BLOCK_IS_INVALID &&
             BlockIsValid[1] == DX_APPDATA_BLOCK_IS_VALID)
    {
        return addr[1];
    }

    // Both two Block are empty
    if (SeqNo[0] == 0xFF &&
        SeqNo[1] == 0xFF)
    {
        return 0;
    }

    if (SeqNo[0] > SeqNo[1] &&
        SeqNo[0] == SeqNo[1] + 1)
    {
        return addr[0];
    }
    else if (SeqNo[1] > SeqNo[0] &&
             SeqNo[1] == SeqNo[0] + 1)
    {
        return addr[1];
    }
    else if ((SeqNo[0] == 0xFF) ||
             (SeqNo[0] == 0xFE && SeqNo[1] == 0x01))
    {
        return addr[1];
    }
    else if ((SeqNo[1] == 0xFF) ||
             (SeqNo[1] == 0xFE && SeqNo[0] == 0x01))
    {
        return addr[0];
    }

    return 0;
}

static dxAPPDATA_RET_CODE dxAppData_ReadToLastData(dxAppDataAccessCtrl_t *handle)
{
    dxOS_RET_CODE ret = DXOS_ERROR;

    if (handle == NULL ||
        handle->hFlash == NULL ||
        handle->hMutex == NULL ||
        handle->nPage  >= 2 ||
        handle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    uint32_t addr[2];
    if (handle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (handle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }
    uint32_t address = addr[handle->nPage];

    dxAppInfo_t dxAppData;

    uint32_t i = 0;
    uint32_t prev = DX_APPDATA_OFFSET_INVALID;
    uint32_t nHeaderSize = sizeof(dxAppDataHeaderInfo_t);
    uint16_t crc16 = 0;

    while (nHeaderSize + i < DX_SYSINFO_FLASH_SIZE) {

        memset(&dxAppData, 0x00, sizeof(dxAppData));

        ret = dxFlash_Read(handle->hFlash,
                           address + nHeaderSize + i,
                           sizeof(dxAppData),
                           &dxAppData);

        if (ret != DXOS_SUCCESS) {
            break;
        }

        if (dxAppData.Size == 0x0FFFF) {
            break;
        }

        if (dxAppData.Size > DX_APPDATA_MAX_APPDATA_SIZE) {

            // Data overwrite
            ret = DXAPPDATA_SIZE_LIMIT_REACHED;
            break;
        }

        prev = i;

        i += sizeof(dxAppData);

        uint8_t *pData = dxMemAlloc(NULL, 1, dxAppData.Size);

        if (pData == NULL) {

            ret = DXAPPDATA_ERROR;
            break;
        }

        if (dxAppData.Size > 0) {

            dxOS_RET_CODE r = dxFlash_Read(handle->hFlash,
                                           address + nHeaderSize + i,
                                           dxAppData.Size,
                                           pData);
            if (r != DXOS_SUCCESS) {

                dxMemFree(pData);
                pData = NULL;

                ret = DXAPPDATA_ERROR;
                break;
            }

            crc16 = Crc16(pData, dxAppData.Size, 0);

            if (crc16 != dxAppData.CRC16) {

                // CRC16 mismatch
                dxMemFree(pData);
                pData = NULL;

                ret = DXAPPDATA_ERROR;
                break;
            }
        }

        dxMemFree(pData);
        pData = NULL;

        i += dxAppData.Size;
    }

    if (ret != DXOS_SUCCESS) {

        return DXAPPDATA_ERROR;
    }

    if (nHeaderSize + i > DX_SYSINFO_FLASH_SIZE) {

        return DXAPPDATA_ERROR;
    }

    handle->nOffset =  prev;

    return DXAPPDATA_SUCCESS;
}

dxAppDataHandle_t dxAppData_Init(dxBLOCKID_t blockId)
{
    dxAppDataAccessCtrl_t *pHandle = NULL;
    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr = 0;

    if (blockId == DXBLOCKID_DEVINFO && dxAppDataAccessCtrl[0])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }
    else if (blockId == DXBLOCKID_SYSINFO && dxAppDataAccessCtrl[1])
    {
        return (dxAppDataHandle_t *)dxAppDataAccessCtrl[blockId];
    }

    pHandle = dxMemAlloc(NULL, 1, sizeof(dxAppDataAccessCtrl_t));

    if (pHandle == NULL) {
        return NULL;
    }

    pHandle->nPage = 0;
    memset(&pHandle->Reserved[0], 0x00, sizeof(pHandle->Reserved));

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    pHandle->hMutex = dxMutexCreate();

    if (pHandle->hMutex == NULL) {
        dxMemFree(pHandle);
        pHandle = NULL;

        return NULL;
    }

    pHandle->hFlash = dxFlash_Init();

    if (pHandle->hFlash == NULL) {
        dxMutexDelete(pHandle->hMutex);
        pHandle->hMutex = NULL;

        dxMemFree(pHandle);
        pHandle = NULL;

        return NULL;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    addr = dxAppData_GetActiveAddress(pHandle->hFlash, blockId);

    if (addr != DX_DEVINFO_FLASH_ADDR1 &&
        addr != DX_DEVINFO_FLASH_ADDR2 &&
        addr != DX_SYSINFO_FLASH_ADDR1 &&
        addr != DX_SYSINFO_FLASH_ADDR2)
    {

        // There is no valid data in these sectors.
        // Write data into sector 1
        if (blockId == DXBLOCKID_DEVINFO)
        {
            addr = DX_DEVINFO_FLASH_ADDR1;
        }
        else if (blockId == DXBLOCKID_SYSINFO)
        {
            addr = DX_SYSINFO_FLASH_ADDR1;
        }

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, addr, DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            dxFlash_Uninit(pHandle->hFlash);
            pHandle->hFlash = NULL;

            dxMutexGive(pHandle->hMutex);
            dxMutexDelete(pHandle->hMutex);
            pHandle->hMutex = NULL;

            dxMemFree(pHandle);
            pHandle = NULL;

            return NULL;
        }

        ret = dxAppData_WriteHeader(pHandle->hFlash, addr, 0x0FF);

        if (ret != DXOS_SUCCESS) {

            dxFlash_Uninit(pHandle->hFlash);
            pHandle->hFlash = NULL;

            dxMutexGive(pHandle->hMutex);
            dxMutexDelete(pHandle->hMutex);
            pHandle->hMutex = NULL;

            dxMemFree(pHandle);
            pHandle = NULL;

            return NULL;
        }
    }

    switch (addr) {
    case DX_DEVINFO_FLASH_ADDR1:
        pHandle->nPage      = 0;
        pHandle->nBlockID   = DXBLOCKID_DEVINFO;
        break;
    case DX_DEVINFO_FLASH_ADDR2:
        pHandle->nPage      = 1;
        pHandle->nBlockID   = DXBLOCKID_DEVINFO;
        break;
    case DX_SYSINFO_FLASH_ADDR1:
        pHandle->nPage      = 0;
        pHandle->nBlockID   = DXBLOCKID_SYSINFO;
        break;
    case DX_SYSINFO_FLASH_ADDR2:
        pHandle->nPage      = 1;
        pHandle->nBlockID   = DXBLOCKID_SYSINFO;
        break;
    default:
        pHandle->nPage      = 0xFF;
        pHandle->nBlockID   = 0xFF;
    }

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    dxAPPDATA_RET_CODE r = dxAppData_ReadToLastData(pHandle);

    dxMutexGive(pHandle->hMutex);

    if (r != DXAPPDATA_SUCCESS) {

        return NULL;
    }

    if (blockId == DXBLOCKID_DEVINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[0];
    }
    else if (blockId == DXBLOCKID_SYSINFO)
    {
        dxAppDataAccessCtrl[blockId] = pHandle;

        return (dxAppDataHandle_t)dxAppDataAccessCtrl[1];
    }

    return NULL;
}

dxAPPDATA_RET_CODE dxAppData_Read(dxAppDataHandle_t handle, void *pAPInf, uint32_t *pnSizeOfAPInfo)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2 ||
        pnSizeOfAPInfo == NULL ||
        pAPInf == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (*pnSizeOfAPInfo == 0) {
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {
        return DXAPPDATA_SUCCESS;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;

    dxAppInfo_t dxAppData;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    ret = dxFlash_Read(pHandle->hFlash,
                       address,
                       sizeof(dxAppData),
                       &dxAppData);

    if (ret != DXOS_SUCCESS) {
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    if (dxAppData.Size > DX_APPDATA_MAX_APPDATA_SIZE) {

        // Incorrect value of .Size
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    if (*pnSizeOfAPInfo < dxAppData.Size) {

        // Not enough space
        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    dxOS_RET_CODE r = dxFlash_Read(pHandle->hFlash,
                                   address + sizeof(dxAppData),
                                   dxAppData.Size,
                                   pAPInf);

    dxMutexGive(pHandle->hMutex);

    if (r != DXOS_SUCCESS) {
        return DXAPPDATA_ERROR;
    }

    uint16_t crc16 = Crc16(pAPInf, dxAppData.Size, 0);

    if (crc16 != dxAppData.CRC16) {
        // CRC16 mismatch
        return DXAPPDATA_ERROR;
    }

    *pnSizeOfAPInfo = dxAppData.Size;

    return DXAPPDATA_SUCCESS;
}

uint32_t dxAppData_ReadDataSize(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {

        return 0;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;

    dxAppInfo_t dxAppData;

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    ret = dxFlash_Read(pHandle->hFlash,
                       address,
                       sizeof(dxAppData),
                       &dxAppData);

    dxMutexGive(pHandle->hMutex);

    if (ret != DXOS_SUCCESS || dxAppData.Size == 0x0FFFF) {
        return 0;
    }

    return dxAppData.Size;
}

dxAPPDATA_RET_CODE dxAppData_Write(dxAppDataHandle_t handle, void *pAPInf, uint32_t nSizeOfAPInfo)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];
    uint32_t prevoffset = 0;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nBlockID >= 2 ||
        pHandle->nPage >= 2 ||
        nSizeOfAPInfo == 0 ||
        pAPInf == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    if (nSizeOfAPInfo == 0 ||
        nSizeOfAPInfo > DX_APPDATA_MAX_APPDATA_SIZE) {

        return DXAPPDATA_SIZE_LIMIT_REACHED;
    }

    uint32_t address = addr[pHandle->nPage] + sizeof(dxAppDataHeaderInfo_t);

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    if (pHandle->nOffset == DX_APPDATA_OFFSET_INVALID) {

        // Both sectors are empty.
        prevoffset = 0;

        ret = dxAppData_WriteHeader(pHandle->hFlash, addr[pHandle->nPage], 0x01);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }
    }
    else {
        address += pHandle->nOffset;
        prevoffset += pHandle->nOffset;

        // handle->nOffset point to the last record, NOT an empty space
        dxAppInfo_t dxAppData;

        ret = dxFlash_Read(pHandle->hFlash,
                           address,
                           sizeof(dxAppData),
                           &dxAppData);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        if (dxAppData.Size <= DX_APPDATA_MAX_APPDATA_SIZE) {

            uint8_t *pData = dxMemAlloc(NULL, 1, dxAppData.Size);

            if (pData == NULL) {
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            ret = dxFlash_Read(pHandle->hFlash,
                               address + sizeof(dxAppData),
                               dxAppData.Size,
                               pData);

            if (ret != DXOS_SUCCESS) {

                dxMemFree(pData);
                pData = NULL;
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            uint16_t crc16 = Crc16(pData, dxAppData.Size, 0);

            if (crc16 != dxAppData.CRC16) {

                // CRC16 mismatch
                dxMemFree(pData);
                pData = NULL;
                dxMutexGive(pHandle->hMutex);
                return DXAPPDATA_ERROR;
            }

            dxMemFree(pData);
            pData = NULL;

            address += sizeof(dxAppData) + dxAppData.Size;
            prevoffset += sizeof(dxAppData) + dxAppData.Size;
        }
    }

    // Check available space
    if ((address + sizeof(dxAppInfo_t) + nSizeOfAPInfo) >= (addr[pHandle->nPage] + DX_SYSINFO_FLASH_SIZE)) {

        // There is no enough space to write in current sector. Write to next sector
        uint8_t newseqno = 0;
        uint8_t blockvalid = 0;

        address = dxAppData_GetNextAvailableAddress(pHandle->hFlash, &newseqno, &blockvalid, pHandle->nBlockID);

        if (address != DX_DEVINFO_FLASH_ADDR1 &&
            address != DX_DEVINFO_FLASH_ADDR2 &&
            address != DX_SYSINFO_FLASH_ADDR1 &&
            address != DX_SYSINFO_FLASH_ADDR2) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, address, DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);
            return DXAPPDATA_ERROR;
        }

        ret = dxAppData_WriteHeader(pHandle->hFlash, address, newseqno);

        if (ret != DXOS_SUCCESS) {

            dxMutexGive(pHandle->hMutex);

            return DXAPPDATA_ERROR;
        }

        switch (address) {
        case DX_DEVINFO_FLASH_ADDR1:
            pHandle->nPage = 0;
            break;
        case DX_DEVINFO_FLASH_ADDR2:
            pHandle->nPage = 1;
            break;
        case DX_SYSINFO_FLASH_ADDR1:
            pHandle->nPage = 0;
            break;
        case DX_SYSINFO_FLASH_ADDR2:
            pHandle->nPage = 1;
            break;
        }

        pHandle->nOffset = 0;
        prevoffset = 0;

        address += sizeof(dxAppDataHeaderInfo_t) + pHandle->nOffset;
    }

    // Append data
    uint32_t len = sizeof(dxAppInfo_t) + nSizeOfAPInfo;
    uint8_t *p = dxMemAlloc(NULL, 1, len);

    if (p == NULL) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    dxAppInfo_t *pAppInfo = (dxAppInfo_t *)p;

    pAppInfo->Size = nSizeOfAPInfo;
    pAppInfo->CRC16 = Crc16(pAPInf, nSizeOfAPInfo, 0);
    memcpy(&p[offsetof(dxAppInfo_t, pData)], pAPInf, nSizeOfAPInfo);

    ret = dxFlash_Write(pHandle->hFlash,
                        address,
                        len,
                        p);

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        dxMemFree(p);
        p = NULL;

        return DXAPPDATA_ERROR;
    }

    pHandle->nOffset = prevoffset;

    dxMutexGive(pHandle->hMutex);
    dxMemFree(p);
    p = NULL;

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Clean(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    dxOS_RET_CODE ret = DXOS_ERROR;
    uint32_t addr[2];

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL ||
        pHandle->nPage >= 2 ||
        pHandle->nBlockID >= 2) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        addr[0] = DX_DEVINFO_FLASH_ADDR1;
        addr[1] = DX_DEVINFO_FLASH_ADDR2;
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        addr[0] = DX_SYSINFO_FLASH_ADDR1;
        addr[1] = DX_SYSINFO_FLASH_ADDR2;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    for (int i = 0; i < sizeof(addr) / sizeof(addr[0]); i++) {

        ret = dxFlash_Erase_SectorWithSize(pHandle->hFlash, addr[i], DX_SYSINFO_FLASH_SIZE);

        if (ret != DXOS_SUCCESS) {

            break;
        }
    }

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    pHandle->nPage = 0;

    pHandle->nOffset = DX_APPDATA_OFFSET_INVALID;

    if (pHandle->nBlockID == DXBLOCKID_DEVINFO)
    {
        ret = dxAppData_WriteHeader(dxAppDataAccessCtrl[pHandle->nBlockID]->hFlash, addr[pHandle->nPage], 0x0FF);
    }
    else if (pHandle->nBlockID == DXBLOCKID_SYSINFO)
    {
        ret = dxAppData_WriteHeader(dxAppDataAccessCtrl[pHandle->nBlockID]->hFlash, addr[pHandle->nPage], 0x0FF);
    }

    if (ret != DXOS_SUCCESS) {

        dxMutexGive(pHandle->hMutex);
        return DXAPPDATA_ERROR;
    }

    dxMutexGive(pHandle->hMutex);

    return DXAPPDATA_SUCCESS;
}

dxAPPDATA_RET_CODE dxAppData_Uninit(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle == NULL ||
        pHandle == NULL ||
        pHandle->hFlash == NULL ||
        pHandle->hMutex == NULL) {
        return DXAPPDATA_INVALID_HANDLE;
    }

    dxMutexTake(pHandle->hMutex, DXOS_WAIT_FOREVER);

    dxAppDataAccessCtrl[pHandle->nBlockID] = NULL;

    dxFlash_Uninit(pHandle->hFlash);
    pHandle->hFlash = NULL;

    dxMutexGive(pHandle->hMutex);
    dxMutexDelete(pHandle->hMutex);

    dxMemFree(pHandle);
    pHandle = NULL;

    return DXAPPDATA_SUCCESS;
}

#if defined(configUSE_LOCAL_SYSINFO_DEBUG) && configUSE_LOCAL_SYSINFO_DEBUG == 1
dxFlashHandle_t dxAppData_GetFlash(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->hFlash;
    }

    return NULL;
}

dxBLOCKID_t dxAppData_GetBlockID(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nBlockID;
    }

    return DXAPPDATA_ERROR;
}

uint8_t dxAppData_GetPage(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nPage;
    }

    return DXAPPDATA_ERROR;
}

uint32_t dxAppData_GetOffset(dxAppDataHandle_t handle)
{
    dxAppDataAccessCtrl_t *pHandle = (dxAppDataAccessCtrl_t *)handle;

    if (handle && pHandle != NULL)
    {
        return pHandle->nOffset;
    }

    return DXAPPDATA_ERROR;
}

#endif //configUSE_LOCAL_SYSINFO_DEBUG
