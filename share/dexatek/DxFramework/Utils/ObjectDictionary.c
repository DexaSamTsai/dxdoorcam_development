//============================================================================
// File: dk_objects.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.161.0
//     Date: 2015/06/23
//     1) Description: Support QA site redirection
//        Added:
//            - ObjectDictionary_SetApiVersion ()
//        Modified:
//            - DKObj_SetHosetname ()
//
//     Version: 0.161.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.161.2
//     Date: 2016/05/04
//     1) Description: Modify function to support clear SessionToken
//        Modified:
//            - ObjectDictionary_SetSessionToken ()
//
//     Version: 0.161.3
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Modified:
//            - ObjectDictionary_DeleteGateway
//            - ObjectDictionary_DeletePeripheral
//            - ObjectDictionary_FreePeripheralMemory
//            - ObjectDictionary_AddPeripheral ()
//            - ObjectDictionary_ClearAllocGatewayPeripheralMem ()
//            - ObjectDictionary_SetHostname ()
//            - ObjectDictionary_AddGateway ()
//        Added:
//            - DX_DEVICENAME_MAX_LEN
//            - DKObj_TrimUTF8StringTrailingChar ()
//
//     Version: 0.161.4
//     Date: 2017/03/02
//     1) Description: Change peripheral's DeviceName
//
//     Version: 0.161.5
//     Date: 2017/06/29
//     1) Description: To avoid copy data into a NULL memory
//        Modified:
//            - ObjectDictionary_AddPeripheralStatus ()
//
//     Version: 0.161.6
//     Date: 2017/09/25
//     1) Description: Fixed "Segmentation fault" issue on embeddex Linux
//        Modified:
//            - ObjectDictionary_SetSessionToken ()
//
//     Version: 0.223.0
//     Date: 2017/09/27
//     1) Description: Support login with "DKAuth".
//                     Move variables to SDRAM if CONFIG_SDRAM_BUFFER_ALLOC equal to 1
//        Modified:
//            - Added DK_LOGINTYPE_ANONYMOUS to DK_LoginType_t
//============================================================================

#include "ObjectDictionary.h"
#include "dxSysDebugger.h"
#include "dk_hex.h"
#include "dxMD5.h"



//============================================================================
//    Define
//============================================================================

#define CALLOC_NAME                                     "DKSM"

#define DK_ID_LEN                                       16
#define DK_TOKEN_LEN                                    32
#define DK_HOST_LEN                                     64
#define DK_APP_API_ID_LEN                               64
#define DX_DEVICENAME_MAX_LEN                           19



//============================================================================
//    Enumerations
//============================================================================



//============================================================================
//    Structures
//============================================================================



//============================================================================
//    Function Declarations
//============================================================================

/*---- Private Function ----------------------------------------------------*/

void _ObjectDictionary_ClearMemory ();
void _ObjectDictionary_TrimUTF8StringTrailingChar (uint8_t* data);



//============================================================================
//    Global Variable
//============================================================================

#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes = @ ".sdram.text"
#endif // CONFIG_SDRAM_BUFFER_ALLOC

static DK_Mem_t DKUser;
static DK_Mem_t DKGateway;
static DK_Mem_t DKPeripheral;
static DK_Mem_t DKPeripheralStatus;
static uint64_t peripheralUpdateTime = 0;
static uint64_t scheduleJobUpdateTime = 0;
static uint64_t advanceJobUpdateTime = 0;

static char                 szApplicationID [DK_APP_API_ID_LEN + 1]     = {0};
static char                 szAPIKey        [DK_APP_API_ID_LEN + 1]     = {0};
static char                 szAPIVer        [DK_ID_LEN + 1]     = {0};
static char                 szSessionToken  [DK_TOKEN_LEN + 1]  = {0};
static char                 szMACAddress    [DK_ID_LEN + 1]     = {0};
static char                 szHashCode      [DK_HOST_LEN + 1]   = {0};
static char                 szHostname      [DK_HOST_LEN + 1]   = {0};
static dxIP_Addr_t          HostIP                              = {0};

static dxTime_t             tmLastTCPConnectedTime = {0};

#if defined (DKOBJ_DUMP_MEM)
static int                  AllocUserMemoryCount = 0;
static int                  AllocGatewayMemoryCount = 0;
static int                  AllocPeripheralMemoryCount = 0;
static int                  AllocPeripheralStatusMemoryCount = 0;
#endif

#if defined (USE_MUTEX)
static dxMutexHandle_t     mutex;
#endif

#if CONFIG_SDRAM_BUFFER_ALLOC
#pragma default_variable_attributes =
#endif // CONFIG_SDRAM_BUFFER_ALLOC



//============================================================================
//    Implementation
//============================================================================

/*---- Public Function -----------------------------------------------------*/

void ObjectDictionary_Init (uint8_t* macaddr, int macaddrlen, uint8_t* dkServerName, uint8_t* appID, uint8_t* apiKey, uint8_t* apiVer)
{
    int len = 0;

#if defined (USE_MUTEX)
    mutex = dxMutexCreate ();
#endif

    memset (szApplicationID, 0x00, sizeof (szApplicationID));
    memset (szAPIKey,        0x00, sizeof (szAPIKey));
    memset (szAPIVer,        0x00, sizeof (szAPIVer));
    memset (szSessionToken,  0x00, sizeof (szSessionToken));
    memset (szMACAddress,    0x00, sizeof (szMACAddress));
    memset (szHashCode,      0x00, sizeof (szHashCode));
    memset (szHostname,      0x00, sizeof (szHostname));
    memset (&HostIP,         0x00, sizeof (HostIP));

    ObjectDictionary_AllocPeripheralMemory (MAXIMUM_DEVICE_KEEPER_ALLOWED);
    ObjectDictionary_AllocPeripheralStatusMemory (MAXIMUM_DEVICE_KEEPER_ALLOWED);

    _ObjectDictionary_ClearMemory ();

    if (macaddr == NULL || macaddrlen != 8)
    {
        return ;
    }

    len = snprintf (szApplicationID, sizeof (szApplicationID), "%s", appID);
    if (len >= sizeof (szApplicationID))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, sizeof (szApplicationID): %d, but length of appID is %d\n",
                         __FUNCTION__,
                         __LINE__,
                         (int) sizeof (szApplicationID), (int) strlen ((char*) appID));
    }

    len = snprintf (szAPIKey, sizeof (szAPIKey), "%s", apiKey);
    if (len >= sizeof (szAPIKey))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, sizeof (szAPIKey): %d, but length of apiKey is %d\n",
                         __FUNCTION__,
                         __LINE__,
                         (int) sizeof (szAPIKey), (int) strlen ((char*) apiKey));
    }

    len = snprintf (szAPIVer, sizeof (szAPIVer), "%s", apiVer);
    if (len >= sizeof (szAPIVer))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, sizeof (szAPIVer): %d, but length of apiVer is %d\n",
                         __FUNCTION__,
                         __LINE__,
                         (int) sizeof (szAPIVer), (int) strlen ((char*) apiVer));
    }

    len = snprintf (szHostname, sizeof (szHostname), "%s", dkServerName);
    if (len >= sizeof (szHostname))
    {
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, sizeof (szHostname): %d, but length of dkServerName is %d\n",
                         __FUNCTION__,
                         __LINE__,
                         (int) sizeof (szHostname), (int) strlen ((char*) dkServerName));
    }

    memcpy (szMACAddress, (char*) macaddr, macaddrlen);
}


char* ObjectDictionary_GetApplicationId ()
{
    return szApplicationID;
}


char* ObjectDictionary_GetApiKey ()
{
    return szAPIKey;
}


char* ObjectDictionary_GetApiVersion ()
{
    return szAPIVer;
}


void ObjectDictionary_SetApiVersion (char* pValue)
{
    int len = strlen (pValue);

    if (len < sizeof (szAPIVer))
    {
        memset (szAPIVer, 0x00, sizeof (szAPIVer));
        memcpy (szAPIVer, pValue, len);
    }
}


uint64_t ObjectDictionary_GetPeripheralUpdatetime (void) {
    return peripheralUpdateTime;
}


void ObjectDictionary_SetPeripheralUpdatetime (uint64_t updatedtime) {
    peripheralUpdateTime = updatedtime;
}


uint64_t ObjectDictionary_GetScheduleJobUpdatetime (void) {
    return scheduleJobUpdateTime;
}


void ObjectDictionary_SetScheduleJobUpdatetime (uint64_t updatedtime) {
    scheduleJobUpdateTime = updatedtime;
}


uint64_t ObjectDictionary_GetAdvanceJobUpdatetime (void) {
    return advanceJobUpdateTime;
}


void ObjectDictionary_SetAdvanceJobUpdatetime (uint64_t updatedtime) {
    advanceJobUpdateTime = updatedtime;
}


char* ObjectDictionary_GetSessionToken ()
{
    return szSessionToken;
}


void ObjectDictionary_SetSessionToken (char* pValue)
{
    memset (szSessionToken, 0x00, sizeof (szSessionToken));

    if (pValue)
    {
        int len = strlen (pValue);

        if (len < sizeof (szSessionToken))
        {

            memcpy (szSessionToken, pValue, len);
        }
    }
}


char* ObjectDictionary_GetMacAddress ()
{
    return szMACAddress;
}


char* ObjectDictionary_GetHashCode ()
{
    return szHashCode;
}


void ObjectDictionary_SetHashCode (char* pValue)
{
    int len = strlen (pValue);

    if (len < sizeof (szHashCode))
    {
        memset (szHashCode, 0x00, sizeof (szHashCode));
        memcpy (szHashCode, pValue, len);
    }
}


char* ObjectDictionary_GetHostname ()
{
    return szHostname;
}


void ObjectDictionary_SetHostname (char* pValue)
{
    int len = strlen (pValue);

    if (len < sizeof (szHostname))
    {
        memset (szHostname, 0x00, sizeof (szHostname));
        memcpy (szHostname, pValue, len);

        // Clear allocate memory
        ObjectDictionary_Lock ();
        ObjectDictionary_DeleteUser ();
        ObjectDictionary_DeleteGateway ();
        ObjectDictionary_Unlock ();

        // Force wiced_hostname_lookup () can be called.
        if (HostIP.version == DXIP_VERSION_V4)
        {
            //HostIP.ip.v4 = 0x00;
            memset (HostIP.v4, 0x00, sizeof (HostIP.v4));
        }
        else
        {
            // Currently, NOT support IPV6
        }
    }
}


dxIP_Addr_t* ObjectDictionary_GetHostIp ()
{
    return &HostIP;
}


void ObjectDictionary_SetHostIp (dxIP_Addr_t ip)
{
    memcpy (&HostIP, &ip, sizeof (dxIP_Addr_t));
}


int ObjectDictionary_CreateHashCode (char* username, char* password, char* hashcode, int hashcodelen)
{
    char szUPWD[sizeof (UserLogin_t) + 1] = {0};     // To store username + password
    char szBuffer[DK_HOST_LEN + 1] = {0};           // To calculate MD5
    int ret = 0;

    if (username == NULL || password == NULL || hashcode == NULL || hashcodelen <= 0)
    {
        return 0;
    }

    // Calculate MD5
    memset ((char*) szUPWD, 0x00, sizeof (szUPWD));
    sprintf ((char*) szUPWD, "%s%s", username, password);

    memset ((char*) szBuffer, 0x00, sizeof (szBuffer));

    dxMD5 ((uint8_t*) szUPWD, strlen ((char*) szUPWD), (uint8_t*) szBuffer);                        // Length of md5 result must be 16
    DKHex_HexToASCII ((char*) szBuffer, 16, (char*) szUPWD, sizeof (szUPWD));

    if (snprintf (NULL, 0, "%s%s", username, szUPWD) > hashcodelen)
    {
        // TODO: szBuffer is too small.
        G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                         "[%s] Line %d, szBuffer[%d] is too small\n",
                         __FUNCTION__,
                         __LINE__,
                         hashcodelen);
        return 0;
    }

    ret = snprintf (hashcode, hashcodelen, "%s%s", username, szUPWD);

    return ret;
}


int ObjectDictionary_AddUser (uint32_t objectID, uint32_t parentObjectID, UserLogin_t* pLogin)
{
    int ret = 0;    // False

    if (/*objectID == 0 || parentObjectID == 0 || */pLogin == NULL) return 0;

    if (DKUser.pData == NULL)
    {
        // David Changed. Use calloc () instead of malloc () + memset ()
        //DKUser.pData = malloc (sizeof (DK_Users_t));
        DKUser.pData = (void*) dxMemAlloc (CALLOC_NAME, 1, sizeof (DK_Users_t));
        if (DKUser.pData == NULL) return 0; // False

        //memset (DKUser.pData, 0x00, sizeof (DK_Users_t));

        DKUser.nItemCount = 1;
        DKUser.nMemSize = (uint16_t) sizeof (DK_Users_t);
        DK_Users_t* pUser = (DK_Users_t*) DKUser.pData;

#if defined (DKOBJ_DUMP_MEM)
        AllocUserMemoryCount++;
#endif

        pUser->ObjectID         = objectID;
        pUser->ParentObjectID   = parentObjectID;

        memcpy (&pUser->UserInfo, pLogin, sizeof (pUser->UserInfo));

        ret = 1;
    }
    else
    {
        // TODO: Support only one user
        DK_Users_t* pUser = (DK_Users_t*) DKUser.pData;

        if (pUser->ObjectID == objectID)
        {
            ret = 1;
        }
    }

    return ret;
}


void ObjectDictionary_DeleteUser (void)
{
    if (DKUser.pData)
    {
        dxMemFree (DKUser.pData);
        DKUser.pData = NULL;
        DKUser.nMemSize = 0;

#if defined (DKOBJ_DUMP_MEM)
        AllocUserMemoryCount--;
#endif
    }

    memset (&DKUser, 0x00, sizeof (DKUser));
}


DK_Users_t* ObjectDictionary_GetUser ()
{
    if (DKUser.nItemCount <= 0 || DKUser.nMemSize <= 0 || DKUser.pData == NULL) return NULL;

    return (DK_Users_t*) DKUser.pData;
}


int ObjectDictionary_AddGateway (uint32_t objectID, uint8_t* macaddr, int macaddrlen, char* wifiv, char* blev, uint8_t* pDeviceName)
{
    int ret = 0;    // False

    if (objectID == 0 || macaddr == 0 || macaddrlen != 8) return 0;

    if (DKGateway.pData == NULL)
    {
        // David Changed. Use calloc () instead of malloc () + memset ()
        //DKGateway.pData = malloc (sizeof (DK_BLEGateway_t));
        DKGateway.pData = (void*) dxMemAlloc (CALLOC_NAME, 1, sizeof (DK_BLEGateway_t));
        if (DKGateway.pData == NULL) return 0;  // False

        DKGateway.nItemCount = 1;
        DKGateway.nMemSize = (uint16_t) sizeof (DK_BLEGateway_t);
        DK_BLEGateway_t* pGateway = (DK_BLEGateway_t*) DKGateway.pData;

        pGateway->ObjectID = objectID;

        memset (pGateway->MACAddress,    0x00,       sizeof (pGateway->MACAddress));
        memcpy (pGateway->MACAddress,    macaddr,    macaddrlen);

        memset (pGateway->WiFiVersion,   0x00,       sizeof (pGateway->WiFiVersion));
        memcpy (pGateway->WiFiVersion,   wifiv,      strlen ((char*) wifiv));

        memset (pGateway->BLEVersion,    0x00,       sizeof (pGateway->BLEVersion));
        memcpy (pGateway->BLEVersion,    blev,       strlen ((char*) blev));

#if defined (DKOBJ_DUMP_MEM)
        AllocGatewayMemoryCount++;
#endif

        if (pDeviceName)
        {
            int nDeviceNameLen = strlen ((char*) pDeviceName);
            if (nDeviceNameLen > DX_DEVICENAME_MAX_LEN)
            {
                nDeviceNameLen = DX_DEVICENAME_MAX_LEN;
            }

            pGateway->pDeviceName = dxMemAlloc (CALLOC_NAME, 1, nDeviceNameLen + 1);     // a NULL character must be appended
            if (pGateway->pDeviceName)
            {
                memcpy (pGateway->pDeviceName, pDeviceName, nDeviceNameLen);
                _ObjectDictionary_TrimUTF8StringTrailingChar (pGateway->pDeviceName);                // Trim illegal UTF8 characters due to limit buffer size
            }
        }

        ret = 1;
    }
    else
    {
        // TODO: Support only one gateway
    }

    return ret;
}


void ObjectDictionary_DeleteGateway ()
{
    if (DKGateway.pData)
    {
        // Free DeviceName that is allocated by dxMemAlloc ()
        DK_BLEGateway_t* pGateway = (DK_BLEGateway_t*) DKGateway.pData;
        if (pGateway->pDeviceName)
        {
            dxMemFree (pGateway->pDeviceName);
        }
        dxMemFree (DKGateway.pData);
        DKGateway.pData = NULL;
        DKGateway.nMemSize = 0;

#if defined (DKOBJ_DUMP_MEM)
        AllocGatewayMemoryCount--;
#endif
    }
    memset (&DKGateway, 0x00, sizeof (DKGateway));

    if (DKPeripheral.pData) {

        DK_BLEPeripheral_t* pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;
        int i;

        for (i = 0; i < DKPeripheral.nItemCount; i++)
        {
            if (pPeripheral[i].pDeviceName)
            {
                dxMemFree (pPeripheral[i].pDeviceName);
                pPeripheral[i].pDeviceName = NULL;
            }

            memset (&pPeripheral[i], 0, sizeof (DK_BLEPeripheral_t));
        }
    }
    DKPeripheral.nItemCount = 0;

    if (DKPeripheralStatus.pData != NULL)
    {
        memset (DKPeripheralStatus.pData, 0x00, DKPeripheralStatus.nMemSize);
    }
    DKPeripheralStatus.nItemCount = 0;
}


DK_BLEGateway_t* ObjectDictionary_GetGateway ()
{
    if (DKGateway.nItemCount <= 0 || DKGateway.nMemSize <= 0 || DKGateway.pData == NULL) return NULL;

    DK_BLEGateway_t* pGateway = DKGateway.pData;

    return &pGateway[0];
}


uint32_t ObjectDictionary_GetGatewayObjectId (uint8_t* macaddr, int macaddrlen)
{
    DK_BLEGateway_t* p = ObjectDictionary_GetGateway ();

    if (p == NULL) return 0;

    if (memcmp (macaddr, p->MACAddress, macaddrlen) == 0)
    {
        return p->ObjectID;
    }

    return 0;
}


int ObjectDictionary_AllocPeripheralMemory (int count)
{
    if (count <= 0 || DKPeripheral.nItemCount > 0 || DKPeripheral.pData != NULL)
    {
        // Must free allocated memory
        return 0;   // False
    }

    // David Changed. Use calloc () instead of malloc () + memset ()
    //DKPeripheral.pData = malloc (sizeof (DK_BLEPeripheral_t) * count);
    //memset (DKPeripheral.pData, 0x00, sizeof (DK_BLEPeripheral_t) * count);
    DKPeripheral.pData = (void*) dxMemAlloc (CALLOC_NAME, 1, count * sizeof (DK_BLEPeripheral_t));

    if (DKPeripheral.pData == NULL)
    {
        return 0;   // False
    }

    DKPeripheral.nMemSize = sizeof (DK_BLEPeripheral_t) * count;
    DKPeripheral.nItemCount = 0;

#if defined (DKOBJ_DUMP_MEM)
    AllocPeripheralMemoryCount++;
#endif

    return 1;       // True
}


int ObjectDictionary_AddPeripheral (uint32_t objectID, uint8_t* macaddr, int macaddrlen, uint32_t gatewayID, uint16_t deviceType, char* fwv, uint8_t* seckey, uint8_t seckeylen, uint8_t* pDeviceName)
{
    DK_BLEPeripheral_t* pPeripheral = NULL;
    int count = DKPeripheral.nMemSize / sizeof (DK_BLEPeripheral_t);
    int n = DKPeripheral.nItemCount;
    uint8_t* pMem = NULL;
    int nMemSize = 0;
    int nItemCount = 0;

    if (macaddr == NULL || macaddrlen != 8 || objectID == 0 || gatewayID == 0)
    {
        // Must call ObjectDictionary_AllocPeripheralMemory () first
        return 0;   // False
    }

    if (ObjectDictionary_DoesPeripheralExist (macaddr, macaddrlen) == 1)
    {
        // macaddr exists in DKPeripheral
        return 0;
    }

    if (n >= count)
    {
        // Not enough memory, reallocate

        // Backup
        nMemSize = DKPeripheral.nMemSize;
        nItemCount = DKPeripheral.nItemCount;
        pMem = (uint8_t*) dxMemAlloc (CALLOC_NAME, 1, nMemSize * sizeof (char));
        memcpy (pMem, DKPeripheral.pData, nMemSize);

        // Free
        ObjectDictionary_FreePeripheralMemory (dxTRUE);

        // Reallocate memory
        if (ObjectDictionary_AllocPeripheralMemory (count + 1) == 0)
        {
            dxMemFree (pMem);
            pMem = NULL;

            return 0;
        }

        DKPeripheral.nItemCount = nItemCount;
        memcpy (DKPeripheral.pData, pMem, nMemSize);

        // Free backup memory
        dxMemFree (pMem);
        pMem = NULL;
    }

    if (DKPeripheral.nItemCount < DKPeripheral.nMemSize / sizeof (DK_BLEPeripheral_t))
    {
        // Get the free space of DK_BLEPeripheral_t
        pPeripheral =  (DK_BLEPeripheral_t*) DKPeripheral.pData;

        // Clear memory before using
        memset (&pPeripheral[n], 0x00, sizeof (DK_BLEPeripheral_t));

        pPeripheral[n].ObjectID        = objectID;
        pPeripheral[n].GatewayID       = gatewayID;
        pPeripheral[n].DeviceType      = deviceType;
        pPeripheral[n].nSecurityKeyLen = seckeylen;

        memcpy (pPeripheral[n].MACAddress,   macaddr,    macaddrlen);
        memcpy (pPeripheral[n].FWVersion,    fwv,        strlen ((char*) fwv));
        memcpy (pPeripheral[n].szSecurityKey,seckey,     seckeylen);

        if (pDeviceName)
        {
            int nDeviceNameLen = strlen ((char*) pDeviceName);
            if (nDeviceNameLen > DX_DEVICENAME_MAX_LEN)
            {
                nDeviceNameLen = DX_DEVICENAME_MAX_LEN;
            }

            pPeripheral[n].pDeviceName = dxMemAlloc (CALLOC_NAME, 1, nDeviceNameLen + 1);    // a NULL character must be appended
            if (pPeripheral[n].pDeviceName)
            {
                memcpy (pPeripheral[n].pDeviceName, pDeviceName, nDeviceNameLen);
                _ObjectDictionary_TrimUTF8StringTrailingChar (pPeripheral[n].pDeviceName);               // Trim illegal UTF8 characters due to limit buffer size
            }
        }

        DKPeripheral.nItemCount++;

    }

    return 1;   // True
}


void ObjectDictionary_DeletePeripheral (uint32_t objectID)
{
    int i = 0;
    int n = 0;
    int found = 0;  // False
    DK_BLEPeripheral_t* pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;

    for (i = 0; i < DKPeripheral.nItemCount; i++)
    {
        if (pPeripheral[i].ObjectID == objectID)
        {
            n = i;
            found = 1;  // True
            break;
        }
    }

    if (found == 1)
    {
        if (pPeripheral[n].pDeviceName)
        {
            dxMemFree (pPeripheral[n].pDeviceName);
            pPeripheral[n].pDeviceName = NULL;
        }

        for (i = n + 1; i < DKPeripheral.nItemCount; i++)
        {
            pPeripheral[i - 1].ObjectID        = pPeripheral[i].ObjectID;
            pPeripheral[i - 1].GatewayID       = pPeripheral[i].GatewayID;
            pPeripheral[i - 1].DeviceType      = pPeripheral[i].DeviceType;
            pPeripheral[i - 1].nSecurityKeyLen = pPeripheral[i].nSecurityKeyLen;
            pPeripheral[i - 1].pDeviceName     = pPeripheral[i].pDeviceName;

            memcpy (pPeripheral[i - 1].MACAddress,       pPeripheral[i].MACAddress,      sizeof (pPeripheral[i].MACAddress));
            memcpy (pPeripheral[i - 1].FWVersion,        pPeripheral[i].FWVersion,       sizeof (pPeripheral[i].FWVersion));
            memcpy (pPeripheral[i - 1].szSecurityKey,    pPeripheral[i].szSecurityKey,   sizeof (pPeripheral[i].szSecurityKey));
        }

        if (DKPeripheral.nItemCount > 0)
        {
            DKPeripheral.nItemCount--;
        }
    }
}


uint32_t ObjectDictionary_FindPeripheralObjectIdByMac (const uint8_t* macaddr, int macaddrlen)
{
    int i = 0;
    uint32_t objectID = 0;  // False
    DK_BLEPeripheral_t* pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;

    if (macaddr == NULL || macaddrlen != 8)
    {
        return 0;
    }

    for (i = 0; i < DKPeripheral.nItemCount; i++)
    {
        if (memcmp (pPeripheral[i].MACAddress, macaddr, macaddrlen) == 0)
        {
            objectID = pPeripheral[i].ObjectID; // True
            break;
        }
    }

    return objectID;
}


DK_BLEPeripheral_t* ObjectDictionary_GetPeripheral (uint32_t objectID)
{
    int i = 0;
    int found = 0;
    DK_BLEPeripheral_t* pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;

    if (DKPeripheral.nItemCount <= 0) return NULL;

    for (i = 0; i < DKPeripheral.nItemCount; i++)
    {
        if (pPeripheral[i].ObjectID == objectID)
        {
            found = 1;
            break;
        }
    }

    if (found == 0) return NULL;

    return &pPeripheral[i];
}


int ObjectDictionary_GetPeripheralCount ()
{
    return DKPeripheral.nItemCount;
}


DK_BLEPeripheral_t* ObjectDictionary_GetPeripheralByIndex (int index)
{
    DK_BLEPeripheral_t* pPeripheral = NULL;

    if (index < 0 || index >= DKPeripheral.nItemCount || DKPeripheral.pData == NULL)
    {
        return NULL;
    }

    pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;
    return &pPeripheral[index];
}


void ObjectDictionary_FreePeripheralMemory (dxBOOL bKeepDeviceName)
{
    if (DKPeripheral.pData != NULL)
    {
        if (bKeepDeviceName == dxFALSE)
        {
            int i = 0;
            int count = DKPeripheral.nItemCount;

            for (i = 0; i < count; i++)
            {
                DK_BLEPeripheral_t* pPeripheral = ObjectDictionary_GetPeripheralByIndex (i);
                if (pPeripheral)
                {
                    if (pPeripheral->pDeviceName)
                    {
                        dxMemFree (pPeripheral->pDeviceName);
                        pPeripheral->pDeviceName = NULL;
                    }
                }
            }
        }

        dxMemFree (DKPeripheral.pData);
        DKPeripheral.pData = NULL;
        DKPeripheral.nMemSize = 0;

#if defined (DKOBJ_DUMP_MEM)
        AllocPeripheralMemoryCount--;
#endif
    }

    memset (&DKPeripheral, 0x00, sizeof (DKPeripheral));
}


int ObjectDictionary_DoesPeripheralExist (uint8_t* macaddr, int macaddrlen)
{
    int i = 0;
    int count = DKPeripheral.nItemCount;
    int ret = 0;
    DK_BLEPeripheral_t* pPeripheral = NULL;

    if (macaddr == NULL ||
        macaddrlen != 8)
    {
        return 0;
    }

    for (i = 0; i < count; i++)
    {
        pPeripheral = ObjectDictionary_GetPeripheralByIndex (i);
        if (pPeripheral && memcmp (pPeripheral->MACAddress, macaddr, sizeof (pPeripheral->MACAddress)) == 0)
        {
            ret = 1;
            break;
        }
    }

    return ret;
}


int ObjectDictionary_ChangePeripheralDeviceName (uint32_t objectID, uint8_t* pDeviceName)
{
    int i = 0;
    int found = 0;
    DK_BLEPeripheral_t* pPeripheral = (DK_BLEPeripheral_t*) DKPeripheral.pData;

    if (DKPeripheral.nItemCount <= 0) return 0;

    for (i = 0; i < DKPeripheral.nItemCount; i++)
    {
        if (pPeripheral[i].ObjectID == objectID)
        {
            found = 1;

            if (pPeripheral[i].pDeviceName)
            {
                dxMemFree (pPeripheral[i].pDeviceName);
                pPeripheral[i].pDeviceName = NULL;
            }

            if (pDeviceName)
            {
                int nDeviceNameLen = strlen ((char*) pDeviceName);
                if (nDeviceNameLen > DX_DEVICENAME_MAX_LEN)
                {
                    nDeviceNameLen = DX_DEVICENAME_MAX_LEN;
                }

                pPeripheral[i].pDeviceName = dxMemAlloc (CALLOC_NAME, 1, nDeviceNameLen + 1);    // a NULL character must be appended
                if (pPeripheral[i].pDeviceName)
                {
                    memcpy (pPeripheral[i].pDeviceName, pDeviceName, nDeviceNameLen);
                    _ObjectDictionary_TrimUTF8StringTrailingChar (pPeripheral[i].pDeviceName);               // Trim illegal UTF8 characters due to limit buffer size
                }
            }
        }
    }

    return found;
}


int ObjectDictionary_AllocPeripheralStatusMemory (int count)
{
    if (count <= 0 || DKPeripheralStatus.nItemCount > 0 || DKPeripheralStatus.pData != NULL)
    {
        // Must free allocated memory
        return 0;   // False
    }

    // David Changed. Use calloc () instead of malloc () + memset ()
    //DKPeripheralStatus.pData = malloc (sizeof (DK_BLEPeripheralStatus_t) * count);
    DKPeripheralStatus.pData = (void*) dxMemAlloc (CALLOC_NAME, 1, count * sizeof (DK_BLEPeripheralStatus_t));
    if (DKPeripheralStatus.pData == NULL)
    {
        return 0;   // False
    }

    DKPeripheralStatus.nMemSize = sizeof (DK_BLEPeripheralStatus_t) * count;
    DKPeripheralStatus.nItemCount = 0;

#if defined (DKOBJ_DUMP_MEM)
    AllocPeripheralStatusMemoryCount++;
#endif

    return 1;       // True
}


int ObjectDictionary_AddPeripheralStatus (uint32_t objectID, uint32_t bleObjectID)
{
    DK_BLEPeripheralStatus_t* pPeripheralStatus = NULL;
    int count = DKPeripheralStatus.nMemSize / sizeof (DK_BLEPeripheralStatus_t);
    int n = DKPeripheralStatus.nItemCount;
    uint8_t* pMem = NULL;
    int nMemSize = 0;
    int nItemCount = 0;

    if (objectID == 0 || bleObjectID == 0)
    {
        return 0;
    }

    if (ObjectDictionary_DoesPeripheralStatusExist (objectID, bleObjectID) == 1)
    {
        // (objectID, bleObjectID) exists in DKPeripheralStatus
        return 0;
    }

    // First allocate memory
    if (count == 0)
    {
        if (ObjectDictionary_AllocPeripheralStatusMemory (1) == 0)
        {
            return 0;
        }

        count = DKPeripheralStatus.nMemSize / sizeof (DK_BLEPeripheralStatus_t);
        n = DKPeripheralStatus.nItemCount;
    }

    if (n >= count)
    {
        // Not enough memory, reallocate

        // Backup
        nMemSize = DKPeripheralStatus.nMemSize;
        nItemCount = DKPeripheralStatus.nItemCount;
        pMem = (uint8_t*) dxMemAlloc (CALLOC_NAME, 1, nMemSize * sizeof (char));

        if (pMem == NULL)
        {
            G_SYS_DBG_LOG_IV (G_SYS_DBG_CATEGORY_SERVERMANAGER_EVENTREPORTHANDLER, G_SYS_DBG_LEVEL_FATAL_ERROR,
                     "[%s] Line %d, dxMemAlloc (%d) == NULL\n",
                     __FUNCTION__,
                     __LINE__,
                     nMemSize);

            return 0;
        }

        memcpy (pMem, DKPeripheralStatus.pData, nMemSize);

        // Free
        ObjectDictionary_FreePeripheralStatusMemory ();

        // Reallocate memory
        if (ObjectDictionary_AllocPeripheralStatusMemory (count + 1) == 0)
        {
            dxMemFree (pMem);
            pMem = NULL;

            return 0;
        }

        DKPeripheralStatus.nItemCount = nItemCount;
        memcpy (DKPeripheralStatus.pData, pMem, nMemSize);

        // Free backup memory
        dxMemFree (pMem);
        pMem = NULL;
    }

    if (DKPeripheralStatus.nItemCount < DKPeripheralStatus.nMemSize / sizeof (DK_BLEPeripheralStatus_t));
    {
        // Get the free space of DK_BLEPeripheralStatus_t
        pPeripheralStatus =  (DK_BLEPeripheralStatus_t*) DKPeripheralStatus.pData;

        pPeripheralStatus[n].ObjectID = objectID;
        pPeripheralStatus[n].BLEObjectID = bleObjectID;

        DKPeripheralStatus.nItemCount++;
    }

    return 1;   // True
}


void ObjectDictionary_DeletePeripheralStatus (uint32_t objectID)
{
    int i = 0;
    int n = 0;
    int found = 0;  // False
    DK_BLEPeripheralStatus_t* pPeripheralStatus = (DK_BLEPeripheralStatus_t*) DKPeripheralStatus.pData;

    for (i = 0; i < DKPeripheralStatus.nItemCount; i++)
    {
        if (pPeripheralStatus[i].ObjectID == objectID)
        {
            n = i;
            found = 1;  // True
            break;
        }
    }

    if (found == 1)
    {
        for (i = n + 1; i < DKPeripheralStatus.nItemCount; i++)
        {
            pPeripheralStatus[i - 1].ObjectID = pPeripheralStatus[i].ObjectID;
            pPeripheralStatus[i - 1].BLEObjectID = pPeripheralStatus[i].BLEObjectID;
        }

        if (DKPeripheralStatus.nItemCount > 0)
        {
            DKPeripheralStatus.nItemCount--;
        }
    }
}


uint32_t ObjectDictionary_FindPeripheralStatusObjectIdByPeripheralObjectId (uint32_t bleObjectID)
{
    int i = 0;
    uint32_t objectID = 0;  // False
    DK_BLEPeripheralStatus_t* pPeripheralStatus = (DK_BLEPeripheralStatus_t*) DKPeripheralStatus.pData;

    if (DKPeripheralStatus.nItemCount <= 0) return 0;

    for (i = 0; i < DKPeripheralStatus.nItemCount; i++)
    {
        if (pPeripheralStatus[i].BLEObjectID == bleObjectID)
        {
            objectID = pPeripheralStatus[i].ObjectID;   // True
            break;
        }
    }

    return objectID;
}


int ObjectDictionary_GetPeripheralStatusCount ()
{
    return DKPeripheralStatus.nItemCount;
}


DK_BLEPeripheralStatus_t* ObjectDictionary_GetPeripheralStatusByIndex (int index)
{
    DK_BLEPeripheralStatus_t* pPeripheralStatus = NULL;

    if (index < 0 || index >= DKPeripheralStatus.nItemCount || DKPeripheralStatus.pData == NULL)
    {
        return NULL;
    }

    pPeripheralStatus = (DK_BLEPeripheralStatus_t*) DKPeripheralStatus.pData;
    return &pPeripheralStatus[index];
}


void ObjectDictionary_FreePeripheralStatusMemory ()
{
    if (DKPeripheralStatus.pData != NULL)
    {
        dxMemFree (DKPeripheralStatus.pData);
        DKPeripheralStatus.pData = NULL;
        DKPeripheralStatus.nMemSize = 0;

#if defined (DKOBJ_DUMP_MEM)
        AllocPeripheralStatusMemoryCount--;
#endif
    }

    memset (&DKPeripheralStatus, 0x00, sizeof (DKPeripheralStatus));
}


DK_BLEPeripheralStatus_t* ObjectDictionary_FindPeripheralStatusByPeripheralMac (uint8_t* macaddr, int macaddrlen)
{
    DK_BLEPeripheralStatus_t* pPeripheralStatus = (DK_BLEPeripheralStatus_t*) DKPeripheralStatus.pData;
    int i = 0;
    uint32_t n = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) macaddr, macaddrlen);
    int found = -1;

    if (n == 0 || DKPeripheralStatus.nItemCount <= 0 || pPeripheralStatus == NULL) return NULL;

    for (i = 0; i < DKPeripheralStatus.nItemCount; i++)
    {
        if (pPeripheralStatus[i].BLEObjectID == n)
        {
            found = i;
            break;
        }
    }

    if (found < 0) return NULL;

    return &pPeripheralStatus[found];
}


int ObjectDictionary_DoesPeripheralStatusExist (uint32_t objectID, uint32_t bleObjectID)
{
    int i = 0;
    int count = DKPeripheralStatus.nItemCount;
    int ret = 0;
    DK_BLEPeripheralStatus_t* pStatus = NULL;

    for (i = 0; i < count; i++)
    {
        pStatus = ObjectDictionary_GetPeripheralStatusByIndex (i);
        if (pStatus && pStatus->ObjectID == objectID && pStatus->BLEObjectID == bleObjectID)
        {
            ret = 1;
            break;
        }
    }

    return ret;
}


dxTime_t ObjectDictionary_GetLastTcpConnectedTime (void)
{
    return tmLastTCPConnectedTime;
}


void ObjectDictionary_SaveCurrentTcpConnectedTime (void)
{
    tmLastTCPConnectedTime = dxTimeGetMS ();
}


void ObjectDictionary_Lock (void)
{
    LOCK (&mutex);
}


void ObjectDictionary_Unlock (void)
{
    UNLOCK (&mutex);
}


void ObjectDictionary_ClearAllocGatewayPeripheralMem (void)
{
    ObjectDictionary_FreePeripheralStatusMemory ();
    ObjectDictionary_FreePeripheralMemory (dxFALSE);
    ObjectDictionary_DeleteGateway ();
    ObjectDictionary_DeleteUser ();

    _ObjectDictionary_ClearMemory ();
}


void ObjectDictionary_Uninit ()
{
    peripheralUpdateTime = 0;
    scheduleJobUpdateTime = 0;
    advanceJobUpdateTime = 0;
    ObjectDictionary_ClearAllocGatewayPeripheralMem ();

    memset (szApplicationID, 0x00, sizeof (szApplicationID));
    memset (szAPIKey,        0x00, sizeof (szAPIKey));
    memset (szSessionToken,  0x00, sizeof (szSessionToken));
    memset (szMACAddress,    0x00, sizeof (szMACAddress));
    memset (szHashCode,      0x00, sizeof (szHashCode));
    memset (szHostname,      0x00, sizeof (szHostname));
}


#if defined (DKOBJ_DUMP_MEM)
void ObjectDictionary_DumpMemory ()
{
    G_SYS_DBG_LOG_V ("++++++ dk_objects ++++++\n");
    G_SYS_DBG_LOG_V ("DKUser: %d, %d\n", DKUser.nMemSize, AllocUserMemoryCount);
    G_SYS_DBG_LOG_V ("DKGateway: %d, %d\n", DKGateway.nMemSize, AllocGatewayMemoryCount);
    G_SYS_DBG_LOG_V ("DKPeripheral: %d, %d\n", DKPeripheral.nMemSize, AllocPeripheralMemoryCount);
    G_SYS_DBG_LOG_V ("DKPeripheralStatus: %d, %d\n", DKPeripheralStatus.nMemSize, AllocPeripheralStatusMemoryCount);
    G_SYS_DBG_LOG_V ("------ dk_objects ------\n");
}
#endif


/*---- Private Function ----------------------------------------------------*/

void _ObjectDictionary_ClearMemory ()
{
    memset ((void*) &DKUser,                 0x00, sizeof (DKUser));
    memset ((void*) &DKGateway,              0x00, sizeof (DKGateway));
    memset ((void*) &DKPeripheral,           0x00, sizeof (DKPeripheral));
    memset ((void*) &DKPeripheralStatus,     0x00, sizeof (DKPeripheralStatus));
    memset ((void*) &tmLastTCPConnectedTime, 0x00, sizeof (tmLastTCPConnectedTime));
}


/**
 * @brief trim illegal UTF8 characters
 */
void _ObjectDictionary_TrimUTF8StringTrailingChar (uint8_t* data)
{
    if (data)
    {
        unsigned int len = strlen ((char*) data);
        unsigned int i = 0;

        while (i < len)
        {
            uint8_t ch = data[i];

            if ((ch & 0x080) == 0)
            {
                // 1 byte
                i++;
            }
            else if ((ch & 0x0E0) == 0x0C0)
            {
                // 2 bytes

                if (i + 2 > len)
                {
                    break;
                }

                i += 2;
            }
            else if ((ch & 0x0F0) == 0x0E0)
            {
                // 3 bytes

                if (i + 3 > len)
                {
                    break;
                }

                i += 3;
            }
            else if ((ch & 0x0F8) == 0x0F0)
            {
                // 4 bytes

                if (i + 4 > len)
                {
                    break;
                }

                i += 4;
            }
            else if ((ch & 0x0FC) == 0x0F8)
            {
                // 5 bytes

                if (i + 5 > len)
                {
                    break;
                }

                i += 5;
            }
            else if ((ch & 0x0FE) == 0x0FC)
            {
                // 6 bytes

                if (i + 6 > len)
                {
                    break;
                }

                i += 6;
            }
            else
            {
                // error
                break;
            }
        }

        data[i] = 0x00;
    }
}
