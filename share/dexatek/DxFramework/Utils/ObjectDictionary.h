//============================================================================
// File: dk_objects.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.161.0
//     Date: 2015/06/23
//     1) Description: Support QA site redirection
//        Added:
//            - ObjectDictionary_SetApiVersion ()
//
//     Version: 0.161.1
//     Date: 2016/01/27
//     1) Description: Revise to use dxOS wrapper function
//
//     Version: 0.161.2
//     Date: 2017/03/01
//     1) Description: Store DeviceName in memory for later use
//        Modified:
//            - Structure DK_BLEGateway_t and DK_BLEPeripheral_t
//            - ObjectDictionary_AddPeripheral ()
//            - ObjectDictionary_AddGateway ()
//
//     Version: 0.161.3
//     Date: 2017/03/02
//     1) Description: Change peripheral's DeviceName
//        Added:
//            - ObjectDictionary_ChangePeripheralDeviceName ()
//
//     Version: 0.161.4
//     Date: 2017/04/06
//     1) Description: Use Mutex, instead of Semaphore
//        Modified:
//            - LOCK ()
//            - UNLOCK ()
//
//     Version: 0.223.0
//     Date: 2017/09/27
//     1) Description: Support login with "DKAuth"
//        Modified:
//            - Added DK_LOGINTYPE_ANONYMOUS to DK_LoginType_t
//============================================================================
#ifndef __OBJECT_DICTIONARY_H__
#define __OBJECT_DICTIONARY_H__

#pragma once

#include "dxOS.h"
#include "dxNET.h"

#ifdef __cplusplus
extern "C" {
#endif

//============================================================================
//    Define
//============================================================================

#define USE_MUTEX

#if defined(USE_MUTEX)
#define LOCK(x)                                     dxMutexTake(*(x), DXOS_WAIT_FOREVER)
#define UNLOCK(x)                                   dxMutexGive(*(x))
#else
#define LOCK(x)
#define UNLOCK(x)
#endif

#define LOG_IN_USERNAME_MAX_LEN             64
#define LOG_IN_PASSWORD_MAX_LEN             32
#define LOG_IN_ACCESSTOKEN_MAX_LEN          256



//============================================================================
//    Enumerations
//============================================================================

typedef enum //_enumLoginType
{
    DK_LOGINTYPE_UNKNOWN            = 0,
    DK_LOGINTYPE_SERVER,
    DK_LOGINTYPE_FACEBOOK,
    DK_LOGINTYPE_GATEWAY,
    DK_LOGINTYPE_ANONYMOUS,
} DK_LoginType_t;



//============================================================================
//    Structures
//============================================================================

/**
 * @brief UserLogin_t structure
 */
typedef struct _tagUserLogin
{
    uint8_t         Username[LOG_IN_USERNAME_MAX_LEN];
    uint8_t         Password[LOG_IN_PASSWORD_MAX_LEN];
    DK_LoginType_t  LoginType;
} UserLogin_t;


/**
 * @brief UserLogin_t structure
 */
typedef struct _tagUsers
{
    uint32_t        ObjectID;                           // int          4
    uint32_t        ParentObjectID;                     // int          4
    UserLogin_t     UserInfo;
} DK_Users_t;


/**
 * @brief UserLogin_t structure
 */
typedef struct _tagBLEGateway
{
    uint32_t        ObjectID;                           // int          4
    uint8_t         MACAddress[8];                      // char         8
    char            WiFiVersion[16];                    // char         16
    char            BLEVersion[16];                     // char         16
    uint8_t*        pDeviceName;                        // uint8_t *    4
} DK_BLEGateway_t;


/**
 * @brief UserLogin_t structure
 */
typedef struct _tagBLEPeripheral
{
    uint32_t        ObjectID;                           // int          4
    uint8_t         MACAddress[8];                      // char         8
    char            FWVersion[16];                      // char         16
    uint32_t        GatewayID;                          // int          4
    uint16_t        DeviceType;                         // smallint     2
    uint8_t         szSecurityKey[16];                  // char         16
    uint8_t         nSecurityKeyLen;                    // char         1
    uint8_t*        pDeviceName;                        // uint8_t *    4
} DK_BLEPeripheral_t;


/**
 * @brief UserLogin_t structure
 */
typedef struct _tagBLEPeripheralStatus
{
    uint32_t        ObjectID;                           // int          4
    uint32_t        BLEObjectID;                        // int          4
} DK_BLEPeripheralStatus_t;


/**
 * @brief UserLogin_t structure
 */
typedef struct _tagDKMem
{
    uint16_t        nItemCount;
    uint16_t        nMemSize;                           // nItemCount and nMemSize are not included.
    void*           pData;                              // Pointing to structure DK_Users_t/DK_DeviceAccessControl_t/DK_BLEGateway_t/
                                                        // DK_BLEPeripheral_t/DK_BLEHistory_t/DK_BLEPeripheralStatus_t/
                                                        // DK_BLESchedule_t/DK_BLEJobQueue_t
} DK_Mem_t;



//============================================================================
//    Function Declarations
//============================================================================

/*---- Init Function -------------------------------------------------------*/

/**
 * @brief ObjectDictionary_Init
 */
void ObjectDictionary_Init (uint8_t* macaddr, int macaddrlen, uint8_t* dkServerName, uint8_t* appID, uint8_t* apiKey, uint8_t* apiVer);


/*---- REST API, HTTP Header Function --------------------------------------*/

/**
 * @brief ObjectDictionary_GetApplicationId
 */
char* ObjectDictionary_GetApplicationId ();


/**
 * @brief ObjectDictionary_GetApiKey
 */
char* ObjectDictionary_GetApiKey ();


/**
 * @brief ObjectDictionary_GetApiVersion
 */
char* ObjectDictionary_GetApiVersion ();


/**
 * @brief ObjectDictionary_SetApiVersion
 */
void ObjectDictionary_SetApiVersion (char* pValue);


/**
 * @brief ObjectDictionary_GetPeripheralUpdatetime
 */
uint64_t ObjectDictionary_GetPeripheralUpdatetime (void);


/**
 * @brief ObjectDictionary_SetPeripheralUpdatetime
 */
void ObjectDictionary_SetPeripheralUpdatetime (uint64_t updatedtime);


/**
 * @brief ObjectDictionary_GetScheduleJobUpdatetime
 */
uint64_t ObjectDictionary_GetScheduleJobUpdatetime (void);


/**
 * @brief ObjectDictionary_SetScheduleJobUpdatetime
 */
void ObjectDictionary_SetScheduleJobUpdatetime (uint64_t updatedtime);


/**
 * @brief ObjectDictionary_GetAdvanceJobUpdatetime
 */
uint64_t ObjectDictionary_GetAdvanceJobUpdatetime (void);


/**
 * @brief ObjectDictionary_SetAdvanceJobUpdatetime
 */
void ObjectDictionary_SetAdvanceJobUpdatetime (uint64_t updatedtime);


/**
 * @brief ObjectDictionary_GetSessionToken
 */
char* ObjectDictionary_GetSessionToken ();


/**
 * @brief ObjectDictionary_SetSessionToken
 */
void ObjectDictionary_SetSessionToken (char* pValue);


/**
 * @brief ObjectDictionary_GetMacAddress
 */
char* ObjectDictionary_GetMacAddress ();


/**
 * @brief ObjectDictionary_GetHashCode
 */
char* ObjectDictionary_GetHashCode ();


/**
 * @brief ObjectDictionary_SetHashCode
 */
void ObjectDictionary_SetHashCode (char* pValue);


/**
 * @brief ObjectDictionary_GetHostname
 */
char* ObjectDictionary_GetHostname ();


/**
 * @brief ObjectDictionary_SetHostname
 */
void ObjectDictionary_SetHostname (char* pValue);


/**
 * @brief ObjectDictionary_GetHostIp
 */
dxIP_Addr_t* ObjectDictionary_GetHostIp ();

/**
 * @brief ObjectDictionary_SetHostIp
 */
void ObjectDictionary_SetHostIp (dxIP_Addr_t ip);


/*---- Login HashCode Function ---------------------------------------------*/

/**
 * @brief ObjectDictionary_CreateHashCode
 */
int ObjectDictionary_CreateHashCode (char* username, char* password, char* hashcode, int hashcodelen);


/*---- User Table Function -------------------------------------------------*/

/**
 * @brief ObjectDictionary_AddUser
 */
int ObjectDictionary_AddUser (uint32_t objectID, uint32_t parentObjectID, UserLogin_t* pLogin);


/**
 * @brief ObjectDictionary_DeleteUser
 */
void ObjectDictionary_DeleteUser (void);


/**
 * @brief ObjectDictionary_GetUser
 */
DK_Users_t* ObjectDictionary_GetUser ();


/*---- Gateway Table Function ----------------------------------------------*/


/**
 * @brief ObjectDictionary_AddGateway
 */
int ObjectDictionary_AddGateway (uint32_t objectID, uint8_t* macaddr, int macaddrlen, char* wifiv, char* blev, uint8_t* pDeviceName);


/**
 * @brief ObjectDictionary_DeleteGateway
 */
void ObjectDictionary_DeleteGateway ();


/**
 * @brief ObjectDictionary_GetGateway
 */
DK_BLEGateway_t* ObjectDictionary_GetGateway ();


/**
 * @brief ObjectDictionary_GetGatewayObjectId
 */
uint32_t ObjectDictionary_GetGatewayObjectId (uint8_t* macaddr, int macaddrlen);


/*---- Peripheral Table Function -------------------------------------------*/

/**
 * @brief ObjectDictionary_AllocPeripheralMemory
 */
int ObjectDictionary_AllocPeripheralMemory (int count);


/**
 * @brief ObjectDictionary_AddPeripheral
 */
int ObjectDictionary_AddPeripheral (uint32_t objectID, uint8_t* macaddr, int macaddrlen, uint32_t gatewayID, uint16_t deviceType, char* fwv, uint8_t* seckey, uint8_t seckeylen, uint8_t* pDeviceName);


/**
 * @brief ObjectDictionary_DeletePeripheral
 */
void ObjectDictionary_DeletePeripheral (uint32_t objectID);


/**
 * @brief ObjectDictionary_FindPeripheralObjectIdByMac
 */
uint32_t ObjectDictionary_FindPeripheralObjectIdByMac (const uint8_t* macaddr, int macaddrlen);


/**
 * @brief ObjectDictionary_GetPeripheral
 */
DK_BLEPeripheral_t* ObjectDictionary_GetPeripheral (uint32_t objectID);


/**
 * @brief ObjectDictionary_GetPeripheralCount
 */
int ObjectDictionary_GetPeripheralCount ();


/**
 * @brief ObjectDictionary_GetPeripheralByIndex
 */
DK_BLEPeripheral_t* ObjectDictionary_GetPeripheralByIndex (int index);


/**
 * @brief ObjectDictionary_FreePeripheralMemory
 */
void ObjectDictionary_FreePeripheralMemory (dxBOOL bKeepDeviceName);


/**
 * @brief ObjectDictionary_DoesPeripheralExist
 */
int ObjectDictionary_DoesPeripheralExist (uint8_t* macaddr, int macaddrlen);


/**
 * @brief ObjectDictionary_ChangePeripheralDeviceName
 */
int ObjectDictionary_ChangePeripheralDeviceName (uint32_t objectID, uint8_t* pDeviceName);


/*---- PeripheralStatus Table Function -------------------------------------*/

/**
 * @brief ObjectDictionary_AllocPeripheralStatusMemory
 */
int ObjectDictionary_AllocPeripheralStatusMemory (int count);


/**
 * @brief ObjectDictionary_AddPeripheralStatus
 */
int ObjectDictionary_AddPeripheralStatus (uint32_t objectID, uint32_t bleObjectID);


/**
 * @brief ObjectDictionary_DeletePeripheralStatus
 */
void ObjectDictionary_DeletePeripheralStatus (uint32_t objectID);


/**
 * @brief ObjectDictionary_FindPeripheralStatusObjectIdByPeripheralObjectId
 */
uint32_t ObjectDictionary_FindPeripheralStatusObjectIdByPeripheralObjectId (uint32_t bleObjectID);


/**
 * @brief ObjectDictionary_GetPeripheralStatusCount
 */
int ObjectDictionary_GetPeripheralStatusCount ();


/**
 * @brief ObjectDictionary_GetPeripheralStatusByIndex
 */
DK_BLEPeripheralStatus_t* ObjectDictionary_GetPeripheralStatusByIndex (int index);


/**
 * @brief ObjectDictionary_FreePeripheralStatusMemory
 */
void ObjectDictionary_FreePeripheralStatusMemory ();


/**
 * @brief ObjectDictionary_DoesPeripheralStatusExist
 */
int ObjectDictionary_DoesPeripheralStatusExist (uint32_t objectID, uint32_t bleObjectID);


/*---- Peripheral/PeripheralStatus table relative Function -----------------*/

/**
 * @brief ObjectDictionary_FindPeripheralStatusByPeripheralMac
 */
DK_BLEPeripheralStatus_t* ObjectDictionary_FindPeripheralStatusByPeripheralMac (uint8_t* macaddr, int macaddrlen);


/**
 * @brief ObjectDictionary_GetLastTcpConnectedTime
 */
dxTime_t ObjectDictionary_GetLastTcpConnectedTime (void);


/**
 * @brief ObjectDictionary_SaveCurrentTcpConnectedTime
 */
void ObjectDictionary_SaveCurrentTcpConnectedTime (void);


/*---- Lock/Unlock Function ------------------------------------------------*/

/**
 * @brief ObjectDictionary_Lock
 */
void ObjectDictionary_Lock (void);


/**
 * @brief ObjectDictionary_Unlock
 */
void ObjectDictionary_Unlock (void);


#if defined (DKOBJ_DUMP_MEM)
/**
 * @brief ObjectDictionary_DumpMemory
 */
void ObjectDictionary_DumpMemory ();
#endif


/**
 * @brief ObjectDictionary_ClearAllocGatewayPeripheralMem
 */
void ObjectDictionary_ClearAllocGatewayPeripheralMem (void);


/**
 * @brief ObjectDictionary_Uninit
 */
void ObjectDictionary_Uninit ();


#ifdef __cplusplus
}
#endif

#endif // _DK_OBJECTS_H
