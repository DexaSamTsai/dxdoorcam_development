//============================================================================
// File: JobManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/10
//     1) Description: Initial
//
//============================================================================

#include <stdlib.h>
#include "JobManager.h"
#include "dk_Job.h"
#include "dk_DkManagerConfig.h"

//============================================================================
// Macros
//============================================================================

//Job Manager project feature setting
#define ENABLE_MOBILE_CLIENT_SESSION_TIME_OUT_CHECK             1
#define ENABLE_AUTOMATIC_PRESET_SCHEDULED_JOB_CREATION          0

//Job Manager project config
#define MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED                  (MAXIMUM_DEVICE_KEEPER_ALLOWED * 3)   //50 //(MAXIMUM_DEVICE_KEEPER_ALLOWED*2)
#define MAXIMUM_ADVANCE_JOB_LIST_ALLOWED                    (MAXIMUM_DEVICE_KEEPER_ALLOWED * 2)   //25
#define MAXIMUM_JOB_ID_INFO_LIST_ALLOWED                    10

#define MAXIMUM_JOB_MANAGER_JOB_WAIT_TIME                   (1*SECONDS)

#define JOB_SCHEDULE_NO_CONDITION_CHECK_MIN_INTERVAL        (15*SECONDS)	    //DO NOT Exceed 30 Seconds, recommended to set between 5 to 15 Sec.

#define ADV_JOB_ID_INFO_RECORD_EXPIRE_TIME_MAX              (120*SECONDS)   //2 Minutes

#define MAXIMUM_JOB_MANAGER_JOB_QUEUE_DEPT_ALLOWED          30

#define JOB_MANAGER_PROCESS_THREAD_PRIORITY                 DXTASK_PRIORITY_NORMAL
#define JOB_MANAGER_PROCESS_THREAD_NAME                     "Job Manager Process"
#define JOB_MANAGER_STACK_SIZE                              1536

#define JOB_MANAGER_DEFAULT_WORKER_PRIORITY                 DXTASK_PRIORITY_NORMAL
#define JOB_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE           1024
#define JOB_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE      (MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED + MAXIMUM_ADVANCE_JOB_LIST_ALLOWED)

#define JOB_PERIPHERAL_BATTERY_UPDATE_DIFF_THRESHOLD        8   //Difference in Percent

#if DEVICE_IS_HYBRID
#define HYBRID_PERIPHERAL_DATA_HANDLER_ID                   MAXIMUM_DEVICE_KEEPER_ALLOWED
#endif

//============================================================================
// Constants
//============================================================================

#define NO_TIME_OUT_TIME        0

//============================================================================
// Enumerations
//============================================================================
typedef enum
{

    JOB_MANAGER_NO_JOB                = 0,
    JOB_MANAGER_PERIPHERAL_STATUS_NEW_UPDATE,
    JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE,
    JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE_FIRST_TIME,
    JOB_MANAGER_PERIPHERAL_DATA_HISTORIC_EVENT_UPDATE_ONLY,
    JOB_MANAGER_PERIPHERAL_DATA_UPDATE_ALIVE,
    JOB_MANAGER_NEW_JOB,
    JOB_MANAGER_NEW_ADV_JOB,
    JOB_MANAGER_ADV_JOB_SINGLE_PACK_EXEC,
    JOB_MANAGER_CLEAN_ALL_JOB,
    JOB_MANAGER_REMOVE_PERIPHERAL_ALL_SPECIFIC_SCHEDULED_JOB,
    JOB_MANAGER_PERIPHERAL_JOB_CHECK,   //This will only do a Job (schedule & Advance) check with provided peripheral data

} JobManager_Job_t;

//============================================================================
// Type Definitions
//============================================================================
typedef struct
{
    dxBOOL              SlotAvailable;
    dxTime_t            JobLastExecutedTime;
    dxTime_t            JobIntervalUpcomingExecuteTime;
    uint64_t            LastConditionMap;
    uint16_t            JobDataLen;
    uint8_t*            JobData;
} JobSchedule_t;

typedef struct
{
    dxBOOL          SlotAvailable;
    dxTime_t        AdvJobLastExecutedTime;
    dxTime_t        AdvJobFirstCheckTick;
    uint32_t        AdvanceJobID;
    uint8_t         AdvJobConditionState; //0 - Unknown, 1 - All condition met, 2 - At least one condition is not met
    uint8_t         AdvJobLastTotalConditionReached;
    uint16_t        JobConditionDataLen;
    uint16_t        JobExecutionDataLen;
    uint8_t*        JobConditionData;
    uint8_t*        JobExecutionData;
} AdvanceJob_t;


typedef struct
{
    ePeripheralSignalStrength   LastUpdatedSignalStrength;
    uint8_t                     SignalStrengthConsDiffCount;
    uint8_t                     LastUpdatedBatteryInfo;
    dxTime_t                    BatteryLevelNormalConditionSince;
    uint16_t                    LastKnowStatus;
    uint16_t                    DataTypePayloadCRC16;	//The CRC of AdData in DkAdvData_t
    dxTime_t                    LastUpdatedPayloadTime;
    dxBOOL                      bForceUpdateData;
    dxBOOL                      bForceUpdateStatus;
} JobManagerPeripheral_t;


typedef struct
{
    JobManager_Job_t        JobType;
    uint16_t                JobSourceOfOrigin;
    uint64_t                JobIdentificationNumber;
    uint32_t                JobAdvId;
    dxTime_t                JobTimeOutTime;
    dxTime_t                JobTimeStamp;       //This is the time in UTC+0 when it was originally created
    uint16_t                JobDataLen;
    uint8_t*                JobData;

} JobManagerJobQueue_t; //This MUST be sent as pointer instead of whole data

typedef struct
{
    JobEventObjHeader_t     Header;
    uint8_t*                pData;
} JobManagerEventReportObj_t;


typedef struct
{
    uint16_t     JobDataLen;

} JobScheduleBackupHeader_t;

typedef struct
{
    JobScheduleBackupHeader_t   Header;
    uint8_t                     JobData[0];
} JobScheduleBackup_t;

typedef struct
{
    uint32_t        AdvanceJobID;
    uint16_t        JobConditionDataLen;
    uint16_t        JobExecutionDataLen;

} AdvanceJobBackupHeader_t;

typedef struct
{
    AdvanceJobBackupHeader_t    Header;
    uint8_t                     JobMergedData[0];
} AdvanceJobBackup_t;

//============================================================================
// Structures
//============================================================================


//============================================================================
// Function Declarations
//============================================================================

static TASK_RET_TYPE Job_Manager_Process_thread_main( TASK_PARAM_TYPE arg );

static int16_t FindAvailableScheduleListSlot();

static int16_t AddScheduleJobToList(int16_t AddToSlot, uint8_t* cJobData, uint16_t cJobDataLen);

static void ProcessScheduledConditionFromNewDataUpdate(DeviceInfoReport_t* pDevInfoReportData);

//static dxJOB_MANAGER_RET_CODE JobManagerUtcTimeUpdateByTick(void);


//============================================================================
// Variable Definitions
//============================================================================

//The index is same as from BLEDeviceKeeper's device index

#if DEVICE_IS_HYBRID
//extend 1 slot for Hybrid Peripheral Data Satatus
static JobManagerPeripheral_t   PeripheralsDataStatus[MAXIMUM_DEVICE_KEEPER_ALLOWED + 1];
#else
static JobManagerPeripheral_t   PeripheralsDataStatus[MAXIMUM_DEVICE_KEEPER_ALLOWED];
#endif  //#if DEVICE_IS_HYBRID

static JobSchedule_t            JobScheduledList[MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED];

static AdvanceJob_t             AdvanceJobList[MAXIMUM_ADVANCE_JOB_LIST_ALLOWED];

static dxTaskHandle_t           JobMainProcess_thread;

static dxWorkweTask_t           EventWorkerThread;

static dxQueueHandle_t          JobManagerJobQueue;

static dxEventHandler_t         RegisteredEventFunction;

//static DKTime_t                 CurrentUTCTime;
//static uint32_t                 CurrentUTCinSec;

static dxBOOL                   MobileClientIsListening = dxTRUE;
static dxTime_t                 MobileClientSessionTimeOutTime = 0;

static JobIdInfo_t              JobIdInfoList[MAXIMUM_JOB_ID_INFO_LIST_ALLOWED];

static dxSemaphoreHandle_t      JobAccessResourceProtectionSemaHandle = NULL;

static dxBOOL                   bJobSuperiorOnlyEnabled = dxFALSE;

static dxBOOL                   bServerIsReady = dxFALSE;

//============================================================================
// Local Function Definitions
//============================================================================

static void _RemoveAllScheduleJobs (dxBOOL bSkipReserved)
{
    uint16_t i = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        if(bSkipReserved == dxTRUE)
        {
            JobSchedule_t* pJobSchedule = &JobScheduledList[i];
            JobHeader_t* JobHeader = (JobHeader_t*)pJobSchedule->JobData;

            if(JobHeader->ScheduleSequenceNumber.Component.UniqueSequenceID >= MIN_SCHEDULE_JOB_RESERVED_UNIQUE_ID)
            {
                //skip removing reserved Schedule Job ID
                continue;
            }
        }

        JobScheduledList[i].SlotAvailable = dxTRUE;
        JobScheduledList[i].JobLastExecutedTime = 0;
        JobScheduledList[i].JobIntervalUpcomingExecuteTime = 0;
        JobScheduledList[i].JobDataLen = 0;
        if (JobScheduledList[i].JobData)
        {
            dxMemFree(JobScheduledList[i].JobData);
            JobScheduledList[i].JobData = NULL;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);
}

static void _RemoveAllAdvanceJobs (dxBOOL bSkipReserved)
{
    uint16_t i = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        if(bSkipReserved == dxTRUE)
        {
            if(AdvanceJobList[i].AdvanceJobID <= MAX_ADV_JOB_RESERVED_ID/*ADV_JOB_RESERVED_ID_MAX*/)
            {
                //skip removing reserved Adv Job ID
                continue;
            }
        }

        AdvanceJobList[i].SlotAvailable = dxTRUE;
        AdvanceJobList[i].AdvJobConditionState = 0; //Initial Unknown
        AdvanceJobList[i].AdvanceJobID = 0;
        AdvanceJobList[i].AdvJobLastExecutedTime = 0;
        AdvanceJobList[i].JobConditionDataLen = 0;
        AdvanceJobList[i].JobExecutionDataLen = 0;
        if (AdvanceJobList[i].JobConditionData)
        {
            dxMemFree(AdvanceJobList[i].JobConditionData);
            AdvanceJobList[i].JobConditionData = NULL;
        }

        if (AdvanceJobList[i].JobExecutionData)
        {
            dxMemFree(AdvanceJobList[i].JobExecutionData);
            AdvanceJobList[i].JobExecutionData = NULL;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);
}

static int16_t FindScheduleIndexOfDeviceWithCondition( uint8_t* DevAddress8Byte, int8_t StartingAtIndex )
{
    int16_t FoundIndex = -1;
    int16_t i = StartingAtIndex;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED)
    {
        for (i = StartingAtIndex; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
        {
            JobSchedule_t* pJobSchedule = &JobScheduledList[i];
            if (pJobSchedule->SlotAvailable != dxTRUE)
            {
                uint8_t* pJobCondition = GetJobConditionStartPointer(pJobSchedule->JobData);

                if (pJobCondition)
                {
                    JobConditionHeader_t* pJobConditionHeader = (JobConditionHeader_t*)(pJobCondition);

                    uint16_t j = 0;
                    dxBOOL DevAddressMatch = dxTRUE;
                    for (j = 0; j < B_ADDR_LEN_8; j++)
                    {
                        if (DevAddress8Byte[j] != pJobConditionHeader->PeripheralDeviceMacAddress.Addr[j])
                        {
                            DevAddressMatch = dxFALSE;
                            break;
                        }
                    }

                    if (DevAddressMatch == dxTRUE)
                    {
                        FoundIndex = i;
                        break;
                    }
                }
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return FoundIndex;
}

static int16_t FindAvailableScheduleListSlot()
{
    int16_t SlotAvailable = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[i];
        if (pJobSchedule->SlotAvailable == dxTRUE)
        {
            SlotAvailable = i;
            break;
        }
    }

    if (SlotAvailable < 0)
    {
        G_SYS_DBG_LOG_V( "WARNING: jobScheduleList is FULL!!!\r\n" );
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return SlotAvailable;
}

static int16_t FindIndexOfScheduleJobAlreadyInList(uint8_t* JobScheduleSequenceNumberInByteToCheck)
{
    int16_t SlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[i];
        if (pJobSchedule->SlotAvailable != dxTRUE)
        {
            if (pJobSchedule->JobData)
            {
                JobHeader_t* JobHeader = (JobHeader_t*)pJobSchedule->JobData;

                //if (JobHeader->ScheduleSequenceNumber == JobScheduleSequenceNumberToCheck)
                if (memcmp(JobScheduleSequenceNumberInByteToCheck, JobHeader->ScheduleSequenceNumber.byte, sizeof(JobHeader->ScheduleSequenceNumber.byte)) == 0)
                {
                    SlotIndex = i;
                    break;
                }
            }
            else
            {
                G_SYS_DBG_LOG_V( "WARNING: jobScheduleList taken but JobData is NULL!!\r\n" );
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return SlotIndex;
}

static int16_t AddScheduleJobToList(int16_t AddToSlot, uint8_t* cJobData, uint16_t cJobDataLen)
{

    int16_t AddedSlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((AddToSlot < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED) && (cJobData != NULL))
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[AddToSlot];

        if (pJobSchedule->SlotAvailable == dxTRUE)
        {
            pJobSchedule->SlotAvailable = dxFALSE;
            pJobSchedule->JobLastExecutedTime = 0;
            pJobSchedule->JobIntervalUpcomingExecuteTime = 0;

            pJobSchedule->JobDataLen = cJobDataLen;
            pJobSchedule->JobData = cJobData;
            AddedSlotIndex = AddToSlot;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return AddedSlotIndex;
}

static dxBOOL RemoveScheduleJobFromList(int16_t SlotToRemove)
{

    dxBOOL Success = dxFALSE;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((SlotToRemove >= 0) && (SlotToRemove < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED))
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[SlotToRemove];

        //Make sure its occupied before we start cleaning up.
        if (pJobSchedule->SlotAvailable == dxFALSE)
        {
            JobHeader_t* JobHeader = (JobHeader_t*)pJobSchedule->JobData;
            G_SYS_DBG_LOG_V2( "Removing SpecificJob(%d/%u) from JonManager\r\n", JobHeader->ScheduleSequenceNumber.Component.PeripheralUniqueID, JobHeader->ScheduleSequenceNumber.Component.UniqueSequenceID );

            JobScheduledList[SlotToRemove].SlotAvailable = dxTRUE;
            JobScheduledList[SlotToRemove].JobLastExecutedTime = 0;
            JobScheduledList[SlotToRemove].JobIntervalUpcomingExecuteTime = 0;
            if (JobScheduledList[SlotToRemove].JobData)
            {
                dxMemFree(JobScheduledList[SlotToRemove].JobData);
                JobScheduledList[SlotToRemove].JobData = NULL;
            }

            Success = dxTRUE;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return Success;
}


static int16_t ReplaceScheduleJobToList(int16_t SlotToReplace, uint8_t* cJobData, uint16_t cJobDataLen)
{

    int16_t ReplacedSlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((SlotToReplace < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED) && (cJobData != NULL))
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[SlotToReplace];

        if (pJobSchedule->SlotAvailable != dxTRUE)
        {
            if (pJobSchedule->JobData)
                dxMemFree(pJobSchedule->JobData);

            pJobSchedule->JobLastExecutedTime = 0;
            pJobSchedule->JobIntervalUpcomingExecuteTime = 0;

            pJobSchedule->JobDataLen = cJobDataLen;
            pJobSchedule->JobData = cJobData;
            ReplacedSlotIndex = SlotToReplace;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return ReplacedSlotIndex;
}



static int16_t FindAvailableAdvanceJobListSlot()
{
    int16_t SlotAvailable = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        AdvanceJob_t* pJobdvance = &AdvanceJobList[i];
        if (pJobdvance->SlotAvailable == dxTRUE)
        {
            SlotAvailable = i;
            break;
        }
    }

    if (SlotAvailable < 0)
    {
        G_SYS_DBG_LOG_V( "WARNING: AdvanceJobList is FULL!!!\r\n" );
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return SlotAvailable;
}



static int16_t FindIndexOfAdvanceJobAlreadyInList(uint32_t AdvanceJobID)
{
    int16_t SlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[i];
        if (pAdvanceJob->SlotAvailable != dxTRUE)
        {
            if (pAdvanceJob->AdvanceJobID == AdvanceJobID)
            {
                SlotIndex = i;
                break;
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return SlotIndex;
}


static int16_t AddAdvanceJobConditionToList(int16_t AddToSlot, uint32_t AdvanceJobID, uint8_t* cJobData, uint16_t cJobDataLen)
{

    int16_t AddedSlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((AddToSlot < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED) && (cJobData != NULL))
    {
        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[AddToSlot];

        if (pAdvanceJob->SlotAvailable == dxTRUE)
        {
            pAdvanceJob->SlotAvailable = dxFALSE;
            pAdvanceJob->AdvanceJobID = AdvanceJobID;
            pAdvanceJob->AdvJobLastExecutedTime = 0;
            pAdvanceJob->AdvJobFirstCheckTick = 0;
            pAdvanceJob->AdvJobLastTotalConditionReached = 0;
            pAdvanceJob->AdvJobConditionState = 0; //Initial unknown

            pAdvanceJob->JobConditionDataLen = cJobDataLen;
            pAdvanceJob->JobConditionData = cJobData;
            AddedSlotIndex = AddToSlot;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return AddedSlotIndex;
}


static dxBOOL RemoveAdvanceJobFromList(int16_t SlotToRemove)
{

    dxBOOL Success = dxFALSE;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((SlotToRemove >= 0) && (SlotToRemove < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED))
    {
        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[SlotToRemove];

        //Make sure its occupied before we start cleaning up.
        if (pAdvanceJob->SlotAvailable == dxFALSE)
        {
            G_SYS_DBG_LOG_V1( "Removing Advance Job (ID=%u) from JonManager\r\n", AdvanceJobList[SlotToRemove].AdvanceJobID );

            AdvanceJobList[SlotToRemove].SlotAvailable = dxTRUE;
            AdvanceJobList[SlotToRemove].AdvanceJobID = 0;
            AdvanceJobList[SlotToRemove].AdvJobLastExecutedTime = 0;
            AdvanceJobList[SlotToRemove].AdvJobFirstCheckTick = 0;
            AdvanceJobList[SlotToRemove].AdvJobLastTotalConditionReached = 0;
            AdvanceJobList[SlotToRemove].AdvJobConditionState = 0; //Initial Unknown

            if (AdvanceJobList[SlotToRemove].JobConditionData)
            {
                dxMemFree(AdvanceJobList[SlotToRemove].JobConditionData);
                AdvanceJobList[SlotToRemove].JobConditionData = NULL;
            }

            if (AdvanceJobList[SlotToRemove].JobExecutionData)
            {
                dxMemFree(AdvanceJobList[SlotToRemove].JobExecutionData);
                AdvanceJobList[SlotToRemove].JobExecutionData = NULL;
            }

            Success = dxTRUE;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return Success;
}


static int16_t ReplaceAdvanceJobConditionToList(int16_t SlotToReplace, uint32_t AdvanceJobID, uint8_t* cJobData, uint16_t cJobDataLen)
{

    int16_t ReplacedSlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((SlotToReplace < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED) && (cJobData != NULL))
    {
        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[SlotToReplace];

        if (pAdvanceJob->SlotAvailable != dxTRUE)
        {
            if (pAdvanceJob->JobConditionData)
                dxMemFree(pAdvanceJob->JobConditionData);

            if (pAdvanceJob->AdvanceJobID != AdvanceJobID)
            {
                G_SYS_DBG_LOG_V( "WARNING: Adv Job ID does not match when replacing the slot\r\n");
            }

            pAdvanceJob->AdvJobConditionState = 0; //Initial Unknown
            pAdvanceJob->AdvanceJobID = AdvanceJobID;
            pAdvanceJob->AdvJobLastExecutedTime = 0;
            pAdvanceJob->AdvJobFirstCheckTick = 0;
            pAdvanceJob->AdvJobLastTotalConditionReached = 0;
            pAdvanceJob->JobConditionDataLen = cJobDataLen;
            pAdvanceJob->JobConditionData = cJobData;
            ReplacedSlotIndex = SlotToReplace;
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return ReplacedSlotIndex;
}


static int16_t AddReplaceAdvanceJobExecutionToExistingList(int16_t ExistingSlotToReplace, uint32_t AdvanceJobID, uint8_t* eJobData, uint16_t eJobDataLen)
{

    int16_t ReplacedSlotIndex = -1;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((ExistingSlotToReplace < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED) && (eJobData != NULL))
    {
        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[ExistingSlotToReplace];

        //Make sure its occupied slot which already contain Condition Job
        if ((pAdvanceJob->SlotAvailable == dxFALSE) &&
            (pAdvanceJob->AdvanceJobID == AdvanceJobID) &&
            (pAdvanceJob->JobConditionData != NULL))
        {
            if (pAdvanceJob->JobExecutionData)
                dxMemFree(pAdvanceJob->JobExecutionData);

            if (pAdvanceJob->AdvanceJobID != AdvanceJobID)
            {
                G_SYS_DBG_LOG_V( "WARNING: Adv Job ID does not match when replacing the slot\r\n");
            }

            pAdvanceJob->JobExecutionDataLen = eJobDataLen;
            pAdvanceJob->JobExecutionData = eJobData;
            ReplacedSlotIndex = ExistingSlotToReplace;
        }
        else
        {
            G_SYS_DBG_LOG_V4( "WARNING: Fail replace exe Adv Job (ID=%u/%u), SA=%d, cJob=%d\r\n", AdvanceJobID, pAdvanceJob->AdvanceJobID,
                                (pAdvanceJob->SlotAvailable) ? 1:0, (pAdvanceJob->JobConditionData) ? 1:0);
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return ReplacedSlotIndex;
}


static uint8_t JobIdAddForAdvJobAndUser(uint64_t GivenJobId, uint32_t AdvJobID, uint32_t UserId)
{
    uint8_t Success = 0;

    if (AdvJobID || UserId)
    {
        uint16_t i = 0;
        uint16_t OldestRecordedIndex = 0;
        dxTime_t OldestRecordedTime = 0;
        dxTime_t CurrentTimer;
        CurrentTimer = dxTimeGetMS();

        for (i = 0; i <MAXIMUM_JOB_ID_INFO_LIST_ALLOWED; i++)
        {
            //We are going to use this slot if its not used or the slot info is too old.
            if (JobIdInfoList[i].JobID == 0)
            {
                JobIdInfoList[i].JobID = GivenJobId;
                JobIdInfoList[i].AdvObjID = AdvJobID;
                JobIdInfoList[i].UserObjID = UserId;
                JobIdInfoList[i].TimeRecorded = CurrentTimer;
                JobIdInfoList[i].Tagged = dxFALSE;
                Success = 1;
                //G_SYS_DBG_LOG_V2( "INFO: JobIdAddForAdvJobAndUser AdvID=%d,T=%d\r\n",AdvJobID, JobIdInfoList[i].TimeRecorded);
                break;
            }
            else
            {
                if (JobIdInfoList[i].TimeRecorded > OldestRecordedTime)
                {
                    OldestRecordedTime = JobIdInfoList[i].TimeRecorded;
                    OldestRecordedIndex = i;
                }
            }
        }

        if (Success == 0)
        {
            //We are going to pick the oldest slot, this usually means either too much orphant or too much adv job coming all at once!
            JobIdInfoList[OldestRecordedIndex].JobID = GivenJobId;
            JobIdInfoList[OldestRecordedIndex].AdvObjID = AdvJobID;
            JobIdInfoList[OldestRecordedIndex].UserObjID = UserId;
            JobIdInfoList[OldestRecordedIndex].TimeRecorded = CurrentTimer;
            JobIdInfoList[OldestRecordedIndex].Tagged = dxFALSE;

            Success = 2;
            G_SYS_DBG_LOG_V( "WARNING: JobIdGenerateForAdvJobAndUser using recycle slot\r\n" );
        }
    }

    return Success;
}

static dxBOOL JobIdInfoTag(uint64_t JobIdToTag)
{
    dxBOOL Success = dxFALSE;

    if (JobIdToTag)
    {
        uint16_t i = 0;
        for (i = 0; i <MAXIMUM_JOB_ID_INFO_LIST_ALLOWED; i++)
        {
            if (JobIdInfoList[i].JobID == JobIdToTag)
            {
                JobIdInfoList[i].Tagged = dxTRUE;
                break;
            }
        }
    }

    return Success;
}

static JobIdInfo_t JobIdInfoPull(uint64_t JobIdToPull, dxBOOL TaggedOnly)
{
    JobIdInfo_t JobIdInfo;
    memset(&JobIdInfo, 0, sizeof(JobIdInfo_t));

    if (JobIdToPull)
    {
        dxTime_t CurrentTimer;
        CurrentTimer = dxTimeGetMS();

        uint16_t i = 0;
        for (i = 0; i <MAXIMUM_JOB_ID_INFO_LIST_ALLOWED; i++)
        {
            //Check if we can find the slot
            if (JobIdInfoList[i].JobID == JobIdToPull)
            {
                //Check if the recorded time of advance job info is "within" the expiry time.. if it is beyond expiry time then we are
                //going to simpy ignore it and clear this slot as it might not be the appriopriate data we want.
                if ((CurrentTimer - JobIdInfoList[i].TimeRecorded) < ADV_JOB_ID_INFO_RECORD_EXPIRE_TIME_MAX)
                {
                    dxBOOL  FoundAndReport = dxTRUE;

                    if ((TaggedOnly) && (!JobIdInfoList[i].Tagged))
                    {
                        FoundAndReport = dxFALSE;
                    }

                    if (FoundAndReport)
                    {
                        //Found Slot
                        JobIdInfo.JobID = JobIdInfoList[i].JobID;
                        JobIdInfo.AdvObjID = JobIdInfoList[i].AdvObjID;
                        JobIdInfo.UserObjID = JobIdInfoList[i].UserObjID;
                        JobIdInfo.TimeRecorded = JobIdInfoList[i].TimeRecorded;

                        //We clear the slot
                        memset(&JobIdInfoList[i], 0, sizeof(JobIdInfo_t));
                        break;
                    }
                }
                else
                {
                    G_SYS_DBG_LOG_V2( "WARNING:JobIdInfoPull AdvInfo expired(TC=%d,TR=%d)\r\n", CurrentTimer, JobIdInfoList[i].TimeRecorded  );
                    //We clear the slot
                    memset(&JobIdInfoList[i], 0, sizeof(JobIdInfo_t));
                }
            }
        }
    }

    return JobIdInfo;
}


static void ProcessJobExecution(uint8_t* JobData, dxTime_t TimeStamp, DeviceInfoReport_t *pDeviceInfo, uint16_t JobSourceOfOrigin, uint64_t jobIdentificationNumber, uint32_t AdvJobId)
{

    //NOTE1:    ProcessJobExecution DOES NOT check the condition, it simply find the execution and process it... IT IS the caller function responsibility to
    //          check the condition or scheduled criteria.
    //NOTE2:    ProcessJobExecution will remove the job from Watch list if this is a ONCE execution Job

    JobHeader_t* JobHeader = (JobHeader_t*)JobData;

    uint8_t* pJobExecution = GetJobExecutionStartPointer(JobData);

    if (JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_STANDARD_BLE)
    {
        //G_SYS_DBG_LOG_V( "ProcessJobExecution JOB_TYPE_STANDARD_BLE\r\n" );

        uint8_t CurrentExecution;
        for (CurrentExecution = 0; CurrentExecution < JobHeader->TotalJobExecution; CurrentExecution++)
        {
            BleStandardJobExecHeader_t* JobExecutionHeader = (BleStandardJobExecHeader_t*)pJobExecution;

            if (RegisteredEventFunction)
            {
                //This is an execution from AdvJob, we generate an id base on Peripheral Macaddress.
                //This is used for later tracking with JobIdInfoPull() to obtain the AdvId and UserId
                if (AdvJobId)
                {
                    char MacAddrDecStr[21] = {0};

                    if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), JobExecutionHeader->PeripheralDeviceMacAddress.Addr, 8))
                    {
                        uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                        if (MacAddrDec)
                        {
                            if (JobIdAddForAdvJobAndUser(MacAddrDec, AdvJobId, 0) == 0)
                            {
                                G_SYS_DBG_LOG_V( "JobIdAddForAdvJobAndUser Failed\r\n");
                            }
                        }
                    }
                }

                //G_SYS_DBG_LOG_V( "ProcessJobExecution sending peripheral job access event\r\n" );

                JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

                SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_ACCESS;
                SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
                SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
                SendJobManagerReportEvent->Header.TimeStamp = TimeStamp;
                if(JobHeader->JobType.bits.JobBleWriteOnce_b4 == BLE_WRITE_RETRY_SHORT)
                {
                    SendJobManagerReportEvent->Header.Timeout = 5 * SECONDS;    //we cannot set this value to 0 since writing to BLE still may need some retry
                }
                else
                {
                    SendJobManagerReportEvent->Header.Timeout = 12 * SECONDS;
                }

                uint8_t* JobExecData = dxMemAlloc( "Job exec data Buff", 1, sizeof(BleStandardJobExecHeader_t) + JobExecutionHeader->DataLen);;
                memcpy(JobExecData, JobExecutionHeader, sizeof(BleStandardJobExecHeader_t)+JobExecutionHeader->DataLen);
                SendJobManagerReportEvent->pData = JobExecData;

                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

                if (EventResult != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "FATAL JOB_TYPE_STANDARD_BLE - JobManager Failed to send Event message back to Application\r\n" );

                    dxMemFree(SendJobManagerReportEvent->pData);
                    dxMemFree(SendJobManagerReportEvent);
                }
            }

            pJobExecution += (sizeof(BleStandardJobExecHeader_t) + JobExecutionHeader->DataLen);
        }
    }
    else if (JobHeader->JobType.bits.JobExecutionType_b12_15 == JOB_TYPE_HYBRIDPERIPHERAL)
    {
        //G_SYS_DBG_LOG_V( "ProcessJobExecution JOB_TYPE_HYBRIDPERIPHERAL\r\n" );

        uint8_t CurrentExecution;
        for (CurrentExecution = 0; CurrentExecution < JobHeader->TotalJobExecution; CurrentExecution++)
        {
            HybridPeripheralJobExecHeader_t* JobExecutionHeader = (HybridPeripheralJobExecHeader_t*)pJobExecution;

/*
            G_SYS_DBG_LOG_V( "%s, UUID:0x%02x, data len = %d, DkPeripheral_IsUuidMatch = %d, adv data len = %d 0x%02x 0x%02x\r\n", __FUNCTION__
                , JobExecutionHeader->CharacteristicUUID[3]
                , JobExecutionHeader->DataLen
                , DkPeripheral_IsUuidMatch(DK_CHARACTERISTIC_REMOTE_CONTROL_MODE_CHANGE_UUID, JobExecutionHeader->CharacteristicUUID)
                , pDeviceInfo->DkAdvertisementData.AdDataLen
                , pDeviceInfo->DkAdvertisementData.AdData[0]
                , pDeviceInfo->DkAdvertisementData.AdData[1]
                );
*/

            if (RegisteredEventFunction)
            {
                //This is an execution from AdvJob, we generate an id base on Peripheral Macaddress.
                //This is used for later tracking with JobIdInfoPull() to obtain the AdvId and UserId
                if (AdvJobId)
                {
                    char MacAddrDecStr[21] = {0};

                    if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), JobExecutionHeader->PeripheralDeviceMacAddress.Addr, 8))
                    {
                        uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                        if (MacAddrDec)
                        {
                            if (JobIdAddForAdvJobAndUser(MacAddrDec, AdvJobId, 0) == 0)
                            {
                                G_SYS_DBG_LOG_V( "JobIdAddForAdvJobAndUser Failed\r\n");
                            }
                        }
                    }
                }

                //G_SYS_DBG_LOG_V( "ProcessJobExecution sending peripheral job access event\r\n" );

                JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

                SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS;
                SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
                SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
                SendJobManagerReportEvent->Header.TimeStamp = TimeStamp;

                dxBOOL bRemoteKeyUuidMatch;
                uint8_t* JobExecData;

                bRemoteKeyUuidMatch = DkPeripheral_IsUuidMatch((uint8_t*)DK_CHARACTERISTIC_REMOTE_CONTROL_MODE_CHANGE_UUID, (uint8_t*)JobExecutionHeader->CharacteristicUUID);

                if(bRemoteKeyUuidMatch)
                {
                    if (pDeviceInfo)
                    {
                        JobExecutionHeader->DataLen = pDeviceInfo->DkAdvertisementData.AdDataLen;
                        JobExecData = dxMemAlloc( "Job exec data Buff", 1, sizeof(HybridPeripheralJobExecHeader_t) + JobExecutionHeader->DataLen);;
                        memcpy(JobExecData, JobExecutionHeader, sizeof(HybridPeripheralJobExecHeader_t));
                        memcpy(JobExecData + sizeof(HybridPeripheralJobExecHeader_t), pDeviceInfo->DkAdvertisementData.AdData, JobExecutionHeader->DataLen);
                    }
                    else //pDeviceInfo is NULL
                    {
                        dxMemFree(SendJobManagerReportEvent);
                        SendJobManagerReportEvent = NULL;
                        G_SYS_DBG_LOG_V( "WARNING: ProcessJobExecution pDeviceInfo is NULL!! Will skip JobEventReport\r\n");
                    }
                }
                else
                {
                    JobExecData = dxMemAlloc( "Job exec data Buff", 1, sizeof(HybridPeripheralJobExecHeader_t) + JobExecutionHeader->DataLen);;
                    memcpy(JobExecData, JobExecutionHeader, sizeof(HybridPeripheralJobExecHeader_t)+JobExecutionHeader->DataLen);
                }

                if (SendJobManagerReportEvent)
                {
                    SendJobManagerReportEvent->pData = JobExecData;

                    dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

                    if (EventResult != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_V(  "FATAL JOB_TYPE_STANDARD_BLE - JobManager Failed to send Event message back to Application\r\n" );

                        dxMemFree(SendJobManagerReportEvent->pData);
                        dxMemFree(SendJobManagerReportEvent);
                    }
                }
            }

            pJobExecution += (sizeof(HybridPeripheralJobExecHeader_t) + JobExecutionHeader->DataLen);
        }
    }
    else //JOB_TYPE_DEVICE_COMMAND
    {
        uint8_t CurrentExecution;
        for (CurrentExecution = 0; CurrentExecution < JobHeader->TotalJobExecution; CurrentExecution++)
        {
            uint8_t*                    JobCommandPayload = NULL;
            JobManager_EventType_t      jobEventType = JOB_MANAGER_EVENT_UNKNOWN;
            DeviceCommandJobHeader_t*   DeviceCommandHeader = (DeviceCommandJobHeader_t*)pJobExecution;

            //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_JOBMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
            //                 "INFORMATION:(Line=%d) GATEWAY_COMMAND_ID:%X\r\n",
            //                 __LINE__,
            //                 DeviceCommandHeader->CommandId);

            if (RegisteredEventFunction)
            {
                if (AdvJobId)
                {
                    G_SYS_DBG_LOG_V( "WARNING:JOB_TYPE_DEVICE_COMMAND with AdvJobID not handled!\r\n");
                }

                switch (DeviceCommandHeader->CommandId)
                {
                    case DEVICE_COMMAND_ID_SCAN_UNPAIR_PERIPHERAL:
                    {

                        if (DeviceCommandHeader->PayloadLen >= sizeof(uint16_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(uint16_t));
                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(uint16_t));
                                jobEventType = JOB_MANAGER_EVENT_SCAN_FOR_PERIPHERAL;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_UNPAIR_PERIPHERAL PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_SET_RENEW_SECURITY_FOR_PERIPHERAL:
                    {
                        if (DeviceCommandHeader->PayloadLen >= sizeof(stRenewSecurityInfo_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(stRenewSecurityInfo_t));
                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(stRenewSecurityInfo_t));
                                jobEventType = JOB_MANAGER_EVENT_SECURITY_SET_RENEW_PERIPHERAL;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_SET_RENEW_SECURITY_FOR_PERIPHERAL PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_UPDATE_PERIPHERAL_RTC:
                    {
                        if (DeviceCommandHeader->PayloadLen >= sizeof(stUpdatePeripheralRtcInfo_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(stUpdatePeripheralRtcInfo_t));
                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(stUpdatePeripheralRtcInfo_t));
                                jobEventType = JOB_MANAGER_EVENT_UPDATE_PERIPHERAL_RTC;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_UPDATE_PERIPHERAL_RTC PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_PAIR_PERIPHERAL:
                    {
                        if (DeviceCommandHeader->PayloadLen >= sizeof(stPairingInfo_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(stPairingInfo_t));
                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(stPairingInfo_t));
                                jobEventType = JOB_MANAGER_EVENT_PAIR_PERIPHERAL;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_PAIR_PERIPHERAL PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_UNPAIR_PERIPHERAL:
                    {
                        if (DeviceCommandHeader->PayloadLen >= sizeof(DevAddress_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(DevAddress_t));
                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(DevAddress_t));
                                jobEventType = JOB_MANAGER_EVENT_UNPAIR_PERIPHERAL;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_UNPAIR_PERIPHERAL PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_IDENTIFY:
                    {
                        jobEventType = JOB_MANAGER_EVENT_GATEWAY_IDENTIFY;
                    }
                    break;

                    case DEVICE_COMMAND_ID_SOFT_RESTART:
                    {
                        jobEventType = JOB_MANAGER_EVENT_SOFT_RESTART;
                    }
                    break;

                    case DEVICE_COMMAND_ID_SEND_NOTIFICATION:
                    {
                        if (DeviceCommandHeader->PayloadLen)
                        {
                            uint8_t TotalParam;
                            uint8_t i;

                            //Check Data integrity before we send to application layer
                            dxBOOL IntegrityCheckSuccess = dxFALSE;
                            uint16_t TotalSizeCheked = 0;
                            uint8_t* CommandJobPayload = pJobExecution + sizeof(DeviceCommandJobHeader_t);
                            uint8_t NotificationKeyLen = CommandJobPayload[0];
                            TotalSizeCheked++;
                            TotalSizeCheked += NotificationKeyLen;
                            if (TotalSizeCheked > DeviceCommandHeader->PayloadLen)
                                goto EarlyCheckOut; //Failed

                            TotalParam = CommandJobPayload[TotalSizeCheked];
                            i = 0;
                            TotalSizeCheked++;

                            for (i = 0; i < TotalParam; i++)
                            {
                                uint8_t ParameterLen = CommandJobPayload[TotalSizeCheked];
                                TotalSizeCheked++;
                                TotalSizeCheked += ParameterLen;
                                if (TotalSizeCheked > (DeviceCommandHeader->PayloadLen + 1))
                                    goto EarlyCheckOut;
                            }

                            IntegrityCheckSuccess = dxTRUE;

EarlyCheckOut:
                            if (IntegrityCheckSuccess == dxTRUE)
                            {
                                JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, DeviceCommandHeader->PayloadLen);

                                if (JobCommandPayload)
                                {
                                    memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), DeviceCommandHeader->PayloadLen);
                                    jobEventType = JOB_MANAGER_EVENT_NOTIFICATION;
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_SEND_NOTIFICATION Payload size integrity check FAILED!!\r\n" );
                            }
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_DELETE_SCHEDULE_LONGTERM_JOB:
                    {
                        int16_t ScheduleIndex = FindIndexOfScheduleJobAlreadyInList(JobHeader->ScheduleSequenceNumber.byte);
                        if (RemoveScheduleJobFromList(ScheduleIndex) == dxTRUE)
                        {
                            jobEventType = JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_SUCCESS;
                        }
                        else
                        {
                            jobEventType = JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_FAILED;
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_DELETE_ADVANCE_JOB:
                    {
                        if (DeviceCommandHeader->PayloadLen >= sizeof(uint32_t))
                        {
                            uint32_t AdvJobObjID = 0;
                            memcpy(&AdvJobObjID, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), sizeof(uint32_t));

                            int16_t AdvJobIndex = FindIndexOfAdvanceJobAlreadyInList(AdvJobObjID);

                            //G_SYS_DBG_LOG_V2( "Delete AdvJob (ID=%d, Index=%d)\r\n", AdvJobObjID, AdvJobIndex );

                            if (RemoveAdvanceJobFromList(AdvJobIndex) == dxTRUE)
                            {
                                jobEventType = JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_SUCCESS;
                            }
                            else
                            {
                                jobEventType = JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_FAILED;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING:DEVICE_COMMAND_ID_DELETE_ADVANCE_JOB PayloadLen is not accurate\r\n" );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_HISTORIC_EVENT_UPDATE:
                    {
                        if (pDeviceInfo)
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(DeviceInfoReport_t));

                            if (JobCommandPayload)
                            {
                                memcpy(JobCommandPayload, pDeviceInfo, sizeof(DeviceInfoReport_t));
                                jobEventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V1( "WARNING: pDeviceInfo not found (CommID = %d)\r\n", DeviceCommandHeader->CommandId );
                        }
                    }
                    break;

                    case DEVICE_COMMAND_ID_UPDATE_LOCAL_PERIPHERAL_NAME:
                    {
                        //Accept as long as we at least the payloadlen contain PeripheralObjID (4 bytes) and name
                        if (DeviceCommandHeader->PayloadLen > sizeof(uint32_t))
                        {
                            JobCommandPayload = dxMemAlloc( "JobManager report event Buff", 1, sizeof(PeripheralNameUpdate_t));
                            if (JobCommandPayload)
                            {
                                uint16_t LenToCopy = DeviceCommandHeader->PayloadLen;
                                memset(JobCommandPayload, 0, sizeof(PeripheralNameUpdate_t));

                                if (DeviceCommandHeader->PayloadLen >= sizeof(PeripheralNameUpdate_t))
                                {
                                    LenToCopy = sizeof(PeripheralNameUpdate_t) - 1;
                                }

                                memcpy(JobCommandPayload, (pJobExecution + sizeof(DeviceCommandJobHeader_t)), LenToCopy);
                                jobEventType = JOB_MANAGER_EVENT_UPDATE_LOCAL_PERIPHERAL_NAME;
                            }
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V( "WARNING: DEVICE_COMMAND_ID_UPDATE_LOCAL_PERIPHERAL_NAME PayloadLen is not accurate \r\n" );
                        }
                    }
                    break;

                    default:
                        G_SYS_DBG_LOG_V1( "WARNING: Gateway command ID not implemented (0X%x\r\n", DeviceCommandHeader->CommandId );
                    break;
                }
            }

            if (RegisteredEventFunction && (jobEventType != JOB_MANAGER_EVENT_UNKNOWN))
            {

                JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

                SendJobManagerReportEvent->Header.EventType = jobEventType;
                SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
                SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
                SendJobManagerReportEvent->Header.TimeStamp = TimeStamp;
                SendJobManagerReportEvent->pData = JobCommandPayload;
                dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

                if (EventResult != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "FATAL JOB_TYPE_OTHER_FORMAT - JobManager Failed to send Event message back to Application\r\n" );

                    if (SendJobManagerReportEvent->pData)
                    {
                        dxMemFree(SendJobManagerReportEvent->pData);
                    }

                    dxMemFree(SendJobManagerReportEvent);
                }
            }

            pJobExecution += (sizeof(DeviceCommandJobHeader_t) + DeviceCommandHeader->PayloadLen);
        }
    }

    if (!JobHeader->ScheduleFlag.bits.ShceduleRepeatable_b15)
    {
        //TODO: Check if this job is in watch list, if so let's remove from the watch list.
    }
}

static void NewJobScheduledConditionProcess(uint8_t* JobData, uint16_t JobLen, uint16_t JobSourceOfOrigin, uint64_t jobIdentificationNumber)
{
    JobManager_EventType_t SendWithEventType = JOB_MANAGER_EVENT_UNKNOWN;

    if (JobLen)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobData;

        //Only process if this Job is one of the following
        //1) Pure schedule Job (eg, not once) without condition
        //2) Conditional Job with or without schedule
        //3) If pure schedule make sure SCHEDULE_TIME is NOT 0xFFFF
        if ((!JobHeader->ScheduleFlag.bits.ScheduleOnce_b0) || (JobHeader->TotalJobCondition))
        {
            //Its a pure schedule job... Lets check SCHEDULE_TIME
            if (JobHeader->TotalJobCondition == 0)
            {
                if (JobHeader->ScheduleTime.Time == 0xFFFF)
                {
                    G_SYS_DBG_LOG_V(  "WARNING: Job shedule/condition unsupported format\r\n" );
                    return;
                }
            }

            int16_t ReplacementSlotIndex = FindIndexOfScheduleJobAlreadyInList(JobHeader->ScheduleSequenceNumber.byte);

            if (ReplacementSlotIndex >= 0)
            {

                G_SYS_DBG_LOG_V( "Replacing scheduled/conditional job to ReplaceScheduleJobToList\r\n" );
                uint8_t* JobAllocDataBuff =	dxMemAlloc("Job schedule/Condition data buff", 1, JobLen);
                if (JobAllocDataBuff)
                {
                    memcpy(JobAllocDataBuff, JobData, JobLen);
                    ReplaceScheduleJobToList(ReplacementSlotIndex, JobAllocDataBuff, JobLen);
                    SendWithEventType = JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_UPDATED;
                }
            }
            else
            {
                int16_t AvailableSlotIndex = FindAvailableScheduleListSlot();
                if (AvailableSlotIndex >= 0)
                {
                    G_SYS_DBG_LOG_V1( "Adding sche./cond. job to AddScheduleJobToList (Size = %d)\r\n", JobLen );
                    uint8_t* JobAllocDataBuff =	dxMemAlloc("Job schedule/Condition data buff", 1, JobLen);
                    if (JobAllocDataBuff)
                    {
                        memcpy(JobAllocDataBuff, JobData, JobLen);
                        AddScheduleJobToList(AvailableSlotIndex, JobAllocDataBuff, JobLen);

                        SendWithEventType = JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_UPDATED;
                    }
                }
            }
        }
    }

    if ((RegisteredEventFunction) && (SendWithEventType != JOB_MANAGER_EVENT_UNKNOWN))
    {

        JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

        SendJobManagerReportEvent->Header.EventType = SendWithEventType;
        SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
        SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
        SendJobManagerReportEvent->pData = NULL;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "FATAL NewJobScheduledConditionProcess (%d)-Failed to send Event message back to Application\r\n", EventResult );

            if (SendJobManagerReportEvent->pData)
                dxMemFree(SendJobManagerReportEvent->pData);

            dxMemFree(SendJobManagerReportEvent);
        }
    }
}


static void NewAdvanceJobConditionProcess(uint8_t* JobData, uint16_t JobLen, uint32_t AdvanceJobID, uint16_t JobSourceOfOrigin, uint64_t jobIdentificationNumber)
{

    //G_SYS_DBG_LOG_V( "JOB_MANAGER_NEW_ADV_JOB\r\n");
    JobManager_EventType_t SendWithEventType = JOB_MANAGER_EVENT_UNKNOWN;

    if (JobLen)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobData;

        //Only process if this Job is one of the following
        //1) Job that is not scheduled once and with at least one condition and no execution.
        if ((!JobHeader->ScheduleFlag.bits.ScheduleOnce_b0) &&
            (JobHeader->TotalJobCondition) &&
            (JobHeader->TotalJobExecution == 0))
        {

            int16_t ReplacementSlotIndex = FindIndexOfAdvanceJobAlreadyInList(AdvanceJobID);

            if (ReplacementSlotIndex >= 0)
            {
                G_SYS_DBG_LOG_V1( "Replacing Advance job (ID=%d) to AdvanceJobToList\r\n", AdvanceJobID);
                uint8_t* JobAllocDataBuff =	dxMemAlloc("Advance Job data buff", 1, JobLen);
                if (JobAllocDataBuff)
                {
                    memcpy(JobAllocDataBuff, JobData, JobLen);
                    ReplaceAdvanceJobConditionToList(ReplacementSlotIndex, AdvanceJobID, JobAllocDataBuff, JobLen);
                    SendWithEventType = JOB_MANAGER_EVENT_ADVANCE_JOB_UPDATED;
                }
            }
            else
            {
                int16_t AvailableSlotIndex = FindAvailableAdvanceJobListSlot();
                if (AvailableSlotIndex >= 0)
                {
                    G_SYS_DBG_LOG_V2( "Adding advance job (ID=%d) to AddAdvanceJobToList (Size=%d)\r\n", AdvanceJobID, JobLen );
                    uint8_t* JobAllocDataBuff =	dxMemAlloc("Advance Job data buff", 1, JobLen);
                    if (JobAllocDataBuff)
                    {
                        memcpy(JobAllocDataBuff, JobData, JobLen);
                        AddAdvanceJobConditionToList(AvailableSlotIndex, AdvanceJobID, JobAllocDataBuff, JobLen);
                        SendWithEventType = JOB_MANAGER_EVENT_ADVANCE_JOB_UPDATED;
                    }
                }
            }
        }
    }

    if ((RegisteredEventFunction) && (SendWithEventType != JOB_MANAGER_EVENT_UNKNOWN))
    {

        JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

        SendJobManagerReportEvent->Header.EventType = SendWithEventType;
        SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
        SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
        SendJobManagerReportEvent->pData = NULL;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "FATAL NewAdvanceJobConditionProcess(%d)-Failed to send Event message back to Application\r\n",EventResult );

            if (SendJobManagerReportEvent->pData)
                dxMemFree(SendJobManagerReportEvent->pData);

            dxMemFree(SendJobManagerReportEvent);
        }
    }
}


static void NewAdvanceJobExecutionProcess(uint8_t* JobData, uint16_t JobLen, uint32_t AdvanceJobID, uint16_t JobSourceOfOrigin, uint64_t jobIdentificationNumber)
{

    //G_SYS_DBG_LOG_V( "JOB_MANAGER_ADV_JOB_SINGLE_PACK_EXEC\r\n");
    JobManager_EventType_t  SendWithEventType = JOB_MANAGER_EVENT_UNKNOWN;

    if (JobLen)
    {
        JobHeader_t* JobHeader = (JobHeader_t*)JobData;

        //Only process if this Job is one of the following
        //1) Job that is scheduled once and with at least one execution and no condition.
        if ((JobHeader->ScheduleFlag.bits.ScheduleOnce_b0) &&
            (JobHeader->TotalJobExecution) &&
            (JobHeader->TotalJobCondition == 0))
        {

            int16_t ReplacementSlotIndex = FindIndexOfAdvanceJobAlreadyInList(AdvanceJobID);

            if (ReplacementSlotIndex >= 0)
            {

                G_SYS_DBG_LOG_V1( "Add/Replacing Advance job (ID=%d) exec job\r\n", AdvanceJobID );
                uint8_t* JobAllocDataBuff =	dxMemAlloc("Advance Job data buff", 1, JobLen);
                if (JobAllocDataBuff)
                {
                    memcpy(JobAllocDataBuff, JobData, JobLen);
                    AddReplaceAdvanceJobExecutionToExistingList(ReplacementSlotIndex, AdvanceJobID, JobAllocDataBuff, JobLen);
                    SendWithEventType = JOB_MANAGER_EVENT_ADVANCE_JOB_UPDATED;
                }
            }
            else
            {
                G_SYS_DBG_LOG_V( "WARNING: Adding adv exec job to non existing adv condition job\r\n");
            }
        }
    }

    if ((RegisteredEventFunction) && (SendWithEventType != JOB_MANAGER_EVENT_UNKNOWN))
    {

        JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

        SendJobManagerReportEvent->Header.EventType = SendWithEventType;
        SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
        SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;
        SendJobManagerReportEvent->pData = NULL;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "FATAL NewAdvanceJobExecutionProcess(%d)-Failed to send Event message back to Application\r\n",EventResult );

            if (SendJobManagerReportEvent->pData)
                dxMemFree(SendJobManagerReportEvent->pData);

            dxMemFree(SendJobManagerReportEvent);
        }
    }

}

static void ProcessDeviceNewDataUpdate(uint8_t* JobData, JobManager_Job_t JobType, uint16_t JobSourceOfOrigin, uint64_t jobIdentificationNumber)
{
    if (!RegisteredEventFunction) {
        goto exit;
    }

    JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

    switch (JobType)
    {
        case JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE_FIRST_TIME:
        {
            //NOTE: Regardless of peripheral is a status only or event payload update, we are going to force status update only
            //      IF this is the first time. Main reason is to prevent sending event to DB (eg, Door) whenever system starts up...
            //      Ofcourse, this will have some drawback such as unable to send first event of JUST newly paired peripheral!.
            SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY;
        }
        break;

        case JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE:
        {
            DeviceInfoReport_t* pDeviceInfo = (DeviceInfoReport_t*)JobData;

            if (DK_FLAG_IS_UPDATE_PAYLOAD_TO_PERIPHERAL_STATUS_ONLY(pDeviceInfo->DkAdvertisementData.Header.Flag))
            {
                SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY;
            }
            else
            {
                SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE;
            }
        }
        break;

        case JOB_MANAGER_PERIPHERAL_DATA_HISTORIC_EVENT_UPDATE_ONLY:
        {
            SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY;
        }
        break;

        case JOB_MANAGER_PERIPHERAL_DATA_UPDATE_ALIVE:
            SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_DATA_KEEP_ALIVE;
        break;
        case JOB_MANAGER_PERIPHERAL_STATUS_NEW_UPDATE:
            SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_PERIPHERAL_STATUS_NEEDS_UPDATE;
        break;

        default:
        {
            dxMemFree(SendJobManagerReportEvent);
            SendJobManagerReportEvent = NULL;
            G_SYS_DBG_LOG_V1( "FATAL ProcessDeviceNewDataUpdate - JobType (%d) unknown\r\n", JobType );
        }
        break;
    }

    if (SendJobManagerReportEvent)
    {
        SendJobManagerReportEvent->Header.IdentificationNumber = jobIdentificationNumber;
        SendJobManagerReportEvent->Header.SourceOfOrigin = JobSourceOfOrigin;

        uint8_t* DevInfoReportData = dxMemAlloc( "Device Info report data Buff", 1, sizeof(DeviceInfoReport_t));;
        memcpy(DevInfoReportData, JobData, sizeof(DeviceInfoReport_t));
        SendJobManagerReportEvent->pData = DevInfoReportData;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "FATAL ProcessDeviceNewDataUpdate(%d)-Failed to send Event message back to Application\r\n",EventResult );

            dxMemFree(SendJobManagerReportEvent->pData);
            dxMemFree(SendJobManagerReportEvent);
        }
    }

exit :

    return;
}

#if ENABLE_AUTOMATIC_PRESET_SCHEDULED_JOB_CREATION

#include "dk_PeripheralAux.h"

#pragma message ( "WARNING!!: ENABLE_AUTOMATIC_PRESET_SCHEDULED_JOB_CREATION Enabled.. NOT For Production!" )

#if 1
static void CreateHomeDoorScheduleJob(DeviceInfoReport_t* pDevInfoReportData)
{

    //Create Door Open notification
    uint8_t HeaderSize = sizeof(JobHeader_t);
    uint8_t ConditionSize = sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t) + 1; //+1 for the Door Status Data (1 byte according to BLE adv spec)
    uint8_t KeyLen = strlen("DOOROPEN");
    uint8_t ParamLen = strlen("HomeDoor");
    uint8_t JobExecutionCommandSize = sizeof(DeviceCommandJobHeader_t) + KeyLen + ParamLen; //DOOROPEN is the Notification Message Key while HomeDoor is the Param
    JobExecutionCommandSize += 3; //3 extra byte for the notification payload (KeyLen, TotalParam, ParamLen)
    uint8_t JobTotalSize = HeaderSize + ConditionSize + JobExecutionCommandSize;

    uint8_t* JobAllocDataBuff =	dxMemAlloc("Job data buff", 1, JobTotalSize);
    uint8_t* pJobDataWorkingBuff = JobAllocDataBuff;

    if (JobAllocDataBuff)
    {
        const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();
        JobHeader_t NotificationJobHeader;
        memcpy(NotificationJobHeader.DeviceMacAddress.MacAddr, DeviceMac->MacAddr, 8);
        memset(NotificationJobHeader.ScheduleSequenceNumber.byte, 0, sizeof(NotificationJobHeader.ScheduleSequenceNumber.byte));
        //NotificationJobHeader.JobCreationTime = 0; //Don't care at the moment
        //NotificationJobHeader.ScheduleSequenceNumber = 0; //Don't care at the moment
        NotificationJobHeader.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
        NotificationJobHeader.ScheduleStartTime.Time = 0; //Start Now
        NotificationJobHeader.ScheduleTime.Time = 0xFFFF; //Any time of the day
        NotificationJobHeader.JobType.bits.JobExecutionType_b12_15 = JOB_TYPE_DEVICE_COMMAND;
        NotificationJobHeader.TotalJobCondition = 1;
        NotificationJobHeader.TotalJobExecution = 1;
        NotificationJobHeader.ReExecutionThreshold = 0; //No threshold.
        memcpy(pJobDataWorkingBuff, &NotificationJobHeader, sizeof(JobHeader_t));
        pJobDataWorkingBuff += sizeof(JobHeader_t);


        JobConditionHeader_t NotificationJobCondHeader;
        memcpy(NotificationJobCondHeader.PeripheralDeviceMacAddress.Addr, pDevInfoReportData->DevAddress.Addr, 8);
        NotificationJobCondHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + 1;
        memcpy(pJobDataWorkingBuff, &NotificationJobCondHeader, sizeof(JobConditionHeader_t));
        pJobDataWorkingBuff += sizeof(JobConditionHeader_t);

        JobConditionPayloadHeader_t NotificationJobCondPayloadHeader;
        NotificationJobCondPayloadHeader.ConditionType.Type = 0;
        NotificationJobCondPayloadHeader.ConditionType.bits.ConditionField_b0_7 = DK_DATA_TYPE_DOOR_STATUS;
        NotificationJobCondPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = STANDARD_CHECK_STDBLE_COND_TYPE_FLAG;    //CONDITION_TYPE_FLAG_ADV_DATA_ID;
        NotificationJobCondPayloadHeader.Operator = OPERATOR_EQUAL;
        NotificationJobCondPayloadHeader.ValueType = VALUE_TYPE_UINT8;
        uint8_t	DoorOpenDataValue = 0x02;
        memcpy(pJobDataWorkingBuff, &NotificationJobCondPayloadHeader, sizeof(JobConditionPayloadHeader_t));
        pJobDataWorkingBuff += sizeof(JobConditionPayloadHeader_t);
        *pJobDataWorkingBuff = DoorOpenDataValue; pJobDataWorkingBuff++;

        DeviceCommandJobHeader_t NotificationJobCommandHeader;
        NotificationJobCommandHeader.CommandId = DEVICE_COMMAND_ID_SEND_NOTIFICATION;
        NotificationJobCommandHeader.PayloadLen = KeyLen + ParamLen + 3;
        memcpy(pJobDataWorkingBuff, &NotificationJobCommandHeader, sizeof(DeviceCommandJobHeader_t));
        pJobDataWorkingBuff += sizeof(DeviceCommandJobHeader_t);
        *pJobDataWorkingBuff = KeyLen; //Key Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "DOOROPEN", KeyLen); //Key value
        pJobDataWorkingBuff += KeyLen;
        *pJobDataWorkingBuff = 1; //Total Param to expect
        pJobDataWorkingBuff++;
        *pJobDataWorkingBuff = ParamLen; //Param Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "HomeDoor", ParamLen); //Param value

        int16_t AvailableSlotIndex = FindAvailableScheduleListSlot();
        if (AvailableSlotIndex >= 0)
        {
            G_SYS_DBG_LOG_V( "CreateHomeDoorScheduleJob adding to AddScheduleJobToList\r\n" );
            AddScheduleJobToList(AvailableSlotIndex, JobAllocDataBuff, JobTotalSize);
        }
    }
}


static void CreateMotionTriggerAdvJob(DeviceInfoReport_t* pDevInfoReportData)
{
    JobInstance_t JobInstance;
    CreateAdvanceJobPackage(&JobInstance, WifiCommManagerGetDeviceMac(), 1);

    DaysOfWeek_t DaysSelection;
    DaysSelection.Monday = TRUE;  DaysSelection.Tuesday = TRUE; DaysSelection.Wednesday = TRUE; DaysSelection.Thursday = TRUE;
    DaysSelection.Friday = TRUE;  DaysSelection.Saturday = TRUE; DaysSelection.Sunday = TRUE;
    SchTime_t InUTC0RangeStartTime, InUTC0RangeEndTime;
    InUTC0RangeStartTime.Minute = 0;
    InUTC0RangeStartTime.Hour24 = 0;
    InUTC0RangeEndTime.Minute = 59;
    InUTC0RangeEndTime.Hour24 = 23;
    AdvJobPackageHeader_RangedTimeAndDayRepeatableJobSetup(&JobInstance, DaysSelection, InUTC0RangeStartTime, InUTC0RangeEndTime, 8.0f);

    uint8_t MotionState = 1; //Detected
    AdvJobPackageConditionAdd_AdvertisementDataConditionJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, 0x16, OPERATOR_EQUAL, VALUE_TYPE_UINT8, &MotionState);

    uint16_t ReturnedPayloadLen = 0;
    uint8_t* AdvPayload = AdvanceJobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

    //Add condition
    NewAdvanceJobConditionProcess(AdvPayload, ReturnedPayloadLen, 123, 0, 0);
    FreeAdvanceJobPackage(&JobInstance);



    //Here we add the Execution
    DevAddress_t DevAddr; //TODO: MANUALLY add Thermostat MAC address here!!!
    //d0:b5:c2:ff:ff:f6:b3:d7
    DevAddr.Addr[0] = 0xD0; DevAddr.Addr[1] = 0xB5; DevAddr.Addr[2] = 0xC2; DevAddr.Addr[3] = 0xFF;
    DevAddr.Addr[4] = 0xFF; DevAddr.Addr[5] = 0xF6; DevAddr.Addr[6] = 0xB3; DevAddr.Addr[7] = 0xD7;

    CreateAdvanceJobPackage(&JobInstance, WifiCommManagerGetDeviceMac(), 1);
    AdvJobPackageHeader_OneTimeJobSetup(&JobInstance);

#if 1
    CBSwitchPointConfig_t ScheduleSetup;
    ScheduleSetup.Monday.FirstONTime = 48; ScheduleSetup.Monday.FirstOFFTime = 48+6; //8AM to 9AM
    ScheduleSetup.Monday.SecondONTime = 66; ScheduleSetup.Monday.SecondOFFTime = 66+18; //11AM to 14PM
    ScheduleSetup.Monday.ThirdONTime = 90; ScheduleSetup.Monday.ThirdOFFTime = 90+24; //15PM to 19PM

    ScheduleSetup.Tuesday.FirstONTime = 48; ScheduleSetup.Tuesday.FirstOFFTime = 48+6; //8AM to 9AM
    ScheduleSetup.Tuesday.SecondONTime = 66; ScheduleSetup.Tuesday.SecondOFFTime = 66+6; //11AM to 12PM
    ScheduleSetup.Tuesday.ThirdONTime = 90; ScheduleSetup.Tuesday.ThirdOFFTime = 90+6; //15PM to 16PM

    ScheduleSetup.Wednesday.FirstONTime = 48; ScheduleSetup.Wednesday.FirstOFFTime = 48+6; //8AM to 9AM
    ScheduleSetup.Wednesday.SecondONTime = 66; ScheduleSetup.Wednesday.SecondOFFTime = 66+6; //11AM to 12PM
    ScheduleSetup.Wednesday.ThirdONTime = 90; ScheduleSetup.Wednesday.ThirdOFFTime = 90+6; //15PM to 16PM

    ScheduleSetup.Thursday.FirstONTime = 48; ScheduleSetup.Thursday.FirstOFFTime = 48+6; //8AM to 9AM
    ScheduleSetup.Thursday.SecondONTime = 66; ScheduleSetup.Thursday.SecondOFFTime = 66+6; //11AM to 12PM
    ScheduleSetup.Thursday.ThirdONTime = 90; ScheduleSetup.Thursday.ThirdOFFTime = 90+6; //15PM to 16PM

    ScheduleSetup.Friday.FirstONTime = 48; ScheduleSetup.Friday.FirstOFFTime = 48+6; //8AM to 9AM
    ScheduleSetup.Friday.SecondONTime = 66; ScheduleSetup.Friday.SecondOFFTime = 66+6; //11AM to 12PM
    ScheduleSetup.Friday.ThirdONTime = 90; ScheduleSetup.Friday.ThirdOFFTime = 90+6; //15PM to 16PM

    ScheduleSetup.Saturday.FirstONTime = 0xFF; ScheduleSetup.Saturday.FirstOFFTime = 0xFF;
    ScheduleSetup.Saturday.SecondONTime = 0xFF; ScheduleSetup.Saturday.SecondOFFTime = 0xFF;
    ScheduleSetup.Saturday.ThirdONTime = 0xFF; ScheduleSetup.Saturday.ThirdOFFTime = 0xFF;

    ScheduleSetup.Sunday.FirstONTime = 55; ScheduleSetup.Sunday.FirstOFFTime = 60;
    ScheduleSetup.Sunday.SecondONTime = 80; ScheduleSetup.Sunday.SecondOFFTime = 100;
    ScheduleSetup.Sunday.ThirdONTime = 120; ScheduleSetup.Sunday.ThirdOFFTime = 122;

    ThermoPackedPayload_t PackedPayload = ThermostatCB_ConvertScheduleToPackedPayload(ScheduleSetup);
    AdvJobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(&JobInstance, &DevAddr, 0xFAE0, 0xFBE1, PackedPayload.PackedPayloadLen, PackedPayload.PackedPayload);
#endif

    ///Here we add Shutdown, manual, RTC, etc......
#if 0

    //ThermoPackedPayload_t PackedPayloadConf = ThermostatCB_ConvertGeneralConfigToPackedPayload(COMMAND_SHUDOWN_MODE, NULL);

    ThermoPackedPayload_t PackedPayloadConf = ThermostatCB_ConvertGeneralConfigToPackedPayload(COMMAND_CHANGE_TO_AUTOMATIC_MODE, NULL);

    //ThermoTemp_t Temperature; Temperature.Heat = 0x3C; //30C
    //ThermoPackedPayload_t PackedPayloadConf = ThermostatCB_ConvertGeneralConfigToPackedPayload(COMMAND_CHANGE_TO_MANUAL_WITH_GIVEN_TEMP, &Temperature);

    //ThermoRealTimeClock_t RTC;
    //RTC.Year = 16; RTC.Month = 9; RTC.Day = 5; RTC.Hour = 13; RTC.Minutes = 15;
    //ThermoPackedPayload_t PackedPayloadConf = ThermostatCB_ConvertGeneralConfigToPackedPayload(COMMAND_SET_REALTIME_CLOCK, &RTC);


    AdvJobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(&JobInstance, &DevAddr, 0xFAE0, 0xFBE3, PackedPayloadConf.PackedPayloadLen, PackedPayloadConf.PackedPayload);

#endif

    ReturnedPayloadLen = 0;
    AdvPayload = AdvanceJobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

    //Add single pack advance job
    NewAdvanceJobExecutionProcess(AdvPayload, ReturnedPayloadLen, 123, 0, 0);

    FreeAdvanceJobPackage(&JobInstance);

}

static void CreatePowerPlugScheduleIntervalJob(DeviceInfoReport_t* pDevInfoReportData)
{
    JobInstance_t JobInstance;
    struct ScheduleSequenceNumber_s ScheduleSEQNo;
    const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

    //Test ONLY - Just generate a unique number base on the device mac address.... not really guarantte uniqueness but should work for the time being as test
    ScheduleSEQNo.PeripheralUniqueID = pDevInfoReportData->DevAddress.Addr[2] + pDevInfoReportData->DevAddress.Addr[4] + pDevInfoReportData->DevAddress.Addr[6] + pDevInfoReportData->DevAddress.Addr[7];
    ScheduleSEQNo.UniqueSequenceID = 0;

    CreateJobPackage(&JobInstance, DeviceMac, &ScheduleSEQNo, 0);

    JobPackageHeader_ScheduleIntervalJobSetup(&JobInstance, 300, 1);
    JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID);

    uint16_t ReturnedPayloadLen = 0;
    uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

    NewJobScheduledConditionProcess(pJobPayload, ReturnedPayloadLen, 0, 0);

    FreeJobPackage(&JobInstance);
}


static void CreatePowerPlugScheduleDayAndTimeJob(DeviceInfoReport_t* pDevInfoReportData)
{
    JobInstance_t JobInstance;
    struct ScheduleSequenceNumber_s ScheduleSEQNo;
    const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

    //Test ONLY - Just generate a unique number base on the device mac address.... not really guarantte uniqueness but should work for the time being as test
    ScheduleSEQNo.PeripheralUniqueID = pDevInfoReportData->DevAddress.Addr[2] + pDevInfoReportData->DevAddress.Addr[4] + pDevInfoReportData->DevAddress.Addr[6] + pDevInfoReportData->DevAddress.Addr[7];
    ScheduleSEQNo.UniqueSequenceID = 0;

    uint32_t        Command = 0;
    SchTime_t       ExecutionTime;
    DaysOfWeek_t    DaySelection;

    {
        CreateJobPackage(&JobInstance, DeviceMac, &ScheduleSEQNo, 0);

        memset(&DaySelection, 0, sizeof(DaysOfWeek_t));
        DaySelection.Sunday = true;

        ExecutionTime.Hour24 = 8; //UTC+0 Time
        ExecutionTime.Minute = 50;
        JobPackageHeader_ScheduleTimeAndDayRepeatableJobSetup(&JobInstance, JOB_SCHEDULE_START_TIME_NOW, DaySelection, ExecutionTime);

        Command = 5; //5 - ON, 6 - OFF
        JobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, DK_SERVICE_POWER_PLUG_UUID, DK_CHARACTERISTIC_POWER_CONFIG_UUID, sizeof(Command), (uint8_t*)&Command);

        uint16_t ReturnedPayloadLen = 0;
        uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

        NewJobScheduledConditionProcess(pJobPayload, ReturnedPayloadLen, 0, 0);

        FreeJobPackage(&JobInstance);
    }

    {
        ScheduleSEQNo.UniqueSequenceID = 1;
        CreateJobPackage(&JobInstance, DeviceMac, &ScheduleSEQNo, 0);

        memset(&DaySelection, 0, sizeof(DaysOfWeek_t));
        DaySelection.Sunday = true;

        ExecutionTime.Hour24 = 9; //UTC+0 Time
        ExecutionTime.Minute = 0;
        JobPackageHeader_ScheduleTimeAndDayRepeatableJobSetup(&JobInstance, JOB_SCHEDULE_START_TIME_NOW, DaySelection, ExecutionTime);

        Command = 6; //5 - ON, 6 - OFF
        JobPackageExecutionAdd_PeripheralAccessWriteOnlyJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, DK_SERVICE_POWER_PLUG_UUID, DK_CHARACTERISTIC_POWER_CONFIG_UUID, sizeof(Command), (uint8_t*)&Command);

        uint16_t ReturnedPayloadLen = 0;
        uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);
        NewJobScheduledConditionProcess(pJobPayload, ReturnedPayloadLen, 0, 0);
        FreeJobPackage(&JobInstance);
    }
}


static void CreateWeatherCubeScheduleIntervalJob(DeviceInfoReport_t* pDevInfoReportData)
{
    JobInstance_t JobInstance;
    struct ScheduleSequenceNumber_s ScheduleSEQNo;
    const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

    //Test ONLY - Just generate a unique number base on the device mac address.... not really guarantte uniqueness but should work for the time being as test
    ScheduleSEQNo.PeripheralUniqueID = pDevInfoReportData->DevAddress.Addr[2] + pDevInfoReportData->DevAddress.Addr[4] + pDevInfoReportData->DevAddress.Addr[6] + pDevInfoReportData->DevAddress.Addr[7];
    ScheduleSEQNo.UniqueSequenceID = 0;

    CreateJobPackage(&JobInstance, DeviceMac, &ScheduleSEQNo, 0);

    JobPackageHeader_ScheduleIntervalJobSetup(&JobInstance, 15, 1);
    JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID);

    uint16_t ReturnedPayloadLen = 0;
    uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

    NewJobScheduledConditionProcess(pJobPayload, ReturnedPayloadLen, 0, 0);

    FreeJobPackage(&JobInstance);
}


static void CreateMotionSensorScheduleIntervalJob(DeviceInfoReport_t* pDevInfoReportData)
{
    JobInstance_t JobInstance;
    struct ScheduleSequenceNumber_s ScheduleSEQNo;
    const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

    //Test ONLY - Just generate a unique number base on the device mac address.... not really guarantte uniqueness but should work for the time being as test
    ScheduleSEQNo.PeripheralUniqueID = pDevInfoReportData->DevAddress.Addr[2] + pDevInfoReportData->DevAddress.Addr[4] + pDevInfoReportData->DevAddress.Addr[6] + pDevInfoReportData->DevAddress.Addr[7];
    ScheduleSEQNo.UniqueSequenceID = 0;

    CreateJobPackage(&JobInstance, DeviceMac, &ScheduleSEQNo, 0);

    JobPackageHeader_ScheduleIntervalJobSetup(&JobInstance, 30*60, 1);
    JobPackageExecutionAdd_PeripheralAccessReadFollowByAggregationUpdateJobSetup(&JobInstance, &pDevInfoReportData->DevAddress, DK_SERVICE_SYSTEM_UTILS_UUID, DK_CHARACTERISTIC_SYSTEM_UTIL_AGGREGATION_DATATYPE_PAYLOAD_ACCESS_UUID);

    uint16_t ReturnedPayloadLen = 0;
    uint8_t* pJobPayload = JobPackageGeneratePayloadPack(&JobInstance, &ReturnedPayloadLen);

    NewJobScheduledConditionProcess(pJobPayload, ReturnedPayloadLen, 0, 0);

    FreeJobPackage(&JobInstance);
}

#else //For Test with multiple Param

static void CreateHomeDoorScheduleJob(DeviceInfoReport_t* pDevInfoReportData)
{

    //Create Door Open notification
    uint8_t HeaderSize = sizeof(JobHeader_t);
    uint8_t ConditionSize = sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t) + 1; //+1 for the Door Status Data (1 byte according to BLE adv spec)
    uint8_t KeyLen = strlen("TESTMultiple");
    uint8_t ParamLen1 = strlen("First Param");
    uint8_t ParamLen2 = strlen("Second Param");
    uint8_t ParamLen3 = strlen("Last Param");

    uint8_t JobExecutionCommandSize = sizeof(DeviceCommandJobHeader_t) + KeyLen + ParamLen1 + ParamLen2 + ParamLen3; //DOOROPEN is the Notification Message Key while HomeDoor is the Param
    JobExecutionCommandSize += 5; //5 extra byte for the notification payload (KeyLen, TotalParam, ParamLen1, ParamLen2, ParamLen3)
    uint8_t JobTotalSize = HeaderSize + ConditionSize + JobExecutionCommandSize;

    uint8_t* JobAllocDataBuff =	dxMemAlloc("Job data buff", 1, JobTotalSize);
    uint8_t* pJobDataWorkingBuff = JobAllocDataBuff;

    if (JobAllocDataBuff)
    {
        const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();
        JobHeader_t NotificationJobHeader;
        memcpy(NotificationJobHeader.DeviceMacAddress.MacAddr, DeviceMac->MacAddr, 8);
        memset(NotificationJobHeader.ScheduleSequenceNumber.byte, 0, sizeof(NotificationJobHeader.ScheduleSequenceNumber.byte));
        //NotificationJobHeader.JobCreationTime = 0; //Don't care at the moment
        //NotificationJobHeader.ScheduleSequenceNumber = 0; //Don't care at the moment
        NotificationJobHeader.ScheduleFlag.bits.ShceduleRepeatable_b15 = 1;
        NotificationJobHeader.ScheduleStartTime = 0; //Start Now
        NotificationJobHeader.ScheduleTime.Time = 0xFFFF; //Any time of the day
        NotificationJobHeader.JobType.bits.JobType_b15 = JOB_TYPE_DEVICE_COMMAND;
        NotificationJobHeader.TotalJobCondition = 1;
        NotificationJobHeader.TotalJobExecution = 1;
        NotificationJobHeader.ReExecutionThreshold = 0; //No threshold.
        memcpy(pJobDataWorkingBuff, &NotificationJobHeader, sizeof(JobHeader_t));
        pJobDataWorkingBuff += sizeof(JobHeader_t);


        JobConditionHeader_t NotificationJobCondHeader;
        memcpy(NotificationJobCondHeader.PeripheralDeviceMacAddress.Addr, pDevInfoReportData->DevAddress.Addr, 8);
        NotificationJobCondHeader.PayloadLen = sizeof(JobConditionPayloadHeader_t) + 1;
        memcpy(pJobDataWorkingBuff, &NotificationJobCondHeader, sizeof(JobConditionHeader_t));
        pJobDataWorkingBuff += sizeof(JobConditionHeader_t);

        JobConditionPayloadHeader_t NotificationJobCondPayloadHeader;
        NotificationJobCondPayloadHeader.ConditionType.Type = 0;
        NotificationJobCondPayloadHeader.ConditionType.bits.ConditionField_b0_7 = DK_DATA_TYPE_DOOR_STATUS;
        NotificationJobCondPayloadHeader.ConditionType.bits.ConditionTypeFlag_b12_15 = STANDARD_CHECK_STDBLE_COND_TYPE_FLAG;    //JOB_CONDITION_TYPE_FLAG_ADV_DATA_ID;
        NotificationJobCondPayloadHeader.Operator = OPERATOR_EQUAL;
        NotificationJobCondPayloadHeader.ValueType = VALUE_TYPE_UINT8;
        uint8_t	DoorOpenDataValue = 0x02;
        memcpy(pJobDataWorkingBuff, &NotificationJobCondPayloadHeader, sizeof(JobConditionPayloadHeader_t));
        pJobDataWorkingBuff += sizeof(JobConditionPayloadHeader_t);
        *pJobDataWorkingBuff = DoorOpenDataValue; pJobDataWorkingBuff++;

        DeviceCommandJobHeader_t NotificationJobCommandHeader;
        NotificationJobCommandHeader.CommandId = DEVICE_COMMAND_ID_SEND_NOTIFICATION;
        NotificationJobCommandHeader.PayloadLen = KeyLen + ParamLen1 + ParamLen2 + ParamLen3 + 5;
        memcpy(pJobDataWorkingBuff, &NotificationJobCommandHeader, sizeof(DeviceCommandJobHeader_t));
        pJobDataWorkingBuff += sizeof(DeviceCommandJobHeader_t);
        *pJobDataWorkingBuff = KeyLen; //Key Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "TESTMultiple", KeyLen); //Key value
        pJobDataWorkingBuff += KeyLen;
        *pJobDataWorkingBuff = 3; //Total Param to expect
        pJobDataWorkingBuff++;
        *pJobDataWorkingBuff = ParamLen1; //Param Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "First Param", ParamLen1); //Param value
        pJobDataWorkingBuff += ParamLen1;
        *pJobDataWorkingBuff = ParamLen2; //Param Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "Second Param", ParamLen2); //Param value
        pJobDataWorkingBuff += ParamLen2;
        *pJobDataWorkingBuff = ParamLen3; //Param Len
        pJobDataWorkingBuff++;
        memcpy(pJobDataWorkingBuff, "Last Param", ParamLen3); //Param value

        int16_t AvailableSlotIndex = FindAvailableScheduleListSlot();
        if (AvailableSlotIndex >= 0)
        {
            G_SYS_DBG_LOG_V( "CreateHomeDoorScheduleJob adding to AddScheduleJobToList\r\n" );
            AddScheduleJobToList(AvailableSlotIndex, JobAllocDataBuff, JobTotalSize);
        }
    }
}

#endif
#endif //ENABLE_AUTOMATIC_PRESET_SCHEDULED_JOB_CREATION


static void ProcessAdvanceJobConditionFromNewDataUpdate(DeviceInfoReport_t* pDevInfoReportData)
{
    uint16_t i = 0;

    // Skip device missing report data
    if (pDevInfoReportData->rssi == 0) {
        goto exit;
    }

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        dxBOOL AdvJobConditionReached = dxFALSE;

        AdvanceJob_t* pAdvanceJob = &AdvanceJobList[i];
        if (pAdvanceJob->SlotAvailable != dxTRUE)
        {
            uint32_t        ConvertedTimeZoneTimeInSec = dxGetUtcInSec();
            DKTime_t        TimeZoneTimeDk = dxGetUtcTime();
            dxBOOL          TimeConditionReached = dxFALSE;
            JobHeader_t*    JobHeader = (JobHeader_t*)pAdvanceJob->JobConditionData;

            //G_SYS_DBG_LOG_V3( "Current TimeOfWeek = %d, Hour=%d, Minutes=%d\r\n", CurrentUTCTime.dayofweek, CurrentUTCTime.hour, CurrentUTCTime.min);

            //Convert to timezone time
            if (JobHeader->TimeZone.value)
            {
                int32_t TimeZoneInSec = ((uint32_t)JobHeader->TimeZone.bits.Integral_b0_3) * 60 * 60; //Convert to seconds for the integral part

                if (JobHeader->TimeZone.bits.FractionalMode_b4_5 == TIMEZONE_FRAC_30MIN)
                {
                    TimeZoneInSec += 30 * 60; //add the fractional part in seconds
                }
                else if (JobHeader->TimeZone.bits.FractionalMode_b4_5 == TIMEZONE_FRAC_45MIN)
                {
                    TimeZoneInSec += 45 * 60; //add the fractional part in seconds
                }

                if (JobHeader->TimeZone.bits.Sign_b7)
                {
                    TimeZoneInSec = -TimeZoneInSec;
                }

                ConvertedTimeZoneTimeInSec += TimeZoneInSec;

                TimeZoneTimeDk = ConvertUTCToDKTime(ConvertedTimeZoneTimeInSec);
            }

            if (    ((JobHeader->ScheduleFlag.bits.ShceduleMon_b1) && (TimeZoneTimeDk.dayofweek == 1)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleTue_b2) && (TimeZoneTimeDk.dayofweek == 2)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleWed_b3) && (TimeZoneTimeDk.dayofweek == 3)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleThu_b4) && (TimeZoneTimeDk.dayofweek == 4)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleFri_b5) && (TimeZoneTimeDk.dayofweek == 5)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleSat_b6) && (TimeZoneTimeDk.dayofweek == 6)) ||
                    ((JobHeader->ScheduleFlag.bits.ShceduleSun_b7) && (TimeZoneTimeDk.dayofweek == 7)) )
            {
                //G_SYS_DBG_LOG_V( "TimeOfWeek MATCHED\r\n");

                //Is time range option?
                if (JobHeader->ScheduleFlag.bits.ScheduleStartTimeOption == 1)
                {
                    uint16_t CurrentTimeInMinutes = (TimeZoneTimeDk.hour*60) + TimeZoneTimeDk.min;
                    uint16_t RangeStartTimeInMinutes = (JobHeader->ScheduleStartTime.bits.SchTimeRangeStartHour_b24_31*60) + JobHeader->ScheduleStartTime.bits.SchTimeRangeStartMinute_b16_23;
                    uint16_t RangeEndTimeInMinutes = (JobHeader->ScheduleStartTime.bits.SchTimeRangeEndHour_b8_15*60) + JobHeader->ScheduleStartTime.bits.SchTimeRangeEndMinute_b0_7;

                    if ((CurrentTimeInMinutes >= RangeStartTimeInMinutes) &&
                        (CurrentTimeInMinutes <= RangeEndTimeInMinutes))
                    {
                        TimeConditionReached = dxTRUE;
                    }
                }
                else
                {
                    //TODO: For now if the schedule start time option is 0 then we assume its at any time of the day.
                    //      In the future we can support the date after specific start time in UTC but at the moment this doesn't look necessary for such
                    //      implementation
                    TimeConditionReached = dxTRUE;
                }
            }

            //G_SYS_DBG_LOG_V3( "Process AdvObjID=%d At Slot=%d, TimeCondReached=%d\r\n", pAdvanceJob->AdvanceJobID, i, TimeConditionReached);

            uint8_t* pJobCondition = GetJobConditionStartPointer(pAdvanceJob->JobConditionData);

            if (pJobCondition)
            {
                uint8_t CurrentCondition = 0;
                uint8_t TotalConditionReached = 0;
                for (CurrentCondition = 0; CurrentCondition < JobHeader->TotalJobCondition; CurrentCondition++)
                {
                    JobConditionHeader_t* pJobConditionHeader = (JobConditionHeader_t*)pJobCondition;

                    uint16_t j = 0;
                    dxBOOL DevAddressMatch = dxTRUE;
                    for (j = 0; j < B_ADDR_LEN_8; j++)
                    {
                        if (pDevInfoReportData->DevAddress.Addr[j] != pJobConditionHeader->PeripheralDeviceMacAddress.Addr[j])
                        {
                            DevAddressMatch = dxFALSE;
                            break;
                        }
                    }

                    //G_SYS_DBG_LOG_V2( "Process CurrentobCondNum=%d, AddressMatch=%d\r\n", CurrentCondition,  DevAddressMatch);

                    JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionHeader_t));

                    if(JobHeader->JobType.bits.JobConditionFirstRun_b5 == CONDITION_FIRST_RUN)
                    {
                        //NOTE: This is left blank intentionally, just to keep track if there is any required
                        //      check/changes when first run is set to true.
                    }
                    else
                    {
                        if (pAdvanceJob->AdvJobFirstCheckTick == 0)
                        {
                            // At first, we set the condition reached bit to 1 (skip this condition check), afterward
                            // it may be updated at the time when correct advertise data arrived (device address matched).
                            pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 1;
                        }
                    }

                    //If device address matched then we are going to check if its condition is met or un-met
                    if (DevAddressMatch == dxTRUE)
                    {
                        dxBOOL      bProceedToConditionCheck = dxFALSE;
                        TypeValue_t stConditionValue;
                        TypeValue_t stAdvDataTypeValue;
                        uint8_t     CondOperator;

                        switch (pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15)
                        {
                            case PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG:
                            case JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL:
                            {
                                dxBOOL  bRetrieveCondData = dxTRUE;
                                if ((pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 == PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG) &&
                                    ((pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 1) ||
                                     (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 1)))
                                {
                                    bRetrieveCondData = dxFALSE;
                                    G_SYS_DBG_LOG_V( "WARNING:Protected Check-Job data unpencrypted(data ignored!)\r\n");
                                }

                                if (bRetrieveCondData == dxTRUE)
                                {
                                    dxBOOL   bRTCSynchProtectedAndPassCheckFailed = dxFALSE;
                                    uint8_t* pRtcTimeUtc0Data = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_TIME_UTC0,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdData);

                                    //Did we find UTC in the adData? if so this adData must be RTC synch protected so we check for
                                    //the RTC allowed offset
                                    if (pRtcTimeUtc0Data)
                                    {
                                        DKTime_t    SwRtc;
                                        uint32_t    CurrentUTCinSec = 0;
                                        uint32_t    RemoteRtcTime = 0;
                                        dxSwRtcGet(&SwRtc);
                                        CurrentUTCinSec = ConvertDKTimeToUTC(SwRtc);
                                        uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(DK_DATA_TYPE_RTC_TIME_UTC0);

                                        if ((DataLen == sizeof(RemoteRtcTime)))
                                        {
                                            memcpy(&RemoteRtcTime, pRtcTimeUtc0Data, DataLen);
                                            uint32_t RtcDifference = (CurrentUTCinSec > RemoteRtcTime) ? (CurrentUTCinSec - RemoteRtcTime) : (RemoteRtcTime - CurrentUTCinSec);

                                            if (RtcDifference > MAX_RTC_OFFSET_IN_SEC)
                                            {
                                                G_SYS_DBG_LOG_V( "WARNING: AdvanceJobCondition RtcDifference: %d > Max Offset:%d\r\n", RtcDifference, MAX_RTC_OFFSET_IN_SEC);
                                                bRTCSynchProtectedAndPassCheckFailed = dxTRUE;
                                            }
                                        }
                                    }

                                    if (bRTCSynchProtectedAndPassCheckFailed == dxFALSE)
                                    {
                                        uint8_t* pConditionValue = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                                        uint8_t* pData = DkPeripheral_GetDataWithDataTypeIDExt(pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7,
                                                pDevInfoReportData->DkAdvertisementData.AdDataLen, pDevInfoReportData->DkAdvertisementData.AdData);
                                        uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7);

                                        if (pData)
                                        {
                                            uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);

                                            if (ValueSizeInByte)
                                            {
                                                if (ValueSizeInByte == DataLen)
                                                {
                                                    stConditionValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                                    memcpy(&stConditionValue.Value, pConditionValue, ValueSizeInByte);

                                                    stAdvDataTypeValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                                    memcpy(&stAdvDataTypeValue.Value, pData, ValueSizeInByte);

                                                    CondOperator = pJobConditionPayloadHeader->Operator;

                                                    bProceedToConditionCheck = dxTRUE;
                                                }
                                                else
                                                {
                                                    G_SYS_DBG_LOG_V2( "WARNING: Job condition ValueType size is different from DataType len from AdvData! (%d, %d)\r\n", ValueSizeInByte, DataLen );
                                                }
                                            }
                                            else
                                            {
                                                G_SYS_DBG_LOG_V1( "WARNING: Job condition ValueType is ZERO!.. Check GetValueTypeSize for ValueType support of (%d)\r\n", pJobConditionPayloadHeader->ValueType );
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                            case KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG:
                            {
                                if (pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 1)
                                {
                                    //Condition PASS, we simply fake/generate a data for bProceedToConditionCheck to pass.
                                    uint8_t PassCondData = 1;
                                    CondOperator = OPERATOR_EQUAL;

                                    stConditionValue.ValueType = VALUE_TYPE_INT8;
                                    memcpy(&stConditionValue.Value, &PassCondData, sizeof(PassCondData));

                                    stAdvDataTypeValue.ValueType = VALUE_TYPE_INT8;
                                    memcpy(&stAdvDataTypeValue.Value, &PassCondData, sizeof(PassCondData));

                                    bProceedToConditionCheck = dxTRUE;
                                }
                                else if (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 1)
                                {
                                   G_SYS_DBG_LOG_V( "WARNING:KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG unable to handle Status_UnknownData_b2\r\n");
                                }
                            }
                            break;

                            case RTC_CHECK_STDBLE_COND_TYPE_FLAG:
                            {
                                if ((pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 0) &&
                                    (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 0))
                                {
                                    dxBOOL RenewRTCRequired = dxFALSE;
                                    uint8_t* pRtcTimeUtc0Data = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_TIME_UTC0,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdData);
                                    if (pRtcTimeUtc0Data)
                                    {
                                        DKTime_t    SwRtc;
                                        uint32_t    CurrentUTCinSec = 0;
                                        uint32_t    RemoteRtcTime = 0;
                                        dxSwRtcGet(&SwRtc);
                                        CurrentUTCinSec = ConvertDKTimeToUTC(SwRtc);

                                        uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(DK_DATA_TYPE_RTC_TIME_UTC0);
                                        if ((DataLen == sizeof(RemoteRtcTime)))
                                        {
                                            memcpy(&RemoteRtcTime, pRtcTimeUtc0Data, DataLen);
                                            uint32_t RtcDifference = (CurrentUTCinSec > RemoteRtcTime) ? (CurrentUTCinSec - RemoteRtcTime) : (RemoteRtcTime - CurrentUTCinSec);

                                            stAdvDataTypeValue.ValueType = VALUE_TYPE_UINT32;
                                            memcpy(&stAdvDataTypeValue.Value, &RtcDifference, sizeof(RtcDifference));

                                            RenewRTCRequired = dxTRUE;
                                        }
                                    }
                                    else
                                    {
                                        uint8_t* pRtcStatusData = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_STATUS,
                                                                                                pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                                pDevInfoReportData->DkAdvertisementData.AdData);
                                        if (pRtcStatusData)
                                        {
                                            //NOTE: we don't really care the actual status of pRtcStatusData as long as this data type exist
                                            //      when pRtcTimeUtc0Data is not found, we are going to make sure the condition will pass
                                            //      by providing a maximum difference.
                                            uint32_t RtcDifferenceMax = 0xFFFFFFFF;
                                            stAdvDataTypeValue.ValueType = VALUE_TYPE_UINT32;
                                            memcpy(&stAdvDataTypeValue.Value, &RtcDifferenceMax, sizeof(RtcDifferenceMax));
                                            RenewRTCRequired = dxTRUE;
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V( "WARNING:RTC_CHECK_STDBLE_COND_TYPE_FLAG does not contain any RTC data\r\n");
                                        }
                                    }

                                    if (RenewRTCRequired == dxTRUE)
                                    {
                                        uint8_t* pConditionValue = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                                        uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);

                                        stConditionValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                        memcpy(&stConditionValue.Value, pConditionValue, ValueSizeInByte);
                                        CondOperator = pJobConditionPayloadHeader->Operator;

                                        bProceedToConditionCheck = dxTRUE;
                                    }
                                }
                            }
                            break;

                            default:
                            {
                                G_SYS_DBG_LOG_V( "WARNING: StdBle Job cond. type %d Not Yet Supported\r\n", pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 );
                            }
                            break;
                        }

                        if (bProceedToConditionCheck == dxTRUE)
                        {

                            int8_t ChallengeResult = UtilsValueTypeChallengeWithOperator(&stAdvDataTypeValue, &stConditionValue, CondOperator);

                            //G_SYS_DBG_LOG_V( "%s Process %d ChallengeResult=%d\r\n", __FUNCTION__, CurrentCondition, ChallengeResult);

                            if (ChallengeResult > 0)
                            {
                                if (pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 == 0)
                                {
                                    pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 1;
                                }

                                TotalConditionReached++;
                            }
                            else if (ChallengeResult == 0) //Challenge failed
                            {
                                //If Challenge fails we are going to reset its condition "regardless" of time condition status
                                //this will facilitate the condition reset as it should NOT be dependent on time condition
                                pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 0;
                            }
                            else //Error
                            {
                                G_SYS_DBG_LOG_V( "WARNING: ValueTypeChallengeWithOperator failed with invalid value\r\n");
                            }
                        }
                    }
                    else //Address not matched, but since its under the same advance job ID we are going to check the condition reach count
                    {
                        if (pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 == 1)
                        {
                            TotalConditionReached++;
                        }
                    }

                    pJobCondition += (sizeof(JobConditionHeader_t) + pJobConditionHeader->PayloadLen);
                }

                if (JobHeader->JobType.bits.JobConditionOr_b7 == CONDITION_TYPE_AND)
                {
                    //AND all conditions
                    if (TotalConditionReached == JobHeader->TotalJobCondition)
                    {
                        AdvJobConditionReached = dxTRUE;
                    }
                }
                else
                {
                    //OR all conditions
                    if (TotalConditionReached > 0)
                    {
                        AdvJobConditionReached = dxTRUE;
                    }

                    if(JobHeader->JobType.bits.JobConditionRst_b6 == CONDITION_RST_NONE) // If more OR condition reached than last time, CONDITION_RST_NONE will trigger again execution
                    {
                        if(pAdvanceJob->AdvJobLastTotalConditionReached < TotalConditionReached)
                        {
                            pAdvanceJob->AdvJobConditionState = 2; // Force execute if more condition reached than last check
                        }
                    }
                }

                if (pAdvanceJob->AdvJobFirstCheckTick == 0)
                {
                    pAdvanceJob->AdvJobFirstCheckTick = dxTimeGetMS();
                    if(JobHeader->JobType.bits.JobConditionFirstRun_b5 == CONDITION_FIRST_RUN)
                    {
                        G_SYS_DBG_LOG_V( "%s Advjob Execute first run\r\n", __FUNCTION__);
                    }
                    else
                    {
                        G_SYS_DBG_LOG_V( "%s Advjob force not execute first run\r\n", __FUNCTION__);
                        pAdvanceJob->AdvJobConditionState = 1;
                    }
                }

                pAdvanceJob->AdvJobLastTotalConditionReached = TotalConditionReached;
            }

            //G_SYS_DBG_LOG_V1( "Process AdvJobConditionReached=%d\r\n", AdvJobConditionReached);

            dxBOOL UpdateAdvJobStateToServer = dxFALSE;
            switch (pAdvanceJob->AdvJobConditionState)
            {
                case 1: //Current State AdvJob all condition was previously met
                {
                    //State changed?
                    if (AdvJobConditionReached == dxFALSE)
                    {
                        UpdateAdvJobStateToServer = dxTRUE;
                        pAdvanceJob->AdvJobConditionState = 2; // At least one Condition not met
                    }
                }
                break;

                case 2: //Current State AdvJob condition at least one is NOT met
                {
                    //State changed?
                    if (AdvJobConditionReached == dxTRUE)
                    {
                        pAdvanceJob->AdvJobConditionState = 1; // All Condition met

                        UpdateAdvJobStateToServer = dxTRUE;

                        //G_SYS_DBG_LOG_V1( "Process ReExecutionThreshold=%d\r\n", JobHeader->ReExecutionThreshold);

                        if (pAdvanceJob->AdvJobLastExecutedTime && JobHeader->ReExecutionThreshold)
                        {
                            dxTime_t CurrentTimer;
                            CurrentTimer = dxTimeGetMS();

                            //Each ReExecutionThreshold unit is 15 Seconds
                            uint32_t ThresholdBeforeNextTriggerInMilliSeconds = JobHeader->ReExecutionThreshold * 15 * SECONDS;

                            G_SYS_DBG_LOG_V2( "Process AdvJobLastExecutedTime=%d, CurrentTime=%d\r\n",  pAdvanceJob->AdvJobLastExecutedTime, CurrentTimer);

                            if ((CurrentTimer - pAdvanceJob->AdvJobLastExecutedTime) < ThresholdBeforeNextTriggerInMilliSeconds)
                            {
                                UpdateAdvJobStateToServer = dxFALSE;
                            }
                        }
                    }
                }
                break;

                default:
                {
                    //Default to always state change
                    UpdateAdvJobStateToServer = dxTRUE;
                    if (AdvJobConditionReached == dxTRUE)
                    {
                        pAdvanceJob->AdvJobConditionState = 1;// All Condition met
                    }
                    else
                    {
                        pAdvanceJob->AdvJobConditionState = 2;// At least one Condition not met
                    }
                }
                break;
            }

            /*
            G_SYS_DBG_LOG_V( "%s 0x%08x%08x AdvJobConditionState=%d\r\n", __FUNCTION__,
                *((uint32_t *) &(pDevInfoReportData->DevAddress.Addr[4])),
                *((uint32_t *) &(pDevInfoReportData->DevAddress.Addr[0])),
                pAdvanceJob->AdvJobConditionState
            );
            */
            if ((bJobSuperiorOnlyEnabled) && (JobHeader->JobType.bits.JobSuperior_b3 == JOB_SUPERIOR_DISABLE))
            {
                UpdateAdvJobStateToServer = dxFALSE;
            }

            if (TimeConditionReached == dxFALSE)
            {
                UpdateAdvJobStateToServer = dxFALSE;
            }


            if (UpdateAdvJobStateToServer == dxTRUE)
            {

                //Only when all condition met we are going to update the last executed time
                if (pAdvanceJob->AdvJobConditionState == 1)
                {
                    pAdvanceJob->AdvJobLastExecutedTime = dxTimeGetMS();
                }

                if (pAdvanceJob->JobExecutionData) //Its single pack
                {
                    if (pAdvanceJob->AdvJobConditionState == 1) //Make sure all condition met before we execute, otherwise we ignore
                    {
                        G_SYS_DBG_LOG_V( "Process AdvJob SinglePack execution\r\n");

                        ProcessJobExecution(pAdvanceJob->JobExecutionData, (dxTime_t)dxGetUtcInSec(), NULL, SOURCE_OF_ORIGIN_DEFAULT, 0, pAdvanceJob->AdvanceJobID);
                    }
                }
                else //Not single pack, server to decide by sending execution job, here we update the condition state to server
                {
                    G_SYS_DBG_LOG_V( "Process UpdateAdvJobStateToServer\r\n");

                    AdvJobStatusSrv_t AdvJobStatusUpdate;
                    AdvJobStatusUpdate.AdvObjID = pAdvanceJob->AdvanceJobID;
                    AdvJobStatusUpdate.JobStatus = pAdvanceJob->AdvJobConditionState;
                    AdvJobStatusUpdate.TimeStamp = dxGetUtcInSec();

                    G_SYS_DBG_LOG_V( "Process UpdateAdvJobStateToServer @TimeStamp=%d\r\n", AdvJobStatusUpdate.TimeStamp);

                    JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));
                    SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_ADV_JOB_STATUS_UPDATE;
                    SendJobManagerReportEvent->Header.IdentificationNumber = 0;
                    SendJobManagerReportEvent->Header.SourceOfOrigin = SOURCE_OF_ORIGIN_DEFAULT;

                    uint8_t* pAdvJobStatus = dxMemAlloc( "Adv Job Status data Buff", 1, sizeof(AdvJobStatusSrv_t));
                    memcpy(pAdvJobStatus, &AdvJobStatusUpdate, sizeof(AdvJobStatusSrv_t));
                    SendJobManagerReportEvent->pData = pAdvJobStatus;

                    dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

                    if (EventResult != DXOS_SUCCESS)
                    {
                        G_SYS_DBG_LOG_V(  "FATAL ADV JOB UPDATE - JobManager Failed to send Event message back to Application\r\n" );

                        dxMemFree(SendJobManagerReportEvent->pData);
                        dxMemFree(SendJobManagerReportEvent);
                    }
                }
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

exit :

    return;
}

static void ProcessScheduledConditionFromNewDataUpdate(DeviceInfoReport_t* pDevInfoReportData)
{
    int8_t LastIndexPos = 0;

    //TODO: NOTE that current schedule job condition handling DOES NOT yet support multi-condition with "Different" peripherals.
    //      For now we are only supporting multi-condition of same peripherals.
    //      ALSO note that advance job will handle this so it is possible that this function get's merged along with advance job condition check!

    // Skip device missing report data
    if (pDevInfoReportData->rssi == 0) {
        goto exit;
    }

    while (LastIndexPos != -1)
    {
        //Find next matching device in scheduled job list
        LastIndexPos = FindScheduleIndexOfDeviceWithCondition( pDevInfoReportData->DevAddress.Addr, LastIndexPos );

        if (LastIndexPos >= 0)
        {

            //TODO: Note that schedule job WITH condition will only support flag as either ONCE or REPEATABLE... Although there is flexibility but
            //      at the moment we are not going to support actual specific date scheduling + condition at the same time.

            // To protect SIMULTANEOUS ACCESS
            dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

            dxBOOL ConditionReached = dxFALSE; //At least one condition is reached
            DeviceInfoReport_t	DevInfoReportCondData;
            JobSchedule_t* pJobSchedule = &JobScheduledList[LastIndexPos];
            JobHeader_t* JobHeader = (JobHeader_t*)pJobSchedule->JobData;

            uint8_t* pJobCondition = GetJobConditionStartPointer(pJobSchedule->JobData);
            dxBOOL RenewRTCRequired = dxFALSE;

            if (pJobCondition)
            {
                uint8_t CurrentCondition = 0;
                uint8_t TotalConditionReached = 0;
                uint8_t TotalConditionPreviouslyReached = 0;

                memcpy(&DevInfoReportCondData, pDevInfoReportData, sizeof(DeviceInfoReport_t));
                memset(DevInfoReportCondData.DkAdvertisementData.AdData,0, sizeof(DevInfoReportCondData.DkAdvertisementData.AdData));
                DevInfoReportCondData.DkAdvertisementData.AdDataLen = 0;
                uint8_t* pReportCondAdvData = DevInfoReportCondData.DkAdvertisementData.AdData;
                uint64_t ConditionMap;

                ConditionMap = 0;

                for (CurrentCondition = 0; CurrentCondition < JobHeader->TotalJobCondition; CurrentCondition++)
                {
                    JobConditionHeader_t* JobConditionHeader = (JobConditionHeader_t*)pJobCondition;
                    JobConditionPayloadHeader_t* pJobConditionPayloadHeader = (JobConditionPayloadHeader_t*)(pJobCondition + sizeof(JobConditionHeader_t));

                    // For debug
                    //G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_JOBMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                    //                 "INFORMATION:(Line=%d) b12_15:%X LastIndexPos:%d CurrentCondition:%d/%d\r\n",
                    //                 __LINE__,
                    //                 pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15,
                    //                 LastIndexPos,
                    //                 CurrentCondition, JobHeader->TotalJobCondition);

                    dxBOOL      bProceedToConditionCheck = dxFALSE;
                    dxBOOL      bSaveAdvData = dxFALSE;
                    TypeValue_t stConditionValue;
                    TypeValue_t stAdvDataTypeValue;
                    uint8_t     CondOperator;

                    RenewRTCRequired = dxFALSE;

                    switch (pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15)
                    {
                        case PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG:
                        case STANDARD_CHECK_STDBLE_COND_TYPE_FLAG:      // Should be, NOT JOB_CONDITION_TYPE_STD_BLE_PERIPHERAL:
                        {
                            dxBOOL  bRetrieveCondData = dxTRUE;
                            if ((pJobConditionPayloadHeader->ConditionType.bits.ConditionTypeFlag_b12_15 == PROTECTED_CHECK_STDBLE_COND_TYPE_FLAG) &&
                                ((pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 1) ||
                                 (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 1)))
                            {
                                bRetrieveCondData = dxFALSE;
                                G_SYS_DBG_LOG_V( "WARNING:Protected Check-Job data unpencrypted(data ignored!)\r\n");
                            }

                            if (bRetrieveCondData == dxTRUE)
                            {
                                dxBOOL   bRTCSynchProtectedAndPassCheckFailed = dxFALSE;
                                uint8_t* pRtcTimeUtc0Data = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_TIME_UTC0,
                                                                                        pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                        pDevInfoReportData->DkAdvertisementData.AdData);

                                //Did we find UTC in the adData? if so this adData must be RTC synch protected so we check for
                                //the RTC allowed offset
                                if (pRtcTimeUtc0Data)
                                {
                                    DKTime_t    SwRtc;
                                    uint32_t    CurrentUTCinSec = 0;
                                    uint32_t    RemoteRtcTime = 0;
                                    dxSwRtcGet(&SwRtc);
                                    CurrentUTCinSec = ConvertDKTimeToUTC(SwRtc);
                                    uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(DK_DATA_TYPE_RTC_TIME_UTC0);

                                    if ((DataLen == sizeof(RemoteRtcTime)))
                                    {
                                        memcpy(&RemoteRtcTime, pRtcTimeUtc0Data, DataLen);
                                        uint32_t RtcDifference = (CurrentUTCinSec > RemoteRtcTime) ? (CurrentUTCinSec - RemoteRtcTime) : (RemoteRtcTime - CurrentUTCinSec);

                                        if (RtcDifference > MAX_RTC_OFFSET_IN_SEC)
                                        {
                                            G_SYS_DBG_LOG_V( "WARNING: ScheduledCondition RtcDifference: %d > Max Offset:%d\r\n", RtcDifference, MAX_RTC_OFFSET_IN_SEC);
                                            bRTCSynchProtectedAndPassCheckFailed = dxTRUE;
                                        }
                                    }
                                }

                                uint8_t* pConditionValue = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                                uint8_t* pData = DkPeripheral_GetDataWithDataTypeIDExt(pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7,
                                                                                       pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                       pDevInfoReportData->DkAdvertisementData.AdData);
                                uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7);
                                if (pData)
                                {
                                    uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);

                                    if (ValueSizeInByte)
                                    {
                                        if (ValueSizeInByte == DataLen)
                                        {
                                            stConditionValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                            memcpy(&stConditionValue.Value, pConditionValue, ValueSizeInByte);

                                            stAdvDataTypeValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                            memcpy(&stAdvDataTypeValue.Value, pData, ValueSizeInByte);

                                            CondOperator = pJobConditionPayloadHeader->Operator;

                                            bProceedToConditionCheck = dxTRUE;

                                            if (bRTCSynchProtectedAndPassCheckFailed == dxFALSE)
                                            {
                                                bSaveAdvData = dxTRUE;
                                            }
                                        }
                                        else
                                        {
                                            G_SYS_DBG_LOG_V2( "WARNING: Job condition ValueType size is different from DataType len from AdvData! (%d, %d)\r\n", ValueSizeInByte, DataLen );
                                        }
                                    }
                                    else
                                    {
                                        G_SYS_DBG_LOG_V1( "WARNING: Job condition ValueType is ZERO!.. Check GetValueTypeSize for ValueType support of (%d)\r\n", pJobConditionPayloadHeader->ValueType );
                                    }
                                }
                            }
                        }
                        break;

                        case KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG:
                        {
                            if (pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 1)
                            {
                                //Condition PASS, we simply fake/generate a data for bProceedToConditionCheck to pass.
                                uint8_t PassCondData = 1;
                                CondOperator = OPERATOR_EQUAL;

                                stConditionValue.ValueType = VALUE_TYPE_INT8;
                                memcpy(&stConditionValue.Value, &PassCondData, sizeof(PassCondData));

                                stAdvDataTypeValue.ValueType = VALUE_TYPE_INT8;
                                memcpy(&stAdvDataTypeValue.Value, &PassCondData, sizeof(PassCondData));

                                bProceedToConditionCheck = dxTRUE;
                            }
                            else if (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 1)
                            {
                               G_SYS_DBG_LOG_V( "WARNING:KEYPROTECTED_CHECK_STDBLE_COND_TYPE_FLAG unable to handle Status_UnknownData_b2\r\n");
                            }
                        }
                        break;

                        case RTC_CHECK_STDBLE_COND_TYPE_FLAG:
                        {
                            if ((pDevInfoReportData->Status.bits.Status_NoSecurity_b1 == 0) &&
                                (pDevInfoReportData->Status.bits.Status_UnknownData_b2 == 0))
                            {
                                //dxBOOL RenewRTCRequired = dxFALSE;
                                uint8_t* pRtcTimeUtc0Data = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_TIME_UTC0,
                                                                                        pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                        pDevInfoReportData->DkAdvertisementData.AdData);
                                if (pRtcTimeUtc0Data)
                                {
                                    DKTime_t    SwRtc;
                                    uint32_t    CurrentUTCinSec = 0;
                                    uint32_t    RemoteRtcTime = 0;
                                    dxSwRtcGet(&SwRtc);
                                    CurrentUTCinSec = ConvertDKTimeToUTC(SwRtc);

                                    uint8_t DataLen = DKPeripheral_GetDataTypeLenForDataTypeID(DK_DATA_TYPE_RTC_TIME_UTC0);
                                    if ((DataLen == sizeof(RemoteRtcTime)))
                                    {
                                        memcpy(&RemoteRtcTime, pRtcTimeUtc0Data, DataLen);
                                        uint32_t RtcDifference = (CurrentUTCinSec > RemoteRtcTime) ? (CurrentUTCinSec - RemoteRtcTime) : (RemoteRtcTime - CurrentUTCinSec);

                                        G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_JOBMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
                                                         "INFORMATION:(Line=%d) RTC:%u RemoteRtcTime:%u RtcDifference:%u\r\n",
                                                         __LINE__,
                                                         CurrentUTCinSec,
                                                         RemoteRtcTime,
                                                         RtcDifference);

                                        stAdvDataTypeValue.ValueType = VALUE_TYPE_UINT32;
                                        memcpy(&stAdvDataTypeValue.Value, &RtcDifference, sizeof(RtcDifference));

                                        RenewRTCRequired = dxTRUE;
                                    }
                                }
                                else
                                {
                                    uint8_t* pRtcStatusData = DkPeripheral_GetDataWithDataTypeIDExt( DK_DATA_TYPE_RTC_STATUS,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdData);
                                    if (pRtcStatusData)
                                    {
                                        //NOTE: we don't really care the actual status of pRtcStatusData as long as this data type exist
                                        //      when pRtcTimeUtc0Data is not found, we are going to make sure the condition will pass
                                        //      by providing a maximum difference.
                                        uint32_t RtcDifferenceMax = 0xFFFFFFFF;
                                        stAdvDataTypeValue.ValueType = VALUE_TYPE_UINT32;
                                        memcpy(&stAdvDataTypeValue.Value, &RtcDifferenceMax, sizeof(RtcDifferenceMax));
                                        RenewRTCRequired = dxTRUE;
                                    }
                                    else
                                    {
                                        G_SYS_DBG_LOG_V( "WARNING:RTC_CHECK_STDBLE_COND_TYPE_FLAG does not contain any RTC data\r\n");
                                    }
                                }

                                if (RenewRTCRequired == dxTRUE)
                                {
                                    uint8_t* pConditionValue = pJobCondition + sizeof(JobConditionHeader_t) + sizeof(JobConditionPayloadHeader_t);
                                    uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);

                                    stConditionValue.ValueType = pJobConditionPayloadHeader->ValueType;
                                    memcpy(&stConditionValue.Value, pConditionValue, ValueSizeInByte);
                                    CondOperator = pJobConditionPayloadHeader->Operator;

                                    bProceedToConditionCheck = dxTRUE;
                                }
                            }
                        }
                        break;

                        default:
                        break;
                    }

                    if (bProceedToConditionCheck == dxTRUE)
                    {
                        int8_t ChallengeResult = UtilsValueTypeChallengeWithOperator(&stAdvDataTypeValue, &stConditionValue, CondOperator);

                        if (ChallengeResult > 0) //Challenge success
                        {
                            ConditionMap = ConditionMap | 0x01 << CurrentCondition;
                            //If last executed time is 0 this means either the system starts up from fresh or system re-login due to session timeout or connection issue
                            //for that we are simply going to set the condition reached flag when condition challenge is success. This prevents firing the execution such as
                            //send notification or save to historic event falsely. Last executed time will be set to dirty at the end of each job schedule condition iteration process.
                            if (pJobSchedule->JobLastExecutedTime == 0)
                            {
                                pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 1;
                            }
                            else if (pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 == 0)
                            {
                                pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 1;
                            }
                            else //Condition was reached before
                            {
                                //We are going to record total condition that was already reached before
                                TotalConditionPreviouslyReached++;
                            }

                            if (bSaveAdvData == dxTRUE)
                            {
                                uint8_t ValueSizeInByte = UtilsGetValueTypeSize(pJobConditionPayloadHeader->ValueType);

                                if ((DevInfoReportCondData.DkAdvertisementData.AdDataLen + ValueSizeInByte + 1) <
                                    PERIPHERAL_DATA_TYPE_PLAYLOAD_SIZE)
                                {

                                    uint8_t* pData = DkPeripheral_GetDataWithDataTypeIDExt( pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdDataLen,
                                                                                            pDevInfoReportData->DkAdvertisementData.AdData);

                                    *pReportCondAdvData = pJobConditionPayloadHeader->ConditionType.bits.ConditionField_b0_7;
                                    pReportCondAdvData++; DevInfoReportCondData.DkAdvertisementData.AdDataLen++;
                                    memcpy(pReportCondAdvData, pData, ValueSizeInByte);
                                    pReportCondAdvData += ValueSizeInByte; DevInfoReportCondData.DkAdvertisementData.AdDataLen += ValueSizeInByte;
                                }
                                else
                                {
                                    G_SYS_DBG_LOG_V( "WARNING: pReportCondAdvData reached max allowed size!\r\n");
                                }
                            }

                            TotalConditionReached++;
                        }
                        else if (ChallengeResult == 0) //Challenge failed
                        {
                            pJobConditionPayloadHeader->ConditionType.bits.ConditionReached_b8 = 0;
                        }
                        else //Error
                        {
                            G_SYS_DBG_LOG_V( "WARNING: ValueTypeChallengeWithOperator failed with invalid value\r\n");
                        }
                    }
                    pJobCondition += (sizeof(JobConditionHeader_t) + JobConditionHeader->PayloadLen);
                }

                if (JobHeader->JobType.bits.JobConditionOr_b7 == CONDITION_TYPE_AND)
                {
//                    G_SYS_DBG_LOG_IV(G_SYS_DBG_CATEGORY_JOBMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION,
//                                     "INFORMATION:(Line=%d) TotalConditionReached:%d TotalJobCondition:%d\r\n",
//                                     __LINE__,
//                                     TotalConditionReached,
//                                     JobHeader->TotalJobCondition);

                    //AND all conditions
                    if (TotalConditionReached == JobHeader->TotalJobCondition)
                    {
                        //NOTE: Only send a condition reached flag if previously reached is not equal to total reach count
                        //      Since job is re-checked whenever value changes, this prevents false reach condition if all condition
                        //      was reached before (meaning reach flag already sent).
                        if (TotalConditionPreviouslyReached != TotalConditionReached)
                        {
//                            G_SYS_DBG_LOG_V( "%s And ConditionReached = dxTRUE\r\n", __FUNCTION__);
                            ConditionReached = dxTRUE;
                        }
                    }
//                    if(!ConditionReached)
//                        G_SYS_DBG_LOG_V( "%s And ConditionReached = dxFALSE\r\n", __FUNCTION__);
                }
                else
                {
                    //OR all conditions
                    if(JobHeader->JobType.bits.JobConditionRst_b6 == CONDITION_RST_NONE) // If more OR condition reached than last time, CONDITION_RST_NONE will trigger again execution
                    {
/*
                        G_SYS_DBG_LOG_V( "%s ConditionMap = 0x%08x%08x LastConditionMap = 0x%08x%08x\r\n", __FUNCTION__
                            , (uint32_t) ((ConditionMap >> 32) & 0xFFFFFFFF)
                            , (uint32_t) (ConditionMap & 0xFFFFFFFF)
                            , (uint32_t) ((pJobSchedule->LastConditionMap >> 32) & 0xFFFFFFFF)
                            , (uint32_t) (pJobSchedule->LastConditionMap & 0xFFFFFFFF)
                            );
*/
                        if ((TotalConditionReached >= TotalConditionPreviouslyReached)
                            && (ConditionMap != pJobSchedule->LastConditionMap))
                        {
//                            G_SYS_DBG_LOG_V( "%s Or CONDITION_RST_NONE ConditionReached = dxTRUE\r\n", __FUNCTION__);
                            ConditionReached = dxTRUE;
                        }
                        else
                        {
//                            G_SYS_DBG_LOG_V( "%s Or CONDITION_RST_NONE ConditionReached = dxFALSE, Reached = %d, LastReached = %d\r\n", __FUNCTION__, TotalConditionReached, TotalConditionPreviouslyReached);
                        }
                    }
                    else
                    {
                        if ((TotalConditionReached > 0) && (TotalConditionPreviouslyReached == 0))
                        {
//                            G_SYS_DBG_LOG_V( "%s Or CONDITION_RST_ALL ConditionReached = dxTRUE\r\n", __FUNCTION__);
                            ConditionReached = dxTRUE;
                        }
                        else
                        {
//                            G_SYS_DBG_LOG_V( "%s Or CONDITION_RST_ALL ConditionReached = dxFALSE, Reached = %d, LastReached = %d\r\n", __FUNCTION__, TotalConditionReached, TotalConditionPreviouslyReached);
                        }
                    }
                }
                pJobSchedule->LastConditionMap = ConditionMap;
            }

            if (ConditionReached == dxTRUE)
            {
                // We need to renew RTC ASAP.  The easy way is to set JobLastExecutedTime to initial value (0).
                // If the value is not zero, we will wait for 15 seconds before renew
                if (RenewRTCRequired == dxTRUE)
                {
                    pJobSchedule->JobLastExecutedTime = 0;
                }

                //TODO: ConditionReached does not mean is ready for execution, we will have to go througth all job condition (if more than 1 in the list)
                //      and check if ConditionReached_b8 is set for all. We are only going to support first job condition for now,
                //      supporting multiple job condition check require a lot more work and will be implement it late when needed.

                dxBOOL JobExecutionNeeded = dxTRUE;
                if (pJobSchedule->JobLastExecutedTime && JobHeader->ReExecutionThreshold)
                {
                    dxTime_t CurrentTimer;
                    CurrentTimer = dxTimeGetMS();

                    //Each ReExecutionThreshold unit is 15 Seconds
                    uint32_t ThresholdBeforeNextTriggerInMilliSeconds = JobHeader->ReExecutionThreshold * 15 * SECONDS;

                    if ((CurrentTimer - pJobSchedule->JobLastExecutedTime) < ThresholdBeforeNextTriggerInMilliSeconds)
                    {
                        JobExecutionNeeded = dxFALSE;
                    }
                }

                if (pJobSchedule->JobLastExecutedTime == 0)
                {
                    if(JobHeader->JobType.bits.JobConditionFirstRun_b5 == CONDITION_FIRST_RUN)
                    {
                        G_SYS_DBG_LOG_V( "%s Execute first run\r\n", __FUNCTION__);
                    }
                    else
                    {
                        G_SYS_DBG_LOG_V( "%s Force not execute first run\r\n", __FUNCTION__);
                        JobExecutionNeeded = dxFALSE;
                    }
                    pJobSchedule->JobLastExecutedTime = 1; //Mark Dirty
                }

                if ((bJobSuperiorOnlyEnabled) && (JobHeader->JobType.bits.JobSuperior_b3 == JOB_SUPERIOR_DISABLE))
                {
                    JobExecutionNeeded = dxFALSE;
                }

                if (JobExecutionNeeded == dxTRUE)
                {
                    pJobSchedule->JobLastExecutedTime = dxTimeGetMS();

                    //NOTE: We are passing the DevInfoReportCondData instead of pDevInfoReportData because we are going to filter out
                    //      the unneeded data type from the datatype payload. This will aid for example, when job execution is sending
                    //      the report data to historic event, it will ONLY contain the changed data of data type. Therefore, when getting the
                    //      historic event it will not contain other unecessary data in the adv data payload which might confuse what value was
                    //      changed/triggered in the first place. So only the one from matched condition(s) is copied to DevInfoReportCondData.
                    ProcessJobExecution(pJobSchedule->JobData, (dxTime_t)dxGetUtcInSec(), &DevInfoReportCondData, SOURCE_OF_ORIGIN_DEFAULT, 0, 0);
                }
            }
            else
            {
                //NOTE: We will have to mark dirty even if condition is not met, this workaround is related to CONDITION_FIRST_RUN which our original
                //      intension is "The FIRST device status we received, if condition is met then we check if we need to execute first time", therefore,
                //      if we received status change afterwards we should NOT check for CONDITION_FIRST_RUN!.
                pJobSchedule->JobLastExecutedTime = 1; //Mark Dirty
            }

            LastIndexPos++; //Next

            // Release control
            dxSemGive(JobAccessResourceProtectionSemaHandle);

        }
#if ENABLE_AUTOMATIC_PRESET_SCHEDULED_JOB_CREATION
        else
        {

            //Check device type and create any necessary presets of scheduled job (eg, Door Sensor for Notification)
            switch (pDevInfoReportData->DkAdvertisementData.Header.ProductID)
            {
                case DK_PRODUCT_TYPE_MOTION_SENSOR_ID:
                {

                    static dxBOOL MotionAlreadyAdded = dxFALSE;

                    if (MotionAlreadyAdded == dxFALSE)
                    {
                        //G_SYS_DBG_LOG_V( "Creating Scheduled Job for DK_PRODUCT_TYPE_MOTION_SENSOR_ID\r\n" );
                        //CreateMotionSensorScheduleIntervalJob(pDevInfoReportData);

                        G_SYS_DBG_LOG_V( "Creating Advance Job for DK_PRODUCT_TYPE_MOTION_SENSOR_ID\r\n" );
                        CreateMotionTriggerAdvJob(pDevInfoReportData);

                        MotionAlreadyAdded = dxTRUE;
                    }

                }
                break;

                case DK_PRODUCT_TYPE_HOME_DOOR_ID:
                {
                    //Since we are in while loop we will always get to here so we need to make sure we DO NOT create more than one job in here
                    //So we check again starting from index 0 and see if we do find any matching peripheral address. If not then we add otherwise its already added and we simply skip.
                    static int8_t StartPositionToLookFor = 0;
                    //StartPositionToLookFor = FindScheduleIndexOfDeviceWithCondition( pDevInfoReportData->DevAddress.Addr, StartPositionToLookFor );

                    if (StartPositionToLookFor == 0)
                    {
                        //Create HomeDoor Scheduled Job and add to list
                        G_SYS_DBG_LOG_V( "Creating Scheduled Job for DK_PRODUCT_TYPE_HOME_DOOR_ID\r\n" );
                        CreateHomeDoorScheduleJob(pDevInfoReportData);

                        StartPositionToLookFor++;
                    }

                }
                break;

                case DK_PRODUCT_TYPE_POWERPLUG_ID:
                {

                    /*
                    G_SYS_DBG_LOG_V( "Creating Scheduled Job for DK_PRODUCT_TYPE_POWERPLUG_ID\r\n" );
                    //CreatePowerPlugScheduleIntervalJob(pDevInfoReportData);
                    CreatePowerPlugScheduleDayAndTimeJob(pDevInfoReportData);
                    */
                }
                break;

                case DK_PRODUCT_TYPE_WEATHER_ID:
                {
                    /*
                    static dxBOOL WeatherAlreadyAdded = dxFALSE;

                    if (WeatherAlreadyAdded == dxFALSE)
                    {
                        G_SYS_DBG_LOG_V( "Creating Scheduled Job for DK_PRODUCT_TYPE_WEATHER_ID\r\n" );
                        CreateWeatherCubeScheduleIntervalJob(pDevInfoReportData);
                        WeatherAlreadyAdded = dxTRUE;
                    }
                    */
                }
                break;


                default:
                break;
            }
        }

#endif

    }

exit :

    return;
}


static void ProcessNonConditionSchedules(void)
{
    int16_t     i = 0;
    DKTime_t    UTCTimeDk = dxGetUtcTime();

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[i];
        if (pJobSchedule->SlotAvailable != dxTRUE)
        {
            JobHeader_t* JobHeader = (JobHeader_t*)pJobSchedule->JobData;

            //Make sure there is no condition as from here we only handle pure schedule Jobs... Condition with/without schedule job will
            //be handled elsewhere
            if (JobHeader->TotalJobCondition == 0)
            {
                dxBOOL OnSchedule = dxFALSE;

                //TODO:  1) Check if interval execution time is up (DONE)
                //       2) Check if schedule in JobHeader match current date/time
                if (JobHeader->ScheduleFlag.bits.ShceduleInterval_b10)
                {
                    dxTime_t CurrentTime;
                    CurrentTime = dxTimeGetMS();

                    //First Time? if not then check if next interval reached
                    if (pJobSchedule->JobIntervalUpcomingExecuteTime == 0)
                    {
                        G_SYS_DBG_LOG_V1( "ProcessNonConditionSchedules (SchTime = %d)\r\n", JobHeader->ScheduleTime.Time * SECONDS);

                        //Update the next iteration
                        pJobSchedule->JobIntervalUpcomingExecuteTime = CurrentTime + (JobHeader->ScheduleTime.Time * SECONDS);
                    }
                    else if (CurrentTime >= pJobSchedule->JobIntervalUpcomingExecuteTime)
                    {
                        //Interval reached.. let's execute
                        OnSchedule = dxTRUE;

                        //Update the next iteration
                        pJobSchedule->JobIntervalUpcomingExecuteTime = CurrentTime + (JobHeader->ScheduleTime.Time * SECONDS);
                    }
                }
                else //Non interval schedule
                {
                    dxTime_t CurrentTime;
                    CurrentTime = dxTimeGetMS();

                    //TODO: To further support Month and Year... Currently we only support up to days and hour/minute

                    //Make sure we do not re-execute the same task again within ~70 seconds (just a bit more than a minute) since its last execution time
                    if ((CurrentTime - pJobSchedule->JobLastExecutedTime) > 70*SECONDS)
                    {
                        if (    ((JobHeader->ScheduleFlag.bits.ShceduleMon_b1) && (UTCTimeDk.dayofweek == 1)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleTue_b2) && (UTCTimeDk.dayofweek == 2)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleWed_b3) && (UTCTimeDk.dayofweek == 3)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleThu_b4) && (UTCTimeDk.dayofweek == 4)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleFri_b5) && (UTCTimeDk.dayofweek == 5)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleSat_b6) && (UTCTimeDk.dayofweek == 6)) ||
                                ((JobHeader->ScheduleFlag.bits.ShceduleSun_b7) && (UTCTimeDk.dayofweek == 7)) )
                        {
                            if (    (JobHeader->ScheduleTime.bits.ShceduleTimeHour_b8_15 == UTCTimeDk.hour) &&
                                    (JobHeader->ScheduleTime.bits.ScheduleTimeMinute_b0_7 == UTCTimeDk.min) )
                            {
                                OnSchedule = dxTRUE;
                            }
                        }
                    }
                }

                if (OnSchedule == dxTRUE)
                {
                    pJobSchedule->JobLastExecutedTime = dxTimeGetMS();
                    ProcessJobExecution(pJobSchedule->JobData, (dxTime_t)dxGetUtcInSec(), NULL, SOURCE_OF_ORIGIN_DEFAULT, 0, 0);
                }
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

}



static TASK_RET_TYPE Job_Manager_Process_thread_main( TASK_PARAM_TYPE arg )
{

    while (1)
    {

        JobManagerJobQueue_t* pGetJobBuff = NULL;
        if (dxQueueReceive(  JobManagerJobQueue,
                             &pGetJobBuff,
                             MAXIMUM_JOB_MANAGER_JOB_WAIT_TIME) == DXOS_SUCCESS)
        {
            if (pGetJobBuff)
            {
                //G_SYS_DBG_LOG_V1("pGetJobBuff->JobType = %d\n", pGetJobBuff->JobType);

                switch(pGetJobBuff->JobType)
                {
                    case JOB_MANAGER_NEW_ADV_JOB:
                    case JOB_MANAGER_NEW_JOB:
                    case JOB_MANAGER_ADV_JOB_SINGLE_PACK_EXEC:
                    {
                        if(pGetJobBuff->JobData)
                        {
                            JobHeader_t* JobHeader = (JobHeader_t*)pGetJobBuff->JobData;

                            /*
                            G_SYS_DBG_LOG_V( "Job_Manager_Process_thread_main JOB_MANAGER_NEW_JOB Gateway MAC\r\n" );
                            uint16_t i = 0;
                            for (i = 0; i < B_ADDR_LEN_8; i++)
                            {
                                G_SYS_DBG_LOG_NV1(  "%x:", JobHeader->DeviceMacAddress.MacAddr[i] );
                            }
                            G_SYS_DBG_LOG_V( "\r\n" );
                            */
                            if (JobHeader->Preamble == JOB_HEADER_PREAMBLE)
                            {
                                //For execution job that is advanced single pack we simply add to the list
                                if (pGetJobBuff->JobType == JOB_MANAGER_ADV_JOB_SINGLE_PACK_EXEC)
                                {
                                    NewAdvanceJobExecutionProcess(pGetJobBuff->JobData, pGetJobBuff->JobDataLen, pGetJobBuff->JobAdvId, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                                }
                                else //All other if its pure execution we execute it, otherwise we process it as schedule or advance job condition and add to list
                                {
                                    if ((JobHeader->ScheduleFlag.bits.ScheduleOnce_b0) && (!JobHeader->TotalJobCondition))
                                    {
                                        //This is an instance JOB (its non scheduled and it doesn't contain any condition) so we process it right away
                                        //Note that advance job execution also falls into this category
                                        ProcessJobExecution(pGetJobBuff->JobData, pGetJobBuff->JobTimeStamp, NULL, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber, pGetJobBuff->JobAdvId);
                                    }
                                    else
                                    {
                                        if (pGetJobBuff->JobType == JOB_MANAGER_NEW_JOB)
                                        {
                                            NewJobScheduledConditionProcess(pGetJobBuff->JobData, pGetJobBuff->JobDataLen, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                                        }
                                        else if (pGetJobBuff->JobType == JOB_MANAGER_NEW_ADV_JOB)
                                        {
                                            NewAdvanceJobConditionProcess(pGetJobBuff->JobData, pGetJobBuff->JobDataLen, pGetJobBuff->JobAdvId, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                G_SYS_DBG_LOG_V( "WARNING: Unknown JOB Header PREAMBLE!!!.. Job Ignored\r\n" );
                            }
                        }
                    }
                    break;

                    case JOB_MANAGER_PERIPHERAL_DATA_UPDATE_ALIVE:
                    case JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE_FIRST_TIME:
                    case JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE:
                    {
                        //NOTE: We always need to iterate all Scheduled and AdvanceJob here since some peripheral is configured as advertisement status only
                        //and will arrive either at aline update or new data update.
                        DeviceInfoReport_t* pDevInfoReportData = (DeviceInfoReport_t*)pGetJobBuff->JobData;

                        //Check the scheduled Job
                        ProcessScheduledConditionFromNewDataUpdate(pDevInfoReportData);
                        //Check the Advance Job
                        ProcessAdvanceJobConditionFromNewDataUpdate(pDevInfoReportData);

                        //NOTE: For data update alive we don't ready care if this data is lost or failed to update to server.
                        ProcessDeviceNewDataUpdate(pGetJobBuff->JobData, pGetJobBuff->JobType, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                    }
                    break;

                    case JOB_MANAGER_PERIPHERAL_JOB_CHECK:
                    {
                        DeviceInfoReport_t* pDevInfoReportData = (DeviceInfoReport_t*)pGetJobBuff->JobData;
                        //Check the scheduled Job
                        ProcessScheduledConditionFromNewDataUpdate(pDevInfoReportData);
                        //Check the Advance Job
                        ProcessAdvanceJobConditionFromNewDataUpdate(pDevInfoReportData);
                    }
                    break;

                    case JOB_MANAGER_PERIPHERAL_DATA_HISTORIC_EVENT_UPDATE_ONLY:
                    {
                        ProcessDeviceNewDataUpdate(pGetJobBuff->JobData, pGetJobBuff->JobType, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                    }
                    break;

                    case JOB_MANAGER_PERIPHERAL_STATUS_NEW_UPDATE:
                    {

                        ePeripheralSignalStrength CurrentSignalStrength = SIGNAL_STRENGTH_NONE;
                        DeviceInfoReport_t* PeripheralInfo =  (DeviceInfoReport_t*)pGetJobBuff->JobData;
                        CONVERT_SIGNAL_STREGTH(PeripheralInfo->rssi,CurrentSignalStrength)

                        //Update the latest info to PeripheralsDataStatus
                        JobManagerPeripheral_t* pPeripheralDataStatus = &PeripheralsDataStatus[PeripheralInfo->DevHandlerID];
                        pPeripheralDataStatus->SignalStrengthConsDiffCount = 0;
                        pPeripheralDataStatus->LastUpdatedSignalStrength = CurrentSignalStrength;
                        pPeripheralDataStatus->LastUpdatedBatteryInfo = PeripheralInfo->DkAdvertisementData.BatteryInfo;
                        pPeripheralDataStatus->LastKnowStatus = PeripheralInfo->Status.flag;
                        ProcessDeviceNewDataUpdate(pGetJobBuff->JobData, pGetJobBuff->JobType, pGetJobBuff->JobSourceOfOrigin, pGetJobBuff->JobIdentificationNumber);
                    }
                    break;

                    case JOB_MANAGER_CLEAN_ALL_JOB:
                    {

                        _RemoveAllScheduleJobs (dxFALSE);
                        _RemoveAllAdvanceJobs (dxFALSE);

                        if (RegisteredEventFunction)
                        {

                            JobManagerEventReportObj_t* SendJobManagerReportEvent = dxMemAlloc( "JobManager report event Buff", 1, sizeof(JobManagerEventReportObj_t));

                            SendJobManagerReportEvent->Header.EventType = JOB_MANAGER_EVENT_JOB_REMOVED_ALL;
                            SendJobManagerReportEvent->Header.IdentificationNumber = pGetJobBuff->JobIdentificationNumber;
                            SendJobManagerReportEvent->Header.SourceOfOrigin = pGetJobBuff->JobSourceOfOrigin;
                            SendJobManagerReportEvent->pData = NULL;

                            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, SendJobManagerReportEvent);

                            if (EventResult != DXOS_SUCCESS)
                            {
                                G_SYS_DBG_LOG_V( "FATAL JOB_MANAGER_CLEAN_ALL_JOB(%d)-Failed to send Event message back to Application\r\n",EventResult );

                                if (SendJobManagerReportEvent->pData)
                                    dxMemFree(SendJobManagerReportEvent->pData);

                                dxMemFree(SendJobManagerReportEvent);
                            }
                        }
                    }
                    break;

                    case JOB_MANAGER_REMOVE_PERIPHERAL_ALL_SPECIFIC_SCHEDULED_JOB:
                    {
                        uint32_t PeripheralID = *((uint32_t*)pGetJobBuff->JobData);

                        //WARNING:  We are NOT going to protect the resource (JobScheduleList) here because
                        //          RemoveScheduleJobFromList already does. Doing so here will generate deadlock!
                        uint16_t i = 0;
                        for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
                        {
                            if (JobScheduledList[i].SlotAvailable == dxFALSE)
                            {
                                if (JobScheduledList[i].JobData)
                                {
                                    JobHeader_t* JobHeader = (JobHeader_t*)JobScheduledList[i].JobData;

                                    if (JobHeader->ScheduleSequenceNumber.Component.PeripheralUniqueID == PeripheralID)
                                    {

                                        RemoveScheduleJobFromList(i);

                                        //We don't Break! here... we go over all item and delete all schedule/longterm job that is related to PeripheralID
                                    }
                                }
                            }
                        }
                    }
                    break;

                    default:
                    break;
                }

                //Make sure we free the Job Data in Job Message
                if(pGetJobBuff->JobData)
                    dxMemFree(pGetJobBuff->JobData);

                //Free the Job Msg
                dxMemFree(pGetJobBuff);
            }
        }

        //We will be executing below common check at least once after JOB wait timeout or follow by whenever job is available
        {
            static dxTime_t LastCheckedTime = 0;
            dxTime_t CurrentTimer;
            CurrentTimer = dxTimeGetMS();

            //TODO: Let's go over all scheduled Job and report event if any Job needs to be executed, WHEN job reaches scheduled or condition criteria we can call
            //      ProcessJobExecution() to process the Job. NOTE: ProcessJob will AUTOMATICALLY remove the Job from watch list IF the Job is Non-Scheduled Job (eg, Once only).

            if ((CurrentTimer - LastCheckedTime) > JOB_SCHEDULE_NO_CONDITION_CHECK_MIN_INTERVAL)
            {
                LastCheckedTime = CurrentTimer;

                ProcessNonConditionSchedules();

                DKTime_t CurrentUTCTime;
                dxSwRtcGet(&CurrentUTCTime);

                //G_SYS_DBG_LOG_V4("Date = %d-%d-%d, %d\n", CurrentUTCTime.year, CurrentUTCTime.month, CurrentUTCTime.day, CurrentUTCTime.dayofweek);
                //G_SYS_DBG_LOG_V3("Time = %d:%d:%d\n", CurrentUTCTime.hour, CurrentUTCTime.min, CurrentUTCTime.sec);
            }

#if ENABLE_MOBILE_CLIENT_SESSION_TIME_OUT_CHECK

#pragma message ( "REMINDER: Job Manager Mobile Client Session timeout check is ENABLED!" )

            //G_SYS_DBG_LOG_V2("MobileClientSessionTimeOutTime = %ld (%ld)\r\n", MobileClientSessionTimeOutTime, CurrentTimer);

            if ((!MobileClientSessionTimeOutTime) || (CurrentTimer > MobileClientSessionTimeOutTime))
            {
                MobileClientIsListening = dxFALSE;
            }
            else
            {
                MobileClientIsListening = dxTRUE;
            }
#endif
        }
    }

    END_OF_TASK_FUNCTION;
}

dxJOB_MANAGER_RET_CODE PeripheralPayloadUpdateCheckProcess(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber, dxBOOL ForceUpdate)
{
    dxBOOL DataNeedsUpdate = dxFALSE;
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    //NOTE: We are not going to print log message here even if the advlen is zero
    //      the reason is because this could happen when the update is for status update (signal) while there isn't yet any advertisement data received.
    if (PeripheralInfo->DkAdvertisementData.AdDataLen)
    {
        dxTime_t CurrentSystemTime;
        CurrentSystemTime = dxTimeGetMS();

        JobManagerPeripheral_t* pPeripheralDataStatus = &PeripheralsDataStatus[PeripheralInfo->DevHandlerID];

        //Since we most likely to have security mask in the advertisement data, we will need to skip those for the CRC calculation for
        //data change detection. Otherwise we will ALWAYS end up with data update.
        int16_t IndexOfSecurityMask = DkPeripheral_GetInfoContainerIndexOfDataTypeIDExt(DK_DATA_TYPE_SECURITY_MASK,
                                                                                        PeripheralInfo->DkAdvertisementData.AdDataLen,
                                                                                        PeripheralInfo->DkAdvertisementData.AdData);

        dxBOOL	ValidAdvDataPayload = dxFALSE;
        int16_t DataLenLeft = (int16_t)PeripheralInfo->DkAdvertisementData.AdDataLen;
        uint8_t* pData = PeripheralInfo->DkAdvertisementData.AdData;
        if (IndexOfSecurityMask >= 0)
        {
            //NOTE: If security mask is not at index 0 then there might be mis-judgement of payoad change detection
            if (IndexOfSecurityMask != 0)
            {
                G_SYS_DBG_LOG_V( "WARNING: SecurityMask is not at index 0\r\n" );
            }
            else
            {

                uint8_t SecurityMaskDataLen = DKPeripheral_GetDataTypeLenForDataTypeID(DK_DATA_TYPE_SECURITY_MASK);

                //Skip the security mask type and data byte
                pData += IndexOfSecurityMask + SecurityMaskDataLen + 1; //+1 is the SecurityMask DataType (1 byte)
                DataLenLeft -=  IndexOfSecurityMask + SecurityMaskDataLen + 1;
                if (DataLenLeft < 0)
                {
                    DataLenLeft = 0;
                    G_SYS_DBG_LOG_V( "FATAL: AdvData Len to calc CRC is less than 0\r\n" );
                }
                else
                {
                    ValidAdvDataPayload = dxTRUE;
                }
            }
        }
        else
        {
            G_SYS_DBG_LOG_V( "WARNING: SecurityMask NOT found in AdvData!\r\n" );
        }

        //Only check for update is we have valid advertisement data payload
        if (ValidAdvDataPayload == dxTRUE)
        {
            uint16_t CurrentDataCrc = 0;
            uint16_t TotalValidDataLen = DataLenLeft;
            int16_t i = 0;
            for (i = (DataLenLeft - 1); i >= 0; i--)
            {
                if (pData[i] == 0)
                {
                    TotalValidDataLen--;
                    if (TotalValidDataLen == 0)
                        break;
                }
                else
                {
                    break;
                }
            }

            //G_SYS_DBG_LOG_V1( "TotalValidDataLen for CRC check is %d\r\n",  TotalValidDataLen);

            if (TotalValidDataLen)
            {
                CurrentDataCrc = Crc16(pData, TotalValidDataLen, 0);
            }

            if (ForceUpdate == dxTRUE)
            {
                DataNeedsUpdate = dxTRUE;
            }
            else if (CurrentDataCrc != pPeripheralDataStatus->DataTypePayloadCRC16)
            {
                if (DK_FLAG_IS_UPDATE_PAYLOAD_TO_PERIPHERAL_STATUS_ONLY(PeripheralInfo->DkAdvertisementData.Header.Flag) &&
                    !MobileClientIsListening)
                {
                    //If the advertisement data is status update only and mobile client is not listening then we ignore the update to save
                    //server access
                    DataNeedsUpdate = dxFALSE;

                    //We clear the Payload CRC so that Gateway will always update the value to server when mobile device is listening. This will ensure
                    //the payload is always up to date even if the previous saved state is same as current one (state may have changed in between the time)
                    pPeripheralDataStatus->DataTypePayloadCRC16 = 0;
                }
                else
                {
                    DataNeedsUpdate = dxTRUE;
                }
            }

            if (bServerIsReady == dxTRUE)
            {
                // Execute force update when server connect back
                if (pPeripheralDataStatus->bForceUpdateData == dxTRUE)
                {
                    DataNeedsUpdate = dxTRUE;
                    pPeripheralDataStatus->bForceUpdateData = dxFALSE;
                }
            }
            else
            {
                // Mark force update when server connect back
                // Data would need to be updated server when logged back
                if (DataNeedsUpdate == dxTRUE)
                {
                    pPeripheralDataStatus->bForceUpdateData = dxTRUE;
                }
            }

            JobManagerJobQueue_t* JobManagerJobMsg = NULL;

            if (DataNeedsUpdate)
            {
                G_SYS_DBG_LOG_V1( "PeripheralPayloadUpdate ProductID=%d\r\n",PeripheralInfo->DkAdvertisementData.Header.ProductID );

                ObjectDictionary_Lock ();
                uint32_t objectId = ObjectDictionary_FindPeripheralObjectIdByMac ((const uint8_t*) PeripheralInfo->DevAddress.Addr, B_ADDR_LEN_8);
                ObjectDictionary_Unlock ();

                // If objectId== 0, means not yet get objectId from server, need to keep uploading
                if (objectId != 0) {
                    //Keep the last updated info
                    pPeripheralDataStatus->DataTypePayloadCRC16 = CurrentDataCrc;
                }

                JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
                JobManagerJobMsg->JobType = JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE;
                JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
                JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;
                JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;

                //If this is the first time (eg, first added or during system re-start/startup) we flag this as first time new data update
                if (pPeripheralDataStatus->LastUpdatedPayloadTime == 0)
                {
                    JobManagerJobMsg->JobType = JOB_MANAGER_PERIPHERAL_DATA_NEW_UPDATE_FIRST_TIME;
                }

                pPeripheralDataStatus->LastUpdatedPayloadTime = CurrentSystemTime;
            }
            else
            {
                if ((CurrentSystemTime - pPeripheralDataStatus->LastUpdatedPayloadTime) > 1000)
                {
                    JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
                    JobManagerJobMsg->JobType = JOB_MANAGER_PERIPHERAL_DATA_UPDATE_ALIVE;
                    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
                    JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;
                    JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;

                    pPeripheralDataStatus->LastUpdatedPayloadTime = CurrentSystemTime;
                }

                //Check if this payload status is related to recent JobID, if so and if its tagged then we should remove it
                //because the access did not change the peripheral state (eg, motion detected and powerplug turn on but powerplug is already on).
                char MacAddrDecStr[21] = {0};
                if(_MACAddrToDecimalStr(MacAddrDecStr, sizeof(MacAddrDecStr), PeripheralInfo->DevAddress.Addr, 8))
                {
                    uint64_t MacAddrDec = strtoull( MacAddrDecStr, NULL, 10 );
                    if (MacAddrDec)
                    {
                        //By pulling the JobInfo (if available) we also remove it
                        JobIdInfoPull(MacAddrDec, dxTRUE);
                    }
                }
            }

            if (JobManagerJobMsg)
            {
                uint8_t* JobData = dxMemAlloc( "Job Data Buff", 1, sizeof(DeviceInfoReport_t));
                memcpy(JobData, PeripheralInfo, sizeof(DeviceInfoReport_t));
                JobManagerJobMsg->JobData = JobData;
                JobManagerJobMsg->JobDataLen = sizeof(DeviceInfoReport_t);

                if (JobManagerJobQueue)
                {
                    if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
                    {
                        G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                        dxMemFree(JobManagerJobMsg->JobData);
                        dxMemFree(JobManagerJobMsg);
                        result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
                    }
                    else
                    {
                        if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
                        {
                            G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                            dxMemFree(JobManagerJobMsg->JobData);
                            dxMemFree(JobManagerJobMsg);
                            result = DXJOBMANAGER_INTERNAL_ERROR;
                        }
                    }
                }
            }
        }
    }

    return result;
}


dxJOB_MANAGER_RET_CODE PeripheralStatusUpdateCheckProcess(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber, dxBOOL ForceUpdate)
{
    dxBOOL StatusNeedsUpdate = dxFALSE;
    dxBOOL BatteryLevelNeedsUpdate = dxFALSE;
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    JobManagerPeripheral_t* pPeripheralDataStatus = &PeripheralsDataStatus[PeripheralInfo->DevHandlerID];

    /* Check the signal strength */
    ePeripheralSignalStrength CurrentSignalStrength = SIGNAL_STRENGTH_NONE;
    CONVERT_SIGNAL_STREGTH_ABS(PeripheralInfo->rssi,CurrentSignalStrength)
    if (CurrentSignalStrength != pPeripheralDataStatus->LastUpdatedSignalStrength)
    {
        pPeripheralDataStatus->SignalStrengthConsDiffCount++;

        if (CurrentSignalStrength == SIGNAL_STRENGTH_NONE)
        {
            G_SYS_DBG_LOG_V( "PERIPHERAL STATUS SIGNAL_STRENGTH_NONE \r\n" );
            //Report right away if signal strength is from valid to NONE
            StatusNeedsUpdate = dxTRUE;
        }
        else if (pPeripheralDataStatus->LastUpdatedSignalStrength == SIGNAL_STRENGTH_NONE)
        {
            G_SYS_DBG_LOG_V( "PERIPHERAL STATUS SIGNAL_STRENGTH from NONE to ACTIVE \r\n" );
            StatusNeedsUpdate = dxTRUE;
        }
        else if (pPeripheralDataStatus->SignalStrengthConsDiffCount > 20)
        {
            G_SYS_DBG_LOG_V( "PERIPHERAL STATUS SIGNAL CHANGE UPDATE \r\n" );
            //Let's update the signal strength if the strength is N time consecutive different than the last updated value
            StatusNeedsUpdate = dxTRUE;
        }
    }
    else //The signal strength is same so we set the diff count to zero again.
    {
        pPeripheralDataStatus->SignalStrengthConsDiffCount = 0;
    }

    /* Check the battery */
    if (PeripheralInfo->DkAdvertisementData.BatteryInfo)
    {
        if (PeripheralInfo->DkAdvertisementData.BatteryInfo < LOW_BATTERY_LESS_THAN_WARNING_VALUE)
        {
            if (pPeripheralDataStatus->LastUpdatedBatteryInfo >= LOW_BATTERY_LESS_THAN_WARNING_VALUE)
            {
                dxTime_t CurrentTime = dxTimeGetMS();
                if ((CurrentTime - pPeripheralDataStatus->BatteryLevelNormalConditionSince) > (45*MINUTES))
                {
                    BatteryLevelNeedsUpdate = dxTRUE;
                    StatusNeedsUpdate = dxTRUE;
                    G_SYS_DBG_LOG_V( "PERIPHERAL STATUS BATTERY LEVEL LOW DETECTED\r\n" );
                }
            }
        }
        else
        {
            pPeripheralDataStatus->BatteryLevelNormalConditionSince = dxTimeGetMS();
            int16_t BattDifference = PeripheralInfo->DkAdvertisementData.BatteryInfo - pPeripheralDataStatus->LastUpdatedBatteryInfo;
            if ((BattDifference > JOB_PERIPHERAL_BATTERY_UPDATE_DIFF_THRESHOLD) || (BattDifference < -JOB_PERIPHERAL_BATTERY_UPDATE_DIFF_THRESHOLD))
            {
                BatteryLevelNeedsUpdate = dxTRUE;
                StatusNeedsUpdate = dxTRUE;
            }
        }
    }

    /*Check the status*/
    if (pPeripheralDataStatus->LastKnowStatus != PeripheralInfo->Status.flag)
    {
        StatusNeedsUpdate = dxTRUE;
    }

    //We are going to handle this when server is Ready as peripheral status is currently updated to server.
    //WARNING:  If we also need to update to other places then we will have to do some workaround to make sure
    //          status is updated to server when its back online, otherwise there is a chance the status WILL NOT
    //          update to server and server latest value could be 0 signal strength if that's the last updated value!
    if (bServerIsReady == dxTRUE)
    {
        // Execute force update when server connect back
        if (pPeripheralDataStatus->bForceUpdateStatus == dxTRUE)
        {
            StatusNeedsUpdate = dxTRUE;
            pPeripheralDataStatus->bForceUpdateStatus = dxFALSE;
        }
    }
    else
    {
        // Mark force update when server connect back
        // Data would need to be updated server when logged back
        if (StatusNeedsUpdate == dxTRUE)
        {
            pPeripheralDataStatus->bForceUpdateStatus = dxTRUE;
        }
    }

    JobManagerJobQueue_t* JobManagerJobMsg = NULL;

    if (StatusNeedsUpdate)
    {
        JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
        JobManagerJobMsg->JobType = JOB_MANAGER_PERIPHERAL_STATUS_NEW_UPDATE;
        JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
        JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;
        JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    }


    if (JobManagerJobMsg)
    {
        uint8_t* JobData = dxMemAlloc( "Job Data Buff", 1, sizeof(DeviceInfoReport_t));
        memcpy(JobData, PeripheralInfo, sizeof(DeviceInfoReport_t));

        if (BatteryLevelNeedsUpdate == dxFALSE)
        {
            DeviceInfoReport_t* pWorkingPeripheralInfo = (DeviceInfoReport_t*)JobData;
            pWorkingPeripheralInfo->DkAdvertisementData.BatteryInfo = pPeripheralDataStatus->LastUpdatedBatteryInfo;
        }

        JobManagerJobMsg->JobData = JobData;
        JobManagerJobMsg->JobDataLen = sizeof(DeviceInfoReport_t);

        if (JobManagerJobQueue)
        {
            if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
            {
                G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
            }
            else
            {
                if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                    dxMemFree(JobManagerJobMsg->JobData);
                    dxMemFree(JobManagerJobMsg);
                    result = DXJOBMANAGER_INTERNAL_ERROR;
                }
            }
        }
    }

    return result;
}

//============================================================================
// Function Definitions
//============================================================================
dxJOB_MANAGER_RET_CODE JobManagerInit(JobManagerEvent_handler_t EventFunction)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;


    if (JobManagerJobQueue == NULL)
    {
        JobManagerJobQueue = dxQueueCreate(MAXIMUM_JOB_MANAGER_JOB_QUEUE_DEPT_ALLOWED, sizeof(JobManagerJobQueue_t*));

        if (JobManagerJobQueue == NULL)
        {
            G_SYS_DBG_LOG_V( "Error creating JobManagerJobQueue \r\n" );
            result = DXJOBMANAGER_INTERNAL_ERROR;
            goto EndInit;
        }
    }

    if (JobAccessResourceProtectionSemaHandle == NULL)
    {
        JobAccessResourceProtectionSemaHandle = dxSemCreate(    1,          // uint32_t uxMaxCount
                                                                1);         // uint32_t uxInitialCount

        if (JobAccessResourceProtectionSemaHandle == NULL)
        {
            result = DXJOBMANAGER_INTERNAL_ERROR;
            goto EndInit;
        }
    }


    if  (dxTaskCreate(  Job_Manager_Process_thread_main,
                        JOB_MANAGER_PROCESS_THREAD_NAME,
                        JOB_MANAGER_STACK_SIZE,
                        NULL,
                        JOB_MANAGER_PROCESS_THREAD_PRIORITY,
                        &JobMainProcess_thread) != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V( "Error creating JobMainProcess_thread\r\n");
        result = DXJOBMANAGER_INTERNAL_ERROR;
        goto EndInit;
    }

    GSysDebugger_RegisterForThreadDiagnostic(&JobMainProcess_thread);

    if (EventFunction)
    {
        if (RegisteredEventFunction == NULL)	//First time, we create worker thread first
        {
            RegisteredEventFunction = EventFunction;

            if (dxWorkerTaskCreate("JOBMGR W", &EventWorkerThread, JOB_MANAGER_DEFAULT_WORKER_PRIORITY, JOB_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE, JOB_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V(  "JobManagerInit - Failed to create worker thread\r\n" );
                RegisteredEventFunction = NULL;
                result = DXJOBMANAGER_INTERNAL_ERROR;
                goto EndInit;
            }
            else
            {
                GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "JobManager Event Thread");
            }
        }

        uint16_t i = 0;
        for (i = 0; i <MAXIMUM_DEVICE_KEEPER_ALLOWED; i++)
        {
            JobManagerPeripheral_t* pPeripheralDataStatus = &PeripheralsDataStatus[i];
            memset(pPeripheralDataStatus, 0, sizeof(JobManagerPeripheral_t));
            pPeripheralDataStatus->LastUpdatedSignalStrength = SIGNAL_STRENGTH_UNKNOWN;
        }

        for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
        {
            JobSchedule_t* pJobSchedule = &JobScheduledList[i];
            pJobSchedule->SlotAvailable = dxTRUE;
        }

        for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
        {
            AdvanceJob_t* pAdvanceJob = &AdvanceJobList[i];
            pAdvanceJob->SlotAvailable = dxTRUE;
        }
    }
    else
    {
        result = DXJOBMANAGER_INVALID_PARAMETER;
        goto EndInit;
    }

EndInit:
    return result;
}

JobEventObjHeader_t* JobManagerGetEventObjHeader(void* ReportedEventArgObj)
{
    JobEventObjHeader_t* EventHeader = NULL;

    if (ReportedEventArgObj)
    {
        JobManagerEventReportObj_t* EventReportObj = (JobManagerEventReportObj_t*)ReportedEventArgObj;
        EventHeader = &EventReportObj->Header;
    }

    return EventHeader;
}


uint8_t* JobManagerGetEventObjPayloadData(void* ReportedEventArgObj)
{
    uint8_t* EventPayload = NULL;

    if (ReportedEventArgObj)
    {
        JobManagerEventReportObj_t* EventReportObj = (JobManagerEventReportObj_t*)ReportedEventArgObj;
        EventPayload = EventReportObj->pData;
    }

    return EventPayload;
}

dxJOB_MANAGER_RET_CODE JobManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    if (ReportedEventArgObj)
    {
        JobManagerEventReportObj_t* EventReportObj = (JobManagerEventReportObj_t*)ReportedEventArgObj;

        if (EventReportObj->pData)
            dxMemFree(EventReportObj->pData);

        dxMemFree(ReportedEventArgObj);
    }

    return result;
}

uint32_t JobManagerGetTotalScheduleJobDataBackupSize(void)
{
    uint32_t RequiredBackupSizeInByte = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[i];

        //Look for slot that is in use
        if (pJobSchedule->SlotAvailable == dxFALSE)
        {
            RequiredBackupSizeInByte += pJobSchedule->JobDataLen + sizeof(JobScheduleBackup_t);
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return RequiredBackupSizeInByte;
}

uint32_t JobManagerGetTotalAdvanceJobDataBackupSize(dxBOOL bExcludeInternalAdvJob )
{
    uint32_t RequiredBackupSizeInByte = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        AdvanceJob_t* pJobdvance = &AdvanceJobList[i];

        //Look for slot that is in use
        if (pJobdvance->SlotAvailable == dxFALSE)
        {
            RequiredBackupSizeInByte += pJobdvance->JobConditionDataLen + pJobdvance->JobExecutionDataLen + sizeof(AdvanceJobBackup_t);
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return RequiredBackupSizeInByte;
}

uint32_t JobManagerGetBackupDataOfAllScheduleJob(void* pBackupDataBuff, uint32_t SizeOfBackupDatabuff)
{
    uint32_t SizeOfBackupData = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    uint8_t* pBackupData = (uint8_t*)pBackupDataBuff;
    uint32_t BackupItemDataSize = 0;
    int64_t AvailableBuffSize = SizeOfBackupDatabuff;
    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_JOB_SCHEDULED_LIST_ALLOWED; i++)
    {
        JobSchedule_t* pJobSchedule = &JobScheduledList[i];

        //Look for slot that is in use
        if (pJobSchedule->SlotAvailable == dxFALSE)
        {
            JobScheduleBackup_t* pWorkingBackupData =  (JobScheduleBackup_t*) pBackupData;
            BackupItemDataSize = sizeof(JobScheduleBackupHeader_t) + pJobSchedule->JobDataLen;

            if (AvailableBuffSize >= BackupItemDataSize)
            {
                pWorkingBackupData->Header.JobDataLen = pJobSchedule->JobDataLen;
                memcpy(pWorkingBackupData->JobData, pJobSchedule->JobData, pWorkingBackupData->Header.JobDataLen);

                pBackupData += BackupItemDataSize;
                AvailableBuffSize -= BackupItemDataSize;
                SizeOfBackupData += BackupItemDataSize;
            }
            else
            {
                SizeOfBackupData = 0;
                break;
            }
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);

    return SizeOfBackupData;
}

uint16_t JobManagerRestoreDataOfAllScheduleJob(void* pRestoredDataBuff, uint32_t SizeOfRestoredDatabuff)
{

    uint16_t TotalRestoredJob = 0;
    uint32_t ProcessedRestoredDataSize = 0;
    uint8_t* pRestoreData = (uint8_t*)pRestoredDataBuff;
    while (SizeOfRestoredDatabuff > ProcessedRestoredDataSize)
    {
        pRestoreData = (uint8_t*)pRestoredDataBuff + ProcessedRestoredDataSize;

        JobScheduleBackup_t* pWorkingBackupData =  (JobScheduleBackup_t*) pRestoreData;

        if (pWorkingBackupData->Header.JobDataLen < (SizeOfRestoredDatabuff - ProcessedRestoredDataSize))
        {
            NewJobScheduledConditionProcess(pWorkingBackupData->JobData, pWorkingBackupData->Header.JobDataLen, 0, 0);

            ProcessedRestoredDataSize += sizeof(JobScheduleBackupHeader_t) + pWorkingBackupData->Header.JobDataLen;

            TotalRestoredJob++;
        }
        else //This should not happen
        {
            G_SYS_DBG_LOG_V("WARNING: JobManagerRestoreDataOfAllScheduleJob imcomplete!\r\n");
            break;
        }
    }

    return TotalRestoredJob;
}

uint32_t JobManagerGetBackupDataOfAllAdvanceJob(void* pBackupDataBuff, uint32_t SizeOfBackupDatabuff, dxBOOL bExcludeInternalAdvJob)
{
    uint32_t SizeOfBackupData = 0;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(JobAccessResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);


    uint8_t* pBackupData = (uint8_t*)pBackupDataBuff;
    uint32_t BackupItemDataSize = 0;
    int64_t AvailableBuffSize = SizeOfBackupDatabuff;
    uint8_t i = 0;
    for (i = 0; i < MAXIMUM_ADVANCE_JOB_LIST_ALLOWED; i++)
    {
        AdvanceJob_t* pJobdvance = &AdvanceJobList[i];

        //Look for slot that is in use
        if (pJobdvance->SlotAvailable == dxFALSE)
        {
            if ((bExcludeInternalAdvJob == dxTRUE) && (pJobdvance->AdvanceJobID <= MAX_ADV_JOB_RESERVED_ID/*ADV_JOB_RESERVED_ID_MAX*/))
            {
                continue;
            }
            else
            {
                AdvanceJobBackup_t* pWorkingBackupData =  (AdvanceJobBackup_t*) pBackupData;
                BackupItemDataSize = sizeof(AdvanceJobBackupHeader_t) + pJobdvance->JobConditionDataLen + pJobdvance->JobExecutionDataLen;

                if (AvailableBuffSize >= BackupItemDataSize)
                {
                    pWorkingBackupData->Header.AdvanceJobID = pJobdvance->AdvanceJobID;
                    pWorkingBackupData->Header.JobConditionDataLen = pJobdvance->JobConditionDataLen;
                    pWorkingBackupData->Header.JobExecutionDataLen = pJobdvance->JobExecutionDataLen;

                    if (pWorkingBackupData->Header.JobConditionDataLen)
                    {
                        memcpy(pWorkingBackupData->JobMergedData, pJobdvance->JobConditionData, pWorkingBackupData->Header.JobConditionDataLen);
                    }

                    if (pWorkingBackupData->Header.JobExecutionDataLen)
                    {
                        memcpy(pWorkingBackupData->JobMergedData + pWorkingBackupData->Header.JobConditionDataLen, pJobdvance->JobExecutionData, pWorkingBackupData->Header.JobExecutionDataLen);
                    }

                    pBackupData += BackupItemDataSize;
                    AvailableBuffSize -= BackupItemDataSize;
                    SizeOfBackupData += BackupItemDataSize;
                }
                else
                {
                    SizeOfBackupData = 0;
                    break;
                }

            }
            //RequiredBackupSizeInByte += pJobdvance->JobConditionDataLen + pJobdvance->JobExecutionDataLen + sizeof(AdvanceJobBackup_t);
        }
    }

    // Release control
    dxSemGive(JobAccessResourceProtectionSemaHandle);


    return SizeOfBackupData;
}


uint16_t JobManagerRestoreDataOfAllAdvanceJob(void* pRestoredDataBuff, uint32_t SizeOfRestoredDatabuff)
{
    uint16_t TotalAdvJobRestored = 0;

    uint32_t ProcessedRestoredDataSize = 0;
    uint8_t* pRestoreData = (uint8_t*)pRestoredDataBuff;
    while (SizeOfRestoredDatabuff > ProcessedRestoredDataSize)
    {
        pRestoreData = (uint8_t*)pRestoredDataBuff + ProcessedRestoredDataSize;

        AdvanceJobBackup_t* pWorkingBackupData =  (AdvanceJobBackup_t*) pRestoreData;

        if ((pWorkingBackupData->Header.JobConditionDataLen + pWorkingBackupData->Header.JobExecutionDataLen)
            < (SizeOfRestoredDatabuff - ProcessedRestoredDataSize))
        {

            if (pWorkingBackupData->Header.JobConditionDataLen)
            {
                NewAdvanceJobConditionProcess(pWorkingBackupData->JobMergedData, pWorkingBackupData->Header.JobConditionDataLen, pWorkingBackupData->Header.AdvanceJobID, 0, 0);
            }

            if (pWorkingBackupData->Header.JobExecutionDataLen)
            {
                NewAdvanceJobExecutionProcess(  pWorkingBackupData->JobMergedData + pWorkingBackupData->Header.JobConditionDataLen,
                                                pWorkingBackupData->Header.JobExecutionDataLen, pWorkingBackupData->Header.AdvanceJobID, 0, 0);
            }

            ProcessedRestoredDataSize += sizeof(AdvanceJobBackupHeader_t) + pWorkingBackupData->Header.JobConditionDataLen + pWorkingBackupData->Header.JobExecutionDataLen;

            TotalAdvJobRestored++;
        }
        else
        {
            G_SYS_DBG_LOG_V("WARNING: JobManagerRestoreDataOfAllAdvanceJob imcomplete!\r\n");
            break;
        }
    }

    return TotalAdvJobRestored;
}

dxJOB_MANAGER_RET_CODE JobManagerNewJobWithTime_Task(void* NewJob, dxTime_t TimeStamp, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    if (!JobLen || (NewJob == NULL))
    {
        return DXJOBMANAGER_INVALID_PARAMETER;
    }

    JobManagerJobQueue_t* JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
    JobManagerJobMsg->JobType = JOB_MANAGER_NEW_JOB;
    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
    JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;
    JobManagerJobMsg->JobTimeStamp = TimeStamp;

    uint8_t* JobData = dxMemAlloc( "Job Data Buff", 1, JobLen);
    memcpy( JobData, NewJob, JobLen );

    JobManagerJobMsg->JobData = JobData;
    JobManagerJobMsg->JobDataLen = JobLen;

    if (JobManagerJobQueue)
    {
        if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            dxMemFree(JobManagerJobMsg->JobData);
            dxMemFree(JobManagerJobMsg);
            result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_INTERNAL_ERROR;
            }
        }
    }

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerNewJob_Task(void* NewJob, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxTime_t TimeStamp = (dxTime_t)dxGetUtcInSec();
    return JobManagerNewJobWithTime_Task(NewJob, TimeStamp, JobLen, SourceOfOrigin, JobIdentificationNumber);
}



dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJob_Task(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    JobManagerJobQueue_t* JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
    JobManagerJobMsg->JobType = JOB_MANAGER_CLEAN_ALL_JOB;
    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
    JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;

    JobManagerJobMsg->JobData = NULL;
    JobManagerJobMsg->JobDataLen = 0;

    if (JobManagerJobQueue)
    {
        if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            if (JobManagerJobMsg->JobData){dxMemFree(JobManagerJobMsg->JobData);}
            dxMemFree(JobManagerJobMsg);
            result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_INTERNAL_ERROR;
            }
        }
    }

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllScheduleJob () {
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllScheduleJobs (dxFALSE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllAdvanceJob () {
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllAdvanceJobs (dxFALSE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJob(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllScheduleJobs (dxFALSE);
    _RemoveAllAdvanceJobs (dxFALSE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllScheduleJobExceptReserved () {
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllScheduleJobs (dxTRUE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllAdvanceJobExceptReserved () {
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllAdvanceJobs (dxTRUE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJobExceptReserved(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    _RemoveAllScheduleJobs (dxTRUE);
    _RemoveAllAdvanceJobs (dxTRUE);

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerRemoveJob(void* Job, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    //TODO: Find it from watch list and remove it, send the Job removed event back to application with  JobIdentificationNumber

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerNewAdvanceJobWithTime_Task(void* NewJob, dxTime_t TimeStamp, uint8_t JobIsSinglePackExecution, uint16_t JobLen, uint32_t AdvjobID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    if (!JobLen || (NewJob == NULL) || (AdvjobID == 0))
    {
        return DXJOBMANAGER_INVALID_PARAMETER;
    }

    G_SYS_DBG_LOG_V1( "JobManagerNewAdvanceJob_Task AdObjID = %d \r\n",AdvjobID );

    JobManagerJobQueue_t* JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
    JobManagerJobMsg->JobType = (JobIsSinglePackExecution) ? JOB_MANAGER_ADV_JOB_SINGLE_PACK_EXEC:JOB_MANAGER_NEW_ADV_JOB;
    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
    JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;
    JobManagerJobMsg->JobAdvId = AdvjobID;
    JobManagerJobMsg->JobTimeStamp = TimeStamp;

    uint8_t* JobData = dxMemAlloc( "Job Data Buff", 1, JobLen);
    memcpy( JobData, NewJob, JobLen );

    JobManagerJobMsg->JobData = JobData;
    JobManagerJobMsg->JobDataLen = JobLen;

    if (JobManagerJobQueue)
    {

        if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            if (JobManagerJobMsg->JobData){dxMemFree(JobManagerJobMsg->JobData);}
            dxMemFree(JobManagerJobMsg);
            result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_INTERNAL_ERROR;
            }
        }
    }

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerNewAdvanceJob_Task(void* NewJob, uint8_t JobIsSinglePackExecution, uint16_t JobLen, uint32_t AdvjobID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxTime_t TimeStamp = (dxTime_t)dxGetUtcInSec();
    return  JobManagerNewAdvanceJobWithTime_Task(NewJob, TimeStamp, JobIsSinglePackExecution, JobLen, AdvjobID, SourceOfOrigin, JobIdentificationNumber);
}

extern dxJOB_MANAGER_RET_CODE JobManagerRemovePeripheralSpecificJobs_Task(uint32_t PeripheralID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    JobManagerJobQueue_t* JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
    JobManagerJobMsg->JobType = JOB_MANAGER_REMOVE_PERIPHERAL_ALL_SPECIFIC_SCHEDULED_JOB;
    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
    JobManagerJobMsg->JobSourceOfOrigin = SourceOfOrigin;
    JobManagerJobMsg->JobIdentificationNumber = JobIdentificationNumber;

    JobManagerJobMsg->JobDataLen = sizeof(PeripheralID);

    uint8_t* JobData = dxMemAlloc( "Job Data Buff", 1, JobManagerJobMsg->JobDataLen);
    memcpy( JobData, &PeripheralID, JobManagerJobMsg->JobDataLen );
    JobManagerJobMsg->JobData = JobData;

    if (JobManagerJobQueue)
    {

        if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
        {
            G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
            if (JobManagerJobMsg->JobData)
            {
                dxMemFree(JobManagerJobMsg->JobData);
            }
            dxMemFree(JobManagerJobMsg);
            result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
        }
        else
        {
            if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_INTERNAL_ERROR;
            }
        }
    }

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerPeripheralDataUpdateExt_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber, dxBOOL ForceUpdate )
{

    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    result = PeripheralPayloadUpdateCheckProcess(PeripheralInfo, SourceOfOrigin, JobIdentificationNumber, ForceUpdate);

    if (result == DXJOBMANAGER_SUCCESS)
    {
        PeripheralStatusUpdateCheckProcess(PeripheralInfo, SourceOfOrigin, JobIdentificationNumber, ForceUpdate);
    }

    return result;
}

dxJOB_MANAGER_RET_CODE JobManagerPeripheralDataUpdate_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    return JobManagerPeripheralDataUpdateExt_Task(PeripheralInfo, SourceOfOrigin,  JobIdentificationNumber, dxFALSE );
}

#if DEVICE_IS_HYBRID
dxJOB_MANAGER_RET_CODE JobManagerHybridPeripheralDataUpdate_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber)
{
    PeripheralInfo->DevHandlerID = HYBRID_PERIPHERAL_DATA_HANDLER_ID;
    return JobManagerPeripheralDataUpdateExt_Task(PeripheralInfo, SourceOfOrigin,  JobIdentificationNumber, dxFALSE );
}
#endif //#if DEVICE_IS_HYBRID

dxJOB_MANAGER_RET_CODE JobManagerPeripheralJobCheck_Task(DeviceInfoJobCheck_t* PeripheralInfo )
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    if (PeripheralInfo == NULL)
    {
        return DXJOBMANAGER_INVALID_PARAMETER;
    }

    JobManagerJobQueue_t* JobManagerJobMsg = NULL;
    JobManagerJobMsg = dxMemAlloc( "Job Manager Job Msg Buff", 1, sizeof(JobManagerJobQueue_t));
    JobManagerJobMsg->JobType = JOB_MANAGER_PERIPHERAL_JOB_CHECK;
    JobManagerJobMsg->JobTimeOutTime = NO_TIME_OUT_TIME;
    JobManagerJobMsg->JobIdentificationNumber = 0;
    JobManagerJobMsg->JobSourceOfOrigin = SOURCE_OF_ORIGIN_DEFAULT;

    if (JobManagerJobMsg)
    {
        DeviceInfoReport_t* JobData = dxMemAlloc( "Job Data Buff", 1, sizeof(DeviceInfoReport_t));
        memcpy(&JobData->DevAddress , &PeripheralInfo->DevAddress, sizeof(DevAddress_t));
        memcpy(&JobData->DkAdvertisementData , &PeripheralInfo->DkAdvertisementData, sizeof(DkAdvData_t));

        JobManagerJobMsg->JobData = (uint8_t*)JobData;
        JobManagerJobMsg->JobDataLen = sizeof(DeviceInfoReport_t);

        if (JobManagerJobQueue)
        {
            if (dxQueueIsFull(JobManagerJobQueue) == dxTRUE)
            {
                G_SYS_DBG_LOG_V( "Line(%d) Error pushing message to queue (Queue is FULL) \r\n", __LINE__ );
                dxMemFree(JobManagerJobMsg->JobData);
                dxMemFree(JobManagerJobMsg);
                result = DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE;
            }
            else
            {
                if (dxQueueSend(JobManagerJobQueue,  &JobManagerJobMsg, 1*(SECONDS)) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V( "Error pushing message to queue (Could be due to timeout) \r\n" );
                    dxMemFree(JobManagerJobMsg->JobData);
                    dxMemFree(JobManagerJobMsg);
                    result = DXJOBMANAGER_INTERNAL_ERROR;
                }
            }
        }
    }
    else
    {
        result = DXJOBMANAGER_INTERNAL_ERROR;
    }

    return result;
}


dxJOB_MANAGER_RET_CODE JobManagerPeripheralUnpaired(DeviceInfoReport_t* UnpairedPeripheralInfo)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    if ((UnpairedPeripheralInfo->DevHandlerID >= 0) && (UnpairedPeripheralInfo->DevHandlerID < MAXIMUM_DEVICE_KEEPER_ALLOWED))
    {
        JobManagerPeripheral_t* pPeripheralDataStatus = &PeripheralsDataStatus[UnpairedPeripheralInfo->DevHandlerID];

        memset(pPeripheralDataStatus, 0, sizeof(JobManagerPeripheral_t));
        pPeripheralDataStatus->LastUpdatedSignalStrength = SIGNAL_STRENGTH_UNKNOWN;
    }

    return result;
}

extern dxJOB_MANAGER_RET_CODE JobManagerMobileClientSessionTimeExtend(uint16_t SessionAliveTime)
{
    dxJOB_MANAGER_RET_CODE result = DXJOBMANAGER_SUCCESS;

    dxTime_t CurrentSystemTime;
    CurrentSystemTime = dxTimeGetMS();

    MobileClientSessionTimeOutTime = CurrentSystemTime + (SessionAliveTime*SECONDS);
    MobileClientIsListening = dxTRUE;

    return result;
}

extern dxBOOL JobManagerTagJobIdInfoByMacAddrDec(uint64_t MacAddrDec)
{
    return JobIdInfoTag(MacAddrDec);
}

extern JobIdInfo_t JobManagerCheckJobIdInfoByMacAddrDec(uint64_t MacAddrDec, dxBOOL TaggedOnly)
{
    return JobIdInfoPull(MacAddrDec, TaggedOnly);
}

extern dxJOB_MANAGER_RET_CODE JobManagerSetJobSuperiorOnly(dxBOOL bJobSuperiorOnly)
{
    bJobSuperiorOnlyEnabled = bJobSuperiorOnly;

    return DXJOBMANAGER_SUCCESS;
}

extern dxJOB_MANAGER_RET_CODE JobManagerNotifyServerReadyState(dxBOOL ServerIsReady)
{
    bServerIsReady = ServerIsReady;

    return DXJOBMANAGER_SUCCESS;
}
