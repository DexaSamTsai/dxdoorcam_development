//============================================================================
// File: JobManager
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/10
//     1) Description: Initial
//
//============================================================================
#ifndef _DXJOBMANAGER_H
#define _DXJOBMANAGER_H

#include "dk_Job.h"
#include "dk_Peripheral.h"
#include "Utils.h"
#include "dxSysDebugger.h"

//============================================================================
// Defines
//============================================================================

typedef enum
{

    DXJOBMANAGER_SUCCESS                    = 0,

    DXJOBMANAGER_INTERNAL_ERROR             = -21,
    DXJOBMANAGER_TOO_MANY_TASK_IN_QUEUE     = -22,
    DXJOBMANAGER_INVALID_PARAMETER          = -23,

} dxJOB_MANAGER_RET_CODE;

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enumerations
//============================================================================

typedef enum
{

    JOB_MANAGER_EVENT_UNKNOWN                       = 0,
    JOB_MANAGER_EVENT_PERIPHERAL_ACCESS,                        /**< The Payload is BleStandardJobExecHeader_t */
    JOB_MANAGER_EVENT_HYBRID_PERIPHERAL_ACCESS,                 /**< The Payload is HybridPeripheralJobExecHeader_t */
    JOB_MANAGER_EVENT_PERIPHERAL_STATUS_NEEDS_UPDATE,           /**< The Payload is DeviceInfoReport_t */
    JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE,             /**< The Payload is DeviceInfoReport_t */
    JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_PERIPHERAL_STATUS_ONLY,       /**< The Payload is DeviceInfoReport_t */
    JOB_MANAGER_EVENT_PERIPHERAL_DATA_NEEDS_UPDATE_TO_HISTORIC_TABLES_ONLY,         /**< The Payload is DeviceInfoReport_t */
    JOB_MANAGER_EVENT_PERIPHERAL_DATA_KEEP_ALIVE,                                   /**< The Payload is DeviceInfoReport_t, this is sent for each peripheral data about a second even if the data integrity of the peripheral remain the same  */
    JOB_MANAGER_EVENT_NOTIFICATION,                                                 /**< The Payload is according to Job's format spec (see Notification command ID's payload) */
    JOB_MANAGER_EVENT_JOB_REMOVED,
    JOB_MANAGER_EVENT_JOB_REMOVED_ALL,
    JOB_MANAGER_EVENT_GATEWAY_IDENTIFY,                                             /**< No Payload */
    JOB_MANAGER_EVENT_SCAN_FOR_PERIPHERAL,                                          /**< The Payload is uint16_t which is the scan discovery timeout in seconds */
    JOB_MANAGER_EVENT_PAIR_PERIPHERAL,                                              /**< The Payload is stPairingInfo_t */
    JOB_MANAGER_EVENT_UNPAIR_PERIPHERAL,                                            /**< The Payload is DevAddress_t */
    JOB_MANAGER_EVENT_SECURITY_SET_RENEW_PERIPHERAL,                                /**< The Payload is stRenewSecurityInfo_t */
    JOB_MANAGER_EVENT_SOFT_RESTART,                                                 /**< No Payload */
    JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_SUCCESS,                         /**< No Payload */
    JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_DELETE_FAILED,                          /**< No Payload */
    JOB_MANAGER_EVENT_ADV_JOB_STATUS_UPDATE,                                        /**< The Payload is AdvJobStatusSrv_t from DKServerManager.h */
    JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_SUCCESS,                                   /**< No Payload */
    JOB_MANAGER_EVENT_ADVANCE_JOB_DELETE_FAILED,                                    /**< No Payload */
    JOB_MANAGER_EVENT_LONGTERM_SCHEDULE_JOB_UPDATED,                                /**< No Payload */
    JOB_MANAGER_EVENT_ADVANCE_JOB_UPDATED,                                          /**< No Payload */
    JOB_MANAGER_EVENT_UPDATE_LOCAL_PERIPHERAL_NAME,                                 /**< The Payload is PeripheralObjectID(uint32_t) + PeripheralNewName */
    JOB_MANAGER_EVENT_UPDATE_PERIPHERAL_RTC,                                        /**< The Payload is stUpdatePeripheralRtcInfo_t */

} JobManager_EventType_t;

// Move to Alarm_Control.c
//// AdvObjID <= 0x0FFF is the reserved area for customized advanced job managed by Gateway itself
//typedef enum
//{
//    ADV_JOB_RESERVED_ID_DO_NOT_USE              = 0x0000,
//    ADV_JOB_RESERVED_ID_JOB_DBRIDGE_EXECUTION   = 0x0001,   //job bridge execution reserved ID
//    ADV_JOB_RESERVED_ID_ALARM_NORMAL            = 0x0010,   //condition for Zone Normal
//    ADV_JOB_RESERVED_ID_ALARM_HOME              = 0x0011,   //condition for Zone Home
//    ADV_JOB_RESERVED_ID_ALARM_24H               = 0x0012,   //condition for Zone 24H
//    ADV_JOB_RESERVED_ID_ALARM_ENABLE            = 0x0013,   //one time execution for enable execution
//    ADV_JOB_RESERVED_ID_ALARM_DISABLE           = 0x0014,   //one time execution for disable execution
//    ADV_JOB_RESERVED_ID_ALARM_NOTIFY            = 0x0015,   //one time execution for alert notification
//    ADV_JOB_RESERVED_ID_ALARM_SABOTAGE          = 0x0016,   //condition for Zone Sabotage
//    ADV_JOB_RESERVED_ID_MAX                     = 0x0FFF
//} AdvJob_ReservedID_t;

//============================================================================
// Type Definitions
//============================================================================

typedef struct
{
    JobManager_EventType_t  EventType;
    uint16_t                SourceOfOrigin;
    uint64_t                IdentificationNumber;
    dxTime_t                TimeStamp; //This is the time in UTC+0 when it was originally created
    dxTime_t                Timeout;
} JobEventObjHeader_t;

typedef struct
{
    uint64_t        JobID;
    uint32_t        AdvObjID;
    uint32_t        UserObjID;
    dxTime_t        TimeRecorded;
    dxBOOL          Tagged;
} JobIdInfo_t;

typedef void (*JobManagerEvent_handler_t)( void* arg );

//============================================================================
// Structures
//============================================================================

//============================================================================
// Function Declarations
//============================================================================

/**
 * @brief Initialize Job Manager, all task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  NOTE1: Use JobGetEventObjHeader(arg) to obtain the JobEventReportObj_t.
 *  NOTE2: According to ReportEvent handle necessary action (see JobManager_EventType_t)
 *  NOTE3: Use JobManagerGetEventObjPayloadData(arg) to obtain the pointer to the payload data if needed
 *  NOTE4: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE5: At the end of EventFunction call JobManagerClearEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[IN] EventFunction - The EventFunction that will be called when reporting activities
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerInit(JobManagerEvent_handler_t EventFunction);


/**
 * @brief Tell JobManager that UTC time has been updated. This is usually called when time is updated..
 *  WARNING: Job Manager will NOT automaticcaly increment the time, application should call this function to update the time frequently (say every 2 to 10 seconds)
 *
 * @param[IN] UTCTimeToUpdate - Latest UTC time update
 * @return dxJOB_MANAGER_RET_CODE
 */
//extern dxJOB_MANAGER_RET_CODE JobManagerUtcTimeUpdate(DKTime_t* UTCTimeToUpdate);


/**
 * @brief Get the event header from reported event obj arg
 *
 * @param[IN] ReportedEventArgObj - The reported obj argument to obtain the header info
 * @return dxJOB_MANAGER_RET_CODE
 */
extern JobEventObjHeader_t* JobManagerGetEventObjHeader(void* ReportedEventArgObj);

/**
 * @brief Get the event payload data from reported event obj arg
 *
 * @param[IN] ReportedEventArgObj - The reported obj argument to obtain the payload data
 * @return dxJOB_MANAGER_RET_CODE
 */
extern uint8_t* JobManagerGetEventObjPayloadData(void* ReportedEventArgObj);

/**
 * @brief All information of Object given by EventFunction will be cleaned
 *
 * @param[IN] ReportedEventArgObj - The reported obj argument to clean up
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerCleanEventArgumentObj(void* ReportedEventArgObj);

/**
 * @brief Size of Backup Schedule Data - Return the size in byte of all schedue job data. This is used to allocate necessary space to obtain the actual total schedule job data
 *
 * @return     uint32_t size of total shcedule job data in byte
 */
extern uint32_t JobManagerGetTotalScheduleJobDataBackupSize(void);

/**
 * @brief size of Backup Advance Data - Return the size in byte of all advance job data. This is used to allocate necessary space to obtain the actual total advance job data
 *
 * @param[IN] bExcludeInternalAdvJob - Exclude internal advance jobs, this is usually used by HybridPeripheral and the range does not conflict with server adv job object id.
 *
 * @return     uint32_t size of total advance job data in byte
 */
extern uint32_t JobManagerGetTotalAdvanceJobDataBackupSize(dxBOOL bExcludeInternalAdvJob );

/**
 * @brief Obtain the Backup Schedule Data
 *
 * @param[IN\OUT]   pBackupDataBuff - The allocated buffer that is going to store the backup data
 * @param[IN]       SizeOfBackupDatabuff - The size of the allocated buffer
 *
 * @return     uint32_t Size of the backup data in byte
 */
extern uint32_t JobManagerGetBackupDataOfAllScheduleJob(void* pBackupDataBuff, uint32_t SizeOfBackupDatabuff);

/**
 * @brief Obtain the Backup Advance Data
 *
 * @param[IN\OUT]   pBackupDataBuff - The allocated buffer that is going to store the backup data
 * @param[IN]       SizeOfBackupDatabuff - The size of the allocated buffer
 * @param[IN]       bExcludeInternalAdvJob - Exclude internal advance jobs, this is usually used by HybridPeripheral and the range does not
 *                                           conflict with server adv job object id.
 *
 * @return     uint32_t Size of the backup data in byte
 */
extern uint32_t JobManagerGetBackupDataOfAllAdvanceJob(void* pBackupDataBuff, uint32_t SizeOfBackupDatabuff, dxBOOL bExcludeInternalAdvJob);

/**
 * @brief Restore Schedule Data
 *
 * @param[IN\OUT]   pRestoredDataBuff - The buffer that contain the restored data
 * @param[IN]       SizeOfRestoredDatabuff - The size of the pRestoredDataBuff
 *
 * @return     uint16_t returned number of restored schedule job
 */
extern uint16_t JobManagerRestoreDataOfAllScheduleJob(void* pRestoredDataBuff, uint32_t SizeOfRestoredDatabuff);

/**
 * @brief Restore Advacne Data
 *
 * @param[IN\OUT]   pRestoredDataBuff - The buffer that contain the restored data
 * @param[IN]       SizeOfRestoredDatabuff - The size of the pRestoredDataBuff
 *
 * @return     uint16_t returned number of restored Adv job
 */
extern uint16_t JobManagerRestoreDataOfAllAdvanceJob(void* pRestoredDataBuff, uint32_t SizeOfRestoredDatabuff);


/**
 * @brief New Job is available for Job Manager to handle.. This is asynchronous
 *
 * @param[IN]  NewJob           :  The New job object from UDP or Server. The Job Object structure should be according to Job data format specification
 * @param[IN]  JobLen           :  The Len/Size in byte of the New Job Object.
 * @param[IN]  SourceOfOrigin   :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber    :  The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerNewJob_Task(void* NewJob, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief New Job is available for Job Manager to handle.. This is asynchronous
 *
 * @param[IN]  NewJob           :  The New job object from UDP or Server. The Job Object structure should be according to Job data format specification
 * @param[IN]  TimeStamp        :  The timestamp (UTC+0) of this Job when it was originally created.
 * @param[IN]  JobLen           :  The Len/Size in byte of the New Job Object.
 * @param[IN]  SourceOfOrigin   :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber    :  The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerNewJobWithTime_Task(void* NewJob, dxTime_t TimeStamp, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief Remove scheduled/condition Job that was previously added (if found)
 *
 * @param[IN]  Job              :   The job object to be removed
 * @param[IN]  JobLen           :   The Len/Size in byte of the Job Object.
 * @param[IN]  SourceOfOrigin   :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveJob(void* Job, uint16_t JobLen, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);


/**
 * @brief New Adv Job is available for Job Manager to handle.. This is asynchronous
 *
 * @param[in]  NewJob   :   The New job object from UDP or Server. The Job Object structure should be according to Job data format specification
 * @param[in]  JobIsSinglePackExecution :   Is Single pack execution job which is supplementary to existing advance job condition, this allows quick execution instead of reporting to server and wait for server to send back the execution job.
 * @param[in]  JobLen   :   The Len/Size in byte of the New Job Object.
 * @param[in]  AdvjobID :   The Advance job object ID given by server.
 * @param[in]  SourceOfOrigin   :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerNewAdvanceJob_Task(void* NewJob, uint8_t JobIsSinglePackExecution, uint16_t JobLen, uint32_t AdvjobID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief New Adv Job is available for Job Manager to handle.. This is asynchronous
 *
 * @param[in]  NewJob   :   The New job object from UDP or Server. The Job Object structure should be according to Job data format specification
 * @param[in]  TimeStamp   : The timestamp (UTC+0) of this Job when it was originally created.
 * @param[in]  JobIsSinglePackExecution :   Is Single pack execution job which is supplementary to existing advance job condition, this allows quick execution instead of reporting to server and wait for server to send back the execution job.
 * @param[in]  JobLen   :   The Len/Size in byte of the New Job Object.
 * @param[in]  AdvjobID :   The Advance job object ID given by server.
 * @param[in]  SourceOfOrigin   :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[in]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerNewAdvanceJobWithTime_Task(void* NewJob, dxTime_t TimeStamp, uint8_t JobIsSinglePackExecution, uint16_t JobLen, uint32_t AdvjobID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief   Remove peripheral "specific" long-term jobs (ALL that is related to this peripheral) from the schedule list. This will compare the PeripheralID to the job's sequencial number.
 *          NOTE: This API will not be returning any event regardless of failure or completion
 *
 * @param[IN]  PeripheralID         :   The matching Peripheral ID to remove from the schedule job list
 * @param[IN]  SourceOfOrigin       :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemovePeripheralSpecificJobs_Task(uint32_t PeripheralID, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);


/**
 * @brief   Remove All scheduled/condition Job that was previously added (if found)
 *
 * @param[IN]  SourceOfOrigin           :  The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :  The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJob_Task(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief Synchronously remove all scheduled jobs that was previously added (if found)
 *
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllScheduleJob ();

/**
 * @brief Synchronously remove all advance jobs that was previously added (if found)
 *
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllAdvanceJob ();

/**
 * @brief   Synchronously Remove All scheduled/condition Job that was previously added (if found)
 *
 * @param[IN]  SourceOfOrigin           :  This is Not longer valid for synchronous remove job
 * @param[IN]  JobIdentificationNumber  :  This is Not longer valid for synchronous remove job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJob(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief Synchronously remove all scheduled jobs that was previously added (if found)
 *
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllScheduleJobExceptReserved ();

/**
 * @brief Synchronously remove all advance jobs that was previously added (if found)
 *
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllAdvanceJobExceptReserved ();

/**
 * @brief   Synchronously Remove All scheduled/condition Job that was previously added (if found)
 *          Except thos within reserved Obj ID
 *
 * @param[IN]  SourceOfOrigin           :  This is Not longer valid for synchronous remove job
 * @param[IN]  JobIdentificationNumber  :  This is Not longer valid for synchronous remove job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerRemoveAllJobExceptReserved(uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

#if DEVICE_IS_HYBRID
/**
 * @brief   Send Hybrid Peripheral data info to Job Manager to check for any Job related to this device
 *
 * @param[IN]  PeripheralInfo       :   The Peripheral info that needs to be checked for any updates
 * @param[IN]  SourceOfOrigin       :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerHybridPeripheralDataUpdate_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);
#endif  //#if DEVICE_IS_HYBRID

/**
 * @brief   Send Peripheral data info to Job Manager to check for any Job related to this device
 *
 * @param[IN]  PeripheralInfo       :   The Peripheral info that needs to be checked for any updates
 * @param[IN]  SourceOfOrigin       :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :   The identification number of this Job
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerPeripheralDataUpdate_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber);

/**
 * @brief   Send Peripheral data info to Job Manager to check for any Job related to this device
 *
 * @param[IN]  PeripheralInfo       :   The Peripheral info that needs to be checked for any updates
 * @param[IN]  SourceOfOrigin       :   The original source/location that sent this Job (see eSourceOfOrigin in dk_job.h)
 * @param[IN]  JobIdentificationNumber  :   The identification number of this Job
 * @param[IN]  ForceUpdate          :   Skip checking data integrity and force report update to server/local
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerPeripheralDataUpdateExt_Task(DeviceInfoReport_t* PeripheralInfo, uint16_t SourceOfOrigin,  uint64_t JobIdentificationNumber, dxBOOL ForceUpdate );

/**
 * @brief   Send Peripheral data info to Job Manager to check for any Job related to this device. NOTE that this will only check for Job and will not report to
 *          any status changes or data updates of the device. It is suggested to use JobManagerPeripheralDataUpdateExt_Task for devices that is non hybrid peripheral
 *          (eg, devices that is BLE).
 *
 * @param[IN]  PeripheralInfo       :   The Peripheral info that needs to be checked for available Job (eg, Schedule & Advance)
 *                                      NOTE: Only device address and decrypted advertisement data (AdData) and its len (AdDataLen) is needed. All other should set to 0.
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerPeripheralJobCheck_Task(DeviceInfoJobCheck_t* PeripheralInfo );

/**
 * @brief   Tell JobManager that Peripheral has been unpaired, JobManager will do internal cleaning for the unpair peripheral which some data was cached previously
 *
 * @param[IN]  UnpairedPeripheralInfo   :   The Peripheral info that has been unpaired
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerPeripheralUnpaired(DeviceInfoReport_t* UnpairedPeripheralInfo);

/**
 * @brief   Tell JobManager to extend the time of mobile client session, this time is used to let Job manager know if certain process needs to be taken care of..
 *          Such as peripheral payload of status only update.
 *
 * @param[IN]  SessionAliveTime         :   Time in seconds for request of session alive time
 * @return     dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerMobileClientSessionTimeExtend(uint16_t SessionAliveTime);

/**
 * @brief   TAG the Job identification info. This is used to TAG the the advance job or from specific user which at later stage
 *          when called JobManagerCheckJobIdInfoByMacAddrDec with TaggedOnly it will return the searched info. This function is a aid
 *          tool to only get the info that is intended for at appropriate time (rg, only when its tagged).
 *
 * @param[IN]   MacAddrDec  :   the peripheral mac address to TAG the Job identification info
 * @return     dxBOOL
 */
extern dxBOOL JobManagerTagJobIdInfoByMacAddrDec(uint64_t MacAddrDec);

/**
 * @brief   Get the Job identification info. This is used to retrieve if the job is from an advance job or from specific user
 *
 * @param[IN]  MacAddrDec   :   the peripheral mac address to find the Job identification info
 * @param[IN]  TaggedOnly   :   Return the info ONLY if the JobID was Tagged.
 * @return     JobIdInfo_t
 */
extern JobIdInfo_t JobManagerCheckJobIdInfoByMacAddrDec(uint64_t MacAddrDec, dxBOOL TaggedOnly);

/**
 * @brief Tell JobManager that entering leaving Superior Only JOb Only period
 *
 * @param[IN] bJobSuperiorOnly :   dxTRUE, Entering Superior Only Job period
 *                              dxFALSE, Leavinging Superior Only Job period
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerSetJobSuperiorOnly(dxBOOL bJobSuperiorOnly);

/**
 * @brief Tell JobManager the current state of server
 *
 * @param[IN] ServerIsReady :   dxTRUE, Server is ready
 *                              dxFALSE, Server is not ready
 * @return dxJOB_MANAGER_RET_CODE
 */
extern dxJOB_MANAGER_RET_CODE JobManagerNotifyServerReadyState(dxBOOL ServerIsReady);

#endif // _DXKJOBMANAGER_H
