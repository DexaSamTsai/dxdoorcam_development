/*******************************************************************************
 * Copyright (c) 2014, 2015 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation and/or initial documentation
 *    Ian Craggs - convert to FreeRTOS
 *******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "MQTTFreeRTOS.h"
#include "dxSysDebugger.h"
#include "netdb.h"
#include "dxSSL.h"



#define MAX_DUMP_BUFFER_SIZE        128
//#define DUMP_MQTT_DATA



static const char dxCert[] = \
    "-----BEGIN CERTIFICATE-----\r\n" \
    "MIIECzCCAvOgAwIBAgIJAJEEN67liR2HMA0GCSqGSIb3DQEBCwUAMIGbMQswCQYD\r\n" \
    "VQQGEwJUVzEPMA0GA1UECAwGVGFpd2FuMQ8wDQYDVQQHDAZUYWlwZWkxEDAOBgNV\r\n" \
    "BAoMB0RleGF0ZWsxDDAKBgNVBAsMA1ImRDEjMCEGA1UEAwwabXF0dC5kZXhhdGVr\r\n" \
    "d2Vic2VydmljZS5uZXQxJTAjBgkqhkiG9w0BCQEWFm1hc29uLmNoZW5AZGV4YXRl\r\n" \
    "ay5jb20wHhcNMTYwNjIyMDYzMTEzWhcNMjEwNjIxMDYzMTEzWjCBmzELMAkGA1UE\r\n" \
    "BhMCVFcxDzANBgNVBAgMBlRhaXdhbjEPMA0GA1UEBwwGVGFpcGVpMRAwDgYDVQQK\r\n" \
    "DAdEZXhhdGVrMQwwCgYDVQQLDANSJkQxIzAhBgNVBAMMGm1xdHQuZGV4YXRla3dl\r\n" \
    "YnNlcnZpY2UubmV0MSUwIwYJKoZIhvcNAQkBFhZtYXNvbi5jaGVuQGRleGF0ZWsu\r\n" \
    "Y29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyXEaBQfthSCbhatS\r\n" \
    "eO9zVQ6c/g3mgFRpw3uJ+y7dlXBEV2cYgvgRdEdbjbvRs4d+/+XIoj5KlYpevXMj\r\n" \
    "QcTvt6fzPV1RFrXtAIfiUXYNABGAwzS0dU7CEIuxDfM/Zu69C/jBy30ULpYvlq3K\r\n" \
    "DQpOGigd493LRhsSw/xYwRuhrdvceGq3WPrh2MwR8AGp5a3athcLIX1VlWRe4Y45\r\n" \
    "MEyHKBoIuA7wTs5aXPbDygXX6cYCzCNV6zeUAm0dJ1+de06Uap7QWwctY7sEWXgo\r\n" \
    "y1ZcKgvFLnHp4/xP3QRC40nPQno/pdruKm3ZshcayhPN/Ua8Uq5uHdBpHQU3PAvA\r\n" \
    "TyrU9QIDAQABo1AwTjAdBgNVHQ4EFgQU9IjF5rS2GW3OhUpejhcmheSk2fwwHwYD\r\n" \
    "VR0jBBgwFoAU9IjF5rS2GW3OhUpejhcmheSk2fwwDAYDVR0TBAUwAwEB/zANBgkq\r\n" \
    "hkiG9w0BAQsFAAOCAQEAOp5+vHWSw1YuXRZJyvZbXorrU99o9H/tDXnkXzCMbcm+\r\n" \
    "CtZWn2UHhTKFiPahB4xwe992AqF2/X4X8vGHbvVZoQ/fqTZYpW49b7R/BD8gBcdx\r\n" \
    "jFbjREX3WveMGtZp6HXj2ssXJQZxN729hZLX+v//4URAOh58nlF7fZVqML76UNN3\r\n" \
    "6zwUGlsR2vhnSLIhVLEOi2yZWrvRRNTTaHBWR8vsu6xUqCGr1YXhf3LrbyqBLeDb\r\n" \
    "oK8ij5xmDZ5TPzSunb2t6AKtpJlux3ezNFQowFXftR/AfXF9Kc5Ppls4vMOtfDKS\r\n" \
    "dSZiivzma729HuWXKKmBiJB1O+0V84HswEbkQVRONw==\r\n" \
    "-----END CERTIFICATE-----\r\n";


void _NetworkDump (
    int send,
    char* data,
    int len
) {
    int size = MAX_DUMP_BUFFER_SIZE / 5 - 1;
    int segment;
    int remain;
    int i;
    int j;
    char buffer[MAX_DUMP_BUFFER_SIZE];

    if (data == NULL) {
        goto fail;
    }

    GSysDebugger_LogNIV (
        G_SYS_DBG_CATEGORY_MQTTMANAGER,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "%s packet[%d] = {\r\n",
        send ? "Send" : "Receive",
        len
    );

    segment = len / size;
    remain = len % size;

    if (remain) {
      segment++;
    }

    for (i = 0; i < segment - 1; i++) {
        memset (buffer, 0x00, sizeof (buffer));
        for (j = 0; j < size; j++) {
            sprintf (&buffer[strlen (buffer)], "0x%02X,", data[i * size + j] & 0xff);
        }
        GSysDebugger_LogNIV (
            G_SYS_DBG_CATEGORY_MQTTMANAGER,
            G_SYS_DBG_LEVEL_FATAL_ERROR,
            "    %s\r\n",
            buffer
        );
    }

    // last segment
    memset (buffer, 0x00, sizeof (buffer));
    for (j = 0; j < remain - 1; j++) {
        sprintf (&buffer[strlen (buffer)], "0x%02X,", data[i * size + j] & 0xff);
    }

    // last byte
    sprintf (&buffer[strlen (buffer)], "0x%02X", data[i * size + j] & 0xff);
    GSysDebugger_LogNIV (
        G_SYS_DBG_CATEGORY_MQTTMANAGER,
        G_SYS_DBG_LEVEL_FATAL_ERROR,
        "    %s\r\n}\r\n",
        buffer
    );

fail :

    return;
}


int _NetworkOpen (
    Network* n
) {
    dxSSLHandle_t* handle;
    dxIP_Addr_t* address = n->address;
    int port = n->port;
    int ret = 0;

    handle = dxSSL_Connect (
        address,
        port,
        dxCert,
        SSL_IS_CLIENT,
        SSL_VERIFY_NONE
    );
    if (handle == NULL) {
        ret = -1;
    }
    n->handle = handle;

    return ret;
}


int _NetworkRead (
    Network* n,
    unsigned char* buffer,
    int len,
    int timeout_ms
) {
    dxSSLHandle_t* handle = n->handle;
    dxTime_t endTime = dxTimeGetMS () + timeout_ms;
    int recvLen = 0;

    do {
        int rc = 0;

        rc = dxSSL_ReceiveTimeout (handle, buffer + recvLen, len - recvLen, timeout_ms / 2);
        if (rc > 0) {
            recvLen += rc;
        } else if (rc < 0) {
            recvLen = rc;
            break;
        }
    } while ((recvLen < len) && (dxTimeGetMS () < endTime));

#ifdef DUMP_MQTT_DATA
    if (recvLen == len) {
        _NetworkDump (0, buffer, len);
    }
#endif

    return recvLen;
}


int _NetworkWrite (
    Network* n,
    unsigned char* buffer,
    int len,
    int timeout_ms
) {
    dxSSLHandle_t* handle = n->handle;
    dxTime_t endTime = dxTimeGetMS () + timeout_ms;
    int sentLen = 0;

    do {
        int rc = 0;

        rc = dxSSL_Send (handle, buffer + sentLen, len - sentLen);
        if (rc > 0) {
            sentLen += rc;
        } else if (rc == POLARSSL_ERR_NET_WANT_READ || rc == POLARSSL_ERR_NET_WANT_WRITE) {
        } else if (rc < 0) {
            sentLen = rc;
            break;
        }
    } while ((sentLen < len) && (dxTimeGetMS () < endTime));

#ifdef DUMP_MQTT_DATA
    if (sentLen == len) {
        _NetworkDump (1, buffer, len);
    }
#endif

    return sentLen;
}


void _NetworkClose (
    Network* n
) {
    dxSSLHandle_t* handle = n->handle;

    if (handle != NULL) {
        dxSSL_Close (handle);
        n->handle = NULL;
    }
}


int _NetworkSelect (
    Network* n
) {
    dxSSLHandle_t* handle = n->handle;
    struct timeval timeout;
    fd_set read_fds;
    fd_set except_fds;
    int socket;
    int rc = 0;

    if (handle != NULL) {
        FD_ZERO (&read_fds);
        FD_ZERO (&except_fds);
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;

        socket = handle->sfd;
        FD_SET (socket, &read_fds);
        FD_SET (socket, &except_fds);
        rc = dxSelect (
            socket + 1,
            &read_fds,
            NULL,
            &except_fds,
            &timeout
        );

        if (FD_ISSET (socket, &except_fds)) {
            rc = -1;
        } else if (FD_ISSET (socket, &read_fds)) {
            rc = 1;
        } else {
            rc = 0;
        }
    }

    return rc;
}


void NetworkInit (
    Network* n,
    dxIP_Addr_t* address,
    uint16_t port
) {
    if (n) {
        n->address = address;
        n->port = port;

        n->_open = _NetworkOpen;
        n->_read = _NetworkRead;
        n->_write = _NetworkWrite;
        n->_close = _NetworkClose;
        n->_select = _NetworkSelect;
    }
}


void TimerCountdownMS (Timer* timer, unsigned int timeout_ms) {
    timer->xTicksToWait = timeout_ms / portTICK_PERIOD_MS; /* convert milliseconds to ticks */
    vTaskSetTimeOutState (&timer->xTimeOut); /* Record the time at which this function was entered. */
}


void TimerCountdown (Timer* timer, unsigned int timeout) {
    TimerCountdownMS (timer, timeout * 1000);
}


int TimerLeftMS (Timer* timer) {
    xTaskCheckForTimeOut (&timer->xTimeOut, &timer->xTicksToWait); /* updates xTicksToWait to the number left */
    return (timer->xTicksToWait * portTICK_PERIOD_MS);
}


char TimerIsExpired (Timer* timer) {
    return xTaskCheckForTimeOut (&timer->xTimeOut, &timer->xTicksToWait) == pdTRUE;
}


void TimerInit (Timer* timer) {
    timer->xTicksToWait = 0;
    memset (&timer->xTimeOut, '\0', sizeof(timer->xTimeOut));
}


void mqtt_printf (
    unsigned char level,
    const char *fmt,
    ...
) {
    static char buffer[LOG_MAX_CHAR_BUFFER + 1];
    memset(buffer, 0x00, sizeof(buffer));

    if (level >= MQTT_DEBUG) {
        va_list ap;

        va_start (ap, fmt);
        vsnprintf (buffer, sizeof(buffer), fmt, ap);
        va_end(ap);

        int n = G_SYS_DBG_LEVEL_DETAIL_INFORMATION;
        switch (level) {
            case MQTT_INFO :    n = G_SYS_DBG_LEVEL_DETAIL_INFORMATION;  break;
            case MQTT_ALWAYS :  n = G_SYS_DBG_LEVEL_GENERAL_INFORMATION; break;
            case MQTT_WARNING : n = G_SYS_DBG_LEVEL_GENERAL_INFORMATION; break;
            case MQTT_ERROR :   n = G_SYS_DBG_LEVEL_FATAL_ERROR;         break;
        }

        G_SYS_DBG_LOG_IV (
            G_SYS_DBG_CATEGORY_MQTTMANAGER,
            n,
            "%s\r\n",
            buffer
        );
    }
}
