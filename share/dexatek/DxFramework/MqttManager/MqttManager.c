//============================================================================
// File: MqttManager.c
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//
// Version: 0.1
//   Date: 2017/12/08
//   1) Description: Initial
//============================================================================

//============================================================================
// Includes
//============================================================================

#include "dxDNS.h"
#include "ObjectDictionary.h"
#include "dk_network.h"
#include "dkjsn.h"
#include "dxOS.h"
#include "dxDeviceInfoManager.h"
#include "dxSysDebugger.h"
#include "JobManager.h"
#include "WifiCommManager.h"
#include "DKServermanager.h"
#include "SimpleTimedStateMachine.h"

#include "MQTTClient.h"
#include "MqttManager.h"



//============================================================================
// Definations
//============================================================================

#define DXMQTT_THREAD_PRIORITY                      (DXTASK_PRIORITY_NORMAL)
#define DXMQTT_THREAD_NAME                          "MQTT"
#define DXMQTT_THREAD_STACK_SIZE                    (6 * 512)

#define DXMQTT_THREAD_QUEUE_NAME                    "MQTQ"
#define DXMQTT_THREAD_QUEUE_MAX_ALLOWED             5

#define DXMQTT_THREAD_WORKER_THREAD_NAME            "MQEvt"
#define DXMQTT_THREAD_WORKER_THREAD_STACK_SIZE      1024
#define DXMQTT_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE 5

#define DXMQTT_THREAD_POP_QUEUE_TIMEOUT_TIME        500         // In milliseconds
#define DXMQTT_YIELD_TIMEOUT_TIME                   1000        // In milliseconds
#define DXMQTT_TASK_SLEEP                           5000        // In milliseconds
#define DXMQTT_KEEP_ALIVE_INTERVAL                  60          // In seconds
#define DXMQTT_COMMAND_TIMEOUT                      30          // In seconds

#define DXMQTT_VERSION                      4
#define DXMQTT_WILL_TOPIC_FORMAT            "out/ugroup/%u/device/%u/status"
#define DXMQTT_WILL_MESSAGE_FORMAT          "{\"func\":\"DKEvent\",\"time\":0,\"args\":{\"Name\":\"DeviceStatus\",\"ObjectID\":%u,\"Status\":0}}"

#define DXMQTT_MAX_RECONNECT                240     // ~20 minutes retrys

#define MQTT_SUBSCRIBE_FORMAT_0                 "out/ugroup/%u/job"
#define MQTT_SUBSCRIBE_FORMAT_1                 "out/ugroup/%u/activeuser"
#define MQTT_SUBSCRIBE_FORMAT_2                 "out/ugroup/%u/device/%u/status"
#define MQTT_SUBSCRIBE_NUMBER                   3

#define MQTT_DEVSTATUS_CHANNEL_TOPIC_NAME       "out/ugroup/%u/device/%u/status"
#define MQTT_DEVSTATUS_CHANNEL_TOPIC_CONTENT    "{\"func\":\"DKEvent\",\"time\":%u,\"args\":{\"Name\":\"DeviceStatus\",\"ObjectID\":%u,\"Status\":%u}}"
#define MQTT_DEVSTATUS_ONLINE                   1
#define MQTT_DEVSTATUS_OFFLINE                  0
#define MQTT_DEVSTATUS_PUBLISH_INTERVAL         (10 * SECONDS)
#define MQTT_RECOVER_WAIT_TIME                  (6 * SECONDS)

#define MAX_DUMP_BUFFER_SIZE                    64
//#define DEBUG_PAYLOAD

#define error(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, ##__VA_ARGS__);
#define debug(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_GENERAL_INFORMATION, ##__VA_ARGS__);
#define trace(...)     GSysDebugger_LogIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_DETAIL_INFORMATION, ##__VA_ARGS__);



//============================================================================
// Types
//============================================================================

/**
 * @brief struct dxMQTTThreadParam_t
 */
typedef enum {
    MQTT_SUBSTATE_INIT          = 0,
    MQTT_SUBSTATE_CONNECT,
    MQTT_SUBSTATE_SUBSCRIBE,
    MQTT_SUBSTATE_RUNNING,
    MQTT_SUBSTATE_FALLBACK,
    MQTT_SUBSTATE_DISCONNECT,
} MqttSubState;


/**
 * @brief struct dxMQTTThreadParam_t
 */
typedef struct dxMQTTThreadParam {
    dxTaskHandle_t  handle;
    dxQueueHandle_t queue;
    dxMutexHandle_t mutex;
    MqttWorker      worker;
    MQTTClient      client;
    Network         network;
    uint8_t         sendBuffer[MQTT_SENDBUF_LEN];
    uint8_t         recvBuffer[MQTT_READBUF_LEN];
    char*           subTopic[MQTT_SUBSCRIBE_NUMBER];
} dxMQTTThreadParam_t;


/**
 * @brief struct dxMQTTLoginAccount_t
 */
typedef struct dxMQTTLoginAccount {
    uint8_t* HostName;
    uint16_t Port;
    uint8_t* UserName;
    uint8_t* AccessKey;
    uint8_t* clientId;
} dxMQTTLoginAccount_t;



//============================================================================
// Private Methods
//============================================================================

/**
 * @brief _MqttManager_Loop
 */
static void _MqttManager_Loop (
    void* argument
);


/**
 * @brief _MqttManager_MessageHandler
 */
static void _MqttManager_MessageHandler (
    MessageData* data
);


/**
 * @brief _MqttManager_PushTopicToQueue
 */
static dxOS_RET_CODE _MqttManager_PushTopicToQueue (
    MqttTopic** topic
);


/**
 * @brief _MqttManager_Substate_Connect
 */
SimpleStateMachine_result_t _MqttManager_Substate_Connect (
    void* data,
    dxBOOL isFirstCall
);


/**
 * @brief _MqttManager_Substate_Subscribe
 */
SimpleStateMachine_result_t _MqttManager_Substate_Subscribe (
    void* data,
    dxBOOL isFirstCall
);


/**
 * @brief _MqttManager_Substate_Running
 */
SimpleStateMachine_result_t _MqttManager_Substate_Running (
    void* data,
    dxBOOL isFirstCall
);


/**
 * @brief _MqttManager_Substate_Fallback
 */
SimpleStateMachine_result_t _MqttManager_Substate_Fallback (
    void* data,
    dxBOOL isFirstCall
);


/**
 * @brief _MqttManager_Substate_Disconnect
 */
SimpleStateMachine_result_t _MqttManager_Substate_Disconnect (
    void* data,
    dxBOOL isFirstCall
);


/**
 * @brief _MqttManager_GetConnectData
 */
MQTTPacket_connectData* _MqttManager_GetConnectData (
    uint32_t userId,
    uint32_t gatewayId
);


/**
 * @brief _MqttManager_GetHostAddress
 */
dxIP_Addr_t* _MqttManager_GetHostAddress (void);


/**
 * @brief _MqttManager_Parse
 */
MqttResult _MqttManager_Parse (
    MqttEvent* event,
    uint8_t* data
);


/**
 * @brief _MqttManager_Dump
 */
void _MqttManager_Dump (
    const char* prefix,
    char* p,
    int len
);



//============================================================================
// Global Variable
//============================================================================

static simple_st_machine_table_t mqttSubstateFunctions[] = {
    {MQTT_SUBSTATE_INIT,         NULL,                                        0},
    {MQTT_SUBSTATE_CONNECT,      _MqttManager_Substate_Connect,     1 * SECONDS},  //Call every seconds if remain in the same state
    {MQTT_SUBSTATE_SUBSCRIBE,    _MqttManager_Substate_Subscribe,   1 * SECONDS},  //Call every seconds if remain in the same state
    {MQTT_SUBSTATE_RUNNING,      _MqttManager_Substate_Running,     1 * SECONDS},  //Call every seconds if remain in the same state
    {MQTT_SUBSTATE_FALLBACK,     _MqttManager_Substate_Fallback,    1 * SECONDS},  //Call every seconds if remain in the same state
    {MQTT_SUBSTATE_DISCONNECT,   _MqttManager_Substate_Disconnect,  1 * SECONDS},  //Call every seconds if remain in the same state

    //MUST BE IN THE LAST STATE ITEM
    {SIMPLE_STATE_INVALID, NULL, 0}                                 //END of state Table - This is a MUST
};


dxMQTTLoginAccount_t mqttAccounts[] = {
    {
        "mqtt.dexatekwebservice.net",
        8883,
        NULL,
        NULL,
        NULL
    },
    {
        "mqttqa.dexatekwebservice.net",
        8883,
        NULL,
        NULL,
        NULL
    }
};


static dxMQTTThreadParam_t threadParam = {
    NULL,       // handle
    NULL,       // queue
    NULL,       // mutex
    {           // MqttWorker
        NULL,   //      handle
        NULL    //      callback
    },
    { 0x00 },   // client
    { 0x00 },   // network
    { 0x00 },   // sendBuffer
    { 0x00 },   // recvBuffer
    {           // subTopic
        NULL,
        NULL
    }
};

static jsmntok_t* tokenTable;
dxBOOL mqttInit = dxFALSE;
uint16_t reconnectCount = 0;
static uint8_t activeIndex = 0;
SimpleStateHandler mqttSubstateHandler = NULL;
MQTTPacket_connectData* connectData = NULL;
dxIP_Addr_t* address = NULL;
uint16_t port;
uint8_t* topicName = NULL;
uint8_t* topicContent = NULL;



//============================================================================
// Implementations
//============================================================================

MqttResult MqttManager_Init (
    dxEventHandler_t eventFunction
) {
    dxOS_RET_CODE result = DXOS_ERROR;
    dxWorkweTask_t* worker;

    if (eventFunction == NULL) {
        result = DXOS_ERROR;
        goto fail_create_token;
    }

    tokenTable = (jsmntok_t*) dxMemAlloc (DK_MEM_ZONE_SLOW, MAX_NUM_JSON_ELEMENT_SUPPORT, sizeof (jsmntok_t));
    if (tokenTable == NULL) {
        goto fail_create_token;
    }

    // Initialize mutex
    threadParam.mutex = dxMutexCreate ();

    // Initialize queue
    if (threadParam.queue == NULL) {
        threadParam.queue = dxQueueCreate (
            DXMQTT_THREAD_QUEUE_MAX_ALLOWED,
            sizeof (MqttTopic*)
        );
        if (threadParam.queue == NULL) {
            result = DXOS_ERROR;
            goto fail_create_queue;
        }
    }

    // Initialize main thread
    if (threadParam.handle == NULL) {
        result = dxTaskCreate (
            _MqttManager_Loop,
            DXMQTT_THREAD_NAME,
            DXMQTT_THREAD_STACK_SIZE,
            (void*) &threadParam,
            DXMQTT_THREAD_PRIORITY,
            &threadParam.handle
        );
        if ((result != DXOS_SUCCESS) || (threadParam.handle == NULL)) {
            goto fail_create_thread;
        }

        GSysDebugger_RegisterForThreadDiagnostic (&threadParam.handle);
    }

    // Initialize worker thread
    if (threadParam.worker.callback == NULL) {
        worker = dxMemAlloc (DXMQTT_THREAD_NAME, 1, sizeof (dxWorkweTask_t));

        result = dxWorkerTaskCreate (
            "MQTT W",
            worker,
            DX_DEFAULT_WORKER_PRIORITY,
            DXMQTT_THREAD_WORKER_THREAD_STACK_SIZE,
            DXMQTT_THREAD_WORKER_THREAD_MAX_EVENT_QUEUE
        );
        if (result != DXOS_SUCCESS) {
            goto fail_create_worker;
        }

        threadParam.worker.handle = worker;

        GSysDebugger_RegisterForThreadDiagnosticGivenName (
            &threadParam.worker.handle->WTask,
            DXMQTT_THREAD_WORKER_THREAD_NAME
        );

        threadParam.worker.callback = eventFunction;

        int i;
        for (i = 0; i < MQTT_SUBSCRIBE_NUMBER; i++) {
            threadParam.subTopic[i] = dxMemAlloc ("subTopic", DXMQTT_SUBSCRIBE_TOPIC_MAX_LEN, sizeof (char));
        }
    }

    mqttSubstateHandler = SimpleStateMachine_Init (mqttSubstateFunctions);
    SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_INIT);

    return DXOS_SUCCESS;

fail_create_worker :

    if (threadParam.handle != NULL) {
        dxTaskDelete (threadParam.handle);
        threadParam.handle = NULL;
    }

fail_create_thread :

    if (threadParam.queue != NULL) {
        dxQueueDelete (threadParam.queue);
        threadParam.queue = NULL;
    }

fail_create_queue :

    dxMemFree (tokenTable);

fail_create_token :

    return result;

}


MqttResult MqttManager_Connect_Async (void) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (!SimpleStateMachine_StateNotChangingWhileEqualTo (mqttSubstateHandler, MQTT_SUBSTATE_INIT)) {
        error (
            "MQTT is not under runnging state, MQTT state=%d, (MqttManager.c, %d)\r\n",
            SimpleStateMachine_GetCurrentState (mqttSubstateHandler),
            __LINE__
        );
        mqttResult = MqttResult_WRONG_STATE;
        goto fail;
    }

    result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_CONNECT);
    if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
        error (
            "Fail to ExecuteState (MQTT_SUBSTATE_CONNECT), (MqttManager.c, %d)\r\n",
            __LINE__
        );
    }

fail :

    return mqttResult;
}


MqttResult MqttManager_Publish_Async (
    uint8_t* topicName,
    uint8_t* topicContent
) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    dxOS_RET_CODE osResult = DXOS_SUCCESS;
    MqttTopic* topic = NULL;

    if (!SimpleStateMachine_StateNotChangingWhileEqualTo (mqttSubstateHandler, MQTT_SUBSTATE_RUNNING)) {
        error (
            "Cannot call MqttManager_Publish_Async () at state=%d, (MqttManager.c, %d)\r\n",
            SimpleStateMachine_GetCurrentState (mqttSubstateHandler),
            __LINE__
        );
        mqttResult = MqttResult_WRONG_STATE;
        goto fail_alloc_message;
    }

    if ((topicName == NULL) || (strlen (topicName) >= DXMQTT_SUBSCRIBE_TOPIC_MAX_LEN)) {
        mqttResult = MqttResult_INVALID_PARAMETER;
        goto fail_alloc_message;
    }

    topic = (MqttTopic*) dxMemAlloc (
        DXMQTT_THREAD_NAME,
        1,
        sizeof (MqttTopic)
    );
    if (topic == NULL) {
        mqttResult = MqttResult_NOT_ENOUGH_MEMORY;
        goto fail_alloc_message;
    }

    memset (topic, 0x00, sizeof (MqttTopic));
    topic->TopicName.Length = strlen (topicName);
    strcpy (topic->TopicName.pData, topicName);

#ifdef DEBUG_PAYLOAD
    _MqttManager_Dump ("TopicName", topicName, strlen (topicName));
#endif

    topic->TopicContent.Length = strlen (topicContent);
    strcpy (topic->TopicContent.pData, topicContent);

#ifdef DEBUG_PAYLOAD
    _MqttManager_Dump ("TopicContent", topicContent, strlen (topicContent));
#endif

    topic->TopicContent.Retained = 1;
    topic->TopicContent.Dup = 0;
    topic->TopicContent.QoS = MqttQos_QOS1;

    osResult = _MqttManager_PushTopicToQueue (&topic);
    if (osResult != DXOS_SUCCESS) {
        mqttResult = MqttResult_ERROR;
        goto fail;
    }

    return MqttResult_SUCCESS;

fail :

    if (topic != NULL) {
        dxMemFree (topic);
        topic = NULL;
    }

fail_alloc_message :

    return mqttResult;
}


MqttResult MqttManager_Disconnect_Async (void) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;

    if (SimpleStateMachine_StateNotChangingWhileEqualTo (mqttSubstateHandler, MQTT_SUBSTATE_INIT)) {
        error (
            "Cannot call MqttManager_Disconnect_Async () at state=%d, (MqttManager.c, %d)\r\n",
            SimpleStateMachine_GetCurrentState (mqttSubstateHandler),
            __LINE__
        );
        mqttResult = MqttResult_ERROR;
        goto fail;
    }

    result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_DISCONNECT);
    if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
        error (
            "Fail to ExecuteState (MQTT_SUBSTATE_DISCONNECT), (MqttManager.c, %d)\r\n",
            __LINE__
        );
    }

fail :

    return mqttResult;
}


MqttResult MqttManager_SetActiveIndex (int index) {
    activeIndex = index;

    return MqttResult_SUCCESS;
}


MqttState MqttManager_GetState (void) {

    MqttState state;

    switch (SimpleStateMachine_GetCurrentState (mqttSubstateHandler)) {
        case MQTT_SUBSTATE_INIT :       state = MqttState_INIT;       break;
        case MQTT_SUBSTATE_CONNECT :    state = MqttState_CONNECT;    break;
        case MQTT_SUBSTATE_SUBSCRIBE :  state = MqttState_SUBSCRIBE;  break;
        case MQTT_SUBSTATE_RUNNING :    state = MqttState_RUNNING;    break;
        case MQTT_SUBSTATE_FALLBACK :   state = MqttState_FALLBACK;    break;
        case MQTT_SUBSTATE_DISCONNECT : state = MqttState_DISCONNECT; break;
        default :                       state = MqttState_INIT;       break;
    }

    return state;
}


MqttHeader MqttManager_GetEventReportHeader (
    void* argument
) {
    MqttEventReport* eventReport;
    MqttHeader header;

    if (argument) {
        eventReport = (MqttEventReport*) argument;
        memcpy (&header, &eventReport->header, sizeof (MqttHeader));
    }

    return header;
}


uint8_t* MqttManager_GetEventReportPayload (
    void* argument
) {
    MqttEventReport* eventReport;
    uint8_t* data = NULL;

    if (argument) {
        eventReport = (MqttEventReport*) argument;
        if (eventReport->header.dataLen > 0) {
            data = eventReport->data;
        }
    }

    return data;
}


MqttResult MqttManager_CleanEventReport (
    void* argument
) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    MqttEventReport* eventReport;

    if (argument == NULL) {
        mqttResult = MqttResult_ERROR;
        goto fail_invalid_param;
    }

    eventReport = (MqttEventReport*) argument;
    if (eventReport->data) {
        dxMemFree (eventReport->data);
        eventReport->data = NULL;
    }
    dxMemFree (eventReport);
    eventReport = NULL;

    return MqttResult_SUCCESS;

fail_invalid_param :

    return mqttResult;
}


MqttResult MqttManager_CleanEvent (
    void* argument
) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    MqttEvent* event;

    if (argument == NULL) {
        mqttResult = MqttResult_ERROR;
        goto fail_invalid_param;
    }

    event = (MqttEvent*) argument;
    if (event->data) {
        dxMemFree (event->data);
        event->data = NULL;
    }

    return MqttResult_SUCCESS;

fail_invalid_param :

    return mqttResult;
}


static void _MqttManager_Loop (
    void* argument
) {
    while (1) {
        SimpleStateMachine_result_t result;

        result = SimpleStateMachine_Process (mqttSubstateHandler);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "FATAL: SimpleStateMachine_Process (mqttSubstateHandler), result=%d, (MqttManager.c, %d)\r\n",
                result,
                __LINE__
            );
        }

        dxTimeDelayMS (10);
    }
}


static void _MqttManager_MessageHandler (
    MessageData* data
) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    dxOS_RET_CODE osResult = DXOS_ERROR;
    MqttEventReport* eventReport = NULL;
    MqttEvent* event = NULL;
    size_t jsonLen;
    int len;
    char* json;

    if (
        (data == NULL) ||
        ((data->message == NULL) || (data->topicName == NULL))
    ) {
        mqttResult = MqttResult_INVALID_PARAMETER;
        goto fail_alloc_report;
    }

    eventReport = (MqttEventReport*) dxMemAlloc (
        DXMQTT_THREAD_WORKER_THREAD_NAME,
        1,
        sizeof (MqttEventReport)
    );
    if (eventReport == NULL) {
        mqttResult = MqttResult_NOT_ENOUGH_MEMORY;
        goto fail_alloc_report;
    }

    eventReport->header.messageType = MqttMessageType_NORMAL;
    eventReport->errorCode = MqttResult_SUCCESS;

    // What's 0? The value of len should be sizeof (MqttEvent) plus length of pData
    // Here, we only are interested in DKMSG_NEWJOBS which without payload
    len = sizeof (MqttEvent) + 0;

    eventReport->header.dataLen = len;
    eventReport->data = dxMemAlloc (
        DXMQTT_THREAD_WORKER_THREAD_NAME,
        1,
        len
    );
    if (eventReport->data == NULL) {
        mqttResult = MqttResult_NOT_ENOUGH_MEMORY;
        goto fail_alloc_data;
    }
    event = (MqttEvent*) eventReport->data;

    json = data->message->payload;
    jsonLen = data->message->payloadlen;
    json[jsonLen] = '\0';

#ifdef DEBUG_PAYLOAD
    _MqttManager_Dump ("DataReceive", json, jsonLen);
#endif

    if (_MqttManager_Parse (event, json) != MqttResult_SUCCESS) {
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }

    osResult = dxWorkerTaskSendAsynchEvent (
        threadParam.worker.handle,
        threadParam.worker.callback,
        eventReport
    );
    if (osResult != DXOS_SUCCESS) {
        mqttResult = MqttResult_ERROR;
    }

    return;

fail :

    if (event != NULL) {
        dxMemFree (event);
        event = NULL;
    }

fail_alloc_data :

    if (eventReport != NULL) {
        dxMemFree (eventReport);
        eventReport = NULL;
    }

fail_alloc_report :

    error (
        "Fail to call _MqttManager_MessageHandler (), mqttResult=%d, (MqttManager.c, %d)\r\n",
        mqttResult,
        __LINE__
    );

    return;
}


static dxOS_RET_CODE _MqttManager_PushTopicToQueue (
    MqttTopic** topic
) {
    dxOS_RET_CODE osResult = DXOS_SUCCESS;
    dxQueueHandle_t queue = threadParam.queue;

    if (queue) {
        if (dxQueueIsFull (queue) == dxTRUE) {
            osResult = DXOS_NO_MEM;
        } else {
            if (dxQueueSend (queue, topic, 1000) != DXOS_SUCCESS) {
                osResult = DXOS_ERROR;
            }
        }
    }

    return osResult;
}


SimpleStateMachine_result_t _MqttManager_Substate_Connect (
    void* data,
    dxBOOL isFirstCall
) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    MQTTClient* client = &threadParam.client;
    Network* network = &threadParam.network;
    uint32_t userId = ObjectDictionary_GetUser ()->ObjectID;
    uint32_t gatewayId = ObjectDictionary_GetGateway ()->ObjectID;
    uint8_t* sendBuffer = threadParam.sendBuffer;
    uint8_t* recvBuffer = threadParam.recvBuffer;
    char** subTopic = threadParam.subTopic;
    int connectResult = 0;

    trace (
        "_MqttManager_Substate_Connect (isFirstCall=%s), reconnectCount=%d, (MqttManager.c, %d)\r\n",
        (isFirstCall == dxTRUE) ? "true" : "false",
        reconnectCount,
        __LINE__
    );

    if (mqttInit == dxFALSE) {
        trace ("userId=%u, gatewayId=%u\r\n", userId, gatewayId);

        connectData = _MqttManager_GetConnectData (userId, gatewayId);
        if (connectData == NULL) {
            goto fail;
        }

        if (address) {
            dxMemFree (address);
            address = NULL;
        }
        address = _MqttManager_GetHostAddress ();
        if (address == NULL) {
            goto fail;
        }

        if (topicName) {
            dxMemFree (topicName);
            topicName = NULL;
        }
        topicName = dxMemAlloc (
            "MQTT",
            1,
            strlen (MQTT_DEVSTATUS_CHANNEL_TOPIC_NAME) + 20
        );
        if (topicName == NULL) {
            goto fail;
        }
        sprintf (topicName, MQTT_DEVSTATUS_CHANNEL_TOPIC_NAME, userId, gatewayId);

        if (topicContent) {
            dxMemFree (topicContent);
            topicContent = NULL;
        }
        topicContent = dxMemAlloc (
            "MQTT",
            1,
            strlen (MQTT_DEVSTATUS_CHANNEL_TOPIC_CONTENT) + 20
        );
        if (topicContent == NULL) {
            goto fail;
        }

        port = mqttAccounts[activeIndex].Port;

        sprintf (subTopic[0], MQTT_SUBSCRIBE_FORMAT_0, userId);
        sprintf (subTopic[1], MQTT_SUBSCRIBE_FORMAT_1, userId);
        sprintf (subTopic[2], MQTT_SUBSCRIBE_FORMAT_2, userId, gatewayId);

        NetworkInit (
            network,
            address,
            port
        );

        mqttInit = dxTRUE;
    }

    if (reconnectCount >= DXMQTT_MAX_RECONNECT) {
        error (
            "MQTT reconnect for %d times, trigger fallback. (MqttManager.c, %d)\r\n",
            reconnectCount,
            __LINE__
        );
        reconnectCount = 0;

        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_FALLBACK);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_FALLBACK), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
        goto fail;
    }

    reconnectCount++;

    MQTTClientInit (
        client,
        network,
        connectData->keepAliveInterval,
        connectData->commandTimeout,
        sendBuffer,
        MQTT_SENDBUF_LEN,
        recvBuffer,
        MQTT_READBUF_LEN
    );

    if (client->isconnected == 0) {

        debug ("Start MQTT connection\r\n");
        if ((connectResult = MQTTConnect (client, connectData)) != 0) {
            debug ("Return code from MQTT connect is %d\r\n", connectResult);
            goto fail;
        }

        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_SUBSCRIBE);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_SUBSCRIBE), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
    }

fail :

    return result;
}


SimpleStateMachine_result_t _MqttManager_Substate_Subscribe (
    void* data,
    dxBOOL isFirstCall
) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    MQTTClient* client = &threadParam.client;
    Network* network = &threadParam.network;
    char** subTopic = threadParam.subTopic;
    int receiveEvent = 0;
    int connectResult = 0;
    int i;

    trace (
        "_MqttManager_Substate_Subscribe (isFirstCall=%s), (MqttManager.c, %d)\r\n",
        (isFirstCall == dxTRUE) ? "true" : "false",
        __LINE__
    );

    receiveEvent = network->_select (network);
    if (receiveEvent < 0) {
        error (
            "MQTT connection produced exception at T%d, will trigger fallback. (MqttManager.c, %d)\r\n",
            dxTimeGetMS (),
            __LINE__
        );

        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_FALLBACK);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_FALLBACK), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
        goto fail;
    }

    for (i = 0; i < MQTT_SUBSCRIBE_NUMBER; i++) {
        if ((connectResult = MQTTSubscribe (client, subTopic[i], QOS2, _MqttManager_MessageHandler)) != 0) {
            error ("Return code from MQTT subscribe is %d\r\n", connectResult);
            break;
        }
        debug ("Subscribe to Topic: %s\r\n", subTopic[i]);
    }

    if (i == MQTT_SUBSCRIBE_NUMBER) {
        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_RUNNING);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_RUNNING), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
    }  else {
        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_FALLBACK);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_FALLBACK), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
    }

fail :

    return result;
}


SimpleStateMachine_result_t _MqttManager_Substate_Running (
    void* data,
    dxBOOL isFirstCall
) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    static dxTime_t startAliveTime;
    dxTime_t aliveTime;
    MQTTMessage content;
    MQTTClient* client = &threadParam.client;
    Network* network = &threadParam.network;
    uint32_t utc;
    uint32_t gatewayId;
    int receiveEvent = 0;
    int connectResult = 0;

    trace (
        "_MqttManager_Substate_Running (isFirstCall=%s), (MqttManager.c, %d)\r\n",
        (isFirstCall == dxTRUE) ? "true" : "false",
        __LINE__
    );

    if (isFirstCall) {

        startAliveTime = dxTimeGetMS ();

    } else {

        aliveTime = dxTimeGetMS ();
        if (aliveTime >= startAliveTime + MQTT_DEVSTATUS_PUBLISH_INTERVAL) {
            startAliveTime = aliveTime;
            utc = dxGetUtcInSec ();
            gatewayId = ObjectDictionary_GetGateway ()->ObjectID;
            sprintf (topicContent, MQTT_DEVSTATUS_CHANNEL_TOPIC_CONTENT, utc, gatewayId, MQTT_DEVSTATUS_ONLINE);
            content.payload = topicContent;
            content.payloadlen = strlen (topicContent);
            content.retained = 1;
            content.qos = QOS1;
            content.dup = 0;
#ifdef DEBUG_PAYLOAD
            _MqttManager_Dump ("TopicName", topicName, strlen (topicName));
            _MqttManager_Dump ("TopicContent", topicContent, strlen (topicContent));
#endif
            MQTTPublish (client, topicName, &content);
        }
    }

    receiveEvent = network->_select (network);
    if (receiveEvent < 0) {
        error (
            "MQTT connection produced exception at T%d, will trigger fallback. (MqttManager.c, %d)\r\n",
            dxTimeGetMS (),
            __LINE__
        );

        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_FALLBACK);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_FALLBACK), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
        goto fail;

    }

    if (receiveEvent) {
        MQTTCycle (client);
    }

    if (client->isconnected == 0) {
        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_FALLBACK);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_FALLBACK), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
    }

    MQTTKeepAlive (client);

fail :

    return result;
}


SimpleStateMachine_result_t _MqttManager_Substate_Fallback (
    void* data,
    dxBOOL isFirstCall
) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    MqttEventReport* eventReport = NULL;
    MQTTClient* client = &threadParam.client;
    Network* network = &threadParam.network;
    dxOS_RET_CODE osResult = DXOS_ERROR;
    static dxTime_t RecoverTimeStart = 0;
    dxTime_t CurrentTime;

    trace (
        "_MqttManager_Substate_Fallback (isFirstCall=%s), (MqttManager.c, %d)\r\n",
        (isFirstCall == dxTRUE) ? "true" : "false",
        __LINE__
    );

    if (isFirstCall) {

        RecoverTimeStart = dxTimeGetMS ();

        MQTTDisconnect (client);

        eventReport = (MqttEventReport*) dxMemAlloc (
            DXMQTT_THREAD_WORKER_THREAD_NAME,
            1,
            sizeof (MqttEventReport)
        );
        if (eventReport == NULL) {
            goto fail;
        }

        eventReport->header.messageType = MqttMessageType_ERROR;
        eventReport->errorCode = MqttResult_FALLBACK;
        eventReport->header.dataLen = 0;
        eventReport->data = NULL;

        osResult = dxWorkerTaskSendAsynchEvent (
            threadParam.worker.handle,
            threadParam.worker.callback,
            eventReport
        );
        if (osResult != DXOS_SUCCESS) {
            goto fail;
        }

    } else {

        CurrentTime = dxTimeGetMS ();

        if ((CurrentTime - RecoverTimeStart) > MQTT_RECOVER_WAIT_TIME) {

            //Regardless of outcome we update it to current time, in case we are still in this state we will keep requesting periodically
            RecoverTimeStart = dxTimeGetMS ();

            result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_CONNECT);
            if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
                error (
                    "Fail to ExecuteState (MQTT_SUBSTATE_CONNECT), (MqttManager.c, %d)\r\n",
                    __LINE__
                );
            }
        }
    }

fail :

    return result;
}


SimpleStateMachine_result_t _MqttManager_Substate_Disconnect (
    void* data,
    dxBOOL isFirstCall
) {
    SimpleStateMachine_result_t result = SMP_STATE_MACHINE_RET_SUCCESS;
    MqttEventReport* eventReport = NULL;
    MQTTClient* client = &threadParam.client;
    Network* network = &threadParam.network;
    dxOS_RET_CODE osResult = DXOS_ERROR;

    trace (
        "_MqttManager_Substate_Disconnect (isFirstCall=%s), (MqttManager.c, %d)\r\n",
        (isFirstCall == dxTRUE) ? "true" : "false",
        __LINE__
    );

    if (isFirstCall) {
        MQTTDisconnect (client);
    } else {
        result = SimpleStateMachine_ExecuteState (mqttSubstateHandler, MQTT_SUBSTATE_INIT);
        if (result != SMP_STATE_MACHINE_RET_SUCCESS) {
            error (
                "Fail to ExecuteState (MQTT_SUBSTATE_INIT), (MqttManager.c, %d)\r\n",
                __LINE__
            );
        }
    }

fail :

    return result;
}


MQTTPacket_connectData* _MqttManager_GetConnectData (
    uint32_t userId,
    uint32_t gatewayId
) {
    MQTTPacket_connectData* connectData = NULL;
    dxDEV_INFO_RET_CODE devResult = DXDEV_INFO_SUCCESS;
    const WifiCommDeviceMac_t* wifiMac = WifiCommManagerGetDeviceMac ();
    int usernameLength = 0;
    int passwordLength = 128;
    int length;
    char mac[21] = {0};
    char* username;
    char* password;
    char* clientId;
    char* willTopic;
    char* willMessage;

    connectData = dxMemAlloc (
        "",
        1,
        sizeof (MQTTPacket_connectData)
    );
    if (connectData == NULL) {
        goto fail_alloc_connect;
    }

    memset (connectData, 0x00, sizeof (connectData));
    connectData->MQTTVersion = DXMQTT_VERSION;
    connectData->keepAliveInterval = DXMQTT_KEEP_ALIVE_INTERVAL;    // seconds
    connectData->commandTimeout = DXMQTT_COMMAND_TIMEOUT;

    usernameLength = _MACAddrToDecimalStr (
        mac,
        sizeof (mac),
        (uint8_t*) wifiMac->MacAddr,
        sizeof (wifiMac->MacAddr)
    );
    if (usernameLength <= 0) {
        goto fail_alloc_connect;
    }
    username = dxMemAlloc (
        "",
        1,
        usernameLength + 5
    );
    if (username == NULL) {
        goto fail_alloc_username;
    }

    sprintf (username, "$MAC$%s", mac);
    memset (&connectData->username, 0x00, sizeof (MQTTString));
    connectData->username.cstring = username;

    password = dxMemAlloc (
        "",
        1,
        passwordLength
    );
    if (password == NULL) {
        goto fail_alloc_password;
    }

    devResult = dxDevInfo_GetData (
        DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY,
        password,
        &passwordLength,
        dxFALSE
    );
    if (devResult != DXDEV_INFO_SUCCESS) {
        goto fail_alloc_clientid;
    }

    trace ("password=%s\r\n", password);

    memset (&connectData->password, 0x00, sizeof (MQTTString));
    connectData->password.cstring = password;

    clientId = dxMemAlloc (
        "",
        1,
        usernameLength
    );
    if (clientId == NULL) {
        goto fail_alloc_clientid;
    }

    sprintf (clientId, "%s", mac);

    memset (&connectData->clientId, 0x00, sizeof (MQTTString));
    connectData->clientId.cstring = clientId;

    length = strlen (DXMQTT_WILL_TOPIC_FORMAT) +
          dxNumOfDigits (userId) +
          dxNumOfDigits (gatewayId);
    willTopic = (char*) dxMemAlloc (
        DXMQTT_THREAD_NAME,
        1,
        length
    );
    if (willTopic == NULL) {
        goto fail_alloc_topic;
    }
    sprintf (willTopic, DXMQTT_WILL_TOPIC_FORMAT, userId, gatewayId);

    length = strlen (DXMQTT_WILL_MESSAGE_FORMAT) + dxNumOfDigits (gatewayId);
    willMessage = (char*) dxMemAlloc (
        DXMQTT_THREAD_NAME,
        1,
        length
    );
    if (willMessage == NULL) {
        goto fail_alloc_message;
    }
    sprintf (willMessage, DXMQTT_WILL_MESSAGE_FORMAT, gatewayId);

    connectData->willFlag = 1;
    memset (&connectData->will.topicName, 0x00, sizeof (MQTTString));
    connectData->will.topicName.cstring = willTopic;
    memset (&connectData->will.message, 0x00, sizeof (MQTTString));
    connectData->will.message.cstring = willMessage;
    connectData->will.qos = MqttQos_QOS2;
    connectData->will.retained = 1;

    return connectData;

fail_alloc_message :

    if (willTopic != NULL) {
        dxMemFree (willTopic);
        willTopic = NULL;
    }

fail_alloc_topic :

    if (clientId != NULL) {
        dxMemFree (clientId);
        clientId = NULL;
    }

fail_alloc_clientid :

    if (password != NULL) {
        dxMemFree (password);
        password = NULL;
    }

fail_alloc_password :

    if (username != NULL) {
        dxMemFree (username);
        username = NULL;
    }

fail_alloc_username :

    if (connectData != NULL) {
        dxMemFree (connectData);
        connectData = NULL;
    }

fail_alloc_connect :

    return NULL;
}


dxIP_Addr_t* _MqttManager_GetHostAddress (void) {

    dxIP_Addr_t* ip;
    dxIPv4_t ip4;
    uint8_t* hostname = mqttAccounts[activeIndex].HostName;

    ip = dxMemAlloc (
        DXMQTT_THREAD_NAME,
        1,
        sizeof (dxIP_Addr_t)
    );
    if (ip == NULL) {
        return NULL;
    }

    if (dxDNS_GetHostByName (hostname, &ip4) != DXNET_SUCCESS) {
        goto fail;
    }

    ip->version = DXIP_VERSION_V4;
    memcpy (&ip->v4[0], &ip4.ip_addr[0], sizeof (ip->v4));

    return ip;

fail :

    if (ip != NULL) {
        dxFree (ip);
        ip = NULL;
    }

    return NULL;
}


MqttResult _MqttManager_Parse (
    MqttEvent* event,
    uint8_t* data
) {
    MqttResult mqttResult = MqttResult_SUCCESS;
    dxPTR_t handle = 0;
    uint32_t activeUser;
    int findList1[] = {DKJSN_TOKEN_EVENT_FUNC, NULL};
    int findList2[] = {DKJSN_TOKEN_EVENT_TIME, NULL};
    int findList3[] = {DKJSN_TOKEN_EVENT_ARGS, NULL};
    int findList4[] = {DKJSN_TOKEN_EVENT_NAME, NULL};
    int findList5[] = {DKJSN_TOKEN_EVENT_VALUE, NULL};
    int rootNode = 0;
    int argsNode = 0;
    int ret = 0;
    int len = 0;
    uint8_t buffer[21] = {0};

    // Check input parameters
    if (event == NULL || data == NULL) {
        error (
            "Invalid parameters, (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail_dkjsn_init;
    }

    // Init json parser
    handle = DKJSN_Init ((char*) data, tokenTable, MAX_NUM_JSON_ELEMENT_SUPPORT);
    if (handle == 0) {
        error (
            "Fail to call DKJSN_Init (), (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail_dkjsn_init;
    }

    // Check first value
    len = DKJSN_GetObjectStringLengthByList (handle, findList1, rootNode);
    if (len == 0) {
        error (
            "Fail to call DKJSN_GetObjectStringLengthByList (), (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }
    memset (buffer, 0x00, sizeof (buffer));
    memcpy (buffer, DKJSN_GetObjectStringByList (handle, findList1, rootNode), len);
    if (memcmp (buffer, "DKEvent", len) != 0) {
        error (
            "Value of \"func\" is wrong, (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }

    // Get time
    len = DKJSN_GetObjectStringLengthByList (handle, findList2, rootNode);
    if (len == 0) {
        error (
            "Fail to call DKJSN_GetObjectStringLengthByList (), (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }
    memset (buffer, 0x00, sizeof (buffer));
    memcpy (buffer, DKJSN_GetObjectStringByList (handle, findList2, rootNode), len);
    event->time = strtoul (buffer, NULL, 10);

    // Find argsNode
    argsNode = DKJSN_SelectObjectByList (handle, findList3, rootNode);
    if (argsNode < 0) {
        error (
            "Fail to find argsNode, (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }

    // Get name
    len = DKJSN_GetObjectStringLengthByList (handle, findList4, argsNode);
    if (len == 0) {
        error (
            "Fail to call DKJSN_GetObjectStringLengthByList (), (MqttManager.c, %d)\r\n",
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }
    memset (buffer, 0x00, sizeof (buffer));
    memcpy (buffer, DKJSN_GetObjectStringByList (handle, findList4, argsNode), len);

    // Get data according to different event type
    if (memcmp (buffer, "Job", len) == 0) {
        event->eventId = DKMSG_NEWJOBS;
        event->dataLen = 0;
        event->data = NULL;
    } else if (memcmp (buffer, "DeviceStatus", len) == 0) {
        event->eventId = DKMSG_DEVICESTATUS;
        event->dataLen = 0;
        event->data = NULL;
    } else if (memcmp (buffer, "NumberOfActiveUser", len) == 0) {
        len = DKJSN_GetObjectStringLengthByList (handle, findList5, argsNode);
        if (len == 0) {
            error (
                "Fail to call DKJSN_GetObjectStringLengthByList (), (MqttManager.c, %d)\r\n",
                __LINE__
            );
            mqttResult = MqttResult_JSON_PARSE_ERROR;
            goto fail;
        }
        memset (buffer, 0x00, sizeof (buffer));
        memcpy (buffer, DKJSN_GetObjectStringByList (handle, findList5, argsNode), len);

        event->eventId = DKMSG_NUMOFACTIVEUSERS;
        event->dataLen = sizeof (uint32_t);
        event->data = dxMemAlloc (
            DXMQTT_THREAD_WORKER_THREAD_NAME,
            1,
            sizeof (uint32_t)
        );
        if (event->data == NULL) {
            error (
                "Fail to allocate memory for data, (MqttManager.c, %d)\r\n",
                __LINE__
            );
            mqttResult = MqttResult_JSON_PARSE_ERROR;
            goto fail;
        }

        uint32_t activeUser = strtoul (buffer, NULL, 10);
        memcpy (event->data, &activeUser, sizeof (activeUser));
    } else {
        error (
            "Unknown event=%s, (MqttManager.c, %d)\r\n",
            buffer,
            __LINE__
        );
        mqttResult = MqttResult_JSON_PARSE_ERROR;
        goto fail;
    }

    DKJSN_Uninit (handle);

    return MqttResult_SUCCESS;

fail :

    DKJSN_Uninit (handle);
    handle = 0;

fail_dkjsn_init :

    return mqttResult;
}


void _MqttManager_Dump (
    const char* prefix,
    char* p,
    int len
) {
    char buffer[64];
    int i = 0;
    int n = 0;
    int buffersize = (sizeof (buffer) < MAX_DUMP_BUFFER_SIZE) ? sizeof (buffer) : MAX_DUMP_BUFFER_SIZE;
    int datalen = buffersize - 1;               // Reserved 1 byte for NULL character

    if (len <= 0) return ;

    n = (len + datalen - 1) / datalen;

    GSysDebugger_LogNIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "%s: ", prefix);

    for (i = 0; i < n; i++) {
        memset (buffer, 0x00, sizeof (buffer));
        memcpy (
            buffer,
            &p[datalen * i],
            (len - datalen * i < datalen) ? len - datalen * i : datalen
        );

        GSysDebugger_LogNIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, buffer);
    }

    GSysDebugger_LogNIV (G_SYS_DBG_CATEGORY_MQTTMANAGER, G_SYS_DBG_LEVEL_FATAL_ERROR, "\r\n");
}
