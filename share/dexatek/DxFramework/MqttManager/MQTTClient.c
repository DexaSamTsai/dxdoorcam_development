/*******************************************************************************
 * Copyright (c) 2014, 2015 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Allan Stockdill-Mander/Ian Craggs - initial API and implementation and/or initial documentation
 *******************************************************************************/
#include "MQTTClient.h"



const char * const msg_types_str[] = {
    "Reserved",
    "CONNECT",
    "CONNACK",
    "PUBLISH",
    "PUBACK",
    "PUBREC",
    "PUBREL",
    "PUBCOMP",
    "SUBSCRIBE",
    "SUBACK",
    "UNSUBSCRIBE",
    "UNSUBACK",
    "PINGREQ",
    "PINGRESP",
    "DISCONNECT",
    "Reserved"
};

const char * const mqtt_status_str[] = {
    "MQTT_START",
    "MQTT_CONNECT",
    "MQTT_SUBTOPIC",
    "MQTT_RUNNING"
};



static void NewMessageData (
    MessageData* md,
    MQTTString* aTopicName,
    MQTTMessage* aMessage
) {
    md->topicName = aTopicName;
    md->message = aMessage;
}


static int getNextPacketId (
    MQTTClient *c
) {
    return c->next_packetid = (c->next_packetid == MAX_PACKET_ID) ? 1 : c->next_packetid + 1;
}


static int sendPacket (
    MQTTClient* c,
    int length,
    Timer* timer
) {
    int rc = MQTT_FAILURE,
        sent = 0;

    while (sent < length && !TimerIsExpired (timer)) {
        rc = c->ipstack->_write (c->ipstack, &c->buf[sent], length, TimerLeftMS (timer));
        if (rc < 0)  // there was an error writing the data
            break;
        sent += rc;
    }

    if (sent == length) {
        TimerCountdown (&c->keepAliveTimer, c->keepAliveInterval); // record the fact that we have successfully sent the packet
        rc = MQTT_SUCCESS;
    } else {
        rc = MQTT_FAILURE;
        mqtt_printf (MQTT_DEBUG, "Send packet failed\r\n");
    }
    return rc;
}


void MQTTClientInit (
    MQTTClient* c,
    Network* network,
    unsigned int keepAliveInterval,
    unsigned int commandTimeout,
    unsigned char* sendbuf,
    size_t sendbuf_size,
    unsigned char* readbuf,
    size_t readbuf_size
) {
    int i;
    c->ipstack = network;

    for (i = 0; i < MAX_MESSAGE_HANDLERS; ++i)
        c->messageHandlers[i].topicFilter = 0;
    c->keepAliveInterval = keepAliveInterval;
    c->commandTimeout = commandTimeout;
    c->buf = sendbuf;
    c->buf_size = sendbuf_size;
    c->readbuf = readbuf;
    c->readbuf_size = readbuf_size;
    c->isconnected = 0;
    c->ping_outstanding = 0;
    c->defaultMessageHandler = NULL;
    c->next_packetid = 1;
}


static int decodePacket (
    MQTTClient* c,
    int* value,
    int timeout
) {
    unsigned char i;
    int multiplier = 1;
    int len = 0;
    const int MAX_NO_OF_REMAINING_LENGTH_BYTES = 4;

    *value = 0;

    do {
        int rc = MQTTPACKET_READ_ERROR;

        if (++len > MAX_NO_OF_REMAINING_LENGTH_BYTES) {
            rc = MQTTPACKET_READ_ERROR; /* bad data */
            goto exit;
        }
        rc = c->ipstack->_read (c->ipstack, &i, 1, timeout);
        if (rc != 1) {
            mqtt_printf (MQTT_ERROR, "read packet failed\r\n");
            goto exit;
        }
        *value += (i & 127) * multiplier;
        multiplier *= 128;
    } while ((i & 128) != 0);

exit :

    return len;
}


static int readPacket (
    MQTTClient* c,
    Timer* timer
) {
    int rc = MQTT_FAILURE;
    MQTTHeader header = {0};
    int len = 0;
    int rem_len = 0;

    /* 1. read the header byte.  This has the packet type in it */
    if (c->ipstack->_read (c->ipstack, c->readbuf, 1, TimerLeftMS (timer)) != 1) {
        mqtt_printf (MQTT_ERROR, "read packet header failed\r\n");
        goto exit;
    }
    len = 1;
    /* 2. read the remaining length.  This is variable in itself */
    decodePacket (c, &rem_len, TimerLeftMS (timer));
    len += MQTTPacket_encode (c->readbuf + 1, rem_len); /* put the original remaining length back into the buffer */

    if (len + rem_len > c->readbuf_size) {
        mqtt_printf (MQTT_WARNING, "rem_len = %d, read buffer will overflow\r\n", rem_len);
        rc = MQTT_OVERFLOW;
        goto exit;
    }
    /* 3. read the rest of the buffer using a callback to supply the rest of the data */
    if (
        (rem_len > 0) &&
        (c->ipstack->_read (c->ipstack, c->readbuf + len, rem_len, TimerLeftMS (timer)) != rem_len)
    ) {
        mqtt_printf (MQTT_ERROR, "read the rest of the data failed\r\n");
        goto exit;
    }
    header.byte = c->readbuf[0];
    rc = header.bits.type;

exit :

    return rc;
}


// assume topic filter and name is in correct format
// # can only be at end
// + and # can only be next to separator
static char isTopicMatched (
    char* topicFilter,
    MQTTString* topicName
) {
    char* curf = topicFilter;
    char* curn = topicName->lenstring.data;
    char* curn_end = curn + topicName->lenstring.len;

    while (*curf && curn < curn_end) {
        if (*curn == '/' && *curf != '/') {
            break;
        }
        if (*curf != '+' && *curf != '#' && *curf != *curn) {
            break;
        }
        if (*curf == '+') {   // skip until we meet the next separator, or end of string
            char* nextpos = curn + 1;
            while (nextpos < curn_end && *nextpos != '/')
                nextpos = ++curn + 1;
        } else if (*curf == '#') {
            curn = curn_end - 1;    // skip until end of string
        }
        curf++;
        curn++;
    };

    return (curn == curn_end) && (*curf == '\0');
}


int deliverMessage (
    MQTTClient* c,
    MQTTString* topicName,
    MQTTMessage* message
) {
    int i;
    int rc = MQTT_FAILURE;

    // we have to find the right message handler - indexed by topic
    for (i = 0; i < MAX_MESSAGE_HANDLERS; ++i) {
        if (
            c->messageHandlers[i].topicFilter != 0 &&
            (
                MQTTPacket_equals (topicName, (char*)c->messageHandlers[i].topicFilter) ||
                isTopicMatched ((char*)c->messageHandlers[i].topicFilter, topicName)
            )
        ) {
            if (c->messageHandlers[i].handler != NULL) {
                MessageData md;
                NewMessageData (&md, topicName, message);
                c->messageHandlers[i].handler (&md);
                rc = MQTT_SUCCESS;
            }
        }
    }

    if (rc == MQTT_FAILURE && c->defaultMessageHandler != NULL) {
        MessageData md;
        NewMessageData (&md, topicName, message);
        c->defaultMessageHandler (&md);
        rc = MQTT_SUCCESS;
    }

    return rc;
}


int keepalive (
    MQTTClient* c
) {
    int rc = MQTT_FAILURE;

    if (c->keepAliveInterval == 0) {
        rc = MQTT_SUCCESS;
        goto exit;
    }

    if (TimerIsExpired (&c->keepAliveTimer)) {
        if (!c->ping_outstanding) {
            TimerInit (&c->commandTimer);
            TimerCountdown (&c->commandTimer, c->commandTimeout);
            int len = MQTTSerialize_pingreq (c->buf, c->buf_size);
            if (len > 0 && (rc = sendPacket (c, len, &c->commandTimer)) == MQTT_SUCCESS) // send the ping packet
                c->ping_outstanding = 1;
            mqtt_printf (MQTT_DEBUG, "MQTT send keepalive PINGREQ\r\n");
        }
    }

exit :

    return rc;
}


int cycle (
    MQTTClient* c,
    Timer* timer
) {
    // read the socket, see what work is due
    unsigned short packet_type = readPacket (c, timer);

    int len = 0,
        rc = MQTT_SUCCESS;

    if (packet_type < sizeof (msg_types_str) / sizeof (msg_types_str[0]))
    {
        mqtt_printf (MQTT_DEBUG, "Read packet type: %d\r\n", packet_type);
    }
    switch (packet_type)
    {
        case CONNACK:
        case PUBACK:
        case SUBACK:
            break;
        case PUBLISH:
        {
            MQTTString topicName;
            MQTTMessage msg;
            int intQoS;
            if (MQTTDeserialize_publish (&msg.dup, &intQoS, &msg.retained, &msg.id, &topicName,
               (unsigned char**)&msg.payload, (int*)&msg.payloadlen, c->readbuf, c->readbuf_size) != 1)
                goto exit;
            msg.qos = (enum QoS)intQoS;
            deliverMessage (c, &topicName, &msg);
            if (msg.qos != QOS0)
            {
                if (msg.qos == QOS1)
                    len = MQTTSerialize_ack (c->buf, c->buf_size, PUBACK, 0, msg.id);
                else if (msg.qos == QOS2)
                    len = MQTTSerialize_ack (c->buf, c->buf_size, PUBREC, 0, msg.id);
                if (len <= 0)
                    rc = MQTT_FAILURE;
                else
                    rc = sendPacket (c, len, timer);
                if (rc == MQTT_FAILURE)
                    goto exit; // there was a problem
            }
            break;
        }
        case PUBREC:
        {
            unsigned short mypacketid;
            unsigned char dup, type;
            if (MQTTDeserialize_ack (&type, &dup, &mypacketid, c->readbuf, c->readbuf_size) != 1)
                rc = MQTT_FAILURE;
            else if ((len = MQTTSerialize_ack (c->buf, c->buf_size, PUBREL, 0, mypacketid)) <= 0)
                rc = MQTT_FAILURE;
            else if ((rc = sendPacket (c, len, timer)) != MQTT_SUCCESS) // send the PUBREL packet
                rc = MQTT_FAILURE; // there was a problem
            if (rc == MQTT_FAILURE)
                goto exit; // there was a problem
            break;
        }
        case PUBREL:
        {
            unsigned short mypacketid;
            unsigned char dup, type;
            if (MQTTDeserialize_ack (&type, &dup, &mypacketid, c->readbuf, c->readbuf_size) != 1)
                rc = MQTT_FAILURE;
            else if ((len = MQTTSerialize_ack (c->buf, c->buf_size, PUBCOMP, 0, mypacketid)) <= 0)
                rc = MQTT_FAILURE;
            else if ((rc = sendPacket (c, len, timer)) != MQTT_SUCCESS) // send the PUBREL packet
                rc = MQTT_FAILURE; // there was a problem
            if (rc == MQTT_FAILURE)
                goto exit; // there was a problem
            break;
        }
        case PUBCOMP:
            break;
        case PINGRESP:
            c->ping_outstanding = 0;
            mqtt_printf (MQTT_DEBUG, "MQTT received keepalive PINGRES (cycle)\r\n");
            break;
    }

exit :

    if (rc == MQTT_SUCCESS)
        rc = packet_type;
    return rc;
}


int waitfor (
    MQTTClient* c,
    int packet_type,
    Timer* timer
) {
    int rc = MQTT_FAILURE;

    do {
        if (TimerIsExpired (timer))
            break; // we timed out
    } while ((rc = cycle (c, timer)) != packet_type);

    return rc;
}


int MQTTConnect (
    MQTTClient* c,
    MQTTPacket_connectData* options
) {
    Timer connect_timer;
    int rc = MQTT_FAILURE;
    MQTTPacket_connectData default_options = MQTTPacket_connectData_initializer;
    int len = 0;

    /* don't send connect packet again if we are already connected */
    if (c->isconnected) {
        mqtt_printf (MQTT_WARNING, "Already connected!\n\r");
        goto exit;
    }

    if ((rc = c->ipstack->_open (c->ipstack)) != 0) {
        mqtt_printf (MQTT_INFO, "Return code from network connect is %d\n", rc);
        goto exit;
    }

    TimerInit (&c->commandTimer);
    TimerCountdown (&c->commandTimer, c->commandTimeout);

    TimerInit (&c->keepAliveTimer);
    TimerCountdown (&c->keepAliveTimer, c->keepAliveInterval);

    if (options == 0) {
        options = &default_options; /* set default options if none were supplied */
    }

    if ((len = MQTTSerialize_connect (c->buf, c->buf_size, options)) <= 0) {
        mqtt_printf (MQTT_WARNING, "Fail to call MQTTSerialize_connect (): len=%d\r\n", len);
        goto exit;
    }
    if ((rc = sendPacket (c, len, &c->commandTimer)) != MQTT_SUCCESS) { // send the connect packet
        mqtt_printf (MQTT_WARNING, "Fail to call sendPacket ()\r\n");
        goto exit; // there was a problem
    }

    // this will be a blocking call, wait for the connack
    if (waitfor (c, CONNACK, &c->commandTimer) == CONNACK) {
        unsigned char connack_rc = 255;
        unsigned char sessionPresent = 0;
        if (MQTTDeserialize_connack (&sessionPresent, &connack_rc, c->readbuf, c->readbuf_size) == 1)
            rc = connack_rc;
        else
            rc = MQTT_FAILURE;
    }
    else{
        mqtt_printf (MQTT_DEBUG, "Not received CONNACK\r\n");
        rc = MQTT_FAILURE;
    }

exit :

    if (rc == MQTT_SUCCESS) {
        c->isconnected = 1;
    }

    return rc;
}


int MQTTSubscribe (
    MQTTClient* c,
    const char* topicFilter,
    enum QoS qos,
    MessageHandler messageHandler
) {
    int rc = MQTT_FAILURE;
    int len = 0;
    MQTTString topic = MQTTString_initializer;
    topic.cstring = (char *)topicFilter;

    if (!c->isconnected)
        goto exit;

    TimerInit (&c->commandTimer);
    TimerCountdown (&c->commandTimer, c->commandTimeout);

    len = MQTTSerialize_subscribe (c->buf, c->buf_size, 0, getNextPacketId (c), 1, &topic, (int*)&qos);
    if (len <= 0)
        goto exit;
    if ((rc = sendPacket (c, len, &c->commandTimer)) != MQTT_SUCCESS) // send the subscribe packet
        goto exit;             // there was a problem

    if (waitfor (c, SUBACK, &c->commandTimer) == SUBACK)      // wait for suback
    {
        int count = 0, grantedQoS = -1;
        unsigned short mypacketid;
        if (MQTTDeserialize_suback (&mypacketid, 1, &count, &grantedQoS, c->readbuf, c->readbuf_size) == 1)
            rc = grantedQoS; // 0, 1, 2 or 0x80
        if (rc != 0x80)
        {
            int i;
            for (i = 0; i < MAX_MESSAGE_HANDLERS; ++i)
            {
                if (c->messageHandlers[i].topicFilter == topicFilter)
                {
                    rc = 0;
                    goto exit;  //already subscribed
                }
            }
            for (i = 0; i < MAX_MESSAGE_HANDLERS; ++i)
            {
                if (c->messageHandlers[i].topicFilter == 0)
                {
                    c->messageHandlers[i].topicFilter = topicFilter;
                    c->messageHandlers[i].handler = messageHandler;
                    rc = 0;
                    break;
                }
            }
        }
    }
    else
        rc = MQTT_FAILURE;

exit :

    return rc;
}


int MQTTUnsubscribe (
    MQTTClient* c,
    const char* topicFilter
) {
    int rc = MQTT_FAILURE;
    MQTTString topic = MQTTString_initializer;
    topic.cstring = (char *)topicFilter;
    int len = 0;

    if (!c->isconnected)
        goto exit;

    TimerInit (&c->commandTimer);
    TimerCountdown (&c->commandTimer, c->commandTimeout);

    if ((len = MQTTSerialize_unsubscribe (c->buf, c->buf_size, 0, getNextPacketId (c), 1, &topic)) <= 0)
        goto exit;
    if ((rc = sendPacket (c, len, &c->commandTimer)) != MQTT_SUCCESS) // send the subscribe packet
        goto exit; // there was a problem

    if (waitfor (c, UNSUBACK, &c->commandTimer) == UNSUBACK)
    {
        unsigned short mypacketid;  // should be the same as the packetid above
        if (MQTTDeserialize_unsuback (&mypacketid, c->readbuf, c->readbuf_size) == 1)
            rc = 0;
            int i;
            for (i = 0; i < MAX_MESSAGE_HANDLERS; ++i)
            {
                if (c->messageHandlers[i].topicFilter == topicFilter)
                {
                    c->messageHandlers[i].topicFilter = 0;
                    c->messageHandlers[i].handler = NULL;
                }
            }
    }
    else
        rc = MQTT_FAILURE;

exit :

    return rc;
}


int MQTTPublish (
    MQTTClient* c,
    const char* topicName,
    MQTTMessage* message
) {
    int rc = MQTT_FAILURE;
    MQTTString topic = MQTTString_initializer;
    topic.cstring = (char *)topicName;
    int len = 0;

    if (!c->isconnected)
        goto exit;

    TimerInit (&c->commandTimer);
    TimerCountdown (&c->commandTimer, c->commandTimeout);

    if (message->qos == QOS1 || message->qos == QOS2)
        message->id = getNextPacketId (c);

    len = MQTTSerialize_publish (c->buf, c->buf_size, 0, message->qos, message->retained, message->id,
              topic, (unsigned char*)message->payload, message->payloadlen);
    if (len <= 0)
        goto exit;
    if ((rc = sendPacket (c, len, &c->commandTimer)) != MQTT_SUCCESS) // send the subscribe packet
        goto exit; // there was a problem

    if (message->qos == QOS1)
    {
        if (waitfor (c, PUBACK, &c->commandTimer) == PUBACK)
        {
            unsigned short mypacketid;
            unsigned char dup, type;
            if (MQTTDeserialize_ack (&type, &dup, &mypacketid, c->readbuf, c->readbuf_size) != 1)
                rc = MQTT_FAILURE;
        }
        else{
            rc = MQTT_FAILURE;
            mqtt_printf (MQTT_DEBUG, "Not received PUBACK\r\n");
        }
    }
    else if (message->qos == QOS2)
    {
        if (waitfor (c, PUBCOMP, &c->commandTimer) == PUBCOMP)
        {
            unsigned short mypacketid;
            unsigned char dup, type;
            if (MQTTDeserialize_ack (&type, &dup, &mypacketid, c->readbuf, c->readbuf_size) != 1)
                rc = MQTT_FAILURE;
        }
        else{
            rc = MQTT_FAILURE;
            mqtt_printf (MQTT_DEBUG, "Not received PUBCOMP\r\n");
        }
    }

exit :

    return rc;
}


int MQTTCycle (
    MQTTClient* c
) {
    TimerInit (&c->commandTimer);
    TimerCountdown (&c->commandTimer, c->commandTimeout);
    cycle (c, &c->commandTimer);
}


int MQTTKeepAlive (
    MQTTClient* c
) {
    keepalive (c);
}


int MQTTYield (
    MQTTClient* c,
    int timeout_ms
) {
    int rc = MQTT_SUCCESS;
    Timer timer;

    TimerInit (&timer);
    TimerCountdownMS (&timer, timeout_ms);

    do {
        if (cycle (c, &timer) == MQTT_FAILURE) {
            rc = MQTT_FAILURE;
            break;
        }
    } while (!TimerIsExpired (&timer));

    return rc;
}


int MQTTDisconnect (
    MQTTClient* c
) {
    int rc = MQTT_FAILURE;
    int len = 0;

    if (c->isconnected) {
        mqtt_printf (MQTT_DEBUG, "MQTTDisconnect\r\n");
        TimerInit (&c->commandTimer);
        TimerCountdown (&c->commandTimer, c->commandTimeout);

        len = MQTTSerialize_disconnect (c->buf, c->buf_size);
        if (len > 0)
            rc = sendPacket (c, len, &c->commandTimer);            // send the disconnect packet

        if (c->ipstack) {
            c->ipstack->_close (c->ipstack); // Force disconnect SSL if exist
        }

        c->isconnected = 0;
    }

    return rc;
}
