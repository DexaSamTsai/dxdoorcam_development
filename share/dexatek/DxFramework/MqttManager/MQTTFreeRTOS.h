/*******************************************************************************
 * Copyright (c) 2014, 2015 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 *
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Allan Stockdill-Mander - initial API and implementation and/or initial documentation
 *******************************************************************************/

#if !defined(MQTTFreeRTOS_H)
#define MQTTFreeRTOS_H

#include <stdio.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "polarssl/ssl.h"

#include "dxOs.h"
#include "dxSysdebugger.h"


/**
 * @brief enum
 */
enum {
    MQTT_EXCESSIVE,
    MQTT_MSGDUMP,
    MQTT_DEBUG,
    MQTT_INFO,
    MQTT_ALWAYS,
    MQTT_WARNING,
    MQTT_ERROR
};


/**
 * @brief struct Network
 */
typedef struct _Network {
    int (*_open) (struct _Network*);
    int (*_read) (struct _Network*, unsigned char*, int, int);
    int (*_write) (struct _Network*, unsigned char*, int, int);
    void (*_close) (struct _Network*);
    int (*_select) (struct _Network*);

    dxSSLHandle_t* handle;
    dxIP_Addr_t* address;
    unsigned short port;
} Network;


/**
 * @brief struct Timer
 */
typedef struct Timer {
    TickType_t xTicksToWait;
    TimeOut_t xTimeOut;
} Timer;


/**
 * @brief Network_Init
 */
void NetworkInit (
    Network* n,
    dxIP_Addr_t* address,
    uint16_t port
);


/**
 * @brief TimerInit
 */
void TimerInit (Timer*);


/**
 * @brief TimerIsExpired
 */
char TimerIsExpired (Timer*);


/**
 * @brief TimerCountdownMS
 */
void TimerCountdownMS (Timer*, unsigned int);


/**
 * @brief TimerCountdown
 */
void TimerCountdown (Timer*, unsigned int);


/**
 * @brief TimerLeftMS
 */
int TimerLeftMS (Timer*);


/**
 * @brief mqtt_printf
 */
void mqtt_printf (
    unsigned char level,
    const char *fmt,
    ...
);

#endif
