//============================================================================
// File: MqttManager.h
//
// Author: Charles Tsai
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//
// Version: 0.1
// Date: 2017/12/08
// 1) Description: Initial
//============================================================================

#ifndef __MQTT_MANAGER_H__
#define __MQTT_MANAGER_H__

#pragma once

//============================================================================
// Includes
//============================================================================

#include "dxOS.h"
#include "dxBSP.h"



#ifdef __cplusplus
extern "C" {
#endif

//============================================================================
// Definations
//============================================================================

#define DXMQTT_HOSTNAME_MAX_LEN                     128
#define DXMQTT_USERNAME_MAX_LEN                     32
#define DXMQTT_PASSWORD_MAX_LEN                     64
#define DXMQTT_CLIENTID_MAX_LEN                     24
#define DXMQTT_SUBSCRIBE_TOPIC_MAX_LEN              128
#define DXMQTT_DATA_MAX_LEN                         1024



//============================================================================
// Types
//============================================================================

/**
 * @brief enum MqttResult
 */
typedef enum {
    MqttResult_SUCCESS            = 0,
    MqttResult_ERROR              = -1,
    MqttResult_INVALID_PARAMETER  = -2,
    MqttResult_BUFFER_TOO_SMALL   = -3,
    MqttResult_TIMEOUT            = -4,
    MqttResult_NOT_ENOUGH_MEMORY  = -5,
    MqttResult_JSON_PARSE_ERROR   = -6,
    MqttResult_FALLBACK           = -7,
    MqttResult_WRONG_STATE        = -8,

} MqttResult;


/**
 * @brief enum MqttState
 */
typedef enum {
    MqttState_INIT       = 0,
    MqttState_CONNECT    = 2,
    MqttState_SUBSCRIBE  = 3,
    MqttState_RUNNING    = 4,
    MqttState_FALLBACK   = 5,
    MqttState_DISCONNECT = 6,

    MqttResult_NUM
} MqttState;


/**
 * @brief enum MqttQos
 */
typedef enum {
    MqttQos_QOS0 = 0,
    MqttQos_QOS1 = 1,
    MqttQos_QOS2 = 2
} MqttQos;


/**
 * @brief enum dxMQTT_Command_t
 */
typedef enum {
    // Event direction from dxMQTT
    MqttMessageType_NORMAL      = 0,
    MqttMessageType_ERROR       = 1,

    MqttMessageType_NUM
} MqttMessageType;


/**
 * @brief struct MqttWorker
 */
typedef struct _MqttWorker {
    dxWorkweTask_t*     handle;
    dxEventHandler_t    callback;
} MqttWorker;


/**
 * @brief struct MqttHeader
 */
typedef struct _MqttHeader {
    MqttMessageType messageType;
    int16_t         dataLen;
} MqttHeader;


/**
 * @brief struct MqttEventReport
 */
typedef struct _MqttEventReport {
    MqttHeader      header;
    int16_t         errorCode;
    uint8_t*        data;
} MqttEventReport;


/**
 * @brief struct MqttTopicName
 */
typedef struct _MqttTopicName {
    uint32_t    Length;
    uint8_t     pData[DXMQTT_SUBSCRIBE_TOPIC_MAX_LEN];
} MqttTopicName;


/**
 * @brief struct MqttTopicContent
 */
typedef struct _MqttTopicContent {
    MqttQos     QoS;
    uint8_t     Retained;
    uint8_t     Dup;
    uint16_t    ID;
    uint32_t    Length;
    uint8_t     pData[DXMQTT_DATA_MAX_LEN];
} MqttTopicContent;


/**
 * @brief struct MqttTopic
 */
typedef struct _MqttTopic {
    MqttTopicName    TopicName;
    MqttTopicContent TopicContent;
} MqttTopic;


/**
 * @brief struct MqttEvent
 */
typedef struct _MqttEvent {
    uint32_t        time;
    uint16_t        eventId;            // The EventID value should be the same as DKMSG_Command_t in DKServerManager.h
    uint16_t        dataLen;            // Length of pData
    uint8_t*        data;             // pData should be NULL if EventID == DKMSG_NEWJOBS, for example
} MqttEvent;



//============================================================================
// Public Methods
//============================================================================

/**
 * @brief MqttManager_Init
 */
MqttResult MqttManager_Init (
    dxEventHandler_t eventFunction
);


/**
 * @brief MqttManager_Connect_Async
 */
MqttResult MqttManager_Connect_Async (void);


/**
 * @brief MqttManager_Publish_Async
 */
MqttResult MqttManager_Publish_Async (
    uint8_t* topicName,
    uint8_t* topicContent
);


/**
 * @brief MqttManager_Disconnect_Async
 */
MqttResult MqttManager_Disconnect_Async (void);


/**
 * @brief MqttManager_GetEventReportHeader
 */
MqttHeader MqttManager_GetEventReportHeader (
    void* argument
);


/**
 * @brief MqttManager_GetEventReportPayload
 */
uint8_t* MqttManager_GetEventReportPayload (
    void* argument
);


/**
 * @brief MqttManager_CleanEventReport
 */
MqttResult MqttManager_CleanEventReport (
    void* argument
);


/**
 * @brief MqttManager_CleanEvent
 */
MqttResult MqttManager_CleanEvent (
    void* argument
);


#ifdef __cplusplus
}
#endif

#endif // _DXMQTT_H
