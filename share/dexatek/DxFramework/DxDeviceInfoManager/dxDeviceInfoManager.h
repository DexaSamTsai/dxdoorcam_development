//============================================================================
// File: dxDeviceInfoManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/02/18
//     1) Description: Initial
//
//============================================================================
#ifndef _DXDEVICEINFOMANAGER_H
#define _DXDEVICEINFOMANAGER_H

//============================================================================
// Include files
//============================================================================

//============================================================================
// Enumeration
//============================================================================

//WARNING: DO NOT CHANGE the value for already defined type (Will cause backward compability issue!)
typedef enum
{
    DX_DEVICE_INFO_TYPE_UNKNOWN =                       0,
    DX_DEVICE_INFO_TYPE_VERSION =                       1,      //uint16_t - Internal Use
    DX_DEVICE_INFO_TYPE_CRC16 =                         2,      //uint32_t - Internal use
    DX_DEVICE_INFO_TYPE_DEV_ACCESS_KEY =                5,      //NULL Terminated String
    DX_DEVICE_INFO_TYPE_DEV_NAME =                      10,     //NULL Terminated String
    DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LATITUDE =          15,     //float
    DX_DEVICE_INFO_TYPE_DEV_GEO_LOC_LONGITUDE =         16,     //float
    DX_DEVICE_INFO_TYPE_WIFI_CONN_SSID =                20,     //NULL Terminated String
    DX_DEVICE_INFO_TYPE_WIFI_CONN_PASSWORD =            21,     //NULL Terminated String
    DX_DEVICE_INFO_TYPE_WIFI_CONN_SECURITY_TYPE =       22,     //int32_t
    DX_DEVICE_INFO_TYPE_DEV_FW_VERSION =                25,     //NULL Terminated String   - should be in format of xx.xx.xx (16 byte reservation should be more than enough)
    DX_DEVICE_INFO_TYPE_DEV_CP_BLE_FW_VERSION =         26,     //NULL Terminated String   - should be in format of xx.xx.xx (16 byte reservation should be more than enough)
    DX_DEVICE_INFO_TYPE_USER_INFO_PREFER_LANG =         30,     //NULL Terminated String
    DX_DEVICE_INFO_TYPE_BLE_PERIPHERAL_INFO_UPDATE =    31,     //uint8_t
    DX_DEVICE_INFO_TYPE_BLE_DEVICE_KEEPER_LIST_UPDATE = 32,     //uint8_t
    DX_DEVICE_INFO_TYPE_DEV_REGISTER_TIME =				33,     //uint32_t

    DX_DEVICE_INFO_TYPE_CONCURRENT_PERIPHERAL_SECURITY_KEY  = 35,     // 16 byte reservation should be more than enough
    DX_DEVICE_INFO_TYPE_REGISTER_STATE =				36,     //uint32_t

} dxDeviceInfoType_e;

typedef enum {

    DXDEV_INFO_SUCCESS                      = 0,
    DXDEV_INFO_SUCCESS_WITH_DATA_RECOVERY,
    DXDEV_INFO_EMPTY,

    DXDEV_INFO_NOT_INITIALIZED              = -1,
    DXDEV_INFO_TYPE_NOT_FOUND               = -2,
    DXDEV_INFO_TYPE_INVALID                 = -3,
    DXDEV_INFO_ERROR_MEM                    = -4,
    DXDEV_INFO_MAX_NUM_TYPE_REACHED         = -5,
    DXDEV_INFO_INTERNAL_ERROR               = -6,
    DXDEV_INFO_INVALID_PARAMETER            = -7,
    DXDEV_INFO_GIVEN_BUFFER_TOO_SMALL       = -8,

} dxDEV_INFO_RET_CODE;



//============================================================================
// Function
//============================================================================
/**
 * @brief   Initialization which will reload all data from flash to a local memory, this function call be recalled but will need to be called
 *          at least once before any other dxDeInfo API can be called.
 *
 * @return dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_Init();

/**
 * @brief
 * @param RecoverFromDiskIfCheckFailed      If TRUE data will be recovered from disk if integrity check fails
 * @return dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_IntegrityCheck(dxBOOL RecoverFromDiskIfCheckFailed);


/**
 * @brief Update Device Information with given type
 * @param[IN] InfoType          Data type to update
 * @param[IN] pDevInfoData      Pointer to data (if data is string it should include null byte)
 * @param[IN] DataSize          The size of the data (if data is string its lenght should also include null terminated byte)
 * @param[IN] SaveToDisk        If TRUE data will be updated to disk (eg, Flash depending on the platform) and if False it will temporary saved to memory
 * @return dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_Update(dxDeviceInfoType_e InfoType, void* pDevInfoData, uint16_t DataSize, dxBOOL SaveToDisk);

/**
 * @brief   Save all current information stored in memory to disk
 * @return  dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_SaveToDisk();

/**
 * @brief       Get the data of given data type
 * @param[IN]       InfoTypeToGet       The data type to search for
 * @param[INOUT]    pDevInfoData        Pointer to buffer for data, this can be NULL, if NULL only the data size is returned for request info type
 * @param[INOUT]    pDataSize           The size of the data buffer, if pDevInfoData is NULL then the given size is ignored, pDataSize cannot be NULL however.
 * @param[IN]       DirectlyFromDisk    If TRUE, the data will be read from the Disk directly instead from the latest storage in memory
 * @return dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_GetData(dxDeviceInfoType_e InfoTypeToGet, void* pDevInfoData, uint16_t* pDataSize, dxBOOL DirectlyFromDisk);

/**
 * @brief   Clean up disk, this will remove all the information from disk and from memory in device info manager
 * @return  dxDEV_INFO_RET_CODE
 */
dxDEV_INFO_RET_CODE dxDevInfo_CleanDisk();


#endif // _DXDEVICEINFOMANAGER_H
