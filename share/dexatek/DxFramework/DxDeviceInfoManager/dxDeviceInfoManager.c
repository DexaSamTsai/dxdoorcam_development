//============================================================================
// File: dxDeviceInfoManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/02/18
//     1) Description: Initial
//============================================================================
#include "dxOS.h"
#include "dxBSP.h"
#include "dxDeviceInfoManager.h"
#include "dxSysDebugger.h"
#include "Utils.h"

//============================================================================
// Definition
//============================================================================
#define DEV_INFO_HEADER_VERSION_INVALID             0
#define DEV_INFO_HEADER_CURRENT_VERSION_NUMBER      1



//============================================================================
// Structure
//============================================================================
typedef struct dxDeviceInfoDataContainer {
    uint16_t            Version;
    uint16_t            DeviceInfoTotalByte;
    void*               pDeviceInfoData;
} dxDeviceInfoDataContainer_t;

typedef struct dxDeviceTypeInfoHeader {
    uint16_t            DataType;       //dxDeviceInfoType_e
    uint16_t            DataSize;
} dxDeviceTypeInfoHeader_t;

typedef struct dxDeviceTypeInfo {
    dxDeviceTypeInfoHeader_t    Header;
    void*                       pData;
} dxDeviceTypeInfo_t;

//============================================================================
// Global variable
//============================================================================
dxAppDataHandle_t               pSysInfoHandler = NULL;
dxDeviceInfoDataContainer_t     DeviceInfoContainer = {DEV_INFO_HEADER_VERSION_INVALID, 0, NULL};
dxBOOL                          Initialized = dxFALSE;
dxSemaphoreHandle_t             ResourceProtectionSemaHandle = NULL;

//============================================================================
// Util Function (DO NOT MUTEX PROTECT!!!)
//============================================================================

dxBOOL dxUtilDevInfo_GetData(dxDeviceInfoType_e DataTypeToFind, dxDeviceInfoDataContainer_t* pDataContainer, dxDeviceTypeInfo_t* pReturnedDevtypeInfo)
{
    uint16_t    DevInfoOffset = 0;
    dxBOOL      Found = dxFALSE;
    if (pReturnedDevtypeInfo == NULL)
    {
        goto Exit;
    }

    while (DevInfoOffset < pDataContainer->DeviceInfoTotalByte)
    {
        dxDeviceTypeInfoHeader_t *pCurrentDevicetypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)pDataContainer->pDeviceInfoData + DevInfoOffset);

        if (pCurrentDevicetypeInfoHeader->DataType == DataTypeToFind)
        {
            pReturnedDevtypeInfo->Header.DataType = pCurrentDevicetypeInfoHeader->DataType;
            pReturnedDevtypeInfo->Header.DataSize = pCurrentDevicetypeInfoHeader->DataSize;
            pReturnedDevtypeInfo->pData = (char*)pDataContainer->pDeviceInfoData + DevInfoOffset + sizeof(dxDeviceTypeInfoHeader_t);
            Found = dxTRUE;
            break;
        }

        DevInfoOffset += sizeof(dxDeviceTypeInfoHeader_t) + pCurrentDevicetypeInfoHeader->DataSize;
    }

Exit:
    return Found;
}

dxBOOL dxUtilDevInfo_IntegrityCheck(dxBOOL UpdateCrcDataTypeIfCheckFailed)
{
    dxBOOL CheckSuccess = dxFALSE;

    if ((DeviceInfoContainer.Version != DEV_INFO_HEADER_VERSION_INVALID) && DeviceInfoContainer.DeviceInfoTotalByte)
    {
        uint16_t DevInfoOffset = 0;
        uint32_t CalcCrcRes = 0;
        uint32_t CRCFromDataInfo = 0;

        dxDeviceTypeInfo_t DevTypeCrc16Info;
        DevTypeCrc16Info.Header.DataType = DX_DEVICE_INFO_TYPE_UNKNOWN;
        if (dxUtilDevInfo_GetData(DX_DEVICE_INFO_TYPE_CRC16, &DeviceInfoContainer, &DevTypeCrc16Info))
        {
            //G_SYS_DBG_LOG_V1( "DBG - DevTypeCrc16Info.Header.DataSize = %d\r\n", DevTypeCrc16Info.Header.DataSize);
            memcpy(&CRCFromDataInfo, DevTypeCrc16Info.pData, DevTypeCrc16Info.Header.DataSize);
        }
        else
        {
            G_SYS_DBG_LOG_V( "WARNING: DeviceInfoContainer does NOT contain CRC Data Type for Integrity Check\r\n");
        }

        while (DevInfoOffset < DeviceInfoContainer.DeviceInfoTotalByte)
        {
            dxDeviceTypeInfoHeader_t *pCurrentDevicetypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + DevInfoOffset);

            //We do not include the CRC itself in the data container into our calculation
            if (pCurrentDevicetypeInfoHeader->DataType != DX_DEVICE_INFO_TYPE_CRC16)
            {
                //Calculate the header first
                CalcCrcRes = Crc16((uint8_t*)pCurrentDevicetypeInfoHeader, sizeof(dxDeviceTypeInfoHeader_t), CalcCrcRes);
                //Follow by its data
                CalcCrcRes = Crc16((uint8_t*)DeviceInfoContainer.pDeviceInfoData + DevInfoOffset + sizeof(dxDeviceTypeInfoHeader_t),
                                    pCurrentDevicetypeInfoHeader->DataSize, CalcCrcRes);
            }

            DevInfoOffset += sizeof(dxDeviceTypeInfoHeader_t) + pCurrentDevicetypeInfoHeader->DataSize;
        }

        //G_SYS_DBG_LOG_V2( "Calculated CRC = 0x%x, CRCFromDataInfo = 0x%x\r\n",CalcCrcRes , CRCFromDataInfo);

        if (CRCFromDataInfo == CalcCrcRes)
        {
            CheckSuccess = dxTRUE;
        }
        else if (UpdateCrcDataTypeIfCheckFailed)
        {
            if (DevTypeCrc16Info.Header.DataType != DX_DEVICE_INFO_TYPE_UNKNOWN)
            {
                memcpy(DevTypeCrc16Info.pData, &CalcCrcRes, sizeof(CalcCrcRes));
                CheckSuccess = dxTRUE;
            }
        }
    }

    return CheckSuccess;
}


dxDEV_INFO_RET_CODE dxUtilDevInfo_SaveToDisk()
{
   dxDEV_INFO_RET_CODE RetCode = DXDEV_INFO_SUCCESS;

   if ((DeviceInfoContainer.Version != DEV_INFO_HEADER_VERSION_INVALID) && DeviceInfoContainer.DeviceInfoTotalByte)
    {
        dxAPPDATA_RET_CODE SysInfoRetCode = dxAppData_Write(pSysInfoHandler, DeviceInfoContainer.pDeviceInfoData, DeviceInfoContainer.DeviceInfoTotalByte);
        if (SysInfoRetCode != DXAPPDATA_SUCCESS)
        {
            G_SYS_DBG_LOG_V1( "WARNING: Saving to disk FAILED! (RetCode=%d\r\n", SysInfoRetCode);
            RetCode = DXDEV_INFO_INTERNAL_ERROR;
        }
    }
    return RetCode;
}


//============================================================================
// Function
//============================================================================
dxDEV_INFO_RET_CODE dxDevInfo_Init()
{
    dxDEV_INFO_RET_CODE RetCode = DXDEV_INFO_SUCCESS;
    uint32_t LastDataSize = 0;

    //Only create if its NULL as dxDevInfo_Init will be called within dxDeviceInfoManager as well
    if (ResourceProtectionSemaHandle == NULL)
    {
        ResourceProtectionSemaHandle = dxSemCreate( 1,          // uint32_t uxMaxCount
                                                    1);         // uint32_t uxInitialCount

        if (ResourceProtectionSemaHandle == NULL)
        {
            RetCode = DXDEV_INFO_INTERNAL_ERROR;
            goto InvalidDevHeader;
        }
    }

    //Clean up memory first if it was previously allocated
    if (DeviceInfoContainer.pDeviceInfoData)
    {
        dxMemFree(DeviceInfoContainer.pDeviceInfoData);
    }

    //If it was not previously initialized
    if (pSysInfoHandler == NULL)
    {
        pSysInfoHandler = dxAppData_Init(DXBLOCKID_SYSINFO);
        if (pSysInfoHandler == NULL)
        {
            RetCode = DXDEV_INFO_INTERNAL_ERROR;
            goto InvalidDevHeader;
        }
    }

    LastDataSize = dxAppData_ReadDataSize(pSysInfoHandler);

    //G_SYS_DBG_LOG_V1( "DBG dxDevInfo_Init dxAppData_ReadDataSize = %d\r\n", LastDataSize);

    if (LastDataSize)
    {
        DeviceInfoContainer.DeviceInfoTotalByte = (uint16_t)LastDataSize;
        DeviceInfoContainer.pDeviceInfoData = dxMemAlloc("DevInfoBuff", 1, DeviceInfoContainer.DeviceInfoTotalByte + 1);

        if (DeviceInfoContainer.pDeviceInfoData)
        {
            dxAPPDATA_RET_CODE xSysInfoRet = dxAppData_Read(pSysInfoHandler, DeviceInfoContainer.pDeviceInfoData, &LastDataSize);
            if (xSysInfoRet != DXAPPDATA_SUCCESS)
            {
                RetCode = DXDEV_INFO_INTERNAL_ERROR;
                dxMemFree(DeviceInfoContainer.pDeviceInfoData);
                goto InvalidDevHeader;
            }
            else
            {
                dxDeviceTypeInfo_t ReturnedDevtypeInfo;
                dxBOOL Found = dxUtilDevInfo_GetData(DX_DEVICE_INFO_TYPE_VERSION, &DeviceInfoContainer, &ReturnedDevtypeInfo);

                //G_SYS_DBG_LOG_V2( "SizeAllocForData = %d, ActualDataSizeRetured = %d\r\n", DeviceInfoContainer.DeviceInfoTotalByte, LastDataSize);

                if (Found)
                {
                    //Version is uint16_t
                    memcpy(&DeviceInfoContainer.Version, ReturnedDevtypeInfo.pData, sizeof(uint16_t));
                }
                else //We should find dev info version type but we didn't, we simply return invalid version but still providing the data and its size abck to app
                {
                    DeviceInfoContainer.Version = DEV_INFO_HEADER_VERSION_INVALID;
                }
             }
        }
        else
        {
            RetCode = DXDEV_INFO_ERROR_MEM;
            goto InvalidDevHeader;
        }
    }
    else
    {
        goto InvalidDevHeader;
    }

    goto InitExit;

InvalidDevHeader:

    G_SYS_DBG_LOG_V( "DBG dxDevInfo_Init InvalidDevHeader\r\n");

    DeviceInfoContainer.Version = DEV_INFO_HEADER_VERSION_INVALID;
    DeviceInfoContainer.DeviceInfoTotalByte = 0;
    DeviceInfoContainer.pDeviceInfoData = NULL;

InitExit:

    if (RetCode >= 0)
    {
        Initialized = dxTRUE;
    }

    return RetCode;
}


dxDEV_INFO_RET_CODE dxDevInfo_IntegrityCheck(dxBOOL RecoverFromDiskIfCheckFailed)
{
    dxDEV_INFO_RET_CODE RetCode = DXDEV_INFO_SUCCESS;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(ResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if ((DeviceInfoContainer.Version != DEV_INFO_HEADER_VERSION_INVALID) && DeviceInfoContainer.DeviceInfoTotalByte)
    {
        if (dxUtilDevInfo_IntegrityCheck(dxFALSE) == dxFALSE)
        {
            G_SYS_DBG_LOG_V( "WARNING: dxDevInfo_IntegrityCheck failed!\r\n");
            if (RecoverFromDiskIfCheckFailed)
            {
                G_SYS_DBG_LOG_V( "NOTE: Recovering devinfo from DISK\r\n");
                //Simply call initialization again to reload the data back from flash
                if (dxDevInfo_Init() != DXDEV_INFO_SUCCESS)
                {
                    G_SYS_DBG_LOG_V( "WARNING: dxDevInfo_IntegrityCheck DXDEV_INFO_INTERNAL_ERROR\r\n");
                    RetCode = DXDEV_INFO_INTERNAL_ERROR;
                }
                else
                {
                    G_SYS_DBG_LOG_V( "dxDevInfo_IntegrityCheck DXDEV_INFO_SUCCESS_WITH_DATA_RECOVERY\r\n");
                    RetCode = DXDEV_INFO_SUCCESS_WITH_DATA_RECOVERY;
                }
            }
        }
    }
    else
    {
        G_SYS_DBG_LOG_V( "dxDevInfo_IntegrityCheck DeviceInfoContainer invalid\r\n");
    }

    // Release control
    dxSemGive(ResourceProtectionSemaHandle);

    return RetCode;
}

dxDEV_INFO_RET_CODE dxDevInfo_Update(dxDeviceInfoType_e InfoType, void* pDevInfoData, uint16_t DataSize, dxBOOL SaveToDisk)
{
    dxDEV_INFO_RET_CODE RetCode = DXDEV_INFO_SUCCESS;
    dxBOOL              UpdateCrc = dxFALSE;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(ResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (!Initialized)
    {
        RetCode = DXDEV_INFO_NOT_INITIALIZED;
        goto UpdateExit;
    }

    if ((pDevInfoData == NULL) || (DataSize == 0))
    {
        RetCode = DXDEV_INFO_INVALID_PARAMETER;
        goto UpdateExit;
    }

    //Brand New
    if (DeviceInfoContainer.DeviceInfoTotalByte == 0)
    {
        //G_SYS_DBG_LOG_V( "DBG dxDevInfo_Update STAGE 1\r\n");

        if (DeviceInfoContainer.pDeviceInfoData)
        {
            G_SYS_DBG_LOG_V( "WARNING: We do not expect info container to contain value\r\n");
            dxMemFree(DeviceInfoContainer.pDeviceInfoData);
        }

        //Size for DX_DEVICE_INFO_TYPE_VERSION, DX_DEVICE_INFO_TYPE_CRC16, and Given InfoTYpe data to update
        uint16_t TotalSizeAllocation = (sizeof(dxDeviceTypeInfoHeader_t) * 3) + sizeof(uint16_t) + sizeof(uint32_t) + DataSize;
        DeviceInfoContainer.pDeviceInfoData = dxMemAlloc("DevInfoBuff", 1, TotalSizeAllocation);
        DeviceInfoContainer.Version = DEV_INFO_HEADER_CURRENT_VERSION_NUMBER;
        DeviceInfoContainer.DeviceInfoTotalByte = TotalSizeAllocation;

        uint16_t Offset = 0;
        char*    pDeviceInfoTypeData = NULL;
        //Add Internal device version
        dxDeviceTypeInfoHeader_t* pDeviceTypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + Offset);
        pDeviceTypeInfoHeader->DataType = DX_DEVICE_INFO_TYPE_VERSION;
        pDeviceTypeInfoHeader->DataSize = sizeof(uint16_t);
        pDeviceInfoTypeData = (char*)DeviceInfoContainer.pDeviceInfoData + Offset + sizeof(dxDeviceTypeInfoHeader_t);
        memcpy(pDeviceInfoTypeData, &DeviceInfoContainer.Version, sizeof(uint16_t));
        Offset += sizeof(dxDeviceTypeInfoHeader_t) + pDeviceTypeInfoHeader->DataSize;

        //Add internal CRC16
        pDeviceTypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + Offset);
        pDeviceTypeInfoHeader->DataType = DX_DEVICE_INFO_TYPE_CRC16;
        pDeviceTypeInfoHeader->DataSize = sizeof(uint32_t);
        Offset += sizeof(dxDeviceTypeInfoHeader_t) + pDeviceTypeInfoHeader->DataSize;

        //Add the requested device type data
        pDeviceTypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + Offset);
        pDeviceTypeInfoHeader->DataType = InfoType;
        pDeviceTypeInfoHeader->DataSize = DataSize;
        pDeviceInfoTypeData = (char*)DeviceInfoContainer.pDeviceInfoData + Offset + sizeof(dxDeviceTypeInfoHeader_t);
        memcpy(pDeviceInfoTypeData, pDevInfoData, DataSize);
        Offset += sizeof(dxDeviceTypeInfoHeader_t) + pDeviceTypeInfoHeader->DataSize;

        UpdateCrc = dxTRUE;
    }
    else
    {
        //G_SYS_DBG_LOG_V( "DBG dxDevInfo_Update STAGE 2\r\n");

        if (DeviceInfoContainer.pDeviceInfoData == NULL)
        {
            G_SYS_DBG_LOG_V( "WARNING: device info data container is NULL!!!\r\n");
            RetCode = DXDEV_INFO_INTERNAL_ERROR;
            goto UpdateExit;
        }

        dxDeviceTypeInfo_t ReturnedDevtypeInfo;
        ReturnedDevtypeInfo.Header.DataType = DX_DEVICE_INFO_TYPE_UNKNOWN;
        dxUtilDevInfo_GetData(InfoType, &DeviceInfoContainer, &ReturnedDevtypeInfo);

        uint16_t NewContainerSize = DeviceInfoContainer.DeviceInfoTotalByte;
        if (ReturnedDevtypeInfo.Header.DataType == DX_DEVICE_INFO_TYPE_UNKNOWN)
        {
            //G_SYS_DBG_LOG_V( "DBG dxDevInfo_Update STAGE 3\r\n");

            NewContainerSize += sizeof(dxDeviceTypeInfoHeader_t) + DataSize;
            void* pNewAllocBuffer = dxMemAlloc("DevInfoBuff", 1, NewContainerSize);
            if (pNewAllocBuffer)
            {
                memcpy(pNewAllocBuffer, DeviceInfoContainer.pDeviceInfoData, DeviceInfoContainer.DeviceInfoTotalByte);
                dxMemFree(DeviceInfoContainer.pDeviceInfoData);
                DeviceInfoContainer.pDeviceInfoData = pNewAllocBuffer;
                dxDeviceTypeInfoHeader_t* pDeviceTypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + DeviceInfoContainer.DeviceInfoTotalByte);
                pDeviceTypeInfoHeader->DataType = InfoType;
                pDeviceTypeInfoHeader->DataSize = DataSize;
                char* pDeviceTypeInfoData = (char*)DeviceInfoContainer.pDeviceInfoData + DeviceInfoContainer.DeviceInfoTotalByte + sizeof(dxDeviceTypeInfoHeader_t);
                memcpy(pDeviceTypeInfoData, pDevInfoData, DataSize);
                DeviceInfoContainer.DeviceInfoTotalByte = NewContainerSize;

                UpdateCrc = dxTRUE;
            }
            else
            {
                RetCode = DXDEV_INFO_ERROR_MEM;
            }
        }
        else //found
        {
            //G_SYS_DBG_LOG_V( "DBG dxDevInfo_Update STAGE 4\r\n");

            NewContainerSize += DataSize - ReturnedDevtypeInfo.Header.DataSize;
            void* pNewAllocBuffer = dxMemAlloc("DevInfoBuff", 1, NewContainerSize);
            if (pNewAllocBuffer)
            {
                uint16_t DevInfoOffset = 0;
                uint16_t NewAllocBufferWriteOffset = 0;
                while (DevInfoOffset < DeviceInfoContainer.DeviceInfoTotalByte)
                {
                    dxDeviceTypeInfoHeader_t *pCurrentDevicetypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + DevInfoOffset);

                    //Only move data that is not the existing data type for replacement
                    if (pCurrentDevicetypeInfoHeader->DataType != InfoType)
                    {
                        //Copy the Header
                        memcpy( (char*)pNewAllocBuffer + NewAllocBufferWriteOffset,
                                pCurrentDevicetypeInfoHeader,
                                sizeof(dxDeviceTypeInfoHeader_t));

                        //Copy the data
                        memcpy( (char*)pNewAllocBuffer + NewAllocBufferWriteOffset + sizeof(dxDeviceTypeInfoHeader_t),
                                (char*)DeviceInfoContainer.pDeviceInfoData + DevInfoOffset + sizeof(dxDeviceTypeInfoHeader_t),
                                pCurrentDevicetypeInfoHeader->DataSize);

                        NewAllocBufferWriteOffset += sizeof(dxDeviceTypeInfoHeader_t) + pCurrentDevicetypeInfoHeader->DataSize;
                    }

                    DevInfoOffset += sizeof(dxDeviceTypeInfoHeader_t) + pCurrentDevicetypeInfoHeader->DataSize;
                }

                dxMemFree(DeviceInfoContainer.pDeviceInfoData);
                DeviceInfoContainer.pDeviceInfoData = pNewAllocBuffer;
                dxDeviceTypeInfoHeader_t* pDeviceTypeInfoHeader = (dxDeviceTypeInfoHeader_t*)((char*)DeviceInfoContainer.pDeviceInfoData + NewAllocBufferWriteOffset);
                pDeviceTypeInfoHeader->DataType = InfoType;
                pDeviceTypeInfoHeader->DataSize = DataSize;
                char* pDeviceInfoTypeData = (char*)DeviceInfoContainer.pDeviceInfoData + NewAllocBufferWriteOffset + sizeof(dxDeviceTypeInfoHeader_t);
                memcpy(pDeviceInfoTypeData, pDevInfoData, DataSize);
                DeviceInfoContainer.DeviceInfoTotalByte = NewContainerSize;

                UpdateCrc = dxTRUE;
            }
            else
            {
                RetCode = DXDEV_INFO_ERROR_MEM;
            }
        }
    }

    if (UpdateCrc)
    {
        if (dxUtilDevInfo_IntegrityCheck(dxTRUE) == dxFALSE)
        {
            G_SYS_DBG_LOG_V( "WARNING: CRC Data Type Data update failed!\r\n");
            RetCode = DXDEV_INFO_INTERNAL_ERROR;
            goto UpdateExit;
        }
    }

    if(SaveToDisk)
    {
        RetCode = dxUtilDevInfo_SaveToDisk();
    }

UpdateExit:

    // Release control
    dxSemGive(ResourceProtectionSemaHandle);

    return RetCode;

}

dxDEV_INFO_RET_CODE dxDevInfo_SaveToDisk()
{

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(ResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    dxDEV_INFO_RET_CODE RetCode = dxUtilDevInfo_SaveToDisk();

    // Release control
    dxSemGive(ResourceProtectionSemaHandle);

    return RetCode;
}


dxDEV_INFO_RET_CODE dxDevInfo_GetData(dxDeviceInfoType_e InfoTypeToGet, void* pDevInfoData, uint16_t* pDataSize, dxBOOL DirectlyFromDisk)
{
    dxDEV_INFO_RET_CODE         RetCode = DXDEV_INFO_TYPE_NOT_FOUND;
    dxDeviceTypeInfo_t          ReturnedDevtypeInfo;
    dxBOOL                      DataFound = dxFALSE;
    dxDeviceInfoDataContainer_t TempDevInfoDataContainerFromDisk;
    TempDevInfoDataContainerFromDisk.pDeviceInfoData = NULL;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(ResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

    if (pDataSize == NULL)
    {
        RetCode = DXDEV_INFO_INVALID_PARAMETER;
        goto GetDataExit;
    }

    if (DirectlyFromDisk) //Direct access from disk
    {

        uint32_t LastDataSize = dxAppData_ReadDataSize(pSysInfoHandler);
        //G_SYS_DBG_LOG_V1( "DBG dxDevInfo_GetData from DISK (SysInfoSize=%d)\r\n", LastDataSize);
        if (LastDataSize)
        {
            //Allocate temporary memory
            TempDevInfoDataContainerFromDisk.pDeviceInfoData = dxMemAlloc("DevInfoBuff", 1, LastDataSize + 1);
            if (TempDevInfoDataContainerFromDisk.pDeviceInfoData)
            {
                dxAPPDATA_RET_CODE xSysInfoRet = dxAppData_Read(pSysInfoHandler, TempDevInfoDataContainerFromDisk.pDeviceInfoData, &LastDataSize);
                if (xSysInfoRet != DXAPPDATA_SUCCESS)
                {
                    G_SYS_DBG_LOG_V1( "dxAppData_Read with error = %d\r\n", xSysInfoRet);
                    RetCode = DXDEV_INFO_INTERNAL_ERROR;
                }
                else
                {
                    TempDevInfoDataContainerFromDisk.DeviceInfoTotalByte = LastDataSize;
                    //TempDevInfoDataContainerFromDisk.Version = WE DONT CARE VERSION IN HERE AS dxUtilDevInfo_GetData will not check
                    DataFound = dxUtilDevInfo_GetData(InfoTypeToGet, &TempDevInfoDataContainerFromDisk, &ReturnedDevtypeInfo);
                }
            }
        }
    }
    else if ((DeviceInfoContainer.Version != DEV_INFO_HEADER_VERSION_INVALID) && DeviceInfoContainer.DeviceInfoTotalByte) //From mem access
    {

        if(dxUtilDevInfo_GetData(InfoTypeToGet, &DeviceInfoContainer, &ReturnedDevtypeInfo))
        {
            DataFound = dxTRUE;
        }
    }

    if (DataFound)
    {
        if (pDevInfoData == NULL)
        {
            *pDataSize = ReturnedDevtypeInfo.Header.DataSize;
        }
        else
        {
            if (*pDataSize < ReturnedDevtypeInfo.Header.DataSize)
            {
                RetCode = DXDEV_INFO_GIVEN_BUFFER_TOO_SMALL;
                goto GetDataExit;
            }
            else
            {
                //G_SYS_DBG_LOG_V1( "dxAppData_Read datalen=%d\r\n", ReturnedDevtypeInfo.Header.DataSize);

                memcpy(pDevInfoData, ReturnedDevtypeInfo.pData, ReturnedDevtypeInfo.Header.DataSize);
                *pDataSize = ReturnedDevtypeInfo.Header.DataSize;
                RetCode = DXDEV_INFO_SUCCESS;
            }
        }
    }

GetDataExit:

    if (TempDevInfoDataContainerFromDisk.pDeviceInfoData)
        dxMemFree(TempDevInfoDataContainerFromDisk.pDeviceInfoData);

    // Release control
    dxSemGive(ResourceProtectionSemaHandle);

    return RetCode;
}


dxDEV_INFO_RET_CODE dxDevInfo_CleanDisk()
{
    dxDEV_INFO_RET_CODE RetCode = DXDEV_INFO_SUCCESS;

    // To protect SIMULTANEOUS ACCESS
    dxSemTake(ResourceProtectionSemaHandle, DXOS_WAIT_FOREVER);

   if (!Initialized)
    {
        RetCode = DXDEV_INFO_NOT_INITIALIZED;
        goto CleanDiskExit;
    }

    dxAppData_Clean(pSysInfoHandler);

    dxDevInfo_Init(); //Reload the data again for synch with disk

    // Release control
    dxSemGive(ResourceProtectionSemaHandle);

CleanDiskExit:
    return RetCode;
}