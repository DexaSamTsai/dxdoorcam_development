//============================================================================
// File: UdpCommManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/14
//     1) Description: Initial
//
//============================================================================

#include <stdlib.h>
#include "dxOS.h"
#include "UdpCommManager.h"

//============================================================================
// Macros
//============================================================================

#define DEVICE_INFO_REPORT_SESSION_EXPIRE_TIME          (20*SECONDS)

//============================================================================
// Constants
//============================================================================

#define UDP_COMM_ACK_QUEUE_WAIT_TIME_OUT                        100
#define UDP_COMMAND_RECEIVED_TIME_LIST_HOLDER_MAX               10
#define UDP_COMMAND_RECEIVED_TIME_VALID_TIME_MAX                (UDP_RECEIVED_VALID_TIME_IN_SEC*SECONDS)

#define RX_WAIT_TIMEOUT                                         (250)

#define UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_PRIORITY          DXTASK_PRIORITY_IDLE
#define UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_THREAD_STACK_SIZE          1024 //512
#define UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_THREAD_MAX_EVENT_QUEUE     20

#define UDP_COMM_MANAGER_CTRL_WORKER_PRIORITY                   DXTASK_PRIORITY_IDLE
#define UDP_COMM_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE          512
#define UDP_COMM_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE     10

#define UDP_COMM_RECEIVE_PACKET_THREAD_PRIORITY                 DXTASK_PRIORITY_IDLE
#define UDP_COMM_RECEIVE_PACKET_THREAD_NAME                     "Udp Manager packet receiver"
#define UDP_COMM_RECEIVE_PACKET_THREAD_STACK_SIZE               1024 //1536

//============================================================================
// Enumerations
//============================================================================

typedef enum
{

    UDP_COMM_MANAGER_NOT_INITIALIZED                = 0,
    UDP_COMM_MANAGER_NOT_STARTED,
    UDP_COMM_MANAGER_IN_OPERATION,

} UdpCommManager_Status_t;

//============================================================================
// Type Definitions
//============================================================================

//============================================================================
// Structures
//============================================================================

typedef struct
{

    uint8_t*                udp_data;
    uint16_t                udp_data_size;

} UdpPacketInfo_t;


typedef struct
{
    uint16_t PacketType;
    uint16_t DeviceIdentificationNumber;
    uint16_t JobIdentificationNumber;
    uint16_t PayloadLen;
    uint8_t* PayloadData; /**< All Payload should be allocated to new memory, this is automatically freed when message is sent. */

} UdpTransmitMsgQueue_t;//Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data


typedef struct
{

    uint16_t  DeviceId;
    uint16_t  TaskId;

} UdpCommAckMsg_t; //Message for RTOS Queue MUST be 4 byte aligned and LESS than 64 bytes, suggest to send pointer of this type instead of whole data

typedef struct
{

    UdpCommAckMsg_t     Command;
    dxTime_t            ReceivedTime;

} UdpCommReceivedTime_t;

//============================================================================
// Function Declarations
//============================================================================

static void DeviceInfoReportSessionTimeOutHandler(void* arg);

static void process_received_udp_packet_thread_main( uint32_t arg );

static void process_Transfer_udp_packet(void* arg);

static void process_Transfer_udp_Ack(void* arg);

//============================================================================
// Variable Definitions
//============================================================================


static dxBOOL                   UdpReceiverThreadSuspended = dxTRUE;
static dxTaskHandle_t           UdpComm_receive_packet_thread;

static dxWorkweTask_t           EventWorkerThread;
static dxWorkweTask_t           UdpProcessTranceiveWorkerThread;

static dxEventHandler_t         RegisteredEventFunction;

static dxQueueHandle_t          CommandAckQueue;

static dxTimerHandle_t          DeviceInfoReportSessionTimer;

static dxUDPHandle_t            udp_socket;

static char                     MulticastGroupIp[16];

static UdpCommManager_Status_t  UdpManagerStatus = UDP_COMM_MANAGER_NOT_INITIALIZED;

static UdpCommReceivedTime_t    UdpCommandReceivedTimeList[UDP_COMMAND_RECEIVED_TIME_LIST_HOLDER_MAX];

static dxSemaphoreHandle_t      UdpActivitySemaHandle = NULL;

static dxBOOL                   UdpSendInProgressWaitingForAck = dxFALSE;

/******************************************************
 *               Local Function Definitions
 ******************************************************/

/**
 * @brief Prepare and build UDP packet based on Dk format
 * @param PacketType    -   according to UdpCommManager_EventType_t in dk_Network OR provide UDP_COMM_EVENT_UNKNOWN for ACK
 * @param DeviceId      -   The device identified
 * @param JobId         -   The Job ID
 * @param PayloadLen    -   The payloadlen if any, if none provide 0
 * @param PayloadData   -   The payload data, if no data provide NULL
 * @return UdpPacketInfo_t
 */
UdpPacketInfo_t* UdpPacketCreateAndBuild(uint16_t PacketType, uint16_t DeviceId, uint16_t JobId, uint16_t PayloadLen, uint8_t* PayloadData)
{
    UdpPacketInfo_t *pUdpPakcetInfo = NULL;
    uint8_t* pUdpDataHolderWriter = NULL;
    uint16_t Udp16ByteAlignPayloadTotalSize = 0;

    if (PayloadLen)
    {
        Udp16ByteAlignPayloadTotalSize = sizeof(UdpPayloadHeader_t) + PayloadLen;
        uint16_t LeftOver = (Udp16ByteAlignPayloadTotalSize%AES_ENC_DEC_BYTE);
        if (LeftOver)
            Udp16ByteAlignPayloadTotalSize = ((Udp16ByteAlignPayloadTotalSize/AES_ENC_DEC_BYTE) + 1)*AES_ENC_DEC_BYTE;
    }

    uint16_t UdapPacketTotalSize = sizeof(UdpPacketHeader_t) + Udp16ByteAlignPayloadTotalSize;
    uint8_t* UdpDataHolder = dxMemAlloc( "UdpData Holder Buff", 1, UdapPacketTotalSize);
    if (UdpDataHolder == NULL)
    {
        G_SYS_DBG_LOG_V("WARNING: UdpDataHolder mem alloc FAILED!\r\n");
        goto Exit;
    }

    pUdpDataHolderWriter = UdpDataHolder;

    pUdpPakcetInfo = dxMemAlloc("UdpPakcetInfo", 1, sizeof(UdpPacketInfo_t));
    if (pUdpPakcetInfo == NULL)
    {
        goto Exit;
    }

    pUdpPakcetInfo->udp_data = dxMemAlloc( "UdpDataBuff", 1, UdapPacketTotalSize);
    if (pUdpPakcetInfo->udp_data == NULL)
    {
        dxMemFree(pUdpPakcetInfo);
        pUdpPakcetInfo = NULL;
        goto Exit;
    }

    pUdpPakcetInfo->udp_data_size = UdapPacketTotalSize;

    //G_SYS_DBG_LOG_V3("UDP -> %d, %d, %d\r\n", sizeof(UdpPayloadHeader_t), PayloadLen, Udp16ByteAlignPayloadTotalSize);



    UdpPacketHeader_t PacketHeader;
    PacketHeader.Preamble = UDP_PACKET_PREAMBLE;
    memset(PacketHeader.Reserver, 0, sizeof(PacketHeader.Reserver));
    PacketHeader.DeviceId = DeviceId;
    PacketHeader.PacketId = JobId;
    PacketHeader.PayloadCRC = 0;    //No CRC Check
    PacketHeader.PayloadLen = Udp16ByteAlignPayloadTotalSize;

    //Copy to data holder and move the pointer position
    memcpy( pUdpDataHolderWriter, &PacketHeader, sizeof(UdpPacketHeader_t) );  pUdpDataHolderWriter += sizeof(UdpPacketHeader_t);

    //Copy the packet header to container first.
    memset(pUdpPakcetInfo->udp_data, 0, UdapPacketTotalSize);
    memcpy( pUdpPakcetInfo->udp_data, UdpDataHolder, sizeof(UdpPacketHeader_t) );

    //Do we have payload data and known event type?
    if (PayloadData && PayloadLen && (PacketType != UDP_COMM_EVENT_UNKNOWN))
    {
        const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();
        dxTime_t CurrentSystemTime;
        CurrentSystemTime = dxTimeGetMS();

        UdpPayloadHeader_t PayloadHeader;
        PayloadHeader.DeviceIdentificationNumber = DeviceId;
        PayloadHeader.PayLoadIdentificationNumber = JobId;
        PayloadHeader.PacketType = PacketType;
        PayloadHeader.PayloadLen = PayloadLen;
        PayloadHeader.TimeToken = CurrentSystemTime;
        memcpy( PayloadHeader.GatewayMacAddr.MacAddr, DeviceMac->MacAddr, 8 );

        memcpy(pUdpDataHolderWriter, &PayloadHeader, sizeof(UdpPayloadHeader_t) ); pUdpDataHolderWriter += sizeof(UdpPayloadHeader_t);
        if (PayloadLen)
        {
            memcpy(pUdpDataHolderWriter, PayloadData, PayloadLen ); pUdpDataHolderWriter += PayloadLen;
        }

        dxAesContext_t   Aes128ctx = NULL;
        Aes128ctx =  dxAESSW_ContextAlloc();

        if (Aes128ctx == NULL)
        {
            G_SYS_DBG_LOG_V("FATAL: UdpManager unable to allocate Aes128ctx\r\n");
        }

        dxAESSW_Setkey( Aes128ctx, (unsigned char *)UdpPacketAesKey, AES_ENC_DEC_BIT, DXAES_ENCRYPT );

        /*Start the encryption after the packet header*/
        uint8_t*  pWorking_UdpDataHolder = UdpDataHolder + sizeof(UdpPacketHeader_t);
        uint8_t*  pWorking_udp_data = pUdpPakcetInfo->udp_data + sizeof(UdpPacketHeader_t);

        while (Udp16ByteAlignPayloadTotalSize)
        {
            dxAESSW_CryptEcb( Aes128ctx, DXAES_ENCRYPT, pWorking_UdpDataHolder, pWorking_udp_data );

            if (Udp16ByteAlignPayloadTotalSize >= AES_ENC_DEC_BYTE)
            {
                Udp16ByteAlignPayloadTotalSize -= AES_ENC_DEC_BYTE;
                pWorking_UdpDataHolder += AES_ENC_DEC_BYTE;
                pWorking_udp_data += AES_ENC_DEC_BYTE;
            }
            else
            {
                G_SYS_DBG_LOG_V("FATAL: Udp16ByteAlignPayloadTotalSize is NOT 16 byte aligned\r\n");
                break;
            }
        }

        if (Aes128ctx)
            dxAESSW_ContextDealloc(Aes128ctx);
    }

Exit:
    if (UdpDataHolder)
    {
        dxMemFree(UdpDataHolder);
        UdpDataHolder = NULL;
    }

    return pUdpPakcetInfo;

}

static void DeviceInfoReportSessionTimeOutHandler(void* arg)
{

    if (UdpManagerStatus == UDP_COMM_MANAGER_IN_OPERATION)
    {

        UdpTransmitMsgQueue_t* TransmitMsg = dxMemAlloc( "Udp Transmit Msg Buff", 1, sizeof(UdpTransmitMsgQueue_t));

        TransmitMsg->PacketType = UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_WILL_EXPIRE;
        TransmitMsg->DeviceIdentificationNumber =0; //Ignore
        TransmitMsg->JobIdentificationNumber =0; //Ignore
        TransmitMsg->PayloadLen = 0;
        TransmitMsg->PayloadData = NULL;

        dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_packet, TransmitMsg);
        if (EventResult != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V("WARNING: DeviceInfoReportSessionTimeOutHandler Unable to send Udp transfer message to queue (possibly full)\r\n");
            dxMemFree(TransmitMsg);
        }
    }

    dxTimerStop(DeviceInfoReportSessionTimer, DXOS_WAIT_FOREVER);
}

dxBOOL UdpCommandAlreadyReceived(uint16_t DeviceId, uint16_t PacketId)
{
    dxBOOL AlreadyReceived = dxFALSE;
    dxTime_t CurrentTime;
    CurrentTime = dxTimeGetMS();

    uint16_t i = 0;
    int16_t AvailableSlot = -1;
    for (i = 0; i < UDP_COMMAND_RECEIVED_TIME_LIST_HOLDER_MAX; i++)
    {

        if ((CurrentTime - UdpCommandReceivedTimeList[i].ReceivedTime) > UDP_COMMAND_RECEIVED_TIME_VALID_TIME_MAX)
        {
            //Session valid time expired, we are going to clear this slot so that other can use
            memset(&UdpCommandReceivedTimeList[i], 0, sizeof (UdpCommReceivedTime_t));
            AvailableSlot = i;
        }
        else
        {
            if ((UdpCommandReceivedTimeList[i].Command.DeviceId == DeviceId) &&
                (UdpCommandReceivedTimeList[i].Command.TaskId == PacketId))
            {
                //Already received, so this command is duplicate.
                AlreadyReceived = dxTRUE;
                break;
            }
        }
    }

    //First time, so let's save it to available slot
    if (AlreadyReceived == dxFALSE)
    {
        if (AvailableSlot >= 0)
        {
            UdpCommandReceivedTimeList[AvailableSlot].Command.DeviceId = DeviceId;
            UdpCommandReceivedTimeList[AvailableSlot].Command.TaskId = PacketId;
            UdpCommandReceivedTimeList[AvailableSlot].ReceivedTime = CurrentTime;

            //NOTE: We still return AlreadyReceived as dxFALSE for first time save
        }
        else
        {
            G_SYS_DBG_LOG_V("WARNING: UdpCommandAlreadyReceived slot FULL!\r\n");
        }
    }

    return AlreadyReceived;
}


void process_Transfer_udp_Ack(void* arg)
{
    if (arg)
    {
        UdpCommAckMsg_t* AckMsg = (UdpCommAckMsg_t*)arg;

        UdpPacketInfo_t* UdpPacketInfo = UdpPacketCreateAndBuild(UDP_COMM_EVENT_UNKNOWN, AckMsg->DeviceId, AckMsg->TaskId, 0, NULL);

        if (UdpPacketInfo && UdpPacketInfo->udp_data)
        {
            dxSemTake(UdpActivitySemaHandle, DXOS_WAIT_FOREVER);

            /* Send the UDP packet */
            if (dxMCAST_SendTo(udp_socket, UdpPacketInfo->udp_data, UdpPacketInfo->udp_data_size, NULL) <= 0)
            {
                G_SYS_DBG_LOG_V("UDP packet send failed\r\n");
            }

            dxSemGive(UdpActivitySemaHandle);

            dxMemFree(UdpPacketInfo->udp_data);
            dxMemFree(UdpPacketInfo);

        }
        else
        {
            G_SYS_DBG_LOG_V("WARNING: packet and/or udp_data is NULL\r\n");
        }

Exit:
        dxMemFree(AckMsg);

    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: process_Transfer_udp_Ack with NULL arg\r\n");
    }

    return;
}

void process_Transfer_udp_packet(void* arg)
{

    if (arg)
    {
        UdpTransmitMsgQueue_t* TransmitMsg = (UdpTransmitMsgQueue_t*)arg;

        dxBOOL  UdpSendInProgress = dxTRUE;
        uint8_t RetryCount = 0;

        UdpSendInProgressWaitingForAck = UdpSendInProgress;

        while(UdpSendInProgress)
        {

            if (RetryCount > UDP_COMMAND_RETRY_MAX)
                break;

            UdpPacketInfo_t* UdpPacketInfo = UdpPacketCreateAndBuild(TransmitMsg->PacketType, TransmitMsg->DeviceIdentificationNumber, TransmitMsg->JobIdentificationNumber, TransmitMsg->PayloadLen, TransmitMsg->PayloadData);

            //G_SYS_DBG_LOG_V("UdpPacketCreateAndBuild size = %d\r\n", UdpPacketInfo->udp_data_size);

            if (UdpPacketInfo && UdpPacketInfo->udp_data)
            {
                /* Send the UDP packet */
                dxSemTake(UdpActivitySemaHandle, DXOS_WAIT_FOREVER);

                if (dxMCAST_SendTo(udp_socket, UdpPacketInfo->udp_data, UdpPacketInfo->udp_data_size, NULL) <= 0)
                {
                    G_SYS_DBG_LOG_V("UDP packet send failed\r\n");
                }

                dxSemGive(UdpActivitySemaHandle);

                dxMemFree(UdpPacketInfo->udp_data);
                dxMemFree(UdpPacketInfo);

            }
            else
            {
                G_SYS_DBG_LOG_V("WARNING: packet and/or udp_data is NULL\r\n");
                UdpSendInProgress = dxFALSE;
            }

            if (UdpSendInProgress)
            {

                //We are not going to wait for Ack if there is no device identification number, device identification number of 0 is mostly sent
                //from UdpCommManager with message to client that does not require ack.
                if (TransmitMsg->DeviceIdentificationNumber == 0)
                    break;

                dxTime_t AckWaitStartTime;
                AckWaitStartTime = dxTimeGetMS();
                while (1)
                {
                    UdpCommAckMsg_t CommAckRsp;

                    if (dxQueueReceive(CommandAckQueue, &CommAckRsp, UDP_COMM_ACK_QUEUE_WAIT_TIME_OUT) == DXOS_SUCCESS)
                    {
                        if ((CommAckRsp.DeviceId == TransmitMsg->DeviceIdentificationNumber) &&
                            (CommAckRsp.TaskId == TransmitMsg->JobIdentificationNumber))
                        {
                            G_SYS_DBG_LOG_V2("UDP Acked %d,%d\r\n", TransmitMsg->DeviceIdentificationNumber, TransmitMsg->JobIdentificationNumber);
                            UdpSendInProgress = dxFALSE;
                            break;
                        }
                    }

                    dxTime_t CurrentSystemTime;
                    CurrentSystemTime = dxTimeGetMS();

                    if ((CurrentSystemTime - AckWaitStartTime) > UDP_COMM_ACK_WAIT_TIME_MAX_MS)
                    {
                        G_SYS_DBG_LOG_V2("UDP Ack TimeOut %d,%d\r\n", TransmitMsg->DeviceIdentificationNumber, TransmitMsg->JobIdentificationNumber);
                        break;
                    }
                }
            }

            RetryCount++;
        }

        if ((TransmitMsg->PayloadLen) && (TransmitMsg->PayloadData))
            dxMemFree(TransmitMsg->PayloadData);

        dxMemFree(TransmitMsg);

    }
    else
    {
        G_SYS_DBG_LOG_V("WARNING: process_Transfer_udp_packet with NULL arg\r\n");
    }

    UdpSendInProgressWaitingForAck = dxFALSE;

    return;
}



static void process_received_udp_packet_thread_main( uint32_t arg )
{
    dxAesContext_t  Aes128ctx = dxAESSW_ContextAlloc();

    while(1)
    {
        //G_SYS_DBG_LOG_V("Process_received_udp_packet_thread_main\r\n");

        //If Suspended we sleep for few seconds before we come back to check
        if (UdpReceiverThreadSuspended)
        {
            dxTimeDelayMS(1*SECONDS);
            continue;
        }

        /* Wait for UDP packet */
        uint16_t ReceiveBuffSize = 256;
        uint8_t* pReceiveBuff = dxMemAlloc( "Udp Receive Buff", 1, ReceiveBuffSize);

        dxSemTake(UdpActivitySemaHandle, DXOS_WAIT_FOREVER);

        int ActualReceiveByte = dxMCAST_RecvFrom(udp_socket, pReceiveBuff, ReceiveBuffSize, NULL, RX_WAIT_TIMEOUT);

        dxSemGive(UdpActivitySemaHandle);

        //G_SYS_DBG_LOG_V("Udp received byte = %d\r\n", ActualReceiveByte);

        if (ActualReceiveByte > 0)
        {

            if (ActualReceiveByte < sizeof(UdpPacketHeader_t))
            {
                G_SYS_DBG_LOG_V("Udp received Packet is too small\r\n");
            }
            else if (ActualReceiveByte >= ReceiveBuffSize)
            {
                G_SYS_DBG_LOG_V("WARNING: UDP Packet Received size might be too large, will ignore this packet\r\n");
            }
            else
            {
                //G_SYS_DBG_LOG_V1( "UDP Packet Received size = %d\r\n",  ActualReceiveByte);

                UdpPacketHeader_t PacketHeader;
                memcpy(&PacketHeader, pReceiveBuff, sizeof(UdpPacketHeader_t));
                uint16_t TotalEncryptedPayloadLen = ActualReceiveByte - sizeof(UdpPacketHeader_t);

                if ((PacketHeader.Preamble == UDP_PACKET_PREAMBLE) && (PacketHeader.PayloadLen == 0))
                {

                    //This is Ack message, we will send it to queue, but we make sure we are listening for ack as
                    //it could be ack from other central which we will not want to populate CommandAckQueue
                    if ((PacketHeader.DeviceId) && (UdpSendInProgressWaitingForAck == dxTRUE))
                    {
                        UdpCommAckMsg_t CommAckRsp;
                        CommAckRsp.DeviceId = PacketHeader.DeviceId;
                        CommAckRsp.TaskId = PacketHeader.PacketId;

                        G_SYS_DBG_LOG_V( "Udp Ack Received\r\n");

                        if (dxQueueSend(CommandAckQueue,  &CommAckRsp, 1*(SECONDS)) != DXOS_SUCCESS)
                        {
                            G_SYS_DBG_LOG_V( "Error pushing CommAckRsp message to queue (Could be due to timeout) \r\n" );
                        }
                    }
                }
                else if ((PacketHeader.Preamble == UDP_PACKET_PREAMBLE) && !(TotalEncryptedPayloadLen%AES_ENC_DEC_BYTE))
                {

                    dxTime_t    CurrentSystemTime;
                    dxBOOL      ReleaseDecryptedData = dxTRUE;

                    uint8_t*    pWorking_rx_data = pReceiveBuff + sizeof(UdpPacketHeader_t);
                    uint16_t    TotalSizeLeftToDecrypt = ActualReceiveByte - sizeof(UdpPacketHeader_t);
                    unsigned char   AesDataOut[AES_ENC_DEC_BYTE];

                    uint8_t* DecryptedData = dxMemAlloc( "BridgeComm Rsp Msg Buff", 1, TotalSizeLeftToDecrypt);
                    uint8_t* pWorkingDecryptedData = DecryptedData;

                    dxAESSW_Setkey( Aes128ctx, UdpPacketAesKey, AES_ENC_DEC_BIT, DXAES_DECRYPT );

                    while (TotalSizeLeftToDecrypt)
                    {
                        dxAESSW_CryptEcb( Aes128ctx, DXAES_DECRYPT, pWorking_rx_data, AesDataOut  );

                        memcpy(pWorkingDecryptedData, AesDataOut, AES_ENC_DEC_BYTE);

                        if (TotalSizeLeftToDecrypt >= AES_ENC_DEC_BYTE)
                        {
                            TotalSizeLeftToDecrypt -= AES_ENC_DEC_BYTE;
                            pWorking_rx_data += AES_ENC_DEC_BYTE;
                            pWorkingDecryptedData += AES_ENC_DEC_BYTE;
                        }
                        else
                        {
                            G_SYS_DBG_LOG_V("FATAL: TotalSizeLeftToDecrypt is NOT 16 byte aligned\r\n");
                            break;
                        }
                    }

                    UdpPayloadHeader_t* UdpPayloadHeader = (UdpPayloadHeader_t*)DecryptedData;

                    CurrentSystemTime = dxTimeGetMS();

                    const WifiCommDeviceMac_t* DeviceMac = WifiCommManagerGetDeviceMac();

                    //G_SYS_DBG_LOG_V1( "UDP Packet Received PacketType = %d\r\n", UdpPayloadHeader->PacketType);

                    switch (UdpPayloadHeader->PacketType)
                    {
                        case UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_START:
                        {
                            //G_SYS_DBG_LOG_V( "UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_START received from Mobile Device\r\n" );

                            dxTimerRestart(DeviceInfoReportSessionTimer, DX_TIMER_PERIOD_NO_CHANGE, DXOS_WAIT_FOREVER);

                            UdpTransmitMsgQueue_t* TransmitMsg = dxMemAlloc( "Udp Transmit Msg Buff", 1, sizeof(UdpTransmitMsgQueue_t));

                            uint8_t* pResponseReserve = dxMemAlloc( "Udp Transmit payload Buff", 1, sizeof(uint32_t));

                            TransmitMsg->PacketType = UDP_COMM_EVENT_DEVICE_INFO_REPORT_SESSION_START_RESPONSE;
                            TransmitMsg->DeviceIdentificationNumber = 0; //Ignore
                            TransmitMsg->JobIdentificationNumber = 0; //Ignore
                            TransmitMsg->PayloadLen = sizeof(uint32_t); //Do not remove - App UDP manager will igore any packet with 0 payload len, this is not an issue but we would like App to receive this completely so that it can update the central alive time
                            TransmitMsg->PayloadData = pResponseReserve;

                            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_packet, TransmitMsg);
                            if (EventResult != DXOS_SUCCESS)
                            {
                                G_SYS_DBG_LOG_V("WARNING: Udp Manager session start response Unable to send Udp transfer message to queue (possibly full)\r\n");
                                dxMemFree(TransmitMsg);
                            }
                        }
                        break;

                        //All NON system message type will be default and sent to application layer
                        default:
                        {
                            //Make sure the time token is within 8 seconds of current system time
                            if ((UdpPayloadHeader->TimeToken < CurrentSystemTime) && ((CurrentSystemTime - UdpPayloadHeader->TimeToken) < 8000))
                            {

                                if ( memcmp( DeviceMac->MacAddr, UdpPayloadHeader->GatewayMacAddr.MacAddr, 8 ) == 0 )
                                {
                                    /* Send Ack */

                                    UdpCommAckMsg_t* AckMsg = dxMemAlloc( "Udp Ack Msg Buff", 1, sizeof(UdpCommAckMsg_t));
                                    AckMsg->DeviceId = PacketHeader.DeviceId;
                                    AckMsg->TaskId = PacketHeader.PacketId;

                                    dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_Ack, AckMsg);
                                    if (EventResult != DXOS_SUCCESS)
                                    {
                                        G_SYS_DBG_LOG_V("WARNING: Udp Manager ack response Unable to send Udp transfer message to queue (possibly full)\r\n");
                                        dxMemFree(AckMsg);
                                    }
                                    else
                                    {
                                        //Check if this command is already received, if so we should ignore it as the ack migth failed to reach to client before.
                                        //This prevent receiving the same command more than 1 time
                                        if (UdpCommandAlreadyReceived(PacketHeader.DeviceId, PacketHeader.PacketId) == dxFALSE)
                                        {
                                            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&EventWorkerThread, RegisteredEventFunction, DecryptedData);
                                            if (EventResult == DXOS_SUCCESS)
                                            {
                                                //Successfully sent to application so we don't release the data buffer as application will do the job after it's being processed
                                                ReleaseDecryptedData = dxFALSE;
                                            }
                                            else
                                            {
                                                G_SYS_DBG_LOG_V( "FATAL - Upd Received Packet Failed to send Event message back to Application\r\n");
                                            }

                                        }
                                    }


                                }
                                else
                                {
                                    G_SYS_DBG_LOG_V( "Job Device MAC Not match\r\n");
                                }
                            }
                            else
                            {
                                //G_SYS_DBG_LOG_V("Udp received Packet time token not valid\r\n");
                            }
                        }
                        break;
                    }

                    if (ReleaseDecryptedData == dxTRUE)
                        dxMemFree(DecryptedData);
                }
                else
                {
                    G_SYS_DBG_LOG_V("WARNING: Received packet is not 16 byte aligned payload\r\n");
                }
            }

            /*
            G_SYS_DBG_LOG_V5 ( "UDP from IP %u.%u.%u.%u:%d\r\n", (unsigned char) ( ( GET_IPV4_ADDRESS(udp_src_ip_addr) >> 24 ) & 0xff ),
                                                                    (unsigned char) ( ( GET_IPV4_ADDRESS(udp_src_ip_addr) >> 16 ) & 0xff ),
                                                                    (unsigned char) ( ( GET_IPV4_ADDRESS(udp_src_ip_addr) >>  8 ) & 0xff ),
                                                                    (unsigned char) ( ( GET_IPV4_ADDRESS(udp_src_ip_addr) >>  0 ) & 0xff ),
                                                                    udp_src_port  );
            */

            //It is highly recommended to short sleep before we start receiving next command as
            //it will drag down the entire system performance even if this thread priority is set to low.
            //Possible reason could be due to receiving UDP packet is not entirely related to the thread priority
            //and therefore it will always return with data when it find it and this will cause issue if there is a lot of peripheral/central
            //in the same network which this thread will become very busy and congest other task.
            dxTimeDelayMS(100);
        }
        else
        {
            //Did not got any data back, Just a short sleep before we continue
            dxTimeDelayMS(100);
        }

        if (pReceiveBuff)
        {
            dxMemFree(pReceiveBuff);
            pReceiveBuff = NULL;
        }
    }
}


/******************************************************
 *               Function Definitions
 ******************************************************/

UdpCommManager_result_t UdpCommManagerInit(event_handler_t EventFunction)
{

    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_NOT_INITIALIZED)
    {

        UdpActivitySemaHandle = dxSemCreate(1,          // uint32_t uxMaxCount
                                            1);         // uint32_t uxInitialCount

        if (UdpActivitySemaHandle == NULL)
        {
            G_SYS_DBG_LOG_V(  "UdpCommManagerInit - Failed to create semaphore\r\n" );
            result = UDP_COMM_RTOS_ERROR;
            goto EndInit;
        }

        if (dxWorkerTaskCreate("UDPCOMM TW", &UdpProcessTranceiveWorkerThread, UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_PRIORITY, UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_THREAD_STACK_SIZE, UDP_COMM_MANAGER_TRANS_PROCESS_WORKER_THREAD_MAX_EVENT_QUEUE) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V(  "UdpCommManagerInit - Failed to create worker thread\r\n" );
            result = UDP_COMM_RTOS_ERROR;
            goto EndInit;
        }
        else
        {
            GSysDebugger_RegisterForThreadDiagnosticGivenName(&UdpProcessTranceiveWorkerThread.WTask, "Udp Int. Event Thread");
        }

        DeviceInfoReportSessionTimer = dxTimerCreate(   "Session Report Timer",
                                                        DEVICE_INFO_REPORT_SESSION_EXPIRE_TIME,
                                                        dxFALSE,
                                                        NULL,
                                                        DeviceInfoReportSessionTimeOutHandler);
        if (DeviceInfoReportSessionTimer == NULL)
        {
            result = UDP_COMM_RTOS_ERROR;
            goto EndInit;
        }

        if (CommandAckQueue == NULL)
        {
            // Create Queue
            CommandAckQueue = dxQueueCreate(5, sizeof(UdpCommAckMsg_t));
            if (!CommandAckQueue)
            {
                G_SYS_DBG_LOG_V( "Error creating CommandAckQueue \r\n");
                result = UDP_COMM_RTOS_ERROR;
                goto EndInit;
            }
        }

        if  (dxTaskCreate(  process_received_udp_packet_thread_main,
                            UDP_COMM_RECEIVE_PACKET_THREAD_NAME,
                            UDP_COMM_RECEIVE_PACKET_THREAD_STACK_SIZE,
                            NULL,
                            UDP_COMM_RECEIVE_PACKET_THREAD_PRIORITY,
                            &UdpComm_receive_packet_thread) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V(  "Error creating UdpComm_receive_packet_thread\r\n" );
            result = UDP_COMM_RTOS_ERROR;
            goto EndInit;
        }

        GSysDebugger_RegisterForThreadDiagnostic(&UdpComm_receive_packet_thread);

        if (EventFunction)
        {
            if (RegisteredEventFunction == NULL)	//First time, we create worker thread first
            {
                RegisteredEventFunction = EventFunction;

                if (dxWorkerTaskCreate("UDPCOMM W", &EventWorkerThread, UDP_COMM_MANAGER_CTRL_WORKER_PRIORITY, UDP_COMM_MANAGER_CTRL_WORKER_THREAD_STACK_SIZE, UDP_COMM_MANAGER_CTRL_WORKER_THREAD_MAX_EVENT_QUEUE) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V(  "UdpCommManagerInit - Failed to create worker thread\r\n" );
                    RegisteredEventFunction = NULL;
                    result = UDP_COMM_RTOS_ERROR;
                    goto EndInit;
                }
                else
                {
                    GSysDebugger_RegisterForThreadDiagnosticGivenName(&EventWorkerThread.WTask, "UdpCommManager Event Thread");
                }
            }
        }
        else
        {
            result = UDP_COMM_INVALID_PARAMETER;
            goto EndInit;
        }

        memset(UdpCommandReceivedTimeList, 0, sizeof(UdpCommandReceivedTimeList));
        UdpManagerStatus = UDP_COMM_MANAGER_NOT_STARTED;
    }

EndInit:
    return result;
}


UdpCommManager_result_t UdpCommManagerStart(void)
{
    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_NOT_STARTED)
    {
        dxIP_Addr_t IpGroup;

        memset( &IpGroup, 0, sizeof(IpGroup) );
        IpGroup.version = DXIP_VERSION_V4;
        IpGroup.v4[0] = UDP_MULTICAST_ADDRESS_FIRST;
        IpGroup.v4[1] = UDP_MULTICAST_ADDRESS_SECOND;
        IpGroup.v4[2] = UDP_MULTICAST_ADDRESS_THIRD;
        IpGroup.v4[3] = UDP_MULTICAST_ADDRESS_FOURTH;

        udp_socket = dxMCAST_Join(IpGroup, UDP_TARGET_PORT);
        if (udp_socket == NULL)
        {
            G_SYS_DBG_LOG_V("UDP socket creation failed\r\n");
            result = UDP_SOCKET_CREATION_FAILED;
            goto Exit;
        }

        UdpReceiverThreadSuspended = dxFALSE;

        UdpManagerStatus = UDP_COMM_MANAGER_IN_OPERATION;
    }
    else if (UdpManagerStatus != UDP_COMM_MANAGER_IN_OPERATION)
    {
        result = UDP_PROCESS_CANNOT_BE_COMPLETE_DUE_TO_INVALID_STATE;
    }

Exit:
    return result;
}


UdpCommManager_result_t UdpCommManagerStop(void)
{

    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_IN_OPERATION)
    {
        G_SYS_DBG_LOG_V("UdpCommManagerStop\r\n");

        UdpReceiverThreadSuspended = dxTRUE;

        dxSemTake(UdpActivitySemaHandle, DXOS_WAIT_FOREVER);

        dxNET_RET_CODE mCastRet = dxMCAST_Leave(udp_socket);

        dxSemGive(UdpActivitySemaHandle);

        if (mCastRet != DXNET_SUCCESS)
        {
            G_SYS_DBG_LOG_V("WARNING: UDP multicast leave failed\r\n");
        }

        UdpManagerStatus = UDP_COMM_MANAGER_NOT_STARTED;
    }

    return result;
}


UdpPayloadHeader_t* UdpCommManagerGetEventObjHeader(void* ReportedEventArgObj)
{
    UdpPayloadHeader_t* UdpHeader = NULL;

    if (ReportedEventArgObj)
    {
        UdpHeader = (UdpPayloadHeader_t*)ReportedEventArgObj;
    }

    return UdpHeader;
}

char* UdpCommManagerGetEventObjPayloadData(void* ReportedEventArgObj)
{
    char* pPayloadData = NULL;

    if (ReportedEventArgObj)
    {
        UdpPayloadHeader_t* UdpHeader = NULL;

        UdpHeader = (UdpPayloadHeader_t*)ReportedEventArgObj;

        if (UdpHeader->PayloadLen)
        {
            pPayloadData = (char*)ReportedEventArgObj + sizeof(UdpPayloadHeader_t);
        }
    }

    return pPayloadData;
}

UdpCommManager_result_t UdpCommManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (ReportedEventArgObj)
    {
        dxMemFree(ReportedEventArgObj);
    }

    return result;

}


UdpCommManager_result_t UdpCommManagerSendDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen)
{

    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_IN_OPERATION)
    {

        if (dxTimerIsTimerActive(DeviceInfoReportSessionTimer) == dxTRUE)
        {
            UdpTransmitMsgQueue_t*  TransmitMsg = dxMemAlloc( "Udp Transmit Msg Buff", 1, sizeof(UdpTransmitMsgQueue_t));
            uint8_t*                TransmitMsgPayload = dxMemAlloc( "Udp Transmit payload Buff", 1, DevInfoPayloadLen);

            TransmitMsg->PacketType = UDP_COMM_EVENT_DEVICE_INFO;
            TransmitMsg->DeviceIdentificationNumber = 0; //Ignore
            TransmitMsg->JobIdentificationNumber = 0; //Ignore
            TransmitMsg->PayloadLen = DevInfoPayloadLen;
            memcpy(TransmitMsgPayload, DevInfoPayload, DevInfoPayloadLen);
            TransmitMsg->PayloadData = TransmitMsgPayload;

            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_packet, TransmitMsg);
            if (EventResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V("WARNING: UdpCommManagerSendDeviceInfo Unable to send Udp transfer message to queue (possibly full)\r\n");
                dxMemFree(TransmitMsgPayload);
                dxMemFree(TransmitMsg);
            }
        }
        else
        {
            result = UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING;
        }
    }
    else
    {
        result = UDP_MANAGER_NOT_IN_OPERATION;
    }

    return result;

}

UdpCommManager_result_t UdpCommManagerSendUnpairDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen)
{
    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_IN_OPERATION)
    {
        if (dxTimerIsTimerActive(DeviceInfoReportSessionTimer) == dxTRUE)
        {
            UdpTransmitMsgQueue_t*  TransmitMsg = dxMemAlloc( "Udp Transmit Msg Buff", 1, sizeof(UdpTransmitMsgQueue_t));
            uint8_t*                TransmitMsgPayload = dxMemAlloc( "Udp Transmit payload Buff", 1, DevInfoPayloadLen);

            TransmitMsg->PacketType = UDP_COMM_EVENT_UNPAIR_DEVICE_INFO;
            TransmitMsg->DeviceIdentificationNumber = 0; //Ignore
            TransmitMsg->JobIdentificationNumber = 0; //Ignore
            TransmitMsg->PayloadLen = DevInfoPayloadLen;
            memcpy(TransmitMsgPayload, DevInfoPayload, DevInfoPayloadLen);
            TransmitMsg->PayloadData = TransmitMsgPayload;

            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_packet, TransmitMsg);
            if (EventResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V("WARNING: UdpCommManagerSendUnpairDeviceInfo Unable to send Udp transfer message to queue (possibly full)\r\n");
                dxMemFree(TransmitMsgPayload);
                dxMemFree(TransmitMsg);
            }
        }
        else
        {
            result = UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING;
        }
    }
    else
    {
        result = UDP_MANAGER_NOT_IN_OPERATION;
    }

    return result;
}

UdpCommManager_result_t UdpCommManagerSendJobExecutionResponse(uint16_t DeviceUniqueIdentification, uint16_t JobIdentificationNumber, int16_t Status)
{
    UdpCommManager_result_t result = UDP_COMM_MANAGER_SUCCESS;

    if (UdpManagerStatus == UDP_COMM_MANAGER_IN_OPERATION)
    {
        if (dxTimerIsTimerActive(DeviceInfoReportSessionTimer) == dxTRUE)
        {

            UdpTransmitMsgQueue_t*  TransmitMsg = dxMemAlloc( "Udp Transmit Msg Buff", 1, sizeof(UdpTransmitMsgQueue_t));
            uint8_t*                TransmitMsgPayload = dxMemAlloc( "Udp Transmit payload Buff", 1, sizeof(Status));

            TransmitMsg->PacketType = UDP_COMM_EVENT_JOB_EXECUTION_RESPONSE;
            TransmitMsg->DeviceIdentificationNumber = DeviceUniqueIdentification;
            TransmitMsg->JobIdentificationNumber = JobIdentificationNumber;
            TransmitMsg->PayloadLen = sizeof(Status);
            memcpy(TransmitMsgPayload, &Status, sizeof(Status));
            TransmitMsg->PayloadData = TransmitMsgPayload;

            dxOS_RET_CODE EventResult = dxWorkerTaskSendAsynchEvent(&UdpProcessTranceiveWorkerThread, process_Transfer_udp_packet, TransmitMsg);
            if (EventResult != DXOS_SUCCESS)
            {
                G_SYS_DBG_LOG_V("WARNING: UdpCommManagerSendJobExecutionResponse Unable to send Udp transfer message to queue (possibly full)\r\n");
                dxMemFree(TransmitMsgPayload);
                dxMemFree(TransmitMsg);
            }

        }
        else
        {
            result = UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING;
        }
    }
    else
    {
        result = UDP_MANAGER_NOT_IN_OPERATION;
    }

    return result;
}

