//============================================================================
// File: UdpCommManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2016/03/14
//     1) Description: Initial
//
//============================================================================
#ifndef _DXUDPCOMMMANAGER_H
#define _DXUDPCOMMMANAGER_H


#include "dk_Network.h"
#include "WifiCommManager.h"
#include "dxSysDebugger.h"

//============================================================================
// Defines
//============================================================================

//============================================================================
// Macros
//============================================================================

//============================================================================
// Constants
//============================================================================

//============================================================================
// Enumerations
//============================================================================

typedef enum
{

    UDP_COMM_MANAGER_SUCCESS                                = 0,    /**< Success */
    UDP_COMM_MANAGER_NO_MOBILE_DEVICE_LISTENING             = 1,    /**< The request is not sent because there is no mobile device listening */

    UDP_SOCKET_CREATION_FAILED                              = -1,
    UDP_SOCKET_FAILED_TO_JOIN_MULTICAST                     = -2,
    UDP_PROCESS_CANNOT_BE_COMPLETE_DUE_TO_INVALID_STATE     = -3,
    UDP_MANAGER_NOT_IN_OPERATION                            = -4,

    UDP_COMM_GENERIC_ERROR                                  = -20,
    UDP_COMM_RTOS_ERROR                                     = -21,
    UDP_COMM_TOO_MANY_TASK_IN_QUEUE                         = -22,
    UDP_COMM_INVALID_PARAMETER                              = -23,
    UDP_COMM_API_ERROR                                      = -24,

} UdpCommManager_result_t;


//============================================================================
// Type Definitions
//============================================================================

//============================================================================
// Structures
//============================================================================

//============================================================================
// Function Declarations
//============================================================================

/** 
 * @brief Initialize Udp Communication Manager, all task or activity will be reported by calling EventFunction passing the arg as EventObject
 *  NOTE1: Use UdpCommManagerGetEventObjHeader(arg) to obtain the UdpCommEventReportObj_t.
 *  NOTE2: According to ReportEvent handle necessary action (see UdpCommManager_EventType_t)
 *  NOTE3: Use UdpCommManagerGetEventObjPayloadData(arg) to obtain the pointer to the payload data.
 *  NOTE4: It is necessary to copy any information that needs to be kept. Do NOT reference the pointer to the provided data.
 *  NOTE5: At the end of EventFunction call UdpCommManagerClearEventArgumentObj(arg) to clean the EventObject otherwise memory leak will occur
 *
 * @param[in]  EventFunction     :  The EventFunction that will be called when reporting activities
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerInit(event_handler_t EventFunction);


/** 
 * @brief Start Udp, this function should be called whenever wifi connection is up and local Ip address is obtained
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerStart(void);


/** 
 * @brief Stop Udp, this function should be called whenever wifi connection is down
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerStop(void);


/** 
 * @brief Get the event header from reported event obj arg
 *
 * @param[in]  ReportedEventArgObj  :   The reported obj argument to obtain the header info
 *
 * @return @ref UdpPayloadHeader_t
 */
extern UdpPayloadHeader_t* UdpCommManagerGetEventObjHeader(void* ReportedEventArgObj);


/** 
 * @brief Get the event payload data from reported event obj arg
 *
 * @param[in]  ReportedEventArgObj  :   The reported obj argument to obtain the payload data
 *
 * @return @ref char*
 */
extern char* UdpCommManagerGetEventObjPayloadData(void* ReportedEventArgObj);


/** 
 * @brief All information of Object given by EventFunction will be cleaned
 *
 * @param[in]  ReportedEventArg :   The reported obj argument to clean up
 *
 * @return @ref UdpCommManager_result_t
 */
UdpCommManager_result_t UdpCommManagerCleanEventArgumentObj(void* ReportedEventArgObj);


/** 
 * @brief Send Job execution feedback via UDP multicast
 *
 * NOTE1:   UdpManager will decide if there is any mobile device interested in listening device info. If there is no mobile device listening then
 *          device info will not be sent. This will save some processing power and bandwith.
 *
 * @param[in]  DeviceUniqueIdentification   :   Device Unique identification
 * @param[in]  JobIdentificationNumber      :   Job Identification
 * @param[in]  Status   :   The Job execution status according to JobReportStatus_t
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerSendJobExecutionResponse(uint16_t DeviceUniqueIdentification, uint16_t JobIdentificationNumber, int16_t Status);


/** 
 * @brief Send device info via UDP multicast
 * NOTE1:   UdpManager will decide if there is any mobile device interested in listening device info. If there is no mobile device listening then
 *          device info will not be sent. This will save some processing power and bandwith.
 *
 * @param[in]  DevInfoPayload       :   The payload data of device info (this is usually DeviceInfoReport_t)
 * @param[in]  DevInfoPayloadLen    :   The len of payload data
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerSendDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen);

/** 
 * @brief Send unpair device info via UDP multicast
 *
 * NOTE1:   UdpManager will decide if there is any mobile device interested in listening device info. If there is no mobile device listening then
 *          device info will not be sent. This will save some processing power and bandwith.
 *
 * @param[in]  DevInfoPayload       :   The payload data of unpair device info (this is usually DeviceInfoReport_t)
 * @param[in]  DevInfoPayloadLen    :   The len of payload data
 *
 * @return @ref UdpCommManager_result_t
 */
extern UdpCommManager_result_t UdpCommManagerSendUnpairDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen);

#endif //_DXUDPCOMMMANAGER_H