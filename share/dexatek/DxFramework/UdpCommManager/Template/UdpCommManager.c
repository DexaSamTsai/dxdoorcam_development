//============================================================================
// File: UdpCommManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
#include "UdpCommManager.h"

//============================================================================
// Macros
//============================================================================


//============================================================================
// Constants
//============================================================================


//============================================================================
// Enumerations
//============================================================================


//============================================================================
// Type Definitions
//============================================================================


//============================================================================
// Structures
//============================================================================


//============================================================================
// Function Declarations
//============================================================================


//============================================================================
// Variable Definitions
//============================================================================


//============================================================================
// Function Definitions
//============================================================================

UdpCommManager_result_t UdpCommManagerInit(event_handler_t EventFunction)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}

UdpCommManager_result_t UdpCommManagerStart(void)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}

UdpCommManager_result_t UdpCommManagerStop(void)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}


UdpPayloadHeader_t* UdpCommManagerGetEventObjHeader(void* ReportedEventArgObj)
{
	// Todo...

    return NULL;
}

char* UdpCommManagerGetEventObjPayloadData(void* ReportedEventArgObj)
{
	// Todo...

    return NULL;
}

UdpCommManager_result_t UdpCommManagerCleanEventArgumentObj(void* ReportedEventArgObj)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}


UdpCommManager_result_t UdpCommManagerSendDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}

UdpCommManager_result_t UdpCommManagerSendUnpairDeviceInfo(uint8_t* DevInfoPayload, uint16_t DevInfoPayloadLen)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}

UdpCommManager_result_t UdpCommManagerSendJobExecutionResponse(uint16_t DeviceUniqueIdentification, uint16_t JobIdentificationNumber, int16_t Status)
{
	// Todo...

    return UDP_COMM_RTOS_ERROR;
}

