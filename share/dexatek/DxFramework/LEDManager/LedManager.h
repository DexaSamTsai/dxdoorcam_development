//============================================================================
//============================================================================
// File: LedManager.h
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================

#pragma once

#include "dxSysDebugger.h"


/******************************************************
 *                      Defines
 ******************************************************/

typedef enum
{
    LED_MANAGER_SUCCESS                   = 0,

    LED_MANAGER_RTOS_ERROR                = -1,
    LED_MANAGER_TOO_MANY_TASK_IN_QUEUE    = -2,
    LED_MANAGER_INVALID_PARAMETER         = -3,
    LED_MANAGER_SEND_MESSAGE_ERROR        = -4,
    LED_MANAGER_OUT_OF_MEMORY_ERROR       = -5,
    LED_MANAGER_QUEUE_FULL_ERROR          = -6,
    LED_MANAGER_QUEUE_BUFFER_NULL         = -7,

} LedManager_result_t;


typedef enum
{

    LED_OPERATION_MODE_INIT   = 0,
    LED_OPERATION_MODE_SWITCHING_TO_NORMAL,
    LED_OPERATION_MODE_NORMAL,
    LED_OPERATION_MODE_SWITCHING_TO_SETUP,
    LED_OPERATION_MODE_SETUP,
    LED_OPERATION_MODE_SWITCHING_TO_RESTART,
    LED_OPERATION_MODE_RESTARTING,
    LED_OPERATION_MODE_SWITCHING_TO_FW_DOWNLOAD,
    LED_OPERATION_MODE_FW_DOWNLOAD,
    LED_OPERATION_MODE_SWITCHING_TO_BLE_CENTRAL_FW_UPDATE,
    LED_OPERATION_MODE_BLE_CENTRAL_FW_UPDATE,

    //This should be placed in the last, for any new mode add above this line
    LED_OPERATION_MODE_UNKNOWN,

} Led_Operation_Mode_t;


typedef enum
{

    LED_CONDITION_NONE    = 0,
    LED_CONDITION_WIFI_CONNECTING,
    LED_CONDITION_CONNECTING_WITH_DK_SERVER,
    LED_CONDITION_DISCONNECTED_BY_DK_SERVER,
    LED_CONDITION_WWW_NOT_REACHABLE,
    LED_CONDITION_BUSY_PAIRING_WITH_PERIPHERAL,
    LED_CONDITION_BUSY_ACCESSING_PERIPHERAL,
    LED_CONDITION_SCANNING_UNPAIR_PERIPHERALS,

    //One time condition, when done it will return to previous condition
    LED_CONDITION_ONE_TIME_ACCESS_DENIED,
    LED_CONDITION_ONE_TIME_IDENTIFY,

    //Error condition
    LED_CONDITION_ERROR,	//DO NOT USE THIS to set condition
    LED_CONDITION_WIFI_CONNECTION_ERROR,
    LED_CONDITION_DK_SERVER_OPERATION_ERROR,
    LED_CONDITION_FW_DOWNLOAD_UPDATE_ERROR,
    LED_CONDITION_INTERNAL_CHECK_ERROR,

    //This should be placed in the last, for any new mode add above this line
    LED_CONDITION_UNKNOWN,

} Led_Condition_t;


/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    Led_Operation_Mode_t      LedOperMode;
    Led_Condition_t           LedCondition;
    uint32_t                    LedColor;
    dxTime_t                    FlashIntervalTime;
    dxBOOL                      ExecuteOnceTime;
    uint8_t                     FlashNumber;

} Led_Table_t;

/******************************************************
 *               Function Declarations
 ******************************************************/

extern LedManager_result_t LedManagerInit(void);

extern LedManager_result_t SetLedCustomTable(Led_Table_t* CustomLedTable, uint16_t SizeofTable);

extern LedManager_result_t SetLedMode(Led_Operation_Mode_t LedMode);

extern LedManager_result_t SetLedCondition(Led_Condition_t LedCondition);

extern LedManager_result_t SetLedModeCondition(Led_Operation_Mode_t LedMode, Led_Condition_t LedCondition);

extern Led_Operation_Mode_t GetLedMode(void);

extern Led_Condition_t GetLedCondition(void);

