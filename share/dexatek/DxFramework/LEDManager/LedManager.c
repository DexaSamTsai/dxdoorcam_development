//============================================================================
//============================================================================
// File: LedManager.c
//
// Author: Aaron Lee
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================



#include "dxOS.h"
#include "dxBSP.h"
#include "dxSysDebugger.h"
#include "LedManager.h"
#include "Utils.h"

/******************************************************
 *                      Macros
 ******************************************************/
#define LED_MANAGER_PROCESS_THREAD_PRIORITY       DXTASK_PRIORITY_HIGH
#define LED_MANAGER_PROCESS_THREAD_NAME           "Led Status Process"
#define LED_MANAGER_STACK_SIZE                    384

/******************************************************
 *                    Constants
 ******************************************************/
#define LED_TIMER_TICK                          (100*MILLISECONDS)
#define MAXIMUM_LED_JOB_QUEUE_DEPT_ALLOWED      10

/******************************************************
 *                   Enumerations
 ******************************************************/

/******************************************************
 *                 Type Definitions
 ******************************************************/

typedef struct
{
    Led_Operation_Mode_t      LedMode;
    Led_Condition_t           LedCondition;

} Device_Led_t;

/******************************************************
 *               Function Declarations
 ******************************************************/
static void LedOperationStart(void);

/******************************************************
 *               Variable Definitions
 ******************************************************/
static Device_Led_t       CurrentDeviceLed;
static Led_Table_t        JobBuff;

static dxTaskHandle_t       LedMainProcess_thread;
static dxQueueHandle_t      LedJobProcessQueue;
static dxTimerHandle_t      LEDSessionTimer;
static dxBOOL               LEDOnceTimeIfDone = dxTRUE;

static dxSemaphoreHandle_t  LedBufferSemaphore;

Led_Table_t*    LedMap = NULL;
uint16_t        LedMapSize = 0;

Led_Table_t DefaultLedMap[] = {

    /* WIFI connect*/
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_WIFI_CONNECTING,        DK_LED_BLUE,    500,    dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_WIFI_CONNECTING,        DK_LED_BLUE,    500,    dxFALSE, 5},
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_WIFI_CONNECTION_ERROR,  DK_LED_RED,     250,    dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_WIFI_CONNECTION_ERROR,  DK_LED_RED,     250,    dxFALSE, 5},

    /* SERVER log-in */
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_CONNECTING_WITH_DK_SERVER, DK_LED_BLUE, 250,    dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_CONNECTING_WITH_DK_SERVER, DK_LED_BLUE, 250,    dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_DISCONNECTED_BY_DK_SERVER, DK_LED_BLUE, 500,    dxFALSE, 5},

    /* WWW is not reachable */
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_WWW_NOT_REACHABLE,         DK_LED_RED, 1000, dxFALSE, 5},

    /* SERVER operation */
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_DK_SERVER_OPERATION_ERROR,  DK_LED_RED, 500, dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_DK_SERVER_OPERATION_ERROR,  DK_LED_RED, 500, dxFALSE, 5},

    /* PERIPHERAL accessing */
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_BUSY_ACCESSING_PERIPHERAL,      DK_LED_BLUE, 100, dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_BUSY_PAIRING_WITH_PERIPHERAL,   DK_LED_BLUE, 100, dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_SCANNING_UNPAIR_PERIPHERALS,    DK_LED_BLUE, 1000, dxFALSE, 5},


    /* Device mode operation */
    {LED_OPERATION_MODE_INIT,                 LED_CONDITION_NONE,                   DK_LED_YELLOW,  2000,   dxFALSE, 5},
    {LED_OPERATION_MODE_INIT,                 LED_CONDITION_INTERNAL_CHECK_ERROR,   DK_LED_RED,     1000,   dxFALSE, 5},
    {LED_OPERATION_MODE_NORMAL,               LED_CONDITION_NONE,                   DK_LED_BLUE,    0,      dxFALSE, 5},
    {LED_OPERATION_MODE_SETUP,                LED_CONDITION_NONE,                   DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_SWITCHING_TO_SETUP,   LED_CONDITION_NONE,                   DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_SWITCHING_TO_NORMAL,  LED_CONDITION_NONE,                   DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_SWITCHING_TO_RESTART, LED_CONDITION_NONE,                   DK_LED_YELLOW,  0,      dxFALSE, 5},
    {LED_OPERATION_MODE_RESTARTING,           LED_CONDITION_NONE,                   DK_LED_YELLOW,  0,      dxFALSE, 5},

    /* Device mode as temporary for one time access, will return to previous mode when one time is done */
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_ONE_TIME_ACCESS_DENIED, DK_LED_RED, 100, dxTRUE, 6},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_ONE_TIME_ACCESS_DENIED, DK_LED_RED, 100, dxTRUE, 6},
    {LED_OPERATION_MODE_SETUP,    LED_CONDITION_ONE_TIME_ACCESS_DENIED, DK_LED_RED, 100, dxTRUE, 6},
    {LED_OPERATION_MODE_INIT,     LED_CONDITION_ONE_TIME_IDENTIFY,      DK_LED_BLUE, 100, dxTRUE, 15},
    {LED_OPERATION_MODE_NORMAL,   LED_CONDITION_ONE_TIME_IDENTIFY,      DK_LED_BLUE, 100, dxTRUE, 15},

    /* FIRMWARE download/update */
    {LED_OPERATION_MODE_SWITCHING_TO_FW_DOWNLOAD,             LED_CONDITION_NONE,                       DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_FW_DOWNLOAD,                          LED_CONDITION_NONE,                       DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_FW_DOWNLOAD,                          LED_CONDITION_FW_DOWNLOAD_UPDATE_ERROR,   DK_LED_RED,     0,      dxFALSE, 5},
    {LED_OPERATION_MODE_SWITCHING_TO_BLE_CENTRAL_FW_UPDATE,   LED_CONDITION_NONE,                       DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_BLE_CENTRAL_FW_UPDATE,                LED_CONDITION_NONE,                       DK_LED_YELLOW,  100,    dxFALSE, 5},
    {LED_OPERATION_MODE_BLE_CENTRAL_FW_UPDATE,                LED_CONDITION_FW_DOWNLOAD_UPDATE_ERROR,   DK_LED_RED,     0,      dxFALSE, 5},

    /* Un-specified Mode-LED mapping will get here */
    {LED_OPERATION_MODE_UNKNOWN, LED_CONDITION_NONE, DK_LED_RED, 100, dxTRUE, 20},

};

/******************************************************
 *               Function Definitions
 ******************************************************/

static LedManager_result_t LedRetainBuffPermissionAccess()
{
    LedManager_result_t result = LED_MANAGER_SUCCESS;

    if (dxSemTake(LedBufferSemaphore, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
    {
        result = LED_MANAGER_RTOS_ERROR;
    }

    return result;

}


static LedManager_result_t LedReleaseBuffPermissionAccess()
{
    LedManager_result_t result = LED_MANAGER_SUCCESS;

    if (dxSemGive(LedBufferSemaphore) != DXOS_SUCCESS)
    {
        result = LED_MANAGER_RTOS_ERROR;
    }

    return result;
}


static LedManager_result_t LedSendRequestToMessageQueue(Device_Led_t* SendMsg)
{
    LedManager_result_t Result = LED_MANAGER_SUCCESS;

    if (SendMsg)
    {
        if (LedJobProcessQueue)
        {
            Device_Led_t* LedSendMsg = dxMemAlloc( "Led Send Msg Buff", 1, sizeof(Device_Led_t));

            if (LedSendMsg)
            {
                memcpy(LedSendMsg, SendMsg, sizeof(Device_Led_t));

                if (dxQueueIsFull(LedJobProcessQueue) == dxTRUE)
                {
                    G_SYS_DBG_LOG_V( "LedSendRequestToMessageQueue - Error pushing message to queue (Queue is FULL)\r\n" );
                    Result = LED_MANAGER_QUEUE_FULL_ERROR;
                    dxMemFree(LedSendMsg);
                }
                else if (dxQueueSend(   LedJobProcessQueue,
                                        &LedSendMsg,
                                        1000) != DXOS_SUCCESS)
                {
                    G_SYS_DBG_LOG_V( "LedSendRequestToMessageQueue - Error pushing message to queue (Could be due to timeout)\r\n" );
                    Result = LED_MANAGER_SEND_MESSAGE_ERROR;
                    dxMemFree(LedSendMsg);
                }
            }
            else
            {
                G_SYS_DBG_LOG_V( "LedSendRequestToMessageQueue - Error pushing message to queue (Out of system memory)\r\n" );
                Result = LED_MANAGER_OUT_OF_MEMORY_ERROR;
            }
        }
        else
        {
            G_SYS_DBG_LOG_V( "LedSendRequestToMessageQueue - Error pushing message to queue (Job process queue is NULL)\r\n" );
            Result = LED_MANAGER_QUEUE_BUFFER_NULL;
        }
    }
    else
    {
        G_SYS_DBG_LOG_V( "LedSendRequestToMessageQueue - Error pushing message to queue (Invalid parameter)\r\n" );
        Result = LED_MANAGER_INVALID_PARAMETER;
    }

    return Result;
}


LedManager_result_t SetLedMode(Led_Operation_Mode_t LedMode)
{
    LedManager_result_t Result = LED_MANAGER_SUCCESS;
    Device_Led_t SendMsg;

    if ( GetLedMode() != LedMode )
    {

        LedRetainBuffPermissionAccess();

        SendMsg.LedMode = LedMode;
        SendMsg.LedCondition = LED_CONDITION_NONE;
        Result = LedSendRequestToMessageQueue(&SendMsg);

        LedReleaseBuffPermissionAccess();

    }
    else
    {
        //G_SYS_DBG_LOG_V( "SetLedMode:: NO CHANGE !!!\r\n" );
    }

    return Result;
}


LedManager_result_t SetLedCondition(Led_Condition_t LedCondition)
{
    LedManager_result_t Result = LED_MANAGER_SUCCESS;
    Device_Led_t SendMsg;

    //G_SYS_DBG_LOG_V1( "SetLedCondition to : %d\r\n",  Condition);

    LedRetainBuffPermissionAccess();

    SendMsg.LedMode = 0xFF;
    SendMsg.LedCondition = LedCondition;
    Result = LedSendRequestToMessageQueue(&SendMsg);

    LedReleaseBuffPermissionAccess();

    return Result;
}


LedManager_result_t SetLedModeCondition(Led_Operation_Mode_t LedMode, Led_Condition_t LedCondition)
{
    LedManager_result_t Result = LED_MANAGER_SUCCESS;
    Device_Led_t SendMsg;

    //G_SYS_DBG_LOG_V2( "SetLedModeCondition to : %d,%d\r\n",  LedMode, LedCondition);

    LedRetainBuffPermissionAccess();

    SendMsg.LedMode = LedMode;
    SendMsg.LedCondition = LedCondition;
    Result = LedSendRequestToMessageQueue(&SendMsg);

    LedReleaseBuffPermissionAccess();

    //G_SYS_DBG_LOG_V3( "SetLedModeCondition: %d  %d - %d.\r\n", SendMsg.LedMode, SendMsg.LedCondition, Result );

    return Result;
}


Led_Operation_Mode_t GetLedMode(void)
{
    return CurrentDeviceLed.LedMode;
}


Led_Condition_t GetLedCondition(void)
{
    return CurrentDeviceLed.LedCondition;
}


static Led_Table_t* GetLedTable( Led_Operation_Mode_t LedMode, Led_Condition_t LedCondition )
{
    uint16_t i = 0;
    Led_Table_t *LedTable;

    for (i = 0; i < (LedMapSize/sizeof(Led_Table_t)); i++)
    {
        LedTable = &LedMap[i];

        if (    (LedTable->LedOperMode == LedMode) &&
                (LedTable->LedCondition == LedCondition ) )
        {
            break;
        }
    }

    return LedTable;
}


static void LedTimeTickHandler(dxTimerHandle_t dkTimer)
{
    static dxBOOL LedState = dxFALSE;
    static uint8_t FlashCnt = 0;

    LedState = !LedState;

    //Transfer LED status
    if ( LedState == dxTRUE )
    {
        dxGPIO_Write(JobBuff.LedColor, DK_LED_CONTROL_LEVEL_ON);
    }
    else
    {
        dxGPIO_Write(JobBuff.LedColor, DK_LED_CONTROL_LEVEL_OFF);
    }

    // Once-time flash check
    if (JobBuff.ExecuteOnceTime == dxTRUE)
    {
        //G_SYS_DBG_LOG_V1( "Led_Process_handler: %d\r\n", FlashCnt);

        if (++FlashCnt >= 2*JobBuff.FlashNumber)
        {
            Led_Table_t *LedStateTable;

            FlashCnt = 0;
            LEDOnceTimeIfDone = dxTRUE;

            // Perform previous led mode/condition state
            LedStateTable = GetLedTable( CurrentDeviceLed.LedMode, CurrentDeviceLed.LedCondition );
            memcpy(&JobBuff, LedStateTable, sizeof(Led_Table_t));
            LedOperationStart();
        }
    }
}


static void LedConfigTimer(dxTime_t time_ms)
{
    if ( time_ms > 0 )
    {
        if (dxTimerRestart(LEDSessionTimer, time_ms, DXOS_WAIT_FOREVER) != DXOS_SUCCESS)
        {
            G_SYS_DBG_LOG_V( "WARNING - Unable to restart LEDSessionTimer\r\n" );
        }
    }
}


static void LedOperationStart(void)
{
    /*
    G_SYS_DBG_LOG_V6( "Led_Process_thread_main: %d  %d  %d  %ld  %d  %d !!\r\n",
                        JobBuff.LedOperMode,
                        JobBuff.LedCondition,
                        JobBuff.LedColor,
                        JobBuff.FlashIntervalTime,
                        JobBuff.ExecuteOnceTime,
                        JobBuff.FlashNumber );
    */

    // Turn off all LEDS
    dxGPIO_Write(DK_LED_RED, DK_LED_CONTROL_LEVEL_OFF);
    dxGPIO_Write(DK_LED_BLUE, DK_LED_CONTROL_LEVEL_OFF);
    dxGPIO_Write(DK_LED_YELLOW, DK_LED_CONTROL_LEVEL_OFF);

    // Start LED operation
    if (JobBuff.FlashIntervalTime > 0)
    {
        if (JobBuff.ExecuteOnceTime == dxTRUE)
        {
            // NOTE: after one time control is finished will need to
            // go back to previous mode/condition automatically. So,
            // no need to update current gateway mode/condition.
            LEDOnceTimeIfDone = dxFALSE;
        }
        else
        {
            // Record current gateway mode and condition
            CurrentDeviceLed.LedMode = JobBuff.LedOperMode;
            CurrentDeviceLed.LedCondition = JobBuff.LedCondition;
        }

        LedConfigTimer(JobBuff.FlashIntervalTime);
    }
    else
    {
        dxTimerStop(LEDSessionTimer, DXOS_WAIT_FOREVER);

        dxGPIO_Write(JobBuff.LedColor, DK_LED_CONTROL_LEVEL_ON);

        // Record current gateway mode and condition
        CurrentDeviceLed.LedMode = JobBuff.LedOperMode;
        CurrentDeviceLed.LedCondition = JobBuff.LedCondition;
    }
}


static TASK_RET_TYPE Led_Process_thread_main( TASK_PARAM_TYPE arg )
{
    while (1)
    {
        Led_Table_t *LedStateTable;
        Device_Led_t *pGetJobBuff = NULL;
        Device_Led_t LedState;

        // Waiting for once-time case is done
        if ( LEDOnceTimeIfDone == dxFALSE )
        {
            dxTimeDelayMS(10);
            continue;
        }

        if (dxQueueReceive(  LedJobProcessQueue,
                             &pGetJobBuff,
                             DXOS_WAIT_FOREVER) == DXOS_SUCCESS)
        {
            if (pGetJobBuff)
            {
                memcpy(&LedState, pGetJobBuff, sizeof(Device_Led_t));
                dxMemFree(pGetJobBuff);

                // Use current mode or condition
                if (LedState.LedMode == 0xFF)
                {
                    LedState.LedMode = CurrentDeviceLed.LedMode;
                }

                // Get LED state table
                LedStateTable = GetLedTable( LedState.LedMode, LedState.LedCondition );
                memcpy(&JobBuff, LedStateTable, sizeof(Led_Table_t));

                //We only handle known operation mode, otherwise we could be stuck until
                //led finishes its sequence which could lead to queue overrun if application
                //keeps setting the mode.
                if (JobBuff.LedOperMode != LED_OPERATION_MODE_UNKNOWN)
                {
                    if ( (CurrentDeviceLed.LedMode != JobBuff.LedOperMode) ||
                         (CurrentDeviceLed.LedCondition != JobBuff.LedCondition) )
                    {
                        LedOperationStart();
                    }
                }
                else
                {
#if 0
#pragma  message ( "WARNING: MAKE SURE LedManager Thread stack size is big enough for debug log!!!" )

#endif
                    G_SYS_DBG_LOG_V( YELLOW("WARNING: Gateway OpMode/Condition LED mapping missing!!! \r\n"));

                }
            }
        }
    }

    END_OF_TASK_FUNCTION;
}


LedManager_result_t LedManagerInit(void)
{
    LedManager_result_t result = LED_MANAGER_SUCCESS;

    // Initial LedMap to default
    LedMap = DefaultLedMap;
    LedMapSize = sizeof(DefaultLedMap);

    // Initial gateway mode & condition
    CurrentDeviceLed.LedMode = LED_OPERATION_MODE_UNKNOWN;
    CurrentDeviceLed.LedCondition = LED_CONDITION_UNKNOWN;

    // Create a semaphore and initialise it to 1
    LedBufferSemaphore = dxSemCreate(1, 1);
    if (!LedBufferSemaphore)
    {
        G_SYS_DBG_LOG_V(  "Error creating LedBufferSemaphore \r\n" );
        result = LED_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    // Create Queue
    LedJobProcessQueue = dxQueueCreate(MAXIMUM_LED_JOB_QUEUE_DEPT_ALLOWED, sizeof(Led_Table_t*));
    if (!LedJobProcessQueue)
    {
        G_SYS_DBG_LOG_V(  "Error creating LedJobProcessQueue \r\n" );
        dxSemDelete(LedBufferSemaphore);
        result = LED_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    // RTOS timer initialization
    LEDSessionTimer = dxTimerCreate("State manager Timer",
                                    LED_TIMER_TICK,
                                    dxTRUE,
                                    NULL,
                                    LedTimeTickHandler);
    if (!LEDSessionTimer)
    {
        G_SYS_DBG_LOG_V(RED( "WARNING - LED Service unable to create RTOS timer\r\n" ));
        dxSemDelete(LedBufferSemaphore);
        dxQueueDelete(LedJobProcessQueue);
        result = LED_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    // Create Thread
    if  (dxTaskCreate(  Led_Process_thread_main,
                        LED_MANAGER_PROCESS_THREAD_NAME,
                        LED_MANAGER_STACK_SIZE,
                        NULL,
                        LED_MANAGER_PROCESS_THREAD_PRIORITY,
                        &LedMainProcess_thread) != DXOS_SUCCESS)
    {
        G_SYS_DBG_LOG_V( "Error creating LedMainProcess_thread\r\n");
        dxSemDelete(LedBufferSemaphore);
        dxQueueDelete(LedJobProcessQueue);
        dxTimerDelete(LEDSessionTimer, DXOS_WAIT_FOREVER);
        result = LED_MANAGER_RTOS_ERROR;
        goto EndInit;
    }

    // Register Thread
    GSysDebugger_RegisterForThreadDiagnostic(&LedMainProcess_thread);

EndInit:
    return result;
}

LedManager_result_t SetLedCustomTable(Led_Table_t* CustomLedTable, uint16_t SizeofTable)
{
    LedManager_result_t result = LED_MANAGER_SUCCESS;

    //Make sure the size is right
    if (CustomLedTable && !(SizeofTable%sizeof(Led_Table_t)))
    {
        LedMap = CustomLedTable;
        LedMapSize = SizeofTable;
    }
    else
    {
        G_SYS_DBG_LOG_V( "WARNING: SetLedCustomTable Invalid Parameter\r\n");
        result = LED_MANAGER_INVALID_PARAMETER;
    }

    return result;
}
