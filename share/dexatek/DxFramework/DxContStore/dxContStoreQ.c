//============================================================================
// File: dxContStoreQ.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/7
//     1) Description: Initial
//
//============================================================================
#include "dxOS.h"
#include "dxContStore.h"
#include "dxContStoreQ.h"

static ContStoreQueueNode_t *pCSQHead = NULL;
static ContStoreQueueNode_t *pCSQTail = NULL;

void CSQ_Empty(void)
{
    ContStoreQueueNode_t *p = NULL;

    p = pCSQHead;
    while (p)
    {
        dxContStoreMTableItemCache_t *q = p->pUserData;

        if (q)
        {
            dxMemFree(q);
            q = NULL;
        }

        pCSQHead = p->Next;
        dxMemFree(p);
        p = pCSQHead;
    }

    pCSQHead = NULL;
    pCSQTail = NULL;
}

uint8_t CSQ_IsEmpty(void)
{
    uint8_t ret = 1;

    if (pCSQHead && pCSQTail)
    {
        ret = 0;
    }

    return ret;
}

uint8_t CSQ_Push(dxContStoreMTableItemCache_t *pUserData)
{
    uint8_t ret = 0;

    if (pUserData == NULL)
    {
        return ret;
    }

    ContStoreQueueNode_t *pNode = (ContStoreQueueNode_t *)dxMemAlloc("CSQ", 1, sizeof(ContStoreQueueNode_t));
    if (pNode)
    {
        pNode->pUserData = (dxContStoreMTableItemCache_t *)dxMemAlloc("CSQ", 1, sizeof(dxContStoreMTableItemCache_t));
        if (pNode->pUserData)
        {
            memcpy(pNode->pUserData, pUserData, sizeof(dxContStoreMTableItemCache_t));

            if (pCSQTail == NULL)
            {
                pCSQTail = pNode;
                pCSQTail->Next = NULL;
                pCSQHead = pNode;
                pCSQHead->Next = NULL;
            }
            else
            {
                pCSQTail->Next = pNode;
                pCSQTail = pNode;
            }

            ret = 1;
        }
    }
/*
    dxContStoreMTableItemCache_t *q = (dxContStoreMTableItemCache_t *)dxMemAlloc("CSQ", 1, sizeof(dxContStoreMTableItemCache_t));

    if (q)
    {
        memcpy(q, pUserData, sizeof(dxContStoreMTableItemCache_t));

        ContStoreQueueNode_t *pNode = (ContStoreQueueNode_t *)dxMemAlloc("CSQ", 1, sizeof(ContStoreQueueNode_t));
        if (pNode)
        {
            pNode->pUserData = q;

            if (pCSQTail == NULL)
            {
                pCSQTail = pNode;
                pCSQTail->Next = NULL;
                pCSQHead = pNode;
                pCSQHead->Next = NULL;
            }
            else
            {
                pCSQTail->Next = pNode;
                pCSQTail = pNode;
            }
        }

        ret = 1;
    }
*/
    return ret;
}

uint8_t CSQ_Pop(dxContStoreMTableItemCache_t **pUserData)
{
    uint8_t ret = 0;
    ContStoreQueueNode_t *pNode = NULL;

    if (pCSQHead)
    {
        if (pCSQHead == pCSQTail)
        {
            pNode = pCSQHead;
            pCSQHead = NULL;
            pCSQTail = NULL;
            pNode->Next = NULL;
        }
        else
        {
            pNode = pCSQHead;
            pCSQHead = pCSQHead->Next;
            pNode->Next = NULL;
        }

        if (pUserData == NULL)
        {
            if (pNode->pUserData)
            {
                dxMemFree(pNode->pUserData);
                pNode->pUserData = NULL;
            }

            dxMemFree(pNode);
            pNode = NULL;
        }
        else
        {
            *pUserData = pNode->pUserData;
            pNode->pUserData = NULL;
            dxMemFree(pNode);
            pNode = NULL;
        }

        ret = 1;
    }

    return ret;
}

uint32_t CSQ_Count(void)
{
    uint32_t ret = 0;

    if (pCSQHead == NULL || pCSQTail == NULL)
    {
        return 0;
    }

    ContStoreQueueNode_t *pNode = pCSQHead;
    while (pNode)
    {
        ret++;

        pNode = pNode->Next;
    }

    return ret;
}

dxContStoreMTableItemCache_t *CSQ_GetAt(uint32_t Index)
{
    dxContStoreMTableItemCache_t *pUserData = NULL;

    if (pCSQHead == NULL || pCSQTail == NULL)
    {
        return NULL;
    }

    uint32_t Count = 0;
    ContStoreQueueNode_t *pNode = pCSQHead;
    while (pNode)
    {
        if (Index == Count)
        {
            pUserData = pNode->pUserData;
            break;
        }
        Count++;

        pNode = pNode->Next;
    }

    return pUserData;
}

dxContStoreMTableItemCache_t *CSQ_FindByBlockID(uint8_t BlockID)
{
    dxContStoreMTableItemCache_t *pUserData = NULL;

    if (pCSQHead == NULL || pCSQTail == NULL)
    {
        return NULL;
    }

    ContStoreQueueNode_t *pNode = pCSQHead;
    while (pNode)
    {
        if (pNode->pUserData &&
            pNode->pUserData->Block.BlockID == BlockID)
        {
            pUserData = pNode->pUserData;
            break;
        }

        pNode = pNode->Next;
    }

    return pUserData;
}

void CSQ_FreeNode(ContStoreQueueNode_t *pNode)
{
    if (pNode)
    {
        if (pNode->pUserData)
        {
            dxMemFree(pNode->pUserData);
            pNode->pUserData = NULL;
        }

        dxMemFree(pNode);
        pNode = NULL;
    }
}

uint8_t CSQ_RemoveBlockID(uint8_t BlockID)
{
    if (pCSQHead == NULL || pCSQTail == NULL)
    {
        return 0;
    }

    ContStoreQueueNode_t *pNodePrev = pCSQHead;
    ContStoreQueueNode_t *pNodeLast = NULL;
    while (pNodePrev)
    {
        if (pNodePrev->pUserData &&
            pNodePrev->pUserData->Block.BlockID == BlockID)
        {
            ContStoreQueueNode_t *p = pNodePrev;

            if (pNodePrev->Next)
            {
                if (pNodePrev == pCSQHead)
                {
                    pCSQHead = pNodePrev->Next;
                }
                pNodePrev = pNodePrev->Next;
                pNodeLast->Next = pNodePrev;

                CSQ_FreeNode(p);
                p = NULL;
            }
            else
            {
                // End of searching
                if (pNodePrev == pCSQHead &&
                    pNodePrev == pCSQTail)
                {
                    pCSQHead = NULL;
                    pCSQTail = NULL;
                }
                else
                {
                    if (pNodeLast)
                    {
                        pCSQTail = pNodeLast;
                        pCSQTail->Next = NULL;
                    }
                }
                pNodePrev = NULL;

                CSQ_FreeNode(p);
                p = NULL;

                break;
            }

            CSQ_FreeNode(p);
            p = NULL;
        }
        else
        {
            pNodeLast = pNodePrev;
            pNodePrev = pNodePrev->Next;
        }
    }

    return 1;
}
