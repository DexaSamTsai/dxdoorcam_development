//============================================================================
// File: dxContStore.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/03
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/03/06
//     1) Description: Revise API
//
//     Version: 0.3
//     Date: 2017/03/30
//     1) Description: Provide new API to erase flash space
//        Added:
//            - dxContStore_Erase()
//
//     Version: 0.4
//     Date: 2017/04/24
//     1) Description: Revise callback function to report error code
//        Modified:
//            - DXCONTSTORE_ACCESSCOMPLETION_CALLBACK
//            - dxContStoreAccessStatus_t
//
//     Version: 0.5
//     Date: 2017/05/04
//     1) Description: Support CRC
//        Added:
//            - Define CONFIG_SUPPORT_CRC16
//            - CONTSTORE_ERROR_CRC16_VERIFY_FAIL
//
//     Version: 0.6
//     Date: 2017/06/28
//     1) Description: Added new error code to dxContStoreErrorCode_t
//        Modified:
//            - dxContStoreErrorCode_t
//============================================================================
#ifndef _DXCONTSTORE_H
#define _DXCONTSTORE_H

#pragma once

#include "dxBSP.h"

#ifdef __cplusplus
extern "C" {
#endif

//============================================================================
// Defines and Macro
//============================================================================

/**
 * CRC-16 value is calculated by CRC-16 CCITT Kermit.
 * And,
 * (1) will be calculated when receving data and before sending it to worker thread's queue.
 * (2) will be verified while read PAYLOAD from flash memory.
 * (3) will be verified just before writing into flash memory.
 */
#define CONFIG_SUPPORT_CRC16                        0

//============================================================================
// Structure and Enumerate
//============================================================================

typedef enum dxContStoreSectorID
{
  CONTSTORE_SECTOR_ONE      = 0,

  CONTSTORE_SECTOR_COUNT    = 1         // CONTSTORE_SECTOR_COUNT should be last one
} dxContStoreSectorID_t;

typedef enum dxContStoreErrorCode
{
    CONTSTORE_ERROR_SUCCESS                 = 0,
    CONTSTORE_ERROR_UNKNOWN_FORMAT          = -1,
    CONTSTORE_ERROR_ERROR                   = -2,
    CONTSTORE_ERROR_INVALID_HANDLE          = -3,
    CONTSTORE_ERROR_SIZE_LIMIT_REACHED      = -4,
    CONTSTORE_ERROR_NOT_FOUND               = -5,
    CONTSTORE_ERROR_NOT_ENOUGH_FLASH_SPACE  = -6,
    CONTSTORE_ERROR_NOT_ENOUGH_HEAP         = -7,
    CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE  = -8,
    CONTSTORE_ERROR_WRITE_READ_VERIFY_FAIL  = -9,
    CONTSTORE_ERROR_CRC16_VERIFY_FAIL       = -10,

    CONTSTORE_ERROR_INCORRECT_PAYLOADLEN    = -11,
    CONTSTORE_ERROR_INVALID_PARAMETER       = -12,
    CONTSTORE_ERROR_DATAPAGE_FINDPAYLOAD_FAIL   = -13,
    CONTSTORE_ERROR_FLASH_SIZE_NOT_SUPPORT  = -14,
    CONTSTORE_ERROR_CREATE_WORKER_FAIL      = -15,

    CONTSTORE_ERROR_FLASH_READ_FAIL         = -16,
    CONTSTORE_ERROR_FLASH_WRITE_FAIL        = -17,
    CONTSTORE_ERROR_FLASH_ERASE_FAIL        = -18,

    // ERROR code add before here
    CONTSTORE_ERROR_NOT_SUPPORT             = -100,
} dxContStoreErrorCode_t;

typedef enum dxContStoreAccessStatus
{
    CONTSTORE_ACCESS_READ               = 1,
    CONTSTORE_ACCESS_FIND               = 2,
    CONTSTORE_ACCESS_WRITE              = 3,
} dxContStoreAccessStatus_t;

typedef struct dxContStore_Filter
{
    uint32_t MID;                       // Container's MID
    uint32_t DID;                       // Container's DID
} dxContStore_Filter_t;

typedef struct dxContStoreData_Header
{
    uint32_t MID;                       // Container's MID
    uint32_t DID;                       // Container's DID
    uint32_t SeqNo;                     // 1 ~ 0xFFFFFFFF
#if CONFIG_SUPPORT_CRC16
    uint16_t    CRC16;
    uint8_t     Reserved[2];            // MUST be 0x0000
#endif // CONFIG_SUPPORT_CRC16
    uint32_t PayloadLen;                // Length of payload
} dxContStoreData_Header_t;

typedef struct dxContStoreData
{
    dxContStoreData_Header_t Header;
    uint8_t *pPayload;                  // Payload
} dxContStoreData_t;

typedef struct dxContStoreMTableHeader
{
    uint64_t    Tag;                    // Must be DX_CONTSTORE_MARK (check dxContStore.c)
    uint8_t     Version;                // Must not 0x01
    uint8_t     Reserved[5];            // Reserved. Must be 0x00000000
    uint16_t    Size;                   // sizeof(dxContStoreMTableHeader_t). Size will be padded to 4 bytes alignment.
} dxContStoreMTableHeader_t;

/**
 * @param EventStatus       Refer to dxContStoreAccessStatus_t
 * @param ClientId
 * @param pStoreData
 * @param NumberOfStoreData
 * @param nErrorCode        Refer to dxContStoreErrorCode_t
 */
typedef void (*DXCONTSTORE_ACCESSCOMPLETION_CALLBACK)(dxContStoreAccessStatus_t EventStatus, uint16_t ClientId, dxContStoreData_t *pStoreData, uint32_t NumberOfStoreData, dxContStoreErrorCode_t nErrorCode);

typedef void * dxCSHandle_t;

/**
 * @brief Initial container store API.
 * @retval CONTSTORE_ERROR_SUCCESS Success
 * @retval CONTSTORE_ERROR_UNKNOWN_FORMAT Caller should call dxContStore_FormatHeader() to format flash when receiving the error code.
 */
dxContStoreErrorCode_t dxContStore_Init(dxContStoreSectorID_t SectorID, uint32_t StartAddr, uint32_t TotalSize, DXCONTSTORE_ACCESSCOMPLETION_CALLBACK pCompletion, dxCSHandle_t *hCS);

/**
 * @brief Reformat flash header. dxContStore_Init() will return CONTSTORE_ERROR_UNKNOWN_FORMAT if flash area is empty.
 */
dxContStoreErrorCode_t dxContStore_FormatHeader(dxContStoreSectorID_t SectorID, dxCSHandle_t *hCS);

/**
 * @brief Erase flash space
 */
dxContStoreErrorCode_t dxContStore_Erase(uint32_t StartAddr, uint32_t TotalSize);

/**
 * @brief Read all container data
 * @param ClientId The value will be send back through callback function
 * @param pFilter Will not support on first phrase
 */
dxContStoreErrorCode_t dxContStore_ReadAll(dxCSHandle_t hCS, uint16_t ClientId, dxContStore_Filter_t *pFilter);

/**
 * @brief Find container data
 * @param ClientId The value will be send back through callback function
 * @param pFilter Will not support on first phrase
 */
dxContStoreErrorCode_t dxContStore_Find(dxCSHandle_t hCS, uint16_t ClientId, uint32_t MID, uint32_t DID);

/**
 * @brief Write container data
 * @param ClientId The value will be send back through callback function
 */
dxContStoreErrorCode_t dxContStore_Write(dxCSHandle_t hCS, uint16_t ClientId, uint32_t MID, uint32_t DID, uint8_t *pPayload, uint32_t PayloadLen);

/**
 * @brief Free allocated memory
 */
dxContStoreErrorCode_t dxContStore_Uninit(dxCSHandle_t hCS);

#ifdef __cplusplus
}
#endif

#endif // _DXCONTSTORE_H
