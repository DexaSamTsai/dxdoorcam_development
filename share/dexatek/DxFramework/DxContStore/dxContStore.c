//============================================================================
// File: dxContStore.c
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/03
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/03/30
//     1) Description: Provide new API to erase flash space
//        Added:
//            - dxContStore_Erase()
//
//     Version: 0.3
//     Date: 2017/04/24
//     1) Description: Enhance stability
//        Added:
//            - ADC0_seed32()
//            - dxFlash_WriteVerify()
//        Modified:
//            - dxContStore_DataPage_AppendData()
//            - dxContStore_Internal_ReadAll()
//            - dxContStore_Internal_Find()
//            - dxContStore_Internal_WriteNewData()
//            - dxContStore_Internal_Write()
//            - dxContStore_MTable_AppendNewBlock()
//            - dxContStore_MTable_InvalidBlock()
//            - dxContStore_MTable_CopyFrom2ndBackup()
//            - dxContStore_MTable_ReclaimSpace()
//            - dxContStore_FormatHeader()
//
//     Version: 0.4
//     Date: 2017/05/04
//     1) Description: Support CRC
//        Added:
//            - Define CONFIG_SUPPORT_CRC16
//        Modified:
//            - dxContStore_SendEventToWorkerThread()
//            - dxContStore_LocalCache_Dump()
//            - dxContStore_LocalCache_GenerateCallbackData()
//            - dxContStore_DataPage_LocalCache_AddData()
//            - dxContStore_DataPage_FindPayload()
//            - dxContStore_DataPage_GetDataByBlockID()
//            - dxContStore_DataPage_AppendData()
//            - dxContStore_Internal_Find()
//            - dxContStore_Internal_WriteNewData()
//            - dxContStore_Internal_Write()
//            - dxContStore_ReadAll()
//            - dxContStore_Find()
//            - dxContStore_Write()
//
//     Version: 0.5
//     Date: 2017/06/28
//     1) Description: Fixed issue that incorrect data read back from dxContStore after renew
//        Modified:
//            - dxContStore_DataPage_FindPayload()
//
//     Version: 0.6
//     Date: 2017/06/29
//     1) Description: Fixed issue when write data into dxContStore.
//                     Before writing, we need to read back payload but found nothing.
//        Modified:
//            - dxContStore_WorkerThreadEventHandler()
//        Added:
//            - dxContStore_Internal_WriteWithRetry()
//============================================================================
#include "dxOS.h"
#include "dxContStore.h"
#include "dxMemConfig.h"
#include "dxContStoreQ.h"

//============================================================================
// Defines and Macro
//============================================================================
#define CONFIG_ENABLE_PRINTF                        0
#define dxDIRECT_DEBUG_PRINT                        0
#if dxDIRECT_DEBUG_PRINT
    #define DX_PRINT                                if (CONFIG_ENABLE_PRINTF) printf
#else // dxDIRECT_DEBUG_PRINT
    #include "dxSysDebugger.h"
    #define DX_PRINT                                if (CONFIG_ENABLE_PRINTF) G_SYS_DBG_LOG_NV
#endif // dxDIRECT_DEBUG_PRINT

#define CONFIG_ENABLE_DEBUG_ERROR_ERROR             0
#if CONFIG_ENABLE_DEBUG_ERROR_ERROR
    #include "dxSysDebugger.h"
    #define ERROR_PRINT                             G_SYS_DBG_LOG_NV
#else
	#define ERROR_PRINT(...)
#endif

#define CONFIG_USE_CSQ                              1
#define CONFIG_DATAPAGE_WRITE_RETRY_COUNT           1


#define DX_CONTSTORE_ALLOC_NAME                     "DXCS"
#define DX_CONTSTORE_WORKER_THREAD_NAME             "DXCSW"
#define DX_CONTSTORE_WORKER_THREAD_PRIORITY         (DX_DEFAULT_WORKER_PRIORITY)
#define DX_CONTSTORE_WORKER_THREAD_STACK_SIZE       (2 * 640)
#define DX_CONTSTORE_WORKER_THREAD_MAX_EVENT_QUEUE  3

#define DX_CONTSTORE_MARK               0xF1E2D3C4B5A69788
#define DX_CONTSTORE_VER                0x01
#define DX_CONTSTORE_ENDOFBLOCK1        0xCC
#define DX_CONTSTORE_ENDOFBLOCK2        0xDD

#define DX_CONTSTORE_BLOCK_FLAG_VALID   1
#define DX_CONTSTORE_BLOCK_FLAG_INVALID 0

#define DX_CONTSTOREDATA_MAX            64//32
#define DX_CONTSTOREPAYLOAD_MEMORY_SIZE 1024 * 6

//============================================================================
// Struture and Enumeration
//============================================================================
typedef enum dxContStoreCommand
{
    CONTSTORE_COMMAND_UNKNOWN       = 0,
    CONTSTORE_COMMAND_READALL       = 1,
    CONTSTORE_COMMAND_FIND          = 2,
    CONTSTORE_COMMAND_WRITE         = 3,
} dxContStoreCommand_t;

typedef struct dxContStoreMessageHeader
{
    uint8_t     CmdID;                  // Refer to dxContStoreCommand_t
    uint8_t     SectorID;               // Refer to dxContStoreSectorID_t
    uint16_t    ClientId;
    uint32_t    MID;
    uint32_t    DID;
#if CONFIG_SUPPORT_CRC16
    uint16_t    CRC16;
    uint8_t     Reserved[2];            // MUST be 0x0000
#endif // CONFIG_SUPPORT_CRC16
    uint32_t    PayloadLen;
} dxContStoreMessageHeader_t;

typedef struct dxContStoreMessage
{
    dxContStoreMessageHeader_t Header;
    uint8_t *pPayload;
} dxContStoreMessage_t;

typedef struct dxContStoreWorker
{
    dxWorkweTask_t      *Handle;
    dxEventHandler_t    RegisterFunc;
} dxContStoreWorker_t;


typedef struct dxContStoreAC_t
{
    uint8_t SectorID;                   // Refer to dxContStoreSectorID_t
    uint8_t nNumberOfDataBlock;         // Should be (nTotalSize - 2 * ( nBackupAddr - nStartAddr)) / 4096
    uint8_t Reserved[2];
    uint32_t nStartAddr;                // The start address in flash
    uint32_t nBackupAddr;               // Second copy of MTable. In general, nStartAddr + 0x1000
    uint32_t nDataAddr;                 // Flash area to store real data. Use dxContStoreData_t data type to perform read/write
    uint32_t nTotalSize;                // Total size in bytes of ContStore including header
    uint32_t nMaxSeqNo;                 // Maximum value of SeqNo that store in BLOCK #1 ~ #3
    uint32_t AvailableTableOffset;      // nStartAddr + sizeof(dxContStoreMTableHeader_t) + AvailableTableOffset will be the address
                                        // that is availabe for writing
                                        //
                                        //      +----------------------------------------+ <== nStartAddr <==+
                                        //      | dxContStoreMTableHeader_t (16 Bytes)   |                   |
                                        //      +----------------------------------------+                   |
                                        //      | dxContStoreMTableItem_t (2 Bytes/Each) |                   |
                                        //      +----------------------------------------+ <== nBackupAddr   |
                                        //      | dxContStoreMTableHeader_t (16 Bytes)   |                   |
                                        //      +----------------------------------------+                   |
                                        //      | dxContStoreMTableItem_t (2 Bytes/Each) |                   |
                                        //  +-- +----------------------------------------+ <== nDataAddr     |
                                        //  |   | dxContStoreData_t (Variant Length)     |  BLOCK #1         |
                                        //  +-- +----------------------------------------+                   |
                                        //  |   | dxContStoreData_t (Variant Length)     |  BLOCK #2         |
                                        //  +-- +----------------------------------------+                   |
                                        //  |   | dxContStoreData_t (Variant Length)     |  BLOCK #3         |
                                        //  |   +----------------------------------------+                   |
                                        //  +=====> nNumberOfDataBlock                                       |
                                        //                                                                   |
                                        //                                                                   +---------------------------+
                                        //                                                                   | dxContStoreMTableHeader_t |
                                        //                                                                   +---------------------------+
                                        //                                                                   | ALREADY USE               |
                                        //                                                                   +---------------------------+
                                        //                                          AvailableTableOffset --> |                           |
                                        //                                                                   +---------------------------+
                                        //
    uint16_t HeaderSize;                // Typically, Sizeof(dxContStoreMTableHeader_t)
    dxMutexHandle_t hMutex;
    dxFlashHandle_t hFlash;
    dxContStoreWorker_t  Worker;
    DXCONTSTORE_ACCESSCOMPLETION_CALLBACK pfnCompletionCallback;
} dxContStoreAC_t;

typedef struct dxCacheMemoryAC
{
    uint32_t StartOffset;
    uint32_t Length;
} dxCacheMemoryAC_t;

typedef struct dxContStoreCache
{
    dxContStoreData_Header_t Header;
    uint8_t  LoadPayloadToRAM;          // Considering SRAM size, if the value is 1 means Payload is loaded to pPayload;
                                        // otherwise, we need to call dxContStore_Find() API to load later.
    uint8_t  BlockID;                   // BlockID where pPayload occupied
    uint8_t  Reserved[2];               // Reserved.
    dxCacheMemoryAC_t Cache;            // To pointer to gStorePayloadMemory that store payload
} dxContStoreCache_t;

//============================================================================
// Static variables
//============================================================================
#pragma default_variable_attributes = @ ".sdram.text"
#if !defined(CONFIG_USE_ATCMD)
static
#endif
dxContStoreAC_t  *pConstStoreAC[CONTSTORE_SECTOR_COUNT] = { NULL };

// gStoreData
//  |
//  |
//  +---> +--------+------------------+---------+----------+-------------------+--------------+
//        | Header | LoadPayloadToRAM | BlockID | Reserved | Cache.StartOffset | Cache.Length |
//        +--------+------------------+---------+----------+-------------------+--------------+
//        |                                 ......                                            |
//        +--------+------------------+---------+----------+-------------------+--------------+
//        |        |                  |         |          |                   |              |
//        +--------+------------------+---------+----------+--------+----------+-------+------+ <== gStoreDataCount
//                                                                  |                  |
//  gStorePayloadMemory                                             |                  V
//   |                                       +----------------------+                  10 (In this example)
//   |                                       |
//   |                                       |
//   +---> +---+---+---+---+---+---+---+---+-V-+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//         |   |   |   |   |   |   |   |   | M | Y |   | P | A | Y | L | O | A | D |   |   |   |   |   |   |   |
//         +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//                                                                                   ^
//                                                                                   |
//                                   gStorePayloadMemoryOccupied --------------------+
#if !defined(CONFIG_USE_ATCMD)
static
#endif // CONFIG_USE_ATCMD
uint32_t gStoreDataCount = 0;

#if !defined(CONFIG_USE_ATCMD)
static
#endif // CONFIG_USE_ATCMD
dxContStoreCache_t gStoreData[DX_CONTSTOREDATA_MAX] = {0};

#if !defined(CONFIG_USE_ATCMD)
static
#endif // CONFIG_USE_ATCMD
uint32_t gStorePayloadMemoryOccupied = 0;

#if !defined(CONFIG_USE_ATCMD)
static
#endif // CONFIG_USE_ATCMD
uint8_t gStorePayloadMemory[DX_CONTSTOREPAYLOAD_MEMORY_SIZE] = {0};  // Use to store pPayload
#pragma default_variable_attributes =

//============================================================================
// Forward Declaration Function
//============================================================================
dxContStoreErrorCode_t dxContStore_MTable_AppendNewBlock(dxContStoreSectorID_t SectorID, uint8_t BlockID);
dxContStoreErrorCode_t dxContStore_MTable_InvalidBlock(dxContStoreSectorID_t SectorID, uint8_t BlockID);
dxContStoreErrorCode_t dxContStore_DataPage_FindPayload(dxContStoreAC_t *pContStore, uint8_t nBlockID, uint32_t nMID, uint32_t nDID, uint32_t nSeqNo, uint8_t *pPayload, uint32_t *pPayloadLen);

//============================================================================
// External Function
//============================================================================

extern uint32_t ADC0_seed32(void);
extern uint16_t crc16_ccitt_kermit(uint8_t *pData, uint16_t len);
extern void DK_PRINT_HEX(uint8_t *pData, int len);

//============================================================================
// Function Defines
//============================================================================

int dxContStore_MTable_IsFull(dxContStoreSectorID_t SectorID);
dxContStoreErrorCode_t dxContStore_MTable_ReclaimSpace(dxContStoreSectorID_t SectorID);
dxContStoreErrorCode_t dxContStore_MTable_LoadIntoCache(dxContStoreSectorID_t SectorID);
int dxContStore_MTable_GetNumberOfUseBlock(dxContStoreSectorID_t SectorID);

dxContStoreErrorCode_t dxFlash_WriteVerify(dxFlashHandle_t flash, uint32_t address, uint32_t len, uint8_t *data)
{
    if (flash == NULL ||
        len == 0 ||
        data == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    // 1. Write
    dxOS_RET_CODE r = dxFlash_Write(flash,
                                    address,
                                    len,
                                    data);

    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Write():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_WRITE_FAIL;
    }

    // 2. Allocate Buffer
    uint8_t *pReadBuffer = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, len);

    if (pReadBuffer == NULL)
    {
        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    memset(pReadBuffer, 0x00, len);

    // 3. Read data back
    r = dxFlash_Read(flash,
                     address,
                     len,
                     pReadBuffer);

    if (r != DXOS_SUCCESS)
    {
        dxMemFree(pReadBuffer);
        ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_READ_FAIL;
    }

    // 4. Compare read/write data
    int n = memcmp((void *)pReadBuffer, data, len);

    dxMemFree(pReadBuffer);

    if (n != 0)
    {
        return CONTSTORE_ERROR_WRITE_READ_VERIFY_FAIL;
    }

    return CONTSTORE_ERROR_SUCCESS;
}

/**
 * @brief dxContStore_SendEventToWorkerThread
 */
dxContStoreErrorCode_t dxContStore_SendEventToWorkerThread(dxContStoreSectorID_t SectorID,
                                                           dxContStoreCommand_t nMessageID,
                                                           uint16_t ClientId,
                                                           uint32_t MID,
                                                           uint32_t DID,
                                                           uint16_t CRC16,
                                                           uint8_t *pPayload,
                                                           uint32_t PayloadLen)
{
    dxContStoreMessage_t *pObj = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, sizeof(dxContStoreMessage_t));
    if (pObj == NULL || nMessageID > UINT8_MAX)
    {
        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    pObj->Header.CmdID = (uint8_t)nMessageID;
    pObj->Header.SectorID = SectorID;
    pObj->Header.ClientId = ClientId;
    pObj->Header.MID = MID;
    pObj->Header.DID = DID;

#if CONFIG_SUPPORT_CRC16
    pObj->Header.CRC16 = CRC16;
#endif // CONFIG_SUPPORT_CRC16

    pObj->Header.PayloadLen = PayloadLen;

    if (pPayload && PayloadLen)
    {
        pObj->pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, PayloadLen + 1);
        memcpy(pObj->pPayload, pPayload, PayloadLen);
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t  *pContStore = pConstStoreAC[SectorID];
    dxOS_RET_CODE result = dxWorkerTaskSendAsynchEvent(pContStore->Worker.Handle,
                                                       pContStore->Worker.RegisterFunc,
                                                       pObj);

    return result;
}

/**
 * @brief dxContStore_GetPayload
 */
uint8_t *dxContStore_GetPayload(dxContStoreMessage_t *pEventObject)
{
    uint8_t *p = NULL;
    if (pEventObject && pEventObject->Header.PayloadLen > 0)
    {
        p = pEventObject->pPayload;
    }

    return p;
}

/**
 * @brief dxContStore_GetHeader
 */
dxContStoreMessageHeader_t *dxContStore_GetHeader(dxContStoreMessage_t *pEventObject)
{
    dxContStoreMessageHeader_t *p = NULL;
    if (pEventObject)
    {
        p = &pEventObject->Header;
    }

    return p;
}

dxContStoreErrorCode_t dxContStore_CleanEventArgumentObj(dxContStoreMessage_t *pEventObject)
{
    if (pEventObject)
    {
        if (pEventObject->pPayload)
        {
            dxMemFree(pEventObject->pPayload);
            pEventObject->pPayload = NULL;
        }

        dxMemFree(pEventObject);
        pEventObject = NULL;
    }

    return CONTSTORE_ERROR_SUCCESS;
}

void dxContStore_LocalCache_Payload_Free(dxCacheMemoryAC_t *pCache)
{
    if (pCache && pCache->Length > 0)
    {
        uint32_t nLengthToFree = pCache->Length;

        if (pCache->StartOffset + pCache->Length > sizeof(gStorePayloadMemory))
        {
            nLengthToFree = sizeof(gStorePayloadMemory) - pCache->StartOffset;
        }

        memset(&gStorePayloadMemory[pCache->StartOffset], 0xFF, nLengthToFree);
    }
}

dxContStoreErrorCode_t dxContStore_LocalCache_Payload_AllocateMemory(dxCacheMemoryAC_t *pCache, uint8_t *pPayload, uint32_t PayloadLen)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pCache == NULL ||
        pPayload == NULL ||
        PayloadLen == 0)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (gStorePayloadMemoryOccupied + PayloadLen + 1 > sizeof(gStorePayloadMemory))     // Including 0x00
    {
        return CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE;
    }

    memcpy(&gStorePayloadMemory[gStorePayloadMemoryOccupied], pPayload, PayloadLen);
    gStorePayloadMemory[gStorePayloadMemoryOccupied + PayloadLen] = 0x00;               // Including 0x00
    pCache->StartOffset = gStorePayloadMemoryOccupied;
    pCache->Length = PayloadLen + 1;                                                    // Including 0x00
    gStorePayloadMemoryOccupied += (PayloadLen + 1);                                    // Including 0x00

    return ret;
}

void dxContStore_LocalCache_Free(dxContStoreAC_t *pContStore)
{
    if (pContStore && pContStore->hMutex)
    {
        for (int i = 0; i < gStoreDataCount; i++)
        {
            dxContStoreCache_t *p = &gStoreData[i];
            if (p->Header.PayloadLen > 0 &&
                p->LoadPayloadToRAM == 1)
            {
                dxContStore_LocalCache_Payload_Free(&p->Cache);
            }
            memset(p, 0x00, sizeof(dxContStoreCache_t));
        }

        gStoreDataCount = 0;

        memset(gStorePayloadMemory, 0x00, sizeof(gStorePayloadMemory));
        gStorePayloadMemoryOccupied = 0;
    }
}

#if CONFIG_ENABLE_PRINTF
static void HEX_Dump_Data(uint8_t *pData, int len)
{
    char szPrintBuff[64];
    int i = 0;
    memset(szPrintBuff, 0x00, sizeof(szPrintBuff));

    while (i < len)
    {
        sprintf(szPrintBuff, "%s%02X", szPrintBuff, (pData[i] & 0x0FF));
        i++;
        if (((i % 16) == 0) ||
            (i == len))
        {
            strcat(szPrintBuff, "\n");

            dxTimeDelayMS(50);

            DX_PRINT(szPrintBuff);
            memset(szPrintBuff, 0x00, sizeof(szPrintBuff));
        }
        else
        {
            strcat(szPrintBuff, " ");
        }
    }
}
#endif // CONFIG_ENABLE_PRINTF

void dxContStore_LocalCache_Dump(dxContStoreAC_t *pContStore, uint32_t fromLineNumber)
{
#if CONFIG_ENABLE_PRINTF
    if (pContStore)
    {
        DX_PRINT("=[From Line %d]================================================\r\n", fromLineNumber);
        for (uint32_t i = 0; i < gStoreDataCount; i++)
        {
            dxContStoreCache_t *p = &gStoreData[i];

            uint16_t nCRC16 = 0;
#if CONFIG_SUPPORT_CRC16
            nCRC16 = p->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

            DX_PRINT("BLOCKID:%X MID:%-8X DID:%-8X SEQ:%-8X CRC:%04X LEN:%-4X\r\n",
                     p->BlockID,
                     p->Header.MID,
                     p->Header.DID,
                     p->Header.SeqNo,
                     nCRC16,
                     p->Header.PayloadLen);
        }
        DX_PRINT("=[From Line %d]================================================\r\n", fromLineNumber);
    }
#endif // CONFIG_ENABLE_PRINTF
}

/**
 * @brief dxContStore_LocalCache_Find()
 * @retval -1 NOT FOUND
 * @retval >= 0 Index of gStoreData
 */
int dxContStore_LocalCache_Find(dxContStoreAC_t *pContStore, dxContStoreData_Header_t *pHeader)
{
    int ret = -1;   // NOT FOUND

    if (pContStore && pHeader)
    {
        for (int i = 0; i < gStoreDataCount; i++)
        {
            dxContStoreCache_t *p = &gStoreData[i];
            if (p->Header.MID == pHeader->MID &&
                p->Header.DID == pHeader->DID)
            {
                ret = i;
                break;
            }
        }
    }

    return ret;
}

/**
 * @brief dxContStore_LocalCache_FindBlockID()
 * @retval -1 NOT FOUND
 * @retval >= 0 Index of gStoreData
 */
int dxContStore_LocalCache_FindBlockID(dxContStoreAC_t *pContStore, uint8_t nBlockID)
{
    int ret = -1;   // NOT FOUND

    if (pContStore)
    {
        for (int i = 0; i < gStoreDataCount; i++)
        {
            dxContStoreCache_t *p = &gStoreData[i];

            if (p->BlockID == nBlockID)
            {
                ret = i;
                break;
            }
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_LocalCache_FreeCallbackData(dxContStoreData_t *pStoreData, uint32_t nNumberOfStoreData)
{
    if (pStoreData && nNumberOfStoreData)
    {
        for (uint32_t i = 0; i < nNumberOfStoreData; i++)
        {
            dxContStoreData_t *p = &pStoreData[i];
            if (p)
            {
                if (p->pPayload && p->Header.PayloadLen > 0)
                {
                    dxMemFree(p->pPayload);
                    p->pPayload = NULL;
                }
            }
        }

        dxMemFree(pStoreData);
        pStoreData = NULL;
    }

    return CONTSTORE_ERROR_SUCCESS;
}

dxContStoreErrorCode_t dxContStore_LocalCache_GenerateCallbackData(dxContStoreAC_t *pContStore, dxContStoreData_t **pStoreData, uint32_t *pNumberOfStoreData)
{
    *pNumberOfStoreData = 0;
    if (pStoreData == NULL ||
        pNumberOfStoreData == NULL)
    {
        ERROR_PRINT("[%s,%d] ERROR pStoreData(0x%X) and pNumberOfStoreData(0x%X) should not be NULL\r\n", __FUNCTION__, __LINE__, pStoreData, pNumberOfStoreData);
        return CONTSTORE_ERROR_INVALID_PARAMETER;
    }

    if (gStoreDataCount == 0)
    {
        return CONTSTORE_ERROR_SUCCESS;
    }

    dxContStoreData_t *pDataArray = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, sizeof(dxContStoreData_t) * gStoreDataCount);
    if (pDataArray == NULL)
    {
        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;
    for (uint32_t i = 0; i < gStoreDataCount; i++)
    {
        dxContStoreCache_t *pSrc = &gStoreData[i];
        dxContStoreData_t *p = &pDataArray[i];
        if (p)
        {
            memcpy(&p->Header, &pSrc->Header, sizeof(pSrc->Header));
            p->pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, p->Header.PayloadLen + 1);
            if (p->pPayload == NULL)
            {
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
                break;
            }

            uint32_t nPayloadRead = 0;

            if (pSrc->LoadPayloadToRAM == 1)
            {
                memcpy(p->pPayload, &gStorePayloadMemory[pSrc->Cache.StartOffset], pSrc->Cache.Length);
                nPayloadRead = pSrc->Cache.Length;
            }
            else
            {
                nPayloadRead = p->Header.PayloadLen + 1;

                ret = dxContStore_DataPage_FindPayload(pContStore,
                                                      pSrc->BlockID,
                                                      p->Header.MID,
                                                      p->Header.DID,
                                                      p->Header.SeqNo,
                                                      p->pPayload,
                                                      &nPayloadRead);

#if CONFIG_SUPPORT_CRC16
                // Verify CRC-16
                if (ret == CONTSTORE_ERROR_CRC16_VERIFY_FAIL)
                {
                    G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail while generating CALLBACK data\r\n",
                                     __FUNCTION__, __LINE__);
                }
#endif // CONFIG_SUPPORT_CRC16

                if (ret != CONTSTORE_ERROR_SUCCESS)
                {
                    break;
                }
            }

            uint16_t nCRC16 = 0;
#if CONFIG_SUPPORT_CRC16
            nCRC16 = p->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

            DX_PRINT("Fatching BLOCKID:%d MID:%X DID:%X SEQNO:%X CRC:%04X LEN:%X\r\n",
                     pSrc->BlockID,
                     p->Header.MID,
                     p->Header.DID,
                     p->Header.SeqNo,
                     nCRC16,
                     nPayloadRead);
        }
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        dxContStore_LocalCache_FreeCallbackData(pDataArray, gStoreDataCount);
    }
    else
    {
        *pStoreData = pDataArray;
        *pNumberOfStoreData = gStoreDataCount;
    }

    return ret;
}

/**
 * @brief dxContStore_LocalCache_FindOldest()
 * @retval -1 NOT FOUND
 * @retval >= 0 Index of gStoreData
 */
int dxContStore_LocalCache_FindOldest(void)
{
    uint32_t nMinSeqNo = UINT32_MAX;
    int nMinSeqNoIndex = -1;    // NOT FOUND

    for (int i = 0; i < gStoreDataCount; i++)
    {
        dxContStoreCache_t *p = &gStoreData[i];

        if (p->Header.SeqNo < nMinSeqNo)
        {
            nMinSeqNo = p->Header.SeqNo;
            nMinSeqNoIndex = i;
        }
    }

    return nMinSeqNoIndex;
}

dxContStoreErrorCode_t dxContStore_DataPage_LocalCache_AddData(dxContStoreAC_t *pContStore, uint8_t nBlockID, dxContStoreData_Header_t *pHeader, uint8_t *pPayload)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore &&
        pContStore->hMutex &&
        pHeader)
    {
        if (gStoreDataCount < DX_CONTSTOREDATA_MAX)
        {
            int n = dxContStore_LocalCache_Find(pContStore, pHeader);

            if (n >= 0)
            {
                dxContStoreCache_t *p = &gStoreData[n];

                if (p->Header.SeqNo < pHeader->SeqNo)
                {
                    // Replace the data in gStoreData
                    p->Header.SeqNo = pHeader->SeqNo;

#if CONFIG_SUPPORT_CRC16
                    p->Header.CRC16 = pHeader->CRC16;
#endif // CONFIG_SUPPORT_CRC16

                    p->Header.PayloadLen = pHeader->PayloadLen;     // Fixed wrong PayloadLen issue
                    p->BlockID = nBlockID;                          // Fixed BLOCK #2 be invalid issue

                    if (p->LoadPayloadToRAM == 1)
                    {
                        if (p->Cache.Length > p->Header.PayloadLen)
                        {
                            // There are enough space, just replace
                            memcpy(&gStorePayloadMemory[p->Cache.StartOffset], pPayload, p->Header.PayloadLen);
                            gStorePayloadMemory[p->Cache.StartOffset + p->Header.PayloadLen] = 0x00;               // Including 0x00
                            p->Cache.Length = p->Header.PayloadLen + 1;
                        }
                        else
                        {
                            // Not enough SDRAM space

                            // Free already use gStorePayloadMemory
                            dxContStore_LocalCache_Payload_Free(&p->Cache);
                            p->LoadPayloadToRAM = 0;

                            // Allocate new gStorePayloadMemory
                            ret = dxContStore_LocalCache_Payload_AllocateMemory(&p->Cache, pPayload, p->Header.PayloadLen);

                            if (ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
                            {
                                p->LoadPayloadToRAM = 0;
                            }
                            else if (ret == CONTSTORE_ERROR_SUCCESS)
                            {
                                p->LoadPayloadToRAM = 1;
                            }
                            else
                            {
                                return ret;
                            }
                        }
                    }
                    else
                    {
                        // PAYLOAD data is not in SDRAM
                        DX_PRINT("[%s] Line %d\r\n", __FUNCTION__, __LINE__);
                    }
                }
                else
                {
                    // Skip
                    DX_PRINT("[%s] Line %d\r\n", __FUNCTION__, __LINE__);
                }
            }
            else
            {
                uint16_t nCRC16 = 0;
#if CONFIG_SUPPORT_CRC16
#endif // CONFIG_SUPPORT_CRC16
                DX_PRINT("[%s] Line %d, ret:%d BLOCKID:%d MID:0x%X DID:0x%X SEQ:0x%X CRC:%04X LEN:0x%X gStoreCount:%d\r\n",
                          __FUNCTION__,
                          __LINE__,
                          n,
                          nBlockID,
                          pHeader->MID,
                          pHeader->DID,
                          pHeader->SeqNo,
                          nCRC16,
                          pHeader->PayloadLen,
                          gStoreDataCount);

                // New
                dxContStoreCache_t *p = &gStoreData[gStoreDataCount];

                memcpy(&p->Header, pHeader, sizeof(dxContStoreData_Header_t));

                p->LoadPayloadToRAM = 0;
                p->BlockID = nBlockID;

                if (p->Header.PayloadLen > 0 &&
                    pPayload)
                {
                    ret = dxContStore_LocalCache_Payload_AllocateMemory(&p->Cache, pPayload, p->Header.PayloadLen);

                    if (ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
                    {
                        p->LoadPayloadToRAM = 0;
                    }
                    else if (ret == CONTSTORE_ERROR_SUCCESS)
                    {
                        p->LoadPayloadToRAM = 1;
                    }
                    else
                    {
                        return ret;
                    }
                }
                gStoreDataCount++;
            }
        }
    }

    return ret;
}

/**
 * @brief
 * @param pContStore
 * @param nBlockID
 * @param nMID
 * @param nDID
 * @param nSeqNo
 * @param pPayload[in,out] Preallocated buffer to store result data. If NULL, pLayLoadLen will be the length of pPayload
 * @parma pPayloadLen[in,out] After function return, *pPayloadLen will be the length of pPayload
 */
dxContStoreErrorCode_t dxContStore_DataPage_FindPayload(dxContStoreAC_t *pContStore, uint8_t nBlockID, uint32_t nMID, uint32_t nDID, uint32_t nSeqNo, uint8_t *pPayload, uint32_t *pPayloadLen)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_ERROR;//CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nNumberOfDataBlock == 0 ||
        pContStore->nDataAddr < 0x0B000 ||
        nBlockID > pContStore->nNumberOfDataBlock ||
        pPayloadLen == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    uint32_t nAddr = pContStore->nDataAddr + DEFAULT_FLASH_SECTOR_ERASE_SIZE * (nBlockID - 1);
    uint32_t i = 0;
    dxOS_RET_CODE r = DXOS_SUCCESS;

    while (i < DEFAULT_FLASH_SECTOR_ERASE_SIZE)
    {
        dxContStoreData_Header_t Header;
        memset(&Header, 0x00, sizeof(Header));
        r = dxFlash_Read(pContStore->hFlash,
                         nAddr + i,
                         sizeof(Header),
                         ( uint8_t * )&Header);

        if (r != DXOS_SUCCESS)
        {
            DX_PRINT("[%s] Line %d, dxFlash_Read(0x%X,%d):%d\r\n", __FUNCTION__, __LINE__, nAddr + i, sizeof(Header));
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        i += sizeof(Header);

        if (Header.MID == nMID &&
            Header.DID == nDID &&
            Header.SeqNo == nSeqNo)
        {
#if CONFIG_SUPPORT_CRC16
            DX_PRINT("[%s] Line %d, FOUND BLOCKID:%d MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X\r\n",
                     __FUNCTION__,
                     __LINE__,
                     nBlockID,
                     Header.MID,
                     Header.DID,
                     Header.SeqNo,
                     Header.CRC16,
                     Header.PayloadLen);
#endif // CONFIG_SUPPORT_CRC16

            if (pPayload != NULL)
            {
                if (Header.PayloadLen > *pPayloadLen)
                {
                    DX_PRINT("[%s] Line %d, dxFlash_Read(0x%X,%d):%d PayloadLen:0x%X but buffer size is 0x%X\r\n",
                             __FUNCTION__,
                             __LINE__,
                             nAddr + i - sizeof(Header),
                             sizeof(Header),
                             Header.PayloadLen,
                             *pPayloadLen);

                    ERROR_PRINT("[%s,%d] dxFlash_Read(0x%X,%d):%d PayloadLen:0x%X but buffer size is 0x%X\r\n",
                                __FUNCTION__,
                                __LINE__,
                                nAddr + i - sizeof(Header),
                                sizeof(Header),
                                Header.PayloadLen,
                                *pPayloadLen);
                    ret = CONTSTORE_ERROR_INCORRECT_PAYLOADLEN;
                    break;
                }

                r = dxFlash_Read(pContStore->hFlash,
                                 nAddr + i,
                                 Header.PayloadLen,
                                 pPayload);

                if (r != DXOS_SUCCESS)
                {
                    DX_PRINT("[%s] Line %d, dxFlash_Read(0x%X,%d):%d\r\n", __FUNCTION__, __LINE__, nAddr + i, sizeof(Header));
                    ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
                    ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
                    break;
                }

#if CONFIG_SUPPORT_CRC16
                // Check CRC-16
                uint16_t nPLDCRC = crc16_ccitt_kermit(pPayload, Header.PayloadLen);
                if (Header.CRC16 != nPLDCRC)
                {
                    G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail. CRC-16 in Header is 0x%04X but Payload is 0x%04X\r\n",
                                     __FUNCTION__, __LINE__, Header.CRC16, nPLDCRC);

                    G_SYS_DBG_LOG_NV("[%s] Line %d, MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X PLDCRC:0x%04X PYLD:\r\n",
                                     __FUNCTION__,
                                     __LINE__,
                                     Header.MID,
                                     Header.DID,
                                     Header.SeqNo,
                                     Header.CRC16,
                                     Header.PayloadLen,
                                     nPLDCRC);
                    DK_PRINT_HEX(pPayload, Header.PayloadLen);

                    ret = CONTSTORE_ERROR_CRC16_VERIFY_FAIL;
                    break;
                }
#endif // CONFIG_SUPPORT_CRC16
            }

            if (r == DXOS_SUCCESS)
            {
                *pPayloadLen = Header.PayloadLen;
                ret = CONTSTORE_ERROR_SUCCESS;
            }

            break;
        }
        else
        {
            // Search next
            i += Header.PayloadLen;
        }
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] ret:%d\r\n", __FUNCTION__, __LINE__, ret);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_DataPage_GetDataByBlockID(dxContStoreAC_t *pContStore, uint8_t nBlockID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nNumberOfDataBlock == 0 ||
        pContStore->nDataAddr < 0x0B000 ||
        nBlockID > pContStore->nNumberOfDataBlock)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    uint32_t nAddr = pContStore->nDataAddr + DEFAULT_FLASH_SECTOR_ERASE_SIZE * (nBlockID - 1);
    uint32_t i = 0;
    dxOS_RET_CODE r = DXOS_SUCCESS;
    dxBOOL dxSearchEndOfBlock = dxFALSE;

    while (i < DEFAULT_FLASH_SECTOR_ERASE_SIZE)
    {
        dxContStoreData_Header_t Header;
        memset(&Header, 0x00, sizeof(Header));

        // NOTE: Use UINT32_MAX to check empty flash space will cause critical issue,
        //       due to the size of empty space may less than 16 bytes, sizeof(dxContStoreData_Header_t)
        //       Please make sure the remain flash space are larger than 16 bytes.
        //       The following codes should be check first.
        //
        //       if (i + sizeof(Header) > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        //       {
        //           break;
        //       }
        //
        if (i + sizeof(Header) > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        {
            dxSearchEndOfBlock = dxTRUE;
            break;
        }

        r = dxFlash_Read(pContStore->hFlash,
                         nAddr + i,
                         sizeof(Header),
                         ( uint8_t * )&Header);

        if (r != DXOS_SUCCESS)
        {
            DX_PRINT("[%s] Line %d, dxFlash_Read(0x%X,%d):%d\r\n", __FUNCTION__, __LINE__, nAddr + i, sizeof(Header));
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        // NOTE: Use UINT32_MAX to check empty flash space will cause critical issue,
        //       due to the size of empty space may less than 16 bytes, sizeof(dxContStoreData_Header_t)
        //       Please make sure the remain flash space are larger than 16 bytes.
        //       The following codes should be check first.
        //
        //       if (i + sizeof(Header) > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        //       {
        //           break;
        //       }
        //
        if (Header.MID == UINT32_MAX &&
            Header.DID == UINT32_MAX &&
            Header.SeqNo == UINT32_MAX &&
#if CONFIG_SUPPORT_CRC16
            Header.CRC16 == UINT16_MAX &&
            Header.Reserved[0] == UINT8_MAX &&
            Header.Reserved[1] == UINT8_MAX &&
#endif // CONFIG_SUPPORT_CRC16
            Header.PayloadLen == UINT32_MAX)
        {
            dxSearchEndOfBlock = dxTRUE;
            break;
        }

        // NOTE: We need to make sure HEADER + PAYLOAD are in one 4KB flash block.
        //
        //       if (i + sizeof(Header) + Header.PayloadLen > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        //       {
        //           break;
        //       }
        //
        if (i + sizeof(Header) + Header.PayloadLen > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        {
            dxSearchEndOfBlock = dxTRUE;
            break;
        }

        // Store max value of SeqNo
        if (pContStore->nMaxSeqNo < Header.SeqNo)
        {
            pContStore->nMaxSeqNo = Header.SeqNo;
        }

        i += sizeof(Header);

        uint8_t *pPayload = NULL;
        if (Header.PayloadLen)
        {
            pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, Header.PayloadLen + 1);
            if (pPayload)
            {
                r = dxFlash_Read(pContStore->hFlash,
                                 nAddr + i,
                                 Header.PayloadLen,
                                 pPayload);

                if (r != DXOS_SUCCESS)
                {
                    DX_PRINT("[%s] Line %d, dxFlash_Read(0x%X,%d):%d\r\n", __FUNCTION__, __LINE__, nAddr + i, sizeof(Header));
                    ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
                    ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
                    break;
                }
            }

            i += Header.PayloadLen;
        }

        if (r != DXOS_SUCCESS ||
            dxSearchEndOfBlock == dxTRUE)
        {
            // To avoid adding (MID,DID) = (0xFFFFFFFF,0xFFFFFFFF) to LocalCache
            break;
        }

#if CONFIG_SUPPORT_CRC16
        // Verify CRC16 before writing
        uint16_t nCRC16 = crc16_ccitt_kermit(pPayload, Header.PayloadLen);
        if (Header.CRC16 != nCRC16)
        {
            G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail. CRC-16 in HEADER is 0x%04X but PAYLOAD is 0x%04X\r\n",
                             __FUNCTION__, __LINE__, Header.CRC16, nCRC16);
        }
#endif // CONFIG_SUPPORT_CRC16

        if (pContStore->nMaxSeqNo == Header.SeqNo)
        {
            dxContStore_LocalCache_Dump(pContStore, __LINE__);
        }

        ret = dxContStore_DataPage_LocalCache_AddData(pContStore, nBlockID, &Header, pPayload);

        if (pContStore->nMaxSeqNo == Header.SeqNo)
        {
             dxContStore_LocalCache_Dump(pContStore, __LINE__);
        }

        dxMemFree(pPayload);
        pPayload = NULL;

        if (ret == CONTSTORE_ERROR_SUCCESS)
        {

        }
        else if (ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
        {
        }
        else
        {
            break;
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_DataPage_AppendData(dxContStoreAC_t *pContStore, uint8_t nBlockID, dxContStoreData_t *pDataToWrite, dxBOOL bUpdateLocalCache)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nNumberOfDataBlock == 0 ||
        pContStore->nDataAddr < 0x0B000 ||
        nBlockID > pContStore->nNumberOfDataBlock ||
        pDataToWrite == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

#if CONFIG_SUPPORT_CRC16
    // Verify CRC16 before writing
    uint16_t nPLDCRC = crc16_ccitt_kermit(pDataToWrite->pPayload, pDataToWrite->Header.PayloadLen);
    if (pDataToWrite->Header.CRC16 != nPLDCRC)
    {
        G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail. CRC-16 in HEADER is 0x%04X but PAYLOAD is 0x%04X\r\n",
                         __FUNCTION__, __LINE__, pDataToWrite->Header.CRC16, nPLDCRC);

        G_SYS_DBG_LOG_NV("[%s] Line %d, MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X PLDCRC:0x%04X PYLD:\r\n",
                         __FUNCTION__,
                         __LINE__,
                         pDataToWrite->Header.MID,
                         pDataToWrite->Header.DID,
                         pDataToWrite->Header.SeqNo,
                         pDataToWrite->Header.CRC16,
                         pDataToWrite->Header.PayloadLen,
                         nPLDCRC);
        DK_PRINT_HEX(pDataToWrite->pPayload, pDataToWrite->Header.PayloadLen);

        return CONTSTORE_ERROR_CRC16_VERIFY_FAIL;
    }
#endif // CONFIG_SUPPORT_CRC16

    uint32_t nAddr = pContStore->nDataAddr + DEFAULT_FLASH_SECTOR_ERASE_SIZE * (nBlockID - 1);
    uint32_t i = 0;
    dxOS_RET_CODE r = DXOS_SUCCESS;

    ret = CONTSTORE_ERROR_ERROR;
    while (i < DEFAULT_FLASH_SECTOR_ERASE_SIZE)
    {
        dxContStoreData_Header_t Header;
        memset(&Header, 0x00, sizeof(Header));
        r = dxFlash_Read(pContStore->hFlash,
                         nAddr + i,
                         sizeof(Header),
                         ( uint8_t * )&Header);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        // NOTE: Use UINT32_MAX to check empty flash space will cause critical issue,
        //       due to the size of empty space may less than 16 bytes, sizeof(dxContStoreData_Header_t)
        //       Please make sure the remain flash space are larger than 16 bytes.
        //       The following codes should be check first.
        //
        //       if (i + sizeof(dxContStoreData_Header_t) + pDataToWrite->Header.PayloadLen > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        //       {
        //           break;
        //       }
        //

        // Empty space
        if (i + sizeof(dxContStoreData_Header_t) + pDataToWrite->Header.PayloadLen > DEFAULT_FLASH_SECTOR_ERASE_SIZE)
        {
            ret = CONTSTORE_ERROR_NOT_ENOUGH_FLASH_SPACE;
            break;
        }

        if (Header.MID == UINT32_MAX &&
            Header.DID == UINT32_MAX &&
            Header.SeqNo == UINT32_MAX &&
#if CONFIG_SUPPORT_CRC16
            Header.CRC16 == UINT16_MAX &&
            Header.Reserved[0] == UINT8_MAX &&
            Header.Reserved[1] == UINT8_MAX &&
#endif // CONFIG_SUPPORT_CRC16
            Header.PayloadLen == UINT32_MAX)
        {
            DX_PRINT("[%s] Line %d, Write to flash addr:0x%X\r\n",
                     __FUNCTION__,
                     __LINE__,
                     nAddr + i);

            // Write HEADER & PAYLOAD in one flash write
            uint32_t totalLen = sizeof(pDataToWrite->Header) + pDataToWrite->Header.PayloadLen;
            uint8_t *p = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, totalLen);
            if (p == NULL)
            {
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
                break;
            }

            memcpy(p, &pDataToWrite->Header, sizeof(pDataToWrite->Header));
            memcpy(&p[sizeof(pDataToWrite->Header)], pDataToWrite->pPayload, pDataToWrite->Header.PayloadLen);

            // Write Header to flash
            ret = dxFlash_WriteVerify(pContStore->hFlash,
                                      nAddr + i,
                                      totalLen,
                                      p);

            dxMemFree(p);
            p = NULL;

            i += totalLen;

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                break;
            }

            ret = CONTSTORE_ERROR_SUCCESS;

            //if (bUpdateLocalCache == dxTRUE)
            {
                uint16_t nCRC16 = 0;
#if CONFIG_SUPPORT_CRC16
                nCRC16 = pDataToWrite->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16
                DX_PRINT("[%s] Line %d, Write to Cache. BLOCKID:%d MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X bUpdate:%d\r\n",
                          __FUNCTION__,
                          __LINE__,
                          nBlockID,
                          pDataToWrite->Header.MID,
                          pDataToWrite->Header.DID,
                          pDataToWrite->Header.SeqNo,
                          nCRC16,
                          pDataToWrite->Header.PayloadLen,
                          bUpdateLocalCache);

                ret = dxContStore_DataPage_LocalCache_AddData(pContStore, nBlockID, &pDataToWrite->Header, pDataToWrite->pPayload);
            }

            break;
        }
        else
        {
            i += sizeof(Header) + Header.PayloadLen;
        }
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] ret:%d\r\n", __FUNCTION__, __LINE__, ret);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_DataPage_Erase(dxContStoreAC_t *pContStore, uint8_t nBlockID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nNumberOfDataBlock == 0 ||
        pContStore->nDataAddr < 0x0B000 ||
        nBlockID > pContStore->nNumberOfDataBlock)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    uint32_t nAddr = pContStore->nDataAddr + DEFAULT_FLASH_SECTOR_ERASE_SIZE * (nBlockID - 1);
    dxOS_RET_CODE r = DXOS_SUCCESS;

    r = dxFlash_Erase_Sector(pContStore->hFlash, nAddr);
    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Erase_Sector():%d\r\n", __FUNCTION__, __LINE__, r);
        ret = CONTSTORE_ERROR_FLASH_ERASE_FAIL;
    }
    return ret;
}

dxContStoreErrorCode_t dxContStore_DataPage_Scan_Unuse(dxContStoreAC_t *pContStore)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_ERROR;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    for (uint32_t i = 0; i < pContStore->nNumberOfDataBlock; i++)
    {
        dxContStoreCache_t *p = &gStoreData[i];

        if (p)
        {
            uint8_t nBlockID = i + 1;       // BLOCKID start from 1

            int found = dxContStore_LocalCache_FindBlockID(pContStore, nBlockID);
            if (found < 0)
            {
                ret = dxContStore_MTable_InvalidBlock(pContStore->SectorID, nBlockID);
            }
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_DataPage_ReadToCache(dxContStoreAC_t *pContStore)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    uint32_t nUseBlockCount = CSQ_Count();
    DX_PRINT("[%s] Line %d, nUseBlockCount:%d\r\n", __FUNCTION__, __LINE__, nUseBlockCount);
    for (int i = 0; i < nUseBlockCount; i++)
    {
        dxContStoreMTableItemCache_t *pBlock = CSQ_GetAt(i);
        DX_PRINT("[%s] Line %d, BLOCKID:%d, Flag:%d\r\n", __FUNCTION__, __LINE__, pBlock->Block.BlockID, pBlock->Block.Flag);
        if (pBlock && pBlock->Block.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {

            ret = dxContStore_DataPage_GetDataByBlockID(pContStore, pBlock->Block.BlockID);
            dxContStore_LocalCache_Dump(pContStore, __LINE__);
            if (ret == CONTSTORE_ERROR_SUCCESS ||
                ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
            {
                ret = CONTSTORE_ERROR_SUCCESS;
            }
            else
            {
                break;
            }
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_Internal_ReadAll(dxContStoreMessage_t *pMessage)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pMessage == NULL)
    {
        ERROR_PRINT("[%s,%d] pMessage == NULL\r\n", __FUNCTION__, __LINE__);
        return CONTSTORE_ERROR_INVALID_PARAMETER;
    }

    dxContStoreAC_t  *pContStore = pConstStoreAC[pMessage->Header.SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    // Free local cache
    dxContStore_LocalCache_Dump(pContStore, __LINE__);
    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    dxContStore_LocalCache_Free(pContStore);
    ret = dxContStore_DataPage_ReadToCache(pContStore);
    dxMutexGive(pContStore->hMutex);
    dxContStore_LocalCache_Dump(pContStore, __LINE__);
    // Report result

    if (pContStore && pContStore->pfnCompletionCallback)
    {
        uint32_t nNumberOfData = 0;
        dxContStoreData_t *p = NULL;

        dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
        ret = dxContStore_LocalCache_GenerateCallbackData(pContStore, &p, &nNumberOfData);
        dxMutexGive(pContStore->hMutex);

        pContStore->pfnCompletionCallback(CONTSTORE_ACCESS_READ, pMessage->Header.ClientId, p, nNumberOfData, ret);

        dxContStore_LocalCache_FreeCallbackData(p, nNumberOfData);
        p = NULL;
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_Internal_Find(dxContStoreMessage_t *pMessage)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pMessage == NULL)
    {
        ERROR_PRINT("[%s,%d] pMessage == NULL\r\n", __FUNCTION__, __LINE__);
        return CONTSTORE_ERROR_INVALID_PARAMETER;
    }

    dxContStoreAC_t  *pContStore = pConstStoreAC[pMessage->Header.SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    // Free local cache
    dxContStoreData_Header_t DataHeaderToFind;
    memset(&DataHeaderToFind, 0x00, sizeof(DataHeaderToFind));

    DataHeaderToFind.MID = pMessage->Header.MID;
    DataHeaderToFind.DID = pMessage->Header.DID;

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);

    int n = dxContStore_LocalCache_Find(pContStore, &DataHeaderToFind);

    uint32_t nNumberOfData = 0;
    dxContStoreData_t *p = NULL;

    if (n >= 0)
    {
        p = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, sizeof(dxContStoreData_t));

        if (p == NULL)
        {
            ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
        }
        else
        {
            dxContStoreCache_t *pSrc = &gStoreData[n];
            memcpy(&p->Header, &pSrc->Header, sizeof(pSrc->Header));

            if (pSrc->Header.PayloadLen > 0)
            {
                p->pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, p->Header.PayloadLen + 1);

                if (p->pPayload == NULL)
                {
                    ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
                }
                else
                {
                    if (pSrc->LoadPayloadToRAM == 1)
                    {
                        memcpy(p->pPayload, &gStorePayloadMemory[pSrc->Cache.StartOffset], pSrc->Cache.Length);
                        nNumberOfData = 1;
                    }
                    else
                    {
                        uint32_t nPayloadRead = p->Header.PayloadLen + 1;
                        ret = dxContStore_DataPage_FindPayload(pContStore,
                                                               pSrc->BlockID,
                                                               p->Header.MID,
                                                               p->Header.DID,
                                                               p->Header.SeqNo,
                                                               p->pPayload,
                                                               &nPayloadRead);

#if CONFIG_SUPPORT_CRC16
                        // Verify CRC-16
                        if (ret == CONTSTORE_ERROR_CRC16_VERIFY_FAIL)
                        {
                            G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail while FIND specific MID and DID\r\n",
                                             __FUNCTION__, __LINE__);
                        }
#endif // CONFIG_SUPPORT_CRC16

                        if (ret == CONTSTORE_ERROR_SUCCESS && nPayloadRead > 0)
                        {
                            nNumberOfData = 1;
                        }
                    }
                }
            }
        }
    }

    dxMutexGive(pContStore->hMutex);

    // Report result

    if (pContStore && pContStore->pfnCompletionCallback)
    {
        pContStore->pfnCompletionCallback(CONTSTORE_ACCESS_FIND, pMessage->Header.ClientId, p, nNumberOfData, ret);

        dxContStore_LocalCache_FreeCallbackData(p, nNumberOfData);
        p = NULL;
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_Internal_WriteNewData(dxContStoreAC_t  *pContStore, dxContStoreData_t *pDataToWrite, uint8_t *pWriteBlockID, dxBOOL bUpdateLocalCache)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pDataToWrite == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    // STEP #1: Searching CSQ and try to append to existing BLOCK (4KB)
    ret = CONTSTORE_ERROR_ERROR;
    uint32_t nUseBlockCount = CSQ_Count();
    for (int i = 0; i < nUseBlockCount; i++)
    {
        dxContStoreMTableItemCache_t *pBlock = CSQ_GetAt(i);
        if (pBlock && pBlock->Block.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {
            uint16_t nCRC16 = 0;
            uint16_t nPLDCRC = 0;
#if CONFIG_SUPPORT_CRC16
            nCRC16 = pDataToWrite->Header.CRC16;
            nPLDCRC = crc16_ccitt_kermit(pDataToWrite->pPayload, pDataToWrite->Header.PayloadLen);
#endif // CONFIG_SUPPORT_CRC16

            DX_PRINT("[%s] Line %d, Write to flash. BLOCKID:%d MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X PLDCRC:0x%04X bUpdate:%d\r\n",
                      __FUNCTION__,
                      __LINE__,
                      pBlock->Block.BlockID,
                      pDataToWrite->Header.MID,
                      pDataToWrite->Header.DID,
                      pDataToWrite->Header.SeqNo,
                      nCRC16,
                      pDataToWrite->Header.PayloadLen,
                      nPLDCRC,
                      bUpdateLocalCache);

            dxContStore_LocalCache_Dump(pContStore, __LINE__);
            // STEP #1.1: Append data to current BLOCK #
            ret = dxContStore_DataPage_AppendData(pContStore, pBlock->Block.BlockID, pDataToWrite, bUpdateLocalCache);

#if CONFIG_SUPPORT_CRC16
            // Verify CRC-16
            if (ret == CONTSTORE_ERROR_CRC16_VERIFY_FAIL)
            {
                G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail while append to existing BLOCK #%d\r\n",
                                 __FUNCTION__, __LINE__, pBlock->Block.BlockID);
                G_SYS_DBG_LOG_NV("[%s] Line %d, Renew MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X\r\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 pDataToWrite->Header.MID,
                                 pDataToWrite->Header.DID,
                                 pDataToWrite->Header.SeqNo,
                                 nCRC16,
                                 pDataToWrite->Header.PayloadLen);
                return ret;
            }
#endif // CONFIG_SUPPORT_CRC16

            dxContStore_LocalCache_Dump(pContStore, __LINE__);
            if (ret == CONTSTORE_ERROR_SUCCESS ||
                ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
            {
                ret = CONTSTORE_ERROR_SUCCESS;

                if (pWriteBlockID)
                {
                    *pWriteBlockID = pBlock->Block.BlockID;
                }
                break;
            }
        }
    }

    // STEP #2: If all fail in STEP #1, write data to a new BLOCK #
    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        // STEP #2.1: Searching for a new available BLOCK from CSQ
        int nNewBlockID = 0;

        if (CSQ_Count() == 0)
        {
            uint32_t index = dxRand();

            nNewBlockID = (index % pContStore->nNumberOfDataBlock) + 1;

            DX_PRINT("[%s] Line %d, random nNewBlockID:%d\r\n",
                      __FUNCTION__,
                      __LINE__,
                      nNewBlockID);
        }
        else
        {
            for (int i = pContStore->nNumberOfDataBlock; i > 0; i--)
            {
                dxContStoreMTableItemCache_t *p = CSQ_FindByBlockID(i);
                if (p == NULL)
                {
                    nNewBlockID = i;
                    break;
                }
            }
        }

        if (nNewBlockID > 0)
        {
            // STEP #2.1: Write the new BLOCKID to MTable
            ret = dxContStore_MTable_AppendNewBlock(pContStore->SectorID, nNewBlockID);
            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                return ret;
            }
            // STEP #2.3: Erase the Block before writing data
            uint8_t nBlockIDToErase = (uint8_t)nNewBlockID;
            ret = dxContStore_DataPage_Erase(pContStore, nBlockIDToErase);
            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                return ret;
            }

            // STEP #2.4: Append data to BLOCK #
            dxContStore_LocalCache_Dump(pContStore, __LINE__);
            ret = dxContStore_DataPage_AppendData(pContStore, nNewBlockID, pDataToWrite, bUpdateLocalCache);

#if CONFIG_SUPPORT_CRC16
            // Verify CRC-16
            if (ret == CONTSTORE_ERROR_CRC16_VERIFY_FAIL)
            {
                uint16_t nCRC16 = 0;
#if CONFIG_SUPPORT_CRC16
                nCRC16 = pDataToWrite->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

                G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail while append to new BLOCK #%d\r\n",
                                 __FUNCTION__, __LINE__, nNewBlockID);
                G_SYS_DBG_LOG_NV("[%s] Line %d, Renew MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X\r\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 pDataToWrite->Header.MID,
                                 pDataToWrite->Header.DID,
                                 pDataToWrite->Header.SeqNo,
                                 nCRC16,
                                 pDataToWrite->Header.PayloadLen);
                return ret;
            }
#endif // CONFIG_SUPPORT_CRC16

            dxContStore_LocalCache_Dump(pContStore, __LINE__);
            DX_PRINT("[%s] Line %d, dxContStore_DataPage_AppendData():%d\r\n", __FUNCTION__, __LINE__, ret);
            if (ret == CONTSTORE_ERROR_SUCCESS ||
                ret == CONTSTORE_ERROR_NOT_ENOUGH_SDRAM_SPACE)
            {
                ret = CONTSTORE_ERROR_SUCCESS;

                if (pWriteBlockID)
                {
                    *pWriteBlockID = nNewBlockID;
                }
            }
        }
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] ret:%d\r\n", __FUNCTION__, __LINE__, ret);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_Internal_Write(dxContStoreMessage_t *pMessage)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;
    dxContStoreAC_t  *pContStore = NULL;
    int n = 0;

    if (pMessage == NULL)
    {
        ERROR_PRINT("[%s,%d] pMessage == NULL\r\n", __FUNCTION__, __LINE__);
        ret = CONTSTORE_ERROR_INVALID_PARAMETER;
        goto Exit;
    }

    pContStore = pConstStoreAC[pMessage->Header.SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL)
    {
        ret = CONTSTORE_ERROR_INVALID_HANDLE;
        goto Exit;
    }

    // STEP #0: Check if MTable is FULL
    n = dxContStore_MTable_IsFull(pMessage->Header.SectorID);
    if (n == 1)
    {
        DX_PRINT("[%s] Line %d, There is NO FREE BLOCK available. Reclaim space\r\n",
                  __FUNCTION__,
                  __LINE__);

        dxContStore_MTable_ReclaimSpace(pMessage->Header.SectorID);

        dxContStore_MTable_LoadIntoCache(pMessage->Header.SectorID);
    }

    // STEP #1: Check if all BLOCK are use
    n = dxContStore_MTable_GetNumberOfUseBlock(pMessage->Header.SectorID);
    if (n == pContStore->nNumberOfDataBlock)
    {
        DX_PRINT("[%s] Line %d, There is NO FREE BLOCK available. Scan and free spaces\r\n",
                  __FUNCTION__,
                  __LINE__);

        dxContStore_DataPage_Scan_Unuse(pContStore);

        dxContStore_MTable_LoadIntoCache(pMessage->Header.SectorID);
    }

    // STEP #2: Renew oldest data
    n = dxContStore_LocalCache_FindOldest();
    if (n >= 0)
    {
        DX_PRINT("[%s] Line %d, Oldest index in LocalCache is %d\r\n",
                  __FUNCTION__,
                  __LINE__,
                  n);

        dxContStoreCache_t *p = &gStoreData[n];
        if (p)
        {
            uint8_t nNewBlockID = UINT8_MAX;
            uint16_t nCRC16 = 0;
            uint32_t nOrgBlockID = p->BlockID;

#if CONFIG_SUPPORT_CRC16
            nCRC16 = p->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

            DX_PRINT("[%s] Line %d, Renew MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X\r\n",
                      __FUNCTION__,
                      __LINE__,
                      p->Header.MID,
                      p->Header.DID,
                      p->Header.SeqNo,
                      nCRC16,
                      p->Header.PayloadLen);

            // STEP #2.1 Read PAYLOADLEN from flash
            uint32_t PayloadLen = 0;
            dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
            ret = dxContStore_DataPage_FindPayload(pContStore,
                                                   nOrgBlockID,
                                                   p->Header.MID,
                                                   p->Header.DID,
                                                   p->Header.SeqNo,
                                                   NULL,
                                                   &PayloadLen);
            dxMutexGive(pContStore->hMutex);

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                DX_PRINT("[%s] Line %d, dxContStore_DataPage_FindPayload():%d\r\n", __FUNCTION__, __LINE__, ret);

                ERROR_PRINT("[%s,%d] dxContStore_DataPage_FindPayload():%d\r\n", __FUNCTION__, __LINE__, ret);
                ret = CONTSTORE_ERROR_DATAPAGE_FINDPAYLOAD_FAIL;
                goto Exit;
            }

            if (PayloadLen != p->Header.PayloadLen)
            {
                DX_PRINT("[%s] Line %d, ERROR!!! PayloadLen 0x%X in LocalCache but 0x%X in flash memory\r\n",
                         __FUNCTION__,
                         __LINE__,
                         PayloadLen,
                         p->Header.PayloadLen);
            }

            // STEP #2.2: Read PAYLOAD from flash if needed
            PayloadLen++;   // Added NULL terminate character space
            uint8_t *pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, PayloadLen);
            if (pPayload == NULL)
            {
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
                goto Exit;
            }

            dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
            ret = dxContStore_DataPage_FindPayload(pContStore,
                                                   nOrgBlockID,
                                                   p->Header.MID,
                                                   p->Header.DID,
                                                   p->Header.SeqNo,
                                                   pPayload,
                                                   &PayloadLen);
            dxMutexGive(pContStore->hMutex);

#if CONFIG_SUPPORT_CRC16
            // Verify CRC-16
            if (ret == CONTSTORE_ERROR_CRC16_VERIFY_FAIL)
            {
                uint16_t nPLDCRC = 0;
                nPLDCRC = crc16_ccitt_kermit(pPayload, p->Header.PayloadLen);

                G_SYS_DBG_LOG_NV("[%s] Line %d, CRC-16 verify fail while RENEW MID, DID, LEN, CRC-16 and PAYLOAD\r\n",
                                 __FUNCTION__, __LINE__);
                G_SYS_DBG_LOG_NV("[%s] Line %d, Renew MID:0x%X DID:0x%X SEQ:0x%X CRC:0x%04X LEN:0x%X PLDCRC:0x%04X\r\n",
                                 __FUNCTION__,
                                 __LINE__,
                                 p->Header.MID,
                                 p->Header.DID,
                                 p->Header.SeqNo,
                                 nCRC16,
                                 p->Header.PayloadLen,
                                 nPLDCRC);

                // Free memory
                if (pPayload)
                {
                    dxMemFree(pPayload);
                    pPayload = NULL;
                }
                goto Exit;
            }
#endif // CONFIG_SUPPORT_CRC16

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                DX_PRINT("[%s] Line %d, dxContStore_DataPage_FindPayload():%d\r\n", __FUNCTION__, __LINE__, ret);

                ERROR_PRINT("[%s,%d] dxContStore_DataPage_FindPayload():%d\r\n", __FUNCTION__, __LINE__, ret);
                dxMemFree(pPayload);
                pPayload = NULL;
                ret = CONTSTORE_ERROR_DATAPAGE_FINDPAYLOAD_FAIL;
                goto Exit;
            }

            // STEP #2.3: Write HEADER and PAYLOAD to flash
            dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
            pContStore->nMaxSeqNo++;
            p->Header.SeqNo = pContStore->nMaxSeqNo;

            dxContStoreData_t DataToRenew;
            memset(&DataToRenew, 0x00, sizeof(DataToRenew));

            DataToRenew.Header.MID = p->Header.MID;
            DataToRenew.Header.DID = p->Header.DID;
            DataToRenew.Header.SeqNo = p->Header.SeqNo;

#if CONFIG_SUPPORT_CRC16
            DataToRenew.Header.CRC16 = p->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

            DataToRenew.Header.PayloadLen = p->Header.PayloadLen;
            DataToRenew.pPayload = pPayload;

            ret = dxContStore_Internal_WriteNewData(pContStore, &DataToRenew, &nNewBlockID, dxFALSE);
            dxMutexGive(pContStore->hMutex);

            DataToRenew.pPayload = NULL;
            dxMemFree(pPayload);
            pPayload = NULL;

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                //ret = CONTSTORE_ERROR_ERROR;
                goto Exit;
            }

            dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);

            // STEP #2.4: Update new BlockID to LocalCache
            if (nNewBlockID < pContStore->nNumberOfDataBlock &&
                nNewBlockID != nOrgBlockID)
            {
                p->BlockID = nNewBlockID;
            }
            dxMutexGive(pContStore->hMutex);
        }
    }

    dxContStoreData_t DataToWrite;
    memset(&DataToWrite, 0x00, sizeof(DataToWrite));

    DataToWrite.Header.MID = pMessage->Header.MID;
    DataToWrite.Header.DID = pMessage->Header.DID;

    DataToWrite.Header.PayloadLen = pMessage->Header.PayloadLen;
    if (pMessage->Header.PayloadLen > 0)
    {
        DataToWrite.pPayload = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, pMessage->Header.PayloadLen + 1);
        if (DataToWrite.pPayload == NULL)
        {
            ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
            goto Exit;
        }

        memcpy(DataToWrite.pPayload, pMessage->pPayload, pMessage->Header.PayloadLen);
    }

    // STEP #3: Write new data
    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    pContStore->nMaxSeqNo++;
    DataToWrite.Header.SeqNo = pContStore->nMaxSeqNo;

#if CONFIG_SUPPORT_CRC16
    DataToWrite.Header.CRC16 = pMessage->Header.CRC16;
#endif // CONFIG_SUPPORT_CRC16

    ret = dxContStore_Internal_WriteNewData(pContStore, &DataToWrite, NULL, dxTRUE);
    dxMutexGive(pContStore->hMutex);

    // STEP #4: Free memory
    if (DataToWrite.pPayload)
    {
        dxMemFree(DataToWrite.pPayload);
        DataToWrite.pPayload = NULL;
    }

Exit:
    // Report result
    if (pContStore && pContStore->pfnCompletionCallback)
    {
        pContStore->pfnCompletionCallback(CONTSTORE_ACCESS_WRITE, pMessage->Header.ClientId, NULL, 0, ret);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_Internal_WriteWithRetry(dxContStoreMessage_t *pMessage, uint32_t nMaxRetryCount)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_ERROR;
    uint32_t i = 0;
    dxContStoreAC_t  *pContStore = NULL;

    if (pMessage == NULL)
    {
        return CONTSTORE_ERROR_INVALID_PARAMETER;
    }

    pContStore = pConstStoreAC[pMessage->Header.SectorID];

    do
    {
        ret = dxContStore_Internal_Write(pMessage);

        if (ret != CONTSTORE_ERROR_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxContStore_Internal_Write():%d, We will try a again(%d)\r\n",
                        __FUNCTION__,
                        __LINE__,
                        ret,
                        i + 1);

            // Free local cache and then re-generate
            dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
            dxContStore_LocalCache_Free(pContStore);
            ret = dxContStore_DataPage_ReadToCache(pContStore);
            dxMutexGive(pContStore->hMutex);
        }

        i++;
    } while (i < nMaxRetryCount && ret != CONTSTORE_ERROR_SUCCESS);

    return ret;
}

/**
 * @brief dxContStore_WorkerThreadEventHandler
 */
static void dxContStore_WorkerThreadEventHandler(void *arg)
{
    dxContStoreMessageHeader_t *pHeader = dxContStore_GetHeader((dxContStoreMessage_t *)arg);

    if (pHeader)
    {
        switch(pHeader->CmdID)
        {
        case CONTSTORE_COMMAND_READALL:     // 1
            dxContStore_Internal_ReadAll((dxContStoreMessage_t *)arg);
            break;
        case CONTSTORE_COMMAND_FIND:        // 2
            dxContStore_Internal_Find((dxContStoreMessage_t *)arg);
            break;
        case CONTSTORE_COMMAND_WRITE:       // 3
            dxContStore_Internal_WriteWithRetry((dxContStoreMessage_t *)arg, CONFIG_DATAPAGE_WRITE_RETRY_COUNT);
            break;
        }
    }

    dxContStore_CleanEventArgumentObj((dxContStoreMessage_t *)arg);
}

dxContStoreErrorCode_t dxContStore_MTable_IsValidHeader(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nStartAddr <= 0x0B000 ||
        pContStore->nTotalSize < pContStore->nBackupAddr - pContStore->nStartAddr)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    dxContStoreMTableHeader_t TableHeader;

    dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                   pContStore->nStartAddr,
                                   sizeof(TableHeader),
                                   ( uint8_t * )&TableHeader);

    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_READ_FAIL;
    }

    uint8_t Reserved[5] = { 0x00, 0x00, 0x00, 0x00, 0x00 };
    if (TableHeader.Tag == DX_CONTSTORE_MARK &&
        TableHeader.Version == DX_CONTSTORE_VER &&
        memcmp(&TableHeader.Reserved[0], Reserved, sizeof(TableHeader.Reserved)) == 0 &&
        TableHeader.Size == sizeof(dxContStoreMTableHeader_t))
    {
        pContStore->HeaderSize = TableHeader.Size;
        ret = CONTSTORE_ERROR_SUCCESS;
    }
    else
    {
        ret = CONTSTORE_ERROR_UNKNOWN_FORMAT;
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_IsValidSecondBlock(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nStartAddr <= 0x0B000 ||
        pContStore->nTotalSize < pContStore->nBackupAddr - pContStore->nStartAddr)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    dxContStoreMTableHeader_t TableHeader;

    dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                   pContStore->nBackupAddr,
                                   sizeof(TableHeader),
                                   ( uint8_t * )&TableHeader);

    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_READ_FAIL;
    }

    uint8_t Reserved[5] = { 0x00, 0x00, 0x00, 0x00, 0x00 };

    if (TableHeader.Tag == DX_CONTSTORE_MARK &&
        TableHeader.Version == DX_CONTSTORE_VER &&
        memcmp(&TableHeader.Reserved[0], Reserved, sizeof(TableHeader.Reserved)) == 0 &&
        TableHeader.Size == sizeof(dxContStoreMTableHeader_t))
    {
        uint32_t n = sizeof(dxContStoreMTableHeader_t);
        while (1)
        {
            dxContStoreMTableItem_t Item;
            dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                           pContStore->nBackupAddr + n,
                                           sizeof(Item),
                                           ( uint8_t * )&Item);
            if (r != DXOS_SUCCESS)
            {
                ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
                ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
                break;
            }

            if (Item.BlockID == UINT8_MAX &&
                Item.Flag == UINT8_MAX)
            {

                ret = CONTSTORE_ERROR_UNKNOWN_FORMAT;
                break;
            }

            if (Item.BlockID == DX_CONTSTORE_ENDOFBLOCK1 &&
                Item.Flag == DX_CONTSTORE_ENDOFBLOCK2)
            {
                ret = CONTSTORE_ERROR_SUCCESS;
                break;
            }

            if (Item.Flag != DX_CONTSTORE_BLOCK_FLAG_VALID &&
                Item.Flag != DX_CONTSTORE_BLOCK_FLAG_INVALID &&
                Item.Flag != UINT8_MAX)
            {
                ret = CONTSTORE_ERROR_UNKNOWN_FORMAT;
                break;
            }

            n += sizeof(dxContStoreMTableItem_t);
        }
    }
    else
    {
        ret = CONTSTORE_ERROR_UNKNOWN_FORMAT;
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_SearchAvailableSpace(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    uint32_t nAddr = pContStore->nStartAddr + pContStore->HeaderSize;
    uint32_t n = 0;
    while (1)
    {
        dxContStoreMTableItem_t Item;
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       nAddr + n,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if (Item.BlockID == UINT8_MAX &&
            Item.Flag == UINT8_MAX)
        {
            pContStore->AvailableTableOffset = n;
            break;
        }
        else
        {
            n += sizeof(dxContStoreMTableItem_t);
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_AppendNewBlock(dxContStoreSectorID_t SectorID, uint8_t BlockID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    uint32_t Addr = pContStore->HeaderSize + pContStore->AvailableTableOffset;
    if (pContStore->nStartAddr + Addr + sizeof(dxContStoreMTableItem_t) > pContStore->nBackupAddr)
    {
        // Reclaim invalid Block

        return CONTSTORE_ERROR_NOT_ENOUGH_FLASH_SPACE;
    }
    else
    {
        dxContStoreMTableItemCache_t ItemC;
        memset(&ItemC, 0x00, sizeof(dxContStoreMTableItemCache_t));
        ItemC.Block.BlockID = BlockID;
        ItemC.Block.Flag = DX_CONTSTORE_BLOCK_FLAG_VALID;
        ItemC.Offset = Addr;

        // Write Header to flash
        ret = dxFlash_WriteVerify(pContStore->hFlash,
                                  pContStore->nStartAddr + Addr,
                                  sizeof(ItemC.Block),
                                  ( uint8_t * )&ItemC.Block);

        if (ret == CONTSTORE_ERROR_SUCCESS)
        {
            pContStore->AvailableTableOffset += sizeof(ItemC.Block);
#if CONFIG_USE_CSQ
            // Push to Cache
            uint8_t n = CSQ_Push(&ItemC);
            if (n == 0)
            {
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
            }
#endif
        }
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_InvalidBlock(dxContStoreSectorID_t SectorID, uint8_t BlockID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];
    uint32_t n = pContStore->HeaderSize;
    while (1)
    {
        if (pContStore->nStartAddr + n >= pContStore->nBackupAddr)
        {
            // Reach last byte of MTable
            break;
        }

        dxContStoreMTableItem_t Item;
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       pContStore->nStartAddr + n,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if (Item.BlockID == UINT8_MAX &&
            Item.Flag == UINT8_MAX)
        {
            // Reach empty area
            break;
        }

        if (Item.BlockID == BlockID &&
            Item.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {
            Item.Flag = DX_CONTSTORE_BLOCK_FLAG_INVALID;

            // Write Header to flash
            ret = dxFlash_WriteVerify(pContStore->hFlash,
                                      pContStore->nStartAddr + n,
                                      sizeof(Item),
                                      ( uint8_t * )&Item);

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                break;
            }

#if CONFIG_USE_CSQ
            // Push to Cache
            uint8_t n = CSQ_RemoveBlockID(BlockID);
            if (n == 0)
            {
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
            }
#endif
        }

        n += sizeof(dxContStoreMTableItem_t);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_CopyFrom2ndBackup(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;
    dxContStoreMTableItem_t Item;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];
    dxOS_RET_CODE r = DXOS_SUCCESS;

    uint32_t i = sizeof(dxContStoreMTableHeader_t);

    // Erase
    r = dxFlash_Erase_Sector(pContStore->hFlash, pContStore->nStartAddr);
    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Erase_Sector():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_ERASE_FAIL;
    }

    while (pContStore->nStartAddr + i < pContStore->nBackupAddr)
    {
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       pContStore->nBackupAddr + i,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if (Item.BlockID == DX_CONTSTORE_ENDOFBLOCK1 &&
            Item.Flag == DX_CONTSTORE_ENDOFBLOCK2)
        {
            ret = CONTSTORE_ERROR_SUCCESS;
            break;
        }

        // Write Header to flash
        ret = dxFlash_WriteVerify(pContStore->hFlash,
                                  pContStore->nStartAddr + i,
                                  sizeof(Item),
                                  ( uint8_t * )&Item);

        if (ret != CONTSTORE_ERROR_SUCCESS)
        {
            break;
        }

        i += sizeof(dxContStoreMTableItem_t);
    }

    if (ret == CONTSTORE_ERROR_SUCCESS)
    {
        // Copy MTable header
        dxContStoreMTableHeader_t Header;

        r = dxFlash_Read(pContStore->hFlash,
                         pContStore->nBackupAddr,
                         sizeof(Header),
                         ( uint8_t * )&Header);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            return CONTSTORE_ERROR_FLASH_READ_FAIL;
        }

        // Write Header to flash
        ret = dxFlash_WriteVerify(pContStore->hFlash,
                                  pContStore->nStartAddr,
                                  sizeof(Header),
                                  ( uint8_t * )&Header);
    }
    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_ReclaimSpace(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;
    uint32_t nTotalReclaim = 0;
    dxContStoreMTableItem_t Item;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];
    uint32_t i = sizeof(dxContStoreMTableHeader_t);
    uint32_t j = sizeof(dxContStoreMTableHeader_t);
    dxOS_RET_CODE r = DXOS_SUCCESS;

    // Erase
    r = dxFlash_Erase_Sector(pContStore->hFlash, pContStore->nBackupAddr);
    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_ERASE_FAIL;
    }

    // Copy MTable header
    dxContStoreMTableHeader_t Header;

    r = dxFlash_Read(pContStore->hFlash,
                     pContStore->nStartAddr,
                     sizeof(Header),
                     ( uint8_t * )&Header);
    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_FLASH_READ_FAIL;
    }

    // Write Header to flash
    ret = dxFlash_WriteVerify(pContStore->hFlash,
                              pContStore->nBackupAddr,
                              sizeof(Header),
                              ( uint8_t * )&Header);

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        return ret;
    }

    uint32_t nMTableSize = pContStore->nBackupAddr - pContStore->nStartAddr;

    while (1)
    {
        if (i >= nMTableSize)
        {
            // Reach last byte of MTable
            if (nTotalReclaim > 0)
            {
                Item.BlockID = DX_CONTSTORE_ENDOFBLOCK1;
                Item.Flag = DX_CONTSTORE_ENDOFBLOCK2;

                // Write Header to flash
                ret = dxFlash_WriteVerify(pContStore->hFlash,
                                          pContStore->nBackupAddr + j,
                                          sizeof(Item),
                                          ( uint8_t * )&Item);

                if (ret != CONTSTORE_ERROR_SUCCESS)
                {
                    break;
                }

                ret = dxContStore_MTable_CopyFrom2ndBackup(SectorID);
            }

            break;
        }

        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       pContStore->nStartAddr + i,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if ((Item.BlockID == UINT8_MAX && Item.Flag == UINT8_MAX) ||
            (i + sizeof(Item) > DEFAULT_FLASH_SECTOR_ERASE_SIZE))
        {
            // Reach empty area
            Item.BlockID = DX_CONTSTORE_ENDOFBLOCK1;
            Item.Flag = DX_CONTSTORE_ENDOFBLOCK2;

            // Write Header to flash
            ret = dxFlash_WriteVerify(pContStore->hFlash,
                                      pContStore->nBackupAddr + j,
                                      sizeof(Item),
                                      ( uint8_t * )&Item);

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                break;
            }

            ret = dxContStore_MTable_CopyFrom2ndBackup(SectorID);

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                return ret;
            }

            break;
        }

        if (Item.Flag == DX_CONTSTORE_BLOCK_FLAG_INVALID)
        {
            nTotalReclaim++;
        }
        else if (Item.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {
            // Write Header to flash
            ret = dxFlash_WriteVerify(pContStore->hFlash,
                                      pContStore->nBackupAddr + j,
                                      sizeof(Item),
                                      ( uint8_t * )&Item);

            if (ret != CONTSTORE_ERROR_SUCCESS)
            {
                break;
            }

            j += sizeof(dxContStoreMTableItem_t);
        }

        i += sizeof(dxContStoreMTableItem_t);
    }

    return ret;
}

dxContStoreErrorCode_t dxContStore_MTable_LoadIntoCache(dxContStoreSectorID_t SectorID)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];
    uint32_t nAddr = pContStore->nStartAddr;
    uint32_t i = sizeof(dxContStoreMTableHeader_t);

    uint32_t nMTableSize = pContStore->nBackupAddr - pContStore->nStartAddr;

#if CONFIG_USE_CSQ
    CSQ_Empty();
#endif // CONFIG_USE_CSQ

    while (i < nMTableSize)
    {
        dxContStoreMTableItem_t Item;
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       nAddr + i,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if (Item.BlockID == UINT8_MAX &&
            Item.Flag == UINT8_MAX)
        {
            // Reach empty area
            pContStore->AvailableTableOffset = (i - sizeof(dxContStoreMTableHeader_t));
            break;
        }

        if (Item.Flag == DX_CONTSTORE_BLOCK_FLAG_INVALID)
        {
        }
        else if (Item.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {
            // Store the data in CSQ
            dxContStoreMTableItemCache_t ItemC;
            memset(&ItemC, 0x00, sizeof(ItemC));
            ItemC.Block.BlockID = Item.BlockID;
            ItemC.Block.Flag = Item.Flag;
            ItemC.Offset = (uint16_t)(i & 0x0FFFF);

#if CONFIG_USE_CSQ
            uint8_t n = CSQ_Push(&ItemC);
            if (n == 0)
            {
                i += sizeof(dxContStoreMTableItem_t);
                ret = CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
                break;
            }
#endif // CONFIG_USE_CSQ
        }

        i += sizeof(dxContStoreMTableItem_t);
    }

    return ret;
}

int dxContStore_MTable_GetNumberOfUseBlock(dxContStoreSectorID_t SectorID)
{
    /**
     *  Maximum number of blocks we supported are 64.
     *   6                 3 3
     *   3                 2 1                0
     *  +-------------------+------------------+
     *  | nBlockUseFlagHigh | nBlockUseFlagLow |
     *  +-------------------+------------------+
     */

    uint32_t nBlockUseFlagHigh = 0;
    uint32_t nBlockUseFlagLow = 0;

    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return 0;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    uint32_t nAddr = pContStore->nStartAddr + pContStore->HeaderSize;
    uint32_t n = 0;
    while (1)
    {
        dxContStoreMTableItem_t Item;
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       nAddr + n,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            ERROR_PRINT("[%s,%d] dxFlash_Read():%d\r\n", __FUNCTION__, __LINE__, r);
            ret = CONTSTORE_ERROR_FLASH_READ_FAIL;
            break;
        }

        if (Item.BlockID == UINT8_MAX &&
            Item.Flag == UINT8_MAX)
        {
            break;
        }

        if (Item.Flag == DX_CONTSTORE_BLOCK_FLAG_VALID)
        {
            DX_PRINT("[%s] Line %d, BlockID:0x%X, Flag:0x%X\r\n",
                      __FUNCTION__,
                      __LINE__,
                      Item.BlockID,
                      Item.Flag);

            if (Item.BlockID > sizeof(nBlockUseFlagLow) * 8)
            {
                nBlockUseFlagHigh |= (1 << (Item.BlockID - sizeof(nBlockUseFlagLow) * 8 - 1));
            }
            else
            {
                nBlockUseFlagLow |= (1 << (Item.BlockID - 1));
            }
        }

        n += sizeof(dxContStoreMTableItem_t);
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        return 0;
    }

    int nNumberOfBlockUse = 0;

    for (int i = 0; i < sizeof(nBlockUseFlagLow) * 8; i++)
    {
        nNumberOfBlockUse += (nBlockUseFlagHigh >> i) & 0x01;
        nNumberOfBlockUse += (nBlockUseFlagLow >> i) & 0x01;
    }

    DX_PRINT("[%s] Line %d, nBlockUseFlag:%08X%08X, nNumberOfBlockUse:%d\r\n",
             __FUNCTION__,
             __LINE__,
             nBlockUseFlagHigh,
             nBlockUseFlagLow,
             nNumberOfBlockUse);

    return nNumberOfBlockUse;
}

/**
 * @retval 1 FULL
 * @retval 0 Space available
 * @retval -1 Fail
 */
int dxContStore_MTable_IsFull(dxContStoreSectorID_t SectorID)
{
    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        ERROR_PRINT("[%s,%d] DX_FLASH_SIZE_TOTAL_MBITS < 32\r\n", __FUNCTION__, __LINE__);
        return CONTSTORE_ERROR_FLASH_SIZE_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    int bIsFull = 1;
    uint32_t nAddr = pContStore->nStartAddr;
    uint32_t n = pContStore->HeaderSize;
    while (n < DEFAULT_FLASH_SECTOR_ERASE_SIZE)
    {
        dxContStoreMTableItem_t Item;
        dxOS_RET_CODE r = dxFlash_Read(pContStore->hFlash,
                                       nAddr + n,
                                       sizeof(Item),
                                       ( uint8_t * )&Item);
        if (r != DXOS_SUCCESS)
        {
            bIsFull = -1;
            break;
        }

        if (Item.BlockID == UINT8_MAX &&
            Item.Flag == UINT8_MAX)
        {
            bIsFull = 0;
            break;
        }

        n += sizeof(Item);
    }

    return bIsFull;
}

dxContStoreErrorCode_t dxContStore_Init(dxContStoreSectorID_t SectorID, uint32_t StartAddr, uint32_t TotalSize, DXCONTSTORE_ACCESSCOMPLETION_CALLBACK pCompletion, dxCSHandle_t *hCS)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    if (pConstStoreAC[SectorID])
    {
        if (pConstStoreAC[SectorID]->pfnCompletionCallback == NULL)
        {
            pConstStoreAC[SectorID]->pfnCompletionCallback = pCompletion;
        }
        return CONTSTORE_ERROR_SUCCESS;
    }

    pConstStoreAC[SectorID] = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, sizeof(dxContStoreAC_t));
    if (pConstStoreAC[SectorID] == NULL)
    {
        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    pConstStoreAC[SectorID]->hMutex = dxMutexCreate();
    if (pConstStoreAC[SectorID]->hMutex == NULL)
    {
        if (pConstStoreAC[SectorID])
        {
            dxMemFree(pConstStoreAC[SectorID]);
        }

        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    pConstStoreAC[SectorID]->hFlash = dxFlash_Init();
    if (pConstStoreAC[SectorID]->hFlash == NULL)
    {
        if (pConstStoreAC[SectorID]->hMutex)
        {
            dxMutexDelete(pConstStoreAC[SectorID]->hMutex);
            pConstStoreAC[SectorID]->hMutex = NULL;
        }

        if (pConstStoreAC[SectorID])
        {
            dxMemFree(pConstStoreAC[SectorID]);
        }

        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    dxWorkweTask_t *worker = dxMemAlloc(DX_CONTSTORE_ALLOC_NAME, 1, sizeof(dxWorkweTask_t));
    if (worker == NULL)
    {

        if (pConstStoreAC[SectorID]->hFlash)
        {
            dxFlash_Uninit(pConstStoreAC[SectorID]->hFlash);
            pConstStoreAC[SectorID]->hFlash = NULL;
        }

        if (pConstStoreAC[SectorID]->hMutex)
        {
            dxMutexDelete(pConstStoreAC[SectorID]->hMutex);
            pConstStoreAC[SectorID]->hMutex = NULL;
        }

        if (pConstStoreAC[SectorID])
        {
            dxMemFree(pConstStoreAC[SectorID]);
        }

        return CONTSTORE_ERROR_NOT_ENOUGH_HEAP;
    }

    dxOS_RET_CODE r = dxWorkerTaskCreate("CNTSTR W",
                                         worker,
                                         DX_CONTSTORE_WORKER_THREAD_PRIORITY,
                                         DX_CONTSTORE_WORKER_THREAD_STACK_SIZE,
                                         DX_CONTSTORE_WORKER_THREAD_MAX_EVENT_QUEUE);

    if (r != DXOS_SUCCESS)
    {
        if (worker)
        {
            dxMemFree(worker);
            worker = NULL;
        }

        if (pConstStoreAC[SectorID]->hFlash)
        {
            dxFlash_Uninit(pConstStoreAC[SectorID]->hFlash);
            pConstStoreAC[SectorID]->hFlash = NULL;
        }

        if (pConstStoreAC[SectorID]->hMutex)
        {
            dxMutexDelete(pConstStoreAC[SectorID]->hMutex);
            pConstStoreAC[SectorID]->hMutex = NULL;
        }

        if (pConstStoreAC[SectorID])
        {
            dxMemFree(pConstStoreAC[SectorID]);
            pConstStoreAC[SectorID] = NULL;
        }

        ERROR_PRINT("[%s,%d] dxWorkerTaskCreate():%d\r\n", __FUNCTION__, __LINE__, r);
        return CONTSTORE_ERROR_CREATE_WORKER_FAIL;
    }

    pConstStoreAC[SectorID]->SectorID = SectorID;
    pConstStoreAC[SectorID]->SectorID = SectorID;
    pConstStoreAC[SectorID]->Worker.Handle = worker;
    GSysDebugger_RegisterForThreadDiagnosticGivenName(&pConstStoreAC[SectorID]->Worker.Handle->WTask, DX_CONTSTORE_WORKER_THREAD_NAME);
    pConstStoreAC[SectorID]->Worker.RegisterFunc = dxContStore_WorkerThreadEventHandler;

    pConstStoreAC[SectorID]->pfnCompletionCallback = pCompletion;

    pConstStoreAC[SectorID]->nStartAddr = StartAddr;
    pConstStoreAC[SectorID]->nBackupAddr = StartAddr + DEFAULT_FLASH_SECTOR_ERASE_SIZE;
    pConstStoreAC[SectorID]->nTotalSize = TotalSize;
    pConstStoreAC[SectorID]->nDataAddr = StartAddr + 2 * DEFAULT_FLASH_SECTOR_ERASE_SIZE;
    pConstStoreAC[SectorID]->nNumberOfDataBlock = (StartAddr + TotalSize - pConstStoreAC[SectorID]->nDataAddr) / DEFAULT_FLASH_SECTOR_ERASE_SIZE;

    dxMutexTake(pConstStoreAC[SectorID]->hMutex, DXOS_WAIT_FOREVER);

    /**
         +---------------------+------------------------------+
         | DX_HP_STORAGE_ADDR1 | DX_HP_STORAGE_ADDR1 + 0x1000 |
         +---------------------+------------------------------+
         | VALID               | VALID                        | => USE DX_HP_STORAGE_ADDR1
         +---------------------+------------------------------+
         | X                   | VALID                        | => USE DX_HP_STORAGE_ADDR1 + 0x1000
         +---------------------+------------------------------+
         | VALID               | X                            | => USE DX_HP_STORAGE_ADDR1
         +---------------------+------------------------------+
     */
    ret = dxContStore_MTable_IsValidHeader(SectorID);
    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        ret = dxContStore_MTable_IsValidSecondBlock(SectorID);

        if (ret == CONTSTORE_ERROR_SUCCESS)
        {
            ret = dxContStore_MTable_CopyFrom2ndBackup(SectorID);
        }
    }

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        goto Exit;
    }

    // Clear
    gStoreDataCount = 0;
    memset(gStoreData, 0x00, sizeof(gStoreData));

    gStorePayloadMemoryOccupied = 0;
    memset(gStorePayloadMemory, 0x00, sizeof(gStorePayloadMemory));

    ret = dxContStore_MTable_LoadIntoCache(SectorID);
    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        goto Exit;
    }

    // Read data into LocalCache
    ret = dxContStore_DataPage_ReadToCache(pConstStoreAC[SectorID]);

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        goto Exit;
    }

    // Invalid unuse BLOCKID in MTable
    //dxContStore_DataPage_Scan_Unuse(pConstStoreAC[SectorID]);

    *hCS = (dxCSHandle_t *)pConstStoreAC[SectorID];

Exit:
    dxMutexGive(pConstStoreAC[SectorID]->hMutex);

    return ret;
}

dxContStoreErrorCode_t dxContStore_FormatHeader(dxContStoreSectorID_t SectorID, dxCSHandle_t *hCS)
{
    dxContStoreErrorCode_t ret = CONTSTORE_ERROR_SUCCESS;

    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (SectorID >= CONTSTORE_SECTOR_COUNT)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = pConstStoreAC[SectorID];

    if (pContStore == NULL ||
        pContStore->hMutex == NULL ||
        pContStore->hFlash == NULL ||
        pContStore->nStartAddr <= 0x0B000 ||
        pContStore->nTotalSize < pContStore->nBackupAddr - pContStore->nStartAddr)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);

    // Erase flash
    dxOS_RET_CODE r = dxFlash_Erase_Sector(pContStore->hFlash, pContStore->nStartAddr);
    if (r != DXOS_SUCCESS)
    {
        ERROR_PRINT("[%s,%d] dxFlash_Erase_SectorWithSize():%d\r\n", __FUNCTION__, __LINE__, r);
        ret = CONTSTORE_ERROR_FLASH_ERASE_FAIL;
        goto Exit;
    }

    // Write ContStore header
    dxContStoreMTableHeader_t TableHeader;
    memset(&TableHeader, 0x00, sizeof(TableHeader));
    TableHeader.Tag     = DX_CONTSTORE_MARK;
    TableHeader.Version = DX_CONTSTORE_VER;
    TableHeader.Size    = sizeof(dxContStoreMTableHeader_t);

    // Write Header to flash
    ret = dxFlash_WriteVerify(pContStore->hFlash,
                              pContStore->nStartAddr,
                              sizeof(TableHeader),
                              (uint8_t *)&TableHeader);

    if (ret != CONTSTORE_ERROR_SUCCESS)
    {
        goto Exit;
    }

    pContStore->HeaderSize = sizeof(dxContStoreMTableHeader_t);
    *hCS = (dxCSHandle_t *)pConstStoreAC[SectorID];

Exit:
    dxMutexGive(pContStore->hMutex);

    return ret;
}

dxContStoreErrorCode_t dxContStore_Erase(uint32_t StartAddr, uint32_t TotalSize)
{
    if (StartAddr == 0 || TotalSize == 0)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    dxFlashHandle_t h = dxFlash_Init();
    if (h == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    dxOS_RET_CODE rc = dxFlash_Erase_SectorWithSize(h, StartAddr, TotalSize);

    ERROR_PRINT("[%s,%d] dxFlash_Erase_SectorWithSize():%d\r\n", __FUNCTION__, __LINE__, rc);

    return (rc == DXOS_SUCCESS) ? CONTSTORE_ERROR_SUCCESS : CONTSTORE_ERROR_FLASH_ERASE_FAIL;
}

dxContStoreErrorCode_t dxContStore_ReadAll(dxCSHandle_t hCS, uint16_t ClientId, dxContStore_Filter_t *pFilter)
{
    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = (dxContStoreAC_t *)hCS;
    uint16_t nCRC16 = 0;

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    dxContStoreSectorID_t SectorID = pContStore->SectorID;
    dxMutexGive(pContStore->hMutex);

    dxContStoreErrorCode_t result = dxContStore_SendEventToWorkerThread(SectorID,
                                                                        CONTSTORE_COMMAND_READALL,
                                                                        ClientId,
                                                                        0,
                                                                        0,
                                                                        nCRC16,
                                                                        NULL,
                                                                        0);

    return result;
}

dxContStoreErrorCode_t dxContStore_Find(dxCSHandle_t hCS, uint16_t ClientId, uint32_t MID, uint32_t DID)
{
    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = (dxContStoreAC_t *)hCS;
    uint16_t nCRC16 = 0;

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    dxContStoreSectorID_t SectorID = pContStore->SectorID;
    dxMutexGive(pContStore->hMutex);

    dxContStoreErrorCode_t result = dxContStore_SendEventToWorkerThread(SectorID,
                                                                        CONTSTORE_COMMAND_FIND,
                                                                        ClientId,
                                                                        MID,
                                                                        DID,
                                                                        nCRC16,
                                                                        NULL,
                                                                        0);

    return result;
}

dxContStoreErrorCode_t dxContStore_Write(dxCSHandle_t hCS, uint16_t ClientId, uint32_t MID, uint32_t DID, uint8_t *pPayload, uint32_t PayloadLen)
{
    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

    dxContStoreAC_t *pContStore = (dxContStoreAC_t *)hCS;
    uint16_t nCRC16 = 0;

#if CONFIG_SUPPORT_CRC16
    nCRC16 = crc16_ccitt_kermit(pPayload, PayloadLen);
#endif // CONFIG_SUPPORT_CRC16

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    dxContStoreSectorID_t SectorID = pContStore->SectorID;
    dxMutexGive(pContStore->hMutex);

    dxContStoreErrorCode_t result = dxContStore_SendEventToWorkerThread(SectorID,
                                                                        CONTSTORE_COMMAND_WRITE,
                                                                        ClientId,
                                                                        MID,
                                                                        DID,
                                                                        nCRC16,
                                                                        pPayload,
                                                                        PayloadLen);

    return result;
}

uint8_t *dxContStore_GetPayloadFromCache(dxCacheMemoryAC_t *pCache)
{
    if (pCache == NULL)
    {
        return NULL;
    }

    if (pCache->StartOffset + pCache->Length > sizeof(gStorePayloadMemory))
    {
        return NULL;
    }

    return &gStorePayloadMemory[pCache->StartOffset];
}

dxContStoreErrorCode_t dxContStore_Uninit(dxCSHandle_t hCS)
{
    if (hCS == NULL)
    {
        return CONTSTORE_ERROR_INVALID_HANDLE;
    }

    if (DX_FLASH_SIZE_TOTAL_MBITS < 32)
    {
        return CONTSTORE_ERROR_NOT_SUPPORT;
    }

#if CONFIG_USE_CSQ
    CSQ_Empty();
#endif // CONFIG_USE_CSQ

    dxContStoreAC_t *pContStore = (dxContStoreAC_t *)hCS;

    dxMutexTake(pContStore->hMutex, DXOS_WAIT_FOREVER);
    dxMutexGive(pContStore->hMutex);


    if (pContStore->hFlash)
    {
        dxFlash_Uninit(pContStore->hFlash);
        pContStore->hFlash = NULL;
    }

    if (pContStore->hMutex)
    {
        dxMutexDelete(pContStore->hMutex);
        pContStore->hMutex = NULL;
    }

    if (pContStore->Worker.Handle)
    {
        dxWorkerTaskDelete(pContStore->Worker.Handle);
        dxMemFree(pContStore->Worker.Handle);
        pContStore->Worker.Handle = NULL;
    }

    if (pContStore->pfnCompletionCallback)
    {
        pContStore->pfnCompletionCallback = NULL;
    }

    dxMemFree(pContStore);
    pContStore = NULL;

    return CONTSTORE_ERROR_SUCCESS;
}
