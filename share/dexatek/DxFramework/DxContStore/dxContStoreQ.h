//============================================================================
// File: dxContStoreQ.h
//
// Author: David Tang
//
// This is proprietary information of Dexatek Technology Ltd.
// All Rights Reserved. Reproduction of this documentation or the
// accompanying programs in any manner whatsoever without the written
// permission of Dexatek Technology Ltd. is strictly forbidden.
//============================================================================
//============================================================================
// Modified History:
//     Version: 0.1
//     Date: 2017/03/03
//     1) Description: Initial
//
//     Version: 0.2
//     Date: 2017/03/06
//     1) Description: Revise API
//============================================================================
#ifndef _DXCONTSTOREQ_H
#define _DXCONTSTOREQ_H

#pragma once

#include "dxBSP.h"
#include "dxContStore.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct dxContStoreMTableItem
{
    uint8_t BlockID;                    // BlockID should be in range of 0x01 ~ 0xFE
    uint8_t Flag;                       // 0x00: BlockID is invalid
} dxContStoreMTableItem_t;

typedef struct dxContStoreMTableItemCache
{
    uint16_t Offset;                    // Offset to DX_HP_STORAGE_ADDR1
    dxContStoreMTableItem_t Block;      // Size of dxContStoreMTableItem_t is 2
} dxContStoreMTableItemCache_t;         // Size of dxContStoreMTableItemCache_t is 4

/**
 * Use for DKRESTCommMsg_t Queue
 */
#ifndef __IAR_SYSTEMS_ICC__
typedef struct _tagContStoreQueueNode ContStoreQueueNode_t;

typedef struct _tagContStoreQueueNode
{
    ContStoreQueueNode_t *Next;
    dxContStoreMTableItemCache_t *pUserData;
} ContStoreQueueNode_t;
#else // __IAR_SYSTEMS_ICC__
typedef struct _tagContStoreQueueNode *PContStoreQueueNode_t;

typedef struct _tagContStoreQueueNode
{
    PContStoreQueueNode_t Next;
    dxContStoreMTableItemCache_t *pUserData;
} ContStoreQueueNode_t;
#endif // __IAR_SYSTEMS_ICC__

void CSQ_Empty(void);
uint8_t CSQ_IsEmpty(void);
uint8_t CSQ_Push(dxContStoreMTableItemCache_t *pUserData);
uint8_t CSQ_Pop(dxContStoreMTableItemCache_t **pUserData);
uint32_t CSQ_Count(void);
dxContStoreMTableItemCache_t *CSQ_GetAt(uint32_t Index);
dxContStoreMTableItemCache_t *CSQ_FindByBlockID(uint8_t BlockID);
uint8_t CSQ_RemoveBlockID(uint8_t BlockID);

#ifdef __cplusplus
}
#endif

#endif // _DXCONTSTOREQ_H
