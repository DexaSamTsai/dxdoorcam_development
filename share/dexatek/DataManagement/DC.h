/*!
 *  @file       dc.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __DC_H__
# define __DC_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================
typedef void *						DC_READER;
typedef void *						DC_WRITER;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern void
DC_RemoveReader( DC_READER reader );

extern int
DC_DropFrame( DC_READER dc_reader );

extern int
DC_ReadFrame( DC_READER reader, AVFrame *frame );

extern int
DC_ReadKeyFrame( DC_READER dc_reader, AVFrame *frame );

extern DC_READER
DC_CreateReader( char *dc_token );

extern u32
DC_GetFrameType( char *dc_token );

extern void
DC_RemoveWriter( DC_WRITER writer );

extern int
DC_WriteFrame( DC_WRITER writer, AVFrame *frame );

extern DC_WRITER
DC_CreateWriter( char *dc_token );

extern int
DC_Init( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif
