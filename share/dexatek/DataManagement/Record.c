/*
 * Record.c
 *
 *  Created on: 2017/07/31
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Record.h"
# include "Format/TS.h"
# include "Format/MP4.h"

//==========================================================================
// Type Define
//==========================================================================
# define RECORD_TEMP_BUFFER_MAX				32
# define RECORD_TEMP_BUFFER_LIMIT_SIZE		( ERTOS_MEM_RECORD_SIZE / RECORD_TEMP_BUFFER_MAX )

enum {
	REC_TEMP_NONE		= 0x00,
	REC_TEMP_WORKING,
};

typedef struct {
	u8		*buff;
	u32		flag;
	u32		size;
	u32		cnt;
	time_t	time;
} rec_temp_t;

typedef struct {
	u32				size;
	u32				type;
    struct timeval	ptime;
	u8				data[ 0 ];
} rec_header_t;

typedef struct {
	void			*handle;
	time_t			target_time;
	u32				pre_secs;
	u32				post_secs;
	u32				total_frames;
	int				( *write )	( void *, AVFrame * );
	int				( *flush )	( void * );
	void			( *close )	( void * );
} rec_ctx_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static struct {
	rec_temp_t	temp[ RECORD_TEMP_BUFFER_MAX ];
	int			idx;
	u32			seq;
} rec;

//==========================================================================
// Static Functions
//==========================================================================
int
rec_generate_file( rec_ctx_t *ctx )
{
	rec_temp_t		*temp	= NULL;
	rec_header_t	*header;
	AVFrame			frame;

	int res	= FAILURE;

	time_t	curr_time, start_time, end_time;
	u8	*dst, *data_end;

	u32 idx	= rec.idx, i;

	if ( ctx->pre_secs > RECORD_TEMP_BUFFER_MAX ) {
		ctx->pre_secs	= RECORD_TEMP_BUFFER_MAX;
	}

	if ( ctx->target_time ) {
		curr_time	= ctx->target_time;
	} else {
		curr_time	= time( NULL );
	}

	start_time	= curr_time - ctx->pre_secs;
	end_time	= curr_time + ctx->post_secs;

	if ( rec.temp[ idx ].time > start_time ) {
		for ( i = 0; i < RECORD_TEMP_BUFFER_MAX; i ++ ) {
			if ( rec.temp[ i ].time < start_time ) {
				for ( idx = ( i + 1 ); idx < ( i + RECORD_TEMP_BUFFER_MAX ); idx ++ ) {
					if ( rec.temp[ ( idx % RECORD_TEMP_BUFFER_MAX ) ].time >= start_time ) {
						idx %= RECORD_TEMP_BUFFER_MAX;
						temp	= &rec.temp[ idx ];
						break;
					}
				}
				break;
			}
		}
	} else {
		temp	= &rec.temp[ idx ];
	}

	if ( temp == NULL ) {
		dbg_warm( "Recording Data not Found!!!" );
		goto err;
	}

	dbg_warm( "=============== Record_GenFile Start" );
	dbg_line( "start_time: %s", ctime( &start_time ) );
	dbg_line( "target_time: %s", ctime( &curr_time ) );
	dbg_line( "end_time: %s", ctime( &end_time ) );

	while ( end_time >= temp->time ) {
		dbg_line( "temp->time: %lu", temp->time );
		if ( temp->flag == REC_TEMP_WORKING ) {
			MSleep( 500 );
			continue;
		} else if ( temp->time < start_time ) {
			if ( ( time( NULL ) - curr_time ) > 5 ) {
				dbg_warm( "Timeout!!!" );
				goto err;
			}

			MSleep( 500 );
			continue;
		} else {
			curr_time	= temp->time;
			dst			= temp->buff;
			data_end	= temp->buff + temp->size;

			while ( dst < data_end ) {
				header = ( rec_header_t* )dst;

				frame.buff	= header->data;
				frame.size	= header->size;
				frame.type	= header->type;
//				frame.ptime	= header->ptime;
				memcpy( &frame.ptime, &header->ptime, sizeof( struct timeval ) );

				ctx->write( ctx->handle, &frame );

				dst += ( sizeof( rec_header_t ) + header->size );

				ctx->total_frames ++;
			}
		}

		idx ++;
		idx %= RECORD_TEMP_BUFFER_MAX;
		temp	= &rec.temp[ idx ];
	}

	if ( ctx->flush ) {
		ctx->flush( ctx->handle );
	}

	dbg_warm( "Total Frames: %d", ctx->total_frames );
	dbg_warm( "=============== Record_GenFile End" );

	res = SUCCESS;

err:
	ctx->close( ctx->handle );

	return res;
}

//==========================================================================
// APIs
//==========================================================================
int
Record_GenTSFile( Record_FileCTX *ctx )
{
	if ( ( ctx == NULL ) || ( ctx->filename == NULL ) ) {
		return FAILURE;
	} else if ( rec.idx < 0 ) {
		return FAILURE;
	} else {
		rec_ctx_t rec_ctx;

		memset( &rec_ctx, 0, sizeof( rec_ctx_t ) );

		rec_ctx.handle	= TS_Open( ctx->filename, 0 );

		if ( rec_ctx.handle == NULL ) {
			dbg_warm( "Do TS_Open( %s, 0 ) Failed!!!", ctx->filename );
			return FAILURE;
		} else {
			rec_ctx.target_time		= ctx->target_time;
			rec_ctx.pre_secs		= ctx->pre_secs;
			rec_ctx.post_secs		= ctx->post_secs;

			rec_ctx.write	= TS_WriteFrame;
			rec_ctx.flush	= NULL;
			rec_ctx.close	= TS_Close;

			return rec_generate_file( &rec_ctx );
		}
	}
}

int
Record_GenMP4File( Record_FileCTX *ctx )
{
	if ( ( ctx == NULL ) || ( ctx->filename == NULL ) ) {
		return FAILURE;
	} else if ( rec.idx < 0 ) {
		return FAILURE;
	} else {
		rec_ctx_t		rec_ctx;
		MP4_CodecCtx	mp4_ctx;

		memset( &rec_ctx, 0, sizeof( rec_ctx_t ) );

		mp4_ctx.video.fps			= ctx->video.fps;
		mp4_ctx.audio.sample_rate	= ctx->audio.sample_rate;
		mp4_ctx.audio.sample_size	= ctx->audio.sample_size;
		mp4_ctx.audio.channels		= ctx->audio.channels;
		mp4_ctx.audio.packet_size	= 1024;

		rec_ctx.handle	= MP4_Open( ctx->filename, &mp4_ctx, 0 );

		if ( rec_ctx.handle == NULL ) {
			dbg_warm( "Do MP4_Open( %s, 0 ) Failed!!!", ctx->filename );
			return FAILURE;
		} else {
			rec_ctx.target_time		= ctx->target_time;
			rec_ctx.pre_secs		= ctx->pre_secs;
			rec_ctx.post_secs		= ctx->post_secs;

			rec_ctx.write	= MP4_WriteFrame;
			rec_ctx.flush	= MP4_Flush;
			rec_ctx.close	= MP4_Close;

			if ( rec_generate_file( &rec_ctx ) == FAILURE ) {
				return FAILURE;
			} else if ( rec_ctx.total_frames == 0 ) {
				return FAILURE;
			} else {
				return SUCCESS;
			}
		}
	}
}

int
Record_FrameIn( AVFrame *frame )
{
	if ( frame ) {
		rec_header_t *header;

		if ( ( ( frame->type & FRAME_AV_MASK ) == FRAME_VIDEO ) && frame->iskey ) {
			if ( rec.idx >= 0 ) {
				rec.temp[ rec.idx ].flag	= REC_TEMP_NONE;
			}

			rec.idx ++;
			rec.idx %= RECORD_TEMP_BUFFER_MAX;

			rec.temp[ rec.idx ].size	= 0;
			rec.temp[ rec.idx ].cnt		= 0;
			rec.temp[ rec.idx ].time	= frame->ptime.tv_sec;
			rec.temp[ rec.idx ].flag	= REC_TEMP_WORKING;
		} else if ( rec.idx < 0 ) {
			return SUCCESS;
		}

		if ( frame->size > ( RECORD_TEMP_BUFFER_LIMIT_SIZE >> 1 ) ) {
			dbg_warm( "frame->size: %d", frame->size );
		}

		if ( ( rec.temp[ rec.idx ].size + frame->size + sizeof( rec_header_t ) ) > RECORD_TEMP_BUFFER_LIMIT_SIZE ) {
			dbg_warm( "( rec.temp[ %d ].size( %d ) + frame.size( %d ) ) > RECORD_TEMP_BUFFER_LIMIT_SIZE( %d )",
					rec.idx, rec.temp[ rec.idx ].size, frame->size, RECORD_TEMP_BUFFER_LIMIT_SIZE );
			return FAILURE;
		}

		header = ( rec_header_t* )&rec.temp[ rec.idx ].buff[ rec.temp[ rec.idx ].size ];
		header->size	= frame->size;
		header->type	= frame->type;
//		header->ptime	= frame->ptime;
		memcpy( &header->ptime, &frame->ptime, sizeof( struct timeval ) );

		memcpy( header->data, frame->buff, frame->size );

		rec.temp[ rec.idx ].size	+= ( sizeof( rec_header_t ) + header->size );
		rec.temp[ rec.idx ].cnt		++;

		return SUCCESS;
	}

	return FAILURE;
}

int
Record_Init()
{
	int i;

	rec.idx		= -1;

	dbg_line( "RECORD_TEMP_BUFFER_MAX: %d", RECORD_TEMP_BUFFER_MAX );

	memset( ( void * )ERTOS_MEM_RECORD_START, 0, ERTOS_MEM_RECORD_SIZE );

	for ( i = 0; i < RECORD_TEMP_BUFFER_MAX; i ++ ) {
		rec.temp[ i ].buff	= ( void * )( ERTOS_MEM_RECORD_START + ( i * RECORD_TEMP_BUFFER_LIMIT_SIZE ) );
	}

	return SUCCESS;
}


