/*
 * Record.h
 *
 *  Created on: 2017¦~7¤ë31¤é
 *      Author: Sam Tsai
 */

# ifndef __RECORD_H__
# define __RECORD_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================
typedef struct {
	u32			target_time;
	u32			pre_secs;
	u32			post_secs;

	struct {
		u32		fps;
	} video;

	struct {
		u32		sample_rate;
		u8		sample_size;
		u8		channels;
	} audio;

	char		*filename;
} Record_FileCTX;

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
Record_GenTSFile( Record_FileCTX *ctx );

extern int
Record_GenMP4File( Record_FileCTX *ctx );

extern int
Record_FrameIn( AVFrame *frame );

extern int
Record_Init( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __RECORD_H__
