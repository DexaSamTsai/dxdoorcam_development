/*
 * Snapshot.h
 *
 *  Created on: 2017¦~7¤ë31¤é
 *      Author: Sam Tsai
 */

# ifndef __SNAPSHOT_H__
# define __SNAPSHOT_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================
typedef struct {
	time_t		target_time;
	u32			pre_secs;
	u32			post_secs;
	char		*filepath;
} Snapshot_FileCTX;

//==========================================================================
// Static Functions
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
Snapshot_GenPicWithAVI( Snapshot_FileCTX *ctx );

extern int
Snapshot_GenPic( u32 pre_secs, u32 post_secs, char *path );

extern int
Snapshot_FrameIn( AVFrame *frame );

extern int
Snapshot_GetFrame( AVFrame *frame );

extern int
Snapshot_Init( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// End __SNAPSHOT_H__
