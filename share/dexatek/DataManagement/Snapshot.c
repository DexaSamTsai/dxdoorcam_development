/*
 * Snapshot.c
 *
 *  Created on: 2017/07/31
 *      Author: Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Snapshot.h"
# include "Format/AVI.h"

//==========================================================================
// Type Define
//==========================================================================
# define SNAPSHOT_TEMP_BUFFER_MAX				8
# define SNAPSHOT_TEMP_BUFFER_LIMIT_SIZE		( ERTOS_MEM_SNAPSHOT_SIZE / 8 )

enum JpegMarker {
	/* start of frame */
	SOF0  = 0xc0,       /* baseline */
	SOF1  = 0xc1,       /* extended sequential, huffman */
	SOF2  = 0xc2,       /* progressive, huffman */
	SOF3  = 0xc3,       /* lossless, huffman */

	SOF5  = 0xc5,       /* differential sequential, huffman */
	SOF6  = 0xc6,       /* differential progressive, huffman */
	SOF7  = 0xc7,       /* differential lossless, huffman */
	JPG   = 0xc8,       /* reserved for JPEG extension */
	SOF9  = 0xc9,       /* extended sequential, arithmetic */
	SOF10 = 0xca,       /* progressive, arithmetic */
	SOF11 = 0xcb,       /* lossless, arithmetic */

	SOF13 = 0xcd,       /* differential sequential, arithmetic */
	SOF14 = 0xce,       /* differential progressive, arithmetic */
	SOF15 = 0xcf,       /* differential lossless, arithmetic */

	DHT   = 0xc4,       /* define huffman tables */

	DAC   = 0xcc,       /* define arithmetic-coding conditioning */

	/* restart with modulo 8 count "m" */
	RST0  = 0xd0,
	RST1  = 0xd1,
	RST2  = 0xd2,
	RST3  = 0xd3,
	RST4  = 0xd4,
	RST5  = 0xd5,
	RST6  = 0xd6,
	RST7  = 0xd7,

	SOI   = 0xd8,       /* start of image */
	EOI   = 0xd9,       /* end of image */
	SOS   = 0xda,       /* start of scan */
	DQT   = 0xdb,       /* define quantization tables */
	DNL   = 0xdc,       /* define number of lines */
	DRI   = 0xdd,       /* define restart interval */
	DHP   = 0xde,       /* define hierarchical progression */
	EXP   = 0xdf,       /* expand reference components */

	APP0  = 0xe0,
	APP1  = 0xe1,
	APP2  = 0xe2,
	APP3  = 0xe3,
	APP4  = 0xe4,
	APP5  = 0xe5,
	APP6  = 0xe6,
	APP7  = 0xe7,
	APP8  = 0xe8,
	APP9  = 0xe9,
	APP10 = 0xea,
	APP11 = 0xeb,
	APP12 = 0xec,
	APP13 = 0xed,
	APP14 = 0xee,
	APP15 = 0xef,

	JPG0  = 0xf0,
	JPG1  = 0xf1,
	JPG2  = 0xf2,
	JPG3  = 0xf3,
	JPG4  = 0xf4,
	JPG5  = 0xf5,
	JPG6  = 0xf6,
	SOF48 = 0xf7,       ///< JPEG-LS
	LSE   = 0xf8,       ///< JPEG-LS extension parameters
	JPG9  = 0xf9,
	JPG10 = 0xfa,
	JPG11 = 0xfb,
	JPG12 = 0xfc,
	JPG13 = 0xfd,

	COM   = 0xfe,       /* comment */

	TEM   = 0x01,       /* temporary private use for arithmetic coding */

	/* 0x02 -> 0xbf reserved */
};

typedef struct {
	u8		*buff;
	u32		size;
	time_t	time;
} snapshot_temp_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static struct {
	snapshot_temp_t	temp[ SNAPSHOT_TEMP_BUFFER_MAX ];
	int				idx;
	u32				seq;
	u32             width;
	u32             height;
} snapshot;

//==========================================================================
// Static Functions
//==========================================================================
static u8 *
mjpeg_put_marker( u8 *p, int code )
{
	*p ++ = 0xff;
	*p ++ = code;
	return p;
}

static inline u8 *
mjpeg_put_u8( u8 *p, u8 value )
{
	*p ++ = value;
	return p;
}

static inline u8 *
mjpeg_put_u16( u8 *p, u16 value )
{
	*p ++ = ( value >> 8 );
	*p ++ = ( value & 0xFF );
	return p;
}

static inline u8 *
mjpeg_put_buff( u8 *p, const u8 *buff, u32 size )
{
	while ( size ) {
		*p ++ = *buff ++;
		size --;
	}

	return p;
}

static int
mjpeg_create_header( u8 *buf )
{
	u8 *p = buf;

    /* Convert from blocks to pixels. */

    /* SOI */
    p = mjpeg_put_marker( p, SOI );

    /* JFIF header */
    p = mjpeg_put_marker( p, APP0 );
    p = mjpeg_put_u16( p, 16 );
    p = mjpeg_put_buff( p, ( const u8 * )"JFIF", 5 );
    p = mjpeg_put_u16( p, 0x0102 );
    p = mjpeg_put_u8( p, 0 );
    p = mjpeg_put_u16( p, 1 );
    p = mjpeg_put_u16( p, 1 );
    p = mjpeg_put_u8( p, 0 );
    p = mjpeg_put_u8( p, 0 );

    return ( p - buf );
}

//==========================================================================
// APIs
//==========================================================================
int
Snapshot_GenPicWithAVI( Snapshot_FileCTX *ctx )
{
	if ( ctx == NULL ) {
		return FAILURE;
	} else if ( snapshot.idx < 0 ) {
		return FAILURE;
	}

	if ( ctx->pre_secs > SNAPSHOT_TEMP_BUFFER_MAX ) {
		ctx->pre_secs	= SNAPSHOT_TEMP_BUFFER_MAX;
	}

	char filename[ 256 ];
	struct tm local = { 0, };
	int fd;
	u32 idx = snapshot.idx, i;
	snapshot_temp_t	*temp	= NULL;
	AVFrame			frame;
	time_t	curr_time, start_time, end_time;

	u32 total_frames	= 0;

	AVIHandle		avi;
	AVI_CodecCtx	avi_ctx;

	if ( ctx->target_time ) {
		curr_time	= ctx->target_time;
	} else {
		curr_time	= time( NULL );
	}

	start_time	= curr_time - ctx->pre_secs;
	end_time	= curr_time + ctx->post_secs;

	if ( snapshot.temp[ idx ].time > start_time ) {
		for ( i = 0; i < SNAPSHOT_TEMP_BUFFER_MAX; i ++ ) {
			if ( snapshot.temp[ i ].time < start_time ) {
				for ( idx = ( i + 1 ); idx < ( i + SNAPSHOT_TEMP_BUFFER_MAX ); idx ++ ) {
					if ( snapshot.temp[ ( idx % SNAPSHOT_TEMP_BUFFER_MAX ) ].time >= start_time ) {
						idx %= SNAPSHOT_TEMP_BUFFER_MAX;
						temp	= &snapshot.temp[ idx ];
						break;
					}
				}
				break;
			}
		}
	} else {
		temp	= &snapshot.temp[ idx ];
	}

	if ( temp == NULL ) {
		dbg_warm( "Snapshot Data not Found!!!" );
		return FAILURE;
	}

	memset( &avi_ctx, 0, sizeof( AVI_CodecCtx ) );

	sprintf( filename, "%s/DxSnapshot.avi", ctx->filepath );

	avi_ctx.video.fps			= 1;
	avi_ctx.video.width			= snapshot.width;
	avi_ctx.video.height		= snapshot.height;

	avi = AVI_Open( filename, &avi_ctx, 0 );

	if ( avi == NULL ) {
		dbg_warm( "Do AVI_Open Failed!!!" );
		return FAILURE;
	}

	dbg_warm( "=============== Snapshot_GenFile Start" );

	dbg_line( "start_time: %s", ctime( &start_time ) );
	dbg_line( "target_time: %s", ctime( &curr_time ) );
	dbg_line( "end_time: %s", ctime( &end_time ) );

	while ( end_time >= temp->time ) {
		if ( temp->time >= start_time ) {
			curr_time	= temp->time;

			localtime_r( &temp->time, &local );

			sprintf( filename, "%s/%04d%02d%02d-%02d%02d%02d.jpg", ctx->filepath,
																( local.tm_year + 1900 ), ( local.tm_mon + 1 ), local.tm_mday,
																local.tm_hour, local.tm_min, local.tm_sec );
			fd	= open( filename, ( O_CREAT | O_RDWR ), 0755 );

			if ( fd > 0 ) {
				write( fd, temp->buff, temp->size );
				close( fd );
			} else {
				dbg_warm( "Create File:%s Failed!!!", filename );
				goto err;
			}

			frame.buff	= temp->buff;
			frame.size	= temp->size;
			frame.type	= FRAME_VIDEO_MJPEG;

			frame.ptime.tv_sec	= temp->time;
			frame.ptime.tv_usec	= 0;

			AVI_WriteFrame( avi, &frame );

			total_frames ++;
		} else {
			if ( ( time( NULL ) - curr_time ) > 5 ) {
				dbg_warm( "Timeout!!!" );
				goto err;
			}

			MSleep( 500 );
			continue;
		}

		idx ++;
		idx %= SNAPSHOT_TEMP_BUFFER_MAX;
		temp	= &snapshot.temp[ idx ];
	}

	AVI_Flush( avi );
	AVI_Close( avi );

	dbg_warm( "Total Frames: %d", total_frames );
	dbg_warm( "=============== Snapshot_GenFile End" );

	if ( total_frames == 0 ) {
		return FAILURE;
	} else {
		return SUCCESS;
	}

err:
	AVI_Close( avi );
	return FAILURE;
}

int
Snapshot_GenPic( u32 pre_secs, u32 post_secs, char *path )
{
	if ( path == NULL ) {
		return FAILURE;
	} else if ( snapshot.idx < 0 ) {
		return FAILURE;
	}

	if ( pre_secs > SNAPSHOT_TEMP_BUFFER_MAX ) {
		pre_secs	= SNAPSHOT_TEMP_BUFFER_MAX;
	}

	char filename[ 256 ];
	struct tm local = { 0, };
	int fd;
	u32 idx = snapshot.idx, i;
	snapshot_temp_t	*temp	= NULL;

	time_t	curr_time	= time( NULL );
	time_t	start_time	= curr_time - pre_secs;
	time_t	end_time	= curr_time + post_secs;

	for ( i = 1; i < SNAPSHOT_TEMP_BUFFER_MAX; i ++ ) {
		if ( snapshot.temp[ ( idx + i ) % SNAPSHOT_TEMP_BUFFER_MAX ].time >= start_time ) {
			idx		= ( idx + i ) % SNAPSHOT_TEMP_BUFFER_MAX;
			temp	= &snapshot.temp[ idx ];
			break;
		}
	}

	if ( temp == NULL ) {
		dbg_warm( "Snapshot Data not Found!!!" );
		return FAILURE;
	}

	dbg_warm( "Snapshot_GenFile Start" );
	dbg_line( "start_time: %lu", start_time );
	dbg_line( "end_time: %lu", end_time );

	while ( end_time >= temp->time ) {
		if ( temp->time >= start_time ) {
			dbg_line( "temp->time: %lu", temp->time );

			curr_time	= temp->time;

			localtime_r( &temp->time, &local );

			sprintf( filename, "%s/%04d%02d%02d-%02d%02d%02d.jpg", path,
																( local.tm_year + 1900 ), ( local.tm_mon + 1 ), local.tm_mday,
																local.tm_hour, local.tm_min, local.tm_sec );
			fd	= open( filename, ( O_CREAT | O_RDWR ), 0755 );

			if ( fd > 0 ) {
				write( fd, temp->buff, temp->size );
				close( fd );
			} else {
				dbg_warm( "Create File:%s Failed!!!", filename );
				return FAILURE;
			}
		} else {
			if ( ( time( NULL ) - curr_time ) > 5 ) {
				dbg_warm( "Timeout!!!" );
				return FAILURE;
			}

			MSleep( 500 );
			continue;
		}

		idx ++;
		idx %= SNAPSHOT_TEMP_BUFFER_MAX;
		temp	= &snapshot.temp[ idx ];
	}

	dbg_warm( "Snapshot_GenFile End" );
	return SUCCESS;
}

int
Snapshot_FrameIn( AVFrame *frame )
{
	if ( frame ) {
		u8  *src = ( u8 * )frame->buff;

		if ( frame->size > SNAPSHOT_TEMP_BUFFER_LIMIT_SIZE ) {
			dbg_warm( "frame.size( %d ) > SNAPSHOT_TEMP_BUFFER_LIMIT_SIZE( %d )",
						frame->size, SNAPSHOT_TEMP_BUFFER_LIMIT_SIZE );
			return FAILURE;
		}

		if ( ( src[ 0 ] == 0xFF ) && ( src[ 1 ] == SOI ) ) {
			u8 *end = src + frame->size;
			u16 len = 0;

			src += 2;

			while ( src < end ) {
				if ( src[ 0 ] == 0xFF ) {
					if ( src[ 1 ] == SOF0 ) {
                        snapshot.height	= ( src[ 5 ] << 8 ) | src[ 6 ];
                        snapshot.width	= ( src[ 7 ] << 8 ) | src[ 8 ];
						break;
					} else {
	                    len = ( src[ 2 ] << 8 ) | src[ 3 ];
	                    src += ( len + 2 );
					}
				} else {
					break;
				}
			}
		}

		snapshot.idx ++;
		snapshot.idx %= SNAPSHOT_TEMP_BUFFER_MAX;

		snapshot.temp[ snapshot.idx ].size	= frame->size;
		snapshot.temp[ snapshot.idx ].time	= frame->ptime.tv_sec;

		memcpy( snapshot.temp[ snapshot.idx ].buff, frame->buff, frame->size );

		return SUCCESS;
	}

	return FAILURE;
}

int
Snapshot_GetFrame( AVFrame *frame )
{
	if ( frame ) {
		int idx = snapshot.idx;

		memset( frame, 0, sizeof( AVFrame ) );

		if ( idx >= 0 ) {
			frame->size			= snapshot.temp[ idx ].size;
			frame->ptime.tv_sec	= snapshot.temp[ idx ].time;
			frame->buff			= snapshot.temp[ idx ].buff;

			return SUCCESS;
		}
	}

	return FAILURE;
}

int
Snapshot_Init()
{
	int i;

	snapshot.idx		= -1;

	dbg_line( "SNAPSHOT_TEMP_BUFFER_MAX: %d", SNAPSHOT_TEMP_BUFFER_MAX );

	memset( ( void * )ERTOS_MEM_SNAPSHOT_START, 0, ERTOS_MEM_SNAPSHOT_SIZE );

	for ( i = 0; i < SNAPSHOT_TEMP_BUFFER_MAX; i ++ ) {
		snapshot.temp[ i ].buff	= ( void * )( ERTOS_MEM_SNAPSHOT_START + ( i * SNAPSHOT_TEMP_BUFFER_LIMIT_SIZE ) );
	}

	return SUCCESS;
}


