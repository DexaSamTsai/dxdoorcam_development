/*!
 *  @file       dc.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include <sys/types.h>

# include "Headers.h"
# include "DC.h"
# include "Project.h"

//==========================================================================
// Type Define
//==========================================================================
# define MEM_PATH				"/var/mem"

# define SHM_SIZE				0x100000		/* make it a 1M shared memory segment */
# define DC_FRAME_SIZE			1024000
# define DC_FREAM_HEADER_MAGIC	0xFF0000FF
# define DC_FREAM_END_MAGIC		0xFFFF00FF
# define DC_GROUP_END_MAGIC		0xFF00FFFF

typedef struct {
	u32					w_seq;
	u32					w_offset;
	u32					key_frame;
	u8					data[ SHM_SIZE + 4 ];
} dc_mem_t;

typedef struct dc_info_t		dc_info_t;
typedef struct dc_reader_t		dc_reader_t;

struct dc_info_t {
	char				*token;
	BOOL				drop;

	dc_mem_t			*mem;
	dc_info_t			*next;
	dc_reader_t			*reader;
};

struct dc_reader_t {
	dc_info_t			*dc;

	BOOL				drop;
	dc_mem_t			*mem;
	u32					r_seq;
	u8					*r_offset;
	dc_reader_t			*next;
};

typedef struct {
	u32				magic;
	u32				seq;
	u32				size;
	u32				type;
	u16				width;
	u16				height;
    struct timeval	ptime;
	u8				data[ 0 ];
} dc_header_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static dc_info_t		*dcs		= NULL;
static pthread_mutex_t	dc_mutex	= PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t	dc_rw_mutex	= PTHREAD_MUTEX_INITIALIZER;

//==========================================================================
// Static Functions
//==========================================================================
static void
dc_free( dc_info_t *dc )
{
	if ( dc ) {
		if ( dc->token ) {
			my_free( dc->token );
		}

		my_free( dc );
	}
}

static dc_info_t *
dc_new( char *token )
{
	dc_info_t *dc = ( dc_info_t * )my_malloc( sizeof( dc_info_t ) );

	if ( dc ) {
		dc->token = my_strdup( token );

		if ( dc->token == NULL ) {
			dbg_error( "Do my_strdup( %s ) Failed!!!", token );
			goto err;
		}

		dc->mem = ( void * )ERTOS_MEM_LIVE_START;

		memset( ( void * )ERTOS_MEM_LIVE_START, 0, ERTOS_MEM_LIVE_SIZE );

		return dc;
	}

err:
	if ( dc ) {
		dc_free( dc );
	}

	return NULL;
}

static int
dc_remove( dc_info_t *dc )
{
	int res = FAILURE;

	pthread_mutex_lock( &dc_mutex );

	if ( dcs == dc ) {
		dcs = dc->next;
		res = SUCCESS;
	} else if ( dcs ) {
		dc_info_t *d	= dcs;

		while ( d->next ) {
			if ( d->next == dc ) {
				d->next		= dc->next;
				res = SUCCESS;
				break;
			}
			d = d->next;
		}
	}

	pthread_mutex_unlock( &dc_mutex );

	return res;
}

static int
dc_insert( dc_info_t *dc )
{
	int res = FAILURE;

	pthread_mutex_lock( &dc_mutex );

	if ( dcs == NULL ) {
		dcs = dc;
		res = SUCCESS;
	} else {
		dc_info_t *d	= dcs;

		while ( d->next ) {
			d = d->next;
		}

		d->next = dc;
		res = SUCCESS;
	}

	pthread_mutex_unlock( &dc_mutex );

	return res;
}

static dc_info_t *
dc_find_by_token( char *token )
{
	dc_info_t *d, *dc = NULL;

	pthread_mutex_lock( &dc_mutex );
	d	= dcs;
	while ( d ) {
		if ( strcmp( d->token, token ) == 0 ) {
			dc = d;
			break;
		}
		d = d->next;
	}

	pthread_mutex_unlock( &dc_mutex );

	return dc;
}

static void
dc_attach_reader( dc_info_t *dc, dc_reader_t *reader )
{
	if ( dc->reader ) {
		dc_reader_t *r = dc->reader;

		while ( r->next ) {
			r	= r->next;
		}

		r->next = reader;
	} else {
		dc->reader = reader;
	}
}

static void
dc_detach_reader( dc_info_t *dc, dc_reader_t *reader )
{
	if ( dc->reader == reader ) {
		dc->reader = reader->next;
	} else {
		dc_reader_t *r = dc->reader;

		while ( r->next ) {
			if ( r->next == reader ) {
				r->next = reader->next;
				break;
			}
			r	= r->next;
		}
	}
}

static void
dc_detach_all_reader( dc_info_t *dc )
{
	pthread_mutex_lock( &dc_mutex );

	if ( dc->reader ) {
		dc_reader_t *r = dc->reader;

		while ( r ) {
			r->mem	= NULL;
			r->dc	= NULL;

			r	= r->next;
		}

		dc->reader = NULL;
	}

	pthread_mutex_unlock( &dc_mutex );
}

static int
dc_add_reader( char *token, dc_reader_t *reader )
{
	dc_info_t *dc;
	int ret = FAILURE;

	pthread_mutex_lock( &dc_mutex );

	dc	= dcs;
	while ( dc ) {
		if ( strcmp( dc->token, token ) == 0 ) {
			dc_attach_reader( dc, reader );

			reader->drop		= TRUE;
			reader->dc			= dc;
			reader->mem			= dc->mem;
			reader->r_offset	= reader->mem->data + reader->mem->w_offset;
			reader->r_seq		= reader->mem->w_seq;

			ret = SUCCESS;
			break;
		}
		dc = dc->next;
	}

	pthread_mutex_unlock( &dc_mutex );

	return ret;
}

static u32
dc_read_frame( dc_reader_t *reader, AVFrame *frame )
{
    dc_header_t			*header;
    dc_header_t			*next_header;
    u32					seq;
	u8					*r_offset;

	pthread_mutex_lock( &dc_rw_mutex );

	while ( TRUE ) {
		header	= ( dc_header_t * )reader->r_offset;

		if ( header->magic == DC_FREAM_END_MAGIC ) {
			break;
		} else if ( header->magic == DC_GROUP_END_MAGIC ) {
			header	= ( dc_header_t * )reader->mem->data;

			if ( ( reader->r_seq + 1 ) != header->seq ) {
				if ( reader->r_seq > header->seq ) {
					if ( ( reader->r_seq > 0x8FFFFFFF ) && ( header->seq < 0x8FFFFFFF ) ) {
						reader->drop	= FALSE;
					} else {
						break;
					}
				} else {
					reader->drop	= FALSE;
				}
			}
		} else if ( header->magic != DC_FREAM_HEADER_MAGIC ) {
			header	= ( dc_header_t * )( reader->mem->data + reader->mem->key_frame );

			if ( header->magic != DC_FREAM_HEADER_MAGIC ) {
				dbg_error( "header->magic != DC_FREAM_HEADER_MAGIC" );

				reader->r_offset	= reader->mem->data;
				reader->drop		= FALSE;
				break;
			} else {
				dbg_line( "Skip Frame" );
			}
		}

		r_offset	= ( u8 * )header + ( sizeof( dc_header_t ) + header->size );
		next_header	= ( dc_header_t * )r_offset;

		 if ( header->size >= SHM_SIZE ) {
			dbg_error( "reader: %p, reader->mem->data: %p, reader->r_offset: %p, header->size = %u, header->magic: %08x", reader, reader->mem->data, reader->r_offset, header->size, header->magic );
			break;
		} else  if ( ( ( u8 * )header + ( sizeof( dc_header_t ) + header->size ) ) > ( reader->mem->data + SHM_SIZE ) ) {
			dbg_error( "( ( u8 * )header + ( sizeof( dc_header_t ) + header->size( %u ) ) ) > ( reader->mem->data + SHM_SIZE )", header->size );
			break;
		} else if ( ( next_header->magic != DC_FREAM_END_MAGIC ) &&
					( next_header->magic != DC_FREAM_HEADER_MAGIC ) &&
					( next_header->magic != DC_GROUP_END_MAGIC ) ) {
			dbg_error( "reader: %p, reader->mem->data: %p, reader->r_offset: %p, header->size = %u, header->magic: %08x", reader, reader->mem->data, reader->r_offset, next_header->size, next_header->magic );
			break;
		}

		if ( reader->drop == FALSE ) {
			seq = reader->r_seq + 1;
			if ( seq > header->seq ) {
				//dbg_line( "reader->r_seq = %u, header->seq = %u", reader->r_seq, header->seq );
				break;
			}
		}

		if ( ( header->type == FRAME_VIDEO_MPEG4_I ) ||
			 ( header->type == FRAME_VIDEO_H264_I ) ) {
			reader->drop	= FALSE;
			frame->iskey	= TRUE;
		} else if ( header->type == FRAME_VIDEO_MJPEG ) {
			reader->drop	= FALSE;
			frame->iskey = TRUE;
		} else if ( ( header->type & FRAME_AV_MASK )== FRAME_AUDIO ) {
			frame->iskey = TRUE;
		} else {
			frame->iskey = FALSE;
		}

		reader->r_offset	= r_offset;
		reader->r_seq		= header->seq;

		if ( ( ( header->type & FRAME_AV_MASK )== FRAME_VIDEO ) &&
			 ( reader->drop == TRUE ) ) {
			continue;
		}

		frame->size			= header->size;
		frame->type			= header->type;
		frame->buff			= header->data;
//		frame->ptime		= header->ptime;
		memcpy( &frame->ptime, &header->ptime, sizeof( header->ptime ) );

		break;
	}

	pthread_mutex_unlock( &dc_rw_mutex );

	return frame->size;
}

static int
dc_write_frame( dc_mem_t *mem, AVFrame *frame )
{
	dc_header_t	*header;

	pthread_mutex_lock( &dc_rw_mutex );

    header = ( dc_header_t * )( mem->data + mem->w_offset );

	if ( ( mem->w_offset + sizeof( dc_header_t ) + frame->size + 4 ) > SHM_SIZE ) {
		header->magic	= DC_GROUP_END_MAGIC;
		mem->w_offset	= 0;
		header	= ( dc_header_t * )( mem->data + mem->w_offset );
	}

	if ( ( frame->type == FRAME_VIDEO_MPEG4_I ) ||
		 ( frame->type == FRAME_VIDEO_H264_I ) ||
		 ( frame->type == FRAME_VIDEO_MJPEG ) ) {
		mem->key_frame	= mem->w_offset;
	}

	mem->w_offset += ( sizeof( dc_header_t ) + frame->size );

	memcpy( header->data, frame->buff, frame->size );

	header->magic	= DC_FREAM_HEADER_MAGIC;
	header->seq		= mem->w_seq;
	header->size	= frame->size;
	header->type	= frame->type;
	memcpy( &header->ptime, &frame->ptime, sizeof( header->ptime ) );
//	header->ptime	= frame->ptime;

	header	= ( dc_header_t * )( mem->data + mem->w_offset );
	header->magic	= DC_FREAM_END_MAGIC;

	mem->w_seq ++;

	pthread_mutex_unlock( &dc_rw_mutex );

	return SUCCESS;
}

//==========================================================================
// APIs
//==========================================================================
void
DC_RemoveReader( DC_READER dc_reader )
{
	if ( dc_reader ) {
		if ( ( ( dc_reader_t * )dc_reader )->dc ) {
			pthread_mutex_lock( &dc_mutex );
			dc_detach_reader( ( ( dc_reader_t * )dc_reader )->dc, ( dc_reader_t * )dc_reader );
			pthread_mutex_unlock( &dc_mutex );
		}

		my_free( dc_reader );
	}
}

int
DC_DropFrame( DC_READER dc_reader )
{
    if ( dc_reader ) {
    	( ( dc_reader_t * )dc_reader )->drop	= TRUE;

		return SUCCESS;
    }

	return FAILURE;
}

int
DC_ReadFrame( DC_READER dc_reader, AVFrame *frame )
{
    if ( dc_reader && frame ) {
    	dc_reader_t	*reader	= ( dc_reader_t * )dc_reader;

    	frame->size	= 0;

		return dc_read_frame( reader, frame );
    }

	return FAILURE;
}

int
DC_ReadKeyFrame( DC_READER dc_reader, AVFrame *frame )
{
    if ( dc_reader && frame ) {
    	dc_reader_t	*reader	= ( dc_reader_t * )dc_reader;

		if ( reader->mem->key_frame == 0 ) {
        	dbg_error( "reader->key_frame == 0" );
        	return FAILURE;
        } else {
            dc_header_t			*header;

    		header	= ( dc_header_t * )( reader->mem->data + reader->mem->key_frame );
    		if ( header->magic == DC_FREAM_HEADER_MAGIC ) {
    			frame->iskey	= 1;
    			frame->size		= header->size;
    			frame->type		= header->type;
    			frame->buff		= header->data;
//    			frame->ptime	= header->ptime;
				memcpy( &frame->ptime, &header->ptime, sizeof( header->ptime ) );
    		} else {
    			dbg_error( "header->magic != DC_FREAM_HEADER_MAGIC" );
    			frame->size			= 0;
    		}

        	return frame->size;
        }
    }

	return FAILURE;
}

DC_READER
DC_CreateReader( char *dc_token )
{
	if ( dc_token ) {
		dc_reader_t *reader = ( dc_reader_t * )my_malloc( sizeof( dc_reader_t ) );

		if ( reader ) {
			if ( dc_add_reader( dc_token, reader ) == FAILURE ) {
				my_free( reader );
				reader = NULL;
			}
		} else {
			dbg_error( "Do my_malloc Failed!!!" );
		}

		return reader;
	} else {
		dbg_line( "dc_token == NULL!!!" );
		return NULL;
	}
}

u32
DC_GetFrameType( char *dc_token )
{
	if ( dc_token ) {
		dc_info_t	*dc = dc_find_by_token( dc_token );
		u32 frame_type = 0;

		if ( dc ) {
			if ( dc->mem->key_frame ) {
		        dc_header_t			*header;

		    	pthread_mutex_lock( &dc_rw_mutex );

				header	= ( dc_header_t * )( dc->mem->data + dc->mem->key_frame );
				if ( header->magic == DC_FREAM_HEADER_MAGIC ) {
					frame_type		= header->type;
				}

		    	pthread_mutex_unlock( &dc_rw_mutex );
			}
		}
		return frame_type;
	} else {
		dbg_line( "dc_token == NULL!!!" );
		return 0;
	}
}

void
DC_RemoveWriter( DC_WRITER dc_writer )
{
	dc_remove( dc_writer );
	dc_detach_all_reader( dc_writer );
	dc_free( dc_writer );
}

int
DC_WriteFrame( DC_WRITER dc_writer, AVFrame *frame )
{
    dc_info_t			*writer	= ( dc_info_t * )dc_writer;

	if ( writer == NULL ) {
		return FAILURE;
	} else if ( writer->mem == NULL ) {
		return FAILURE;
	}

	if ( ( frame->type == FRAME_VIDEO_MPEG4_I ) ||
		 ( frame->type == FRAME_VIDEO_H264_I ) ) {
		writer->drop		= FALSE;
	} else if ( frame->type == FRAME_VIDEO_MJPEG ) {
		writer->drop		= FALSE;
	} else if ( ( frame->type & FRAME_AV_MASK )== FRAME_AUDIO ) {
		writer->drop		= FALSE;
	}

	if ( writer->drop == FALSE ) {
		dc_write_frame( writer->mem, frame );
	} else {
		dbg_warm( "%s: writer->drop = %d, Dropped Frame Size: %d, Type: %04x", writer->token, writer->drop, frame->size, frame->type );
	}

	return 0;
}

DC_WRITER
DC_CreateWriter( char *dc_token )
{
	if ( dc_token ) {
		dc_info_t *writer = dc_new( dc_token );

		if ( writer ) {
			dc_insert( writer );

			return writer;
		} else {
			dbg_error( "Do my_malloc Failed!!!" );
			return NULL;
		}
	} else {
		return NULL;
	}
}

int
DC_Init()
{
	pthread_mutex_init( &dc_mutex, NULL );
	pthread_mutex_init( &dc_rw_mutex, NULL );

	return SUCCESS;
}
