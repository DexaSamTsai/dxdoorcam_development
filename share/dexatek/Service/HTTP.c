/*!
 *  @file       HTTP.c
 *  @version    1.00
 *  @author     Sam Tsai
 */

//==========================================================================
// Header File
//==========================================================================
# include "Socket.h"
# include "HTTP.h"

//==========================================================================
// Type Declaration
//==========================================================================

//==========================================================================
static struct {
	int			num;
	const char	*description;
} *http_err_desc, err_description[ ] = {
	{ 100, "Continue" },
	{ 101, "Switching Protocols" },
	{ 200, "OK" },
	{ 201, "Created" },							// RTSP: Record
	{ 202, "Accepted" },
	{ 203, "Non-Authoritative Information" },
	{ 204, "No Content" },
	{ 205, "Reset Content" },
	{ 206, "Partial Content" },
	{ 250, "Low on Storage Space" },			// RTSP: Record
	{ 300, "Multiple Choices" },
	{ 301, "Moved Permanently" },
	{ 302, "Found" },
	{ 303, "See Other" },
	{ 304, "Not Modified" },
	{ 305, "Use Proxy" },
	{ 307, "Temporary Redirect" },
	{ 400, "Bad Request" },
	{ 401, "Unauthorized" },
	{ 402, "Payment Required" },
	{ 403, "Forbidden" },
	{ 404, "Not Found" },
	{ 405, "Method Not Allowed" },
	{ 406, "Not Acceptable" },
	{ 407, "Proxy Authentication Required" },
	{ 408, "Request Timeout" },
	{ 409, "Conflict" },
	{ 410, "Gone" },
	{ 411, "Length Required" },
	{ 412, "Precondition Failed" },				// RTSP: Describe, Setup
	{ 413, "Request Entity Too Large" },
	{ 414, "Request URI Too Long" },
	{ 415, "Unsupported Media Type" },
	{ 416, "Requested Range Not Satisfiable" },
	{ 451, "Parameter Not Understood" },		// RTSP: Setup
	{ 452, "Conference Not Found" },
	{ 453, "Not Enough Bandwidth" },			// RTSP: Setup
	{ 454, "Session Not Found" },				// RTSP: Setup
	{ 455, "Method Not Valid in This State" },
	{ 456, "Header Field Not Valid for Resource" },
	{ 457, "Invalid Range" },					// RTSP: Play
	{ 458, "Parameter Is Read-Only" },			// RTSP: Set_Parameter
	{ 459, "Aggregate operation not allowed" },
	{ 460, "Only aggregate operation allowed" },
	{ 461, "Unsupported transport" },
	{ 462, "Destination unreachable" },
	{ 500, "Internal Server Error" },
	{ 501, "Not Implemented" },
	{ 502, "Bad Gateway" },
	{ 503, "Service Unavailable" },
	{ 504, "Gateway Timeout" },
	{ 505, "HTTP Version Not Supported" },
	{ 551, "Option not supported" },
	{ 0,	NULL			}
};

char	*network_name = "efnvr";

//==========================================================================
// Static Functions
//==========================================================================
static void
http_set_mime( char *mime, const char *string )
{
	char *f_type = strstr( &string[ strlen( string ) - 5 ], "." );
	static struct {
		const char *ext, *type;
	} *p, types[ ] = {
		{ "svg",		"image/svg+xml"					},
		{ "torrent",	"application/x-bittorrent"		},
		{ "wav",		"audio/x-wav"					},
		{ "mp3",		"audio/x-mp3"					},
		{ "mid",		"audio/mid"						},
		{ "m3u",		"audio/x-mpegurl"				},
		{ "ram",		"audio/x-pn-realaudio"			},
		{ "ra",			"audio/x-pn-realaudio"			},
		{ "doc",		"application/msword",			},
		{ "exe",		"application/octet-stream"		},
		{ "zip",		"application/x-zip-compressed"	},
		{ "xls",		"application/excel"				},
		{ "tgz",		"application/x-tar-gz"			},
		{ "tar.gz",		"application/x-tar-gz"			},
		{ "tar",		"application/x-tar"				},
		{ "gz",			"application/x-gunzip"			},
		{ "arj",		"application/x-arj-compressed"	},
		{ "rar",		"application/x-arj-compressed"	},
		{ "rtf",		"application/rtf"				},
		{ "pdf",		"application/pdf"				},
		{ "jpeg",		"image/jpeg"					},
		{ "png",		"image/png"						},
		{ "mpg",		"video/mpeg"					},
		{ "mpeg",		"video/mpeg"					},
		{ "asf",		"video/x-ms-asf"				},
		{ "avi",		"video/x-msvideo"				},
		{ "bmp",		"image/bmp"						},
		{ "jpg",		"image/jpeg"					},
		{ "mjpg",		"image/mjpg"					},
		{ "gif",		"image/gif"						},
		{ "ico",		"image/x-icon"					},
		{ "txt",		"text/plain"					},
		{ "css",		"text/css"						},
		{ "htm",		"text/html"						},
		{ "html",		"text/html"						},
		{ "xml",		"text/xml"						},
		{ "js",			"application/x-javascript"		},
		{ "cab",		"application/cab"				},
		{ "ocx",		"application/ocx"				},
		{ "3gp",		"application/x-rtsp-tunnelled"	},
		{ "sdp",		"video/quicktime"				},
		{ "mp4",		"video/mp4"						},
		{ "h264",		"video/h264"					},
		{ NULL,			NULL							}
	};

	if ( f_type == NULL ) {
		sprintf( mime, "text/html" );
		return;
	}
	f_type ++;

	if ( strcmp( f_type, "mp4" ) == 0 ) {
		sprintf( mime, "application/octet-stream" );
	} else {
		for ( p = types; p->ext != NULL; p ++ ) {
			if ( strcmp( f_type, p->ext ) == 0 ) {
				sprintf( mime, "%s", p->type );
				return;
			}
		}
		sprintf( mime, "text/html" );
	}
}

static int
http_urldecode( char *from, char *to )
{
	int i, a, b;
#define HEXTOI( x )	( isdigit( x ) ? x - '0' : x - 'W' )

	for ( i = 0; *from != '\0'; i ++, from ++ ) {
		if ( from[ 0 ] == '%' &&
			isxdigit( ( u8 ) from[ 1 ] ) &&
			isxdigit( ( u8 ) from[ 2 ] ) ) {
			a = tolower( from[ 1 ] );
			b = tolower( from[ 2 ] );
			to[ i ] = ( HEXTOI( a ) << 4 );
			to[ i ] += HEXTOI( b );
//			if ( to[ i ] == 0 ) {
//				to[ i ] = ' ';
//			}
			from += 2;
		} else if ( from[ 0 ] == '+' ) {
			to[ i ] = ' ';
		} else
			to[ i ] = from[ 0 ];
	}
	to[ i ] = '\0';
	return i;
}

//==========================================================================
// APIs
//==========================================================================
int
http_get_header_len( const char *buf, size_t buflen )
{
	const char	*s;

	if ( ( s = strstr( buf, "\r\n\r\n" ) ) ) {
		return ( s - buf ) + 4;
	} else if ( ( s = strstr( buf, "\n\n" ) ) ) {
		return ( s - buf ) + 2;
	} else {
		return 0;
	}
}

int
http_parser_header( char *header, struct header_env *env, int env_cnt )
{
	char	*field	= header;
	char	*value	= header;
	char	*next	= header;
	int		cnt = 0;

	if ( ( env == NULL ) || ( env_cnt == 0 ) ) {
		return 0;
	}

	memset( env, 0, ( sizeof( struct header_env ) * env_cnt ) );

	if ( header == NULL ) {
		return 0;
	} else {
		field = strstr( header, "\r\n" );
		if ( field ) {
			field[ 0 ] = 0;
			field += 2;
		}
	}

	while ( field && ( cnt < env_cnt ) ) {
		next = strstr( field, "\r\n" );
		if ( next ) {
			if ( next == field ) {
				break;
			}
			next[ 0 ] = 0;
			next += 2;
		}

		value = strstr( field, ": " );
		if ( value ) {
			*value = 0;
			value += 2;
		}

		env[ cnt ].field = field;
		env[ cnt ].value = value;

		field = next;

		cnt ++;
	}

	return cnt;
}

void
http_parser_cgi( char *src, struct header_env *env, u32 env_cnt )
{
static char empty_value[ 1 ] = { "\0" };
	char	*field = src;
	char	*value = src;
	char	*next;
	int		cnt = 0;

	if ( ( env == NULL ) || ( env_cnt == 0 ) ) {
		return;
	}

	memset( env, 0, ( sizeof( struct header_env ) * env_cnt ) );

	if ( !src ) {
		return;
	} else if ( field[ 0 ] == '?' ) {
		field ++;
	}

	while ( field && ( cnt < env_cnt ) ) {
		next	= strchr( field, '&' );
		if ( next ) {
			if ( next == field ) {
				break;
			}
			next[ 0 ] = 0;
			next ++;
		}

		value = strchr( field, '=' );
		if ( value ) {
			value[ 0 ] = 0;
			value ++;
			http_urldecode( value, value );
		} else {
			value = empty_value;
		}

		env[ cnt ].field	= field;
		env[ cnt ].value	= value;

		field = next;

		cnt ++;
	}
}

char *
http_get_field( struct header_env *env, char *field, int env_cnt )
{
	if ( !env ) {
		return NULL;
	} else {
		int i;

		for ( i = 0; i < env_cnt; i ++ ) {
			if ( env[ i ].field ) {
				if ( strcasecmp( env[ i ].field, field ) == 0 ) {
					if ( env[ i ].value == NULL ) {
						return NULL;
					} else {
						return env[ i ].value;
					}
				}
			} else {
				break;
			}
		}

		return NULL;
	}
}

// Request:
// HTTP/1.1 401 Unauthorized
// WWW-Authenticate: Digest
// realm="testrealm@host.com",
// qop="auth,auth-int",
// nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
// opaque="5ccc069c403ebaf9f0171e9517f40e41"
//
int
http_request_auth( http_info_t *http )
{
	char *auth;

	if ( ( auth = http_get_field( http->http_env, "WWW-Authenticate", MAX_HTTP_TAG ) ) ) {
		if ( strncmp( auth, "Digest ", 7 ) == 0 ) {
			http->auth_req = WEB_AUTH_TYPE_DIGEST;
			memset( &http->dig, 0, sizeof( struct digest ) );
			auth_digest_param( &http->dig, auth );
			return 0;
		} else if ( strncmp( auth, "Basic ", 6 ) == 0 ) {
			http->auth_req = WEB_AUTH_TYPE_BASE64;
			return 0;
		}
	} else {
		dbg_line( "WWW-Authenticate not found..." );
	}
	return -1;
}

int
http_responce_auth( http_info_t *http, char *method, char *username, char *password, char *data )
{
	if ( http->auth_req == WEB_AUTH_TYPE_DIGEST ) {
		return auth_digest_make( &http->dig, method, http->uri, username, password, data );
	} else if ( http->auth_req == WEB_AUTH_TYPE_BASE64 ) {
		return auth_basic_make( username, password, data );
	}

	return -1;
}

// Responce:
// Authorization: Digest username="Mufasa",
// realm="testrealm@host.com",
// nonce="dcd98b7102dd2f0e8b11d0f600bfb0c093",
// uri="/dir/index.html",
// qop=auth,
// nc=00000001,
// cnonce="0a4f113b",
// response="6629fae49393a05397450978507c4ef1",
// opaque="5ccc069c403ebaf9f0171e9517f40e41"
int
http_check_auth( http_info_t *http, char *method, char *username )
{
	char *auth;

	if ( ( auth = http_get_field( http->http_env, "Authorization", MAX_HTTP_TAG ) ) ) {
		if ( strncmp( auth, "Digest ", 7 ) == 0 ) {
			auth_free_digest( &http->dig );
			auth_digest_parser( &http->dig, auth );

			if ( auth_digest_check( &http->dig, method, username ) < 0 ) {
				auth_free_digest( &http->dig );
				return HTTP_AUTH_CHK_ILLEGAL;
			} else {
				auth_free_digest( &http->dig );
				return HTTP_AUTH_CHK_PASS;
			}
		} else if ( strncmp( auth, "Basic ", 6 ) == 0 ) {
			auth += 6;

			if ( auth_basic_check( auth, username ) < 0 ) {
				return HTTP_AUTH_CHK_ILLEGAL;
			} else {
				return HTTP_AUTH_CHK_PASS;
			}
		} else {
			return HTTP_AUTH_CHK_ILLEGAL;
		}
	} else {
		return HTTP_AUTH_CHK_NO_AUTH;
	}
}

int
http_parser_responce( http_info_t *http )
{
	char	str_status[ 64 ];

	http->reqlen = http_get_header_len( ( char * )http->header.buf, http->header.size );

	if ( http->reqlen == 0 ) {
		return 0;
	}

	http->rw.size = http->header.size - http->reqlen;

	if ( http->rw.size ) {
		memcpy( http->rw.buf, &http->header.buf[ http->reqlen ], http->rw.size );
	}

	http->rw.buf[ http->rw.size ] = 0;
	http->header.buf[ http->reqlen ] = 0;

	if ( sscanf( ( char * )http->header.buf, "%15[a-zA-Z/.0-9] %d %63[a-zA-Z0-9]", http->proto, &http->status, str_status ) != 3 ) {
		int i;

		dbg_line( "%s", http->header.buf );
		dbg_line( "http_parser_responce error: " );

		for ( i = 0; i < 16; i ++ ) {
			printf( "%02x ", http->header.buf[ i ] );
		}
		printf( "\n" );

		http->status = 700;				// Un-Know Fail
		return NET_ERR_HTTP_PARSER_FAIL;
	}

	http_parser_header( ( char * )http->header.buf, http->http_env, MAX_HTTP_TAG );

	if ( http->status != 200 ) {
		if ( http->status == 401 ) {
			http_request_auth( http );
		}
		return FAILURE;
	} else {
		char * temp;

		if( ( temp = http_get_field( http->http_env, "Content-Length", MAX_HTTP_TAG ) ) ) {
			http->cclength = strtoul( temp, NULL, 0 );
		} else {
			http->cclength = 0;
		}

		if( ( temp = http_get_field( http->http_env, "Content-Base", MAX_HTTP_TAG ) ) ) {
			int size = sprintf( http->uri, temp );
			if ( http->uri[ size - 1 ] == '/' ) {
				http->uri[ size - 1 ]	= 0;
			}
		}

		return http->reqlen;
	}
}

int
http_parser_request( http_info_t *http )
{
	char	fmt[ 32 ];

	http->reqlen = http_get_header_len( ( char * )http->header.buf, http->header.size );

	if ( http->reqlen == 0 ) {
		return 0;
	}

	http->rw.size = http->header.size - http->reqlen;

	if ( http->rw.size ) {
		memcpy( http->rw.buf, &http->header.buf[ http->reqlen ], http->rw.size );
	}

	http->rw.buf[ http->rw.size ] = 0;
	http->header.buf[ http->reqlen ] = 0;


	snprintf( fmt, sizeof( fmt ), "%%%us %%%us %%%us", ( unsigned ) sizeof( http->method ) - 1,
													( unsigned ) sizeof( http->uri ) - 1,
													( unsigned ) sizeof( http->proto ) - 1 );

	if ( sscanf( ( char * )http->header.buf, fmt, http->method, http->uri, http->proto ) != 3 ) {
		http->status = 404;
		return FAILURE;
	}

	http_parser_header( ( char * )http->header.buf, http->http_env, MAX_HTTP_TAG );

	return http->reqlen;
}

int
http_init_header( http_info_t *http )
{
	if ( !http )
		return -1;

	http->header.size		= 0;
	http->rw.size			= 0;
	http->reqlen			= 0;
	http->proto[ 0 ]		= 0;
	http->cclength			= 0;
	http->status			= 0;

// 	http->http_env[ 0 ].field	= NULL;
// 	http->env[ 0 ].field		= NULL;

	return 0;
}

int
http_build_reply_header( char *service, char *path, http_info_t *header )
{
	char			pdate[ 64 ];
	const char		*fmt;
	time_t			current_time = time( NULL );

	/* Prepare Etag, Date, Last-Modified headers */
	fmt = "%a, %d %b %Y %H:%M:%S GMT";

	strftime( pdate, sizeof( pdate ), fmt, localtime( &current_time ) );

	/* Local read buffer should be empty */
	if ( header->status == 200 ) {
		char lm[ 64 ], mime[ 64 ];

		/* Figure out the mime type */
		if ( path == NULL ) {
			sprintf( mime, "text/html" );
		} else {
			http_set_mime( mime, path );
		}

		if ( header->st.st_mtime ) {
			strftime( lm, sizeof( lm ), fmt, localtime( &header->st.st_mtime ) );
		} else {
			strftime( lm, sizeof( lm ), fmt, localtime( &current_time ) );
		}

		header->rw.size =  sprintf( ( char * )header->rw.buf,	"%s 200 OK\r\n"
																"Date: %s\r\n"
																"Last-Modified: %s\r\n"
																"Content-Type: %s\r\n",
																header->proto,
																pdate, lm, mime );

		if ( strstr( mime, "text" ) || strstr( mime, "application" ) ) {
			header->rw.size +=  sprintf( ( char * )&header->rw.buf[ header->rw.size ],	"Cache-Control: no-cache\r\n" );
		}

		if ( header->st.st_size ) {
			header->rw.size +=  sprintf( ( char * )&header->rw.buf[ header->rw.size ],	"Content-Length: %u\r\n", ( u32 )header->st.st_size );
		}

		header->rw.size +=  sprintf( ( char * )&header->rw.buf[ header->rw.size ],	"Connection: close\r\n\r\n" );

		return header->rw.size;
	} else {
		char auth_req[ 128 ], str_html[ 128 ];
		int	len = 0;

		if ( header->status == 401 ) {
			if ( header->auth_req & WEB_AUTH_TYPE_DIGEST ) {
				len = sprintf( auth_req, "WWW-Authenticate: Digest qop=\"auth\", stale=false, algorithm=MD5, realm=\"%s\", nonce=\"%lu\"\r\n",
									service, ( unsigned long ) current_time );
			} else {
				len = sprintf( auth_req, "WWW-Authenticate: Basic realm=%s\r\n", service );
			}

			if ( len > 128 ) {
				dbg_line( "auth error" );
			}
		} else {
			auth_req[ 0 ] = 0;
		}

		for ( http_err_desc = err_description; http_err_desc->description != NULL; http_err_desc ++ ) {
			if ( http_err_desc->num == header->status ) {
				sprintf( str_html,	"<HTML><HEAD><TITLE>%d %s</TITLE></HEAD>\n"
									"<BODY><H1>%d %s</H1></BODY></HTML>\n",
									header->status, http_err_desc->description,
									header->status, http_err_desc->description );

				header->rw.size = sprintf( ( char * )header->rw.buf,	"%s %d %s\r\n"
																		"Date: %s\r\n"
																		"Content-type: text/html\r\n"
																		"Content-Length: %d\r\n"
																		"%s"
																		"%s"
																		"\r\n"
																		"%s",
																		header->proto, header->status, http_err_desc->description,
																		pdate,
																		( int )strlen( str_html ),
																		auth_req,
																		( header->status == 401 ) ? "Connection: keep-alive\r\n" : "Connection: close\r\n",
																		str_html );
				return header->rw.size;
			}
		}

		dbg_line( "unknow status..." );
		return 0;
	}
}

void
http_release( http_info_t *http )
{
	if ( http ) {
		auth_free_digest( &http->dig );
	}
}

int
HTTP_MakeAuthResponce( char *description, char *method, char *uri, char *username, char *password, char *data )
{
	int	size = 0;

	if ( description && strncmp( description, "Digest ", 7 ) == 0 ) {
		struct digest	dig;

		memset( &dig, 0, sizeof( struct digest ) );
		if ( auth_digest_param( &dig, description ) == 0 ) {
			size = auth_digest_make( &dig, method, uri, username, password, data );

			if ( size < 0 ) {
				size = 0;
			}
		}

		auth_free_digest( &dig );
		return size;
	} else {
		size = auth_basic_make( username, password, data );

		if ( size < 0 ) {
			size = 0;
		}
		return size;
	}
}


