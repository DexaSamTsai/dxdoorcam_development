/*!
 *  @file       HTTP.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __HTTP_H__
# define __HTTP_H__
//==========================================================================
// Include File
//==========================================================================
# include "HTTPd.h"
# include "Auth.h"

//==========================================================================
// Type Declaration
//==========================================================================
enum {
	METHOD_NONE = 0,
	METHOD_GET,
	METHOD_POST,
	METHOD_PUT,
	METHOD_DELETE,
	METHOD_HEAD,
};

enum {
	HTTP_AUTH_CHK_PASS		= 0,
	HTTP_AUTH_CHK_NO_AUTH	= -1,
	HTTP_AUTH_CHK_ILLEGAL	= -2,
};

#define MAX_HTTP_TAG					32
#define MAX_HTTP_PARAM					64

#define	IO_MAX							( 4092 )

typedef struct {
    int		size;					/* IO finished		*/
    int     offset;
    u8		buf[ IO_MAX + 4 ];		/* Buffer		*/
} io;

struct header_env {
	char	*field;
	char	*value;
};

struct http_responce_header {
	io					responce;					/* Saved responce		*/
	int					reslen;						/* Responce length		*/
	char				proto[ 16 ];				/* HTTP protocol		*/
	int					cclength;					/* Client Content-Length	*/
	int					status;

	struct header_env	http_env[ MAX_HTTP_TAG ];
	struct header_env	env[ MAX_HTTP_PARAM ];
};

typedef struct {
	io					header;						/* Saved request		*/
    io               	rw;
	int					reqlen;						/* Request length		*/
	int					http_method;				/* HTTP method			*/

	char				method[ 16 ];				/* Used method			*/
	char				uri[ MAX_URI_LENGTH ];		/* Url-decoded URI		*/
	char				proto[ 16 ];				/* HTTP protocol		*/
	char				host[ 64 ];					/* Host		*/

	struct header_env	http_env[ MAX_HTTP_TAG ];
	struct header_env	env[ MAX_HTTP_PARAM ];

	int					auth_req;
	char*				session;
	int					keep_alive;					/* Local file descriptor	*/
	int					cclength;					/* Client Content-Length	*/
	time_t				ims;						/* If-Modified-Since:		*/
	struct stat			st;							/* Stats of requested file	*/
	int					fd;							/* Local file descriptor	*/

	int					status;
    u32					timeout;

	struct digest		dig;
} http_info_t;

//==========================================================================
// HTTP API
//==========================================================================
extern int
http_get_header_len( const char *buf, size_t buflen );

extern int
http_parser_header( char *header, struct header_env *env, int env_cnt );

extern void
http_parser_cgi( char *src, struct header_env *env, u32 env_cnt );

extern char *
http_get_field( struct header_env *env, char *field, int env_cnt );

extern int
http_request_auth( http_info_t *http );

extern int
http_responce_auth( http_info_t *http, char *method, char *username, char *password, char *data );

extern int
http_check_auth( http_info_t *http, char *method, char *username );

extern int
http_parser_responce( http_info_t *http );

extern int
http_parser_request( http_info_t *http );

extern int
http_init_header( http_info_t *http );

extern void
http_release( http_info_t *http );

extern int
http_build_reply_header( char *service, char *path, http_info_t *header );

//==========================================================================
#endif // __HTTP_H__



