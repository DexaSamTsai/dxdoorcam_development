/*!
 *  @file       Socket.c
 *  @version    1.00
 *  @author     Sam Tsai
 */

//==========================================================================
// Header File
//==========================================================================
#include "Socket.h"

extern int h_errno;

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Static Param
//==========================================================================

//==========================================================================
// Static Function
//==========================================================================
static int
socket_get_ipaddr( char *addr, char *ipstr, int type )
{
	struct addrinfo hints, *servinfo, *p;
	int status, res = FAILURE;

	memset( &hints, 0, sizeof( hints ) );
	hints.ai_family		= type; // AF_INET 或 AF_INET6 可以指定版本
	hints.ai_socktype	= SOCK_STREAM;

	if ( ( status = getaddrinfo( addr, NULL, &hints, &servinfo ) ) != 0 ) {
		dbg_line( "%s: %s", addr, gai_strerror( status ) );
		return res;
	}

	for ( p = servinfo; p != NULL; p = p->ai_next ) {
		if ( p->ai_family == type ) {
			if ( p->ai_family == AF_INET ) { // IPv4
				struct sockaddr_in *ipv4 = ( struct sockaddr_in * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv4->sin_addr, ipstr, INET_ADDRSTRLEN );
			} else { // IPv6
# ifdef INET6_ADDRSTRLEN
				struct sockaddr_in6 *ipv6 = ( struct sockaddr_in6 * )p->ai_addr;
				inet_ntop( p->ai_family, &ipv6->sin6_addr, ipstr, INET6_ADDRSTRLEN );
# else
				dbg_error( "Unsupport IPV6!!!" );
# endif
			}
			res = SUCCESS;
			break;
		}
	}

	freeaddrinfo( servinfo );

	return res;
}

//==========================================================================
// APIs
//==========================================================================
int
Socket_isReadableNonBlock( int fd )
{
	fd_set				fds;
	struct timeval		to;

	FD_ZERO( &fds );
	FD_SET( fd, &fds );

	to.tv_sec	= 0;
	to.tv_usec	= 0;

	return select( ( fd + 1 ), &fds, NULL, NULL, &to );
}

int
Socket_isReadable( int fd )
{
	fd_set				fds;
	struct timeval		to;

	FD_ZERO( &fds );
	FD_SET( fd, &fds );

	to.tv_sec	= 0;
	to.tv_usec	= 100000;

	return select( ( fd + 1 ), &fds, NULL, NULL, &to );
}

int
Socket_isWritable( int fd )
{
	fd_set				fds;
	struct timeval		to;

	FD_ZERO( &fds );
	FD_SET( fd, &fds );

	to.tv_sec	= 0;
	to.tv_usec	= 100000;

	return select( ( fd + 1 ), NULL, &fds, NULL, &to );
}

int
Socket_SetBlockMode( int fd, int block )
{
	if ( fd > 0 ) {
		if ( block ) {
			int opt = fcntl( fd, F_GETFL, 0 );

			if ( opt & O_NONBLOCK ) {
				struct timeval timeo = { 1, 0 };
				if ( setsockopt( fd, SOL_SOCKET, SO_RCVTIMEO, &timeo, sizeof( struct timeval ) ) < 0 ) {
					dbg_line( "setsockopt fail..." );
				}
				fcntl( fd, F_SETFL, ( opt - O_NONBLOCK ) );
			}
		} else {
			fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) );
		}
		return SUCCESS;
	} else {
		return FAILURE;
	}
}

int
Socket_RecvMsg( int fd, struct msghdr *mh )
{
	int size;

	size = recvmsg( fd, mh, 0 );

	if ( size < 0 ) {
		if ( errno != 11 ) {
			return size;
		} else
			return 0;
	}

	return size;
}

int
Socket_Recv( int fd, void *dst, int len )
{
	if ( ( fd <= 0 ) || ( dst == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd <= 0 ) || ( dst == NULL ) || ( len <= 0 )" );
		return FAILURE;
	} else {
		int res = recv( fd, dst, len, MSG_WAITALL );

		if ( res < 0 ) {
			if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				return res;
			} else {
				return 0;
			}
		} else {
			return res;
		}
	}
}
#if 0
int
Socket_SendMsg( int fd, struct msghdr *mh )
{
	int size = 0, i, total_size = 0, res, retry = 0;
	struct iovec	*v = mh->msg_iov;

	for ( i = 0; i < mh->msg_iovlen; i ++ ) {
		total_size += v[ i ].iov_len;
	}

	while ( size < total_size ) {
		res = Socket_isWritable( fd );
		if ( res > 0 ) {
			res = sendmsg( fd, mh, MSG_WAITALL );
			if ( res < 0 ) {
				if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
					return res;
				}
			} else if ( res == 0 ) {
				return 0;
			} else {
				retry	= 0;
				size += res;
			}
		} else if ( res < 0 ) {
			return -1;
		} else {
			retry ++;
			if ( retry > 5 ) {
				return -1;
			}
		}
	}

	return size;
}
#else
int
Socket_SendMsg( int fd, struct msghdr *mh )
{
	int size = 0;

	if ( ( fd <= 0 ) || ( mh == NULL ) ) {
		dbg_error( "( fd <= 0 ) || ( mh == NULL )" );
		return FAILURE;
	} else {
# ifdef MSG_NOSIGNAL
		size = sendmsg( fd, mh, MSG_NOSIGNAL );
# else
		size = sendmsg( fd, mh, 0 );
# endif
		if ( size < 0 ) {
			if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				return size;
			}
		}

		return size;
	}
}
#endif

int
Socket_Send( int fd, void *src, int len )
{
	int res;

	if ( ( fd < 0 ) || ( src == NULL ) || ( len <= 0 ) ) {
		dbg_error( "( fd < 0 ) || ( src == NULL ) || ( len <= 0 )" );
		return -1;
	}

# ifdef MSG_NOSIGNAL
	res = send( fd, src, len, MSG_NOSIGNAL );
# else
	res = send( fd, src, len, 0 );
# endif

	if ( res < 0 ) {
		if ( !( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
			dbg_line( "errno: %s", strerror( errno ) );
			return -1;
		}
		return 0;
	}

	return res;
}

int
Socket_Server( u16 port, int cnt )
{
	struct sockaddr_in addr;
	int opt = 1;
	int fd;

	addr.sin_family			= AF_INET;
	addr.sin_addr.s_addr	= INADDR_ANY;
	addr.sin_port			= htons( port );

	if ( ( fd = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) ) != 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof( opt ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( bind( fd, ( struct sockaddr * )&addr, sizeof( addr ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( listen( fd, cnt ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	fcntl( fd, F_SETFD, FD_CLOEXEC );

	return 0;
}

int
Socket_Bind( int fd, u16 port )
{
	struct sockaddr_in sockaddr;
	int opt = 1;

	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_addr.s_addr	= 0;
	sockaddr.sin_port			= htons( port );

	if ( bind( fd, ( struct sockaddr * )&sockaddr, sizeof( sockaddr ) ) < 0 ) {
		return -1;
	} else if ( setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof( opt ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	return 0;
}

void
Socket_Disconnect( int fd )
{
	close( fd );
}

int
Socket_Connect( char *addr, u16 port, u16 local_port, u32 type )
{
	int fd = 0;
	int on = 1;
	struct sockaddr_in sockaddr;
	socklen_t socklen = sizeof( struct sockaddr_in );
	struct linger m_linger;

# ifdef INET6_ADDRSTRLEN
	char ipstr[ INET6_ADDRSTRLEN ];
# else
	char ipstr[ INET_ADDRSTRLEN ];
# endif

	if ( socket_get_ipaddr( addr, ipstr, AF_INET ) == FAILURE ) {	//  或 AF_INET6
		return FAILURE;
	}

	if ( type == SOCKET_PROTOCOL_TCP ) {
		fd = socket( PF_INET, SOCK_STREAM, 0 );
	} else {
		fd = socket( PF_INET, SOCK_DGRAM, 0 );
	}

	if ( fd < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return FAILURE;
	}

	if ( local_port ) {
		sockaddr.sin_family			= AF_INET;
		sockaddr.sin_addr.s_addr	= 0;
		sockaddr.sin_port			= htons( local_port );

		if ( bind( fd, ( struct sockaddr * )&sockaddr, sizeof( sockaddr ) ) < 0 ) {
			dbg_line( "%s", strerror( errno ) );
			close( fd );
			return FAILURE;
		}
	}

	m_linger.l_onoff	= 1;	//(?�closesocket()调用,但是还�??�据没�??��?毕�??�候容许逗�?)
	m_linger.l_linger	= 5;	//(容许?��??�时?�为5�?

	fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) );

	setsockopt( fd, SOL_SOCKET, SO_KEEPALIVE, ( void * )&on, sizeof( on ) );
	setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, ( void * )&on, sizeof( on ) );

	if ( type == SOCKET_PROTOCOL_UDP ) {
		setsockopt( fd, IPPROTO_TCP, TCP_NODELAY, ( void * )&on, sizeof( on ) );
		setsockopt( fd, SOL_SOCKET, SO_LINGER, &m_linger, sizeof( struct linger ) );
	}

	bzero( &sockaddr, socklen );

	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_port			= htons( port );
	sockaddr.sin_addr.s_addr	= inet_addr( addr );

	if ( sockaddr.sin_addr.s_addr == -1 ) {
		dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	}

	u32 use_time = Ticks();
	connect( fd, ( struct sockaddr * )&sockaddr, socklen );

	if ( getsockname( fd, ( struct sockaddr * )&sockaddr, &socklen ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		if ( Ticks() - use_time > 100 ) {
			dbg_line( "use_time = %u", use_time - Ticks() );
		}
		close( fd );
		return FAILURE;
	}

	if ( Ticks() - use_time > 100 ) {
		dbg_line( "use_time = %u", use_time - Ticks() );
	}

	return fd;
}

int
Socket_OpenHostUDPListenPort( u16 port )
{
	int on = 1;
	struct sockaddr_in sockaddr;
	struct linger m_linger;
	int fd = socket( PF_INET, SOCK_DGRAM, 0 );

	if ( fd <= 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return FAILURE;
	}
	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_addr.s_addr	= 0;
	sockaddr.sin_port			= htons( port );

	if ( bind( fd, ( struct sockaddr * )&sockaddr, sizeof( sockaddr ) ) < 0 ) {
//		dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	} else {
		fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) );

		setsockopt( fd, SOL_SOCKET, SO_KEEPALIVE, ( void * )&on, sizeof( on ) );
		setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, ( void * )&on, sizeof( on ) );
		setsockopt( fd, IPPROTO_TCP, TCP_NODELAY, ( void * )&on, sizeof( on ) );
		setsockopt( fd, SOL_SOCKET, SO_LINGER, &m_linger, sizeof( struct linger ) );
		return fd;
	}
}

int
Socket_ConnectServerUDPPort( int fd, char *addr, u16 port )
{
	struct sockaddr_in sockaddr;
	socklen_t socklen = sizeof( struct sockaddr_in );

	bzero( &sockaddr, socklen );

	sockaddr.sin_family			= AF_INET;
	sockaddr.sin_port			= htons( port );
	sockaddr.sin_addr.s_addr	= inet_addr( addr );

	if ( sockaddr.sin_addr.s_addr == -1 ) {
		dbg_line( "%s", strerror( errno ) );
		close( fd );
		return FAILURE;
	}

	connect( fd, ( struct sockaddr * )&sockaddr, socklen );

	return SUCCESS;
}

int
Socket_GetIPbySocket( int fd, char *addr )
{
	if ( ( fd < 0 ) || ( addr == NULL ) ) {
		return FAILURE;
	} else {
		struct sockaddr_in sockaddr;
		socklen_t socklen = sizeof( struct sockaddr_in );

		if ( getpeername( fd, ( struct sockaddr * )&sockaddr, &socklen ) < 0 ) {
			dbg_line( "%s", strerror( errno ) );
			return FAILURE;
		} else {
			sprintf( addr, "%s", inet_ntoa( sockaddr.sin_addr ) );
			return SUCCESS;
		}
	}
}

int
Socket_Accept( int fd, struct sockaddr_in *client_addr, int allow )
{
	socklen_t addrlen = sizeof( struct sockaddr_in );
	int clientfd;

	clientfd = accept( fd, ( struct sockaddr * )client_addr, &addrlen );

	if ( !allow ) {
		if ( clientfd >= 0 ) {
			close( clientfd );
		}
		return -1;
	}

	fcntl( clientfd, F_SETFL, ( O_NONBLOCK | fcntl( clientfd, F_GETFL, 0 ) ) );
	fcntl( clientfd, F_SETFD, FD_CLOEXEC );

	return clientfd;
}

int
Socket_CreateServer( u16 port )
{
	struct sockaddr_in addr;
	int opt = 1;
	int fd;

	addr.sin_family			= AF_INET;
	addr.sin_addr.s_addr	= INADDR_ANY;
	addr.sin_port			= htons( port );

	if ( ( fd = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( fcntl( fd, F_SETFL, ( O_NONBLOCK | fcntl( fd, F_GETFL, 0 ) ) ) != 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( setsockopt( fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof( opt ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( bind( fd, ( struct sockaddr * )&addr, sizeof( addr ) ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	} else if ( listen( fd, 1024 ) < 0 ) {
		dbg_line( "%s", strerror( errno ) );
		return -1;
	}

	fcntl( fd, F_SETFD, FD_CLOEXEC );

	return fd;
}
