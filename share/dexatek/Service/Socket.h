/*!
 *  @file       Socket.h
 *  @version    1.00
 *  @author     Sam Tsai
 */

# ifndef __SOCKET_H__
# define __SOCKET_H__

//==========================================================================
# include "Headers.h"

//==========================================================================

//==========================================================================
enum {
	SOCKET_PROTOCOL_TCP = 0,
	SOCKET_PROTOCOL_UDP,
};

//==========================================================================
// API
//==========================================================================
extern int
Socket_isReadableNonBlock( int fd );

extern int
Socket_isReadable( int fd );

extern int
Socket_isWritable( int fd );

extern int
Socket_SetBlockMode( int fd, int block );

extern void
Socket_Disconnect( int fd );

extern int
Socket_RecvMsg( int fd, struct msghdr *mh );

extern int
Socket_Recv( int fd, void *dst, int len );

extern int
Socket_SendMsg( int fd, struct msghdr *mh );

extern int
Socket_Send( int fd, void *src, int len );

extern int
Socket_Server( u16 port, int cnt );

extern int
Socket_Bind( int fd, u16 port );

extern int
Socket_Connect( char *addr, u16 port, u16 host_port, u32 type );

extern int
Socket_OpenHostUDPListenPort( u16 port );

extern int
Socket_ConnectServerUDPPort( int fd, char *addr, u16 port );

extern int
Socket_GetIPbySocket( int fd, char *addr );

extern int
Socket_Accept( int fd, struct sockaddr_in *client_addr, int allow );

extern int
Socket_CreateServer( u16 port );

//==========================================================================

# endif // __SOCKET_H__



