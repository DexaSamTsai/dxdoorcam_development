/*!
 *  @file       Auth.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include Files
//==========================================================================
# include "Auth.h"
# include "Utils/MD5.h"

//==========================================================================
// Definetion
//==========================================================================
struct auth_info_t {
	unsigned int	used;
	unsigned int	level;
	char			base64[ 128 ];
	char			username[ 64 ];
	char			password[ 64 ];
} *auth_info = NULL;

//==========================================================================
// Static Param
//==========================================================================
static int max_auth = 0;

//==========================================================================
// Static Functions
//==========================================================================
static void
random_bytes( u8 *dest, int len )
{
	int i;

	for( i = 0; i < len; i ++ ) {
		dest[ i ] = ( random() & 0xff );
	}
}

static inline char *
digest_parse_field( char *src, char *target )
{
	char *src_s = strstr( src, target );
	char *src_e, *dst = NULL;

	if ( src_s ) {
		src_s = src_s + strlen( target );
		if ( *src_s == '"' ) {
			src_s ++;
			src_e = strstr( src_s, "\"" );
			if ( src_e == NULL ) {
				dbg_warm( "src_e == NULL" );
				return NULL;
			}
		} else {
			src_e = strstr( src_s, "," );
			if ( src_e == NULL ) {
				dbg_warm( "src_e == NULL" );
				src_e = strstr( src_s, "\n" );
				if ( src_e == NULL ) {
					dbg_warm( "src_e == NULL" );
					return NULL;
				}
			}
		}

		dst = ( char * )my_malloc( ( src_e - src_s ) + 1 );
		if ( dst ) {
			memcpy( dst, src_s, ( src_e - src_s ) );
			return dst;
		} else {
			return NULL;
		}
	} else {
		return NULL;
	}
}

//==========================================================================
// APIs
//==========================================================================
int
auth_digest_param( struct digest *dig, char *data )
{
	if ( dig && data ) {
		dig->nonce	= digest_parse_field( data, "nonce=" );
		dig->qop	= digest_parse_field( data, "qop=" );
		dig->realm	= digest_parse_field( data, "realm=" );
		if ( dig->realm == NULL ) {
			dbg_line( "dig->realm == NULL( %s )", data );
		}

		return 0;
	} else {
		return -1;
	}
}

int
auth_digest_parser( struct digest *dig, char *data )
{
	if ( dig && data ) {
		char *nc;

		dig->realm	= digest_parse_field( data, "realm=" );
		dig->user	= digest_parse_field( data, "username=" );
		dig->nonce	= digest_parse_field( data, "nonce=" );
		dig->resp	= digest_parse_field( data, "response=" );
		dig->uri	= digest_parse_field( data, "uri=" );
		dig->qop	= digest_parse_field( data, "qop=" );
		dig->cnonce	= digest_parse_field( data, "cnonce=" );

		nc			= digest_parse_field( data, "nc=" );

		if ( nc ) {
        	dig->nc		= strtol( nc, NULL, 16 );
        } else {
        	dig->nc		= 0;
        }

		return 0;
	} else {
		return -1;
	}
}

int
auth_basic_make( char *username, char *password, char *result )
{
	char base64[ 128 ], auth[ 128 ], *dst = result;

	if ( ( username == NULL ) ||
		 ( password == NULL ) ||
		 ( result == NULL ) ) {
		return -1;
	}

	sprintf( auth, "%s:%s", username, password );
	Encode64( ( u8 * )base64, 128, auth );

	dst += sprintf( dst, "Authorization: Basic %s\r\n", base64 );
	return ( dst - result );
}

int
auth_basic_check( char *key, char *username )
{
	if ( auth_info && key ) {
		int i;

		for ( i = 0; i < max_auth; i ++ ) {
			if ( auth_info[ i ].used ) {
				if ( strcmp( key, auth_info[ i ].base64 ) == 0 ) {
					if ( username ) {
						memcpy( username, auth_info[ i ].username, 64 );
					}
					return 0;
				}
			}
		}
	}

	return -1;
}

void
auth_free_digest( struct digest *dig )
{
#define FREENULL( p ) { if ( p ) { my_free( p ); p = NULL; } }

	if ( dig ) {
		FREENULL( dig->realm );
		FREENULL( dig->nonce );
		FREENULL( dig->qop );
		FREENULL( dig->algorithm );
		FREENULL( dig->stale );
		FREENULL( dig->opaque );
		FREENULL( dig->domain );

		FREENULL( dig->user );
		FREENULL( dig->uri );
		FREENULL( dig->cnonce );
		FREENULL( dig->resp );
	}
}

int
auth_digest_make( struct digest	*dig, char *method, char *uri, char *username, char *password, char *result )
{
	if ( ( dig == NULL ) ||
		 ( method == NULL ) ||
		 ( uri == NULL ) ||
		 ( username == NULL ) ||
		 ( password == NULL ) ||
		 ( result == NULL ) ) {
		return -1;
	} else {
		char a1[ 36 ], a2[ 36 ], *dst = result;

		if ( dig->realm == NULL ) {
			dbg_line( "Digest Authentication: Mandatory 'realm' value not available" );
			return -1;;
		}

		dig->nc = 1;

		if ( dig->uri ) {
			dig->uri = ( char * )my_realloc( dig->uri, ( strlen( uri ) + 4 ) & 0xFFFFFFFC );
		} else {
			dig->uri = ( char * )my_malloc( ( strlen( uri ) + 4 ) & 0xFFFFFFFC );
		}
		sprintf( dig->uri, uri );

		MyMD5( a1, username, ":", dig->realm, ":", password, NULL );

		if ( dig->qop && strcmp( dig->qop, "auth-int" ) == 0 ) {
			char ent[ 36 ];
			memset( ent, 0, 36 );
			MyMD5( ent, NULL );
			MyMD5( a2, method, ":", uri, ":", ent, NULL );
		} else {
			MyMD5( a2, method, ":", uri, NULL );
		}

		if ( dig->resp == NULL ) {
			dig->resp = ( char * )my_malloc( 36 );
			memset( dig->resp, 0, 36 );
		}

		if ( dig->qop &&
			 ( strcmp( dig->qop, "auth" ) == 0 ||
			   strcmp( dig->qop, "auth-int" ) == 0 ) )
		{
			char nc[ 12 ], rnd[ 36 ];
			sprintf( nc, "%08x", dig->nc );

			if ( dig->cnonce == NULL ) {
				dig->cnonce = ( char * )my_malloc( 36 );
			}

			random_bytes( ( u8 * )rnd, 32 );
			rnd[ 32 ] = 0;
			MyMD5( dig->cnonce, rnd, NULL );

			MyMD5( dig->resp, a1, ":", dig->nonce, ":", nc, ":", dig->cnonce, ":", dig->qop, ":", a2, NULL );
		} else {
			MyMD5( dig->resp, a1, ":", dig->nonce, ":", a2, NULL );
		}

		/* Mandatory parameters */
        dst += sprintf( dst,	"Authorization: Digest "
								"username=\"%s\", "
								"realm=\"%s\", "
								"nonce=\"%s\", "
								"uri=\"%s\", "
								"response=\"%s\"",
								username,
								dig->realm,
								dig->nonce,
								uri,
								dig->resp );

		if ( dig->algorithm ) {
			dst += sprintf( dst,	", algorithm=\"%s\"", dig->algorithm );
		}

		if ( dig->opaque ) {
			dst += sprintf( dst,	", opaque=\"%s\"", dig->opaque );
		}

		if ( dig->qop ) {
			dst += sprintf( dst,	", qop=\"%s\"", dig->qop );

			if ( dig->cnonce ) {
				dst += sprintf( dst,	", cnonce=\"%s\"", dig->cnonce );
			}

			if ( dig->nc ) {
				dst += sprintf( dst,	", nc=\"%08x\"", dig->nc );
			} else {
				dst += sprintf( dst,	", uglyhack=\"%08x\"", dig->nc );
			}
		}

		dst += sprintf( dst, "\r\n" );

		return ( dst - result );
	}
}

int
auth_digest_check( struct digest *dig, char *method, char *username )
{
	if ( auth_info ) {
		char a1[ 36 ], a2[ 36 ], resp[ 36 ], nc[ 12 ];
		int i;

		if ( dig->realm == NULL || dig->resp == NULL ) {
			dbg_line( "dig->realm == NULL || dig->resp == NULL" );
			return -1;
		}

		for ( i = 0; i < max_auth; i ++ ) {
			if ( auth_info[ i ].used ) {
				sprintf( nc, "%08x", dig->nc );

				MyMD5( a1, auth_info[ i ].username, ":", dig->realm, ":", auth_info[ i ].password, NULL );
				MyMD5( a2, method, ":", dig->uri, NULL );
				MyMD5( resp, a1, ":", dig->nonce, ":", nc, ":", dig->cnonce, ":", dig->qop, ":", a2, NULL );

				if ( strcmp( resp, dig->resp ) == 0 ) {
					if ( username ) {
						memcpy( username, auth_info[ i ].username, 64 );
					}
					return 0;
				}
			}
		}
	}

	return -1;
}


