/*!
 *  @file       HTTPd.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __HTTP_D_H__
# define __HTTP_D_H__

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Media Frame Type Declaration
//==========================================================================

//==========================================================================
// Error Type Definition
//==========================================================================

//==========================================================================
// Structure
//==========================================================================
# define MAX_RTSP_URI_LENGTH					256

typedef enum {
	WEB_AUTH_TYPE_NONE		= 0,
	WEB_AUTH_TYPE_DIGEST	= 0x01,
	WEB_AUTH_TYPE_BASE64	= 0x02,
	WEB_AUTH_TYPE_MAX,
} WEB_AUTH_TYPE;

//==========================================================================
// Type Define
//==========================================================================
# define NET_ERR_DISCONNECT					-1
# define NET_ERR_CONNECT_FAIL				-2
# define NET_ERR_CONNECT_TIMEOUT			-3
# define NET_ERR_SEND_DATA_FAIL				-4
# define NET_ERR_RECV_DATA_FAIL				-5
# define NET_ERR_DISCONNECT_FROM_REMOTE		-6
# define NET_ERR_STREAM_PARSE_ERROR			-7
# define NET_ERR_RECV_TIMEOUT				-8
# define NET_ERR_IGNORE						-9
# define NET_ERR_UNSUPPORT					-10
# define NET_ERR_HANDSHAKING_ERROR			-11

# define NET_ERR_NO_RESOURCE				-51
# define NET_ERR_INDEX_ERROR				-52
# define NET_ERR_HTTP_PARSER_FAIL			-53
# define NET_ERR_MAGIC_NUM_ERROR			-54

# define NET_ERR_BAD_REQUEST				-400
# define NET_ERR_UNAUTHORIZED				-401
# define NET_ERR_NOT_FOUND					-404
# define NET_ERR_UNSUPPORTED_MEDIA_TYPE		-415
# define NET_ERR_UNSUPPORTED_TRANSPORT		-461
# define NET_ERR_SERVICE_UNAVAILABLE		-503

# define NET_DISCONNECT						-600

//==========================================================================
// Httpd
//==========================================================================
typedef void *					HttpdHandle;

typedef struct {
	char			server_name[ 128 ];
	char			root[ 128 ];
	char			homepage[ 128 ];
	u16				http_port;
	u16				https_port;
	u32				auth_type;
} Httpd_ServerContext;

typedef struct {
	char	url[ MAX_NAME_LENGTH + 4 ];
	void	*cgi_handle;
	int		auth_enabled;

	int		( *cgi_get )			( HttpdHandle, int );									// httpd_handle, fd
	int		( *cgi_post )			( HttpdHandle, int );									// httpd_handle, fd
} Httpd_CGIContext;

typedef enum {
	WEB_VIRTUAL_FILE_STATUS_NONE = 0,
	WEB_VIRTUAL_FILE_STATUS_DATA_IN,
	WEB_VIRTUAL_FILE_STATUS_CONN,
	WEB_VIRTUAL_FILE_STATUS_DISCONN,
	WEB_VIRTUAL_FILE_STATUS_MAX,
} WEB_VIRTUAL_FILE_STATUS;

typedef int ( *Webs_GetVFile )			( void * );					// fd
typedef int ( *Webs_PutVFile )			( void *, u8 *, int );		// fd, data, size
typedef int ( *Webs_CloseVFile )		( void * );					// fd

typedef struct {
	char				filename[ 128 ];

	Webs_GetVFile		get;
	Webs_CloseVFile		get_close;
	Webs_PutVFile		post;
	Webs_CloseVFile		post_close;

	int					auth_enabled;
} web_vfile_t;

typedef int ( *HTTP_RESULT ) 		( void *, int, int );		// Custom Data, status, size,
typedef struct {
	char			*ip;
	u16				port;
	char			*uri;
	char			*host;

	char			*username;
	char			*password;

	void			*custom;

	u8				*data;
	int				data_len;

	u8				*post_data;
	int				post_data_len;

	HTTP_RESULT		result;
} web_http_t;

typedef struct {
	char	*field;
	char	*value;
} web_param_t;

//==========================================================================
// APIs
//==========================================================================
extern const char *
Httpd_GetHeaderField( HttpdHandle handle, char *field );

extern const char *
Httpd_GetCGIField( HttpdHandle handle, char *field );

extern int
Httpd_SendHttpHeader( HttpdHandle handle, char *url, int code );

extern int
Httpd_SendResponse( HttpdHandle handle, char *url, char *data, int size );

extern int
Httpd_RecvHttpHeader( HttpdHandle handle );

extern int
Httpd_RecvData( HttpdHandle handle, u8 **data );

extern int
Httpd_RegCGIService( Httpd_CGIContext *context );

extern int
Httpd_SetContext( Httpd_ServerContext *context );

extern int
Httpd_AddLocation( char *location );

extern int
Httpd_ServerStart( Httpd_ServerContext *context );

# endif // __HTTP_D_H__

