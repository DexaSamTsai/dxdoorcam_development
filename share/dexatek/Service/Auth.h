/*!
 *  @file       Auth.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __AUTH_H__
# define __AUTH_H__
//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

//==========================================================================
// Definetion
//==========================================================================
struct digest
{
	char		*realm;
	char		*nonce;
	char		*qop;
	char		*algorithm;
	char		*stale;
	char		*opaque;
	char		*domain;

	char		*user;
	char		*uri;
	char		*cnonce;
	char		*resp;
	u32			nc;
};

//==========================================================================
// Internal APIs
//==========================================================================
extern void
auth_free_digest( struct digest *dig );

extern int
auth_digest_param( struct digest *dig, char *data );

extern int
auth_digest_parser( struct digest *dig, char *data );

extern int
auth_digest_make( struct digest *dig, char *method, char *uri, char *username, char *password, char *result );

extern int
auth_digest_check( struct digest *dig, char *method, char *username );

extern int
auth_basic_make( char *username, char *password, char *result );

extern int
auth_basic_check( char *key, char *username );

# endif // __AUTH_H__
