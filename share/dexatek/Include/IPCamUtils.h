/*!
 *  @file       Utils.h
 *  @version    1.0
 *  @author     Sam Tsai
 */

# ifndef __IPCAM_UTILS_H__
# define __IPCAM_UTILS_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

# include "Json.h"
# include "Utils/Malloc.h"
# include "Utils/Base64.h"
# include "Utils/CRC32.h"
# include "Utils/DateTime.h"
# include "Utils/Thread.h"

//==========================================================================
// Inline Functions
//==========================================================================
static inline int
str16to8( u16 *src, u8 *dst )
{
    int cnt = 0;

    if ( ( src == NULL ) || ( dst == NULL ) ) {
        return 0;
    }

    while ( src[ cnt ] ) {
        dst[ cnt ] = ( u8 )src[ cnt ];
        cnt ++;
    }

    dst[ cnt ] = 0;
    return cnt;
}

static inline int
str8to16( u8 *src, u16 *dst )
{
    int cnt = 0;

    if ( src == NULL || dst == NULL )
        return 0;

    while ( src[ cnt ] ) {
        dst[ cnt ] = src[ cnt ];
        cnt ++;
    }

    dst[ cnt ] = 0;
    return cnt;
}

static inline int
strcpy16( u16 *src, u16 *dst )
{
    int cnt = 0;

    if ( src == NULL || dst == NULL ) {
        return 0;
    }

    while ( src[ cnt ] ) {
        dst[ cnt ] = src[ cnt ];
        cnt ++;
    }

    dst[ cnt ] = 0;
    return cnt;
}

static inline int
str_parse_mac( char *s8_mac, u8 *u8_mac )
{
	int		i;
	char	str[ 6 ][ 3 ];

	memset( u8_mac, 0, 6 );

	if ( sscanf( s8_mac, "%2[a-fA-F0-9]:%2[a-fA-F0-9]:%2[a-fA-F0-9]:%2[a-fA-F0-9]:%2[a-fA-F0-9]:%2[a-fA-F0-9]", str[ 0 ], str[ 1 ], str[ 2 ], str[ 3 ], str[ 4 ], str[ 5 ] ) != 6 ) {
		if ( sscanf( s8_mac, "%2[a-fA-F0-9]-%2[a-fA-F0-9]-%2[a-fA-F0-9]-%2[a-fA-F0-9]-%2[a-fA-F0-9]-%2[a-fA-F0-9]", str[ 0 ], str[ 1 ], str[ 2 ], str[ 3 ], str[ 4 ], str[ 5 ] ) != 6 ) {
			if ( sscanf( s8_mac, "%2[a-fA-F0-9]%2[a-fA-F0-9]%2[a-fA-F0-9]%2[a-fA-F0-9]%2[a-fA-F0-9]%2[a-fA-F0-9]", str[ 0 ], str[ 1 ], str[ 2 ], str[ 3 ], str[ 4 ], str[ 5 ] ) != 6 ) {
				return -1;
			}
		}
	}

	for ( i = 0; i < 6; i ++ ) {
		u8_mac[ i ] = ( u8 ) strtol( str[ i ], NULL, 16 );
	}

	return 0;
}

static inline int
str_parse_ip( char *s8_ip, u8 *u8_ip )
{
	int		i;
	char	str[ 4 ][ 4 ];

	memset( u8_ip, 0, 4 );

	if ( sscanf( s8_ip, "%3[0-9].%3[0-9].%3[0-9].%3[0-9]", str[ 0 ], str[ 1 ], str[ 2 ], str[ 3 ] ) != 4 ) {
		return -1;
	}

	for ( i = 0; i < 4; i ++ ) {
		u8_ip[ i ] = atoi( str[ i ] );
	}

	return 0;
}

static inline u32
ipstr2u32( char *str_ip )
{
	u8		ip[ 4 ];

	if ( str_parse_ip( str_ip, ip )  ) {
		return 0;
	} else {
		return ( ( ip[ 0 ] << 24 ) & 0xff000000 ) | ( ( ip[ 1 ] << 16 ) & 0xff0000 ) | ( ( ip[ 2 ] << 8 ) & 0xff00 ) | ( ip[ 3 ] & 0xff );
	}
}

static inline int
make_ip_str( u32 u32_ip, char *str_ip )
{
	if ( u32_ip && str_ip ) {
		return sprintf( str_ip, "%d.%d.%d.%d",	( u32_ip & 0xFF000000 ) >> 24,
												( u32_ip & 0x00FF0000 ) >> 16,
												( u32_ip & 0x0000FF00 ) >> 8,
												( u32_ip & 0x000000FF ) );
	} else {
		return 0;
	}
}

static inline int
make_mac_str( u8 *mac, char *str_mac )
{
	if ( mac && str_mac ) {
		return sprintf( str_mac, "%02X:%02X:%02X:%02X:%02X:%02X",	mac[ 0 ], mac[ 1 ], mac[ 2 ],
																	mac[ 3 ], mac[ 4 ], mac[ 5 ] );
	} else {
		return 0;
	}
}

static void
utf8_to_unicode( u8 *utf8, u16 *unicode, int size )
{
	u32		temp;
	int		cnt = 0;

#define IS_UTF8( x )  ( x >= 0x80 && x <= 0xBF )

	while ( utf8[ 0 ] && ( cnt + 1 < size ) ) {
		if ( utf8[ 0 ] >= 0xF0 ) {			// Bigger then 0x0000FFFF
			unicode[ cnt ++ ] = utf8[ 0 ];
			utf8 ++;
		} else if ( utf8[ 0 ] >= 0xE0 ) {
			temp = utf8[ 0 ];
			utf8 ++;

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			unicode[ cnt ++ ] = ( ( temp & 0x000F0000 ) >> 4 ) | ( ( temp & 0x00003F00 ) >> 2 ) | ( temp & 0x3F );
		} else if ( utf8[ 0 ] >= 0xC0 ) {
			temp = utf8[ 0 ];
			utf8 ++;

			if ( IS_UTF8( utf8[ 0 ] ) ) {
				temp = ( temp << 8 ) | utf8[ 0 ];
				utf8 ++;
			}

			unicode[ cnt ++ ] = ( ( temp & 0x00001F00 ) >> 2 ) | ( temp & 0x3F );
		} else {
			unicode[ cnt ++ ] = utf8[ 0 ];
			utf8 ++;
		}
	}
	unicode[ cnt ++ ] = 0;
}

static int
unicode_to_utf8( u16 *unicode, char *utf8, int size )
{
	u32		temp;
	char	*u8_dst = utf8;
	int		cnt = 0;

	while ( unicode[ 0 ] && ( size > 1 ) ) {
		cnt = 0;

		if ( unicode[ 0 ] == 0x20 ) {			// Bigger then 0x0000FFFF
			u8_dst[ cnt ++ ] = unicode[ 0 ];
		} else if ( unicode[ 0 ] < 0x80 ) {
			u8_dst[ cnt ++ ] = unicode[ 0 ];
		} else if ( unicode[ 0 ] < 0x800 ) {
			temp = ( ( 0xC0 | ( unicode[ 0 ] >> 6 ) ) << 8 ) | ( 0x80 | ( unicode[ 0 ] & 0x3F ) );

			u8_dst[ cnt ++ ] = ( temp >> 8 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp & 0xFF );
		} else {
			temp = ( ( 0xE0 | ( unicode[ 0 ] >> 12 ) ) << 16 ) | ( ( 0x80 | ( ( unicode[ 0 ] >> 6 ) & 0x3F ) ) << 8 ) | ( 0x80 | ( unicode[ 0 ] & 0x3F ) );

			u8_dst[ cnt ++ ] = ( temp >> 16 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp >> 8 ) & 0xFF;
			u8_dst[ cnt ++ ] = ( temp & 0xFF );
		}

		u8_dst += cnt;
		size -= cnt;
		unicode ++;
	}
	u8_dst[ 0 ] = 0;

	return ( u8_dst - utf8 );
}

static void
char_to_utf8( char *code, char *utf8, int size )
{
	int		cnt = 0;

	if ( code == NULL || utf8 == NULL || size == 0 ) {
		dbg_line( "code = %p, utf8 = %p, size = %d", code, utf8, size );
		return;
	}

	while ( code[ 0 ] && ( size > 1 ) ) {
		if ( ( code[ 0 ] >= 'A' && code[ 0 ] <= 'Z' ) ||
			 ( code[ 0 ] >= 'a' && code[ 0 ] <= 'z' ) ||
			 ( code[ 0 ] >= '0' && code[ 0 ] <= '9' ) ||
			 ( code[ 0 ] == '-' ) ||
			 ( code[ 0 ] == '_' ) ||
			 ( code[ 0 ] == '.' ) ||
			 ( code[ 0 ] == '~' ) ) {			// Bigger then 0x0000FFFF
			cnt = snprintf( utf8, size, "%c", ( u8 )code[ 0 ] );
		} else if ( code[ 0 ] == ' ' ) {
			cnt = snprintf( utf8, size, "+" );
		} else {
			cnt = snprintf( utf8, size, "%%%02X", code[ 0 ] );
		}

		utf8 += cnt;
		size -= cnt;
		code ++;
	}
	utf8[ 0 ] = 0;
}

static void
system( char *cmd )
{

}

//==========================================================================
// APIs
//==========================================================================
/*!
 * @brief   Get Free Memory Size
 * @return  Free memory size
 */
extern u32
sys_avail( void );

extern void
Dump( void *data, u32 size );

extern void
my_toupper( char *string );

extern void
my_tolower( char *string );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif // __IPCAM_UTILS_H__

