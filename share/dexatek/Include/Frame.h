/*!
 *  @file       frame.h
 *  @version    1.0
 *  @author     Sam Tsai
 */

# ifndef __FRAME_H__
# define __FRAME_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

// ----------------------------------------------------------------------------
// Constant Definition
// ----------------------------------------------------------------------------
/*! @defgroup type_defs Type Define */
// @{
# define FRAME_VIDEO					0x00010000
# define FRAME_VIDEO_MPEG4				0x00010000
# define FRAME_VIDEO_MPEG4_I			0x00010001
# define FRAME_VIDEO_MPEG4_P			0x00010002

# define FRAME_VIDEO_H264				0x00010100
# define FRAME_VIDEO_H264_SPS			0x00010101
# define FRAME_VIDEO_H264_PPS			0x00010102
# define FRAME_VIDEO_H264_I				0x00010103
# define FRAME_VIDEO_H264_P				0x00010104

# define FRAME_VIDEO_MJPEG				0x00010200

# define FRAME_OTHER					0x00030000			// Metadata that User Defined
# define FRAME_DATA						0x00030300
# define FRAME_DATA_INFO				0x00030301
# define FRAME_DATA_META				0x00030302

# define FRAME_AUDIO					0x00020000
# define FRAME_AUDIO_PCMU				0x00020400
# define FRAME_AUDIO_GSM				0x00020403
# define FRAME_AUDIO_G723				0x00020404
# define FRAME_AUDIO_DVI4				0x00020405
# define FRAME_AUDIO_DVI4_16			0x00020406
# define FRAME_AUDIO_PCMA				0x00020408
# define FRAME_AUDIO_ADPCM				0x00020409
# define FRAME_AUDIO_AAC				0x0002040A
# define FRAME_AUDIO_PCM				0x0002040B


# define FRAME_MASK						0xFFFFFF00
# define FRAME_AV_MASK					0xFFFF0000
# define FRAME_UNCOMPLETED				0xFFFFFFFF

enum {
	SOURCE_VIDEO = 0,
	SOURCE_AUDIO,
	SOURCE_META,
	SOURCE_2WAY_AUDIO,
	SOURCE_MAX,
};

typedef enum {
	RESOLUTION_2688X1512	= 0,
	RESOLUTION_2560X1440,
	RESOLUTION_2304x1296,
	RESOLUTION_1920X1080,
	RESOLUTION_1440x900,
	RESOLUTION_1280x1024,
	RESOLUTION_1280x960,
	RESOLUTION_1280x800,
	RESOLUTION_1280x720,
	RESOLUTION_1024x768,
	RESOLUTION_1024x640,
	RESOLUTION_960x720,
	RESOLUTION_800x600,
	RESOLUTION_800x592,
	RESOLUTION_800x500,
	RESOLUTION_800x450,
	RESOLUTION_768x576,
	RESOLUTION_720x480,
	RESOLUTION_704x576,
	RESOLUTION_704x480,
	RESOLUTION_640x480,
	RESOLUTION_640x400,
	RESOLUTION_640x360,
	RESOLUTION_480x360,
	RESOLUTION_480x352,
	RESOLUTION_480x300,
	RESOLUTION_480x270,
	RESOLUTION_384x288,
	RESOLUTION_352x288,
	RESOLUTION_352x240,
	RESOLUTION_320x240,
	RESOLUTION_320x200,
	RESOLUTION_320x180,
	RESOLUTION_240x180,
	RESOLUTION_192x144,
	RESOLUTION_176x144,
	RESOLUTION_176x120,
	RESOLUTION_160x120,
	RESOLUTION_160x100,
	RESOLUTION_160x90,
	RESOLUTION_80x50,
	RESOLUTION_MAX,
} RESOLUTION_SUPPORT_LIST;

# define FRAME_HEADER_SIZE_MAX			128

typedef struct av_frame_t		AVFrame;
typedef struct av_frame_t		av_frame_t;

struct av_frame_t {
	u8				*buff;
	u32				size;
	u32				type;
	u32				iskey;
	struct timeval	ptime;
};

// ----------------------------------------------------------------------------
// Type Declaration
// ----------------------------------------------------------------------------

// @}

# ifdef __cplusplus
}
# endif // __cplusplus

# endif //__FRAME_H__
