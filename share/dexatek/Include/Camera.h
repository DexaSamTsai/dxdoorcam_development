/*
 * camera.h
 *
 *  Created on: 2017¦~7¤ë24¤é
 *      Author: Sam Tsai
 */

# ifndef CAMERA_CAMERA_H_
# define CAMERA_CAMERA_H_

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Frame.h"

//==========================================================================
// Type Declaration
//==========================================================================
# define	CAMERA_MD_MAX					3

# define	CAMERA_MD_SENSITIVITY			50
# define	CAMERA_MD_PERCENTAGE_LOW		20
# define	CAMERA_MD_PERCENTAGE_MIDDLE		7
# define	CAMERA_MD_PERCENTAGE_HIGH		1
# define	CAMERA_MD_AREA_WIDTH_MAX		100
# define	CAMERA_MD_AREA_HEIGHT_MAX		100

# define	CAMERA_MASK_MAX					5
# define	CAMERA_MASK_WIDTH_LIMIT			500
# define	CAMERA_MASK_HEIGHT_LIMIT		500

typedef enum {
	CAMERA_BITRATECTRL_VBR		= 0,
	CAMERA_BITRATECTRL_CBR,
	CAMERA_BITRATECTRL_CVBR,
	CAMERA_BITRATECTRL_MAX,
} CAMERA_BITRATECTRL;

typedef enum {
	CAMERA_PROFILE_BASELINE		= 0,
	CAMERA_PROFILE_MAIN,
	CAMERA_PROFILE_HIGH,
	CAMERA_PROFILE_MAX,
} CAMERA_PROFILE;

typedef enum {
	CAMERA_SWITCH_OFF		= 0,
	CAMERA_SWITCH_ON,
	CAMERA_SWITCH_MAX,
} CAMERA_SWITCH;

typedef enum {
	CAMERA_CTRL_TYPE_AUTO		= 0,
	CAMERA_CTRL_TYPE_MANUAL,
	CAMERA_CTRL_TYPE_MAX,
} CAMERA_CTRL_TYPE;

typedef enum {
	CAMERA_AWB_MODE_TEMPERATURE		= 0,
	CAMERA_AWB_MODE_AUTO,
	CAMERA_AWB_MODE_COMPONENT,
	CAMERA_AWB_MODE_MAX,
} CAMERA_AWB_MODE;

typedef enum {
	CAMERA_AE_MODE_AUTO		= 0,
	CAMERA_AE_MODE_MANUAL,
	CAMERA_AE_MODE_MAX,
} CAMERA_AE_MODE;

typedef enum {
    CAMERA_POWER_FREQ_OFF		= 0,
    CAMERA_POWER_FREQ_50HZ,
    CAMERA_POWER_FREQ_60HZ,
    CAMERA_POWER_FREQ_MAX,
} CAMERA_POWER_FREQ_MODE;

typedef enum {
	CAMERA_IRCUT_MODE_OFF		= 0,
	CAMERA_IRCUT_MODE_AUTO,
	CAMERA_IRCUT_MODE_MANUAL,
	CAMERA_IRCUT_MODE_MAX,
} CAMERA_IRCUT_MODE;

typedef enum {
	CAMERA_STREAM_VIDEO_H264_1	= 0,
	CAMERA_STREAM_VIDEO_H264_2,
	CAMERA_STREAM_VIDEO_MJPEG,
	CAMERA_STREAM_AUDIO,
	CAMERA_STREAM_MAX,
	//CAMERA_STREAM_VIDEO_MAX	= CAMERA_STREAM_VIDEO_MJPEG,
	CAMERA_STREAM_VIDEO_MAX	= CAMERA_STREAM_MAX,
} CAMERA_STREAM;

# if 1
#		define CAMERA_STREAM_RECORD			CAMERA_STREAM_VIDEO_H264_2
#		define CAMERA_STREAM_LIVE			CAMERA_STREAM_VIDEO_H264_1
# else
#		define CAMERA_STREAM_RECORD			CAMERA_STREAM_VIDEO_H264_1
#		define CAMERA_STREAM_LIVE			CAMERA_STREAM_VIDEO_H264_1
# endif

# define CAMERA_STREAM_SNAPSHOT				CAMERA_STREAM_VIDEO_MJPEG

typedef struct {
	u16					width;
	u16					height;
	u16					fps;
	u16					kbps;
} CodecCtx;

typedef struct {
	int					brightness;
	int					contrast;
	int					hue;
	int					saturation;
	int					sharpness;
	int					gamma;

	u32					blc;
	u32					gain;
	int					power_freq;

	u32					roll;
	u32					flip;
	u32					mirror;
	u32					rotate;

	int					roi;

	u32					gray_mode;

	struct {
		u32				mode;
		u32				level;
		u32				r_gain;
		u32				g_gain;
		u32				b_gain;
	} awb;

	struct {
		u32				mode;
		u32				level;
	} wdr;

	struct {
		u32				mode;
		u32				gain;
		u32				exposure_time;
	} ae;

	u32					dnr3;
	u32					dehaze;
	u32					ir_mode;
} ImageCtx;

typedef struct {
	int		enable;
	int		x;
	int		y;
	int		width;
	int		height;
	int		sensitivity;
	int		percentage;
} MDCtx;

typedef struct {
	int		enable;
	int		x;
	int		y;
	int		width;
	int		height;
} MaskCtx;

typedef struct {
	char	show_date;
	char	show_time;
	char	show_string;
	u8		*string;
	u32		len;
} OsdCtx;

typedef struct {
	u32		sampling_freq;
	u16		channels;
	u16		sample;
	u32		type;
} AudioConf;

typedef int 					( *MD_CBFunc )		( u32, void *, int );		// MD Index, MD Data, Data Size
typedef int 					( *Stream_CBFunc )	( u32, AVFrame * );			// Stream Index, Frame
typedef int 					( *Audio_CBFunc )	( u32 );					// Audio DB

//==========================================================================
// APIs
//==========================================================================
extern int
Camera_ShowOSD( OsdCtx *ctx );

extern int
Camera_SetMask( u32 id, MaskCtx *ctx );

extern int
Camera_SetMDCBFunc( MD_CBFunc cb );

extern int
Camera_SetMD( u32 id, MDCtx *ctx );

extern int
Camera_SetImage( ImageCtx *ctx, char force );

extern int
Camera_GetImage( ImageCtx *ctx );

extern int
Camera_SetVideoCodec( CAMERA_STREAM stream, CodecCtx *ctx );

extern int
Camera_SetVideoBitrate( CAMERA_STREAM stream, u32 bitrate );

extern int
Camera_SetAudioCBFunc( Audio_CBFunc cb );

extern int
Camera_EnableAudio( char on );

extern int
Camera_GetAudioInConf( AudioConf *conf );

extern int
Camera_SetAudioInVolume( u32 volume );

extern int
Camera_SetAudioOutVolume( u32 volume );

extern int
Camera_OpenAudioOut( void );

extern int
Camera_CloseAudioOut( void );

extern int
Camera_PlayAudio( u8 *data, u32 size );

extern int
Camera_SetStreamCBFunc( Stream_CBFunc cb );

extern int
Camera_Init( void );

extern void
Camera_Deinit( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif /* CAMERA_CAMERA_H_ */
