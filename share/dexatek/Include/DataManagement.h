/*!
 *  @file       DataManagement.h
 *  @version    1.0
 *  @author     Sam Tsai
 */

# ifndef __DATA_MANAGEMENT_H__
# define __DATA_MANAGEMENT_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

# include "DataManagement/DC.h"
# include "DataManagement/Record.h"
# include "DataManagement/Snapshot.h"

//==========================================================================
// APIs
//==========================================================================

# ifdef __cplusplus
}
# endif // __cplusplus

# endif // __DATA_MANAGEMENT_H__

