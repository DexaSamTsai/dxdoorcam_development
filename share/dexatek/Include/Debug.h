/*!
 *  @file       debug.h
 *  @version    1.0
 *  @author     Sam Tsai
 *  @note       Debug Setting <br>
 */

# ifndef __DEBUG_H__
# define __DEBUG_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

# ifndef dbg
#	define dbg( fmt, ... )			printf( fmt"\n", ##__VA_ARGS__ )
# endif

# ifndef dbg_line
#	define dbg_line( fmt, ... )		printf( "[%s line: %d]"fmt"\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_error
#	define dbg_error( fmt, ... )	printf( "\033[1;31m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_hint
#	define dbg_hint( fmt, ... )		printf( "\033[1;32m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_r
#	define dbg_r( fmt, args... )	printf( "\033[1;31m"fmt"\033[0;39m\n", ## args )
# endif

# ifndef dbg_g
#	define dbg_g( fmt, args... )	printf( "\033[1;32m"fmt"\033[0;39m\n", ## args )
# endif

# ifndef dbg_b
#	define dbg_b( fmt, args... )	printf( "\033[1;34m"fmt"\033[0;39m\n", ## args )
# endif

# ifndef dbg_y
#	define dbg_y( fmt, args... )	printf( "\033[1;33m"fmt"\033[0;39m\n", ## args )
# endif

# ifdef __cplusplus
}
# endif // __cplusplus

# endif //__DEBUG_H__

