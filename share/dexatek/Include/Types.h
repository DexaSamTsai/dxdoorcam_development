/*!
 *  @file       types.h
 *  @version    1.0
 *  @author     Sam Tsai
 */

# ifndef __TYPES_H__
# define __TYPES_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

// ----------------------------------------------------------------------------
// Constant Definition
// ----------------------------------------------------------------------------
/*! @defgroup type_defs Type Define */
// @{
# define u8					unsigned char
# define u16				unsigned short
# define u32				unsigned int
# define u64				unsigned long long
# define i8					char
# define i16				short
# define i32				int
# define i64				long long

# ifndef BOOL
#	define BOOL				char
# endif

# ifndef TRUE
#	define TRUE				1
# endif

# ifndef FALSE
#	define FALSE			0
# endif

# ifndef SUCCESS
#	define SUCCESS			0
# endif

# ifndef FAILURE
#	define FAILURE			-1
# endif

# ifndef SET_BIT
#	define SET_BIT( x, y )			( ( x ) |= ( 1 << y ) )
# endif

# ifndef CLR_BIT
#	define CLR_BIT( x, y )			( ( x ) &= ~( 1 << y ) )
# endif

# ifndef GET_BIT
#	define GET_BIT( x, y )			( ( x >> y ) & 0x00000001 )
# endif

# ifndef CHKBIT_SET
#	define CHKBIT_SET( x, y )		( ( ( x ) & ( y ) ) == ( y ) )
# endif

# ifndef CHKBIT_CLR
#	define CHKBIT_CLR( x, y )		( ( ( x ) & ( y ) ) == ( 0 ) )
# endif

# define PUT_8( p, v )				( ( p )[ 0 ] = ( v ) & 0xff )
# define PUT_16( p, v )				( ( p )[ 0 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 1 ] = ( v ) & 0xff )
# define PUT_24( p, v )				( ( p )[ 0 ] = ( ( v ) >> 16 ) & 0xff, ( p )[ 1 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 2 ] = ( v ) & 0xff )
# define PUT_32( p, v )				( ( p )[ 0 ] = ( ( v ) >> 24 ) & 0xff, ( p )[ 1 ] = ( ( v ) >> 16 ) & 0xff, ( p )[ 2 ] = ( ( v ) >> 8 ) & 0xff, ( p )[ 3 ] = ( v ) & 0xff )
# define GET_8( p )					( ( p )[ 0 ] )
# define GET_16( p )				( ( ( p )[ 0 ] << 8 ) | ( p )[ 1 ] )
# define GET_24( p )				( ( ( p )[ 0 ] << 16 ) | ( ( p )[ 1 ] << 8 ) | ( p )[ 2 ] )
# define GET_32( p )				( ( ( p )[ 0 ] << 24 ) | ( ( p )[ 1 ] << 16 ) | ( ( p )[ 2 ] << 8 ) | ( p )[ 3 ] )

# define gettid()						syscall( __NR_gettid )

# define MAX_NAME_LENGTH				64
# define MAX_URI_LENGTH					512
# define MAX_PATH_LENGTH				256
# define MAX_DESCRIPTION_LENGTH			256
# define MAX_DOMAIN_NAME_LENGTH			128
# define MAX_IPADDR_LENGTH				16
# define MAX_MAC_LENGTH					32

// ----------------------------------------------------------------------------
// Type Declaration
// ----------------------------------------------------------------------------

// @}

# ifdef __cplusplus
}
# endif // __cplusplus

# endif //__TYPES_H__
