/*!
 *  @file       Support.h
 *  @version    1.0
 *  @author     Allen Liao
 */

# ifndef __SUPPORT_H__
# define __SUPPORT_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

// ----------------------------------------------------------------------------
// Constant Definition
// ----------------------------------------------------------------------------
/*! @defgroup type_defs Type Define */
// @{
# define SD_CARD_SUPPORT				1

// ----------------------------------------------------------------------------
// Type Declaration
// ----------------------------------------------------------------------------

// @}

# ifdef __cplusplus
}
# endif // __cplusplus

# endif //__SUPPORT_H__