/*!
 *  @file       Media.h
 *  @version    1.0
 *  @author     Sam Tsai
 */

# ifndef __MEDIA_H__
# define __MEDIA_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"

# include "Media/MediaCenter.h"

//==========================================================================
// APIs
//==========================================================================

# ifdef __cplusplus
}
# endif // __cplusplus

# endif // __MEDIA_H__

