/*!
 *  @file       HPClient.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
//# include "Project.h"
# include "Headers.h"
# include <stdio.h>
# include <netdb.h>
# include <sys/time.h>

# include "HPClient.h"

# include "dk_PeripheralAux.h"
# include "HybridPeripheralManager.h"
# include "dk_Job.h"

//==========================================================================
// Type Declaration
//==========================================================================

# ifndef dbg_line
#	define dbg_line( fmt, ... )		printf( "[%s line: %d]"fmt"\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_error
#	define dbg_error( fmt, ... )	printf( "\033[1;31m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

# ifndef dbg_warm
#	define dbg_warm( fmt, ... )		printf( "\033[1;33m[%s line: %d]"fmt"\033[0;39m\n", __FUNCTION__, __LINE__, ##__VA_ARGS__ )
# endif

typedef struct __attribute__ ( ( packed ) ) {
	uint8_t		datatype;
	uint8_t		data;
} hp_datatype_event_record;

typedef struct __attribute__ ( ( packed ) ) {
	uint8_t		datatype;
	uint8_t		data;
} hp_datatype_motion;

typedef struct __attribute__ ( ( packed ) ) {
	hp_datatype_event_record		record;
	hp_datatype_motion				motion;
} hp_datatype_payload;

typedef enum
{
	HP_JOB_RESERVED_ID_DO_NOT_USE				= MIN_SCHEDULE_JOB_RESERVED_UNIQUE_ID,
	HP_JOB_RESERVED_ID_EVENT_RECORD_VIDEO,
	HP_JOB_RESERVED_ID_EVENT_RECORD_IMAGE,
	HP_JOB_RESERVED_ID_EVENT_RECORD_BOTH,
} HP_JOB_RESERVED_ID;

typedef struct {
	uint32_t	msgid;
} hp_record_info;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static dxBOOL	frw_server_ready	= dxFALSE;
static dxBOOL	hp_client_ready		= dxFALSE;
static dxBOOL	hp_client_server_online	= dxFALSE;
static dxBOOL	hp_job_ready		= dxFALSE;
static uint32_t	hp_cmd				= 0;

static Container_MType		*m_containers;
static Service_MType		*m_service;

static char					*upload_video_url	= NULL;
static char					*download_video_url	= NULL;
static char					*upload_image_url	= NULL;
static char					*download_image_url	= NULL;
static dxBOOL				waiting_for_video_url	= dxFALSE;
static dxBOOL				waiting_for_image_url	= dxFALSE;

static pthread_mutex_t		hp_mutex = PTHREAD_MUTEX_INITIALIZER;

static dxBOOL				fw_available	= dxFALSE;

static HP_CBFuncs			hp_cb_funcs		= { 0, };

static hp_datatype_payload	datatype_payload;
static hp_record_info		record_info[ HP_RECORD_TYPE_MAX ]	= { { 0, } };
static uint8_t				*hp_mac	= NULL;

//==========================================================================
// Static Functions
//==========================================================================
static int
hp_get_mac( void )
{
	if ( hp_mac ) {
		return HP_SUCCESS;
	}

	struct netif *netif = netif_find( "wl0" );

	if ( netif ) {
		uint8_t mac[ 6 ];

		hp_mac	= ( u8 * )dxMemAlloc( "", 1, 8 );

		libwifi_get_mac( mac );

		Dump( mac, 6 );

		hp_mac[ 0 ]	= mac[ 0 ];
		hp_mac[ 1 ]	= mac[ 1 ];
		hp_mac[ 2 ]	= mac[ 2 ];
		hp_mac[ 3 ]	= 0xFF;
		hp_mac[ 4 ]	= 0xFF;
		hp_mac[ 5 ]	= mac[ 3 ];
		hp_mac[ 6 ]	= mac[ 4 ];
		hp_mac[ 7 ]	= mac[ 5 ];

		return HP_SUCCESS;
	} else {
		return HP_ERROR;
	}
}

static int
hp_send_msg( eHpCmdMessage cmd, uint32_t msg_id, void *data, int size )
{
	if ( hp_client_ready == dxTRUE ) {
		HpCmdMessageQueue_t *pCmdMessage = dxMemAlloc( "HpCmdMsg", 1, sizeof( HpCmdMessageQueue_t ) );

		pCmdMessage->Priority		= 0; //Normal

		pCmdMessage->MessageID		= msg_id; //Make the message ID same as the command request
		pCmdMessage->CommandMessage	= cmd;
		pCmdMessage->PayloadData	= ( uint8_t * )data;
		pCmdMessage->PayloadSize	= size;

		//Send the message to HybridPeripheral Server
		HpManagerSendTo_Server( pCmdMessage );
		HpManagerCleanEventArgumentObj( pCmdMessage );

		return HP_SUCCESS;
	} else {
		return HP_ERROR;
	}
}

static int
hp_send_history_event( void *data, int size )
{
	uint16_t HistoricEventDataSize		= sizeof( HpDataExcHistoryEventDetailData_t ) + size;

	HpDataExcHistoryEventDetailData_t *pHistoricEventData = ( HpDataExcHistoryEventDetailData_t * )dxMemAlloc( "", 1, HistoricEventDataSize );

	pHistoricEventData->PayloadDataSize			= size;

	memcpy( pHistoricEventData->PayloadData, data, size );

	return hp_send_msg( HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ,
						HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_REQ,
						pHistoricEventData,
						HistoricEventDataSize );
}

static int
hp_send_datatype_data( void *data, int size )
{
	uint16_t DataTypeDataSize		= sizeof( HpDataExcDataTypeDetailData_t ) + size;

	HpDataExcDataTypeDetailData_t *pDataTypeData = ( HpDataExcDataTypeDetailData_t * )dxMemAlloc( "", 1, DataTypeDataSize );

	pDataTypeData->PayloadDataSize			= size;

	memcpy( pDataTypeData->PayloadData, data, size );

	return hp_send_msg( HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ,
						HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_REQ,
						pDataTypeData,
						DataTypeDataSize );
}

static void
hp_add_shcedule_job( uint32_t job_id, uint32_t datatype, uint8_t state )
{
	JobInstance_t JobInstance;

	struct ScheduleSequenceNumber_s	ScheduleSEQNo;
	WifiCommDeviceMac_t				GatewayMac;
	DevAddress_t					PeripheralMac;
	uint16_t	ReturnedPayloadLen;
	uint8_t		*pJobPayload;

	ScheduleSEQNo.PeripheralUniqueID	= 0xFFFFFFFF;
	ScheduleSEQNo.UniqueSequenceID		= job_id;

	memcpy( GatewayMac.MacAddr, hp_mac, 8 );
	memcpy( PeripheralMac.Addr, hp_mac, 8 );

	CreateJobPackage( &JobInstance, &GatewayMac, &ScheduleSEQNo, 0 );

	JobPackageHeader_ScheduleRepeatableJobSetup( &JobInstance, JOB_SCHEDULE_START_TIME_NOW );

	JobPackageConditionAdd_AdvertisementDataConditionJobSetup( &JobInstance, &PeripheralMac, datatype, OPERATOR_EQUAL, VALUE_TYPE_UINT8, &state );
	JobPackageExecutionAdd_GatewayJob_HistoricEventUpdateSetup( &JobInstance );

	ReturnedPayloadLen	= 0;
	pJobPayload			= JobPackageGeneratePayloadPack( &JobInstance, &ReturnedPayloadLen );

	if ( HP_CMD_JobBridge_ScheduleJobBridgeAddReq( 0, pJobPayload, ReturnedPayloadLen, job_id, 0 ) == dxFALSE ) {
		dbg_error( "Do HP_CMD_JobBridge_ScheduleJobBridgeAddReq Failed!!!" );
	}

	FreeJobPackage( &JobInstance );
}

static void
hp_add_customized_shcedule_job( void )
{
	if ( hp_get_mac() == HP_ERROR ) {
		return;
	}

	hp_add_shcedule_job( HP_JOB_RESERVED_ID_EVENT_RECORD_VIDEO, DK_DATA_TYPE_RECORDING_EVENT, EVENT_VIDEO_RECORD );
	hp_add_shcedule_job( HP_JOB_RESERVED_ID_EVENT_RECORD_IMAGE, DK_DATA_TYPE_RECORDING_EVENT, EVENT_IMAGE_RECORD );
	hp_add_shcedule_job( HP_JOB_RESERVED_ID_EVENT_RECORD_BOTH, DK_DATA_TYPE_RECORDING_EVENT, EVENT_RECORD_BOTH );
}

static void
hp_parser_container_update( HpCmdMessageQueue_t *pHpCmdMessage )
{
	HpDataExcContainerDataReport_t *pHpDataReport = ( HpDataExcContainerDataReport_t * )pHpCmdMessage->PayloadData;

	if ( pHpDataReport == NULL ) {
		dbg_warm( "pHpDataReport == NULL" );
	} else {
		uint32_t mid, did;

		for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
			if ( m_containers[ mid ].active == dxTRUE ) {
				if ( m_containers[ mid ].mtype == pHpDataReport->ContMType ) {
					Container_DType *container_dtypes	= m_containers[ mid ].dtypes;

					if ( m_containers[ mid ].mid && ( m_containers[ mid ].mid != pHpDataReport->ContMID ) ) {
						dbg_warm( "MType%u: Mid( %llu -> %llu ) !!!", m_containers[ mid ].mtype, m_containers[ mid ].mid, pHpDataReport->ContMID );
					}

					m_containers[ mid ].mid	= pHpDataReport->ContMID;

					for ( did = 0; did < m_containers[ mid ].dtypes_cnt; did ++ ) {
						if ( pHpDataReport->ContDType == container_dtypes[ did ].type ) {
							if ( container_dtypes[ did ].id && ( container_dtypes[ did ].id != pHpDataReport->ContDID ) ) {
								dbg_warm( "MType%u-DType%u: Did( %llu -> %llu ) !!!", m_containers[ mid ].mtype, container_dtypes[ did ].type,
																					container_dtypes[ did ].id, pHpDataReport->ContDID );
							}

							container_dtypes[ did ].id	= pHpDataReport->ContDID;

							if ( container_dtypes[ did ].crc == pHpDataReport->ContLVCRC ) {
								container_dtypes[ did ].updated		= dxFALSE;
								container_dtypes[ did ].need_value	= dxFALSE;
							} else if ( pHpDataReport->LValueLen ) {
								pHpDataReport->CombinedValue[ pHpDataReport->LValueLen ]	= 0;

								container_dtypes[ did ].crc			= pHpDataReport->ContLVCRC;

								if ( container_dtypes[ did ].set_config == NULL ) {
									dbg_warm( "MType%u: container_dtypes[ %u ].set_config is NULL!!!", m_containers[ mid ].mtype, pHpDataReport->ContDType );
								} else {
									dbg_line( "pHpDataReport->ContLVCRC: %08x", pHpDataReport->ContLVCRC );
									dbg_line( "pHpDataReport->LValueLen: %d", pHpDataReport->LValueLen );
									dbg_line( "pHpDataReport->CombinedValue: %s", pHpDataReport->CombinedValue );
									container_dtypes[ did ].set_config( pHpDataReport->CombinedValue, pHpDataReport->LValueLen, container_dtypes[ did ].data );
								}

								container_dtypes[ did ].updated		= dxTRUE;
								container_dtypes[ did ].need_value	= dxFALSE;
							} else {
								container_dtypes[ did ].need_value	= dxTRUE;
								dbg_line( "container_dtypes[ did ].need_value" );
							}

							return;
						}
					}

					break;
				}
			}
		}
	}
}

static void
hp_parser_data_exchange( HpCmdMessageQueue_t *pHpCmdMessage )
{
	HpDataExcRes_t *pHpDataExcRes = ( HpDataExcRes_t * )pHpCmdMessage->PayloadData;

	if ( pHpDataExcRes == NULL ) {
		dbg_warm( "pHpDataExcRes == NULL" );
	} else if ( pHpCmdMessage->PayloadSize != sizeof( HpDataExcRes_t ) ) {
		dbg_warm( "PayloadSize Error( %d )!!!", pHpCmdMessage->PayloadSize );
	} else {
		HpDataExcRes_t *ExcRes = ( HpDataExcRes_t * )pHpCmdMessage->PayloadData;

		if ( ExcRes->Status == HP_DATA_EXCHANGE_SUCCESS ) {
			dbg_line( "hp_parser_data_exchange Success" );
		} else {
			dbg_line( "hp_parser_data_exchange Failed = %d", ExcRes->Status );
		}
	}
}

static dxBOOL
hp_uuid_match( const char my_uuid[ 32 ], uint8_t req_uuid[ 16 ] )
{
	uint8_t u8_uuid[ 16 ];

	DKHex_ASCToHex( my_uuid, 32, u8_uuid, 16 );

	if ( memcmp( u8_uuid, req_uuid, 16 ) ) {
		return dxFALSE;
	} else {
		return dxTRUE;
	}
}

static void
hp_control_access_handle( uint32_t mtype, uint32_t dtype, void *data, uint32_t len )
{
	uint32_t mid, did;

	for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
		if ( m_containers[ mid ].mtype == mtype ) {
			if ( m_containers[ mid ].mid ) {
				for ( did = 0; did < m_containers[ mid ].dtypes_cnt; did ++ ) {
					if ( m_containers[ mid ].dtypes[ did ].type == dtype ) {
						if ( m_containers[ mid ].dtypes[ did ].id ) {
							HP_CMD_DataExchange_UpdateContainerDetailData( ( uint32_t )time( NULL ),
																			m_containers[ mid ].mid,
																			m_containers[ mid ].dtypes[ did ].id,
																			m_containers[ mid ].dtypes[ did ].type,
																			0, NULL,
																			0, NULL,
																			len, data,
																			0 );
						} else {
							dbg_warm( "No m_containers[ %d ].dtypes[ %d ].id!!!", mid, did );
						}

						return;
					}
				}
			} else {
				dbg_warm( "No m_containers[ %d ].mid!!!", mid );
			}

			return;
		}
	}
}

static Response_Payload_t
hp_parser_control_access( HpCmdMessageQueue_t *pHpCmdMessage, dxBOOL send_to_server )
{
	HpControlAccessReq_t	*pHpCtrLAcc = ( HpControlAccessReq_t * )pHpCmdMessage->PayloadData;
	Response_Payload_t		Payload = { 0, };

	if ( pHpCtrLAcc == NULL ) {
		dbg_warm( "pHpCtrLAcc == NULL" );
	} else {
		uint32_t mid, did;

		for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
			if ( m_service[ mid ].uuid &&
				 ( hp_uuid_match( m_service[ mid ].uuid, pHpCtrLAcc->ServiceUUID ) == dxTRUE ) ) {
				if ( m_service[ mid ].dtypes ) {
					Service_DType *dtypes	= m_service[ mid ].dtypes;

					for ( did = 0; did < m_service[ mid ].dtypes_cnt; did ++ ) {
						if ( hp_uuid_match( dtypes[ did ].uuid, pHpCtrLAcc->CharacteristicUUID ) == dxTRUE ) {
							if ( dtypes[ did ].action ) {
								dtypes[ did ].action( pHpCtrLAcc->pDataPayload, pHpCtrLAcc->DataLen, pHpCmdMessage->TimeStamp, &Payload );
							}

							if ( send_to_server ) {
								dbg_line( "pHpCtrLAcc->DataLen: %d", pHpCtrLAcc->DataLen );
								dbg_line( "pHpCtrLAcc->pDataPayload: %s", pHpCtrLAcc->pDataPayload );
								hp_control_access_handle( m_service[ mid ].mtype, dtypes[ did ].dtype, pHpCtrLAcc->pDataPayload, pHpCtrLAcc->DataLen );
							}

							return Payload;
						}
					}
				}

				return Payload;
			}
		}
	}

	return Payload;
}

static int
hp_send_control_access_ack( void *request, uint32_t msg_id, uint32_t cmd_id, Response_Payload_t *response )
{
	HpControlAccessReq_t	*pHpCtrLAcc = ( HpControlAccessReq_t * )request;
	HpControlAccessRes_t	*pControlAccessRes = dxMemAlloc( "HpCmdMsg", 1, sizeof( HpControlAccessRes_t ) + response->payload_len );

	pControlAccessRes->Status				= HP_CONTROL_ACCESS_SUCCESS;
	pControlAccessRes->SourceOfOrigin		= pHpCtrLAcc->SourceOfOrigin;
	pControlAccessRes->IdentificationNumber = pHpCtrLAcc->IdentificationNumber;

	if ( response->payload ) {
		pControlAccessRes->DataLen	= response->payload_len;
		memcpy( pControlAccessRes->pDataPayload, response->payload, response->payload_len );
		free( response->payload );
	}

	return hp_send_msg( cmd_id,
						msg_id,
						pControlAccessRes,
						sizeof( HpControlAccessRes_t ) );
}

static dxBOOL
hp_is_building_container( Container_MType *mcont )
{
	int did;

	for ( did = 0; did < mcont->dtypes_cnt; did ++ ) {
		if ( mcont->dtypes[ did ].id == 0 ) {
			if ( mcont->dtypes[ did ].default_value == NULL ) {
				dbg_error( "mcont->dtypes[ %d ].default_value == NULL", did );
				continue;
			}

			uint32_t default_size	= strlen( mcont->dtypes[ did ].default_value ) + 1;

			if ( mcont->mid ) {
				if ( mcont->dtypes[ did ].is_lvalue ) {
					HP_CMD_DataExchange_InsertContainerDetailData( ( uint32_t )time( NULL ), mcont->mid, mcont->dtypes[ did ].type, 0, NULL, 0, NULL, default_size, mcont->dtypes[ did ].default_value, 0 );
				} else {
					HP_CMD_DataExchange_InsertContainerDetailData( ( uint32_t )time( NULL ), mcont->mid, mcont->dtypes[ did ].type, 0, NULL, default_size, mcont->dtypes[ did ].default_value, 0, NULL, 0 );
				}
			} else {
				if ( mcont->dtypes[ did ].is_lvalue ) {
					HP_CMD_DataExchange_CreateFullContainerData( ( uint32_t )time( NULL ), mcont->mtype, mcont->dtypes[ did ].type, 0, NULL, 0, NULL, 0, NULL, default_size, mcont->dtypes[ did ].default_value, 0 );
				} else {
					HP_CMD_DataExchange_CreateFullContainerData( ( uint32_t )time( NULL ), mcont->mtype, mcont->dtypes[ did ].type, 0, NULL, 0, NULL, default_size, mcont->dtypes[ did ].default_value, 0, NULL, 0 );
				}
			}

			return dxTRUE;
		}
	}

	return dxFALSE;
}

static dxBOOL
hp_chk_container_ready( void )
{
	int mid;

	for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
		if ( m_containers[ mid ].active == dxTRUE ) {
			if ( hp_is_building_container( &m_containers[ mid ] ) == dxTRUE ) {
				dbg_line( "hp_is_building_container" );
				return dxFALSE;
			}
		}
	}

	return dxTRUE;
}

static dxBOOL
hp_chk_container_lvalue_ready( void )
{
	uint32_t mid, did;

	for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
		if ( m_containers[ mid ].active == dxTRUE ) {
			for ( did = 0; did < m_containers[ mid ].dtypes_cnt; did ++ ) {
				if ( m_containers[ mid ].dtypes[ did ].need_value == dxTRUE ) {
					dbg_line( "m_containers[ %d ].dtypes[ %d ].id: %llu", mid, did, m_containers[ mid ].dtypes[ did ].id );
					dbg_line( "HP_CMD_DataExchange_ContainerDataRequest" );
					HP_CMD_DataExchange_ContainerDataRequest( ( uint32_t )time( NULL ), m_containers[ mid ].mid, m_containers[ mid ].dtypes[ did ].id, 0 );
					return dxFALSE;
				}
			}
		}
	}

	return dxTRUE;
}

static void
hp_clear_container_id( void )
{
	uint32_t mid, did;

	for ( mid = 0; mid < HP_MACHINE_MAX; mid ++ ) {
		if ( m_containers[ mid ].active == dxTRUE ) {
			m_containers[ mid ].mid		= 0;

			for ( did = 0; did < m_containers[ mid ].dtypes_cnt; did ++ ) {
				m_containers[ mid ].dtypes[ did ].id		= 0;
				m_containers[ mid ].dtypes[ did ].crc		= 0;
				m_containers[ mid ].dtypes[ did ].updated	= 0;
			}
		}
	}
}

//DxIpCam must start before SigmaCam, or DxIpCam may lose CommandMessage
static void
hp_update_stream_url( HpCmdMessageQueue_t *pHpCmdMessage )
{
	if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
		HpStreamingUrlInfo_t *pStreamingUrlRes = ( HpStreamingUrlInfo_t * )pHpCmdMessage->PayloadData;

		if ( pStreamingUrlRes->UrlLen ) {
			char		*url = NULL;
			uint32_t	url_len	= 0;

			url = ( char * )dxMemAlloc( "hp_proc", 1, ( pStreamingUrlRes->UrlLen + 1 ) );

			if ( url ) {
				url_len	= sprintf( url, "%s", pStreamingUrlRes->pUrl );

				hp_cb_funcs.update_upload_stream_url( url, url_len );
			}
		}
	}
}

static void
hp_update_file_url( HpCmdMessageQueue_t *pHpCmdMessage )
{
	if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
		HpVideoImageEvtUrlInfo_t *pEventUrlInfo = ( HpVideoImageEvtUrlInfo_t * )pHpCmdMessage->PayloadData;

		char *upload_url	= NULL;
		char *download_url	= NULL;

		if ( pEventUrlInfo->UploadUrlLen ) {
			upload_url = ( char * )dxMemAlloc( "hp_proc", 1, ( pEventUrlInfo->UploadUrlLen + 1 ) );

			if ( upload_url ) {
				memcpy( upload_url, pEventUrlInfo->CombineUrl, pEventUrlInfo->UploadUrlLen );
			}
		}

		if ( pEventUrlInfo->DownloadUrlLen ) {
			download_url = ( char * )dxMemAlloc( "hp_proc", 1, ( pEventUrlInfo->DownloadUrlLen + 1 ) );

			if ( download_url ) {
				memcpy( download_url, &pEventUrlInfo->CombineUrl[ pEventUrlInfo->UploadUrlLen ], pEventUrlInfo->DownloadUrlLen );
			}
		}

		hp_cb_funcs.update_upload_record_url( pHpCmdMessage->MessageID, upload_url, download_url );
	}
}

static void
hp_proc( void *arg )
{
	if ( arg ) {
		HpCmdMessageQueue_t		*pHpCmdMessage	= ( HpCmdMessageQueue_t * )arg;
		Response_Payload_t		response = { 0, };

		char		*url = NULL;
		uint32_t	url_len	= 0;

		switch ( pHpCmdMessage->CommandMessage ) {
			//DataExchange Direction = HP to Framework
			case HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE:							//Payload given as HpDataExcStatDataUpdate_t
			case HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ:							//Payload given as HpDataExcContainerDataRequest_t
			case HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_REQ:				//Payload given as HpDataExcCreateFullContainerData_t
			case HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_REQ:				//Payload given as HpDataExcInsertContainerDetailData_t
			case HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_REQ:				//Payload given as HpDataExcUpdateContainerDetailData_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_REQ" );
				break;

			//DataExchange Direction = Framework to HP
			case HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT:							//Payload given as HpDataExcContainerDataReport_t
																					//This message will automatically be reported to HP during
																					//initial boot, as well as it will be reported as response from HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ
																					//NOTE: The message ID will follow the provided one when requested from HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REQ
				dbg_warm( "HP_CMD_DATAEXCHANGE_CONTAINER_DATA_REPORT" );
				hp_parser_container_update( pHpCmdMessage );

				if ( frw_server_ready == dxTRUE ) {
					if ( hp_chk_container_ready() == dxTRUE ) {
						if ( hp_chk_container_lvalue_ready() == dxTRUE ) {
							hp_client_ready	= dxTRUE;
						}
					}
				}

				break;

			case HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE_RES:						//Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_STATUS_DATA_UPDATE_RES" );
				hp_parser_data_exchange( pHpCmdMessage );
				break;

			case HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_RES:				//Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_CREATE_FULL_CONTAINER_DATA_RES" );
				hp_parser_data_exchange( pHpCmdMessage );
				break;

			case HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_RES:				//Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_INSERT_CONTAINER_DETAIL_DATA_RES" );
				hp_parser_data_exchange( pHpCmdMessage );
				break;

			case HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_RES:				//Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_UPDATE_CONTAINER_DETAIL_DATA_RES" );
				hp_parser_data_exchange( pHpCmdMessage );
				break;

			case HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_RES:               //Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_DATATYPE_DATA_PAYLOAD_UPDATE_RES" );
				break;

			case HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_RES:            //Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_HISTORY_EVENT_WITH_DATA_PAYLOAD_RES" );
				break;

			case HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_RES:                     //Payload given as generic HpDataExcRes_t
				dbg_warm( "HP_CMD_DATAEXCHANGE_AGGREGATED_DATA_UPDATE_RES" );
				break;

			/* STORAGE ACCESS COMMAND MESSAGE
			*/

			//Storage Access Direction = HP to Framework
			case HP_CMD_STORAGE_ACCESS_STORAGE_INFO_REQUEST:
			case HP_CMD_STORAGE_ACCESS_STORAGE_WRITE_REQUEST:
			case HP_CMD_STORAGE_ACCESS_STORAGE_READ_REQUEST:
			case HP_CMD_STORAGE_ACCESS_STORAGE_ERASE_REQUEST:
				dbg_warm( "HP_CMD_STORAGE_ACCESS_STORAGE_REQUEST" );
				break;

			//Storage Access Direction = Framework to HP
			case HP_CMD_STORAGE_ACCESS_STORAGE_INFO_RES:
			case HP_CMD_STORAGE_ACCESS_STORAGE_WRITE_RES:
			case HP_CMD_STORAGE_ACCESS_STORAGE_READ_RES:
			case HP_CMD_STORAGE_ACCESS_STORAGE_ERASE_RES:
				dbg_warm( "HP_CMD_STORAGE_ACCESS_STORAGE_RES" );
				break;

			/* CONTROL MODULE & PROFILE COMMAND MESSAGE
			*/

			//Control Access Direction = Framework to HP
			case HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST:		//Right after access it is required to update status data (SValue)
				dbg_warm( "HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_REQUEST" );
				// Streaming Config and Motion Config
				response = hp_parser_control_access( pHpCmdMessage, dxTRUE );
				hp_send_control_access_ack( pHpCmdMessage->PayloadData, pHpCmdMessage->MessageID, HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_RES, &response );
				break;

			case HP_CMD_CONTROL_ACCESS_WRITE_REQUEST:
				dbg_warm( "HP_CMD_CONTROL_ACCESS_WRITE_REQUEST" );
				// Event Request
				response = hp_parser_control_access( pHpCmdMessage, dxFALSE );
				hp_send_control_access_ack( pHpCmdMessage->PayloadData, pHpCmdMessage->MessageID, HP_CMD_CONTROL_ACCESS_WRITE_RES, &response );
				break;

			case HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST:		//TODO: Implementation to support such is not yet finalized.
				dbg_warm( "HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_REQUEST" );
				break;


			//Control Access Direction = HP to Framework
			case HP_CMD_CONTROL_ACCESS_WRITE_WITH_STATUS_DATA_UPDATE_RES:			//Must provide status report right after both the "actual" write access & sending the status datat update request via DataExchange.
			case HP_CMD_CONTROL_ACCESS_WRITE_RES:									//Must provide status report right after the "actual" write access.
			case HP_CMD_CONTROL_ACCESS_READ_FOLLOW_AGGRETATE_UPDATE_RES:			//Must provide status report right after both the "actual" write access & sending the status datat update request via DataExchange.
				dbg_warm( "HP_CMD_CONTROL_ACCESS" );
				break;

			/* FrameWork Status Report MESSAGE
			*/

			//Status Report Direction = Framework to HP
			//WARNING: State EXITING should always follow right after ENTERING (eg if ENTERING is 600 then same state EXITING should be 601)
			case HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING:							//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_IDLE_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_IDLE_EXITING:							//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_IDLE_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_EXITING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_FWUPDATE_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_EXITING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_OFFLINE_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING:					//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_ENTERING" );
				hp_client_server_online	= dxTRUE;
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_EXITING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_SRVONLINE_EXITING" );
				hp_client_server_online	= dxFALSE;
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_SETUP_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_SETUP_EXITING:							//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_SETUP_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_ENTERING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_EXITING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_REGISTER_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_FATAL_ENTERING:						//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_FATAL_ENTERING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_ST_FATAL_EXITING:							//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_ST_FATAL_EXITING" );
				break;

			case HP_CMD_FRW_STATUS_REPORT_SERVER_READY:								//No Payload
				dbg_warm( "HP_CMD_FRW_STATUS_REPORT_SERVER_READY" );

				if ( hp_job_ready == FALSE ) {
					hp_add_customized_shcedule_job();
					hp_job_ready	= TRUE;
				}

				frw_server_ready	= dxTRUE;

				if ( hp_chk_container_ready() == dxTRUE ) {
					if ( hp_chk_container_lvalue_ready() == dxTRUE ) {
						hp_client_ready	= dxTRUE;
					}
				}

				break;

			case HP_CMD_STREAMING_URL_RES:
				dbg_warm( "HP_CMD_STREAMING_URL_RES" );

				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpStreamingUrlInfo_t *pStreamingUrlRes = ( HpStreamingUrlInfo_t * )pHpCmdMessage->PayloadData;

					if ( hp_cb_funcs.update_upload_stream_url ) {
						if ( pStreamingUrlRes->UrlLen ) {
							url = ( char * )dxMemAlloc( "hp_proc", 1, ( pStreamingUrlRes->UrlLen + 1 ) );

							if ( url ) {
								url_len	= sprintf( url, "%s", pStreamingUrlRes->pUrl );
							}
						}
					}
				}

				if ( hp_cb_funcs.update_upload_stream_url ) {
					hp_cb_funcs.update_upload_stream_url( url, url_len );
				}

				break;

			case HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES:
				dbg_warm( "HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_RES" );
				if ( hp_cb_funcs.update_upload_record_url ) {
					hp_update_file_url( pHpCmdMessage );
				}
				break;

			case HP_CMD_EVENT_NOTIFICATION_SEND_RES:
				dbg_warm( "HP_CMD_EVENT_NOTIFICATION_SEND_RES" );

				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpEvtGenericRes_t* pEventNotificationRes = ( HpEvtGenericRes_t * )pHpCmdMessage->PayloadData;
					if ( pEventNotificationRes->Status == HP_EVENT_ACCESS_SUCCESS ) {
						dbg_line( "HP_CMD_EVENT_NOTIFICATION_SEND_RES Success" );
					} else {
						dbg_line( "HP_CMD_EVENT_NOTIFICATION_SEND_RES Failed = %d", pEventNotificationRes->Status );
					}
				}
				break;

			case HP_CMD_NOTIFY_EVENT_DONE_SEND_RES:
				dbg_warm( "HP_CMD_NOTIFY_EVENT_DONE_SEND_RES" );

				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpEvtGenericRes_t* pNotifyEventDoneRes = ( HpEvtGenericRes_t * )pHpCmdMessage->PayloadData;
					if ( pNotifyEventDoneRes->Status == HP_EVENT_ACCESS_SUCCESS ) {
						dbg_line( "HP_CMD_NOTIFY_EVENT_DONE_SEND_RES Success" );
					} else {
						dbg_line( "HP_CMD_NOTIFY_EVENT_DONE_SEND_RES Failed = %d", pNotifyEventDoneRes->Status );
					}
				}
				break;

			/* system Report MESSAGE
			*/

			//System Report Direction = Framework to HP
			case HP_CMD_ENTER_FW_UPDATE_REPORT:										//Payload given as HpFwUpdateStatusRes_t
				dbg_warm( "HP_CMD_ENTER_FW_UPDATE_REPORT" );
				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpFwUpdateStatusRes_t *pFwUpdateStatusRes = ( HpFwUpdateStatusRes_t * )pHpCmdMessage->PayloadData;

					if ( hp_cb_funcs.update_fw_status ) {
						hp_cb_funcs.update_fw_status( FW_STATUS_UPGRADING, pFwUpdateStatusRes->Progress );
					}
				}
				break;

			case HP_CMD_SYSTEM_NOT_REGISTERED:										//No Payload, will be sent when framewrok is in IDLE state and system is not registered
				if ( frw_server_ready ) {
					dbg_warm( "HP_CMD_SYSTEM_NOT_REGISTERED" );
					hp_clear_container_id();
				}

				frw_server_ready	= dxFALSE;
				hp_client_ready		= dxFALSE;

				break;

			case HP_CMD_CHECK_FW_UPDATE_AVAILABE_RES:
				dbg_warm( "HP_CMD_CHECK_FW_UPDATE_AVAILABE_RES" );
				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpFwUpdateAvailableRes_t *pFwUpdateAvailableRes = ( HpFwUpdateAvailableRes_t * )pHpCmdMessage->PayloadData;

					if ( pFwUpdateAvailableRes->FwUpdateAvailabilityStatus && hp_cb_funcs.update_fw_status ) {
						hp_cb_funcs.update_fw_status( FW_STATUS_AVAILABE, 0 );
						fw_available	= dxTRUE;
					}
				}
				break;

			case HP_CMD_SYS_REPORT_GET_LOCAL_TIME_RES:
				dbg_warm( "HP_CMD_SYS_REPORT_GET_LOCAL_TIME_RES" );
				if ( pHpCmdMessage->PayloadSize && pHpCmdMessage->PayloadData ) {
					HpLocalTimeGenericRes_t *pLocalTimeGenericRes = ( HpLocalTimeGenericRes_t * )pHpCmdMessage->PayloadData;

					if ( ( pLocalTimeGenericRes->Status == HP_LOCAL_TIME_GET_SUCCESS ) && hp_cb_funcs.update_local_time ) {
						hp_cb_funcs.update_local_time( &pLocalTimeGenericRes->Time );
					}
				}
				break;

			default:
				break;
		}

		hp_cmd = pHpCmdMessage->CommandMessage;

		HpManagerCleanEventArgumentObj( arg );
	}
}

//==========================================================================
// APIs
//==========================================================================
int
HPClient_Init( Container_MType mcont[ HP_MACHINE_MAX ], Service_MType mservice[ HP_MACHINE_MAX ] )
{
	if ( ( mcont == NULL ) || ( mservice == NULL ) ) {
		return HP_ERROR;
	}

	frw_server_ready	= dxFALSE;
	hp_client_ready		= dxFALSE;
	hp_job_ready		= dxFALSE;
	hp_cmd				= 0;

	upload_video_url	= NULL;
	download_video_url	= NULL;
	upload_image_url	= NULL;
	download_image_url	= NULL;
	waiting_for_video_url	= dxFALSE;
	waiting_for_image_url	= dxFALSE;

	pthread_mutex_init( &hp_mutex, NULL );

	fw_available	= dxFALSE;

	m_containers	= mcont;
	m_service		= mservice;

	memset( &hp_cb_funcs, 0, sizeof( HP_CBFuncs ) );
	memset( &datatype_payload, 0, sizeof( datatype_payload ) );

	datatype_payload.record.datatype		= DK_DATA_TYPE_RECORDING_EVENT;
	datatype_payload.motion.datatype		= DK_DATA_TYPE_MOTION_SENSE_STATUS;

	hp_mac	= NULL;

	hp_get_mac();

	if ( HpManagerInit_Client( hp_proc ) != HP_MANAGER_SUCCESS ) {
		dbg_error( "Do HpManagerInit_Client Failed !!!" );
		return HP_ERROR;
	}

	return HP_SUCCESS;
}

char
HPClient_ServerReady( void )
{
	return hp_client_ready;
}

char
HPClient_ServerOnline( void )
{
	return hp_client_server_online;
}

int
HPClient_GetUploadStreamToken( void )
{
	return hp_send_msg( HP_CMD_STREAMING_URL_REQUEST,
						HP_CMD_STREAMING_URL_REQUEST,
						NULL,
						0 );
}

int
HPClient_GetUploadVideoToken( char **upload_token, char **download_token, time_t trigger_time )
{
	if ( ( upload_token == NULL ) || ( download_token == NULL ) ) {
		return HP_ERROR;
	} else if ( hp_client_ready == dxFALSE ) {
		return HP_ERROR;
	}

	HpVideoImageEvtUrlReq_t *pEvtUrlRequestInfo = ( HpVideoImageEvtUrlReq_t * )dxMemAlloc( "pEvtUrlRequestInfo", 1, sizeof( HpVideoImageEvtUrlReq_t ) );
	uint32_t	start_clock	= seconds();

	waiting_for_video_url	= dxTRUE;

	pthread_mutex_lock( &hp_mutex );
	if ( upload_video_url ) {
		dxMemFree( upload_video_url );
		upload_video_url = NULL;
	}

	if ( download_video_url ) {
		dxMemFree( download_video_url );
		download_video_url = NULL;
	}
	pthread_mutex_unlock( &hp_mutex );

	pEvtUrlRequestInfo->EventType	= HP_EVENT_TYPE_VIDEO;
	pEvtUrlRequestInfo->FileFormat	= HP_FILE_TYPE_MP4;
	pEvtUrlRequestInfo->DateTime	= trigger_time;

	hp_send_msg( HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST,
				 HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST,
				 pEvtUrlRequestInfo,
				 sizeof( HpVideoImageEvtUrlReq_t ) );

	while ( ( upload_video_url == NULL ) && ( download_video_url == NULL ) ) {
		if ( seconds() > ( start_clock + 10 ) ) {
			waiting_for_video_url	= dxFALSE;
			dbg_warm( "Timeout!!!" );
			return HP_ERROR;
		}
		MSleep( 100 );
	}

	pthread_mutex_lock( &hp_mutex );

	*upload_token	= strdup( upload_video_url );
	*download_token	= strdup( download_video_url );

	pthread_mutex_unlock( &hp_mutex );

	waiting_for_video_url	= dxFALSE;

	if ( ( *upload_token == NULL ) || ( *download_token == NULL ) ) {
		if ( *upload_token ) {
			free( *upload_token );
			*upload_token = NULL;
		}

		if ( *download_token ) {
			free( *download_token );
			*download_token = NULL;
		}

		return HP_ERROR;
	}

	return HP_SUCCESS;
}

int
HPClient_GetUploadImageToken( char **upload_token, char **download_token, time_t trigger_time )
{
	if ( ( upload_token == NULL ) || ( download_token == NULL ) ) {
		return HP_ERROR;
	} else if ( hp_client_ready == dxFALSE ) {
		return HP_ERROR;
	}

	HpVideoImageEvtUrlReq_t *pEvtUrlRequestInfo = ( HpVideoImageEvtUrlReq_t * )dxMemAlloc( "pEvtUrlRequestInfo", 1, sizeof( HpVideoImageEvtUrlReq_t ) );
	uint32_t	start_clock	= seconds();

	waiting_for_image_url	= dxTRUE;

	pthread_mutex_lock( &hp_mutex );
	if ( upload_image_url ) {
		dxMemFree( upload_image_url );
		upload_image_url = NULL;
	}

	if ( download_image_url ) {
		dxMemFree( download_image_url );
		download_image_url = NULL;
	}
	pthread_mutex_unlock( &hp_mutex );

	pEvtUrlRequestInfo->EventType	= HP_EVENT_TYPE_IMAGE;
	pEvtUrlRequestInfo->FileFormat	= HP_FILE_TYPE_TAR_GZ;
	pEvtUrlRequestInfo->DateTime	= trigger_time;

	hp_send_msg( HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST,
				 HP_CMD_VIDEO_IMAGE_EVENT_DATA_UPLOAD_URL_REQUEST,
				 pEvtUrlRequestInfo,
				 sizeof( HpVideoImageEvtUrlReq_t ) );

	while ( ( upload_image_url == NULL ) && ( download_image_url == NULL ) ) {
		if ( seconds() > ( start_clock + 10 ) ) {
			dbg_warm( "Timeout!!!" );
			waiting_for_image_url	= dxFALSE;
			return HP_ERROR;
		}
		MSleep( 100 );
	}

	pthread_mutex_lock( &hp_mutex );

	*upload_token	= strdup( upload_image_url );
	*download_token	= strdup( download_image_url );

	pthread_mutex_unlock( &hp_mutex );

	waiting_for_image_url	= dxFALSE;

	if ( ( *upload_token == NULL ) || ( *download_token == NULL ) ) {
		if ( *upload_token ) {
			free( *upload_token );
			*upload_token = NULL;
		}

		if ( *download_token ) {
			free( *download_token );
			*download_token = NULL;
		}

		return HP_ERROR;
	}

	return HP_SUCCESS;
}

int
HPClient_SendVideoNotification( char *download_token )
{
	if ( download_token == NULL ) {
		return HP_ERROR;
	} else {
		uint16_t    ResourceLen				= strlen( download_token );
		uint16_t    EvtNotificationInfoSize = sizeof( HpEvtNotificationReq_t ) + ResourceLen + 1;

		HpEvtNotificationReq_t *pEvtNotificationInfo = dxMemAlloc( "pEvtNotificationInfo",
																	1,
																	EvtNotificationInfoSize );

		pEvtNotificationInfo->MessageCategoryType		= HP_EVENT_TYPE_VIDEO;
		pEvtNotificationInfo->MessageCategoryTemplate	= HP_TEMPLATE_STD;
		pEvtNotificationInfo->MessageCategoryFileType	= HP_FILE_TYPE_MP4;
		pEvtNotificationInfo->MessageResourceLen		= ResourceLen + 1; //Including NULL

		sprintf( ( char * )pEvtNotificationInfo->CombinePayload, "%s", download_token );

		return hp_send_msg( HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST,
							HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST,
							pEvtNotificationInfo,
							EvtNotificationInfoSize );
	}
}

int
HPClient_SendImageNotification( char *download_token )
{
	if ( download_token == NULL ) {
		return HP_ERROR;
	} else {
		uint16_t    ResourceLen				= strlen( download_token ) + 1;
		uint16_t    EvtNotificationInfoSize = sizeof( HpEvtNotificationReq_t ) + ResourceLen;

		HpEvtNotificationReq_t *pEvtNotificationInfo = dxMemAlloc( "pEvtNotificationInfo",
																	1,
																	EvtNotificationInfoSize );

		pEvtNotificationInfo->MessageCategoryType		= HP_EVENT_TYPE_IMAGE;
		pEvtNotificationInfo->MessageCategoryTemplate	= HP_TEMPLATE_STD;
		pEvtNotificationInfo->MessageCategoryFileType	= HP_FILE_TYPE_TAR_GZ;
		pEvtNotificationInfo->MessageResourceLen		= ResourceLen + 1; //Including NULL

		sprintf( ( char * )pEvtNotificationInfo->CombinePayload, "%s", download_token );

		return hp_send_msg( HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST,
							HP_CMD_EVENT_NOTIFICATION_SEND_REQUEST,
							pEvtNotificationInfo,
							EvtNotificationInfoSize );
	}
}

int
HPClient_SendEventRecordReport( int report_video, int report_image )
{
	datatype_payload.record.datatype		= DK_DATA_TYPE_RECORDING_EVENT;

	if ( report_video && report_image ) {
		datatype_payload.record.data		= EVENT_RECORD_BOTH;
	} else if ( report_video ) {
		datatype_payload.record.data		= EVENT_VIDEO_RECORD;
	} else if ( report_image ) {
		datatype_payload.record.data		= EVENT_IMAGE_RECORD;
	} else {
		datatype_payload.record.data		= EVENT_NONE;
	}

	return hp_send_datatype_data( &datatype_payload, sizeof( hp_datatype_payload ) );
}

int
HPClient_SendMotionDetectionReport( BOOL motion_triggered )
{
	datatype_payload.motion.datatype		= DK_DATA_TYPE_MOTION_SENSE_STATUS;

	// Motion Type Should be Defined in dk_PeripheralAux.h.
	datatype_payload.motion.data			= ( motion_triggered )? 1: 0;		// No Motion (0)
																				// Motion Detected (1)
																				// Temper Detected (2)

	return hp_send_datatype_data( &datatype_payload, sizeof( hp_datatype_payload ) );
}

int
HPClient_UpdateFWInfo( void )
{
	return hp_send_msg( HP_CMD_CHECK_FW_UPDATE_AVAILABE_REQUEST,
						HP_CMD_CHECK_FW_UPDATE_AVAILABE_REQUEST,
						NULL,
						0 );
}

int
HPClient_DoFWUpgrade( void )
{
	if ( fw_available == dxTRUE ) {
		return hp_send_msg( HP_CMD_ENTER_FW_UPDATE_REQUEST,
							HP_CMD_ENTER_FW_UPDATE_REQUEST,
							NULL,
							0 );
	} else {
		dbg_warm( "FW Not Available!!!" );
		return HP_ERROR;
	}
}

int
HPClient_UpdateLocalTime( void )
{
	return hp_send_msg( HP_CMD_SYS_REPORT_GET_LOCAL_TIME_REQ,
						HP_CMD_SYS_REPORT_GET_LOCAL_TIME_REQ,
						NULL,
						0 );
}

int
HPClient_RegisterCBFuncs( HP_CBFuncs *funcs )
{
	if ( funcs ) {
		hp_cb_funcs	= *funcs;
	}

	return HP_SUCCESS;
}

int
HPClient_RegisterCBUpdateUploadStreamURL( UpdateUploadStreamURL cb )
{
	hp_cb_funcs.update_upload_stream_url	= cb;

	return HP_SUCCESS;
}

int
HPClient_RegisterCBUpdateUploadRecordURL( UpdateUploadRecordURL cb )
{
	hp_cb_funcs.update_upload_record_url	= cb;

	return HP_SUCCESS;
}

int
HPClient_RegisterCBUpdateFWStatus( UpdateFWStatus cb )
{
	hp_cb_funcs.update_fw_status	= cb;

	return HP_SUCCESS;
}

int
HPClient_RegisterCBUpdateLocalTime( UpdateLocalTime cb )
{
	hp_cb_funcs.update_local_time	= cb;

	return HP_SUCCESS;
}

