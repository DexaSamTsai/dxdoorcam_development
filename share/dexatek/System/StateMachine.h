/*!
 *  @file      	StateMachine.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __STATE_MACHINE_H__
# define __STATE_MACHINE_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Camera.h"
# include "Config/Config.h"


//==========================================================================
// Type Define
//==========================================================================
# define EVENT_DURATION			15

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
StateMachine_GetCameraConfig( CFG_CameraCtx *ctx );

extern int
StateMachine_SetCameraConfig( CFG_CameraCtx *ctx );

extern void
StateMachine_Run( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// __STATE_MACHINE_H__
