/*!
 *  @file      	HPClient.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __HP_CLIENT_H__
# define __HP_CLIENT_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "dxOS.h"
# include "dk_Peripheral.h"

//==========================================================================
// Type Define
//==========================================================================
# define HP_SUCCESS							0
# define HP_ERROR							-1

# define HP_MAX_URI_LENGTH					1400

# define DEFAULT_IPCAM_STREAM_CONFIG		"{\"StreamingConfig\":{\"Resolution\":1080,\"FrameRate\":30,\"BitRate\":768}}"
# define DEFAULT_IPCAM_MOTION_CONFIG		"{\"MotionConfig\": {\"Mode\": 0,\"ActiveWindow\": [{\"WinStartX\": 0.00,\"WinStartY\": 0.00,\"WinEndX\": 100.00,\"WinEndY\": 100.00,\"Sensitivity\": 1}]}}"
# define DEFAULT_IPCAM_VIDEO_CONFIG			"{\"VideoConfig\": {\"UpSideDown\":0}}"

typedef enum {
	IPCAM_CONTAINER_STREAMING_CONFIG		= 0,
	IPCAM_CONTAINER_MOTION_CONFIG,
	IPCAM_CONTAINER_MAX,
} IPCAM_CONTAINER;

typedef enum {
	IPCAM_CONTROL_ACCESS_STREAMING_CONFIG		= 0,
	IPCAM_CONTROL_ACCESS_MOTION_CONFIG,
	IPCAM_CONTROL_ACCESS_EVENT_REQUEST,
	IPCAM_CONTROL_ACCESS_CAPTURE_IMAGE,
	IPCAM_CONTROL_ACCESS_DOWN_STREAMING,
	IPCAM_CONTROL_ACCESS_MAX,
} IPCAM_CONTROL_ACCESS;

typedef enum {
	HP_MACHINE_ALARM_SERVER		= 0,
	HP_MACHINE_IPCAM			= 1,
	HP_MACHINE_MAX,
} HP_MACHINE_TYPE;

typedef enum {
	HP_RECORD_TYPE_VIDEO		= 0,
	HP_RECORD_TYPE_IMAGE,
	HP_RECORD_TYPE_MAX,
} HP_RECORD_TYPE;

typedef struct {
	uint8_t			*payload;
	uint16_t		payload_len;
} Response_Payload_t;

typedef struct {
	uint32_t		type;
	uint64_t		id;
	uint16_t		crc;
	dxBOOL			updated;
	dxBOOL			need_value;
	void			( *set_config )	( void *, uint32_t, void * );
	void			*data;
	dxBOOL			is_lvalue;
	void			*default_value;
} Container_DType;

typedef struct {
	dxBOOL				active;
	uint64_t			mid;
	uint32_t			mtype;
	uint16_t			dtypes_cnt;
	Container_DType		*dtypes;
} Container_MType;

typedef struct {
	char				*uuid;
	uint32_t			dtype;
	void				( *action )	( void *, uint32_t, uint32_t, Response_Payload_t * );		// Data, Data Length, Trigger Time, Job Done Response
} Service_DType;

typedef struct {
	char				*uuid;
	uint32_t			mtype;
	uint16_t			dtypes_cnt;
	Service_DType		*dtypes;
} Service_MType;

typedef enum {
	FW_STATUS_NONE		= 0,
	FW_STATUS_AVAILABE,
	FW_STATUS_UPGRADING,
} FW_STATUS;

typedef void ( *UpdateUploadStreamURL )		( char *, uint32_t );						// Streaming URL, URL Length
typedef void ( *UpdateUploadRecordURL )		( uint32_t, char *, char * );				// HP_RECORD_TYPE, Trigger Time, Upload URL, Download URL
typedef void ( *UpdateFWStatus )			( FW_STATUS, uint32_t );					// Status, Progress
typedef void ( *UpdateLocalTime )			( DKTimeLocal_t * );						// DKTimeLocal_t

typedef struct {
	UpdateUploadStreamURL		update_upload_stream_url;
	UpdateUploadRecordURL		update_upload_record_url;
	UpdateFWStatus				update_fw_status;
	UpdateLocalTime				update_local_time;
} HP_CBFuncs;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// APIs
//==========================================================================
extern int
HPClient_Init( Container_MType mcont[ HP_MACHINE_MAX ], Service_MType mservice[ HP_MACHINE_MAX ] );

extern char
HPClient_ServerReady( void );

extern char
HPClient_ServerOnline( void );

extern int
HPClient_GetUploadStreamToken( void );

extern int
HPClient_GetUploadToken( HP_RECORD_TYPE type, uint32_t trigger_time, uint32_t key );

extern int
HPClient_GetUploadVideoToken( char **upload_token, char **download_token, time_t trigger_time );

extern int
HPClient_GetUploadImageToken( char **upload_token, char **download_token, time_t trigger_time );

extern int
HPClient_SendVideoNotification( char *download_token );

extern int
HPClient_SendImageNotification( char *download_token );

extern int
HPClient_SendEventRecordReport( int report_video, int report_image );

extern int
HPClient_SendMotionDetectionReport( BOOL motion_triggered );

extern int
HPClient_UpdateFWInfo( void );

extern int
HPClient_DoFWUpgrade( void );

extern int
HPClient_RegisterCBFuncs( HP_CBFuncs *funcs );

extern int
HPClient_RegisterCBUpdateUploadStreamURL( UpdateUploadStreamURL cb );

extern int
HPClient_RegisterCBUpdateUploadRecordURL( UpdateUploadRecordURL cb );

extern int
HPClient_RegisterCBUpdateFWStatus( UpdateFWStatus cb );

extern int
HPClient_RegisterCBUpdateLocalTime( UpdateLocalTime cb );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif	// __HP_CLIENT_H__
