/*!
 *  @file       StateMachine.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "StateMachine.h"
# include "HPClient.h"
# include "HybridPeripheralManager.h"
# include "dk_Peripheral.h"
# include "dk_PeripheralAux.h"
//# include "UploadTask.h"
//# include "BackupFile.h"
# include "Media.h"
# include "DataManagement.h"
//# include "DataManagement.h"
//# include "FileManager.h"

//==========================================================================
// Type Declaration
//==========================================================================
//# define DYNAMIC_BITRATE_ENABLE

# ifdef TEST_MODE_ENABLE
//#	define TEST_STREAM_UPLOAD
//#	define TEST_OSD
//#	define TEST_PLAY_AUDIO
//#	define TEST_RECORD
//# define TEST_IMAGE_DEFUALT
//#	define TEST_IMAGE
//#	define TEST_MOTION_DETECTION
//#	define TEST_DYNAMIC_BITRATE
# endif

# define FW_PROGRESS_INIT		0xFFFFFFFF

# ifdef TEST_MODE_ENABLE
#	define FW_UPGRADE_START_TIME	( 12 * 60 )
#	define FW_UPGRADE_END_TIME		( 13 * 60 ) + 30
# else
#	define FW_UPGRADE_START_TIME	( 12 * 60 )
#	define FW_UPGRADE_END_TIME		( 13 * 60 ) + 30
# endif

# define SOUND_DETECTTION_VOLUME	40

# define TODO

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================
static CFG_CameraCtx		cfg_camera_ctx;

static IPCamStreamingConfigHeader_t StreamingConfig;
static IPCamMotionConfigHeader_t	MotionConfig;

/*
 *
typedef struct {
	uint32_t		type;
	uint64_t		id;
	uint16_t		crc;
	dxBOOL			updated;
	dxBOOL			need_value;
	void			( *set_config )	( void *, uint32_t, void * );
	void			*data;
	dxBOOL			is_lvalue;
	void			*default_value;
	uint16_t		default_size;
} Container_DType;

 * */

static Container_DType	ipcam_container_dtypes[ IPCAM_CONTAINER_MAX ]	= {
/*	  type												id	crc	updated		need_value	set_config		*data				is_lvalue	*default						default_size	*/
	{ DK_DATA_TYPE_IPCAMERA_STREAMING_CONFIG,			0,	0,	dxFALSE,	dxFALSE,	NULL,			&StreamingConfig,	dxTRUE,		DEFAULT_IPCAM_STREAM_CONFIG, },
	{ DK_DATA_TYPE_IPCAMERA_MOTION_DETECTION_CONFIG,	0,	0,	dxFALSE,	dxFALSE,	NULL,			&MotionConfig,		dxTRUE,		DEFAULT_IPCAM_MOTION_CONFIG, },
};

static Service_DType ipcam_service_dtypes[ IPCAM_CONTROL_ACCESS_MAX ]	= {
/*	  UUID												DType									action_only	*/
	{ DK_CHARACTERISTIC_STREAMING_CONFIG_UUID,			DK_DATA_TYPE_IPCAMERA_STREAMING_CONFIG,			NULL,	},
	{ DK_CHARACTERISTIC_WIN_MOTION_DET_CONFIG_UUID,		DK_DATA_TYPE_IPCAMERA_MOTION_DETECTION_CONFIG,	NULL,	},
	{ DK_CHARACTERISTIC_EVENT_REQUEST_UUID,				0,												NULL,	},
	{ DK_CHARACTERISTIC_CAPTURE_IMAGE_UUID,				0,												NULL,	},
	{ DK_CHARACTERISTIC_DOWN_STREAMING_UUID,			0,												NULL,	},
};

static Container_MType m_containers[ HP_MACHINE_MAX ] = {
	{	// Alarm Server
		dxFALSE,								// active
		0,										// mid
		DK_MASTER_DATA_TYPE_ALARM_CENTRAL_ID,	// mtype
		0,										// dtypes_cnt
		NULL,
	},
	{	// IPCam
		dxTRUE,
		0,
		DK_MASTER_DATA_TYPE_IPCAM_ID,
		IPCAM_CONTAINER_MAX,
		ipcam_container_dtypes,
	},
};

static Service_MType	m_service[ HP_MACHINE_MAX ] = {
	{	// Alarm Server
		NULL,									// uuid
		DK_MASTER_DATA_TYPE_ALARM_CENTRAL_ID,	// mtype
		0,
		NULL,
	},
	{	// IPCam
		DK_SERVICE_IPCAM_UUID,
		DK_MASTER_DATA_TYPE_IPCAM_ID,
		IPCAM_CONTROL_ACCESS_MAX,
		ipcam_service_dtypes,
	},
};

static pthread_mutex_t	mutex = PTHREAD_MUTEX_INITIALIZER;

static DC_WRITER		live_dc;

static u32		live_stream_current_bitrate				= 0;
static u32		live_stream_real_bitrate				= 0;
static u32		live_stream_last_change_bitrate_clock	= 0;

static u32		live_stream_current_total_frames	= 0;
static u32		live_stream_last_total_frames		= 0;
static u32		live_stream_last_video_clock		= 0;

static u32		motion_trigger_clock		= 0;
static u32		sound_detected_clock		= 0;
static BOOL		fw_available				= FALSE;
static u32		fw_upgrade_progress			= FW_PROGRESS_INIT;
static u32		fw_upgrade_end_time			= FW_UPGRADE_END_TIME;

static DKTimeLocal_t local_time	= { 0, };

static char		*down_streaming_url	= NULL;
static u32		upload_streaming_url_request_clock		= 0;

static u8		audio_out_volume	= 80;

# ifdef TEST_MODE_ENABLE

static u8 *jpeg			= NULL;
static u32 jpeg_size	= 0;

# endif

//==========================================================================
// Static Functions
//==========================================================================
static void
test( void )
{
# ifdef TEST_STREAM_UPLOAD
	MediaCenter_StreamCTX	scu_ctx;

static char *test_shm_name	= "0";
	scu_ctx.shm_name	= test_shm_name;
	scu_ctx.port		= 1935;

# if 0
	scu_ctx.host_name	= "192.168.1.23";
	scu_ctx.title		= "9999";
	scu_ctx.url			= "hls?sessiontoken=987654321";
# else
//rtmp://35.201.187.46:1935/live?streampassword=123_aaaaaaaaaaSHA256KEYaaaaaaaaaaaaa/123
//static char *test_host_name	= "35.189.169.130";
static char *test_host_name	= "35.201.137.157";
static char *test_title		= "123";
static char *test_url		= "live?streampassword=123_aaaaaaaaaaSHA256KEYaaaaaaaaaaaaa";
	//rtmp://35.189.169.130:1935/live?streampassword=157036_01dafea5b3cf90713b3b09cf868c5bd0779f2a56e7585f7db4a4eebfa85b769e
	//scu_ctx.host_name	= "54.183.83.178";
	scu_ctx.port		= 1935;
	scu_ctx.host_name	= test_host_name;
	scu_ctx.title		= test_title;
	scu_ctx.url			= test_url;
# endif

	scu_ctx.username	= NULL;
	scu_ctx.password	= NULL;

	MediaCenter_StartUpload( 0, &scu_ctx );
# endif

# ifdef TEST_OSD
	OsdCtx		osd_ctx;
	char osd[ 64 ];

	memset( &osd_ctx, 0, sizeof( OsdCtx ) );

	osd_ctx.show_date	= 1;
	osd_ctx.show_time	= 1;
	osd_ctx.show_string = 1;

	osd_ctx.string		= ( u8 * )osd;
	osd_ctx.len = sprintf( osd, "12345678901234567890" );

	Camera_ShowOSD( &osd_ctx );
# endif

# ifdef TEST_PLAY_AUDIO
	Camera_SetAudioOutVolume( 100 );
# endif

# ifdef TEST_RECORD
	MediaCenter_RecordCTX ctx;

	ctx.fps			= 1;
	ctx.target_time	= time( NULL );
	ctx.pre_secs	= 5;
	ctx.post_secs	= 5;

	MediaCenter_StartRecord( MC_UPLOAD_TYPE_VIDEO, &ctx );
# endif

# ifdef TEST_IMAGE_DEFUALT
        CFG_CameraCtx camCtx;
        ImageCtx isp;
        CFG_LoadDefault( &camCtx );
        rts_cfg_to_isp( &camCtx.image, &isp);
        Camera_SetImage( &isp, 1 );
# endif

# ifdef TEST_IMAGE
	ImageCtx isp;

	Camera_GetImage( &isp );

//	Camera_SetImage( &isp, 1 );
# endif

# ifdef TEST_MOTION_DETECTION

    int i;

    MDCtx		md_ctx;

    for ( i = 0; i < MAX_MD_SUPPORTED; i ++ ) {
        md_ctx.enable			= 1;
        md_ctx.x				= i * 64;
        md_ctx.y				= i * 36;
        md_ctx.width			= 64;
        md_ctx.height			= 36;
        md_ctx.sensitivity		= 100;
        md_ctx.percentage		= 0; //No used

        Camera_SetMD( i, &md_ctx );
    }
# endif

# ifdef TEST_PLAY_AUDIO
	MediaCenter_StreamCTX scd_ctx;

	MediaCenter_StopDownload();

	scd_ctx.shm_name	= "0";
	scd_ctx.port		= 1935;

	scd_ctx.host_name	= "192.168.1.27";
	scd_ctx.title		= "0";
	scd_ctx.url			= "live";

	scd_ctx.username	= NULL;
	scd_ctx.password	= NULL;

	MediaCenter_StartDownload( &scd_ctx );

# endif

	dbg_error( "Start Test...." );
}

static void
record_file_update()
{
# ifdef TODO
	dbg_error( "TODO..." );
# else
	if ( UploadTask_ExistTask() ) {
		UploadTask_Update();
	} else if ( HPClient_ServerReady() ) {
# ifndef TEST_RESERVE_VIDEO
		BackupFile_Info info;

		if ( BackupFile_FileGet( &info ) == SUCCESS ) {
			UploadTask_NewUploadBackupFile( info.path, info.create_time );
		}
# endif
	}
# endif
}

static int
streaming_set_url( MediaCenter_StreamCTX *ctx, void *data, uint32_t len )
{
	char *proto, *addr, *url, *title, *port = NULL;

	proto	= ( char * )data;
	addr	= strstr( proto, "://" );
	if ( addr == NULL ) {
		dbg_error( "URL Format Error( %s )!!!", proto );
		return FAILURE;
	}

	*addr	= 0;
	addr += 3;

	url	= strstr( addr, "/" );
	if ( url == NULL ) {
		dbg_error( "URL Format Error( %s )!!!", proto );
		return FAILURE;
	}

	*url	= 0;
	url ++;

	title	= strstr( url, "/" );
	if ( title == NULL ) {
		dbg_error( "URL Format Error( %s )!!!", proto );
		return FAILURE;
	}

	*title	= 0;
	title ++;

	port	= strstr( addr, ":" );
	if ( port ) {
		*port	= 0;
		port ++;
	}

	ctx->shm_name	= "0";

	if ( port ) {
		ctx->port		= atoi( port );
	} else {
		ctx->port		= 1935;
	}

	ctx->host_name	= addr;
	ctx->title		= title;
	ctx->url		= url;

	ctx->username	= NULL;
	ctx->password	= NULL;

	return SUCCESS;
}

static u32
stream_out_get_higher_bitrate( u32 bitrate )
{
	if ( bitrate == 256 ) {
		return 512;
	} else if ( bitrate == 512 ) {
		return 768;
	} else {
		return 1024;
	}
}

static u32
stream_out_get_lower_bitrate( u32 bitrate )
{
	if ( bitrate == 1024 ) {
		return 768;
	} else if ( bitrate == 768 ) {
		return 512;
	} else {
		return 256;
	}
}

static void
stream_out_monitor( u32 curr_clock )
{
	MC_STREAM_OUT_STATUS status = MediaCenter_StreamOutStatus();

	u32		new_bitrate = live_stream_current_bitrate;

	if ( status == MC_STREAM_OUT_STATUS_ERROR ) {
		dbg_error( "======================================" );
		dbg_error( "Codec Error...." );
		dbg_error( "Reboot System...." );
		dbg_error( "======================================" );

//# ifdef TEST_MODE_ENABLE
//		exit( 0 );
//# else
//		system( "reboot" );
//# endif
	} else if ( status == MC_STREAM_OUT_STATUS_DELAY ) {
		if ( curr_clock > live_stream_last_change_bitrate_clock + 5 ) {
			new_bitrate	= stream_out_get_lower_bitrate( live_stream_current_bitrate );
		}
	} else if ( status == MC_STREAM_OUT_STATUS_NORMAL ) {
# ifdef TEST_DYNAMIC_BITRATE
		int b[ 4 ] = { 1024, 768, 512, 256, };

		new_bitrate = b[ ( ticks() & 0x03 ) ];
# else
		new_bitrate	= stream_out_get_lower_bitrate( live_stream_real_bitrate );

		if ( live_stream_current_bitrate == new_bitrate ) {
			if ( motion_trigger_clock ) {
				if ( curr_clock > ( live_stream_last_change_bitrate_clock + 5 ) ) {
					new_bitrate	= live_stream_real_bitrate;
				}
			} else if ( sound_detected_clock ) {
				if ( curr_clock > ( live_stream_last_change_bitrate_clock + 5 ) ) {
					new_bitrate	= live_stream_real_bitrate;
				}
			}
		} else if ( live_stream_current_bitrate > new_bitrate ) {
			if ( motion_trigger_clock ) {
				new_bitrate	= live_stream_current_bitrate;
			} else if ( sound_detected_clock ) {
				new_bitrate	= live_stream_current_bitrate;
			}
		} else {
			if ( motion_trigger_clock ) {
				if ( curr_clock > ( live_stream_last_change_bitrate_clock + 5 ) ) {
					new_bitrate	= stream_out_get_higher_bitrate( live_stream_current_bitrate );
				}
			} else if ( sound_detected_clock ) {
				if ( curr_clock > ( live_stream_last_change_bitrate_clock + 5 ) ) {
					new_bitrate	= stream_out_get_higher_bitrate( live_stream_current_bitrate );
				}
			} else if ( curr_clock > ( live_stream_last_change_bitrate_clock + 60 ) ) {
				new_bitrate	= stream_out_get_higher_bitrate( live_stream_current_bitrate );
			} else {
				new_bitrate	= live_stream_current_bitrate;
			}
		}
# endif
	}

# ifdef DYNAMIC_BITRATE_ENABLE
	if ( new_bitrate != live_stream_current_bitrate ) {
		live_stream_current_bitrate				= new_bitrate;
		live_stream_last_change_bitrate_clock	= curr_clock;

		if ( Camera_SetVideoBitrate( CAMERA_STREAM_LIVE, live_stream_current_bitrate ) ) {
			dbg_warm( "Do Camera_SetVideoBitrate( CAMERA_STREAM_LIVE ) Failed!!!" );
		}

		dbg_warm( "Change Bitrate to %d Kbps", new_bitrate );
	}
# endif
}

static void
rts_isp_to_cfg( ImageCtx *isp, CFG_Image *cfg )
{
	cfg->blc.mode				= CAMERA_SWITCH_OFF;
	cfg->blc.level				= isp->blc;
	cfg->ir_cut.mode			= isp->ir_mode;
	cfg->power_freq				= isp->power_freq;

# define FIELD_TO_CFG( field )		cfg->field	= isp->field
	FIELD_TO_CFG( dnr3 );
	FIELD_TO_CFG( dehaze );

# define COLOR_TO_CFG( field )		cfg->color.field	= isp->field
	COLOR_TO_CFG( brightness );
	COLOR_TO_CFG( contrast );
	COLOR_TO_CFG( hue );
	COLOR_TO_CFG( saturation );
	COLOR_TO_CFG( sharpness );
	COLOR_TO_CFG( gamma );
	COLOR_TO_CFG( gray_mode );

# define PIC_TO_CFG( field )		cfg->pic.field	= isp->field
	PIC_TO_CFG( roll );
	PIC_TO_CFG( flip );
	PIC_TO_CFG( mirror );
	PIC_TO_CFG( rotate );

# define AWB_TO_CFG( field )		cfg->awb.field	= isp->awb.field
	AWB_TO_CFG( mode );
	AWB_TO_CFG( level );
	AWB_TO_CFG( r_gain );
	AWB_TO_CFG( g_gain );
	AWB_TO_CFG( b_gain );

# define WDR_TO_CFG( field )		cfg->wdr.field	= isp->wdr.field
	WDR_TO_CFG( mode );
	WDR_TO_CFG( level );

# define AE_TO_CFG( field )		cfg->ae.field	= isp->ae.field
	AE_TO_CFG( mode );
	AE_TO_CFG( gain );
	AE_TO_CFG( exposure_time );
}

static void
rts_cfg_to_isp( CFG_Image *cfg, ImageCtx *isp )
{
	isp->blc				= cfg->blc.level;
	isp->ir_mode			= cfg->ir_cut.mode;
	isp->power_freq			= cfg->power_freq;

# define FIELD_TO_ISP( field )		isp->field	= cfg->field
	FIELD_TO_ISP( dnr3 );
	FIELD_TO_ISP( dehaze );

# define COLOR_TO_ISP( field )		isp->field	= cfg->color.field
	COLOR_TO_ISP( brightness );
	COLOR_TO_ISP( contrast );
	COLOR_TO_ISP( hue );
	COLOR_TO_ISP( saturation );
	COLOR_TO_ISP( sharpness );
	COLOR_TO_ISP( gamma );
	COLOR_TO_ISP( gray_mode );

# define PIC_TO_ISP( field )		isp->field	= cfg->pic.field
	PIC_TO_ISP( roll );
	PIC_TO_ISP( flip );
	PIC_TO_ISP( mirror );
	PIC_TO_ISP( rotate );

# define AWB_TO_ISP( field )		isp->awb.field	= cfg->awb.field
	AWB_TO_ISP( mode );
	AWB_TO_ISP( level );
	AWB_TO_ISP( r_gain );
	AWB_TO_ISP( g_gain );
	AWB_TO_ISP( b_gain );

# define WDR_TO_ISP( field )		isp->wdr.field	= cfg->wdr.field
	WDR_TO_ISP( mode );
	WDR_TO_ISP( level );

# define AE_TO_ISP( field )			isp->ae.field	= cfg->ae.field
	AE_TO_ISP( mode );
	AE_TO_ISP( gain );
	AE_TO_ISP( exposure_time );
}

static void
rts_isp_hardcode( ImageCtx *isp )
{
	isp->blc				= 1;

	isp->brightness			= 10;
	isp->saturation			= 45;

	isp->ae.mode			= 0;
	isp->ae.gain			= 256;
	isp->ae.exposure_time	= 200;

	isp->wdr.mode			= 1;        // 0: Disable, 1: Manual, 2: Auto, Def: 0
	isp->wdr.level			= 70;		// 0 ~ 100
}

static int
ipcam_set_video_stream( u32 id, CFG_Stream *stream )
{
	CodecCtx	ctx;

	ctx.width	= stream->w;
	ctx.height	= stream->h;
	ctx.kbps	= stream->bitrate;
	ctx.fps		= stream->fps;

	if ( id == CAMERA_STREAM_LIVE ) {
# ifdef DYNAMIC_BITRATE_ENABLE
		ctx.kbps	= live_stream_current_bitrate;
		dbg_error( "stream->bitrate: %u", stream->bitrate );
		dbg_error( "ctx.kbps: %u", ctx.kbps );
# else
		ctx.kbps	= 256;
# endif
	}

	return Camera_SetVideoCodec( id, &ctx );
}

static int
ipcam_set_motion_detection( u32 id, CFG_MD *md )
{
# ifdef TEST_MOTION_DETECTION
	return SUCCESS;
# else
	MDCtx		md_ctx;

	md_ctx.enable			= md->area[ id ].enable;
	md_ctx.x				= md->area[ id ].x;
	md_ctx.y				= md->area[ id ].y;
	md_ctx.width			= md->area[ id ].width;
	md_ctx.height			= md->area[ id ].height;
	md_ctx.percentage		= md->area[ id ].percentage;
	md_ctx.sensitivity		= md->sensitivity;

	return Camera_SetMD( id, &md_ctx );
# endif
}

static int
ipcam_setup_camera( void )
{
	int i;

	ImageCtx	img_ctx;
	MaskCtx		mask_ctx;
	OsdCtx		osd_ctx;

	// Video Settings
	if ( ipcam_set_video_stream( CAMERA_STREAM_VIDEO_H264_1, &cfg_camera_ctx.video.live ) ) {
		dbg_warm( "Do ipcam_set_video_stream( CAMERA_STREAM_VIDEO_H264_1 ) Failed!!!" );
		return FAILURE;
	} else if ( ipcam_set_video_stream( CAMERA_STREAM_VIDEO_H264_2, &cfg_camera_ctx.video.recording ) ) {
		dbg_warm( "Do ipcam_set_video_stream( CAMERA_STREAM_VIDEO_H264_2 ) Failed!!!" );
		return FAILURE;
	} else if ( ipcam_set_video_stream( CAMERA_STREAM_VIDEO_MJPEG, &cfg_camera_ctx.video.snapshot ) ) {
		dbg_warm( "Do ipcam_set_video_stream( CAMERA_STREAM_VIDEO_MJPEG ) Failed!!!" );
		return FAILURE;
	}

	// Audio Settings
	Camera_EnableAudio( cfg_camera_ctx.audio.enable );
	Camera_SetAudioInVolume( cfg_camera_ctx.audio.volume );
	//Camera_SetAudioInVolume( 90 );

	// Image Settings
	Camera_GetImage( &img_ctx );
	rts_cfg_to_isp( &cfg_camera_ctx.image, &img_ctx );
	rts_isp_hardcode( &img_ctx );
	Camera_SetImage( &img_ctx, 1 );

	// Motion Detection Settings
	for ( i = 0; i < MAX_MD_SUPPORTED; i ++ ) {
		ipcam_set_motion_detection( i, &cfg_camera_ctx.md );
	}

	// Mask Settings
	for ( i = 0; i < MAX_MASK_SUPPORTED; i ++ ) {
		mask_ctx.enable			= cfg_camera_ctx.mask[ i ].enable;
		mask_ctx.x				= cfg_camera_ctx.mask[ i ].x;
		mask_ctx.y				= cfg_camera_ctx.mask[ i ].y;
		mask_ctx.width			= cfg_camera_ctx.mask[ i ].width;
		mask_ctx.height			= cfg_camera_ctx.mask[ i ].height;

		Camera_SetMask( i, &mask_ctx );
	}

	// OSD Settings
	osd_ctx.show_date	= cfg_camera_ctx.osd.show_date;
	osd_ctx.show_time	= cfg_camera_ctx.osd.show_time;
	osd_ctx.show_string = cfg_camera_ctx.osd.show_text;
	osd_ctx.string		= ( u8 * )cfg_camera_ctx.osd.string;

	Camera_ShowOSD( &osd_ctx ) ;

	return SUCCESS;
}

static void
ipcam_load_default_image( void )
{
	ImageCtx img_ctx;

	Camera_GetImage( &img_ctx );
	CFG_LoadDefault( &cfg_camera_ctx );

	rts_isp_to_cfg( &img_ctx, &cfg_camera_ctx.image );
}

static void
ipcam_cfg_adjust( void )
{
	int i;

	CFG_MD	*md = &cfg_camera_ctx.md;

	if ( md->sensitivity != CAMERA_MD_SENSITIVITY ) {
		md->sensitivity = CAMERA_MD_SENSITIVITY;
	}

	for ( i = 0; i < MAX_MD_SUPPORTED; i ++ ) {
		if ( md->area[ i ].enable == TRUE ) {
			if ( md->area[ i ].x >= CAMERA_MD_AREA_WIDTH_MAX ) {
				md->area[ i ].x	= 0;
			}

			if ( md->area[ i ].y >= CAMERA_MD_AREA_HEIGHT_MAX ) {
				md->area[ i ].y	= 0;
			}

			if ( ( md->area[ i ].width == 0 ) ||
				 ( md->area[ i ].width >= CAMERA_MD_AREA_WIDTH_MAX ) ) {
				md->area[ i ].width	= CAMERA_MD_AREA_WIDTH_MAX;
			}

			if ( ( md->area[ i ].height == 0 ) ||
				 ( md->area[ i ].height >= CAMERA_MD_AREA_HEIGHT_MAX ) ) {
				md->area[ i ].height	= CAMERA_MD_AREA_HEIGHT_MAX;
			}

			if ( ( md->area[ i ].percentage != CAMERA_MD_PERCENTAGE_LOW ) &&
				 ( md->area[ i ].percentage != CAMERA_MD_PERCENTAGE_MIDDLE ) &&
				 ( md->area[ i ].percentage != CAMERA_MD_PERCENTAGE_HIGH ) ) {
				md->area[ i ].percentage	= CAMERA_MD_PERCENTAGE_MIDDLE;
			}
		}
	}
}

static void
down_streaming_stop( char * new_url )
{
	BOOL stop = FALSE;

	pthread_mutex_lock( &mutex );

	if ( down_streaming_url ) {
		if ( strcmp( down_streaming_url, new_url ) == 0 ) {
			my_free( down_streaming_url );
			down_streaming_url	= NULL;
			stop = TRUE;
		}
	}

	pthread_mutex_unlock( &mutex );

	if ( stop == TRUE ) {
		dbg_warm( "MediaCenter_StopDownload" );
		MediaCenter_StopDownload();
		Camera_SetAudioOutVolume( 0 );
	}
}

static void
down_streaming_start( char * new_url, u32 new_url_len )
{
	pthread_mutex_lock( &mutex );
	if ( down_streaming_url && ( strcmp( down_streaming_url, new_url ) == 0 ) ) {
		pthread_mutex_unlock( &mutex );
		return;
	}
	pthread_mutex_unlock( &mutex );

	char *url = my_strdup( new_url );

	if ( url ) {
		MediaCenter_StreamCTX scu_ctx;

		if ( streaming_set_url( &scu_ctx, new_url, new_url_len ) == SUCCESS ) {
			MediaCenter_StopDownload();

			pthread_mutex_lock( &mutex );

			if ( down_streaming_url ) {
				my_free( down_streaming_url );
			}
			down_streaming_url	= url;

			pthread_mutex_unlock( &mutex );

			dbg_warm( "MediaCenter_StartDownload" );
			Camera_SetAudioOutVolume( audio_out_volume );
			MediaCenter_StartDownload( &scu_ctx );
		} else {
			my_free( url );
		}
	} else {
		dbg_error( "Do my_strdup Failed!!!" );
	}
}

static void
container_handle_mapping_stream_config()
{
static const struct {
	uint16_t			width;
	uint16_t			height;
} map_resolution[ 2 ] = {
	{ 1920, 1080 },
	{ 1280, 720 },
};

static const uint32_t map_bitrate[ 4 ] = { 256, 512, 768, 1024, };
static const uint32_t map_fps[ 2 ] = { 15, 30, };

	cfg_camera_ctx.video.live.bitrate	= map_bitrate[ StreamingConfig.Bitrate ];
	cfg_camera_ctx.video.live.fps		= map_fps[ StreamingConfig.FPS ];
	cfg_camera_ctx.video.live.gop		= map_fps[ StreamingConfig.FPS ];
	cfg_camera_ctx.video.live.w			= map_resolution[ StreamingConfig.Resolution ].width;
	cfg_camera_ctx.video.live.h			= map_resolution[ StreamingConfig.Resolution ].height;
	cfg_camera_ctx.video.live.crc		= ipcam_container_dtypes[ IPCAM_CONTAINER_STREAMING_CONFIG ].crc;
}

static void
container_handle_mapping_motion_config( void )
{
	memset( &cfg_camera_ctx.md, 0, sizeof( CFG_MD ) );

	if ( MotionConfig.Mode == IPCAM_MOTION_MODE_ON ) {
		int i, cnt = MotionConfig.ActiveWindowLen;

		cfg_camera_ctx.md.sensitivity	= CAMERA_MD_SENSITIVITY;

		if ( cnt > MAX_MD_SUPPORTED ) {
			cnt = MAX_MD_SUPPORTED;
		}

		for ( i = 0; i < cnt; i ++ ) {
			cfg_camera_ctx.md.area[ i ].enable		= TRUE;
			cfg_camera_ctx.md.area[ i ].x			= MotionConfig.ActiveWindow[ i ].WinStartX;
			cfg_camera_ctx.md.area[ i ].y			= MotionConfig.ActiveWindow[ i ].WinStartY;
			cfg_camera_ctx.md.area[ i ].width		= MotionConfig.ActiveWindow[ i ].WinEndX - MotionConfig.ActiveWindow[ i ].WinStartX;
			cfg_camera_ctx.md.area[ i ].height		= MotionConfig.ActiveWindow[ i ].WinEndY - MotionConfig.ActiveWindow[ i ].WinStartY;

			switch ( MotionConfig.ActiveWindow[ i ].Sensitivity ) {
				case IPCAM_MOTION_SENSITIVITY_HIGH:
					cfg_camera_ctx.md.area[ i ].percentage	= CAMERA_MD_PERCENTAGE_HIGH;
					break;

				case IPCAM_MOTION_SENSITIVITY_MIDDLE:
					cfg_camera_ctx.md.area[ i ].percentage	= CAMERA_MD_PERCENTAGE_MIDDLE;
					break;

				case IPCAM_MOTION_SENSITIVITY_LOW:
				default:
					cfg_camera_ctx.md.area[ i ].percentage	= CAMERA_MD_PERCENTAGE_LOW;
					break;
			}
		}
	}

	cfg_camera_ctx.md_crc	= ipcam_container_dtypes[ IPCAM_CONTAINER_MOTION_CONFIG ].crc;
}

static void
container_handle_set_event_request( void *data, uint32_t len, uint32_t trigger_time, Response_Payload_t *response )
{
# ifdef TODO
	dbg_error( "TODO..." );
# else
	// Do Parser
	IPCamEventRequestHeader_t EventRequest	= IPCam_ParseEventRequestPackedPayload( len, data );

	if ( EventRequest.VideoEvt.Avail ) {
		MediaCenter_RecordCTX	ctx = { 0 };

		dbg_line( "EventRequest.VideoEvt.Avail" );

		ctx.fps				= cfg_camera_ctx.video.recording.fps;
		ctx.target_time		= trigger_time;
		ctx.pre_secs		= EventRequest.VideoEvt.PreRecordDuration;
		ctx.post_secs		= EventRequest.VideoEvt.PostRecordDuration;

		if ( UploadTask_Create( MC_UPLOAD_TYPE_VIDEO, &ctx ) == FAILURE ) {
			dbg_warm( "Do UploadTask_Create Failed!!!" );
		}
	}

	if ( EventRequest.JPEGEvt.Avail ) {
		MediaCenter_RecordCTX	ctx = { 0 };

		dbg_line( "EventRequest.JPEGEvt.Avail" );

		ctx.fps				= cfg_camera_ctx.video.snapshot.fps;
		ctx.target_time		= trigger_time;
		ctx.pre_secs		= EventRequest.JPEGEvt.PreRecordDuration;
		ctx.post_secs		= EventRequest.JPEGEvt.PostRecordDuration;

		if ( UploadTask_Create( MC_UPLOAD_TYPE_IMAGE, &ctx ) == FAILURE ) {
			dbg_warm( "Do UploadTask_Create Failed!!!" );
		}
	}
# endif
}

static void
container_handle_capture_image( void *data, uint32_t len, uint32_t trigger_time, Response_Payload_t *response )
{
	if ( response ) {
		IPCamCaptureImage_t			CaptureImage	= IPCam_ParseCaptureImagePackedPayload( len, data );
		GenericPackedPayloadAlloc_t	PacketPayload;

		AVFrame frame	= { 0, };

		if ( CaptureImage.Image ) {
			free( CaptureImage.Image );
		}

		if ( Snapshot_GetFrame( &frame ) == FAILURE ) {
			dbg_error( "Do Snapshot_GetFrame Failed!!!" );
		}

		CaptureImage.Resolution		= IPCAM_RESOLUTION_480;
		CaptureImage.Format			= IPCAM_CAPTURE_IMAGE_FORMAT_JPEG;
	    CaptureImage.Image			= frame.buff;
	    CaptureImage.ImageSize		= frame.size;

		PacketPayload		= IPCam_ConvertCaptureImagePackedPayload( CaptureImage );

		response->payload		= PacketPayload.pPackedPayload;
		response->payload_len	= PacketPayload.PackedPayloadLen;
	}
}

static void
container_handle_down_streaming( void *data, uint32_t len, uint32_t trigger_time, Response_Payload_t *response )
{
	// Todo
	dbg_error( "Todo..." );
# if 0
	IPCamDownStreaming_t DownStreaming	= IPCam_ParseDownStreamingPackedPayload( len, data );

	if ( DownStreaming.Url ) {
		if ( DownStreaming.Action == IPCAM_DOWN_STREAMING_ACTION_STOP ) {
			dbg_warm( "IPCAM_DOWN_STREAMING_ACTION_STOP" );
			down_streaming_stop( DownStreaming.Url );
		} else if ( DownStreaming.Action == IPCAM_DOWN_STREAMING_ACTION_PLAY ) {
			dbg_warm( "IPCAM_DOWN_STREAMING_ACTION_PLAY" );
			down_streaming_start( DownStreaming.Url, DownStreaming.UrlLength );
		}

		free( DownStreaming.Url );
	}
# endif
}

static void
container_handle_set_streaming_config( void *data, uint32_t len, void *value )
{
	dbg_line( "%s", ( char * )data );
	*( IPCamStreamingConfigHeader_t * )value	= IPCam_ParseStreamingConfigPackedPayload( len, data );
}

static void
container_handle_set_motion_config( void *data, uint32_t len, void *value )
{
	dbg_line( "%s", ( char * )data );
	*( IPCamMotionConfigHeader_t * )value	= IPCam_ParseMotionConfigPackedPayload( len, data );
}

static void
container_handle_set_video_config( void *data, uint32_t len, void *value )
{
	dbg_line( "%s", ( char * )data );
	*( IPCamVideoConfigHeader_t * )value	= IPCam_ParseVideoConfigPackedPayload( len, data );
}

static void
container_handle_init( void )
{
	ipcam_container_dtypes[ IPCAM_CONTAINER_STREAMING_CONFIG ].crc	= cfg_camera_ctx.video.live.crc;
	ipcam_container_dtypes[ IPCAM_CONTAINER_MOTION_CONFIG ].crc		= cfg_camera_ctx.md_crc;

	// Assign Action Function to the Control Access of Event Request
	m_service[ HP_MACHINE_IPCAM ].dtypes[ IPCAM_CONTROL_ACCESS_EVENT_REQUEST ].action		= container_handle_set_event_request;
	m_service[ HP_MACHINE_IPCAM ].dtypes[ IPCAM_CONTROL_ACCESS_CAPTURE_IMAGE ].action		= container_handle_capture_image;
	m_service[ HP_MACHINE_IPCAM ].dtypes[ IPCAM_CONTROL_ACCESS_DOWN_STREAMING ].action		= container_handle_down_streaming;

	// Assign Configuration Setting Function
	m_containers[ HP_MACHINE_IPCAM ].dtypes[ IPCAM_CONTAINER_STREAMING_CONFIG ].set_config	= container_handle_set_streaming_config;
	m_containers[ HP_MACHINE_IPCAM ].dtypes[ IPCAM_CONTAINER_MOTION_CONFIG ].set_config		= container_handle_set_motion_config;
}

static void
container_handle_update( void )
{
	BOOL cfg_update	= FALSE;

	if ( ipcam_container_dtypes[ IPCAM_CONTAINER_STREAMING_CONFIG ].updated == TRUE ) {
		container_handle_mapping_stream_config();

		if ( live_stream_real_bitrate != cfg_camera_ctx.video.live.bitrate ) {
			live_stream_real_bitrate				= cfg_camera_ctx.video.live.bitrate;
			live_stream_current_bitrate				= stream_out_get_lower_bitrate( live_stream_real_bitrate );
			live_stream_last_change_bitrate_clock	= seconds();
		}

		if ( ipcam_set_video_stream( CAMERA_STREAM_LIVE, &cfg_camera_ctx.video.live ) ) {
			dbg_warm( "Do ipcam_set_video_stream( CAMERA_STREAM_LIVE ) Failed!!!" );
		}
		ipcam_container_dtypes[ IPCAM_CONTAINER_STREAMING_CONFIG ].updated	= FALSE;
		cfg_update	= TRUE;
	}

	if ( ipcam_container_dtypes[ IPCAM_CONTAINER_MOTION_CONFIG ].updated == TRUE ) {
		int i;

		container_handle_mapping_motion_config();

		for ( i = 0; i < MAX_MD_SUPPORTED; i ++ ) {
			if ( ipcam_set_motion_detection( i, &cfg_camera_ctx.md ) ) {
				dbg_warm( "Do ipcam_set_motion_detection( %d ) Failed!!!", i );
			}
		}

		ipcam_container_dtypes[ IPCAM_CONTAINER_MOTION_CONFIG ].updated		= FALSE;
		cfg_update	= TRUE;
	}

	if ( cfg_update ) {
		if ( CFG_Save( &cfg_camera_ctx ) == FAILURE ) {
			dbg_error( "Do CFG_Save Failed !!!" );
		}
	}
}

static int
md_cb( u32 mid, void *data, int size )
{
	if ( mid >= CAMERA_MD_MAX ) {
		return FAILURE;
	}

	if ( local_time.year == 0 ) {
		return SUCCESS;
	}

	if ( motion_trigger_clock == 0 ) {
		if ( HPClient_SendMotionDetectionReport( TRUE ) == HP_ERROR ) {
			return SUCCESS;
		}

		dbg_r( "Motion Detected( %d )", mid );
	}

	motion_trigger_clock	= seconds();

	return SUCCESS;
}

static int
stream_in_cb( u32 sid, AVFrame *frame )
{
static int key_frame_in = 0;

	if ( frame == NULL ) {
		return FAILURE;
	}

	gettimeofday( &frame->ptime, NULL );

	if ( sid == CAMERA_STREAM_VIDEO_MJPEG ) {
		if ( key_frame_in ) {
			Snapshot_FrameIn( frame );
			key_frame_in = 0;
		}
	} else if ( sid == CAMERA_STREAM_AUDIO ) {
		DC_WriteFrame( live_dc, frame );
		Record_FrameIn( frame );
	} else {
		if ( sid == CAMERA_STREAM_RECORD ) {
			Record_FrameIn( frame );
		}

		if ( sid == CAMERA_STREAM_LIVE ) {
			live_stream_current_total_frames ++;
			live_stream_last_video_clock	= seconds();
			DC_WriteFrame( live_dc, frame );

			if ( frame->iskey == TRUE ) {
				key_frame_in	= 1;
			}
		}
	}

# if 0

static int frame_cnt_h2641	= 0;
static u32 frame_size_h2641	= 0;
static u32 frame_time_h2641	= 0;
static int frame_cnt_h2642	= 0;
static u32 frame_size_h2642	= 0;
static u32 frame_time_h2642	= 0;
static int frame_cnt_jpeg	= 0;
static time_t jpeg_time		= 0;
static u32 frame_curr_time	= 0;

static int frame_cnt_audio	= 0;
static u32 frame_size_audio	= 0;

	frame_curr_time = ticks;

	if ( sid == CAMERA_STREAM_VIDEO_MJPEG ) {
		frame_cnt_jpeg ++;

		if ( jpeg_time != frame->ptime.tv_sec ) {
			dbg_line( "JPeG: frame: %d: frame->ptime.tv_sec: %lu", frame_cnt_jpeg, frame->ptime.tv_sec );
			frame_cnt_jpeg	= 0;
			jpeg_time		= frame->ptime.tv_sec;
		}
	} else if ( sid == CAMERA_STREAM_RECORD ) {
		frame_cnt_h2642 ++;
		frame_size_h2642 += frame->size;

		if ( frame->iskey ) {
			dbg_line( "Stream2: frame: %d, size: %u, frame->ptime.tv_sec: %lu, time: %d", frame_cnt_h2642, frame_size_h2642, frame->ptime.tv_sec, ( frame_curr_time - frame_time_h2642 ) );
			frame_cnt_h2642		= 0;
			frame_size_h2642	= 0;
			frame_time_h2642	= frame_curr_time;
		}
	} else if ( sid == CAMERA_STREAM_LIVE ) {
		frame_cnt_h2641 ++;
		frame_size_h2641 += frame->size;

		if ( frame->iskey ) {
			dbg_line( "Stream1: frame: %d, size: %u, frame->ptime.tv_sec: %lu, time: %d", frame_cnt_h2641, frame_size_h2641, frame->ptime.tv_sec, ( frame_curr_time - frame_time_h2641 ) );
			frame_cnt_h2641		= 0;
			frame_size_h2641	= 0;
			frame_time_h2641	= frame_curr_time;

			dbg_line( "Audio: frame: %d, size: %u", frame_cnt_audio, frame_size_audio );
			frame_cnt_audio		= 0;
			frame_size_audio	= 0;
		}
	} else if ( sid == CAMERA_STREAM_AUDIO ) {
		frame_cnt_audio ++;
		frame_size_audio += frame->size;
	}

# endif

	return SUCCESS;
}

static void
stream_in_monitor( u32 curr_clock )
{
	if ( live_stream_last_total_frames == live_stream_current_total_frames ) {
		if ( curr_clock > ( live_stream_last_video_clock + 2 ) ) {
			dbg_error( "======================================" );
			dbg_error( "Codec Error...." );
//			dbg_error( "Reboot System...." );
			dbg_error( "======================================" );

//# ifdef TEST_MODE_ENABLE
//			exit( 0 );
//# else
//			system( "reboot" );
//# endif
		}
	}

	live_stream_last_total_frames	= live_stream_current_total_frames;
}

static int
audio_cb( u32 db )
{
	if ( db >= SOUND_DETECTTION_VOLUME ) {
		//sound_detected_clock	= seconds();
		//dbg_y( "Sound Detected( %d DB ) at %s", db, ctime( &sound_detected_clock ) );
	}

	return 0;
}

static void
upload_stream_url_update( char *stream_url, uint32_t url_len )
{
	if ( stream_url && url_len) {
		MediaCenter_StreamCTX scu_ctx;

dbg_warm( "stream_url: %s", stream_url );
		sprintf( cfg_camera_ctx.video.live.url, "%s", stream_url );
dbg_warm( "cfg_camera_ctx.video.live.url: %s", cfg_camera_ctx.video.live.url );
		if ( CFG_Save( &cfg_camera_ctx ) == FAILURE ) {
			dbg_error( "Do CFG_Save Failed !!!" );
		}

dbg_error( "streaming_set_url" );
		if ( streaming_set_url( &scu_ctx, stream_url, url_len ) == SUCCESS ) {
dbg_error( "streaming_set_url" );
			MediaCenter_StartUpload( 0, &scu_ctx );
		}

		free( stream_url );
	}

	upload_streaming_url_request_clock = 0;
}

static void
fw_status_update( FW_STATUS status, uint32_t progress )
{
	if ( status == FW_STATUS_AVAILABE ) {
		dbg_warm( "FW Availabe" );
		fw_available	= TRUE;
	} else if ( status == FW_STATUS_UPGRADING ) {
		dbg_warm( "Upgrade FW: %d %%", progress );
		fw_upgrade_progress	= progress;
	}
}

static void
local_time_update( DKTimeLocal_t *ltime )
{
	if ( ltime ) {
		local_time	= *ltime;

		dbg_line( "%04d/%02d/%02d %02d:%02d:%02d",	local_time.year, local_time.month, local_time.day,
													local_time.hour, local_time.min, local_time.sec );
	}
}

static void
event_update( u32 curr_clock )
{
	if ( motion_trigger_clock && ( curr_clock > ( motion_trigger_clock + EVENT_DURATION ) ) ) {
		HPClient_SendMotionDetectionReport( FALSE );
		motion_trigger_clock	= 0;

		dbg_r( "Motion Detected DONE" );
	}

	if ( curr_clock > ( sound_detected_clock + EVENT_DURATION ) ) {
		sound_detected_clock	= 0;
	}
}

//==========================================================================
// APIs
//==========================================================================
int
StateMachine_GetCameraConfig( CFG_CameraCtx *ctx )
{
	if ( ctx ) {
		ImageCtx	img_ctx;

		memcpy( ctx, &cfg_camera_ctx, sizeof( CFG_CameraCtx ) );

		Camera_GetImage( &img_ctx );

		rts_isp_to_cfg( &img_ctx, &ctx->image );

		return SUCCESS;
	}

	return FAILURE;
}

int
StateMachine_SetCameraConfig( CFG_CameraCtx *ctx )
{
	if ( ctx ) {
        CFG_CameraCtx ctx_tmp;

		memcpy( &ctx_tmp, &cfg_camera_ctx, sizeof( CFG_CameraCtx ) );
		memcpy( &cfg_camera_ctx, ctx, sizeof( CFG_CameraCtx ) );

		if ( live_stream_real_bitrate != cfg_camera_ctx.video.live.bitrate ) {
			live_stream_real_bitrate				= cfg_camera_ctx.video.live.bitrate;
			live_stream_current_bitrate				= stream_out_get_lower_bitrate( live_stream_real_bitrate );
			live_stream_last_change_bitrate_clock	= seconds();
		}

# if 1
		ImageCtx	img_ctx;

		Camera_GetImage( &img_ctx );
		rts_cfg_to_isp( &cfg_camera_ctx.image, &img_ctx );
		img_ctx.power_freq			= 2;
		Camera_SetImage( &img_ctx, 1 );

		if ( CFG_Save( &cfg_camera_ctx ) == FAILURE ) {
			dbg_error( "Do CFG_Save Failed !!!" );
		}

		return SUCCESS;
# else
		if ( ipcam_setup_camera() == FAILURE ) {
			dbg_error( "Do ipcam_setup_camera Failed !!!" );
			memcpy( &cfg_camera_ctx, &ctx_tmp, sizeof( CFG_CameraCtx ) );
			return FAILURE;
		} else {
			if ( CFG_Save( &cfg_camera_ctx ) == FAILURE ) {
				dbg_error( "Do CFG_Save Failed !!!" );
			}

			return SUCCESS;
		}
# endif
	}

	return FAILURE;
}

void
StateMachine_Run( void )
{
static int start_test = 0;

	BOOL	first_init	= FALSE;

	u32	curr_clock				= seconds();
	u32	check_job_1min			= 0;

	Camera_SetStreamCBFunc( stream_in_cb );
	Camera_SetMDCBFunc( md_cb );
	Camera_SetAudioCBFunc( audio_cb );

	Camera_SetAudioOutVolume( 0 );

	live_dc	= DC_CreateWriter( "0" );
	if ( live_dc == NULL ) {
		dbg_error( "live_dc == NULL" );
		exit( 0 );
	}

# ifdef TODO
	dbg_error( "TODO... UploadTask_Init" );
# else
	UploadTask_Init();
# endif

	ipcam_load_default_image();

	if ( CFG_Load( &cfg_camera_ctx ) == FAILURE ) {
		ipcam_load_default_image();

		if ( CFG_Save( &cfg_camera_ctx ) == FAILURE ) {
			dbg_error( "Do CFG_Save Failed !!!" );
		}
	} else {
		ipcam_cfg_adjust();
	}

	live_stream_real_bitrate	= cfg_camera_ctx.video.live.bitrate;
	live_stream_current_bitrate	= stream_out_get_lower_bitrate( live_stream_real_bitrate );

	if ( ipcam_setup_camera() == FAILURE ) {
		dbg_error( "Do ipcam_setup_camera Failed !!!" );
	}

	container_handle_init();

	if ( HPClient_Init( m_containers, m_service ) < 0 ) {
		dbg_error( "Do HPClient_Init Failed !!!" );
		exit( 0 );
	}

	HPClient_RegisterCBUpdateUploadStreamURL( upload_stream_url_update );
	HPClient_RegisterCBUpdateFWStatus( fw_status_update );
	HPClient_RegisterCBUpdateLocalTime( local_time_update );

# ifdef TODO
	dbg_error( "TODO... FileManager_Start" );
# else
	FileManager_Start();
# endif

    dbg_line( "ertos_heap_cur_free = %u Bs", ertos_heap_cur_free() );
    dbg_line( "ertos_heap_min_free = %u Bs", ertos_heap_min_free() );
    dbg_line( "rom_ertos_heap_size = %u Bs", rom_ertos_heap_size );

	while ( 1 ) {
		curr_clock	= seconds();

# ifdef TEST_MODE_ENABLE
		if ( live_stream_current_total_frames ) {
			if ( start_test == 0 ) {
				test();
				start_test = 1;
			}
		}
# endif

		container_handle_update();

		event_update( curr_clock );

//		if ( ( first_init == FALSE ) && HPClient_ServerOnline() ) {
//			if ( MediaCenter_ConnectionAvailable( 0 ) == FALSE ) {
//				if ( cfg_camera_ctx.video.live.url[ 0 ] == 'r' ) {
//					MediaCenter_StreamCTX scu_ctx;
//					char url[ MAX_URI_LENGTH + 1 ];
//
//					sprintf( url, "%s", cfg_camera_ctx.video.live.url );
//
//					if ( streaming_set_url( &scu_ctx, url, strlen( cfg_camera_ctx.video.live.url ) ) == SUCCESS ) {
//						MediaCenter_StartUpload( 0, &scu_ctx );
//					}
//				}
//			}
//		}

		if ( HPClient_ServerReady() ) {
			if ( local_time.year == 0 ) {
				HPClient_UpdateLocalTime();
				continue;
			}

			if ( first_init == FALSE ) {
				HPClient_SendEventRecordReport( 0, 0 );
				HPClient_SendMotionDetectionReport( FALSE );

				motion_trigger_clock	= 0;

				first_init = TRUE;
			}

			stream_in_monitor( curr_clock );
			stream_out_monitor( curr_clock );

			if ( ( curr_clock >= ( check_job_1min + 60 ) ) || ( curr_clock < check_job_1min ) ) {
				HPClient_UpdateLocalTime();

				check_job_1min	= curr_clock;
			}

			if ( fw_upgrade_progress == FW_PROGRESS_INIT ) {
				if ( fw_available == TRUE ) {
					dbg_warm( "Do FWUpgrade..." );
					fw_upgrade_progress	= 0;
					HPClient_DoFWUpgrade();
				}
			}

			if ( upload_streaming_url_request_clock ) {
				if ( curr_clock > ( upload_streaming_url_request_clock + 60 ) ) {
					if ( HPClient_GetUploadStreamToken() == HP_SUCCESS ) {
						upload_streaming_url_request_clock = seconds();
					}
				}
			} else if ( MediaCenter_ConnectionAvailable( 0 ) == FALSE ) {
				if ( HPClient_GetUploadStreamToken() == HP_SUCCESS ) {
					upload_streaming_url_request_clock = seconds();
				}
			}

			pthread_mutex_lock( &mutex );
			if ( down_streaming_url ) {
dbg_error( "down_streaming_url" );
				if ( MediaCenter_DownloadIsWorking() == FALSE ) {
dbg_error( "MediaCenter_DownloadIsWorking() == FALSE" );
					my_free( down_streaming_url );
					down_streaming_url	= NULL;
					MediaCenter_StopDownload();
					Camera_SetAudioOutVolume( 0 );
				}
			}
			pthread_mutex_unlock( &mutex );
		}

# ifdef TODO
# else
		record_file_update();
# endif
		msleep( 100 );
	}
}
