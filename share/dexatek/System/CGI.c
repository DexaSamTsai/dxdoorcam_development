/*!
 *  @file       CGI.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "CGI.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static Httpd_CGIContext *cgi_contexts[] = {
	&CGI_Camera,
	&CGI_Media,
	NULL,
};

//==========================================================================
// APIs
//==========================================================================
int
CGI_ReturnError( HttpdHandle httpd, char *reason )
{
	char data[ 256 ];
	int data_size = sprintf( data, "{\"Result\":\"Error\",\"Description\":\"%s\"}", reason );
	Httpd_SendResponse( httpd, "error.js", data, data_size );
	return FAILURE;
}

int
CGI_ChkAuth( HttpdHandle httpd, char *privilege )
{
	return SUCCESS;
}

int
CGI_Init( void )
{
	int i = 0;

	while ( cgi_contexts[ i ] ) {
		if ( Httpd_RegCGIService( cgi_contexts[ i ] ) != SUCCESS ) {
			dbg_warm( "Do Httpd_RegCGIService Failed!!!" );
			return -1;
		}

		i ++;
	}

	return SUCCESS;
}

