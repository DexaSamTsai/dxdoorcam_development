/*!
 *  @file       CGI.h
 *  @version    0.01
 *  @author     Sam Tsai
 */

# ifndef __CGI_H__
# define __CGI_H__

# ifdef __cplusplus
extern "C"
{
# endif // __cplusplus

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Service/HTTPd.h"

//==========================================================================
// Type Define
//==========================================================================
# define CGI_PATH	"cgi-bin/"

//==========================================================================
// Global Params
//==========================================================================
extern Httpd_CGIContext     CGI_Camera;
extern Httpd_CGIContext     CGI_Media;

//==========================================================================
// APIs
//==========================================================================
extern int
CGI_ReturnError( HttpdHandle httpd, char *reason );

extern int
CGI_ChkAuth( HttpdHandle httpd, char *privilege );

extern int
CGI_Init( void );

# ifdef __cplusplus
}
# endif // __cplusplus

# endif
