/*!
 *  @file       CGI_Media.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Camera.h"
# include "Service/HTTPd.h"
# include "DataManagement.h"
# include "System/CGI.h"
# include "Format/FMP4.h"

//==========================================================================
// Type Define
//==========================================================================
#define RESV_SIZE 			32
#define FRAME_POOL_SIZE		2 * 1024 * 1024

typedef struct {
	HttpdHandle		httpd;
	int				fd;
	int				ch;
	const char		*camera_name;

	u32				inited:			1;
	u32				is_file:		1;
	u32				start_stream:	1;
	u32				resv:			29;

	u8				*buff;
	u32				buff_size;
	u32				alloc_size;
	u32				file_size;

	char			etag[ 36 ];

	mp4_media_t		mp4_media;
	int				frame_cnt;

	FILE            *fp;
} cgi_media_context_t;

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static void
random_id( char *dest, int len )
{
	int i;
	char *dst = dest;

	for ( i = 0; i < len / 2; i ++ ) {
		dst += sprintf( dst, "%02X", ( u32 )( random() & 0xff ) );
	}

	dst[ 0 ] = 0;
}

static int
media_write_data( cgi_media_context_t *media, void *src, int size )
{
	int res;
	u32 curr_clock = seconds();

	while ( size > 0 ) {
		if ( ( curr_clock + 10 ) < seconds() ) {
			dbg_line( "Timeout" );
			return FAILURE;
		}
# ifdef MSG_NOSIGNAL
		res = send( media->fd, src, size, MSG_NOSIGNAL );
# else
		res = send( media->fd, src, size, 0 );
# endif

		if ( res < 0 ) {
			if ( ( errno == EAGAIN || errno == EWOULDBLOCK ) ) {
				MSleep( 50 );
				continue;
			}

			dbg_error( "Do send Failed!!!( res = %d, %s , fd = %d)", res, strerror( errno ), media->fd );
			return FAILURE;
		} else if ( res == 0 ) {
			MSleep( 50 );
		}

		size -= res;
		src  += res;

		media->file_size += res;
	}

	return SUCCESS;
}

static int
media_send_http( cgi_media_context_t *media, int datasize )
{
	int		size;
	char	http[ 1024 ];

	if ( media->etag[ 0 ] == 0 ) {
		random_id( media->etag, 32 );
	}

	size = sprintf( http, "HTTP/1.1 206 Partial Content\r\n"
						   "ETag: \"%s%s\"\r\n"
						   "Content-Type: video/mp4\r\n"
						   "Accept-Ranges: bytes\r\n"
						   "Content-Range: bytes %d-%d\r\n"
						   "Content-Length: %d\r\n"
						   "Access-Control-Allow-Origin: *\r\n"
						   "Access-Control-Expose-Headers: Content-Length, Date, Server, Transfer-Encoding, origin, range\r\n"
						   "Alternate-Protocol: 80:quic,p=0.5\r\n\r\n", media->camera_name, media->etag, media->file_size, media->file_size + datasize - 1, datasize );

	return media_write_data( media, http, size );
}

static int
media_write_file_header( cgi_media_context_t *media )
{
	media->mp4_media.start_time		= media->mp4_media.frame[ 0 ].ptime;
	media->mp4_media.pre_time		= media->mp4_media.frame[ 0 ].ptime;

	MP4_BuildFileHeader( &media->mp4_media );

	if ( ( media->is_file == FALSE ) && ( media_send_http( media, media->mp4_media.header_size ) != SUCCESS ) ) {
		dbg_line( "Do media_send_http Failed!!!" );
		return FAILURE;
	}

	if ( media->fp ) {
		fwrite( media->mp4_media.header, 1, media->mp4_media.header_size, media->fp );
	}

	if ( media_write_data( media, media->mp4_media.header, media->mp4_media.header_size ) != SUCCESS ) {
		dbg_line( "Do media_write_data Failed!!!" );
		return FAILURE;
	}

	if ( ( media->is_file == FALSE ) && ( Httpd_RecvHttpHeader( media->httpd ) != SUCCESS ) ) {
		dbg_line( "Do Httpd_RecvHttpHeader Failed!!!" );
		return FAILURE;
	}

	return SUCCESS;
}

static int
media_write_media_header( cgi_media_context_t *media )
{
	MP4_BuildMediaHeader( &media->mp4_media );

	if ( media->fp ) {
		fwrite( media->mp4_media.header, 1, media->mp4_media.header_size, media->fp );
	}

	if ( ( media->is_file == FALSE ) && ( media_send_http( media, ( media->buff_size + media->mp4_media.header_size ) ) != SUCCESS ) ) {
		return FAILURE;
	} else {
		return media_write_data( media, media->mp4_media.header, media->mp4_media.header_size );
	}
}

static int
media_realloc_buff( cgi_media_context_t *media, u32 expect_size )
{
	u32 new_size	= ( expect_size + 0x00000FFF ) & 0xFFFFF000;
	u8 *org_buff	= media->buff;
	u8 *new_buff	= ( u8 * )my_realloc( media->buff, new_size );

	media->alloc_size		= new_size;

	if ( new_buff == NULL ) {
		media->buff = NULL;
		return FAILURE;
	} else {
		if ( new_buff != media->buff ) {
			media->buff = new_buff;
			return MP4_ResetFramePos( &media->mp4_media, org_buff, new_buff );
		}
		return SUCCESS;
	}
}

static cgi_media_context_t *
media_create( void )
{
	cgi_media_context_t *media = ( cgi_media_context_t * )my_malloc( sizeof( cgi_media_context_t ) );

	if ( media ) {
		media->alloc_size	= FRAME_POOL_SIZE;
		media->buff			= ( u8 * )my_malloc( media->alloc_size );
		if ( media->buff == NULL ) {
			my_free( media );
			return NULL;
		}
	} else {
		dbg_line( "Do my_malloc Failed!!!" );
	}

	return  media;
}

static int
media_write( cgi_media_context_t *media, av_frame_t *frame )
{
	if ( frame->iskey ) {
		Dump( frame->buff, 64 );
	}

	if ( ( media->buff_size + RESV_SIZE ) > media->alloc_size ) {
		dbg_line( "Memory Realloc( %d )", media->alloc_size );
		media_realloc_buff( media, ( media->buff_size + RESV_SIZE ) );
	}

	media->buff_size += MP4_PutFrame( &media->mp4_media, frame, &media->buff[ media->buff_size ] );

	return SUCCESS;
}

static int
media_flush( cgi_media_context_t *media )
{
	if ( media->inited == 0 ) {
		if ( media_write_file_header( media ) != SUCCESS ) {
			return FAILURE;
		}
		media->inited = 1;
	}

	if ( media_write_media_header( media ) != SUCCESS ) {
		dbg_error( "Do mp4_write_media_header Failed!!!" );
		return FAILURE;
	} else if ( media_write_data( media, media->buff, media->buff_size ) != SUCCESS ) {
		dbg_error( "Do media_write_data Failed!!!( %s )", strerror( errno ) );
		return FAILURE;
	} else {
		if ( media->fp ) {
			fwrite( media->buff, 1, media->buff_size, media->fp );
		}

		media->buff_size	= 0;
		media->mp4_media.media_size	= 0;
		media->mp4_media.frame_cnt	= 0;
	}

	if ( media->is_file == TRUE ) {
		return SUCCESS;
	} else {
		return Httpd_RecvHttpHeader( media->httpd );
	}
}

static void
media_close( cgi_media_context_t *media )
{
	if ( media->buff ) {
		my_free( media->buff );
	}

	my_free( media );
}

static void
media_live( cgi_media_context_t *media )
{
	void *reader	= DC_CreateReader( "0" );

	if ( reader ) {
		av_frame_t		frame;

		while ( TRUE ) {
			if ( DC_ReadFrame( reader, &frame ) == FAILURE ) {
				dbg_line( "Do DC_ReadFrame Failed!!!" );
				break;
			}

			if ( frame.size ) {
				if (  ( frame.type & FRAME_AV_MASK ) == FRAME_AUDIO ) {
					continue;
				}

				if ( media_write( media, &frame ) != SUCCESS ) {
					dbg_line( "Do media_write Failed!!!" );
					break;
				}

				media->frame_cnt ++;

				if ( media->frame_cnt > 5 ) {
					if ( media_flush( media ) != SUCCESS ) {
						dbg_line( "Do media_flush Failed!!!" );
						break;
					}
					media->frame_cnt = 0;
				}
			} else {
				MSleep( 30 );
			}
		}

		DC_RemoveReader( reader );

		dbg_line( "Done." );
	} else {
		dbg_line( "Do DC_CreateReader Failed." );
		Httpd_SendHttpHeader( media->httpd, "Error.html", 403 );
	}
}

static int
media_get( HttpdHandle httpd, int fd )
{
	cgi_media_context_t		*media = media_create();

	if ( media ) {
		media->httpd				= httpd;
		media->fd					= fd;
		media->mp4_media.is_stream	= TRUE;
		media->is_file				= FALSE;

        media->fp   = fopen( "/mnt/test.mp4", "wb" );

		media_live( media );

		media_close( media );
	} else {
		Httpd_SendHttpHeader( httpd, "Error.html", 500 );
	}

	return FAILURE;
}

//==========================================================================
// APIs
//==========================================================================
Httpd_CGIContext	CGI_Media = {
	.url		= CGI_PATH"Media",
	.cgi_get	= media_get,
	.cgi_post	= NULL,
};


