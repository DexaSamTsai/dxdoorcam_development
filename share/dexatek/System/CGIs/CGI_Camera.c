/*!
 *  @file       CGI_Camera.c
 *  @version    0.01
 *  @author     Sam Tsai
 */

//==========================================================================
// Include File
//==========================================================================
# include "Headers.h"
# include "Camera.h"
# include "Config/Config.h"
# include "Service/HTTPd.h"
# include "System/StateMachine.h"
# include "System/CGI.h"

//==========================================================================
// Type Define
//==========================================================================

//==========================================================================
// Global Params
//==========================================================================

//==========================================================================
// Static Params
//==========================================================================

//==========================================================================
// Static Functions
//==========================================================================
static int
camera_get( HttpdHandle httpd, int fd )
{
	char	*data	= NULL;
	int		size;
    CFG_CameraCtx ctx;

    StateMachine_GetCameraConfig( &ctx );

	size	= CFG_Generate( &ctx, &data );

	if ( size > 0 ) {
		dbg_line( "%s", data );
		dbg_line( "size = %d", size );
		Httpd_SendResponse( httpd, "camera.json", data, size );
		my_free( data );
		return FAILURE;
	} else {
		return CGI_ReturnError( httpd, "Get Config Error" );
	}
}

static int
camera_post( HttpdHandle httpd, int fd )
{
	char *input;
	int input_size	= Httpd_RecvData( httpd, ( u8 ** )&input );

	if ( input_size > 0 ) {
        CFG_CameraCtx ctx;
		dbg_warm( "input_size: %d", input_size );
		dbg_line( "%s", input );
		StateMachine_GetCameraConfig( &ctx );
		int res = CFG_Parser( input, input_size, &ctx );

		my_free( input );

		if ( res == FAILURE ) {
			dbg_warm( "Set Camera Failed" );
			return CGI_ReturnError( httpd, "Set Camera Failed" );
		} else if ( StateMachine_SetCameraConfig( &ctx ) == FAILURE ) {
			dbg_warm( "Set Camera Failed" );
			return CGI_ReturnError( httpd, "Set Camera Failed" );
		} else {
			char data[ 256 ];
			int size = sprintf( data, "{\"Result\":\"Success\"}" );
			dbg_warm( "%s", data );

			Httpd_SendResponse( httpd, "camera.js", data, size );

			return FAILURE;
		}
	} else {
		return CGI_ReturnError( httpd, "Parameter Error" );
	}
}

//==========================================================================
// APIs
//==========================================================================
Httpd_CGIContext	CGI_Camera = {
	.url		= CGI_PATH"Camera",
	.cgi_get	= camera_get,
	.cgi_post	= camera_post,
};


